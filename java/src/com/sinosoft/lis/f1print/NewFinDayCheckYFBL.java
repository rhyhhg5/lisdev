package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author  lys
 * @date:2003-06-04
 * @version 1.0
 */

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class NewFinDayCheckYFBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mDay[] = null; //获取时间
    private GlobalInput mGlobalInput = new GlobalInput(); //全局变量
    public NewFinDayCheckYFBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        if (cOperate.equals("PRINTPAY")) //打印收费
        {
            if (!getPrintDataPay())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) //打印付费
    {
        //全局变量
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FinDayCheckBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintDataPay()
    {
        ExeSQL exesql = new ExeSQL();
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("NewFinDayCheckYF.vts", "printer"); //最好紧接着就初始化xml文档

        /***************************************************************************
         * date:2003-06-04
         * function:打印财务应付日结清单
         * description：采用的日期是入机日期（MakeDate）；查询的总表是LJAGet(实付总表)
         * author :liuyansong
         */
        //在LJAGet实付总表中查询出该时间段，该管理机构的所有的记录的实付号码和类型
        String ActuGetNo_sql =
                " Select ActuGetNo  From LJAGet Where MakeDate >='"
                + mDay[0] + "' and MakeDate <= '" + mDay[1] +
                "' and ManageCom Like '"
                + mGlobalInput.ManageCom + "%' ";
        //生存领取表的查询
        String Type1 = " and OtherNoType in ('0','1','2') ";
        //赔付实付表的查询
        String Type2 = " and OtherNoType = '5' ";
        //批改补退费表的查询
        String Type3 = " and OtherNoType = '3' ";
        //对暂收费退费的查询语句是
        String Type4 = " and OtherNoType = '4' ";
        //对其他表的查询语句是
        String Type5 = " and OtherNoType in ('6','8') ";
        //对红利表的查询是
        String Type6 = " and OtherNoType = '7' ";

        //满期信息的查询
        String ManQi = "Select RiskCode,Sum(GetMoney),Sum(1) From LJAGetDraw "
                       + " Where ActuGetNo in ( " + ActuGetNo_sql + Type1 +
                       " ) "
                       +
                " and GetDutyCode in (Select GetDutyCode From LMDutyGet Where GetType1 = '0' ) ";
        //年金信息的查询
        String NianJin =
                "Select RiskCode,Sum(GetMoney),Sum(1) From LJAGetDraw "
                + " Where ActuGetNo in ( " + ActuGetNo_sql + Type1 + ") "
                + " and GetDutyCode in (Select GetDutyCode From LMDutyGet Where GetType1 <> '0' ) ";
        //伤残给付的查询
        String ShangCan = "Select RiskCode,Sum(Pay),Sum(1) From LJAGetClaim "
                          + "Where ActuGetNo in ( " + ActuGetNo_sql + Type2 +
                          " ) "
                          + " and GetDutyKind in ('101','201') ";
        //医疗给付的查询
        String YiLiao = "Select RiskCode,Sum(Pay),Sum(1) From LJAGetClaim "
                        + "Where ActuGetNo in ( " + ActuGetNo_sql + Type2 +
                        " ) "
                        + " and GetDutyKind in ('100','200')";
        //退保金的查询
        String TuiBao =
                "Select RiskCode,Sum(GetMoney),Sum(1) From LJAGetEndorse "
                + "Where ActuGetNo in ( " + ActuGetNo_sql + Type3 + " )"
                + " and FeeFinaType = 'TB' ";
        //保全退费－退余额
        String YuE = "Select RiskCode,Sum(GetMoney),Sum(1) From LJAGetEndorse "
                     + "Where ActuGetNo in ( " + ActuGetNo_sql + Type3 + " )"
                     + " and FeeFinaType = 'EY' ";
        //保全退费－退费
        String TuiFei =
                "Select RiskCode,Sum(GetMoney),Sum(1) From LJAGetEndorse "
                + "Where ActuGetNo in ( " + ActuGetNo_sql + Type3 + " )"
                + " and FeeFinaType = 'TF' ";

        //死亡给付的查询
        String SiWang = "Select RiskCode,Sum(Pay),Sum(1) From LJAGetClaim "
                        + " Where ActuGetNo in ( " + ActuGetNo_sql + Type2 +
                        " ) "
                        + " and GetDutyKind in ('001','102','202')";
        //对暂收费退费的查询
        String ZanShou =
                "Select RiskCode,Sum(GetMoney),Sum(1) From LJAGetTempFee "
                + " Where ActuGetNo in ( " + ActuGetNo_sql + Type4 + " ) ";
        //对其他退费的查询
        String QiTa_1 = "Select RiskCode,Sum(GetMoney),Sum(1) From ( ";
        String QiTa_2 = "Select RiskCode,GetMoney ,1 From LBPol,LJAGetOther Where LBPol.PolNo = LJAGetOther.OtherNo  and ActuGetNo in ( " +
                        ActuGetNo_sql + Type5 + " ) ";
        String QiTa_3 = "union all Select RiskCode,GetMoney ,2 From LCPol,LJAGetOther Where LCPol.PolNo = LJAGetOther.OtherNo and ActuGetNo in ( " +
                        ActuGetNo_sql + Type5 + " ) ";
        String QiTa_4 = "union all Select RiskCode,GetMoney ,3 From LBGrpPol,LJAGetOther Where LBGrpPol.GrpPolNo = LJAGetOther.OtherNo  and ActuGetNo in ( " +
                        ActuGetNo_sql + Type5 + " ) ";
        String QiTa_5 = "union all Select RiskCode,GetMoney ,4 From LCGrpPol,LJAGetOther Where LCGrpPol.GrpPolNo = LJAGetOther.OtherNo  and ActuGetNo in ( " +
                        ActuGetNo_sql + Type5 + " ) ";
        //对红利的查询语句
        String HongLi_1 = "Select RiskCode,Sum(GetMoney),Sum(1) From ( ";
        String HongLi_2 = "Select RiskCode,GetMoney ,1 From LBPol,LJABonusGet Where LBPol.PolNo = LJABonusGet.OtherNo  and ActuGetNo in ( " +
                          ActuGetNo_sql + Type6 + " ) ";
        String HongLi_3 = "union all Select RiskCode,GetMoney ,2 From LCPol,LJABonusGet Where LCPol.PolNo = LJABonusGet.OtherNo and ActuGetNo in ( " +
                          ActuGetNo_sql + Type6 + " ) ";

        String Grp = " Group By RiskCode";

        /***************************************************************************
         * 一级科目分类明细的查询标准
         * 在LJAGet(表)中的OtherNoType的分类详细如下：
         * （0,1,2）满期和年金 --------（生存领取表）
         * ----满期：（LMDutyGet表中的GetType1＝'0'）
         * ----年金：（LMDutyGet表中的GetType1<>'0'）
         * （5） 赔付（伤残、医疗、死亡）--------（赔付实付表）
         * ----伤残：（GetDutyKind in（'100','200'））
         * ----医疗:（GetDutyKind in（'101','201'））
         * ----死亡：（GetDutyKind in（'001','102','202'））
         * （3）批改补退费(FeeFinaType='TB')
         * （4）暂收费退费
         * （7）红利
         * （8，6）其他
         */

        /**************************************************************************
         * 二级科目分类明细
         * 在LMRiskApp（表）中的RiskType4的分类详细如下：
         * 1  终身 ZhongS   2  两全及生存 LiangQ   3  定期 DingQ  4  年金NianJ
         * 5  重大疾病 ZhongJ  6  意外YiW  7  健康JianK   (8,9)  其他QiT
         *
         */

        String ZhongS =
                " and RiskCode in ( Select RiskCode From LMRiskApp Where  RiskType4 = '1' )";
        String LiangQ =
                " and RiskCode in ( Select RiskCode From LMRiskApp Where  RiskType4 = '2' )";
        String DingQ =
                " and RiskCode in ( Select RiskCode From LMRiskApp Where  RiskType4 = '3' )";
        String NianJ =
                " and RiskCode in ( Select RiskCode From LMRiskApp Where  RiskType4 = '4' )";
        String ZhongJ =
                " and RiskCode in ( Select RiskCode From LMRiskApp Where  RiskType4 = '5' )";
        String YiW =
                " and RiskCode in ( Select RiskCode From LMRiskApp Where  RiskType4 = '6' )";
        String JianK =
                " and RiskCode in ( Select RiskCode From LMRiskApp Where  RiskType4 = '7' )";
        String QiT = " and RiskCode in ( Select RiskCode From LMRiskApp Where  RiskType4 = '8' or RiskType4 = '9' )";

        int Count = 0;
        double Money = 0;
        //满期
        int mq_count = 0; //满期总数
        int mq_count1 = 0;
        int mq_count2 = 0;
        int mq_count3 = 0;
        int mq_count4 = 0;
        int mq_count5 = 0;
        int mq_count6 = 0;
        int mq_count7 = 0;
        int mq_count8 = 0;

        double mq_money = 0; //满期总金额
        double mq_money1 = 0;
        double mq_money2 = 0;
        double mq_money3 = 0;
        double mq_money4 = 0;
        double mq_money5 = 0;
        double mq_money6 = 0;
        double mq_money7 = 0;
        double mq_money8 = 0;

        //对年金进行初始化
        int nj_count = 0; //年金总数
        int nj_count1 = 0;
        int nj_count2 = 0;
        int nj_count3 = 0;
        int nj_count4 = 0;
        int nj_count5 = 0;
        int nj_count6 = 0;
        int nj_count7 = 0;
        int nj_count8 = 0;

        double nj_money = 0; //年金总金额
        double nj_money1 = 0;
        double nj_money2 = 0;
        double nj_money3 = 0;
        double nj_money4 = 0;
        double nj_money5 = 0;
        double nj_money6 = 0;
        double nj_money7 = 0;
        double nj_money8 = 0;

        //对伤残给付的初始化
        int sc_count = 0; //伤残总数
        int sc_count1 = 0;
        int sc_count2 = 0;
        int sc_count3 = 0;
        int sc_count4 = 0;
        int sc_count5 = 0;
        int sc_count6 = 0;
        int sc_count7 = 0;
        int sc_count8 = 0;

        double sc_money = 0; //伤残总金额
        double sc_money1 = 0;
        double sc_money2 = 0;
        double sc_money3 = 0;
        double sc_money4 = 0;
        double sc_money5 = 0;
        double sc_money6 = 0;
        double sc_money7 = 0;
        double sc_money8 = 0;

        //对医疗给付的初始化
        int yl_count = 0; //医疗总数
        int yl_count1 = 0;
        int yl_count2 = 0;
        int yl_count3 = 0;
        int yl_count4 = 0;
        int yl_count5 = 0;
        int yl_count6 = 0;
        int yl_count7 = 0;
        int yl_count8 = 0;

        double yl_money = 0; //医疗总金额
        double yl_money1 = 0;
        double yl_money2 = 0;
        double yl_money3 = 0;
        double yl_money4 = 0;
        double yl_money5 = 0;
        double yl_money6 = 0;
        double yl_money7 = 0;
        double yl_money8 = 0;

        //对退保金给付的初始化
        int tb_count = 0; //退保总数
        int tb_count1 = 0;
        int tb_count2 = 0;
        int tb_count3 = 0;
        int tb_count4 = 0;
        int tb_count5 = 0;
        int tb_count6 = 0;
        int tb_count7 = 0;
        int tb_count8 = 0;

        double tb_money = 0; //退保总金额
        double tb_money1 = 0;
        double tb_money2 = 0;
        double tb_money3 = 0;
        double tb_money4 = 0;
        double tb_money5 = 0;
        double tb_money6 = 0;
        double tb_money7 = 0;
        double tb_money8 = 0;

        //对死亡给付的初始化
        int sw_count = 0; //死亡总数
        int sw_count1 = 0;
        int sw_count2 = 0;
        int sw_count3 = 0;
        int sw_count4 = 0;
        int sw_count5 = 0;
        int sw_count6 = 0;
        int sw_count7 = 0;
        int sw_count8 = 0;

        double sw_money = 0; //死亡总金额
        double sw_money1 = 0;
        double sw_money2 = 0;
        double sw_money3 = 0;
        double sw_money4 = 0;
        double sw_money5 = 0;
        double sw_money6 = 0;
        double sw_money7 = 0;
        double sw_money8 = 0;

        //暂收费
        int zs_count = 0; //暂收总数
        int zs_count1 = 0;
        int zs_count2 = 0;
        int zs_count3 = 0;
        int zs_count4 = 0;
        int zs_count5 = 0;
        int zs_count6 = 0;
        int zs_count7 = 0;
        int zs_count8 = 0;

        double zs_money = 0; //暂收总金额
        double zs_money1 = 0;
        double zs_money2 = 0;
        double zs_money3 = 0;
        double zs_money4 = 0;
        double zs_money5 = 0;
        double zs_money6 = 0;
        double zs_money7 = 0;
        double zs_money8 = 0;

        //其他给付的初始化
        int qt_count = 0; //其他总个数
        int qt_count1 = 0;
        int qt_count2 = 0;
        int qt_count3 = 0;
        int qt_count4 = 0;
        int qt_count5 = 0;
        int qt_count6 = 0;
        int qt_count7 = 0;
        int qt_count8 = 0;

        double qt_money = 0; //其他总金额
        double qt_money1 = 0;
        double qt_money2 = 0;
        double qt_money3 = 0;
        double qt_money4 = 0;
        double qt_money5 = 0;
        double qt_money6 = 0;
        double qt_money7 = 0;
        double qt_money8 = 0;

        //对红利的初始化
        int hl_count = 0; //其他总个数
        int hl_count1 = 0;
        int hl_count2 = 0;
        int hl_count3 = 0;
        int hl_count4 = 0;
        int hl_count5 = 0;
        int hl_count6 = 0;
        int hl_count7 = 0;
        int hl_count8 = 0;

        double hl_money = 0; //其他总金额
        double hl_money1 = 0;
        double hl_money2 = 0;
        double hl_money3 = 0;
        double hl_money4 = 0;
        double hl_money5 = 0;
        double hl_money6 = 0;
        double hl_money7 = 0;
        double hl_money8 = 0;

        //保全退费……退余额
        int ye_count = 0; //余额总个数
        int ye_count1 = 0;
        int ye_count2 = 0;
        int ye_count3 = 0;
        int ye_count4 = 0;
        int ye_count5 = 0;
        int ye_count6 = 0;
        int ye_count7 = 0;
        int ye_count8 = 0;

        double ye_money = 0; //余额总金额
        double ye_money1 = 0;
        double ye_money2 = 0;
        double ye_money3 = 0;
        double ye_money4 = 0;
        double ye_money5 = 0;
        double ye_money6 = 0;
        double ye_money7 = 0;
        double ye_money8 = 0;

        //保全退费－退费的初始化
        int tf_count = 0; //退费总个数
        int tf_count1 = 0;
        int tf_count2 = 0;
        int tf_count3 = 0;
        int tf_count4 = 0;
        int tf_count5 = 0;
        int tf_count6 = 0;
        int tf_count7 = 0;
        int tf_count8 = 0;

        double tf_money = 0; //余额总金额
        double tf_money1 = 0;
        double tf_money2 = 0;
        double tf_money3 = 0;
        double tf_money4 = 0;
        double tf_money5 = 0;
        double tf_money6 = 0;
        double tf_money7 = 0;
        double tf_money8 = 0;

        for (int t = 1; t <= 11; t++)
        {
            if (t == 1)
            {
                //满期给付
                for (int k = 1; k <= 8; k++)
                {
                    ListTable tlisttable = new ListTable();
                    String tRiskCode = "";
                    //满期－终身 寿险
                    if (k == 1)
                    {
                        System.out.println("满期－终身寿险");
                        String MQZS = ManQi + ZhongS + Grp;
                        SSRS MQZS_ssrs = exesql.execSQL(MQZS);
                        if (MQZS_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= MQZS_ssrs.getMaxRow(); i++)
                            {
                                mq_money1 = mq_money1 +
                                            Double.parseDouble(MQZS_ssrs.
                                        GetText(i, 2));
                                mq_count1 = mq_count1 +
                                            Integer.parseInt(
                                            MQZS_ssrs.GetText(i, 3));
                                System.out.println( +i + "满期金额是" +
                                        Double.
                                        parseDouble(MQZS_ssrs.GetText(i, 2)));
                                tRiskCode = "满期给付\\终身寿险\\";
                            }
                            System.out.println("满期总金额是" + mq_money1);
                            tlisttable.setName("RISKA1");
                        }

                    }
                    //满期－两全 险
                    if (k == 2)
                    {
                        System.out.println("满期－两全险");
                        String MQLQ = ManQi + LiangQ + Grp;
                        SSRS MQLQ_ssrs = exesql.execSQL(MQLQ);
                        if (MQLQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= MQLQ_ssrs.getMaxRow(); i++)
                            {
                                mq_money2 = mq_money2 +
                                            Double.parseDouble(MQLQ_ssrs.
                                        GetText(i, 2));
                                mq_count2 = mq_count2 +
                                            Integer.parseInt(
                                            MQLQ_ssrs.GetText(i, 3));
                                System.out.println( +i + "满期两全险的金额是" +
                                        Double.
                                        parseDouble(MQLQ_ssrs.GetText(i, 2)));
                                tRiskCode = "满期给付\\两全险\\";
                            }
                            System.out.println("满期两全总金额是" + mq_money2);
                            tlisttable.setName("RISKA2");
                        }

                    }
                    //满期－定期 保险
                    if (k == 3)
                    {
                        String MQDQ = ManQi + DingQ + Grp;
                        System.out.println("满期－定期保险");
                        SSRS MQDQ_ssrs = exesql.execSQL(MQDQ);
                        if (MQDQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= MQDQ_ssrs.getMaxRow(); i++)
                            {
                                mq_money3 = mq_money3 +
                                            Double.parseDouble(MQDQ_ssrs.
                                        GetText(i, 2));
                                mq_count3 = mq_count3 +
                                            Integer.parseInt(
                                            MQDQ_ssrs.GetText(i, 3));
                                System.out.println(i + "满期定期保险的金额是" +
                                        Double.
                                        parseDouble(MQDQ_ssrs.GetText(i, 2)));
                                tRiskCode = "满期给付\\定期保险\\";
                            }
                            System.out.println("满期－定期的中金额是" + mq_money3);
                            tlisttable.setName("RISKA3");
                        }

                    }
                    if (k == 4)
                    {
                        //满期－年金 保险
                        String MQNJ = ManQi + NianJ + Grp;
                        System.out.println("满期－年金保险");
                        SSRS MQNJ_ssrs = exesql.execSQL(MQNJ);
                        if (MQNJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= MQNJ_ssrs.getMaxRow(); i++)
                            {
                                mq_money4 = mq_money4 +
                                            Double.parseDouble(MQNJ_ssrs.
                                        GetText(i, 2));
                                mq_count4 = mq_count4 +
                                            Integer.parseInt(
                                            MQNJ_ssrs.GetText(i, 3));
                                System.out.println(i + "满期－年金保险的金额是" +
                                        Double.
                                        parseDouble(MQNJ_ssrs.GetText(i, 2)));
                                tRiskCode = "满期给付\\年金保险\\";
                            }
                            System.out.println("满期－年金保险的总金额是" + mq_money4);
                            tlisttable.setName("RISKA4");
                        }
                    }
                    if (k == 5)
                    {
                        //满期－重大疾病保险
                        String MQZJ = ManQi + ZhongJ + Grp;
                        System.out.println("满期－重大疾病");
                        SSRS MQZJ_ssrs = exesql.execSQL(MQZJ);
                        if (MQZJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= MQZJ_ssrs.getMaxRow(); i++)
                            {
                                mq_money5 = mq_money5 +
                                            Double.parseDouble(MQZJ_ssrs.
                                        GetText(i, 2));
                                mq_count5 = mq_count5 +
                                            Integer.parseInt(
                                            MQZJ_ssrs.GetText(i, 3));
                                System.out.println(i + "满期－重大疾病的金额是" +
                                        Double.
                                        parseDouble(MQZJ_ssrs.GetText(i, 2)));
                                tRiskCode = "满期给付\\重大疾病保险\\";
                            }
                            System.out.println("满期－重大疾病的总金额是" + mq_money5);
                            tlisttable.setName("RISKA5");
                        }
                    }
                    //满期－意外伤害 保险
                    if (k == 6)
                    {
                        String MQYW = ManQi + YiW + Grp;
                        System.out.println("满期－意外伤害保险");
                        SSRS MQYW_ssrs = exesql.execSQL(MQYW);
                        if (MQYW_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= MQYW_ssrs.getMaxRow(); i++)
                            {
                                mq_money6 = mq_money6 +
                                            Double.parseDouble(MQYW_ssrs.
                                        GetText(i, 2));
                                mq_count6 = mq_count6 +
                                            Integer.parseInt(
                                            MQYW_ssrs.GetText(i, 3));
                                System.out.println(i + "满期－意外伤害保险的金额是" +
                                        Double.
                                        parseDouble(MQYW_ssrs.GetText(i, 2)));
                                tRiskCode = "满期给付\\意外伤害\\";
                            }
                            System.out.println("满期－意外伤害的总金额是" + mq_money6);
                            tlisttable.setName("RISKA6");
                        }
                    }
                    if (k == 7)
                    {
                        //满期－健康 险
                        String MQJK = ManQi + JianK + Grp;
                        System.out.println("满期－健康险");
                        SSRS MQJK_ssrs = exesql.execSQL(MQJK);
                        if (MQJK_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= MQJK_ssrs.getMaxRow(); i++)
                            {
                                mq_money7 = mq_money7 +
                                            Double.parseDouble(MQJK_ssrs.
                                        GetText(i, 2));
                                mq_count7 = mq_count7 +
                                            Integer.parseInt(
                                            MQJK_ssrs.GetText(i, 3));
                                System.out.println(i + "满期－健康险的金额是" +
                                        Double.
                                        parseDouble(MQJK_ssrs.GetText(i, 2)));
                                tRiskCode = "满期给付\\健康险\\";
                            }
                            System.out.println("满期－健康险的总金额是" + mq_money7);
                            tlisttable.setName("RISKA7");
                        }
                    }
                    if (k == 8)
                    {
                        //满期－其他 险
                        String MQQT = ManQi + QiT + Grp;
                        System.out.println("满期－其他险");
                        SSRS MQQT_ssrs = exesql.execSQL(MQQT);
                        if (MQQT_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= MQQT_ssrs.getMaxRow(); i++)
                            {
                                mq_money8 = mq_money8 +
                                            Double.parseDouble(MQQT_ssrs.
                                        GetText(i, 2));
                                mq_count8 = mq_count8 +
                                            Integer.parseInt(
                                            MQQT_ssrs.GetText(i, 3));
                                System.out.println(i + "满期－其他险的金额是" +
                                        Double.
                                        parseDouble(MQQT_ssrs.GetText(i, 2)));
                                tRiskCode = "满期给付\\其他\\";
                            }
                            System.out.println("满期－其他险的总金额是" + mq_money8);
                            tlisttable.setName("RISKA8");
                        }

                    }
                    //会计的三级明细
                    String strArr[] = null;
                    //抽象成函数
                    String tXsqdName = ""; //销售渠道
                    String tXzdlName = ""; //险种大类
                    String tSxqName = ""; //首续期
                    String tDqjName = ""; //趸期交

                    for (int i = 1; i <= 4; i++) //销售渠道
                    {
                        String tXsqdSql = getXsqdSql(i);
                        System.out.println(i + "当i 的值是" + i + "的时候");
                        if (k == 1)
                        {
                            tXsqdSql = ManQi + ZhongS + tXsqdSql;
                        }
                        if (k == 2)
                        {
                            tXsqdSql = ManQi + LiangQ + tXsqdSql;
                        }
                        if (k == 3)
                        {
                            tXsqdSql = ManQi + DingQ + tXsqdSql;
                        }
                        if (k == 4)
                        {
                            tXsqdSql = ManQi + NianJ + tXsqdSql;
                        }
                        if (k == 5)
                        {
                            tXsqdSql = ManQi + ZhongJ + tXsqdSql;
                        }
                        if (k == 6)
                        {
                            tXsqdSql = ManQi + YiW + tXsqdSql;
                        }
                        if (k == 7)
                        {
                            tXsqdSql = ManQi + JianK + tXsqdSql;
                        }
                        if (k == 8)
                        {
                            tXsqdSql = ManQi + QiT + tXsqdSql;
                        }
                        for (int j = 1; j <= 4; j++) //险种大类
                        {
                            String tXzdlSql = getXzdlSql(j);
                            tXzdlSql = tXsqdSql + tXzdlSql + Grp;
                            System.out.println("K的值是" + k);
                            System.out.println("I的值是" + i);
                            System.out.println("J的值是" + j);
                            System.out.println(j + "查询的语句是");
                            SSRS tssrs = exesql.execSQL(tXzdlSql);
                            if (tssrs.getMaxRow() != 0)
                            {
                                for (int q = 1; q <= tssrs.MaxRow; q++)
                                {
                                    strArr = new String[7];
                                    String tRiskcode = tssrs.GetText(q, 1);
                                    LMRiskDB tLMRiskDB = new LMRiskDB();
                                    tLMRiskDB.setRiskCode(tssrs.GetText(q, 1));
                                    if (tLMRiskDB.getInfo())
                                    {
                                        strArr[0] = tRiskCode +
                                                tLMRiskDB.getRiskName();
                                    }
                                    strArr[1] = getXsqdName(i);
                                    strArr[2] = getXzdlName(j);
                                    strArr[5] = tssrs.GetText(q, 2);
                                    strArr[6] = tssrs.GetText(q, 3);
                                    tlisttable.add(strArr);
                                }
                                strArr = new String[7];
                                strArr[0] = "Risk";
                                strArr[1] = "Xsqd";
                                strArr[2] = "Xzdl";
                                strArr[3] = "Sxq";
                                strArr[4] = "Dqj";
                                strArr[5] = "Money";
                                strArr[6] = "Count";
                                xmlexport.addListTable(tlisttable, strArr);
                            }
                        }
                    }
                }
                //以上是k的结束
            }
            if (t == 2)
            {
                //年金给付
                for (int k = 1; k <= 8; k++)
                {
                    ListTable tlisttable = new ListTable();
                    String tRiskCode = "";
                    //年金给付－终身 寿险
                    if (k == 1)
                    {
                        System.out.println("年金给付－终身寿险");
                        String NJZS = NianJin + ZhongS + Grp;
                        SSRS NJZS_ssrs = exesql.execSQL(NJZS);
                        if (NJZS_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= NJZS_ssrs.getMaxRow(); i++)
                            {
                                nj_money1 = nj_money1 +
                                            Double.parseDouble(NJZS_ssrs.
                                        GetText(i, 2));
                                nj_count1 = nj_count1 +
                                            Integer.parseInt(
                                            NJZS_ssrs.GetText(i, 3));
                                System.out.println( +i + "年金金额是" +
                                        Double.
                                        parseDouble(NJZS_ssrs.GetText(i, 2)));
                                tRiskCode = "年金给付\\终身寿险\\";
                            }
                            System.out.println("年金终身寿险总金额是" + nj_money1);
                        }
                        tlisttable.setName("RISKB1");
                    }
                    //年金给付－两全 险
                    if (k == 2)
                    {
                        System.out.println("年金给付－两全险");
                        String NJLQ = NianJin + LiangQ + Grp;
                        SSRS NJLQ_ssrs = exesql.execSQL(NJLQ);
                        if (NJLQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= NJLQ_ssrs.getMaxRow(); i++)
                            {
                                nj_money2 = nj_money2 +
                                            Double.parseDouble(NJLQ_ssrs.
                                        GetText(i, 2));
                                nj_count2 = nj_count2 +
                                            Integer.parseInt(
                                            NJLQ_ssrs.GetText(i, 3));
                                System.out.println( +i + "年金两全险的金额是" +
                                        Double.
                                        parseDouble(NJLQ_ssrs.GetText(i, 2)));
                                tRiskCode = "年金给付\\两全险\\";
                            }
                            System.out.println("年金给付\\两全总金额是" + nj_money2);
                        }
                        tlisttable.setName("RISKB2");
                    }
                    //年金给付－定期 保险
                    if (k == 3)
                    {
                        String NJDQ = NianJin + DingQ + Grp;
                        System.out.println("年金－定期保险");
                        SSRS NJDQ_ssrs = exesql.execSQL(NJDQ);
                        if (NJDQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= NJDQ_ssrs.getMaxRow(); i++)
                            {
                                nj_money3 = nj_money3 +
                                            Double.parseDouble(NJDQ_ssrs.
                                        GetText(i, 2));
                                nj_count3 = nj_count3 +
                                            Integer.parseInt(
                                            NJDQ_ssrs.GetText(i, 3));
                                System.out.println(i + "年金定期保险的金额是" +
                                        Double.
                                        parseDouble(NJDQ_ssrs.GetText(i, 2)));
                                tRiskCode = "年金给付\\定期保险\\";
                            }
                            System.out.println("年金－定期的中金额是" + nj_money3);
                        }
                        tlisttable.setName("RISKB3");
                    }
                    if (k == 4)
                    {
                        //年金给付－年金 保险
                        String NJNJ = NianJin + NianJ + Grp;
                        System.out.println("满期－年金保险");
                        SSRS NJNJ_ssrs = exesql.execSQL(NJNJ);
                        if (NJNJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= NJNJ_ssrs.getMaxRow(); i++)
                            {
                                nj_money4 = nj_money4 +
                                            Double.parseDouble(NJNJ_ssrs.
                                        GetText(i, 2));
                                nj_count4 = nj_count4 +
                                            Integer.parseInt(
                                            NJNJ_ssrs.GetText(i, 3));
                                System.out.println(i + "年金－年金保险的金额是" +
                                        Double.
                                        parseDouble(NJNJ_ssrs.GetText(i, 2)));
                                tRiskCode = "年金给付\\年金保险\\";
                            }
                            System.out.println("年金－年金保险的总金额是" + nj_money4);
                        }
                        tlisttable.setName("RISKB4");
                    }
                    if (k == 5)
                    {
                        //年金－重大疾病保险
                        String NJZJ = NianJin + ZhongJ + Grp;
                        System.out.println("年金－重大疾病");
                        SSRS NJZJ_ssrs = exesql.execSQL(NJZJ);
                        if (NJZJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= NJZJ_ssrs.getMaxRow(); i++)
                            {
                                nj_money5 = nj_money5 +
                                            Double.parseDouble(NJZJ_ssrs.
                                        GetText(i, 2));
                                nj_count5 = nj_count5 +
                                            Integer.parseInt(
                                            NJZJ_ssrs.GetText(i, 3));
                                System.out.println(i + "年金－重大疾病的金额是" +
                                        Double.
                                        parseDouble(NJZJ_ssrs.GetText(i, 2)));
                                tRiskCode = "年金给付\\重大疾病保险\\";
                            }
                            System.out.println("满期－重大疾病的总金额是" + mq_money5);
                        }
                        tlisttable.setName("RISKB5");
                    }
                    //年金－意外伤害 保险
                    if (k == 6)
                    {
                        String NJYW = NianJin + YiW + Grp;
                        System.out.println("年金－意外伤害保险");
                        SSRS NJYW_ssrs = exesql.execSQL(NJYW);
                        if (NJYW_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= NJYW_ssrs.getMaxRow(); i++)
                            {
                                nj_money6 = nj_money6 +
                                            Double.parseDouble(NJYW_ssrs.
                                        GetText(i, 2));
                                nj_count6 = nj_count6 +
                                            Integer.parseInt(
                                            NJYW_ssrs.GetText(i, 3));
                                System.out.println(i + "年金－意外伤害保险的金额是" +
                                        Double.
                                        parseDouble(NJYW_ssrs.GetText(i, 2)));
                                tRiskCode = "年金给付\\意外伤害\\";
                            }
                            System.out.println("年金－意外伤害的总金额是" + nj_money6);
                        }
                        tlisttable.setName("RISKB6");
                    }
                    if (k == 7)
                    {
                        // 年金－健康 险
                        String NJJK = NianJin + JianK + Grp;
                        System.out.println("年金－健康险");
                        SSRS NJJK_ssrs = exesql.execSQL(NJJK);
                        if (NJJK_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= NJJK_ssrs.getMaxRow(); i++)
                            {
                                nj_money7 = nj_money7 +
                                            Double.parseDouble(NJJK_ssrs.
                                        GetText(i, 2));
                                nj_count7 = nj_count7 +
                                            Integer.parseInt(
                                            NJJK_ssrs.GetText(i, 3));
                                System.out.println(i + "年金－健康险的金额是" +
                                        Double.
                                        parseDouble(NJJK_ssrs.GetText(i, 2)));
                                tRiskCode = "年金给付\\健康险\\";
                            }
                            System.out.println("年金－健康险的总金额是" + nj_money7);
                        }
                        tlisttable.setName("RISKB7");
                    }
                    if (k == 8)
                    {
                        //年金－其他 险
                        String NJQT = NianJin + QiT + Grp;
                        System.out.println("年金－其他险");
                        SSRS NJQT_ssrs = exesql.execSQL(NJQT);
                        if (NJQT_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= NJQT_ssrs.getMaxRow(); i++)
                            {
                                nj_money8 = nj_money8 +
                                            Double.parseDouble(NJQT_ssrs.
                                        GetText(i, 2));
                                nj_count8 = nj_count8 +
                                            Integer.parseInt(
                                            NJQT_ssrs.GetText(i, 3));
                                System.out.println(i + "年金－其他险的金额是" +
                                        Double.
                                        parseDouble(NJQT_ssrs.GetText(i, 2)));
                                tRiskCode = "年金给付\\其他\\";
                            }
                            System.out.println("年金－其他险的总金额是" + nj_money8);
                        }
                        tlisttable.setName("RISKB8");
                    }
                    //会计的三级明细
                    String strArr[] = null;
                    //抽象成函数
                    String tXsqdName = ""; //销售渠道
                    String tXzdlName = ""; //险种大类
                    String tSxqName = ""; //首续期
                    String tDqjName = ""; //趸期交

                    for (int i = 1; i <= 4; i++) //销售渠道
                    {
                        String tXsqdSql = getXsqdSql(i);
                        System.out.println("T是" + t + "时的情况");
                        System.out.println(i + "当i 的值是" + i + "的时候");
                        if (k == 1)
                        {
                            tXsqdSql = NianJin + ZhongS + tXsqdSql;
                        }
                        if (k == 2)
                        {
                            tXsqdSql = NianJin + LiangQ + tXsqdSql;
                        }
                        if (k == 3)
                        {
                            tXsqdSql = NianJin + DingQ + tXsqdSql;
                        }
                        if (k == 4)
                        {
                            tXsqdSql = NianJin + NianJ + tXsqdSql;
                        }
                        if (k == 5)
                        {
                            tXsqdSql = NianJin + ZhongJ + tXsqdSql;
                        }
                        if (k == 6)
                        {
                            tXsqdSql = NianJin + YiW + tXsqdSql;
                        }
                        if (k == 7)
                        {
                            tXsqdSql = NianJin + JianK + tXsqdSql;
                        }
                        if (k == 8)
                        {
                            tXsqdSql = NianJin + QiT + tXsqdSql;
                        }
                        for (int j = 1; j <= 4; j++) //险种大类
                        {
                            String tXzdlSql = getXzdlSql(j);
                            tXzdlSql = tXsqdSql + tXzdlSql + Grp;
                            System.out.println("T的值是" + t);
                            System.out.println("K的值是" + k);
                            System.out.println("I的值是" + i);
                            System.out.println("J的值是" + j);
                            System.out.println(j + "查询的语句是");
                            SSRS tssrs = exesql.execSQL(tXzdlSql);
                            if (tssrs.getMaxRow() != 0)
                            {
                                for (int q = 1; q <= tssrs.MaxRow; q++)
                                {
                                    strArr = new String[7];
                                    String tRiskcode = tssrs.GetText(q, 1);
                                    LMRiskDB tLMRiskDB = new LMRiskDB();
                                    tLMRiskDB.setRiskCode(tssrs.GetText(q, 1));
                                    if (tLMRiskDB.getInfo())
                                    {
                                        strArr[0] = tRiskCode +
                                                tLMRiskDB.getRiskName();
                                    }
                                    strArr[1] = getXsqdName(i);
                                    strArr[2] = getXzdlName(j);
                                    strArr[5] = tssrs.GetText(q, 2);
                                    strArr[6] = tssrs.GetText(q, 3);
                                    tlisttable.add(strArr);
                                }
                            }
                        }
                    }
                    strArr = new String[7];
                    strArr[0] = "Risk";
                    strArr[1] = "Xsqd";
                    strArr[2] = "Xzdl";
                    strArr[3] = "Sxq";
                    strArr[4] = "Dqj";
                    strArr[5] = "Money";
                    strArr[6] = "Count";
                    xmlexport.addListTable(tlisttable, strArr);
                }
                //以上是k的结束
            } //end of t=2
            if (t == 3)
            {
                //伤残给付
                for (int k = 1; k <= 8; k++)
                {
                    ListTable tlisttable = new ListTable();
                    String tRiskCode = "";
                    //伤残给付－终身 寿险
                    if (k == 1)
                    {
                        System.out.println("伤残给付－终身寿险");
                        String SCZS = ShangCan + ZhongS + Grp;
                        SSRS SCZS_ssrs = exesql.execSQL(SCZS);
                        if (SCZS_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= SCZS_ssrs.getMaxRow(); i++)
                            {
                                sc_money1 = sc_money1 +
                                            Double.parseDouble(SCZS_ssrs.
                                        GetText(i, 2));
                                sc_count1 = sc_count1 +
                                            Integer.parseInt(
                                            SCZS_ssrs.GetText(i, 3));
                                System.out.println( +i + "伤残金额是" +
                                        Double.
                                        parseDouble(SCZS_ssrs.GetText(i, 2)));
                                tRiskCode = "伤残给付\\终身寿险\\";
                            }
                            System.out.println("伤残终身寿险总金额是" + sc_money1);
                        }
                        tlisttable.setName("RISKC1");
                    }
                    //伤残给付－两全 险
                    if (k == 2)
                    {
                        System.out.println("伤残给付－两全险");
                        String SCLQ = ShangCan + LiangQ + Grp;
                        SSRS SCLQ_ssrs = exesql.execSQL(SCLQ);
                        if (SCLQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= SCLQ_ssrs.getMaxRow(); i++)
                            {
                                sc_money2 = sc_money2 +
                                            Double.parseDouble(SCLQ_ssrs.
                                        GetText(i, 2));
                                sc_count2 = sc_count2 +
                                            Integer.parseInt(
                                            SCLQ_ssrs.GetText(i, 3));
                                System.out.println( +i + "伤残两全险的金额是" +
                                        Double.
                                        parseDouble(SCLQ_ssrs.GetText(i, 2)));
                                tRiskCode = "伤残给付\\两全险\\";
                            }
                            System.out.println("伤残给付\\两全总金额是" + sc_money2);
                        }
                        tlisttable.setName("RISKC2");
                    }
                    //伤残给付－定期 保险
                    if (k == 3)
                    {
                        String SCDQ = ShangCan + DingQ + Grp;
                        System.out.println("伤残－定期保险");
                        SSRS SCDQ_ssrs = exesql.execSQL(SCDQ);
                        if (SCDQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= SCDQ_ssrs.getMaxRow(); i++)
                            {
                                sc_money3 = sc_money3 +
                                            Double.parseDouble(SCDQ_ssrs.
                                        GetText(i, 2));
                                sc_count3 = sc_count3 +
                                            Integer.parseInt(
                                            SCDQ_ssrs.GetText(i, 3));
                                System.out.println(i + "伤残定期保险的金额是" +
                                        Double.
                                        parseDouble(SCDQ_ssrs.GetText(i, 2)));
                                tRiskCode = "伤残给付\\定期保险\\";
                            }
                            System.out.println("伤残－定期的中金额是" + sc_money3);
                        }
                        tlisttable.setName("RISKC3");
                    }
                    if (k == 4)
                    {
                        //伤残给付－年金 保险
                        String SCNJ = ShangCan + NianJ + Grp;
                        System.out.println("伤残－年金保险");
                        SSRS SCNJ_ssrs = exesql.execSQL(SCNJ);
                        if (SCNJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= SCNJ_ssrs.getMaxRow(); i++)
                            {
                                sc_money4 = sc_money4 +
                                            Double.parseDouble(SCNJ_ssrs.
                                        GetText(i, 2));
                                sc_count4 = sc_count4 +
                                            Integer.parseInt(
                                            SCNJ_ssrs.GetText(i, 3));
                                System.out.println(i + "伤残－年金保险的金额是" +
                                        Double.
                                        parseDouble(SCNJ_ssrs.GetText(i, 2)));
                                tRiskCode = "伤残给付\\年金保险\\";
                            }
                            System.out.println("伤残－年金保险的总金额是" + sc_money4);
                        }
                        tlisttable.setName("RISKC4");
                    }
                    if (k == 5)
                    {
                        //伤残－重大疾病保险
                        String SCZJ = ShangCan + ZhongJ + Grp;
                        System.out.println("伤残－重大疾病");
                        SSRS SCZJ_ssrs = exesql.execSQL(SCZJ);
                        if (SCZJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= SCZJ_ssrs.getMaxRow(); i++)
                            {
                                sc_money5 = sc_money5 +
                                            Double.parseDouble(SCZJ_ssrs.
                                        GetText(i, 2));
                                sc_count5 = sc_count5 +
                                            Integer.parseInt(
                                            SCZJ_ssrs.GetText(i, 3));
                                System.out.println(i + "年金－重大疾病的金额是" +
                                        Double.
                                        parseDouble(SCZJ_ssrs.GetText(i, 2)));
                                tRiskCode = "伤残给付\\重大疾病保险\\";
                            }
                            System.out.println("伤残－重大疾病的总金额是" + sc_money5);
                        }
                        tlisttable.setName("RISKC5");
                    }
                    //伤残－意外伤害 保险
                    if (k == 6)
                    {
                        String SCYW = ShangCan + YiW + Grp;
                        System.out.println("伤残－意外伤害保险");
                        SSRS SCYW_ssrs = exesql.execSQL(SCYW);
                        if (SCYW_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= SCYW_ssrs.getMaxRow(); i++)
                            {
                                sc_money6 = sc_money6 +
                                            Double.parseDouble(SCYW_ssrs.
                                        GetText(i, 2));
                                sc_count6 = sc_count6 +
                                            Integer.parseInt(
                                            SCYW_ssrs.GetText(i, 3));
                                System.out.println(i + "伤残－意外伤害保险的金额是" +
                                        Double.
                                        parseDouble(SCYW_ssrs.GetText(i, 2)));
                                tRiskCode = "伤残给付\\意外伤害\\";
                            }
                            System.out.println("伤残－意外伤害的总金额是" + sc_money6);
                        }
                        tlisttable.setName("RISKC6");
                    }
                    if (k == 7)
                    {
                        // 伤残－健康 险
                        String SCJK = ShangCan + JianK + Grp;
                        System.out.println("伤残－健康险");
                        SSRS SCJK_ssrs = exesql.execSQL(SCJK);
                        if (SCJK_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= SCJK_ssrs.getMaxRow(); i++)
                            {
                                sc_money7 = sc_money7 +
                                            Double.parseDouble(SCJK_ssrs.
                                        GetText(i, 2));
                                sc_count7 = sc_count7 +
                                            Integer.parseInt(
                                            SCJK_ssrs.GetText(i, 3));
                                System.out.println(i + "伤残－健康险的金额是" +
                                        Double.
                                        parseDouble(SCJK_ssrs.GetText(i, 2)));
                                tRiskCode = "伤残给付\\健康险\\";
                            }
                            System.out.println("伤残－健康险的总金额是" + sc_money7);
                        }
                        tlisttable.setName("RISKC7");
                    }
                    if (k == 8)
                    {
                        //伤残－其他 险
                        String SCQT = ShangCan + QiT + Grp;
                        System.out.println("伤残－其他险");
                        SSRS SCQT_ssrs = exesql.execSQL(SCQT);
                        if (SCQT_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= SCQT_ssrs.getMaxRow(); i++)
                            {
                                sc_money8 = sc_money8 +
                                            Double.parseDouble(SCQT_ssrs.
                                        GetText(i, 2));
                                sc_count8 = sc_count8 +
                                            Integer.parseInt(
                                            SCQT_ssrs.GetText(i, 3));
                                System.out.println(i + "伤残－其他险的金额是" +
                                        Double.
                                        parseDouble(SCQT_ssrs.GetText(i, 2)));
                                tRiskCode = "伤残给付\\其他\\";
                            }
                            System.out.println("伤残－其他险的总金额是" + sc_money8);
                        }
                        tlisttable.setName("RISKC8");
                    }
                    //会计的三级明细
                    String strArr[] = null;
                    //抽象成函数
                    String tXsqdName = ""; //销售渠道
                    String tXzdlName = ""; //险种大类
                    String tSxqName = ""; //首续期
                    String tDqjName = ""; //趸期交

                    for (int i = 1; i <= 4; i++) //销售渠道
                    {
                        String tXsqdSql = getXsqdSql(i);
                        System.out.println("T是" + t + "时的情况");
                        System.out.println(i + "当i 的值是" + i + "的时候");
                        if (k == 1)
                        {
                            tXsqdSql = ShangCan + ZhongS + tXsqdSql;
                        }
                        if (k == 2)
                        {
                            tXsqdSql = ShangCan + LiangQ + tXsqdSql;
                        }
                        if (k == 3)
                        {
                            tXsqdSql = ShangCan + DingQ + tXsqdSql;
                        }
                        if (k == 4)
                        {
                            tXsqdSql = ShangCan + NianJ + tXsqdSql;
                        }
                        if (k == 5)
                        {
                            tXsqdSql = ShangCan + ZhongJ + tXsqdSql;
                        }
                        if (k == 6)
                        {
                            tXsqdSql = ShangCan + YiW + tXsqdSql;
                        }
                        if (k == 7)
                        {
                            tXsqdSql = ShangCan + JianK + tXsqdSql;
                        }
                        if (k == 8)
                        {
                            tXsqdSql = ShangCan + QiT + tXsqdSql;
                        }
                        for (int j = 1; j <= 4; j++) //险种大类
                        {
                            String tXzdlSql = getXzdlSql(j);
                            tXzdlSql = tXsqdSql + tXzdlSql + Grp;
                            System.out.println("T的值是" + t);
                            System.out.println("K的值是" + k);
                            System.out.println("I的值是" + i);
                            System.out.println("J的值是" + j);
                            System.out.println(j + "查询的语句是");
                            SSRS tssrs = exesql.execSQL(tXzdlSql);
                            if (tssrs.getMaxRow() != 0)
                            {
                                for (int q = 1; q <= tssrs.MaxRow; q++)
                                {
                                    strArr = new String[7];
                                    String tRiskcode = tssrs.GetText(q, 1);
                                    LMRiskDB tLMRiskDB = new LMRiskDB();
                                    tLMRiskDB.setRiskCode(tssrs.GetText(q, 1));
                                    if (tLMRiskDB.getInfo())
                                    {
                                        strArr[0] = tRiskCode +
                                                tLMRiskDB.getRiskName();
                                    }
                                    strArr[1] = getXsqdName(i);
                                    strArr[2] = getXzdlName(j);
                                    strArr[5] = tssrs.GetText(q, 2);
                                    strArr[6] = tssrs.GetText(q, 3);

                                    if (!(strArr[6] == null) ||
                                        (strArr[6].equals("")))
                                    {
                                        // tCount = tCount+Integer.parseInt(strArr[6]);
                                    }
                                    //  SumMoney = SumMoney + Double.parseDouble(strArr[5]);
                                    tlisttable.add(strArr);
                                }
                            }
                        }
                    }
                    strArr = new String[7];
                    strArr[0] = "Risk";
                    strArr[1] = "Xsqd";
                    strArr[2] = "Xzdl";
                    strArr[3] = "Sxq";
                    strArr[4] = "Dqj";
                    strArr[5] = "Money";
                    strArr[6] = "Count";
                    xmlexport.addListTable(tlisttable, strArr);
                }
                //以上是k的结束
            } //end of t=3

            if (t == 4)
            {
                //医疗给付
                for (int k = 1; k <= 8; k++)
                {
                    ListTable tlisttable = new ListTable();
                    String tRiskCode = "";
                    //医疗给付－终身 寿险
                    if (k == 1)
                    {
                        System.out.println("医疗给付－终身寿险");
                        String YLZS = YiLiao + ZhongS + Grp;
                        SSRS YLZS_ssrs = exesql.execSQL(YLZS);
                        if (YLZS_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= YLZS_ssrs.getMaxRow(); i++)
                            {
                                yl_money1 = yl_money1 +
                                            Double.parseDouble(YLZS_ssrs.
                                        GetText(i, 2));
                                yl_count1 = yl_count1 +
                                            Integer.parseInt(
                                            YLZS_ssrs.GetText(i, 3));
                                System.out.println( +i + "医疗金额是" +
                                        Double.
                                        parseDouble(YLZS_ssrs.GetText(i, 2)));
                                tRiskCode = "医疗给付\\终身寿险\\";
                            }
                            System.out.println("医疗终身寿险总金额是" + yl_money1);
                        }
                        tlisttable.setName("RISKD1");
                    }
                    //医疗给付－两全 险
                    if (k == 2)
                    {
                        System.out.println("医疗给付－两全险");
                        String YLLQ = YiLiao + LiangQ + Grp;
                        SSRS YLLQ_ssrs = exesql.execSQL(YLLQ);
                        if (YLLQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= YLLQ_ssrs.getMaxRow(); i++)
                            {
                                yl_money2 = yl_money2 +
                                            Double.parseDouble(YLLQ_ssrs.
                                        GetText(i, 2));
                                yl_count2 = yl_count2 +
                                            Integer.parseInt(
                                            YLLQ_ssrs.GetText(i, 3));
                                System.out.println( +i + "医疗两全险的金额是" +
                                        Double.
                                        parseDouble(YLLQ_ssrs.GetText(i, 2)));
                                tRiskCode = "医疗给付\\两全险\\";
                            }
                            System.out.println("医疗给付\\两全总金额是" + yl_money2);
                        }
                        tlisttable.setName("RISKD2");
                    }
                    //医疗给付－定期 保险
                    if (k == 3)
                    {
                        String YLDQ = YiLiao + DingQ + Grp;
                        System.out.println("医疗－定期保险");
                        SSRS YLDQ_ssrs = exesql.execSQL(YLDQ);
                        if (YLDQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= YLDQ_ssrs.getMaxRow(); i++)
                            {
                                yl_money3 = yl_money3 +
                                            Double.parseDouble(YLDQ_ssrs.
                                        GetText(i, 2));
                                yl_count3 = yl_count3 +
                                            Integer.parseInt(
                                            YLDQ_ssrs.GetText(i, 3));
                                System.out.println(i + "医疗定期保险的金额是" +
                                        Double.
                                        parseDouble(YLDQ_ssrs.GetText(i, 2)));
                                tRiskCode = "医疗给付\\定期保险\\";
                            }
                            System.out.println("医疗－定期的中金额是" + yl_money3);
                        }
                        tlisttable.setName("RISKD3");
                    }
                    if (k == 4)
                    {
                        //医疗给付－年金 保险
                        String YLNJ = YiLiao + NianJ + Grp;
                        System.out.println("医疗－年金保险");
                        SSRS YLNJ_ssrs = exesql.execSQL(YLNJ);
                        if (YLNJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= YLNJ_ssrs.getMaxRow(); i++)
                            {
                                yl_money4 = yl_money4 +
                                            Double.parseDouble(YLNJ_ssrs.
                                        GetText(i, 2));
                                yl_count4 = yl_count4 +
                                            Integer.parseInt(
                                            YLNJ_ssrs.GetText(i, 3));
                                System.out.println(i + "医疗－年金保险的金额是" +
                                        Double.
                                        parseDouble(YLNJ_ssrs.GetText(i, 2)));
                                tRiskCode = "医疗给付\\年金保险\\";
                            }
                            System.out.println("医疗－年金保险的总金额是" + yl_money4);
                        }
                        tlisttable.setName("RISKD4");
                    }
                    if (k == 5)
                    {
                        //医疗－重大疾病保险
                        String YLZJ = YiLiao + ZhongJ + Grp;
                        System.out.println("医疗－重大疾病");
                        SSRS YLZJ_ssrs = exesql.execSQL(YLZJ);
                        if (YLZJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= YLZJ_ssrs.getMaxRow(); i++)
                            {
                                yl_money5 = yl_money5 +
                                            Double.parseDouble(YLZJ_ssrs.
                                        GetText(i, 2));
                                yl_count5 = yl_count5 +
                                            Integer.parseInt(
                                            YLZJ_ssrs.GetText(i, 3));
                                System.out.println(i + "医疗－重大疾病的金额是" +
                                        Double.
                                        parseDouble(YLZJ_ssrs.GetText(i, 2)));
                                tRiskCode = "医疗给付\\重大疾病保险\\";
                            }
                            System.out.println("医疗－重大疾病的总金额是" + yl_money5);
                        }
                        tlisttable.setName("RISKD5");
                    }
                    //医疗－意外伤害 保险
                    if (k == 6)
                    {
                        String YLYW = YiLiao + YiW + Grp;
                        System.out.println("医疗－意外伤害保险");
                        SSRS YLYW_ssrs = exesql.execSQL(YLYW);
                        if (YLYW_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= YLYW_ssrs.getMaxRow(); i++)
                            {
                                yl_money6 = yl_money6 +
                                            Double.parseDouble(YLYW_ssrs.
                                        GetText(i, 2));
                                yl_count6 = yl_count6 +
                                            Integer.parseInt(
                                            YLYW_ssrs.GetText(i, 3));
                                System.out.println(i + "医疗－意外伤害保险的金额是" +
                                        Double.
                                        parseDouble(YLYW_ssrs.GetText(i, 2)));
                                tRiskCode = "医疗给付\\意外伤害\\";
                            }
                            System.out.println("医疗－意外伤害的总金额是" + yl_money6);
                        }
                        tlisttable.setName("RISKD6");
                    }
                    if (k == 7)
                    {
                        // 医疗－健康 险
                        String YLJK = YiLiao + JianK + Grp;
                        System.out.println("医疗－健康险");
                        SSRS YLJK_ssrs = exesql.execSQL(YLJK);
                        if (YLJK_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= YLJK_ssrs.getMaxRow(); i++)
                            {
                                yl_money7 = yl_money7 +
                                            Double.parseDouble(YLJK_ssrs.
                                        GetText(i, 2));
                                yl_count7 = yl_count7 +
                                            Integer.parseInt(
                                            YLJK_ssrs.GetText(i, 3));
                                System.out.println(i + "医疗－健康险的金额是" +
                                        Double.
                                        parseDouble(YLJK_ssrs.GetText(i, 2)));
                                tRiskCode = "医疗给付\\健康险\\";
                            }
                            System.out.println("医疗－健康险的总金额是" + yl_money7);
                        }
                        tlisttable.setName("RISKD7");
                    }
                    if (k == 8)
                    {
                        //医疗－其他 险
                        String YLQT = YiLiao + QiT + Grp;
                        System.out.println("医疗－其他险");
                        SSRS YLQT_ssrs = exesql.execSQL(YLQT);
                        if (YLQT_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= YLQT_ssrs.getMaxRow(); i++)
                            {
                                yl_money8 = yl_money8 +
                                            Double.parseDouble(YLQT_ssrs.
                                        GetText(i, 2));
                                yl_count8 = yl_count8 +
                                            Integer.parseInt(
                                            YLQT_ssrs.GetText(i, 3));
                                System.out.println(i + "医疗－其他险的金额是" +
                                        Double.
                                        parseDouble(YLQT_ssrs.GetText(i, 2)));
                                tRiskCode = "医疗给付\\其他\\";
                            }
                            System.out.println("医疗－其他险的总金额是" + yl_money8);
                        }
                        tlisttable.setName("RISKD8");
                    }
                    //会计的三级明细
                    String strArr[] = null;
                    //抽象成函数
                    String tXsqdName = ""; //销售渠道
                    String tXzdlName = ""; //险种大类
                    String tSxqName = ""; //首续期
                    String tDqjName = ""; //趸期交

                    for (int i = 1; i <= 4; i++) //销售渠道
                    {
                        String tXsqdSql = getXsqdSql(i);
                        System.out.println("T是" + t + "时的情况");
                        System.out.println(i + "当i 的值是" + i + "的时候");
                        if (k == 1)
                        {
                            tXsqdSql = YiLiao + ZhongS + tXsqdSql;
                        }
                        if (k == 2)
                        {
                            tXsqdSql = YiLiao + LiangQ + tXsqdSql;
                        }
                        if (k == 3)
                        {
                            tXsqdSql = YiLiao + DingQ + tXsqdSql;
                        }
                        if (k == 4)
                        {
                            tXsqdSql = YiLiao + NianJ + tXsqdSql;
                        }
                        if (k == 5)
                        {
                            tXsqdSql = YiLiao + ZhongJ + tXsqdSql;
                        }
                        if (k == 6)
                        {
                            tXsqdSql = YiLiao + YiW + tXsqdSql;
                        }
                        if (k == 7)
                        {
                            tXsqdSql = YiLiao + JianK + tXsqdSql;
                        }
                        if (k == 8)
                        {
                            tXsqdSql = YiLiao + QiT + tXsqdSql;
                        }
                        for (int j = 1; j <= 4; j++) //险种大类
                        {
                            String tXzdlSql = getXzdlSql(j);
                            tXzdlSql = tXsqdSql + tXzdlSql + Grp;
                            System.out.println("T的值是" + t);
                            System.out.println("K的值是" + k);
                            System.out.println("I的值是" + i);
                            System.out.println("J的值是" + j);
                            System.out.println(j + "查询的语句是");
                            SSRS tssrs = exesql.execSQL(tXzdlSql);
                            if (tssrs.getMaxRow() != 0)
                            {
                                for (int q = 1; q <= tssrs.MaxRow; q++)
                                {
                                    strArr = new String[7];
                                    String tRiskcode = tssrs.GetText(q, 1);
                                    LMRiskDB tLMRiskDB = new LMRiskDB();
                                    tLMRiskDB.setRiskCode(tssrs.GetText(q, 1));
                                    if (tLMRiskDB.getInfo())
                                    {
                                        strArr[0] = tRiskCode +
                                                tLMRiskDB.getRiskName();
                                    }
                                    strArr[1] = getXsqdName(i);
                                    strArr[2] = getXzdlName(j);
                                    strArr[5] = tssrs.GetText(q, 2);
                                    strArr[6] = tssrs.GetText(q, 3);

                                    if (!(strArr[6] == null) ||
                                        (strArr[6].equals("")))
                                    {
                                        // tCount = tCount+Integer.parseInt(strArr[6]);
                                    }
                                    //  SumMoney = SumMoney + Double.parseDouble(strArr[5]);
                                    tlisttable.add(strArr);
                                }
                            }
                        }
                    }
                    strArr = new String[7];
                    strArr[0] = "Risk";
                    strArr[1] = "Xsqd";
                    strArr[2] = "Xzdl";
                    strArr[3] = "Sxq";
                    strArr[4] = "Dqj";
                    strArr[5] = "Money";
                    strArr[6] = "Count";
                    xmlexport.addListTable(tlisttable, strArr);
                }
                //以上是k的结束
            } //end of t=4

            //对退保金的查询
            if (t == 5)
            {
                //退保金给付
                for (int k = 1; k <= 8; k++)
                {
                    ListTable tlisttable = new ListTable();
                    String tRiskCode = "";
                    //退保金－终身 寿险
                    if (k == 1)
                    {
                        System.out.println("退保金－终身寿险");
                        String TBZS = TuiBao + ZhongS + Grp;
                        SSRS TBZS_ssrs = exesql.execSQL(TBZS);
                        if (TBZS_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= TBZS_ssrs.getMaxRow(); i++)
                            {
                                tb_money1 = tb_money1 +
                                            Double.parseDouble(TBZS_ssrs.
                                        GetText(i, 2));
                                tb_count1 = tb_count1 +
                                            Integer.parseInt(
                                            TBZS_ssrs.GetText(i, 3));
                                System.out.println( +i + "退保金金额是" +
                                        Double.
                                        parseDouble(TBZS_ssrs.GetText(i, 2)));
                                tRiskCode = "退保金给付\\终身寿险\\";
                            }
                            System.out.println("退保金总金额是" + tb_money1);
                        }
                        tlisttable.setName("RISKE1");
                    }
                    //退保金－两全 险
                    if (k == 2)
                    {
                        System.out.println("退保金－两全险");
                        String TBLQ = TuiBao + LiangQ + Grp;
                        SSRS TBLQ_ssrs = exesql.execSQL(TBLQ);
                        if (TBLQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= TBLQ_ssrs.getMaxRow(); i++)
                            {
                                tb_money2 = tb_money2 +
                                            Double.parseDouble(TBLQ_ssrs.
                                        GetText(i, 2));
                                tb_count2 = tb_count2 +
                                            Integer.parseInt(
                                            TBLQ_ssrs.GetText(i, 3));
                                System.out.println( +i + "退保金两全险的金额是" +
                                        Double.
                                        parseDouble(TBLQ_ssrs.GetText(i, 2)));
                                tRiskCode = "退保金给付\\两全险\\";
                            }
                            System.out.println("退保金两全总金额是" + tb_money2);
                        }
                        tlisttable.setName("RISKE2");
                    }
                    //退保金－定期 保险
                    if (k == 3)
                    {
                        String TBDQ = TuiBao + DingQ + Grp;
                        System.out.println("退保金－定期保险");
                        SSRS TBDQ_ssrs = exesql.execSQL(TBDQ);
                        if (TBDQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= TBDQ_ssrs.getMaxRow(); i++)
                            {
                                tb_money3 = tb_money3 +
                                            Double.parseDouble(TBDQ_ssrs.
                                        GetText(i, 2));
                                tb_count3 = tb_count3 +
                                            Integer.parseInt(
                                            TBDQ_ssrs.GetText(i, 3));
                                System.out.println(i + "退保金定期保险的金额是" +
                                        Double.
                                        parseDouble(TBDQ_ssrs.GetText(i, 2)));
                                tRiskCode = "退保金给付\\定期保险\\";
                            }
                            System.out.println("退保金－定期的中金额是" + tb_money3);
                        }
                        tlisttable.setName("RISKE3");
                    }
                    if (k == 4)
                    {
                        //退保金－年金 保险
                        String TBNJ = TuiBao + NianJ + Grp;
                        System.out.println("退保金－年金保险");
                        SSRS TBNJ_ssrs = exesql.execSQL(TBNJ);
                        if (TBNJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= TBNJ_ssrs.getMaxRow(); i++)
                            {
                                tb_money4 = tb_money4 +
                                            Double.parseDouble(TBNJ_ssrs.
                                        GetText(i, 2));
                                tb_count4 = tb_count4 +
                                            Integer.parseInt(
                                            TBNJ_ssrs.GetText(i, 3));
                                System.out.println(i + "退保金－年金保险的金额是" +
                                        Double.
                                        parseDouble(TBNJ_ssrs.GetText(i, 2)));
                                tRiskCode = "退保金给付\\年金保险\\";
                            }
                            System.out.println("退保金－年金保险的总金额是" + tb_money4);
                        }
                        tlisttable.setName("RISKE4");
                    }
                    if (k == 5)
                    {
                        //退保金－重大疾病保险
                        String TBZJ = TuiBao + ZhongJ + Grp;
                        System.out.println("退保金－重大疾病");
                        SSRS TBZJ_ssrs = exesql.execSQL(TBZJ);
                        if (TBZJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= TBZJ_ssrs.getMaxRow(); i++)
                            {
                                tb_money5 = tb_money5 +
                                            Double.parseDouble(TBZJ_ssrs.
                                        GetText(i, 2));
                                tb_count5 = tb_count5 +
                                            Integer.parseInt(
                                            TBZJ_ssrs.GetText(i, 3));
                                System.out.println(i + "退保金－重大疾病的金额是" +
                                        Double.
                                        parseDouble(TBZJ_ssrs.GetText(i, 2)));
                                tRiskCode = "退保金给付\\重大疾病保险\\";
                            }
                            System.out.println("退保金－重大疾病的总金额是" + tb_money5);
                        }
                        tlisttable.setName("RISKE5");
                    }
                    //退保金－意外伤害 保险
                    if (k == 6)
                    {
                        String TBYW = TuiBao + YiW + Grp;
                        System.out.println("退保金－意外伤害保险");
                        SSRS TBYW_ssrs = exesql.execSQL(TBYW);
                        if (TBYW_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= TBYW_ssrs.getMaxRow(); i++)
                            {
                                tb_money6 = tb_money6 +
                                            Double.parseDouble(TBYW_ssrs.
                                        GetText(i, 2));
                                tb_count6 = tb_count6 +
                                            Integer.parseInt(
                                            TBYW_ssrs.GetText(i, 3));
                                System.out.println(i + "退保金－意外伤害保险的金额是" +
                                        Double.parseDouble(TBYW_ssrs.GetText(i,
                                        2)));
                                tRiskCode = "退保金给付\\意外伤害\\";
                            }
                            System.out.println("退保金－意外伤害的总金额是" + tb_money6);
                        }
                        tlisttable.setName("RISKE6");
                    }
                    if (k == 7)
                    {
                        //退保金－健康 险
                        String TBJK = TuiBao + JianK + Grp;
                        System.out.println("退保金－健康险");
                        SSRS TBJK_ssrs = exesql.execSQL(TBJK);
                        if (TBJK_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= TBJK_ssrs.getMaxRow(); i++)
                            {
                                tb_money7 = tb_money7 +
                                            Double.parseDouble(TBJK_ssrs.
                                        GetText(i, 2));
                                tb_count7 = tb_count7 +
                                            Integer.parseInt(
                                            TBJK_ssrs.GetText(i, 3));
                                System.out.println(i + "退保金－健康险的金额是" +
                                        Double.
                                        parseDouble(TBJK_ssrs.GetText(i, 2)));
                                tRiskCode = "退保金给付\\健康险\\";
                            }
                            System.out.println("退保金－健康险的总金额是" + tb_money7);
                        }
                        tlisttable.setName("RISKE7");
                    }
                    if (k == 8)
                    {
                        //退保金－其他 险
                        String TBQT = TuiBao + QiT + Grp;
                        System.out.println("退保金－其他险");
                        SSRS TBQT_ssrs = exesql.execSQL(TBQT);
                        if (TBQT_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= TBQT_ssrs.getMaxRow(); i++)
                            {
                                tb_money8 = tb_money8 +
                                            Double.parseDouble(TBQT_ssrs.
                                        GetText(i, 2));
                                tb_count8 = tb_count8 +
                                            Integer.parseInt(
                                            TBQT_ssrs.GetText(i, 3));
                                System.out.println(i + "退保金－其他险的金额是" +
                                        Double.
                                        parseDouble(TBQT_ssrs.GetText(i, 2)));
                                tRiskCode = "退保金给付\\其他\\";
                            }
                            System.out.println("退保金－其他险的总金额是" + tb_money8);
                        }
                        tlisttable.setName("RISKE8");
                    }
                    //会计的三级明细
                    String strArr[] = null;
                    //抽象成函数
                    String tXsqdName = ""; //销售渠道
                    String tXzdlName = ""; //险种大类
                    String tSxqName = ""; //首续期
                    String tDqjName = ""; //趸期交

                    for (int i = 1; i <= 4; i++) //销售渠道
                    {
                        String tXsqdSql = getXsqdSql(i);
                        System.out.println(i + "当i 的值是" + i + "的时候");
                        if (k == 1)
                        {
                            tXsqdSql = TuiBao + ZhongS + tXsqdSql;
                        }
                        if (k == 2)
                        {
                            tXsqdSql = TuiBao + LiangQ + tXsqdSql;
                        }
                        if (k == 3)
                        {
                            tXsqdSql = TuiBao + DingQ + tXsqdSql;
                        }
                        if (k == 4)
                        {
                            tXsqdSql = TuiBao + NianJ + tXsqdSql;
                        }
                        if (k == 5)
                        {
                            tXsqdSql = TuiBao + ZhongJ + tXsqdSql;
                        }
                        if (k == 6)
                        {
                            tXsqdSql = TuiBao + YiW + tXsqdSql;
                        }
                        if (k == 7)
                        {
                            tXsqdSql = TuiBao + JianK + tXsqdSql;
                        }
                        if (k == 8)
                        {
                            tXsqdSql = TuiBao + QiT + tXsqdSql;
                        }
                        for (int j = 1; j <= 4; j++) //险种大类
                        {
                            String tXzdlSql = getXzdlSql(j);
                            tXzdlSql = tXsqdSql + tXzdlSql + Grp;
                            System.out.println("T的值是" + t);
                            System.out.println("K的值是" + k);
                            System.out.println("I的值是" + i);
                            System.out.println("J的值是" + j);
                            System.out.println(j + "查询的语句是");
                            SSRS tssrs = exesql.execSQL(tXzdlSql);
                            if (tssrs.getMaxRow() != 0)
                            {
                                for (int q = 1; q <= tssrs.MaxRow; q++)
                                {
                                    strArr = new String[7];
                                    String tRiskcode = tssrs.GetText(q, 1);
                                    LMRiskDB tLMRiskDB = new LMRiskDB();
                                    tLMRiskDB.setRiskCode(tssrs.GetText(q, 1));
                                    if (tLMRiskDB.getInfo())
                                    {
                                        strArr[0] = tRiskCode +
                                                tLMRiskDB.getRiskName();
                                    }
                                    strArr[1] = getXsqdName(i);
                                    strArr[2] = getXzdlName(j);
                                    strArr[5] = tssrs.GetText(q, 2);
                                    strArr[6] = tssrs.GetText(q, 3);

                                    if (!(strArr[6] == null) ||
                                        (strArr[6].equals("")))
                                    {
                                        // tCount = tCount+Integer.parseInt(strArr[6]);
                                    }
                                    //  SumMoney = SumMoney + Double.parseDouble(strArr[5]);
                                    tlisttable.add(strArr);
                                }
                            }
                        }
                    }
                    strArr = new String[7];
                    strArr[0] = "Risk";
                    strArr[1] = "Xsqd";
                    strArr[2] = "Xzdl";
                    strArr[3] = "Sxq";
                    strArr[4] = "Dqj";
                    strArr[5] = "Money";
                    strArr[6] = "Count";
                    xmlexport.addListTable(tlisttable, strArr);
                }
                //以上是k的结束
            } //end of t==5

            //死亡给付的查询
            if (t == 6)
            {
                //死亡给付
                for (int k = 1; k <= 8; k++)
                {
                    ListTable tlisttable = new ListTable();
                    String tRiskCode = "";
                    //死亡－终身 寿险
                    if (k == 1)
                    {
                        System.out.println("死亡－终身寿险");
                        String SWZS = SiWang + ZhongS + Grp;
                        SSRS SWZS_ssrs = exesql.execSQL(SWZS);
                        if (SWZS_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= SWZS_ssrs.getMaxRow(); i++)
                            {
                                sw_money1 = sw_money1 +
                                            Double.parseDouble(SWZS_ssrs.
                                        GetText(i, 2));
                                sw_count1 = sw_count1 +
                                            Integer.parseInt(
                                            SWZS_ssrs.GetText(i, 3));
                                System.out.println( +i + "死亡金额是" +
                                        Double.
                                        parseDouble(SWZS_ssrs.GetText(i, 2)));
                                tRiskCode = "死亡给付\\终身寿险\\";
                            }
                            System.out.println("死亡总金额是" + sw_money1);
                        }
                        tlisttable.setName("RISKF1");
                    }
                    //死亡－两全 险
                    if (k == 2)
                    {
                        System.out.println("死亡－两全险");
                        String SWLQ = SiWang + LiangQ + Grp;
                        SSRS SWLQ_ssrs = exesql.execSQL(SWLQ);
                        if (SWLQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= SWLQ_ssrs.getMaxRow(); i++)
                            {
                                sw_money2 = sw_money2 +
                                            Double.parseDouble(SWLQ_ssrs.
                                        GetText(i, 2));
                                sw_count2 = sw_count2 +
                                            Integer.parseInt(
                                            SWLQ_ssrs.GetText(i, 3));
                                System.out.println( +i + "死亡两全险的金额是" +
                                        Double.
                                        parseDouble(SWLQ_ssrs.GetText(i, 2)));
                                tRiskCode = "死亡给付\\两全险\\";
                            }
                            System.out.println("死亡两全总金额是" + sw_money2);
                        }
                        tlisttable.setName("RISKE2");
                    }
                    //死亡－定期 保险
                    if (k == 3)
                    {
                        String SWDQ = SiWang + DingQ + Grp;
                        System.out.println("死亡－定期保险");
                        SSRS SWDQ_ssrs = exesql.execSQL(SWDQ);
                        if (SWDQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= SWDQ_ssrs.getMaxRow(); i++)
                            {
                                sw_money3 = sw_money3 +
                                            Double.parseDouble(SWDQ_ssrs.
                                        GetText(i, 2));
                                sw_count3 = sw_count3 +
                                            Integer.parseInt(
                                            SWDQ_ssrs.GetText(i, 3));
                                System.out.println(i + "死亡定期保险的金额是" +
                                        Double.
                                        parseDouble(SWDQ_ssrs.GetText(i, 2)));
                                tRiskCode = "死亡给付\\定期保险\\";
                            }
                            System.out.println("死亡－定期的中金额是" + sw_money3);
                        }
                        tlisttable.setName("RISKF3");
                    }
                    if (k == 4)
                    {
                        //死亡－年金 保险
                        String SWNJ = SiWang + NianJ + Grp;
                        System.out.println("死亡－年金保险");
                        SSRS SWNJ_ssrs = exesql.execSQL(SWNJ);
                        if (SWNJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= SWNJ_ssrs.getMaxRow(); i++)
                            {
                                sw_money4 = sw_money4 +
                                            Double.parseDouble(SWNJ_ssrs.
                                        GetText(i, 2));
                                sw_count4 = sw_count4 +
                                            Integer.parseInt(
                                            SWNJ_ssrs.GetText(i, 3));
                                System.out.println(i + "死亡－年金保险的金额是" +
                                        Double.
                                        parseDouble(SWNJ_ssrs.GetText(i, 2)));
                                tRiskCode = "死亡给付\\年金保险\\";
                            }
                            System.out.println("死亡－年金保险的总金额是" + sw_money4);
                        }
                        tlisttable.setName("RISKF4");
                    }
                    if (k == 5)
                    {
                        //死亡－重大疾病保险
                        String SWZJ = SiWang + ZhongJ + Grp;
                        System.out.println("死亡－重大疾病");
                        SSRS SWZJ_ssrs = exesql.execSQL(SWZJ);
                        if (SWZJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= SWZJ_ssrs.getMaxRow(); i++)
                            {
                                sw_money5 = sw_money5 +
                                            Double.parseDouble(SWZJ_ssrs.
                                        GetText(i, 2));
                                sw_count5 = sw_count5 +
                                            Integer.parseInt(
                                            SWZJ_ssrs.GetText(i, 3));
                                System.out.println(i + "死亡－重大疾病的金额是" +
                                        Double.
                                        parseDouble(SWZJ_ssrs.GetText(i, 2)));
                                tRiskCode = "死亡给付\\重大疾病保险\\";
                            }
                            System.out.println("死亡－重大疾病的总金额是" + sw_money5);
                        }
                        tlisttable.setName("RISKF5");
                    }
                    //死亡－意外伤害 保险
                    if (k == 6)
                    {
                        String SWYW = SiWang + YiW + Grp;
                        System.out.println("死亡－意外伤害保险");
                        SSRS SWYW_ssrs = exesql.execSQL(SWYW);
                        if (SWYW_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= SWYW_ssrs.getMaxRow(); i++)
                            {
                                sw_money6 = sw_money6 +
                                            Double.parseDouble(SWYW_ssrs.
                                        GetText(i, 2));
                                sw_count6 = sw_count6 +
                                            Integer.parseInt(
                                            SWYW_ssrs.GetText(i, 3));
                                System.out.println(i + "死亡－意外伤害保险的金额是" +
                                        Double.
                                        parseDouble(SWYW_ssrs.GetText(i, 2)));
                                tRiskCode = "死亡给付\\意外伤害\\";
                            }
                            System.out.println("死亡－意外伤害的总金额是" + sw_money6);
                        }
                        tlisttable.setName("RISKF6");
                    }
                    if (k == 7)
                    {
                        //死亡－健康 险
                        String SWJK = SiWang + JianK + Grp;
                        System.out.println("死亡－健康险");
                        SSRS SWJK_ssrs = exesql.execSQL(SWJK);
                        if (SWJK_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= SWJK_ssrs.getMaxRow(); i++)
                            {
                                sw_money7 = sw_money7 +
                                            Double.parseDouble(SWJK_ssrs.
                                        GetText(i, 2));
                                sw_count7 = sw_count7 +
                                            Integer.parseInt(
                                            SWJK_ssrs.GetText(i, 3));
                                System.out.println(i + "死亡－健康险的金额是" +
                                        Double.
                                        parseDouble(SWJK_ssrs.GetText(i, 2)));
                                tRiskCode = "死亡给付\\健康险\\";
                            }
                            System.out.println("死亡－健康险的总金额是" + sw_money7);
                        }
                        tlisttable.setName("RISKF7");
                    }
                    if (k == 8)
                    {
                        //死亡－其他 险
                        String SWQT = SiWang + QiT + Grp;
                        System.out.println("死亡－其他险");
                        SSRS SWQT_ssrs = exesql.execSQL(SWQT);
                        if (SWQT_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= SWQT_ssrs.getMaxRow(); i++)
                            {
                                sw_money8 = sw_money8 +
                                            Double.parseDouble(SWQT_ssrs.
                                        GetText(i, 2));
                                sw_count8 = sw_count8 +
                                            Integer.parseInt(
                                            SWQT_ssrs.GetText(i, 3));
                                System.out.println(i + "死亡－其他险的金额是" +
                                        Double.
                                        parseDouble(SWQT_ssrs.GetText(i, 2)));
                                tRiskCode = "死亡给付\\其他\\";
                            }
                            System.out.println("死亡－其他险的总金额是" + sw_money8);
                        }
                        tlisttable.setName("RISKF8");
                    }
                    //会计的三级明细
                    String strArr[] = null;
                    //抽象成函数
                    String tXsqdName = ""; //销售渠道
                    String tXzdlName = ""; //险种大类
                    String tSxqName = ""; //首续期
                    String tDqjName = ""; //趸期交

                    for (int i = 1; i <= 4; i++) //销售渠道
                    {
                        String tXsqdSql = getXsqdSql(i);
                        System.out.println(i + "当i 的值是" + i + "的时候");
                        if (k == 1)
                        {
                            tXsqdSql = SiWang + ZhongS + tXsqdSql;
                        }
                        if (k == 2)
                        {
                            tXsqdSql = SiWang + LiangQ + tXsqdSql;
                        }
                        if (k == 3)
                        {
                            tXsqdSql = SiWang + DingQ + tXsqdSql;
                        }
                        if (k == 4)
                        {
                            tXsqdSql = SiWang + NianJ + tXsqdSql;
                        }
                        if (k == 5)
                        {
                            tXsqdSql = SiWang + ZhongJ + tXsqdSql;
                        }
                        if (k == 6)
                        {
                            tXsqdSql = SiWang + YiW + tXsqdSql;
                        }
                        if (k == 7)
                        {
                            tXsqdSql = SiWang + JianK + tXsqdSql;
                        }
                        if (k == 8)
                        {
                            tXsqdSql = SiWang + QiT + tXsqdSql;
                        }
                        for (int j = 1; j <= 4; j++) //险种大类
                        {
                            String tXzdlSql = getXzdlSql(j);
                            tXzdlSql = tXsqdSql + tXzdlSql + Grp;
                            System.out.println("T的值是" + t);
                            System.out.println("K的值是" + k);
                            System.out.println("I的值是" + i);
                            System.out.println("J的值是" + j);
                            System.out.println(j + "查询的语句是");
                            SSRS tssrs = exesql.execSQL(tXzdlSql);
                            if (tssrs.getMaxRow() != 0)
                            {
                                for (int q = 1; q <= tssrs.MaxRow; q++)
                                {
                                    strArr = new String[7];
                                    String tRiskcode = tssrs.GetText(q, 1);
                                    LMRiskDB tLMRiskDB = new LMRiskDB();
                                    tLMRiskDB.setRiskCode(tssrs.GetText(q, 1));
                                    if (tLMRiskDB.getInfo())
                                    {
                                        strArr[0] = tRiskCode +
                                                tLMRiskDB.getRiskName();
                                    }
                                    strArr[1] = getXsqdName(i);
                                    strArr[2] = getXzdlName(j);
                                    strArr[5] = tssrs.GetText(q, 2);
                                    strArr[6] = tssrs.GetText(q, 3);
                                    tlisttable.add(strArr);
                                }
                            }
                        }
                    }
                    strArr = new String[7];
                    strArr[0] = "Risk";
                    strArr[1] = "Xsqd";
                    strArr[2] = "Xzdl";
                    strArr[3] = "Sxq";
                    strArr[4] = "Dqj";
                    strArr[5] = "Money";
                    strArr[6] = "Count";
                    xmlexport.addListTable(tlisttable, strArr);
                }
                //以上是k的结束
            } //end of t==6

            if (t == 7)
            {
                //暂收费给付
                String strArr[] = null;
                for (int k = 1; k <= 8; k++)
                {
                    ListTable tlisttable = new ListTable();
                    String tRiskCode = "";
                    //暂收费给付－终身 寿险
                    if (k == 1)
                    {
                        System.out.println("暂收费给付－终身寿险");
                        String ZSZS = ZanShou + ZhongS + Grp;
                        SSRS ZSZS_ssrs = exesql.execSQL(ZSZS);
                        if (ZSZS_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= ZSZS_ssrs.getMaxRow(); i++)
                            {
                                zs_money1 = zs_money1 +
                                            Double.parseDouble(ZSZS_ssrs.
                                        GetText(i, 2));
                                zs_count1 = zs_count1 +
                                            Integer.parseInt(
                                            ZSZS_ssrs.GetText(i, 3));
                                System.out.println( +i + "年金金额是" +
                                        Double.
                                        parseDouble(ZSZS_ssrs.GetText(i, 2)));
                                tRiskCode = "暂收费给付\\终身寿险\\";
                            }
                            System.out.println("暂收费终身寿险总金额是" + zs_money1);
                        }
                        tlisttable.setName("RISKG1");
                    }
                    //年金给付－两全 险
                    if (k == 2)
                    {
                        System.out.println("暂收费给付－两全险");
                        String ZSLQ = ZanShou + LiangQ + Grp;
                        SSRS ZSLQ_ssrs = exesql.execSQL(ZSLQ);
                        if (ZSLQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= ZSLQ_ssrs.getMaxRow(); i++)
                            {
                                zs_money2 = zs_money2 +
                                            Double.parseDouble(ZSLQ_ssrs.
                                        GetText(i, 2));
                                zs_count2 = zs_count2 +
                                            Integer.parseInt(
                                            ZSLQ_ssrs.GetText(i, 3));
                                System.out.println("暂收费两全险的金额是" +
                                        Double.
                                        parseDouble(ZSLQ_ssrs.GetText(i, 2)));
                                tRiskCode = "暂收费给付\\两全险\\";
                            }
                            System.out.println("暂收费给付\\两全总金额是" + zs_money2);
                        }
                        tlisttable.setName("RISKG2");
                    }
                    //暂收费给付－定期 保险
                    if (k == 3)
                    {
                        String ZSDQ = ZanShou + DingQ + Grp;
                        System.out.println("暂收费－定期保险");
                        SSRS ZSDQ_ssrs = exesql.execSQL(ZSDQ);
                        if (ZSDQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= ZSDQ_ssrs.getMaxRow(); i++)
                            {
                                zs_money3 = zs_money3 +
                                            Double.parseDouble(ZSDQ_ssrs.
                                        GetText(i, 2));
                                zs_count3 = zs_count3 +
                                            Integer.parseInt(
                                            ZSDQ_ssrs.GetText(i, 3));
                                System.out.println(i + "暂收费定期保险的金额是" +
                                        Double.
                                        parseDouble(ZSDQ_ssrs.GetText(i, 2)));
                                tRiskCode = "暂收费给付\\定期保险\\";
                            }
                            System.out.println("暂收费－定期的中金额是" + zs_money3);
                        }
                        tlisttable.setName("RISKG3");
                    }
                    if (k == 4)
                    {
                        //暂收费给付－年金 保险
                        String ZSNJ = ZanShou + NianJ + Grp;
                        System.out.println("暂收费－年金保险");
                        SSRS ZSNJ_ssrs = exesql.execSQL(ZSNJ);
                        if (ZSNJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= ZSNJ_ssrs.getMaxRow(); i++)
                            {
                                zs_money4 = zs_money4 +
                                            Double.parseDouble(ZSNJ_ssrs.
                                        GetText(i, 2));
                                zs_count4 = zs_count4 +
                                            Integer.parseInt(
                                            ZSNJ_ssrs.GetText(i, 3));
                                System.out.println(i + "暂收费－年金保险的金额是" +
                                        Double.
                                        parseDouble(ZSNJ_ssrs.GetText(i, 2)));
                                tRiskCode = "暂收费给付\\年金保险\\";
                            }
                            System.out.println("暂收费－年金保险的总金额是" + zs_money4);
                        }
                        tlisttable.setName("RISKG4");
                    }
                    if (k == 5)
                    {
                        //暂收费－重大疾病保险
                        String ZSZJ = ZanShou + ZhongJ + Grp;
                        System.out.println("暂收费－重大疾病");
                        SSRS ZSZJ_ssrs = exesql.execSQL(ZSZJ);
                        if (ZSZJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= ZSZJ_ssrs.getMaxRow(); i++)
                            {
                                zs_money5 = zs_money5 +
                                            Double.parseDouble(ZSZJ_ssrs.
                                        GetText(i, 2));
                                zs_count5 = zs_count5 +
                                            Integer.parseInt(
                                            ZSZJ_ssrs.GetText(i, 3));
                                System.out.println(i + "暂收费－重大疾病的金额是" +
                                        Double.
                                        parseDouble(ZSZJ_ssrs.GetText(i, 2)));
                                tRiskCode = "暂收费给付\\重大疾病保险\\";
                            }
                            System.out.println("暂收费－重大疾病的总金额是" + zs_money5);
                        }
                        tlisttable.setName("RISKG5");
                    }
                    //年金－意外伤害 保险
                    if (k == 6)
                    {
                        String ZSYW = ZanShou + YiW + Grp;
                        System.out.println("暂收费－意外伤害保险");
                        SSRS ZSYW_ssrs = exesql.execSQL(ZSYW);
                        if (ZSYW_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= ZSYW_ssrs.getMaxRow(); i++)
                            {
                                zs_money6 = zs_money6 +
                                            Double.parseDouble(ZSYW_ssrs.
                                        GetText(i, 2));
                                zs_count6 = zs_count6 +
                                            Integer.parseInt(
                                            ZSYW_ssrs.GetText(i, 3));
                                System.out.println(i + "暂收费－意外伤害保险的金额是" +
                                        Double.parseDouble(ZSYW_ssrs.GetText(i,
                                        2)));
                                tRiskCode = "暂收费给付\\意外伤害\\";
                            }
                            System.out.println("暂收费－意外伤害的总金额是" + zs_money6);
                        }
                        tlisttable.setName("RISKG6");
                    }
                    if (k == 7)
                    {
                        // 暂收费－健康 险
                        String ZSJK = ZanShou + JianK + Grp;
                        System.out.println("暂收费－健康险");
                        SSRS ZSJK_ssrs = exesql.execSQL(ZSJK);
                        if (ZSJK_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= ZSJK_ssrs.getMaxRow(); i++)
                            {
                                zs_money7 = zs_money7 +
                                            Double.parseDouble(ZSJK_ssrs.
                                        GetText(i, 2));
                                zs_count7 = zs_count7 +
                                            Integer.parseInt(
                                            ZSJK_ssrs.GetText(i, 3));
                                System.out.println(i + "暂收费－健康险的金额是" +
                                        Double.
                                        parseDouble(ZSJK_ssrs.GetText(i, 2)));
                                tRiskCode = "暂收费给付\\健康险\\";
                            }
                            System.out.println("暂收费－健康险的总金额是" + zs_money7);
                        }
                        tlisttable.setName("RISKG7");
                    }
                    if (k == 8)
                    {
                        //暂收费－其他 险
                        String ZSQT = ZanShou + QiT + Grp;
                        System.out.println("暂收费－其他险");
                        SSRS ZSQT_ssrs = exesql.execSQL(ZSQT);
                        if (ZSQT_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= ZSQT_ssrs.getMaxRow(); i++)
                            {
                                zs_money8 = zs_money8 +
                                            Double.parseDouble(ZSQT_ssrs.
                                        GetText(i, 2));
                                zs_count8 = zs_count8 +
                                            Integer.parseInt(
                                            ZSQT_ssrs.GetText(i, 3));
                                System.out.println(i + "暂收费－其他险的金额是" +
                                        Double.
                                        parseDouble(ZSQT_ssrs.GetText(i, 2)));
                                tRiskCode = "暂收费给付\\其他\\";
                            }
                            System.out.println("暂收费－其他险的总金额是" + zs_money8);
                        }
                        tlisttable.setName("RISKG8");
                    }

                    String tXsqdName = ""; //销售渠道
                    String tXzdlName = ""; //险种大类

//       for (int i=1;i<=4;i++)   //销售渠道
//       {
//         String tXsqdSql=getXsqdSql(i);
//         System.out.println("T是"+t+"时的情况");
//         System.out.println(i+"当i 的值是"+i+"的时候");
                    String ZanShou_sql = "";
                    if (k == 1)
                    {
                        ZanShou_sql = ZanShou + ZhongS;
                    }
                    if (k == 2)
                    {
                        ZanShou_sql = ZanShou + LiangQ;
                    }
                    if (k == 3)
                    {
                        ZanShou_sql = ZanShou + DingQ;
                    }
                    if (k == 4)
                    {
                        ZanShou_sql = ZanShou + NianJ;
                    }
                    if (k == 5)
                    {
                        ZanShou_sql = ZanShou + ZhongJ;
                    }
                    if (k == 6)
                    {
                        ZanShou_sql = ZanShou + YiW;
                    }
                    if (k == 7)
                    {
                        ZanShou_sql = ZanShou + JianK;
                    }
                    if (k == 8)
                    {
                        ZanShou_sql = ZanShou + QiT;
                    }
                    for (int j = 1; j <= 4; j++) //险种大类
                    {
                        String tXzdlSql = getXzdlSql(j);
                        tXzdlSql = ZanShou_sql + tXzdlSql + Grp;
                        System.out.println("T的值是" + t);
                        System.out.println("K的值是" + k);
                        System.out.println("J的值是" + j);
                        System.out.println(j + "查询的语句是");
                        SSRS tssrs = exesql.execSQL(tXzdlSql);
                        if (tssrs.getMaxRow() != 0)
                        {
                            for (int q = 1; q <= tssrs.MaxRow; q++)
                            {
                                strArr = new String[7];
                                String tRiskcode = tssrs.GetText(q, 1);
                                LMRiskDB tLMRiskDB = new LMRiskDB();
                                tLMRiskDB.setRiskCode(tssrs.GetText(q, 1));
                                if (tLMRiskDB.getInfo())
                                {
                                    strArr[0] = tRiskCode +
                                                tLMRiskDB.getRiskName();
                                }
                                strArr[2] = getXzdlName(j);
                                strArr[5] = tssrs.GetText(q, 2);
                                strArr[6] = tssrs.GetText(q, 3);
                                tlisttable.add(strArr);
                            }
                        }
                    }
//       }
                    strArr = new String[7];
                    strArr[0] = "Risk";
                    strArr[1] = "Xsqd";
                    strArr[2] = "Xzdl";
                    strArr[3] = "Sxq";
                    strArr[4] = "Dqj";
                    strArr[5] = "Money";
                    strArr[6] = "Count";
                    xmlexport.addListTable(tlisttable, strArr);
                }
                //以上是k的结束
            } //end of t=7
            if (t == 8)
            {
                //其他给付
                for (int k = 1; k <= 8; k++)
                {
                    ListTable tlisttable = new ListTable();
                    String tRiskCode = "";
                    //其他给付－终身 寿险
                    if (k == 1)
                    {
                        System.out.println("其他给付－终身寿险");
                        String QTZS = QiTa_1 + QiTa_2 + ZhongS + QiTa_3 +
                                      ZhongS + QiTa_4 + ZhongS + QiTa_5 +
                                      ZhongS + " ) " + Grp;
                        SSRS QTZS_ssrs = exesql.execSQL(QTZS);
                        if (QTZS_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= QTZS_ssrs.getMaxRow(); i++)
                            {
                                qt_money1 = qt_money1 +
                                            Double.parseDouble(QTZS_ssrs.
                                        GetText(i, 2));
                                qt_count1 = qt_count1 +
                                            Integer.parseInt(
                                            QTZS_ssrs.GetText(i, 3));
                                System.out.println( +i + "其他金额是" +
                                        Double.
                                        parseDouble(QTZS_ssrs.GetText(i, 2)));
                                tRiskCode = "其他给付\\终身寿险\\";
                            }
                            System.out.println("其他终身寿险总金额是" + qt_money1);
                        }
                        tlisttable.setName("RISKH1");
                    }
                    //其他给付－两全 险
                    if (k == 2)
                    {
                        System.out.println("其他给付－两全险");
                        String QTLQ = QiTa_1 + QiTa_2 + LiangQ + QiTa_3 +
                                      LiangQ + QiTa_4 + LiangQ + QiTa_5 +
                                      LiangQ + " ) " + Grp;
                        SSRS QTLQ_ssrs = exesql.execSQL(QTLQ);
                        if (QTLQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= QTLQ_ssrs.getMaxRow(); i++)
                            {
                                qt_money2 = qt_money2 +
                                            Double.parseDouble(QTLQ_ssrs.
                                        GetText(i, 2));
                                qt_count2 = qt_count2 +
                                            Integer.parseInt(
                                            QTLQ_ssrs.GetText(i, 3));
                                System.out.println( +i + "其他给付- 两全险的金额是" +
                                        Double.parseDouble(QTLQ_ssrs.GetText(i,
                                        2)));
                                tRiskCode = "其他给付\\两全险\\";
                            }
                            System.out.println("其他给付\\两全总金额是" + qt_money2);
                        }
                        tlisttable.setName("RISKH2");
                    }
                    //其他给付－定期 保险
                    if (k == 3)
                    {
                        String QTDQ = QiTa_1 + QiTa_2 + DingQ + QiTa_3 + DingQ +
                                      QiTa_4 + DingQ + QiTa_5 + DingQ + " ) " +
                                      Grp;
                        System.out.println("其他给付－定期保险");
                        SSRS QTDQ_ssrs = exesql.execSQL(QTDQ);
                        if (QTDQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= QTDQ_ssrs.getMaxRow(); i++)
                            {
                                qt_money3 = qt_money3 +
                                            Double.parseDouble(QTDQ_ssrs.
                                        GetText(i, 2));
                                qt_count3 = qt_count3 +
                                            Integer.parseInt(
                                            QTDQ_ssrs.GetText(i, 3));
                                System.out.println(i + "其他定期保险的金额是" +
                                        Double.
                                        parseDouble(QTDQ_ssrs.GetText(i, 2)));
                                tRiskCode = "其他给付\\定期保险\\";
                            }
                            System.out.println("其他给付－定期的中金额是" + qt_money3);
                        }
                        tlisttable.setName("RISKH3");
                    }
                    if (k == 4)
                    {
                        //其他给付－年金 保险
                        String QTNJ = QiTa_1 + QiTa_2 + NianJ + QiTa_3 + NianJ +
                                      QiTa_4 + NianJ + QiTa_5 + NianJ + " ) " +
                                      Grp;
                        System.out.println("其他给付－年金保险");
                        SSRS QTNJ_ssrs = exesql.execSQL(QTNJ);
                        if (QTNJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= QTNJ_ssrs.getMaxRow(); i++)
                            {
                                qt_money4 = qt_money4 +
                                            Double.parseDouble(QTNJ_ssrs.
                                        GetText(i, 2));
                                qt_count4 = qt_count4 +
                                            Integer.parseInt(
                                            QTNJ_ssrs.GetText(i, 3));
                                System.out.println(i + "其他给付－年金保险的金额是" +
                                        Double.
                                        parseDouble(QTNJ_ssrs.GetText(i, 2)));
                                tRiskCode = "其他给付\\年金保险\\";
                            }
                            System.out.println("其他给付－年金保险的总金额是" + qt_money4);
                        }
                        tlisttable.setName("RISKH4");
                    }
                    if (k == 5)
                    {
                        //其他给付－重大疾病保险
                        String QTZJ = QiTa_1 + QiTa_2 + ZhongJ + QiTa_3 +
                                      ZhongJ + QiTa_4 + ZhongJ + QiTa_5 +
                                      ZhongJ + " ) " + Grp;
                        System.out.println("其他给付－重大疾病");
                        SSRS QTZJ_ssrs = exesql.execSQL(QTZJ);
                        if (QTZJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= QTZJ_ssrs.getMaxRow(); i++)
                            {
                                qt_money5 = qt_money5 +
                                            Double.parseDouble(QTZJ_ssrs.
                                        GetText(i, 2));
                                qt_count5 = qt_count5 +
                                            Integer.parseInt(
                                            QTZJ_ssrs.GetText(i, 3));
                                System.out.println(i + "其他给付－重大疾病的金额是" +
                                        Double.
                                        parseDouble(QTZJ_ssrs.GetText(i, 2)));
                                tRiskCode = "其他给付\\重大疾病保险\\";
                            }
                            System.out.println("其他给付－重大疾病的总金额是" + qt_money5);
                        }
                        tlisttable.setName("RISKH5");
                    }
                    //其他给付－意外伤害 保险
                    if (k == 6)
                    {
                        String QTYW = QiTa_1 + QiTa_2 + YiW + QiTa_3 + YiW +
                                      QiTa_4 + YiW + QiTa_5 + YiW + " ) " + Grp;
                        System.out.println("其他给付－意外伤害保险");
                        SSRS QTYW_ssrs = exesql.execSQL(QTYW);
                        if (QTYW_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= QTYW_ssrs.getMaxRow(); i++)
                            {
                                qt_money6 = qt_money6 +
                                            Double.parseDouble(QTYW_ssrs.
                                        GetText(i, 2));
                                qt_count6 = qt_count6 +
                                            Integer.parseInt(
                                            QTYW_ssrs.GetText(i, 3));
                                System.out.println(i + "其他给付－意外伤害保险的金额是" +
                                        Double.parseDouble(QTYW_ssrs.GetText(i,
                                        2)));
                                tRiskCode = "其他给付\\意外伤害\\";
                            }
                            System.out.println("其他给付－意外伤害的总金额是" + qt_money6);
                        }
                        tlisttable.setName("RISKH6");
                    }
                    if (k == 7)
                    {
                        // 其他给付－健康 险
                        String QTJK = QiTa_1 + QiTa_2 + JianK + QiTa_3 + JianK +
                                      QiTa_4 + JianK + QiTa_5 + JianK + " ) " +
                                      Grp;
                        System.out.println("其他给付－健康险");
                        SSRS QTJK_ssrs = exesql.execSQL(QTJK);
                        if (QTJK_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= QTJK_ssrs.getMaxRow(); i++)
                            {
                                qt_money7 = qt_money7 +
                                            Double.parseDouble(QTJK_ssrs.
                                        GetText(i, 2));
                                qt_count7 = qt_count7 +
                                            Integer.parseInt(
                                            QTJK_ssrs.GetText(i, 3));
                                System.out.println(i + "其他给付－健康险的金额是" +
                                        Double.
                                        parseDouble(QTJK_ssrs.GetText(i, 2)));
                                tRiskCode = "其他给付\\健康险\\";
                            }
                            System.out.println("其他给付－健康险的总金额是" + qt_money7);
                        }
                        tlisttable.setName("RISKH7");
                    }
                    if (k == 8)
                    {
                        //其他给付－其他 险
                        String QTQT = QiTa_1 + QiTa_2 + QiT + QiTa_3 + QiT +
                                      QiTa_4 + QiT + QiTa_5 + QiT + " ) " + Grp;
                        System.out.println("其他给付－其他险");
                        SSRS QTQT_ssrs = exesql.execSQL(QTQT);
                        if (QTQT_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= QTQT_ssrs.getMaxRow(); i++)
                            {
                                qt_money8 = qt_money8 +
                                            Double.parseDouble(QTQT_ssrs.
                                        GetText(i, 2));
                                qt_count8 = qt_count8 +
                                            Integer.parseInt(
                                            QTQT_ssrs.GetText(i, 3));
                                System.out.println(i + "其他给付－其他险的金额是" +
                                        Double.
                                        parseDouble(QTQT_ssrs.GetText(i, 2)));
                                tRiskCode = "其他给付\\其他\\";
                            }
                            System.out.println("其他给付－其他险的总金额是" + qt_money8);
                        }
                        tlisttable.setName("RISKH8");
                    }
                    //会计的三级明细
                    String strArr[] = null;
                    //抽象成函数
                    String tXsqdName = ""; //销售渠道
                    String tXzdlName = ""; //险种大类
                    String tSxqName = ""; //首续期
                    String tDqjName = ""; //趸期交

                    for (int i = 1; i <= 4; i++) //销售渠道
                    {
                        String tXsqdSql = getXsqdSql(i);
                        String tXsqdSql_1 = getXsqdSql_1(i);
                        System.out.println("T是" + t + "时的情况");
                        System.out.println(i + "当i 的值是" + i + "的时候");
                        if (k == 1)
                        {
                            tXsqdSql = QiTa_1 + QiTa_2 + ZhongS + tXsqdSql +
                                       QiTa_3 + ZhongS + tXsqdSql
                                       + QiTa_4 + ZhongS + tXsqdSql_1 + QiTa_5 +
                                       ZhongS + tXsqdSql_1;
                        }
                        if (k == 2)
                        {
                            tXsqdSql = QiTa_1 + QiTa_2 + LiangQ + tXsqdSql +
                                       QiTa_3 + LiangQ + tXsqdSql
                                       + QiTa_4 + LiangQ + tXsqdSql_1 + QiTa_5 +
                                       LiangQ + tXsqdSql_1;
                        }
                        if (k == 3)
                        {
                            tXsqdSql = QiTa_1 + QiTa_2 + DingQ + tXsqdSql +
                                       QiTa_3 + DingQ + tXsqdSql
                                       + QiTa_4 + DingQ + tXsqdSql_1 + QiTa_5 +
                                       DingQ + tXsqdSql_1;
                        }
                        if (k == 4)
                        {
                            tXsqdSql = QiTa_1 + QiTa_2 + NianJ + tXsqdSql +
                                       QiTa_3 + NianJ + tXsqdSql
                                       + QiTa_4 + NianJ + tXsqdSql_1 + QiTa_5 +
                                       NianJ + tXsqdSql_1;
                        }
                        if (k == 5)
                        {
                            tXsqdSql = QiTa_1 + QiTa_2 + ZhongJ + tXsqdSql +
                                       QiTa_3 + ZhongJ + tXsqdSql
                                       + QiTa_4 + ZhongJ + tXsqdSql_1 + QiTa_5 +
                                       ZhongJ + tXsqdSql_1;
                        }
                        if (k == 6)
                        {
                            tXsqdSql = QiTa_1 + QiTa_2 + YiW + tXsqdSql +
                                       QiTa_3 + YiW + tXsqdSql
                                       + QiTa_4 + YiW + tXsqdSql_1 + QiTa_5 +
                                       YiW + tXsqdSql_1;
                        }
                        if (k == 7)
                        {
                            tXsqdSql = QiTa_1 + QiTa_2 + JianK + tXsqdSql +
                                       QiTa_3 + JianK + tXsqdSql
                                       + QiTa_4 + JianK + tXsqdSql_1 + QiTa_5 +
                                       JianK + tXsqdSql_1;
                        }
                        if (k == 8)
                        {
                            tXsqdSql = QiTa_1 + QiTa_2 + QiT + tXsqdSql +
                                       QiTa_3 + QiT + tXsqdSql
                                       + QiTa_4 + QiT + tXsqdSql_1 + QiTa_5 +
                                       QiT + tXsqdSql_1;
                        }
                        for (int j = 1; j <= 4; j++) //险种大类
                        {
                            String tXzdlSql = getXzdlSql(j);
                            tXzdlSql = tXsqdSql + tXzdlSql + " )" + Grp;
                            System.out.println("T的值是" + t);
                            System.out.println("K的值是" + k);
                            System.out.println("I的值是" + i);
                            System.out.println("J的值是" + j);
                            System.out.println(j + "查询的语句是");
                            SSRS tssrs = exesql.execSQL(tXzdlSql);
                            if (tssrs.getMaxRow() != 0)
                            {
                                for (int q = 1; q <= tssrs.MaxRow; q++)
                                {
                                    strArr = new String[7];
                                    String tRiskcode = tssrs.GetText(q, 1);
                                    LMRiskDB tLMRiskDB = new LMRiskDB();
                                    tLMRiskDB.setRiskCode(tssrs.GetText(q, 1));
                                    if (tLMRiskDB.getInfo())
                                    {
                                        strArr[0] = tRiskCode +
                                                tLMRiskDB.getRiskName();
                                    }
                                    strArr[1] = getXsqdName(i);
                                    strArr[2] = getXzdlName(j);
                                    strArr[5] = tssrs.GetText(q, 2);
                                    strArr[6] = tssrs.GetText(q, 3);
                                    tlisttable.add(strArr);
                                }
                            }
                        }
                    }
                    strArr = new String[7];
                    strArr[0] = "Risk";
                    strArr[1] = "Xsqd";
                    strArr[2] = "Xzdl";
                    strArr[3] = "Sxq";
                    strArr[4] = "Dqj";
                    strArr[5] = "Money";
                    strArr[6] = "Count";
                    xmlexport.addListTable(tlisttable, strArr);
                }
                //以上是k的结束
            } //end of t=8

            if (t == 9)
            {
                //红利给付
                for (int k = 1; k <= 8; k++)
                {
                    ListTable tlisttable = new ListTable();
                    String tRiskCode = "";
                    //红利给付－终身 寿险
                    if (k == 1)
                    {
                        System.out.println("红利给付－终身寿险");
                        String HLZS = HongLi_1 + HongLi_2 + ZhongS + HongLi_3 +
                                      ZhongS + " ) " + Grp;
                        SSRS HLZS_ssrs = exesql.execSQL(HLZS);
                        if (HLZS_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= HLZS_ssrs.getMaxRow(); i++)
                            {
                                hl_money1 = hl_money1 +
                                            Double.parseDouble(HLZS_ssrs.
                                        GetText(i, 2));
                                hl_count1 = hl_count1 +
                                            Integer.parseInt(
                                            HLZS_ssrs.GetText(i, 3));
                                System.out.println( +i + "红利金额是" +
                                        Double.
                                        parseDouble(HLZS_ssrs.GetText(i, 2)));
                                tRiskCode = "红利给付\\终身寿险\\";
                            }
                            System.out.println("红利给付终身寿险总金额是" + hl_money1);
                        }
                        tlisttable.setName("RISKI1");
                    }
                    //红利给付－两全 险
                    if (k == 2)
                    {
                        System.out.println("红利给付－两全险");
                        String HLLQ = HongLi_1 + HongLi_2 + LiangQ + HongLi_3 +
                                      LiangQ + " ) " + Grp;
                        SSRS HLLQ_ssrs = exesql.execSQL(HLLQ);
                        if (HLLQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= HLLQ_ssrs.getMaxRow(); i++)
                            {
                                hl_money2 = hl_money2 +
                                            Double.parseDouble(HLLQ_ssrs.
                                        GetText(i, 2));
                                hl_count2 = hl_count2 +
                                            Integer.parseInt(
                                            HLLQ_ssrs.GetText(i, 3));
                                System.out.println( +i + "红利给付- 两全险的金额是" +
                                        Double.parseDouble(HLLQ_ssrs.GetText(i,
                                        2)));
                                tRiskCode = "红利给付\\两全险\\";
                            }
                            System.out.println("红利给付\\两全总金额是" + hl_money2);
                        }
                        tlisttable.setName("RISKI2");
                    }
                    //红利给付－定期 保险
                    if (k == 3)
                    {
                        String HLDQ = HongLi_1 + HongLi_2 + DingQ + HongLi_3 +
                                      DingQ + " ) " + Grp;
                        System.out.println("红利给付－定期保险");
                        SSRS HLDQ_ssrs = exesql.execSQL(HLDQ);
                        if (HLDQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= HLDQ_ssrs.getMaxRow(); i++)
                            {
                                hl_money3 = hl_money3 +
                                            Double.parseDouble(HLDQ_ssrs.
                                        GetText(i, 2));
                                hl_count3 = hl_count3 +
                                            Integer.parseInt(
                                            HLDQ_ssrs.GetText(i, 3));
                                System.out.println(i + "红利给付定期保险的金额是" +
                                        Double.
                                        parseDouble(HLDQ_ssrs.GetText(i, 2)));
                                tRiskCode = "红利给付\\定期保险\\";
                            }
                            System.out.println("红利给付－定期的中金额是" + hl_money3);
                        }
                        tlisttable.setName("RISKI3");
                    }
                    if (k == 4)
                    {
                        //红利给付－年金 保险
                        String HLNJ = HongLi_1 + HongLi_2 + NianJ + HongLi_3 +
                                      NianJ + " ) " + Grp;
                        System.out.println("红利给付－年金保险");
                        SSRS HLNJ_ssrs = exesql.execSQL(HLNJ);
                        if (HLNJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= HLNJ_ssrs.getMaxRow(); i++)
                            {
                                hl_money4 = hl_money4 +
                                            Double.parseDouble(HLNJ_ssrs.
                                        GetText(i, 2));
                                hl_count4 = hl_count4 +
                                            Integer.parseInt(
                                            HLNJ_ssrs.GetText(i, 3));
                                System.out.println(i + "红利给付－年金保险的金额是" +
                                        Double.
                                        parseDouble(HLNJ_ssrs.GetText(i, 2)));
                                tRiskCode = "红利给付\\年金保险\\";
                            }
                            System.out.println("红利给付－年金保险的总金额是" + hl_money4);
                        }
                        tlisttable.setName("RISKI4");
                    }
                    if (k == 5)
                    {
                        //红利给付－重大疾病保险
                        String HLZJ = HongLi_1 + HongLi_2 + ZhongJ + HongLi_3 +
                                      ZhongJ + " ) " + Grp;
                        System.out.println("红利给付－重大疾病");
                        SSRS HLZJ_ssrs = exesql.execSQL(HLZJ);
                        if (HLZJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= HLZJ_ssrs.getMaxRow(); i++)
                            {
                                hl_money5 = hl_money5 +
                                            Double.parseDouble(HLZJ_ssrs.
                                        GetText(i, 2));
                                hl_count5 = hl_count5 +
                                            Integer.parseInt(
                                            HLZJ_ssrs.GetText(i, 3));
                                System.out.println(i + "红利给付－重大疾病的金额是" +
                                        Double.
                                        parseDouble(HLZJ_ssrs.GetText(i, 2)));
                                tRiskCode = "红利给付\\重大疾病保险\\";
                            }
                            System.out.println("红利给付－重大疾病的总金额是" + hl_money5);
                        }
                        tlisttable.setName("RISKI5");
                    }
                    //红利给付－意外伤害 保险
                    if (k == 6)
                    {
                        String HLYW = HongLi_1 + HongLi_2 + YiW + HongLi_3 +
                                      YiW + " ) " + Grp;
                        System.out.println("红利给付－意外伤害保险");
                        SSRS HLYW_ssrs = exesql.execSQL(HLYW);
                        if (HLYW_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= HLYW_ssrs.getMaxRow(); i++)
                            {
                                hl_money6 = hl_money6 +
                                            Double.parseDouble(HLYW_ssrs.
                                        GetText(i, 2));
                                hl_count6 = hl_count6 +
                                            Integer.parseInt(
                                            HLYW_ssrs.GetText(i, 3));
                                System.out.println(i + "红利给付－意外伤害保险的金额是" +
                                        Double.parseDouble(HLYW_ssrs.GetText(i,
                                        2)));
                                tRiskCode = "红利给付\\意外伤害\\";
                            }
                            System.out.println("红利给付－意外伤害的总金额是" + hl_money6);
                        }
                        tlisttable.setName("RISKI6");
                    }
                    if (k == 7)
                    {
                        // 红利给付－健康 险
                        String HLJK = HongLi_1 + HongLi_2 + JianK + HongLi_3 +
                                      JianK + " ) " + Grp;
                        System.out.println("红利给付－健康险");
                        SSRS HLJK_ssrs = exesql.execSQL(HLJK);
                        if (HLJK_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= HLJK_ssrs.getMaxRow(); i++)
                            {
                                hl_money7 = hl_money7 +
                                            Double.parseDouble(HLJK_ssrs.
                                        GetText(i, 2));
                                hl_count7 = hl_count7 +
                                            Integer.parseInt(
                                            HLJK_ssrs.GetText(i, 3));
                                System.out.println(i + "红利给付－健康险的金额是" +
                                        Double.
                                        parseDouble(HLJK_ssrs.GetText(i, 2)));
                                tRiskCode = "红利给付\\健康险\\";
                            }
                            System.out.println("红利给付－健康险的总金额是" + hl_money7);
                        }
                        tlisttable.setName("RISKI7");
                    }
                    if (k == 8)
                    {
                        //红利给付－其他 险
                        String HLQT = HongLi_1 + HongLi_2 + QiT + HongLi_3 +
                                      QiT + " ) " + Grp;
                        System.out.println("红利给付－其他险");
                        SSRS HLQT_ssrs = exesql.execSQL(HLQT);
                        if (HLQT_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= HLQT_ssrs.getMaxRow(); i++)
                            {
                                hl_money8 = hl_money8 +
                                            Double.parseDouble(HLQT_ssrs.
                                        GetText(i, 2));
                                hl_count8 = hl_count8 +
                                            Integer.parseInt(
                                            HLQT_ssrs.GetText(i, 3));
                                System.out.println(i + "红利给付－其他险的金额是" +
                                        Double.
                                        parseDouble(HLQT_ssrs.GetText(i, 2)));
                                tRiskCode = "红利给付\\其他\\";
                            }
                            System.out.println("红利给付－其他险的总金额是" + hl_money8);
                        }
                        tlisttable.setName("RISKI8");
                    }
                    //会计的三级明细
                    String strArr[] = null;
                    //抽象成函数
                    String tXsqdName = ""; //销售渠道
                    String tXzdlName = ""; //险种大类
                    String tSxqName = ""; //首续期
                    String tDqjName = ""; //趸期交

                    for (int i = 1; i <= 4; i++) //销售渠道
                    {
                        String tXsqdSql = getXsqdSql(i);
                        System.out.println("T是" + t + "时的情况");
                        System.out.println(i + "当i 的值是" + i + "的时候");
                        if (k == 1)
                        {
                            tXsqdSql = HongLi_1 + HongLi_2 + ZhongS + tXsqdSql +
                                       HongLi_3 + ZhongS + tXsqdSql;
                        }
                        if (k == 2)
                        {
                            tXsqdSql = HongLi_1 + HongLi_2 + LiangQ + tXsqdSql +
                                       HongLi_3 + LiangQ + tXsqdSql;
                        }
                        if (k == 3)
                        {
                            tXsqdSql = HongLi_1 + HongLi_2 + DingQ + tXsqdSql +
                                       HongLi_3 + DingQ + tXsqdSql;
                        }
                        if (k == 4)
                        {
                            tXsqdSql = HongLi_1 + HongLi_2 + NianJ + tXsqdSql +
                                       HongLi_3 + NianJ + tXsqdSql;
                        }
                        if (k == 5)
                        {
                            tXsqdSql = HongLi_1 + HongLi_2 + ZhongJ + tXsqdSql +
                                       HongLi_3 + ZhongJ + tXsqdSql;
                        }
                        if (k == 6)
                        {
                            tXsqdSql = HongLi_1 + HongLi_2 + YiW + tXsqdSql +
                                       HongLi_3 + YiW + tXsqdSql;
                        }
                        if (k == 7)
                        {
                            tXsqdSql = HongLi_1 + HongLi_2 + JianK + tXsqdSql +
                                       HongLi_3 + JianK + tXsqdSql;
                        }
                        if (k == 8)
                        {
                            tXsqdSql = HongLi_1 + HongLi_2 + QiT + tXsqdSql +
                                       HongLi_3 + QiT + tXsqdSql;
                        }
                        for (int j = 1; j <= 4; j++) //险种大类
                        {
                            String tXzdlSql = getXzdlSql(j);
                            tXzdlSql = tXsqdSql + tXzdlSql + " )" + Grp;
                            System.out.println("T的值是" + t);
                            System.out.println("K的值是" + k);
                            System.out.println("I的值是" + i);
                            System.out.println("J的值是" + j);
                            System.out.println(j + "查询的语句是");
                            SSRS tssrs = exesql.execSQL(tXzdlSql);
                            if (tssrs.getMaxRow() != 0)
                            {
                                for (int q = 1; q <= tssrs.MaxRow; q++)
                                {
                                    strArr = new String[7];
                                    String tRiskcode = tssrs.GetText(q, 1);
                                    LMRiskDB tLMRiskDB = new LMRiskDB();
                                    tLMRiskDB.setRiskCode(tssrs.GetText(q, 1));
                                    if (tLMRiskDB.getInfo())
                                    {
                                        strArr[0] = tRiskCode +
                                                tLMRiskDB.getRiskName();
                                    }
                                    strArr[1] = getXsqdName(i);
                                    strArr[2] = getXzdlName(j);
                                    strArr[5] = tssrs.GetText(q, 2);
                                    strArr[6] = tssrs.GetText(q, 3);
                                    tlisttable.add(strArr);
                                }
                            }
                        }
                    }
                    strArr = new String[7];
                    strArr[0] = "Risk";
                    strArr[1] = "Xsqd";
                    strArr[2] = "Xzdl";
                    strArr[3] = "Sxq";
                    strArr[4] = "Dqj";
                    strArr[5] = "Money";
                    strArr[6] = "Count";
                    xmlexport.addListTable(tlisttable, strArr);
                }
                //以上是k的结束
            } //end of t=9

            //保全退费－退余额
            if (t == 10)
            {
                //保全退费（退余额）
                for (int k = 1; k <= 8; k++)
                {
                    ListTable tlisttable = new ListTable();
                    String tRiskCode = "";
                    //保全退费（退余额）－终身 寿险
                    if (k == 1)
                    {
                        System.out.println("保全退费（退余额）－终身寿险");
                        String YEZS = YuE + ZhongS + Grp;
                        SSRS YEZS_ssrs = exesql.execSQL(YEZS);
                        if (YEZS_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= YEZS_ssrs.getMaxRow(); i++)
                            {
                                ye_money1 = ye_money1 +
                                            Double.parseDouble(YEZS_ssrs.
                                        GetText(i, 2));
                                ye_count1 = ye_count1 +
                                            Integer.parseInt(
                                            YEZS_ssrs.GetText(i, 3));
                                tRiskCode = "保全退费给付（退余额）\\终身寿险\\";
                            }
                        }
                        tlisttable.setName("RISKJ1");
                    }
                    //保全退费给付（退余额）－两全 险
                    if (k == 2)
                    {
                        String YELQ = YuE + LiangQ + Grp;
                        SSRS YELQ_ssrs = exesql.execSQL(YELQ);
                        if (YELQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= YELQ_ssrs.getMaxRow(); i++)
                            {
                                ye_money2 = ye_money2 +
                                            Double.parseDouble(YELQ_ssrs.
                                        GetText(i, 2));
                                ye_count2 = ye_count2 +
                                            Integer.parseInt(
                                            YELQ_ssrs.GetText(i, 3));
                                tRiskCode = "保全退费给付（退余额）\\两全险\\";
                            }
                        }
                        tlisttable.setName("RISKJ2");
                    }
                    //保全退费给付（退余额）－定期 保险
                    if (k == 3)
                    {
                        String YEDQ = YuE + DingQ + Grp;
                        SSRS YEDQ_ssrs = exesql.execSQL(YEDQ);
                        if (YEDQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= YEDQ_ssrs.getMaxRow(); i++)
                            {
                                ye_money3 = ye_money3 +
                                            Double.parseDouble(YEDQ_ssrs.
                                        GetText(i, 2));
                                ye_count3 = ye_count3 +
                                            Integer.parseInt(
                                            YEDQ_ssrs.GetText(i, 3));
                                tRiskCode = "保全退费给付（退余额）\\定期保险\\";
                            }
                        }
                        tlisttable.setName("RISKJ3");
                    }
                    if (k == 4)
                    {
                        //保全退费给付（退余额）－年金 保险
                        String YENJ = YuE + NianJ + Grp;
                        SSRS YENJ_ssrs = exesql.execSQL(YENJ);
                        if (YENJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= YENJ_ssrs.getMaxRow(); i++)
                            {
                                ye_money4 = ye_money4 +
                                            Double.parseDouble(YENJ_ssrs.
                                        GetText(i, 2));
                                ye_count4 = ye_count4 +
                                            Integer.parseInt(
                                            YENJ_ssrs.GetText(i, 3));
                                tRiskCode = "保全退费给付（退余额）\\年金保险\\";
                            }
                        }
                        tlisttable.setName("RISKJ4");
                    }
                    if (k == 5)
                    {
                        //保全退费给付（退余额）－重大疾病保险
                        String YEZJ = YuE + ZhongJ + Grp;
                        SSRS YEZJ_ssrs = exesql.execSQL(YEZJ);
                        if (YEZJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= YEZJ_ssrs.getMaxRow(); i++)
                            {
                                ye_money5 = ye_money5 +
                                            Double.parseDouble(YEZJ_ssrs.
                                        GetText(i, 2));
                                ye_count5 = ye_count5 +
                                            Integer.parseInt(
                                            YEZJ_ssrs.GetText(i, 3));
                                tRiskCode = "保全退费给付（退余额）\\重大疾病保险\\";
                            }
                        }
                        tlisttable.setName("RISKJ5");
                    }
                    //保全退费给付（退余额）－意外伤害 保险
                    if (k == 6)
                    {
                        String YEYW = YuE + YiW + Grp;
                        SSRS YEYW_ssrs = exesql.execSQL(YEYW);
                        if (YEYW_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= YEYW_ssrs.getMaxRow(); i++)
                            {
                                ye_money6 = ye_money6 +
                                            Double.parseDouble(YEYW_ssrs.
                                        GetText(i, 2));
                                ye_count6 = ye_count6 +
                                            Integer.parseInt(
                                            YEYW_ssrs.GetText(i, 3));
                                tRiskCode = "保全退费给付（退余额）\\意外伤害\\";
                            }
                        }
                        tlisttable.setName("RISKJ6");
                    }
                    if (k == 7)
                    {
                        //保全退费给付（退余额）－健康 险
                        String YEJK = YuE + JianK + Grp;
                        SSRS YEJK_ssrs = exesql.execSQL(YEJK);
                        if (YEJK_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= YEJK_ssrs.getMaxRow(); i++)
                            {
                                ye_money7 = ye_money7 +
                                            Double.parseDouble(YEJK_ssrs.
                                        GetText(i, 2));
                                ye_count7 = ye_count7 +
                                            Integer.parseInt(
                                            YEJK_ssrs.GetText(i, 3));
                                tRiskCode = "保全退费给付（退余额）\\健康险\\";
                            }
                        }
                        tlisttable.setName("RISKJ7");
                    }
                    if (k == 8)
                    {
                        //保全退费给付（退余额）－其他 险
                        String YEQT = YuE + QiT + Grp;
                        SSRS YEQT_ssrs = exesql.execSQL(YEQT);
                        if (YEQT_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= YEQT_ssrs.getMaxRow(); i++)
                            {
                                ye_money8 = ye_money8 +
                                            Double.parseDouble(YEQT_ssrs.
                                        GetText(i, 2));
                                ye_count8 = ye_count8 +
                                            Integer.parseInt(
                                            YEQT_ssrs.GetText(i, 3));
                                tRiskCode = "保全退费给付（退余额）\\其他\\";
                            }
                        }
                        tlisttable.setName("RISKJ8");
                    }
                    //会计的三级明细
                    String strArr[] = null;
                    //抽象成函数
                    String tXsqdName = ""; //销售渠道
                    String tXzdlName = ""; //险种大类
                    String tSxqName = ""; //首续期
                    String tDqjName = ""; //趸期交

                    for (int i = 1; i <= 4; i++) //销售渠道
                    {
                        String tXsqdSql = getXsqdSql(i);
                        System.out.println(i + "当i 的值是" + i + "的时候");
                        if (k == 1)
                        {
                            tXsqdSql = YuE + ZhongS + tXsqdSql;
                        }
                        if (k == 2)
                        {
                            tXsqdSql = YuE + LiangQ + tXsqdSql;
                        }
                        if (k == 3)
                        {
                            tXsqdSql = YuE + DingQ + tXsqdSql;
                        }
                        if (k == 4)
                        {
                            tXsqdSql = YuE + NianJ + tXsqdSql;
                        }
                        if (k == 5)
                        {
                            tXsqdSql = YuE + ZhongJ + tXsqdSql;
                        }
                        if (k == 6)
                        {
                            tXsqdSql = YuE + YiW + tXsqdSql;
                        }
                        if (k == 7)
                        {
                            tXsqdSql = YuE + JianK + tXsqdSql;
                        }
                        if (k == 8)
                        {
                            tXsqdSql = YuE + QiT + tXsqdSql;
                        }
                        for (int j = 1; j <= 4; j++) //险种大类
                        {
                            String tXzdlSql = getXzdlSql(j);
                            tXzdlSql = tXsqdSql + tXzdlSql + Grp;
                            System.out.println("T的值是" + t);
                            System.out.println("K的值是" + k);
                            System.out.println("I的值是" + i);
                            System.out.println("J的值是" + j);
                            System.out.println(j + "查询的语句是");
                            SSRS tssrs = exesql.execSQL(tXzdlSql);
                            if (tssrs.getMaxRow() != 0)
                            {
                                for (int q = 1; q <= tssrs.MaxRow; q++)
                                {
                                    strArr = new String[7];
                                    String tRiskcode = tssrs.GetText(q, 1);
                                    LMRiskDB tLMRiskDB = new LMRiskDB();
                                    tLMRiskDB.setRiskCode(tssrs.GetText(q, 1));
                                    if (tLMRiskDB.getInfo())
                                    {
                                        strArr[0] = tRiskCode +
                                                tLMRiskDB.getRiskName();
                                    }
                                    strArr[1] = getXsqdName(i);
                                    strArr[2] = getXzdlName(j);
                                    strArr[5] = tssrs.GetText(q, 2);
                                    strArr[6] = tssrs.GetText(q, 3);
                                    tlisttable.add(strArr);
                                }
                            }
                        }
                    }
                    strArr = new String[7];
                    strArr[0] = "Risk";
                    strArr[1] = "Xsqd";
                    strArr[2] = "Xzdl";
                    strArr[3] = "Sxq";
                    strArr[4] = "Dqj";
                    strArr[5] = "Money";
                    strArr[6] = "Count";
                    xmlexport.addListTable(tlisttable, strArr);
                }
                //以上是k的结束
            } //end of t==10

            //保全退费给付（退费）
            if (t == 11)
            {
                //保全退费给付（退费）
                for (int k = 1; k <= 8; k++)
                {
                    ListTable tlisttable = new ListTable();
                    String tRiskCode = "";
                    //保全退费给付（退费）－终身 寿险
                    if (k == 1)
                    {
                        String TFZS = TuiFei + ZhongS + Grp;
                        SSRS TFZS_ssrs = exesql.execSQL(TFZS);
                        if (TFZS_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= TFZS_ssrs.getMaxRow(); i++)
                            {
                                tf_money1 = tf_money1 +
                                            Double.parseDouble(TFZS_ssrs.
                                        GetText(i, 2));
                                tf_count1 = tf_count1 +
                                            Integer.parseInt(
                                            TFZS_ssrs.GetText(i, 3));
                                tRiskCode = "保全退费给付（退费）\\终身寿险\\";
                            }
                        }
                        tlisttable.setName("RISKK1");
                    }
                    //保全退费给付（退费）－两全 险
                    if (k == 2)
                    {
                        String TFLQ = TuiFei + LiangQ + Grp;
                        SSRS TFLQ_ssrs = exesql.execSQL(TFLQ);
                        if (TFLQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= TFLQ_ssrs.getMaxRow(); i++)
                            {
                                tf_money2 = tf_money2 +
                                            Double.parseDouble(TFLQ_ssrs.
                                        GetText(i, 2));
                                tf_count2 = tf_count2 +
                                            Integer.parseInt(
                                            TFLQ_ssrs.GetText(i, 3));
                                tRiskCode = "保全退费给付（退费）\\两全险\\";
                            }
                        }
                        tlisttable.setName("RISKK2");
                    }
                    //保全退费给付（退费）－定期 保险
                    if (k == 3)
                    {
                        String TFDQ = TuiFei + DingQ + Grp;
                        SSRS TFDQ_ssrs = exesql.execSQL(TFDQ);
                        if (TFDQ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= TFDQ_ssrs.getMaxRow(); i++)
                            {
                                tf_money3 = tf_money3 +
                                            Double.parseDouble(TFDQ_ssrs.
                                        GetText(i, 2));
                                tf_count3 = tf_count3 +
                                            Integer.parseInt(
                                            TFDQ_ssrs.GetText(i, 3));
                                tRiskCode = "保全退费给付（退费）\\定期保险\\";
                            }
                        }
                        tlisttable.setName("RISKK3");
                    }
                    if (k == 4)
                    {
                        //保全退费给付（退费）－年金 保险
                        String TFNJ = TuiFei + NianJ + Grp;
                        SSRS TFNJ_ssrs = exesql.execSQL(TFNJ);
                        if (TFNJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= TFNJ_ssrs.getMaxRow(); i++)
                            {
                                tf_money4 = tf_money4 +
                                            Double.parseDouble(TFNJ_ssrs.
                                        GetText(i, 2));
                                tf_count4 = tf_count4 +
                                            Integer.parseInt(
                                            TFNJ_ssrs.GetText(i, 3));
                                tRiskCode = "保全退费给付（退费）\\年金保险\\";
                            }
                        }
                        tlisttable.setName("RISKK4");
                    }
                    if (k == 5)
                    {
                        //保全退费给付（退费）－重大疾病保险
                        String TFZJ = TuiFei + ZhongJ + Grp;
                        SSRS TFZJ_ssrs = exesql.execSQL(TFZJ);
                        if (TFZJ_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= TFZJ_ssrs.getMaxRow(); i++)
                            {
                                tf_money5 = tf_money5 +
                                            Double.parseDouble(TFZJ_ssrs.
                                        GetText(i, 2));
                                tf_count5 = tf_count5 +
                                            Integer.parseInt(
                                            TFZJ_ssrs.GetText(i, 3));
                                tRiskCode = "保全退费给付（退费）\\重大疾病保险\\";
                            }
                        }
                        tlisttable.setName("RISKK5");
                    }
                    //保全退费给付（退费）－意外伤害 保险
                    if (k == 6)
                    {
                        String TFYW = TuiFei + YiW + Grp;
                        SSRS TFYW_ssrs = exesql.execSQL(TFYW);
                        if (TFYW_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= TFYW_ssrs.getMaxRow(); i++)
                            {
                                tf_money6 = tf_money6 +
                                            Double.parseDouble(TFYW_ssrs.
                                        GetText(i, 2));
                                tf_count6 = tf_count6 +
                                            Integer.parseInt(
                                            TFYW_ssrs.GetText(i, 3));
                                tRiskCode = "保全退费给付（退费）\\意外伤害\\";
                            }
                        }
                        tlisttable.setName("RISKK6");
                    }
                    if (k == 7)
                    {
                        //保全退费给付（退费）－健康 险
                        String TFJK = TuiFei + JianK + Grp;
                        SSRS TFJK_ssrs = exesql.execSQL(TFJK);
                        if (TFJK_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= TFJK_ssrs.getMaxRow(); i++)
                            {
                                tf_money7 = tf_money7 +
                                            Double.parseDouble(TFJK_ssrs.
                                        GetText(i, 2));
                                tf_count7 = tf_count7 +
                                            Integer.parseInt(
                                            TFJK_ssrs.GetText(i, 3));
                                tRiskCode = "保全退费给付（退费）\\健康险\\";
                            }
                        }
                        tlisttable.setName("RISKK7");
                    }
                    if (k == 8)
                    {
                        //保全退费给付（退费）－其他 险
                        String TFQT = TuiFei + QiT + Grp;
                        SSRS TFQT_ssrs = exesql.execSQL(TFQT);
                        if (TFQT_ssrs.getMaxRow() != 0)
                        {
                            for (int i = 1; i <= TFQT_ssrs.getMaxRow(); i++)
                            {
                                tf_money8 = tf_money8 +
                                            Double.parseDouble(TFQT_ssrs.
                                        GetText(i, 2));
                                tf_count8 = tf_count8 +
                                            Integer.parseInt(
                                            TFQT_ssrs.GetText(i, 3));
                                tRiskCode = "保全退费给付（退费）\\其他\\";
                            }
                        }
                        tlisttable.setName("RISKK8");
                    }
                    //会计的三级明细
                    String strArr[] = null;
                    //抽象成函数
                    String tXsqdName = ""; //销售渠道
                    String tXzdlName = ""; //险种大类
                    String tSxqName = ""; //首续期
                    String tDqjName = ""; //趸期交

                    for (int i = 1; i <= 4; i++) //销售渠道
                    {
                        String tXsqdSql = getXsqdSql(i);
                        System.out.println(i + "当i 的值是" + i + "的时候");
                        if (k == 1)
                        {
                            tXsqdSql = TuiFei + ZhongS + tXsqdSql;
                        }
                        if (k == 2)
                        {
                            tXsqdSql = TuiFei + LiangQ + tXsqdSql;
                        }
                        if (k == 3)
                        {
                            tXsqdSql = TuiFei + DingQ + tXsqdSql;
                        }
                        if (k == 4)
                        {
                            tXsqdSql = TuiFei + NianJ + tXsqdSql;
                        }
                        if (k == 5)
                        {
                            tXsqdSql = TuiFei + ZhongJ + tXsqdSql;
                        }
                        if (k == 6)
                        {
                            tXsqdSql = TuiFei + YiW + tXsqdSql;
                        }
                        if (k == 7)
                        {
                            tXsqdSql = TuiFei + JianK + tXsqdSql;
                        }
                        if (k == 8)
                        {
                            tXsqdSql = TuiFei + QiT + tXsqdSql;
                        }
                        for (int j = 1; j <= 4; j++) //险种大类
                        {
                            String tXzdlSql = getXzdlSql(j);
                            tXzdlSql = tXsqdSql + tXzdlSql + Grp;
                            System.out.println("T的值是" + t);
                            System.out.println("K的值是" + k);
                            System.out.println("I的值是" + i);
                            System.out.println("J的值是" + j);
                            System.out.println(j + "查询的语句是");
                            SSRS tssrs = exesql.execSQL(tXzdlSql);
                            if (tssrs.getMaxRow() != 0)
                            {
                                for (int q = 1; q <= tssrs.MaxRow; q++)
                                {
                                    strArr = new String[7];
                                    String tRiskcode = tssrs.GetText(q, 1);
                                    LMRiskDB tLMRiskDB = new LMRiskDB();
                                    tLMRiskDB.setRiskCode(tssrs.GetText(q, 1));
                                    if (tLMRiskDB.getInfo())
                                    {
                                        strArr[0] = tRiskCode +
                                                tLMRiskDB.getRiskName();
                                    }
                                    strArr[1] = getXsqdName(i);
                                    strArr[2] = getXzdlName(j);
                                    strArr[5] = tssrs.GetText(q, 2);
                                    strArr[6] = tssrs.GetText(q, 3);
                                    tlisttable.add(strArr);
                                }
                            }
                        }
                    }
                    strArr = new String[7];
                    strArr[0] = "Risk";
                    strArr[1] = "Xsqd";
                    strArr[2] = "Xzdl";
                    strArr[3] = "Sxq";
                    strArr[4] = "Dqj";
                    strArr[5] = "Money";
                    strArr[6] = "Count";
                    xmlexport.addListTable(tlisttable, strArr);
                }
                //以上是k的结束
            }
        }
        //以上是t的结束
        //对.vts进行附值
        TextTag texttag = new TextTag();
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("ManageCom", mGlobalInput.ManageCom);
        //对满期进行附值
        mq_count = mq_count1 + mq_count2 + mq_count3 + mq_count4 + mq_count5 +
                   mq_count6 + mq_count7 + mq_count8;
        mq_money = mq_money1 + mq_money2 + mq_money3 + mq_money4 + mq_money5 +
                   mq_money6 + mq_money7 + mq_money8;
        texttag.add("CountA", mq_count);
        texttag.add("CountA1", mq_count1);
        texttag.add("CountA2", mq_count2);
        texttag.add("CountA3", mq_count3);
        texttag.add("CountA4", mq_count4);
        texttag.add("CountA5", mq_count5);
        texttag.add("CountA6", mq_count6);
        texttag.add("CountA7", mq_count7);
        texttag.add("CountA8", mq_count8);
        texttag.add("MoneyA", new DecimalFormat("0.00").format(mq_money));
        texttag.add("MoneyA1", new DecimalFormat("0.00").format(mq_money1));
        texttag.add("MoneyA2", new DecimalFormat("0.00").format(mq_money2));
        texttag.add("MoneyA3", new DecimalFormat("0.00").format(mq_money3));
        texttag.add("MoneyA4", new DecimalFormat("0.00").format(mq_money4));
        texttag.add("MoneyA5", new DecimalFormat("0.00").format(mq_money5));
        texttag.add("MoneyA6", new DecimalFormat("0.00").format(mq_money6));
        texttag.add("MoneyA7", new DecimalFormat("0.00").format(mq_money7));
        texttag.add("MoneyA8", new DecimalFormat("0.00").format(mq_money8));

        //对年金的附值
        nj_count = nj_count1 + nj_count2 + nj_count3 + nj_count4 + nj_count5 +
                   nj_count6 + nj_count7 + nj_count8;
        nj_money = nj_money1 + nj_money2 + nj_money3 + nj_money4 + nj_money5 +
                   nj_money6 + nj_money7 + nj_money8;
        texttag.add("CountB", nj_count);
        texttag.add("CountB1", nj_count1);
        texttag.add("CountB2", nj_count2);
        texttag.add("CountB3", nj_count3);
        texttag.add("CountB4", nj_count4);
        texttag.add("CountB5", nj_count5);
        texttag.add("CountB6", nj_count6);
        texttag.add("CountB7", nj_count7);
        texttag.add("CountB8", nj_count8);
        texttag.add("MoneyB", new DecimalFormat("0.00").format(nj_money));
        texttag.add("MoneyB1", new DecimalFormat("0.00").format(nj_money1));
        texttag.add("MoneyB2", new DecimalFormat("0.00").format(nj_money2));
        texttag.add("MoneyB3", new DecimalFormat("0.00").format(nj_money3));
        texttag.add("MoneyB4", new DecimalFormat("0.00").format(nj_money4));
        texttag.add("MoneyB5", new DecimalFormat("0.00").format(nj_money5));
        texttag.add("MoneyB6", new DecimalFormat("0.00").format(nj_money6));
        texttag.add("MoneyB7", new DecimalFormat("0.00").format(nj_money7));
        texttag.add("MoneyB8", new DecimalFormat("0.00").format(nj_money8));

        //对伤残的附值
        sc_count = sc_count1 + sc_count2 + sc_count3 + sc_count4 + sc_count5 +
                   sc_count6 + sc_count7 + sc_count8;
        sc_money = sc_money1 + sc_money2 + sc_money3 + sc_money4 + sc_money5 +
                   sc_money6 + sc_money7 + sc_money8;
        texttag.add("CountC", sc_count);
        texttag.add("CountC1", sc_count1);
        texttag.add("CountC2", sc_count2);
        texttag.add("CountC3", sc_count3);
        texttag.add("CountC4", sc_count4);
        texttag.add("CountC5", sc_count5);
        texttag.add("CountC6", sc_count6);
        texttag.add("CountC7", sc_count7);
        texttag.add("CountC8", sc_count8);
        texttag.add("MoneyC", new DecimalFormat("0.00").format(sc_money));
        texttag.add("MoneyC1", new DecimalFormat("0.00").format(sc_money1));
        texttag.add("MoneyC2", new DecimalFormat("0.00").format(sc_money2));
        texttag.add("MoneyC3", new DecimalFormat("0.00").format(sc_money3));
        texttag.add("MoneyC4", new DecimalFormat("0.00").format(sc_money4));
        texttag.add("MoneyC5", new DecimalFormat("0.00").format(sc_money5));
        texttag.add("MoneyC6", new DecimalFormat("0.00").format(sc_money6));
        texttag.add("MoneyC7", new DecimalFormat("0.00").format(sc_money7));
        texttag.add("MoneyC8", new DecimalFormat("0.00").format(sc_money8));

        //对医疗的附值
        yl_count = yl_count1 + yl_count2 + yl_count3 + yl_count4
                   + yl_count5 + yl_count6 + yl_count7 + yl_count8;
        yl_money = yl_money1 + yl_money2 + yl_money3 + yl_money4
                   + yl_money5 + yl_money6 + yl_money7 + yl_money8;
        texttag.add("CountD", yl_count);
        texttag.add("CountD1", yl_count1);
        texttag.add("CountD2", yl_count2);
        texttag.add("CountD3", yl_count3);
        texttag.add("CountD4", yl_count4);
        texttag.add("CountD5", yl_count5);
        texttag.add("CountD6", yl_count6);
        texttag.add("CountD7", yl_count7);
        texttag.add("CountD8", yl_count8);
        texttag.add("MoneyD", new DecimalFormat("0.00").format(yl_money));
        texttag.add("MoneyD1", new DecimalFormat("0.00").format(yl_money1));
        texttag.add("MoneyD2", new DecimalFormat("0.00").format(yl_money2));
        texttag.add("MoneyD3", new DecimalFormat("0.00").format(yl_money3));
        texttag.add("MoneyD4", new DecimalFormat("0.00").format(yl_money4));
        texttag.add("MoneyD5", new DecimalFormat("0.00").format(yl_money5));
        texttag.add("MoneyD6", new DecimalFormat("0.00").format(yl_money6));
        texttag.add("MoneyD7", new DecimalFormat("0.00").format(yl_money7));
        texttag.add("MoneyD8", new DecimalFormat("0.00").format(yl_money8));

        //对退保金给付的附值
        tb_count = tb_count1 + tb_count2 + tb_count3 + tb_count4
                   + tb_count5 + tb_count6 + tb_count7 + tb_count8;
        tb_money = tb_money1 + tb_money2 + tb_money3 + tb_money4
                   + tb_money5 + tb_money6 + tb_money7 + tb_money8;
        texttag.add("CountE", tb_count);
        texttag.add("CountE1", tb_count1);
        texttag.add("CountE2", tb_count2);
        texttag.add("CountE3", tb_count3);
        texttag.add("CountE4", tb_count4);
        texttag.add("CountE5", tb_count5);
        texttag.add("CountE6", tb_count6);
        texttag.add("CountE7", tb_count7);
        texttag.add("CountE8", tb_count8);

        texttag.add("MoneyE", new DecimalFormat("0.00").format(tb_money));
        texttag.add("MoneyE1", new DecimalFormat("0.00").format(tb_money1));
        texttag.add("MoneyE2", new DecimalFormat("0.00").format(tb_money2));
        texttag.add("MoneyE3", new DecimalFormat("0.00").format(tb_money3));
        texttag.add("MoneyE4", new DecimalFormat("0.00").format(tb_money4));
        texttag.add("MoneyE5", new DecimalFormat("0.00").format(tb_money5));
        texttag.add("MoneyE6", new DecimalFormat("0.00").format(tb_money6));
        texttag.add("MoneyE7", new DecimalFormat("0.00").format(tb_money7));
        texttag.add("MoneyE8", new DecimalFormat("0.00").format(tb_money8));

        //对死亡进行附值
        sw_count = sw_count1 + sw_count2 + sw_count3 + sw_count4
                   + sw_count5 + sw_count6 + sw_count7 + sw_count8;
        sw_money = sw_money1 + sw_money2 + sw_money3 + sw_money4
                   + sw_money5 + sw_money6 + sw_money7 + sw_money8;
        texttag.add("CountF", sw_count);
        texttag.add("CountF1", sw_count1);
        texttag.add("CountF2", sw_count2);
        texttag.add("CountF3", sw_count3);
        texttag.add("CountF4", sw_count4);
        texttag.add("CountF5", sw_count5);
        texttag.add("CountF6", sw_count6);
        texttag.add("CountF7", sw_count7);
        texttag.add("CountF8", sw_count8);

        texttag.add("MoneyF", new DecimalFormat("0.00").format(sw_money));
        texttag.add("MoneyF1", new DecimalFormat("0.00").format(sw_money1));
        texttag.add("MoneyF2", new DecimalFormat("0.00").format(sw_money2));
        texttag.add("MoneyF3", new DecimalFormat("0.00").format(sw_money3));
        texttag.add("MoneyF4", new DecimalFormat("0.00").format(sw_money4));
        texttag.add("MoneyF5", new DecimalFormat("0.00").format(sw_money5));
        texttag.add("MoneyF6", new DecimalFormat("0.00").format(sw_money6));
        texttag.add("MoneyF7", new DecimalFormat("0.00").format(sw_money7));
        texttag.add("MoneyF8", new DecimalFormat("0.00").format(sw_money8));

        //对暂收费退费进行附值
        zs_count = zs_count1 + zs_count2 + zs_count3 + zs_count4
                   + zs_count5 + zs_count6 + zs_count7 + zs_count8;
        zs_money = zs_money1 + zs_money2 + zs_money3 + zs_money4
                   + zs_money5 + zs_money6 + zs_money7 + zs_money8;
        texttag.add("CountG", zs_count);
        texttag.add("CountG1", zs_count1);
        texttag.add("CountG2", zs_count2);
        texttag.add("CountG3", zs_count3);
        texttag.add("CountG4", zs_count4);
        texttag.add("CountG5", zs_count5);
        texttag.add("CountG6", zs_count6);
        texttag.add("CountG7", zs_count7);
        texttag.add("CountG8", zs_count8);

        texttag.add("MoneyG", new DecimalFormat("0.00").format(zs_money));
        texttag.add("MoneyG1", new DecimalFormat("0.00").format(zs_money1));
        texttag.add("MoneyG2", new DecimalFormat("0.00").format(zs_money2));
        texttag.add("MoneyG3", new DecimalFormat("0.00").format(zs_money3));
        texttag.add("MoneyG4", new DecimalFormat("0.00").format(zs_money4));
        texttag.add("MoneyG5", new DecimalFormat("0.00").format(zs_money5));
        texttag.add("MoneyG6", new DecimalFormat("0.00").format(zs_money6));
        texttag.add("MoneyG7", new DecimalFormat("0.00").format(zs_money7));
        texttag.add("MoneyG8", new DecimalFormat("0.00").format(zs_money8));

        //对其他的附值
        qt_count = qt_count1 + qt_count2 + qt_count3 + qt_count4 + qt_count5 +
                   qt_count6 + qt_count7 + qt_count8;
        qt_money = qt_money1 + qt_money2 + qt_money3 + qt_money4 + qt_money5 +
                   qt_money6 + qt_money7 + qt_money8;
        texttag.add("CountH", qt_count);
        texttag.add("CountH1", qt_count1);
        texttag.add("CountH2", qt_count2);
        texttag.add("CountH3", qt_count3);
        texttag.add("CountH4", qt_count4);
        texttag.add("CountH5", qt_count5);
        texttag.add("CountH6", qt_count6);
        texttag.add("CountH7", qt_count7);
        texttag.add("CountH8", qt_count8);
        texttag.add("MoneyH", new DecimalFormat("0.00").format(qt_money));
        texttag.add("MoneyH1", new DecimalFormat("0.00").format(qt_money1));
        texttag.add("MoneyH2", new DecimalFormat("0.00").format(qt_money2));
        texttag.add("MoneyH3", new DecimalFormat("0.00").format(qt_money3));
        texttag.add("MoneyH4", new DecimalFormat("0.00").format(qt_money4));
        texttag.add("MoneyH5", new DecimalFormat("0.00").format(qt_money5));
        texttag.add("MoneyH6", new DecimalFormat("0.00").format(qt_money6));
        texttag.add("MoneyH7", new DecimalFormat("0.00").format(qt_money7));
        texttag.add("MoneyH8", new DecimalFormat("0.00").format(qt_money8));

        //对红利的附值
        hl_count = hl_count1 + hl_count2 + hl_count3 + hl_count4
                   + hl_count5 + hl_count6 + hl_count7 + hl_count8;
        hl_money = hl_money1 + hl_money2 + hl_money3 + hl_money4
                   + hl_money5 + hl_money6 + hl_money7 + hl_money8;
        texttag.add("CountI", hl_count);
        texttag.add("CountI1", hl_count1);
        texttag.add("CountI2", hl_count2);
        texttag.add("CountI3", hl_count3);
        texttag.add("CountI4", hl_count4);
        texttag.add("CountI5", hl_count5);
        texttag.add("CountI6", hl_count6);
        texttag.add("CountI7", hl_count7);
        texttag.add("CountI8", hl_count8);

        texttag.add("MoneyI", new DecimalFormat("0.00").format(hl_money));
        texttag.add("MoneyI1", new DecimalFormat("0.00").format(hl_money1));
        texttag.add("MoneyI2", new DecimalFormat("0.00").format(hl_money2));
        texttag.add("MoneyI3", new DecimalFormat("0.00").format(hl_money3));
        texttag.add("MoneyI4", new DecimalFormat("0.00").format(hl_money4));
        texttag.add("MoneyI5", new DecimalFormat("0.00").format(hl_money5));
        texttag.add("MoneyI6", new DecimalFormat("0.00").format(hl_money6));
        texttag.add("MoneyI7", new DecimalFormat("0.00").format(hl_money7));
        texttag.add("MoneyI8", new DecimalFormat("0.00").format(hl_money8));

        //对保全退费－退余额进行附值
        ye_count = ye_count1 + ye_count2 + ye_count3 + ye_count4
                   + ye_count5 + ye_count6 + ye_count7 + ye_count8;
        ye_money = ye_money1 + ye_money2 + ye_money3 + ye_money4
                   + ye_money5 + ye_money6 + ye_money7 + ye_money8;
        texttag.add("CountJ", ye_count);
        texttag.add("CountJ1", ye_count1);
        texttag.add("CountJ2", ye_count2);
        texttag.add("CountJ3", ye_count3);
        texttag.add("CountJ4", ye_count4);
        texttag.add("CountJ5", ye_count5);
        texttag.add("CountJ6", ye_count6);
        texttag.add("CountJ7", ye_count7);
        texttag.add("CountJ8", ye_count8);

        texttag.add("MoneyJ", new DecimalFormat("0.00").format(ye_money));
        texttag.add("MoneyJ1", new DecimalFormat("0.00").format(ye_money1));
        texttag.add("MoneyJ2", new DecimalFormat("0.00").format(ye_money2));
        texttag.add("MoneyJ3", new DecimalFormat("0.00").format(ye_money3));
        texttag.add("MoneyJ4", new DecimalFormat("0.00").format(ye_money4));
        texttag.add("MoneyJ5", new DecimalFormat("0.00").format(ye_money5));
        texttag.add("MoneyJ6", new DecimalFormat("0.00").format(ye_money6));
        texttag.add("MoneyJ7", new DecimalFormat("0.00").format(ye_money7));
        texttag.add("MoneyJ8", new DecimalFormat("0.00").format(ye_money8));

        //对保全退费－退费进行附值
        tf_count = tf_count1 + tf_count2 + tf_count3 + tf_count4
                   + tf_count5 + tf_count6 + tf_count7 + tf_count8;
        tf_money = tf_money1 + tf_money2 + tf_money3 + tf_money4
                   + tf_money5 + tf_money6 + tf_money7 + tf_money8;
        texttag.add("CountK", tf_count);
        texttag.add("CountK1", tf_count1);
        texttag.add("CountK2", tf_count2);
        texttag.add("CountK3", tf_count3);
        texttag.add("CountK4", tf_count4);
        texttag.add("CountK5", tf_count5);
        texttag.add("CountK6", tf_count6);
        texttag.add("CountK7", tf_count7);
        texttag.add("CountK8", tf_count8);

        texttag.add("MoneyK", new DecimalFormat("0.00").format(tf_money));
        texttag.add("MoneyK1", new DecimalFormat("0.00").format(tf_money1));
        texttag.add("MoneyK2", new DecimalFormat("0.00").format(tf_money2));
        texttag.add("MoneyK3", new DecimalFormat("0.00").format(tf_money3));
        texttag.add("MoneyK4", new DecimalFormat("0.00").format(tf_money4));
        texttag.add("MoneyK5", new DecimalFormat("0.00").format(tf_money5));
        texttag.add("MoneyK6", new DecimalFormat("0.00").format(tf_money6));
        texttag.add("MoneyK7", new DecimalFormat("0.00").format(tf_money7));
        texttag.add("MoneyK8", new DecimalFormat("0.00").format(tf_money8));

        Count = mq_count + nj_count + sc_count + yl_count + tb_count + sw_count +
                zs_count
                + qt_count + hl_count + ye_count + tf_count;
        Money = mq_money + nj_money + sc_money + yl_money + tb_money + sw_money +
                zs_money
                + qt_money + hl_money + ye_money + tf_money;

        texttag.add("Count", Count);
        texttag.add("Money", new DecimalFormat("0.00").format(Money));
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        //xmlexport.outputDocumentToFile("e:\\","财务应付日结清单");
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

    //销售渠道
    private String getXsqdSql(int tIndex)
    {
        String tsqladd = "";
        switch (tIndex)
        {
            case 1: //团体险
                tsqladd = " and ( polno in (select polno from lcpol where SaleChnl='01') or polno in ( select polno from lbpol where SaleChnl='01') )";
                break;
            case 2: //个人营销
                tsqladd = " and ( polno in (select polno from lcpol where SaleChnl='02') or polno in ( select polno from lbpol where SaleChnl='02') )";
                break;
            case 3: //银行代理
                tsqladd = " and ( polno in (select polno from lcpol where SaleChnl='03')  or polno in ( select polno from lbpol where SaleChnl='03') )";
                break;
            case 4: //兼业代理
                tsqladd = " and ( polno in (select polno from lcpol where SaleChnl='04')  or polno in ( select polno from lbpol where SaleChnl='04') )";
                break;
        }
        return tsqladd;
    }

    //销售渠道团体
    private String getXsqdSql_1(int tIndex)
    {
        String tsqladd = "";
        switch (tIndex)
        {
            case 1: //团体险
                tsqladd = " and ( Grppolno in (select Grppolno from LCGrpPol where SaleChnl='01') or Grppolno in ( select Grppolno from LBGrpPol where SaleChnl='01') )";
                break;
            case 2: //个人营销
                tsqladd = " and ( Grppolno in (select Grppolno from LCGrpPol where SaleChnl='02') or Grppolno in ( select Grppolno from LBGrpPol where SaleChnl='02') )";
                break;

            case 3: //银行代理
                tsqladd = " and ( Grppolno in (select Grppolno from LCGrpPol where SaleChnl='03') or Grppolno in ( select Grppolno from LBGrpPol where SaleChnl='03') )";
                break;
            case 4: //兼业代理
                tsqladd = " and ( Grppolno in (select Grppolno from LCGrpPol where SaleChnl='04') or Grppolno in ( select Grppolno from LBGrpPol where SaleChnl='04') )";
                break;
        }
        return tsqladd;
    }


//险种大类
    private String getXzdlSql(int tIndex)
    {
        String tsqladd = "";
        switch (tIndex)
        {
            case 1: //传统险
                tsqladd =
                        " and RiskCode in (select RiskCode from LMRiskApp where RiskType3='1') ";
                break;
            case 2: //分红险
                tsqladd =
                        " and RiskCode in (select RiskCode from LMRiskApp where RiskType3='2') ";
                break;
            case 3: //投连险
                tsqladd =
                        " and RiskCode in (select RiskCode from LMRiskApp where RiskType3='3') ";
                break;
            case 4: //万能险
                tsqladd =
                        " and RiskCode in (select RiskCode from LMRiskApp where RiskType3='4') ";
                break;
        }
        return tsqladd;
    }

//销售渠道名称
    private String getXsqdName(int tIndex)
    {
        String tsqladd = "";
        switch (tIndex)
        {
            case 1: //团体险
                tsqladd = "团体险";
                break;
            case 2: //个人营销
                tsqladd = "个人营销";
                break;
            case 3: //银行代理
                tsqladd = "银行代理";
                break;
            case 4: //兼业代理
                tsqladd = "兼业代理";
                break;
        }
        return tsqladd;
    }

//险种大类名称
    private String getXzdlName(int tIndex)
    {
        String tsqladd = "";
        switch (tIndex)
        {
            case 1: //传统险
                tsqladd = "传统险";
                break;
            case 2: //分红险
                tsqladd = "分红险";
                break;
            case 3: //投连险
                tsqladd = "投连险";
                break;
            case 4: //万能险
                tsqladd = "万能险";
                break;
        }
        return tsqladd;
    }
}