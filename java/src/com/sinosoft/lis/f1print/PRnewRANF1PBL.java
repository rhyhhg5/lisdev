package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author kevin
 * @version 1.0
 */

import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCUWMasterDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCUWMasterSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PRnewRANF1PBL extends NoticeF1PBO
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private double fPremSum = 0;
    private double fPremAddSum = 0;
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();

    public PRnewRANF1PBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        try
        {
            if (!cOperate.equals("CONFIRM") &&
                !cOperate.equals("PRINT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }
            if (cOperate.equals("CONFIRM"))
            {
                mResult.clear();

                // 准备所有要打印的数据
                getPrintData();
            }
            else if (cOperate.equals("PRINT"))
            {
                if (!saveData(cInputData))
                {
                    return false;
                }
            }

            return true;

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", ex.toString());
            return false;
        }
    }

    public static void main(String[] args)
    {

    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                "LOPRTManagerSchema", 0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "RANF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void getPrintData() throws Exception
    {
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();

        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema); //将prtseq传给DB，目的查找所有相关信息，然后还要返回给schema
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            throw new Exception("在取得打印队列中数据时发生错误");
        }

        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
        if (mLOPRTManagerSchema.getStateFlag() == null)
        {
            buildError("getprintData", "无效的打印状态");
        }
        else if (!mLOPRTManagerSchema.getStateFlag().equals("0"))
        {
            buildError("getprintData", "该打印请求不是在请求状态");
        }

        XmlExport xmlExport = new XmlExport();

        ListTable listTable = new ListTable();
        LCPolDB tLCPolDB = null;

        // 主险保单号的信息，从实付表的相关数据中任意取一条
        String tPolNo = mLOPRTManagerSchema.getOtherNo();
        // 将得到的数据放到xmlExport对象中
        TextTag texttag = new TextTag();
        xmlExport.createDocument("PRnewRAN.vts", "");

        tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(tPolNo);

        // 如果strMainPolNo没有被正确地设置，则查询不出有用的数据，但是不报错。
        tLCPolDB.getInfo();

        LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();

        String strSQL = "SELECT * FROM LCUWMaster WHERE PolNo = '" + tPolNo +
                        "'  order by makedate ,maketime desc";

        LCUWMasterSet tLCUWMasterSet = tLCUWMasterDB.executeQuery(strSQL);

        if (tLCUWMasterSet == null || tLCUWMasterSet.size() < 1)
        {
            mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
            throw new Exception("在取得核保意见时出现错误");
        }

        String strUWError = "";
        String strUWCode = "";

        if (tLCUWMasterSet.size() >= 1)
        {
            strUWError = tLCUWMasterSet.get(1).getUWIdea();
            strUWCode = tLCUWMasterSet.get(1).getOperator();
        }

        texttag.add("AppntName", tLCPolDB.getAppntName());
        texttag.add("PolNo", tLCPolDB.getPolNo());
        texttag.add("PrtNo", tLCPolDB.getPrtNo());
        texttag.add("AgentName", getAgentName(tLCPolDB.getAgentCode()));
        texttag.add("AgentCode", tLCPolDB.getAgentCode());
        texttag.add("ManageCom", getComName(tLCPolDB.getManageCom()));
        texttag.add("InsuredName", tLCPolDB.getInsuredName());
        texttag.add("PrtSeq", mLOPRTManagerSchema.getPrtSeq());
        texttag.add("RiskName", getRiskName(tLCPolDB.getRiskCode()));
        texttag.add("UWError", strUWError);
        texttag.add("UWCode", strUWCode);

        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
        texttag.add("Today", df.format(new Date()));

        xmlExport.addTextTag(texttag);
        mResult.clear();
        mResult.addElement(xmlExport);

    }

    private boolean saveData(VData mInputData)
    {
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("saveData", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setDoneDate(CurrentDate);
        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
        tLOPRTManagerDB.setSchema((LOPRTManagerSchema) mInputData.
                                  getObjectByObjectName("LOPRTManagerSchema", 0));

        mResult.add(mLOPRTManagerSchema);
        mResult.add(tLOPRTManagerDB);
        PRnewRANF1PBLS tPRnewRANF1PBLS = new PRnewRANF1PBLS();
        tPRnewRANF1PBLS.submitData(mResult, mOperate);
        if (tPRnewRANF1PBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tPRnewRANF1PBLS.mErrors);
            buildError("saveData", "提交数据库出错！");
            return false;
        }
        return true;
    }


}