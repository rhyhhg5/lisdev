/*
 * <p>ClassName: LARiskContSave1UI  </p>
 * <p>Description: LARiskContSave1UI 类文件 </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft </p>
 * @Database:
 * @CreateDate：2007-08-06
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LARiskContSave1UI {
    public LARiskContSave1UI() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

   public CErrors mErrors = new CErrors();
   private VData mInputData = new VData();
   /** 数据操作字符串 */
   private String mOperate;
   private VData mResult = new VData();


   public boolean submitData(VData cInputData,String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData=(VData)cInputData.clone();
       System.out.println("String LARiskContSave1UI ...");
        LARiskContSave1BL  tLARiskContSave1BL = new LARiskContSave1BL();
        if (!tLARiskContSave1BL.submitData(mInputData, mOperate))
        {
            if (tLARiskContSave1BL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tLARiskContSave1BL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "LARiskContSave1UI发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult =tLARiskContSave1BL.getResult();
            return true;
        }
    }

    /**
  * 追加错误信息
  * @param szFunc String
  * @param szErrMsg String
  */
 private void buildError(String szFunc, String szErrMsg)
 {
     CError cError = new CError();

     cError.moduleName = "LARiskContSave1UI";
     cError.functionName = szFunc;
     cError.errorMessage = szErrMsg;
     this.mErrors.addOneError(cError);
 }


 /**
  * 取得返回处理过的结果
  * @return VData
  */
 public VData getResult() {
     return this.mResult;
 }

    private void jbInit() throws Exception {
    }


}
