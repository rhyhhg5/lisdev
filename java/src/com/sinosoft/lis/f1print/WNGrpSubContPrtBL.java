package com.sinosoft.lis.f1print;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * 团单分单打印
 * 
 * @author LY
 *
 */
public class WNGrpSubContPrtBL implements PrintService
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mOperate = "";

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LCGrpContSchema mGrpContInfo = null;

    private LCContSchema mGrpSubContInfo = null;

    private LCInsuredSchema mGrpSCInsuInfo = null;

    private LCPolSet mGrpSCInsuPolInfo = null;

    private String mLoadFlag;

    private String mflag = null;

    public WNGrpSubContPrtBL()
    {
    }

    /**
     * 传输数据的公共方法
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        try
        {
            // 得到外部传入的数据，将数据备份到本类中（不管有没有operate,都要执行这一部）
            if (!getInputData(cInputData))
            {
                return false;
            }

            mResult.clear();

            // 准备所有要打印的数据
            if (!getPrintData())
            {
                return false;
            }

            return true;

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", ex.toString());
            return false;
        }
    }

    private String getDate(String cDate)
    {
        Date tDate = new FDate().getDate(cDate);
        if (tDate == null)
        {
            System.out.println("[" + cDate + "]日期转换失败");
            return cDate;
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
        return df.format(tDate);
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.getObjectByObjectName("LOPRTManagerSchema", 0));
        if (!dealPrintManager())
        {
            return false;
        }

        TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (tTransferData != null)
        {
            this.mLoadFlag = (String) tTransferData.getValueByName("LoadFlag");
        }

        mflag = mOperate;

        return true;
    }

    //得到返回值
    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CCSFirstPayBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    // 准备所有要打印的数据
    private boolean getPrintData()
    {
        XmlExport xmlExport = new XmlExport();

        xmlExport.createDocument("WNGrpSubContPrint", "");

        // 预设pdf接口节点。
        if (!dealPdfBaseInfo(xmlExport))
        {
            return false;
        }
        // ------------------------------

        // 处理团体分单基本信息。
        if (!dealGrpSubContBaseInfo(xmlExport))
        {
            return false;
        }
        // ------------------------------

        // 处理团体分单基本信息。
        if (!getInsuAccInfo(xmlExport))
        {
            return false;
        }
        // ------------------------------

        //xmlExport.outputDocumentToFile("e:\\", "CCSFirstPay");

        mResult.clear();
        mResult.addElement(xmlExport);

        // 处理打印轨迹。
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
        mLOPRTManagerSchema.setPrintTimes(mLOPRTManagerSchema.getPrintTimes() + 1);

        mResult.add(mLOPRTManagerSchema);
        // ------------------------------

        return true;
    }

    /**
     * 获取团单信息
     * @param cGrpContNo
     * @return
     */
    private boolean loadGrpContInfo(String cGrpContNo)
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(cGrpContNo);
        if (!tLCGrpContDB.getInfo())
        {
            return false;
        }
        mGrpContInfo = tLCGrpContDB.getSchema();
        return true;
    }

    /**
     * 获取分单数据。
     * @param cContNo
     * @return
     */
    private boolean loadGrpSubContInfo(String cContNo)
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(cContNo);
        if (!tLCContDB.getInfo())
        {
            return false;
        }
        mGrpSubContInfo = tLCContDB.getSchema();
        return true;
    }

    /**
     * 产生PDF打印相关基本信息。
     * @param cTextTag
     * @return
     */
    private boolean dealPdfBaseInfo(XmlExport cXmlExport)
    {
        TextTag tTmpTextTag = new TextTag();

        // 单证类型标志。
        tTmpTextTag.add("JetFormType", "GS003");
        // -------------------------------

        // 四位管理机构代码。
        String sqlusercom = "select comcode from lduser where usercode='" + mGlobalInput.Operator + "' ";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") || comcode.equals("86000000"))
        {
            comcode = "86";
        }
        else if (comcode.length() >= 4)
        {
            comcode = comcode.substring(0, 4);
        }
        else
        {
            buildError("dealGrpContInfo", "操作员机构查询出错!");
            return false;
        }
        String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='" + comcode + "' ";
        String printcode = new ExeSQL().getOneValue(printcom);

        tTmpTextTag.add("ManageComLength4", printcode);
        // -------------------------------

        // 客户端IP。
        tTmpTextTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        // -------------------------------

        // 预览标志。
        if (mflag.equals("batch"))
        {
            tTmpTextTag.add("previewflag", "0");
        }
        else
        {
            tTmpTextTag.add("previewflag", "1");
        }
        // -------------------------------

        cXmlExport.addTextTag(tTmpTextTag);

        return true;
    }

    /**
     * 处理团体保单层信息。
     * @param cTextTag
     * @return
     */
    private boolean dealGrpSubContBaseInfo(XmlExport cXmlExport)
    {
        TextTag tTmpTextTag = new TextTag();

        tTmpTextTag.add("GrpContNo", mGrpContInfo.getGrpContNo());
        tTmpTextTag.add("PrtNo", mGrpContInfo.getPrtNo());
        tTmpTextTag.add("AppntName", mGrpContInfo.getGrpName());

        tTmpTextTag.add("CValidate", getDate(mGrpSubContInfo.getCValiDate()));

        tTmpTextTag.add("ManageCom", mGrpContInfo.getManageCom());
        tTmpTextTag.add("ManageComName", chgGrpManageCom(mGrpContInfo.getManageCom()));

        // 被保人当期保费
        String tSumPrem = getInsuPrem();
        tTmpTextTag.add("SumPrem", tSumPrem);
        // --------------------

        tTmpTextTag.add("Remark", StrTool.cTrim(mGrpContInfo.getRemark()).equals("") ? "无" : mGrpContInfo.getRemark());

        tTmpTextTag.add("SignDate", mGrpSubContInfo.getSignDate());

        String tAgentCode = mGrpContInfo.getAgentCode();
        String tTmpSql = " select laa.Name, labg.BranchAttr, labg.Name,laa.groupagentcode " + " from LAAgent laa "
                + " inner join LABranchGroup labg on labg.AgentGroup = laa.AgentGroup " + " where laa.AgentCode = '"
                + tAgentCode + "' ";
        SSRS tSSRS = new SSRS();
        tSSRS = new ExeSQL().execSQL(tTmpSql);
        if (tSSRS == null || tSSRS.getMaxRow() == 0)
        {
            buildError("dealGrpSubContBaseInfo", "未找到业务员相关信息。");
            return false;
        }
        String tAgentName = tSSRS.GetText(1, 1);
        String tBranchAttr = tSSRS.GetText(1, 2);
        String tBranchAttrName = tSSRS.GetText(1, 3);
        String tGroupAgentCode = tSSRS.GetText(1, 4);

        tTmpTextTag.add("SaleChnl", mGrpContInfo.getSaleChnl());
        tTmpTextTag.add("AgentCom", mGrpContInfo.getAgentCom());
        tTmpTextTag.add("AgentCode", tGroupAgentCode);
        tTmpTextTag.add("AgentName", tAgentName);
        tTmpTextTag.add("AgentGroup", mGrpContInfo.getAgentGroup());
        tTmpTextTag.add("BranchAttr", tBranchAttr);
        tTmpTextTag.add("BranchAttrName", tBranchAttrName);

        tTmpTextTag.add("PayIntv", chgGrpPayIntv(String.valueOf(mGrpContInfo.getPayIntv())));

        tTmpTextTag.add("ContNo", mGrpSCInsuInfo.getContNo());
        tTmpTextTag.add("InsuredNo", mGrpSCInsuInfo.getInsuredNo());
        tTmpTextTag.add("InsuredName", mGrpSCInsuInfo.getName());
        tTmpTextTag.add("InsuredSex", mGrpSCInsuInfo.getSex());
        tTmpTextTag.add("InsuredBirthday", mGrpSCInsuInfo.getBirthday());
        tTmpTextTag.add("InsuredIdType", getIDTypeName(mGrpSCInsuInfo.getIDType()));
        tTmpTextTag.add("InsuredIdNo", mGrpSCInsuInfo.getIDNo());

        // 获取缴费期间，但该方式所取数据不准。
        String tPayPeriod = getPayPeriod();
        tTmpTextTag.add("PayPeriod", tPayPeriod);
        // ---------------------

        tTmpTextTag.add("Today", getDate(PubFun.getCurrentDate()));

        cXmlExport.addTextTag(tTmpTextTag);

        return true;
    }

    /**
     * 获取现金价值表信息
     * @param cXmlDataset XMLDataset
     * @param cLCPolSet LCPolSet
     * @return boolean
     * @throws Exception
     */
    private boolean getInsuAccInfo(XmlExport cXmlExport)
    {
        LCPolSet tGSCPol = mGrpSCInsuPolInfo;

        //特殊标签
        // XMLDataList tXMLDataList = new XMLDataList();
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String tStrSql = null;

        //循环处理险种下的现金价值信息
        for (int i = 1; i <= tGSCPol.size(); i++)
        {
            LCPolSchema tPolInfo = tGSCPol.get(i);

            String tRiskCode = tPolInfo.getRiskCode();

            //若是绑定型附加险，则不需要打印现金价值表
            if (!isGrpULIRisk(tRiskCode))
            {
                continue;
            }

            TextTag tTmpTextTag = new TextTag();

            // 险种信息
            LMRiskAppDB tRiskInfo = new LMRiskAppDB();
            tRiskInfo.setRiskCode(tRiskCode);
            if (!tRiskInfo.getInfo())
            {
                buildError("getInsuAccInfo", "险种信息查询出错!");
                return false;
            }

            String tRiskName = tRiskInfo.getRiskName();
            tTmpTextTag.add("RiskCode", tRiskCode);
            tTmpTextTag.add("RiskName", tRiskName);
            // --------------------

            // 帐户相关信息
            tStrSql = "";
            tStrSql += " select ";
            tStrSql += " (select b.getdutyname from lcget a,lmdutygetalive b where a.contno=c.contno and a.insuredno=c.insuredno and b.getdutycode=a.getdutycode  fetch first 1 rows only) as GetDutyName, ";
            tStrSql += " (select a.GetYear from lcduty a where polno in (select Polno from lcpol where contno=c.contno and insuredno=c.insuredno) fetch first 1 rows only) as GetYearName, ";
            tStrSql += " (select nvl(sum(l.Money), 0) from LCInsureAccTrace l,lmdutypay m where l.polno in(select Polno from lcpol where contno=c.contno and insuredno=c.insuredno) and l.payplancode=m.payplancode and m.AccPayClass in('5','6')) as Selfprem, ";
            tStrSql += " (select nvl(sum(l.Money), 0) from LCInsureAccTrace l,lmdutypay m where l.polno in(select Polno from lcpol where contno=c.contno and insuredno=c.insuredno) and l.payplancode=m.payplancode and m.AccPayClass ='4') as Grpprem, ";
            tStrSql += " (select nvl(sum(l.Money), 0) from LCInsureAccTrace l,lmdutypay m where l.OtherType = '1' and l.polno in(select Polno from lcpol where contno=c.contno and insuredno=c.insuredno) and l.payplancode=m.payplancode and m.AccPayClass in('4', '5','6')) as InsuAccprem, ";
            tStrSql += " (select nvl(sum(l.Money), 0) from LCInsureAccTrace l,lmdutypay m where l.polno in (select Polno from lcpol where contno=c.contno and insuredno=c.insuredno) and l.payplancode=m.payplancode and m.AccPayClass in('4', '5','6')) as InsuAccBala, ";
            tStrSql += " '' ";
            tStrSql += " from lcinsured c ";
            tStrSql += " where grpcontno= '" + tPolInfo.getGrpContNo() + "' ";
            tStrSql += " and InsuredNo = '" + tPolInfo.getInsuredNo() + "' ";
            tStrSql += " and exists (select 1 from lcpol where contno = c.contno and poltypeflag = '0') ";

            tSSRS = tExeSQL.execSQL(tStrSql);

            if (tSSRS == null || tSSRS.getMaxRow() != 1)
            {
                buildError("getInsuAccInfo", "帐户相关信息查询出错!");
                return false;
            }

            String[] tInsuAccInfos = tSSRS.getRowData(1); // 由于此sql应该仅有一条，因此，直接取首行

            String tGetDutyName = tInsuAccInfos[0];
            String tGetYearName = tInsuAccInfos[1];
            String tSelfprem = tInsuAccInfos[2];
            String tGrpprem = tInsuAccInfos[3];
            String tInsuAccprem = tInsuAccInfos[4];
            String tInsuAccBala = tInsuAccInfos[5];

            tTmpTextTag.add("InsuAccGetDutyKind", tGetDutyName);
            tTmpTextTag.add("InsuAccGetYearInfo", tGetYearName);
            tTmpTextTag.add("InsuAccPersonPrem", format(Double.parseDouble(tSelfprem)));
            tTmpTextTag.add("InsuAccAppntPrem", format(Double.parseDouble(tGrpprem)));
            tTmpTextTag.add("InsuAccPrem", format(Double.parseDouble(tInsuAccprem)));
            tTmpTextTag.add("InsuAccBala", format(Double.parseDouble(tInsuAccBala)));
            // --------------------

            String mSqlInit = " select distinct nvl(l.FeeValue, 0) from LCGrpFee l,lmdutypay m where l.payplancode=m.payplancode and m.AccPayClass in('4', '5','6') "
                    + " and l.GrpContNo = '" + tPolInfo.getGrpContNo() + "' ";
            String tFeeRate = tExeSQL.getOneValue(mSqlInit);
            if (!"".equals(tFeeRate))
            {
                tTmpTextTag.add("InsuAccFirstFeeRate", tFeeRate);
            }
            else
            {
                tTmpTextTag.add("InsuAccFirstFeeRate", "");
            }

            String mSqlManage = "select nvl(feevalue, 0) from lcgrpfee where feecode in (select feecode from lmriskfee where FeeItemType='03') and grpcontno='"
                    + tPolInfo.getGrpContNo() + "'";
            String tManageFee = tExeSQL.getOneValue(mSqlManage);
            if (!"".equals(tManageFee))
            {
                tTmpTextTag.add("InsuAccGLFeeValue", tManageFee);
            }
            else
            {
                tTmpTextTag.add("InsuAccGLFeeValue", "");
            }

            cXmlExport.addTextTag(tTmpTextTag);

        }

        return true;
    }

    /**
     * 获取被保人险种信息。
     * @return
     */
    private boolean loadGrpSubInsuPol(String cContNo, String cInsuredNo)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        String tStrSql = " select * from LCPol lcp where lcp.ContNo = '" + mGrpSubContInfo.getContNo() + "' "
                + " and lcp.InsuredNo = '" + mGrpSCInsuInfo.getInsuredNo() + "' ";
        mGrpSCInsuPolInfo = tLCPolDB.executeQuery(tStrSql);
        if (mGrpSCInsuPolInfo == null || mGrpSCInsuPolInfo.size() == 0)
        {
            return false;
        }
        return true;
    }

    /**
     * 获取被保人数据。
     * @param cContNo
     * @return
     */
    private boolean loadGrpSCInsuInfo(String cContNo, String cInsuredNo)
    {
        LCInsuredDB tInsuDB = new LCInsuredDB();
        tInsuDB.setContNo(cContNo);
        tInsuDB.setInsuredNo(cInsuredNo);
        if (!tInsuDB.getInfo())
        {
            return false;
        }
        mGrpSCInsuInfo = tInsuDB.getSchema();
        return true;
    }

    /**
     *
     * @return LOPRTManagerDB
     * @throws Exception
     */
    private boolean dealPrintManager()
    {
        String tContNo = mLOPRTManagerSchema.getStandbyFlag1();

        if (!loadGrpSubContInfo(tContNo))
        {
            buildError("dealPrintManager", "获取分单数据失败！");
            return false;
        }

        String tGrpContNo = mGrpSubContInfo.getGrpContNo();

        if (!loadGrpContInfo(tGrpContNo))
        {
            buildError("dealPrintManager", "获取团单数据失败！");
            return false;
        }

        String tInsuredNo = mLOPRTManagerSchema.getStandbyFlag2();
        if (!loadGrpSCInsuInfo(tContNo, tInsuredNo))
        {
            buildError("dealPrintManager", "获取被保人数据失败！");
            return false;
        }

        if (!loadGrpSubInsuPol(tContNo, tInsuredNo))
        {
            buildError("dealPrintManager", "获取被保人险种数据失败！");
            return false;
        }

        String tStrPrtSeq = PubFun1.CreateMaxNo("PRTSEQNO", null);
        if (tStrPrtSeq == null || tStrPrtSeq.equals(""))
        {
            buildError("dealPrintManager", "生成打印流水号失败。");
            return false;
        }

        mLOPRTManagerSchema.setPrtSeq(tStrPrtSeq);

        mLOPRTManagerSchema.setOtherNoType("16");

        mLOPRTManagerSchema.setReqCom(mGlobalInput.ManageCom);
        mLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("1");

        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());

        return true;
    }

    /**
     * 获取证件号对应中文说明
     * @param cIDType
     * @return
     */
    private String getIDTypeName(String cIDType)
    {
        String tStrSql = " select CodeName from LDCode " + " where CodeType = 'idtype' and Code = '" + cIDType + "' ";
        return new ExeSQL().getOneValue(tStrSql);
    }

    /**
     * 获取缴费期间。该函数获取信息不准
     * @return
     */
    private String getPayPeriod()
    {
        String tPayPeriod = null;
        LCPolSet tPolInfoSet = mGrpSCInsuPolInfo;

        // 没有获取规则，因此只随机取第一条。
        LCPolSchema tPolInfo = tPolInfoSet.get(1);
        int tPayYears = tPolInfo.getPayYears();
        int tPayIntv = tPolInfo.getPayIntv();
        if (tPayIntv == 0)
        {
            tPayPeriod = "趸缴";
        }
        else
        {
            tPayPeriod = tPayYears + "年";
        }
        // --------------------
        return tPayPeriod;
    }

    private String getInsuPrem()
    {
        LCPolSet tPolInfoSet = mGrpSCInsuPolInfo;
        double tSumPrem = 0d;
        for (int i = 1; i <= tPolInfoSet.size(); i++)
        {
            LCPolSchema tTmpPol = tPolInfoSet.get(i);
            tSumPrem += tTmpPol.getPrem();
        }

        //        return String.valueOf(PubFun.setPrecision(tSumPrem, "0.00"));
        //      By zhangyang
        return format(tSumPrem);
    }

    /**
     * 获取缴费频次代码对应中文含义。
     * @param cPayIntv
     * @return
     */
    private String chgGrpPayIntv(String cPayIntv)
    {
        String tStrSql = "select CodeName from LDCode where CodeType = 'payintv' " + " and Code = '" + cPayIntv + "'";
        return new ExeSQL().getOneValue(tStrSql);
    }

    /**
     * 获取机构代码代码对应中文含义。
     * @param cPayIntv
     * @return
     */
    private String chgGrpManageCom(String cManageCom)
    {
        String tStrSql = "select Name from LDCom where ComCode = '" + cManageCom + "'";
        return new ExeSQL().getOneValue(tStrSql);
    }

    private boolean isGrpULIRisk(String cRiskCode)
    {
        String tStrSql = " select 1 from LMRiskApp where 1 = 1 and RiskType4 = '4' and RiskProp = 'G' "
                + " and RiskCode = '" + cRiskCode + "' ";
        String tStrResult = new ExeSQL().getOneValue(tStrSql);
        return "1".equals(tStrResult);
    }

    /**
     * 格式化浮点型数据
     * @param dValue double
     * @return String
     */
    private static String format(double dValue)
    {
        return new DecimalFormat("0.00").format(dValue);
    }

    public CErrors getErrors()
    {
        return null;
    }
}
