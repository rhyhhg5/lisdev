/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import java.util.Vector;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLDatasets;
import com.sinosoft.lis.schema.LCContRePrtLogSchema;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.StrTool;

/*
 * <p>ClassName: LCGrpContF1PBL </p>
 * <p>Description: LCPolF1BL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-11-04
 */
public class ReLCGrpContF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    private VData mResult2 = new VData();
    private MMap map = new MMap();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCGrpContSet mOldLCGrpContSet = new LCGrpContSet();
    private LCGrpContSet mNewLCGrpContSet = new LCGrpContSet();
    private LCContRePrtLogSchema mLCContRePrtLogSchema = new LCContRePrtLogSchema();
    private XMLDatasets mXMLDatasets = new XMLDatasets();
    private String mOperate = "";
    private String currDate = PubFun.getCurrentDate();
    private String currTime = PubFun.getCurrentTime();


    /*
     * 对于同时传入主险和附加险保单号的情况，如果它们是同一个印刷号的，
     * 将被存在同一个保单数据块中。所以将打印过的保单号存放在这个Vector中。
     */
    private Vector m_vPolNo = new Vector();

    public ReLCGrpContF1PBL()
    {
        mXMLDatasets.createDocument();
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

            if (!cOperate.equals("PRINT")
                && !cOperate.equals("CONFIRM"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }
            if (!checkdate())
            {
                return false;
            }
            if (cOperate.equals("PRINT"))
            // 准备所有要打印的数据
            {
                if (!getPrintData())
                {
                    return false;
                }
                if (!dealRePrtLog()) {
                    return false;
                }
                PubSubmit pubsubmit = new PubSubmit();
                mResult2.add(map);
                if (!pubsubmit.submitData(mResult2, cOperate)) {
                    mErrors.copyAllErrors(pubsubmit.mErrors);
                    return false;
                }
                System.out.println("---End pubsubmit---");
            }
            else if (cOperate.equals("CONFIRM"))
            {
                if (!getConfirm())
                {
                    return false;
                }
            }

            return true;
        }

    /**
     * 调试函数
     * @param args String[]
     */
    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "pc";
        tG.ComCode = "86";
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        ReLCGrpContF1PUI tReLCGrpContF1PUI = new ReLCGrpContF1PUI();
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo("0000229001");
        tLCGrpContSchema.setContPrintType("1");
        tLCGrpContSet.add(tLCGrpContSchema);

        LCContRePrtLogSchema tLCContRePrtLogSchema=new LCContRePrtLogSchema();
        tLCContRePrtLogSchema.setContNo("0000229001");
        tLCContRePrtLogSchema.setPrtNo("18000009552");
        tLCContRePrtLogSchema.setContType("2");
        tLCContRePrtLogSchema.setRePrtReasonCode("6");
        tLCContRePrtLogSchema.setRemark("其他");
        tLCContRePrtLogSchema.setManageCom("86");

        VData vData = new VData();

        vData.addElement(tLCGrpContSet);
        vData.addElement(tLCContRePrtLogSchema);
        vData.add(tG);
         ReLCGrpContF1PBL tReLCGrpContF1PBL = new ReLCGrpContF1PBL();
        if (!tReLCGrpContF1PBL.submitData(vData, "PRINT"))
        {
        }
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //获取前台传入的数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mOldLCGrpContSet.set((LCGrpContSet) cInputData.getObjectByObjectName(
                "LCGrpContSet",
                0));
        mLCContRePrtLogSchema.setSchema((LCContRePrtLogSchema) cInputData.
                                        getObjectByObjectName(
                                                "LCContRePrtLogSchema",
                                                0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    /**
     * 获取返回信息
     * @return VData
     */
    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCGrpContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        LCGrpContSet tLCGrpContSet = null;
        LCGrpContDB tLCGrpContDB = null;
        LCGrpContSchema tLCGrpContSchema = null;
        String strSql = "";

//        m_vPolNo.clear(); // Clear contents of m_vPolNo

//        for (int nIndex = 0; nIndex < mOldLCGrpContSet.size(); nIndex++)
//        {
//            tLCGrpContSchema = mOldLCGrpContSet.get(nIndex + 1);
//
//            // Have been printed, continue ...
//            if (m_vPolNo.contains(tLCGrpContSchema.getContNo()))
//            {
//                continue;
//            }
//
//            strSql = "SELECT * FROM LCGrpCont WHERE ContNo = '" +
//                     tLCGrpContSchema.getContNo() + "'";
//            strSql += " AND AppFlag = '1'";
//            strSql += " AND PrintCount = 1 ";
//
//            System.out.println(strSql);
//
//            tLCGrpContSet = new LCGrpContDB().executeQuery(strSql);
//
//            if (tLCGrpContSet.size() == 0)
//            {
//                buildError("getPrintData", "所指定的保单不符合重打的条件");
//                return false;
//            }
//
//            tLCGrpContSchema = tLCGrpContSet.get(1);
//
//            // 查询保单信息的时候需要加上印刷号的信息
//            mPrtNo = tLCGrpContSchema.getPrtNo();

        // 如果这个保单不是主险保单
        // 如果是主险保单，该保单的主险保单号等于自身的保单号
//            if (!tLCGrpContSchema.getMainPolNo().equals(tLCGrpContSchema.getContNo()))
//            {
//
//                strSql = "SELECT * FROM LCPol WHERE PolNo = '" +
//                         tLCGrpContSchema.getMainPolNo() + "'";
//                strSql += " AND PrtNo = '" + mPrtNo + "'";
//                strSql += " AND AppFlag = '1'";
//                strSql += " AND PrintCount = 1 ";
//
//                System.out.println(strSql);
//
//                tLCGrpContSet = new LCGrpContDB().executeQuery(strSql);
//                if (tLCGrpContSet.size() == 0)
//                { // 没有查询到同一印刷号的符合打印条件的主险保单
//                    buildError("getPrintData", "找不到同一印刷号的符合重打条件的主险保单");
//                    return false;
//                }
//                else
//                { // 将查询到的主险保单放在mLCPolSchema中
//                    tLCGrpContSchema = tLCGrpContSet.get(1);
//                }
//            }

        /**
         * 查询同一印刷号的所有附加险保单
         * 经过上一步操作，在tLCPolSchema中包含着一个主险保单的信息
         */
//            strSql = "SELECT * FROM LCPol WHERE MainPolNo = '" +
//                     tLCGrpContSchema.getMainPolNo() + "'";
//            strSql += " AND PolNo <> '" + tLCGrpContSchema.getMainPolNo() + "'";
//            strSql += " AND PrtNo = '" + mPrtNo + "'";
//
//            System.out.println(strSql);
//
//            tLCGrpContSet = new LCGrpContDB().executeQuery(strSql);

        /**
         * 校验所有的附加险保单是否符合打印的条件
         * 记录下本次打印过的所有保单号
         */
//            m_vPolNo.add(tLCGrpContSchema.getContNo());
//
//            for (int n = 0; n < tLCGrpContSet.size(); n++)
//            {
//                LCGrpContSchema tempLCPolSchema = tLCGrpContSet.get(n + 1);
//
//                m_vPolNo.add(tempLCPolSchema.getContNo());
//                /*Lis5.3 upgrade get
//                        if( !tempLCPolSchema.getAppFlag().equals("1")
//                            || tempLCPolSchema.getPrintCount() == 0 ) {
//                          buildError("getPrintData", "附加险保单不符合重打条件");
//                          return false;
//                        }
//                 */
//            }
//        }

        // end of "for(int nIndex = 0; nIndex < mOldLCGrpContSet.size(); nIndex++)"
        System.out.println("mOldLCGrpContSet.size() is " +
                           mOldLCGrpContSet.size());
        for (int nIndex = 1; nIndex <= mOldLCGrpContSet.size(); nIndex++)
        {
            tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(mOldLCGrpContSet.get(nIndex).getGrpContNo());
            System.out.println("************");
            tLCGrpContDB.getInfo();
            System.out.println("************");
            tLCGrpContDB.setPrintCount( -1);
            tLCGrpContDB.setContPrintType(mOldLCGrpContSet.get(nIndex).getContPrintType());
            System.out.println("************"+mOldLCGrpContSet.get(nIndex).getContPrintType());
            mNewLCGrpContSet.add(tLCGrpContDB.getSchema());
            System.out.println("************");
        }
        map.put(mNewLCGrpContSet,"UPDATE");
//        System.out.println("************");
//        mResult.add(mNewLCGrpContSet);
//        ReLCGrpContF1PBLS tReLCGrpContF1PBLS = new ReLCGrpContF1PBLS();
//        tReLCGrpContF1PBLS.submitData(mResult, mOperate);
//        if (tReLCGrpContF1PBLS.mErrors.needDealError())
//        {
//            mErrors.copyAllErrors(tReLCGrpContF1PBLS.mErrors);
//            buildError("saveData", "提交数据库出错！");
//            return false;
//        }
        return true;
    }

    /**
     * 重新生成打印数据处理
     * @return boolean
     */
    private boolean getConfirm()
    {
        LCGrpContDB tLCGrpContDB = null;
        //循环外部传入的数据，获取每个个单合同的详细信息
        for (int nIndex = 1; nIndex < mOldLCGrpContSet.size(); nIndex++)
        {
            tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(mOldLCGrpContSet.get(nIndex).getGrpContNo());
            tLCGrpContDB.getInfo();
            //将打印记录置为0
            tLCGrpContDB.setPrintCount(0);
            mNewLCGrpContSet.add(tLCGrpContDB);
        }
        mResult.add(mNewLCGrpContSet);
        //执行数据更新
        ReLCGrpContF1PBLS tReLCGrpContF1PBLS = new ReLCGrpContF1PBLS();
        tReLCGrpContF1PBLS.submitData(mResult, mOperate);
        if (tReLCGrpContF1PBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tReLCGrpContF1PBLS.mErrors);
            buildError("saveData", "提交数据库出错！");
            return false;
        }
        return true;
    }

    private boolean checkdate() {

        for (int nIndex = 1; nIndex <= mOldLCGrpContSet.size(); nIndex++) {
            LCContReceiveDB tLCContReceiveDB = new LCContReceiveDB();
            LCContReceiveSet tLCContReceiveSet = new LCContReceiveSet();
            tLCContReceiveDB.setContNo(mOldLCGrpContSet.get(nIndex).getGrpContNo());
            tLCContReceiveDB.setReceiveState("0");
            tLCContReceiveSet = tLCContReceiveDB.query();
            if (tLCContReceiveSet.size() > 0) {
                String errormsg = "保单号为" +
                                  mOldLCGrpContSet.get(nIndex).getGrpContNo() +
                                  "的记录还没有进行合同接收操作，请先合同接收后再打印！";
                System.out.println(errormsg);
                buildError("checkdate", errormsg);
                return false;

            }
        }
        if (mLCContRePrtLogSchema == null) {
            String errormsg = "请录入重打原因！";
            System.out.println(errormsg);
            buildError("checkdate", errormsg);

            return false;
        }
        if(StrTool.cTrim(mOldLCGrpContSet.get(1).getContPrintType()).equals("")){
            buildError("checkdate","保单打印类型为空!");
                return false;
        }

        return true;
}
    private boolean dealRePrtLog() {
            LCContRePrtLogDB tLCContRePrtLogDB = new LCContRePrtLogDB();
            LCContRePrtLogSet tLCContRePrtLogSet = new LCContRePrtLogSet();
            tLCContRePrtLogSet = tLCContRePrtLogDB.executeQuery(
                    "select * from lccontreprtlog where contno='" +
                    mLCContRePrtLogSchema.getContNo() + "' and conttype='2'");
            if (tLCContRePrtLogSet.size() < 1) {
                mLCContRePrtLogSchema.setPrintID(1);
                mLCContRePrtLogSchema.setStateFlag("1");
                mLCContRePrtLogSchema.setPrintCount("1");
                mLCContRePrtLogSchema.setOpeManageCom(mGlobalInput.ManageCom);
                mLCContRePrtLogSchema.setOperator(mGlobalInput.Operator);
                mLCContRePrtLogSchema.setMakeDate(currDate);
                mLCContRePrtLogSchema.setMakeTime(currTime);
                mLCContRePrtLogSchema.setModifyDate(currDate);
                mLCContRePrtLogSchema.setModifyTime(currTime);

                map.put(mLCContRePrtLogSchema, "INSERT");
            } else {
                for (int i = 1; i <= tLCContRePrtLogSet.size(); i++) {
                    LCContRePrtLogSchema tLCContRePrtLogSchema = new
                            LCContRePrtLogSchema();
                    tLCContRePrtLogSchema = tLCContRePrtLogSet.get(i).getSchema();
                    if (tLCContRePrtLogSchema.getStateFlag().equals("1")) {
                        tLCContRePrtLogSchema.setStateFlag("0");
                        tLCContRePrtLogSchema.setOpeManageCom(mGlobalInput.
                                ManageCom);
                        tLCContRePrtLogSchema.setOperator(mGlobalInput.Operator);
                        tLCContRePrtLogSchema.setModifyDate(currDate);
                        tLCContRePrtLogSchema.setModifyTime(currTime);
                        map.put(tLCContRePrtLogSchema, "UPDATE");
                    }
                }
                mLCContRePrtLogSchema.setPrintID(tLCContRePrtLogSet.size() + 1);
                mLCContRePrtLogSchema.setStateFlag("1");
                mLCContRePrtLogSchema.setPrintCount(tLCContRePrtLogSet.size() + 1);
                mLCContRePrtLogSchema.setOpeManageCom(mGlobalInput.ManageCom);
                mLCContRePrtLogSchema.setOperator(mGlobalInput.Operator);
                mLCContRePrtLogSchema.setMakeDate(currDate);
                mLCContRePrtLogSchema.setMakeTime(currTime);
                mLCContRePrtLogSchema.setModifyDate(currDate);
                mLCContRePrtLogSchema.setModifyTime(currTime);
                map.put(mLCContRePrtLogSchema, "INSERT");
            }
            return true;
    }
}
