package com.sinosoft.lis.f1print;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.FileWriter;
import java.io.*;

public class LAWageBaseIndexInfoBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    //private String mtype = null;

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();
//    private String WageNoStart="";
//    private String WageNoEnd="";
//    private String tName="";

    public LAWageBaseIndexInfoBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        SSRS tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LAStandardRCDownloadBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 7][23];

        mToExcel[0][0] = "薪资计算基础指标明细表";
        mToExcel[3][0] = "薪资年月";
        mToExcel[3][1] = "代理人编码";
        mToExcel[3][2] = "代理人姓名";
        mToExcel[3][3] = "代理人职级";
        mToExcel[3][4] = "管理机构";
        mToExcel[3][5] = "销售机构代码";
        mToExcel[3][6] = "销售机构名称";
        mToExcel[3][7] = "个人当考核月保费";
        mToExcel[3][8] = "13月新单保费(分母)";
        mToExcel[3][9] = "13月有效保费(分子)";
        mToExcel[3][10] = "13月继续率";
        mToExcel[3][11] = "个人月FYC";
        mToExcel[3][12] = "个人当考核月FYC";
        mToExcel[3][13] = "个人当季度FYC";
        mToExcel[3][14] = "团队当月FYC";
        mToExcel[3][15] = "团队当季度FYC";
        mToExcel[3][16] = "个人月续年续佣";
        mToExcel[3][17] = "个人月续保续佣";
        mToExcel[3][18] = "个人当月新标准客户";
        mToExcel[3][19] = "个人当考核月客户";
        mToExcel[3][20] = "当月新增人数";
        mToExcel[3][21] = "当季新增人数";
        
        

        int num=tSSRS.getMaxRow();
        int mcol=tSSRS.getMaxCol();
        for(int row = 4; row <= num+3; row++)
        {
            for(int col = 1; col <= mcol; col++)
            {
            	//mToExcel[row][0] = Integer.toString(row-3);
                mToExcel[row][col-1] = tSSRS.GetText(row-3, col);
            }
        }
        mToExcel[num+4][10] = "制表时间："+mCurDate+""+mCurTime;
        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAStandardRCDownloadBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
//        WageNoStart = (String) tf.getValueByName("StartDate");
//        WageNoEnd = (String) tf.getValueByName("EndDate");
//        tName = (String) tf.getValueByName("tName");
       //mtype = (String) tf.getValueByName("Type");
        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAStandardRCDownloadBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {
        //RateCommisionBL RateCommisionBL = new
           //RateCommisionBL();
    }
}
