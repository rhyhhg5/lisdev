	package com.sinosoft.lis.f1print;

	/**
	 * <p>Title: AgentSystem</p>
	 *
	 * <p>Description: </p>
	 *
	 * <p>Copyright: Copyright (c) 2005</p>
	 *
	 * <p>Company: Sinosoft</p>
	 *
	 * @author
	 * @version 1.0
	 */
	import com.sinosoft.lis.pubfun.GlobalInput;
	import com.sinosoft.lis.pubfun.MMap;
	import com.sinosoft.lis.pubfun.PubFun;
	import com.sinosoft.lis.pubfun.PubSubmit;
	import com.sinosoft.utility.CError;
	import com.sinosoft.utility.CErrors;
	import com.sinosoft.utility.SSRS;
	import com.sinosoft.utility.TransferData;
	import com.sinosoft.utility.VData;
	import com.sinosoft.lis.schema.InterfaceTableSchema;

	public class InterfacetableDataBL
	{
	    /**错误信息容器*/
	    public CErrors mErrors = new CErrors();

	    private GlobalInput mGI = null;

	    private String BatchNo = "";
	    private String DepCode = "";
	    private String VoucherType = "";
	    private String ChargeDate = "";
	    private String SerialNo = "";
	    private String AgentNo="";
	    private String NewAgentNo="";
	    private String NewChargeDate="";
	    private String Style="";
	    private String mCurDate = PubFun.getCurrentDate();
		
	       

	    public InterfacetableDataBL()
	    {
	    }

	    public boolean submitData(VData cInputData, String cOperate) 
	    {

	    	System.out.println("Start InterfacetableDataBL Submit ...");
	        if (!getInputData(cInputData))
	        {
	            return false;
	        }

	        if (!checkData())
	        {
	            return false;
	        }
	        
	            if (!emptyData()) {
	             return false;
	            }
	        return true;
	    }

	    /**
	     * 校验操作是否合法
	     * @return boolean
	     */
	    private boolean checkData()
	    {
	        return true;
	    }

	    /**
	     * dealData
	     * 处理业务数据
	     *
	     * @return boolean：true提交成功, false提交失败
	     */
	    public boolean emptyData()
	    {
	        System.out.println("BL->置空数据"); 
	    	//提交后台处理
	        MMap tmap = new MMap();
	        VData tInputData = new VData();
	        System.out.println("batchno : "+ BatchNo);
	        if(Style!="2"&&!Style.equals("2")&&Style!="3"&&!Style.equals("3")){
	        tmap.put("update interfacetable set agentno='"+NewAgentNo+"' where batchno = '"+BatchNo+"' and vouchertype = '"+VoucherType+"' and depcode = '"+DepCode+"' and chargedate = '"+ChargeDate+"' and serialno = '"+SerialNo+"' and agentno = '"+AgentNo+"' and ReadState='3'", "UPDATE");
	        }
	        if(Style!="1"&&!Style.equals("1")&&Style!="3"&&!Style.equals("3")){
	       
	        tmap.put("update interfacetable set readstate = NULL,readdate = NULL,readtime = NULL where depcode = '"+DepCode+"' and batchno = '"+BatchNo+"' and chargedate='"+ChargeDate+"' and vouchertype = '"+VoucherType+"' and ReadState='3'", "UPDATE");
	        }
	        if(Style!="1"&&!Style.equals("1")&&Style!="2"&&!Style.equals("2")){
		        tmap.put("update interfacetable set readstate = NULL,readdate = NULL,readtime = NULL,chargedate='"+NewChargeDate+"' where batchno = '"+BatchNo+"' and chargedate='"+ChargeDate+"' and vouchertype = '"+VoucherType+"' and ReadState='3'", "UPDATE");
		        }
	        tInputData.add(tmap);
	        System.out.println(tmap);       
	        PubSubmit tPubSubmit = new PubSubmit();     
	        if (!tPubSubmit.submitData(tInputData, ""))
	        {
	            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
	            buiError( "gatherData", "批次号码为" + BatchNo + "恢复数据出错，提示信息为：" + tPubSubmit.mErrors.getFirstError());         
	            tmap=null;
	            tInputData=null;
	            return false;
	        }
	        
	    	tmap=null;
	        tInputData=null;
	        return true;
	  }
	    

	    /**
	     * getInputData
	     *将外部传入的数据分解到本类的属性中
	     * @param cInputData VData：submitData中传入的VData对象
	     * @return boolean：true提交成功, false提交失败
	     * @throws java.text.ParseException 
	     */
	    public boolean getInputData(VData data)
	    {
	        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
	        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
	        System.out.println(mGI.ManageCom);

	        if (mGI == null || tf == null)
	        {
	        	buiError("getInputData","数据维护出错");
	            return false;
	        }
	        BatchNo = (String) tf.getValueByName("BatchNo");      
	        VoucherType = (String) tf.getValueByName("VoucherType");
	        AgentNo = (String) tf.getValueByName("AgentNo");
	        ChargeDate = (String) tf.getValueByName("ChargeDate");
	        NewChargeDate = (String) tf.getValueByName("NewChargeDate");
	        NewAgentNo = (String) tf.getValueByName("NewAgentNo");
	        SerialNo = (String) tf.getValueByName("SerialNo");
	        DepCode = (String) tf.getValueByName("DepCode");
	        Style = (String) tf.getValueByName("Style");
	        System.out.println(Style);
	        if (Style == null ||BatchNo == null || VoucherType == null || AgentNo == null || ChargeDate == null || SerialNo == null || DepCode == null)
	        {
	            buiError("getInputData", "传入信息不完整！");
	            return false;
	        }
	        return true;
	    }
	    
	    private void buiError(String functionName,String errorMessage){
	    	CError tError = new CError();
	        tError.moduleName = "IntFaceTabDataBL";
	        tError.functionName = functionName;
	        tError.errorMessage = errorMessage;
	        mErrors.addOneError(tError);
	    }
	    public static void main(String[] args)
	    {
	       
	    }
	}


