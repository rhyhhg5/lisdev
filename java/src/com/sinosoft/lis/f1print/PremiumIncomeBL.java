package com.sinosoft.lis.f1print;

/**
 * <p>Title: 保费收入与实收保费 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PremiumIncomeBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();
	// 取得的时间
	private String mDay[] = null;
	// 部门
	private String ManageCom_1 = null;
	// 输入的查询sql语句
	private String msql = "";
	private String nsql = "";
	// 业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	public PremiumIncomeBL() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("PRINT")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		mResult.clear();

		// 准备所有要打印的数据
		if (!getNewPrintData()) {
			return false;
		}

		return true;
	}

	public static void main(String[] args) {
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		// 全局变量
		mDay = (String[]) cInputData.get(0);
//		ManageCom_1 = (String) cInputData.get(1);
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));

		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}

		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "PremiumIncomeBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	/**
	 * 
	 * 
	 * @return
	 */
	private boolean getNewPrintData() {

		SSRS tSSRS = new SSRS();
		GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
		tGetSQLFromXML.setParameters("ManageCom", mDay[2]);
		tGetSQLFromXML.setParameters("StartDay", mDay[0]);
		tGetSQLFromXML.setParameters("EndDay", mDay[1]);

		String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
	//	tServerPath = "D:/workspace/picch/ui/";
		
		ExeSQL tExeSQL = new ExeSQL();

//		String nsql = "select Name from LDCom where ComCode='"
//				+ mDay[2] + "'"; // 管理机构
//		tSSRS = tExeSQL.execSQL(nsql);
//		String ManageCom_1 = tSSRS.GetText(1, 1);

		TextTag texttag = new TextTag(); // 新建一个TextTag的实例
		XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
		texttag.add("StartDay", mDay[0]);
		texttag.add("EndDay", mDay[1]);
		texttag.add("ManageCom", mDay[2]);
		xmlexport.createDocument("PremiumIncome.vts", "printer"); // 最好紧接着就初始化xml文档

		String[] detailArr = new String[] { "机构名称", "业务系统机构编号","财务系统机构编号","险种","保费收入","实收保费" };

		
		//保费收入与实收保费
		String msql = tGetSQLFromXML.getSql(
				tServerPath+"f1print/picctemplate/PremiumIncomeSql.xml", "Prem");
		ListTable tDetailListTable = getDetailListTable(msql, "Prem");
		xmlexport.addListTable(tDetailListTable, detailArr);

		

		
		
	
		
		if (texttag.size() > 0) {
			xmlexport.addTextTag(texttag);
		}

		mResult.clear();
		mResult.addElement(xmlexport);
		return true;
	}

	/**
	 * 获得明细ListTable
	 * 
	 * @param msql -
	 *            执行的SQL
	 * @param tName -
	 *            Table Name
	 * @return
	 */
	private ListTable getDetailListTable(String msql, String tName) {
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(msql);
		ListTable tlistTable = new ListTable();
		String strArr[] = null;
		String strSum="";
		tlistTable.setName(tName);
		for (int i = 1; i <= tSSRS.MaxRow; i++) {
			strArr = new String[tSSRS.MaxCol]; // 列数
			for (int j = 1; j <= tSSRS.MaxCol; j++) {
				strArr[j - 1] = tSSRS.GetText(i, j);
				if (strArr[j - 1] == "null") {
					strArr[j - 1] = "";
				}
				
					System.out.println("str = " + strArr[j - 1]);
					if(j==5||j==6) {
						if (strArr[j - 1] == "null") 
							strArr[j - 1] = "-";
					   else strSum = new DecimalFormat("0.00").format(Double.valueOf(strArr[j - 1]));						
					}  else 
							strSum = strArr[j - 1];
	                strArr[j - 1] = strSum;
					
				
                continue;
			}
			tlistTable.add(strArr);
		}
		return tlistTable;
	}
}