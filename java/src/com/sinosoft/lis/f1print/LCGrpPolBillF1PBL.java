package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author zy
 * @version 1.0
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LCGrpPolBillF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();
    //取得的时间
    private String mDay[] = null;
    private String mTime[] = null;
    private String AgentGroup = "";
    private String StartGrpPolNo = "";
    private String EndGrpPolNo = "";
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public LCGrpPolBillF1PBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        mDay = (String[]) cInputData.get(0);
        mTime = (String[]) cInputData.get(1);
        mLCGrpPolSchema.setSchema((LCGrpPolSchema) cInputData.
                                  getObjectByObjectName("LCGrpPolSchema", 0));
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        AgentGroup = mLCGrpPolSchema.getAgentGroup();
        StartGrpPolNo = mLCGrpPolSchema.getGrpPolNo();
        EndGrpPolNo = mLCGrpPolSchema.getAgentType();
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCGrpPolBillF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        SSRS tSSRS = new SSRS();

        String strModifyTime = "";
        if (!mTime[0].equals(""))
        {
            strModifyTime = " and LCPolPrint.ModifyTime >= '" + mTime[0] + "'";
        }
        if (!mTime[1].equals(""))
        {
            strModifyTime = strModifyTime + " and LCPolPrint.ModifyTime <= '" +
                            mTime[1] + "'";
        }

        String strStartGrpPolNoWhere = "";
        if (StartGrpPolNo != null && !StartGrpPolNo.equals(""))
        {
            strStartGrpPolNoWhere = " AND LCGrpPol.GrpPolNo >='" +
                                    StartGrpPolNo + "' ";
        }
        String strEndGrpPolNoWhere = "";
        if (EndGrpPolNo != null && !EndGrpPolNo.equals(""))
        {
            strEndGrpPolNoWhere = " AND LCGrpPol.GrpPolNo <='" + EndGrpPolNo +
                                  "' ";
        }

        //add by Minim
        String strExtendWhere = "";
        if (mLCGrpPolSchema.getRiskCode() != null &&
            !mLCGrpPolSchema.getRiskCode().equals(""))
        {
            strExtendWhere = strExtendWhere + " AND LCGrpPol.RiskCode ='" +
                             mLCGrpPolSchema.getRiskCode() + "' ";
        }

        if (mLCGrpPolSchema.getPrtNo() != null &&
            !mLCGrpPolSchema.getPrtNo().equals(""))
        {
            strExtendWhere = strExtendWhere + " AND LCGrpPol.PrtNo ='" +
                             mLCGrpPolSchema.getPrtNo() + "' ";
        }

        if (mLCGrpPolSchema.getGrpName() != null &&
            !mLCGrpPolSchema.getGrpName().equals(""))
        {
            strExtendWhere = strExtendWhere + " AND LCGrpPol.GrpName ='" +
                             mLCGrpPolSchema.getGrpName() + "' ";
        }

        if (mLCGrpPolSchema.getAgentCode() != null &&
            !mLCGrpPolSchema.getAgentCode().equals(""))
        {
            strExtendWhere = strExtendWhere + " AND LCGrpPol.AgentCode ='" +
                             mLCGrpPolSchema.getAgentCode() + "' ";
        }

        String strAgentGroupWhere = " AND LCGrpPol.AgentGroup in (select agentgroup from labranchgroup where branchattr LIKE '" +
                                    AgentGroup + "%' AND ManageCom LIKE '" +
                                    mLCGrpPolSchema.getManageCom() + "%')";

        msql = "select LCGrpPol.GrpPolNo,labranchgroup.name,LCGrpPol.GrpName,LCGrpPol.Peoples2,LAAgent.Name from labranchgroup,LCGrpPol,LCPolPrint,LAAgent where LAAgent.AgentCode=LCGrpPol.AgentCode and LCPolPrint.ModifyDate>='" +
               mDay[0] + "' and LCPolPrint.ModifyDate<='" + mDay[1] + "'" +
               strModifyTime + " and LCGrpPol.RiskCode in (select RiskCode from LMRiskApp where SubRiskFlag = 'M') and LCGrpPol.GrpPolNo = LCPolPrint.MainPolNo and LCGrpPol.ManageCom like'" +
               mLCGrpPolSchema.getManageCom() +
                "%' and LCPolPrint.PolType='2' and LCGrpPol.AgentGroup=labranchgroup.AgentGroup"
               + strAgentGroupWhere + strStartGrpPolNoWhere +
               strEndGrpPolNoWhere + strExtendWhere + " order by LCGrpPol.GrpPolNo,LCGrpPol.ManageCom,LCGrpPol.AgentGroup,LCGrpPol.AgentCode";
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println(msql);
        tSSRS = tExeSQL.execSQL(msql);
        ListTable tlistTable = new ListTable();
        String strArr[] = null;
        tlistTable.setName("LCGRPPOL");
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            strArr = new String[8];
            for (int j = 1; j <= tSSRS.getMaxCol(); j++)
            {
                strArr[j - 1] = tSSRS.GetText(i, j);
            }
            tlistTable.add(strArr);
        }
        strArr = new String[8];
        strArr[0] = "GrpPolNo";
        strArr[1] = "GrpName";
        strArr[2] = "Peoples2";
        strArr[3] = "Prem";
        strArr[4] = "Name";
        strArr[5] = "";
        strArr[6] = "";
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LCGrpPolBill.vts", "printer"); //最好紧接着就初始化xml文档
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("ManageCom", mLCGrpPolSchema.getManageCom());
        texttag.add("Operator", mGlobalInput.Operator);
        texttag.add("Sum", tSSRS.getMaxRow());
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        //xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
        mResult.addElement(xmlexport);
        return true;

    }

    public static void main(String[] args)
    {
    }
}