package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.AgentPubFun;
import java.math.BigDecimal;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LADimissionContBL {

    public CErrors mErrors = new CErrors();

    private VData mInputData = new VData();

    private VData mResult = new VData();

    private String mOperate = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();
    private String mBranchAttr = "";
    private String mStartDate = "";
    private String mEndDate = "";
    private String mAgentState = "";
    private String mAgentGroup ="";
    private String mManageCom = "";
    private String  mAgentStateMin ;
    private String  mAgentStateMax ;
    private XmlExport mXmlExport = null;

    private SSRS mSSRS1 = new SSRS();


    private ListTable mListTable = new ListTable();

    private PubFun mPubFun = new PubFun();

    private String mManageName = "";

    public LADimissionContBL() {
    }
    public static void main(String[] args)
    {
        //
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";

        LADimissionContBL tLADimissionContBL = new LADimissionContBL();
       // tLADimissionContBL.submitData(tVData, "");

    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mOperate = cOperate;
        mInputData = (VData) cInputData;
        if (mOperate.equals("")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!this.getInputData(mInputData)) {
            return false;
        }

        if (!dealdate()) {
            return false;
        }

        if (!getPrintData()) {
            this.bulidError("getPrintData", "查询数据失败！");
            return false;
        }

        return true;
    }

    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
      try
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            this.mManageCom = (String) mTransferData.getValueByName("tManageCom");
        } catch (Exception ex) {
            this.mErrors.addOneError("");
            return false;
        }

        return true;
    }
    /**
    * 业务处理方法
    * @return boolean
    */

   private boolean dealdate()
   {


      return true;
   }

    /**
     *
     * @return boolean
     */
    private boolean getPrintData() {
        TextTag tTextTag = new TextTag();

        mXmlExport = new XmlExport();

        mXmlExport.createDocument("LADimissionContReport.vts", "printer");

        String tMakeDate = "";
        String tMakeTime = "";

        tMakeDate = mPubFun.getCurrentDate();
        tMakeTime = mPubFun.getCurrentTime();
        tTextTag.add("MakeDate", tMakeDate);
        tTextTag.add("MakeTime", tMakeTime);
        tTextTag.add("tName", mManageName);

        System.out.println("121212121212121212121212121212" + tMakeDate);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] title = {"", "", "", "", "" ,"11","11","11","11","11","11","11","11","11","11"};

        if (!getListTable()) {
            return false;
        }
        System.out.print("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.print("121");
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 查询列表显示数据
     * @return boolean
     */
    private boolean getListTable()
    {
        String strSql = "select a.contno,b.managecom,getUniteCode(b.agentcode),b.name  from  lccont a,laagent b "
                        +" where a.agentcode=b.agentcode  "
                        +" and b.branchtype='1' and b.branchtype2='01' and b.agentstate>='06'"
//                        and uwfalg<>'a'
                        +" and b.ManageCom like '"+mManageCom+"%' order by b.managecom,b.agentcode ";
        SSRS tSSRS2 = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS2 = tExeSQL.execSQL(strSql);
        if (tSSRS2.getMaxRow() >= 1)
        {
            //此人有职级变更
            for (int j = 1; j <= tSSRS2.getMaxRow(); j++)
            {
                String Info[] = new String[4];
                Info[0] = tSSRS2.GetText(j, 1);
                Info[1] = tSSRS2.GetText(j, 2);
                Info[2] = tSSRS2.GetText(j, 3);
                Info[3] = tSSRS2.GetText(j, 4);
                mListTable.add(Info);
            }
        }
        else
        {
            bulidError("getListTable","没有符合条件的信息!");
            return false;
        }
        mListTable.setName("ZT");

        return true;
    }


    /**
     * 获取打印所需要的数据
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg) {

        CError tCError = new CError();

        tCError.moduleName = "LADimissionContBL";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;

        this.mErrors.addOneError(tCError);

    }

    /**
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


}
