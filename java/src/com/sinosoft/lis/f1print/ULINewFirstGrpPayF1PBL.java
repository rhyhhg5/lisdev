package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.lis.pubfun.FDate;

public class ULINewFirstGrpPayF1PBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //取得的保单号码
    private String mContNo = "";

    //输入的查询sql语句
    private String msql = "";

    //取得的延期承保原因
    private String mUWError = "";

    private String mOperate = "";

    //取得的代理人编码
    private String mAgentCode = "";

    private String mGrpContNo = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    private LCGrpContSet mLCGrpContSet = new LCGrpContSet();

    private LCContPlanDutyParamSchema mLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();

    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

    private String[] PubRiskcode = new String[10];

    private String PubRiskcodePrintFlag = "0";

    private static int sumCount = 7;

    public ULINewFirstGrpPayF1PBL()
    {
    }

    /**
     * 传输数据的公共方法
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //        if (!cOperate.equals("CONFIRM") && !cOperate.equals("PRINT"))
        //        {
        //            buildError("submitData", "不支持的操作字符串");
        //            return false;
        //        }
        mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {

    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.getObjectByObjectName("LOPRTManagerSchema", 0));
        if (mLOPRTManagerSchema == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (mLOPRTManagerSchema.getPrtSeq() == null)
        {
            buildError("getInputData", "没有得到足够的信息:印刷号不能为空！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {

        //根据印刷号查询打印队列中的纪录
        String PrtNo = mLOPRTManagerSchema.getPrtSeq(); //打印流水号
        int WH = 0;
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setProposalGrpContNo(mLOPRTManagerSchema.getOtherNo());
        int m, i;
        LCGrpContSet mLCGrpContSet = new LCGrpContSet();
        mLCGrpContSet = tLCGrpContDB.query();
        if (mLCGrpContSet.size() > 1 || mLCGrpContSet.size() == 0)
        {
            mErrors.copyAllErrors(tLCGrpContDB.mErrors);
            buildError("outputXML", "在取得LCCont的数据时发生错误");
            return false;
        }
        mLCGrpContSchema = mLCGrpContSet.get(1);

        mAgentCode = mLCGrpContSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "在取得LAAgent的数据时发生错误");
            return false;
        }
        mLAAgentSchema = tLAAgentDB.getSchema(); //保存代理人信息
        //险种信息
        mGrpContNo = mLCGrpContSchema.getGrpContNo();
        //2-1 查询体检子表
        String[] RiskTitle = new String[5];
        RiskTitle[0] = "SumInsureds";
        RiskTitle[1] = "RiskCode";
        RiskTitle[2] = "RiskName";
        RiskTitle[3] = "PubMoney";
        RiskTitle[4] = "SelfMoney";

        ListTable tRiskInfoTable = new ListTable();
        //ListTable tRiskInfoTable1 = new ListTable();
        tRiskInfoTable.setName("RiskInfo");
        // tRiskInfoTable1.setName("RiskInfo");
        String strRiskInfo[] = null;

        String Sql = "";
        String tSql = "";
        ExeSQL q_exesql = new ExeSQL();
        String sql = "select sum(Prem) from lcpol where grpcontno='" + mLCGrpContSchema.getGrpContNo()
                + "' and uwflag in ('4','9') with ur ";
        String LJSPayPrem = q_exesql.getOneValue(sql);
    
        
            Sql = "select b.peoples2,a.riskcode,(select riskname from lmriskapp where riskcode=a.riskcode),(select nvl(sum(round((l.prem),2)),0.00) from lcprem l,lmdutypay m where l.payplancode=m.payplancode and m.AccPayClass='3' and grpcontno=b.grpcontno),(select nvl(sum(round((l.prem),2)),0.00) from lcprem l,lmdutypay m where l.payplancode=m.payplancode and m.AccPayClass!='3' and grpcontno=b.grpcontno)   from lcgrppol a,lcgrpcont b where a.prtno='"+mLCGrpContSchema.getPrtNo()+"'and a.prtno=b.prtno";
            
    
        
        // ---------------------------------------
      java.text.NumberFormat  formater  =  java.text.DecimalFormat.getInstance();  
   	  formater.setMaximumFractionDigits(2);  
   	  formater.setMinimumFractionDigits(2); 
        SSRS s_ssrs = q_exesql.execSQL(Sql); 
        SSRS ts_ssrs = new SSRS();
          
        WH = s_ssrs.getMaxRow();
        String ContPlanInfo = "";
        double SumPrem = 0.0;
        for (int Index = 1; Index <= s_ssrs.getMaxRow(); Index++)
        {
            strRiskInfo = new String[5]; 
            strRiskInfo[0] = s_ssrs.GetText(Index, 1);
            strRiskInfo[1] = s_ssrs.GetText(Index, 2);
            strRiskInfo[2] = s_ssrs.GetText(Index, 3);
            strRiskInfo[3] = cancelKeXueCount(s_ssrs.GetText(Index, 4));
            strRiskInfo[4] = cancelKeXueCount(s_ssrs.GetText(Index, 5));
   
            tRiskInfoTable.add(strRiskInfo);
       }
        
        if (LJSPayPrem.equals(null) || LJSPayPrem.equals("null") || LJSPayPrem.equals(""))
        {
            LJSPayPrem = "0";
        }
        SumPrem = Double.parseDouble(LJSPayPrem);

        //其它模版上单独不成块的信息
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        ListTable tCountTable = new ListTable();
        String[] tCount = new String[1];
        tCountTable.setName("Blank");
        tCount[0] = "Blank";
        int count = sumCount - WH;
        if (count > 0)
        {
            for (i = 1; i <= count; i++)
            {
                String[] blank = { "" };
                //              tCountTable.add(blank);
            }
        }

        // 增加模版分支，如果为TPA保单，缴费通知书模版为：TPAGrpFirstPay.vts
      
        if ("2".equals(this.mLCGrpContSchema.getContPrintType()))
        {
            xmlexport.createDocument("NewGrpFirstPay.vts", "printer");
        }
        else
        {
            xmlexport.createDocument("NewGrpFirstPay.vts", "printer"); //最好紧接着就初始化xml文档
        }
      
        //------------------------------------------

        /** 动态行数，为了将显示等分 */

        if (tCountTable.size() > 0)
        {
            xmlexport.addListTable(tCountTable, tCount);
        }

        //生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();

        //模版自上而下的元素
        if (mLCGrpContSchema.getPayMode() == null)
        {
            xmlexport.addDisplayControl("displaymoney");
        }
        else
        {
            if (mLCGrpContSchema.getPayMode().equals("4"))
            {
                xmlexport.addDisplayControl("displaybank");
            }
            else
            {
                xmlexport.addDisplayControl("displaymoney");
            }
        }
        String sql4 = " select PostalAddress,ZipCode from LCGrpAppnt where grpContNo='"
                + mLCGrpContSchema.getGrpContNo() + "'";
        SSRS GrpInfoSSRS = null;
        String PostalAddress = "";
        String AppntZipcode = "";
        try
        {
            GrpInfoSSRS = q_exesql.execSQL(sql4);
            PostalAddress = GrpInfoSSRS.GetText(1, 1);
            AppntZipcode = GrpInfoSSRS.GetText(1, 2);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        //int prtNo = Integer.parseInt(PubFun1.CreateMaxNo("GrpFirstPay",2))+1;
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mLCGrpContSchema.getManageCom());
        if (!tLDComDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLDComDB.mErrors);
            return false;
        }

        texttag.add("JetFormType", "ULI_57");
        // -------------------------------

        // 四位管理机构代码。
        String sqlusercom = "select comcode from lduser where usercode='" + mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") || comcode.equals("86000000"))
        {
            comcode = "86";
        }
        else if (comcode.length() >= 4)
        {
            comcode = comcode.substring(0, 4);
        }
        else
        {
            buildError("dealGrpContInfo", "操作员机构查询出错!");
            return false;
        }
        String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='" + comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        texttag.add("ManageComLength4", printcode);
        // -------------------------------

        // 客户端IP。
        texttag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        // -------------------------------

        // 预览标志。
        if ("batch".equals(mOperate))
        {
            texttag.add("previewflag", "0");
        }
        else
        {
            texttag.add("previewflag", "1");
        }

        LDComSchema tLDComSchema = tLDComDB.getSchema();
        texttag.add("BarCode1", mLOPRTManagerSchema.getPrtSeq());
        texttag.add("BarCodeParam1",
                "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("AppntName", mLCGrpContSchema.getGrpName()); //投保人名称

        texttag.add("SumPrem", cancelKeXueCount(new DecimalFormat("0.00").format(SumPrem)));
        if (StrTool.cTrim(mLCGrpContSchema.getCardFlag()).equals("0"))
        {
            String strSql = "select linkmanaddress1,linkmanzipcode1,linkman1 from lcgrpaddress where customerno='"
                    + mLCGrpContSchema.getAppntNo()
                    + "' and addressno=(select addressno from lcgrpappnt where grpcontno='"
                    + mLCGrpContSchema.getGrpContNo() + "')";
            SSRS tSSRS = q_exesql.execSQL(strSql);
            texttag.add("AppntAddr", tSSRS.GetText(1, 1)); //投保经办人
            texttag.add("AppntZipcode", tSSRS.GetText(1, 2)); //投保经办人
            texttag.add("HandlerName", tSSRS.GetText(1, 3)); //投保经办人
        }
        else
        {
            texttag.add("AppntAddr", PostalAddress); //投保人名称
            texttag.add("AppntZipcode", AppntZipcode); //投保人名称
            texttag.add("HandlerName", mLCGrpContSchema.getHandlerName()); //投保经办人
        }
        if (mLCGrpContSchema.getHandlerDate() != null)
        {
            String[] ApplyDate = mLCGrpContSchema.getHandlerDate().split("-");
            texttag.add("ApplyDate", ApplyDate[0] + "年" + ApplyDate[1] + "月" + ApplyDate[2] + "日"); //投保经办人
        }
        else
        {
            texttag.add("ApplyDate", ""); //投保经办人
        }
        texttag.add("Bank", getBankName(mLCGrpContSchema.getBankCode()));
        texttag.add("AccNo", mLCGrpContSchema.getBankAccNo());
        texttag.add("Account", mLCGrpContSchema.getAccName());

        texttag.add("AgentName", mLAAgentSchema.getName()); //代理人姓名
        texttag.add("AgentCode", mLAAgentSchema.getGroupAgentCode()); //代理人代码  modify by zjd 代理人编码显示集团统一工号
        texttag.add("AgentMobile", mLAAgentSchema.getPhone()); //代理人电话
        texttag.add("PrtNo", mLCGrpContSchema.getPrtNo()); //印刷号
        texttag.add("Letterservicename", tLDComSchema.getLetterServiceName()); //印刷号
        texttag.add("Address", tLDComSchema.getLetterServicePostAddress()); //印刷号
        texttag.add("ZipCode", tLDComSchema.getZipCode()); //印刷号
        texttag.add("Fax", tLDComSchema.getFax()); //印刷号
        texttag.add("Phone", tLDComSchema.getPhone()); //印刷号
        texttag.add("ServicePhone", tLDComSchema.getPhone()); //印刷号
        Date makeDate = (new FDate()).getDate(tLOPRTManagerDB.getMakeDate());
        texttag.add("Today", getDate(makeDate));
        //处理保险计划名称
        texttag.add("ContPlanCodeAndName", ContPlanInfo); //印刷号and EnterAccDate is  null
        String tempFeeSql = "select sum(PayMoney)  from ljtempfee where TempFeeType in ('0','1') and othernotype='4' and otherno='"
                + mLCGrpContSchema.getPrtNo() + "'";

        if (LJSPayPrem.equals("null") || LJSPayPrem.equals(""))
        {
            LJSPayPrem = "0.00";
            System.out.println("ULINewFirstGrpPayF1PBL.getPrintData()");
            System.out.println("344");
        }
        texttag.add("LJSPrem", cancelKeXueCount(LJSPayPrem));
        String LJAPayPrem = "";
        try
        {
            LJAPayPrem = q_exesql.getOneValue(tempFeeSql);
        }
        catch (Exception ex)
        {
            System.out.println("ULINewFirstGrpPayF1PBL.getPrintData()  \n--Line:348  --Author:YangMing");
            LJAPayPrem = "0.00";
        }
        if (LJAPayPrem.equals(""))
        {
            LJAPayPrem = "0.00";
        }
        
        texttag.add("LJAPrem", cancelKeXueCount(LJAPayPrem));
        //		System.out.println("测试 ："+ LJSPayPrem +"    " + LJAPayPrem );
        texttag.add("Diff", cancelKeXueCount(new DecimalFormat("0.00").format(Double.parseDouble(LJSPayPrem)
                - Double.parseDouble(LJAPayPrem))));
        //缴费方式
        String tPayMode = mLCGrpContSchema.getPayMode();
        if (tPayMode == null)
        {
            tPayMode = "1";
        }
        if (tPayMode.equals("1"))
        {
            tPayMode = "现金";
        }
        if (tPayMode.equals("2"))
        {
            tPayMode = "支票";
        }
        if (tPayMode.equals("3"))
        {
            tPayMode = "支票";
        }
        if (tPayMode.equals("4"))
        {
            tPayMode = "银行转账";
        }
        texttag.add("PayMode", tPayMode);
        texttag.add("PayToDate", "");
        //缴费日期与险种相关，把所有的险种和险种对应的交至日期
        //        String Today = mLOPRTManagerSchema.getMakeDate();
        //        String payToDateSql = "select date('" + Today + "')+15 day from dual";
        //        Today = q_exesql.getOneValue(payToDateSql);
        //
        //        texttag.add("PayToDate",
        //                    Today.split("-")[0] + "年" +
        //                    Today.split("-")[1] + "月" +
        //                    Today.split("-")[2] + "日");
        //        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
        //        texttag.add("Today",
        //                    mLOPRTManagerSchema.getMakeDate().split("-")[0] + "年" +
        //                    mLOPRTManagerSchema.getMakeDate().split("-")[1] + "月" +
        //                    mLOPRTManagerSchema.getMakeDate().split("-")[2] + "日");

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        //保存体检信息
        xmlexport.addListTable(tRiskInfoTable, RiskTitle); //保存体检信息

        ListTable tEndTable = new ListTable();
        tEndTable.setName("END");
        String[] tEndTitle = new String[0];
        xmlexport.addListTable(tEndTable, tEndTitle);

        xmlexport.outputDocumentToFile("D:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
        mLOPRTManagerSchema.setPrintTimes(mLOPRTManagerSchema.getPrintTimes() + 1);
        mResult.add(mLOPRTManagerSchema);

        return true;
    }

    /**
     * 得到通过机构代码得到机构名称
     *
     * @param strComCode String
     * @return java.lang.String
     */
    private String getComName(String strComCode)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();
    }

    private String getBankName(String strBankCode)

    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strBankCode);
        tLDCodeDB.setCodeType("bank");
        if (tLDCodeDB.getInfo())
        {
            return tLDCodeDB.getCodeName();
        }
        else
        {
            return null;
        }
    }

    private String getDate(Date date)
    {

        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
        return df.format(date);
    }
    
    private String cancelKeXueCount(String moneyString){
    	DecimalFormat df = new DecimalFormat("#,##0.00");
    	BigDecimal b = new BigDecimal(String.valueOf(moneyString));
    	return df.format(b.doubleValue());
    }

}
