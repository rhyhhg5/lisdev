package com.sinosoft.lis.f1print;


import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.f1print.*;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.sinosoft.lis.bq.*;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PContPausePrintBL implements PrintService{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private VData mResult = new VData();    
    private LCContSchema tLCContSchema = new LCContSchema();
    private LCPolSchema tLCPolSchema = new LCPolSchema();
    private LAAgentSchema tLAAgentSchema = new LAAgentSchema();
    private LDComSchema tLDComSchema = new LDComSchema();
    private LCAddressSchema tLCAddressSchema = new LCAddressSchema();
    private LJSPayGrpSchema tLJSPayGrpSchema = new LJSPayGrpSchema();
    private LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
    private LJSPayBDB tLJSPayBDB = new LJSPayBDB();
    private SSRS mSSRS;
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    //交费信息
    private int needPrt = -1 ;
    private String mOperate = "";
    private String showPaymode = "";
    private String strPayToDate = "";
    private String strPayDate = "";
    private String mDif = "";
    private String strPayCanDate = "";
    TextTag textTag = new TextTag(); //新建一个TextTag的实例tLJSPaySchema

    private Object tExeSQL;
    
    private String mflag = null;
    public PContPausePrintBL() {

    }


    //By WangJH

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        System.out.println("PContPausePrintBL begin");
        mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mflag = cOperate;
        mResult.clear();
        if (!getListData()) {
            return false;
        }

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }

        System.out.println("IndiDueFeePrintBL end");
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);

        LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();
        ttLOPRTManagerSchema = (LOPRTManagerSchema)cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(ttLOPRTManagerSchema.getStandbyFlag2());
        System.out.println(ttLOPRTManagerSchema.getStandbyFlag2()+"+++++++++++++++++++++++++++++++");
        //根据保单号得到保单对象
        LCContSet tLCContSet = tLCContDB.query();
        if(tLCContSet!=null && tLCContSet.size()>0)
        {
        	tLCContSchema = tLCContSet.get(1);
        }
        else
        {
        	 buildError("getInputData", "为空--VTS打印入口没有得到足够的信息！");
             return false;
        }
        //得到投保人联系电话
        LCAddressDB tLCAddressDB=new LCAddressDB();
        tLCAddressDB.setCustomerNo(tLCContSchema.getAppntNo());
        LCAddressSet tLCAddressSet = tLCAddressDB.query();
        if(tLCAddressSet!=null && tLCAddressSet.size()>0)
        {
        	tLCAddressSchema = tLCAddressSet.get(1);
        }
        else
        {
        	 buildError("getInputData", "为空--VTS打印入口没有得到足够的信息！");
             return false;
        }
        //tLDComSchema 得到机构信息
        LDComDB tLDComDB=new LDComDB();
        tLDComDB.setComCode(tLCContSchema.getManageCom());
        LDComSet tLDComSet = tLDComDB.query();
        if(tLDComSet!=null && tLDComSet.size()>0)
        {
        	tLDComSchema = tLDComSet.get(1);
        }
        else
        {
        	 buildError("getInputData", "为空--VTS打印入口没有得到足够的信息！");
             return false;
        }
        //tLCPolSchema 得到业务员信息
        LCPolDB tLCPolDB=new LCPolDB();
        tLCPolDB.setContNo(tLCContSchema.getContNo());
        LCPolSet tLCPolSet = tLCPolDB.query();
        if(tLCPolSet!=null && tLCPolSet.size()>0)
        {
        	tLCPolSchema = tLCPolSet.get(1);
        }
        else
        {
        	 buildError("getInputData", "为空--VTS打印入口没有得到足够的信息！");
             return false;
        }
        // tLAAgentSchema
        LAAgentDB tLAAgentDB=new LAAgentDB();
        tLAAgentDB.setAgentCode(tLCContSchema.getAgentCode());
        LAAgentSet tLAAgentSet = tLAAgentDB.query();
        if(tLAAgentSet!=null && tLAAgentSet.size()>0)
        {
        	tLAAgentSchema = tLAAgentSet.get(1);
        }
        else
        {
        	 buildError("getInputData", "为空--VTS打印入口没有得到足够的信息！");
             return false;
        }
        System.out.println("投保人客户号："+tLCContSchema.getAppntNo()+"+++++++++++++++++++++++++++++++投保人："+tLCContSchema.getAccName()
        		+"投保人联系电话："+tLCAddressSchema.getPhone()+"经办人："+tLCContSchema.getOperator());
        
        
        
        return true;
       

    }

    public VData getResult() {
        return this.mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }


    private boolean getPrintData() {
        String ContNo = tLCContSchema.getContNo();
        System.out.println("ContNo=" + ContNo);
        String GetNoticeNo = "";
        String sql = "";
        String sqlone = "";
        String sqltwo = "";
        String sql3 = "";
        String s1 = "";

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("IndiDueFeePrint.vts", "printer");
             
        String mCurrentDate = PubFun.getCurrentDate();//得到当前日期
        textTag.add("CurrentDate", mCurrentDate);//当前日期
        
        textTag.add("ContNo", ContNo);//保单号
        textTag.add("AppntNo", tLCContSchema.getAppntNo());//客户号
        textTag.add("AppntName", tLCContSchema.getAppntName());//投保人 
        
        
        String tOperator = mGlobalInput.Operator;//得到经办人
        textTag.add("Operator", tOperator);//经办人
        
        textTag.add("Mobile", tLCAddressSchema.getMobile());//投保人联系电话
        textTag.add("ZipCode", tLCAddressSchema.getZipCode());//投保人邮政编码
        textTag.add("PostalAddress", tLCAddressSchema.getPostalAddress());//投保人通讯地址
        textTag.add("ComName", tLDComSchema.getName());//机构名称
        textTag.add("ComAddress", tLDComSchema.getAddress());//机构地址
        textTag.add("ComZipCode", tLDComSchema.getZipCode());//机构邮编
        textTag.add("ComFax", tLDComSchema.getFax());//机构传真
        textTag.add("ComPhone", tLDComSchema.getPhone());//机构电话
        
        //tLCPolSchema  addTwoYear
        String tPolApplyDate=tLCPolSchema.getPolApplyDate();//投保单申请日
        String PaytoDate=tLCPolSchema.getPaytoDate();//投保终止效力日
        String fPaytoDate=tLCPolSchema.getPaytoDate();//投保复效日日
        String printStr="特别提醒您，您于"+toDate(tPolApplyDate)+"投保的"+tLCContSchema.getContNo()+"号已于"
                         +toDate(PaytoDate)+"终止效力，最后复效日期为"+addTwoYear(PaytoDate);
        
        textTag.add("printStr", printStr);//机构电话
        textTag.add("AgentCode", tLCContSchema.getAgentCode());//业务员代码 
        textTag.add("AgentName", tLAAgentSchema.getName());//业务员名字 
        textTag.add("AgentCom", tLAAgentSchema.getOldCom());//业务员部门
        textTag.add("AgentPhone", tLAAgentSchema.getPhone());//业务员联系电话 
        
        //确定投保人性别
        String sexCode = tLCContSchema.getAppntSex();
        String sex;
        if(sexCode == null)
        {
            sex = "先生/女士";
        }
        else if(sexCode.equals("0"))
        {
            sex = "先生";
        }
        else if(sexCode.equals("1"))
        {
            sex = "女士";
        }
        else
        {
            sex = "先生/女士";
        }
        textTag.add("LinkManSex", sex);

        if (textTag.size() > 0) {
            xmlexport.addTextTag(textTag);
        }
        //表头信息
        String[] title = {"险种代码", "险种名称", "保额（元）",  "保险期间（年）",
                          "缴费年期（年）", "保费（元）"};
        xmlexport.addListTable(getListTable(), title);
        ListTable tEndTable = new ListTable();
        tEndTable.setName("END");
        String[] tEndTitle = new String[0];
        xmlexport.addListTable(tEndTable, tEndTitle);
        
        xmlexport.outputDocumentToFile("C:\\","indiDueFeehuxl");
        mResult.clear();
        mResult.addElement(xmlexport);


       
       //放入打印列表
       LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
       tLOPRTManagerDB.setStandbyFlag2(tLCContSchema.getContNo());
       tLOPRTManagerDB.setCode("111");
       needPrt = tLOPRTManagerDB.query().size();
       System.out.println("-------------------------the size is "+needPrt+"--------------------------");
       if (needPrt == 0)
       { //没有数据，进行封装
           String tLimit = PubFun.getNoLimit(tLCContSchema.getManageCom());
           String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
           mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
           mLOPRTManagerSchema.setOtherNo(tLCContSchema.getContNo());
           mLOPRTManagerSchema.setOtherNoType("00");
           mLOPRTManagerSchema.setCode("111");
           mLOPRTManagerSchema.setManageCom(tLCContSchema.getManageCom());
           mLOPRTManagerSchema.setAgentCode(tLCContSchema.getAgentCode());
           mLOPRTManagerSchema.setReqCom(tLCContSchema.getManageCom());
           mLOPRTManagerSchema.setReqOperator(tLCContSchema.getOperator());
           mLOPRTManagerSchema.setPrtType("0");
           mLOPRTManagerSchema.setStateFlag("0");
           mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
           mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
           mLOPRTManagerSchema.setStandbyFlag2(tLCContSchema.getContNo()); //这里存放 （保单号）
        }
       else
       {
           mLOPRTManagerSchema.setPrintTimes(mLOPRTManagerSchema.getPrintTimes()+1);
       }
       mResult.add(mLOPRTManagerSchema);

       xmlexport.outputDocumentToFile("C:\\", "xmlwhl");
       return true;
    }
    //转化日期：将第一个-转为年，第二个-转为月
   private String toDate(String date)
   {
	   String[] dDate=date.split("-");
	   
	   return dDate[0]+"年"+dDate[1]+"月"+dDate[2]+"日";
   }
   //转化日期：将第一个-转为年，第二个-转为月,得到复效日，复效日为终止日加两年
   private String addTwoYear(String date)
   {
	   String[] dDate=date.split("-");
	   
	   return Integer.parseInt(dDate[0])+2+"年"+dDate[1]+"月"+dDate[2]+"日";
   }
   
   


    /**
     *  获取列表数据
     * @param 无
     * @return ListTable
     */
    private ListTable getListTable() {

        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[10];
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                info[j - 1] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        tListTable.setName("ZT");
        return tListTable;
    }

    /**
     * 查询列表显示数据
     * @param schema LCContSchema
     * @return String
     */

    private boolean getListData() {
        StringBuffer sql = new StringBuffer();
        sql.append(" select a.riskcode,(select b.riskname from lmrisk b where b.riskcode=a.riskcode),a.amnt,a.insuyear,(case when a.insuyearflag='Y' then '年' when a.insuyearflag='D' then '天' when a.insuyearflag='M' then '月' end),a.payendyear,(case when a.payendyearflag='Y' then '年' when a.payendyearflag='M' then '月' when a.payendyearflag='D' then '日' end)  ,a.prem from lcpol a ,lmriskapp c where  c.riskperiod = 'L' and c.riskcode=a.riskcode and ")
            .append("  a.ContNo='")
            .append(tLCContSchema.getContNo())
            .append("'");
        
        String tSQL = sql.toString();
        System.out.println(tSQL);
        ExeSQL tExeSQL = new ExeSQL();
        mSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

   

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealPrintMag() {
        MMap tMap = new MMap();
        tMap.put(mLOPRTManagerSchema, "INSERT");
        VData tVData = new VData();
        tVData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tVData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
            return false;
        }
        return true;
    }
}
