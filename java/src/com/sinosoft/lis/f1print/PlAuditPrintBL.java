package com.sinosoft.lis.f1print;

import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PlAuditPrintBL {
	 public CErrors mErrors = new CErrors();
	    private VData mResult = new VData();
	    private String strBeginDate="";     //统计起期
	    private String strEndDate="";       //统计止期
	    private GlobalInput mG = new GlobalInput();
	    private String mFlag = "";
	    private GlobalInput mGlobalInput = null ;
	    private String mManageCom ;

	    public PlAuditPrintBL() {
	    }

	    /**
	     传输数据的公共方法
	     */
	    public boolean submitData(VData cInputData, String cOperate) {

	        if (!cOperate.equals("PRINT")) {
	            buildError("submitData", "不支持的操作字符串");
	            return false;
	        }

	        // 得到外部传入的数据，将数据备份到本类中
	        if (!getInputData(cInputData)) {
	            return false;
	        }
	        mResult.clear();
	        if (!getPrintData()) {
	            return false;
	        }

	        return true;
	    }

	    private boolean getInputData(VData cInputData) {
	    	strBeginDate=(String) cInputData.get(0);
	    	strEndDate=(String) cInputData.get(1);
	    	mGlobalInput = (GlobalInput)cInputData.get(2);
	    	mManageCom = (String)cInputData.get(3);
	    	System.out.println("管理机构："+mManageCom);
	        return true;
	    }

	    public VData getResult() {
	        return this.mResult;
	    }

	    private void buildError(String szFunc, String szErrMsg) {
	        CError cError = new CError();
	        cError.moduleName = "PlAuditPrintBL";
	        cError.functionName = szFunc;
	        cError.errorMessage = szErrMsg;
	        this.mErrors.addOneError(cError);
	    }
	    private boolean getPrintData() 
	    {

			SSRS tSSRS = new SSRS();

			TextTag texttag = new TextTag(); // 新建一个TextTag的实例
			XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
			xmlexport.createDocument("PlAuditPrint.vts", "printer"); // 最好紧接着就初始化xml文档
			String strArr[] = new String[25];
			if (texttag.size() > 0) 
			{
				xmlexport.addTextTag(texttag);
			}
			String[] detailArr = new String[] {"序号","所属公司名称","承租方","出租方","房屋所有权证号","地址","房屋面积","租赁期间","出让方是否具有出租房屋的权利","租赁标的所占用的土地性质是否是出让","是否办理租赁登记","租赁用途","该房屋所占用的土地是否为集体用地","该房屋所占用的土地是否为农用地","租赁是否到期"};
			{
				String sql = " select * from (SELECT char(rownumber() over(order by Placeno)) as rolnum,a.Managecom,a.Comname,a.Lessor,a.Houseno,a.Address,"+
	               " (nvl(a.sigarea,0) + nvl(a.grparea,0) + nvl(a.bankarea,0) + nvl(a.healtharea,0) + nvl(a.servicearea,0) +nvl(a.manageroffice,0) + nvl(a.departpersno,0) + nvl(a.planfinance,0) + nvl(a.jointarea,0) + nvl(a.publicarea,0) +nvl(a.otherarea,0)) as sumarea,trim(char(a.Begindate) || '至' || char(a.Actualenddate)),"+
	               " (case a.Islessorright when '1' then '是' else '否' end),(case a.Ishouserent when '1' then '是' else '否' end),(case a.Isrentregister when '1' then '是' else '否' end),(case a.Isplaceoffice when '1' then '是' else '否' end),(case a.Ishousegroup when '1' then '是' else '否' end),(case Ishousefarm when '1' then '是' else '否' end),"+
	               " (case when '"+strEndDate+"'>a.actualenddate then '是' else '否' end) FROM LIPlaceRentInfo a where 1 = 1"+
	               "  and ManageCom like '"+mManageCom+"%'"
	               ;
				if(!"".equals(strBeginDate) && !"".equals(strEndDate))
				{
					sql += " and a.actualenddate>='"+strBeginDate+"' and a.Begindate<='"+strEndDate+"'";//统计起止期与租赁起止期的交集
				}
				sql += " and a.state <>'02' ) as aa";
				
				System.out.println("sql:"+sql);
				ExeSQL main_exesql = new ExeSQL();
			    SSRS main_ssrs = main_exesql.execSQL(sql);
				ListTable tDetailListTable = new ListTable(); // 明细ListTable
				tDetailListTable.setName("INFO");
				System.out.println(main_ssrs.MaxRow);//行数
	            System.out.println(main_ssrs.MaxCol);//列数
				for(int i=1;i<=main_ssrs.MaxRow;i++)
				{
					strArr = new String[15];
					for(int j=1;j<=main_ssrs.MaxCol;j++)
					{
						if(j==7){
	            			strArr[j - 1] = main_ssrs.GetText(i, j);
	        				if(strArr[j - 1]!=null && (!"".equals(strArr[j - 1])))
	        				{
	        					
	        					String strSum = new DecimalFormat("0.00").format(Double.valueOf(strArr[j - 1]));
	        				    strArr[j - 1] = strSum;
	        				 }
	            		}else{
	            			strArr[j - 1] = main_ssrs.GetText(i, j);
	            		}
					}
					tDetailListTable.add(strArr);
				}
				xmlexport.addListTable(tDetailListTable, detailArr); // 明细ListTable
																		// 放入xmlexport对象
			}

			mResult.clear();
			mResult.addElement(xmlexport);

			return true;

	    }
}
