package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCUWMasterDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCUWMasterSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCUWMasterSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class UWF1PCONFIRMBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private String mOperate = "";
    private VData mResult = new VData();
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private boolean PatchFlag = false;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mManageCom = new String();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LCUWMasterSchema mLCUWMasterSchema = new LCUWMasterSchema();
    private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();
//  private LCIssuePolSet mLCIssuePolSet =new LCIssuePolSet(); //问题件表

    public UWF1PCONFIRMBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        /*
            if( !cOperate.equals("PRINT") ) {
              buildError("submitData", "不支持的操作字符串");
              return false;
            }
         */
        mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!saveData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        UWF1PCONFIRMBL tUWF1PCONFIRMBL = new UWF1PCONFIRMBL();
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setOtherNo("86110020030110002734");
        VData tVData = new VData();
        tVData.addElement(tLOPRTManagerSchema);
        tUWF1PCONFIRMBL.submitData(tVData, "PRINT");
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                "LOPRTManagerSchema", 0));
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mOperate = mGlobalInput.Operator;
        mManageCom = mGlobalInput.ManageCom;

        if (mLOPRTManagerSchema == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (mLOPRTManagerSchema.getPrtSeq() == null)
        {
            buildError("getInputData", "没有得到足够的信息:印刷号不能为空！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean saveData()
    {

        //根据印刷号查询打印队列中的纪录
        //mLOPRTManagerSchema
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();

        if (mLOPRTManagerSchema.getStateFlag() == null)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中的数据，其打印状态为空！");
            return false;
        }
        if (mLOPRTManagerSchema.getStateFlag().trim().equals("1"))
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML",
                       "打印队列已超时，该通知书已被操作员：" + mLOPRTManagerSchema.getExeOperator() +
                       "打印");
            return false;
        }
        if (mLOPRTManagerSchema.getStateFlag().trim().equals("2"))
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML",
                       "打印队列已超时，该通知书已被操作员：" + mLOPRTManagerSchema.getExeOperator() +
                       "打印,并且已处于回收状态");
            return false;
        }

        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setDoneDate(CurrentDate);
        mLOPRTManagerSchema.setDoneTime(CurrentTime);
        mLOPRTManagerSchema.setExeCom(mManageCom);
        mLOPRTManagerSchema.setExeOperator(mOperate);

        if (mLOPRTManagerSchema.getPatchFlag() == null)
        {
            PatchFlag = false;
        }
        else if (mLOPRTManagerSchema.getPatchFlag().equals("0"))
        {
            PatchFlag = false;
        }
        else if (mLOPRTManagerSchema.getPatchFlag().equals("1"))
        {
            PatchFlag = true;
        }
        if (PatchFlag == false) //不用补打，则更新数据状态
        {
            LCPolSet tLCPolSet = new LCPolSet();
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setMainPolNo(mLOPRTManagerSchema.getOtherNo());
            tLCPolSet = tLCPolDB.query();
            if (tLCPolDB.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tLCPolDB.mErrors);
                buildError("outputXML", "在取得保单表的数据时发生错误");
                return false;
            }
            //更新核保主表
            LCUWMasterSet tmLCUWMasterSet = new LCUWMasterSet();
            for (int i = 1; i <= tLCPolSet.size(); i++)
            {
                LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
                tLCUWMasterDB.setProposalNo(tLCPolSet.get(i).getProposalNo());
                tmLCUWMasterSet = tLCUWMasterDB.query();
                if (tLCUWMasterDB.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
                    buildError("outputXML", "在取得(核保通知书)LCUWMaster的数据时发生错误");
                    return false;
                }
                if (tmLCUWMasterSet.size() > 0)
                {
                    mLCUWMasterSchema = tmLCUWMasterSet.get(1);
                    mLCUWMasterSchema.setPrintFlag("2");
                    if (mLCUWMasterSchema.getSpecFlag() != null)
                    {
                        //if(mLCUWMasterSchema.getSpecFlag().equals("1"))
                        //{
                        //    mLCUWMasterSchema.setSpecFlag("2");
                        // }
                        //sxy---将其注释(2003-07-28)
                        //(SpecFlag:0 ---非 1 ---是，但是没有回复 2 ---是，而且已经回复)
                    }
                    if (mLCUWMasterSchema.getChangePolFlag() != null)
                    {
                        //if(mLCUWMasterSchema.getChangePolFlag().equals("1"))
                        //{
                        //    mLCUWMasterSchema.setChangePolFlag("2");
                        //}
                        //sxy---将其注释(2003-07-28)
                        //(ChangePolFlag:0 ---非 1 ---是，但是没有回复 2 ---是，而且已经回复)
                    }
                    mLCUWMasterSchema.setModifyDate(CurrentDate);
                    mLCUWMasterSchema.setModifyTime(CurrentTime);
                    mLCUWMasterSet.add(mLCUWMasterSchema);
                }
            }
//        //更新问题件
//        LCIssuePolDB tLCIssuePolDB = new LCIssuePolDB();
//        LCIssuePolSet tLCIssuePolSet = new LCIssuePolSet();
//        tLCIssuePolDB.setProposalNo(mLOPRTManagerSchema.getOtherNo());
//        tLCIssuePolDB.setBackObjType("3");//返回类型是保户
//        tLCIssuePolDB.setNeedPrint("Y");//打印标记
//        tLCIssuePolSet.set(tLCIssuePolDB.query());
//        for(int i=1;i<=tLCIssuePolSet.size();i++)
//        {
//            LCIssuePolSchema tLCIssuePolSchema = new LCIssuePolSchema();
//            tLCIssuePolSchema=tLCIssuePolSet.get(i);
//            tLCIssuePolSchema.setNeedPrint("N");
//            mLCIssuePolSet.add(tLCIssuePolSchema);
//        }
        }

        mResult.add(mLOPRTManagerSchema);
        mResult.add(mLCUWMasterSet);
//    mResult.add(mLCIssuePolSet);
        UWF1PCONFIRMBLS tUWF1PCONFIRMBLS = new UWF1PCONFIRMBLS();
        tUWF1PCONFIRMBLS.submitData(mResult, mOperate);
        if (tUWF1PCONFIRMBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tUWF1PCONFIRMBLS.mErrors);
            buildError("saveData", "提交数据库出错！");
            return false;
        }
        return true;
    }

    /**
     * 得到通过机构代码得到机构名称
     * @param strComCode
     * @return
     * @throws Exception
     */
    private String getComName(String strComCode)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();
    }

    /**
     * 根据主险保单号码查询主险保单
     * @param tMainPolNo
     * @return LCPolSchema
     */
    private LCPolSchema queryMainPol(String tMainPolNo)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(tMainPolNo);
        tLCPolDB.setMainPolNo(tMainPolNo);
        LCPolSet tLCPolSet = new LCPolSet();
        tLCPolSet = tLCPolDB.query();
        if (tLCPolSet == null)
        {
            buildError("queryMainPol", "没有找到主险保单！");
            return null;
        }
        if (tLCPolSet.size() == 0)
        {
            buildError("queryMainPol", "没有找到主险保单！");
            return null;
        }
        return tLCPolSet.get(1);
    }

}