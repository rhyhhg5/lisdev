package com.sinosoft.lis.f1print;

import java.util.Date;

import com.sinosoft.lis.llcase.CaseFunPub;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LJSPayPersonSchema;
import com.sinosoft.lis.vschema.LJSPayPersonSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 查询保单效力中止清单程序</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft</p>
 * @author LiuYansong
 * @version 1.0
 */
public class ShowPolPauseBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    String mStartDate = "";
    String mEndDate = "";
    String mComCode = "";
    String mbeforeDate = "";
    LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();
    public ShowPolPauseBL()
    {}

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!CaseFunPub.checkDate(mStartDate, mEndDate))
        {
            return false;
        }
        mbeforeDate = getbeforeDate(mStartDate);
        mLJSPayPersonSet.set(queryData(mbeforeDate, mEndDate));
        mResult.clear();
        mResult.add(mLJSPayPersonSet);
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    private boolean getInputData(VData cInputData)
    {
        // 暂交费查询条件
        mStartDate = (String) cInputData.get(0);
        mEndDate = (String) cInputData.get(1);
        mComCode = (String) cInputData.get(2);
        return true;
    }

    //    得到前一天的日期
    private String getbeforeDate(String StartDate)
    {
        String tDate = "";
        FDate tFDate = new FDate();
        Date aDate = new Date();
        Date cDate = new Date();
        aDate = tFDate.getDate(StartDate);
        Date bDate = new Date();
        bDate = PubFun.calDate(aDate, -1, "D", cDate);
        tDate = tFDate.getString(bDate);
        return tDate;
    }

    private LJSPayPersonSet queryData(String StartDate, String EndDate)
    {
        LJSPayPersonSet tLJSPayPersonSet = new LJSPayPersonSet();
        //查询出所有的保单效力中止的主险信息
//    String main_sql = " select PolNo,PayDate,PayCount  from LJSPayPerson "
//                    +" where PayDate >='"+mbeforeDate+"' and PayDate <'"+mEndDate+"' and ManageCom like '"
//                    +mComCode+"%' and PolNo in (select MainPolNo from lcpol where MainPolNo = PolNo )" ;
//

        String main_sql = " select PolNo,PayDate,PayCount from LJSPayPerson "
                          +
                " where PolNo in ( select  OtherNo  from LOPrtManager where Code = '42' "
                          + " and MakeDate < '" + mEndDate +
                          "' and MakeDate >='" + mbeforeDate +
                          "' and ManageCom like '"
                          + mComCode + "%' ) ";

        System.out.println("查询的语句是" + main_sql);
        ExeSQL main_exesql = new ExeSQL();
        SSRS main_ssrs = main_exesql.execSQL(main_sql);
        if (main_ssrs.getMaxRow() <= 0)
        {
            CError tError = new CError();
            tError.moduleName = "RegisterBL";
            tError.functionName = "submitData";
            tError.errorMessage = "没有符合条件的数据";
            this.mErrors.addOneError(tError);

        }
        else
        {
            for (int count = 1; count <= main_ssrs.getMaxRow(); count++)
            {
                LJSPayPersonSchema tLJSPayPersonSchema = new LJSPayPersonSchema();
                tLJSPayPersonSchema.setPolNo(main_ssrs.GetText(count, 1));
                tLJSPayPersonSchema.setPayDate(main_ssrs.GetText(count, 2));
                tLJSPayPersonSchema.setPayCount(main_ssrs.GetText(count, 3));
                tLJSPayPersonSet.add(tLJSPayPersonSchema);
            }
        }
        return tLJSPayPersonSet;
    }

    public static void main(String[] args)
    {
        String aStartDate = "2004-5-1";
        String aEndDate = "2004-10-31";
        String aComCode = "86";
        ShowPolPauseBL aShowPolPauseBL = new ShowPolPauseBL();
        VData tVData = new VData();
        tVData.addElement(aStartDate);
        tVData.addElement(aEndDate);
        tVData.addElement(aComCode);
        aShowPolPauseBL.submitData(tVData, "SHOW");
    }
}