/**
 * <p>ClassName: HMComHospSetStaReportBL.java </p>
 * <p>Description: 分机构医院设置统计 </p>
 * <p>Copyright: Copyright (c) 2010 </p>
 * <p>Company: </p>
 * @author chenxw
 * @version 1.0
 * @CreateDate：2010-3-31
 */

package com.sinosoft.lis.f1print;

import java.lang.reflect.Array;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class HMClmCustomerReportBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	private String mOperate;/** 数据操作字符串 */
	
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	private TransferData mTransferData = new TransferData();	
	private String mManageCom = ""; 		// 统计机构 	
	private String mSaleChnl = ""; 			// 业务渠道 	
	private String mSaleChnlComName = ""; 			// 业务渠道 	
	private String mDudyKind = "";			// 责任类型 	
	private String mRiskCode = "";			// 险种 	
	private String mDisease = ""; 			// 病种 	
	private String mHospit = ""; 			// 医院 	
	private String mAgentCode = "";			// 业务员 	
	private String mBlackCus = "";			// 黑名单 	
	private String mGrpName = ""; 			// 单位名称 	
	private String mStartDate = ""; 		// 统计起期 	
	private String mEndDate = "";			// 统计止期 	
	private String mGrpContNo = "";			// 保单号码 	
	private String mGiveMoney = ""; 		// 住院定额给付金 	
	private String mHisMoney = ""; 			// 费用保险赔付
	private String mGiveDate = ""; 			// 住院定额住院天数
	private String mhospitCount = "";		// 保单年度住院次数 	
	private String mRiskAmnt = "";			// 风险保额
	

	public HMClmCustomerReportBL() {

	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = cInputData;
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}

		if (!prepareOutputData()) {
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();

		System.out.println("Start HMClmCustomerReportBL Submit...");

		if (!tPubSubmit.submitData(mInputData, null)) {
			this.mErrors.copyAllErrors(this.mErrors);
			return false;
		}

		mInputData = null;
		return true;

	}
	private boolean getInputData() 
	{
		System.out.println("=======HMClmCustomerReportBL.getInputData===开始");
		mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
		mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
		if (mGlobalInput == null) 
		{
			CError cError = new CError();
			cError.moduleName = "HMClmCustomerReportBL";
			cError.functionName = "getInputData";
			cError.errorMessage = "mGlobalInput 为空值";
			mErrors.addOneError(cError);
			return false;
		}
		
		if (mTransferData == null) {
			CError cError = new CError();
			cError.moduleName = "HMClmCustomerReportBL";
			cError.functionName = "getInputData";
			cError.errorMessage = "mTransferData 为空值";
			mErrors.addOneError(cError);
			return false;
		}
	
		mManageCom 	  = (String) mTransferData.getValueByName("ManageCom");
		mSaleChnl 	  = (String) mTransferData.getValueByName("SaleChnl");
		mSaleChnlComName 	  = (String) mTransferData.getValueByName("SaleChnlComName");
		mDudyKind 	  = (String) mTransferData.getValueByName("DudyKind");
		mRiskCode 	  = (String) mTransferData.getValueByName("RiskCode");
		mDisease	  = (String) mTransferData.getValueByName("Disease");
		mHospit 	  = (String) mTransferData.getValueByName("Hospit");
		mAgentCode 	  = (String) mTransferData.getValueByName("AgentCode");
		mBlackCus 	  = (String) mTransferData.getValueByName("BlackCus");
		mGrpName 	  = (String) mTransferData.getValueByName("GrpName");
		mStartDate 	  = (String) mTransferData.getValueByName("StartDate");
		mEndDate 	  = (String) mTransferData.getValueByName("EndDate");
		mGrpContNo 	  = (String) mTransferData.getValueByName("GrpContNo");
		mGiveMoney 	  = (String) mTransferData.getValueByName("GiveMoney");
		mHisMoney 	  = (String) mTransferData.getValueByName("HisMoney");  
		mGiveDate 	  = (String) mTransferData.getValueByName("GiveDate");
		mhospitCount  = (String) mTransferData.getValueByName("hospitCount");  
		mRiskAmnt 	  = (String) mTransferData.getValueByName("RiskAmnt");
		
		return true;
	}
	private boolean checkData() 
	{
		System.out.println("=======HMClmCustomerReportBL.checkData===开始");
		return true;
	}
	private boolean dealData() 
	{
		//报表打印
		if (!PrintData()) {
			return false;
		}
		return true;
	}
	/**
	 * 报表打印主程序
	 * 
	 * @return
	 */
	private boolean PrintData() 
	{
		XmlExport xmlExport = new XmlExport(); // 新建一个XmlExport的实例
		xmlExport.createDocument("HMClmCustomerReport.vts", ""); // 最好紧接着就初始化xml文档
		ListTable tlistTable = new ListTable();
		TextTag texttag = new TextTag();
		String strArr[] = null;
		tlistTable.setName("HMReport");
		
		String tChooseSql="";
		if(mGrpName != null && !"".equals(mGrpName))//单位名称
		{
			tChooseSql +=" and exists (select 1 from lcgrpcont where GrpName='"+ mGrpName +"' and grpcontno=b.grpcontno"
				+" union all select 1 from lbgrpcont where GrpName='"+ mGrpName +"' and grpcontno=b.grpcontno)";
		}
		if(mGrpContNo != null && !"".equals(mGrpContNo))//保单号
		{
			tChooseSql +=" and (b.grpcontno='"+ mGrpContNo +"' or b.contno='"+mGrpContNo+"')";
		}
		if(mRiskCode != null && !"".equals(mRiskCode))//医疗险种
		{
			tChooseSql +=" and a.riskcode='"+ mRiskCode +"'";
		}
		
		if(mDudyKind != null && !"".equals(mDudyKind))//责任类型
		{
			tChooseSql +=" and exists (select 1 from lmriskapp where riskcode=a.riskcode and RiskType3='"+ mDudyKind +"')";
		}
		if(mAgentCode != null && !"".equals(mAgentCode))//业务员
		{
			tChooseSql +=" and exists (select 1 from laagent where agentcode=b.agentcode and name='"+ mAgentCode +"')";
		}
		if(mDisease != null && !"".equals(mDisease))//病种
		{
			tChooseSql +=" and exists (select 1 from LLCaseCure where DiseaseCode='"+ mDisease +"' and caseno=a.caseno)";
		}
		if(mHospit != null && !"".equals(mHospit)) //医院
		{
			tChooseSql +=" and exists (select 1 from llfeemain where HospitalCode='"+ mHospit +"' and caseno=a.caseno)";
		}
		if("1".equals(mBlackCus))//黑名单
		{
			tChooseSql +=" and exists (select 1 from LDBlackList where BlackListNo=a.customerno)";
		}
		else if("2".equals(mBlackCus))//黑名单
		{
			tChooseSql +=" and not exists (select 1 from LDBlackList where BlackListNo=a.customerno)";
		}
		if("A".equals(mSaleChnl))//业务渠道
		{
			texttag.add("SaleChnlComName", "团险-社保补充");
			tChooseSql +=" and exists (select 1 from lcgrpcont lcc,lcgrppol lcp,lmriskapp lm where lcp.riskcode=lm.riskcode "
				+" and lcc.grpcontno=lcp.grpcontno  and lcc.grpcontno=a.grpcontno and lm.riskprop='G' and lcc.MarketType in ('3','5','7') union all"
				+" select 1 from lbgrpcont lbc,lbgrppol lbp,lmriskapp lm where lbp.riskcode=lm.riskcode "
				+" and lbc.grpcontno=lbp.grpcontno and lbc.grpcontno=a.grpcontno and lm.riskprop='G' and lbc.MarketType in ('3','5','7'))";
		}else if("B".equals(mSaleChnl))
		{
			texttag.add("SaleChnlComName", "团险-企业补充");
			tChooseSql +=" and exists (select 1 from lcgrpcont lcc,lcgrppol lcp,lmriskapp lm where lcp.riskcode=lm.riskcode "
				+" and lcc.grpcontno=lcp.grpcontno and lcc.grpcontno=a.grpcontno and lm.riskprop='G' and lcc.MarketType ='9' union all"
				+" select 1 from lbgrpcont lbc,lcgrppol lbp,lmriskapp lm where lbp.riskcode=lm.riskcode "
				+" and lbc.grpcontno=lbp.grpcontno and lbc.grpcontno=a.grpcontno and lm.riskprop='G' and lbc.MarketType ='9')";
		}else if("C".equals(mSaleChnl))
		{
			texttag.add("SaleChnlComName", "团险-其他");
			tChooseSql +=" and exists (select 1 from lcgrpcont lcc,lcgrppol lcp,lmriskapp lm where lcp.riskcode=lm.riskcode "
				+" and lcc.grpcontno=lcp.grpcontno and lcc.grpcontno=a.grpcontno and lm.riskprop='G' and lcc.MarketType in ('1','2','4','6','8','99') union all"
				+" select 1 from lbgrpcont lbc,lbgrppol lbp,lmriskapp lm where lbp.riskcode=lm.riskcode "
				+" and lbc.grpcontno=lbp.grpcontno and lbc.grpcontno=a.grpcontno  and lm.riskprop='G' and lbc.MarketType in ('1','2','4','6','8','99'))";
		}else if("D".equals(mSaleChnl))
		{
			texttag.add("SaleChnlComName", "个险业务");
			tChooseSql +=" and exists (select 1 from lcpol lc,lmriskapp lm where lc.riskcode=lm.riskcode "
				+" and lc.contno=a.contno and lm.riskprop='I' and lc.SaleChnl not in ('04','13') union all"
				+" select 1 from lbpol lb,lmriskapp lm where lb.riskcode=lm.riskcode "
				+" and lb.contno=a.contno and lm.riskprop='I' and lb.SaleChnl not in ('04','13'))";
		}
		if(mGiveMoney != null && !"".equals(mGiveMoney))//住院定额型保险给付金额
		{
			tChooseSql +=" and exists (select 1 from lmriskapp where risktype3='2' and riskcode=a.riskcode)"
				+" and exists (select sum(sumrealpay) from hmcustomer where contno=a.contno and riskcode=a.riskcode having sum(sumrealpay)>="+ mGiveMoney +")";
		}
		if(mHisMoney != null && !"".equals(mHisMoney))//医疗费用型保险累计赔付
		{
			tChooseSql +=" and exists (select 1 from lmriskapp where risktype3 in ('1','5') and riskcode=a.riskcode)"
				+" and exists (select sum(sumrealpay) from hmcustomer where contno=a.contno and riskcode=a.riskcode having sum(sumrealpay)>="+ mHisMoney +")";
		} 
		if(mGiveDate != null && !"".equals(mGiveDate))//住院定额型保险住院天数
		{
			tChooseSql +=" and exists (select 1 from lmriskapp where risktype3='2' and riskcode=a.riskcode)"
				+" and exists (select sum(realhospdate) from llfeemain,llcase where llfeemain.caseno=llcase.caseno and llcase.customerno=a.customerno having sum(realhospdate)>="+mGiveDate+")";
		} 
		if(mhospitCount != null && !"".equals(mhospitCount))//保单年度住院次数
		{
			tChooseSql +=" and exists (select count(distinct mainfeeno) from llfeemain,llcase where llfeemain.caseno=llcase.caseno and llcase.customerno=a.customerno having count(distinct mainfeeno)>="+mhospitCount+")";
		} 
		if(mRiskAmnt != null && !"".equals(mRiskAmnt)&&!"0".equals(mRiskAmnt))//医疗风险保额
		{
			tChooseSql +=" and b.amnt/(10000*"+mRiskAmnt+")>=(select salary from lcappnt where contno=a.contno fetch first 1 rows only)";
		} 
		String tNeedsql="select b.insuredno,b.insuredname,case when b.insuredidtype='0' then b.insuredidno else CHAR(InsuredBirthday) end , "
			 	+" '',(select phone from lcgrpappnt where b.appntno=customerno fetch first 1 rows only),"
			 	+" (select postaladdress from lcgrpappnt where b.appntno=customerno fetch first 1 rows only), "
			 	+" (select grpname from lcgrpcont where grpcontno=b.grpcontno fetch first 1 rows only),"
			 	+" (select name from laagent where b.agentcode=agentcode fetch first 1 rows only),b.agentcode,"
			 	+" (select phone from laagent where b.agentcode=agentcode fetch first 1 rows only),"
			 	+" coalesce(sum(a.sumrealpay),0),(select coalesce(sum(realhospdate),0) from llfeemain ,llcase where llfeemain.caseno=llcase.caseno and llcase.customerno=a.customerno and llfeemain.feetype='2'),"
			 	+" (select coalesce(count(distinct mainfeeno),0) from llfeemain ,llcase where llfeemain.caseno=llcase.caseno and llcase.customerno=a.customerno and llfeemain.feetype='2')"
				+" from hmcustomer a,lccont b" 
				+" where a.contno=b.contno  "
				+" and b.managecom like '"+ mManageCom +"%'"
				+" and b.cvalidate>='"+ mStartDate +"' and b.cinvalidate<='"+ mEndDate +"'"
				+ tChooseSql
				+" group by b.insuredno,b.insuredname,b.insuredidno,b.appntno,b.grpcontno,b.agentcode,a.customerno,insuredidno,InsuredBirthday,insuredidtype "
				+" union all"
				+" select b.insuredno,b.insuredname,case when b.insuredidtype='0' then b.insuredidno else CHAR(InsuredBirthday) end, "
			 	+" '',(select phone from lbgrpappnt where b.appntno=customerno fetch first 1 rows only),"
			 	+" (select postaladdress from lbgrpappnt where b.appntno=customerno fetch first 1 rows only), "
			 	+" (select grpname from lbgrpcont where grpcontno=b.grpcontno fetch first 1 rows only),"
			 	+" (select name from laagent where b.agentcode=agentcode fetch first 1 rows only),b.agentcode,"
			 	+" (select phone from laagent where b.agentcode=agentcode fetch first 1 rows only),"
			 	+" coalesce(sum(a.sumrealpay),0),(select coalesce(sum(realhospdate),0) from llfeemain ,llcase where llfeemain.caseno=llcase.caseno and llcase.customerno=a.customerno and llfeemain.feetype='2'),"
			 	+" (select coalesce(count(distinct mainfeeno),0) from llfeemain ,llcase where llfeemain.caseno=llcase.caseno and llcase.customerno=a.customerno and llfeemain.feetype='2')"
				+" from hmcustomer a,lbcont b" 
				+" where a.contno=b.contno  "
				+" and b.managecom like '"+ mManageCom +"%'"
				+" and b.cvalidate>='"+ mStartDate +"' and b.cinvalidate<='"+ mEndDate +"'"
				+ tChooseSql
				+" group by b.insuredno,b.insuredname,b.insuredidno,b.appntno,b.grpcontno,b.agentcode,a.customerno,insuredidno,InsuredBirthday,insuredidtype with ur";
	
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(tNeedsql);
		System.out.println("================"+tSSRS.getMaxRow());
		if(tSSRS.getMaxRow()<=0)
		{
			CError cError = new CError();
			cError.moduleName = "HMClmCustomerReportBL";
			cError.functionName = "getInputData";
			cError.errorMessage = "没有查询出符合条件的数据信息";
			mErrors.addOneError(cError);
			return false;
		}
		int NO = 0; //序号
		try 
		{
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) 
			{
				NO = NO + 1;
				strArr = new String[14];
				strArr[0] = String.valueOf(NO);
				strArr[1 ] = tSSRS.GetText(i,1 );
				strArr[2 ] = tSSRS.GetText(i,2 );
				strArr[3 ] = tSSRS.GetText(i,3 );
				strArr[4 ] = tSSRS.GetText(i,4 );
				strArr[5 ] = tSSRS.GetText(i,5 );
				strArr[6 ] = tSSRS.GetText(i,6 );
				strArr[7 ] = tSSRS.GetText(i,7 );
				strArr[8 ] = tSSRS.GetText(i,8 );
				strArr[9 ] = tSSRS.GetText(i,9 );
				strArr[10] = tSSRS.GetText(i,10);
				strArr[11] = tSSRS.GetText(i,11);
				strArr[12] = tSSRS.GetText(i,12); 
				strArr[13] = tSSRS.GetText(i,13); 
								
				tlistTable.add(strArr);
			}
		} catch (Exception e) {
			e.printStackTrace();
			CError cError = new CError();
			cError.moduleName = "HMClmCustomerReportBL";
			cError.functionName = "PrintData";
			cError.errorMessage = e.toString();
			mErrors.addOneError(cError);
			return false;
		}
				
		texttag.add("ManageComName", mManageCom);
		if(mSaleChnl == null || "".equals(mSaleChnl))
		{
			texttag.add("SaleChnlComName", "全部业务渠道");
		}
		texttag.add("StartDate", mStartDate);
		texttag.add("EndDate", mEndDate);
		texttag.add("Operator",mGlobalInput.Operator);
		texttag.add("MakeDate",PubFun.getCurrentDate());
		
		if (texttag.size() > 0) {
			xmlExport.addTextTag(texttag);
		}
		xmlExport.addListTable(tlistTable, strArr);
		xmlExport.outputDocumentToFile("e:\\", "test");
		mResult.clear();
		mResult.addElement(xmlExport);
		mResult.addElement(mTransferData);

		return true;
	}
	private boolean prepareOutputData() 
	{
		return true;
	}
	public VData getResult() {
		return this.mResult;
	}
	}