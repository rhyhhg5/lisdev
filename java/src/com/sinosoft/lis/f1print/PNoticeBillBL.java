package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author 刘岩松
 * function :print InputEfficiency or print CheckEfficiency Business Logic layer
 * @version 1.0
 * @date 2003-04-04
 */

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PNoticeBillBL
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /*
     * @define globe variable
     */
    private String strMngCom;
    //private String strAgentCode;
    private String strStartDate;
    private String strEndDate;
    //private String strFlag;
    //private String strStateFlag;
    private String strSQL;

    public PNoticeBillBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();

        if (!getPrintData())
        {
            return false;
        }
        return true;
    }

    /***
     * Name : getInputData()
     * function :receive data from jsp and check date
     * check :judge whether startdate begin enddate
     * return :true or false
     */
    private boolean getInputData(VData cInputData)
    {
        strMngCom = (String) cInputData.get(0);
        //strAgentCode = (String)cInputData.get(1);
        strStartDate = (String) cInputData.get(1);
        strEndDate = (String) cInputData.get(2);
        //strFlag      = (String)cInputData.get(4);
        //strStateFlag = (String)cInputData.get(5);
        //strAgentGroup = (String)cInputData.get(6);
        //strBranchGroup = (String)cInputData.get(7);
        strSQL = (String) cInputData.get(3);

        //System.out.println("管理机构是"+strMngCom);
        //System.out.println("代理人编码"+strAgentCode);
        //System.out.println("开始日期"+strStartDate);
        //System.out.println("结束日期"+strEndDate);
        //System.out.println("标志"+strFlag);
        //System.out.println("打印状态是"+strStateFlag);
        System.out.println("查询语句:" + strSQL);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    /********
     * name : buildError
     * function : Prompt error message
     * return :error message
     */

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PEfficiencyBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /********
     * name : getPrintData()
     * function :get print data
     * parameter : null
     * return :true or false
     */

    private boolean getPrintData()
    {
        ListTable tlistTable = new ListTable();
        tlistTable.setName("MODE");
        //String t_sql = "";                         //choose all Operator from LCPol between start date and end date
        /*if(strFlag.equals("0"))
             {
          System.out.println("开始执行strFlag＝"+strFlag+"的sql操作");
          t_sql = "select LOPRTManager.PrtSeq,LOPRTManager.code,LOPRTManager.AgentCode, LAAgent.Name"
                +" from LOPRTManager, LAAgent where( LOPRTManager.PrtSeq in (select max(PrtSeq) from LOPRTManager group by OtherNo,Code )) "
                +"and  LOPRTManager.DoneDate >='"
                +strStartDate+"' and LOPRTManager.DoneDate <= '"
                +strEndDate+"' and LOPRTManager.StateFlag = '"
                +strStateFlag+"' and LOPRTManager.ManageCom like'"
                +strMngCom+"%'  and LOPRTManager.AgentCode=LAAgent.AgentCode order by  AgentCode";
          System.out.println("您的t_sql的执行结果是"+t_sql);
             }
             if(strFlag.equals("1"))
             {
          System.out.println("开始执行strFlag＝"+strFlag+"的sql操作");
          t_sql = "select LOPRTManager.PrtSeq,LOPRTManager.code,LOPRTManager.AgentCode, LAAgent.Name"
                +" from LOPRTManager, LAAgent where (LOPRTManager.PrtSeq in (select max(PrtSeq) from LOPRTManager group by OtherNo,Code))"
                +" and LOPRTManager.DoneDate >= '"
                +strStartDate+"' and LOPRTManager.DoneDate <= '"
                +strEndDate+"' and LOPRTManager.StateFlag = '"
                +strStateFlag+"' and LOPRTManager.ManageCom like'"
                +strMngCom+"%' and LOPRTManager.AgentCode = '"
                +strAgentCode+"' and LOPRTManager.AgentCode=LAAgent.AgentCode order by  AgentCode";

          System.out.println("您的t_sql的执行结果是"+t_sql);
             } (sxy2003-11-05)*/
        ExeSQL exeSQL = new ExeSQL();
        SSRS ssrs = exeSQL.execSQL(strSQL);
        int i_count = ssrs.getMaxRow();
        if (i_count == 0)
        {
            CError tError = new CError();
            tError.moduleName = "PNoticeBillBL";
            tError.functionName = "getDutyGetClmInfo";
            tError.errorMessage = "在该时间段内没有打印信息，请确认起止日期是否正确！！！";
            this.mErrors.addOneError(tError);
            return false;
        }

        TextTag texttag = new TextTag(); //Create a TextTag instance
        XmlExport xmlexport = new XmlExport(); //Create a XmlExport instance
        xmlexport.createDocument("PNoticeBill.vts", "printer"); //appoint to a special output .vts file
        ListTable alistTable = new ListTable(); //Create a ListTable instance
        alistTable.setName("NOTICE"); //Appoint to a sepcial flag

        /*****
         * define variables used to remark
         * group,individual,banksupply,and error of all operators
         */
        System.out.println("满足条件的记录共有" + i_count + "条");
        for (int i = 1; i <= i_count; i++)
        {
            String[] cols = new String[7];
            String strCodeInfo = "其他";
            cols[6] = "";
            cols[5] = "";
            if (!(ssrs.GetText(i, 2).equals("") || ssrs.GetText(i, 2).equals(null)))
            {
                if (!ssrs.GetText(i, 1).equals("01"))
                {
                    cols[0] = ssrs.GetText(i, 1);
                    cols[2] = ssrs.GetText(i, 3);
                    cols[3] = ssrs.GetText(i, 4);
                    if (!(ssrs.GetText(i, 5) == null ||
                          ssrs.GetText(i, 5).equals("")))
                    {
                        cols[4] = ssrs.GetText(i, 5);
                    }
                    System.out.println("2003-05-2-16描述表中取出信息！！！！");
                    String strCodeName =
                            "Select CodeName From LDCode Where CodeType = 'printcodetype' and Code = '"
                            + ssrs.GetText(i, 2) + "'";
                    SSRS CodeName_ssrs = exeSQL.execSQL(strCodeName);
                    String tempCodeInfo = CodeName_ssrs.GetText(1, 1);
                    int mLength = tempCodeInfo.length();
                    System.out.println("该描述的长度是" + mLength);
                    if (mLength > 4)
                    {
                        strCodeInfo = tempCodeInfo.substring(0, mLength - 3);
                    }
                    else
                    {
                        strCodeInfo = tempCodeInfo;
                    }

                    System.out.println("查询出的描述信息是" + CodeName_ssrs.GetText(1, 1));
                    cols[1] = strCodeInfo;
                    alistTable.add(cols);
                }
            }
        }
        String[] col = new String[7];
        xmlexport.addDisplayControl("displaynotice");
        xmlexport.addListTable(alistTable, col);
        texttag.add("MngCom", strMngCom);
        texttag.add("StartDate", strStartDate);
        texttag.add("EndDate", strEndDate);
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        //xmlexport.outputDocumentToFile("e:\\","PNoticeBill");//输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

    /*
     * @function :in order to debug
     */
    public static void main(String[] args)
    {
        String tStrMngCom = "86";
        String tStrAgentCode = "0000000001";
        String tStartDate = "2003-05-01";
        String tEndDate = "2003-05-08";
        String tFlag = "0";
        String tStateFlag = "1";
        VData tVData = new VData();
        tVData.addElement(tStrMngCom);
        tVData.addElement(tStrAgentCode);
        tVData.addElement(tStartDate);
        tVData.addElement(tEndDate);
        tVData.addElement(tFlag);
        tVData.addElement(tStateFlag);

        PNoticeBillUI tPNoticeBillUI = new PNoticeBillUI();
        tPNoticeBillUI.submitData(tVData, "PRINT");
    }
}