package com.sinosoft.lis.f1print;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author LH
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

public class LAJXStatisticReportBL{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput() ;
    private String mStartDay = "";
    private String mEndDay = "";
    private String mYear = "";
    private String mManageCom = "";
    private String[][] mShowDataList = null;
    private String[] mDataList = null;
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
  /**
   * 传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate)
  {
      // 得到外部传入的数据，将数据备份到本类中
      if (!getInputData(cInputData)) {
          return false;
      }

      // 进行数据的必要验证
      if(!check())
      {
          return false;
      }

      // 进行数据查询
      if (!queryData()) {
          return false;
      }

      return true;
  }

  /**
   * 验证
   * @return boolean
   */
  private boolean check()
  {
      if(!mStartDay.substring(0,4).equals(mEndDay.substring(0,4)))
      {
          buildError("check", "[期初]与[期末]必须是同一年！");
          return false;
      }

      mYear = mStartDay.substring(0,4);

      return true;
  }

  /**
   * 得到表示数据列表
   * @return boolean
   */
  private boolean getDataList()
  {
      String tSQL = "";

      if(mManageCom==null||mManageCom.equals(""))
      {
      // 1、得到全部已开业的机构
      tSQL  = "select * from ldcom where Sign='1'";
      tSQL += "   and comcode like '86%' and length(trim(comcode))=8";
      tSQL += "   and comcode <> '86000000' and   comgrade='03' order by comcode";
      }
      else
      {
    	 tSQL  = "select * from ldcom where Sign='1'";
         tSQL += "   and comcode like '"+mManageCom+"%' and length(trim(comcode))=8";
         tSQL += "   and comcode <> '86000000' and   comgrade='03' order by comcode";
      }
      LDComDB tLDComDB = new LDComDB();
      LDComSet tLDComSet = new LDComDBSet();
      tLDComSet = tLDComDB.executeQuery(tSQL);
      if(tLDComSet.size()==0)
      {
          buildError("queryData", "没有附和条件的机构！");
          return false;
      }

      // 2、查询需要表示的数据
      String[][] tShowDataList = new String[tLDComSet.size()][5];
      for(int i=1;i<=tLDComSet.size();i++)
      {// 循环机构进行统计
          LDComSchema tLDComSchema = new LDComSchema();
          tLDComSchema = tLDComSet.get(i);
          tShowDataList[i-1][0] = tLDComSchema.getName();
          System.out.println(tLDComSchema.getComCode()+"/"+tShowDataList[i-1][0]);

          // 查询需要表示的数据
          if(!queryOneDataList(tLDComSchema.getComCode(),tShowDataList[i-1]))
          {
              System.out.println("["+tLDComSchema.getComCode()+"] 本机构数据查询失败！");
              return false;
          }
          mShowDataList = tShowDataList;
      }

      // 3、进行排序整理

      return true;
  }

  /**
   * 查询管理机构下的期初人数
   * @param pmManageCom String
   * @return int
   */
  private int getAgentCountByManageCom(String pmDate,String pmManageCom)
  {
      String tSQL = "";
      int tRtValue = 0;

      tSQL  = "select count(*)";
      tSQL += "  from laagent a";
      tSQL += " where a.ManageCom like '" + pmManageCom + "%'";
      tSQL += "   and a.employdate<='" + pmDate + "'";
      tSQL += "   and (a.outworkdate>'" + pmDate + "' or a.outworkdate is null )";
      tSQL += "   and a.Branchtype='1' and a.BranchType2='01'";

      try{
          tRtValue = (int) execQuery(tSQL);
      }catch(Exception ex)
      {
          System.out.println("getAgentCountByManageCom 出错！");
      }

      return tRtValue;
  }

  /**
   * 取得本期平均人力
   * @param pmValue1 int
   * @param pmValue2 int
   * @return double
   */
  private String getAverageAgentCount(String pmValue1,String pmValue2)
  {
      String tSQL = "";
      String tRtValue = "";

      // 判断被除数是0
      if("0".equals(pmValue2))
      {
          return "0";
      }

      // 取得平均值
//      tSQL = "select DECIMAL((" + pmValue1 + " + " + pmValue2 +
//             ") / 2 ,12,1) from dual";
      tSQL = "select DECIMAL(DECIMAL(" + pmValue1 + " + " + pmValue2 +
             ",12,2) / 2,12,2) from dual";

      try{
          tRtValue = String.valueOf(execQuery(tSQL));
      }catch(Exception ex)
      {
          System.out.println("getAverageAgentCount 出错！");
      }

      return tRtValue;
  }

  /**
   * 统计机构期内首年/续年保费
   * @param pmStartDate String
   * @param pmEndDate String
   * @param pmComCode String
   * @param pmStatType String  0：首年保费  1：续年保费
   * @return String
   */
  private String getFirstYearMoneyByManageCom(String pmStartDate,
                                              String pmEndDate,
                                              String pmComCode,
                                              String pmStatType)
  {
      String tSQL = "";
      String tRtValue = "";
      String tWhere = "";
      DecimalFormat tDF = new DecimalFormat("0.##");

      // 判断统计的是首年保费 还是续年保费
      if("0".equals(pmStatType))
      {
          tWhere = " and PayYear = 0 and renewcount=0  ";
      }
      else if("1".equals(pmStatType))
      {
          tWhere = " and PayYear > 0 and renewcount=0 ";
      }
      else if("2".equals(pmStatType))
      {
          tWhere = " and renewcount>=1";
      }
      tSQL  = "select sum(TransMoney) from lacommision";
      tSQL += " where ManageCom like '" + pmComCode + "%'";
      tSQL += tWhere;
      tSQL += "   and TMakeDate >= '" + pmStartDate + "'";
      tSQL += "   and TMakeDate <= '" + pmEndDate + "'";
      tSQL += "   and Branchtype='1' and BranchType2='03'";

      try{
          tRtValue = tDF.format(execQuery(tSQL));
      }catch(Exception ex)
      {
          System.out.println("getFirstYearMoneyByManageCom 出错！");
      }

      return tRtValue;
  }

  /**
   * 计算人均首年保费
   * @param pmFirstYearMoney String
   * @param pmAgentCont String
   * @return String
   */
  private String getFirstYearAverageMoney(String pmFirstYearMoney,String pmAgentCont)
  {
      String tSQL = "";
      String tRtValue = "";
      double tDbValue = 0.00;

      if(0.5 > Double.parseDouble(pmAgentCont))
      {
          return "0.0";
      }

//      tSQL = "select DECIMAL(" + pmFirstYearMoney + " / " + pmAgentCont +
//             " ,12,2) from dual";
      tSQL = "select DECIMAL(DECIMAL(" + pmFirstYearMoney + ",12,2) / DECIMAL(" +
             pmAgentCont + ",12,2),12,2) from dual";

      try{
          tDbValue = execQuery(tSQL);
      }catch(Exception ex)
      {
          System.out.println("getFirstYearAverageMoney 出错！");
      }

      tRtValue = String.valueOf(tDbValue);

      return tRtValue;
  }

  /**
   * 取得客户数
   * @param pmStartDate String
   * @param pmEndDate String
   * @param pmManageCom String
   * @return String
   */
  private String getAppCount(String pmStartDate,String pmEndDate,String pmManageCom)
  {
      String tSQL = "";
      String tRtValue = "";
      DecimalFormat tDF = new DecimalFormat("0.##");

      tSQL  = "SELECT count(distinct appntno)";
      tSQL += "  from LACommision a";
      tSQL += " where a.ManageCom like '" + pmManageCom + "%'";
      tSQL += "   and a.TMakeDate >= '" + pmStartDate + "'";
      tSQL += "   and a.TMakeDate <= '" + pmEndDate + "'";
      tSQL += "   and a.Branchtype='1' and a.BranchType2='03'";
      tSQL += "    and a.Paycount=0 and renewcount=0 ";

      try{
          tRtValue = "" + tDF.format(execQuery(tSQL));
      }catch(Exception ex)
      {
          System.out.println("getAppCount 出错！");
      }

      return tRtValue;
  }

  /**
   * 首年度件均保费
   * @param pmFirstYearMoney String
   * @param pmCaseCount String
   * @return String
   */
  private String getFirstYearAverageByCase(String pmFirstYearMoney,String pmCaseCount)
  {
      String tSQL = "";
      String tRtValue = "";
      DecimalFormat tDF = new DecimalFormat("0.##");

      if(0 == Integer.parseInt(pmCaseCount))
      {
          return "0";
      }

//      tSQL = "select DECIMAL(" + pmFirstYearMoney + " / " + pmCaseCount +
//             " ,12,2) from dual";
      tSQL = "select DECIMAL(DECIMAL(" + pmFirstYearMoney + ",12,2) / DECIMAL(" +
             pmCaseCount + ",12,2),12,2) from dual";

      try{
          tRtValue = "" + tDF.format(execQuery(tSQL));
      }catch(Exception ex)
      {
          System.out.println("getFirstYearAverageByCase 出错！");
      }

      return tRtValue;
  }




  /**
   * 取得分公司保费计划
   * @param pmStartDay String
   * @return String
   */
  private String getMoneyPlan(String pmComCode,String pmStartDay)
  {
      String tRtValue = "0";
      DecimalFormat tDF = new DecimalFormat("0.##");
      // 取得统计年月
      String tYearMonth = this.mStartDay.substring(0, 4)
                          + this.mStartDay.substring(5, 7);

      LAPlanUnitSet tLAPlanUnitSet = new LAPlanUnitSet();
      LAPlanUnitDB tLAPlanUnitDB = new LAPlanUnitDB();
      tLAPlanUnitDB.setPlanType("0");
      tLAPlanUnitDB.setPlanObject(pmComCode);
      tLAPlanUnitDB.setPlanPeriodUnit("12");
      tLAPlanUnitDB.setPlanStartYM(tYearMonth);
      tLAPlanUnitDB.setPlanItemType("01");

      if(tLAPlanUnitDB.getInfo())
      {
          tRtValue = "" + tDF.format(tLAPlanUnitDB.getPlanValue());
      }

      return tRtValue;
  }


  /**
   * 查询填充表示数据
   * @param pmComCode String
   * @param pmOneDataList String[]
   * @return boolean
   */
  private boolean queryOneDataList(String pmComCode,String[] pmOneDataList)
  {
      try{
          // 0、机构名称

          // 1、首年保费收入
          pmOneDataList[1] = getFirstYearMoneyByManageCom(mStartDay, mEndDay,
                  pmComCode, "0");
          // 2、续期保费收入
          pmOneDataList[2] = getFirstYearMoneyByManageCom(mStartDay, mEndDay,
                  pmComCode, "1");
          // 3、续保保费收入
          pmOneDataList[3] = getFirstYearMoneyByManageCom(mStartDay, mEndDay,
                  pmComCode, "2");
          // 4、新单客户数
          pmOneDataList[4] = getAppCount(mStartDay, mEndDay, pmComCode);

      }catch(Exception ex)
      {
          buildError("queryOneDataList", "准备数据时出错！");
          System.out.println(ex.toString());
          return false;
      }

      return true;
  }

  /**
   * 进行数据查询
   * @return boolean
   */
  private boolean queryData()
  {
      String tSQL = "";
      String CurrentDate = PubFun.getCurrentDate();//得到当天日期
      //String strArr[] = null;

      try{
          // 1、查询数据
          if(!getDataList())
          {
              return false;
          }

System.out.println(mShowDataList.length);
System.out.println(mShowDataList[0].length);
          this.mDataList = new String[mShowDataList[0].length];

//          // 2、进行排序  先进行保费计划达成率排序 再 进行活动率排名
//          LISComparator tLISComparator = new LISComparator();
//          tLISComparator.setNum(15);
//          Arrays.sort(mShowDataList, tLISComparator);
//          for (int j = 0; j < mShowDataList.length; j++) { // 追加序号
//              mShowDataList[j][16] = "" + (j + 1);
//          }
//
//          tLISComparator = new LISComparator();
//          tLISComparator.setNum(6);
//          Arrays.sort(mShowDataList,tLISComparator);
//          for(int j=0;j<mShowDataList.length;j++)
//          {// 追加序号
//              mShowDataList[j][7] = "" + (j + 1);
//          }

          // 3、追加 合计 行
          if(!setAddRow(mShowDataList.length + 1))
          {
             buildError("queryData", "进行合计计算时出错！");
             return false;
          }
          // 4、设置报表属性
          ListTable tlistTable = new ListTable();
          tlistTable.setName("LAJX");

          for(int i=0;i<mShowDataList.length;i++)
          {
              tlistTable.add(mShowDataList[i]);
          }
          tlistTable.add(mDataList);
          TextTag texttag = new TextTag();    //新建一个TextTag的实例
          texttag.add("MakeDate", currentDate);
          texttag.add("MakeTime", currentTime);
     //     texttag.add("tName", mManageName);
          texttag.add("tStartDate",mStartDay);
          texttag.add("tEndDate",mEndDay);

          XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
          xmlexport.createDocument("LAJXStatisticReport.vts", "printer"); //最好紧接着就初始化xml文档
          if (texttag.size() > 0)
              xmlexport.addTextTag(texttag);     //添加动态文本标签
          xmlexport.addListTable(tlistTable, mShowDataList[0]); //添加列表
          mResult.addElement(xmlexport);
      }catch (Exception ex)
      {
          buildError("queryData", "LAStatisticReportBL发生错误，准备数据时出错！");
          return false;
      }

      return true;
  }

  /**
   * 追加一条合计行
   * @return boolean
   */
  private boolean setAddRow(int pmRow)
  {
      System.out.println("合计行的行数是："+pmRow);

      mDataList[0] = "合  计";
      mDataList[1] = dealSum(1);
      mDataList[2] = dealSum(2);
      mDataList[3] = dealSum(3);
      mDataList[4] = dealSum(4);
      return true;
  }

  /**
   * 对传入的数组进行求和处理
   * @param pmArrNum int
   * @return String
   */
  private String dealSum(int pmArrNum)
  {
      String tReturnValue = "";
      DecimalFormat tDF = new DecimalFormat("0.##");
      String tSQL = "select 0";

      for(int i=0;i<this.mShowDataList.length;i++)
      {
          tSQL += " + " + this.mShowDataList[i][pmArrNum];
      }

      tSQL += " + 0 from dual";

      tReturnValue = "" + tDF.format(execQuery(tSQL));

      return tReturnValue;
  }

  /**
   * 取得传入的数据
   * @return boolean
   */
  private boolean getInputData(VData pmInputData)
  {
      //全局变量
//      mGlobalInput.setSchema((GlobalInput) pmInputData.getObjectByObjectName(
//              "GlobalInput", 0));
      mStartDay = (String) pmInputData.get(0);
      mEndDay = (String) pmInputData.get(1);
      mManageCom = (String) pmInputData.get(2);
      System.out.println(mStartDay+" / "+mEndDay);

      return true;
  }
  /**
   * 追加错误信息
   * @param szFunc String
   * @param szErrMsg String
   */
  private void buildError(String szFunc, String szErrMsg)
  {
      CError cError = new CError();
      cError.moduleName = "LAStatisticReportBL";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      System.out.println(szFunc + "--" + szErrMsg);
      this.mErrors.addOneError(cError);
  }

  /**
   * 执行SQL文查询结果
   * @param sql String
   * @return double
   */
  private double execQuery(String sql)
  {
      Connection conn;
      conn = null;
      conn = DBConnPool.getConnection();

      System.out.println(sql);

      PreparedStatement st = null;
      ResultSet rs = null;
      try {
          if (conn == null)return 0.00;
          st = conn.prepareStatement(sql);
          if (st == null)return 0.00;
          rs = st.executeQuery();
          if (rs.next()) {
              return rs.getDouble(1);
          }
          return 0.00;
      } catch (Exception ex) {
          ex.printStackTrace();
          return -1;
      } finally {
          try {
            if (!conn.isClosed()) {
                conn.close();
            }
            try {
                st.close();
                rs.close();
            } catch (Exception ex2) {
                ex2.printStackTrace();
            }
            st = null;
            rs = null;
            conn = null;
          } catch (Exception e) {}

      }
    }

  /**
   * 取得返回处理过的结果
   * @return VData
   */
  public VData getResult()
  {
      return this.mResult;
  }
}
