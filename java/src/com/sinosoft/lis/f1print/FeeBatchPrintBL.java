package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class FeeBatchPrintBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private TransferData mTransferData = new TransferData();
    private String mOperate="";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();


	String mInvoiceState = "";
	String mIncomeType = "";
	String mPrtNo = "";
	String mMngCom = "";
	String mSaleChnl = "";
    String mIncomeNo="";
    String mAgentCode="";
    String mBranchChar="";
    String mEnterAccDate="";
    String mEnterAccDateEnd="";
    
    

    public FeeBatchPrintBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mOperate=cOperate;
        if (!cOperate.equals("CONFIRM") &&
            !cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
         
        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
		mInvoiceState = (String) mTransferData.getValueByName("InvoiceState");
		mIncomeType = (String) mTransferData.getValueByName("IncomeType");
		mPrtNo = (String) mTransferData.getValueByName("PrtNo");
		mMngCom = (String) mTransferData.getValueByName("MngCom");
		mSaleChnl = (String) mTransferData.getValueByName("SaleChnl");
        mIncomeNo=(String) mTransferData.getValueByName("IncomeNo");	
        mAgentCode=(String) mTransferData.getValueByName("AgentCode");
        mBranchChar=(String) mTransferData.getValueByName("BranchChar");
        mEnterAccDate=(String) mTransferData.getValueByName("EnterAccDate");
        mEnterAccDateEnd=(String) mTransferData.getValueByName("EnterAccDateEnd");
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "FinGetDayBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        ListTable tlistTable = new ListTable();
        String strArr[] = new String[11];
        String mSql=find();
        if(this.mOperate.equals("PRINT")){
            
            tSSRS = tExeSQL.execSQL(mSql+" with ur ");
            tlistTable.setName("Get");
            for (int i = 1; i <= tSSRS.MaxRow; i++)
            { 
                strArr = new String[11];
                for (int j = 1; j <= tSSRS.MaxCol; j++)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                }
                tlistTable.add(strArr);
            }
        }
      
        
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        
        xmlexport.createDocument("FeePrintReport.vts", "printer");
        texttag.add("ManageCom", mGlobalInput.ManageCom);

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }
    
    public String  find(){
    	// 书写SQL语句
		String strSQL = "";
        
		strSQL = "select case IncomeType when '2' then '个险' when '1' then '团险' end IncomeType,b.prtno,IncomeNo,SumActuPayMoney,PayDate,a.ManageCom,getUniteCode(b.agentcode) AgentCode, ";
		strSQL +=" (select name from labranchgroup where agentgroup =b.agentgroup) group, ";
		strSQL +="case b.salechnl when '01' then '个险直销' when '02' then '团险直销' when '03' then '中介' when '04' then '银行代理' when '06' then '交差销售' when '07' then '职团开拓' when '08' then '其他' end ";
        if(this.mIncomeType.equals("2")){
            strSQL +=" from LJAPay a,lccont b where 1=1 and a.makedate>'2007-06-27' and a.IncomeNo=b.contno ";			 
        }else if(this.mIncomeType.equals("1")){
            strSQL +=" from LJAPay a,lcgrpcont b where 1=1 and a.makedate>'2007-06-27' and a.IncomeNo=b.grpcontno ";    
        }
          
        if(!this.mSaleChnl.equals("")){
		    strSQL=strSQL+ " and  b.SaleChnl='"+this.mSaleChnl+"' ";
		}
		if (!this.mPrtNo.equals(""))
		{
    		if (this.mIncomeType.equals("1"))
    		{
    			strSQL = strSQL + " and incomeno in (select grpcontno from lcgrpcont where prtno = '"+this.mPrtNo+"')";
    		}
    		else if (this.mIncomeType.equals("2")) 
    		{ 
    		  strSQL = strSQL + " and incomeno in (select contno from lccont where prtno = '"+this.mPrtNo+"')";
    		}
		}	
        if(!this.mIncomeType.equals("")){
            strSQL = strSQL + " and a.IncomeType='"+this.mIncomeType+"'";
        }
        if(!this.mIncomeNo.equals("")){
            strSQL = strSQL +" and a.IncomeNo='"+this.mIncomeNo+"'";
        }
        if(!this.mAgentCode.equals("")){
            strSQL = strSQL +" and b.AgentCode=getAgentCode('"+this.mAgentCode+"') ";
        }
        if(this.mInvoiceState.equals("1")){
            strSQL = strSQL  +" and a.payno not in (select otherno from LOPRTManager2 b where code='35' )";
        }else if(this.mInvoiceState.equals("2")){
            strSQL = strSQL  +" and a.payno in (select otherno from LOPRTManager2 b where code='35' )";
        }
        if(!this.mBranchChar.equals("")){
            strSQL = strSQL  +" and b.agentgroup in (select agentgroup from labranchgroup where branchattr ='"+this.mBranchChar+"')";
        }
        if(!this.mEnterAccDate.equals("")){
            strSQL+=" and a.EnterAccDate>='"+mEnterAccDate+"' ";
        }
        if(!this.mEnterAccDateEnd.equals("")){
            strSQL+=" and a.EnterAccDate<='"+mEnterAccDateEnd+"' ";
        }
        strSQL+=" and a.ManageCom like '"+mMngCom+"%' ";
        strSQL = strSQL + "order by a.IncomeType,b.prtno,a.IncomeNo";
        System.out.println(strSQL);
        return strSQL;
  	}
  	
}