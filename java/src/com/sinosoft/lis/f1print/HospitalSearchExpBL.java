package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: </p>
 * @author Zhousj
 * @version 1.0
 */
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;
import com.sinosoft.lis.f1print.*;
import java.lang.*;

public class HospitalSearchExpBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();
     private LHDiagnoSet mLHDiagnoSet = new LHDiagnoSet();

    public HospitalSearchExpBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("1 ...");
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        //System.out.println("zsj");
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量

        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        if (mTransferData == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "HospitalSearchExpBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {

        //TextTag texttag = new TextTag(); //新建一个TextTag的实例
        String MMngCom = (String) mTransferData.getValueByName("MngCom");
        String MHospitalType = (String) mTransferData.getValueByName(
                "HospitalType");
        String MHospitCode = (String) mTransferData.getValueByName("HospitCode");
        String MHospitName = (String) mTransferData.getValueByName("HospitName");
        String MAreaCode = (String) mTransferData.getValueByName("AreaCode");
        String MAreaName = (String) mTransferData.getValueByName("AreaName");
        String MCommunFixFlag = (String) mTransferData.getValueByName(
                "CommunFixFlag");
        String MAdminiSortCode = (String) mTransferData.getValueByName(
                "AdminiSortCode");
        String MEconomElemenCode = (String) mTransferData.getValueByName(
                "EconomElemenCode");
        String MLevelCode = (String) mTransferData.getValueByName("LevelCode");
        String MBusiTypeCode = (String) mTransferData.getValueByName(
                "BusiTypeCode");
        String MAssociateClass = (String) mTransferData.getValueByName(
                "AssociateClass");
        String MSpecialCode = (String) mTransferData.getValueByName(
                "SpecialCode");
        String MRiskcode = (String) mTransferData.getValueByName("Riskcode");
        String MICDCode = (String) mTransferData.getValueByName("ICDCode");
        String MContraState = (String) mTransferData.getValueByName(
                "ContraState");
        String MDutyName = (String) mTransferData.getValueByName("DutyName");
        String MDutyItem = (String) mTransferData.getValueByName("DutyItem");
        String MDutyItemState = (String) mTransferData.getValueByName(
                "DutyItemState");

        //得到客户基本信息

        // String aCorp="";//登陆公司
        String mlen;
        if (mGlobalInput.ManageCom.toString().length() == 8) {
            mlen = mGlobalInput.ManageCom.substring(0, 4);
        } else {
            mlen = mGlobalInput.ManageCom;
        }
        String SpecialCode = "";
        //System.out.println("MSpecialCode==>" + MSpecialCode);
        if (!StrTool.cTrim(MSpecialCode).equals("")) {
            SpecialCode = " and exists ( select 1 from  LDHospitalInfoIntro b where b.HospitCode=a.HospitCode and b.SpecialName = '" +
                          MSpecialCode + "')";
        }
        String Riskcode = "";

        if (!StrTool.cTrim(MRiskcode).equals("")) { //赔付险种类别不为空
            Riskcode = " and exists (select 1 from llfeemain b, llcase c "
                       +
                       " where a.HospitCode=b.HospitalCode and b.caseno = c.caseno and c.rgtstate in ('09','11','12')"
                       +
                       " and exists (select  distinct  c.Caseno from llclaimpolicy c where c.Caseno=b.caseno  "
                       + " and c.riskcode='" + MRiskcode + "'))";
        }

        String ICDCode = "";
        if (!StrTool.cTrim(MICDCode).equals("")) { //诊断疾病类别不为空
            ICDCode = " and exists ( select 1 from llfeemain b, llcase c "
                      + " where b.HospitalCode=a.HospitCode and b.caseno = c.caseno and c.rgtstate in ('09','11','12') "
                      +
                      "  and exists (select 1 from llcasecure c where  "
                      + " b.caserelano=c.caserelano and c.Diseasecode='" + MICDCode + "'))";
        }
        ListTable DiagListTable = new ListTable();
        DiagListTable.setName("DIAG");
        String[] Title_D = {"ld",
                           "HospitName", "HospitShortName", "CommunFixFlag",
                           "AssociateClass", "LevelCode",
                           "ManageCom", "Address", "Phone", "AdminiSortCode",
                           "EconomElemenCode", "BusiTypeCode"};

        try {
            SSRS tSSRS_D = new SSRS();
            String sql;
            Integer ii;
            if ( (MContraState != null && !MContraState.equals("")) || ( MDutyName != null && !MDutyName.equals("")) || ( MDutyItem != null && !MDutyItem.equals("")) || ( MDutyItemState != null && !MDutyItemState.equals("")))
            {
                //System.out.println("zsj1");
                //rownumber() over(),

                sql = "select int(rownumber() over())," +
                                      "a.HospitName, a.HospitShortName,"+
                                      "(select distinct codename from LDCode where CodeType = 'communfixflag'  and code = a.CommunFixFlag ),"+
                                      "(select distinct codename from LDCode where CodeType = 'llhospiflag'  and code = a.AssociateClass ),"+
                                     "(select distinct codename from LDCode where CodeType = 'hospitalclass'  and code = a.LevelCode ),"+
                                      "(select distinct name from LDCom where comcode=a.ManageCom),"+
                                      "a.Address,a.Phone,"+
                                      "(select distinct codename from LDCode where CodeType = 'hmadminisortcode'  and code = a.AdminiSortCode ),"+
                                      "(select distinct codename from LDCode where CodeType = 'hmeconomelemencode'  and code = a.EconomElemenCode ),"+
                                      "(select distinct codename from LDCode where CodeType = 'hmbusitype'  and code = a.BusiTypeCode ) "+
                                      " from LDHospital a,LHGroupCont b, LHContItem c " +
                                      "where 1=1 and  a.HospitCode=b.HospitCode and b.ContraNo=c.ContraNo "
                                      + SpecialCode
                                      + Riskcode
                                      + ICDCode
                                      // + SumFee
                                      + " and a.Managecom like '" + mlen + "%'"
                                      ;
                                if (MHospitalType != null && !MHospitalType.equals("")) {
                                    sql += "and a.HospitalType='" + MHospitalType + "'";
                                }
                                if (MHospitCode != null && !MHospitCode.equals("")) {
                                    sql += "and a.HospitCode='" + MHospitCode + "'";
                                }
                                if (MHospitName != null && !MHospitName.equals("")) {
                                    sql += "and a.HospitName='" + MHospitName + "'";
                                }
                                if (MAreaCode != null && !MAreaCode.equals("")) {
                                    sql += "and a.AreaCode='" + MAreaCode + "'";
                                }
                                if (MAreaName != null && !MAreaName.equals("")) {
                                    sql += "and a.AreaName='" + MAreaName + "'";
                                }
                                if (MMngCom != null && !MMngCom.equals("")) {
                                    sql += " and a.ManageCom='" + MMngCom + "'";
                                }
                                if (MCommunFixFlag != null && !MCommunFixFlag.equals("")) {
                                    sql += "and a.CommunFixFlag='" + MCommunFixFlag + "'";
                                }
                                if (MAdminiSortCode != null && !MAdminiSortCode.equals("")) {
                                    sql += "and a.AdminiSortCode='" + MAdminiSortCode + "'";
                                }
                                if (MEconomElemenCode != null && !MEconomElemenCode.equals("")) {
                                    sql += "and a.EconomElemenCode='" + MEconomElemenCode + "'";
                                }
                                if (MLevelCode != null && !MLevelCode.equals("")) {
                                    sql += "and a.LevelCode='" + MLevelCode + "'";
                                }
                                if (MAssociateClass != null && !MAssociateClass.equals("")) {
                                    sql += "and a.AssociateClass='" + MAssociateClass + "'";
                                }
            } else {
                //System.out.println("zsj2");
               // System.out.println("SpecialCode==>" + SpecialCode);
               //rownumber() over(),
                sql = " select int(rownumber() over())," +
                      "a.HospitName, a.HospitShortName,"+
                    "(select distinct codename from LDCode where CodeType = 'communfixflag'  and code = a.CommunFixFlag),"+
                                     "(select distinct codename from LDCode where CodeType = 'llhospiflag'  and code = a.AssociateClass),"+
                                    "(select distinct codename from LDCode where CodeType = 'hospitalclass'  and code = a.LevelCode),"+
                                     "(select distinct name from LDCom where comcode=a.ManageCom),"+
                                     "a.Address,a.Phone,"+
                                     "(select distinct codename from LDCode where CodeType = 'hmadminisortcode'  and code = a.AdminiSortCode),"+
                                     "(select distinct codename from LDCode where CodeType = 'hmeconomelemencode'  and code = a.EconomElemenCode),"+
                                     "(select distinct codename from LDCode where CodeType = 'hmbusitype'  and code = a.BusiTypeCode) "+
                     " from LDHospital a " +
                     "where 1=1 "
                     + " and a.Managecom like '" + mlen + "%' "
                     + SpecialCode
                     + Riskcode
                     + ICDCode
                     // + SumFee

                     ;
               if (MHospitalType != null && !MHospitalType.equals("")) {
                   sql += "and a.HospitalType='" + MHospitalType + "'";
               }
               if (MHospitCode != null && !MHospitCode.equals("")) {
                   sql += "and a.HospitCode='" + MHospitCode + "'";
               }
               if (MHospitName != null && !MHospitName.equals("")) {
                   sql += "and a.HospitName='" + MHospitName + "'";
               }
               if (MAreaCode != null && !MAreaCode.equals("")) {
                   sql += "and a.AreaCode='" + MAreaCode + "'";
               }
               if (MAreaName != null && !MAreaName.equals("")) {
                   sql += "and a.AreaName='" + MAreaName + "'";
               }
               if (MMngCom != null && !MMngCom.equals("")) {
                   sql += " and a.ManageCom='" + MMngCom + "'";
               }
               if (MCommunFixFlag != null && !MCommunFixFlag.equals("")) {
                   sql += "and a.CommunFixFlag='" + MCommunFixFlag + "'";
               }
               if (MAdminiSortCode != null && !MAdminiSortCode.equals("")) {
                   sql += "and a.AdminiSortCode='" + MAdminiSortCode + "'";
               }
               if (MEconomElemenCode != null && !MEconomElemenCode.equals("")) {
                   sql += "and a.EconomElemenCode='" + MEconomElemenCode + "'";
               }
               if (MLevelCode != null && !MLevelCode.equals("")) {
                   sql += "and a.LevelCode='" + MLevelCode + "'";
               }
               if (MBusiTypeCode != null && !MBusiTypeCode.equals("")) {
                   sql += "and a.BusiTypeCode='" + MBusiTypeCode + "'";
               }
               if (MAssociateClass != null && !MAssociateClass.equals("")) {
                   sql += "and a.AssociateClass='" + MAssociateClass + "'";
               }
               if (MDutyItem != null && !MDutyItem.equals("")) {
                   sql += "and c.DutyItem='" + MDutyItem + "'";
               }
               if (MContraState != null && !MContraState.equals("")) {
                   sql += "and b.ContraState='" + MContraState + "'";
               }
               if (MDutyName != null && !MDutyName.equals("")) {
                   sql += "and c.DutyItemCode='" + MDutyName + "'";
               }
               if (MDutyItemState != null && !MDutyItemState.equals("")) {
                   sql += "and c.DutyState='" + MDutyItemState + "'";
               }

            }
            System.out.println("SQL 11 :"+sql);
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS_D = tExeSQL.execSQL(sql);
            int count_D = tSSRS_D.getMaxRow();
            for (int i = 0; i < count_D; i++) {
                String temp_D[][] = tSSRS_D.getAllData();
                String[] strCol;
                strCol = new String[12];
                strCol[0] = temp_D[i][0];
                strCol[1] = temp_D[i][1];
                strCol[2] = temp_D[i][2];
                strCol[3] = temp_D[i][3];
                strCol[4] = temp_D[i][4];
                strCol[5] = temp_D[i][5];
                strCol[6] = temp_D[i][6];
                strCol[7] = temp_D[i][7];
                strCol[8] = temp_D[i][8];
                strCol[9] = temp_D[i][9];
                strCol[10] = temp_D[i][10];
                strCol[11] = temp_D[i][11];

                DiagListTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "HospitalSearchExpBL";
            tError.functionName = "createAddressNo";
            tError.errorMessage = "客户就诊信息查询失败";
            this.mErrors.addOneError(tError);
        }

        //其它模版上单独不成块的信息

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("HospitalSearchExp.vts", "printer"); //最好紧接着就初始化xml文档
        xmlexport.addListTable(DiagListTable, Title_D);
        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }


    public static void main(String[] args) {
        HospitalSearchExpBL tHospitalSearchExpBL = new HospitalSearchExpBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        TransferData tTransferData = new TransferData();
        tVData.addElement(tTransferData);
        tHospitalSearchExpBL.submitData(tVData, "PRINT");
        VData mVData = tHospitalSearchExpBL.getResult();


    }

    private void jbInit() throws Exception {
    }

}
