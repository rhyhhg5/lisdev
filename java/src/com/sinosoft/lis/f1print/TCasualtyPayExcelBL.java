package com.sinosoft.lis.f1print;

/**
 * <p>Title: TCasualtyPayExcelBL</p>
 * <p>Description:非现场业务审计  死伤医疗给付数据表 </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: sinosoft</p>
 * @author :  y
 * @date:2013-12-3
 * @version 1.0
 */
import java.math.BigDecimal;
import java.text.DecimalFormat;

import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class TCasualtyPayExcelBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	private String mDay[] = null; // 获取时间
	//取得文件路径
	private String tOutXmlPath="";
	private GlobalInput mGlobalInput = new GlobalInput(); // 全局变量
	private String mFilePath = "";
	private String mFileName = "";	//文件名称
	private String mManageCom = "";
	private String codealias = "";
	private String mRiskCode = "";
	private CSVFileWrite mCSVFileWrite = null;

	public TCasualtyPayExcelBL() {
	}

	public static void main(String[] args) {
		GlobalInput tG = new GlobalInput();
		tG.Operator = "001";
		tG.ManageCom = "86110000";
		VData vData = new VData();
		String[] tDay = new String[4];
		tDay[0] = "2006-03-01";
		tDay[1] = "2006-03-31";
		vData.addElement(tDay);
		vData.addElement(tG);

		TCasualtyPayExcelBL tC = new TCasualtyPayExcelBL();
		tC.submitData(vData, "PRINT");

	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("PRINT")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		if (!getInputData(cInputData)) {
			return false;
		}
		if (cOperate.equals("PRINT")) // 打印收费
		{
			if (!getPrintData()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) { 
		// 全局变量
		System.out.println("start TCasualtyPayExcelBL...TCasualtyPay");
		mDay = (String[]) cInputData.get(0);
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        tOutXmlPath=(String)cInputData.get(2);
        mFileName = (String)cInputData.get(3);
        mManageCom = (String)cInputData.get(4);
        mRiskCode = (String)cInputData.get(5);
		
		System.out.println("BL的打印日期："+mDay[0]+"至"+mDay[1]+";文件名mFileName："+mFileName);
		System.out.println("BL的tOutXmlPath:"+tOutXmlPath);
		
		if (tOutXmlPath == null || tOutXmlPath.equals("")) {
			buildError("getInputData", "没有获得文件路径信息！");
			return false;
		}
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		if(mFileName.equals("") || mFileName == null){
			buildError("getInputData", "没有获得文件名称信息！");
			return false;
		}
		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "TCasualtyPayExcelBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
	
	/**
	 * 新的打印数据方法
	 * 
	 * @return
	 */
	private boolean getPrintData() {
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		
		String Msql = "select Name from LDCom where ComCode='"+ mGlobalInput.ManageCom + "'"; // 管理机构
		tSSRS = tExeSQL.execSQL(Msql);
		String manageCom = tSSRS.GetText(1, 1);
		
    	mFilePath = tOutXmlPath;
    	mCSVFileWrite = new CSVFileWrite(mFilePath,mFileName);
		String[][] tTitle = new String[4][];
		tTitle[0] = new String[]{"非现场业务审计数据表4：死伤医疗给付"};
		tTitle[1] = new String[]{"金额：元"};
		tTitle[2] = new String[]{"统计日期："+mDay[0]+"至"+mDay[1]};
		tTitle[3] = new String[]{"管理机构","渠道代码","险种代码","保单号","印刷号","死伤医疗给付号","凭证类型","业务信息","凭证号","金额","投保人","投保日期","生效日期","过账日期","给付日期","保单状态","代理机构","代理人代码"};
		String[] tContentType = new String[]{"String","String","String","String","String","String","String","String","String","Number","String","String","String","String","String","String","String","String"};
		if(!mCSVFileWrite.addTitle(tTitle, tContentType)){
			return false;
		}
//		String sql = "SELECT a.manageCom AS managecom," + //管理机构-1
//				"a.saleChnl AS saleChnl," + //渠道代码-2
//				"a.riskCode AS riskCode," + //险种代码-3
//				"a.contno AS contno," + //保单号-4
//				"(SELECT prtno FROM lccont WHERE contno=a.contno " +
//				"UNION SELECT prtno FROM lbcont WHERE contno=a.contno " +
//				"UNION SELECT prtno FROM lcgrpcont WHERE grpcontno=a.contno " +
//				"UNION SELECT prtno FROM lbgrpcont WHERE grpcontno=a.contno fetch first row only) AS printno," + //印刷号-5
//				"(SELECT indexno FROM fiabstANDarddata WHERE serialno = a.serialno fetch first row only) AS indexno," + //死伤医疗给付号-6
//				"vouchertype AS vouchertype," + //凭证类型-7
//				"(SELECT vouchername FROM FIVoucherDef WHERE voucherid=a.certificateid) AS clASsInfo," + //业务信息-8
//				"(SELECT voucherid FROM interfacetable WHERE importtype = a.clASstype " +
//				"AND batchno = a.batchno " +
//				"AND depcode = a.managecom " +
//				"AND accountcode = a.accountcode " +
//				"AND chargedate = a.accountdate fetch first 1 rows only) AS voucherid," + //凭证号-9
//				"a.summoney AS summoney," + //金额-10
//				"(SELECT appntname FROM lccont WHERE contno=a.contno " +
//				"UNION SELECT appntname FROM lbcont WHERE contno=a.contno " +
//				"UNION SELECT grpname FROM lcgrpcont WHERE grpcontno=a.contno " +
//				"UNION SELECT grpname FROM lbgrpcont WHERE grpcontno=a.contno fetch first row only) AS appntname," + //投保人-11
//				"(SELECT PolApplyDate FROM lccont WHERE contno=a.contno " +
//				"UNION SELECT PolApplyDate FROM lbcont WHERE contno=a.contno " +
//				"UNION SELECT PolApplyDate FROM lcgrpcont WHERE grpcontno=a.contno " +
//				"UNION SELECT PolApplyDate FROM lbgrpcont WHERE grpcontno=a.contno fetch first row only) AS PolApplyDate," + //投保日期-12
//				"(SELECT cvalidate FROM lccont WHERE contno=a.contno " +
//				"UNION SELECT cvalidate FROM lbcont WHERE contno=a.contno " +
//				"UNION SELECT cvalidate FROM lcgrpcont WHERE grpcontno=a.contno " +
//				"UNION SELECT cvalidate FROM lbgrpcont WHERE grpcontno=a.contno fetch first row only) AS cvalidate," + //生效日期-13
//				"a.accountdate AS accountdate," + //过账日期-14
//				"CJYLJFDATE(a.serialno,a.accountdate,a.contno) as yljfdate," + //死伤医疗给付日期-15
//				"(SELECT stateflag FROM lccont WHERE contno=a.contno " +
//				"UNION SELECT stateflag FROM lbcont WHERE contno=a.contno " +
//				"UNION SELECT stateflag FROM lcgrpcont WHERE grpcontno=a.contno " +
//				"UNION SELECT stateflag FROM lbgrpcont WHERE grpcontno=a.contno fetch first row only) AS stateflag," + //保单状态-16
//				"(SELECT agentcom FROM lccont WHERE contno=a.contno " +
//				"UNION SELECT agentcom FROM lbcont WHERE contno=a.contno " +
//				"UNION SELECT agentcom FROM lcgrpcont WHERE grpcontno=a.contno " +
//				"UNION SELECT agentcom FROM lbgrpcont WHERE grpcontno=a.contno fetch first row only) AS agentcom," + //代理机构编码-17
//				"(SELECT agentcode FROM lccont WHERE contno=a.contno " +
//				"UNION SELECT agentcode FROM lbcont WHERE contno=a.contno " +
//				"UNION SELECT agentcode FROM lcgrpcont WHERE grpcontno=a.contno " +
//				"UNION SELECT agentcode FROM lbgrpcont WHERE grpcontno=a.contno fetch first row only) AS agentcode " + //代理人编码-18
//				"FROM FiVoucherDataDetail a WHERE 1=1 " +
//				"AND a.accountdate BETWEEN '"+mDay[0]+"' AND '"+mDay[1]+"'" +
//			    "AND a.accountcode IN ('6511020100','6511020200','7511020100','7511020200') " +
//			    "AND a.finitemtype = 'D' " +
//			    "AND a.checkflag = '00' ";
		
		StringBuffer sqlbuf = new StringBuffer();
		sqlbuf.append("SELECT X.managecom,X.salechnl,X.riskcode,X.contno,Y.prtno,X.indexno," +
			"X.vouchertype,X.classInfo,X.voucherid,X.summoney,Y.appntname,Y.polapplydate," +
			"Y.cvalidate,X.accountdate,X.yljfdate,Y.stateflag,Y.agentcom,Y.agentcode " +
			"FROM (" +
			"SELECT a.manageCom AS managecom,a.saleChnl AS saleChnl," +
			"a.riskCode AS riskCode,a.Contno AS contno,a.Vouchertype AS vouchertype," +
			"(SELECT indexno FROM fiabstANDarddata WHERE serialno = a.serialno fetch first row only) AS indexno," +
			"(SELECT vouchername FROM FIVoucherDef WHERE voucherid=a.certificateid fetch first row only) AS classInfo," +
			"(SELECT voucherid FROM interfacetable WHERE importtype = a.classtype " +
			"AND batchno = a.batchno " +
			"AND depcode = a.managecom " +
			"AND accountcode = a.accountcode " +
			"AND chargedate = a.accountdate fetch first 1 rows only) AS voucherid," +
			"a.Summoney AS summoney,a.Accountdate AS accountdate,a.Accountdate AS yljfdate " +
			"FROM FiVoucherDataDetail a " +
			"WHERE a.accountcode IN ('6511020100','6511020200','7511020100','7511020200') " +
			"AND a.finitemtype = 'D' " +
			"AND a.checkflag = '00' "); 
		if(!"".equals(mManageCom) && !"86".equals(mManageCom) && !"860000".equals(mManageCom)){
			String manageSql = "SELECT codealias FROM ficodetrans WHERE codetype='ManageCom' AND code='"+mManageCom+"' with ur";
			tSSRS = tExeSQL.execSQL(manageSql);
			codealias = tSSRS.GetText(1, 1);
			if(codealias!=null && !"".equals(codealias)){
				sqlbuf.append("AND a.managecom = '"+codealias+"' ");
			}
		}
		if(!"".equals(mRiskCode)){
			sqlbuf.append("AND a.riskcode = '"+mRiskCode+"' ");
		}
		sqlbuf.append("AND a.accountdate BETWEEN '"+mDay[0]+"' AND '"+mDay[1]+"') as X ," +
			"lateral(" +
			"SELECT Prtno AS prtno,Appntname AS appntname,PolApplyDate AS polapplydate,CValidate AS cvalidate,Stateflag AS stateflag,Agentcom AS agentcom,Agentcode AS agentcode FROM lccont WHERE contno=X.contno " +
			"UNION SELECT Prtno AS prtno,Appntname AS appntname,PolApplyDate AS polapplydate,CValidate AS cvalidate,Stateflag AS stateflag,Agentcom AS agentcom,Agentcode AS agentcode FROM lbcont WHERE contno=X.contno " +
			"UNION SELECT Prtno AS prtno,Grpname AS appntname,PolApplyDate AS polapplydate,CValidate AS cvalidate,Stateflag AS stateflag,Agentcom AS agentcom,Agentcode AS agentcode FROM lcgrpcont WHERE grpcontno=X.contno " +
			"UNION SELECT Prtno AS prtno,Grpname AS appntname,PolApplyDate AS polapplydate,CValidate AS cvalidate,Stateflag AS stateflag,Agentcom AS agentcom,Agentcode AS agentcode FROM lbgrpcont WHERE grpcontno=X.contno fetch first row only) " +
			"as Y ");
		
		RSWrapper rs = new RSWrapper();
		if(!rs.prepareData(null, sqlbuf.toString())){
			return false;
		}
		try {
			
			do {
				tSSRS = rs.getSSRS();
				int maxRow = tSSRS.getMaxRow();
				System.out.println("查询返回行数："+maxRow);
				if(tSSRS!=null && tSSRS.getMaxRow()>0){
					String[][] tContent = new String[maxRow][];
					for(int row=1; row<=maxRow;row++){
						String[] ListInfo = new String[18];
						for(int n=0;n< ListInfo.length;n++){
							ListInfo[n] = "";
						}
						for(int col=1;col<=tSSRS.getMaxCol();col++){
							if(col==10){
								ListInfo[col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
							}else{
								ListInfo[col-1] = tSSRS.GetText(row, col);
							}
						}
						tContent[row-1] = ListInfo;
					}
					mCSVFileWrite.addContent(tContent);
					if(!mCSVFileWrite.writeFile()){
						mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
						return false;
					}
				}else{
					String[][] tContent = new String[1][];
					String[] ListInfo = new String[18];
					for(int i=0;i<ListInfo.length;i++){
						ListInfo[i] = "";
					}
					tContent[0] = ListInfo;
					mCSVFileWrite.addContent(tContent);
					if(!mCSVFileWrite.writeFile()){
						mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
					}
				}
			} while (tSSRS!=null && tSSRS.getMaxRow()>0);
		} catch (Exception e) {
			// TODO: handle exception
		}finally{
			rs.close();
		}
	return true;
}

}
