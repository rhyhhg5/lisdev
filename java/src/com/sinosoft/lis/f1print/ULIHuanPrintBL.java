package com.sinosoft.lis.f1print;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bq.*;
import java.text.DecimalFormat;

//程序名称：ULIHuanPrintBL.java
//程序功能：万能缓交状态打印
//创建日期：2009-7-23
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
public class ULIHuanPrintBL implements PrintService 
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private VData mResult = new VData();

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    TextTag textTag = new TextTag(); //新建一个TextTag的实例tLJSPaySchema

    private String mOperator = ""; 
    
    private String mCurrentDate = PubFun.getCurrentDate();
    
    private String mContNo = ""; 
    
    private String mAppntNo = "";
    
    private String mManageCom = "";
    
    private String mOperate = "";
    
    private String mPrtSeqNo = "";
    
    ExeSQL mExeSQL = new ExeSQL();

    public ULIHuanPrintBL() 
    {
    }

    public boolean submitData(VData cInputData, String cOperate) 
    {
    	System.out.println("-----ULIHuanPrintBL print start-----");
    	this.mOperate = cOperate;
    	if (!getInputData(cInputData))
        {
            return false;
        }
    	
        // 准备所有要打印的数据
        if (!getPrintData()) 
        {
            return false;
        }

        //加入到打印列表
        if (!dealPrintMag()) 
        {
            return false;
        }
        
        System.out.println("-----ULIHuanPrintBL print end-----");
        	
        return true;
    }

	private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);
        if( mGlobalInput==null || mLOPRTManagerSchema==null )
        {
            CError tError = new CError();
            tError.moduleName = "ULIHuanPrintBL.java";
            tError.functionName = "getInputData";
            tError.errorMessage = "获取数据失败！";
            mErrors.addOneError(tError);
            return false;
        }
        mContNo = mLOPRTManagerSchema.getStandbyFlag1();
        mAppntNo = mLOPRTManagerSchema.getStandbyFlag2();
        mOperator = mGlobalInput.Operator;
        mManageCom = mGlobalInput.ComCode;
        String tLimit = PubFun.getNoLimit(mManageCom);
        mPrtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        mResult.clear();
        return true;
    }

    public VData getResult() 
    {
        return this.mResult;
    }

    public CErrors getErrors() 
    {
        return this.mErrors;
    }

    private boolean getPrintData()
    {
    	XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("ULIHuanPrint", "printer");
        textTag.add("JetFormType", "OM002");
        String sqlusercom = "select comcode from lduser where usercode='" + mOperator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") || comcode.equals("86000000")) 
        {
            comcode = "86";
        } 
        else if (comcode.length() >= 4) 
        {
            comcode = comcode.substring(0, 4);
        } 
        else 
        {
            CError.buildErr(this,"操作员机构查询出错！");
            return false;
        }

        String printcom ="select codename from ldcode where codetype='pdfprintcom' and code='"
                         +comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);
        textTag.add("ManageComLength4", printcode);
        textTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        if("batch".equals(mOperate)) 
        {
            textTag.add("previewflag", "0");
        } 
        else 
        {
            textTag.add("previewflag", "1");
        }
        
        setPrintInfo();//设置打印信息
        setAppntAddress();//设置客户联系地址
        setServiceAddress();//设置分公司联系地址
        setContents();//设置通知书内容
        setAgent();//设置业务员
        
        xmlexport.addTextTag(textTag);
        String[] title = {"险种代码","险种名称","缴费年期","期交保费"};
        xmlexport.addListTable(getListTable(), title);
        ListTable xEndTable = new ListTable();
        xEndTable.setName("END");
        String[] xEndTitle = new String[0];
        xmlexport.addListTable(xEndTable, xEndTitle);

        xmlexport.outputDocumentToFile("C:\\","zmgtest");
        mResult.addElement(xmlexport);
        return true;
    }

    private boolean dealPrintMag()
    {
        mLOPRTManagerSchema.setPrtSeq(mPrtSeqNo);
        mLOPRTManagerSchema.setOtherNo(mContNo);
        mLOPRTManagerSchema.setOtherNoType("OM");
        mLOPRTManagerSchema.setCode("OM002");
        mLOPRTManagerSchema.setManageCom(mManageCom);
        mLOPRTManagerSchema.setAgentCode(mGlobalInput.AgentCom);
        mLOPRTManagerSchema.setReqCom(mGlobalInput.ManageCom);
        mLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mResult.addElement(mLOPRTManagerSchema);
        return true;
    }

    //设置打印信息
    private void setPrintInfo()
    {
    	textTag.add("Operator", mOperator);
    	String tPrintDate = CommonBL.decodeDate(mCurrentDate);
    	textTag.add("PrintDate", tPrintDate);
    	textTag.add("BarCode1", mPrtSeqNo);//条形码
    	textTag.add("Yuliu1","Yuliu2");//预留1
        textTag.add("Yuliu2","Yuliu2");//预留2
        textTag.add("Yuliu3","Yuliu3");//预留3
    }
    //设置客户联系地址
    private void setAppntAddress()
    {
    	StringBuffer sql = new StringBuffer(128);
    	sql.append("select a.appntname,case when (phone is not null and phone <> '') then phone ")
    	   .append("when (mobile is not null and mobile <> '') then mobile else mobile2 end, postaladdress,zipcode ")
    	   .append("from lcappnt a,lcaddress b ")
    	   .append("where a.appntno = b.customerno and a.addressno = b.addressno and appntno = '")
    	   .append(mAppntNo).append("' ");
    	System.out.println("客户联系地址："+sql.toString());
    	SSRS tSSRS = new SSRS();
    	tSSRS = mExeSQL.execSQL(sql.toString());
    	textTag.add("CustomerNo", mAppntNo);
    	textTag.add("CustomerName", tSSRS.GetText(1, 1));
    	textTag.add("CustomerPhone", tSSRS.GetText(1, 2));
    	textTag.add("CustomerAddress", tSSRS.GetText(1, 3));
    	textTag.add("CustomerZipCode", tSSRS.GetText(1, 4));
    }
    //设置分公司联系地址
    private void setServiceAddress()
    {
    	StringBuffer sql = new StringBuffer(128);
    	sql.append("select letterservicename,servicepostaddress,servicepostzipcode,fax,servicephone ")
    	   .append("from ldcom a where comcode = '").append(mManageCom).append("' ");
    	System.out.println("分公司联系地址："+sql.toString());
    	SSRS tSSRS = new SSRS();
    	tSSRS = mExeSQL.execSQL(sql.toString());
    	textTag.add("ServiceName", tSSRS.GetText(1, 1));
    	textTag.add("ServiceAddress", tSSRS.GetText(1, 2));
    	textTag.add("ServiceZipCode", tSSRS.GetText(1, 3));
    	textTag.add("ServiceFax", tSSRS.GetText(1, 4));
    	textTag.add("ServicePhone", tSSRS.GetText(1, 5));
    }
    
    //设置通知书内容
    private void setContents()
    {
    	String tContent1="";//称呼
    	String tContent2="";//警告
    	String tContent3="";//处理
    	
    	String sql = "select paytodate,appntname,appntsex,prem from lccont where contno = '" + mContNo + "' ";
    	SSRS tSSRS = new SSRS();
    	tSSRS = mExeSQL.execSQL(sql);
    	tContent1 = "尊敬的" + tSSRS.GetText(1, 2) + CommonBL.decodeSex(tSSRS.GetText(1, 3));
    	System.out.println(tContent1);
    	
    	sql = "select paydate,getnoticeno from ljspayb where otherno = '" + mContNo + "' " 
    	    + "and othernotype = '2' and dealstate = '2' order by makedate desc ";
    	SSRS tSSRS1 = new SSRS();
    	tSSRS1 = mExeSQL.execSQL(sql);
    	String tPayDate = CommonBL.decodeDate(tSSRS1.GetText(1, 1));
    	String tGetNoticeno = tSSRS1.GetText(1, 2);
    	tContent2 = "根据您与本公司签订的" + mContNo + "号保单的约定，您在我公司投保的前述保单应于" + tPayDate
    			  + "之前交纳期缴保费" + tSSRS.GetText(1, 4) + "元（《个险续期续保通知书》编号：" + tGetNoticeno + "）。";
    	System.out.println(tContent2);
    	
    	String tPayToDate = CommonBL.decodeDate(tSSRS.GetText(1, 1));
    	tContent3 = "截至" + tPayDate + "，我公司尚未收到以上保费。保单" + mContNo 
    	          + "已于" + tPayToDate + "进入保费缓缴状态，特此通知。";
    	System.out.println(tContent3);
    	textTag.add("Content1", tContent1);
    	textTag.add("Content2", tContent2);
    	textTag.add("Content3", tContent3);
    }
    
    //获得险种信息
    private ListTable getListTable()
    {
    	String sql = "select riskcode, (select riskname from lmrisk where riskcode = a.riskcode), "
    		       + "trim(char(year(payenddate)-year(cvalidate)))||'年', "
    		       + "prem from lcpol a where contno = '" + mContNo + "' order by riskcode";
    	System.out.println("险种信息："+sql.toString());
    	SSRS tSSRS = new SSRS();
    	tSSRS = mExeSQL.execSQL(sql);
    	ListTable tListTable = new ListTable();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) 
        {
            String[] info = new String[tSSRS.getMaxCol()];
            for (int j = 1; j <= tSSRS.getMaxCol(); j++) 
            {
                info[j - 1] = tSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        tListTable.setName("ULIRisk");
        return tListTable;
    }
    
    //设置业务员
    private void setAgent()
    {
    	StringBuffer sql = new StringBuffer(128);
    	sql.append("select name,getUniteCode(agentcode), (select name from labranchgroup where agentgroup = a.agentgroup), ")
    	   .append("case when (phone is not null and phone <> '') then phone else mobile end ")
    	   .append("from laagent a where agentcode = (select agentcode from lccont where contno = '").append(mContNo).append("') ");
    	System.out.println("业务员："+sql.toString());
    	SSRS tSSRS = new SSRS();
    	tSSRS = mExeSQL.execSQL(sql.toString());
    	textTag.add("AgentName", tSSRS.GetText(1, 1));
    	textTag.add("AgentCode", tSSRS.GetText(1, 2));
    	textTag.add("AgentGroup", tSSRS.GetText(1, 3));
    	textTag.add("AgentPhone", tSSRS.GetText(1, 4));
    }
}




