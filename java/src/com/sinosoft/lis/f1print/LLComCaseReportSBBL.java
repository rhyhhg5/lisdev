package com.sinosoft.lis.f1print;

import java.io.InputStream;
import java.text.DecimalFormat;

import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;


public class LLComCaseReportSBBL {
	  public CErrors mErrors = new CErrors();

	    private VData mResult = new VData();

	    /** 全局变量 */
	    private GlobalInput mGlobalInput = new GlobalInput();
	    private VData mInputData = new VData();
	    private TransferData mTransferData = new TransferData();
	    private XmlExport mXmlExport = null;
	    private CSVFileWrite mCSVFileWrite = null;

	    private String mFileNameB = "";
	    private String mManageComNam = "";
	    private String mManageCom = "";
	    private String mStartDate = "";
	    private String mEndDate = "";
	    private String mOperator = "";
	    private String mOperate = "";
	    private String mFilePath = "";
	    private String mFileName = "";	    
	    /** 统计类型 */
		private String mStatsType = "";
		
	    /**
	     * 传输数据的公共方法
	     */
	    public boolean submitData(VData cInputData, String cOperate) {

	        mOperate = cOperate;
	        mInputData = (VData) cInputData;
	        if (mOperate.equals("")) {
	            this.bulidError("submitData", "数据不完整");
	            return false;
	        }

	        if (!mOperate.equals("PRINT")) {
	            this.bulidError("submitData", "数据不完整");
	            return false;
	        }

	        // 得到外部传入的数据，将数据备份到本类中
	        if (!getInputData(mInputData)) {
	            return false;
	        }
	        System.out.println("BBBXXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
	                           + this.mManageCom);
	        
	    
	        // 进行数据查询
	        if (!queryDataToCVS()){
	            return false;
	        } else {

	        }
	        System.out.println("数据查询完毕！");
	        return true;
	    }

	    private void bulidError(String cFunction, String cErrorMsg) {

	        CError tCError = new CError();

	        tCError.moduleName = "LLCasePolicy";
	        tCError.functionName = cFunction;
	        tCError.errorMessage = cErrorMsg;

	        this.mErrors.addOneError(tCError);

	    }

	    /**
	     * 取得传入的数据 如果没有传入管理机构和起止如期则查全部机构全年的信息
	     *
	     * @return boolean
	     */
	    private boolean getInputData(VData cInputData) {
	        try {
	            mGlobalInput.setSchema((GlobalInput) cInputData
	                                   .getObjectByObjectName("GlobalInput", 0));
	            mTransferData = (TransferData) cInputData.getObjectByObjectName(
	                    "TransferData", 0);
	            // 页面传入的数据 三个
	            mOperator = (String) mTransferData.getValueByName("tOperator");
	            mFileNameB = (String) mTransferData.getValueByName("tFileNameB");
	            System.out.println("FileNameB:"+mFileNameB);
	          
	            this.mManageCom = (String) mTransferData
	                              .getValueByName("MngCom");

	            System.out.println("XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
	                               + this.mManageCom);
	            this.mStartDate = (String) mTransferData
	                              .getValueByName("tStartDate");//结案起始日期
	            this.mEndDate = (String) mTransferData.getValueByName("tEndDate");//结案截止日期
	            this.mStatsType = (String) mTransferData.getValueByName("StatsType");// 统计类别
	            System.out.println("mStatsType:"+mStatsType);
	            
	            
	            if (mManageCom == null || mManageCom.equals("")) {
	            	 this.mErrors.addOneError("管理机构获取失败");
	            	 return false;
	            }
	            if ("8600".equals(mManageCom)) {
	            	mManageCom="86";
	            }
	            mManageComNam=getManagecomName(mManageCom);
	            

	            System.out
	                    .println(
	                            "XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
	                            + mManageCom);
	        } catch (Exception ex) {
	            this.mErrors.addOneError("");
	            return false;
	        }
	        return true;
	    }
	 
	    private boolean queryDataToCVS()
	    {
	    	
	      int datetype = -1;
	      LDSysVarDB tLDSysVarDB = new LDSysVarDB();
	    	tLDSysVarDB.setSysVar("LPCSVREPORT");

	    	if (!tLDSysVarDB.getInfo()) {
	    		buildError("queryData", "查询文件路径失败");
	    		return false;
	    	}
	    	
	    	mFilePath = tLDSysVarDB.getSysVarValue(); 
	        //   	本地测试
	    	//mFilePath = "F:\\picch\\ui\\vtsfile\\";
	    	if (mFilePath == null || "".equals(mFilePath)) {
	    		buildError("queryData", "查询文件路径失败");
	    		return false;
	    	}
	    	System.out.println("XXXXXXXXXXXXXXXXXXXmFilePath:"+mFilePath);
	    	String tTime = PubFun.getCurrentTime3().replaceAll(":", "");
	    	String tDate = PubFun.getCurrentDate2();

	    	mFileName = "WJAJSXBB" + mGlobalInput.Operator + tDate + tTime;
	    	mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
	    
	    	String[][] tTitle = new String[5][];
	    	tTitle[0] = new String[] { "","","","","","","机构理赔案件处理状况统计表" };
	    	tTitle[1] = new String[] { "机构："+mManageComNam,"","","","业务类型：社会保险","","","","","统计时间："+mStartDate+"至"+mEndDate };
	    	tTitle[2] = new String[] { "制表人："+mOperator,"","","","","","","","","制表时间："+PubFun.getCurrentDate() };
	    	tTitle[3] = new String[] { "","","","","","案件量","","","","","结案金额"};
	    	tTitle[4] = new String[] { "机构编码","机构名称","受理","权限内案件量","机构完成量","总公司完成量","机构上报案件量","回退件数","回退件次","结案总赔款","机构完成案件赔款","总公司完成案件赔款","机构上报案件赔款"};
	    	String[] tContentType = {"String","String","Number","Number","Number","Number","Number","Number","Number","Number","Number","Number","Number"};
	    	if (!mCSVFileWrite.addTitle(tTitle,tContentType)) {
	    		return false;
	    	}
	    	if (!getDataListToCVS(datetype))
	    	{
	    		return false;
	    	}

	    	mCSVFileWrite.closeFile();

	    	return true;
	    }
	    /**
	     * 统计案件结果
	     */
	    private boolean getDataListToCVS(int datetype)
	    {
	    	ExeSQL tExeSQL = new ExeSQL();
	    	
	        String sql = "select X.comcode,X.name "
	        	+"from ldcom X "
	        	+"where X.comcode like '" + mManageCom + "%' "
	        	+"and X.comgrade='02' and X.sign='1'  "
	        	+"group by X.comcode,X.name "
	        	+"with ur ";

	        System.out.println("SQL:" + sql);

	        RSWrapper rsWrapper = new RSWrapper();
	        if (!rsWrapper.prepareData(null, sql)) {
	            System.out.println("机构查询失败! ");
	            return false;
	        }
	     
	        try {
	            SSRS tSSRS = new SSRS();
	            do {
	                tSSRS = rsWrapper.getSSRS();
	                if (tSSRS != null && tSSRS.MaxRow > 0) {
	                
	                    String tcomcode = "";
	                    String tContent[][] = new String[tSSRS.getMaxRow()+1][];
	                    String ListSum[] =  new String[] {"","","","","","","","","","","","",""};
	                    DecimalFormat tDF = new DecimalFormat("0.##");
	                    double tListSum2 = 0;
	                    double tListSum3 = 0;
	                    double tListSum4 = 0;
	                    double tListSum5 = 0;
	                    double tListSum6 = 0;
	                    double tListSum7 = 0;
	                    double tListSum8 = 0;
	                    double tListSum9 = 0;
	                    double tListSum10 = 0;
	                    double tListSum11 = 0;
	                    double tListSum12 = 0;
                   	    ListSum[0]="";
                	    ListSum[1]="合计";
	                    for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
	                    	String ListInfo[] = new String[13];
	                    	tcomcode=tSSRS.GetText(i, 1);
	                    	 //机构编码
	                    	 ListInfo[0] = tSSRS.GetText(i, 1);
	                    	 //机构名称
	                    	 ListInfo[1] = tSSRS.GetText(i, 2);

	                    	    //受理案件量
		                        String trgtSQL = "select count(X.caseno) count " +
		                        		"from llcase X " +
		                        		"where rgtdate between '"+mStartDate+"' and '"+mEndDate+"' " +
		                        		"and mngcom like '"+tcomcode+"%' and CHECKGRPCONT((select rgtobjno from llregister where rgtno=X.rgtno)) = 'Y' with ur ";
		                        System.out.println(trgtSQL);
		                        SSRS trgtSSRS = tExeSQL.execSQL(trgtSQL);
		                        if (trgtSSRS.getMaxRow() > 0) {
		                            //案件量-受理
		                        	ListInfo[2] = trgtSSRS.GetText(1, 1);
		                        	tListSum2+= Double.parseDouble(trgtSSRS.GetText(1, 1));
		                        }
	                    	    //案件量
		                        String tcountcaseSQL = "select  "
		                        	+"sum(case when Q.count=0  or Q.count=1 then 1 else 0 end),"
		                        	+"sum(case when Q.count=0 then 1 else 0 end), "
		                        	+"sum(case when Q.count=1 then 1 else 0 end) from "
		                        	+"(   "
		                        	+"select  "
		                        	+"nvl((select case when comcode='86' then 1 else 0 end from llsocialclaimuser where usercode=X.signer),0) count         "
		                        	+"from llcase X "
		                        	+"where X.rgtstate in ('09','11','12')"
		                        	+"and CHECKGRPCONT((select rgtobjno from llregister where rgtno=X.rgtno)) = 'Y' "
		                        	+"and X.endcasedate between '"+mStartDate+"' and '"+mEndDate+"'  "
		                        	+"and X.mngcom like '"+tcomcode+"%'       "
		                        	+"and not exists ( "
		                        	+"select 1 from ( "
		                        	+"select q.caseno caseno,q.rgtno rgtno, "
		                        	+"q.riskcode riskcode,q.givetype givetype,       "
		                        	+"(case when q.givetype='3' then sum(p.declineamnt) else q.realpay end) realpay   "
		                        	+"from llclaimdetail p,llclaimpolicy q,llcase o  "
		                        	+"where p.caseno=q.caseno and p.rgtno=q.rgtno and p.clmno=q.clmno      "
		                        	+"and o.caseno=q.caseno and o.rgtno=q.rgtno and o.rgtstate in ('09','11','12')    "
		                        	+"and o.endcasedate between '"+mStartDate+"' and '"+mEndDate+"'      "
		                        	+"and p.grpcontno=q.grpcontno and p.grppolno=q.grppolno     "
		                        	+"and p.contno=q.contno and p.polno=q.polno and p.riskcode=q.riskcode  "
		                        	+"and p.caserelano = q.caserelano     "
		                        	+"group by q.caseno,q.rgtno,q.riskcode,q.givetype,q.realpay "
		                        	+") as m,lmriskapp n,(  "
		                        	+"select substr(b.comcode,1,4) comcode,  "
		                        	+"max(b.socialmoney) socialmoney        "
		                        	+"from llsocialclaimuser b "
		                        	+"where  "
		                        	+"b.comcode<>'86' and b.stateflag='1' "
		                        	+"group by substr(b.comcode,1,4)"
		                        	+") as Y "
		                        	+"where m.riskcode=n.riskcode  "
		                        	+" "
		                        	+"and m.realpay > Y.socialmoney and m.caseno=X.caseno and m.rgtno=X.rgtno          "
		                        	+"and substr(X.mngcom,1,4) = Y.comcode   "
		                        	+"and X.mngcom like '"+tcomcode+"%'"
		                        	+")        "
		                        	+")AS Q  "
		                        	+"with ur ";  
		                        System.out.println(tcountcaseSQL);
		                        SSRS tcountcaseSSRS = tExeSQL.execSQL(tcountcaseSQL);
		                        if (tcountcaseSSRS.getMaxRow() > 0) {
		                            //案件量-权限内案件量
		                        	ListInfo[3] = tcountcaseSSRS.GetText(1, 1);
		                        	tListSum3+= Double.parseDouble(tcountcaseSSRS.GetText(1, 1));
                                    //案件量-机构完成量
		                        	ListInfo[4] = tcountcaseSSRS.GetText(1, 2);	
		                        	tListSum4+= Double.parseDouble(tcountcaseSSRS.GetText(1, 2));
                                    //案件量-总公司完成量
		                        	ListInfo[5] = tcountcaseSSRS.GetText(1, 3);	
		                        	tListSum5+= Double.parseDouble(tcountcaseSSRS.GetText(1, 3));	
		                        }
		                        
                               
		                        String tshangSQL = " select  "
		                        	+" nvl(sum(case when Q.count=0 then 1 else 1 end),0)  "
		                        	+" from  "
		                        	+" ( "
		                        	+" select X.mngcom mngcom, "
		                        	+" nvl((select case when comcode='86' then 1 else 0 end  from llsocialclaimuser where "
		                        	+" usercode=X.handler),0) count "
		                        	+" from llcase X "
		                        	+" where X.rgtstate in ('09','11','12') "
		                        	+" and CHECKGRPCONT((select rgtobjno from llregister where rgtno=X.rgtno)) = 'Y' "
		                        	+" and X.endcasedate between '"+mStartDate+"' and '"+mEndDate+"'   "
		                        	+" and X.mngcom like '"+tcomcode+"%'" 
		                        	+" and exists ( "
		                        	+" select 1 from ( "
		                        	+" select q.caseno caseno,q.rgtno rgtno, "
		                        	+" q.riskcode riskcode,q.givetype givetype, "
		                        	+" (case when q.givetype='3' then sum(p.declineamnt) else q.realpay end) realpay "
		                        	+" from llclaimdetail p,llclaimpolicy q,llcase o "
		                        	+" where p.caseno=q.caseno and p.rgtno=q.rgtno and p.clmno=q.clmno "
		                        	+" and o.caseno=q.caseno and o.rgtno=q.rgtno and o.rgtstate in ('09','11','12') "
		                        	+" and o.endcasedate between '"+mStartDate+"' and '"+mEndDate+"'  " 
		                        	+" and p.grpcontno=q.grpcontno and p.grppolno=q.grppolno "
		                        	+" and p.contno=q.contno and p.polno=q.polno and p.riskcode=q.riskcode "
		                        	+" and p.caserelano = q.caserelano "
		                        	+" group by q.caseno,q.rgtno,q.riskcode,q.givetype,q.realpay "
		                        	+" ) as m,lmriskapp n,( "
		                        	+" select substr(b.comcode,1,4) comcode, "
		                        	+" max(b.socialmoney) socialmoney "
		                        	+" from llsocialclaimuser b "
		                        	+" where  "
		                        	+" b.comcode<>'86' and b.stateflag='1' "
		                        	+" group by substr(b.comcode,1,4) "
		                        	+" ) as Y "
		                        	+" where m.riskcode=n.riskcode   "
		                        	+" "
		                        	+" and m.realpay > Y.socialmoney and m.caseno=X.caseno and m.rgtno=X.rgtno "
		                        	+" and substr(X.mngcom,1,4) = Y.comcode "
		                        	+" ) "
		                        	+" )AS Q  "
		                        	+" with ur ";
		                        System.out.println(tshangSQL);
		                        SSRS tshangSSRS = tExeSQL.execSQL(tshangSQL);
		                        if (tshangSSRS.getMaxRow() > 0) {
		                            //案件量-机构上报案件量
		                        	ListInfo[6] = tshangSSRS.GetText(1, 1);	
		                        	tListSum6 += Double.parseDouble(tshangSSRS.GetText(1, 1));	
		                        }  
		                        
		                        String tbackSQL ="select count(distinct a.caseno) count "
		                        	+"from llcase a,llcaseback b,llsocialclaimuser c "
		                        	+"where a.caseno=b.caseno and b.nhandler=c.usercode "
		                        	+"and c.comcode <> '86' "
		                        	+"and CHECKGRPCONT((select rgtobjno from llregister where rgtno=a.rgtno)) = 'Y' "
		                        	+"and b.mngcom='86' "
		                        	+"and b.backtype is null "
		                        	+"and a.endcasedate between '"+mStartDate+"' and '"+mEndDate+"'  " 
		                        	+"and a.mngcom like '"+tcomcode+"%'" 
		                        	+"with ur ";
		                        System.out.println(tbackSQL);
		                        SSRS tbackSSRS = tExeSQL.execSQL(tbackSQL);
		                        if (tbackSSRS.getMaxRow() > 0) {
		                            //案件量-回退件数
		                        	ListInfo[7] = tbackSSRS.GetText(1, 1);
		                        	tListSum7 += Double.parseDouble(tbackSSRS.GetText(1, 1));
		                        }
	                    	   
		                        String tbackSQL2 ="select count( a.caseno) count "
		                        	+"from llcase a,llcaseback b,llsocialclaimuser c "
		                        	+"where a.caseno=b.caseno and b.nhandler=c.usercode "
		                        	+"and c.comcode <> '86' "
		                        	+"and CHECKGRPCONT((select rgtobjno from llregister where rgtno=a.rgtno)) = 'Y' "
		                        	+"and b.mngcom='86' "
		                        	+"and b.backtype is null "
		                        	+"and a.endcasedate between '"+mStartDate+"' and '"+mEndDate+"'  " 
		                        	+"and a.mngcom like '"+tcomcode+"%'" 
		                        	+"with ur ";
		                        System.out.println(tbackSQL2);
		                        SSRS tbackSSRS2 = tExeSQL.execSQL(tbackSQL2);
		                        if (tbackSSRS2.getMaxRow() > 0) {
		                            //案件量-回退件次
		                        	ListInfo[8] = tbackSSRS2.GetText(1, 1);
		                        	tListSum8 += Double.parseDouble(tbackSSRS2.GetText(1, 1));
		                        }
		                        
		                        String tSumPaySQL ="select "
		                        	+"nvl(sum(Z.realpay) ,0) "
		                        	+"from llcase X,llclaimdetail Z "
		                        	+"where X.rgtstate in ('09','11','12') "
		                        	+"and CHECKGRPCONT((Z.grpcontno)) = 'Y' "
		                        	+"and X.endcasedate between '"+mStartDate+"' and '"+mEndDate+"'  "  
		                        	+"and X.caseno=Z.caseno and X.rgtno=Z.rgtno "
		                        	+"and X.mngcom like '"+tcomcode+"%'" 
		                        	+"with ur";
		                        System.out.println(tSumPaySQL);
		                        SSRS tSumPaySSRS = tExeSQL.execSQL(tSumPaySQL);
		                        if (tSumPaySSRS.getMaxRow() > 0) {
		                            //结案金额-结案总赔款
		                        	ListInfo[9] = tSumPaySSRS.GetText(1, 1);
		                        	tListSum9 += Double.parseDouble(tSumPaySSRS.GetText(1, 1));
		                        }
		                        String tComMoneySQL =" select  "
		                        	+" nvl(sum(case when Q.count=0 then Q.realpay else 0 end),0), "
		                        	+" nvl(sum(case when Q.count=1 then Q.realpay else 0 end),0) "
		                        	+" from  "
		                        	+" ( "
		                        	+" select X.mngcom mngcom, "
		                        	+" nvl((select case when comcode='86' then 1 else 0 end  from llsocialclaimuser where "
		                        	+" usercode=X.signer),0) count, "
		                        	+" sum(Z.realpay) realpay "
		                        	+" from llcase X,llclaimdetail Z "
		                        	+" where X.rgtstate in ('09','11','12') "
		                        	+" and CHECKGRPCONT((Z.grpcontno)) = 'Y' "
		                        	+" and X.endcasedate between '"+mStartDate+"' and '"+mEndDate+"'   "
		                        	+" and X.caseno=Z.caseno and X.rgtno=Z.rgtno "
		                        	+" and X.mngcom like '"+tcomcode+"%' "
		                        	+" and not exists ( "
		                        	+" select 1 from ( "
		                        	+" select q.caseno caseno,q.rgtno rgtno, "
		                        	+" q.riskcode riskcode,q.givetype givetype, "
		                        	+" (case when q.givetype='3' then sum(p.declineamnt) else q.realpay end) realpay "
		                        	+" from llclaimdetail p,llclaimpolicy q,llcase o "
		                        	+" where p.caseno=q.caseno and p.rgtno=q.rgtno and p.clmno=q.clmno "
		                        	+" and o.caseno=q.caseno and o.rgtno=q.rgtno and o.rgtstate in ('09','11','12') "
		                        	+" and o.endcasedate between '"+mStartDate+"' and '"+mEndDate+"'    "
		                        	+" and p.grpcontno=q.grpcontno and p.grppolno=q.grppolno "
		                        	+" and p.contno=q.contno and p.polno=q.polno and p.riskcode=q.riskcode "
		                        	+" and p.caserelano = q.caserelano "
		                        	+" group by q.caseno,q.rgtno,q.riskcode,q.givetype,q.realpay "
		                        	+" ) as m,lmriskapp n,( "
		                        	+" select substr(b.comcode,1,4) comcode, "
		                        	+" max(b.socialmoney) socialmoney "
		                        	+" from llsocialclaimuser b "
		                        	+" where  "
		                        	+" b.comcode<>'86' and b.stateflag='1' "
		                        	+" group by substr(b.comcode,1,4) "
		                        	+" ) as Y "
		                        	+" where m.riskcode=n.riskcode   "
		                        	+"  "
		                        	+" and m.realpay > Y.socialmoney and m.caseno=X.caseno and m.rgtno=X.rgtno "
		                        	+" and substr(X.mngcom,1,4) = Y.comcode "
		                        	+" ) group by X.mngcom,X.signer "
		                        	+" )AS Q  "
		                        	+" with ur ";
		                        System.out.println(tComMoneySQL);
		                        SSRS tComMoneySSRS = tExeSQL.execSQL(tComMoneySQL);
		                        if (tComMoneySSRS.getMaxRow() > 0) {
		                            //结案金额-机构完成案件赔款
		                        	ListInfo[10] = tComMoneySSRS.GetText(1, 1);	
		                        	tListSum10 += Double.parseDouble(tComMoneySSRS.GetText(1, 1));	
		                        	//结案金额-总公司完成案件赔款
		                        	ListInfo[11] = tComMoneySSRS.GetText(1, 2);	
		                        	tListSum11 += Double.parseDouble(tComMoneySSRS.GetText(1, 2));	
		                        }
		                        String tComUpSQL ="select  "
		                        	+"nvl(sum(Q.realpay),0) "
		                        	+"from  "
		                        	+"( "
		                        	+"select X.mngcom mngcom, "
		                        	+"nvl((select case when comcode='86' then 1 else 0 end  from llsocialclaimuser where usercode=X.handler),0) count, "
		                        	+"sum(Z.realpay) realpay "
		                        	+"from llcase X,llclaimdetail Z "
		                        	+"where X.rgtstate in ('09','11','12') "
		                        	+" and CHECKGRPCONT((Z.grpcontno)) = 'Y' "
		                        	+"and X.endcasedate between '"+mStartDate+"' and '"+mEndDate+"' "
		                        	+"and X.caseno=Z.caseno and X.rgtno=Z.rgtno "
		                        	+"and X.mngcom like '"+tcomcode+"%'  "
		                        	+"and exists ( "
		                        	+"select 1 from ( "
		                        	+"select q.caseno caseno,q.rgtno rgtno, "
		                        	+"q.riskcode riskcode,q.givetype givetype, "
		                        	+"(case when q.givetype='3' then sum(p.declineamnt) else q.realpay end) realpay "
		                        	+"from llclaimdetail p,llclaimpolicy q,llcase o "
		                        	+"where p.caseno=q.caseno and p.rgtno=q.rgtno and p.clmno=q.clmno "
		                        	+"and o.caseno=q.caseno and o.rgtno=q.rgtno and o.rgtstate in ('09','11','12') "
		                        	+"and o.endcasedate between '"+mStartDate+"' and '"+mEndDate+"'  "
		                        	+"and p.grpcontno=q.grpcontno and p.grppolno=q.grppolno "
		                        	+"and p.contno=q.contno and p.polno=q.polno and p.riskcode=q.riskcode "
		                        	+"and p.caserelano = q.caserelano "
		                        	+"group by q.caseno,q.rgtno,q.riskcode,q.givetype,q.realpay "
		                        	+") as m,lmriskapp n,( "
		                        	+"select substr(b.comcode,1,4) comcode, "
		                        	+"max(b.socialmoney) socialmoney "
		                        	+"from llsocialclaimuser b "
		                        	+"where "
		                        	+"b.comcode<>'86' and b.stateflag='1' "
		                        	+"group by substr(b.comcode,1,4) "
		                        	+") as Y "
		                        	+"where m.riskcode=n.riskcode  "
		                        	+" "
		                        	+"and m.realpay > Y.socialmoney and m.caseno=X.caseno and m.rgtno=X.rgtno "
		                        	+"and substr(X.mngcom,1,4) = Y.comcode "
		                        	+") group by X.mngcom,X.handler "
		                        	+")AS Q  "
		                        	+"with ur ";
		                        System.out.println(tComUpSQL);
		                        SSRS tComUpSSRS = tExeSQL.execSQL(tComUpSQL);
		                        if (tComUpSSRS.getMaxRow() > 0) {
		                            //结案金额-机构上报案件赔款
		                        	ListInfo[12] = tComUpSSRS.GetText(1, 1);
		                        	tListSum12 += Double.parseDouble(tComUpSSRS.GetText(1, 1));
		                        }
		           	         //  mListTable.add(ListInfo);
		                       tContent[i - 1] = ListInfo;
		                    if(i == tSSRS.getMaxRow()){
		                    	ListSum[2]=tDF.format(tListSum2);
		                    	ListSum[3]=tDF.format(tListSum3);
		                    	ListSum[4]=tDF.format(tListSum4);
		                    	ListSum[5]=tDF.format(tListSum5);
		                    	ListSum[6]=tDF.format(tListSum6);
		                    	ListSum[7]=tDF.format(tListSum7);
		                    	ListSum[8]=tDF.format(tListSum8);
		                    	ListSum[9]=tDF.format(tListSum9);
		                    	ListSum[10]=tDF.format(tListSum10);
		                    	ListSum[11]=tDF.format(tListSum11);
		                    	ListSum[12]=tDF.format(tListSum12);
	                            	tContent[i]=ListSum;
	                            	
	                            }
	                    }
	                   
	       mCSVFileWrite.addContent(tContent);
	       if (!mCSVFileWrite.writeFile()) {
				mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
				return false;
			}

	   }
	} while (tSSRS != null && tSSRS.MaxRow > 0);
	} catch (Exception ex) {

	} finally {
	rsWrapper.close();
	}
	return true;
	      }
	 

//	    private String getYear(String pmDate) {
//	        String mYear = "";
//	        String tSQL = "";
//	        SSRS tSSRS = new SSRS();
//	        ExeSQL tExeSQL = new ExeSQL();
//	        tSSRS = tExeSQL.execSQL(tSQL);
//	        mYear = tSSRS.GetText(1, 1);
//	        return mYear;
//	    }
//
//	  
//	    /**
//	     * 获取险种类型查询条件
//	     * @param aContType String
//	     * @return String
//	     */
//	    private String getContType(String aContType) {
//	        if (!"".equals(aContType)) {
//	            String tContTypeSQL =
//	                    " and exists (select 1 from lmriskapp where riskcode=b.riskcode and riskprop='" +
//	                    aContType + "' ) ";
//	            return tContTypeSQL;
//	        } else {
//	            return "";
//	        }
//	    }
//
//	    /**
//	     * 获取批次号查询条件
//	     * @param aRgtNo String
//	     * @return String
//	     */
//	    private String getRgtNo(String aRgtNo) {
//	        if (!"".equals(aRgtNo)) {
//	            String tRgtSQL = " and a.rgtno='" + aRgtNo + "'";
//	            return tRgtSQL;
//	        } else {
//	            return "";
//	        }
//	    }
//
//	    /**
//	     * 获取保单号查询条件
//	     * @param aContNo String
//	     * @return String
//	     */
//	    private String getContNo(String aContNo) {
//	        if (!"".equals(aContNo)) {
//	            String tContSQL = " and (b.grpcontno='" + aContNo +
//	                              "' or b.contno='" + aContNo + "')";
//	            return tContSQL;
//	        } else {
//	            return "";
//	        }
//
//	    }
//	    
//	    /**
//	     * 获取险种查询条件
//	     * @param aRgtNo String
//	     * @return String
//	     */
//	    private String getRiskCode(String aRiskCode) {
//	        if (!"".equals(aRiskCode)) {
//	            String tRiskCodeSQL = " and b.riskcode='" + aRiskCode + "'";
//	            return tRiskCodeSQL;
//	        } else {
//	            return "";
//	        }
//	    }

	    /**
	     * @return VData
	     */
	    public VData getResult() {
	        return mResult;
	    }
		public String getMFilePath() {
			return mFilePath;
		}

		public void setMFilePath(String filePath) {
			mFilePath = filePath;
		}

		public String getMFileName() {
			return mFileName;
		}

		public void setMFileName(String fileName) {
			mFileName = fileName;
		}
		private void buildError(String szFunc, String szErrMsg) {
			CError cError = new CError();
			cError.moduleName = "LLCasePolicyBL";
			cError.functionName = szFunc;
			cError.errorMessage = szErrMsg;
			System.out.println(szFunc + "--" + szErrMsg);
			this.mErrors.addOneError(cError);
		}
	    /**
	     * 得到xml的输入流
	     * @return InputStream
	     */
	    public InputStream getInputStream() {
	        return mXmlExport.getInputStream();
	    }
		public String getManagecomName(String com) {
			String sq="select name from ldcom where comcode='"+com+"' with ur";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS tGrpTypeSSRS = tExeSQL.execSQL(sq);
			return tGrpTypeSSRS.GetText(1, 1);
		}

}
