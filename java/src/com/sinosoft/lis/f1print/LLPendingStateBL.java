package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.vschema.LGWorkSet;
import com.sinosoft.lis.db.LGWorkDB;
import com.sinosoft.lis.vdb.LGWorkDBSet;
import com.sinosoft.utility.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LPBnfSet;
import com.sinosoft.lis.db.LPBnfDB;
import java.util.*;
/**
 * <p>Title: 保全统计 </p>
 * <p>Description: 保单工作状况统计月报表 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author yanchao
 * @version 1.0
 * @date 2005-11.18
 */

public class LLPendingStateBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private ListTable tlistTable;
    private ListTable klistTable;
    private VData mResult = new VData();
    SSRS tSSRS = new SSRS();
    SSRS kSSRS = new SSRS();
    SSRS xSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    String StartDate = "";
    String EndDate = "";
    String organcode="";
    String comcode="";
    String organname="";
    String handlecode="";
    String handlename="";
    String handler="";
     String handler1="";
    public LLPendingStateBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }

        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LLPendingBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        TransferData tTransferData = (TransferData) cInputData.
                                     getObjectByObjectName("TransferData", 0);
        StartDate = (String) tTransferData.getValueByName("StartDate");
        EndDate = (String) tTransferData.getValueByName("EndDate");
        organcode = (String) tTransferData.getValueByName("organcode");
        organname = (String) tTransferData.getValueByName("organname");
        handlecode = (String) tTransferData.getValueByName("handlecode");
        handlename = (String) tTransferData.getValueByName("handlename");
        comcode=organcode;


        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }


    public String  getCount(int tDate,String State,String Condtion) {
        String tCount = "";
        int count = 0;
        String CaseNo = "";

         if(State.equals("14")) {
             String SQL = "select count(caseno) from llcase where days('" + EndDate +
               "')-days(rgtdate) "+Condtion+ tDate
               + " and rgttype='1' "+organcode + handler +
               " and  cancledate <= '"+ EndDate + "' and rgtstate='14'  with ur ";
            tCount=tExeSQL.getOneValue(SQL);
          System.out.println("案件号"+CaseNo+"状态14");
         }
         if(!State.equals("14")){

             String SQL = "select caseno from llcase where days('" + EndDate +
                          "')-days(rgtdate) "+Condtion+ tDate
                          + " and rgttype='1' "+organcode + handler +
                          " and caseno not in (select caseno from llcase where endcasedate <= '"
                          + EndDate + "')" + " and caseno not in (select caseno from llcase where cancledate <= '"
                          + EndDate + "' and rgtstate='14') with ur ";
             tSSRS = tExeSQL.execSQL(SQL);
              System.out.println("SQLRow="+tSSRS.getMaxRow());
             for (int k = 1; k <= tSSRS.getMaxRow(); k++) {
                 String State2="";
                 CaseNo = tSSRS.GetText(k, 1);
                 String SQL0 ="select rgtstate from llcase where caseno ='" +CaseNo + "' ";
                 String Statetemp = tExeSQL.getOneValue(SQL0);
                 //取到案件的当前状态,如果当前为13或02,则案件既往状态也一样,因为没有回退到13和02状态
                 if(Statetemp.equals("13")||Statetemp.equals("02"))
                     State2 =Statetemp;
                 if (State2.equals("") || State2 == null) {
                     String SQL1 =
                             "select rgtstate from llcaseoptime where caseno ='" +
                             CaseNo + "' and enddate ='" + EndDate +
                             "' order by endtime desc fetch first 1 rows only ";
                     State2 = tExeSQL.getOneValue(SQL1);
                  //判断在统计截至日,在时效表中有无对应数据,即enddate=统计截至日 ,统计截至日发生了状态变化
                     if(State2.equals("03")&&Statetemp.equals("01"))
                        State2=Statetemp;
                    //如果是07的记录,则当时案件应为08状态
                    if(State2.equals("07"))
                        State2="08";
                 }
                  if (State2.equals("") || State2 == null) {
                      String SQL3 =
                              "select c.rgtstate from llcaseoptime c  where c.caseno ='" +
                              CaseNo + "' and '" + EndDate + "' between c.Startdate and c.enddate  "
                              + " order by c.enddate,c.endtime  fetch first 1 rows only ";
                     String StatetempMid = tExeSQL.getOneValue(SQL3);
                      StatetempMid = StrTool.cTrim(StatetempMid);
                  //统计截至日没有状态变化,统计截至日在时效表记录的某个状态的时间段内
                  //07和10状态记录都为时间段
                      if (StatetempMid.equals("07") || StatetempMid.equals("10") ||
                          StatetempMid.equals("01")) {
                          State2 = StatetempMid;
                      } else {
                   //时效表记录中enddate为变为记录状态的时间,处于这段时间内时,案件应为这条记录状态的前一个状态,01状态除外
                        String  SQL6 = "select c.rgtstate,o.rgtstate from llcaseoptime c,llcaseoptime o  where c.caseno=o.caseno and c.caseno ='" +
                                 CaseNo + "' and '" + EndDate +"' between c.Startdate and c.enddate and c.startdate=o.enddate and c.starttime=o.endtime "
                                 + " order by c.enddate,c.endtime  fetch first 1 rows only ";
                          xSSRS = tExeSQL.execSQL(SQL6);
                          if (xSSRS.getMaxRow() > 0)
                                  State2 = xSSRS.GetText(1, 2);
                      }
                  }
                  //统计截至日,endate没有对应记录,某个状态的时间段内也没有记录,则默认取时效表记录中最后一条记录的状态
                 if (State2.equals("") || State2 == null) {
                             String SQL4 = "select rgtstate from llcaseoptime where caseno ='" +
                                           CaseNo + "'   order by enddate desc,endtime desc  fetch first 1 rows only ";
                             State2 = tExeSQL.getOneValue(SQL4);
                             //如果是07的记录,则当时案件应为08状态
                             if(State2.equals("07")&&!Statetemp.equals("07"))
                                 State2="08";
                         }
            //增加特殊情况判断 : 发生上级审批,但审批不通过,继续给再上级审批 ,此时案件应该是05状态
            if(State2.equals("06")&&Statetemp.equals("05"))
                State2=Statetemp;
           if (State2.equals(State)){
               count = count + 1;
               System.out.println("案件号" + CaseNo + "状态" + State2);
               }
           }
         tCount = String.valueOf(count);
         }

        return tCount;
    }
    private boolean getPrintData() {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LLPendingStateLis.vts", "printer"); //最好紧接着就初始化xml文档

        if (!handlecode.equals("")) {
            handler = " and handler ='" + handlecode + "' ";
            handler1 = " and dealer ='" + handlecode + "' ";

        }
        if (organcode.equals("86")) {
            organcode = "";
        } else if (organcode.substring(0,4).equals("8600")){
            organcode =
                  " and handler in ( select usercode from llclaimuser u where  comcode ='86' ) ";
        }else{
            organcode =
                    " and handler in ( select usercode from llclaimuser u where  comcode ='" +
                    organcode + "' ) ";
        }

    //按受理人统计
    klistTable = new ListTable();
    klistTable.setName("ORGANP");
    int count1=0;
    int count2=0;
    int count3=0;
    int count4=0;
    int count5 = 0;
    int count6 = 0;
    int count7 = 0;
    int count8 = 0;
    int count9 = 0;
    int count10 = 0;
    int count11 = 0;
    int count12= 0;

    String SQLA = "select code,codename from ldcode u where codetype='llrgtstate' and code not in ('09','11','12','15','13') order by code with ur ";
    kSSRS = tExeSQL.execSQL(SQLA);

    for (int i = 1; i <= kSSRS.getMaxRow(); i++) {
        String[] strArr = new String[15];
        strArr[0] = kSSRS.GetText(i, 2);
        strArr[1] = getCount(0, kSSRS.GetText(i, 1),"=");
        count1 += Integer.parseInt(strArr[1]);
        strArr[2] = getCount(1, kSSRS.GetText(i, 1),"=");
        count2 += Integer.parseInt(strArr[2]);
        strArr[3] = getCount(2, kSSRS.GetText(i, 1),"=");
        count3 += Integer.parseInt(strArr[3]);
        strArr[4] = getCount(3, kSSRS.GetText(i, 1),"=");
        count4 += Integer.parseInt(strArr[4]);
        strArr[5] = getCount(4, kSSRS.GetText(i, 1),"=");
        count5 += Integer.parseInt(strArr[5]);
        strArr[6] = getCount(5, kSSRS.GetText(i, 1),"=");
        count6 += Integer.parseInt(strArr[6]);
        strArr[7] = getCount(6, kSSRS.GetText(i, 1),"=");
        count7 += Integer.parseInt(strArr[7]);
        strArr[8] = getCount(7, kSSRS.GetText(i, 1),"=");
        count8 += Integer.parseInt(strArr[8]);
        strArr[9] = getCount(8, kSSRS.GetText(i, 1),"=");
        count9 += Integer.parseInt(strArr[9]);
        strArr[10]= getCount(9, kSSRS.GetText(i, 1),"=");
        count10 += Integer.parseInt(strArr[10]);
        strArr[11]= getCount(9, kSSRS.GetText(i, 1),">");
        count11 += Integer.parseInt(strArr[11]);
        int x=Integer.parseInt(strArr[1])+Integer.parseInt(strArr[2])+Integer.parseInt(strArr[3])
              +Integer.parseInt(strArr[4])+Integer.parseInt(strArr[5])+Integer.parseInt(strArr[6])
              +Integer.parseInt(strArr[7])+Integer.parseInt(strArr[8])+Integer.parseInt(strArr[9])
              +Integer.parseInt(strArr[10])+Integer.parseInt(strArr[11]);
        String  FinCount = String.valueOf(x);
        strArr[12]= FinCount ;
        count12 += Integer.parseInt(strArr[12]);
        klistTable.add(strArr);

         }
        String[] strArr1Head = new String[15];
        strArr1Head[0] = "操作步骤";
        strArr1Head[1] = "1天";
        strArr1Head[2] = "2天";
        strArr1Head[3] = "3天";
        strArr1Head[4] = "4天";
        strArr1Head[5] = "5天";
        strArr1Head[6] = "6天";
        strArr1Head[7] = "7天";
        strArr1Head[8] = "8天";
        strArr1Head[9] = "9天";
        strArr1Head[9] = "10天";
        strArr1Head[9] = "10天以上";
        strArr1Head[9] = "合计";
        xmlexport.addListTable(klistTable, strArr1Head);
        texttag.add("count1", count1);
        texttag.add("count2", count2);
        texttag.add("count3", count3);
        texttag.add("count4", count4);
        texttag.add("count5", count5);
        texttag.add("count6", count6);
        texttag.add("count7", count7);
        texttag.add("count8", count8);
        texttag.add("count9", count9);
        texttag.add("count10", count10);
        texttag.add("count11", count11);
        texttag.add("count12", count12);
        String SQL5="select codename from ldcode  where code='"+comcode+"' and codetype='station' ";
        organname=tExeSQL.getOneValue(SQL5);
        String SQL6="select username from llclaimuser where usercode='"+handlecode+"'";
        handlename=tExeSQL.getOneValue(SQL6);

        texttag.add("StartDate", StartDate);
        texttag.add("EndDate", EndDate);
        texttag.add("organcode", organcode);
        texttag.add("organname", organname);
        texttag.add("handlecode", handlecode);
        texttag.add("handlename", handlename);
        texttag.add("operator", mGlobalInput.Operator);

        String MakeDate = PubFun.getCurrentDate();
        MakeDate = AgentPubFun.formatDate(MakeDate, "yyyy-MM-dd");
        texttag.add("MakeDate", MakeDate);

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }


        xmlexport.outputDocumentToFile("d:\\", "TaskPrint"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    public static void main(String[] args) {

       String   tEndDate="2006-08-15";
       String   tOrgancode="860000";
       String  tHandlecode="";

       VData tVData = new VData();
       GlobalInput tG = new GlobalInput();
       LLPendingStateBL LLPendingStateBL = new LLPendingStateBL();
       TransferData tTransferData = new TransferData();
//       tTransferData.setNameAndValue("StartDate", tStartDate);
       tTransferData.setNameAndValue("EndDate", tEndDate);
       tTransferData.setNameAndValue("organcode", tOrgancode);
        tTransferData.setNameAndValue("handlecode", tHandlecode);
       tVData.addElement(tG);
       tVData.addElement(tTransferData);

       LLPendingStateBL.submitData(tVData,"PRINT");


    }

}
