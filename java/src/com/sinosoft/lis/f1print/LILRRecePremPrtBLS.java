package com.sinosoft.lis.f1print;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.lis.db.*;
import java.sql.Connection;
import com.sinosoft.lis.vschema.LILRRecePremPrtSet;
import com.sinosoft.lis.vdb.LILRRecePremPrtDBSet;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LILRRecePremPrtBLS {


    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    //业务处理相关变量
    /** 全局数据 */
    private String mOperate = "";
    private VData mResult = new VData();
    private LILRRecePremPrtSchema mLILRRecePremPrtSchema= new LILRRecePremPrtSchema();
    private LILRRecePremPrtDB mLILRRecePremPrtDB = new LILRRecePremPrtDB();
    public LILRRecePremPrtBLS() {
    }
    public static void main(String[] args)
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Start MeetF1PBLS Submit...");

        tReturn = save(cInputData);

        if (tReturn)
        {
            System.out.println("Save sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End MeetF1PBLS Submit...");

        return tReturn;
    }

    //保存操作
    private boolean save(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn = DBConnPool.getConnection();

        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "MeetF1PBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);

// 核保通知书(update)
            System.out.println("Start 长线计提数据...");

            LILRRecePremPrtDB tLILRRecePremPrtDB = new LILRRecePremPrtDB(conn);
            LILRRecePremPrtDBSet tLILRRecePremPrtDBSet = new LILRRecePremPrtDBSet();
            tLILRRecePremPrtDBSet.set((LILRRecePremPrtSet) mInputData.
                                      getObjectByObjectName(
                    "LILRRecePremPrtSet", 0));
            if (!tLILRRecePremPrtDBSet.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLILRRecePremPrtDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LILRRecePremPrtBLS";
                tError.functionName = "save";
                tError.errorMessage = "长线计提数据插入失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
            System.out.println("commit end");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LILRRecePremPrtBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
            tReturn = false;
        }

        return tReturn;
    }

}
