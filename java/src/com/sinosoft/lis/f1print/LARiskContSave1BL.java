package com.sinosoft.lis.f1print;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author XX
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;



class LARiskContSave1BL {

           /** 错误处理类，每个需要错误处理的类中都放置该类 */
       public CErrors mErrors = new CErrors();
       private VData mResult = new VData();
       /** 全局变量 */
       private GlobalInput mGlobalInput = new GlobalInput() ;
       private String mManageCom="";
       private String mStartDate= "";
       private String mEndDate = "";
       private String mBranchAttr = "";
       private String mAgentGroup = "";
       private String fPerson="";
       private String mPerson="";
       private String tPerson="";
       private String tSum="";


       private VData mInputData = new VData();
       private String mOperate = "";
       private PubFun mPubFun = new PubFun();
       private String mManageName = "";
       private TransferData mTransferData = new TransferData();
       private String[] mDataList = null;
       private String[][] mShowDataList = null;


       /**
      * 传输数据的公共方法
      */
     public boolean submitData(VData cInputData, String cOperate)
     {

       mOperate = cOperate;
       mInputData = (VData) cInputData;
       if (mOperate.equals("")) {
           this.bulidErrorB("submitData", "数据不完整");
           return false;
       }

       if (!mOperate.equals("PRINT")) {
           this.bulidErrorB("submitData", "数据不完整");
           return false;
       }

       // 得到外部传入的数据，将数据备份到本类中
         if (!getInputData(mInputData)) {
             return false;
         }

        // 进行数据查询
        if (!queryData()) {
            return false;
        }

    System.out.println("dayinchenggong1232121212121");

         return true;
     }


     /**
       * 取得传入的数据
       * @return boolean
       */
      private boolean getInputData(VData cInputData)
      {

          try
          {
              mGlobalInput.setSchema((GlobalInput) cInputData.
                                     getObjectByObjectName("GlobalInput", 0));
              mTransferData = (TransferData) cInputData.getObjectByObjectName(
                      "TransferData", 0);
               //页面传入的数据 四个
               this.mManageCom = (String) mTransferData.getValueByName("tManageCom");
               this.mStartDate = (String) mTransferData.getValueByName("tStartDate");
               this.mEndDate = (String) mTransferData.getValueByName("tEndDate");
               this.mBranchAttr = (String) mTransferData.getValueByName("tBranchAttr");
               this.mAgentGroup = (String) mTransferData.getValueByName("tAgentGroup");
               System.out.println("11111111111111111and"+mManageCom);
               System.out.println("11111111111111111and"+mAgentGroup);

          } catch (Exception ex) {
              this.mErrors.addOneError("");
              return false;
          }

          return true;

   }

     private void bulidErrorB(String cFunction, String cErrorMsg) {

             CError tCError = new CError();

             tCError.moduleName = "LAMaContReportBL";
             tCError.functionName = cFunction;
             tCError.errorMessage = cErrorMsg;

             this.mErrors.addOneError(tCError);

    }



   /**
      * 获取打印所需要的数据
      * @param cFunction String
      * @param cErrorMsg String
      */
     /**
   * 追加错误信息
   * @param szFunc String
   * @param szErrMsg String
   */
  private void buildError(String szFunc, String szErrMsg)
  {
      CError cError = new CError();
      cError.moduleName = "LAMaContCBReportBL";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      System.out.println(szFunc + "--" + szErrMsg);
      this.mErrors.addOneError(cError);
  }



    private boolean queryData()
      {
          try{
              // 查询数据
              if(!getDataList())
              {
                  return false;
              }

              System.out.println(mShowDataList.length);
              System.out.println(mShowDataList[0].length);

            ListTable tlistTable = new ListTable();
            tlistTable.setName("Order");
            this.mDataList = new String[mShowDataList[0].length];

            // 2、进行排序  进行保费排序
            LISComparator tLISComparator = new LISComparator();
            tLISComparator.setNum(1);
            Arrays.sort(mShowDataList,tLISComparator);
            for(int j=1;j<mShowDataList.length;j++)
            {// 追加序号
             if((mShowDataList[j][1]).equals(mShowDataList[j-1][1]))
                {
                    mShowDataList[j][2] = mShowDataList[j-1][2];
                }
                else
                    mShowDataList[j][2] = "" + (j + 1);
            }

            for(int i=0;i<mShowDataList.length;i++)
                 {
                   tlistTable.add(mShowDataList[i]);
                       }

            // 3、追加 合计 行
            if(!setAddRow(mShowDataList.length + 1))
            {
               buildError("queryData", "进行合计计算时出错！");
               return false;
            }
            else  { tlistTable.add(mDataList);  }


            //新建一个TextTag的实例
              TextTag  tTextTag = new TextTag();
              String tMakeDate = "";
              String tMakeTime = "";

              tMakeDate = mPubFun.getCurrentDate();
              tMakeTime = mPubFun.getCurrentTime();

              System.out.print("dayin252");
            if (!getManageNameB()) {
              return false;
              }
          tTextTag.add("MakeDate", tMakeDate);
          tTextTag.add("MakeTime", tMakeTime);
          tTextTag.add("tName", mManageName);
          tTextTag.add("StartDate",mStartDate);
          tTextTag.add("EndDate",mEndDate);


          XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
          xmlexport.createDocument("LARiskContSave2.vts", "printer"); //初始化xml文档
          if (tTextTag.size() > 0)
          xmlexport.addTextTag(tTextTag);     //添加动态文本标签
          xmlexport.addListTable(tlistTable, mShowDataList[0]); //添加列表
          xmlexport.outputDocumentToFile("c:\\", "new1");
          //this.mResult.clear();
          mResult.addElement(xmlexport);
          System.out.print("22222222222222222");
          return true;


          }catch(Exception ex)
          {
              buildError("queryData", "LABL发生错误，准备数据时出错！");
              return false;
          }


  }


  private boolean getDataList()
   {
       String tSQL = "";

       // 1、得到全部的险种类型
       tSQL  = "select  *   ";
       tSQL += "from  lmriskApp  where  risktype7='1'  order by riskcode  DESC";
       System.out.println(tSQL);


       LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
       LMRiskAppSet tLMRiskAppSet = new LMRiskAppSet();
       tLMRiskAppSet = tLMRiskAppDB.executeQuery(tSQL);
       if(tLMRiskAppSet.size()==0)
       {
           buildError("queryData", "没有符合条件的机构！");
           return false;
       }

       // 2、查询需要表示的数据
       String[][] tShowDataList = new String[tLMRiskAppSet.size()][7];
       for(int i=1;i<=tLMRiskAppSet.size();i++)
       {// 循环机构进行统计
           LMRiskAppSchema tLMRiskAppSchema = new LMRiskAppSchema();
           tLMRiskAppSchema = tLMRiskAppSet.get(i);
           tShowDataList[i-1][0] = tLMRiskAppSchema.getRiskName();
           System.out.println(tLMRiskAppSchema.getRiskName()+"/"+tShowDataList[i-1][0]);

           // 查询需要表示的数据
           if (!queryOneDataList(tLMRiskAppSchema.getRiskCode(),
                                 tLMRiskAppSchema.getRiskName(),
                                 tShowDataList[i - 1]))
           {
               System.out.println("[" + tLMRiskAppSchema.getRiskName() +
                                  "] 数据查询失败！");
               return false;
           }
       }
       mShowDataList = tShowDataList;

       return true;
  }



  /**
    * 查询填充表示数据
    * @return boolean
    */
   private boolean queryOneDataList(String pmRiskCode,String pmRiskName,String[] pmOneDataList)
   {
       try{


           //1.险种名称
           pmOneDataList[0] = pmRiskName;
           // 2、本期累计保费
           pmOneDataList[1] = getSumPrem(pmRiskCode);
           // 3、本期累计保费排名
           pmOneDataList[2] = "1";
           //4、交单件数
           pmOneDataList[3] = getSumCont(pmRiskCode);
           // 5、件均保费
           pmOneDataList[4] = getAvgMoney(pmOneDataList[1],pmOneDataList[3]);
           // 6、人均保费
           fPerson=""+getAgentCountByManageCom(mStartDate,mAgentGroup);
           mPerson=""+getAgentCountByManageCom(mEndDate,mAgentGroup);
           tPerson = getTransPreson(fPerson,mPerson);
           pmOneDataList[5] = getAvgPrem(pmOneDataList[1],tPerson );
            // 7、保费占比
          tSum=getSumMoney(mStartDate,mAgentGroup);
          pmOneDataList[6] = getAct(pmOneDataList[1],tSum);


       }catch(Exception ex)
       {
           buildError("queryOneDataList", "准备数据时出错！");
           System.out.println(ex.toString());
           return false;
       }

       return true;
   }


  /**
    * 本期累计保费
    * @param  pmAgentGroup String
    * @return String
    */
   private String getSumPrem(String pmRiskCode)
   {
     String tSQL1 = "";
     String tSQL2 = "";
     String tSQL3 = "";
     String tRtValue = "";
     double sumprem;
     DecimalFormat tDF = new DecimalFormat("0.######");


     tSQL1  = "select value(sum(b.prem),0)  from lcpol b,lccont c,lmriskApp y where c.contno=b.contno ";
     tSQL1 += " and   c.salechnl='01' and  b.riskcode ='"+pmRiskCode+"'";
     tSQL1 += "   and c.agentgroup in (select a.AgentGroup from labranchgroup a where a.branchseries like '%" + mAgentGroup + "%')";
     tSQL1 += "   and  c.makedate >= '" + mStartDate + "'";
     tSQL1 += "   and  c.makedate <= '" + mEndDate + "' and y.riskcode=b.riskcode";
     tSQL1 += "   and  c.managecom  like '" + mManageCom + "%' and   y.risktype7='1'";

      System.out.println(tSQL1);
      tSQL2  = "select value(sum(b.prem),0) from lbpol b,lbcont c,lmriskApp y where c.contno=b.contno ";
        tSQL2 += " and   c.salechnl='01' and  b.riskcode ='"+pmRiskCode+"'";
        tSQL2 += "   and c.agentgroup in (select a.AgentGroup from labranchgroup a where a.branchseries like '%" + mAgentGroup + "%')";
        tSQL2 += "   and  c.makedate >= '" + mStartDate + "'";
        tSQL2 += "   and  c.makedate <= '" + mEndDate + "' and y.riskcode=b.riskcode";
        tSQL2 += "   and  c.managecom  like '" + mManageCom + "%' and   y.risktype7='1'";

   System.out.println(tSQL2);
   tSQL3  = "select value(sum(b.prem),0) from lbpol b,lccont c,lmriskApp y where c.contno=b.contno ";
  tSQL3 += " and   c.salechnl='01' and  b.riskcode ='"+pmRiskCode+"'";
  tSQL3 += "   and c.agentgroup in (select a.AgentGroup from labranchgroup a where a.branchseries like '%" + mAgentGroup + "%')";
  tSQL3 += "   and  c.makedate >= '" + mStartDate + "'";
  tSQL3 += "   and  c.makedate <= '" + mEndDate + "' and y.riskcode=b.riskcode";
  tSQL3 += "   and  c.managecom  like '" + mManageCom + "%' and   y.risktype7='1'";

   System.out.println(tSQL3);
   try{

       sumprem=execQuery(tSQL1)+execQuery(tSQL2)+execQuery(tSQL3);
        System.out.println(sumprem);
         tRtValue = tDF.format(Arith.div(sumprem,10000));
     }
     catch(Exception ex)
     {
         System.out.println("getFirstPrem 出错！");
     }

     return tRtValue;

  }

  /**
    * 交单件数
    * @param  pmAgentGroup String
    * @return String
    */
   private String getSumCont(String pmRiskCode)
   {
       String tSQL1 = "";
       String tSQL2 = "";
       String tSQL3 = "";
        double sumprem;

     int tRtValue = 0;
     tSQL1  = "select count(distinct b.contno)  from lcpol b,lccont c,lmriskApp y where c.contno=b.contno ";
     tSQL1 += " and   c.salechnl='01' and  b.riskcode ='"+pmRiskCode+"'";
     tSQL1 += "   and c.agentgroup in (select a.AgentGroup from labranchgroup a where a.branchseries like '%" + mAgentGroup + "%')";
     tSQL1 += "   and  c.makedate >= '" + mStartDate + "'";
     tSQL1 += "   and  c.makedate <= '" + mEndDate + "' and y.riskcode=b.riskcode";
     tSQL1 += "   and  c.managecom  like '" + mManageCom + "%' and   y.risktype7='1'";
      System.out.println(tSQL1);
   tSQL2  = "select count(distinct b.contno)  from lbpol b,lbcont c ,lmriskApp y where c.contno=b.contno ";
   tSQL2 += " and   c.salechnl='01' and  b.riskcode ='"+pmRiskCode+"'";
   tSQL2 += "   and c.agentgroup in (select a.AgentGroup from labranchgroup a where a.branchseries like '%" + mAgentGroup + "%')";
   tSQL2 += "   and  c.makedate >= '" + mStartDate + "'";
   tSQL2 += "   and  c.makedate <= '" + mEndDate + "' and y.riskcode=b.riskcode";
   tSQL2 += "   and  c.managecom  like '" + mManageCom + "%' and   y.risktype7='1' ";
   System.out.println(tSQL2);
      tSQL3  = "select count(distinct b.contno)  from lbpol b,lccont c,lmriskApp y where c.contno=b.contno ";
   tSQL3 += " and   c.salechnl='01' and  b.riskcode ='"+pmRiskCode+"'";
   tSQL3 += "   and c.agentgroup in (select a.AgentGroup from labranchgroup a where a.branchseries like '%" + mAgentGroup + "%')";
   tSQL3 += "   and  c.makedate >= '" + mStartDate + "'";
   tSQL3 += "   and  c.makedate <= '" + mEndDate + "' and y.riskcode=b.riskcode";
   tSQL3 += "   and  c.managecom  like '" + mManageCom + "%' and   y.risktype7='1' ";
   System.out.println(tSQL3);


   try{
       sumprem=execQuery(tSQL1)+execQuery(tSQL2)+execQuery(tSQL3);
        System.out.println(sumprem);
       tRtValue = (int)sumprem;
     }catch(Exception ex)
     {
         System.out.println("getSumCont 出错！");
     }

     return ""+tRtValue;

  }



  /**
     * 计算件均保费
     * @param pmAgentCont
     * @param pmSumMoney
     * @return String
     */
    private String getAvgMoney(String pmSumMoney,String pmAgentCont)
    {
        String tSQL = "";
        String tRtValue = "";
        DecimalFormat tDF = new DecimalFormat("0.##");

        if("0".equals(pmAgentCont))
         {
             return "0";
         }

        tSQL = "select DECIMAL(DECIMAL(" + pmSumMoney + "*10000,12,2) / DECIMAL(" +
               pmAgentCont + ",12,2),12,2) from dual";

        try{
            tRtValue = "" + tDF.format(execQuery(tSQL));

        }catch(Exception ex)
        {
            System.out.println("getAvgMoney 出错！");
        }

        return tRtValue;
    }




    /**
      * 查询管理机构下的期初、期末人数
      * @return int
      */
     private int getAgentCountByManageCom(String pmDate,String pmAgentGroup)
     {
         String tSQL = "";
         int tRtValue = 0;

         tSQL  = "select count(b.agentcode) from laagent b ";
         tSQL += "  where   (b.outworkdate > '" + pmDate + "' or b.outworkdate is null )";
         tSQL += "   and  b.employdate<='" + pmDate + "'";
         tSQL += "  and  b.branchtype='1' and b.branchtype2='01' ";
         tSQL += "   and  b.managecom  like '" + mManageCom + "%'";
         tSQL += "   and  b.agentgroup in (select a.AgentGroup from labranchgroup a  where a.branchseries like '%" + pmAgentGroup + "%')";
         try{
             tRtValue = (int) execQuery(tSQL);
         }catch(Exception ex)
         {
             System.out.println("getAgentCountByManageCom 出错！");
         }

         return tRtValue;
     }



     /**
        * 取得平均人力
        * @param pmValue1
        * @param pmValue2
        * @return String
        */
       private String getTransPreson(String pmValue1,String pmValue2)
       {
           String tSQL = "";
           String tRtValue = "";
           DecimalFormat tDF = new DecimalFormat("0.##");
           tSQL = "select DECIMAL(DECIMAL(" + pmValue1 + " + " + pmValue2 +
                  ",12,2) / 2,12,2) from dual";
           try{
                tRtValue = "" + tDF.format(execQuery(tSQL));

           }catch(Exception ex)
           {
               System.out.println("getTransPreson 出错！");
           }

           return tRtValue;
       }




 /**
   * 查询首年件均保费
   * @param  String
   * @return String
   */
  private String getAvgPrem(String pmSumMoney,String pmProCont)
 {
        String tSQL = "";
        String tRtValue = "";
        DecimalFormat tDF = new DecimalFormat("0.##");

        if("0".equals(pmProCont))
       {
           return "0";
       }


        tSQL = "select DECIMAL(DECIMAL(" + pmSumMoney + "*10000,12,2) / DECIMAL(" +
               pmProCont + ",12,2),12,2) from dual";

        try{
            tRtValue = "" + tDF.format(execQuery(tSQL));
        }catch(Exception ex)
        {
            System.out.println("getAvgPrem 出错！");
        }

        return tRtValue;
    }

    /**
        * 计算总保费
        * @param pmAgentCont
        * @param pmSumMoney
        * @return String
        */
       private String getSumMoney(String pmSumMoney,String pmAgentCont)
       {
           String tSQL1 = "";
      String tSQL2 = "";
      String tSQL3 = "";
      String tRtValue = "";
      double sumprem;
      DecimalFormat tDF = new DecimalFormat("0.##");


      tSQL1  = "select value(sum(b.prem),0)  from lcpol b,lccont c where c.contno=b.contno ";
      tSQL1 += " and   c.salechnl='01' ";
      tSQL1 += "   and c.agentgroup in (select a.AgentGroup from labranchgroup a where a.branchseries like '%" + mAgentGroup + "%')";
      tSQL1 += "   and  c.makedate >= '" + mStartDate + "'";
      tSQL1 += "   and  c.makedate <= '" + mEndDate + "'";
      tSQL1 += "   and  c.managecom  like '" + mManageCom + "%'";
    System.out.println(tSQL1);
    tSQL2  = "select value(sum(b.prem),0)  from lbpol b,lbcont c where c.contno=b.contno ";
    tSQL2 += " and   c.salechnl='01' ";
    tSQL2 += "   and c.agentgroup in (select a.AgentGroup from labranchgroup a where a.branchseries like '%" + mAgentGroup + "%')";
    tSQL2 += "   and  c.makedate >= '" + mStartDate + "'";
    tSQL2 += "   and  c.makedate <= '" + mEndDate + "'";
    tSQL2 += "   and  c.managecom  like '" + mManageCom + "%'";
    System.out.println(tSQL2);
       tSQL3  = "select value(sum(b.prem),0)  from lbpol b,lccont c where c.contno=b.contno ";
    tSQL3 += " and   c.salechnl='01' ";
    tSQL3 += "   and c.agentgroup in (select a.AgentGroup from labranchgroup a where a.branchseries like '%" + mAgentGroup + "%')";
    tSQL3 += "   and  c.makedate >= '" + mStartDate + "'";
    tSQL3 += "   and  c.makedate <= '" + mEndDate + "'";
    tSQL3 += "   and  c.managecom  like '" + mManageCom + "%'";
    System.out.println(tSQL3);
    try{

        sumprem=execQuery(tSQL1)+execQuery(tSQL2)+execQuery(tSQL3);
        System.out.println(sumprem);
          tRtValue = tDF.format(sumprem);
      }
      catch(Exception ex)
      {
          System.out.println("getSumMoney 出错！");
      }

      return tRtValue;

       }

     /**
       * 追加一条合计行
       * @return boolean
       */
      private boolean setAddRow(int pmRow)
      {
          System.out.println("合计行的行数是："+pmRow);

          mDataList[0] = "合  计";
          mDataList[1] = dealSum(1);
          mDataList[2] = "";
          mDataList[3] = dealSum(3);
          mDataList[4] = getAvgMoney(mDataList[1],mDataList[3]);
          mDataList[5] = getAvgMoney(mDataList[1],tPerson);
          mDataList[6] = "100";

          return true;
      }

      /**
       * 对传入的数组进行求和处理
       * @param pmArrNum int
       * @return String
       */
      private String dealSum(int pmArrNum)
      {
          String tReturnValue = "";
          DecimalFormat tDF = new DecimalFormat("0.######");
          String tSQL = "select 0";

          for(int i=0;i<this.mShowDataList.length;i++)
          {
              tSQL += " + " + this.mShowDataList[i][pmArrNum];
          }

          tSQL += " + 0 from dual";

          tReturnValue = "" + tDF.format(execQuery(tSQL));

          return tReturnValue;
      }

      /**
         * 计算保费占比
         * @param  String
         * @param  String
         * @return String
         */
        private String getAct(String pmPrem,String pmSumPrem)
        {
            String tSQL = "";
            String tRtValue = "";
            DecimalFormat tDF = new DecimalFormat("0.##");

            if("0".equals(pmSumPrem))
            {
                return "0";
            }

            tSQL = "select DECIMAL(DECIMAL(" + pmPrem + "*10000* 100,12,2) / DECIMAL(" +
                   pmSumPrem + ",12,2) ,12,2) from dual";
//          System.out.println(tSQL);
            try{
                tRtValue = "" + tDF.format(execQuery(tSQL));
//              System.out.println(tRtValue);
            }catch(Exception ex)
            {
                System.out.println("getAct 出错！");
            }

            return tRtValue;
        }






   /**
     * 执行SQL文查询结果
     * @param sql String
     * @return double
     */
    private double execQuery(String sql)
    {
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        System.out.println(sql);

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null)return 0.00;
            st = conn.prepareStatement(sql);
            if (st == null)return 0.00;
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getDouble(1);
            }
            return 0.00;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
               if (!conn.isClosed()) {
                   conn.close();
               }
               try {
                   st.close();
                   rs.close();
               } catch (Exception ex2) {
                   ex2.printStackTrace();
               }
               st = null;
               rs = null;
               conn = null;
             } catch (Exception e) {}

        }
    }



    private boolean getManageNameB() {

             String sql = "select name from ldcom where comcode='" + mManageCom +
                          "'";

             SSRS tSSRS = new SSRS();

             ExeSQL tExeSQL = new ExeSQL();

             tSSRS = tExeSQL.execSQL(sql);

             if (tExeSQL.mErrors.needDealError()) {

                 this.mErrors.addOneError("销售单位不存在！");

                 return false;

             }

             if (mManageCom.equals("86")) {
                 this.mManageName = "";
             } else {
                 if(mManageCom.length()>4)
                 {this.mManageName = tSSRS.GetText(1, 1) + "分公司";}
                 else
                 {this.mManageName = tSSRS.GetText(1, 1);}
             }
             System.out.println("1111111111111111111111");
             System.out.println(mManageName);
             return true;
      }






      /**
       * @return VData
       */
      public VData getResult() {
          return this.mResult;
    }
}
