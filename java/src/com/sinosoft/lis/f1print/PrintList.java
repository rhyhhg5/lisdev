package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.TextTag;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ListTable;
import java.util.ArrayList;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PrintList
{
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private TransferData transferData = new TransferData();

    private GlobalInput globalInput = new GlobalInput();

    private SSRS mSSRS = new SSRS();

    public PrintList()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            this.bulidError("submitData", "不支持的操作字符串");
            return false;
        }

        //外部传入数据
        if (!getInputData(cInputData))
        {
            this.bulidError("getInputData", "数据不完整");
            return false;
        }

        //数据合法性
        if (!getListTable())
        {
            return false;
        }

        //获取打印数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * 接收数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {

        try
        {
            globalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            transferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
        }
        catch (Exception e)
        {
            this.mErrors.addOneError("数据不完整");
            return false;
        }

        return true;
    }

    /**
     * 查询数据
     * @return boolean
     */
    private boolean getListTable()
    {
        String sql = (String) transferData.getValueByName("sql");

        ExeSQL exeSQL = new ExeSQL();
        System.out.println(sql);
        mSSRS = exeSQL.execSQL(sql);
        //System.out.println(mSSRS.MaxCol + "****" + mSSRS.MaxNumber + "****" + mSSRS.MaxRow);

        if (exeSQL.mErrors.needDealError())
        {
            CError error = new CError();
            error.moduleName = "CardUsedPrintBL";
            error.functionName = "getListTable";
            error.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(error);

            return false;
        }
        if (mSSRS.getMaxRow() < 1)
        {
            CError error = new CError();
            error.moduleName = "CardUsedPrintBL";
            error.functionName = "getListTable";
            error.errorMessage = "没有查询结果！";
            this.mErrors.addOneError(error);

            return false;
        }

        return true;
    }

    /**
     * 将数据存放到XmlExport中
     * @return boolean
     */
    private boolean getPrintData()
    {

        XmlExport xmlExport = new XmlExport();
        xmlExport.createDocument((String) transferData.getValueByName("vtsName"),
                                 (String) transferData.getValueByName("printerName"));

        TextTag textTag = new TextTag();

        textTag.add("ManageName", this.getManageName()); //机构
        textTag.add("PrintDate", PubFun.getCurrentDate()); //打印时间
        textTag.add("PrintOperator", globalInput.Operator); //打印人

        ArrayList arrayList = (ArrayList) transferData.getValueByName("arrayList");
        if(arrayList != null)
        {
            for (int i = 0; i < arrayList.size(); i++)
            {
                textTag.add((String) arrayList.get(i++), (String) arrayList.get(i));
                //System.out.println(arrayList.get(i));
            }
        }

        if (textTag.size() > 1)
        {
            xmlExport.addTextTag(textTag);
        }

        String[] title = (String[]) transferData.getValueByName("title");

        xmlExport.addListTable(this.getListTableData(title.length), title);

//        tXmlExport.outputDocumentToFile("c:\\", "new");

        mResult.clear();

        mResult.add(xmlExport);

        return true;
    }

    /**
     * 将数据存放到ListTable中
     * @return ListTable
     */
    private ListTable getListTableData(int infoLength)
    {
        ListTable listTable = new ListTable();

        listTable.setName((String) transferData.getValueByName("tableName"));

        if (mSSRS.getMaxRow() > 0)
        {

            for (int i = 1; i <= mSSRS.getMaxRow(); i++)
            {
                String info[] = new String[infoLength];

                for(int j = 0; j < infoLength; j++)
                {
                    info[j] = mSSRS.GetText(i, j + 1);
                }

                listTable.add(info);
            }
        }
        return listTable;
    }

    /**
     * 写入错误消息
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg)
    {
        CError error = new CError();

        error.moduleName = "CardUsedPrintBL";
        error.functionName = cFunction;
        error.errorMessage = cErrorMsg;

        this.mErrors.addOneError(error);

    }

    /**
     * 查询机构名称
     * @return String
     */
    private String getManageName()
    {
        String ManageName = "";
        String sql = "";

        sql = "select name from ldcom where comcode='" + globalInput.ManageCom +
              "'";

        SSRS tSSRS = new SSRS();

        ExeSQL tExeSQL = new ExeSQL();

        tSSRS = tExeSQL.execSQL(sql);
        System.out.println(sql);

        if (tExeSQL.mErrors.needDealError())
        {
            CError error = new CError();
            error.moduleName = "CardUsedPrintBL";
            error.functionName = "getManageName";
            error.errorMessage = "机构名不存在！";
            this.mErrors.addOneError(error);
        }

        if (tSSRS.getMaxRow() > 0)
        {
            System.out.println(tSSRS.GetText(1, 1));
            ManageName = tSSRS.GetText(1, 1);
        }
        else
        {
            CError error = new CError();
            error.moduleName = "CardUsedPrintBL";
            error.functionName = "getManageName";
            error.errorMessage = "机构名不存在！";
            this.mErrors.addOneError(error);
        }

        return ManageName;
    }

    /**
     * 获得结果
     * @return VData
     */
    public VData getResult()
    {
        return this.mResult;
    }

    public static void main(String[] args)
    {
        PrintList printlist = new PrintList();
    }
}
