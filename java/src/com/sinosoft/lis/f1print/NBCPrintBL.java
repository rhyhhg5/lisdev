/**
 * Copyright (c) 2005 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.text.DecimalFormat;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author chenhq
 * @version 1.0
 */
public class NBCPrintBL
    implements PrintService {

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  private VData mResult = new VData();

  //取得的集体合同号码
  private String mGrpContNo = "";

  //取得的电话号码
  private String telnumber = "";

  //取得的退款数额
  private double money = 0;

  //取得的单位名称
  private String mGrpName = "";

  //取得的管理机构代码
  private String mManageCom = "";
  private String mdate = "";
  //业务处理相关变量
  private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
  private LJAGetDB mLJAGetDB = new LJAGetDB();
  private LCAddressSchema mLCAddressSchema = new LCAddressSchema();
  private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

  public NBCPrintBL() {
  }

  /**
   * 传输数据的公共方法
   * @param cInputData VData
   * @param cOperate String
   * @return boolean
   */
  public boolean submitData(VData cInputData, String cOperate) {
    if (!cOperate.equals("PRINT")) {
      buildError("submitData", "不支持的操作字符串");
      return false;
    }

    // 得到外部传入的数据，将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }

    mResult.clear();

    if (mLJAGetSchema.getOtherNoType().equals("4")) {

      //正常暂交费退费打印流程
      if (!getPrintData()) {
        return false;
      }
    }
    else {
      //溢交保费退费打印流程
      if (!getNewPrintData()) {
        return false;
      }
    }

    return true;
  }

  private boolean getInputData(VData cInputData) {
    //只获取实付总表的数据，通过othernotype来判定执行的操作
    mLJAGetSchema.setSchema( (LJAGetSchema) cInputData.getObjectByObjectName(
        "LJAGetSchema", 0));
    mLJAGetDB.setSchema(mLJAGetSchema);
    if (mLJAGetDB.getInfo() == false) {
      mErrors.copyAllErrors(mLJAGetDB.mErrors);
      buildError("outputXML", "在取得mLJAGetDB数据时发生错误");
      return false;
    }
    mLJAGetSchema = mLJAGetDB.getSchema();
    return true;
  }

  public VData getResult() {
    return mResult;
  }

  public CErrors getErrors() {
    return mErrors;
  }

  private void buildError(String szFunc, String szErrMsg) {
    CError cError = new CError();
    cError.moduleName = "LCContF1PBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }

  /**
   * 暂交费退费打印流程
   * @return boolean
   */
  private boolean getPrintData() {
    String tTempFeeNo = mLJAGetSchema.getOtherNo(); //暂交费退费的时候，实付总表存储的是暂交费收据号
    StringBuffer tSBql = new StringBuffer(128);
    tSBql.append(
        "select * from lccont where prtno in (select otherno from ljtempfee where tempfeeno = '");
    tSBql.append(tTempFeeNo);
    tSBql.append("')");
    LCContDB tLCContDB = new LCContDB();
    LCContSet tLCContSet = tLCContDB.executeQuery(tSBql.toString());
    tLCContDB.setSchema(tLCContSet.get(1));
    if (tLCContSet.size() == 0) {
      buildError("outputXML", "查询合同表信息时发生错误");
      return false;
    }
    //获取实付总表的详细信息
    LJAGetDB tLJAGetDB = new LJAGetDB();
    tLJAGetDB.setSchema(mLJAGetSchema);
    if (!tLJAGetDB.getInfo()) {
      mErrors.copyAllErrors(tLJAGetDB.mErrors);
      buildError("outputXML", "在取得打印队列1中数据时发生错误");
      return false;
    }

    LDComDB tLDComDB = new LDComDB();
    tLDComDB.setComCode(mLJAGetSchema.getManageCom());
    if (!tLDComDB.getInfo()) {
      mErrors.copyAllErrors(tLDComDB.mErrors);
      buildError("outputXML", "在取得打印队列2中数据时发生错误");
      return false;
    }

    //投保人地址和邮编
    LCAppntDB tLCAppntDB = new LCAppntDB();
    tLCAppntDB.setContNo(tLCContDB.getContNo());
    if (tLCAppntDB.getInfo() == false) {
      mErrors.copyAllErrors(tLCAppntDB.mErrors);
      buildError("outputXML", "在取得打印队列3中数据时发生错误");
      return false;
    }
    if (tLCAppntDB.getAddressNo() != null && !tLCAppntDB.equals("")) {
      LCAddressDB tLCAddressDB = new LCAddressDB();
      tLCAddressDB.setCustomerNo(tLCContDB.getAppntNo());
      tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
      if (tLCAddressDB.getInfo() == false) {
        mErrors.copyAllErrors(tLCAddressDB.mErrors);
        buildError("outputXML", "在取得打印队列4中数据时发生错误");
        return false;
      }
      mLCAddressSchema = tLCAddressDB.getSchema();
    }

    String mAgentCode = tLCContDB.getAgentCode();
    LAAgentDB tLAAgentDB = new LAAgentDB();
    tLAAgentDB.setAgentCode(mAgentCode);
    if (!tLAAgentDB.getInfo()) {
      mErrors.copyAllErrors(tLAAgentDB.mErrors);
      buildError("outputXML", "在取得LAAgent代理人的数据时发生错误");
      return false;
    }
    mLAAgentSchema = tLAAgentDB.getSchema(); //保存代理人信息

    //生成-年-月-日格式的日期
    StrTool tSrtTool = new StrTool();
    String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
        tSrtTool.getDay() + "日";
    String LastDate = PubFun.calDate(PubFun.getCurrentDate(), 30, "D", null) +
        "-";
    LastDate = StrTool.decodeStr(LastDate, "-", 1) + "年" +
        StrTool.decodeStr(LastDate, "-", 2) + "月" +
        StrTool.decodeStr(LastDate, "-", 3) + "日";
    //合同信息
    LCPolDB tLCPolDB = new LCPolDB();
    tLCPolDB.setContNo(tLCContDB.getContNo());
    LCPolSet tLCPolSet = new LCPolSet();
    tLCPolSet = tLCPolDB.query();

    //险种信息
    LMRiskDB tLMRiskDB = new LMRiskDB();
    String tRiskCode = tLCPolSet.get(1).getRiskCode();
    tLMRiskDB.setRiskCode(tRiskCode);
    if (!tLMRiskDB.getInfo()) {
      mErrors.copyAllErrors(tLMRiskDB.mErrors);
      buildError("outputXML", "在取得主险LMRisk的数据时发生错误");
      return false;
    }
    String MainRiskName = tLMRiskDB.getRiskName();

//保费计算
    //double SumPrem = tLCContDB.getPrem();    //应付
    double GetPrem = 0.00; //全部退交
    String sGetPrem = "0.00";

    String tsql = "select nvl(sum(Paymoney),0) from ljtempfee where tempfeeno = '" +
        tTempFeeNo + "'";
    ExeSQL tExeSQL = new ExeSQL();
    String sPaymoney = tExeSQL.getOneValue(tsql); //暂交费
    double Paymoney = Double.parseDouble(sPaymoney);
    sPaymoney = new DecimalFormat("0.00").format(Paymoney);
    double BackPrem = Paymoney - GetPrem; //退费
    String sBackPrem = new DecimalFormat("0.00").format(BackPrem);

//退费原因
    String Reason = "";
    if(tLCContDB.getUWFlag().equals("1") || tLCContDB.getUWFlag().equals("8") || tLCContDB.getUWFlag().equals("a"))
    {
      Reason = "已被注销，由于您已交暂交保费，故需无息全额退还暂交款项。";
    }
//    if(tLCContDB.getUWFlag().equals("a"))
//    {
//      Reason = "投保申请被撤销";
//    }
/////------------------------------
    //其它模版上单独不成块的信息
    TextTag texttag = new TextTag(); //新建一个TextTag的实例
    XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
    xmlexport.createDocument("NBCPpayback.vts", "printer"); //最好紧接着就初始化xml文档

    texttag.add("AppntName", tLCContDB.getAppntName()); //投保人姓名
    if (tLCContDB.getAppntSex().equals("1")) { //先生或女士
      texttag.add("AppntSex", "女士");
    }
    else {
      texttag.add("AppntSex", "先生");
    }
    String InsuredName = "";
    if(tLCContDB.getInsuredName().equals(tLCContDB.getAppntName()))
    {
      InsuredName = "本人";
    }
    else
    {
      InsuredName = "被保险人"+tLCContDB.getInsuredName();
    }
    texttag.add("InsuredName", InsuredName); //被保人姓名
    texttag.add("RiskName", MainRiskName); //主险
    texttag.add("GetPrem", sGetPrem); //应收
    texttag.add("Paymoney", sPaymoney); //暂交费
    texttag.add("BackPrem", sBackPrem); //退费
    texttag.add("Reason",  Reason); //退费原因
    texttag.add("PartPrem", "全部"); //退费

//////打印脚本  chenhq////////////////////////////////////////////////////////////////////
    texttag.add("PrtNo", tLCContDB.getPrtNo()); //投保书条码
    texttag.add("Phone", mLCAddressSchema.getPhone());
    texttag.add("Mobile", mLCAddressSchema.getMobile());
    String SaleChnlName = "";
    if (tLCContDB.getSaleChnl() != null &&
        tLCContDB.getSaleChnl().equals("01")) {
      SaleChnlName = "团体销售";
    }
    else if (tLCContDB.getSaleChnl() != null &&
             tLCContDB.getSaleChnl().equals("02")) {
      SaleChnlName = "个人销售";
    }
    else if (tLCContDB.getSaleChnl() != null &&
             tLCContDB.getSaleChnl().equals("03")) {
      SaleChnlName = "银行代理";
    }
    texttag.add("SaleChnl", SaleChnlName); //销售渠道
    tLDComDB.setComCode(tLCContDB.getManageCom());
    if (!tLDComDB.getInfo()) {
      texttag.add("ManagePhone", "");
    }
    texttag.add("ManagePhone", tLDComDB.getPhone()); //公司电话
    texttag.add("ManageAddress", tLDComDB.getAddress()); //公司地址
    texttag.add("ManagePost", tLDComDB.getZipCode()); //邮    编
    texttag.add("AgentName", mLAAgentSchema.getName()); //代理人姓名
    texttag.add("AgentCode", tLCContDB.getAgentCode()); //代理人代码
    LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
    tLABranchGroupDB.setAgentGroup(mLAAgentSchema.getAgentGroup());
    if (!tLABranchGroupDB.getInfo()) {
      mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
      buildError("outputXML", "在取得代理人的营业机构时发生错误");
      return false;
    }
    texttag.add("ManageName", tLABranchGroupDB.getName()); //营业机构
    texttag.add("SysDate", SysDate); //系统打印时间
    texttag.add("PrintCount", 1); //系统打印次数
//////////////////////////////////////////////////////////////////////////////////////
    if (texttag.size() > 0) {
      xmlexport.addTextTag(texttag);
    }
    mResult.clear();
    mResult.addElement(xmlexport);
    return true;
  }

  /**
   * 溢交保费退费打印流程
   * @return boolean
   */
  private boolean getNewPrintData() {
    LJAGetDB tLJAGetDB = new LJAGetDB();
    tLJAGetDB.setSchema(mLJAGetSchema);
    if (!tLJAGetDB.getInfo()) {
      mErrors.copyAllErrors(tLJAGetDB.mErrors);
      buildError("outputXML", "在取得打印队列1中数据时发生错误");
      return false;
    }
    StringBuffer tSBql = new StringBuffer(128);
    //个单溢交退费

    tSBql.append("select * from lCCont where ContNo = '");
    tSBql.append(mLJAGetSchema.getOtherNo());
    tSBql.append("'");

    LCContDB tLCContDB = new LCContDB();
    tLCContDB.setContNo(mLJAGetSchema.getOtherNo());
    if (tLCContDB.getInfo() == false) {
      mErrors.copyAllErrors(tLCContDB.mErrors);
      buildError("outputXML", "在取得tLCContDB打印队列中数据时发生错误");
      return false;
    }

    LDComDB tLDComDB = new LDComDB();
    tLDComDB.setComCode(mLJAGetSchema.getManageCom());
    if (!tLDComDB.getInfo()) {
      mErrors.copyAllErrors(tLDComDB.mErrors);
      buildError("outputXML", "在取得打印队列2中数据时发生错误");
      return false;
    }

    //投保人地址和邮编
    LCAppntDB tLCAppntDB = new LCAppntDB();
    tLCAppntDB.setContNo(tLCContDB.getContNo());
    if (tLCAppntDB.getInfo() == false) {
      mErrors.copyAllErrors(tLCAppntDB.mErrors);
      buildError("outputXML", "在取得打印队列3中数据时发生错误");
      return false;
    }
    if (tLCAppntDB.getAddressNo() != null && !tLCAppntDB.equals("")) {
      LCAddressDB tLCAddressDB = new LCAddressDB();
      tLCAddressDB.setCustomerNo(tLCContDB.getAppntNo());
      tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
      if (tLCAddressDB.getInfo() == false) {
        mErrors.copyAllErrors(tLCAddressDB.mErrors);
        buildError("outputXML", "在取得打印队列4中数据时发生错误");
        return false;
      }
      mLCAddressSchema = tLCAddressDB.getSchema();
    }

    String mAgentCode = tLCContDB.getAgentCode();
    LAAgentDB tLAAgentDB = new LAAgentDB();
    tLAAgentDB.setAgentCode(mAgentCode);
    if (!tLAAgentDB.getInfo()) {
      mErrors.copyAllErrors(tLAAgentDB.mErrors);
      buildError("outputXML", "在取得LAAgent代理人的数据时发生错误");
      return false;
    }
    mLAAgentSchema = tLAAgentDB.getSchema(); //保存代理人信息

    //生成-年-月-日格式的日期
    StrTool tSrtTool = new StrTool();
    String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
        tSrtTool.getDay() + "日";
    String LastDate = PubFun.calDate(PubFun.getCurrentDate(), 30, "D", null) +
        "-";
    LastDate = StrTool.decodeStr(LastDate, "-", 1) + "年" +
        StrTool.decodeStr(LastDate, "-", 2) + "月" +
        StrTool.decodeStr(LastDate, "-", 3) + "日";
    //合同信息
    LCPolDB tLCPolDB = new LCPolDB();
    tLCPolDB.setContNo(tLCContDB.getContNo());
    LCPolSet tLCPolSet = new LCPolSet();
    tLCPolSet = tLCPolDB.query();

    //险种信息
    LMRiskDB tLMRiskDB = new LMRiskDB();
    String tRiskCode = tLCPolSet.get(1).getRiskCode();
    tLMRiskDB.setRiskCode(tRiskCode);
    if (!tLMRiskDB.getInfo()) {
      mErrors.copyAllErrors(tLMRiskDB.mErrors);
      buildError("outputXML", "在取得主险LMRisk的数据时发生错误");
      return false;
    }
    String MainRiskName = tLMRiskDB.getRiskName();

//保费计算

    String tsql = "select nvl(sum(Paymoney),0) from ljtempfee where OtherNo = '" +
        mLJAGetSchema.getOtherNo() + "'";
    ExeSQL tExeSQL = new ExeSQL();
    String sPaymoney = tExeSQL.getOneValue(tsql); //暂交费
    double Paymoney = Double.parseDouble(sPaymoney);
    sPaymoney = new DecimalFormat("0.00").format(Paymoney);
    double BackPrem = tLJAGetDB.getSumGetMoney(); //退费
    String sBackPrem = new DecimalFormat("0.00").format(BackPrem);
    double GetPrem =  Paymoney - BackPrem;//应收保费
    String sGetPrem = new DecimalFormat("0.00").format(GetPrem);

/////------------------------------
    //其它模版上单独不成块的信息
    TextTag texttag = new TextTag(); //新建一个TextTag的实例
    XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
    xmlexport.createDocument("NBCPpayback.vts", "printer"); //最好紧接着就初始化xml文档

    texttag.add("AppntName", tLCContDB.getAppntName()); //投保人姓名
    if (tLCContDB.getAppntSex().equals("1")) { //先生或女士
      texttag.add("AppntSex", "女士");
    }
    else {
      texttag.add("AppntSex", "先生");
    }
    String InsuredName = "";
    if(tLCContDB.getInsuredName().equals(tLCContDB.getAppntName()))
    {
      InsuredName = "本人";
    }
    else
    {
      InsuredName = "被保险人"+tLCContDB.getInsuredName();
    }
    texttag.add("InsuredName", InsuredName); //被保人姓名
    texttag.add("RiskName", MainRiskName); //主险
    texttag.add("Reason","我们已同意接受，由于您所交保费多于应交保费，故需退还部分保险费。"); //退费原因
    texttag.add("GetPrem", sGetPrem); //应收
    texttag.add("Paymoney", sPaymoney); //暂交费
    texttag.add("BackPrem", sBackPrem); //退费
    texttag.add("PartPrem", "部分"); //退费

//////打印脚本  chenhq////////////////////////////////////////////////////////////////////
    texttag.add("PrtNo", tLCContDB.getPrtNo()); //投保书条码
    texttag.add("Phone", mLCAddressSchema.getPhone());
    texttag.add("Mobile", mLCAddressSchema.getMobile());
    String SaleChnlName = "";
    if (tLCContDB.getSaleChnl() != null &&
        tLCContDB.getSaleChnl().equals("01")) {
      SaleChnlName = "团体销售";
    }
    else if (tLCContDB.getSaleChnl() != null &&
             tLCContDB.getSaleChnl().equals("02")) {
      SaleChnlName = "个人销售";
    }
    else if (tLCContDB.getSaleChnl() != null &&
             tLCContDB.getSaleChnl().equals("03")) {
      SaleChnlName = "银行代理";
    }
    texttag.add("SaleChnl", SaleChnlName); //销售渠道
    tLDComDB.setComCode(tLCContDB.getManageCom());
    if (!tLDComDB.getInfo()) {
      texttag.add("ManagePhone", "");
    }
    texttag.add("ManagePhone", tLDComDB.getPhone()); //公司电话
    texttag.add("ManageAddress", tLDComDB.getAddress()); //公司地址
    texttag.add("ManagePost", tLDComDB.getZipCode()); //邮    编
    texttag.add("AgentName", mLAAgentSchema.getName()); //代理人姓名
    texttag.add("AgentCode", tLCContDB.getAgentCode()); //代理人代码
    LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
    tLABranchGroupDB.setAgentGroup(mLAAgentSchema.getAgentGroup());
    if (!tLABranchGroupDB.getInfo()) {
      mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
      buildError("outputXML", "在取得代理人的营业机构时发生错误");
      return false;
    }
    texttag.add("ManageName", tLABranchGroupDB.getName()); //营业机构
    texttag.add("SysDate", SysDate); //系统打印时间
    texttag.add("PrintCount", 1); //系统打印次数
//////////////////////////////////////////////////////////////////////////////////////
    if (texttag.size() > 0) {
      xmlexport.addTextTag(texttag);
    }
    mResult.clear();
    mResult.addElement(xmlexport);
    return true;
  }
}
