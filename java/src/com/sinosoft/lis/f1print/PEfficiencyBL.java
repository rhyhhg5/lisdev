package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author 刘岩松
 * function :print InputEfficiency or print CheckEfficiency Business Logic layer
 * @version 1.0
 * @date 2003-04-04
 */

import com.sinosoft.lis.llcase.CaseFunPub;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PEfficiencyBL
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /*
     * @define globe variable
     */
    private String strStartDate;
    private String strEndDate;
    private String strOperateFlag;

    public PEfficiencyBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();

        if (!getPrintData())
        {
            return false;
        }
        return true;
    }

    /***
     * Name : getInputData()
     * function :receive data from jsp and check date
     * check :judge whether startdate begin enddate
     * return :true or false
     */
    private boolean getInputData(VData cInputData)
    {
        strStartDate = (String) cInputData.get(0);
        strEndDate = (String) cInputData.get(1);
        strOperateFlag = (String) cInputData.get(2);
        int date_flag = strStartDate.compareTo(strEndDate);
        if (!CaseFunPub.checkDate(strStartDate, strEndDate))
        {
            CError tError = new CError();
            tError.moduleName = "ShowBillBL";
            tError.functionName = "getDutyGetClmInfo";
            tError.errorMessage = "开始日期不能比结束日期晚，请您重新输入起止日期！！！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    /********
     * name : buildError
     * function : Prompt error message
     * return :error message
     */

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PEfficiencyBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /********
     * name : getPrintData()
     * function :get print data
     * parameter : null
     * return :true or false
     */

    private boolean getPrintData()
    {
        if (strOperateFlag.equals("Input")) //if the operateFlag is "Input" then execute the follow program
        {
            ListTable tlistTable = new ListTable();
            tlistTable.setName("MODE");
            String t_SQL = ""; //choose all Operator from LCPol between start date and end date
            t_SQL = " select distinct Operator from LCPol where MakeDate >='" +
                    strStartDate.trim()
                    + "' and MakeDate <='" + strEndDate.trim() + "' union "
                    + " select distinct Operator from LBPol where MakeDate >='" +
                    strStartDate.trim()
                    + "' and MakeDate <='" + strEndDate.trim() + "' ";
            System.out.println("所以操作人员的语句是" + t_SQL);
            //查询出在LCPol和LBPol中不重复的所以的操作人员的sql
            ExeSQL exeSQL = new ExeSQL();
            SSRS ssrs = exeSQL.execSQL(t_SQL);
            int i_count = ssrs.getMaxRow();
            if (i_count == 0)
            {
                CError tError = new CError();
                tError.moduleName = "PEfficiencyBL";
                tError.functionName = "getDutyGetClmInfo";
                tError.errorMessage = "在该时间段内没有打印信息，请确认起止日期是否正确！！！";
                this.mErrors.addOneError(tError);
                return false;
            }

            TextTag texttag = new TextTag(); //Create a TextTag instance
            XmlExport xmlexport = new XmlExport(); //Create a XmlExport instance
            xmlexport.createDocument("InputEfficiency.vts", "printer"); //appoint to a special output .vts file
            ListTable alistTable = new ListTable(); //Create a ListTable instance
            alistTable.setName("INFO"); //Appoint to a sepcial flag

            /*****
             * define variables used to remark
             * group,individual,banksupply,and error of all operators
             */

            int td_sum = 0; //团单
            int gd_sum = 0; //个单
            int yd_sum = 0; //银行代理
            int back_sum = 0; //返回件的数量
            int sum = 0; //总数量

            for (int i = 1; i <= i_count; i++)
            {
                String[] cols = new String[6];
                String strOperator = ssrs.GetText(i, 1);
                ExeSQL tExeSQL = new ExeSQL();
                SSRS t_ssrs;
                //查询团单的录入工作量(以印刷号码来统计，一个团单的印刷号码是相同的)
                String t_sql1 = "select nvl(count(distinct(PrtNo)),0) from LCPol where SaleChnl = '01' and Operator = '"
                                + strOperator.trim() + "' and MakeDate >='" +
                                strStartDate.trim() + "' and MakeDate <='" +
                                strEndDate.trim() +
                        "' and mainpolno = polno  and RenewCount = '0' union "
                                + "select nvl(count(distinct(PrtNo)),0) from LbPol where SaleChnl = '01' and Operator = '"
                                + strOperator.trim() + "' and MakeDate >='" +
                                strStartDate.trim() + "' and MakeDate <='" +
                                strEndDate.trim() +
                                "'and mainpolno = polno and RenewCount = '0' ";
                System.out.println("查询的团单录入工作量是" + t_sql1);
                t_ssrs = tExeSQL.execSQL(t_sql1);
                int td_count = 0; //count of group
                if (t_ssrs.getMaxRow() > 0)
                {
                    for (int t_count = 1; t_count <= t_ssrs.getMaxRow();
                                       t_count++)
                    {
                        System.out.println("团单的查询结果是" + t_count);
                        td_count = td_count +
                                   Integer.parseInt(t_ssrs.GetText(t_count, 1));
                    }
                }
                System.out.println("团单的查询结果的合计是" + td_count);

                //查询个单的录入量统计
                String t_sql2 = "select nvl(Count(distinct(PrtNo)),0) from LCPol where SaleChnl = '02' and Operator = '"
                                + strOperator.trim() + "' and MakeDate >='" +
                                strStartDate.trim() + "' and MakeDate <='" +
                                strEndDate.trim() +
                        "' and mainpolno = polno and RenewCount = '0' union "
                                + " select nvl(Count(distinct(PrtNo)),0) from LBPol where SaleChnl = '02' and Operator = '"
                                + strOperator.trim() + "' and MakeDate >='" +
                                strStartDate.trim() + "' and MakeDate <='" +
                                strEndDate.trim() +
                                "' and mainpolno = polno and RenewCount = '0' ";
                System.out.println("个单录入量统计的查询语句是:" + t_sql2);
                t_ssrs.Clear();
                t_ssrs = tExeSQL.execSQL(t_sql2);
                int gd_count = 0; //count of individual
                if (t_ssrs.getMaxRow() > 0)
                {
                    for (int g_count = 1; g_count <= t_ssrs.getMaxRow();
                                       g_count++)
                    {
                        System.out.println("个单录入量是" + t_ssrs.GetText(g_count, 1));
                        gd_count = gd_count +
                                   Integer.parseInt(t_ssrs.GetText(g_count, 1));
                    }
                }
                System.out.println("个单的录入总量是" + gd_count);
                //查询出银行代理的新单录入量
                String t_sql3 = " select nvl(Count(distinct(PrtNo)),0) from LCPol where SaleChnl = '03' and Operator = '"
                                + strOperator.trim() + "' and MakeDate >='" +
                                strStartDate.trim() + "' and MakeDate <='"
                                + strEndDate.trim() +
                        "' and mainpolno = polno  and RenewCount = '0' union  "
                                + " select nvl(Count(distinct(PrtNo)),0) from LBPol where SaleChnl = '03' and Operator = '"
                                + strOperator.trim() + "' and MakeDate >='" +
                                strStartDate.trim() + "' and MakeDate <='"
                                + strEndDate.trim() +
                                "' and mainpolno = polno and RenewCount = '0' ";
                System.out.println("银行代理的查询是" + t_sql3);
                t_ssrs.Clear();
                t_ssrs = tExeSQL.execSQL(t_sql3);
                int yd_count = 0; //count of banksupply
                if (t_ssrs.getMaxRow() > 0)
                {
                    for (int y_count = 1; y_count <= t_ssrs.getMaxRow();
                                       y_count++)
                    {
                        System.out.println("银行代理的数量是" + yd_count);
                        yd_count = yd_count +
                                   Integer.parseInt(t_ssrs.GetText(y_count, 1));
                    }
                }
                System.out.println("银行代理的总数量是" + yd_count);
                int count = gd_count + td_count + yd_count;
                System.out.println("总量是" + count);

                String t_sql4 = "select nvl(Count(distinct(ProposalNo)),0) from LCIssuePol where BackObjType = '1' and BackObj = '"
                                + strOperator.trim() + "' and MakeDate >='" +
                                strStartDate.trim() + "' and MakeDate <='"
                                + strEndDate.trim() + "'  ";
                t_ssrs = tExeSQL.execSQL(t_sql4);

                int back_count = Integer.parseInt(t_ssrs.GetText(1, 1));
                cols[0] = strOperator;
                cols[1] = String.valueOf(count);
                cols[2] = String.valueOf(gd_count);
                cols[3] = String.valueOf(yd_count);
                cols[4] = String.valueOf(td_count);
                cols[5] = String.valueOf(back_count);
                td_sum = td_sum + td_count;
                gd_sum = gd_sum + gd_count;
                yd_sum = yd_sum + yd_count;
                back_sum = back_sum + back_count;
                sum = sum + count;
                alistTable.add(cols);
            }

            String[] col = new String[6];
            xmlexport.addDisplayControl("displayinfo");
            xmlexport.addListTable(alistTable, col);
            texttag.add("StartDate", strStartDate);
            texttag.add("EndDate", strEndDate);
            texttag.add("Count", sum);
            texttag.add("Count1", gd_sum);
            texttag.add("Count2", yd_sum);
            texttag.add("Count3", td_sum);
            texttag.add("BackCount", back_sum);
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            // xmlexport.outputDocumentToFile("e:\\","InputEfficiency");//输出xml文档到文件
            mResult.clear();
            mResult.addElement(xmlexport);
        }
        if (strOperateFlag.equals("Check"))
        {
            ListTable tlistTable = new ListTable();
            tlistTable.setName("MODE");
            String t_SQL = "";
            t_SQL =
                    "select distinct(ApproveCode) from LCPol where ApproveDate >='"
                    + strStartDate.trim() + "' and ApproveDate <='" +
                    strEndDate.trim() + "' union "
                    +
                    "select distinct(ApproveCode) from LBPol where ApproveDate >='"
                    + strStartDate.trim() + "' and ApproveDate <='" +
                    strEndDate.trim() + "'";
            System.out.println("查询所以符合人员的sql是" + t_SQL);
            ExeSQL exeSQL = new ExeSQL();
            SSRS ssrs = exeSQL.execSQL(t_SQL);
            int i_count = ssrs.getMaxRow();
            if (i_count == 0)
            {
                CError tError = new CError();
                tError.moduleName = "PEfficiencyBL";
                tError.functionName = "getDutyGetClmInfo";
                tError.errorMessage = "在该时间段内没有进行符合的信息，请确认起止日期是否正确！！！";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (((i_count == 1) && (ssrs.GetText(1, 1).equals(""))) ||
                ((i_count == 1) && (ssrs.GetText(1, 1).equals(null))))
            {
                CError tError = new CError();
                tError.moduleName = "PEfficiencyBL";
                tError.functionName = "getDutyGetClmInfo";
                tError.errorMessage = "在该时间段内没有做复核操作，请确认起止日期是否正确！！！";
                this.mErrors.addOneError(tError);
                return false;
            }
            TextTag texttag = new TextTag(); //Create a TextTag instance
            XmlExport xmlexport = new XmlExport(); //Create a XmlExport instance
            xmlexport.createDocument("CheckEfficiency.vts", "printer");
            ListTable alistTable = new ListTable();
            alistTable.setName("INFO");
            int td_sum = 0; //团单个数
            int gd_sum = 0; //个单个数
            int yd_sum = 0; //银行代理个数
            int sum = 0; //总个数

            for (int j = 1; j <= i_count; j++)
            {
                String[] cols = new String[5];
                String strApproveCode = ssrs.GetText(j, 1);
                ExeSQL tExeSQL = new ExeSQL();
                SSRS t_ssrs;
                String t_sql1 = "select nvl(Count(distinct(PrtNo)),0) from LCPol where SaleChnl = '01' and ApproveCode = '"
                                + strApproveCode.trim() +
                                "' and ApproveDate >='" + strStartDate.trim()
                                + "' and ApproveDate <='" + strEndDate.trim() +
                        "' and mainpolno = polno and RenewCount = '0' union "
                                + "select nvl(Count(distinct(PrtNo)),0) from LBPol where SaleChnl = '01' and ApproveCode = '"
                                + strApproveCode.trim() +
                                "' and ApproveDate >='" + strStartDate.trim()
                                + "' and ApproveDate <='" + strEndDate.trim() +
                                "' and mainpolno = polno and RenewCount = '0' ";
                System.out.println("团单的查询语句是" + t_sql1);
                t_ssrs = tExeSQL.execSQL(t_sql1);
                int td_count = 0; //记录团单的个数
                if (t_ssrs.getMaxRow() > 0)
                {
                    for (int t_count = 1; t_count <= t_ssrs.getMaxRow();
                                       t_count++)
                    {
                        System.out.println("符合的团单的数量是：" +
                                           t_ssrs.GetText(t_count, 1));
                        td_count = td_count +
                                   Integer.parseInt(t_ssrs.GetText(t_count, 1));
                    }
                }

                System.out.println("符合的团单的数量的合计是：" + td_count);
                String t_sql2 = "select Count(distinct(PrtNo)) from LCPol where SaleChnl = '02' and ApproveCode = '"
                                + strApproveCode.trim() +
                                "' and ApproveDate >='" + strStartDate.trim()
                                + "' and ApproveDate <='" + strEndDate.trim() +
                        "' and mainpolno = polno and RenewCount = '0' union "
                                + "select Count(distinct(PrtNo)) from LBPol where SaleChnl = '02' and ApproveCode = '"
                                + strApproveCode.trim() +
                                "' and ApproveDate >='" + strStartDate.trim()
                                + "' and ApproveDate <='" + strEndDate.trim() +
                                "' and mainpolno = polno and RenewCount = '0' ";
                System.out.println("查询个单的语句是" + t_sql2);
                t_ssrs.Clear();
                t_ssrs = tExeSQL.execSQL(t_sql2);
                int gd_count = 0; //个单的个数
                if (t_ssrs.getMaxRow() > 0)
                {
                    for (int g_count = 1; g_count <= t_ssrs.getMaxRow();
                                       g_count++)
                    {
                        System.out.println("符合个单数量" + gd_count);
                        gd_count = gd_count +
                                   Integer.parseInt(t_ssrs.GetText(g_count, 1));
                    }
                }
                System.out.println("符合个单总数是" + gd_count);

                //查询银行代理的语句
                String t_sql3 = "select nvl(Count(distinct(PrtNo)),0) from LCPol where SaleChnl = '03' and ApproveCode = '"
                                + strApproveCode.trim() +
                                "' and ApproveDate >='" + strStartDate.trim()
                                + "' and ApproveDate <='" + strEndDate.trim() +
                        "' and mainpolno = polno and RenewCount = '0' union "
                                + "select nvl(Count(distinct(PrtNo)),0) from LBPol where SaleChnl = '03' and ApproveCode = '"
                                + strApproveCode.trim() +
                                "' and ApproveDate >='" + strStartDate.trim()
                                + "' and ApproveDate <='" + strEndDate.trim() +
                                "' and mainpolno = polno and RenewCount = '0' ";
                System.out.println("银行代理的查询语句是" + t_sql3);
                t_ssrs.Clear();
                t_ssrs = tExeSQL.execSQL(t_sql3);
                int yd_count = 0; //记录银行代理的个数
                if (t_ssrs.getMaxRow() > 0)
                {
                    for (int y_count = 1; y_count <= t_ssrs.getMaxRow();
                                       y_count++)
                    {
                        System.out.println("符合银行代理的数量是" +
                                           t_ssrs.GetText(y_count, 1));
                        yd_count = yd_count +
                                   Integer.parseInt(t_ssrs.GetText(y_count, 1));
                    }
                }
                int count = gd_count + td_count + yd_count; //记录总的个数;
                cols[0] = strApproveCode;
                cols[1] = String.valueOf(count);
                cols[2] = String.valueOf(gd_count);
                cols[3] = String.valueOf(yd_count);
                cols[4] = String.valueOf(td_count);
                td_sum = td_sum + td_count;
                gd_sum = gd_sum + gd_count;
                yd_sum = yd_sum + yd_count;
                sum = sum + count;
                alistTable.add(cols);
            }
            String[] col = new String[5];
            xmlexport.addDisplayControl("displayinfo");
            xmlexport.addListTable(alistTable, col);
            texttag.add("StartDate", strStartDate);
            texttag.add("EndDate", strEndDate);
            texttag.add("Count", sum);
            texttag.add("Count1", gd_sum);
            texttag.add("Count2", yd_sum);
            texttag.add("Count3", td_sum);
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            //xmlexport.outputDocumentToFile("e:\\","checkEfficiency");//输出xml文档到文件
            mResult.clear();
            mResult.addElement(xmlexport);
        }
        return true;
    }

    /*
     * @function :in order to debug
     */
    public static void main(String[] args)
    {

        String tStartDate = "2004-07-01";
        String tEndDate = "2004-07-28";
        //String test = "123";
        //int receive = 0;
        //receive = Integer.parseInt(test);
        String tOperate = "Input";
        VData tVData = new VData();
        tVData.addElement(tStartDate);
        tVData.addElement(tEndDate);
        tVData.addElement(tOperate);
        PEfficiencyUI tPEfficiencyUI = new PEfficiencyUI();
        tPEfficiencyUI.submitData(tVData, "PRINT");

    }
}