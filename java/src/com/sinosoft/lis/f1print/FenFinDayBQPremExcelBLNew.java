package com.sinosoft.lis.f1print;

/**
 * <p>Title:FenFinDayBQPremExcelBL </p>
 * <p>Description:X4-保全保费日结单 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author	yan
 * @version 1.0
 */

import java.io.File;
import java.text.DecimalFormat;

import jxl.*;
import jxl.format.Alignment;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.*;


import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class FenFinDayBQPremExcelBLNew {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	// 取得的时间
	private String mDay[] = null;
	private String tOutXmlPath = "";
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	public FenFinDayBQPremExcelBLNew() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		if (!getInputData(cInputData)) {
			return false;
		}	
		// 准备所有要打印的数据
		if (cOperate.equals("PRINTPAY"))
		{
			if (!getPrintDataPay()) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		GlobalInput tG = new GlobalInput();
		tG.Operator = "001";
		tG.ManageCom = "86110000";
		VData vData = new VData();
		String[] tDay = new String[2];
		tDay[0] = "2004-1-13";
		tDay[1] = "2004-1-13";
		vData.addElement(tDay);
		vData.addElement(tG);

		FenFinDayBQPremExcelBLNew tF = new FenFinDayBQPremExcelBLNew();
		tF.submitData(vData, "PRINTPAY");

	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) // 打印付费
	{
		// 全局变量
		mDay = (String[]) cInputData.get(0);
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		tOutXmlPath = (String)cInputData.get(2);
		
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		
		if (tOutXmlPath == null || tOutXmlPath.equals("")) {
			buildError("getInputData", "没有得到文件路径的信息！");
			return false;
		}
		
		System.out.println("打印日期-->"+mDay[0]+"至"+mDay[1]);
		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "FenFinDayBQPremExcelBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
    
	/**
	 * 新的打印方法
	 * 
	 * @return
	 */
	private boolean getPrintDataPay() {

		WritableWorkbook book = null;
		try{
			
			book = Workbook.createWorkbook(new File(tOutXmlPath));
			WritableSheet sheet0 = book.createSheet(PubFun.getCurrentDate(), 0);
			
			WritableFont wf = new WritableFont(
					WritableFont.ARIAL, 	//定义格式
					10,						//字体
					WritableFont.NO_BOLD,	//粗体
					false,					//斜体
					UnderlineStyle.NO_UNDERLINE, //下划线
					jxl.format.Colour.BLACK		//黑色
			);

			WritableCellFormat wcf = new WritableCellFormat(wf);
			wcf.setAlignment(Alignment.LEFT); //设置对齐方式--左对齐
			wcf.setVerticalAlignment(VerticalAlignment.CENTRE);
//			wcf.setBorder(Border.ALL, BorderLineStyle.THIN);
			
			ExeSQL tExeSQL = new ExeSQL();
			SSRS manageSSRS = new SSRS();
			String nsql = "select Name from LDCom where ComCode='"+ mGlobalInput.ManageCom + "'"; // 管理机构
			String manageCom = tExeSQL.getOneValue(nsql);
			
			sheet0.addCell(new Label(0,0,"保全日结单",wcf));
			sheet0.addCell(new Label(0,1,"统计日期",wcf));
			sheet0.addCell(new Label(1,1,mDay[0]+"至"+mDay[1],wcf));
			sheet0.addCell(new Label(0,2,"管理机构",wcf));
			sheet0.addCell(new Label(1,2,manageCom,wcf));
			sheet0.addCell(new Label(0,4,"类型",wcf));
			sheet0.addCell(new Label(1,4,"渠道",wcf));
			sheet0.addCell(new Label(2,4,"批单号码",wcf));
			sheet0.addCell(new Label(3,4,"保单号码",wcf));
			sheet0.addCell(new Label(4,4,"金额",wcf));
			
			GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
			tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
			tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
			tGetSQLFromXML.setParameters("EndDate", mDay[1]);
			
			String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
//			tServerPath ="E:/lisdev/ui/";
			
			String[] getTypes = new String[] { 
					"BQTF", // 保全退费
//					"YGBJ", //预估结余返还数据---2014-6-13号新添加
					"BQTBJ", // 退保金
					"BQYEI", // YEI
					"BQMJ", // 满期金
					"BQMJLX",// 满期利息
					"BQSSBF", // 实收保费
					"BQTJ", // 体检费
					"BQFX", // 保单复效利息
					"BQCJ", // 保户储金及投资款
					"BQTZLX", // 投资款业务利息
					"BQTZLX-H",   //（委托管理）帐号管理费,20150127,解青青添加
					"BQGL", // 账户管理费
					"BQGB", // 工本费
					"WNSSBF_BJCJ3",//万能实收保费-保户储金
					"WNSSBF_ZHGLF3",//万能实收保费-账户管理费
					"WNFX_CSKF", //万能复效-初始扣费----2014-3-21号新添加
	                "WNFX_FXBF", //万能复效-风险保费----2014-3-21号新添加
					"XTXSSBF_BHCJ3",//新特需实收保费-保户储金
					"XTXSSBF_ZHGLF3",//新特需实收保费-账户管理费
					"WNYJ_ZHLX3",//万能月结-账户利息
					"WNYJ_FXBF3",//万能月结-风险保费
					"WNYJ_BDGLF3",//万能月结-保单管理费
//					"WNBFLQ_ZHGLF3",//万能部分领取-账户管理费
//					"WNBQTB_BDGLF3",//万能保全退保-保单管理费
					"WNBQTB_FXKF3",//万能保全退保-风险扣费
					"WNBQTB_ZHLX3",//万能保全退保-账户利息
					"WNBQTB_ZHGLF3",//万能保全退保-账户管理费
//					"XTXBQXYTB_JYGLF3",//新特需保全协议退保-解约管理费				
					"BQCM",			//万能资料变更
					"BQDQJSJE",		//定期结算
//					"XTXBFLQ_ZHGLF3",//新特需部分领取-账户管理费
//					"XTXBQTB_BDGLF3",//新特需保全退保-保单管理费
//					"XTXBQTB_FXKF3",//新特需保全退保-风险扣费
//					"XTXBQTB_ZHLX3",//新特需保全退保-账户利息
//					"XTXBQTB_ZHGLF3"//新特需保全退保-账户管理费
					"WNBJ_BFLQ",       //万能——本金——部分领取
					"WNGLF_BFLQ",      //万能——利息——部分领取
					"XTXBJ_BFLQ",      //新特需——本金——部分领取
					"XTXGLF_BFLQ",     //新特需——本金——部分领取
					"WN_CXJJ",         //万能-持续奖金
					"WN_JYGLF",        //万能-解约管理费
					"BDZF",            //保单作废
					"YDTB_FC",		//约定缴费退保应收反冲
					"HL_XJ",           //分红险红利结算-现金领取：包括周年日领取和退保时临时结算领取
					"HL_LX",            //保单红利支出—累积生息：包括周年日进账户和退保时临时结算账户利息
					"BQPR",         //保全万能保单迁移本金
					"BQYS",			//保全预收保费-账户余额
					"YGBJ", //预估结余返还数据---2015-5-5号新添加
					"YGBJFC", //预估结余返还数据---2015-5-6号新添加
					"BQMJJT", // 满期金计提
				   "BQMJJT-F" // 满期金计提反冲
			};
			
			int original = 5;
			
			for(int node=0; node<getTypes.length; node++){
				String msql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FenFeePrintSql.xml", getTypes[node]);
				manageSSRS = tExeSQL.execSQL(msql);
				int maxrows = manageSSRS.getMaxRow();
				for(int col = 0; col < 5 ; col++){
					for(int row = original; row <= maxrows+original-1; row++){
						sheet0.addCell(new Label(col,row,manageSSRS.GetText(row-original+1, col+1),wcf));
					}
				}
				original += maxrows;
				
				msql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FenFeePrintSql.xml",getTypes[node] + "Z");
				manageSSRS = tExeSQL.execSQL(msql);
				maxrows = manageSSRS.getMaxRow();
				System.out.println("maxrows===="+maxrows);
				if(manageSSRS.getMaxRow()==0 || manageSSRS == null){
					String[] str = getChineseNameWithPY(getTypes[node]+"Z");
					sheet0.addCell(new Label(0,original,str[0],wcf));
					sheet0.addCell(new Label(3,original,"总计金额",wcf));
					sheet0.addCell(new Label(4,original,str[1],wcf));
					
					original += 2;
				}else{
					System.out.println("original===="+original);
					for(int rows = original; rows <= maxrows+original-1; rows++){
						System.out.println("总金额为："+manageSSRS.GetText(rows-original+1, 2));
						sheet0.addCell(new Label(0,rows,manageSSRS.GetText(rows-original+1, 1),wcf));
						sheet0.addCell(new Label(3,rows,"总计金额",wcf));
						sheet0.addCell(new Label(4,rows,new DecimalFormat("0.00").format(Double.valueOf(manageSSRS.GetText(rows-original+1, 2))),wcf));
					}
					original += maxrows +1 ;
				}
			}
			sheet0.addCell(new Label(0,original+1,"制表员：",wcf));
			sheet0.addCell(new Label(2,original+1,"审核员：",wcf));
			System.out.println("X4文件打印行数->"+original);
			book.write();			
			book.close();
			System.out.println("生成X4文件完成");
			return true;
		}catch(Exception e){
			e.printStackTrace();
			buildError("WriteXls","生成报表失败!原因是"+e.toString());
	        return false;
		}
	}

	/**
	 * 显示无金额的情况
	 * 
	 * @param pName
	 * @return
	 */
	private String[] getChineseNameWithPY(String pName) {
		String[] result = new String[2];
		if ("BQTFZ".equals(pName)) {
			result[0] = "保全退费 合计";
		} else if ("BQTBJZ".equals(pName)) {
			result[0] = "退保金 合计";
		} else if ("BQYEIZ".equals(pName)) {
			result[0] = "YEI 合计";
		} else if ("BQMJZ".equals(pName)) {
			result[0] = "满期金 合计";
		} else if ("BQMJLXZ".equals(pName)) {
			result[0] = "满期利息 小计";
		}else if ("BQSSBFZ".equals(pName)) {
			result[0] = "实收保费 合计";
		} else if ("BQTJZ".equals(pName)) {
			result[0] = "体检费 合计";
		} else if ("BQFXZ".equals(pName)) {
			result[0] = "保单复效利息 合计";
		} else if ("BQCJZ".equals(pName)) {
			result[0] = "保户储金及投资款 合计";
		} else if ("BQTZLXZ".equals(pName)) {
			result[0] = "投资款业务利息 合计";
		} else if ("BQGLZ".equals(pName)) {
			result[0] = "账户管理费 合计";
		} else if ("BQGBZ".equals(pName)) {
			result[0] = "工本费 合计";
		}else if ("WNSSBF_BJCJ3Z".equals(pName)) {
			result[0] = "万能实收保费-保户储金 合计";
		} else if ("WNSSBF_ZHGLF3Z".equals(pName)) {
			result[0] = "能实收保费-账户管理费 合计";
		}else if ("WNFX_CSKFZ".equals(pName)) {
			result[0] = "万能复效-初始扣费 合计";
		}else if ("WNFX_FXBFZ".equals(pName)) {
			result[0] = "万能复效-风险保费 合计";
		} else if ("XTXSSBF_BHCJ3Z".equals(pName)) {
			result[0] = "新特需实收保费-保户储金 合计";
		} else if ("XTXSSBF_ZHGLF3Z".equals(pName)) {
			result[0] = "新特需实收保费-账户管理费 合计";
		} else if ("WNYJ_ZHLX3Z".equals(pName)) {
			result[0] = "万能月结-账户利息 合计";
		} else if ("WNYJ_FXBF3Z".equals(pName)) {
			result[0] = "万能月结-风险保费 合计";
		} else if ("WNYJ_BDGLF3Z".equals(pName)) {
			result[0] = "万能月结-保单管理费 合计";
		} else if ("WNBFLQ_ZHGLF3Z".equals(pName)) {
			result[0] = "万能部分领取-账户管理费 合计";
		} else if ("WNBQTB_BDGLF3Z".equals(pName)) {
			result[0] = "万能保全退保-保单管理费 合计";
		} else if ("WNBQTB_FXKF3Z".equals(pName)) {
			result[0] = "万能保全退保-风险扣费 合计";
		} else if ("WNBQTB_ZHLX3Z".equals(pName)) {
			result[0] = "万能保全退保-账户利息 合计";
		} else if ("WNBQTB_ZHGLF3Z".equals(pName)) {
			result[0] = "万能保全退保-账户管理费 合计";
		} else if ("XTXBQXYTB_JYGLF3Z".equals(pName)) {
			result[0] = "新特需保全协议退保-解约管理费 合计";
		} else if ("BQCMJZ".equals(pName)) {
			result[0] = "万能客户资料变更 合计";
		} else if ("BQDQJSJEHJ".equals(pName)) {
			result[0] = "保全定期结算 合计";	
		}else if ("WNBJ_BFLQZ".equals(pName)) {
			result[0] = "万能保全部分领取-本金 合计";	
		}else if ("WNGLF_BFLQZ".equals(pName)) {
			result[0] = "万能保全部分领取-利息 合计";	
		}else if ("XTXBJ_BFLQZ".equals(pName)) {
			result[0] = "新特需保全部分领取-本金  合计";	
		}else if ("XTXGLF_BFLQZ".equals(pName)) {
			result[0] = "新特需保全部分领取-管理费 合计";	
		}else if ("WN_CXJJZ".equals(pName)) {
			result[0] = "万能-持续奖金 合计";	
		}else if ("WN_JYGLFZ".equals(pName)) {
			result[0] = "万能-解约管理费 合计";	
		}else if ("BDZFZ".equals(pName)) {
			result[0] = "保单作废 合计";	
		}else if ("YDTB_FCZ".equals(pName)) {
			result[0] = "约定缴费退保反冲 合计";
		}else if ("HL_XJZ".equals(pName)) {
			result[0] = "分红险红利结算-现金领取 合计";	
		}else if ("HL_LXZ".equals(pName)) {
			result[0] = "分红险红利结算-累积生息 合计";	
		}else if ("BQPRZ".equals(pName)) {
			result[0] = "保全万能保单迁移 合计";	
		}else if ("BQYSZ".equals(pName)) {
			result[0] = "保全预收保费-账户余额 合计";	
		}else if ("YGBJZ".equals(pName)) {
			result[0] = "预估结余返还数据 小计";
		}else if ("YGBJFCZ".equals(pName)) {
			result[0] = "预估结余返还反冲数据 小计";
		}else if ("BQMJJTZ".equals(pName)) {
			result[0] = "满期金计提 小计";
		}else if ("BQMJJT-FZ".equals(pName)) {
			result[0] = "满期金计提反冲 小计";
		}

		result[1] = "0.00";
		return result;
	}

}
