package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LAFycRateReportBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //取得的代理人编码
    private String mAgentCode = "";

//取得的机构外部代码 
    private String mBranchAttr = "";

//取得的管理机构代码
    private String mManageCom = "";
//取得前台传入结束日期
    private String mEndMonth = "";
 //取得前台传入起始日期
    private String mStartMonth = "";
    
    private String startDate  ="";
    private String EndDate = "";
    private String mState = "";
    private SSRS managSSRS = new SSRS();//管理机构容器
    private String branchattr = "";//所在的机构外部编码
	private String name = "";//所在机构的名称
	private String AgentGroup = "";
    //业务处理相关变量
    /** 全局数据 */
   private GlobalInput mGlobalInput = new GlobalInput();
   private String  mdate = PubFun.getCurrentDate();
   private String  mtime = PubFun.getCurrentTime();
    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }
/**
 * 求预计在职月
 */
private int getPresentMonth(String cAgentCode)
{
		   //先求业务员预计在职月
		   //判读业务员在职状态
		   int presentMonth = 0;
		   String state = "";
		   System.out.println("startDate"+startDate);
		   String SQL = "select agentstate,employdate,outworkdate from laagent where agentcode='"+cAgentCode+"'";
		   ExeSQL orExeSQL = new ExeSQL();
		   SSRS orSSRS = new SSRS();
		   orSSRS = orExeSQL.execSQL(SQL);	
		   state = orSSRS.GetText(1, 1);
		   System.out.println("outworkdate"+orSSRS.GetText(1, 3));
		   if(orSSRS.GetText(1, 3)!=null&&!orSSRS.GetText(1, 3).equals(""))
		   {//判断与统计止期之间的大小，如果离职时间大于统计止期则认为为在职状态
			   String mmsql = "select date('"+EndDate+"')-date('"+orSSRS.GetText(1, 3)+"') from dual";
		       String mdays = orExeSQL.getOneValue(mmsql);
		       if(mdays.compareTo("0")<0)
		       {
		    	   state = "01";
		       }
		   }
		   System.out.println("state"+state);
		   if(state.compareTo("06")<0)//表示现在还是处于在职状态
		   {
			   
			   String msql = "select date('"+startDate+"')-date('"+orSSRS.GetText(1, 2)+"') from dual";
		       String days = orExeSQL.getOneValue(msql);
		       if(days.compareTo("0")<=0)//入司时间在统计起期之后
		       {
	    	      System.out.println("########"+Integer.parseInt(orSSRS.GetText(1, 2).substring(5,7)));
		    	  presentMonth = 12-Integer.parseInt(orSSRS.GetText(1, 2).substring(5,7));
		    	  if(Integer.parseInt(mStartMonth.substring(0,4))<Integer.parseInt(mEndMonth.substring(0,4)))//跨年   								    	   
		    		   presentMonth = presentMonth+12*
		    		   (Integer.parseInt(mEndMonth.substring(0,4))-Integer.parseInt(mStartMonth.substring(0,4)));
		       }
		       else//入司时间在统计起期之前
		       {
		    	   presentMonth = 12;
		    	   if(Integer.parseInt(mStartMonth.substring(0,4))<Integer.parseInt(mEndMonth.substring(0,4)))//跨年   								    	   
		    		   presentMonth = presentMonth+12*
		    		   (Integer.parseInt(mEndMonth.substring(0,4))-Integer.parseInt(mStartMonth.substring(0,4)));   								    	  							    	   
		       }
		   }
		   else//现状态为离职状态
		   {
			  
			   String msql = "select date('"+startDate+"')-date('"+orSSRS.GetText(1, 2)+"') from dual";
		       String days = orExeSQL.getOneValue(msql);
		       if(days.compareTo("0")<=0)//入司时间在统计起期之后
		       {
	    	   
		    	  presentMonth = Integer.parseInt(orSSRS.GetText(1, 3).substring(5,7))-Integer.parseInt(orSSRS.GetText(1, 2).substring(5,7));
		    	  if(Integer.parseInt(mStartMonth.substring(0,4))<Integer.parseInt(mEndMonth.substring(0,4)))//跨年   								    	   
		    		   presentMonth = presentMonth+12*
		    		   (Integer.parseInt(mEndMonth.substring(0,4))-Integer.parseInt(mStartMonth.substring(0,4)));
		       }
		       else//入司时间在统计起期之前
		       {
		    	   presentMonth = Integer.parseInt(orSSRS.GetText(1, 3).substring(5,7));
		    	   if(Integer.parseInt(mStartMonth.substring(0,4))<Integer.parseInt(mEndMonth.substring(0,4)))//跨年   								    	   
		    		   presentMonth = presentMonth+12*
		    		   (Integer.parseInt(mEndMonth.substring(0,4))-Integer.parseInt(mStartMonth.substring(0,4)));   								    	  							    	   
		       }
		   }
	
	return 	presentMonth;
}
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
    	 mGlobalInput.setSchema((GlobalInput) cInputData.
                 getObjectByObjectName("GlobalInput", 0));
    	mManageCom = (String)cInputData.get(0);
    	mBranchAttr = (String)cInputData.get(1);
    	mAgentCode = (String)cInputData.get(2);
    	mStartMonth = (String)cInputData.get(3);
    	mEndMonth = (String)cInputData.get(4);
    	mState = (String)cInputData.get(5);
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {       
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
        txmlexport.createDocument("LAFycRate.vts", "printer"); //最好紧接着就初始化xml文档
        ListTable tListTable = new ListTable();
        tListTable.setName("LFR");
        String[] title = {"","","","","","","","","","","","","",""};
        //处理时间
        startDate = mStartMonth.substring(0,4)+"-"+mStartMonth.substring(4,6)+"-01";//统计起期月份的1号
        String endDate1 = mEndMonth.substring(0,4)+"-"+mEndMonth.substring(4,6)+"-01";
        String endSQL = "select date('"+endDate1+"')+1 month from dual";
        ExeSQL endExeSQL = new ExeSQL();
        String endDate2 = endExeSQL.getOneValue(endSQL);
        String EndDateSQL = "select date('"+endDate2+"')-1 day from dual";
        EndDate = endExeSQL.getOneValue(EndDateSQL);
        System.out.println("mAgentCode"+mAgentCode);
        System.out.println("mBranchAttr"+mBranchAttr);
        int n = 1;
       if((this.mAgentCode==null||this.mAgentCode.equals(""))&&
    		  (this.mBranchAttr==null||this.mBranchAttr.equals("")))
       {
    	   String sql = "";
          //判断是否有输入机构，输入机构按机构查询，未输入按总公司查询
    	   if(this.mManageCom==null||this.mManageCom.equals(""))
    	   {
    		   sql = "select * from ldcom where comcode like '86%' and length(trim(comcode))=8 and sign='1' order by comcode";
    	   }
    	   else
    		   sql ="select * from ldcom where comcode like '"+this.mManageCom+"%' and length(trim(comcode))=8 order by comcode";
    	   System.out.println(sql);
    	   LDComDB tLDComDB = new LDComDB();
    	   LDComSet tLDComSet = new LDComSet();
    	   tLDComSet = tLDComDB.executeQuery(sql);
    	   //机构循环
    	   if(tLDComSet.size()>=1)
    	   {
    		   
    		   //查询机构下的所有销售机构		  
    		   for(int i=1;i<=tLDComSet.size();i++)
    		   {
    			String jgSQL = "";
    			//中介不包括在内
    			jgSQL = "select * from labranchgroup where managecom like '"+tLDComSet.get(i).getComCode()+"%' and (state is null or state='0') " +
    					"and endflag='N' and branchlevel<>'31' and branchtype='2' and branchtype2='01'";
    			System.out.println(jgSQL);
    			LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
    			LABranchGroupSet tLABranchGroupSet =new LABranchGroupSet();
    			tLABranchGroupSet = tLABranchGroupDB.executeQuery(jgSQL);
    			if(tLABranchGroupSet.size()>=1)
    			{   
    				for(int j=1;j<=tLABranchGroupSet.size();j++)
    				{
    					//获取地区类型
    					String areatype=AgentPubFun.getAreaType(tLABranchGroupSet.get(j).getAgentGroup());
    					System.out.println("areatype"+areatype);
    					String personSQL = "";
    					if(this.mState.equals("1"))//统计止期仍然在职的业务员
    					{			
    					  personSQL = "SELECT * from latree where agentseries='0' and branchtype='2' and branchtype2='01' and agentgroup='"+tLABranchGroupSet.get(j).getAgentGroup()+"'" +
    							" and  agentcode in (select agentcode from laagent where employdate<='"+EndDate+"' and (outworkdate is null or outworkdate>'"+EndDate+"'))" ;
    					}
    					else if(this.mState.equals("2"))//统计期间内离职的业务员
    					{
    						 personSQL = "SELECT * from latree where agentseries='0' and branchtype='2' and branchtype2='01' and agentgroup='"+tLABranchGroupSet.get(j).getAgentGroup()+"'" +
 							" and  agentcode in (select agentcode from laagent where employdate<='"+EndDate+"' and outworkdate is not null and  outworkdate>='"+startDate+"' and outworkdate<='"+EndDate+"')" ;
    					}
    					else//统计期间内曾经在职的业务员
    						 personSQL = "SELECT * from latree where agentseries='0' and branchtype='2' and branchtype2='01' and agentgroup='"+tLABranchGroupSet.get(j).getAgentGroup()+"'" +
 							" and  agentcode in (select agentcode from laagent where employdate<='"+EndDate+"' and (outworkdate is null or outworkdate>='"+startDate+"'))" ;
    				    System.out.println(personSQL);
    					LATreeDB tLATreeDB = new LATreeDB();
    				   LATreeSet tLATreeSet = new LATreeSet();
    				   tLATreeSet = tLATreeDB.executeQuery(personSQL);
    				   //进行人员循环
    				   if(tLATreeSet.size()>=1)
    				   {  
    					   for(int k=1;k<=tLATreeSet.size();k++)
    					   {
    						   
    						   String agentGrade = AgentPubFun.getMonthAgentGrade(tLATreeSet.get(k).getAgentCode(),this.mEndMonth);
    						   ExeSQL tExeSQL = new ExeSQL();
    						   //查询人员对应的交叉销售保费
    						   String tProperTyprem ="";
    						   String interSQL = "select value(sum(propertyprem),0) from LAActiveCharge where agentcode ='"+tLATreeSet.get(k).getAgentCode()+"'" +
    						   		"and wageno >='"+this.mStartMonth+"' and wageno <='"+this.mEndMonth+"'";
    						   System.out.println(interSQL);
    						   tProperTyprem = tExeSQL.getOneValue(interSQL);
    						   if(tProperTyprem==null||tProperTyprem.equals("")){
    							   tProperTyprem="0";
    						   }
    						   //查询实收保费
    						   String tTransMoney="";
    						   String tTransMoneySql =" select value(sum(transmoney),0) from lacommision where agentcode = '"+tLATreeSet.get(k).getAgentCode()+"'" +
    						   		" and wageno >='"+this.mStartMonth+"' and wageno <='"+this.mEndMonth+"'  ";
    						   System.out.println("tTransMoneySql"+tTransMoneySql);
    						   tTransMoney = tExeSQL.getOneValue(tTransMoneySql);
    						   if(tTransMoney==null || tTransMoney.equals(""))
    						   {
    							   tTransMoney="0";
    						   }
    						   //查询时间段内的完成的累计标保
    						   double Rate = 0.00;//达标完成率
    						   String PremSQL = "";
    						   String standardPrem = "";
    						   PremSQL = "select value(sum(standprem),0) from lacommision where agentcode='"+tLATreeSet.get(k).getAgentCode()+"'" +
    						   		" and wageno>='"+this.mStartMonth+"' and wageno<='"+this.mEndMonth+"'";
    						   System.out.println(PremSQL);

    						   String standPrem = tExeSQL.getOneValue(PremSQL);
    						   //查询人员职级所对应的考核标准
    						   String standardSQL = "select value(indfycsum,0) from LAAgentPromRadix where agentgrade='"+agentGrade+"'" +
    						   		" and destagentgrade = '"+tLATreeSet.get(k).getAgentGrade()+"' and areatype='"+areatype+"'";
    						  System.out.println(standardSQL);
    					      standardPrem = tExeSQL.getOneValue(standardSQL);
    					      if(standardPrem==null||standardPrem.equals(""))
 						        {
    					    	    standardPrem ="0";
 						        	Rate = 100;
 						        }else if(standPrem.compareTo("0")<0)
 						        {
 						        	Rate=0;
 						        }else
 						       {
    							   //判断职级
    							   System.out.println("AgentGrade"+tLATreeSet.get(k).getAgentGrade() + "_newAgentGrade:" + agentGrade);
    							   if(tLATreeSet.get(k).getAgentGrade().equals("D01"))
    							   {
    								   String standardSQL1 = "select value(indfycsum,0) from LAAgentPromRadix where agentgrade='"+agentGrade+"'" +
       						   		" and destagentgrade = 'D02' and areatype='"+areatype+"'";
       						        System.out.println(standardSQL1);
       						        standardPrem = tExeSQL.getOneValue(standardSQL1);
      						   
       						    	   Rate = Arith.div(Double.parseDouble(standPrem), Double.parseDouble(standardPrem));
    							   }    							   
          						       else
          						       {
    								  int month =  getPresentMonth(tLATreeSet.get(k).getAgentCode());
    								  System.out.println("month="+month);
    								  if(month==0)						  
    									  continue;
    								  System.out.println("standardPrem---->>"+standardPrem);
    								  System.out.println("month---->>"+month);
    								  double ms = Arith.div(Double.parseDouble(standardPrem)*month, 12);
    							      Rate = Arith.div(Double.parseDouble(standPrem), ms);
          						       }
    							   }    	
    							   Rate = Arith.round(Rate, 2);
    							   
    							   //    							 查询统一工号： 2014-11-21  解青青
    							   ExeSQL tttExeSQL = new ExeSQL();
    							  String tttGAC = "select groupagentcode from laagent where agentcode='"+tLATreeSet.get(k).getAgentCode()+"'";
    							  String tttGroupAgentCode = tttExeSQL.getOneValue(tttGAC);
    						   
    						   //查询代理人姓名
    						   String AgentInfo = "select name,employdate from laagent where agentcode='"+tLATreeSet.get(k).getAgentCode()+"'";
    						   SSRS InfoSSRS = new SSRS();
    						   InfoSSRS = tExeSQL.execSQL(AgentInfo);
    						   if(InfoSSRS.mErrors.needDealError())
    						   {
    							   CError.buildErr(this, "代理人信息查询失败");
    							   return false;
    						   }
    						   //查询代理人的期末职级
    						   
    						   System.out.println("~~~~~~~~~~~~~");
    						   String Info[] = new String[14];    					    	
    					    	  Info[0] = String.valueOf(n);  
    					    	  Info[1] = tLDComSet.get(i).getComCode();
    					    	  Info[2] = tLDComSet.get(i).getName();
    					    	  Info[3] = tLABranchGroupSet.get(j).getName();
    					    	  Info[4] = tLABranchGroupSet.get(j).getBranchAttr();
    					    	  Info[5] = InfoSSRS.GetText(1, 1);
    					    	  Info[6] =tttGroupAgentCode;
//    					    	  Info[6] = tLATreeSet.get(k).getAgentCode();
    					    	  Info[7] = AgentPubFun.getMonthAgentGrade(tttGroupAgentCode,this.mEndMonth);
    					    	  Info[8] = InfoSSRS.GetText(1,2);
    					    	  Info[9] =  standPrem;
    					    	  Info[10] = standardPrem;
    					    	  Info[11] = tProperTyprem;
    					    	  Info[12] = tTransMoney;
    					    	  Info[13] = String.valueOf(Rate);
      					    	  tListTable.add(Info); 
      					    	  n++;
      					    	  System.out.println("n:"+n);
    					   }
    				   }
    				}
    			 }
    		   }
    	   }
       }
       else 
         {  //前台传入的是外部编码
    	   String personSQL  = "";
    	   ExeSQL tmExeSQL = new ExeSQL();
    	   if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
           {
    	   String AgentGroupSQL = "select agentgroup from labranchgroup where branchattr='"+this.mBranchAttr+"' and branchtype='2' and branchtype2='01'";    	   
    	   AgentGroup = tmExeSQL.getOneValue(AgentGroupSQL);   
//    	 查询机构下所有非主管人员
    	   if(this.mState.equals("1"))//统计止期仍然在职的业务员
			{			
			  personSQL = "SELECT * from latree where agentseries='0' and branchtype='2' and branchtype2='01' and agentgroup='"+AgentGroup+"'" +
					" and  agentcode in (select agentcode from laagent where employdate<='"+EndDate+"' and (outworkdate is null or outworkdate>='"+EndDate+"'))" ;
			}
			else if(this.mState.equals("2"))//统计期间内离职的业务员
			{
				 personSQL = "SELECT * from latree where agentseries='0' and branchtype='2' and branchtype2='01' and agentgroup='"+AgentGroup+"'" +
				" and  agentcode in (select agentcode from laagent where employdate<='"+EndDate+"' and outworkdate is not null and  outworkdate>='"+startDate+"' and outworkdate<='"+EndDate+"')" ;
			}
			else//统计期间内曾经在职的业务员
				
    	   personSQL = "SELECT * from latree where agentseries='0' and branchtype='2' and branchtype2='01' and agentgroup = '"+AgentGroup+"' " +
			" and agentcode in (select agentcode from laagent where employdate<='"+EndDate+"' and (outworkdate is null or outworkdate>='"+startDate+"'))" ;
		
    	   
    	   String managecomSQL = "select comcode,name from ldcom where comcode = (select managecom from labranchgroup where agentgroup='"+AgentGroup+"')";
    	   System.out.println(managecomSQL);
    	   managSSRS = tmExeSQL.execSQL(managecomSQL);
 		  if(managSSRS.getMaxRow()<=0||managSSRS.getMaxRow()>1)
 		  {
 			  CError.buildErr(this, "代理人的机构信息查询失败");
 			  return false;
 		  }
 		  String nameSQL = "select name from labranchgroup where agentgroup='"+AgentGroup+"'";
 		  ExeSQL tExeSQL = new ExeSQL();
 		  name = tExeSQL.getOneValue(nameSQL);
 		  branchattr = this.mBranchAttr;	   
           }
    	   
    	  if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
    	  {
    		  personSQL = "SELECT * from latree where agentseries='0' and branchtype='2' and branchtype2='01'  and agentcode='"+this.mAgentCode+"' " +
  			" and agentcode in (select agentcode from laagent where employdate<='"+EndDate+"' and (outworkdate is null or outworkdate>='"+startDate+"'))";
    		 System.out.println(personSQL);
    		  String managecomSQL = "select comcode,name from ldcom where comcode = (select managecom from laagent where agentcode='"+this.mAgentCode+"')";
    		  managSSRS = tmExeSQL.execSQL(managecomSQL);
    		  if(managSSRS.getMaxRow()<=0||managSSRS.getMaxRow()>1)
    		  {
    			  CError.buildErr(this, "代理人的管理机构信息查询失败！");
    			  return false;
    		  }
    		  //人员所在的机构
    		  String SQL = "select branchattr,name,agentgroup from labranchgroup where agentgroup=(select agentgroup from laagent where agentcode='"+this.mAgentCode+"')";
    	      ExeSQL mExeSQL = new ExeSQL();
    	      SSRS mSSRS = new SSRS();
    	      mSSRS = mExeSQL.execSQL(SQL);
    	      if(mSSRS.mErrors.needDealError())
    	      {
    	    	  this.mErrors.copyAllErrors(mSSRS.mErrors);
    	    	  return false;
    	      }
    	      if(mSSRS.getMaxRow()<=0||mSSRS.getMaxRow()>1)
    	      {
    	    	  CError.buildErr(this, "代理人的销售机构信息查询失败！");
    			  return false;
    	      }
    	      branchattr = mSSRS.GetText(1, 1);
    	      name = mSSRS.GetText(1, 2);
    	      AgentGroup = mSSRS.GetText(1, 3);
    	  }
    	   LATreeDB tLATreeDB = new LATreeDB();
		   LATreeSet tLATreeSet = new LATreeSet();
		   System.out.println(personSQL);
		   tLATreeSet = tLATreeDB.executeQuery(personSQL);
		   //进行人员循环
		   if(tLATreeSet.size()>=1)
		   {  		
			   for(int m=1;m<=tLATreeSet.size();m++)
			   {
				   
				   String agentGrade = AgentPubFun.getMonthAgentGrade(tLATreeSet.get(m).getAgentCode(),this.mEndMonth);

				   
				   ExeSQL tExeSQL = new ExeSQL();
				   //查询人员对应的交叉销售保费
				   String tProperTyprem ="";
				   String interSQL = "select value(sum(propertyprem),0) from LAActiveCharge where agentcode ='"+tLATreeSet.get(m).getAgentCode()+"'" +
				   		"and wageno >='"+this.mStartMonth+"' and wageno <='"+this.mEndMonth+"'";
				   System.out.println(interSQL);
				   tProperTyprem = tExeSQL.getOneValue(interSQL);
				   if(tProperTyprem==null||tProperTyprem.equals("")){
					   tProperTyprem="0";
				   }
				 //查询业务员的实收保费
				   String tTransMoney="";
				   String tTransMoneySql="";
				   tTransMoneySql =" select value(sum(transmoney),0) from lacommision where agentcode ='"+tLATreeSet.get(m).getAgentCode()+"'" +
				   		" and wageno >='"+this.mStartMonth+"' and wageno <= '"+this.mEndMonth+"' ";
				   tTransMoney = tExeSQL.getOneValue(tTransMoneySql);
				   if(tTransMoney == null || tTransMoney.equals(""))
				   {
					   tTransMoney = "0";
				   }
				 //查询时间段内的完成的累计标保
				   double Rate = 0.00;//达标完成率
				   String PremSQL = "";
				   String standardPrem = "";
				   PremSQL = "select value(sum(standprem),0) from lacommision where agentcode='"+tLATreeSet.get(m).getAgentCode()+"'" +
				   		" and wageno>='"+this.mStartMonth+"' and wageno<='"+this.mEndMonth+"'";
				   String standPrem = tExeSQL.getOneValue(PremSQL);
				   //查询人员职级所对应的考核标准
				   String standardSQL = "select value(indfycsum,0) from LAAgentPromRadix where agentgrade='"+agentGrade+"'" +
				   		" and destagentgrade = '"+tLATreeSet.get(m).getAgentGrade()+"' and areatype='"+AgentPubFun.getAreaType(AgentGroup)+"'";
				   standardPrem = tExeSQL.getOneValue(standardSQL);
				   if(standardPrem==null||standardPrem.equals(""))
				   {
					   standardPrem="0";
					   Rate = 100;
				   }else if(standPrem.compareTo("0")<0)
				    {
				        	Rate=0;
				     }
				   else
				   {
					   //判断职级
					   if(tLATreeSet.get(m).getAgentGrade().equals("D01"))
					   {
						   String standardSQL1 = "select value(indfycsum,0) from LAAgentPromRadix where agentgrade='"+agentGrade+"'" +
					   		" and destagentgrade = 'D02' and areatype='"+AgentPubFun.getAreaType(AgentGroup)+"'";
					      standardPrem = tExeSQL.getOneValue(standardSQL1);
						  Rate = Arith.div(Double.parseDouble(standPrem), Double.parseDouble(standardPrem))*100;
					   }
					   else
					   {
						  int month =  getPresentMonth(tLATreeSet.get(m).getAgentCode());
						  System.out.println("month"+month);
						  if(month==0)						  
							  continue;
//									  
						  double ms = Arith.div(Double.parseDouble(standardPrem)*month, 12);
						  System.out.println("ms"+ms);
					      Rate = Arith.div(Double.parseDouble(standPrem), ms)*100;
					   }
					   Rate = Arith.round(Rate, 2);
				   }
				   
				   //查询统一工号： 2014-11-21  解青青
					   ExeSQL tttExeSQL = new ExeSQL();
					  String tttGAC = "select groupagentcode from laagent where agentcode='"+tLATreeSet.get(m).getAgentCode()+"'";
					  String tttGroupAgentCode = tttExeSQL.getOneValue(tttGAC);
					  
				   //查询代理人姓名
				   String AgentInfo = "select name,employdate from laagent where agentcode='"+tLATreeSet.get(m).getAgentCode()+"'";
				   SSRS InfoSSRS = new SSRS();
				   InfoSSRS = tExeSQL.execSQL(AgentInfo);
				   if(InfoSSRS.mErrors.needDealError())
				   {
					   CError.buildErr(this, "代理人信息查询失败");
					   return false;
				   }
				 
				   String Info[] = new String[14];    					    	
			    	  Info[0] = String.valueOf(n); 
			    	  Info[1] = managSSRS.GetText(1, 1);
			    	  Info[2] = managSSRS.GetText(1, 2);
			    	  Info[3] = name;
			    	  Info[4] = branchattr;
			    	  Info[5] = InfoSSRS.GetText(1, 1);
//			    	  Info[6] = tLATreeSet.get(m).getAgentCode();
			    	  Info[6] = tttGroupAgentCode;
			    	  Info[7] = AgentPubFun.getMonthAgentGrade(tttGroupAgentCode,this.mEndMonth);
			    	  Info[8] = InfoSSRS.GetText(1,2);
			    	  Info[9] = standPrem;
			    	  Info[10] = standardPrem;
			    	  Info[11] = tProperTyprem;
			    	  Info[12] = tTransMoney;
			    	  Info[13] = String.valueOf(Rate);
				      tListTable.add(Info); 
				      n++;
			   }
		   }
       }   
       if(tListTable==null||tListTable.equals(""))
       {
    	   CError tCError = new CError();
           tCError.moduleName = "MakeXMLBL";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "没有符合条件的信息！";

           this.mErrors.addOneError(tCError);

           return false;
       }
       
    
         texttag.add("StartMonth", mStartMonth);
         texttag.add("EndMonth", mEndMonth);
         texttag.add("MakeDate", mdate); //日期
         texttag.add("MakeTime", mtime);
        if (texttag.size() > 0)
        {
            txmlexport.addTextTag(texttag);
        }
        txmlexport.addListTable(tListTable,title);
        
        mResult.clear();
        mResult.addElement(txmlexport);
        return true;
    }
   private String getName(String ComCode)
   {
	   String ComSQL = "select name from ldcom where comcode = '"+ComCode+"'";
	   ExeSQL mExeSQL = new ExeSQL();
	   String name = mExeSQL.getOneValue(ComSQL);
	   if(mExeSQL.mErrors.needDealError())
	   {
		   mErrors.copyAllErrors(mExeSQL.mErrors);
           return "";		   
	   }
	   return name;
   }
}
