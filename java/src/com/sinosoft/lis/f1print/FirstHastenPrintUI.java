package com.sinosoft.lis.f1print;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import java.io.FileOutputStream;
import java.io.InputStream;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class FirstHastenPrintUI implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    public FirstHastenPrintUI()
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        FirstHastenPrintBL tFirstHastenPrintBL = new FirstHastenPrintBL();
        System.out.println("Start FirstPayF1PUI Submit ...");

        if (!tFirstHastenPrintBL.submitData(cInputData, cOperate))
        {
            if (tFirstHastenPrintBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tFirstHastenPrintBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "FirstPayF1PBL发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tFirstHastenPrintBL.getResult();
            return true;
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "tFirstHastenPrintBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    public static void main(String[] args)
    {

    }
}
