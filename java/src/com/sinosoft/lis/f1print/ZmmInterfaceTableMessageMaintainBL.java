package com.sinosoft.lis.f1print;


//import com.sinosoft.pubfun.GlobalInput;
//import com.sinosoft.pubfun.MMap;
//import com.sinosoft.pubfun.PubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIPeriodManagementSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ZmmInterfaceTableMessageMaintainBL {
	 /**错误信息容器*/
    public CErrors mErrors = new CErrors();
    private VData mResult=new VData();;
    /** 往后面传输数据的容器 */
	private VData mInputData;
    /** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
    /**页面传输的数据*/
	private TransferData tf ;
	/** 数据操作字符串 */
	private String mOperate;
	/** 业务处理相关变量 */
	private FIPeriodManagementSchema mFIPeriodManagementSchema = new FIPeriodManagementSchema();
	
    private MMap map = new MMap();
   
    private String Serialno ="" ;
    private String Batchno = "";
	private String Chargedate = "";
	private String Costcenter = "";
	private String Readstate = "";
	private String Chinal = "";
	private String[][] mToExcel = null;
    /*private String mCurDate = PubFun.getCurrentDate();
     *private String mmResult = new String();//备用返回条件，暂不启用。*/  
   
    public ZmmInterfaceTableMessageMaintainBL()
    {
    	System.out.println("---ZmmInterfaceTableMessageMaintainBL()---");
    }

    public boolean submitData(VData InputData, String Operator) 
    {
    	// 将操作数据拷贝到本类中
        this.mInputData=InputData;
        this.mOperate = Operator;
    	
    	//获取数据
        if (!getInputData(InputData))
        {
        	System.out.println("----getInputData(InputData)---");
            return false;
        }
        //检查数据
        if (!checkData())
        {
        	System.out.println("-----checkData()-----");
        	return false;
        }
        //业务处理
        if (!dealData())
        {	
        	System.out.println("---dealData()---");
            return false;
        }
        return true;
    }
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param InputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     * @throws java.text.ParseException 
     */
    public boolean getInputData(VData data)
    {
		mGlobalInput = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
        System.out.println(mGlobalInput+"...............");
        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        System.out.println("进行校验.....");
        if (tf == null) {
            buildError("checkData", "传入参数为null！");
            return false;
        }
        if (mGlobalInput == null) {
            buildError("checkData", "登陆信息丢失,请重新登陆！");
            return false;
        }
        Batchno = (String) tf.getValueByName("Batchno");
        Serialno = (String) tf.getValueByName("Serialno");
        Costcenter = (String) tf.getValueByName("Costcenter");
        Chinal = (String) tf.getValueByName("Chinal");
		Chargedate = (String) tf.getValueByName("Chargedate");
		Readstate = (String) tf.getValueByName("Readstate");
		System.out.println("Batchno : "+Batchno);
		System.out.println("Serialno : "+Serialno);
		System.out.println("Costcenter : "+Costcenter);
		System.out.println("Chinal : "+Chinal);
		System.out.println("Chargedate : "+Chargedate);
		System.out.println("Readstate : "+Readstate);
		if (Batchno == null || Batchno.equals("")) {
            buildError("checkData", "传入批次号为空！");
            return false;
        }
        if (Readstate == null || Readstate.equals("")) {
            buildError("checkData", "传入读取状态为空！");
            return false;
        }
        return true;
    }
//Serialno,Batchno,Costcenter,Chinal,Chargedate,Readstate
    /**
     * dealData
     * 处理业务数据
     * @return boolean：true提交成功, false提交失败
     */
    public boolean dealData()
    {
    	
        if(mOperate.equals("INSERT")){
        	String sql= "INSERT INTO interfacetable (Serialno,Batchno,Costcenter,Chinal,Chargedate,Readstate) "
        			+ "VALUES('"+Serialno+"','"+Batchno+"','"+Costcenter+"','"+Chinal+"','"+Chargedate+"','"+Readstate+"')";
        	map.put(sql, "INSERT");
            }else if(mOperate.equals("DELETE")){
            	map.put("DELETE FROM interfacetable where Batchno = '"+Batchno+"' and Serialno='"+Serialno+"'", "DELETE");
        	}else if(mOperate.equals("UPDATE")){
        		if (Costcenter != null && !Costcenter.equals("") && Chinal != null && !Chinal.equals("")) {
                	map.put("UPDATE interfacetable SET Costcenter = '"+Costcenter+"',Chinal= '"+Chinal+"' where batchno = '"+Batchno+"'", "UPDATE");
                }  
            }
        
        mResult.add(map);
        System.out.println(map+"******************");       
        PubSubmit tPubSubmit = new PubSubmit();     
        if (!tPubSubmit.submitData(mResult, ""))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buildError( "gatherData", "数据出错，提示信息为：" + tPubSubmit.mErrors.getFirstError());         
            map=null;
            mResult=null;
            return false;
        }
        
    	map=null;
    	mResult=null;
        return true;
  }
    public static void main(String[] args)
    {
        /*GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "cwad";
        tG.ComCode = "86";
        TransferData tTransferData = new TransferData();
        VData vData = new VData();
        vData.add(tG);
        vData.add(tTransferData);
        PreRecPrintUI tPreRecPrintUI = new PreRecPrintUI();
        if (!tPreRecPrintUI.submitData(vData, ""))
        {
            System.out.print("失败！");
        }*/
    } 
    private void buildError(String functionName,String errorMessage){
    	CError tError = new CError();
        tError.moduleName = "GInterfaceTableMaintainBL";
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        mErrors.addOneError(tError);
    }

	public String[][] getMToExcel() {
		return mToExcel;
	}
}


