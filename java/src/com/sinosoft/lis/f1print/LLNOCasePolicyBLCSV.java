package com.sinosoft.lis.f1print; 

import java.io.InputStream;

import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.RSWrapper;

public class LLNOCasePolicyBLCSV {
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mManageCom = "";

    private String mStartDate = "";

    private String mEndDate = "";
    

    private String mOperator = "";

    private String tCurrentDate = "";

    private VData mInputData = new VData();

    private String mOperate = "";

    private PubFun mPubFun = new PubFun();

    private ListTable mListTable = new ListTable();

    private TransferData mTransferData = new TransferData();

    private XmlExport mXmlExport = null;

    private String mManageComNam = "";

    private String mFileNameB = "";

    private String mMakeDate = "";

    private String mContType = "";

    private String mContNo = "";

    private String mRgtNo = "";
    
    private String mRiskCode = "";

    private CSVFileWrite mCSVFileWrite = null;
    private String mFilePath = "";
    private String mFileName = "";
    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mOperate = cOperate;
        mInputData = (VData) cInputData;
        if (mOperate.equals("")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(mInputData)) {
            return false;
        }
        System.out.println("BBBXXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
                           + this.mManageCom);
        
    
        // 进行数据查询
        if (!queryDataToCVS()){
            return false;
        } else {

        }

        System.out.println("dayinchenggong1232121212121");

        return true;
    }

    private void bulidError(String cFunction, String cErrorMsg) {

        CError tCError = new CError();

        tCError.moduleName = "LLCasePolicy";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;

        this.mErrors.addOneError(tCError);

    }

    /**
     * 取得传入的数据 如果没有传入管理机构和起止如期则查全部机构全年的信息
     *
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        // tCurrentDate = mPubFun.getCurrentDate();
        try {
            mGlobalInput.setSchema((GlobalInput) cInputData
                                   .getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            // 页面传入的数据 三个
            mOperator = (String) mTransferData.getValueByName("tOperator");
            mFileNameB = (String) mTransferData.getValueByName("tFileNameB");
            mContType = (String) mTransferData.getValueByName("tContType");
            mContNo = (String) mTransferData.getValueByName("tContNo");
            mRgtNo = (String) mTransferData.getValueByName("RgtNo");
            mRiskCode = (String) mTransferData.getValueByName("RiskCode");      
            System.out.println("mContType:" + mContType);
            this.mManageCom = (String) mTransferData
                              .getValueByName("tManageCom");

            System.out.println("XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
                               + this.mManageCom);
            this.mStartDate = (String) mTransferData
                              .getValueByName("tStartDate");//结案起始日期
            this.mEndDate = (String) mTransferData.getValueByName("tEndDate");//结案截止日期
            
            
            if (mManageCom == null || mManageCom.equals("")) {
            	 return false;
            }
            mManageComNam=getManagecomName(mManageCom);
            
            /*if (mStartDate == null || mStartDate.equals("")) {
                this.mStartDate = getYear(tCurrentDate) + "-01-01";
            }
            if (mEndDate == null || mEndDate.equals("")) {
                this.mEndDate = getYear(tCurrentDate) + "-12-31";
            }*/

            System.out
                    .println(
                            "XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXXLLLLLLLLLLLLLLLLLLLL"
                            + mManageCom);
        } catch (Exception ex) {
            this.mErrors.addOneError("");
            return false;
        }
        return true;
    }
 
    private boolean queryDataToCVS()
    {
    	
      int datetype = -1;
      LDSysVarDB tLDSysVarDB = new LDSysVarDB();
    	tLDSysVarDB.setSysVar("LPCSVREPORT");

    	if (!tLDSysVarDB.getInfo()) {
    		buildError("queryData", "查询文件路径失败");
    		return false;
    	}
    	
    	mFilePath = tLDSysVarDB.getSysVarValue(); 
        //   	本地测试
//    	mFilePath="F:\\";
    	if (mFilePath == null || "".equals(mFilePath)) {
    		buildError("queryData", "查询文件路径失败");
    		return false;
    	}
    	System.out.println("XXXXXXXXXXXXXXXXXXXmFilePath:"+mFilePath);
    	String tTime = PubFun.getCurrentTime3().replaceAll(":", "");
    	String tDate = PubFun.getCurrentDate2();

    	mFileName = "WJAJSXBB" + mGlobalInput.Operator + tDate + tTime;
    	mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
    
    	String TGstate="";
    	if("I".equals(mContType)){
    		TGstate="个单";
    	}else if("G".equals(mContType)){
    		TGstate="团单";
    	}
    	String[][] tTitle = new String[4][];
    	tTitle[0] = new String[] { "","","","","","未结案案件明细报表" };
    	tTitle[1] = new String[] { "机构："+mManageComNam,"","团险/个险选择："+TGstate,"","保单号："+mContNo,"","批次号："+mRgtNo,"","理算起止日期："+mStartDate+"至"+mEndDate };
    	tTitle[2] = new String[] { "制表人："+mOperator,"","","","","","","","制表时间："+mPubFun.getCurrentDate() };
    	tTitle[3] = new String[] { "管理机构","投保单位","保单号码","批次号","理赔号","案件状态","客户号","客户姓名","治疗类型","理赔险种","帐单金额","赔付金额","赔付结论","处理人代码","处理人姓名","当前待处理人代码","当前待处理人姓名","当前处理时效"};
    	String[] tContentType = {"String","String","String","String","String","String","String","String",
    			"String","String","Number","Number","String","String","String","String","String","Number"};
    	if (!mCSVFileWrite.addTitle(tTitle,tContentType)) {
    		return false;
    	}
      if (!getDataListToCVS(datetype))
      {
        return false;
      }

      mCSVFileWrite.closeFile();

    	return true;
    }
    /*
     * 统计案件结果
     */
    private boolean getDataListToCVS(int datetype)
    {
    	ExeSQL tExeSQL = new ExeSQL();
    	
    	/**String tCountSQL = "select coalesce(count(a.caseno),0)"
    		             + " from llcase a,llclaimdetail b"
    		             + " where a.caseno=b.caseno "+getCondition()+" and a.mngcom like '" + mManageCom
//    		             + " and a.endcasedate between '" + mStartDate + "' and '" +
//    		             mEndDate
//    		             + "' and a.mngcom like '" + mManageCom
    		             + "%' "
    		             + getContType(mContType)
    		             + getRgtNo(mRgtNo)
    		             + getContNo(mContNo)
    		             + getRiskCode(mRiskCode)
    		             + " with ur";
    	
    	int tCount = Integer.parseInt(tExeSQL.getOneValue(tCountSQL));
    	
    	if (tCount>30000) {
    		bulidError("getDataList", "统计数据量过大，导致系统异常，请缩短统计时间，建议统计区间为一个月！");
    		return false;
    	}
    	**/

        String sql = "select "
                     + " (select name from ldcom where comcode=a.mngcom), "
                     + " (case when b.grpcontno='00000000000000000000' then b.contno else b.grpcontno end),"
                     + " b.contno,a.rgtno,a.caseno, "
                     + " codename('llrgtstate',a.rgtstate), "
                     + " a.customerno,a.customername,a.custbirthday, "
                     + " codename('sex',a.customersex), "
                     + " codename('idtype',a.idtype), "
                     + " a.idno,b.riskcode,sum(b.realpay), "
                     + " codename('llclaimdecision',b.givetype), "
                     + " a.rgtdate,a.rigister,a.endcasedate,a.handler,b.grpcontno,b.grppolno,b.polno "
                     + " ,a.CustomerAge,(select getdutyname from lmdutygetclm where getdutycode=b.getdutycode and getdutykind=b.getdutykind), "
                     + " (select min(m.accdate) from llsubreport m,llcaserela n where m.subrptno=n.subrptno and n.caseno=a.caseno), "
                     //当前案件处理人对应代码
                     + " a.dealer,"
                     //当前案件处理人对应姓名
                     + " (select username from lduser where usercode= a.dealer),"
                     //案件从受理到当前的天数，不满一天的按一天计算
                     +"  abs(days(current date)-days(a.rgtdate))+1 ,"
                     //处理人对应代码
                     + " a.handler,"
                     //处理人对应姓名
                     + " (select username from lduser where usercode= a.handler)"
                     + " from llcase a,llclaimdetail b,llclaim d "
                     + " where a.caseno=b.caseno and a.caseno=d.caseno  and a.mngcom like '" + mManageCom + "%' "
                     + " and a.endcasedate is null and b.givetype is not null "
                     + " and a.claimcaldate between '" + mStartDate + "' and '" +   mEndDate + "' "
                     + getContType(mContType)
                     + getRiskCode(mRiskCode)
                     + getRgtNo(mRgtNo)
                     + getContNo(mContNo)
                     + " group by a.mngcom,b.grpcontno,b.contno,a.rgtno, "
                     + " a.caseno,a.rgtstate,a.customerno,a.customername, "
                     + " a.custbirthday,a.customersex,a.idtype,a.idno, "
                     + " b.riskcode,b.givetype,a.rgtdate,a.rigister, "
                     + " a.endcasedate,a.handler,b.grppolno,b.polno,a.CustomerAge,b.getdutycode,b.getdutykind,a.dealer with ur ";

        System.out.println("SQL:" + sql);

        RSWrapper rsWrapper = new RSWrapper();
        if (!rsWrapper.prepareData(null, sql)) {
            System.out.println("数据准备失败! ");
            return false;
        }
     
        try {
            SSRS tSSRS = new SSRS();
            do {
                tSSRS = rsWrapper.getSSRS();
                if (tSSRS != null || tSSRS.MaxRow > 0) {
                
                    String tCaseNo = "";
                    String tContNo = "";
                    String tCustomerNo = "";
                    String tRiskCode = "";
                    String tContent[][] = new String[tSSRS.getMaxRow()][];
          
                    for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                        String ListInfo[] = new String[18];
         
                        for (int m = 0; m < ListInfo.length; m++) {
                            ListInfo[m] = "";
                        }
                        tCaseNo = tSSRS.GetText(i, 5);
                        System.out.println(tCaseNo);
                        tCustomerNo = tSSRS.GetText(i, 7);
                        tContNo = tSSRS.GetText(i, 3);
                        tRiskCode = tSSRS.GetText(i, 13);

                        //管理机构
                        ListInfo[0] = tSSRS.GetText(i, 1);
             
                        //保单号码
                        ListInfo[2] = tSSRS.GetText(i, 2);
                        
                        //获取团体保单号
                        String grpcontno=tSSRS.GetText(i, 20);
                        //团体险种号
                        String grppolno= tSSRS.GetText(i, 21);
                        String tPolNo = tSSRS.GetText(i, 22);
                    
             
                        //批次号
                        ListInfo[3] = tSSRS.GetText(i, 4);
                        //理赔号
                        ListInfo[4] = tSSRS.GetText(i, 5);
                        //案件状态
                        ListInfo[5] = tSSRS.GetText(i, 6);
                        //客户号
                        ListInfo[6] = tSSRS.GetText(i, 7);
                        //客户姓名
                        ListInfo[7] = tSSRS.GetText(i, 8);


                        //理赔险种
                        ListInfo[9] = tSSRS.GetText(i, 13);
  
                        //赔付金额
                        ListInfo[11] = tSSRS.GetText(i, 14);
                        //赔付结论
                        ListInfo[12] = tSSRS.GetText(i, 15);
                        //处理人代码
                        ListInfo[13] = tSSRS.GetText(i, 29);
                        //处理人姓名
                        ListInfo[14] = tSSRS.GetText(i, 30);
                        //当前待处理人代码
                        ListInfo[15] = tSSRS.GetText(i, 26);
                        //当前待处理人姓名
                        ListInfo[16] = tSSRS.GetText(i, 27);
                        //当前处理时效
                        ListInfo[17] = tSSRS.GetText(i, 28);
                    	
                        String tInsuredSQL = "select a.appntname,a.appntno,"
                                             + " b.occupationcode,"
                                             + " (select occupationname from ldoccupation where occupationcode=b.occupationcode fetch first 1 rows only),"
                                             + " a.amnt,a.prem,"
                                             +
                                             " codename('payintv',char(a.payintv)),"
                                             + " a.cvalidate,a.enddate,"
                                             +
                                             " (select codealias from ldcode1 where codetype='salechnl' and code=a.salechnl fetch first 1 rows only), "
                                             + " codename('uwflag',a.uwflag),"
                                             + " getUniteCode(a.agentcode), "
                                             + " (select Name from laagent where agentcode=a.agentcode fetch first 1 rows only), "
                                             + " (select branchattr from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), "
                                             + " (select Name from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), "
                                             + " a.AgentCom, "
                                             +
                                             " (select name from lacom where AgentCom=a.AgentCom), " +
                                             " a.occupationtype,(select codename from ldcode where codetype='occupationtype' and code=a.occupationtype) "

                                             + " from lcpol a,lcinsured b "
                                             +
                                             " where a.insuredno=b.insuredno and a.contno=b.contno "
                                             +
                                             " and a.insuredno='" + tCustomerNo +
                                             "' and a.contno='" + tContNo +
                                             "' and riskcode='" + tRiskCode +
                                             "' union all "
                                             + " select a.appntname,a.appntno,"
                                             + " b.occupationcode,"
                                             + " (select occupationname from ldoccupation where occupationcode=b.occupationcode fetch first 1 rows only),"
                                             + " a.amnt,a.prem,"
                                             +
                                             " codename('payintv',char(a.payintv)),"
                                             + " a.cvalidate,a.enddate,"
                                             +
                                             " (select codealias from ldcode1 where codetype='salechnl' and code=a.salechnl fetch first 1 rows only), "
                                             + " codename('uwflag',a.uwflag),"
                                             + " getUniteCode(a.agentcode), "
                                             + " (select Name from laagent where agentcode=a.agentcode fetch first 1 rows only), "
                                             + " (select branchattr from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), "
                                             + " (select Name from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), "
                                             + " a.AgentCom, "
                                             +
                                             " (select name from lacom where AgentCom=a.AgentCom), "
                                             +" a.occupationtype,(select codename from ldcode where codetype='occupationtype' and code=a.occupationtype) "
                                             + " from lbpol a,lcinsured b "
                                             +
                                             " where a.insuredno=b.insuredno and a.contno=b.contno "
                                             +
                                             " and a.insuredno='" + tCustomerNo +
                                             "' and a.contno='" + tContNo +
                                             "' and riskcode='" + tRiskCode +
                                             "' union all "
                                             + " select a.appntname,a.appntno,"
                                             + " b.occupationcode,"
                                             + " (select occupationname from ldoccupation where occupationcode=b.occupationcode fetch first 1 rows only),"
                                             + " a.amnt,a.prem,"
                                             +
                                             " codename('payintv',char(a.payintv)),"
                                             + " a.cvalidate,a.enddate,"
                                             +
                                             " (select codealias from ldcode1 where codetype='salechnl' and code=a.salechnl fetch first 1 rows only), "
                                             + " codename('uwflag',a.uwflag),"
                                             + " getUniteCode(a.agentcode), "
                                             + " (select Name from laagent where agentcode=a.agentcode fetch first 1 rows only), "
                                             + " (select branchattr from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), "
                                             + " (select Name from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), "
                                             + " a.AgentCom, "
                                             +
                                             " (select name from lacom where AgentCom=a.AgentCom), "
                                             +" a.occupationtype,(select codename from ldcode where codetype='occupationtype' and code=a.occupationtype) "
                                             + " from lbpol a,lbinsured b "
                                             +
                                             " where a.insuredno=b.insuredno and a.contno=b.contno "
                                             +
                                             " and a.insuredno='" + tCustomerNo +
                                             "' and a.contno='" + tContNo +
                                             "' and riskcode='" + tRiskCode +
                                             "' fetch first 1 rows only"
                                             + " with ur";

                        System.out.println(tInsuredSQL);
                        SSRS tInsuredSSRS = tExeSQL.execSQL(tInsuredSQL);
                        if (tInsuredSSRS.getMaxRow() > 0) {
                            //投保单位
                            ListInfo[1] = tInsuredSSRS.GetText(1, 1);
                        
                        }
                        String tFeeSQL =
                                "select codename('insustat',a.insuredstat), "
                                + " codename('llfeetype',a.feetype),"
                                +
                                " sum(a.sumfee) OVER (PARTITION BY a.caseno),"
                                + " '',"
                                + " a.hospitalcode,a.hospitalname,"
                                + " codename('hospitalclass',a.hosgrade),"
                                + " a.hospstartdate,a.hospenddate,"
                                +
                                " sum(realhospdate) OVER (PARTITION BY a.caseno),"
                                + " (select codename('llhospiflag',associateclass) from ldhospital where hospitcode=a.hospitalcode fetch first 1 rows only)"
                                + " from llfeemain a,llcaserela b "
                                + " where a.caseno=b.caseno and a.caserelano=b.caserelano "
                                + " and b.caseno='" + tCaseNo
                                + "' order by a.makedate "
                                + " fetch first 1 rows only "
                                + " with ur";
                        System.out.println(tFeeSQL);

                        SSRS tFeeSSRS = tExeSQL.execSQL(tFeeSQL);

                        if (tFeeSSRS.getMaxRow() > 0) {
                      
                            //治疗类别
                            ListInfo[8] = tFeeSSRS.GetText(1, 2);
                            //帐单金额
                            ListInfo[10] = tFeeSSRS.GetText(1, 3);
                            
                           
                        }
                 
         //  mListTable.add(ListInfo);
           tContent[i - 1] = ListInfo;
       }
       mCSVFileWrite.addContent(tContent);
       if (!mCSVFileWrite.writeFile()) {
			mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
			return false;
		}
   }else{
	String tContent[][] = new String[1][];
   String ListInfo[]={"","","","","","","","","","","","","","","","","",""};	
   ListInfo[3]="0";
   ListInfo[4]="0";
   ListInfo[1]="0";
   ListInfo[2]="0";
   ListInfo[5]="0";
   ListInfo[6]="0";
   ListInfo[7]="0";
   ListInfo[8]="0";
   ListInfo[0]="0";
   ListInfo[9]="0";
   ListInfo[10]="0";
   ListInfo[11]="0";
   ListInfo[12]="0";
   ListInfo[13]="0";
   ListInfo[14]="0";
   ListInfo[15]="0";
   ListInfo[16]="0";
   ListInfo[17]="0";
   tContent[0]=ListInfo;
   mCSVFileWrite.addContent(tContent);
   if (!mCSVFileWrite.writeFile()) {
		mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
		return false;
	}

   }
} while (tSSRS != null && tSSRS.MaxRow > 0);
} catch (Exception ex) {

} finally {
rsWrapper.close();
}
return true;
      }
 

    private String getYear(String pmDate) {
        String mYear = "";
        String tSQL = "";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        mYear = tSSRS.GetText(1, 1);
        return mYear;
    }

  
    /**
     * 获取险种类型查询条件
     * @param aContType String
     * @return String
     */
    private String getContType(String aContType) {
        if (!"".equals(aContType)) {
            String tContTypeSQL =
                    " and exists (select 1 from lmriskapp where riskcode=b.riskcode and riskprop='" +
                    aContType + "' ) ";
            return tContTypeSQL;
        } else {
            return "";
        }
    }

    /**
     * 获取批次号查询条件
     * @param aRgtNo String
     * @return String
     */
    private String getRgtNo(String aRgtNo) {
        if (!"".equals(aRgtNo)) {
            String tRgtSQL = " and a.rgtno='" + aRgtNo + "'";
            return tRgtSQL;
        } else {
            return "";
        }
    }

    /**
     * 获取保单号查询条件
     * @param aContNo String
     * @return String
     */
    private String getContNo(String aContNo) {
        if (!"".equals(aContNo)) {
            String tContSQL = " and (b.grpcontno='" + aContNo +
                              "' or b.contno='" + aContNo + "')";
            return tContSQL;
        } else {
            return "";
        }

    }
    
    /**
     * 获取险种查询条件
     * @param aRgtNo String
     * @return String
     */
    private String getRiskCode(String aRiskCode) {
        if (!"".equals(aRiskCode)) {
            String tRiskCodeSQL = " and b.riskcode='" + aRiskCode + "'";
            return tRiskCodeSQL;
        } else {
            return "";
        }
    }

    /**
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }
	public String getMFilePath() {
		return mFilePath;
	}

	public void setMFilePath(String filePath) {
		mFilePath = filePath;
	}

	public String getMFileName() {
		return mFileName;
	}

	public void setMFileName(String fileName) {
		mFileName = fileName;
	}
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLCasePolicyBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}
    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream() {
        return mXmlExport.getInputStream();
    }
	public String getManagecomName(String com) {
		String sq="select name from ldcom where comcode='"+com+"' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tGrpTypeSSRS = tExeSQL.execSQL(sq);
		return tGrpTypeSSRS.GetText(1, 1);
	}
}
