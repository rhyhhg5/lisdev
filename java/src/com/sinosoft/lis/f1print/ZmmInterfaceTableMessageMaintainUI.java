package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;




public class ZmmInterfaceTableMessageMaintainUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    //业务处理相关变量
    public ZmmInterfaceTableMessageMaintainUI()
    {
    }
    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData InputData, String Operator)
    {
    	System.out.println("----ZmmInterfaceTableMessageMaintainUI----");
    	ZmmInterfaceTableMessageMaintainBL bl = new ZmmInterfaceTableMessageMaintainBL();
        if(!bl.submitData(InputData, Operator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }
        System.out.println("---ZmmInterfaceTableMessageMaintainUI方法执行完了---");
        return true;
    }
    public static void main(String[] args)
    {

    }
}

