package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.f1print.LAGrpRiskWageBL;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author weili
 * @version 1.0
 */
public class LAGrpRiskWage{

    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();


    public LAGrpRiskWage() {
    }

    public boolean submitData(VData cInputData, String cOperate) {

        LAGrpRiskWageBL tLAGrpRiskWageBL = new LAGrpRiskWageBL();

        if (!tLAGrpRiskWageBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tLAGrpRiskWageBL.mErrors);

            return false;
        } else {
            this.mResult = tLAGrpRiskWageBL.getResult();
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }


    public static void main(String[] args) {

    }
}
