package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.util.Vector;

public class NewGrpPayColPrtBL
     implements PrintService{
 /** 错误处理类，每个需要错误处理的类中都放置该类 */
 public CErrors mErrors = new CErrors();

 private VData mResult = new VData();

//打印次数或打印方式（即时打印or批量打印）
 private String mdirect = "";
 //案件批次号
 private String mRgtNo = "";
 //外部传入的操作符
 private String mflag = null;
 //打印的总次数
 private int mPrintTimes = 0;
 //打印管理类
 private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

 //业务处理相关变量
  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();
  private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema(); //立案/申请登记总表
  public NewGrpPayColPrtBL()
    {
    }
  public boolean submitData(VData cInputData, String cOperate) {

      mflag = cOperate;

   // 得到外部传入的数据，将数据备份到本类中
   if (!getInputData(cInputData)) {
     return false;
   }
   mResult.clear();

   // 准备所有要打印的数据
   if (!getPrintData()) {
     return false;
   }
   return true;
 }

 private void buildError(String Fun, String Msg) {
   CError cError = new CError();
   cError.moduleName = "LCContF1PBL";
   cError.functionName = Fun;
   cError.errorMessage = Msg;
   this.mErrors.addOneError(cError);
 }
 
 /**
  * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
  */
 private boolean getInputData(VData cInputData) {
   //全局变量
     LOBatchPRTManagerSchema prtSchema = (LOBatchPRTManagerSchema)
         cInputData.getObjectByObjectName("LOBatchPRTManagerSchema", 0);
   mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",
       0);
   LOPRTManagerSchema tLOPRTManagerSchema = (LOPRTManagerSchema)
   cInputData.
   getObjectByObjectName("LOPRTManagerSchema", 0);
   if (tLOPRTManagerSchema.getOtherNo() != null) {
       mRgtNo = tLOPRTManagerSchema.getOtherNo();
       mdirect += tLOPRTManagerSchema.getStandbyFlag3();
       mPrintTimes = (int) tLOPRTManagerSchema.getPrintTimes();
   } else if (prtSchema != null && prtSchema.getOtherNo() != null) {
       mRgtNo = prtSchema.getOtherNo();
   }else {
       buildError("getInputData", "没有指定打印数据！");
       return false;
   }
   return true;
 }

 public VData getResult() {
    return mResult;
  }

  public CErrors getErrors() {
    return mErrors;
  }

  private boolean getPrintData() {
      
      //打印前数据准备
      if (mRgtNo == null || "".equals(mRgtNo) || "null".equals(mRgtNo)) {
          buildError("getPrintData", "批次号不能为空！");
          return false;
      }
      LLRegisterDB mLLRegisterDB = new LLRegisterDB();
      mLLRegisterDB.setRgtNo(mRgtNo);

      if (mLLRegisterDB.getInfo() == false)
        {
            mErrors.copyAllErrors(mLLRegisterDB.mErrors);
            buildError("outputXML", "请先保存该团体批次信息！");
            return false;
        }

      String strSQL = "select Username from lduser where usercode='" + mGlobalInput.Operator + "'";
      ExeSQL exesql1 = new ExeSQL();
      String OperName = exesql1.getOneValue(strSQL);
      strSQL = "select Username from lduser where usercode='" + mLLRegisterDB.getOperator() + "'";
      String CommitName = exesql1.getOneValue(strSQL);

      //生成PDF打印必须的XML
      TextTag texttag = new TextTag();
      XmlExport xmlexport = new XmlExport();
      xmlexport.createDocument("NewGrpPayAppPrt", "Printer");
      //生成-年-月-日格式的日期
      StrTool tSrtTool = new StrTool();
      String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
          tSrtTool.getDay() + "日";
        String sqlusercom = "select comcode from lduser where usercode='" +
              mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        texttag.add("JetFormType", "lp016");
        if (comcode.equals("86") || comcode.equals("8600") ||
        comcode.equals("86000000")) {
        comcode = "86";
        } else if (comcode.length() >= 4) {
        comcode = comcode.substring(0, 4);
        } else {
        buildError("getInputData", "操作员机构查询出错！");
        return false;
        }
        String printcom =
        "select codename from ldcode where codetype='pdfprintcom' and code='" +
        comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);
        
        texttag.add("ManageComLength4", printcode);
        texttag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        if (mflag.equals("batch")) {
            texttag.add("previewflag", "0");
        } else {
            texttag.add("previewflag", "1");
        }
      //GrpPayColPrtBL模版元素
      texttag.add("BarCode1", mLLRegisterDB.getRgtNo());
      texttag.add("BarCodeParam1"
                  , "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
      texttag.add("GrpName",mLLRegisterDB.getGrpName());            //投保单位名称
      texttag.add("RgtObjNo",mLLRegisterDB.getRgtObjNo());     //团体号码
      texttag.add("RgtantName",mLLRegisterDB.getRgtantName()); //联系人
      texttag.add("PostCode",mLLRegisterDB.getPostCode());     //邮政编码
      texttag.add("RgtantPhone", mLLRegisterDB.getRgtantPhone());      //投保单位联系电话
      texttag.add("RgtantAddress",mLLRegisterDB.getRgtantAddress());   //投保单位地址
      texttag.add("Handler1Phone",mLLRegisterDB.getHandler1Phone());   //提交人联系电话
      texttag.add("Operator", CommitName);            //提交人
      texttag.add("AppPeoples",mLLRegisterDB.getAppPeoples());         //申请人数
      texttag.add("Remark",mLLRegisterDB.getRemark());                 //备注
      texttag.add("Handler",OperName);            //经办人
      texttag.add("date",SysDate);     //日期

      if (texttag.size() > 0) {
        xmlexport.addTextTag(texttag);
      }
      //保存信息
      xmlexport.outputDocumentToFile("E:\\", "testHZMHYD"); //输出xml文档到文件
      
      String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
      String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
      mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
      mLOPRTManagerSchema.setOtherNo(mRgtNo);
      mLOPRTManagerSchema.setOtherNoType("12");
      mLOPRTManagerSchema.setCode("lp016");
      mLOPRTManagerSchema.setManageCom(this.mGlobalInput.ManageCom);
      mLOPRTManagerSchema.setAgentCode("");
      mLOPRTManagerSchema.setReqCom(this.mGlobalInput.ManageCom);
      mLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
      mLOPRTManagerSchema.setPrtType("0");
      mLOPRTManagerSchema.setStateFlag("0");
      mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
      mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
      mLOPRTManagerSchema.setStandbyFlag3("1");
      
      mResult.clear();
      mResult.addElement(mLOPRTManagerSchema);
      mResult.addElement(xmlexport);

      return true;

}


  public static void main(String[] args) {

   NewGrpPayColPrtBL tGrpPayColPrtBL = new NewGrpPayColPrtBL();
   VData tVData = new VData();
   GlobalInput tGlobalInput = new GlobalInput();
   tGlobalInput.ManageCom = "86";
   tGlobalInput.Operator = "001";
   tVData.addElement(tGlobalInput);
   LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
   tLOPRTManagerSchema.setOtherNo("P3500131121000002");
   tVData.addElement(tLOPRTManagerSchema);
   tGrpPayColPrtBL.submitData(tVData, "PRINT");
   VData vdata = tGrpPayColPrtBL.getResult();
   System.out.println(vdata);
   System.out.println("成功打印");
 }


}
