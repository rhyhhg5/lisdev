package com.sinosoft.lis.f1print;

//程序名称：PrtContContinueBL.java
//程序功能：个险保单继续率统计,查询保单继续率汇总,下载清单。
//创建日期：2011-4-18 
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BonusNoticeQueryBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private CreateExcelList mCreateExcelList = new CreateExcelList("");

	private TransferData mTransferData = new TransferData();

	private GlobalInput mGlobalInput = new GlobalInput();

	// 对比起始日期
	private String mStartDate = "";

	private String mEndDate = "";

	private String mManageCom = "";

	private String mQueryType = "";

	private String mGetType = "";

	public BonusNoticeQueryBL() {
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到公共信息！");
			return false;
		}
		mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mStartDate = (String) mTransferData.getValueByName("StartDate");
		mEndDate = (String) mTransferData.getValueByName("EndDate");
		mManageCom = (String) mTransferData.getValueByName("ManageCom");
		mQueryType = (String) mTransferData.getValueByName("queryType");
		if (mStartDate.equals("") || mEndDate.equals("") || mStartDate == null || mEndDate == null) {
			buildError("getInputData", "没有得到足够的信息:起始和终止日期不能为空！");
			return false;
		}
		if (mGetType.equals("") || mGetType == null) {
			buildError("getInputData", "没有得到足够的信息:红利派发情况不能为空！");
			return false;
		}
		return true;
	}

	/**
	 * 传输数据的公共方法
	 */
	public CreateExcelList getsubmitData(VData cInputData, String cOperate) {
		mGetType = cOperate;

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return null;
		}

		if (mGetType.equals("1")) // 1-尚未派发,2-已派发
		{
			if (!getBeforeBonusPrintData()) {
				return null;
			}
		}
		if (mGetType.equals("2")) // 1-尚未派发,2-已派发
		{
			if (!getAfterBonusPrintData()) {
				return null;
			}
		}

		if (mCreateExcelList == null) {
			buildError("submitData", "Excel数据为空");
			return null;
		}
		return mCreateExcelList;
	}

	public static void main(String[] args) {
		BonusNoticeQueryBL tbl = new BonusNoticeQueryBL();
		GlobalInput tGlobalInput = new GlobalInput();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("StartDate", "2009-01-23"); // 生效日期
		tTransferData.setNameAndValue("EndDate", "2009-02-23");
		tTransferData.setNameAndValue("ManageCom", "8691");
		tTransferData.setNameAndValue("ContType", "0"); // 0|全部^1|个险^2|银保
		tTransferData.setNameAndValue("SaleChnl", "00"); // 渠道 00-全部
		tTransferData.setNameAndValue("ContinueType", "2"); // 1-13个月，2-25个月
		tTransferData.setNameAndValue("Orphans", "0"); // 保单服务状态, 1-在职单 2-孤儿单
														// 0-全部
		tTransferData.setNameAndValue("RiskType", "2"); // 1-传统险,2-万能
		tGlobalInput.ManageCom = "86";
		tGlobalInput.Operator = "zgm";
		VData tData = new VData();
		tData.add(tGlobalInput);
		tData.add(tTransferData);

		CreateExcelList tCreateExcelList = new CreateExcelList();
		tCreateExcelList = tbl.getsubmitData(tData, "1");
		if (tCreateExcelList == null) {
			System.out.println("112321231");
		} else {
			try {
				tCreateExcelList.write("c:\\contactcompare.xls");
			} catch (Exception e) {
				System.out.println("EXCEL生成失败！");
			}
		}
	}

	public VData getResult() {
		return mResult;
	}

	public CErrors getErrors() {
		return mErrors;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "PrtContContinueBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private boolean getBeforeBonusPrintData() {
		// 创建EXCEL列表
		mCreateExcelList.createExcelFile();
		String[] sheetName = { "list" };
		mCreateExcelList.addSheet(sheetName);

		// 设置表头
		String[][] tTitle = { { "管理机构","营销部门", "保单号", "投保人姓名", "被保人姓名",
			 "投保人联系地址","投保人家庭电话","投保人手机","保费金额","保单生效日",
			 "代理人编码","代理人姓名","代理人固定电话","代理人手机","红利应派发日","红利领取方式","保单状态" } };

		// 表头的显示属性
		int[] displayTitle = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ,16,17 };
		// 数据的显示属性
		int[] displayData = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ,16,17 };

		int row = mCreateExcelList.setData(tTitle, displayTitle);
		if (row == -1) {
			buildError("getPrintData", "EXCEL中指定数据失败！");
			return false;
		}

		// "管理机构","营销部门", "保单号", "投保人姓名", "被保人姓名",
		// "投保人联系地址","投保人家庭电话","投保人手机","保费金额","保单生效日",
		// "代理人编码","代理人姓名","代理人固定电话","代理人手机","红利应派发日","红利领取方式"

		//优化sql 
		ExeSQL tExeSQL = new ExeSQL();
		String sql = "select riskcode from lmriskapp where risktype4='2'";
		SSRS riskSSRS = tExeSQL.execSQL(sql);
		StringBuffer riskStr = new StringBuffer();
		for (int i = 1; i <= riskSSRS.getMaxRow(); i++) {
			riskStr.append("'"+riskSSRS.GetText(i, 1)+"',");
		}
		String newSql = riskStr.toString().substring(0,riskStr.toString().length()-1);
		
		// 获得EXCEL列信息
		StringBuffer tSQL = new StringBuffer();
		tSQL.append("select ShowManageName(a.ManageCom) mana,(select (select name from LABranchGroup z where z.agentgroup = subStr(x.branchseries,1,12)  ) from LABranchGroup x , LAAgent y where x.agentgroup = y.agentgroup and y.agentcode = a.AgentCode) agen,")
		.append("a.contno,a.AppntName,a.insuredname, ")
		.append("b.Postaladdress,(case when b.phone is null then b.homephone else b.phone end), b.mobile, ")
		.append("a.prem,a.cvalidate , ")
		.append("getUniteCode(a.AgentCode), c.Name,c.Phone,c.Mobile, ")
		.append("(a.cvalidate + 1 year), ")
		.append("(select codename from ldcode where codetype='bonusgetmode' and code=a.bonusgetmode), ")
		.append("(select db2inst1.codename('stateflag',a.StateFlag) from dual)")
		.append("from LCPol a,LCAddress b,laagent c where 1=1 ")
		.append("and b.AddressNo = (select AddressNo from LCAppnt where ContNo = a.ContNo) ")
		.append("and a.appntno=b.customerno ")
		.append("and a.agentcode=c.agentcode ")
		.append("and a.conttype='1' ")
		.append("and a.appflag='1' ")
		.append("and a.stateflag='1' ")
		.append("and a.riskcode in (").append(newSql).append(") ")
		.append("and a.bonusgetmode='").append(mQueryType).append("' ")
		.append("and a.managecom like '").append(mManageCom).append("%' ")
		.append("and not exists (select 1 from lobonuspol where polno=a.polno) ")
		.append("and (a.cvalidate + 1 year) ")
		.append(" between '").append(mStartDate).append("' and '").append(mEndDate).append("' ")
		.append(" union all ")
		.append("select ShowManageName(a.ManageCom) mana,(select (select name from LABranchGroup z where z.agentgroup = subStr(x.branchseries,1,12)  ) from LABranchGroup x , LAAgent y where x.agentgroup = y.agentgroup and y.agentcode = a.AgentCode) agen,")
		.append("a.contno,a.AppntName,a.insuredname, ")
		.append("b.Postaladdress,(case when b.phone is null then b.homephone else b.phone end), b.mobile, ")
		.append("a.prem,a.cvalidate , ")
		.append("getUniteCode(a.AgentCode), c.Name,c.Phone,c.Mobile, ")
		.append("(select (max(sgetdate)+1 year) from lobonuspol where polno=a.polno), ")
		.append("(select codename from ldcode where codetype='bonusgetmode' and code=a.bonusgetmode), ")
		.append("(select db2inst1.codename('stateflag',a.StateFlag) from dual)")
		.append("from LCPol a,LCAddress b,laagent c where 1=1 ")
		.append("and b.AddressNo = (select AddressNo from LCAppnt where ContNo = a.ContNo) ")
		.append("and a.appntno=b.customerno ")
		.append("and a.agentcode=c.agentcode ")
		.append("and a.conttype='1' ")
		.append("and a.appflag='1' ")
		.append("and a.stateflag='1' ")
		.append("and a.riskcode in (").append(newSql).append(") ")
		.append("and a.bonusgetmode='").append(mQueryType).append("' ")
		.append("and a.managecom like '").append(mManageCom).append("%' ")
		.append("and exists (select 1 from lobonuspol where polno=a.polno) ")
		.append("and (select (max(sgetdate)+1 year) from lobonuspol where polno=a.polno) ")
		.append(" between '").append(mStartDate).append("' and '").append(mEndDate).append("' ")
		.append(" with ur ");

		System.out.println("查询sql：" + tSQL.toString());
		
		SSRS tSSRS = tExeSQL.execSQL(tSQL.toString());
		if (tExeSQL.mErrors.needDealError()) {
			CError tError = new CError();
			tError.moduleName = "PrtContContinueBL";
			tError.functionName = "getPrintData";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			return false;
		}

		String[][] tGetData = null;
		tGetData = tSSRS.getAllData();

		if (tGetData == null) {
			CError tError = new CError();
			tError.moduleName = "PrtContContinueBL";
			tError.functionName = "getPrintData";
			tError.errorMessage = "没有查询到需要输出的数据";
			return false;
		}

		System.out.println("拢共有这些条：" + tGetData.length);

		if (mCreateExcelList.setData(tGetData, displayData) == -1) {
			buildError("getPrintData", "EXCEL中设置数据失败！");
			return false;
		}

		return true;
	}

	private boolean getAfterBonusPrintData() {
		// 创建EXCEL列表
		mCreateExcelList.createExcelFile();
		String[] sheetName = { "list" };
		mCreateExcelList.addSheet(sheetName);

		// 设置表头
		String[][] tTitle = { { "管理机构","营销部门", "保单号", "投保人姓名", "被保人姓名",
			 "投保人联系地址","投保人家庭电话","投保人手机","保费金额","保单生效日",
			 "代理人编码","代理人姓名","代理人固定电话","代理人手机","红利应派发日","红利领取方式","已派发金额","实际分红日期","保单状态" } };

		// 表头的显示属性
		int[] displayTitle = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ,16,17,18,19 };
		// 数据的显示属性
		int[] displayData = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ,16,17,18,19 };

		int row = mCreateExcelList.setData(tTitle, displayTitle);
		if (row == -1) {
			buildError("getPrintData", "EXCEL中指定数据失败！");
			return false;
		}

		// "管理机构","营销部门", "保单号", "投保人姓名", "被保人姓名",
		// "投保人联系地址","投保人家庭电话","投保人手机","保费金额","保单生效日",
		// "代理人编码","代理人姓名","代理人固定电话","代理人手机","红利应派发日","红利领取方式","已派发金额","实际分红日期"

		//优化sql
		ExeSQL tExeSQL = new ExeSQL();
		String sql = "select riskcode from lmriskapp where risktype4='2'";
		SSRS riskSSRS = tExeSQL.execSQL(sql);
		StringBuffer riskStr = new StringBuffer();
		for (int i = 1; i <= riskSSRS.getMaxRow(); i++) {
			riskStr.append("'"+riskSSRS.GetText(i, 1)+"',");
		}
		String newSql = riskStr.toString().substring(0,riskStr.toString().length()-1);
		
		// 获得EXCEL列信息
		StringBuffer tSQL = new StringBuffer();
		tSQL.append("select ShowManageName(a.ManageCom) mana,(select (select name from LABranchGroup z where z.agentgroup = subStr(x.branchseries,1,12)  ) from LABranchGroup x , LAAgent y where x.agentgroup = y.agentgroup and y.agentcode = a.AgentCode) agen,")
		.append("a.contno,a.AppntName,a.insuredname, ")
		.append("b.Postaladdress,(case when b.phone is null then b.homephone else b.phone end), b.mobile, ")
		.append("a.prem,a.cvalidate , ")
		.append("getUniteCode(a.AgentCode), c.Name,c.Phone,c.Mobile, ")
		.append("d.sgetdate, ")
		.append("(select codename from ldcode where codetype='bonusgetmode' and code=d.bonusflag), ")
		.append("d.bonusmoney,d.agetdate, ")
		.append("(select db2inst1.codename('stateflag',a.StateFlag) from dual)")
		.append("from LCPol a,LCAddress b,laagent c,lobonuspol d where 1=1 ")
		.append("and b.AddressNo = (select AddressNo from LCAppnt where ContNo = a.ContNo) ")
		.append("and a.appntno=b.customerno ")
		.append("and a.agentcode=c.agentcode ")
		.append("and a.polno=d.polno ")
		.append("and a.conttype='1' ")
		.append("and a.appflag='1' ")
		.append("and a.stateflag='1' ")
		.append("and a.riskcode in (").append(newSql).append(") ")
		.append("and d.bonusflag='").append(mQueryType).append("' ")
		.append("and a.managecom like '").append(mManageCom).append("%' ")
		.append("and d.sgetdate between '").append(mStartDate).append("' and '").append(mEndDate).append("' ")
		.append(" union ")
		.append("select ShowManageName(a.ManageCom) mana,(select (select name from LABranchGroup z where z.agentgroup = subStr(x.branchseries,1,12)  ) from LABranchGroup x , LAAgent y where x.agentgroup = y.agentgroup and y.agentcode = a.AgentCode) agen,")
		.append("a.contno,a.AppntName,a.insuredname, ")
		.append("b.Postaladdress,(case when b.phone is null then b.homephone else b.phone end), b.mobile, ")
		.append("a.prem,a.cvalidate , ")
		.append("getUniteCode(a.AgentCode), c.Name,c.Phone,c.Mobile, ")
		.append("d.sgetdate, ")
		.append("(select codename from ldcode where codetype='bonusgetmode' and code=d.bonusflag), ")
		.append("d.bonusmoney,d.agetdate, ")
		.append("(select db2inst1.codename('stateflag',a.StateFlag) from dual)")
		.append("from LBPol a,LCAddress b,laagent c,lobonuspol d where 1=1 ")
		.append("and b.AddressNo = (select AddressNo from LBAppnt where ContNo = a.ContNo) ")
		.append("and a.appntno=b.customerno ")
		.append("and a.agentcode=c.agentcode ")
		.append("and a.polno=d.polno ")
		.append("and a.conttype='1' ")
		.append("and a.appflag='1' ")
		.append("and a.riskcode in (").append(newSql).append(") ")
		.append("and d.bonusflag='").append(mQueryType).append("' ")
		.append("and a.managecom like '").append(mManageCom).append("%' ")
		.append("and d.sgetdate between '").append(mStartDate).append("' and '").append(mEndDate).append("' ")
		.append(" with ur ");

		System.out.println("查询sql：" + tSQL.toString());
		SSRS tSSRS = tExeSQL.execSQL(tSQL.toString());
		if (tExeSQL.mErrors.needDealError()) {
			CError tError = new CError();
			tError.moduleName = "PrtContContinueBL";
			tError.functionName = "getPrintData";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			return false;
		}

		String[][] tGetData = null;
		tGetData = tSSRS.getAllData();

		if (tGetData == null) {
			CError tError = new CError();
			tError.moduleName = "PrtContContinueBL";
			tError.functionName = "getPrintData";
			tError.errorMessage = "没有查询到需要输出的数据";
			return false;
		}

		System.out.println("拢共有这些条：" + tGetData.length);

		if (mCreateExcelList.setData(tGetData, displayData) == -1) {
			buildError("getPrintData", "EXCEL中设置数据失败！");
			return false;
		}

		return true;
	}
}
