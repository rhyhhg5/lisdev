package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;

public class LAOrphanRiskReportBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //取得的代理人编码
    private String mAgentCode = "";

//取得的机构外部代码 
    private String mBranchAttr = "";

//取得的代理人离职日期
    private String mOutWorkDate = "";

//取得的代理人姓名
    private String mName = "";

//取得的保单号
    private String mContNo = "";

//取得的管理机构代码
    private String mManageCom = "";
//取得当前日期
    private String mdate = "";
 //取得当期时间
    private String mtime = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
//    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
//    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
//    private LDComSchema mLDComSchema = new LDComSchema();

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
    	 mGlobalInput.setSchema((GlobalInput) cInputData.
                 getObjectByObjectName("GlobalInput", 0));
    	mManageCom = (String)cInputData.get(0);
    	mBranchAttr = (String)cInputData.get(1);
    	mAgentCode = (String)cInputData.get(2);
    	mName = (String)cInputData.get(3);
    	mOutWorkDate = (String)cInputData.get(4);
    	mContNo = (String)cInputData.get(5);
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {       
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
        txmlexport.createDocument("LAOrphanRisk.vts", "printer"); //最好紧接着就初始化xml文档
        ListTable tListTable = new ListTable();
        tListTable.setName("LAOrphanRisk");
        String[] title = {"", "", "", "", "" , "" , "" , "" , "" , "" ,"","","","","","" };
       
    	String tAgentGroup = "";
       //前台传入的参数为外部编码，因此需要做转换
       if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
       {
        String GroupSQL = "select agentgroup from labranchgroup where branchtype='1' and branchattr='"+this.mBranchAttr+"'";  
    	ExeSQL tExeSQL = new ExeSQL();
    	tAgentGroup = tExeSQL.getOneValue(GroupSQL);
       }
       //查询所有的记录
       msql = "select c.contno,e.cvalidate ,getUniteCode(b.agentcode),b.name,b.managecom,d.branchattr,e.appntname , "
    	   +"(select f.mobile from lcaddress f where f.customerno = e.appntno  and  f.addressno=(select addressno from lcappnt where lcappnt.contno=e.contno)),"
    	   +"(select g.postaladdress from lcaddress g where g.customerno = e.appntno and g.addressno=(select addressno from lcappnt where lcappnt.contno=e.contno)),"
    	   +"(select f.phone from lcaddress f where f.customerno = e.appntno  and  f.addressno=(select addressno from lcappnt where lcappnt.contno=e.contno)),"
    	   +" e.insuredname"
    	   +", e.riskcode"
    	   +", (select riskname from lmriskapp where riskcode=e.riskcode)"
    	   +", e.prem,(case e.stateflag when '0' then '投保' when '1' then '承保有效' when '2' then '失效中止' else '终止' end),'个单' "
       		+"  from laagent b,laorphanpolicy c,labranchgroup d  ,lcpol e " +
       		"where c.agentcode=b.agentcode and  d.agentgroup=b.branchcode   and " +
       		" c.agentgroup=d.agentgroup and d.branchtype='1' and d.branchtype2='01'" +
       		" and b.branchtype='1' and b.branchtype2='01' and b.agentstate>='06' and c.contno=e.contno and c.reasontype='0' " ;
       if(this.mManageCom!=null&&!this.mManageCom.equals(""))    
       {
    	   msql += " and c.managecom like '"+this.mManageCom+"%'";
       }
       if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
       {
    	   msql += " and c.agentcode= getAgentCode('"+this.mAgentCode+"')";  	   
       }
       if(this.mContNo!=null&&!this.mContNo.equals(""))
       {
    	   msql+= " and c.contno='"+this.mContNo+"'";    	   
       }
       if(this.mName!=null&&!this.mName.equals(""))
       {
    	   msql+=" and c.agentcode exists (select agentcode from laagent where agentname='"+this.mName+"') ";
       }
       if(tAgentGroup!=null&&!tAgentGroup.equals(""))
       {
    	   msql += " and d.agentgroup ='"+tAgentGroup+"'";
       }
       msql +="union select c.contno,e.cvalidate ,getUniteCode(b.agentcode),b.name,b.managecom,d.branchattr,e.grpname  "
    	   +",'-'"
    	   +", (select g.grpaddress from lcgrpaddress g where g.customerno = e.customerno and g.addressno=(select addressno from lcgrpappnt where lcgrpappnt.grpcontno=e.grpcontno)) "
    	   +", (select f.phone1 from lcgrpaddress f where f.customerno = e.customerno  and  f.addressno=(select addressno from lcgrpappnt where lcgrpappnt.grpcontno=e.grpcontno))"
    	   +", '-'"
    	   +", e.riskcode"
    	   +", (select riskname from lmriskapp where riskcode=e.riskcode)"
    	   +", e.prem,(case e.stateflag when '0' then '投保' when '1' then '承保有效' when '2' then '失效中止' else '终止' end)  ,'团单'"
       		+"  from laagent b,laorphanpolicy c,labranchgroup d  ,lcgrppol e " +
       		"where c.agentcode=b.agentcode and  d.agentgroup=b.branchcode   and " +
       		" c.agentgroup=d.agentgroup and d.branchtype='1' and d.branchtype2='01'" +
       		" and b.branchtype='1' and b.branchtype2='01' and b.agentstate>='06' and e.grpcontno=c.contno  and c.reasontype='1'  " ;
       if(this.mManageCom!=null&&!this.mManageCom.equals(""))    
       {
    	   msql += " and c.managecom like '"+this.mManageCom+"%'";
       }
       if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
       {
    	   msql += " and c.agentcode=getAgentCode('"+this.mAgentCode+"')";  	   
       }
       if(this.mContNo!=null&&!this.mContNo.equals(""))
       {
    	   msql+= " and c.contno='"+this.mContNo+"'";    	   
       }
       if(this.mName!=null&&!this.mName.equals(""))
       {
    	   msql+=" and c.agentcode exists (select agentcode from laagent where agentname='"+this.mName+"') ";
       }
       if(tAgentGroup!=null&&!tAgentGroup.equals(""))
       {
    	   msql += " and d.agentgroup ='"+tAgentGroup+"'";
       }
      ExeSQL mExeSQL = new ExeSQL();
      SSRS mSSRS = new SSRS();
      mSSRS = mExeSQL.execSQL(msql);
      System.out.println(msql);
      if (mSSRS.mErrors.needDealError()) {
          CError tCError = new CError();
          tCError.moduleName = "MakeXMLBL";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "查询XML数据出错！";

          this.mErrors.addOneError(tCError);

          return false;

      }
      if (mSSRS.getMaxRow() <= 0) {
          CError tCError = new CError();
          tCError.moduleName = "MakeXMLBL";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "没有符合条件的孤儿单信息！";

          this.mErrors.addOneError(tCError);

          return false;
      }
      if(mSSRS.getMaxRow()>=1)
      {
    	 for(int i=1;i<=mSSRS.getMaxRow();i++)
    	 {
    	  String Info[] = new String[16];
    	  System.out.println(mSSRS.GetText(i, 1));
    	  Info[0] = mSSRS.GetText(i, 1);
    	  System.out.println(mSSRS.GetText(i, 2));
    	  Info[1] = mSSRS.GetText(i, 2);
    	  Info[2] = mSSRS.GetText(i, 3);
    	  Info[3] = mSSRS.GetText(i, 4);
    	  Info[4] = mSSRS.GetText(i, 5);
    	  Info[5] = mSSRS.GetText(i, 6);
    	  Info[6] = mSSRS.GetText(i, 7);
    	  Info[7] = mSSRS.GetText(i, 8);
    	  Info[8] = mSSRS.GetText(i, 9);
    	  Info[9] = mSSRS.GetText(i, 10);
    	  Info[10] = mSSRS.GetText(i, 11);
    	  Info[11] = mSSRS.GetText(i, 12);
    	  Info[12] = mSSRS.GetText(i, 13);
    	  Info[13] = mSSRS.GetText(i, 14);
    	  Info[14] = mSSRS.GetText(i, 15);
    	  Info[15] = mSSRS.GetText(i, 16);
    	  tListTable.add(Info);
    	 }
      }
      else {
          CError tCError = new CError();
          tCError.moduleName = "CreateXml";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "没有符合条件的孤儿单信息！";
          this.mErrors.addOneError(tCError);
          return false;
      }
        mdate = PubFun.getCurrentDate();
        mtime = PubFun.getCurrentTime();
         texttag.add("MakeDate", mdate); //日期
         texttag.add("MakeTime", mtime);
        if (texttag.size() > 0)
        {
            txmlexport.addTextTag(texttag);
        }
        txmlexport.addListTable(tListTable,title);
        mResult.clear();
        mResult.addElement(txmlexport);
        return true;
    }

    private String getComName(String strComCode)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();

    }
}
