/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SINOSOFT</p>
 * @author unascribed
 * @version 1.0
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ErrCheckUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //业务处理相关变量

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mManageCom = "";
    private String[] mDay = null;
    private String mScanOper = "";
    private String mDefineCode = "";

    public ErrCheckUI()
    {
    }

    /**
         传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY"))
            {
                buildError("submitData", "不支持的操作字符串");

                return false;
            }

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            // 进行业务处理
            if (!dealData())
            {
                return false;
            }

            // 准备传往后台的数据
            VData vData = new VData();

            if (!prepareOutputData(vData))
            {
                return false;
            }

            ErrCheckBL tErrCheckBL = new ErrCheckBL();
            System.out.println("Start ErrCheckBL Submit ...");

            if (!tErrCheckBL.submitData(vData, cOperate))
            {
                if (tErrCheckBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tErrCheckBL.mErrors);

                    return false;
                }
                else
                {
                    buildError("submitData", "ErrCheckBL发生错误，但是没有提供详细的出错信息");

                    return false;
                }
            }
            else
            {
                mResult = tErrCheckBL.getResult();

                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("submitData", "发生异常");

            return false;
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.add(mDay);
            vData.add(mManageCom);
            vData.add(mScanOper);
            vData.add(mDefineCode);
            vData.add(mGlobalInput);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");

            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    { //全局变量
        mDay = (String[]) cInputData.get(0);
        mManageCom = (String) cInputData.get(1);
        mScanOper = (String) cInputData.get(2);
        mDefineCode = (String) cInputData.get(3);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput",
                0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");

            return false;
        }

        if (mDefineCode == "")
        {
            buildError("mDefineCode", "没有得到足够的信息！");

            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ErrCheckUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        ErrCheckUI sc = new ErrCheckUI();
        String[] ttDay = new String[2];
        ttDay[0] = "2004-07-01";
        ttDay[1] = "2004-07-01";

        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "001";

        String code = "OutErr";
        String oper = "";
        String mMng = "WBGrp";
        VData tVData = new VData();
        tVData.addElement(ttDay);
        tVData.addElement(mMng);
        tVData.addElement(oper);
        tVData.addElement(code);
        tVData.addElement(tG);
        sc.submitData(tVData, "PRINTPAY");
    }
}
