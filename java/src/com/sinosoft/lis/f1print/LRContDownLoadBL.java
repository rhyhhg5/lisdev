package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LRContDownLoadBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private CreateExcelList mCreateExcelList = new CreateExcelList("");

	private TransferData mTransferData = new TransferData();

	// 查询出的问题保单
	private String[][] mExcelData = null;

	private GlobalInput mGlobalInput = new GlobalInput();
	/**
	 * 下载类型 cont:合同下载
	 */
	public String mType = "";

	/**
	 * y详细下载类型
	 */
	public String mDetailType = "";

	public  boolean submit(VData cInputData) {
		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 准备所有要打印的数据
		if (!getPrintData()) {
			buildError("getPrintData", "下载失败");
			return false;
		}

		if (mCreateExcelList == null) {
			buildError("submitData", "Excel数据为空");
			return false;
		}
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);

		mType = (String) mTransferData.getValueByName("type");
		if (mType.equals("") || mType == null) {
			buildError("mContType", "没有得到足够的信息:下载类型不能为空");
			return false;
		}

		mDetailType = (String) mTransferData.getValueByName(mType + "Type");
		if (mDetailType.equals("") || mDetailType == null) {
			buildError("mDetailType", "没有得到足够的信息:下载详细类型不能为空");
			return false;
		}

		return true;
	}

	public VData getResult() {
		return mResult;
	}

	public CErrors getErrors() {
		return mErrors;
	}
	
	public  CreateExcelList getCreateExcelList()
	{
		return this.mCreateExcelList;
	}
	
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "LCGrpContF1PBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private boolean getPrintData() {
		// 创建EXCEL列表
		mCreateExcelList.createExcelFile();
		String[] sheetName = { "list" };
		mCreateExcelList.addSheet(sheetName);

		// 正常合同信息下载
		if ("0".equals(this.mDetailType)) {
			// 设置表头
			String[][] tTitle = { { "再保合同号", "再保合同名称", "险种编码", "再保合同生效日期",
					"再保合同失效日期", "再保合同类型", "最低再保保额", "最高再保保额", "分保比例", "接受份额",
					"手续费比例", "特殊佣金比例", "理赔参与限额", "盈余佣金比例", "管理费比例", "选择折扣比例",
					"通知限额" } };
			// 表头的显示属性
			int[] displayTitle = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
					14, 15, 16, 17 };
			// 数据的显示属性
			int[] displayData = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
					14, 15, 16, 17 };
			int row = mCreateExcelList.setData(tTitle, displayTitle);
			if (row == -1) {
				buildError("getPrintData", "EXCEL中指定数据失败！");
				return false;
			}
			String mSql = "select distinct a.recontcode as 再保合同号,a.recontname as 再保合同名称,"
					+ "b.riskcode as 险种编码,a.rvalidate as 再保合同生效日期,a.rinvalidate as 再保合同失效日期,"
					+ "a.cessionmode as 再保合同类型, "
					+ "(select factorvalue from lrcalfactorvalue where recontcode=b.recontcode  and riskcode=b.riskcode and factorcode='LowReAmnt') as 最低再保保额, "
					+ "(select factorvalue from lrcalfactorvalue where recontcode=b.recontcode  and riskcode=b.riskcode and factorcode='AutoLimit') as 最高再保保额, "
					+ "(select factorvalue from lrcalfactorvalue where recontcode=b.recontcode  and riskcode=b.riskcode and factorcode='CessionRate') as 分保比例, "
					+ "(select factorvalue from lrcalfactorvalue where recontcode=b.recontcode  and riskcode=b.riskcode and factorcode='AccQuot') as 接受份额  , "
					+ "(select factorvalue from lrcalfactorvalue where recontcode=b.recontcode  and riskcode=b.riskcode and factorcode='ReinProcFeeRate') as 手续费比例, "
					+ "(select factorvalue from lrcalfactorvalue where recontcode=b.recontcode  and riskcode=b.riskcode and factorcode='SpeComRate') as 特殊佣金比例, "
					+ "(select factorvalue from lrcalfactorvalue where recontcode=b.recontcode  and riskcode=b.riskcode and factorcode='ClaimAttLmt') as 理赔参与限额, "
					+ "(select factorvalue from lrcalfactorvalue where recontcode=b.recontcode  and riskcode=b.riskcode and factorcode='ProfitCom') as 盈余佣金比例, "
					+ "(select factorvalue from lrcalfactorvalue where recontcode=b.recontcode  and riskcode=b.riskcode and factorcode='ReinManFeeRate') as 管理费比例, "
					+ "(select factorvalue from lrcalfactorvalue where recontcode=b.recontcode  and riskcode=b.riskcode and factorcode='ChoiRebaRate') as 选择折扣比例, "
					+ "(select factorvalue from lrcalfactorvalue where recontcode=b.recontcode  and riskcode=b.riskcode and factorcode='NoticeLmt') as 通知限额 "
					+ "from lrcontinfo a,lrcalfactorvalue b where a.recontcode=b.recontcode  with ur";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = tExeSQL.execSQL(mSql);
			if (tExeSQL.mErrors.needDealError()) {
				CError tError = new CError();
				tError.moduleName = "CreateExcelList";
				tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
				tError.errorMessage = "没有查询到需要下载的数据";
				mErrors.addOneError(tError);
				return false;
			}

			mExcelData = tSSRS.getAllData();
			if (mExcelData == null) {
				CError tError = new CError();
				tError.moduleName = "CreateExcelList";
				tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
				tError.errorMessage = "没有查询到需要输出的数据";
				return false;
			}

			if (mCreateExcelList.setData(mExcelData, displayData) == -1) {
				buildError("getPrintData", "EXCEL中设置数据失败！");
				return false;
			}
		} else if ("1".equals(this.mDetailType)) {
			// 设置表头
			String[][] tTitle = { { "再保合同号", "再保合同名称", "险种编码", "合同生效日", "合同类型",
					"最低再保保额", "最高再保保额", "分保比例", "手续费比例", "接受份额", "通知限额",
					"理赔参与限额" } };
			// 表头的显示属性
			int[] displayTitle = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
			// 数据的显示属性
			int[] displayData = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
			int row = mCreateExcelList.setData(tTitle, displayTitle);
			if (row == -1) {
				buildError("getPrintData", "EXCEL中指定数据失败！");
				return false;
			}
			String mSql = "select distinct a.tempcontcode 再保合同号,a.tempcontname 再保合同名称,b.riskcode 险种编码,"
					+ "a.makedate 合同生效日, a.cessionmode 合同类型,b.calmode 最低再保保额, b.retainamnt 最高再保保额,"
					+ "b.cessionrate 分保比例,b.reinprocfeerate 手续费比例, b.accquot 接受份额, b.claimnoticelmt 通知限额,"
					+ "b.claimattlmt 理赔参与限额  from lrtempcesscont a,lrtempcesscontinfo b "
					+ "where a.tempcontcode=b.tempcontcode and  a.recontstate='01' with ur";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = tExeSQL.execSQL(mSql);
			if (tExeSQL.mErrors.needDealError()) {
				CError tError = new CError();
				tError.moduleName = "CreateExcelList";
				tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
				tError.errorMessage = "没有查询到需要下载的数据";
				mErrors.addOneError(tError);
				return false;
			}

			mExcelData = tSSRS.getAllData();
			if (mExcelData == null) {
				CError tError = new CError();
				tError.moduleName = "CreateExcelList";
				tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
				tError.errorMessage = "没有查询到需要输出的数据";
				return false;
			}

			if (mCreateExcelList.setData(mExcelData, displayData) == -1) {
				buildError("getPrintData", "EXCEL中设置数据失败！");
				return false;
			}

		}
		return true;
	}
	public static void main(String[] args) {

		PrintPauseListEXCELBL tbl = new PrintPauseListEXCELBL();
		GlobalInput tGlobalInput = new GlobalInput();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("StartDate", "2012-5-08");
		tTransferData.setNameAndValue("EndDate", "2012-6-08");
		tTransferData.setNameAndValue("ManageCom", "8644");
		tGlobalInput.ManageCom = "8644";
		// tGlobalInput.Operator="xp";
		VData tData = new VData();
		tData.add(tGlobalInput);
		tData.add(tTransferData);

		CreateExcelList tCreateExcelList = new CreateExcelList();
		tCreateExcelList = tbl.getsubmitData(tData);
		if (tCreateExcelList == null) {
			System.out.println("112321231");
		} else {
			try {
				tCreateExcelList.write("c:\\cytzpare.xls");
			} catch (Exception e) {
				System.out.println("EXCEL生成失败！");
			}
		}
	}
}
