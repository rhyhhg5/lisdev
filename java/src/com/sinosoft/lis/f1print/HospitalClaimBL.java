package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.utility.*;
import java.lang.*;

public class HospitalClaimBL implements PrintService {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
       public CErrors mErrors = new CErrors();

       private VData mResult = new VData();

       //业务处理相关变量
       /** 全局数据 */
       private GlobalInput mGlobalInput = new GlobalInput();
       private TransferData mTransferData;
       private String ManageCom; //管理机构
       private String MakeDate1; //入机日期,起始日期
       private String MakeDate2; //入机日期,终止日期
       private String FeeType;//费用类型
       private String hospital;

       public HospitalClaimBL() {
       }

       /**
         传输数据的公共方法
        */
       public boolean submitData(VData cInputData, String cOperate) {
           if (!cOperate.equals("PRINT")) {
               buildError("submitData", "不支持的操作字符串");
               return false;
           }

           // 得到外部传入的数据，将数据备份到本类中
           if (!getInputData(cInputData)) {
               return false;
           }
           if(!dealData()){
               return false;
           }
           System.out.println("1 ...");
           mResult.clear();

           // 准备所有要打印的数据
           if (!getPrintData()) {
               return false;
           }
           return true;
       }

       /**
        * 根据前面的输入数据，进行BL逻辑处理
        * 如果在处理过程中出错，则返回false,否则返回true
        */
       private boolean dealData() {
           return true;
       }

       /**
        * 从输入数据中得到所有对象
        * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
        */
       private boolean getInputData(VData cInputData) {
           //全局变量
           mTransferData = (TransferData) cInputData.getObjectByObjectName(
                   "TransferData", 0);
           mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput",
                0);
           return true;
       }

       public VData getResult() {
           return mResult;
       }

       public CErrors getErrors() {
           return mErrors;
       }

       private void buildError(String szFunc, String szErrMsg) {
           CError cError = new CError();
           cError.moduleName = "HospitalClaimBL";
           cError.functionName = szFunc;
           cError.errorMessage = szErrMsg;
           this.mErrors.addOneError(cError);
       }

       private boolean getPrintData() {

           TextTag texttag = new TextTag(); //新建一个TextTag的实例

           MakeDate1 = (String) mTransferData.getValueByName("MakeDate1"); //统计日期,起始日期
           MakeDate2 = (String) mTransferData.getValueByName("MakeDate2"); //统计日期,终止日期
           FeeType = (String) mTransferData.getValueByName("FeeType"); //费用类型
           hospital = (String) mTransferData.getValueByName("hospital");
           ManageCom = (String) mTransferData.getValueByName("ManageCom");

           String md1 = "";
           String md2 = "";
           String makedate = ""; //统计时间,结案日期
           String strFeeType = "";//费用类型
           String strManageCom="";
           if (  MakeDate1 != null && MakeDate2 != null && !MakeDate2.equals("")&&!MakeDate1.equals("")) {
               makedate = " and c.EndCaseDate between '" + MakeDate1 + "' and '" +
                          MakeDate2 + "'";
               md1 = CommonBL.decodeDate(MakeDate1);
               md2 = CommonBL.decodeDate(MakeDate2);
               texttag.add("Date1", md1);
               texttag.add("Date2", md2);
           }
           if ((MakeDate1.equals("") || MakeDate1 == null) &&
               (MakeDate2.equals("") || MakeDate2 == null)) {
               MakeDate2 = new PubFun().getCurrentDate();
               md2 = CommonBL.decodeDate(MakeDate2);
               texttag.add("Date2", md2);
           }
           if(ManageCom !=null && !ManageCom.equals("")){
               strManageCom = " and c.mngcom like '"+ManageCom+"%'";
           }
           if(!FeeType.equals("0")){
               strFeeType = " and m.FeeType='"+FeeType+"'";
           }

           ListTable tListTable = new ListTable();
           tListTable.setName("table1");
           ListTable tListTable2 = new ListTable();
           tListTable2.setName("table2");

           String strhospital="";
           if(hospital.equals("1")){
               strhospital=" and not exists (select 1 from llfeemain a where a.caseno=m.caseno and a.hospitalcode<>m.hospitalcode)";
           }else if(hospital.equals("2")){
               strhospital=" and exists (select 1 from llfeemain a where a.caseno=m.caseno and a.hospitalcode<>m.hospitalcode)";
           }else if(hospital.equals("12")){
               strhospital="";
           }

              String sqla = "select xx.hospitalcode,h.hospitname,xx.feetype,sum(a),sum(c),sum(b) from "
                             +"(select m.caseno as caseno,m.hospitalcode as hospitalcode,m.feetype as feetype,value(sum(m.sumfee),0) as a,"
                             +"(select value(sum(realpay),0) from llclaimdetail p where m.caseno=p.caseno group by p.caseno) as c,count(m.sumfee) as b from "
                             +"llcase c,llfeemain m"
                             +" where  c.caseno=m.caseno and c.rgtstate in ('09','11','12')"
                             + strManageCom
                             + makedate
                             + strFeeType
                             + strhospital
                             +" group by  m.caseno,m.hospitalcode,m.feetype )as xx,"
                             +" ldhospital h where xx.hospitalcode=h.hospitcode group by xx.hospitalcode,xx.feetype,h.hospitname"
                             +" with ur";
               System.out.println(sqla);
               SSRS sba = new ExeSQL().execSQL(sqla);
               if(sba!=null){
                   for(int i=0;i<sba.getMaxRow();i++){
                       String v[] = new String[5];
                       for(int j=0;j<v.length;j++){
                           v[j]=sba.GetText(i+1,j+2);
                       }
                       if(v[1].equals("1")){
                           v[1]="门诊";
                       }else if(v[1].equals("2")){
                           v[1]="住院";
                       }else if(v[1].equals("3")){
                           v[1]="门诊特殊病";
                       }else{
                           v[1]="其它";
                       }
                       if(!sba.GetText(i+1,5).equals("0")){
                           tListTable.add(v);
                       }
                   }
               }
               String sqlb = "select sum(a),sum(c),sum(b) from "
                             +"(select m.caseno as caseno,m.hospitalcode as hospitalcode,m.feetype as feetype,value(sum(m.sumfee),0) as a,"
                             +"(select value(sum(realpay),0) from llclaimdetail p where m.caseno=p.caseno group by p.caseno) as c,count(m.sumfee) as b from "
                             +"llcase c,llfeemain m"
                             +" where  c.caseno=m.caseno and c.rgtstate in ('09','11','12')"
                             + strManageCom
                             + makedate
                             + strFeeType
                             + strhospital
                             +" group by  m.caseno,m.hospitalcode,m.feetype )as xx"
                             +" with ur";
               System.out.println(sqlb);
               SSRS sbb = new ExeSQL().execSQL(sqlb);
               if(sbb!= null){
                   String v[]={"0","0","0"};
                   for(int i=0;i<sbb.getMaxCol();i++){
                       if(sbb.GetText(1,i+1)!="null"){
                           v[i] = sbb.GetText(1, i + 1);
                       }
                   }
                   tListTable2.add(v);
               }

           String sqlm = "select distinct substr(Comcode,1,4) as comcode from LDcom where 1 = 1 and Sign='1' and comcode like '" +
                         ManageCom + "%' order by comcode with ur";
           String manage = new ExeSQL().getOneValue(sqlm);//机构号
           String sqln = "select name from LdCom  where ComCode='" +
                         manage + "' with ur";
           String name = new ExeSQL().getOneValue(sqln);//机构名
           texttag.add("ManageCom", name);

           XmlExport xmlexport = new XmlExport();

           xmlexport.createDocument("HospitalClaim.vts", "printer"); //分机构


           if (texttag.size() > 0) {
               xmlexport.addTextTag(texttag);
           }
           xmlexport.addListTable(tListTable, new String[5]);
           xmlexport.addListTable(tListTable2, new String[3]);

           mResult.clear();
           mResult.addElement(xmlexport);

           return true;
       }


       public static void main(String[] args) {

           HospitalClaimBL tHospitalClaimBL = new HospitalClaimBL();
           VData tVData = new VData();
           GlobalInput tGlobalInput = new GlobalInput();
           tGlobalInput.ManageCom = "86";
           tGlobalInput.Operator = "001";
           tVData.addElement(tGlobalInput);
           tHospitalClaimBL.getPrintData();

       }
   }
