package com.sinosoft.lis.f1print;

/**
 * <p>Title:FFPayDayF1PExcelUI </p>
 * <p>Description:X2-日结单 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author yan
 * @version 1.0
 */

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class FFPayDayF1PExcelUI {

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors=new CErrors();
  /** 全局数据 */
  private GlobalInput mGlobalInput =new GlobalInput() ;
  private String mDay[]=null;
  private String tOutXmlPath = "";

  public FFPayDayF1PExcelUI() {
  }
  /**
       传输数据的公共方法
       */
  public boolean submitData(VData cInputData, String cOperate)
  {
    try{
      if( !cOperate.equals("CONFIRM") && !cOperate.equals("PRINT"))
	    {
	      buildError("submitData", "不支持的操作字符串");
	      return false;
	    }

      // 得到外部传入的数据，将数据备份到本类中
      if( !getInputData(cInputData) ) {
        return false;
      }

      // 进行业务处理
      if( !dealData() ) {
        return false;
      }

      // 准备传往后台的数据
      VData vData = new VData();

      if( !prepareOutputData(vData) ) {
        return false;
      }

      FFPayDayF1PExcelBL tFFPayDayF1PExcelBL = new FFPayDayF1PExcelBL();
      System.out.println("Start FFPayDayF1PExcelBL Submit ...X2");
      if( !tFFPayDayF1PExcelBL.submitData(vData, cOperate) ) {
    	  if( tFFPayDayF1PExcelBL.mErrors.needDealError() ) {
    		  mErrors.copyAllErrors(tFFPayDayF1PExcelBL.mErrors);
              return false;
    	  }else{
    		  buildError("submitData", "FFPayDayF1PExcelBL，但是没有提供详细的出错信息");
              return false;
    	  }
      }
    }catch(Exception e){
      e.printStackTrace();
      buildError("submitData", "操作失败");
      return false;
    }
    return true;
  }



  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData(VData vData)
  {
    try {
      vData.clear();
      vData.add(mDay);
      vData.add(mGlobalInput);
      vData.add(tOutXmlPath);
    } catch (Exception ex) {
      ex.printStackTrace();
      buildError("prepareOutputData", "发生异常");
      return false;
    }
    return true;
  }

  /**
   * 根据前面的输入数据，进行UI逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData()
  {
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    //全局变量
    mDay=(String[])cInputData.get(0);
    mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    tOutXmlPath = (String)cInputData.get(2);

    System.out.println("Start FFPayDayF1PExcelUI getInputData ...X2");
    System.out.println("打印日期-->"+mDay[0]+"至"+mDay[1]+";文件路径-->"+tOutXmlPath);
    System.out.println("Operator-->"+mGlobalInput.Operator+";Managecom-->"+mGlobalInput.ManageCom);
    if( mGlobalInput==null ) {
		buildError("getInputData", "没有得到足够的信息！");
		return false;
    }
    if( tOutXmlPath==null || tOutXmlPath.equals("")) {
        buildError("getInputData", "没有获得文件路径信息！");
        return false;
    }
    return true;
  }

  private void buildError(String szFunc, String szErrMsg)
  {
    CError cError = new CError( );

    cError.moduleName = "FFPayDayF1PExcelUI";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }


  public static void main(String[] args) {
    FFPayDayF1PExcelUI fin = new FFPayDayF1PExcelUI();
    String ttDay[]=new String[2];
    ttDay[0]="2002-10-1";
    ttDay[1]="2003-11-30";
    VData tVData = new VData();
    tVData.addElement(ttDay);
    GlobalInput tG = new GlobalInput();
    tG.ManageCom="86110000";
    tG.Operator="001";
    tVData.addElement(tG);
    fin.submitData(tVData,"");
  }
}