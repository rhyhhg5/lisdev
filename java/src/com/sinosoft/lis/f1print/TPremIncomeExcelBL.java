package com.sinosoft.lis.f1print;

/**
 * <p>Title: TPremIncomeExcelBL</p>
 * <p>Description:非现场业务审计 保费收入数据表 </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: sinosoft</p>
 * @author :  y
 * @date:2013-11-11
 * @version 1.0
 */
import java.math.BigDecimal;
import java.text.DecimalFormat;

import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class TPremIncomeExcelBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	private String mDay[] = null; // 获取时间
	//取得文件路径
	private String tOutXmlPath="";
	private GlobalInput mGlobalInput = new GlobalInput(); // 全局变量
	private String mFilePath = "";
	private String mFileName = "";
	private String mManageCom = "";
	private String codealias = "";
	private String mRiskCode = "";
	private CSVFileWrite mCSVFileWrite = null;

	public TPremIncomeExcelBL() {
	}

	public static void main(String[] args) {
		GlobalInput tG = new GlobalInput();
		tG.Operator = "001";
		tG.ManageCom = "86110000";
		VData vData = new VData();
		String[] tDay = new String[4];
		tDay[0] = "2006-03-01";
		tDay[1] = "2006-03-31";
		vData.addElement(tDay);
		vData.addElement(tG);

		TPremIncomeExcelBL tC = new TPremIncomeExcelBL();
		tC.submitData(vData, "PRINT");

	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("PRINT")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		if (!getInputData(cInputData)) {
			return false;
		}
		if (cOperate.equals("PRINT")) // 打印收费
		{
			if (!getPrintData()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		// 全局变量
		System.out.println("start TPRemIncomeExcelBL...TPremIncome");
		mDay = (String[]) cInputData.get(0);
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        tOutXmlPath=(String)cInputData.get(2);
        mFileName = (String)cInputData.get(3);
        mManageCom = (String)cInputData.get(4);
        mRiskCode = (String)cInputData.get(5);
		
		System.out.println("BL的打印日期："+mDay[0]+"至"+mDay[1]+";文件名mFileName："+mFileName);
		System.out.println("BL的tOutXmlPath:"+tOutXmlPath);
		
		if (tOutXmlPath == null || tOutXmlPath.equals("")) {
			buildError("getInputData", "没有获得文件路径信息！");
			return false;
		}
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		if(mFileName.equals("") || mFileName==null){
			buildError("getInputData", "没有获得文件名称信息！");
			return false;
		}
		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "TPremIncomeExcelBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
	
	/**
	 * 新的打印数据方法
	 * 
	 * @return
	 */
	private boolean getPrintData() {
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		
		String Msql = "select Name from LDCom where ComCode='"+ mGlobalInput.ManageCom + "'"; // 管理机构
		tSSRS = tExeSQL.execSQL(Msql);
		String manageCom = tSSRS.GetText(1, 1);

		mFilePath = tOutXmlPath;
    	mCSVFileWrite = new CSVFileWrite(mFilePath,mFileName);
		String[][] tTitle = new String[4][];
		tTitle[0] = new String[]{"非现场业务审计数据表1：保费收入"};
		tTitle[1] = new String[]{"金额：元"};
		tTitle[2] = new String[]{"统计日期："+mDay[0]+"至"+mDay[1]};
		tTitle[3] = new String[]{"管理机构","渠道代码","险种代码","保单号","印刷号","凭证类型","业务信息","凭证号","金额","缴费方式","缴费类型","投保人","被保险人","投保日期","生效日期","过账日期","保单状态","代理机构","代理人代码"};
		String[] tContentType = new String[]{"String","String","String","String","String","String","String","String","Number","String","String","String","String","String","String","String","String","String","String"};
		if(!mCSVFileWrite.addTitle(tTitle, tContentType)){
			return false;
		}
//		String sql = "SELECT a.manageCom AS managecom," + //管理机构-1
//				"a.saleChnl AS saleChnl," + //渠道代码-2
//				"a.riskCode AS riskCode," + //险种代码-3
//				"a.contno AS contno," + //保单号-4
//				"(SELECT prtno FROM lccont WHERE contno=a.contno " +
//				"UNION SELECT prtno FROM lbcont WHERE contno=a.contno " +
//				"UNION SELECT prtno FROM lcgrpcont WHERE grpcontno=a.contno " +
//				"UNION SELECT prtno FROM lbgrpcont WHERE grpcontno=a.contno fetch first row only) AS printno," + //印刷号-5
//				"vouchertype AS vouchertype," + //凭证类型-6
//				"(SELECT vouchername FROM FIVoucherDef WHERE voucherid=a.certificateid) AS classInfo," + //业务信息-7
//				"SELECT voucherid FROM interfacetable WHERE importtype = a.classtype " +
//				"AND batchno = a.batchno " +
//				"AND depcode = a.managecom " +
//				"AND accountcode = a.accountcode " +
//				"AND chargedate = a.accountdate fetch first 1 rows only) AS voucherid," + //凭证号-8
//				"a.summoney AS summoney," + //金额-9
//				"a.pcont AS pcont," + //缴费方式-10
//				"(case (SELECT paymode FROM lccont WHERE contno=a.contno " +
//				"UNION SELECT paymode FROM lbcont WHERE contno=a.contno " +
//				"UNION SELECT paymode FROM lcgrpcont WHERE grpcontno=a.contno " +
//				"UNION SELECT paymode FROM lbgrpcont WHERE grpcontno=a.contno fetch first row only) " +
//				"when '1' then '现金' " +
//				"when '2' then '现金支票' " +
//				"when '3' then '转帐支票' " +
//				"when '4' then '银行转帐' " +
//				"when '5' then '内部转帐' " +
//				"when '6' then 'pos机' " +
//				"when '8' then '医保卡个人账户转账' " +
//				"when '10' then '定期结算应收保费' " +
//				"when '11' then '银行汇款' " +
//				"when '12' then '银行代收' " +
//				"when '14' then '营销员代收（现金）' " +
//				"when '15' then '营销员代收（卡转帐）' " +
//				"when '16' then '现金汇款' " +
//				"when '17' then '银行缴款单' " +
//				"else '其他' end) AS paymode," + //缴费类型-11
//				"(SELECT appntname FROM lccont WHERE contno=a.contno " +
//				"UNION SELECT appntname FROM lbcont WHERE contno=a.contno " +
//				"UNION SELECT grpname FROM lcgrpcont WHERE grpcontno=a.contno " +
//				"UNION SELECT grpname FROM lbgrpcont WHERE grpcontno=a.contno fetch first row only) AS appntname," + //投保人-12
//				"(case listflag when '2' then '--' else ( " +
//				"SELECT insuredname FROM lccont WHERE contno=a.contno " +
//				"UNION SELECT insuredname FROM lbcont WHERE contno=a.contno fetch first row only) end) AS insuredname," + //被保险人-13
//				"(SELECT PolApplyDate FROM lccont WHERE contno=a.contno " +
//				"UNION SELECT PolApplyDate FROM lbcont WHERE contno=a.contno " +
//				"UNION SELECT PolApplyDate FROM lcgrpcont WHERE grpcontno=a.contno " +
//				"UNION SELECT PolApplyDate FROM lbgrpcont WHERE grpcontno=a.contno fetch first row only) AS PolApplyDate," + //投保日期-14
//				"(SELECT cvalidate FROM lccont WHERE contno=a.contno " +
//				"UNION SELECT cvalidate FROM lbcont WHERE contno=a.contno " +
//				"UNION SELECT cvalidate FROM lcgrpcont WHERE grpcontno=a.contno " +
//				"UNION SELECT cvalidate FROM lbgrpcont WHERE grpcontno=a.contno fetch first row only) AS cvalidate," + //生效日期-15
//				"a.accountdate AS accountdate," + //过账日期-16
//				"(SELECT stateflag FROM lccont WHERE contno=a.contno " +
//				"UNION SELECT stateflag FROM lbcont WHERE contno=a.contno " +
//				"UNION SELECT stateflag FROM lcgrpcont WHERE grpcontno=a.contno " +
//				"UNION SELECT stateflag FROM lbgrpcont WHERE grpcontno=a.contno fetch first row only) AS stateflag," + //保单状态-17
//				"(SELECT agentcom FROM lccont WHERE contno=a.contno " +
//				"UNION SELECT agentcom FROM lbcont WHERE contno=a.contno " +
//				"UNION SELECT agentcom FROM lcgrpcont WHERE grpcontno=a.contno " +
//				"UNION SELECT agentcom FROM lbgrpcont WHERE grpcontno=a.contno fetch first row only) AS agentcom," + //代理机构编码-18
//				"(SELECT agentcode FROM lccont WHERE contno=a.contno " +
//				"UNION SELECT agentcode FROM lbcont WHERE contno=a.contno " +
//				"UNION SELECT agentcode FROM lcgrpcont WHERE grpcontno=a.contno " +
//				"UNION SELECT agentcode FROM lbgrpcont WHERE grpcontno=a.contno fetch first row only) AS agentcode " + //代理人编码-19
//				"FROM FiVoucherDataDetail a WHERE 1=1 " +
//				"AND accountdate BETWEEN '"+mDay[0]+"' AND '"+mDay[1]+"' " +
//				"AND a.accountcode in('6031000000','7031000000') " +
//			    "AND a.finitemtype = 'C' " +
//			    "AND a.checkflag = '00' ";
		StringBuffer sqlbuf = new StringBuffer(); 
		sqlbuf.append("SELECT X.managecom,X.salechnl,X.riskcode,X.contno,Y.prtno,X.vouchertype,X.classInfo,X.voucherid,X.summoney,X.pcont," +
		"(case Y.paymode " +
		"when '1' then '现金' " +
		"when '2' then '现金支票' " +
		"when '3' then '转帐支票' " +
		"when '4' then '银行转帐' " +
		"when '5' then '内部转帐' " +
		"when '6' then 'pos机' " +
		"when '8' then '医保卡个人账户转账' " +
		"when '10' then '定期结算应收保费' " +
		"when '11' then '银行汇款' " +
		"when '12' then '银行代收' " +
		"when '14' then '营销员代收（现金）' " +
		"when '15' then '营销员代收（卡转帐）' " +
		"when '16' then '现金汇款' " +
		"when '17' then '银行缴款单' " +
		"else '其他' end),Y.appntname,Y.insuredname,Y.polapplydate,Y.cvalidate,X.accountdate," +
		"Y.stateflag,Y.agentcom,Y.agentcode " +
		"FROM (" +
		"SELECT a.Managecom AS managecom, a.saleChnl AS salechnl,a.Riskcode AS riskcode," +
		"a.Contno AS contno,a.Vouchertype AS vouchertype," +
		"(SELECT Vouchername FROM FIVoucherDef WHERE voucherid=a.certificateid fetch first row only) AS classInfo," +
		"(SELECT Voucherid FROM interfacetable WHERE importtype = a.classtype " +
		"AND batchno = a.batchno " +
		"AND depcode = a.managecom " +
		"AND accountcode = a.accountcode " +
		"AND chargedate = a.accountdate fetch first 1 rows only) AS voucherid," +
		"a.Summoney AS summoney,a.PCont AS pcont,a.accountdate AS accountdate " +
		"FROM FiVoucherDataDetail a " +
		"WHERE a.accountcode in('6031000000','7031000000') " +
		"AND a.finitemtype = 'C' " +
		"AND a.checkflag = '00' ");
		if(!"".equals(mManageCom) && !"86".equals(mManageCom) && !"860000".equals(mManageCom)){
			String manageSql = "SELECT codealias FROM ficodetrans WHERE codetype='ManageCom' AND code='"+mManageCom+"' with ur";
			tSSRS = tExeSQL.execSQL(manageSql);
			codealias = tSSRS.GetText(1, 1);
			if(codealias!=null && !"".equals(codealias)){
				sqlbuf.append("AND a.managecom = '"+codealias+"' ");
			}
		}
		if(!"".equals(mRiskCode)){
			sqlbuf.append("AND a.riskcode = '"+mRiskCode+"' ");
		}
		sqlbuf.append("AND a.accountdate BETWEEN '"+mDay[0]+"' AND '"+mDay[1]+"')as X, " +
		"lateral(" +
		"SELECT Prtno AS prtno,Paymode AS paymode,Appntname AS appntname,Insuredname AS insuredname,PolApplyDate AS polapplydate,CValidate AS cvalidate,Stateflag AS stateflag,Agentcom AS agentcom,Agentcode AS agentcode FROM lccont WHERE contno=X.contno " +
		"UNION SELECT Prtno AS prtno,Paymode AS paymode,Appntname AS appntname,Insuredname AS insuredname,PolApplyDate AS polapplydate,CValidate AS cvalidate,Stateflag AS stateflag,Agentcom AS agentcom,Agentcode AS agentcode FROM lbcont WHERE contno=X.contno " +
		"UNION SELECT Prtno AS prtno,Paymode AS paymode,Grpname AS appntname,'--' AS insuredname,PolApplyDate AS polapplydate,CValidate AS cvalidate,Stateflag AS stateflag,Agentcom AS agentcom,Agentcode AS agentcode FROM lcgrpcont WHERE grpcontno=X.contno " +
		"UNION SELECT Prtno AS prtno,Paymode AS paymode,Grpname AS appntname,'--' AS insuredname,PolApplyDate AS polapplydate,CValidate AS cvalidate,Stateflag AS stateflag,Agentcom AS agentcom,Agentcode AS agentcode FROM lbgrpcont WHERE grpcontno=X.contno fetch first row only) " +
		"as Y ");
		
		RSWrapper rs = new RSWrapper();
		if(!rs.prepareData(null, sqlbuf.toString())){
			return false;
		}
		try {
			
			do {
				tSSRS = rs.getSSRS();
				int maxRow = tSSRS.getMaxRow();
				System.out.println("查询返回行数："+maxRow);
				if(tSSRS!=null && tSSRS.getMaxRow()>0){
					String[][] tContent = new String[maxRow][];
					for(int row=1; row<=maxRow;row++){
						String[] ListInfo = new String[19];
						for(int n=0;n< ListInfo.length;n++){
							ListInfo[n] = "";
						}
						for(int col=1;col<=tSSRS.getMaxCol();col++){
							if(col==9){
								ListInfo[col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
							}else{
								ListInfo[col-1] = tSSRS.GetText(row, col);
							}
						}
						tContent[row-1] = ListInfo;
					}
					mCSVFileWrite.addContent(tContent);
					if(!mCSVFileWrite.writeFile()){
						mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
						return false;
					}
				}else{
					String[][] tContent = new String[1][];
					String[] ListInfo = new String[19];
					for(int i=0;i<ListInfo.length;i++){
						ListInfo[i] = "";
					}
					tContent[0] = ListInfo;
					mCSVFileWrite.addContent(tContent);
					if(!mCSVFileWrite.writeFile()){
						mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
					}
				}
			} while (tSSRS!=null && tSSRS.getMaxRow()>0);
		} catch (Exception e) {
			// TODO: handle exception
		}finally{
			rs.close();
		}
	return true;
}

}
