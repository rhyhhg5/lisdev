package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author MengWt
 * @version 1.0
 */
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;
public class FirstTrialOperatorStatisticUI {
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
         public CErrors mErrors = new CErrors();

         private VData mResult = new VData();
         //业务处理相关变量
         /** 全局数据 */
         private GlobalInput mGlobalInput = new GlobalInput();
         //private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    public FirstTrialOperatorStatisticUI() {
    }
    public boolean submitData(VData cInputData, String cOperate)
{
   if (!cOperate.equals("PRINT"))
   {
       buildError("submitData", "不支持的操作字符串");
       return false;
   }

   // 得到外部传入的数据，将数据备份到本类中
   if (!getInputData(cInputData))
   {
       return false;
   }

   // 进行业务处理
   if (!dealData())
   {
       return false;
   }

   // 准备传往后台的数据
   VData vData = new VData();

   if (!prepareOutputData(vData))
   {
       return false;
   }

   FirstTrialOperatorStatisticBL tFirstTrialOperatorStatisticBL = new FirstTrialOperatorStatisticBL();
   System.out.println("Start FirstTrialOperatorStatisticUI Submit ...");

   if (!tFirstTrialOperatorStatisticBL.submitData(cInputData, cOperate))
   {
       if (tFirstTrialOperatorStatisticBL.mErrors.needDealError())
       {
           mErrors.copyAllErrors(tFirstTrialOperatorStatisticBL.mErrors);
           return false;
       }
       else
       {
           buildError("submitData", "PayColPrtBL发生错误，但是没有提供详细的出错信息");
           return false;
       }
   }
   else
   {
       mResult = tFirstTrialOperatorStatisticBL.getResult();
       return true;
   }
}


/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
private boolean prepareOutputData(VData vData)
{
   return true;
}

/**
* 根据前面的输入数据，进行UI逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
   return true;
}

/**
* 从输入数据中得到所有对象
* 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
*/
private boolean getInputData(VData cInputData)
{
   return true;
}

public VData getResult()
{
   return this.mResult;
}

private void buildError(String szFunc, String szErrMsg)
{
   CError cError = new CError();

   cError.moduleName = "DerferAppF1PUI";
   cError.functionName = szFunc;
   cError.errorMessage = szErrMsg;
   this.mErrors.addOneError(cError);
}


public static void main(String[] args)
{
    FirstTrialOperatorStatisticUI tFirstTrialOperatorStatisticUI = new FirstTrialOperatorStatisticUI();
    VData tVData = new VData();
    GlobalInput tGlobalInput = new GlobalInput();
    tGlobalInput.ManageCom = "86";
    tGlobalInput.Operator = "001";
    tVData.add(tGlobalInput);
    tFirstTrialOperatorStatisticUI.submitData(tVData, "PRINT");
}

}
