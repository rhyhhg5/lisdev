package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.DecimalFormat;

import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.lis.db.LJAGetEndorseDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LOPRTManager2DB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.schema.LOPRTInvoiceManagerSchema;
import com.sinosoft.lis.schema.LOPRTManager2Schema;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LOPRTManager2Set;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.db.LJSPayBDB;
import com.sinosoft.lis.finfee.FFInvoicePrtManagerBL;
import com.sinosoft.lis.vschema.LJSPayBSet;
import com.sinosoft.lis.schema.LJSPayBSchema;
import com.sinosoft.lis.vdb.LJAPayDBSet;
import com.sinosoft.lis.vdb.LOPRTManager2DBSet;

public class FeeInvoiceF1PBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private String mPayContr = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LJAPaySchema mLJAPaySchema = new LJAPaySchema();

    private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();

    private LJAPayPersonSet mNewLJAPayPersonSet = new LJAPayPersonSet();

    private String mOperate = "";

    private LOPRTManager2Schema mLOPRTManager2Schema = new LOPRTManager2Schema();

    private LOPRTManager2Set mLOPRTManager2Set = new LOPRTManager2Set();

    private TransferData mTransferData = new TransferData();

    private String appntName = "";

    private String riskName = "";

    private String hPerson = "";

    private String cPerson = "";

    private String remark = "";

    private LOPRTInvoiceManagerSchema mLOPRTInvoiceManagerSchema = new LOPRTInvoiceManagerSchema();

    private String mInvoiceNo = "";

    public FeeInvoiceF1PBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        System.out.println("cOperate:" + cOperate);
        if (!cOperate.equals("CONFIRM") && !cOperate.equals("PRINT")
                && !cOperate.equals("REPRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        if (cOperate.equals("CONFIRM"))
        {
            mResult.clear();
            // 准备所有要打印的数据
            getPrintData();
        }
        else if (cOperate.equals("PRINT"))
        {
            System.out.print("update print");
            if (!saveData())
            {
                return false;
            }
        }
        else if (cOperate.equals("REPRINT"))
        {
            mResult.clear();
            // 准备所有要打印的数据
            getPrintData2();
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        if (mOperate.equals("PRINT") || mOperate.equals("REPRINT"))
        {
            mGlobalInput.setSchema((GlobalInput) cInputData
                    .getObjectByObjectName("GlobalInput", 0));
            mLOPRTManager2Schema.setSchema((LOPRTManager2Schema) cInputData
                    .getObjectByObjectName("LOPRTManager2Schema", 0));
        }

        if (mOperate.equals("CONFIRM"))
        {
            //全局变量
            mGlobalInput.setSchema((GlobalInput) cInputData
                    .getObjectByObjectName("GlobalInput", 0));
            mLJAPaySchema.setSchema((LJAPaySchema) cInputData
                    .getObjectByObjectName("LJAPaySchema", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            mLJAPayPersonSet = (LJAPayPersonSet) cInputData
                    .getObjectByObjectName("LJAPayPersonSet", 0);
            this.appntName = (String) mTransferData.getValueByName("AppntName");
            this.riskName = (String) mTransferData.getValueByName("RiskName");
            this.hPerson = (String) mTransferData.getValueByName("HPerson");
            this.cPerson = (String) mTransferData.getValueByName("CPerson");
            this.remark = (String) mTransferData.getValueByName("Remark");

        }

        TransferData tParameters = (TransferData) cInputData
                .getObjectByObjectName("TransferData", 0);
        if (tParameters != null)
            mInvoiceNo = (String) tParameters.getValueByName("invoiceNo");

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "FeeInvoiceF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        String tContNo = "";
        String tEndorNo = "";
        String tAgentNo = "";
        String tAgentGroup = "";
        String tBranchattr = "";
        String tPayToDay = "";
        String AgentName = "";
        String AgentGroupName = "";
        String State = "";
        String Result = "";
        String tManageCom = "";
        int count = 0;

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String tSql = "";

        LJAPayDB tLJAPayDB = new LJAPayDB();
        tLJAPayDB.setSchema(mLJAPaySchema);
        if (!tLJAPayDB.getInfo())
        {
            mErrors.copyAllErrors(tLJAPayDB.mErrors);
            buildError("outputXML", "在取得LJAPay的数据时发生错误");
            return false;
        }
        String DSumMoney = "";
        double sumMoney = 0.0;
        if (tLJAPayDB.getIncomeType().equals("15"))
        {
            mPayContr = "(";
            for (int i = 1; i <= mLJAPayPersonSet.size(); i++)
            {
                if (i == mLJAPayPersonSet.size())
                {
                    mPayContr = mPayContr + "'"
                            + mLJAPayPersonSet.get(i).getPayNo() + "'";
                }
                else
                {
                    mPayContr = mPayContr + "'"
                            + mLJAPayPersonSet.get(i).getPayNo() + "',";
                }
            }
            mPayContr = mPayContr + ")";
            String paySql = "select * from LJAPayPerson where payno in "
                    + mPayContr;
            LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();

            mNewLJAPayPersonSet = tLJAPayPersonDB.executeQuery(paySql);
            if (mNewLJAPayPersonSet == null
                    || mNewLJAPayPersonSet.size() != mLJAPayPersonSet.size())
            {
                mErrors.copyAllErrors(mNewLJAPayPersonSet.mErrors);
                buildError("outputXML", "在取得LJAPayPerson的数据时发生错误");
                return false;
            }
            String strSQL = "select sum(SumActuPayMoney) from LJAPay where Payno in "
                    + mPayContr;
            System.out.println("strSQL");
            String sMoney = tExeSQL.getOneValue(strSQL);

            if (StrTool.cTrim(sMoney).equals(""))
            {
                sumMoney = tLJAPayDB.getSumActuPayMoney();
            }
            else
            {
                sumMoney = Double.parseDouble(sMoney);
            }
            DSumMoney = PubFun.getChnMoney(sumMoney);

        }
        else
        {
            String strSQL = "select SumActuPayMoney from LJAPay where Payno ='"
                    + tLJAPayDB.getPayNo() + "'";
            System.out.println("strSQL");
            String sMoney = tExeSQL.getOneValue(strSQL);

            if (StrTool.cTrim(sMoney).equals(""))
            {
                sumMoney = tLJAPayDB.getSumActuPayMoney();
            }
            else
            {
                sumMoney = Double.parseDouble(sMoney);
            }
            DSumMoney = PubFun.getChnMoney(sumMoney);

        }

        if (tLJAPayDB.getIncomeType().equals("2"))
        {
            LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();

            tLJAPayPersonDB.setPayNo(mLJAPaySchema.getPayNo());
            LJAPayPersonSet tLJAPayPersonSet = tLJAPayPersonDB.query();
            LJAPayPersonSchema aLJAPayPersonSchema = tLJAPayPersonSet.get(1);
            tContNo = aLJAPayPersonSchema.getContNo();
            tAgentNo = aLJAPayPersonSchema.getAgentCode();
            tLAAgentDB.setAgentCode(tAgentNo);
            if (!tLAAgentDB.getInfo())
            {
                mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                buildError("outputXML", "在取得LAAgent的数据时发生错误");
                return false;
            }

            tAgentGroup = tLAAgentDB.getAgentGroup();
            tPayToDay = aLJAPayPersonSchema.getCurPayToDate();
            tLABranchGroupDB.setAgentGroup(tAgentGroup);
            if (!tLABranchGroupDB.getInfo())
            {
                mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                buildError("outputXML", "在取得LABranchGroup的数据时发生错误");
                return false;
            }
            tBranchattr = tLABranchGroupDB.getBranchAttr();
            String strSQL = "";
            if (!StrTool.cTrim(tLJAPayDB.getGetNoticeNo()).equals(""))
            {
                Result = (new ExeSQL()
                        .getOneValue("select count(1) from ljapay a,ljspayb b where a.getnoticeno=b.getnoticeno and a.incometype='2' and a.getnoticeno='"
                                + tLJAPayDB.getGetNoticeNo() + "'"));
                count = Integer.parseInt(Result);
                if (count > 0
                        && (tLJAPayDB.getManageCom().equals("8694") || tLJAPayDB
                                .getManageCom().equals("86940000")))
                {
                    AgentName = "业务员：" + tLAAgentDB.getName();
                    AgentGroupName = "营销服务部："
                            + tLABranchGroupDB.getBranchAttr();
                    State = "续期";
                }
            }

        }

        if (tLJAPayDB.getIncomeType().equals("1"))
        {
            LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
            tLJAPayGrpDB.setPayNo(mLJAPaySchema.getPayNo());
            LJAPayGrpSet tLJAPayGrpSet = tLJAPayGrpDB.query();
            LJAPayGrpSchema aLJAPayGrpSchema = tLJAPayGrpSet.get(1);
            tContNo = aLJAPayGrpSchema.getGrpContNo();
            tPayToDay = aLJAPayGrpSchema.getCurPayToDate();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(aLJAPayGrpSchema.getAgentCode());
            if (!tLAAgentDB.getInfo())
            {
                mErrors.copyAllErrors(tLAAgentDB.mErrors);
                buildError("outputXML", "在取得LAAgent的数据时发生错误");
                return false;
            }
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(aLJAPayGrpSchema.getAgentGroup());
            if (!tLABranchGroupDB.getInfo())
            {
                mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                buildError("outputXML", "在取得LABranchGroup的数据时发生错误");
                return false;
            }
            String strSQL = "";
            count = 0;
            Result = "";
            if (!StrTool.cTrim(aLJAPayGrpSchema.getGetNoticeNo()).equals(""))
            {
                Result = (new ExeSQL()
                        .getOneValue("select count(1) from ljapaygrp a,ljspaygrpb b where a.getnoticeno=b.getnoticeno and a.grppolno=b.grppolno and a.getnoticeno='"
                                + aLJAPayGrpSchema.getGetNoticeNo() + "'"));
                count = Integer.parseInt(Result);
                if (count > 0
                        && (tLJAPayDB.getManageCom().equals("8694") || tLJAPayDB
                                .getManageCom().equals("86940000")))
                {
                    AgentName = "业务员：" + tLAAgentDB.getName();
                    AgentGroupName = "营销服务部："
                            + tLABranchGroupDB.getBranchAttr();
                    State = "续期";
                }
            }

        }

        if (tLJAPayDB.getIncomeType().equals("10"))
        {
            LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
            tLJAGetEndorseDB.setActuGetNo(mLJAPaySchema.getPayNo());
            LJAGetEndorseSet tLJAGetEndorseSet = tLJAGetEndorseDB.query();
            LJAGetEndorseSchema aLJAGetEndorseSchema = tLJAGetEndorseSet.get(1);

            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setContNo(aLJAGetEndorseSchema.getContNo());
            if (!tLCContDB.getInfo())
            {
                mErrors.copyAllErrors(tLCContDB.mErrors);
                buildError("outputXML", "在取得LCCont的数据时发生错误");
                return false;
            }
            tContNo = tLCContDB.getContNo();
            tPayToDay = tLCContDB.getPaytoDate();
            tEndorNo = aLJAGetEndorseSchema.getEndorsementNo();
        }

        if (tLJAPayDB.getIncomeType().equals("3"))
        {
            LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
            tLJAGetEndorseDB.setActuGetNo(mLJAPaySchema.getPayNo());
            LJAGetEndorseSet tLJAGetEndorseSet = tLJAGetEndorseDB.query();
            LJAGetEndorseSchema aLJAGetEndorseSchema = tLJAGetEndorseSet.get(1);

            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(aLJAGetEndorseSchema.getGrpContNo());
            if (!tLCGrpContDB.getInfo())
            {
                mErrors.copyAllErrors(tLCGrpContDB.mErrors);
                buildError("outputXML", "在取得LCGrpCont的数据时发生错误");
                return false;
            }
            tContNo = tLCGrpContDB.getGrpContNo();

            LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
            tLJAPayGrpDB.setPayNo(mLJAPaySchema.getPayNo());
            LJAPayGrpSet tLJAPayGrpSet = tLJAPayGrpDB.query();
            LJAPayGrpSchema aLJAPayGrpSchema = tLJAPayGrpSet.get(1);
            if (tLJAPayGrpSet.size() > 0)
            {
                tPayToDay = aLJAPayGrpSchema.getCurPayToDate();
            }
            else
            {
                tPayToDay = "";
            }
            tEndorNo = aLJAGetEndorseSchema.getEndorsementNo();
        }
        if (tLJAPayDB.getIncomeType().equals("13"))
        {
            LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
            // tLJAGetEndorseDB.setActuGetNo(mLJAPaySchema.getPayNo());
            String endorseSQL = " Select * from ljagetendorse where actugetno in ( "
                    + " Select Btactuno from ljaedorbaldetail where actuno in ( "
                    + " Select getnoticeno from LJAPay where incomeno='"
                    + tLJAPayDB.getIncomeNo() + "' and incometype ='13')) ";

            LJAGetEndorseSet tLJAGetEndorseSet = tLJAGetEndorseDB
                    .executeQuery(endorseSQL);
            LJAGetEndorseSchema aLJAGetEndorseSchema = tLJAGetEndorseSet.get(1);

            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(aLJAGetEndorseSchema.getGrpContNo());
            if (!tLCGrpContDB.getInfo())
            {
                mErrors.copyAllErrors(tLCGrpContDB.mErrors);
                buildError("outputXML", "在取得LCGrpCont的数据时发生错误");
                return false;
            }
            tContNo = tLCGrpContDB.getGrpContNo();

            LJSPayBDB tLJSPayBDB = new LJSPayBDB();
            tLJSPayBDB.setGetNoticeNo(tLJAPayDB.getGetNoticeNo());
            LJSPayBSet tLJSPayBSet = tLJSPayBDB.query();
            LJSPayBSchema aLJSPayBSchema = tLJSPayBSet.get(1);
            tPayToDay = aLJSPayBSchema.getPayDate();
            tEndorNo = tLJAPayDB.getIncomeNo();
        }
        if (tLJAPayDB.getIncomeType().equals("15"))
        {
            if (mLJAPayPersonSet.size() >= 2)
            {
                tContNo = tLJAPayDB.getIncomeNo();
            }
            else
            {
                tContNo = mLJAPayPersonSet.get(1).getContNo();
            }
        }
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLJAPayDB.getManageCom());
        if (!tLDComDB.getInfo())
        {
            mErrors.copyAllErrors(tLDComDB.mErrors);
            buildError("outputXML", "在取得LDCom的数据时发生错误");
            return false;
        }
        if (tLJAPayDB.getIncomeType().equals("15"))
        {
            if (controlPrt(mNewLJAPayPersonSet) == false)
            {
                buildError("outputXML", "在准备打印数据时发生错误");
                return false;
            }
        }
        else
        {
            if (controlPrt(tLJAPayDB.getSchema()) == false)
            {
                buildError("outputXML", "在准备打印数据时发生错误");
                return false;
            }
        }

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        tManageCom = tLJAPayDB.getManageCom().substring(0, 4);
        if (tManageCom.equals("8637"))
        {
            xmlexport.createDocument("FeeInvoiceSD.vts", "printer"); //最好紧接着就初始化xml文档
        }
        else if (tManageCom.equals("8632"))
        {
            xmlexport.createDocument("FeeInvoiceJS.vts", "printer"); //最好紧接着就初始化xml文档
        }
        else if (tManageCom.equals("8633"))
        {
            xmlexport.createDocument("FeeInvoiceZJ.vts", "printer"); //最好紧接着就初始化xml文档
        }
        else if (tManageCom.equals("8621"))
        {
            xmlexport.createDocument("FeeInvoiceLN.vts", "printer"); //最好紧接着就初始化xml文档
        }
        else if (tManageCom.equals("8635"))
        {
            xmlexport.createDocument("FeeInvoiceFJ.vts", "printer"); //最好紧接着就初始化xml文档
        }
        else if (tManageCom.equals("8644"))
        {
            xmlexport.createDocument("FeeInvoiceGD.vts", "printer"); //最好紧接着就初始化xml文档
        }
        else if (tManageCom.equals("8641"))
        {
            //获取长无忧险的主险和附加险
            String sql="select prem,riskcode,(select riskname from lmriskapp where riskcode=lcpol.riskcode) code,mainpolno,polno from lcpol where contno='"+tContNo+"' with ur";
            ExeSQL tESQL = new ExeSQL(); 
            SSRS tRS = tESQL.execSQL(sql);
            int flag=0;
            String mainRisk="";
            String subRisk="";
            if(tRS!=null && tRS.MaxRow>0){
               for(int index=1;index<=tRS.MaxRow;index++){
                   String tPrem=tRS.GetText(index, 1);
                   String tRiskCode=tRS.GetText(index, 2);
                   String tRiskName=tRS.GetText(index, 3);
                   String tMainRisk=tRS.GetText(index, 4);
                   String tSubRisk=tRS.GetText(index, 5);
                   if(tRiskCode!=null && (tRiskCode.equals("530301") || tRiskCode.equals("330501"))){
                       flag=1;
                   }
                   if(StrTool.cTrim(tMainRisk).equals(StrTool.cTrim(tSubRisk))){
                       if(mainRisk.equals("")){
                           mainRisk+="\t\n主险:"+tRiskName+" "+tPrem+"元";
                       }else{
                           mainRisk+="\t\n\t"+tRiskName+" "+tPrem+"元";
                       }
                   }else{
                       if(subRisk.equals("")){
                           subRisk+="\t\n附加险:"+tRiskName+" "+tPrem+"元";
                       }else{
                           subRisk+="\t\n\t"+tRiskName+" "+tPrem+"元";
                       }
                   }
               }
            }
            if(flag==1){
                if(riskName.substring(riskName.length()-1).equals("等")){
                    riskName=riskName.substring(0,riskName.length()-1);
                }
                riskName+=mainRisk+subRisk;
            }
            System.out.println(riskName);
            xmlexport.createDocument("FeeInvoiceHN.vts", "printer"); //最好紧接着就初始化xml文档
        }
        else
        {
            xmlexport.createDocument("FeeInvoice.vts", "printer"); //最好紧接着就初始化xml文档
        }
        texttag.add("XSumMoney", "￥" + format(sumMoney));
        texttag.add("AppntName", appntName);
        texttag.add("RiskName", riskName);
        texttag.add("ContNo", tContNo);
        texttag.add("PayToDay", tPayToDay);
        if (!StrTool.cTrim(tEndorNo).equals("") && (tManageCom.equals("8637")))
        {
            texttag.add("EndorNo", "批单号：" + tEndorNo);
        }
        else if ((!StrTool.cTrim(tEndorNo).equals("") && (tManageCom
                .equals("8632"))))
        {
            texttag.add("EndorNo", "批单号：" + tEndorNo);
        }
        else
        {
            texttag.add("EndorNo", tEndorNo);
        }
        if (!StrTool.cTrim(remark).equals("") && (tManageCom.equals("8637")))
        {
            texttag.add("Remark", "批注：" + remark);
        }
        else if ((!StrTool.cTrim(remark).equals("") && (tManageCom
                .equals("8632"))))
        {
            texttag.add("Remark", "批注：" + remark);
        }
        else
        {
            texttag.add("Remark", remark);
        }
        texttag.add("HPerson", hPerson);
        texttag.add("CPerson", cPerson);
        texttag.add("AgentCode", tAgentNo);
        texttag.add("AgentGroup", tBranchattr);
        texttag.add("State", State);
        texttag.add("AgentName", AgentName);
        texttag.add("AgentGroupName", AgentGroupName);
        texttag.add("ManageCom", tLJAPayDB.getManageCom());

        if (tManageCom.equals("8611") || tManageCom.equals("8612"))
        {
            texttag.add("DSumMoney", DSumMoney);
        }
        else
        {
            texttag.add("DSumMoney", "人民币 " + DSumMoney);
            texttag.add("Add", tLDComDB.getLetterServicePostAddress());
            texttag.add("Tel", tLDComDB.getServicePhone());
        }
        texttag.add("Com", tLDComDB.getName());
        texttag.add("TaxNo", tLDComDB.getTaxRegistryNo());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        texttag.add("Today", sdf.format(new Date()));

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        System.out.println("add : " + tLDComDB.getLetterServicePostAddress());
        xmlexport.outputDocumentToFile("D:\\", "fapiao");
        mResult.clear();
        mResult.addElement(xmlexport);
        mResult.addElement(mLOPRTManager2Schema);
        mResult.addElement(mLOPRTManager2Set);

        // 记录发票打印信息
        try
        {
            if (mGlobalInput.ManageCom.length() >= 4
                    && ("8621".equals(mGlobalInput.ManageCom.substring(0, 4))
                    ||"8641".equals(mGlobalInput.ManageCom.substring(0, 4))))
            {
                mLOPRTInvoiceManagerSchema.setComCode(mGlobalInput.ManageCom);
                mLOPRTInvoiceManagerSchema.setInvoiceNo(mInvoiceNo);
                mLOPRTInvoiceManagerSchema.setContNo(tContNo);
                mLOPRTInvoiceManagerSchema.setEndorNo(tEndorNo);
                mLOPRTInvoiceManagerSchema.setXSumMoney(sumMoney);
                mLOPRTInvoiceManagerSchema.setPayerName(appntName);
                //        mLOPRTInvoiceManagerSchema.setPayerAddr("辽宁");
                mLOPRTInvoiceManagerSchema.setOpdate(PubFun.getCurrentDate());

                VData tVData = new VData();
                tVData.add(mGlobalInput);
                tVData.add(mLOPRTInvoiceManagerSchema);

                FFInvoicePrtManagerBL tFFInvoicePrtManagerBL = new FFInvoicePrtManagerBL();
                if (!tFFInvoicePrtManagerBL.submitData(tVData, "INSERT"))
                {
                    System.out.println("Invoice print info save failed...");
                }
                else
                {
                    System.out.println("Invoice print info save successed...");
                }
            }
        }
        catch (Exception ex)
        {
            System.out.println("Invoice print info save failed...");
            CError cError = new CError();
            cError.moduleName = "BriefFeeF1PBL";
            cError.functionName = "FFInvoicePrtManagerBL.submit";
            cError.errorMessage = "发票打印信息保存失败";
            this.mErrors.addOneError(cError);
        }

        return true;

    }

    private boolean getPrintData2()
    {
        /*     int i;
         String PolNo = "";
         LJAPayDB tLJAPayDB = new LJAPayDB();
         tLJAPayDB.setPayNo(mLOPRTManager2Schema.getOtherNo());
         if (!tLJAPayDB.getInfo())
         {
         mErrors.copyAllErrors(tLJAPayDB.mErrors);
         buildError("outputXML", "在取得LJAPay的数据时发生错误");
         return false;
         }
         mLJAPaySchema.setSchema(tLJAPayDB.getSchema());
         String DSumMoney = "";
         double SumMoney = tLJAPayDB.getSumActuPayMoney();
         DSumMoney = PubFun.getChnMoney(SumMoney);

         LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
         tLJAPayPersonDB.setPayNo(mLJAPaySchema.getPayNo());
         tLJAPayPersonDB.setPayType("ZC");
         LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
         tLJAPayPersonSet.set(tLJAPayPersonDB.query());
         ListTable tlistTable = new ListTable();
         String strArr[] = null;
         tlistTable.setName("FEE");
         LJAPayPersonSchema aLJAPayPersonSchema = null;
         for (i = 0; i < tLJAPayPersonSet.size(); i++)
         {

         aLJAPayPersonSchema = new LJAPayPersonSchema();
         aLJAPayPersonSchema.setSchema(tLJAPayPersonSet.get(i + 1));
         LCPolDB tLCPolDB = new LCPolDB();
         tLCPolDB.setPolNo(aLJAPayPersonSchema.getPolNo());
         tLCPolDB.setMainPolNo(aLJAPayPersonSchema.getPolNo());
         if (tLCPolDB.getCount() == 1)
         {
         PolNo = tLCPolDB.getPolNo();
         }
         strArr = new String[3];
         LMRiskDB tLMRiskDB = new LMRiskDB();
         tLMRiskDB.setRiskCode(aLJAPayPersonSchema.getRiskCode());
         if (!tLMRiskDB.getInfo())
         {
         mErrors.copyAllErrors(tLMRiskDB.mErrors);
         buildError("outputXML", "在取得LMRisk的数据时发生错误");
         return false;
         }
         strArr[0] = tLMRiskDB.getRiskName();
         LDCodeDB tLDCodeDB = new LDCodeDB();
         tLDCodeDB.setCodeType("payintv");
         tLDCodeDB.setCode(String.valueOf(aLJAPayPersonSchema.getPayIntv()));
         if (!tLDCodeDB.getInfo())
         {
         mErrors.copyAllErrors(tLDCodeDB.mErrors);
         buildError("outputXML", "在取得LDCode的数据时发生错误");
         return false;
         }
         strArr[1] = tLDCodeDB.getCodeName();
         strArr[2] = String.valueOf(aLJAPayPersonSchema.getSumActuPayMoney());
         tlistTable.add(strArr);
         }
         strArr = new String[3];
         strArr[0] = "Risk";
         strArr[1] = "Mode";
         strArr[2] = "Money";

         LCPolDB tLCPolDB = new LCPolDB();
         tLCPolDB.setPolNo(PolNo);
         if (!tLCPolDB.getInfo())
         {
         mErrors.copyAllErrors(tLCPolDB.mErrors);
         buildError("outputXML", "在取得LCPol的数据时发生错误");
         return false;
         }

         LAAgentDB tLAAgentDB = new LAAgentDB();
         tLAAgentDB.setAgentCode(tLCPolDB.getAgentCode());
         if (!tLAAgentDB.getInfo())
         {
         mErrors.copyAllErrors(tLAAgentDB.mErrors);
         buildError("outputXML", "在取得LAAgent的数据时发生错误");
         return false;
         }

         if (controlPrt(tLJAPayDB.getSchema()) == false)
         {
         buildError("outputXML", "在准备打印数据时发生错误");
         return false;
         }

         String strPaytoDate = tLCPolDB.getPaytoDate();

         strPaytoDate = strPaytoDate.substring(0, 4) + "年"
         + strPaytoDate.substring(5, 7) + "月"
         + strPaytoDate.substring(8, 10) + "日";

         TextTag texttag = new TextTag(); //新建一个TextTag的实例
         XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
         xmlexport.createDocument("FeeInvoice.vts", "printer"); //最好紧接着就初始化xml文档
         texttag.add("PayNo", tLJAPayDB.getPayNo());
         texttag.add("XSumMoney", tLJAPayDB.getSumActuPayMoney());
         texttag.add("DSumMoney", DSumMoney);
         texttag.add("AppntName", tLCPolDB.getAppntName());
         texttag.add("PolNo", tLCPolDB.getPolNo());
         texttag.add("PaytoDate", strPaytoDate);
         //texttag.add("Handler",tLCPolDB.getHandler());
         texttag.add("Handler", mGlobalInput.Operator);
         texttag.add("AgentCode", tLCPolDB.getAgentCode());
         texttag.add("AgentName", tLAAgentDB.getName());

         SimpleDateFormat sdf = new SimpleDateFormat("yyyy  MM  dd");
         texttag.add("Today", sdf.format(new Date()));

         if (texttag.size() > 0)
         {
         xmlexport.addTextTag(texttag);
         }
         xmlexport.addListTable(tlistTable, strArr);
         mResult.clear();
         mResult.addElement(xmlexport);
         mResult.addElement(mLOPRTManager2Schema);*/
        return true;

    }

    public boolean saveData()
    {
        System.out.print("update print:" + mLOPRTManager2Schema.getPrtSeq());
        LOPRTManager2DB tLOPRTManager2DB = new LOPRTManager2DB();
        tLOPRTManager2DB.setPrtSeq(mLOPRTManager2Schema.getPrtSeq());
        if (tLOPRTManager2DB.getInfo() == false)
        {
            CError tError = new CError();
            tError.moduleName = "outputXML";
            tError.functionName = "getPrintData";
            tError.errorMessage = "打印数据准备失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLOPRTManager2Schema.setSchema(tLOPRTManager2DB.getSchema());
        mLOPRTManager2Schema.setStateFlag("1");
        tLOPRTManager2DB = new LOPRTManager2DB();
        tLOPRTManager2DB.setSchema(mLOPRTManager2Schema);
        tLOPRTManager2DB.update();
        System.out.print("update print");

        return true;
    }

    public boolean controlPrt(LJAPaySchema tLJAPaySchema)
    {
        //添加打印控制--预览时插入打印管理数据，打印时更新打印数据状态为已打
        LOPRTManager2DB tLOPRTManager2DB = new LOPRTManager2DB();
        LOPRTManager2Set tLOPRTManager2Set = new LOPRTManager2Set();
        tLOPRTManager2DB.setOtherNo(tLJAPaySchema.getPayNo());
        tLOPRTManager2DB.setOtherNoType("05"); //05 --- 交费收据号码
        tLOPRTManager2DB.setCode(PrintManagerBL.CODE_INVOICE);
        tLOPRTManager2Set = tLOPRTManager2DB.query();
        if (tLOPRTManager2Set.size() == 0)
        {
            LOPRTManager2Schema tLOPRTManager2Schema = getPrintData(tLJAPaySchema);
            if (tLOPRTManager2Schema == null)
            {
                CError tError = new CError();
                tError.moduleName = "outputXML";
                tError.functionName = "getPrintData";
                tError.errorMessage = "打印数据准备失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            else
            {
                tLOPRTManager2DB = new LOPRTManager2DB();
                tLOPRTManager2DB.setSchema(tLOPRTManager2Schema);
                if (tLOPRTManager2DB.insert() == false)
                {
                    this.mErrors.copyAllErrors(tLOPRTManager2DB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "outputXML";
                    tError.functionName = "getPrintData";
                    tError.errorMessage = "打印数据保存失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            mLOPRTManager2Schema.setSchema(tLOPRTManager2Schema);
        }
        else
        {
            mLOPRTManager2Schema.setSchema(tLOPRTManager2Set.get(1));
        }

        return true;
    }

    public boolean controlPrt(LJAPayPersonSet tLJAPayPersonSet)
    {

        String tSql = " select * from LOPRTManager2 where OtherNo in "
                + mPayContr + " and OtherNoType='05' and code ='"
                + PrintManagerBL.CODE_INVOICE + "'";
        //添加打印控制--预览时插入打印管理数据，打印时更新打印数据状态为已打
        LOPRTManager2DB tLOPRTManager2DB = new LOPRTManager2DB();
        LOPRTManager2DBSet tLOPRTManager2DBSet = new LOPRTManager2DBSet();
        LOPRTManager2Set tLOPRTManager2Set = new LOPRTManager2Set();
        tLOPRTManager2Set = tLOPRTManager2DB.executeQuery(tSql);
        if (tLOPRTManager2Set.size() == 0)
        {
            tLOPRTManager2Set = getPrintData(tLJAPayPersonSet);
            if (tLOPRTManager2Set == null)
            {
                CError tError = new CError();
                tError.moduleName = "outputXML";
                tError.functionName = "getPrintData";
                tError.errorMessage = "打印数据准备失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            else
            {
                tLOPRTManager2DBSet.set(tLOPRTManager2Set);
                if (tLOPRTManager2DBSet.insert() == false)
                {
                    this.mErrors.copyAllErrors(tLOPRTManager2DB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "outputXML";
                    tError.functionName = "getPrintData";
                    tError.errorMessage = "打印数据保存失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            mLOPRTManager2Set.set(tLOPRTManager2Set);
        }
        else
        {
            mLOPRTManager2Set.set(tLOPRTManager2Set);
        }

        return true;
    }

    public LOPRTManager2Schema getPrintData(LJAPaySchema tLJAPaySchema)
    {
        LOPRTManager2Schema tLOPRTManager2Schema = new LOPRTManager2Schema();
        try
        {
            String tLimit = PubFun.getNoLimit(tLJAPaySchema.getManageCom());
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            tLOPRTManager2Schema.setPrtSeq(prtSeqNo);
            tLOPRTManager2Schema.setOtherNo(tLJAPaySchema.getPayNo());
            tLOPRTManager2Schema.setOtherNoType("05");
            tLOPRTManager2Schema.setCode(PrintManagerBL.CODE_INVOICE);
            tLOPRTManager2Schema.setManageCom(tLJAPaySchema.getManageCom());
            tLOPRTManager2Schema.setAgentCode(tLJAPaySchema.getAgentCode());
            tLOPRTManager2Schema.setReqCom(tLJAPaySchema.getManageCom());
            tLOPRTManager2Schema.setReqOperator(tLJAPaySchema.getOperator());
            tLOPRTManager2Schema.setExeOperator(this.mGlobalInput.Operator);
            tLOPRTManager2Schema.setStandbyFlag4(this.mInvoiceNo);
            tLOPRTManager2Schema.setPrtType("0");
            tLOPRTManager2Schema.setStateFlag("0");
            tLOPRTManager2Schema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManager2Schema.setMakeTime(PubFun.getCurrentTime());
        }
        catch (Exception ex)
        {
            return null;
        }
        return tLOPRTManager2Schema;
    }

    public LOPRTManager2Set getPrintData(LJAPayPersonSet tLJAPayPersonSet)
    {

        LOPRTManager2Set tLOPRTManager2Set = new LOPRTManager2Set();
        try
        {

            for (int i = 1; i <= tLJAPayPersonSet.size(); i++)
            {

                LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
                tLJAPayPersonSchema = tLJAPayPersonSet.get(i);
                LOPRTManager2Schema tLOPRTManager2Schema = new LOPRTManager2Schema();
                String tLimit = PubFun.getNoLimit(tLJAPayPersonSchema
                        .getManageCom());
                String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);

                tLOPRTManager2Schema.setPrtSeq(prtSeqNo);
                tLOPRTManager2Schema.setOtherNo(tLJAPayPersonSchema.getPayNo());
                tLOPRTManager2Schema.setOtherNoType("05");
                tLOPRTManager2Schema.setCode(PrintManagerBL.CODE_INVOICE);
                tLOPRTManager2Schema.setManageCom(tLJAPayPersonSchema
                        .getManageCom());
                tLOPRTManager2Schema.setAgentCode(tLJAPayPersonSchema
                        .getAgentCode());
                tLOPRTManager2Schema.setReqCom(tLJAPayPersonSchema
                        .getManageCom());
                tLOPRTManager2Schema.setStandbyFlag2("15");
                tLOPRTManager2Schema.setStandbyFlag3(mLJAPaySchema
                        .getIncomeNo());
                tLOPRTManager2Schema.setStandbyFlag4(this.mInvoiceNo);
                tLOPRTManager2Schema.setReqOperator(tLJAPayPersonSchema
                        .getOperator());
                tLOPRTManager2Schema.setExeOperator(this.mGlobalInput.Operator);
                tLOPRTManager2Schema.setStandbyFlag4("1");
                tLOPRTManager2Schema.setPrtType("0");
                tLOPRTManager2Schema.setStateFlag("0");
                tLOPRTManager2Schema.setMakeDate(PubFun.getCurrentDate());
                tLOPRTManager2Schema.setMakeTime(PubFun.getCurrentTime());
                tLOPRTManager2Set.add(tLOPRTManager2Schema);
            }

        }
        catch (Exception ex)
        {
            return null;
        }
        return tLOPRTManager2Set;
    }

    /**
     * 格式化浮点型数据
     * @param dValue double
     * @return String
     */
    private static String format(double dValue)
    {
        return new DecimalFormat("0.00").format(dValue);
    }

    public static void main(String[] args)
    {
//        FeeInvoiceF1PBL FeeInvoiceF1PBL1 = new FeeInvoiceF1PBL();
//        
//        String startNo   = "001";  
//        String certifyCode   = "002";  
//        
//        String appntName ="appntName";
//        String riskName ="";  
//        String hPerson ="";
//        String cPerson ="";
//        String remark ="";  
//        VData tVData = new VData();
//        TransferData tTransferData = new TransferData();
//        tTransferData.setNameAndValue("AppntName",appntName);
//        tTransferData.setNameAndValue("RiskName",riskName);     
//        tTransferData.setNameAndValue("HPerson",hPerson);
//        tTransferData.setNameAndValue("CPerson",cPerson);
//        tTransferData.setNameAndValue("Remark",remark);
//        tTransferData.setNameAndValue("invoiceNo",startNo);
//        LJAPayDB tLJAPayDB=new LJAPayDB();
//        tLJAPayDB.setPayNo("32000311823");
//        
//        tVData.addElement(tLJAPayDB.query().get(1));
//        tVData.addElement(new GlobalInput());
//        tVData.addElement(tTransferData);
//        FeeInvoiceF1PBL1.submitData(tVData, "CONFIRM");
    }
}
