package com.sinosoft.lis.f1print;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author XX
 * @version 1.0
 */
import java.text.DecimalFormat;

import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

class LLSurveyBL {
	public LLSurveyBL() {

	}

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 全局变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mManageCom = "";

	private String mStartDate = "";

	private String mEndDate = "";

	private String mOperator = "";

	private String tCurrentDate = "";

	private VData mInputData = new VData();

	private String mOperate = "";

	private ListTable mListTable = new ListTable();

	private TransferData mTransferData = new TransferData();

	private XmlExport mXmlExport = null;

	private String mManageComNam = "";

	private String mFileNameB = "";

	private String mMakeDate = "";
	
	private DecimalFormat mDF = new DecimalFormat("0.##");

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		mOperate = cOperate;
		mInputData = (VData) cInputData;
		if (mOperate.equals("")) {
			this.bulidErrorB("submitData", "数据不完整");
			return false;
		}

		if (!mOperate.equals("PRINT")) {
			this.bulidErrorB("submitData", "数据不完整");
			return false;
		}

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(mInputData)) {
			return false;
		}
		System.out.println("BBBXXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
				+ this.mManageCom);

		// 进行数据查询
		if (!queryData()) {
			return false;
		} else {
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("tFileNameB", mFileNameB);
			tTransferData.setNameAndValue("tMakeDate", mMakeDate);
			tTransferData.setNameAndValue("tOperator", mOperator);
			LLPrintSave tLLPrintSave = new LLPrintSave();
			VData tVData = new VData();
			tVData.addElement(tTransferData);
			if (!tLLPrintSave.submitData(tVData, "")) {
				this.bulidErrorB("LLPrintSave-->submitData", "数据不完整");
				return false;
			}
		}

		System.out.println("dayinchenggong1232121212121");

		return true;
	}

	private void bulidErrorB(String cFunction, String cErrorMsg) {

		CError tCError = new CError();

		tCError.moduleName = "LLSurveyBL";
		tCError.functionName = cFunction;
		tCError.errorMessage = cErrorMsg;

		this.mErrors.addOneError(tCError);

	}

	/**
	 * 取得传入的数据 如果没有传入管理机构和起止如期则查全部机构全年的信息
	 * 
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		// tCurrentDate = mPubFun.getCurrentDate();
		try {
			mGlobalInput.setSchema((GlobalInput) cInputData
					.getObjectByObjectName("GlobalInput", 0));
			mTransferData = (TransferData) cInputData.getObjectByObjectName(
					"TransferData", 0);
			// 页面传入的数据 三个
			mOperator = (String) mTransferData.getValueByName("tOperator");
			mFileNameB = (String) mTransferData.getValueByName("tFileNameB");
			this.mManageCom = (String) mTransferData
					.getValueByName("tManageCom");

			System.out.println("XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
					+ this.mManageCom);
			this.mStartDate = (String) mTransferData
					.getValueByName("tStartDate");
			this.mEndDate = (String) mTransferData.getValueByName("tEndDate");
			if (mManageCom == null || mManageCom.equals("")) {
				this.mManageCom = "86";
			}
			if (mStartDate == null || mStartDate.equals("")) {
				this.mStartDate = getYear(tCurrentDate) + "-01-01";
			}
			if (mEndDate == null || mEndDate.equals("")) {
				this.mEndDate = getYear(tCurrentDate) + "-12-31";
			}

			System.out
					.println("XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXXLLLLLLLLLLLLLLLLLLLL"
							+ mManageCom);
		} catch (Exception ex) {
			this.mErrors.addOneError("");
			return false;
		}
		return true;
	}

	private boolean queryData() {
		TextTag tTextTag = new TextTag();
		mXmlExport = new XmlExport();
		// 设置模版名称
		mXmlExport.createDocument("LLSurvey.vts", "printer");

		String tMakeDate = "";
		tMakeDate = PubFun.getCurrentDate();

		System.out.print("dayin252");
		String sql = "select name from ldcom where comcode='" + mManageCom
				+ "'";
		ExeSQL tExeSQL = new ExeSQL();
		String mManageComName = tExeSQL.getOneValue(sql);
		tTextTag.add("ManageCom", mManageComName);
		tTextTag.add("StartDate", mStartDate);
		tTextTag.add("EndDate", mEndDate);
		tTextTag.add("MakeDate", tMakeDate);
		tTextTag.add("operator", mOperator);
		mMakeDate = tMakeDate;
		System.out.println("1212121" + tMakeDate);
		if (tTextTag.size() < 1) {
			return false;
		}
		mXmlExport.addTextTag(tTextTag);
		String[] title = { "", "", "", "", "", "", "", "", "", "", "", "", "",
				"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
				"", "", "", "", "", "", "", "", "" };
		mListTable.setName("ENDOR");
		if (!getDataList()) {
			return false;
		}
		if (mListTable == null || mListTable.equals("")) {
			bulidErrorB("getDataList", "没有符合条件的信息!");
			return false;
		}

		System.out.println("111");
		mXmlExport.addListTable(mListTable, title);
		System.out.println("121");
		mXmlExport.outputDocumentToFile("e:\\", "new1");
		this.mResult.clear();
		mResult.addElement(mXmlExport);

		return true;
	}

	private String getYear(String pmDate) {
		String mYear = "";
		String tSQL = "";
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(tSQL);
		mYear = tSSRS.GetText(1, 1);
		return mYear;
	}

	private boolean getDataList() {

		SSRS tSSRS = new SSRS();
		String sql = " select c.caseno,substr(a.surveyno,18,1), "
				+ " (select name from ldcom where comcode = b.inqdept), "
				+ " (select name from ldcom where comcode = c.mngcom), "
				+ " (case when d.grpcontno='00000000000000000000' then d.contno else d.grpcontno end), "
				+ " getUniteCode(d.agentcode),(select name from laagent where agentcode=d.agentcode fetch first 1 rows only), "
				+ " d.agentgroup,(select name from labranchgroup where agentgroup=d.agentgroup fetch first 1 rows only), "
				+ " d.appntno,d.appntname,c.customerno,c.customername,d.riskcode, "
				+ " (case  when e.riskprop='I' then '个险' else '团险' end),d.polno, "
				+ " c.endcasedate,nvl(sum(realpay),0), "
				+ " (select givetypedesc from llclaim where caseno=c.caseno fetch first 1 rows only), "
				+ " (select n.accdate from llcaserela m,llsubreport n where m.subrptno=n.subrptno and m.caseno=c.caseno fetch first 1 rows only), "
				+ " a.surveyoperator,a.surveystartdate,a.surveyenddate, "
				+ " (select nvl(sum(inqfee),0) from llinqfeesta where surveyno=a.surveyno and confer is not null and confdate is not null), "
				+ " (select nvl(sum(indfee.feesum),0) from llcaseindfee indfee where indfee.otherno=a.otherno "
				+ " and exists (select 1 from llindirectfee where feedate = indfee.feedate and uwstate='1' ) ), "
				+ " '1',b.inqdept,c.mngcom "
				+ " from llsurvey a,llinqapply b,llcase c,llclaimpolicy d,lmriskapp e "
				+ " where a.surveyno=b.surveyno "
				+ " and a.otherno=c.caseno and a.otherno=d.caseno and d.riskcode=e.riskcode "
				+ " and c.rgtstate in ('09','11','12') "
				+ " and c.endcasedate between '"
				+ mStartDate
				+ "' and '"
				+ mEndDate
				+ "' "
				+ " and b.inqdept like '"
				+ mManageCom
				+ "%' "
				+ " group by a.otherno,c.caseno,a.surveyno,b.inqdept,c.mngcom,d.grpcontno,d.contno,d.agentcode,d.agentgroup,d.appntno, "
				+ " d.appntname,c.customerno,c.customername,d.riskcode,d.polno,e.riskprop,c.endcasedate, "
				+ " a.surveyoperator,a.surveystartdate,a.surveyenddate "
				+ " union all "
				+ " select c.caseno,substr(a.surveyno,18,1), "
				+ " (select name from ldcom where comcode = b.inqdept), "
				+ " (select name from ldcom where comcode = c.mngcom), "
				+ " '','','','','','','',c.customerno,c.customername,'','','', "
				+ " c.cancledate,0,'', "
				+ " (select n.accdate from llcaserela m,llsubreport n where m.subrptno=n.subrptno and m.caseno=c.caseno fetch first 1 rows only), "
				+ " a.surveyoperator,a.surveystartdate,a.surveyenddate, "
				+ " (select nvl(sum(inqfee),0) from llinqfeesta where surveyno=a.surveyno and confer is not null and confdate is not null), "
				+ " (select nvl(sum(indfee.feesum),0) from llcaseindfee indfee where indfee.otherno=a.otherno "
				+ " and exists (select 1 from llindirectfee where feedate = indfee.feedate and uwstate='1' ) ), "
				+ " '2',b.inqdept,c.mngcom "
				+ " from llsurvey a,llinqapply b,llcase c "
				+ " where a.surveyno=b.surveyno and a.otherno=c.caseno and c.rgtstate ='14' "
				+ " and c.cancledate between '"
				+ mStartDate
				+ "' and '"
				+ mEndDate
				+ "' "
				+ " and b.inqdept like '"
				+ mManageCom
				+ "%'"
				+ " union all "
				+ " select c.consultno,substr(a.surveyno,18,1), "
				+ " (select name from ldcom where comcode = b.inqdept), "
				+ " (select name from ldcom where comcode = c.mngcom), "
				+ " '','','','','','','',c.customerno,c.customername,'','','', "
				+ " c.makedate,0,'', "
				+ " (select n.accdate from llaskrela m,llsubreport n where m.subrptno=n.subrptno and m.consultno=c.consultno fetch first 1 rows only), "
				+ " a.surveyoperator,a.surveystartdate,a.surveyenddate, "
				+ " (select nvl(sum(inqfee),0) from llinqfeesta where surveyno=a.surveyno and confer is not null and confdate is not null), "
				+ " (select nvl(sum(indfee.feesum),0) from llcaseindfee indfee where indfee.otherno=a.otherno "
				+ " and exists (select 1 from llindirectfee where feedate = indfee.feedate and uwstate='1' ) ), "
				+ " '3',b.inqdept,c.mngcom " + " from llsurvey a,llinqapply b,llconsult c "
				+ " where a.surveyno=b.surveyno "
				+ " and a.otherno=c.consultno" 
				//2722机构调查明细报表的提取范围 （调查报告复核）
				+" and a.surveyFlag='3' "
				+"and c.makedate between '"
				+ mStartDate + "' and '" + mEndDate + "' "
				+ " and b.inqdept like '" + mManageCom + "%' " + " with ur ";

		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(sql);

		String tCaseNo = "";
		String tPolNo = "";

		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			String ListInfo[] = new String[38];
			tCaseNo = tSSRS.GetText(i, 1);
			System.out.println(tCaseNo);

			for (int m = 0; m < 38; m++) {
				ListInfo[m] = "";
			}
			//2983
			if(tCaseNo.equals("C3605160509000002")){
				// 案件号
				ListInfo[0] = "c3605160509000002";
			}else{
			// 案件号
			ListInfo[0] = tCaseNo;
			}
			// 调查序号
			ListInfo[1] = tSSRS.GetText(i, 2);
			// 调查机构
			ListInfo[2] = tSSRS.GetText(i, 3);
			// 案件机构
			ListInfo[3] = tSSRS.GetText(i, 4);
			// 保单号
			ListInfo[4] = tSSRS.GetText(i, 5);
			// 业务员编码
			ListInfo[5] = tSSRS.GetText(i, 6);
			// 业务员姓名
			ListInfo[6] = tSSRS.GetText(i, 7);
			// 所属营业单位编码
			ListInfo[7] = tSSRS.GetText(i, 8);
			// 所属营业部
			ListInfo[8] = tSSRS.GetText(i, 9);
			// 投保人客户号
			ListInfo[11] = tSSRS.GetText(i, 10);
			// 投保人姓名
			ListInfo[12] = tSSRS.GetText(i, 11);
			// 被保险人客户号
			ListInfo[13] = tSSRS.GetText(i, 12);
			// 被保险人姓名
			ListInfo[14] = tSSRS.GetText(i, 13);
			// 险种类型
			ListInfo[15] = tSSRS.GetText(i, 15);
			// 险种编码
			ListInfo[16] = tSSRS.GetText(i, 14);

			tPolNo = tSSRS.GetText(i, 16);

			// 结案、撤件日期
			ListInfo[20] = tSSRS.GetText(i, 17);
			// 赔款
			ListInfo[21] = mDF.format(Double.parseDouble(tSSRS.GetText(i, 18)));
			// 赔付结论
			ListInfo[22] = tSSRS.GetText(i, 19);
			// 出险日期
			ListInfo[30] = tSSRS.GetText(i, 20);
			// 调查人
			ListInfo[31] = tSSRS.GetText(i, 21);
			// 调查开始时间
			ListInfo[32] = tSSRS.GetText(i, 22);
			// 调查结束时间
			ListInfo[33] = tSSRS.GetText(i, 23);
			// 直接调查费
			ListInfo[34] = mDF.format(Double.parseDouble(tSSRS.GetText(i, 24)));
			// 间接调查费
			ListInfo[35] = mDF.format(Double.parseDouble(tSSRS.GetText(i, 25)));
			//调查机构编码
			ListInfo[36] = tSSRS.GetText(i, 27);
			//案件机构编码
			ListInfo[37] = tSSRS.GetText(i, 28);
			

			if ("1".equals(tSSRS.GetText(i, 26)) && !"".equals(tPolNo)
					&& tPolNo != null) {
				String tPolSQL = " select a.agentcom, "
						+ " (select name from lacom where agentcom = a.agentcom fetch first 1 rows only), "
						+ " a.prem,CODENAME('payintv',char(a.payintv)),a.cvalidate "
						+ " from lcpol a "
						+ " where a.polno='"
						+ tPolNo
						+ "' "
						+ " union all "
						+ " select a.agentcom, "
						+ " (select name from lacom where agentcom = a.agentcom fetch first 1 rows only), "
						+ " a.prem,CODENAME('payintv',char(a.payintv)),a.cvalidate "
						+ " from lbpol a " + " where a.polno='" + tPolNo + "' "
						+ " with ur";
				System.out.println(tPolSQL);
				SSRS tPolSSRS = tExeSQL.execSQL(tPolSQL);
				if (tPolSSRS.getMaxRow() > 0) {
					// 中介机构编码
					ListInfo[9] = tPolSSRS.GetText(1, 1);
					// 中介机构名称
					ListInfo[10] = tPolSSRS.GetText(1, 2);
					// 首期保费
					ListInfo[17] = mDF.format(Double.parseDouble(tPolSSRS.GetText(1, 3)));
					// 缴费频次
					ListInfo[18] = tPolSSRS.GetText(1, 4);
					// 生效日期
					ListInfo[19] = tPolSSRS.GetText(1, 5);
				}
			} else {
				ListInfo[9] = "";
				ListInfo[10] = "";
				ListInfo[17] = "";
				ListInfo[18] = "";
				ListInfo[19] = "";
			}

			if (!"3".equals(tSSRS.GetText(i, 26)) && !"".equals(tCaseNo)
					&& tCaseNo != null) {
				String tHospSQL = "select hospitalcode,hospitalname,nvl(sum(realhospdate) over (Partition by caseno),0) from llfeemain where caseno='"
						+ tCaseNo + "' fetch first 1 rows only with ur";
				SSRS tHospSSRS = tExeSQL.execSQL(tHospSQL);
				if (tHospSSRS.getMaxRow() > 0) {
					// 医院编码
					ListInfo[23] = tHospSSRS.GetText(1, 1);
					// 医院名称
					ListInfo[24] = tHospSSRS.GetText(1, 2);
					// 住院天数
					ListInfo[25] = tHospSSRS.GetText(1, 3);
				}

				String tAccSQL = "select code,name from llaccident where caseno='"
						+ tCaseNo + "' fetch first 1 rows only with ur";
				SSRS tAccSSRS = tExeSQL.execSQL(tAccSQL);

				if (tAccSSRS.getMaxRow() > 0) {
					// 意外编码
					ListInfo[26] = tAccSSRS.GetText(1, 1);
					// 意外名称
					ListInfo[27] = tAccSSRS.GetText(1, 2);
				}

				String tCureSQL = "select diseasecode,diseasename from llcasecure where caseno='"
						+ tCaseNo + "' fetch first 1 rows only with ur";
				SSRS tCureSSRS = tExeSQL.execSQL(tCureSQL);
				if (tCureSSRS.getMaxRow() > 0) {
					// 疾病编码
					ListInfo[28] = tCureSSRS.GetText(1, 1);
					// 疾病名称
					ListInfo[29] = tCureSSRS.GetText(1, 2);
				}
			} else {
				ListInfo[23] = "";
				ListInfo[24] = "";
				ListInfo[25] = "";
				ListInfo[26] = "";
				ListInfo[27] = "";
				ListInfo[28] = "";
				ListInfo[29] = "";
			}

			mListTable.add(ListInfo);
		}

		return true;
	}

	/**
	 * @return VData
	 */
	public VData getResult() {
		return mResult;
	}

	public static void main(String[] args) {
		LLSurveyBL ls = new LLSurveyBL();
	}

}
