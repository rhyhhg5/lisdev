package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author MengWt
 * @version 1.0
 */
import java.util.Calendar;
import java.util.GregorianCalendar;

import utils.system;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;

public class ARStatisticCSVBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();
	private CSVFileWrite mCSVFileWrite = null;

	private String mFilePath = "";

	private String mFileName = "";

    public ARStatisticCSVBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));

        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        if (mTransferData == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        mFilePath = (String) mTransferData.getValueByName("realpath");
        
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "WorkEfficiencyBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {
    	
        VData result = new VData();
        //统计的参数
        String OperatorManagecom = (String) mTransferData.getValueByName(
                "OperatorManagecom");

        String Managecom = (String) mTransferData.getValueByName("Managecom");

        String StartDate = (String) mTransferData.getValueByName("StartDate");

        String EndDate = (String) mTransferData.getValueByName("EndDate");

        String QYType = (String) mTransferData.getValueByName("QYType");

        String QYType_ch = (String) mTransferData.getValueByName("QYType_ch");

        String MakeDate = PubFun.getCurrentDate();
        Calendar cal = new GregorianCalendar();
        String min=String.valueOf(cal.get(Calendar.MINUTE));
        String sec=String.valueOf(cal.get(Calendar.SECOND));
        System.out.println("zhuzhu" + QYType);
        
        SSRS ttSSRS_M = new SSRS();
        String ttsql_M = "select Name from ldcom where comcode = '" +
                       OperatorManagecom +
                       "'";
        ExeSQL ttExeSQL = new ExeSQL();
        ttSSRS_M = ttExeSQL.execSQL(ttsql_M);
        String mngname=ttSSRS_M.GetText(1, 1);
        String SysDate = MakeDate;
        String[][] tTitle = new String[3][];
        mFilePath+="vtsfile/";
        
		if (QYType.equals("1")) {
			mFileName = "tbclaim_"+mGlobalInput.Operator+"_"+ min + sec;
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
			tTitle[0] = new String[] { "", "","个单投保状况明细汇总统计报表", "","" };
			tTitle[1] = new String[] { "统计部门：" + mngname, "","制表时间："+SysDate,"",
					"统计时间：" + StartDate + "至" + EndDate };
			tTitle[2] = new String[] { "机构名称", "险种代码", "险种名称","投保件数","投保保费" };
			String[] tContentType = { "String","String","String", "Number", "Number" };
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);

			if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
				return false;
			}
		}
		if (QYType.equals("2")) {
			mFileName = "hbclaim_"+mGlobalInput.Operator+"_"+ min + sec;
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
			tTitle[0] = new String[] { "","","","", "个单核保状况明细汇总统计报表", "","","","" };
			tTitle[1] = new String[] { "统计部门：" + mngname, "","","制表时间："+SysDate,"","",
					"统计时间：" + StartDate + "至" + EndDate,"","" };
			tTitle[2] = new String[] { "机构名称", "险种代码", "险种名称","契撤件数","契撤保费","拒保件数","拒保保费","核保通过未签单件数","核保通过未签单保费" };
			String[] tContentType = { "String","String","String", "Number", "Number" , "Number", "Number", "Number", "Number"};
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
			if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
				return false;
			}
		}
		if (QYType.equals("3")) {
			mFileName = "cbclaim_"+mGlobalInput.Operator+"_"+ min + sec;
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
			tTitle[0] = new String[] { "", "","个单承保状况明细汇总统计报表", "","","" };
			tTitle[1] = new String[] { "统计部门：" + mngname, "","制表时间："+SysDate,"",
					"统计时间：" + StartDate + "至" + EndDate,"" };
			tTitle[2] = new String[] { "机构名称", "险种代码", "险种名称","承保件数","承保保费","年度承保总保费" };
			String[] tContentType = { "String","String","String", "Number", "Number", "Number" };
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);

			if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
				return false;
			}
		}
		if (QYType.equals("4")) {
			mFileName = "grptbclaim_"+mGlobalInput.Operator+"_"+ min + sec;
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
			tTitle[0] = new String[] { "", "","","团单投保状况明细汇总统计报表", "","","" };
			tTitle[1] = new String[] { "统计部门：" + mngname, "","","制表时间："+SysDate,"","" ,
					"统计时间：" + StartDate + "至" + EndDate};
			tTitle[2] = new String[] { "机构名称", "险种代码", "险种名称","投保件数","投保客户数","投保人数","投保保费" };
			String[] tContentType = { "String","String","String", "Number", "Number", "Number", "Number" };
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);

			if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
				return false;
			}
		}
		if (QYType.equals("5")) {
			mFileName = "grphbclaim_"+mGlobalInput.Operator+"_"+ min + sec;
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
			tTitle[0] = new String[] { "", "","","团单核保状况明细汇总统计报表","", "","","" };
			tTitle[1] = new String[] { "统计部门：" + mngname, "","","制表时间："+SysDate,"","",
					"统计时间：" + StartDate + "至" + EndDate,"" };
			tTitle[2] = new String[] { "机构名称", "险种代码", "险种简称","契撤件数","契撤保费","核保通过待签单件数","核保通过待签单人数","核保通过待签单保费" };
			String[] tContentType = { "String","String","String", "Number", "Number", "Number", "Number", "Number" };
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);

			if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
				return false;
			}
		}
		if (QYType.equals("6")) {
			mFileName = "grpcbclaim_"+mGlobalInput.Operator+"_"+ min + sec;
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
			tTitle[0] = new String[] { "", "","","团单承保状况明细汇总统计报表", "","","","" };
			tTitle[1] = new String[] { "统计部门：" + mngname, "","","制表时间："+SysDate,"","",
					"统计时间：" + StartDate + "至" + EndDate,"" };
			tTitle[2] = new String[] { "机构名称", "险种代码", "险种简称","承保件数","承保客户数","承保人数","承保保费","年度承保总保费" };
			String[] tContentType = { "String","String","String", "Number", "Number", "Number" , "Number", "Number"};
			mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);

			if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
				return false;
			}
		}
        StringBuffer sql = new StringBuffer();
        /** 数据库查询 */
        ExeSQL tExeSQL = new ExeSQL();
        /** 返回结果集 */
        SSRS ssrs = null;
        if (QYType.equals("1")) {
            if (Managecom.length() == 2) {
                SSRS tSSRS_M;
                String sql_M = "select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode";
                System.out.println("zhuzhu" + sql_M);
                tExeSQL = new ExeSQL();
                tSSRS_M = tExeSQL.execSQL(sql_M);
                String temp_M[][] = tSSRS_M.getAllData();
                String Mng;
                if (tSSRS_M.getMaxRow() > 0) {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++) {
                        Mng = temp_M[i - 1][0];
                        String Mng1;
                        if (Mng.equals("86000000")) {
                            Mng1 = Mng;
                        } else {
                            Mng1 = Mng.substring(0, 4);
                        }
                        sql = new StringBuffer();
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng1 + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(
                                " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                        sql.append(
                                " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                        sql.append(" (select count(distinct insuredno) from lcpol where renewcount<=0 and  prtno in(select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "') and conttype='1' and riskcode=Z.riskcode ) C1, ");
                        sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  prtno in(select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "') and conttype='1' and riskcode=Z.riskcode ) C2, ");
                        sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  prtno in(select prtno from lbcont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "') and conttype='1' and riskcode=Z.riskcode ) C3, ");

                        sql.append(" (select sum(prem) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1'  and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and riskcode=Z.riskcode) D1 ,");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1'  and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and riskcode=Z.riskcode) D2 ,");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1'  and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and riskcode=Z.riskcode) D3 ");
                        sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                        sql.append(" ) as X ");
                        sql.append(" union ");
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng1 + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append("   '总计' A,'总计' B, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "')  ) C1, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "')  ) C2, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "')  ) C3, ");

                        sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') ) D1 ,");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') ) D2 ,");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') ) D3 ");
                        sql.append(" from  lcpol where renewcount<=0 and conttype='1'");
                        sql.append(" ) as X order by A with ur");
 //                       System.out.println(sql.toString());
                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null) {
                            if (ssrs.getMaxRow() > 0) {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");
                    }
                    sql = new StringBuffer();
                    sql.append(" select '全国',A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append(
                            " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                    sql.append(
                            " (select RiskCode from lmrisk where riskcode=Z.riskcode) A, ");
                    sql.append(" (select count(distinct (insuredno)) from lcpol where renewcount<=0 and  prtno in (select prtno from lccont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1' ) and conttype='1'  and riskcode=Z.riskcode ) C1, ");
                    sql.append(" (select count(distinct (insuredno)) from lbpol where renewcount<=0 and  prtno in (select prtno from lccont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1' ) and conttype='1'  and riskcode=Z.riskcode ) C2, ");
                    sql.append(" (select count(distinct (insuredno)) from lbpol where renewcount<=0 and  prtno in (select prtno from lbcont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1' ) and conttype='1'  and riskcode=Z.riskcode ) C3, ");

                    sql.append(" (select sum(prem) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1')  and riskcode=Z.riskcode) D1 ,");
                    sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1')  and riskcode=Z.riskcode) D2 ,");
                    sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1')  and riskcode=Z.riskcode) D3 ");
                    sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                    sql.append(" ) as X ");
                    sql.append(" union ");
                    sql.append(" select '全国',A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                    sql.append(" from ");
                    sql.append(" ( select  '总计' A,'总计' B, ");
                    sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1')  ) C1, ");
                    sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1')  ) C2, ");
                    sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1')  ) C3, ");

                    sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1') ) D1 ,");
                    sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1') ) D2 ,");
                    sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and conttype='1') ) D3 ");
                    sql.append(" from  lcpol where conttype='1'");
                    sql.append(" ) as X order by A with ur");
//                    System.out.println(sql.toString());
                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null) {
                        if (ssrs.getMaxRow() > 0) {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    System.out.println("处理完成第二个Sql语句~~~~~");
                }
            } else if (Managecom.length() == 4) {
                SSRS tSSRS_M;
                String Mng = tExeSQL.getOneValue("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='" +
                                                 Managecom +
                                                 "' order by ComCode");
                sql = new StringBuffer();
                sql.append(
                        " select (select showname from ldcom where sign='1' and comcode= '" +
                        Mng + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(
                        " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(
                        " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                sql.append(" (select count(distinct insuredno) from lcpol where renewcount<=0 and   prtno in(select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "') and conttype='1' and riskcode=Z.riskcode ) C1, ");
                sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and   prtno in(select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "') and conttype='1' and riskcode=Z.riskcode ) C2, ");
                sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and   prtno in(select prtno from lbcont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "') and conttype='1' and riskcode=Z.riskcode ) C3, ");

                sql.append(" (select sum(prem) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1'  and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and riskcode=Z.riskcode) D1 ,");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1'  and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and riskcode=Z.riskcode) D2 ,");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1'  and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and riskcode=Z.riskcode) D3 ");
                sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(
                        " select (select showname from ldcom where sign='1' and comcode= '" +
                        Mng + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append("   '总计' A,'总计' B, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "')  ) C1, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "')  ) C2, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "')  ) C3, ");

                sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') ) D1 ,");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') ) D2 ,");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') ) D3 ");
                sql.append(" from  lcpol where conttype='1'");
                sql.append(" ) as X order by A with ur");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");

                sql = new StringBuffer();
                sql.append(
                        " select (select showname from ldcom where sign='1' and comcode= '" +
                        Mng + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(
                        " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(
                        " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                sql.append(" (select count(distinct insuredno) from lcpol where renewcount<=0 and   prtno in(select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "') and conttype='1' and riskcode=Z.riskcode ) C1, ");
                sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and   prtno in(select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "') and conttype='1' and riskcode=Z.riskcode ) C2, ");
                sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and   prtno in(select prtno from lbcont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "') and conttype='1' and riskcode=Z.riskcode ) C3, ");

                sql.append(" (select sum(prem) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1'  and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and riskcode=Z.riskcode) D1 ,");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1'  and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and riskcode=Z.riskcode) D2 ,");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1'  and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and riskcode=Z.riskcode) D3 ");
                sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(
                        " select (select showname from ldcom where sign='1' and comcode= '" +
                        Mng + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append("   '总计' A,'总计' B, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "')  ) C1, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "')  ) C2, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "')  ) C3, ");

                sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') ) D1 ,");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') ) D2 ,");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') ) D3 ");
                sql.append(" from  lcpol where conttype='1'");
                sql.append(" ) as X order by A with ur");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第二个Sql语句~~~~~");

            } else if (Managecom.length() == 8) {
                sql = new StringBuffer();
                sql.append(
                        " select (select showname from ldcom where sign='1' and comcode= '" +
                        Managecom + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(
                        " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(
                        " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                sql.append(" (select count(distinct insuredno) from lcpol where renewcount<=0 and   prtno in(select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "') and conttype='1' and riskcode=Z.riskcode ) C1, ");
                sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and   prtno in(select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "') and conttype='1' and riskcode=Z.riskcode ) C2, ");
                sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and   prtno in(select prtno from lbcont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "') and conttype='1' and riskcode=Z.riskcode ) C3, ");

                sql.append(" (select sum(prem) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1'  and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and riskcode=Z.riskcode) D1 ,");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1'  and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and riskcode=Z.riskcode) D2 ,");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1'  and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and riskcode=Z.riskcode) D3 ");
                sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(
                        " select (select showname from ldcom where sign='1' and comcode= '" +
                        Managecom + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append("   '总计' A,'总计' B, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom + "')  ) C1, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom + "')  ) C2, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom + "')  ) C3, ");

                sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom + "') ) D1 ,");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lccont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom + "') ) D2 ,");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  conttype='1' and prtno in (select prtno from lbcont where  makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom + "') ) D3 ");
                sql.append(" from  lcpol where conttype='1'");
                sql.append(" ) as X order by A with ur");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql = new StringBuffer();

            }

        } else if (QYType.equals("2")) {
            if (Managecom.length() == 2) {
                SSRS tSSRS_M;
                String sql_M = "select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode";
                System.out.println("zhuzhu" + sql_M);
                tExeSQL = new ExeSQL();
                tSSRS_M = tExeSQL.execSQL(sql_M);
                String temp_M[][] = tSSRS_M.getAllData();
                String Mng;
                if (tSSRS_M.getMaxRow() > 0) {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++) {
                        Mng = temp_M[i - 1][0];
                        String Mng1;
                        if (Mng.equals("86000000")) {
                            Mng1 = Mng;
                        } else {
                            Mng1 = Mng.substring(0, 4);
                        }
                        sql = new StringBuffer();
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng1 +
                                "'),A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(
                                " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                        sql.append(
                                " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                        sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='a' and substr(b.managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') C , ");
                        sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='a' and substr(b.managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') D, ");
                        sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='1' and substr(b.managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') E , ");
                        sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='1' and substr(b.managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') F, ");
                        sql.append(" (select count(distinct insuredno) from lcpol where  contno in(select contno from lccont where  uwdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "' and uwflag in('4','9') and appflag<>'1') and conttype='1' and riskcode=Z.riskcode ) G, ");
                        sql.append(" (select sum(prem) from lcpol where  contno in(select contno from lccont where  uwdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "' and uwflag in('4','9') and appflag<>'1') and conttype='1' and riskcode=Z.riskcode ) H ");
                        sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                        sql.append(" ) as X ");
                        sql.append(" union ");
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng1 +
                                "'),A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                        sql.append(" from ");
                        sql.append(" ( select  '总计' A,'总计' B, ");
                        sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and a.passflag='a' and substr(b.managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') C , ");
                        sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and a.passflag='a' and substr(b.managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') D, ");
                        sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and a.passflag='1' and substr(b.managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') E , ");
                        sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and a.passflag='1' and substr(b.managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') F, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                                   StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') ) G ,");
                        sql.append(" (select sum(prem) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                                   StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng + "') ) H ");
                        sql.append(" from  lcpol where conttype='1'");
                        sql.append(" ) as X order by A with ur");
                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null) {
                            if (ssrs.getMaxRow() > 0) {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");
                    }
                    sql = new StringBuffer();
                    sql.append(" select '全国',A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append(
                            " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                    sql.append(
                            " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                    sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and c.riskcode=Z.riskcode and a.passflag='a' ) C , ");
                    sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and c.riskcode=Z.riskcode and a.passflag='a' ) D, ");
                    sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and c.riskcode=Z.riskcode and a.passflag='1' ) E , ");
                    sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and c.riskcode=Z.riskcode and a.passflag='1' ) F, ");
                    sql.append(" (select count(insuredno) from lcpol where  conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                               StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1')  and riskcode=Z.riskcode) G ,");
                    sql.append(" (select sum(prem) from lcpol where  conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                               StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1')  and riskcode=Z.riskcode) H ");
                    sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                    sql.append(" ) as X ");
                    sql.append(" union ");
                    sql.append(" select '全国',A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append("  '总计' A,'总计' B, ");
                    sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and a.passflag='a' )C , ");
                    sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno  and b.conttype='1' and a.makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and a.passflag='a' ) D, ");
                    sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and a.passflag='1' ) E , ");
                    sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno  and b.conttype='1' and a.makedate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and a.passflag='1' ) F, ");
                    sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and appflag<>'1' and uwflag in('4','9') and conttype='1') ) G ,");
                    sql.append(" (select sum(prem) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                               StartDate + "' and '" + EndDate +
                               "' and appflag<>'1' and uwflag in('4','9') and conttype='1') ) H");
                    sql.append(" from  lcpol where conttype='1'");
                    sql.append(" ) as X order by A with ur");
                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null) {
                        if (ssrs.getMaxRow() > 0) {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    System.out.println("处理完成第二个Sql语句~~~~~");
                }
            } else if (Managecom.length() == 4) {
                SSRS tSSRS_M;
                String Mng = tExeSQL.getOneValue("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='" +
                                                 Managecom +
                                                 "' order by ComCode");
                sql = new StringBuffer();
                sql.append(
                        " select (select showname from ldcom where sign='1' and comcode= '" +
                        Mng +
                        "'),A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(
                        " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(
                        " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                           StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='a' and substr(b.managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') C , ");
                sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                           StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='a' and substr(b.managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') D, ");
                sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                           StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='1' and substr(b.managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') E , ");
                sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                           StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='1' and substr(b.managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') F, ");
                sql.append(" (select count(distinct insuredno) from lcpol where  contno in (select contno from lccont where  uwdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "' and uwflag in('4','9') and appflag<>'1') and conttype='1' and riskcode=Z.riskcode ) G, ");
                sql.append(" (select sum(prem) from lcpol where  contno in (select contno from lccont where  uwdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "' and uwflag in('4','9') and appflag<>'1') and conttype='1' and riskcode=Z.riskcode ) H ");
                sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(
                        " select (select showname from ldcom where sign='1' and comcode= '" +
                        Mng +
                        "'),A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                sql.append(" from ");
                sql.append(" ( select  '总计' A,'总计' B, ");
                sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and a.passflag='a' and substr(b.managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') C , ");
                sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and a.passflag='a' and substr(b.managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') D, ");
                sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and a.passflag='1' and substr(b.managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') E , ");
                sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and a.passflag='1' and substr(b.managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') F, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                           StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') ) G ,");
                sql.append(" (select sum(prem) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                           StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') ) H ");
                sql.append(" from  lcpol where conttype='1'");
                sql.append(" ) as X order by A with ur");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql = new StringBuffer();
                sql.append(" select '总计',A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(
                        " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(
                        " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                           StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='a' and substr(b.managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') C , ");
                sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                           StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='a' and substr(b.managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') D, ");
                sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                           StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='1' and substr(b.managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') E , ");
                sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                           StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='1' and substr(b.managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') F, ");
                sql.append(" (select count(distinct insuredno) from lcpol where  contno in (select contno from lccont where  uwdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "' and uwflag in('4','9') and appflag<>'1') and conttype='1' and riskcode=Z.riskcode ) G, ");
                sql.append(" (select sum(prem) from lcpol where  contno in(select contno from lccont where  uwdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "' and uwflag in('4','9') and appflag<>'1') and conttype='1' and riskcode=Z.riskcode ) H ");
                sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(
                        " select (select showname from ldcom where sign='1' and comcode= '" +
                        Mng +
                        "'),A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                sql.append(" from ");
                sql.append(" ( select  '总计' A,'总计' B, ");
                sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and a.passflag='a' and substr(b.managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') C , ");
                sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and a.passflag='a' and substr(b.managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') D, ");
                sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and a.passflag='1' and substr(b.managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') E , ");
                sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and a.passflag='1' and substr(b.managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') F ,");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                           StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') ) G ,");
                sql.append(" (select sum(prem) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                           StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng + "') ) H ");
                sql.append(" from  lcpol where conttype='1'");
                sql.append(" ) as X order by A with ur");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第二个Sql语句~~~~~");

            } else if (Managecom.length() == 8) {
                SSRS tSSRS_M;
                 String Mng = tExeSQL.getOneValue("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='" +
                                                  Managecom +
                                                  "' order by ComCode");
                 sql = new StringBuffer();
                 sql.append(
                         " select (select showname from ldcom where sign='1' and comcode= '" +
                         Managecom +
                         "'),A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                 sql.append(" from ");
                 sql.append(" ( ");
                 sql.append(" select ");
                 sql.append(
                         " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                 sql.append(
                         " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                 sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                            StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='a' and substr(b.managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') C , ");
                 sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                            StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='a' and substr(b.managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') D, ");
                 sql.append(" (select count(a.contno) from lcuwmaster a,lccont b,lcpol c where a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                            StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='1' and substr(b.managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') E , ");
                 sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                            StartDate + "' and '" + EndDate + "' and c.riskcode=Z.riskcode and a.passflag='1' and substr(b.managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') F, ");
                 sql.append(" (select count(distinct insuredno) from lcpol where  contno in(select contno from lccont where  uwdate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and conttype='1' and substr(managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "' and uwflag in('4','9') and appflag<>'1') and conttype='1' and riskcode=Z.riskcode ) G, ");
                 sql.append(" (select sum(prem) from lcpol where  contno in(select contno from lccont where  uwdate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and conttype='1' and substr(managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "' and uwflag in('4','9') and appflag<>'1') and conttype='1' and riskcode=Z.riskcode ) H ");
                 sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                 sql.append(" ) as X ");
                 sql.append(" union ");
                 sql.append(
                         " select (select showname from ldcom where sign='1' and comcode= '" +
                         Managecom +
                         "'),A,B, VALUE(C,0),VALUE(D,0),VALUE(E,0),VALUE(F,0),VALUE(G,0),VALUE(H,0) ");
                 sql.append(" from ");
                 sql.append(" ( select  '总计' A,'总计' B, ");
                 sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and a.passflag='a' and substr(b.managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') C , ");
                 sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and a.passflag='a' and substr(b.managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') D, ");
                 sql.append(" (select count(a.contno) from lcuwmaster a,lccont b where a.contno=b.contno and  b.conttype='1' and a.makedate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and a.passflag='1' and substr(b.managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') E , ");
                 sql.append(" (select sum(c.prem) from lcuwmaster a,lccont b,lcpol c where c.prtno=b.prtno and a.contno=b.contno  and  a.polno=c.polno and b.conttype='1' and a.makedate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and a.passflag='1' and substr(b.managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') F, ");
                 sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                            StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1' and substr(managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') ) G ,");
                 sql.append(" (select sum(prem) from lcpol where conttype='1' and contno in (select contno from lccont where  uwdate between '" +
                            StartDate + "' and '" + EndDate + "' and uwflag in('4','9') and appflag<>'1' and conttype='1' and substr(managecom,1,length('" +
                            Managecom + "'))='" + Managecom + "') ) H ");
                 sql.append(" from  lcpol where conttype='1'");
                 sql.append(" ) as X order by A with ur");
                 ssrs = tExeSQL.execSQL(sql.toString());
                 if (ssrs != null) {
                     if (ssrs.getMaxRow() > 0) {
                         for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                             result.add(ssrs.getRowData(m));
                         }
                     }
                 }
                 ssrs = null;
                 sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");

            }

        } else if (QYType.equals("3")) {
            //承保
            if (Managecom.length() == 2) {
                SSRS tSSRS_M;
                String sql_M = "select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode";
                System.out.println("zhuzhu" + sql_M);
                tExeSQL = new ExeSQL();
                tSSRS_M = tExeSQL.execSQL(sql_M);
                String temp_M[][] = tSSRS_M.getAllData();
                String Mng;
                if (tSSRS_M.getMaxRow() > 0) {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++) {
                        Mng = temp_M[i - 1][0];
                        String Mng1;
                        if (Mng.equals("86000000")) {
                            Mng1 = Mng;
                        } else {
                            Mng1 = Mng.substring(0, 4);
                        }
                        sql = new StringBuffer();
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng1 + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(
                                " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                        sql.append(
                                " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                        sql.append(" (select count(distinct insuredno) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C1, ");
                        sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C2, ");
                        sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C3, ");
                        sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D1, ");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D2, ");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D3, ");

                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E1, ");
                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and   contno in(select contno from lccont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E2, ");
                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E3 ");

                        sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                        sql.append(" ) as X ");
                        sql.append(" union ");
                        sql.append(
                                " select (select showname from ldcom where sign='1' and comcode= '" +
                                Mng1 + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append("  '总计' A,'总计' B, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') C1, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') C2, ");
                        sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') C3, ");
                        sql.append(" (select sum(prem) from lcpol where renewcount<=0 and   contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') D1, ");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') D2, ");
                        sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') D3, ");

                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') E1, ");
                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') E2, ");
                        sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where   signdate between '" +
                                   StartDate + "' and '" + EndDate +
                                   "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                                   Managecom + "')*4)='" + Mng +
                                   "' ) and appflag='1'  and conttype='1') E3 ");

                        sql.append(" from  lcpol where conttype='1'");
                        sql.append(" ) as X order by A with ur");
                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null) {
                            if (ssrs.getMaxRow() > 0) {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");
                    }
                    sql = new StringBuffer();
                    sql.append(" select '全国',A,B, to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append(
                            " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                    sql.append(
                            " (select RiskCode from lmrisk where riskcode=Z.riskcode) A, ");
                    sql.append(" (select count(distinct insuredno) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C1, ");
                    sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C2, ");
                    sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C3, ");
                    sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D1, ");
                    sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D2, ");
                    sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D3, ");

                    sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E1, ");
                    sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E2, ");
                    sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E3 ");


                    sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                    sql.append(" ) as X ");
                    sql.append(" union ");
                    sql.append(" select '全国',A,B, to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append("  '总计' A,'总计' B, ");
                    sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1'  ) and appflag='1'  and conttype='1'    ) C1, ");
                    sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and   contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1'  ) and appflag='1'  and conttype='1'    ) C2, ");
                    sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1'  ) and appflag='1'  and conttype='1'    ) C3, ");
                    sql.append(" (select sum(prem) from lcpol where contno in(select contno from lccont where renewcount<=0 and   signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1'  ) and appflag='1'  and conttype='1'    ) D1, ");
                    sql.append(" (select sum(prem) from lbpol where contno in(select contno from lccont where renewcount<=0 and   signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1'  ) and appflag='1'  and conttype='1'    ) D2, ");
                    sql.append(" (select sum(prem) from lbpol where contno in(select contno from lbcont where renewcount<=0 and   signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1'  ) and appflag='1'  and conttype='1'    ) D3, ");

                    sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1'  ) and appflag='1'  and conttype='1'    ) E1, ");
                    sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1'  ) and appflag='1'  and conttype='1'    ) E2, ");
                    sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                               StartDate + "' and '" + EndDate + "' and conttype='1' and appflag='1'  ) and appflag='1'  and conttype='1'    ) E3 ");


                    sql.append(" from  lcpol where conttype='1'");
                    sql.append(" ) as X order by A with ur");
                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null) {
                        if (ssrs.getMaxRow() > 0) {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    System.out.println("处理完成第二个Sql语句~~~~~");
                }
            } else if (Managecom.length() == 4) {
                SSRS tSSRS_M;
                String Mng = tExeSQL.getOneValue("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='" +
                                                 Managecom +
                                                 "' order by ComCode");
                sql = new StringBuffer();
                sql.append(
                        " select (select showname from ldcom where sign='1' and comcode= '" +
                        Mng + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(
                        " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(
                        " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                sql.append(" (select count(distinct insuredno) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C1, ");
                sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C2, ");
                sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C3, ");
                sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D1, ");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D2, ");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D3, ");

                sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E1, ");
                sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E2, ");
                sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E3 ");


                sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(
                        " select (select showname from ldcom where sign='1' and comcode= '" +
                        Mng + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append("   '总计' A,'总计' B, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1') C1, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1') C2, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1') C3, ");
                sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1') D1, ");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1') D2, ");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1') D3, ");

                sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                            Managecom + "')*2)='" + Mng +
                            "' ) and appflag='1'  and conttype='1') E1, ");
                 sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                            Managecom + "')*2)='" + Mng +
                            "' ) and appflag='1'  and conttype='1') E2, ");
                 sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where   signdate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                            Managecom + "')*2)='" + Mng +
                            "' ) and appflag='1'  and conttype='1') E3 ");


                sql.append(" from  lcpol where conttype='1'");
                sql.append(" ) as X order by A with ur");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");

                sql = new StringBuffer();
                sql.append(" select '总计',A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(
                        " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(
                        " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                sql.append(" (select count(distinct insuredno) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C1, ");
                sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C2, ");
                sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C3, ");
                sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D1, ");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D2, ");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D3, ");

                sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E1, ");
                sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E2, ");
                sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E3 ");


                sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(" select '总计',A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append("  '总计' A,'总计' B, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1') C1, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1') C2, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1') C3, ");
                sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1') D1, ");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1') D2, ");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1') D3, ");

                sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1') E1, ");
                sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lccont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1') E2, ");
                sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  contno in(select contno from lbcont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "')*2)='" + Mng +
                           "' ) and appflag='1'  and conttype='1') E3 ");


                sql.append(" from  lcpol where conttype='1'");
                sql.append(" ) as X order by A with ur");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第二个Sql语句~~~~~");

            } else if (Managecom.length() == 8) {
                sql = new StringBuffer();
                sql.append(
                        " select (select showname from ldcom where sign='1' and comcode= '" +
                        Managecom + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(
                        " (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(
                        " (select Riskcode from lmrisk where riskcode=Z.riskcode) A, ");
                sql.append(" (select count(distinct insuredno) from lcpol where renewcount<=0 and  prtno in(select prtno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C1, ");
                sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and  prtno in(select prtno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C2, ");
                sql.append(" (select count(distinct insuredno) from lbpol where renewcount<=0 and   prtno in(select prtno from lbcont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) C3, ");
                sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  prtno in(select prtno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D1, ");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  prtno in(select prtno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D2, ");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  prtno in(select prtno from lbcont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) D3, ");

                sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  prtno in(select prtno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E1, ");
                sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  prtno in(select prtno from lccont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E2, ");
                sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  prtno in(select prtno from lbcont where  signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1'  and riskcode=Z.riskcode ) E3 ");

                sql.append(" from lmriskapp Z where Z.RiskProp='I'");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(
                        " select (select showname from ldcom where sign='1' and comcode= '" +
                        Managecom + "'),A,B,to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2)+to_zero(D3),to_zero(E1)+to_zero(E2)+to_zero(E3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append("   '总计' A,'总计' B, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lcpol where renewcount<=0 and  prtno in(select prtno from lccont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1') C1, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and  prtno in(select prtno from lccont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1') C2, ");
                sql.append(" (select count(distinct (insuredno||riskcode)) from lbpol where renewcount<=0 and  prtno in(select prtno from lbcont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1') C3, ");
                sql.append(" (select sum(prem) from lcpol where renewcount<=0 and  prtno in(select prtno from lccont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1') D1, ");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  prtno in(select prtno from lccont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1') D2, ");
                sql.append(" (select sum(prem) from lbpol where renewcount<=0 and  prtno in(select prtno from lbcont where   signdate between '" +
                           StartDate + "' and '" + EndDate +
                           "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' ) and appflag='1'  and conttype='1') D3, ");

                sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lcpol where renewcount<=0 and  prtno in(select prtno from lccont where   signdate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                            Managecom + "'))='" + Managecom +
                            "' ) and appflag='1'  and conttype='1') E1, ");
                 sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  prtno in(select prtno from lccont where   signdate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                            Managecom + "'))='" + Managecom +
                            "' ) and appflag='1'  and conttype='1') E2, ");
                 sql.append(" (select sum(case when payintv=0 then prem else (prem*12/payintv) end ) from lbpol where renewcount<=0 and  prtno in(select prtno from lbcont where   signdate between '" +
                            StartDate + "' and '" + EndDate +
                            "' and conttype='1' and appflag='1' and substr(managecom,1,length('" +
                            Managecom + "'))='" + Managecom +
                            "' ) and appflag='1'  and conttype='1') E3 ");

                sql.append(" from  lcpol where conttype='1'");
                sql.append(" ) as X order by A with ur");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql = new StringBuffer();
            }

        } else if (QYType.equals("4"))
        {
            if (Managecom.length() == 2)
            {
                SSRS tSSRS_M;
                String sql_M = "select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode";
                System.out.println("zhuzhu" + sql_M);
                tExeSQL = new ExeSQL();
                tSSRS_M = tExeSQL.execSQL(sql_M);
                String temp_M[][] = tSSRS_M.getAllData();
                String Mng;
                if (tSSRS_M.getMaxRow() > 0)
                {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++)
                    {
                        Mng = temp_M[i - 1][0];
                        String Mng1;
                        if (Mng.equals("86000000"))
                        {
                            Mng1 = Mng;
                        }
                        else
                        {
                            Mng1 = Mng.substring(0, 4);
                        }
                        sql = new StringBuffer();
                        sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng1
                                + "'),A,B, ");
                        sql
                                .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                        sql.append(" Z.riskcode A, ");
                        sql
                                .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) ) C1, ");
                        sql
                                .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) ) C2, ");
                        sql
                                .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) ) C3, ");

                        sql
                                .append(" (select count(distinct (b.APPNTNo)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' and a.riskcode=Z.riskcode ) D1,");
                        sql
                                .append(" (select count(distinct (b.APPNTNo)) from lbgrppol a,lbgrpcont b where b.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' and a.riskcode=Z.riskcode ) D2,");

                        sql
                                .append(" (select count(distinct (insuredno)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom
                                        + "')*4)='"
                                        + Mng
                                        + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                        sql
                                .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom
                                        + "')*4)='"
                                        + Mng
                                        + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                        sql
                                .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom
                                        + "')*4)='"
                                        + Mng
                                        + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                        sql
                                .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and riskcode=Z.riskcode ) F1,");
                        sql
                                .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and riskcode=Z.riskcode ) F2,");
                        sql
                                .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and riskcode=Z.riskcode ) F3");

                        sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                        sql.append(" ) as X ");
                        sql.append(" union ");
                        sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng1
                                + "'),A,B, ");
                        sql
                                .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(" '总计' A,'总计' B, ");
                        sql
                                .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "'  )) C1, ");
                        sql
                                .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "'  )) C2, ");

                        sql
                                .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lbgrpcont   where edorno not like 'xb%' and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "'  )) C3, ");

                        sql
                                .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) D1,");
                        sql
                                .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where b.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) D2,");

                        sql
                                .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')  and PolTypeFlag <>'2') E1,");
                        sql
                                .append(" (select count(insuredno||riskcode) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')  and PolTypeFlag <>'2') E2,");
                        sql
                                .append(" (select count(insuredno||riskcode) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')  and PolTypeFlag <>'2') E3,");

                        sql
                                .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')  ) F1,");
                        sql
                                .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') ) F2,");
                        sql
                                .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') ) F3");
                        sql.append(" from ldcom Z where Z.comcode ='" + Mng + "' ");
                        sql.append(" ) as X  order by A with ur");

                        ssrs = tExeSQL.execSQL(sql.toString());
                        System.out.println("sql" + sql.toString());
                        if (ssrs != null)
                        {
                            if (ssrs.getMaxRow() > 0)
                            {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++)
                                {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");
                    }
                    sql = new StringBuffer();
                    sql.append(" select '全国',A,B, ");
                    sql
                            .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                    sql.append(" Z.riskcode A, ");
                    sql
                            .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and   makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C1, ");
                    sql
                            .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and    makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C2, ");
                    sql
                            .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and   makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C3, ");
                    sql
                            .append(" (select count(distinct (b.APPNTNo)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  and a.riskcode=Z.riskcode ) D1,");
                    sql
                            .append(" (select count(distinct (b.APPNTNo)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  and a.riskcode=Z.riskcode ) D2,");

                    sql
                            .append(" (select count(distinct (insuredno)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                    + StartDate
                                    + "' and '"
                                    + EndDate
                                    + "' ) and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                    sql
                            .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                    + StartDate
                                    + "' and '"
                                    + EndDate
                                    + "' ) and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                    sql
                            .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                    + StartDate
                                    + "' and '"
                                    + EndDate
                                    + "' ) and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                    sql
                            .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                    + StartDate + "' and '" + EndDate + "' ) and riskcode=Z.riskcode ) F1,");
                    sql
                            .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                    + StartDate + "' and '" + EndDate + "' ) and riskcode=Z.riskcode ) F2,");
                    sql
                            .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                    + StartDate + "' and '" + EndDate + "' ) and riskcode=Z.riskcode ) F3");
                    sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                    sql.append(" ) as X ");
                    sql.append(" union ");
                    sql.append(" select '全国',A,B, ");
                    sql
                            .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append(" '总计' A,'总计' B, ");
                    sql
                            .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C1, ");
                    sql
                            .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C2, ");
                    sql
                            .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C3, ");

                    sql
                            .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) D1,");
                    sql
                            .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) D2,");

                    sql
                            .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                    + StartDate + "' and '" + EndDate + "' )  and PolTypeFlag <>'2') E1,");
                    sql
                            .append(" (select count(distinct (insuredno||riskcode)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                    + StartDate + "' and '" + EndDate + "' )  and PolTypeFlag <>'2') E2,");
                    sql
                            .append(" (select count(distinct (insuredno||riskcode)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where  edorno not like 'xb%' and makedate between '"
                                    + StartDate + "' and '" + EndDate + "' )  and PolTypeFlag <>'2') E3,");

                    sql
                            .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                    + StartDate + "' and '" + EndDate + "' ) ) F1,");
                    sql
                            .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                    + StartDate + "' and '" + EndDate + "' )  ) F2,");
                    sql
                            .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where  edorno not like 'xb%' and makedate between '"
                                    + StartDate + "' and '" + EndDate + "' )  ) F3");
                    sql.append(" from ldcom Z where Z.comcode ='86' ");
                    sql.append(" ) as X  order by A with ur");
                    ssrs = tExeSQL.execSQL(sql.toString());
                    System.out.println("sql" + sql.toString());
                    if (ssrs != null)
                    {
                        if (ssrs.getMaxRow() > 0)
                        {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++)
                            {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    System.out.println("处理完成第二个Sql语句~~~~~");
                }
            }
            else if (Managecom.length() == 4)
            {
                SSRS tSSRS_M;
                String Mng = tExeSQL
                        .getOneValue("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='"
                                + Managecom + "' order by ComCode");

                sql = new StringBuffer();
                sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng + "'),A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(" Z.riskcode A, ");
                sql
                        .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' ) ) C1, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' ) ) C2, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' ) ) C3, ");

                sql
                        .append(" (select count(distinct (b.APPNTNo)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' and a.riskcode=Z.riskcode ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' and a.riskcode=Z.riskcode ) D2,");

                sql
                        .append(" (select count(distinct (insuredno)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode ) F3");

                sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng + "'),A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" '总计' A,'总计' B, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  )) C1, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  )) C2, ");

                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lbgrpcont   where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  )) C3, ");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' ) D2,");

                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "')  and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "')  and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "')  and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') ) F3");
                sql.append(" from ldcom Z where Z.comcode ='" + Mng + "' ");
                sql.append(" ) as X  order by A with ur");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null)
                {
                    if (ssrs.getMaxRow() > 0)
                    {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++)
                        {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");

                sql = new StringBuffer();
                sql.append(" select '总计',A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(" Z.riskcode A, ");
                sql
                        .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' ) ) C1, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' ) ) C2, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' ) ) C3, ");
                sql
                        .append(" (select count(distinct b.APPNTNo) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' and a.riskcode=Z.riskcode ) D1,");
                sql
                        .append(" (select count(distinct b.APPNTNo) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' and a.riskcode=Z.riskcode ) D2,");

                sql
                        .append(" (select count(distinct (insuredno)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where  edorno not like 'xb%' and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and riskcode=Z.riskcode ) F3");
                sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(" select '总计',A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" '总计' A,'总计' B, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where grpcontno in (select grpcontno from lcgrpcont   where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  )) C1, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lcgrpcont   where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  )) C2, ");

                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lbgrpcont   where  edorno not like 'xb%' and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  )) C3, ");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  ) D2,");

                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "')  and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%'  and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "')  ) F3");
                sql.append(" from ldcom Z where Z.comcode ='86' ");
                sql.append(" ) as X  order by A with ur");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null)
                {
                    if (ssrs.getMaxRow() > 0)
                    {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++)
                        {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                System.out.println("处理完成第二个Sql语句~~~~~");
            }
            else if (Managecom.length() == 8)
            {
                SSRS tSSRS_M;
                sql = new StringBuffer();
                sql
                        .append(" select (select showname from ldcom where sign='1' and comcode= '" + Managecom
                                + "'),A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(" Z.riskcode A, ");
                sql
                        .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) ) C1, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) ) C2, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) ) C3, ");

                sql
                        .append(" (select count(distinct (b.APPNTNo)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' and a.riskcode=Z.riskcode ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo)) from lbgrppol a,lbgrpcont b where b.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' and a.riskcode=Z.riskcode ) D2,");

                sql
                        .append(" (select count(distinct (insuredno)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode ) F3");
                sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql
                        .append(" select (select showname from ldcom where sign='1' and comcode= '" + Managecom
                                + "'),A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" '总计' A,'总计' B, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where grpcontno in (select grpcontno from lcgrpcont   where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  )) C1, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  )) C2, ");

                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where grpcontno in (select grpcontno from lbgrpcont   where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  )) C3, ");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) D2,");

                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  ) F3");
                sql.append(" from ldcom Z where Z.comcode ='" + Managecom + "' ");
                sql.append(" ) as X  order by A with ur");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null)
                {
                    if (ssrs.getMaxRow() > 0)
                    {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++)
                        {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql = new StringBuffer();
            }
            
        } else if (QYType.equals("5"))
        {
            if (Managecom.length() == 2)
            {
                SSRS tSSRS_M;
                String sql_M = "select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode";
                System.out.println("zhuzhu" + sql_M);
                tExeSQL = new ExeSQL();
                tSSRS_M = tExeSQL.execSQL(sql_M);
                String temp_M[][] = tSSRS_M.getAllData();
                String Mng;
                if (tSSRS_M.getMaxRow() > 0)
                {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++)
                    {
                        Mng = temp_M[i - 1][0];
                        String Mng1;
                        if (Mng.equals("86000000"))
                        {
                            Mng1 = Mng;
                        }
                        else
                        {
                            Mng1 = Mng.substring(0, 4);
                        }
                        sql = new StringBuffer();
                        sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng1
                                + "'),A,B, ");
                        sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                        sql.append(" Z.riskcode A, ");
                        sql
                                .append(" (select count(distinct a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom
                                        + "')*4)='"
                                        + Mng
                                        + "'  and a.passflag='a' and c.riskcode=Z.riskcode) G, ");
                        sql
                                .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom
                                        + "')*4)='"
                                        + Mng
                                        + "'  and a.passflag='a' and c.riskcode=Z.riskcode) H, ");
                        sql
                                .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' )) L, ");
                        sql
                                .append(" (select count(distinct insuredno) from lcpol where  conttype='2' and  riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and PolTypeFlag <>'2') M, ");
                        sql
                                .append(" (select sum(prem) from lcpol where  conttype='2' and  riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and PolTypeFlag <>'2') N ");
                        sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                        sql.append(" ) as X ");
                        sql.append(" union ");
                        sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng1
                                + "'),A,B, ");
                        sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(" '总计' A,'总计' B, ");
                        sql
                                .append(" (select count(distinct (a.grpcontno||riskcode)) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "'  and a.passflag='a' ) G, ");
                        sql
                                .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "'  and a.passflag='a' ) H, ");
                        sql
                                .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where  grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')) L, ");
                        sql
                                .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and PolTypeFlag <>'2') M, ");
                        sql
                                .append(" (select sum(prem) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and PolTypeFlag <>'2') N ");
                        sql.append(" from ldcom Z where Z.comcode ='" + Mng + "' ");
                        sql.append(" ) as X  order by A with ur");

                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null)
                        {
                            if (ssrs.getMaxRow() > 0)
                            {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++)
                                {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");
                    }
                    sql = new StringBuffer();
                    sql.append(" select '全国',A,B, ");
                    sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                    sql.append(" Z.riskcode A, ");
                    sql
                            .append(" (select count(distinct a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                    + StartDate
                                    + "' and '"
                                    + EndDate
                                    + "'   and a.passflag='a' and c.riskcode=Z.riskcode) G, ");
                    sql
                            .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                    + StartDate
                                    + "' and '"
                                    + EndDate
                                    + "'   and a.passflag='a' and c.riskcode=Z.riskcode) H, ");
                    sql
                            .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                    + StartDate + "' and '" + EndDate + "' )) L, ");
                    sql
                            .append(" (select count(distinct insuredno) from lcpol where  conttype='2' and  riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                    + StartDate + "' and '" + EndDate + "') and PolTypeFlag <>'2') M, ");
                    sql
                            .append(" (select sum(prem) from lcpol where  conttype='2' and riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                    + StartDate + "' and '" + EndDate + "') and PolTypeFlag <>'2') N ");
                    sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                    sql.append(" ) as X ");
                    sql.append(" union ");
                    sql.append(" select '全国',A,B, ");
                    sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append(" '总计' A,'总计' B, ");
                    sql
                            .append(" (select count(distinct (a.grpcontno||riskcode)) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                    + StartDate + "' and '" + EndDate + "'   and a.passflag='a' ) G, ");
                    sql
                            .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                    + StartDate + "' and '" + EndDate + "'   and a.passflag='a' ) H, ");
                    sql
                            .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where  grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                    + StartDate + "' and '" + EndDate + "' )) L, ");
                    sql
                            .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                    + StartDate + "' and '" + EndDate + "') and PolTypeFlag <>'2') M, ");
                    sql
                            .append(" (select sum(prem) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                    + StartDate + "' and '" + EndDate + "') and PolTypeFlag <>'2') N ");
                    sql.append(" from ldcom Z where Z.comcode ='86' ");
                    sql.append(" ) as X  order by A with ur");
                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null)
                    {
                        if (ssrs.getMaxRow() > 0)
                        {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++)
                            {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    System.out.println("处理完成第二个Sql语句~~~~~");
                }
            }
            else if (Managecom.length() == 4)
            {
                SSRS tSSRS_M;
                String Mng = tExeSQL
                        .getOneValue("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='"
                                + Managecom + "' order by ComCode");

                sql = new StringBuffer();
                sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng + "'),A,B, ");
                sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(" Z.riskcode A, ");
                sql
                        .append(" (select count(distinct a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  and a.passflag='a' and c.riskcode=Z.riskcode) G, ");
                sql
                        .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  and a.passflag='a' and c.riskcode=Z.riskcode) H, ");
                sql
                        .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' )) L, ");
                sql
                        .append(" (select count(distinct insuredno) from lcpol where  conttype='2' and  riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') M, ");
                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2' and  riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') N ");
                sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng + "'),A,B, ");
                sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" '总计' A,'总计' B, ");
                sql
                        .append(" (select count(distinct (a.grpcontno||riskcode)) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  and a.passflag='a' ) G, ");
                sql
                        .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  and a.passflag='a' ) H, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where  grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "')) L, ");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') M, ");
                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') N ");
                sql.append(" from ldcom Z where Z.comcode ='" + Mng + "' ");
                sql.append(" ) as X  order by A with ur");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null)
                {
                    if (ssrs.getMaxRow() > 0)
                    {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++)
                        {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql = new StringBuffer();
                sql.append(" select '总计',A,B, ");
                sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(" Z.riskcode A, ");
                sql
                        .append(" (select count(distinct a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  and a.passflag='a' and c.riskcode=Z.riskcode) G, ");
                sql
                        .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  and a.passflag='a' and c.riskcode=Z.riskcode) H, ");
                sql
                        .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "' )) L, ");
                sql
                        .append(" (select count(distinct insuredno) from lcpol where  conttype='2' and  riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') M, ");
                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2' and  riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') N ");
                sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(" select '总计',A,B, ");
                sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" '总计' A,'总计' B, ");
                sql
                        .append(" (select count(distinct (a.grpcontno||riskcode)) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  and a.passflag='a' ) G, ");
                sql
                        .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "'  and a.passflag='a' ) H, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where  grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "')) L, ");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') M, ");
                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "')*2)='" + Mng + "') and PolTypeFlag <>'2') N ");
                sql.append(" from ldcom Z where Z.comcode ='" + Mng + "' ");
                sql.append(" ) as X  order by A with ur");

            }
            else if (Managecom.length() == 8)
            {
                SSRS tSSRS_M;
                sql = new StringBuffer();
                sql
                        .append(" select (select showname from ldcom where sign='1' and comcode= '" + Managecom
                                + "'),A,B, ");
                sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(" Z.riskcode A, ");
                sql
                        .append(" (select count(distinct a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  and a.passflag='a' and c.riskcode=Z.riskcode) G, ");
                sql
                        .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  and a.passflag='a' and c.riskcode=Z.riskcode) H, ");
                sql
                        .append(" (select count(distinct prtno) from lcgrppol where riskcode=Z.riskcode and grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' )) L, ");
                sql
                        .append(" (select count(distinct insuredno) from lcpol where  conttype='2' and  riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and PolTypeFlag <>'2') M, ");
                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2' and  riskcode=Z.riskcode  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and PolTypeFlag <>'2') N ");
                sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql
                        .append(" select (select showname from ldcom where sign='1' and comcode= '" + Managecom
                                + "'),A,B, ");
                sql.append("VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" '总计' A,'总计' B, ");
                sql
                        .append(" (select count(distinct (a.grpcontno||riskcode)) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  and a.passflag='a' ) G, ");
                sql
                        .append(" (select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  and a.passflag='a' ) H, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where  grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')) L, ");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and PolTypeFlag <>'2') M, ");
                sql
                        .append(" (select sum(prem) from lcpol where  conttype='2'  and grpcontno in (select grpcontno from lcGRPcont where uwflag in('9') and appflag<>'1' and UWdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and PolTypeFlag <>'2') N ");
                sql.append(" from ldcom Z where Z.comcode ='" + Managecom + "' ");
                sql.append(" ) as X  order by A with ur");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null)
                {
                    if (ssrs.getMaxRow() > 0)
                    {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++)
                        {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
            }
            

        }
        else
        {
            if (Managecom.length() == 2)
            {
                SSRS tSSRS_M;
                String sql_M = "select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode";
                System.out.println("zhuzhu" + sql_M);
                tExeSQL = new ExeSQL();
                tSSRS_M = tExeSQL.execSQL(sql_M);
                String temp_M[][] = tSSRS_M.getAllData();
                String Mng;
                if (tSSRS_M.getMaxRow() > 0)
                {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++)
                    {
                        Mng = temp_M[i - 1][0];
                        String Mng1;
                        if (Mng.equals("86000000"))
                        {
                            Mng1 = Mng;
                        }
                        else
                        {
                            Mng1 = Mng.substring(0, 4);
                        }
                        sql = new StringBuffer();
                        sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng1
                                + "'),A,B, ");
                        sql
                                .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                        sql.append(" Z.riskcode A, ");
                        sql
                                .append(" (select count(distinct prtno) from lcgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) ) C1, ");
                        sql
                                .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) ) C2, ");
                        sql
                                .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) ) C3, ");

                        sql
                                .append(" (select count(distinct (b.APPNTNo)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' and a.riskcode=Z.riskcode ) D1,");
                        sql
                                .append(" (select count(distinct (b.APPNTNo)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' and a.riskcode=Z.riskcode ) D2,");

                        sql
                                .append(" (select count(distinct (insuredno)) from lcpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom
                                        + "')*4)='"
                                        + Mng
                                        + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                        sql
                                .append(" (select count(distinct (insuredno)) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom
                                        + "')*4)='"
                                        + Mng
                                        + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                        sql
                                .append(" (select count(distinct (insuredno)) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom
                                        + "')*4)='"
                                        + Mng
                                        + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                        sql
                                .append(" (select sum(prem) from lcpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and riskcode=Z.riskcode ) F1,");
                        sql
                                .append(" (select sum(prem) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and riskcode=Z.riskcode ) F2,");
                        sql
                                .append(" (select sum(prem) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and riskcode=Z.riskcode ) F3,");

                        sql
                                .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and riskcode=Z.riskcode ) G1,");
                        sql
                                .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and riskcode=Z.riskcode ) G2,");
                        sql
                                .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') and riskcode=Z.riskcode ) G3");

                        sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                        sql.append(" ) as X ");
                        sql.append(" union ");
                        sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng1
                                + "'),A,B, ");
                        sql
                                .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append(" select ");
                        sql.append(" '总计' A,'总计' B, ");
                        sql
                                .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "'  )) C1, ");
                        sql
                                .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "'  )) C2, ");

                        sql
                                .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lbgrpcont   where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "'  )) C3, ");

                        sql
                                .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) D1,");
                        sql
                                .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(b.managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "' ) D2,");

                        sql
                                .append(" (select count(distinct (insuredno||riskcode)) from lcpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')  and PolTypeFlag <>'2') E1,");
                        sql
                                .append(" (select count(insuredno||riskcode) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')  and PolTypeFlag <>'2') E2,");
                        sql
                                .append(" (select count(insuredno||riskcode) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')  and PolTypeFlag <>'2') E3,");

                        sql
                                .append(" (select sum(prem) from lcpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') ) F1,");
                        sql
                                .append(" (select sum(prem) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') ) F2,");
                        sql
                                .append(" (select sum(prem) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')  ) F3,");

                        sql
                                .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') ) G1,");
                        sql
                                .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and  signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "') ) G2,");
                        sql
                                .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                        + StartDate
                                        + "' and '"
                                        + EndDate
                                        + "' and substr(managecom,1,length('"
                                        + Managecom + "')*4)='" + Mng + "')  ) G3");

                        sql.append(" from ldcom Z where Z.comcode ='" + Mng + "' ");
                        sql.append(" ) as X  order by A with ur");

                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null)
                        {
                            if (ssrs.getMaxRow() > 0)
                            {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++)
                                {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");
                    }
                    sql = new StringBuffer();
                    sql.append(" select '全国',A,B, ");
                    sql
                            .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                    sql.append(" Z.riskcode A, ");
                    sql
                            .append(" (select count(distinct prtno) from lcgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and   signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C1, ");
                    sql
                            .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and    signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C2, ");
                    sql
                            .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and   signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C3, ");
                    sql
                            .append(" (select count(distinct (b.APPNTNo)) from lcgrppol a,lcgrpcont b where a.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  and a.riskcode=Z.riskcode ) D1,");
                    sql
                            .append(" (select count(distinct (b.APPNTNo)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  and a.riskcode=Z.riskcode ) D2,");

                    sql
                            .append(" (select count(distinct (insuredno)) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                    + StartDate
                                    + "' and '"
                                    + EndDate
                                    + "' ) and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                    sql
                            .append(" (select count(distinct (insuredno)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                    + StartDate
                                    + "' and '"
                                    + EndDate
                                    + "' ) and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                    sql
                            .append(" (select count(distinct (insuredno)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  appflag='1' and signdate between '"
                                    + StartDate
                                    + "' and '"
                                    + EndDate
                                    + "' ) and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                    sql
                            .append(" (select sum(prem) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' ) and riskcode=Z.riskcode ) F1,");
                    sql
                            .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' ) and riskcode=Z.riskcode ) F2,");
                    sql
                            .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' ) and riskcode=Z.riskcode ) F3,");

                    sql
                            .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' ) and riskcode=Z.riskcode ) G1,");
                    sql
                            .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' ) and riskcode=Z.riskcode ) G2,");
                    sql
                            .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' ) and riskcode=Z.riskcode ) G3");

                    sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                    sql.append(" ) as X ");
                    sql.append(" union ");
                    sql.append(" select '全国',A,B, ");
                    sql
                            .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append(" select ");
                    sql.append(" '总计' A,'总计' B, ");
                    sql
                            .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C1, ");
                    sql
                            .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C2, ");
                    sql
                            .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) ) C3, ");

                    sql
                            .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where a.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) D1,");
                    sql
                            .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                    + StartDate + "' and '" + EndDate + "'  ) D2,");

                    sql
                            .append(" (select count(distinct (insuredno||riskcode)) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' )  and PolTypeFlag <>'2') E1,");
                    sql
                            .append(" (select count(distinct (insuredno||riskcode)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' )  and PolTypeFlag <>'2') E2,");
                    sql
                            .append(" (select count(distinct (insuredno||riskcode)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' )  and PolTypeFlag <>'2') E3,");

                    sql
                            .append(" (select sum(prem) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' )  ) F1,");
                    sql
                            .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' )  ) F2,");
                    sql
                            .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' )  ) F3,");

                    sql
                            .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' )  ) G1,");
                    sql
                            .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' )  ) G2,");
                    sql
                            .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                    + StartDate + "' and '" + EndDate + "' )  ) G3");

                    sql.append(" from ldcom Z where Z.comcode ='86' ");
                    sql.append(" ) as X  order by A with ur");
                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null)
                    {
                        if (ssrs.getMaxRow() > 0)
                        {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++)
                            {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    System.out.println("处理完成第二个Sql语句~~~~~");
                }
            }
            else if (Managecom.length() == 4)
            {
                SSRS tSSRS_M;
                //                String Mng = tExeSQL
                //                        .getOneValue("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='"
                //                                + Managecom + "' order by ComCode");

                String Mng = Managecom;

                sql = new StringBuffer();
                sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng + "'),A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(" Z.riskcode A, ");
                sql
                        .append(" (select count(distinct prtno) from lcgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' ) ) C1, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' ) ) C2, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' ) ) C3, ");

                sql
                        .append(" (select count(distinct (b.APPNTNo)) from lcgrppol a,lcgrpcont b where a.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' and a.riskcode=Z.riskcode ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' and a.riskcode=Z.riskcode ) D2,");

                sql
                        .append(" (select count(distinct (insuredno)) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) F3,");

                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) G1,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) G2,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) G3");

                sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(" select (select showname from ldcom where sign='1' and comcode= '" + Mng + "'),A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" '总计' A,'总计' B, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "'  )) C1, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "'  )) C2, ");

                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lbgrpcont   where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "'  )) C3, ");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where a.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' ) D2,");

                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) F3,");

                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) G1,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) G2,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) G3");

                sql.append(" from ldcom Z where Z.comcode ='" + Mng + "' ");
                sql.append(" ) as X  order by A with ur");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null)
                {
                    if (ssrs.getMaxRow() > 0)
                    {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++)
                        {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");

                sql = new StringBuffer();
                sql.append(" select '总计',A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(" Z.riskcode A, ");
                sql
                        .append(" (select count(distinct prtno) from lcgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' ) ) C1, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' ) ) C2, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' ) ) C3, ");
                sql
                        .append(" (select count(distinct b.APPNTNo) from lcgrppol a,lcgrpcont b where a.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' and a.riskcode=Z.riskcode ) D1,");
                sql
                        .append(" (select count(distinct b.APPNTNo) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "' and a.riskcode=Z.riskcode ) D2,");

                sql
                        .append(" (select count(distinct (insuredno)) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) F3,");

                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) G1,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) G2,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and riskcode=Z.riskcode ) G3");

                sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql.append(" select '总计',A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" '总计' A,'总计' B, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "'  )) C1, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont   where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "'  )) C2, ");

                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lbgrpcont   where edorno not like 'xb%' and  appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "'  )) C3, ");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where b.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "'  ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "'  ) D2,");

                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "') and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) F3,");

                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) G1,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) G2,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Mng + "')  ) G3");

                sql.append(" from ldcom Z where Z.comcode ='86' ");
                sql.append(" ) as X  order by A with ur");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null)
                {
                    if (ssrs.getMaxRow() > 0)
                    {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++)
                        {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                System.out.println("处理完成第二个Sql语句~~~~~");
            }
            else if (Managecom.length() == 8)
            {
                SSRS tSSRS_M;
                sql = new StringBuffer();
                sql
                        .append(" select (select showname from ldcom where sign='1' and comcode= '" + Managecom
                                + "'),A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" (select RiskShortName from lmrisk where riskcode=Z.riskcode) B, ");
                sql.append(" Z.riskcode A, ");
                sql
                        .append(" (select count(distinct prtno) from lcgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) ) C1, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) ) C2, ");
                sql
                        .append(" (select count(distinct prtno) from lbgrppol where appflag='1' and riskcode=Z.riskcode and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) ) C3, ");

                sql
                        .append(" (select count(distinct (b.APPNTNo)) from lcgrppol a,lcgrpcont b where a.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' and a.riskcode=Z.riskcode ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' and a.riskcode=Z.riskcode ) D2,");

                sql
                        .append(" (select count(distinct (insuredno)) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno)) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode ) F3,");

                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode ) G1,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode ) G2,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "') and riskcode=Z.riskcode ) G3");

                sql.append(" from lmriskapp Z where Z.RiskProp='G' ");
                sql.append(" ) as X ");
                sql.append(" union ");
                sql
                        .append(" select (select showname from ldcom where sign='1' and comcode= '" + Managecom
                                + "'),A,B, ");
                sql
                        .append("to_zero(C1)+to_zero(C2)+to_zero(C3),to_zero(D1)+to_zero(D2),to_zero(E1)+to_zero(E2)+to_zero(E3),to_zero(F1)+to_zero(F2)+to_zero(F3),to_zero(G1)+to_zero(G2)+to_zero(G3) ");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select ");
                sql.append(" '总计' A,'总计' B, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lcgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont   where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  )) C1, ");
                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lcgrpcont   where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  )) C2, ");

                sql
                        .append(" (select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and grpcontno in (select grpcontno from lbgrpcont   where edorno not like 'xb%' and  appflag='1' and signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "'  )) C3, ");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where a.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) D1,");
                sql
                        .append(" (select count(distinct (b.APPNTNo||a.riskcode)) from lbgrppol a,lbgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(b.managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "' ) D2,");

                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  and PolTypeFlag <>'2') E1,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where appflag='1' and   conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  and PolTypeFlag <>'2') E2,");
                sql
                        .append(" (select count(distinct (insuredno||riskcode)) from lbpol where  appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  and PolTypeFlag <>'2') E3,");

                sql
                        .append(" (select sum(prem) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  ) F1,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  ) F2,");
                sql
                        .append(" (select sum(prem) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  ) F3,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  ) G1,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  ) G2,");
                sql
                        .append(" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"
                                + StartDate
                                + "' and '"
                                + EndDate
                                + "' and substr(managecom,1,length('"
                                + Managecom
                                + "'))='" + Managecom + "')  ) G3");

                sql.append(" from ldcom Z where Z.comcode ='" + Managecom + "' ");
                sql.append(" ) as X  order by A with ur");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null)
                {
                    if (ssrs.getMaxRow() > 0)
                    {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++)
                        {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql = new StringBuffer();
            }
            
        }
        int num = result.size();
        String tContent[][] = new String[num][];
		for (int i = 0; i < num; i++) {
			String ListInfo[] = (String[])result.get(i);
			tContent[i] = ListInfo;

		}
		mCSVFileWrite.addContent(tContent);
        
        
		if (!mCSVFileWrite.writeFile()) {
			mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
			return false;
		}
		
		mCSVFileWrite.closeFile();

        return true;
    }
    
	public String getMFileName() {
		return mFileName;
	}

	public void setMFileName(String fileName) {
		mFileName = fileName;
	}

    public static void main(String[] args) {
        VData tVData = new VData();
        ARStatisticCSVBL tWorkEfficiencyBL = new ARStatisticCSVBL();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "8611";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        TransferData mTransferData = new TransferData();
        mTransferData.setNameAndValue("OperatorManagecom", "8611");
        mTransferData.setNameAndValue("Managecom", "8611");
        mTransferData.setNameAndValue("StartDate", "2013-9-22");
        mTransferData.setNameAndValue("EndDate", "2013-9-22");
        mTransferData.setNameAndValue("QYType", "3");
        tVData.add(mTransferData);
        tWorkEfficiencyBL.submitData(tVData, "PRINT");
        VData vData = tWorkEfficiencyBL.getResult();

    }
}
