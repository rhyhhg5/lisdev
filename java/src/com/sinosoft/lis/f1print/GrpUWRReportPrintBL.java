package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author cuiwei
 * @version 1.0
 */


import java.text.SimpleDateFormat;
import java.util.Date;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCGrpContDB;

import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LCGrpIssuePolDB;
import com.sinosoft.lis.vschema.LCGrpIssuePolSet;
import com.sinosoft.lis.schema.LCGrpIssuePolSchema;
import com.sinosoft.lis.schema.LCGrpAddressSchema;
import com.sinosoft.lis.db.LCGrpAppntDB;
import com.sinosoft.lis.db.LCGrpAddressDB;
import com.sinosoft.lis.db.LCRReportItemDB;
import com.sinosoft.lis.vschema.LCRReportItemSet;
import com.sinosoft.lis.schema.LCRReportItemSchema;
import com.sinosoft.lis.db.LCGrpRReportItemDB;
import com.sinosoft.lis.vschema.LCGrpRReportItemSet;
import com.sinosoft.lis.schema.LCGrpRReportItemSchema;
import com.sinosoft.lis.db.LCRReportDB;
import com.sinosoft.lis.vschema.LCRReportSet;
import com.sinosoft.lis.schema.LCRReportSchema;
import com.sinosoft.lis.db.LCGrpRReportDB;
import com.sinosoft.lis.vschema.LCGrpRReportSet;

public class GrpUWRReportPrintBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //取得的代理人编码
    private String mAgentCode = "";
    private String mGrpContNo = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LCGrpContSet mLCGrpContSet = new LCGrpContSet();
    private LCContPlanDutyParamSchema mLCContPlanDutyParamSchema = new
            LCContPlanDutyParamSchema();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LDComSchema mLDComSchema = new LDComSchema();
    private LCGrpIssuePolSchema mLCGrpIssuePolSchema = new LCGrpIssuePolSchema();
    private LCGrpIssuePolSet mLCGrpIssuePolSet = new LCGrpIssuePolSet();
    private LCGrpAddressSchema mLCGrpAddressSchema = new LCGrpAddressSchema();
    public GrpUWRReportPrintBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("CONFIRM") &&
            !cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }

        return true;
    }

    public static void main(String[] args) {
        String d = "2005-6-2";
        d = d.substring(0,
                        d.indexOf("-")) + "年" +
            d.substring(d.indexOf("-") + 1,
                        d.lastIndexOf("-")) +
            "月" + d.substring(
                    d.lastIndexOf("-") + 1) + "日";
        System.out.println(" D : " + d);
        String[] a = d.split("-");
        String c = a[0] + "年" + a[1] + "月" + a[2] + "日";

    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.
                               getObjectByObjectName(
                                       "GlobalInput", 0)
                );
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                                              "LOPRTManagerSchema", 0));
        if (mLOPRTManagerSchema == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (mLOPRTManagerSchema.getPrtSeq() == null) {
            buildError("getInputData", "没有得到足够的信息:印刷号不能为空！");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {

        //根据印刷号查询打印队列中的纪录
        String PrtNo = mLOPRTManagerSchema.getPrtSeq(); //打印流水号

        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (tLOPRTManagerDB.getInfo() == false) {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();

        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setProposalGrpContNo(mLOPRTManagerSchema.getOtherNo()); //发通知的时候把集体合同号码放入OtherNo
        int m, i;
        LCGrpContSet mLCGrpContSet = new LCGrpContSet();
        mLCGrpContSet = tLCGrpContDB.query();
        if (mLCGrpContSet.size() > 1 || mLCGrpContSet.size() == 0) {
            mErrors.copyAllErrors(tLCGrpContDB.mErrors);
            buildError("outputXML", "在取得LCCont的数据时发生错误");
            return false;
        }
        mLCGrpContSchema = mLCGrpContSet.get(1);

//        LCGrpIssuePolDB tLCGrpIssuePolDB = new  LCGrpIssuePolDB();
//        tLCGrpIssuePolDB.setProposalGrpContNo(mLOPRTManagerSchema.getOtherNo()); //发通知的时候把集体合同号码放入OtherNo
//        LCGrpIssuePolSet mLCGrpIssuePolSet = new LCGrpIssuePolSet();
//        mLCGrpIssuePolSet = tLCGrpIssuePolDB.query() ;
//        if ( mLCGrpIssuePolSet.size() == 0) {
//            mErrors.copyAllErrors(tLCGrpIssuePolDB.mErrors);
//            buildError("outputXML", "在取得LCGrpIssuePol的数据时发生错误");
//            return false;
//        }


        mAgentCode = mLCGrpContSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mAgentCode);
        if (!tLAAgentDB.getInfo()) {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "在取得LAAgent的数据时发生错误");
            return false;
        }
        mLAAgentSchema = tLAAgentDB.getSchema(); //保存代理人信息

        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mGlobalInput.ManageCom);
        if (!tLDComDB.getInfo()) {
            mErrors.copyAllErrors(tLDComDB.mErrors);
            buildError("outputXML", "在取得LDCom的数据时发生错误");
            return false;
        }
        mLDComSchema = tLDComDB.getSchema(); //机构信息
        //险种信息
        mGrpContNo = mLCGrpContSchema.getGrpContNo();

        //投保人地址和邮编
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        if (tLCGrpAppntDB.getInfo() == false) {
            mErrors.copyAllErrors(tLCGrpAppntDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }

        LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();

        tLCGrpAddressDB.setCustomerNo(mLCGrpContSchema.getAppntNo());
        tLCGrpAddressDB.setAddressNo(tLCGrpAppntDB.getAddressNo());
        if (tLCGrpAddressDB.getInfo() == false) {
            mErrors.copyAllErrors(tLCGrpAddressDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }

        mLCGrpAddressSchema = tLCGrpAddressDB.getSchema();

//        //团单下被保人生调项目
//        String[] MeetTitle = new String[3];
//        MeetTitle[0] = "ID";
//        MeetTitle[1] = "CHECKITEM";
//        MeetTitle[2] = "result";
//        ListTable tMeetListTable = new ListTable();
//        String strPE[] = null;
//        tMeetListTable.setName("CHECKITEM"); //对应模版体检部分的行对象名
//        LCRReportItemDB tLCRReportItemDB = new LCRReportItemDB();
//        LCRReportItemSet tLCRReportItemSet = new LCRReportItemSet();
//        tLCRReportItemDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
//        tLCRReportItemDB.setPrtSeq(PrtNo);
//
//        tLCRReportItemSet.set(tLCRReportItemDB.query());
//        if (tLCRReportItemSet == null || tLCRReportItemSet.size() == 0) {
//        } else {
//
//            System.out.println(tLCRReportItemSet.size());
//            for (i = 1; i <= tLCRReportItemSet.size(); i++) {
//                LCRReportItemSchema tLCRReportItemSchema = new
//                        LCRReportItemSchema();
//                tLCRReportItemSchema.setSchema(tLCRReportItemSet.get(i));
//                strPE = new String[3];
//                strPE[0] = tLCRReportItemSchema.getRReportItemCode(); //序号
//                strPE[1] = tLCRReportItemSchema.getRReportItemName(); //序号对应的内容
//                strPE[2] = "";
//                tMeetListTable.add(strPE);
//            }
//        }
//
        String[] Title = new String[3];
        Title[0] = "ID";
        Title[1] = "Name";
        Title[2] = "IDNo";
        ListTable tListTable = new ListTable();
        String pPE[] = null;
        tListTable.setName("ITEM"); //对应模版体检部分的行对象名
        LCRReportDB tLCRReportDB = new LCRReportDB();
        LCRReportSet tLCRReportSet = new LCRReportSet();
        tLCRReportDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        tLCRReportDB.setPrtSeq(PrtNo);

        tLCRReportSet.set(tLCRReportDB.query());
        if (tLCRReportSet == null || tLCRReportSet.size() == 0) {
        } else {
            System.out.println(tLCRReportSet.size());
            for (i = 1; i <= tLCRReportSet.size(); i++) {
                LCRReportSchema tLCRReportSchema = new LCRReportSchema();
                tLCRReportSchema.setSchema(tLCRReportSet.get(i));
                pPE = new String[3];
                pPE[0] = i + ""; //序号
                pPE[1] = tLCRReportSchema.getName(); //被保人姓名
                // strPE[2] = tLCRReportSchema.getCustomerNo() ; //生调人编码
                String Sql1 = "select IDNo from LCinsured where insuredno='" +
                              tLCRReportSchema.getCustomerNo() +
                              "' and IDType='0'";
                SSRS tSSRS = new SSRS();
                ExeSQL tExeSQL = new ExeSQL();
                tSSRS = tExeSQL.execSQL(Sql1);
                try {
                    pPE[2] = tSSRS.GetText(1, 1);
                } catch (Exception ex) {
                    pPE[2] = "";
                }
                tListTable.add(pPE);
            }
        }

        //团单生调项目
        String[] GrpMeetTitle = new String[4];
        GrpMeetTitle[0] = "ID";
        GrpMeetTitle[1] = "GRPCHECKITEM";
        GrpMeetTitle[2] = "EndDate";
        GrpMeetTitle[3] = "result";
        ListTable tGrpMeetListTable = new ListTable();
        String GrpstrPE[] = null;
        tGrpMeetListTable.setName("GRPCHECKITEM"); //对应模版契调部分的行对象名
        LCGrpRReportItemDB tLCGrpRReportItemDB = new LCGrpRReportItemDB();
        LCGrpRReportItemSet tLCGrpRReportItemSet = new LCGrpRReportItemSet();
        //tLCGrpRReportItemDB.setGrpContNo(mLOPRTManagerSchema.getOtherNo());
        tLCGrpRReportItemDB.setPrtSeq(PrtNo);

        tLCGrpRReportItemSet.set(tLCGrpRReportItemDB.query());
        /**
         * 用于向后面的团体契约调查表中填充 截止日期
         */
        String EndDate = "";
        if (tLCGrpRReportItemSet == null || tLCGrpRReportItemSet.size() == 0) {
            return false;
        } else {
            System.out.println(tLCGrpRReportItemSet.size());
            for (i = 1; i <= tLCGrpRReportItemSet.size(); i++) {
                LCGrpRReportItemSchema tLCGrpRReportItemSchema = new
                        LCGrpRReportItemSchema();
                tLCGrpRReportItemSchema.setSchema(tLCGrpRReportItemSet.get(i));
                GrpstrPE = new String[4];
                GrpstrPE[0] = tLCGrpRReportItemSchema.getRReportItemCode(); //序号
                GrpstrPE[1] = tLCGrpRReportItemSchema.getRReportItemName(); //序号对应的内容
                GrpstrPE[2] = tLCGrpRReportItemSchema.getEndDate();
                EndDate = tLCGrpRReportItemSchema.getEndDate();
                GrpstrPE[3] = "";
                tGrpMeetListTable.add(GrpstrPE);
            }//b.RReportItemCode,
        }
        String Sql = "select distinct b.RReportItemCode, a.Name,c.IDNo,b.RReportItemName "
                     +"from LCRReport a,LCRReportItem b ,LCinsured c where a.GrpContNo='" +
                     mLCGrpContSchema.getGrpContNo() +
                     "' and a.grpcontno=b.grpcontno and a.prtseq=b.prtseq and c.insuredno=a.customerno and a.ProposalContNo=b.ProposalContNo order by a.Name";


        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(Sql);

        String PETitle[] = new String[5];
        PETitle[0] = "ID";
        PETitle[1] = "name";
        PETitle[2] = "IDNo";
        PETitle[3] = "RReportItemName";
        PETitle[4] = "Result";
        ListTable tPEListTable = new ListTable();
        tPEListTable.setName("PE");
//        if (!(tSSRS.GetText(1, 1).equals("0") ||
//              tSSRS.GetText(1, 1).trim().equals("") ||
//              tSSRS.GetText(1, 1).equals("null"))) {

            String strITEM1[][] = new String[tSSRS.getMaxRow()][4];
            strITEM1= tSSRS.getAllData();
            String IDNo="";
            for (i = 0; i < tSSRS.getMaxRow(); i++) {
                String strITEM[] = new String[5];
                strITEM[0] =strITEM1[i][0];
                strITEM[1] =strITEM1[i][1];
                strITEM[2] =strITEM1[i][2];
                try {
                    strITEM[2] =strITEM1[i][2];
                } catch (Exception ex) {
                    strITEM[2] ="";
                }


                strITEM[3] =strITEM1[i][3];
                strITEM[4] = "";
               tPEListTable.add(strITEM);

            }

        //}
//        //查询团体生调通知总表
        String[] NameTitle = new String[2];
        NameTitle[0] = "Index";
        NameTitle[1] = "name";

        ListTable tInfoTable = new ListTable();
        tInfoTable.setName("NameInfo");
        String strNameInfo[] = null;

        LCGrpRReportDB tLCGrpRReportDB = new LCGrpRReportDB();
        LCGrpRReportSet tLCGrpRReportSet = new LCGrpRReportSet();

        tLCGrpRReportDB.setPrtSeq(PrtNo);

        tLCGrpRReportSet.set(tLCGrpRReportDB.query());

        String Commissioner = "";
        String ComName = "";
        for (int Index = 1; Index <= tLCGrpRReportSet.size(); Index++) {

            strNameInfo = new String[2];

            strNameInfo[0] = Index + "";
            System.out.println("Index : " + Index);
            strNameInfo[1] = tLCGrpRReportSet.get(Index).getCommissioner();
            Commissioner = tLCGrpRReportSet.get(Index).getCommissioner();
            String Sql2 = "select UserName from LDUser where UserCode='" +
                          Commissioner + "' and OtherPopedom='2'";
//            SSRS tSSRS = new SSRS();
//            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(Sql2);
            try {
                ComName = tSSRS.GetText(1, 1);
            } catch (Exception ex) {
                ComName = "";
            }

            tInfoTable.add(strNameInfo);

        }

        //其它模版上单独不成块的信息
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("GrpRReport.vts", "printer"); //最好紧接着就初始化xml文档
        //生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();

        //模版自上而下的元素
//        if (mLCGrpContSchema.getPayMode() == null) {
//            xmlexport.addDisplayControl("displaymoney");
//        } else {
//            if (mLCGrpContSchema.getPayMode().equals("4")) {
//                xmlexport.addDisplayControl("displaybank");
//            } else {
//                xmlexport.addDisplayControl("displaymoney");
//            }
//        }
        String sql4 =
                " select PostalAddress,ZipCode,Phone from LCGrpAppnt where grpContNo='"
                + mLCGrpContSchema.getGrpContNo() + "'";
        SSRS GrpInfoSSRS = null;
        String PostalAddress = "";
        String AppntZipcode = "";
        String Phone = "";
        ExeSQL q_exesql = new ExeSQL();
        try {
            GrpInfoSSRS = q_exesql.execSQL(sql4);
            PostalAddress = GrpInfoSSRS.GetText(1, 1);
            AppntZipcode = GrpInfoSSRS.GetText(1, 2);
            Phone = GrpInfoSSRS.GetText(1, 3);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //int prtNo = Integer.parseInt(PubFun1.CreateMaxNo("GrpFirstPay",2))+1;
        texttag.add("BarCode1", mLOPRTManagerSchema.getPrtSeq());
        texttag.add("BarCodeParam1",
                    "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

        texttag.add("BarCode1", mLOPRTManagerSchema.getPrtSeq());
        texttag.add("BarCodeParam1",
                    "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("AppntName", mLCGrpContSchema.getGrpName()); //单位名称
        texttag.add("AppntAddr", PostalAddress); //联系地址
        texttag.add("AppntZipcode", AppntZipcode); //邮政编码
        texttag.add("GrpPhone", Phone); //单位电话
        texttag.add("EndDate", EndDate); //截止时间
        texttag.add("Commissioner", ComName); //截止时间

        texttag.add("HandlerName", mLCGrpContSchema.getHandlerName()); //收件人
        if (mLCGrpContSchema.getPolApplyDate() != null) {
            String[] ApplyDate = mLCGrpContSchema.getPolApplyDate().split("-");
            texttag.add("ApplyDate",
                        ApplyDate[0] + "年" + ApplyDate[1] + "月" + ApplyDate[2] +
                        "日"); //投保经办人
        } else {
            texttag.add("ApplyDate", ""); //投保日期
        }

        texttag.add("Letterservicename", mLDComSchema.getLetterServiceName()); //印刷号
        texttag.add("Address", mLDComSchema.getLetterServicePostAddress()); //印刷号
        texttag.add("ZipCode", mLDComSchema.getZipCode()); //邮编
        texttag.add("Fax", mLDComSchema.getFax()); //传真
        texttag.add("ComPhone", mLDComSchema.getPhone()); //机构电话
        texttag.add("ServicePhone", mLDComSchema.getServicePhone()); //客户服务电话
        texttag.add("AgentName", mLAAgentSchema.getName()); //代理人姓名
        texttag.add("LCCont.AgentCode", mLAAgentSchema.getGroupAgentCode()); //代理人业务号
        texttag.add("LCCont.PrtNo", mLCGrpContSchema.getPrtNo()); //印刷号
        texttag.add("AgentMobile", mLAAgentSchema.getPhone()); //代理人电话
        texttag.add("PrtNo", mLCGrpContSchema.getPrtNo()); //印刷号

        texttag.add("GrpAddress", mLCGrpAddressSchema.getGrpAddress()); //单位地址
        texttag.add("LinkMan", mLCGrpAddressSchema.getLinkMan1()); //联系人姓名
        texttag.add("Phone", mLCGrpAddressSchema.getPhone1()); //联系人电话

        //缴费日期与险种相关，把所有的险种和险种对应的交至日期
        String Today = PubFun.getCurrentDate();
        String payToDateSql = "select date('" + Today + "')+15 day from dual";
        Today = q_exesql.getOneValue(payToDateSql);
        String[] a = Today.split("-");
        texttag.add("PayToDate", a[0] + "年" + a[1] + "月" + a[2] +
                    "日");
//                    Today.substring(0,
//                                    Today.indexOf("-")) + "年" +
//                    Today.substring(Today.indexOf("-") + 1,
//                                    Today.lastIndexOf("-")) +
//                    "月" + Today.substring(
//                            Today.lastIndexOf("-") + 1) + "日");
        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
        texttag.add("Today", df.format(new Date()));

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        //保存体检信息

      xmlexport.addListTable(tListTable, Title); //保存体检信息
        // xmlexport.addListTable(tListTable, ITEM); //生调人项目信息
         xmlexport.addListTable(tGrpMeetListTable, GrpMeetTitle); //团单下生调信息
         xmlexport.addListTable(tPEListTable, PETitle); //保存团单下被保人生调信息

        xmlexport.outputDocumentToFile("D:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }


}
