package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.*;

public class LAJXRiskWageBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //取得的代理人编码
    private String mStartDate = "";

//取得的机构外部代码 
    private String mBranchAttr = "";

//取得的代理人离职日期
    private String mEndDate = "";

//取得的管理机构代码
    private String mManageCom = "";
//取得当前日期
    private String mdate = "";
 //取得当期时间
    private String mtime = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
//    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
//    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
//    private LDComSchema mLDComSchema = new LDComSchema();

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
    	 mGlobalInput.setSchema((GlobalInput) cInputData.
                 getObjectByObjectName("GlobalInput", 0));
    	mManageCom = (String)cInputData.get(0);
    	mBranchAttr = (String)cInputData.get(1);
    	mStartDate = (String)cInputData.get(2);
    	mEndDate = (String)cInputData.get(3);   	
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {       
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
        txmlexport.createDocument("LAJXRiskWage.vts", "printer"); //最好紧接着就初始化xml文档
        ListTable tListTable = new ListTable();
        tListTable.setName("LAJX");
        ListTable mListTable = new ListTable();
        mListTable.setName("SUM");//合计行
        String[] title = {"", "", "", "", "" ,"","","","","",""};      
    	String tAgentGroup = "";
    	String SQL = "";
        double PremSum = 0.00;
        int jianshuSum = 0;
        double tuibaoPremSum = 0.00;
        int tuibaoJianSum = 0;
        double firstFycSum = 0.00;
        double nextFycSum = 0.00;
        double xubaoFycSum = 0.00;
        int customNumSum = 0;
        if(this.mBranchAttr!=null&&!this.mBranchAttr.equals(""))
        {     	
		        //查找销售过的险种,然后进行循环
		        String riskCodeSQL = "select distinct(riskcode) from lacommision where  branchattr like '"+this.mBranchAttr+"%' and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"' and branchtype='1' and branchtype2='03'";
		       	SSRS tSSRS = new SSRS();		       	       	    	  	    		 
    	    	ExeSQL tExeSQL = new ExeSQL();
    	    	tSSRS = tExeSQL.execSQL(riskCodeSQL);
                // 总保费
  	    		 String sumPremSQL = "select value(sum(transmoney),0) from lacommision where   branchattr like '"+this.mBranchAttr+"%' and branchtype='1' and branchtype2='03' and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"'";
  	    	     String sumPrem = tExeSQL.getOneValue(sumPremSQL);	
    	    	System.out.println(riskCodeSQL);
    	    	if(tSSRS.getMaxRow()>0)
    	    	{
       	    		for(int j =1;j<=tSSRS.getMaxRow();j++)
       	    		{
       	    			double rate = 0.0;
       	    		//保费
    	    		 String PremSQL = "select value(sum(transmoney),0) from lacommision where  branchattr like '"+this.mBranchAttr+"%' and  riskcode='"+tSSRS.GetText(j, 1)+"' and branchtype='1' and branchtype2='03' and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"'";
       	    	     String Prem = tExeSQL.getOneValue(PremSQL);
       	    	     //件数
       	    	     String nextPremSQL ="select count(distinct(grpcontno)) from lacommision where  branchattr like '"+this.mBranchAttr+"%' and  riskcode='"+tSSRS.GetText(j, 1)+"' and branchtype='1' and branchtype2='03'and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"'";
       	    	     String jianshu = tExeSQL.getOneValue(nextPremSQL);
       	    	     //退保保费
       	    	     String tuibaoPremSQL = "select value(sum(transmoney),0) from lacommision where branchattr like '"+this.mBranchAttr+"%' and riskcode='"+tSSRS.GetText(j, 1)+"' and transtype='WT' and branchtype='1' and branchtype2='03' and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"'";
       	    	     String tuibaoPrem = tExeSQL.getOneValue(tuibaoPremSQL);
       	    	     //新单客户数
       	    	     String customNumSQL = "select count(distinct(appntno)) from lacommision where paycount=0 and renewcount=0 and branchattr like '"+this.mBranchAttr+"%' and riskcode='"+tSSRS.GetText(j, 1)+"' and branchtype='1' and branchtype2='03' and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"'";
       	    	     String customNum = tExeSQL.getOneValue(customNumSQL);
       	    	     //退保件数
       	    	     String tuibaoJianSQL = "select count(distinct(grpcontno)) from lacommision where  branchattr like '"+this.mBranchAttr+"%' and riskcode='"+tSSRS.GetText(j, 1)+"' and transtype='WT' and branchtype='1' and branchtype2='03' and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"'";
       	    	     String tuibaoJian = tExeSQL.getOneValue(tuibaoJianSQL);
                     //新单佣金
       	    	     String firstFycSQL = "select value(sum(fyc),0) from lacommision where paycount=0 and renewcount=0 and branchattr like '"+this.mBranchAttr+"%' and riskcode='"+tSSRS.GetText(j, 1)+"' and branchtype='1' and branchtype2='03' and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"'";
    	    	     String firstFyc = tExeSQL.getOneValue(firstFycSQL);
                     //续年度佣金
       	    	     String nextFycSQL = "select value(sum(fyc),0) from lacommision where payyear>0 and renewcount=0 and branchattr like '"+this.mBranchAttr+"%' and riskcode='"+tSSRS.GetText(j, 1)+"' and branchtype='1' and branchtype2='03' and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"'";
       	    	     String nextFyc = tExeSQL.getOneValue(nextFycSQL);
       	    	     //续保佣金
       	    	     String xubaoFycSQL ="select value(sum(fyc),0) from lacommision where renewcount>0 and branchattr like '"+this.mBranchAttr+"%' and riskcode='"+tSSRS.GetText(j, 1)+"' and branchtype='1' and branchtype2='03' and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"'";
       	    	     String xubaoFyc = tExeSQL.getOneValue(xubaoFycSQL);
       	    	     //保费占比
       	    	     rate = Arith.div(Double.parseDouble(Prem),Double.parseDouble(sumPrem));
       	    	     rate = Arith.round(rate*100, 2);
        	    	 String Rate = String.valueOf(rate);       	    
       	    	     String info[] = new String[11];
       	    	     info[0] = tSSRS.GetText(j, 1);
       	    	     info[1] = getRiskName(tSSRS.GetText(j, 1));
       	    	     info[2] = Prem;
       	    	     info[3] = jianshu;
       	    	     info[4] = tuibaoPrem;       	    	     
       	    	     info[5] = tuibaoJian;
    	    	     info[6] = firstFyc;
    	    	     info[7] = nextFyc;
    	    	     info[8] = xubaoFyc;
    	    	     info[9] = customNum;
    	    	     info[10] = Rate;
       	    	     tListTable.add(info);
       	    	     PremSum+=Double.parseDouble(Prem);
       	    	     jianshuSum+=Integer.parseInt(jianshu);
       	    	     tuibaoPremSum+=Double.parseDouble(tuibaoPrem);
       	    	     tuibaoJianSum+=Integer.parseInt(tuibaoJian);
       	    	     firstFycSum+=Double.parseDouble(firstFyc);
       	    	     nextFycSum+=Double.parseDouble(nextFyc);
       	    	     xubaoFycSum+=Double.parseDouble(xubaoFyc);
       	    	     customNumSum+=Integer.parseInt(customNum);      	    	           	    	     
       	    	 }
       	     }
    	   }   
        else
        {
        	  //查找销售过的险种,然后进行循环
	        String riskCodeSQL = "select distinct(riskcode) from lacommision where  managecom  like '"+this.mManageCom+"%' and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"' and branchtype='1' and branchtype2='03'";
	       	SSRS tSSRS = new SSRS();		       	       	    	  	    		 
	    	ExeSQL tExeSQL = new ExeSQL();
	    	tSSRS = tExeSQL.execSQL(riskCodeSQL);
            // 总保费
	    		 String sumPremSQL = "select value(sum(transmoney),0) from lacommision where   managecom  like '"+this.mManageCom+"%'  and branchtype='1' and branchtype2='03' and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"'";
	    	     String sumPrem = tExeSQL.getOneValue(sumPremSQL);	
	    	System.out.println(riskCodeSQL);
	    	if(tSSRS.getMaxRow()>0)
	    	{
   	    		for(int j =1;j<=tSSRS.getMaxRow();j++)
   	    		{
   	    			double rate = 0.0;
   	    		//保费
	    		 String PremSQL = "select value(sum(transmoney),0) from lacommision where  managecom  like '"+this.mManageCom+"%' and  riskcode='"+tSSRS.GetText(j, 1)+"' and branchtype='1' and branchtype2='03' and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"'";
   	    	     String Prem = tExeSQL.getOneValue(PremSQL);
   	    	     //件数
   	    	     String nextPremSQL ="select count(distinct(grpcontno)) from lacommision where  managecom  like '"+this.mManageCom+"%' and  riskcode='"+tSSRS.GetText(j, 1)+"' and branchtype='1' and branchtype2='03'and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"'";
   	    	     String jianshu = tExeSQL.getOneValue(nextPremSQL);
   	    	     //退保保费
   	    	     String tuibaoPremSQL = "select value(sum(transmoney),0) from lacommision where managecom  like '"+this.mManageCom+"%' and riskcode='"+tSSRS.GetText(j, 1)+"' and transtype='WT' and branchtype='1' and branchtype2='03' and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"'";
   	    	     String tuibaoPrem = tExeSQL.getOneValue(tuibaoPremSQL);
   	    	     //新单客户数
   	    	     String customNumSQL = "select count(distinct(appntno)) from lacommision where paycount=0 and renewcount=0 and managecom  like '"+this.mManageCom+"%' and riskcode='"+tSSRS.GetText(j, 1)+"' and branchtype='1' and branchtype2='03' and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"'";
   	    	     String customNum = tExeSQL.getOneValue(customNumSQL);
   	    	     //退保件数
   	    	     String tuibaoJianSQL = "select count(distinct(grpcontno)) from lacommision where  managecom  like '"+this.mManageCom+"%' and riskcode='"+tSSRS.GetText(j, 1)+"' and transtype='WT' and branchtype='1' and branchtype2='03' and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"'";
   	    	     String tuibaoJian = tExeSQL.getOneValue(tuibaoJianSQL);
                 //新单佣金
   	    	     String firstFycSQL = "select value(sum(fyc),0) from lacommision where paycount=0 and renewcount=0 and managecom  like '"+this.mManageCom+"%' and riskcode='"+tSSRS.GetText(j, 1)+"' and branchtype='1' and branchtype2='03' and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"'";
	    	     String firstFyc = tExeSQL.getOneValue(firstFycSQL);
                 //续年度佣金
   	    	     String nextFycSQL = "select value(sum(fyc),0) from lacommision where payyear>0 and renewcount=0 and managecom  like '"+this.mManageCom+"%' and riskcode='"+tSSRS.GetText(j, 1)+"' and branchtype='1' and branchtype2='03' and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"'";
   	    	     String nextFyc = tExeSQL.getOneValue(nextFycSQL);
   	    	     //续保佣金
   	    	     String xubaoFycSQL ="select value(sum(fyc),0) from lacommision where  renewcount>0 and managecom  like '"+this.mManageCom+"%' and riskcode='"+tSSRS.GetText(j, 1)+"' and branchtype='1' and branchtype2='03' and tmakedate>='"+mStartDate+"' and tmakedate<='"+this.mEndDate+"'";
   	    	     String xubaoFyc = tExeSQL.getOneValue(xubaoFycSQL);
   	    	     //保费占比
   	    	     rate = Arith.div(Double.parseDouble(Prem),Double.parseDouble(sumPrem));
   	    	     rate = Arith.round(rate*100, 2);
    	    	 String Rate = String.valueOf(rate);       	    
   	    	     String info[] = new String[11];
   	    	     info[0] = tSSRS.GetText(j, 1);
   	    	     info[1] = getRiskName(tSSRS.GetText(j, 1));
   	    	     info[2] = Prem;
   	    	     info[3] = jianshu;
   	    	     info[4] = tuibaoPrem;
   	    	     info[5] = tuibaoJian;
   	    	     info[6] = firstFyc;
   	    	     info[7] = nextFyc;
   	    	     info[8] = xubaoFyc;
   	    	     info[9] = customNum;
   	    	     info[10] = Rate;
   	    	     tListTable.add(info);
   	    	     PremSum+=Double.parseDouble(Prem);
   	    	     jianshuSum+=Integer.parseInt(jianshu);
   	    	     tuibaoPremSum+=Double.parseDouble(tuibaoPrem);
   	    	     tuibaoJianSum+=Integer.parseInt(tuibaoJian);
   	    	     firstFycSum+=Double.parseDouble(firstFyc);
   	    	     nextFycSum+=Double.parseDouble(nextFyc);
   	    	     xubaoFycSum+=Double.parseDouble(xubaoFyc);
   	    	     customNumSum+=Integer.parseInt(customNum);      	    	           	    	     
   	    	 }
   	     }
        }
    	   //求合计
           String sum[] = new String[11];
           sum[0] = "合  计";
           sum[1] = "";
           sum[2] = String.valueOf(Arith.round(PremSum, 2));
           sum[3] = String.valueOf(jianshuSum);
           sum[4] = String.valueOf(Arith.round(tuibaoPremSum,2));
           sum[5] = String.valueOf(tuibaoJianSum);
           sum[6] = String.valueOf(Arith.round(firstFycSum,2));
           sum[7] = String.valueOf(Arith.round(nextFycSum,2));
           sum[8] = String.valueOf(Arith.round(xubaoFycSum,2));     
           sum[9] = String.valueOf(customNumSum);
           sum[10] = "100";
           mListTable.add(sum);         
       if(tListTable.size()<=0)
       {
    	   CError tCError = new CError();
           tCError.moduleName = "MakeXMLBL";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "没有需要统计的承保信息！";
           this.mErrors.addOneError(tCError);
           return false; 
       }
         mdate = PubFun.getCurrentDate();
         mtime = PubFun.getCurrentTime();
         texttag.add("CurrentDate", mdate); //日期
         texttag.add("CurrentTime", mtime);
         texttag.add("tStartDate",this.mStartDate);
         texttag.add("tEndDate", this.mEndDate);
         texttag.add("tName", getComName(this.mManageCom));
        // texttag.add("tManageComName", this.mGlobalInput.ManageCom);
        if (texttag.size() > 0)
        {
            txmlexport.addTextTag(texttag);
        }
        txmlexport.addListTable(tListTable,title);
        txmlexport.addListTable(mListTable,title);
        mResult.clear();
        mResult.addElement(txmlexport);
        return true;
    }
   private String getRiskName(String cRiskCode)
   {
	  String riskSQL = "select riskname from lmriskapp where riskcode='"+cRiskCode+"'";
	  ExeSQL tExeSQL = new ExeSQL();
	  String riskName = tExeSQL.getOneValue(riskSQL);
	   if(riskName==null||riskName.equals(""))
	   {
		   mErrors.copyAllErrors(tExeSQL.mErrors);
           return "";
	   }
	   return riskName;
   }
    private String getComName(String strComCode)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();

    }
}
