package com.sinosoft.lis.f1print;

/**
 * <p>Title: LLCaseStatisticsBL</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author MN
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

public class LLCaseStatisticsBL {
    public LLCaseStatisticsBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mStartDate = "";

    private String mEndDate = "";

    private String mManageCom = "";

    private String mManageComName = "";

    private String mContType = "";

    private String mCon = "";

    private String moperator = "";

    private long mStartDays = 0;

    private long mEndDays = 0;

    private String[][] mShowDataList = null;

    private XmlExport mXmlExport = null;

    private String[] mDataList = null;

    private ListTable mListTable = new ListTable();

    private String currentDate = PubFun.getCurrentDate();

    private String mFileNameB = "";

    private String mMakeDate = "";

    private TransferData mTransferData = new TransferData();
    
    /** 统计类型 */
	private String mStatsType = "";


    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!checkData()) {
            return true;
        } else{
        	 // 进行数据查询
            if (!queryData()) {
                return false;
            } else {
                TransferData tTransferData = new TransferData();
                tTransferData.setNameAndValue("tFileNameB", mFileNameB);
                tTransferData.setNameAndValue("tMakeDate", mMakeDate);
                tTransferData.setNameAndValue("tOperator", moperator);
                LLPrintSave tLLPrintSave = new LLPrintSave();
                VData tVData = new VData();
                tVData.addElement(tTransferData);
                if (!tLLPrintSave.submitData(tVData, "")) {
                    return false;
                }
            }
        }
       
        return true;
    }

    /**
     * 得到表示数据列表
     *
     * @return boolean
     */
    private boolean getSumDataList(String tComGrade) {
        // double tSumPeople1 = 0;
        double tSumPeople = 0;
        double tSumPrem1 = 0;
        double tSumActuPay1 = 0;
        double tSumTemPrem1 = 0;
        // double tSumCaseNum1 = 0;
        double tSumRealPay1 = 0;

        // double tSumPeople2 = 0;
        double tSumPrem2 = 0;
        double tSumActuPay2 = 0;
        double tSumTemPrem2 = 0;
        // double tSumCaseNum2 = 0;
        double tSumRealPay2 = 0;
        double tSumPeople11 = 0; //不含特需所有承保人数
        double tSumPeople22 = 0; //含特需所有承保人数

        String tSQL = "";

        if(tComGrade.equals("02") || tComGrade == "02"){
        // 1、得到全部已开业的机构
	        tSQL = "SELECT  b.comcode,b.name,a.riskcode,a.riskname  from ldcom B,lmriskapp A where B.sign='1'" 
	        	   + " and  length(trim(comcode))=4"
	               + "  AND  B.comcode LIKE '"
	               + mManageCom
	               + "%' and A.riskprop='"
	               + mContType
	               + "' order by b.comcode,a.riskcode with ur";
        }else{
//        	 1、得到全部已开业的机构
            tSQL = "SELECT  b.comcode,b.name,a.riskcode,a.riskname  from ldcom B,lmriskapp A where B.sign='1'" 
            	   + " and  length(trim(comcode))>4 and length(trim(comcode))= 8 "
                   + "  AND  B.comcode LIKE '"
                   + mManageCom
                   + "%' and A.riskprop='"
                   + mContType
                   + "' order by b.comcode,a.riskcode with ur";
        }
        /*
         * if (mManageCom.length() == 4) { tSQL = "SELECT
         * b.comcode,b.name,a.riskcode,a.riskname from ldcom B,lmriskapp A where
         * B.sign='1' " + " AND B.comcode LIKE '" + mManageCom + "%' AND
         * b.comcode <> '"+mManageCom+"0000"+"'and A.riskprop='" + mContType + "'
         * order by b.comcode,a.riskcode with ur"; }
         */

        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tSSRS.getMaxRow() <= 0) {
            buildError("queryData", "没有附和条件的机构和险种信息！");
            return false;
        }

        // 2、查询需要表示的数据
        // String[][] tShowDataList = new String[tSSRS.getMaxRow()][4];
        String tManageCom = "";
        String tComName = "";
        String tRiskCode = "";
        String tRiskName = "";
        double tempSum1[] = new double[6]; // 临时团险特需合计
        double tempSum2[] = new double[6]; // 临时团险无特需合计
        double tempSum3[] = new double[6]; // 临时个险合计
        DecimalFormat tDF1 = new DecimalFormat("0.##");
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) { // 循环机构进行统计
            String Info[] = new String[10];
            tManageCom = tSSRS.GetText(i, 1);
            tComName = tSSRS.GetText(i, 2);
            tRiskCode = tSSRS.GetText(i, 3);
            tRiskName = tSSRS.GetText(i, 4);

            if (i != 1 && !tManageCom.equals(tSSRS.GetText(i - 1, 1))) {
                if (mContType.equals("G")) {
                    String sumInfo1[] = new String[10];
                    sumInfo1[0] = tSSRS.GetText(i - 1, 2);
                    sumInfo1[1] = "";
                    sumInfo1[2] = "不含特需合计";
                    sumInfo1[3] = String.valueOf((int) tempSum2[0]);
                    sumInfo1[4] = String.valueOf(tDF1.format(tempSum2[1]));
                    // sumInfo1[5] = String.valueOf(tDF1.format(tempSum2[2]));
                    sumInfo1[5] = String.valueOf(tDF1.format(tempSum2[3]));
                    sumInfo1[6] = getComCaseNum1(tSSRS.GetText(i - 1, 1));
                    sumInfo1[7] = String.valueOf(tDF1.format(tempSum2[5]));
                    sumInfo1[8] = tSSRS.GetText(i - 1, 1);
                    mListTable.add(sumInfo1);
                    tSumPeople11 += Double.parseDouble(sumInfo1[3]);
                    tempSum2 = null;
                    tempSum2 = new double[6];
                    String sumInfo2[] = new String[10];
                    sumInfo2[0] = tSSRS.GetText(i - 1, 2);
                    sumInfo2[1] = "";
                    sumInfo2[2] = "含特需合计";
                    sumInfo2[3] = String.valueOf((int) tempSum1[0]);
                    sumInfo2[4] = String.valueOf(tDF1.format(tempSum1[1]));
                    //sumInfo2[5] = String.valueOf(tDF1.format(tempSum1[2]));
                    sumInfo2[5] = String.valueOf(tDF1.format(tempSum1[3]));
                    sumInfo2[6] = getComCaseNum2(tSSRS.GetText(i - 1, 1));
                    sumInfo2[7] = String.valueOf(tDF1.format(tempSum1[5]));
                    sumInfo2[8] = tSSRS.GetText(i - 1, 1);
                    mListTable.add(sumInfo2);
                    tSumPeople22 += Double.parseDouble(sumInfo2[3]);
                    tempSum1 = null;
                    tempSum1 = new double[6];
                    tSumPeople += Double.parseDouble(sumInfo2[3]);
                    tSumPrem1 += Double.parseDouble(sumInfo1[4]);

                    //tSumActuPay1 += Double.parseDouble(sumInfo1[5]);
                    tSumTemPrem1 += Double.parseDouble(sumInfo1[5]);
                    tSumRealPay1 += Double.parseDouble(sumInfo1[7]);

                    tSumPrem2 += Double.parseDouble(sumInfo2[4]);
                    //tSumActuPay2 += Double.parseDouble(sumInfo2[5]);
                    tSumTemPrem2 += Double.parseDouble(sumInfo2[5]);
                    tSumRealPay2 += Double.parseDouble(sumInfo2[7]);

                } else {
                    String sumInfo[] = new String[10];
                    sumInfo[0] = tSSRS.GetText(i - 1, 2);
                    sumInfo[1] = "";
                    sumInfo[2] = "合计";
                    sumInfo[3] = String.valueOf((int) tempSum3[0]);
                    sumInfo[4] = String.valueOf(tDF1.format(tempSum3[1]));
                    //sumInfo[5] = String.valueOf(tDF1.format(tempSum3[2]));
                    sumInfo[5] = String.valueOf(tDF1.format(tempSum3[3]));
                    sumInfo[6] = getComCaseNum(tSSRS.GetText(i - 1, 1));
                    sumInfo[7] = String.valueOf(tDF1.format(tempSum3[5]));
                    sumInfo[8] = tSSRS.GetText(i - 1, 1);
                    mListTable.add(sumInfo);
                    tempSum3 = null;
                    tempSum3 = new double[6];
                    tSumPeople += Double.parseDouble(sumInfo[3]);
                    tSumPrem1 += Double.parseDouble(sumInfo[4]);
                    //tSumActuPay1 += Double.parseDouble(sumInfo[5]);
                    tSumTemPrem1 += Double.parseDouble(sumInfo[5]);
                    tSumRealPay1 += Double.parseDouble(sumInfo[7]);

                }
            }
            Info[0] = tComName;
            Info[1] = tRiskCode;
            Info[2] = tRiskName;
            Info[3] = getPeoples(tManageCom, tRiskCode);
            Info[4] = getPrem(tManageCom, tRiskCode);
            // Info[5] = getActuPay(tManageCom, tRiskCode);
            Info[5] = getTemPrem(tManageCom, tRiskCode);
            Info[6] = getCaseNum(tManageCom, tRiskCode);
            Info[7] = getRealPay(tManageCom, tRiskCode);
            Info[8] = tManageCom;
            mListTable.add(Info);
            if (mContType.equals("G")) { // 团体险种类型

                String ttsql =
                        "SELECT count(*) FROM lmriskapp WHERE riskcode='" +
                        tRiskCode +
                        "' AND risktype3='7' AND risktype <>'M' with ur";
                ExeSQL tExeSQLtt = new ExeSQL();
                String tCount1 = tExeSQLtt.getOneValue(ttsql);

                tempSum1[0] += Double.parseDouble(Info[3]);
                tempSum1[1] += Double.parseDouble(Info[4]);
                // tempSum1[2] += Double.parseDouble(Info[5]);
                tempSum1[3] += Double.parseDouble(Info[5]);
                tempSum1[4] += Double.parseDouble(Info[6]);
                tempSum1[5] += Double.parseDouble(Info[7]);

                if (tCount1.equals("0")) { // 含特需合计
                    tempSum2[0] += Double.parseDouble(Info[3]);
                    tempSum2[1] += Double.parseDouble(Info[4]);
                    //tempSum2[2] += Double.parseDouble(Info[5]);
                    tempSum2[3] += Double.parseDouble(Info[5]);
                    tempSum2[4] += Double.parseDouble(Info[6]);
                    tempSum2[5] += Double.parseDouble(Info[7]);

                }
            } else { // 个体险种类型

                tempSum3[0] += Double.parseDouble(Info[3]);
                tempSum3[1] += Double.parseDouble(Info[4]);
                //tempSum3[2] += Double.parseDouble(Info[5]);
                tempSum3[3] += Double.parseDouble(Info[5]);
                tempSum3[4] += Double.parseDouble(Info[6]);
                tempSum3[5] += Double.parseDouble(Info[7]);
            }
            System.out.println("这是第 " + i + "个");
        }

        // 最后一个分公司的合计
        System.out.println("最后一个分公司的合计...");
        if (mContType.equals("G")) {
            String sumInfo1[] = new String[10];
            sumInfo1[0] = tComName;
            sumInfo1[1] = "";
            sumInfo1[2] = "不含特需合计";
            sumInfo1[3] = String.valueOf((int) tempSum2[0]);
            sumInfo1[4] = String.valueOf(tDF1.format(tempSum2[1]));
            //sumInfo1[5] = String.valueOf(tDF1.format(tempSum2[2]));
            sumInfo1[5] = String.valueOf(tDF1.format(tempSum2[3]));
            sumInfo1[6] = getComCaseNum1(tManageCom);
            sumInfo1[7] = String.valueOf(tDF1.format(tempSum2[5]));
            sumInfo1[8] = tManageCom;
            mListTable.add(sumInfo1);
            String sumInfo2[] = new String[10];
            tSumPeople11 += Double.parseDouble(sumInfo1[3]);
            tempSum2 = null;
            tempSum2 = new double[6];
            sumInfo2[0] = tComName;
            sumInfo2[1] = "";
            sumInfo2[2] = "含特需合计";
            sumInfo2[3] = String.valueOf((int) tempSum1[0]);
            sumInfo2[4] = String.valueOf(tDF1.format(tempSum1[1]));
            //sumInfo2[5] = String.valueOf(tDF1.format(tempSum1[2]));
            sumInfo2[5] = String.valueOf(tDF1.format(tempSum1[3]));
            sumInfo2[6] = getComCaseNum2(tManageCom);
            sumInfo2[7] = String.valueOf(tDF1.format(tempSum1[5]));
            sumInfo2[8] = tManageCom;
            mListTable.add(sumInfo2);
            tSumPeople22 += Double.parseDouble(sumInfo2[3]);
            tempSum1 = null;
            tempSum1 = new double[6];
            tSumPeople += Double.parseDouble(sumInfo2[3]);
            tSumPrem1 += Double.parseDouble(sumInfo1[4]);
            //tSumActuPay1 += Double.parseDouble(sumInfo1[5]);
            tSumTemPrem1 += Double.parseDouble(sumInfo1[5]);
            tSumRealPay1 += Double.parseDouble(sumInfo1[7]);

            tSumPrem2 += Double.parseDouble(sumInfo2[4]);
            //tSumActuPay2 += Double.parseDouble(sumInfo2[5]);
            tSumTemPrem2 += Double.parseDouble(sumInfo2[5]);
            tSumRealPay2 += Double.parseDouble(sumInfo2[7]);

        } else {
            String sumInfo[] = new String[10];
            sumInfo[0] = tComName;
            sumInfo[1] = "";
            sumInfo[2] = "合计";
            sumInfo[3] = String.valueOf((int) tempSum3[0]);
            sumInfo[4] = String.valueOf(tDF1.format(tempSum3[1]));
            //sumInfo[5] = String.valueOf(tDF1.format(tempSum3[2]));
            sumInfo[5] = String.valueOf(tDF1.format(tempSum3[3]));
            sumInfo[6] = getComCaseNum(tManageCom);
            sumInfo[7] = String.valueOf(tDF1.format(tempSum3[5]));
            sumInfo[8] = tManageCom;
            mListTable.add(sumInfo);
            tempSum3 = null;
            tempSum3 = new double[6];
            tSumPeople += Double.parseDouble(sumInfo[3]);
            tSumPrem1 += Double.parseDouble(sumInfo[4]);
            //tSumActuPay1 += Double.parseDouble(sumInfo[5]);
            tSumTemPrem1 += Double.parseDouble(sumInfo[5]);
            tSumRealPay1 += Double.parseDouble(sumInfo[7]);

        }

        tSQL = "SELECT riskcode,riskname FROM  lmriskapp A WHERE  A.riskprop='"
               + mContType + "' ORDER BY riskcode with ur";
        tSSRS = new SSRS();
        tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            System.out.println("循环险种进行统计第 " + i); // 循环 险种进行统计
            tRiskCode = tSSRS.GetText(i, 1);
            tRiskName = tSSRS.GetText(i, 2);
            String Info[] = new String[10];
            Info[0] = "合计";
            Info[1] = tRiskCode;
            Info[2] = tRiskName;
            Info[3] = getPeoples(mManageCom, tRiskCode);
            Info[4] = getPrem(mManageCom, tRiskCode);
            //Info[5] = getActuPay(mManageCom, tRiskCode);
            Info[5] = getTemPrem(mManageCom, tRiskCode);
            Info[6] = getCaseNum(mManageCom, tRiskCode);
            Info[7] = getRealPay(mManageCom, tRiskCode);
            Info[8] = mManageCom;

            mListTable.add(Info);

        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        if (mContType.equals("G")) {
            // 合计所有公司所有险种不含特需
            String Info1[] = new String[10];
            Info1[0] = "合计";
            Info1[1] = "";
            Info1[2] = "不含特需合计";
            Info1[3] = tDF.format(tSumPeople11);
            Info1[4] = tDF.format(tSumPrem1);
            //Info1[5] = tDF.format(tSumActuPay1);
            Info1[5] = tDF.format(tSumTemPrem1);
            Info1[6] = getComCaseNum1(mManageCom);
            Info1[7] = tDF.format(tSumRealPay1);
            Info1[8] = mManageCom;
            mListTable.add(Info1);
            // 合计所有公司所有险种含特需
            String Info2[] = new String[10];
            Info2[0] = "合计";
            Info2[1] = "";
            Info2[2] = "含特需合计";
            Info2[3] = tDF.format(tSumPeople22);
            Info2[4] = tDF.format(tSumPrem2);
            //Info2[5] = tDF.format(tSumActuPay2);
            Info2[5] = tDF.format(tSumTemPrem2);
            Info2[6] = getComCaseNum2(mManageCom);
            Info2[7] = tDF.format(tSumRealPay2);
            Info2[8] = mManageCom;
            System.out.println("合计所有公司所有险种...");

            mListTable.add(Info2);
        } else {
            String Info[] = new String[10];
            Info[0] = "合计";
            Info[1] = "";
            Info[2] = "合计";
            Info[3] = tDF.format((int) tSumPeople);
            Info[4] = tDF.format(tSumPrem1);
            //Info[5] = tDF.format(tSumActuPay1);
            Info[5] = tDF.format(tSumTemPrem1);
            Info[6] = getComCaseNum(mManageCom);
            Info[7] = tDF.format(tSumRealPay1);
            Info[8] = mManageCom;
            mListTable.add(Info);
        }
        return true;
    }

    /**
     * 承保人数
     *
     * @param cManageCom
     *            String
     * @param cRiskCode
     *            String
     * @return String
     */
    private String getPeoples(String cManageCom, String cRiskCode) {
        //添加社保案件的判断逻辑
        String tPartSql = "";
        if("1".equals(mStatsType)){
          tPartSql = " and CHECKGRPCONT(a.grpcontno) = 'N' ";//商业保险
          System.out.println("tPartSql:"+tPartSql);
        }else if("2".equals(mStatsType)){
          tPartSql = " and CHECKGRPCONT(a.grpcontno) = 'Y' ";//社会保险
          System.out.println("tPartSql:"+tPartSql);
        }else{
          tPartSql = "";//全部
          System.out.println("tPartSql:"+tPartSql);
        }
    	
    	String tSql = null;
        if (mContType.equals("G")) {
            tSql = "SELECT SUM(a.peoples2) FROM LQCLAIMGRPPEOPLES a WHERE a.salechnl not in ('04','13') AND a.RISKCODE='"
                   + cRiskCode
                   + "'  AND "
                   + " a.managecom LIKE '"
                   + cManageCom
                   + "%' AND a.signdate>='"
                   + mStartDate
                   + "'"
                   + tPartSql
                   + " AND a.signdate<='" + mEndDate + "' with ur";
        } else {
            tSql = "SELECT sum(peoples) FROM lqclaimpol WHERE type='C' AND salechnl not in ('04','13') AND RISKCODE='"
                   + cRiskCode
                   + "'  AND "
                   + " managecom LIKE '"
                   + cManageCom
                   + "%' AND signdate>='"
                   + mStartDate
                   + "'"
                   + " AND signdate<='" + mEndDate + "' with ur";
        }
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        System.out.println("11111");
        return tRturn;
    }

    /**
     * 承保保费(团体)
     *
     * @param cManageCom
     *            String
     * @param cRiskCode
     *            String
     * @return String
     */
    private String getPremGroup(String cManageCom, String cRiskCode) {
    	//添加社保案件的判断逻辑
        String tPartSql = "";
        if("1".equals(mStatsType)){
          tPartSql = " and CHECKGRPCONT(a.grpcontno) = 'N' ";//商业保险
          System.out.println("tPartSql:"+tPartSql);
        }else if("2".equals(mStatsType)){
          tPartSql = " and CHECKGRPCONT(a.grpcontno) = 'Y' ";//社会保险
          System.out.println("tPartSql:"+tPartSql);
        }else{
          tPartSql = "";//全部
          System.out.println("tPartSql:"+tPartSql);
        }
    	
        String tSql = null;
        double tSumPrem = 0.0;
        ExeSQL tExeSQL = new ExeSQL();
        String tPrem = "";
        // 用case主要是将java计算转变为数据库计算，直接在数据库内进行判断累加，而不必将数据全部取出后累加计算
        tSql = "SELECT sum(a.prem)"
               + " FROM lqgrppremdata a WHERE a.type='C' and a.prem IS NOT NULL AND a.salechnl not in ('04','13') AND a.RISKCODE='"
               + cRiskCode
               + "'  AND "
               + " a.managecom LIKE '"
               + cManageCom
               + "%' AND a.signdate>='"
               + mStartDate
               + "'"
               + tPartSql
               + " AND a.signdate<='" + mEndDate + "' with ur";

        tPrem = tExeSQL.getOneValue(tSql);

        if (tPrem.equals("null") || tPrem == null || tPrem.equals("")) {
            tSumPrem += 0;
        } else {
            tSumPrem += Double.parseDouble(tPrem);
        }

//        tSql = "SELECT sum(a.prem)"
//               + " FROM lbgrppol a,lbgrpcont b WHERE a.grpcontno=b.grpcontno AND a.prem IS NOT NULL AND a.appflag='1' AND b.salechnl not in ('04','13') AND a.RISKCODE='"
//               + cRiskCode
//               + "'  AND "
//               + " a.managecom LIKE '"
//               + cManageCom
//               + "%' AND b.signdate>='"
//               + mStartDate
//               + "'"
//               + " AND b.signdate<='" + mEndDate + "' with ur";
//        tPrem = tExeSQL.getOneValue(tSql);
//
//        if (tPrem.equals("null")||tPrem==null||tPrem.equals("")) {
//            tSumPrem += 0;
//        } else {
//            tSumPrem += Double.parseDouble(tPrem);
//        }
//
//        tSql = "SELECT sum(a.prem)"
//               + " FROM lbgrppol a,lcgrpcont b WHERE a.grpcontno=b.grpcontno AND a.prem IS NOT NULL AND a.appflag='1' AND b.salechnl not in ('04','13') AND a.RISKCODE='"
//               + cRiskCode
//               + "'  AND "
//               + " a.managecom LIKE '"
//               + cManageCom
//               + "%' AND b.signdate>='"
//               + mStartDate
//               + "'"
//               + " AND b.signdate<='" + mEndDate + "' with ur";
//        tPrem = tExeSQL.getOneValue(tSql);
//        System.out.println(tPrem);
//
//        if (tPrem.equals("null")||tPrem==null||tPrem.equals("")) {
//            tSumPrem += 0;
//        } else {
//            tSumPrem += Double.parseDouble(tPrem);
//        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tSumPrem);
        return tRturn;
    }

    /**
     * 承保保费（个人）
     *
     * @param cManageCom
     *            String
     * @param cRiskCode
     *            String
     * @return String
     */
    private String getPremPerson(String cManageCom, String cRiskCode) {
        String tSql = null;
        double tSumPrem = 0.0;
        ExeSQL tExeSQL = new ExeSQL();
        String tPrem = "";

        tSql = "SELECT sum(prem) FROM lqclaimpol WHERE type='C' AND salechnl not in ('04','13') AND RISKCODE='"
               + cRiskCode
               + "'  AND "
               + " managecom LIKE '"
               + cManageCom
               + "%' AND signdate>='"
               + mStartDate
               + "'"
               + " AND signdate<='" + mEndDate + "' with ur";
        tPrem = tExeSQL.getOneValue(tSql);

        if (tPrem.equals("null") || tPrem == null || tPrem.equals("")) {
            tSumPrem += 0;
        } else {
            tSumPrem += Double.parseDouble(tPrem);
        }

//        tSql = "SELECT sum(prem)"
//               +
//               " FROM lbpol WHERE conttype = '1' AND appflag='1' AND salechnl not in ('04','13') AND RISKCODE='"
//               + cRiskCode
//               + "'  AND "
//               + " managecom LIKE '"
//               + cManageCom
//               + "%' AND signdate>='"
//               + mStartDate
//               + "'"
//               + " AND signdate<='" + mEndDate + "' with ur";
//        tPrem = tExeSQL.getOneValue(tSql);
//
//        if (tPrem.equals("null")||tPrem==null||tPrem.equals("")) {
//            tSumPrem += 0;
//        } else {
//            tSumPrem += Double.parseDouble(tPrem);
//        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tSumPrem);
        return tRturn;

    }

    /**
     * 承保保费
     *
     * @param cManageCom
     *            String
     * @param cRiskCode
     *            String
     * @return String
     */
    private String getPrem(String cManageCom, String cRiskCode) {
        System.out.println(PubFun.getCurrentTime());
        if (mContType.equals("G")) {
            return this.getPremGroup(cManageCom, cRiskCode);
        } else {
            return this.getPremPerson(cManageCom, cRiskCode);
        }
    }

    /**
     * 实收保费
     *
     * @param cManageCom
     *            String
     * @param cRiskCode
     *            String
     * @return String
     */
    private String getActuPay(String cManageCom, String cRiskCode) {
        String tSql = null;
        System.out.println("实收保费" + PubFun.getCurrentTime());
        if (mContType.equals("G")) {
            tSql = "SELECT SUM(a.SumActuPayMoney) FROM ljapaygrp a WHERE a.endorsementno IS NULL AND a.riskcode ='"
                   + cRiskCode
                   + "' AND EXISTS(SELECT 1 FROM lcgrpcont WHERE grpcontno=a.grpcontno AND salechnl not in ('04','13') AND appflag='1'"
                   + " AND managecom LIKE '"
                   + cManageCom
                   + "%' AND signdate>='"
                   + mStartDate
                   + "' AND signdate<='"
                   + mEndDate
                   + "') AND a.makedate<='"
                   + mEndDate
                   + "' with ur";
        } else {
            tSql = "SELECT SUM(a.SumActuPayMoney) FROM ljapayperson a,ljapay b WHERE a.payno=b.payno AND b.incometype NOT IN('3','10') AND a.riskcode ='"
                   + cRiskCode
                   + "' AND EXISTS(SELECT 1 FROM lccont WHERE contno=a.contno AND conttype='1' AND salechnl not in ('04','13') AND appflag='1' "
                   + " AND managecom LIKE '"
                   + cManageCom
                   + "%' AND signdate>='"
                   + mStartDate
                   + "' AND signdate<='"
                   + mEndDate
                   + "') AND a.makedate<='"
                   + mEndDate
                   + "' with ur";
        }
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);
        System.out.println("实收保费" + PubFun.getCurrentTime());

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue += 0;
        } else {
            tValue += Double.parseDouble(tCount1);
        }
        System.out.println("实收保费" + PubFun.getCurrentTime());
        if (mContType.equals("G")) {
            tSql = "SELECT SUM(a.SumActuPayMoney) FROM ljapaygrp a WHERE a.endorsementno IS NULL AND a.riskcode ='"
                   + cRiskCode
                   + "' AND EXISTS(SELECT 1 FROM lbgrpcont WHERE grpcontno=a.grpcontno AND salechnl not in ('04','13') AND appflag='1'"
                   + " AND managecom LIKE '"
                   + cManageCom
                   + "%' AND signdate>='"
                   + mStartDate
                   + "' AND signdate<='"
                   + mEndDate
                   + "') AND a.makedate<='"
                   + mEndDate
                   + "' with ur";
        } else {
            tSql = "SELECT SUM(a.SumActuPayMoney) FROM ljapayperson a,ljapay b WHERE a.payno=b.payno AND b.incometype NOT IN('3','10') AND a.riskcode ='"
                   + cRiskCode
                   + "' AND EXISTS(SELECT 1 FROM lbcont WHERE contno=a.contno AND conttype='1' AND salechnl not in ('04','13') AND appflag='1' "
                   + " AND managecom LIKE '"
                   + cManageCom
                   + "%' AND signdate>='"
                   + mStartDate
                   + "' AND signdate<='"
                   + mEndDate
                   + "') AND a.makedate<='"
                   + mEndDate
                   + "' with ur";
        }
        tCount1 = tExeSQL.getOneValue(tSql);
        System.out.println("实收保费" + PubFun.getCurrentTime());
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue += 0;
        } else {
            tValue += Double.parseDouble(tCount1);
        }
        System.out.println("实收保费" + PubFun.getCurrentTime());
        if (mContType.equals("G")) {
            tSql =
                    "SELECT SUM(a.getmoney) FROM ljagetendorse a WHERE a.riskcode ='"
                    + cRiskCode
                    + "' AND EXISTS(SELECT 1 FROM lcgrpcont WHERE grpcontno=a.grpcontno AND salechnl not in ('04','13') AND appflag='1'"
                    + " AND managecom LIKE '"
                    + cManageCom
                    + "%' AND signdate>='"
                    + mStartDate
                    + "' AND signdate<='"
                    + mEndDate
                    + "') AND a.makedate<='"
                    + mEndDate
                    + "' with ur";
        } else {
            tSql =
                    "SELECT SUM(a.getmoney) FROM ljagetendorse a WHERE  a.riskcode ='"
                    + cRiskCode
                    + "' AND EXISTS(SELECT 1 FROM lccont WHERE contno=a.contno AND conttype='1' AND salechnl not in ('04','13') AND appflag='1' "
                    + " AND managecom LIKE '"
                    + cManageCom
                    + "%' AND signdate>='"
                    + mStartDate
                    + "' AND signdate<='"
                    + mEndDate
                    + "') AND a.makedate<='"
                    + mEndDate
                    + "' with ur";
        }
        tCount1 = tExeSQL.getOneValue(tSql);
        System.out.println("实收保费" + PubFun.getCurrentTime());
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue += 0;
        } else {
            tValue += Double.parseDouble(tCount1);
        }
        System.out.println("实收保费" + PubFun.getCurrentTime());
        if (mContType.equals("G")) {
            tSql =
                    "SELECT SUM(a.getmoney) FROM ljagetendorse a WHERE a.riskcode ='"
                    + cRiskCode
                    + "' AND EXISTS(SELECT 1 FROM lbgrpcont WHERE grpcontno=a.grpcontno AND salechnl not in ('04','13') AND appflag='1'"
                    + " AND managecom LIKE '"
                    + cManageCom
                    + "%' AND signdate>='"
                    + mStartDate
                    + "' AND signdate<='"
                    + mEndDate
                    + "') AND a.makedate<='"
                    + mEndDate
                    + "' with ur";
        } else {
            tSql =
                    "SELECT SUM(a.getmoney) FROM ljagetendorse a WHERE  a.riskcode ='"
                    + cRiskCode
                    + "' AND EXISTS(SELECT 1 FROM lbcont WHERE contno=a.contno AND conttype='1' AND salechnl not in ('04','13') AND appflag='1' "
                    + " AND managecom LIKE '"
                    + cManageCom
                    + "%' AND signdate>='"
                    + mStartDate
                    + "' AND signdate<='"
                    + mEndDate
                    + "') AND a.makedate<='"
                    + mEndDate
                    + "' with ur";
        }
        tCount1 = tExeSQL.getOneValue(tSql);
        System.out.println("实收保费" + PubFun.getCurrentTime());
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue += 0;
        } else {
            tValue += Double.parseDouble(tCount1);
        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 经过保费(团体)
     *
     * @param cManageCom
     *            String
     * @param cRiskCode
     *            String
     * @return String
     */
    private String getTemPremGroup(String cManageCom, String cRiskCode) {
        System.out.println("开始时间-团险经过保费1：" + PubFun.getCurrentTime());
        //添加社保案件的判断逻辑
        String tPartSql = "";
        if("1".equals(mStatsType)){
          tPartSql = " and CHECKGRPCONT(a.grpcontno) = 'N' ";//商业保险
          System.out.println("tPartSql:"+tPartSql);
        }else if("2".equals(mStatsType)){
          tPartSql = " and CHECKGRPCONT(a.grpcontno) = 'Y' ";//社会保险
          System.out.println("tPartSql:"+tPartSql);
        }else{
          tPartSql = "";//全部
          System.out.println("tPartSql:"+tPartSql);
        }
        
        String tSql = null;
        ExeSQL tExeSQL = new ExeSQL();
        double tSumPrem = 0.0;
        SSRS tSSRS = new SSRS();
        //sum内不能有子查询，列函数（比如days（））所以下面的sql不能用sum，只能查询后外用sum
        //注意t为虚拟表的名字，虚拟表需要给定名字外面才能用！
        tSql = " select "
				+ " sum(nvl((case when a.payintv <= 0 then case when days(a.enddate) - days(a.cvalidate) = 0"
				+ " then 0 else"
				+ " a.prem / (days(a.enddate) - days(a.cvalidate))"
				+ " end else a.prem * 12 / a.payintv / 365 end),0) * "
				+ " (case when a.cvalidate between '" + mStartDate + "' and '"
				+ mEndDate + "' then case when a.enddate <='" + mEndDate
				+ "' then days(a.enddate)-days(a.cvalidate)" + "  else days('"
				+ mEndDate + "')-days(a.cvalidate) + 1 end"
				+ " when a.cvalidate <'" + mStartDate
				+ "' then case when a.enddate between '" + mStartDate
				+ "' and '" + mEndDate + "' then days(a.enddate)-days('"
				+ mStartDate + "') when a.enddate >'" + mEndDate
				+ "' then days('" + mEndDate + "')-days('" + mStartDate
				+ "')+1 else 0 end else 0 end))" 
				+ " FROM lqgrppremdata a"
				+ " WHERE a.salechnl not in ('04','13') and a.managecom LIKE '"
				+ cManageCom + "%' and a.riskcode='" + cRiskCode + "'"
				+ tPartSql
				+ " and a.cvalidate <='" + mEndDate + "'"
				+ " and a.enddate >='" + mStartDate + "' with ur";
        System.out.println("经过保费:"+tSql);
        tSSRS = tExeSQL.execSQL(tSql);
        // 在tExeSQL.execSQL(tSql)执行后的结果，如果没有数据则置为"null"字符串，注意是被置为"null"字符串以示标志
        // 所以在计算时要进行判断
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            if (tSSRS.GetText(i, 1).equals("null")) {
                tSumPrem += 0;
            } else {
                tSumPrem += Double.parseDouble(tSSRS.GetText(i, 1));
            }
        }
        System.out.println(tSumPrem);
//        System.out.println("开始时间-团险经过保费2：" + PubFun.getCurrentTime());
//        tSql = "select sum(prem) from (SELECT  "
//               + " case when days(a.cvalidate)>" + mStartDays
//               + " then  "
//               +
//               " case when days((SELECT MAX(enddate) FROM lbpol WHERE a.grppolno=grppolno))<" +
//               mEndDays
//               + " then "
//               + " case when a.payintv<=0"
//               + " then  a.prem / 365 * (days((SELECT MAX(enddate) FROM lbpol WHERE a.grppolno=grppolno)) - days(a.cvalidate) + 1)"
//               + " else  a.prem * 12 /  a.payintv / 365* (days((SELECT MAX(enddate) FROM lbpol WHERE a.grppolno=grppolno)) - days(a.cvalidate) + 1)"
//               + " end "
//               + " else "
//               + " case when a.payintv<=0"
//               + " then  a.prem / 365 * (" + mEndDays +
//               " - days(a.cvalidate) + 1)"
//               + " else  a.prem * 12 / a.payintv / 365* (" + mEndDays +
//               " - days(a.cvalidate) + 1)"
//               + " end "
//               + " end "
//               + " else "
//               +
//               " case when days((SELECT MAX(enddate) FROM lbpol WHERE a.grppolno=grppolno))<" +
//               mEndDays
//               + " then "
//               + " case when a.payintv<=0"
//               + " then  a.prem / 365 * (days((SELECT MAX(enddate) FROM lbpol WHERE a.grppolno=grppolno)) - " +
//               mStartDays + " + 1)"
//               + " else  a.prem * 12 /  a.payintv / 365* (days((SELECT MAX(enddate) FROM lbpol WHERE a.grppolno=grppolno)) - " +
//               mStartDays + " + 1)"
//               + " end"
//               + " else "
//               + " case when a.payintv<=0"
//               + " then  a.prem / 365 * (" + mEndDays + " - " + mStartDays +
//               " + 1)"
//               + " else  a.prem * 12 /  a.payintv / 365* (" + mEndDays + " - " +
//               mStartDays + " + 1)"
//               + " end "
//               + " end "
//               + " end prem "
//
//               + " FROM lbgrppol a"
//               +
//               " WHERE a.appflag='1' and a.salechnl not in ('04','13') and a.managecom LIKE '"
//               + cManageCom
//               + "%' and a.riskcode ='"
//               + cRiskCode
//               + "'"
//               + " and a.cvalidate <='"
//               + mEndDate
//               + "'"
//               +
//               " and (SELECT MAX(enddate) FROM lbpol WHERE a.grppolno=grppolno) >='"
//               + mStartDate + "' ) t with ur";
//
//        tSSRS = tExeSQL.execSQL(tSql);
//        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
//            if (tSSRS.GetText(i, 1).equals("null")) {
//                tSumPrem += 0;
//            } else {
//                tSumPrem += Double.parseDouble(tSSRS.GetText(i, 1));
//            }
//        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tSumPrem);
        System.out.println("结束时间-团险经过保费1：" + PubFun.getCurrentTime());
        return tRturn;
    }

    /**
     * 经过保费(个险)
     *
     * @param cManageCom
     *            String
     * @param cRiskCode
     *            String
     * @return String
     */
    private String getTemPremPerson(String cManageCom, String cRiskCode) {
        System.out.println("开始时间个险。。。。" + PubFun.getCurrentTime());
        String tSql = null;
        ExeSQL tExeSQL = new ExeSQL();
        double tSumPrem = 0.0;
        SSRS tSSRS = new SSRS();

        tSql = " select "
			+ " sum(nvl((case when a.payintv <= 0 then case when days(a.enddate) - days(a.cvalidate) = 0"
			+ " then 0 else"
			+ " a.prem / (days(a.enddate) - days(a.cvalidate))"
			+ " end else a.prem * 12 / a.payintv / 365 end),0) * "
			+ " (case when a.cvalidate between '" + mStartDate + "' and '"
			+ mEndDate + "' then case when a.enddate <='" + mEndDate
			+ "' then days(a.enddate)-days(a.cvalidate)" + "  else days('"
			+ mEndDate + "')-days(a.cvalidate) + 1 end"
			+ " when a.cvalidate <'" + mStartDate
			+ "' then case when a.enddate between '" + mStartDate
			+ "' and '" + mEndDate + "' then days(a.enddate)-days('"
			+ mStartDate + "') when a.enddate >'" + mEndDate
			+ "' then days('" + mEndDate + "')-days('" + mStartDate
			+ "')+1 else 0 end else 0 end))" 
			+ " FROM lqclaimpol a"
			+ " WHERE a.salechnl not in ('04','13') and a.managecom LIKE '"
			+ cManageCom + "%' and a.riskcode='" + cRiskCode + "'"
			+ " and a.cvalidate <='" + mEndDate + "'"
			+ " and a.enddate >='" + mStartDate + "' with ur";

        System.out.println(tSql + "经过保费");
        tSSRS = tExeSQL.execSQL(tSql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            if (tSSRS.GetText(i, 1).equals("null")) {
                tSumPrem += 0;
            } else {
                tSumPrem += Double.parseDouble(tSSRS.GetText(i, 1));
            }
        }

//        tSql = "select sum(prem) from (SELECT  "
//               + " case when days(cvalidate)>" + mStartDays
//               + " then  "
//               + " case when days(enddate)<" + mEndDays
//               + " then "
//               + " case when payintv<=0"
//               + " then  prem / (days(enddate) - days(cvalidate) + 1) * (days(enddate) - days(cvalidate) + 1)"
//               +
//               " else  prem * 12 /  payintv / 365* (days(enddate) - days(cvalidate) + 1)"
//               + " end "
//               + " else "
//               + " case when payintv<=0"
//               + " then  prem / (days(enddate) - days(cvalidate) + 1) * (" +
//               mEndDays + " - days(cvalidate) + 1)"
//               + " else  prem * 12 / payintv / 365* (" + mEndDays +
//               " - days(cvalidate) + 1)"
//               + " end "
//               + " end "
//               + " else  "
//               + " case when days(enddate)<" + mEndDays
//               + " then "
//               + " case when payintv<=0"
//               +
//               " then  prem / (days(enddate) - days(cvalidate) + 1) * (days(enddate) - " +
//               mStartDays + " + 1)"
//               + " else  prem * 12 / payintv / 365* (days(enddate) - " +
//               mStartDays + " + 1)"
//               + " end "
//               + " else "
//               + " case when payintv<=0"
//               + " then  prem / (days(enddate) - days(cvalidate) + 1) * (" +
//               mEndDays + " - " + mStartDays + " + 1)"
//               + " else  prem * 12 / payintv / 365* (" + mEndDays + " - " +
//               mStartDays + " + 1)"
//               + " end "
//               + " end "
//               + " end prem"
//
//               + " FROM lbpol"
//               +
//               " WHERE conttype='1' AND salechnl not in ('04','13') AND appflag='1' and managecom LIKE '"
//               + cManageCom
//               + "%' and riskcode ='"
//               + cRiskCode
//               + "' and grpcontno='00000000000000000000'"
//               + " and cvalidate <='"
//               + mEndDate
//               + "'"
//               + " and enddate >='" + mStartDate + "' ) t with ur";
//
//        tSSRS = tExeSQL.execSQL(tSql);
//        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
//            if (tSSRS.GetText(i, 1).equals("null")) {
//                tSumPrem += 0;
//            } else {
//                tSumPrem += Double.parseDouble(tSSRS.GetText(i, 1));
//            }
//        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tSumPrem);
        System.out.println("结束时间个险。。。。" + PubFun.getCurrentTime());
        return tRturn;
    }

    /**
     * 经过保费
     *
     * @param cManageCom
     *            String
     * @param cRiskCode
     *            String
     * @return String
     */
    //针对个险或团险调用相应方法来执行
    private String getTemPrem(String cManageCom, String cRiskCode) {
        if (mContType.equals("G")) {
            return this.getTemPremGroup(cManageCom, cRiskCode);

        } else {
            return this.getTemPremPerson(cManageCom, cRiskCode);
        }
    }

    /**
     * 理赔件数
     *
     * @param cManageCom
     *            String
     * @param cRiskCode
     *            String
     * @return String
     */
    private String getCaseNum(String cManageCom, String cRiskCode) {
        //添加社保案件的判断逻辑
        String tPartSql = "";
        if("1".equals(mStatsType)){
          tPartSql = " and CHECKGRPCONT((select rgtobjno from llregister where llregister.rgtno=llcase.rgtno)) = 'N' ";//商业保险
          System.out.println("tPartSql:"+tPartSql);
        }else if("2".equals(mStatsType)){
          tPartSql = " and CHECKGRPCONT((select rgtobjno from llregister where llregister.rgtno=llcase.rgtno)) = 'Y' ";//社会保险
          System.out.println("tPartSql:"+tPartSql);
        }else{
          tPartSql = "";//全部
          System.out.println("tPartSql:"+tPartSql);
        }
    	
        String tSql =
                "SELECT COUNT(distinct(a.caseno)) from llclaimdetail a WHERE  a.salechnl not in ('04','13') AND a.riskcode ='"
                + cRiskCode
                + "' AND EXISTS(SELECT 1 from llcase WHERE rgtstate IN ('09','11','12') "+tPartSql+"AND caseno=a.caseno AND endcasedate>='"
                + mStartDate
                + "' AND endcasedate<='"
                + mEndDate
                + "' AND mngcom LIKE '" + cManageCom + "%') WITH UR";
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 赔款支出
     *
     * @param cManageCom
     *            String
     * @param cRiskCode
     *            String
     * @return String
     */
    private String getRealPay(String cManageCom, String cRiskCode) {
    	//添加社保案件的判断逻辑
        String tSql = "";
        if("1".equals(mStatsType)){//商业保险
        	tSql = "select sum(pay) from ljagetclaim a where a.riskcode='"
		       +cRiskCode+"'  " 
		       + " and CHECKGRPCONT(a.grpcontno) = 'N' "	
		       + " and makedate between '"
		       + mStartDate+"' and '"
		       + mEndDate+"' and ManageCom like '"
	           + cManageCom+"%' and exists (select 1 from lcpol where salechnl not in ('04','13') and polno=a.polno) with ur";
        	System.out.println("tSql:"+tSql);
        }else if("2".equals(mStatsType)){//社会保险
        	tSql = "select sum(pay) from ljagetclaim a where a.riskcode='"
 		       +cRiskCode+"'  " 
 		       + " and CHECKGRPCONT(a.grpcontno) = 'Y' "	
 		       + " and makedate between '"
 		       + mStartDate+"' and '"
 		       + mEndDate+"' and ManageCom like '"
 	           + cManageCom+"%' and exists (select 1 from lcpol where salechnl not in ('04','13') and polno=a.polno) with ur";
         	System.out.println("tSql:"+tSql);
        }else{//全部
//            tSql =
//                "SELECT sum(a.realpay) from lqclaimrealpay a where a.salechnl not in ('04','13') AND a.riskcode = '"
//                + cRiskCode
//                + "' and  a.endcasedate>='"
//                + mStartDate
//                + "' and a.endcasedate<='"
//                + mEndDate
//                +
//                "' and a.managecom LIKE '" +
//                cManageCom + "%' with ur";
        	//#2862 查询机“团险理赔统计报表”调整
        	tSql="SELECT cast(sum(a.realpay) as decimal(12,2)) from llclaimdetail a,llcase b ,lccont c"
            		+" where b.caseno=a.caseno and c.contno=a. contno"
            	  	+"  and c.conttype='1'"
            	  	+"  and c.grpcontno='00000000000000000000'"
            	  	+"  and a. realpay<>0"
            	  	+"  and b.rgtstate in( '09','11','12')"
            	  	+"  and a.salechnl not in('04','13')"
            	  	+" and a.riskcode='"
            	    + cRiskCode
            	    +"' and b.endcasedate >='"
            	    + mStartDate
            	    +"' and b.endcasedate<='"
            	    + mEndDate
            	    +"' and b.mngcom LIKE '"
            	    + cManageCom +"%'"
            	    +" group by c.managecom,a. riskcode,a.salechnl,b.endcasedate,b.rgtstate"
            	    +" union all"
            	    +" SELECT cast(sum(a.realpay) as decimal(12,2)) realpay from llclaimdetail a,llcase b,lbcont c"
            	    +" where b.caseno=a.caseno and c.contno=a.contno"
            	    +" and conttype='1'"
            	    +" and c.grpcontno='00000000000000000000'"
            	    +" and a.realpay<>0"
            	    +" and b. rgtstate in('09','11','12')"
            	    +" and a.salechnl not in('04','13')"
            	    +"and a.riskcode='"
            	    + cRiskCode 
            	    +"' and b.endcasedate>='"
                    + mStartDate
                    + "' and b.endcasedate<='"
                    + mEndDate
                    +"' and b.mngcom LIKE '" 
                    + cManageCom +"%'"
                    +" group by c.managecom,a.riskcode,a.salechnl,b.endcasedate,b.rgtstate"
                    +" union all"
                    +" SELECT cast(sum(a.realpay) as decimal(12,2))from llclaimdetail a,llcase b ,lcgrpcont c"
                    +" where b.caseno=a.caseno"
            	    +" and c.grpcontno=a.grpcontno"
            	    +" and a.realpay<>0"
            	    +" and b .rgtstate in( '09','11','12')"
            	    +" and a.salechnl not in('04','13')"
            	    +" and a.riskcode='"
            	    + cRiskCode
            	    +"' and b.endcasedate >='"
            	    + mStartDate
            	    + "' and b.endcasedate<='"
                    + mEndDate
                    +"' and b.mngcom LIKE '" 
                    + cManageCom +"%'"
                    +" group by c.managecom,a.riskcode,a.salechnl,b.endcasedate,b.rgtstate"
                    +" union all"
                    +" SELECT cast(sum(a.realpay) as decimal(12,2))from llclaimdetail a,llcase b,lbgrpcont c"
                    +" where b.caseno=a.caseno"
            	    +" and c.grpcontno=a.grpcontno"
            	    +" and a.realpay<>0"
            	    +" and b. rgtstate in('09','11','12')"
            	    +" and a.salechnl not in('04','13')"
            	    +" and a.riskcode='"
            	    + cRiskCode 
            	    +"' and b.endcasedate >='" 
            	    + mStartDate
            	    + "' and b.endcasedate<='"
                    + mEndDate
                    +"' and b.mngcom LIKE '" 
                    + cManageCom +"%'"
                    +" group by c.managecom,a.riskcode,a.salechnl,b.endcasedate,b.rgtstate with ur";
        	
            System.out.println("tSql:"+tSql);
        }
    	
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }

//        String tBSql =
//                "SELECT sum(a.realpay) from llclaimdetail a where a.salechnl not in ('04','13') AND a.riskcode = '"
//                + cRiskCode
//                + "' and exists(SELECT 1 from llcase where caseno=a.caseno and rgtstate in('09','11','12') and endcasedate>='"
//                + mStartDate
//                + "' and endcasedate<='"
//                + mEndDate
//                + "' ) and exists (select 1 from lbcont where contno=a.contno and managecom LIKE '" +
//                cManageCom + "%') with ur";
//    	String tSql = "select sum(pay) from ljagetclaim a where a.riskcode='"
//    		       +cRiskCode+"' and  makedate between '"
//    		       + mStartDate+"' and '"
//    		       + mEndDate+"' and ManageCom like '"
//    	           + cManageCom+"%' and exists (select 1 from lcpol where salechnl not in ('04','13') and polno=a.polno) with ur";
//        System.out.println(tBSql);
//
//        String tCount2 = tExeSQL.getOneValue(tBSql);
//
//        double tValue2 = 0;
//        if (tCount2 == null || tCount2.equals("") || tCount2.equals("0")) {
//            tValue2 = 0;
//        } else {
//            tValue2 = Double.parseDouble(tCount2);
//        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 分公司不含特需合计承保人数
     *
     * @param cManageCom
     *            String
     * @param cRiskCode
     *            String
     * @return String
     */
    private String getComPeoples1(String cManageCom) {
        String tSql = "SELECT SUM(a.peoples2) FROM lqgrppremdata a WHERE a.type='C' AND a.salechnl not in ('04','13') AND NOT EXISTS"
                      + "(SELECT 1 FROM lmriskapp WHERE riskcode=a.riskcode AND risktype3='7' AND risktype <>'M')"
                      + "AND EXISTS (SELECT 1 FROM lmriskapp WHERE riskcode=a.riskcode AND riskprop='G')"
                      + " AND a.managecom LIKE '"
                      + cManageCom
                      + "%' AND a.signdate>='"
                      + mStartDate
                      + "'"
                      + " AND a.signdate<='" + mEndDate + "' with ur";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 分公司含特需承保人数
     *
     * @param cManageCom
     *            String
     * @return String
     */
    private String getComPeoples2(String cManageCom) {
        String tSql = "SELECT SUM(a.peoples2) FROM lqgrppremdata a WHERE a.type='C' AND a.salechnl not in ('04','13') AND NOT EXISTS"
                      +
                      "(SELECT 1 FROM lmriskapp WHERE riskcode=a.riskcode AND riskprop='I')"
                      + " AND a.managecom LIKE '"
                      + cManageCom
                      + "%' AND a.signdate>='"
                      + mStartDate
                      + "'"
                      + " AND a.signdate<='" + mEndDate + "' with ur";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 分公司不含特需理赔件数
     *
     * @param cManageCom
     *            String
     * @return String
     */
    private String getComCaseNum1(String cManageCom) {
    	//添加社保案件的判断逻辑
    	String tPartSql = "";
    	if("1".equals(mStatsType)){
    		tPartSql = " and CHECKGRPCONT((select rgtobjno from llregister where llregister.rgtno=llcase.rgtno)) = 'N' ";//商业保险
    		System.out.println("tPartSql:"+tPartSql);
    	}else if("2".equals(mStatsType)){
    		tPartSql = " and CHECKGRPCONT((select rgtobjno from llregister where llregister.rgtno=llcase.rgtno)) = 'Y' ";//社会保险
    		System.out.println("tPartSql:"+tPartSql);
    	}else{
    		tPartSql = "";//全部
    		System.out.println("tPartSql:"+tPartSql);
    	}
        String tSql =
                "SELECT COUNT(DISTINCT(a.caseno)) FROM llclaimdetail a WHERE a.salechnl not in ('04','13') AND NOT EXISTS"
                + "(SELECT 1 FROM lmriskapp WHERE riskcode=a.riskcode AND risktype3='7' AND risktype <>'M')"
                + "AND EXISTS (SELECT 1 FROM lmriskapp WHERE riskcode=a.riskcode AND riskprop='G')"
                + " AND EXISTS(SELECT 1 FROM llcase WHERE caseno=a.caseno "+tPartSql+" AND rgtstate in ('09','11','12') AND endcasedate>='"
                + mStartDate
                + "' AND endcasedate<='"
                + mEndDate
                + "' AND mngcom LIKE '" + cManageCom + "%') with ur";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 分公司含特需理赔件数
     *
     * @param cManageCom
     *            String
     * @return String
     */
    private String getComCaseNum2(String cManageCom) {
    	//添加社保案件的判断逻辑
    	String tPartSql = "";
    	if("1".equals(mStatsType)){
    		tPartSql = " and CHECKGRPCONT((select rgtobjno from llregister where llregister.rgtno=llcase.rgtno)) = 'N' ";//商业保险
    		System.out.println("tPartSql:"+tPartSql);
    	}else if("2".equals(mStatsType)){
    		tPartSql = " and CHECKGRPCONT((select rgtobjno from llregister where llregister.rgtno=llcase.rgtno)) = 'Y' ";//社会保险
    		System.out.println("tPartSql:"+tPartSql);
    	}else{
    		tPartSql = "";//全部
    		System.out.println("tPartSql:"+tPartSql);
    	}
    	
        String tSql =
                "SELECT COUNT(DISTINCT(a.caseno)) FROM llclaimdetail a WHERE a.salechnl not in ('04','13') AND EXISTS"
                +
                "(SELECT 1 FROM lmriskapp WHERE riskcode=a.riskcode AND riskprop='G')"
                + " AND EXISTS(SELECT 1 FROM llcase WHERE rgtstate in ('09','11','12') "+tPartSql+" AND caseno=a.caseno AND endcasedate>='"
                + mStartDate
                + "' AND endcasedate<='"
                + mEndDate
                + "' AND mngcom LIKE '" + cManageCom + "%') with ur";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 个险分公司承保人数
     *
     * @param cManageCom
     *            String
     * @return String
     */
    private String getComPeoples(String cManageCom) {
        String tSql = "SELECT COUNT(a.insuredno) FROM lcinsured a WHERE EXISTS(SELECT 1 FROM lcpol WHERE a.contno=contno AND conttype='1' AND salechnl not in ('04','13') AND appflag='1' "
                      + " AND managecom LIKE '"
                      + cManageCom
                      + "%' AND signdate>='"
                      + mStartDate
                      + "'"
                      + " AND signdate<='"
                      + mEndDate
                      + "') with ur";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 个险分公司理赔案件
     *
     * @param cManageCom
     *            String
     * @return String
     */
    private String getComCaseNum(String cManageCom) {
        String tSql =
                "SELECT COUNT(DISTINCT(a.caseno)) FROM llclaimdetail a WHERE a.salechnl not in ('04','13') AND EXISTS"
                +
                "(SELECT 1 FROM lmriskapp WHERE riskcode=a.riskcode AND riskprop='I')"
                + " AND EXISTS(SELECT 1 FROM llcase WHERE rgtstate in ('09','11','12') AND caseno=a.caseno AND endcasedate>='"
                + mStartDate
                + "' AND endcasedate<='"
                + mEndDate
                + "' AND mngcom LIKE '" + cManageCom + "%') with ur";
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 8位机构打印结果展示
     * @return
     */
    private boolean getDataList() {
        double tSumPeople1 = 0;
        double tSumPrem1 = 0;
        double tSumActuPay1 = 0;
        double tSumTemPrem1 = 0;
        // double tSumCaseNum1 = 0;
        double tSumRealPay1 = 0;

        double tSumPeople2 = 0;
        double tSumPrem2 = 0;
        double tSumActuPay2 = 0;
        double tSumTemPrem2 = 0;
        // double tSumCaseNum2 = 0;
        double tSumRealPay2 = 0;

        String tSQL = null;
        // String tManageCom = "";
        // String tComName = "";
        String tRiskCode = "";
        String tRiskName = "";
        String tManageCom = "";
        String tComName = "";

        tSQL = "SELECT  b.comcode,b.name,a.riskcode,a.riskname  from ldcom B,lmriskapp A where B.sign='1'" 
        + " and  length(trim(comcode))=8  "
        + "  AND  B.comcode LIKE '"
        + mManageCom
        + "%' and A.riskprop='"
        + mContType
        + "' order by b.comcode,a.riskcode with ur";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) { // 循环 险种进行统计
        	tManageCom = tSSRS.GetText(i, 1);
        	System.out.println("tManageCom:"+tManageCom);
            tComName = tSSRS.GetText(i, 2);
            tRiskCode = tSSRS.GetText(i, 3);
            tRiskName = tSSRS.GetText(i, 4);
            String Info[] = new String[10];
            Info[0] = tComName;
            Info[1] = tRiskCode;
            Info[2] = tRiskName;
            Info[3] = getPeoples(mManageCom, tRiskCode);
            Info[4] = getPrem(mManageCom, tRiskCode);
            //Info[5] = getActuPay(mManageCom, tRiskCode);
            Info[5] = getTemPrem(mManageCom, tRiskCode);
            Info[6] = getCaseNum(mManageCom, tRiskCode);
            Info[7] = getRealPay(mManageCom, tRiskCode);
            Info[8] = mManageCom;
            mListTable.add(Info);

            tSQL = "SELECT 1 FROM lmriskapp WHERE riskcode='" + tRiskCode
                   + "' AND risktype3='7' AND risktype <>'M' with ur";
            ExeSQL tExeSQL1 = new ExeSQL();
            SSRS tSSRS1 = new SSRS();
            tSSRS1 = tExeSQL1.execSQL(tSQL);
            if (tSSRS1.getMaxRow() > 0) {
                tSumPeople2 += Double.parseDouble(Info[3]);
                tSumPrem2 += Double.parseDouble(Info[4]);
                //tSumActuPay2 += Double.parseDouble(Info[5]);
                tSumTemPrem2 += Double.parseDouble(Info[5]);
                tSumRealPay2 += Double.parseDouble(Info[7]);
            } else {
                tSumPeople1 += Double.parseDouble(Info[3]);
                tSumPrem1 += Double.parseDouble(Info[4]);
                // tSumActuPay1 += Double.parseDouble(Info[5]);
                tSumTemPrem1 += Double.parseDouble(Info[5]);
                tSumRealPay1 += Double.parseDouble(Info[7]);

                tSumPeople2 += Double.parseDouble(Info[3]);
                tSumPrem2 += Double.parseDouble(Info[4]);
                // tSumActuPay2 += Double.parseDouble(Info[5]);
                tSumTemPrem2 += Double.parseDouble(Info[5]);
                tSumRealPay2 += Double.parseDouble(Info[7]);
            }
        }
        DecimalFormat tDF = new DecimalFormat("0.##");
        if (mContType.equals("G")) {
            // 合计所有公司所有险种不含特需
            String Info1[] = new String[10];
            Info1[0] = "合计";
            Info1[1] = "";
            Info1[2] = "不含特需合计";
            Info1[3] = tDF.format(tSumPeople1);
            Info1[4] = tDF.format(tSumPrem1);
            //Info1[5] = tDF.format(tSumActuPay1);
            Info1[5] = tDF.format(tSumTemPrem1);
            Info1[6] = getComCaseNum1(mManageCom);
            Info1[7] = tDF.format(tSumRealPay1);
            Info1[8] = mManageCom;
            mListTable.add(Info1);
            // 合计所有公司所有险种含特需
            String Info2[] = new String[10];
            Info2[0] = "合计";
            Info2[1] = "";
            Info2[2] = "含特需合计";
            Info2[3] = tDF.format(tSumPeople2);
            Info2[4] = tDF.format(tSumPrem2);
            //Info2[5] = tDF.format(tSumActuPay2);
            Info2[5] = tDF.format(tSumTemPrem2);
            Info2[6] = getComCaseNum2(mManageCom);
            Info2[7] = tDF.format(tSumRealPay2);
            Info2[8] = mManageCom;

            mListTable.add(Info2);
        } else {
            String Info[] = new String[10];
            Info[0] = "合计";
            Info[1] = "";
            Info[2] = "合计";
            Info[3] = tDF.format(tSumPeople1);
            Info[4] = tDF.format(tSumPrem1);
            //Info[5] = tDF.format(tSumActuPay1);
            Info[5] = tDF.format(tSumTemPrem1);
            Info[6] = getComCaseNum(mManageCom);
            Info[7] = tDF.format(tSumRealPay1);
            Info[8] = mManageCom;
            mListTable.add(Info);
        }
        return true;
    }

    /**
     * 进行数据查询
     *
     * @return boolean
     */
    private boolean queryData() {
        TextTag tTextTag = new TextTag();
        mXmlExport = new XmlExport();
        // 设置模版名称
        mXmlExport.createDocument("LLCaseStatisticsReport.vts", "printer");
        System.out.print("XML模板生成开始");
        tTextTag.add("ManageComName", mManageComName);
        tTextTag.add("StartDate", mStartDate);
        tTextTag.add("EndDate", mEndDate);
        tTextTag.add("Con", mCon);
        tTextTag.add("Operator", moperator);
        tTextTag.add("MakeDate", currentDate);
        String tStatsName = "";
        if("1".equals(mStatsType)){
        	tStatsName = "商业保险";
        }else if("2".equals(mStatsType)){
        	tStatsName = "社会保险";
        }else{
        	tStatsName = "全部";
        }
        tTextTag.add("StatsName", tStatsName);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] title = {"", "", "", "", "", "", "", "", ""};
        if (mManageCom.equals("86")) {
            if (!getSumDataList("02")) {
                return false;
            }
        } else if (mManageCom.length() == 4) {
            if (!getSumDataList("03")) {
                return false;
            }
        } else {
            if (!getDataList()) {
                return false;
            }
        }
        mListTable.setName("ENDOR");
        System.out.println("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.println("121");
        mXmlExport.outputDocumentToFile("E:\\Temp\\", "LLCaseStatisticsReport");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 取得传入的数据
     *
     * @return boolean
     */
    private boolean getInputData(VData pmInputData) {
        // 全局变量
        // mGlobalInput.setSchema((GlobalInput)
        // pmInputData.getObjectByObjectName(
        // "GlobalInput", 0));
        mStartDate = (String) pmInputData.get(0);
        mEndDate = (String) pmInputData.get(1);
        mManageCom = (String) pmInputData.get(2);
        moperator = (String) pmInputData.get(3);
        mManageComName = (String) pmInputData.get(4);
        mContType = (String) pmInputData.get(5);
        mCon = (String) pmInputData.get(6);
        mStatsType = (String) pmInputData.get(7);//统计类型

        String tSql = "SELECT days('" + mStartDate + "'),days('" + mEndDate
                      + "') FROM dual where 1=1 with ur";
        ExeSQL tExeSQLDate = new ExeSQL();
        SSRS tSSRSDate = new SSRS();
        tSSRSDate = tExeSQLDate.execSQL(tSql);
        mStartDays = Integer.parseInt(tSSRSDate.GetText(1, 1));
        mEndDays = Integer.parseInt(tSSRSDate.GetText(1, 2));
        mTransferData = (TransferData) pmInputData.getObjectByObjectName(
                "TransferData", 0);
        mFileNameB = (String) mTransferData.getValueByName("tFileNameB");
        mMakeDate = currentDate;
        return true;
    }
    
    /**
     * 数据初步校验
     *
     * @return boolean
     */
    private boolean checkData() {
    	if("I".equals(mContType) && ("1".equals(mStatsType) || "2".equals(mStatsType))){
    		TextTag tTextTag = new TextTag();
            mXmlExport = new XmlExport();
            //设置模版名称
            //此处修改为“excel打印”，模板名称采用之前vts模板的名称	#2076 团个险分险种报表 vts打印调整为excel格式
            mXmlExport.createDocument("LLCaseStatisticsReport.vts", "printer");
            System.out.print("XML模板生成开始");
            tTextTag.add("ManageComName", mManageComName);
            tTextTag.add("StartDate", mStartDate);
            tTextTag.add("EndDate", mEndDate);
            tTextTag.add("Con", mCon);
            tTextTag.add("Operator", moperator);
            tTextTag.add("MakeDate", currentDate);
            
            String[] title = {"", "", "", "", "", "", "", "", ""};
            mXmlExport.addTextTag(tTextTag);
            mListTable.setName("ENDOR");
            mXmlExport.addListTable(mListTable, title);
            mXmlExport.outputDocumentToFile("E:\\Temp\\", "new1");
            this.mResult.clear();
            mResult.addElement(mXmlExport);
            return false;
    	}        
        return true;
    }


    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLCaseStatisticsBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 取得返回处理过的结果
     *
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    private void jbInit() throws Exception {
    }

    public static void main(String[] args) {

        VData tVData = new VData();
        LLCaseStatisticsBL tClientRegisterBL = new LLCaseStatisticsBL();

        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86";
        mGlobalInput.ComCode = "86";
        mGlobalInput.Operator = "lptest";
        String fileNameB = "lptest_0.vts";
          TransferData tTransferData= new TransferData();
        tTransferData.setNameAndValue("tFileNameB",fileNameB);
        tVData.addElement("2009-1-1");
	tVData.addElement("2009-5-31");
        tVData.addElement("86");
          tVData.addElement("lptest");
          tVData.addElement(ReportPubFun.getMngName("86"));
          tVData.addElement("I");
          tVData.addElement("个险");
          tVData.addElement(tTransferData);

        tVData.add(mGlobalInput);
        tClientRegisterBL.submitData(tVData, "");
        tVData.clear();
        tVData = tClientRegisterBL.getResult();
    }
}
