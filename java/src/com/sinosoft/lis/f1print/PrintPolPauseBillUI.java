package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PrintPolPauseBillUI {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    public VData mInputData = new VData();
    /** 往界面传输数据的容器 */
    public VData mResult = new VData();
    /** 数据操作字符串 */
    public String mOperate = "";

    public PrintPolPauseBillUI() {
    }

    public static void main(String[] args) {

    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

//        this.mOperate = cOperate;

        PrintPolPauseBillBL tPrintPolPauseBillBL = new PrintPolPauseBillBL();

        if (!tPrintPolPauseBillBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tPrintPolPauseBillBL.mErrors);
            mResult.clear();
            return false;
        } else {
            mResult = tPrintPolPauseBillBL.getResult();
        }

        return true;
    }

    /**
     * 返回数据
     * @return VData
     */
    public VData getResult(){
        return mResult;
    }
}
