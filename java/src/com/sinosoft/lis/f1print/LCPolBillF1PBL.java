package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author zy
 * @version 1.0
 */

import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LCPolBillF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    private String mOperate = "";
    //取得的时间
    private String mDay[] = null;
    private String mTime[] = null;
    private String SaleChnl = "";
    private String AgentGroup = "";
    private String AgentCode = "";
    private String StartPolNo = "";
    private String EndPolNo = "";
    private String RiskCode = "";
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    private String funsql = "";
    private String orderbysql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public LCPolBillF1PBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT") && !cOperate.equals("PRINT||BANK"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        mOperate = cOperate;

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        mDay = (String[]) cInputData.get(0);
        mTime = (String[]) cInputData.get(1);
        mLCPolSchema.setSchema((LCPolSchema) cInputData.getObjectByObjectName(
                "LCPolSchema", 0));
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        SaleChnl = mLCPolSchema.getSaleChnl();
        AgentGroup = mLCPolSchema.getAgentGroup();
        AgentCode = mLCPolSchema.getAgentCode();
        StartPolNo = mLCPolSchema.getPolNo();
        /*Lis5.3 upgrade get
             EndPolNo=mLCPolSchema.getConsignNo();
         */
        RiskCode = mLCPolSchema.getRiskCode();

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolBillF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        double dSumCount = 0;
        SSRS tSSRS = new SSRS();

        String strMofifyTime = "";
        if (!mTime[0].equals(""))
        {
            strMofifyTime = " and LCPolPrint.ModifyTime >= '" + mTime[0] + "'";
        }
        if (!mTime[1].equals(""))
        {
            strMofifyTime = strMofifyTime + " and LCPolPrint.ModifyTime <= '" +
                            mTime[1] + "'";
        }

        String strSaleChnlWhere = "";
        if (SaleChnl != null && !SaleChnl.equals(""))
        {
            strSaleChnlWhere = "  AND LCPol.SaleChnl = '" + SaleChnl + "' ";
        }
        String strAgentCodeWhere = "";

        if (AgentCode == null || AgentCode.equals(""))
        {
            strAgentCodeWhere = "";
        }
        else
        {
            strAgentCodeWhere = "  AND LCPol.AgentCode = '" + AgentCode + "' ";
        }
        String strStartPolNoWhere = "";

        if (StartPolNo != null && !StartPolNo.equals(""))
        {
            strStartPolNoWhere = "  AND LCPol.PolNo >='" + StartPolNo + "' ";
        }

        String strEndPolNoWhere = "";
        if (EndPolNo != null && !EndPolNo.equals(""))
        {
            strEndPolNoWhere = "  AND LCPol.PolNo <='" + EndPolNo + "' ";
        }
        String strRiskCodeWhere = "";
        if (RiskCode == null || RiskCode.equals(""))
        {
            strRiskCodeWhere = "";
        }
        else
        {
            strRiskCodeWhere = " AND LCPol.RiskCode = '" + RiskCode + "' ";
        }

        //add by Minim
        String strExtendWhere = "";
        if (mLCPolSchema.getPrtNo() != null &&
            !mLCPolSchema.getPrtNo().equals(""))
        {
            strExtendWhere = strExtendWhere + " AND LCPol.PrtNo ='" +
                             mLCPolSchema.getPrtNo() + "' ";
        }

        if (mLCPolSchema.getAppntName() != null &&
            !mLCPolSchema.getAppntName().equals(""))
        {
            strExtendWhere = strExtendWhere + " AND LCPol.AppntName ='" +
                             mLCPolSchema.getAppntName() + "' ";
        }

        if (mLCPolSchema.getInsuredName() != null &&
            !mLCPolSchema.getInsuredName().equals(""))
        {
            strExtendWhere = strExtendWhere + " AND LCPol.InsuredName ='" +
                             mLCPolSchema.getInsuredName() + "' ";
        }

        String strAgentGroupWhere = " AND LCPol.AgentGroup in ( select AgentGroup from labranchgroup where branchattr LIKE '" +
                                    AgentGroup + "%' AND ManageCom LIKE '" +
                                    mLCPolSchema.getManageCom() + "%')";

        if (mOperate.equals("PRINT||BANK"))
        {
            msql = "select LCPol.PolNo,labranchgroup.name,LCPol.PrtNo,LCPol.AppntName,LCPol.InsuredName,LAAgent.Name "
                   + " from LCPol,labranchgroup,LAAgent "
                   +
                    " where LAAgent.AgentCode=LCPol.AgentCode and LCPol.SignDate>= '"
                   + mDay[0] + "' and LCPol.SignDate<= '" + mDay[1]
                   +
                    "' and LCPol.StandbyFlag3 = 'Y' and LCPol.StandbyFlag4 is not null "
                   + " and LCPol.ManageCom like'" + mLCPolSchema.getManageCom()
                   + "%' and labranchgroup.agentgroup=LCPol.AgentGroup";
        }
        if (mOperate.equals("PRINT"))
        {
            msql = "select LCPol.PolNo,labranchgroup.name,LCPol.PrtNo,LCPol.AppntName,LCPol.InsuredName,LAAgent.Name from LCPol,labranchgroup,LCPolPrint,LAAgent where LAAgent.AgentCode=LCPol.AgentCode and LCPolPrint.ModifyDate>='"
                   + mDay[0] + "' and LCPolPrint.ModifyDate<= '" + mDay[1] +
                   "'" + strMofifyTime
                   +
                    " and LCPol.PolNo = LCPolPrint.MainPolNo and LCPol.ManageCom like'"
                   + mLCPolSchema.getManageCom() +
                    "%' and LCPolPrint.PolType='1' and labranchgroup.agentgroup=LCPol.AgentGroup";
        }
        msql = msql + strSaleChnlWhere + strAgentGroupWhere + strAgentCodeWhere +
               strStartPolNoWhere
               + strEndPolNoWhere + strRiskCodeWhere + strExtendWhere;

        //added by lys for prtno 2004-3-18
        // add by guoxiang for 保单交接清单权限控制  use ora function at 2003-12-9 12:00
        // add 1:
        funsql = " and VERIFYOPERATEPOPEDOM(LCPol.Riskcode,LCPol.Managecom,'" +
                 mGlobalInput.ManageCom + "','Fa')=1";
        orderbysql =
                " order by LCPol.PolNo,LCPol.ManageCom,LCPol.AgentGroup,LCPol.AgentCode";
        // query lcpol first
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println(msql + funsql + orderbysql);
        tSSRS = tExeSQL.execSQL(msql + funsql + orderbysql);
        ListTable tlistTable = new ListTable();
        String strArr[] = null;
        tlistTable.setName("LCPOL");
        dSumCount = tSSRS.getMaxRow();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            //查询销售渠道
            //strArr = new String[8];
            strArr = new String[9];
            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("salechnl");
            if (SaleChnl != null && !SaleChnl.equals(""))
            {
                tLDCodeDB.setCode(SaleChnl);

            }
            else
            {
                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setPolNo(tSSRS.GetText(i, 1));
                if (!tLCPolDB.getInfo())
                {
                    mErrors.copyAllErrors(tLDCodeDB.mErrors);
                    buildError("SaleChnl", "在取得tLCPolDB的数据时发生错误");
                    return false;
                }

                String SaleChnl1 = tLCPolDB.getSaleChnl();
                tLDCodeDB.setCode(SaleChnl1);
            }
            if (!tLDCodeDB.getInfo())
            {
                mErrors.copyAllErrors(tLDCodeDB.mErrors);
                buildError("SaleChnl", "在取得CodeName的数据时发生错误");
                return false;
            }

            //strArr[5] = tLDCodeDB.getCodeName();
            strArr[6] = tLDCodeDB.getCodeName();
            for (int j = 1; j <= tSSRS.getMaxCol(); j++)
            {
                strArr[j - 1] = tSSRS.GetText(i, j);
            }
            tlistTable.add(strArr);
        }

        // then query lbpol
        // replace "LCPol" by "LBPol"
        msql = StrTool.replaceEx(msql, "lcpol", "lbpol");
        msql = StrTool.replaceEx(msql, "lbpolprint", "lcpolprint");
        // get a correct sql here
        //add 2:
        funsql = StrTool.replaceEx(funsql, "lcpol", "lbpol");
        orderbysql = StrTool.replaceEx(orderbysql, "lcpol", "lbpol");
        tExeSQL = new ExeSQL();
//System.out.println(msql+funsql+orderbysql);
        tSSRS = tExeSQL.execSQL(msql + funsql + orderbysql);
        dSumCount += tSSRS.getMaxRow();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            //查询销售渠道
            //strArr = new String[8];

            strArr = new String[9];
            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("salechnl");
            if (SaleChnl != null && !SaleChnl.equals(""))
            {
                tLDCodeDB.setCode(SaleChnl);

            }
            else
            {
                LBPolDB tLBPolDB = new LBPolDB();
                tLBPolDB.setPolNo(tSSRS.GetText(i, 1));
                //tLBPolDB.setEdorNo("86110020040420000034");
                if (!tLBPolDB.getInfo())
                {
                    mErrors.copyAllErrors(tLDCodeDB.mErrors);
                    buildError("SaleChnl", "在取得tLBPolDB的数据时发生错误");
                    return false;
                }
                String SaleChnl1 = tLBPolDB.getSaleChnl();
                tLDCodeDB.setCode(SaleChnl1);
            }
            if (!tLDCodeDB.getInfo())
            {
                mErrors.copyAllErrors(tLDCodeDB.mErrors);
                buildError("SaleChnl", "在取得CodeName的数据时发生错误");
                return false;
            }

            //strArr[5] = tLDCodeDB.getCodeName();
            strArr[6] = tLDCodeDB.getCodeName();
            for (int j = 1; j <= tSSRS.getMaxCol(); j++)
            {
                strArr[j - 1] = tSSRS.GetText(i, j);
            }
            tlistTable.add(strArr);
        }
        // end of preparing list data

        //strArr = new String[8];
        strArr = new String[9];
        //strArr[0] = "PolNo"; strArr[1] ="AppntName";strArr[2] = "InsuredName"; strArr[3] ="Name";strArr[4] ="CodeName";strArr[5] ="";strArr[6] ="";

        strArr[0] = "PolNo";
        strArr[1] = "AppntName";
        strArr[2] = "PrtNo";
        strArr[3] = "InsuredName";
        strArr[4] = "Name";
        strArr[5] = "CodeName";
        strArr[6] = "";
        strArr[7] = "";

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LCPolBill.vts", "printer"); //最好紧接着就初始化xml文档
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("StartTime", mTime[0]);
        texttag.add("EndTime", mTime[1]);
        texttag.add("ManageCom", mLCPolSchema.getManageCom());
        texttag.add("Operator", mGlobalInput.Operator);
        texttag.add("Sum", new java.text.DecimalFormat("0").format(dSumCount));

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        //xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
        mResult.addElement(xmlexport);
        return true;

    }

    public static void main(String[] args)
    {
        LCPolBillF1PBL LCPolBillF1PBL1 = new LCPolBillF1PBL();
    }

//  public String replace(
}
