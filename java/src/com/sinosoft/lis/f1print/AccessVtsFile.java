package com.sinosoft.lis.f1print;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class AccessVtsFile
{
    public AccessVtsFile()
    {
    }

    public static void saveToFile(ByteArrayOutputStream dataStream,
                                  String strVFPathName)
    {
        //store file of VTS to disc 333
        byte[] bArr = dataStream.toByteArray();
        OutputStream wFile = null;
        try
        {
            wFile = new FileOutputStream(strVFPathName);
            wFile.write(bArr);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            try
            {
                wFile.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    public static void loadToBuffer(ByteArrayOutputStream dataStream,
                                    String strVFPathName)
    {
        //read file of VTS from disc
        InputStream rFile = null;
        try
        {
            rFile = new FileInputStream(strVFPathName);
            byte[] bArr = new byte[rFile.available()];
            rFile.read(bArr);
            dataStream.write(bArr);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {            
            try
            {
                rFile.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}