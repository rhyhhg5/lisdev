package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 工单管理系统</p>
 * <p>Description: 电话回访转交BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author Yang Yalin
 * @version 1.0
 * @date 2005-03-31
 */

public class TaskPrintBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private VData mResult = new VData();

    private LGWorkSchema tLGWorkSchema = new LGWorkSchema();

    public TaskPrintBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            mErrors.addOneError("不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        tLGWorkSchema.setSchema((LGWorkSchema) cInputData.getObjectByObjectName(
                "LGWorkSchema", 0));

        if (tLGWorkSchema == null)
        {
            mErrors.addOneError("没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }


    private boolean getPrintData()
   {
       //获得受理号对应的工单信息
       String acceptNo = tLGWorkSchema.getAcceptNo();
       System.out.println("acceptNo=" + acceptNo);

       LGWorkDB tLGWorkDB = new LGWorkDB();
       LGWorkSet tLGWorkSet = new LGWorkSet();
       LGWorkSchema tLGWorkSchema = new LGWorkSchema();

       //获得工单号相应的schema
//       tLGWorkDB.setAcceptNo(acceptNo);
//       tLGWorkSet = tLGWorkDB.query();
       if(null==acceptNo||"".equals(acceptNo)){
           mErrors.addOneError("传入的工单号为空");
           return false;
       }
       String workQuery ="select * from lgwork where AcceptNo='"+acceptNo+"' with ur ";
       tLGWorkSet = tLGWorkDB.executeQuery(workQuery);
       if(tLGWorkSet.size()>0)
       {
           tLGWorkSchema = tLGWorkSet.get(1).getSchema();
       }else{
    	   mErrors.addOneError("工单信息查询失败");
           return false;
       }
       TextTag texttag = new TextTag(); //新建一个TextTag的实例
       XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
       xmlexport.createDocument("TaskPrint.vts", "printer"); //最好紧接着就初始化xml文档

       //生成答应的数据
       StrTool tSrtTool = new StrTool();
       String printDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
                        tSrtTool.getDay() + "日";

       texttag.add("printDate", printDate);
       texttag.add("acceptNo", tLGWorkSchema.getAcceptNo());
       texttag.add("customerNo", tLGWorkSchema.getCustomerNo());

       //获得业务类型
       SSRS tSSRS = new SSRS();
       ExeSQL tExeSQL = new ExeSQL();
       String typeSql = "select workTypeName " +
                        "from LGWorkType " +
                        "where workTypeNo = '" +
                        tLGWorkSchema.getTypeNo().substring(0, 2) + "' ";
       String subTypeSql = "select workTypeName " +
                           "from LGWorkType " +
                           "where workTypeNo = '" +
                           tLGWorkSchema.getTypeNo() + "' ";

       try
       {
           //业务类型
           tSSRS = tExeSQL.execSQL(typeSql);
           texttag.add("type", tSSRS.GetText(1, 1));

           //子业务类型
           tSSRS = tExeSQL.execSQL(subTypeSql);
           if (tSSRS.getMaxRow()!= 0)
           {
               texttag.add("subType", tSSRS.GetText(1, 1));
           }
       }
       catch(Exception e)
       {
           mErrors.addOneError("获取打印数据出错");
           return false;
       }

       LDComDB tLDComDB = new LDComDB();
       tLDComDB.setComCode(mGlobalInput.ComCode);
       tLDComDB.getInfo();
       texttag.add("ComName", tLDComDB.getLetterServiceName());

       texttag.add("BarCode1", tLGWorkSchema.getAcceptNo()); //受理号
       texttag.add("BarCodeParam1",
                   "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

       if (texttag.size() > 0)
       {
           xmlexport.addTextTag(texttag);
       }

       xmlexport.outputDocumentToFile("C:\\", "TaskPrint"); //输出xml文档到文件
       mResult.clear();
       mResult.addElement(xmlexport);

       return true;
   }

   public static void main(String[] args)
    {

        TaskPrintBL tTaskPrintBL = new TaskPrintBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();

        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);

        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setAcceptNo("20050524000012");

        tVData.addElement(tLGWorkSchema);
        tTaskPrintBL.submitData(tVData, "PRINT");

        VData vdata = tTaskPrintBL.getResult();
    }

}
