package com.sinosoft.lis.f1print;

/**
 * <p>Title: X1-实收日结单 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class FFChargeDayModeF1PBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    //取得的时间
    private String mDay[] = null;
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public FFChargeDayModeF1PBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
//        if (!getPrintData()) //getNewPrintData
        if (!getNewPrintData()) //
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    
    /**
     * 遗弃暂不使用，现在使用 ---> getNewPrintData()
     * @return
     */
    private boolean getPrintData()
    {
//        LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
//        LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
        SSRS tSSRS = new SSRS();
        SSRS nSSRS = new SSRS();
        double SumMoney = 0;
        long SumNumber = 0;
        
//        msql = "select CodeName,sum(b.PayMoney),1,b.otherno,(case b.othernotype when '2' then '个单保单号' when '4' then '印刷号' when '3' then '团单批单号' when '7' then '团单保单号' when '10' then '个单批单号' else '其他' end) " +
//        "from LJTempFeeClass a,LJTempFee b,LDCode where a.tempfeeno=b.tempfeeno and CodeType='paymode' and Code=PayMode and PayMode<>'4' and a.EnterAccDate>='" +
//               mDay[0] + "' and a.EnterAccDate<='" + mDay[1] +
//               "' and a.ManageCom like'" + mGlobalInput.ManageCom +
//               "%' group by a.tempfeeno,codename,b.otherno,b.othernotype ";        
//      按方式打印 收费明细SQL
        msql =  "select (case a.paymode when '1' then '现金' when '3' then '转帐支票' when '4' then '银行转帐' when '11' then '银行汇款' when '5' then '内部转帐' when '10' then '白条' else '其他' end),"
        	  + "  a.paymoney, count(distinct b.tempfeeno), b.otherno, (case b.othernotype when '0' then '个人保单号' when '1' then '团体保单号' when '2' then '个人保单号' when '7' then '团体保单号' when '4' then '个人印刷号' when '5' then '团体印刷号' when '10' then '个人批单号' when '3' then '团体批单号' else '其他' end)"
        	  + " from ljtempfeeclass a, ljtempfee b where 1=1 and a.tempfeeno = b.tempfeeno "
        	  + " and b.tempfeetype <> '16' and a.confmakedate>='"+mDay[0]+"' and a.confmakedate<='"+mDay[1]+"'"
        	  + " and a.ManageCom like '" + mGlobalInput.ManageCom + "%' "
        	  + " group by a.paymode, b.othernotype, b.otherno, b.tempfeeno, a.paymoney ";
        	  
        
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        ListTable tlistTable = new ListTable();
        String strSum="";
        String strArr[] = null;
        tlistTable.setName("MODE");
        for (int i = 1; i <= tSSRS.MaxRow; i++)
        {
            strArr = new String[5];
            for (int j = 1; j <= tSSRS.MaxCol; j++)
            {
                if (j == 1)
                {
                	strArr[j - 1] = tSSRS.GetText(i, j);
                }
                if (j == 2)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    strSum = new DecimalFormat("0.00").format(Double.valueOf(strArr[j - 1]));
                    strArr[j - 1] = strSum;   
                    SumMoney = SumMoney + Double.parseDouble(strArr[j - 1]);
                }
                if (j == 3)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                }
                if (j == 4)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                }   
                if (j == 5)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                }                              
            }
            tlistTable.add(strArr);
        }
//        System.out.println("总金额:" + SumMoney);
        strArr = new String[5];
        strArr[0] = "Mode";
        strArr[1] = "Money";
        strArr[2] = "Mult";
        strArr[3] = "ContNo";  
        strArr[4] = "ContNoType";        

//        msql = "select CodeName||'汇总',sum(PayMoney),count(1) from LJTempFeeClass,LDCode where CodeType='paymode' and Code=PayMode and PayMode<>'4' and EnterAccDate>='" +
//               mDay[0] + "' and EnterAccDate<='" + mDay[1] +
//               "' and ManageCom like'" + mGlobalInput.ManageCom +
//               "%' Group By CodeName ";

        // 按方式打印 收费日结汇总SQL
        msql = "select * from ( select (case paymode when '1' then '现金' when '2' then '现金支票' when '3' then '转帐支票' when '4' then '银行转帐' when '5' then '内部转帐' "
        	 + " when '6' then 'pos机' when '9' then '其他' when '12' then '银行代收' when '13' then '赠送保费'  when '10' then '应收保费' when '11' then '银行汇款'   else '其他' end)||'汇总' as CName,"
        	 + " sum(PayMoney) as summoney ,count(1)  as count"
        	 + " from LJTempFeeClass  where not exists (select '1' from ljtempfee where tempfeetype ='16' and tempfeeno = LJTempFeeClass.tempfeeno) "
        	 + " and confmakedate>='"+mDay[0]+"' and confmakedate<='"+mDay[1]+"'"
        	 + " and paymode <> '12' and ManageCom like '"+ mGlobalInput.ManageCom +"%' Group By paymode union all "
        	 + " select trim(insbankaccno) || '汇总' as CName, sum(paymoney) as summoney , count(tempfeeno)  as count from ljtempfeeclass where paymode in ('2','3','4','11')  "
        	 + " and confmakedate>='"+mDay[0]+"' and confmakedate<='"+mDay[1]+"' and managecom like '"+ mGlobalInput.ManageCom +"%' group by insbankaccno) as aa order by 1 desc "
        	 ;
        
        
               
        tSSRS = tExeSQL.execSQL(msql);
        ListTable alistTable = new ListTable();
        String strSumArr[] = null;
        alistTable.setName("SUM");
        for (int i = 1; i <= tSSRS.MaxRow; i++)
        {
            strSumArr = new String[3];
            for (int j = 1; j <= tSSRS.MaxCol; j++)
            {
                if (j == 1)
                {
                    strSumArr[j - 1] = tSSRS.GetText(i, j);
                }
                if (j == 2)
                {
                    strSumArr[j - 1] = tSSRS.GetText(i, j);
                    strSum = new DecimalFormat("0.00").format(Double.
                            valueOf(strSumArr[j - 1]));
                    strSumArr[j - 1] = strSum;
//                    SumMoney = SumMoney + Double.parseDouble(strSumArr[j - 1]);
                }
                if (j == 3)
                {
                    strSumArr[j - 1] = tSSRS.GetText(i, j);
                    SumNumber = SumNumber +
                                Long.valueOf(strSumArr[j - 1]).longValue();
                }               
            }
            alistTable.add(strSumArr);
        }
//        System.out.println("SumNumber: "+SumNumber);
//        String tSumMoney = String.valueOf(SumMoney);
//        tSumMoney = new DecimalFormat("0.00").format(Double.valueOf(tSumMoney));
        strSumArr = new String[]{"付款方式","号码类型","号码","金额","件数"};
//        strSumArr[0] = "Mode";
//        strSumArr[1] = "Money";
//        strSumArr[2] = "Mult";
              
        nsql = "select Name from LDCom where ComCode='" + mGlobalInput.ManageCom + "'";
        ExeSQL nExeSQL = new ExeSQL();
        nSSRS = nExeSQL.execSQL(nsql);
        String manageCom = nSSRS.GetText(1, 1);
        // 按方式打印 收费合计SQL
        StringBuffer sql = new StringBuffer("select '合计', count(tempfeeno),sum(paymoney) from ljtempfeeclass where");
        sql.append(" not exists (select '1' from ljtempfee where tempfeeno = ljtempfeeclass.tempfeeno and tempfeetype = '16')");
        sql.append(" and paymoney <> 0 and paymode <> '12' and confmakedate>='"+mDay[0]+"' and confmakedate<='"+mDay[1]+"'");
        sql.append(" and ManageCom like '"+ mGlobalInput.ManageCom +"%'");
        sql.append(" group by '合计' with ur ");
        // 修改收费汇总
        String tSumMoney = "0";
        nSSRS = nExeSQL.execSQL(sql.toString());
        if(nSSRS.MaxRow > 0){
        	SumNumber = Long.parseLong(nSSRS.GetText(1, 2));
        	tSumMoney = nSSRS.GetText(1, 3);
//        	for(int iCol=1; iCol<=nSSRS.MaxCol; iCol++){
//        		System.out.println(" iCol+"+iCol+nSSRS.GetText(1, iCol));;
//        	}
        }
        
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("FFChargeDayMode.vts", "printer"); //最好紧接着就初始化xml文档
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("ManageCom", manageCom);
        texttag.add("SumMoney", tSumMoney);
        texttag.add("SumNumber", SumNumber);

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        xmlexport.addListTable(alistTable, strSumArr);   
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;

    }
    
    /**
     * 新方法 X1-实收日结单  
     * @return
     */
    private boolean getNewPrintData(){
    	
        SSRS tSSRS = new SSRS();
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
        tGetSQLFromXML.setParameters("EndDate",   mDay[1]);
       
        String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
//        String tServerPath= "E:/lisdev/ui/";
        ExeSQL tExeSQL = new ExeSQL();
        
        String nsql = "select Name from LDCom where ComCode='" + mGlobalInput.ManageCom + "'"; // 管理机构
        tSSRS = tExeSQL.execSQL(nsql);
        String manageCom = tSSRS.GetText(1, 1);
        TextTag texttag = new TextTag();       //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate",   mDay[1]);
        texttag.add("ManageCom", manageCom);
        xmlexport.createDocument("FFChargeDayMode.vts", "printer"); //最好紧接着就初始化xml文档
        
        String[] detailArr = new String[]{"付款方式","号码类型","号码","金额","件数"};

        //  按方式打印 收费明细SQL
        String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "X1SSMX");
       	ListTable tDetailListTable = getDetailListTable(msql, "MODE");      
       	xmlexport.addListTable(tDetailListTable, detailArr);
        	 
       	// 收费日结汇总SQL
       	msql = tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "X1SSMXZ");
       	ListTable tHZListTable = getDetailListTable(msql, "SUM");            
       	xmlexport.addListTable(tHZListTable, detailArr);                            	

       	// 收费合计SQL
    	msql = tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "X1SSHJ"); 
        String[] sTotal = getHJ(msql);// 收费汇总 总金额 总件数
        
        texttag.add("SumMoney",  sTotal[0]);
        texttag.add("SumNumber", sTotal[1]);  
        
        if (texttag.size() > 0){
            xmlexport.addTextTag(texttag);
        }      	
        
        mResult.clear();
        mResult.addElement(xmlexport);
    	return true;
    }
    
    /**
     * 收费合计
     * @param hjSQL
     * @return
     */
    private String[] getHJ(String hjSQL){
    	SSRS nSSRS = new SSRS();
        nSSRS = new ExeSQL().execSQL(hjSQL);
        String[] total = new String[]{"0","0"};
        if(nSSRS.MaxRow > 0){
        	total[0] = nSSRS.GetText(1, 3); // $=/SumNumber
        	total[1] = nSSRS.GetText(1, 2); // $=/SumMoney
        }
        return total;
    }
    
	/**
	 * 获得明细ListTable
	 * @param msql - 执行的SQL
	 * @param tName - Table Name
	 * @return
	 */
	private ListTable getDetailListTable(String msql, String tName){
		SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        ListTable tlistTable = new ListTable();
        String strSum="";
        String strArr[] = null;
        tlistTable.setName(tName);			
        for (int i = 1; i <= tSSRS.MaxRow; i++){
            strArr = new String[tSSRS.MaxCol]; // 列数
            for (int j = 1; j <= tSSRS.MaxCol; j++){
                if (j == 2){
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    strSum = new DecimalFormat("0.00").format(Double.valueOf(strArr[j - 1]));
                    strArr[j - 1] = strSum;
                    continue;
                }
                strArr[j - 1] = tSSRS.GetText(i, j);
            }
            tlistTable.add(strArr);
        }
        return tlistTable;
	}
    
    
}