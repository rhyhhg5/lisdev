/**
 * <p>ClassName: StatHospCircsByGradeUI.java </p>
 * <p>Description: 分合作级别医院设置情况统计 </p>
 * <p>Copyright: Copyright (c) 2010 </p>
 * <p>Company: sinosoft</p>
 * @author 石和平
 * @version 1.0
 * @CreateDate：2010-04-01
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class StatHospCircsByGradeUI {
	public CErrors mErrors = new CErrors();
	private VData mInputData = new VData();
	
	/** 数据操作字符串 */
	private String mOperate;
	private VData mResult = new VData();

	public boolean submitData(VData cInputData,String cOperate){
		//将操作数据拷贝到本类中
        this.mOperate = cOperate;
        
        //得到外部传入的数据,将数据备份到本类中
	    mInputData=(VData)cInputData.clone();
	    
	    System.out.println("String HMComHospSetStaReportBL...");
	    StatHospCircsByGradeBL tStatHospCircsByGradeBL= new StatHospCircsByGradeBL();
	    if (!tStatHospCircsByGradeBL.submitData(mInputData, mOperate)){
            if (tStatHospCircsByGradeBL.mErrors.needDealError()){
                mErrors.copyAllErrors(tStatHospCircsByGradeBL.mErrors);
                return false;
            } else {
                buildError("submitData", "HMComHospSetStaReportUI发生错误，但是没有提供详细的出错信息");
                return false;
	        }
	    } else {
            mResult = tStatHospCircsByGradeBL.getResult();
            return true;
	    }
 	}

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg){
        CError cError = new CError();

        cError.moduleName = "LLCaseStatisticsUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 取得返回处理过的结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }
}