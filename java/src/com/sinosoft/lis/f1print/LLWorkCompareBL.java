package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;
import java.text.DecimalFormat;

public class LLWorkCompareBL
    implements PrintService {

/** 错误处理类，每个需要错误处理的类中都放置该类 */
public CErrors mErrors = new CErrors();

private VData mResult = new VData();

//取得的保单号码
private String mContNo = "";

//输入的查询sql语句
private String msql = "";

//取得的延期承保原因
private String mUWError = "";

//取得的代理人编码
private String mAgentCode = "";

//业务处理相关变量
/** 全局数据 */
private GlobalInput mGlobalInput = new GlobalInput();
private TransferData mTransferData = new TransferData();
private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

private LCContSchema mLCContSchema = new LCContSchema();
private LCContSet mLCContSet = new LCContSet();
private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema(); //立案/申请登记总表
private LLAppealSchema mLLAppealSchema = new LLAppealSchema(); //理赔申诉错误表
private LLAppClaimReasonDB mLLAppClaimReasonDB = new LLAppClaimReasonDB();
private LLAppClaimReasonSchema mLLAppClaimReasonSchema = new
        LLAppClaimReasonSchema();
private LLAppClaimReasonSet mLLAppClaimReasonSet = new LLAppClaimReasonSet();

private String mFileNameB = "";
private String moperator = "";
private String mMakeDate = "";
public LLWorkCompareBL() {
}

/**
 传输数据的公共方法
 */
public boolean submitData(VData cInputData, String cOperate) {
    if (!cOperate.equals("PRINT")) {
        buildError("submitData", "不支持的操作字符串");
        return false;
    }

    // 得到外部传入的数据，将数据备份到本类中
    if (!getInputData(cInputData)) {
        return false;
    }
    System.out.println("1 ...");
    mResult.clear();

    // 准备所有要打印的数据
    if (!getPrintData()) {
        return false;
    }else{
		TransferData tTransferData= new TransferData();
		tTransferData.setNameAndValue("tFileNameB",mFileNameB );
		tTransferData.setNameAndValue("tMakeDate", mMakeDate);
		tTransferData.setNameAndValue("tOperator", moperator);
		LLPrintSave tLLPrintSave = new LLPrintSave();
		VData tVData = new VData();
		tVData.addElement(tTransferData);
		if(!tLLPrintSave.submitData(tVData,"")){
			return false;
		     }
	}
    return true;
}

/**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
 */
private boolean dealData() {
    return true;
}

/**
 * 从输入数据中得到所有对象
 * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData) {
    //全局变量
    mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
            "GlobalInput", 0));
    mTransferData = ((TransferData) cInputData.getObjectByObjectName(
            "TransferData", 0));

    if (mTransferData == null) {
        buildError("getInputData", "没有得到足够的信息！");
        return false;
    }
    mFileNameB = (String)mTransferData.getValueByName("tFileNameB");
    moperator = mGlobalInput.Operator;
    return true;
}

public VData getResult() {
    return mResult;
}

public CErrors getErrors() {
    return mErrors;
}

private void buildError(String szFunc, String szErrMsg) {
    CError cError = new CError();

    cError.moduleName = "LCContF1PBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
}

private boolean getPrintData() {
    boolean PEFlag = false; //打印理赔申请回执的判断标志
    boolean shortFlag = false; //打印备注信息
    TextTag texttag = new TextTag(); //新建一个TextTag的实例
    //根据印刷号查询打印队列中的纪录
    String[] strCol;
    String SpanType = (String) mTransferData.getValueByName("SpanType");
    String MStartDate = (String) mTransferData.getValueByName("MStartDate");
    String MEndDate = (String) mTransferData.getValueByName("MEndDate");
    String YStartDate = (String) mTransferData.getValueByName("YStartDate");
    String YEndDate = (String) mTransferData.getValueByName("YEndDate");

    String Year = (String) mTransferData.getValueByName("Year");
    String Month = (String) mTransferData.getValueByName("Month");
    String Days = (String) mTransferData.getValueByName("Days");
    String NStartDate = String.valueOf(Integer.parseInt(Year) - 1) +"-"+ Month +"-"+ "01";
    String NEndDate = String.valueOf(Integer.parseInt(Year) - 1) +"-"+ Month +"-"+ Days;
    String OYStartDate = String.valueOf(Integer.parseInt(Year) - 1) +"-"+ "01" +"-"+"01";
   String OYEndDate =  String.valueOf(Integer.parseInt(Year) - 1) +"-"+ Month +"-"+ Days;

    ListTable appColListTable = new ListTable();
    ExeSQL tExeSQL = new ExeSQL();
    SSRS tSSRS = new SSRS();
    SSRS kSSRS = new SSRS();
    System.out.print("MStartDate"+MStartDate+"NStartDate"+NStartDate+"MEndDate"+MEndDate+"NEndDate"+NEndDate);
    appColListTable.setName("Report");

    String sql0 = "select  d.comcode,d.name from ldcom d  where comcode in (select distinct mngcom from llcase) or comcode='86' order by comcode desc";

    tSSRS = tExeSQL.execSQL(sql0);

    System.out.println("row=" + tSSRS.getMaxRow());

    for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
        String[] strArr = new String[25];
if(SpanType.equals("1")){

        String Sql2 =
                     "select count(surveyno) from llsurvey where mngcom like '"+ tSSRS.GetText(i, 1)+
                     "%' and surveyflag='3' and surveyenddate BETWEEN '"+MStartDate+"' AND '"+MEndDate+"'";

        kSSRS = tExeSQL.execSQL(Sql2);
        String SurveyCount = kSSRS.GetText(1, 1);

        String Sql3 =
                     " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='5' "+
                     " AND MNGCOM like '"+ tSSRS.GetText(i, 1)+"%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') "+
                     " AND ENDCASEDATE BETWEEN '"+MStartDate+"' AND '"+MEndDate+"'";
        kSSRS = tExeSQL.execSQL(Sql3);
        String RCount = kSSRS.GetText(1, 1);

        String Sql4 =
             " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='4' "+
             " AND MNGCOM like '"+ tSSRS.GetText(i, 1)+"%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') "+
             " AND ENDCASEDATE BETWEEN '"+MStartDate+"' AND '"+MEndDate+"'";
         kSSRS = tExeSQL.execSQL(Sql4);
         String SCount = kSSRS.GetText(1, 1);

        String Sql5 =
                     " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='1' "+
                     " AND MNGCOM like '"+ tSSRS.GetText(i, 1)+"%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') "+
                     " AND ENDCASEDATE BETWEEN '"+MStartDate+"' AND '"+MEndDate+"'"+
                     " AND (TO_DATE(ENDCASEDATE))-(TO_DATE(RGTDATE))=2 ";
        kSSRS = tExeSQL.execSQL(Sql3);
        String CaseCount3 = kSSRS.GetText(1, 1);

        String Sql6 =
             " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='1' "+
             " AND MNGCOM like '"+ tSSRS.GetText(i, 1)+"%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') "+
             " AND ENDCASEDATE BETWEEN '"+MStartDate+"' AND '"+MEndDate+"'"+
             " AND (TO_DATE(ENDCASEDATE))-(TO_DATE(RGTDATE))=6 ";
         kSSRS = tExeSQL.execSQL(Sql6);
         String CaseCount7 = kSSRS.GetText(1, 1);

         String Sql7 =
            " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='1' " +
            " AND MNGCOM like '" + tSSRS.GetText(i, 1) + "%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') " +
            " AND ENDCASEDATE BETWEEN '" + MStartDate + "' AND '" + MEndDate + "'";
         kSSRS = tExeSQL.execSQL(Sql7);
         String CaseCount = kSSRS.GetText(1, 1);

         String Sql15 =
            " SELECT SUM(TO_DATE(A11.ENDCASEDATE)-TO_DATE(A11.RGTDATE)+1) FROM LLCASE  WHERE  RGTTYPE='1' " +
            " AND MNGCOM like '" + tSSRS.GetText(i, 1) +
            "%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '" +
            OYEndDate + "') " +
            " AND ENDCASEDATE BETWEEN '" + MStartDate + "' AND '" + MEndDate + "'";
         kSSRS = tExeSQL.execSQL(Sql15);
         String Time = kSSRS.GetText(1, 1);


         String Sql8 =
                      "select count(surveyno) from llsurvey where mngcom like '"+ tSSRS.GetText(i, 1)+
                      "%' and surveyflag='3' and surveyenddate BETWEEN '"+NStartDate+"' AND '"+NEndDate+"'";

         kSSRS = tExeSQL.execSQL(Sql8);
         String NSurveyCount = kSSRS.GetText(1, 1);

         String Sql9 =
                      " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='5' "+
                      " AND MNGCOM like '"+ tSSRS.GetText(i, 1)+"%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') "+
                      " AND ENDCASEDATE BETWEEN '"+NStartDate+"' AND '"+NEndDate+"'";
         kSSRS = tExeSQL.execSQL(Sql9);
         String NRCount = kSSRS.GetText(1, 1);

         String Sql10 =
              " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='4' "+
              " AND MNGCOM like '"+ tSSRS.GetText(i, 1)+"%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') "+
              " AND ENDCASEDATE BETWEEN '"+NStartDate+"' AND '"+NEndDate+"'";
          kSSRS = tExeSQL.execSQL(Sql10);
          String NSCount = kSSRS.GetText(1, 1);

         String Sql11 =
                      " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='1' "+
                      " AND MNGCOM like '"+ tSSRS.GetText(i, 1)+"%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') "+
                      " AND ENDCASEDATE BETWEEN '"+NStartDate+"' AND '"+NEndDate+"'"+
                      " AND (TO_DATE(ENDCASEDATE))-(TO_DATE(RGTDATE))=2 ";
         kSSRS = tExeSQL.execSQL(Sql11);
         String NCaseCount3 = kSSRS.GetText(1, 1);

         String Sql12 =
              " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='1' "+
              " AND MNGCOM like '"+ tSSRS.GetText(i, 1)+"%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') "+
              " AND ENDCASEDATE BETWEEN '"+NStartDate+"' AND '"+NEndDate+"'"+
              " AND (TO_DATE(ENDCASEDATE))-(TO_DATE(RGTDATE))=6 ";
          kSSRS = tExeSQL.execSQL(Sql12);
          String NCaseCount7 = kSSRS.GetText(1, 1);

          String Sql13 =
             " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='1' " +
             " AND MNGCOM like '" + tSSRS.GetText(i, 1) + "%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') " +
             " AND ENDCASEDATE BETWEEN '" + NStartDate + "' AND '" + NEndDate + "'";
          kSSRS = tExeSQL.execSQL(Sql13);
         String NCaseCount = kSSRS.GetText(1, 1);

         String Sql14 =
            " SELECT SUM(TO_DATE(A11.ENDCASEDATE)-TO_DATE(A11.RGTDATE)+1) FROM LLCASE  WHERE  RGTTYPE='1' " +
            " AND MNGCOM like '" + tSSRS.GetText(i, 1) + "%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') " +
            " AND ENDCASEDATE BETWEEN '" + NStartDate + "' AND '" + NEndDate + "'";
         kSSRS = tExeSQL.execSQL(Sql14);
         String NTime = kSSRS.GetText(1, 1);

        if (tSSRS.GetText(i, 1).equals("86")) {
            strArr[1] = " ";
        } else {
            strArr[0] = tSSRS.GetText(i, 1);
        }
        if (tSSRS.GetText(i, 2).equals("总公司"))
        {
            strArr[1]="总   计";
        }else{
            strArr[1] = tSSRS.GetText(i, 2);
        }
        if (SurveyCount.equals("0")||NSurveyCount.equals("0")) {
            strArr[2] = "0";
        } else {
            strArr[2] = String.valueOf(Arith.round(Double.parseDouble(SurveyCount) /
                                       Double.parseDouble(NSurveyCount),2));
        }

        if (RCount.equals("0")||NRCount .equals("0")) {
            strArr[3] = "0";
        } else {
            strArr[3] = String.valueOf(Arith.round(Double.parseDouble(RCount) /
                                       Double.parseDouble(NRCount),2));
        }
        if (SCount.equals("0")||NSCount.equals("0")) {
            strArr[4] = "0";
        } else {
            strArr[4] = String.valueOf(Arith.round(Double.parseDouble(SCount) /
                                       Double.parseDouble(NSCount),2));
        }
        if (CaseCount3.equals("0")||NCaseCount3.equals("0")) {
            strArr[5] = "0";
        } else {
            strArr[5] = String.valueOf(Arith.round(Double.parseDouble(CaseCount3) /
                                       Double.parseDouble(NCaseCount3),2));
        }
        if (NCaseCount7.equals("0")||CaseCount7.equals("0")) {
            strArr[6] = "0";
        } else {
            strArr[6] = String.valueOf(Arith.round(Double.parseDouble(CaseCount7) /
                                       Double.parseDouble(NCaseCount7),2));
        }
        if (NCaseCount.equals("0")||CaseCount.equals("0")) {
            strArr[7] = "0";
        } else {
            strArr[7] = String.valueOf(Arith.round(Double.parseDouble(CaseCount) /
                                       Double.parseDouble(NCaseCount),2));
        }
        if (NCaseCount.equals("0") || CaseCount.equals("0")||Time.equals("0")||NTime.equals("0")) {
            strArr[8] = "0";
        } else {
            strArr[8] = String.valueOf(Arith.round(((Double.parseDouble(Time)/Double.parseDouble(CaseCount))/
                                                   (Double.parseDouble(NTime)/Double.parseDouble(NCaseCount))),
                                                   2));
        }


}else{
    String Sql2 =
                     "select count(surveyno) from llsurvey where mngcom like '"+ tSSRS.GetText(i, 1)+
                     "%' and surveyflag='3' and surveyenddate BETWEEN '"+YStartDate+"' AND '"+YEndDate+"'";

        kSSRS = tExeSQL.execSQL(Sql2);
        String SurveyCount = kSSRS.GetText(1, 1);

        String Sql3 =
                     " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='5' "+
                     " AND MNGCOM like '"+ tSSRS.GetText(i, 1)+"%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') "+
                     " AND ENDCASEDATE BETWEEN '"+YStartDate+"' AND '"+YEndDate+"'";
        kSSRS = tExeSQL.execSQL(Sql3);
        String RCount = kSSRS.GetText(1, 1);

        String Sql4 =
             " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='4' "+
             " AND MNGCOM like '"+ tSSRS.GetText(i, 1)+"%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') "+
             " AND ENDCASEDATE BETWEEN '"+YStartDate+"' AND '"+YEndDate+"'";
         kSSRS = tExeSQL.execSQL(Sql4);
         String SCount = kSSRS.GetText(1, 1);

        String Sql5 =
                     " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='1' "+
                     " AND MNGCOM like '"+ tSSRS.GetText(i, 1)+"%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') "+
                     " AND ENDCASEDATE BETWEEN '"+YStartDate+"' AND '"+YEndDate+"'"+
                     " AND (TO_DATE(ENDCASEDATE))-(TO_DATE(RGTDATE))=2 ";
        kSSRS = tExeSQL.execSQL(Sql3);
        String CaseCount3 = kSSRS.GetText(1, 1);

        String Sql6 =
             " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='1' "+
             " AND MNGCOM like '"+ tSSRS.GetText(i, 1)+"%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') "+
             " AND ENDCASEDATE BETWEEN '"+YStartDate+"' AND '"+YEndDate+"'"+
             " AND (TO_DATE(ENDCASEDATE))-(TO_DATE(RGTDATE))=6 ";
         kSSRS = tExeSQL.execSQL(Sql6);
         String CaseCount7 = kSSRS.GetText(1, 1);

         String Sql7 =
            " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='1' " +
            " AND MNGCOM like '" + tSSRS.GetText(i, 1) + "%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') " +
            " AND ENDCASEDATE BETWEEN '" + YStartDate + "' AND '" + YEndDate + "'";
         kSSRS = tExeSQL.execSQL(Sql7);
         String CaseCount = kSSRS.GetText(1, 1);

         String Sql15 =
            " SELECT SUM(TO_DATE(A11.ENDCASEDATE)-TO_DATE(A11.RGTDATE)+1) FROM LLCASE  WHERE  RGTTYPE='1' " +
            " AND MNGCOM like '" + tSSRS.GetText(i, 1) +
            "%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '" +
            OYEndDate + "') " +
            " AND ENDCASEDATE BETWEEN '" + MStartDate + "' AND '" + MEndDate + "'";
         kSSRS = tExeSQL.execSQL(Sql15);
         String Time = kSSRS.GetText(1, 1);


         String Sql8 =
                      "select count(surveyno) from llsurvey where mngcom like '"+ tSSRS.GetText(i, 1)+
                      "%' and surveyflag='3' and surveyenddate BETWEEN '"+OYStartDate+"' AND '"+OYEndDate+"'";

         kSSRS = tExeSQL.execSQL(Sql8);
         String NSurveyCount = kSSRS.GetText(1, 1);

         String Sql9 =
                      " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='5' "+
                      " AND MNGCOM like '"+ tSSRS.GetText(i, 1)+"%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') "+
                      " AND ENDCASEDATE BETWEEN '"+OYStartDate+"' AND '"+OYEndDate+"'";
         kSSRS = tExeSQL.execSQL(Sql9);
         String NRCount = kSSRS.GetText(1, 1);

         String Sql10 =
              " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='4' "+
              " AND MNGCOM like '"+ tSSRS.GetText(i, 1)+"%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') "+
              " AND ENDCASEDATE BETWEEN '"+OYStartDate+"' AND '"+OYEndDate+"'";
          kSSRS = tExeSQL.execSQL(Sql10);
          String NSCount = kSSRS.GetText(1, 1);

         String Sql11 =
                      " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='1' "+
                      " AND MNGCOM like '"+ tSSRS.GetText(i, 1)+"%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') "+
                      " AND ENDCASEDATE BETWEEN '"+OYStartDate+"' AND '"+OYEndDate+"'"+
                      " AND (TO_DATE(ENDCASEDATE))-(TO_DATE(RGTDATE))=2 ";
         kSSRS = tExeSQL.execSQL(Sql11);
         String NCaseCount3 = kSSRS.GetText(1, 1);

         String Sql12 =
              " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='1' "+
              " AND MNGCOM like '"+ tSSRS.GetText(i, 1)+"%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') "+
              " AND ENDCASEDATE BETWEEN '"+OYStartDate+"' AND '"+OYEndDate+"'"+
              " AND (TO_DATE(ENDCASEDATE))-(TO_DATE(RGTDATE))=6 ";
          kSSRS = tExeSQL.execSQL(Sql12);
          String NCaseCount7 = kSSRS.GetText(1, 1);

          String Sql13 =
             " SELECT COUNT(CASENO) FROM LLCASE  WHERE  RGTTYPE='1' " +
             " AND MNGCOM like '" + tSSRS.GetText(i, 1) + "%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') " +
             " AND ENDCASEDATE BETWEEN '" + OYStartDate + "' AND '" + OYEndDate + "'";
          kSSRS = tExeSQL.execSQL(Sql13);
         String NCaseCount = kSSRS.GetText(1, 1);

         String Sql14 =
            " SELECT SUM(TO_DATE(A11.ENDCASEDATE)-TO_DATE(A11.RGTDATE)+1) FROM LLCASE  WHERE  RGTTYPE='1' " +
            " AND MNGCOM like '" + tSSRS.GetText(i, 1) + "%' AND ENDCASEDATE IS NOT NULL AND (RGTSTATE<>'14' or cancledate > '"+OYEndDate+"') " +
            " AND ENDCASEDATE BETWEEN '" + OYStartDate + "' AND '" + OYEndDate + "'";
         kSSRS = tExeSQL.execSQL(Sql14);
         String NTime = kSSRS.GetText(1, 1);


        if (tSSRS.GetText(i, 1).equals("86")) {
            strArr[1] = " ";
        } else {
            strArr[0] = tSSRS.GetText(i, 1);
        }
        if (tSSRS.GetText(i, 2).equals("总公司"))
        {
            strArr[1]="总   计";
        }else{
            strArr[1] = tSSRS.GetText(i, 2);
        }
        if (SurveyCount.equals("0")||NSurveyCount.equals("0")) {
            strArr[2] = "0";
        } else {
            strArr[2] = String.valueOf(Arith.round(Double.parseDouble(SurveyCount) /
                                       Double.parseDouble(NSurveyCount),2));
        }

        if (RCount.equals("0")||NRCount .equals("0")) {
            strArr[3] = "0";
        } else {
            strArr[3] = String.valueOf(Arith.round(Double.parseDouble(RCount) /
                                       Double.parseDouble(NRCount),2));
        }
        if (SCount.equals("0")||NSCount.equals("0")) {
            strArr[4] = "0";
        } else {
            strArr[4] = String.valueOf(Arith.round(Double.parseDouble(SCount) /
                                       Double.parseDouble(NSCount),2));
        }
        if (CaseCount3.equals("0")||NCaseCount3.equals("0")) {
            strArr[5] = "0";
        } else {
            strArr[5] = String.valueOf(Arith.round(Double.parseDouble(CaseCount3) /
                                       Double.parseDouble(NCaseCount3),2));
        }
        if (NCaseCount7.equals("0")||CaseCount7.equals("0")) {
            strArr[6] = "0";
        } else {
            strArr[6] = String.valueOf(Arith.round(Double.parseDouble(CaseCount7) /
                                       Double.parseDouble(NCaseCount7),2));
        }
        if (NCaseCount.equals("0")||CaseCount.equals("0")) {
            strArr[7] = "0";
        } else {
            strArr[7] = String.valueOf(Arith.round(Double.parseDouble(CaseCount) /
                                       Double.parseDouble(NCaseCount),2));
        }
        if (NCaseCount.equals("0") || CaseCount.equals("0") || Time.equals("0") ||
            NTime.equals("0")) {
            strArr[8] = "0";
        } else {
            strArr[8] = String.valueOf(Arith.round(((Double.parseDouble(Time) /
                                                     Double.parseDouble(CaseCount)) /
                                                    (Double.parseDouble(NTime) /
                                                     Double.parseDouble(NCaseCount))),
                                                   2));
        }




}

        appColListTable.add(strArr);

    }


        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LLWorkCompare.vts", "printer"); //最好紧接着就初始化xml文档
        //生成-年-月-日格式的日期
        String StaDate = "";
        if (SpanType.equals("1")) {
            texttag.add("TableName", "月理赔同比");
            StaDate = Year + "年" + Month + "月";
        }
        if (SpanType.equals("2")) {
            texttag.add("TableName", "年理赔同比");
            StaDate = Year + "年";
        }
        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
                         tSrtTool.getDay() + "日";
        //PayColPrtBL模版元素
        texttag.add("SysDate", SysDate);
        texttag.add("StaDate", StaDate);
        String Operator=mGlobalInput.Operator;
        texttag.add("Operator", Operator);
        String CurrentDate = PubFun.getCurrentDate();
        CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");
        mMakeDate = CurrentDate;
        System.out.print("CurrentDate"+CurrentDate);
        texttag.add("CurrentDate", CurrentDate);
        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        String[] Title = {
                     "ComCode", "ComName", "MCaseCount", "MSubRptCount",
                     "MPayAmnt", "MPayRate", "MSpan","YCaseCount",
                     "YSubRptCount","YPayAmnt", "YPayRate","YSpan"};

        //保存信息
        xmlexport.addListTable(appColListTable, Title); //保存申请资料信息
//    xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

       return true;
    }


public static void main(String[] args) {
    TransferData PrintElement = new TransferData();
    PrintElement.setNameAndValue("MStartDate", "2006-6-1");
    PrintElement.setNameAndValue("MEndDate", "2006-6-30");
    PrintElement.setNameAndValue("YStartDate", "2006-1-1");
    PrintElement.setNameAndValue("YEndDate", "2006-6-30");
    PrintElement.setNameAndValue("Year", "2006");
    PrintElement.setNameAndValue("Month", "6");
    PrintElement.setNameAndValue("Days", "30");
    PrintElement.setNameAndValue("SpanType", "1");
    GlobalInput tG = new GlobalInput();
    tG.ManageCom = "86110000";
    tG.Operator = "001";
    VData tVData = new VData();
    tVData.addElement(PrintElement);
    tVData.addElement(tG);

    LLRsuCompareUI tLLRsuCompareUI = new LLRsuCompareUI();
    tLLRsuCompareUI.submitData(tVData, "PRINT");
}
}
