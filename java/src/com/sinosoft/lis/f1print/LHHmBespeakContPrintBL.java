package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;
import java.text.DecimalFormat;
import com.sinosoft.lis.f1print.*;
import java.lang.*;

public class LHHmBespeakContPrintBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LDPersonSchema mLDPersonSchema = new LDPersonSchema();
    private LHHmServBeManageSchema mLHHmServBeManageSchema = new LHHmServBeManageSchema();
    private TransferData mTransferData = new TransferData();

    public LHHmBespeakContPrintBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("1 ...");
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        this.mLDPersonSchema.setSchema((LDPersonSchema)
                                       cInputData.getObjectByObjectName(
                                               "LDPersonSchema", 0));
        System.out.println("CustomerNO  "+mLDPersonSchema.getCustomerNo());
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        if (mLDPersonSchema == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (mLDPersonSchema.getCustomerNo() == null) {
            buildError("getInputData", "没有得到足够的信息:客户号不能为空！");
            return false;
        }
        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        if (mTransferData == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LHHmBespeakContPrintBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        //得到客户基本信息
        String aCustomerNo = "";
        String aName = "";
        String aSex = "";
        String aAge = "";
        String aIDType = "";
        String aIDNo = "";
        String aRemark="";
        String aBirthday="";
        String aLinkMan = "";
        String aLinkTel = "";
      //服务预约信息
        String aBalanceManner = "";
        String aBespeakComID = "";
        String aMakeDate = "";
        String aTaskExecNo = "";
        String aServBespeakDate = "";
        String aManageCom="";
        String aTestGrpCode="";
        String ServBespeakDate="";
        String aOtherSign="";
        aCustomerNo = mLDPersonSchema.getCustomerNo();
        LDPersonDB mLDPersonDB = new LDPersonDB();
        mLDPersonDB.setCustomerNo(aCustomerNo);

        if (!mLDPersonDB.getInfo()) {
            System.out.println("--------DB_GetInfo_Fail--------");
        }
        aCustomerNo = mLDPersonDB.getCustomerNo();
        aName = mLDPersonDB.getName();
        aSex = mLDPersonDB.getSex();
        aIDType = mLDPersonDB.getIDType();
        aIDNo = mLDPersonDB.getIDNo();
        aRemark = mLDPersonDB.getRemark();
        aBirthday= mLDPersonDB.getBirthday();


        System.out.println("aCustomerNo:" + aCustomerNo + "---aName:" +
                           aName + "---aSex:" + aSex + "---aIDType" +
                          aIDType+"aGrpName:"+aIDNo+"aRemark:"+aRemark+
                          "aBirthday:"+aBirthday+"aAge:" + aAge);

      //服务预约基本信息
       try {
           SSRS tSSRS_B = new SSRS();
           String sql =  " select (select  HospitName from LDHospital where LDHospital.HospitCode=a.BespeakComID),"
                         +" a.ServBespeakDate,a.BalanceManner,"
                        +" (select distinct c.Testgrpname from ldtestgrpmgt c where ( c.testgrptype='3' or c.testgrptype='2') and c.Testgrpcode=a.TestGrpCode), "
                        +" (select distinct f.othersign from LDTestGrpMgt  f where  f.TestGrpType = '3'   and f.TestGrpCode=a.TestGrpCode) "
                       +" from   LHHmServBeManage a "
                        +" where  a.CustomerNo = '" + aCustomerNo + "' "
                        ;

           System.out.println(sql);
           ExeSQL tExeSQL = new ExeSQL();
          tSSRS_B = tExeSQL.execSQL(sql);
           int count_B = tSSRS_B.getMaxRow();
          for (int i = 0; i < count_B; i++) {
               String temp_B[][] = tSSRS_B.getAllData();
               aBespeakComID = temp_B[i][0];
               aServBespeakDate = temp_B[i][1];
               aBalanceManner = temp_B[i][2];
               aTestGrpCode = temp_B[i][3];
                aOtherSign = temp_B[i][4];
             System.out.println("---aBespeakComID:" +
                                aBespeakComID + "---aServBespeakDate:" + aServBespeakDate + "---aBalanceManner" +
                                aBalanceManner+ "---aTestGrpCode:" +aTestGrpCode+ "---aOtherSign:" +aOtherSign);
             // appColListTable.add(strCol);
          }
      } catch (Exception e) {
          CError tError = new CError();
           tError.moduleName = "LHHmServBeManageBL";
          tError.functionName = "";
          tError.errorMessage = "客户基本信息查询失败";
         // this.mErrors.addOneError(/);
     }



        //其它模版上单独不成块的信息

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LhPersonBeSpeak.vts", "printer"); //最好紧接着就初始化xml文档
        //生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
                         tSrtTool.getDay() + "日";
         ServBespeakDate=aServBespeakDate.replaceFirst("-","年");

         ServBespeakDate=ServBespeakDate.replaceFirst("-","月");

        texttag.add("CustomerNo", aCustomerNo);
        texttag.add("Name", aName);

        texttag.add("IDNo", aIDNo);
        //texttag.add("Remark", aRemark);
        if (aSex.equals("0")) {
            aSex = "男";
        } else {
            aSex = "女";
        }
         if (aIDType.equals("0")) {
              aIDType = "身份证";
         }
         if (aIDType.equals("1")) {
             aIDType = "护照";
         }
         if (aIDType.equals("2")) {
             aIDType = "军官证";
         }
         if (aIDType.equals("3")) {
             aIDType = "驾照";
         }
         if (aIDType.equals("4")) {
             aIDType = "户口本";
         }
         if (aIDType.equals("5")) {
             aIDType = "学生证";
         }
         if (aIDType.equals("6")) {
             aIDType = "工作证";
         }
         if (aIDType.equals("9")) {
             aIDType = "无证件";
         }

         texttag.add("IDType", aIDType);
        System.out.println(aSex);
        texttag.add("Sex", aSex);
        String Age = this.getAge(aBirthday);
        System.out.println("aBirthday = " + Age);
        texttag.add("Age", Age);

        texttag.add("BespeakComID", aBespeakComID);
        texttag.add("ServBespeakDate", ServBespeakDate);
        texttag.add("TestGrpCode", aTestGrpCode);
        if (aBalanceManner.equals("1")) {
             aBalanceManner = "公司结算";
         }
         if (aBalanceManner.equals("2")) {
             aBalanceManner = "客户结算";
         }
         if (aBalanceManner.equals("3")) {
             aBalanceManner = "客户结算（按协议价格）";
         }
         if(aOtherSign.equals("Y"))
         {
             aOtherSign="是";
         }
         if(aOtherSign.equals("N"))
         {
             aOtherSign = "否";
         }

        texttag.add("BalanceManner",aBalanceManner);
        texttag.add("OtherSign",aOtherSign);
        SSRS tTask = null;
        String sql1="select name  from ldcom where comcode='"+ mGlobalInput.ManageCom +"'";
        ExeSQL tExeSQL= new ExeSQL();
        tTask=tExeSQL.execSQL(sql1);
        String[][] msgInfo = tTask.getAllData();
        System.out.println("AAAAAAAAAAAAAAAAAAAa "+msgInfo[0][0]);
        if(msgInfo[0][0].equals("总公司"))
        {
            texttag.add("Corp", msgInfo[0][0]);
        }
        else
        {
            texttag.add("Corp", msgInfo[0][0]+" 分公司");
        }

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }

        //保存信息
        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    private String getAge(String BirthDay) {
        Integer ii = new Integer("0");
               Integer ii2 = new Integer("0");
               int i = (ii2.parseInt(PubFun.getCurrentDate().substring(0, 4)) -
                        ii.parseInt(BirthDay.substring(0, 4)));

               if (ii2.parseInt(PubFun.getCurrentDate().substring(5, 7)) -
                   ii.parseInt(BirthDay.substring(5, 7)) > 0)
               {
                   BirthDay = i + "";
               }
               else
               {
                   if ((ii2.parseInt(PubFun.getCurrentDate().substring(5, 7)) -
                        ii.parseInt(BirthDay.substring(5, 7))) < 0)
                   {
                       BirthDay = (i - 1) + "";
                   }
                   else
                   {
                       if ((ii2.parseInt(PubFun.getCurrentDate().substring(8, 10)) -
                            ii.parseInt(BirthDay.substring(8, 10))) < 0)
                       {
                           BirthDay = (i - 1) + "";
                       }
                       else
                       {
                           BirthDay = i + "";
                       }
                   }
               }
        return BirthDay;
    }

    public static void main(String[] args) {

    }

    private void jbInit() throws Exception {
    }

}
