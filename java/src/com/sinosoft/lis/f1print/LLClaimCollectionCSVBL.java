package com.sinosoft.lis.f1print;

/**
 * <p>Title: LLBranchCaseReportBL</p>
 * <p>Description: 承保赔付汇总表</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author MN
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.io.File;
import java.sql.*;

public class LLClaimCollectionCSVBL {
	public LLClaimCollectionCSVBL() {
	}

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 全局变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mCvaliDateS = ""; // 生效起期

	private String mCvaliDateE = ""; // 生效止期

	private String mEndCaseDateS = ""; // 结案起期

	private String mEndCaseDateE = ""; // 结案止期

	private String mManageCom = "";

	private String mManageComName = "";

	private String mOperator = "";

	private String mGrpContNo = "";

	private String mGrpName = "";

	private String mPeoples = "";

	private String mPrem = "";

	private String mGrpType = "";

	private String mRiskCode = "";

	private String mGrpTypeName = "";

	private DecimalFormat mDF = new DecimalFormat("0.##");

	// 首期保费合计
	private double mSumFPrem = 0.0;

	// 承保保费合计
	private double mSumSPrem = 0.0;

	// 经过保费合计
	private double mSumTPrem = 0.0;

	// 实收保费合计
	private double mSumActu = 0.0;

	// 被保费险人数合计
	private int mSumPeoples = 0;

	// 赔付件数合计
	private int mSumCase = 0;

	// 结案人数合计
	private int mSumECase = 0;

	// 结案赔款合计
	private double mSumERealPay = 0.0;

	// 财务实付赔款合计
	private double mSumARealPay = 0.0;

	// 承保在职人数
	private int mOnWork = 0;

	// 承保退休人数
	private int mOffWork = 0;

	// 理赔在职人数
	private int mClaimOnWork = 0;

	// 理赔退休人数
	private int mClaimOffWork = 0;

	// 理赔退休人次
	private int mClaimOffTime = 0;

	// 理赔在职人次
	private int mClaimOnTime = 0;

	// 退休人员赔款
	private double mOffRealpay = 0.0;

	// 在职人员赔款
	private double mOnRealpay = 0.0;

	 //  共保反冲赔款
	private double mClaimmoney = 0.0;
	
	/** 统计类型 */
	private String mStatsType = "";
	
	private String[][] mShowDataList = null;

	private XmlExport mXmlExport = null;

	private String[] mDataList = null;

	private ListTable mListTable = new ListTable();

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mFileNameB = "";

	private TransferData mTransferData = new TransferData();

	private CSVFileWrite mCSVFileWrite = null;

	private String mFileName = "";

	private String mFilePath = "";

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行数据查询
		if (!queryData()) {
			return false;
		}

		return true;
	}

	/**
	 * 按照重点业务类型统计
	 * 
	 * @param aGrpType
	 *            String
	 * @return boolean
	 */
	private boolean getDataList(String aGrpType) {
		// 首期保费合计
		double tSumFPrem = 0.0;
		// 承保保费合计
		double tSumSPrem = 0.0;
		// 经过保费合计
		double tSumTPrem = 0.0;
		// 实收保费合计
		double tSumActu = 0.0;
		// 被保费险人数合计
		int tSumPeoples = 0;
		// 赔付件数合计
		int tSumCase = 0;
		// 结案人数合计
		int tSumECase = 0;
		// 结案赔款合计
		double tSumERealPay = 0.0;
		// 财务实付赔款合计
		double tSumARealPay = 0.0;

		// 承保在职人数
		int tOnWork = 0;
		// 承保退休人数
		int tOffWork = 0;
		// 理赔在职人数
		int tClaimOnWork = 0;
		// 理赔退休人数
		int tClaimOffWork = 0;
		// 理赔退休人次
		int tClaimOffTime = 0;
		// 理赔在职人次
		int tClaimOnTime = 0;
		// 退休人员赔款
		double tOffRealpay = 0.0;
		// 在职人员赔款
		double tOnRealpay = 0.0;
        //	  共保反冲赔款
    	double tClaimmoney = 0.0;
    	
		StringBuffer tSQL = new StringBuffer();
		tSQL
		.append("select a.managecom,")
		.append(" (select name from ldcom where comcode=a.managecom),")
		.append(" a.grpcontno,a.grppolno,a.riskcode,")
		.append(
				" (select riskname from lmrisk where riskcode=a.riskcode),")
		.append(
				" (select coalesce(sum(d.sumactupaymoney),0) from lqclaimactug d")
		.append(" where a.grppolno = d.grppolno and d.makedate < '")
		.append(mEndCaseDateE)
		.append("'),")
		.append(
				" (select peoples2 from lcgrpcont where grpcontno=a.grpcontno), coalesce(days(a.payenddate) - days(a.firstpaydate)+1,0),")
		.append(
				" (select count(distinct llcase.caseno) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=a.grppolno and llcase.rgtstate in ('09','11','12') and llcase.endcasedate between '"
						+ mEndCaseDateS
						+ "' and '"
						+ mEndCaseDateE
						+ "'),")
		.append(
				" (select count(distinct customerno) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=a.grppolno and llcase.rgtstate in ('09','11','12') and llcase.endcasedate between '"
						+ mEndCaseDateS
						+ "' and '"
						+ mEndCaseDateE
						+ "'),")
		.append(
				" (select coalesce(sum(realpay),0) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=a.grppolno and llcase.rgtstate in ('09','11','12') and llcase.endcasedate between '"
						+ mEndCaseDateS
						+ "' and '"
						+ mEndCaseDateE
						+ "'),")
		.append(
				" coalesce((select sum(realpay) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=a.grppolno and rgtstate ='12' and llcase.endcasedate between '"
						+ mEndCaseDateS
						+ "' and '"
						+ mEndCaseDateE
						+ "'),0), ")
		.append(" getUniteCode(a.agentcode), ")
		.append(
				" (select Name from laagent where agentcode=a.agentcode fetch first 1 rows only), ")
		.append(" a.AgentCom, ")
		.append(
				" (select name from lacom where AgentCom=a.AgentCom),a.paytodate, ")
		.append(
				" (select codealias from ldcode1 where codetype='salechnl' and code=a.salechnl fetch first 1 rows only), ")
		.append(
				" (select branchattr from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), ")
		.append(
				" (select Name from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), ")
	    .append(
				" (select max(llsubreport.accdate) from llsubreport,llcaserela,llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and llcaserela.caseno=llcase.caseno and llsubreport.subrptno=llcaserela.subrptno  and grppolno=a.grppolno and llcase.rgtstate in ('09','11','12') and llcase.endcasedate between '"
						+ mEndCaseDateS
						+ "' and '"
						+ mEndCaseDateE
						+ "') ")
		.append(" from lcgrppol a")
		.append(" where a.appflag='1' and a.cvalidate between '")
		.append(mCvaliDateS)
		.append("' and '")
		.append(mCvaliDateE)
		.append("' and a.managecom like '")
		.append(mManageCom)
		.append("%'")
		.append(getGrpType(aGrpType))
		.append(getRiskCode(mRiskCode))
		.append(getContNo(mGrpContNo))
		.append(getStatsType(mStatsType))//社保业务清分功能
		.append(getGrpName(mGrpName))
		.append(getPeoples(mPeoples))
		.append(getActu(mPrem))
		.append(" union select a.managecom,")
		.append(" (select name from ldcom where comcode=a.managecom),")
		.append(" a.grpcontno,a.grppolno,a.riskcode,")
		.append(
				" (select riskname from lmrisk where riskcode=a.riskcode),")
		.append(
				" (select coalesce(sum(d.sumactupaymoney),0) from lqclaimactug d")
		.append(" where a.grppolno = d.grppolno and d.makedate < '")
		.append(mEndCaseDateE)
		.append("'),")
		.append(
				" (select peoples2 from lcgrpcont where grpcontno=a.grpcontno), coalesce(days(a.payenddate) - days(a.firstpaydate)+1,0),")
		.append(
				" (select count(distinct llcase.caseno) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=a.grppolno and llcase.rgtstate in ('09','11','12') and llcase.endcasedate between '"
						+ mEndCaseDateS
						+ "' and '"
						+ mEndCaseDateE
						+ "'),")
		.append(
				" (select count(distinct customerno) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=a.grppolno and llcase.rgtstate in ('09','11','12') and llcase.endcasedate between '"
						+ mEndCaseDateS
						+ "' and '"
						+ mEndCaseDateE
						+ "'),")
		.append(
				" (select coalesce(sum(realpay),0) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=a.grppolno and llcase.rgtstate in ('09','11','12') and llcase.endcasedate between '"
						+ mEndCaseDateS
						+ "' and '"
						+ mEndCaseDateE
						+ "'),")
		.append(
				" coalesce((select sum(realpay) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=a.grppolno and rgtstate ='12' and llcase.endcasedate between '"
						+ mEndCaseDateS
						+ "' and '"
						+ mEndCaseDateE
						+ "'),0), ")
		.append(" getUniteCode(a.agentcode), ")
		.append(
				" (select Name from laagent where agentcode=a.agentcode fetch first 1 rows only), ")
		.append(" a.AgentCom, ")
		.append(
				" (select name from lacom where AgentCom=a.AgentCom),a.paytodate, ")
		.append(
				" (select codealias from ldcode1 where codetype='salechnl' and code=a.salechnl fetch first 1 rows only), ")
		.append(
				" (select branchattr from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), ")
		.append(
				" (select Name from LABranchGroup where AgentGroup=a.AgentGroup fetch first 1 rows only), ")
		.append(
				" (select max(llsubreport.accdate) from llsubreport,llcaserela,llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and llcaserela.caseno=llcase.caseno and llsubreport.subrptno=llcaserela.subrptno  and grppolno=a.grppolno and llcase.rgtstate in ('09','11','12') and llcase.endcasedate between '"
						+ mEndCaseDateS
						+ "' and '"
						+ mEndCaseDateE
						+ "') ")
		.append(" from lbgrppol a").append(
				" where a.appflag='1' and a.cvalidate between '")
		.append(mCvaliDateS).append("' and '").append(mCvaliDateE)
		.append("' and a.managecom like '").append(mManageCom).append(
				"%'").append(getGrpType(aGrpType)).append(
				getRiskCode(mRiskCode)).append(getContNo(mGrpContNo)).append(getStatsType(mStatsType))//社保业务清分功能
		.append(getGrpName(mGrpName)).append(getPeoples(mPeoples))
		.append(getActu(mPrem)).append(" with ur");

		String tSeachSQL = tSQL.toString();
		System.out.println("tSeachSQL="+tSeachSQL);
		RSWrapper rsWrapper = new RSWrapper();
		if (!rsWrapper.prepareData(null, tSeachSQL)) {
			System.out.println("数据准备失败! ");
			return false;
		}
		// 是否合计
		boolean tHaveFlag = false;
		try {
			SSRS tSSRS = new SSRS();
			do {
				tSSRS = rsWrapper.getSSRS();
				if (tSSRS != null || tSSRS.MaxRow > 0) {
					String tContent[][] = new String[tSSRS.getMaxRow()][];
					for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
						tHaveFlag = true;
						String tGrpContNo = "";
						String tGrpPolNo = "";
						// 缴费天数
						int tPayDays = 0;
						// 统计期内有效天数
						int tTDays = 0;
						// 每天保费
						double tDayPrem = 0.0;

						String ListInfo[] = new String[40];
						tGrpContNo = tSSRS.GetText(i, 3);
						tGrpPolNo = tSSRS.GetText(i, 4);
						tPayDays = Integer.parseInt(tSSRS.GetText(i, 9));

						ListInfo[0] = tSSRS.GetText(i, 1);// 机构编码
						ListInfo[1] = tSSRS.GetText(i, 2);// 机构名称
						ListInfo[4] = tGrpContNo;// 保单号
						ListInfo[14] = tSSRS.GetText(i, 5);// 险种代码
						ListInfo[15] = tSSRS.GetText(i, 6);// 险种名称
						ListInfo[24] = mDF.format(Arith.round(Double
								.parseDouble(tSSRS.GetText(i, 7)), 2));// 实收保费
						tSumActu += Double.parseDouble(ListInfo[24]);
						mSumActu += Double.parseDouble(ListInfo[24]);
						ListInfo[25] = mDF.format((Integer.parseInt(tSSRS
								.GetText(i, 8))));// 被保险人数
						tSumPeoples += Integer.parseInt(ListInfo[25]);
						mSumPeoples += Integer.parseInt(ListInfo[25]);
						ListInfo[28] = mDF.format((Integer.parseInt(tSSRS
								.GetText(i, 10))));// 赔付件数
						tSumCase += Integer.parseInt(ListInfo[28]);
						mSumCase += Integer.parseInt(ListInfo[28]);
						ListInfo[29] = mDF.format((Integer.parseInt(tSSRS
								.GetText(i, 11))));// 理赔结案人数
						tSumECase += Integer.parseInt(ListInfo[29]);
						mSumECase += Integer.parseInt(ListInfo[29]);
						ListInfo[34] = mDF.format(Arith.round(Double
								.parseDouble(tSSRS.GetText(i, 12)), 2));// 结案赔款
						tSumERealPay += Double.parseDouble(ListInfo[34]);
						mSumERealPay += Double.parseDouble(ListInfo[34]);
						ListInfo[38] = mDF.format(Arith.round(Double
								.parseDouble(tSSRS.GetText(i, 13)), 2));// 财务实付赔款
						tSumARealPay += Double.parseDouble(ListInfo[38]);
						mSumARealPay += Double.parseDouble(ListInfo[38]);
                        //结案案件最晚事故日期
						ListInfo[39] = tSSRS.GetText(i, 22);
						// 业务员编码
						ListInfo[10] = tSSRS.GetText(i, 14);
						// 业务员姓名
						ListInfo[11] = tSSRS.GetText(i, 15);
						// 中介机构编码
						ListInfo[5] = tSSRS.GetText(i, 16);
						// 中介机构名称
						ListInfo[6] = tSSRS.GetText(i, 17);
						// 交至日期
						ListInfo[21] = tSSRS.GetText(i, 18);
						// 销售渠道
						ListInfo[7] = tSSRS.GetText(i, 19);
						// 所属营业部代码
						ListInfo[8] = tSSRS.GetText(i, 20);
						// 所属营业部名称
						ListInfo[9] = tSSRS.GetText(i, 21);
						
						
						 
                        /**add by qianzq
						 * 共保反冲赔款
						 */
						StringBuffer claimmoneySQL = new StringBuffer();
						claimmoneySQL
								.append(
										"select -sum(claimmoney) from lcispayget where  1=1 and grppolno = '")
								.append(tGrpPolNo)
								.append(
										"' and othernotype in ('5','C','F') and state = '2' and recoildate between '")
								.append(mEndCaseDateS).append("' and '")
								.append(mEndCaseDateE).append("' with ur");

						System.out.println("claimmoneySQL        =  "
								+ claimmoneySQL);

						ExeSQL claimmoneyExeSQL = new ExeSQL();
						String tclaimmoneySQL = claimmoneySQL.toString();

						SSRS tClaimmoneySSRS = claimmoneyExeSQL
								.execSQL(tclaimmoneySQL);
						System.out.println(tClaimmoneySSRS.getMaxRow());
						
						String a = tClaimmoneySSRS.GetText(1, 1);
						System.out.println(a);
						
						if (!a.equals(null)&&!a.equals("null")&&!a.equals("") ) {
							
							ListInfo[35] = a;
							
							tClaimmoney+=Double.parseDouble(a); //算合计
							mClaimmoney+=Double.parseDouble(a); //算合计
							
						} else {
							
							ListInfo[35] = "0";
						}
                        

						StringBuffer tGrpSQL = new StringBuffer();
						tGrpSQL
								.append("select a.appntno,a.grpname,")
								.append(
										" (select codename from ldcode where codetype='markettype' and code=a.markettype),")
								.append(
										" (case when a.bigprojectflag ='1' then '大项目' else '非大项目' end),")
								.append(
										" (select f.codename from ldcode1 e,ldcode f where e.code1=a.grpcontno and e.code=f.code and e.codetype=f.codetype and f.codetype='llgrptype' fetch first 1 rows only)")
								.append(" , a.cvalidate,a.cinvalidate, ")
								.append(
										" a.onworkpeoples,a.offworkpeoples,a.signdate ")
								.append(" from lcgrpcont a where a.grpcontno='")
								.append(tGrpContNo)
								.append(
										"' union all select a.appntno,a.grpname,")
								.append(
										" (select codename from ldcode where codetype='markettype' and code=a.markettype),")
								.append(
										" (case when a.bigprojectflag ='1' then '大项目' else '非大项目' end),")
								.append(
										" (select f.codename from ldcode1 e,ldcode f where e.code1=a.grpcontno and e.code=f.code and e.codetype=f.codetype and f.codetype='llgrptype'  fetch first 1 rows only)")
								.append(" , a.cvalidate,a.cinvalidate, ")
								.append(
										" a.onworkpeoples,a.offworkpeoples,a.signdate ")
								.append(" from lbgrpcont a where a.grpcontno='")
								.append(tGrpContNo).append("' with ur");
						ExeSQL tExeSQL = new ExeSQL();
						String tGrpSQLS = tGrpSQL.toString();
						System.out.println(tGrpSQLS);
						SSRS tGrpSSRS = tExeSQL.execSQL(tGrpSQLS);
						if (tGrpSSRS.getMaxRow() > 0) {
							// 团体客户号
							ListInfo[3] = tGrpSSRS.GetText(1, 1);
							// 投保人
							ListInfo[2] = tGrpSSRS.GetText(1, 2);
							// 市场类型
							ListInfo[12] = tGrpSSRS.GetText(1, 3);
							// 项目属性
							ListInfo[13] = tGrpSSRS.GetText(1, 4);
							// 重点业务
							// ListInfo[] = tGrpSSRS.GetText(1, 5);
							// 生效日期
							ListInfo[17] = tGrpSSRS.GetText(1, 6);
							// 失效日期
							ListInfo[18] = tGrpSSRS.GetText(1, 7);
							// 投保在职人数
							ListInfo[27] = tGrpSSRS.GetText(1, 8);
							tOnWork += Integer.parseInt(ListInfo[27]);
							mOnWork += Integer.parseInt(ListInfo[27]);
							// 投保退休人数
							ListInfo[26] = tGrpSSRS.GetText(1, 9);
							tOffWork += Integer.parseInt(ListInfo[26]);
							mOffWork += Integer.parseInt(ListInfo[26]);

							// 承保日期
							ListInfo[16] = tGrpSSRS.GetText(1, 10);

							mGrpTypeName = ListInfo[2];
						} else {
							ListInfo[3] = "";
							ListInfo[2] = "";
							ListInfo[12] = "";
							ListInfo[13] = "";
							ListInfo[17] = "";
							ListInfo[18] = "";
							ListInfo[27] = "";
							ListInfo[26] = "";
							ListInfo[16] = "";
							// ListInfo[] = "";

						}
						StringBuffer tPremSQL = new StringBuffer(); // 经过保费按照生效起止日期提取
						tPremSQL.append(" select sum(nvl(a.prem,0)),")
								.append(" (select codename from ldcode where code = char(a.payintv) and codetype = 'payintv'),")
								.append(" sum(nvl((case when a.payintv <= 0 then (case when a.NI = 'NI' then")
								.append(" (case when days(a.grpenddate) - days(a.grpcvalidate) = 0 then 0 else ")
								.append(" a.prem / (days(a.grpenddate) - days(a.grpcvalidate)) end) else ")
								.append(" (case when days(a.enddate) - days(a.cvalidate) = 0")
								.append(" then 0 else")
								.append(" a.prem / (days(a.enddate) - days(a.cvalidate))")
								.append(" end) end) else a.prem * 12 / a.payintv / 365 end),0) * ")
								.append(" (case when a.cvalidate between '")
								.append(mEndCaseDateS)
								.append("' and '")
								.append(mEndCaseDateE)
								.append("' then case when a.enddate <='")
								.append(mEndCaseDateE)
								.append("' then days(a.enddate)-days(a.cvalidate)")
								.append("  else days('")
								.append(mEndCaseDateE)
								.append("')-days(a.cvalidate) + 1 end")
								.append(" when a.cvalidate <'")
								.append(mEndCaseDateS)
								.append("' then case when a.enddate between '")
								.append(mEndCaseDateS)
								.append("' and '")
								.append(mEndCaseDateE)
								.append("' then days(a.enddate)-days('")
								.append(mEndCaseDateS)
								.append("') when a.enddate >'")
								.append(mEndCaseDateE)
								.append("' then days('")
								.append(mEndCaseDateE)
								.append("')-days('")
								.append(mEndCaseDateS)
								.append("')+1 else 0 end else 0 end)),")
								.append(" case a.payintv when 0 then sum(a.prem) when -1 then sum(a.prem) else sum(a.prem)*12/a.payintv end")
								.append(" from lqgrppremdata a where grppolno='")
								.append(tGrpPolNo)
								.append("' group by a.payintv,a.grpcontno with ur");

						String tPremSQLS = tPremSQL.toString();
						System.out.println(tPremSQLS);
						SSRS tPremSSRS = tExeSQL.execSQL(tPremSQLS);

						if (tPremSSRS.getMaxRow() > 0) {
							// 期缴保费
							ListInfo[19] = mDF.format(Arith.round(Double
									.parseDouble(tPremSSRS.GetText(1, 1)), 2));
							tSumFPrem += Double.parseDouble(ListInfo[19]);
							mSumFPrem += Double.parseDouble(ListInfo[19]);

							// 缴费频次
							ListInfo[20] = tPremSSRS.GetText(1, 2);

							ListInfo[22] = mDF.format(Arith.round(Double
									.parseDouble(tPremSSRS.GetText(1, 4)), 2)); // 承保保费
							// 团险的长期险也按照一年来统计

							tSumSPrem += Double.parseDouble(ListInfo[22]);
							mSumSPrem += Double.parseDouble(ListInfo[22]);

							// 经过保费
							ListInfo[23] = mDF.format(Arith.round(Double
									.parseDouble(tPremSSRS.GetText(1, 3)), 2));
							tSumTPrem += Double.parseDouble(ListInfo[23]);
							mSumTPrem += Double.parseDouble(ListInfo[23]);

						} else {
							ListInfo[19] = mDF.format(Arith.round(0, 2));
							ListInfo[20] = "";
							ListInfo[22] = mDF.format(Arith.round(0, 2));
							ListInfo[23] = mDF.format(Arith.round(0, 2));
						}

						StringBuffer tInsuredSQL = new StringBuffer();
						tInsuredSQL
								.append(" select ")
								.append(
										" coalesce(sum(case when X.insuredstat='1' then 1 else 0 end),0),")
								.append(
										" coalesce(sum(case when X.insuredstat in ('2','3') then 1 else 0 end),0) ")
								.append(" from ( ")
								.append(" select ")
								.append(
										" distinct a.customerno,c.insuredstat insuredstat ")
								.append(
										" from llcase a,llclaimdetail b,lcinsured c ")
								.append(
										" where a.caseno=b.caseno and a.customerno=c.insuredno ")
								.append(
										" and b.contno=c.contno and b.grppolno='")
								.append(tGrpPolNo)
								.append(
										"' and a.rgtstate in ('09', '11', '12') ")
								.append(" and a.endcasedate between '")
								.append(mEndCaseDateS)
								.append("' and '")
								.append(mEndCaseDateE)
								.append("' ")
								.append(" union all ")
								.append(" select ")
								.append(
										" distinct a.customerno,c.insuredstat insuredstat ")
								.append(
										" from llcase a,llclaimdetail b,lbinsured c ")
								.append(
										" where a.caseno=b.caseno and a.customerno=c.insuredno ")
								.append(
										" and b.contno=c.contno and b.grppolno='")
								.append(tGrpPolNo)
								.append(
										"' and a.rgtstate in ('09', '11', '12') ")
								.append(" and a.endcasedate between '").append(
										mEndCaseDateS).append("' and '")
								.append(mEndCaseDateE).append(
										"' ) AS X with ur");
						String tInsuredSQLS = tInsuredSQL.toString();
						System.out.println(tInsuredSQLS);

						SSRS tInsuredSSRS = tExeSQL.execSQL(tInsuredSQLS);

						if (tInsuredSSRS.getMaxRow() > 0) {
							// 理赔在职人数
							ListInfo[32] = tInsuredSSRS.GetText(1, 1);
							tClaimOnWork += Integer.parseInt(ListInfo[32]);
							mClaimOnWork += Integer.parseInt(ListInfo[32]);
							// 理赔退休人数
							ListInfo[30] = tInsuredSSRS.GetText(1, 2);
							tClaimOffWork += Integer.parseInt(ListInfo[30]);
							mClaimOffWork += Integer.parseInt(ListInfo[30]);
						} else {
							ListInfo[32] = "";
							ListInfo[30] = "";
						}

						StringBuffer tInsuredTimeSQL = new StringBuffer();
						tInsuredTimeSQL
								.append(" select ")
								.append(
										" coalesce(sum(case when X.insuredstat='1' then 1 else 0 end),0),")
								.append(
										" coalesce(sum(case when X.insuredstat in ('2','3') then 1 else 0 end),0), ")
								.append(
										" coalesce(sum(case when X.insuredstat='1' then X.realpay else 0 end),0),")
								.append(
										" coalesce(sum(case when X.insuredstat in ('2','3') then X.realpay else 0 end),0) ")
								.append(" from ( ")
								.append(" select ")
								.append(" a.caseno,c.insuredstat insuredstat, ")
								.append(" coalesce(sum(b.realpay),0) realpay ")
								.append(
										" from llcase a,llclaimdetail b,lcinsured c ")
								.append(
										" where a.caseno=b.caseno and a.customerno=c.insuredno ")
								.append(
										" and b.contno=c.contno and b.grppolno='")
								.append(tGrpPolNo)
								.append(
										"' and a.rgtstate in ('09', '11', '12') ")
								.append(" and a.endcasedate between '")
								.append(mEndCaseDateS)
								.append("' and '")
								.append(mEndCaseDateE)
								.append("' group by a.caseno,c.insuredstat")
								.append(" union all ")
								.append(" select ")
								.append(" a.caseno,c.insuredstat insuredstat, ")
								.append(" coalesce(sum(b.realpay),0) realpay ")
								.append(
										" from llcase a,llclaimdetail b,lbinsured c ")
								.append(
										" where a.caseno=b.caseno and a.customerno=c.insuredno ")
								.append(
										" and b.contno=c.contno and b.grppolno='")
								.append(tGrpPolNo)
								.append(
										"' and a.rgtstate in ('09', '11', '12') ")
								.append(" and a.endcasedate between '")
								.append(mEndCaseDateS)
								.append("' and '")
								.append(mEndCaseDateE)
								.append(
										"' group by a.caseno,c.insuredstat ) AS X with ur");
						String tInsuredTimeSQLS = tInsuredTimeSQL.toString();
						System.out.println(tInsuredTimeSQLS);

						SSRS tInsuredTimeSSRS = tExeSQL
								.execSQL(tInsuredTimeSQLS);

						if (tInsuredTimeSSRS.getMaxRow() > 0) {
							// 理赔退休人次
							ListInfo[31] = tInsuredTimeSSRS.GetText(1, 2);
							tClaimOffTime += Integer.parseInt(ListInfo[31]);
							mClaimOffTime += Integer.parseInt(ListInfo[31]);
							// 理赔在职人次
							ListInfo[33] = tInsuredTimeSSRS.GetText(1, 1);
							tClaimOnTime += Integer.parseInt(ListInfo[33]);
							mClaimOnTime += Integer.parseInt(ListInfo[33]);
							// 退休人员赔款
							ListInfo[36] = tInsuredTimeSSRS.GetText(1, 4);
							tOffRealpay += Double.parseDouble(ListInfo[36]);
							mOffRealpay += Double.parseDouble(ListInfo[36]);
							// 在职人员赔款
							ListInfo[37] = tInsuredTimeSSRS.GetText(1, 3);
							tOnRealpay += Double.parseDouble(ListInfo[37]);
							mOnRealpay += Double.parseDouble(ListInfo[37]);
						} else {
							ListInfo[31] = "";
							ListInfo[33] = "";
							ListInfo[36] = "";
							ListInfo[37] = "";
						}

						tContent[i - 1] = ListInfo;
					}
					mCSVFileWrite.addContent(tContent);
					if (!mCSVFileWrite.writeFile()) {
						mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
						return false;
					}
				}
			} while (tSSRS != null && tSSRS.MaxRow > 0);
		} catch (Exception ex) {
			ex.printStackTrace();
			mCSVFileWrite.closeFile();
			return false;
		} finally {
			rsWrapper.close();
		}

		// 合计
		if (tHaveFlag) {
			String SumInfo[] = new String[40];
			SumInfo[0] = mManageCom;
			SumInfo[1] = mManageComName;
			SumInfo[2] = "合计";
			SumInfo[3] = "";
			SumInfo[4] = "";
			SumInfo[5] = "";
			SumInfo[6] = "";
			// if (!"".equals(mGrpType)) {
			// SumInfo[7] = mGrpTypeName;
			// } else {
			// SumInfo[7] = "";
			// }
			SumInfo[7] = "";
			SumInfo[8] = "";
			SumInfo[9] = "";
			SumInfo[10] = "";
			SumInfo[11] = "";
			SumInfo[12] = "";
			SumInfo[13] = "";
			SumInfo[14] = "";
			SumInfo[15] = "";
			SumInfo[16] = "";
			SumInfo[17] = "";
			SumInfo[18] = "";
			// 期缴保费
			SumInfo[19] = mDF.format(Arith.round(tSumFPrem, 2));
			SumInfo[20] = "";
			SumInfo[21] = "";
			// 承保保费
			SumInfo[22] = mDF.format(Arith.round(tSumSPrem, 2));
			// 经过保费
			SumInfo[23] = mDF.format(Arith.round(tSumTPrem, 2));
			// 实收保费
			SumInfo[24] = mDF.format(Arith.round(tSumActu, 2));
			// 被保险人人数
			SumInfo[25] = mDF.format(tSumPeoples);
			// 承保退休人数
			SumInfo[26] = mDF.format(tOffWork);
			// 承保在职人数
			SumInfo[27] = mDF.format(tOnWork);
			// 赔付件数
			SumInfo[28] = mDF.format(tSumCase);
			// 理赔结案人数
			SumInfo[29] = mDF.format(tSumECase);
			// 理赔退休人数
			SumInfo[30] = mDF.format(tClaimOffWork);
			// 理赔退休人次
			SumInfo[31] = mDF.format(tClaimOffTime);
			// 理赔在职人数
			SumInfo[32] = mDF.format(tClaimOnWork);
			// 理赔在职人次
			SumInfo[33] = mDF.format(tClaimOnTime);
			// 结案赔款
			SumInfo[34] = mDF.format(Arith.round(tSumERealPay, 2));
			//共保反冲赔款
			SumInfo[35] = mDF.format(tClaimmoney);
			// 退休人员赔款
			SumInfo[36] = mDF.format(tOffRealpay);
			// 在职人员赔款
			SumInfo[37] = mDF.format(tOnRealpay);
			// 财务实付赔款
			SumInfo[38] = mDF.format(Arith.round(tSumARealPay, 2));
			SumInfo[39] = "";
			
			String tContent[][] = new String[1][];
			tContent[0] = SumInfo;
			mCSVFileWrite.addContent(tContent);
			if (!mCSVFileWrite.writeFile()) {
				mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
				return false;
			}
		}
		return true;
	}

	/**
	 * 统计全部重点业务
	 * 
	 * @return boolean
	 */
	private boolean getSumDataList() {
		String tGrpTypeSQL = "select code,codename from ldcode where codetype='llgrptype' order by code with ur";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tGrpTypeSSRS = tExeSQL.execSQL(tGrpTypeSQL);
		for (int i = 1; i <= tGrpTypeSSRS.getMaxRow(); i++) {
			getDataList(tGrpTypeSSRS.GetText(i, 1));
		}

		// 合计
		String SumInfo[] = new String[39];
		SumInfo[0] = mManageCom;
		SumInfo[1] = mManageComName;
		SumInfo[2] = "全部重点业务合计";
		SumInfo[3] = "";
		SumInfo[4] = "";
		SumInfo[5] = "";
		SumInfo[6] = "";
		SumInfo[7] = "";
		SumInfo[8] = "";
		SumInfo[9] = "";
		SumInfo[10] = "";
		SumInfo[11] = "";
		SumInfo[12] = mDF.format(Arith.round(mSumFPrem, 2));
		SumInfo[13] = "";
		SumInfo[14] = mDF.format(Arith.round(mSumSPrem, 2));
		SumInfo[15] = mDF.format(Arith.round(mSumTPrem, 2));
		SumInfo[16] = mDF.format(Arith.round(mSumActu, 2));
		SumInfo[17] = mDF.format(mSumPeoples);
		SumInfo[18] = mDF.format(mSumCase);
		SumInfo[19] = mDF.format(mSumECase);
		SumInfo[20] = mDF.format(Arith.round(mSumERealPay, 2));
		SumInfo[21] = mDF.format(Arith.round(mSumARealPay, 2));
		SumInfo[22] = "";
		SumInfo[23] = "";
		SumInfo[24] = "";
		SumInfo[25] = "";
		SumInfo[26] = mDF.format(mOnWork);
		SumInfo[27] = mDF.format(mOffWork);
		SumInfo[28] = mDF.format(mClaimOnWork);
		SumInfo[29] = mDF.format(mClaimOffWork);
		SumInfo[30] = "";
		SumInfo[31] = "";
		SumInfo[32] = "";
		SumInfo[33] = "";
		SumInfo[34] = "";
		SumInfo[35] = mDF.format(mClaimOffTime);
		SumInfo[36] = mDF.format(mClaimOnTime);
		SumInfo[37] = mDF.format(mOffRealpay);
		SumInfo[38] = mDF.format(mOnRealpay);
		mListTable.add(SumInfo);

		return true;
	}

	/**
     * 社保业务清分
     * @param statsType
     * @return
     */
    private String getStatsType(String statsType) {
    	String tPartSql = "";
        if("1".equals(statsType)){
          tPartSql = " and CHECKGRPCONT(a.grpcontno) = 'N' ";//商业保险
          System.out.println("tPartSql:"+tPartSql);
        }else if("2".equals(statsType)){
          tPartSql = " and CHECKGRPCONT(a.grpcontno) = 'Y' ";//社会保险
          System.out.println("tPartSql:"+tPartSql);
        }else{
          tPartSql = "";//全部
          System.out.println("tPartSql:"+tPartSql);
        }
		return tPartSql;
	}
	
	/**
	 * 获取保单号查询条件
	 * 
	 * @param aGrpContNo
	 *            String
	 * @return String
	 */
	private String getContNo(String aGrpContNo) {
		if (!"".equals(aGrpContNo)) {
			String ContNo = " and a.grpcontno = '" + aGrpContNo + "'";
			return ContNo;
		} else {
			return "";
		}
	}

	/**
	 * 获取单位名称查询条件
	 * 
	 * @param aGrpName
	 *            String
	 * @return String
	 */
	private String getGrpName(String aGrpName) {
		if (!"".equals(aGrpName)) {
			String ContName = " and a.grpname = '" + aGrpName + "'";
			return ContName;
		} else {
			return "";
		}
	}

	/**
	 * 获取承保人数查询条件
	 * 
	 * @param aPeoples
	 *            String
	 * @return String
	 */
	private String getPeoples(String aPeoples) {
		if (!"".equals(aPeoples)) {
			String Person = " and a.peoples2 >= " + aPeoples;
			return Person;
		} else {
			return "";
		}
	}

	/**
	 * 获取实收保费查询条件
	 * 
	 * @param aPrem
	 *            String
	 * @return String
	 */
	private String getActu(String aPrem) {
		if (!"".equals(aPrem)) {
			String prem = " and exists (select grppolno from lqclaimactug where grppolno=a.grppolno "
					+ "group by grppolno  having sum(sumactupaymoney) >= "
					+ aPrem + " ) ";
			return prem;
		} else {
			return "";
		}
	}

	/**
	 * 获取重点业务查询条件
	 * 
	 * @param aGrpType
	 *            String
	 * @return String
	 */
	private String getGrpType(String aGrpType) {
		String prem = "";
		if (!"".equals(aGrpType)) {
			if ("0".equals(aGrpType))// 为全部时
				prem = " and exists (select 1 from ldcode1 where codetype='llgrptype' and code1=a.grpcontno) ";

			else
				// 不为全部时
				prem = " and exists (select 1 from ldcode1 where codetype='llgrptype' and code='"
						+ aGrpType + "' and code1=a.grpcontno) ";
			return prem;
		} else {
			return "";
		}
	}

	/**
	 * 获取险种查询条件
	 * 
	 * @param aGrpType
	 *            String
	 * @return String
	 */
	private String getRiskCode(String aRiskCode) {
		if (!"".equals(aRiskCode)) {
			String ContNo = " and a.riskcode = '" + aRiskCode + "'";
			return ContNo;
		} else {
			return "";
		}
	}

	/**
	 * 进行数据查询
	 * 
	 * @return boolean
	 */
	private boolean queryData() {

		LDSysVarDB tLDSysVarDB = new LDSysVarDB();
		tLDSysVarDB.setSysVar("LPCSVREPORT");

		if (!tLDSysVarDB.getInfo()) {
			buildError("queryData", "查询文件路径失败");
			return false;
		}

		mFilePath = tLDSysVarDB.getSysVarValue();

		//mFilePath = "F:\\picch\\ui\\vtsfile\\";

		if (mFilePath == null || "".equals(mFilePath)) {
			buildError("queryData", "查询文件路径失败");
			return false;
		}

		System.out.println(mFilePath);

		String tTime = PubFun.getCurrentTime3().replaceAll(":", "");
		String tDate = PubFun.getCurrentDate2();

		mFileName = "CBPFHZB" + mGlobalInput.Operator + tDate + tTime;

		//当前日期
		String[] tCurrentDate = PubFun.getCurrentDate().split("-");
		//当前日期  年
		String tYear = tCurrentDate[0];
		//当前日期  月
		String tMonth = tCurrentDate[0]+tCurrentDate[1];
		//当前日期   日
		String tDay = tCurrentDate[0]+tCurrentDate[1]+tCurrentDate[2];
		
		//年文件
		File yFile = new File(mFilePath+tYear);
		//月文件
		File mFile = new File(mFilePath+tYear+"/"+tMonth);
		//日文件
		File dFile = new File(mFilePath+tYear+"/"+tMonth+"/"+tDay); 

		if(!yFile.exists()||!mFile.exists()||!dFile.exists())
		{
			dFile.mkdirs();
		}
		//最终文件存放路径
		mFilePath = mFilePath+tYear+"/"+tMonth+"/"+tDay+"/";
		System.out.println(mFileName);

		mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
		
		String tName = "";
		if("1".equals(mStatsType)){
			tName = "商业保险";
		}else if("2".equals(mStatsType)){
			tName = "社会保险";
		}else{
			tName = "全部";
		}
		
		String[][] tTitle = new String[3][];
		tTitle[0] = new String[] { "承保赔付汇总表" };
		tTitle[1] = new String[] { "统计部门：" + mManageComName,"业务类型："+ tName,
				"生效日期：" + mCvaliDateS + "至" + mCvaliDateE,
				"结案日期：" + mEndCaseDateS + "至" + mEndCaseDateE,
				"制表人：" + mOperator, "制表日期：" + mCurrentDate };
		tTitle[2] = new String[] { "机构编码", "机构名称", "投保人", "团体客户号", "保单号",
				"中介机构编码", "中介机构名称", "销售渠道", "所属营业部代码", "所属营业部名称", "业务员编码",
				"业务员姓名", "市场类型", "项目属性", "险种代码", "险种名称", "承保时间", "生效时间",
				"失效时间", "首期保费", "缴费频次", "保费交至日期", "承保保费", "经过保费", "实收保费",
				"被保险人数", "承保退休人数", "承保在职人数", "赔付件数", "理赔结案人数", "理赔退休人数",
				"理赔退休人次", "理赔在职人数", "理赔在职人次", "结案赔款", "共保反冲赔款","退休人员赔款", "在职人员赔款",
				"财务实付赔款","结案案件最晚事故日期"  };
		String[] tContentType = {"String","String","String","String","String",
				"String","String","String","String","String","String",
				"String","String","String","String","String","String","String",
				"String","Number","String","String","Number","Number","Number",
				"Number","Number","Number","Number","Number","Number",
				"Number","Number","Number","Number","Number","Number","Number",
				"Number","String"};
		

		if (!mCSVFileWrite.addTitle(tTitle,tContentType)) {
			return false;
		}

		if (!getDataList(mGrpType)) {
			return false;
		}

		mCSVFileWrite.closeFile();

		return true;
	}

	/**
	 * 取得传入的数据
	 * 
	 * @return boolean
	 */
	private boolean getInputData(VData mInputData) {
		// 全局变量
		mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mOperator = mGlobalInput.Operator;

		mTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		mManageCom = (String) mTransferData.getValueByName("MngCom"); // 统计机构
		mCvaliDateS = (String) mTransferData.getValueByName("CvaliDateS"); // 生效起期
		mCvaliDateE = (String) mTransferData.getValueByName("CvaliDateE"); // 生效止期
		mEndCaseDateS = (String) mTransferData.getValueByName("EndCaseDateS");// 结案起期
		mEndCaseDateE = (String) mTransferData.getValueByName("EndCaseDateE");// 结案止期
		mGrpContNo = (String) mTransferData.getValueByName("ContNo"); // 保单号
		mGrpName = StrTool.unicodeToGBK((String) mTransferData
				.getValueByName("GrpName"));// 单位名称
		mManageComName = StrTool.unicodeToGBK((String) mTransferData
				.getValueByName("ManageName"));// 机构名称
		mPeoples = (String) mTransferData.getValueByName("Person"); // 参保人数
		mPrem = (String) mTransferData.getValueByName("Prem"); // 保费规模
		mGrpType = (String) mTransferData.getValueByName("GrpTypeS"); // 重点业务
		mRiskCode = (String) mTransferData.getValueByName("RiskCode"); // 重点业务
		mStatsType = (String) mTransferData.getValueByName("StatsType");	//社保业务清分
		return true;
	}

	/**
	 * 追加错误信息
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLClaimCollectionBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	/**
	 * 取得返回处理过的结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		return this.mResult;
	}

	public String getFileName() {
		return mFileName;
	}

	public static void main(String[] args) {

		GlobalInput tG = new GlobalInput();
		tG.Operator = "lptest";
		tG.ManageCom = "86";

		TransferData mTransferData = new TransferData();

		mTransferData.setNameAndValue("MngCom", "86");
		mTransferData.setNameAndValue("ManageName", "总公司");
		mTransferData.setNameAndValue("CvaliDateS", "2011-1-1");
		mTransferData.setNameAndValue("CvaliDateE", "2011-3-31");
		mTransferData.setNameAndValue("EndCaseDateS", "2011-1-1");
		mTransferData.setNameAndValue("EndCaseDateE", "2011-3-31");
		mTransferData.setNameAndValue("SaleChnl", "");
		mTransferData.setNameAndValue("Sort", "");
		mTransferData.setNameAndValue("GrpTypeS", "");
		mTransferData.setNameAndValue("RiskCode", "");

		mTransferData.setNameAndValue("ClaimRatioStd", "");
		mTransferData.setNameAndValue("ContNo", "");
		mTransferData.setNameAndValue("GrpName", "");
		mTransferData.setNameAndValue("Person", "");
		mTransferData.setNameAndValue("Prem", "");

		VData tVData = new VData();
		tVData.addElement(tG);
		tVData.addElement(mTransferData);
		LLClaimCollectionCSVBL tLLClaimCollectionBL = new LLClaimCollectionCSVBL();
		tLLClaimCollectionBL.submitData(tVData, "");

	}
}
