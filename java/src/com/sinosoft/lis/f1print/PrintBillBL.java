package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author 刘岩松
 * function :
 * @version 1.0
 * @date 2003-04-04
 */

import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LYReturnFromBankBDB;
import com.sinosoft.lis.db.LYReturnFromBankDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDBankSchema;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LYReturnFromBankBSchema;
import com.sinosoft.lis.schema.LYReturnFromBankSchema;
import com.sinosoft.lis.vschema.LDBankSet;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LYReturnFromBankBSet;
import com.sinosoft.lis.vschema.LYReturnFromBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PrintBillBL
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String strBillNo;
    private String strBankCode;
    private String strMngCom;

    private String mBankName; //银行名称
    private String mErrorReason; //失败原因
    private String mChkSuccFlag; //银行校验成功标志；
    private String mChkFailFlag; //银行校验失败标志；
    public PrintBillBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
        return true;
    }


    private boolean getInputData(VData cInputData)
    {
        System.out.println("getInputData begin");
        strBillNo = (String) cInputData.get(0);
        strBankCode = (String) cInputData.get(1);
        strMngCom = (String) cInputData.get(2);
        System.out.println("在PrintBillBL.java中得到的批次号码是" + strBillNo);
        System.out.println("在PrintBillBL.java中得到的银行代码是" + strBankCode);
        System.out.println("在PrintBillBL.java中得到的管理机构是" + strMngCom);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PayNoticeF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {

        System.out.println("查询银行名称的函数！！！");
        LDBankDB tLDBankDB = new LDBankDB();
        tLDBankDB.setBankCode(strBankCode);
        LDBankSet tLDBankSet = new LDBankSet();
        tLDBankSet.set(tLDBankDB.query());
        int bank_count = tLDBankSet.size();
        if (bank_count == 0)
        {
            this.mErrors.copyAllErrors(tLDBankDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "PrintBillBL";
            tError.functionName = "submitData";
            tError.errorMessage = "银行信息表查询失败，没有该银行代码！！！";
            this.mErrors.addOneError(tError);
            return false;
        }
        LDBankSchema tLDBankSchema = new LDBankSchema();
        tLDBankSchema.setSchema(tLDBankSet.get(1));
        mBankName = tLDBankSchema.getBankName();
        System.out.println("打印出银行信息和批次号码！！！");
        ListTable tlistTable = new ListTable();
        tlistTable.setName("MODE");

        System.out.println("对银行返回盘记录表的查询");

        String t_sql = "select * from LYReturnFromBank where SerialNo = '"
                       + strBillNo.trim()
                       + "' and BankCode = '"
                       + strBankCode.trim()
                       + "' and ComCode like '"
                       + strMngCom
                       + "%'";
        LYReturnFromBankDB tLYReturnFromBankDB = new LYReturnFromBankDB();
        System.out.println("返回盘中执行的sql语句是" + t_sql);
        LYReturnFromBankSet tLYReturnFromBankSet = new LYReturnFromBankSet();
        tLYReturnFromBankSet = (tLYReturnFromBankDB.executeQuery(t_sql));
        int n = tLYReturnFromBankSet.size();
        if (n == 0)
        {
            //定义变量记录总金额和总笔数
            double SumMoney_b = 0;
            int SumCount_b = 0;
            System.out.println("该次交易可能被核销，在银行返回盘记录备份表中进行查询");
            String y_sql = "select * from LYReturnFromBankB where SerialNo = '"
                           + strBillNo.trim()
                           + "' and BankCode = '"
                           + strBankCode.trim()
                           + "' and ComCode like '"
                           + strMngCom.trim()
                           + "%'";
            LYReturnFromBankBSet tLYReturnFromBankBSet = new
                    LYReturnFromBankBSet();
            LYReturnFromBankBDB tLYReturnFromBankBDB = new LYReturnFromBankBDB();
            tLYReturnFromBankBSet = (tLYReturnFromBankBDB.executeQuery(y_sql));
            System.out.println("备份表中所执行的语句是" + y_sql);
            int m = tLYReturnFromBankBSet.size();
            if (m == 0)
            {
                this.mErrors.copyAllErrors(tLYReturnFromBankBDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "PrintBillBL";
                tError.functionName = "submitData";
                tError.errorMessage = "没有满足条件的信息！！！";
                this.mErrors.addOneError(tError);
                return false;
            }

            int b_count;
            b_count = 1;
            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("PrintBill.vts", "printer");
            ListTable alistTable = new ListTable();
            alistTable.setName("INFO");

            for (int j = 1; j <= m; j++)
            {
                System.out.println("在银行返回盘记录备份表的处理中此时的J的数值是￥￥￥￥" + j);
                /******
                 * date:2003-05-09
                 * 修改了.vts模板，程序相应作修改
                 *
                 */
                String[] cols = new String[11];
                LYReturnFromBankBSchema tLYReturnFromBankBSchema = new
                        LYReturnFromBankBSchema();
                tLYReturnFromBankBSchema.setSchema(tLYReturnFromBankBSet.get(j));

                String tbPayCode = tLYReturnFromBankBSchema.getPayCode();
                String b_BankSuccFlag;
                if (tLYReturnFromBankBSchema.getBankSuccFlag() == null ||
                    tLYReturnFromBankBSchema.getBankSuccFlag().equals(""))
                {
                    b_BankSuccFlag = "1";
                }
                else
                {
                    b_BankSuccFlag = tLYReturnFromBankBSchema.getBankSuccFlag();
                }
                System.out.println("银行的成功标志是" + b_BankSuccFlag);
                System.out.println("2003-04-25");
                System.out.println("采用了胡博的方法");
                String hq_flag_b = getBankSuccFlag(strBankCode);
                System.out.println("获取的成功标志是" + hq_flag_b);
                boolean jy_flag_b = verifyBankSuccFlag(hq_flag_b,
                        b_BankSuccFlag);

                System.out.println("校验后的成功标志是" + jy_flag_b);
                if (jy_flag_b)
                {
                    continue;
                }
                b_count++;
                //2003-06-04
                SumCount_b++;
                LDCode1DB tLDCode1DB = new LDCode1DB();
                tLDCode1DB.setCodeType("bankerror");
                tLDCode1DB.setCode(strBankCode);
                tLDCode1DB.setCode1(b_BankSuccFlag);
                System.out.println("2003-05-21LYS错误的标志是" + b_BankSuccFlag);
                LDCode1Set tLDCode1Set = new LDCode1Set();
                LDCode1Schema tLDCode1Schema = new LDCode1Schema();
                tLDCode1Set.set(tLDCode1DB.query());
                int k = tLDCode1Set.size();
                String b_errorReason;
                if (k == 0)
                {
                    b_errorReason = "没有指明错误原因";
                }
                else
                {
                    tLDCode1Schema.setSchema(tLDCode1Set.get(1));
                    b_errorReason = tLDCode1Schema.getCodeName();
                }
                cols[3] = tLYReturnFromBankBSchema.getAccNo();
                cols[4] = tLYReturnFromBankBSchema.getAccName();
                cols[6] = b_errorReason;
                cols[9] = tLYReturnFromBankBSchema.getPolNo();
                cols[10] = String.valueOf(tLYReturnFromBankBSchema.getPayMoney());
                //2003-05-06
                SumMoney_b = SumMoney_b + tLYReturnFromBankBSchema.getPayMoney();

                System.out.println("失败原因是" + tLDCode1Schema.getCodeName());
                System.out.println("银行的账号是" + tLYReturnFromBankBSchema.getAccNo());
                System.out.println("银行的账号名是" +
                                   tLYReturnFromBankBSchema.getAccName());
                System.out.println("查询应收总表，查询出代理人信息");
                System.out.print("当J的值是" + j + "时，银行的账号是" + cols[3]);
                System.out.println("当J的值是" + j + "时，银行的账号名是" + cols[4]);
                System.out.println("当J的值是" + j + "时，错误原因是" + cols[6]);
                LJSPayDB tLJSPayDB = new LJSPayDB();
                tLJSPayDB.setGetNoticeNo(tbPayCode);
                tLJSPayDB.setBankCode(strBankCode);
                System.out.println("将要执行的是对代理人信息进行附值");
                LJSPaySet tLJSPaySet = new LJSPaySet();
                tLJSPaySet.set(tLJSPayDB.query());
                int a = tLJSPaySet.size();
                //对代理人编码、代理人姓名、代理人组别进行附值
                if (a != 0)
                {
                    LJSPaySchema tLJSPaySchema = new LJSPaySchema();
                    tLJSPaySchema.setSchema(tLJSPaySet.get(1));
                    cols[5] = String.valueOf(tLJSPaySchema.getSendBankCount());
                    System.out.println("总失败次数是" +
                                       tLJSPaySchema.getSendBankCount());
                }
                else
                {
                    cols[5] = "过期";
                }
                //根据代理人的编码查询出代理人的姓名
                ExeSQL exesql = new ExeSQL();
                String AgentName_sql =
                        "Select Name,AgentGroup from LAAgent Where AgentCode = '"
                        + tLYReturnFromBankBSchema.getAgentCode() + "'";
                SSRS AgentName_ssrs = exesql.execSQL(AgentName_sql);
                //查询出代理人组别的名称
                String AgentGroup_sql =
                        "Select Name from LABranchGroup Where AgentGroup = '"
                        + AgentName_ssrs.GetText(1, 2) + "'";
                System.out.println("2003-05-27");
                System.out.println("查询应收总表所得到的代理人的信息是" + AgentGroup_sql);
                SSRS AgentGroup_ssrs = exesql.execSQL(AgentGroup_sql);

                cols[0] = tLYReturnFromBankBSchema.getAgentCode();
                if (AgentName_ssrs.getMaxRow() == 0)
                {
                    cols[1] = "无";
                }
                else
                {
                    cols[1] = AgentName_ssrs.GetText(1, 1);
                    System.out.println("代理人的姓名是" + AgentName_ssrs.GetText(1, 1));
                }
                if (AgentGroup_ssrs.getMaxRow() == 0)
                {
                    cols[2] = "无";
                }
                else
                {
                    cols[2] = AgentGroup_ssrs.GetText(1, 1);
                    System.out.println("代理人组别是" + AgentGroup_ssrs.GetText(1, 1));
                }

                //根据LYReturnFromBankB表中的NoType的值来判断号码的类型根据不同的类型来执行不同的sql语句
                String AppntNo_sql = "";
                String t_NoType = tLYReturnFromBankBSchema.getNoType();
                if (!(t_NoType.equals("") || t_NoType == null))
                {
                    if (t_NoType.equals("1"))
                    {
                        System.out.println("NoType==1时代表集体保单号码");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where GrpPolNo = '"
                                + tLYReturnFromBankBSchema.getPolNo() + "'";
                    }
                    if (t_NoType.equals("2"))
                    {
                        System.out.println("NoType==2时代表的是个人保单号");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where PolNo = '"
                                + tLYReturnFromBankBSchema.getPolNo() + "'";
                    }
                    if (t_NoType.equals("4"))
                    {
                        System.out.println("NoType==4时代表的是合同投保单号");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where ContNo = '"
                                + tLYReturnFromBankBSchema.getPolNo() + "'";

                    }
                    if (t_NoType.equals("6"))
                    {
                        System.out.println("NoType==6时代表的是个人投保单号");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where ProposalNo = '"
                                + tLYReturnFromBankBSchema.getPolNo() + "'";
                    }
                    if (t_NoType.equals("9"))
                    {
                        System.out.println("NoType==9时代表的是表示个人印刷号");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where PrtNo = '"
                                + tLYReturnFromBankBSchema.getPolNo() + "'";
                    }

                }
                SSRS AppntNo_ssrs = exesql.execSQL(AppntNo_sql);
                if (AppntNo_ssrs.getMaxRow() != 0)
                {
                    System.out.println("备份表中的投保人客户号是" +
                                       AppntNo_ssrs.GetText(1, 1));
                    //查询出投保人的名称和联系方式
                    String AppntName_sql =
                            "Select Name,Mobile from LCAppntInd Where CustomerNo = '"
                            + AppntNo_ssrs.GetText(1, 1) + "'";
                    SSRS AppntName_ssrs = exesql.execSQL(AppntName_sql);
                    if (AppntName_ssrs.getMaxRow() == 0)
                    {
                        cols[7] = "无";
                    }
                    else
                    {
                        cols[7] = AppntName_ssrs.GetText(1, 1);
                        System.out.println("投保人的信息如下：投保人的姓名是" +
                                           AppntName_ssrs.GetText(1, 1));
                    }
                    if (AppntName_ssrs.getMaxRow() == 0)
                    {
                        cols[8] = "无";

                    }
                    else
                    {
                        cols[8] = AppntName_ssrs.GetText(1, 2);
                        System.out.println("投保人的名称是" +
                                           AppntName_ssrs.GetText(1, 2));
                    }
                }
                alistTable.add(cols);
            }
            String[] col = new String[11];
            xmlexport.addDisplayControl("displayinfo");
            xmlexport.addListTable(alistTable, col);
            texttag.add("BillNo", strBillNo);
            texttag.add("BankCode", strBankCode);
            texttag.add("BankName", mBankName);
            texttag.add("SumMoney", SumMoney_b);
            texttag.add("SumCount", SumCount_b);
            texttag.add("Date", PubFun.getCurrentDate());
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }

            //xmlexport.outputDocumentToFile("e:\\","printbill");//输出xml文档到文件
            mResult.clear();
            mResult.addElement(xmlexport);
            System.out.println("￥￥￥￥￥￥b_count的值是====" + b_count);
            if (b_count == 1)
            {
                this.mErrors.copyAllErrors(tLDBankDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "PrintBillBL";
                tError.functionName = "submitData";
                tError.errorMessage = "没有失败的信息！！！不能进行打印！！！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        else
        {
            int _count;
            _count = 1;
            double SumMoney = 0;
            int SumCount = 0;
            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("PrintBill.vts", "printer");
            ListTable blistTable = new ListTable();
            blistTable.setName("INFO");
            for (int i = 1; i <= n; i++)
            {
                LYReturnFromBankSchema tLYReturnFromBankSchema = new
                        LYReturnFromBankSchema();
                tLYReturnFromBankSchema.setSchema(tLYReturnFromBankSet.get(i));
                //记录账号名和账户
                String[] cols = new String[11];
                String mBankSuccFlag;
                String _BankSuccFlag;
                if (tLYReturnFromBankSchema.getBankSuccFlag() == null ||
                    tLYReturnFromBankSchema.getBankSuccFlag().equals(""))
                {
                    _BankSuccFlag = "1";
                }
                else
                {
                    _BankSuccFlag = tLYReturnFromBankSchema.getBankSuccFlag();
                }
                System.out.println("银行的成功标志是" + _BankSuccFlag);
                System.out.println("采用了胡博的方法后回盘表的数据");
                System.out.println("银行的成功标志是" + _BankSuccFlag);
                String hq_flag = getBankSuccFlag(strBankCode);
                System.out.println("获取的成功标志是" + hq_flag);
                boolean jy_flag = verifyBankSuccFlag(hq_flag, _BankSuccFlag);
                System.out.println("银行的校验标志是" + jy_flag);
                System.out.println("2003-04-25");
                if (jy_flag)
                {
                    continue;
                }
                _count++;
                SumCount++;
                String tPayCode = tLYReturnFromBankSchema.getPayCode();
                mBankSuccFlag = tLYReturnFromBankSchema.getBankSuccFlag();
                LDCode1DB mLDCode1DB = new LDCode1DB();
                mLDCode1DB.setCodeType("bankerror");
                mLDCode1DB.setCode(strBankCode);
                mLDCode1DB.setCode1(mBankSuccFlag);
                LDCode1Set mLDCode1Set = new LDCode1Set();
                LDCode1Schema mLDCode1Schema = new LDCode1Schema();
                mLDCode1Set.set(mLDCode1DB.query());
                int c = mLDCode1Set.size();
                String errorReason;
                if (c == 0)
                {
                    errorReason = "没有指明错误原因";
                }
                else
                {
                    mLDCode1Schema.setSchema(mLDCode1Set.get(1));
                    errorReason = mLDCode1Schema.getCodeName();
                }
                System.out.println("失败原因是" + errorReason);
                cols[6] = errorReason;

                cols[3] = tLYReturnFromBankSchema.getAccNo();
                cols[4] = tLYReturnFromBankSchema.getAccName();
                cols[9] = tLYReturnFromBankSchema.getPolNo();
                cols[10] = String.valueOf(tLYReturnFromBankSchema.getPayMoney());
                SumMoney = SumMoney + tLYReturnFromBankSchema.getPayMoney();
                System.out.println("银行的账号是" + tLYReturnFromBankSchema.getAccNo());
                System.out.println("银行的账户名称是：" +
                                   tLYReturnFromBankSchema.getAccName());
                LJSPayDB tLJSPayDB = new LJSPayDB();
                tLJSPayDB.setGetNoticeNo(tPayCode);
                tLJSPayDB.setBankCode(strBankCode);
                LJSPaySet tLJSPaySet = new LJSPaySet();
                System.out.println("将对代理人进行附值和错误次数进行处理;");
                tLJSPaySet.set(tLJSPayDB.query());
                int a = tLJSPaySet.size();
                if (a != 0)
                {
                    LJSPaySchema tLJSPaySchema = new LJSPaySchema();
                    tLJSPaySchema.setSchema(tLJSPaySet.get(1));
                    cols[5] = String.valueOf(tLJSPaySchema.getSendBankCount());
                    System.out.println("总失败次数是" +
                                       tLJSPaySchema.getSendBankCount());
                }
                else
                {
                    cols[5] = "过期";
                }

                //根据代理人的编码查询出代理人的姓名
                ExeSQL exesql = new ExeSQL();
                String AgentName_sql =
                        "Select Name,AgentGroup from LAAgent Where AgentCode = '"
                        + tLYReturnFromBankSchema.getAgentCode() + "'";
                SSRS AgentName_ssrs = exesql.execSQL(AgentName_sql);
                //查询出代理人组别的名称
                String AgentGroup_sql =
                        "Select Name from LABranchGroup Where AgentGroup = '"
                        + AgentName_ssrs.GetText(1, 2) + "'";
                System.out.println("2003-05-27");
                System.out.println("查询代理人信息的语句是" + AgentGroup_sql);
                SSRS AgentGroup_ssrs = exesql.execSQL(AgentGroup_sql);

                cols[0] = tLYReturnFromBankSchema.getAgentCode();
                if (AgentName_ssrs.getMaxRow() == 0)
                {
                    cols[1] = "无";
                }
                else
                {
                    cols[1] = AgentName_ssrs.GetText(1, 1);
                }
                if (AgentGroup_ssrs.getMaxRow() == 0)
                {
                    cols[2] = "无";
                }
                else
                {
                    cols[2] = AgentGroup_ssrs.GetText(1, 1);
                }
                //对投保人和投保人的联系方式进行附值
                //查询出投保人的客户号码
                //ExeSQL exesql = new ExeSQL();
                String AppntNo_sql = "";
                String t_NoType = tLYReturnFromBankSchema.getNoType();
                if (!(t_NoType.equals("") || t_NoType == null))
                {
                    if (t_NoType.equals("1"))
                    {
                        System.out.println("NoType==1时代表集体保单号码");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where GrpPolNo = '"
                                + tLYReturnFromBankSchema.getPolNo() + "'";
                    }
                    if (t_NoType.equals("2"))
                    {
                        System.out.println("NoType==2时代表的是个人保单号");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where PolNo = '"
                                + tLYReturnFromBankSchema.getPolNo() + "'";
                    }
                    //          if(t_NoType.equals("3"))
                    //          {
                    //            System.out.println("NoType==3时代表的是个人批改号");
                    //          }
                    if (t_NoType.equals("4"))
                    {
                        System.out.println("NoType==4时代表的是合同投保单号");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where ContNo = '"
                                + tLYReturnFromBankSchema.getPolNo() + "'";

                    }
                    //          if(t_NoType.equals("5"))
                    //          {
                    //            System.out.println("NoType==5时代表的是集体投保单号");
                    //
                    //          }
                    if (t_NoType.equals("6"))
                    {
                        System.out.println("NoType==6时代表的是个人投保单号");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where ProposalNo = '"
                                + tLYReturnFromBankSchema.getPolNo() + "'";
                    }
                    //        if(t_NoType.equals("7"))
                    //        {
                    //          System.out.println("NoType==7时代表的是表示合同印刷号");
                    //        }
                    //        if(t_NoType.equals("8"))
                    //        {
                    //          System.out.println("NoType==8时代表的是表示集体印刷号");
                    //        }
                    if (t_NoType.equals("9"))
                    {
                        System.out.println("NoType==9时代表的是表示个人印刷号");
                        AppntNo_sql =
                                "Select AppntNo From LCPol Where PrtNo = '"
                                + tLYReturnFromBankSchema.getPolNo() + "'";
                    }

//          else
//          {
//
//          }
                }
                SSRS AppntNo_ssrs = exesql.execSQL(AppntNo_sql);
                System.out.println("备份表中的投保人客户号是" + AppntNo_ssrs.GetText(1, 1));
                //查询出投保人的名称和联系方式
                String AppntName_sql =
                        "Select Name,Mobile from LCAppntInd Where CustomerNo = '"
                        + AppntNo_ssrs.GetText(1, 1) + "'";
                SSRS AppntName_ssrs = exesql.execSQL(AppntName_sql);
                if (AppntName_ssrs.getMaxRow() == 0)
                {
                    cols[7] = "无";
                }
                else
                {
                    cols[7] = AppntName_ssrs.GetText(1, 1);
                }
                if (AppntName_ssrs.getMaxRow() == 0)
                {
                    cols[8] = AppntName_ssrs.GetText(1, 2);
                }

                blistTable.add(cols);
                System.out.println("getPrintData end");
            }
            String[] b_col = new String[11];
            xmlexport.addDisplayControl("displayinfo");
            xmlexport.addListTable(blistTable, b_col);
            texttag.add("BillNo", strBillNo);
            texttag.add("BankCode", strBankCode);
            texttag.add("BankName", mBankName);
            texttag.add("Date", PubFun.getCurrentDate());
            texttag.add("SumMoney", SumMoney);
            texttag.add("SumCount", SumCount);
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            // xmlexport.outputDocumentToFile("e:\\","printbill");//输出xml文档到文件
            mResult.clear();
            mResult.addElement(xmlexport);
            System.out.println("对银行信息表的操作，根据银行代码查找出银行名称");
            System.out.println("对“应收总表”进行查询，查询出代理人的信息");
            System.out.println("getPrintData begin");
        }
        return true;
    }

    //获取银行的成功标志
    public String getBankSuccFlag(String tBankCode)
    {
        System.out.println("银行代码是" + tBankCode);
        try
        {
            LDBankSchema tLDBankSchema = new LDBankSchema();

            tLDBankSchema.setBankCode(tBankCode);
            tLDBankSchema.setSchema(tLDBankSchema.getDB().query().get(1));

            return tLDBankSchema.getAgentPaySuccFlag();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new NullPointerException("获取银行扣款成功标志信息失败！(getBankSuccFlag) " +
                                           e.getMessage());
        }
    }

//校验成功标志的方法
    public boolean verifyBankSuccFlag(String bankSuccFlag, String bBankSuccFlag)
    {
        int i;
        boolean passFlag = false;

        String[] arrSucc = PubFun.split(bankSuccFlag, ";");
        String tSucc = bBankSuccFlag;

        for (i = 0; i < arrSucc.length; i++)
        {
            if (arrSucc[i].equals(tSucc))
            {
                passFlag = true;
                break;
            }
        }

        return passFlag;
    }


    public static void main(String[] args)
    {
        String tBillNo = "00000000000000000002";
        String tBankCode = "0701";
        String tMngCom = "86";
        VData tVData = new VData();
        tVData.addElement(tBillNo);
        tVData.addElement(tBankCode);
        tVData.addElement(tMngCom);
        PrintBillUI tPrintBillUI = new PrintBillUI();
        tPrintBillUI.submitData(tVData, "PRINT");
    }
}