package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

public class LLEndCaseStatisticsBL
{
	 public LLEndCaseStatisticsBL() 
{
	        try {
	            jbInit();
	        } 
	        catch (Exception ex) 
	        {
	            ex.printStackTrace();
	        } 
}

public CErrors mErrors = new CErrors();
private VData mResult = new VData();
private GlobalInput mGlobalInput = new GlobalInput();
private String mECaseDateE="";//结案起期
private String mECaseDateS="";//结案止期
private String mRgtDateS="";//受理起期
private String mRgtDateE="";//受理止期
private String mAccDateS="";//事件起期
private String mAccDateE="";//事件止期
private String mOperator ="";
private String mManageCom="";
private String mDateType="";
private String mComName="";
private DecimalFormat mDF = new DecimalFormat("0.00");
private XmlExport mXmlExport = null;
private ListTable mListTable = new ListTable();
private String    mCurrentDate = PubFun.getCurrentDate();
private String    mFileNameB="";
private TransferData mTransferData = new TransferData();


//传输数据公共方法
 public boolean submitData(VData cInputData, String cOperate) 
{
    // 得到外部传入的数据，将数据备份到本类中
    if (!getInputData(cInputData)) 
    {
        return false;
    }

// 进行数据查询
if (!queryData())
{
    return false;
}
        return true;
}
 
/*
 * 统计案件结果
 */
private boolean getDataList(int datetype)
{

  if (mManageCom == null || mManageCom.equals("")) {
	  buildError("getDataList", "机构编码缺失！");
	  return false;
  } 
  try{
	 ExeSQL ttExeSQL = new ExeSQL();
    


    ////////////////////////////////////////以年显示///////////////////////////////////////////////////////////
    if (datetype==1)
    { /*
    	 String YSQL=" select rgtMess.endyear,coalesce(rgtMess.count,0),coalesce(rgtMess.sum,0), coalesce(accMess.count,0),coalesce(accMess.sum,0)"
    	             +" from (select year(a.endcasedate) endyear, count(distinct a.caseno) count,sum(b.realpay) sum from llcase a,llclaimdetail b "
    	             +" where a.caseno=b.caseno and a.mngcom like '"+mManageCom+"%'"
    	             +" and a.endcasedate Between '"+mECaseDateS+"'And '"+mECaseDateE+"'"
    	             +" And a.Rgtdate Between '"+mRgtDateS+"' And '"+mRgtDateE+"'"
    	             +" and a.rgtstate in ('09','11','12') and a.endcasedate is not null "
    	             +" group by year(a.endcasedate)) as rgtMess full join "
    	             +"(select year(a.endcasedate) endyear,count(distinct a.caseno) count,sum(d.realpay) sum "
    	             +" from LLcase a, LLcaserela b,LLsubreport c ,llclaimdetail d where a.caseno= d.caseno And a.caseno = b.caseno and b.subrptno=c.subrptno "
    	             +" and a.mngcom like '"+mManageCom+"%'"
    	             +" and a.endcasedate Between '"+mECaseDateS+"'And '"+mECaseDateE+"'" 
    	             +" And c.Accdate Between '"+mAccDateS+"'And '"+mAccDateE+"'"
    	             +" And a.rgtstate in ('09','11','12') and a.endcasedate is not null "
    	             +" group by year(a.endcasedate)) as accMess"
    	             +" on rgtMess.endyear=accMess.endyear  "
    	             +" group by rgtMess.endyear,rgtMess.count,rgtMess.sum,accMess.count,accMess.sum "
    	             +" order by rgtMess.endyear with ur";
    	             */
   
    	 String YSQL=" select (case when nvl(rgtMess.endyear,0) = 0 then accMess.endyear else rgtMess.endyear end)startyear,coalesce(rgtMess.count,0),coalesce(rgtMess.sum,0), coalesce(accMess.count,0),coalesce(accMess.sum,0)"
             +" from (select year(a.rgtdate) endyear, count(distinct a.caseno) count,sum(b.realpay) sum from llcase a,llclaimdetail b "
             +" where a.caseno=b.caseno and a.mngcom like '"+mManageCom+"%'"
             +" and a.endcasedate Between '"+mECaseDateS+"'And '"+mECaseDateE+"'"
             +" And a.Rgtdate Between '"+mRgtDateS+"' And '"+mRgtDateE+"'"
             +" and a.rgtstate in ('09','11','12') and a.endcasedate is not null "
             +" group by year(a.rgtdate)) as rgtMess full join "
             +"(select year(c.accdate) endyear,count(distinct a.caseno) count,sum(d.realpay) sum "
             +" from LLcase a, LLcaserela b,LLsubreport c ,llclaimdetail d where a.caseno= d.caseno And a.caseno = b.caseno and b.subrptno=c.subrptno "
             +" and a.mngcom like '"+mManageCom+"%'"
             +" and a.endcasedate Between '"+mECaseDateS+"'And '"+mECaseDateE+"'" 
             +" And c.Accdate Between '"+mAccDateS+"'And '"+mAccDateE+"'"
             +" And a.rgtstate in ('09','11','12') and a.endcasedate is not null "
             +" group by year(c.accdate)) as accMess"
             +" on rgtMess.endyear=accMess.endyear  "
             +" group by rgtMess.endyear,accMess.endyear,rgtMess.count,rgtMess.sum,accMess.count,accMess.sum "
             +" order by startyear with ur";
    	    String YSQL1 = " select count(distinct a.caseno),sum(b.realpay)"
	 			+" from llcase a , llclaimdetail b"
	 			+" where a.caseno = b.caseno and a.mngcom like '" + mManageCom +"%'"
	 			+" and a.endcasedate Between '"+mECaseDateS+"'And '"+mECaseDateE+"'" 
	 			+" And a.rgtstate in ('09','11','12') and a.endcasedate is not null "
	 			+" with ur";
	 			

    	        System.out.println(YSQL);
    	        System.out.println(YSQL1);
                SSRS ttSSRS1 =ttExeSQL.execSQL(YSQL);
                SSRS ttSSRS2 =ttExeSQL.execSQL(YSQL1);
                String str1 = compare2(mRgtDateS,mAccDateS);
                System.out.println(str1+"min");
               
	            String str2 = compare1(mRgtDateE,mAccDateE);
	            System.out.println(str2+"max");
	            
	            //当受理时间和事件时间的最小值小于查询最小值时：
	              //当日期最小值年份小于查询最小年份时：
	              if(Integer.parseInt(str1.substring(0,4))<Integer.parseInt(ttSSRS1.GetText(1, 1)) ) {
	            	 
	            	  for (int  i = Integer.parseInt(str1.substring(0,4));i <Integer.parseInt(ttSSRS1.GetText(1, 1));i++ ) {
	            		 
		            	 
		            		 String ListInfo[]={"","","","","",""};	
		            		  ListInfo[0]=i+"";//时间
		     	              ListInfo[1]="0";//受理间结案案件数
		     	              ListInfo[2]="0.00";//受理间结案金额
		     	              ListInfo[3]="0";//事件间结案案件数
		     	              ListInfo[4]="0.00";//事件间结案金额
		     	              mListTable.add(ListInfo);	
		            	 
	            		
	            	}
	            	 
	            	  
	              } else {}//当日期最小值年份等于查询最小年份时：
	            	  
//	            //当查询结果不为零时
                if (ttSSRS1.getMaxRow()>0)
                {
                for(int i=1;i<=ttSSRS1.getMaxRow();i++)
	            {
                String rgtpay = mDF.format(Double.parseDouble(ttSSRS1.GetText(i, 3)));
                String  accpay = mDF.format(Double.parseDouble(ttSSRS1.GetText(i, 5)));
                    
                System.out.println(rgtpay);
                 String ListInfo[]={"","","","","",""};	
                 ListInfo[0]=ttSSRS1.GetText(i, 1);//时间
	             ListInfo[1]=ttSSRS1.GetText(i, 2);//受理间案件结案数
	             ListInfo[2]=rgtpay;//受理间结案金额
	             ListInfo[3]=ttSSRS1.GetText(i, 4);//事件间案件结案数
	             ListInfo[4]=accpay;//时间间案件金额
	             mListTable.add(ListInfo);
	            }
//              当受理时间和事件时间的最大值大于查询最大值时：
	              //当日期最大值年份大于查询最大年份时：
                if(Integer.parseInt(str2.substring(0,4))>Integer.parseInt(ttSSRS1.GetText(ttSSRS1.getMaxRow(), 1)) ) {
		            	//
              	      System.out.println( Integer.parseInt(ttSSRS1.GetText(ttSSRS1.getMaxRow(), 1)));
	            	  for ( int i1 = Integer.parseInt(ttSSRS1.GetText(ttSSRS1.getMaxRow(), 1))+1;i1 <=Integer.parseInt(str2.substring(0,4));i1++ ) {
		            		 
			            		 String ListInfo[]={"","","","","",""};	
			            		  ListInfo[0]=i1+ "";//时间
			     	              ListInfo[1]="0";//受理间结案案件数
			     	              ListInfo[2]="0.00";//受理间结案金额
			     	              ListInfo[3]="0";//事件间结案案件数
			     	              ListInfo[4]="0.00";//事件间结案金额
			     	              mListTable.add(ListInfo);	
			            	 
		            		
		            	}
	            	
		             
		            	
		            	  
		              } else {
		              }
		             
                 String ListInfo[]={"","","","","",""};	
                 ListInfo[0]="结案期间内";//时间
	              ListInfo[1]="结案件数:";
	              ListInfo[2]=ttSSRS2.GetText(1, 1);//结案期间结案总案件数
	              ListInfo[3]="结案赔款:";
	              ListInfo[4]=mDF.format(Double.parseDouble(ttSSRS2.GetText(1, 2)));//结案期间结案总赔款
	              mListTable.add(ListInfo);	
                 }else{
                String ListInfo[]={"","","","","",""};	
		        ListInfo[3]="0";
		        ListInfo[4]="0";
		        ListInfo[1]="0";
		        ListInfo[2]="0";
		
	    }
	 
		  	  
       }
      ///////////////////////////////////////////////////以年月显示/////////////////////////////////////////////////////////////////
            if (datetype==2)
    {
    	  
//            	   String YMSQL=" select rgtMess.endyear,rgtMess.endmonth,coalesce(rgtMess.count,0),coalesce(rgtMess.sum,0),"
//   					+" coalesce(accMess.count,0),coalesce(accMess.sum,0)"
//   					+" from"
//   					+" (select year(a.endcasedate) endyear,month(a.endcasedate) endmonth,count(distinct a.caseno) count,sum(b.realpay) sum"
//   					+" from llcase a,llclaimdetail b"
//   					+" where a.caseno=b.caseno"
//   					+" and a.mngcom like '"+mManageCom+"%'"
//   					+" and a.endcasedate Between '"+mECaseDateS+"'And '"+mECaseDateE+"'"
//   					+" And a.Rgtdate Between '"+mRgtDateS+"' And '"+mRgtDateE+"'"
//   					+" and a.rgtstate in ('09','11','12')"
//   					+" and a.endcasedate is not null"
//  					+" group by year(a.endcasedate),month(a.endcasedate)) as rgtMess full join" 						
//  					+" (select year(a.endcasedate) endyear,month(a.endcasedate) endmonth,count(distinct a.caseno) count,sum(d.realpay) sum"
//   					+" from LLcase a, LLcaserela b,LLsubreport c ,llclaimdetail d"
//   					+" where a.caseno= d.caseno"
//   					+" And a.caseno = b.caseno and b.subrptno=c.subrptno"
//   					+" and a.mngcom like '"+mManageCom+"%'"
//   					+" and a.endcasedate Between '"+mECaseDateS+"'And '"+mECaseDateE+"'"
//   					+" And c.Accdate Between '"+mAccDateS+"'And '"+mAccDateE+"'"
//   					+" and a.rgtstate in ('09','11','12')"
//   					+" and a.endcasedate is not null"
//   					+" group by year(a.endcasedate),month(a.endcasedate)) as accMess"	
//   					+" on rgtMess.endyear=accMess.endyear and rgtMess.endmonth=accMess.endmonth"
//   					+" group by rgtMess.endyear,rgtMess.endmonth,accMess.endyear,accMess.endmonth,rgtMess.count,rgtMess.sum,accMess.count,accMess.sum"
//   					+" order by rgtMess.endyear,rgtMess.endmonth,accMess.endyear,accMess.endmonth"
//   					+" with ur";
            	   String YMSQL=" select  (case when nvl(rgtMess.endyear,0) = 0 then accMess.endyear else rgtMess.endyear end)startyear ,(case when nvl(rgtMess.endmonth,0) = 0 then accMess.endmonth else rgtMess.endmonth end)overyear,coalesce(rgtMess.count,0),coalesce(rgtMess.sum,0),"
  					+" coalesce(accMess.count,0),coalesce(accMess.sum,0)"
   					+" from"
   					+" (select year(a.rgtdate) endyear,month(a.rgtdate) endmonth,count(distinct a.caseno) count,sum(b.realpay) sum"
   					+" from llcase a,llclaimdetail b"
   					+" where a.caseno=b.caseno"
   					+" and a.mngcom like '"+mManageCom+"%'"
   					+" and a.endcasedate Between '"+mECaseDateS+"'And '"+mECaseDateE+"'"
   					+" And a.Rgtdate Between '"+mRgtDateS+"' And '"+mRgtDateE+"'"
   					+" and a.rgtstate in ('09','11','12')"
   					+" and a.endcasedate is not null"
   					+" group by year(a.rgtdate),month(a.rgtdate)) as rgtMess full join" 						
  					+" (select year(c.accdate) endyear,month(c.accdate) endmonth,count(distinct a.caseno) count,sum(d.realpay) sum"
   					+" from LLcase a, LLcaserela b,LLsubreport c ,llclaimdetail d"
   					+" where a.caseno= d.caseno"
   					+" And a.caseno = b.caseno and b.subrptno=c.subrptno"
   					+" and a.mngcom like '"+mManageCom+"%'"
   					+" and a.endcasedate Between '"+mECaseDateS+"'And '"+mECaseDateE+"'"
   					+" And c.Accdate Between '"+mAccDateS+"'And '"+mAccDateE+"'"
   					+" and a.rgtstate in ('09','11','12')"
  					+" and a.endcasedate is not null"
  					+" group by year(c.accdate),month(c.accdate)) as accMess"	
   					+" on rgtMess.endyear=accMess.endyear and rgtMess.endmonth=accMess.endmonth"
   					+" group by rgtMess.endyear,rgtMess.endmonth,accMess.endyear,accMess.endmonth,rgtMess.count,rgtMess.sum,accMess.count,accMess.sum"
   					+" order by startyear,overyear"
   					+" with ur";
            	   
            	   String YMSQL1 = " select count(distinct a.caseno),sum(b.realpay)"
   		 			+" from llcase a , llclaimdetail b"
   		 			+" where a.caseno = b.caseno and a.mngcom like '" + mManageCom +"%'"
   		 			+" and a.endcasedate Between '"+mECaseDateS+"'And '"+mECaseDateE+"'" 
   		 			+" And a.rgtstate in ('09','11','12') and a.endcasedate is not null "
   		 			+" with ur";
   		 			

   	              
    	          System.out.println(YMSQL);
    	          System.out.println(YMSQL1);
                  SSRS ttSSRS2 =ttExeSQL.execSQL(YMSQL);
                  SSRS ttSSRS3 =ttExeSQL.execSQL(YMSQL1);
                 
                  String str1 = compare2(mRgtDateS,mAccDateS);
                  System.out.println(str1+"min");
                 
	              String str2 = compare1(mRgtDateE,mAccDateE);
	              System.out.println(str2+"max");
	              //当受理时间和事件时间的最小值小于查询最小值时：
	              //当日期最小值年份小于查询最小年份时：
	              if(Integer.parseInt(str1.substring(0,4))<Integer.parseInt(ttSSRS2.GetText(1, 1)) ) {
	            	  int i = Integer.parseInt(str1.substring(0,4));
	            	  int j = 0;
	            	  try {
		            		 j = Integer.parseInt(str1.substring(5,7));
		            	 } catch (Exception e) {
		            		 j = Integer.parseInt(str1.substring(5,6));
		            	 }
		            	 for(;j <= 12;j ++) {
		            		 String ListInfo[]={"","","","","",""};	
		            		  ListInfo[0]=i+ "-"+j;//时间
		     	              ListInfo[1]="0";//受理间结案案件数
		     	              ListInfo[2]="0.00";//受理间结案金额
		     	              ListInfo[3]="0";//事件间结案案件数
		     	              ListInfo[4]="0.00";//事件间结案金额
		     	              mListTable.add(ListInfo);	
		            	 }
	            	  for ( i = Integer.parseInt(str1.substring(0,4))+1;i <Integer.parseInt(ttSSRS2.GetText(1, 1));i++ ) {
	            		 
		            	 for(int z = 1;z <= 12;z ++) {
		            		 String ListInfo[]={"","","","","",""};	
		            		  ListInfo[0]=i+ "-"+z;//时间
		     	              ListInfo[1]="0";//受理间结案案件数
		     	              ListInfo[2]="0.00";//受理间结案金额
		     	              ListInfo[3]="0";//事件间结案案件数
		     	              ListInfo[4]="0.00";//事件间结案金额
		     	              mListTable.add(ListInfo);	
		            	 }
	            		
	            	}
	            	 if(i == Integer.parseInt(ttSSRS2.GetText(1, 1))) {
	            		 for (int k = 1;k <Integer.parseInt(ttSSRS2.GetText(1, 2));k++) {
	            			 String ListInfo[]={"","","","","",""};	
		            		  ListInfo[0]=i+ "-"+k;//时间
		     	              ListInfo[1]="0";//受理间结案案件数
		     	              ListInfo[2]="0.00";//受理间结案金额
		     	              ListInfo[3]="0";//事件间结案案件数
		     	              ListInfo[4]="0.00";//事件间结案金额
		     	              mListTable.add(ListInfo);	
	            		 }
	            	 }
	            	 
	            	  
	              } else {//当日期最小值年份等于查询最小年份时：
	            	  int j = 0;
	            	  try {
	            		 j = Integer.parseInt(str1.substring(5,7));
	            	 } catch (Exception e) {
	            		 j = Integer.parseInt(str1.substring(5,6));
	            	 }
	            	 for(;j < Integer.parseInt(ttSSRS2.GetText(1, 2));j ++) {
	            		 String ListInfo[]={"","","","","",""};	
	            		  ListInfo[0]=ttSSRS2.GetText(1, 1)+ "-"+j;//时间
	     	              ListInfo[1]="0";//受理间结案案件数
	     	              ListInfo[2]="0.00";//受理间结案金额
	     	              ListInfo[3]="0";//事件间结案案件数
	     	              ListInfo[4]="0.00";//事件间结案金额
	     	              mListTable.add(ListInfo);	
	            	 }
	              }
	              
	             //当查询结果不为零时
                  if (ttSSRS2.getMaxRow()>0)
                  { int i;
                   for(i=1;i<=ttSSRS2.getMaxRow();i++)
                   { String rgtpay = mDF.format(Double.parseDouble(ttSSRS2.GetText(i, 4)));
                     String  accpay = mDF.format(Double.parseDouble(ttSSRS2.GetText(i, 6)));
                     
                     System.out.println(rgtpay);
                   String ListInfo[]={"","","","","",""};	
                   ListInfo[0]=ttSSRS2.GetText(i, 1)+'-'+ttSSRS2.GetText(i, 2);//时间
 	              ListInfo[1]=ttSSRS2.GetText(i, 3);//受理间结案案件数
 	              ListInfo[2]=rgtpay;//受理间结案金额
 	              ListInfo[3]=ttSSRS2.GetText(i, 5);//事件间结案案件数
 	              ListInfo[4]=accpay;//事件间结案金额
 	              mListTable.add(ListInfo);	
                   }
                   //当受理时间和事件时间的最大值大于查询最大值时：
 	              //当日期最大值年份大于查询最大年份时：
                  if(Integer.parseInt(str2.substring(0,4))>Integer.parseInt(ttSSRS2.GetText(ttSSRS2.getMaxRow(), 1)) ) {
		            	//
                	   int i1 = Integer.parseInt(ttSSRS2.GetText(ttSSRS2.getMaxRow(),1));
		            	 for(int z = Integer.parseInt(ttSSRS2.GetText(ttSSRS2.getMaxRow(),2))+1;z<=12;z++) {
		            		 String ListInfo[]={"","","","","",""};	
		            		  ListInfo[0]=i1+ "-"+z;//时间
		     	              ListInfo[1]="0";//受理间结案案件数
		     	              ListInfo[2]="0.00";//受理间结案金额
		     	              ListInfo[3]="0";//事件间结案案件数
		     	              ListInfo[4]="0.00";//事件间结案金额
		     	              mListTable.add(ListInfo);	
		            	 }
	            	  for ( i1 = Integer.parseInt(ttSSRS2.GetText(ttSSRS2.getMaxRow(), 1))+1;i1 <Integer.parseInt(str2.substring(0,4));i1++ ) {
		            		 int j = 0;
//			            	  try {
//			            		 j = Integer.parseInt(str2.substring(5,7));
//			            	 } catch (Exception e) {
//			            		 j = Integer.parseInt(str2.substring(5,6));
//			            	 }
			            	 for(j = 1;j <= 12;j ++) {
			            		 String ListInfo[]={"","","","","",""};	
			            		  ListInfo[0]=i1+ "-"+j;//时间
			     	              ListInfo[1]="0";//受理间结案案件数
			     	              ListInfo[2]="0.00";//受理间结案金额
			     	              ListInfo[3]="0";//事件间结案案件数
			     	              ListInfo[4]="0.00";//事件间结案金额
			     	              mListTable.add(ListInfo);	
			            	 }
		            		
		            	}
	            	
		             
		            	if( i1 == Integer.parseInt(str2.substring(0,4))) {
		            		int month= 0;
	            	       try {
	            	    	   month = Integer.parseInt(str2.substring(5,7));
		            	   } catch (Exception e) {
		            		   month = Integer.parseInt(str2.substring(5,6));
		            	   }
			            	 for(int j = 1;j <= month ;j ++) {
			            		 String ListInfo[]={"","","","","",""};	
			            		  ListInfo[0]=i1+ "-"+j;//时间
			     	              ListInfo[1]="0";//受理间结案案件数
			     	              ListInfo[2]="0.00";//受理间结案金额
			     	              ListInfo[3]="0";//事件间结案案件数
			     	              ListInfo[4]="0.00";//事件间结案金额
			     	              mListTable.add(ListInfo);	
			            	 }
		            	}
		            	 
		            	  
		              } else {// 当日期最大值年份等于查询最大年份时：
		            	  int month = 0;
		            	  try {
		            		 month = Integer.parseInt(str2.substring(5,7));
		            		 System.out.println(month);
		            	 } catch (Exception e) {
		            		 month = Integer.parseInt(str2.substring(5,6));
		            		 System.out.println(month);
		            	 }
		            	 System.out.println(month);
		            	 for( int j = Integer.parseInt(ttSSRS2.GetText(ttSSRS2.getMaxRow(), 2))+1; j <= month;j ++) {
		            		 String ListInfo[]={"","","","","",""};	
		            		  ListInfo[0]=ttSSRS2.GetText(ttSSRS2.getMaxRow(),1)+ "-"+j;//时间
		     	              ListInfo[1]="0";//受理间结案案件数
		     	              ListInfo[2]="0.00";//受理间结案金额
		     	              ListInfo[3]="0";//事件间结案案件数
		     	              ListInfo[4]="0.00";//事件间结案金额
		     	              mListTable.add(ListInfo);	
		            	 }
		              }
		             
                   String ListInfo[]={"","","","","",""};	
                   ListInfo[0]="结案期间内";//时间
 	              ListInfo[1]="结案件数:";
 	              ListInfo[2]=ttSSRS3.GetText(1, 1);//结案期间结案总案件数
 	              ListInfo[3]="结案赔款:";
 	              ListInfo[4]=mDF.format(Double.parseDouble(ttSSRS3.GetText(1, 2)));//结案期间结案总赔款
 	              mListTable.add(ListInfo);	
                  }else{
                 	 String ListInfo[]={"","","","","",""};	
 		          ListInfo[3]="0";
 		          ListInfo[4]="0.00";
 		          ListInfo[1]="0";
 		          ListInfo[2]="0.00";
 	              }
 	 
                   }
   }
              catch (Exception ex) {
	          ex.printStackTrace();
    }   
      
     return true;
    
  }
  //取两个字符串格式日期的较大值 	
private String compare1(String s1,String s2) {
	String  flag   =  "" ; 

	for(int   i   =   0;i   <   s1.length();   i++) 
	{ 
	    if   (s1.charAt(i)   <   s2.charAt(i))   
	        { 
	            flag = s2;
	            return flag;
	        } 
	        else if(s1.charAt(i)   >  s2.charAt(i)) { 
	            flag   =  s1;
	            return flag;
	         } else {}
	    
	}
	return s1;
	

}
//取两个字符串格式日期的较小值
private String compare2(String s1,String s2) {
	String  flag   =  "" ; 

	for(int   i   =   0;i   <   s1.length();   i++) 
	{ 
	    if   (s1.charAt(i)   <   s2.charAt(i))   
	        { 
	            flag = s1;
	            return flag;
	        } 
	        else if(s1.charAt(i)   >  s2.charAt(i)) { 
	            flag   =  s2;
	            return flag;
	         } else {}
	    
	}
	return s1;
	

}
private boolean queryData()
{
  
  int datetype = -1;
  TextTag tTextTag = new TextTag();
  mXmlExport = new XmlExport();
  //设置模板名称
  mXmlExport.createDocument("LLEndCaseStatistics.vts","printer");
  System.out.println("获取基本信息：");
  
  String Sql0="select Name from ldcom where comcode='"+mManageCom+"'";
  ExeSQL ttExeSQL = new ExeSQL();
  SSRS ttSSRS3 =ttExeSQL.execSQL(Sql0);
  
  tTextTag.add("ComName",ttSSRS3.GetText(1,1));
  tTextTag.add("ECaseDateS",mECaseDateS);
  tTextTag.add("ECaseDateE",mECaseDateE);
  tTextTag.add("RgtDateS", mRgtDateS);
  tTextTag.add("RgtDateE", mRgtDateE);
  tTextTag.add("AccDateS", mAccDateS);
  tTextTag.add("AccDateE", mAccDateE);
  tTextTag.add("Operator", mGlobalInput.Operator);
  tTextTag.add("MakeDate", mCurrentDate);
 
    //2种统计类型
      if (mDateType!=null&&mDateType.equals("Y")) //以年统计
    {
	  datetype = 1;                                        
	} else if (mDateType!=null&&mDateType.equals("YM"))//以年月统计
	{
	  datetype = 2;                                       
	} 
	
	
  mXmlExport.addTextTag(tTextTag);
  String[] tTitle ={ "COL1", "COL2", "COL3", "COL4", "COL5"};
  if (!getDataList(datetype))
  {
    return false;
  }
  mListTable.setName("ENDOR");
  mXmlExport.addListTable(mListTable,tTitle);
  mXmlExport.outputDocumentToFile("D:\\", "test7");
  this.mResult.clear();
  mResult.addElement(mXmlExport);
  return true;

}

//取得传入的数据
private boolean getInputData(VData mInputData)
{
 mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
 mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
 mManageCom =(String)mTransferData.getValueByName("ManageCom");
 mDateType  =(String)mTransferData.getValueByName("DateType");
 mECaseDateS=(String)mTransferData.getValueByName("ECaseDateS");
 mECaseDateE=(String)mTransferData.getValueByName("ECaseDateE");
 mRgtDateS  =(String)mTransferData.getValueByName("RgtDateS");
 mRgtDateE  =(String)mTransferData.getValueByName("RgtDateE");
 mAccDateS  =(String)mTransferData.getValueByName("AccDateS");
 mAccDateE  =(String)mTransferData.getValueByName("AccDateE");
 mFileNameB =(String) mTransferData.getValueByName("tFileNameB");
 mComName   =(String) mTransferData.getValueByName("ComName");
 return true;
}

 /**
	 * 追加错误信息
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLEndCaseStatisticsBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	/**
	 * 取得返回处理过的结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		return this.mResult;
	}

	public static void main(String[] args) {
		
		

	}

	private void jbInit() throws Exception {
	}
 
}
