package com.sinosoft.lis.f1print;

/**
 * <p>Title: ClaimDayBalanceBL</p>
 * <p>Description:X5理赔应付日结单 </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: sinosoft</p>
 * @author : zhangjun yanghao
 * @date:2006-03-30
 * @version 1.0
 */
import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;

public class FenClaimDayBalanceBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private String mDay[] = null; // 获取时间

	private GlobalInput mGlobalInput = new GlobalInput(); // 全局变量

	public FenClaimDayBalanceBL() {
	}

	public static void main(String[] args) {
		GlobalInput tG = new GlobalInput();
		tG.Operator = "001";
		tG.ManageCom = "86110000";
		VData vData = new VData();
		String[] tDay = new String[4];
		tDay[0] = "2006-03-01";
		tDay[1] = "2006-03-31";
		vData.addElement(tDay);
		vData.addElement(tG);

		FenClaimDayBalanceBL tC = new FenClaimDayBalanceBL();
		tC.submitData(vData, "PRINT");

	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("PRINT")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		if (!getInputData(cInputData)) {
			return false;
		}
		mResult.clear();

		if (cOperate.equals("PRINT")) { // 打印提数
			if (!getPrintData()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) { // 打印付费
		// 全局变量
		mDay = (String[]) cInputData.get(0);
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "FinDayCheckBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	/**
	 * 新的打印数据方法
	 * 
	 * @return
	 */
	private boolean getPrintData() {
		SSRS tSSRS = new SSRS();

		GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
		tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
		tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
		tGetSQLFromXML.setParameters("EndDate", mDay[1]);

		String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
		//tServerPath = "D:/workspace/picc/WebContent/";
		ExeSQL tExeSQL = new ExeSQL();

		String nsql = "select Name from LDCom where ComCode='"
				+ mGlobalInput.ManageCom + "'"; // 管理机构
		tSSRS = tExeSQL.execSQL(nsql);
		String manageCom = tSSRS.GetText(1, 1);
		TextTag texttag = new TextTag(); // 新建一个TextTag的实例
		XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
		xmlexport.createDocument("FenClaimDayCheck.vts", "printer"); // 最好紧接着就初始化xml文档
		texttag.add("StartDate", mDay[0]);
		texttag.add("EndDate", mDay[1]);
		texttag.add("ManageCom", manageCom);
		if (texttag.size() > 0) {
			xmlexport.addTextTag(texttag);
		}
		String[] detailArr = new String[] { "类型", "渠道", "案件号码", "保单号码", "金额" };
		String[] totalArr = new String[] { "类型", "总金额" };

		// 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
		String[] getTypes = new String[] { "LPSSYLJF", // 死伤医疗给付
				"LPPKZC", // 赔款支出
				"LPTX", // 特需理赔
				"LPKCF", // 勘查费
				"XTXLPSSYLJF", // 新特需死伤医疗给付
				"XTXLPPKZC", // 新特需赔款支出
				"XTXLPTX", // 新特需理赔
				"WNLPSSYLJF", // 万能死伤医疗给付-非账户
				"WNLPPKZC",  // 万能死伤医疗给付-账户
				//"WNLPTX", // 万能理赔
				//"YFPK",    //预付赔款-给付确认
				"YFPK_SQ"  //预付赔款-申请
		};

		for (int i = 0; i < getTypes.length; i++) {
			String msql = tGetSQLFromXML.getSql(tServerPath
					+ "f1print/picctemplate/FenFeePrintSql.xml", getTypes[i]);
			ListTable tDetailListTable = getDetailListTable(msql, getTypes[i]); // 明细ListTable
			xmlexport.addListTable(tDetailListTable, detailArr); // 明细ListTable
																	// 放入xmlexport对象

			// 汇总的SQL语句是在明细名字后加个 "Z" 代表汇总
			msql = tGetSQLFromXML
					.getSql(tServerPath
							+ "f1print/picctemplate/FenFeePrintSql.xml",
							getTypes[i] + "Z");
			System.out.println("+++++++++++++++++"+msql);
			System.out.println("-----------------"+getTypes[i]);
			ListTable tTotalListTabel = getHZListTable(msql, getTypes[i] + "Z"); // 汇总ListTable
			xmlexport.addListTable(tTotalListTabel, totalArr); // 汇总ListTable
																// 放入xmlexport对象
		}

		mResult.clear();
		mResult.addElement(xmlexport);
		return true;
	}

	/**
	 * 获得明细ListTable
	 * 
	 * @param msql -
	 *            执行的SQL
	 * @param tName -
	 *            Table Name
	 * @return
	 */
	private ListTable getDetailListTable(String msql, String tName) {
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(msql);
		ListTable tlistTable = new ListTable();
		String strSum = "";
		String strArr[] = null;
		tlistTable.setName(tName);
		for (int i = 1; i <= tSSRS.MaxRow; i++) {
			strArr = new String[5];
			for (int j = 1; j <= tSSRS.MaxCol; j++) {
				if (j == 5) {
					strArr[j - 1] = tSSRS.GetText(i, j);
					strSum = new DecimalFormat("0.00").format(Double
							.valueOf(strArr[j - 1]));
					strArr[j - 1] = strSum;
					continue;
				}
				strArr[j - 1] = tSSRS.GetText(i, j);
			}
			tlistTable.add(strArr);
		}
		return tlistTable;
	}

	/**
	 * 显示无金额的情况
	 * 
	 * @param pName
	 * @return
	 */
	private String[] getChineseNameWithPY(String pName) {
		String[] result = new String[2];
		if ("LPSSYLJFZ".equals(pName)) {
			result[0] = "死伤医疗给付 合计";
		} else if ("LPPKZCZ".equals(pName)) {
			result[0] = "赔款支出 合计";
		} else if ("LPTXZ".equals(pName)) {
			result[0] = "特需理赔 合计";
		} else if ("LPKCFZ".equals(pName)) {
			result[0] = "勘查费 合计";
		}
		else if ("XTXLPSSYLJFZ".equals(pName)) {
			result[0] = "新特需死伤医疗给付 合计";
		} else if ("XTXLPPKZCZ".equals(pName)) {
			result[0] = "新特需赔款支出 合计";
		} else if ("LXTXLPTXZ".equals(pName)) {
			result[0] = "新特需理赔 合计";
		}
		else if ("WNLPSSYLJFZ".equals(pName)) {
			result[0] = "万能理赔-非账户 合计";
		} else if ("WNLPPKZCZ".equals(pName)) {
			result[0] = "万能理赔-账户 合计";
		} else if ("WNLPTXZ".equals(pName)) {
			result[0] = "万能理赔 合计";
		} else if ("YFPKZ".equals(pName)) {
			result[0] = "预付赔款给付确认 合计";
		}else if ("YFPK_SQZ".equals(pName)) {
			result[0] = "预付赔款申请 合计";
		}
		result[1] = "0";
		return result;
	}

	/**
	 * 获得汇总ListTable
	 * 
	 * @param msql -
	 *            执行的SQL
	 * @param tName -
	 *            Table Name
	 * @return
	 */
	private ListTable getHZListTable(String msql, String tName) {
		ListTable tHZlistTable = new ListTable();
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(msql);
		if(tExeSQL==null)
		{
			tExeSQL= new ExeSQL();
		}
		tHZlistTable.setName(tName); // 设置Table名字
		String[] strArr = new String[2];
		if (tSSRS.MaxRow > 0) {
			for (int j = 1; j <= tSSRS.MaxCol; j++) {
				strArr[j - 1] = tSSRS.GetText(1, j);
			}
		} else {
			strArr = getChineseNameWithPY(tName);
		}
		tHZlistTable.add(strArr);
		return tHZlistTable;
	}

	/**
	 * 以前的打印方法
	 * 
	 * @return
	 */
	private boolean getPrintDataOld() {
		// String msql = "select otherno,sum(pay) a from LJAGetClaim where
		// MakeDate>='"+mDay[0]+"' and MakeDate<='"+mDay[1]+"' "
		// + "and ManageCom like '"+mGlobalInput.ManageCom+"%' and RiskCode not
		// in (Select RiskCode From LMRiskApp Where Risktype3 = '7' and riskcode
		// not in ('170101','170301')) or RiskCode is null group by otherno"
		// + " union all select tempfeeno,sum(getmoney) from LJAGetTempFee where
		// MakeDate>='"+mDay[0]+"'"
		// + " and MakeDate<='"+mDay[1]+"' and ManageCom like
		// '"+mGlobalInput.ManageCom+"%' and RiskCode not in (Select RiskCode
		// From LMRiskApp Where Risktype3 = '7' and riskcode not in
		// ('170101','170301')) group by tempfeeno";
		GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
		tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
		tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
		tGetSQLFromXML.setParameters("EndDate", mDay[1]);
		String tServerPath = (new ExeSQL())
				.getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
		String msql = tGetSQLFromXML.getSql(tServerPath
				+ "f1print/picctemplate/FeePrintSql.xml", "ClaimDayBalance");

		if (!productXml(msql))
			return false;
		else
			return true;
	}

	// 生成XML函数
	private boolean productXml(String msql) {
		SSRS tSSRS = new SSRS();
		SSRS nSSRS = new SSRS();
		double ESumMoney = 0;
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(msql);
		ListTable tlistTable = new ListTable();
		String strSum = "";
		String strArr[] = null;
		tlistTable.setName("CLAIM");
		for (int i = 1; i <= tSSRS.MaxRow; i++) {
			strArr = new String[2];
			for (int j = 1; j <= tSSRS.MaxCol; j++) {
				if (j == 1) {
					strArr[j - 1] = tSSRS.GetText(i, j);
				}
				if (j == 2) {
					strArr[j - 1] = tSSRS.GetText(i, j);
					strSum = new DecimalFormat("0.00").format(Double
							.valueOf(strArr[j - 1]));
					strArr[j - 1] = strSum;
					ESumMoney = ESumMoney + Double.parseDouble(strArr[j - 1]);
				}
			}
			tlistTable.add(strArr);
		}
		ESumMoney = PubFun.setPrecision(ESumMoney, "0.00");
		strArr = new String[2];
		strArr[0] = "CaseNo";
		strArr[1] = "Money";

		String nsql = "select Name from LDCom where ComCode='"
				+ mGlobalInput.ManageCom + "'";
		ExeSQL nExeSQL = new ExeSQL();
		nSSRS = nExeSQL.execSQL(nsql);
		String manageCom = nSSRS.GetText(1, 1);

		TextTag texttag = new TextTag(); // 新建一个TextTag的实例
		XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
		xmlexport.createDocument("ClaimDayCheck.vts", "printer"); // 最好紧接着就初始化xml文档
		texttag.add("StartDate", mDay[0]);
		texttag.add("EndDate", mDay[1]);
		texttag.add("ManageCom", manageCom);
		texttag.add("SumMoney", ESumMoney);

		if (texttag.size() > 0) {
			xmlexport.addTextTag(texttag);
		}
		xmlexport.addListTable(tlistTable, strArr);
		mResult.clear();
		mResult.addElement(xmlexport);
		xmlexport.outputDocumentToFile("E:\\", "ClaimDayBalance");
		return true;
	}
}
