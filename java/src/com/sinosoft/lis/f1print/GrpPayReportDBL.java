package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Xx
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Date.*;
import java.util.GregorianCalendar;
import com.sinosoft.utility.VData;
public class GrpPayReportDBL
    implements PrintService {

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();
  private VData mResult = new VData();
  private String mdirect ="";
  private FDate fDate = new FDate();
  //业务处理相关变量
  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();

  private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
  private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema(); //立案/申请登记总表
  private LOBatchPRTManagerSchema mLOBatchPRTManagerSchema = new LOBatchPRTManagerSchema(); //理赔申诉错误表
  private LLAppClaimReasonDB mLLAppClaimReasonDB = new LLAppClaimReasonDB();
  private LLAppClaimReasonSchema mLLAppClaimReasonSchema = new
      LLAppClaimReasonSchema();
  private LLAppClaimReasonSet mLLAppClaimReasonSet = new LLAppClaimReasonSet();

  public GrpPayReportDBL() {
  }

  /**
   传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    if (!cOperate.equals("PRINT")) {
      buildError("submitData", "不支持的操作字符串");
      return false;
    }

    // 得到外部传入的数据，将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    mResult.clear();

    // 准备所有要打印的数据
    if (!getPrintData()) {
      return false;
    }
    return true;
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    mLOBatchPRTManagerSchema.setSchema( (LOBatchPRTManagerSchema) cInputData.getObjectByObjectName(
        "LOBatchPRTManagerSchema", 0));
    mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",
        0);
    if (mLOBatchPRTManagerSchema == null) {
      buildError("getInputData", "没有得到足够的信息！");
      return false;
    }
    mdirect += mLOBatchPRTManagerSchema.getStandbyFlag2();
    if (mLOBatchPRTManagerSchema.getOtherNo() == null) {
      buildError("getInputData", "没有得到足够的信息:立案号不能为空！");
      return false;
    }
    return true;
  }

  public VData getResult() {
    return mResult;
  }

  public CErrors getErrors() {
    return mErrors;
  }

  private void buildError(String szFunc, String szErrMsg) {
    CError cError = new CError();

    cError.moduleName = "LCContF1PBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
}

private boolean getPrintData() {
    TextTag texttag = new TextTag(); //新建一个TextTag的实例
    //根据印刷号查询打印队列中的纪录
    String aGrpName = "";
    int count = 0;
    String GrpNo="";
    String GrpContNo = "";
    GrpNo= mLOBatchPRTManagerSchema.getOtherNo();
    GrpContNo= mLOBatchPRTManagerSchema.getStandbyFlag3();
    LCGrpContDB tLCGrpContDB = new LCGrpContDB();
    tLCGrpContDB.setGrpContNo(GrpContNo);
    FDate fDate = new FDate();
    if(!tLCGrpContDB.getInfo()){
        buildError("LCGrpCont", "没有得到保单生效日期！");
        return false;
    }
   Date tStartDate = new Date();
    Date tEndDate = new Date();
    ExeSQL texecsql = new ExeSQL();
    SSRS tSSRS = new SSRS();
    //ListTable

    ListTable ClaimTable = new ListTable();
    ClaimTable.setName("ClaimR");
    String ToSQL="select codename,substr(a.code,1,1) from ldcode a where a.codetype='getdutykind' and a.code like '%00%'";
    tSSRS=texecsql.execSQL(ToSQL);

    for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
      String[] strArr = new String[52];
      tStartDate = fDate.getDate(tLCGrpContDB.getCValiDate());
      String sql1 = "select count(distinct a1.caseno) from llclaimdetail a1,llcase c1 " +
                " where c1.caseno=a1.caseno  and c1.rgtstate in ('12','11','09') and a1.getdutykind in " +
                "(select code from ldcode where codetype='getdutykind' and code like '"+tSSRS.GetText(i,2).substring(0,1)+"%') " +
                " and a1.grpcontno = '" + GrpContNo +
                "' and c1.endcasedate >='";
      String sql2 = "select coalesce(sum(a1.realpay),0) from llclaimdetail a1,llcase c1 "+
                " where c1.caseno=a1.caseno  and c1.rgtstate in ('12','11','09')  and a1.getdutykind in "+
                "(select code from ldcode where codetype='getdutykind' and code like '"+tSSRS.GetText(i,2).substring(0,1)+"%') "+
                " and a1.grpcontno='"+GrpContNo+"' and c1.endcasedate >='";
      strArr[0] = tSSRS.GetText(i, 1);
      strArr[1] = tSSRS.GetText(i, 1);

      for (int t = 1; t <= 24; t++) {
          GregorianCalendar mCalendar = new GregorianCalendar();
          mCalendar.setTime(tStartDate);
          int mYears = mCalendar.get(Calendar.YEAR);
          int mMonths = mCalendar.get(Calendar.MONTH);
          mCalendar.set(mYears, mMonths, 01);
          tStartDate = mCalendar.getTime();
          tEndDate = PubFun.calDate(tStartDate, 1, "M", null);
          String tempS = fDate.getString(tStartDate);
          String tempE = fDate.getString(tEndDate);
          String sqlpart3 = tempS + "' and c1.endcasedate<'" + tempE + "'";
          String ClaimSqlC = sql1+sqlpart3;
          String ClaimSqlM = sql2+sqlpart3;
          String temp2=texecsql.getOneValue(ClaimSqlM);
          // 1  2,3    2  4,5  3  6,7 4 8,9 5 10 11
          int y=t+t;
          strArr[y]=texecsql.getOneValue(ClaimSqlC);
          y++;
          strArr[y]=texecsql.getOneValue(ClaimSqlM);
          tStartDate = tEndDate;
      }
      ClaimTable.add(strArr);
    }

    ListTable ClaimTableT = new ListTable();
    ClaimTableT.setName("TClaimR");
    String[] strArrT = new String[52];
    String partSql=" group by a1.caseno,getdutykind ) as x ";
    String tsql1 = "select coalesce(sum(M),0) from ( select count(distinct a1.caseno) M from llclaimdetail a1,llcase c1 "+
              " where c1.caseno=a1.caseno  and c1.rgtstate in ('12','11','09') and a1.grpcontno = '"+GrpContNo+
              "' and c1.endcasedate >='";
    String tsql2 = "select coalesce(sum(a1.realpay),0) from llclaimdetail a1,llcase c1 "+
              " where c1.caseno=a1.caseno  and c1.rgtstate in ('12','11','09') and a1.grpcontno='"+GrpContNo+
                "' and c1.endcasedate >='";
      strArrT[0] = "";
      strArrT[1] = "合 计";
      tStartDate = fDate.getDate(tLCGrpContDB.getCValiDate());
      for (int t = 1; t <= 24; t++) {
          GregorianCalendar mCalendar = new GregorianCalendar();
          mCalendar.setTime(tStartDate);
          int mYears = mCalendar.get(Calendar.YEAR);
          int mMonths = mCalendar.get(Calendar.MONTH);
          mCalendar.set(mYears, mMonths, 01);
          tStartDate = mCalendar.getTime();
          tEndDate = PubFun.calDate(tStartDate, 1, "M", null);
          String tempS = fDate.getString(tStartDate);
          String tempE = fDate.getString(tEndDate);
          String sqlpart3 = tempS + "' and c1.endcasedate<'" + tempE + "'";
          String ClaimSqlTC = tsql1 + sqlpart3 +partSql;
          String ClaimSqlTM = tsql2 + sqlpart3 ;
          int y = t + t;
          strArrT[y] = texecsql.getOneValue(ClaimSqlTC);
          y++;
          strArrT[y] = texecsql.getOneValue(ClaimSqlTM);
          tStartDate = tEndDate;
      }
      ClaimTableT.add(strArrT);
      ListTable ClaimTableM = new ListTable();
      ClaimTableM.setName("Month");
      String[] strDate = new String[24];
      tStartDate = fDate.getDate(tLCGrpContDB.getCValiDate());
      for (int t = 1; t <= 24; t++) {
          GregorianCalendar mCalendar = new GregorianCalendar();
          mCalendar.setTime(tStartDate);
          int mYears = mCalendar.get(Calendar.YEAR);
          int mMonths = mCalendar.get(Calendar.MONTH);
          mCalendar.set(mYears, mMonths, 01);
          tStartDate = mCalendar.getTime();
          tEndDate = PubFun.calDate(tStartDate, 1, "M", null);
          String tempS = fDate.getString(tStartDate);
          int e = tempS.lastIndexOf("-");
          strDate[t - 1] = tempS.substring(0, e).replace('-', '年') + "月";
          tStartDate = tEndDate;
      }
      ClaimTableM.add(strDate);

    String[] Title = {"ReasonCode","Reason","COL01","COL02","COL03","COL04","COL05","COL06",
                     "COL07","COL08","COL09","COL10","COL11","COL12","COL13",
                     "COL14","COL15","COL16","COL17","COL18","COL19","COL20",
                     "COL21","COL22","COL23","COL24","COL25","COL26",
                     "COL27","COL28","COL29","COL30","COL31","COL32",
                     "COL33","COL34","COL35","COL36","COL37","COL38",
                     "COL39","COL40","COL41","COL42","COL43","COL44",
                     "COL45","COL46","COL47","COL48","COL49","COL50"};
    String[] TitleT = {"TReasonCode","TReason","TCOL01","TCOL02","TCOL03","TCOL04",
                     "TCOL05","TCOL06","TCOL07","TCOL08","TCOL09","TCOL10","TCOL11",
                     "TCOL12","TCOL13","TCOL14","TCOL15","TCOL16","TCOL17","TCOL18",
                     "TCOL19","TCOL20","TCOL21","TCOL22","TCOL23","TCOL24","TCOL25","TCOL26",
                     "TCOL27","TCOL28","TCOL29","TCOL30","TCOL31","TCOL32",
                     "TCOL33","TCOL34","TCOL35","TCOL36","TCOL37","TCOL38",
                     "TCOL39","TCOL40","TCOL41","TCOL42","TCOL43","TCOL44",
                     "TCOL45","TCOL46","TCOL47","TCOL48","TCOL49","TCOL50"};
   String[] TitleM = {"TCOL01","TCOL02","TCOL03","TCOL04",
                    "TCOL05","TCOL06","TCOL07","TCOL08","TCOL09","TCOL10","TCOL11",
                    "TCOL12","TCOL13","TCOL14","TCOL15","TCOL16","TCOL17","TCOL18",
                     "TCOL19","TCOL20","TCOL21","TCOL22","TCOL23","TCOL24"};



    XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
    //xmlexport.createDocuments("PayColPrt.vts", mGlobalInput); //最好紧接着就初始化xml文档
    xmlexport.createDocuments("GrpClaimReport2.vts", mGlobalInput);
    //生成-年-月-日格式的日期

    //PayColPrtBL模版元素
//    texttag.add("BarCode1", aRgtNo);
//    texttag.add("BarCodeParam1"
//                , "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

    texttag.add("GrpName",aGrpName);
    texttag.add("count",count);

    if (texttag.size() > 0) {
      xmlexport.addTextTag(texttag);
    }
    xmlexport.addListTable(ClaimTable, Title);
    xmlexport.addListTable(ClaimTableT, TitleT);
    xmlexport.addListTable(ClaimTableM, TitleM);
    //保存信息
    mResult.clear();
    mResult.addElement(xmlexport);

    return true;
  }

  private String DateFormat(String ori_date)
  {
      int sp_pos1 = ori_date.indexOf("-");
      int sp_pos2 = ori_date.lastIndexOf("-");
      String new_Date = ori_date.substring(0,sp_pos1)+"年"+
                ori_date.substring(sp_pos1+1,sp_pos2)+"月"+
                ori_date.substring(sp_pos2+1)+"日";
      return new_Date;
  }



  public static void main(String[] args) {

    GrpPayReportDBL tGrpPayReportBL = new GrpPayReportDBL();
    VData tVData = new VData();
    GlobalInput tGlobalInput = new GlobalInput();
    tGlobalInput.ManageCom = "86";
    tGlobalInput.Operator = "001";
    tVData.addElement(tGlobalInput);
    LOBatchPRTManagerSchema tLOBatchPRTManagerSchema = new LOBatchPRTManagerSchema();
    tLOBatchPRTManagerSchema.setOtherNo("00000209");
    tLOBatchPRTManagerSchema.setStandbyFlag3("0000020901");
    tVData.addElement(tLOBatchPRTManagerSchema);
    tGrpPayReportBL.submitData(tVData, "PRINT");
    VData vdata = tGrpPayReportBL.getResult();

  }

}
