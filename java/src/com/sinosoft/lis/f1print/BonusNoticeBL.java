package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author z
 * @version 1.0
 */
import java.text.DecimalFormat;

import com.sinosoft.lis.db.LCInsureAccDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LMRiskBonusDB;
import com.sinosoft.lis.db.LOBonusPolDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.CalBase;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.ChangeCodetoName;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.schema.LOBonusPolSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;


public class BonusNoticeBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //业务处理相关变量

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();

    /*转换精确位数的对象   */
    private String FORMATMODOL = "0.00"; //保费保额计算出来后的精确位数
    private DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL); //数字转换对象

    public BonusNoticeBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        try
        {
            if (!cOperate.equals("CONFIRM") && !cOperate.equals("PRINT"))
            {
                buildError("submitData", "不支持的操作字符串");

                return false;
            }

            // 得到外部传入的数据，将数据备份到本类中（不管有没有operate,都要执行这一部）
            if (!getInputData(cInputData))
            {
                return false;
            }

            if (cOperate.equals("CONFIRM"))
            {
                mResult.clear();

                // 准备所有要打印的数据
                getPrintData();
            }
            else if (cOperate.equals("PRINT"))
            {
                if (!saveData(cInputData))
                {
                    return false;
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", ex.toString());

            return false;
        }
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                                              "LOPRTManagerSchema", 0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");

            return false;
        }

        return true;
    }

    //得到返回值
    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "RefuseAppF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    // 准备所有要打印的数据
    private void getPrintData() throws Exception
    {
        XmlExport xmlExport = new XmlExport(); //新建一个XmlExport的实例
        xmlExport.createDocument("BonusNotice.vts", ""); //最好紧接着就初始化xml文档

        LCPolDB tLCPolDB = new LCPolDB();
        LOBonusPolDB tLOBonusPolDB = new LOBonusPolDB();
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();

        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);

        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            throw new Exception("在取得打印队列中数据时发生错误");
        }

        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();

        tLCPolDB.setPolNo(mLOPRTManagerSchema.getOtherNo());

        if (!tLCPolDB.getInfo())
        {
            mErrors.copyAllErrors(tLCPolDB.mErrors);
            throw new Exception("在获取保单信息时出错！");
        }

        LDComSchema tLDComSchema = new LDComSchema();
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLCPolDB.getManageCom().substring(0, 4));

        if (!tLDComDB.getInfo())
        {
            mErrors.copyAllErrors(tLDComDB.mErrors);
            throw new Exception("在取得管理机构名称数据时发生错误！");
        }

        tLDComSchema = tLDComDB.getSchema();

        tLOBonusPolDB.setPolNo(tLCPolDB.getPolNo());
        tLOBonusPolDB.setFiscalYear(Integer.parseInt(StrTool.getYear()) - 1);

        if (!tLOBonusPolDB.getInfo())
        {
            mErrors.copyAllErrors(tLOBonusPolDB.mErrors);
            throw new Exception("在获取保单红利表信息时出错！");
        }

        String strPolNo = tLCPolDB.getPolNo();

        //红利领取方式
        String bonusGetMode = tLCPolDB.getBonusGetMode();

        if (!tLCPolDB.getMainPolNo().equals(strPolNo))
        {
            throw new Exception("传入的号码不是主险投保单号码");
        }

        // 查询打印队列的信息
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();

        if (mLOPRTManagerSchema.getStateFlag() == null)
        {
            buildError("getprintData", "无效的打印状态");
        }
        else if (!mLOPRTManagerSchema.getStateFlag().equals("0"))
        {
            buildError("getprintData", "该打印请求不是在请求状态");
        }

        mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setDoneTime(PubFun.getCurrentTime());

        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);

        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
        }

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(
                "select ZipCode,PostalAddress from LCAppntInd where polno='" +
                tLCPolDB.getPolNo() + "' and CustomerNo='" +
                tLCPolDB.getAppntNo() + "'");

        if (tExeSQL.mErrors.needDealError() || (tSSRS == null))
        {
            CError.buildErr(this,
                            "查询投保人通讯地址/邮编失败!(保单号:" + tLCPolDB.getPolNo() + ")",
                            mErrors);
        }

        TextTag texttag = new TextTag();
        texttag.add("ZipCode", tSSRS.GetText(1, 1));
        texttag.add("Address", tSSRS.GetText(1, 2));
        texttag.add("PolNo", tLCPolDB.getPolNo());
        texttag.add("RiskCode",
                    ChangeCodetoName.getRiskName(tLCPolDB.getRiskCode()));
        texttag.add("CValiDate", tLCPolDB.getCValiDate());
        texttag.add("AppntName", tLCPolDB.getAppntName());
        texttag.add("BonusGetMode",
                    ChangeCodetoName.getBonusGetModeName(tLCPolDB.
                getBonusGetMode()));
        texttag.add("ManageCom", tLDComSchema.getShortName());

        //texttag.add("ManageCom", ChangeCodetoName.getManageName(tLCPolDB.getManageCom().substring(0,4)));
        texttag.add("AssignDate", tLOBonusPolDB.getSGetDate());
        texttag.add("BonusMoney", tLOBonusPolDB.getBonusMoney() + "元");

        texttag.add("Tel", tLDComSchema.getPhone());

        if (bonusGetMode.equals("1") || bonusGetMode.equals("4")) //红利累计和其他
        {
            LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
            tLCInsureAccDB.setPolNo(tLCPolDB.getPolNo());
            tLCInsureAccDB.setInsuAccNo("000001");
            //2005.1.20杨明注释掉问题代码tLCInsureAccDB.setOtherNo(tLCPolDB.getPolNo());

            double accMoney = 0.0;

            if (tLCInsureAccDB.getInfo() == true)
            {
                accMoney = tLCInsureAccDB.getInsuAccBala();
            }

            if ((tLOBonusPolDB.getBonusFlag() != null) &&
                tLOBonusPolDB.getBonusFlag().equals("0"))
            {
                accMoney = accMoney + tLOBonusPolDB.getBonusMoney();
            }

            double InsuAccBala = accMoney;
            texttag.add("LeaveMoney", InsuAccBala + "元");
            texttag.add("PayFee", "---");
            texttag.add("PayAmnt", "---");
            texttag.add("SumAmnt", "---");
        }

        if (bonusGetMode.equals("3")) //抵交保险费-红利转保费
        {
            texttag.add("PayFee", tLOBonusPolDB.getBonusMoney() + "元");
            texttag.add("LeaveMoney", "---");
            texttag.add("PayAmnt", "---");
            texttag.add("SumAmnt", "---");
        }

        if (bonusGetMode.equals("2")) //-现金
        {
            texttag.add("PayFee", "---");
            texttag.add("LeaveMoney", "---");
            texttag.add("PayAmnt", "---");
            texttag.add("SumAmnt", "---");
        }

        if (bonusGetMode.equals("5")) //增额交清-
        {
            //        LCDutyDB tLCDutyDB =new LCDutyDB();
            //        LCDutySet tLCDutySet =new LCDutySet();
            //        tLCDutyDB.setPolNo(tLCPolDB.getPolNo());
            //        String dutycode="001";
            //
            //        tLCDutySet=tLCDutyDB.query();
            //        for(int i=1;i<=tLCDutySet.size();i++)
            //        {
            //            if(tLCDutySet.get(i).getDutyCode().trim().length()==10)
            //            {
            //                 if(Integer.parseInt(tLCDutySet.get(i).getDutyCode().substring(7,10))>=Integer.parseInt(dutycode))
            //                 {
            //                    dutycode=tLCDutySet.get(i).getDutyCode().substring(7,10);
            //                    System.out.println("dutycode:"+dutycode);
            //                    PayAmnt=tLCDutySet.get(i).getAmnt();
            //                 }
            //            }
            //        }
            double PayAmnt = calAmnt(tLCPolDB.getSchema(),
                                     tLOBonusPolDB.getSchema());

            if (PayAmnt <= 0)
            {
                throw new Exception("增额交清计算有误:" + mErrors.getFirstError());
            }

            double SumAmnt = 0;

            //如果未分配
            if ((tLOBonusPolDB.getBonusFlag() == null) ||
                tLOBonusPolDB.getBonusFlag().equals("0"))
            {
                SumAmnt = tLCPolDB.getAmnt() + PayAmnt;
            }
            else
            {
                //已经分配
                SumAmnt = tLCPolDB.getAmnt();
            }

            texttag.add("PayAmnt", PayAmnt + "元");
            texttag.add("SumAmnt", SumAmnt + "元");
            texttag.add("LeaveMoney", "---");
            texttag.add("PayFee", "---");
        }

        //    SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
        //    texttag.add("Today", df.format(new Date()));
        if (texttag.size() > 0)
        {
            xmlExport.addTextTag(texttag);
        }

        mResult.clear();
        mResult.addElement(xmlExport);
    }

    private boolean saveData(VData mInputData)
    {
        //根据印刷号查询打印队列中的纪录
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);

        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");

            return false;
        }

        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setDoneDate(CurrentDate);
        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
        tLOPRTManagerDB.setSchema((LOPRTManagerSchema) mInputData.
                                  getObjectByObjectName(
                                          "LOPRTManagerSchema", 0));

        mResult.add(mLOPRTManagerSchema);
        mResult.add(tLOPRTManagerDB);

        BonusNoticeBLS tBonusNoticeBLS = new BonusNoticeBLS();
        tBonusNoticeBLS.submitData(mResult, mOperate);

        if (tBonusNoticeBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tBonusNoticeBLS.mErrors);
            buildError("saveData", "提交数据库出错！");

            return false;
        }

        return true;
    }

    /**
     * 计算增额交清
     * @param tLCPolSchema
     * @param tLOBonusPolSchema
     * @return
     */
    private double calAmnt(LCPolSchema tLCPolSchema,
                           LOBonusPolSchema tLOBonusPolSchema)
    {
        int PolYear = PubFun.calInterval(tLCPolSchema.getCValiDate(),
                                         tLOBonusPolSchema.getSGetDate(), "Y");

        //int ActuAge=PubFun.calInterval(tLCPolSchema.getInsuredBirthday(),tLOBonusPolSchema.getSGetDate(),"Y");
        int ActuAge = tLCPolSchema.getInsuredAppAge();
        CalBase mCalBase = new CalBase();
        mCalBase.setPrem(tLOBonusPolSchema.getBonusMoney());
        mCalBase.setAppAge(ActuAge);
        mCalBase.setSex(tLCPolSchema.getInsuredSex());
        mCalBase.setPayEndYear(tLCPolSchema.getPayEndYear());
        mCalBase.setPayEndYearFlag(tLCPolSchema.getPayEndYearFlag());
        mCalBase.setPayIntv(0); //都是趸交

        Calculator mCalculator = new Calculator();
        LMRiskBonusDB tLMRiskBonusDB = new LMRiskBonusDB();
        tLMRiskBonusDB.setRiskCode(tLCPolSchema.getRiskCode());

        if (tLMRiskBonusDB.getInfo() == false)
        {
            CError tError = new CError();
            tError.moduleName = "AssignBonus";
            tError.functionName = "getByAddPrem";
            tError.errorMessage = "没有找到增额交清对应的险种描述项";
            this.mErrors.addOneError(tError);

            return 0.0;
        }

        if (tLMRiskBonusDB.getAddAmntCoefCode() == null)
        {
            CError tError = new CError();
            tError.moduleName = "AssignBonus";
            tError.functionName = "getByAddPrem";
            tError.errorMessage = "没有找到增额交清对应的险种算法";
            this.mErrors.addOneError(tError);

            return 0.0;
        }

        mCalculator.setCalCode(tLMRiskBonusDB.getAddAmntCoefCode());

        //增加基本要素
        mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
        mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mCalBase.getSex());
        mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
        mCalculator.addBasicFactor("PayEndYearFlag",
                                   mCalBase.getPayEndYearFlag());
        mCalculator.addBasicFactor("PolYear", String.valueOf(PolYear)); //添加保单年度值
        mCalculator.addBasicFactor("PayIntv", mCalBase.getPayIntv());

        //计算得到新的保额
        String tStr = "";
        double mValue;
        tStr = mCalculator.calculate();

        if (tStr.trim().equals(""))
        {
            mValue = 0;
        }
        else
        {
            mValue = Double.parseDouble(tStr);
        }

        if (mValue == 0)
        {
            return 0.0;
        }

        //格式化，取两位精度-校验
        String strCalPrem = mDecimalFormat.format(mValue); //转换计算后的保费(规定的精度)
        mValue = Double.parseDouble(strCalPrem);

        return mValue;
    }
}
