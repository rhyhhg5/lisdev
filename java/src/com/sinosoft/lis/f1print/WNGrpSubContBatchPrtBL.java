package com.sinosoft.lis.f1print;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * 团单分单打印
 * 
 * @author LY
 *
 */
public class WNGrpSubContBatchPrtBL
{
    private String mOperate;

    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private GlobalInput mGlobalInput = new GlobalInput();

    private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();

    private TransferData mTransferData = new TransferData();

    private int mCount = 0;

    private MMap mMap = new MMap();

    private String mCode = null;

    public WNGrpSubContBatchPrtBL()
    {
    }

    /**
     * 传输数据的公共方法
     * 
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        cInputData = (VData) cInputData.clone();

        this.mOperate = cOperate;
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    private boolean getInputData(VData cInputData)
    {
        mLOPRTManagerSet = (LOPRTManagerSet) cInputData.getObjectByObjectName("LOPRTManagerSet", 0);
        if (mLOPRTManagerSet == null)
        {
            buildError("getInputData", "未找到打印清单。");
            return false;
        }

        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mCode = (String) mTransferData.getValueByName("Code");
        if (mCode == null || mCode.equals(""))
        {
            buildError("getInputData", "打印类型出错。");
            return false;
        }

        return true;
    }

    private boolean checkdata()
    {
        return true;
    }

    public int getCount()
    {
        return mCount;
    }

    /**
     * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if (!prepareOutputData())
        {
            return false;
        }

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(mLOPRTManagerSet);
        PDFPrintBatchManagerBL tPDFPrintBatchManagerBL = new PDFPrintBatchManagerBL();
        if (!tPDFPrintBatchManagerBL.submitData(tVData, mOperate))
        {
            mErrors.copyAllErrors(tPDFPrintBatchManagerBL.mErrors);
            return false;
        }

        return true;
    }

    private boolean prepareOutputData()
    {
        String tOtherNoType = "16";

        for (int i = 1; i <= mLOPRTManagerSet.size(); i++)
        {
            LOPRTManagerSchema tTmpPrtMng = mLOPRTManagerSet.get(i);
            tTmpPrtMng.setOtherNoType(tOtherNoType);
            tTmpPrtMng.setCode(mCode);
        }

        return true;
    }

    public void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GrpSubContBatchPrtBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
