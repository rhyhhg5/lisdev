package com.sinosoft.lis.f1print;

import java.io.InputStream;
import java.text.DecimalFormat;

import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class LLGrpAgentComNotCardClaimReportCSVBL {
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 全局变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mManageCom = "";

	private String mStartDate = "";

	private String mEndDate = "";

	//private String mCaseType = "";

	private String mOperator = "";

	private String tCurrentDate = "";

	private VData mInputData = new VData();

	private String mOperate = "";

	private PubFun mPubFun = new PubFun();

	private ListTable mListTable = new ListTable();

	private TransferData mTransferData = new TransferData();

	private XmlExport mXmlExport = null;

	private String mManageComNam = "";

	private String mFileNameB = "";

	private CSVFileWrite mCSVFileWrite = null;

	private String mFilePath = "";

	private String mFileName = "";
	
	private String mAgentCom = "";
	
	private String mACType = "";
	
	private String mAgentComState = "";
	
	private String mSignDate = "";
	
	private String EndSignDate = "";

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		mOperate = cOperate;
		mInputData = (VData) cInputData;
		if (mOperate.equals("")) {
			this.bulidError("submitData", "数据不完整");
			return false;
		}

		if (!mOperate.equals("PRINT")) {
			this.bulidError("submitData", "数据不完整");
			return false;
		}

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(mInputData)) {
			return false;
		}
		System.out.println("BBBXXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
				+ this.mManageCom);

		// 进行数据查询
		if (!queryDataToCVS()) {
			return false;
		}

		System.out.println("dayinchenggong1232121212121");

		return true;
	}

	private void bulidError(String cFunction, String cErrorMsg) {

		CError tCError = new CError();

		tCError.moduleName = "LLCasePolicy";
		tCError.functionName = cFunction;
		tCError.errorMessage = cErrorMsg;

		this.mErrors.addOneError(tCError);

	}

	/**
	 * 取得传入的数据 如果没有传入结案日期则查询出的为保费收入所对应的全部已结案赔款额
	 * 
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		// tCurrentDate = mPubFun.getCurrentDate();
		try {
			mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
			mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
			// 页面传入的数据 
			mOperator = (String) mTransferData.getValueByName("tOperator");
			mFileNameB = (String) mTransferData.getValueByName("tFileNameB");

			this.mManageCom = (String) mTransferData.getValueByName("MngCom");

			System.out.println("XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
					+ this.mManageCom);
			this.mAgentCom = (String) mTransferData.getValueByName("AgentCom"); //中介机构
			this.mACType = (String) mTransferData.getValueByName("ACType");  //中介机构类型
			String tAgentComState = (String) mTransferData.getValueByName("AgentComState");  //中介机构状态
			if(tAgentComState.equals("1") || tAgentComState == "1"){
				mAgentComState = "N";
			}else if(tAgentComState.equals("2") || tAgentComState == "2"){
				mAgentComState = "Y";
			}else{
				mAgentComState = tAgentComState;
			}
			
			this.mSignDate = (String) mTransferData.getValueByName("SignDate");  //财务结算起期
			this.EndSignDate = (String) mTransferData.getValueByName("EndSignDate");  //财务结算止期
			this.mStartDate = (String) mTransferData.getValueByName("StartDate");// 结案起期
			this.mEndDate = (String) mTransferData.getValueByName("EndDate");// 统结案止期

			if (mManageCom == null || mManageCom.equals("")) {
				this.mErrors.addOneError("管理机构获取失败");
				return false;
			}
			mManageComNam = getManagecomName(mManageCom);

			System.out
					.println("XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXXLLLLLLLLLLLLLLLLLLLL"
							+ mManageCom);
		} catch (Exception ex) {
			this.mErrors.addOneError("");
			return false;
		}
		return true;
	}

	private boolean queryDataToCVS() {

		int datetype = -1;
		LDSysVarDB tLDSysVarDB = new LDSysVarDB();
		tLDSysVarDB.setSysVar("LPCSVREPORT");

		if (!tLDSysVarDB.getInfo()) {
			buildError("queryData", "查询文件路径失败");
			return false;
		}

		 mFilePath = tLDSysVarDB.getSysVarValue();
//		 本地测试
//		mFilePath = "E:\\lisdev\\ui\\vtsfile\\";
		if (mFilePath == null || "".equals(mFilePath)) {
			buildError("queryData", "查询文件路径失败");
			return false;
		}
		System.out.println("XXXXXXXXXXXXXXXXXXXmFilePath:" + mFilePath);
		String tTime = PubFun.getCurrentTime3().replaceAll(":", "");
		String tDate = PubFun.getCurrentDate2();

		mFileName = "WJAJSXBB" + mGlobalInput.Operator + tDate + tTime;
		mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
		
		String[][] tTitle = new String[4][];
		tTitle[0] = new String[] { "中介机构团单(不包含卡折)业务赔款明细报表(团险中介)" };
		tTitle[1] = new String[] { "机构：" + mManageComNam,
				"统计时间：" + mSignDate + "至" + EndSignDate };
		tTitle[2] = new String[] { "制表人：" + mOperator, 
				"制表时间：" + mPubFun.getCurrentDate() };
		tTitle[3] = new String[] { "管理机构名称", "管理机构代码", "中介机构名称", "中介机构代码",
				"中介机构类型", "险种名称","险种编码","保费收入", "结案赔款" };
		
		String[] tContentType = { "String", "String", "String", "String", 
				"String", "String", "String", "Number","Number" };

		if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
			return false;
		}
		if (!getDataListToCVS(datetype)) {
			return false;
		}

		mCSVFileWrite.closeFile();

		return true;
	}

	/*
	 * 统计案件结果
	 */
	private boolean getDataListToCVS(int datetype) {
		ExeSQL tExeSQL = new ExeSQL();
		boolean cardFlag = false;  //判断是否属于卡折

//		String sql = "select (select name from ldcom where comcode=a.managecom),a.managecom,a.name,"
//				+ "a.agentcom,(select codename from ldcode where codetype = 'actype' and code=a.actype),"
//				+ "(select riskname from lmriskapp where riskcode = b.riskcode),b.riskcode,sum(d.prem),sum(c.realpay) "
//				+ "from lacom a, lcgrppol b, llclaimdetail c, lcgrpcont d, llcase e "
//				+ "where a.agentcom=b.agentcom and b.SaleChnl='03' and b.grppolno=c.grppolno " 
//				+ "and b.managecom=a.managecom and b.grpcontno=d.grpcontno "
//				+ "and e.rgtstate in ('09','11','12') and c.caseno=e.caseno and c.rgtno=e.rgtno "
//				+ "and a.managecom like '"+mManageCom+"%' "
//				+ "and d.SignDate between '"+mSignDate+"' and '"+EndSignDate+"' "
//				+ "and (d.CardFlag not in('2','3') or d.CardFlag is null) "
//				+ getEndFlag(mAgentComState)
//				+ getAgentCom(mAgentCom)
//				+ getACType(mACType)
//				+ getEndCaseDate(mStartDate,mEndDate)
//				+ "group by a.managecom,a.name,a.agentcom,a.actype,b.riskcode "
//				+ "union all "
//				+ "select (select name from ldcom where comcode=a.managecom),a.managecom,a.name,"
//				+ "a.agentcom,(select codename from ldcode where codetype = 'actype' and code=a.actype),"
//				+ "(select riskname from lmriskapp where riskcode = b.riskcode),b.riskcode,sum(d.prem),sum(c.realpay) "
//				+ "from lacom a, lbgrppol b, llclaimdetail c, lbgrpcont d, llcase e "
//				+ "where a.agentcom=b.agentcom and b.SaleChnl='03' and b.grppolno=c.grppolno " 
//				+ "and b.managecom=a.managecom and b.grpcontno=d.grpcontno "
//				+ "and e.rgtstate in ('09','11','12') and c.caseno=e.caseno and c.rgtno=e.rgtno "
//				+ "and a.managecom like '"+mManageCom+"%' "
//				+ "and d.SignDate between '"+mSignDate+"' and '"+EndSignDate+"' "
//				+ "and (d.CardFlag not in('2','3') or d.CardFlag is null) "
//				+ getEndFlag(mAgentComState)
//				+ getAgentCom(mAgentCom)
//				+ getACType(mACType)
//				+ getEndCaseDate(mStartDate,mEndDate)
//		        + "group by a.managecom,a.name,a.agentcom,a.actype,b.riskcode with ur";
		
		String sql = "select tname,managecom,name,agentcom,codename,riskwrapcode,wrapname,sum(prem) ,NVL(sum(realpay),0) from( "
			+"select (select name from ldcom where comcode = a.managecom) tname,a.managecom managecom,a.name name,a.agentcom agentcom,(select codename from ldcode where codetype = 'actype' and code = a.actype) codename,(select riskname from lmriskapp where riskcode = b.riskcode) riskwrapcode,b.riskcode wrapname,sum(b.prem)  prem,"
			+ "(select value(sum(realpay),0) from llclaimdetail c, llcase g where c.caseno = g.caseno and c.rgtno = g.rgtno and g.rgtstate in ('09', '11', '12') and b.grpcontno = c.grpcontno and b.riskcode = c.riskcode "+getEndCaseDate(mStartDate, mEndDate)+" ) realpay "
			+ "from lacom a, lcgrppol b, lcgrpcont d "
			+ "where a.agentcom=b.agentcom and b.SaleChnl='03'  " 
			+ "and b.managecom=a.managecom and b.grpcontno=d.grpcontno "				
			+ "and a.managecom like '"+mManageCom+"%' "
			+ "and d.SignDate between '"+mSignDate+"' and '"+EndSignDate+"' "
			+ "and (d.CardFlag not in('2','3') or d.CardFlag is null) "
			+ getEndFlag(mAgentComState)
			+ getAgentCom(mAgentCom)
			+ getACType(mACType)
			+ "group by a.managecom,a.name,a.agentcom,a.actype,b.riskcode,b.grpcontno  " 
            + "union all "
            +" select (select name from ldcom where comcode = a.managecom) tname,a.managecom managecom,a.name name,a.agentcom agentcom,(select codename from ldcode where codetype = 'actype' and code = a.actype) codename,(select riskname from lmriskapp where riskcode = b.riskcode) riskwrapcode,b.riskcode wrapname,sum(b.prem) prem,"
			+ "(select value(sum(realpay),0) from llclaimdetail c, llcase g where c.caseno = g.caseno and c.rgtno = g.rgtno and g.rgtstate in ('09', '11', '12') and b.grpcontno = c.grpcontno and b.riskcode = c.riskcode "+getEndCaseDate(mStartDate, mEndDate)+" ) realpay "
			+ "from lacom a, lbgrppol b, lbgrpcont d "
			+ "where a.agentcom=b.agentcom and b.SaleChnl='03'  " 
			+ "and b.managecom=a.managecom and b.grpcontno=d.grpcontno "				
			+ "and a.managecom like '"+mManageCom+"%' "
			+ "and d.SignDate between '"+mSignDate+"' and '"+EndSignDate+"' "
			+ "and (d.CardFlag not in('2','3') or d.CardFlag is null) "
			+ getEndFlag(mAgentComState)
			+ getAgentCom(mAgentCom)
			+ getACType(mACType)
			+ "group by a.managecom,a.name,a.agentcom,a.actype,b.riskcode,b.grpcontno  )as aa"
            +" group by tname,managecom,name,agentcom,codename,riskwrapcode,wrapname " 
            +" with ur";
		System.out.println("SQL:" + sql);

		RSWrapper rsWrapper = new RSWrapper();
		if (!rsWrapper.prepareData(null, sql)) {
			System.out.println("准备数据失败! ");
			return false;
		}

		try {
			SSRS tSSRS = new SSRS();
			do {
				tSSRS = rsWrapper.getSSRS();
				System.out.println("tSSRS的长度为："+tSSRS.MaxRow);
				if (tSSRS != null && tSSRS.MaxRow > 0) {

					String tContent[][] = new String[tSSRS.getMaxRow() + 1][];
					String ListSum[] = new String[] { "", "", "", "", "", "","", "", "" };
					DecimalFormat tDF = new DecimalFormat("0.##");
					double tListSum7 = 0;
					double tListSum8 = 0;
					ListSum[0] = "合计";
					for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
						String ListInfo[] = new String[9];
						// 机构名称
						ListInfo[0] = tSSRS.GetText(i, 1);
						// 机构编码
						ListInfo[1] = tSSRS.GetText(i, 2);
						//中介机构名称
						ListInfo[2] = tSSRS.GetText(i, 3);
						//中介机构编码
						ListInfo[3] = tSSRS.GetText(i, 4);
						//中介机构类型
						ListInfo[4] = tSSRS.GetText(i, 5);
						//险种名称
						ListInfo[5] = tSSRS.GetText(i, 6);
                        //险种编码
						ListInfo[6] = tSSRS.GetText(i, 7);
						//保费收入
						ListInfo[7] = tSSRS.GetText(i, 8);
						tListSum7 += Double.parseDouble(tSSRS.GetText(i, 8));
						//结案赔款
						ListInfo[8] = tSSRS.GetText(i, 9);
						tListSum8 += Double.parseDouble(tSSRS.GetText(i, 9));

						tContent[i - 1] = ListInfo;
						if (i == tSSRS.getMaxRow()) {
							ListSum[7] = tDF.format(tListSum7);
							ListSum[8] = tDF.format(tListSum8);
							tContent[i] = ListSum;

						}
					}

					mCSVFileWrite.addContent(tContent);
					if (!mCSVFileWrite.writeFile()) {
						mErrors.addOneError(mCSVFileWrite.mErrors
								.getFirstError());
						return false;
					}

				}else{
					String tContent[][] = new String[1][];
					String ListInfo[]={"","","","","","","","",""};
					ListInfo[0]=" ";
					ListInfo[1]=" ";
					ListInfo[2]=" ";
					ListInfo[3]=" ";
					ListInfo[4]=" ";
					ListInfo[5]=" ";
					ListInfo[6]=" ";
					ListInfo[7]=" ";
					ListInfo[8]=" ";
					
					tContent[0] = ListInfo;
					mCSVFileWrite.addContent(tContent);
					if (!mCSVFileWrite.writeFile()) {
						mErrors.addOneError(mCSVFileWrite.mErrors
								.getFirstError());
						return false;
					}
				}
			} while (tSSRS != null && tSSRS.MaxRow > 0);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			rsWrapper.close();
		}
		return true;
	}
	
	//中介机构状态
	private String getEndFlag(String agentComState){
		String sql = "";
		if(agentComState != null && !agentComState.equals("")){
			sql = "and a.endflag='"+agentComState+"' ";
		}
		
		return sql;
	}
	
	//中介机构
	private String getAgentCom(String agentCom){
		String sql = "";
		if(agentCom != null && !agentCom.equals("")){
			sql = "and a.agentcom='"+agentCom+"' ";
		}
		
		return sql;
	}
	
	//中介机构类型
	private String getACType(String actype){
		String sql = "";
		if(actype != null && !actype.equals("")){
			sql = "and a.actype='"+actype+"' ";
		}
		
		return sql;
	}	
	
//	结案日期
	private String getEndCaseDate(String startDate, String endDate){
		String sql = "";
		System.out.println("getEndCaseDate");
		if(startDate != null && !startDate.equals("") && endDate != null && !endDate.equals("")){
			sql = "and endcasedate between '"+ startDate +"' and '"+ endDate +"' ";
		}
		
		return sql;
	}

	/**
	 * @return VData
	 */
	public VData getResult() {
		return mResult;
	}

	public String getMFilePath() {
		return mFilePath;
	}

	public void setMFilePath(String filePath) {
		mFilePath = filePath;
	}

	public String getMFileName() {
		return mFileName;
	}

	public void setMFileName(String fileName) {
		mFileName = fileName;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLCasePolicyBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	/**
	 * 得到xml的输入流
	 * 
	 * @return InputStream
	 */
	public InputStream getInputStream() {
		return mXmlExport.getInputStream();
	}

	public String getManagecomName(String com) {
		String sq = "select name from ldcom where comcode='" + com
				+ "' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tGrpTypeSSRS = tExeSQL.execSQL(sq);
		return tGrpTypeSSRS.GetText(1, 1);
	}
	public String getFileName() {
		return mFileName;
	}

}
