package com.sinosoft.lis.f1print;

/**
 * <p>Title: TContBenExcelBL</p>
 * <p>Description:保监会大病利润表 </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: sinosoft</p>
 * @author :  y
 * @date:2013-11-11
 * @version 1.0
 */
import java.math.BigDecimal;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class TContBenExcelBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	private String mDay[] = null; // 获取时间
	//取得文件路径
	private String tOutXmlPath="";
	private GlobalInput mGlobalInput = new GlobalInput(); // 全局变量

	public TContBenExcelBL() {
	}

	public static void main(String[] args) {
		GlobalInput tG = new GlobalInput();
		tG.Operator = "001";
		tG.ManageCom = "86110000";
		VData vData = new VData();
		String[] tDay = new String[4];
		tDay[0] = "2006-03-01";
		tDay[1] = "2006-03-31";
		vData.addElement(tDay);
		vData.addElement(tG);

		TContBenExcelBL tC = new TContBenExcelBL();
		tC.submitData(vData, "PRINT");

	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("PRINT")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		if (!getInputData(cInputData)) {
			return false;
		}
		if (cOperate.equals("PRINT")) // 打印收费
		{
			if (!getPrintData()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) { // 打印付费
		// 全局变量
		System.out.println("start TContBenExcelBL...TContBen");
		mDay = (String[]) cInputData.get(0);
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        tOutXmlPath=(String)cInputData.get(2);
		
		System.out.println("打印日期："+mDay[0]+"至"+mDay[1]);
		
		if (tOutXmlPath == null || tOutXmlPath=="") {
			buildError("getInputData", "没有获得文件路径信息！");
			return false;
		}
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "TContPreExcelBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
	
	/**
	 * 新的打印数据方法
	 * 
	 * @return
	 */
	private boolean getPrintData() {
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();

		String nSql = "SELECT DISTINCT CODE FROM FICODETRANS WHERE CODETYPE='ContNo'";
		tSSRS = tExeSQL.execSQL(nSql);
		if(tSSRS.getMaxRow() == 0){
			buildError("ContPreExcelBL", "没有符合打印条件的保单数据");
		}
		String ContNo = "";
		for(int c = 1;c <= tSSRS.getMaxRow(); c++){
			if(c == 1){
				ContNo = "'"+tSSRS.GetText(c, 1)+"'";
			}else{
				ContNo = ContNo+",'"+tSSRS.GetText(c, 1)+"'";
			}
		}
		System.out.println("大病保单为："+ContNo);
		
		String[][] mToExcel = new String[10000][100];
		mToExcel[0][0] = "保险公司大病保险业务利润表";
		mToExcel[1][0] = "报送单位：";
		mToExcel[1][1] = "_________";
		mToExcel[1][2] = "保险公司";
		mToExcel[2][0] = "报表时段：";
		mToExcel[2][1] = "_________";
		mToExcel[2][2] = "    年";
		mToExcel[2][3] = "_________";
		mToExcel[2][4] = "季";
		mToExcel[3][0] = "单位：万元";
		mToExcel[4][0] = "    项目";
		mToExcel[4][1] = "横行序号";
		mToExcel[5][0] = "一、已赚保费(=2+3-4-5)";
		mToExcel[5][1] = "1";
		mToExcel[6][0] = "原保险保费收入";
		mToExcel[6][1] = "2";
		mToExcel[7][0] = "分保费收入";
		mToExcel[7][1] = "3";
		mToExcel[8][0] = "分出保费";
		mToExcel[8][1] = "4";
		mToExcel[9][0] = "提取未到期责任准备金";
		mToExcel[9][1] = "5";
		mToExcel[10][0] = "二、保险业务支出(=7-8+9-10+11+12-13)";
		mToExcel[10][1] = "6";
		mToExcel[11][0] = "赔付支出";
		mToExcel[11][1] = "7";
		mToExcel[12][0] = "摊回赔付支出";
		mToExcel[12][1] = "8";
		mToExcel[13][0] = "提取未决赔款准备金";
		mToExcel[13][1] = "9";
		mToExcel[14][0] = "摊回未决赔款准备金";
		mToExcel[14][1] = "10";
		mToExcel[15][0] = "分保费用";
		mToExcel[15][1] = "11";
		mToExcel[16][0] = "业务及管理费";
		mToExcel[16][1] = "12";
		mToExcel[17][0] = "摊回分保费用";
		mToExcel[17][1] = "13";
		mToExcel[18][0] = "三、承保利润(=1-6)";
		mToExcel[18][1] = "14";
		mToExcel[19][0] = "四、分摊的投资收益";
		mToExcel[19][1] = "15";
		mToExcel[20][0] = "五、经营利润(=14+15)";
		mToExcel[20][1] = "16";
		
		String sql = "select manage,"
			+"NVL(sum(case accountcode when '6031000000' then summon else 0 end),0),"
			+"NVL(sum(cessprem),0), "
			+"NVL(sum(case accountcode when '6031000000' then 0 else -summon end),0),"
			+"NVL(sum(ClaimBackFee),0), "
			+"NVL(sum(Reprocfee),0) from("
			+"select (select codename from ficodetrans where codetype='FXManageCom' and code=a.managecom fetch first row only) as manage,"
			+"accountcode as accountcode,"
			+"sum(case finitemtype when 'C' then summoney else -summoney end) as summon, "
			+"(select NVL(sum(cessprem),0) from lrcesslist b where b.getdatadate =a.accountdate and b.grpcontno=a.contno) as cessprem, "
			+"(select NVL(sum(ClaimBackFee),0) from lrclaimlist c where c.getdatadate =a.accountdate and c.grpcontno=a.contno) as ClaimBackFee, "
			+"(select NVL(sum(Reprocfee),0) from lrcesslist d where d.getdatadate =a.accountdate and d.grpcontno=a.contno) as Reprocfee "
			+"from fivoucherdatadetail a where 1=1 "
			+"and a.accountdate between '"+mDay[0]+"' and '"+mDay[1]+"' " 
			+"and a.contno in ("+ContNo+") "
			+"and (a.accountcode ='6031000000' or a.accountcode like '651101%') "
			+"and a.riskcode in('161201','161401','161101') "
			+"group by managecom,accountcode,accountdate,contno )as aa group by manage";
		tSSRS = tExeSQL.execSQL(sql);
		int maxrow=tSSRS.getMaxRow();
		String managecom = "";
		for (int row = 1; row <= maxrow; row++)
        {
            for (int col = 1; col <=6; col++)
            { 
            	if (col==1) {
            		managecom = tSSRS.GetText(row, col); ;
            		mToExcel[4][2+row-1] = managecom.substring(4, managecom.length()-3); 
            	}
            	if (col==2){
            		String bf = tSSRS.GetText(row, col);
            		mToExcel[6][2+row-1]=divide(bf, "10000", 2);
				}
            	if (col==3){
            		String fbf = tSSRS.GetText(row, col);
            		mToExcel[8][2+row-1]=divide(fbf, "10000", 2);
            	}
            	if (col==4){
            		String lp = tSSRS.GetText(row, col);
            		mToExcel[11][2+row-1]=divide(lp, "10000", 2);
            	}
            	if (col==5){
            		String blp = tSSRS.GetText(row, col);
            		mToExcel[12][2+row-1]=divide(blp, "10000", 2);
            	}
            	if (col==6){
            		String bbf = tSSRS.GetText(row, col);
            		mToExcel[17][2+row-1]=divide(bbf, "10000", 2);
            	}
            }
//            System.out.println(""+managecom.substring(4, managecom.length()-3));
        }
		try
        {
            System.out.println("TContBen--tOutXmlPath:"+tOutXmlPath);
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName = { PubFun.getCurrentDate() };
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(tOutXmlPath);
            System.out.println("生成TContBen文件完成");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("dealData", ex.toString());
            return false;
        }
	return true;
}

	public double divide(double v1,double v2,int scale, int round_mode){
		if(scale < 0){
		throw new IllegalArgumentException("The scale must be a positive integer or zero");
		}
		BigDecimal b1 = new BigDecimal(Double.toString(v1));
		BigDecimal b2 = new BigDecimal(Double.toString(v2));

		return b1.divide(b2, scale, round_mode).doubleValue();
	}


	public String divide(String s1,String s2, int scale){
		String temp = "";
		if(s1.equals("")|| s1=="null"){
			temp = "0.00";
		}else{
			double v1=Double.parseDouble(s1);
			double v2=Double.parseDouble(s2);
			double v= divide(v1, v2, scale, BigDecimal.ROUND_HALF_EVEN);
			temp=String.valueOf(v);
			String c=temp.replace(".", ",");
			String[] arr=c.split(",");
			if("0".equals(arr[1])){
				temp+="0";
			}
			if(arr[1].length()==1 && !arr[1].equals("0")){
				temp+="0";
			}
		}
		return temp;
	}

}
