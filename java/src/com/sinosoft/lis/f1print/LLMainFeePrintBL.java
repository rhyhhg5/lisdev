package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.f1print.*;
import java.util.*;
import com.sinosoft.lis.bq.*;

public class LLMainFeePrintBL implements PrintService {
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private VData mResult = new VData();

    private LLConsultSchema mLLConsultSchema = new LLConsultSchema();
    private LLConsultDB mLLConsultDB = new LLConsultDB();
    private LLConsultSet mLLConsultSet = new LLConsultSet();
    private LLMainAskSchema mLLMainAskSchema = new LLMainAskSchema();
    private LDPersonSchema tLDPersonSchema = new LDPersonSchema();
    private LLAnswerInfoSet tLLAnswerInfoSet = new LLAnswerInfoSet();
    private LLAnswerInfoSchema tLLAnswerInfoSchema = new LLAnswerInfoSchema();
    private SSRS mSSRS;
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    //交费信息
    private int needPrt = -1;
    private String mOperate = "";
    private String showPaymode = "";
    private String strPayToDate = "";
    private String strPayDate = "";
    private String mDif = "";
    private String strPayCanDate = "";
    TextTag textTag = new TextTag(); //新建一个TextTag的实例tLJSPaySchema

    private Object tExeSQL;
    public LLMainFeePrintBL() {

    }


    //By WangJH

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        System.out.println("IndiDueFeePrintBL begin");
        if (!cOperate.equals("PRINT") && !cOperate.equals("INSERT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        System.out.println("1,end");
        mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("2,end");

         mResult.clear();
//        if (!getListData()) {
//            return false;
//        }

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        System.out.println("3,end");

        if (cOperate.equals("INSERT") && needPrt == 0) {
            if (!dealPrintMag()) {
                return false;
            }
        }

        System.out.println("LLMainFeePrintBL end");
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();
        ttLOPRTManagerSchema = (LOPRTManagerSchema)cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);
        if(ttLOPRTManagerSchema == null)
        {
            mLLConsultDB.setSchema((LLConsultDB) cInputData.
                                   getObjectByObjectName(
                                           "LLConsultDB", 0));
            System.out.println(mLLConsultDB.getSchema());
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput",
                    0);
            System.out.println(mGlobalInput);
            if (mLLConsultDB == null) {
                buildError("getInputData", "没有得到足够的信息！");
                return false;
            }

            if (mLLConsultDB.getConsultNo() == null ||
                mLLConsultDB.getConsultNo().equals("")) {
                buildError("getInputData", "没有得到足够的信息:咨询号不能为空！");
                return false;
            }
        }else {
            mLLConsultDB.setConsultNo(ttLOPRTManagerSchema.getStandbyFlag2());
//            if(!mLLConsultDB.getInfo())
//            {
//                mErrors.addOneError("PDF入口LJSPayBDB传入的数据不完整。");
//                return false;
//            }
        }
        return true;

    }

    public VData getResult() {
        return this.mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }


    private boolean getPrintData() {
        boolean PEFlag = false; //打印理赔咨询通知的判断标志
        boolean shortFlag = false; //打印备注信息
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        //根据印刷号查询打印队列中的纪录
        String aConsultNo = mLLConsultDB.getConsultNo();
        System.out.println(aConsultNo);
        String alogNo = "";
        String aCustomerNo = "";

        //LLConsultDB mLLConsultDB = new LLConsultDB();
        mLLConsultDB.setConsultNo(aConsultNo);
        //mLLCaseDB.setRgtNo(aRgtNo);

        mLLConsultSet.set(mLLConsultDB.query());
        if (mLLConsultSet == null || mLLConsultSet.size() == 0) {
            PEFlag = false;
        } else {
            mLLConsultSchema.setSchema(mLLConsultSet.get(1));
        }

        alogNo = mLLConsultSchema.getLogNo(); //立案号
        aCustomerNo = mLLConsultSchema.getCustomerNo();

        LLMainAskDB mLLMainAskDB = new LLMainAskDB(); //以下怎么处理
        mLLMainAskDB.setLogNo(alogNo);
        if (mLLMainAskDB.getInfo() == false) {
            mErrors.copyAllErrors(mLLMainAskDB.mErrors);
            buildError("outputXML", "mLLMainAskDB在取得打印队列中数据时发生错误");
            return false;
        }
        mLLMainAskSchema = mLLMainAskDB.getSchema();

        LDPersonDB mLDPersonDB = new LDPersonDB();
        mLDPersonDB.setCustomerNo(aCustomerNo);
        if (mLDPersonDB.getInfo() == false) {
            mErrors.copyAllErrors(mLDPersonDB.mErrors);
            buildError("outputXML", "mLDPersonDB在取得打印队列中数据时发生错误");
            return false;
        }
        tLDPersonSchema.setSchema(mLDPersonDB.getSchema());

        LLAnswerInfoDB mLLAnswerInfoDB = new LLAnswerInfoDB();
        mLLAnswerInfoDB.setConsultNo(aConsultNo);
        mLLAnswerInfoDB.setLogNo(alogNo);
        tLLAnswerInfoSet.set(mLLAnswerInfoDB.query());
        if (tLLAnswerInfoSet == null || tLLAnswerInfoSet.size() == 0) {
            PEFlag = false;
        } else {
            tLLAnswerInfoSchema.setSchema(tLLAnswerInfoSet.get(1));
        }

        //其它模版上单独不成块的信息

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("CsuRplPrt.vts", "printer"); //最好紧接着就初始化xml文档
        //生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
                         tSrtTool.getDay() + "日";

        //PayAppPrtBL模版元素
        //texttag.add("BarCodeParam1","");
        String sql =
                "select codename from LDCode where CodeType='llaskmode' and Code='" +
                mLLMainAskSchema.getAskMode() + "' ";
        ExeSQL execsql = new ExeSQL();
        String AskMode = execsql.getOneValue(sql);
        sql =
                "select codename from LDCode where CodeType='llcuststatus' and Code='" +
                mLLConsultSchema.getCustStatus() + "' ";
        String CustState = execsql.getOneValue(sql);
        String strSQL = "select Username from lduser where usercode='" +
                        mGlobalInput.Operator + "'";
        ExeSQL exesql1 = new ExeSQL();
        String OperName = exesql1.getOneValue(strSQL);

        texttag.add("BarCode1", mLLConsultSchema.getConsultNo());
        texttag.add("BarCodeParam1"
                    , "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("BarCode1", mLLConsultSchema.getConsultNo());
        texttag.add("BarCodeParam1"
                    , "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("CustomerName", mLLConsultSchema.getCustomerName());
        texttag.add("CustomerNo", mLLConsultSchema.getCustomerNo());
        texttag.add("IDNo", tLDPersonSchema.getIDNo());
        texttag.add("Phone", mLLMainAskSchema.getPhone());
        texttag.add("Mobile", mLLMainAskSchema.getMobile());
        texttag.add("Email", mLLMainAskSchema.getEmail());
        texttag.add("PostalAddress", mLLMainAskSchema.getAskAddress());
        texttag.add("ZipCode", mLLMainAskSchema.getPostCode());
        texttag.add("LogComp", mLLMainAskSchema.getLogComp());
        texttag.add("AskMode", AskMode);
        texttag.add("AskType", (mLLMainAskSchema.getAskType() == "0" ? "咨询" : "通知"));
        texttag.add("CustState", CustState);
        texttag.add("CContent", mLLConsultSchema.getCContent());
        texttag.add("Answer", tLLAnswerInfoSchema.getAnswer());
        texttag.add("Operator", OperName);
        texttag.add("SysDate", SysDate);
        if (shortFlag) {
            texttag.add("Supply1", "1、根据保险合同的约定，您所提供的资料不齐，请根据上述提示补齐资料后再行申请；");
            texttag.add("Supply2",
                        "2、本公司审核过程中，如发现您所提交的材料不足以证明保险事故的原因、性质或者程度，本公司将另行发出材料补齐通知，待您补齐相关材料之后再行理赔审核。");
        } else {
            texttag.add("Supply1",
                        "本公司审核过程中，如发现您所提交的材料不足以证明保险事故的原因、性质或者程度，本公司将另行发出材料补齐通知，待您补齐相关材料之后再行理赔审核。");
            texttag.add("Supply2", "");
        }
        ////////////
        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        //保存体检信息
        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        if (mOperate.equals("INSERT")) {
            //放入打印列表
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            tLOPRTManagerDB.setStandbyFlag2(mLLConsultDB.getConsultNo());
            tLOPRTManagerDB.setCode("lp004");
            needPrt = tLOPRTManagerDB.query().size();
            System.out.println("-------------------------the size is " +
                               needPrt + "--------------------------");
            if (needPrt == 0) { //没有数据，进行封装
                String tLimit = PubFun.getNoLimit(mLLConsultSchema.getMngCom());
                String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
                mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
                mLOPRTManagerSchema.setOtherNo(mLLConsultSchema.getCustomerNo());
                mLOPRTManagerSchema.setOtherNoType("09");
                mLOPRTManagerSchema.setCode("lp004");
                mLOPRTManagerSchema.setManageCom(mLLConsultSchema.getMngCom());
               // mLOPRTManagerSchema.setAgentCode(mLLConsultSchemagetAgentCode());
                mLOPRTManagerSchema.setReqCom(mLLConsultSchema.getMngCom());
                mLOPRTManagerSchema.setReqOperator(mLLConsultSchema.getOperator());
                mLOPRTManagerSchema.setPrtType("0");
                mLOPRTManagerSchema.setStateFlag("0");
                mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
                mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
                mLOPRTManagerSchema.setStandbyFlag2(mLLConsultDB.getConsultNo()); //存放咨询信息表(LLconsult)的咨询号码
            }
        }
         return true;
}
    /**
     * 得到险种未续期续保之前的交至日期
     * @return String
     */
    private String getPayToDate() {

        return null;
    }

    /**
     * 查询总期缴保费
     * @return String
     */
//    private String getPrem() {
//        StringBuffer sql = new StringBuffer();
//        sql.append(" select sum(sumDuePayMoney) ")
//                .append("from LJSPayPersonB ")
//                .append("where getNoticeNo = '")
//                .append(tLJSPayBDB.getGetNoticeNo())
//                .append("'  and sumDuePayMoney > 0 ")
//                .append("   and payType = 'ZC' ");
//        ExeSQL e = new ExeSQL();
//        String prem = e.getOneValue(sql.toString());
//        if (prem.equals("") || prem.equals("null")) {
//            CError.buildErr(this, "没有查询到财务数据");
//            return "0.00";
//        }
//        return CommonBL.bigDoubleToCommonString(Double.parseDouble(prem),
//                                                "0.00");
//    }

//    private void setFixedInfo() {
//        LDComDB tLDComDB = new LDComDB();
//        tLDComDB.setComCode(tLCContSchema.getManageCom());
//        tLDComDB.getInfo();
//        textTag.add("ServicePhone", tLDComDB.getServicePhone());
//        textTag.add("letterservicename", tLDComDB.getLetterServiceName());
//        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
//        textTag.add("ServiceZip", tLDComDB.getLetterServicePostZipcode());
//        textTag.add("ComName", tLDComDB.getLetterServiceName());
//        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
//        textTag.add("Fax", tLDComDB.getFax());
//    }

    /**
     *  获取列表数据
     * @param 无
     * @return ListTable
     */
//    private ListTable getListTable() {
//
//        ListTable tListTable = new ListTable();
//        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
//            String[] info = new String[10];
//            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
//                info[j - 1] = mSSRS.GetText(i, j);
//            }
//            tListTable.add(info);
//        }
//        tListTable.setName("ZT");
//        return tListTable;
//    }

    /**
     * 查询列表显示数据
     * @param schema LCContSchema
     * @return String
     */

//    private boolean getListData() {
//        StringBuffer sql = new StringBuffer();
//        sql.append("select a.SignDate ")
//                .append("from LBCont a, LCRnewStateLog b ")
//                .append("where a.contNo = b.newContNo ")
//                .append("   and a.contNo = '")
//                .append(tLJSPayBDB.getOtherNo())
//                .append("' order by b.newContNo ");
//        ExeSQL e = new ExeSQL();
//        String sourceSignDate = e.getOneValue(sql.toString());
//
//        if (sourceSignDate.equals("null")) {
//            sourceSignDate = "";
//        }
//
//        StringBuffer sqlSB = new StringBuffer();
//        sqlSB.append("select distinct a.RiskSeqNo ,(select riskname  from lmrisk where riskcode=a.riskcode) ,a.InsuredName , ")
//                .append("   case when b.FactorValue='1' then trim(to_char(a.Amnt)) || '.00' when b.FactorValue='3' then to_char(a.Mult) end, ");
//        if (sourceSignDate.equals("")) {
//            //sqlSB.append("   (select polApplyDate from LCCont where contNo = a.contNo), ");
//            sqlSB.append("   (select polApplyDate from LCCont where contNo = '")
//                    .append(tLJSPayBDB.getOtherNo()).append("'), ");
//        } else {
//            sqlSB.append("'").append(sourceSignDate).append("', ");
//        }
//        sqlSB.append("   char(c.lastpaytodate)||'～'||char((select max(curpaytodate)- 1 day from ljspaypersonb where polno=a.polno)), ")
//                .append("   (select codeName from LDCode where codeType = 'payintv' and code = char(a.payIntv)), (select varchar(sum(sumDuePayMoney)) from LJSPayPersonB where polNo = c.polNo and getNoticeNo = c.getNoticeNo and sumDuePayMoney >= 0 and payType = 'ZC'),")
//                .append("   case when c.PayTypeFlag = '1' then (SELECT case rnewFlag when 'B' then '保证续保' else '同意续保' end from LMRisk where riskCode = a.riskCode) else '' end, ") //续保说明
//                .append(
//                "   case when c.PayTypeFlag = '1' then '续保保费' else '续期保费' end ")
//                .append("from LCPol a,LDRiskParamPrint b, LJSPayPersonB c ")
//                .append("where a.polNo = c.polNo ")
//                .append("  and a.RiskCode=b.RiskCode")
//                .append("  and c.getNoticeNo = '")
//                .append(tLJSPayBDB.getGetNoticeNo()).append("' ");
//
//        //.append(" group by  b.SerialNo,b.GetNoticeNo,a.GrpName,a.GrpContNo,b.SumDuePayMoney,a.paymode, b.dealstate,b.Operator")
//        //.append(" order by b.GetNoticeNo desc"
//        //只处理续保终止
//        sqlSB.append(" union ")
//                .append(
//                "select a.RiskSeqNo,(select riskname  from lmrisk where riskcode=a.riskcode),")
//                .append("   a.insuredName, ")
//                .append("   (select case min(FactorValue) when '1' then trim(to_char(a.Amnt)) || '.00' when '3' then to_char(a.Mult) end from LDRiskParamPrint where riskCode = a.riskCode), ")
//                .append("   a.polApplyDate, ")
//                .append("   (select char(max(lastPayToDate)) || '～' || char(max(curpaytodate) - 1 day ) from ljApayperson where polno=a.polno), ")
//                .append("   (select codeName from LDCode where codeType = 'payintv' and code = char(a.payIntv)),")
//                .append("   varchar(a.prem), '终止续保', '' ")
//                .append(
//                "from LCPol a, LCRnewStateLog b, LPUWMaster c, LGWork d ")
//                .append("where a.polNo = b.polNo ")
//                .append("   and b.newPolNo = c.polNo ")
//                .append("   and c.edorNo = d.workNo ")
//                .append("   and (c.passFlag = '").append(BQ.PASSFLAG_STOPXB) //续保终止、
//                .append("'  or c.passFlag = '").append(BQ.PASSFLAG_CONDITION) //条件通过且客户不同意则续保终止
//                .append("'     and CustomerReply = '2' and DisagreeDeal='3') ")
////            .append("   and c.passFlag = '").append(BQ.PASSFLAG_STOPXB).append("' ")
////            .append("5' or c.passFlag = '4' and CustomerReply = '2' and DisagreeDeal='3')")
//                .append("   and c.edorType = '")
//                .append(BQ.EDORTYPE_XB).append("' ")
//                .append("   and d.innersource = '")
//                .append(tLJSPayBDB.getGetNoticeNo())
//                .append("' ");
//
//        String tSQL = sqlSB.toString();
//        System.out.println(tSQL);
//        ExeSQL tExeSQL = new ExeSQL();
//
//        mSSRS = tExeSQL.execSQL(tSQL);
//        if (tExeSQL.mErrors.needDealError()) {
//            CError tError = new CError();
//            tError.moduleName = "MakeXMLBL";
//            tError.functionName = "creatFile";
//            tError.errorMessage = "查询XML数据出错！";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
//
//        return true;
//    }

    /**
     * 查询保单余额
     * @param schema LCContSchema
     * @return String
     */

//    private boolean getBale() {
//        //得到账户余额
////        String StrBale = "select accgetmoney from lcappacc where customerno='" +
////                         tLCContSchema.getAppntNo() + "'";
////        ExeSQL tExeSQL = new ExeSQL();
////        String count = tExeSQL.getOneValue(StrBale);
//
////        if (tExeSQL.mErrors.needDealError()) {
////            this.mErrors.addOneError("保单号:" + tLCContSchema.getContNo() +
////                                     "查询余额时错误:" +
////                                     tExeSQL.mErrors.getFirstError());
////
////            return false;
////        }
//
//        String sql = "  select abs(sum(sumActuPayMoney)) "
//                     + "from LJSPayPersonB "
//                     + "where getNoticeNo = '"
//                     + tLJSPayBDB.getGetNoticeNo() + "' "
//                     + "   and payType = 'YEL' ";
//        ExeSQL tExeSQL = new ExeSQL();
//        String count = tExeSQL.getOneValue(sql);
//        if ((count != null) && !count.equals("")) {
//            mDif = CommonBL.bigDoubleToCommonString(Double.parseDouble(count),
//                    "0.00");
//        } else {
//            mDif = "0.00";
//        }
//
//        return true;
//    }

    public static void main(String[] a) {
        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBDB.setGetNoticeNo("31000001965");
        tLJSPayBDB.getInfo();

        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema.setContNo(tLJSPayBDB.getOtherNo());

        GlobalInput tG = new GlobalInput();
        tG.Operator = "pa0001";
        tG.ComCode = "86";

        VData tVData = new VData();
        tVData.addElement(tLCContSchema);
        tVData.addElement(tG);
        tVData.addElement(tLJSPayBDB);

        IndiDueFeePrintBL bl = new IndiDueFeePrintBL();
        if (bl.submitData(tVData, "PRINT")) {
            System.out.println(bl.mErrors.getErrContent());
        }
    }

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealPrintMag() {
        MMap tMap = new MMap();
        tMap.put(mLOPRTManagerSchema, "INSERT");
        VData tVData = new VData();
        tVData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tVData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
            return false;
        }
        return true;
    }

}
