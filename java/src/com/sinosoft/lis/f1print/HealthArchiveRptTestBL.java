package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: </p>
 * @author guoly
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;
import java.text.DecimalFormat;
import com.sinosoft.lis.f1print.*;
import java.lang.*;

public class HealthArchiveRptTestBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LDPersonSchema mLDPersonSchema = new LDPersonSchema();
    private TransferData mTransferData = new TransferData();

    private LHCustomInHospitalSchema mLHCustomInHospitalSchema = new
            LHCustomInHospitalSchema();

    public HealthArchiveRptTestBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("1 ...");
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        this.mLDPersonSchema.setSchema((LDPersonSchema)
                                       cInputData.getObjectByObjectName(
                                               "LDPersonSchema", 0));
        System.out.println(mLDPersonSchema.getCustomerNo());
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        if (mLDPersonSchema == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (mLDPersonSchema.getCustomerNo() == null) {
            buildError("getInputData", "没有得到足够的信息:客户号不能为空！");
            return false;
        }
        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        if (mTransferData == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "HealthArchiveRptTestBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        //统计的起讫时间
        String MStartDate = (String) mTransferData.getValueByName("Begin");
        String MEndDate = (String) mTransferData.getValueByName("End");

        //得到客户基本信息
        String aCustomerNo = "";
        aCustomerNo = mLDPersonSchema.getCustomerNo();
        //体检信息

        ListTable SomiteListTable = new ListTable();
        SomiteListTable.setName("SOMITE");
        String[] Title_D2 = {
                           "INHOSPITMODE", "INHOSPITDATE", "HOSPITNAME", "MEDICAITEMNAME"
        };

        try {
            SSRS tSSRS_D2 = new SSRS();
            String sql2 = "select rownumber() over(),"
                         +
                         "( case a.InHospitMode when  '31' then '核保体检' when '32' then  '体检服务'  else '无' end ),"
                         + "a.InHospitDate,(select HospitName from LDHospital where HospitCode=a.HospitCode) ,"
                         + "(select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                         + " b.TestResult ||' '||(select distinct c.standardmeasureunit from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ),"
                         +" (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = b.MedicaItemCode ) "
                         +" from LHCustomInHospital a ,LHCustomTest b "
                         + " where 1=1 and inhospitmode in ('31','32')  "
                         + " and a.InHospitNo=b.InHospitNo and b.isnormal='异常'"
                         + " and a.CustomerNo = '"+aCustomerNo+ "' "
                         + " and    a.customerno = b.customerno "
                         + " and    (a.InHospitDate between '"+MStartDate+"' and '"+MEndDate+"')"
                        // + " order by a.InHospitDate"
                         ;
           System.out.println(sql2);
            ExeSQL tExeSQL2 = new ExeSQL();
            tSSRS_D2 = tExeSQL2.execSQL(sql2);
            int count_D2 = tSSRS_D2.getMaxRow();
            System.out.println(count_D2);
            for (int i = 0; i < count_D2;i++) {

                String temp_D2[][] = tSSRS_D2.getAllData();

                String[] strCol2;
                strCol2 = new String[7];
                strCol2[0] = temp_D2[i][0];
                strCol2[1] = temp_D2[i][1];
                strCol2[2] = temp_D2[i][2];
                strCol2[3] = temp_D2[i][3];
                strCol2[4] = temp_D2[i][4];
                strCol2[5] = temp_D2[i][5];
                strCol2[6] = temp_D2[i][6];
                System.out.println(
                        "INHOSPITMODE:" + temp_D2[i][1] + ", INHOSPITDATE:" + temp_D2[i][2] +
                        ", HOSPITNAME:" + temp_D2[i][3] + ", MEDICAITEMNAME:" + temp_D2[i][4]);

                SomiteListTable.add(strCol2);
            }

        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "HealthArchiveRptTestBL";
            tError.functionName = "createAddressNo";
            tError.errorMessage = "客户体检信息查询失败";
            this.mErrors.addOneError(tError);
            mLHCustomInHospitalSchema.setInHospitNo("");
        }

        //其它模版上单独不成块的信息

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("HealthArchiveTest.vts", "printer"); //最好紧接着就初始化xml文档
        MStartDate=MStartDate.replaceFirst("-","年");
        MStartDate=MStartDate.replaceFirst("-","月");
        MEndDate=MEndDate.replaceFirst("-","年");
        MEndDate=MEndDate.replaceFirst("-","月");

        texttag.add("MStartDate", MStartDate);
        texttag.add("MEndDate", MEndDate);
        texttag.add("CustomerNo", aCustomerNo);
        SSRS tTask = null;
        String sql1="select name  from ldcom where comcode='"+ mGlobalInput.ManageCom +"'";
        ExeSQL tExeSQL= new ExeSQL();
        tTask=tExeSQL.execSQL(sql1);
        String[][] msgInfo = tTask.getAllData();
        texttag.add("Corp",msgInfo[0][0]);

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }

        //保存信息
        xmlexport.addListTable(SomiteListTable, Title_D2);
        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    public static void main(String[] args) {

        HealthArchiveRptTestBL tHealthArchiveRptTestBL = new HealthArchiveRptTestBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        LDPersonSchema tLDPersonSchema = new LDPersonSchema();
        tLDPersonSchema.setCustomerNo("000000142");
        tVData.addElement(tLDPersonSchema);
        tHealthArchiveRptTestBL.submitData(tVData, "PRINT");
        VData vdata = tHealthArchiveRptTestBL.getResult();

    }

    private void jbInit() throws Exception {
    }

}
