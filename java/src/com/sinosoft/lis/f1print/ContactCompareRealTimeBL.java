package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.util.ArrayList;

import com.sinosoft.lis.db.LCContactDB;
import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContactSchema;
import com.sinosoft.lis.vschema.LCContactSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ContactCompareRealTimeBL {

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors=new CErrors();

  private VData mResult = new VData();
  
  private CreateExcelList mCreateExcelList = new CreateExcelList("");
  
  private TransferData mTransferData = new TransferData();
  
  private GlobalInput mGlobalInput =new GlobalInput();
  
  //对比起始日期
  private String mStartDate="";
  private String mEndDate="";
  private String mManageCom=""; 
  
  //查询出的问题保单
  private String[][] mExcelData=null;
  
  public ContactCompareRealTimeBL() {
  }

/**
  传输数据的公共方法
*/
    public CreateExcelList getsubmitData(VData cInputData, String cOperate)
    {
      if( !cOperate.equals("PRINT")&&!cOperate.equals("GRPPRINT") ) {
        buildError("submitData", "不支持的操作字符串");
        return null;
      }

      // 得到外部传入的数据，将数据备份到本类中
      if( !getInputData(cInputData) ) {
        return null;
      }

      if(cOperate.equals("PRINT")){
      // 准备所有要打印的数据
      if( !PgetPrintData() ) {
        return null;
      }}
      if(cOperate.equals("GRPPRINT")){
          // 准备所有要打印的数据
          if( !GgetPrintData() ) {
            return null;
          }}

      if(mCreateExcelList==null){
    	  buildError("submitData", "Excel数据为空");
          return null;
      }
      return mCreateExcelList;
    }
  
  public static void main(String[] args)
  {
      
	  ContactCompareRealTimeBL tbl =new ContactCompareRealTimeBL();
      GlobalInput tGlobalInput = new GlobalInput();
      TransferData tTransferData = new TransferData();
      tTransferData.setNameAndValue("StartDate", "2010-09-26");
      tTransferData.setNameAndValue("EndDate", "2010-09-27");
      tTransferData.setNameAndValue("ManageCom", "86");
      tGlobalInput.ManageCom="86";
      tGlobalInput.Operator="xp";
      VData tData = new VData();
      tData.add(tGlobalInput);
      tData.add(tTransferData);

      CreateExcelList tCreateExcelList=new CreateExcelList();
      tCreateExcelList=tbl.getsubmitData(tData,"GRPPRINT");
      if(tCreateExcelList==null){
    	  System.out.println("112321231");
      }
      else{
      try{
    	  tCreateExcelList.write("c:\\contactcompare.xls");
      }catch(Exception e)
      {
    	  System.out.println("EXCEL生成失败！");
      }}
  }


  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
//全局变量
	  mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
    if( mGlobalInput==null ) {
      buildError("getInputData", "没有得到足够的信息！");
      return false;
    }
    mTransferData = (TransferData) cInputData.getObjectByObjectName(
			"TransferData", 0);
    mStartDate = (String) mTransferData.getValueByName("StartDate");
    mEndDate = (String) mTransferData.getValueByName("EndDate");
    mManageCom = (String) mTransferData.getValueByName("ManageCom");
    if( mStartDate.equals("")||mEndDate.equals("")||mStartDate==null||mEndDate==null ) {
      buildError("getInputData", "没有得到足够的信息:起始和终止日期不能为空！");
      return false;
    }
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public CErrors getErrors()
  {
    return mErrors;
  }

  private void buildError(String szFunc, String szErrMsg)
  {
    CError cError = new CError( );

    cError.moduleName = "LCGrpContF1PBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }

  private boolean PgetPrintData()
  {

	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"机构代码", "机构名称", "销售渠道","网点代码", "网点", "业务员姓名",
                           "业务员代码", "保单号", "投保单号","归档号", "投保人","保费 ","投保人联系电话","投保人手机","业务员电话","问题说明"}};
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
      //获得EXCEL列信息      
      String tSQL="select a.managecom ,(select name from ldcom where comcode=a.managecom )," +
      		"(select codealias from ldcode1 where codetype ='salechnl' and code=a.salechnl)," +
      		"a.agentcom,(select name from lacom where agentcom = a.agentcom)," +
      		"(select b.Name from laagent b where b.AgentCode=a.AgentCode)," +
      		"a.AgentCode,a.contno,a.prtno," +
      		"(select archiveno from es_doc_main where doccode=a.prtno fetch first 1 rows only)," +
      		"a.appntname,a.prem," +
      		"(select c.Phone from lcaddress c where c.CustomerNo=a.AppntNo  and c.AddressNo = (select AddressNo from lcappnt where contno=a.contno) fetch first 1 row only)," +
      		"(select c.Mobile from lcaddress c where c.CustomerNo=a.AppntNo and c.AddressNo = (select AddressNo from lcappnt where contno=a.contno) fetch first 1 row only)," +
      		"(select phone from laagent where agentcode=a.agentcode),'' from lccont a where conttype='1' and appflag='1' and stateflag='1' " +
      		" and managecom like '"+mManageCom+"%' and signdate between '"+mStartDate+"' and '"+mEndDate+"' with ur ";
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL);
      if (tExeSQL.mErrors.needDealError()) {
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      

      ArrayList tArrayList = new ArrayList();
      
      mExcelData=tSSRS.getAllData();
      if(mExcelData==null){
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }
      
      for (int j = 0; j < mExcelData.length;j++) {
    	  if(!(mExcelData[j][14].equals("")||mExcelData[j][14]==null)){
    		  if((mExcelData[j][12].equals("")||mExcelData[j][12]==null)&&(mExcelData[j][13].equals("")||mExcelData[j][13]==null)){
        		  System.out.println("保单号"+mExcelData[j][7]+"无投保人联系电话信息");
        	  }
    		  else {
    			  if(mExcelData[j][12].equals(mExcelData[j][14])||mExcelData[j][13].equals(mExcelData[j][14])){
        			  mExcelData[j][15]=mExcelData[j][7]+"保险合同的投保人联系电话和业务员电话一致。";  
        		  }
    		  }
    	  }else{
    		  System.out.println("保单号"+mExcelData[j][7]+"无业务员电话信息"); 		  
    	  }
    	  if(!mExcelData[j][15].equals("") && !mExcelData[j][15].equals("null") && mExcelData[j][15] != null)
    	  {
    		  tArrayList.add(mExcelData[j]);
    	  }

      }
      String[][] tExcelData=new String[tArrayList.size()][]; 
      for(int m=0 ;m<tArrayList.size(); m++)
      {
    	  tExcelData[m]=(String[]) tArrayList.get(m);
      }

      
      if(mCreateExcelList.setData(tExcelData,displayData)==-1){
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

    return true;
  }

  private boolean GgetPrintData()
  {

	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"机构代码", "机构名称", "销售渠道", "网点代码","网点 ", "业务员姓名",
                           "业务员代码", "保单号", "投保单号","归档号", "投保人","保费 ","投保人联系电话","投保人手机","业务员电话","问题说明"}};
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
      //获得EXCEL列信息      
      String tSQL="select a.managecom ,(select name from ldcom where comcode=a.managecom ) ," +
      		"(select codealias from ldcode1 where codetype ='salechnl' and code=a.salechnl)," +
      		"a.agentcom,(select name from lacom where agentcom = a.agentcom) ," +
      		"(select b.Name from laagent b where b.AgentCode=a.AgentCode) ,a.AgentCode ,a.grpcontno," +
      		"a.prtno,(select archiveno from es_doc_main where doccode=a.prtno fetch first 1 rows only)," +
      		"a.grpname ,a.prem ," +
      		"(select c.phone1 from lcgrpaddress c where c.CustomerNo=a.AppntNo and c.AddressNo = (select AddressNo from lcgrpappnt where grpcontno=a.grpcontno) fetch first 1 row only) ," +
      		"(select c.mobile1 from lcgrpaddress c where c.CustomerNo=a.AppntNo and c.AddressNo = (select AddressNo from lcgrpappnt where grpcontno=a.grpcontno) fetch first 1 row only)," +
      		"(select phone from laagent where agentcode=a.agentcode),''  from lcgrpcont a where appflag='1' and stateflag='1' " +
      		" and managecom like '"+mManageCom+"%' and signdate between '"+mStartDate+"' and '"+mEndDate+"' with ur ";
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL);
      if (tExeSQL.mErrors.needDealError()) {
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }
      
          
      ArrayList tArrayList = new ArrayList();
      mExcelData=tSSRS.getAllData();
      if(mExcelData==null){
    	  CError tError = new CError();
    	  tError.moduleName = "CreateExcelList";
    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }
      
      for (int j = 0; j < mExcelData.length;j++) {
    	  if(!(mExcelData[j][14].equals("")||mExcelData[j][14]==null)){
    		  if((mExcelData[j][12].equals("")||mExcelData[j][12]==null)&&(mExcelData[j][13].equals("")||mExcelData[j][13]==null)){
        		  System.out.println("保单号"+mExcelData[j][7]+"无联系人联系电话信息");
        	  }
    		  else {
    			  if(mExcelData[j][12].equals(mExcelData[j][14])||mExcelData[j][13].equals(mExcelData[j][14])){
        			  mExcelData[j][15]=mExcelData[j][7]+"保险合同的联系人联系电话和业务员电话一致。";  
        		  }
    		  }
    	  }else{
    		  System.out.println("保单号"+mExcelData[j][7]+"无业务员电话信息"); 		  
    	  }
    	  if(!mExcelData[j][15].equals("") && !mExcelData[j][15].equals("null") && mExcelData[j][15] != null)
    	  {
    		  tArrayList.add(mExcelData[j]);
    	  }
      }
      String[][] tExcelData=new String[tArrayList.size()][];  
      for(int m=0 ;m<tArrayList.size(); m++)
      {
    	  tExcelData[m]=(String[]) tArrayList.get(m);
      }
      if(mCreateExcelList.setData(tExcelData,displayData)==-1){
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }
    return true;
  }
  
  /**
   * 得到通过传入参数得到相关信息
   * @param strComCode
   * @return
   * @throws Exception
   */
  private boolean getExcelData()
  {

	  String tSQL="select * from lccont lcc where conttype='1' and appflag='1' and stateflag='1'" +
	  		" and managecom like '' and signdate between '' and '' with ur ";
	  ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL);
      if (tExeSQL.mErrors.needDealError()) {
          CError tError = new CError();
          tError.moduleName = "CreateExcelList";
          tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
          tError.errorMessage = "没有查询到需要下载的数据";
          mErrors.addOneError(tError);
          return false;
      }
      mExcelData=tSSRS.getAllData();
      if(mExcelData==null){
    	  CError tError = new CError();
          tError.moduleName = "CreateExcelList";
          tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
          tError.errorMessage = "没有查询到需要下载的数据";
    	  return false;
      }
      return true;
  }
  
  
}

