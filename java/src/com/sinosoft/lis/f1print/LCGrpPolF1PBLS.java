package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author zz
 * @version 1.0
 */
import java.sql.Connection;
import java.sql.Statement;
import java.util.List;

import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.COracleBlob;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLDatasets;
import org.jdom.Document;
import org.jdom.Element;

public class LCGrpPolF1PBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    //业务处理相关变量
    /** 全局数据 */
    private String mOperate = "";
    private VData mInputData;
    private VData mResult = new VData();

    public LCGrpPolF1PBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        mInputData = (VData) cInputData.clone();

        if (mOperate.equals("PRINT"))
        {
            return save(mInputData);
        }
        else if (mOperate.equals("REPRINT"))
        {
            return update(mInputData);
        }

        return true;
    }

    //保存操作
    private boolean save(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn = DBConnPool.getConnection();

        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpPolF1PBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);

            System.out.println("Start ....");
            LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB(conn);

            // 获取要保存的数据
            LCGrpPolSet tLCGrpPolSet = (LCGrpPolSet) mInputData.
                                       getObjectByObjectName("LCGrpPolSet", 0);
            XMLDatasets tXMLDatasets =
                    (XMLDatasets) mInputData.getObjectByObjectName(
                            "XMLDatasets", 0);
            GlobalInput tGlobalInput = (GlobalInput) mInputData.
                                       getObjectByObjectName("GlobalInput", 0);

            for (int i = 0; i < tLCGrpPolSet.size(); i++)
            {
                tLCGrpPolDB.setSchema(tLCGrpPolSet.get(i + 1));

                if (!tLCGrpPolDB.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LCGrpPolF1PBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "更新打印次数失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            } //end for ()

            // 保存数据流
            saveDataStream(tXMLDatasets, tGlobalInput, conn);

            conn.commit();
            conn.close();
            System.out.println("commit end");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpPolF1PBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
            tReturn = false;
        }

        return tReturn;
    }

    /**
     * 将所有得到的打印数据按照主险保单号存入数据库，为以后的补打保单做准备
     * @param aXMLDataSets
     * @param aGlobalInput
     * @param conn
     * @throws Exception
     */
    private void saveDataStream(XMLDatasets aXMLDataSets,
                                GlobalInput aGlobalInput,
                                Connection conn) throws Exception
    {

        Document doc = aXMLDataSets.getDocument();
        List list = doc.getRootElement().getChildren("DATASET");

        for (int nIndex = 0; nIndex < list.size(); nIndex++)
        {
            Element ele = (Element) list.get(nIndex);

            //
            // Build a new element from the content of old element.
            // By doing this, we can detach element from document
            //
            ele = new Element("DATASET").setMixedContent(ele.getMixedContent());

            // Document newDoc = new Document(new Element("DATASETS").addContent(ele));
            Document newDoc = new Document(ele);
            saveOneDataStream(newDoc, aGlobalInput, conn);
        }
    }


    /**
     * 保存一条保单打印的数据
     * @param doc
     * @param aGlobalInput
     * @param conn
     * @throws Exception
     */
    private void saveOneDataStream(Document doc,
                                   GlobalInput aGlobalInput,
                                   Connection conn) throws Exception
    {
        //modify by yt
        COracleBlob tCOracleBlob = new COracleBlob();
        try
        {
            String strGrpPolNo =
                    doc.getRootElement().getChildText("LCGrpPol.GrpPolNo");

            String strCurDate = PubFun.getCurrentDate();
            String strCurTime = PubFun.getCurrentTime();

            // 得到数据输出对象
            String szSQL = "DELETE FROM LCPolPrint WHERE MainPolNo = '" +
                           strGrpPolNo + "'";
            if (!tCOracleBlob.DeleteBlobRecord(szSQL, conn))
            {
                throw new Exception("删除数据失败！" + szSQL);
            }

            szSQL = "INSERT INTO LCPolPrint(MainPolNo, PrtTimes, Operator, MakeDate, MakeTime, ModifyDate, ModifyTime, PolInfo, PolType) VALUES('"
                    + strGrpPolNo + "', "
                    + "1, '"
                    + aGlobalInput.Operator + "', '"
                    + strCurDate + "', '"
                    + strCurTime + "', '"
                    + strCurDate + "', '"
                    + strCurTime + "', empty_blob(), '2')";

            if (!tCOracleBlob.InsertBlankBlobRecord(szSQL, conn))
            {
                throw new Exception("插入数据失败！" + szSQL);
            }

            szSQL = " and MainPolNo = '" + strGrpPolNo + "' ";
            if (!tCOracleBlob.UpdateBlob(doc, "LCPolPrint", "PolInfo", szSQL,
                                         conn))
            {
                throw new Exception("修改数据失败！" + szSQL);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * Kevin 2003-03-27
     * 补打保单时对应的保存函数
     * @param vData
     * @return
     */
    private boolean update(VData vData)
    {
        LCGrpPolSet tLCGrpPolSet = (LCGrpPolSet) vData.getObjectByObjectName(
                "LCGrpPolSet", 0);

        Connection conn = null;
        Statement stmt = null;
        String strSQL = "";

        try
        {
            conn = DBConnPool.getConnection();
            stmt = conn.createStatement();

            if (conn == null)
            {
                throw new Exception("连接数据库失败！");
            }

            conn.setAutoCommit(false);

            for (int nIndex = 0; nIndex < tLCGrpPolSet.size(); nIndex++)
            {
                LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolSet.get(nIndex + 1);

                strSQL = "UPDATE LCPolPrint SET ModifyDate = '" +
                         tLCGrpPolSchema.getModifyDate()
                         + "' , ModifyTime = '" + tLCGrpPolSchema.getModifyTime()
                         + "' WHERE MainPolNo = '" +
                         tLCGrpPolSchema.getGrpPolNo() + "'";
                /*Lis5.3 upgrade get
                      PrtTimes = " + tLCGrpPolSchema.getPrintCount()
                       + ",
                 */

                stmt.executeUpdate(strSQL);
            }

            conn.commit();

            if (stmt != null)
            {
                stmt.close();
            }
            if (conn != null)
            {
                conn.close();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            try
            {
                if (stmt != null)
                {
                    stmt.close();
                }
                if (conn != null)
                {
                    conn.close();
                }
            }
            catch (Exception e)
            {
                // do nothing
            }
            buildError("getRePrintData", ex.getMessage());
            return false;
        }

        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
