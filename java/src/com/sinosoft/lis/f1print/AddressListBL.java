package com.sinosoft.lis.f1print;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class AddressListBL {
	/** 错误信息容器 */
	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = null;

	private String mOutXmlPath = null;

	private String mSignStartDate;

	private String mSignEndDate;

	private String mCVStartDate;

	private String mCVEndDate;

	private String mCusStartDate;

	private String mCusEndDate;

	private String mManageCom;

	public AddressListBL() {
	}

	public boolean submitData(VData cInputData, String operate) {

		if (!getInputData(cInputData)) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		return true;
	}

	/**
	 * getInputData 将外部传入的数据分解到本类的属性中
	 * 
	 * @param cInputData
	 *            VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean getInputData(VData data) {
		mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		TransferData tf = (TransferData) data.getObjectByObjectName(
				"TransferData", 0);
		System.out.println(mGI.ManageCom);

		if (mGI == null || tf == null) {
			CError tError = new CError();
			tError.moduleName = "GetAdvancePrintBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
		mSignStartDate = (String) tf.getValueByName("SignStartDate");
		mSignEndDate = (String) tf.getValueByName("SignEndDate");
		mCVStartDate = (String) tf.getValueByName("CVStartDate");
		mCVEndDate = (String) tf.getValueByName("CVEndDate");
		mCusStartDate = (String) tf.getValueByName("CusStartDate");
		mCusEndDate = (String) tf.getValueByName("CusEndDate");
		mManageCom = (String) tf.getValueByName("ManageCom");
		if (mManageCom == null || mManageCom.trim().equals("")) {
			CError tError = new CError();
			tError.moduleName = "GetAdvancePrintBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}

		return true;
	}

	/**
	 * dealData 处理业务数据
	 * 
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean dealData() {

		String sql = "select cc.contno,ca.appntno,cad.POSTALADDRESS,cad.HOMEADDRESS,cad.COMPANYADDRESS from lccont cc "
				+ "inner join lcappnt ca on ca.appntno=cc.appntno and ca.contno=cc.contno "
				+ "inner join lcaddress cad on cad.customerno=ca.appntno and cad.addressno=ca.addressno "
				+ "where ";
		String whereSql = "";

		if (mSignStartDate != null && !mSignStartDate.equals("")) {
			whereSql = whereSql + "cc.signdate >= '" + mSignStartDate
					+ "' and ";
		}
		if (mSignEndDate != null && !mSignEndDate.equals("")) {
			whereSql = whereSql + "cc.signdate <= '" + mSignEndDate + "' and ";
		}
		if (mCVStartDate != null && !mCVStartDate.equals("")) {
			whereSql = whereSql + "cc.cvalidate >= '" + mCVStartDate + "' and ";
		}
		if (mCVEndDate != null && !mCVEndDate.equals("")) {
			whereSql = whereSql + "cc.cvalidate <= '" + mCVEndDate + "' and ";
		}
		if (mCusStartDate != null && !mCusStartDate.equals("")) {
			whereSql = whereSql + "cc.customgetpoldate >= '" + mCusStartDate
					+ "' and ";
		}
		if (mCusEndDate != null && !mCusEndDate.equals("")) {
			whereSql = whereSql + "cc.customgetpoldate <= '" + mCusEndDate
					+ "' and ";
		}

		if (whereSql.equals("")) {
			CError tError = new CError();
			tError.moduleName = "GetAdvancePrintBL";
			tError.functionName = "dealData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		} else {
			sql = sql + whereSql + " cc.managecom like '" + mManageCom
					+ "%' and " + "conttype='1'";
		}

		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(sql);

		List tList = checkNum(tSSRS);

		String[][] mToExcel = new String[tList.size() + 14][30];
		mToExcel[0][0] = "可疑客户资料清单";
		mToExcel[1][0] = "";
		mToExcel[2][0] = "管理机构：" + getName(mManageCom);
		mToExcel[3][0] = "签单日期：" + mSignStartDate + "至" + mSignEndDate;
		mToExcel[4][0] = "生效日期：" + mCVStartDate + "至" + mCVEndDate;
		mToExcel[5][0] = "回执回销日期：" + mCusStartDate + "至" + mCusEndDate;

		mToExcel[7][0] = "销售渠道";
		mToExcel[7][1] = "网点";
		mToExcel[7][2] = "网点名称";
		mToExcel[7][3] = "业务员姓名";
		mToExcel[7][4] = "保单号";
		mToExcel[7][5] = "投保单号";
		mToExcel[7][6] = "投保人";
		mToExcel[7][7] = "保费";
		mToExcel[7][8] = "投保人联系地址";
		mToExcel[7][9] = "投保人家庭地址";
		mToExcel[7][10] = "投保人公司地址";
		mToExcel[7][11] = "业务员电话";
		mToExcel[7][12] = "问题说明";

		int row = 0;
		for (; row < tList.size(); row++) {
			String[] arr = (String[]) tList.get(row);
			mToExcel[row + 8][12] = arr[1];
			mToExcel[row + 8][8] = arr[2];
			mToExcel[row + 8][9] = arr[3];
			mToExcel[row + 8][10] = arr[4];

			String contSql = "select (select distinct CodeName from LDCode where "
					+ "(CodeType = 'lcsalechnl' or codetype = 'salechnl') and Code=lccont.salechnl),"
					+ "agentcom,(select name from lacom where agentcom=lccont.agentcom),"
					+ "(select name from laagent where agentcode=lccont.agentcode),contno,prtno,appntname,prem,(select mobile from laagent where agentcode=lccont.agentcode) "
					+ "from lccont where contno='"
					+ arr[0].trim()
					+ "' and conttype='1'";

			tExeSQL = new ExeSQL();
			SSRS contSSRS = tExeSQL.execSQL(contSql);
			mToExcel[row + 8][0] = contSSRS.GetText(1, 1);
			mToExcel[row + 8][1] = contSSRS.GetText(1, 2);
			mToExcel[row + 8][2] = contSSRS.GetText(1, 3);
			mToExcel[row + 8][3] = contSSRS.GetText(1, 4);
			mToExcel[row + 8][4] = contSSRS.GetText(1, 5);
			mToExcel[row + 8][5] = contSSRS.GetText(1, 6);
			mToExcel[row + 8][6] = contSSRS.GetText(1, 7);
			mToExcel[row + 8][7] = contSSRS.GetText(1, 8);
			mToExcel[row + 8][11] = contSSRS.GetText(1, 9);
		}

		mToExcel[row + 10][0] = "制表单位：" + getName(mGI.ManageCom);
		mToExcel[row + 10][5] = "制表人：" + mGI.Operator;

		try {
			System.out.println(mOutXmlPath);
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate() };
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(mOutXmlPath);
			System.out.println("生成文件完成");
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}

		return true;
	}

	private List checkNum(SSRS tSSRS) {
		List tList = new ArrayList();
		for (int row = 1; row <= tSSRS.getMaxRow(); row++) {
			String[] arr = new String[5];
			String contNo = tSSRS.GetText(row, 1);
			String customerNo = tSSRS.GetText(row, 2);
			String postal = tSSRS.GetText(row, 3);
			String home = tSSRS.GetText(row, 4);
			String company = tSSRS.GetText(row, 5);
			int pos = 0;
			int hom = 0;
			int com = 0;
			String posM = "";
			String homM = "";
			String comM = "";
			List posCus = new ArrayList();
			List homCus = new ArrayList();
			List comCus = new ArrayList();
			for (int row2 = 1; row2 <= tSSRS.getMaxRow(); row2++) {
				if (row == row2 || customerNo.equals(tSSRS.GetText(row2, 2))) {
					continue;
				}
				if (postal != null && !postal.trim().equals("")
						&& postal.trim().equals(tSSRS.GetText(row2, 3))) {
					if (!checkCus(posCus, tSSRS.GetText(row2, 2))) {
						pos++;
						posCus.add(tSSRS.GetText(row2, 2));
					}
					posM = posM + tSSRS.GetText(row2, 1) + "、";
				}
				if (home != null && !home.trim().equals("")
						&& home.trim().equals(tSSRS.GetText(row2, 4))) {
					if (!checkCus(homCus, tSSRS.GetText(row2, 2))) {
						hom++;
						homCus.add(tSSRS.GetText(row2, 2));
					}
					homM = homM + tSSRS.GetText(row2, 1) + "、";
				}
				if (company != null && !company.trim().equals("")
						&& company.trim().equals(tSSRS.GetText(row2, 5))) {
					if (!checkCus(comCus, tSSRS.GetText(row2, 2))) {
						com++;
						comCus.add(tSSRS.GetText(row2, 2));
					}
					comM = comM + tSSRS.GetText(row2, 1) + "、";
				}
			}
			String info = "";
			if (pos > 1) {
				info = "与保单" + posM.substring(0, posM.length() - 1) + "联系地址重复，"
						+ postal;
			} else if (hom > 1) {
				if (!"".equals(info)) {
					info = info + "，";
				}
				info = info + "与保单" + homM.substring(0, homM.length() - 1)
						+ "家庭地址重复，" + home;
			} else if (com > 1) {
				if (!"".equals(info)) {
					info = info + "，";
				}
				info = info + "与保单" + comM.substring(0, comM.length() - 1)
						+ "公司地址重复，" + company;
			}

			if (!"".equals(info)) {
				arr[0] = contNo;
				arr[1] = info;
				arr[2] = postal;
				arr[3] = home;
				arr[4] = company;
				tList.add(arr);
			}
		}
		return tList;
	}

	private boolean checkCus(List cusArr, String cus) {
		for (int row = 0; row < cusArr.size(); row++) {
			if (cus.equals(cusArr.get(row))) {
				return true;
			}
		}
		return false;
	}

	private String getName(String managecom) {
		String tSQL = "";
		String tRtValue = "";
		tSQL = "select name from ldcom where comcode='" + managecom + "'";
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(tSQL);
		if (tSSRS.getMaxRow() == 0) {
			tRtValue = "";
		} else
			tRtValue = tSSRS.GetText(1, 1);
		return tRtValue;
	}

	public static void main(String[] args) {
		GlobalInput tG = new GlobalInput();
		tG.ManageCom = "86";
		tG.Operator = "cwad";
		tG.ComCode = "86";
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("OutXmlPath", "E:\\loong\\test\\b.xls");
		VData vData = new VData();
		vData.add(tG);
		vData.add(tTransferData);
		AppListUI tAppListUI = new AppListUI();
		if (!tAppListUI.submitData(vData, "")) {
			System.out.print("失败！");
		}
	}
}
