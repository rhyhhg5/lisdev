package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description:
 * 得到打印满期失效清单的XmlExport数据，直接得到前台传入的Sql
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author：yangyalin
 * @version 1.3
 * @date
 */

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import java.util.HashMap;

public class PrintPauseListBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput;  //用户信息
    private String mSql;  //存储传入的Sql

    private ExeSQL mExeSQL = new ExeSQL();

    private HashMap mManageComMap = new HashMap();  //存储保单管理机构及代码，避免多次查询数据库
    private HashMap mAgentGroupMap = new HashMap();  //作用同上

    private XmlExport mXmlExport = new XmlExport();   //得到的清单数据

    public PrintPauseListBL()
    {
    }

    /**
     * 得到保单保单需要打印的失效保单数据
     * @param data VData：包括
     * 1、TransferData：存储页面传入的Sql，生成清单用
     * 2、GlobalInput对象，用户信息
     * @param operate String:""
     * @return XmlExport：得到需打印的清单数据
     */
    public XmlExport getXmlExport(VData data, String operate)
    {
        if(getInputData(data) == false)
        {
            return null;

        }

        if(dealData() == false)
        {
            return null;
        }

        return mXmlExport;
    }

    /**
     * 生成打印数据
     * @return boolean：成功true，否则fasle
     */
    private boolean dealData()
    {
        SSRS tSSRS = new ExeSQL().execSQL(mSql);
        if(tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("没有查询到需要的清单数据");
            return false;
        }

        ListTable tlistTable = new ListTable();
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String[] info = new String[tSSRS.getMaxCol() + 1];
            info[0] = "" + i;
            for(int position = 1; position <= tSSRS.getMaxCol(); position++)
            {
                //管理机构编码显示为下面的格式汉字：二级机构名称——三级机构名称
                if(position == 2)
                {
                    info[position]
                        = getCodeNameManageCom(tSSRS.GetText(i, position));
                }
                //
                else if(position == 3)
                {
                    info[position]
                        = showCodeNameAgentGroup(tSSRS.GetText(i, position));
                }
                else
                {
                    info[position] = StrTool.cTrim(tSSRS.GetText(i, position));
                }
            }
            tlistTable.add(info);
        }

        tlistTable.setName("PausePol");
        mXmlExport.createDocument("PrtPauseList.vts", "printer");
        mXmlExport.addListTable(tlistTable, new String[tSSRS.getMaxRow()]);
        return true;
    }

    /**
     * 将营销部门编码显示为下面格式汉字：营业部名称（营销服务部）－营业区名称－营业处名称
     * 若团单只有营业部
     * @param agentGroup String：销售机构代码
     * @return String：营业部名称（营销服务部）－营业区名称－营业处名称
     */
    private String showCodeNameAgentGroup(String agentGroup)
    {
        String agentGroupName3 = ""; //查询营业处（团单为营业部）
        String agentGroupName2 = ""; //查询营业区
        String agentGroupName1 = ""; //查询营业部

        String upBranch = "";  //直属上级

        SSRS nameAndUpBanch = null;


        String sqlTemp = "select Name, upBranch from LABranchGroup ";

        //查询营业处（团单为营业部）
        nameAndUpBanch = (SSRS)mAgentGroupMap.get(agentGroup);
        if(nameAndUpBanch != null)
        {
            agentGroupName3 = nameAndUpBanch.GetText(1, 1);
            upBranch = nameAndUpBanch.GetText(1, 2);
        }
        else
        {
            String sql3 = sqlTemp + "where agentGroup = '" + agentGroup + "' ";
            SSRS tSSRS = mExeSQL.execSQL(sql3);
            if(tSSRS.getMaxRow() == 0)
            {
                return "";
            }
            agentGroupName3 = tSSRS.GetText(1, 1);
            upBranch = tSSRS.GetText(1, 2);

            mAgentGroupMap.put(agentGroup, tSSRS);
        }

        //查询营业区
        nameAndUpBanch = (SSRS)mAgentGroupMap.get(upBranch);
        if(nameAndUpBanch != null)
        {
            agentGroupName3 = nameAndUpBanch.GetText(1, 1);
            upBranch = nameAndUpBanch.GetText(1, 2);
        }
        else
        {
            String sql2 = sqlTemp + "where agentGroup = '" + upBranch + "' ";
            SSRS tSSRS = mExeSQL.execSQL(sql2);
            if(tSSRS.getMaxRow() == 0)
            {
                return agentGroupName3;
            }
            else
            {
                agentGroupName2 = tSSRS.GetText(1, 1);
                upBranch = tSSRS.GetText(1, 2);

                mAgentGroupMap.put(agentGroup, tSSRS);
            }
        }

        //查询营业部
        nameAndUpBanch = (SSRS)mAgentGroupMap.get(upBranch);
        if(nameAndUpBanch != null)
        {
            agentGroupName3 = nameAndUpBanch.GetText(1, 1);
            upBranch = nameAndUpBanch.GetText(1, 2);
        }
        else
        {
            String sql1 = sqlTemp + "where agentGroup = '" + upBranch + "' ";
            SSRS tSSRS = mExeSQL.execSQL(sql1);
            if(tSSRS.getMaxRow() != 0)
            {
                agentGroupName1 = tSSRS.GetText(1, 1);
                mAgentGroupMap.put(agentGroup, tSSRS);
            }
            else
            {
                agentGroupName1 = "";
            }
        }

        return agentGroupName1 + " " + agentGroupName2 + " " + agentGroupName3;
    }

    /**
     * 管理机构编码显示为下面的格式汉字：二级机构名称——三级机构名称
     * @param manageCode String：管理机构编码
     * @return String：管理机构编码的格式汉字
     */
    private String getCodeNameManageCom(String manageCom)
    {
        String sqlTemp = "select name from LDCom ";

        String manageComName3 = (String) mManageComMap.get(manageCom);
        if(manageComName3 == null || manageComName3.equals(""))
        {
            String sql3 = sqlTemp + "where comCode = '" + manageCom + "' "; //查询3级机构
            manageComName3 = mExeSQL.getOneValue(sql3);
            mManageComMap.put(manageCom, manageComName3);
        }

        String manageCom2 = manageCom.substring(0, 4); //二级管理机构
        String manageComName2 = (String) mManageComMap.get(manageCom2);
        if(manageComName2 == null || manageComName2.equals(""))
        {
            String sql2 = sqlTemp + "where comCode = '"
                          + manageCom2 + "' "; //查询2级机构
            manageComName2 = mExeSQL.getOneValue(sql2);
            if(manageComName2 == null || manageComName2 == "null")
            {
                manageComName2 = "";
            }
            mManageComMap.put(manageCom2, manageComName2);
        }

        return manageComName2 + " " + manageComName3;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param cInputData VData：getXmlExport中传入的VData对象
     * @return boolean：成功true，否则fasle
     */
    private boolean getInputData(VData cInputData)
    {
        mSql = (String) ((TransferData) cInputData.get(0)).getValueByName("sql");
        if(mSql == null || mSql.equals(""))
        {
            mErrors.addOneError("请查询到清单后再执行打印程序。");
            return false;
        }

        mGlobalInput = (GlobalInput) cInputData.get(1);
        return true;
    }

    public static void main(String[] args)
    {
        TransferData tTransferData = new TransferData();
        VData tVData = new VData();
        tTransferData.setNameAndValue("sql", "select c.MakeDate, b.manageCom, b.agentGroup, b.contNo, b.appntName,    (select mobile from LCAddress where '1162351712000'='1162351712000' and  customerNo = b.appntNo and addressNo = d.addressNo),    (select postalAddress from LCAddress where customerNo = b.appntNo and addressNo = d.addressNo),   c.getNoticeNo, c.sumDuePayMoney, b.payToDate, c.payDate,   (select codeName from LDCode where codeType = 'paymode' and code = b.payMode),    a.prtSeq, a.makeDate,    (select codeName from LDCode where codeType = 'pausecode' and code=a.code),    b.agentCode, (select mobile from LAAGent where agentCode = b.agentCode),    (select name from LAAGent where agentCode = b.agentCode)from LOPRTManager a, LCCont b, LJSPayB c, LCAppnt d where a.otherNo = b.ContNo    and b.contNo = c.otherNo    and b.contNo = d.contNo  and a.MakeDate>='2006-08-01'  and a.MakeDate<='2006-11-01'  AND ( a.Code = '42' OR a.Code = '21' )");
        GlobalInput tG = new GlobalInput();
        tVData.add(0, tTransferData);
        tVData.add(1, tG);

        PrintPauseListBL test = new PrintPauseListBL();
        test.getXmlExport(tVData, "");
    }

}
