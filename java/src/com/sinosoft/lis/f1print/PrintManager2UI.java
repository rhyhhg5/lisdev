package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LOPRTManager2Schema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PrintManager2UI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
//业务处理相关变量
    /** 全局数据 */
    private LOPRTManager2Schema mLOPRTManager2Schema = new LOPRTManager2Schema();
    private GlobalInput tG = new GlobalInput();

    public PrintManager2UI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        PrintManager2BL tPrintManager2BL = new PrintManager2BL();
        System.out.println("Start PrintManager2UI Submit...");
        tPrintManager2BL.submitData(mInputData, mOperate);
        System.out.println("End PrintManager2UI Submit...");
        //如果有需要处理的错误，则返回
        if (tPrintManager2BL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPrintManager2BL.mErrors);
            CError tError = new CError();
            tError.moduleName = "PrintManager2UI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
//    if (mOperate.equals("INSERT||CODE"))
//    {
        this.mResult.clear();
        this.mResult = tPrintManager2BL.getResult();
//    }
        mInputData = null;
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(this.mLOPRTManager2Schema);
            mInputData.add(this.tG);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PrintManager2UI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        this.mLOPRTManager2Schema.setSchema((LOPRTManager2Schema) cInputData.
                                            getObjectByObjectName(
                "LOPRTManager2Schema", 0));
        this.tG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "001";
        tG.ComCode = "";
        LOPRTManager2Schema mLOPRTManager2Schema = new LOPRTManager2Schema();
        mLOPRTManager2Schema.setPrtSeq("86000020030820000005");
        VData tVData = new VData();
        tVData.addElement(tG);
        tVData.addElement(mLOPRTManager2Schema);
        PrintManager2UI tPrintManager2UI = new PrintManager2UI();
        tPrintManager2UI.submitData(tVData, "REPRINT");
        VData mResult = new VData();
        mResult = tPrintManager2UI.getResult();
    }
}