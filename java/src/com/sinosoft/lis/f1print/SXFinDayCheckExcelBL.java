package com.sinosoft.lis.f1print;

import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;


public class SXFinDayCheckExcelBL {

		/** 错误处理类，每个需要错误处理的类中都放置该类 */
		public CErrors mErrors = new CErrors();
		private String mDay[] = null; // 获取时间
		private GlobalInput mGlobalInput = new GlobalInput(); // 全局变量
		private String tOutXmlPath = "";
		String msql = "";

		public SXFinDayCheckExcelBL() {
		}

		public static void main(String[] args) {
			GlobalInput tG = new GlobalInput();
			tG.Operator = "cwad1";
			tG.ManageCom = "86";
			VData vData = new VData();
			String[] tDay = new String[4];
			tDay[0] = "2010-10-01";
			tDay[1] = "2010-10-01";
			vData.addElement(tDay);
			vData.addElement(tG);

			SXFinDayCheckExcelBL sXFinDayCheckBL = new SXFinDayCheckExcelBL();
			sXFinDayCheckBL.submitData(vData, "PRINT");

		}

		/**
		 * 传输数据的公共方法
		 */
		public boolean submitData(VData cInputData, String cOperate) {
			if (!cOperate.equals("PRINT")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}
			if (!getInputData(cInputData)) {
				return false;
			}
			if (cOperate.equals("PRINT")) { // 打印提数
				if (!getPrintData()) {
					return false;
				}
			}
			return true;
		}

		/**
		 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
		 */
		private boolean getInputData(VData cInputData) { // 打印付费
			// 全局变量
			mDay = (String[]) cInputData.get(0);
			mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
			tOutXmlPath = (String)cInputData.get(2);
			
			if (mGlobalInput == null) {
				buildError("getInputData", "没有得到足够的信息！");
				return false;
			}
			if (tOutXmlPath == null || tOutXmlPath.equals("")) {
				buildError("getInputData", "没有得到文件路径的信息！");
				return false;
			}
			return true;
		}

		private void buildError(String szFunc, String szErrMsg) {
			CError cError = new CError();
			cError.moduleName = "SXFinDayCheckExcelBL";
			cError.functionName = szFunc;
			cError.errorMessage = szErrMsg;
			this.mErrors.addOneError(cError);
		}

		/**
		 * 新的打印数据方法
		 *
		 * @return
		 */
		private boolean getPrintData() {
			
			ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = new SSRS();
			
			String nsql = "select Name from LDCom where ComCode='"+ mGlobalInput.ManageCom + "'"; // 管理机构
			tSSRS = tExeSQL.execSQL(nsql);
			String manageCom = tSSRS.GetText(1, 1);
			
			msql="select (select name from lacom where " +
			"agentcom=a.agentcom),b.contno,b.receiptno,b.riskcode,c.appntname,b.transmoney,b.charge/b.transmoney,b.charge,a.managecom from ljaget a,lacharge" +
			" b,lccont c where  a.otherno=b.receiptno and b.contno=c.contno and a.GetNoticeNo = b.commisionsn and a.othernotype in('BC','AC','MC') and " +
			"(b.grpcontno='00000000000000000000' or b.grpcontno is null) and "
			+"b.managecom like '" + mGlobalInput.ManageCom + "%' and b.modifydate>='"+mDay[0]+"' and b.modifydate<='"+mDay[1]+"' and b.transmoney<>0 " +
					"and b.transmoney is not null and b.charge<>0 and chargestate in('1','3')"
			+"union all"+" select (select name from lacom where " +
					"agentcom=a.agentcom),b.contno,b.receiptno,b.riskcode,c.appntname,b.transmoney,b.charge/b.transmoney,b.charge,a.managecom from ljaget a,lacharge"
			+" b,lbcont c where  a.otherno=b.receiptno and b.contno=c.contno and a.GetNoticeNo = b.commisionsn and a.othernotype in ('BC','AC','MC') and " +
					"(b.grpcontno='00000000000000000000' or b.grpcontno is null) and "
			+"b.managecom like '" + mGlobalInput.ManageCom + "%' and b.modifydate>='"+mDay[0]+"' and b.modifydate<='"+mDay[1]+"' and b.transmoney<>0 " +
					"and b.transmoney is not null and b.charge<>0 and chargestate in('1','3')"
			+"union all"
			+"  select (select name from lacom where " +
					"agentcom=a.agentcom),b.contno,b.receiptno,b.riskcode,c.grpname,b.transmoney,b.charge/b.transmoney,b.charge,a.managecom"
			                  +"   from ljaget a,lacharge b,lcgrpcont c"
			                  +"   where a.otherno=b.receiptno and b.grpcontno=c.grpcontno and a.GetNoticeNo = b.commisionsn  and a.othernotype in ('BC','AC','MC') and "
			                  +"  b.managecom like '" + mGlobalInput.ManageCom + "%' and b.modifydate>='"+mDay[0]+"' and b.modifydate<='"+mDay[1]+"' " +
			                  		"and"
			                  +"  b.transmoney<>0 and b.transmoney is not null"
			                   +" and b.grpcontno <> '00000000000000000000'"
			                    +"  and b.grpcontno is not null and b.charge<>0 and chargestate in('1','3')"
			+"  union all" +
					" select (select name from lacom where " +
					"agentcom=a.agentcom),b.contno,b.receiptno,b.riskcode,c.grpname,b.transmoney,b.charge/b.transmoney,b.charge,a.managecom "
			                   +"  from ljaget a,lacharge b,lbgrpcont c"
			                   +"  where a.otherno=b.receiptno and b.grpcontno=c.grpcontno and a.GetNoticeNo = b.commisionsn and a.othernotype in ('BC','AC','MC') and "
			                   +"  b.managecom like '" + mGlobalInput.ManageCom + "%' and b.modifydate>='"+mDay[0]+"' and b.modifydate<='"+mDay[1]+"' " +
			                   		"and "
			                   +"  b.transmoney<>0 and b.transmoney is not null"
			                   +"  and b.grpcontno <> '00000000000000000000'"
			                   +"  and b.grpcontno is not null and b.charge<>0 and chargestate in('1','3') with ur";
			
			tSSRS = tExeSQL.execSQL(msql);
            
            String[][] mToExcel = new String[tSSRS.MaxRow+10][9];
            mToExcel[0][0] = "手续费日结单";
            mToExcel[1][0] = "统计日期：";
            mToExcel[1][2] = mDay[0]+"至"+mDay[1];
            mToExcel[2][0] = "管理机构：";
            mToExcel[2][2] = manageCom;
            mToExcel[4][0] = "中介机构";
            mToExcel[4][1] = "保单号码";
            mToExcel[4][2] = "结算号";
            mToExcel[4][3] = "险种代码";
            mToExcel[4][4] = "投保人";
            mToExcel[4][5] = "保费收入";
            mToExcel[4][6] = "手续费率";
            mToExcel[4][7] = "手续费金额";
            mToExcel[4][8] = "机构代码";
            
            int no=5;  //初始打印行数
            
			for (int row = 1; row <= tSSRS.MaxRow; row++){ 
				for (int col = 1; col <= tSSRS.MaxCol; col++) {
					if (col == 6) {
						if(tSSRS.GetText(row, col)!=null && (!"".equals(tSSRS.GetText(row, col)))){
							mToExcel[no+row-1][col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
						}
						continue;
					}
					if (col == 7) {
						if(tSSRS.GetText(row, col)!=null && (!"".equals(tSSRS.GetText(row, col)))){
							mToExcel[no+row-1][col-1] = new DecimalFormat("0.000").format(Double.valueOf(tSSRS.GetText(row, col)));
						}
						continue;
					}
					if (col == 8) {
						if(tSSRS.GetText(row, col)!=null && (!"".equals(tSSRS.GetText(row, col)))){
							mToExcel[no+row-1][col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
						}
						continue;
					}
					mToExcel[no+row-1][col-1] = tSSRS.GetText(row, col);
				}
			}
			no=no+tSSRS.MaxRow;
			
			msql="select COALESCE(sum(tt.charge),0) from (select distinct b.* from ljaget a,lacharge b where a.otherno=b.receiptno and b.chargestate in('1','3') and a.othernotype in ('BC','AC')" +
			" and b.managecom like '" + mGlobalInput.ManageCom + "%' and b.modifydate>='"+mDay[0]+"' and b.modifydate<='"+mDay[1]+"') as tt with ur";
			tSSRS = tExeSQL.execSQL(msql);
			mToExcel[no][0] = "合计";
			mToExcel[no][1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
			
			mToExcel[no+2][0] = "制表员：";
			mToExcel[no+2][4] = "审核员：";
			
			try{
				System.out.println("X9-SXF--tOutXmlPath:"+tOutXmlPath);
				WriteToExcel t = new WriteToExcel("");
				t.createExcelFile();
				String[] sheetName = {PubFun.getCurrentDate()};
				t.addSheet(sheetName);
				t.setData(0, mToExcel);
				t.write(tOutXmlPath);
				System.out.println("生成X9-SXF文件完成");
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				buildError("getPrintData", e.toString());
			}
			return true;
		}

//		public static void main(String[] args){
//
//			SXFinDayCheckBL sSXFinDayCheckBL = new SXFinDayCheckBL();
//			VData t = new VData();
//			String[] mDay = {"2010-10-10","2010-10-10"};
//			t.add(mDay);
//			t.add("SXFDDS");
//			GlobalInput tG = new GlobalInput();
//			tG.ManageCom = "86";
//			t.add(tG);
//
//		}


}
