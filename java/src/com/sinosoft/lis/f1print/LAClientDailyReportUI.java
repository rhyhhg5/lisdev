/*
 * <p>ClassName: LAGroupUniteUI </p>
 * <p>Description: LAGroupUniteUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-11-23 09:58:25
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;

public class LAClientDailyReportUI
{
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private VData mResult = new VData();

    public static void main(String[] agrs)
    {
        String EndDay = "2006-02-10";
        String DailyType = "03";

        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "mak001";
        tGlobalInput.ManageCom = "86";
        tGlobalInput.ComCode = "mak001";

        tVData.addElement(EndDay);
        tVData.addElement(DailyType);
        tVData.addElement(tGlobalInput);
        LAClientDailyReportUI UI = new LAClientDailyReportUI();
        if (!UI.submitData(tVData, "PRINT")) {
            if (UI.mErrors.needDealError()) {
                System.out.println(UI.mErrors.getFirstError());
            } else {
                System.out.println("UI发生错误，但是没有提供详细的出错信息");
            }
        } else {
            VData vData = UI.getResult();
            System.out.println("已经接收了数据!!!");
        }
    }

    public boolean submitData(VData cInputData,String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData=(VData)cInputData.clone();
System.out.println("Start LAClientDailyReportBL...");
        LAClientDailyReportBL tLAClientDailyReportBL= new LAClientDailyReportBL();
        if (!tLAClientDailyReportBL.submitData(mInputData, mOperate))
        {
            if (tLAClientDailyReportBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tLAClientDailyReportBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "LAStatisticReportUI发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tLAClientDailyReportBL.getResult();
            return true;
        }
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAStatisticReportUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 取得返回处理过的结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }
}
