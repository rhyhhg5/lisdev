package com.sinosoft.lis.f1print;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.db.LJSPayBDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 接收PrintPauseListUI.java传入的数据，生成打印清单所需要的XmlExport数据。
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class PrintPauseNoticeBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;
    private GlobalInput mGlobalInput = null;
    private String mPrtSeq = null;
    private LOPRTManagerSchema mLOPRTManagerSchema = null;

    private TextTag textTag = new TextTag();
    private XmlExport mXmlExport = new XmlExport();  //处理后的清单打印数据。

    public PrintPauseNoticeBL()
    {
    }

    /**
     *
     * @param cInputData VData：包含
     1、	TransferData对象：包括查询Sql
     2、	GlobalInput对象：完整的操作员信息。
     * @param operate String：操作方式，可为空串“”
     * @return XmlExport:打印清单数据，失败为null
     */
    public XmlExport getXmlExport(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return mXmlExport;
    }

    /**
     * dealData
     *处理业务逻辑，生成打印通知书数据
     * @return boolean：boolean，操作成功true，否则false
     */
    private boolean dealData()
    {
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setPrtSeq(mPrtSeq);
        if(!tLOPRTManagerDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "PrintPauseNoticeBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到通知书" + mPrtSeq;
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();

        if(!setCommonInfo())
        {
            return false;
        }

        //团单和个单的满期都为21
        if(mLOPRTManagerSchema.getCode().equals("21"))
        {
            textTag.add("EndDate", getEndDate());
            if(!isGrpCont(mLOPRTManagerSchema.getOtherNo()))
            {
                //个单
                mXmlExport.createDocument("PEndDateNotice.vts", "printer");

                if(!setPCommonInfo())
                {
                    return false;
                }
            }
            else
            {
                //团单
                mXmlExport.createDocument("GEndDateNotice.vts", "printer");

                if(!setGrpConmmonInfo())
                {
                    return false;
                }
            }
        }
        else if(mLOPRTManagerSchema.getCode().equals("42"))
        {
            textTag.add("EndDate", CommonBL.decodeDate(
                mLOPRTManagerSchema.getStandbyFlag1()));
            textTag.add("PayDate", CommonBL.decodeDate(
                mLOPRTManagerSchema.getStandbyFlag2()));
            String sumDuePayMoney = getSumDuePayMoney();
            if(sumDuePayMoney == null)
            {
                return false;
            }
            textTag.add("SumDuePayMoney", sumDuePayMoney);
            textTag.add("GetNoticeNo", mLOPRTManagerSchema.getStandbyFlag3());

            if(!isGrpCont(mLOPRTManagerSchema.getOtherNo()))
            {
                //个单失效
                mXmlExport.createDocument("PPauseNotice.vts", "printer");

                if(!setPCommonInfo())
                {
                    return false;
                }

            }
            else
            {
                //团单暂停
                mXmlExport.createDocument("GPauseNotice.vts", "printer");
                if(!setGrpConmmonInfo())
                {
                    return false;
                }
            }
        }
        else if(mLOPRTManagerSchema.getCode().equals("58"))
        {
            //团单暂停
            mXmlExport.createDocument("GPauseNoticeJS.vts", "printer");

            textTag.add("EndDate", CommonBL.decodeDate(
                mLOPRTManagerSchema.getStandbyFlag1()));
            textTag.add("PayDate", CommonBL.decodeDate(
                mLOPRTManagerSchema.getStandbyFlag2()));
            String sumDuePayMoney = getSumDuePayMoney();
            if(sumDuePayMoney == null)
            {
                return false;
            }
            textTag.add("SumDuePayMoney", sumDuePayMoney);
            String workNo = getWorkNo(mLOPRTManagerSchema.getStandbyFlag3());
            if(workNo == null)
            {
                return false;
            }
            textTag.add("GetNoticeNo", workNo);

            if(!setGrpConmmonInfo())
            {
                return false;
            }
        }

        mXmlExport.addTextTag(textTag);


        return true;
    }

    /**
     * 得到结算工单号
     * @param getNoticeNo String：收费号
     * @return String：结算工单号
     */
    private String getWorkNo(String getNoticeNo)
    {
        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBDB.setGetNoticeNo(getNoticeNo);

        if(!tLJSPayBDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "PrintPauseNoticeBL";
            tError.functionName = "getWorkNo";
            tError.errorMessage = "没有查询到应收号为"
                                  + getNoticeNo
                                  + "的结算工单号，不能生成未结算暂停通知书";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return tLJSPayBDB.getOtherNo();
    }

    /**
     * 得到应收保费
     * @return String：应收保费
     */
    private String getSumDuePayMoney()
    {
        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBDB.setGetNoticeNo(mLOPRTManagerSchema.getStandbyFlag3());
        if(!tLJSPayBDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "PrintPauseNoticeBL";
            tError.functionName = "getSumDuePayMoney";
            tError.errorMessage = "没查询到保单的应收记录，不能打印通知书";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        return String.valueOf(tLJSPayBDB.getSumDuePayMoney());
    }

    /**
     * 得到个单的联系方式
     * @param grpContNo String
     * @return boolean
     */
    private boolean setPCommonInfo()
    {
        String sql =
            "select distinct  a.ZipCode, a.PostalAddress, a.Mobile,b.appntName,"
            + "   b.appntSex, b.appntNo "
            + "from LCAddress a, LCAppnt b "
            + "where a.customerNo = b.appntNo "
            + "   and a.addressNo = b.addressNo "
            + "   and b.contNo = '" + mLOPRTManagerSchema.getOtherNo() + "' "
            + "union "
            +
            "select distinct a.ZipCode, a.PostalAddress, a.Mobile,b.appntName,"
            + "   b.appntSex, b.appntNo "
            + "from LCAddress a, LBAppnt b "
            + "where a.customerNo = b.appntNo "
            + "   and a.addressNo = b.addressNo "
            + "   and b.contNo = '" + mLOPRTManagerSchema.getOtherNo() + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if(tSSRS.getMaxRow() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "PrintPauseNoticeBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到联系人地址信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        textTag.add("ZipCode", tSSRS.GetText(1, 1));
        textTag.add("Address", tSSRS.GetText(1, 2));
        textTag.add("Mobile", tSSRS.GetText(1, 3));
        textTag.add("AppntName", tSSRS.GetText(1, 4));
        textTag.add("Sex", CommonBL.decodeSex(tSSRS.GetText(1, 5)));
        textTag.add("AppntNo", tSSRS.GetText(1, 6));
        textTag.add("ContNo", mLOPRTManagerSchema.getOtherNo());

        return true;

    }


    /**
     * 得到团单的联系方式
     * @param grpContNo String
     * @return boolean
     */
    private boolean setGrpConmmonInfo()
    {
        String sql =
            "select distinct  a.GrpZipCode, a.GrpAddress, a.phone1,b.Name "
            + "from LCGrpAddress a, LCGrpAppnt b "
            + "where a.customerNo = b.customerNo "
            + "   and a.addressNo = b.addressNo "
            + "   and b.grpContNo = '" + mLOPRTManagerSchema.getOtherNo() + "' "
            + "union "
            +
            "select distinct  a.GrpZipCode, a.GrpAddress, a.phone1,b.Name "
            + "from LCGrpAddress a, LCGrpAppnt b "
            + "where a.customerNo = b.customerNo "
            + "   and a.addressNo = b.addressNo "
            + "   and b.GrpContNo = '" + mLOPRTManagerSchema.getOtherNo() + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if(tSSRS.getMaxRow() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "PrintPauseNoticeBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到联系人地址信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        textTag.add("ZipCode", tSSRS.GetText(1, 1));
        textTag.add("Address", tSSRS.GetText(1, 2));
        textTag.add("Phone1", tSSRS.GetText(1, 3));
        textTag.add("GrpName", tSSRS.GetText(1, 4));

        String grpContNo = mLOPRTManagerSchema.getOtherNo();
        textTag.add("GrpContNo", grpContNo);

        textTag.add("CustomerNo", grpContNo.substring(0, grpContNo.length() - 2));

        return true;
    }

    private boolean isGrpCont(String grpContNo)
    {
        String sql = "select 1 "
                     + "from LCGrpCont "
                     + "where grpContNo = '" + grpContNo + "' "
                     + "union "
                     + "select 1 "
                     + "from LBGrpCont "
                     + "where grpContNo = '" + grpContNo + "' ";
        String rs = new ExeSQL().getOneValue(sql);
        if(rs.equals("") || rs.equals("null"))
        {
            return false;
        }

        return true;
    }

    /**
     * 得到机构信息
     * @param manageCom String
     * @return boolean
     */
    private boolean setCommonInfo()
    {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mLOPRTManagerSchema.getManageCom());
        tLDComDB.getInfo();

        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("ServiceFax", tLDComDB.getFax());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        textTag.add("Operator", mGlobalInput.Operator);

        //业务员信息
        String agentCode = getAgentCode();
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(agentCode);
        tLaAgentDB.getInfo();
        textTag.add("AgentName", StrTool.cTrim(tLaAgentDB.getName()));
        textTag.add("AgentCode", StrTool.cTrim(tLaAgentDB.getGroupAgentCode()));

        String agntPhone = "";
        String temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") || temPhone.equals("null"))
        {
            agntPhone = tLaAgentDB.getPhone();
        } else
        {
            agntPhone = temPhone;
        }
        textTag.add("Phone", StrTool.cTrim(agntPhone));

        //业务员机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(tLaAgentDB.getAgentGroup());
        tLABranchGroupDB.getInfo();
        textTag.add("AgentGroup", StrTool.cTrim(tLABranchGroupDB.getName()));


        textTag.add("BarCode1", mLOPRTManagerSchema.getPrtSeq());
        textTag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

        return true;
    }

    /**
     * 得到保单业务员
     * @return String
     */
    private String getAgentCode()
    {
        String contNo = mLOPRTManagerSchema.getOtherNo();

        String sql = "select AgentCode "
                     + "from LCGrpCont "
                     + "where grpContNo = '" + contNo + "' "
                     + "union "
                     + "select AgentCode "
                     + "from LBGrpCont "
                     + "where grpContNo = '" + contNo + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        String rs = tExeSQL.getOneValue(sql);
        if(rs != "" && !rs.equals("null"))
        {
            return rs;
        }

        sql = "select AgentCode "
              + "from LCCont "
              + "where ContNo = '" + contNo + "' "
              + "union "
              + "select AgentCode "
              + "from LBCont "
              + "where ContNo = '" + contNo + "' ";
        tExeSQL = new ExeSQL();
        rs = tExeSQL.getOneValue(sql);
        if(rs != "" && !rs.equals("null"))
        {
            return rs;
        }

        return "";
    }

    /**
     * 得到失效日期
     * @return String
     */
    private String getEndDate()
    {
        return CommonBL.decodeDate(mLOPRTManagerSchema.getStandbyFlag1());
    }

    /**
     * 得到传入的数据
     * @param data VData：getXmlExport中传入的VData集合
     * @return boolean：成功true，否则false
     */
    private boolean getInputData(VData data)
    {
        mGlobalInput = (GlobalInput) data
                       .getObjectByObjectName("GlobalInput", 0);

        mTransferData = (TransferData) data
                       .getObjectByObjectName("TransferData", 0);


        if(mGlobalInput == null || mGlobalInput.Operator == null
           || mGlobalInput.Operator.equals(""))
        {
            mErrors.addOneError("传入的数据不完整。mGlobalInput");
            return false;
        }

        if(mTransferData == null)
        {
            mErrors.addOneError("传入的数据不完整mTransferData。");
            return false;
        }
        mPrtSeq = (String) mTransferData.getValueByName("PrtSeq");
        if(mPrtSeq == null)
        {
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("PrtSeq", "81000004566");

        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "endor0";
        tGI.ComCode = "86";
        tGI.ManageCom = tGI.ComCode;

        VData data = new VData();
        data.add(tGI);
        data.add(tTransferData);

        PrintPauseNoticeBL bl = new PrintPauseNoticeBL();
        XmlExport xml = bl.getXmlExport(data, "");
        if(xml == null)
        {
            System.out.println(bl.mErrors);
        }
    }

}
