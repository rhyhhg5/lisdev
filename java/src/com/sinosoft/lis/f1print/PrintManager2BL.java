package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LOPRTManager2DB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LOPRTManager2Schema;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LOPRTManager2Set;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PrintManager2BL {

    // 常量定义
    // OtherNoType　其它号码类型
    public static final String ONT_INDPOL = "00"; // 个人保单号
    public static final String ONT_GRPPOL = "01"; // 集体保单号
    public static final String ONT_CONT = "02"; // 合同号
    public static final String ONT_EDOR = "03"; // 批单号
    public static final String ONT_GET = "04"; // 实付收据号
    public static final String ONT_PRT = "05"; // 保单印刷号

    // Code　单据类型
    public static final String CODE_DECLINE = "00"; // 拒保通知书
    public static final String CODE_OUTPAY = "01"; // 首期溢交保费通知书
    public static final String CODE_BALANCE = "02"; // 退余额的打印格式
    public static final String CODE_PE = "03"; // 体检通知书
    public static final String CODE_MEET = "04"; // 面见通知书
    public static final String CODE_UW = "05"; // 核保通知书
    public static final String CODE_DEFER = "06"; // 延期承保通知书
    public static final String CODE_FIRSTPAY = "07"; // 首期交费通知书
    public static final String CODE_REFUND = "08"; // 新契约退费通知书
    public static final String CODE_WITHDRAW = "09"; // 撤单通知书

    public static final String CODE_URGE_FP = "10"; // 首期交费通知书催办通知书
    public static final String CODE_URGE_PE = "11"; // 体检通知书催办通知书
    public static final String CODE_URGE_UW = "12"; // 核保通知书催办通知书
    public static final String CODE_URGE_GC = "13"; // 收款收据
    public static final String CODE_AGEN_QUEST = "14"; // 业务员问题件通知书
    public static final String CODE_URGE_PB = "15"; // 缴费催办通知书

    public static final String CODE_PEdorDECLINE = "20"; // 保全拒保通知书
    public static final String CODE_PEdorPE = "23"; // 保全体检通知书
    public static final String CODE_PEdorMEET = "24"; // 保全面见通知书
    public static final String CODE_PEdorUW = "25"; // 保全核保通知书
    public static final String CODE_PEdorDEFER = "26"; // 保全延期承保通知书

    public static final String CODE_PRnewDECLINE = "40"; // 续保拒保通知书
    public static final String CODE_PRnewPE = "43"; // 续保体检通知书
    public static final String CODE_PRnewMEET = "44"; // 续保面见通知书
    public static final String CODE_PRnewUW = "45"; // 续保核保通知书
    public static final String CODE_PRnewDEFER = "46"; // 续保延期承保通知书
    public static final String CODE_PRnewNotice = "47"; // 续保催收通知书
    public static final String CODE_PRnewSure = "49"; // 续保催收通知书

    public static final String CODE_BONUSPAY = "30"; // 个人红利派发通知书
    public static final String CODE_REPAY = "31"; // 续期缴费通知书
    public static final String CODE_PINVOICE = "32"; // 个人发票
    public static final String CODE_GINVOICE = "33"; // 团体发票
    public static final String CODE_GRPBONUSPAY = "34"; // 团体红利派发通知书

    public static final String CODE_GRP_UW = "50"; // 团体核保通知书
    public static final String CODE_GRP_DECLINE = "51"; // 团体拒保通知书
    public static final String CODE_GRP_WITHDRAW = "52"; // 团体撤单通知书
    public static final String CODE_GRP_DEFER = "53"; // 团体延期承保通知书

    public static final String ASK_GRP_DECLINE = "60"; // 团体询价拒保通知书
    public static final String ASk_GRP_SUCESS = "61"; // 团体询价成功通知书
    public static final String Ask_GRP_WITHDRAW = "62"; // 团体询价撤单通知书
    public static final String ASK_GRP_DEFER = "63"; // 团体询价延期通知书
    public static final String ASK_GRP_INFO = "64"; // 团体询价补充材料通知书
    public static final String ASK_GRP_TRACK = "65"; // 团单询价跟踪通知书

    // PrtType 打印方式
    public static final String PT_FRONT = "0"; // 前台打印
    public static final String PT_BACK = "1"; // 后台打印

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    //业务处理相关变量
    /** 全局数据 */
    private String mOperate = "";
    private VData mResult = new VData();//返回结果
    private GlobalInput mGlobalInput = new GlobalInput();//存储用户信息
    private LOPRTManager2Schema mLOPRTManager2Schema = new LOPRTManager2Schema();//打印数据
    private LOPRTManager2Set mLOPRTManager2Set = new LOPRTManager2Set();//打印数据
    private String mRePrintFlag = "";//重打标记
    private String mIncomeType = ""; //定额单证发票打印

    public PrintManager2BL() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("CONFIRM")
            && !cOperate.equals("REQUEST")
            && !cOperate.equals("REPRINT")
            && !cOperate.equals("REQ")
            && !cOperate.equals("POSTREPRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        mOperate = cOperate;

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        // 保存数据
        String tOperate = "";
        VData tVData = new VData();
        if (mIncomeType!=null && mIncomeType.equals("15") && mOperate.equals("CONFIRM")) {
            tVData.add(mLOPRTManager2Set);
        } else {
            tVData.add(mLOPRTManager2Schema);
        }
        tVData.add(mIncomeType);
        if (mOperate.equals("REQUEST")) {
            tOperate = "INSERT";
        } else if (mOperate.equals("CONFIRM")) {
            tOperate = "UPDATE";
        } else if (mOperate.equals("POSTREPRINT")) {
            tOperate = "UPDATE";
        } else if (mOperate.equals("REQ")) {
            // 调用者通过getResult得到处理过的数据，在外部对得到的数据再进行处理
            mResult = tVData;
            return true;
        } else if (mOperate.equals("REPRINT")) {
            // 直接返回
            return true;
        }
        PrintManager2BLS tPrintManager2BLS = new PrintManager2BLS();

        if (!tPrintManager2BLS.submitData(tVData, tOperate)) {
            mErrors.copyAllErrors(tPrintManager2BLS.mErrors);
            return false;
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        if (mOperate.equals("REQUEST") || mOperate.equals("REQ")) { // 打印申请
            // 处于未打印状态的通知书在打印队列中只能有一个
            // 条件：同一个单据类型，同一个其它号码，同一个其它号码类型

            String tOtherNo = mLOPRTManager2Schema.getOtherNo();

            String strOthNo[] = tOtherNo.split(",");
            // if(strOthNo.length >
            //mIncomeType=15 定额单证发票打印
            if (mIncomeType!=null && mIncomeType.equals("15")) {

                LOPRTManager2DB tLOPRTManager2DB = new LOPRTManager2DB();
                tOtherNo = "";
                for (int i = 0; i < strOthNo.length; i++) {
                    if (i == strOthNo.length - 1) {
                        tOtherNo = tOtherNo + "'" + strOthNo[i] + "'";
                    } else {
                        tOtherNo = tOtherNo + "'" + strOthNo[i] + "',";
                    }
                }
                String strSql = " select * from LOPRTManager2 where Code='"
                                + mLOPRTManager2Schema.getCode()
                                + "' and OtherNo in ("
                                + tOtherNo
                                + ") and OtherNoType='"
                                + mLOPRTManager2Schema.getOtherNoType()
                                + "'"
                                ;
                LOPRTManager2Set tLOPRTManager2Set = tLOPRTManager2DB.
                        executeQuery(strSql);
                if (tLOPRTManager2Set == null) {
                    buildError("dealData", "查询LOPRTManager2表时出现错误");
                    return false;
                }
                if (tLOPRTManager2Set.size() != 0) {
                    if (mRePrintFlag.equals("1")) { //再次打印
                        System.out.println("======再次打印======");
                        for (int j = 1; j <= tLOPRTManager2Set.size(); j++) {
                            LOPRTManager2Schema tLOPRTManager2Schema = new
                                    LOPRTManager2Schema();
                            tLOPRTManager2Schema = tLOPRTManager2Set.get(j);
                            LOPRTManager2Schema tNewLOPRTManager2Schema = new
                                    LOPRTManager2Schema();
                            if (tLOPRTManager2Schema.getStateFlag().equals("1")) {
                                //buildError("dealData", "处于未打印状态的通知书在打印队列中只能有一个");
                                //return false;

                                String strPrtTime = tLOPRTManager2Schema.
                                        getStandbyFlag1(); //StandbyFlag1记录打印次数
                                int intPrtTime = 0;
                                if (strPrtTime == null) {
                                    tNewLOPRTManager2Schema.setStandbyFlag1("1");
                                } else if (strPrtTime.equals("")) {
                                    tNewLOPRTManager2Schema.setStandbyFlag1("1");
                                } else {
                                    intPrtTime = Integer.parseInt(strPrtTime);
                                    intPrtTime++;
                                    tNewLOPRTManager2Schema.setStandbyFlag1(
                                            intPrtTime +
                                            "");
                                }
                                mOperate = "CONFIRM";
                                tNewLOPRTManager2Schema.setPrtSeq(
                                        tLOPRTManager2Schema.getPrtSeq());
                                tNewLOPRTManager2Schema.setOtherNo(
                                        tLOPRTManager2Schema.getOtherNo());
                                tNewLOPRTManager2Schema.setOtherNoType(
                                        tLOPRTManager2Schema.getOtherNoType());
                                tNewLOPRTManager2Schema.setCode(
                                        tLOPRTManager2Schema.getCode());
                                tNewLOPRTManager2Schema.setManageCom(
                                        tLOPRTManager2Schema.getManageCom());
                                tNewLOPRTManager2Schema.setAgentCode(
                                        tLOPRTManager2Schema.getAgentCode());

                                tNewLOPRTManager2Schema.setReqCom(mGlobalInput.
                                        ComCode);
                                tNewLOPRTManager2Schema.setReqOperator(
                                        mGlobalInput.
                                        Operator);
                                tNewLOPRTManager2Schema.setExeCom(mGlobalInput.
                                        ComCode);
                                tNewLOPRTManager2Schema.setExeOperator(
                                        mGlobalInput.
                                        Operator);
                                tNewLOPRTManager2Schema.setStateFlag("1");
                                tNewLOPRTManager2Schema.setPrtType("0");
                                tNewLOPRTManager2Schema.setMakeDate(PubFun.
                                        getCurrentDate());
                                tNewLOPRTManager2Schema.setMakeTime(PubFun.
                                        getCurrentTime());
                                tNewLOPRTManager2Schema.setDoneDate(PubFun.
                                        getCurrentDate());
                                tNewLOPRTManager2Schema.setDoneTime(PubFun.
                                        getCurrentTime());
                                tNewLOPRTManager2Schema.setStandbyFlag2(
                                        tLOPRTManager2Schema.getStandbyFlag2());
                                tNewLOPRTManager2Schema.setStandbyFlag3(
                                        tLOPRTManager2Schema.getStandbyFlag3());
                                tNewLOPRTManager2Schema.setStandbyFlag4(
                                        tLOPRTManager2Schema.getStandbyFlag4());
                                tNewLOPRTManager2Schema.setOldPrtSeq(
                                        tLOPRTManager2Schema.getOldPrtSeq());

                                mLOPRTManager2Set.add(tNewLOPRTManager2Schema);

                            } else {
                                buildError("dealData", "通知书在打印队列中未首次打印");
                                return false;
                            }
                        }
                    } else {
                        System.out.println("首次打印。。。。。。。。。。。。。。。。");
                        for (int j = 1; j <= tLOPRTManager2Set.size(); j++) {
                            LOPRTManager2Schema tLOPRTManager2Schema = new
                                    LOPRTManager2Schema();
                            tLOPRTManager2Schema = tLOPRTManager2Set.get(j);
                            LOPRTManager2Schema tNewLOPRTManager2Schema = new
                                    LOPRTManager2Schema();

                            if (tLOPRTManager2Schema.getStateFlag().equals("0")) {
                                //buildError("dealData", "处于未打印状态的通知书在打印队列中只能有一个");
                                //return false;

                                mOperate = "CONFIRM";
                                tNewLOPRTManager2Schema.setPrtSeq(
                                        tLOPRTManager2Schema.getPrtSeq());
                                tNewLOPRTManager2Schema.setOtherNo(
                                        tLOPRTManager2Schema.getOtherNo());
                                tNewLOPRTManager2Schema.setOtherNoType(
                                        tLOPRTManager2Schema.getOtherNoType());
                                tNewLOPRTManager2Schema.setCode(
                                        tLOPRTManager2Schema.getCode());
                                tNewLOPRTManager2Schema.setManageCom(
                                        tLOPRTManager2Schema.getManageCom());
                                tNewLOPRTManager2Schema.setAgentCode(
                                        tLOPRTManager2Schema.getAgentCode());
                                tNewLOPRTManager2Schema.setReqCom(mGlobalInput.
                                        ComCode);
                                tNewLOPRTManager2Schema.setReqOperator(
                                        mGlobalInput.
                                        Operator);
                                tNewLOPRTManager2Schema.setExeCom(mGlobalInput.
                                        ComCode);
                                tNewLOPRTManager2Schema.setExeOperator(
                                        mGlobalInput.
                                        Operator);
                                tNewLOPRTManager2Schema.setStateFlag("1");
                                tNewLOPRTManager2Schema.setPrtType("0");
                                tNewLOPRTManager2Schema.setMakeDate(PubFun.
                                        getCurrentDate());
                                tNewLOPRTManager2Schema.setMakeTime(PubFun.
                                        getCurrentTime());
                                tNewLOPRTManager2Schema.setDoneDate(PubFun.
                                        getCurrentDate());
                                tNewLOPRTManager2Schema.setDoneTime(PubFun.
                                        getCurrentTime());
                                tNewLOPRTManager2Schema.setStandbyFlag1(
                                        tLOPRTManager2Schema.getStandbyFlag1());
                                tNewLOPRTManager2Schema.setStandbyFlag2(
                                        tLOPRTManager2Schema.getStandbyFlag2());
                                tNewLOPRTManager2Schema.setStandbyFlag3(
                                        tLOPRTManager2Schema.getStandbyFlag3());
                                tNewLOPRTManager2Schema.setStandbyFlag4(
                                        tLOPRTManager2Schema.getStandbyFlag4());
                                tNewLOPRTManager2Schema.setOldPrtSeq(
                                        tLOPRTManager2Schema.getOldPrtSeq());

                                mLOPRTManager2Set.add(tNewLOPRTManager2Schema);
                            } else {
                                buildError("dealData", "处于未打印状态的通知书在打印队列中只能有一个");
                                return false;
                            }

                        }
                    }

                } else {
                    String strNoLimit = PubFun.getNoLimit(mGlobalInput.ComCode);
                    mLOPRTManager2Schema.setPrtSeq(PubFun1.CreateMaxNo(
                            "PRTSEQ2NO", strNoLimit));
                    mLOPRTManager2Schema.setReqCom(mGlobalInput.ComCode);
                    mLOPRTManager2Schema.setReqOperator(mGlobalInput.Operator);
                    mLOPRTManager2Schema.setExeCom(mGlobalInput.ComCode);
                    mLOPRTManager2Schema.setExeOperator(mGlobalInput.Operator);
                    mLOPRTManager2Schema.setStateFlag("1");
                    mLOPRTManager2Schema.setPrtType("0");
                    mLOPRTManager2Schema.setMakeDate(PubFun.getCurrentDate());
                    mLOPRTManager2Schema.setMakeTime(PubFun.getCurrentTime());
                    mLOPRTManager2Schema.setDoneDate(PubFun.getCurrentDate());
                    mLOPRTManager2Schema.setDoneTime(PubFun.getCurrentTime());

                }

            } else {
                LOPRTManager2DB tLOPRTManager2DB = new LOPRTManager2DB();
                LOPRTManager2Schema tLOPRTManager2Schema = new
                        LOPRTManager2Schema();
                tLOPRTManager2DB.setCode(mLOPRTManager2Schema.getCode());
                tLOPRTManager2DB.setOtherNo(mLOPRTManager2Schema.getOtherNo());
                tLOPRTManager2DB.setOtherNoType(mLOPRTManager2Schema.
                                                getOtherNoType());
                tLOPRTManager2DB.setManageCom(mLOPRTManager2Schema.getManageCom());
                tLOPRTManager2DB.setAgentCode(mLOPRTManager2Schema.getAgentCode());
                tLOPRTManager2DB.setStandbyFlag1(mLOPRTManager2Schema.
                                                 getStandbyFlag1());
                tLOPRTManager2DB.setStandbyFlag2(mLOPRTManager2Schema.
                                                 getStandbyFlag2());
                //tLOPRTManager2DB.setStateFlag("0");

                LOPRTManager2Set tLOPRTManager2Set = tLOPRTManager2DB.query();

                if (tLOPRTManager2Set == null) {
                    buildError("dealData", "查询LOPRTManager2表时出现错误");
                    return false;
                }
                if (tLOPRTManager2Set.size() != 0) {
                    tLOPRTManager2Schema.setSchema(tLOPRTManager2Set.get(1));
                    if (mRePrintFlag.equals("1")) { //再次打印
                        System.out.println("======再次打印======");
                        if (tLOPRTManager2Schema.getStateFlag().equals("1")) {
                            //buildError("dealData", "处于未打印状态的通知书在打印队列中只能有一个");
                            //return false;

                            String strPrtTime = tLOPRTManager2Schema.
                                                getStandbyFlag1(); //StandbyFlag1记录打印次数
                            int intPrtTime = 0;
                            if (strPrtTime == null) {
                                mLOPRTManager2Schema.setStandbyFlag1("1");
                            } else if (strPrtTime.equals("")) {
                                mLOPRTManager2Schema.setStandbyFlag1("1");
                            } else {
                                intPrtTime = Integer.parseInt(strPrtTime);
                                intPrtTime++;
                                mLOPRTManager2Schema.setStandbyFlag1(intPrtTime +
                                        "");
                            }
                            mOperate = "CONFIRM";
                            mLOPRTManager2Schema.setPrtSeq(tLOPRTManager2Schema.
                                    getPrtSeq());
                            mLOPRTManager2Schema.setReqCom(mGlobalInput.ComCode);
                            mLOPRTManager2Schema.setReqOperator(mGlobalInput.
                                    Operator);
                            mLOPRTManager2Schema.setExeCom(mGlobalInput.ComCode);
                            mLOPRTManager2Schema.setExeOperator(mGlobalInput.
                                    Operator);
                            mLOPRTManager2Schema.setStateFlag("1");
                            mLOPRTManager2Schema.setPrtType("0");
                            mLOPRTManager2Schema.setMakeDate(PubFun.
                                    getCurrentDate());
                            mLOPRTManager2Schema.setMakeTime(PubFun.
                                    getCurrentTime());
                            mLOPRTManager2Schema.setDoneDate(PubFun.
                                    getCurrentDate());
                            mLOPRTManager2Schema.setDoneTime(PubFun.
                                    getCurrentTime());
                        } else {
                            buildError("dealData", "通知书在打印队列中未首次打印");
                            return false;
                        }
                    } else {
                        System.out.println("首次打印。。。。。。。。。。。。。。。。");
                        if (tLOPRTManager2Schema.getStateFlag().equals("0")) {
                            //buildError("dealData", "处于未打印状态的通知书在打印队列中只能有一个");
                            //return false;

                            mOperate = "CONFIRM";
                            mLOPRTManager2Schema.setPrtSeq(tLOPRTManager2Schema.
                                    getPrtSeq());
                            mLOPRTManager2Schema.setReqCom(mGlobalInput.ComCode);
                            mLOPRTManager2Schema.setReqOperator(mGlobalInput.
                                    Operator);
                            mLOPRTManager2Schema.setExeCom(mGlobalInput.ComCode);
                            mLOPRTManager2Schema.setExeOperator(mGlobalInput.
                                    Operator);
                            mLOPRTManager2Schema.setStateFlag("1");
                            mLOPRTManager2Schema.setPrtType("0");
                            mLOPRTManager2Schema.setMakeDate(PubFun.
                                    getCurrentDate());
                            mLOPRTManager2Schema.setMakeTime(PubFun.
                                    getCurrentTime());
                            mLOPRTManager2Schema.setDoneDate(PubFun.
                                    getCurrentDate());
                            mLOPRTManager2Schema.setDoneTime(PubFun.
                                    getCurrentTime());
                        } else {
                            buildError("dealData", "处于未打印状态的通知书在打印队列中只能有一个");
                            return false;
                        }

                    }
                } else {
                    String strNoLimit = PubFun.getNoLimit(mGlobalInput.ComCode);
                    mLOPRTManager2Schema.setPrtSeq(PubFun1.CreateMaxNo(
                            "PRTSEQ2NO", strNoLimit));
                    mLOPRTManager2Schema.setReqCom(mGlobalInput.ComCode);
                    mLOPRTManager2Schema.setReqOperator(mGlobalInput.Operator);
                    mLOPRTManager2Schema.setExeCom(mGlobalInput.ComCode);
                    mLOPRTManager2Schema.setExeOperator(mGlobalInput.Operator);
                    mLOPRTManager2Schema.setStateFlag("1");
                    mLOPRTManager2Schema.setPrtType("0");
                    mLOPRTManager2Schema.setMakeDate(PubFun.getCurrentDate());
                    mLOPRTManager2Schema.setMakeTime(PubFun.getCurrentTime());
                    mLOPRTManager2Schema.setDoneDate(PubFun.getCurrentDate());
                    mLOPRTManager2Schema.setDoneTime(PubFun.getCurrentTime());
                }
            }
        } else if (mOperate.equals("CONFIRM")) { // 打印执行

            LOPRTManager2DB tLOPRTManager2DB = new LOPRTManager2DB();
            tLOPRTManager2DB.setPrtSeq(mLOPRTManager2Schema.getPrtSeq());
            if (!tLOPRTManager2DB.getInfo()) {
                mErrors.copyAllErrors(tLOPRTManager2DB.mErrors);
                return false;
            }

            // 查询打印队列的信息
            mLOPRTManager2Schema = tLOPRTManager2DB.getSchema();

            if (mLOPRTManager2Schema.getStateFlag() == null) {
                buildError("dealData", "无效的打印状态");
                return false;
            } else if (!mLOPRTManager2Schema.getStateFlag().equals("0")) {
                buildError("dealData", "该打印请求不是在请求状态");
                return false;
            }

            // 调用打印服务
            if (!callPrintService(mLOPRTManager2Schema)) {
                return false;
            }

            // 打印后的处理
            mLOPRTManager2Schema.setStateFlag("1");
            mLOPRTManager2Schema.setDoneDate(PubFun.getCurrentDate());
            mLOPRTManager2Schema.setDoneTime(PubFun.getCurrentTime());

        } else if (mOperate.equals("REPRINT")) { // 补打所执行的操作
            LOPRTManager2DB tLOPRTManager2DB = new LOPRTManager2DB();
            tLOPRTManager2DB.setPrtSeq(mLOPRTManager2Schema.getPrtSeq());
            if (!tLOPRTManager2DB.getInfo()) {
                mErrors.copyAllErrors(tLOPRTManager2DB.mErrors);
                return false;
            }

            // 查询打印队列的信息
            mLOPRTManager2Schema = tLOPRTManager2DB.getSchema();

            if (!callPrintService(mLOPRTManager2Schema)) {
                return false;
            }

        } else if (mOperate.equals("POSTREPRINT")) {
//     LOPRTManager2Schema tLOPRTManager2Schema = new LOPRTManager2Schema();
//     tLOPRTManager2Schema.setSchema(mLOPRTManager2Schema);
            mLOPRTManager2Schema.setExeCom(mGlobalInput.ComCode);
            mLOPRTManager2Schema.setExeOperator(mGlobalInput.Operator);
            mLOPRTManager2Schema.setDoneDate(PubFun.getCurrentDate());
            mLOPRTManager2Schema.setDoneTime(PubFun.getCurrentTime());

        } else {
            buildError("dealData", "不支持的操作字符串");
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLOPRTManager2Schema.setSchema((LOPRTManager2Schema) cInputData.
                                       getObjectByObjectName(
                                               "LOPRTManager2Schema", 0));
        mRePrintFlag = "" +
                       (String) cInputData.getObjectByObjectName("String", 2);
        System.out.println("重打标志： " + mRePrintFlag);
        mIncomeType = (String) cInputData.getObjectByObjectName("String", 3);

        if (mGlobalInput == null || mLOPRTManager2Schema == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "PrintManager2BL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用打印服务
     * @param aLOPRTManager2Schema
     * @return
     */
    private boolean callPrintService(LOPRTManager2Schema aLOPRTManager2Schema) {

        // 查找打印服务
        String strSQL = "SELECT * FROM LDCode WHERE CodeType = 'print_service'";
        strSQL += " AND Code = '" + aLOPRTManager2Schema.getCode() + "'";
        strSQL += " AND OtherSign = '0'";

        LDCodeSet tLDCodeSet = new LDCodeDB().executeQuery(strSQL);

        if (tLDCodeSet.size() == 0) {
            buildError("dealData",
                       "找不到对应的打印服务类(Code = '" + aLOPRTManager2Schema.getCode() +
                       "')");
            return false;
        }

        // 调用打印服务
        LDCodeSchema tLDCodeSchema = tLDCodeSet.get(1);

        try {
            Class cls = Class.forName(tLDCodeSchema.getCodeAlias());
            PrintService ps = (PrintService) cls.newInstance();

            // 准备数据
            String strOperate = tLDCodeSchema.getCodeName();

            VData vData = new VData();

            vData.add(mGlobalInput);
            vData.add(aLOPRTManager2Schema);

            if (!ps.submitData(vData, strOperate)) {
                mErrors.copyAllErrors(ps.getErrors());
                return false;
            }

            mResult = ps.getResult();

        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("callPrintService", ex.toString());
            return false;
        }

        return true;
    }

    public static void main(String[] args) {

        GlobalInput tGlobalInput = new GlobalInput();

        tGlobalInput.ComCode = "8611";
        tGlobalInput.ManageCom = "8611";
        tGlobalInput.Operator = "wuser";

        LOPRTManager2Schema tLOPRTManager2Schema = new LOPRTManager2Schema();
        tLOPRTManager2Schema.setOtherNo("32000197273");
        tLOPRTManager2Schema.setOtherNoType("05");
        tLOPRTManager2Schema.setCode("35");
        VData vData = new VData();
        vData.add(tGlobalInput);
        vData.add(tLOPRTManager2Schema);
        vData.add("");
        vData.add("10"); //wanglong

        PrintManager2BL tPrintManager2BL = new PrintManager2BL();
        tPrintManager2BL.submitData(vData, "REQUEST");

//        LOPRTManager2Schema tLOPRTManager2Schema = new LOPRTManager2Schema();
//        tLOPRTManager2Schema.setPrtSeq("81000006271");
//        VData vData = new VData();
//        vData.add(tGlobalInput);
//        vData.add(tLOPRTManager2Schema);
//        PrintManager2BL tPrintManager2BL = new PrintManager2BL();
//        tPrintManager2BL.submitData(vData, "REPRINT");

    }
}
