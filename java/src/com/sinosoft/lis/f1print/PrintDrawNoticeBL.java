package com.sinosoft.lis.f1print;

import java.io.File;

import com.sinosoft.lis.db.LCBnfDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LJSGetDrawDB;
import com.sinosoft.lis.llcase.CaseFunPub;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCBnfSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.schema.LJSGetDrawSchema;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.lis.vschema.LJSGetDrawSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ConfigInfo;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author LiuYansong
 * @version 1.0
 */
public class PrintDrawNoticeBL
{
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private String mOperate;
    private String mIP = "";
    String t_PrintFlag = "";
    String t_PolNo = "";
    LCPolSet mLCPolSet = new LCPolSet();
    public PrintDrawNoticeBL()
    {}


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        PrintDrawNoticeBL tPrintDrawNoticeBL = new PrintDrawNoticeBL();
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (t_PrintFlag.equals("0"))
        {
            if (!Single_Print())
            {
                return false;
            }
        }
        if (t_PrintFlag.equals("1"))
        {
            if (!Banch_Print())
            {
                return false;
            }
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    private boolean getInputData(VData cInputData)
    {
        System.out.println("在PrintDrawNoticeBL.java中获得的数据是");
        if (mOperate.equals("SINGLE_P"))
        {
            t_PrintFlag = (String) cInputData.get(0);
            t_PolNo = (String) cInputData.get(1);
            System.out.println("单独打印时接受的通知书号码是" + t_PolNo);
        }
        if (mOperate.equals("Banch_P"))
        {
            t_PrintFlag = (String) cInputData.get(0);
            mLCPolSet.set((LCPolSet) cInputData.getObjectByObjectName(
                    "LCPolSet", 0));
            System.out.println("此时接受的打印符合是" + t_PrintFlag);
            mIP = (String) cInputData.getObjectByObjectName("String", 1);
            System.out.println("IP地址是" + mIP);
        }
        return true;
    }

    private boolean Single_Print()
    {
        String s_sql = "select * from LJSGetDraw where PolNo = '" + t_PolNo +
                       "' ";
        LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
        LJSGetDrawSet tLJSGetDrawSet = new LJSGetDrawSet();
        tLJSGetDrawSet.set(tLJSGetDrawDB.executeQuery(s_sql));
        LJSGetDrawSchema tLJSGetDrawSchema = new LJSGetDrawSchema();
        if (tLJSGetDrawSet.size() > 0)
        {
            tLJSGetDrawSchema.setSchema(tLJSGetDrawSet.get(1));
            //查询投保人信息
            String t_AppntName = "";
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(t_PolNo);
            LCPolSchema tLCPolSchema = new LCPolSchema();
            if (tLCPolDB.getInfo())
            {
                tLCPolSchema.setSchema(tLCPolDB);
                if (!(tLCPolSchema.getAppntName() == null ||
                      tLCPolSchema.getAppntName().equals("")))
                {
                    t_AppntName = tLCPolSchema.getAppntName();
                }
                else
                {
                    t_AppntName = "未指定投保人";
                }
            }
            System.out.println("投保人是" + t_AppntName);
            //生成生存领取的日期转换函数
            String t_GetDate = CaseFunPub.Display_Year(tLJSGetDrawSchema.
                    getGetDate());
            System.out.println("领取日期是" + t_GetDate);
            //生存领取的金额
            double t_Money = tLJSGetDrawSchema.getGetMoney();
            System.out.println("领取金额是" + t_Money);
            //判断领取方式
            String t_LiveGetMode = "其他";
            if (!((tLCPolSchema.getLiveGetMode() == null) ||
                  tLCPolSchema.getLiveGetMode().equals("")))
            {
                LDCodeDB tLDCodeDB = new LDCodeDB();
                tLDCodeDB.setCodeType("livegetmode");
                tLDCodeDB.setCode(tLCPolSchema.getLiveGetMode());
                LDCodeSet tLDCodeSet = new LDCodeSet();
                LDCodeSchema tLDCodeSchema = new LDCodeSchema();
                tLDCodeSet.set(tLDCodeDB.query());
                if (tLDCodeSet.size() > 0)
                {
                    tLDCodeSchema.setSchema(tLDCodeSet.get(1));
                    t_LiveGetMode = tLDCodeSchema.getCodeName();
                }
            }
            System.out.println("领取方式是" + t_LiveGetMode);
            //受益人姓名
            String t_BnfName = "未指定";
            if (!(tLCPolSchema.getBnfFlag() == null ||
                  tLCPolSchema.getBnfFlag().equals("")))
            {
                if (tLCPolSchema.getBnfFlag().equals("0"))
                {
                    LCBnfDB tLCBnfDB = new LCBnfDB();
                    tLCBnfDB.setPolNo(t_PolNo);
                    tLCBnfDB.setBnfType("0");
                    tLCBnfDB.setBnfGrade("1");
                    LCBnfSchema tLCBnfSchema = new LCBnfSchema();
                    LCBnfSet tLCBnfSet = new LCBnfSet();
                    tLCBnfSet.set(tLCBnfDB.query());
                    if (tLCBnfSet.size() > 0)
                    {
                        tLCBnfSchema.setSchema(tLCBnfSet.get(1));
                        t_BnfName = tLCBnfSchema.getName();
                    }
                }
            }
            System.out.println("受益人是" + t_BnfName);
            //咨询电话和公司名称、分公司的地址
            String t_ReferPhone = "未指定";
            String t_CompanyName = "未指定";
            String t_CompanyAddr = "未指定";
            LDComDB tLDComDB = new LDComDB();
            tLDComDB.setComCode(tLCPolSchema.getManageCom());
            LDComSet tLDComSet = new LDComSet();
            tLDComSet.set(tLDComDB.query());
            if (tLDComSet.size() > 0)
            {
                LDComSchema tLDComSchema = new LDComSchema();
                tLDComSchema.setSchema(tLDComSet.get(1));
                t_ReferPhone = tLDComSchema.getPhone();
                t_CompanyName = tLDComSchema.getName();
                t_CompanyAddr = tLDComSchema.getAddress();
            }
            System.out.println("电话是" + t_ReferPhone);
            System.out.println("公司名称是" + t_CompanyName);
            System.out.println("公司地址是" + t_CompanyAddr);
            //系统日期
            String t_Date = CaseFunPub.Display_Year(PubFun.getCurrentDate());
            //领取通知书号码
            String t_GetNoticeNo = tLJSGetDrawSchema.getGetNoticeNo();
            //进行打印操作
            TextTag texttag = new TextTag(); //Create a TextTag instance
            XmlExport xmlexport = new XmlExport(); //Create a XmlExport instance
            xmlexport.createDocument("NewShengCunLQ.vts", "printer"); //appoint to a special output .vts file
            ListTable alistTable = new ListTable(); //Create a ListTable instance
            alistTable.setName("INFO");
            texttag.add("AppntName", t_AppntName);
            texttag.add("PolNo", t_PolNo);
            texttag.add("GetDate", t_GetDate);
            texttag.add("Money", t_Money);
            texttag.add("GetMode", t_LiveGetMode);
            texttag.add("Name", t_BnfName);
            texttag.add("ReferPhone", t_ReferPhone);
            texttag.add("CompanyName", t_CompanyName);
            texttag.add("CurrentDate", t_Date);
            texttag.add("GetNoticeNo", t_GetNoticeNo);

            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.outputDocumentToFile("e:\\", "printbill");

            mResult.clear();
            mResult.add(xmlexport);
        }
        return true;
    }

    private boolean Banch_Print()
    {
        mIP = mIP + ".0"; //普通模板
        System.out.println("不通模板的地址是" + mIP);
        String pathfull = ConfigInfo.GetValuebyArea(mIP);
        int intIndex = 0;
        int intPosition = 0;
        String strRecords = "";
        String servername = "";
        String pathname = "";
        String printer = "";
        String printname = ""; //单据对应的打印机名
        if ((pathfull.length() > 0))
        {
            intIndex = StrTool.getPos(pathfull, SysConst.PACKAGESPILTER,
                                      intPosition, 1);
            servername = pathfull.substring(intPosition, intIndex);
            pathfull = pathfull.substring(intIndex + 1);
            intIndex = StrTool.getPos(pathfull, SysConst.PACKAGESPILTER,
                                      intPosition, 1);
            pathname = pathfull.substring(intPosition, intIndex);
            System.out.println("pathname:" + pathname);
            pathfull = pathfull.substring(intIndex + 1);
            printer = pathfull;
        }
        else
        {
            buildError("getPrintDataGrpPer", "IP解析错误！");
            return false;
        }
        System.out.println("printer====" + printer);
        if (mLCPolSet.size() > 1)
        {
            for (int count = 1; count <= mLCPolSet.size(); count++)
            {
                LCPolSchema tLCPolSchema = new LCPolSchema();
                tLCPolSchema.setSchema(mLCPolSet.get(count));
                LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
                tLJSGetDrawDB.setPolNo(tLCPolSchema.getPolNo());
                LJSGetDrawSet tLJSGetDrawSet = new LJSGetDrawSet();
                tLJSGetDrawSet.set(tLJSGetDrawDB.query());
                if (tLJSGetDrawSet.size() > 0)
                {
                    XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
                    LJSGetDrawSchema tLJSGetDrawSchema = new LJSGetDrawSchema();
                    tLJSGetDrawSchema.setSchema(tLJSGetDrawSet.get(1));
                    //查询投保人信息
                    String t_AppntName = "未指定";
                    LCPolDB tLCPolDB = new LCPolDB();
                    tLCPolDB.setPolNo(tLCPolSchema.getPolNo());
                    tLCPolDB.getInfo();
                    LCPolSchema aLCPolSchema = new LCPolSchema();
                    aLCPolSchema.setSchema(tLCPolDB);
                    if (!(aLCPolSchema.getAppntName() == null ||
                          aLCPolSchema.getAppntName().equals("")))
                    {
                        t_AppntName = aLCPolSchema.getAppntName();
                    }
                    String t_GetDate = CaseFunPub.Display_Year(
                            tLJSGetDrawSchema.getGetDate());
                    double t_Money = tLJSGetDrawSchema.getGetMoney();
                    String t_LiveGetMode = "其他";
                    if (!((tLCPolSchema.getLiveGetMode() == null) ||
                          tLCPolSchema.getLiveGetMode().equals("")))
                    {
                        LDCodeDB tLDCodeDB = new LDCodeDB();
                        tLDCodeDB.setCodeType("livegetmode");
                        tLDCodeDB.setCode(tLCPolSchema.getLiveGetMode());
                        LDCodeSet tLDCodeSet = new LDCodeSet();
                        LDCodeSchema tLDCodeSchema = new LDCodeSchema();
                        tLDCodeSet.set(tLDCodeDB.query());
                        if (tLDCodeSet.size() > 0)
                        {
                            tLDCodeSchema.setSchema(tLDCodeSet.get(1));
                            t_LiveGetMode = tLDCodeSchema.getCodeName();
                        }
                    }
                    System.out.println("领取方式是" + t_LiveGetMode);
                    //受益人姓名
                    String t_BnfName = "未指定";
                    if (!(tLCPolSchema.getBnfFlag() == null ||
                          tLCPolSchema.getBnfFlag().equals("")))
                    {
                        if (tLCPolSchema.getBnfFlag().equals("0"))
                        {
                            LCBnfDB tLCBnfDB = new LCBnfDB();
                            tLCBnfDB.setPolNo(t_PolNo);
                            tLCBnfDB.setBnfType("0");
                            tLCBnfDB.setBnfGrade("1");
                            LCBnfSchema tLCBnfSchema = new LCBnfSchema();
                            LCBnfSet tLCBnfSet = new LCBnfSet();
                            tLCBnfSet.set(tLCBnfDB.query());
                            if (tLCBnfSet.size() > 0)
                            {
                                tLCBnfSchema.setSchema(tLCBnfSet.get(1));
                                t_BnfName = tLCBnfSchema.getName();
                            }
                        }
                    }
                    System.out.println("受益人是" + t_BnfName);
                    //咨询电话和公司名称、分公司的地址
                    String t_ReferPhone = "未指定";
                    String t_CompanyName = "未指定";
                    String t_CompanyAddr = "未指定";
                    LDComDB tLDComDB = new LDComDB();
                    tLDComDB.setComCode(tLCPolSchema.getManageCom());
                    LDComSet tLDComSet = new LDComSet();
                    tLDComSet.set(tLDComDB.query());
                    if (tLDComSet.size() > 0)
                    {
                        LDComSchema tLDComSchema = new LDComSchema();
                        tLDComSchema.setSchema(tLDComSet.get(1));
                        t_ReferPhone = tLDComSchema.getPhone();
                        t_CompanyName = tLDComSchema.getName();
                        t_CompanyAddr = tLDComSchema.getAddress();
                    }
                    System.out.println("电话是" + t_ReferPhone);
                    System.out.println("公司名称是" + t_CompanyName);
                    System.out.println("公司地址是" + t_CompanyAddr);
                    //系统日期
                    String t_Date = CaseFunPub.Display_Year(PubFun.
                            getCurrentDate());
                    //领取通知书号码
                    String t_GetNoticeNo = tLJSGetDrawSchema.getGetNoticeNo();
                    xmlexport.createDocument("NewShengCunLQ.vts", printer);
                    TextTag texttag = new TextTag(); //新建一个TextTag的实例

                    texttag.add("AppntName", t_AppntName);
                    texttag.add("PolNo", aLCPolSchema.getPolNo());
                    texttag.add("GetDate", t_GetDate);
                    texttag.add("Money", t_Money);
                    texttag.add("GetMode", t_LiveGetMode);
                    texttag.add("Name", t_BnfName);
                    texttag.add("ReferPhone", t_ReferPhone);
                    texttag.add("CompanyName", t_CompanyName);
                    texttag.add("CurrentDate", t_Date);
                    texttag.add("GetNoticeNo", t_GetNoticeNo);

                    if (texttag.size() > 0)
                    {
                        xmlexport.addTextTag(texttag);
                    }
                    File tFileXml = new File(pathname);
                    System.out.println("Xml.pathname==" + pathname);
                    if (!tFileXml.exists())
                    {
                        tFileXml.mkdirs();
                    }
                    xmlexport.outputDocumentToFile(pathname,
                            aLCPolSchema.getPolNo());
                    mResult.clear();
                    mResult.add(xmlexport);
                }
            }
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GrpPersonBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        PrintDrawNoticeBL aPrintDrawNoticeBL = new PrintDrawNoticeBL();
        String PolNo = "86110020030210000107";
        String PrintFlag = "0";
        VData tVData = new VData();
        tVData.addElement(PrintFlag);
        tVData.addElement(PolNo);
        aPrintDrawNoticeBL.submitData(tVData, "SINGLE_P");
    }
}