package com.sinosoft.lis.f1print;

import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PlSolvencyPrintBL {
	public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private String strBeginDate="";     //统计起期
    private String strEndDate="";       //统计止期
    private String year="";             //当前年度
    private String mManageCom = "";		//选择打印机构编码
    private String strComCode = "";		//当前登录机构编码

    public PlSolvencyPrintBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {

        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();
        if (!getPrintData()) {
            return false;
        }

        return true;
    }

    private boolean getInputData(VData cInputData) {
    	strBeginDate=(String) cInputData.get(0);
    	strEndDate=(String) cInputData.get(1);
    	year=(String) cInputData.get(2);
    	mGlobalInput=(GlobalInput) cInputData.get(3);
    	mManageCom=(String) cInputData.get(4);
    	System.out.println(mManageCom);
    	strComCode=mGlobalInput.ComCode;
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "PlSolvencyPrintBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    private boolean getPrintData() 
    {
    	SSRS tSSRS = new SSRS();
    	ExeSQL tExeSQL = new ExeSQL();
		String nsql = "select Name from LDCom where ComCode='"
			+ mManageCom + "'"; // 管理机构
	    tSSRS = tExeSQL.execSQL(nsql);
	    String manageCom = tSSRS.GetText(1, 1);
	    
		TextTag texttag = new TextTag(); // 新建一个TextTag的实例
		texttag.add("MANAGECOM", manageCom);
		texttag.add("YEAR", year);
		XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
		xmlexport.createDocument("PlSolvencyPrint.vts", "printer"); // 最好紧接着就初始化xml文档
		xmlexport.addTextTag(texttag);
		String strArr[]=null;
		if (texttag.size() > 0) 
		{
			xmlexport.addTextTag(texttag);
		}
		String[] detailArr = new String[] {"序号","出租人为关联方的经营性租入的房屋","年","月","省","市","租赁期","出租人","本期支付的租金总额"};
		String sql1="";   //出租方为关联方
		String sql2="";   //出租方为非关联方
		double sum1=0.00;  //出租方为关联方租金总金额
		double sum2=0.00;  //出租方为非关联方租金总金额
			
		sql1 = " select * from (SELECT '1.1.' || trim(char(rownumber() over(order by Begindate desc))) as rolnum,";
		sql2 = " select * from (SELECT '1.2.' || trim(char(rownumber() over(order by Begindate desc))) as rolnum,";
		String sql=" a.Address,YEAR(a.Begindate),MONTH(a.Begindate),a.Province,a.City,"+
           " (year(a.Actualenddate)-year(a.Begindate))*12+ month(a.Actualenddate)-month(a.Begindate)+1,a.Lessor,"+
           " (select sum(money) from LIPlaceRentFee where placeno=a.Placeno and Feetype='01' and PayDate between '"+strBeginDate+"' and '"+strEndDate+"') FROM LIPlaceRentInfo a where 1 = 1";
		if(!"".equals(strBeginDate) && !"".equals(strEndDate))
		{
			sql += " and exists(select 1 from LIPlaceRentFee where placeno=a.Placeno and Feetype='01' and PayDate between '"+strBeginDate+"' and '"+strEndDate+"')";
		}
		if(!"".equals(mManageCom) && mManageCom != null)
		{
			sql += " and a.managecom like '" +mManageCom +"%'";
		}
		sql += " and state <>'02'"; 
		
		sql1=sql1+sql+"and Islessorassociate='1' order by a.Begindate desc) as aa";  //出租方为关联方
		
		sql2=sql2+sql+"and Islessorassociate='0' order by a.Begindate desc) as aa";  //出租方为非关联方
		
		System.out.println("出租方为关联方sql:"+sql1);
		System.out.println("出租方为非关联方sql:"+sql2);
		
		for (int w = 0; w < 2; w++) {
			ExeSQL main_exesql = new ExeSQL();
			ListTable tDetailListTable = new ListTable(); // 明细ListTable
			SSRS main_ssrs=null;
			if (w == 0) {
			    main_ssrs = main_exesql.execSQL(sql1);
				tDetailListTable.setName("INFO");
			}else {
			    main_ssrs = main_exesql.execSQL(sql2);
				tDetailListTable.setName("INFO2");
			}
			System.out.println(main_ssrs.MaxRow);//行数
            System.out.println(main_ssrs.MaxCol);//列数
			for(int i=1;i<=main_ssrs.MaxRow;i++)
			{
				strArr = new String[9];
				for(int j=1;j<=main_ssrs.MaxCol;j++)
				{
						if(j==9){
	            			strArr[j - 1] = main_ssrs.GetText(i, j);
	        				if(strArr[j - 1]!=null && (!"".equals(strArr[j - 1])))
	        				{
	        					double mDouble=Double.valueOf(strArr[j - 1]).doubleValue();
	        					if (w == 0) {
	        						sum1=sum1 + mDouble;
								} else {
									sum2=sum2 + mDouble;
								}
	        					String strSum = new DecimalFormat("0.00").format(Double.valueOf(strArr[j - 1]));
	        				    strArr[j - 1] = strSum;
	        				 }
	            		}else{
            			strArr[j - 1] = main_ssrs.GetText(i, j);
            		}
				}
				tDetailListTable.add(strArr);
			}
			xmlexport.addListTable(tDetailListTable, detailArr); // 明细ListTable
			// 放入xmlexport对象
			
		}
		String sumz = new DecimalFormat("0.00").format(sum1+sum2);
		String sumo = new DecimalFormat("0.00").format(sum1);
		String sumt = new DecimalFormat("0.00").format(sum2);
		System.out.println("sumz:"+sumz+";sumo:"+sumo+";sumt:"+sumt);
		texttag.add("SUM", sumz);
		texttag.add("SUM1", sumo);
		texttag.add("SUM2", sumt);
		texttag.add("ZSUM", sumz);              
		if (texttag.size() > 0) {
			xmlexport.addTextTag(texttag);
		}
		mResult.clear();
		mResult.addElement(xmlexport);

		return true;

    }
}
