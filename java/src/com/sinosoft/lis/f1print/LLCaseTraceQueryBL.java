package com.sinosoft.lis.f1print; 

import java.io.InputStream;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;


public class LLCaseTraceQueryBL {
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mManageCom = "";
    
    private String mOperator = "";

    private VData mInputData = new VData();

    private String mOperate = "";

    private TransferData mTransferData = new TransferData();

    private XmlExport mXmlExport = null;

    private String mFileNameB = "";

    /** 案件号 */
    private String mCaseNo = "";

    private CSVFileWrite mCSVFileWrite = null;
    private String mFilePath = "";
    private String mFileName = "";
    
    public static void main(String[] args) {
    	LLCaseTraceQueryBL tLLCaseTraceQueryBL = new LLCaseTraceQueryBL();
    	TransferData tTransferData= new TransferData();
//    	传参
    	tTransferData.setNameAndValue("tManageCom","86110000");
    	tTransferData.setNameAndValue("tCaseNo","C1100141020000011");
    	tTransferData.setNameAndValue("tOperator","cm001");
    	tTransferData.setNameAndValue("tFileNameB","Test.cvs");

    	GlobalInput tG = new GlobalInput();
    	VData tVData = new VData();
    	tVData.addElement(tG);
    	tVData.addElement(tTransferData);
    	
    	tLLCaseTraceQueryBL.submitData(tVData, "PRINT");
	}
    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
    	System.out.println("<-Go Into LLCaseTraceQueryBL->");

        mOperate = cOperate;
        mInputData = (VData) cInputData;
        if (mOperate.equals("")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(mInputData)) {
            return false;
        }
        System.out.println("打印报表机构："
                           + this.mManageCom);
        
    
        // 进行数据查询
        if (!queryDataToCVS()){
            return false;
        } else {

        }

        System.out.println("结束理赔案件轨迹查询");

        return true;
    }

    private void bulidError(String cFunction, String cErrorMsg) {

        CError tCError = new CError();

        tCError.moduleName = "LLCasePolicy";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;

        this.mErrors.addOneError(tCError);

    }

    /**
     * 获取前台传入的数据
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

    	System.out.println("<-Go Into getInputData()->");
        try {
            mGlobalInput.setSchema((GlobalInput) cInputData
                                   .getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            // 页面传入的数据
            mOperator = (String) mTransferData.getValueByName("tOperator");
            mFileNameB = (String) mTransferData.getValueByName("tFileNameB");
            System.out.println("mOperator:"+mOperator);
            System.out.println("mFileNameB:"+mFileNameB);
            mCaseNo = (String) mTransferData.getValueByName("tCaseNo");     
            this.mManageCom = (String) mTransferData
                              .getValueByName("tManageCom");
            
        } catch (Exception ex) {
            this.mErrors.addOneError("");
            return false;
        }
        return true;
    }
 
    private boolean queryDataToCVS()
    {
    	
      int datetype = -1;
      LDSysVarDB tLDSysVarDB = new LDSysVarDB();
    	tLDSysVarDB.setSysVar("LPCSVREPORT");

    	if (!tLDSysVarDB.getInfo()) {
    		buildError("queryData", "查询文件路径失败");
    		return false;
    	}
    	
    	mFilePath = tLDSysVarDB.getSysVarValue(); 
        //   	本地测试
//    	mFilePath="F:\\";
    	if (mFilePath == null || "".equals(mFilePath)) {
    		buildError("queryData", "查询文件路径失败");
    		return false;
    	}
    	System.out.println("生成文件存放位置:"+mFilePath);
    	String tTime = PubFun.getCurrentTime3().replaceAll(":", "");
    	String tDate = PubFun.getCurrentDate2();

    	mFileName = "LLCaseTraceQuery" + mGlobalInput.Operator + tDate + tTime;
    	mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);
    
	    if (!getDataListToCVS(datetype))
	    {
	        return false;
	    }

        mCSVFileWrite.closeFile();

    	return true;
    }

    /**
     * 处理案件轨迹信息
     * @param datetype
     * @return
     */
    private boolean getDataListToCVS(int datetype)
    {
    	//判断传入的案件号类型：C-正常；R/S-申诉/纠错
    	String tCaseType = mCaseNo.substring(0, 1);
    	if("C".equals(tCaseType)){
    		String tCheckSql = "select appealno from llappeal where caseno='"+mCaseNo+"'";
    		ExeSQL tExeSQL = new ExeSQL();
    		SSRS tSSRS = new SSRS();    		
    		tSSRS = tExeSQL.execSQL(tCheckSql);
    		if(tSSRS == null || tSSRS.getMaxRow() <= 0){//无申诉、纠错案件
    			getDataList(mCaseNo);
    		}else{//有该类案件(暂不处理)
    			
    		}
    	}else{//为申诉、纠错案件，查询原案件(暂不处理)
    		String tCheckSql = "select caseno from llappeal where appealno='"+mCaseNo+"'";
    		ExeSQL tExeSQL = new ExeSQL();
    		//原始案件号
    		String tCaseNo = tExeSQL.getOneValue(tCheckSql);
    		System.out.println("原始案件号为：" + tCaseNo);
    		getDataList(mCaseNo);
    		
    	}
    	
        return true;
    }
 
    /**
     * 传入案件号，生成该案件轨迹CVS
     * @param caseNo
     * @return
     */
    private boolean getDataList(String caseNo) {
    	
        //调用写出CVS文件的公共类
        String[][] tTitle = new String[4][];
    	tTitle[0] = new String[] { "","","","","","理赔案件轨迹查询" };
    	tTitle[1] = new String[] {"查询案件号：" + caseNo};
    	tTitle[2] = new String[] {""};
    	tTitle[3] = new String[] { "操作步骤","操作开始日期","操作开始时间","操作结束日期","操作结束时间","操作人员代码","操作人员姓名","所属机构代码","所属机构名称"};
    	String[] tContentType = {"String","String","String","String","String","String","String","String","String"};
    	if (!mCSVFileWrite.addTitle(tTitle,tContentType)) {
    		return false;
    	}
    	
    	 String sql = "select b.codename,b.startdate sdate,b.starttime stime," +
    	 	"b.enddate ,b.endtime ,b.operator,b.username,b.managecom,b.managename,b.sequance " +
    	 	"from ("+
    		"select (select codename from ldcode where codetype like '%llrgtstate%' and code = a.rgtstate) codename," +
	 		"a.startdate ," +
	 		"a.starttime ," +
	 		"a.enddate ," +
	 		"a.endtime ," +
	 		"a.operator ," +
	 		"(select username from lduser where usercode = a.operator) username," +
	 		"a.managecom ," +
	 		"(select name from ldcom where comcode = a.managecom)  managename, " +
	 		"a.sequance "+
	 		"from llcaseoptime a where a.caseno = '"+caseNo+"' " +
	 		"and sequance='1' " +
	 		"order by a.startdate,a.starttime) b " +
	 		"union all " +
	 		"select b.codename,b.sdate ,b.stime ," +
    	 	"b.enddate ,b.endtime ,b.operator,b.username,b.managecom,b.managename,b.sequance " +
    	 	"from ("+
	 		"select (select codename from ldcode where codetype like '%llrgtstate%' and code = a.rgtstate) codename," +
	 		"a.enddate sdate," +
	 		"a.endtime stime," +
	 		"a.enddate ," +
	 		"a.endtime ," +
	 		"a.operator ," +
	 		"(select username from lduser where usercode = a.operator) username," +
	 		"a.managecom ," +
	 		"(select name from ldcom where comcode = a.managecom) managename, " +
	 		"a.sequance "+
	 		"from llcaseoptime a where a.caseno = '"+caseNo+"' " +
	 		"and sequance<>'1' " +
	 		"order by a.enddate,a.endtime) b order by sdate,stime with ur" ;
		 System.out.println("SQL:" + sql);
		 
		 //判断案件是否被撤件
		 String tCaseCancel = "select '撤件状态' ," +
		 		"a.modifydate ," +
		 		"a.modifytime ," +
		 		"a.modifydate ," +
		 		"a.modifytime ," +
		 		"a.cancler ," +
		 		"b.username ," +
		 		"b.comcode ," +
		 		"(select name from ldcom where comcode = b.comcode) " +
		 		"from llcase a,lduser b where a.caseno='"+caseNo+"' " +
		 		"and a.cancler = b.usercode " +
		 		"and a.CancleDate is not null "+
		 		"with ur";
		 ExeSQL tCaseCancelExeSQL = new ExeSQL();
		 SSRS tCaseCancelSSRS = new SSRS();
		 tCaseCancelSSRS = tCaseCancelExeSQL.execSQL(tCaseCancel);		
		 
		 //判断案件是否为“通知状态”
//		 modify lyc #2113 2014-08-29 调整处理人提取逻辑
		 String tCaseTZ = "select '通知状态' ," +
	 		"a.OPConfirmDate ," +
	 		"a.OPConfirmTime ," +
	 		"a.modifydate ," +
	 		"a.modifytime ," +
	 		"a.OPConfirmCode ," +
	 		"b.username ," +
	 		"b.comcode ," +
	 		"(select name from ldcom where comcode = b.comcode) " +
	 		"from ljagetclaim a,lduser b where a.otherno='"+caseNo+"' " +
	 		"and a.OPConfirmCode = b.usercode and  othernotype='5' and feeoperationtype<>'FC' " +
	 		"fetch first 1 rows only " +
	 		"with ur";
//		 add lyc 
		 ExeSQL tCaseTZExeSQL = new ExeSQL();
		 SSRS tCaseTZSSRS = new SSRS();
		 tCaseTZSSRS = tCaseTZExeSQL.execSQL(tCaseTZ);	
		 
		 //判断案件是否为“给付状态”
		 String tCaseJF = "select '给付状态' ," +
	 		"a.modifydate ," +
	 		"a.modifytime ," +
	 		"a.modifydate ," +
	 		"a.modifytime ," +
	 		"a.operator ," +
	 		"(select username from lduser where usercode=a.operator) ," +
	 		"(select comcode from lduser where usercode=a.operator) ," +
	 		"(select name from ldcom where comcode = (select comcode from lduser where usercode=a.operator) ) " +
	 		"from ljaget a where a.otherno='"+caseNo+"' " +
	 		"and a.confdate is not null "+
	 		"with ur";
		 ExeSQL tCaseJFExeSQL = new ExeSQL();
		 SSRS tCaseJFSSRS = new SSRS();
		 tCaseJFSSRS = tCaseJFExeSQL.execSQL(tCaseJF);	
		
		 ExeSQL tCaseTraceExeSQL = new ExeSQL();
		 SSRS tCaseTraceSSRS = new SSRS();
		 tCaseTraceSSRS = tCaseTraceExeSQL.execSQL(sql);   
		 try {
	         if (tCaseTraceSSRS != null || tCaseTraceSSRS.MaxRow > 0) {

	             String tContent[][] = new String[tCaseTraceSSRS.getMaxRow()+tCaseCancelSSRS.getMaxRow()+tCaseTZSSRS.getMaxRow()+tCaseJFSSRS.getMaxRow()][];
	   
	             for (int i = 1; i <= tCaseTraceSSRS.getMaxRow(); i++) {
	                 String ListInfo[] = new String[9];
	  
	                 for (int m = 0; m < ListInfo.length; m++) {
	                     ListInfo[m] = "";
	                 }
	                 String tSequance = tCaseTraceSSRS.GetText(i, 10);
	                 //案件状态
	                 ListInfo[0] = tCaseTraceSSRS.GetText(i, 1);   
	                 if(!"1".equals(tSequance)){//对于多次操作的状态，其开始时间取上一操作的‘结束时间’；结束时间取本操作的结束时间
	                	 if("".equals(tCaseTraceSSRS.GetText(i-1, 4))){
	                		 ListInfo[1] = tCaseTraceSSRS.GetText(i, 4);
	                		 ListInfo[2] = tCaseTraceSSRS.GetText(i, 5);
	                	 }else{
	                		 //开始日期
			                 ListInfo[1] = tCaseTraceSSRS.GetText(i-1, 4);
			                 //开始时间
			                 ListInfo[2] = tCaseTraceSSRS.GetText(i-1, 5);
	                	 }
	                	 
		                 //结束日期
		                 ListInfo[3] = tCaseTraceSSRS.GetText(i, 4);
		                 //结束时间
		                 ListInfo[4] = tCaseTraceSSRS.GetText(i, 5);
	                 }else{
	                	 //开始日期
		                 ListInfo[1] = tCaseTraceSSRS.GetText(i, 2);
		                 //开始时间
		                 ListInfo[2] = tCaseTraceSSRS.GetText(i, 3);
		                 
	                	 if("".equals(tCaseTraceSSRS.GetText(i, 4))){
	                		 ListInfo[3] = tCaseTraceSSRS.GetText(i, 2);
	                		 ListInfo[4] = tCaseTraceSSRS.GetText(i, 3);
	                	 }else{
			                 //结束日期
			                 ListInfo[3] = tCaseTraceSSRS.GetText(i, 4);
			                 //结束时间
			                 ListInfo[4] = tCaseTraceSSRS.GetText(i, 5);
	                	 }

	                 }
	                 
	                
	                 //操作人代码
	                 ListInfo[5] = tCaseTraceSSRS.GetText(i, 6);
	                 //操作人姓名
	                 ListInfo[6] = tCaseTraceSSRS.GetText(i, 7);
	                 //所属机构代码
	                 ListInfo[7] = tCaseTraceSSRS.GetText(i, 8);
	                 //所属机构名称
	                 ListInfo[8] = tCaseTraceSSRS.GetText(i, 9);
	          
	                 tContent[i - 1] = ListInfo;
	             }	             
	             
	             if(tCaseTZSSRS != null || tCaseTZSSRS.getMaxRow() > 0){//有通知记录
	            	 for (int i = 1; i <= tCaseTZSSRS.getMaxRow(); i++) {
		                 String ListInfo[] = new String[9];
		  
		                 for (int m = 0; m < ListInfo.length; m++) {
		                     ListInfo[m] = "";
		                 }		
		                 //案件状态
		                 ListInfo[0] = tCaseTZSSRS.GetText(i, 1);            
		                 //开始日期
		                 ListInfo[1] = tCaseTZSSRS.GetText(i, 2);
		                 //开始时间
		                 ListInfo[2] = tCaseTZSSRS.GetText(i, 3);
		                 //结束日期
		                 ListInfo[3] = tCaseTZSSRS.GetText(i, 4);
		                 //结束时间
		                 ListInfo[4] = tCaseTZSSRS.GetText(i, 5);
		                 //操作人代码
		                 ListInfo[5] = tCaseTZSSRS.GetText(i, 6);
		                 //操作人姓名
		                 ListInfo[6] = tCaseTZSSRS.GetText(i, 7);
		                 //所属机构代码
		                 ListInfo[7] = tCaseTZSSRS.GetText(i, 8);
		                 //所属机构名称
		                 ListInfo[8] = tCaseTZSSRS.GetText(i, 9);
		          
		                 tContent[tCaseTraceSSRS.getMaxRow()+tCaseTZSSRS.getMaxRow()-1] = ListInfo;
		             } 
	             }   
	             if(tCaseJFSSRS != null || tCaseJFSSRS.getMaxRow() > 0){//有给付记录
	            	 for (int i = 1; i <= tCaseJFSSRS.getMaxRow(); i++) {
		                 String ListInfo[] = new String[9];
		  
		                 for (int m = 0; m < ListInfo.length; m++) {
		                     ListInfo[m] = "";
		                 }		
		                 //案件状态
		                 ListInfo[0] = tCaseJFSSRS.GetText(i, 1);            
		                 //开始日期
		                 ListInfo[1] = tCaseJFSSRS.GetText(i, 2);
		                 //开始时间
		                 ListInfo[2] = tCaseJFSSRS.GetText(i, 3);
		                 //结束日期
		                 ListInfo[3] = tCaseJFSSRS.GetText(i, 4);
		                 //结束时间
		                 ListInfo[4] = tCaseJFSSRS.GetText(i, 5);
		                 //操作人代码
		                 ListInfo[5] = tCaseJFSSRS.GetText(i, 6);
		                 //操作人姓名
		                 ListInfo[6] = tCaseJFSSRS.GetText(i, 7);
		                 //所属机构代码
		                 ListInfo[7] = tCaseJFSSRS.GetText(i, 8);
		                 //所属机构名称
		                 ListInfo[8] = tCaseJFSSRS.GetText(i, 9);
		          
		                 tContent[tCaseTraceSSRS.getMaxRow()+tCaseTZSSRS.getMaxRow()+tCaseJFSSRS.getMaxRow()-1] = ListInfo;
		             } 
	             }
	             if(tCaseCancelSSRS != null || tCaseCancelSSRS.getMaxRow() > 0){//有撤件记录
	            	 for (int i = 1; i <= tCaseCancelSSRS.getMaxRow(); i++) {
		                 String ListInfo[] = new String[9];
		  
		                 for (int m = 0; m < ListInfo.length; m++) {
		                     ListInfo[m] = "";
		                 }
		                 //案件状态
		                 ListInfo[0] = tCaseCancelSSRS.GetText(i, 1);            
		                 //开始日期
		                 ListInfo[1] = tCaseCancelSSRS.GetText(i, 2);
		                 //开始时间
		                 ListInfo[2] = tCaseCancelSSRS.GetText(i, 3);
		                 //结束日期
		                 ListInfo[3] = tCaseCancelSSRS.GetText(i, 4);
		                 //结束时间
		                 ListInfo[4] = tCaseCancelSSRS.GetText(i, 5);
		                 //操作人代码
		                 ListInfo[5] = tCaseCancelSSRS.GetText(i, 6);
		                 //操作人姓名
		                 ListInfo[6] = tCaseCancelSSRS.GetText(i, 7);
		                 //所属机构代码
		                 ListInfo[7] = tCaseCancelSSRS.GetText(i, 8);
		                 //所属机构名称
		                 ListInfo[8] = tCaseCancelSSRS.GetText(i, 9);
		          
		                 tContent[tCaseTraceSSRS.getMaxRow()+tCaseTZSSRS.getMaxRow()+tCaseJFSSRS.getMaxRow()] = ListInfo;
		             } 
	             }
	             mCSVFileWrite.addContent(tContent);
	             if (!mCSVFileWrite.writeFile()) {
	             	mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
	             	return false;
	             }
	         }else{//若查询出无案件信息，为生成文件赋值
	         	String tContent[][] = new String[1][];
	         	String ListInfo[]={"","","","","","","","",""};	
				    ListInfo[0]="0";
				    ListInfo[1]="0";
				    ListInfo[2]="0";
				    ListInfo[3]="0";
				    ListInfo[4]="0";
				    ListInfo[5]="0";
				    ListInfo[6]="0";
				    ListInfo[7]="0";
				    ListInfo[8]="0";
				    
				    tContent[0]=ListInfo;
				    mCSVFileWrite.addContent(tContent);
				    if (!mCSVFileWrite.writeFile()) {
				    	mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
				    	return false;
				    }
	         }
	     
		 } catch (Exception ex) {
		
		 } finally {
			 
		 }
		 return true;
	}
	

    /**
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }
	public String getMFilePath() {
		return mFilePath;
	}

	public void setMFilePath(String filePath) {
		mFilePath = filePath;
	}

	public String getMFileName() {
		return mFileName;
	}

	public void setMFileName(String fileName) {
		mFileName = fileName;
	}
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLCasePolicyBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}
    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream() {
        return mXmlExport.getInputStream();
    }

}
