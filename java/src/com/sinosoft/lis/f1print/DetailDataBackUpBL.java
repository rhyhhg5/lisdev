package com.sinosoft.lis.f1print;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.adventnet.aclparser.ParseException;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.vschema.FIAboriginalGenAttPSet;

import com.sinosoft.lis.vschema.FIAboriginalMainAttPSet;


public class DetailDataBackUpBL {

	 /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    

    private String StateDate = "";
    private String EndDate = "";
    
    private String mCurDate = PubFun.getCurrentDate();

    	
	private String[][] mToExcel = null;
	
	
       

    public DetailDataBackUpBL()
    {
    }

    public boolean submitData(VData cInputData, String operate) 
    {

        if (!getInputData(cInputData))
        	
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }       

        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    public boolean dealData()
    {
        System.out.println("开始备份属性表！");   
    	//主属性表备份
        System.out.println("开始备份主属性表！");   
    	if (!dealMainAttData())
            {
                return false;
            } 
    //明细属性表备份
        System.out.println("开始备份明细属性表！");      	
    	if (!dealDetailAttData())
            {
                return false;
            } 
        System.out.println("属性表备份完成！");  
        
    	return true;
    }



private boolean dealMainAttData() {
		// TODO Auto-generated method stub
    String insertmainatt = 
		"select * from FIAboriginalMainAtt a where a.gdcardflag<>'1' " +
		"and a.fidate = '"+StateDate+"'"+
		"and not exists (select 1 from FIAboriginalMainAttP c where a.indexno=c.indexno and a.indexcode=c.indexcode  and a.mserialno = c.mserialno) ";
    
 /*   String deletemainatt = "delete  from FIAboriginalMainAtt a " +
		"where exists  (select 1 from FIAboriginalMainAttP b where a.indexno=b.indexno and a.indexcode=b.indexcode  and a.mserialno = b.mserialno ) " +
		"and a.fidate = '"+StateDate+"' and a.gdcardflag<>'1'";*/
	
    RSWrapper rsWrapper1 = new RSWrapper();    
	FIAboriginalMainAttPSet tFIAboriginalMainAttPSet = new FIAboriginalMainAttPSet();
    
	if (!rsWrapper1.prepareData(tFIAboriginalMainAttPSet, insertmainatt))
    {
        mErrors.copyAllErrors(rsWrapper1.mErrors);
        System.out.println("批量准备主属性表数据失败"+rsWrapper1.mErrors.getFirstError());
        buildError("DetailDataBackUpBL","dealMainAttData", "批量准备主属性表数据失败,提示信息为：" +rsWrapper1.mErrors.getFirstError());
        return false;
    }		
    
	do
    {
    	rsWrapper1.getData();
        if (tFIAboriginalMainAttPSet != null && tFIAboriginalMainAttPSet.size() > 0)
        {
        	System.out.println("开始往主属性表备份表插入主属性表数据！");
        	dealMainAttPData(tFIAboriginalMainAttPSet);
                     	
        }                 
    }while(tFIAboriginalMainAttPSet != null && tFIAboriginalMainAttPSet.size() > 0);          
	tFIAboriginalMainAttPSet=null;
    rsWrapper1.close();
 /*   MMap tMap= new MMap();
	VData tInputData = new VData();            	
	tMap.put(deletemainatt, "DELETE");
	tInputData.add(tMap);
	PubSubmit mPubSubmitU = new PubSubmit();
    System.out.println("开始删除主属性表已经备份过得数据！");       		
    if (!mPubSubmitU.submitData(tInputData, ""))
    {
        this.mErrors.copyAllErrors(mPubSubmitU.mErrors);
        System.out.println("删除主属性表数据报错");
        buildError("DetailDataBackUpBL","dealMainAttData", "删除主属性表数据报错,提示信息为：" +mPubSubmitU.mErrors.getFirstError());
        return false;
    }
    tInputData.clear();  
    tMap = new MMap();  */
    
    System.out.println("主属性表备份完成！");	
		
    return true;
	}


private boolean dealDetailAttData() {
// TODO Auto-generated method stub
	
    String insertdetailatt = 
		"select *  from FIAboriginalGenAtt a where a.gdcardflag<>'1' " +
		"and a.fidate = '"+StateDate+"'"+
		"and not exists (select 1 from FIAboriginalGenAttP c where a.indexno=c.indexno and a.indexcode=c.indexcode  and a.aserialno = c.aserialno) "; 
   
//明细表属性表删除           
  
  /*  String deletedetailatt = "delete  from FIAboriginalGenAtt a " +
    		"where exists  (select 1 from FIAboriginalGenAttP b where a.indexno=b.indexno and a.indexcode=b.indexcode and a.aserialno = b.aserialno ) " +
    		"and a.fidate = '"+StateDate+"' and a.gdcardflag<>'1'";*/

	RSWrapper rsWrapper2 = new RSWrapper();    
	FIAboriginalGenAttPSet tFIAboriginalGenAttPSet = new FIAboriginalGenAttPSet();
    
	if (!rsWrapper2.prepareData(tFIAboriginalGenAttPSet, insertdetailatt))
    {
        mErrors.copyAllErrors(rsWrapper2.mErrors);
        System.out.println("批量准备明细属性表数据失败"+rsWrapper2.mErrors.getFirstError());
        buildError("DetailDataBackUpBL","dealDetailAttData", "批量准备明细属性表数据失败,提示信息为：" +rsWrapper2.mErrors.getFirstError());
        return false;
    }		
    
	do
    {
    	rsWrapper2.getData();
       
    	if (tFIAboriginalGenAttPSet != null && tFIAboriginalGenAttPSet.size() > 0)
        {
        	System.out.println("开始往明细属性表备份表插入明细属性表数据！");
        	dealDetailAttPData(tFIAboriginalGenAttPSet);
            	
        }                 
    }while(tFIAboriginalGenAttPSet != null && tFIAboriginalGenAttPSet.size() > 0);        
    
	tFIAboriginalGenAttPSet=null;
    rsWrapper2.close();    
 /*   MMap tMap= new MMap();
	VData tInputData = new VData();            	
	tMap.put(deletedetailatt, "DELETE");
	tInputData.add(tMap);
	PubSubmit mPubSubmitU = new PubSubmit();
    System.out.println("开始删除明细属性表已经备份过得数据！");       		
    if (!mPubSubmitU.submitData(tInputData, ""))
    {
        this.mErrors.copyAllErrors(mPubSubmitU.mErrors);
        System.out.println("删除明细属性表数据报错");
        buildError("DetailDataBackUpBL","dealDetailAttData", "删除明细属性表数据报错,提示信息为：" +mPubSubmitU.mErrors.getFirstError());
        return false;
    }
    tInputData.clear();  
    tMap = new MMap();        */
    System.out.println("明细属性表备份完成！");	
		
    return true;
	}






private boolean dealMainAttPData(FIAboriginalMainAttPSet tFIAboriginalMainAttPSet) {
	// TODO Auto-generated method stub
	if(tFIAboriginalMainAttPSet!=null && tFIAboriginalMainAttPSet.size()>0)
	{
		MMap tMap= new MMap();
		VData tInputData = new VData();
		tMap.put(tFIAboriginalMainAttPSet, "INSERT");
		tInputData.add(tMap);
	    PubSubmit tPubSubmit = new PubSubmit();
       
	    if (!tPubSubmit.submitData(tInputData, ""))
        {
            tMap = null;
            tInputData = null;
        	this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buildError("DetailDataBackUpBL","dealMainAttPData", "往主属性表备份表插入数据时出错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
            return false;
        }
       
	    tMap = null;
        tInputData = null;
        tFIAboriginalMainAttPSet=null;

	}
    return true;
}
private boolean dealDetailAttPData(FIAboriginalGenAttPSet tFIAboriginalGenAttPSet) {
	// TODO Auto-generated method stub
	if(tFIAboriginalGenAttPSet!=null && tFIAboriginalGenAttPSet.size()>0)
	{
		MMap tMap= new MMap();
		VData tInputData = new VData();
		tMap.put(tFIAboriginalGenAttPSet, "INSERT");
		tInputData.add(tMap);
	    PubSubmit tPubSubmit = new PubSubmit();
        
	    if (!tPubSubmit.submitData(tInputData, ""))
        {
            tMap = null;
            tInputData = null;
        	this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buildError("DetailDataBackUpBL","dealDetailAttPData", "往明细属性表备份表插入数据时出错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
            return false;
        }
        
	    tMap = null;
        tInputData = null;
        tFIAboriginalGenAttPSet=null;

	}
    return true;
}

private void buildError(String szModuleName, String szFunc, String szErrMsg)
{
    CError cError = new CError();
    cError.moduleName = szModuleName;
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
    System.out.print(szErrMsg);
}

	public String[][] getMToExcel() {
		return mToExcel;
	}
	
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     * @throws java.text.ParseException 
     */
    public boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
        System.out.println(mGI.ManageCom);

        StateDate = (String) tf.getValueByName("StateDate");      
        EndDate = (String) tf.getValueByName("EndDate");
        

        if (StateDate == null || EndDate == null)
        {
            buiError("getInputData", "传入信息不完整！");
            return false;
        }
        return true;
    }
    
    private void buiError(String functionName,String errorMessage){
    	CError tError = new CError();
        tError.moduleName = "DetailDataBackUpBL";
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        mErrors.addOneError(tError);
    }
    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "cwad";
        tG.ComCode = "86";
        TransferData tTransferData = new TransferData();
//        tTransferData.setNameAndValue("OutXmlPath", "D:\\test\\a.xls");
        tTransferData.setNameAndValue("EndDate", "2012-05-01");
        tTransferData.setNameAndValue("StartDate", "");
        VData vData = new VData();
        vData.add(tG);
        vData.add(tTransferData);
        DetailDataBackUpUI tDetailDataBackUpUI = new DetailDataBackUpUI();
        if (!tDetailDataBackUpUI.submitData(vData, ""))
        {
            System.out.print("失败！");
        }
    }
	
}
