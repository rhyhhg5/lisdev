package com.sinosoft.lis.f1print;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class OmnipAnnalsTaxPrtListBL {

    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = new TransferData();
    private XmlExport xmlexport = null;
    private VData mResult = null;
    private SSRS mSSRS;
    private SSRS mRiskSSRS;
    private String mStartDate = ""; //起始日
    private String mEndDate = ""; //截止日
    private String mDealState = ""; //催收状态
    private String msql = "";


    public OmnipAnnalsTaxPrtListBL() {
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //取得外部传入数据
        if (!getInputData(cInputData)) {
            return false;
        }

        //校验数据合法性
        if (!getListData()) {
            return false;
        }

        //获取打印所需数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        try {

            mGlobalInput = (GlobalInput) cInputData.
                           getObjectByObjectName("GlobalInput", 0);
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            if (mGlobalInput == null || mTransferData == null) {
                mErrors.addOneError("传入的数据不完整。");
                return false;
            }
        } catch (Exception e) {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }
        msql = (String) mTransferData.getValueByName("tstrsql");
        System.out.println(msql);
        return true;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }

    /**
     * 查询列表显示数据
     * @param schema LCContSchema
     * @return String
     */
    private boolean getListData() {
        String tSQL = msql;
        ExeSQL tExeSQL = new ExeSQL();
        mSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获取打印所需数据
     * @param schema LCContSchema
     * @return String
     */
    private boolean getPrintData() {

        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("OmnipAnnalsTaxPrtList.vts", "printer"); //最好紧接着就初始化xml文
        String[] title = {"序", "管理机构", "保单号", "险种号", "投保人", "投保日期",
                         "结息月度", "年度结息价值", "业务员名称","业务员代码"," 业务员部门","移动电话"};

        xmlexport.addListTable(getListTable(), title);
        xmlexport.outputDocumentToFile("C:\\", "OmnipAnnalsTaxPrtList");
        mResult = new VData();
        mResult.addElement(xmlexport);

        return true;
    }

    /**
     *  获取列表数据
     * @param schema LCContSchema
     * @return String
     */
    private ListTable getListTable()
    {
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[11];
            //标记序列号
            info[0] = String.valueOf(i);
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                    info[j] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        tListTable.setName("ZT");
        return tListTable;
    }


}
