package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LJFIGetSchema;
import com.sinosoft.lis.vschema.LJFIGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class FinPayDayF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    //取得的时间
    private String mDay[] = null;
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public FinPayDayF1PBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("CONFIRM") &&
            !cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
         if (!getPrintData())
         {
            return false;
         }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "FinPayDayF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
        LJFIGetSet tLJFIGetSet = new LJFIGetSet();
        SSRS tSSRS = new SSRS();
        double SumMoney = 0;
        long SumNumber = 0;

        msql =  "select (case OtherNoType when '2' then '印刷号' when '3' then '团单批单号' when '5' then '理赔赔案号' when '6' then '个单保单号' when '8' then '团单保单号' when '10' then '个单批单号' else '其他' end) " +
                " ,CodeName,GetMoney,1,OtherNo from LJFIGet,LDCode where CodeType='paymode' and Code=PayMode and PayMode<>'4' and confdate>='" +
                mDay[0] + "' and confdate<='" + mDay[1] +
                "' and Operator='" + mGlobalInput.Operator +
                "' Order By CodeName,OtherNo ";

        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        ListTable tlistTable = new ListTable();
        String strArr[] = null;
        tlistTable.setName("Pay");
        for (int i = 1; i <= tSSRS.MaxRow; i++)
        {
            strArr = new String[5];
            for (int j = 1; j <= tSSRS.MaxCol; j++)
            {
                if (j == 1)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                }
                if (j == 2)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                }
                if (j == 3)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    String strSum = new DecimalFormat("0.00").format(Double.
                            valueOf(strArr[j - 1]));
                    strArr[j - 1] = strSum;
                }
                if (j == 4)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                }
                if (j == 5)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                }
            }
            tlistTable.add(strArr);
        }

        strArr = new String[5];
        strArr[0] = "OtherNoType";
        strArr[1] = "PayMode";
        strArr[2] = "Money";
        strArr[3] = "Mult";
        strArr[4] = "OtherNo";

        msql =  "select CodeName||'汇总',sum(GetMoney),sum(1) from LJFIGet,LDCode where CodeType='paymode' and Code=PayMode and PayMode<>'4' and confmakedate>='" +
                mDay[0] + "' and confmakedate<='" + mDay[1] +
                "' and Operator='" + mGlobalInput.Operator +
                "' Group By CodeName";

        tSSRS = tExeSQL.execSQL(msql);
        ListTable alistTable = new ListTable();
        String strSumArr[] = null;
        alistTable.setName("Sum");
        for (int i = 1; i <= tSSRS.MaxRow; i++)
        {
            strSumArr = new String[3];
            for (int j = 1; j <= tSSRS.MaxCol; j++)
            {
                if (j == 1)
                {
                    strSumArr[j - 1] = tSSRS.GetText(i, j);
                }
                if (j == 2)
                {
                    strSumArr[j - 1] = tSSRS.GetText(i, j);
                    String strSum = new DecimalFormat("0.00").format(Double.
                            valueOf(strSumArr[j - 1]));
                    strSumArr[j - 1] = strSum;
                    SumMoney = SumMoney + Double.parseDouble(strSumArr[j - 1]);
                }
                if (j == 3)
                {
                    strSumArr[j - 1] = tSSRS.GetText(i, j);
                    SumNumber = SumNumber +
                                Long.valueOf(strSumArr[j - 1]).longValue();
                }
            }
            alistTable.add(strSumArr);
        }
        String tSumMoney = String.valueOf(SumMoney);
        tSumMoney = new DecimalFormat("0.00").format(Double.valueOf(tSumMoney));
        strSumArr = new String[3];
        strArr[0] = "PayMode";
        strArr[1] = "Money";
        strArr[2] = "Mult";

        nsql = "select Name from LDCom where ComCode='" +
               mGlobalInput.ManageCom + "'";
        tSSRS = tExeSQL.execSQL(nsql);
        String manageCom = tSSRS.GetText(1, 1);

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("FinPayDay.vts", "printer"); //最好紧接着就初始化xml文档
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("ManageCom", manageCom);
        texttag.add("Operator", mGlobalInput.Operator);
        texttag.add("SumMoney", tSumMoney);
        texttag.add("SumNumber", SumNumber);
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        xmlexport.addListTable(alistTable, strSumArr);
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

}
