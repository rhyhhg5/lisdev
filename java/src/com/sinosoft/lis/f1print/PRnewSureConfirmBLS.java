package com.sinosoft.lis.f1print;

import java.sql.Connection;

import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class PRnewSureConfirmBLS
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;

    public PRnewSureConfirmBLS()
    {
    }

    public static void main(String[] args)
    {
        PRnewNoticeConfirmBLS mPRnewNoticeConfirmBLS = new
                PRnewNoticeConfirmBLS();
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Start PRnewNoticeConfirmBLS  Submit...");

        tReturn = save(cInputData);

        if (tReturn)
        {
            System.out.println("Save sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }

        System.out.println("End PRnewNoticeConfirmBLS  Submit...");

        return tReturn;
    }

//保存操作
    private boolean save(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn = DBConnPool.getConnection();

        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PRnewNoticeConfirmBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);

            // 打印管理表
            System.out.println("Start 打印管理表...");
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB(conn);
            tLOPRTManagerDB.setSchema((LOPRTManagerSchema) mInputData.
                                      getObjectByObjectName(
                    "LOPRTManagerSchema", 0));
            if (!tLOPRTManagerDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "PRnewNoticeConfirmBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "打印管理表数据更新失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            conn.commit();
            conn.close();
            System.out.println("commit end");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PRnewNoticeConfirmBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
            tReturn = false;
        }
        return tReturn;
    }
}