/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import utils.system;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CertPrintTable;
import com.cbsws.obj.LCCustomerImpartTable;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.midplat.common.DateUtil;
//import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;

/*
 * <p>ClassName: LCContF1PUI </p>
 * <p>Description: LCContF1PUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-11-04
 */
public class YBKPrintUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCContSchema mLCContSchema = new LCContSchema();

    private String mTemplatePath = null;
    private String mOutXmlPath = null;
    private String contPrintFlag = "1";//保单是否打印标记：0，不打印；1，打印
    private TransferData mTransferData = null;
    
    private String prtno = "";
    private String risk = "";

    public YBKPrintUI() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData vData) {
    	
//	    // 得到外部传入的数据，将数据备份到本类中
//	    if (!getInputData(cInputData)) {
//	        return false;
//	    }
//	    String risk = (String) cInputData.getObjectByObjectName("risk", 0);
//        String prtno = (String) cInputData.getObjectByObjectName("tPrtNo[0]", 0);
//        System.out.println("====接收参数!prtno = " + prtno);
//        System.out.println("====接收参数!risk = " + risk);
	    // 准备传往后台的数据
	    
//	    vData.add(risk);
//	    vData.add(prtno);
	    YBKPrintBL YBKPrintBL = new YBKPrintBL();
	
	    if (!YBKPrintBL.submitData(vData)) {
	    	
	        System.out.println("向后台传输数据失败!");
	        System.out.println("=======测试===========");
	        return false;
	    }
		return true;
	    
    }
    
    
    /**
     * 创建xml表
     * */
    
//    public MsgCollection creatDocument(String mPrtno) {
//		//cLogger.info("Into ICBC_WxPrint.creatDocument()...");
//		
//		String pCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
//		String pCurrentTime = DateUtil.getCurDate("HH:mm:ss");
//		
//		String cx_sql = "select insuredno From lcinsured where prtno = '"+ mPrtno +"' with ur";
//		
//		MsgCollection tMsgCollection = new MsgCollection();
//		MsgHead tMsgHead = new MsgHead();
//		tMsgHead.setBatchNo("123456789");	//
//		tMsgHead.setBranchCode("001");	//
//		tMsgHead.setMsgType("YBT_001");
//		tMsgHead.setSendDate(pCurrentDate);
//		tMsgHead.setSendTime(pCurrentTime);
//		tMsgHead.setSendOperator("ybt");
//		tMsgCollection.setMsgHead(tMsgHead);
//		CertPrintTable tCertPrintTable = new CertPrintTable();
//		tCertPrintTable.setCardNo(mPrtno);
//		tMsgCollection.setBodyByFlag("CertPrintTable", tCertPrintTable);		
//		LCCustomerImpartTable tLCCustomerImpartTable = new LCCustomerImpartTable();
//		tLCCustomerImpartTable.setInsuredNo(new ExeSQL().getOneValue(cx_sql));
//		tLCCustomerImpartTable.setImpartContent("身高(cm)");
//		tLCCustomerImpartTable.setImpartParamModle("170");
//		tMsgCollection.setBodyByFlag("LCCustomerImpartTable", tLCCustomerImpartTable);
//		return tMsgCollection;
//	}
//    

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     * @param vData VData
     * @return boolean
     */

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private static boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        prtno=(String) cInputData.getObjectByObjectName("tPrtNo[0]", 0);
        risk=(String) cInputData.getObjectByObjectName("risk", 0);
        System.out.println("aaaa" + prtno);
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCContF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args) {
        String SQl = "select contno from lccont where prtno in('16000050005')";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(SQl);
        for (int m = 1; m <= tSSRS.getMaxRow(); m++) {
            System.out.println("tSSRS" + tSSRS.GetText(m, 1));
            YBKPrintUI tLCContF1PUI = new YBKPrintUI();
//        LCContSet tLCContSet = new LCContSet();
            LCContSchema tLCContSchema = new LCContSchema();
//        tLCContSchema.setContNo("230110000000051");
            tLCContSchema.setContNo(tSSRS.GetText(m, 1));
//        tLCContSet.add(tLCContSchema);
            GlobalInput tGlobalInput = new GlobalInput();
            tGlobalInput.Operator = "UW0009";
            tGlobalInput.ManageCom = "86110000";
            VData vData = new VData();
            vData.addElement(tGlobalInput);
            vData.addElement(tLCContSchema);
            vData.addElement("E:\\01.PICC核心业务系统\\03.lis\\ui\\f1print\\template\\");
            vData.addElement("E:\\01.PICC核心业务系统\\03.lis\\ui\\");
            //tLCContF1PUI.submitData(vData, "PRINT");

        }

    }
}
