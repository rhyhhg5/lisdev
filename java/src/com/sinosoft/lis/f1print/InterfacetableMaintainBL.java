package com.sinosoft.lis.f1print;


//import com.sinosoft.pubfun.GlobalInput;
//import com.sinosoft.pubfun.MMap;
//import com.sinosoft.pubfun.PubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class InterfacetableMaintainBL {
	 /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private String BatchNo = "";
    private String SerialNo = "";
    private String CostCenter = "";
    private String Chinal = "";
    private String ChargeDate = "";
    private String ReadState = "";
    private String StateF = "";
    private String mOperate = "";
    
    private String mCurDate = PubFun.getCurrentDate();
    	
	private String[][] mToExcel = null;
	
	private String mMail = "";
       

    public InterfacetableMaintainBL()
    {
    }

    public boolean submitData(VData cInputData, String mOperate) 
    {
    	this.mOperate = mOperate;
    	
    	//获取数据
        if (!getInputData(cInputData))
        {
            return false;
        }
      //检查数据
        if (!checkData())
        {
        	return false;
        }
      //业务处理
        if (!dealData())
        {	
            return false;
        }
        
		if(!createFile()){
			return false;
		}        

        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    public boolean dealData()
    {
        System.out.println("BL->dealData()"); 
    	//提交后台处理
        MMap tmap = new MMap();
        VData tInputData = new VData();
        //System.out.println("CheckFlag : "+CheckF);
        
        
        if(mOperate.equals("UPDATE")){
        	if (CostCenter != null && !CostCenter.equals("") && Chinal != null && !Chinal.equals("")) {
            	tmap.put("update interfacetable set CostCenter = '"+CostCenter+"',Chinal= '"+Chinal+"' where batchno = '"+BatchNo+"'", "UPDATE");
            } 
        	
        }else if(mOperate.equals("DELETE")){
        	
        	tmap.put("delete interfacetable where batchno = '"+BatchNo+"'", "DELETE");
        	
        }else if(mOperate.equals("INSERT")){
 //insert into interfacetable(batchno,serialno,CostCenter,Chinal,ChargeDate,ReadState) values('Y0000000000000008050','','9210910300','01','','')
        	
        	String sql="insert into interfacetable (BatchNo,SerialNo,CostCenter,Chinal,ChargeDate) "
        			+ "values('"+BatchNo+"','"+SerialNo+"','"+CostCenter+"','"+Chinal+"','"+ChargeDate+"')";
        	
        	tmap.put(sql, "INSERT");

        }
        
        tInputData.add(tmap);
        System.out.println(tmap+"******************");       
        PubSubmit tPubSubmit = new PubSubmit();     
        if (!tPubSubmit.submitData(tInputData, ""))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buiError( "gatherData", "数据出错，提示信息为：" + tPubSubmit.mErrors.getFirstError());         
            tmap=null;
            tInputData=null;
            return false;
        }
        
    	tmap=null;
        tInputData=null;
        return true;
  }
    

    
	private boolean createFile(){
		return true;
	}

	public String[][] getMToExcel() {
		return mToExcel;
	}
	
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     * @throws java.text.ParseException 
     */
    public boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
        System.out.println(mGI+"...............");

        if (mGI == null || tf == null)
        {
        	buiError("getInputData","数据维护出错");
            return false;
        }
        BatchNo = (String) tf.getValueByName("BatchNo");
        SerialNo = (String) tf.getValueByName("SerialNo");
        CostCenter = (String) tf.getValueByName("CostCenter");
        Chinal = (String) tf.getValueByName("Chinal");
        ChargeDate = (String) tf.getValueByName("ChargeDate");
        ReadState = (String) tf.getValueByName("ReadState");
        mOperate = (String) tf.getValueByName("mOperate");
        
        System.out.println("BatchNo-----"+BatchNo);
        System.out.println("SerialNo-----"+SerialNo);
        System.out.println("CostCenter-----"+CostCenter);
        System.out.println("Chinal-----"+Chinal);
        System.out.println("ChargeDate-----"+ChargeDate);
        System.out.println("ReadState-----"+ReadState);
        System.out.println("mOperate-----"+mOperate);

        return true;
    }
    
    private void buiError(String functionName,String errorMessage){
    	CError tError = new CError();
        tError.moduleName = "InterfacetableMaintainBL";
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        mErrors.addOneError(tError);
    }
    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "cwad";
        tG.ComCode = "86";
        TransferData tTransferData = new TransferData();
        VData vData = new VData();
        vData.add(tG);
        vData.add(tTransferData);
        PreRecPrintUI tPreRecPrintUI = new PreRecPrintUI();
        if (!tPreRecPrintUI.submitData(vData, ""))
        {
            System.out.print("失败！");
        }
    }
}


