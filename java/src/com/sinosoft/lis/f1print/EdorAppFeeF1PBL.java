package com.sinosoft.lis.f1print;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LPEdorAppDB;
import com.sinosoft.lis.db.LPEdorMainDB;
import com.sinosoft.lis.db.LPGrpEdorMainDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LPEdorAppSchema;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LPEdorMainSet;
import com.sinosoft.lis.vschema.LPGrpEdorMainSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class EdorAppFeeF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();


    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();

    //打印相关变量
    private ListTable tlistTable;
    private String tEdorAcceptNo = "";
    private String tEdorNo = "";
    private String tGetNoticeNo = "";
    private String tAppntNo = "";
    private String tManageCom = "";
    private String tManagePhone = "";
    private String tSumDuePayMoney = "";
    private String tAgentcode = "";
    private String tEdorAppDate = "";
    private String tEdorNames = "";
    private String tAppName = "";
    private String tContNo = "";
    private double tGetMoneyForPay = 0; //收费明细总和
    private double tGetMoneyForGet = 0; //退费明细总和
    private double tGetMoneyBF = 0;
    private double tGetMoneyLX = 0;
    private double tGetMoneyGB = 0;
    private double tGetMoneyTB = 0;


    public EdorAppFeeF1PBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLPEdorAppSchema.setSchema((LPEdorAppSchema) cInputData
                                   .getObjectByObjectName("LPEdorAppSchema",
                0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "EdorFeeF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        tEdorAcceptNo = mLPEdorAppSchema.getEdorAcceptNo();
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(tEdorAcceptNo);
        if (!tLPEdorAppDB.getInfo())
        {
            buildError("getPrintData", "LPEdorApp表中没有相关的记录！");
            return false;
        }

        String strSql = "select * from ljspay where otherno = '" +
                        tEdorAcceptNo +
                        "'";
        LJSPayDB tPayDB = new LJSPayDB();
        LJSPaySet tPaySet = tPayDB.executeQuery(strSql);
        if (tPaySet != null && tPaySet.size() == 1)
        {
            tGetNoticeNo = tPaySet.get(1).getGetNoticeNo();
            tAppntNo = tPaySet.get(1).getAppntNo();
            tSumDuePayMoney = Double.toString(tPaySet.get(1).getSumDuePayMoney());
            tAgentcode = tPaySet.get(1).getAgentCode();
            tManageCom = tPaySet.get(1).getManageCom();
        }
        else
        {
            buildError("getPrintData", "LJSPay表中没有相关的记录！");
            return false;
        }
        //取得机构电话
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tManageCom);
        if (!tLDComDB.getInfo())
        {
            buildError("getPrintData", "LDCom表中没有相关的机构记录！");
        }
        else
        {
            tManagePhone = tLDComDB.getPhone();
        }

        //判断保全是团单还是个单，true--团单，false--个单
        boolean isGrp = true;
        String sql = "select * from lpedormain where edoracceptno ='" +
                     tEdorAcceptNo + "' ";
        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        LPEdorMainSet tLPEdorMainSet = tLPEdorMainDB.executeQuery(sql);

        sql = "select * from lpgrpedormain where edoracceptno ='" +
              tEdorAcceptNo + "'";
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        LPGrpEdorMainSet tLPGrpEdorMainSet = tLPGrpEdorMainDB.executeQuery(sql);
        if ((tLPEdorMainSet == null || tLPEdorMainSet.size() < 1) &&
            (tLPGrpEdorMainSet == null || tLPGrpEdorMainSet.size() < 1))
        {
            buildError("getPrintData",
                       "EdorAcceptNo:" + tEdorAcceptNo + "下无批单记录");
            return false;
        }

        //取打印所需的值
        tlistTable = new ListTable();
        tlistTable.setName("Fee");
        String[] printArray = new String[2];
        if (tLPEdorMainSet != null)
        {
            for (int i = 0; i < tLPEdorMainSet.size(); i++)
            {
                printArray[0] = "保单号:" + tLPEdorMainSet.get(i + 1).getContNo();
                if (tLPEdorMainSet.get(i + 1).getGetMoney() <= 0)
                {
                    printArray[1] = "应退" +
                                    Double.toString( -tLPEdorMainSet.get(i + 1).
                            getGetMoney()) + "元";
                }
                else
                {
                    printArray[1] = "应收" +
                                    tLPEdorMainSet.get(i + 1).getGetMoney() +
                                    "元";
                }
                tlistTable.add(printArray);
            }
        }
        if (tLPGrpEdorMainSet != null)
        {
            for (int i = 0; i < tLPGrpEdorMainSet.size(); i++)
            {
                printArray[0] = "保单号:" +
                                tLPGrpEdorMainSet.get(i + 1).getGrpContNo();
                if (tLPEdorMainSet.get(i + 1).getGetMoney() <= 0)
                {
                    printArray[1] = "应退" +
                                    Double.toString( -tLPGrpEdorMainSet.
                            get(i + 1).getGetMoney()) + "元";
                }
                else
                {
                    printArray[1] = "应收" +
                                    tLPGrpEdorMainSet.get(i + 1).getGetMoney() +
                                    "元";
                }
                tlistTable.add(printArray);
            }

        }

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("EdorAppFeeNotice.vts", "printer"); //最好紧接着就初始化xml文档

        if (tLPEdorAppDB.getGetMoney() <= 0)
        {
            texttag.add("FeeInfo", "退费信息");
            texttag.add("SumMoney",
                        "本次变更应退费合计：" +
                        Double.toString( -tLPEdorAppDB.getGetMoney()) + "元");
        }
        else
        {
            texttag.add("FeeInfo", "收费信息");
            texttag.add("SumMoney",
                        "本次变更应收费合计：" + tLPEdorAppDB.getGetMoney() + "元");
        }
        texttag.add("GetNoticeNo", tGetNoticeNo);

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }

        String[] headArr = new String[2];
        headArr[0] = "保单号";
        headArr[1] = "费用";
        xmlexport.addListTable(tlistTable, headArr);

        //xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    public static void main(String[] args)
    {
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();
        mLPEdorAppSchema.setEdorAcceptNo("86000000000222");
        tVData.addElement(mLPEdorAppSchema);
        EdorAppFeeF1PBL edorFeeF1PBL1 = new EdorAppFeeF1PBL();
        edorFeeF1PBL1.submitData(tVData, "PRINT");
    }
}
