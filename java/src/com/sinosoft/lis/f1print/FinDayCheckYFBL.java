package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author  hzy lys
 * @date:2003-06-04
 * @version 1.0
 */
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Vector;

import com.sinosoft.lis.bl.LCGrpPolBL;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LJABonusGetDB;
import com.sinosoft.lis.db.LJAGetClaimDB;
import com.sinosoft.lis.db.LJAGetDrawDB;
import com.sinosoft.lis.db.LJAGetEndorseDB;
import com.sinosoft.lis.db.LJAGetOtherDB;
import com.sinosoft.lis.db.LJAGetTempFeeDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJABonusGetSchema;
import com.sinosoft.lis.schema.LJAGetClaimSchema;
import com.sinosoft.lis.schema.LJAGetDrawSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LJAGetOtherSchema;
import com.sinosoft.lis.schema.LJAGetTempFeeSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJABonusGetSet;
import com.sinosoft.lis.vschema.LJAGetClaimSet;
import com.sinosoft.lis.vschema.LJAGetDrawSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LJAGetOtherSet;
import com.sinosoft.lis.vschema.LJAGetTempFeeSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CodeJudge;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;

public class FinDayCheckYFBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mDay[] = null; //获取时间
    private String mRiskCode;
    private String mRiskType;
    private String mXsqd;
    private String mXzdl;
    private String mSxq;
    private String mDqj;
    private LMRiskAppSet mLMRiskAppSet;
    private String mRiskName = "";
    private GlobalInput mGlobalInput = new GlobalInput(); //全局变量
    public FinDayCheckYFBL()
    {
        mErrors=new CErrors();
        mResult=new VData();
        mDay=null;
        mRiskName="";
        mGlobalInput=new GlobalInput();

    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86110000";
        VData vData = new VData();
        String[] tDay = new String[2];
        tDay[0] = "2004-05-14";
        tDay[1] = "2004-05-14";
        vData.addElement(tDay);
        vData.addElement(tG);

        FinDayCheckYFBL tF = new FinDayCheckYFBL();
        tF.submitData(vData, "PRINTPAY");

    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        this.mLMRiskAppSet = tLMRiskAppDB.query();

        if (cOperate.equals("PRINTPAY")) //打印收费
        {
            if (!getPrintDataPay())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) //打印付费
    {
        //全局变量
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FinDayCheckBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintDataPay()
    {
        SSRS tSSRS = new SSRS();
        SSRS nSSRS = new SSRS();
        double ESumMoney = 0;
        long ESumNumber = 0;
        double OSumMoney = 0;
        long OSumNumber = 0;
        double SumMoney = 0;
        long SumNumber = 0;

//        String msql = "select distinct c.edorname,a.endorsementno,-sum(a.getmoney),1,'保全支出',a.feeoperationtype " +
//        "from LJAGetEndorse a,LJAGet b,LMEdorItem c where a.getflag='1' and c.edorcode=a.feeoperationtype and b.Makedate>='" +
//               mDay[0] + "' and b.Makedate<='" + mDay[1] +
//               "' and b.confdate is null and b.enteraccdate is null and b.ManageCom like'" + mGlobalInput.ManageCom +
//               "%' and((a.othernotype = '10' and c.appobj = 'I') or (a.othernotype = '3' and c.appobj = 'G'))"+
//               " and b.paymode  not in ('B','J') and b.actugetno = a.actugetno group by a.feeoperationtype,c.edorname,a.endorsementno "+
//               " union all select distinct c.edorname,a.endorsementno,-sum(a.getmoney),1, '定期结算保全支出',a.feeoperationtype from LJAGetEndorse a,LMEdorItem c where "+
//                "   a.actugetno in (  Select Btactuno from ljaedorbaldetail where "
//                + " actuno in (  Select actugetno from LJAGet where  othernotype='13' and sumgetmoney <> 0 and Makedate>='" +mDay[0] + "' and Makedate<='" + mDay[1] +"' and ManageCom like '" + mGlobalInput.ManageCom + "%'  and  confdate is null and enteraccdate is null ) ) and FeeFinaType!='LX' "
//               + " and FeeFinaType!='GB' and FeeFinaType!='HK' and FeeFinaType!='HD' and FeeFinaType != 'TJ' "
//                   + " and FeeFinaType!='YHSL' and c.edorcode=a.feeoperationtype group by a.feeoperationtype,c.edorname,a.endorsementno ";
        GetSQLFromXML tGetSQLFromXML=new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
        tGetSQLFromXML.setParameters("EndDate", mDay[1]);
        String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
        String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "YingFu1");
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        ListTable tlistTable = new ListTable();
        String strSum="";
        String strArr[] = null;
        tlistTable.setName("ENDOR");
        for (int i = 1; i <= tSSRS.MaxRow; i++)
        {
            strArr = new String[5];
            for (int j = 1; j <= tSSRS.MaxCol; j++)
            {
                if (j == 1)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                }
                if (j == 2)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                }
                if (j == 3)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    strSum = new DecimalFormat("0.00").format(Double.
                            valueOf(strArr[j - 1]));
                    strArr[j - 1] = strSum;
                    ESumMoney = ESumMoney + Double.parseDouble(strArr[j - 1]);
                }
                if (j == 4)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    ESumNumber = ESumNumber +
                                Long.valueOf(strArr[j - 1]).longValue();
                }
                if (j == 5)
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                }
            }
            tlistTable.add(strArr);
        }

        strArr = new String[5];
        strArr[0] = "EndorName";
        strArr[1] = "EndorNo";
        strArr[2] = "Money";
        strArr[3] = "Mult";
        strArr[4] = "Endor";

//        msql = "select (case othernotype when '4' then '暂收退费' when '5' then '理赔' when '6' then '溢缴退费' when '8' then '溢缴退费' else '其他' end),otherno,sumgetmoney,1,'其他业务支出' from LJAGet where OtherNoType not in ('3','10') and Shoulddate>='" +
//               mDay[0] + "' and Shoulddate<='" + mDay[1] +
//               "' and ManageCom like'" + mGlobalInput.ManageCom +
//               "%' order by othernotype";
        msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "YingFu2");
        tSSRS = tExeSQL.execSQL(msql);
        ListTable alistTable = new ListTable();
        String strSumArr[] = null;
        alistTable.setName("OTHER");
        for (int i = 1; i <= tSSRS.MaxRow; i++)
        {
            strSumArr = new String[5];
            for (int j = 1; j <= tSSRS.MaxCol; j++)
            {
                if (j == 1)
                {
                    strSumArr[j - 1] = tSSRS.GetText(i, j);
                }
                if (j == 2)
                {
                    strSumArr[j - 1] = tSSRS.GetText(i, j);
                }
                if (j == 3)
                {
                    strSumArr[j - 1] = tSSRS.GetText(i, j);
                    strSum = new DecimalFormat("0.00").format(Double.
                            valueOf(strSumArr[j - 1]));
                    strSumArr[j - 1] = strSum;
                    OSumMoney = OSumMoney + Double.parseDouble(strSumArr[j - 1]);
                }
                if (j == 4)
                {
                    strSumArr[j - 1] = tSSRS.GetText(i, j);
                    OSumNumber = OSumNumber +
                                Long.valueOf(strSumArr[j - 1]).longValue();
                }
                if (j == 5)
                {
                    strSumArr[j - 1] = tSSRS.GetText(i, j);
                }
            }
            alistTable.add(strSumArr);
        }
        strSumArr = new String[5];
        strSumArr[0] = "EndorName";
        strSumArr[1] = "EndorNo";
        strSumArr[2] = "Money";
        strSumArr[3] = "Mult";
        strSumArr[4] = "Endor";

        SumMoney = ESumMoney + OSumMoney;
        SumNumber = OSumNumber + OSumNumber;

        String nsql = "select Name from LDCom where ComCode='" +
               mGlobalInput.ManageCom + "'";
        ExeSQL nExeSQL = new ExeSQL();
        nSSRS = nExeSQL.execSQL(nsql);
        String manageCom = nSSRS.GetText(1, 1);

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("FinDayCheckYF.vts", "printer"); //最好紧接着就初始化xml文档
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("ManageCom", manageCom);
        texttag.add("ESumMoney", ESumMoney);
        texttag.add("ESumNumber", ESumNumber);
        texttag.add("OSumMoney", OSumMoney);
        texttag.add("OSumNumber", OSumNumber);
        texttag.add("SumMoney", SumMoney);
        texttag.add("SumNumber", SumNumber);

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        xmlexport.addListTable(alistTable, strSumArr);
        mResult.clear();
        mResult.addElement(xmlexport);
        xmlexport.outputDocumentToFile("E:\\","YingFu");
        return true;
    }

    private void initCommon()
    {
        this.mRiskCode = "";
        this.mRiskType = "";
        this.mXsqd = "";
        this.mXzdl = "";
        this.mSxq = "";
        this.mDqj = "";
    }


}
