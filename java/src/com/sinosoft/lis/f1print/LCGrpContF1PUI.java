/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


/**
 * LCGrpContF1PUI
 * <p>Title: </p>
 * <p>Description: 保单xml文件生成UI部分 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class LCGrpContF1PUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();


    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private String mTemplatePath = null;
    private String mOutXmlPath = null;
    private String mOperate = "";

    public LCGrpContF1PUI()
    {
    }


    /**
     * 数据处理主函数
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("PRINT")
                && !cOperate.equals("REPRINT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            mOperate = cOperate;

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            // 进行业务处理
            if (!dealData())
            {
                return false;
            }

            // 准备传往后台的数据
            VData vData = new VData();

            if (!prepareOutputData(vData))
            {
                return false;
            }

            LCGrpContF1PBL tLCGrpContF1PBL = new LCGrpContF1PBL();
            System.out.println("Start LCGrpContF1PUI Submit ...");

            if (!tLCGrpContF1PBL.submitData(vData, cOperate))
            {
                if (tLCGrpContF1PBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tLCGrpContF1PBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("sbumitData", "tZhuF1PBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tLCGrpContF1PBL.getResult();
                return true;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submit", "发生异常");
            return false;
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * @param vData VData
     * @return boolean
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.add(mGlobalInput);
            vData.add(mLCGrpContSchema);
            vData.add(mTemplatePath);
            vData.add(mOutXmlPath);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * @return boolean
     *  如果在处理过程中出错，则返回false,否则返回true
     */
    private static boolean dealData()
    {
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //获取数据的时候不需要区分打印模式
        mGlobalInput.setSchema((GlobalInput) cInputData.
                               getObjectByObjectName(
                                       "GlobalInput", 0));
        mLCGrpContSchema.setSchema((LCGrpContSchema) cInputData.
                                   getObjectByObjectName(
                                           "LCGrpContSchema", 0));
        mTemplatePath = (String) cInputData.get(2);
        mOutXmlPath = (String) cInputData.get(3);
        return true;
    }


    /**
     * 返回信息
     * @return VData
     */
    public VData getResult()
    {
        return this.mResult;
    }


    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LCGrpContF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 测试函数
     * @param args String[]
     */
    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "pc";
        tG.ComCode = "86";
        LCGrpContF1PUI tZhuF1PUI = new LCGrpContF1PUI();
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo("0000262701"); //合同单号18000000090
//        tLCGrpContSchema.setGrpContNo("240110000000024"); //合同单号
        VData vData = new VData();
        vData.addElement(tG);
        vData.addElement(tLCGrpContSchema);
        vData.addElement("Y:/ui/f1print/template/");
        vData.addElement("Y:/ui/f1print/template/");
        vData.addElement("Y:/");
        if (!tZhuF1PUI.submitData(vData, "PRINT"))
        {
            System.out.println(tZhuF1PUI.mErrors.getFirstError());
            System.out.println("fail to get print data");
        }
    }
}
