package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import java.util.Date;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.bq.CommonBL;
public class URGENoticeNewBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //取得的代理人编码
    private String mAgentCode = "";
    private String PrtSeq = "";
    private String mOperate = "";
     private String mRePrintFlag;
    /** class object */
    private GlobalInput mGlobalInput = new GlobalInput();
    private XmlExport xmlexport = new XmlExport();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LCContSchema mLCContSchema = new LCContSchema();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LMRiskSchema mLMRiskSchema = new LMRiskSchema();
    private LCAddressSchema mLCAddressSchema = new LCAddressSchema();
     LOPRTManagerDB mLOPRTManagerDB = new LOPRTManagerDB();
    private String CurrentDate = PubFun.getCurrentDate();
    public URGENoticeNewBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mOperate = cOperate;
        try {
//            if (!cOperate.equals("CONFIRM") &&
//                !cOperate.equals("PRINT")) {
//                buildError("submitData", "不支持的操作字符串");
//                return false;
//            }
            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData)) {
                return false;
            }

//            if (cOperate.equals("CONFIRM")) {
                mResult.clear();
                // 准备所有要打印的数据
                getPrintData();
//            } else if (cOperate.equals("PRINT")) {
                if (!saveData(cInputData)) {
                    return false;
                }
//            }
            return true;

        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("submitData", ex.toString());
            return false;
        }
        /*mResult.clear();
                 // 准备所有要打印的数据
                 if (!getPrintData())
                 {
            return false;
                 }

                 return true;*/
    }

    public static void main(String[] args) throws Exception {
        /*  String PrtSeq = "160000393760801";
          LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
          tLOPRTManagerSchema.setPrtSeq(PrtSeq);
          System.out.println(PrtSeq);
          GlobalInput tG = new GlobalInput();

          VData tVData = new VData();
          VData mResult = new VData();
          tVData.addElement(tLOPRTManagerSchema);
          URGENoticeBL tURGENoticeBL = new URGENoticeBL();
          tURGENoticeBL.submitData(tVData, "PRINT");*/
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {

       //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                                              "LOPRTManagerSchema", 0));
        TransferData tTransferData = (TransferData) cInputData.
                                     getObjectByObjectName("TransferData", 0);
        if (mLOPRTManagerSchema == null) {//vts入口
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        mLOPRTManagerSchema.setCode(mLOPRTManagerSchema.getStandbyFlag1());
        if (tTransferData != null) {
           this.mRePrintFlag = (String) tTransferData.getValueByName("RePrintFlag");
       }
//只赋给schema一个prtseq

       if (mGlobalInput == null) {
           buildError("getInputData", "没有得到足够的信息！");
           return false;
       }

//       else
//        {//PDF入口
            //取得LJSPayBDB
//            System.out.println("---------不为空--PDF入口！");
//            LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
//            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
//            tLOPRTManagerDB.setPrtSeq(mLOPRTManagerSchema.getOldPrtSeq());
//            tLOPRTManagerDB.setCode(mLOPRTManagerSchema.getCode());
//            tLOPRTManagerDB.setOtherNo(mLOPRTManagerSchema.getOtherNo());
//            if(!tLOPRTManagerDB.getInfo())
//            {
//                mErrors.addOneError("PDF入口tLOPRTManagerDB传入的数据不完整。");
//                return false;
//            }
//            mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
            return true;

    }

    public VData getResult() {
        return this.mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    private void getPrintData() throws Exception {
//        LOPRTManagerDB mLOPRTManagerDB = getPrintManager();

        //根据印刷号查询打印队列中的纪录
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setPrtSeq(mLOPRTManagerSchema.getOldPrtSeq());
        if (tLOPRTManagerDB.getInfo() == false) {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("getPrintData", "在取得打印队列中数据时发生错误");
           // return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema(); //保存打印队列数据
        if (mLOPRTManagerSchema.getStateFlag() == null) {
            buildError("getprintData", "无效的打印状态");
        }
        mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setDoneTime(PubFun.getCurrentTime());

        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = new LCContSet();
        String strContNo = tLCContDB.getContNo();
        tLCContDB.setProposalContNo(mLOPRTManagerSchema.getOtherNo());
        tLCContSet = tLCContDB.query();
        if (tLCContSet.size() != 1) {
            mErrors.copyAllErrors(tLCContDB.mErrors);
            buildError("getPrintData", "在取得LCPol的数据时发生错误");
            //return false;
        }

        mLCContSchema = tLCContSet.get(1).getSchema(); //保存主险投保单信息

        mAgentCode = mLCContSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mAgentCode);
        if (!tLAAgentDB.getInfo()) {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("getPrintData", "在取得LAAgent的数据时发生错误");
            //return false;
        }
        mLAAgentSchema = tLAAgentDB.getSchema(); //保存代理人信息

        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet =
                tLABranchGroupDB.executeQuery("select * from labranchgroup where agentgroup=(select agentgroup from laagent where  agentcode='" +
                                              mLCContSchema.getAgentCode() +
                                              "')");
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            buildError("getprintData", "没有查到该报单的代理人组别");
        }

        //投保人地址和邮编
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(mLCContSchema.getContNo());
        if (tLCAppntDB.getInfo() == false) {
            mErrors.copyAllErrors(tLCAppntDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            //return false;
        }

        LCAddressDB tLCAddressDB = new LCAddressDB();

        tLCAddressDB.setCustomerNo(mLCContSchema.getAppntNo());
        tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
        if (tLCAddressDB.getInfo() == false) {
            mErrors.copyAllErrors(tLCAddressDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            //return false;
        }

        mLCAddressSchema = tLCAddressDB.getSchema();

        xmlexport = new XmlExport();
        xmlexport.createDocument("URGENotice.vts", "printer");

        //生成-年-月-日格式的日期
//        StrTool tSrtTool = new StrTool();
        String SysDate = StrTool.getYear() + "年" + StrTool.getMonth() + "月" +
                         StrTool.getDay() + "日";

        String codeType = mLOPRTManagerSchema.getCode();
        String oldCodeType = "";
        if (codeType.equals("17")) { //催缴通知书
            oldCodeType = "07";
        }
        if (codeType.equals("11")) { //体检通知书
            oldCodeType = "03";
        }
        if (codeType.equals("19")) { //承保计划变更通知书
            oldCodeType = "05";
        }
        if (codeType.equals("18")) { //问题件通知书
            oldCodeType = "85";
        }
        //找到催办的原通知书打印数据（其它号码相同，且StateFlag=3）
        SSRS tRS = new SSRS();
        ExeSQL tES = new ExeSQL();
        String NoticeDate = "";
        String NewDate = "";
        String strSql =
                "select makedate,prtseq,formakedate from LOPRTManager where OtherNo='" +
                mLOPRTManagerSchema.getOtherNo() + "' ";
        strSql = strSql + " and prtseq = '" + mLOPRTManagerSchema.getOldPrtSeq() +
                 "'";
        strSql = strSql + " and Code='" + oldCodeType + "'";
        System.out.println("strSql:" + strSql);
        tRS = tES.execSQL(strSql);
        if (tES.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tES.mErrors);
            buildError("commonInfo", "打印管理表查询失败！");
           // return false;
        }
        if (tRS.MaxRow == 0) {
            //NoticeDate="2003-3-1";
            buildError("commonInfo", "没有找到催办的原通知书的打印数据！");
           // return false;
        } else {
            NoticeDate = tRS.GetText(1, 1);
            PrtSeq = tRS.GetText(1, 2);
            NewDate = tRS.GetText(1, 3);
        }
        FDate tD = new FDate();
        Date newBaseDate = new Date();
        //liuqiang 催办通知书的截至日期应该是当前系统日期加上可配置的一个时间间隔
        //newBaseDate=tD.getDate(NoticeDate); //将字符串转换为日期,后面计算用
        newBaseDate = tD.getDate(NewDate); //将字符串转换为日期,后面计算用
        if (newBaseDate == null) {
            buildError("commonInfo", "没有找到要催办的日期！");
            //return false;
        }
        Date AfterDate = PubFun.calDate(newBaseDate, 10, "D", null);
        NewDate = tD.getString(AfterDate);

        //liuqiang 催办通知书的截至日期应该是当前系统日期加上可配置的一个时间间隔
        //newBaseDate=tD.getDate(NoticeDate); //将字符串转换为日期,后面计算用

        TextTag texttag = new TextTag();
        //模版自上而下的元素
        texttag.add("JetFormType", "11");
        //借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                             mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600")||comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            buildError("getInputData", "操作员机构查询出错！");
        }
        String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"+comcode+"' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        texttag.add("ManageComLength4", printcode);
        texttag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.","_"));
        if ("batch".equals(mOperate)) {
            texttag.add("previewflag", "0");
        } else {
            texttag.add("previewflag", "1");
        }
        texttag.add("BarCode1", mLOPRTManagerSchema.getPrtSeq());
        texttag.add("BarCodeParam1", "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("LCPol.AppntName", mLCContSchema.getAppntName()); //投保人名称
        texttag.add("Post", mLCAddressSchema.getZipCode()); //投保人邮政编码
        texttag.add("Address", mLCAddressSchema.getPostalAddress()); //投保人地址
        texttag.add("LCPol.PolNo", mLCContSchema.getContNo()); //投保单号
        texttag.add("RiskName", mLMRiskSchema.getRiskName()); //主险名称
        texttag.add("NoticeDate", CommonBL.decodeDate(NoticeDate));
        texttag.add("NewDate", CommonBL.decodeDate(NewDate));
        texttag.add("AppName", mLCContSchema.getAppntName()); //投保人名称
        texttag.add("AgentName", mLAAgentSchema.getName()); //代理人姓名
        //2014-11-4  杨阳   业务员编码显示laagent表中groupAgentcode字段
        String groupAgentCode = tES.getOneValue("select getUniteCode("+mLCContSchema.getAgentCode()+") from dual ");
        texttag.add("LCPol.AgentCode", groupAgentCode); //代理人业务号
        // texttag.add("LCPol.ManageCom", getComName(mLCPolSchema.getManageCom())); //营业机构
        texttag.add("PrtNo", mLOPRTManagerSchema.getPrtSeq()); //流水号
        texttag.add("LCPol.PrtNo", mLCContSchema.getPrtNo()); //印刷号
        texttag.add("LCPol.UWCode", mLCContSchema.getUWOperator()); // 核保师代码
        texttag.add("SysDate", CommonBL.decodeDate(mLOPRTManagerSchema.getMakeDate()));
        //add by zhangxing
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mLCContSchema.getManageCom());
        if (!tLDComDB.getInfo()) {
            this.mErrors.copyAllErrors(tLDComDB.mErrors);
           // return false;
        }

        texttag.add("XI_ManageCom", tLCContDB.getManageCom());
        texttag.add("ManageCom", tLDComDB.getLetterServiceName());
        texttag.add("XI_AppntNo", mLCContSchema.getAppntNo());
        texttag.add("AppntName", mLCContSchema.getAppntName());
        texttag.add("ManageAddress", tLDComDB.getServicePostAddress());
        texttag.add("ManageZipCode", tLDComDB.getLetterServicePostZipcode());
        texttag.add("ManageFax", tLDComDB.getFax());
        texttag.add("ManagePhone", tLDComDB.getServicePhone());
        texttag.add("Cavidate1", CommonBL.decodeDate(mLCContSchema.getCValiDate()));
        texttag.add("AgentPhone", mLAAgentSchema.getPhone()); //代理人电话
        //2014-10-31  杨阳   业务员编码显示laagent表中groupAgentcode字段
        groupAgentCode = tES.getOneValue("select getUniteCode("+mLCContSchema.getAgentCode()+") from dual ");
        texttag.add("Agentcode", groupAgentCode);
        texttag.add("PrtSeq", PrtSeq);
        if (mLCContSchema.getAppntSex().equals("1")) {
            texttag.add("Title", "女士");
        } else {
            texttag.add("Title", "先生");
        }
        //add by zhangxing
        texttag.add("AgentGroupName", tLABranchGroupSet.get(1).getName());
        texttag.add("GlobalServicePhone", tLDComDB.getServicePhone());

        //判断打印队列的类型(相关表信息看：寿险业务承保(个单明细))
        if (mLOPRTManagerSchema.getCode().equals("17")) { //首期交费通知书催办通知书
            texttag.add("NoticeType", "催缴通知书");
        }
        if (mLOPRTManagerSchema.getCode().equals("11")) { //体检通知书催办通知书
            texttag.add("NoticeType", "体检通知书");
        }
        if (mLOPRTManagerSchema.getCode().equals("18")) { //问题件通知书催办通知书
            texttag.add("NoticeType", "问题件通知书");
        }
        if (mLOPRTManagerSchema.getCode().equals("19")) { //承保计划变更通知书催办通知书
            texttag.add("NoticeType", "承保计划变更通知书");
        }

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.outputDocumentToFile("d:\\", "bodycheckprint");
        mResult.clear();
        mResult.addElement(xmlexport);

        //return true;
    }

    private LCContDB getContInfo() throws Exception {
        LCContDB tLCContDB = new LCContDB();
        // 打印时传入的是主险投保单的投保单号
        tLCContDB.setProposalContNo(mLOPRTManagerSchema.getOtherNo());
        LCContSet tLCContSet = tLCContDB.query();
        if (tLCContSet.size() <= 0) {
            throw new Exception("查询合同信息失败！");
        }
        if (tLCContSet.size() > 1) {
            throw new Exception("查询合同信息失败！");
        }
//        if (!tLCContDB.getInfo()) {
//            mErrors.copyAllErrors(tLCContDB.mErrors);
//            throw new Exception("在获取保单信息时出错！");
//        }
        tLCContDB.setSchema(tLCContSet.get(1).getSchema());
        return tLCContDB;
    }

    private String getOperatorName(String tOperatorCode) {
        String name = (new ExeSQL()).getOneValue(
                "select username from lduser where usercode='" + tOperatorCode +
                "'");
        if (name == null || name.equals("") || name.equals("null")) {
            return "";
        }
        return name;
    }


    private LOPRTManagerDB getPrintManager() throws Exception {
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema); //将prtseq传给DB，目的查找所有相关信息，然后还要返回给schema
        if (tLOPRTManagerDB.getInfo() == false) {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            throw new Exception("在取得打印队列中数据时发生错误");
        }
        //需要判断是否已经打印？！
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema(); //get all message！
        return tLOPRTManagerDB;
    }

    private boolean saveData(VData mInputData) {

        //根据印刷号查询打印队列中的纪录
        //mLOPRTManagerSchema
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (tLOPRTManagerDB.getInfo() == false) {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setDoneDate(CurrentDate);
        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
        mResult.addElement(mLOPRTManagerSchema);

//        tLOPRTManagerDB.setSchema((LOPRTManagerSchema) mInputData.
//                                  getObjectByObjectName("LOPRTManagerSchema", 0));
//
//        mResult.add(mLOPRTManagerSchema);
//        mResult.add(tLOPRTManagerDB);
//        FirstPayF1PBLS tFirstPayF1PBLS = new FirstPayF1PBLS();
//        tFirstPayF1PBLS.submitData(mResult, mOperate);
//        if (tFirstPayF1PBLS.mErrors.needDealError()) {
//            mErrors.copyAllErrors(tFirstPayF1PBLS.mErrors);
//            buildError("saveData", "提交数据库出错！");
//            return false;
//        }
        return true;

    }
}
