package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.otof.OtoFExport;
import com.sinosoft.utility.XMLDatasets;
import com.sinosoft.utility.TransferData;

public class LCGrpSuccorUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();


    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private String mTemplatePath = null;
    private String mOutXmlPath = null;
    private TransferData mTransferData;
    private String mOperate = "";

    public LCGrpSuccorUI() {
    }

    /**
     * 数据处理主函数
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        try {
            if (!cOperate.equals("PRINT")
                && !cOperate.equals("REPRINT")) {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            mOperate = cOperate;

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData)) {
                return false;
            }

            // 进行业务处理
            if (!dealData()) {
                return false;
            }

            // 准备传往后台的数据
            VData vData = new VData();

            if (!prepareOutputData(vData)) {
                return false;
            }

            LCGrpSuccorBL tLCGrpSuccorBL = new LCGrpSuccorBL();
            System.out.println("Start LCGrpContF1PUI Submit ...");

            if (!tLCGrpSuccorBL.submitData(vData, cOperate)) {
                if (tLCGrpSuccorBL.mErrors.needDealError()) {
                    mErrors.copyAllErrors(tLCGrpSuccorBL.mErrors);
                    return false;
                } else {
                    buildError("sbumitData", "tZhuF1PBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            } else {
                mResult = tLCGrpSuccorBL.getResult();
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("submit", "发生异常");
            return false;
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * @param vData VData
     * @return boolean
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData) {
        try {
            vData.clear();
            vData.add(mGlobalInput);
            vData.add(mLCGrpContSchema);
            vData.add(this.mTransferData);
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * @return boolean
     *  如果在处理过程中出错，则返回false,否则返回true
     */
    private static boolean dealData() {
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //获取数据的时候不需要区分打印模式
        mGlobalInput.setSchema((GlobalInput) cInputData.
                               getObjectByObjectName(
                                       "GlobalInput", 0));
        mLCGrpContSchema.setSchema((LCGrpContSchema) cInputData.
                                   getObjectByObjectName(
                                           "LCGrpContSchema", 0));

        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mTemplatePath = (String) mTransferData.getValueByName("TemplatePath");
        mOutXmlPath = (String) mTransferData.getValueByName("OutXmlPath");
        return true;
    }


    /**
     * 返回信息
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }


    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCGrpContF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 测试函数
     * @param args String[]
     */
    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "QY0101";
        tG.ComCode = "86";
        tG.ManageCom = "86110000";
        LCGrpSuccorUI tZhuF1PUI = new LCGrpSuccorUI();
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo("0000199401"); //合同单号
//        tLCGrpContSchema.setGrpContNo("240110000000024"); //合同单号
        VData vData = new VData();
        vData.addElement(tG);
        vData.addElement(tLCGrpContSchema);
        String szTemplatePath = "D:/workspace/UI/f1print/template/";
        String sOutXmlPath = "D:/workspace/UI";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("TemplatePath", szTemplatePath);
        tTransferData.setNameAndValue("OutXmlPath", sOutXmlPath);
        vData.add(tTransferData);
        vData.addElement("D:/workspace/UI/f1print/template/");
        vData.addElement("D:/workspace/UI/f1print/template/");
        if (!tZhuF1PUI.submitData(vData, "PRINT")) {
            System.out.println(tZhuF1PUI.mErrors.getFirstError());
            System.out.println("fail to get print data");
        }

    }


}
