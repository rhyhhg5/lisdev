package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author kevin
 * @version 1.0
 */

import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCRReportItemSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.vschema.LCRReportItemSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.schema.LCPolSchema;
import java.text.DecimalFormat;
import com.sinosoft.lis.vschema.LWMissionSet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LBMissionSchema;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LWMissionSchema;
import com.sinosoft.lis.pubfun.PubSubmit;

public class MeetF1PNewBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LCContSchema mLCContSchema = new LCContSchema();

    private LCAddressSchema mLCAddressSchema = new LCAddressSchema();

    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

    private LCPolSchema mLCPolSchema = new LCPolSchema();

    private LCPolSet mLCPolSet = new LCPolSet();

    private double SumPrem = 0; //合计保费

    private String mOperate = "";

    private String mAgentCode = "";

    private MMap tMMap = new MMap();

    private String CurrentDate = PubFun.getCurrentDate();

    boolean MEETFlag = false;

    private double SpecAddFeeSum = 0; //和保险计划变更一起的客户的加费

    private String FORMATMODOL = "0.00"; //保费保额计算出来后的精确位数

    private DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL); //数字转换对象

    private Reflections mReflections = new Reflections();

    public MeetF1PNewBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        try
        {

            //            if (!cOperate.equals("CONFIRM") &&
            //                !cOperate.equals("PRINT"))
            //            {
            //                buildError("submitData", "不支持的操作字符串");
            //                return false;
            //            }

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            //          if (cOperate.equals("CONFIRM"))
            //            {

            mResult.clear();

            // 准备所有要打印的数据
            getPrintData();

            //            }
            //            else if (cOperate.equals("PRINT"))
            //            {
            if (!saveData(cInputData))
            {
                return false;
            }
            //            }
            return true;

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", ex.toString());
            return false;
        }
    }

    public static void main(String[] args)
    {
    }

    private String getDate(Date date)
    {

        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
        return df.format(date);
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.getObjectByObjectName("LOPRTManagerSchema", 0));
        TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "RefuseAppF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() throws Exception
    {
        XmlExport xmlExport = new XmlExport(); //新建一个XmlExport的实例
        xmlExport.createDocument("MeetNotice.vts", ""); //最好紧接着就初始化xml文档

        String PrtNo = mLOPRTManagerSchema.getOldPrtSeq(); //打印流水号
        LCContDB tLCContDB = new LCContDB();
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();

        tLOPRTManagerDB.setPrtSeq(mLOPRTManagerSchema.getOldPrtSeq()); //将prtseq传给DB，目的查找所有相关信息，然后还要返回给schema
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            throw new Exception("在取得打印队列中数据时发生错误");
        }
        //需要判断是否已经打印？！
        //if (tLOPRTManagerDB.getStateFlag().equals("0") || tLOPRTManagerDB.getStateFlag().equals("1"))
        if (tLOPRTManagerDB.getStateFlag() != null)
        {
            mLOPRTManagerSchema = tLOPRTManagerDB.getSchema(); //get all message！

            // 打印时传入的是主险投保单的投保单号
            tLCContDB.setContNo(mLOPRTManagerSchema.getOtherNo());

            if (!tLCContDB.getInfo())
            {
                mErrors.copyAllErrors(tLCContDB.mErrors);
                throw new Exception("在获取保单信息时出错！");
            }
            mLCContSchema = tLCContDB.getSchema();

            //add by zhangxing 代理人信息
            mAgentCode = mLCContSchema.getAgentCode();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(mAgentCode);
            if (!tLAAgentDB.getInfo())
            {
                mErrors.copyAllErrors(tLAAgentDB.mErrors);
                buildError("outputXML", "在取得LAAgent的数据时发生错误");

            }
            mLAAgentSchema = tLAAgentDB.getSchema(); //保存代理人信息
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(
            //去除业务员个险直销的校验
                    //"select * from labranchgroup where agentgroup=(select agentgroup from laagent where branchtype='1' and branchtype2='01' and agentcode='" +
                    "select * from LABranchGroup where AgentGroup=(select AgentGroup from LAAgent where AgentCode='"
                            + tLCContDB.getAgentCode() + "')");
            if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0)
            {
                buildError("getprintData", "没有查到该报单的代理人组别");
            }

            // 查询打印队列的信息
            mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();

            if (mLOPRTManagerSchema.getStateFlag() == null)
            {
                buildError("getprintData", "无效的打印状态");
            }
            else if (!mLOPRTManagerSchema.getStateFlag().equals("0"))
            {
                buildError("getprintData", "该打印请求不是在请求状态");
            }
            // 调用打印服务

            mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
            mLOPRTManagerSchema.setDoneTime(PubFun.getCurrentTime());

            tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
            if (tLOPRTManagerDB.getInfo() == false)
            {
                mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
                buildError("outputXML", "在取得打印队列中数据时发生错误");
            }
            LCRReportDB tLCRReportDB = new LCRReportDB();
            tLCRReportDB.setProposalContNo(tLCContDB.getContNo());
            tLCRReportDB.setPrtSeq(mLOPRTManagerSchema.getPrtSeq());
            tLCRReportDB.getInfo();
            String meetName = tLCRReportDB.getName();
            System.out.println("meetName=" + meetName);
            //生调子表
            String[] PETitle = new String[3];
            PETitle[0] = "ID";
            PETitle[1] = "CHECKITEM";
            PETitle[2] = "CHECKITEMContent";
            ListTable tPEListTable = new ListTable();
            String strPE[] = null;
            tPEListTable.setName("CHECKITEM"); //对应模版体检部分的行对象名
            LCRReportItemDB tLCRReportItemDB = new LCRReportItemDB();
            LCRReportItemSet tLCRReportItemSet = new LCRReportItemSet();
            tLCRReportItemDB.setContNo(mLCContSchema.getContNo());
            tLCRReportItemDB.setPrtSeq(PrtNo);
            
            ListTable tContListTable = new ListTable();
            tContListTable.setName("CheckContent");
            String[] ContTitle = new String[1];
            ContTitle[0] = "IDCont";
            String strCo[] = null;

            tLCRReportItemSet.set(tLCRReportItemDB.query());
            if (tLCRReportItemSet == null || tLCRReportItemSet.size() == 0)
            {
                MEETFlag = false;
            }
            else
            {
                MEETFlag = true;
                LCRReportItemSchema tLCRReportItemSchema;
                for (int i = 1; i <= tLCRReportItemSet.size(); i++)
                {
                    tLCRReportItemSchema = new LCRReportItemSchema();
                    tLCRReportItemSchema.setSchema(tLCRReportItemSet.get(i));
                    strPE = new String[3];
                    strPE[0] = tLCRReportItemSchema.getRReportItemCode(); //序号
                    strPE[1] = tLCRReportItemSchema.getRReportItemName(); //序号对应的内容
                    strPE[2] = tLCRReportItemSchema.getRRItemContent(); //序号对应的内容
                    System.out.println("RReportItemName=" + tLCRReportItemSchema.getRReportItemName());
                    strCo = new String[1];
                    if(strPE[2]==null||"".equals(strPE[2])){
                    	strCo[0] = i + "." + strPE[1] ;
                    }else{
                    	strCo[0] = i + "." + strPE[1] + ":" + strPE[2];
                    }
                    tPEListTable.add(strPE);
                    tContListTable.add(strCo);
                }
            }

            //投保人地址和邮编
            LCAppntDB tLCAppntDB = new LCAppntDB();
            tLCAppntDB.setContNo(mLCContSchema.getContNo());
            if (tLCAppntDB.getInfo() == false)
            {
                mErrors.copyAllErrors(tLCAppntDB.mErrors);

                throw new Exception("在取得打印队列中数据时发生错误");
            }

            LCAddressDB tLCAddressDB = new LCAddressDB();

            tLCAddressDB.setCustomerNo(mLCContSchema.getAppntNo());
            tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
            if (tLCAddressDB.getInfo() == false)
            {
                mErrors.copyAllErrors(tLCAddressDB.mErrors);
                buildError("outputXML", "在取得打印队列中数据时发生错误");

            }

            mLCAddressSchema = tLCAddressDB.getSchema();
            String MainRiskName = new String();
            String Sql = new String();
            String tSql = new String();
            ListTable tRiskListTable = new ListTable();
            String[] RiskInfoTitle = new String[10];

            LCPolDB tLCPolDB = new LCPolDB();
            //        tLCPolDB.setContNo(tLCContDB.getContNo());
            StringBuffer sql = new StringBuffer();
            sql.append("select a.* from lcpol a,lcinsured b where a.contno ='");
            sql.append(tLCContDB.getContNo());
            sql.append("' and a.contno=b.contno and a.insuredno=b.insuredno order by b.SequenceNo");
            LCPolSet tempLCPolSet = tLCPolDB.executeQuery(sql.toString());
            System.out.println("polno=" + tempLCPolSet.get(1).getPolNo());

            tRiskListTable.setName("RiskInfo"); //对应模版投保信息部分的行对象名
            RiskInfoTitle[9] = "RiskName"; //险种名称
            RiskInfoTitle[0] = "Amnt"; //保险金额
            RiskInfoTitle[1] = "PayYears"; //缴费年期
            RiskInfoTitle[2] = "PayIntv"; //缴费方式（间隔）
            RiskInfoTitle[3] = "Prem"; //保费
            RiskInfoTitle[4] = "AddPrem"; //加费
            RiskInfoTitle[5] = "InsuredName"; //加费
            RiskInfoTitle[6] = "RiskCode"; //加费
            RiskInfoTitle[7] = "InsuYears"; //加费
            RiskInfoTitle[8] = "SeqNo"; //加费

            for (int nIndex = 0; nIndex < tempLCPolSet.size(); nIndex++)
            {
                //查询主险保单

                mLCPolSchema = tempLCPolSet.get(nIndex + 1).getSchema(); //保存险种投保单信息
                System.out.println("mLCPolSchema.getPolNo()=" + mLCPolSchema.getPolNo());

                //1-险种信息：
                LMRiskDB tLMRiskDB = new LMRiskDB();
                tLMRiskDB.setRiskCode(mLCPolSchema.getRiskCode());
                if (!tLMRiskDB.getInfo())
                {
                    mErrors.copyAllErrors(tLMRiskDB.mErrors);
                    buildError("outputXML", "在取得主险LMRisk的数据时发生错误");
                    return false;
                }
                MainRiskName = tLMRiskDB.getRiskName();
                System.out.println("MainRiskName=" + MainRiskName);
                SSRS tempSSRS = new SSRS();
                ExeSQL tempExeSQL = new ExeSQL();
                String sTemp = "";
                Double fTemp = new Double(mLCPolSchema.getAmnt());

                String[] RiskInfo = new String[10];
                RiskInfo[9] = MainRiskName; //险种名称
                RiskInfo[8] = nIndex + 1 + "";
                int iTemp = 0;
                RiskInfo[0] = mLCPolSchema.getMult() > 0 ? getNumberToCharacter(mLCPolSchema.getMult()) + "档" : fTemp
                        .toString().substring(0, fTemp.toString().lastIndexOf(".")); //保险金额
                if (mLCPolSchema.getPayEndYear() == 1000 && mLCPolSchema.getPayEndYearFlag().toUpperCase().equals("A"))
                {
                    RiskInfo[1] = "终生"; //交费年期
                }
                else
                {
                    RiskInfo[1] = (new Integer(mLCPolSchema.getPayYears())).toString(); //交费年期
                }

                if (mLCPolSchema.getPayIntv() == -1)
                {
                    sTemp = "不定期交费";
                }
                if (mLCPolSchema.getPayIntv() == 0)
                {
                    sTemp = "趸交";
                }
                if (mLCPolSchema.getPayIntv() == 1)
                {
                    sTemp = "月交";
                }
                if (mLCPolSchema.getPayIntv() == 3)
                {
                    sTemp = "季交";
                }
                if (mLCPolSchema.getPayIntv() == 6)
                {
                    sTemp = "半年交";
                }
                if (mLCPolSchema.getPayIntv() == 12)
                {
                    sTemp = "年交";
                }
                RiskInfo[2] = sTemp; //交费方式
                RiskInfo[3] = new Double(mLCPolSchema.getStandPrem()).toString(); //保费

                // 取主险投保单加费信息
                tSql = "select sum(Prem) from LCPrem where PolNo='" + mLCPolSchema.getPolNo()
                        + "' and PayPlanCode like '000000%'";
                System.out.println("Sql:" + tSql);
                tempSSRS = tempExeSQL.execSQL(tSql);
                if (tempSSRS.MaxCol > 0)
                {
                    System.out.println("tempSSRS.GetText(1,1)=" + tempSSRS.GetText(1, 1));
                    if (!(tempSSRS.GetText(1, 1).equals("0") || tempSSRS.GetText(1, 1).trim().equals("") || tempSSRS
                            .GetText(1, 1).equals("null")))
                    {
                        RiskInfo[4] = tempSSRS.GetText(1, 1); //累计加费
                        SpecAddFeeSum = SpecAddFeeSum + Double.parseDouble(tempSSRS.GetText(1, 1)); //加费合计
                    }
                    else
                    {
                        RiskInfo[4] = "0";
                    }
                }
                String YearFlag = "";
                RiskInfo[5] = mLCPolSchema.getInsuredName();
                RiskInfo[6] = mLCPolSchema.getRiskCode();
                if (mLCPolSchema.getInsuYearFlag().equals("Y"))
                {
                    YearFlag = "年";
                }
                else if (mLCPolSchema.getInsuYearFlag().equals("M"))
                {
                    YearFlag = "月";
                }
                else if (mLCPolSchema.getInsuYearFlag().equals("D"))
                {
                    YearFlag = "日";
                }
                else if (mLCPolSchema.getInsuYearFlag().equals("A"))
                {
                    YearFlag = "岁";
                }

                RiskInfo[7] = mLCPolSchema.getInsuYear() + YearFlag;

                if (mLCPolSchema.getInsuYearFlag().toUpperCase().equals("A"))
                {
                    RiskInfo[7] = "至" + mLCPolSchema.getInsuYear() + "岁";
                }

                if (mLCPolSchema.getInsuYear() == 1000 && mLCPolSchema.getInsuYearFlag().toUpperCase().equals("A"))
                {
                    RiskInfo[7] = "终身";
                }
                if (mLCPolSchema.getInsuYear() == 84 && mLCPolSchema.getInsuYearFlag().toUpperCase().equals("A"))
                {
                    RiskInfo[7] = "至84岁";
                }

                tRiskListTable.add(RiskInfo); //加入主险信息
                SumPrem = SumPrem + mLCPolSchema.getStandPrem(); // 原保费合计

            }
            SpecAddFeeSum = Double.parseDouble(mDecimalFormat.format(SpecAddFeeSum)); //转换计算后的保费(规定的精度)
            SumPrem = Double.parseDouble(mDecimalFormat.format(SumPrem)); //转换计算后的保费(规定的精度)

            LDComDB tLDComDB = new LDComDB();
            tLDComDB.setComCode(mLCContSchema.getManageCom());
            if (tLDComDB.getInfo() == false)
            {
                mErrors.copyAllErrors(tLCAppntDB.mErrors);
                throw new Exception("管理机构在取得打印队列中数据时发生错误");
            }

            //            StrTool tSrtTool = new StrTool();
            String SysDate = StrTool.getYear() + "年" + StrTool.getMonth() + "月" + StrTool.getDay() + "日";
            String sexname = "先生/女士";
            TextTag texttag = new TextTag();
            texttag.add("JetFormType", "04");
            //借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
            String sqlusercom = "select comcode from lduser where usercode='" + mGlobalInput.Operator + "' with ur";
            String comcode = new ExeSQL().getOneValue(sqlusercom);
            if (comcode.equals("86") || comcode.equals("8600") || comcode.equals("86000000"))
            {
                comcode = "86";
            }
            else if (comcode.length() >= 4)
            {
                comcode = comcode.substring(0, 4);
            }
            else
            {
                buildError("getInputData", "操作员机构查询出错！");
                return false;
            }
            String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='" + comcode
                    + "' with ur";
            String printcode = new ExeSQL().getOneValue(printcom);

            texttag.add("ManageComLength4", printcode);
            texttag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
            texttag.add("previewflag", "1");

            texttag.add("BarCode1", mLOPRTManagerSchema.getPrtSeq());
            texttag.add("BarCodeParam1",
                    "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
            texttag.add("AppntZipcode", mLCAddressSchema.getZipCode()); //投保人邮政编码
            texttag.add("AppntAddr", mLCAddressSchema.getPostalAddress()); //投保人地址
            //texttag.add("SysDate", SysDate);
          //2014-11-2    杨阳
            //业务员号友显示LAagent表中GroupAgentcode字段
            String groupAgentCode = new ExeSQL().getOneValue("select getUniteCode("+ mLAAgentSchema.getAgentCode()+") from dual with ur");
            texttag.add("Agentcode", groupAgentCode);
            texttag.add("Cavidate1", mLCContSchema.getCValiDate());
            texttag.add("PrtNo", mLCContSchema.getPrtNo());
            texttag.add("XI_AppntNo", tLCContDB.getAppntNo());
            texttag.add("AppntName", tLCContDB.getAppntName());
            if (tLCContDB.getAppntSex().equals("0"))
            {
                sexname = "先生";
            }
            if (tLCContDB.getAppntSex().equals("1"))
            {
                sexname = "女士";
            }
            texttag.add("Title", sexname);
            Date PolApplyDate = (new FDate()).getDate(tLCContDB.getPolApplyDate());
            texttag.add("Cavidate", getDate(PolApplyDate));
            //texttag.add("Cavidate", tLCContDB.getPolApplyDate());
            texttag.add("XI_ManageCom", tLCContDB.getManageCom());
            texttag.add("ManageCom", tLDComDB.getLetterServiceName());
            texttag.add("ManageAddress", tLDComDB.getServicePostAddress());
            texttag.add("ManageZipCode", tLDComDB.getLetterServicePostZipcode());
            texttag.add("ManageFax", tLDComDB.getFax());
            texttag.add("ManagePhone", tLDComDB.getPhone());
            texttag.add("ServicePhone", tLDComDB.getServicePhone());
            //add by zhangxing 增加代理人组别
            texttag.add("AgentGroup", mLAAgentSchema.getAgentGroup());

            texttag.add("AgentGroupName", tLABranchGroupSet.get(1).getName());
            texttag.add("GlobalServicePhone", tLDComDB.getServicePhone());

            //SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
            //texttag.add("Today", df.format(new Date()));
            //texttag.add("Today", df.format(mLOPRTManagerSchema.getMakeDate()));
            Date makeDate = (new FDate()).getDate(tLOPRTManagerDB.getMakeDate());
            texttag.add("Today", getDate(makeDate));

            if (texttag.size() > 0)
            {
                xmlExport.addTextTag(texttag);
            }
            if (MEETFlag == true)
            {
                xmlExport.addListTable(tPEListTable, PETitle); //保存体检信息     
            }
            xmlExport.addListTable(tRiskListTable, RiskInfoTitle); //保存险种信息及其标题栏
            //add by wangna #1865  需求编号：（2014-030） 优化人工核保契调功能模块
            ListTable tRiskEndTable = new ListTable();
            tRiskEndTable.setName("RiskInfoEnd");
            String[] tRiskEndTitle = new String[0];
            xmlExport.addListTable(tRiskEndTable, tRiskEndTitle);
            xmlExport.addListTable(tContListTable, ContTitle);
            
            ListTable tEndTable = new ListTable();
            tEndTable.setName("END");
            String[] tEndTitle = new String[0];
            xmlExport.addListTable(tEndTable, tEndTitle);
            xmlExport.outputDocumentToFile("D:\\", "testHZ"); //输出xml文档到文件
            mResult.clear();
            mResult.addElement(xmlExport);
            //处理工作流
            if (tLOPRTManagerDB.getStateFlag().equals("0"))
            {
                LWMissionDB mLWMissionDB = new LWMissionDB();
                LWMissionSet mLWMissionSet = new LWMissionSet();
                mLWMissionDB.setActivityID("0000001108");
                mLWMissionDB.setProcessID("0000000003");
                mLWMissionDB.setMissionProp3(tLOPRTManagerDB.getPrtSeq());
                mLWMissionSet = mLWMissionDB.query();
                if (mLWMissionSet.size() != 1)
                {
                    //                    mErrors.copyAllErrors(mLWMissionSet.mErrors);
                    //                    throw new Exception("取契调通知书下发工作流失败！");
                }
                else
                {
                    //tMMap.put(mLWMissionSet, "DELETE");

                    LBMissionSchema tLBMissionSchema = new LBMissionSchema();
                    LWMissionSchema tLWMissionSchema = new LWMissionSchema();
                    tLWMissionSchema = mLWMissionSet.get(1);
                    String tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
                    mReflections.transFields(tLBMissionSchema, tLWMissionSchema);
                    tLBMissionSchema.setSerialNo(tSerielNo);
                    tLBMissionSchema.setActivityStatus("3"); //节点任务执行完毕
                    tLBMissionSchema.setLastOperator(mGlobalInput.Operator);
                    tLBMissionSchema.setMakeDate(PubFun.getCurrentDate());
                    tLBMissionSchema.setMakeTime(PubFun.getCurrentTime());
                    tMMap.put(tLBMissionSchema, "INSERT");
                    tMMap.put("DELETE from lwmission where ActivityID='0000001108' and missionprop3='"
                            + tLOPRTManagerDB.getPrtSeq() + "'", "DELETE");
                    mLWMissionSet.get(1).setActivityID("0000001113");
                    tMMap.put(mLWMissionSet, "INSERT");

                }
                PubSubmit tPubSubmit = new PubSubmit();
                VData tVData = new VData();
                tVData.add(tMMap);
                if (!tPubSubmit.submitData(tVData, ""))
                {
                    buildError("MeetF1BL", "向工作流中插入数据失败！");
                }
            }
        }
        else
        {
            buildError("MeetF1BL", "已经打印面见通知书！");

        }
        return true;
    }

    //保存打印状态数据

    private boolean saveData(VData mInputData)
    {

        //根据印刷号查询打印队列中的纪录
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setDoneDate(CurrentDate);
        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
        mResult.addElement(mLOPRTManagerSchema);
        return true;
    }

    // 下面是一些辅助函数

    private String getAgentName(String strAgentCode) throws Exception
    {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(strAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            throw new Exception("在取得LAAgent的数据时发生错误");
        }
        return tLAAgentDB.getName();
    }

    private String getComName(String strComCode) throws Exception
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            throw new Exception("在取得LDCode的数据时发生错误");
        }
        return tLDCodeDB.getCodeName();
    }

    private String getNumberToCharacter(double Num)
    {
        String cNum = String.valueOf((int) Num);
        if (cNum.equals("1"))
        {
            return "一";
        }
        if (cNum.equals("2"))
        {
            return "二";
        }
        if (cNum.equals("3"))
        {
            return "三";
        }
        if (cNum.equals("4"))
        {
            return "四";
        }
        if (cNum.equals("5"))
        {
            return "五";
        }
        if (cNum.equals("6"))
        {
            return "六";
        }
        if (cNum.equals("7"))
        {
            return "七";
        }
        if (cNum.equals("8"))
        {
            return "八";
        }
        return null;
    }
}
