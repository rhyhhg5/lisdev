package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author MengWt
 * @version 1.0
 */

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;


public class ARStatisticCSVUI {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	// 业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mrealpath = "";

	private Readhtml rh;

	private String mFileName="";
	
	private TransferData mTransferData = new TransferData();

	public ARStatisticCSVUI() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("PRINT")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData(cInputData,cOperate)) {
			return false;
		}
		
		return true;
		
	}


	/**
	 * 根据前面的输入数据，进行UI逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData(VData cInputData, String cOperate) {
		
		String strRealPath = mrealpath;
    	rh = new Readhtml();
		ARStatisticCSVBL tARStatisticBL = new ARStatisticCSVBL();

		if (!tARStatisticBL.submitData(cInputData, cOperate)) {
			if (tARStatisticBL.mErrors.needDealError()) {
				mErrors.copyAllErrors(tARStatisticBL.mErrors);
                return false;
			} else {
				buildError("submitData", "PayColPrtBL发生错误，但是没有提供详细的出错信息");
				return false;
			}
		} 
		
        String[] tInputEntry = new String[1];
        tInputEntry[0] = strRealPath+"vtsfile/"+tARStatisticBL.getMFileName() + ".csv";
        mFileName=tARStatisticBL.getMFileName()+".zip";
		String tZipFile=mrealpath+ "vtsfile/"+mFileName;
	    rh.CreateZipFile(tInputEntry, tZipFile);
				
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
    	mrealpath = (String) mTransferData.getValueByName("realpath");
        return true;
	}

	public VData getResult() {
		return this.mResult;
	}
	
	public String getFileName() {
		return mFileName;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "DerferAppF1PUI";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {
		ARStatisticCSVUI tARStatisticUI = new ARStatisticCSVUI();
		VData tVData = new VData();
		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.ManageCom = "86";
		tGlobalInput.Operator = "001";
		tVData.add(tGlobalInput);
		tARStatisticUI.submitData(tVData, "PRINT");
	}
}
