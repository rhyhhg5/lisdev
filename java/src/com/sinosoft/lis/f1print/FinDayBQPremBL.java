package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author
 * @version 1.0
 */

import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Vector;

import com.sinosoft.lis.bl.LCGrpPolBL;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LJAGetEndorseDB;
import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CodeJudge;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class FinDayBQPremBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	// 取得的时间
	private String mDay[] = null;

	// 业务处理相关变量
	private String mRiskCode;

	private String mRiskType;

	private String mXsqd;

	private String mXzdl;

	private String mSxq;

	private String mDqj;

	private LMRiskAppSet mLMRiskAppSet;

	private String mRiskName = "";

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	public FinDayBQPremBL() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		if (!getInputData(cInputData)) {
			return false;
		}
		mResult.clear();
		/*
		 * 准备所有要打印的数据 if (cOperate.equals("PRINTGET")) //打印付费 { if(
		 * !getPrintDataGet() ) { return false; } }
		 */
		LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
		this.mLMRiskAppSet = tLMRiskAppDB.query();
		if (cOperate.equals("PRINTPAY")) // 打印收费
		{
			if (!getPrintDataPay()) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		GlobalInput tG = new GlobalInput();
		tG.Operator = "001";
		tG.ManageCom = "86110000";
		VData vData = new VData();
		String[] tDay = new String[2];
		tDay[0] = "2004-1-13";
		tDay[1] = "2004-1-13";
		vData.addElement(tDay);
		vData.addElement(tG);

		FinDayBQPremBL tF = new FinDayBQPremBL();
		tF.submitData(vData, "PRINTPAY");

	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) // 打印付费
	{
		// 全局变量
		mDay = (String[]) cInputData.get(0);
		// 11-26
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "FinDayCheckBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	/**
	 * 新的打印方法
	 * 
	 * @return
	 */
	private boolean getPrintDataPay() {
		SSRS tSSRS = new SSRS();

		GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
		tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
		tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
		tGetSQLFromXML.setParameters("EndDate", mDay[1]);

	    String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
		// String tServerPath = "E:/sinowork/picch/WebRoot/";
		ExeSQL tExeSQL = new ExeSQL();

		String nsql = "select Name from LDCom where ComCode='"
				+ mGlobalInput.ManageCom + "'"; // 管理机构
		tSSRS = tExeSQL.execSQL(nsql);
		String manageCom = tSSRS.GetText(1, 1);
		TextTag texttag = new TextTag(); // 新建一个TextTag的实例
		XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
		xmlexport.createDocument("FinDayBQPrem.vts", "printer"); // 最好紧接着就初始化xml文档
		texttag.add("StartDate", mDay[0]);
		texttag.add("EndDate", mDay[1]);
		texttag.add("ManageCom", manageCom);
		if (texttag.size() > 0) {
			xmlexport.addTextTag(texttag);
		}
		String[] detailArr = new String[] { "类型", "渠道", "批单号码", "保单号码", "金额" };
		String[] totalArr = new String[] { "类型", "总金额" };

		// 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
		String[] getTypes = new String[] { "BQTF", // 保全退费
				"BQTBJ", // 退保金
				"BQYEI", // YEI
				"BQMJ", // 满期金
				"BQSSBF", // 实收保费
				"BQTJ", // 体检费
				"BQFX", // 保单复效利息
				"BQCJ", // 保户储金及投资款
				"BQTZLX", // 投资款业务利息
				"BQGL", // 账户管理费
				"BQGB" // 工本费
		};

		for (int i = 0; i < getTypes.length; i++) {
			String msql = tGetSQLFromXML.getSql(tServerPath
					+ "f1print/picctemplate/FeePrintSql.xml", getTypes[i]);
			ListTable tDetailListTable = getDetailListTable(msql, getTypes[i]); // 明细ListTable
			xmlexport.addListTable(tDetailListTable, detailArr); // 明细ListTable
																	// 放入xmlexport对象

			// 汇总的SQL语句是在明细名字后加个 "Z" 代表汇总
			msql = tGetSQLFromXML
					.getSql(tServerPath
							+ "f1print/picctemplate/FeePrintSql.xml",
							getTypes[i] + "Z");
			ListTable tTotalListTabel = getHZListTable(msql, getTypes[i] + "Z"); // 汇总ListTable
			xmlexport.addListTable(tTotalListTabel, totalArr); // 汇总ListTable
																// 放入xmlexport对象
		}

		mResult.clear();
		mResult.addElement(xmlexport);
		return true;
	}

	/**
	 * 获得明细ListTable
	 * 
	 * @param msql -
	 *            执行的SQL
	 * @param tName -
	 *            Table Name
	 * @return
	 */
	private ListTable getDetailListTable(String msql, String tName) {
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(msql);
		ListTable tlistTable = new ListTable();
		String strSum = "";
		String strArr[] = null;
		tlistTable.setName(tName);
		for (int i = 1; i <= tSSRS.MaxRow; i++) {
			strArr = new String[5];
			for (int j = 1; j <= tSSRS.MaxCol; j++) {
				if (j == 5) {
					strArr[j - 1] = tSSRS.GetText(i, j);
					strSum = new DecimalFormat("0.00").format(Double
							.valueOf(strArr[j - 1]));
					strArr[j - 1] = strSum;
					continue;
				}
				strArr[j - 1] = tSSRS.GetText(i, j);
			}
			tlistTable.add(strArr);
		}
		return tlistTable;
	}

	/**
	 * 显示无金额的情况
	 * 
	 * @param pName
	 * @return
	 */
	private String[] getChineseNameWithPY(String pName) {
		String[] result = new String[2];
		if ("BQTFZ".equals(pName)) {
			result[0] = "保全退费 小计";
		} else if ("BQTBJZ".equals(pName)) {
			result[0] = "退保金 小计";
		} else if ("BQYEIZ".equals(pName)) {
			result[0] = "YEI 小计";
		} else if ("BQMJZ".equals(pName)) {
			result[0] = "满期金 小计";
		} else if ("BQSSBFZ".equals(pName)) {
			result[0] = "实收保费 小计";
		} else if ("BQTJZ".equals(pName)) {
			result[0] = "体检费 小计";
		} else if ("BQFXZ".equals(pName)) {
			result[0] = "保单复效利息 小计";
		} else if ("BQCJZ".equals(pName)) {
			result[0] = "保户储金及投资款 小计";
		} else if ("BQTZLXZ".equals(pName)) {
			result[0] = "投资款业务利息 小计";
		} else if ("BQGLZ".equals(pName)) {
			result[0] = "账户管理费 小计";
		} else if ("BQGBZ".equals(pName)) {
			result[0] = "工本费 小计";
		}

		result[1] = "0";
		return result;
	}

	/**
	 * 获得汇总ListTable
	 * 
	 * @param msql -
	 *            执行的SQL
	 * @param tName -
	 *            Table Name
	 * @return
	 */
	private ListTable getHZListTable(String msql, String tName) {
		ListTable tHZlistTable = new ListTable();
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(msql);
		tHZlistTable.setName(tName); // 设置Table名字
		String[] strArr = new String[2];
		if (tSSRS.MaxRow > 0) {
			for (int j = 1; j <= tSSRS.MaxCol; j++) {
				strArr[j - 1] = tSSRS.GetText(1, j);
			}
		} else {
			strArr = getChineseNameWithPY(tName);
		}
		tHZlistTable.add(strArr);
		return tHZlistTable;
	}

	/**
	 * 以前的打印方法
	 * @return
	 */
	private boolean getPrintDataPayOld() {
		SSRS tSSRS = new SSRS();
		double SumMoney = 0;

		// String msql = "select a.c,sum(a) from ("+
		// "select b.endorsementno c,(a.sumactupaymoney) a from LJAPay
		// a,ljagetendorse b where 1=1 "+
		// "and a.ConfDate>='"+mDay[0]+"' and a.ConfDate<='"+mDay[1]+"' and
		// a.ManageCom like '"+mGlobalInput.ManageCom+"%' "+
		// "and a.sumactupaymoney <> 0 "+
		// "and (b.riskcode not in (Select RiskCode From LMRiskApp Where
		// Risktype3 = '7' and riskcode not in ('170101','170301')) or
		// b.riskcode is null) "+
		// "and a.IncomeType in('10','3') and a.payno = b.actugetno and
		// b.FeeFinaType in ('BF','LX','TB','JK','HK','HD','GB','YHSL','TJ') " +
		// "union " +
		// "select b.endorsementno c,(a.sumgetmoney) a from LJAGet
		// a,ljagetendorse b where 1=1 " +
		// "and a.ConfDate>='"+mDay[0]+"' and a.ConfDate<='"+mDay[1]+"' and
		// a.ManageCom like '"+mGlobalInput.ManageCom+"%' "+
		// "and a.sumgetmoney <> 0 "+
		// "and (b.riskcode not in (Select RiskCode From LMRiskApp Where
		// Risktype3 = '7' and riskcode not in ('170101','170301')) or
		// b.riskcode is null) "+
		// "and a.Othernotype in ('10','3') and a.paymode not in ('B','J') and
		// a.actugetno = b.actugetno "+
		// "and b.FeeFinaType in ('BF','LX','HK','HD','GB','YHSL','TJ') "+
		// ") a group by a.c";
		GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
		tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
		tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
		tGetSQLFromXML.setParameters("EndDate", mDay[1]);
		String tServerPath = (new ExeSQL())
				.getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
		String msql = tGetSQLFromXML.getSql(tServerPath
				+ "f1print/picctemplate/FeePrintSql.xml", "BQPrem");

		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(msql);
		ListTable tlistTable = new ListTable();
		String strArr[] = null;
		tlistTable.setName("RISK");
		for (int i = 1; i <= tSSRS.MaxRow; i++) {
			strArr = new String[2];
			for (int j = 1; j <= tSSRS.MaxCol; j++) {
				if (j == 1) {
					strArr[j - 1] = tSSRS.GetText(i, j);
				}
				if (j == 2) {
					strArr[j - 1] = tSSRS.GetText(i, j);
					String strSum = new DecimalFormat("0.00").format(Double
							.valueOf(strArr[j - 1]));
					strArr[j - 1] = strSum;
					System.out.println(Double.parseDouble(strArr[j - 1]));
					SumMoney = SumMoney + Double.parseDouble(strArr[j - 1]);

				}
			}
			tlistTable.add(strArr);
		}

		strArr = new String[2];
		strArr[0] = "ENDORNO";
		strArr[1] = "Money";

		String nsql = "select Name from LDCom where ComCode='"
				+ mGlobalInput.ManageCom + "'";
		tSSRS = tExeSQL.execSQL(nsql);
		String manageCom = tSSRS.GetText(1, 1);

		TextTag texttag = new TextTag(); // 新建一个TextTag的实例
		XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
		xmlexport.createDocument("FinDayBQPrem.vts", "printer"); // 最好紧接着就初始化xml文档
		texttag.add("StartDate", mDay[0]);
		texttag.add("EndDate", mDay[1]);
		texttag.add("ManageCom", manageCom);
		texttag.add("SumMoney", SumMoney);
		if (texttag.size() > 0) {
			xmlexport.addTextTag(texttag);
		}
		xmlexport.addListTable(tlistTable, strArr);
		mResult.clear();
		mResult.addElement(xmlexport);
		return true;
	}

}
