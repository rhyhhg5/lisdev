/**
 * <p>
 * ClassName: LLUnEndCaseStatisticsUI.java
 * </p>
 * <p>
 * Description: 
 * </p>
 * <p>
 * Copyright: Copyright (c) 2009
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @Database:
 * @CreateDate：2010-6-22
 */

// 包名
package com.sinosoft.lis.f1print;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.pubfun.*;

public class LLUnEndCaseStatisticsUI {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();
	
	private String mFileName = "";

	/** 数据操作字符串 */
	private String mOperate;

	// 业务处理相关变量

	public LLUnEndCaseStatisticsUI() {

	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		LLUnEndCaseStatisticsBL tLLUnEndCaseStatisticsBL = new LLUnEndCaseStatisticsBL();
		tLLUnEndCaseStatisticsBL.submitData(cInputData, mOperate);
		// 如果有需要处理的错误，则返回
		if (tLLUnEndCaseStatisticsBL.mErrors.needDealError()) {
			// @@错误处理
			this.mErrors.copyAllErrors(tLLUnEndCaseStatisticsBL.mErrors);
			CError tError = new CError();
			tError.moduleName = "LLUnEndCaseStatisticsUI";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		this.mResult.clear();
		this.mResult = tLLUnEndCaseStatisticsBL.getResult();
		this.mFileName = tLLUnEndCaseStatisticsBL.getFileName();
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}
	
	public String getFileName(){
		return this.mFileName;
	}
	
	public static void main(String[] args) {

	}

}
