package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author MengWt
 * @version 1.0
 */
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;

public class ScanStatisticBL implements PrintService {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    public ScanStatisticBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));

        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        if (mTransferData == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "WorkEfficiencyBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        VData result = new VData();
        //统计的参数
        String OperatorManagecom = (String) mTransferData.getValueByName(
                "OperatorManagecom");

        String Managecom = (String) mTransferData.getValueByName("Managecom");

        String StartDate = (String) mTransferData.getValueByName("StartDate");

        String EndDate = (String) mTransferData.getValueByName("EndDate");

        String QYType = (String) mTransferData.getValueByName("QYType");

        String QYType_ch = (String) mTransferData.getValueByName("QYType_ch");

        String QYType11 = (String) mTransferData.getValueByName("QYType1");

        String ScanOperator1 = (String) mTransferData.getValueByName(
                "ScanOperator1");

        String MakeDate = PubFun.getCurrentDate();
        StringBuffer sql = new StringBuffer();
        /** 数据库查询 */
        ExeSQL tExeSQL = new ExeSQL();
        /** 返回结果集 */
        SSRS ssrs = null;

        if (Managecom.length() == 2) {
            SSRS tSSRS_M;
            String sql_M = "select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode";
            System.out.println("zhuzhu" + sql_M);
            tExeSQL = new ExeSQL();
            tSSRS_M = tExeSQL.execSQL(sql_M);
            String temp_M[][] = tSSRS_M.getAllData();
            String Mng;
            if (Managecom.equals("86")) {
                sql.append("select '86', ScanOperator,'" +
                           QYType_ch +
                           "',count(DocID),sum(NumPages) from ES_DOC_MAIN where " +
                           QYType11 +
                           " and managecom='86' " + ScanOperator1 +
                           " group by ScanOperator,SubType");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                } // if (ssrs != null) {
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");

                if (tSSRS_M.getMaxRow() > 0) {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++) {
                        Mng = temp_M[i - 1][0];
                        String Mng1;
                        if (Managecom.equals("86000000")) {
                            Mng1 = Mng;
                        } else {
                            Mng1 = Mng.substring(0, 4);
                        }
                        sql = new StringBuffer();
                        sql.append("select '" + Mng1 + "', ScanOperator,'" +
                                   QYType_ch +
                                   "',count(DocID),sum(NumPages) from ES_DOC_MAIN where " +
                                   QYType11 +
                                   " and substr(managecom,1,length('" +
                                   Managecom + "')*2)='" + Mng1 +
                                   "' " + ScanOperator1 +
                                   " group by ScanOperator,SubType");
                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null) {
                            if (ssrs.getMaxRow() > 0) {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第二个Sql语句~~~~~");
                    }
                    sql = new StringBuffer();
                    sql.append("select '总计',ScanOperator,'" + QYType_ch +
                               "',count(DocID),sum(NumPages) from ES_DOC_MAIN where " +
                               QYType11 + "" + ScanOperator1 +
                               " group by ScanOperator,SubType");
                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null) {
                        if (ssrs.getMaxRow() > 0) {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    System.out.println("处理完成第三个Sql语句~~~~~");
                } else {
                    if (tSSRS_M.getMaxRow() > 0) {
                        for (int i = 1; i <= tSSRS_M.getMaxRow(); i++) {
                            Mng = temp_M[i - 1][0];
                            String Mng1;
                            if (Managecom.equals("86000000")) {
                                Mng1 = Mng;
                            } else {
                                Mng1 = Mng.substring(0, 4);
                            }
                            sql = new StringBuffer();
                            sql.append("select '" + Mng1 + "', ScanOperator,'" +
                                       QYType_ch +
                                       "',count(DocID),sum(NumPages) from ES_DOC_MAIN where " +
                                       QYType11 +
                                       " and substr(managecom,1,length('" +
                                       Managecom + "'))='" + Mng1 +
                                       "' " + ScanOperator1 +
                                       " group by ScanOperator,SubType");
                            ssrs = tExeSQL.execSQL(sql.toString());
                            if (ssrs != null) {
                                if (ssrs.getMaxRow() > 0) {
                                    for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                        result.add(ssrs.getRowData(m));
                                    }
                                }
                            }
                            ssrs = null;
                            sql = new StringBuffer();
                            System.out.println("处理完成第一个Sql语句~~~~~");
                        }
                        sql = new StringBuffer();
                        sql.append("select '总计',ScanOperator,'" + QYType_ch +
                                   "',count(DocID),sum(NumPages) from ES_DOC_MAIN where " +
                                   QYType11 + "" + ScanOperator1 +
                                   " group by ScanOperator,SubType");
                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null) {
                            if (ssrs.getMaxRow() > 0) {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        System.out.println("处理完成第二个Sql语句~~~~~");
                    }
                }
            }
            } else if (Managecom.length() == 4) {
                String Mng2;
                String Mng11 = tExeSQL.getOneValue("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='" +
                        Managecom +
                        "' order by ComCode");
                Mng2 = Mng11.substring(0, 4);
                if (Managecom.equals(Mng2)) {
                    sql.append("select '" + Mng2 + "', ScanOperator,'" +
                               QYType_ch +
                               "',count(DocID),sum(NumPages) from ES_DOC_MAIN where " +
                               QYType11 + " and substr(managecom,1,length('" +
                               Managecom + "')*2)='" + Mng2 +
                               "' " + ScanOperator1 +
                               " group by ScanOperator,SubType");
                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null) {
                        if (ssrs.getMaxRow() > 0) {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    sql = new StringBuffer();
                    System.out.println("处理完成第一个Sql语句~~~~~");

                    sql = new StringBuffer();
                    sql.append("select '" + Mng11 + "', ScanOperator,'" +
                               QYType_ch +
                               "',count(DocID),sum(NumPages) from ES_DOC_MAIN where " +
                               QYType11 + " and substr(managecom,1,length('" +
                               Managecom + "')*2)='" + Mng11 +
                               "' " + ScanOperator1 +
                               " group by ScanOperator,SubType");
                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null) {
                        if (ssrs.getMaxRow() > 0) {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    sql = new StringBuffer();
                    System.out.println("处理完成第二个Sql语句~~~~~");

                    sql = new StringBuffer();
                    sql.append("select '总计', ScanOperator,'" + QYType_ch +
                               "',count(DocID),sum(NumPages) from ES_DOC_MAIN where " +
                               QYType11 + " and substr(managecom,1,length('" +
                               Managecom + "'))='" + Mng2 +
                               "' " + ScanOperator1 +
                               " group by ScanOperator,SubType");
                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null) {
                        if (ssrs.getMaxRow() > 0) {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    sql = new StringBuffer();
                    System.out.println("处理完成第三个Sql语句~~~~~");
                } else {
                    sql = new StringBuffer();
                    sql.append("select '" + Mng11 + "', ScanOperator,'" +
                               QYType_ch +
                               "',count(DocID),sum(NumPages) from ES_DOC_MAIN where " +
                               QYType11 + " and substr(managecom,1,length('" +
                               Managecom + "')*2)='" + Mng11 +
                               "' " + ScanOperator1 +
                               " group by ScanOperator,SubType");
                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null) {
                        if (ssrs.getMaxRow() > 0) {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    sql = new StringBuffer();
                    System.out.println("处理完成第一个Sql语句~~~~~");

                    sql = new StringBuffer();
                    sql.append("select '总计',ScanOperator,'" + QYType_ch +
                               "',count(DocID),sum(NumPages) from ES_DOC_MAIN where " +
                               QYType11 + " and substr(managecom,1,length('" +
                               Managecom + "')*2)='" + Mng11 +
                               "' " + ScanOperator1 +
                               " group by ScanOperator,SubType");
                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null) {
                        if (ssrs.getMaxRow() > 0) {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    sql = new StringBuffer();
                    System.out.println("处理完成第二个Sql语句~~~~~");
                }

            } else if (Managecom.length() == 8) {
                sql = new StringBuffer();
                sql.append("select '" + Managecom + "', ScanOperator,'" +
                           QYType_ch +
                           "',count(DocID),sum(NumPages) from ES_DOC_MAIN where " +
                           QYType11 + " and substr(managecom,1,length('" +
                           Managecom + "'))='" + Managecom +
                           "' " + ScanOperator1 +
                           " group by ScanOperator,SubType");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql = new StringBuffer();
                sql.append("select '总计',ScanOperator,'" + QYType_ch +
                           "',count(DocID),sum(NumPages) from ES_DOC_MAIN where '" +
                           QYType11 + "' and substr(managecom,1,length(" +
                           Managecom + "'))=" + Managecom +
                           "' " + ScanOperator1 +
                           " group by ScanOperator,SubType");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第二个Sql语句~~~~~");

            }

            ListTable tListTable = new ListTable();
            tListTable.setName("WorkEfficencyReport");
            String[] title = {"A", "B", "C", "D", "E"};
            /** To MengWT ：tListTable将保存每一行的动态数据，其效果就是
             * ┎─┰─┰─┰─┰─┰─┰─┰─┰─┰─┒
             *   A1 B1  C1 D1  E1 F1  G1 H1  I1 J1
             *   A2 B2  C2 D2  E2 F2  G2 H2  I2 J2
             * ┖─┸─┸─┸─┸─┸─┸─┸─┸─┸─┚
             * 就是说 A - J 必须是一维的（一个数据）的数据
             * 不能出现A1--An是一个数据 B1--Bn是一个数组，
             * 这样会无法显示。
             *  */
            for (int i = 0; i < result.size(); i++) {
                tListTable.add((String[]) result.get(i));
            }

            XmlExport xml = new XmlExport(); //新建一个XmlExport的实例
            xml.createDocument("ScanStatistic.vts", "printer"); //最好紧接着就初始化xml文档
            //生成-年-月-日格式的日期
            StrTool tSrtTool = new StrTool();
            String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() +
                             "月" +
                             tSrtTool.getDay() + "日";

            //WorkEfficiencyBL模版元素
            StartDate = StartDate.replace('-', '月');
            StartDate = StartDate.replaceFirst("月", "年") + "日";
            EndDate = EndDate.replace('-', '月');
            EndDate = EndDate.replaceFirst("月", "年") + "日";
            MakeDate = MakeDate.replace('-', '月');
            MakeDate = MakeDate.replaceFirst("月", "年") + "日";
            texttag.add("StartDate", StartDate);
            texttag.add("EndDate", EndDate);
            texttag.add("QYType_ch", QYType_ch);
            texttag.add("MakeDate", MakeDate);
            SSRS tSSRS_M = new SSRS();
            String sql_M = "select Name from ldcom where comcode = '" +
                           OperatorManagecom +
                           "'";
            System.out.println("zhuzhu" + sql_M);
            tExeSQL = new ExeSQL();
            tSSRS_M = tExeSQL.execSQL(sql_M);
            String temp_M[][] = tSSRS_M.getAllData();
            texttag.add("MngCom", temp_M[0][0]);

            if (texttag.size() > 0) {
                xml.addTextTag(texttag);
            }

            //保存信息
            xml.addListTable(tListTable, title);
            /** 添加完成将输出的结果保存到结果集中 */
            mResult.add(xml);
            mResult.clear();
            mResult.addElement(xml);

            return true;
        }

        public static void main(String[] args) {
            VData tVData = new VData();
            ScanStatisticBL tWorkEfficiencyBL = new ScanStatisticBL();
            GlobalInput tGlobalInput = new GlobalInput();
            tGlobalInput.ManageCom = "8611";
            tGlobalInput.Operator = "001";
            tVData.addElement(tGlobalInput);
            TransferData mTransferData = new TransferData();
            mTransferData.setNameAndValue("OperatorManagecom", "8611");
            mTransferData.setNameAndValue("Managecom", "86");
            mTransferData.setNameAndValue("StartDate", "2005-1-1");
            mTransferData.setNameAndValue("EndDate", "2005-11-1");
            mTransferData.setNameAndValue("QYType", "1");
            mTransferData.setNameAndValue("QYType1",
                                          " SubType like 'TB%' and makedate between '2005-1-1' and '2005-12-31'");
            mTransferData.setNameAndValue("ScanOperator1", " ");
            tVData.add(mTransferData);
            tWorkEfficiencyBL.submitData(tVData, "PRINT");
            VData vData = tWorkEfficiencyBL.getResult();
        }
    }


