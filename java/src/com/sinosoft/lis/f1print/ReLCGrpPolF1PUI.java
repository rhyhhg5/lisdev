/*
 * <p>ClassName: ReLCGrpPolF1PUI </p>
 * <p>Description: ReLCGrpPolF1PUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-11-04
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ReLCGrpPolF1PUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
    private String mszTemplatePath = null;

    public ReLCGrpPolF1PUI()
    {
    }

    /**
                  传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("PRINT")
                && !cOperate.equals("CONFIRM"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            // 进行业务处理
            if (!dealData())
            {
                return false;
            }

            // 准备传往后台的数据
            VData vData = new VData();

            if (!prepareOutputData(vData))
            {
                return false;
            }

            ReLCGrpPolF1PBL tReLCGrpPolF1PBL = new ReLCGrpPolF1PBL();
            System.out.println("Start ReLCGrpPolF1P UI Submit ...");

            if (!tReLCGrpPolF1PBL.submitData(vData, cOperate))
            {
                if (tReLCGrpPolF1PBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tReLCGrpPolF1PBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("sbumitData", "tLCGrpPolF1PBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tReLCGrpPolF1PBL.getResult();
                return true;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submit", "发生异常");
            return false;
        }
    }

    public static void main(String[] args)
    {
        ReLCGrpPolF1PUI tReLCGrpPolF1PUI = new ReLCGrpPolF1PUI();
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();

        tLCGrpPolSchema.setGrpPolNo("86110020030220000080");
        tLCGrpPolSet.add(tLCGrpPolSchema);

        VData vData = new VData();

        vData.addElement(new GlobalInput());
        vData.addElement(tLCGrpPolSet);
        vData.addElement("");

        if (!tReLCGrpPolF1PUI.submitData(vData, "CONFIRM"))
        {
            System.out.println("fail to get print data");
        }
        else
        {
            vData = tReLCGrpPolF1PUI.getResult();
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.add(mGlobalInput);
            vData.add(mLCGrpPolSet);
            vData.add(mszTemplatePath);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLCGrpPolSet.set((LCGrpPolSet) cInputData.getObjectByObjectName(
                "LCGrpPolSet", 0));
        mszTemplatePath = (String) cInputData.getObjectByObjectName("String", 0);

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCGrpPolF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}