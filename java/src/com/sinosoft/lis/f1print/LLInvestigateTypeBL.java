package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author St.GN
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;
import java.text.DecimalFormat;
import com.sinosoft.lis.f1print.*;
import java.lang.*;

public class LLInvestigateTypeBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();

    public LLInvestigateTypeBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        if (mTransferData == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LLInvestigateTypeBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        //统计的起讫时间
        String StartDate = (String) mTransferData.getValueByName("Begin");
        String EndDate = (String) mTransferData.getValueByName("End");
        String ManageCom = (String) mTransferData.getValueByName("ManageCom");

        //调查件信息
        ListTable InvestigateTable = new ListTable();
        InvestigateTable.setName("INVESTIGATE");
        String[] Title = {
                         "type", "num", "amnt", "occurraito","claimratio", "normaln", "normalm",
                         "partn","partm","trn","trm","refusen","refusem","xyn","xym"
        };
        try {
            SSRS tSSRS = new SSRS();
            String sql =  " SELECT '调查件', CASE WHEN A IS NULL THEN 0 ELSE A END,"
                         +" CASE WHEN B IS NULL THEN 0 ELSE B END,"
                         +" CASE WHEN A IS NULL OR C IS NULL OR C=0 THEN 0 ELSE decimal(decimal(A)*100/decimal(C),12,2) END,"
                         +" CASE WHEN B IS NULL OR GpayR IS NULL OR PpayR IS NULL OR PpayR*GpayR=0 THEN 0 ELSE decimal(decimal(B)*100/decimal(GpayR+PpayR),12,2) END,"
                 +" CASE WHEN NormalNum IS NULL THEN 0 ELSE NormalNum END,"
                 +" CASE WHEN NormalPay IS NULL THEN 0 ELSE NormalPay END,"
                 +" CASE WHEN PartNum IS NULL THEN 0 ELSE PartNum END,"
                 +" CASE WHEN PartPay IS NULL THEN 0 ELSE PartPay END,"
                 +" CASE WHEN TRNum IS NULL THEN 0 ELSE TRNum END,"
                 +" CASE WHEN TRPay IS NULL THEN 0 ELSE TRPay END,"
                 +" CASE WHEN RefuseNum IS NULL THEN 0 ELSE RefuseNum END,"
                 +" CASE WHEN RefusePay IS NULL THEN 0 ELSE RefusePay END,"
                 +" CASE WHEN XYNum IS NULL THEN 0 ELSE XYNum END,"
                 +" CASE WHEN XYPay IS NULL THEN 0 ELSE XYPay END "
                 +" FROM "
                 +" (SELECT"
                 +" count(b.caseno) A, sum(a.realpay) B,"
                 +" (select count(s.caseno) from llcase s where s.endcasedate is not null) C,"
                 +" (select (1-0.25)*SUM(F.Prem)*ROUND((TO_DATE('"+EndDate+"')-TO_DATE('"+StartDate+"')+1)/365,2) FROM LCPOL F WHERE F.Grpcontno <> '00000000000000000000' and F.riskcode <> '1605' and F.AppFlag='1' and TO_DATE(GETCINVALIDATE(F.POLNO)) >= TO_DATE('"+StartDate+"') AND TO_DATE(F.Cvalidate) <= TO_DATE('"+EndDate+"') ) GpayR,"
                 +" (select (1-0.35)*SUM(F.Prem)*ROUND((TO_DATE('"+EndDate+"')-TO_DATE('"+StartDate+"')+1)/365,2) FROM LCPOL F WHERE F.Grpcontno = '00000000000000000000' and F.AppFlag='1' and TO_DATE(GETCINVALIDATE(F.POLNO)) >= TO_DATE('"+StartDate+"') AND TO_DATE(F.Cvalidate) <= TO_DATE('"+EndDate+"') ) PpayR,"
                 +" (select count(a.caseno) from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '1' and b.endcasedate is not null and b.surveyflag='1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') NormalNum,"
                 +" (select sum(a.realpay)  from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '1' and b.endcasedate is not null and b.surveyflag='1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') NormalPay,"
                 +" (select count(a.caseno) from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '2' and b.endcasedate is not null and b.surveyflag='1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') PartNum,"
                 +" (select sum(a.realpay)  from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '2' and b.endcasedate is not null and b.surveyflag='1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') PartPay,"
                 +" (select count(a.caseno) from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '5' and b.endcasedate is not null and b.surveyflag='1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') TRNum,"
                 +" (select sum(a.realpay)  from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '5' and b.endcasedate is not null and b.surveyflag='1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') TRPay,"
                 +" (select count(a.caseno) from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '3' and b.endcasedate is not null and b.surveyflag='1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') RefuseNum,"
                 +" (select sum(a.realpay)  from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '3' and b.endcasedate is not null and b.surveyflag='1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') RefusePay,"
                 +" (select count(a.caseno) from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '4' and b.endcasedate is not null and b.surveyflag='1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') XYNum,"
                 +" (select sum(a.realpay)  from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '4' and b.endcasedate is not null and b.surveyflag='1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') XYPay"
                 +" FROM  llclaim a, llcase b "
                 +" where a.caseno = b.caseno and b.endcasedate  is not null and b.surveyflag = '1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"' "
                 +") as x "
                 ;
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            for (int i = 0; i < count; i++) {
                String temp[][] = tSSRS.getAllData();

                String[] strCol;
                strCol = new String[15];
                strCol[0] = temp[i][0];
                strCol[1] = temp[i][1];
                strCol[2] = temp[i][2];
                strCol[3] = temp[i][3];
                strCol[4] = temp[i][4];
                strCol[5] = temp[i][5];
                strCol[6] = temp[i][6];
                strCol[7] = temp[i][7];
                strCol[8] = temp[i][8];
                strCol[9] = temp[i][9];
                strCol[10] = temp[i][10];
                strCol[11] = temp[i][11];
                strCol[12] = temp[i][12];
                strCol[13] = temp[i][13];
                strCol[14] = temp[i][14];
     System.out.println(temp[i][0]+temp[i][1]+temp[i][2]+temp[i][3]+temp[i][4]);
                InvestigateTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "LLInvestigateTypeBL";
            tError.functionName = "";
            tError.errorMessage = "客户基本信息查询失败";
            this.mErrors.addOneError(tError);
        }

        //非调查件信息
        ListTable UnInvestiTable = new ListTable();
        UnInvestiTable.setName("UNINVESTIGATE");
        String[] Title_un = {
                            "type", "num", "amnt", "occurraito","claimratio", "normaln", "normalm",
                         "partn","partm","trn","trm","refusen","refusem","xyn","xym"
        };
        try {
            SSRS tSSRS = new SSRS();
            String sql =  " SELECT '非调查件', CASE WHEN A IS NULL THEN 0 ELSE A END,"
                         +" CASE WHEN B IS NULL THEN 0 ELSE B END,"
                         +" CASE WHEN A IS NULL OR C IS NULL OR C=0 THEN 0 ELSE decimal(decimal(A)*100/decimal(C),12,2) END,"
                         +" CASE WHEN B IS NULL OR GpayR IS NULL OR PpayR IS NULL OR PpayR*GpayR=0 THEN 0 ELSE decimal(decimal(B)*100/decimal(GpayR+PpayR),12,2) END,"
                 +" CASE WHEN NormalNum IS NULL THEN 0 ELSE NormalNum END,"
                 +" CASE WHEN NormalPay IS NULL THEN 0 ELSE NormalPay END,"
                 +" CASE WHEN PartNum IS NULL THEN 0 ELSE PartNum END,"
                 +" CASE WHEN PartPay IS NULL THEN 0 ELSE PartPay END,"
                 +" CASE WHEN TRNum IS NULL THEN 0 ELSE TRNum END,"
                 +" CASE WHEN TRPay IS NULL THEN 0 ELSE TRPay END,"
                 +" CASE WHEN RefuseNum IS NULL THEN 0 ELSE RefuseNum END,"
                 +" CASE WHEN RefusePay IS NULL THEN 0 ELSE RefusePay END,"
                 +" CASE WHEN XYNum IS NULL THEN 0 ELSE XYNum END,"
                 +" CASE WHEN XYPay IS NULL THEN 0 ELSE XYPay END "
                 +" FROM "
                 +" (SELECT"
                 +" count(b.caseno) A, sum(a.realpay) B,"
                 +" (select count(s.caseno) from llcase s where s.endcasedate is not null) C,"
                 +" (select (1-0.25)*SUM(F.Prem)*ROUND((TO_DATE('"+EndDate+"')-TO_DATE('"+StartDate+"')+1)/365,2) FROM LCPOL F WHERE F.Grpcontno <> '00000000000000000000' and F.riskcode <> '1605' and F.AppFlag='1' and TO_DATE(GETCINVALIDATE(F.POLNO)) >= TO_DATE('"+StartDate+"') AND TO_DATE(F.Cvalidate) <= TO_DATE('"+EndDate+"') ) GpayR,"
                 +" (select (1-0.35)*SUM(F.Prem)*ROUND((TO_DATE('"+EndDate+"')-TO_DATE('"+StartDate+"')+1)/365,2) FROM LCPOL F WHERE F.Grpcontno = '00000000000000000000'  and F.AppFlag='1' and TO_DATE(GETCINVALIDATE(F.POLNO)) >= TO_DATE('"+StartDate+"') AND TO_DATE(F.Cvalidate) <= TO_DATE('"+EndDate+"') ) PpayR,"
                 +" (select count(a.caseno) from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '1' and b.endcasedate is not null and b.surveyflag is null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') NormalNum,"
                 +" (select sum(a.realpay)  from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '1' and b.endcasedate is not null and b.surveyflag is null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') NormalPay,"
                 +" (select count(a.caseno) from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '2' and b.endcasedate is not null and b.surveyflag is null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') PartNum,"
                 +" (select sum(a.realpay)  from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '2' and b.endcasedate is not null and b.surveyflag is null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') PartPay,"
                 +" (select count(a.caseno) from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '5' and b.endcasedate is not null and b.surveyflag is null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') TRNum,"
                 +" (select sum(a.realpay)  from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '5' and b.endcasedate is not null and b.surveyflag is null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') TRPay,"
                 +" (select count(a.caseno) from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '3' and b.endcasedate is not null and b.surveyflag is null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') RefuseNum,"
                 +" (select sum(a.realpay)  from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '3' and b.endcasedate is not null and b.surveyflag is null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') RefusePay,"
                 +" (select count(a.caseno) from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '4' and b.endcasedate is not null and b.surveyflag is null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') XYNum,"
                 +" (select sum(a.realpay)  from llclaim a, llcase b where a.caseno = b.caseno and a.givetype = '4' and b.endcasedate is not null and b.surveyflag is null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') XYPay"
                 +" FROM  llclaim a, llcase b "
                 +" where a.caseno = b.caseno and b.endcasedate  is not null and b.surveyflag is null and b.rgtdate between '"+StartDate+"' and '"+EndDate+"' "
                 +") as x "
                 ;
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            for (int i = 0; i < count; i++) {
                String temp[][] = tSSRS.getAllData();

                String[] strCol;
                strCol = new String[15];
                strCol[0] = temp[i][0];
                strCol[1] = temp[i][1];
                strCol[2] = temp[i][2];
                strCol[3] = temp[i][3];
                strCol[4] = temp[i][4];
                strCol[5] = temp[i][5];
                strCol[6] = temp[i][6];
                strCol[7] = temp[i][7];
                strCol[8] = temp[i][8];
                strCol[9] = temp[i][9];
                strCol[10] = temp[i][10];
                strCol[11] = temp[i][11];
                strCol[12] = temp[i][12];
                strCol[13] = temp[i][13];
                strCol[14] = temp[i][14];
     System.out.println(temp[i][0]+temp[i][1]+temp[i][2]+temp[i][3]+temp[i][4]);
                UnInvestiTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "LLInvestigateTypeBL";
            tError.functionName = "";
            tError.errorMessage = "客户基本信息查询失败";
            this.mErrors.addOneError(tError);
        }

        //院内调查件信息
                ListTable InHospitalTable = new ListTable();
                InHospitalTable.setName("INHOS");
                String[] Title_i = {
                                    "type", "num", "amnt", "occurraito","claimratio", "normaln", "normalm",
                                 "partn","partm","trn","trm","refusen","refusem","xyn","xym"
                };
                try {
                    SSRS tSSRS = new SSRS();
                    String sql =   " SELECT '院内调查件', CASE WHEN A IS NULL THEN 0 ELSE A END, "
                                  +" CASE WHEN B IS NULL THEN 0 ELSE B END,"
                                  +" CASE WHEN A IS NULL OR C IS NULL OR C=0 THEN 0 ELSE decimal(decimal(A)*100/decimal(C),12,2) END,"
                                  +" CASE WHEN B IS NULL OR GpayR IS NULL OR PpayR IS NULL OR PpayR*GpayR=0 THEN 0 ELSE decimal(decimal(B)*100/decimal(GpayR+PpayR),12,2) END,"
                                  +" CASE WHEN NormalNum IS NULL THEN 0 ELSE NormalNum END,"
                                  +" CASE WHEN NormalPay IS NULL THEN 0 ELSE NormalPay END,"
                                  +" CASE WHEN PartNum IS NULL THEN 0 ELSE PartNum END,"
                                  +" CASE WHEN PartPay IS NULL THEN 0 ELSE PartPay END,"
                                  +" CASE WHEN TRNum IS NULL THEN 0 ELSE TRNum END,"
                                  +" CASE WHEN TRPay IS NULL THEN 0 ELSE TRPay END,"
                                  +" CASE WHEN RefuseNum IS NULL THEN 0 ELSE RefuseNum END,"
                                  +" CASE WHEN RefusePay IS NULL THEN 0 ELSE RefusePay END,"
                                  +" CASE WHEN XYNum IS NULL THEN 0 ELSE XYNum END,"
                                  +" CASE WHEN XYPay IS NULL THEN 0 ELSE XYPay END"
                                  +" FROM (SELECT count(b.caseno) A, sum(a.realpay) B,"
                                  +" (select count(s.caseno) from llcase s where s.endcasedate is not null) C,"
                                  +" (select (1-0.25)*SUM(F.Prem)*ROUND((TO_DATE('"+EndDate+"')-TO_DATE('"+StartDate+"')+1)/365,2) FROM LCPOL F WHERE F.Grpcontno <> '00000000000000000000' and F.riskcode <> '1605' and F.AppFlag='1' and TO_DATE(GETCINVALIDATE(F.POLNO)) >= TO_DATE('"+StartDate+"') AND TO_DATE(F.Cvalidate) <= TO_DATE('"+EndDate+"') ) GpayR,"
                                  +" (select (1-0.35)*SUM(F.Prem)*ROUND((TO_DATE('"+EndDate+"')-TO_DATE('"+StartDate+"')+1)/365,2) FROM LCPOL F WHERE F.Grpcontno = '00000000000000000000'  and F.AppFlag='1' and TO_DATE(GETCINVALIDATE(F.POLNO)) >= TO_DATE('"+StartDate+"') AND TO_DATE(F.Cvalidate) <= TO_DATE('"+EndDate+"') ) PpayR,"
                                  +" (select count(a.caseno) from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '1' and b.endcasedate is not null and c.surveytype = '0' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') NormalNum,"
                                  +" (select sum(a.realpay)  from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '1' and b.endcasedate is not null and c.surveytype = '0' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') NormalPay,"
                                  +" (select count(a.caseno) from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '2' and b.endcasedate is not null and c.surveytype = '0' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') PartNum,"
                                  +" (select sum(a.realpay)  from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '2' and b.endcasedate is not null and c.surveytype = '0' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') PartPay,"
                                  +" (select count(a.caseno) from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '5' and b.endcasedate is not null and c.surveytype = '0' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') TRNum,"
                                  +" (select sum(a.realpay)  from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '5' and b.endcasedate is not null and c.surveytype = '0' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') TRPay,"
                                  +" (select count(a.caseno) from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '3' and b.endcasedate is not null and c.surveytype = '0' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') RefuseNum,"
                                  +" (select sum(a.realpay)  from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '3' and b.endcasedate is not null and c.surveytype = '0' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') RefusePay,"
                                  +" (select count(a.caseno) from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '4' and b.endcasedate is not null and c.surveytype = '0' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') XYNum,"
                                  +" (select sum(a.realpay)  from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '4' and b.endcasedate is not null and c.surveytype = '0' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') XYPay"
                                  +" FROM  llclaim a, llcase b, llsurvey c"
                                  +"  where a.caseno = b.caseno and c.otherno = b.caseno and b.endcasedate is not null and c.surveytype = '0' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"' ) as x "
                                  ;
                    ExeSQL tExeSQL = new ExeSQL();
                    tSSRS = tExeSQL.execSQL(sql);
                    int count = tSSRS.getMaxRow();
                    for (int i = 0; i < count; i++) {
                        String temp[][] = tSSRS.getAllData();

                        String[] strCol;
                        strCol = new String[15];
                        strCol[0] = temp[i][0];
                        strCol[1] = temp[i][1];
                        strCol[2] = temp[i][2];
                        strCol[3] = temp[i][3];
                        strCol[4] = temp[i][4];
                        strCol[5] = temp[i][5];
                        strCol[6] = temp[i][6];
                        strCol[7] = temp[i][7];
                        strCol[8] = temp[i][8];
                        strCol[9] = temp[i][9];
                        strCol[10] = temp[i][10];
                        strCol[11] = temp[i][11];
                        strCol[12] = temp[i][12];
                        strCol[13] = temp[i][13];
                        strCol[14] = temp[i][14];
             System.out.println(temp[i][0]+temp[i][1]+temp[i][2]+temp[i][3]+temp[i][4]);
                        InHospitalTable.add(strCol);
                    }
                } catch (Exception e) {
                    CError tError = new CError();
                    tError.moduleName = "LLInvestigateTypeBL";
                    tError.functionName = "";
                    tError.errorMessage = "客户基本信息查询失败";
                    this.mErrors.addOneError(tError);
                }

                //院外调查件信息
                ListTable OutHospitalTable = new ListTable();
                OutHospitalTable.setName("OUTHOS");
                String[] Title_o = {
                                    "type", "num", "amnt", "occurraito","claimratio", "normaln", "normalm",
                                 "partn","partm","trn","trm","refusen","refusem","xyn","xym"
                };
                try {
                    SSRS tSSRS = new SSRS();
                    String sql =   " SELECT '院外调查件', CASE WHEN A IS NULL THEN 0 ELSE A END, "
                                  +" CASE WHEN B IS NULL THEN 0 ELSE B END,"
                                  +" CASE WHEN A IS NULL OR C IS NULL OR C=0 THEN 0 ELSE decimal(decimal(A)*100/decimal(C),12,2) END,"
                                  +" CASE WHEN B IS NULL OR GpayR IS NULL OR PpayR IS NULL OR PpayR*GpayR=0 THEN 0 ELSE decimal(decimal(B)*100/decimal(GpayR+PpayR),12,2) END,"
                                  +" CASE WHEN NormalNum IS NULL THEN 0 ELSE NormalNum END,"
                                  +" CASE WHEN NormalPay IS NULL THEN 0 ELSE NormalPay END,"
                                  +" CASE WHEN PartNum IS NULL THEN 0 ELSE PartNum END,"
                                  +" CASE WHEN PartPay IS NULL THEN 0 ELSE PartPay END,"
                                  +" CASE WHEN TRNum IS NULL THEN 0 ELSE TRNum END,"
                                  +" CASE WHEN TRPay IS NULL THEN 0 ELSE TRPay END,"
                                  +" CASE WHEN RefuseNum IS NULL THEN 0 ELSE RefuseNum END,"
                                  +" CASE WHEN RefusePay IS NULL THEN 0 ELSE RefusePay END,"
                                  +" CASE WHEN XYNum IS NULL THEN 0 ELSE XYNum END,"
                                  +" CASE WHEN XYPay IS NULL THEN 0 ELSE XYPay END"
                                  +" FROM (SELECT count(b.caseno) A, sum(a.realpay) B,"
                                  +" (select count(s.caseno) from llcase s where s.endcasedate is not null) C,"
                                  +" (select (1-0.25)*SUM(F.Prem)*ROUND((TO_DATE('"+EndDate+"')-TO_DATE('"+StartDate+"')+1)/365,2) FROM LCPOL F WHERE F.Grpcontno <> '00000000000000000000' and F.AppFlag='1' and F.riskcode <> '1605' and TO_DATE(GETCINVALIDATE(F.POLNO)) >= TO_DATE('"+StartDate+"') AND TO_DATE(F.Cvalidate) <= TO_DATE('"+EndDate+"') ) GpayR,"
                                  +" (select (1-0.35)*SUM(F.Prem)*ROUND((TO_DATE('"+EndDate+"')-TO_DATE('"+StartDate+"')+1)/365,2) FROM LCPOL F WHERE F.Grpcontno = '00000000000000000000'  and F.AppFlag='1' and TO_DATE(GETCINVALIDATE(F.POLNO)) >= TO_DATE('"+StartDate+"') AND TO_DATE(F.Cvalidate) <= TO_DATE('"+EndDate+"') ) PpayR,"
                                  +" (select count(a.caseno) from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '1' and b.endcasedate is not null and c.surveytype = '1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') NormalNum,"
                                  +" (select sum(a.realpay)  from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '1' and b.endcasedate is not null and c.surveytype = '1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') NormalPay,"
                                  +" (select count(a.caseno) from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '2' and b.endcasedate is not null and c.surveytype = '1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') PartNum,"
                                  +" (select sum(a.realpay)  from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '2' and b.endcasedate is not null and c.surveytype = '1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') PartPay,"
                                  +" (select count(a.caseno) from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '5' and b.endcasedate is not null and c.surveytype = '1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') TRNum,"
                                  +" (select sum(a.realpay)  from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '5' and b.endcasedate is not null and c.surveytype = '1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') TRPay,"
                                  +" (select count(a.caseno) from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '3' and b.endcasedate is not null and c.surveytype = '1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') RefuseNum,"
                                  +" (select sum(a.realpay)  from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '3' and b.endcasedate is not null and c.surveytype = '1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') RefusePay,"
                                  +" (select count(a.caseno) from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '4' and b.endcasedate is not null and c.surveytype = '1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') XYNum,"
                                  +" (select sum(a.realpay)  from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '4' and b.endcasedate is not null and c.surveytype = '1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') XYPay"
                                  +" FROM  llclaim a, llcase b, llsurvey c"
                                  +"  where a.caseno = b.caseno and c.otherno = b.caseno and b.endcasedate is not null and c.surveytype = '1' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"' ) as x "
                                  ;
                    ExeSQL tExeSQL = new ExeSQL();
                    tSSRS = tExeSQL.execSQL(sql);
                    int count = tSSRS.getMaxRow();
                    for (int i = 0; i < count; i++) {
                        String temp[][] = tSSRS.getAllData();

                        String[] strCol;
                        strCol = new String[15];
                        strCol[0] = temp[i][0];
                        strCol[1] = temp[i][1];
                        strCol[2] = temp[i][2];
                        strCol[3] = temp[i][3];
                        strCol[4] = temp[i][4];
                        strCol[5] = temp[i][5];
                        strCol[6] = temp[i][6];
                        strCol[7] = temp[i][7];
                        strCol[8] = temp[i][8];
                        strCol[9] = temp[i][9];
                        strCol[10] = temp[i][10];
                        strCol[11] = temp[i][11];
                        strCol[12] = temp[i][12];
                        strCol[13] = temp[i][13];
                        strCol[14] = temp[i][14];
             System.out.println(temp[i][0]+temp[i][1]+temp[i][2]+temp[i][3]+temp[i][4]);
                        OutHospitalTable.add(strCol);
                    }
                } catch (Exception e) {
                    CError tError = new CError();
                    tError.moduleName = "LLInvestigateTypeBL";
                    tError.functionName = "";
                    tError.errorMessage = "客户基本信息查询失败";
                    this.mErrors.addOneError(tError);
                }

                //院内外调查件信息
                ListTable InOutTable = new ListTable();
                InOutTable.setName("INOUT");
                String[] Title_io = {
                                    "type", "num", "amnt", "occurraito","claimratio", "normaln", "normalm",
                                 "partn","partm","trn","trm","refusen","refusem","xyn","xym"
                };
                try {
                    SSRS tSSRS = new SSRS();
                    String sql =   " SELECT '院内外调查件', CASE WHEN A IS NULL THEN 0 ELSE A END, "
                                  +" CASE WHEN B IS NULL THEN 0 ELSE B END,"
                                  +" CASE WHEN A IS NULL OR C IS NULL OR C=0 THEN 0 ELSE decimal(decimal(A)*100/decimal(C),12,2) END,"
                                  +" CASE WHEN B IS NULL OR GpayR IS NULL OR PpayR IS NULL OR PpayR*GpayR=0 THEN 0 ELSE decimal(decimal(B)*100/decimal(GpayR+PpayR),12,2) END,"
                                  +" CASE WHEN NormalNum IS NULL THEN 0 ELSE NormalNum END,"
                                  +" CASE WHEN NormalPay IS NULL THEN 0 ELSE NormalPay END,"
                                  +" CASE WHEN PartNum IS NULL THEN 0 ELSE PartNum END,"
                                  +" CASE WHEN PartPay IS NULL THEN 0 ELSE PartPay END,"
                                  +" CASE WHEN TRNum IS NULL THEN 0 ELSE TRNum END,"
                                  +" CASE WHEN TRPay IS NULL THEN 0 ELSE TRPay END,"
                                  +" CASE WHEN RefuseNum IS NULL THEN 0 ELSE RefuseNum END,"
                                  +" CASE WHEN RefusePay IS NULL THEN 0 ELSE RefusePay END,"
                                  +" CASE WHEN XYNum IS NULL THEN 0 ELSE XYNum END,"
                                  +" CASE WHEN XYPay IS NULL THEN 0 ELSE XYPay END"
                                  +" FROM (SELECT count(b.caseno) A, sum(a.realpay) B,"
                                  +" (select count(s.caseno) from llcase s where s.endcasedate is not null) C,"
                                  +" (select (1-0.25)*SUM(F.Prem)*ROUND((TO_DATE('"+EndDate+"')-TO_DATE('"+StartDate+"')+1)/365,2) FROM LCPOL F WHERE F.Grpcontno <> '00000000000000000000' and F.AppFlag='1' and F.riskcode <> '1605' and TO_DATE(GETCINVALIDATE(F.POLNO)) >= TO_DATE('"+StartDate+"') AND TO_DATE(F.Cvalidate) <= TO_DATE('"+EndDate+"') ) GpayR,"
                                  +" (select (1-0.35)*SUM(F.Prem)*ROUND((TO_DATE('"+EndDate+"')-TO_DATE('"+StartDate+"')+1)/365,2) FROM LCPOL F WHERE F.Grpcontno = '00000000000000000000'  and F.AppFlag='1' and TO_DATE(GETCINVALIDATE(F.POLNO)) >= TO_DATE('"+StartDate+"') AND TO_DATE(F.Cvalidate) <= TO_DATE('"+EndDate+"') ) PpayR,"
                                  +" (select count(a.caseno) from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '1' and b.endcasedate is not null and c.surveytype = '2' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') NormalNum,"
                                  +" (select sum(a.realpay)  from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '1' and b.endcasedate is not null and c.surveytype = '2' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') NormalPay,"
                                  +" (select count(a.caseno) from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '2' and b.endcasedate is not null and c.surveytype = '2' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') PartNum,"
                                  +" (select sum(a.realpay)  from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '2' and b.endcasedate is not null and c.surveytype = '2' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') PartPay,"
                                  +" (select count(a.caseno) from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '5' and b.endcasedate is not null and c.surveytype = '2' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') TRNum,"
                                  +" (select sum(a.realpay)  from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '5' and b.endcasedate is not null and c.surveytype = '2' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') TRPay,"
                                  +" (select count(a.caseno) from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '3' and b.endcasedate is not null and c.surveytype = '2' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') RefuseNum,"
                                  +" (select sum(a.realpay)  from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '3' and b.endcasedate is not null and c.surveytype = '2' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') RefusePay,"
                                  +" (select count(a.caseno) from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '4' and b.endcasedate is not null and c.surveytype = '2' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') XYNum,"
                                  +" (select sum(a.realpay)  from llclaim a, llcase b, llsurvey c where a.caseno = b.caseno and a.caseno = c.otherno and a.givetype = '4' and b.endcasedate is not null and c.surveytype = '2' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"') XYPay"
                                  +" FROM  llclaim a, llcase b, llsurvey c"
                                  +"  where a.caseno = b.caseno and c.otherno = b.caseno and b.endcasedate is not null and c.surveytype = '2' and b.rgtdate between '"+StartDate+"' and '"+EndDate+"' ) as x "
                                  ;
                    ExeSQL tExeSQL = new ExeSQL();
                    tSSRS = tExeSQL.execSQL(sql);
                    int count = tSSRS.getMaxRow();
                    for (int i = 0; i < count; i++) {
                        String temp[][] = tSSRS.getAllData();

                        String[] strCol;
                        strCol = new String[15];
                        strCol[0] = temp[i][0];
                        strCol[1] = temp[i][1];
                        strCol[2] = temp[i][2];
                        strCol[3] = temp[i][3];
                        strCol[4] = temp[i][4];
                        strCol[5] = temp[i][5];
                        strCol[6] = temp[i][6];
                        strCol[7] = temp[i][7];
                        strCol[8] = temp[i][8];
                        strCol[9] = temp[i][9];
                        strCol[10] = temp[i][10];
                        strCol[11] = temp[i][11];
                        strCol[12] = temp[i][12];
                        strCol[13] = temp[i][13];
                        strCol[14] = temp[i][14];
             System.out.println(temp[i][0]+temp[i][1]+temp[i][2]+temp[i][3]+temp[i][4]);
                        InOutTable.add(strCol);
                    }
                } catch (Exception e) {
                    CError tError = new CError();
                    tError.moduleName = "LLInvestigateTypeBL";
                    tError.functionName = "";
                    tError.errorMessage = "客户基本信息查询失败";
                    this.mErrors.addOneError(tError);
                }


        //其它模版上单独不成块的信息

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LLInvestigateType.vts", "printer"); //最好紧接着就初始化xml文档

        //LLInvestigateTypeBL模版元素

        StartDate = StartDate.replace('-', '月');
        StartDate = StartDate.replaceFirst("月", "年") + "日";
        EndDate = EndDate.replace('-', '月');
        EndDate = EndDate.replaceFirst("月", "年") + "日";
        texttag.add("StartDateN", StartDate);
        texttag.add("EndDateN", EndDate);

        SSRS tSSRS_M = new SSRS();
        String sql_M = "select Name from ldcom where comcode = '" + ManageCom +
                       "'";
        System.out.println(sql_M);
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS_M = tExeSQL.execSQL(sql_M);
        String temp_M[][] = tSSRS_M.getAllData();
        texttag.add("ManageComName", temp_M[0][0]);

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }

        //保存信息

        xmlexport.addListTable(InvestigateTable, Title);
        xmlexport.addListTable(UnInvestiTable, Title_un);
        xmlexport.addListTable(InHospitalTable, Title_i);
        xmlexport.addListTable(OutHospitalTable, Title_o);
        xmlexport.addListTable(InOutTable, Title_io);
        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }


    public static void main(String[] args) {

        LLInvestigateTypeBL tLLInvestigateTypeBL = new LLInvestigateTypeBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        LDPersonSchema tLDPersonSchema = new LDPersonSchema();
        tLDPersonSchema.setCustomerNo("000000142");
        tVData.addElement(tLDPersonSchema);
        tLLInvestigateTypeBL.submitData(tVData, "PRINT");
        VData vdata = tLLInvestigateTypeBL.getResult();
//PubSubmit ps = new PubSubmit();
//if (ps.submitData(vdata, ""))
//{
//    System.out.println("succeed in pubsubmit");
//}

    }

    private void jbInit() throws Exception {
    }

}
