package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;

public class CsuRplPrtBL
    implements PrintService {

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  private VData mResult = new VData();

  //输入的查询sql语句
  private String msql = "";

  //业务处理相关变量
  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();

  private LLConsultSchema mLLConsultSchema = new LLConsultSchema();
  private LLConsultSet mLLConsultSet = new LLConsultSet();
  private LLMainAskSchema mLLMainAskSchema = new LLMainAskSchema(); //咨询通知总表
  private LLAnswerInfoSchema tLLAnswerInfoSchema = new LLAnswerInfoSchema();
  private LLAnswerInfoSet tLLAnswerInfoSet = new LLAnswerInfoSet();
  private LDPersonSchema tLDPersonSchema = new LDPersonSchema();

  public CsuRplPrtBL() {
  }

  /**
   传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    if (!cOperate.equals("PRINT")) {
      buildError("submitData", "不支持的操作字符串");
      return false;
    }

    // 得到外部传入的数据，将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    System.out.println("1 ...");
    mResult.clear();

    // 准备所有要打印的数据
    if (!getPrintData()) {
      return false;
    }
    return true;
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    mLLConsultSchema.setSchema( (LLConsultSchema) cInputData.getObjectByObjectName(
        "LLConsultSchema", 0));
    mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",
        0);
    if (mLLConsultSchema == null) {
      buildError("getInputData", "没有得到足够的信息！");
      return false;
    }

    if (mLLConsultSchema.getConsultNo() == null) {
      buildError("getInputData", "没有得到足够的信息:咨询号不能为空！");
      return false;
    }
    return true;
  }

  public VData getResult() {
    return mResult;
  }

  public CErrors getErrors() {
    return mErrors;
  }

  private void buildError(String szFunc, String szErrMsg) {
    CError cError = new CError();

    cError.moduleName = "LLConsultBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }

  private boolean getPrintData() {
    boolean PEFlag = false; //打印理赔申请回执的判断标志
    boolean shortFlag = false; //打印备注信息
     TextTag texttag = new TextTag(); //新建一个TextTag的实例
    //根据印刷号查询打印队列中的纪录
    String aConsultNo = mLLConsultSchema.getConsultNo();
    String alogNo = "";
    String aCustomerNo = "";

    LLConsultDB mLLConsultDB = new LLConsultDB();
    mLLConsultDB.setConsultNo(aConsultNo);
    //mLLCaseDB.setRgtNo(aRgtNo);

    mLLConsultSet.set(mLLConsultDB.query());
    if (mLLConsultSet == null || mLLConsultSet.size() == 0) {
        PEFlag = false;
    } else {
        mLLConsultSchema.setSchema(mLLConsultSet.get(1));
    }

    alogNo = mLLConsultSchema.getLogNo(); //立案号
    aCustomerNo = mLLConsultSchema.getCustomerNo();

    LLMainAskDB mLLMainAskDB = new LLMainAskDB(); //以下怎么处理
    mLLMainAskDB.setLogNo(alogNo);
    if (mLLMainAskDB.getInfo() == false) {
      mErrors.copyAllErrors(mLLMainAskDB.mErrors);
      buildError("outputXML", "mLLMainAskDB在取得打印队列中数据时发生错误");
      return false;
    }
    mLLMainAskSchema = mLLMainAskDB.getSchema();

    LDPersonDB mLDPersonDB = new LDPersonDB();
    mLDPersonDB.setCustomerNo(aCustomerNo);
    if (mLDPersonDB.getInfo() == false) {
      mErrors.copyAllErrors(mLDPersonDB.mErrors);
      buildError("outputXML", "mLDPersonDB在取得打印队列中数据时发生错误");
      return false;
    }
    tLDPersonSchema.setSchema(mLDPersonDB.getSchema());

    LLAnswerInfoDB mLLAnswerInfoDB = new LLAnswerInfoDB();
    mLLAnswerInfoDB.setConsultNo(aConsultNo);
    mLLAnswerInfoDB.setLogNo(alogNo);
    tLLAnswerInfoSet.set(mLLAnswerInfoDB.query());
    if (tLLAnswerInfoSet == null || tLLAnswerInfoSet.size() == 0) {
        PEFlag = false;
    } else {
        tLLAnswerInfoSchema.setSchema(tLLAnswerInfoSet.get(1));
    }


    //其它模版上单独不成块的信息

    XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
    xmlexport.createDocument("CsuRplPrt.vts", "printer"); //最好紧接着就初始化xml文档
    //生成-年-月-日格式的日期
    StrTool tSrtTool = new StrTool();
    String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
        tSrtTool.getDay() + "日";

    //PayAppPrtBL模版元素
    //texttag.add("BarCodeParam1","");
    String sql = "select codename from LDCode where CodeType='llaskmode' and Code='" +
                 mLLMainAskSchema.getAskMode() + "' ";
    ExeSQL execsql = new ExeSQL();
    String AskMode = execsql.getOneValue(sql);
    sql = "select codename from LDCode where CodeType='llcuststatus' and Code='" +
                mLLConsultSchema.getCustStatus()+ "' ";
    String CustState = execsql.getOneValue(sql);
    String strSQL = "select Username from lduser where usercode='" + mGlobalInput.Operator + "'";
    ExeSQL exesql1 = new ExeSQL();
    String OperName = exesql1.getOneValue(strSQL);

    texttag.add("BarCode1", mLLConsultSchema.getConsultNo());
    texttag.add("BarCodeParam1"
                , "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
    texttag.add("BarCode1", mLLConsultSchema.getConsultNo());
    texttag.add("BarCodeParam1"
                , "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
    texttag.add("CustomerName", mLLConsultSchema.getCustomerName());
    texttag.add("CustomerNo",mLLConsultSchema.getCustomerNo());
    texttag.add("IDNo", tLDPersonSchema.getIDNo());
    texttag.add("Phone", mLLMainAskSchema.getPhone());
    texttag.add("Mobile", mLLMainAskSchema.getMobile());
    texttag.add("Email", mLLMainAskSchema.getEmail());
    texttag.add("PostalAddress", mLLMainAskSchema.getAskAddress());
    texttag.add("ZipCode", mLLMainAskSchema.getPostCode());
    texttag.add("LogComp", mLLMainAskSchema.getLogComp());
    texttag.add("AskMode", AskMode);
    texttag.add("AskType", (mLLMainAskSchema.getAskType()=="0"?"咨询":"通知"));
    texttag.add("CustState", CustState);
    texttag.add("CContent", mLLConsultSchema.getCContent());
    texttag.add("Answer", tLLAnswerInfoSchema.getAnswer());
    texttag.add("Operator", OperName);
    texttag.add("SysDate", SysDate);
    String t_AddSQL="select LetterServicePostAddress,LetterServiceName from ldcom where comcode='86' with ur";
    ExeSQL exesql = new ExeSQL();
    SSRS tssrs = exesql.execSQL(t_AddSQL);
    System.out.println(tssrs.GetText(1,2)+tssrs.GetText(1,1));
    texttag.add("LetterServiceName", tssrs.GetText(1,2));
    texttag.add("Address", tssrs.GetText(1,1));
    if (shortFlag) {
      texttag.add("Supply1", "1、根据保险合同的约定，您所提供的资料不齐，请根据上述提示补齐资料后再行申请；");
      texttag.add("Supply2",
          "2、本公司审核过程中，如发现您所提交的材料不足以证明保险事故的原因、性质或者程度，本公司将另行发出材料补齐通知，待您补齐相关材料之后再行理赔审核。");
    }
    else {
      texttag.add("Supply1",
          "本公司审核过程中，如发现您所提交的材料不足以证明保险事故的原因、性质或者程度，本公司将另行发出材料补齐通知，待您补齐相关材料之后再行理赔审核。");
      texttag.add("Supply2", "");
    }
    ////////////
    if (texttag.size() > 0) {
      xmlexport.addTextTag(texttag);
    }
    //保存体检信息
    xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
    mResult.clear();
    mResult.addElement(xmlexport);

    return true;
  }

  public static void main(String[] args) {

    CsuRplPrtBL tCsuRplPrtBL = new CsuRplPrtBL();
    VData tVData = new VData();
    GlobalInput tGlobalInput = new GlobalInput();
    tGlobalInput.ManageCom = "86";
    tGlobalInput.Operator = "001";
    tVData.addElement(tGlobalInput);
    LLConsultSchema tLLConsultSchema = new LLConsultSchema();
    tLLConsultSchema.setConsultNo("Z0000050511000001");
    tVData.addElement(tLLConsultSchema);
    tCsuRplPrtBL.submitData(tVData, "PRINT");
    VData vdata = tCsuRplPrtBL.getResult();
//PubSubmit ps = new PubSubmit();
//if (ps.submitData(vdata, ""))
//{
//    System.out.println("succeed in pubsubmit");
//}

  }

}
