package com.sinosoft.lis.f1print;

import java.io.*;
import java.util.*;

import com.sinosoft.utility.*;

import java.util.regex.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Readhtml
{
    private String readfilename = ""; // 读文件的地址

    private String writfilename = ""; // 写文件的地址

    private int[] hanglei = new int[2];// 单个变量 行列信息

    private String[][] singarray = null;// 单个变量信息

    private String[][] forarray = null;// 单个列表的内容信息

    private String ErrorMession = ""; // 错误信息

    private SSRS singssrs = null; // 单个变量内容 ssrs

    private SSRS forssrs = null; // 单个列表内容ssrs

    private int index = 0; // 单个变量 读到第几个信息 ssrs

    private Vector myvector = null; // 单个变量内容

    private int ListTableSum = 0; // 总共有几个列表

    private int NowListTableIndex = 0; // 当前是第几个列表

    private List MuchListTableInfo = null;// 每个列表的内容对应的类名

    private List MuchListTable = null; // 每个列表的内容,内可含有ssrs,String[][],listtable等

    private List ForListByCol = null;// 按列的形式给列表赋值;

    private int ci = 0;

    private ListTable listtable = null;

    private String TrTemp = ""; // 记录每个<tr>的行

    private String falge = "";

    private List SingListTableList = null; // 除了单个变量的其余的类似单个变量，内放ListTable，如放计划，子女，

    private HashMap inHashmap = null;

    private int inHashmapSum = 0;

    private  String gTempLateName="";//xml解析出的模板名称
    
	private List gListTablelist=new ArrayList();//xml解析出的Listtable名称
	
	private TextTag gTextTag=new TextTag();//xml解析出的texttag名称
	
    private boolean PraseMuch()
    {
        boolean bflag = false;

        try
        {
            File f = new File(this.readfilename);

            InputStreamReader read = new InputStreamReader(new FileInputStream(f));

            BufferedReader reader = new BufferedReader(read);

            String line;

            FileWriter fw = new FileWriter(this.writfilename);

            int flag = 0;
            int jxFlag = 0;
            List list = new ArrayList();

            line = reader.readLine();

            do
            {
                if ((line.indexOf("<tr")) > 0)
                {
                    this.TrTemp = line;
                }
                if ((line.indexOf("&amp;&amp;2")) == -1 && (line.indexOf("%%2")) == -1 && (line.indexOf("#")) == -1)// 多表的第一种情况，不存在列表开始标识
                {
                    fw.write(line);
                    fw.write("\r\n");
                    line = reader.readLine();
                    continue;
                }
                if ((line.indexOf("#")) > 0 && (line.indexOf("%%2")) == -1 && (line.indexOf("&amp;&amp;2")) == -1)// 第二种情况，单个变量
                {
                    SingReplace(line, fw);
                   
                    line = reader.readLine();
                    continue;
                }

                if ((line.indexOf("&amp;&amp;2")) > 0 && (line.indexOf("#")) > 0)// 多表的第三种情况，存在列表开始标识，并判断是不要解析
                {
                    // 判断两#间的值是否存在
                    int disFlag = 0;
                    String display[] = null;
                    display = line.split("#");

                    line = reader.readLine();
                    if (this.myvector != null)
                    {
                        String[] temparray = new String[2];
                        int size = this.myvector.size();
                        for (int in = 0; in < size; in++)
                        {
                            temparray = (String[]) myvector.get(in);
                           
                            if (display[1].equals(temparray[0]))
                            {
                                disFlag = 1;
                                break;
                            }
                        }
                    }
                   
                    if (disFlag == 1)
                    {

                        jxFlag = 1;

                        while (line.indexOf("%%2") == -1)
                        {

                            // 解析
                            // 解析需要解析的hancw
                            if ((line.indexOf("<tr")) > 0)
                            {
                                this.TrTemp = line;
                            }
                           
                            if (flag == 0 && (line.indexOf("&amp;&amp;1")) == -1 && (line.indexOf("%%1")) == -1
                                    && (line.indexOf("#")) == -1)// 第一种情况
                            {

                                fw.write(line);
                                fw.write("\r\n");
                                line = reader.readLine();
                                continue;
                               
                            }

                            if (flag == 0 && (line.indexOf("&amp;&amp;1")) > 0 && (line.indexOf("%%1")) == -1)// 第二种情况
                            // 循环开始
                            {
                                flag = 1;
                                line = line.replaceAll("&amp;&amp;1", "");
                                list.add(TrTemp);
                                list.add(line);
                                line = reader.readLine();
                                continue;
                               
                            }

                            if (flag == 1 && (line.indexOf("&amp;&amp;1")) == -1 && (line.indexOf("%%1")) == -1) // 第三种情况
                            // 循环中
                            {
                                if (line.indexOf("#") > 0)
                                {
                                   
                                    list.add(line);
                                    line = reader.readLine();
                                    continue;

                                }
                                else
                                {
                                   
                                    fw.write(line);
                                    fw.write("\r\n");
                                    line = reader.readLine();
                                    continue;
                                }
                            }

                            if (flag == 1 && (line.indexOf("%%1") > 0 && (line.indexOf("&amp;&amp;1")) == -1)) // 第四种情况
                            // 循环结束
                            {

                                flag = 0;
                                line = line.replaceAll("%%1", "");
                                list.add(line);
                                line = reader.readLine();
                                while (line.indexOf("%%2") == -1)
                                {
                                    if ((line.indexOf("</tr>")) > 0)
                                    {
                                        list.add(line);
                                        break;
                                    }
                                    else
                                    {
                                        list.add(line);

                                    }
                                    line = reader.readLine();
                                }
                              
                               
                                ListTable inListTable = (ListTable) this.inHashmap.get(display[1]);
                                this.setMuchLineByListTable(inListTable);

                                forwrite(list, fw);
                                list.clear();
                                line = reader.readLine();
                                continue;

                            }

                            if (flag == 0 && (line.indexOf("#")) > 0 && (line.indexOf("%%1")) == -1
                                    && (line.indexOf("&amp;&amp;1")) == -1)// 第五种情况
                            // 单个变量
                            {
                                SingReplace(line, fw);
                              
                                line = reader.readLine();
                                continue;
                            }
                        }
                        if (line.indexOf("%%2") > 0)
                        {
                            // 当前行为“%%2”则换行
                            line = reader.readLine();
                            continue;
                        }
                    }

                    else
                    {
                        do
                        {
                            line = reader.readLine();
                            continue;
                        }
                        while ((line.indexOf("%%2")) == -1);

                        // 当读完“%%2”换行
                        line = reader.readLine();
                        continue;
                    }

                }

               

            }
            while (line != null);
            read.close();
            reader.close();
            fw.close();
            bflag = true;
        }
        catch (Exception e)
        {
            ci = 1;
            e.printStackTrace();
            this.ErrorMession = e.getMessage();

        }

        return bflag;
    }

    private boolean Prase()
    {
        boolean bflag = false;

        try
        {
            File f = new File(this.readfilename);

            InputStreamReader read = new InputStreamReader(new FileInputStream(f));

            BufferedReader reader = new BufferedReader(read);

            String line;

            FileWriter fw = new FileWriter(this.writfilename);

            int flag = 0;
            int jxFlag = 0;
            List list = new ArrayList();

            line = reader.readLine();

            do
            {
                if ((line.indexOf("<tr")) > 0)
                {
                    this.TrTemp = line;
                }

               
                if (flag == 0 && (line.indexOf("&amp;&amp;1")) == -1 && (line.indexOf("%%1")) == -1
                        && (line.indexOf("#")) == -1)// 第一种情况
                {

                    fw.write(line);
                    fw.write("\r\n");
                    line = reader.readLine();
                    continue;
                   
                }

                if (flag == 0 && (line.indexOf("&amp;&amp;1")) > 0 && (line.indexOf("%%1")) == -1)// 第二种情况
                // 循环开始
                {
                    flag = 1;
                    line = line.replaceAll("&amp;&amp;1", "");
                    list.add(TrTemp);
                    list.add(line);
                    line = reader.readLine();
                    continue;
                   
                }

                if (flag == 1 && (line.indexOf("&amp;&amp;1")) == -1 && (line.indexOf("%%1")) == -1) // 第三种情况
                // 循环中
                {
                    if (line.indexOf("#") > 0)
                    {
                       
                        list.add(line);
                        line = reader.readLine();
                        continue;

                    }
                    else
                    {
                       
                        fw.write(line);
                        fw.write("\r\n");
                        line = reader.readLine();
                        continue;
                    }
                }

                if (flag == 1 && (line.indexOf("%%1") > 0 && (line.indexOf("&amp;&amp;1")) == -1)) // 第四种情况
                // 循环结束
                {

                    flag = 0;
                    line = line.replaceAll("%%1", "");
                    list.add(line);
                    while ((line = reader.readLine()) != null)
                    {
                        if ((line.indexOf("</tr>")) > 0)
                        {
                            list.add(line);
                            break;
                        }
                        else
                        {
                            list.add(line);

                        }
                    }
                 
                    if (this.ListTableSum > 1) // 说明是多个列表
                    {
                       
                        String classname = (String) this.MuchListTableInfo.get(this.NowListTableIndex);
                      
                        if (classname.toLowerCase().equals("ssrs"))
                        {
                            this.forssrs = (SSRS) this.MuchListTable.get(this.NowListTableIndex);
                            this.NowListTableIndex++;
                            forwrite(list, fw);
                        }
                        if (classname.toLowerCase().equals("string[][]"))
                        {
                            this.forarray = (String[][]) this.MuchListTable.get(this.NowListTableIndex);
                            this.NowListTableIndex++;
                            forwrite(list, fw);
                        }
                        if (classname.toLowerCase().equals("list"))
                        {
                            this.ForListByCol = (List) this.MuchListTable.get(this.NowListTableIndex);
                            this.NowListTableIndex++;
                            forwrite(list, fw);
                        }
                        if (classname.toLowerCase().equals("listtable"))
                        {
                           
                            this.listtable = (ListTable) this.MuchListTable.get(this.NowListTableIndex);
                            this.NowListTableIndex++;
                           
                            forwrite(list, fw);
                        }

                       
                    }
                    else
                    // 说明是一个列表
                    {
                        forwrite(list, fw);
                      
                    }
                    line = reader.readLine();
                    continue;
                }

                if (flag == 0 && (line.indexOf("#")) > 0 && (line.indexOf("%%1")) == -1
                        && (line.indexOf("&amp;&amp;1")) == -1)// 第五种情况 单个变量
                {
                    SingReplace(line, fw);
                  
                    line = reader.readLine();
                    continue;
                }

                

            }
            while (line != null);
            read.close();
            reader.close();
            fw.close();
            bflag = true;
        }
        catch (Exception e)
        {
            ci = 1;
            e.printStackTrace();
            this.ErrorMession = e.getMessage();

        }

        return bflag;
    }

    private void SingReplace(String line, FileWriter fw) throws IOException
    {
        // TODO 自动生成方法存根
        if (this.singarray != null && (line.indexOf("%")) < 0)
        {
            int singhang = this.hanglei[0];
           

            for (int i = 0; i < singhang; i++)
            {
                if ((line.indexOf(this.singarray[i][0])) > 0)
                {
                    line = line.replaceAll(this.singarray[i][0], this.singarray[i][1]);
                 
                    fw.write(line);
                    fw.write("\r\n");
                    break;
                }
            }
        }

        if (this.singssrs != null && (line.indexOf("%")) < 0)
        {
            index++;
            if (index <= this.singssrs.getMaxCol())
            {
                line = line.replaceAll("###", this.singssrs.GetText(1, index));
                fw.write(line);
                fw.write("\r\n");
            }
            else
            {
               
            }
        }

        if (this.myvector != null && (line.indexOf("%")) < 0)
        {
            String[] temparray = new String[2];
            int size = this.myvector.size();
           

            for (int in = 0; in < size; in++)
            {
                
                temparray = (String[]) myvector.get(in);
               

                if ((line.indexOf(temparray[0])) > 0)
                {
                    line = line.replaceAll(temparray[0], temparray[1]);// 变量值替换变量名
                }
                
            }
            line = line.replaceAll("#", "");
            fw.write(line);
            fw.write("\r\n");
        }

        if (this.SingListTableList != null && (line.indexOf("%")) > 0)
        {
            line = line.replaceAll("#", "");
            for (int i = 0; i < this.SingListTableList.size(); i++)
            {
                ListTable templisttable = (ListTable) this.SingListTableList.get(i);
                String name = templisttable.getName();
             
                if ((line.indexOf(name)) > 0)
                {
                    String[] temparray = templisttable.get(0);
                    Pattern pattern = Pattern.compile("%.*");
                    Matcher matcher = pattern.matcher(line);
                    StringBuffer buffer = new StringBuffer();
                    String templine = "";
                    if (matcher.find())
                    {
                        buffer.append(matcher.group());
                        templine = buffer.toString();
                    }
                  
                    templine = templine.replaceAll("%", "");
                    templine = templine.replaceAll("#", "");
                    templine = templine.replaceAll("</td>", "");
                    templine = templine.replaceAll("</font>", "");
                    int linecol = 0;
                    try
                    {
                        linecol = Integer.parseInt(templine) - 1;
                    }
                    catch (Exception ex)
                    {
                       
                        ex.printStackTrace();
                    }
                   
                    String value = "";
                    if (linecol >= temparray.length)
                    {
                        value = "";
                    }
                    else
                    {
                        if (temparray[linecol] == null)
                        {
                            value = "";
                        }
                        else
                        {
                            value = temparray[linecol];
                        }
                    }
                    String replacestr = name + "%" + templine;
               
                    line = line.replaceAll(replacestr, value);
                    fw.write(line);
                    fw.write("\r\n");
                    break;
                }

            }
        }

    }

    private void forwrite(List tdlist, FileWriter fw) throws IOException
    {
        int imtr = 0;
        int imtd = 0;

        if (this.forarray != null) // 数组类型的
        {
            int forhang = this.forarray.length;
            int forlei = tdlist.size();
            for (int i = 0; i < forhang; i++)
            {
                for (int j = 0; j < forlei; j++)
                {

                    String line = (String) tdlist.get(j);
                    if ((line.indexOf("##")) > 0)
                    {
                        line = line.replaceAll("##", this.forarray[i][j - 1]);
                        fw.write(line);
                        fw.write("\r\n");
                    }
                    else
                    {
                        if (imtr == 0)
                        {
                            imtr++;

                        }
                        else
                        {
                            if ((line.indexOf("</tr")) > 0)
                            {
                                if (imtd < forhang)
                                {
                                    imtd++;
                                    fw.write(line);
                                    fw.write("\r\n");
                                }
                            }
                            fw.write(line);
                            fw.write("\r\n");
                        }
                    }

                }
            }
            this.forarray = null;
        }
        if (this.forssrs != null) // ssrs类型的
        {
            int forhang = this.forssrs.getMaxRow();
            int forcol = tdlist.size();
            for (int i = 1; i <= forhang; i++)
            {
                for (int j = 0; j < forcol; j++)
                {
                    String line = (String) tdlist.get(j);
                    if ((line.indexOf("##")) > 0)
                    {
                        line = line.replaceAll("##", this.forssrs.GetText(i, j));
                        fw.write(line);
                        fw.write("\r\n");
                    }
                    else
                    {
                        if (imtr == 0)
                        {
                            imtr++;
                        }
                        else
                        {
                            if ((line.indexOf("</tr")) > 0)
                            {
                                if (imtd != forhang)
                                {
                                    fw.write(line);
                                    fw.write("\r\n");
                                }
                            }
                            fw.write(line);
                            fw.write("\r\n");
                        }
                    }
                }
            }
            this.forssrs = null;
        }

        if (this.ForListByCol != null)
        {
            int maxcol = this.ForListByCol.size();
            String temp[] = (String[]) this.ForListByCol.get(0);
            int maxrow = temp.length;
            for (int row = 0; row < maxrow; row++)
            {
                for (int col = 0; col < maxcol; col++)
                {
                    String line = (String) tdlist.get(col);
                    if ((line.indexOf("##")) > 0)
                    {
                        String[] ColInfo = (String[]) this.ForListByCol.get(col);
                        line = line.replaceAll("##", ColInfo[row]);
                        fw.write(line);
                        fw.write("\r\n");
                    }
                    else
                    {
                        if (imtr == 0)
                        {
                            imtr++;
                        }
                        else
                        {
                            if ((line.indexOf("</tr")) > 0)
                            {
                                if (imtd != maxrow)
                                {
                                    fw.write(line);
                                    fw.write("\r\n");
                                }
                            }
                            fw.write(line);
                            fw.write("\r\n");
                        }
                    }
                }
            }
            this.ForListByCol = null;
        }

        if (this.listtable != null)
        {
            
            int maxrow = listtable.size();
            String listtabelname = listtable.getName();
            int length = tdlist.size();
           
            for (int row = 0; row < maxrow; row++)
            {
                String[] values = (String[]) listtable.get(row);
                for (int col = 0; col < length; col++)
                {
                    String templine = "";
                    String line = (String) tdlist.get(col);
                    String value = "";
                    if ((line.indexOf("#%")) > 0)
                    {
                        if ((line.indexOf(listtabelname)) > 0)
                        {
                            Pattern pattern = Pattern.compile("#%.*");
                            Matcher matcher = pattern.matcher(line);
                            StringBuffer buffer = new StringBuffer();
                            if (matcher.find())
                            {
                                buffer.append(matcher.group());
                                templine = buffer.toString();
                            }
                            templine = templine.replaceAll("#%", "");
                            templine = templine.replaceAll("</td>", "");
                            templine = templine.replaceAll("</font>", "");

                            int linecol = Integer.parseInt(templine) - 1;
                           

                            int len = values.length;
                         
                            if (linecol >= len)
                            {
                                value = "";
                            }

                            else
                            {
                                if (values[linecol] == null)
                                {
                                    value = "";
                                }
                                else
                                {
                                    value = values[linecol];
                                }
                            }
                            String repalcestr = listtabelname + "#%" + templine;
                            line = line.replaceAll(repalcestr, value);
                            fw.write(line);
                            fw.write("\r\n");
                        }
                    }
                    else
                    {
                        if (imtr == 0)
                        {
                            imtr++;
                        }
                        else
                        {
                            
                            fw.write(line);
                            fw.write("\r\n");
                           
                        }

                    }
                }
            }

        }
        this.listtable = null;
    }

    private boolean PraseByVts()
    {
        boolean bflag = false;

        try
        {
            File f = new File(this.readfilename);

            InputStreamReader read = new InputStreamReader(new FileInputStream(f));

            BufferedReader reader = new BufferedReader(read);

            String line;

            FileWriter fw = new FileWriter(this.writfilename);

            int flag = 0;
            List list = new ArrayList();

            line = reader.readLine();
            do
            {

                if ((line.indexOf("<tr")) > 0)
                {
                    this.TrTemp = line;
                }

                if (flag == 0 && (line.indexOf("&amp;&amp;1")) == -1 && (line.indexOf("%%1")) == -1
                        && (line.indexOf("$")) == -1)// 第一种情况 vts
                {
                    
                    fw.write(line);
                    fw.write("\r\n");
                  
                    line = reader.readLine();
                    continue;
                }

                if (flag == 0 && (line.indexOf("&amp;&amp;1")) > 0 && (line.indexOf("%%1")) == -1)// 第二种情况
                // 循环开始
                // vts
                {
                    list.add(this.TrTemp);
                    flag = 1;
                    line = line.replaceAll("&amp;&amp;1", "");
                    line = line.replaceAll("\\$", "");// 去掉$
                    line = line.replaceAll("\\*", "");// 去掉*
                    line = line.replaceFirst("/", "");
                  
                    list.add(line);
                    line = reader.readLine();
                    continue;
                  
                }

                if (flag == 1 && (line.indexOf("&amp;&amp;1")) == -1 && (line.indexOf("%%1")) == -1) // 第三种情况
                // 循环进行
                // vts
                {
                    if (line.indexOf("$*") > 0)
                    {
                       
                        line = line.replaceAll("\\$", "");
                        line = line.replaceFirst("\\*", "");
                        line = line.replaceFirst("/", "");
                        list.add(line);

                    }
                    else
                    {
                      
                        fw.write(line);
                        fw.write("\r\n");
                    }
                    line = reader.readLine();
                    continue;
                }

                if (flag == 1 && (line.indexOf("%%1") > 0 && (line.indexOf("&amp;&amp;1")) == -1)) // 第四种情况
                // 循环结束 vts
                {

                    flag = 0;
                    line = line.replaceAll("%%1", "");
                    line = line.replaceAll("\\$", "");
                    line = line.replaceFirst("\\*", "");
                    line = line.replaceFirst("/", "");
                    list.add(line);
                    list.add("</tr>");
                    if (this.MuchListTable != null) // 说明是多个列表
                    {
                        int size = this.MuchListTable.size();
                        for (int i = 0; i < size; i++)
                        {
                            this.listtable = (ListTable) this.MuchListTable.get(i);
                            String name = this.listtable.getName();
                            if ((line.indexOf(name)) > 0)
                            {

                                forwritebyvts(list, fw);
                                break;
                            }
                        }

                    }
                    else
                    // 说明是一个列表
                    {
                        forwritebyvts(list, fw);

                    }
                    line = reader.readLine();
                    continue;
                }

                if (flag == 0 && (line.indexOf("$")) > 0 && (line.indexOf("%%1")) == -1
                        && (line.indexOf("&amp;&amp;1")) == -1)// 第五种情况 单个变量
                // vts
                {
                    line = line.replaceAll("\\$=/", "");
                   
                    line = line.replaceAll("\\$", "");
                   

                    SingReplaceByVTS(line, fw);

                   
                    line = reader.readLine();
                    continue;
                }

            }
            while (line != null);
            read.close();
            reader.close();
            fw.close();
            bflag = true;
        }
        catch (Exception e)
        {
            ci = 1;
            e.printStackTrace();
            this.ErrorMession = e.getMessage();

        }

        return false;
    }

    //新增处理存在显示区域控制的情况,并对一些细节进行改进了
    private boolean PraseByVtsMuch()
    {
        boolean bflag = false;

        try
        {
            File f = new File(this.readfilename);

            InputStreamReader read = new InputStreamReader(new FileInputStream(f));

            BufferedReader reader = new BufferedReader(read);

            String line;

            FileWriter fw = new FileWriter(this.writfilename);

            List list = new ArrayList();
            List colList = new ArrayList();
            
            int mFlag = 0; //1代表该line行在<tr>和</tr>之间，0代表不在；

            line = reader.readLine();
            do
            {
                //采用list里记录整行标记的处理方式,改进了涉及循环变量的行的html标记输出不完整的问题
//              if ((line.indexOf("<tr")) > 0)
//              {
//                  this.TrTemp = line;
//              }

                if ( (line.indexOf("$&lt;/")) > 0 )//第七种情况 存在显示区域控制变量的开始标识
                {
                    int disFlag = 0;  //1代表该显示区域控制变量对应的控制区域要显示,0代表不显示
                    //当下面的"line = reader.readLine();"行被注释后,display变量存在的意义已经没有了
//                    String display = "";
//                    line = line.replaceAll("\\$&lt;/", "");
//                    display = line;

                    //忽略"$</"行多余的<tr>标记
                    list.clear();
                    //之所以注释下面一行是为了保证当存在显示区域控制变量的行的html标记里的<tr>和</tr>之间
                    //只有一行<td></td>标记(即显示区域控制变量所在单元格)的情况时,本方法依然能够正确处理
                    //line = reader.readLine();

                    //准确定位单个变量或者显示区域变量名
            		String display = "";
                    //取出$及其以后的字符串
    				Pattern pattern = Pattern.compile("\\$.*");
                    Matcher matcher = pattern.matcher(line);
                    StringBuffer buffer = new StringBuffer();
                    String templine = "";
                    if (matcher.find())
                    {
                        buffer.append(matcher.group());
                        templine = buffer.toString();
                    }
                    //得到循环变量名
                    templine = templine.replaceFirst("\\$&lt;/", "");
                    templine = templine.replaceAll("</td>", "");
                    display = templine;

                    //判断显示区域控制变量控制的区域是不是要显示
                    if (this.myvector != null)
                    {
                        String[] tempArray = new String[2];
                        int size = this.myvector.size();
                        for (int in = 0; in < size; in++)
                        {
                            tempArray = (String[]) myvector.get(in);

                            //处理单个变量或者显示区域变量名的子集问题
                            //if (line.indexOf(tempArray[0]) > 0)
                            if ( display.equals(tempArray[0]) ) //此处直接用display[1],是未考虑一行存在多个显示区域变量的情况.
                            {
                                disFlag = 1;
                                break;
                            }
                        }
                    }

                    //对要显示的控制区域进行相应的处理
                    if (disFlag == 1)
                    {

                        //忽略"$</"行多余的</tr><td></td>标记
                    	do
                        {
                            line = reader.readLine();
                            //continue;
                        }
                        while ((line.indexOf("</tr>")) == -1);
                    	if (line.indexOf("</tr>") > 0)
                        {
                            line = reader.readLine();
                        }
                    	//真正开始处理显示的控制区域,也就是在处理一行html标记
                    	while (line.indexOf("$&gt;") == -1)
                        {

                        	//开始处理一行html标记,以<tr开头的line行
                    		if ( (line.indexOf("<tr")) > 0 )
                            {

                        		mFlag = 1;
                        		list.add(line);

                            	line = reader.readLine();
                            	continue;
                            }
                            //处理一行html标记里<tr>和</tr>之间的line行
                        	else if ( mFlag == 1 && (line.indexOf("</tr>")) == -1 )
                        	{
                        		list.add(line);

                            	line = reader.readLine();
                            	continue;
                        	}
                    		//一行html标记结束,内容为</tr>的line行
                        	else if ( mFlag == 1 && (line.indexOf("</tr>")) > 0 )
                            {
                        		mFlag = 0;
                        		//list里得到一行完整的html标记
                        		list.add(line);
                        		int colFlag = 0;  //1代表该行是循环变量所在行,0代表不是
                                int flag = 0;
                        		int maxcol = list.size();
                                String colLine;
                        		for (int col = 0; col < maxcol; col++)
                                {
                        			colLine = (String) list.get(col);
                        			if ((colLine.indexOf("&amp;&amp;1")) > 0)
                                    {
                                    	colFlag = 1;
                                    	break;
                                    }
                                }
                        		//处理循环变量所在行
                        		if(colFlag == 1)
                        		{

                        			String tempColLine = "";
                        			for (int col = 0; col < maxcol; col++)
                                    {
                                        colLine = (String) list.get(col);
                                        if (flag == 0 && (colLine.indexOf("&amp;&amp;1")) == -1 && (colLine.indexOf("%%1")) == -1
                                                && (colLine.indexOf("$")) == -1)// 第一种情况 vts
                                        {

                                        	colList.add(colLine);

                                            continue;
                                        }

                                        if (flag == 0 && (colLine.indexOf("&amp;&amp;1")) > 0 && (colLine.indexOf("%%1")) > 0)// 第六种情况 单列循环变量
                                        // vts
                                        {

                                        	flag = 1;
                                        	colLine = colLine.replaceAll("&amp;&amp;1", "");

                                        }

                                        if (flag == 0 && (colLine.indexOf("&amp;&amp;1")) > 0 && (colLine.indexOf("%%1")) == -1)// 第二种情况
                                        // 循环开始
                                        // vts
                                        {

                                        	flag = 1;
                                        	colLine = colLine.replaceAll("&amp;&amp;1", "");
                                            colLine = colLine.replaceAll("\\$", "");// 去掉$
                                            colLine = colLine.replaceAll("\\*", "");// 去掉*
                                            colLine = colLine.replaceFirst("/", "");

                                            colList.add(colLine);

                                            continue;

                                        }

                                        if (flag == 1 && (colLine.indexOf("&amp;&amp;1")) == -1 && (colLine.indexOf("%%1")) == -1) // 第三种情况
                                        // 循环进行
                                        // vts
                                        {

                                        	colLine = colLine.replaceAll("\\$", "");
                                            colLine = colLine.replaceFirst("\\*", "");
                                            colLine = colLine.replaceFirst("/", "");

                                            colList.add(colLine);

                                            continue;
                                        }

                                        if (flag == 1 && (colLine.indexOf("%%1") > 0 && (colLine.indexOf("&amp;&amp;1")) == -1)) // 第四种情况
                                        // 循环结束 vts
                                        {

                                            flag = 0;
                                            tempColLine = colLine;
                                            colLine = colLine.replaceAll("%%1", "");
                                            colLine = colLine.replaceAll("\\$", "");
                                            colLine = colLine.replaceFirst("\\*", "");
                                            colLine = colLine.replaceFirst("/", "");
                                            colList.add(colLine);

                                            continue;
                                        }

                                        if (flag == 0 && (colLine.indexOf("$=/")) > 0 && (colLine.indexOf("%%1")) == -1
                                                && (colLine.indexOf("&amp;&amp;1")) == -1)// 第五种情况 单个变量
                                        // vts
                                        {
                                        	//colLine = colLine.replaceAll("\\$=/", "");
                                        	colLine = colLine.replaceAll("=/", "");

                                            //colLine = colLine.replaceAll("\\$", "");

                                            colLine = SingReplaceByVTSMuch(colLine, fw);
                                            
                                            colList.add(colLine);

                                            continue;
                                        }
                                    }
                        			//现在一般只用的到if的情况,else已经不走了。
                        			if (this.MuchListTable != null) // 说明是多个列表
                                    {
                                        //取出%%1及其以后的字符串
                        				Pattern pattern1 = Pattern.compile("%%1.*");
                                        Matcher matcher1 = pattern1.matcher(tempColLine);
                                        StringBuffer buffer1 = new StringBuffer();
                                        String templine1 = "";
                                        if (matcher1.find())
                                        {
                                            buffer1.append(matcher1.group());
                                            templine1 = buffer1.toString();
                                        }
                                        //取出/ROW/COL及其以后的字符串
                        				Pattern pattern2 = Pattern.compile("/ROW/COL.*");
                                        Matcher matcher2 = pattern2.matcher(tempColLine);
                                        StringBuffer buffer2 = new StringBuffer();
                                        String templine2 = "";
                                        if (matcher2.find())
                                        {
                                            buffer2.append(matcher2.group());
                                            templine2 = buffer2.toString();
                                        }
                                        //得到循环变量名
                                        templine1 = templine1.replaceAll("%%1", "");
                                        templine1 = templine1.replaceAll("\\$", "");
                                        templine1 = templine1.replaceFirst("\\*", "");
                                        templine1 = templine1.replaceFirst("/", "");
                                        templine1 = templine1.replaceAll(templine2, "");
                                        tempColLine = templine1;

                                        int nameFlag = 0;  //0代表该循环变量在BL类里没有被赋值,1代表赋值了.
                                        int size = this.MuchListTable.size();
                                        for (int i = 0; i < size; i++)
                                        {
                                            this.listtable = (ListTable) this.MuchListTable.get(i);
                                            String name = this.listtable.getName();
                                            //处理循环变量名的子集问题.
                                            //if ((tempColLine.indexOf(name)) > 0)
                                            if ( tempColLine.equals(name) )
                                            {

                                            	nameFlag = 1;
                                            	forwritebyvtsmuch(colList, fw);
                                                colList.clear();
                                                //因为一行里只会存在一个循环变量.
                                                break;
                                            }
                                        }
                                        if( nameFlag == 0 )
                                        {
                                        	forwritebyvtsmuchnull(colList, fw, tempColLine);  //处理没有被赋值的循环变量
                                        	colList.clear();
                                        }
                                    }
                                    else
                                    // 说明是一个列表
                                    {
                                        forwritebyvtsmuch(colList, fw);

                                    }
                        		}
                        		//处理循环变量以外的行
                        		else
                        		{
                        			for (int col = 0; col < maxcol; col++)
                                    {
                                        colLine = (String) list.get(col);
                                        if ( (colLine.indexOf("$")) == -1 )// 第一种情况 vts
                                        {

                                            fw.write(colLine);
                                            fw.write("\r\n");

                                            continue;
                                        }

                                        if ( (colLine.indexOf("$=/")) > 0 )// 第五种情况 单个变量
                                        // vts
                                        {
                                        	//colLine = colLine.replaceAll("\\$=/", "");
                                        	colLine = colLine.replaceAll("=/", "");

                                        	//colLine = colLine.replaceAll("\\$", "");


                                        	colLine = SingReplaceByVTSMuch(colLine, fw);

                                            fw.write(colLine);
                                            fw.write("\r\n");

                                            continue;
                                        }

                                    }

                        		}
                        		//清空list,保证list里始终记录的是一行所对应的html标记.
                        		list.clear();

                        		line = reader.readLine();
                                continue;
                            }
                        }
                        if (line.indexOf("$&gt;") > 0)
                        {
                        	//忽略"$>"行里多出来的<tr>标记
                        	list.clear();
                        	//忽略"$>"行里多出来的</tr><td></td>标记
                        	do
                            {
                                line = reader.readLine();
                                //continue;
                            }
                            while ((line.indexOf("</tr>")) == -1);
                        	if (line.indexOf("</tr>") > 0)
                            {
                                line = reader.readLine();
                                continue;
                            }
                        }
                    }
                    //对不需要显示的控制区域进行相应的处理
                    else
                    {
                        do
                        {
                            line = reader.readLine();
                            //continue;
                        }
                        while ((line.indexOf("$&gt;")) == -1);

                        if (line.indexOf("$&gt;") > 0)
                        {
                        	//忽略"$>"行里多余的</tr><td></td>标记
                        	do
                            {
                                line = reader.readLine();
                                //continue;
                            }
                            while ((line.indexOf("</tr>")) == -1);
                        	if (line.indexOf("</tr>") > 0)
                            {
                                line = reader.readLine();
                                continue;
                            }
                        }
                    }

                }

                //处理一行html标记
                //开始处理一行html标记,以<tr开头的line行
            	if ( (line.indexOf("<tr")) > 0 )
                {

            		mFlag = 1;
            		list.add(line);

                	line = reader.readLine();
                	continue;
                }
            	//处理一行html标记里<tr>和</tr>之间的line行
            	else if ( mFlag == 1 && (line.indexOf("</tr>")) == -1 )
            	{
            		list.add(line);

                	line = reader.readLine();
                	continue;
            	}
            	//一行html标记结束,内容为</tr>的line行
            	else if ( mFlag == 1 && (line.indexOf("</tr>")) > 0 )
                {
            		mFlag = 0;
            		//list里得到一行完整的html标记
            		list.add(line);
            		int colFlag = 0;
                    int flag = 0;
            		int maxcol = list.size();
                    String colLine;
            		for (int col = 0; col < maxcol; col++)
                    {
            			colLine = (String) list.get(col);
            			if ((colLine.indexOf("&amp;&amp;1")) > 0)
                        {
                        	colFlag = 1;
                        	break;
                        }
                    }
            		//处理循环变量所在行
            		if(colFlag == 1)
            		{

            			String tempColLine = "";
            			for (int col = 0; col < maxcol; col++)
                        {
                            colLine = (String) list.get(col);
                            if (flag == 0 && (colLine.indexOf("&amp;&amp;1")) == -1 && (colLine.indexOf("%%1")) == -1
                                    && (colLine.indexOf("$")) == -1)// 第一种情况 vts
                            {

                            	colList.add(colLine);

                                continue;
                            }

                            if (flag == 0 && (colLine.indexOf("&amp;&amp;1")) > 0 && (colLine.indexOf("%%1")) > 0)// 第六种情况 单列循环变量
                            // vts
                            {

                            	flag = 1;
                            	colLine = colLine.replaceAll("&amp;&amp;1", "");

                            }

                            if (flag == 0 && (colLine.indexOf("&amp;&amp;1")) > 0 && (colLine.indexOf("%%1")) == -1)// 第二种情况
                            // 循环开始
                            // vts
                            {

                            	flag = 1;
                            	colLine = colLine.replaceAll("&amp;&amp;1", "");
                                colLine = colLine.replaceAll("\\$", "");// 去掉$
                                colLine = colLine.replaceAll("\\*", "");// 去掉*
                                colLine = colLine.replaceFirst("/", "");

                                colList.add(colLine);

                                continue;

                            }

                            if (flag == 1 && (colLine.indexOf("&amp;&amp;1")) == -1 && (colLine.indexOf("%%1")) == -1) // 第三种情况
                            // 循环进行
                            // vts
                            {

                            	colLine = colLine.replaceAll("\\$", "");
                                colLine = colLine.replaceFirst("\\*", "");
                                colLine = colLine.replaceFirst("/", "");

                                colList.add(colLine);

                                continue;
                            }

                            if (flag == 1 && (colLine.indexOf("%%1") > 0 && (colLine.indexOf("&amp;&amp;1")) == -1)) // 第四种情况
                            // 循环结束 vts
                            {

                                flag = 0;
                                tempColLine = colLine;
                                colLine = colLine.replaceAll("%%1", "");
                                colLine = colLine.replaceAll("\\$", "");
                                colLine = colLine.replaceFirst("\\*", "");
                                colLine = colLine.replaceFirst("/", "");
                                colList.add(colLine);

                                continue;
                            }

                            if (flag == 0 && (colLine.indexOf("$=/")) > 0 && (colLine.indexOf("%%1")) == -1
                                    && (colLine.indexOf("&amp;&amp;1")) == -1)// 第五种情况 单个变量
                            // vts
                            {
                            	//colLine = colLine.replaceAll("\\$=/", "");
                            	colLine = colLine.replaceAll("=/", "");

                                //colLine = colLine.replaceAll("\\$", "");

                                colLine = SingReplaceByVTSMuch(colLine, fw);
                                
                                colList.add(colLine);

                                continue;
                            }
                        }
            			//现在一般只用的到if的情况,else已经不走了。
            			if (this.MuchListTable != null) // 说明是多个列表
                        {
                            //取出%%1及其以后的字符串
            				Pattern pattern1 = Pattern.compile("%%1.*");
                            Matcher matcher1 = pattern1.matcher(tempColLine);
                            StringBuffer buffer1 = new StringBuffer();
                            String templine1 = "";
                            if (matcher1.find())
                            {
                                buffer1.append(matcher1.group());
                                templine1 = buffer1.toString();
                            }
                            //取出/ROW/COL及其以后的字符串
            				Pattern pattern2 = Pattern.compile("/ROW/COL.*");
                            Matcher matcher2 = pattern2.matcher(tempColLine);
                            StringBuffer buffer2 = new StringBuffer();
                            String templine2 = "";
                            if (matcher2.find())
                            {
                                buffer2.append(matcher2.group());
                                templine2 = buffer2.toString();
                            }
                            //得到循环变量名
                            templine1 = templine1.replaceAll("%%1", "");
                            templine1 = templine1.replaceAll("\\$", "");
                            templine1 = templine1.replaceFirst("\\*", "");
                            templine1 = templine1.replaceFirst("/", "");
                            templine1 = templine1.replaceAll(templine2, "");
                            tempColLine = templine1;

                            int nameFlag = 0;  //0代表该循环变量在BL类里没有被赋值,1代表赋值了.
                            int size = this.MuchListTable.size();
                            for (int i = 0; i < size; i++)
                            {
                                this.listtable = (ListTable) this.MuchListTable.get(i);
                                String name = this.listtable.getName();
                                //处理循环变量名的子集问题.
                                //if ((tempColLine.indexOf(name)) > 0)
                                if ( tempColLine.equals(name) )
                                {

                                	nameFlag = 1;
                                	forwritebyvtsmuch(colList, fw);
                                    colList.clear();
                                    //因为一行里只会存在一个循环变量.
                                    break;
                                }
                            }
                            if( nameFlag == 0 )
                            {
                            	forwritebyvtsmuchnull(colList, fw, tempColLine);  //处理没有被赋值的循环变量
                            	colList.clear();
                            }
                        }
                        else
                        // 说明是一个列表
                        {
                            forwritebyvtsmuch(colList, fw);

                        }
            		}
            		//处理循环变量以外的行
            		else
            		{
            			for (int col = 0; col < maxcol; col++)
                        {
                            colLine = (String) list.get(col);
                            if ( (colLine.indexOf("$")) == -1 )// 第一种情况 vts
                            {

                                fw.write(colLine);
                                fw.write("\r\n");

                                continue;
                            }

                            if ( (colLine.indexOf("$=/")) > 0 )// 第五种情况 单个变量
                            // vts
                            {
                            	//colLine = colLine.replaceAll("\\$=/", "");
                            	colLine = colLine.replaceAll("=/", "");

                            	//colLine = colLine.replaceAll("\\$", "");


                            	colLine = SingReplaceByVTSMuch(colLine, fw);

                                fw.write(colLine);
                                fw.write("\r\n");

                                continue;
                            }

                        }

            		}
            		//清空list,保证list里始终记录的是一行所对应的html标记.
            		list.clear();

            		line = reader.readLine();
                    continue;
                }
            	//处理不是一行的html标记，即不是<tr></tr>且不在<tr></tr>之间的标记行
            	else
                {

                    fw.write(line);
                    fw.write("\r\n");

                    line = reader.readLine();
                    continue;
                }

            	//以下注释部分为在之前PraseByVts()方法里的部分内容基础上所做的工作,
            	//加入了对单列循环和显示区域的处理,缺陷是对存在循环变量的行的html标记输出不完整
//                if (flag == 0 && (line.indexOf("&amp;&amp;1")) == -1 && (line.indexOf("%%1")) == -1
//                        && (line.indexOf("$")) == -1)// 第一种情况 vts
//                {
//
//                    fw.write(line);
//                    fw.write("\r\n");
//
//                    line = reader.readLine();
//                    continue;
//                }
//
//                if (flag == 0 && (line.indexOf("&amp;&amp;1")) > 0 && (line.indexOf("%%1")) > 0)// 第六种情况 单列循环变量
//                // vts
//                {
//                	//list.add(this.TrTemp);
//                	flag = 1;
//                    line = line.replaceAll("&amp;&amp;1", "");
//
//                }
//
//                if (flag == 0 && (line.indexOf("&amp;&amp;1")) > 0 && (line.indexOf("%%1")) == -1)// 第二种情况
//                // 循环开始
//                // vts
//                {
//                	//list.add(this.TrTemp);
//                	flag = 1;
//                    line = line.replaceAll("&amp;&amp;1", "");
//                    line = line.replaceAll("\\$", "");// 去掉$
//                    line = line.replaceAll("\\*", "");// 去掉*
//                    line = line.replaceFirst("/", "");
//
//                    list.add(line);
//                    line = reader.readLine();
//                    continue;
//
//                }
//
//                if (flag == 1 && (line.indexOf("&amp;&amp;1")) == -1 && (line.indexOf("%%1")) == -1) // 第三种情况
//                // 循环进行
//                // vts
//                {
//                    //此处多余考虑了循环空列的情况
//                	//if (line.indexOf("$*") > 0)
//                    //{
//
//                        line = line.replaceAll("\\$", "");
//                        line = line.replaceFirst("\\*", "");
//                        line = line.replaceFirst("/", "");
//                        list.add(line);
//
//                    //}
//                    //else
//                    //{
//
//                    //    fw.write(line);
//                    //    fw.write("\r\n");
//                    //}
//                    line = reader.readLine();
//                    continue;
//                }
//
//                if (flag == 1 && (line.indexOf("%%1") > 0 && (line.indexOf("&amp;&amp;1")) == -1)) // 第四种情况
//                // 循环结束 vts
//                {
//
//                    flag = 0;
//                    line = line.replaceAll("%%1", "");
//                    line = line.replaceAll("\\$", "");
//                    line = line.replaceFirst("\\*", "");
//                    line = line.replaceFirst("/", "");
//                    list.add(line);
//                    list.add("</tr>");
//                    if (this.MuchListTable != null) // 说明是多个列表
//                    {
//                        int size = this.MuchListTable.size();
//                        for (int i = 0; i < size; i++)
//                        {
//                            this.listtable = (ListTable) this.MuchListTable.get(i);
//                            String name = this.listtable.getName();
//                            if ((line.indexOf(name)) > 0)
//                            {
//
//                                forwritebyvtsmuch(list, fw);
//                                break;
//                            }
//                        }
//
//                    }
//                    else
//                    // 说明是一个列表
//                    {
//                        forwritebyvtsmuch(list, fw);
//
//                    }
//                    line = reader.readLine();
//                    continue;
//                }
//
//                if (flag == 0 && (line.indexOf("$=/")) > 0 && (line.indexOf("%%1")) == -1
//                        && (line.indexOf("&amp;&amp;1")) == -1)// 第五种情况 单个变量
//                // vts
//                {
//                    line = line.replaceAll("\\$=/", "");
//
//                    line = line.replaceAll("\\$", "");
//
//
//                    SingReplaceByVTSMuch(line, fw);
//
//
//                    line = reader.readLine();
//                    continue;
//                }
//
//                if ((line.indexOf("$&lt;/")) > 0)//第七种情况 存在列表开始标识，判断是不是要解析
//                {
//                    int disFlag = 0;
//                    String display = "";
//                    line = line.replaceAll("\\$&lt;/", "");
//                    display = line;
//
//                    line = reader.readLine();
//
//                    if (this.myvector != null)
//                    {
//                        String[] tempArray = new String[2];
//                        int size = this.myvector.size();
//                        for (int in = 0; in < size; in++)
//                        {
//                            tempArray = (String[]) myvector.get(in);
//                           
//                            //if (display.equal(tempArray[0]))
//                            if (display.indexOf(tempArray[0]) > 0)
//                            {
//                                disFlag = 1;
//                                break;
//                            }
//                        }
//                    }
//                   
//                    if (disFlag == 1)
//                    {
//
//                        while (line.indexOf("$&gt;") == -1)
//                        {
//
////                            if ((line.indexOf("<tr")) > 0)
////                            {
////                                this.TrTemp = line;
////                            }
//
//                        	if ((line.indexOf("</tr>")) == -1)
//                            {
//                            	list.add(line);
//                            }
//
//                            if ((line.indexOf("</tr>")) > 0)
//                            {
//                            	list.clear();
//                            }
//
//                            if (flag == 0 && (line.indexOf("&amp;&amp;1")) == -1 && (line.indexOf("%%1")) == -1
//                                    && (line.indexOf("$")) == -1)// 第一种情况 vts
//                            {
//
//                                fw.write(line);
//                                fw.write("\r\n");
//
//                                line = reader.readLine();
//                                continue;
//                            }
//
//                            if (flag == 0 && (line.indexOf("&amp;&amp;1")) > 0 && (line.indexOf("%%1")) > 0)// 第六种情况 单列循环变量
//                            // vts
//                            {
//                                //list.add(this.TrTemp);
//                                flag = 1;
//                                line = line.replaceAll("&amp;&amp;1", "");
//
//                            }
//
//                            if (flag == 0 && (line.indexOf("&amp;&amp;1")) > 0 && (line.indexOf("%%1")) == -1)// 第二种情况
//                            // 循环开始
//                            // vts
//                            {
//                                //list.add(this.TrTemp);
//                                flag = 1;
//                                line = line.replaceAll("&amp;&amp;1", "");
//                                line = line.replaceAll("\\$", "");// 去掉$
//                                line = line.replaceAll("\\*", "");// 去掉*
//                                line = line.replaceFirst("/", "");
//
//                                list.add(line);
//                                line = reader.readLine();
//                                continue;
//
//                            }
//
//                            if (flag == 1 && (line.indexOf("&amp;&amp;1")) == -1 && (line.indexOf("%%1")) == -1) // 第三种情况
//                            // 循环进行
//                            // vts
//                            {
//                                //此处多余考虑了循环空列的情况
//                            	//if (line.indexOf("$*") > 0)
//                                //{
//
//                                    line = line.replaceAll("\\$", "");
//                                    line = line.replaceFirst("\\*", "");
//                                    line = line.replaceFirst("/", "");
//                                    list.add(line);
//
//                                //}
//                                //else
//                                //{
//
//                                //    fw.write(line);
//                                //    fw.write("\r\n");
//                                //}
//                                line = reader.readLine();
//                                continue;
//                            }
//
//                            if (flag == 1 && (line.indexOf("%%1") > 0 && (line.indexOf("&amp;&amp;1")) == -1)) // 第四种情况
//                            // 循环结束 vts
//                            {
//
//                                flag = 0;
//                                line = line.replaceAll("%%1", "");
//                                line = line.replaceAll("\\$", "");
//                                line = line.replaceFirst("\\*", "");
//                                line = line.replaceFirst("/", "");
//                                list.add(line);
//                                list.add("</tr>");
//                                if (this.MuchListTable != null) // 说明是多个列表
//                                {
//                                    int size = this.MuchListTable.size();
//                                    for (int i = 0; i < size; i++)
//                                    {
//                                        this.listtable = (ListTable) this.MuchListTable.get(i);
//                                        String name = this.listtable.getName();
//                                        if ((line.indexOf(name)) > 0)
//                                        {
//
//                                            forwritebyvtsmuch(list, fw);
//                                            break;
//                                        }
//                                    }
//
//                                }
//                                else
//                                // 说明是一个列表
//                                {
//                                    forwritebyvtsmuch(list, fw);
//
//                                }
//                                line = reader.readLine();
//                                continue;
//                            }
//
//                            if (flag == 0 && (line.indexOf("$=/")) > 0 && (line.indexOf("%%1")) == -1
//                                    && (line.indexOf("&amp;&amp;1")) == -1)// 第五种情况 单个变量
//                            // vts
//                            {
//                                line = line.replaceAll("\\$=/", "");
//
//                                line = line.replaceAll("\\$", "");
//
//
//                                SingReplaceByVTSMuch(line, fw);
//
//
//                                line = reader.readLine();
//                                continue;
//                            }
//                        }
//                        if (line.indexOf("$&gt;") > 0)
//                        {
//                            // 当前行为“$&gt;”则换行
//                            line = reader.readLine();
//                            continue;
//                        }
//                    }
//
//                    else
//                    {
//                        do
//                        {
//                            line = reader.readLine();
//                            continue;
//                        }
//                        while ((line.indexOf("$&gt;")) == -1);
//
//                        // 当读完“$&gt;”换行
//                        line = reader.readLine();
//                        continue;
//                    }
//
//                }

            }
            while (line != null);
            read.close();
            reader.close();
            fw.close();
            bflag = true;
        }
        catch (Exception e)
        {
            ci = 1;
            e.printStackTrace();
            this.ErrorMession = e.getMessage();

        }

        //传回布尔值错误纠正
        //return false;
        return bflag;
    }

    private void SingReplaceByVTS(String line, FileWriter fw) throws IOException
    {
       
        String name = "";
        //此处意图是处理变量名里含有/ROW/COL的单个变量,但是这样的变量在系统里应该不存在,所以if部分其实可以去除的。
        //if ((line.indexOf("ROWCOL")) > 0)
        if ((line.indexOf("/ROW/COL")) > 0)
        {
            int size = this.MuchListTable.size();
            for (int i = 0; i < size; i++)
            {
                this.listtable = (ListTable) this.MuchListTable.get(i);
                name = this.listtable.getName();
                if ((line.indexOf(name)) > 0)
                {
                   
                    break;

                }
            }

            String templine = "";
            line = line.replaceAll("\\*", "");
            Pattern pattern = Pattern.compile("ROWCOL.*");
            Matcher matcher = pattern.matcher(line);
            StringBuffer buffer = new StringBuffer();
            if (matcher.find())
            {
                buffer.append(matcher.group());
               
                templine = buffer.toString();
            }
            templine = templine.replaceAll("ROWCOL", "");
            templine = templine.replaceAll("<td>", "");
            int no = Integer.parseInt(templine);
            String[] values = (String[]) listtable.get(0);
           
            String valuesss = "";
         
            if ((no - 1) < values.length)
            {
                if (values[no - 1] == null)
                {
                    valuesss = "";
                }
                else
                {
                    valuesss = values[no - 1];
                }
            }
            else
            {
                valuesss = "";
            }

            //此处意图是处理变量名里含有/ROW/COL的单个变量,但是这样的变量在系统里应该不存在
            //line = line.replaceAll(name + "ROWCOL" + no, valuesss);
            line = line.replaceAll(name + "/ROW/COL" + no, valuesss);
            line = line.replaceAll("<td>", "</td>");
            
            fw.write(line);
            fw.write("\r\n");

        }
        else
        {
            if (this.myvector != null)
            {

                String[] temparray = new String[2];
                int size = this.myvector.size();
              

                for (int in = 0; in < size; in++)
                {
                    temparray = (String[]) myvector.get(in);

                    if ((line.indexOf(temparray[0])) > 0)
                    {
                        line = line.replaceAll(temparray[0], temparray[1]);
                        line = line.replaceAll("<td>", "</td>");
                       
                        fw.write(line);
                        fw.write("\r\n");
                        break;
                    }
                }

            }
        }

    }

    //输入单个变量名所在的line行,取出单个变量的值,并用它替换单个变量名,然后返回单个变量所在的line行.
    private String SingReplaceByVTSMuch(String line, FileWriter fw) throws IOException
    {

    	if (this.myvector != null)
        {
            //准确定位单个变量或者显示区域变量名
    		String display[] = null;
            display = line.split("\\$");
            String[] tempArray = new String[2];
            int size = this.myvector.size();
            int length = display.length;

            line = line.replaceAll("\\$", "");

            for (int i = 1; i < length; i++)
            {
            	int flag = 0;   //1代表该单个变量或者显示区域变量在BL类里被赋值了,0代表没有.
                for (int j = 0; j < size; j++)
                {
                	tempArray = (String[]) myvector.get(j);
                	if ( display[i].equals(tempArray[0]) )
                    {
                		flag = 1;
                		//用StrTool.replace方法主要是为了处理将要替换的字符里含有$的情况,避免用String.replace方法出现的JAVA内部缺陷导致的错误
                		line = StrTool.replace(line,tempArray[0],tempArray[1]);
                		break;
                        //前期有将所有/替换为空的情况，现在已经没有了。
                        //line = line.replaceAll("<td>", "</td>");

                        //解决一行上有多个单个变量的情况所带来的问题。
                        //fw.write(line);
                        //fw.write("\r\n");
                        //break;
                    }
                }
                //处理未在BL类里赋值的变量
                if( flag == 0 )
                {
                	line = StrTool.replace(line,display[i],"");
                }
            	//因为变量名总是存储在下标为奇数的数组项里
            	i++;
            }
//            //解决一行上有多个单个变量的情况所带来的问题。
//            fw.write(line);
//            fw.write("\r\n");
        }
    	return line;
    }

    private void forwritebyvts(List list, FileWriter fw) throws IOException
    {
        
        int num = 0;
        int maxrow = listtable.size();
        int maxcol = list.size();
        String listtabelname = listtable.getName();
       

        for (int row = 0; row < maxrow; row++)
        {
            String[] values = (String[]) listtable.get(row);
            values=replace(values);
            for (int col = 0; col < maxcol; col++)
            {

                String line = (String) list.get(col);
              
                if ((line.indexOf(listtabelname)) > 0)
                {
                   
                    Pattern pattern = Pattern.compile("/ROW/COL.*");
                    Matcher matcher = pattern.matcher(line);
                    StringBuffer buffer = new StringBuffer();
                    String templine = "";
                    int no = 0;

                    if (matcher.find())
                    {
                        buffer.append(matcher.group());
                        templine = buffer.toString();
                       

                    }

                    templine = templine.replaceAll("/ROW/COL", "");
                    templine = templine.replaceAll("</td>", "");

                    no = Integer.parseInt(templine);
                   
                    if (values[no - 1] == null)
                    {
                        String temp = "";
                        line = line.replaceAll(listtabelname + "/ROW/COL" + "" + no, temp);
                       
                        fw.write(line);
                        fw.write("\r\n");
                      
                    }
                    else
                    {
                        line = line.replaceAll(listtabelname + "/ROW/COL" + "" + no, values[no - 1]);
                     
                        fw.write(line);
                        fw.write("\r\n");
                        
                    }

                }
                else
                {

                    

                    if (num == 0)
                    {
                        num = 1;
                    }
                    else
                    {
                        if ((line.indexOf("</td>")) > 0)
                        {
                            if (row == maxrow - 1)
                            {
                                fw.write("\r\n");
                            }
                            else
                            {
                               
                                fw.write(line);
                                fw.write("\r\n");
                            }
                        }
                        else
                        {
                            
                            fw.write(line);
                            fw.write("\r\n");
                        }

                    }

                }

            }

        }

        num = 0;
    }

    private String[] replace(String[] values) 
    {
		// TODO 自动生成方法存根
    	for(int i=0;i<values.length;i++)
    	{
    		if(values[i].indexOf("$")>0)
    		{
    			values[i]=values[i].replaceAll("\\$", "");
    		}
    	}
		return values;
	}

    //输出包含循环变量的行,用相应的循环变量的值替换循环变量名
    private void forwritebyvtsmuch(List list, FileWriter fw) throws IOException
    {
        
        int maxrow = listtable.size();
        int maxcol = list.size();
        String listtabelname = listtable.getName();
       
        for (int row = 0; row < maxrow; row++)
        {
            String[] values = (String[]) listtable.get(row);
            //判断是不是存在一行循环变量的值全为空的情况,有的话就不输出该行了!
            int flag = 0;  //0代表该行的循环变量的值全为空,否则为1
            for (int col = 0; col < maxcol; col++)
            {
                String line = (String) list.get(col);
                if ((line.indexOf(listtabelname)) > 0)
                {
                    Pattern pattern = Pattern.compile("/ROW/COL.*");
                    Matcher matcher = pattern.matcher(line);
                    StringBuffer buffer = new StringBuffer();
                    String templine = "";
                    int no = 0;
                    if (matcher.find())
                    {
                        buffer.append(matcher.group());
                        templine = buffer.toString();
                    }
                    templine = templine.replaceAll("/ROW/COL", "");
                    templine = templine.replaceAll("</td>", "");
                    no = Integer.parseInt(templine);
                    if (values[no - 1] != null)
                    {
                        flag = 1;
                        break;
                    }
                }
            }
            //处理循环变量的值不全为空的一行的html标记
            if(flag == 1)
            {
            	for (int col = 0; col < maxcol; col++)
                {

                    String line = (String) list.get(col);

                    if ((line.indexOf(listtabelname)) > 0)
                    {

                        Pattern pattern = Pattern.compile("/ROW/COL.*");
                        Matcher matcher = pattern.matcher(line);
                        StringBuffer buffer = new StringBuffer();
                        String templine = "";
                        int no = 0;

                        if (matcher.find())
                        {
                            buffer.append(matcher.group());
                            templine = buffer.toString();

                        }

                        templine = templine.replaceAll("/ROW/COL", "");
                        templine = templine.replaceAll("</td>", "");

                        no = Integer.parseInt(templine);
                        //用StrTool.replace方法主要是为了处理将要替换的字符里含有$的情况,避免用String.replace方法
                        //出现的JAVA内部缺陷导致的错误.而且用该方法的话,若一行里的某一个循环变量为空,该列就会输出空格
                        if((no-1)>=values.length) //防止出现数组越界错误
                        {
                        	line = StrTool.replace(line,listtabelname + "/ROW/COL" + "" + no,"");
                        }
                        else
                        {
                        	line = StrTool.replace(line,listtabelname + "/ROW/COL" + "" + no,values[no - 1]);
                        }
                        
                    }

                    fw.write(line);
                    fw.write("\r\n");
                }
            }
        }
    }

    //处理没有被赋值的循环变量所在行
    private void forwritebyvtsmuchnull(List list, FileWriter fw, String cycleName) throws IOException
    {

        int maxcol = list.size();
    	for (int col = 0; col < maxcol; col++)
        {
            String line = (String) list.get(col);

            if ( (line.indexOf(cycleName)) > 0 )
            {
                Pattern pattern = Pattern.compile("/ROW/COL.*");
                Matcher matcher = pattern.matcher(line);
                StringBuffer buffer = new StringBuffer();
                String templine = "";

                if (matcher.find())
                {
                    buffer.append(matcher.group());
                    templine = buffer.toString();
                }

                templine = templine.replaceAll("</td>", "");

                line = StrTool.replace(line,cycleName + templine,"");
            }
            fw.write(line);
            fw.write("\r\n");
        }
    }

	public  void XmlParse(InputStream tis)
    {
    	System.out.println("V3 开始！");
    	try
    	{
    		 DocumentBuilderFactory domfac=DocumentBuilderFactory.newInstance();
    		 DocumentBuilder dombuilder=domfac.newDocumentBuilder();
 			 InputStream is=tis;
 			 Document doc=dombuilder.parse(is);
 			 Element root=doc.getDocumentElement();
 			 NodeList FirstChildNodeList=root.getChildNodes();
 			 String TempLateName="";//html模板文件名
 			 
 			 if(FirstChildNodeList!=null)//第一层
 			 {
 				 for(int i=0;i<FirstChildNodeList.getLength();i++)
 				 {
 					 Node FirstChildNode=FirstChildNodeList.item(i);
 					 if(FirstChildNode!=null)//第一层节点不为空
 					 {
 						if(FirstChildNode.getNodeName().indexOf("#")<0)//节点上不是以#开始
 	 					 {
 							if(FirstChildNode.getNodeName().equals("CONTROL"))//以CONTROL开头，查找模板文件。
 							{
 								NodeList TempLateNodeList=FirstChildNode.getChildNodes();
 								if(TempLateNodeList!=null)
 								{
 									for(int j=0;j<TempLateNodeList.getLength();j++)
 									{
 										Node TempLateNode=TempLateNodeList.item(j);
 										if(TempLateNode.getNodeName().indexOf("#")<0)
 										{
 											if(TempLateNode.getNodeName().equals("TEMPLATE"))
 	 	 									{
 												if(TempLateNode.getFirstChild()!=null)
 												{
 													TempLateName=TempLateNode.getFirstChild().getNodeValue();
 	 	 	 										TempLateName=TempLateName.replaceAll("\\.vts", ".htm");//模板名称取值完毕
 	 	 	 									    this.gTempLateName=TempLateName;
 	 	 	 										break;
 												}
 	 	 										
 	 	 									}
 										}
 										
 									}
 									
 								}
 								continue;
 							}
 								
 							NodeList SecondChildNodeList= FirstChildNode.getChildNodes();  //第二层
 							if(SecondChildNodeList!=null)
 							{
 								if(SecondChildNodeList.getLength()==1)//此时的FirstChildNode是单个变量
 	 							{
 	 								String SingValue=FirstChildNode.getFirstChild().getNodeValue();
 	 								String SingName=FirstChildNode.getNodeName();
 	 								gTextTag.add(SingName, SingValue);
 	 							}
 								if(SecondChildNodeList.getLength()>1) //此时的FirstChildNode是循环变量
 								{
 									ListTable tListTable = new ListTable();
									tListTable.setName(FirstChildNode.getNodeName());
 									for(int n=0;n<SecondChildNodeList.getLength();n++)
 									{
 										int MaxCol=0;//最大列数
 										Node SecondChildNode=SecondChildNodeList.item(n);
 										if(SecondChildNode!=null)
 										{
 											if(SecondChildNode.getNodeName().indexOf("#")<0)//节点上不是以#开始
 											{
 												if(SecondChildNode.getNodeName().equals("ROW"))
 												{
 													NodeList ThirdNodeList=SecondChildNode.getChildNodes();//第三层
 													List templist=new ArrayList();//记录一行中每列的值
 													if(ThirdNodeList!=null)
 													{
 														for(int x=0;x<ThirdNodeList.getLength();x++)//列开始循环,取一行的值
 														{
 															Node ThirdNode=ThirdNodeList.item(x);
 															if(ThirdNode!=null)
 															{
 																if((ThirdNode.getNodeName().indexOf("#"))<0)//节点上不是以#开始
 																{
 																	if(ThirdNode.getNodeType()==Node.ELEMENT_NODE)
 																	{
 																		MaxCol=Integer.parseInt(ThirdNode.getNodeName().replaceAll("COL", ""));
 																		if(ThirdNode.getFirstChild()!=null)
 																		{
 																			
 	 	 																	String ThirdNodeValue=ThirdNode.getFirstChild().getNodeValue();
 	 	 																	if(ThirdNodeValue.indexOf("\n")>0)
 	 	 																	{
 	 	 																		
 	 	 																		ThirdNodeValue="";
 	 	 																	}
 	 	 																	int index=MaxCol;
 	 	 																	String temp[]=new String[2];
 	 	 																	temp[0]=""+index;  //列号
 	 	 																	temp[1]=ThirdNodeValue;//值
 	 	 																	templist.add(temp);

 																		}
 																		else
 																		{
 																			
 																			String temp[]=new String[2];
 	 	 																	temp[0]=""+MaxCol;  //列号
 	 	 																	temp[1]="";//值
 	 	 																	templist.add(temp);
 																		}
 																		
 																	}
 																	
 																}
 															}
 														}
 														String HangValue[]=new String[MaxCol];
 														int index=0;
 														for(int y=0;y<templist.size();y++)
 														{
 															String temp1[]=new String[2];
 															temp1=(String[])templist.get(y);
 															if(y+1==Integer.parseInt(temp1[0]))
 															{
 																index=y;
 																HangValue[index]=temp1[1];
 																
 															}
 															else
 															{
 																HangValue[index]="";
 															}
 														}
 														
 														
 														tListTable.add(HangValue);
 														this.gListTablelist.add(tListTable);
 													}
 												}
 											}
 										}
 									}
 								}
 							} 							 								 						
 	 					 }
 					 }
 					 
 				 }
 			 }
           this.setSingTextTag(this.gTextTag.getMyVector());
           this.setMuchListTable(this.gListTablelist);
    	}
    	catch (Exception e)
    	{
    		System.out.println("Error Mession is "+e.getMessage());
    		e.printStackTrace();
    	}
    }

    //此解析Xml的方法可以处理包含显示区域控制的情况
    public  void XmlParseMuch(InputStream tis)
    {
    	System.out.println("V4 开始！");
    	try
    	{
    	    DocumentBuilderFactory domfac=DocumentBuilderFactory.newInstance();
    		DocumentBuilder dombuilder=domfac.newDocumentBuilder();
 			InputStream is=tis;
 			Document doc=dombuilder.parse(is);
 			Element root=doc.getDocumentElement();
 			NodeList FirstChildNodeList=root.getChildNodes();
 			String TempLateName="";//html模板文件名

 			if(FirstChildNodeList!=null)//第一层
 			{
 		        int FirstChildNodeListLength = FirstChildNodeList.getLength();
 			    for(int i=0;i<FirstChildNodeListLength;i++)
 				{
 				    Node FirstChildNode=FirstChildNodeList.item(i);
 					if(FirstChildNode.getNodeName().indexOf("#")<0)//节点上不是以#开始
					{
					    if(FirstChildNode.getNodeName().equals("CONTROL"))//以CONTROL开头，查找模板文件。
						{
					        NodeList TempLateNodeList=FirstChildNode.getChildNodes();
							if(TempLateNodeList!=null)
							{
							    int TempLateNodeListLength = TempLateNodeList.getLength();
								for(int j=0;j<TempLateNodeListLength;j++)
								{
									Node TempLateNode=TempLateNodeList.item(j);
								    if(TempLateNode.getNodeName().indexOf("#")<0)
									{
									    if(TempLateNode.getNodeName().equals("TEMPLATE"))
	 									{
											if(TempLateNode.getFirstChild()!=null)
											{
												TempLateName=TempLateNode.getFirstChild().getNodeValue();
	 	 										TempLateName=TempLateName.replaceAll("\\.vts", ".htm");//模板名称取值完毕
	 	 									    this.gTempLateName=TempLateName;
	 	 										//break;
											}
	 										
	 									}
										//取出显示区域控制变量,和单个变量放在一起,因此要求单个变量名和循环变量名不能相同.
										if(TempLateNode.getNodeName().equals("DISPLAY"))
	 									{
											NodeList tNodeList = TempLateNode.getChildNodes();
											if(tNodeList != null)
											{
												int tNodeListLength = tNodeList.getLength();
												for(int m = 0;m < tNodeListLength;m++)
												{
													Node tNode = tNodeList.item(m);
													if(tNode.getNodeName().indexOf("#")<0)
													{
														String SingValue = "1";
														String SingName = tNode.getNodeName();
														gTextTag.add(SingName, SingValue);
													}
												}
											}
	 									}
									}
								}
							}
							continue;
						}

						NodeList SecondChildNodeList= FirstChildNode.getChildNodes();  //第二层
						if(SecondChildNodeList!=null)
						{
						    if(SecondChildNodeList.getLength()==1)//此时的FirstChildNode是单个变量
							{
								String SingValue=FirstChildNode.getFirstChild().getNodeValue();
								String SingName=FirstChildNode.getNodeName();
								gTextTag.add(SingName, SingValue);
							}
							if(SecondChildNodeList.getLength()>1) //此时的FirstChildNode是循环变量
							{
								ListTable tListTable = new ListTable();
								tListTable.setName(FirstChildNode.getNodeName());
								int SecondChildNodeListLength = SecondChildNodeList.getLength();
								for(int n=0;n<SecondChildNodeListLength;n++)
								{
									int MaxCol=0;//最大列数
									Node SecondChildNode=SecondChildNodeList.item(n);
									if(SecondChildNode.getNodeName().indexOf("#")<0)//节点上不是以#开始
									{
										if(SecondChildNode.getNodeName().equals("ROW"))
										{
											NodeList ThirdNodeList=SecondChildNode.getChildNodes();//第三层
											List templist=new ArrayList();//记录一行中每列的值
											if(ThirdNodeList!=null)
											{
												int ThirdNodeListLength = ThirdNodeList.getLength();
												for(int x=0;x<ThirdNodeListLength;x++)//列开始循环,取一行的值
												{
													Node ThirdNode=ThirdNodeList.item(x);
													if((ThirdNode.getNodeName().indexOf("#"))<0)//节点上不是以#开始
													{
														if(ThirdNode.getNodeType()==Node.ELEMENT_NODE)//判断该节点是否有子节点
														{
															MaxCol=Integer.parseInt(ThirdNode.getNodeName().replaceAll("COL", ""));
															if(ThirdNode.getFirstChild()!=null)
															{
															    String ThirdNodeValue=ThirdNode.getFirstChild().getNodeValue();
																if(ThirdNodeValue.indexOf("\n")>0)
																{
																	ThirdNodeValue="";
																}
																int index=MaxCol;
																String temp[]=new String[2];
																temp[0]=""+index;  //列号
																temp[1]=ThirdNodeValue;//值
																templist.add(temp);
															}
															else
															{
																String temp[]=new String[2];
																temp[0]=""+MaxCol;  //列号
																temp[1]="";//值
																templist.add(temp);
															}
														}
												    }
												}
												String HangValue[]=new String[MaxCol];
												int index=0;
												int templistSize = templist.size();
												for(int y=0;y<templistSize;y++)
												{
													String temp1[]=new String[2];
													temp1=(String[])templist.get(y);
													if(y+1==Integer.parseInt(temp1[0]))
													{
														index=y;
														HangValue[index]=temp1[1];
														
													}
													else
													{
														HangValue[index]="";
													}
												}
												tListTable.add(HangValue);
											}
										}
									}
								}
								this.gListTablelist.add(tListTable);
							}
						}
				    }
 				}
 			}
            this.setSingTextTagMuch(this.gTextTag.getMyVector());
            this.setMuchListTableMuch(this.gListTablelist);
    	}
    	catch (Exception e)
    	{
    		System.out.println("Error Mession is "+e.getMessage());
    		e.printStackTrace();
    	}
    }

    public int ReturnResult()
    {
        return this.ci;
    }

    public void setReadFileAddress(String filename) // 设置读文件的地址
    {
        this.readfilename = filename;

    }

    public void setWriteFileAddress(String filename) // 设置写文件的地址
    {
        this.writfilename = filename;

    }

    public void setSingArray(String[][] singarray2) // 设置单个变量 String[][]
    {
        this.singarray = singarray2;
        this.hanglei[0] = this.singarray.length;
        this.hanglei[1] = 2;

    }

    public void setForArray(String[][] forarray2) // 设置列表变量 String[][]
    {
        // TODO 自动生成方法存根
        this.forarray = forarray2;

    }

    public void setHangLei(int[] hanglei2) // 当只有一个列表似的行列信息
    {
        // TODO 自动生成方法存根
        this.hanglei = hanglei2;

    }

    public boolean start(String falge) // 程序开始
    {
        boolean flag = false;
        if (falge.toLowerCase().equals("standard"))
        {
            flag = Prase();
        }
        if (falge.toLowerCase().equals("standardmuch"))
        {
            flag = PraseMuch();
        }
        if (falge.toLowerCase().equals("vts"))
        {
            flag = PraseByVts();
        }
        if (falge.toLowerCase().equals("vtsmuch"))
        {
            flag = PraseByVtsMuch();
        }
        return flag;
    }

    public String getErrorMession() // 返回错误信息
    {
        // TODO 自动生成方法存根
        return this.ErrorMession;
    }

    public void setSingSSRS(SSRS ssrs) // 设置单个变量ssrs
    {
        this.singssrs = ssrs;
    }

    public void setForSSRS(SSRS ssrs) // 设置列表变量ssrs
    {
        this.forssrs = ssrs;
    }

    public void setSingTextTag(Vector vector) // 设置单个变量 TextTag类型
    {
        this.myvector = vector;

    }

    //设置 TextTag 类型的单个变量和显示区域控制变量按变量名长度由长到短依次排列,目的是为了处理变量名存在交集的现象
    public void setSingTextTagMuch(Vector vector)
    {
        String[] SingTextTagName = new String[vector.size()];

        int vectorSize = vector.size();
        for(int i = 0;i < vectorSize;i++)
        {
            String[] tempArray = new String[2];
            tempArray = (String[])vector.get(i);
     	    SingTextTagName[i] = tempArray[0];
        }

        //首先对变量名进行排序
        int SingTextTagNameLength = SingTextTagName.length;
        for(int i = 0;i < SingTextTagNameLength;i++)
        {
            for(int j = i;j < SingTextTagNameLength - 1;j++)
      	    {
            	if(i == SingTextTagNameLength - 1)
        		{
        		    break;
        		}
        		else
        		{
        		    String StandName = SingTextTagName[i];
        		    int StandNameLength = StandName.length();
        		    String TempName = SingTextTagName[j + 1];
        		    int TempNameLength = TempName.length();
        		    if(StandNameLength < TempNameLength)
        		    {
        		    	String temp = "";
        				temp = SingTextTagName[i];
        				SingTextTagName[i] = SingTextTagName[j + 1];
        				SingTextTagName[j + 1] = temp;
      			    }
        		}
            }
        }
        //让排序反映到变量的名和值里
        Vector tempvector = new Vector();
        for(int i = 0;i < SingTextTagNameLength;i++)
        {
            for(int j = 0;j < SingTextTagNameLength;j++)
            {
                String singName = SingTextTagName[i];
                String[] tempArray = new String[2];
           	    tempArray = (String[])vector.get(j);
                if(singName.equals(tempArray[0]))
                {
                    tempvector.add(tempArray);
                    break;
                }
            }
        }
        this.myvector = tempvector;
    }

    // public void setListTableNo(int num) //设置总共有几个列表
    // {
    // this.ListTableSum=num;
    // }

    public void setMuchListTableInfo(List list) // 设置多个列表内容对应的类名
    {
        if (list != null)
        {
            this.MuchListTableInfo = list;

        }
    }

    public void setMuchListTable(List list) // 设置多个列表内容
    {
        if (list != null)
        {
            this.MuchListTable = list;
            this.ListTableSum = list.size();
        }
    }

    //设置多个列表内容
    public void setMuchListTableMuch(List list) // 解决循环变量名存在子集的问题
    {
    	if (list != null)
        {
        	int TempSize = list.size();
        	ListTable TempListtable = null;
        	String[] listtableName = new String[list.size()];
            for (int i = 0; i < TempSize; i++)
            {
            	TempListtable = (ListTable) list.get(i);
                listtableName[i] = TempListtable.getName();
            }
            //排序
            for (int i = 0; i < TempSize; i++)
            {
                for (int j = i; j < TempSize - 1; j++)
                {
                	if(i == TempSize - 1)
            		{
            		    break;
            		}
            		else
            		{
            		    String StandName = listtableName[i];
            		    int StandNameLength = StandName.length();
            		    String TempName = listtableName[j + 1];
            		    int TempNameLength = TempName.length();
            		    if(StandNameLength < TempNameLength)
            		    {
            		    	String temp = "";
            				temp = listtableName[i];
            				listtableName[i] = listtableName[j + 1];
            				listtableName[j + 1] = temp;
          			    }
            		}
                }
            }
    		List TempList = new ArrayList();
            for(int i = 0;i < TempSize;i++)
            {
                for(int j = 0;j < TempSize;j++)
                {
                    String singName = listtableName[i];
                    TempListtable = (ListTable) list.get(j);
                    String tempName = TempListtable.getName();
                    if(singName.equals(tempName))
                    {
                    	TempList.add(TempListtable);
                        break;
                    }
                }
            }
            this.MuchListTable = TempList;
            this.ListTableSum = TempSize;
        }
    }

    public void setMuchHashMapListTable(HashMap hashmap) // 设置多个表内容
    {
        if (hashmap != null)
        {
            this.inHashmap = hashmap;
            this.inHashmapSum = hashmap.size();
        }
    }

    public void setMuchlistTableByCol(List list) // 设置列表的内容 按列赋值
    {
        if (list != null)
        {
            this.ForListByCol = list;
        }
    }

    public void setMuchLineByListTable(ListTable listtable)
    {
        this.listtable = listtable;
    }

    public void setFlage(String temp)
    {
        this.falge = temp;
    }

    public void setSingListTableList(List list)
    {
        this.SingListTableList = list;
    }
    
    public String getTempLateName()
    {
    	return this.gTempLateName;
    }
    
    /**
     * 压缩较大的xls报表文件。
     * @param tInputEntry
     * @param tZipFile
     * @return
     */
    public boolean CreateZipFile(String[] tInputEntry, String tZipFile) {
		final int BUFFER = 2048;
		BufferedInputStream origin = null;
		FileOutputStream f = null;
		ZipOutputStream out = null;

		int fileCount = tInputEntry.length;
		String[] tOutputEntry = new String[fileCount];
		try {
			origin = null;
			f = new FileOutputStream(tZipFile);
			out = new ZipOutputStream(new BufferedOutputStream(f));
			byte data[] = new byte[BUFFER];
			for (int i = 0; i < fileCount; i++) {
				FileInputStream fi = new FileInputStream(tInputEntry[i]);
				origin = new BufferedInputStream(fi, BUFFER);

				int index = 0;
				if (tInputEntry[i].lastIndexOf("/") != -1) {
					index = tInputEntry[i].lastIndexOf("/") + 1;
				} else {
					index = tInputEntry[i].lastIndexOf("\\") + 1;
				}
				tOutputEntry[i] = tInputEntry[i].substring(index);

				ZipEntry entry = new ZipEntry(tOutputEntry[i]);
				out.putNextEntry(entry);
				int count;
				while ((count = origin.read(data, 0, BUFFER)) != -1) {
					out.write(data, 0, count);
				}
				origin.close();
			}
			out.close();
		} catch (Exception ex) {
			return false;
		} finally {
			try {
				origin.close();
				out.close();
			} catch (Exception ex) {
			}
		}
		return true;
	}
    
}
