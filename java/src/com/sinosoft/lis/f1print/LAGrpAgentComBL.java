package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.AgentPubFun;
import java.math.BigDecimal;
import com.sinosoft.lis.pubfun.Arith;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LAGrpAgentComBL {

    public CErrors mErrors = new CErrors();

    private VData mInputData = new VData();

    private VData mResult = new VData();
    private String mReportName = "";
    private String mOperate = "";
    private int tSumStartNo = 0;
    private int tSumEndNo =  0;
    private int tSumAddNo =  0;
    private int tSumSubNo =  0;
    private GlobalInput mGlobalInput = new GlobalInput();
    private String[][] mShowDataList = null;
    private int mRow = 0;
    private int mCol = 6;
    private String[] mDataList = null;
    private TransferData mTransferData = new TransferData();
    private String mBranchAttr = "";
    private String mStartDate = "";
    private String mEndDate = "";
    private String mYear = "";
    private String mBranchName = "";
    private String mMonth = "";
    private String mAgentGroup ="";
    private String mFlag ="";
    private String mManageCom = "";
    private XmlExport mXmlExport = null;

    private SSRS mSSRS1 = new SSRS();


    private ListTable mListTable = new ListTable();

    private PubFun mPubFun = new PubFun();

    private String mManageName = "";

    public LAGrpAgentComBL() {
    }
    public static void main(String[] args)
    {
        //
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";
        LAGrpAgentComBL tLAGrpAgentComBL = new LAGrpAgentComBL();
       // tLAGrpAgentComBL.submitData(tVData, "");
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mOperate = cOperate;
        mInputData = (VData) cInputData;
        if (mOperate.equals("")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!this.getInputData(mInputData)) {
            return false;
        }
        if (!this.check()) {
            return false;
        }

        if (!dealdate()) {
            return false;
        }

        if (!getPrintData()) {
            this.bulidError("getPrintData", "查询数据失败！");
            return false;
        }

        return true;
    }

    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        try
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);

        } catch (Exception ex) {
            this.mErrors.addOneError("");
            return false;
        }
        this.mManageCom = (String) mTransferData.getValueByName("tManageCom");
        this.mBranchAttr = (String) mTransferData.getValueByName("tBranchAttr");
        this.mBranchName = (String) mTransferData.getValueByName("tBranchName");
        this.mAgentGroup = (String) mTransferData.getValueByName("tAgentGroup");
        this.mFlag = (String) mTransferData.getValueByName("tFlag");
        this.mYear = (String) mTransferData.getValueByName("tYear");
        this.mMonth = (String) mTransferData.getValueByName("tMonth");
        this.mStartDate = (String) mTransferData.getValueByName("tStartDate");
        this.mEndDate = (String) mTransferData.getValueByName("tEndDate");
        return true;
    }
    /**
    * 处理前校验
    * @return boolean
    */

   private boolean check()
   {
     if(mFlag ==null ||     mFlag == "")
     {
         this.bulidError("check", "数据不完整!");
         return false;
     }
     else
     {
         if(mFlag.equals("1"))
         {
             this.bulidError("check", "查询年月的起止期出错!");
             return false;
         }
     }
     return true;

   }

    /**
    * 业务处理方法
    * @return boolean
    */

   private boolean dealdate()
   {
      if (!getAgentGradeNow())
      {
         return false;
      }

      return true;
   }
   /**
    * 查询数据
    * @return boolean
   */
   private boolean getAgentGradeNow() {

       ExeSQL tExeSQL = new ExeSQL();
       String tSql =
               "select gradecode,gradename   "
               + " from laagentgrade a "
               + " where  1=1  "
               + " and a.branchtype='2' and a.branchtype2='01' "
               +  " order by  a.gradecode  ";

       mSSRS1 = tExeSQL.execSQL(tSql);
       System.out.println(tSql);
       if (tExeSQL.mErrors.needDealError()) {
           CError tCError = new CError();
           tCError.moduleName = "MakeXMLBL";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "查询XML数据出错！";

           this.mErrors.addOneError(tCError);

           return false;

       }
       if (mSSRS1.getMaxRow() <= 0) {
           CError tCError = new CError();
           tCError.moduleName = "MakeXMLBL";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "没有符合条件的职级信息！";

           this.mErrors.addOneError(tCError);

           return false;
       }

       mRow = mSSRS1.getMaxRow();
       return true;
   }

   private boolean getManageName() {

       String sql = "select name from ldcom where comcode='" + mManageCom +
                    "'";

       SSRS tSSRS = new SSRS();

       ExeSQL tExeSQL = new ExeSQL();

       tSSRS = tExeSQL.execSQL(sql);

       if (tExeSQL.mErrors.needDealError()) {

           this.mErrors.addOneError("销售单位不存在！");

           return false;

       }

       if (mManageCom.equals("86")) {
           this.mManageName = "";
       } else {
           this.mManageName = tSSRS.GetText(1, 1) + "分公司";
       }

       return true;
   }


   /**
        *
        * @return boolean
   */
    private boolean getReportName()
    {
        if(mManageCom.equals("86"))
        {
            mReportName="人保健康总公司";
        }
        else
        {
            mReportName = "人保健康"+mManageName;
        }
        if(mAgentGroup != null && !mAgentGroup.equals(""))
        {
            String tSql="select name from labranchgroup where agentgroup = '"+mAgentGroup+"'";
            ExeSQL tExeSQL = new ExeSQL();
            String tName = tExeSQL.getOneValue(tSql);
            mReportName = mReportName+tName+"营业部";
        }
        if(mMonth == null || mMonth.equals(""))
        {
            mReportName=mReportName+"\n"+mYear+"年团险销售人力一览表";
        }
        else
        {
            mReportName=mReportName+"\n"+mYear+"年"+mMonth+"月团险销售人力一览表";
        }
        return true;
    }
    /**
     *
     * @return boolean
     */
    private boolean getPrintData() {
        TextTag tTextTag = new TextTag();

        mXmlExport = new XmlExport();
        //设置模版名称               LAGrpAgentComReport
        mXmlExport.createDocument("LAGrpAgentComReport.vts", "printer");

        String tMakeDate = "";
        String tMakeTime = "";



        tMakeDate = mPubFun.getCurrentDate();
        tMakeTime = mPubFun.getCurrentTime();

        if (!getManageName()) {
            return false;
        }
        if(!getReportName())
        {
            return false ;
        }

        tTextTag.add("MakeDate", tMakeDate);
        tTextTag.add("MakeTime", tMakeTime);
        tTextTag.add("tName", mManageName);
        tTextTag.add("RePortName", mReportName);

        System.out.println("121212121212121212121212121212" + tMakeDate);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] title = {"1", "2", "3", "4", "5" ,"6" };

        if (!getListTable()) {
            return false;
        }
       // mListTable.add(title);
        for(int i=0;i<mShowDataList.length;i++)
        {
            mListTable.add(mShowDataList[i]);
        }
        //mListTable.add(mDataList);
        mListTable.setName("ZT");
        System.out.print("111");
        mXmlExport.addListTable(mListTable,mShowDataList[0]);
        System.out.print("121");
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 查询列表显示数据
     * @return boolean
     */
    private boolean getListTable() {

        if (mSSRS1.getMaxRow() > 0) {
            mShowDataList=  new String[mRow+1][mCol];;
            for (int i = 1; i <= mSSRS1.getMaxRow(); i++)
            {
                String tAgentGrade = mSSRS1.GetText(i, 1);
                String tGradeName = mSSRS1.GetText(i, 2);
                int tStartNo = getStartNo(tAgentGrade);

                int tEndNo = getEndNo(tAgentGrade);
                int tAddNo = getAddNo(tAgentGrade);
                int tSubNo = getSubNo(tAgentGrade);

                String tgradeName =  tGradeName+"/"+tAgentGrade;
                mShowDataList[i-1][0] = tgradeName;
                mShowDataList[i-1][1] = String.valueOf(tStartNo);
                mShowDataList[i-1][2] = String.valueOf(tEndNo);
                mShowDataList[i-1][3] = String.valueOf(tAddNo);
                mShowDataList[i-1][4] = String.valueOf(tSubNo);
                mShowDataList[i-1][5] = "" ;
                tSumStartNo = tStartNo+tSumStartNo;
                tSumEndNo =  tEndNo+tSumEndNo;
                tSumAddNo =  tAddNo+tSumAddNo;
                tSumSubNo =  tSubNo+tSumSubNo;
            }
            //计算合计
            int tLength = mShowDataList.length;
            if(!setAddRow(tLength-1))
            {
                this.bulidError("check", "计算合计失败!");
                return false;
            }
            //计算人力占比
        } else {
            CError tCError = new CError();
            tCError.moduleName = "CreateXml";
            tCError.functionName = "creatFile";
            tCError.errorMessage = "没有符合条件的职级信息！";
            this.mErrors.addOneError(tCError);
            return false;
        }
        return true;
    }
    /**
        * 计算合计和占比
        * @return boolean
    */
    private boolean setAddRow(int tLen)
    {
        mShowDataList[tLen][0] = "合计";
        mShowDataList[tLen][1] = String.valueOf(tSumStartNo);
        mShowDataList[tLen][2] = String.valueOf(tSumEndNo);
        mShowDataList[tLen][3] = String.valueOf(tSumAddNo);
        mShowDataList[tLen][4] = String.valueOf(tSumSubNo);
        mShowDataList[tLen][5] = "100";
        int tSumNo =Integer.parseInt(mShowDataList[tLen][2]);
        //计算占比
        for(int i=0;i<tLen;i++)
        {
            int t1=Integer.parseInt(mShowDataList[i][2]);
            mShowDataList[i][5]=String.valueOf(Arith.mul( Arith.div(t1,tSumNo,3),100) ) ;
        }
        return true;
    }

    /**
        * 查询列表显示数据
        * @return boolean
        */
    private int getStartNo(String tAgentGrade)
    {
        int tStartNo;
        String tSql = "select count(*) from laagent a,latree b,labranchgroup c where "
                      +" a.agentcode=b.agentcode and a.agentgroup=c.agentgroup "
                      +" and b.agentgrade = '"+tAgentGrade+"' "
                      +" and a.managecom like '"+mManageCom+"%'"
                      +" and a.employdate<='"+mStartDate+"'    "
                      +" and (a.outworkdate is null or a.outworkdate>'"+mStartDate+"' ) "
                      +" and (c.state is null or c.state<>'1') "
                      ;
        if(mAgentGroup != null && !mAgentGroup.equals(""))
        {
            tSql += " and a.agentgroup = '"+mAgentGroup+"' ";
        }
        ExeSQL tExeSQL = new ExeSQL();
        String tValue = tExeSQL.getOneValue(tSql);
        if(tValue==null || tValue.equals(""))
        {
            tStartNo=0;
        }
        tStartNo=Integer.parseInt(tValue) ;
        return tStartNo;
    }
    /**
        * 查询列表显示数据
        * @return boolean
        */
    private int getEndNo(String tAgentGrade)
    {
        int tStartNo;
        String tSql = "select count(*) from laagent a,latree b,labranchgroup c where"
                      +" a.agentcode=b.agentcode and a.agentgroup=c.agentgroup "
                      +" and b.agentgrade = '"+tAgentGrade+"' "
                      +" and a.managecom like '"+mManageCom+"%'"
                      +" and a.employdate<='"+mEndDate+"'    "
                      +" and (a.outworkdate is null or a.outworkdate>'"+mEndDate+"' ) "
                      +" and (c.state is null or c.state<>'1') "
                      ;
        if(mAgentGroup != null && !mAgentGroup.equals(""))
        {
            tSql += " and a.agentgroup = '"+mAgentGroup+"' ";
        }
        ExeSQL tExeSQL = new ExeSQL();
        String tValue = tExeSQL.getOneValue(tSql);
        if(tValue==null || tValue.equals(""))
        {
            tStartNo=0;
        }
        tStartNo=Integer.parseInt(tValue) ;
        return tStartNo;
    }
    /**
        * 查询列表显示数据
        * @return boolean
        */
    private int getAddNo(String tAgentGrade)
    {
        int tStartNo;
        String tSql = "select count(*) from laagent a,latree b,labranchgroup c where"
                      +" a.agentcode=b.agentcode and a.agentgroup=c.agentgroup "
                      +" and b.agentgrade = '"+tAgentGrade+"' "
                      +" and a.managecom like '"+mManageCom+"%'"
                      +" and a.employdate<='"+mEndDate+"' and a.employdate>='"+mStartDate+"'   "
                      +" and (c.state is null or c.state<>'1') "
                      ;
        if(mAgentGroup != null && !mAgentGroup.equals(""))
        {
            tSql += " and a.agentgroup = '"+mAgentGroup+"' ";
        }
        ExeSQL tExeSQL = new ExeSQL();
        String tValue = tExeSQL.getOneValue(tSql);
        if(tValue==null || tValue.equals(""))
        {
            tStartNo=0;
        }
        tStartNo=Integer.parseInt(tValue) ;
        return tStartNo;
    }
    /**
        * 查询列表显示数据
        * @return boolean
        */
    private int getSubNo(String tAgentGrade)
    {
        int tStartNo;
        String tSql =
                "select count(*) from laagent a,latree b,labranchgroup c where"
                + " a.agentcode=b.agentcode and a.agentgroup=c.agentgroup "
                + " and b.agentgrade = '" + tAgentGrade + "' "
                + " and a.managecom like '" + mManageCom + "%'"
                + " and a.outworkDate>='" + mStartDate + "'  and a.outworkDate<='" + mEndDate + "' "
                + " and (c.state is null or c.state<>'1') "
                ;
        if(mAgentGroup != null && !mAgentGroup.equals(""))
        {
            tSql += " and a.agentgroup = '" + mAgentGroup + "' ";
        }
        ExeSQL tExeSQL = new ExeSQL();
        String tValue = tExeSQL.getOneValue(tSql);
        if (tValue == null || tValue.equals(""))
        {
            tStartNo = 0;
        }
        tStartNo = Integer.parseInt(tValue);
        return tStartNo;
    }






    /**
     * 获取打印所需要的数据
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg) {

        CError tCError = new CError();
        tCError.moduleName = "LAGrpAgentComBL";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;
        this.mErrors.addOneError(tCError);

    }

    /**
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


}
