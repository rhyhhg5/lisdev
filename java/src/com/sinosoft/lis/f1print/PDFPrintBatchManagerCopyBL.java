package com.sinosoft.lis.f1print;

import java.io.*;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.lis.pubfun.SysMaxNoPicch;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import java.net.SocketException;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: sinosoft</p>
 * @author liuli
 * @version 1.0
 */

public class PDFPrintBatchManagerCopyBL {

    private GlobalInput mGlobalInput = new GlobalInput();
    /**打印管理表信息*/
    private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private String mUrl = null;
    private String mName = null;
    private String mflag = null;
    public PDFPrintBatchManagerCopyBL() {

    }

    /**
     * 提交方法
     * @param cInputData VData
     * @param flag String
     * @return boolean
     * 08/11/17 修改：单打也需要生成清单文件，所以单打也调用本类。
     * submitData(VData cInputData, String flag)中flag参数失去意义。
     */
    public boolean submitData(VData cInputData, String flag) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mflag = flag; //batch
        if (!dealData()) {
            return false;
        }

        //上传文件,上传至和单打xml在同一个地址
        if (!sendZip(mUrl+mName+".xml")) {
            return false;
        } else {
            //上传成功后删除
            File cFile = new File(mUrl+mName+".xml");
            cFile.delete(); //删除上传完的文件
        }

        return true;
    }

    /**
     * sendZip
     * 上传文件至指定FTP（清单文件和单打的数据文件在同一个目录）
     * @param string String
     * @param flag String
     * @return boolean
     */
    private boolean sendZip(String cFileUrlName) {
        //登入前置机
         String ip = "";
         String user = "";
         String pwd = "";
         int port = 21;//默认端口为21

         String sql = "select code,codename from LDCODE where codetype='printserver'";
         SSRS tSSRS = new ExeSQL().execSQL(sql);
         if(tSSRS != null){
             for(int m=1;m<=tSSRS.getMaxRow();m++){
                 if(tSSRS.GetText(m,1).equals("IP")){
                     ip = tSSRS.GetText(m,2);
                 }else if(tSSRS.GetText(m,1).equals("UserName")){
                     user = tSSRS.GetText(m,2);
                 }else if(tSSRS.GetText(m,1).equals("Password")){
                     pwd = tSSRS.GetText(m,2);
                 }else if(tSSRS.GetText(m,1).equals("Port")){
                     port = Integer.parseInt(tSSRS.GetText(m,2));
                 }
             }
         }
         FTPTool tFTPTool = new FTPTool(ip, user, pwd, port);
         try
         {
             if (!tFTPTool.loginFTP())
             {
                 System.out.println(tFTPTool.getErrContent(1));
                 return false;
             }
             else
             {
                  if (!tFTPTool.upload(cFileUrlName)) {
                         CError tError = new CError();
                         tError.errorMessage = tFTPTool
                                               .getErrContent(
                                 FTPReplyCodeName.LANGUSGE_CHINESE);
                         buildError("sendZip", tError.errorMessage);
                         System.out.println("上载文件失败!");
                         return false;
                  }

                 tFTPTool.logoutFTP();
             }
         }
         catch (SocketException ex)
         {
             ex.printStackTrace();
             CError tError = new CError();
             tError.errorMessage = tFTPTool
                      .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
              buildError("sendZip",tError.errorMessage);
             System.out.println("上载文件失败，可能是网络异常!");
             return false;
         }
         catch (IOException ex)
         {
             ex.printStackTrace();
             CError tError = new CError();
             tError.errorMessage = tFTPTool
                     .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
             buildError("sendZip",tError.errorMessage);
             System.out.println("上载文件失败，可能是无法写入文件");
             return false;
         }

         return true;

    }

    /**
     * dealData
     * 数据逻辑处理
     * @return boolean
     */
    private boolean dealData() {
        String code = (String)mLOPRTManagerSet.get(1).getCode();//获取本批次的打印单证类型，要求传过来的集合里必须是同一类单证
        System.out.println("获取本批次的打印单证类型，要求传过来的集合里必须是同一类单证");
        ExeSQL exesql = new ExeSQL();
    	String sql1 = "select CHECKGRPCONT(llcd.GrpContNo)"+
    			 	  " from LLClaimDetail llcd where llcd.CaseNo='"+(String)mLOPRTManagerSet.get(1).getOtherNo()+"'";
    	String shebao = exesql.getOneValue(sql1);
    	System.out.println(shebao);
    	String comcode = mGlobalInput.ManageCom;
    	System.out.println(comcode);
    	String comcodeStr="";
        if(comcode!=null && comcode.length()>=6){        
        	comcodeStr=comcode.substring(0, 6);
        	System.out.println(comcodeStr);
        }
        //新余或新余下级机构的机构代码 
    	if(code.equals("lp008") && ("863605".equals(comcodeStr)) && ("Y".equals(shebao))){
    		code="lp008xy";
    	} 
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("JetFormType", code);
        ListTable tFileNameList = new ListTable();
        tFileNameList.setName("filename");
        int size = mLOPRTManagerSet.size();

        //循环调用单打管理类
        for(int i=1;i<=size;i++)
        {
			try {
				LOPRTManagerSchema tLOPRTManagerSchema = mLOPRTManagerSet
						.get(i);
				VData tVData = new VData();
				tVData.add(tLOPRTManagerSchema);
				tVData.add(mGlobalInput);
				PDFPrintManagerCopyBL tPDFPrintManagerBL = new PDFPrintManagerCopyBL();
				if (!tPDFPrintManagerBL.submitData(tVData, mflag)) {
					System.out.println("发生错误了，进来了");
					mErrors.addOneError(tPDFPrintManagerBL.mErrors.getError(0));
					System.out.println("第一个错误："+mErrors.getFirstError());
					if (!mErrors.getError(0).functionName.equals("saveData")) {
						System.out.println("不需要处理的错误："+mErrors.getFirstError());
						continue;
					} else {
						System.out.println("PDFPrintBatchManagerBL报非中断错："
								+ mErrors.getFirstError());
					}
				}
				String[] fileList = new String[1];
				fileList[0] = tPDFPrintManagerBL.getFileName();
				System.out.println("文件铭文："+fileList[0]);
				tFileNameList.add(fileList);
				System.out.println("打印名称为："+fileList[0]);
			} catch (Exception e) {
				// TODO: handle exception
			}            
        }

        getUrlName();//文件地址和文件名
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument(mName);
        xmlexport.addTextTag(texttag);
        xmlexport.addListTable(tFileNameList,new String[1]);
        xmlexport.outputDocumentToFile(mUrl,mName);
        System.out.println("打印出来的目录"+xmlexport.outputString());
        return true;
    }


    /**
     * getUrlName
     * 获取清单getUrlName文件的存放地址，生成文件名字（当前日期＋流水号）
     * @return boolean
     */
    private boolean getUrlName() {
        String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='PDFstrUrl'";//生成文件的存放路径
        mUrl = new ExeSQL().getOneValue(sqlurl);//生成文件的存放路径
        System.out.println("生成文件的存放路径   "+mUrl);//调试用－－－－－
        if(mUrl == null ||mUrl.equals("")){
            buildError("getFileUrlName","获取文件存放路径出错");
            return false;
        }

        //生成 当前日期＋6位流水号的文件名
        String MaxNo = new SysMaxNoPicch().CreateMaxNo("PDFPRINT",PubFun.getCurrentDate2(),6);
        mName = PubFun.getCurrentDate2()+MaxNo;
        System.out.println("生成文件名："+mName);
        if(mName == null || mName.equals("")){
            buildError("getFileUrlName","生成文件名出错");
            return false;
        }
        return true;
    }


    /**
     * getInputData
     * 获取数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        mLOPRTManagerSet = (LOPRTManagerSet) cInputData.getObjectByObjectName(
                "LOPRTManagerSet", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mLOPRTManagerSet.size() <= 0) {
            buildError("getInputData", "没有获取到打印信息！");
            return false;
        }
        if(mGlobalInput.Operator==null ||mGlobalInput.Operator.equals("")){
            buildError("getInputData", "没有获取到操作员的信息！");
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "PDFPrintManagerBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
