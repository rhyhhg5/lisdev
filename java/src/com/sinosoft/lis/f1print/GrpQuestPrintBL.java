package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author cuiwei
 * @version 1.0
 */


import java.text.SimpleDateFormat;
import java.util.Date;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCGrpContDB;

import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LCGrpIssuePolDB;
import com.sinosoft.lis.vschema.LCGrpIssuePolSet;
import com.sinosoft.lis.schema.LCGrpIssuePolSchema;
import com.sinosoft.lis.pubfun.MMap;

public class GrpQuestPrintBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    private MMap map = new MMap();
    //取得的代理人编码
    private String mAgentCode = "";
    private String mGrpContNo = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LCGrpContSet mLCGrpContSet = new LCGrpContSet();
    private LCContPlanDutyParamSchema mLCContPlanDutyParamSchema = new
            LCContPlanDutyParamSchema();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LDComSchema mLDComSchema = new LDComSchema();
    private LCGrpIssuePolSchema mLCGrpIssuePolSchema = new  LCGrpIssuePolSchema();
    private LCGrpIssuePolSet mLCGrpIssuePolSet = new  LCGrpIssuePolSet();
    public GrpQuestPrintBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("CONFIRM") &&
            !cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }

        return true;
    }

    public static void main(String[] args) {
        String d = "2005-6-2";
        d = d.substring(0,
                        d.indexOf("-")) + "年" +
            d.substring(d.indexOf("-") + 1,
                        d.lastIndexOf("-")) +
            "月" + d.substring(
                    d.lastIndexOf("-") + 1) + "日";
            System.out.println(" D : "+d);
            String[] a = d.split("-");
        String c = a[0]+"年"+a[1]+"月"+a[2]+"日";

    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput)cInputData.
                                      getObjectByObjectName(
                                              "GlobalInput", 0)
) ;
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                                              "LOPRTManagerSchema", 0));
        if (mLOPRTManagerSchema == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (mLOPRTManagerSchema.getPrtSeq() == null) {
            buildError("getInputData", "没有得到足够的信息:印刷号不能为空！");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {

        //根据印刷号查询打印队列中的纪录
        String PrtNo = mLOPRTManagerSchema.getPrtSeq(); //打印流水号

        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (tLOPRTManagerDB.getInfo() == false) {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();

        boolean PEFlag = false; //打印体检件部分的判断标志
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setProposalGrpContNo(mLOPRTManagerSchema.getOtherNo()); //发通知的时候把集体合同号码放入OtherNo
        int m, i;
        LCGrpContSet mLCGrpContSet = new LCGrpContSet();
        mLCGrpContSet = tLCGrpContDB.query();
        if (mLCGrpContSet.size() > 1 || mLCGrpContSet.size() == 0) {
            mErrors.copyAllErrors(tLCGrpContDB.mErrors);
            buildError("outputXML", "在取得LCCont的数据时发生错误");
            return false;
        }
        mLCGrpContSchema = mLCGrpContSet.get(1);

        LCGrpIssuePolDB tLCGrpIssuePolDB = new  LCGrpIssuePolDB();
        tLCGrpIssuePolDB.setProposalGrpContNo(mLOPRTManagerSchema.getOtherNo()); //发通知的时候把集体合同号码放入OtherNo
        LCGrpIssuePolSet mLCGrpIssuePolSet = new LCGrpIssuePolSet();
        mLCGrpIssuePolSet = tLCGrpIssuePolDB.executeQuery(
                "select * from LCGrpIssuepol where grpcontno='" +
                mLOPRTManagerSchema.getOtherNo() +
                "' and needprint='Y' and state<>'5' and prtseq='"+mLOPRTManagerSchema.getPrtSeq()+"' "
                            );
        mAgentCode = mLCGrpContSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mAgentCode);
        if (!tLAAgentDB.getInfo()) {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "在取得LAAgent的数据时发生错误");
            return false;
        }
        mLAAgentSchema = tLAAgentDB.getSchema(); //保存代理人信息

        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mGlobalInput.ManageCom);
        if (!tLDComDB.getInfo()) {
            mErrors.copyAllErrors(tLDComDB.mErrors);
            buildError("outputXML", "在取得LDCom的数据时发生错误");
            return false;
        }
        mLDComSchema = tLDComDB.getSchema();//机构信息
        //险种信息
        mGrpContNo = mLCGrpContSchema.getGrpContNo();
        //2-1 查询体检子表
        String[] QuestionTitle = new String[5];
        QuestionTitle[0] = "Index";
        QuestionTitle[1] = "BackObj";
        QuestionTitle[2] = "IssueCont";
        QuestionTitle[3] = "Content";
        QuestionTitle[4] = "Remark";

        ListTable tQuestionInfoTable = new ListTable();
        tQuestionInfoTable.setName("QuestionInfo");
        String strQuestionInfo[] = null;


       ExeSQL q_exesql = new ExeSQL();

        for (int Index = 1; Index <= mLCGrpIssuePolSet.size(); Index++) {


            strQuestionInfo = new String[5];

            strQuestionInfo[0] = Index+"";
            System.out.println("Index : "+Index);
            strQuestionInfo[1] = mLCGrpIssuePolSet.get(Index).getBackObj() == null ? "" :
                                 mLCGrpIssuePolSet.get(Index).getBackObj();
            strQuestionInfo[2] = mLCGrpIssuePolSet.get(Index).getIssueCont() == null ? "" :
                                 mLCGrpIssuePolSet.get(Index).getIssueCont();
            strQuestionInfo[3] = "";
            strQuestionInfo[4] = "";


            tQuestionInfoTable.add(strQuestionInfo);

        }

        //其它模版上单独不成块的信息
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("GrpQuest.vts", "printer"); //最好紧接着就初始化xml文档
        //生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();

        //模版自上而下的元素
//        if (mLCGrpContSchema.getPayMode() == null) {
//            xmlexport.addDisplayControl("displaymoney");
//        } else {
//            if (mLCGrpContSchema.getPayMode().equals("4")) {
//                xmlexport.addDisplayControl("displaybank");
//            } else {
//                xmlexport.addDisplayControl("displaymoney");
//            }
//        }
        String sql4 =
                " select grpaddress,grpzipcode,linkman1 from LCGrpAddress where customerno='"
                + mLCGrpContSchema.getAppntNo() + "'";
        SSRS GrpInfoSSRS = null;
        String PostalAddress = "";
        String AppntZipcode = "";
        String LinkMan1="";
        try {
            GrpInfoSSRS = q_exesql.execSQL(sql4);
            PostalAddress = GrpInfoSSRS.GetText(1, 1);
            AppntZipcode = GrpInfoSSRS.GetText(1, 2);
            LinkMan1=GrpInfoSSRS.GetText(1, 3);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //int prtNo = Integer.parseInt(PubFun1.CreateMaxNo("GrpFirstPay",2))+1;
        texttag.add("XI_ManageCom", mLCGrpContSchema.getManageCom());
        texttag.add("BarCode1", mLOPRTManagerSchema.getPrtSeq());
        texttag.add("BarCodeParam1",
                "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

        texttag.add("BarCode1", mLOPRTManagerSchema.getPrtSeq());
        texttag.add("BarCodeParam1",
                    "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("XI_AppntNo", mLCGrpContSchema.getAppntNo());
        texttag.add("AppntName", mLCGrpContSchema.getGrpName()); //单位名称
        texttag.add("AppntAddr", PostalAddress); //联系地址
        texttag.add("AppntZipcode", AppntZipcode); //邮政编码

        texttag.add("HandlerName", LinkMan1); //收件人
        if (mLCGrpContSchema.getPolApplyDate() != null) {
            String[] ApplyDate = mLCGrpContSchema.getPolApplyDate().split("-");
            texttag.add("ApplyDate",
                        ApplyDate[0] + "年" + ApplyDate[1] + "月" + ApplyDate[2] +
                        "日"); //投保经办人
        } else {
            String[] ApplyDate = mLCGrpContSchema.getMakeDate().split("-");
            texttag.add("ApplyDate",  ApplyDate[0] + "年" + ApplyDate[1] + "月" + ApplyDate[2] +
                        "日"); //投保经办人
        }
        texttag.add("AgentName", mLAAgentSchema.getName()); //代理人姓名
        texttag.add("AgentMobile", mLAAgentSchema.getPhone()); //代理人电话
        texttag.add("PrtNo", mLCGrpContSchema.getPrtNo()); //印刷号
        texttag.add("Letterservicename", tLDComDB.getLetterServiceName()); //印刷号
        texttag.add("Address", tLDComDB.getLetterServicePostAddress()); //印刷号
        texttag.add("ZipCode", tLDComDB.getZipCode()); //印刷号
        texttag.add("Fax", tLDComDB.getFax()); //印刷号
        texttag.add("Phone", tLDComDB.getPhone()); //印刷号
        texttag.add("ServicePhone", tLDComDB.getPhone()); //印刷号


        //缴费日期与险种相关，把所有的险种和险种对应的交至日期
        String Today = PubFun.getCurrentDate();
        String payToDateSql = "select date('" + Today + "')+15 day from dual";
        Today = q_exesql.getOneValue(payToDateSql);
        String[] a = Today.split("-");
        texttag.add("PayToDate",a[0] + "年" + a[1] + "月" + a[2] +
                        "日");
//                    Today.substring(0,
//                                    Today.indexOf("-")) + "年" +
//                    Today.substring(Today.indexOf("-") + 1,
//                                    Today.lastIndexOf("-")) +
//                    "月" + Today.substring(
//                            Today.lastIndexOf("-") + 1) + "日");
        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
        texttag.add("Today", df.format(new Date()));

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        //保存体检信息

            xmlexport.addListTable(tQuestionInfoTable, QuestionTitle); //保存体检信息


        xmlexport.outputDocumentToFile("D:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }


}
