package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.taskservice.YBKMailTask;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


public class YBKPrintSendBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;
	
	private String mContNo = "";

	//业务处理相关变量
	
	private MMap mMap = new MMap();
	
	public YBKPrintSendBL(){
		
	}
	
	public boolean submitData(VData cInputData) {
		
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCProjectMonthInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败LCProjectCancelBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		
//		保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			System.out.print("保存数据返回");
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LCProjectCancelBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	

	public VData getResult() {
		this.mResult.clear();
		return mResult;
	}

	
	//获得输出数据
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mContNo = (String)tTransferData.getValueByName("ContNo");
		if (mContNo == null || "".equals(mContNo)){
			CError tError = new CError();
			tError.moduleName = "YBKPrintSendBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保单号码异常！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	//  业务处理
	private boolean dealData() {
		YBKMailTask tYBKMailTask = new YBKMailTask(mContNo);
		tYBKMailTask.run();
		String tSendFlag = tYBKMailTask.getSendFlag();
		if("2".equals(tSendFlag)||"".equals(tSendFlag)||tSendFlag==null){
			CError tError = new CError();
			tError.moduleName = "YBKPrintSendBL";
			tError.functionName = "prepareData";
			tError.errorMessage = tYBKMailTask.getRemark();
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "YBKPrintSendBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
}
