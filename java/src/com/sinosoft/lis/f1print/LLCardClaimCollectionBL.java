package com.sinosoft.lis.f1print;

/**
 * <p>
 * Title: LLBranchCaseReportBL
 * </p>
 * <p>
 * Description: 承保赔付汇总表
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003
 * </p>
 * <p>
 * Company: sinosoft
 * </p>
 * 
 * @author MN
 * @version 1.0
 */
// import com.sinosoft.lis.db.*;
// import com.sinosoft.lis.vdb.*;
// import com.sinosoft.lis.schema.*;
// import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
// import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;

// import java.util.*;
// import java.sql.*;

public class LLCardClaimCollectionBL {
	public LLCardClaimCollectionBL() {
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 全局变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mCvaliDateS = ""; // 生效起期

	private String mCvaliDateE = ""; // 生效止期

	private String mEndCaseDateS = ""; // 结案起期

	private String mEndCaseDateE = ""; // 结案止期

	private String mActiveDateS = ""; // 激活起期

	private String mActiveDateE = ""; // 激活止期

	private String mJieSuanDateS = ""; // 结算起期

	private String mJieSuanDateE = ""; // 结算止期

	private String mManageCom = "";

	private String mManageComName = "";
	

	private String mOperator = "";

	private DecimalFormat mDF = new DecimalFormat("0.##");

	private XmlExport mXmlExport = null;

	// private String[] mDataList = null;
	private ListTable mListTable = new ListTable();

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mFileNameB = "";

	private TransferData mTransferData = new TransferData();

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行数据查询
		if (!queryData()) {
			return false;
		} else {
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("tFileNameB", mFileNameB);
			tTransferData.setNameAndValue("tMakeDate", mCurrentDate);
			tTransferData.setNameAndValue("tOperator", mOperator);
			LLPrintSave tLLPrintSave = new LLPrintSave();
			VData tVData = new VData();
			tVData.addElement(tTransferData);
			if (!tLLPrintSave.submitData(tVData, "")) {
				System.out.println("LLPrintSave is fault!");
				return false;
			}
		}

		return true;
	}

	/**
	 * 按照重点业务类型统计
	 * 
	 * @param datetype
	 *            int
	 * @return boolean
	 */
	private boolean getDataList(int datetype) {
		String getPeopleNum = "";
		String sql = "";
		String sql1 = "";
		
		int tSumPeoples = 0;// 承保人数合计
		double tSumPrem = 0.00;// 承保保费合计
		int tSumCaseNum = 0;// 赔案件数
		double tSumRealpay = 0.00;// 结案赔款
		// String dateCondition=getDateCondition(datetype);

		if (mManageCom == null || mManageCom.equals("")) {
			buildError("getDataList", "机构编码缺失！");
			return false;
		}

		ExeSQL ttExeSQL = new ExeSQL();
		
		RSWrapper rsWrapper = new RSWrapper();
		if (datetype == 0) {
			
			// 结算日期时
		sql = "select  Y.comcode,Y.name ,Z.riskwrapcode,Z.wrapname,sum(Z.peoples2),sum(Z.prem),sum(Z.count),sum(Z.realpay) " +
		"from (select m.comcode comcode,m.name name ,m.comgrade comgrade from ldcom m where m.comcode like '"
		+ mManageCom
		+ "%'  and m.sign='1' ";
    if (mManageCom.length() == 8) {
	  sql = sql + " and m.comgrade='03'";
   } else {
	sql = sql + " and m.comgrade in ('02','03')";
} 
    sql = sql + ") as Y right join "
    
		+ "(select X.managecom managecom,(select name from ldcom where comcode=X.managecom) name,X.riskwrapcode riskwrapcode,X.wrapname wrapname,sum(X.peoples2)peoples2,sum(X.prem)prem,sum(X.count)count,sum(X.realpay)realpay "
		+ "from ("
		+"select f.managecom managecom,a.riskwrapcode riskwrapcode,a.wrapname wrapname,1 peoples2,d.prem prem,"
		+"(select coalesce(count(distinct m.caseno),0) from llcase m,llclaimdetail n where m.caseno=n.caseno and m.rgtstate in ('09','11','12') and n.contno=d.cardno  and m.endcasedate >= '"
		+ mEndCaseDateS + "'  and  m.endcasedate<='"+mEndCaseDateE+"' ) count,"
		+"(select coalesce(sum(n.realpay),0) from llcase m,llclaimdetail n where m.caseno=n.caseno and m.rgtstate in ('09','11','12') and n.contno=d.cardno   and m.endcasedate >='"
		 +mEndCaseDateS+"' and  m.endcasedate<='" + mEndCaseDateE + "') realpay "
		+"from ldwrap a, lmcardrisk c,licertify d,lcgrpcont f "
		+"where c.certifycode=d.certifycode  "
		+"and c.riskcode = a.riskwrapcode and "
		+"f.managecom like '" +mManageCom + "%'" 
		+"and f.signdate>='"
		+mJieSuanDateS + "' and f.signdate<='"
		+mJieSuanDateE+"' and d.grpcontno = f.grpcontno) AS X "
		+"group by X.managecom,X.riskwrapcode,X.wrapname "
		+ " ) as Z  on Y.comcode=substr(Z.managecom,1,length(trim(Y.comcode)))  "
		+ " group by Y.comcode,Y.comgrade ,Y.name,Z.riskwrapcode,Z.wrapname "
		
		+ " order by substr(Y.comcode,1,4) ,Y.comgrade desc,Y.comcode with ur";
    
    sql1 = "select  Y.comcode,Y.name ,Z.riskwrapcode,Z.wrapname,sum(Z.peoples2),sum(Z.prem),sum(Z.count),sum(Z.realpay) " +
	"from (select m.comcode comcode,m.name name ,m.comgrade comgrade from ldcom m where m.comcode = '86'"
	+" and m.sign='1' and m.comgrade ='01' "
	+ ") as Y right join "
	+ "(select X.managecom managecom,(select name from ldcom where comcode=X.managecom) name,X.riskwrapcode riskwrapcode,X.wrapname wrapname,sum(X.peoples2)peoples2,sum(X.prem)prem,sum(X.count)count,sum(X.realpay)realpay "
	+ "from ("
	+"select f.managecom managecom,a.riskwrapcode riskwrapcode,a.wrapname wrapname,1 peoples2,d.prem prem,"
	+"(select coalesce(count(distinct m.caseno),0) from llcase m,llclaimdetail n where m.caseno=n.caseno and m.rgtstate in ('09','11','12') and n.contno=d.cardno  and m.endcasedate >= '"
	+ mEndCaseDateS + "'  and  m.endcasedate<='"+mEndCaseDateE+"' ) count,"
	+"(select coalesce(sum(n.realpay),0) from llcase m,llclaimdetail n where m.caseno=n.caseno and m.rgtstate in ('09','11','12') and n.contno=d.cardno   and m.endcasedate >='"
	 +mEndCaseDateS+"' and  m.endcasedate<='" + mEndCaseDateE + "') realpay "
	+"from ldwrap a, lmcardrisk c,licertify d,lcgrpcont f "
	+"where c.certifycode=d.certifycode  "
	+"and c.riskcode = a.riskwrapcode and "
	+"f.managecom like '" +mManageCom + "%'" 
	+"and f.signdate>='"
	+mJieSuanDateS + "' and f.signdate<='"
	+mJieSuanDateE+"' and d.grpcontno = f.grpcontno) AS X "
	+"group by X.managecom,X.riskwrapcode,X.wrapname "
	+ " ) as Z  on Y.comcode=substr(Z.managecom,1,length(trim(Y.comcode)))  "
	+ " group by Y.comcode,Y.comgrade ,Y.name,Z.riskwrapcode,Z.wrapname "
	
	+ " with ur";
		System.out.println("结算日期SQL:"+sql);
		System.out.println("结算日期SQL1:"+sql1);
		if (!rsWrapper.prepareData(null, sql)) {
			System.out.println("数据准备失败! ");
			return false;
		}
		try {
			SSRS tSSRS = new SSRS();
			tSSRS = rsWrapper.getSSRS();
			if (tSSRS == null) {
				buildError("getDataList", "没有查询到数据！");
				return false;
			}
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

				String ListInfo[] = { "", "", "","",  "0", "0", "0", "0" };

				ListInfo[0] = tSSRS.GetText(i, 1);// 机构号
				ListInfo[1] = tSSRS.GetText(i, 2);// 机构名称
				
				ListInfo[2] = tSSRS.GetText(i, 3);// 
			    ListInfo[3] = tSSRS.GetText(i, 4);//卡折套餐名称
//			    tSumPeoples += Integer.parseInt(ListInfo[3]);
			    ListInfo[4] = tSSRS.GetText(i, 5);// 承保人数
//			    tSumPrem += Arith.round(
//				Double.parseDouble(ListInfo[4]), 2);
			    ListInfo[5] = tSSRS.GetText(i, 6);// 承保保费
//				tSumCaseNum += Integer.parseInt(ListInfo[5]);// 赔案件数合计
				ListInfo[6] = tSSRS.GetText(i, 7);// 结案件数
//				tSumRealpay += Arith.round(Double.parseDouble(ListInfo[6]),
//						2);// 结案赔款合计
				ListInfo[7] = tSSRS.GetText(i, 8);//结案赔款
				mListTable.add(ListInfo);
	}
//			if (tSSRS.getMaxRow() > 0) {
//				String SumInfo[] = new String[9];
//				SumInfo[0] = mManageCom;
//				SumInfo[1] = mManageComName;
//				SumInfo[2] = "合计";
//				
//				
//				SumInfo[3] = mDF.format(tSumPeoples);
//				SumInfo[4] = mDF.format(tSumPrem);
//				SumInfo[5] = mDF.format(tSumCaseNum);
//				SumInfo[6] = mDF.format(tSumRealpay);
//				mListTable.add(SumInfo);
//			}
 } catch (Exception ex) {
ex.printStackTrace();
} finally {
rsWrapper.close();
}
if(mManageCom.length() == 2) {
  if (!rsWrapper.prepareData(null, sql1)) {
		System.out.println("数据准备失败! ");
		return false;
	 }
	 try {
		SSRS tSSRS = new SSRS();
		tSSRS = rsWrapper.getSSRS();
		if (tSSRS == null) {
			buildError("getDataList", "没有查询到数据！");
			return false;
		}
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

			String ListInfo1[] = { "", "", "","",  "0", "0", "0", "0" };

			ListInfo1[0] = tSSRS.GetText(i, 1);// 机构号
			ListInfo1[1] = tSSRS.GetText(i, 2);// 机构名称
			
			ListInfo1[2] = tSSRS.GetText(i, 3);// 
		    ListInfo1[3] = tSSRS.GetText(i, 4);//卡折套餐名称
//		    tSumPeoples += Integer.parseInt(ListInfo[3]);
		    ListInfo1[4] = tSSRS.GetText(i, 5);// 承保人数
//		    tSumPrem += Arith.round(
//			Double.parseDouble(ListInfo[4]), 2);
		    ListInfo1[5] = tSSRS.GetText(i, 6);// 承保保费
//			tSumCaseNum += Integer.parseInt(ListInfo[5]);// 赔案件数合计
			ListInfo1[6] = tSSRS.GetText(i, 7);// 结案件数
//			tSumRealpay += Arith.round(Double.parseDouble(ListInfo[6]),
//					2);// 结案赔款合计
			ListInfo1[7] = tSSRS.GetText(i, 8);//结案赔款
			mListTable.add(ListInfo1);
	}

	} catch (Exception ex) {
	ex.printStackTrace();
	} finally {
	rsWrapper.close();
	}
}

			} else if (datetype == 1) {
				// 激活日期时

//			
				sql = "select  Y.comcode,Y.name ,Z.riskwrapcode ,Z.wrapname,sum(Z.peoples2),sum(Z.prem),sum(Z.count),sum(Z.realpay) " +
				"from (select m.comcode comcode,m.name name ,m.comgrade comgrade from ldcom m where m.comcode like '"
				+ mManageCom
				+ "%'  and m.sign='1' ";
		    if (mManageCom.length() == 8) {
			  sql = sql + " and m.comgrade='03'";
		   } else {
			sql = sql + " and m.comgrade in ('02','03')";
		} 
		    sql = sql + ") as Y right join "
		    
				    + "(select X.managecom managecom,(select name from ldcom where comcode=X.managecom) name,X.riskwrapcode riskwrapcode,X.wrapname wrapname,sum(X.peoples2)peoples2,sum(X.prem)prem,sum(X.count)count,sum(X.realpay)realpay "
					+ "from ("
					+"select d.managecom managecom,a.riskwrapcode riskwrapcode,a.wrapname wrapname,1 peoples2,coalesce(d.prem,0) prem,"
					+"(select coalesce(count(distinct m.caseno),0) from llcase m,llclaimdetail n where m.caseno=n.caseno and m.rgtstate in ('09','11','12') and n.contno=d.cardno   and m.endcasedate >= '"
					+ mEndCaseDateS + "' and  m.endcasedate<='"+mEndCaseDateE+"' ) count,"
					+"(select coalesce(sum(n.realpay),0) from llcase m,llclaimdetail n where m.caseno=n.caseno and m.rgtstate in ('09','11','12')and n.contno=d.cardno and m.endcasedate >='"
					 +mEndCaseDateS+"' and  m.endcasedate<='" + mEndCaseDateE + "') realpay "
					+"from ldwrap a, lmcardrisk c,licertify d,licardactiveinfolist g "
					+"where c.certifycode=d.certifycode and d.cardno=g.cardno and d.cardno=g.cardno  and g.sequenceno = '1'  "
					+"and  c.riskcode = a.riskwrapcode and d.managecom like '" +mManageCom + "%' " 
					+"and g.activedate>='"
					+mActiveDateS + "' and g.activedate<='"
					+mActiveDateE+"') AS X "
					+"group by X.managecom,X.riskwrapcode,X.wrapname "
					+ " ) as Z  on Y.comcode=substr(Z.managecom,1,length(trim(Y.comcode)))  "
					+ " group by Y.comcode,Y.comgrade ,Y.name,Z.riskwrapcode,Z.wrapname "
					+ " order by substr(Y.comcode,1,4) ,Y.comgrade desc,Y.comcode with ur";
					
		    sql1 = "select  Y.comcode,Y.name ,Z.riskwrapcode,Z.wrapname,sum(Z.peoples2),sum(Z.prem),sum(Z.count),sum(Z.realpay) " +
			"from (select m.comcode comcode,m.name name ,m.comgrade comgrade from ldcom m where m.comcode = '86'"
			+" and m.sign='1' and m.comgrade ='01' "
			+ ") as Y right join "
			+ "(select X.managecom managecom,(select name from ldcom where comcode=X.managecom) name,X.riskwrapcode riskwrapcode,X.wrapname wrapname,sum(X.peoples2)peoples2,sum(X.prem)prem,sum(X.count)count,sum(X.realpay)realpay "
			+ "from ("
			+"select d.managecom managecom,a.riskwrapcode riskwrapcode,a.wrapname wrapname,1 peoples2,coalesce(d.prem,0) prem,"
			+"(select coalesce(count(distinct m.caseno),0) from llcase m,llclaimdetail n where m.caseno=n.caseno and m.rgtstate in ('09','11','12') and n.contno=g.cardno   and m.endcasedate >= '"
			+ mEndCaseDateS + "' and  m.endcasedate<='"+mEndCaseDateE+"' ) count,"
			+"(select coalesce(sum(n.realpay),0) from llcase m,llclaimdetail n where m.caseno=n.caseno and m.rgtstate in ('09','11','12')and n.contno=g.cardno and m.endcasedate >='"
			 +mEndCaseDateS+"' and  m.endcasedate<='" + mEndCaseDateE + "') realpay "
			+"from ldwrap a, lmcardrisk c,licertify d,licardactiveinfolist g "
			+"where c.certifycode=d.certifycode and d.cardno=g.cardno  and g.sequenceno = '1'   "
			+"and  c.riskcode = a.riskwrapcode and d.managecom like '" +mManageCom + "%' " 
			+"and g.activedate>='"
			+mActiveDateS + "' and g.activedate<='"
			+mActiveDateE+"') AS X "
			+"group by X.managecom,X.riskwrapcode,X.wrapname "
			+ " ) as Z  on Y.comcode=substr(Z.managecom,1,length(trim(Y.comcode)))  "
			+ " group by Y.comcode,Y.comgrade ,Y.name,Z.riskwrapcode,Z.wrapname "
			+"with ur";
				System.out.println("激活日期SQL:"+sql);
				System.out.println("激活日期SQL:"+sql1);
					if (!rsWrapper.prepareData(null, sql)) {
						System.out.println("数据准备失败! ");
						return false;
					}
					try {
						SSRS tSSRS = new SSRS();
						tSSRS = rsWrapper.getSSRS();
						if (tSSRS == null) {
							buildError("getDataList", "没有查询到数据！");
							return false;
						}
						for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

							String ListInfo[] = { "", "", "","",  "0", "0", "0", "0" };

							ListInfo[0] = tSSRS.GetText(i, 1);// 机构号
							ListInfo[1] = tSSRS.GetText(i, 2);// 机构名称
							
							ListInfo[2] = tSSRS.GetText(i, 3);// 
						    ListInfo[3] = tSSRS.GetText(i, 4);//卡折套餐名称
//						    tSumPeoples += Integer.parseInt(ListInfo[3]);
						    ListInfo[4] = tSSRS.GetText(i, 5);// 承保人数
//						    tSumPrem += Arith.round(
//							Double.parseDouble(ListInfo[4]), 2);
						    ListInfo[5] = tSSRS.GetText(i, 6);// 承保保费
//							tSumCaseNum += Integer.parseInt(ListInfo[5]);// 赔案件数合计
							ListInfo[6] = tSSRS.GetText(i, 7);// 结案件数
//							tSumRealpay += Arith.round(Double.parseDouble(ListInfo[6]),
//									2);// 结案赔款合计
							ListInfo[7] = tSSRS.GetText(i, 8);//结案赔款
							
							mListTable.add(ListInfo);
				}
//						
			 } catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			rsWrapper.close();
		}
		
		if(mManageCom.length() == 2) {
			  if (!rsWrapper.prepareData(null, sql1)) {
					System.out.println("数据准备失败! ");
					return false;
				 }
				 try {
					SSRS tSSRS = new SSRS();
					tSSRS = rsWrapper.getSSRS();
					if (tSSRS == null) {
						buildError("getDataList", "没有查询到数据！");
						return false;
					}
					for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

						String ListInfo1[] = { "", "", "","",  "0", "0", "0", "0" };

						ListInfo1[0] = tSSRS.GetText(i, 1);// 机构号
						ListInfo1[1] = tSSRS.GetText(i, 2);// 机构名称
						
						ListInfo1[2] = tSSRS.GetText(i, 3);// 
					    ListInfo1[3] = tSSRS.GetText(i, 4);//卡折套餐名称
//					    tSumPeoples += Integer.parseInt(ListInfo[3]);
					    ListInfo1[4] = tSSRS.GetText(i, 5);// 承保人数
//					    tSumPrem += Arith.round(
//						Double.parseDouble(ListInfo[4]), 2);
					    ListInfo1[5] = tSSRS.GetText(i, 6);// 承保保费
//						tSumCaseNum += Integer.parseInt(ListInfo[5]);// 赔案件数合计
						ListInfo1[6] = tSSRS.GetText(i, 7);// 结案件数
//						tSumRealpay += Arith.round(Double.parseDouble(ListInfo[6]),
//								2);// 结案赔款合计
						ListInfo1[7] = tSSRS.GetText(i, 8);//结案赔款
						mListTable.add(ListInfo1);
				}

				} catch (Exception ex) {
				ex.printStackTrace();
				} finally {
				rsWrapper.close();
				}
		  }
			}  else if (datetype == 2) {
					// 生效日期时
			
					sql = "select  Y.comcode,Y.name ,Z.riskwrapcode,Z.wrapname,sum(Z.peoples2),sum(Z.prem),sum(Z.count),sum(Z.realpay) " +
					"from (select m.comcode comcode,m.name name ,m.comgrade comgrade from ldcom m where m.comcode like '"
					+ mManageCom
					+ "%'  and m.sign='1' ";
			    if (mManageCom.length() == 8) {
				  sql = sql + " and m.comgrade='03'";
			   } else {
				sql = sql + " and m.comgrade in ('02','03')";
			} 
			    sql = sql + ") as Y right join "
			    
					+ "(select X.managecom managecom,(select name from ldcom where comcode=X.managecom) name,X.riskwrapcode riskwrapcode,X.wrapname wrapname,sum(X.peoples2)peoples2,sum(X.prem)prem,sum(X.count)count,sum(X.realpay)realpay "
					+ "from ("
					+"select f.managecom managecom,a.riskwrapcode riskwrapcode,a.wrapname wrapname,1 peoples2,d.prem prem,"
					+"(select coalesce(count(distinct m.caseno),0) from llcase m,llclaimdetail n where m.caseno=n.caseno and m.rgtstate in ('09','11','12') and n.contno=g.cardno  and m.endcasedate >= '"
					+ mEndCaseDateS + "'  and  m.endcasedate<='"+mEndCaseDateE+"' ) count,"
					+"(select coalesce(sum(n.realpay),0) from llcase m,llclaimdetail n where m.caseno=n.caseno and m.rgtstate in ('09','11','12') and n.contno=g.cardno   and m.endcasedate >='"
					 +mEndCaseDateS+"' and  m.endcasedate<='" + mEndCaseDateE + "') realpay "
					+"from ldwrap a, lmcardrisk c,licertify d,licardactiveinfolist g ,lcgrpcont f "
					+"where c.certifycode=d.certifycode and d.cardno=g.cardno  "
					+"and c.riskcode = a.riskwrapcode and "
					+"f.managecom like '" +mManageCom + "%'" 
					+"and g.cvalidate>='"
					+mCvaliDateS + "' and g.cvalidate<='"
					+mCvaliDateE+"' and d.grpcontno = f.grpcontno) AS X "
					+"group by X.managecom,X.riskwrapcode,X.wrapname "
					+ " ) as Z  on Y.comcode=substr(Z.managecom,1,length(trim(Y.comcode)))  "
					+ " group by Y.comcode,Y.comgrade ,Y.name,Z.riskwrapcode,Z.wrapname "
					
					+ " order by substr(Y.comcode,1,4) ,Y.comgrade desc,Y.comcode with ur";
			    sql1 = "select  Y.comcode,Y.name ,Z.riskwrapcode,Z.wrapname,sum(Z.peoples2),sum(Z.prem),sum(Z.count),sum(Z.realpay) " +
				"from (select m.comcode comcode,m.name name ,m.comgrade comgrade from ldcom m where m.comcode = '86'"
				+" and m.sign='1' and m.comgrade ='01' "
				+ ") as Y right join "
				+ "(select X.managecom managecom,(select name from ldcom where comcode=X.managecom) name,X.riskwrapcode riskwrapcode,X.wrapname wrapname,sum(X.peoples2)peoples2,sum(X.prem)prem,sum(X.count)count,sum(X.realpay)realpay "
				+ "from ("
				+"select f.managecom managecom,a.riskwrapcode riskwrapcode,a.wrapname wrapname,1 peoples2,d.prem prem,"
				+"(select coalesce(count(distinct m.caseno),0) from llcase m,llclaimdetail n where m.caseno=n.caseno and m.rgtstate in ('09','11','12') and n.contno=g.cardno  and m.endcasedate >= '"
				+ mEndCaseDateS + "'  and  m.endcasedate<='"+mEndCaseDateE+"' ) count,"
				+"(select coalesce(sum(n.realpay),0) from llcase m,llclaimdetail n where m.caseno=n.caseno and m.rgtstate in ('09','11','12') and n.contno=g.cardno   and m.endcasedate >='"
				 +mEndCaseDateS+"' and  m.endcasedate<='" + mEndCaseDateE + "') realpay "
				+"from ldwrap a, lmcardrisk c,licertify d,licardactiveinfolist g ,lcgrpcont f "
				+"where c.certifycode=d.certifycode and d.cardno=g.cardno  "
				+"and c.riskcode = a.riskwrapcode and "
				+"f.managecom like '" +mManageCom + "%'" 
				+"and g.cvalidate>='"
				+mCvaliDateS + "' and g.cvalidate<='"
				+mCvaliDateE+"' and d.grpcontno = f.grpcontno) AS X "
				+"group by X.managecom,X.riskwrapcode,X.wrapname "
				+ " ) as Z  on Y.comcode=substr(Z.managecom,1,length(trim(Y.comcode)))  "
				+ " group by Y.comcode,Y.comgrade ,Y.name,Z.riskwrapcode,Z.wrapname "
				
				+ " with ur";
					
					System.out.println("生效日期SQL:"+sql);
					System.out.println("生效日期SQL:"+sql1);
					if (!rsWrapper.prepareData(null, sql)) {
						System.out.println("数据准备失败! ");
						return false;
					}
					try {
						SSRS tSSRS = new SSRS();
						tSSRS = rsWrapper.getSSRS();
						if (tSSRS == null) {
							buildError("getDataList", "没有查询到数据！");
							return false;
						}
						System.out.println("tSSRS.getMaxRow()" + tSSRS.getMaxRow());
						for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

							String ListInfo[] = { "", "", "","",  "0", "0", "0", "0" };

							ListInfo[0] = tSSRS.GetText(i, 1);// 机构号
							ListInfo[1] = tSSRS.GetText(i, 2);// 机构名称
							
							ListInfo[2] = tSSRS.GetText(i, 3);// 卡折套餐代码
						    ListInfo[3] = tSSRS.GetText(i, 4);//卡折套餐名称
//						    tSumPeoples += Integer.parseInt(ListInfo[3]);
						    ListInfo[4] = tSSRS.GetText(i, 5);// 承保人数
//						    tSumPrem += Arith.round(
//							Double.parseDouble(ListInfo[4]), 2);
						    ListInfo[5] = tSSRS.GetText(i, 6);// 承保保费
//							tSumCaseNum += Integer.parseInt(ListInfo[5]);// 赔案件数合计
							ListInfo[6] = tSSRS.GetText(i, 7);// 结案件数
//							tSumRealpay += Arith.round(Double.parseDouble(ListInfo[6]),
//									2);// 结案赔款合计
							ListInfo[7] = tSSRS.GetText(i, 8);//结案赔款
							
							mListTable.add(ListInfo);
				}
//						if (tSSRS.getMaxRow() > 0) {
//							String SumInfo[] = new String[9];
//							SumInfo[0] = mManageCom;
//							SumInfo[1] = mManageComName;
//							SumInfo[2] = "合计";
//							
//							
//							SumInfo[3] = mDF.format(tSumPeoples);
//							SumInfo[4] = mDF.format(tSumPrem);
//							SumInfo[5] = mDF.format(tSumCaseNum);
//							SumInfo[6] = mDF.format(tSumRealpay);
//							mListTable.add(SumInfo);
//						}
			 } catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			rsWrapper.close();
		}
		
		if(mManageCom.length() == 2) {
			  if (!rsWrapper.prepareData(null, sql1)) {
					System.out.println("数据准备失败! ");
					return false;
				 }
				 try {
					SSRS tSSRS = new SSRS();
					tSSRS = rsWrapper.getSSRS();
					if (tSSRS == null) {
						buildError("getDataList", "没有查询到数据！");
						return false;
					}
					for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

						String ListInfo1[] = { "", "", "","",  "0", "0", "0", "0" };

						ListInfo1[0] = tSSRS.GetText(i, 1);// 机构号
						ListInfo1[1] = tSSRS.GetText(i, 2);// 机构名称
						
						ListInfo1[2] = tSSRS.GetText(i, 3);// 
					    ListInfo1[3] = tSSRS.GetText(i, 4);//卡折套餐名称
//					    tSumPeoples += Integer.parseInt(ListInfo[3]);
					    ListInfo1[4] = tSSRS.GetText(i, 5);// 承保人数
//					    tSumPrem += Arith.round(
//						Double.parseDouble(ListInfo[4]), 2);
					    ListInfo1[5] = tSSRS.GetText(i, 6);// 承保保费
//						tSumCaseNum += Integer.parseInt(ListInfo[5]);// 赔案件数合计
						ListInfo1[6] = tSSRS.GetText(i, 7);// 结案件数
//						tSumRealpay += Arith.round(Double.parseDouble(ListInfo[6]),
//								2);// 结案赔款合计
						ListInfo1[7] = tSSRS.GetText(i, 8);//结案赔款
						mListTable.add(ListInfo1);
				}

				} catch (Exception ex) {
				ex.printStackTrace();
				} finally {
				rsWrapper.close();
				}
		  }
	 }
		return true;
	}

	/**
	 * 进行数据查询
	 * 
	 * @return boolean
	 */
	private boolean queryData() {
		String startDate = "";
		String endDate = "";
		int datetype = -1;// 结算日期0、激活日期1、生效日期2

		TextTag tTextTag = new TextTag();
		mXmlExport = new XmlExport();
		// 设置模版名称
		mXmlExport.createDocument("LLCardClaimCollection.vts", "printer");
		System.out.println("获取基本信息：");
		tTextTag.add("ManageComName", mManageComName);
		if (mActiveDateS != null && !"".equals(mActiveDateS)) {
			startDate = "激活日期：" + mActiveDateS;
			endDate = mActiveDateE;
			datetype = 1;
		} else if (mJieSuanDateS != null && !"".equals(mJieSuanDateS)) {
			startDate = "结算日期：" + mJieSuanDateS;
			endDate = mJieSuanDateE;
			datetype = 0;
		} else if (mCvaliDateS != null && !"".equals(mCvaliDateS)) {
			startDate = "生效日期：" + mCvaliDateS;
			endDate = mCvaliDateE;
			datetype = 2;
		} else {
			buildError("queryData", "激活日期、结算日期、生效日期缺失！");
			return false;
		}
		tTextTag.add("CStartDate", startDate);
		tTextTag.add("CEndDate", endDate);
		tTextTag.add("StartDate", mEndCaseDateS);
		tTextTag.add("EndDate", mEndCaseDateE);
		tTextTag.add("operator", mOperator);
		tTextTag.add("MakeDate", mCurrentDate);
		tTextTag.add("MakeDate", mCurrentDate);
		if (tTextTag.size() < 1) {
			return false;
		}

		mXmlExport.addTextTag(tTextTag);

		String[] tTitle = { "COL1", "COL2", "COL3", "COL4", "COL5", "COL6",
				"COL7", "COL8"};
		if (!getDataList(datetype)) {
			return false;
		}

		mListTable.setName("FAULT");
		mXmlExport.addListTable(mListTable, tTitle);
		mXmlExport.outputDocumentToFile("D:\\", "test2");
		this.mResult.clear();

		mResult.addElement(mXmlExport);

		return true;
	}

	/**
	 * 取得传入的数据
	 * 
	 * @return boolean
	 */
	private boolean getInputData(VData mInputData) {
		// 全局变量
		mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mOperator = mGlobalInput.Operator;

		mTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		mManageCom = (String) mTransferData.getValueByName("MngCom"); // 统计机构
		mCvaliDateS = (String) mTransferData.getValueByName("CvaliDateS"); // 生效起期
		mCvaliDateE = (String) mTransferData.getValueByName("CvaliDateE"); // 生效止期
		mEndCaseDateS = (String) mTransferData.getValueByName("EndCaseDateS");// 结案起期
		mEndCaseDateE = (String) mTransferData.getValueByName("EndCaseDateE");// 结案止期
		mActiveDateS = (String) mTransferData.getValueByName("ActiveDateS");// 激活止期
		mActiveDateE = (String) mTransferData.getValueByName("ActiveDateE");// 激活止期
		mJieSuanDateS = (String) mTransferData.getValueByName("JieSuanS");// 结算止期
		mJieSuanDateE = (String) mTransferData.getValueByName("JieSuanE");// 结算止期

		mManageComName = StrTool.unicodeToGBK((String) mTransferData
				.getValueByName("ManageName"));// 机构名称

		if (mEndCaseDateS == null || mEndCaseDateS.equals("")
				|| mEndCaseDateE == null || mEndCaseDateE.equals("")) {
			buildError("getInputData", "结案日期不能为空！");
			return false;
		}

		mFileNameB = (String) mTransferData.getValueByName("tFileNameB");
		return true;
	}

	/**
	 * 追加错误信息
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLClaimCollectionBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	/**
	 * 取得返回处理过的结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		return this.mResult;
	}

	public static void main(String[] args) {

	}

	private void jbInit() throws Exception {
	}
}
