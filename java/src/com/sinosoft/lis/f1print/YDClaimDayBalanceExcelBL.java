 
package com.sinosoft.lis.f1print;

/**
 * <p>Title: YDClaimDayBalanceExcelBL</p>
 * <p>Description:X6-异地理赔日结提数 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft</p>
 * @author : yan
 * @date:2013-09-16
 * @version 1.0
 */
import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;

public class YDClaimDayBalanceExcelBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	private String mDay[] = null; //获取时间
	private GlobalInput mGlobalInput = new GlobalInput(); //全局变量
	private String tOutXmlPath = ""; //文件路径
	
    public YDClaimDayBalanceExcelBL() {
    }
    
    public static void main(String[] args) {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
    	System.out.println("进入...YDClaimDayBalanceExcelBL...");
        if (!cOperate.equals("PRINT") ) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        if (cOperate.equals("PRINT")) { //打印提数
            if (!getPrintData()) {
                return false;
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) { //打印付费
        //全局变量
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        tOutXmlPath = (String)cInputData.get(2);
        
        System.out.println("X6--打印日期："+mDay[0]+"至"+mDay[1]);
        System.out.println("X6--tOutXmlPath："+tOutXmlPath);
        
        if (mGlobalInput == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (tOutXmlPath == null || tOutXmlPath.equals("")) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "YDClaimDayBalanceExcelBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    /**
     * 获得结果集的记录总数
     * @return
     */
    private int getNumber(){
        int no=5;  //初始打印行数

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
      
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
        tGetSQLFromXML.setParameters("EndDate", mDay[1]);
        
        String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
        //tServerPath= "E:/lisdev/ui/";
       
        // 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
        String[] getTypes = new String[]{
                "LPYDSF",    // 异地理赔日结单 实付
                "YDMQJF",    // 异地满期实付
                "YDBQJF",    // 异地保全给付
                "YDYELQ",    // 异地余额领取
                "YDZSTF",    // 异地暂收退费
                "YDSXF"      // 异地手续费
                };
        
        for(int i=0; i<getTypes.length;i++){
            String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", getTypes[i]);
            tSSRS = tExeSQL.execSQL(msql);
            int maxrow = tSSRS.getMaxRow();
            System.out.println("maxrow----"+maxrow);
            no = no+maxrow;
            
            // 汇总的SQL语句是在明细名字后加个 "Z" 代表汇总
            msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", getTypes[i]+"Z");
            tSSRS = tExeSQL.execSQL(msql);
            int zmaxrow = tSSRS.getMaxRow();
            if(zmaxrow == 0){
                no=no+1;
            }else{
                for(int row = 1; row <= zmaxrow; row ++){   

                    if(tSSRS.GetText(row, 2).equals("")||tSSRS.GetText(row, 2).equals("null")||tSSRS.GetText(row, 2)==null){
                        zmaxrow = 1;
                    }else{
                        
                    }
                }
            }       
            no = no+zmaxrow+1;
        }
        
        System.out.println("tMaxRowNum:" + no);
        
        return no;
    }

    /**
     * 新的打印数据方法
     * @return
     */
    private boolean getPrintData(){
      
    	ExeSQL tExeSQL = new ExeSQL();
    	SSRS tSSRS = new SSRS();
    	String nsql = "select Name from LDCom where ComCode='" + mGlobalInput.ManageCom + "'"; // 管理机构
        tSSRS = tExeSQL.execSQL(nsql);
        String manageCom = tSSRS.GetText(1, 1);
        
        int tMaxRowNum = getNumber();
        
        String[][] mToExcel = new String[tMaxRowNum+10][6];
		mToExcel[0][0] = "异地理赔日结单";
		mToExcel[1][0] = "统计日期：";
		mToExcel[1][1] = mDay[0]+"至"+mDay[1];
		mToExcel[2][0] = "管理机构：";
		mToExcel[2][1] = manageCom;
		mToExcel[4][0] = "类型";
		mToExcel[4][1] = "渠道";
		mToExcel[4][2] = "业务号码";
		mToExcel[4][3] = "保单号码";
		mToExcel[4][4] = "金额";
		mToExcel[4][5] = "对方代码";
		
		int no=5;  //初始打印行数
        
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
        tGetSQLFromXML.setParameters("EndDate", mDay[1]);
        
        String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
        //tServerPath= "E:/lisdev/ui/";
        
        // 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
        String[] getTypes = new String[]{
        		"LPYDSF",    // 异地理赔日结单 实付
        		"YDMQJF",	 // 异地满期实付
        		"YDBQJF",    // 异地保全给付
        		"YDYELQ",    // 异地余额领取
        		"YDZSTF",    // 异地暂收退费
        		"YDSXF"      // 异地手续费
        		};
        
        for(int i=0; i<getTypes.length;i++){
        	String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", getTypes[i]);
        	tSSRS = tExeSQL.execSQL(msql);
        	int maxrow = tSSRS.getMaxRow();
        	System.out.println("maxrow----"+maxrow);
        	for(int row = 1; row <= maxrow; row++){
        		for(int col = 1; col <= 6; col++){
        			if(col == 5){
        				mToExcel[no+row-1][col-1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, col)));
        			}else{
        				mToExcel[no+row-1][col-1] = tSSRS.GetText(row, col);
        			}
        		}
        	}
        	no = no+maxrow;
        	
        	// 汇总的SQL语句是在明细名字后加个 "Z" 代表汇总
        	msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", getTypes[i]+"Z");
        	tSSRS = tExeSQL.execSQL(msql);
        	int zmaxrow = tSSRS.getMaxRow();
        	if(zmaxrow == 0){
        		String [] str = getChineseNameWithPY(getTypes[i]+"Z");
				mToExcel[no][0] = str[0];
    			mToExcel[no][3] = "总计金额：";
				mToExcel[no][4] = str[1];
				no=no+1;
        	}else{
        		for(int row = 1; row <= zmaxrow; row ++){  	
            		System.out.println("汇总值----"+tSSRS.GetText(row, 2));
            		mToExcel[no+row-1][0] = tSSRS.GetText(row, 1);
        			mToExcel[no+row-1][3] = "总计金额：";
        			if(tSSRS.GetText(row, 2).equals("")||tSSRS.GetText(row, 2).equals("null")||tSSRS.GetText(row, 2)==null){
        				mToExcel[no+row-1][4] = "0";
        				zmaxrow = 1;
        			}else{
        				mToExcel[no+row-1][4] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 2)));
        			}	
            	}
        	}     	
        	no = no+zmaxrow+1;
        }
        mToExcel[no+1][0]="制表员：";
        mToExcel[no+1][2]="审核员：";
        
        try{
        	System.out.println("X6--tOutXmlPath:"+tOutXmlPath);
        	WriteToExcel t = new WriteToExcel("");
        	t.createExcelFile();
        	String [] sheetName = {PubFun.getCurrentDate()};
        	t.addSheet(sheetName);
        	t.setData(0, mToExcel);
        	t.write(tOutXmlPath);
        	System.out.println("生成X6文件完成");
        }catch (Exception e) {
			// TODO: handle exception
        	e.printStackTrace();
        	buildError("getPrintData", e.toString());
        	return false;
		}
    	return true;
    }

	/**
	 * 显示无金额的情况
	 * @param pName
	 * @return
	 */
	private String[] getChineseNameWithPY(String pName){
		String[] result = new String[2];
	    if("LPYDSFZ".equals(pName)){
			result[0] = "异地理赔日结单 合计";
		}
	    else if("YDMQJFZ".equals(pName)){
			result[0] = "异地满期给付日结单 合计";
		}
	    else if("YDBQJFZ".equals(pName)){
			result[0] = "异地保全给付日结单 合计";
			System.out.print("ssssssssssssssssssssss");
		}
	    else if("YDYELQZ".equals(pName)){
			result[0] = "异地余额领取日结单 合计";
		}
	    else if("YDZSTFZ".equals(pName)){
			result[0] = "异地暂收退费日结单 合计";
		}
	    else if("YDSXFZ".equals(pName)){
			result[0] = "异地手续费日结单 合计";
		}
		result[1] = "0";
		return result;
	}

}
