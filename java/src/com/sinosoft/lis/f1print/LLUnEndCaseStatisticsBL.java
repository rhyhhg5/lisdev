/**
 * <p>
 * ClassName: LLUnEndCaseStatisticsBL.java
 * </p>
 * <p>
 * Description: 共用保额要素维护
 * </p>
 * <p>
 * Copyright: Copyright (c) 2009
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author
 * @version 1.0
 * @CreateDate：2010-6-22
 */

// 包名
package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LLUnEndCaseStatisticsBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	
	private String mOperator = "";

	private String mManageCom = "";

	private String mRgtDateS = "";

	private String mRgtDateE = "";

	private String mRiskProp = "";

	private String mSaleChnl = "";

	private String mEndDate = "";

	private String mType = "";

	private String mFileNameB = "";

	private TransferData mTransferData = new TransferData();

	private String mMakeDate = PubFun.getCurrentDate();

	private XmlExport mXmlExport = null;

	private ListTable mListTable = new ListTable();

	/** 数据操作字符串 */
	private String mOperate;

	private String mFileName = "";

	private String mFilePath = "";

	private CSVFileWrite mCSVFileWrite = null;
	
	/** 统计类型 */
	private String mStatsType = "";

	/** 业务处理相关变量 */
	// private ××××Schema t****Schema=new ****Schema();
	private MMap map = new MMap();

	public LLUnEndCaseStatisticsBL() {

	}

	public String getFileName() {
		return mFileName;
	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = cInputData;
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}

		if (!prepareOutputData()) {
			return false;
		}

		// PubSubmit tPubSubmit = new PubSubmit();
		//
		// System.out.println("Start LLUnEndCaseStatisticsBL Submit...");
		//
		// if (!tPubSubmit.submitData(mInputData, null)) {
		// this.mErrors.copyAllErrors(tPubSubmit.mErrors);
		// return false;
		// }

		mInputData = null;
		System.out.println("End LLUnEndCaseStatisticsBL Submit...");
		return true;

	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * 
	 */
	private boolean getInputData() {
		mManageCom = (String) mInputData.get(0);
		mRgtDateS = (String) mInputData.get(1);
		mRgtDateE = (String) mInputData.get(2);
		mRiskProp = (String) mInputData.get(3);
		mSaleChnl = (String) mInputData.get(4);
		mEndDate = (String) mInputData.get(5);
		mType = (String) mInputData.get(6);
		mStatsType = (String) mInputData.get(7);//统计类型

		mTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		mFileNameB = (String) mTransferData.getValueByName("FileNameB");

		mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mOperator = mGlobalInput.Operator;

		return true;
	}

	private boolean checkData() {

		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		if (!queryData()) {
			return false;
		} else {
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("tFileNameB", mFileNameB);
			tTransferData.setNameAndValue("tMakeDate", mMakeDate);
			tTransferData.setNameAndValue("tOperator", mGlobalInput.Operator);
			LLPrintSave tLLPrintSave = new LLPrintSave();
			VData tVData = new VData();
			tVData.addElement(tTransferData);
			if (!tLLPrintSave.submitData(tVData, "")) {
				return false;
			}
		}
		return true;
	}

	private boolean queryData() {
		LDSysVarDB tLDSysVarDB = new LDSysVarDB();
		tLDSysVarDB.setSysVar("LPCSVREPORT");

		if (!tLDSysVarDB.getInfo()) {
			buildError("queryData", "查询文件路径失败");
			return false;
		}

		mFilePath = tLDSysVarDB.getSysVarValue();
		//测试使用
		//mFilePath = "F:\\picch\\ui\\vtsfile\\";


		if (mFilePath == null || "".equals(mFilePath)) {
			buildError("queryData", "查询文件路径失败");
			return false;
		}

		System.out.println( "mFilePath:" + mFilePath);

		String tTime = PubFun.getCurrentTime3().replaceAll(":", "");
		String tDate = PubFun.getCurrentDate2();

		mFileName = "WJANTJB" + mGlobalInput.Operator + tDate + tTime;

		System.out.println(mFileName + " mFileName");

		mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);

		String tTypeName = "";
		if ("1".equals(mType)) {
			tTypeName = "已受理未理算";
		} else {
			tTypeName = "已理算未结案";
		}

		String tRiskPropName = "";
		if ("1".equals(mRiskProp)) {
			tRiskPropName = "个险";
		} else {
			tRiskPropName = "团险";
		}
		String tSaleChnlName = "";

		if ("1".equals(mSaleChnl)) {
			tSaleChnlName = "个险";
		} else if ("2".equals(mSaleChnl)) {
			tSaleChnlName = "团险";
		} else if ("3".equals(mSaleChnl)) {
			tSaleChnlName = "银保";
		}
		
		String tStatsTypeName = "";
		if ("1".equals(mStatsType)) {
			tStatsTypeName = "商业保险";
		} else if ("2".equals(mStatsType)) {
			tStatsTypeName = "社会保险";
		} else if ("3".equals(mStatsType)) {
			tStatsTypeName = "全部";
		}
		
		String[][] tTitle = new String[4][];
		if ("1".equals(mType)) {
			tTitle[0] = new String[] { "未决案件统计表" };
			tTitle[1] = new String[] { "统计部门：" + mManageCom,
					"类型：" + tTypeName, "险种: " + tRiskPropName, "渠道: " + tSaleChnlName,"业务类型: " + tStatsTypeName,
					"受理日期：" + mRgtDateS + "至" + mRgtDateE};
			tTitle[2] = new String[] { "制表人：" + mOperator , "制表日期：" + mEndDate};
			tTitle[3] = new String[] { "机构编码", "机构名称", "险种代码", "险种名称",
			"已受理未理算案件数" };
		} else {
			tTitle[0] = new String[] { "未决案件统计表" };
			tTitle[1] = new String[] { "统计部门：" + mManageCom,
					"类型：" + tTypeName, "险种: " + tRiskPropName, "渠道: " + tSaleChnlName,"业务类型: " + tStatsTypeName,
					"受理日期：" + mRgtDateS + "至" + mRgtDateE};
			tTitle[2] = new String[] { "制表人：" + mOperator, "制表日期：" + mEndDate};
			tTitle[3] = new String[] { "机构编码" , "机构名称", "险种代码", "险种名称",
					"已理算未结案案件数", "已理算未结案赔款" };
		}
		String[] tContentType = null;
		if ("1".equals(mType)) {
			tContentType = new String[] { "String", "String", "String",
					"String", "Number" };
		} else {
			tContentType = new String[] { "String", "String", "String",
					"String", "Number", "Number" };
		}
		if (!mCSVFileWrite.addTitle(tTitle, tContentType)) {
			return false;
		} 
		
		LDComDB tLDComDB = new LDComDB();
		tLDComDB.setComCode(mManageCom);
		if (!tLDComDB.getInfo()) {
			buildError("queryData", "查询统计机构错误");
		}

		if (!getDataList()) {
			return false;
		}

		return true;
	}

	private boolean getDataList() {
		// String tComGrade = "03";
		// if (mManageCom.length() == 2) {
		// tComGrade = "02";
		// } else if (mManageCom.length() == 4) {
		// tComGrade = "03";
		// }
		
		String tRiskProp = "";
		if ("1".equals(mRiskProp)) {
			tRiskProp = "I";
		} else {
			tRiskProp = "G";
		}

		String tAgentType = "";
		if ("1".equals(mSaleChnl)) {
			tAgentType = " and exists (select 1 from laagent where getUniteCode(b.agentcode)=getUniteCode(agentcode) and branchtype='1')";
		} else if ("2".equals(mSaleChnl)) {
			tAgentType = " and exists (select 1 from laagent where getUniteCode(b.agentcode)=getUniteCode(agentcode) and branchtype in ('2','4'))";
		} else if ("3".equals(mSaleChnl)) {
			tAgentType = " and exists (select 1 from laagent where getUniteCode(b.agentcode)=getUniteCode(agentcode) and branchtype='3')";
		}

		String tSQL = "";
		if ("1".equals(mType)) {
			//添加案件业务类型清分查询
			String tStatsTypeSql = "";
	        if("1".equals(mStatsType)){
	        	tStatsTypeSql = " and CHECKGRPCONT((select rgtobjno from llregister where rgtno=a.rgtno)) = 'N'  ";
	        }else if("2".equals(mStatsType)){
	        	tStatsTypeSql = " and CHECKGRPCONT((select rgtobjno from llregister where rgtno=a.rgtno)) = 'Y' ";
	        }else{
	        	tStatsTypeSql = "";
	        }
			
			tSQL = " select "
					+ " Y.comcode,Y.name,Y.riskcode,Y.riskname,count(X.customerno) "
					+ " from "
					+ " (select m.comgrade comgrade,m.comcode comcode,name name,n.riskcode riskcode,n.riskname riskname"
					+ " from ldcom m,lmriskapp n " + " where m.comcode like '"
					+ mManageCom
					+ "%' and m.comcode<>'86000000' and m.sign='1' ";
			if (mManageCom.length() == 8) {
				tSQL = tSQL + " and m.sign='1' AND  m.comcode LIKE '" + mManageCom + "%'";
			} else{
				tSQL = tSQL + " and m.sign='1' AND length(trim(m.comcode))>=4";
			}
			tSQL = tSQL
					+ " and n.riskprop='"
					+ tRiskProp
					+ "') as Y left join "
					+ " ("
					+ " select a.mngcom managecom,b.riskcode riskcode,a.customerno customerno "
					+ " from llcase a,lcpol b,llcaserela c,llsubreport d "
					+ " where a.customerno = b.insuredno and a.caseno=c.caseno and c.subrptno=d.subrptno "
					+ " and a.rgtdate between '"
					+ mRgtDateS
					+ "' and '"
					+ mRgtDateE
					+ "' "
					+ " and d.accdate between b.cvalidate and b.enddate "
					+ " and b.appflag='1' "
					+ tAgentType
					+ tStatsTypeSql
					+ " and (a.cancledate is null or a.cancledate > '"
					+ mEndDate
					+ "') "
					+ " and a.mngcom like '"
					+ mManageCom
					+ "%' "
					+ " and not exists (select 1 from llclaimdetail where caseno=a.caseno and makedate <='"
					+ mEndDate
					+ "') "
					+ " union all "
					+ " select a.mngcom managecom,b.riskcode riskcode,a.customerno customerno "
					+ " from llcase a,lbpol b,llcaserela c,llsubreport d "
					+ " where a.customerno = b.insuredno and a.caseno=c.caseno and c.subrptno=d.subrptno "
					+ " and a.rgtdate between '"
					+ mRgtDateS
					+ "' and '"
					+ mRgtDateE
					+ "' "
					+ " and d.accdate between b.cvalidate and b.enddate "
					+ " and b.appflag='1' "
					+ tAgentType
					+ tStatsTypeSql
					+ " and (a.cancledate is null or a.cancledate > '"
					+ mEndDate
					+ "') "
					+ " and a.mngcom like '"
					+ mManageCom
					+ "%' "
					+ " and not exists (select 1 from llclaimdetail where caseno=a.caseno and makedate <='"
					+ mEndDate
					+ "') "
					+ " ) as X  on Y.comcode=substr(X.managecom,1,length(trim(Y.comcode))) and Y.riskcode=X.riskcode "
					+ " group by Y.comcode,Y.name,Y.riskcode,Y.riskname,Y.comgrade "
					+ " order by substr(Y.comcode,1,4),Y.comgrade desc,Y.comcode,Y.riskcode with ur";
		} else {
			//添加案件业务类型清分查询
			String tStatsTypeSql = "";
	        if("1".equals(mStatsType)){
	        	tStatsTypeSql = " and CHECKGRPCONT((b.grpcontno)) = 'N'  ";
	        }else if("2".equals(mStatsType)){
	        	tStatsTypeSql = " and CHECKGRPCONT((b.grpcontno)) = 'Y' ";
	        }else{
	        	tStatsTypeSql = "";
	        }
			
			tSQL = " select "
					+ " Y.comcode,Y.name,Y.riskcode,Y.riskname,nvl(casecount,0),nvl(realpay,0) "
					+ " from "
					+ " (select m.comgrade comgrade,m.comcode comcode,name name,n.riskcode riskcode,n.riskname riskname "
					+ " from ldcom m,lmriskapp n " + " where m.comcode like '"
					+ mManageCom
					+ "%' and m.comcode<>'86000000' and m.sign='1' ";
			if (mManageCom.length() == 8) {
				tSQL = tSQL + " and m.sign='1' AND comcode LIKE '" + mManageCom + "%'";
			} else{
				tSQL = tSQL + " and m.sign='1' AND length(trim(m.comcode))>=4";
			}
			tSQL = tSQL
					+ " and n.riskprop='"
					+ tRiskProp
					+ "') as Y left join "
					+ " ("
					+ " select a.mngcom managecom,b.riskcode riskcode,count(distinct a.caseno) casecount, "
					+ " sum(b.realpay) realpay "
					+ " from llcase a, llclaimdetail b "
					+ " where a.caseno = b.caseno "
					+ " and a.rgtdate between '"
					+ mRgtDateS
					+ "' and '"
					+ mRgtDateE
					+ "' "
					+ tAgentType
					+ tStatsTypeSql
					+ " and (a.endcasedate is null or a.endcasedate > '"
					+ mEndDate
					+ "') "
					+ " and a.mngcom like '"
					+ mManageCom
					+ "%' group by a.mngcom,b.riskcode "
					+ " ) as X  on Y.comcode=substr(X.managecom,1,length(trim(Y.comcode))) and Y.riskcode=X.riskcode "
					+ " order by substr(Y.comcode,1,4),Y.comgrade desc,Y.comcode,Y.riskcode with ur";
		}

		RSWrapper rsWrapper = new RSWrapper();
		System.out.println(tSQL);

		if (!rsWrapper.prepareData(null, tSQL)) {
			System.out.println("数据准备失败! ");
			return false;
		}
		SSRS tSSRS = new SSRS();
		try {
			do {
				tSSRS = rsWrapper.getSSRS();
				String tContent[][] = new String[tSSRS.getMaxRow()][];
				for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
					if ("1".equals(mType)) {
						String Info[] = new String[5];
						Info[0] = tSSRS.GetText(i, 1);
						Info[1] = tSSRS.GetText(i, 2);
						Info[2] = tSSRS.GetText(i, 3);
						Info[3] = tSSRS.GetText(i, 4);
						Info[4] = tSSRS.GetText(i, 5);
						mListTable.add(Info);
						tContent[i - 1] = Info;
					} else {
						String Info[] = new String[6];
						Info[0] = tSSRS.GetText(i, 1);
						Info[1] = tSSRS.GetText(i, 2);
						Info[2] = tSSRS.GetText(i, 3);
						Info[3] = tSSRS.GetText(i, 4);
						Info[4] = tSSRS.GetText(i, 5);
						Info[5] = tSSRS.GetText(i, 6);
						mListTable.add(Info);
						tContent[i - 1] = Info;
					}
				}
				mCSVFileWrite.addContent(tContent);
				if (!mCSVFileWrite.writeFile()) {
					mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
					return false;
				}
			} while (tSSRS != null && tSSRS.MaxRow > 0);
		} catch (Exception ex) {
			ex.printStackTrace();
			mCSVFileWrite.closeFile();
			return false;
		} finally {
			rsWrapper.close();
		}
		return true;
	}

	/**
	 * 追加错误信息
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLUnEndCaseStatics";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	private boolean prepareOutputData() {
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	public static void main(String[] args) {

	}
}
