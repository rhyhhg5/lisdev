/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.sinosoft.lis.bq.ChangeCodeBL;
import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.db.LCSpecDB;
import com.sinosoft.lis.db.LCUWMasterDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCUWMasterSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.tb.CachedRiskInfo;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LCSpecSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 * 475要修改
 * 508要修改
 * @todo 主要分两个部分，一个是使用listtable方式
 * 另一个是texttag的方式,前者用于动态显示,后者用于
 * 静态显示
 */
public class ContYTPBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //取得的保单号码
    //    private String mPolNo = "";
    //String InsuredName="";
    //输入的查询sql语句

    //取得的延期承保原因
    //    private String mUWError = "";
    //取得的代理人编码
    private String mAgentCode = "";

    //    private String lys_Flag_ab = "0";

    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

    //    private LAAgentSchema tLAAgentSchema = new LAAgentSchema();

    private GlobalInput mGlobalInput;

    private String mflag = null;

    //业务处理相关变量
    /** 全局数据 */
    //    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    /**
     * 个人合同表
     */
    private LCContSchema tLCContSchema = null;

    private LCPolSchema mLCPolSchema = new LCPolSchema();

    private LCUWMasterSchema tLCUWMasterSchema = null;

    private TransferData mTransferData;

    private String mLoadFlag;

    private double fPremSum = 0;

    private double fPremAddSum = 0;

    private String fPrem = "";

    private String fPremAdd = "";

    private String CurrentDate = PubFun.getCurrentDate();

    private CachedRiskInfo mCRI = CachedRiskInfo.getInstance();

    private boolean NeedAdd = true;

    private static int sumCount = 11;

    public ContYTPBL()
    {

    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mflag = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        try
        {
            if (!getPrintData())
            {
                return false;
            }
        }
        catch (Exception e)
        {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData
                .getObjectByObjectName("LOPRTManagerSchema", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mLOPRTManagerSchema == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (mTransferData != null)
        {
            this.mLoadFlag = (String) mTransferData.getValueByName("LoadFlag");
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LCPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData1()
    {
        ExeSQL tExeSQL = new ExeSQL();

        /**
         * 查询合同信息
         */
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLOPRTManagerSchema.getOtherNo());
        if (!tLCContDB.getInfo())
        {
            System.out
                    .println("UWResultPBL.getPrintData()  \n--Line:165  --Author:YangMing");
            //            this.mErrors.copyAllErrors(tLCContDB.mErrors);
            //            return false;
            LCContSet tLCContSet = tLCContDB
                    .executeQuery("select * from lccont where proposalcontno='"
                            + mLOPRTManagerSchema.getOtherNo() + "'");
            if (tLCContSet.size() <= 0)
            {
                // @@错误处理
                System.out.println("UWResultPBLUWResultPBL.java中"
                        + "getPrintData方法报错，" + "在程序154行，Author:Yangming");
                CError tError = new CError();
                tError.moduleName = "UWResultPBL.java";
                tError.functionName = "getPrintData";
                tError.errorMessage = "查询LCCont失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
            else if (tLCContSet.size() == 1)
            {
                tLCContSchema = tLCContSet.get(1).getSchema();
                tLCContDB.setSchema(tLCContSchema);
            }
            else
            {
                // @@错误处理
                System.out.println("UWResultPBLUWResultPBL.java中"
                        + "getPrintData方法报错，" + "在程序154行，Author:Yangming");
                CError tError = new CError();
                tError.moduleName = "UWResultPBL.java";
                tError.functionName = "getPrintData";
                tError.errorMessage = "查询LCCont失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        else
        {
            tLCContSchema = tLCContDB.getSchema();
        }

        // 获取通知书轨迹
        if (!loadPrintManager())
        {
            return false;
        }
        // -------------------------------

        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB
        //去除业务员是个险直销的条件  2008-05-13
                //.executeQuery("select * from labranchgroup where agentgroup=(select agentgroup from laagent where branchtype='1' and branchtype2='01' and agentcode='"
                .executeQuery("select * from labranchgroup where agentgroup=(select agentgroup from laagent where agentcode='"
                        + tLCContDB.getAgentCode() + "')");
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0)
        {
            buildError("getprintData", "没有查到该报单的代理人组别");
        }

        /**
         * 查询投保人信息
         */
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(tLCContSchema.getContNo());
        if (tLCAppntDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLCAppntDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        LCAddressDB tLCAddressDB = new LCAddressDB();
        tLCAddressDB.setCustomerNo(tLCContSchema.getAppntNo());
        tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
        if (tLCAddressDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLCAddressDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        /**
         * 查询代理人信息
         */
        mAgentCode = tLCContDB.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "在取得LAAgent的数据时发生错误");
            return false;
        }
        /**
         * 查询保单下全部险种信息
         */
        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSet tempLCPolSet = null;
        if (!StrTool.cTrim(this.mLoadFlag).equals("Defer"))
        {
            tempLCPolSet = tLCPolDB
                    .executeQuery("select * from lcpol where proposalcontno='"
                            + tLCContSchema.getProposalContNo()
                            + "' order by insuredname ");
        }
        if (StrTool.cTrim(this.mLoadFlag).equals("Defer"))
        {
            tempLCPolSet = tLCPolDB
                    .executeQuery("select * from lcpol where proposalcontno='"
                            + tLCContSchema.getProposalContNo()
                            + "' and uwflag not in ('4','9') order by insuredname ");
        }
        if (tempLCPolSet.size() <= 0)
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "查询险种信息失败!未找到该保单下险种信息");
            System.out
                    .println("UWResultPBL.getPrintData()  \n--Line:197  --Author:YangMing");
            return false;
        }
        //完成信息查询
        System.out.println("完成信息查询,开始准备向模板中填入数据!");

        String tStrPolSql = " select "
                + " InsuredName, "
                + " (select codename from ldcode where codetype='idtype' and code=b.IDType) as IDType, "
                + " b.IDNo as IDNo, "
                + " rownumber() over() as RiskID, "
                + " RiskCode, "
                + " case "
                + " when InsuYearFlag='A' and InsuYear>=100 then '终身' "
                + " when InsuYearFlag='A' and InsuYear<100 then '至'||rtrim(char(InsuYear))||'岁' "
                + " when InsuYearFlag='Y' then rtrim(char(InsuYear))||'年' "
                + " when InsuYearFlag='M' then rtrim(char(InsuYear))||'月' "
                + " when InsuYearFlag='D' then rtrim(char(InsuYear))||'天' "
                + " else rtrim(char(InsuYear))||'年' "
                + " end YearsIntv, "
                + " case "
                + " when PayEndYearFlag='A' and PayEndYear>=100 then '终身' "
                + " when PayEndYearFlag='A' and PayEndYear<100 "
                + " then '至'||rtrim(char(PayEndYear))||'岁' "
                + " when PayEndYearFlag='Y' then rtrim(char(PayEndYear))||'年' "
                + " when PayEndYearFlag='M' then rtrim(char(PayEndYear))||'月' "
                + " when PayEndYearFlag='D' then rtrim(char(PayEndYear))||'天' "
                + " else "
                + " rtrim(char(PayEndYear))||'年' "
                + " end PayYearsIntv, "
                + " CodeName('payintv', trim(char(PayIntv))) PayIntv,b.InsuredNo,PolNo, "
                + " case Mult "
                + " when 0 then "
                + " ( "
                + "     case "
                + "     when '4' = (select RiskType4 from LMRiskApp where RiskCode = LCPol.RiskCode) "
                + "     then "
                + "     ( "
                + "     case "
                + "     when (select CodeName from LDCode where CodeType = 'UniversalAmnt' and Code = LCPol.RiskCode) is null "
                + "     then varchar(Amnt) "
                + "     else (select CodeName from LDCode where CodeType = 'UniversalAmnt' and Code = LCPol.RiskCode) "
                + "     end "
                + " ) "
                + " else varchar(Amnt) "
                + " end "
                + " ) "
                + " else char(integer(Mult)) "
                + " end AM, "
                + " Prem,LCPol.ContNo ContNo,b.SequenceNo,LCPol.RiskSeqNo,LCPol.SupplementaryPrem SupplementaryPrem, "
                + " LCPol.Prem + LCPol.SupplementaryPrem PolSumPrem, "
                + " nvl( "
                + " ( "
                + "     select sum(Prem) SumPrem "
                + "     from LCPol subPol "
                + "     where subPol.ContNo = LCPol.ContNo "
                + "     and subPol.insuredno = b.InsuredNo "
                + "     and subPol.RiskCode in "
                + "     ( "
                + "     select RiskCode "
                + "     from LDRiskParamPrint "
                + "     where ParamType1='sumprem' "
                + "     and ParamValue1 in (select ParamValue1 from LDRiskParamPrint where RiskCode = subPol.RiskCode) "
                + "     ) "
                + " ), 0) SumPrem, "
                + " ( "
                + " select NotPrintPol "
                + " from LMRiskApp a "
                + " where a.RiskCode = LCPol.RiskCode "
                + " ) PrintFlag, "
                + " ( "
                + " select "
                + " case "
                + " when (select RiskWrapPlanName from LDCode1 where CodeType = 'checkappendrisk' and Code1 = a.RiskCode) is not null   "
                + " then (select RiskWrapPlanName from LDCode1 where CodeType = 'checkappendrisk' and Code1 = a.RiskCode) "
                + " else "
                + " RiskName "
                + " end as RiskName "
                + " from LMRiskApp a "
                + " where a.RiskCode = LCPol.RiskCode "
                + " ) RiskName "
                + " from LCPol,LCInsured b "
                + " where LCPol.ContNo=b.ContNo "
                + " and LCPol.InsuredNo=b.InsuredNo "
                + " and LCPol.ContNo = '"
                + tLCContSchema.getContNo()
                + "' "
                + " and LCPol.RiskCode not in(select Code from LDCode1 where CodeType = 'checkappendrisk' and RiskWrapPlanName is not null) "
                + " order by int(b.SequenceNo),int(LCPol.RiskSeqNo) ";

        SSRS tPolSSRS = tExeSQL.execSQL(tStrPolSql);

        System.out.println("1,动态列表");
        ListTable tUWResultTable = new ListTable();
        String[] UWResultTitle = new String[12];

        tUWResultTable.setName("RiskInfo"); //对应模版投保信息部分的行对象名
        UWResultTitle[0] = "ColNo";
        UWResultTitle[1] = "InsurdName";
        UWResultTitle[2] = "RiskName";
        UWResultTitle[3] = "RiskCode";
        UWResultTitle[4] = "MultOrAmnt";
        UWResultTitle[5] = "InsuYears";
        UWResultTitle[6] = "PayYears";
        UWResultTitle[7] = "PayIntv";
        UWResultTitle[8] = "PrintFlag";
        UWResultTitle[9] = "Prem";
        UWResultTitle[10] = "SumPrem";
        UWResultTitle[11] = "SupplementaryPrem";

        for (int i = 1; i <= tPolSSRS.MaxRow; i++)
        {
            String[] tPolInfo = tPolSSRS.getRowData(i);

            String[] UWResult = new String[12];
            UWResult[0] = "" + i; //模板中的第一列 序号
            UWResult[1] = tPolInfo[0];
            UWResult[2] = tPolInfo[19];
            UWResult[3] = tPolInfo[4];
            UWResult[4] = tPolInfo[10];
            UWResult[5] = tPolInfo[5];
            UWResult[6] = tPolInfo[6];
            UWResult[7] = tPolInfo[7];
            UWResult[8] = tPolInfo[18];
            UWResult[9] = tPolInfo[11];
            UWResult[10] = tPolInfo[17];
            UWResult[11] = tPolInfo[15];

            tUWResultTable.add(UWResult);
        }

        //        boolean needWriteBack = false;
        //        ExeSQL tExeSQL = new ExeSQL();
        //        for (int nIndex = 0; nIndex < tempLCPolSet.size(); nIndex++)
        //        {
        //            //查询主险保单
        //            LCPolSchema mLCPolSchema = tempLCPolSet.get(nIndex + 1).getSchema(); //保存险种投保单信息
        //            //1-险种信息：
        //            LMRiskDB tLMRiskDB = new LMRiskDB();
        //            tLMRiskDB.setRiskCode(mLCPolSchema.getRiskCode());
        //            if (!tLMRiskDB.getInfo())
        //            {
        //                mErrors.copyAllErrors(tLMRiskDB.mErrors);
        //                buildError("outputXML", "在取得主险LMRisk的数据时发生错误");
        //                return false;
        //            }
        //            //险种名称
        //            String MainRiskName = tLMRiskDB.getRiskName();
        //            //保额
        //            double Amnt = mLCPolSchema.getAmnt();
        //            //保费
        //            double Prem = mLCPolSchema.getPrem();
        //            //档次
        //            String Mult = mLCPolSchema.getMult() > 0 ? getNumberToCharacter(mLCPolSchema
        //                    .getMult())
        //                    + "档"
        //                    : null;
        //            String[] UWResult = new String[10];
        //            //行号
        //            UWResult[0] = (1 + nIndex) + ""; //模板中的第一列 序号
        //            UWResult[1] = mLCPolSchema.getInsuredName();
        //            UWResult[2] = MainRiskName;
        //            UWResult[3] = mLCPolSchema.getRiskCode();
        //            UWResult[4] = Mult == null ? String.valueOf(Amnt).substring(0,
        //                    String.valueOf(Amnt).indexOf(".")) : Mult;
        //            if (Mult == null)
        //            {
        //                UWResult[4] = "保额 " + UWResult[4];
        //            }
        //            else
        //            {
        //                UWResult[4] = "档次 " + UWResult[4];
        //            }
        //            String Sql = "select sum(prem) from lcprem where contno='"
        //                    + mLCPolSchema.getContNo() + "' and polno='"
        //                    + mLCPolSchema.getPolNo()
        //                    + "' and payplancode not like '000000%'";
        //            Prem = Double.parseDouble(tExeSQL.getOneValue(Sql));
        //            UWResult[5] = String.valueOf(Prem);
        //            //核保结论较复杂，需要单独处理
        //            try
        //            {
        //                UWResult[6] = dealUWResult(mLCPolSchema);
        //            }
        //            catch (Exception ex)
        //            {
        //                ex.printStackTrace();
        //            }
        //            if (tLCUWMasterSchema != null)
        //            {
        //                UWResult[7] = this.tLCUWMasterSchema.getUWIdea() == null ? ""
        //                        : this.tLCUWMasterSchema.getUWIdea();
        //                if (UWResult[6].equals("")
        //                        || tLCUWMasterSchema.getPassFlag().equals("9")
        //                        || tLCUWMasterSchema.getPassFlag().equals("1")
        //                        || tLCUWMasterSchema.getPassFlag().equals("a")
        //                        || tLCUWMasterSchema.getPassFlag().equals("8")
        //                        || mLCPolSchema.getUWFlag().equals("a"))
        //                {
        //                    UWResult[8] = "    --";
        //                    UWResult[9] = "    --";
        //                }
        //                else
        //                {
        //                    UWResult[8] = "□同意  □不同意";
        //                    UWResult[9] = "险种解约";
        //                    needWriteBack = true;
        //                    if (StrTool.cTrim(this.mLoadFlag).equals("Defer"))
        //                    {
        //                        UWResult[8] = "    --";
        //                        UWResult[9] = "    --";
        //                        needWriteBack = false;
        //                    }
        //                }
        //                tUWResultTable.add(UWResult); //加入主险信息
        //            }
        //        }
        //        ListTable tBlankListTable = new ListTable();
        //        String[] BlankListTitle = new String[1];
        //        tBlankListTable.setName("Blank"); //对应模版投保信息部分的行对象名
        //        BlankListTitle[0] = "Row1";
        //        for (int i = 1; i <= 1; i++)
        //        {
        //            String[] Row = new String[1];
        //            Row[0] = "";
        //            tBlankListTable.add(Row);
        //        }
        //完成动态填充,开始静态
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLCContDB.getManageCom());
        mLAAgentSchema = tLAAgentDB.getSchema(); //保存代理人信息
        if (!tLDComDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLDComDB.mErrors);
            return false;
        }
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        TextTag texttag1 = new TextTag();
        //      借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='"
                + mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);

        // 根据轨迹表中保存的单证类型，确定通知书样式
        String tCode = mLOPRTManagerSchema.getCode();
        texttag1.add("JetFormType", tCode);
        // ----------------------------------------

        if (comcode.equals("86") || comcode.equals("8600")
                || comcode.equals("86000000"))
        {
            comcode = "86";
        }
        else if (comcode.length() >= 4)
        {
            comcode = comcode.substring(0, 4);
        }
        else
        {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
        }
        String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"
                + comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        texttag1.add("ManageComLength4", printcode);
        texttag1.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        if (mflag.equals("batch"))
        {
            texttag1.add("previewflag", "0");
        }
        else
        {
            texttag1.add("previewflag", "1");
        }
        texttag.add("XI_ManageCom", tLCContDB.getManageCom());
        texttag.add("ManageCom", tLDComDB.getLetterServiceName());
        texttag.add("ManageAddress", tLDComDB.getServicePostAddress());
        texttag.add("ManageZipCode", tLDComDB.getLetterServicePostZipcode());
        texttag.add("ManageFax", tLDComDB.getFax());
        texttag.add("ManagePhone", tLDComDB.getServicePhone());
        texttag.add("XI_AppntNo", tLCContSchema.getAppntNo());
        texttag.add("AppntName", tLCContSchema.getAppntName());
        String strSql = "select date('"
                + this.mLOPRTManagerSchema.getMakeDate()
                + "') + 5 days from dual ";
        String WriteBack = tExeSQL.getOneValue(strSql);
        texttag
                .add("WriteBack", WriteBack.split("-")[0] + "年"
                        + WriteBack.split("-")[1] + "月"
                        + WriteBack.split("-")[2] + "日");
        texttag.add("AppntZipcode", tLCAddressDB.getZipCode());
        texttag.add("AppntAddr", tLCAddressDB.getPostalAddress());
        texttag.add("PrtNo", tLCContSchema.getPrtNo());
		//2014-11-3   杨阳
		//业务员编码显示为表LAagent中的GroupAgentCode字段
		String groupAgentcode = new ExeSQL().getOneValue("select getUniteCode("+tLCContSchema.getAgentCode()+") from dual");
        texttag.add("AgentCode",groupAgentcode );
        texttag.add("AgentName", mLAAgentSchema.getName()); //代理人姓名
        //add by zhangxing
        texttag.add("AgentGroup", mLAAgentSchema.getAgentGroup()); //代理人组别
        texttag.add("AgentGroupName", tLABranchGroupSet.get(1).getBranchAttr());

        texttag.add("GlobalServicePhone", tLDComDB.getServicePhone());

        texttag.add("AgentPhone", getAgentPhone());

        texttag.add("Cavidate", tLCContSchema.getCValiDate().split("-")[0]
                + "年" + tLCContSchema.getCValiDate().split("-")[1] + "月"
                + tLCContSchema.getCValiDate().split("-")[2] + "日");
        texttag.add("Title", tLCContSchema.getAppntSex().equals("0") ? "先生"
                : "女士");
        texttag.add("Today",
                this.mLOPRTManagerSchema.getMakeDate().split("-")[0] + "年"
                        + this.mLOPRTManagerSchema.getMakeDate().split("-")[1]
                        + "月"
                        + this.mLOPRTManagerSchema.getMakeDate().split("-")[2]
                        + "日");
        texttag.add("BarCode1", mLOPRTManagerSchema.getPrtSeq());
        texttag
                .add(
                        "BarCodeParam1",
                        "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

        // 加入投保件归档号。
        String tArchiveNo = "";
        String tStrSql = "select * from es_doc_main where doccode = '"
                + tLCContSchema.getPrtNo() + "'";
        ES_DOC_MAINDB tES_DOC_MAINBDB = new ES_DOC_MAINDB();
        ES_DOC_MAINSet tES_DOC_MAINSet = tES_DOC_MAINBDB.executeQuery(tStrSql);
        if (tES_DOC_MAINSet.size() > 1)
        {
            buildError("getPrintData", "系统中相关扫描件出现多份！");
        }
        if (tES_DOC_MAINSet.size() == 1
                && tES_DOC_MAINSet.get(1).getArchiveNo() != null)
        {
            tArchiveNo = tES_DOC_MAINSet.get(1).getArchiveNo();
        }
        texttag.add("ArchiveNo", tArchiveNo);
        // --------------------------------------

        // 增加整单核保结论
        texttag.add("UWFlag", tLCContSchema.getUWFlag());
        // --------------------------------------

        // 增加到帐保费节点。只有拒保通知书才有该节点。
        texttag.add("EnterAccFee", getEnterAccFee(tLCContSchema));
        texttag.add("DifMoney", getDifMoney(tLCContSchema));
        texttag.add("PremSum", getPremSum(tLCContSchema));
        // --------------------

        // 增加银行帐户节点。目前没确定如何取，先置空。
        texttag.add("BankCode", "");
        texttag.add("BankAccCode", "");
        texttag.add("AccName", "");
        // --------------------

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("UWResult.vts", "printer");
        //控制页面是否显示
        //        if (needWriteBack)
        //        {
        //            xmlexport.addDisplayControl("displaydescribe");
        //            xmlexport.addDisplayControl("displaywriteback");
        //        }
        xmlexport.addTextTag(texttag1);
        xmlexport.addTextTag(texttag);
        xmlexport.addListTable(tUWResultTable, UWResultTitle); //保存险种信息及其标题栏
        //xmlexport.addListTable(tBlankListTable, BlankListTitle); //保存险种信息及其标题栏
        ListTable tEndTable = new ListTable();
        tEndTable.setName("END");
        String[] tEndTitle = new String[0];
        xmlexport.addListTable(tEndTable, tEndTitle);
        xmlexport.outputDocumentToFile("D:\\", "UWResult");
        mResult.clear();
        mResult.addElement(xmlexport);
        mLOPRTManagerSchema
                .setPrintTimes(mLOPRTManagerSchema.getPrintTimes() + 1);
        mResult.addElement(mLOPRTManagerSchema);
        return true;
    }

    private String getNumberToCharacter(double Num)
    {
        String cNum = String.valueOf((int) Num);
        if (cNum.equals("1"))
        {
            return "一";
        }
        if (cNum.equals("2"))
        {
            return "二";
        }
        if (cNum.equals("3"))
        {
            return "三";
        }
        if (cNum.equals("4"))
        {
            return "四";
        }
        if (cNum.equals("5"))
        {
            return "五";
        }
        if (cNum.equals("6"))
        {
            return "六";
        }
        if (cNum.equals("7"))
        {
            return "七";
        }
        if (cNum.equals("8"))
        {
            return "八";
        }
        return null;
    }

    /**
     *
     * @todo 处理核保结论，主要针对加费免责信息
     * @param schema String
     * @return String
     */
    private String dealUWResult(LCPolSchema schema)
    {

        String dPolNo = schema.getProposalNo();
        String SpecInfo = "";
        String addFeeResult = "";
        String Sresult = "";
        LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
        tLCUWMasterDB.setProposalNo(dPolNo);
        if (!tLCUWMasterDB.getInfo())
        {
            buildError("outputXML", "处理核保结论时，查询核保主表失败！");
            return "";
        }
        tLCUWMasterSchema = tLCUWMasterDB.getSchema();
        if (StrTool.cTrim(tLCContSchema.getUWFlag()).equals("a"))
        {
            return "撤销申请";
        }
        System.out.println("#####:" + tLCUWMasterSchema.getAddPremFlag());
        if (!StrTool.cTrim(tLCUWMasterSchema.getSpecFlag()).equals("0"))
        {
            //有特别约定
            LCSpecDB tLCSpecDB = new LCSpecDB();
            tLCSpecDB.setPolNo(dPolNo);
            LCSpecSet tLCSpecSet = tLCSpecDB.query();
            if (tLCSpecSet.size() <= 0)
            {
                //                buildError("outputXML", "处理核保结论时，查询特约信息失败！");
                //                return "";
            }
            for (int i = 1; i <= tLCSpecSet.size(); i++)
            {
                SpecInfo += tLCSpecSet.get(i).getSpecContent() + ";";
                if (tLCSpecSet.get(i).getSpecStartDate() != null
                        && tLCSpecSet.get(i).getSpecEndDate() != null)
                {
                    SpecInfo += "免责期间为: "
                            + tLCSpecSet.get(i).getSpecStartDate().split("-")[0]
                            + "年"
                            + tLCSpecSet.get(i).getSpecStartDate().split("-")[1]
                            + "月"
                            + tLCSpecSet.get(i).getSpecStartDate().split("-")[2]
                            + "日" + "至"
                            + tLCSpecSet.get(i).getSpecEndDate().split("-")[0]
                            + "年"
                            + tLCSpecSet.get(i).getSpecEndDate().split("-")[1]
                            + "月"
                            + tLCSpecSet.get(i).getSpecEndDate().split("-")[2]
                            + "日 ";
                }
                else if (tLCSpecSet.get(i).getSpecEndDate() == null
                        && tLCSpecSet.get(i).getSpecStartDate() != null)
                {
                    SpecInfo += "免责期间自"
                            + tLCSpecSet.get(i).getSpecStartDate().split("-")[0]
                            + "年"
                            + tLCSpecSet.get(i).getSpecStartDate().split("-")[1]
                            + "月"
                            + tLCSpecSet.get(i).getSpecStartDate().split("-")[2]
                            + "日" + "开始 ";
                }
            }
        }

        if (!StrTool.cTrim(tLCUWMasterSchema.getAddPremFlag()).equals("0"))
        {
            //有加费
            LCPremDB tLCPremDB = new LCPremDB();
            tLCPremDB.setPolNo(dPolNo);
            LCPremSet tLCPremSet = tLCPremDB.query();
            if (tLCPremSet.size() <= 0)
            {
                buildError("outputXML", "处理核保结论时，查询加费信息失败！");
                return "";
            }
            double addfee = 0.00;
            double addFeeRate = 0.00;
            String strSql = "";
            String strSql1 = "";
            String StrarDate = "";
            String EndDate = "";
            for (int i = 1; i <= tLCPremSet.size(); i++)
            {
                if (tLCPremSet.get(i).getPayPlanCode().startsWith("000000"))
                {
                    addFeeRate = tLCPremSet.get(i).getRate();
                    addfee = tLCPremSet.get(i).getPrem();
                    strSql = (new ExeSQL()).getOneValue("select int("
                            + addFeeRate * 100 + ") from dual");
                    strSql1 = (new ExeSQL()).getOneValue("select varchar("
                            + addfee + ") from dual");
                    EndDate = tLCPremSet.get(i).getPayEndDate();
                    StrarDate = tLCPremSet.get(i).getPayStartDate();
                    break;
                }
            }
            if (addfee > 0 || addFeeRate > 0)
            {
                addFeeResult = "附加风险保费";
                addFeeResult += (addFeeRate > 0 ? strSql + "％(" + strSql1
                        + "元)" : strSql1 + "元;");
                System.out.println("addFeeResult" + addFeeResult);
                addFeeResult += "加费自" + StrarDate.split("-")[0] + "年"
                        + StrarDate.split("-")[1] + "月"
                        + StrarDate.split("-")[2] + "日" + "开始";
                if (!StrTool.cTrim(EndDate).equals(""))
                {
                    addFeeResult += "至" + EndDate.split("-")[0] + "年"
                            + EndDate.split("-")[1] + "月"
                            + EndDate.split("-")[2] + "日" + "终止";
                }
            }
            System.out.println("@@@@@ : " + addFeeResult);
        }
        if (!StrTool.cTrim(tLCUWMasterSchema.getAddPremFlag()).equals("0"))
        {
            //有承保计划变更
        }
        if (tLCUWMasterSchema.getPassFlag().equals("8"))
        {
            //延期承保
            String tPostponeDay = tLCUWMasterSchema.getPostponeDay();
            String tPostponeDate = tLCUWMasterSchema.getPostponeDate();
            String result = "延期承保";
            if (tPostponeDay == null && tPostponeDate == null)
            {
                result = "延期承保";
            }
            else if (tPostponeDay != null)
            {
                result = "延期" + tPostponeDay + "天";
            }
            else if (tPostponeDate != null)
            {
                result = "延期处理至" + tPostponeDate.split("-")[0] + "年"
                        + tPostponeDate.split("-")[1] + "月"
                        + tPostponeDate.split("-")[2] + "日";
            }
            return result;
        }
        if (!StrTool.cTrim(tLCUWMasterSchema.getSubMultFlag()).equals("0")
                && tLCUWMasterSchema.getMult() > 0)
        {
            //延期承保
            double subMult = tLCUWMasterSchema.getMult();
            double tMult = schema.getMult();
            /**@author:Yangming 处理降档 */
            Sresult = "档次由" + (int) subMult + "档降至" + (int) tMult + "档";
            // return result;
        }
        if (!StrTool.cTrim(tLCUWMasterSchema.getSubAmntFlag()).equals("0")
                && tLCUWMasterSchema.getAmnt() > 0)
        {
            //延期承保
            double subAmnt = tLCUWMasterSchema.getAmnt();
            double tAmnt = schema.getAmnt();
            /**@author:Yangming 处理降档 */
            Sresult = "保额由" + (int) subAmnt + "降至" + (int) tAmnt + "";
            //return result;
        }
        if (tLCUWMasterSchema.getPassFlag().equals("a"))
        {
            return "撤销申请";
        }
        if (tLCUWMasterSchema.getPassFlag().equals("1"))
        {
            return "谢绝承保";
        }
        if (tLCUWMasterSchema.getPassFlag().equals("9"))
        {
            return "正常通过";
        }
        return SpecInfo + addFeeResult + Sresult;
    }

    private String getAgentPhone()
    {
        String phone = !StrTool.cTrim(mLAAgentSchema.getMobile()).equals("") ? mLAAgentSchema
                .getMobile()
                : StrTool.cTrim(mLAAgentSchema.getPhone());
        return phone.equals("") ? "          " : phone;
    }

    /**
     * 获取打印轨迹
     * @return LOPRTManagerDB
     * @throws Exception
     */
    private boolean loadPrintManager()
    {
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (!tLOPRTManagerDB.getInfo())
        {
            buildError("loadPrintManager", "查询打印管理轨迹失败。");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();

        if (mLOPRTManagerSchema == null)
        {
            buildError("loadPrintManager", "获取打印管理轨迹失败。");
            return false;
        }

        return true;
    }

    /**
     * 获取到帐保费。
     * @param tLCContSchema
     * @return  到帐总金额
     */
    private String getEnterAccFee(LCContSchema tLCContSchema)
    {
        ExeSQL tExeSQL = new ExeSQL();
        String tStrSql = null;
        String tResult = null;

        // 获取暂收总保费。
        String tContNo = tLCContSchema.getContNo();

        tStrSql = " select nvl(sum(ljtf.PayMoney), 0) from LJTempFee ljtf "
                + " where ljtf.OtherNoType = '2' " // 个单暂收核销后标志
                // + " and ljtf.RiskCode != '000000' " // 除去保全相关暂收数据
                + " and ljtf.ConfFlag = '1' " // 可用销暂收数据
                + " and ljtf.EnterAccDate is not null " // 财务到帐
                + " and ljtf.OtherNo = '" + tContNo + "' ";
        tResult = tExeSQL.getOneValue(tStrSql);
        // --------------------

        return format(Double.parseDouble(tResult));
    }

    /**
     * 获取暂收与应缴保费差额
     * 差额 = 总暂收保费 - 应收期缴保费
     * @param cContInfo
     * @return
     */
    private String getDifMoney(LCContSchema cContInfo)
    {
        String tStrSql = null;
        String tResult = null;

        String tPrtNo = cContInfo.getPrtNo();

        tStrSql = "select LF_PayMoneyNo('" + tPrtNo + "') from Dual";
        tResult = new ExeSQL().getOneValue(tStrSql);
        if ("".equals(tResult))
        {
            // 不会出现这种情况，为防止万一，如果产生，后台报错，但不阻断，视为0。
            String tStrErr = "以印刷号查询，未查到合同号。";
            System.out.println(tStrErr);
            buildError("getDifPrem", tStrErr);
            return format(0);
        }
        double tOutPrem = 0;
        tOutPrem = Double.parseDouble(tResult);
        System.out.println("DifPrem:" + tOutPrem);
        tResult = null;
        tStrSql = null;
        return format(tOutPrem);
    }

    private String getDifMoney(double fPremSum, double fPremAddSum,
            String tEnterAccFee)
    {
        BigDecimal tDifMoney = new BigDecimal(0);

        // 累计标准承保保费
        tDifMoney = tDifMoney.add(new BigDecimal(fPremSum));
        // 累计承保加费
        tDifMoney = tDifMoney.add(new BigDecimal(fPremAddSum));

        tDifMoney = new BigDecimal(tEnterAccFee).subtract(tDifMoney);

        // 精确到2位小数。末位四舍五入。
        tDifMoney = tDifMoney.setScale(2, BigDecimal.ROUND_HALF_UP);

        return tDifMoney.toString();
    }

    /**
     * 获取总期缴保费
     * @param cContInfo
     * @return
     */
    private String getPremSum(LCContSchema cContInfo)
    {
        ExeSQL tExeSQL = new ExeSQL();
        String tStrSql = null;
        String tResult = null;

        // 获取暂收总保费。
        String tContNo = cContInfo.getContNo();

        tStrSql = " select nvl(sum(nvl(lcp.Prem, 0) + nvl(lcp.SupplementaryPrem, 0)), 0) "
                + " from LCPol lcp where 1 = 1 "
                + " and lcp.UWFlag in ('4', '9') "
                + " and lcp.ContNo = '"
                + tContNo + "' ";
        tResult = tExeSQL.getOneValue(tStrSql);
        // --------------------

        return format(Double.parseDouble(tResult));
    }

    /**
     * 格式化浮点型数据
     * @param dValue double
     * @return String
     */
    private static String format(double dValue)
    {
        return new DecimalFormat("0.00").format(dValue);
    }

    private boolean getPrintData() throws Exception
    {
        /** 首先封装打印管理信息数据 */
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setCode(mLOPRTManagerSchema.getCode());
        tLOPRTManagerDB.setOtherNo(mLOPRTManagerSchema.getOtherNo());
        mLOPRTManagerSchema = tLOPRTManagerDB.query().get(1);
        LCContDB tLCContDB = getContInfo();
        String strContNo = tLCContDB.getContNo();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB
                .executeQuery("select * from labranchgroup where agentgroup=(select agentgroup from laagent where  agentcode='"
                        + tLCContDB.getAgentCode() + "')");
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0)
        {
            buildError("getprintData", "没有查到该报单的代理人组别");
            return false;
        }
        LDComDB tLDComDB = new LDComDB();
        LDComSet tLDComSet = new LDComSet();
        tLDComDB.setComCode(tLCContDB.getManageCom());
        tLDComSet = tLDComDB.query();

        if (mLOPRTManagerSchema.getStateFlag() == null)
        {
            buildError("getprintData", "无效的打印状态");
            return false;
        }
        mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setDoneTime(PubFun.getCurrentTime());
        /** 获取代理人信息 */
        getAgentInfo(tLCContDB);

        // 设置合计项
        fPremSum = 0;
        fPremAddSum = 0;
        double fSupplementaryPrem = 0;
        TextTag texttag1 = new TextTag();
        //      借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='"
                + mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        //        texttag1.add("JetFormType", "07");

        // 根据轨迹表中保存的单证类型，确定通知书样式
        String tCode = mLOPRTManagerSchema.getCode();
        texttag1.add("JetFormType", tCode);
        // ----------------------------------------

        if (comcode.equals("86") || comcode.equals("8600")
                || comcode.equals("86000000"))
        {
            comcode = "86";
        }
        else if (comcode.length() >= 4)
        {
            comcode = comcode.substring(0, 4);
        }
        else
        {
            this.mErrors.copyAllErrors(tLDComDB.mErrors);
            throw new Exception("操作员机构查询出错!");
        }
        String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"
                + comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        texttag1.add("ManageComLength4", printcode);
        texttag1.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        if (mflag.equals("batch"))
        {
            texttag1.add("previewflag", "0");
        }
        else
        {
            texttag1.add("previewflag", "1");
        }

        //不显示绑定型附加险
        String checkAppendRisk = "  and RiskCode not in "
                + "   (select Code from LDCode1 "
                + "   where CodeType = 'checkappendrisk' "
                + "      and RiskWrapPlanName is not null) ";

        // 查出所有的险种投保单
        String strsql = "SELECT a.* FROM LCPol a,LCUWMaster b WHERE a.ContNo = '"
                + strContNo
                + "' and a.polno=b.polno and a.UWFlag in ('4','9') "
                + checkAppendRisk + " order by a.PolNo";
        if (!StrTool.cTrim(tLCContDB.getCardFlag()).equals("")
                && !StrTool.cTrim(tLCContDB.getCardFlag()).equals("0"))
        {
            strsql = "SELECT a.* FROM LCPol a WHERE a.ContNo = '" + strContNo
                    + "' " + checkAppendRisk + "order by a.PolNo";
        }
        LCPolDB tempLCPolDB = new LCPolDB();
        System.out.println("$$ : " + strsql);
        LCPolSet tLCPolSet = tempLCPolDB.executeQuery(strsql);

        if (tempLCPolDB.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tempLCPolDB.mErrors);
            throw new Exception("在获取附加险投保单时出错！");
        }
        String[] RiskInfoTitle = new String[11];

        RiskInfoTitle[0] = "RiskCode";
        RiskInfoTitle[1] = "InsuredName";
        RiskInfoTitle[2] = "Prem";
        RiskInfoTitle[3] = "PremAdd";
        RiskInfoTitle[4] = "PremSum";
        RiskInfoTitle[5] = "YearsIntv";
        RiskInfoTitle[6] = "PayYearsIntv";
        RiskInfoTitle[7] = "AM";
        RiskInfoTitle[8] = "PayIntv";
        RiskInfoTitle[9] = "SupplementaryPrem";
        RiskInfoTitle[10] = "RiskName";
        ListTable tListTable = new ListTable();
        String StrRiskInfo[] = null;
        tListTable.setName("RiskInfo");

        for (int nIndex = 0; nIndex < tLCPolSet.size(); nIndex++)
        {
            NeedAdd = true;
            mLCPolSchema = tLCPolSet.get(nIndex + 1).getSchema();
            StrRiskInfo = new String[11];

            //            LMRiskDB tLMRiskDB = new LMRiskDB();
            //            tLMRiskDB.setRiskCode(mLCPolSchema.getRiskCode());
            //            if (!tLMRiskDB.getInfo()) {
            //                mErrors.copyAllErrors(tLMRiskDB.mErrors);
            //                buildError("outputXML", "在取得主险LMRisk的数据时发生错误");
            //
            //            }
            LMRiskAppSchema tLMRiskAppSchema = this.mCRI
                    .findRiskAppByRiskCode(mLCPolSchema.getRiskCode());
            StrRiskInfo[10] = tLMRiskAppSchema.getRiskName();

            String sql = "select RiskWrapPlanName from LDCode1 "
                    + "where CodeType = 'checkappendrisk' " + "  and Code1='"
                    + mLCPolSchema.getRiskCode() + "' ";
            String riskWrapPlanName = new ExeSQL().getOneValue(sql);
            if (!"".equals(riskWrapPlanName)
                    && !"null".equals(riskWrapPlanName))
            {
                StrRiskInfo[10] = riskWrapPlanName;
            }

            // StrRiskInfo[1] = (new Double(mLCPolSchema.getAmnt())).toString();
            //StrRiskInfo[2] = (new Integer(mLCPolSchema.getPayYears())).toString();
            StrRiskInfo[0] = mLCPolSchema.getRiskCode();
            StrRiskInfo[1] = mLCPolSchema.getInsuredName();
            String strSQL = "SELECT SUM(Prem) FROM LCPrem WHERE" + " PolNo = '"
                    + mLCPolSchema.getPolNo() + "'"
                    + " AND PayPlanCode NOT LIKE '000000%'";

            if (tLMRiskAppSchema.getNotPrintPol() != null)
            {
                if (tLMRiskAppSchema.getNotPrintPol().equals("1"))
                {
                    strSQL = "SELECT SUM(Prem) FROM LCPrem WHERE "
                            + "PolNo in (select PolNo from LCPol where "
                            + "ContNo='"
                            + this.mLCPolSchema.getContNo()
                            + "' and insuredno='"
                            + this.mLCPolSchema.getInsuredNo()
                            + "'  and RiskCode in (select RiskCode from  LDRiskParamPrint "
                            + " where ParamType1='sumprem' and ParamValue1='"
                            + mLCPolSchema.getRiskCode() + "')"
                            //+ " and (Polno = '" + mLCPolSchema.getPolNo() + "' or MainPolNo = '" + mLCPolSchema.getPolNo() + "') " 
                            + ")" + " AND PayPlanCode NOT LIKE '000000%'";
                }
                if (tLMRiskAppSchema.getNotPrintPol().equals("0"))
                {
                    strSQL = "select '--' from dual";
                    NeedAdd = false;
                }
            }

            ExeSQL exeSQL = new ExeSQL();

            SSRS ssrs = exeSQL.execSQL(strSQL);

            if (exeSQL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(exeSQL.mErrors);
                throw new Exception("取标准保费时出错");
            }

            if (!(ssrs.GetText(1, 1).equals("0")
                    || ssrs.GetText(1, 1).trim().equals("") || ssrs.GetText(1,
                    1).equals("null")))
            {
                fPrem = ssrs.GetText(1, 1);
            }
            else
            {
                fPrem = "0";
            }

            StrRiskInfo[2] = fPrem;
            //追加保费
            fSupplementaryPrem += mLCPolSchema.getSupplementaryPrem();
            StrRiskInfo[9] = mLCPolSchema.getSupplementaryPrem() + "";

            // 取加费
            String strSql = "SELECT SUM(Prem) FROM LCPrem WHERE" + " PolNo = '"
                    + mLCPolSchema.getPolNo() + "'"
                    + " AND PayPlanCode LIKE '000000%'";

            if (tLMRiskAppSchema.getNotPrintPol() != null)
            {
                if (tLMRiskAppSchema.getNotPrintPol().equals("1"))
                {
                    strSql = "SELECT SUM(Prem) FROM LCPrem WHERE "
                            + "PolNo in (select PolNo from LCPol where "
                            + "ContNo='"
                            + this.mLCPolSchema.getContNo()
                            + "' and insuredno='"
                            + this.mLCPolSchema.getInsuredNo()
                            + "'  and RiskCode in (select RiskCode from LDRiskParamPrint "
                            + " where ParamType1='sumprem' and ParamValue1='"
                            + mLCPolSchema.getRiskCode() + "')"
                            //+ " and (Polno = '" + mLCPolSchema.getPolNo() + "' or MainPolNo = '" + mLCPolSchema.getPolNo() + "') " 
                            + ")" + " AND PayPlanCode LIKE '000000%'";
                }
                if (tLMRiskAppSchema.getNotPrintPol().equals("0"))
                {
                    strSql = "select '--' from dual";
                    NeedAdd = false;
                }
            }

            exeSQL = new ExeSQL();

            ssrs = exeSQL.execSQL(strSql);

            if (exeSQL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(exeSQL.mErrors);
                throw new Exception("取加费时出错");
            }

            if (!(ssrs.GetText(1, 1).equals("0")
                    || ssrs.GetText(1, 1).trim().equals("") || ssrs.GetText(1,
                    1).equals("null")))
            {
                fPremAdd = ssrs.GetText(1, 1);
            }
            else
            {
                fPremAdd = "0";
            }
            StrRiskInfo[3] = StrTool.cTrim(fPremAdd);
            double fSum = 0.00;
            if (NeedAdd)
            {
                fPremSum += PubFun.setPrecision(Double.parseDouble(fPrem),
                        "0.00");
                fPremAddSum += PubFun.setPrecision(
                        Double.parseDouble(fPremAdd), "0.00");
                fSum = PubFun
                        .setPrecision(Double.parseDouble(fPremAdd), "0.00")
                        + PubFun
                                .setPrecision(Double.parseDouble(fPrem), "0.00");
                String strSumPrem = StrTool.cTrim(String.valueOf(fSum));

                StrRiskInfo[4] = (new ExeSQL()).getOneValue("select varchar("
                        + strSumPrem + ") from dual");
            }
            else
            {
                StrRiskInfo[4] = "--";
            }

            //StrRiskInfo[5] = (setPrecision(mLCPolSchema.getPrem(),"0.00")).toString();
            //            StrRiskInfo[5] = fSum;

            strSQL = "select rtrim(char(InsuYear))||case InsuYearFlag when 'Y' then '年' when 'M' then '月' when 'D' then '天' when 'A' then '岁' else '年' end from LCPol where"
                    + " PolNo = '" + mLCPolSchema.getPolNo() + "'";
            exeSQL = new ExeSQL();
            ssrs = exeSQL.execSQL(strSQL);
            if (tLCPolSet.get(nIndex + 1).getInsuYearFlag().equals("A")
                    && (tLCPolSet.get(nIndex + 1).getInsuYear() == 1000 || tLCPolSet
                            .get(nIndex + 1).getInsuYear() == 106))
            {
                StrRiskInfo[5] = "终身";
            }
            if (tLCPolSet.get(nIndex + 1).getInsuYearFlag().equals("A")
                    && (tLCPolSet.get(nIndex + 1).getInsuYear() != 1000 && tLCPolSet
                            .get(nIndex + 1).getInsuYear() != 106))
            {
                StrRiskInfo[5] = "至" + tLCPolSet.get(nIndex + 1).getInsuYear()
                        + "岁";
            }
            if (tLCPolSet.get(nIndex + 1).getInsuYearFlag().toUpperCase()
                    .equals("M"))
            {
                StrRiskInfo[5] = tLCPolSet.get(nIndex + 1).getInsuYear() + "月";
            }
            if (tLCPolSet.get(nIndex + 1).getInsuYearFlag().toUpperCase()
                    .equals("Y"))
            {
                StrRiskInfo[5] = tLCPolSet.get(nIndex + 1).getInsuYear() + "年";
            }
            if (tLCPolSet.get(nIndex + 1).getInsuYearFlag().toUpperCase()
                    .equals("D"))
            {
                StrRiskInfo[5] = tLCPolSet.get(nIndex + 1).getInsuYear() + "日";
            }
            if (tLCPolSet.get(nIndex + 1).getPayIntv() == 0)
            {
                StrRiskInfo[6] = "-";
            }
            else
            {
                strSQL = "select rtrim(char(PayEndYear))||case PayEndYearFlag when 'Y' then '年' when 'M' then '月' when 'D' then '天' when 'A' then '岁' else '年' end from LCPol where"
                        + " PolNo = '" + mLCPolSchema.getPolNo() + "'";
                exeSQL = new ExeSQL();
                ssrs = exeSQL.execSQL(strSQL);
                StrRiskInfo[6] = StrTool.cTrim(ssrs.GetText(1, 1));
                if (StrRiskInfo[6].endsWith("岁"))
                {
                    StrRiskInfo[6] = "至" + StrRiskInfo[7];
                }
            }
            strSQL = "select case when (Mult = 0 or Mult = 10) then Amnt else integer(Mult) end from LCPol where"
                    + " PolNo = '" + mLCPolSchema.getPolNo() + "'";
            exeSQL = new ExeSQL();
            ssrs = exeSQL.execSQL(strSQL);
            if (ssrs.GetText(1, 1).length() == 1)
            {
                StrRiskInfo[7] = ssrs.GetText(1, 1) + '档';
            }
            else
            {
                StrRiskInfo[7] = StrTool.cTrim(ssrs.GetText(1, 1)) + "元";
            }
            /**@author:Yangming 添加缴费频次 */
            strSQL = "select codename from ldcode a,lcpol b where b.polno='"
                    + mLCPolSchema.getPolNo()
                    + "' and a.code=char(b.payintv) and a.codetype='payintv' and conttype='1'";
            String tPayIntv = exeSQL.getOneValue(strSQL);
            if (tPayIntv == null || tPayIntv.equals("")
                    || tPayIntv.equals("null"))
            {
                buildError("getPrintData", "查找险种" + mLCPolSchema.getRiskCode()
                        + "缴费频次失败！");
                throw new Exception("查找险种" + mLCPolSchema.getRiskCode()
                        + "缴费频次失败！");
            }
            StrRiskInfo[8] = tPayIntv;
            tListTable.add(StrRiskInfo);
        }
        /** 动态行数，为了将显示等分 */
        ListTable tCountTable = new ListTable();

        String[] tCount = new String[1];
        //tCountTable.setName("Blank");
        // tCount[0] = "Blank";
        int count = sumCount - tLCPolSet.size();
        //  if (count > 0)
        //  {
        //            for (int i = 1; i <= count; i++)
        //            {
        //                String[] blank = { "" };
        //                tCountTable.add(blank);
        //            }
        // }

        XmlExport xmlExport = new XmlExport(); //新建一个XmlExport的实例
        System.out.println(" mLoadFlag : " + mLoadFlag);
        if (StrTool.cTrim(mLoadFlag).equals("Back"))
        {
            xmlExport.createDocument("FirstPayNoticeBack.vts", "");
        }
        else
        {
            xmlExport.createDocument("FirstPayNotice.vts", ""); //最好紧接着就初始化xml文档
        }
        xmlExport.addTextTag(texttag1);

        TextTag texttag = new TextTag();
        texttag.add("BarCode1", mLOPRTManagerSchema.getPrtSeq());
        texttag
                .add(
                        "BarCodeParam1",
                        "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("XI_AppntNo", tLCContDB.getAppntNo());
        texttag.add("AppntName", tLCContDB.getAppntName());
        System.out.println("==============================");
        System.out.println("tLCContDB.getAppntName() : "
                + tLCContDB.getAppntName());
        System.out.println("tLCContDB.getAppntName() : "
                + StrTool.unicodeToGBK(tLCContDB.getAppntName()));
        if (tLCContDB.getAppntSex().equals("1"))
        {
            texttag.add("Title", "女士");
        }
        else
        {
            texttag.add("Title", "先生");
        }
        texttag.add("ContNo", tLCContDB.getContNo());
        /*        texttag.add("Cavidate",
         tLCContDB.getCValiDate().split("-")[0] + "年" +
         tLCContDB.getCValiDate().split("-")[1] + "月" +
         tLCContDB.getCValiDate().split("-")[2] + "日");
         */texttag.add("AppntAddr", getAppAddr(tLCContDB.getContNo()));
        texttag.add("AppntZipcode", getAppZipcode(tLCContDB.getContNo()));

        texttag.add("PrtNo", tLCContDB.getPrtNo());
        texttag.add("prtSeq", mLOPRTManagerSchema.getPrtSeq());

        texttag.add("AgentName", this.mLAAgentSchema.getName());
        //2014-11-3   杨阳
        //业务员编码显示为表LAagent中的GroupAgentCode字段
        String groupAgentcode = new ExeSQL().getOneValue("select getUniteCode("+tLCContDB.getAgentCode()+") from dual");
        texttag.add("AgentCode",groupAgentcode);
        //add by zhangxing 增加代理人信息

        texttag.add("AgentGroupName", tLABranchGroupSet.get(1).getBranchAttr());
        if (tLDComSet == null || tLDComSet.size() == 0)
        {
            texttag.add("GlobalServicePhone", "");
        }
        else
        {
            texttag.add("GlobalServicePhone", tLDComSet.get(1)
                    .getServicePhone());
        }

        if (!tLDComDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLDComDB.mErrors);
            throw new Exception("读取管理机构错误!");
        }
        texttag.add("XI_ManageCom", tLCContDB.getManageCom());
        texttag.add("ManageCom", tLDComDB.getLetterServiceName());
        texttag.add("ManageAddress", tLDComDB.getServicePostAddress());
        texttag.add("ManageZipCode", tLDComDB.getLetterServicePostZipcode());
        texttag.add("ManageFax", tLDComDB.getFax());
        texttag.add("ManagePhone", tLDComDB.getPhone());
        texttag.add("InsuredName", tLCContDB.getInsuredName());
        System.out.println("=======tLCContDB.getInsuredName() : "
                + tLCContDB.getInsuredName());
        texttag.add("PrtSeq", mLOPRTManagerSchema.getPrtSeq());
        texttag.add("LCPol.prtno", tLCContDB.getPrtNo());
        texttag.add("MangeCom", tLDComDB.getServiceName());
        //2014-11-3   杨阳
        //业务员编码显示为表LAagent中的GroupAgentCode字段
        groupAgentcode = new ExeSQL().getOneValue("select getUniteCode("+tLCContDB.getAgentCode()+") from dual");
        texttag.add("Agentcode", groupAgentcode);
        texttag.add("Prem", fPremSum);
        texttag.add("PremAdd", fPremAddSum);
        texttag.add("PayMoneyCHS", PubFun.getChnMoney(fPremSum + fPremAddSum
                + fSupplementaryPrem));
        texttag.add("Bank", getBankName(tLCContDB.getBankCode()));
        texttag.add("BankID", tLCContDB.getBankAccNo());
        texttag.add("Account", tLCContDB.getAccName());
        texttag.add("PayMode", getPayMode(tLCContDB.getPayMode()));
        texttag.add("AgentPhone", getAgentPhone());
        texttag.add("OperatorName", getOperatorName(mGlobalInput.Operator));
        texttag.add("OperatorCode", mGlobalInput.Operator);
        texttag.add("PayDate", getDate((new FDate()).getDate(PubFun
                .getCurrentDate())));

        // 加入投保件归档号。
        String tArchiveNo = "";
        String tStrSql = "select * from es_doc_main where doccode = '"
                + tLCContDB.getPrtNo() + "'";
        ES_DOC_MAINDB tES_DOC_MAINBDB = new ES_DOC_MAINDB();
        ES_DOC_MAINSet tES_DOC_MAINSet = tES_DOC_MAINBDB.executeQuery(tStrSql);
        if (tES_DOC_MAINSet.size() > 1)
        {
            buildError("getPrintData", "系统中相关扫描件出现多份！");
            throw new Exception("系统中相关扫描件出现多份！");
        }
        if (tES_DOC_MAINSet.size() == 1
                && tES_DOC_MAINSet.get(1).getArchiveNo() != null)
        {
            tArchiveNo = tES_DOC_MAINSet.get(1).getArchiveNo();
        }
        texttag.add("ArchiveNo", tArchiveNo);
        // --------------------------------------

        // 调整缴费通知书，新增节点信息。2008-01-27

        // 先收费标志。0：先收费；""：正常
        texttag.add("PayLocation", StrTool.cTrim(tLCContDB.getPayLocation()));

        // 保单关联的缴费凭证号
        texttag.add("TempFeeNo", StrTool.cTrim(tLCContDB.getTempFeeNo()));

        // 实际到帐保费（对后收费的保单该值为0）
        String tEnterAccFee = getEnterAccFee(tLCContDB.getSchema());
        texttag.add("EnterAccFee", tEnterAccFee);

        texttag.add("DifMoney", getDifMoney(fPremSum, fPremAddSum
                + fSupplementaryPrem, tEnterAccFee));

        // --------------------------------------

        // 增加银行帐户节点。目前没确定如何取，先置空。
        texttag.add("BankCode", "");
        texttag.add("BankAccCode", "");
        texttag.add("AccName", "");
        // --------------------

        if (StrTool.cTrim(tLCContDB.getPayMode()).equals("4"))
        {
            Date makeDate = (new FDate()).getDate(mLOPRTManagerSchema
                    .getMakeDate());
            texttag.add("Today", getDate(makeDate));
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(makeDate);
            calendar.add(Calendar.DATE, 3);
            texttag.add("Cavidate", getDate(calendar.getTime()));
        }
        else
        {
            Date makeDate = (new FDate()).getDate(mLOPRTManagerSchema
                    .getMakeDate());
            texttag.add("Today", getDate(makeDate));
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(makeDate);
            calendar.add(Calendar.DATE, 10);
            texttag.add("Cavidate", getDate(calendar.getTime()));
        }
        Date Cavidate1 = (new FDate()).getDate(tLCContDB.getCValiDate());
        texttag.add("Cavidate1", getDate(Cavidate1));

        if (texttag.size() > 0)
        {
            xmlExport.addTextTag(texttag);
        }

        if (tCountTable.size() > 0)
        {
            xmlExport.addListTable(tCountTable, tCount);
        }
        if (tLCContDB.getPayMode() == null || tLCContDB.getPayMode() == "")
        {
            //mErrors.copyAllErrors(tLCContDB.mErrors);
            throw new Exception("没有录入缴费方式！");
        }
        if (tLCContDB.getPayMode().equals("4"))
        {
            xmlExport.addDisplayControl("displaybank");
        }
        else
        {
            xmlExport.addDisplayControl("displaymoney");
        }

        xmlExport.addListTable(tListTable, RiskInfoTitle);

        ListTable tEndTable = new ListTable();
        tEndTable.setName("END");
        String[] tEndTitle = new String[0];
        xmlExport.addListTable(tEndTable, tEndTitle);
        TextTag texttag2 = new TextTag();
        texttag2.add("PremSum", String.valueOf(fPremSum + fPremAddSum
                + fSupplementaryPrem));
        if (texttag2.size() > 0)
        {
            xmlExport.addTextTag(texttag2);
        }

        xmlExport.outputDocumentToFile("d:\\", "firtpay");
        mResult.clear();
        mResult.addElement(xmlExport);
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setDoneDate(CurrentDate);
        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
        mLOPRTManagerSchema
                .setPrintTimes(mLOPRTManagerSchema.getPrintTimes() + 1);
        mResult.add(mLOPRTManagerSchema);

        return true;
    }

    private LCContDB getContInfo() throws Exception
    {
        LCContDB tLCContDB = new LCContDB();
        // 打印时传入的是主险投保单的投保单号
        tLCContDB.setContNo(mLOPRTManagerSchema.getOtherNo());
        LCContSet tLCContSet = tLCContDB.query();
        if (tLCContSet.size() <= 0)
        {
            throw new Exception("查询合同信息失败！");
        }
        if (tLCContSet.size() > 1)
        {
            throw new Exception("查询合同信息失败！");
        }
        //        if (!tLCContDB.getInfo()) {
        //            mErrors.copyAllErrors(tLCContDB.mErrors);
        //            throw new Exception("在获取保单信息时出错！");
        //        }
        tLCContDB.setSchema(tLCContSet.get(1).getSchema());
        return tLCContDB;
    }

    /**
     * getPayMode
     *
     * @param tPayMode String
     * @return String
     */
    private String getPayMode(String tPayMode)
    {
        return StrTool.cTrim(tPayMode).equals("1") ? "现金" : "银行转帐";
    }

    private void getAgentInfo(LCContDB tLCContDB)
    {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tLCContDB.getAgentCode());
        if (!tLAAgentDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
        }
        mLAAgentSchema = tLAAgentDB.getSchema();
    }

    private String getAddrNo(String strContNo) throws Exception
    {
        LCAppntDB tLCAppntDB = new LCAppntDB();

        tLCAppntDB.setContNo(strContNo);
        if (tLCAppntDB.getInfo())
        {
            return tLCAppntDB.getAddressNo();
        }
        else
        {
            return null;
        }
    }

    private String getAppAddr(String strContNo) throws Exception
    {

        LCAddressDB tLCAddressDB = new LCAddressDB();

        tLCAddressDB.setCustomerNo(getAppNo(strContNo));
        tLCAddressDB.setAddressNo(getAddrNo(strContNo));

        if (tLCAddressDB.getInfo())
        {
            return tLCAddressDB.getPostalAddress();
        }
        else
        {
            return null;
        }
    }

    private String getAppZipcode(String strContNo) throws Exception
    {

        LCAddressDB tLCAddressDB = new LCAddressDB();

        tLCAddressDB.setCustomerNo(getAppNo(strContNo));
        tLCAddressDB.setAddressNo(getAddrNo(strContNo));

        if (tLCAddressDB.getInfo())
        {
            return tLCAddressDB.getZipCode();
        }
        else
        {
            return null;
        }
    }

    private String getDate(Date date)
    {

        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
        return df.format(date);
    }

    private String getBankName(String strBankCode) throws Exception
    {
        //LDCodeDB tLDCodeDB = new LDCodeDB();
        return ChangeCodeBL.getCodeName("Bank", strBankCode, "BankCode");
        //        tLDCodeDB.setCode(strBankCode);
        //        tLDCodeDB.setCodeType("bank");
        //        if (tLDCodeDB.getInfo()) {
        //            return tLDCodeDB.getCodeName();
        //        } else {
        //            return null;
        //        }
    }

    private String getOperatorName(String tOperatorCode)
    {
        String name = (new ExeSQL())
                .getOneValue("select username from lduser where usercode='"
                        + tOperatorCode + "'");
        if (name == null || name.equals("") || name.equals("null"))
        {
            return "";
        }
        return name;
    }

    private String getAppNo(String strContNo) throws Exception
    {
        LCAppntDB tLCAppntDB = new LCAppntDB();

        tLCAppntDB.setContNo(strContNo);
        if (tLCAppntDB.getInfo())
        {
            return tLCAppntDB.getAppntNo();
        }
        else
        {
            return null;
        }
    }

}
