package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import java.io.File;
import com.sinosoft.lis.easyscan.ProposalDownloadBL;

/**
 * <p>Title: 分保计算结果明细</p>
 * <p>Description: 分保计算结果明细清单</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author huxl
 * @version 1.0
 * @date 2007-03-31
 */

public class ReinsureCessListBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    String StartDate = "";
    String EndDate = "";
    String ReRiskSort = "";
    String RecontCode = "";
    String ReRiskCode = "";
    String SysPath = ""; //存放生成文件的路径
    String FullPath = "";
    public ReinsureCessListBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cSysPath) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        SysPath = cSysPath;
        // 准备所有要打印的数据
        if (!downloadCessData()) {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        TransferData tTransferData = (TransferData) cInputData.
                                     getObjectByObjectName("TransferData", 0);
        StartDate = (String) tTransferData.getValueByName("StartDate");
        EndDate = (String) tTransferData.getValueByName("EndDate");
        ReRiskSort = (String) tTransferData.getValueByName("ReRiskSort");
        RecontCode = (String) tTransferData.getValueByName("RecontCode");
        ReRiskCode = (String) tTransferData.getValueByName("ReRiskCode");
        return true;
    }

    public String getResult() {
        return FullPath;
    }

    public CErrors getErrors() {
        return mErrors;
    }


    private boolean downloadCessData() {
        try {
            SysPath = SysPath + "reinsure/";
            String Path = PubFun.getCurrentDate() + "-" + PubFun.getCurrentTime() +
                          ".xls";
            Path = Path.replaceAll(":", "-");
            WriteToExcel tWriteToExcel = new WriteToExcel(Path);
            String[] FilePaths = new String[1];
            FilePaths[0] = SysPath + Path;
            String[] FileNames = new String[1];
            FileNames[0] = "ReCessData" +
                           ((RecontCode == null || RecontCode.equals("")) ? "" :
                            "_" + RecontCode) +
                           ((ReRiskCode == null || ReRiskCode.equals("")) ? "" :
                            "_" + ReRiskCode) +
                           ".xls";
            tWriteToExcel.createExcelFile();
            String[] sheetName = {StartDate + "__" + EndDate};
            tWriteToExcel.addSheet(sheetName);

            if (ReRiskSort.equals("1")) {
                String sql = " and ((a.GetDataDate between '" + StartDate +
                             "' and '" + EndDate + "' and c.GetDataDate <= '" +
                             EndDate + "') or (a.GetDataDate < '" + StartDate +
                             "' and c.GetDataDate between '" + StartDate +
                             "' and '" + EndDate + "')) ";
                if (RecontCode != null && !RecontCode.equals("")) {
                    sql += "and a.RecontCode = '" + RecontCode + "' ";
                }
                if (ReRiskCode != null && !ReRiskCode.equals("")) {
                    sql += "and a.RiskCode = '" + ReRiskCode + "' ";
                }
                String sql1 = "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,'',a.SignDate,(select CodeName " +
                              "from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'),c.PayConfDate, " +
                              "'',a.AppntNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo), " +
                              "a.OccupationType,a.Amnt,char(a.InsuYear)||a.InsuYearFlag,a.Prem,c.PayMoney,d.ShouldCount,d.RemainCount,d.ShouldCessPrem," +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,d.CessionRate,'',d.CessPrem, " +
                              "d.ReProcFeeRate,d.SpeCemmRate,d.ReProcFee " +
                              "from LRPol a,LCCont b,LRPolDetail c,LRPolResult d where a.ContNo = b.ContNo and a.PolNo " +
                              "= c.Polno and a.PolNo = d.Polno and a.RecontCode = d.RecontCode and a.ContType = '1' " +
                              "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount and c.PayCount = d.PayCount and a.RiskCalSort = '1' " +
                              sql +
                              "union all " +
                              "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,'',a.SignDate,(select CodeName " +
                              "from LDCode where Code = char(a.PayIntv) and codetype = 'payintv'),c.PayConfDate, " +
                              "'',a.AppntNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo), " +
                              "a.OccupationType,a.Amnt,char(a.InsuYear)||a.InsuYearFlag,a.Prem,c.PayMoney,d.ShouldCount,d.RemainCount,d.ShouldCessPrem, " +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,d.CessionRate,'',d.CessPrem, " +
                              "d.ReProcFeeRate,d.SpeCemmRate,d.ReProcFee " +
                              "from LRPol a,LBCont b,LRPolDetail c,LRPolResult d where a.ContNo = b.ContNo and a.PolNo " +
                              "= c.Polno and a.PolNo = d.Polno and a.RecontCode = d.RecontCode and a.ContType = '1' " +
                              "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount and c.PayCount = d.PayCount and a.RiskCalSort = '1' " +
                              sql +
                              "union all  " +
                              "select a.ManageCom,a.RiskCode,a.GrpContNo,b.CValidate,b.CInvalidate,char(a.CValidate),a.SignDate,(select CodeName " +
                              "from LDCode where Code = char(a.PayIntv) and codetype = 'payintv'),c.PayConfDate, " +
                              "a.AppntName,a.AppntNo,char(count(a.grpcontno)),char(avg(a.InsuredAppAge))," +
                              "'','','',sum(a.Amnt),char(max(a.InsuYear))||a.InsuYearFlag,sum(a.Prem),sum(c.PayMoney),d.ShouldCount,d.RemainCount,sum(d.ShouldCessPrem), " +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,d.CessionRate,'',sum(d.CessPrem), " +
                              "d.ReProcFeeRate,d.SpeCemmRate,sum(d.ReProcFee) " +
                              "from LRPol a,LCGrpCont b,LRPolDetail c,LRPolResult d where a.GrpContNo = b.GrpContNo and a.PolNo " +
                              "= c.Polno and a.PolNo = d.Polno and a.RecontCode = d.RecontCode and a.ContType = '2' " +
                              "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount and c.PayCount = d.PayCount and a.RiskCalSort = '1' " +
                              sql + "group by a.ManageCom,a.RiskCode,a.GrpContNo,b.CValidate,b.CInvalidate,a.CValidate,a.SignDate," +
                              "a.PayIntv,c.PayConfDate,a.AppntName,a.AppntNo,a.InsuYearFlag,a.TempCessFlag,d.CessionRate,d.ReProcFeeRate," +
                              "d.SpeCemmRate,d.ShouldCount,d.RemainCount " +
                              "union all " +
                              "select a.ManageCom,a.RiskCode,a.GrpContNo,b.CValidate,b.CInvalidate,char(a.CValidate),a.SignDate,(select CodeName " +
                              "from LDCode where Code = char(a.PayIntv) and codetype = 'payintv'),c.PayConfDate, " +
                              "a.AppntName,a.AppntNo,char(count(a.grpcontno)),char(avg(a.InsuredAppAge))," +
                              "'','','',sum(a.Amnt),char(max(a.InsuYear))||a.InsuYearFlag,sum(a.Prem),sum(c.PayMoney),d.ShouldCount,d.RemainCount,sum(d.ShouldCessPrem), " +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end,d.CessionRate,'',sum(d.CessPrem), " +
                              "d.ReProcFeeRate,d.SpeCemmRate,sum(d.ReProcFee) " +
                              "from LRPol a,LBGrpCont b,LRPolDetail c,LRPolResult d where a.GrpContNo = b.GrpContNo and a.PolNo " +
                              "= c.Polno and a.PolNo = d.Polno and a.RecontCode = d.RecontCode and a.ContType = '2' " +
                              "and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount and c.PayCount = d.PayCount and a.RiskCalSort = '1' " +
                              sql + "group by a.ManageCom,a.RiskCode,a.GrpContNo,b.CValidate,b.CInvalidate,a.CValidate,a.SignDate," +
                              "a.PayIntv,c.PayConfDate,a.AppntName,a.AppntNo,a.InsuYearFlag,a.TempCessFlag,d.CessionRate,d.ReProcFeeRate," +
                              "d.SpeCemmRate ,d.ShouldCount,d.RemainCount with ur ";
                String[][] strArr1Head = new String[1][30];
                strArr1Head[0][0] = "机构代码";
                strArr1Head[0][1] = "产品代码";
                strArr1Head[0][2] = "保单号";
                strArr1Head[0][3] = "保单生效日期";
                strArr1Head[0][4] = "保单终止日期";
                strArr1Head[0][5] = "团险增人生效日期";
                strArr1Head[0][6] = "签单日期";
                strArr1Head[0][7] = "缴费方式";
                strArr1Head[0][8] = "缴费日期";

                strArr1Head[0][9] = "团体名称";
                strArr1Head[0][10] = "客户号";
                strArr1Head[0][11] = "被保险人人数/姓名";
                strArr1Head[0][12] = "平均年龄/生日";
                strArr1Head[0][13] = "性别";
                strArr1Head[0][14] = "身份证号码";
                strArr1Head[0][15] = "职业类别";
                strArr1Head[0][16] = "限额/保额";
                strArr1Head[0][17] = "保险期间";
                strArr1Head[0][18] = "期交保费";
                strArr1Head[0][19] = "当期保费";
                strArr1Head[0][20] = "应缴期数";
                strArr1Head[0][21] = "待缴期数";
                strArr1Head[0][22] = "全年应收保费收入";

                strArr1Head[0][23] = "分保类型";
                strArr1Head[0][24] = "成数分出比例";
                strArr1Head[0][25] = "成数分出保额";
                strArr1Head[0][26] = "成数分出保费";
                strArr1Head[0][27] = "分保手续费率";
                strArr1Head[0][28] = "特殊佣金费率";
                strArr1Head[0][29] = "分保手续费";
                tWriteToExcel.setData(0, strArr1Head);
                System.out.println(sql1);
                tWriteToExcel.setData(0, sql1);
            } else if (ReRiskSort.equals("2")) {
                String sql = " and a.GetDataDate between '" + StartDate +
                             "' and '" + EndDate + "' ";
                if (RecontCode != null && !RecontCode.equals("")) {
                    sql += " and a.RecontCode = '" + RecontCode + "' ";
                }
                if (ReRiskCode != null && !ReRiskCode.equals("")) {
                    sql += " and a.RiskCode = '" + ReRiskCode + "' ";
                }
                String sql1 = "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,'',a.SignDate,(select CodeName " +
                              "from LDCode where Code =char(a.PayIntv) and codetype = 'payintv')," +
                              "'',a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo), " +
                              "a.OccupationType,varchar(a.DiseaseAddFeeRate),a.Amnt,e.SumAmnt 累计保额,e.SumCessAmnt,a.StandPrem,a.Prem,'', " +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end," +
                              "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='RetainAmnt'), " +
                              "d.CessionAmount," +
                              "d.CessionRate, d.CessPremRate,d.CessPrem,d.ChoiRebaFeeRate,d.ReProcFee " +
                              "from LRPol a,LCCont b,LRPolResult d,LRPolCessAmnt e where a.ContNo = b.ContNo " +
                              "and a.PolNo = d.Polno and a.RecontCode = d.RecontCode and a.ContType = '1' " +
                              "and a.ReNewCount = d.ReNewCount and a.RiskCalSort = '2' and a.ReNewCount = e.ReNewCount and a.PolNo = e.Polno " +
                              sql +
                              "union all " +
                              "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,'',a.SignDate,(select CodeName " +
                              "from LDCode where Code =char(a.PayIntv) and codetype = 'payintv')," +
                              "'',a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo), " +
                              "a.OccupationType,varchar(a.DiseaseAddFeeRate),a.Amnt,e.SumAmnt 累计保额,e.SumCessAmnt,a.StandPrem,a.Prem,'', " +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end," +
                              "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='RetainAmnt') " +
                              ",d.CessionAmount, " +
                              "d.CessionRate, d.CessPremRate,d.CessPrem,d.ChoiRebaFeeRate,d.ReProcFee " +
                              "from LRPol a,LBCont b,LRPolResult d,LRPolCessAmnt e where a.ContNo = b.ContNo " +
                              "and a.PolNo = d.Polno and a.RecontCode = d.RecontCode and a.ContType = '1' " +
                              "and a.ReNewCount = d.ReNewCount and a.RiskCalSort = '2' and a.ReNewCount = e.ReNewCount and a.PolNo = e.Polno " +
                              sql +
                              "union all " +
                              "select a.ManageCom,a.RiskCode,a.GrpContNo,b.CValidate,b.CInvalidate,char(a.CValidate),a.SignDate,(select CodeName " +
                              "from LDCode where Code = char(a.PayIntv) and codetype = 'payintv'),a.AppntName," +
                              "a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo)," +
                              "a.OccupationType,'' 加费率,a.Amnt,e.SumAmnt 累计保额,e.SumCessAmnt,a.StandPrem,a.Prem," +
                              "varchar(div(a.prem,a.standPrem))," +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end," +
                              "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='RetainAmnt')," +
                              "d.CessionAmount,d.CessionRate, d.CessPremRate,d.CessPrem,d.ChoiRebaFeeRate,d.ReProcFee " +
                              "from LRPol a,LCGrpCont b,LRPolResult d,LRPolCessAmnt e where a.GrpContNo = b.GrpContNo " +
                              "and a.PolNo = d.Polno and a.RecontCode = d.RecontCode and a.ContType = '2' " +
                              "and a.ReNewCount = d.ReNewCount and a.RiskCalSort = '2' and a.ReNewCount = e.ReNewCount and a.PolNo = e.Polno " +
                              sql +
                              "union all " +
                              "select a.ManageCom,a.RiskCode,a.GrpContNo,b.CValidate,b.CInvalidate,char(a.CValidate),a.SignDate,(select CodeName " +
                              "from LDCode where Code = char(a.PayIntv) and codetype = 'payintv'),a.AppntName," +
                              "a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo)," +
                              "a.OccupationType,'' 加费率,a.Amnt,e.SumAmnt 累计保额,e.SumCessAmnt,a.StandPrem,a.Prem," +
                              "varchar(div(a.prem,a.standPrem))," +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end," +
                              "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='RetainAmnt')," +
                              "d.CessionAmount,d.CessionRate, d.CessPremRate,d.CessPrem,d.ChoiRebaFeeRate,d.ReProcFee " +
                              "from LRPol a,LBGrpCont b,LRPolResult d,LRPolCessAmnt e where a.GrpContNo = b.GrpContNo " +
                              "and a.PolNo = d.Polno and a.RecontCode = d.RecontCode and a.ContType = '2' " +
                              "and a.ReNewCount = d.ReNewCount and a.RiskCalSort = '2' and a.ReNewCount = e.ReNewCount and a.PolNo = e.Polno " +
                              sql + " with ur";
                String[][] strArr1Head = new String[1][31];
                strArr1Head[0][0] = "机构代码";
                strArr1Head[0][1] = "产品代码";
                strArr1Head[0][2] = "保单号";
                strArr1Head[0][3] = "保单生效日期";
                strArr1Head[0][4] = "保单终止日期";
                strArr1Head[0][5] = "团险增人生效日期";
                strArr1Head[0][6] = "签单日期";
                strArr1Head[0][7] = "缴费方式";

                strArr1Head[0][8] = "团体名称";
                strArr1Head[0][9] = "投保人客户号";
                strArr1Head[0][10] = "被保人人客户号";
                strArr1Head[0][11] = "被保险人人数/姓名";
                strArr1Head[0][12] = "平均年龄/生日";
                strArr1Head[0][13] = "性别";
                strArr1Head[0][14] = "身份证号码";
                strArr1Head[0][15] = "职业类别";
                strArr1Head[0][16] = "加费率";
                strArr1Head[0][17] = "保额";
                strArr1Head[0][18] = "累计保额"; //目前存储在LRPolCessAmnt中
                strArr1Head[0][19] = "累计分出保额"; //目前存储在LRPolCessAmnt中
                strArr1Head[0][20] = "标准保费";
                strArr1Head[0][21] = "实收保费";
                strArr1Head[0][22] = "折扣比例";

                strArr1Head[0][23] = "分保类型";
                strArr1Head[0][24] = "自留额";
                strArr1Head[0][25] = "溢额分出保额";
                strArr1Head[0][26] = "溢额分出比例";
                strArr1Head[0][27] = "净分保费率";
                strArr1Head[0][28] = "净分保费";
                strArr1Head[0][29] = "选择折扣率";
                strArr1Head[0][30] = "分保手续费";
                tWriteToExcel.setData(0, strArr1Head);
                System.out.println(sql1);
                tWriteToExcel.setData(0, sql1);
            } else if (ReRiskSort.equals("3")) {
                String sql = " and a.GetDataDate between '" + StartDate +
                             "' and '" + EndDate + "' ";
                if (RecontCode != null && !RecontCode.equals("")) {
                    sql += " and a.RecontCode = '" + RecontCode + "' ";
                }
                if (ReRiskCode != null && !ReRiskCode.equals("")) {
                    sql += " and a.RiskCode = '" + ReRiskCode + "' ";
                }
                String sql1 = "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,'',(select max(CurPaytoDate) from " +
                              "LRPolDetail where polno = a.Polno),(select CodeName " +
                              "from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'),char(a.InsuYear)||a.InsuYearFlag," +
                              "char(a.PayEndYear)||a.PayEndYearFlag, Year(a.GetDataDate - a.CValidate)+1,'' 核保结论," +
                              "'',a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo), " +
                              "a.OccupationType,(select min(impartparam) from LCCustomerImpartParams where impartcode='020' and " +
                              "impartver='001' and customerno=a.insuredno and impartparamno='2' and proposalno=a.proposalno)," +
                              "varchar(a.DeadAddFeeRate),varchar(a.DiseaseAddFeeRate),a.Amnt,e.SumAmnt,e.SumReserve,e.SumRiskAmnt,e.SumCessAmnt,a.StandPrem,a.Prem,'', " +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end," +
                              "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='RetainAmnt'), " +
                              "d.CessionAmount,d.CessionRate, d.CessPremRate,d.CessPrem,d.ChoiRebaFeeRate,d.ReProcFee " +
                              "from LRPol a,LCCont b,LRPolResult d,LRPolCessAmnt e where a.ContNo = b.ContNo " +
                              "and a.PolNo = d.Polno and a.RecontCode = d.RecontCode and a.ContType = '1' " +
                              "and a.ReNewCount = d.ReNewCount and a.RiskCalSort = '3' and a.ReNewCount = e.ReNewCount and a.PolNo = e.Polno " +
                              sql +
                              "union all " +
                              "select a.ManageCom,a.RiskCode,a.ContNo,b.CValidate,b.CInvalidate,'',(select max(CurPaytoDate) from " +
                              "LRPolDetail where polno = a.Polno),(select CodeName " +
                              "from LDCode where Code =char(a.PayIntv) and codetype = 'payintv'),char(a.InsuYear)||a.InsuYearFlag," +
                              "char(a.PayEndYear)||a.PayEndYearFlag, Year(a.GetDataDate - a.CValidate)+1,'' 核保结论," +
                              "'',a.AppntNo,a.InsuredNo,a.InsuredName,char(a.InsuredBirthday),a.InsuredSex,(select IdNo from LDPerson where CustomerNo = a.InsuredNo), " +
                              "a.OccupationType,(select min(impartparam) from LCCustomerImpartParams where impartcode='020' and " +
                              "impartver='001' and customerno=a.insuredno and impartparamno='2' and proposalno=a.proposalno)," +
                              "varchar(a.DeadAddFeeRate),varchar(a.DiseaseAddFeeRate),a.Amnt,e.SumAmnt,e.SumReserve,e.SumRiskAmnt,e.SumCessAmnt,a.StandPrem,a.Prem,'', " +
                              "case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end," +
                              "(select distinct factorvalue from LRCalFactorValue where recontcode= a.RecontCode and FactorCode='RetainAmnt'), " +
                              "d.CessionAmount,d.CessionRate, d.CessPremRate,d.CessPrem,d.ChoiRebaFeeRate,d.ReProcFee " +
                              "from LRPol a,LBCont b,LRPolResult d,LRPolCessAmnt e where a.ContNo = b.ContNo " +
                              "and a.PolNo = d.Polno and a.RecontCode = d.RecontCode and a.ContType = '1' " +
                              "and a.ReNewCount = d.ReNewCount and a.RiskCalSort = '3' and a.ReNewCount = e.ReNewCount and a.PolNo = e.Polno " +
                              sql + " with ur ";
                String[][] strArr1Head = new String[1][39];
                strArr1Head[0][0] = "机构代码";
                strArr1Head[0][1] = "产品代码";
                strArr1Head[0][2] = "保单号";
                strArr1Head[0][3] = "保单生效日期";
                strArr1Head[0][4] = "保单终止日期";
                strArr1Head[0][5] = "团险增人生效日期";
                strArr1Head[0][6] = "交至日";
                strArr1Head[0][7] = "缴费方式";
                strArr1Head[0][8] = "保险期间";
                strArr1Head[0][9] = "缴费期间";
                strArr1Head[0][10] = "保单年度";
                strArr1Head[0][11] = "核保结论"; //暂时没有，应该增加LRPol的字段

                strArr1Head[0][12] = "团体名称";
                strArr1Head[0][13] = "投保人客户号";
                strArr1Head[0][14] = "被保人客户号";
                strArr1Head[0][15] = "被保险人姓名";
                strArr1Head[0][16] = "平均年龄/生日";
                strArr1Head[0][17] = "性别";
                strArr1Head[0][18] = "身份证号码";
                strArr1Head[0][19] = "职业类别";
                strArr1Head[0][20] = "吸烟状态";
                strArr1Head[0][21] = "死亡加费率";
                strArr1Head[0][22] = "重疾加费率";
                strArr1Head[0][23] = "原始保额";
                strArr1Head[0][24] = "累计保额";    //  目前存储在LRPolCessAmnt中
                strArr1Head[0][25] = "累计准备金";  //  目前存储在LRPolCessAmnt中
                strArr1Head[0][26] = "累计风险保额"; //  目前存储在LRPolCessAmnt中
                strArr1Head[0][27] = "累计分出保额"; //  目前存储在LRPolCessAmnt中
                strArr1Head[0][28] = "标准保费";
                strArr1Head[0][29] = "实收保费";
                strArr1Head[0][30] = "折扣比例";

                strArr1Head[0][31] = "分保类型";
                strArr1Head[0][32] = "自留额";
                strArr1Head[0][33] = "溢额分出保额";
                strArr1Head[0][34] = "溢额分出比例";
                strArr1Head[0][35] = "净分保费率";
                strArr1Head[0][36] = "净分保费";
                strArr1Head[0][37] = "选择折扣率";
                strArr1Head[0][38] = "分保手续费";
                tWriteToExcel.setData(0, strArr1Head);
                System.out.println(sql1);
                tWriteToExcel.setData(0, sql1);
            }
            tWriteToExcel.write(SysPath);
            String NewPath = PubFun.getCurrentDate() + "-" +
                             PubFun.getCurrentTime() + ".zip";
            NewPath = NewPath.replaceAll(":", "-");
            FullPath = NewPath;
            NewPath = SysPath + NewPath;
            CreateZip(FilePaths, FileNames, NewPath);
            File fd = new File(FilePaths[0]);
            fd.delete();
            System.out.println("FullPath!  : " + NewPath);

        } catch (Exception ex) {
            ex.toString();
            ex.printStackTrace();
        }
        return true;
    }

    public boolean CreateZip(String[] tFilePaths, String[] tFileNames,
                             String tZipPath) {
        ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
        if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath)) {
            System.out.println("生成压缩文件失败");
            CError.buildErr(this, "生成压缩文件失败");
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
    }
}
