package com.sinosoft.lis.f1print;

/**
 * <p>Title: FFChargeDayModeF1PExcelBL </p>
 * <p>Description:X1-实收日结单 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author yan
 * @version 1.0
 */

import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;

public class FFChargeDayModeF1PExcelBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    //取得的时间
    private String mDay[] = null;
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String tOutXmlPath = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public FFChargeDayModeF1PExcelBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getNewPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
    	//全局变量
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        tOutXmlPath = (String)cInputData.get(2);
        

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        if (tOutXmlPath == null || tOutXmlPath.equals(""))
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "FFChargeDayModeF1PExcelBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    /**
     * 获得结果集的记录总数
     * @return
     */
    private int getNumber(){
        int no = 6;
        
        String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
//        tServerPath= "C:/Users/db2admin.qqing-pc/Picch/ui/";
        ExeSQL mExeSQL = new ExeSQL();
        
        SSRS tSSRS = new SSRS();
        
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
        tGetSQLFromXML.setParameters("EndDate",   mDay[1]);
        
//      要提取的类型，对应FeePrintSql.xml里的SQL语句节点
        String[] getTypes = new String[] { 
                "X1SSMX",
                "X1YBKJS",
                "X1GBPKCWFC"
        };
        int maxrow = 0;
        //  按方式打印 收费明细SQL
        for(int i=0;i<getTypes.length;i++){
            String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", getTypes[i]);
            tSSRS = mExeSQL.execSQL(msql);
            maxrow=tSSRS.getMaxRow();

            no=no+maxrow;
        }
        // 收费日结汇总SQL
        for(int n=0;n<getTypes.length;n++){
            msql = tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", getTypes[n]+"Z");                              
            tSSRS = mExeSQL.execSQL(msql);
            maxrow=tSSRS.getMaxRow();

            no=no+maxrow;
        }
        // 收费合计SQL
        msql = tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "X1SSHJ"); 
        tSSRS = mExeSQL.execSQL(msql);
        maxrow=tSSRS.getMaxRow();

        no=no+maxrow+2;
        
        System.out.println("tMaxRowNum:" + no);
        
        return no;
    }
    /**
     * 新方法 X1-实收日结单  
     * @return
     */
    private boolean getNewPrintData(){
    	
    	SSRS mSSRS = new SSRS();
		String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
//		tServerPath= "C:/Users/db2admin.qqing-pc/Picch/ui/";
		ExeSQL mExeSQL = new ExeSQL();

		String nsql = "select Name from LDCom where ComCode='"+ mGlobalInput.ManageCom + "'"; // 管理机构
		mSSRS = mExeSQL.execSQL(nsql);
		String manageCom = mSSRS.GetText(1, 1);
        
        int tMaxRowNum = getNumber();
        
        //初始化打印数组
        String[][] mToExcel = new String[tMaxRowNum+10][7];
        mToExcel[1][0] = "财务收费日结单（方式）";
        mToExcel[2][0] = "统计日期：";
        mToExcel[2][1] = mDay[0]+"至"+mDay[1];
        mToExcel[3][0] = "统计机构：";
        mToExcel[3][1] = manageCom;
        mToExcel[6][0] = "收款方式";
        mToExcel[6][1] = "号码类型";
        mToExcel[6][2] = "号码";
        mToExcel[6][3] = "金额";
        mToExcel[6][4] = "净收费";
        mToExcel[6][5] = "税额";
        mToExcel[6][6] = "件数";
        mToExcel[4][1] = "关于暂收价税分离数据，受核心价税分离数据存储方式的限制说明如下： 1、多种缴费方式的数据其价税后的金额只能显示在某种缴费方式的一行，其他行显示为0.00  2、暂收价税后的数据无法按照缴费方式或银行账号汇总 3、健康服务管理费应总公司财务要求不在日结单显示";
       	
        int no=7;  //初始打印行数
    	
        SSRS tSSRS = new SSRS();
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
        tGetSQLFromXML.setParameters("EndDate",   mDay[1]);
        
//      要提取的类型，对应FeePrintSql.xml里的SQL语句节点
        String[] getTypes = new String[] { 
        		"X1SSMX",
        		"X1YBKJS",
        		"X1GBPKCWFC"
        };
        int maxrow = 0;
        //  按方式打印 收费明细SQL
        for(int i=0;i<getTypes.length;i++){
	        String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", getTypes[i]);
	        tSSRS = mExeSQL.execSQL(msql);
	        System.out.println("ExecSQL : " + StrTool.unicodeToGBK(msql));
			maxrow=tSSRS.getMaxRow();
	       	for (int row = 1; row <= maxrow; row++)
	        {
             mToExcel[no+row-1][0]=tSSRS.GetText(row, 1);
             mToExcel[no+row-1][1]=tSSRS.GetText(row, 7);
             mToExcel[no+row-1][2]=tSSRS.GetText(row, 6);
             mToExcel[no+row-1][3]=new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 2)));
             if (row >= 2 && ((tSSRS.GetText(row, 8)).equals(tSSRS.GetText(row-1, 8)))){
            	 System.out.println("重复啦！！！");
                	 mToExcel[no+row-1][4]= new DecimalFormat("0.00").format(Double.valueOf("0.00"));
                	 mToExcel[no+row-1][5]= new DecimalFormat("0.00").format(Double.valueOf("0.00"));           
             }
             else{
            	 System.out.println("没有重复！！！");
//            	 System.out.println("第7列值为"+tSSRS.GetText(row, 8));
//            	 System.out.println("第8列值为"+tSSRS.GetText(row, 9));
            	 System.out.println("第4列值为"+tSSRS.GetText(row, 3));
            	 if(tSSRS.GetText(row, 3)!=null&&!"".equals(tSSRS.GetText(row, 3))){
            		 System.out.println("第4列值为"+tSSRS.GetText(row, 3));
            		 mToExcel[no+row-1][4]=new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 3)));
            	 }else{
            		 System.out.println("第4列是空的");
            		 mToExcel[no+row-1][4]=new DecimalFormat("0.00").format(Double.valueOf("0.00"));
            	 }
            	 System.out.println("第5列值为"+tSSRS.GetText(row, 4));
            	 if(tSSRS.GetText(row, 4)!=null&&!"".equals(tSSRS.GetText(row, 4))){
            		 System.out.println("第5列值为"+tSSRS.GetText(row, 4));
            		 mToExcel[no+row-1][5]=new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 4)));
            	 }else{
            		 System.out.println("第5列是空的");
            		 mToExcel[no+row-1][5]=new DecimalFormat("0.00").format(Double.valueOf("0.00"));
            		 
            	 }
             }
             
             mToExcel[no+row-1][6]=tSSRS.GetText(row, 5);
	        }
	       	no=no+maxrow;
        }
       	// 收费日结汇总SQL
        for(int n=0;n<getTypes.length;n++){
	       	msql = tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", getTypes[n]+"Z");                           	
	        tSSRS = mExeSQL.execSQL(msql);
			maxrow=tSSRS.getMaxRow();
	       	for (int row = 1; row <= maxrow; row++)
	        {
             mToExcel[no+row-1][0]=tSSRS.GetText(row, 1);
             mToExcel[no+row-1][3]=new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 2)));
             mToExcel[no+row-1][6]=tSSRS.GetText(row, 3);
	        }
	       	no=no+maxrow;
        }
       	// 收费合计SQL
    	msql = tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "X1SSHJ"); 
        tSSRS = mExeSQL.execSQL(msql);
		maxrow=tSSRS.getMaxRow();
       	for (int row = 1; row <= maxrow; row++)
        {
         mToExcel[no+row-1][0]=tSSRS.GetText(row, 1);
         mToExcel[no+row-1][3]=new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 3)));
         mToExcel[no+row-1][4]=new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 4)));
         mToExcel[no+row-1][5]=new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 5)));
         mToExcel[no+row-1][6]=tSSRS.GetText(row, 2);
        }
        no=no+maxrow+2;
        
        System.out.println("tNo:" + no);

        
       	mToExcel[no][0] = "制表员：";
       	mToExcel[no][2] = "审核员：";
        try
        {
            System.out.println("X1--tOutXmlPath:"+tOutXmlPath);
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName = { PubFun.getCurrentDate() };
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(tOutXmlPath);
            System.out.println("生成X1文件完成");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("getNewPrintData", ex.toString());
            return false;
        }
    	return true;
    }
    
}