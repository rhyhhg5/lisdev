package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class QYTShuUI {
	/** 错误信息容器 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private String mStartDate = "";

	private String mEndDate = "";

	VData mVData = new VData();

	/** 全局变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mOperator;

	private String mMangeCom;

	private String mtype;

	private TransferData mTransferData = new TransferData();

	private String mrealpath = "";

	private Readhtml rh;

	private String mFileName;

	public QYTShuUI() {
		System.out.println("QYTShuUI");
	}

	public boolean submitData(VData cInputData, String cOperator) {

		mVData = cInputData;

		if (!getInputData(cInputData)) {
			return false;
		}

		if (!dealDate()) {
			return false;
		}

		return true;
	}

	private boolean dealDate() {
		String tPrintType[] = mtype.split(",");
		int len = tPrintType.length;
		String[] tInputEntry = new String[len];
		String strRealPath = mrealpath;
		rh = new Readhtml();
		for (int i = 0; i < len; i++) {			
			String report = "";
			AppMngPremBL tAppMngPremBL = new AppMngPremBL();
			tAppMngPremBL.submitData(mVData, tPrintType[i]);
			
			report = strRealPath+"vtsfile/"+tAppMngPremBL.getMFileName() + ".csv";
//			report = "F:\\vtsfile\\"+tAppMngPremBL.getMFileName() + ".csv";
			System.out.println("cvs的路径:" + report);
			tInputEntry[i] = report;
		}
		
		mFileName=mOperator+"_"+PubFun.getCurrentDate2()+PubFun.getCurrentTime2()+".zip";
		String tZipFile=mrealpath+ "vtsfile/"+mFileName;
	    rh.CreateZipFile(tInputEntry, tZipFile);

		return true;
	}

	private boolean getInputData(VData mInputData) {

		mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mOperator = mGlobalInput.Operator;
		mMangeCom = mGlobalInput.ManageCom;
		System.out.println(mOperator + mMangeCom);
		mTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		mStartDate = (String) mTransferData.getValueByName("StartDate");// 起始日期
		mEndDate = (String) mTransferData.getValueByName("EndDate");// 截止日期
		mtype = (String) mTransferData.getValueByName("type");
		mrealpath = (String) mTransferData.getValueByName("realpath");
		System.out.println(mtype + "------------" + mrealpath);
		System.out.println(mStartDate + "---------" + mEndDate);
		return true;
	}
	
	public String getFileName() {
		return mFileName;
	}

	public static void main(String[] args) {

	}
}
