package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
//import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.*;
import java.text.DecimalFormat;

public class NewGetCredenceF1PBL implements PrintService {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private int index;
    private String mOperate = "";
    private String mPolNo;
    private String mRiskName;
    private String mAppntName;
    private String mInsuredNameLable = "客户姓名";
    private String mInsuredName;
    private String mInsuredNo;
    private String mOtherNo;
    private String mDrawer;
    private String mDrawerID;
    private String mBnfName;
    private String mPayItem;
    private String mCaseNo;
    private String mRgtClass = "";
    private boolean mNeedStore = false;
    private String mOper = "";
    private LOBatchPRTManagerSchema tLOBatchPRTManagerSchema = null;
    private String mflag = null;
    private String mPostalAddress="";
    
    public NewGetCredenceF1PBL() {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mflag = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();
//        if (mOperate.equals("PRINT")) {
            if (!dealData()) {
                return false;
            }
//        }
        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        if (cOperate.equals("INSERT") || mNeedStore) {
            if (!dealPrintMag("INSERT")) {
                return false;
            }
        }
        if(mOper.equals("UPDATE")){   //第二次调用批打程序时执行打印次数加一。
            if (!dealPrintMag(mOper)) {
                return false;
            }
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
        //用实付号和code（lp008）在打印管理表里查找给付凭证打印记录
        tLOPRTManagerDB.setStandbyFlag2(mLJAGetSchema.getActuGetNo());
        ExeSQL exesql = new ExeSQL();
    	String sql1 = "select CHECKGRPCONT(llcd.GrpContNo)"+
    			 	  " from LLClaimDetail llcd where llcd.CaseNo='"+mLJAGetSchema.getOtherNo()+"'";
    	String shebao = exesql.getOneValue(sql1);
    	System.out.println(shebao);
    	String comcode = mGlobalInput.ManageCom;
    	System.out.println(comcode);
    	String comcodeStr="";
        if(comcode!=null && comcode.length()>=6){        
        	comcodeStr=comcode.substring(0, 6);
        	System.out.println(comcodeStr);
        }
        //新余或新余下级机构的机构代码 
    	if(("863605".equals(comcodeStr)) && ("Y".equals(shebao))){
            tLOPRTManagerDB.setCode("lp008xy");
    	}else{
            tLOPRTManagerDB.setCode("lp008");
    	}
        tLOPRTManagerSet = tLOPRTManagerDB.query();
        if (tLOPRTManagerSet == null || tLOPRTManagerSet.size() == 0) {
            index = 0;
            mNeedStore = true;
        } else {
            if (tLOPRTManagerDB.mErrors.needDealError()) {
                mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
                buildError("dealData", "在取得LOPRTManager的数据时发生错误");
                return false;
            }
            //打印次数
            index = (int) tLOPRTManagerSet.get(1).getPrintTimes();
            mNeedStore = false;
            if (tLOBatchPRTManagerSchema != null) {
                mLOPRTManagerSchema = tLOPRTManagerSet.get(1);
                mLOPRTManagerSchema.setPrintTimes(mLOPRTManagerSchema.getPrintTimes()+1);
                mOper = "UPDATE";
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();
        ttLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.
                               getObjectByObjectName("LOPRTManagerSchema", 0);

        //批次印管理表信息
        tLOBatchPRTManagerSchema = new
                LOBatchPRTManagerSchema();
        tLOBatchPRTManagerSchema = ((LOBatchPRTManagerSchema)
                                    cInputData.getObjectByObjectName(
                                            "LOBatchPRTManagerSchema", 0));

        if (ttLOPRTManagerSchema == null) {
            if (tLOBatchPRTManagerSchema == null) {
                System.out.println("---------为空--初次打印入口！");
                mLJAGetSchema.setSchema((LJAGetSchema) cInputData.
                                        getObjectByObjectName("LJAGetSchema", 0));
                mRgtClass = mLJAGetSchema.getPayMode();
                System.out.println("mRgtClass:"+mRgtClass);

                if (mLJAGetSchema == null) {
                    buildError("getInputData", "为空--初次打印入口没有得到足够的信息！");
                    return false;
                }
            } else {
                //批次打印的调用入口
                mCaseNo = tLOBatchPRTManagerSchema.getOtherNo();
                String tActuGetNo = tLOBatchPRTManagerSchema.getStandbyFlag1(); //
                mLJAGetSchema.setOtherNo(mCaseNo);
                mLJAGetSchema.setActuGetNo(tActuGetNo);
                mLJAGetSchema.setPayMode("0"); //借用交费方式字段传递申请类型
                mRgtClass = tLOBatchPRTManagerSchema.getStandbyFlag2();

                if (tActuGetNo == null || tActuGetNo == "") {
                    buildError("getInputData", "为空--批次打印入口没有得到足够的信息！");
                    return false;
                }
            }
            return true;
        } else {
            //PDF入口
            System.out.println("---------不为空--PDF入口！");
            LJAGetDB mLJAGetDB = new LJAGetDB();
            mLJAGetDB.setActuGetNo(ttLOPRTManagerSchema.getStandbyFlag2());
            //取出申请类型
            mRgtClass = ttLOPRTManagerSchema.getStandbyFlag3();
            if (!mLJAGetDB.getInfo()) {
                mErrors.addOneError("PDF入口LJAGetDB传入的数据不完整。");
                return false;
            }
            mLJAGetSchema = mLJAGetDB.getSchema();
            return true;
        }
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean print(String tPolNo) {
        mPolNo = tPolNo;
        LCPolSchema tLCPolSchema = new LCPolSchema();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(mPolNo);
        if (!tLCPolDB.getInfo()) {
            LBPolDB tLBPolDB = new LBPolDB();
            tLBPolDB.setPolNo(mPolNo);
            if (!tLBPolDB.getInfo()) {
                return false;
            }
            Reflections aReflections = new Reflections();
            aReflections.transFields(tLCPolSchema, tLBPolDB.getSchema());
        } else {
            tLCPolSchema.setSchema(tLCPolDB.getSchema());
        }
        mAppntName = tLCPolSchema.getAppntName();
        mInsuredName = tLCPolSchema.getInsuredName();
        LMRiskDB tLMRiskDB = new LMRiskDB();
        tLMRiskDB.setRiskCode(tLCPolSchema.getRiskCode());
        if (!tLMRiskDB.getInfo()) {
            mErrors.copyAllErrors(tLMRiskDB.mErrors);
            buildError("Print", "在取得险种名称时发生错误");
            return false;
        }
        mRiskName = tLMRiskDB.getRiskName();
        LCBnfDB tLCBnfDB = new LCBnfDB();
        tLCBnfDB.setPolNo(mPolNo);
        tLCBnfDB.setBnfType("0");
        LCBnfSet tLCBnfSet = new LCBnfSet();
        tLCBnfSet.set(tLCBnfDB.query());
        if (tLCBnfDB.mErrors.needDealError()) {
            mErrors.copyAllErrors(tLCBnfDB.mErrors);
            buildError("Print", "在取得受益人时发生错误");
            return false;
        }
        if (tLCBnfSet.size() == 0) {
            mBnfName = "法定";
        } else {
            mBnfName = tLCBnfSet.get(1).getName();

            for (int i = 1; i < tLCBnfSet.size(); i++) {
                mBnfName += "、" + tLCBnfSet.get(i + 1).getName();
            }
        }

        return true;
    }

    /**
     * 集体退费数据
     * @param tGrpPolNo
     * @return
     */
    private boolean printGrp(String tGrpPolNo) {
        mPolNo = tGrpPolNo; //模版元素所用
        String mGrpPolNo = tGrpPolNo;
        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpPolNo(mGrpPolNo);
        if (!tLCGrpPolDB.getInfo()) {
            LBGrpPolDB tLBGrpPolDB = new LBGrpPolDB();
            tLBGrpPolDB.setGrpPolNo(mGrpPolNo);
            if (!tLBGrpPolDB.getInfo()) {
                return false;
            }
            Reflections aReflections = new Reflections();
            aReflections.transFields(tLCGrpPolSchema, tLBGrpPolDB.getSchema());
        } else {
            tLCGrpPolSchema.setSchema(tLCGrpPolDB.getSchema());
        }
        mAppntName = tLCGrpPolSchema.getGrpName();
        mInsuredName = ""; //被保人姓名
        mBnfName = ""; //受益人姓名
        LMRiskDB tLMRiskDB = new LMRiskDB();
        tLMRiskDB.setRiskCode(tLCGrpPolSchema.getRiskCode());
        if (!tLMRiskDB.getInfo()) {
            mErrors.copyAllErrors(tLMRiskDB.mErrors);
            buildError("Print", "在取得险种名称时发生错误");
            return false;
        }
        mRiskName = tLMRiskDB.getRiskName();

        return true;
    }

    private boolean getPrintData() {
        String tOtherNoType = "";
        boolean lptype=false;
        LJAGetDB tLJAGetDB = new LJAGetDB();
        tLJAGetDB.setSchema(mLJAGetSchema);
        if (!tLJAGetDB.getInfo()) {

            mErrors.copyAllErrors(tLJAGetDB.mErrors);
            buildError("getPrintData", "案件为团体统一给付或尚未给付确认，不能打印给付凭证");
            return false;
        }
        tOtherNoType = tLJAGetDB.getOtherNoType();
        mOtherNo = tLJAGetDB.getOtherNo();
        System.out.println("otherno:" + tLJAGetDB.getOtherNo() +
                           "  othernotype:" + tLJAGetDB.getOtherNoType());

        if (tOtherNoType.equals("0") || tOtherNoType.equals("1") ||
            tOtherNoType.equals("2")) {
            LJAGetDrawSchema tLJAGetDrawSchema = new LJAGetDrawSchema();
            LJAGetDrawDB tLJAGetDrawDB = new LJAGetDrawDB();
            tLJAGetDrawDB.setActuGetNo(tLJAGetDB.getActuGetNo());
            LJAGetDrawSet tLJAGetDrawSet = new LJAGetDrawSet();
            tLJAGetDrawSet.set(tLJAGetDrawDB.query());
            tLJAGetDrawSchema.setSchema(tLJAGetDrawSet.get(1));
            if (tOtherNoType.equals("2")) {
                if (!print(tLJAGetDrawSchema.getPolNo())) {
                    buildError("print", "在获取打印数据时发生错误1");
                    return false;
                }
            }
            mPayItem = "生存领取";
        } else if (tOtherNoType.equals("3")) {
            LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
            LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
            tLJAGetEndorseDB.setActuGetNo(tLJAGetDB.getActuGetNo());
            LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
            tLJAGetEndorseSet.set(tLJAGetEndorseDB.query());
            tLJAGetEndorseSchema.setSchema(tLJAGetEndorseSet.get(1));
            if ((tLJAGetEndorseSchema.getGrpPolNo() == null) ||
                (
                        tLJAGetEndorseSchema.getGrpPolNo().equals(
                                "00000000000000000000"))) {
                if (!print(tLJAGetEndorseSchema.getPolNo())) {
                    buildError("print", "在获取打印数据时发生错误3");
                    return false;
                }
                mPayItem = "批改退费";

            } else {
                if (!printGrp(tLJAGetEndorseSchema.getGrpPolNo())) {
                    buildError("print", "在获取打印数据时发生错误3b");
                    return false;
                }
                mPayItem = "批改退费";
            }

        } else if (tOtherNoType.equals("4")) {
            LJAGetTempFeeSchema tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
            LJAGetTempFeeDB tLJAGetTempFeeDB = new LJAGetTempFeeDB();
            tLJAGetTempFeeDB.setActuGetNo(tLJAGetDB.getActuGetNo());
            LJAGetTempFeeSet tLJAGetTempFeeSet = new LJAGetTempFeeSet();
            tLJAGetTempFeeSet.set(tLJAGetTempFeeDB.query());
            tLJAGetTempFeeSchema.setSchema(tLJAGetTempFeeSet.get(1));
            LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
            tLJTempFeeDB.setTempFeeNo(tLJAGetTempFeeSchema.getTempFeeNo());
            tLJTempFeeDB.setTempFeeType(tLJAGetTempFeeSchema.getTempFeeType());
            tLJTempFeeDB.setRiskCode(tLJAGetTempFeeSchema.getRiskCode());
            if (!tLJTempFeeDB.getInfo()) {
                mErrors.copyAllErrors(tLJTempFeeDB.mErrors);
                buildError("getPrintData", "在取得LJTempFee的数据时发生错误");
                return false;
            }
            mPolNo = tLJTempFeeDB.getOtherNo();
            LMRiskDB tLMRiskDB = new LMRiskDB();
            tLMRiskDB.setRiskCode(tLJAGetTempFeeSchema.getRiskCode());
            if (!tLMRiskDB.getInfo()) {
                mErrors.copyAllErrors(tLMRiskDB.mErrors);
                buildError("getPrintData", "在取得险种名称时发生错误");
                return false;
            }
            mRiskName = tLMRiskDB.getRiskName();
            mPayItem = "暂交费退费";
        } else if (tOtherNoType.equals("5")) {
        	ExeSQL exesql = new ExeSQL();
        	String sql1 = "select CHECKGRPCONT(llcd.GrpContNo)"+
        			 	  " from LLClaimDetail llcd where llcd.CaseNo='"+tLJAGetDB.getOtherNo()+"'";
        	String comcode = mGlobalInput.ManageCom;
        	String shebao = exesql.getOneValue(sql1);
        	System.out.println(comcode);
        	System.out.println(shebao);
        	String comcodeStr="";
	        if(comcode!=null && comcode.length()>=6){        
	        	comcodeStr=comcode.substring(0, 6);
	        	System.out.println(comcodeStr);
	        }
        	//新余或新余下级机构的机构代码并且为社保
        	if(("863605".equals(comcodeStr)) && ("Y".equals(shebao))){
              	lptype=true;
        		String sql2="select lcgs.ProjectName from lcgrpcontsub lcgs where lcgs.Prtno="+
        					"(select lcg.Prtno from LLClaimDetail llcd,lcgrpcont lcg "+
        					" where llcd.GrpContNo=lcg.GrpContNo and llcd.CaseNo='"+tLJAGetDB.getOtherNo()+"')"+ 
        					" union "+
        					"select lcgs.ProjectName from lcgrpcontsub lcgs where lcgs.Prtno="+
        					"(select lbg.Prtno from LLClaimDetail llcd,lbgrpcont lbg "+
        					" where llcd.GrpContNo=lbg.GrpContNo and llcd.CaseNo='"+tLJAGetDB.getOtherNo()+"')";
        		String ProjectName="";
                SSRS ssrs1 = exesql.execSQL(sql2);
                if (ssrs1 == null || ssrs1.getMaxRow() <= 0) {
                	ProjectName="";
                }else{
                	ProjectName=ssrs1.GetText(1, 1);
                }
        		System.out.println(ProjectName);
        		mPayItem = ProjectName+"项目理赔";
        		System.out.println(mPayItem);
        		//新增客户地址
              	String sql3="select PostalAddress from llcaseext where CaseNo='"+tLJAGetDB.getOtherNo()+"'";
                SSRS ssrs2 = exesql.execSQL(sql3);
                if (ssrs2 == null || ssrs2.getMaxRow() <= 0) {
                	mPostalAddress="";
                }else{
                	mPostalAddress=ssrs2.GetText(1, 1);
                }
              	System.out.println(mPostalAddress);
        	}else{
        		mPayItem = "理赔";
        	}
            if ("0".equals(mRgtClass)) {
                //个案
                LLCaseDB tLLCaseDB = new LLCaseDB();
                tLLCaseDB.setCaseNo(tLJAGetDB.getOtherNo());
                if (!tLLCaseDB.getInfo()) {
                    mErrors.copyAllErrors(tLLCaseDB.mErrors);
                    buildError("getPrintData", "在取得理赔信息时发生错误");
                    return false;

                } else {
                    LLHospCaseDB tLLHospCaseDB = new LLHospCaseDB();
                    tLLHospCaseDB.setCaseNo(tLJAGetDB.getOtherNo());
                    if (tLLHospCaseDB.getInfo()) {
						if ("1".equals(tLLHospCaseDB.getDealType())
								&& !"2".equals(tLLHospCaseDB.getConfirmState())
								&& (tLLHospCaseDB.getCaseType() == null || ""
										.equals(tLLHospCaseDB.getCaseType()))) {
							buildError("getPrintData", "医保通案件，不能进行处理");
							return false;
						}
					}

                    mInsuredName = tLLCaseDB.getCustomerName();
                    mInsuredNo = tLLCaseDB.getCustomerNo();
                    if(tLJAGetDB.getPayMode().equals("1") || tLJAGetDB.getPayMode().equals("2"))
                    mDrawer = tLJAGetDB.getDrawer();
                    else mDrawer = tLJAGetDB.getAccName();
                    mDrawerID = tLJAGetDB.getDrawerID();
                }

            }
            if ("1".equals(mRgtClass)) {
                mInsuredNameLable = "单位名称";
                //团体案件LLRegister
                LLRegisterDB tLLRegisterDB = new LLRegisterDB();
                tLLRegisterDB.setRgtNo(tLJAGetDB.getOtherNo());
                if (!tLLRegisterDB.getInfo()) {
                    mErrors.copyAllErrors(tLLRegisterDB.mErrors);
                    buildError("getPrintData", "在取得理赔信息时发生错误");
                    return false;

                } else {
                    mInsuredNo = tLLRegisterDB.getCustomerNo(); //团体客户号
                    String tpaymode=tLJAGetDB.getPayMode();//支付方式
                    if(tpaymode!=null)
                    {	
                    	if(!tpaymode.equals("1") && !tpaymode.equals("2"))//银行转账、银行汇款、转账支票
                    	{
                    		mDrawer = tLJAGetDB.getAccName();//团体单位名称by zzh 101027
                    		mDrawerID = "";//为空by zzh 101027
                    	}
                    	else
                    	{
                    		mDrawer = tLJAGetDB.getDrawer();
                    		mDrawerID = tLJAGetDB.getDrawerID();
                    	}
                    }
                    else
                    {
                    	mErrors.copyAllErrors(tLLRegisterDB.mErrors);
                        buildError("getPrintData", "支付方式发生错误");
                        return false;
                    }
                    mInsuredName = tLLRegisterDB.getGrpName();
                }

            }

        } else if (tOtherNoType.equals("6")) {
            if (!print(tLJAGetDB.getOtherNo())) {
                buildError("print", "在获取打印数据时发生错误6");
                return false;
            }
            mPayItem = "退费";
        } else if (tOtherNoType.equals("7")) {
            if (!print(tLJAGetDB.getOtherNo())) {
                buildError("print", "在获取打印数据时发生错误7");
                return false;
            }
            mPayItem = "红利给付";
        }

        else if (tOtherNoType.equals("8")) {
            if (!printGrp(tLJAGetDB.getOtherNo())) {
                buildError("print", "在获取打印数据时发生错误8");
                return false;
            }
            mPayItem = "退费";
        }

        else if (tOtherNoType.equals("10")) {
            LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
            tLPEdorAppDB.setEdorAcceptNo(tLJAGetDB.getOtherNo());
            if (!tLPEdorAppDB.getInfo()) {
                buildError("print", "在获取打印数据时发生错误");
                return false;
            } else {
                if (tLPEdorAppDB.getOtherNoType().equals("1")) {
                    LDPersonDB tLDPersonDB = new LDPersonDB();
                    tLDPersonDB.setCustomerNo(tLPEdorAppDB.getOtherNo());
                    if (tLDPersonDB.getInfo()) {
                        mInsuredName = tLDPersonDB.getName();
                        mInsuredNo = tLDPersonDB.getCustomerNo();
                        if ("0".equals(tLDPersonDB.getIDType())) {
                        }
                    }
                }
                if (tLPEdorAppDB.getOtherNoType().equals("3")) {
                    LCAppntDB tLCAppntDB = new LCAppntDB();
                    tLCAppntDB.setContNo(tLPEdorAppDB.getOtherNo());
                    if (tLCAppntDB.getInfo()) {
                        mInsuredName = tLCAppntDB.getAppntName();
                        mInsuredNo = tLCAppntDB.getAppntNo();
                        if ("0".equals(tLCAppntDB.getIDType())) {
                        }
                    } else {
                        LBAppntDB tLBAppntDB = new LBAppntDB();
                        tLBAppntDB.setContNo(tLPEdorAppDB.getOtherNo());
                        LBAppntSet tLBAppntSet = new LBAppntSet();
                        tLBAppntSet = tLBAppntDB.query();
                        if (tLBAppntDB.mErrors.needDealError() ||
                            tLBAppntSet.size() == 0) {
                            buildError("Print", "在查询投保人信息时失败!");
                            return false;
                        }
                        tLBAppntDB.setSchema(tLBAppntSet.get(1));
                        mInsuredName = tLBAppntDB.getAppntName();
                        mInsuredNo = tLBAppntDB.getAppntNo();
                        if ("0".equals(tLBAppntDB.getIDType())) {
                        }

                    }
                }
                mDrawer = tLPEdorAppDB.getEdorAppName();
                mPayItem = "保全退费";
            }
        }
        //新增预付回销信息------Begin----
        String tPreSql="";
        tPreSql="select coalesce(sum(pay),0) from ljagetclaim where otherno='"+ mOtherNo +"' and othernotype='Y' and feefinatype='YF' ";
       
        ExeSQL tExeSQL = new ExeSQL();
        double tSumPay = 0;
        String tPrePaidMess="";
        tSumPay = Double.parseDouble(tExeSQL.getOneValue(tPreSql));
        if(tSumPay != 0)
        tPrePaidMess ="其中回销预付赔款"+ (-tSumPay) +"元";        
        //新增预付回销信息------End----
        String Money = new DecimalFormat("0.00").format((double) (tLJAGetDB.getSumGetMoney()-(tSumPay)));
        //2774打印的给付凭证中	“给付金额”字段值同受理申请时页面填写的“申报金额”值。
        if(LLCaseCommon.checkSocialSecurity(mOtherNo,"rgtno").equals("1")&&mOtherNo.substring(1, 3).equals("94")){
        	LLRegisterDB tLLRegisterDB = new LLRegisterDB();
            tLLRegisterDB.setRgtNo(tLJAGetDB.getOtherNo());
            if (!tLLRegisterDB.getInfo()) {
                mErrors.copyAllErrors(tLLRegisterDB.mErrors);
                buildError("getPrintData", "在取得理赔信息时发生错误");
                return false;

            } else {
            	Money=new DecimalFormat("0.00").format((double)tLLRegisterDB.getAppAmnt());
            }
        }
        ListTable tlistTable = new ListTable();
        System.out.println("理赔赔款money====" + Money);
        System.out.println("预付回销tSumPay=="+tSumPay);
        String strArr[] = null;
        tlistTable.setName("GET");
        strArr = new String[4];
        strArr[0] = mPayItem;
        strArr[1] = Money;
        strArr[2] = "";
        strArr[3] = "";
        tlistTable.add(strArr);
        String CurrentDate = PubFun.getCurrentDate();
        int a = CurrentDate.lastIndexOf("-");
        String tYear = CurrentDate.substring(0, 4);
        String tMonth = CurrentDate.substring(5, a);
        String tDay = CurrentDate.substring(a + 1, CurrentDate.length());
        String tDate = tYear + "年" + tMonth + "月" + tDay + "日";

        String sql = "select username from lduser where usercode = '" +
                     mGlobalInput.Operator + "'";
        ExeSQL exesql = new ExeSQL();
        String opername = exesql.getOneValue(sql);

        String sqlMode =
                "select CodeName from ldcode where CodeType = 'paymode' and Code='" +
                tLJAGetDB.getPayMode() + "'";
        String PayMode = exesql.getOneValue(sqlMode);

        String sqlAgent = "select Name from LAAgent where  AgentCode='" +
                          tLJAGetDB.getAgentCode() + "'";
        String AgentName = exesql.getOneValue(sqlAgent);
        
        String AgentCodes="select getUniteCode(agentcode) from LAAgent where AgentCode='" + tLJAGetDB.getAgentCode() + "'";
        
        String tAgentCodes = exesql.getOneValue(AgentCodes);
        
        String sqlAgentgroup = "select Name from labranchgroup where  Agentgroup='" 
        	                 + tLJAGetDB.getAgentGroup() + "'";
        String tAgentGroupName = exesql.getOneValue(sqlAgentgroup);
        
        System.out.println(mInsuredName);
        System.out.println(mInsuredNo);
        System.out.println(mOtherNo);
        System.out.println(mPayItem);
        System.out.println(mDrawer);
        System.out.println(mDrawerID);
        System.out.println(tDate);
        String AppealRemark = "";
        if (mOtherNo.substring(0, 1).equals("S") ||
            mOtherNo.substring(0, 1).equals("R")) {
            AppealRemark = "特别提示栏：本次给付是对案件号为";
            LLAppealDB tLLAppealDB = new LLAppealDB();
            tLLAppealDB.setAppealNo(mOtherNo);
            if (!tLLAppealDB.getInfo()) {
                buildError("print", "申诉信息查询失败！");
                return false;
            }
            AppealRemark += tLLAppealDB.getCaseNo() + "的补充给付。";
            String tsql = "select distinct a.actugetno,a.sumgetmoney from " +
                          " ljaget a,ljagetclaim b where a.actugetno = b.actugetno "
                          + " and b.otherno = '" + tLLAppealDB.getCaseNo() +
                          "'";
            ExeSQL exeSQL = new ExeSQL();
            SSRS ssrs = exeSQL.execSQL(tsql);
            if (exeSQL.mErrors.getErrorCount() > 0 || ssrs == null ||
                ssrs.getMaxRow() <= 0) {
                AppealRemark += "原案件未给付，本次给付" + Money + "元，合计实付"
                        + Money + "元。";
            } else {
                AppealRemark += "给付凭证号为" + ssrs.GetText(1, 1) + "的给付金额为"
                        + ssrs.GetText(1, 2) + "元，本次给付" + Money + "元，合计实付";
                double oldmoney = Double.parseDouble(ssrs.GetText(1, 2));
                double newmoney = oldmoney + tLJAGetDB.getSumGetMoney();
                String tNewMoney = new DecimalFormat("0.00").format((double)
                        newmoney);
                AppealRemark += tNewMoney + "元。";
            }

        }

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
//      借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                          mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if(lptype==true){
        	texttag.add("JetFormType", "lp008xy");
        }else{
        	texttag.add("JetFormType", "lp008");
        }
         if (comcode.equals("86") || comcode.equals("8600")||comcode.equals("86000000")) {
            comcode = "86";
             } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
          } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
          }
          String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"+comcode+"' with ur";
          String printcode = new ExeSQL().getOneValue(printcom);

        texttag.add("ManageComLength4", printcode);
        texttag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.","_"));
        if(mflag.equals("batch")){
            texttag.add("previewflag", "0");
        }else{
            texttag.add("previewflag", "1");
        }
        xmlexport.createDocument("GetCredence.vts", "printer"); //最好紧接着就初始化xml文档
        texttag.add("BarCode1", mLJAGetSchema.getActuGetNo());
        texttag.add("BarCodeParam1"
                    , "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("BarCode2", mLJAGetSchema.getActuGetNo());
        texttag.add("BarCodeParam2"
                    , "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        texttag.add("PolNo", mPolNo);
        texttag.add("PolNo", mPolNo);
        texttag.add("RiskName", mRiskName);
        texttag.add("AppntName", mAppntName);
        texttag.add("InsuredNameLable", mInsuredNameLable);
        texttag.add("InsuredName", mInsuredName);
        texttag.add("InsuredNo", mInsuredNo);
        texttag.add("Operator", opername);
        texttag.add("OtherNo", mOtherNo);
        texttag.add("PayItem", mPayItem);
        if(lptype==true){
        	texttag.add("InsuredAddress", mPostalAddress);
        }
        texttag.add("Drawer", mDrawer);
        texttag.add("DrawerID", mDrawerID);
        texttag.add("BnfName", mBnfName);
        texttag.add("PayMode", PayMode);
        texttag.add("AgentName", AgentName);
        texttag.add("Agentcode",tAgentCodes);
        texttag.add("AgentGroupName", tAgentGroupName);

        texttag.add("Year", tYear);
        texttag.add("Month", tMonth);
        texttag.add("Day", tDay);
        texttag.add("ActuGetNo", mLJAGetSchema.getActuGetNo());
        texttag.add("Date", tDate);
        texttag.add("Index", index + 1); 
        texttag.add("AppealRemark", AppealRemark);
        texttag.add("MoneyCHN", PubFun.getChnMoney(Double.parseDouble(Money)));
        texttag.add("Money", Money);

        texttag.add("XI_ContNo","");
        texttag.add("XI_ManageCom",tLJAGetDB.getManageCom());
        texttag.add("PrePaidMess",tPrePaidMess);
       
        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            tLOPRTManagerDB.setStandbyFlag2(tLJAGetDB.getActuGetNo());
            if(lptype==true){
                tLOPRTManagerDB.setCode("lp008xy");
            }else{
                tLOPRTManagerDB.setCode("lp008");
            }

            String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            mLOPRTManagerSchema.setOtherNo(tLJAGetDB.getOtherNo());
            mLOPRTManagerSchema.setOtherNoType("09");
            if(lptype==true){
                mLOPRTManagerSchema.setCode("lp008xy");
            }else{
                mLOPRTManagerSchema.setCode("lp008");
            }
            mLOPRTManagerSchema.setManageCom(this.mGlobalInput.ManageCom);
            mLOPRTManagerSchema.setAgentCode("");
            mLOPRTManagerSchema.setReqCom(this.mGlobalInput.ManageCom);
            mLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
            mLOPRTManagerSchema.setPrtType("0");
            mLOPRTManagerSchema.setStateFlag("0");
            mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            mLOPRTManagerSchema.setStandbyFlag2(tLJAGetDB.getActuGetNo()); //这里存放实付总表的实付号码
            mLOPRTManagerSchema.setStandbyFlag3(mRgtClass);
            if (tLOBatchPRTManagerSchema != null){
                mLOPRTManagerSchema.setPrintTimes("1");
            }

        mResult.addElement(mLOPRTManagerSchema);
        return true;

    }


    public CErrors getErrors() {
        return mErrors;
    }

    private boolean dealPrintMag(String cOperate) {
        MMap tMap = new MMap();
        tMap.put(mLOPRTManagerSchema, cOperate);
        VData tVData = new VData();
        tVData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tVData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
            return false;
        }
        return true;
    }


    public static void main(String[] args) {
    	String mOtherNo="C1100110301000005";
    	 String tPreSql="SELECT NVL(SUM(CASE WHEN X.realpay>Y.prepaidbala THEN Y.prepaidbala ELSE " 
         	+" X.realpay END),0)  FROM  (SELECT caseno,rgtno,grpcontno,grppolno,sum(realpay) realpay " 
         	+" FROM llclaimdetail  GROUP BY caseno,rgtno,grpcontno,grppolno) AS X,  llprepaidgrppol "
         	+" Y WHERE X.grpcontno=Y.grpcontno AND X.grppolno=Y.grppolno  "
         	+" and (X.caseno='"+ mOtherNo +"' or X.rgtno='"+ mOtherNo +"') ";
         ExeSQL tExeSQL = new ExeSQL();
         double tSumPay = 0;
         String tPrePaidMess="";
         tSumPay = Double.parseDouble(tExeSQL.getOneValue(tPreSql));
         if(tSumPay > 0)
         tPrePaidMess ="其中回销预付赔款"+ tSumPay +"元";
        System.out.println(tPrePaidMess);
    }
}
