package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author 
 * function ://
 * @version 1.0
 * @date 2014-2-20
 */


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class PlChangeRelationBL {
    public CErrors mErrors = new CErrors();

    public PlChangeRelationBL() {
    }

    /**
     传输数据的公共方法
     */
    public String getAllLine(String node) {
    	StringBuffer sb=new StringBuffer();
		String str1=getByPlNo(node);
		System.out.println("str1---"+str1);
		String str2=getByRefNo(node);
		System.out.println("str2---"+str2);
		
		if("'".equals(str1) && "'".equals(str2)){
			sb.append(str1).append(node).append(str2);
		}else if("'".equals(str1) && !"'".equals(str2)){
			sb.append(str1).append(node).append("','").append(str2.substring(0,str2.lastIndexOf(",")));
		}else if(!"'".equals(str1) && "'".equals(str2)){
			sb.append(str1.substring(3)).append("','").append(node).append(str2);
		}else{
			sb.append(str1.substring(3)).append("','").append(node).append("','").append(str2.substring(0,str2.lastIndexOf(",")));
		}
		return sb.toString();
    }
    
    public static String getByRefNo(String node){
    	System.out.println("传进来的值-node-getByRefNo()-"+node);
    	SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		String sql="select placeno from LIPlaceChangeTrace where changetype in('01','02','03') and changestate <> 'DL' and placenorefer='"+node+"' order by makedate desc  fetch  first row only";
		tSSRS = tExeSQL.execSQL(sql);
		if(tSSRS.getMaxRow()>0){
			System.out.println("placeno==="+tSSRS.GetText(1, 1));
			if(tSSRS.GetText(1, 1)!=null && !"".equals(tSSRS.GetText(1, 1))){
				return tSSRS.GetText(1, 1)+"','"+getByRefNo(tSSRS.GetText(1, 1));
			}
		}
		return "'";
	}
    
    public static String getByPlNo(String node){
    	System.out.println("传进来的值-node-getByPlNo()-"+node);
    	SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		String sql="select placenorefer from LIPlaceChangeTrace where changetype in('01','02','03') and changestate <> 'DL' and placeno='"+node+"' order by makedate desc  fetch  first row only";
		tSSRS = tExeSQL.execSQL(sql);
		if(tSSRS.getMaxRow()>0){
			System.out.println("placenorefer==="+tSSRS.GetText(1, 1));
			if(tSSRS.GetText(1, 1)!=null && !"".equals(tSSRS.GetText(1, 1))){
				return getByPlNo(tSSRS.GetText(1, 1))+"','"+tSSRS.GetText(1, 1);
			}
		}
		return "'";
	}
    
    public static void main (String args[]){
    	PlChangeRelationBL s = new PlChangeRelationBL();
//		System.out.println(s.getAllLine("86120100GS2011000707"));
		System.out.println(s.getAllLine("86120100GS2012000808"));
	}
}
