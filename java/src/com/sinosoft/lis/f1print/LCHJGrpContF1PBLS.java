/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.Statement;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCContGetPolDB;
import com.sinosoft.lis.db.LCContPrintDB;
import com.sinosoft.lis.db.LCContReceiveDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCPolPrintDB;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCContGetPolSchema;
import com.sinosoft.lis.schema.LCContPrintSchema;
import com.sinosoft.lis.schema.LCContReceiveSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vdb.LCContGetPolDBSet;
import com.sinosoft.lis.vdb.LCContPrintDBSet;
import com.sinosoft.lis.vdb.LCContReceiveDBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LCContGetPolSet;
import com.sinosoft.lis.vschema.LCContPrintSet;
import com.sinosoft.lis.vschema.LCContReceiveSet;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LDSysVarSet;
import com.sinosoft.utility.CBlob;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLDatasets;

/**
 * <p>Title: </p>
 * <p>Description: 数据提交及xml流处理</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author zz
 * @version 1.0
 */
public class LCHJGrpContF1PBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private File mFile = null;

    //业务处理相关变量
    /** 全局数据 */
    private String mOperate = "";

    private String mGrpContNo = "";

    private VData mInputData;

    private String mPrtNo;

    private String mcvalidate;

    private String mManageCom;

    private int mPrintCount = 1;

    private String mdate;

    private String mtime;

    private MMap map = new MMap();

    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    private GlobalInput mGlobalInput = new GlobalInput();
    
    private boolean isFilialePrint = false;//是否为分公司打印

    //    private VData mResult = new VData();

    public LCHJGrpContF1PBLS()
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        mInputData = (VData) cInputData.clone();

        if (mOperate.equals("PRINT"))
        {
            return save(mInputData);
        }
        else if (mOperate.equals("REPRINT"))
        {
            return update(mInputData);
        }

        return true;
    }

    /**
     * 打印操作
     * @param mInputData VData
     * @return boolean
     */
    private boolean save(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        mdate = PubFun.getCurrentDate();
        mtime = PubFun.getCurrentTime();
        Connection conn = DBConnPool.getConnection();

        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpContF1PBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start ....");
            // 获取要保存的数据
            XMLDatasets tXMLDatasets = (XMLDatasets) mInputData
                    .getObjectByObjectName("XMLDatasets", 0);

            GlobalInput tGlobalInput = (GlobalInput) mInputData
                    .getObjectByObjectName("GlobalInput", 0);
            mGlobalInput = tGlobalInput;
            //更新LCGrpCont表数据
            LCGrpContDB tLCGrpContDB = new LCGrpContDB(conn);
            tLCGrpContDB.setSchema((LCGrpContSchema) mInputData
                    .getObjectByObjectName("LCGrpContSchema", 0));
            String tWebPath = (String) mInputData.getObjectByObjectName(
                    "String", 0);
            mGrpContNo = tLCGrpContDB.getGrpContNo();
            //@将打印的xml放入lccontprint表中,做下载用---begin
            LCGrpContDB ttLCGrpContDB = new LCGrpContDB();
            ttLCGrpContDB.setGrpContNo(mGrpContNo);
            LCGrpContSet ttLCGrpContSet = ttLCGrpContDB.query();
            mLCGrpContSchema = ttLCGrpContSet.get(1).getSchema();
            mPrtNo = ttLCGrpContSet.get(1).getPrtNo();
            mcvalidate = ttLCGrpContSet.get(1).getCValiDate();
            mManageCom = ttLCGrpContSet.get(1).getManageCom();
            LCContPrintDB mLCContPrintDB = new LCContPrintDB();
            mLCContPrintDB.setOtherNo(mGrpContNo);
            LCContPrintSet mLCContPrintSet = new LCContPrintSet();
            mLCContPrintSet = mLCContPrintDB.query();
            System.out.println("mLCContPrintSet.size() is " + mLCContPrintSet.size());
            if (mLCContPrintSet.size() > 0)
            {
                mPrintCount = mLCContPrintSet.get(1).getPrintCount();
                mPrintCount = mPrintCount + 1;
                LCContPrintDBSet mLCContPrintDBSet = new LCContPrintDBSet(conn);
                mLCContPrintDBSet.set(mLCContPrintSet);
                if (!mLCContPrintDBSet.delete())
                {
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            if (!tLCGrpContDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "GrpFeeBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "删除管理费主表信息失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            //将数据流的信息存入blob字段中，更换了数据库，目前只支持db2的blob数据插入
            //saveDataStream(tXMLDatasets, tGlobalInput, conn);

            //文件路径
            //管理机构一层
            String FilePath = tWebPath + "/printdata";
            mFile = new File(FilePath);
            if (!mFile.exists())
            {
                mFile.mkdir();
            }
            //根据合同号生成打印数据存放文件夹
            //            FilePath = tWebPath + "/printdata" + "/" + mGrpContNo;
            FilePath = tWebPath + "/printdata" + "/data";
            mFile = new File(FilePath);
            if (!mFile.exists())
            {
                mFile.mkdir();
            }

            InputStream ins = tXMLDatasets.getInputStream();
            InputStream ins_Card = tXMLDatasets.getInputStream();
            //根据团单合同号生成文件
            String XmlFile = FilePath + "/" + mGrpContNo + ".xml";
            String XmlFile_Card = FilePath + "/" + mGrpContNo + "_Card"
                    + ".xml";
            FileOutputStream fos_card = null;
            if (!StrTool.cTrim(tLCGrpContDB.getCardFlag()).equals(""))
            {
                FilePath = tWebPath + "/printdata" + "/data/brief/";
                mFile = new File(FilePath);
                if (!mFile.exists())
                {
                    mFile.mkdir();
                }
                System.out.println("XmlFile : " + XmlFile);
                XmlFile = FilePath + mGrpContNo + ".xml";
                XmlFile_Card = FilePath + mGrpContNo + "_Card" + ".xml";
                System.out.println("XmlFile : " + XmlFile);
                fos_card = new FileOutputStream(XmlFile_Card);
            }
            FileOutputStream fos = new FileOutputStream(XmlFile);
            int n = 0;
            //            while ((n = ins.read()) != -1)
            //            {
            //                fos.write(n);
            //            }
            //采用缓冲池的方式写文件，针对I/O修改
            byte[] c = new byte[4096];
            while ((n = ins.read(c)) != -1)
            {
                fos.write(c, 0, n);
            }
            fos.close();
            
            //判断保单是否为分公司打印
            String sql = "select count(1) from LDCode1 where CodeType = 'printchannel' and CodeName = '1' "
                + "and Code = '" + mManageCom.substring(0, 4) + "' and Code1 = '2' ";
            isFilialePrint = new ExeSQL().getOneValue(sql).equals("1");

            String contPrintFlag = (String) mInputData.get(5);//保单是否打印标记：0，不打印；1，打印

            //@将打印的xml放入lccontprint表中,做下载用
            LCContPrintSet tLCContPrintSet = new LCContPrintSet();
            LCContPrintSchema tLCContPrintSchema = new LCContPrintSchema();
            if (!StrTool.cTrim(tLCGrpContDB.getCardFlag()).equals("0"))
            {
                System.out.println("开始放入xml");
                tLCContPrintSchema.setPrintID(PubFun1.CreateMaxNo("PrintID", 11));
                tLCContPrintSchema.setPrtNo(mPrtNo);
                tLCContPrintSchema.setOtherNo(mGrpContNo);
                tLCContPrintSchema.setOtherNoType("2");
                tLCContPrintSchema.setPrintCount(mPrintCount);
                tLCContPrintSchema.setFilePath("printdata/data/" + mGrpContNo + ".xml");
                tLCContPrintSchema.setCValiDate(mcvalidate);
                tLCContPrintSchema.setManageCom(mManageCom);
                tLCContPrintSchema.setOperator(tGlobalInput.Operator);
                tLCContPrintSchema.setMakeDate(mdate);
                tLCContPrintSchema.setMakeTime(mtime);
                tLCContPrintSchema.setModifyDate(mdate);
                tLCContPrintSchema.setModifyTime(mtime);
                
                //如果不需要打印保单，置下载次数为 1，即已下载状态
                if(isFilialePrint || "0".equals(contPrintFlag))
                {
                    tLCContPrintSchema.setDownloadCount(1);
                    tLCContPrintSchema.setDownloadDate(mdate);
                    tLCContPrintSchema.setDownloadTime(mtime);
                    tLCContPrintSchema.setDownOperator(tGlobalInput.Operator);
                }
                //加入set中
                tLCContPrintSet.add(tLCContPrintSchema);
                System.out.println("xml放入完毕");
            }

            //判断,如果是卡单类型单独生成xml
            int m = 0;
            byte[] d = new byte[4096];
            if (!StrTool.cTrim(tLCGrpContDB.getCardFlag()).equals(""))
            {
                while ((m = ins_Card.read(d)) != -1)
                {
                    fos_card.write(d, 0, m);
                }
                fos.close();
            }

            //获取保单影印件列表信息
            String[][] tScanPic = (String[][]) mInputData.get(4);
            ///****************************************************************
            // * 只有在正式机才启用这部分代码。共两段。当前(1/2) 2007-07-23
            // * 
            LDSysVarDB tLDSysVarDB = new LDSysVarDB();
            LDSysVarSet tLDSysVarSet = tLDSysVarDB
                .executeQuery("select * from ldsysvar where sysvar='InnerServiceURL'");
            if (tLDSysVarSet.size() < 1)
            {
                CError tError = new CError();
                tError.moduleName = "LCContF1PBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "缺少内部服务URL的描述数据！";
                this.mErrors.addOneError(tError);
                return false;
            }
            String InnerServiceURL = tLDSysVarSet.get(1).getSysVarValue();
            // * 
            // ***************************************************************/
            if (tScanPic != null)
            {
                for (int i = 0; i < tScanPic.length; i++)
                {
                    ////替换内部服务ip
                    ///********************************************************
                    // * 只有在正式机才启用这部分代码。共两段。当前(2/2) 2007-07-23
                    String subtScanPic = tScanPic[i][0].replaceAll("http://","");
                    String replaceValue = subtScanPic.substring(0,
                            subtScanPic.indexOf("/"));
                    tScanPic[i][0] = tScanPic[i][0].replaceAll("http://"
                            + replaceValue, InnerServiceURL);
                    // * 
                    // *******************************************************/

                    URL jspUrl = new URL(tScanPic[i][0]);
                    URLConnection uc = jspUrl.openConnection();
                    System.out.println("URL===" + uc.getURL());
                    fos = new FileOutputStream(FilePath + "/" + tScanPic[i][1]);
                    int fn = 0;
                    //while ((fn = uc.getInputStream().read()) != -1)
                    //{
                    //    fos.write(fn);
                    //}
                    while ((n = uc.getInputStream().read(c)) != -1)
                    {
                        fos.write(c, 0, n);
                    }
                    fos.close();
                    //循环加入图片
                    System.out.println("开始放入图片");
                    LCContPrintSchema mtLCContPrintSchema = new LCContPrintSchema();
                    mtLCContPrintSchema.setPrintID(PubFun1.CreateMaxNo("PrintID", 11));
                    mtLCContPrintSchema.setPrtNo(mPrtNo);
                    mtLCContPrintSchema.setOtherNo(mGrpContNo);
                    mtLCContPrintSchema.setOtherNoType("2");
                    mtLCContPrintSchema.setPrintCount(mPrintCount);
                    mtLCContPrintSchema.setFilePath("printdata/data/" + tScanPic[i][1]);
                    mtLCContPrintSchema.setCValiDate(mcvalidate);
                    mtLCContPrintSchema.setManageCom(mManageCom);
                    mtLCContPrintSchema.setOperator(tGlobalInput.Operator);
                    mtLCContPrintSchema.setMakeDate(mdate);
                    mtLCContPrintSchema.setMakeTime(mtime);
                    mtLCContPrintSchema.setModifyDate(mdate);
                    mtLCContPrintSchema.setModifyTime(mtime);
                    
                    //如果不需要打印保单，置下载次数为 1，即已下载状态
                    if(isFilialePrint || "0".equals(contPrintFlag))
                    {
                    	mtLCContPrintSchema.setDownloadCount(1);
                    	mtLCContPrintSchema.setDownloadDate(mdate);
                    	mtLCContPrintSchema.setDownloadTime(mtime);
                    	mtLCContPrintSchema.setDownOperator(tGlobalInput.Operator);
                    }
                    //加入set中
                    tLCContPrintSet.add(mtLCContPrintSchema);
                    map.put(tLCContPrintSet, "INSERT");
                }
            }
            LCContPrintDBSet tLCContPrintDBSet = new LCContPrintDBSet(conn);
            tLCContPrintDBSet.set(tLCContPrintSet);

            if (!tLCContPrintDBSet.insert())
            {
                conn.rollback();
                conn.close();
                return false;
            }

            //            //获取协议影印件列表信息
            //            String[][] tScanDoc = (String[][]) mInputData.get(5);
            //            if (tScanDoc != null) {
            //                for (int i = 0; i < tScanDoc.length; i++) {
            //                    URL jspUrl = new URL(tScanDoc[i][0]);
            //                    URLConnection uc = jspUrl.openConnection();
            //                    fos = new FileOutputStream(FilePath + "/" + tScanDoc[i][1]);
            //                    int fn = 0;
            ////                    while ((fn = uc.getInputStream().read()) != -1)
            ////                    {
            ////                        fos.write(fn);
            ////                    }
            //                    while ((n = uc.getInputStream().read(c)) != -1) {
            //                        fos.write(c, 0, n);
            //                    }
            //                    fos.close();
            //                }
            //            }
            
            //分公司保单打印，且需要打印保单，则需要发送新外包
            if(isFilialePrint && "1".equals(contPrintFlag))
            {
                if(!sendXML(XmlFile))
                {
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            
            if (!dealreceive())
            {
                conn.rollback();
                conn.close();
                return false;
            }

            conn.commit();
            conn.close();
            System.out.println("commit end");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpContF1PBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {
            }
            tReturn = false;
        }

        return tReturn;
    }

    /**
     * 将所有得到的打印数据按照主险保单号存入数据库，为以后的补打保单做准备
     * @param cXMLDataSets XMLDatasets
     * @param cGlobalInput GlobalInput
     * @param conn Connection
     * @throws Exception
     */
    private void saveDataStream(XMLDatasets cXMLDataSets,
            GlobalInput cGlobalInput, Connection conn) throws Exception
    {

        InputStream tIns = cXMLDataSets.getInputStream();
        saveOneDataStream(tIns, cGlobalInput, conn);
        //        Document doc = aXMLDataSets.getDocument();
        //        List list = doc.getRootElement().getChildren("DATASET");
        //        //转换xml流数据为document对象
        //        for (int nIndex = 0; nIndex < list.size(); nIndex++)
        //        {
        //            Element ele = (Element) list.get(nIndex);
        //            // Build a new element from the content of old element.
        //            // By doing this, we can detach element from document
        //            ele = new Element("DATASET").setMixedContent(ele.getMixedContent());
        //            // Document newDoc = new Document(new Element("DATASETS").addContent(ele));
        //            Document newDoc = new Document(ele);
        //        }
    }

    /**
     * 保存一条保单打印的数据
     * @param cIns InputStream
     * @param cGlobalInput GlobalInput
     * @param conn Connection
     * @throws Exception
     */
    private void saveOneDataStream(InputStream cIns, GlobalInput cGlobalInput,
            Connection conn) throws Exception
    {
        try
        {
            //获得合同单号
            String strCurDate = PubFun.getCurrentDate();
            String strCurTime = PubFun.getCurrentTime();
            //数据库中删除旧有数据，针对保单重打时候的操作
            LCPolPrintDB tLCPolPrintDB = new LCPolPrintDB(conn);
            tLCPolPrintDB.setMainPolNo(mGrpContNo);
            //删除操作改用db的delete方法执行，通用化
            if (!tLCPolPrintDB.delete())
            {
                throw new Exception("删除数据失败！");
            }

            String[] parmStrArr = new String[4];
            StringBuffer tSBql = new StringBuffer(256);
            //插入数据操作
            //            parmStrArr[0] = "INSERT INTO LCPolPrint(MainPolNo, PrtTimes, Operator, MakeDate, MakeTime, ModifyDate, ModifyTime,PolInfo, PolType) VALUES('"
            //                            + mGrpContNo + "', 1, '" +
            //                            cGlobalInput.Operator +
            //                            "', '" + strCurDate + "', '" + strCurTime + "', '" +
            //                            strCurDate + "', '" + strCurTime +
            //                            "',empty_blob() ,'2')";
            tSBql
                    .append("INSERT INTO LCPolPrint(MainPolNo, PrtTimes, Operator, MakeDate, MakeTime, ModifyDate, ModifyTime,PolInfo, PolType) VALUES('");
            tSBql.append(mGrpContNo);
            tSBql.append("', 1, '");
            tSBql.append(cGlobalInput.Operator);
            tSBql.append("', '");
            tSBql.append(strCurDate);
            tSBql.append("', '");
            tSBql.append(strCurTime);
            tSBql.append("', '");
            tSBql.append(strCurDate);
            tSBql.append("', '");
            tSBql.append(strCurTime);
            tSBql.append("',empty_blob() ,'2')");

            parmStrArr[0] = tSBql.toString();
            parmStrArr[1] = "LCPolPrint";
            parmStrArr[2] = "PolInfo";
            parmStrArr[3] = " and MainPolNo = '" + mGrpContNo + "' ";

            //            CBlob tCBlob = new CBlob();
            if (!CBlob.BlobInsert(cIns, parmStrArr, conn))
            {
                throw new Exception("Blob数据操作失败！");
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * Kevin 2003-03-27
     * 补打保单时对应的保存函数
     * @param vData VData
     * @return boolean
     */
    private boolean update(VData vData)
    {
        LCGrpContSchema tLCGrpContSchema = (LCGrpContSchema) vData
                .getObjectByObjectName("LCGrpContSchema", 0);

        Connection conn = null;
        Statement stmt = null;
        //        String strSQL = "";

        try
        {
            conn = DBConnPool.getConnection();
            stmt = conn.createStatement();

            if (conn == null)
            {
                throw new Exception("连接数据库失败！");
            }
            conn.setAutoCommit(false);
            //            strSQL = "UPDATE LCPolPrint SET ModifyDate = '" +
            //                     PubFun.getCurrentDate() + "' , ModifyTime = '" +
            //                     PubFun.getCurrentTime()
            //                     + "' WHERE MainPolNo = '" + tLCGrpContSchema.getGrpContNo() +
            //                     "'";
            StringBuffer tSBql = new StringBuffer(256);
            tSBql.append("UPDATE LCPolPrint SET ModifyDate = '");
            tSBql.append(PubFun.getCurrentDate());
            tSBql.append("' , ModifyTime = '");
            tSBql.append(PubFun.getCurrentTime());
            tSBql.append("' WHERE MainPolNo = '");
            tSBql.append(tLCGrpContSchema.getGrpContNo());
            tSBql.append("'");

            stmt.executeUpdate(tSBql.toString());
            conn.commit();
            if (stmt != null)
            {
                stmt.close();
            }
            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            try
            {
                if (stmt != null)
                {
                    stmt.close();
                }
                if (conn != null)
                {
                    conn.close();
                }
            }
            catch (Exception e)
            {
                // do nothing
            }
            buildError("getRePrintData", ex.getMessage());
            return false;
        }

        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 处理合同接收
     * @param cIns InputStream
     * @param cGlobalInput GlobalInput
     * @param conn Connection
     * @throws Exception
     */
    private boolean dealreceive()
    {
        System.out.println("in dealreceive");
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        LAAgentSet tLAAgentSet = tLAAgentDB.executeQuery("select * from laagent where agentcode='"
        		+ mLCGrpContSchema.getAgentCode()
        		//去除业务员是团险销售渠道的校验    2008-7-7
        		//+ "' and branchtype='2'");
        		+ "'");
        tLAAgentSchema = tLAAgentSet.get(1);
        String tAgentName = tLAAgentSchema.getName();
        LCContReceiveDBSet cLCContReceiveDBSet = new LCContReceiveDBSet();
        LCContReceiveSet cLCContReceiveSet = new LCContReceiveSet();
        LCContReceiveDBSet c2LCContReceiveDBSet = new LCContReceiveDBSet();
        LCContReceiveSet c2LCContReceiveSet = new LCContReceiveSet();

        LCContReceiveSchema tLCContReceiveSchema = new LCContReceiveSchema();
        tLCContReceiveSchema.setReceiveID(1);
        tLCContReceiveSchema.setContNo(mLCGrpContSchema.getGrpContNo());
        tLCContReceiveSchema.setPrtNo(mLCGrpContSchema.getPrtNo());
        tLCContReceiveSchema.setContType("2");
        tLCContReceiveSchema.setCValiDate(mLCGrpContSchema.getCValiDate());
        tLCContReceiveSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
        tLCContReceiveSchema.setAppntName(mLCGrpContSchema.getGrpName());
        tLCContReceiveSchema.setPrem(mLCGrpContSchema.getPrem());
        tLCContReceiveSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        tLCContReceiveSchema.setAgentName(tAgentName);
        tLCContReceiveSchema.setSignDate(mLCGrpContSchema.getSignDate());
        tLCContReceiveSchema.setManageCom(mLCGrpContSchema.getManageCom());
        tLCContReceiveSchema.setPrintManageCom(mGlobalInput.ManageCom);
        tLCContReceiveSchema.setPrintOperator(mGlobalInput.Operator);
        tLCContReceiveSchema.setPrintDate(mdate);
        tLCContReceiveSchema.setPrintTime(mtime);
        tLCContReceiveSchema.setPrintCount(mPrintCount);
        tLCContReceiveSchema.setReceiveState("0");
        tLCContReceiveSchema.setDealState("0");
        tLCContReceiveSchema.setMakeDate(mdate);
        tLCContReceiveSchema.setMakeTime(mtime);
        tLCContReceiveSchema.setModifyDate(mdate);
        tLCContReceiveSchema.setModifyTime(mtime);
        
        //分公司保单打印自动接收
        if(isFilialePrint)
        {
            tLCContReceiveSchema.setReceiveManageCom(mGlobalInput.ManageCom);
            tLCContReceiveSchema.setReceiveOperator(mGlobalInput.Operator);
            tLCContReceiveSchema.setReceiveDate(mdate);
            tLCContReceiveSchema.setReceiveTime(mtime);
            tLCContReceiveSchema.setReceiveState("1");
        }

        LCContReceiveDB tLCContReceiveDB = new LCContReceiveDB();
        LCContReceiveSet tLCContReceiveSet = new LCContReceiveSet();
        tLCContReceiveDB.setContNo(mLCGrpContSchema.getGrpContNo());
        tLCContReceiveSet = tLCContReceiveDB.query();

        if (tLCContReceiveSet.size() > 0)
        {
            int k = tLCContReceiveSet.size() + 1;
            System.out.println("第" + k + "次生成合同接收记录");
            tLCContReceiveSchema.setReceiveID(tLCContReceiveSet.size() + 1);
            LCContReceiveDB t2LCContReceiveDB = new LCContReceiveDB();
            LCContReceiveSet t2LCContReceiveSet = new LCContReceiveSet();
            t2LCContReceiveDB.setContNo(mLCGrpContSchema.getGrpContNo());
            t2LCContReceiveDB.setDealState("0");
            t2LCContReceiveSet = t2LCContReceiveDB.query();
            if (t2LCContReceiveSet.size() > 0)
            {
                for (int i = 1; i <= t2LCContReceiveSet.size(); i++)
                {
                    LCContReceiveSchema t2LCContReceiveSchema = new LCContReceiveSchema();
                    t2LCContReceiveSchema = t2LCContReceiveSet.get(i)
                            .getSchema();
                    t2LCContReceiveSchema.setDealState("1");
                    c2LCContReceiveSet.add(t2LCContReceiveSchema);
                }
            }

        }
        else
        {
            System.out.println("第1次生成合同接收记录");
        }
        if (c2LCContReceiveSet.size() > 0)
        {
            c2LCContReceiveDBSet.add(c2LCContReceiveSet);
            if (!c2LCContReceiveDBSet.update())
            {
                return false;
            }
        }
        cLCContReceiveSet.add(tLCContReceiveSchema);
        cLCContReceiveDBSet.add(cLCContReceiveSet);
        if (!cLCContReceiveDBSet.insert())
        {
            return false;
        }
        
        //分公司保单打印，如果没有合同接收信息，则增加
        if(isFilialePrint)
        {
            LCContGetPolSet tLCContGetPolSet = new LCContGetPolSet();
            LCContGetPolDB tLCContGetPolDB = new LCContGetPolDB();
            tLCContGetPolDB.setContNo(mLCGrpContSchema.getGrpContNo());
            tLCContGetPolSet = tLCContGetPolDB.query();
            if (tLCContGetPolSet.size() != 1)
            {
                LCContGetPolSchema tLCContGetPolSchema = new LCContGetPolSchema();
                tLCContGetPolSchema.setContNo(mLCGrpContSchema.getGrpContNo());
                tLCContGetPolSchema.setPrtNo(mLCGrpContSchema.getPrtNo());
                tLCContGetPolSchema.setContType("2");
                tLCContGetPolSchema.setCValiDate(mLCGrpContSchema.getCValiDate());
                tLCContGetPolSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
                tLCContGetPolSchema.setAppntName(mLCGrpContSchema.getGrpName());
                tLCContGetPolSchema.setPrem(mLCGrpContSchema.getPrem());
                tLCContGetPolSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
                tLCContGetPolSchema.setAgentName(tAgentName);
                tLCContGetPolSchema.setSignDate(mLCGrpContSchema.getSignDate());
                tLCContGetPolSchema.setManageCom(mLCGrpContSchema.getManageCom());
                tLCContGetPolSchema.setReceiveManageCom(mManageCom);
                tLCContGetPolSchema.setReceiveOperator(mGlobalInput.Operator);
                tLCContGetPolSchema.setReceiveDate(mdate);
                tLCContGetPolSchema.setReceiveTime(mtime);
                tLCContGetPolSchema.setGetpolState("0");
                tLCContGetPolSchema.setMakeDate(mdate);
                tLCContGetPolSchema.setMakeTime(mtime);
                tLCContGetPolSchema.setModifyDate(mdate);
                tLCContGetPolSchema.setModifyTime(mtime);

                LCContGetPolDBSet cLCContGetPolDBSet = new LCContGetPolDBSet();
                cLCContGetPolDBSet.add(tLCContGetPolSchema);
                if (!cLCContGetPolDBSet.insert())
                {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * sendXML
     * 将xml发送到指定目录
     * @param cXmlFile String
     * @return boolean
     */
    private boolean sendXML(String cXmlFile) throws SocketException, IOException
    {
        String tIP = CommonBL.getCodeName("printserver", "IP");
        String tPort = CommonBL.getCodeName("printserver", "Port");
        String tUserName = CommonBL.getCodeName("printserver", "UserName");
        String tPassword = CommonBL.getCodeName("printserver", "Password");

        if(tIP == null || tIP.equals("") || tPort == null || tPort.equals("")
            || tUserName == null || tUserName.equals("")
            || tPassword == null || tPassword.equals(""))
        {
            mErrors.addOneError("FTP服务器IP、用户名、密码、端口都不能为空，否则无法发送xml");
            return false;
        }

        FTPTool tFTPTool = new FTPTool(tIP, tUserName, tPassword,
                                       Integer.parseInt(tPort));
        try
        {
            if(!tFTPTool.loginFTP())
            {
                mErrors.addOneError("登录ftp服务器失败");
                return false;
            }
        }
        catch(SocketException ex)
        {
            mErrors.addOneError("无法登录ftp服务器");
            return false;
        }
        catch(IOException ex)
        {
            mErrors.addOneError("无法读取ftp服务器信息");
            return false;
        }

        if(!tFTPTool.upload(".", cXmlFile))
        {
            System.out.println("上载文件失败!");
            CError tError = new CError();
            tError.moduleName = "LCGrpContF1PBLS";
            tError.functionName = "sendXML";
            tError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
            mErrors.addOneError(tError);
            tFTPTool.logoutFTP();
            System.out.println(tError.errorMessage);

            return false;
        }
        tFTPTool.logoutFTP();

        return true;
    }

}
