/**
 * <p>Title:无输入统计项的的清单报表</p>
 * <p>Description: 4张报表</p>
 * <p>claim5：七日结案率</p>
 * <p>claim6：三日理赔调查完成率</p>
 * <p>claim10：人均案件状况（分审核人）</p>
 * <p>claim11：机构理赔进度月报（分审核人）</p>
 * <p>claim15：调查件占比</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author guoxiang
 * @version 1.0
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ReportPubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class RiskClaimstatisticBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String[] mDay = null;
    private String mDefineCode = "";

    public RiskClaimstatisticBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY"))
        {
            buildError("submitData", "不支持的操作字符串");

            return false;
        }

        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        if (!getPrintDataPay())
        {
            return false;
        }

        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "RiskClaimStatisisticBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        System.out.println("大小：" + cInputData.size());
        mDefineCode = (String) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput",
                0));

        if (mDefineCode == "")
        {
            buildError("mDefineCode", "没有得到足够的信息！");

            return false;
        }
        mDay = (String[]) cInputData.get(1);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");

            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private boolean getPrintDataPay()
    {
        System.out.println("报表代码类型：" + mDefineCode);

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        String[] strArr = null;
        ListTable tlistTable = new ListTable();
        FinDayTool tFinDayTool = new FinDayTool();

        ////////////////////////////////////////////////////人均案件状况///////////////////////////////////////////////
        if (mDefineCode.equals("claim10"))
        {
            xmlexport.createDocument("RiskClaimAvgPepole.vts", "printer"); //最好紧接着就初始化xml文档
            tlistTable.setName("claim10"); //xmlname="人均案件状况";

            String kSql =
                    "select MngCom from  llclaimuwmain where AppActionType='1'"
                    + ReportPubFun.getWherePart("makedate", mDay[0], mDay[1], 1);
            System.out.println("-------------------------结案总数-------------");

            ExeSQL pExeSQL = new ExeSQL();
            SSRS pSSRS = new SSRS();
            pSSRS = pExeSQL.execSQL(kSql);

            String Sql = "select ComCode from LLClaimUser";
            System.out.println("-----------------结案人数-------------");

            SSRS nSSRS = new SSRS();
            ExeSQL nExeSQL = new ExeSQL();
            nSSRS = nExeSQL.execSQL(Sql);

            String UWJ_sql =
                    "select comCode from ldcom where length(trim(comCode))=4";
            System.out.println("-------------获得唯一的4位和两位管理机构的sql语句是-----------");

            ExeSQL kExeSQL = new ExeSQL();
            SSRS kSSRS = new SSRS();
            kSSRS = kExeSQL.execSQL(UWJ_sql);
            for (int i = 1; i <= kSSRS.MaxRow; i++)
            {
                strArr = new String[5];
                strArr[0] = ReportPubFun.getMngName(kSSRS.GetText(i, 1));
                strArr[1] = kSSRS.GetText(i, 1); //机构代码
                strArr[2] = ReportPubFun.getCount(pSSRS, kSSRS.GetText(i, 1)); //案件数
                strArr[3] = ReportPubFun.getCount(nSSRS, kSSRS.GetText(i, 1)); //人数
                strArr[4] = ReportPubFun.functionDivision(strArr[2], strArr[3],
                        "0.0"); //均案
                tlistTable.add(strArr);
            }
            strArr = new String[5];
            strArr[0] = "Mange";
            strArr[1] = "MangeCode";
            strArr[2] = "SumCaseCount";
            strArr[3] = "SumPepoleCount";
            strArr[4] = "AvgPepoleCount";
            xmlexport.addListTable(tlistTable, strArr);
        }
        if (mDefineCode.equals("claim11"))
        { //机构理赔进度月报
            xmlexport.createDocument("RiskClaimSchedule.vts", "printer"); //最好紧接着就初始化xml文档
            tlistTable.setName("claim11");

            String nSql = "select mngcom from llregister where 1=1"
                          + ReportPubFun.getWherePart("rgtdate", mDay[0],
                    mDay[1], 1);
            System.out.println(
                    "----------------------求（立案）总数的-------------------");

            ExeSQL nExeSQL = new ExeSQL();
            SSRS nSSRS = new SSRS();
            nSSRS = nExeSQL.execSQL(nSql);

            String mSql = "select mngcom from llclaim"
                          + " where clmstate in('2','3')"
                          + ReportPubFun.getWherePart("endcasedate", mDay[0],
                    mDay[1], 1);
            System.out.println(
                    "---------------------已结案的案件数的----------------------");

            ExeSQL mExeSQL = new ExeSQL();
            SSRS mSSRS = new SSRS();
            mSSRS = mExeSQL.execSQL(mSql);

            String sSql = "select mngcom from llregister"
                          +
                    " where rgtno in(select rgtno from llclaim where clmstate not in('2','3'))"
                          + ReportPubFun.getWherePart("llregister.rgtdate",
                    mDay[0], mDay[1], 1);
            System.out.println("---------------未结案的案件数的-------------------");

            ExeSQL sExeSQL = new ExeSQL();
            SSRS sSSRS = new SSRS();
            sSSRS = sExeSQL.execSQL(sSql);

            String kSql =
                    "select llregister.mngcom from  llclaimuwmain,llregister "
                    + " where (llclaimuwmain.makedate-llregister.rgtdate)>7"
                    +
                    " and llclaimuwmain.rgtno =llregister.rgtno and llclaimuwmain.appactiontype='1'"
                    + ReportPubFun.getWherePart("llregister.rgtdate",
                                                mDay[0], "", 1)
                    + ReportPubFun.getWherePart("llclaimuwmain.makedate",
                                                "", mDay[1], 1);
            System.out.println(
                    "-------------------7日未结案数-----------------------------");

            ExeSQL pExeSQL = new ExeSQL();
            SSRS pSSRS = new SSRS();
            pSSRS = pExeSQL.execSQL(kSql);

            String UWJ_sql =
                    "select comCode from ldcom where length(trim(comCode))=4";
            System.out.println("-------------获得唯一的4位管理机构的sql语句是-----------");

            ExeSQL kExeSQL = new ExeSQL();
            SSRS kSSRS = new SSRS();
            kSSRS = kExeSQL.execSQL(UWJ_sql);

            for (int i = 1; i <= kSSRS.MaxRow; i++)
            {
                strArr = new String[6];
                strArr[0] = ReportPubFun.getMngName(kSSRS.GetText(i, 1));
                strArr[1] = kSSRS.GetText(i, 1); //机构代码
                strArr[2] = ReportPubFun.getCount(nSSRS, kSSRS.GetText(i, 1)); //立案件数
                strArr[3] = ReportPubFun.getCount(mSSRS, kSSRS.GetText(i, 1)); //结案件数
                strArr[4] = ReportPubFun.getCount(sSSRS, kSSRS.GetText(i, 1)); //未案件数
                strArr[5] = ReportPubFun.getCount(pSSRS, kSSRS.GetText(i, 1)); //七日案件数
                tlistTable.add(strArr);
            }
            tlistTable.setName("claim11"); //xmlname="机构理赔进度月报";
            strArr = new String[6];
            strArr[0] = "Mange";
            strArr[1] = "MangeCode";
            strArr[2] = "SumCaseCount";
            strArr[3] = "SumEndCaseCount";
            strArr[4] = "SumNoCaseCount";
            strArr[5] = "SumNoSevenCount";
            xmlexport.addListTable(tlistTable, strArr);
        }

        /////////////////////////////////////////////claim15：调查件占比//////////////////////////////////////
        if (mDefineCode.equals("claim15"))
        {
            xmlexport.createDocument("RiskClaimSurvey.vts", "printer"); //最好紧接着就初始化xml文档

            String mSql = "select mngcom from llclaim"
                          + " where clmstate in('2','3')"
                          + ReportPubFun.getWherePart("endcasedate", mDay[0],
                    mDay[1], 1);
            System.out.println(
                    "---------------------已结案的案件数的----------------------");

            ExeSQL mExeSQL = new ExeSQL();
            SSRS mSSRS = new SSRS();
            mSSRS = mExeSQL.execSQL(mSql);

            System.out.println("---------------结案中的调查件----------------");

            String nSql = "select llsurvey.mngcom from llsurvey,llclaim"
                          + " where llsurvey.rgtno=llclaim.rgtno"
                          + " and llclaim.clmstate in('2','3')"
                          + ReportPubFun.getWherePart("llclaim.endcasedate",
                    mDay[0], mDay[1], 1);
            ExeSQL pExeSQL = new ExeSQL();
            SSRS pSSRS = new SSRS();
            pSSRS = pExeSQL.execSQL(nSql);

            String UWJ_sql =
                    "select comCode from ldcom where length(trim(comCode))=4";
            System.out.println("-------------获得唯一的4位管理机构的sql语句是-----------");

            ExeSQL kExeSQL = new ExeSQL();
            SSRS kSSRS = new SSRS();
            kSSRS = kExeSQL.execSQL(UWJ_sql);
            tlistTable.setName("claim15");
            for (int i = 1; i <= kSSRS.MaxRow; i++)
            {
                strArr = new String[4];
                strArr[0] = ReportPubFun.getMngName(kSSRS.GetText(i, 1));
                strArr[1] = kSSRS.GetText(i, 1); //机构代码
                strArr[2] = ""; //无
                strArr[3] = ReportPubFun.functionDivision(ReportPubFun.getCount(
                        pSSRS,
                        kSSRS
                        .GetText(i,
                                 1)),
                        ReportPubFun.getCount(mSSRS,
                                              kSSRS
                                              .GetText(i,
                        1)),
                        "0.000"); //案件数
                tlistTable.add(strArr);
            }
            strArr = new String[4];
            strArr[0] = "Mange";
            strArr[1] = "MangeCode";
            strArr[2] = "NO";
            strArr[3] = "Survey";
            xmlexport.addListTable(tlistTable, strArr);
        }

        String CurrentDate = PubFun.getCurrentDate();
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("Operator", mGlobalInput.Operator);
        texttag.add("time", CurrentDate);
        System.out.println("大小" + texttag.size());
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }

        //xmlexport.outputDocumentToFile("e:\\",xmlname);//输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    public static void main(String[] args)
    {
    }
}
