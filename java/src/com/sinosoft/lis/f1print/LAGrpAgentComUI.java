package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.f1print.LAGrpAgentComBL;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author xiangchun
 * @version 1.0
 */
public class LAGrpAgentComUI{

    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();


    public LAGrpAgentComUI() {
    }

    public boolean submitData(VData cInputData, String cOperate) {

        LAGrpAgentComBL tLAGrpAgentComBL = new LAGrpAgentComBL();

        if (!tLAGrpAgentComBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tLAGrpAgentComBL.mErrors);

            return false;
        } else {
            this.mResult = tLAGrpAgentComBL.getResult();
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }


    public static void main(String[] args) {

    }
}
