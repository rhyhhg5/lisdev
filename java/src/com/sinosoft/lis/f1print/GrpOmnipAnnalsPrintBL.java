package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpAddressSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCGrpAddressDB;
import com.sinosoft.lis.db.LCGrpAppntDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LCGrpAddressSet;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.utility.TextTag;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import java.text.DecimalFormat;

/**
 * <p>Title: 团体万能险年报打印（单打） </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author fuxin
 * @version 1.0
 */
public class GrpOmnipAnnalsPrintBL implements PrintService {


    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private VData mResult = new VData();

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    private LCContSchema mLCContSchema = new LCContSchema();

    private LCGrpAddressSchema mLCGrpAddressSchema = new LCGrpAddressSchema();

    TextTag textTag = new TextTag(); //新建一个TextTag的实例tLJSPaySchema

    SSRS mSSRS = new SSRS();

    SSRS tSSRS = new SSRS();

    private String mPolYear = "";

    private String mPolMonth ="";

    private String mOperate = "";

    private String mRunDateStart = ""; //批处理起期

    private String mRunDateEnd = "" ;   //

    private String mTempPolYear ="";

    private String mStartDate ="";

    private String mEndDate ="";

    private String mSumMoney ="";
    
    private ExeSQL mExeSQL = new ExeSQL();
    
    DecimalFormat df = new DecimalFormat("#.00");

    public GrpOmnipAnnalsPrintBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("==GrpOmnipAnnalsPrintBL start==");

        this.mOperate = cOperate;
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }

        //加入到打印列表
        if (!dealPrintMag()) {
            return false;
        }

        System.out.println("==OmnipAnnalsPrintNewBL end==");

        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.getObjectByObjectName(
                "LOPRTManagerSchema", 0);
        if( mGlobalInput==null || mLOPRTManagerSchema==null )
        {
            CError tError = new CError();
            tError.moduleName = "OmnipAnnalsPrintNewBL.java";
            tError.functionName = "submitData";
            tError.errorMessage = "获取数据失败！";
            mErrors.addOneError(tError);
            return false;
        }
        mPolMonth = mLOPRTManagerSchema.getStandbyFlag1();
        mPolYear  = mLOPRTManagerSchema.getStandbyFlag3();
        System.out.println("保单的结算年度："+mPolYear+"年，结算月度："+mPolMonth+"'月");

        mRunDateStart = mLOPRTManagerSchema.getStandbyFlag2();

        mRunDateEnd   = mLOPRTManagerSchema.getStandbyFlag4();

//        LCContDB tLCContDB = new LCContDB();
//        tLCContDB.setContNo(mLOPRTManagerSchema.getOtherNo());
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLOPRTManagerSchema.getOtherNo());
        
        if (!tLCGrpContDB.getInfo()) {
            CError tError = new CError();
            tError.moduleName = "GrpOmnipAnnalsPrintBL.java";
            tError.functionName = "dealPrintMag";
            tError.errorMessage = "获得保单信息失败！";
            mErrors.addOneError(tError);
            return false;
        }
        mLCGrpContSchema = tLCGrpContDB.getSchema();

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    public CErrors getErrors() {
        return this.mErrors;
    }


    private boolean getPrintData()
    {
        //业务员信息
        String agntPhone = "";
        String temPhone = "";
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(mLCGrpContSchema.getAgentCode());
        tLaAgentDB.getInfo();
        textTag.add("AgentName", tLaAgentDB.getName());
        textTag.add("AgentCode", tLaAgentDB.getAgentCode());
        temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") || temPhone.equals("null")) {
            agntPhone = tLaAgentDB.getPhone();
        } else {
            agntPhone = temPhone;
        }
//        textTag.add("Phone", agntPhone);
        textTag.add("AgentPhone", agntPhone);

         XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
         xmlexport.createDocument("GrpOmnipAnnals.vts", "printer");
         textTag.add("JetFormType", "OM004");


        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            CError.buildErr(this,"操作员机构查询出错！");
            return false;
        }

         String printcom ="select codename from ldcode where codetype='pdfprintcom' and code='"
                          +comcode + "' with ur";
         String printcode = new ExeSQL().getOneValue(printcom);
         textTag.add("ManageComLength4", printcode);
         textTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
         if ("batch".equals(mOperate)) {
             textTag.add("previewflag", "0");
         } else {
             textTag.add("previewflag", "1");
         }

        //机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        String branchSQL = " select * from LABranchGroup where agentgroup = "
                           +
                           " (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
                           +
                           " (select agentgroup from laagent where agentcode ='"
                           + tLaAgentDB.getAgentCode() + "'))"
                           ;
        LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(
                branchSQL);
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            CError.buildErr(this, "查询业务员机构失败");
            return false;
        }
        textTag.add("AgentGroup", tLABranchGroupSet.get(1).getName());

        //获取投保人地址
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());

        if (!tLCGrpAppntDB.getInfo()) {
            CError.buildErr(this,"投保人表中缺少数据");
            return false;
        }

        String sql =" select * from LCGrpAddress where CustomerNo=(select appntno from LCGrpCont where GrpContNo='"
                   +  mLCGrpContSchema.getGrpContNo() + "') AND AddressNo = '"
                   +  tLCGrpAppntDB.getAddressNo() + "'"
                   ;
        System.out.println("sql=" + sql);
        LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
        LCGrpAddressSet tLCGrpAddressSet = tLCGrpAddressDB.executeQuery(sql);
        mLCGrpAddressSchema = tLCGrpAddressSet.get(tLCGrpAddressSet.size()).getSchema();
        textTag.add("Name",tLCGrpAppntDB.getName()); //投保单位

        setFixedInfo();
        textTag.add("GrpZipCode", mLCGrpAddressSchema.getGrpZipCode());
        System.out.println("GrpZipCode=" + mLCGrpAddressSchema.getGrpZipCode());
        textTag.add("GrpAddress", mLCGrpAddressSchema.getGrpAddress());
        String appntPhoneStr = " ";
        if (mLCGrpAddressSchema.getPhone1() != null &&
            !mLCGrpAddressSchema.getPhone1().equals("")) {
            appntPhoneStr += mLCGrpAddressSchema.getPhone1() + "、";
        }
        if (mLCGrpAddressSchema.getPhone2() != null &&
            !mLCGrpAddressSchema.getPhone2().equals("")) {
            appntPhoneStr += mLCGrpAddressSchema.getPhone2() + "、";
        }
        if (mLCGrpAddressSchema.getMobile1() != null &&
            !mLCGrpAddressSchema.getMobile1().equals("")) {
            appntPhoneStr += mLCGrpAddressSchema.getMobile1() + "、";
        }
        if (mLCGrpAddressSchema.getMobile2() != null &&
            !mLCGrpAddressSchema.getMobile2().equals("")) {
            appntPhoneStr += mLCGrpAddressSchema.getMobile2() + "、";
        }


//        textTag.add("PrintDate",PubFun.getCurrentDate2());//打印日期
        textTag.add("GrpContNo",mLCGrpContSchema.getGrpContNo());

        //被保人信息
//        textTag.add("InsuredName",mLCContSchema.getInsuredName());
      //结算年度：
        mTempPolYear = new ExeSQL().getOneValue(
                "select distinct polYear From lcinsureaccbalance a,lcinsureacc b where  a.contno=b.contno and " +
                " a.DueBalaDate>='"+mRunDateStart+"'  and a.DueBalaDate<='"+mRunDateEnd+"'" +
                " and a.insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "+  
                "	(select riskcode from lcgrppol where grpcontno =b.grpcontno) and  payplancode  in  "+ 
                "	(select payplancode from LMDutyPay where AccPayClass ='3'))  "+
                " and a.polmonth = 12 and  b.grpcontno='" +
                	mLCGrpContSchema.getGrpContNo() +
                "' with ur ");
        //报告期间
        if("1".equals(mTempPolYear))
        {
        	mStartDate = new ExeSQL().getOneValue(
                    "select  min(DueBalaDate) From lcinsureaccbalance a,lcinsureacc b where  a.contno=b.contno " +
                    " and a.insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "+  
                    "	(select riskcode from lcgrppol where grpcontno =b.grpcontno) and  payplancode  in  "+ 
                    "	(select payplancode from LMDutyPay where AccPayClass ='3'))  "+
                    " and polyear= "+mTempPolYear+" and a.polmonth = 1 and  b.grpcontno='" +mLCGrpContSchema.getGrpContNo() +
                    "' with ur ");
        }
        else
        {
        	mStartDate = new ExeSQL().getOneValue(
                    "select  max(DueBalaDate)-1 month From lcinsureaccbalance a,lcinsureacc b where  a.contno=b.contno  " +
                    " and a.insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "+  
                    "	(select riskcode from lcgrppol where grpcontno =b.grpcontno) and  payplancode  in  "+ 
                    "	(select payplancode from LMDutyPay where AccPayClass ='3'))  "+
                    " and polyear="+mTempPolYear+" and a.polmonth = 1 and  b.grpcontno='" +mLCGrpContSchema.getGrpContNo() +
                    "' with ur ");
        }

        String baladate = "select max(DueBalaDate)-1 day,max(DueBalaDate) From lcinsureaccbalance  a,lcinsureacc b where  a.contno=b.contno  " +
        				  " and a.insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "+  
        				  "	(select riskcode from lcgrppol where grpcontno =b.grpcontno) and  payplancode  in  "+ 
        				  "	(select payplancode from LMDutyPay where AccPayClass ='3'))  "+
        				  " and PolYear = "+ mTempPolYear + " and polmonth = 12 and b.grpcontno='" +mLCGrpContSchema.getGrpContNo() +
        				  "' with ur ";
        SSRS dateSSRS = mExeSQL.execSQL(baladate);
        mEndDate = dateSSRS.GetText(1, 1);
        String tBalaDate = dateSSRS.GetText(1, 2);//结算日期

        textTag.add("startDate", CommonBL.decodeDate(mStartDate));//报告期间起
        textTag.add("endDate", CommonBL.decodeDate(mEndDate));//报告期间止

        textTag.add("BarCode1", createWorkNo()); //条形码
        textTag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
     
        if(!addNewInfo())
        {
        	return false;
        }

        //添加节点
        if (textTag.size() > 0) {
            xmlexport.addTextTag1(textTag);
        }

        
        xmlexport.outputDocumentToFile("F:\\", "GrpOmnipAnnals");
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    private void setFixedInfo() {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mLCGrpContSchema.getManageCom());
        tLDComDB.getInfo();
        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("letterservicename", tLDComDB.getLetterServiceName());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getLetterServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        textTag.add("Fax", tLDComDB.getFax());
    }

    private boolean dealPrintMag()
    {
        String tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
        mLOPRTManagerSchema.setOtherNoType("00");
        mLOPRTManagerSchema.setCode("OM004");
        mLOPRTManagerSchema.setManageCom(mLCGrpContSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        mLOPRTManagerSchema.setReqCom(mLCGrpContSchema.getManageCom());
        mLOPRTManagerSchema.setReqOperator(mLCGrpContSchema.getOperator());
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
//        mLOPRTManagerSchema.setStandbyFlag2(tLJSGetSchema.getGetNoticeNo()); //这里存放 （也为交费收据号）
//        mLOPRTManagerSchema.setStandbyFlag1(tLJSGetSchema.getOtherNo());
        mResult.addElement(mLOPRTManagerSchema);
        return true;
    }

    /**
     *  获取列表数据
     * @param 无
     * @return ListTable
     */
    private ListTable getListTable() {

        String tempMoney ="";
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[10];
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                info[j - 1] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        tListTable.setName("RISKNAME");
        return tListTable;
    }

    private ListTable getListTable(SSRS tSSRS)
    {
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String[] info = new String[10];
            for (int j = 1; j <= tSSRS.getMaxCol(); j++) {
                    info[j - 1] = tSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        tListTable.setName("LIST");
        return tListTable;
    }

    /**
     * 根据账户保单号码得到账户类型
     * @param insuAccNo String
     * @return String
     */
    private String getInsuAccNo() {
        String sql = "select InsuAccNo from LCInsureAcc a where ContNo ='"+mLCContSchema.getContNo()+"'";
        return (new ExeSQL()).getOneValue(sql);
    }

    private String getPolno ()
    {
        String sql = "select polno from LCInsureAcc a where ContNo ='"+mLCContSchema.getContNo()+"'";
        return (new ExeSQL()).getOneValue(sql);
    }
    
    /**
     * 生成流水号
     * @return String
     */
    private String createWorkNo() {

        String str = "16";
        String allString = "";
        String tDate = PubFun.getCurrentDate();
        tDate = tDate.substring(2, 4) + tDate.substring(5, 7)
                + tDate.substring(8, 10);
        allString = str + tDate;
        String tWorkNo = tDate + PubFun1.CreateMaxNo("GOOD" + allString, 6);
        str += tWorkNo;
        return str;
    }
//  20091118 req00000573 新增内容 zhanggm    
    private boolean addNewInfo()
    {
    	System.out.println("20091118 req00000573 新增内容新增内容开始————————————————————————");
        String tGrpContNo = mLCGrpContSchema.getGrpContNo();
        String sql1 = "select (select riskname from lmrisk where riskcode = a.riskcode),cvalidate, "
        	        + "a.riskcode,prem,payintv,amnt,grppolno from lcgrppol a "
        	        + "where grpcontno = '" + tGrpContNo + "' and exists (select 1 from lmriskapp "
        	        + "where riskcode = a.riskcode and risktype4 = '" + BQ.RISKTYPE1_ULI + "') with ur";
        SSRS tSSRS1 = new SSRS();
        tSSRS1 = mExeSQL.execSQL(sql1);
        textTag.add("RiskCodeName", tSSRS1.GetText(1, 1)); //产品名称
        textTag.add("CValiDate", CommonBL.decodeDate(tSSRS1.GetText(1, 2))); //保单生效日
        
//    	首期缴纳进账户的保险费
    	sql1 = "select sum(money) from ("
    		+ " select nvl(sum(money),0) money from lcinsureacctrace where moneytype = 'BF' and Grpcontno = '" 
    		+ tGrpContNo + "' and othertype = '1'" 
    		+ " union " 
    		+ "select nvl(sum(money),0) money from lbinsureacctrace where moneytype = 'BF' and Grpcontno = '" 
    		+ tGrpContNo + "' and othertype = '1'" 
    		+ " ) with ur";
    	double firstPrem1 = Double.parseDouble(mExeSQL.getOneValue(sql1));
    	
//    	首期缴纳保险费扣费
    	sql1 = "select sum(fee) from ( "
    		  + " select nvl(sum(fee),0) fee from lcinsureaccfeetrace where moneytype = 'GL' and grpcontno = '" 
    		  + tGrpContNo + "' and othertype = '1' " 
    		  + " union "
    		  + " select nvl(sum(fee),0) fee from lbinsureaccfeetrace where moneytype = 'GL' and grpcontno = '" 
    		  + tGrpContNo + "' and othertype = '1' " 
    		  + " ) with ur";
    	double firstFee = Double.parseDouble(mExeSQL.getOneValue(sql1));
        
//    	首期缴纳进账户的保险费
    	double firstPrem = firstPrem1 + firstFee;
    	textTag.add("Prem", PubFun.setPrecision2(firstPrem)); //期初缴纳保险费
    	
        String tRiskCode = tSSRS1.GetText(1, 3);

        
        sql1 = " select sum(money) from ( "
        	+ " select nvl(sum(money),0) money from lcinsureacctrace where moneytype = 'ZF' and grpcontno = '" 
        	+ tGrpContNo + "' and othertype = '1'" 
        	+ " union "
        	+ " select nvl(sum(money),0) money from lbinsureacctrace where moneytype = 'ZF' and grpcontno = '" 
        	+ tGrpContNo + "' and othertype = '1'" 
            + " ) with ur";
        double tbAddPrem = Double.parseDouble(mExeSQL.getOneValue(sql1));//首期追加保险费
 		
        sql1 =" select sum(money) from ( "
        	 + " select nvl(sum(money),0) money from lcinsureacctrace a where moneytype in ('ZF','BF') and grpcontno = '" 
        	 + tGrpContNo + "' and paydate between '" + mStartDate + "' and '"+ mEndDate + "' and othertype = '"
        	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpgrpedoritem where edortype = 'TY' and edorno = a.otherno) " 
        	 + " union "
        	 + " select nvl(sum(money),0) money from lbinsureacctrace a where moneytype in ('ZF','BF') and grpcontno = '" 
        	 + tGrpContNo + "' and paydate between '" + mStartDate + "' and '"+ mEndDate + "' and othertype = '"
        	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpgrpedoritem where edortype = 'TY' and edorno = a.otherno) " 
        	 + " ) with ur";
        double bqAddPrem = Double.parseDouble(mExeSQL.getOneValue(sql1));//保全追加保费
        
        double addPrem = 0.0; 
        if("1".equals(mTempPolYear))
        {
        	addPrem = tbAddPrem + bqAddPrem;
        }
        else
        {
        	addPrem = bqAddPrem;
        }
        textTag.add("AddPrem", PubFun.setPrecision2(addPrem)); //追加保险费：本结算年度的追加保费之和  
        
        double totalPrem = firstPrem + addPrem;
        textTag.add("TotalPrem", PubFun.setPrecision2(totalPrem)); //累计保险费：首期+追加
        double fee1 = 0;//首期费用
        if("1".equals(mTempPolYear))
        {
        	sql1 = "select sum(fee) from ("
        		+ " select sum(fee) fee from lcinsureaccfeetrace where grpcontno = '" + tGrpContNo 
        		+ "' and moneytype in ('GL','KF') and othertype = '1'" 
        		+ " union "
        		+ " select sum(fee) fee from lbinsureaccfeetrace where grpcontno = '" + tGrpContNo 
        		+ "' and moneytype in ('GL','KF') and othertype = '1'" 
        		+ ") with ur";
        	fee1 = Double.parseDouble(mExeSQL.getOneValue(sql1));
        }
//        else
//        {
//        	sql1 = "select nvl(sum(fee),0) From lcinsureaccfeetrace a where grpcontno = '" + tGrpContNo 
//    			+ "' and moneytype = 'GL' and othertype = '2' and exists (select 1 from ljapayperson where contno = "  
//    			+ "a.contno and year(curpaytodate-1 day) = " + mPolYear + " and riskcode = a.riskcode and getnoticeno = a.otherno) with ur";
//        }
        
        sql1 = "select sum(fee) from ("
        	 + " select nvl(sum(fee),0) fee from lcinsureaccfeetrace a where moneytype = 'MF' and grpcontno = '" 
         	 + tGrpContNo + "' and paydate between '" + mStartDate + "' and '"+ mEndDate + "' and othertype = '"
        	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'TY' and edorno = a.otherno)"
        	 + " union "
        	 + " select nvl(sum(fee),0) fee from lbinsureaccfeetrace a where moneytype = 'MF' and grpcontno = '" 
         	 + tGrpContNo + "' and paydate between '" + mStartDate + "' and '"+ mEndDate + "' and othertype = '"
        	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'TY' and edorno = a.otherno)"
        	 + ") with ur";
        double fee2 = Double.parseDouble(mExeSQL.getOneValue(sql1));//保全追加保费扣费
        
        double fee = fee1 + fee2;
        textTag.add("Fee", PubFun.setPrecision2(fee));//初始费用（扣除项）：本结算年度的初始费用之和
        
        
//      生成结算利息列表,报告期内各月年化结算利率及结算信息
        for(int m=1 ;m<=12; m++)
        {
        	String tPolMonth = String.valueOf(m);
        	try
        	{
        		this.setMonthRate(tGrpContNo,tRiskCode,tPolMonth);
        	}
        	catch (Exception ex)
            {
                CError.buildErr(this, "打印终止，获取保单"+tGrpContNo+"第"+mTempPolYear+"年"+tPolMonth+"月年化利率失败。");
                return false;
            }
        	
        }
        
		ExeSQL tExeSQL = new ExeSQL();
        String sql="select Year,quarter,solvency||'%',case when flag='1' then '达到' else '未达到' end  from  ldriskrate where  codetype='CFNL' and state='1' order by Year desc,quarter desc fetch first row only  with ur ";
        tSSRS=tExeSQL.execSQL(sql);
        textTag.add("Year1", tSSRS.GetText(1, 1));
        textTag.add("Quarter1", tSSRS.GetText(1, 2));
        textTag.add("Solvency", tSSRS.GetText(1, 3));
        textTag.add("Flag", tSSRS.GetText(1, 4));
        
        String sql2="select Year,quarter,riskrate  from  ldriskrate where  codetype='FXDJ' and state='1' order by Year desc,quarter desc fetch first row only  with ur ";
        tSSRS=tExeSQL.execSQL(sql2);
        textTag.add("Year2", tSSRS.GetText(1, 1));
        textTag.add("Quarter2", tSSRS.GetText(1, 2));
        textTag.add("RiksRate", tSSRS.GetText(1, 3));
        
//      生成公共账户各月结算信息
        for(int m=1 ;m<=12; m++)
        {
        	String tPolMonth = String.valueOf(m);
        	try
        	{
        		this.setMonthListG(tGrpContNo,tRiskCode,tPolMonth);
        	}
        	catch (Exception ex)
            {
                CError.buildErr(this, "打印终止，获取保单"+tGrpContNo+"第"+mTempPolYear+"年"+tPolMonth+"月结算信息明细失败。");
                return false;
            }
        }
//      生成个人账户各月结算信息
        for(int n=1 ;n<=12; n++)
        {
        	String tPolMonth = String.valueOf(n);
        	try
        	{
        		this.setMonthListP(tGrpContNo,tRiskCode,tPolMonth);
        	}
        	catch (Exception ex)
            {
                CError.buildErr(this, "打印终止，获取保单"+tGrpContNo+"第"+mTempPolYear+"年"+tPolMonth+"月结算信息明细失败。");
                return false;
            }
        }
        System.out.println("20091118 req00000573 新增内容新增内容结束————————————————————————");
    	return true;
    }
    
//  生成结算利息列表,报告期内各月年化结算利率及结算信息
    private void setMonthRate(String aGrpContNo,String aRiskCode,String aPolMonth)
    {
    	String sql = "";
    	sql =  " select max(duebaladate),max(duebaladate) - 1 day From lcinsureaccbalance  a,lcinsureacc b where a.contno=b.contno " +
    		   " and a.insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "+  
               "	(select riskcode from lcgrppol where grpcontno =b.grpcontno) and  payplancode  in  "+ 
               "	(select payplancode from LMDutyPay where AccPayClass ='3'))  "+
    		   " and  polYear = " + mTempPolYear + " and polmonth = " + aPolMonth + " and b.grpcontno ='" + aGrpContNo +
			   "' and length(sequenceno) <> 14 with ur ";
    	SSRS tSSRS = mExeSQL.execSQL(sql);
    	String tDueBalaDate = tSSRS.GetText(1, 1); //应结算日期
    	String tEndDate = tSSRS.GetText(1, 2); //本月结束时间
    	textTag.add("RateMonth"+aPolMonth, decodeDate(tEndDate));
    	
    	sql = "select rate From Interest000001 where riskcode = '" + aRiskCode +"'"
    		+ " and grpcontno=(case when grpcontno='" + aGrpContNo +"' then '" + aGrpContNo +"' else '000000' end) "
    		+ " and enddate = '" + tDueBalaDate + "' with ur";
     	String rate = mExeSQL.getOneValue(sql);
     	textTag.add("Rate"+aPolMonth, String.valueOf(Double.valueOf(rate))); //年化结算利率
    }

	//  生成各月结算信息公共账户
    private void setMonthListG(String aGrpContNo,String aRiskCode,String aPolMonth)
    {
    	textTag.add("GPolMonth"+aPolMonth, SysConst.TEG_START);
    	String sql = "";
    	if("1".equals(mTempPolYear) && "1".equals(aPolMonth))
        {
        	sql = " select min(DueBalaDate) From lcinsureaccbalance a,lcinsureacc b where  a.contno=b.contno and a.polyear = 1 " 
        		+ " and a.polmonth = 1 and b.grpcontno = '" + aGrpContNo + "'" 
        		+ " and a.insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
        		+ "   (select riskcode from lcgrppol where grpcontno =b.grpcontno) and  payplancode in "
        		+ "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
        		+ " and length(sequenceno) <> 14 with ur ";
        }
        else
        {
        	sql = "select max(DueBalaDate)-1 month From lcinsureaccbalance a,lcinsureacc b where  a.contno=b.contno and a.polyear = " 
        		+ mTempPolYear + " and a.polmonth = " + aPolMonth + " and b.grpcontno = '" + aGrpContNo + "'" 
        		+ " and a.insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
        		+ "   (select riskcode from lcgrppol where grpcontno =b.grpcontno) and  payplancode in "
        		+ "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
        		+ " and length(sequenceno) <> 14 with ur ";
        }
        String tStartDate = mExeSQL.getOneValue(sql); //本月开始时间
        
    	sql = "select max(duebaladate),max(duebaladate) - 1 day From lcinsureaccbalance a,lcinsureacc b where  a.contno=b.contno and a.polyear = "
			   + mTempPolYear + " and a.polmonth = " + aPolMonth + " and b.grpcontno ='" + aGrpContNo +"'"
       		   + " and a.insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
       		   + "   (select riskcode from lcgrppol where grpcontno =b.grpcontno) and  payplancode in "
       		   + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
			   + " and length(sequenceno) <> 14 with ur ";
    	SSRS tSSRS = mExeSQL.execSQL(sql);
    	String tDueBalaDate = tSSRS.GetText(1, 1); //应结算日期
    	String tEndDate = tSSRS.GetText(1, 2); //本月结束时间
    	textTag.add("GMonth"+aPolMonth, decodeDate(tEndDate));
     	
//     	本月期初帐户价值
    	
    	if("1".equals(mTempPolYear) && "1".equals(aPolMonth))
        {
    		sql = "select nvl(sum(money),0) "
    	    + "From lcinsureacctrace a where grpcontno = '" 
        	+ mLCGrpContSchema.getGrpContNo() + "' and paydate <= '" + tStartDate + "'" 
    		+ " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
       		+ "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode in "
       		+ "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
        	+ " with ur ";
        }
        else
        {
        	sql = "select nvl(sum(case when paydate = '" + tStartDate + "' and othertype <> '6' then 0 else money end),0) "
    	    + "From lcinsureacctrace a where grpcontno = '" 
        	+ mLCGrpContSchema.getGrpContNo() + "' and paydate <= '" + tStartDate + "' " 
    		+ " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
       		+ "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode in "
       		+ "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
        	+ " with ur ";
        }
    	
        
        textTag.add("GStartMoney"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月期初帐户价值
        
    	sql = "select nvl(sum(-fee),0) from lcinsureaccfeetrace a where moneytype ='MF' and contno = '" 
         	 + aGrpContNo + "' and paydate between '" + tStartDate + "' and '"+ tEndDate + "' and othertype = '"
         	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'TY' and edorno = a.otherno) " 
    		 + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
      		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode in "
      	     + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
          	 + " with ur";
      	textTag.add("GBQAddPremFee"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月追加保费扣费
        
    	sql = "select nvl(sum(money),0) from lcinsureacctrace a where moneytype ='BF' and grpcontno = '" 
       	 + aGrpContNo + "' and paydate between '" + tStartDate + "' and '"+ tEndDate + "' and othertype = '"
       	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'TY' and edorno = a.otherno)" 
 		 + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
   		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode in "
   		 + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
       	 + " with ur";
    	textTag.add("GBQAddPrem"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月追加保费
      	
    	sql = "select nvl(sum(money),0) from lcinsureacctrace a where moneytype='TQ' and grpcontno = '" 
        	 + aGrpContNo + "' and paydate between '" + tStartDate + "' and '"+ tEndDate + "' and othertype = '"
        	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'TQ' and edorno = a.otherno)" 
  		     + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
    		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode in "
    		 + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
        	 + " with ur";
     	textTag.add("GTQAddPrem"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月公共账户离职领取
       	    	
    	sql = "select nvl(sum(case when moneytype = 'LX' and othertype='6' and  paydate='"+tDueBalaDate+"' then money " 
    		 + "	  when moneytype = 'LX' and othertype<>'6' and  paydate='"+tStartDate+"' then money "
    		 + "      when moneytype = 'LX'  and paydate>'"+tStartDate+"' and paydate<'"+tDueBalaDate+"' then money else 0 end),0), "
          	 + " nvl(sum(case when moneytype = 'MF' and othertype='6' and paydate='"+tDueBalaDate+"' then -money  " 
          	 + "	  when moneytype = 'MF' and othertype<>'6' and paydate='"+tStartDate+"' then -money	"
          	 + " 	  when moneytype = 'MF'  and paydate>'"+tStartDate+"' and paydate<'"+tDueBalaDate+"' then -money else 0 end),0) " 
          	 + " from lcinsureacctrace a where grpcontno = '" 
          	 + aGrpContNo + "' and paydate between '" + tStartDate + "' and '"+ tDueBalaDate + "' " 
     		 + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
       		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode in "
       	     + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
         	 + " with ur ";
    	SSRS tSSRS2 = mExeSQL.execSQL(sql);
        textTag.add("GLXMoney"+aPolMonth, PubFun.setPrecision2(tSSRS2.GetText(1, 1))); //保单帐户结算收益
        textTag.add("GMFMoney"+aPolMonth, PubFun.setPrecision2(tSSRS2.GetText(1, 2))); //保单管理费
        
    	sql = "select nvl(sum(-money),0) from lcinsureacctrace a where moneytype='TA' and grpcontno = '" 
          	 + aGrpContNo + "' and paydate between '" + tStartDate + "' and '"+ tEndDate + "' and othertype = '"
          	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'TA' and edorno = a.otherno)" 
    		 + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
      		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode in "
      		 + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
          	 + " with ur";
       	textTag.add("GTAAddPrem"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月公共账户资金分配
       	
    	sql = "select nvl(sum(-money),0) from lcinsureacctrace a where moneytype='TL' and grpcontno = '" 
         	 + aGrpContNo + "' and paydate between '" + tStartDate + "' and '"+ tEndDate + "' and othertype = '"
         	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'TL' and edorno = a.otherno)" 
   		 + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
     		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode in "
     		 + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
         	 + " with ur";
      	textTag.add("GTLAddPrem"+aPolMonth,PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月公共账户部分领取

        
//      本月期末帐户价值
        sql = " select nvl(sum(case when paydate = '" + tDueBalaDate + "' and othertype <> '6' then 0 else money end),0) "
        	+ " From lcinsureacctrace a where grpcontno = '" 
        	+ aGrpContNo + "' and paydate <= '" + tDueBalaDate + "'" 
 		    + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
    		+ "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode in "
    		+ "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
        	+ " with ur ";
        textTag.add("GEndMoney"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月期末帐户价值
        
        textTag.add("GPolMonth"+aPolMonth, SysConst.TEG_END);
    }
	//  生成各月结算信息
    private void setMonthListP(String aGrpContNo,String aRiskCode,String aPolMonth)
    {
    	textTag.add("PolMonth"+aPolMonth, SysConst.TEG_START);
    	String sql = "";
    	if("1".equals(mTempPolYear) && "1".equals(aPolMonth))
        {
        	sql = " select min(DueBalaDate) From lcinsureaccbalance a,lcinsureacc b where  a.contno=b.contno and a.polyear = 1 " 
        		+ " and a.polmonth = 1 and b.grpcontno = '" + aGrpContNo + "'" 
        		+ " and a.insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
        		+ "   (select riskcode from lcgrppol where grpcontno =b.grpcontno) and  payplancode  in "
        		+ "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
        		+ " and length(sequenceno) <> 14 with ur ";
        }
        else
        {
        	sql = "select max(DueBalaDate)-1 month From lcinsureaccbalance a,lcinsureacc b where  a.contno=b.contno and a.polyear = " 
        		+ mTempPolYear + " and a.polmonth = " + aPolMonth + " and b.grpcontno = '" + aGrpContNo + "'" 
        		+ " and a.insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
        		+ "   (select riskcode from lcgrppol where grpcontno =b.grpcontno) and  payplancode in "
        		+ "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
        		+ " and length(sequenceno) <> 14 with ur ";
        }
        String tStartDate = mExeSQL.getOneValue(sql); //本月开始时间
        
    	sql = "select max(duebaladate),max(duebaladate) - 1 day From lcinsureaccbalance a,lcinsureacc b where  a.contno=b.contno and a.polyear = "
			   + mTempPolYear + " and a.polmonth = " + aPolMonth + " and b.grpcontno ='" + aGrpContNo +"'"
       		   + " and a.insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
       		   + "   (select riskcode from lcgrppol where grpcontno =b.grpcontno) and  payplancode  in "
       		   + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
			   + " and length(sequenceno) <> 14 with ur ";
    	SSRS tSSRS = mExeSQL.execSQL(sql);
    	String tDueBalaDate = tSSRS.GetText(1, 1); //应结算日期
    	String tEndDate = tSSRS.GetText(1, 2); //本月结束时间
    	textTag.add("Month"+aPolMonth, decodeDate(tEndDate));

//     	本月期初帐户价值
    	
    	if("1".equals(mTempPolYear) && "1".equals(aPolMonth))
        {
    		sql = "select sum(money) from ("
    		+"select nvl(sum(money),0) money "
    	    + "From lcinsureacctrace a where grpcontno = '" 
        	+ mLCGrpContSchema.getGrpContNo() + "' and paydate <= '" + tStartDate + "'" 
    		+ " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
       		+ "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
       		+ "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
       		+ " union  all "
       		+ " select nvl(sum(money),0) money "
    	    + " From lbinsureacctrace a where grpcontno = '" 
        	+ mLCGrpContSchema.getGrpContNo() + "' and paydate <= '" + tStartDate + "'" 
    		+ " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
       		+ "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
       		+ "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
        	+ " ) with ur ";
        }
        else
        {
        	sql = " select sum(money) from (" 
        	+ " select nvl(sum(case when paydate = '" + tStartDate + "' and othertype <> '6' then 0 else money end),0) money "
    	    + " From lcinsureacctrace a where grpcontno = '" 
        	+ mLCGrpContSchema.getGrpContNo() + "' and paydate <= '" + tStartDate + "' " 
    		+ " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
       		+ "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
       		+ "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
       		+ " union all "
       		+ " select nvl(sum(case when paydate = '" + tStartDate + "' and othertype <> '6' then 0 else money end),0) money "
    	    + " From lbinsureacctrace a where grpcontno = '" 
        	+ mLCGrpContSchema.getGrpContNo() + "' and paydate <= '" + tStartDate + "' " 
    		+ " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
       		+ "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
       		+ "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
        	+ " ) with ur ";
        }
    	
        
        textTag.add("StartMoney"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月期初帐户价值
    	
    	sql = " select sum(fee) from ("
    		 + " select nvl(sum(-fee),0) fee from lcinsureaccfeetrace a where moneytype ='MF' and contno = '" 
          	 + aGrpContNo + "' and paydate between '" + tStartDate + "' and '"+ tEndDate + "' and othertype = '"
          	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'TY' and edorno = a.otherno) " 
     		 + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
       		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
       	     + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
       	     + " union all "
       	     + " select nvl(sum(-fee),0) fee from lbinsureaccfeetrace a where moneytype ='MF' and contno = '" 
          	 + aGrpContNo + "' and paydate between '" + tStartDate + "' and '"+ tEndDate + "' and othertype = '"
          	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'TY' and edorno = a.otherno) " 
     		 + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
       		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
       	     + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
           	 + " ) with ur";
       	textTag.add("BQAddPremFee"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月追加保费扣费
       	
    	sql = " select sum(money) from ("
    	 + " select nvl(sum(money),0) money from lcinsureacctrace a where moneytype ='BF' and grpcontno = '" 
       	 + aGrpContNo + "' and paydate between '" + tStartDate + "' and '"+ tEndDate + "' and othertype = '"
       	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'TY' and edorno = a.otherno)" 
 		 + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
   		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
   		 + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
   		 + " union all"
   		 + " select nvl(sum(money),0) money from lbinsureacctrace a where moneytype ='BF' and grpcontno = '" 
       	 + aGrpContNo + "' and paydate between '" + tStartDate + "' and '"+ tEndDate + "' and othertype = '"
       	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'TY' and edorno = a.otherno)" 
 		 + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
   		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
   		 + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
       	 + " ) with ur";
    	textTag.add("BQAddPrem"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月追加保费
        
    	sql = " select sum(money) from ("
    		 + " select nvl(sum(money),0) money from lcinsureacctrace a where moneytype='TA' and grpcontno = '" 
          	 + aGrpContNo + "' and paydate between '" + tStartDate + "' and '"+ tEndDate + "' and othertype = '"
          	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'TA' and edorno = a.otherno)" 
    		 + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
      		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
      		 + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
      		 + " union all"
      		 + " select nvl(sum(money),0) money from lbinsureacctrace a where moneytype='TA' and grpcontno = '" 
          	 + aGrpContNo + "' and paydate between '" + tStartDate + "' and '"+ tEndDate + "' and othertype = '"
          	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'TA' and edorno = a.otherno)" 
    		 + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
      		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
      		 + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
          	 + " ) with ur";
       	textTag.add("TAAddPrem"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月公共账户资金分配
       		
    	sql = " select sum(LX),sum(MF) from ("
    		 + " select nvl(sum(case when moneytype = 'LX' and othertype='6' and  paydate='"+tDueBalaDate+"' then money " 
    		 + "      when moneytype = 'LX'  and paydate='"+tStartDate+"' and othertype<>'6' then money "
    		 + "      when moneytype = 'LX'  and paydate>'"+tStartDate+"' and paydate<'"+tDueBalaDate+"' then money else 0 end),0) LX, "
          	 + " nvl(sum(case when moneytype = 'MF' and othertype='6' and paydate='"+tDueBalaDate+"' then -money  " 
          	 + " 	  when moneytype = 'MF'  and paydate='"+tStartDate+"' and othertype<>'6' then -money "
          	 + " 	  when moneytype = 'MF'  and paydate>'"+tStartDate+"' and paydate<'"+tDueBalaDate+"' then -money else 0 end),0) MF "
          	 + " from lcinsureacctrace a where grpcontno = '" 
          	 + aGrpContNo + "'  and paydate between '" + tStartDate + "' and '"+ tDueBalaDate + "' " 
     		 + "  and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
       		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
       	     + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
       	     + " union all"
       	     + " select nvl(sum(case when moneytype = 'LX' and othertype='6' and  paydate='"+tDueBalaDate+"' then money " 
       	     + "      when moneytype = 'LX'  and paydate='"+tStartDate+"' and othertype<>'6' then money "
       	     + "      when moneytype = 'LX'  and paydate>'"+tStartDate+"' and paydate<'"+tDueBalaDate+"' then money else 0 end),0) LX, "
       	     + " nvl(sum(case when moneytype = 'MF' and othertype='6' and paydate='"+tDueBalaDate+"' then -money  " 
       	     + " 	  when moneytype = 'MF'  and paydate='"+tStartDate+"' and othertype<>'6' then -money "
       	     + " 	  when moneytype = 'MF'  and paydate>'"+tStartDate+"' and paydate<'"+tDueBalaDate+"' then -money else 0 end),0) MF "
          	 + " from lbinsureacctrace a where grpcontno = '" 
          	 + aGrpContNo + "'  and paydate between '" + tStartDate + "' and '"+ tDueBalaDate + "' " 
     		 + "  and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
       		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
       	     + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
         	 + " ) with ur ";
    	SSRS tSSRS2 = mExeSQL.execSQL(sql);

        textTag.add("LXMoney"+aPolMonth, PubFun.setPrecision2(tSSRS2.GetText(1, 1))); //保单帐户结算收益
        textTag.add("MFMoney"+aPolMonth, PubFun.setPrecision2(tSSRS2.GetText(1, 2))); //保单管理费

    	sql = " select sum(summoney) from ("
    		 + " select nvl(sum(-money),0) summoney from lcinsureacctrace a where moneytype='TL' and grpcontno = '" 
         	 + aGrpContNo + "' and paydate between '" + tStartDate + "' and '"+ tEndDate + "' and othertype = '"
         	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'TL' and edorno = a.otherno)" 
         	 + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
     		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
     		 + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
     		 + " union all "
     		 + " select nvl(sum(-money),0) summoney from lbinsureacctrace a where moneytype='TL' and grpcontno = '" 
         	 + aGrpContNo + "' and paydate between '" + tStartDate + "' and '"+ tEndDate + "' and othertype = '"
         	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'TL' and edorno = a.otherno)" 
         	 + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
     		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
     		 + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
         	 + " ) with ur";
    	SSRS tSSRS3 = mExeSQL.execSQL(sql);
      	textTag.add("TLAddPrem"+aPolMonth, PubFun.setPrecision2(PubFun.setPrecision2(tSSRS3.GetText(1, 1)))); //本月个人账户部分领取

      	
    	sql = "select nvl(sum(-money),0) from lbinsureacctrace a where moneytype not in ('LX','MF') and grpcontno = '" 
        	 + aGrpContNo + "' and paydate between '" + tStartDate + "' and '"+ tEndDate + "' and othertype = '"
        	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'TQ' and edorno = a.otherno)" 
  		     + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
    		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
    		 + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
        	 + " with ur";
     	textTag.add("TQAddPrem"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月个人账户离职领取
     	
    	sql = "select nvl(sum(-money),0) from lbinsureacctrace a where moneytype='TF' and grpcontno = '" 
       	 + aGrpContNo + "' and paydate between '" + tStartDate + "' and '"+ tEndDate + "' and othertype = '"
       	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'SG' and edorno = a.otherno)" 
 		     + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
   		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
   		 + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
       	 + " with ur";
    	textTag.add("SGAddPrem"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月个人账户身故领取
    	
    	sql = " select sum(money) from ("
    		 + " select nvl(sum(-money),0) money from lcinsureacctrace a where moneytype='UM' and grpcontno = '" 
          	 + aGrpContNo + "' and paydate between '" + tStartDate + "' and '"+ tEndDate + "' and othertype = '"
          	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'UM' and edorno = a.otherno)" 
    		     + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
      		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
      		 + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
      		 + " union all"
      		 + " select nvl(sum(-money),0) money from lbinsureacctrace a where moneytype='UM' and grpcontno = '" 
         	 + aGrpContNo + "' and paydate between '" + tStartDate + "' and '"+ tEndDate + "' and othertype = '"
         	 + BQ.NOTICETYPE_G + "' and exists (select 1 from lpGrpedoritem where edortype = 'UM' and edorno = a.otherno)" 
   		     + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
     		 + "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
     		 + "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
          	 + " ) with ur";
       	textTag.add("UMAddPrem"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月个人账户老年护理金领取
        
        
//      本月期末帐户价值
        sql = " select sum(money) from ( "
        	+ " select nvl(sum(case when paydate = '" + tDueBalaDate + "' and othertype <> '6' then 0 else money end),0) money "
        	+ " From lcinsureacctrace a where grpcontno = '" 
        	+ aGrpContNo + "' and paydate <= '" + tDueBalaDate + "'" 
 		    + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
    		+ "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
    		+ "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
    		+ " union all"
    		+ " select nvl(sum(case when paydate = '" + tDueBalaDate + "' and othertype <> '6' then 0 else money end),0) money "
         	+ " From lbinsureacctrace a where grpcontno = '" 
         	+ aGrpContNo + "' and paydate <= '" + tDueBalaDate + "'" 
  		    + " and insuaccno in (select insuaccno from LMRiskAccPay where riskcode in "
     		+ "   (select riskcode from lcgrppol where grpcontno =a.grpcontno) and  payplancode not in "
     		+ "   (select payplancode from LMDutyPay where AccPayClass ='3'))"
        	+ " ) with ur ";
        textTag.add("EndMoney"+aPolMonth, PubFun.setPrecision2(mExeSQL.getOneValue(sql))); //本月期末帐户价值
        
        textTag.add("PolMonth"+aPolMonth, SysConst.TEG_END);
    }
    /**
     * 把型如"2009-01-01"格式的日期转为"2009年01月"格式
     * @param date String
     * @return String
     */
    private String decodeDate(String date)
    {
        if ((date == null) || (date.equals("")))
        {
            System.out.println("日期格式不正确！");
            return "";
        }
        String data[] = date.split("-");
        if (data.length != 3)
        {
            System.out.println("日期格式不正确！");
            return "";
        }
        return (data[0] + "年" + data[1] + "月");
    }
}
