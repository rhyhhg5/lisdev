package com.sinosoft.lis.f1print;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ReadyFeeFinDetailPrintBL {
	

    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;

    private String AccountCode = "";
    private String DepCodea = "";
    private String DepCodeb = "";
    private String StartDate = "";
    private String EndDate = "";
    
    private String mCurDate = PubFun.getCurrentDate();

    SSRS tSSRS = new SSRS();
    	
	private String[][] mToExcel = null;
	
	private String mMail = "";
       

    public ReadyFeeFinDetailPrintBL()
    {
    }

    public boolean submitData(VData cInputData, String operate) 
    {

        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        
		if(!createFile()){
			return false;
		}        

        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    public boolean dealData()
    {
        System.out.println("BL->dealDate()");

            ExeSQL tExeSQL = new ExeSQL();
            if(AccountCode!="2205040000"){
            mSql =  "select '',a.Accountcode as 科目,b.managecom as 机构代码,a.summoney as 发生额,a.finitemtype as 借贷标识,b.riskcode as 险种代码,a.contno as 保单号,c.SignDate as 签单日,c.CValiDate as 保单生效日,c.CInValiDate as 保单终止日,(select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate  fetch first 1 rows only) as 凭证号,a.AccountDate as 凭证日期  from LIDataTransResult a,liaboriginaldata b,LCCont c where a.STANDBYSTRING3=b.serialno and a.AccountDAte between '"+StartDate+"' and '"+EndDate+"' and a.Accountcode='"+AccountCode+"' and a.managecom between '"+DepCodea+"' and '"+DepCodeb+"' and a.listflag ='1' and (a.contno=c.contno or a.contno=c.prtno)  and exists(select 1 from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate) "
                  +"union all "+"select '',a.Accountcode as 科目,b.managecom as 机构代码,a.summoney as 发生额,a.finitemtype as 借贷标识,b.riskcode as 险种代码,a.contno as 保单号,c.SignDate as 签单日,c.CValiDate as 保单生效日,c.CInValiDate as 保单终止日,(select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate  fetch first 1 rows only) as 凭证号,a.AccountDate as 凭证日期 from LIDataTransResult a,liaboriginaldata b,LBCont c where a.STANDBYSTRING3=b.serialno and a.AccountDAte between '"+StartDate+"' and '"+EndDate+"' and a.Accountcode='"+AccountCode+"' and a.managecom between '"+DepCodea+"' and '"+DepCodeb+"' and a.listflag ='1' and (a.contno=c.contno or a.contno=c.prtno) and exists(select 1 from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate)  "
                  +"union all "+"select '',a.Accountcode as 科目,b.managecom as 机构代码,a.summoney as 发生额,a.finitemtype as 借贷标识,b.riskcode as 险种代码,a.contno as 保单号,c.SignDate as 签单日,c.CValiDate as 保单生效日,c.CInValiDate as 保单终止日,(select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate  fetch first 1 rows only) as 凭证号,a.AccountDate as 凭证日期 from LIDataTransResult a,liaboriginaldata b,LBGrpCont c where a.STANDBYSTRING3=b.serialno and a.AccountDAte between '"+StartDate+"' and '"+EndDate+"' and a.Accountcode='"+AccountCode+"' and a.managecom between '"+DepCodea+"' and '"+DepCodeb+"' and a.listflag ='2' and (a.contno=c.grpcontno or a.contno=c.prtno)  and exists(select 1 from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate) "
                  +"union all "+"select '',a.Accountcode as 科目,b.managecom as 机构代码,a.summoney as 发生额,a.finitemtype as 借贷标识,b.riskcode as 险种代码,a.contno as 保单号,c.SignDate as 签单日,c.CValiDate as 保单生效日,c.CInValiDate as 保单终止日,(select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate  fetch first 1 rows only) as 凭证号,a.AccountDate as 凭证日期 from LIDataTransResult a,liaboriginaldata b,LCGrpCont c where a.STANDBYSTRING3=b.serialno and a.AccountDAte between '"+StartDate+"' and '"+EndDate+"' and a.Accountcode='"+AccountCode+"' and a.managecom between '"+DepCodea+"' and '"+DepCodeb+"' and a.listflag ='2' and (a.contno=c.grpcontno or a.contno=c.prtno) and exists(select 1 from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate) "
                  +"union all "+"select '',a.Accountcode as 科目,b.managecom as 机构代码,a.summoney as 发生额,a.finitemtype as 借贷标识,b.riskcode as 险种代码,a.contno as 保单号,'1990-1-1' as 签单日,'1990-1-1' as 保单生效日,'1990-1-1' as 保单终止日,(select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate  fetch first 1 rows only) as 凭证号,a.AccountDate as 凭证日期 from LIDataTransResult a,liaboriginaldata b  where a.STANDBYSTRING3=b.serialno and a.AccountDAte between '"+StartDate+"' and '"+EndDate+"' and a.Accountcode='"+AccountCode+"' and a.managecom between '"+DepCodea+"' and '"+DepCodeb+"' and not exists(select 1 from Lccont where (contno=a.contno or prtno=a.contno)) and not exists(select 1 from Lbcont where (contno=a.contno or prtno=a.contno)) and not exists(select 1 from Lbgrpcont where (grpcontno=a.contno or prtno=a.contno)) and not exists(select 1 from Lcgrpcont where (grpcontno=a.contno or prtno=a.contno)) and exists(select 1 from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate)";
          }else {
        	  
        	  mSql =  "select '',a.Accountcode as 科目,b.managecom as 机构代码,a.summoney as 发生额,a.finitemtype as 借贷标识,b.riskcode as 险种代码,a.contno as 保单号,c.SignDate as 签单日,c.CValiDate as 保单生效日,c.CInValiDate as 保单终止日,(select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate  fetch first 1 rows only) as 凭证号,a.AccountDate as 凭证日期  from LIDataTransResult a,liaboriginaldata b,LCCont c where a.STANDBYSTRING3=b.serialno and a.AccountDAte between '"+StartDate+"' and '"+EndDate+"' and a.Accountcode='"+AccountCode+"' and a.managecom between '"+DepCodea+"' and '"+DepCodeb+"'  and (a.contno=c.contno or a.contno=c.prtno)  and exists(select 1 from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate) "
              +"union all "+"select '',a.Accountcode as 科目,b.managecom as 机构代码,a.summoney as 发生额,a.finitemtype as 借贷标识,b.riskcode as 险种代码,a.contno as 保单号,c.SignDate as 签单日,c.CValiDate as 保单生效日,c.CInValiDate as 保单终止日,(select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate  fetch first 1 rows only) as 凭证号,a.AccountDate as 凭证日期 from LIDataTransResult a,liaboriginaldata b,LBCont c where a.STANDBYSTRING3=b.serialno and a.AccountDAte between '"+StartDate+"' and '"+EndDate+"' and a.Accountcode='"+AccountCode+"' and a.managecom between '"+DepCodea+"' and '"+DepCodeb+"'  and (a.contno=c.contno or a.contno=c.prtno) and exists(select 1 from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate)  "
              +"union all "+"select '',a.Accountcode as 科目,b.managecom as 机构代码,a.summoney as 发生额,a.finitemtype as 借贷标识,b.riskcode as 险种代码,a.contno as 保单号,c.SignDate as 签单日,c.CValiDate as 保单生效日,c.CInValiDate as 保单终止日,(select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate  fetch first 1 rows only) as 凭证号,a.AccountDate as 凭证日期 from LIDataTransResult a,liaboriginaldata b,LBGrpCont c where a.STANDBYSTRING3=b.serialno and a.AccountDAte between '"+StartDate+"' and '"+EndDate+"' and a.Accountcode='"+AccountCode+"' and a.managecom between '"+DepCodea+"' and '"+DepCodeb+"'  and (a.contno=c.grpcontno or a.contno=c.prtno)  and exists(select 1 from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate) "
              +"union all "+"select '',a.Accountcode as 科目,b.managecom as 机构代码,a.summoney as 发生额,a.finitemtype as 借贷标识,b.riskcode as 险种代码,a.contno as 保单号,c.SignDate as 签单日,c.CValiDate as 保单生效日,c.CInValiDate as 保单终止日,(select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate  fetch first 1 rows only) as 凭证号,a.AccountDate as 凭证日期 from LIDataTransResult a,liaboriginaldata b,LCGrpCont c where a.STANDBYSTRING3=b.serialno and a.AccountDAte between '"+StartDate+"' and '"+EndDate+"' and a.Accountcode='"+AccountCode+"' and a.managecom between '"+DepCodea+"' and '"+DepCodeb+"'  and (a.contno=c.grpcontno or a.contno=c.prtno) and exists(select 1 from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate) "
              +"union all "+"select '',a.Accountcode as 科目,b.managecom as 机构代码,a.summoney as 发生额,a.finitemtype as 借贷标识,b.riskcode as 险种代码,a.contno as 保单号,'1990-1-1' as 签单日,'1990-1-1' as 保单生效日,'1990-1-1' as 保单终止日,(select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate  fetch first 1 rows only) as 凭证号,a.AccountDate as 凭证日期 from LIDataTransResult a,liaboriginaldata b  where a.STANDBYSTRING3=b.serialno and a.AccountDAte between '"+StartDate+"' and '"+EndDate+"' and a.Accountcode='"+AccountCode+"' and a.managecom between '"+DepCodea+"' and '"+DepCodeb+"' and not exists(select 1 from Lccont where (contno=a.contno or prtno=a.contno)) and not exists(select 1 from Lbcont where (contno=a.contno or prtno=a.contno)) and not exists(select 1 from Lbgrpcont where (grpcontno=a.contno or prtno=a.contno)) and not exists(select 1 from Lcgrpcont where (grpcontno=a.contno or prtno=a.contno)) and exists(select 1 from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate)";
    
          }
            System.out.println(mSql);
            tSSRS = tExeSQL.execSQL(mSql);
            
            if(tSSRS.getMaxRow()<=0)
            {
            	buiError("dealData","没有符合条件的信息!");
                return false;
            }
 
        return true;
    }
    
	private boolean createFile(){
		
//		 1、查看数据行数
	  	  int dataSize = this.tSSRS.getMaxRow();
	  	  if(this.tSSRS==null||this.tSSRS.getMaxRow()<1)
	  	  {
	  		  buiError("dealData","没有符合条件的信息!");
	  		  return false;
	  	  }
	        // 定义一个二维数组 
	  	   String[][] mToExcel = new String[dataSize + 6][30];
//	       mToExcel = new String[10000][30];
	        mToExcel[0][4] = "预收&应收保费科目明细";
//	        mToExcel[1][5] = EndDate.substring(0, EndDate.indexOf('-'))+ "年"+ EndDate.substring(EndDate.indexOf('-') + 1, EndDate.lastIndexOf('-')) + "月份，实际结余返还金额统计表";
	        mToExcel[1][6] = AccountCode+"科目的业务清单";
	        mToExcel[2][8] = "编制单位：" + getName();

	        mToExcel[3][0] = "序号";
	        mToExcel[3][1] = "科目信息";
	        mToExcel[3][2] = "机构代码";
	        mToExcel[3][3] = "发生额";
	        mToExcel[3][4] = "借贷标识";
	        mToExcel[3][5] = "险种代码";
	        mToExcel[3][6] = "保单号码";
	        mToExcel[3][7] = "签单日";
	        mToExcel[3][8] = "保单生效日";
	        mToExcel[3][9] = "保单终止日";
	        mToExcel[3][10] = "凭证号";
	        mToExcel[3][11] = "凭证日期";
	        
	        int printNum = 3;
	        
	           int no = 1;
	            for (int row = 1; row <= tSSRS.getMaxRow(); row++)
	            {
	            	
	                for (int col = 1; col <= tSSRS.getMaxCol(); col++)
	                {       
	                        if (tSSRS.GetText(row, col).equals("null"))
	                        {
	                            mToExcel[row + printNum][col - 1] = "";
	                        }
	                        else
	                        {
	                            mToExcel[row + printNum][col - 1] = tSSRS.GetText(row, col);
	                        }
	                        }
	              
	                mToExcel[row + printNum][0] = no + "";
	                no++;}           

	        mToExcel[printNum + tSSRS.getMaxRow() + 2][0] = "制表";
	        mToExcel[printNum + tSSRS.getMaxRow() + 2][14] = "日期：" + mCurDate;
	            
		
		try {
			System.out.println(mOutXmlPath);
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate() };
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(mOutXmlPath);
			System.out.println("生成文件完成");
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}
		
		return true;
	}

	public String[][] getMToExcel() {
		return mToExcel;
	}
	
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     * @throws java.text.ParseException 
     */
    public boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
        System.out.println(mGI.ManageCom);

        if (mGI == null || tf == null)
        {
        	buiError("getInputData", "传入信息不完整！");
            return false;
        }
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        AccountCode = (String) tf.getValueByName("AccountCode");     
        DepCodea = (String) tf.getValueByName("DepCodea");
        DepCodeb = (String) tf.getValueByName("DepCodeb");
        StartDate = (String) tf.getValueByName("StartDate");
        EndDate = (String) tf.getValueByName("EndDate");
        mMail = (String) tf.getValueByName("toMail");
        
        
        System.out.println("mMail-----"+mMail);

        if (AccountCode == null || AccountCode == null)
        {
            buiError("getInputData", "传入信息不完整！");
            return false;
        }
        return true;
    }
    
    
    private String getName()
    {
        String tSQL = "";
        String tRtValue = "";
        tSQL = "select name from ldcom where comcode='" + mGI.ManageCom + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tSSRS.getMaxRow() == 0)
        {
            tRtValue = "";
        }
        else
            tRtValue = tSSRS.GetText(1, 1);
        return tRtValue;
    }
    private void buiError(String functionName,String errorMessage){
    	CError tError = new CError();
        tError.moduleName = "ReadyFeeFinDetailPrintBL";
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        mErrors.addOneError(tError);
    }
    public static void main(String[] args)
    {
//        GlobalInput tG = new GlobalInput();
//        tG.ManageCom = "86";
//        tG.Operator = "cwad";
//        tG.ComCode = "86";
//        TransferData tTransferData = new TransferData();
//        tTransferData.setNameAndValue("OutXmlPath", "D:\\test\\a.xls");
////        tTransferData.setNameAndValue("EndDate", "2012-05-01");
////        tTransferData.setNameAndValue("StartDate", "");
//        VData vData = new VData();
//        vData.add(tG);
//        vData.add(tTransferData);
//        ReadyFeeFinDetailPrintUI tReadyFeeFinDetailPrintUI = new ReadyFeeFinDetailPrintUI();
//        if (!tReadyFeeFinDetailPrintUI.submitData(vData, ""))
//        {
//            System.out.print("失败！");
//        }
    }
}
