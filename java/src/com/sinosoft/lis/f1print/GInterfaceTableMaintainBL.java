package com.sinosoft.lis.f1print;


//import com.sinosoft.pubfun.GlobalInput;
//import com.sinosoft.pubfun.MMap;
//import com.sinosoft.pubfun.PubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GInterfaceTableMaintainBL {
	 /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String Serialno ="21826194" ;
    private String Batchno = "";
	private String Depcode = "";
	private String Chargedate = "";
	private String Vouchertype = "";
	private String Readstate = "";
	private String Agentno = "";
	private String mOperate = "";
    
    private String mCurDate = PubFun.getCurrentDate();
    	
	private String[][] mToExcel = null;
	
	private String mMail = "";
       

    public GInterfaceTableMaintainBL()
    {
    }

    public boolean submitData(VData cInputData, String mOperate) 
    {
    	this.mOperate = mOperate;
    	
    	//获取数据
        if (!getInputData(cInputData))
        {
            return false;
        }
      //检查数据
        if (!checkData())
        {
        	return false;
        }
      //业务处理
        if (!dealData())
        {	
            return false;
        }
        
		if(!createFile()){
			return false;
		}        

        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    public boolean dealData()
    {
       
    	//提交后台处理
        MMap tmap = new MMap();
        VData tInputData = new VData();
        
        
        if(mOperate.equals("UPDATE")){
        	if (Agentno != null && !Agentno.equals("")) {
            	tmap.put("update interfacetable set Agentno = '"+Agentno+"' where Serialno='21826194'", "UPDATE");
            } 
        	
        }else if(mOperate.equals("DELETE")){
        	
        	tmap.put("delete from interfacetable where Agentno = '"+Agentno+"'", "DELETE");
        	
        }else if(mOperate.equals("INSERT")){
        	

    String s2= "INSERT INTO interfacetable VALUES ('21826181','00000000000000008398','S-01','C','X9','CNY','3400',NULL,'',NULL,NULL,NULL,NULL,'0303',NULL,NULL,{d '2017-05-04' },'',700.00,'1000002754','3','2017',{d '2017-06-03' },'10:00:14',{d '2017-06-02' },'18:52:40',{d '2017-05-15' },'09:52:32',{d '2017-05-15' },'09:52:32','cwjk',0,0,'2000005992',NULL,NULL,'')";
        	

        	tmap.put(s2, "INSERT");

        }
        
        tInputData.add(tmap);
        System.out.println(tmap+"******************");       
        PubSubmit tPubSubmit = new PubSubmit();     
        if (!tPubSubmit.submitData(tInputData, ""))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buiError( "gatherData", "数据出错，提示信息为：" + tPubSubmit.mErrors.getFirstError());         
            tmap=null;
            tInputData=null;
            return false;
        }
        
    	tmap=null;
        tInputData=null;
        return true;
  }
    

    
	private boolean createFile(){
		return true;
	}

	public String[][] getMToExcel() {
		return mToExcel;
	}
	
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     * @throws java.text.ParseException 
     */
    public boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
        System.out.println(mGI+"...............");

        if (mGI == null || tf == null)
        {
        	buiError("getInputData","数据维护出错");
            return false;
        }
        Batchno = (String) tf.getValueByName("Batchno");
		Depcode = (String) tf.getValueByName("Depcode");
		Chargedate = (String) tf.getValueByName("Chargedate");
		Vouchertype = (String) tf.getValueByName("Vouchertype");
		Readstate = (String) tf.getValueByName("Readstate");
		Agentno = (String) tf.getValueByName("Agentno");
		mOperate = (String) tf.getValueByName("mOperate");

		System.out.println("Batchno-----" + Batchno);
		System.out.println("Depcode-----" + Depcode);
		System.out.println("Chargedate-----" + Chargedate);
		System.out.println("Vouchertype-----" + Vouchertype);
		System.out.println("Readstate-----" + Readstate);
		System.out.println("Agentno-----" + Agentno);
		System.out.println("mOperate-----" + mOperate);


        return true;
    }
    
    private void buiError(String functionName,String errorMessage){
    	CError tError = new CError();
        tError.moduleName = "GInterfaceTableMaintainBL";
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        mErrors.addOneError(tError);
    }
    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "cwad";
        tG.ComCode = "86";
        TransferData tTransferData = new TransferData();
        VData vData = new VData();
        vData.add(tG);
        vData.add(tTransferData);
        PreRecPrintUI tPreRecPrintUI = new PreRecPrintUI();
        if (!tPreRecPrintUI.submitData(vData, ""))
        {
            System.out.print("失败！");
        }
    }
}


