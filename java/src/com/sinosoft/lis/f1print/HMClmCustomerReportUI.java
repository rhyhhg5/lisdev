/*
 * <p>ClassName: LLClaimerESReportUI </p>
 * <p>Description: LLClaimerESReportUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database:
 * @CreateDate：2010-01-11
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class HMClmCustomerReportUI
{
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private VData mResult = new VData();

    public boolean submitData(VData cInputData,String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData=(VData)cInputData.clone();
        System.out.println("String HMClmCustomerReportBL...");
        HMClmCustomerReportBL tHMClmCustomerReportBL= new HMClmCustomerReportBL();
        if (!tHMClmCustomerReportBL.submitData(mInputData, mOperate))
        {
            if (tHMClmCustomerReportBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tHMClmCustomerReportBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "HMClmCustomerReportUI发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tHMClmCustomerReportBL.getResult();
            return true;
        }
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "HMClmCustomerReportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 取得返回处理过的结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }
}
