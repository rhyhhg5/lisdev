package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author 刘岩松
 * @version 1.0
 * @date 2003-02-17
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PLPsqsUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String tRgtNo;

    public PLPsqsUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("PRINT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            // 进行业务处理
            if (!dealData())
            {
                return false;
            }

            // 准备传往后台的数据
            VData vData = new VData();

            if (!prepareOutputData(vData))
            {
                return false;
            }

            PLPsqsBL tPLPsqsBL = new PLPsqsBL();
            System.out.println("Start PLPsqs UI Submit ...");

            if (!tPLPsqsBL.submitData(vData, cOperate))
            {
                if (tPLPsqsBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tPLPsqsBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData",
                               "FinChargeDayModeF1PBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tPLPsqsBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "PLPsqsUI";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        tLLRegisterSchema.setRgtNo(tRgtNo);
        try
        {
            vData.clear();

            vData.add(tLLRegisterSchema);
            vData.add(mGlobalInput);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
        mLLRegisterSchema = (LLRegisterSchema) cInputData.getObjectByObjectName(
                "LLRegisterSchema", 0);

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        tRgtNo = mLLRegisterSchema.getRgtNo();

        if (tRgtNo == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "FinChargeDayModeF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        LLRegisterSchema yLLRegisterSchema = new LLRegisterSchema();
        yLLRegisterSchema.setRgtNo("");
        //定义一个全局变量存储赔案号
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";

        VData tVData = new VData();
        VData tResult = new VData();

        PLPsqsUI yPLPsqsUI = new PLPsqsUI(); //调用批单打印的类
        try
        {
            tVData.addElement(tG);
            tVData.addElement(yLLRegisterSchema);
            yPLPsqsUI.submitData(tVData, "PRINT");
        }
        catch (Exception ex)
        {
            System.out.println("Fail");
        }
        tResult = yPLPsqsUI.getResult();
        XmlExport txmlExport = new XmlExport();
        txmlExport = (XmlExport) tResult.getObjectByObjectName("XmlExport", 0);
        if (txmlExport == null)
        {
            System.out.println("null");
        }
    }
}