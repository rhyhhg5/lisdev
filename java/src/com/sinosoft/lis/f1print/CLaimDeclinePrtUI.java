package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LOBatchPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class CLaimDeclinePrtUI implements PrintService
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOBatchPRTManagerSchema mLOBatchPRTManagerSchema = new LOBatchPRTManagerSchema();


    public CLaimDeclinePrtUI()
    {
    }

    /**
         传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        // 进行业务处理
        if (!dealData())
        {
            return false;
        }

        // 准备传往后台的数据
        VData vData = new VData();

        if (!prepareOutputData(vData))
        {
            return false;
        }

        CLaimDeclinePrtBL tCLaimDeclinePrtBL = new CLaimDeclinePrtBL();
        System.out.println("Start CLaimDecideUI Submit ...");

        if (!tCLaimDeclinePrtBL.submitData(vData, cOperate))
        {
            if (tCLaimDeclinePrtBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tCLaimDeclinePrtBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "CLaimDecideBL发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tCLaimDeclinePrtBL.getResult();
            return true;
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.add(mGlobalInput);
            vData.add(mLOBatchPRTManagerSchema);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        System.out.println("claimdeclineprint begin....");
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLOBatchPRTManagerSchema.setSchema((LOBatchPRTManagerSchema) cInputData.getObjectByObjectName(
                "LOBatchPRTManagerSchema", 0));

        if (mLOBatchPRTManagerSchema == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "DerferAppF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    public static void main(String[] args)
    {
        CLaimDeclinePrtUI tCLaimDeclinePrtUI = new CLaimDeclinePrtUI();

        LOBatchPRTManagerSchema tLOBatchPRTManagerSchema = new LOBatchPRTManagerSchema();
        tLOBatchPRTManagerSchema.setOtherNo("550000000000123");

        VData tVData = new VData();
        tVData.addElement(tLOBatchPRTManagerSchema);
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86110000";
        tG.Operator = "001";
        tVData.addElement(tG);
        tCLaimDeclinePrtUI.submitData(tVData, "PRINT");
    }

    public CErrors getErrors() {
        return null;
    }
}
