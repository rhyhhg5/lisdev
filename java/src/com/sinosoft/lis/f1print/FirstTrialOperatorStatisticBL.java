package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author MengWt
 * @version 1.0
 */
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;

public class FirstTrialOperatorStatisticBL implements PrintService {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();

    public FirstTrialOperatorStatisticBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));

        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        if (mTransferData == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "WorkEfficiencyBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        VData result = new VData();
        //统计的参数
        String OperatorManagecom = (String) mTransferData.getValueByName(
                "OperatorManagecom");

        String Managecom = (String) mTransferData.getValueByName("Managecom");

        String StartDate = (String) mTransferData.getValueByName("StartDate");

        String EndDate = (String) mTransferData.getValueByName("EndDate");

        String QYType = (String) mTransferData.getValueByName("QYType");

        String QYType_ch = (String) mTransferData.getValueByName("QYType_ch");

        String QYType1 = (String) mTransferData.getValueByName("QYType1");

        String QYType2 = (String) mTransferData.getValueByName("QYType2");

        String QYType3 = (String) mTransferData.getValueByName("QYType3");

        String FirstTrialOperator1 = (String) mTransferData.getValueByName(
                "FirstTrialOperator1");
            String FirstTrialOperator2 = (String) mTransferData.getValueByName(
                "FirstTrialOperator2");

        String MakeDate = PubFun.getCurrentDate();
        StringBuffer sql = new StringBuffer();
        /** 数据库查询 */
        ExeSQL tExeSQL = new ExeSQL();
        /** 返回结果集 */
        SSRS ssrs = null;
        if (QYType.equals("1")) {
            if (Managecom.length() == 2) {
                SSRS tSSRS_M;
                String sql_M = "select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode";
                System.out.println("zhuzhu" + sql_M);
                tExeSQL = new ExeSQL();
                tSSRS_M = tExeSQL.execSQL(sql_M);
                String temp_M[][] = tSSRS_M.getAllData();
                String Mng;
                if (tSSRS_M.getMaxRow() > 0) {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++) {
                        Mng = temp_M[i - 1][0];
                        String Mng1;
                        if (Mng.equals("86000000")) {
                            Mng1 = Mng;
                        } else {
                            Mng1 = Mng.substring(0, 4);
                        }
                        sql = new StringBuffer();
                        sql.append("  select '"+ Mng +"',U,'个险投保书',sum(V),sum(W),sum(M),decimal(decimal(sum(M)+sum(M1))/decimal(sum(V))*100,12,2)");
                        sql.append(" from ");
                        sql.append(" ( ");
                        sql.append("   select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1 ");
                        sql.append("   from ");
                        sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                        sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lccont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                        sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1' and  substr(managecom,1,length('"+Managecom+"')*4)='"+ Mng +"'  "+FirstTrialOperator2+" and conttype='1') E from LCIssuePol a,lccont b where b.conttype='1' ");
                        sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                        sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"'  "+FirstTrialOperator1+"  group by ");
                        sql.append("   b.FirstTrialOperator ");
                        sql.append("     union ");
                        sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                        sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                        sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1' and  substr(managecom,1,length('"+Managecom+"')*4)='"+ Mng +"'  "+FirstTrialOperator2+" and conttype='1') E   from LCIssuePol a,lbcont b where b.conttype='1' ");
                        sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                        sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"'  "+FirstTrialOperator1+"  group by ");
                        sql.append("   b.FirstTrialOperator ");
                        sql.append("       union ");
                        sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                        sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                        sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1' and  substr(managecom,1,length('"+Managecom+"')*4)='"+ Mng +"'  "+FirstTrialOperator2+" and conttype='1') E    from LobIssuePol a,lobcont b where b.conttype='1' ");
                        sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                        sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+"  group by ");
                        sql.append("   b.FirstTrialOperator ");
                        sql.append("   ) as X group by A,C ");
                        sql.append("   union ");
                        sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
                        sql.append("   count(distinct b.prtno) B    from lccont b where b.conttype='1' and b.cardflag<>'1' ");
                        sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
                        sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" ");
                        sql.append("   group by b.FirstTrialOperator ");
                        sql.append("     union ");
                        sql.append("   select b.FirstTrialOperator A, ");
                        sql.append("   count(distinct b.prtno) B    from lbcont b where b.conttype='1' and b.cardflag<>'1' ");
                        sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
                        sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" ");
                        sql.append("   group by b.FirstTrialOperator ");
                        sql.append("       union ");
                        sql.append("   select b.FirstTrialOperator A, ");
                        sql.append("   count(distinct b.prtno) B    from lobcont b where b.conttype='1' and b.cardflag<>'1' ");
                        sql.append("  and  b.proposalcontno not in (select proposalcontno from LobIssuePol) and b.makedate ");
                        sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" ");
                        sql.append("   group by b.FirstTrialOperator ");
                        sql.append("   ) as Y group by A ");
                        sql.append("   ) as z group by U ");

                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null) {
                            if (ssrs.getMaxRow() > 0) {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");
                    }
                    sql = new StringBuffer();
                    sql.append("  select '总计',U,'个险投保书',sum(V),sum(W),sum(M),decimal(decimal(sum(M)+sum(M1))/decimal(sum(V))*100,12,2) ");
                   sql.append(" from ");
                   sql.append(" ( ");
                   sql.append("   select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1 ");
                   sql.append("   from ");
                   sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                   sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lccont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append(" and a.cardflag<>'1' and b.code='85'  and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1') E from LCIssuePol a,lccont b where b.conttype='1' ");
                   sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' "+FirstTrialOperator1+" ");
                   sql.append("   and b.cardflag<>'1'    group by ");
                   sql.append("   b.FirstTrialOperator ");
                   sql.append("     union ");
                   sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                   sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append(" and a.cardflag<>'1' and b.code='85'  and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1') E   from LCIssuePol a,lbcont b where b.conttype='1' ");
                   sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' "+FirstTrialOperator1+" ");
                   sql.append("   and b.cardflag<>'1'     group by ");
                   sql.append("   b.FirstTrialOperator ");
                   sql.append("       union ");
                   sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                   sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append(" and a.cardflag<>'1' and b.code='85'  and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1') E    from LobIssuePol a,lobcont b where b.conttype='1' ");
                   sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' "+FirstTrialOperator1+" ");
                   sql.append("   and b.cardflag<>'1'     group by ");
                   sql.append("   b.FirstTrialOperator ");
                   sql.append("   ) as X group by A,C ");
                   sql.append("   union ");
                   sql.append("   select A U,sum(B) V,0 W ,0 M ,0 M1 from (   select b.FirstTrialOperator A, ");
                   sql.append("   count(distinct b.prtno) B    from lccont b where b.conttype='1' and b.cardflag<>'1' ");
                   sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
                   sql.append("  between '"+StartDate+"' and '"+EndDate+"'  "+FirstTrialOperator1+"   ");
                   sql.append("   group by b.FirstTrialOperator ");
                   sql.append("     union ");
                   sql.append("   select b.FirstTrialOperator A, ");
                   sql.append("   count(distinct b.prtno) B    from lbcont b where b.conttype='1' and b.cardflag<>'1' ");
                   sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
                   sql.append("  between '"+StartDate+"' and '"+EndDate+"'  "+FirstTrialOperator1+"   ");
                   sql.append("   group by b.FirstTrialOperator ");
                   sql.append("       union ");
                   sql.append("   select b.FirstTrialOperator A, ");
                   sql.append("   count(distinct b.prtno) B    from lobcont b where b.conttype='1' and b.cardflag<>'1' ");
                   sql.append("  and  b.proposalcontno not in (select proposalcontno from LobIssuePol) and b.makedate ");
                   sql.append("  between '"+StartDate+"' and '"+EndDate+"'  "+FirstTrialOperator1+"  ");
                   sql.append("   group by b.FirstTrialOperator ");
                   sql.append("   ) as Y group by A ");
                   sql.append("   ) as z group by U ");

                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null) {
                        if (ssrs.getMaxRow() > 0) {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    System.out.println("处理完成第二个Sql语句~~~~~");
                }

            } else if (Managecom.length() == 4) {
                SSRS tSSRS_M;
                String Mng = tExeSQL.getOneValue("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='" +
                                                 Managecom +
                                                 "' order by ComCode");
                sql = new StringBuffer();
                sql.append("  select '"+ Mng +"',U,'个险投保书',sum(V),sum(W),sum(M),decimal(decimal(sum(M)+sum(M1))/decimal(sum(V))*100,12,2)");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append("   select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
                sql.append("   from ");
                sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lccont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator  "+FirstTrialOperator1+"  ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"') E from LCIssuePol a,lccont b where b.conttype='1' ");
                sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'   "+FirstTrialOperator1+"   group by ");
                sql.append("   b.FirstTrialOperator ");
                sql.append("     union ");
                sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator  "+FirstTrialOperator1+"  ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"') E   from LCIssuePol a,lbcont b where b.conttype='1' ");
                sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'   "+FirstTrialOperator1+"   group by ");
                sql.append("   b.FirstTrialOperator ");
                sql.append("       union ");
                sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator  "+FirstTrialOperator1+"  ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"') E    from LobIssuePol a,lobcont b where b.conttype='1' ");
                sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'   "+FirstTrialOperator1+"   group by ");
                sql.append("   b.FirstTrialOperator ");
                sql.append("   ) as X group by A,C ");
                sql.append("   union ");
                sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
                sql.append("   count(distinct b.prtno) B    from lccont b where b.conttype='1' and b.cardflag<>'1' ");
                sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
                sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'   "+FirstTrialOperator1+"  ");
                sql.append("   group by b.FirstTrialOperator ");
                sql.append("     union ");
                sql.append("   select b.FirstTrialOperator A, ");
                sql.append("   count(distinct b.prtno) B    from lbcont b where b.conttype='1' and b.cardflag<>'1' ");
                sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
                sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'   "+FirstTrialOperator1+"  ");
                sql.append("   group by b.FirstTrialOperator ");
                sql.append("       union ");
                sql.append("   select b.FirstTrialOperator A, ");
                sql.append("   count(distinct b.prtno) B    from lobcont b where b.conttype='1' and b.cardflag<>'1' ");
                sql.append("  and  b.proposalcontno not in (select proposalcontno from LobIssuePol) and b.makedate ");
                sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+"   ");
                sql.append("   group by b.FirstTrialOperator ");
                sql.append("   ) as Y group by A ");
                sql.append("   ) as z group by U ");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");

                sql = new StringBuffer();
                sql.append("  select '总计',U,'个险投保书',sum(V),sum(W),sum(M),decimal(decimal(sum(M)+sum(M1))/decimal(sum(V))*100,12,2)");
               sql.append(" from ");
               sql.append(" ( ");
               sql.append("   select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1 ");
               sql.append("   from ");
               sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lccont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator  "+FirstTrialOperator1+"  ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"') E from LCIssuePol a,lccont b where b.conttype='1' ");
               sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'   "+FirstTrialOperator1+"   group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("     union ");
               sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator  "+FirstTrialOperator1+"  ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"') E   from LCIssuePol a,lbcont b where b.conttype='1' ");
               sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'   "+FirstTrialOperator1+"   group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("       union ");
               sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator  "+FirstTrialOperator1+"  ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"') E    from LobIssuePol a,lobcont b where b.conttype='1' ");
               sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'   "+FirstTrialOperator1+"   group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("   ) as X group by A,C ");
               sql.append("   union ");
               sql.append("   select A U,sum(B) V,0 W ,0 M ,0 M1 from (   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lccont b where b.conttype='1' and b.cardflag<>'1' ");
               sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'   "+FirstTrialOperator1+"  ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("     union ");
               sql.append("   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lbcont b where b.conttype='1' and b.cardflag<>'1' ");
               sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'   "+FirstTrialOperator1+"  ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("       union ");
               sql.append("   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lobcont b where b.conttype='1' and b.cardflag<>'1' ");
               sql.append("  and  b.proposalcontno not in (select proposalcontno from LobIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+"   ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("   ) as Y group by A ");
               sql.append("   ) as z group by U ");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第二个Sql语句~~~~~");

            } else if (Managecom.length() == 8) {
                sql = new StringBuffer();
                sql.append("  select '"+ Managecom +"',U,'个险投保书',sum(V),sum(W),sum(M),decimal(decimal(sum(M)+sum(M1))/decimal(sum(V))*100,12,2)");
              sql.append(" from ");
              sql.append(" ( ");
              sql.append("   select A U,sum(B) V,sum(C) W,sum(D) M ,sum(E) M1");
              sql.append("   from ");
              sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lccont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator  "+FirstTrialOperator1+") D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"') E from LCIssuePol a,lccont b where b.conttype='1' ");
              sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("     union ");
              sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+") D ,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"') E  from LCIssuePol a,lbcont b where b.conttype='1' ");
              sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("       union ");
              sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+") D  ,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"') E  from LobIssuePol a,lobcont b where b.conttype='1' ");
              sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("   ) as X group by A,C ");
              sql.append("   union ");
              sql.append("   select A U,sum(B) V,0 W ,0 M ,0 M1 from (   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lccont b where b.conttype='1' and b.cardflag<>'1' ");
              sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+"");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("     union ");
              sql.append("   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lbcont b where b.conttype='1' and b.cardflag<>'1' ");
              sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+"");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("       union ");
              sql.append("   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lobcont b where b.conttype='1' and b.cardflag<>'1' ");
              sql.append("  and  b.proposalcontno not in (select proposalcontno from LobIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+"");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("   ) as Y group by A ");
              sql.append("   ) as z group by U ");
               ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql = new StringBuffer();
                sql.append("  select '总计',U,'个险投保书',sum(V),sum(W),sum(M),decimal(decimal(sum(M)+sum(M1))/decimal(sum(V))*100,12,2)");
              sql.append(" from ");
              sql.append(" ( ");
              sql.append("   select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1 ");
              sql.append("   from ");
              sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lccont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator  "+FirstTrialOperator1+") D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"') E from LCIssuePol a,lccont b where b.conttype='1' ");
              sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("     union ");
              sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+") D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"') E   from LCIssuePol a,lbcont b where b.conttype='1' ");
              sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("       union ");
              sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+") D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"') E    from LobIssuePol a,lobcont b where b.conttype='1' ");
              sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("   ) as X group by A,C ");
              sql.append("   union ");
              sql.append("   select A U,sum(B) V,0 W ,0 M ,0 M1 from (   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lccont b where b.conttype='1' and b.cardflag<>'1' ");
              sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+"");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("     union ");
              sql.append("   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lbcont b where b.conttype='1' and b.cardflag<>'1' ");
              sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+"");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("       union ");
              sql.append("   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lobcont b where b.conttype='1' and b.cardflag<>'1' ");
              sql.append("  and  b.proposalcontno not in (select proposalcontno from LobIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+"");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("   ) as Y group by A ");
              sql.append("   ) as z group by U ");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第二个Sql语句~~~~~");

            }
        } else if(QYType.equals("2")){
            if (Managecom.length() == 2) {
                SSRS tSSRS_M;
                String sql_M = "select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode";
                System.out.println("zhuzhu" + sql_M);
                tExeSQL = new ExeSQL();
                tSSRS_M = tExeSQL.execSQL(sql_M);
                String temp_M[][] = tSSRS_M.getAllData();
                String Mng;
                if (tSSRS_M.getMaxRow() > 0) {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++) {
                        Mng = temp_M[i - 1][0];
                        String Mng1;
                        if (Mng.equals("86000000")) {
                            Mng1 = Mng;
                        } else {
                            Mng1 = Mng.substring(0, 4);
                        }
                        sql = new StringBuffer();
                        sql.append(" select '"+ Mng +"',U,'团险投保书',sum(V),sum(W),sum(M),decimal(decimal(sum(M))/decimal(sum(V))*100,12,2)");
                      sql.append(" from ");
                      sql.append(" ( ");
                      sql.append("   select A U,sum(B) V,sum(C) W,sum(D) M ");
                      sql.append("   from ");
                      sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                      sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                      sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                      sql.append("   and b.appflag='8'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" group by ");
                      sql.append("   b.FirstTrialOperator ");
                      sql.append("     union ");
                      sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                      sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                      sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                      sql.append("   and b.appflag='8'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
                      sql.append("   b.FirstTrialOperator ");
                      sql.append("       union ");
                      sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                      sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                      sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                      sql.append("   and b.appflag='8'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
                      sql.append("   b.FirstTrialOperator ");
                      sql.append("   ) as X group by A,C ");
                      sql.append("   union ");
                      sql.append("   select A U,sum(B) V,0 W ,0 M  from (   select b.FirstTrialOperator A, ");
                      sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag='8' ");
                      sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                      sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" ");
                      sql.append("   group by b.FirstTrialOperator ");
                      sql.append("     union ");
                      sql.append("   select b.FirstTrialOperator A, ");
                      sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag='8' ");
                      sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                      sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" ");
                      sql.append("   group by b.FirstTrialOperator ");
                      sql.append("       union ");
                      sql.append("   select b.FirstTrialOperator A, ");
                      sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag='8' ");
                      sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
                      sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" ");
                      sql.append("   group by b.FirstTrialOperator ");
                      sql.append("   ) as Y group by A ");
                      sql.append("   ) as z group by U ");

                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null) {
                            if (ssrs.getMaxRow() > 0) {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");
                    }
                    sql = new StringBuffer();
                    sql.append(" select '总计',U,'团险投保书',sum(V),sum(W),sum(M),decimal(decimal(sum(M))/decimal(sum(V))*100,12,2)");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append("   select A U,sum(B) V,sum(C) W,sum(D) M ");
                    sql.append("   from ");
                    sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                    sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                    sql.append(" and a.appflag='8' and b.code='85'  and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                    sql.append("   and b.appflag='8'    "+FirstTrialOperator1+" group by ");
                    sql.append("   b.FirstTrialOperator ");
                    sql.append("     union ");
                    sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                    sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                    sql.append(" and a.appflag='8' and b.code='85'  and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                    sql.append("   and b.appflag='8'     "+FirstTrialOperator1+" group by ");
                    sql.append("   b.FirstTrialOperator ");
                    sql.append("       union ");
                    sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                    sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                    sql.append(" and a.appflag='8' and b.code='85'  and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                    sql.append("   and b.appflag='8'     "+FirstTrialOperator1+" group by ");
                    sql.append("   b.FirstTrialOperator ");
                    sql.append("   ) as X group by A,C ");
                    sql.append("   union ");
                    sql.append("   select A U,sum(B) V,0 W ,0 M  from (   select b.FirstTrialOperator A, ");
                    sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag='8' ");
                    sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                    sql.append("  between '"+StartDate+"' and '"+EndDate+"'    "+FirstTrialOperator1+" ");
                    sql.append("   group by b.FirstTrialOperator ");
                    sql.append("     union ");
                    sql.append("   select b.FirstTrialOperator A, ");
                    sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag='8' ");
                    sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                    sql.append("  between '"+StartDate+"' and '"+EndDate+"'    "+FirstTrialOperator1+" ");
                    sql.append("   group by b.FirstTrialOperator ");
                    sql.append("       union ");
                    sql.append("   select b.FirstTrialOperator A, ");
                    sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag='8' ");
                    sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
                    sql.append("  between '"+StartDate+"' and '"+EndDate+"'    "+FirstTrialOperator1+" ");
                    sql.append("   group by b.FirstTrialOperator ");
                    sql.append("   ) as Y group by A ");
                    sql.append("   ) as z group by U ");

                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null) {
                        if (ssrs.getMaxRow() > 0) {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    System.out.println("处理完成第二个Sql语句~~~~~");
                }

            } else if (Managecom.length() == 4) {
                SSRS tSSRS_M;
                String Mng = tExeSQL.getOneValue("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='" +
                                                 Managecom +
                                                 "' order by ComCode");
                sql = new StringBuffer();
                sql.append(" select '"+ Mng +"',U,'团险投保书',sum(V),sum(W),sum(M),decimal(decimal(sum(M))/decimal(sum(V))*100,12,2)");
              sql.append(" from ");
              sql.append(" ( ");
              sql.append("   select A U,sum(B) V,sum(C) W,sum(D) M ");
              sql.append("   from ");
              sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.appflag='8'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("     union ");
              sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.appflag='8'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("       union ");
              sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.appflag='8'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("   ) as X group by A,C ");
              sql.append("   union ");
              sql.append("   select A U,sum(B) V,0 W ,0 M  from (   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag='8' ");
              sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("     union ");
              sql.append("   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag='8' ");
              sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("       union ");
              sql.append("   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag='8' ");
              sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("   ) as Y group by A ");
              sql.append("   ) as z group by U ");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");

                sql = new StringBuffer();
                sql.append(" select '总计',U,'团险投保书',sum(V),sum(W),sum(M),decimal(decimal(sum(M))/decimal(sum(V))*100,12,2)");
               sql.append(" from ");
               sql.append(" ( ");
               sql.append("   select A U,sum(B) V,sum(C) W,sum(D) M ");
               sql.append("   from ");
               sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.appflag='8'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("     union ");
               sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.appflag='8'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("       union ");
               sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.appflag='8'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("   ) as X group by A,C ");
               sql.append("   union ");
               sql.append("   select A U,sum(B) V,0 W ,0 M  from (   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag='8' ");
               sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("     union ");
               sql.append("   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag='8' ");
               sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("       union ");
               sql.append("   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag='8' ");
               sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("   ) as Y group by A ");
               sql.append("   ) as z group by U ");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第二个Sql语句~~~~~");

            } else if (Managecom.length() == 8) {
                sql = new StringBuffer();
                sql.append(" select '"+ Managecom +"',U,'团险投保书',sum(V),sum(W),sum(M),decimal(decimal(sum(M))/decimal(sum(V))*100,12,2)");
              sql.append(" from ");
              sql.append(" ( ");
              sql.append("   select A U,sum(B) V,sum(C) W,sum(D) M ");
              sql.append("   from ");
              sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.appflag='8'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("     union ");
              sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.appflag='8'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("       union ");
              sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.appflag='8'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("   ) as X group by A,C ");
              sql.append("   union ");
              sql.append("   select A U,sum(B) V,0 W ,0 M  from (   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag='8' ");
              sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("     union ");
              sql.append("   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag='8' ");
              sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("       union ");
              sql.append("   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag='8' ");
              sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("   ) as Y group by A ");
              sql.append("   ) as z group by U ");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql = new StringBuffer();
                sql.append(" select '总计',U,'团险投保书',sum(V),sum(W),sum(M),decimal(decimal(sum(M))/decimal(sum(V))*100,12,2)");
              sql.append(" from ");
              sql.append(" ( ");
              sql.append("   select A U,sum(B) V,sum(C) W,sum(D) M ");
              sql.append("   from ");
              sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.appflag='8'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("     union ");
              sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.appflag='8'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("       union ");
              sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.appflag='8'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("   ) as X group by A,C ");
              sql.append("   union ");
              sql.append("   select A U,sum(B) V,0 W ,0 M  from (   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag='8' ");
              sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("     union ");
              sql.append("   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag='8' ");
              sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("       union ");
              sql.append("   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag='8' ");
              sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("   ) as Y group by A ");
              sql.append("   ) as z group by U ");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第二个Sql语句~~~~~");

            }

        }else if(QYType.equals("3")){
            if (Managecom.length() == 2) {
                SSRS tSSRS_M;
                String sql_M = "select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode";
                System.out.println("zhuzhu" + sql_M);
                tExeSQL = new ExeSQL();
                tSSRS_M = tExeSQL.execSQL(sql_M);
                String temp_M[][] = tSSRS_M.getAllData();
                String Mng;
                if (tSSRS_M.getMaxRow() > 0) {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++) {
                        Mng = temp_M[i - 1][0];
                        String Mng1;
                        if (Mng.equals("86000000")) {
                            Mng1 = Mng;
                        } else {
                            Mng1 = Mng.substring(0, 4);
                        }
                        sql = new StringBuffer();
                        sql.append(" select '"+ Mng +"',U,'团险投保书',sum(V),sum(W),sum(M),decimal(decimal(sum(M)+sum(M1))/decimal(sum(V))*100,12,2)");
                      sql.append(" from ");
                      sql.append(" ( ");
                      sql.append("   select A U,sum(B) V,sum(C) W,sum(D) M ,sum(E) M1");
                      sql.append("   from ");
                      sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                      sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                      sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' and substr(managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator2+" and makedate between '"+StartDate+"' and '"+EndDate+"') E from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                      sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" group by ");
                      sql.append("   b.FirstTrialOperator ");
                      sql.append("     union ");
                      sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                      sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                      sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' and substr(managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator2+" and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                      sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
                      sql.append("   b.FirstTrialOperator ");
                      sql.append("       union ");
                      sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                      sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                      sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' and substr(managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator2+" and makedate between '"+StartDate+"' and '"+EndDate+"') E    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                      sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
                      sql.append("   b.FirstTrialOperator ");
                      sql.append("   ) as X group by A,C ");
                      sql.append("   union ");
                      sql.append("   select A U,sum(B) V,0 W ,0 M ,0 M1 from (   select b.FirstTrialOperator A, ");
                      sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag<>'8' ");
                      sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                      sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" ");
                      sql.append("   group by b.FirstTrialOperator ");
                      sql.append("     union ");
                      sql.append("   select b.FirstTrialOperator A, ");
                      sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag<>'8' ");
                      sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                      sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" ");
                      sql.append("   group by b.FirstTrialOperator ");
                      sql.append("       union ");
                      sql.append("   select b.FirstTrialOperator A, ");
                      sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag<>'8' ");
                      sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
                      sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" ");
                      sql.append("   group by b.FirstTrialOperator ");
                      sql.append("   ) as Y group by A ");
                      sql.append("   ) as z group by U ");

                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null) {
                            if (ssrs.getMaxRow() > 0) {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");
                    }
                    sql = new StringBuffer();
                    sql.append(" select '总计',U,'团险投保书',sum(V),sum(W),sum(M),decimal(decimal(sum(M)+sum(M1))/decimal(sum(V))*100,12,2)");
                    sql.append(" from ");
                    sql.append(" ( ");
                    sql.append("   select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
                    sql.append("   from ");
                    sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                    sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                    sql.append(" and a.appflag<>'8' and b.code='54'  and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and makedate between '"+StartDate+"' and '"+EndDate+"') E from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                    sql.append("   and b.appflag<>'8'    "+FirstTrialOperator1+" group by ");
                    sql.append("   b.FirstTrialOperator ");
                    sql.append("     union ");
                    sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                    sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                    sql.append(" and a.appflag<>'8' and b.code='54'  and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                    sql.append("   and b.appflag<>'8'     "+FirstTrialOperator1+" group by ");
                    sql.append("   b.FirstTrialOperator ");
                    sql.append("       union ");
                    sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                    sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                    sql.append(" and a.appflag<>'8' and b.code='54'  and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and makedate between '"+StartDate+"' and '"+EndDate+"') E    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                    sql.append("   and b.appflag<>'8'     "+FirstTrialOperator1+" group by ");
                    sql.append("   b.FirstTrialOperator ");
                    sql.append("   ) as X group by A,C ");
                    sql.append("   union ");
                    sql.append("   select A U,sum(B) V,0 W ,0 M ,0 M1 from (   select b.FirstTrialOperator A, ");
                    sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag<>'8' ");
                    sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                    sql.append("  between '"+StartDate+"' and '"+EndDate+"'    "+FirstTrialOperator1+" ");
                    sql.append("   group by b.FirstTrialOperator ");
                    sql.append("     union ");
                    sql.append("   select b.FirstTrialOperator A, ");
                    sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag<>'8' ");
                    sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                    sql.append("  between '"+StartDate+"' and '"+EndDate+"'    "+FirstTrialOperator1+" ");
                    sql.append("   group by b.FirstTrialOperator ");
                    sql.append("       union ");
                    sql.append("   select b.FirstTrialOperator A, ");
                    sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag<>'8' ");
                    sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
                    sql.append("  between '"+StartDate+"' and '"+EndDate+"'    "+FirstTrialOperator1+" ");
                    sql.append("   group by b.FirstTrialOperator ");
                    sql.append("   ) as Y group by A ");
                    sql.append("   ) as z group by U ");

                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null) {
                        if (ssrs.getMaxRow() > 0) {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    System.out.println("处理完成第二个Sql语句~~~~~");
                }

            } else if (Managecom.length() == 4) {
                SSRS tSSRS_M;
                String Mng = tExeSQL.getOneValue("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='" +
                                                 Managecom +
                                                 "' order by ComCode");
                sql = new StringBuffer();
                sql.append(" select '"+ Mng +"',U,'团险投保书',sum(V),sum(W),sum(M),decimal(decimal(sum(M)+sum(M1))/decimal(sum(V))*100,12,2)");
              sql.append(" from ");
              sql.append(" ( ");
              sql.append("   select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1 ");
              sql.append("   from ");
              sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("     union ");
              sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("       union ");
              sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("   ) as X group by A,C ");
              sql.append("   union ");
              sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag<>'8' ");
              sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("     union ");
              sql.append("   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag<>'8' ");
              sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("       union ");
              sql.append("   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag<>'8' ");
              sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("   ) as Y group by A ");
              sql.append("   ) as z group by U ");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");

                sql = new StringBuffer();
                sql.append(" select '总计',U,'团险投保书',sum(V),sum(W),sum(M),decimal(decimal(sum(M)+sum(M1))/decimal(sum(V))*100,12,2)");
               sql.append(" from ");
               sql.append(" ( ");
               sql.append("   select A U,sum(B) V,sum(C) W,sum(D) M ,sum(E) M1");
               sql.append("   from ");
               sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("     union ");
               sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("       union ");
               sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D ,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("   ) as X group by A,C ");
               sql.append("   union ");
               sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag<>'8' ");
               sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("     union ");
               sql.append("   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag<>'8' ");
               sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("       union ");
               sql.append("   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag<>'8' ");
               sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("   ) as Y group by A ");
               sql.append("   ) as z group by U ");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第二个Sql语句~~~~~");

            } else if (Managecom.length() == 8) {
                sql = new StringBuffer();
                sql.append(" select '"+ Managecom +"',U,'团险投保书',sum(V),sum(W),sum(M),decimal(decimal(sum(M)+sum(M1))/decimal(sum(V))*100,12,2)");
              sql.append(" from ");
              sql.append(" ( ");
              sql.append("   select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
              sql.append("   from ");
              sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("     union ");
              sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("       union ");
              sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("   ) as X group by A,C ");
              sql.append("   union ");
              sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag<>'8' ");
              sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("     union ");
              sql.append("   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag<>'8' ");
              sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("       union ");
              sql.append("   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag<>'8' ");
              sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("   ) as Y group by A ");
              sql.append("   ) as z group by U ");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql = new StringBuffer();
                sql.append(" select '总计',U,'团险投保书',sum(V),sum(W),sum(M),decimal(decimal(sum(M)+sum(M1))/decimal(sum(V))*100,12,2)");
              sql.append(" from ");
              sql.append(" ( ");
              sql.append("   select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
              sql.append("   from ");
              sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("     union ");
              sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("       union ");
              sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
              sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
              sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
              sql.append("   b.FirstTrialOperator ");
              sql.append("   ) as X group by A,C ");
              sql.append("   union ");
              sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag<>'8' ");
              sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("     union ");
              sql.append("   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag<>'8' ");
              sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("       union ");
              sql.append("   select b.FirstTrialOperator A, ");
              sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag<>'8' ");
              sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
              sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
              sql.append("   group by b.FirstTrialOperator ");
              sql.append("   ) as Y group by A ");
              sql.append("   ) as z group by U ");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第二个Sql语句~~~~~");

            }

        }else {
            if (Managecom.length() == 2) {
                SSRS tSSRS_M;
                String sql_M = "select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode";
                System.out.println("zhuzhu" + sql_M);
                tExeSQL = new ExeSQL();
                tSSRS_M = tExeSQL.execSQL(sql_M);
                String temp_M[][] = tSSRS_M.getAllData();
                String Mng;
                if (tSSRS_M.getMaxRow() > 0) {
                    for (int i = 1; i <= tSSRS_M.getMaxRow(); i++) {
                        Mng = temp_M[i - 1][0];
                        String Mng1;
                        if (Mng.equals("86000000")) {
                            Mng1 = Mng;
                        } else {
                            Mng1 = Mng.substring(0, 4);
                        }
                        sql = new StringBuffer();
                        sql.append(" select '"+ Mng +"',U,'全部',sum(V),sum(W),sum(M),decimal(decimal(sum(M)+sum(M1))/decimal(sum(V))*100,12,2)");
                       sql.append(" from ");
                       sql.append(" ( ");
                       sql.append(" select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
                       sql.append("   from ");
                       sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                       sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lccont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                       sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1' and  substr(managecom,1,length('"+Managecom+"')*4)='"+ Mng +"'  "+FirstTrialOperator2+" and conttype='1') E from LCIssuePol a,lccont b where b.conttype='1' ");
                       sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                       sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" group by ");
                       sql.append("   b.FirstTrialOperator ");
                       sql.append("     union ");
                       sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                       sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                       sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1' and  substr(managecom,1,length('"+Managecom+"')*4)='"+ Mng +"'  "+FirstTrialOperator2+" and conttype='1') E   from LCIssuePol a,lbcont b where b.conttype='1' ");
                       sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                       sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
                       sql.append("   b.FirstTrialOperator ");
                       sql.append("       union ");
                       sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                       sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                       sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1' and  substr(managecom,1,length('"+Managecom+"')*4)='"+ Mng +"'  "+FirstTrialOperator2+" and conttype='1') E    from LobIssuePol a,lobcont b where b.conttype='1' ");
                       sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                       sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
                       sql.append("   b.FirstTrialOperator ");
                       sql.append("   ) as X group by A,C ");
                       sql.append("   union ");
                       sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
                       sql.append("   count(distinct b.prtno) B    from lccont b where b.conttype='1' and b.cardflag<>'1' ");
                       sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
                       sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" ");
                       sql.append("   group by b.FirstTrialOperator ");
                       sql.append("     union ");
                       sql.append("   select b.FirstTrialOperator A, ");
                       sql.append("   count(distinct b.prtno) B    from lbcont b where b.conttype='1' and b.cardflag<>'1' ");
                       sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
                       sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" ");
                       sql.append("   group by b.FirstTrialOperator ");
                       sql.append("       union ");
                       sql.append("   select b.FirstTrialOperator A, ");
                       sql.append("   count(distinct b.prtno) B    from lobcont b where b.conttype='1' and b.cardflag<>'1' ");
                       sql.append("  and  b.proposalcontno not in (select proposalcontno from LobIssuePol) and b.makedate ");
                       sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" ");
                       sql.append("   group by b.FirstTrialOperator ");
                       sql.append("   ) as Y group by A ");
                       sql.append("   union ");
                       sql.append("     select A U,sum(B) V,sum(C) W,sum(D) M ,sum(E) M1");
                       sql.append("   from ");
                       sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                       sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                       sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' and substr(managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator2+" and makedate between '"+StartDate+"' and '"+EndDate+"') E from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                       sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" group by ");
                       sql.append("   b.FirstTrialOperator ");
                       sql.append("     union ");
                       sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                       sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                       sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' and substr(managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator2+" and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                       sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
                       sql.append("   b.FirstTrialOperator ");
                       sql.append("       union ");
                       sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                       sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                       sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' and substr(managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator2+" and makedate between '"+StartDate+"' and '"+EndDate+"') E    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                       sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
                       sql.append("   b.FirstTrialOperator ");
                       sql.append("   ) as P group by A,C ");
                       sql.append("   union ");
                       sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
                       sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag<>'8' ");
                       sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                       sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" ");
                       sql.append("   group by b.FirstTrialOperator ");
                       sql.append("     union ");
                       sql.append("   select b.FirstTrialOperator A, ");
                       sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag<>'8' ");
                       sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                       sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" ");
                       sql.append("   group by b.FirstTrialOperator ");
                       sql.append("       union ");
                       sql.append("   select b.FirstTrialOperator A, ");
                       sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag<>'8' ");
                       sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
                       sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" ");
                       sql.append("   group by b.FirstTrialOperator ");
                       sql.append("   ) as Q group by A ");
                       sql.append("    union ");
                       sql.append("     select A U,sum(B) V,sum(C) W,sum(D) M ,sum(E) M1");
                       sql.append("   from ");
                       sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                       sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                       sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag='8' and substr(managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator2+" and makedate between '"+StartDate+"' and '"+EndDate+"') E from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                       sql.append("   and b.appflag='8'  and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" group by ");
                       sql.append("   b.FirstTrialOperator ");
                       sql.append("     union ");
                       sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                       sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                       sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag='8' and substr(managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator2+" and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                       sql.append("   and b.appflag='8'  and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
                       sql.append("   b.FirstTrialOperator ");
                       sql.append("       union ");
                       sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                       sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                       sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag='8' and substr(managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator2+" and makedate between '"+StartDate+"' and '"+EndDate+"') E    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                       sql.append("   and b.appflag='8'  and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
                       sql.append("   b.FirstTrialOperator ");
                       sql.append("   ) as S group by A,C ");
                       sql.append("   union ");
                       sql.append("   select A U,sum(B) V,0 W ,0 M ,0 M1 from (   select b.FirstTrialOperator A, ");
                       sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag='8' ");
                       sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                       sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" ");
                       sql.append("   group by b.FirstTrialOperator ");
                       sql.append("     union ");
                       sql.append("   select b.FirstTrialOperator A, ");
                       sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag='8' ");
                       sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                       sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" ");
                       sql.append("   group by b.FirstTrialOperator ");
                       sql.append("       union ");
                       sql.append("   select b.FirstTrialOperator A, ");
                       sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag='8' ");
                       sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
                       sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*4)='"+ Mng +"' "+FirstTrialOperator1+" ");
                       sql.append("   group by b.FirstTrialOperator ");
                       sql.append("   ) as L group by A ");
                       sql.append("     ) as z group by U ");


                        ssrs = tExeSQL.execSQL(sql.toString());
                        if (ssrs != null) {
                            if (ssrs.getMaxRow() > 0) {
                                for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                    result.add(ssrs.getRowData(m));
                                }
                            }
                        }
                        ssrs = null;
                        sql = new StringBuffer();
                        System.out.println("处理完成第一个Sql语句~~~~~");
                    }
                    sql = new StringBuffer();
                    sql.append(" select '全部',U,'全部',sum(V),sum(W),sum(M),decimal(decimal(sum(M)+sum(M1))/decimal(sum(V))*100,12,2)");
                   sql.append(" from ");
                   sql.append(" ( ");
                   sql.append(" select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
                   sql.append("   from ");
                   sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                   sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lccont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append(" and a.cardflag<>'1' and b.code='85'  and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1') E from LCIssuePol a,lccont b where b.conttype='1' ");
                   sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append("   and b.cardflag<>'1'    "+FirstTrialOperator1+" group by ");
                   sql.append("   b.FirstTrialOperator ");
                   sql.append("     union ");
                   sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                   sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append(" and a.cardflag<>'1' and b.code='85'  and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1') E   from LCIssuePol a,lbcont b where b.conttype='1' ");
                   sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append("   and b.cardflag<>'1'     "+FirstTrialOperator1+" group by ");
                   sql.append("   b.FirstTrialOperator ");
                   sql.append("       union ");
                   sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                   sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append(" and a.cardflag<>'1' and b.code='85'  and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1') E    from LobIssuePol a,lobcont b where b.conttype='1' ");
                   sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append("   and b.cardflag<>'1'     "+FirstTrialOperator1+" group by ");
                   sql.append("   b.FirstTrialOperator ");
                   sql.append("   ) as X group by A,C ");
                   sql.append("   union ");
                   sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
                   sql.append("   count(distinct b.prtno) B    from lccont b where b.conttype='1' and b.cardflag<>'1' ");
                   sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
                   sql.append("  between '"+StartDate+"' and '"+EndDate+"'    "+FirstTrialOperator1+" ");
                   sql.append("   group by b.FirstTrialOperator ");
                   sql.append("     union ");
                   sql.append("   select b.FirstTrialOperator A, ");
                   sql.append("   count(distinct b.prtno) B    from lbcont b where b.conttype='1' and b.cardflag<>'1' ");
                   sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
                   sql.append("  between '"+StartDate+"' and '"+EndDate+"'    "+FirstTrialOperator1+" ");
                   sql.append("   group by b.FirstTrialOperator ");
                   sql.append("       union ");
                   sql.append("   select b.FirstTrialOperator A, ");
                   sql.append("   count(distinct b.prtno) B    from lobcont b where b.conttype='1' and b.cardflag<>'1' ");
                   sql.append("  and  b.proposalcontno not in (select proposalcontno from LobIssuePol) and b.makedate ");
                   sql.append("  between '"+StartDate+"' and '"+EndDate+"'   "+FirstTrialOperator1+" ");
                   sql.append("   group by b.FirstTrialOperator ");
                   sql.append("   ) as Y group by A ");
                   sql.append("   union ");
                   sql.append("     select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
                   sql.append("   from ");
                   sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                   sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append(" and a.appflag<>'8' and b.code='54'  and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and makedate between '"+StartDate+"' and '"+EndDate+"') E from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append("   and b.appflag<>'8'    "+FirstTrialOperator1+" group by ");
                   sql.append("   b.FirstTrialOperator ");
                   sql.append("     union ");
                   sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                   sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append(" and a.appflag<>'8' and b.code='54'  and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append("   and b.appflag<>'8'     "+FirstTrialOperator1+" group by ");
                   sql.append("   b.FirstTrialOperator ");
                   sql.append("       union ");
                   sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                   sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append(" and a.appflag<>'8' and b.code='54'  and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and makedate between '"+StartDate+"' and '"+EndDate+"') E    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append("   and b.appflag<>'8'     "+FirstTrialOperator1+" group by ");
                   sql.append("   b.FirstTrialOperator ");
                   sql.append("   ) as P group by A,C ");
                   sql.append("   union ");
                   sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
                   sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag<>'8' ");
                   sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                   sql.append("  between '"+StartDate+"' and '"+EndDate+"'    "+FirstTrialOperator1+" ");
                   sql.append("   group by b.FirstTrialOperator ");
                   sql.append("     union ");
                   sql.append("   select b.FirstTrialOperator A, ");
                   sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag<>'8' ");
                   sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                   sql.append("  between '"+StartDate+"' and '"+EndDate+"'    "+FirstTrialOperator1+" ");
                   sql.append("   group by b.FirstTrialOperator ");
                   sql.append("       union ");
                   sql.append("   select b.FirstTrialOperator A, ");
                   sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag<>'8' ");
                   sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
                   sql.append("  between '"+StartDate+"' and '"+EndDate+"'    "+FirstTrialOperator1+" ");
                   sql.append("   group by b.FirstTrialOperator ");
                   sql.append("   ) as Q group by A ");
                   sql.append("    union ");
                   sql.append("     select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
                   sql.append("   from ");
                   sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                   sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append(" and a.appflag='8' and b.code='85'  and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag='8' "+FirstTrialOperator2+" and makedate between '"+StartDate+"' and '"+EndDate+"') E from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append("   and b.appflag='8'   "+FirstTrialOperator1+" group by ");
                   sql.append("   b.FirstTrialOperator ");
                   sql.append("     union ");
                   sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                   sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append(" and a.appflag='8' and b.code='85'  and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag='8' "+FirstTrialOperator2+" and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append("   and b.appflag='8'    "+FirstTrialOperator1+" group by ");
                   sql.append("   b.FirstTrialOperator ");
                   sql.append("       union ");
                   sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                   sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append(" and a.appflag='8' and b.code='85'  and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag='8' "+FirstTrialOperator2+" and makedate between '"+StartDate+"' and '"+EndDate+"') E    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                   sql.append("   and b.appflag='8'    "+FirstTrialOperator1+" group by ");
                   sql.append("   b.FirstTrialOperator ");
                   sql.append("   ) as S group by A,C ");
                   sql.append("   union ");
                   sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
                   sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag='8' ");
                   sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                   sql.append("  between '"+StartDate+"' and '"+EndDate+"'    "+FirstTrialOperator1+" ");
                   sql.append("   group by b.FirstTrialOperator ");
                   sql.append("     union ");
                   sql.append("   select b.FirstTrialOperator A, ");
                   sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag='8' ");
                   sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                   sql.append("  between '"+StartDate+"' and '"+EndDate+"'    "+FirstTrialOperator1+" ");
                   sql.append("   group by b.FirstTrialOperator ");
                   sql.append("       union ");
                   sql.append("   select b.FirstTrialOperator A, ");
                   sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag='8' ");
                   sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
                   sql.append("  between '"+StartDate+"' and '"+EndDate+"'    "+FirstTrialOperator1+" ");
                   sql.append("   group by b.FirstTrialOperator ");
                   sql.append("   ) as L group by A ");
                   sql.append("     ) as z group by U ");


                    ssrs = tExeSQL.execSQL(sql.toString());
                    if (ssrs != null) {
                        if (ssrs.getMaxRow() > 0) {
                            for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                                result.add(ssrs.getRowData(m));
                            }
                        }
                    }
                    ssrs = null;
                    System.out.println("处理完成第二个Sql语句~~~~~");
                }

            } else if (Managecom.length() == 4) {
                SSRS tSSRS_M;
                String Mng = tExeSQL.getOneValue("select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='" +
                                                 Managecom +
                                                 "' order by ComCode");
                sql = new StringBuffer();
                sql.append(" select '"+ Mng +"',U,'全部',sum(V),sum(W),sum(M),decimal(decimal(sum(M)+sum(M1))/decimal(sum(V))*100,12,2)");
                sql.append(" from ");
                sql.append(" ( ");
                sql.append(" select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
                sql.append("   from ");
                sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lccont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"') E from LCIssuePol a,lccont b where b.conttype='1' ");
                sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" group by ");
                sql.append("   b.FirstTrialOperator ");
                sql.append("     union ");
                sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"') E   from LCIssuePol a,lbcont b where b.conttype='1' ");
                sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
                sql.append("   b.FirstTrialOperator ");
                sql.append("       union ");
                sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"') E    from LobIssuePol a,lobcont b where b.conttype='1' ");
                sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
                sql.append("   b.FirstTrialOperator ");
                sql.append("   ) as X group by A,C ");
                sql.append("   union ");
                sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
                sql.append("   count(distinct b.prtno) B    from lccont b where b.conttype='1' and b.cardflag<>'1' ");
                sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
                sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
                sql.append("   group by b.FirstTrialOperator ");
                sql.append("     union ");
                sql.append("   select b.FirstTrialOperator A, ");
                sql.append("   count(distinct b.prtno) B    from lbcont b where b.conttype='1' and b.cardflag<>'1' ");
                sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
                sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
                sql.append("   group by b.FirstTrialOperator ");
                sql.append("       union ");
                sql.append("   select b.FirstTrialOperator A, ");
                sql.append("   count(distinct b.prtno) B    from lobcont b where b.conttype='1' and b.cardflag<>'1' ");
                sql.append("  and  b.proposalcontno not in (select proposalcontno from LobIssuePol) and b.makedate ");
                sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
                sql.append("   group by b.FirstTrialOperator ");
                sql.append("   ) as Y group by A ");
                sql.append("   union ");
                sql.append("     select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
                sql.append("   from ");
                sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" group by ");
                sql.append("   b.FirstTrialOperator ");
                sql.append("     union ");
                sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
                sql.append("   b.FirstTrialOperator ");
                sql.append("       union ");
                sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
                sql.append("   b.FirstTrialOperator ");
                sql.append("   ) as P group by A,C ");
                sql.append("   union ");
                sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
                sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag<>'8' ");
                sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
                sql.append("   group by b.FirstTrialOperator ");
                sql.append("     union ");
                sql.append("   select b.FirstTrialOperator A, ");
                sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag<>'8' ");
                sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
                sql.append("   group by b.FirstTrialOperator ");
                sql.append("       union ");
                sql.append("   select b.FirstTrialOperator A, ");
                sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag<>'8' ");
                sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
                sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
                sql.append("   group by b.FirstTrialOperator ");
                sql.append("   ) as Q group by A ");
                sql.append("    union ");
                sql.append("     select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
                sql.append("   from ");
                sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag='8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append("   and b.appflag='8'  and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" group by ");
                sql.append("   b.FirstTrialOperator ");
                sql.append("     union ");
                sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag='8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append("   and b.appflag='8'  and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
                sql.append("   b.FirstTrialOperator ");
                sql.append("       union ");
                sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag='8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                sql.append("   and b.appflag='8'  and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
                sql.append("   b.FirstTrialOperator ");
                sql.append("   ) as S group by A,C ");
                sql.append("   union ");
                sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
                sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag='8' ");
                sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
                sql.append("   group by b.FirstTrialOperator ");
                sql.append("     union ");
                sql.append("   select b.FirstTrialOperator A, ");
                sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag='8' ");
                sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
                sql.append("   group by b.FirstTrialOperator ");
                sql.append("       union ");
                sql.append("   select b.FirstTrialOperator A, ");
                sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag='8' ");
                sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
                sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
                sql.append("   group by b.FirstTrialOperator ");
                sql.append("   ) as L group by A ");
                sql.append("     ) as z group by U ");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");

                sql = new StringBuffer();
                sql.append(" select '总计',U,'全部',sum(V),sum(W),sum(M),decimal(decimal(sum(M)+sum(M1))/decimal(sum(V))*100,12,2)");
               sql.append(" from ");
               sql.append(" ( ");
               sql.append(" select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
               sql.append("   from ");
               sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lccont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"') E  from LCIssuePol a,lccont b where b.conttype='1' ");
               sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("     union ");
               sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"') E    from LCIssuePol a,lbcont b where b.conttype='1' ");
               sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("       union ");
               sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"') E     from LobIssuePol a,lobcont b where b.conttype='1' ");
               sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("   ) as X group by A,C ");
               sql.append("   union ");
               sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lccont b where b.conttype='1' and b.cardflag<>'1' ");
               sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("     union ");
               sql.append("   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lbcont b where b.conttype='1' and b.cardflag<>'1' ");
               sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("       union ");
               sql.append("   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lobcont b where b.conttype='1' and b.cardflag<>'1' ");
               sql.append("  and  b.proposalcontno not in (select proposalcontno from LobIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("   ) as Y group by A ");
               sql.append("   union ");
               sql.append("     select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
               sql.append("   from ");
               sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("     union ");
               sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("       union ");
               sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("   ) as P group by A,C ");
               sql.append("   union ");
               sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag<>'8' ");
               sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("     union ");
               sql.append("   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag<>'8' ");
               sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("       union ");
               sql.append("   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag<>'8' ");
               sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("   ) as Q group by A ");
               sql.append("    union ");
               sql.append("     select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
               sql.append("   from ");
               sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag='8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.appflag='8'  and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("     union ");
               sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag='8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.appflag='8'  and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("       union ");
               sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag='8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.appflag='8'  and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"'  "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("   ) as S group by A,C ");
               sql.append("   union ");
               sql.append("   select A U,sum(B) V,0 W ,0 M, 0 M1  from (   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag='8' ");
               sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("     union ");
               sql.append("   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag='8' ");
               sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("       union ");
               sql.append("   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag='8' ");
               sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"')*2)='"+ Mng +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("   ) as L group by A ");
               sql.append("     ) as z group by U ");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第二个Sql语句~~~~~");

            } else if (Managecom.length() == 8) {
                sql = new StringBuffer();
                sql.append(" select '"+ Managecom +"',U,'全部',sum(V),sum(W),sum(M),decimal(decimal(sum(M)+sum(M1))/decimal(sum(V))*100,12,2)");
               sql.append(" from ");
               sql.append(" ( ");
               sql.append(" select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
               sql.append("   from ");
               sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lccont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"') E from LCIssuePol a,lccont b where b.conttype='1' ");
               sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("     union ");
               sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"') E   from LCIssuePol a,lbcont b where b.conttype='1' ");
               sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("       union ");
               sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"') E    from LobIssuePol a,lobcont b where b.conttype='1' ");
               sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("   ) as X group by A,C ");
               sql.append("   union ");
               sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lccont b where b.conttype='1' and b.cardflag<>'1' ");
               sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("     union ");
               sql.append("   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lbcont b where b.conttype='1' and b.cardflag<>'1' ");
               sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("       union ");
               sql.append("   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lobcont b where b.conttype='1' and b.cardflag<>'1' ");
               sql.append("  and  b.proposalcontno not in (select proposalcontno from LobIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("   ) as Y group by A ");
               sql.append("   union ");
               sql.append("     select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
               sql.append("   from ");
               sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("     union ");
               sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("       union ");
               sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("   ) as P group by A,C ");
               sql.append("   union ");
               sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag<>'8' ");
               sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("     union ");
               sql.append("   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag<>'8' ");
               sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("       union ");
               sql.append("   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag<>'8' ");
               sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("   ) as Q group by A ");
               sql.append("    union ");
               sql.append("     select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
               sql.append("   from ");
               sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag='8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.appflag='8'  and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("     union ");
               sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag='8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.appflag='8'  and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("       union ");
               sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
               sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag='8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
               sql.append("   and b.appflag='8'  and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
               sql.append("   b.FirstTrialOperator ");
               sql.append("   ) as S group by A,C ");
               sql.append("   union ");
               sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag='8' ");
               sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("     union ");
               sql.append("   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag='8' ");
               sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("       union ");
               sql.append("   select b.FirstTrialOperator A, ");
               sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag='8' ");
               sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
               sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
               sql.append("   group by b.FirstTrialOperator ");
               sql.append("   ) as L group by A ");
               sql.append("     ) as z group by U ");

                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第一个Sql语句~~~~~");
                sql = new StringBuffer();
                sql.append(" select '总计',U,'全部',sum(V),sum(W),sum(M),decimal(decimal(sum(M)+sum(M1))/decimal(sum(V))*100,12,2)");
                 sql.append(" from ");
                 sql.append(" ( ");
                 sql.append(" select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
                 sql.append("   from ");
                 sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                 sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lccont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                 sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"') E from LCIssuePol a,lccont b where b.conttype='1' ");
                 sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                 sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" group by ");
                 sql.append("   b.FirstTrialOperator ");
                 sql.append("     union ");
                 sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                 sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                 sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"') E   from LCIssuePol a,lbcont b where b.conttype='1' ");
                 sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                 sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
                 sql.append("   b.FirstTrialOperator ");
                 sql.append("       union ");
                 sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                 sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobcont a,loprtmanager b where a.proposalcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                 sql.append(" and a.cardflag<>'1' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(1) from lobcont where makedate between '"+StartDate+"' and '"+EndDate+"' and cardflag<>'1'   "+FirstTrialOperator2+" and conttype='1' and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"') E    from LobIssuePol a,lobcont b where b.conttype='1' ");
                 sql.append("   and b.proposalcontno=a.proposalcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                 sql.append("   and b.cardflag<>'1'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
                 sql.append("   b.FirstTrialOperator ");
                 sql.append("   ) as X group by A,C ");
                 sql.append("   union ");
                 sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
                 sql.append("   count(distinct b.prtno) B    from lccont b where b.conttype='1' and b.cardflag<>'1' ");
                 sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
                 sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
                 sql.append("   group by b.FirstTrialOperator ");
                 sql.append("     union ");
                 sql.append("   select b.FirstTrialOperator A, ");
                 sql.append("   count(distinct b.prtno) B    from lbcont b where b.conttype='1' and b.cardflag<>'1' ");
                 sql.append("  and  b.proposalcontno not in (select proposalcontno from LCIssuePol) and b.makedate ");
                 sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
                 sql.append("   group by b.FirstTrialOperator ");
                 sql.append("       union ");
                 sql.append("   select b.FirstTrialOperator A, ");
                 sql.append("   count(distinct b.prtno) B    from lobcont b where b.conttype='1' and b.cardflag<>'1' ");
                 sql.append("  and  b.proposalcontno not in (select proposalcontno from LobIssuePol) and b.makedate ");
                 sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
                 sql.append("   group by b.FirstTrialOperator ");
                 sql.append("   ) as Y group by A ");
                 sql.append("   union ");
                 sql.append("     select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
                 sql.append("   from ");
                 sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                 sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                 sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                 sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" group by ");
                 sql.append("   b.FirstTrialOperator ");
                 sql.append("     union ");
                 sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                 sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                 sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                 sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
                 sql.append("   b.FirstTrialOperator ");
                 sql.append("       union ");
                 sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                 sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                 sql.append(" and a.appflag<>'8' and b.code='54' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag<>'8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                 sql.append("   and b.appflag<>'8'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
                 sql.append("   b.FirstTrialOperator ");
                 sql.append("   ) as P group by A,C ");
                 sql.append("   union ");
                 sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
                 sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag<>'8' ");
                 sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                 sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
                 sql.append("   group by b.FirstTrialOperator ");
                 sql.append("     union ");
                 sql.append("   select b.FirstTrialOperator A, ");
                 sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag<>'8' ");
                 sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                 sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
                 sql.append("   group by b.FirstTrialOperator ");
                 sql.append("       union ");
                 sql.append("   select b.FirstTrialOperator A, ");
                 sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag<>'8' ");
                 sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
                 sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
                 sql.append("   group by b.FirstTrialOperator ");
                 sql.append("   ) as Q group by A ");
                 sql.append("    union ");
                 sql.append("     select A U,sum(B) V,sum(C) W,sum(D) M,sum(E) M1");
                 sql.append("   from ");
                 sql.append("   (   select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                 sql.append("   count(distinct a.prtseq) C,  (select count(*) from  lcgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                 sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag='8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E  from LCgrpIssuePol a,lcgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                 sql.append("   and b.appflag='8'  and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" group by ");
                 sql.append("   b.FirstTrialOperator ");
                 sql.append("     union ");
                 sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                 sql.append("   count(distinct a.prtseq) C ,(select count(*) from  lbgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                 sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag='8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E   from LCgrpIssuePol a,lbgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                 sql.append("   and b.appflag='8'  and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
                 sql.append("   b.FirstTrialOperator ");
                 sql.append("       union ");
                 sql.append("     select b.FirstTrialOperator A,   count(distinct b.prtno) B   , ");
                 sql.append("   count(distinct a.prtseq) C,(select count(*) from  lobgrpcont a,loprtmanager b where a.proposalgrpcontno=b.otherno and a.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                 sql.append(" and a.appflag='8' and b.code='85' and substr(a.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and a.FirstTrialOperator=b.FirstTrialOperator "+FirstTrialOperator1+" ) D,(select count(*) from lobgrpcont where  appflag='8' "+FirstTrialOperator2+" and substr(managecom,1,length('"+Managecom+"'))='"+ Managecom +"' and makedate between '"+StartDate+"' and '"+EndDate+"') E    from LobgrpIssuePol a,lobgrpcont b where  b.proposalgrpcontno=a.proposalgrpcontno and b.makedate between '"+StartDate+"' and '"+EndDate+"' ");
                 sql.append("   and b.appflag='8'  and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"'  "+FirstTrialOperator1+" group by ");
                 sql.append("   b.FirstTrialOperator ");
                 sql.append("   ) as S group by A,C ");
                 sql.append("   union ");
                 sql.append("   select A U,sum(B) V,0 W ,0 M,0 M1  from (   select b.FirstTrialOperator A, ");
                 sql.append("   count(distinct b.prtno) B    from lcgrpcont b where  b.appflag='8' ");
                 sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                 sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
                 sql.append("   group by b.FirstTrialOperator ");
                 sql.append("     union ");
                 sql.append("   select b.FirstTrialOperator A, ");
                 sql.append("   count(distinct b.prtno) B    from lbgrpcont b where  b.appflag='8' ");
                 sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LCgrpIssuePol) and b.makedate ");
                 sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
                 sql.append("   group by b.FirstTrialOperator ");
                 sql.append("       union ");
                 sql.append("   select b.FirstTrialOperator A, ");
                 sql.append("   count(distinct b.prtno) B    from lobgrpcont b where  b.appflag='8' ");
                 sql.append("  and  b.proposalgrpcontno not in (select proposalgrpcontno from LobgrpIssuePol) and b.makedate ");
                 sql.append("  between '"+StartDate+"' and '"+EndDate+"'   and substr(b.managecom,1,length('"+Managecom+"'))='"+ Managecom +"' "+FirstTrialOperator1+" ");
                 sql.append("   group by b.FirstTrialOperator ");
                 sql.append("   ) as L group by A ");
               sql.append("     ) as z group by U ");
                ssrs = tExeSQL.execSQL(sql.toString());
                if (ssrs != null) {
                    if (ssrs.getMaxRow() > 0) {
                        for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                            result.add(ssrs.getRowData(m));
                        }
                    }
                }
                ssrs = null;
                sql = new StringBuffer();
                System.out.println("处理完成第二个Sql语句~~~~~");

            }

        }


        ListTable tListTable = new ListTable();
        tListTable.setName("WorkEfficencyReport");
        String[] title = {"A", "B", "C", "D", "E", "F","G"};
        /** To MengWT ：tListTable将保存每一行的动态数据，其效果就是
         * ┎─┰─┰─┰─┰─┰─┰─┰─┰─┰─┒
         *   A1 B1  C1 D1  E1 F1  G1 H1  I1 J1
         *   A2 B2  C2 D2  E2 F2  G2 H2  I2 J2
         * ┖─┸─┸─┸─┸─┸─┸─┸─┸─┸─┚
         * 就是说 A - J 必须是一维的（一个数据）的数据
         * 不能出现A1--An是一个数据 B1--Bn是一个数组，
         * 这样会无法显示。
         *  */
        for (int i = 0; i < result.size(); i++) {
            tListTable.add((String[]) result.get(i));
        }

        XmlExport xml = new XmlExport(); //新建一个XmlExport的实例
        xml.createDocument("FirstTrialOperatorStatistic.vts", "printer"); //最好紧接着就初始化xml文档
        //生成-年-月-日格式的日期
        StrTool tSrtTool = new StrTool();
        String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() +
                         "月" +
                         tSrtTool.getDay() + "日";

        //WorkEfficiencyBL模版元素
        StartDate = StartDate.replace('-', '月');
        StartDate = StartDate.replaceFirst("月", "年") + "日";
        EndDate = EndDate.replace('-', '月');
        EndDate = EndDate.replaceFirst("月", "年") + "日";
        MakeDate = MakeDate.replace('-', '月');
        MakeDate = MakeDate.replaceFirst("月", "年") + "日";
        texttag.add("StartDate", StartDate);
        texttag.add("EndDate", EndDate);
        texttag.add("QYType_ch", QYType_ch);
        texttag.add("MakeDate", MakeDate);
        SSRS tSSRS_M = new SSRS();
        String sql_M = "select Name from ldcom where comcode = '" +
                       OperatorManagecom +
                       "'";
        System.out.println("zhuzhu" + sql_M);
        tExeSQL = new ExeSQL();
        tSSRS_M = tExeSQL.execSQL(sql_M);
        String temp_M[][] = tSSRS_M.getAllData();
        texttag.add("MngCom", temp_M[0][0]);

        if (texttag.size() > 0) {
            xml.addTextTag(texttag);
        }

        //保存信息
        xml.addListTable(tListTable, title);
        /** 添加完成将输出的结果保存到结果集中 */
        mResult.add(xml);
        mResult.clear();
        mResult.addElement(xml);

        return true;
    }

    public static void main(String[] args) {
        VData tVData = new VData();
        FirstTrialOperatorStatisticBL tFirstTrialOperatorStatisticBL = new
                FirstTrialOperatorStatisticBL();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        TransferData mTransferData = new TransferData();
        mTransferData.setNameAndValue("OperatorManagecom", "86");
        mTransferData.setNameAndValue("Managecom", "86");
        mTransferData.setNameAndValue("StartDate", "2005-1-1");
        mTransferData.setNameAndValue("EndDate", "2005-11-1");
        mTransferData.setNameAndValue("QYType", "1");
        tVData.add(mTransferData);
        tFirstTrialOperatorStatisticBL.submitData(tVData, "PRINT");
        VData vData = tFirstTrialOperatorStatisticBL.getResult();

    }


}
