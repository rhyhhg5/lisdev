package com.sinosoft.lis.f1print;

import java.io.InputStream;

import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.RSWrapper;

public class PrintClaimListBL {
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mManageCom = "";

    private VData mInputData = new VData();

    private String mOperate = "";

    private PubFun mPubFun = new PubFun();

    private ListTable mListTable = new ListTable();

    private TransferData mTransferData = new TransferData();

    private XmlExport mXmlExport = null;

    private String mSQL = "";

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mOperate = cOperate;
        mInputData = (VData) cInputData;
        if (mOperate.equals("")) {
            this.bulidErrorB("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidErrorB("submitData", "数据不完整");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(mInputData)) {
            return false;
        }
        System.out.println("BBBXXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
                           + this.mManageCom);

        // 进行数据查询
        if (!queryData()) {
            return false;
        }

        System.out.println("dayinchenggong1232121212121");

        return true;
    }

    private void bulidErrorB(String cFunction, String cErrorMsg) {

        CError tCError = new CError();

        tCError.moduleName = "LLSurveyBL";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;

        this.mErrors.addOneError(tCError);

    }

    /**
     * 取得传入的数据 如果没有传入管理机构和起止如期则查全部机构全年的信息
     *
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        // tCurrentDate = mPubFun.getCurrentDate();
        try {
            mGlobalInput.setSchema((GlobalInput) cInputData
                                   .getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            // 页面传入的数据 三个
            mSQL = (String) mTransferData.getValueByName("ClaimListSql");
            String tContNo = (String) mTransferData.getValueByName("ContNo");
            System.out.println("tContNo："+tContNo);
            //tContNo="";
            if("".equals(tContNo) || tContNo == null || "null".equals(tContNo)){
                this.mErrors.addOneError("请核实前台传入的保单号/团单号，此处不许为空，请返回上级页面重新操作！");
                return false;
            }

        } catch (Exception ex) {
            this.mErrors.addOneError("");
            return false;
        }
        return true;
    }

    private boolean queryData() {
        TextTag tTextTag = new TextTag();
        mXmlExport = new XmlExport();
        // 设置模版名称
        mXmlExport.createDocument("PrintClaimList.vts", "printer");

        String tMakeDate = "";
        tMakeDate = mPubFun.getCurrentDate();

        System.out.print("dayin252");

        System.out.println("1212121" + tMakeDate);

        mXmlExport.addTextTag(tTextTag);
        String[] title = {"", "", "", "", "", "", "", "", "", "" ,""};
        mListTable.setName("List");
        if (!getDataList()) {
            return false;
        }
        if (mListTable == null || mListTable.equals("")) {
            bulidErrorB("getDataList", "没有符合条件的信息!");
            return false;
        }

        System.out.println("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.println("121");
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();
        mResult.addElement(mXmlExport);

        return true;
    }

    private boolean getDataList() {
        System.out.println("SQL："
                           + mSQL);
        ExeSQL tExeSQL = new ExeSQL();

        RSWrapper rsWrapper = new RSWrapper();
        if (!rsWrapper.prepareData(null, mSQL)) {
            System.out.println("数据准备失败! ");
            return false;
        }

        try {
            SSRS tSSRS = new SSRS();
            int j = 1;
            do {
                tSSRS = rsWrapper.getSSRS();
                if (tSSRS != null || tSSRS.MaxRow > 0) {

                    for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                        String ListInfo[] = new String[11];

                        ListInfo[0] = String.valueOf(j++); ;
                        ListInfo[1] = tSSRS.GetText(i, 1);
                        ListInfo[2] = tSSRS.GetText(i, 2);
                        ListInfo[3] = tSSRS.GetText(i, 3);
                        ListInfo[4] = tSSRS.GetText(i, 4);
                        ListInfo[5] = tSSRS.GetText(i, 5);
                        ListInfo[6] = tSSRS.GetText(i, 6);
                        ListInfo[7] = tSSRS.GetText(i, 7);
                        ListInfo[8] = tSSRS.GetText(i, 8);
                        ListInfo[9] = tSSRS.GetText(i, 9);
                        ListInfo[10] = "";

                        mListTable.add(ListInfo);
                    }
                }
            } while (tSSRS != null && tSSRS.MaxRow > 0);
        } catch (Exception ex) {

        } finally {
            rsWrapper.close();
        }
        System.out.println("heoewjowehfdihieeehh~~~~~~~~~~~~~~~~~~~~~~~~~~"
                           + mSQL);
        return true;
    }

    /**
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }

    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream() {
        return mXmlExport.getInputStream();
    }
}
