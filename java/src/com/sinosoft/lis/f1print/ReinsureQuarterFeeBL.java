package com.sinosoft.lis.f1print;

/**
 * <p>Title: ReinsureQuarterTabBL</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author MN
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class ReinsureQuarterFeeBL {
    public ReinsureQuarterFeeBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局变量 */
    private String mStartDate = "";
    private String mEndDate = "";
    private String mFeeNo = "";
    private String mReComCode = "";
    private String mReComName = "";
    private String mReContName = "";
    private String mRecontCode = "";
    private String mOperationYear = "";
    private String mReinsureFeeLoan = "";
    private String mChargeLend = "";
    private String mClaimBackFee = "";
    private String mBalanceLend = "";
    private String mSumLead = "";
    private String mSumLoan = "";
    private XmlExport mXmlExport = null;
    private String currentDate = PubFun.getCurrentDate();

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 进行数据查询
        if (!queryData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        return true;
    }


    /**
     * 得到表示数据列表
     * @return boolean
     */
    private boolean queryData() {
        String tSQL = null;
        ExeSQL tExeSQL = new ExeSQL();

        String tRiskCalSort = "";
        String tCessPrem = "";
        String tReProcFee = "";
        String tEdorBackFee = "";
        String tEdorProcFee = "";

        tSQL = "select  recomname from lrrecominfo where recomcode='" +
               mReComCode + "'";
        mReComName = tExeSQL.getOneValue(tSQL);

        tExeSQL = new ExeSQL();
        tSQL = "select recontname from lrcontinfo where recontcode='" +
               mRecontCode + "'";
        mReContName = tExeSQL.getOneValue(tSQL);

        tExeSQL = new ExeSQL();
        tSQL = "select distinct Riskcalsort from lrpol where recontcode = '" +
               mRecontCode + "'";
        tRiskCalSort = tExeSQL.getOneValue(tSQL);
        System.out.println("riskcalsort");

        if (tRiskCalSort.equals("1")) {
            tSQL = "select sum(d.CessPrem),sum(d.ReProcFee) from lrpol a, lrpoldetail c, lrpolresult d"
                   + " where a.PolNo = c.Polno and a.PolNo = d.Polno and a.RecontCode = d.RecontCode "
                   + " and a.ReNewCount = c.ReNewCount and a.ReNewCount = d.ReNewCount and c.PayCount = d.PayCount"
                   + " and a.RecontCode = '" + mRecontCode + "'"
                   + " and((a.GetDataDate between '" + mStartDate +
                   "' and '" + mEndDate + "' and c.GetDataDate  <= '" +
                   mEndDate + "') or (a.GetDataDate  < '" + mStartDate +
                   "' and c.GetDataDate between '" + mStartDate +
                   "' and '" + mEndDate + "')) with ur";
        } else {
            tSQL =
                    "select sum(d.CessPrem),sum(d.ReProcFee) from lrpol a,lrpolresult d"
                    + " where a.PolNo = d.Polno and a.RecontCode = d.RecontCode and a.ReNewCount = d.ReNewCount"
                    + " and a.RecontCode = '" + mRecontCode + "'"
                    + " and a.GetDataDate between '" +
                    mStartDate +
                    "' and '" + mEndDate + "' with ur";
        }

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println(tSQL);
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(tSQL);
        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        if (tSSRS.GetText(1, 1) == null || tSSRS.GetText(1, 1) == "" ||
            tSSRS.GetText(1, 1).equals("null")) {
            tCessPrem = "0";
        } else {
            tCessPrem = tSSRS.GetText(1, 1);
        }
        if (tSSRS.GetText(1, 2) == null || tSSRS.GetText(1, 2) == "" ||
            tSSRS.GetText(1, 2).equals("null")) {
            tReProcFee = "0";
        } else {
            tReProcFee = tSSRS.GetText(1, 2);
        }
        tExeSQL = new ExeSQL();
        tSSRS = new SSRS();

        tSQL = "select sum(e.EdorBackFee), sum(e.EdorProcFee) from LRPolEdor c, LRPolEdorResult e" +
               " where c.polno = e.Polno and c.RecontCode = e.RecontCode and c.RecontCode = '" +
               mRecontCode + "' " + " and c.GetDataDate between '" + mStartDate +
               "' and '" + mEndDate + "' ";

        System.out.println(tSQL);
        tSSRS = tExeSQL.execSQL(tSQL);

        System.out.println(tSSRS.GetText(1, 1));
        System.out.println(tSSRS.GetText(1, 2));

        if (tSSRS.GetText(1, 1) == null || tSSRS.GetText(1, 1) == "" ||
            tSSRS.GetText(1, 1).equals("null")) {
            tEdorBackFee = "0";
        } else {
            tEdorBackFee = tSSRS.GetText(1, 1);
        }
        if (tSSRS.GetText(1, 2) == null || tSSRS.GetText(1, 2) == "" ||
            tSSRS.GetText(1, 2).equals("null")) {
            tEdorProcFee = "0";
        } else {
            tEdorProcFee = tSSRS.GetText(1, 2);
        }
//      这里保全摊回，保全摊回手续费，系统中存储的都是负数，所以改为加号. modified by huxl @ 20080829
        mReinsureFeeLoan = Double.toString(Double.parseDouble(tCessPrem) +
                                           Double.parseDouble(tEdorBackFee));
        mChargeLend = Double.toString(Double.parseDouble(tReProcFee) +
                                      Double.parseDouble(tEdorProcFee));

        tExeSQL = new ExeSQL();

        tSQL = "select sum(e.ClaimBackFee) FROM LRPolClm c, LRPolClmResult e" +
               " where c.PolNo = e.Polno and c.RecontCode = e.RecontCode and c.ClmNo = e.ClmNo and e.RecontCode = '" +
               mRecontCode + "' and c.GetDataDate between '" + mStartDate +
               "' and '" + mEndDate + "' ";

        if (tExeSQL.getOneValue(tSQL) == null ||
            tExeSQL.getOneValue(tSQL) == "" ||
            tExeSQL.getOneValue(tSQL).equals("null")) {
            mClaimBackFee = "0";
        } else {
            mClaimBackFee = tExeSQL.getOneValue(tSQL);
        }
        System.out.println(mClaimBackFee);

        mBalanceLend = Double.toString(Double.parseDouble(mReinsureFeeLoan) -
                                       Double.parseDouble(mChargeLend) -
                                       Double.parseDouble(mClaimBackFee));
        mSumLoan = Double.toString(Double.parseDouble(mReinsureFeeLoan));
        mSumLead = Double.toString(Double.parseDouble(mChargeLend) +
                                   Double.parseDouble(mClaimBackFee) +
                                   Double.parseDouble(mBalanceLend));

        return true;
    }


    /**
     * 进行数据查询
     * @return boolean
     */
    private boolean dealData() {
        TextTag tTextTag = new TextTag();
        mXmlExport = new XmlExport();
        //设置模版名称
        mXmlExport.createDocument("ReinsureQuarterTab.vts", "printer");
        System.out.print("dayin252");
        tTextTag.add("FeeNo", mFeeNo);
        tTextTag.add("RecontCode", mRecontCode);
        tTextTag.add("MakeDate", currentDate);
        tTextTag.add("ManageComName", mReComName);
        tTextTag.add("OperationYear", mOperationYear);
        tTextTag.add("StartDate", mStartDate);
        tTextTag.add("EndDate", mEndDate);
        tTextTag.add("ReContName", mReContName);
        tTextTag.add("ReinsureFeeLoan", mReinsureFeeLoan);
        tTextTag.add("ChargeLend", mChargeLend);
        tTextTag.add("ClaimBackFee", mClaimBackFee);
        tTextTag.add("BalanceLend", mBalanceLend);
        tTextTag.add("SumLoan", mSumLoan);
        tTextTag.add("SumLead", mSumLead);
        if (tTextTag.size() < 1) {
            return false;
        }
        mXmlExport.addTextTag(tTextTag);
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();
        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 取得传入的数据
     * @param pmInputData VData
     * @return boolean
     */
    private boolean getInputData(VData pmInputData) {
        //全局变量
        mStartDate = (String) pmInputData.get(0);
        mEndDate = (String) pmInputData.get(1);
        mFeeNo = (String) pmInputData.get(2);
        mRecontCode = (String) pmInputData.get(3);
        mReComCode = (String) pmInputData.get(4);
        mOperationYear = mStartDate.substring(0, 4);
        return true;
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "ReinsureQuarterTab";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 取得返回处理过的结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    private void jbInit() throws Exception {
    }
}
