/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import utils.system;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CertPrintTable;
import com.cbsws.obj.LCCustomerImpartTable;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.midplat.common.DateUtil;
//import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;

/*
 * <p>ClassName: LCContF1PUI </p>
 * <p>Description: LCContF1PUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-11-04
 */
public class YBKPrintSendUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量

    public YBKPrintSendUI() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData vData) {
    	
	    YBKPrintSendBL tYBKPrintSendBL = new YBKPrintSendBL();
	
	    if (!tYBKPrintSendBL.submitData(vData)) {
	    	
	    	this.mErrors.copyAllErrors(tYBKPrintSendBL.mErrors);
			CError tError = new CError();
			tError.moduleName = "YBKPrintSendUI";
			tError.functionName = "submitDat";
			tError.errorMessage = "BL类处理失败!";
			this.mErrors.addOneError(tError);
	        return false;
	    }
		return true;
	    
    }
    
    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCContF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
