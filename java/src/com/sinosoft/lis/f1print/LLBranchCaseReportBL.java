package com.sinosoft.lis.f1print;

/**
 * <p>Title: LLBranchCaseReportBL</p>
 * <p>Description: 机构理赔状况报表</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author MN
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

public class LLBranchCaseReportBL{
    public LLBranchCaseReportBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mStartDate = "";
    private String mEndDate = "";
    private String mManageCom = "";
    private String mManageComName = "";
    private String moperator = "";
    private long mStartDays = 0;
    private long mEndDays = 0;
    private String[][] mShowDataList = null;
    private XmlExport mXmlExport = null;
    private String[] mDataList = null;
    private ListTable mListTable = new ListTable();
    private String currentDate = PubFun.getCurrentDate();
	private String mFileNameB = "";
	private String mMakeDate = "";
	private String mOperator = "";
	private TransferData mTransferData = new TransferData();
	/** 统计类型 */
	private String mStatsType = "";
    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 进行数据查询
        if (!queryData()) {
            return false;
        }else{
			TransferData tTransferData= new TransferData();
			tTransferData.setNameAndValue("tFileNameB",mFileNameB );
			tTransferData.setNameAndValue("tMakeDate", mMakeDate);
			tTransferData.setNameAndValue("tOperator", mOperator);
			LLPrintSave tLLPrintSave = new LLPrintSave();
			VData tVData = new VData();
			tVData.addElement(tTransferData);
			if(!tLLPrintSave.submitData(tVData,"")){
				System.out.println("LLPrintSave is fault!");
				return false;
			     }
		}

        return true;
    }


    /**
     * 得到表示数据列表
     * @return boolean
     */
    private boolean getDataList() {
        double tRCase = 0;
        double tCCase = 0;
        double tRECase = 0;
        double tECase = 0;
        double tEPay = 0;
        double tETime = 0;
        double tSumETime = 0;
        double tSumPeoples = 0;
       
        double tSumERPay = 0;
        double tSumPrem = 0;
        double tSumERCase = 0;
        double tREPay = 0;
        String tSQL = "";
        DecimalFormat tDF = new DecimalFormat("0.##");
        DecimalFormat tDF1 = new DecimalFormat("0.#");
 
        // 1、得到全部已开业的机构
        if (mManageCom.length() == 2) {
            tSQL = "SELECT comcode,name FROM ldcom WHERE sign='1' AND length(trim(comcode))=4 AND comcode like '" +
                   mManageCom + "%' ORDER BY comcode";
        //cbs00058484 修改，为避免重复，如果是用4位机构登陆查询，只需要带出该4位机构下的所有8位机构的数据即可。    
        } else if (mManageCom.length() == 4) {
            tSQL = "SELECT comcode,name FROM ldcom WHERE sign='1' AND length(trim(comcode))=8 AND comcode like '" +
                   mManageCom + "%' ORDER BY comcode";
            System.out.println("ZZZZZZZ");
        } else {
            tSQL =
                    "SELECT comcode,name FROM ldcom WHERE sign='1' AND comcode LIKE '" +
                    mManageCom + "%' ORDER BY comcode";
        }
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tSSRS.getMaxRow() <= 0) {
            buildError("queryData", "没有附和条件的机构信息！");
            return false;
        }

        // 2、查询需要表示的数据
        String tManageCom = "";
        String tComName = "";

        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {// 循环机构进行统计
        	double tERPay = 0;
        	double tPrem = 0;
        	double tERCase = 0;
        	double tPeoples = 0;
            String Info[] = new String[10];
            tManageCom = tSSRS.GetText(i, 1);
            tComName = tSSRS.GetText(i, 2);

            Info[0] = tComName;
            Info[1] = getRCase(tManageCom);
            Info[2] = getCCase(tManageCom);
            Info[3] = getRECase(tManageCom);
            Info[4] = getREPay(tManageCom);
            Info[5] = getECase(tManageCom);
            Info[6] = getEPay(tManageCom);
            tETime = getETime(tManageCom);
            if (Info[5].equals("0")) {
                Info[7] = "0";
            } else {
                Info[7] = tDF1.format(tETime / Double.parseDouble(Info[5]));
            }
            tPeoples = getPeoples(tManageCom);
            if (tPeoples == 0) {
                Info[8] = "0";
            } else {
                tERCase = getERCase(tManageCom);
                Info[8] = tDF.format(tERCase / tPeoples * 100);
            }
            tPrem = getTemPrem(tManageCom);
            if (tPrem == 0) {
                Info[9] = "0";
            } else {
                tERPay = getERPay(tManageCom);
                Info[9] = tDF.format(tERPay / tPrem * 100);
            }
            System.out.println("tManageCom:"+tManageCom+" tPeoples:"+tPeoples+" tERCase:"+tERCase+" tPrem:"+tPrem+" tERPay"+tERPay);
            tRCase += Double.parseDouble(Info[1]);
            tCCase += Double.parseDouble(Info[2]);
            tRECase += Double.parseDouble(Info[3]);
            tREPay += Double.parseDouble(Info[4]);
            tECase += Double.parseDouble(Info[5]);
            tEPay += Double.parseDouble(Info[6]);
            tSumETime += tETime;
            tSumPeoples += tPeoples;
            System.out.println("tSumPeoples="+tSumPeoples);
            tSumERCase += tERCase;
            System.out.println("tSumERCase="+tSumERCase);
            tSumERPay += tERPay;
            System.out.println("tSumERPay="+tSumERPay);
            tSumPrem += tPrem;
            System.out.println("tSumPrem="+tSumPrem);
            mListTable.add(Info);
        }

        //合计所有公司
        String Info[] = new String[10];

        Info[0] = "合计";
        Info[1] = tDF.format(tRCase);
        Info[2] = tDF.format(tCCase);
        Info[3] = tDF.format(tRECase);
        Info[4] = tDF.format(tREPay);
        Info[5] = tDF.format(tECase);
        Info[6] = tDF.format(tEPay);
        if (Info[5].equals("0")) {
            Info[7] = "0";
        } else {
            Info[7] = tDF1.format(tSumETime / Double.parseDouble(Info[5]));
        }
        if (tSumPeoples == 0) {
            Info[8] = "0";
        } else {
            Info[8] = tDF.format(tSumERCase / tSumPeoples * 100);
        }
        if (tSumPrem == 0) {
            Info[9] = "0";
        } else {
            Info[9] = tDF.format(tSumERPay / tSumPrem * 100);
        }
        mListTable.add(Info);

        return true;
    }

    /**
     * 申请件数
     * @param cManageCom String
     * @return String
     */
    private String getRCase(String cManageCom) {
    	String tSql = "";
    	if("1".equals(mStatsType)){//商团案件数
    		tSql = "SELECT COUNT(a.caseno) FROM llcase a,llregister b WHERE a.mngcom LIKE '"
                + cManageCom +
                "%' AND a.rgtdate BETWEEN '" + mStartDate + "' AND '" +
                mEndDate + "' " +
                " and a.rgtno=b.rgtno and CHECKGRPCONT(b.rgtobjno) = 'N' WITH UR ";
    	}else if("2".equals(mStatsType)){//社保案件数
    		tSql = "SELECT COUNT(a.caseno) FROM llcase a,llregister b WHERE a.mngcom LIKE '"
                + cManageCom +
                "%' AND a.rgtdate BETWEEN '" + mStartDate + "' AND '" +
                mEndDate + "' " +
                "and a.rgtno=b.rgtno " +
                "and CHECKGRPCONT(b.rgtobjno) = 'Y' WITH UR ";
    	}else{//全部
    		tSql = "SELECT COUNT(caseno) FROM llcase WHERE mngcom LIKE '"
                + cManageCom +
                "%' AND rgtdate BETWEEN '" + mStartDate + "' AND '" +
                mEndDate + "' WITH UR ";
    	}
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 撤销件数
     * @param cManageCom String
     * @return String
     */
    private String getCCase(String cManageCom) {
        String tSql = "";
        if("1".equals(mStatsType)){
        	tSql = "SELECT COUNT(a.caseno) FROM llcase a,llregister b WHERE a.rgtstate='14' AND a.mngcom LIKE '"
                + cManageCom +
                "%' AND a.cancledate BETWEEN '" + mStartDate + "' AND '" +
                mEndDate + "' " +
                " and a.rgtno=b.rgtno and CHECKGRPCONT(b.rgtobjno) = 'N' WITH UR ";
        }else if("2".equals(mStatsType)){
        	tSql = "SELECT COUNT(a.caseno) FROM llcase a,llregister b WHERE a.rgtstate='14' AND a.mngcom LIKE '"
                + cManageCom +
                "%' AND a.cancledate BETWEEN '" + mStartDate + "' AND '" +
                mEndDate + "' " +
                "and a.rgtno=b.rgtno  " +
                "and CHECKGRPCONT(b.rgtobjno) = 'Y' WITH UR ";
        }else{
        	tSql = "SELECT COUNT(caseno) FROM llcase WHERE rgtstate='14' AND mngcom LIKE '"
                + cManageCom +
                "%' AND cancledate BETWEEN '" + mStartDate + "' AND '" +
                mEndDate + "' WITH UR ";
        }
        
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 申请已结件数
     * @param cManageCom String
     * @return String
     */
    private String getRECase(String cManageCom) {
        String tSql = "";
        if("1".equals(mStatsType)){
            tSql = "SELECT COUNT(distinct a.caseno) FROM llcase a,llclaimdetail b WHERE a.rgtstate IN ('09','11','12') AND a.mngcom LIKE '"
                + cManageCom + "%' AND a.rgtdate BETWEEN '" + mStartDate +
                "' AND '" + mEndDate + "' AND a.endcasedate <='" + mEndDate +
                "' and a.caseno=b.caseno and CHECKGRPCONT(b.grpcontno) = 'N' WITH UR ";
        }else if("2".equals(mStatsType)){
        	tSql = "SELECT COUNT(distinct a.caseno) FROM llcase a,llclaimdetail b WHERE a.rgtstate IN ('09','11','12') AND a.mngcom LIKE '"
                + cManageCom + "%' AND a.rgtdate BETWEEN '" + mStartDate +
                "' AND '" + mEndDate + "' AND a.endcasedate <='" + mEndDate +
                "' and a.caseno=b.caseno  " +
                "and CHECKGRPCONT(b.grpcontno) = 'Y' WITH UR ";
        }else{
        	tSql = "SELECT COUNT(caseno) FROM llcase WHERE rgtstate IN ('09','11','12') AND mngcom LIKE '"
                + cManageCom + "%' AND rgtdate BETWEEN '" + mStartDate +
                "' AND '" + mEndDate + "' AND endcasedate <='" + mEndDate +
                "' WITH UR ";
        }
        
        double tSumPrem = 0.0;
        String tPrem = "";
        ExeSQL tExeSQL = new ExeSQL();

        tPrem = tExeSQL.getOneValue(tSql);
        if (tPrem.equals("null") || tPrem == null || tPrem.equals("")) {
            tSumPrem = 0;
        } else {
            tSumPrem = Double.parseDouble(tPrem);
        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tSumPrem);
        return tRturn;

    }

    /**
     * 申请已结赔款
     * @param cManageCom String
     * @return String
     */
    private String getREPay(String cManageCom) {
        String tSql = "";
        if("1".equals(mStatsType)){
        	tSql = "SELECT SUM(a.RealPay) FROM llclaimdetail a,llcase b WHERE a.caseno=b.caseno AND b.rgtstate IN ('09','11','12') AND b.mngcom LIKE '" +
            	cManageCom + "%' AND b.rgtdate BETWEEN '" + mStartDate +
            	"' AND '" + mEndDate + "' AND b.endcasedate <='" +
            	mEndDate + "' " +
            	"and CHECKGRPCONT(a.grpcontno) = 'N' WITH UR ";
        }else if("2".equals(mStatsType)){
        	tSql = "SELECT SUM(a.RealPay) FROM llclaimdetail a,llcase b WHERE a.caseno=b.caseno AND b.rgtstate IN ('09','11','12') AND b.mngcom LIKE '" +
        		cManageCom + "%' AND b.rgtdate BETWEEN '" + mStartDate +
        		"' AND '" + mEndDate + "' AND b.endcasedate <='" +
        		mEndDate + "'  " +
        		"and CHECKGRPCONT(a.grpcontno) = 'Y' WITH UR ";
        }else{
        	tSql = "SELECT SUM(a.RealPay) FROM llclaimdetail a,llcase b WHERE a.caseno=b.caseno AND b.rgtstate IN ('09','11','12') AND b.mngcom LIKE '" +
            	cManageCom + "%' AND b.rgtdate BETWEEN '" + mStartDate +
            	"' AND '" + mEndDate + "' AND b.endcasedate <='" +
            	mEndDate + "' WITH UR";
        }
        
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0.0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 结案件数
     * @param cManageCom String
     * @return String
     */
    private String getECase(String cManageCom) {
        String tSql = "";
        if("1".equals(mStatsType)){
        	tSql = "SELECT COUNT(distinct a.caseno) FROM llcase a,llclaimdetail b WHERE a.rgtstate IN ('09','11','12') AND a.mngcom LIKE '" +
        		cManageCom + "%' AND a.endcasedate BETWEEN '" + mStartDate +
        		"' AND '" + mEndDate + "' " +
        		" and a.caseno=b.caseno and CHECKGRPCONT(b.grpcontno) = 'N' WITH UR ";
        }else if("2".equals(mStatsType)){
        	tSql = "SELECT COUNT( distinct a.caseno) FROM llcase a,llclaimdetail b WHERE a.rgtstate IN ('09','11','12') AND a.mngcom LIKE '" +
    			cManageCom + "%' AND a.endcasedate BETWEEN '" + mStartDate +
    			"' AND '" + mEndDate + "' and a.caseno=b.caseno " +
    			"and CHECKGRPCONT(b.grpcontno) = 'Y' WITH UR ";
        }else{
        	tSql = "SELECT COUNT(caseno) FROM llcase WHERE  rgtstate IN ('09','11','12') AND mngcom LIKE '" +
        		cManageCom + "%' AND endcasedate BETWEEN '" + mStartDate +
        		"' AND '" + mEndDate + "' WITH UR  ";
        }
        
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0.0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 结案赔款
     * @param cManageCom String
     * @return String
     */

    private String getEPay(String cManageCom) {
        String tSql = "";
        if("1".equals(mStatsType)){
            tSql = "SELECT SUM(a.realpay) FROM llclaimdetail a,llcase b WHERE a.caseno=b.caseno AND b.rgtstate IN ('09','11','12')  AND b.mngcom LIKE '" +
        		cManageCom +
        		"%' AND b.endcasedate BETWEEN '" + mStartDate + "' AND '" +
        		mEndDate + "' " +
        		"and CHECKGRPCONT(a.grpcontno) = 'N' WITH UR ";
        }else if("2".equals(mStatsType)){
        	tSql = "SELECT SUM(a.realpay) FROM llclaimdetail a,llcase b WHERE a.caseno=b.caseno AND b.rgtstate IN ('09','11','12')  AND b.mngcom LIKE '" +
     			cManageCom +
     			"%' AND b.endcasedate BETWEEN '" + mStartDate + "' AND '" +
     			mEndDate + "' " +
     			"and CHECKGRPCONT(a.grpcontno) = 'Y' WITH UR ";
        }else{
        	tSql = "SELECT SUM(a.realpay) FROM llclaimdetail a,llcase b WHERE a.caseno=b.caseno AND b.rgtstate IN ('09','11','12')  AND b.mngcom LIKE '" +
        		cManageCom +
        		"%' AND b.endcasedate BETWEEN '" + mStartDate + "' AND '" +
        		mEndDate + "' WITH UR ";
        }
        
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0.0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }

        DecimalFormat tDF = new DecimalFormat("0.##");
        String tRturn = tDF.format(tValue);
        return tRturn;
    }

    /**
     * 结案时间
     * @param cManageCom String
     * @return Double
     */
    private double getETime(String cManageCom) {
        String tSql = "";
        if("1".equals(mStatsType)){
        	tSql = "SELECT SUM(TO_DATE(a.endcasedate)-TO_DATE(a.rgtdate)+1) FROM llcase a WHERE a.rgtstate IN ('09','11','12') AND a.mngcom LIKE '" +
        		cManageCom +
        		"%' AND a.endcasedate BETWEEN '" + mStartDate + "' AND '" +
        		mEndDate + "' " +
        		" and exists(select 1 from llclaimdetail b where b.caseno=a.caseno and CHECKGRPCONT(b.grpcontno) = 'N')   WITH UR ";
        }else if("2".equals(mStatsType)){
        	tSql = "SELECT SUM(TO_DATE(a.endcasedate)-TO_DATE(a.rgtdate)+1) FROM llcase a WHERE a.rgtstate IN ('09','11','12') AND a.mngcom LIKE '" +
        		cManageCom +
        		"%' AND a.endcasedate BETWEEN '" + mStartDate + "' AND '" +
        		mEndDate + "'   " +
        		" and exists(select 1 from llclaimdetail b where b.caseno=a.caseno and CHECKGRPCONT(b.grpcontno) = 'Y')   WITH UR ";
        }else{
            tSql = "SELECT SUM(TO_DATE(endcasedate)-TO_DATE(rgtdate)+1) FROM llcase WHERE rgtstate IN ('09','11','12') AND mngcom LIKE '" +
        		cManageCom +
        		"%' AND endcasedate BETWEEN '" + mStartDate + "' AND '" +
        		mEndDate + "' WITH UR";
        }

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0.0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        return tValue;
    }

    /**
     * 申请件数（计算发生率）
     * @param cManageCom String
     * @return String
     */
    private double getERCase(String cManageCom) {
        String tSql = "";
        if("1".equals(mStatsType)){
        	tSql = "SELECT COUNT(a.caseno) FROM llcase a WHERE a.rgttype='1' AND a.rgtstate IN ('09','11','12') AND a.mngcom LIKE '" +
        		cManageCom + "%' AND a.endcasedate BETWEEN '" +
        		mStartDate +
        		"' AND '" + mEndDate + "' AND EXISTS (SELECT 1 FROM llclaimdetail WHERE caseno=a.caseno and CHECKGRPCONT(grpcontno) = 'N' AND riskcode NOT IN (SELECT riskcode FROM lmriskapp WHERE riskperiod='L' OR risktype3='7' AND risktype <>'M') ) WITH UR  ";

        }else if("2".equals(mStatsType)){
        	tSql = "SELECT COUNT(a.caseno) FROM llcase a WHERE a.rgttype='1' AND a.rgtstate IN ('09','11','12') AND a.mngcom LIKE '" +
        		cManageCom + "%' AND a.endcasedate BETWEEN '" +
        		mStartDate +
        		"' AND '" + mEndDate + "' AND EXISTS (SELECT 1 FROM llclaimdetail WHERE caseno=a.caseno and CHECKGRPCONT(grpcontno) = 'Y' AND riskcode NOT IN (SELECT riskcode FROM lmriskapp WHERE riskperiod='L' OR risktype3='7' AND risktype <>'M') ) WITH UR  ";

        }else{
        	tSql = "SELECT COUNT(a.caseno) FROM llcase a WHERE a.rgttype='1' AND a.rgtstate IN ('09','11','12') AND a.mngcom LIKE '" +
        		cManageCom + "%' AND a.endcasedate BETWEEN '" +
        		mStartDate +
        		"' AND '" + mEndDate + "' AND EXISTS (SELECT 1 FROM llclaimdetail WHERE caseno=a.caseno AND riskcode NOT IN (SELECT riskcode FROM lmriskapp WHERE riskperiod='L' OR risktype3='7' AND risktype <>'M') ) WITH UR  ";

        }
        
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0.0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        System.out.println(tSql+"/t"+tValue);
        return tValue;
    }


    /**
     * 被保人数
     * @param cManageCom String
     * @return String
     */
    private double getPeoples(String cManageCom) {
        String tSql = "";
        if("1".equals(mStatsType)){
        	tSql = "SELECT COUNT(distinct insuredno) FROM lcpol WHERE appflag='1'  AND managecom LIKE '" +
        		cManageCom + "%' AND enddate>='" + mStartDate +
        		"' AND cvalidate<='" + mEndDate +
        		"' and CHECKGRPCONT(grpcontno) = 'N' AND riskcode NOT IN (SELECT riskcode FROM lmriskapp WHERE riskperiod='L' OR risktype3='7' AND risktype <>'M') WITH UR";

        }else if("2".equals(mStatsType)){
        	tSql = "SELECT COUNT(distinct insuredno) FROM lcpol WHERE appflag='1'  AND managecom LIKE '" +
    			cManageCom + "%' AND enddate>='" + mStartDate +
    			"' AND cvalidate<='" + mEndDate +
    			"' and CHECKGRPCONT(grpcontno) = 'Y' AND riskcode NOT IN (SELECT riskcode FROM lmriskapp WHERE riskperiod='L' OR risktype3='7' AND risktype <>'M') WITH UR";

        }else{
        	tSql = "SELECT COUNT(distinct insuredno) FROM lcpol WHERE appflag='1'  AND managecom LIKE '" +
        		cManageCom + "%' AND enddate>='" + mStartDate +
        		"' AND cvalidate<='" + mEndDate +
        		"' AND riskcode NOT IN (SELECT riskcode FROM lmriskapp WHERE riskperiod='L' OR risktype3='7' AND risktype <>'M') WITH UR";

        }
        
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);
        System.out.println(tSql);
        double tValue = 0.0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        System.out.println(tSql+"/t"+tValue);
        return tValue;
    }

    /**
     * 结案赔款(计算赔付率)
     * @param cManageCom String
     * @return String
     */
    private double getERPay(String cManageCom) {
        String tSql = "";
        if("1".equals(mStatsType)){
        	tSql = "SELECT SUM(realpay) FROM llclaimdetail a,llcase b WHERE a.caseno=b.caseno AND b.rgtstate IN ('09','11','12')  AND b.mngcom LIKE '" +
        		cManageCom +
        		"%' AND b.endcasedate BETWEEN '" + mStartDate + "' AND '" +
        		mEndDate + "' AND a.riskcode NOT IN (SELECT riskcode FROM lmriskapp WHERE riskperiod='L' OR risktype3='7' AND risktype <>'M')  " +
        		" and CHECKGRPCONT(a.grpcontno) = 'N' WITH UR ";

        }else if("2".equals(mStatsType)){
        	tSql = "SELECT SUM(realpay) FROM llclaimdetail a,llcase b WHERE a.caseno=b.caseno AND b.rgtstate IN ('09','11','12')  AND b.mngcom LIKE '" +
    			cManageCom +
    			"%' AND b.endcasedate BETWEEN '" + mStartDate + "' AND '" +
    			mEndDate + "' AND a.riskcode NOT IN (SELECT riskcode FROM lmriskapp WHERE riskperiod='L' OR risktype3='7' AND risktype <>'M')  " +
    			" and CHECKGRPCONT(a.grpcontno) = 'Y' WITH UR ";
        }else{
        	tSql = "SELECT SUM(realpay) FROM llclaimdetail a,llcase b WHERE a.caseno=b.caseno AND b.rgtstate IN ('09','11','12')  AND b.mngcom LIKE '" +
        		cManageCom +
        		"%' AND b.endcasedate BETWEEN '" + mStartDate + "' AND '" +
        		mEndDate + "' AND a.riskcode NOT IN (SELECT riskcode FROM lmriskapp WHERE riskperiod='L' OR risktype3='7' AND risktype <>'M')  WITH UR ";

        }
        
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0.0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        System.out.println(tSql+"/t"+tValue);
        return tValue;
    }

    /**
     * 经过保费
     * @param cManageCom String
     * @return String
     */
    private double getTemPrem(String cManageCom) {
        String tSql = null;
        ExeSQL tExeSQL = new ExeSQL();
        double tSumPrem = 0.0;
        SSRS tSSRS = new SSRS();
        String tPartSql = "";
        if("1".equals(mStatsType)){
        	tPartSql = " and CHECKGRPCONT(grpcontno) = 'N' ";
        }else if("2".equals(mStatsType)){
        	tPartSql = " and CHECKGRPCONT(grpcontno) = 'Y' ";
        }else{
        	tPartSql = "";
        }
        
        tSql = "select sum(prem) from (SELECT  "
               + " case when days(cvalidate)>" + mStartDays
               + " then  "
               + " case when days(enddate)<" + mEndDays
               + " then "
               + " case when payintv<=0"
               + " then  prem / (days(enddate) - days(cvalidate) + 1) * (days(enddate) - days(cvalidate) + 1)"
               +
               " else  prem * 12 /  payintv / 365* (days(enddate) - days(cvalidate) + 1)"
               + " end "
               + " else "
               + " case when payintv<=0"
               + " then  prem / (days(enddate) - days(cvalidate) + 1) * (" +
               mEndDays + " - days(cvalidate) + 1)"
               + " else  prem * 12 / payintv / 365* (" + mEndDays +
               " - days(cvalidate) + 1)"
               + " end "
               + " end "
               + " else "
               + " case when days(enddate)<" + mEndDays
               + " then "
               + " case when payintv<=0"
               +
               " then  prem / (days(enddate) - days(cvalidate) + 1) * (days(enddate) - " +
               mStartDays + " + 1)"
               + " else  prem * 12 / payintv / 365* (days(enddate) - " +
               mStartDays + " + 1)"
               + " end "
               + " else "
               + " case when payintv<=0"
               + " then  prem / (days(enddate) - days(cvalidate) + 1) * (" +
               mEndDays + " - " + mStartDays + " + 1)"
               + " else  prem * 12 / payintv / 365* (" + mEndDays + " - " +
               mStartDays + " + 1)"
               + " end "
               + " end "
               + " end prem "

               + " FROM lcpol"
               +
               " WHERE appflag='1' and managecom LIKE '"
               + cManageCom
               + "%' "
               + " AND cvalidate <='"
               + mEndDate
               + "'"
               + " AND enddate >='" + mStartDate + "' AND riskcode NOT IN (SELECT riskcode FROM lmriskapp WHERE riskperiod='L' OR risktype3='7' AND risktype <>'M') " 
               + tPartSql 
               +") t WITH UR";
        
        System.out.println("经过保费1"+tSql);
        tSSRS = tExeSQL.execSQL(tSql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            if (tSSRS.GetText(i, 1).equals("null")) {
                tSumPrem += 0;
            } else {
                tSumPrem += Double.parseDouble(tSSRS.GetText(i, 1));
            }
            
        }

        tSql = "select sum(prem) from (SELECT  "
               + " case when days(cvalidate)>" + mStartDays
               + " then  "
               + " case when days(enddate)<" + mEndDays
               + " then "
               + " case when payintv<=0"
               + " then  prem / (days(enddate) - days(cvalidate) + 1) * (days(enddate) - days(cvalidate) + 1)"
               +
               " else  prem * 12 /  payintv / 365* (days(enddate) - days(cvalidate) + 1)"
               + " end "
               + " else "
               + " case when payintv<=0"
               + " then  prem / (days(enddate) - days(cvalidate) + 1) * (" +
               mEndDays + " - days(cvalidate) + 1)"
               + " else  prem * 12 / payintv / 365* (" + mEndDays +
               " - days(cvalidate) + 1)"
               + " end "
               + " end "
               + " else  "
               + " case when days(enddate)<" + mEndDays
               + " then "
               + " case when payintv<=0"
               +
               " then  prem / (days(enddate) - days(cvalidate) + 1) * (days(enddate) - " +
               mStartDays + " + 1)"
               + " else  prem * 12 / payintv / 365* (days(enddate) - " +
               mStartDays + " + 1)"
               + " end "
               + " else "
               + " case when payintv<=0"
               + " then  prem / (days(enddate) - days(cvalidate) + 1) * (" +
               mEndDays + " - " + mStartDays + " + 1)"
               + " else  prem * 12 / payintv / 365* (" + mEndDays + " - " +
               mStartDays + " + 1)"
               + " end "
               + " end "
               + " end prem"

               + " FROM lbpol"
               +
               " WHERE appflag='1' and managecom LIKE '"
               + cManageCom
               + "%'"
               + " and cvalidate <='"
               + mEndDate
               + "'"
               + " and enddate >='" + mStartDate + "' AND riskcode NOT IN (SELECT riskcode FROM lmriskapp WHERE riskperiod='L' OR risktype3='7' AND risktype <>'M') " 
               + tPartSql 
               + ") t WITH UR";
        System.out.println("经过保费2"+tSql);
        tSSRS = tExeSQL.execSQL(tSql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            if (tSSRS.GetText(i, 1).equals("null")) {
                tSumPrem += 0;
            } else {
                tSumPrem += Double.parseDouble(tSSRS.GetText(i, 1));
            }
        }
        
        return tSumPrem;
    }


    /**
     * 进行数据查询
     * @return boolean
     */
    private boolean queryData() {
        TextTag tTextTag = new TextTag();
        mXmlExport = new XmlExport();
        //设置模版名称
        mXmlExport.createDocument("LLBranchCaseReport.vts", "printer");
        System.out.print("dayin252");
        tTextTag.add("ManageComName", mManageComName);
        tTextTag.add("StartDateN", mStartDate);
        tTextTag.add("EndDateN", mEndDate);
        tTextTag.add("Operator", moperator);
        tTextTag.add("MakeDate", currentDate);
        String tStatsName = "";
        if("1".equals(mStatsType)){
        	tStatsName = "商业保险";
        }else if("2".equals(mStatsType)){
        	tStatsName = "社会保险";
        }else{
        	tStatsName = "全部";
        }
        tTextTag.add("StatsName", tStatsName);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] title = {"", "", "", "", "", "", "", "", "", ""};

        if (!getDataList()) {
            return false;
        }
        mListTable.setName("ENDOR");
        System.out.println("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.println("121");
        mXmlExport.outputDocumentToFile("E:\\", "LLBranchCaseReport");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 取得传入的数据
     * @return boolean
     */
    private boolean getInputData(VData pmInputData) {
        //全局变量
//      mGlobalInput.setSchema((GlobalInput) pmInputData.getObjectByObjectName(
//              "GlobalInput", 0));
        mStartDate = (String) pmInputData.get(0);
        mEndDate = (String) pmInputData.get(1);
        mManageCom = (String) pmInputData.get(2);
        moperator = (String) pmInputData.get(3);
        mManageComName = (String) pmInputData.get(4);
        mStatsType = (String) pmInputData.get(5);//统计类型
        mTransferData = (TransferData) pmInputData.getObjectByObjectName(
				"TransferData", 0);
    	mOperator = moperator;
		mFileNameB = (String)mTransferData.getValueByName("tFileNameB");

        String tSql = "SELECT days('" + mStartDate + "'),days('" + mEndDate +
                      "') FROM dual where 1=1 with ur";
        ExeSQL tExeSQLDate = new ExeSQL();
        SSRS tSSRSDate = new SSRS();
        tSSRSDate = tExeSQLDate.execSQL(tSql);
        mStartDays = Integer.parseInt(tSSRSDate.GetText(1, 1));
        mEndDays = Integer.parseInt(tSSRSDate.GetText(1, 2));
        mMakeDate = PubFun.getCurrentDate();
        return true;
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLBranchCaseReportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 取得返回处理过的结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    private void jbInit() throws Exception {
    }
}
