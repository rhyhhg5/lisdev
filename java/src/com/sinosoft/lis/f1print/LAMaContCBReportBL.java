package com.sinosoft.lis.f1print;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author XX
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;



public class LAMaContCBReportBL {

        /** 错误处理类，每个需要错误处理的类中都放置该类 */
       public CErrors mErrors = new CErrors();
       private VData mResult = new VData();
       /** 全局变量 */
       private GlobalInput mGlobalInput = new GlobalInput() ;
       private String mManageCom="";
       private String mStartDate= "";
       private String mEndDate = "";
//     文件暂存路径
       private String mfilePathAndName;
       private VData mInputData = new VData();
       private String mOperate = "";
       private PubFun mPubFun = new PubFun();
       private String mManageName = "";
       private TransferData mTransferData = new TransferData();
       private String[][] mShowDataList = null;


       /**
      * 传输数据的公共方法
      */
     public boolean submitData(VData cInputData, String cOperate)
     {

       mOperate = cOperate;
       mInputData = (VData) cInputData;
       if (mOperate.equals("")) {
           this.bulidErrorB("submitData", "数据不完整");
           return false;
       }

       if (!mOperate.equals("PRINT")) {
           this.bulidErrorB("submitData", "数据不完整");
           return false;
       }

       // 得到外部传入的数据，将数据备份到本类中
         if (!getInputData(mInputData)) {
             return false;
         }

        // 进行数据查询
        if (!queryData()) {
            return false;
        }
         return true;
     }


     private void bulidErrorB(String cFunction, String cErrorMsg) {

             CError tCError = new CError();

             tCError.moduleName = "LAMaContReportBL";
             tCError.functionName = cFunction;
             tCError.errorMessage = cErrorMsg;

             this.mErrors.addOneError(tCError);

    }

     /**
       * 取得传入的数据
       * @return boolean
       */
      private boolean getInputData(VData cInputData)
      {

          try
          {
              mGlobalInput.setSchema((GlobalInput) cInputData.
                                     getObjectByObjectName("GlobalInput", 0));
              mManageCom = (String)cInputData.get(0);
              mStartDate = (String)cInputData.get(1);
              mEndDate = (String)cInputData.get(2);
              mfilePathAndName=(String)cInputData.get(3);
          } catch (Exception ex) {
              this.mErrors.addOneError("");
              return false;
          }

          return true;

   }

   /**
      * 获取打印所需要的数据
      * @param cFunction String
      * @param cErrorMsg String
      */
     /**
   * 追加错误信息
   * @param szFunc String
   * @param szErrMsg String
   */
  private void buildError(String szFunc, String szErrMsg)
  {
      CError cError = new CError();
      cError.moduleName = "LAMaContCBReportBL";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      System.out.println(szFunc + "--" + szErrMsg);
      this.mErrors.addOneError(cError);
  }



    private boolean queryData()
      {
          try{
              // 查询数据
              if(!getDataList())
              {
                  return false;
              }

          return true;

          }catch(Exception ex)
          {
              buildError("queryData", "LABL发生错误，准备数据时出错！");
              return false;
          }


  }


  private boolean getDataList()
   {
	  CreateCSVFile tCreateCSVFile=new CreateCSVFile();
      tCreateCSVFile.initFile(this.mfilePathAndName);
      String[][] tFirstDataList = new String[1][12];
      tFirstDataList[0][0]="营业单位编码";
      tFirstDataList[0][1]="营业单位";
      tFirstDataList[0][2]="主管姓名";
      tFirstDataList[0][3]="首年保费(万元)";
      tFirstDataList[0][4]="首年件数";
      tFirstDataList[0][5]="首年件均保费";
      tFirstDataList[0][6]="平均人力";
      tFirstDataList[0][7]="人均保费";
      tFirstDataList[0][8]="客户数";
      tFirstDataList[0][9]="续期保费(万元)";
      tFirstDataList[0][10]="续保保费(万元)";
      tFirstDataList[0][11]="活动率(%)";
//      tCreateCSVFile.doWrite(tFirstDataList);
      tCreateCSVFile.doWrite(tFirstDataList,11);
	  String tSQL = "";
       
       // 1、得到全部已开业的机构
       tSQL  = "select *  from labranchgroup where ";
       tSQL += "    managecom like  '" + mManageCom + "%'";
       tSQL += "    and founddate <='" + mEndDate + "'";
       tSQL += "    and  ( enddate > '" + mStartDate + "' or enddate is null)";
       tSQL += "    and  branchtype='1' and  branchtype2='01'";
       tSQL += " order by branchattr";

       LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
       LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
       tLABranchGroupSet = tLABranchGroupDB.executeQuery(tSQL);
       if(tLABranchGroupSet.size()==0)
       {
           buildError("queryData", "没有符合条件的机构！");
           return false;
       }

       // 2、查询需要表示的数据
       String[][] tShowDataList = new String[tLABranchGroupSet.size()][12];
       for(int i=1;i<=tLABranchGroupSet.size();i++)
       {// 循环机构进行统计
           LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
           tLABranchGroupSchema = tLABranchGroupSet.get(i);
           tShowDataList[i-1][0] = tLABranchGroupSchema.getBranchAttr();
           System.out.println(tLABranchGroupSchema.getAgentGroup()+"/"+tShowDataList[i-1][0]);

           // 查询需要表示的数据
           if (!queryOneDataList(tLABranchGroupSchema.getBranchAttr(),
                                 tLABranchGroupSchema.getAgentGroup(),
                                 tLABranchGroupSchema.getName(),
                                 tLABranchGroupSchema.getBranchManagerName(),
                                 tShowDataList[i - 1]))
           {
               System.out.println("[" + tLABranchGroupSchema.getAgentGroup() +
                                  "] 本机构数据查询失败！");
               return false;
           }
       }
       tCreateCSVFile.doWrite(tShowDataList,11);
       tCreateCSVFile.doClose();
       return true;
  }



  /**
    * 查询填充表示数据
    * @return boolean
    */
   private boolean queryOneDataList(String pmBranchAttr,String pmAgentGroup,
                                    String pmName,String pmBranchManageName,String[] pmOneDataList)
   {
       try{
           String fPerson="";
           String mPerson="";
           String cPerson="";
           pmOneDataList[0] = pmBranchAttr;
           // 2、管理机构名称
           pmOneDataList[1] = pmName;
           // 3、主管名称
           pmOneDataList[2] = pmBranchManageName==null?"":pmBranchManageName;
           // 4、首年保费（万元）
           pmOneDataList[3] = getFirstPrem(pmAgentGroup);
           // 5、首年件数
           pmOneDataList[4] = getSumCont(pmAgentGroup);
           // 6、首年件均保费
           pmOneDataList[5] = getAvgPrem(pmOneDataList[3], pmOneDataList[4]);
           // 7、平均人力
           fPerson=""+getAgentCountByManageCom(mStartDate,pmAgentGroup);
           mPerson=""+getAgentCountByManageCom(mEndDate,pmAgentGroup);
           pmOneDataList[6] = getTransPreson(fPerson,mPerson);
           // 8、人均保费
           pmOneDataList[7] = getAvgMoney(pmOneDataList[3],pmOneDataList[6]);
           // 9、客户数
           pmOneDataList[8] = getAgentCount(pmAgentGroup);
           // 10、续期保费（万元）
           pmOneDataList[9] = getRenewalSum(pmAgentGroup);
           // 11、续保保费（万元）
           pmOneDataList[10] = getRePremSum(pmAgentGroup);
           // 12、活动率
           cPerson = getHaveCaseAgentCount(pmAgentGroup);
           pmOneDataList[11] = getAct(cPerson,pmOneDataList[6]);

       }catch(Exception ex)
       {
           buildError("queryOneDataList", "准备数据时出错！");
           System.out.println(ex.toString());
           return false;
       }

       return true;
   }


  /**
    * 查询首年保费
    * @param  pmAgentGroup String
    * @return String
    */
   private String getFirstPrem(String pmAgentGroup)
   {
     String tSQL = "";
     String tRtValue = "";
     DecimalFormat tDF = new DecimalFormat("0.######");


     tSQL  = "select  value(sum(c.transmoney),0)  from lacommision c,latempmision d ";
     tSQL += " where  c.commisionsn=d.commisionsn and c.branchtype='1'  and  c.branchtype2='01'  and  c.payyear=0  and  c.renewcount=0  ";
     tSQL += "   and  (d.department='"+ pmAgentGroup+ "' or d.section='"+pmAgentGroup+"' or d.branch='"+pmAgentGroup+"')";
     tSQL += "   and  d.tmakedate >= '" + mStartDate + "'";
     tSQL += "   and  d.tmakedate <= '" + mEndDate + "'";
     System.out.println(tSQL);
     try{
         tRtValue = tDF.format(Arith.div(execQuery(tSQL),10000));
     }
     catch(Exception ex)
     {
         System.out.println("getFirstPrem 出错！");
     }

     return tRtValue;

  }

  /**
     * 查询续期保费（万元）
     * @param  pmAgentGroup String
     * @return String
     */
    private String getRenewalSum(String pmAgentGroup)
    {
      String tSQL = "";
      String tRtValue = "";
      DecimalFormat tDF = new DecimalFormat("0.######");


     tSQL  = "select  value(sum(c.transmoney),0)  from lacommision c,latempmision d   ";
     tSQL += "   where  c.commisionsn=d.commisionsn and c.branchtype='1'  and  c.branchtype2='01' and c.payyear>0 and  c.renewcount=0  ";
     tSQL += "   and  (d.department='"+ pmAgentGroup+ "' or d.section='"+pmAgentGroup+"' or d.branch='"+pmAgentGroup+"')";
     tSQL += "   and  d.tmakedate >= '" + mStartDate + "'";
     tSQL += "   and  d.tmakedate <= '" + mEndDate + "'";

      try{
          tRtValue = tDF.format(Arith.div(execQuery(tSQL),10000));

      }catch(Exception ex)
      {
          System.out.println("getRenewalSum 出错！");
      }

      return tRtValue;

   }

   /**
        * 查询续保保费（万元）
        * @param  String
        * @return String
        */
       private String getRePremSum(String pmAgentGroup)
       {
         String tSQL = "";
         String tRtValue = "";
         DecimalFormat tDF = new DecimalFormat("0.######");


     tSQL  = "select value(sum(c.transmoney),0) from lacommision c,latempmision d   ";
     tSQL += " where  c.commisionsn=d.commisionsn and   c.branchtype='1' and c.branchtype2='01'  and  c.renewcount > 0  ";
     tSQL += "   and  (d.department='"+ pmAgentGroup+ "' or d.section='"+pmAgentGroup+"' or d.branch='"+pmAgentGroup+"')";
     tSQL += "   and  d.tmakedate >= '" + mStartDate + "'";
     tSQL += "   and  d.tmakedate <= '" + mEndDate + "'";

         System.out.println(tSQL);
         try{
             tRtValue = tDF.format(Arith.div(execQuery(tSQL),10000));
         }catch(Exception ex)
         {
             System.out.println("getSumPrem 出错！");
         }

         return tRtValue;

   }
  /**
   * 查询首年件数
   * @param  pmAgentGroup String
   * @return String
   */
  private String getSumCont(String pmAgentGroup)
  {
    String tSQL = "";
    int tRtValue = 0;
    tSQL  = "select count(distinct c.contno) from lacommision c,latempmision d ";
    tSQL += " where  c.commisionsn=d.commisionsn and   c.branchtype='1' and c.branchtype2='01' and  c.payyear=0  and  c.renewcount=0 ";
    tSQL += "   and  (d.department='"+ pmAgentGroup+ "' or d.section='"+pmAgentGroup+"' or d.branch='"+pmAgentGroup+"')";
    tSQL += "   and  d.tmakedate >= '" + mStartDate + "'";
    tSQL += "   and  d.tmakedate <= '" + mEndDate + "'";

    System.out.println(tSQL);
    try{
        tRtValue = (int)execQuery(tSQL);
    }catch(Exception ex)
    {
        System.out.println("getSumCont 出错！");
    }

    return ""+tRtValue;

 }



 /**
   * 查询首年件均保费
   * @param  String
   * @return String
   */
  private String getAvgPrem(String pmSumMoney,String pmProCont)
  {
         String tSQL = "";
         String tRtValue = "";
         DecimalFormat tDF = new DecimalFormat("0.##");

         if("0".equals(pmProCont))
        {
            return "0";
        }

         tSQL = "select DECIMAL(DECIMAL(" + pmSumMoney + "*10000,12,2) / DECIMAL(" +
                pmProCont + ",12,2),12,2) from dual";

         try{
             tRtValue = "" + tDF.format(execQuery(tSQL));
         }catch(Exception ex)
         {
             System.out.println("getAvgPrem 出错！");
         }

         return tRtValue;
     }



  /**
   * 查询管理机构下的期初、期末人数
   * @return int
   */
  private int getAgentCountByManageCom(String pmDate,String pmAgentGroup)
  {
      String tSQL = "";
      int tRtValue = 0;

      tSQL  = "select count(distinct b.agentcode) from laagent b, labranchgroup aa ";
      tSQL += "  where b.branchtype='1' and b.branchtype2='01' and b.agentgroup ";
      tSQL += "  in (select a.agentgroup from  labranchgroup a ";
      tSQL += "   where (aa.agentgroup =substr(a.branchseries,1,12)  or aa.agentgroup =substr(a.branchseries,14,12)  ";
      tSQL += "   or  aa.agentgroup =substr(a.branchseries,27,12)) )";
      tSQL += "   and  aa.agentgroup='" + pmAgentGroup+ "'";
      tSQL += "   and  b.employdate<='" + pmDate + "'";
      tSQL += "   and  (b.outworkdate> '" + pmDate + "' or b.outworkdate is null )";

      try{
          tRtValue = (int) execQuery(tSQL);
      }catch(Exception ex)
      {
          System.out.println("getAgentCountByManageCom 出错！");
      }

      return tRtValue;
  }

  /**
    * 取得平均人力
    * @param pmValue1
    * @param pmValue2
    * @return String
    */
   private String getTransPreson(String pmValue1,String pmValue2)
   {
       String tSQL = "";
       String tRtValue = "";
       DecimalFormat tDF = new DecimalFormat("0.##");
       tSQL = "select DECIMAL(DECIMAL(" + pmValue1 + " + " + pmValue2 +
              ",12,2) / 2,12,2) from dual";
       try{
            tRtValue = "" + tDF.format(execQuery(tSQL));

       }catch(Exception ex)
       {
           System.out.println("getTransPreson 出错！");
       }

       return tRtValue;
   }

   /**
     * 计算人均保费
     * @param pmAgentCont
     * @param pmSumMoney
     * @return String
     */
    private String getAvgMoney(String pmSumMoney,String pmAgentCont)
    {
        String tSQL = "";
        String tRtValue = "";
        DecimalFormat tDF = new DecimalFormat("0.##");

        if("0".equals(pmAgentCont))
         {
             return "0";
         }

        tSQL = "select DECIMAL(DECIMAL(" + pmSumMoney + "*10000,12,2) / DECIMAL(" +
               pmAgentCont + ",12,2),12,2) from dual";

        try{
            tRtValue = "" + tDF.format(execQuery(tSQL));

        }catch(Exception ex)
        {
            System.out.println("getAvgMoney 出错！");
        }

        return tRtValue;
    }



    /**
      * 统计客户数
      * @param  pmAgentGroup String
      * @return String
      */
     private String getAgentCount(String pmAgentGroup)
     {
         String tSQL = "";
         int tRtValue = 0;

         tSQL  = "select count(c.P12) from lacommision c,latempmision d   ";
         tSQL += " where  c.commisionsn=d.commisionsn and   c.branchtype='1' and c.branchtype2='01'  ";
         tSQL += "   and  (d.department='"+ pmAgentGroup+ "' or d.section='"+pmAgentGroup+"' or d.branch='"+pmAgentGroup+"')";
         tSQL += "   and  d.tmakedate >= '" + mStartDate + "'";
         tSQL += "   and  d.tmakedate <= '" + mEndDate + "'";

         try{
             tRtValue = (int) execQuery(tSQL);
         }catch(Exception ex)
         {
             System.out.println("getAgentCount 出错！");
         }

         return "" + tRtValue;
  }

    /**
      * 计算活动率
      * @param pmCaseAgentCount String
      * @param pmAgentCountInit String
      * @return String
      */
     private String getAct(String pmCaseAgentCount,String pmAgentCountInit)
     {
         String tSQL = "";
         String tRtValue = "";
         DecimalFormat tDF = new DecimalFormat("0.##");

         if("0".equals(pmAgentCountInit))
         {
             return "0";
         }

         tSQL = "select DECIMAL(DECIMAL(" + pmCaseAgentCount + "* 100,12,2) / DECIMAL(" +
                pmAgentCountInit + ",12,2) ,12,2) from dual";
         try{
             tRtValue = "" + tDF.format(execQuery(tSQL));
         }catch(Exception ex)
         {
             System.out.println("getAct 出错！");
         }

         return tRtValue;
     }

     /**
          * 统计出单人数
          * @param  pmAgentGroup String
          * @return String
          */
         private String getHaveCaseAgentCount(String pmAgentGroup)
         {
             String tSQL = "";
             int tRtValue = 0;

             tSQL  = " select count(distinct(c.agentcode)) from lacommision c,latempmision d  ";
             tSQL += " where  c.commisionsn=d.commisionsn and   (d.department='"+ pmAgentGroup+ "' or d.section='"+pmAgentGroup+"' or d.branch='"+pmAgentGroup+"')";
             tSQL += "   and  d.tmakedate >= '" + mStartDate + "'";
             tSQL += "   and  d.tmakedate <= '" + mEndDate + "'";

             try{
                 tRtValue = (int) execQuery(tSQL);
             }catch(Exception ex)
             {
                 System.out.println("getHaveCaseAgentCount 出错！");
             }

             return "" + tRtValue;
  }


   /**
     * 执行SQL文查询结果
     * @param sql String
     * @return double
     */
    private double execQuery(String sql)
    {
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        System.out.println(sql);

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null)return 0.00;
            st = conn.prepareStatement(sql);
            if (st == null)return 0.00;
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getDouble(1);
            }
            return 0.00;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
                st.close();
                rs.close();
                conn.close();
                st = null;
                rs = null;
                conn = null;
            } catch (Exception e) {}
        }
    }



    private boolean getManageNameB() {

             String sql = "select name from ldcom where comcode='" + mManageCom +
                          "'";

             SSRS tSSRS = new SSRS();

             ExeSQL tExeSQL = new ExeSQL();

             tSSRS = tExeSQL.execSQL(sql);

             if (tExeSQL.mErrors.needDealError()) {

                 this.mErrors.addOneError("销售单位不存在！");

                 return false;

             }

             if (mManageCom.equals("86")) {
                 this.mManageName = "";
             } else {
                 if(mManageCom.length()>4)
                 {this.mManageName = tSSRS.GetText(1, 1) + "分公司";}
                 else
                 {this.mManageName = tSSRS.GetText(1, 1);}
             }
             return true;
      }






      /**
       * @return VData
       */
      public VData getResult() {
          return mResult;
    }


}
