/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import java.util.Vector;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLDatasets;

/*
 * <p>ClassName: LCContF1PBL </p>
 * <p>Description: LCPolF1BL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-11-04
 */
public class ReLCContF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mVData = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCContSet mOldLCContSet = new LCContSet();
    private LCContSet mNewLCContSet = new LCContSet();
    private XMLDatasets mXMLDatasets = new XMLDatasets();
    private String mOperate = "";

    /*
     * 对于同时传入主险和附加险保单号的情况，如果它们是同一个印刷号的，
     * 将被存在同一个保单数据块中。所以将打印过的保单号存放在这个Vector中。
     */
    private Vector m_vPolNo = new Vector();

    public ReLCContF1PBL()
    {
        mXMLDatasets.createDocument();
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("PRINT")
                && !cOperate.equals("CONFIRM"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }
            if (!checkdate())
            {
                return false;
            }

            if (cOperate.equals("PRINT"))
            // 准备所有要打印的数据
            {
                if (!getPrintData())
                {
                    return false;
                }
            }
            else if (cOperate.equals("CONFIRM"))
            {
                if (!getConfirm())
                {
                    return false;
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submit", "发生异常");
            return false;
        }
    }

    /**
     * 调试函数
     * @param args String[]
     */
    public static void main(String[] args)
    {
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //获取前台传入的数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mOldLCContSet.set((LCContSet) cInputData.getObjectByObjectName(
                "LCContSet",
                0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    /**
     * 构建错误信息
     * @param cFunc String
     * @param cErrMsg String
     */
    private void buildError(String cFunc, String cErrMsg)
    {
        CError tError = new CError();
        tError.moduleName = "LCContF1PBL";
        tError.functionName = cFunc;
        tError.errorMessage = cErrMsg;
        this.mErrors.addOneError(tError);
    }

    /**
     * 重新打印，不生成新的业务数据
     * @return boolean
     */
    private boolean getPrintData()
    {
        LCContDB tLCContDB = null;
        //查询前台传入的数据信息
        for (int nIndex = 1; nIndex <= mOldLCContSet.size(); nIndex++)
        {
            tLCContDB = new LCContDB();
            tLCContDB.setContNo(mOldLCContSet.get(nIndex).getContNo());
            tLCContDB.getInfo();
            //置打印状态为-1，以便打印时特殊处理
            tLCContDB.setPrintCount( -1);
            //将新数据放入到准备传入后台的容器中
            mNewLCContSet.add(tLCContDB.getSchema());
        }
        mVData.add(mNewLCContSet);
        //提交后台执行数据更新
        ReLCContF1PBLS tReLCContF1PBLS = new ReLCContF1PBLS();
        tReLCContF1PBLS.submitData(mVData, mOperate);
        if (tReLCContF1PBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tReLCContF1PBLS.mErrors);
            buildError("saveData", "提交数据库出错！");
            return false;
        }
        return true;
    }

    /**
     * 重新打印，生成新的业务数据
     * @return boolean
     */
    private boolean getConfirm()
    {
        LCContDB tLCContDB = null;
        //查询前台传入的数据信息
        for (int nIndex = 1; nIndex < mOldLCContSet.size(); nIndex++)
        {
            tLCContDB = new LCContDB();
            tLCContDB.setContNo(mOldLCContSet.get(nIndex).getContNo());
            tLCContDB.getInfo();
            //置打印状态为-1，以便打印时特殊处理
            tLCContDB.setPrintCount(0);
            //将新数据放入到准备传入后台的容器中
            mNewLCContSet.add(tLCContDB);
        }
        mVData.add(mNewLCContSet);
        //提交后台执行数据更新
        ReLCContF1PBLS tReLCContF1PBLS = new ReLCContF1PBLS();
        tReLCContF1PBLS.submitData(mVData, mOperate);
        if (tReLCContF1PBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tReLCContF1PBLS.mErrors);
            buildError("saveData", "提交数据库出错！");
            return false;
        }
        return true;
    }

    private boolean checkdate() {

        for (int nIndex = 1; nIndex <= mOldLCContSet.size(); nIndex++) {
            LCContReceiveDB tLCContReceiveDB = new LCContReceiveDB();
            LCContReceiveSet tLCContReceiveSet = new LCContReceiveSet();
            tLCContReceiveDB.setContNo(mOldLCContSet.get(nIndex).getContNo());
            tLCContReceiveDB.setReceiveState("0");
            tLCContReceiveSet = tLCContReceiveDB.query();
            if (tLCContReceiveSet.size() > 0) {
                String errormsg = "保单号为" + mOldLCContSet.get(nIndex).getContNo() +
                                  "的记录还没有进行合同接收操作，请先合同接收后再打印！";
                System.out.println(errormsg);
                buildError("checkdate", errormsg);
                return false;

            }
        }

        return true;
    }
}
