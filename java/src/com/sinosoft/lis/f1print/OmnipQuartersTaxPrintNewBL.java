package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.utility.TextTag;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.fininterface.TransItem.RiskCode;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import java.text.DecimalFormat;

/**
 * <p>
 * Title: 税优万能年报打印（单打）
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2007
 * </p>
 * 
 * <p>
 * Company:
 * </p>
 * 
 * @author Lichang
 * @version 1.0
 */
public class OmnipQuartersTaxPrintNewBL implements PrintService {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private VData mResult = new VData();

	private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

	private LCContSchema mLCContSchema = new LCContSchema();

	private LCPolSchema mLCPolSchema = new LCPolSchema();

	private LCAddressSchema mLCAddressSchema = new LCAddressSchema();

	TextTag textTag = new TextTag();

	SSRS mSSRS = new SSRS();

	SSRS tSSRS = new SSRS();

	private String mPolYear = "";// 报告年度

	private String mQuarter = ""; // 报告季度

	private String mOperate = "";

	private String mTempPolYear = "";

	private String mStartDate = "";

	private String mEndDate = "";

	private ExeSQL mExeSQL = new ExeSQL();

	DecimalFormat df = new DecimalFormat("#.00");

	public OmnipQuartersTaxPrintNewBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println("==OmnipQuartersTaxPrintNewBL start==");

		this.mOperate = cOperate;
		if (!getInputData(cInputData)) {
			return false;
		}

		mResult.clear();

		// 准备所有要打印的数据
		if (!getPrintData()) {
			return false;
		}

		// 加入到打印列表
		if (!dealPrintMag()) {
			return false;
		}

		System.out.println("==OmnipAnnalsTaxPrintNewBL end==");

		return true;
	}

	private boolean getInputData(VData cInputData) {
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		mLOPRTManagerSchema = (LOPRTManagerSchema) cInputData
				.getObjectByObjectName("LOPRTManagerSchema", 0);
		if (mGlobalInput == null || mLOPRTManagerSchema == null) {
			CError tError = new CError();
			tError.moduleName = "OmnipQuartersPrintNewBL.java";
			tError.functionName = "submitData";
			tError.errorMessage = "获取数据失败！";
			mErrors.addOneError(tError);
			return false;
		}
		mQuarter = mLOPRTManagerSchema.getStandbyFlag1();
		mPolYear = mLOPRTManagerSchema.getStandbyFlag3();
		System.out.println("保单的结算年度：" + mPolYear + "年，季度：" + mQuarter + "' ");
		if (null == mPolYear || "".equals(mPolYear) || null == mQuarter || "".equals(mQuarter)) {
			CError tError = new CError();
			tError.moduleName = "OmnipQuartersPrintNewBL.java";
			tError.functionName = "submitData";
			tError.errorMessage = "获取打印年度季度信息失败";
			return false;
		}
		// 根据报告的季度设置起止日期
		if ("1".equals(mQuarter)) {
			mStartDate = mPolYear + "-01-01";
			mEndDate = mPolYear + "-03-31";
		} else if ("2".equals(mQuarter)) {
			mStartDate = mPolYear + "-04-01";
			mEndDate = mPolYear + "-06-30";
		} else if ("3".equals(mQuarter)) {
			mStartDate = mPolYear + "-07-01";
			mEndDate = mPolYear + "-09-30";
		} else if ("4".equals(mQuarter)) {
			mStartDate = mPolYear + "-10-01";
			mEndDate = mPolYear + "-12-31";
		}

		System.out.println("==获取险种信息开始==");
		System.out.println("==otherno:'" + mLOPRTManagerSchema.getOtherNo());

		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setPolNo(mLOPRTManagerSchema.getOtherNo());
		if (!tLCPolDB.getInfo()) {
			CError tError = new CError();
			tError.moduleName = "OmnipQuartersPrintNewBL.java";
			tError.functionName = "dealPrintMag";
			tError.errorMessage = "获得险种信息失败！";
			mErrors.addOneError(tError);
			return false;
		}
		mLCPolSchema = tLCPolDB.getSchema();

		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(mLCPolSchema.getContNo());
		if (!tLCContDB.getInfo()) {
			CError tError = new CError();
			tError.moduleName = "OmnipQuartersPrintNewBL.java";
			tError.functionName = "dealPrintMag";
			tError.errorMessage = "获得保单信息失败！";
			mErrors.addOneError(tError);
			return false;
		}
		mLCContSchema = tLCContDB.getSchema();

		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	public CErrors getErrors() {
		return this.mErrors;
	}

	private boolean getPrintData() {
		XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
		xmlexport.createDocument("OmnipQuartersTax.vts", "printer");
		textTag.add("JetFormType", "SY003");

		String sqlusercom = "select comcode from lduser where usercode='"
				+ mGlobalInput.Operator + "' with ur";
		String comcode = new ExeSQL().getOneValue(sqlusercom);
		if (comcode.equals("86") || comcode.equals("8600")
				|| comcode.equals("86000000")) {
			comcode = "86";
		} else if (comcode.length() >= 4) {
			comcode = comcode.substring(0, 4);
		} else {
			CError.buildErr(this, "操作员机构查询出错！");
			return false;
		}

		String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"
				+ comcode + "' with ur";
		String printcode = new ExeSQL().getOneValue(printcom);
		textTag.add("ManageComLength4", printcode);
		textTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
		if ("batch".equals(mOperate)) {
			textTag.add("previewflag", "1");//测试
		} else {
			textTag.add("previewflag", "1");
		}

		textTag.add("Operator", mGlobalInput.Operator); // 操作员

		textTag.add("BarCode1", createWorkNo()); // 条形码
		textTag.add("BarCodeParam1","BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

		// 获取投保人地址
		LCAppntDB tLCAppntDB = new LCAppntDB();
		tLCAppntDB.setContNo(mLCContSchema.getContNo());

		if (!tLCAppntDB.getInfo()) {
			CError.buildErr(this, "投保人表中缺少数据");
			return false;
		}

		String sql = " select * from LCAddress where CustomerNo='"
				+ mLCContSchema.getAppntNo()
				+ "' AND AddressNo = '"
				+ tLCAppntDB.getAddressNo() + "'";
		System.out.println("sql=" + sql);
		LCAddressDB tLCAddressDB = new LCAddressDB();
		LCAddressSet tLCAddressSet = tLCAddressDB.executeQuery(sql);
		mLCAddressSchema = tLCAddressSet.get(tLCAddressSet.size()).getSchema();
		textTag.add("AppntName", tLCAppntDB.getAppntName()); // 投保人姓名
		textTag.add("ZipCode", mLCAddressSchema.getZipCode());
		System.out.println("ZipCode=" + mLCAddressSchema.getZipCode());
		textTag.add("Address", mLCAddressSchema.getPostalAddress());
		textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));// 打印日期

		// 保单年度
		mTempPolYear = new ExeSQL()
				.getOneValue("select min(polYear) From lcinsureaccbalance where  year(DueBalaDate - 1 days) ="
						+ mPolYear
						+ " and DueBalaDate <= '"
						+ mEndDate
						+ "' "
						+ " and DueBalaDate >= '"
						+ mStartDate
						+ "' "
						+ " and contno ='"
						+ mLCContSchema.getContNo()
						+ "' and polno ='"
						+ mLCPolSchema.getPolNo()
						+ "' with ur ");

		textTag.add("StartDate", CommonBL.decodeDate(mStartDate));// 报告期间起
		textTag.add("EndDate", CommonBL.decodeDate(mEndDate));// 报告期间止

		textTag.add("CustomerName", mLCPolSchema.getAppntName());// 客户姓名

		if (!addNewInfo()) {
			return false;
		}

		// 添加节点
		if (textTag.size() > 0) {
			xmlexport.addTextTag1(textTag);
		}

		xmlexport.outputDocumentToFile("F:\\", "OmnipAnnalsTax");
		mResult.clear();
		mResult.addElement(xmlexport);

		return true;
	}

	private boolean dealPrintMag() {
		String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
		String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
		mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
		mLOPRTManagerSchema.setOtherNo(mLCPolSchema.getPolNo());
		mLOPRTManagerSchema.setOtherNoType("00");
		mLOPRTManagerSchema.setCode("SY003");
		mLOPRTManagerSchema.setManageCom(mLCContSchema.getManageCom());
		mLOPRTManagerSchema.setAgentCode(mLCContSchema.getAgentCode());
		mLOPRTManagerSchema.setReqCom(mLCContSchema.getManageCom());
		mLOPRTManagerSchema.setReqOperator(mLCContSchema.getOperator());
		mLOPRTManagerSchema.setPrtType("0");
		mLOPRTManagerSchema.setStateFlag("0");
		mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
		mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
		// mLOPRTManagerSchema.setStandbyFlag2(tLJSGetSchema.getGetNoticeNo());
		// //这里存放 （也为交费收据号）
		// mLOPRTManagerSchema.setStandbyFlag1(tLJSGetSchema.getOtherNo());
		mResult.addElement(mLOPRTManagerSchema);
		return true;
	}

	/**
	 * 获取列表数据
	 * 
	 * @param 无
	 * @return ListTable
	 */
	private ListTable getListTable() {

		String tempMoney = "";
		ListTable tListTable = new ListTable();
		for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
			String[] info = new String[10];
			for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
				info[j - 1] = mSSRS.GetText(i, j);
			}
			tListTable.add(info);
		}
		tListTable.setName("RISKNAME");
		return tListTable;
	}

	private ListTable getListTable(SSRS tSSRS) {
		ListTable tListTable = new ListTable();
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			String[] info = new String[10];
			for (int j = 1; j <= tSSRS.getMaxCol(); j++) {
				info[j - 1] = tSSRS.GetText(i, j);
			}
			tListTable.add(info);
		}
		tListTable.setName("LIST");
		return tListTable;
	}

	/**
	 * 根据账户保单号码得到账户类型
	 * 
	 * @param insuAccNo
	 *            String
	 * @return String
	 */
	private String getInsuAccNo() {
		String sql = "select InsuAccNo from LCInsureAcc a where ContNo ='"
				+ mLCContSchema.getContNo() + "'";
		return (new ExeSQL()).getOneValue(sql);
	}

	private String getPolno() {
		String sql = "select polno from LCInsureAcc a where ContNo ='"
				+ mLCContSchema.getContNo() + "'";
		return (new ExeSQL()).getOneValue(sql);
	}

	/**
	 * 生成流水号
	 * 
	 * @return String
	 */
	private String createWorkNo() {

		String str = "16";
		String allString = "";
		String tDate = PubFun.getCurrentDate();
		tDate = tDate.substring(2, 4) + tDate.substring(5, 7)
				+ tDate.substring(8, 10);
		allString = str + tDate;
		String tWorkNo = tDate + PubFun1.CreateMaxNo("GOOD" + allString, 6);
		str += tWorkNo;
		return str;
	}

	private boolean addNewInfo() {
		System.out.println("报文内容开始————————————————————————");
		String tContNo = mLCContSchema.getContNo();
		String sql1 = "select (select riskname from lmrisk where riskcode = a.riskcode),cvalidate, "
				+ "a.riskcode,prem,payintv,amnt,polno,stateflag from lcpol a "
				+ "where contno = '"
				+ tContNo
				+ "' and exists (select 1 from lmriskapp "
				+ "where riskcode = a.riskcode and risktype4 = '"
				+ BQ.RISKTYPE1_ULI
				+ "' and taxoptimal='Y')"
				+ " and polno ='"
				+ mLCPolSchema.getPolNo() + "' with ur";
		SSRS tSSRS1 = new SSRS();
		tSSRS1 = mExeSQL.execSQL(sql1);
		textTag.add("RiskCodeName", tSSRS1.GetText(1, 1)); // 产品名称
		textTag.add("ContNo", mLCContSchema.getContNo()); // 保险单编号

		String contstate = "";
		if (tSSRS1.GetText(1, 8) != null && !"".equals(tSSRS1.GetText(1, 8))
				&& "1".equals(tSSRS1.GetText(1, 8))) {
			contstate = "有效";
		} else {
			contstate = "无效";
		}
		String tRiskCode = tSSRS1.GetText(1, 3);
		textTag.add("ContState", contstate);// 保单状态（有效/无效）

		textTag.add("AppntName", mLCPolSchema.getAppntName());// 投保人姓名
		textTag.add("InsuredName", mLCPolSchema.getInsuredName());// 被保险人姓名

		// 首期承保生效日
		String CValiDateSQL = "select min(temp.cvalidate) "
				+ "from (select cvalidate cvalidate,cinvalidate cinvalidate "
				+ "from lccont "
				+ "where conttype='1' and appflag='1' and contno='"
				+ tContNo
				+ "' "
				+ "union all select cvalidate cvalidate,cinvalidate cinvalidate "
				+ "from lbcont where conttype='1' and appflag='1' and edorno like 'xb%' and contno "
				+ "in (select newcontno from lcrnewstatelog where contno='"
				+ tContNo + "' and state='6')) temp "
				// + "where temp.cvalidate <= date('"+mEndDate+"') "
				+ "with ur";

		String CValiDate = mExeSQL.getOneValue(CValiDateSQL);
		if (null == CValiDate || "".equals(CValiDate)) {
			CValiDate = mLCContSchema.getCValiDate();
		}
		textTag.add("CValiDate", CommonBL.decodeDate(CValiDate));// 保单生效日

		//已缴费金额
		sql1 = "select nvl(sum(SumActuPayMoney),0) from ljapayperson where contno = '"
				+ tContNo
				+ "' and polno ='"
				+ mLCPolSchema.getPolNo()
				+ "' and paytype='ZC' with ur";
		double totalPrem = Double.parseDouble(mExeSQL.getOneValue(sql1));

		//缴费次数
        String countStr="select count(distinct curpaytodate) from ljapayperson where contno='"+mLCPolSchema.getContNo()+"' and paytype='ZC' ";
        String totalCount=mExeSQL.getOneValue(countStr);
        if(null == totalCount || "".equals(totalCount)){
        	totalCount="0";
        }
        textTag.add("TotalCount", totalCount);//已交费次数
		textTag.add("TotalPrem", PubFun.setPrecision2(totalPrem));

		// 生成结算利息列表,报告期内各月年化结算利率及结算信息
		for (int m = 1; m <= 3; m++) {
			String tmpStartDate = "";
			if (m == 1) {
				tmpStartDate = mStartDate;
			} else {
				tmpStartDate = PubFun.calDate(mStartDate, m - 1, "M", null);
			}
			try {
				textTag.add("RateMonth" + m, decodeDate(tmpStartDate));

				String ratesql = "select rate From LMInsuAccRate where riskcode = '"
						+ tRiskCode
						+ "' and StartBalaDate = '"
						+ tmpStartDate
						+ "' with ur";
				String rate = mExeSQL.getOneValue(ratesql);
				textTag.add("Rate" + m, String.valueOf(Double.valueOf(rate)));
			} catch (Exception ex) {
				CError.buildErr(this, "打印终止，获取保单" + tContNo + "第"
						+ mTempPolYear + "年" + tmpStartDate + "年化利率失败。");
				return false;
			}
		}

		ExeSQL tExeSQL = new ExeSQL();
		String sql = "select Year,quarter,solvency||'%',case when flag='1' then '达到' else '未达到' end  from  ldriskrate where  codetype='CFNL' and state='1' order by Year desc,quarter desc fetch first row only  with ur ";
		tSSRS = tExeSQL.execSQL(sql);
		textTag.add("Year1", tSSRS.GetText(1, 1));
		textTag.add("Quarter1", tSSRS.GetText(1, 2));
		textTag.add("Solvency", tSSRS.GetText(1, 3));
		textTag.add("Flag", tSSRS.GetText(1, 4));

		String sql2 = "select Year,quarter,riskrate  from  ldriskrate where  codetype='FXDJ' and state='1' order by Year desc,quarter desc fetch first row only  with ur ";
		tSSRS = tExeSQL.execSQL(sql2);
		textTag.add("Year2", tSSRS.GetText(1, 1));
		textTag.add("Quarter2", tSSRS.GetText(1, 2));
		textTag.add("RiskRate", tSSRS.GetText(1, 3));

		// 生成各月结算信息
		for (int m = 1; m <= 3; m++) {
			String tPolMonth = String.valueOf(m);
			String tBalaDate = "";
			if (m == 1) {
				tBalaDate = mStartDate;
			} else {
				tBalaDate = PubFun.calDate(mStartDate, m - 1, "M", null);
			}
			try {
				this.setMonthList(tContNo, tRiskCode, tPolMonth, tBalaDate);
			} catch (Exception ex) {
				CError.buildErr(this, "打印终止，获取保单" + tContNo + "第"
						+ mTempPolYear + "年" + tPolMonth + "月结算信息明细失败。");
				return false;
			}
		}
		System.out.println("12个月详细报文内容结束————————————————————————");
		return true;
	}

	// 生成各月结算信息
	private void setMonthList(String aContNo, String aRiskCode,
			String aPolMonth, String tStartDate) {
		textTag.add("PolMonth" + aPolMonth, SysConst.TEG_START);
		String sql = "";

		sql = "select max(duebaladate),(max(duebaladate) - 1 day),PolYear,PolMonth From lcinsureaccbalance where "
				+ " contno ='"
				+ aContNo
				+ "' and polno ='"
				+ mLCPolSchema.getPolNo()
				+ "' and (duebaladate - 1 month)='"
				+ tStartDate + "' and length(sequenceno) <> 14 "
				+ " group by PolYear,PolMonth with ur ";
		SSRS tSSRS = mExeSQL.execSQL(sql);
		if(null == tSSRS || tSSRS.getMaxRow()<=0){
			//没有查询到月结记录则应为保单投保首月所在的季度,填充默认数据
			textTag.add("Month" + aPolMonth, decodeDate(tStartDate));
			textTag.add("StartMoney" + aPolMonth, PubFun.setPrecision2("0.00")); // 本月期初帐户价值
			textTag.add("BQAddPrem" + aPolMonth, PubFun.setPrecision2("0.00")); // 本月追加保费
			textTag.add("BRAddPrem" + aPolMonth, PubFun.setPrecision2("0.00")); // 本月差额返还
			textTag.add("LXMoney" + aPolMonth, PubFun.setPrecision2("0.00")); // 保单帐户结算收益
			textTag.add("RiskPrem" + aPolMonth, PubFun.setPrecision2("0.00")); // 风险保险费
			textTag.add("EndMoney" + aPolMonth, PubFun.setPrecision2("0.00")); // 本月期末帐户价值
			textTag.add("PolMonth" + aPolMonth, SysConst.TEG_END);
			return ;
		}
		String tDueBalaDate = tSSRS.GetText(1, 1); // 应结算日期
		String tEndDate = tSSRS.GetText(1, 2); // 本月结束时间
		textTag.add("Month" + aPolMonth, decodeDate(tEndDate));
		String tempPolYear = tSSRS.GetText(1, 3);//当月的保单年度
		String tempPolMonth = tSSRS.GetText(1, 4);//月度
		String tStartMoney = "";
		String tEndMoney = "";
		if("1".equals(tempPolYear) && "1".equals(tempPolMonth))
        {
    		sql = "select nvl(sum(money),0) "
    	    + "From lcinsureacctrace where contno = '" 
        	+ mLCContSchema.getContNo() + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate <= '" + tStartDate + "' with ur ";
        }
        else
        {
        	sql = "select nvl(sum(case when paydate = '" + tStartDate + "' and othertype <> '6' then 0 else money end),0) "
    	    + "From lcinsureacctrace where contno = '" 
        	+ mLCContSchema.getContNo() + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate <= '" + tStartDate + "' with ur ";
        }
    	
        
		tStartMoney = mExeSQL.getOneValue(sql); //本月期初帐户价值
		if(null==tStartMoney || "".equals(tStartMoney)){
			mErrors.addOneError("获取月初账户账户价值失败！");
			return ;
		}
		textTag.add("StartMoney" + aPolMonth, PubFun.setPrecision2(tStartMoney)); // 本月期初帐户价值

		sql = "select nvl(sum(money),0) from lcinsureacctrace a where moneytype in ('ZF','BF') and contno = '"
				+ aContNo
				+ "' and polno ='"
				+ mLCPolSchema.getPolNo()
				+ "' and paydate between '"
				+ tStartDate
				+ "' and '"
				+ tEndDate
				+ "' and othertype = '"
				+ BQ.NOTICETYPE_P
				+ "' and exists (select 1 from lpedoritem where edortype = 'ZB' and edorno = a.otherno) with ur";
		textTag.add("BQAddPrem" + aPolMonth, PubFun.setPrecision2(mExeSQL
				.getOneValue(sql))); // 本月追加保费

		sql = "select nvl(sum(money),0) from lcinsureacctrace a where moneytype ='BR' and contno = '"
				+ aContNo
				+ "' and polno ='"
				+ mLCPolSchema.getPolNo()
				+ "' and paydate between '"
				+ tStartDate
				+ "' and '"
				+ tEndDate
				+ "' with ur";
		textTag.add("BRAddPrem" + aPolMonth, PubFun.setPrecision2(mExeSQL
				.getOneValue(sql))); // 本月差额返还

		sql = "select nvl(sum(case when moneytype = 'LX' then money end),0), "
				+ "nvl(sum(case when moneytype = 'RP' then -money end),0) "
				+ "from lcinsureacctrace a where contno = '"
				+ aContNo
				+ "' and polno ='"
				+ mLCPolSchema.getPolNo()
				+ "' and otherno in (select sequenceno from lcinsureaccbalance where " 
				+ " DueBalaDate = '"+ tDueBalaDate +"') with ur ";
		SSRS tSSRS2 = mExeSQL.execSQL(sql);
		textTag.add("LXMoney" + aPolMonth, PubFun.setPrecision2(tSSRS2.GetText(
				1, 1))); // 保单帐户结算收益
		textTag.add("RiskPrem" + aPolMonth, PubFun.setPrecision2(tSSRS2
				.GetText(1, 2))); // 风险保险费
		sql = "select nvl(sum(case when paydate = '" + tDueBalaDate + "' and othertype <> '6' then 0 else money end),0) "
        	+ "From lcinsureacctrace where contno = '" 
        	+ mLCContSchema.getContNo() + "' and polno ='"+mLCPolSchema.getPolNo()+"' and paydate <= '" + tDueBalaDate + "' with ur ";
        	
        tEndMoney =  mExeSQL.getOneValue(sql); //本月期末帐户价值
        if(null==tEndMoney || "".equals(tEndMoney)){
			mErrors.addOneError("获取月末账户价值失败！");
			return ;
		}
		textTag.add("EndMoney" + aPolMonth, PubFun.setPrecision2(tEndMoney)); // 本月期末帐户价值

		textTag.add("PolMonth" + aPolMonth, SysConst.TEG_END);
	}

	/**
	 * 把型如"2009-01-01"格式的日期转为"2009年01月"格式
	 * 
	 * @param date
	 *            String
	 * @return String
	 */
	private String decodeDate(String date) {
		if ((date == null) || (date.equals(""))) {
			System.out.println("日期格式不正确！");
			return "";
		}
		String data[] = date.split("-");
		if (data.length != 3) {
			System.out.println("日期格式不正确！");
			return "";
		}
		return (data[0] + "年" + data[1] + "月");
	}
}
