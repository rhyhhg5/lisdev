package com.sinosoft.lis.f1print;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GetFeePrintBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private String mSql = null;

    private String mOutXmlPath = null;

    private String StartDate = "";

    private String EndDate = "";

    private String mCurDate = PubFun.getCurrentDate();

    SSRS tSSRS = new SSRS();
    	
	private String[][] mToExcel = null;
	
	private String mMail = "";
       

    public GetFeePrintBL()
    {
    }

    public boolean submitData(VData cInputData, String operate)
    {

        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        
		if(!createFile()){
			return false;
		}        

        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    public boolean dealData()
    {
        System.out.println("BL->dealDate()");
        String dSql = "";
        String rSql = "";
        if (StartDate != null && !StartDate.equals(""))
        {
            dSql = " and gc.cvalidate >='" + StartDate + "'";
        }
        if(!"".equals(mMail) && mMail != null){
        	rSql = " and gc.Markettype not in ('1','9') ";
        }
        ExeSQL manExeSQL = new ExeSQL();
        String manSql = "select distinct managecom from ("
        	    +"select distinct gc.managecom from lcgrpcont gc Inner Join Lcgrppol Gp On Gc.Grpcontno = Gp.Grpcontno"
                + " where gc.payintv not in(-1,0,12) and gc.managecom like '"+ mGI.ManageCom+ "%' "
                + " and exists (select 1 from lmriskapp where riskcode = Gp.riskcode and riskperiod in ('S', 'M')) "
                + " and Exists (Select 1 From lidatatransresult Gpp Where contno =Gc.grpcontno and classid in('O-NC-000003','00000116','00000125') and riskcode=Gp.riskcode) "
                + " and gc.cvalidate <= '" + EndDate + "' " + dSql + rSql
                + " and LF_PAYMONEY(Gp.grppolno,Gp.paytodate,Gp.payenddate,Gp.payintv)<>0"
                + " and not exists(select 1 from lidatatransresult a where a.contno=Gc.grpcontno and classid in('Y-BQ-FC-000003','BDZF0001','00000143'))"
                + " and Gc.signdate is not null "
                + " ) as aa order by aa.managecom";
        SSRS manSSRS = manExeSQL.execSQL(manSql);
        if (manExeSQL.mErrors.needDealError())
        {
            System.out.println(manExeSQL.mErrors.getErrContent());
            buiError("dealData", manExeSQL.mErrors.getErrContent());
            return false;
        }
        if (manSSRS.getMaxRow() == 0)
        {
        	buiError("dealData", "没有符合条件的数据，请重新输入条件");
            return false;
        }

        mToExcel = new String[10000][30];
        mToExcel[0][0] = "中国人民健康保险股份有限公司";
        mToExcel[1][0] = EndDate.substring(0, EndDate.indexOf('-'))+ "年"+ EndDate.substring(EndDate.indexOf('-') + 1, EndDate.lastIndexOf('-')) + "月应收保费统计表";
        mToExcel[2][0] = "编制单位：" + getName();
        mToExcel[2][14] = "金额单位：元";

        mToExcel[3][0] = "序号";
        mToExcel[3][1] = "机构代码";
        mToExcel[3][2] = "机构名称";
        mToExcel[3][3] = "项目名称";
        mToExcel[3][4] = "险种代码";
        mToExcel[3][5] = "险种名称";
        mToExcel[3][6] = "投保单号";
        mToExcel[3][7] = "保单号";
        mToExcel[3][8] = "投保单位名称";
        mToExcel[3][9] = "市场类型";
        mToExcel[3][10] = "缴费频次";
        mToExcel[3][11] = "生效日期";
        mToExcel[3][12] = "终止日期";
        mToExcel[3][13] = "业务员";
        mToExcel[3][14] = "本方共保比例";
        mToExcel[3][15] = "保费收入金额";
        mToExcel[3][16] = "实收保费金额";
        mToExcel[3][17] = "应收保费金额";
        mToExcel[3][18] = "未逾期应收保费余额";
        mToExcel[3][19] = "逾期在三个月以内应收保费余额";
        mToExcel[3][20] = "逾期三个月至六个月应收保费余额";
        mToExcel[3][21] = "逾期六个月至一年应收保费余额";
        mToExcel[3][22] = "逾期一年以上应收保费余额";
        mToExcel[3][23] = "理赔金额";
       

        String polCom = manSSRS.GetText(1, 1).substring(0, 4) + "0000";
        int printNum = 3;
        //总合计
        double sumMoney = 0;     //总合计保费收入金额
        double sumActuMoney = 0; //总合计实收保费金额
        double sumShouMoney = 0; //总合计应收保费金额
        double sumASMoney = 0;   //总合计未逾期应收保费余额
        double sumSaMoney = 0;   //总合计逾期在三个月以内应收保费
        double sumSbMoney = 0;   //总合计逾期三个月至六个月应收保费
        double sumScMoney = 0;   //总合计逾期六个月至一年应收保费
        double sumSdMoney = 0;   //总合计逾期一年以上应收保费
        double sumPkMoney = 0;   //总合计赔款
        //分公司合计
        double fMoney = 0;     //分公司保费收入金额
        double fActuMoney = 0; //分公司实收保费金额
        double fShouMoney = 0; //分公司应收保费金额
        double fASMoney = 0;   //分公司未逾期应收保费余额
        double fSaMoney = 0;   //分公司逾期在三个月以内应收保费
        double fSbMoney = 0;   //分公司逾期三个月至六个月应收保费
        double fScMoney = 0;   //分公司逾期六个月至一年应收保费
        double fSdMoney = 0;   //分公司逾期一年以上应收保费
        double fPkMoney = 0;   //分公司赔款

        for (int comnum = 1; comnum <= manSSRS.getMaxRow(); comnum++)
        {
            ExeSQL tExeSQL = new ExeSQL();
            mSql = "select * from ("
            	   +" Select char(rownumber() over()) as id,Gc.Managecom 机构代码,(Select Name From Ldcom Where Comcode = Gc.Managecom) 机构名称,"
            	   +" (select ProjectName from LCGrpContSub where PrtNo = Gc.PrtNo) as 项目名称,"
                   +" Gp.Riskcode 险种代码,(Select Riskname From Lmriskapp Where Rtrim(Riskcode) = Gp.Riskcode) 险种名称,"
                   +" Gc.Prtno 投保单号,Gc.Grpcontno 保单号,Gc.Grpname 投保单位名称,(Select Codename From Ldcode Where Codetype = 'markettype' And Code = Gc.Markettype) 市场类型,"
                   +" (case Gc.payintv when 1 then '月缴' when 3 then '季缴' when 6 then '半年缴' else '查询错误' end)as 缴费频次,Gc.Cvalidate 生效日期,Gc.Cinvalidate 终止日期,(select name from laagent where agentcode=Gc.agentcode) as 业务员,"
                   +"(select 1-nvl(sum(rate),0) from LCCoInsuranceParam  where GrpContNo = Gc.GrpContNo) as 本方共保比例,"
                   +" (select  nvl(sum(case finitemtype when 'D' then -summoney else summoney end),0) from lidatatransresult where contno=Gc.grpcontno and accountcode='6031000000' and classtype in('N-05','X-03','X-04') and classid not in('00000116','00000125','00000117','00000145','00000143','O-NC-000003','O-XQ-000008','O-XQ-000011','Y-BQ-FC-000003') and riskcode=Gp.riskcode)+LF_PAYMONEY(Gp.grppolno,Gp.paytodate,Gp.payenddate,Gp.payintv) as 保费收入金额,"
                   +" (select  nvl(sum(case finitemtype when 'D' then -summoney else summoney end),0) from lidatatransresult where contno=Gc.grpcontno and accountcode='6031000000' and classtype in('N-05','X-03','X-04') and classid not in('00000116','00000125','00000117','00000145','00000143','O-NC-000003','O-XQ-000008','O-XQ-000011','Y-BQ-FC-000003','O-GB-000001','O-GB-XQ-000001') and riskcode=Gp.riskcode) 实收保费金额,"
                   +" LF_PAYMONEY(Gp.grppolno,Gp.paytodate,Gp.payenddate,Gp.payintv) 应收保费金额,"
                   +" LF_GETSHOUTMONEY(Gp.grppolno,Gp.payenddate,Gp.paytodate,Gp.payintv,'0',Current Date) 未逾期应收保费余额,"
                   +" LF_GETSHOUTMONEY(Gp.grppolno,Gp.payenddate,Gp.paytodate,Gp.payintv,'1',Current Date)  逾期在三个月以内,"
                   +" LF_GETSHOUTMONEY(Gp.grppolno,Gp.payenddate,Gp.paytodate,Gp.payintv,'2',Current Date) 逾期三个月至六个月,"
                   +" LF_GETSHOUTMONEY(Gp.grppolno,Gp.payenddate,Gp.paytodate,Gp.payintv,'3',Current Date) 逾期六个月至一年,"
                   +" LF_GETSHOUTMONEY(Gp.grppolno,Gp.payenddate,Gp.paytodate,Gp.payintv,'4',Current Date) 一年以上,"
                   +" (select abs((Select nvl(Sum(case finitemtype when 'D' then -summoney else summoney end),0) From lidatatransresult a  Where a.contno in(select distinct contno from ljagetclaim where grpcontno=Gc.Grpcontno and riskcode=Gp.Riskcode) And a.Riskcode = Gp.Riskcode And accountcode in('6511010100','1123010100') and classid not in('Y-LP-000002','YF000002'))"
                   +" +(Select nvl(Sum(case finitemtype when 'D' then -summoney else summoney end),0) From lidatatransresult a Where contno = Gc.Grpcontno  And Riskcode = Gp.Riskcode And accountcode in('6511010100','1123010100'))"
                   +" +(select nvl(sum(pay),0)  from ljagetclaim where grpcontno=Gc.Grpcontno  and riskcode=Gp.Riskcode and feefinatype='YF' and feeoperationtype='000')) from dual) 赔款"                  
                   +" From Lcgrpcont Gc Inner Join Lcgrppol Gp On Gc.Grpcontno = Gp.Grpcontno"
                   +" Where Gc.Payintv not in(0,12,-1) and Gc.Cvalidate <='"+EndDate+"'"+dSql + rSql
                   +" and exists (select 1 from lmriskapp where riskcode = Gp.riskcode and riskperiod in ('S', 'M'))"
                   +" and gc.managecom='"+manSSRS.GetText(comnum, 1)+"'"
                   +" And Gc.Signdate Is Not Null"
                   +" And Exists (Select 1 From lidatatransresult Gpp Where contno =Gc.grpcontno and classid in('O-NC-000003','00000116','00000125') and riskcode=Gp.riskcode) "
                   +" and LF_PAYMONEY(Gp.grppolno,Gp.paytodate,Gp.payenddate,Gp.payintv)<>0"
                   +" and not exists(select 1 from lidatatransresult a where a.contno=Gc.grpcontno and classid in('Y-BQ-FC-000003','BDZF0001','00000143'))"
		           +")as bb ";
            System.out.println(mSql);
            tSSRS = tExeSQL.execSQL(mSql);
            System.out.println(polCom + "--"+ tSSRS.GetText(tSSRS.getMaxRow(), 2));
            if (!polCom.equals(tSSRS.GetText(tSSRS.getMaxRow(), 2).substring(0,4)+ "0000"))
            {
                mToExcel[printNum + 1][0] = "分公司合计：";
                mToExcel[printNum + 1][15] = PubFun.format(fMoney);
                mToExcel[printNum + 1][16] = PubFun.format(fActuMoney);
                mToExcel[printNum + 1][17] = PubFun.format(fShouMoney);
                mToExcel[printNum + 1][18] = PubFun.format(fASMoney);
                mToExcel[printNum + 1][19] = PubFun.format(fSaMoney);
                mToExcel[printNum + 1][20] = PubFun.format(fSbMoney);
                mToExcel[printNum + 1][21] = PubFun.format(fScMoney);
                mToExcel[printNum + 1][22] = PubFun.format(fSdMoney);
                mToExcel[printNum + 1][23] = PubFun.format(fPkMoney);
           	    fMoney = 0;     //分公司保费收入金额
                fActuMoney = 0; //分公司实收保费金额
                fShouMoney = 0; //分公司应收保费金额
                fASMoney = 0;   //分公司未逾期应收保费余额
                fSaMoney = 0;   //分公司逾期在三个月以内应收保费
                fSbMoney = 0;   //分公司逾期三个月至六个月应收保费
                fScMoney = 0;   //分公司逾期六个月至一年应收保费
                fSdMoney = 0;   //分公司逾期一年以上应收保费
                fPkMoney = 0;   //分公司赔款
                printNum = printNum + 2;
            }
            polCom = tSSRS.GetText(tSSRS.getMaxRow(), 2).substring(0, 4)+ "0000";

            //支公司合计
            double zMoney = 0;     //分公司保费收入金额
            double zActuMoney = 0; //分公司实收保费金额
            double zShouMoney = 0; //分公司应收保费金额
            double zASMoney = 0;   //分公司未逾期应收保费余额
            double zSaMoney = 0;   //分公司逾期在三个月以内应收保费
            double zSbMoney = 0;   //分公司逾期三个月至六个月应收保费
            double zScMoney = 0;   //分公司逾期六个月至一年应收保费
            double zSdMoney = 0;   //分公司逾期一年以上应收保费
            double zPkMoney = 0;   //分公司赔款
            
            int no = 1;
            for (int row = 1; row <= tSSRS.getMaxRow(); row++)
            {
                for (int col = 1; col <= tSSRS.getMaxCol(); col++)
                {
                    if (col<16)
                    {
                        if (tSSRS.GetText(row, col).equals("null"))
                        {
                            mToExcel[row + printNum][col - 1] = "";
                        }else{
                            mToExcel[row + printNum][col - 1] = tSSRS.GetText(row, col);
                        }
                    }else
                    {
                        double finMoney = 0.0;
                      	double rate= Double.parseDouble(tSSRS.GetText(row,15));
                        if (tSSRS.GetText(row, col).equals("null"))
                        {
                        	mToExcel[row + printNum][col - 1] = "";
                        }else if(col!=16 && col != 17 && col != 24)
                        {
                        	finMoney = Double.parseDouble(tSSRS.GetText(row,col))*rate;     	
                        	mToExcel[row + printNum][col - 1] = PubFun.format(finMoney);
                        }else if(col==16){
                        	if(!tSSRS.GetText(row,17).equals("null")&&!tSSRS.GetText(row,18).equals("null")){
                        		finMoney=Double.parseDouble(tSSRS.GetText(row,17)) + Double.parseDouble(tSSRS.GetText(row,18))*rate;	
                        	}else if(tSSRS.GetText(row,17).equals("null")&&!tSSRS.GetText(row,18).equals("null")){
                        		finMoney=Double.parseDouble(tSSRS.GetText(row,18))*rate;
                        	}else if(!tSSRS.GetText(row,17).equals("null")&&tSSRS.GetText(row,18).equals("null")){
                        		finMoney=Double.parseDouble(tSSRS.GetText(row,17));
                        	}     	
                        	mToExcel[row + printNum][col - 1] = PubFun.format(finMoney);
                        }else
                        {    	
                        		finMoney = Double.parseDouble(tSSRS.GetText(row,col));
                        	mToExcel[row + printNum][col - 1] = PubFun.format(finMoney);
                        }
                        	//支公司
                        	if (col == 16) {zMoney = zMoney+finMoney;}
                        	if (col == 17) {zActuMoney = zActuMoney+finMoney;}
                        	if (col == 18) {zShouMoney = zShouMoney+finMoney;}
                        	if (col == 19) {zASMoney = zASMoney+finMoney;}
                        	if (col == 20) {zSaMoney = zSaMoney+finMoney;}
                        	if (col == 21) {zSbMoney = zSbMoney+finMoney;}
                        	if (col == 22) {zScMoney = zScMoney+finMoney;}
                        	if (col == 23) {zSdMoney = zSdMoney+finMoney;}
                        	if (col == 24) {zPkMoney = zPkMoney+finMoney;}

                            //分公司
                        	if (col == 16) {fMoney = fMoney+finMoney;}
                        	if (col == 17) {fActuMoney = fActuMoney+finMoney;}
                        	if (col == 18) {fShouMoney = fShouMoney+finMoney;}
                        	if (col == 19) {fASMoney = fASMoney+finMoney;}
                        	if (col == 20) {fSaMoney = fSaMoney+finMoney;}
                        	if (col == 21) {fSbMoney = fSbMoney+finMoney;}
                        	if (col == 22) {fScMoney = fScMoney+finMoney;}
                        	if (col == 23) {fSdMoney = fSdMoney+finMoney;}
                        	if (col == 24) {fPkMoney = fPkMoney+finMoney;}
                        	
                            //总公司
                        	if (col == 16) {sumMoney = sumMoney+finMoney;}
                        	if (col == 17) {sumActuMoney = sumActuMoney+finMoney;}
                        	if (col == 18) {sumShouMoney = sumShouMoney+finMoney;}
                        	if (col == 19) {sumASMoney = sumASMoney+finMoney;}
                        	if (col == 20) {sumSaMoney = sumSaMoney+finMoney;}
                        	if (col == 21) {sumSbMoney = sumSbMoney+finMoney;}
                        	if (col == 22) {sumScMoney = sumScMoney+finMoney;}
                        	if (col == 23) {sumSdMoney = sumSdMoney+finMoney;}
                        	if (col == 24) {sumPkMoney = sumPkMoney+finMoney;}
						}
                        
                    }
                mToExcel[row + printNum][0] = no + "";
                no++;
            }
            mToExcel[printNum + tSSRS.getMaxRow() + 1][0] = "支公司合计：";
            mToExcel[printNum + tSSRS.getMaxRow() + 1][15] = PubFun.format(zMoney);
            mToExcel[printNum + tSSRS.getMaxRow() + 1][16] = PubFun.format(zActuMoney);
            mToExcel[printNum + tSSRS.getMaxRow() + 1][17] = PubFun.format(zShouMoney);
            mToExcel[printNum + tSSRS.getMaxRow() + 1][18] = PubFun.format(zASMoney);
            mToExcel[printNum + tSSRS.getMaxRow() + 1][19] = PubFun.format(zSaMoney);
            mToExcel[printNum + tSSRS.getMaxRow() + 1][20] = PubFun.format(zSbMoney);
            mToExcel[printNum + tSSRS.getMaxRow() + 1][21] = PubFun.format(zScMoney);
            mToExcel[printNum + tSSRS.getMaxRow() + 1][22] = PubFun.format(zSdMoney);
            mToExcel[printNum + tSSRS.getMaxRow() + 1][23] = PubFun.format(zPkMoney);
            printNum = printNum + tSSRS.getMaxRow() + 1;
            tSSRS.Clear();
        }
        mToExcel[printNum + 1][0] = "分公司合计：";
        mToExcel[printNum + 1][15] = PubFun.format(fMoney);
        mToExcel[printNum + 1][16] = PubFun.format(fActuMoney);
        mToExcel[printNum + 1][17] = PubFun.format(fShouMoney);
        mToExcel[printNum + 1][18] = PubFun.format(fASMoney);
        mToExcel[printNum + 1][19] = PubFun.format(fSaMoney);
        mToExcel[printNum + 1][20] = PubFun.format(fSbMoney);
        mToExcel[printNum + 1][21] = PubFun.format(fScMoney);
        mToExcel[printNum + 1][22] = PubFun.format(fSdMoney);
        mToExcel[printNum + 1][23] = PubFun.format(fPkMoney);

        mToExcel[printNum + 3][0] = "总合计：";
        mToExcel[printNum + 3][15] = PubFun.format(sumMoney);
        mToExcel[printNum + 3][16] = PubFun.format(sumActuMoney);
        mToExcel[printNum + 3][17] = PubFun.format(sumShouMoney);
        mToExcel[printNum + 3][18] = PubFun.format(sumASMoney);
        mToExcel[printNum + 3][19] = PubFun.format(sumSaMoney);
        mToExcel[printNum + 3][20] = PubFun.format(sumSbMoney);
        mToExcel[printNum + 3][21] = PubFun.format(sumScMoney);
        mToExcel[printNum + 3][22] = PubFun.format(sumSdMoney);
        mToExcel[printNum + 3][23] = PubFun.format(sumPkMoney);

        mToExcel[printNum + 5][0] = "制表";
        mToExcel[printNum + 5][14] = "日期：" + mCurDate;

        return true;
    }
    
	private boolean createFile(){
		
		try {
			System.out.println(mOutXmlPath);
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate() };
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(mOutXmlPath);
			System.out.println("生成文件完成");
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}
		
		return true;
	}

	public String[][] getMToExcel() {
		return mToExcel;
	}
	
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    public boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
        System.out.println(mGI.ManageCom);

        if (mGI == null || tf == null)
        {
        	buiError("getInputData", "传入信息不完整！");
            return false;
        }
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        StartDate = (String) tf.getValueByName("StartDate");
        EndDate = (String) tf.getValueByName("EndDate");
        mMail = (String) tf.getValueByName("toMail");
        
        System.out.println("mMail-----"+mMail);

        if (EndDate == null || mOutXmlPath == null)
        {
            buiError("getInputData", "传入信息不完整！");
            return false;
        }
        return true;
    }

    private String getName()
    {
        String tSQL = "";
        String tRtValue = "";
        tSQL = "select name from ldcom where comcode='" + mGI.ManageCom + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tSSRS.getMaxRow() == 0)
        {
            tRtValue = "";
        }
        else
            tRtValue = tSSRS.GetText(1, 1);
        return tRtValue;
    }
    private void buiError(String functionName,String errorMessage){
    	CError tError = new CError();
        tError.moduleName = "GetFeePrintBL";
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        mErrors.addOneError(tError);
    }
    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "cwad";
        tG.ComCode = "86";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("OutXmlPath", "D:\\test\\a.xls");
        tTransferData.setNameAndValue("EndDate", "2012-05-01");
        tTransferData.setNameAndValue("StartDate", "");
        VData vData = new VData();
        vData.add(tG);
        vData.add(tTransferData);
        PreRecPrintUI tPreRecPrintUI = new PreRecPrintUI();
        if (!tPreRecPrintUI.submitData(vData, ""))
        {
            System.out.print("失败！");
        }
        //        System.out.println(PubFun.format(111111111111111.0));
    }
}
