package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BlankDateCTQueryBL {


	  /** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public CErrors mErrors=new CErrors();

	  private VData mResult = new VData();
	  
	  private CreateExcelList mCreateExcelList = new CreateExcelList("");
	  
	  private TransferData mTransferData = new TransferData();
	  
	  private GlobalInput mGlobalInput =new GlobalInput();
	  
	//  实收起始日期
	  private String mActuStartDate="";
	  private String mActuEndDate="";
	//  其他要素
	  private String mSaleChnlType = "";
	  private String mManageCom=""; 
	  
	//  子算法SQL
	  private String msubpolsql = "";
	  private String msubpaypersonsql=""; 
	  private String mactupaypersonsql=" 1=1 "; 
	  
	  //查询出的问题保单
	  private String[][] mExcelData=null;
	  
	  public BlankDateCTQueryBL() {
	  }

	/**
	  传输数据的公共方法
	*/
	    public CreateExcelList getsubmitData(VData cInputData)
	    {


	      // 得到外部传入的数据，将数据备份到本类中
	      if( !getInputData(cInputData) ) {
	        return null;
	      }

	   // 获取各项条件SQL
	      getsubsql();
	      
	      // 准备所有要打印的数据
	      if( !getPrintData() ) {
	        return null;	
	      }



	      if(mCreateExcelList==null){
	    	  buildError("submitData", "Excel数据为空");
	          return null;
	      }
	      return mCreateExcelList;
	    }
	  
	  public static void main(String[] args)
	  {
	      
		  PAchieveRateBL tbl =new PAchieveRateBL();
	      GlobalInput tGlobalInput = new GlobalInput();
	      TransferData tTransferData = new TransferData();
	      tTransferData.setNameAndValue("StartDate", "2010-10-08");
	      tTransferData.setNameAndValue("EndDate", "2010-10-08");
	      tTransferData.setNameAndValue("ManageCom", "8644");
	      tGlobalInput.ManageCom="8644";
	      tGlobalInput.Operator="xp";
	      VData tData = new VData();
	      tData.add(tGlobalInput);
	      tData.add(tTransferData);

	      CreateExcelList tCreateExcelList=new CreateExcelList();
	      tCreateExcelList=tbl.getsubmitData(tData,"1");
	      if(tCreateExcelList==null){
	    	  System.out.println("112321231");
	      }
	      else{
	      try{
	    	  tCreateExcelList.write("c:\\contactcompare.xls");
	      }catch(Exception e)
	      {
	    	  System.out.println("EXCEL生成失败！");
	      }}
	  }

	  /**
	   * 获取所有条件子SQL
	   */
	  private void getsubsql()
	  {
//			管理机构
		  msubpolsql=" and lcp.managecom like '"+mManageCom+"%' ";
	      msubpaypersonsql=" and ljsp.managecom like '"+mManageCom+"%' ";
	      
	      
//	    实收时间
	      if( (!(mActuStartDate.equals("")||mActuStartDate==null))&&(!(mActuEndDate.equals("")||mActuEndDate==null)))  {
//	    	  msubpolsql=" and exists (select 1 from ljapayperson where polno=lcp.polno and confdate between '" + mActuStartDate + "' and '" + mActuEndDate + "') ";
//	    	  msubpolsql+=" and 1=2 ";
//	        msubpaypersonsql+=" and ljsp.confdate between '" + mActuStartDate + "' and '" + mActuEndDate + "' ";
//	    	  实收的保单仅影响实收费用
	    	  mactupaypersonsql = "  ljsp.confdate between '" + mActuStartDate + "' and '" + mActuEndDate + "' ";
	        }
	       
	      
	      if(mSaleChnlType.equals("1")){
	    	  msubpolsql+=" and lcp.salechnl not in ('04','13') ";
	          msubpaypersonsql+="  and exists (select 1 from lccont where contno=ljsp.contno and  salechnl not in ('04','13') union select 1 from lbcont where contno=ljsp.contno and  salechnl not in ('04','13'))  ";
	      }
	      else if(mSaleChnlType.equals("2")){
	    	  msubpolsql+=" and lcp.salechnl in ('04','13') ";
	          msubpaypersonsql+=" and exists (select 1 from lccont where contno=ljsp.contno and  salechnl  in ('04','13')  union select 1 from lbcont where contno=ljsp.contno and  salechnl in ('04','13')) ";
	      }
	      
	  }

	  
	  /**
	   * 从输入数据中得到所有对象
	   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	   */
	  private boolean getInputData(VData cInputData)
	  {
	//全局变量
		  mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
	    if( mGlobalInput==null ) {
	      buildError("getInputData", "没有得到足够的信息！");
	      return false;
	    }
	    mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);

	    mManageCom = (String) mTransferData.getValueByName("ManageCom");
	    if( mManageCom.equals("")||mManageCom==null) {
	        buildError("getInputData", "没有得到足够的信息:管理机构不能为空！");
	        return false;
	      }
	    mActuStartDate = (String) mTransferData.getValueByName("DueStartDate");
	    mActuEndDate = (String) mTransferData.getValueByName("DueEndDate");
	    if( (!(mActuStartDate.equals("")||mActuStartDate==null))&&(mActuEndDate.equals("")||mActuEndDate==null) ) {
	        buildError("getInputData", "没有得到足够的信息:应收起始和终止日期必须同时有值！");
	        return false;
	      }
	    if( (!(mActuEndDate.equals("")||mActuEndDate==null))&&(mActuStartDate.equals("")||mActuStartDate==null) ) {
	        buildError("getInputData", "没有得到足够的信息:应收起始和终止日期必须同时有值！");
	        return false;
	      }

	    mSaleChnlType = (String) mTransferData.getValueByName("SaleChnlType");
	    if( mSaleChnlType.equals("")||mSaleChnlType==null) {
	        buildError("getInputData", "没有得到足够的信息:保单类型不能为空！");
	        return false;
	      }

	    return true;
	  }

	  public VData getResult()
	  {
	    return mResult;
	  }

	  public CErrors getErrors()
	  {
	    return mErrors;
	  }

	  private void buildError(String szFunc, String szErrMsg)
	  {
	    CError cError = new CError( );

	    cError.moduleName = "LCGrpContF1PBL";
	    cError.functionName = szFunc;
	    cError.errorMessage = szErrMsg;
	    this.mErrors.addOneError(cError);
	  }

	  private boolean getPrintData()
	  {

		  //创建EXCEL列表
	      mCreateExcelList.createExcelFile();
	      String[] sheetName ={"list"};
	      mCreateExcelList.addSheet(sheetName);
	      
	      //设置表头
	      String[][] tTitle = {{"保单生效日期","退保日期","保单号","投保人号","投保人姓名",
	    	  					"险种号","险种名称","缴费方式","期缴保费","退保金额",
	    	  					"销售渠道","出单网点"}};
//	    表头的显示属性
	      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12};
//	    数据的显示属性
	      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12};
	     
	      int row = mCreateExcelList.setData(tTitle,displayTitle);
	      if(row ==-1) {
	    	  buildError("getPrintData", "EXCEL中指定数据失败！");
	          return false;
	      }
	      
	      //获得EXCEL列信息      
	      String tSQL="  select (select cvalidate from lccont where contno=a.contno union all select cvalidate from lbcont where contno=a.contno ) 保单生效日期,"+
      " b.confdate 退保日期, a.contno 保单号,  c.appntno 投保人号,  c.appntname 投保人姓名,  c.riskcode 险种号, "+
       "(select riskname from lmriskapp where riskcode=c.riskcode) 险种名称, "+
       "(select codename from ldcode where codetype='paymode'   and code=c.paymode) 缴费方式,"+
      " c.prem 期缴保费, a.getmoney 退保金额,"+
      " (select codename from ldcode where codetype='salechnl' 	and code=c.SaleChnl) 销售渠道, "+
      "  ( select name from lacom where branchtype ='3' and branchtype2 ='01' and banktype = '04'  " +
      "  and agentcom= c.agentcom) 出单网点 " +
      "  from lpedoritem a,lpedorapp b,lbpol c"+
      "  where a.edorno=b.edoracceptno " +
      " and c.edorno=a.edorno " +
      "  and b.confdate between '"+mActuStartDate+"' and '"+mActuEndDate+"' " +
      "  and exists (select 1 from lbpol where edorno=a.edorno and conttype='1') " +
      " and b.edorstate='0' " ;
      if("1".equals(mSaleChnlType)){
    	  tSQL = tSQL+  "   and a.edortype='WT'";
      }else if("2".equals(mSaleChnlType)){
    	  tSQL = tSQL+  "   and a.edortype='LQ'";
      }else if("3".equals(mSaleChnlType)){  
    	  tSQL = tSQL+  "   and a.edortype='XT'";
      }else if("4".equals(mSaleChnlType)){  
    	  tSQL = tSQL+  "   and a.edortype='CT'";
      }
      else{
    	  tSQL =  tSQL+ "  and edortype in ('CT' ,'WT','XT','LQ') " ;
      }

      tSQL = tSQL+
      "   and a.managecom like '"+mManageCom+"%' " +
      "  with ur";
	      
	      ExeSQL tExeSQL = new ExeSQL();
	      SSRS tSSRS = tExeSQL.execSQL(tSQL);
	      if (tExeSQL.mErrors.needDealError()) {
	    	  CError tError = new CError();
	    	  tError.moduleName = "CreateExcelList";
	    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
	    	  tError.errorMessage = "没有查询到需要下载的数据";
	    	  mErrors.addOneError(tError);
	    	  return false;
	      }
	      
	      mExcelData=tSSRS.getAllData();
	      if(mExcelData==null){
	    	  CError tError = new CError();
	    	  tError.moduleName = "CreateExcelList";
	    	  tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
	    	  tError.errorMessage = "没有查询到需要输出的数据";
	    	  return false;
	      }

//	      for (int j = 0; j < mExcelData.length;j++) {
//	    	  if(Double.parseDouble(mExcelData[j][4])!=0)
//	    	  {
//	    	  mExcelData[j][12]=String.valueOf(PubFun.setPrecision(Double.parseDouble(mExcelData[j][6])/Double.parseDouble(mExcelData[j][4]), "0.0000"));
//	    	  mExcelData[j][14]=String.valueOf(PubFun.setPrecision(Double.parseDouble(mExcelData[j][8])/Double.parseDouble(mExcelData[j][4]), "0.0000"));
//	    	  mExcelData[j][15]=String.valueOf(PubFun.setPrecision(Double.parseDouble(mExcelData[j][10])/Double.parseDouble(mExcelData[j][4]), "0.0000"));
//	    	  }
//	    	  if(Double.parseDouble(mExcelData[j][5])!=0)
//	    	  {
//	    	  mExcelData[j][13]=String.valueOf(PubFun.setPrecision(Double.parseDouble(mExcelData[j][7])/Double.parseDouble(mExcelData[j][5]), "0.0000"));
//	    	  }
//	      }
	      
	      if(mCreateExcelList.setData(mExcelData,displayData)==-1){
	    	  buildError("getPrintData", "EXCEL中设置数据失败！");
	          return false;
	      }

	    return true;
	  }

	  
//	  /**
//	   * 得到通过传入参数得到相关信息
//	   * @param strComCode
//	   * @return
//	   * @throws Exception
//	   */
//	  private boolean getExcelData()
//	  {
//
//		  String tSQL="select * from lccont lcc where conttype='1' and appflag='1' and stateflag='1'" +
//		  		" and managecom like '' and signdate between '' and '' with ur ";
//		  ExeSQL tExeSQL = new ExeSQL();
//	      SSRS tSSRS = tExeSQL.execSQL(tSQL);
//	      if (tExeSQL.mErrors.needDealError()) {
//	          CError tError = new CError();
//	          tError.moduleName = "CreateExcelList";
//	          tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
//	          tError.errorMessage = "没有查询到需要下载的数据";
//	          mErrors.addOneError(tError);
//	          return false;
//	      }
//	      mExcelData=tSSRS.getAllData();
//	      if(mExcelData==null){
//	    	  CError tError = new CError();
//	          tError.moduleName = "CreateExcelList";
//	          tError.functionName = "setData(String aQuerySql,int []aColAttribute)";
//	          tError.errorMessage = "没有查询到需要下载的数据";
//	    	  return false;
//	      }
//	      return true;
//	  }
	  
	  

}
