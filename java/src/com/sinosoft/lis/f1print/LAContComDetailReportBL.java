package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.AgentPubFun;
import java.math.BigDecimal;
import com.sinosoft.lis.pubfun.Arith;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LAContComDetailReportBL {

    public CErrors mErrors = new CErrors();

    private VData mInputData = new VData();

    private VData mResult = new VData();

    private String mOperate = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();
    private String mManageCom = "";
    private String mBranchAttr = "";
    private String mAgentGroup ="";
    private String mStartDate1 = "";
    private String mEndDate1 = "";
    private String mStartDate2 = "";
    private String mEndDate2 = "";

    private String  mAgentStateMin ;
    private String  mAgentStateMax ;
    private XmlExport mXmlExport = null;

    private SSRS mSSRS1 = new SSRS();
    private SSRS mSSRS2 = new SSRS();
    private ListTable mListTable = new ListTable();

    private PubFun mPubFun = new PubFun();

    private String mManageName = "";

    public LAContComDetailReportBL() {
    }
    public static void main(String[] args)
    {
        //
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";

        LAContComDetailReportBL tLAContComDetailReportBL = new LAContComDetailReportBL();
       // tLAContComDetailReportBL.submitData(tVData, "");

    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mOperate = cOperate;
        mInputData = (VData) cInputData;
        if (mOperate.equals("")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!this.getInputData(mInputData)) {
            return false;
        }

        if (!dealdate()) {
            return false;
        }

        if (!getPrintData()) {
            this.bulidError("getPrintData", "查询数据失败！");
            return false;
        }

        return true;
    }

    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
      try
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            this.mManageCom = (String) mTransferData.getValueByName("tManageCom");
            this.mAgentGroup = (String) mTransferData.getValueByName("tAgentGroup");
            this.mBranchAttr = (String) mTransferData.getValueByName("tBranchAttr");
            this.mStartDate1 = (String) mTransferData.getValueByName("tStartDate1");
            this.mEndDate1 = (String) mTransferData.getValueByName("tEndDate1");
            this.mStartDate2 = (String) mTransferData.getValueByName("tStartDate2");
            this.mEndDate2 = (String) mTransferData.getValueByName("tEndDate2");
        } catch (Exception ex)
        {
            this.mErrors.addOneError("");
            return false;
        }

        return true;
    }
    /**
    * 业务处理方法
    * @return boolean
    */

   private boolean dealdate()
   {
      if(!getAgent())
      {
          return false;
      }

      return true;
   }
   /**
   * 得到人员
   * @return boolean
   */
   private boolean getAgent()
   {
       String tSQL = "select distinct b.agentcode,b.name  from  lacommision a,laagent b "
                     + " where a.agentcode=b.agentcode  "
                     + " and a.branchtype='1' and a.branchtype2='01'  "
                     + " and b.branchtype='1' and b.branchtype2='01'  "
                     +
                     " and a.paycount=1 and a.renewcount=0 and transtype='ZC' " //第一次正常交费( 新单)
                     + " and a.managecom like '" + mManageCom + "%' "
                     + " and a.signdate<='" + mEndDate2 + "' ";
       if (mStartDate2 != null && !mStartDate2.equals("")) {
           tSQL += " and a.signdate>='" + mStartDate2 + "' ";
       }
       if (mAgentGroup != null && !mAgentGroup.equals("")) {
           tSQL += " and a.branchseries like '%" + mAgentGroup + "%' ";
       }
       tSQL += " order by  b.agentcode  ";
       ExeSQL tExeSQL = new ExeSQL();
       mSSRS1 = tExeSQL.execSQL(tSQL);
       if(mSSRS1.getMaxRow()<=0)
       {
           bulidError("getagent","没有符合条件的信息!");
           return false;
       }

       return true;

   }
   /**
   * 得到
   * 一个人的保费
   * @return boolean
   */
   private boolean getPrem(String tAgentCode)
   {
       //查询正常保单
       SSRS tSSRS1 = new SSRS();
       SSRS tSSRS2 = new SSRS();
       SSRS tSSRS3 = new SSRS();
       SSRS tSSRS4 = new SSRS();
       String tSQL = "select a.contno,sum(a.transmoney),a.scandate,a.signdate "
                     +
                     " from  lacommision a,lccont b where   a.p14=b.prtno   "
                     + "and a.branchtype='1' and a.branchtype2='01'  "
                     +
                     " and a.paycount=1 and a.renewcount=0 and transtype='ZC' " //第一次正常交费( 新单)
                     + " and (b.cardflag='0' or  b.cardflag is null )" //正常保单
                     + " and a.managecom like '" + mManageCom +
                     "%' and a.agentcode='" + tAgentCode + "'   "
                     + " and a.scandate>='" + mStartDate1 +
                     "' and a.scandate<='" + mEndDate1 + "' "
                     + " and a.signdate<='" + mEndDate2 + "' and "
                     + " b.contno not in (select aa.contno from LCRnewStateLog aa "
                     + " where aa.grpcontno='00000000000000000000'  ) ";

       if (mStartDate2 != null && !mStartDate2.equals("")) {
           tSQL += " and a.signdate>='" + mStartDate2 + "' ";
       }
       if (mAgentGroup != null && !mAgentGroup.equals("")) {
           tSQL += " and a.branchseries like '%" + mAgentGroup + "%' ";
       }
       tSQL += " group by a.contno ,a.signdate,a.scandate order by a.contno";
       ExeSQL tExeSQL = new ExeSQL();
       tSSRS1 = tExeSQL.execSQL(tSQL);
       System.out.println("121212121212121212121212121212" + tSQL);
       //查询lbont备份表
       tSQL = "select a.contno,sum(a.transmoney),a.scandate,a.signdate  "
              + " from  lacommision a,lbcont b where  a.p14=b.prtno   "
              + " and a.branchtype='1' and a.branchtype2='01'  "
              + " and a.paycount=1 and a.renewcount=0 and transtype='ZC' " //第一次正常交费( 新单)
              + " and   (b.cardflag='0' or b.cardflag is null ) " //正常保单
              + " and a.managecom like '" + mManageCom + "%' and a.agentcode='" +
              tAgentCode + "'   "
              + " and a.scandate>='" + mStartDate1 + "' and a.scandate<='" +
              mEndDate1 + "' "
              + " and a.signdate<='" + mEndDate2 + "' and "
              + " b.contno not in (select aa.contno from LCRnewStateLog aa "
              + " where aa.grpcontno='00000000000000000000'  ) ";

       if (mStartDate2 != null && !mStartDate2.equals("")) {
           tSQL += " and a.signdate>='" + mStartDate2 + "' ";
       }
       if (mAgentGroup != null && !mAgentGroup.equals("")) {
           tSQL += " and a.branchseries like '%" + mAgentGroup + "%' ";
       }
       tSQL += " group by a.contno ,a.signdate,a.scandate order by a.contno";
       tExeSQL = new ExeSQL();
       tSSRS2 = tExeSQL.execSQL(tSQL);
       System.out.println("121212121212121212121212121212" + tSQL);

       //查询简易保单------------------------------------------------------------
       tSQL = "select  a.contno,sum(a.transmoney),b.makedate,a.signdate   "
              + " from  lacommision a,lccont b where   a.p14=b.prtno   "
              + " and a.branchtype='1' and a.branchtype2='01'  "
              + " and a.paycount=1 and a.renewcount=0 and transtype='ZC' " //第一次正常交费( 新单)
              + " and b.cardflag in ('1','2','3','4','15') " //简易保单
              + " and a.managecom like '" + mManageCom + "%' and a.agentcode='" +
              tAgentCode + "'   "
              + " and b.makedate>='" + mStartDate1 + "' and b.makedate<='" +
              mEndDate1 + "' "
              + " and a.signdate<='" + mEndDate2 + "' and "
              + " b.contno not in (select aa.contno from LCRnewStateLog aa "
              + " where aa.grpcontno='00000000000000000000'  ) ";

       if (mStartDate2 != null && !mStartDate2.equals("")) {
           tSQL += " and a.signdate>='" + mStartDate2 + "' ";
       }
       if (mAgentGroup != null && !mAgentGroup.equals("")) {
           tSQL += " and a.branchseries like '%" + mAgentGroup + "%' ";
       }
       tSQL += " group by a.contno ,a.signdate,b.makedate order by a.contno";
       tExeSQL = new ExeSQL();
       tSSRS3 = tExeSQL.execSQL(tSQL);
       System.out.println("121212121212121212121212121212" + tSQL);
       //简易保单备份信息
       tSQL = "select a.contno,sum(a.transmoney),b.makedate,a.signdate "
              + " from  lacommision a,lbcont b where   a.p14=b.prtno   "
              + " and a.branchtype='1' and a.branchtype2='01'  "
              + " and a.paycount=1 and a.renewcount=0 and transtype='ZC' " //第一次正常交费( 新单)
              + " and b.cardflag in ('1','2','3','4','15') " //简易保单
              + " and a.managecom like '" + mManageCom + "%' and a.agentcode='" +
              tAgentCode + "'   "
              + " and b.makedate>='" + mStartDate1 + "' and b.makedate<='" +
              mEndDate1 + "' "
              + " and a.signdate<='" + mEndDate2 + "' and "
              + " b.contno not in (select aa.contno from LCRnewStateLog aa "
              + " where aa.grpcontno='00000000000000000000'  ) ";

       if (mStartDate2 != null && !mStartDate2.equals("")) {
           tSQL += " and a.signdate>='" + mStartDate2 + "' ";
       }
       if (mAgentGroup != null && !mAgentGroup.equals("")) {
           tSQL += " and a.branchseries like '%" + mAgentGroup + "%' ";
       }
       tSQL += " group by a.contno ,a.signdate,b.makedate order by a.contno";
       tExeSQL = new ExeSQL();
       tSSRS4 = tExeSQL.execSQL(tSQL);
       System.out.println("121212121212121212121212121212" + tSQL);
       mSSRS2 = new SSRS();
       mSSRS2.setMaxCol(4);
       mSSRS2.addRow(tSSRS1);
       mSSRS2.addRow(tSSRS2);
       mSSRS2.addRow(tSSRS3);
       mSSRS2.addRow(tSSRS4);
       return true ;
   }

    /**
     *
     * @return boolean
     */
    private boolean getPrintData() {
        TextTag tTextTag = new TextTag();

        mXmlExport = new XmlExport();

        mXmlExport.createDocument("LAContComDetailReport.vts", "printer");

        String tMakeDate = "";
        String tMakeTime = "";

        tMakeDate = mPubFun.getCurrentDate();
        tMakeTime = mPubFun.getCurrentTime();
        tTextTag.add("MakeDate", tMakeDate);
        tTextTag.add("MakeTime", tMakeTime);
        tTextTag.add("tName", mManageName);

        System.out.println("121212121212121212121212121212" + tMakeDate);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] title = {"", "", "", "", "",""  };

        if (!getListTable()) {
            return false;
        }
        System.out.print("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.print("121");
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 查询列表显示数据
     * @return boolean
     */
    private boolean getListTable()
    {
        if (mSSRS1.getMaxRow() >= 1)
        {
            //此人有职级变更
            boolean tFlag = false;//判断是否所有人业绩为0
            for (int i = 1; i <= mSSRS1.getMaxRow(); i++)
            {
                String tAgentCode = mSSRS1.GetText(i,1);
                if(!getPrem(tAgentCode))
                {
                    bulidError("getListTable","查询出错!");
                    return false;
                }
                if(mSSRS2.getMaxRow()<=0)
                {
                   continue;
                }
                for(int k=1;k<=mSSRS2.getMaxRow();k++)
                {
                    String Info[] = new String[6];
                    Info[0] = mSSRS1.GetText(i, 1);
                    Info[1] = mSSRS1.GetText(i, 2);
                    Info[2] = mSSRS2.GetText(k,1);
                    Info[3] = mSSRS2.GetText(k,2);
                    Info[4] = mSSRS2.GetText(k,3);
                    Info[5] = mSSRS2.GetText(k,4);
                    tFlag = true; //
                    mListTable.add(Info);
                }
            }
            if(tFlag==false)
            {   //所有人的业绩为0
                bulidError("getListTable","没有符合条件的信息!");
                return false;
            }
        }
        else
        {
            bulidError("getListTable","没有符合条件的信息!");
            return false;
        }
        mListTable.setName("ZT");

        return true;
    }


    /**
     * 获取打印所需要的数据
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg)
    {
        CError tCError = new CError();
        tCError.moduleName = "LAContComDetailReportBL";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;
        this.mErrors.addOneError(tCError);
    }

    /**
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


}
