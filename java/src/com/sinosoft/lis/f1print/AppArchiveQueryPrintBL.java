package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.TextTag;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class AppArchiveQueryPrintBL
{


    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private VData mInputData = new VData();

    private String mOperate = "";

    private TransferData mTransferData = new TransferData();

    private GlobalInput mGlobalInput = new GlobalInput();

    private String minArchiveNo;//最小归档号
    private String maxArchiveNo;//最大归档好

    private String mSQL = ""; //查询语句

    private String ManageCom = "";

    private SSRS mSSRS = new SSRS();


    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mInputData = cInputData;
        this.mOperate = cOperate;
        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "不支持的操作字符串");
            return false;
        }

        //外部传入数据
        if (!getInputData(mInputData)) {
            this.bulidError("getInputData", "数据不完整");
            return false;
        }

        //数据合法性
        if (!getListTable()) {
            return false;
        }

        //获取打印数据
        if (!getPrintData()) {
            return false;
        }

        return true;
    }

    /**
     * 接收数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        try {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName(
                                           "GlobalInput", 0));

            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
        } catch (Exception e) {
            this.mErrors.addOneError("数据不完整");
            return false;
        }
        mSQL = (String) mTransferData.getValueByName("SQL");
        minArchiveNo = (String) mTransferData.getValueByName("minArchiveNo");
        maxArchiveNo = (String) mTransferData.getValueByName("maxArchiveNo");

        System.out.println("SQL : " + mSQL);
        this.ManageCom = mGlobalInput.ManageCom;

        return true;
    }

    /**
     * 查询数据
     * @return boolean
     */
    private boolean getListTable() {

        String sql =  mSQL;

        ExeSQL tExeSQL = new ExeSQL();
        System.out.println( sql);
        mSSRS = tExeSQL.execSQL(sql.toString());

        if(mSSRS.getMaxRow() < 1)
        {
            CError error = new CError();
            error.moduleName = "AppArchiveQueryPrintBL";
            error.functionName = "getListTable";
            error.errorMessage = "没有查询结果！";
            this.mErrors.addOneError(error);

            return false;
        }
        //System.out.println(mSSRS.MaxCol + "****" + mSSRS.MaxNumber + "****" + mSSRS.MaxRow);

        if (tExeSQL.mErrors.needDealError()) {
            CError error = new CError();
            error.moduleName = "MakeXMLBL";
            error.functionName = "getListTable";
            error.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(error);

            return false;
        }

        return true;
    }

    /**
     * 将数据存放到XmlExport中
     * @return boolean
     */
    private boolean getPrintData() {

        XmlExport tXmlExport = new XmlExport();
        tXmlExport.createDocument("AppArchive.vts", "printer");

        TextTag tTextTag = new TextTag();

        tTextTag.add("ManageName", this.getManageName()); //机构
        tTextTag.add("PrintDate", PubFun.getCurrentDate()); //打印时间
        tTextTag.add("PrintOperator", mGlobalInput.Operator); //打印人
        tTextTag.add("minArchiveNo", minArchiveNo); //最小档案号
        tTextTag.add("maxArchiveNo", maxArchiveNo); //最大档案号
        tTextTag.add("number", mSSRS.getMaxRow()); //份数

        if (tTextTag.size() > 1) {
            tXmlExport.addTextTag(tTextTag);
        }

        String[] title = {"档案号", "印刷号", "投保人", "业务员姓名", "页数"};

        tXmlExport.addListTable(this.getListTableData(), title);

//        tXmlExport.outputDocumentToFile("c:\\", "new");

        mResult.clear();

        mResult.add(tXmlExport);

        return true;
    }

    /**
     * 将数据存放到ListTable中
     * @return ListTable
     */
    private ListTable getListTableData() {
        ListTable tListTable = new ListTable();

        tListTable.setName("BB");

        if (mSSRS.getMaxRow() > 0) {

            for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
                String info[] = new String[5];

                info[0] = mSSRS.GetText(i, 1);
                info[1] = mSSRS.GetText(i, 2);
                info[2] = mSSRS.GetText(i, 3);
                info[3] = mSSRS.GetText(i, 4);
                info[4] = mSSRS.GetText(i, 5);

                tListTable.add(info);

            }

        }

        return tListTable;
    }

    /**
     * 写入错误消息
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg) {
        CError error = new CError();

        error.moduleName = "AppArchiveQueryPrintBL";
        error.functionName = cFunction;
        error.errorMessage = cErrorMsg;

        this.mErrors.addOneError(error);

    }

    /**
     * 查询机构名称
     * @return String
     */
    private String getManageName() {
        String ManageName = "";
        String sql = "";

        sql = "select name from ldcom where comcode='" + ManageCom + "'";

        SSRS tSSRS = new SSRS();

        ExeSQL tExeSQL = new ExeSQL();

        tSSRS = tExeSQL.execSQL(sql);
        System.out.println(sql);

        if (tExeSQL.mErrors.needDealError()) {
            CError error = new CError();
            error.moduleName = "GrpConPayListPrintBL";
            error.functionName = "getManageName";
            error.errorMessage = "机构名不存在！";
            this.mErrors.addOneError(error);
        }

        if (tSSRS.getMaxRow() > 0) {
            System.out.println(tSSRS.GetText(1, 1));
            ManageName = tSSRS.GetText(1, 1);
        } else {
            CError error = new CError();
            error.moduleName = "GrpConPayListPrintBL";
            error.functionName = "getManageName";
            error.errorMessage = "机构名不存在！";
            this.mErrors.addOneError(error);
        }

        return ManageName;
    }

    /**
     * 获得结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    /**
     * 组合 SQL （待完善）
     * @return String
     */
    private String getSql() {
        String sql = "";

        return sql;
    }

    public static void main(String[] args) {

    }
}
