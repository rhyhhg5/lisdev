package com.sinosoft.lis.f1print;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.FIVoucherDataDetailSchema;

public class ShieldBorUnevenBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private String BatchNo = "";
    private String ContNo = "";
    private String ManageCom = "";
    private String VoucherType = "";
    private String CheckF = "";
    private String AccountDate = "";
    
    private String mCurDate = PubFun.getCurrentDate();
    	
	private String[][] mToExcel = null;
	
	private String mMail = "";
       

    public ShieldBorUnevenBL()
    {
    }

    public boolean submitData(VData cInputData, String operate) 
    {

        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        
		if(!createFile()){
			return false;
		}        

        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    public boolean dealData()
    {
        System.out.println("BL->dealDate()"); 
    	//提交后台处理
        MMap tmap = new MMap();
        VData tInputData = new VData();
        System.out.println("CheckFlag : "+CheckF);
        
        if (VoucherType != null && !VoucherType.equals("") && ManageCom != null && !ManageCom.equals("")) {
        	tmap.put("update FIVoucherDataDetail set CheckFlag = '"+CheckF+"'where batchno = '"+BatchNo+"' and contno = '"+ContNo+"' and accountdate = '"+AccountDate+"' and managecom = '"+ManageCom+"' and VoucherType = '" + VoucherType + "'", "UPDATE");
       
        } 
        else if  (ManageCom != null && !ManageCom.equals("")){
        	tmap.put("update FIVoucherDataDetail set CheckFlag = '"+CheckF+"'where batchno = '"+BatchNo+"' and contno = '"+ContNo+"' and accountdate = '"+AccountDate+"' and managecom = '"+ManageCom+"'", "UPDATE");
        
        }
        else if (VoucherType != null && !VoucherType.equals("")){
        	tmap.put("update FIVoucherDataDetail set CheckFlag = '"+CheckF+"'where batchno = '"+BatchNo+"' and contno = '"+ContNo+"' and accountdate = '"+AccountDate+"' and VoucherType = '" + VoucherType + "'", "UPDATE");
        } else {
        	 tmap.put("update FIVoucherDataDetail set CheckFlag = '"+CheckF+"'where batchno = '"+BatchNo+"' and contno = '"+ContNo+"' and accountdate = '"+AccountDate+"'", "UPDATE");
        }
        tInputData.add(tmap);
        System.out.println(tmap);       
        PubSubmit tPubSubmit = new PubSubmit();     
        if (!tPubSubmit.submitData(tInputData, ""))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buiError( "gatherData", "批次号码为" + BatchNo + "屏蔽数据出错，提示信息为：" + tPubSubmit.mErrors.getFirstError());         
            tmap=null;
            tInputData=null;
            return false;
        }
        
    	tmap=null;
        tInputData=null;
        return true;
  }
    

    
	private boolean createFile(){
		return true;
	}

	public String[][] getMToExcel() {
		return mToExcel;
	}
	
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     * @throws java.text.ParseException 
     */
    public boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
        System.out.println(mGI.ManageCom);

        if (mGI == null || tf == null)
        {
        	buiError("getInputData","数据维护出错");
            return false;
        }
        BatchNo = (String) tf.getValueByName("BatchNo");      
        ContNo = (String) tf.getValueByName("ContNo");
        ManageCom = (String) tf.getValueByName("ManageCom");
        VoucherType = (String) tf.getValueByName("VoucherType");
        CheckF = (String) tf.getValueByName("CheckF");
        AccountDate = (String) tf.getValueByName("AccountDate");
        mMail = (String) tf.getValueByName("toMail");
        
        System.out.println("mMail-----"+mMail);

        if (BatchNo == null || ContNo == null)
        {
            buiError("getInputData", "传入信息不完整！");
            return false;
        }
        return true;
    }
    
    private void buiError(String functionName,String errorMessage){
    	CError tError = new CError();
        tError.moduleName = "ShieldBorUnevenBL";
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        mErrors.addOneError(tError);
    }
    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "cwad";
        tG.ComCode = "86";
        TransferData tTransferData = new TransferData();
        VData vData = new VData();
        vData.add(tG);
        vData.add(tTransferData);
        PreRecPrintUI tPreRecPrintUI = new PreRecPrintUI();
        if (!tPreRecPrintUI.submitData(vData, ""))
        {
            System.out.print("失败！");
        }
    }
}

