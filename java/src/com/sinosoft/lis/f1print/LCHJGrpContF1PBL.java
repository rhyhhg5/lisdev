/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import java.io.*;
import java.math.BigDecimal;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.Date;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.LCContGetPolDBSet;
import com.sinosoft.lis.vdb.LCContReceiveDBSet;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>
 * ClassName: LCGrpContF1PBL
 * </p>
 * <p>
 * Description: 保单xml文件生成，数据准备类
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: sinosoft
 * </p>
 * 
 * @Database: LIS
 * @CreateDate：2014-08-25 以后增加补充协议时需要将getScanDoc函数,private String[][] mDocFile =
 *                        null;注释取消
 */
public class LCHJGrpContF1PBL implements PrintService
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private String mOperate = "";

    private File mFile = null;

    // 业务处理相关变量

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    private LCGrpAppntSchema mLCGrpAppntSchema = new LCGrpAppntSchema();

    private LCGrpAddressSchema mLCGrpAddressSchema = new LCGrpAddressSchema();

    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

    private LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupSchema();
    
    private LDComSchema mLDComSchema = new LDComSchema();

    private LDUserSchema mLDUserSchema = new LDUserSchema();
    
    private String mTemplatePath = null;

    private String mOutXmlPath = null;
    
//    private String mTemplatePath = "E:\\hmyc\\Projects\\PICCH\\ui\\f1print\\template\\";
//    private String mOutXmlPath = "D:\\temp\\";

    private XMLDatasets mXMLDatasets = null;

    private String[][] mScanFile = null;

    private String mSQL;

    private String ContPrintType = null;

    private String[] PubRiskcode = new String[10];

    private String PubRiskcodePrintFlag = "0";

    private String printInsureDetail = "1";// 被保险人清单是否打印标记：0，不打印；1，打印

    private String contPrintFlag = "1";// 保单是否打印标记：0，不打印；1，打印

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    
    // private String[][] mDocFile = null;

    /*
     * 对于同时传入主险和附加险保单号的情况，如果它们是同一个印刷号的， 将被存在同一个保单数据块中。所以将打印过的保单号存放在这个Vector中。
     */
    // private Vector m_vGrpPolNo = new Vector();
    public LCHJGrpContF1PBL()
    {
    }

    private String getDate(Date date)
    {

        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
        return df.format(date);
    }
    
    private String getFormatDate(String tContSignDate){
       return  tContSignDate = tContSignDate.split("-")[0] + "年" + tContSignDate.split("-")[1] + "月"
        + tContSignDate.split("-")[2] + "日";
    }

    /**
     * 主程序
     * 
     * @param cInputData
     *            VData
     * @param cOperate
     *            String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            // 判定打印类型
//            if (!cOperate.equals("PRINT") && !cOperate.equals("REPRINT"))
//            {
//                buildError("submitData", "不支持的操作字符串");
//                return false;
//            }
            // 全局变量赋值
            mOperate = cOperate;
            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }
            // 数据校验

            if (!checkdate())
            {
                return false;
            }

            ExeSQL tExeSQL = new ExeSQL();
            ContPrintType = tExeSQL.getOneValue("select ContPrintType from lcgrpcont where grpcontno='"
                    + this.mLCGrpContSchema.getGrpContNo() + "'");
            
            mXMLDatasets = new XMLDatasets();
            mXMLDatasets.createDocument();
            
         // 数据准备
            if (!getPrintData())
            {
                return false;
            }
            
            mLOPRTManagerSchema.setStateFlag("1");
    		mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
    		mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
    		mLOPRTManagerSchema
    				.setPrintTimes(mLOPRTManagerSchema.getPrintTimes() + 1);

    		mResult.add(mLOPRTManagerSchema);
            // 正常打印处理
//            if (mOperate.equals("PRINT"))
//            {
//                // 新建一个xml对象
//                mXMLDatasets = new XMLDatasets();
//                mXMLDatasets.createDocument();
//
//                // 数据准备
//                if (!getPrintData())
//                {
//                    return false;
//                }
//            }
//            // 重打处理
//            if (mOperate.equals("REPRINT"))
//            {
//                if (!getRePrintData())
//                {
//                    return false;
//                }
//            }
            return true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submit", ex.getMessage());
            return false;
        }
    }

    /**
     * 从输入数据中得到所有对象
     * 
     * @param cInputData
     *            VData
     * @return boolean 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
    	// 全局变量
    			mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
    					"GlobalInput", 0));
    			if (mGlobalInput == null) {
    				buildError("getInputData", "没有得到足够的信息！");
    				return false;
    			}

    			mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData
    					.getObjectByObjectName("LOPRTManagerSchema", 0));
    			LCGrpContDB tLCGrpContDB=new LCGrpContDB();
    			tLCGrpContDB.setGrpContNo(mLOPRTManagerSchema.getOtherNo());
    			String tSql=" select * from LCGrpCont where grpcontno='"+mLOPRTManagerSchema.getOtherNo()+"'";
    			mLCGrpContSchema=tLCGrpContDB.executeQuery(tSql).get(1);
    			if(mLCGrpContSchema==null){
    				buildError("getInputData", "保单信息查询失败！");
    				return false;
    			}
        // 正常打印
//        if (mOperate.equals("PRINT"))
//        {
//            mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
//            mLCGrpContSchema.setSchema((LCGrpContSchema) cInputData.getObjectByObjectName("LCGrpContSchema", 0));
//            mTemplatePath = (String) cInputData.get(2);
//            mOutXmlPath = (String) cInputData.get(3);
//            try
//            {
//                printInsureDetail = (String) cInputData.get(4);
//                contPrintFlag = (String) cInputData.get(5);
//            }
//            catch (Exception e)
//            {
//                // do nothing
//            }
//        }
//        // 重打模式
//        if (mOperate.equals("REPRINT"))
//        {
//            mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
//            mLCGrpContSchema.setSchema((LCGrpContSchema) cInputData.getObjectByObjectName("LCGrpContSchema", 0));
//        }
        return true;
    }

    /**
     * 返回信息
     * 
     * @return VData
     */
    public VData getResult()
    {
        return this.mResult;
    }

    /**
     * 出错处理
     * 
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LCGrpContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 正常打印处理
     * 
     * @return boolean
     * @throws Exception
     */
    private boolean getPrintData() throws Exception
    {
        // 团单Schema
        LCGrpContSchema tLCGrpContSchema = null;
        // 原则上一次只打印一个团单，当然可以一次多打
        tLCGrpContSchema = this.mLCGrpContSchema;

        // 取得该团单下的所有投保险种信息
        LCGrpPolSet tLCGrpPolSet = getRiskList(tLCGrpContSchema.getGrpContNo());
        // 正常打印流程，目前只关心这个
        if (!getGrpPolDataSet(tLCGrpPolSet))
        {
            return false;
        }

        // 将准备好的数据，放入mResult对象，传递给BLS进行后台操作
        // mResult.add(tLCGrpPolSet);//其实没必要更新这个表
        this.mLCGrpContSchema.setPrintCount(1);
        mResult.add(this.mLCGrpContSchema); // 只更新这个表就好拉
        mResult.add(mXMLDatasets); // xml数据流
        mResult.add(mGlobalInput); // 全局变量
        mResult.add(mOutXmlPath); // 输出目录
        mResult.add(mScanFile);
        mResult.add(contPrintFlag); // 保单是否打印标记：0，不打印；1，打印
        // mResult.add(mDocFile); //协议影印件数组
        System.out.println("生成的报文："+mXMLDatasets.getDocument().getRootElement().toString());
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setSchema(mLCGrpContSchema); 
        if (!tLCGrpContDB.update())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpFeeBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "更新保单主表打印次数信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
//        TransformerFactory   tf   =   TransformerFactory.newInstance();
//        Transformer t = tf.newTransformer();
//        t.setOutputProperty("encoding","GB23121");//解决中文问题，试过用GBK不行
//        ByteArrayOutputStream   bos   =   new   ByteArrayOutputStream();
//        DOMOutputter outputter = new DOMOutputter();
//        t.transform(new DOMSource(outputter.output(mXMLDatasets.getDocument())), new StreamResult(bos));
//        String xmlStr = bos.toString();
//        System.out.println("生成的报文："+xmlStr);
        XMLOutputter outputter = new XMLOutputter("  ", true, "GBK");

        System.out.println("生成的报文："+outputter.outputString(mXMLDatasets.getDocument()));
        // 后台提交
//        LCHJGrpContF1PBLS tLCHJGrpContF1PBLS = new LCHJGrpContF1PBLS();
//        tLCHJGrpContF1PBLS.submitData(mResult, "PRINT");
//         //判错处理
//        if (tLCHJGrpContF1PBLS.mErrors.needDealError())
//        {
//            mErrors.copyAllErrors(tLCHJGrpContF1PBLS.mErrors);
//            buildError("saveData", "提交数据库出错！");
//            return false;
//        }
        // 返回数据容器清空
        //mResult.clear();
        //System.out.println("add inputstream to mResult");
        // 将xml信息放入返回容器
        //mResult.add(mXMLDatasets.getInputStream());
        
        //汇交件打印时不做合同接收处理(注释掉以下方法)  add by cz 2016-1-25
//      增加合同接收
//        if (!dealreceive())
//        {
//        	this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "GrpFeeBLS";
//            tError.functionName = "saveData";
//            tError.errorMessage = "生成保单合同接收数据失败!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
        
        return true;
    }


    /**
     * 正常打印流程
     * 
     * @param cLCGrpPolSet
     *            LCGrpPolSet
     * @return boolean
     * @throws Exception
     */
    private boolean getGrpPolDataSet(LCGrpPolSet cLCGrpPolSet) throws Exception
    {
        // xml对象
        XMLDataset tXMLDataset = mXMLDatasets.createDataset();

        // 查询个单合同表信息
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        // 根据合同号查询
        tLCGrpContDB.setGrpContNo(this.mLCGrpContSchema.getGrpContNo());
        // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLCGrpContDB.getInfo())
        {
            buildError("getInfo", "查询个单合同信息失败！");
            return false;
        }
        this.mLCGrpContSchema = tLCGrpContDB.getSchema();
       
        tXMLDataset.addDataObject(new XMLDataTag("JetFormType", "J202")); //承保通知书
        
        String sqlusercom = "select comcode from lduser where usercode='" + mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") || comcode.equals("86000000"))
        {
            comcode = "86";
        }
        else if (comcode.length() >= 4)
        {
            comcode = comcode.substring(0, 4);
        }
        else
        {
            buildError("dealGrpContInfo", "操作员机构查询出错!");
            return false;
        }
        String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='" + comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        tXMLDataset.addDataObject(new XMLDataTag("ManageComLength4", printcode)); //
        if("".equals(mGlobalInput.ClientIP) || mGlobalInput.ClientIP ==null){
        	tXMLDataset.addDataObject(new XMLDataTag("userIP", "")); //
        }else{
        	tXMLDataset.addDataObject(new XMLDataTag("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"))); //
        }
        
     // 预览标志。
        if (mOperate.equals("batch"))
        {
            tXMLDataset.addDataObject(new XMLDataTag("previewflag", "0")); //
        }
        else
        {
            tXMLDataset.addDataObject(new XMLDataTag("previewflag", "1")); //
        }
        
        
        // 团个单标志，1个单，2团单
        //tXMLDataset.addDataObject(new XMLDataTag("ContType", "2"));
        
        // 获取打印类型，团险无法按险种划分打印类型，故此统一使用000000代替
        LMRiskFormDB tLMRiskFormDB = new LMRiskFormDB();
        tLMRiskFormDB.setRiskCode("000000");
        tLMRiskFormDB.setFormType("PG");
        if (!tLMRiskFormDB.getInfo())
        {
            // 只有当查询出错的时候才报错，如果是空记录无所谓
            if (tLMRiskFormDB.mErrors.needDealError())
            {
                buildError("getInfo", "查询打印模板信息失败！");
                return false;
            }
            // 团个单标志，1个单，2团单
            tXMLDataset.setContType("2");
            // 默认填入空
            tXMLDataset.setTemplate("");
        }
        else
        {
            // 团个单标志，1个单，2团单
            tXMLDataset.setContType("2");
            // 打印模板信息
            tXMLDataset.setTemplate(tLMRiskFormDB.getFormName());
        }

        String tStrSql = " select ArchiveNo from ES_DOC_Main where DocCode = '"
                + mLCGrpContSchema.getPrtNo() + "' order by DocId fetch first 1 rows only ";
        String mArchiveNo=new ExeSQL().getOneValue(tStrSql);
        tXMLDataset.addDataObject(new XMLDataTag("ArchiveNo", mArchiveNo));
//      团单号
        tXMLDataset.addDataObject(new XMLDataTag("GrpContNo", mLCGrpContSchema.getGrpContNo()));
//      印刷号
        tXMLDataset.addDataObject(new XMLDataTag("PrtNo", mLCGrpContSchema.getPrtNo()));
        
        tXMLDataset.addDataObject(new XMLDataTag("MANAGECOM", mLCGrpContSchema.getManageCom()));
        //保费
        tXMLDataset.addDataObject(new XMLDataTag("SUMPREM", format(mLCGrpContSchema.getPrem())));
//      签单日期
        tXMLDataset.addDataObject(new XMLDataTag("SIGNDATE", this.getFormatDate(mLCGrpContSchema.getSignDate())));
//      销售渠道
        tXMLDataset.addDataObject(new XMLDataTag("SALECHNL", mLCGrpContSchema.getSaleChnl()));
        
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(this.mLCGrpContSchema.getAgentCode());
        // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLAAgentDB.getInfo())
        {
            buildError("getInfo", "查询销售人员信息失败！");
            return false;
        }
        this.mLAAgentSchema = tLAAgentDB.getSchema();
        
//      代理机构
        tXMLDataset.addDataObject(new XMLDataTag("AGENTCOM", mLCGrpContSchema.getAgentCom()));
//      代理人编码
        tXMLDataset.addDataObject(new XMLDataTag("AGENTCODE", mLAAgentSchema.getGroupAgentCode()));
//      代理人姓名
        tXMLDataset.addDataObject(new XMLDataTag("AGENTNAME", mLAAgentSchema.getName()));//姓名
//      查询管理机构信息
        LDUserDB tLDUserDB = new LDUserDB();
        // 根据管理机构查询，管理机构的选取可根据个保险公司不同而不同
        tLDUserDB.setUserCode(mGlobalInput.Operator);
        // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLDUserDB.getInfo())
        {
            buildError("getInfo", "操作员信息失败！");
            return false;
        }
        this.mLDUserSchema = tLDUserDB.getSchema();
        
//      操作员
        tXMLDataset.addDataObject(new XMLDataTag("OPERATOR", mGlobalInput.Operator));
//      操作员姓名
        tXMLDataset.addDataObject(new XMLDataTag("OPERATORNAME",mLDUserSchema.getUserName()));
        
//      查询投保人信息
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        // 根据合同号查询
        tLCGrpAppntDB.setGrpContNo(tLCGrpContDB.getGrpContNo());
        // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLCGrpAppntDB.getInfo())
        {
            buildError("getInfo", "查询个单投保人信息失败！");
            return false;
        }
        this.mLCGrpAppntSchema = tLCGrpAppntDB.getSchema();
        
//      投保人姓名
        tXMLDataset.addDataObject(new XMLDataTag("APPNTNAME", mLCGrpAppntSchema.getName()));
//      性别
        tXMLDataset.addDataObject(new XMLDataTag("APPNTSEX", ""));
//      出生日期
        tXMLDataset.addDataObject(new XMLDataTag("APPNTBIRTHDAY", ""));
//      证件类型
        tXMLDataset.addDataObject(new XMLDataTag("APPNTIDTYPE", ""));
//      证件号码
        tXMLDataset.addDataObject(new XMLDataTag("APPNTIDNO", ""));
//      汇交人数
        tXMLDataset.addDataObject(new XMLDataTag("PEOPLES", mLCGrpContSchema.getPeoples2()));
        
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String tSql = "select cvalidate,cinvalidate ,Remark from LCGrpCont where GrpContNo = '"
                + this.mLCGrpContSchema.getGrpContNo() + "'";

        tSSRS = tExeSQL.execSQL(tSql);

        tXMLDataset.addDataObject(new XMLDataTag("CVALIDATE", this.getFormatDate(tSSRS.GetText(1, 1))));
        tXMLDataset.addDataObject(new XMLDataTag("CINVALIDATE", this.getFormatDate(tSSRS.GetText(1, 2))));
//      打印日期
        tXMLDataset.addDataObject(new XMLDataTag("PRINTDATE", this.getFormatDate(PubFun.getCurrentDate())));
        //特别约定
        tXMLDataset.addDataObject(new XMLDataTag("REMARK", tSSRS.GetText(1, 3)));
        
        String tcomsql=" select * from ldcom where comcode='"+mLCGrpContSchema.getManageCom()+"'";
        LDComDB tLDComDB=new LDComDB();
        mLDComSchema=tLDComDB.executeQuery(tcomsql).get(1);
      //管理机构信息
        tXMLDataset.addDataObject(new XMLDataTag("SERVICEPHONE", mLDComSchema.getServicePhone()));//客户服务热线
        tXMLDataset.addDataObject(new XMLDataTag("PHONE", mLDComSchema.getPhone()));//客户服务热线
        tXMLDataset.addDataObject(new XMLDataTag("WEBADDRESS", mLDComSchema.getWebAddress()));//公司网址
        tXMLDataset.addDataObject(new XMLDataTag("ADDRESS", mLDComSchema.getAddress()));//公司地址
        tXMLDataset.addDataObject(new XMLDataTag("ZIPCODE", mLDComSchema.getZipCode()));//公司邮编
        tXMLDataset.addDataObject(new XMLDataTag("Fax", mLDComSchema.getFax()));//公司地址
        
        if("Y".equals(mLDComSchema.getInteractFlag())){
        	tLDComDB.setComCode(mLCGrpContSchema.getManageCom().substring(0, 4));
        	if (!tLDComDB.getInfo())
            {
                buildError("getInfo", "查询管理机构信息失败！");
                return false;
            }
        	mLDComSchema.setName(tLDComDB.getSchema().getName());
        }
        tXMLDataset.addDataObject(new XMLDataTag("SIGNCOMNAME",mLDComSchema.getName()));//签发机构
        
        // 产生团单下明细信息（责任信息、特约信息、个人险种明细、险种条款信息等）
        if (!genPolList(tXMLDataset))
        {
            return false;
        }
        // 产生团单下被保人清单
        if (!genInsuredDetail(tXMLDataset))
        {
            return false;
        }
       
        return true;
    }

   

   

    /**
     * 根据团单合同号，查询明细信息 责任信息、特约信息、个人险种明细、险种条款信息等
     * 
     * @param cXmlDataset
     *            XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean genPolList(XMLDataset cXmlDataset) throws Exception
    {
        
       
        System.out.println("查询险种责任信息开始。。。。。。");
        
        String tSql =" select distinct lc.RiskCode,lc.ContPlanCode,lc.ContPlanName,lc.DutyCode,lc.CalFactor,lc.CalFactorType,lc.CalFactorValue,lc.GetDutyCode,"
        	        +" (select lm.riskname from lmrisk lm where lm.riskcode=lc.RiskCode),"
        	        +" (select dutyname from lmduty lmd where lmd.dutycode=lc.dutycode),Lmr.Subriskflag,ldc.comcode "
        	        +" from LCContPlanDutyParam lc "
        	        +" left join LMRiskApp lmr on lc.riskcode=lmr.riskcode "
        	        +" left join ldcode ldc on ldc.codetype = 'grphjrisk' and code = lc.riskcode "
        	        +" where GrpContNo='"+mLCGrpContSchema.getGrpContNo()+"' "
        	        +" and lc.ContPlanCode <> '11' "
        	        +" order by ldc.comcode,lmr.subriskflag ";
        ExeSQL tExeSQL2 = new ExeSQL();
        SSRS tSSRS2 = tExeSQL2.execSQL(tSql);
        double tsumprem=0.0;//总保费
        if(tSSRS2.MaxRow>0){
//          处理“保额/限额/档次”节点
            //保障计划下责任的清单
            int mhashno=0;
            Hashtable thashtable=new Hashtable();
            for(int i=1;i<=tSSRS2.MaxRow;i++){
                String mRCD=""+tSSRS2.GetText(i, 2)+tSSRS2.GetText(i, 1)+tSSRS2.GetText(i, 4);
                if(!thashtable.containsValue(mRCD)){
                    thashtable.put(""+(++mhashno), mRCD);
                    System.out.println("key值："+(mhashno));
                }
            }
            
            System.out.println("HashTable长度："+thashtable.size());
            if (thashtable.size()>0)
            {
                
                // String flag="1";
                XMLDataList xmlDataList = new XMLDataList();
                // 添加xml一个新的对象Term
                xmlDataList.setDataObjectID("RISKLIST");
                xmlDataList.addColHead("RiskName");
                xmlDataList.addColHead("DutyName");
                xmlDataList.addColHead("Amnt");
                xmlDataList.addColHead("Mult");
                xmlDataList.addColHead("Copys");
                xmlDataList.addColHead("MaxMedFee");
                xmlDataList.addColHead("GetLimit");
                xmlDataList.addColHead("GetRate");
                xmlDataList.addColHead("ContPlanCode");

                xmlDataList.buildColHead();
                System.out.println("ContPrintType" + ContPrintType);
                
                if (ContPrintType.equals("5"))
                {
                   
                    for (int j = 1; j <= thashtable.size(); j++)
                    {
	                    xmlDataList.setColValue("Prem", "--");
	                    xmlDataList.setColValue("Amnt", "--");
	                    xmlDataList.setColValue("Mult", "--");
	                    xmlDataList.setColValue("Copys", "--");
	                    xmlDataList.setColValue("MaxMedFee", "--");
	                    xmlDataList.setColValue("GetLimit", "--");
	                    xmlDataList.setColValue("GetRate", "--");
                        System.out.println("取得key值"+j);
                        String tHashRCM=thashtable.get(""+j).toString();
                        
                        for(int i=1;i<=tSSRS2.MaxRow;i++){
                            String mRCD=""+tSSRS2.GetText(i, 2)+tSSRS2.GetText(i, 1)+tSSRS2.GetText(i, 4);
                            System.out.println("综合主键信息"+mRCD);
                            System.out.println("hashtable中取值"+"  "+j+tHashRCM);
                            if(mRCD.equals(tHashRCM)){
                                System.out.println(tSSRS2.GetText(i , 5));
                                
                                xmlDataList.setColValue("RiskName", tSSRS2.GetText(i, 9));
                                xmlDataList.setColValue("DutyName", tSSRS2.GetText(i, 10));
                                
                                if(tSSRS2.GetText(i , 5).trim().equals("Prem")){
                                	//查询保费
                                	String tprem=new ExeSQL().getOneValue("select sum(ld.Prem) from lcduty ld,lccont lc where lc.contno=ld.contno and lc.grpcontno='"+mLCGrpContSchema.getGrpContNo()+"'  and ld.dutycode='"+tSSRS2.GetText(i, 4)+"' ");
                                	if(!"".equals(tSSRS2.GetText(i, 7)) && tSSRS2.GetText(i, 7)!=null){
                                		xmlDataList.setColValue("Prem", tSSRS2.GetText(i, 7));
                                	}else{
                                		if(!"".equals(tprem) && tprem!=null){
                                			xmlDataList.setColValue("Prem", tprem);
                                		}else{
                                			xmlDataList.setColValue("Prem", "--");//如果为空默认为“--”
                                		}
                                	}
                                	
                                    System.out.println("Prem"+tSSRS2.GetText(i, 7));
                                    if(!"".equals(tSSRS2.GetText(i, 7)) &&tSSRS2.GetText(i, 7)!=null){
                                        tsumprem+=Double.parseDouble(tSSRS2.GetText(i, 7));
                                    }
                                }
                                
                                if(tSSRS2.GetText(i , 5).trim().equals("Amnt")){
                                	//查询保额
                                	String tAmnt=new ExeSQL().getOneValue("select sum(ld.Amnt) from lcduty ld,lccont lc where lc.contno=ld.contno and lc.grpcontno='"+mLCGrpContSchema.getGrpContNo()+"'  and ld.dutycode='"+tSSRS2.GetText(i, 4)+"'  ");
                                	if(!"".equals(tSSRS2.GetText(i, 7)) && tSSRS2.GetText(i, 7)!=null){
                                		xmlDataList.setColValue("Amnt", tSSRS2.GetText(i, 7));
                                	}else{
                                		if(!"".equals(tAmnt) && tAmnt!=null){
                                			xmlDataList.setColValue("Amnt", tAmnt);
                                		}else{
                                			xmlDataList.setColValue("Amnt", "--");//如果为空默认为“--”
                                		}
                                	}
                                    
                                    System.out.println("Amnt"+tSSRS2.GetText(i, 7));
                                }
                                
                                if(tSSRS2.GetText(i , 5).trim().equals("Mult")){
                                	if(!"".equals(tSSRS2.GetText(i, 7).trim()) && tSSRS2.GetText(i, 7)!=null){
                                		xmlDataList.setColValue("Mult", tSSRS2.GetText(i, 7));
                                	}else{
                                		xmlDataList.setColValue("Mult", "--");
                                	}
                                    
                                    System.out.println("Mult"+tSSRS2.GetText(i, 7));
                                }
                                
                                if(tSSRS2.GetText(i , 5).trim().equals("Copys")){
                                	if(!"".equals(tSSRS2.GetText(i, 7).trim()) && tSSRS2.GetText(i, 7)!=null){
                                		xmlDataList.setColValue("Copys", tSSRS2.GetText(i, 7));
                                	}else{
                                		xmlDataList.setColValue("Copys", "--");
                                	}
                                    
                                    System.out.println("Copys"+tSSRS2.GetText(i, 7));
                                }
                                
                                if(tSSRS2.GetText(i , 5).trim().equals("MaxMedFee")){
                                	if(!"".equals(tSSRS2.GetText(i, 7)) && tSSRS2.GetText(i, 7)!=null){
                                		xmlDataList.setColValue("MaxMedFee", tSSRS2.GetText(i, 7));
                                	}else{
                                		xmlDataList.setColValue("MaxMedFee", "--");
                                	}
                                    
                                    System.out.println("MaxMedFee"+tSSRS2.GetText(i, 7));
                                }
                                
                                if(tSSRS2.GetText(i , 5).trim().equals("GetLimit")){
                                	if(!"".equals(tSSRS2.GetText(i, 7)) && tSSRS2.GetText(i, 7)!=null){
                                		xmlDataList.setColValue("GetLimit", tSSRS2.GetText(i, 7));
                                	}else{
                                		xmlDataList.setColValue("GetLimit", "--");
                                	}
                                    
                                    System.out.println("GetLimit"+tSSRS2.GetText(i, 7));
                                }
                                
                                if(tSSRS2.GetText(i , 5).trim().equals("GetRate")){
                                	if(!"".equals(tSSRS2.GetText(i, 7)) && tSSRS2.GetText(i, 7)!=null){
                                		xmlDataList.setColValue("GetRate", tSSRS2.GetText(i, 7));
                                	}else{
                                		xmlDataList.setColValue("GetRate", "--");
                                	}
                                    
                                    System.out.println("GetRate"+tSSRS2.GetText(i, 7));
                                }
                                
                                xmlDataList.setColValue("ContPlanCode", tSSRS2.GetText(i, 2));
                            }
                        }
                        xmlDataList.insertRow(0);
                    }
                    cXmlDataset.addDataObject(xmlDataList);
                }
             
            }
        }else{
            System.out.println("查询险种信息失败！");
            buildError("getPolList", "查询险种信息失败 !");
            return false;
        }
        
        return true;
    }

    /**
     * 根据团单合同号，查询被保人信息
     * 
     * @param cXmlDataset
     *            XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean genInsuredDetail(XMLDataset cXmlDataset) throws Exception
    {
        String tSql=" select rownumber() over() as InsuredID,InsuredName,(select CodeName from LDCode where CodeType = 'sex' and Code = Sex) as Sex,Birthday,SchoolNmae,ClassName,(select codename from ldcode where codetype='idtype' and code=IDType) as IDType,IDNo  from LBInsuredList where GrpContNo = '"+mLCGrpContSchema.getGrpContNo()+"' order by integer(InsuredID) with ur";
        ExeSQL tExeSQL5 = new ExeSQL();
        SSRS tSSRS5 = tExeSQL5.execSQL(tSql.toString());
        if (tSSRS5.getMaxRow() >1)
        {
            
            // String flag="1";
            XMLDataList xmlDataList = new XMLDataList();
            // 添加xml一个新的对象Term
            xmlDataList.setDataObjectID("INSUREDLIST");
            xmlDataList.addColHead("Row");
            xmlDataList.addColHead("InsuredName");
            xmlDataList.addColHead("Sex");
            xmlDataList.addColHead("Birthday");
            xmlDataList.addColHead("SchoolNmae");
            xmlDataList.addColHead("ClassName");
            //xmlDataList.addColHead("IDType");
            //xmlDataList.addColHead("IDNo");
            xmlDataList.buildColHead();
            System.out.println("ContPrintType" + ContPrintType);
            if (ContPrintType.equals("5"))
            {
                for (int j = 1; j <= tSSRS5.getMaxRow(); j++)
                {
                    xmlDataList.setColValue("Row", j);
                    xmlDataList.setColValue("InsuredName", tSSRS5.GetText(j, 2));
                    xmlDataList.setColValue("Sex", tSSRS5.GetText(j, 3));
                    xmlDataList.setColValue("Birthday", tSSRS5.GetText(j, 4));
                    xmlDataList.setColValue("SchoolNmae", tSSRS5.GetText(j, 5));
                    xmlDataList.setColValue("ClassName", tSSRS5.GetText(j, 6));
                    //xmlDataList.setColValue("IDType", tSSRS5.GetText(j, 7));
                    //xmlDataList.setColValue("IDNo", tSSRS5.GetText(j, 8));
                    xmlDataList.insertRow(0);
                }
                cXmlDataset.addDataObject(xmlDataList);
            }
         
        }

        return true;
    }

   
    /**
     * 取得团单下的全部LCGrpPol表数据
     * 
     * @param cGrpContNo
     *            String
     * @return LCGrpPolSet
     * @throws Exception
     */
    private static LCGrpPolSet getRiskList(String cGrpContNo) throws Exception
    {
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();

        tLCGrpPolDB.setGrpContNo(cGrpContNo);
        // 由于LCGrpCont和LCGrpPol为一对多的关系，所以采用query方法
        tLCGrpPolSet = tLCGrpPolDB.query();

        return tLCGrpPolSet;
    }

    /**
     * 获取补打保单的数据
     * 
     * @return boolean
     * @throws Exception
     */
    private boolean getRePrintData() throws Exception
    {
        // String tCurDate = PubFun.getCurrentDate();
        // String tCurTime = PubFun.getCurrentTime();

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = null;

        // 查询LCPolPrint表，获取要补打的保单数据
        String tSql = "SELECT PrtTimes + 1 FROM LCPolPrint WHERE MainPolNo = '" + mLCGrpContSchema.getGrpContNo() + "'";
        System.out.println(tSql);
        tSSRS = tExeSQL.execSQL(tSql);

        if (tExeSQL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tExeSQL.mErrors);
            throw new Exception("获取打印数据失败");
        }
        if (tSSRS.MaxRow < 1)
        {
            throw new Exception("找不到原来的打印数据，可能传入的不是主险保单号！");
        }

        mResult.clear();
        mResult.add(mLCGrpContSchema);

        LCGrpContF1PBLS tLCGrpContF1PBLS = new LCGrpContF1PBLS();
        if (!tLCGrpContF1PBLS.submitData(mResult, "REPRINT"))
        {
            if (tLCGrpContF1PBLS.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tLCGrpContF1PBLS.mErrors);
            }
            throw new Exception("保存数据失败");
        }

        // 取打印数据
        Connection conn = null;

        try
        {
            DOMBuilder tDOMBuilder = new DOMBuilder();
            Element tRootElement = new Element("DATASETS");

            conn = DBConnPool.getConnection();

            if (conn == null)
            {
                throw new Exception("连接数据库失败！");
            }

            // String tSql = "";
            java.sql.Blob tBlob = null;
            // CDB2Blob tCDB2Blob = new CDB2Blob();

            tSql = " and MainPolNo = '" + mLCGrpContSchema.getGrpContNo() + "'";
            tBlob = CBlob.SelectBlob("LCPolPrint", "PolInfo", tSql, conn);

            if (tBlob == null)
            {
                throw new Exception("找不到打印数据");
            }

            // BLOB blob = (oracle.sql.BLOB)tBlob; --Fanym
            Element tElement = tDOMBuilder.build(tBlob.getBinaryStream()).getRootElement();
            tElement = new Element("DATASET").setMixedContent(tElement.getMixedContent());
            tRootElement.addContent(tElement);

            ByteArrayOutputStream tByteArrayOutputStream = new ByteArrayOutputStream();
            XMLOutputter tXMLOutputter = new XMLOutputter("  ", true, "UTF-8");
            tXMLOutputter.output(tRootElement, tByteArrayOutputStream);

            // mResult.clear();
            // mResult.add(new
            // ByteArrayInputStream(tByteArrayOutputStream.toByteArray()));
            ByteArrayInputStream tByteArrayInputStream = new ByteArrayInputStream(tByteArrayOutputStream.toByteArray());

            // 仅仅重新生成xml打印数据，影印件信息暂时不重新获取
            String FilePath = mOutXmlPath + "/printdata";
            mFile = new File(FilePath);
            if (!mFile.exists())
            {
                mFile.mkdir();
            }
            // 根据合同号生成打印数据存放文件夹
            FilePath = mOutXmlPath + "/printdata" + "/" + mLCGrpContSchema.getGrpContNo();
            mFile = new File(FilePath);
            if (!mFile.exists())
            {
                mFile.mkdir();
            }

            // 根据团单合同号生成文件
            String XmlFile = FilePath + "/" + mLCGrpContSchema.getGrpContNo() + ".xml";
            System.out.println("XmlFileXmlFile" + XmlFile);
            FileOutputStream fos = new FileOutputStream(XmlFile);
            // 此方法写入数据准确，但是相对效率比较低下
            int n = 0;
            while ((n = tByteArrayInputStream.read()) != -1)
            {
                fos.write(n);
            }
            fos.close();

            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();

            try
            {
                if (conn != null)
                {
                    conn.close();
                }
            }
            catch (Exception e)
            {
                // do nothing
            }

            throw ex;
        }
        return true;
    }

    /**
     * 格式化浮点型数据
     * 
     * @param dValue
     *            double
     * @return String
     */
    private static String format(double dValue)
    {
        return new DecimalFormat("0.00").format(dValue);
    }

    private boolean checkdate() throws Exception
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
//        tLCGrpContDB.setGrpContNo(this.mLCGrpContSchema.getGrpContNo());
//        tLCGrpContSet = tLCGrpContDB.query();
        if(!"".equals(this.mLCGrpContSchema.getGrpContNo()) && this.mLCGrpContSchema.getGrpContNo()!=null){
            String tSql=" select * from LCGrpCont where GrpContNo='"+this.mLCGrpContSchema.getGrpContNo()+"'";
            tLCGrpContSet=tLCGrpContDB.executeQuery(tSql); 
        }else{
            buildError("checkdate", "取保单号失败 !");
            return false;
        }
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        SSRS tSSRS1 = new SSRS();
        SSRS tSSRS2 = new SSRS();
        tSSRS = tExeSQL.execSQL("select distinct Grpname,customerno,payintv from lcgrppol where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "'");
        tSSRS1 = tExeSQL.execSQL("select distinct AppntName,AppntNo,payintv from lccont where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "'  and  appflag in('1','9') ");
        tSSRS2 = tExeSQL.execSQL("select distinct AppntName,AppntNo,payintv from lcpol where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "' and appflag in('1','9') ");
        if (tSSRS.getMaxRow() != 1 || tSSRS1.getMaxRow() != 1 || tSSRS2.getMaxRow() != 1)
        {
            buildError("checkdate", "取投保客户数据失败!");
            return false;
        }
        if (!(tLCGrpContSet.get(1).getGrpName().equals(tSSRS.GetText(1, 1)))
                || !(tLCGrpContSet.get(1).getGrpName().equals(tSSRS1.GetText(1, 1)))
                || !(tLCGrpContSet.get(1).getGrpName().equals(tSSRS2.GetText(1, 1))))
        {
            buildError("checkdate", "取投保客户名称失败!");
            return false;
        }
        if (!(tLCGrpContSet.get(1).getAppntNo().equals(tSSRS.GetText(1, 2)))
                || !(tLCGrpContSet.get(1).getAppntNo().equals(tSSRS1.GetText(1, 2)))
                || !(tLCGrpContSet.get(1).getAppntNo().equals(tSSRS2.GetText(1, 2))))
        {
            buildError("checkdate", "取投保客户号码失败!");
            return false;
        }
        if (!(String.valueOf(tLCGrpContSet.get(1).getPayIntv()).equals(tSSRS.GetText(1, 3)))
                || !(String.valueOf(tLCGrpContSet.get(1).getPayIntv()).equals(tSSRS1.GetText(1, 3)))
                || !(String.valueOf(tLCGrpContSet.get(1).getPayIntv()).equals(tSSRS2.GetText(1, 3))))
        {
            buildError("checkdate", "取缴费频次失败!");
            return false;
        }
        tSSRS.Clear();
        tSSRS1.Clear();
        tSSRS2.Clear();
        tSSRS = tExeSQL.execSQL("select count(insuredno) from lccont where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo()
                + "' and appflag in('1','9') and  insuredname not in ('公共账户','无名单')");
        tSSRS1 = tExeSQL.execSQL("select count(insuredno) from lcinsured where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo()
                + "'  and  name not in ('公共账户','无名单') and relationtomaininsured='00' ");
        if (!tSSRS.GetText(1, 1).equals(tSSRS1.GetText(1, 1)))
        {
            buildError("checkdate", "取被保人数失败!");
            return false;
        }
        tSSRS2 = tExeSQL
                .execSQL("SELECT count(1) FROM LCGrpCont  WHERE AppFlag in ('1','9') and  PrintCount =1   and grpcontno='"
                        + this.mLCGrpContSchema.getGrpContNo() + "' ");
        System.out.println("tSSRS2.GetText(1,1)" + tSSRS2.GetText(1, 1));
        if (!StrTool.cTrim(tSSRS2.GetText(1, 1)).equals("0"))
        {
            buildError("checkdate", "该保单已经进行过打印!");
            return false;
        }
        tSSRS.Clear();
        tSSRS1.Clear();
        tSSRS2.Clear();
        tSSRS2 = tExeSQL.execSQL("select count(1) from lcinsured where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "'  and  name in ('无名单') ");
        System.out.println("tSSRS2.GetText(1,1)" + tSSRS2.GetText(1, 1));
        if (StrTool.cTrim(tSSRS2.GetText(1, 1)).equals("0"))
        {
            tSSRS = tExeSQL.execSQL("select sum(peoples) from lccont where grpcontno='"
                    + this.mLCGrpContSchema.getGrpContNo()
                    + "' and appflag in('1','9') and  insuredname not in ('公共账户','无名单')");
            tSSRS1 = tExeSQL.execSQL("select count(insuredno) from lcinsured where grpcontno='"
                    + this.mLCGrpContSchema.getGrpContNo() + "'  and  name not in ('公共账户','无名单') ");
            if (!(String.valueOf(tLCGrpContSet.get(1).getPeoples2()).equals(tSSRS.GetText(1, 1)))
                    || !((String.valueOf(tLCGrpContSet.get(1).getPeoples2()).equals(tSSRS1.GetText(1, 1)))))
            {
                buildError("checkdate", "取团体被保人数失败!");
                return false;
            }
        }
        if (StrTool.cTrim(tLCGrpContSet.get(1).getCustomerReceiptNo()).equals(""))
        {
            buildError("checkdate", "取保险合同送达回执号失败!");
            return false;
        }
        if (StrTool.cTrim(tLCGrpContSet.get(1).getContPrintType()).equals(""))
        {
            buildError("checkdate", "取保单打印类型失败!");
            return false;
        }

        tSSRS.Clear();
        tSSRS1.Clear();
        tSSRS2.Clear();
        tSSRS = tExeSQL.execSQL("select sum(prem) from lcgrppol where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "' and appflag in('1','9')");
        tSSRS1 = tExeSQL.execSQL("select sum(prem) from lcpol where grpcontno='" + this.mLCGrpContSchema.getGrpContNo()
                + "' and appflag in('1','9')");
        tSSRS2 = tExeSQL.execSQL("select sum(prem) from lccont where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "' and appflag in('1','9')");
        if (tLCGrpContSet.get(1).getPrem() != Double.parseDouble(tSSRS.GetText(1, 1))
                || tLCGrpContSet.get(1).getPrem() != Double.parseDouble(tSSRS1.GetText(1, 1))
                || tLCGrpContSet.get(1).getPrem() != Double.parseDouble(tSSRS2.GetText(1, 1)))
        {
            System.out.println("zzzzz" + tLCGrpContSet.get(1).getPrem());
            System.out.println("bbbbbb" + tSSRS2.GetText(1, 1));
            buildError("checkdate", "取保单费用失败!");
            return false;
        }
        if (StrTool.cTrim(tLCGrpContSet.get(1).getPayMode()).equals("4"))
        {
            if (StrTool.cTrim(tLCGrpContSet.get(1).getAccName()).equals(""))
            {
                buildError("checkdate", "取银行帐户名失败！");
                return false;
            }

            if (StrTool.cTrim(tLCGrpContSet.get(1).getBankAccNo()).equals(""))
            {
                buildError("checkdate", "取银行帐号失败！");
                return false;
            }
            if (StrTool.cTrim(tLCGrpContSet.get(1).getBankCode()).equals(""))
            {
                buildError("checkdate", "取银行代码失败！");
                return false;
            }
        }

        LDComDB tLDComDB = new LDComDB();
        LDComSet tLDComSet = new LDComSet();
        tLDComDB.setComCode(tLCGrpContSet.get(1).getManageCom());
        tLDComSet = tLDComDB.query();
        if (StrTool.cTrim(tLDComSet.get(1).getName()).equals(""))
        {
            buildError("checkdate", "取分公司名称失败！");
            return false;
        }
        if (StrTool.cTrim(tLDComSet.get(1).getClaimReportPhone()).equals(""))
        {
            buildError("checkdate", "取理赔报案电话失败！");
            return false;
        }

        if (StrTool.cTrim(tLDComSet.get(1).getLetterServiceName()).equals(""))
        {
            buildError("checkdate", "取信函服务名称失败！");
            return false;
        }

        if (StrTool.cTrim(tLDComSet.get(1).getLetterServicePostAddress()).equals(""))
        {
            buildError("checkdate", "取地址信函服务地址失败！");
            return false;
        }

        if (StrTool.cTrim(tLDComSet.get(1).getLetterServicePostZipcode()).equals(""))
        {
            buildError("checkdate", "取地址信函服务邮编失败！");
            return false;
        }

        if (StrTool.cTrim(tLDComSet.get(1).getServicePostAddress()).equals(""))
        {
            buildError("checkdate", "取客户服务中心地址失败！");
            return false;
        }

        if (StrTool.cTrim(tLDComSet.get(1).getServicePostZipcode()).equals(""))
        {
            buildError("checkdate", "取客户服务中心邮编失败！");
            return false;
        }
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSet tLAAgentSet = new LAAgentSet();
        tLAAgentDB.setAgentCode(tLCGrpContSet.get(1).getAgentCode());
        tLAAgentSet = tLAAgentDB.query();
        if (StrTool.cTrim(tLAAgentSet.get(1).getName()).equals(""))
        {
            buildError("checkdate", "取业务员名称失败！");
            return false;
        }
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupDB.setAgentGroup(tLCGrpContSet.get(1).getAgentGroup());
        tLABranchGroupSet = tLABranchGroupDB.query();
        if (StrTool.cTrim(tLABranchGroupSet.get(1).getBranchAttr()).equals(""))
        {
            buildError("checkdate", "取营业单位代码失败！");
            return false;
        }
        if (StrTool.cTrim(tLABranchGroupSet.get(1).getName()).equals(""))
        {
            buildError("checkdate", "取营业单位名称失败！");
            return false;
        }
        
        String tStrPrtSeq = PubFun1.CreateMaxNo("PRTSEQNO", null);
		if (tStrPrtSeq == null || tStrPrtSeq.equals("")) {
			buildError("dealPrintManager", "生成打印流水号失败。");
			return false;
		}
        
        mLOPRTManagerSchema.setPrtSeq(tStrPrtSeq);

		mLOPRTManagerSchema.setOtherNoType("16");

		mLOPRTManagerSchema.setReqCom(mGlobalInput.ManageCom);
		mLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
		mLOPRTManagerSchema.setPrtType("0");
		mLOPRTManagerSchema.setStateFlag("1");

		mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
		mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());

        return true;
    }

   
    public static void main(String[] args)
    {
        LCHJGrpContF1PBL bl = new LCHJGrpContF1PBL();
        bl.mGlobalInput.ComCode = "86";
        bl.mGlobalInput.ManageCom = bl.mGlobalInput.ComCode;
        bl.mGlobalInput.Operator = "group";
        bl.mTemplatePath = "E:\\lisdev\\ui\\f1print\\template\\";
        bl.mOutXmlPath = "D:\\temp\\";

        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo("00108375000008");
        tLCGrpContDB.getInfo();
        bl.mLCGrpContSchema = tLCGrpContDB.getSchema();
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setCode("J202");
    	tLOPRTManagerSchema.setOtherNo("00108375000008");
    	tLOPRTManagerSchema.setStandbyFlag1("00108375000008");

    	
        XMLDatasets tXMLDatasets = new XMLDatasets();
        tXMLDatasets.createDocument();

        //        XMLDataset tXMLDataset = tXMLDatasets.createDataset();

        //        if (!bl.combineInvioce(tXMLDataset))
        //        {
        //            System.out.println(bl.mErrors.getErrContent());
        //        }
        VData aInputData = new VData();
        aInputData.add(bl.mGlobalInput);
        aInputData.add(bl.mLCGrpContSchema);
        aInputData.add(bl.mTemplatePath);
        aInputData.add(bl.mOutXmlPath);
        aInputData.add(tLOPRTManagerSchema);

        if (!bl.submitData(aInputData, "PRINT"))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }

	public CErrors getErrors() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
     * 处理合同接收
     * @param cIns InputStream
     * @param cGlobalInput GlobalInput
     * @param conn Connection
     * @throws Exception
     */
    private boolean dealreceive()
    {
        System.out.println("in dealreceive");
        String tCurDate = PubFun.getCurrentDate();
        String tCurTime = PubFun.getCurrentTime();
        String tAgentName = mLAAgentSchema.getName();
        LCContReceiveDBSet cLCContReceiveDBSet = new LCContReceiveDBSet();
        LCContReceiveSet cLCContReceiveSet = new LCContReceiveSet();
        LCContReceiveDBSet c2LCContReceiveDBSet = new LCContReceiveDBSet();
        LCContReceiveSet c2LCContReceiveSet = new LCContReceiveSet();

        LCContReceiveSchema tLCContReceiveSchema = new LCContReceiveSchema();
        tLCContReceiveSchema.setReceiveID(1);
        tLCContReceiveSchema.setContNo(mLCGrpContSchema.getGrpContNo());
        tLCContReceiveSchema.setPrtNo(mLCGrpContSchema.getPrtNo());
        tLCContReceiveSchema.setContType("2");
        tLCContReceiveSchema.setCValiDate(mLCGrpContSchema.getCValiDate());
        tLCContReceiveSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
        tLCContReceiveSchema.setAppntName(mLCGrpContSchema.getGrpName());
        tLCContReceiveSchema.setPrem(mLCGrpContSchema.getPrem());
        tLCContReceiveSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        tLCContReceiveSchema.setAgentName(tAgentName);
        tLCContReceiveSchema.setSignDate(mLCGrpContSchema.getSignDate());
        tLCContReceiveSchema.setManageCom(mLCGrpContSchema.getManageCom());
        tLCContReceiveSchema.setPrintManageCom(mGlobalInput.ManageCom);
        tLCContReceiveSchema.setPrintOperator(mGlobalInput.Operator);
        tLCContReceiveSchema.setPrintDate(tCurDate);
        tLCContReceiveSchema.setPrintTime(tCurTime);
        tLCContReceiveSchema.setPrintCount(1);
        tLCContReceiveSchema.setReceiveState("1");
        tLCContReceiveSchema.setDealState("0");
        tLCContReceiveSchema.setMakeDate(tCurDate);
        tLCContReceiveSchema.setMakeTime(tCurTime);
        tLCContReceiveSchema.setModifyDate(tCurDate);
        tLCContReceiveSchema.setModifyTime(tCurTime);
        
        LCContReceiveDB tLCContReceiveDB = new LCContReceiveDB();
        LCContReceiveSet tLCContReceiveSet = new LCContReceiveSet();
        tLCContReceiveDB.setContNo(mLCGrpContSchema.getGrpContNo());
        tLCContReceiveSet = tLCContReceiveDB.query();

        if (tLCContReceiveSet.size() > 0)
        {
            int k = tLCContReceiveSet.size() + 1;
            System.out.println("第" + k + "次生成合同接收记录");
            tLCContReceiveSchema.setReceiveID(tLCContReceiveSet.size() + 1);
            LCContReceiveDB t2LCContReceiveDB = new LCContReceiveDB();
            LCContReceiveSet t2LCContReceiveSet = new LCContReceiveSet();
            t2LCContReceiveDB.setContNo(mLCGrpContSchema.getGrpContNo());
            t2LCContReceiveDB.setDealState("0");
            t2LCContReceiveSet = t2LCContReceiveDB.query();
            if (t2LCContReceiveSet.size() > 0)
            {
                for (int i = 1; i <= t2LCContReceiveSet.size(); i++)
                {
                    LCContReceiveSchema t2LCContReceiveSchema = new LCContReceiveSchema();
                    t2LCContReceiveSchema = t2LCContReceiveSet.get(i)
                            .getSchema();
                    t2LCContReceiveSchema.setDealState("1");
                    c2LCContReceiveSet.add(t2LCContReceiveSchema);
                }
            }

        }
        else
        {
            System.out.println("第1次生成合同接收记录");
        }
        if (c2LCContReceiveSet.size() > 0)
        {
            c2LCContReceiveDBSet.add(c2LCContReceiveSet);
            if (!c2LCContReceiveDBSet.update())
            {
                return false;
            }
        }
        cLCContReceiveSet.add(tLCContReceiveSchema);
        cLCContReceiveDBSet.add(cLCContReceiveSet);
        if (!cLCContReceiveDBSet.insert())
        {
            return false;
        }
        
        LCContGetPolSet tLCContGetPolSet = new LCContGetPolSet();
        LCContGetPolDB tLCContGetPolDB = new LCContGetPolDB();
        tLCContGetPolDB.setContNo(mLCGrpContSchema.getGrpContNo());
        tLCContGetPolSet = tLCContGetPolDB.query();
        if (tLCContGetPolSet.size() == 0)
        {
            LCContGetPolSchema tLCContGetPolSchema = new LCContGetPolSchema();
            tLCContGetPolSchema.setContNo(mLCGrpContSchema.getGrpContNo());
            tLCContGetPolSchema.setPrtNo(mLCGrpContSchema.getPrtNo());
            tLCContGetPolSchema.setContType("2");
            tLCContGetPolSchema.setCValiDate(mLCGrpContSchema.getCValiDate());
            tLCContGetPolSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
            tLCContGetPolSchema.setAppntName(mLCGrpContSchema.getGrpName());
            tLCContGetPolSchema.setPrem(mLCGrpContSchema.getPrem());
            tLCContGetPolSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
            tLCContGetPolSchema.setAgentName(tAgentName);
            tLCContGetPolSchema.setSignDate(mLCGrpContSchema.getSignDate());
            tLCContGetPolSchema.setManageCom(mLCGrpContSchema.getManageCom());
            tLCContGetPolSchema.setReceiveManageCom(mLCGrpContSchema.getManageCom());
            tLCContGetPolSchema.setReceiveOperator(mGlobalInput.Operator);
            tLCContGetPolSchema.setReceiveDate(tCurDate);
            tLCContGetPolSchema.setReceiveTime(tCurTime);
            tLCContGetPolSchema.setGetpolState("0");
            tLCContGetPolSchema.setMakeDate(tCurDate);
            tLCContGetPolSchema.setMakeTime(tCurTime);
            tLCContGetPolSchema.setModifyDate(tCurDate);
            tLCContGetPolSchema.setModifyTime(tCurTime);

            LCContGetPolDBSet cLCContGetPolDBSet = new LCContGetPolDBSet();
            cLCContGetPolDBSet.add(tLCContGetPolSchema);
            if (!cLCContGetPolDBSet.insert())
            {
                return false;
            }
        }

        return true;
    }
}
