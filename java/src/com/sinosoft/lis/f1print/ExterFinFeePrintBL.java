package com.sinosoft.lis.f1print;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.adventnet.aclparser.ParseException;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ExterFinFeePrintBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private String mSql = null;
    private String mmSql = null;
    private String mOutXmlPath = null;

    private String StartDate = "";
    
    private String EndDate = "";
    
    private String mCurDate = PubFun.getCurrentDate();

    SSRS tSSRS = new SSRS();
    SSRS ttSSRS = new SSRS();
    	
	private String[][] mToExcel = null;
	
	private String mMail = "";
       

    public ExterFinFeePrintBL()
    {
    }

    public boolean submitData(VData cInputData, String operate) 
    {

        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        
		if(!createFile()){
			return false;
		}        

        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    public boolean dealData()
    {
        System.out.println("BL->dealDate()");
 
            ExeSQL tExeSQL = new ExeSQL();
            mSql = "select * from ("
            	   +" Select char(rownumber() over()) as id,a.contno 非居民保单号,a.appntname 投保人名称,(select codename from ldcode m where m.codetype='nativeplace' and m.code = c.NativePlace fetch first row only) 投保人国籍,"            	   
                   +" c.nativecity 投保人国家代码,(select codename from ldcode t where t.codetype='nativecity' and t.code = c.nativecity fetch first row only) as 投保人国家名称," 
                   +"(select codename from ldcode m where m.codetype='idtype' and m.code = c.IDType fetch first row only) 证件类型,c.IDNo 证件号码,"
                   +" '' 投保单位企业类型,b.insuredname 被保险人名称,(select case d.NativePlace when 'HK' then '港澳台人士' when 'ML' then '中国大陆' when 'OS' then '外籍人士'when 'OT' then '其它国籍'end from lcinsured d where d.contno = b.contno and d.insuredno = b.insuredno fetch first row only) 被保险人国籍,"
                   +"(select t.nativecity from lcinsured t where t.contno = b.contno and t.insuredno = b.insuredno fetch first row only) as 被保险人国家代码,"
                   +"(select t.codename from ldcode t,lcinsured d where t.code = d.nativecity and d.contno = b.contno and d.insuredno = b.insuredno fetch first row only ) as 被保险人国家名称,"
                   +"(case b.insuyearflag when 'Y' then '年' when 'M' then '月' when 'D' then '天' when 'A' then '年' end ) as 保险期间标志,"
                   +"b.years as 保险期间,b.cvalidate 保单生效日（险种层）,b.enddate 保单到期日（险种层）,b.riskcode 险种,"
                   +"(select case RiskType when 'L'  then '寿险' when 'A' then '意外险' when 'H' then '健康险'when 'R' then '责任险'when 'M' then '健康管理服务产品'when 'U' then '万能险'when 'O' then '其他'end from lmriskapp e where e.riskcode = b.riskcode fetch first row only) as 险类1,"
                   +"(select case RiskPeriod when 'L' then '长险'when 'M' then '一年期险'when 'S' then '极短期险'else '其他'end from lmriskapp e where e.riskcode = b.riskcode fetch first row only) as 险类2,"
                   +" (select codename from ldcode m where m.codetype='payintv' and m.code = b.payintv fetch first row only) as 缴费频次,b.payyears 缴费年限,"
                   +" (select nvl(sum(case finitemtype when 'C' then summoney else -summoney end),0) from lidatatransresult g where 1=1 and g.accountcode in('6031000000','7031000000')and g.accountdate between '"+StartDate+"' and '"+EndDate+"'and g.contno = a.contno and g.riskcode = b.riskcode) as 当月保费收入,"
                   +" (select nvl(sum(case finitemtype when 'C' then summoney else -summoney end),0) from lidatatransresult g where 1=1 and g.accountcode like '6511%' and g.accountcode <>'6511040100' and g.accountdate between '"+StartDate+"' and '"+EndDate+"'and g.contno = a.contno and g.riskcode = b.riskcode) as 当月赔付支出 ,"
                   +" (select nvl(sum(summoney),0) from lidatatransresult g where 1=1  and g.accountcode = '1122010000' and g.accountdate <= date('"+StartDate+"') - 1 days and g.finitemtype = 'D'and g.contno = a.contno and g.riskcode = b.riskcode) as 上月末应收保费余额,"
                   +" (select nvl(sum(summoney),0) from lidatatransresult g where 1=1 and g.accountcode = '1122010000' and g.accountdate <='"+EndDate+"'and g.finitemtype = 'D'and g.contno = a.contno and g.riskcode = b.riskcode) as 本月末应收保费余额"                                             
                   +" from lccont a ,lcpol b, lcappnt c"
                   +" where 1=1 and a.stateflag = '1' and a.ContType = '1' "                   
                   +" and a.appflag = '1' and a.contno = b.contno and a.contno = c.contno and a.appntno = c.appntno"
                   +" and ((c.NativePlace <> 'ML' and c.NativePlace is not null) or (c.NativePlace is null and c.IDType = '1'))"
		           +")as bb "; 
            System.out.println(mSql);
//            SSRS ttSSRS = new SSRS();
            tSSRS = tExeSQL.execSQL(mSql);
            
            if(tSSRS.getMaxRow()<=0)
            {
            	buiError("dealData","没有符合条件的信息!");
                return false;
            }
            
            
        return true;
    }
    
   
    
	private boolean createFile(){
		
//		 1、查看数据行数
  	  int dataSize = this.tSSRS.getMaxRow();
  	  if(this.tSSRS==null||this.tSSRS.getMaxRow()<1)
  	  {
  		  buiError("dealData","没有符合条件的信息!");
  		  return false;
  	  }
        // 定义一个二维数组 
  	    String[][] mToExcel = new String[dataSize + 6][30];
       
//        mToExcel = new String[5000][30];
        mToExcel[0][0] = "中国人民健康保险股份有限公司";
        mToExcel[1][0] = EndDate.substring(0, EndDate.indexOf('-'))+ "年"+ EndDate.substring(EndDate.indexOf('-') + 1, EndDate.lastIndexOf('-')) + "月份，对外金融资产提数报表";
        mToExcel[2][0] = "编制单位：" + getName();

        mToExcel[3][0] = "序号";
        mToExcel[3][1] = "非居民保单号";
        mToExcel[3][2] = "投保人名称";
        mToExcel[3][3] = "投保人国籍";
        mToExcel[3][4] = "投保人国家代码";
        mToExcel[3][5] = "投保人国家名称";
        mToExcel[3][6] = "证件类型";
        mToExcel[3][7] = "证件号码";
        mToExcel[3][8] = "投保单位企业类型";
        mToExcel[3][9] = "被保险人名称";
        mToExcel[3][10] = "被保险人国籍";
        mToExcel[3][11] = "被保险人国家代码";
        mToExcel[3][12] = "被保险人国家名称";
        mToExcel[3][13] = "保险期间标志";
        mToExcel[3][14] = "保险期间";
        mToExcel[3][15] = "保单生效日（险种层）";
        mToExcel[3][16] = "保单到期日（险种层）";
        mToExcel[3][17] = "险种";
        mToExcel[3][18] = "险类1";
        mToExcel[3][19] = "险类2";
        mToExcel[3][20] = "缴费频次";
        mToExcel[3][21] = "缴费年限";
        mToExcel[3][22] = "当月保费收入";
        mToExcel[3][23] = "当月赔付支出";
        mToExcel[3][24] = "上月末应收保费余额";
        mToExcel[3][25] = "本月末应收保费余额";
      
      int printNum = 3;
      int no = 1;
      for (int row = 1; row <= tSSRS.getMaxRow(); row++)
      {
          for (int col = 1; col <= tSSRS.getMaxCol(); col++)
          {                   
                  if (tSSRS.GetText(row, col).equals("null"))
                  {
                      mToExcel[row + printNum][col - 1] = "";
                  }
                  else
                  {
                      mToExcel[row + printNum][col - 1] = tSSRS.GetText(row, col);
                  }
              }
        
          mToExcel[row + printNum][0] = no + "";
          no++;}           

  mToExcel[printNum + tSSRS.getMaxRow() + 2][0] = "制表";
  mToExcel[printNum + tSSRS.getMaxRow() + 2][14] = "日期：" + mCurDate;
		
  
		try {
			System.out.println(mOutXmlPath);
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate() };
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(mOutXmlPath);
			System.out.println("生成文件完成");
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}
		
		return true;
	}

	public String[][] getMToExcel() {
		return mToExcel;
	}
	
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     * @throws java.text.ParseException 
     */
    public boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
        System.out.println(mGI.ManageCom);

        if (mGI == null || tf == null)
        {
        	buiError("getInputData", "传入信息不完整！");
            return false;
        }
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        StartDate = (String) tf.getValueByName("StartDate");      
        EndDate = (String) tf.getValueByName("EndDate");
        mMail = (String) tf.getValueByName("toMail");
        
        System.out.println("mMail-----"+mMail);

        if (EndDate == null || mOutXmlPath == null)
        {
            buiError("getInputData", "传入信息不完整！");
            return false;
        }
        return true;
    }
    
    
    private String getName()
    {
        String tSQL = "";
        String tRtValue = "";
        tSQL = "select name from ldcom where comcode='" + mGI.ManageCom + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tSSRS.getMaxRow() == 0)
        {
            tRtValue = "";
        }
        else
            tRtValue = tSSRS.GetText(1, 1);
        return tRtValue;
    }
    private void buiError(String functionName,String errorMessage){
    	CError tError = new CError();
        tError.moduleName = "ExterFinFeePrintBL";
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        mErrors.addOneError(tError);
    }
    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "cwad";
        tG.ComCode = "86";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("OutXmlPath", "D:\\test\\a.xls");
        tTransferData.setNameAndValue("EndDate", "2012-05-01");
        tTransferData.setNameAndValue("StartDate", "");
        VData vData = new VData();
        vData.add(tG);
        vData.add(tTransferData);
        PreRecPrintUI tPreRecPrintUI = new PreRecPrintUI();
        if (!tPreRecPrintUI.submitData(vData, ""))
        {
            System.out.print("失败！");
        }
    }
}
