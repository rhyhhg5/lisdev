package com.sinosoft.lis.f1print;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author XX
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;



class LAMarketBL {
    public LAMarketBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
       public CErrors mErrors = new CErrors();
       private VData mResult = new VData();
       /** 全局变量 */
       private GlobalInput mGlobalInput = new GlobalInput() ;
       private String mManageCom="";
       private String mStartDate= "";
       private String mEndDate = "";
       private String mGrpContNo= "";
       private String tCurrentDate = "";
       private VData mInputData = new VData();
       private String mOperate = "";
       private PubFun mPubFun = new PubFun();

       private ListTable mListTable = new ListTable();
       private TransferData mTransferData = new TransferData();
       private XmlExport mXmlExport = null;


       /**
      * 传输数据的公共方法
      */
     public boolean submitData(VData cInputData, String cOperate)
     {

       mOperate = cOperate;
       mInputData = (VData) cInputData;
       if (mOperate.equals("")) {
           this.bulidErrorB("submitData", "数据不完整");
           return false;
       }

       if (!mOperate.equals("PRINT")) {
           this.bulidErrorB("submitData", "数据不完整");
           return false;
       }

       // 得到外部传入的数据，将数据备份到本类中
         if (!getInputData(mInputData)) {
             return false;
         }

        // 进行数据查询
        if (!queryData()) {
            return false;
        }

    System.out.println("dayinchenggong1232121212121");

         return true;
     }


     private void bulidErrorB(String cFunction, String cErrorMsg) {

             CError tCError = new CError();

             tCError.moduleName = "LAMarketBL";
             tCError.functionName = cFunction;
             tCError.errorMessage = cErrorMsg;

             this.mErrors.addOneError(tCError);

    }

     /**
       * 取得传入的数据
       * 如果没有传入管理机构和起止如期则查全部机构全年的信息
       * @return boolean
       */
      private boolean getInputData(VData cInputData)
      {
          tCurrentDate = mPubFun.getCurrentDate();
          try
          {
              mGlobalInput.setSchema((GlobalInput) cInputData.
                                     getObjectByObjectName("GlobalInput", 0));
              mTransferData = (TransferData) cInputData.getObjectByObjectName(
                      "TransferData", 0);
               //页面传入的数据 三个
               this.mManageCom = (String) mTransferData.getValueByName("tManageCom");
               this.mStartDate = (String) mTransferData.getValueByName("tStartDate");
               this.mEndDate = (String) mTransferData.getValueByName("tEndDate");
               this.mGrpContNo = (String) mTransferData.getValueByName("tGrpContNo");
               if(mManageCom==null||mManageCom.equals(""))
               {  this.mManageCom = "86";
               }
               if(mStartDate==null||mStartDate.equals(""))
               {  this.mStartDate =  getYear(tCurrentDate) + "-01-01";
               }
               if(mEndDate==null||mEndDate.equals(""))
               {  this.mEndDate =  getYear(tCurrentDate) + "-12-31";
               }

               System.out.println(mManageCom);
          } catch (Exception ex) {
              this.mErrors.addOneError("");
              return false;
          }
         return true;
   }


    private boolean queryData()
      {
                TextTag tTextTag = new TextTag();
                mXmlExport = new XmlExport();
                //设置模版名称
                mXmlExport.createDocument("LAGrpMarket.vts", "printer");

                String tMakeDate = "";
                String tMakeTime = "";
                tMakeDate = mPubFun.getCurrentDate();
                tMakeTime = mPubFun.getCurrentTime();
                System.out.print("dayin252");

                tTextTag.add("MakeDate", tMakeDate);
                tTextTag.add("MakeTime", tMakeTime);
                tTextTag.add("StartDate", mStartDate);
                tTextTag.add("EndDate", mEndDate);
                tTextTag.add("ManageCom", mManageCom);
                tTextTag.add("GrpContNo", mGrpContNo);

                System.out.println("1212121" + tMakeDate);
                if (tTextTag.size() < 1) {
                    return false;
                }
                mXmlExport.addTextTag(tTextTag);
                String[] title = {"", "", "", "", "" ,"","","","","",""};
                mListTable.setName("Order");
                if (!getDataList()) {
                  return false;
                }
               if(mListTable==null||mListTable.equals(""))
                {
                   bulidErrorB("getDataList","没有符合条件的信息!");
                  return false;
                }

                System.out.println("111");
                mXmlExport.addListTable(mListTable, title);
                System.out.println("121");
                mXmlExport.outputDocumentToFile("c:\\", "new1");
                this.mResult.clear();
                mResult.addElement(mXmlExport);

                return true;
  }

 private String getAppntName(String pmGrpContno)
  {
    String mAppntName = "";
    String tSQL = "";
    tSQL  = "select distinct a.grpname  from lcgrpcont  a";
    tSQL += " where grpcontno= '" + pmGrpContno+ "' with ur";
    System.out.println(tSQL);
    SSRS tSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    tSSRS = tExeSQL.execSQL(tSQL);
    mAppntName = tSSRS.GetText(1, 1);
     System.out.println(mAppntName);
    return mAppntName;
}
private String getRiskCode(String pmGrpContno)
  {
      String mRiskCode = "";
      String tSQL = "";
      int k;
      tSQL = "select distinct a.RiskCode  from lcgrppol  a";
      tSQL += " where grpcontno= '" + pmGrpContno + "' with ur";
       System.out.println(tSQL);
      SSRS tSSRS = new SSRS();
      ExeSQL tExeSQL = new ExeSQL();
      tSSRS = tExeSQL.execSQL(tSQL);
      k=tSSRS.getMaxRow();
      System.out.println("tSSRS.getMaxRow():"+k);
      if(k>0)
      {
          mRiskCode = tSSRS.GetText(1, 1);
          if (tSSRS.getMaxRow() > 1) {

              for (int i = 2; i <= tSSRS.getMaxRow(); i++) {
                  mRiskCode = mRiskCode + "-" + tSSRS.GetText(i, 1);
              }

          }
      }
      else mRiskCode="";

       System.out.println(mRiskCode);
          return mRiskCode;
  }

private String getYear(String pmDate)
  {
    String mYear = "";
    String tSQL = "";
    tSQL  = "select year('"+pmDate+"')  from dual  with ur";
    SSRS tSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    tSSRS = tExeSQL.execSQL(tSQL);
    mYear = tSSRS.GetText(1, 1);
    return mYear;
}
 private String getCValiDate(String pmGrpContno)
  {
    String mCValiDate = "";
    String tSQL = "";
    tSQL  = "select distinct a.CValiDate  from lcgrpcont  a";
    tSQL += " where grpcontno= '" + pmGrpContno+ "' with ur";
           System.out.println(tSQL);
    SSRS tSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    tSSRS = tExeSQL.execSQL(tSQL);
    mCValiDate = tSSRS.GetText(1, 1);
           System.out.println(mCValiDate);
    return mCValiDate;
}
 private String getCInValiDate(String pmGrpContno)
  {
    String mCInValiDate = "";
    String tSQL = "";
    tSQL  = "select distinct a.CInValiDate  from lcgrpcont  a";
    tSQL += " where grpcontno= '" + pmGrpContno+ "' with ur";
      System.out.println(tSQL);
    SSRS tSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    tSSRS = tExeSQL.execSQL(tSQL);
    mCInValiDate = tSSRS.GetText(1, 1);
    System.out.println(mCInValiDate);
    return mCInValiDate;
}
private String getPeoples2(String pmGrpContno)
  {
    String mPeoples2 = "";
    String tSQL = "";
    tSQL  = "select distinct a.Peoples2  from lcgrpcont  a";
    tSQL += " where grpcontno= '" + pmGrpContno+ "' with ur";
          System.out.println(tSQL);
    SSRS tSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    tSSRS = tExeSQL.execSQL(tSQL);
    mPeoples2 = tSSRS.GetText(1, 1);
        System.out.println(mPeoples2);
    return mPeoples2;
}
  private double execQuery(String sql)
    {
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        System.out.println(sql);

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null)return 0.00;
            st = conn.prepareStatement(sql);
            if (st == null)return 0.00;
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getDouble(1);
            }
            return 0.00;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
            if (!conn.isClosed()) {
                conn.close();
            }
            try {
                st.close();
                rs.close();
            } catch (Exception ex2) {
                ex2.printStackTrace();
            }
            st = null;
            rs = null;
            conn = null;
          } catch (Exception e) {}

        }
    }
 private String getSum(String pmGrpContno)
   {
     String tSQL = "";
     String tRtValue = "";
     DecimalFormat tDF = new DecimalFormat("0.##");


     tSQL  = "select sumprem  from lcgrpcont  a";
     tSQL += " where grpcontno= '" + pmGrpContno+ "' with ur";

     //System.out.println(tSQL);
     try{
         tRtValue = tDF.format(execQuery(tSQL));

     }
     catch(Exception ex)
     {
         System.out.println("getSum 出错！");
     }
     return tRtValue;

  }
  private String getShouldSum(String pmGrpContno)
   {
     String tSQL = "";
     String tRtValue = "";
     DecimalFormat tDF = new DecimalFormat("0.##");


     tSQL  = "select prem  from lcgrpcont  a";
     tSQL += " where grpcontno= '" + pmGrpContno+ "' with ur";

    // System.out.println(tSQL);
     try{
         tRtValue = tDF.format(execQuery(tSQL));
     }
     catch(Exception ex)
     {
         System.out.println("getShouldSum 出错！");
     }

     return tRtValue;
  }
    private String getAccSum(String pmGrpContno)
   {
     String tSQL = "";
     String tRtValue = "";
     DecimalFormat tDF = new DecimalFormat("0.##");

     tSQL  = "select sum(sumactupaymoney)  from ljapaygrp ";
     tSQL += " where grpcontno= '" + pmGrpContno+ "' with ur";

     //System.out.println(tSQL);
     try{
         tRtValue = tDF.format(execQuery(tSQL));
     }
     catch(Exception ex)
     {
         System.out.println("getAccSum 出错！");
     }

     return tRtValue;
  }


  private boolean getDataList()
   {

       if (this.mGrpContNo == null || this.mGrpContNo.equals(""))
       {
           String Sql = "";
           String contMsSQl ="select managecom, grpcontno  from lcgrpcont where managecom like '"
           +mManageCom+"%' and  salechnl='02' and  markettype='2' and signdate>='"
           +mStartDate+"' and signdate<='"+mEndDate+"' order by managecom ";
           System.out.println(contMsSQl);
           SSRS tSSRS = new SSRS();
           ExeSQL tExeSQL = new ExeSQL();
           tSSRS = tExeSQL.execSQL(contMsSQl);
           if (tSSRS.getMaxRow() > 0) {
           for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                           String ListInfo[] = new String[11];
                           ListInfo[0] = tSSRS.GetText(i, 1);
                           ListInfo[1] = tSSRS.GetText(i, 2);
                           System.out.println("1111111111111"+ListInfo[1]);
                           ListInfo[2] = (getAppntName(ListInfo[1]))==null?"":(getAppntName(ListInfo[1]));
                           ListInfo[3] = (getRiskCode(ListInfo[1]))==null?"":(getRiskCode(ListInfo[1]));
                           ListInfo[4] = "";
                           ListInfo[5] = (getCValiDate(ListInfo[1]))==null?"":(getCValiDate(ListInfo[1]));
                           ListInfo[6] = (getCInValiDate(ListInfo[1]))==null?"":(getCInValiDate(ListInfo[1]));
                           ListInfo[7] = (getPeoples2(ListInfo[1]))==null?"":(getPeoples2(ListInfo[1]));
                           ListInfo[8] = (getSum(ListInfo[1]))==null?"":(getSum(ListInfo[1]));
                           ListInfo[9] = (getShouldSum(ListInfo[1]))==null?"":(getShouldSum(ListInfo[1]));
                           ListInfo[10] = (getAccSum(ListInfo[1]))==null?"":(getAccSum(ListInfo[1]));
                           mListTable.add(ListInfo);

                       }

                   }
                 else  if (tSSRS.getMaxRow()<=0)
            {
         bulidErrorB("getDataList","没有符合条件的信息!");
         return false;
         }
       }

      else
      {
        String contMsSQl ="select managecom,grpcontno from lcgrpcont where grpcontno='" +
                mGrpContNo + "' and salechnl='02' and  managecom like '"
                 +mManageCom+"%' and and salechnl='02' and  markettype='2' and signdate>='"
                 +mStartDate+"' and signdate<='"+mEndDate+"' order by managecom ";
      System.out.println(contMsSQl);
           SSRS tSSRS = new SSRS();
           ExeSQL tExeSQL = new ExeSQL();
           tSSRS = tExeSQL.execSQL(contMsSQl);
           if (tSSRS.getMaxRow() > 0) {
           for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                           String ListInfo[] = new String[11];
                           ListInfo[0] = tSSRS.GetText(i, 1);
                           ListInfo[1] = tSSRS.GetText(i, 2);
                           ListInfo[2] = getAppntName(ListInfo[1]);
                           ListInfo[3] = getRiskCode(ListInfo[1]);
                           ListInfo[4] = "";
                           ListInfo[5] = getCValiDate(ListInfo[1]);
                           ListInfo[6] = getCInValiDate(ListInfo[1]);
                           ListInfo[7] = getPeoples2(ListInfo[1]);
                           ListInfo[8] = getSum(ListInfo[1]);
                           ListInfo[9] = getShouldSum(ListInfo[1]);
                           ListInfo[10] = getAccSum(ListInfo[1]);
                           mListTable.add(ListInfo);

                       }

                   }
         else  if (tSSRS.getMaxRow()<=0)
         {
           bulidErrorB("getDataList","没有符合条件的信息!");
           return false;
          }
       }
       return true;
  }

      /**
       * @return VData
       */
      public VData getResult() {
          return mResult;
    }

    private void jbInit() throws Exception {
    }

}
