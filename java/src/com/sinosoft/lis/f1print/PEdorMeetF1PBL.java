package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author kevin
 * @version 1.0
 */

import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PEdorMeetF1PBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();

    public PEdorMeetF1PBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData
     * @param cOperate
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        try
        {

            if (!cOperate.equals("CONFIRM") &&
                !cOperate.equals("PRINT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            if (cOperate.equals("CONFIRM"))
            {

                mResult.clear();

                // 准备所有要打印的数据
                getPrintData();

            }
            // else if( cOperate.equals("PRINT") )
            //{
            //  if( !saveData(cInputData) )  {
            //	 return false;
            // }
            //}
            return true;

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", ex.toString());
            return false;
        }
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                "LOPRTManagerSchema", 0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "RefuseAppF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void getPrintData() throws Exception
    {
        XmlExport xmlExport = new XmlExport(); //新建一个XmlExport的实例
        xmlExport.createDocument("PEdorMeetNotice.vts", ""); //最好紧接着就初始化xml文档

        LCPolDB tLCPolDB = new LCPolDB();
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();

        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema); //将prtseq传给DB，目的查找所有相关信息，然后还要返回给schema
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            throw new Exception("在取得打印队列中数据时发生错误");
        }
        //需要判断是否已经打印？！
        if (tLOPRTManagerDB.getStateFlag().equals("0"))
        {

            mLOPRTManagerSchema = tLOPRTManagerDB.getSchema(); //get all message！

            // 打印时传入的是主险保单的保单号
            tLCPolDB.setPolNo(mLOPRTManagerSchema.getOtherNo());

            if (!tLCPolDB.getInfo())
            {
                mErrors.copyAllErrors(tLCPolDB.mErrors);
                throw new Exception("在获取保单信息时出错！");
            }

            String strPolNo = tLCPolDB.getPolNo();

            if (!tLCPolDB.getMainPolNo().equals(strPolNo))
            {
                throw new Exception("传入的号码不是主险投保单号码");
            }

            // 查询打印队列的信息
            mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();

            if (mLOPRTManagerSchema.getStateFlag() == null)
            {
                buildError("getprintData", "无效的打印状态");
            }
            else if (!mLOPRTManagerSchema.getStateFlag().equals("0"))
            {
                buildError("getprintData", "该打印请求不是在请求状态");
            }
            // 调用打印服务

            mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
            mLOPRTManagerSchema.setDoneTime(PubFun.getCurrentTime());

            tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
            if (tLOPRTManagerDB.getInfo() == false)
            {
                mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
                buildError("outputXML", "在取得打印队列中数据时发生错误");
            }

            TextTag texttag = new TextTag();

            texttag.add("AppntName", tLCPolDB.getAppntName());
            texttag.add("PolNo", tLCPolDB.getPolNo());
            texttag.add("PrtNo", tLCPolDB.getPrtNo());
            texttag.add("AgentName", getAgentName(tLCPolDB.getAgentCode()));
            texttag.add("AgentCode", tLCPolDB.getAgentCode());
            texttag.add("ManageCom", getComName(tLCPolDB.getManageCom()));
            texttag.add("RiskName", getRiskName(tLCPolDB.getRiskCode()));
            texttag.add("InsuredName", tLCPolDB.getInsuredName());
            texttag.add("PrtSeq", mLOPRTManagerSchema.getPrtSeq());
            texttag.add("EdorNo", mLOPRTManagerSchema.getStandbyFlag2()); //保全申请号

            SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
            texttag.add("Today", df.format(new Date()));

            if (texttag.size() > 0)
            {
                xmlExport.addTextTag(texttag);
            }

            mResult.clear();
            mResult.addElement(xmlExport);
        }

    }

//保存打印状态数据

    private boolean saveData(VData mInputData)
    {

        //根据印刷号查询打印队列中的纪录
        //mLOPRTManagerSchema
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (tLOPRTManagerDB.getInfo() == false)
        {
            mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
            buildError("outputXML", "在取得打印队列中数据时发生错误");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setDoneDate(CurrentDate);
        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
        tLOPRTManagerDB.setSchema((LOPRTManagerSchema) mInputData.
                                  getObjectByObjectName("LOPRTManagerSchema", 0));

        mResult.add(mLOPRTManagerSchema);
        mResult.add(tLOPRTManagerDB);
        MeetF1PBLS tMeetF1PBLS = new MeetF1PBLS();
        tMeetF1PBLS.submitData(mResult, mOperate);
        if (tMeetF1PBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tMeetF1PBLS.mErrors);
            buildError("saveData", "提交数据库出错！");
            return false;
        }
        return true;

    }

    // 下面是一些辅助函数

    private String getRiskName(String strRiskCode) throws Exception
    {
        LMRiskDB tLMRiskDB = new LMRiskDB();
        tLMRiskDB.setRiskCode(strRiskCode);
        if (!tLMRiskDB.getInfo())
        {
            mErrors.copyAllErrors(tLMRiskDB.mErrors);
            throw new Exception("在取得主险LMRisk的数据时发生错误");
        }

        return tLMRiskDB.getRiskName();
    }

    private String getAgentName(String strAgentCode) throws Exception
    {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(strAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            mErrors.copyAllErrors(tLAAgentDB.mErrors);
            throw new Exception("在取得LAAgent的数据时发生错误");
        }
        return tLAAgentDB.getName();
    }

    private String getComName(String strComCode) throws Exception
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            throw new Exception("在取得LDCode的数据时发生错误");
        }
        return tLDCodeDB.getCodeName();
    }
}