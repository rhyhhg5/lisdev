package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.AgentPubFun;
import java.math.BigDecimal;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LAGrpAgentDetailBL {

    public CErrors mErrors = new CErrors();

    private VData mInputData = new VData();

    private VData mResult = new VData();

    private String mOperate = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();
    private String mBranchAttr = "";
    private String mStartDate = "";
    private String mEndDate = "";
    private String mAgentState = "";
    private String mAgentGroup ="";
    private String mManageCom = "";
    private String  mAgentStateMin ;
    private String  mAgentStateMax ;
    private XmlExport mXmlExport = null;

    private SSRS mSSRS1 = new SSRS();


    private ListTable mListTable = new ListTable();

    private PubFun mPubFun = new PubFun();

    private String mManageName = "";

    public LAGrpAgentDetailBL() {
    }
    public static void main(String[] args)
    {
        //xjh add
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";



        LAGrpAgentDetailBL tLAGrpAgentDetailBL = new LAGrpAgentDetailBL();
       // tLAGrpAgentDetailBL.submitData(tVData, "");

    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mOperate = cOperate;
        mInputData = (VData) cInputData;
        if (mOperate.equals("")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!this.getInputData(mInputData)) {
            return false;
        }

        if (!dealdate()) {
            return false;
        }

        if (!getPrintData()) {
            this.bulidError("getPrintData", "查询数据失败！");
            return false;
        }

        return true;
    }

    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        try {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);

        } catch (Exception ex) {
            this.mErrors.addOneError("");
            return false;
        }

        this.mStartDate = (String) mTransferData.getValueByName("tStartDate");
        this.mEndDate =  (String) mTransferData.getValueByName("tEndDate");
        this.mAgentState=(String) mTransferData.getValueByName("tAgentState");
        this.mManageCom = (String) mTransferData.getValueByName("tManageCom");
        this.mBranchAttr = (String) mTransferData.getValueByName("tBranchAttr");
        this.mAgentGroup = (String) mTransferData.getValueByName("tAgentGroup");

        if ("01".equals(mAgentState))
        {
            mAgentStateMin="01";
            mAgentStateMax="04";
        }
        else if ("02".equals(mAgentState))
        {
            mAgentStateMin="05";
            mAgentStateMax="07";
        }
        return true;
    }
    /**
    * 业务处理方法
    * @return boolean
    */

   private boolean dealdate()
   {

      if (!getAgentNow())
      {
         return false;
      }

      return true;
   }
   /**
    * 得到分公司
    * @return boolean
   */
   private SSRS getManageCom(String sManageCom)
   {
       String tManageCom=sManageCom.substring(0,4);
       ExeSQL tExeSQL = new ExeSQL();
       String tSql = "select name,comcode from ldcom where comcode = '" + tManageCom + "' ";
       SSRS tSSRS = new SSRS();
       tSSRS = tExeSQL.execSQL(tSql);

       return tSSRS ;
   }
   private boolean getAgentNow() {

       ExeSQL tExeSQL = new ExeSQL();
       String tSql="select a.managecom,c.name,c.branchattr,a.name,a.groupagentcode, "
                  +" (select gradename from laagentgrade where laagentgrade.gradecode=b.agentgrade), "
                  +" b.agentgrade,a.sex,a.employdate,b.initgrade,b.tutorship,a.agentstate "
                  +" from laagent a,latree b ,labranchgroup c "
                  +" where "
                  +" a.agentcode=b.agentcode and a.agentgroup=c.agentgroup  "
                  +" and a.managecom like '"+mManageCom+"%' "
            //      +" and a.employdate>='"+mStartDate+"'     and a.employdate<='"+mEndDate+"' "
            //       +" and a.agentstate>='"+mAgentStateMin+"' and a.agentstate<='"+mAgentStateMax+"' "
                  +" and (c.state is null  or c.state <> '1' )"
                  +" and a.branchtype='2' and a.branchtype2='01' ";
      if(mAgentState.equals("01"))
      {
          tSql+=" and a.employdate<='"+mEndDate+"' "
               +" and  (a.outworkdate is null or a.outworkdate > '"+mEndDate+"') ";
      }
      else if(mAgentState.equals("02"))
      {
          tSql+=" and   a.outworkdate >= '"+mStartDate+"' and  a.outworkdate <= '"+mEndDate+"'  ";
      }
     if (mAgentGroup!=null && !mAgentGroup.equals(""))
     {
        tSql +=" and a.agentgroup='" + mAgentGroup + "' ";
     }
        tSql +=" order by a.managecom,c.branchattr,a.agentcode " ;

       mSSRS1 = tExeSQL.execSQL(tSql);
       System.out.println(tSql);
       if (tExeSQL.mErrors.needDealError()) {
           CError tCError = new CError();
           tCError.moduleName = "MakeXMLBL";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "查询XML数据出错！";

           this.mErrors.addOneError(tCError);

           return false;

       }
       if(mSSRS1.getMaxRow()<=0)
       {
           CError tCError = new CError();
           tCError.moduleName = "MakeXMLBL";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "没有符合条件的人员信息！";

           this.mErrors.addOneError(tCError);

           return false;
       }

       return true;

   }
   private SSRS  getAgentHistory(String sAgentCode)
   {
       ExeSQL tExeSQL = new ExeSQL();
       String tSql="select distinct agentgrade,startdate,makedate from  latreeb a where a.agentcode= '" + sAgentCode + "' "
                  +" and removetype  in ('01','31') order by startdate desc,makedate desc ";
      SSRS tSSRS = new SSRS();
      System.out.println(tSql);
      tSSRS = tExeSQL.execSQL(tSql);

      return tSSRS ;


   }
    /**
     *
     * @return boolean
     */
    private boolean getPrintData() {
        TextTag tTextTag = new TextTag();

        mXmlExport = new XmlExport();

        mXmlExport.createDocument("LAGrpAgentDetailReport.vts", "printer");

        String tMakeDate = "";
        String tMakeTime = "";

        tMakeDate = mPubFun.getCurrentDate();
        tMakeTime = mPubFun.getCurrentTime();

        if (!getManageName()) {
            return false;
        }
        tTextTag.add("MakeDate", tMakeDate);
        tTextTag.add("MakeTime", tMakeTime);
        tTextTag.add("tName", mManageName);

        System.out.println("121212121212121212121212121212" + tMakeDate);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] title = {"", "", "", "", "" ,"11","11","11","11","11","11","11","11","11","11"};

        if (!getListTable()) {
            return false;
        }
        System.out.print("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.print("121");
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 查询列表显示数据
     * @return boolean
     */
    private boolean getListTable() {

        if (mSSRS1.getMaxRow() > 0) {
            for (int i = 1; i <= mSSRS1.getMaxRow(); i++)
            {
                String tSex="" ;
                if ("0".equals(mSSRS1.GetText(i, 8)))
                {
                    tSex="男" ;
                }
                else if ("1".equals(mSSRS1.GetText(i, 8)))
                {
                    tSex="女" ;
                }
                else
                {
                    tSex="其他";
                }
                String tAgentState="";
                if ("01".equals(mSSRS1.GetText(i, 12)) || "02".equals(mSSRS1.GetText(i, 12)) )
                {
                    tAgentState="在职";
                }
                else if("03".equals(mSSRS1.GetText(i, 12)) || "04".equals(mSSRS1.GetText(i, 12)) )
                {
                   tAgentState="离职登记";
                }
                else
                {
                   tAgentState="离职确认";
                }
                SSRS tSSRS1 = new SSRS();
                tSSRS1 = getManageCom(mSSRS1.GetText(i, 1));
                System.out.print("111" + tSSRS1.getMaxRow());
                if (tSSRS1.getMaxRow() <= 0) {
                    CError tCError = new CError();
                    tCError.moduleName = "CreateXml";
                    tCError.functionName = "creatFile";
                    tCError.errorMessage = "没有符合条件的机构信息！";
                    this.mErrors.addOneError(tCError);
                    return false;
                }
                SSRS tSSRS2 = new SSRS();
                tSSRS2 = getAgentHistory(mSSRS1.GetText(i, 5));
                if (tSSRS2.getMaxRow() >= 1) {
                    //此人有职级变更
                    for (int j = 1; j <= tSSRS2.getMaxRow(); j++) {
                        if (j > 1) {
                            //如果 此人变更职级的开始时间相等，则以最后一次为准，其他不算，一天只能有一个有效的职级
                            if (tSSRS2.GetText(j,
                                2).equals(tSSRS2.GetText(j - 1, 2))) {
                                continue;
                            }
                        }
                        String Info[] = new String[15];
                        Info[0] = tSSRS1.GetText(1, 1);
                        Info[1] = tSSRS1.GetText(1, 2);
                        Info[2] = mSSRS1.GetText(i, 2);
                        Info[3] = mSSRS1.GetText(i, 3);
                        Info[4] = mSSRS1.GetText(i, 4);
                        Info[5] = mSSRS1.GetText(i, 5);
                        Info[6] = mSSRS1.GetText(i, 6);
                        Info[7] = mSSRS1.GetText(i, 7);
                        Info[8] = tSex;
                        Info[9] = mSSRS1.GetText(i, 9);
                        Info[10] = mSSRS1.GetText(i, 10);
                        Info[11] = tSSRS2.GetText(j, 1);
                        Info[12] = tSSRS2.GetText(j, 2);
                        Info[13] = mSSRS1.GetText(i, 11);
                        Info[14] = tAgentState;
                        mListTable.add(Info);
                    }
                }
                else
                {
                    //此人职级没有变更
                    String Info[] = new String[15];
                    Info[0] = tSSRS1.GetText(1, 1);
                    Info[1] = tSSRS1.GetText(1, 2);
                    Info[2] = mSSRS1.GetText(i, 2);
                    Info[3] = mSSRS1.GetText(i, 3);
                    Info[4] = mSSRS1.GetText(i, 4);
                    Info[5] = mSSRS1.GetText(i, 5);
                    Info[6] = mSSRS1.GetText(i, 6);
                    Info[7] = mSSRS1.GetText(i, 7);
                    Info[8] = tSex;
                    Info[9] = mSSRS1.GetText(i, 9);
                    Info[10] = mSSRS1.GetText(i, 10);
                    Info[11] ="";
                    Info[12] = "";
                    Info[13] = mSSRS1.GetText(i, 11);
                    Info[14] = tAgentState ;
                        mListTable.add(Info);
                }



            }


            mListTable.setName("ZT");

        } else {

            CError tCError = new CError();
            tCError.moduleName = "CreateXml";
            tCError.functionName = "creatFile";
            tCError.errorMessage = "没有符合条件的人员信息！";

            this.mErrors.addOneError(tCError);

            return false;

        }

        return true;

    }

    private boolean getManageName() {

        String sql = "select name from ldcom where comcode='" + mManageCom +
                     "'";

        SSRS tSSRS = new SSRS();

        ExeSQL tExeSQL = new ExeSQL();

        tSSRS = tExeSQL.execSQL(sql);

        if (tExeSQL.mErrors.needDealError()) {

            this.mErrors.addOneError("销售单位不存在！");

            return false;

        }

        if (mManageCom.equals("86")) {
            this.mManageName = "";
        } else {
            this.mManageName = tSSRS.GetText(1, 1) + "分公司";
        }

        return true;
    }


    /**
     * 获取打印所需要的数据
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg) {

        CError tCError = new CError();

        tCError.moduleName = "LAGrpAgentDetailBL";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;

        this.mErrors.addOneError(tCError);

    }

    /**
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


}
