package com.sinosoft.lis.f1print;

/**
 * <p>Title: LLCustomerInfoBL</p>
 * <p>Description: 理赔客户信息表</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author MN
 * @version 1.0
 */
//import com.sinosoft.lis.db.*;
//import com.sinosoft.lis.vdb.*;
//import com.sinosoft.lis.schema.*;
//import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
//import com.sinosoft.lis.agentprint.LISComparator;
//import java.text.DecimalFormat;
//import java.util.*;
//import java.sql.*;

public class LLCustomerInfoBL {
    public LLCustomerInfoBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mEndCaseDateS = "";	//结案起期
    private String mEndCaseDateE = "";	//结案止期
    private String mManageCom = "";
    private String mManageComName = "";
    private String mOperator = "";
//    private DecimalFormat mDF = new DecimalFormat("0.##");

//    private String[][] mShowDataList = null;
    private XmlExport mXmlExport = null;
//    private String[] mDataList = null;
    private ListTable mListTable = new ListTable();
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mFileNameB = "";
    private TransferData mTransferData = new TransferData();
    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 进行数据查询
        if (!queryData()) {
            return false;
        } else {
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("tFileNameB", mFileNameB);
            tTransferData.setNameAndValue("tMakeDate", mCurrentDate);
            tTransferData.setNameAndValue("tOperator", mOperator);
            LLPrintSave tLLPrintSave = new LLPrintSave();
            VData tVData = new VData();
            tVData.addElement(tTransferData);
            if (!tLLPrintSave.submitData(tVData, "")) {
                System.out.println("LLPrintSave is fault!");
                return false;
            }
        }

        return true;
    }


    /**
     * 按照重点业务类型统计
     * @param aGrpType String
     * @return boolean
     */
    private boolean getDataList() {
    	String tCountSQL = "select coalesce(count(caseno),0)"
            + " from llcase  where  mngcom like '" + mManageCom
            + "%' and rgtstate in ('09','11','12') and endcasedate>='"+mEndCaseDateS+"' and endcasedate<='"+mEndCaseDateE+"'"
            + " with ur";//判断大致数据量，如果太大，提醒缩短结案起止期
    	ExeSQL tExeSQL = new ExeSQL();
		int tCount = Integer.parseInt(tExeSQL.getOneValue(tCountSQL));

		if (tCount>30000) {
		buildError("getDataList", "统计数据量过大，导致系统异常，请缩短统计时间！");
		return false;
		}
		String sql="select c.caseno,case when a.grpcontno='00000000000000000000' then a.contno else a.grpcontno end ,c.customername,"+
		"b.rgtdate,(select OPConfirmDate from ljagetclaim where otherno=c.caseno and othernotype='5' and feeoperationtype<>'FC' fetch first 1 rows only)," +
		"codename('llclaimdecision',d.givetype),sum(a.realpay),(select coalesce(e.phone,e.homephone,e.mobile) from lcaddress e where e.customerno=c.customerno fetch first 1 rows only)," +
		"(select lcaddress.postaladdress from lcaddress where lcaddress.customerno=c.customerno fetch first 1 rows only),b.rgtantname,coalesce(b.rgtantphone,b.rgtantmobile),b.rgtantaddress"+
		" from llclaimdetail a,llregister b,llcase c,llclaim d where a.caseno=c.caseno and a.caseno=d.caseno and b.rgtno=c.rgtno and c.mngcom like '"+mManageCom+"%' and c.rgtstate in"+
		"('09','11','12') and c.endcasedate>='"+mEndCaseDateS+"' and c.endcasedate<='"+mEndCaseDateE+"'"+
		"group by c.caseno,a.grpcontno,a.contno,c.customerno,c.customername,b.rgtdate,d.givetype,b.rgtantname,b.rgtantphone,b.rgtantmobile,b.rgtantaddress with ur";
		System.out.println("LLCustomerInfoSQL:" + sql);

        RSWrapper rsWrapper = new RSWrapper();
        if (!rsWrapper.prepareData(null, sql)) {
            System.out.println("数据准备失败! ");
            return false;
        }
        try{
        	SSRS tSSRS = new SSRS();
        	tSSRS = rsWrapper.getSSRS();
        	if(tSSRS==null)
        	{
        		buildError("getDataList", "没有查询到数据！");
        		return false;
        	}
        	for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
        		String ListInfo[] = new String[14];
                for (int m = 0; m < ListInfo.length; m++) {
                    ListInfo[m] = "";
                }
                ListInfo[0] = tSSRS.GetText(i, 1);//案件号
                ListInfo[1] = tSSRS.GetText(i, 2);//保单号
                ListInfo[2] = tSSRS.GetText(i, 3);//被保险人姓名
                ListInfo[3] = tSSRS.GetText(i, 4);//申请日期
                ListInfo[4] = tSSRS.GetText(i, 5);//通知日期
                
//                String getOPConfirmDate="select OPConfirmDate from ljagetclaim where otherno='"+ListInfo[0]+
//                "' and othernotype='5' and feeoperationtype<>'FC'";
//                ListInfo[4]=tExeSQL.getOneValue(getOPConfirmDate);//通知日期
                
                ListInfo[5] = tSSRS.GetText(i, 6);//理赔结论
                ListInfo[6] = tSSRS.GetText(i, 7);//赔付金额
                ListInfo[7] = tSSRS.GetText(i, 8);//被保险人联系电话
                ListInfo[8] = tSSRS.GetText(i, 9);//被保险人联系地址
                ListInfo[9] = tSSRS.GetText(i, 10);//申请人
                ListInfo[10] = tSSRS.GetText(i, 11);//申请人联系电话
                ListInfo[11] = tSSRS.GetText(i, 12);//申请人联系地址
                mListTable.add(ListInfo);
        	}
        }
        catch(Exception ex){
        	ex.printStackTrace();
        }
        finally {
            rsWrapper.close();
        }
		return true;
    }

    /**
     * 进行数据查询
     * @return boolean
     */
    private boolean queryData() {
        TextTag tTextTag = new TextTag();
        mXmlExport = new XmlExport();
        //设置模版名称
        mXmlExport.createDocument("LLCustomerInfo.vts", "printer");
        System.out.println("获取基本信息：");
        tTextTag.add("ManageComName", mManageComName);
        tTextTag.add("StartDate", mEndCaseDateS);
        tTextTag.add("EndDate", mEndCaseDateE);
        tTextTag.add("operator", mOperator);
        tTextTag.add("MakeDate", mCurrentDate);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] tTitle = {
                          "COL1", "COL2", "COL3", "COL4", "COL5", "COL6",
                          "COL7", "COL8", "COL9", "COL10", "COL11", "COL12",
                          "COL13", "COL14"};
        if (!getDataList()) {
          return false;
        }
        if (mListTable == null||mListTable.size()==0) {
        	buildError("queryData", "没有符合条件的信息!");
            return false;
        }
        mListTable.setName("FAULT");
        mXmlExport.addListTable(mListTable, tTitle);
        mXmlExport.outputDocumentToFile("D:\\", "test1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 取得传入的数据
     * @return boolean
     */
    private boolean getInputData(VData mInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mOperator = mGlobalInput.Operator;

        mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
        mManageCom = (String) mTransferData.getValueByName("MngCom");		//统计机构
        mEndCaseDateS = (String) mTransferData.getValueByName("EndCaseDateS");//结案起期
        mEndCaseDateE = (String) mTransferData.getValueByName("EndCaseDateE");//结案止期
        mManageComName = StrTool.unicodeToGBK((String) mTransferData.getValueByName("ManageName"));//机构名称

        mFileNameB = (String) mTransferData.getValueByName("tFileNameB");
        return true;
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLClaimCollectionBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 取得返回处理过的结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }
    public static void main(String[] args) {
        
//   	 String mOperator = "001";        
//   	 String mManageCom = "86";		//统计机构
//   	 String mCvaliDateS = "2010-03-01";	//生效起期
//   	 String mCvaliDateE = "2010-03-22";	//生效止期
//   	 String mEndCaseDateS = "2010-03-01";//结案起期
//   	 String mEndCaseDateE = "2010-03-22";//结案止期
//   	 String tGrpPolNo = "2200006844";		//保单号
//   	 String aGrpType="0";
//
//	    LLClaimCollectionBL aLLClaimCollectionBL = new LLClaimCollectionBL();
//	    StringBuffer tPremSQL = new StringBuffer();	//经过保费按照生效起止日期提取
//	    tPremSQL.append("select a.managecom,")
//        .append(" (select name from ldcom where comcode=a.managecom),")
//        .append(" a.grpcontno,a.grppolno,a.riskcode,")
//        .append(" (select riskname from lmrisk where riskcode=a.riskcode),")
//        .append(" (select coalesce(sum(d.sumactupaymoney),0) from lqclaimactug d")
//        .append(" where a.grppolno = d.grppolno and d.makedate < '")
//        .append(mEndCaseDateE)
//        .append("'),")
//        .append(" (select peoples2 from lcgrpcont where grpcontno=a.grpcontno), coalesce(days(a.payenddate) - days(a.firstpaydate)+1,0),")
//        .append(" (select count(distinct llcase.caseno) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=a.grppolno and llcase.rgtstate in ('09','11','12') and llcase.endcasedate between '"+mEndCaseDateS+"' and '"+mEndCaseDateE+"'),") 
//        .append(" (select count(distinct customerno) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=a.grppolno and llcase.rgtstate in ('09','11','12') and llcase.endcasedate between '"+mEndCaseDateS+"' and '"+mEndCaseDateE+"'),")
//        .append(" (select coalesce(sum(realpay),0) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=a.grppolno and llcase.rgtstate in ('09','11','12') and llcase.endcasedate between '"+mEndCaseDateS+"' and '"+mEndCaseDateE+"'),")
//        .append(" coalesce((select sum(realpay) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=a.grppolno and rgtstate ='12' and llcase.endcasedate between '"+mEndCaseDateS+"' and '"+mEndCaseDateE+"'),0) ")
//        .append(" from lcgrppol a")
//        .append(" where a.appflag='1' and a.cvalidate between '")
//        .append(mCvaliDateS)
//        .append("' and '")
//        .append(mCvaliDateE)
//        .append("' and a.managecom like '")
//        .append(mManageCom)
//        .append("%'")
//        .append(" group by a.managecom,a.grpcontno, a.grppolno,")
//        .append(" a.grpcontno, a.grppolno,a.riskcode,")
//        .append(" a.peoples2, a.firstpaydate,a.payenddate")
//        .append(" union select a.managecom,")
//        .append(" (select name from ldcom where comcode=a.managecom),")
//        .append(" a.grpcontno,a.grppolno,a.riskcode,")
//        .append(" (select riskname from lmrisk where riskcode=a.riskcode),")
//        .append(" (select coalesce(sum(d.sumactupaymoney),0) from lqclaimactug d")
//        .append(" where a.grppolno = d.grppolno and d.makedate < '")
//        .append(mEndCaseDateE)
//        .append("'),")
//        .append(" (select peoples2 from lcgrpcont where grpcontno=a.grpcontno), coalesce(days(a.payenddate) - days(a.firstpaydate)+1,0),")
//        .append(" (select count(distinct llcase.caseno) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=a.grppolno and llcase.rgtstate in ('09','11','12') and llcase.endcasedate between '"+mEndCaseDateS+"' and '"+mEndCaseDateE+"'),") 
//        .append(" (select count(distinct customerno) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=a.grppolno and llcase.rgtstate in ('09','11','12') and llcase.endcasedate between '"+mEndCaseDateS+"' and '"+mEndCaseDateE+"'),")
//        .append(" (select coalesce(sum(realpay),0) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=a.grppolno and llcase.rgtstate in ('09','11','12') and llcase.endcasedate between '"+mEndCaseDateS+"' and '"+mEndCaseDateE+"'),")
//        .append(" coalesce((select sum(realpay) from llclaimdetail,llcase where llclaimdetail.caseno=llcase.caseno and grppolno=a.grppolno and rgtstate ='12' and llcase.endcasedate between '"+mEndCaseDateS+"' and '"+mEndCaseDateE+"') ,0) ")
//        .append(" from lbgrppol a")
//        .append(" where a.appflag='1' and a.cvalidate between '")
//        .append(mCvaliDateS)
//        .append("' and '")
//        .append(mCvaliDateE)
//        .append("' and a.managecom like '")
//        .append(mManageCom)
//        .append("%'")
//        .append(" group by a.managecom,a.grpcontno, a.grppolno,")
//        .append(" a.grpcontno, a.grppolno,a.riskcode,")
//        .append(" a.peoples2, a.firstpaydate,a.payenddate")
//        .append(" with ur");
//
//       String tPremSQLS = tPremSQL.toString();
//       System.out.println(tPremSQLS);
	    
   }

    private void jbInit() throws Exception {
    }
}
