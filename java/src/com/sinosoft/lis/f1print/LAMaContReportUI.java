/*
 * <p>ClassName: LAMaContReportUI  </p>
 * <p>Description: LAMaContReportUI 类文件 </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft </p>
 * @Database:
 * @CreateDate：2007-7-30
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAMaContReportUI {
    public LAMaContReportUI() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public CErrors mErrors = new CErrors();
   private VData mInputData = new VData();
   /** 数据操作字符串 */
   private String mOperate;
   private VData mResult = new VData();


   public boolean submitData(VData cInputData,String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData=(VData)cInputData.clone();
       System.out.println("String LAMaContReportBL ...");
        LAMaContReportBL  tLAMaContReportBL = new LAMaContReportBL();
        if (!tLAMaContReportBL.submitData(mInputData, mOperate))
        {
            if (tLAMaContReportBL .mErrors.needDealError())
            {
                mErrors.copyAllErrors(tLAMaContReportBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "LAMaContReportUI发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tLAMaContReportBL.getResult();
            return true;
        }
    }

    /**
  * 追加错误信息
  * @param szFunc String
  * @param szErrMsg String
  */
 private void buildError(String szFunc, String szErrMsg)
 {
     CError cError = new CError();

     cError.moduleName = "LAMaContReportUI";
     cError.functionName = szFunc;
     cError.errorMessage = szErrMsg;
     this.mErrors.addOneError(cError);
 }


 /**
  * 取得返回处理过的结果
  * @return VData
  */
 public VData getResult() {
     return this.mResult;
 }

    private void jbInit() throws Exception {
    }


}
