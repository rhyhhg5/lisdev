package com.sinosoft.lis.f1print;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAClientNumReportUI
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    public LAClientNumReportUI()
    {
        System.out.println("LAClientNumReportUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
    	LAClientNumReportBL tLAClientNumReportBL = new LAClientNumReportBL();
        if(!tLAClientNumReportBL.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(tLAClientNumReportBL.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }
}
