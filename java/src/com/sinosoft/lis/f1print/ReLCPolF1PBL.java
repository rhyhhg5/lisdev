/*
 * <p>ClassName: LCPolF1PBL </p>
 * <p>Description: LCPolF1BL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-11-04
 */
package com.sinosoft.lis.f1print;

import java.util.Vector;

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLDatasets;

public class ReLCPolF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCPolSet mLCPolSet = new LCPolSet();
    private String mPrtNo = "";
    private XMLDatasets mXMLDatasets = new XMLDatasets();
    private String mOperate = "";

    /*
     * 对于同时传入主险和附加险保单号的情况，如果它们是同一个印刷号的，
     * 将被存在同一个保单数据块中。所以将打印过的保单号存放在这个Vector中。
     */
    private Vector m_vPolNo = new Vector();

    public ReLCPolF1PBL()
    {
        mXMLDatasets.createDocument();
    }

    /**
         传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("PRINT")
                && !cOperate.equals("CONFIRM"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }
            if (cOperate.equals("PRINT"))
            // 准备所有要打印的数据
            {
                if (!getPrintData())
                {
                    return false;
                }
            }
            else if (cOperate.equals("CONFIRM"))
            {
                if (!getConfirm())
                {
                    return false;
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submit", "发生异常");
            return false;
        }
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLCPolSet.set((LCPolSet) cInputData.getObjectByObjectName("LCPolSet", 0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        LCPolSet tLCPolSet = null;
        LCPolDB tLCPolDB = null;
        LCPolSchema tLCPolSchema = null;
        String strSql = "";

        m_vPolNo.clear(); // Clear contents of m_vPolNo

        for (int nIndex = 0; nIndex < mLCPolSet.size(); nIndex++)
        {
            tLCPolSchema = mLCPolSet.get(nIndex + 1);

            // Have been printed, continue ...
            if (m_vPolNo.contains(tLCPolSchema.getPolNo()))
            {
                continue;
            }

            strSql = "SELECT * FROM LCPol WHERE PolNo = '" +
                     tLCPolSchema.getPolNo() + "'";
            strSql += " AND AppFlag = '1'";
            strSql += " AND PrintCount = 1 ";

            System.out.println(strSql);

            tLCPolSet = new LCPolDB().executeQuery(strSql);

            if (tLCPolSet.size() == 0)
            {
                buildError("getPrintData", "所指定的保单不符合重打的条件");
                return false;
            }

            tLCPolSchema = tLCPolSet.get(1);

            // 查询保单信息的时候需要加上印刷号的信息
            mPrtNo = tLCPolSchema.getPrtNo();

            // 如果这个保单不是主险保单
            // 如果是主险保单，该保单的主险保单号等于自身的保单号
            if (!tLCPolSchema.getMainPolNo().equals(tLCPolSchema.getPolNo()))
            {

                strSql = "SELECT * FROM LCPol WHERE PolNo = '" +
                         tLCPolSchema.getMainPolNo() + "'";
                strSql += " AND PrtNo = '" + mPrtNo + "'";
                strSql += " AND AppFlag = '1'";
                strSql += " AND PrintCount = 1 ";

                System.out.println(strSql);

                tLCPolSet = new LCPolDB().executeQuery(strSql);
                if (tLCPolSet.size() == 0)
                { // 没有查询到同一印刷号的符合打印条件的主险保单
                    buildError("getPrintData", "找不到同一印刷号的符合重打条件的主险保单");
                    return false;
                }
                else
                { // 将查询到的主险保单放在mLCPolSchema中
                    tLCPolSchema = tLCPolSet.get(1);
                }
            }

            /**
             * 查询同一印刷号的所有附加险保单
             * 经过上一步操作，在tLCPolSchema中包含着一个主险保单的信息
             */
            strSql = "SELECT * FROM LCPol WHERE MainPolNo = '" +
                     tLCPolSchema.getMainPolNo() + "'";
            strSql += " AND PolNo <> '" + tLCPolSchema.getMainPolNo() + "'";
            strSql += " AND PrtNo = '" + mPrtNo + "'";

            System.out.println(strSql);

            tLCPolSet = new LCPolDB().executeQuery(strSql);

            /**
             * 校验所有的附加险保单是否符合打印的条件
             * 记录下本次打印过的所有保单号
             */
            m_vPolNo.add(tLCPolSchema.getPolNo());

            for (int n = 0; n < tLCPolSet.size(); n++)
            {
                LCPolSchema tempLCPolSchema = tLCPolSet.get(n + 1);

                m_vPolNo.add(tempLCPolSchema.getPolNo());
                /*Lis5.3 upgrade get
                        if( !tempLCPolSchema.getAppFlag().equals("1")
                            || tempLCPolSchema.getPrintCount() == 0 ) {
                          buildError("getPrintData", "附加险保单不符合重打条件");
                          return false;
                        }
                 */
            }
        } // end of "for(int nIndex = 0; nIndex < mLCPolSet.size(); nIndex++)"

        LCPolSet tempLCPolSet = new LCPolSet();

        for (int nIndex = 0; nIndex < m_vPolNo.size(); nIndex++)
        {
            tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo((String) m_vPolNo.get(nIndex));
            tLCPolDB.getInfo();
            /*Lis5.3 upgrade set
                   tLCPolDB.setPrintCount(-1);
             */
            tempLCPolSet.add(tLCPolDB);
        }

        mResult.add(tempLCPolSet);
        ReLCPolF1PBLS tReLCPolF1PBLS = new ReLCPolF1PBLS();
        tReLCPolF1PBLS.submitData(mResult, mOperate);
        if (tReLCPolF1PBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tReLCPolF1PBLS.mErrors);
            buildError("saveData", "提交数据库出错！");
            return false;
        }
        return true;
    }

    private boolean getConfirm()
    {
        LCPolSet tLCPolSet = null;
        LCPolDB tLCPolDB = null;
        LCPolSchema tLCPolSchema = null;
        String strSql = "";

        m_vPolNo.clear(); // Clear contents of m_vPolNo

        for (int nIndex = 0; nIndex < mLCPolSet.size(); nIndex++)
        {
            tLCPolSchema = mLCPolSet.get(nIndex + 1);

            // Have been printed, continue ...
            if (m_vPolNo.contains(tLCPolSchema.getPolNo()))
            {
                continue;
            }

            strSql = "SELECT * FROM LCPol WHERE PolNo = '" +
                     tLCPolSchema.getPolNo() + "'";
            strSql += " AND AppFlag = '1'";
            strSql += " AND PrintCount = 1 ";

            System.out.println(strSql);

            tLCPolSet = new LCPolDB().executeQuery(strSql);

            if (tLCPolSet.size() == 0)
            {
                buildError("getPrintData", "所指定的保单不符合重打的条件");
                return false;
            }

            tLCPolSchema = tLCPolSet.get(1);

            // 查询保单信息的时候需要加上印刷号的信息
            mPrtNo = tLCPolSchema.getPrtNo();

            // 如果这个保单不是主险保单
            // 如果是主险保单，该保单的主险保单号等于自身的保单号
            if (!tLCPolSchema.getMainPolNo().equals(tLCPolSchema.getPolNo()))
            {

                strSql = "SELECT * FROM LCPol WHERE PolNo = '" +
                         tLCPolSchema.getMainPolNo() + "'";
                strSql += " AND PrtNo = '" + mPrtNo + "'";
                strSql += " AND AppFlag = '1'";
                strSql += " AND PrintCount = 1 ";

                System.out.println(strSql);

                tLCPolSet = new LCPolDB().executeQuery(strSql);
                if (tLCPolSet.size() == 0)
                { // 没有查询到同一印刷号的符合打印条件的主险保单
                    buildError("getPrintData", "找不到同一印刷号的符合重打条件的主险保单");
                    return false;
                }
                else
                { // 将查询到的主险保单放在mLCPolSchema中
                    tLCPolSchema = tLCPolSet.get(1);
                }
            }

            /**
             * 查询同一印刷号的所有附加险保单
             * 经过上一步操作，在tLCPolSchema中包含着一个主险保单的信息
             */
            strSql = "SELECT * FROM LCPol WHERE MainPolNo = '" +
                     tLCPolSchema.getMainPolNo() + "'";
            strSql += " AND PolNo <> '" + tLCPolSchema.getMainPolNo() + "'";
            strSql += " AND PrtNo = '" + mPrtNo + "'";

            System.out.println(strSql);

            tLCPolSet = new LCPolDB().executeQuery(strSql);

            /**
             * 校验所有的附加险保单是否符合打印的条件
             * 记录下本次打印过的所有保单号
             */
            m_vPolNo.add(tLCPolSchema.getPolNo());

            for (int n = 0; n < tLCPolSet.size(); n++)
            {
                LCPolSchema tempLCPolSchema = tLCPolSet.get(n + 1);

                m_vPolNo.add(tempLCPolSchema.getPolNo());
                /*Lis5.3 upgrade get
                        if( !tempLCPolSchema.getAppFlag().equals("1")
                            || tempLCPolSchema.getPrintCount() == 0 ) {
                          buildError("getPrintData", "附加险保单不符合重打条件");
                          return false;
                        }
                 */
            }
        } // end of "for(int nIndex = 0; nIndex < mLCPolSet.size(); nIndex++)"

        LCPolSet tempLCPolSet = new LCPolSet();

        for (int nIndex = 0; nIndex < m_vPolNo.size(); nIndex++)
        {
            tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo((String) m_vPolNo.get(nIndex));
            tLCPolDB.getInfo();
            /*Lis5.3 upgrade set
                   tLCPolDB.setPrintCount(0);
             */
            tempLCPolSet.add(tLCPolDB);
        }

        mResult.add(tempLCPolSet);
        ReLCPolF1PBLS tReLCPolF1PBLS = new ReLCPolF1PBLS();
        tReLCPolF1PBLS.submitData(mResult, mOperate);
        if (tReLCPolF1PBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tReLCPolF1PBLS.mErrors);
            buildError("saveData", "提交数据库出错！");
            return false;
        }
        return true;
    }
}
