package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author St.GN
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;
import java.text.DecimalFormat;
import com.sinosoft.lis.f1print.*;
import java.lang.*;

public class LLMultBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();

    public LLMultBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        if (mTransferData == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LLMultBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        //统计的起讫时间
        String StartDate = (String) mTransferData.getValueByName("Begin");
        String EndDate = (String) mTransferData.getValueByName("End");
        String ManageCom = (String) mTransferData.getValueByName("ManageCom");

        //险种档次信息
        ListTable MultTable = new ListTable();
        MultTable.setName("MULT");
        String[] Title = {
                         "a1", "a2", "a3", "a4","a5", "a6","a7"
        };
        try {
            SSRS tSSRS = new SSRS();
            String sql =  "select m.riskcode, m.riskname, z.* from lmrisk m LEFT OUTER JOIN ("
                         +" SELECT A, B, C, D, decimal(decimal(C)/decimal(num)*100,12,2), "
                         +" CASE WHEN (GpayR+PpayR=0) or D is null then 0 else decimal(decimal(D)/decimal(GpayR + PpayR)*100,5, 2) end "
                         +" FROM (SELECT   a.riskcode A, a.mult B, count(clm.caseno) C, sum(clm.realpay) D,"
                         +" (select count(p.polno) from lcpol p where p.riskcode = a.riskcode and p.mult = a.mult and days(p.ENDDATE) >= days('"+StartDate+"') AND days(p.Cvalidate) <= days('"+EndDate+"') ) num,"
                         +"nvl( (select (1-0.25)*SUM(F.Prem)*(days('"+EndDate+"')-days('"+StartDate+"')+1)/365 FROM LCPOL F WHERE F.Grpcontno <> '00000000000000000000' and F.AppFlag='1' and F.riskcode = a.riskcode and F.riskcode <> '1605' and F.mult = a.mult and days(F.ENDDATE) >= days('"+StartDate+"') AND days(F.Cvalidate) <= days('"+EndDate+"') ),0) GpayR,"
                         +"nvl( (select (1-0.35)*SUM(F.Prem)*(days('"+EndDate+"')-days('"+StartDate+"')+1)/365 FROM LCPOL F WHERE F.Grpcontno = '00000000000000000000'  and F.AppFlag='1' and F.riskcode = a.riskcode and F.mult = a.mult and days(F.ENDDATE) >= days('"+StartDate+"') AND days(F.Cvalidate) <= days('"+EndDate+"') ),0) PpayR"
                         +" FROM lcpol a, (select distinct cd.caseno, cd.polno, cm.realpay from llclaimdetail cd, llcase c, llclaim cm where cd.caseno = c.caseno and c.caseno = cm.caseno and c.endcasedate is not null and c.rgtdate between '"+StartDate+"' and '"+EndDate+"') as clm "
                         +" where  a.polno = clm.polno GROUP BY a.riskcode, a.mult  ORDER BY A ) as x  ) as z on m.riskcode = z.a order by m.riskcode "
                 ;
   System.out.println(sql);
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            for (int i = 0; i < count; i++) {
                String temp[][] = tSSRS.getAllData();

                String[] strCol;
                strCol = new String[8];
                strCol[0] = temp[i][0];
                strCol[1] = temp[i][1];
                strCol[2] = temp[i][2];
                if(temp[i][3].equals("null"))
                {strCol[3] = "" ;}
                else{strCol[3] = temp[i][3];}

                strCol[4] = temp[i][4];
                if(temp[i][5].equals("null"))
                {strCol[5] = "0" ;}
                else{strCol[5] = temp[i][5];}

                if(temp[i][6].equals("null"))
                {strCol[6] = "0" ;}
                else{strCol[6] = temp[i][6];}

                if(temp[i][7].equals("null"))
                {strCol[7] = "0" ;}
                else{strCol[7] = temp[i][7];}

                MultTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "LLMultBL";
            tError.functionName = "";
            tError.errorMessage = "客户基本信息查询失败";
            this.mErrors.addOneError(tError);
        }

        //正常给付
        ListTable NormalTable = new ListTable();
        NormalTable.setName("NORMAL");
        String[] Title_1 = {
                         "a1", "a2"
        };
        try {
            SSRS tSSRS = new SSRS();
            String sql =    " select WW.* from lmrisk m LEFT OUTER JOIN ( select distinct * from "
                           +" ( select m.riskcode A0, z1.A A1, z1.B A2 from lmrisk m LEFT OUTER JOIN "
                           +" (SELECT    A, B FROM (SELECT   a.riskcode A, a.mult B"
                           +" FROM     lcpol a, (select distinct cd.caseno, cd.polno, cm.realpay from llclaimdetail cd, llcase c, llclaim cm where cd.caseno = c.caseno and c.caseno = cm.caseno and c.endcasedate is not null and c.rgtdate between '"+StartDate+"' and '"+EndDate+"') as clm "
                           +" where    a.polno = clm.polno  GROUP BY a.riskcode, a.mult  ORDER BY A "
                           +" ) as x ) as z1 on m.riskcode = z1.A  ) as zz"
                           +" left outer join "
                           +" ( select    m.riskcode AA, z.* from lmrisk m LEFT OUTER JOIN "
                           +" ( SELECT    A, B, C, D FROM ("
                           +" SELECT   a.riskcode A, a.mult B, count(clm.caseno) C, sum(clm.realpay) D"
                           +" FROM     lcpol a, (select distinct cd.caseno, cd.polno, cm.realpay from llclaimdetail cd, llcase c, llclaim cm where cd.caseno = c.caseno and c.caseno = cm.caseno and cm.givetype = '1' and c.endcasedate is not null and c.rgtdate between '"+StartDate+"' and '"+EndDate+"') as clm "
                           +" where    a.polno = clm.polno GROUP BY a.riskcode, a.mult "
                           +" ) as x ) as z on m.riskcode = z.a ) as yy  on zz.A0 = yy.AA  and zz.A2 = yy.B"
                           +" ) AS WW on m.riskcode = WW.A0 order by m.riskcode"
                           ;
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            for (int i = 0; i < count; i++) {
                String temp[][] = tSSRS.getAllData();
                String[] strCol;
                strCol = new String[2];
                strCol[0] = temp[i][6];
                if(temp[i][7].equals("null"))
                {strCol[1] = "0" ;}
                else{strCol[1] = temp[i][7];}

             NormalTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "LLMultBL";
            tError.functionName = "";
            tError.errorMessage = "客户基本信息查询失败";
            this.mErrors.addOneError(tError);
        }

        //部分给付
        ListTable PartTable = new ListTable();
        PartTable.setName("PART");
        String[] Title_2 = {"a1", "a2"};
        try {
            SSRS tSSRS = new SSRS();
            String sql =   " select WW.* from lmrisk m LEFT OUTER JOIN ( select distinct * from "
                           +" ( select m.riskcode A0, z1.A A1, z1.B A2 from lmrisk m LEFT OUTER JOIN "
                           +" (SELECT    A, B FROM (SELECT   a.riskcode A, a.mult B"
                           +" FROM     lcpol a, (select distinct cd.caseno, cd.polno, cm.realpay from llclaimdetail cd, llcase c, llclaim cm where cd.caseno = c.caseno and c.caseno = cm.caseno and c.endcasedate is not null and c.rgtdate between '"+StartDate+"' and '"+EndDate+"') as clm "
                           +" where    a.polno = clm.polno  GROUP BY a.riskcode, a.mult  ORDER BY A "
                           +" ) as x ) as z1 on m.riskcode = z1.A  ) as zz"
                           +" left outer join "
                           +" ( select    m.riskcode AA, z.* from lmrisk m LEFT OUTER JOIN "
                           +" ( SELECT    A, B, C, D FROM ("
                           +" SELECT   a.riskcode A, a.mult B, count(clm.caseno) C, sum(clm.realpay) D"
                           +" FROM     lcpol a, (select distinct cd.caseno, cd.polno, cm.realpay from llclaimdetail cd, llcase c, llclaim cm where cd.caseno = c.caseno and c.caseno = cm.caseno and cm.givetype = '2' and c.endcasedate is not null and c.rgtdate between '"+StartDate+"' and '"+EndDate+"') as clm "
                           +" where    a.polno = clm.polno GROUP BY a.riskcode, a.mult "
                           +" ) as x ) as z on m.riskcode = z.a ) as yy  on zz.A0 = yy.AA  and zz.A2 = yy.B"
                           +" ) AS WW on m.riskcode = WW.A0 order by m.riskcode"
                           ;

            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            System.out.println(count);
            for (int i = 0; i < count; i++) {
                String temp[][] = tSSRS.getAllData();

                String[] strCol;
                strCol = new String[2];
                strCol[0] = temp[i][6];
                if(temp[i][7].equals("null"))
                {strCol[1] = "0" ;}
                else{strCol[1] = temp[i][7];}
             PartTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "LLMultBL";
            tError.functionName = "";
            tError.errorMessage = "客户基本信息查询失败";
            this.mErrors.addOneError(tError);
        }

        //全额拒付
        ListTable RefuseTable = new ListTable();
        RefuseTable.setName("REFUSE");
        String[] Title_3 = { "a1", "a2" };
        try {
            SSRS tSSRS = new SSRS();
            String sql =   " select WW.* from lmrisk m LEFT OUTER JOIN ( select distinct * from "
                           +" ( select m.riskcode A0, z1.A A1, z1.B A2 from lmrisk m LEFT OUTER JOIN "
                           +" (SELECT    A, B FROM (SELECT   a.riskcode A, a.mult B"
                           +" FROM     lcpol a, (select distinct cd.caseno, cd.polno, cm.realpay from llclaimdetail cd, llcase c, llclaim cm where cd.caseno = c.caseno and c.caseno = cm.caseno and c.endcasedate is not null and c.rgtdate between '"+StartDate+"' and '"+EndDate+"') as clm "
                           +" where    a.polno = clm.polno  GROUP BY a.riskcode, a.mult  ORDER BY A "
                           +" ) as x ) as z1 on m.riskcode = z1.A  ) as zz"
                           +" left outer join "
                           +" ( select    m.riskcode AA, z.* from lmrisk m LEFT OUTER JOIN "
                           +" ( SELECT    A, B, C, D FROM ("
                           +" SELECT   a.riskcode A, a.mult B, count(clm.caseno) C, sum(clm.realpay) D"
                           +" FROM     lcpol a, (select distinct cd.caseno, cd.polno, cm.realpay from llclaimdetail cd, llcase c, llclaim cm where cd.caseno = c.caseno and c.caseno = cm.caseno and cm.givetype = '3' and c.endcasedate is not null and c.rgtdate between '"+StartDate+"' and '"+EndDate+"') as clm "
                           +" where    a.polno = clm.polno GROUP BY a.riskcode, a.mult "
                           +" ) as x ) as z on m.riskcode = z.a ) as yy  on zz.A0 = yy.AA  and zz.A2 = yy.B"
                           +" ) AS WW on m.riskcode = WW.A0 order by m.riskcode"
                           ;

            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            for (int i = 0; i < count; i++) {
                String temp[][] = tSSRS.getAllData();

                String[] strCol;
                strCol = new String[2];
                strCol[0] = temp[i][6];
                if(temp[i][7].equals("null"))
                {strCol[1] = "0" ;}
                else{strCol[1] = temp[i][7];}



             RefuseTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "LLMultBL";
            tError.functionName = "";
            tError.errorMessage = "客户基本信息查询失败";
            this.mErrors.addOneError(tError);
        }

        //协议给付
        ListTable XYTable = new ListTable();
        XYTable.setName("XY");
        String[] Title_4 = { "a1", "a2"  };
        try {
            SSRS tSSRS = new SSRS();
            String sql =   " select WW.* from lmrisk m LEFT OUTER JOIN ( select distinct * from "
                           +" ( select m.riskcode A0, z1.A A1, z1.B A2 from lmrisk m LEFT OUTER JOIN "
                           +" (SELECT    A, B FROM (SELECT   a.riskcode A, a.mult B"
                           +" FROM     lcpol a, (select distinct cd.caseno, cd.polno, cm.realpay from llclaimdetail cd, llcase c, llclaim cm where cd.caseno = c.caseno and c.caseno = cm.caseno and c.endcasedate is not null and c.rgtdate between '"+StartDate+"' and '"+EndDate+"') as clm "
                           +" where    a.polno = clm.polno  GROUP BY a.riskcode, a.mult  ORDER BY A "
                           +" ) as x ) as z1 on m.riskcode = z1.A  ) as zz"
                           +" left outer join "
                           +" ( select    m.riskcode AA, z.* from lmrisk m LEFT OUTER JOIN "
                           +" ( SELECT    A, B, C, D FROM ("
                           +" SELECT   a.riskcode A, a.mult B, count(clm.caseno) C, sum(clm.realpay) D"
                           +" FROM     lcpol a, (select distinct cd.caseno, cd.polno, cm.realpay from llclaimdetail cd, llcase c, llclaim cm where cd.caseno = c.caseno and c.caseno = cm.caseno and cm.givetype = '4' and c.endcasedate is not null and c.rgtdate between '"+StartDate+"' and '"+EndDate+"') as clm "
                           +" where    a.polno = clm.polno GROUP BY a.riskcode, a.mult "
                           +" ) as x ) as z on m.riskcode = z.a ) as yy  on zz.A0 = yy.AA  and zz.A2 = yy.B"
                           +" ) AS WW on m.riskcode = WW.A0 order by m.riskcode"
                           ;

            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            for (int i = 0; i < count; i++) {
                String temp[][] = tSSRS.getAllData();

                String[] strCol;
                strCol = new String[2];
                strCol[0] = temp[i][6];
                if(temp[i][7].equals("null"))
                {strCol[1] = "0" ;}
                else{strCol[1] = temp[i][7];}
             XYTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "LLMultBL";
            tError.functionName = "";
            tError.errorMessage = "客户基本信息查询失败";
            this.mErrors.addOneError(tError);
        }

        //通融给付
        ListTable TRTable = new ListTable();
        TRTable.setName("TR");
        String[] Title_5 = {
                         "a1", "a2"
        };
        try {
            SSRS tSSRS = new SSRS();
            String sql =   " select WW.* from lmrisk m LEFT OUTER JOIN ( select distinct * from "
                           +" ( select m.riskcode A0, z1.A A1, z1.B A2 from lmrisk m LEFT OUTER JOIN "
                           +" (SELECT    A, B FROM (SELECT   a.riskcode A, a.mult B"
                           +" FROM     lcpol a, (select distinct cd.caseno, cd.polno, cm.realpay from llclaimdetail cd, llcase c, llclaim cm where cd.caseno = c.caseno and c.caseno = cm.caseno and c.endcasedate is not null and c.rgtdate between '"+StartDate+"' and '"+EndDate+"') as clm "
                           +" where    a.polno = clm.polno  GROUP BY a.riskcode, a.mult  ORDER BY A "
                           +" ) as x ) as z1 on m.riskcode = z1.A  ) as zz"
                           +" left outer join "
                           +" ( select    m.riskcode AA, z.* from lmrisk m LEFT OUTER JOIN "
                           +" ( SELECT    A, B, C, D FROM ("
                           +" SELECT   a.riskcode A, a.mult B, count(clm.caseno) C, sum(clm.realpay) D"
                           +" FROM     lcpol a, (select distinct cd.caseno, cd.polno, cm.realpay from llclaimdetail cd, llcase c, llclaim cm where cd.caseno = c.caseno and c.caseno = cm.caseno and cm.givetype = '5' and c.endcasedate is not null and c.rgtdate between '"+StartDate+"' and '"+EndDate+"') as clm "
                           +" where    a.polno = clm.polno GROUP BY a.riskcode, a.mult "
                           +" ) as x ) as z on m.riskcode = z.a ) as yy  on zz.A0 = yy.AA  and zz.A2 = yy.B"
                           +" ) AS WW on m.riskcode = WW.A0 order by m.riskcode"
                           ;

            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            for (int i = 0; i < count; i++) {
                String temp[][] = tSSRS.getAllData();

                String[] strCol;
                strCol = new String[2];
                strCol[0] = temp[i][6];
                if(temp[i][7].equals("null"))
                {strCol[1] = "0" ;}
                else{strCol[1] = temp[i][7];}
             TRTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "LLMultBL";
            tError.functionName = "";
            tError.errorMessage = "客户基本信息查询失败";
            this.mErrors.addOneError(tError);
        }

        //其它模版上单独不成块的信息

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LLMult.vts", "printer"); //最好紧接着就初始化xml文档

        //LLMultBL模版元素

        StartDate = StartDate.replace('-', '月');
        StartDate = StartDate.replaceFirst("月", "年") + "日";
        EndDate = EndDate.replace('-', '月');
        EndDate = EndDate.replaceFirst("月", "年") + "日";
        texttag.add("StartDateN", StartDate);
        texttag.add("EndDateN", EndDate);

        SSRS tSSRS_M = new SSRS();
        String sql_M = "select Name from ldcom where comcode = '" + ManageCom +
                       "'";
        System.out.println(sql_M);
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS_M = tExeSQL.execSQL(sql_M);
        String temp_M[][] = tSSRS_M.getAllData();
        texttag.add("ManageComName", temp_M[0][0]);

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }

        //保存信息

        xmlexport.addListTable(MultTable, Title);
        xmlexport.addListTable(NormalTable, Title_1);
        xmlexport.addListTable(PartTable, Title_2);
        xmlexport.addListTable(RefuseTable, Title_3);
        xmlexport.addListTable(XYTable, Title_4);
        xmlexport.addListTable(TRTable, Title_5);
//        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }


    public static void main(String[] args) {

        LLMultBL tLLMultBL = new LLMultBL();
        VData tVData = new VData();
        TransferData StartEnd = new TransferData();
        StartEnd.setNameAndValue("Begin","2006-2-1");
        StartEnd.setNameAndValue("End","2006-2-28");
        StartEnd.setNameAndValue("ManageCom","86");
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tVData.addElement(tGlobalInput);
        tVData.addElement(StartEnd);
        tLLMultBL.submitData(tVData, "PRINT");
        VData vdata = tLLMultBL.getResult();
//PubSubmit ps = new PubSubmit();
//if (ps.submitData(vdata, ""))
//{
//    System.out.println("succeed in pubsubmit");
//}

    }

    private void jbInit() throws Exception {
    }

}
