package com.sinosoft.lis.f1print;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PrtUnSignPayedListUI
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    private VData mResult = null;

    public PrtUnSignPayedListUI()
    {
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        PrtUnSignPayedListBL bl = new PrtUnSignPayedListBL();

        if(!bl.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }
        mResult = bl.getResult();

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public static void main(String[] args)
    {
    }
}
