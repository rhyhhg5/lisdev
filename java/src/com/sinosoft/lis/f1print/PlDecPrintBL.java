package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author 刘岩松
 * function ://打印“银行代扣正确清单包括（首期和续期）”
 * @version 1.0
 * @date 2004-6-28
 */


import java.math.BigDecimal;


import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.StrTool;
import com.sinosoft.lis.pubfun.*;


public class PlDecPrintBL {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String placeno = "";        //职场编码
    private String strComLevel = "";    //机构级别
    private String strMngCom = "";      //机构代码
    private String strBeginDate="";     //统计起期
    private String strEndDate="";       //统计止期
//    private String strComCode="";		//当前登录机构编码
    
    
   //获取统计日期的年份和月份
   private int strBeginDateYear=0;
   private int strEndDateYear=0;
   private int strBeginDateMonth=0;
   private int strEndDateMonth=0;
    
    //前期投入及摊销
    private double prepreMoney=0.00;    //以前期间摊销
    private double prePayMoney=0.00;    //本期摊销
    private double preNotMoney=0.00;    //未摊销
    //本期新增投入及摊销
    private double preMoney=0.00;    //以前期间摊销
    private double payMoney=0.00;       //本期摊销
    private double NotMoney=0.00;       //未摊销
    
    private GlobalInput mG = new GlobalInput();
    private String mFlag = "";

    public PlDecPrintBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {

        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();
        try {
        	if (!getPrintData()) {
                return false;
            }
			
		} catch (Exception e) {
			buildError("getPrintData","生成报表错误！");
			
		}

        return true;
    }

    private boolean getInputData(VData cInputData) {
    	placeno = (String) cInputData.get(0);
    	strMngCom = (String) cInputData.get(1);
    	strComLevel=(String) cInputData.get(2);
    	strBeginDate=(String) cInputData.get(3);
    	strEndDate=(String) cInputData.get(4);
//    	strComCode=(String)cInputData.get(5);
    	
    	strBeginDateYear=Integer.parseInt(strBeginDate.substring(0,4));
    	strEndDateYear=Integer.parseInt(strEndDate.substring(0,4));
    	System.out.println(strBeginDate.indexOf("-"));
    	System.out.println(strBeginDate.lastIndexOf("-"));
    	strBeginDateMonth=Integer.parseInt(strBeginDate.substring(strBeginDate.indexOf("-")+1,strBeginDate.length()));
    	strEndDateMonth=Integer.parseInt(strEndDate.substring(strBeginDate.indexOf("-")+1,strBeginDate.length()));
    	
    	System.out.println("统计起期-年："+strBeginDateYear);
    	System.out.println("统计起期-月："+strBeginDateMonth);
    	System.out.println("统计止期-年："+strEndDateYear);
    	System.out.println("统计止期-月："+strEndDateMonth);

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "PlDecPrintBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    private boolean getPrintData() 
    {

		SSRS tSSRS = new SSRS();

		String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
		//String tServerPath= "D:/workspace/picc/WebContent/";


		TextTag texttag = new TextTag(); // 新建一个TextTag的实例
		XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
		xmlexport.createDocument("PlDecPrint.vts", "printer"); // 最好紧接着就初始化xml文档
		String strArr[] = new String[25];
		if (texttag.size() > 0) 
		{
			xmlexport.addTextTag(texttag);
		}
		String[] detailArr = new String[] {"序号","机构名称","机构级别","编码","地址","租赁起止期","租期","职场面积","装修投入","以前期间摊销","本期摊销","未摊销","装修投入","本期摊销","未摊销","总投入","以前期间摊销","本期摊销合计","未摊销合计"};
		{
			String sql = " select * from (SELECT char(rownumber() over(order by Placeno)) as rolnum,a.Comname, (case a.comlevel when '1' then '省级分公司' when '2' then '地市级公司' when '3' then '四级机构-区' else '四级机构-县' end) , " +
					     " a.Placeno, a.Address, char(a.Begindate) || '至' || char(a.Actualenddate),trim(to_char(to_date(a.Actualenddate) - to_date(a.Begindate) + 1)) || '天'," +
					     " cast(round(nvl(sigarea,0) + nvl(grparea,0) + nvl(bankarea,0) + nvl(healtharea,0) + nvl(servicearea,0) +nvl(manageroffice,0) + nvl(departpersno,0) + nvl(planfinance,0) + nvl(jointarea,0) + nvl(publicarea,0) +nvl(otherarea,0),2) as decimal(10,2)) as sumarea " +
					     " FROM LIPlaceRentInfo a where 1 = 1 and exists(select 1 from liplacerentfee where placeno=a.placeno and feetype='03')";
			if(!"".equals(placeno))
			{
				sql += " and placeno='"+placeno+"'";
			}
			if(!"".equals(strMngCom))
			{
				sql += " and managecom like'"+strMngCom+"%'";
			}
//			if("".equals(strMngCom) || strMngCom == null)
//			{
//				sql += " and managecom like '"+strComCode+"%'";
//			}
			if(!"".equals(strComLevel))
			{
				sql += " and comlevel='"+strComLevel+"'";
			}
			/*if(!"".equals(strBeginDate))
			{
				sql += " and begindate>='"+strBeginDate+"'";
			}
			if(!"".equals(strEndDate))
			{
				sql += " and enddate<='"+strEndDate+"'";
			}*/
			sql += " and state <> '02' ) as aa";
			
			System.out.println("sql:"+sql);
			ExeSQL main_exesql = new ExeSQL();
		    SSRS main_ssrs = main_exesql.execSQL(sql);
			ListTable tDetailListTable = new ListTable(); // 明细ListTable
			tDetailListTable.setName("INFO");
			System.out.println(main_ssrs.MaxRow);
            System.out.println(main_ssrs.MaxCol);
			for(int i=1;i<=main_ssrs.MaxRow;i++)
			{
				strArr = new String[19];
				for(int j=1;j<=main_ssrs.MaxCol;j++)
				{
					System.out.println("i===="+i);
					System.out.println("j===="+j);
                    strArr[j - 1] = main_ssrs.GetText(i, j);
				}
				
				System.out.println("职场编码："+main_ssrs.GetText(i, 4));
				getmoney(main_ssrs.GetText(i, 4));
				
				//四舍五入
				prepreMoney =doubleformat(prepreMoney);
				prePayMoney =doubleformat(prePayMoney);
				preNotMoney =doubleformat(preNotMoney);
				preMoney =doubleformat(preMoney);
				payMoney =doubleformat(payMoney);
				NotMoney =doubleformat(NotMoney);
				
				System.out.println("前期-以前期间摊销："+prepreMoney);
				System.out.println("前期-本期摊销："+prePayMoney);
				System.out.println("前期-未摊销："+preNotMoney);
				System.out.println("本期-以前期间摊销："+preMoney);
				System.out.println("本期-本期摊销："+payMoney);
				System.out.println("本期-未摊销："+NotMoney);
			    
				strArr[8]=Double.toString(prepreMoney+prePayMoney+preNotMoney);
				strArr[9]=Double.toString(prepreMoney);
				strArr[10]=Double.toString(prePayMoney);
				strArr[11]=Double.toString(preNotMoney);
				strArr[12]=Double.toString(payMoney+NotMoney);
				strArr[13]=Double.toString(payMoney);
				strArr[14]=Double.toString(NotMoney);
				strArr[15]=Double.toString(prepreMoney+prePayMoney+preNotMoney+payMoney+NotMoney);
				strArr[16]=Double.toString(prepreMoney);
				strArr[17]=Double.toString(prePayMoney+payMoney);
				strArr[18]=Double.toString(preNotMoney+NotMoney);
				
				
				//清除数据
			    prepreMoney=0.00;    //以前期间摊销
			    prePayMoney=0.00;    //本期摊销
			    preNotMoney=0.00;    //未摊销
			    //本期新增投入及摊销
			    preMoney=0.00;    //以前期间摊销
			    payMoney=0.00;       //本期摊销
			    NotMoney=0.00;       //未摊销
				
				tDetailListTable.add(strArr);
			}
			xmlexport.addListTable(tDetailListTable, detailArr); // 明细ListTable
																	// 放入xmlexport对象
		}

		mResult.clear();
		mResult.addElement(xmlexport);

		return true;

    }
    private boolean getmoney(String id)
    {
    	String sql="select placeno,paydate,paybegindate,payenddate,money from LIPlaceRentFee where feetype='03' and placeno='"+id+"'";
    	ExeSQL exesql = new ExeSQL();
    	SSRS ssrs = exesql.execSQL(sql);
    	for(int i=1;i<=ssrs.MaxRow;i++){
    		
    		int payDateYear=Integer.parseInt(ssrs.GetText(i, 2).substring(0,4));
    		int payDateMonth=Integer.parseInt(ssrs.GetText(i, 2).substring(ssrs.GetText(i, 2).indexOf("-")+1,ssrs.GetText(i, 2).lastIndexOf("-")));
    		System.out.println("支付日期："+ssrs.GetText(i, 2));
    		System.out.println("支付日期-年："+payDateYear);
    		System.out.println("支付日期-月："+payDateMonth);
    		if(payDateYear<strBeginDateYear){
    			System.out.println("前期");
    			if(!preMoney(ssrs.GetText(i, 3),ssrs.GetText(i, 4),Double.parseDouble(ssrs.GetText(i, 5)),0)){
    				return false;
    			}
    		}else if(payDateYear>=strBeginDateYear && payDateYear<=strEndDateYear){
    			if(payDateYear==strBeginDateYear && payDateMonth<strBeginDateMonth){
    				System.out.println("前期");
    				if(!preMoney(ssrs.GetText(i, 3),ssrs.GetText(i, 4),Double.parseDouble(ssrs.GetText(i, 5)),0)){
    					return false;
    				}
    			}else if(payDateYear==strEndDateYear && payDateMonth>strEndDateMonth){
    				continue;
    			}else{
    				System.out.println("本期");
    				if(!preMoney(ssrs.GetText(i, 3),ssrs.GetText(i, 4),Double.parseDouble(ssrs.GetText(i, 5)),1)){
    					return false;
    				}
    			}
    		}else{
    			continue;
    		}
    		
    	}
    	return true;
    }
    private boolean preMoney(String begindate,String enddate,double money,int flag){
    	try{
        begindate=begindate.substring(0,begindate.lastIndexOf("-"));
        enddate=enddate.substring(0,enddate.lastIndexOf("-"));
        
    	System.out.println("摊销起期:"+begindate);
    	System.out.println("摊销止期:"+enddate);
        
    	System.out.println("flag:"+flag);
    	System.out.println("money:"+money);
    	System.out.println("月份:"+betwDate(enddate,begindate));
    	double avgMoney=money/betwDate(enddate,begindate);
    	System.out.println("平均值："+avgMoney);
    	
    	if(compareDate(strBeginDate,enddate)==1){
    		if(flag==0){
    			prepreMoney=prepreMoney+money;
    		}else{
    			preMoney=preMoney+money;
    		}
    		
    	}else if(compareDate(begindate,strEndDate)==1){
    		if(flag==0){
    			preNotMoney=preNotMoney+money;
    		}else{
    			NotMoney=NotMoney+money;
    		}
    		
    	}else if(compareDate(strBeginDate,begindate)==1 && compareDate(enddate,strBeginDate)!=-1 && compareDate(strEndDate,enddate)!=-1){
    		if(flag==0){
    			prepreMoney=prepreMoney+(betwDate(strBeginDate,begindate)-1)*avgMoney;
        		prePayMoney=prePayMoney+betwDate(enddate,strBeginDate)*avgMoney;
    		}else{
    			preMoney=preMoney+(betwDate(strBeginDate,begindate)-1)*avgMoney;
        		payMoney=payMoney+betwDate(enddate,strBeginDate)*avgMoney;
    		}
    	}else if(compareDate(strBeginDate,begindate)==1 && compareDate(enddate,strEndDate)==1 ){
    		if(flag==0){
    			prepreMoney=prepreMoney+(betwDate(strBeginDate,begindate)-1)*avgMoney;
        		prePayMoney=prePayMoney+betwDate(strEndDate,strBeginDate)*avgMoney;
        		preNotMoney=preNotMoney+(betwDate(enddate,strEndDate)-1)*avgMoney;
    		}else{
    			preMoney=preMoney+(betwDate(strBeginDate,begindate)-1)*avgMoney;
        		payMoney=payMoney+betwDate(strEndDate,strBeginDate)*avgMoney;
        		NotMoney=NotMoney+(betwDate(enddate,strEndDate)-1)*avgMoney;
    		}
    		
    	}else if(compareDate(begindate,strBeginDate)!=-1 && compareDate(begindate,strEndDate)!=1 && compareDate(enddate,strEndDate)!=1){
    		if(flag==0){
    			prePayMoney=prepreMoney+betwDate(enddate,begindate)*avgMoney;
    		}else{
    			payMoney=payMoney+betwDate(enddate,begindate)*avgMoney;
    		}
    	}else if(compareDate(begindate,strBeginDate)!=-1 && compareDate(begindate,strEndDate)!=1 && compareDate(enddate,strEndDate)==1){
    		if(flag==0){
    			prePayMoney=prepreMoney+betwDate(strEndDate,begindate)*avgMoney;
        		preNotMoney=preNotMoney+(betwDate(enddate,strEndDate)-1)*avgMoney;
    		}else{
    			payMoney=payMoney+betwDate(strEndDate,begindate)*avgMoney;
        		NotMoney=NotMoney+(betwDate(enddate,strEndDate)-1)*avgMoney;
    		}
    	}else{
    		return false;
    	}
    	return true;
    	}catch(Exception e){
    		buildError("preMoney","生成金额错误！");
    		return false;
    	}
    }
    
    /*
     *作用：比较日期m和n的大小
     *返回值：如果m>n 返回1； 如果m=n 返回0 ；如果m<n 返回-1
    */
    private int compareDate(String m,String n){
    	int mYear=Integer.parseInt(m.substring(0,4));
    	int nYear=Integer.parseInt(n.substring(0,4));
    	int mMonth=Integer.parseInt(m.substring(m.indexOf("-")+1,m.length()));
    	int nMonth=Integer.parseInt(n.substring(n.indexOf("-")+1,n.length()));
    	if(mYear>nYear){
    		return 1;
    	}else if(mYear==nYear){
    		if(mMonth>nMonth){
    			return 1;
    		}else if(mMonth==nMonth){
    			return 0;
    		}else{
    			return -1;
    		}
    		
    	}else{
    		return -1;
    	}
    	
    }
    private int betwDate(String m,String n){
    	System.out.println("betwDate:"+m+"和"+n);
    	int mYear=Integer.parseInt(m.substring(0,4));
    	int nYear=Integer.parseInt(n.substring(0,4));
    	int mMonth=Integer.parseInt(m.substring(m.indexOf("-")+1,m.length()));
    	int nMonth=Integer.parseInt(n.substring(n.indexOf("-")+1,n.length()));
    	int months=12*(mYear-nYear)+(mMonth-nMonth)+1;
    	return months;
    	
    }
    //double 类型 保留两位小数 四舍五入
    private double doubleformat(double y) {
    	BigDecimal bg = new BigDecimal(y);
        double m = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		return m;
	}
}
