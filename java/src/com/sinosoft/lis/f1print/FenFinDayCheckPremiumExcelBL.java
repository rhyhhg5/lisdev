package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author hezy lys yh
 * @version 1.0
 */

import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class FenFinDayCheckPremiumExcelBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	// 取得的时间
	private String mDay[] = null;
	//取得文件路径
	private String tOutXmlPath="";

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	public FenFinDayCheckPremiumExcelBL() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		if (!getInputData(cInputData)) {
			return false;
		}
		mResult.clear();
		if (cOperate.equals("PRINTPAY")) // 打印收费
		{
			if (!getPrintDataPay()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData)
	{
		// 全局变量
		mDay = (String[]) cInputData.get(0);
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		tOutXmlPath=(String)cInputData.get(2);
		
		System.out.println("起始日期："+mDay[0]);
		System.out.println("起始日期："+mDay[1]);
		
		if (tOutXmlPath == null || tOutXmlPath=="") {
			buildError("getInputData", "没有获得文件路径信息！");
			return false;
		}
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "FenFinDayCheckPremiumtypeExcelBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
    
    /**
     * 获得结果集的记录总数
     * @return
     */
    private int getNumber(){

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
      
        int no=5;  //初始打印行数
//        int no=7;  //初始打印行数
        
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
        tGetSQLFromXML.setParameters("EndDate", mDay[1]);
        
        String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
 //       tServerPath= "E:/DYL-workspace/Picch/ui/";
        System.out.println("X3日结单测试");
        // 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
        String[] getTypes = new String[] { 
                "QYSS", // 契约实收
                "XQSS", // 续期实收
                "CXYS", // 长险应收保费
                "DXYS",// 短险应收保费（一年期首期生成全年保费）
                "YNQFCYS", // 一年期分次缴费业务应收冲销
                "QYSSGLF", // 契约实收保费-帐户管理费             
                "QYSSBFCJ", // 契约实收保费-保户储金
                "XQSSGLF", // 续期实收保费-帐户管理费
                "XQSSBFCJ", // 续期实收保费-保户储金
                "XQHT", // 续期回退
                "WNQYSS_BHCJ", // 万能契约实收保费-保户储金
                "WNQYSSBF_ZHGLF", // 万能契约实收保费-账户管理费
                "WNXQSSBF_BHCJ", // 万能续期实收保费-保户储金
                "WNXQSSBF_ZHGLF",// 万能续期实收保费-账户管理费
                "XTXQYSSBF_BHCJ", // 新特需契约实收保费-保户储金
                "XTXQYSSBF_ZHGLF", // 新特需契约实收保费-账户管理费
                "XTXXQSSBF_BHCJ", // 新特需续期实收保费-保户储金
                "XTXXQSSBF_ZHGLF", // 新特需续期实收保费-账户管理费
                "XQYSZF", //续期应收作废
                "QYYJJZH",  //契约溢交进帐户(包括个险和团险)
                "YDJF",  //约定缴费-应收
                "YDJF_YSFC",  //约定缴费-缴费金额小于约定金额-应收反冲
                "YDJF_SS",   //约定缴费-缴费金额大于约定金额-实收
                "CXYSJT"//长险应收计提保费

        };

        for (int i = 0; i < getTypes.length; i++) {
            String sql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FenFeePrintSql.xml", getTypes[i]);
//            System.out.println(sql);
            tSSRS = tExeSQL.execSQL(sql);
            int maxrow=tSSRS.getMaxRow();
            no=no+maxrow+2;
        }
        System.out.println("tMaxRowNum:" + no);
        
        return no;
    }

	private boolean getPrintDataPay() {
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		String Msql = "select Name from LDCom where ComCode='"+ mGlobalInput.ManageCom + "'"; // 管理机构
		tSSRS = tExeSQL.execSQL(Msql);
		String manageCom = tSSRS.GetText(1, 1);
		
		//int tMaxRowNum = getNumber();
        
//        String[][] mToExcel = new String[tMaxRowNum+10][4];
        String[][] mToExcel = new String[30000][6];
		mToExcel[0][0] = "首期续期收入日结单";
		mToExcel[1][0] = "统计日期：";
		mToExcel[1][1] = mDay[0]+"至"+mDay[1];
		mToExcel[2][0] = "统计机构：";
		mToExcel[2][1] = manageCom;
//		mToExcel[4][0] = "类型";
//		mToExcel[4][1] = "渠道";
//		mToExcel[4][2] = "保单号码";
//		mToExcel[4][3] = "金额";
		
		mToExcel[4][0] = "类型";
		mToExcel[4][1] = "渠道";
		mToExcel[4][2] = "保单号码";
		mToExcel[4][3] = "金额";
		mToExcel[4][4] = "净金额";
		mToExcel[4][5] = "税额";

	
		int no=5;  //初始打印行数
		
		
		GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
		tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
		tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
		tGetSQLFromXML.setParameters("EndDate", mDay[1]);
		
		String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
	//	tServerPath= "E:/DYL-workspace/Picch/ui/";
		
		// 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
		String[] getTypes = new String[] { 
				"QYSS", // 契约实收
				"XQSS", // 续期实收
				"CXYS", // 长险应收保费
				"DXYS",// 短险应收保费（一年期首期生成全年保费）
				"YNQFCYS", // 一年期分次缴费业务应收冲销
				"QYSSGLF", // 契约实收保费-帐户管理费
				"QYSSBFCJ", // 契约实收保费-保户储金
				"XQSSGLF", // 续期实收保费-帐户管理费
				"XQSSBFCJ", // 续期实收保费-保户储金
				"XQHT", // 续期回退
				"WNQYSS_BHCJ", // 万能契约实收保费-保户储金
				"WNQYSSBF_ZHGLF", // 万能契约实收保费-账户管理费
				"WNXQSSBF_BHCJ", // 万能续期实收保费-保户储金
				"WNXQSSBF_ZHGLF",// 万能续期实收保费-账户管理费
				"XTXQYSSBF_BHCJ", // 新特需契约实收保费-保户储金
				"XTXQYSSBF_ZHGLF", // 新特需契约实收保费-账户管理费
				"XTXXQSSBF_BHCJ", // 新特需续期实收保费-保户储金
				"XTXXQSSBF_ZHGLF", // 新特需续期实收保费-账户管理费
				"XQYSZF", //续期应收作废
				"QYYJJZH",//契约溢交进帐户(包括个险和团险)
				"YDJF",  //约定缴费-应收
				"YDJF_YSFC", //约定缴费-缴费金额小于约定金额-应收反冲
				"YDJF_SS", //约定缴费-缴费金额大于约定金额-实收
				"CXYSJT"//长险应收计提保费

		};

		for (int i = 0; i < getTypes.length; i++) {
			String sql = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FenFeePrintSql.xml", getTypes[i]);
		System.out.println("节点是:"+getTypes[i]+"    sql是："+sql);
		
		String[] split = sql.split(";");
		SSRS mSSRS = new SSRS();
		int maxrowexcel = 0;
/*		int maxcol=mSSRS.getMaxCol();
		System.out.println("列数"+maxcol);*/
		double sumMoney=0.00;
		double moneynotax=0.00;
		double moneytax=0.00;

		for(int s = 0 ;s<split.length;s++){
			
		System.out.println("节点是:"+getTypes[i]+"里的第" +(s+1) +"个分SQL,sql是："+split[s].toString());
		mSSRS = tExeSQL.execSQL(split[s].toString());
		int maxrow = mSSRS.getMaxRow();
		maxrowexcel = maxrowexcel + maxrow;
			for (int row = 1; row <= maxrow; row++)
            {
				System.out.println("循环开始");
				


			  for (int col = 1; col <=6; col++)
                { 
                	if (col==4) {

                		mToExcel[no+row-1][col - 1]=new DecimalFormat("0.00").format(Double.valueOf(mSSRS.GetText(row, col)));
						sumMoney=sumMoney+Double.parseDouble(mSSRS.GetText(row,col));
						System.out.println("节点金额目前累计为：" + sumMoney );
                		System.out.println("第 "+(no+row)+" 行，第  "+col + " 列的值为" +mSSRS.GetText(row, col));
					}else if (col==5){

						if (mSSRS.GetText(row,col) != null && !"".equals(mSSRS.GetText(row,col))){
							mToExcel[no+row-1][col - 1]=new DecimalFormat("0.00").format(Double.valueOf(mSSRS.GetText(row, col)));
							moneynotax=moneynotax+Double.parseDouble(mSSRS.GetText(row,col));
	                		System.out.println("第 "+(no+row)+" 行，第  "+col + " 列的值为" +mSSRS.GetText(row, col));
						}else{
							mToExcel[no+row-1][col - 1]="0.00";
	                		System.out.println("第 "+(no+row)+" 行，第  "+col + " 列的值为" +mSSRS.GetText(row, col));
						}
						
					}else if(col==6){
						if (mSSRS.GetText(row,col) != null && !"".equals(mSSRS.GetText(row,col))){
							mToExcel[no+row-1][col - 1]=new DecimalFormat("0.00").format(Double.valueOf(mSSRS.GetText(row, col)));
							moneytax=moneytax+Double.parseDouble(mSSRS.GetText(row,col));
	                		System.out.println("第 "+(no+row)+" 行，第  "+col + " 列的值为" +mSSRS.GetText(row, col));
						}else{
							mToExcel[no+row-1][col - 1]="0.00";
	                		System.out.println("第 "+(no+row)+" 行，第  "+col + " 列的值为" +mSSRS.GetText(row, col));
						}
						}
				
                	else {
                		System.out.println("col 为1、2、3、7");
						mToExcel[no+row-1][col - 1] = mSSRS.GetText(row, col);  
                		System.out.println("第 "+(no+row)+" 行，第  "+col + " 列的值为" +mSSRS.GetText(row, col));
					}
                    }

                }
			no=no+maxrow;

		}
//no = no+1;
			mToExcel[no][0] =getChineseNameWithPY(getTypes[i]);
			mToExcel[no][2] ="金额合计：";
			mToExcel[no][3] =new DecimalFormat("0.00").format(Double.valueOf(sumMoney));
			mToExcel[no][4] =new DecimalFormat("0.00").format(Double.valueOf(moneynotax));
			mToExcel[no][5] =new DecimalFormat("0.00").format(Double.valueOf(moneytax));
			no=no+2;

		}
		
		//添加制表员和审核员
		mToExcel[no+1][0] = "制表员：";
		mToExcel[no+1][2] = "审核员：";
		 try
	        {
	            System.out.println(tOutXmlPath);
	            WriteToExcel t = new WriteToExcel("");
	            t.createExcelFile();
	            String[] sheetName = { PubFun.getCurrentDate() };
	            t.addSheet(sheetName);
	            t.setData(0, mToExcel);
	            t.write(tOutXmlPath);
	            System.out.println("生成文件完成");
	        }
	        catch (Exception ex)
	        {
	            ex.printStackTrace();
	            buiError("dealData", ex.toString());
	            return false;
	        }
		return true;
	}

	/**
	 * 显示总计无金额的情况
	 * 
	 * @param pName
	 * @return
	 */
	private String getChineseNameWithPY(String pName) {
		String result ="";
		if ("QYSS".equals(pName)) {
			result = "契约实收保费小计";
		} else if ("XQSS".equals(pName)) {
			result = "续期实收小计";
		} else if ("CXYS".equals(pName)) {
			result = "长险应收保费小计";
		}else if ("DXYS".equals(pName)) {
			result = "短险应收保费小计";
        } else if ("YNQFCYS".equals(pName)) {
        	result = "一年期分次缴费业务应收冲销小计";
		} else if ("QYSSGLF".equals(pName)) {
			result = "契约实收保费-帐户管理费小计";
		} else if ("QYSSBFCJ".equals(pName)) {
			result = "契约实收保费-保户储金小计";
		} else if ("XQSSGLF".equals(pName)) {
			result = "续期实收保费-帐户管理费小计";
		} else if ("XQSSBFCJ".equals(pName)) {
			result = "续期实收保费-保户储金小计";
		} else if ("XQHT".equals(pName)) {
			result = "续期回退小计(不包括万能)";
		}else if ("WNQYSS_BHCJ".equals(pName)) {
			result = "万能契约实收保费-保户储金小计";
		}else if ("WNQYSSBF_ZHGLF".equals(pName)) {
			result = "万能契约实收保费-账户管理费小计";
		}else if ("WNXQSSBF_BHCJ".equals(pName)) {
			result = "万能续期实收保费-保户储金小计";
		}else if ("WNXQSSBF_ZHGLF".equals(pName)) {
			result = "万能续期实收保费-账户管理费小计";
		}else if ("XTXQYSSBF_BHCJ".equals(pName)) {
			result = "新特需契约实收保费-保户储金小计";
		}else if ("XTXQYSSBF_ZHGLF".equals(pName)) {
			result = "新特需契约实收保费-账户管理费小计";
		}else if ("XTXXQSSBF_BHCJ".equals(pName)) {
			result = "新特需续期实收保费-保户储金小计";
		}else if ("XTXXQSSBF_ZHGLF".equals(pName)) {
			result = "新特需续期实收保费-账户管理费小计";
		}else if ("QYYJJZH".equals(pName)) {
			result = "契约溢交进帐户小计";
		}else if("XQYSZF".equals(pName)) {
			result = "续期应收作废小计";
		}else if("YDJF".equals(pName)){
			result = "约定缴费小计";
		}else if("YDJF_YSFC".equals(pName)){
			result = "约定缴费-应收反冲小计";
		}else if("YDJF_SS".equals(pName)){
			result = "约定缴费-实收小计";
		}else if("CXYSJT".equals(pName)){
			result = "长险应收计提保费小计";
		}
		
		return result;
	}
    private void buiError(String functionName,String errorMessage){
    	CError tError = new CError();
        tError.moduleName = "FenFinDayCheckPremiumExcelBL";
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        mErrors.addOneError(tError);
    }
}
