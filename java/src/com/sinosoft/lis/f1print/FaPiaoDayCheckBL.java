package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author  hzy lys
 * @date:2003-06-04
 * @version 1.0
 */
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Vector;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CodeJudge;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;

public class FaPiaoDayCheckBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mDay[] = null; //获取时间
    private String mRiskCode;
    private String mRiskType;
    private String mXsqd;
    private String mXzdl;
    private String mSxq;
    private String mDqj;
    private String mRiskName = "";
    private GlobalInput mGlobalInput = new GlobalInput(); //全局变量
    public FaPiaoDayCheckBL()
    {
        mErrors=new CErrors();
        mResult=new VData();
        mDay=null;
        mRiskName="";
        mGlobalInput=new GlobalInput();

    }

    public static void main(String[] args)
    {
//        GlobalInput tG = new GlobalInput();
//        tG.Operator = "001";
//        tG.ManageCom = "86110000";
//        VData vData = new VData();
//        String[] tDay = new String[2];
//        tDay[0] = "2007-01-14";
//        tDay[1] = "2007-07-14";
//        vData.addElement(tDay);
//        vData.addElement(tG);
//
//        TeXuDayCheckBL tF = new TeXuDayCheckBL();
//        tF.submitData(vData, "PRINTGET");

    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINTGET") && !cOperate.equals("PRINTPAY"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();

        if (cOperate.equals("PRINTGET")) //打印收费
        {
            if (!getPrintDataPay())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) //打印付费
    {
        //全局变量
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FinDayCheckBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintDataPay()
    {
        SSRS tSSRS = new SSRS();
        double SumMoney = 0;
        int SumNum=0;
        //GetSQLFromXML tGetSQLFromXML=new GetSQLFromXML();
        //tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        //tGetSQLFromXML.setParameters("BeginDate", mDay[0]);
        //tGetSQLFromXML.setParameters("EndDate", mDay[1]);
        //String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
        //String msql=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/FeePrintSql.xml", "TeXu");
        
        
 				String msql="select payno,(select name from ldperson where customerno=ljapay.appntno union select grpname from ldgrp where customerno=ljapay.appntno) pname,incomeno,sumactupaymoney,paydate,confdate, "
							+"operator from ljapay where 1=1 and managecom like '"+mGlobalInput.ManageCom+"%'"
							+" and exists(select '1' from loprtmanager2 where code='35' "
							+" and otherno=ljapay.payno and makedate>='"+mDay[0]+"' "
							+" and makedate<='"+mDay[1]+"') with ur";
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        ListTable tlistTable = new ListTable();
        String strSum="";
        String strArr[] = null;
        tlistTable.setName("FaPiao");
        SumNum=tSSRS.MaxRow;
        for (int i = 1; i <= SumNum; i++)
        {
            strArr = new String[7];
            for (int j = 1; j <= tSSRS.MaxCol; j++)
            {
                strArr[j - 1] = tSSRS.GetText(i, j);
                if (j == 4 )
                {
                    strArr[j - 1] = tSSRS.GetText(i, j);
                    if(strArr[j - 1]!=null && !strArr[j - 1].trim().equals("") && !strArr[j - 1].trim().equals("null")){
                        strSum = new DecimalFormat("0.00").format(Double.
                                valueOf(strArr[j - 1]));
                        strArr[j - 1] = strSum;
                        SumMoney = SumMoney + Double.parseDouble(strArr[j - 1]);
                    }else{
                        strArr[j - 1]="0.00";
                    }
                    
                }
            }
            tlistTable.add(strArr);
        }
        strArr = new String[8];
        strArr[0]="contno";
        strArr[1]="riskcode";
        strArr[2]="riskname";
        strArr[3]="benjin";
        strArr[4]="lixi";
        strArr[5]="guanlifee";
        strArr[6]="jyzhifu";


        String nsql = "select Name from LDCom where ComCode='" +
               mGlobalInput.ManageCom + "'";
        ExeSQL nExeSQL = new ExeSQL();
        SSRS nSSRS = new SSRS();
        nSSRS = nExeSQL.execSQL(nsql);
        String manageCom = nSSRS.GetText(1, 1);

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("FaPiaoDayCheck.vts", "printer"); //最好紧接着就初始化xml文档
        texttag.add("StartDate", mDay[0]);
        texttag.add("EndDate", mDay[1]);
        texttag.add("ManageCom", manageCom);
        texttag.add("SumMoney", SumMoney);
        

        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        mResult.clear();
        mResult.addElement(xmlexport);
        xmlexport.outputDocumentToFile("E:\\","FaPiao");
        return true;
    }

    private void initCommon()
    {
        this.mRiskCode = "";
        this.mRiskType = "";
        this.mXsqd = "";
        this.mXzdl = "";
        this.mSxq = "";
        this.mDqj = "";
    }


}
