package com.sinosoft.lis.f1print;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.PubFun;

import org.dom4j.*;
import org.dom4j.io.*;
import org.dom4j.dom.*;
import java.io.*;

public class ExportExcelXML {

	private Document mDocument;

	private Element mRoot;

	public ExportExcelXML() {
	}

	/**
	 * 初始化文件
	 * 
	 * @return
	 */
	public boolean createDocument() {
		mDocument = DocumentHelper.createDocument();

		DOMProcessingInstruction tDPI = new DOMProcessingInstruction(
				"mso-application", "progid=\"Excel.Sheet\"");
		mDocument.add(tDPI);
		mRoot = mDocument.addElement("Workbook",
				"urn:schemas-microsoft-com:office:spreadsheet");
		mRoot.addAttribute("xmlns:ss",
				"urn:schemas-microsoft-com:office:spreadsheet");
		return true;
	}

	/**
	 * 添加列表及标题、类型
	 * 
	 * @param aListTable
	 * @param aTitle
	 * @param aType
	 * @return
	 */
	public boolean addListTable(ListTable aListTable, String[] aTitle,
			String[] aType) {
		// 添加表
		Element tWorkSheet = mRoot.addElement("Worksheet").addAttribute(
				"ss:Name", "Sheet1");
		Element tTable = tWorkSheet.addElement("Table");
		// 添加标题行
		Element tTitleRow = tTable.addElement("Row");

		if (aTitle.length != aType.length) {
			System.out.println("标题与类型列数不一致！");
			return false;
		}

		for (int i = 0; i < aTitle.length; i++) {
			Element tCell = tTitleRow.addElement("Cell");
			Element tData = tCell.addElement("Data").addAttribute("ss:Type",
					"String");
			tData.addText(aTitle[i]);
		}

		for (int j = 0; j < aListTable.size(); j++) {
			Element tRow = tTable.addElement("Row");
			for (int m = 0; m < aTitle.length; m++) {

				Element tCell = tRow.addElement("Cell");
				String tDataType = "String";

				if (aType[m] != null && aType[m].length() > 0) {
					tDataType = aType[m];
				}
				
				String tText = aListTable.getValue(m, j);

				if ("DateTime".equals(tDataType)) {
					tDataType = "String";
					tText = FormatDate(tText);
				} else if ("Number".equals(tDataType)) {
					try {
						Double.parseDouble(tText);;
					} catch (Exception ex) {
						tText = "";
					}
				}
				

				Element tData = tCell.addElement("Data").addAttribute(
						"ss:Type", tDataType);
				tData.addText(tText);
			}
		}

		return true;
	}

	/**
	 * 输出.xls文件
	 * 
	 * @param aPathName
	 * @param aFileName
	 *            不需要后缀
	 */
	public void outputDocumentToFile(String aPathName, String aFileName,
			String aEncoding) {
		try {

			String tFile = aPathName + aFileName + ".xls";

			FileWriter fileWriter = new FileWriter(tFile);
			// 设置文件格式
			OutputFormat xmlFormat = new OutputFormat(" ", true, aEncoding);

			// 创建写文件方法
			XMLWriter xmlWriter = new XMLWriter(fileWriter, xmlFormat);

			// 写入文件
			xmlWriter.write(mDocument);
			// 关闭
			xmlWriter.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String FormatDate(String aDate) {
		String rDate = "";
		rDate += aDate;

		if (!PubFun.checkDateForm(rDate)) {
			int days = 0;
			try {
				days = Integer.parseInt(aDate) - 2;
			} catch (Exception ex) {
				return "";
			}
			rDate = PubFun.calDate("1900-1-1", days, "D", null);
		}
		return rDate;
	}

	public static void main(String[] args) {
		ExportExcelXML tExportExcelXML = new ExportExcelXML();
		tExportExcelXML.createDocument();

		String[] tTitle = { "姓名", "性别", "出生日期", "金额" };
		ListTable tListTable = new ListTable();
		String[] tA1 = { "张三", "男", "2001-4-5", "45.12" };
		tListTable.add(tA1);

		String[] tA2 = { "李四", "女", "2004-6-12", "78.10" };
		tListTable.add(tA2);

		String[] tType = { "String", "String", "", "Number" };

		tExportExcelXML.addListTable(tListTable, tTitle, tType);
		tExportExcelXML.outputDocumentToFile("E:/", "sd", "GBK");
	}
}
