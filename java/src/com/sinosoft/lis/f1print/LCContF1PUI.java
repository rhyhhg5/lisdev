/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
//import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;

/*
 * <p>ClassName: LCContF1PUI </p>
 * <p>Description: LCContF1PUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-11-04
 */
public class LCContF1PUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCContSchema mLCContSchema = new LCContSchema();

    private String mTemplatePath = null;
    private String mOutXmlPath = null;
    private String contPrintFlag = "1";//保单是否打印标记：0，不打印；1，打印
    private TransferData mTransferData = null;
    public LCContF1PUI() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        try {
            if (!cOperate.equals("PRINT") // 正常打印
                && !cOperate.equals("REPRINT") // 保单遗失补发
                && !cOperate.equals("PRINTEX")) { // 前台保单打印
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData)) {
                return false;
            }

            // 进行业务处理
            if (!dealData()) {
                return false;
            }

            // 准备传往后台的数据
            VData vData = new VData();

            if (!prepareOutputData(vData)) {
                return false;
            }

            LCContF1PBL f1pLCContBL = new LCContF1PBL();
            System.out.println("Start LCContF1P UI Submit ...");

            if (!f1pLCContBL.submitData(vData, cOperate)) {
                if (f1pLCContBL.mErrors.needDealError()) {
                    mErrors.copyAllErrors(f1pLCContBL.mErrors);
                    return false;
                } else {
                    buildError("sbumitData", "f1pLCContBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            } else {
                mResult = f1pLCContBL.getResult();
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("submit", "发生异常");
            return false;
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     * @param vData VData
     * @return boolean
     */
    private boolean prepareOutputData(VData vData) {
        try {
            vData.clear();
            vData.add(mGlobalInput);
            vData.add(mLCContSchema);
            vData.add(mTemplatePath);
            vData.add(mOutXmlPath);
            vData.add(contPrintFlag);
            vData.add(mTransferData);
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private static boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLCContSchema.setSchema((LCContSchema) cInputData.getObjectByObjectName(
                "LCContSchema", 0));
        mTemplatePath = (String) cInputData.get(2);
        mOutXmlPath = (String) cInputData.get(3);
        try
        {
            contPrintFlag = (String) cInputData.get(4);
        }
        catch(Exception e)
        {
            //do nothing
        }
        mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);

        if (mGlobalInput == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCContF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args) {
        String SQl = "select contno from lccont where prtno in('16000050005')";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(SQl);
        for (int m = 1; m <= tSSRS.getMaxRow(); m++) {
            System.out.println("tSSRS" + tSSRS.GetText(m, 1));
            LCContF1PUI tLCContF1PUI = new LCContF1PUI();
            LCContSchema tLCContSchema = new LCContSchema();
            tLCContSchema.setContNo(tSSRS.GetText(m, 1));
            GlobalInput tGlobalInput = new GlobalInput();
            tGlobalInput.Operator = "UW0009";
            tGlobalInput.ManageCom = "86110000";
            VData vData = new VData();
            vData.addElement(tGlobalInput);
            vData.addElement(tLCContSchema);
            vData.addElement("E:\\01.PICC核心业务系统\\03.lis\\ui\\f1print\\template\\");
            vData.addElement("E:\\01.PICC核心业务系统\\03.lis\\ui\\");
            tLCContF1PUI.submitData(vData, "PRINT");

        }

    }
}
