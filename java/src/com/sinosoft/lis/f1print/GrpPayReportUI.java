package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class GrpPayReportUI implements PrintService
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    //业务处理相关变量
    /** 全局数据 */
    String StartDate = "";
    String EndDate = "";
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOBatchPRTManagerSchema mLOBatchPRTManagerSchema = new LOBatchPRTManagerSchema();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    public GrpPayReportUI()
    {
    }

    /**
         传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        // 准备传往后台的数据
        VData vData = new VData();

        if (!prepareOutputData(vData))
        {
            return false;
        }

        GrpPayReportBL tGrpPayReportBL = new GrpPayReportBL();
        System.out.println("Start PayAppPrt UI Submit ...");

        if (!tGrpPayReportBL.submitData(vData, cOperate))
        {
            if (tGrpPayReportBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tGrpPayReportBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "PayColPrtBL发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tGrpPayReportBL.getResult();
            return true;
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            TransferData cTransferData = new TransferData();
            cTransferData.setNameAndValue("StartDate", StartDate);
            cTransferData.setNameAndValue("EndDate",EndDate);

            vData.add(mGlobalInput);
            vData.add(cTransferData);
            vData.add(mLOBatchPRTManagerSchema);
            vData.add(mLOBatchPRTManagerSchema);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        mLOBatchPRTManagerSchema.setSchema((LOBatchPRTManagerSchema) cInputData.getObjectByObjectName("LOBatchPRTManagerSchema", 0));
        TransferData tTransferData = (TransferData) cInputData.
                                     getObjectByObjectName("TransferData", 0);
        StartDate = (String) tTransferData.getValueByName("StartDate");
        EndDate = (String) tTransferData.getValueByName("EndDate");
        System.out.println("wwwwwwwwwwwwwwww" + EndDate);
        if (mLOBatchPRTManagerSchema == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "DerferAppF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    public static void main(String[] args)
    {
        GrpPayReportUI tGrpPayReportUI = new GrpPayReportUI();
//        LCContSchema tLCContSchema = new LCContSchema();
        LOBatchPRTManagerSchema tLOBatchPRTManagerSchema = new LOBatchPRTManagerSchema();
        tLOBatchPRTManagerSchema.setOtherNo("00000002");
        tLOBatchPRTManagerSchema.setStandbyFlag3("0000000202");
        tLOBatchPRTManagerSchema.setStandbyFlag2("1");
        VData tVData = new VData();
        tVData.addElement(tLOBatchPRTManagerSchema);
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "xuxin";
        tVData.addElement(tG);
        TransferData tTransferData = new TransferData();
    String StartDate ="2006-01-01";
    String EndDate = "2006-09-01";
    tTransferData.setNameAndValue("StartDate", StartDate);
    tTransferData.setNameAndValue("EndDate",EndDate);
    tVData.addElement(tTransferData);

        tGrpPayReportUI.submitData(tVData, "PRINT");
    }

    public CErrors getErrors() {
        return null;
    }
}
