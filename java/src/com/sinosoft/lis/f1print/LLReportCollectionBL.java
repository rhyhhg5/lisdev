package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Xx
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;
import java.text.DecimalFormat;

public class LLReportCollectionBL
    implements PrintService {

/** 错误处理类，每个需要错误处理的类中都放置该类 */
public CErrors mErrors = new CErrors();

private VData mResult = new VData();


//业务处理相关变量
/** 全局数据 */
private GlobalInput mGlobalInput = new GlobalInput();
private TransferData mTransferData = new TransferData();
private String mFileNameB = "";
private String moperator = "";
private String mMakeDate = "";
public LLReportCollectionBL() {
}

/**
 传输数据的公共方法
 */
public boolean submitData(VData cInputData, String cOperate) {
    if (!cOperate.equals("PRINT")) {
        buildError("submitData", "不支持的操作字符串");
        return false;
    }

    // 得到外部传入的数据，将数据备份到本类中
    if (!getInputData(cInputData)) {
        return false;
    }
    System.out.println("1 ...");
    mResult.clear();

    // 准备所有要打印的数据
    if (!getPrintData()) {
        return false;
    }else{
		TransferData tTransferData= new TransferData();
		tTransferData.setNameAndValue("tFileNameB",mFileNameB );
		tTransferData.setNameAndValue("tMakeDate", mMakeDate);
		tTransferData.setNameAndValue("tOperator", moperator);
		LLPrintSave tLLPrintSave = new LLPrintSave();
		VData tVData = new VData();
		tVData.addElement(tTransferData);
		if(!tLLPrintSave.submitData(tVData,"")){
			return false;
		     }
	}
    return true;
}

/**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
 */
private boolean dealData() {
    return true;
}

/**
 * 从输入数据中得到所有对象
 * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData) {
    //全局变量
    mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
            "GlobalInput", 0));
    mTransferData = ((TransferData) cInputData.getObjectByObjectName(
            "TransferData", 0));

    if (mTransferData == null) {
        buildError("getInputData", "没有得到足够的信息！");
        return false;
    }
    mFileNameB = (String)mTransferData.getValueByName("tFileNameB");
    moperator = mGlobalInput.Operator;
    return true;
}

public VData getResult() {
    return mResult;
}

public CErrors getErrors() {
    return mErrors;
}

private void buildError(String szFunc, String szErrMsg) {
    CError cError = new CError();

    cError.moduleName = "LCContF1PBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
}

private boolean getPrintData() {
    boolean PEFlag = false; //打印理赔申请回执的判断标志
    boolean shortFlag = false; //打印备注信息
    TextTag texttag = new TextTag(); //新建一个TextTag的实例
    //根据印刷号查询打印队列中的纪录
    String[] strCol;
    String MStartDate = (String) mTransferData.getValueByName("MStartDate");
    String MEndDate = (String) mTransferData.getValueByName("MEndDate");
    String YStartDate = (String) mTransferData.getValueByName("YStartDate");
    String YEndDate = (String) mTransferData.getValueByName("YEndDate");
    String Year = (String) mTransferData.getValueByName("Year");
    String Month = (String) mTransferData.getValueByName("Month");

    //ListTable
    ListTable appColListTable = new ListTable();
    appColListTable.setName("Report");
    String[] Title = {
                     "ComCode", "ComName", "MCaseCount", "MSubRptCount",
                     "MPayAmnt", "MPayRate", "MSpan","YCaseCount",
                     "YSubRptCount","YPayAmnt", "YPayRate","YSpan"};
    ExeSQL exesql = new ExeSQL();
    String sql0 = "select round((TO_DATE('"+MEndDate+"')-TO_DATE('"+MStartDate+"')+1)/365,5) from dual with ur ";
    String diff = exesql.getOneValue(sql0);
    String sql01 = "select round((TO_DATE('"+YEndDate+"')-TO_DATE('"+YStartDate+"')+1)/365,5) from dual with ur";
    String diff2 = exesql.getOneValue(sql01);
    String Sql = "select A,B,C,D,E,F,coalesce(G,0),coalesce(H,0),coalesce(I,0),coalesce(J,0),coalesce(K,0),coalesce(L,0),G1,H1 FROM ( SELECT A.COMCODE A ,A.NAME B, " +
                 "( SELECT COUNT ( A2.CASENO ) FROM LLCASE A2 WHERE A2.ENDCASEDATE IS NOT NULL AND SUBSTR ( A2.MNGCOM , 1 , 4 )=SUBSTR ( A.COMCODE , 1 , 4 ) " +
                 " AND A2.ENDCASEDATE BETWEEN '" + MStartDate + "' AND '" + MEndDate +"' AND A2.RGTTYPE='1' AND (A2.RGTSTATE<>'14' or A2.cancledate > '"+MEndDate+"') )  C, " +
                 "( SELECT COUNT ( A3.CASENO ) FROM LLCASE A3 WHERE  A3.ENDCASEDATE IS NOT NULL AND SUBSTR ( A3.MNGCOM , 1 , 4 )=SUBSTR ( A.COMCODE , 1 , 4 ) " +
                 " AND A3.ENDCASEDATE BETWEEN '" + YStartDate + "' AND '" + YEndDate + "' AND A3.RGTTYPE='1' AND (A3.RGTSTATE<>'14' or A3.cancledate > '"+YEndDate+"') ) D, " +
                 "( SELECT COUNT ( B4.SUBRPTNO ) FROM LLCASE A4,LLCASERELA B4 WHERE B4.CASENO=A4.CASENO AND SUBSTR ( A4.MNGCOM , 1 , 4 )=SUBSTR ( A.COMCODE , 1 , 4 ) " +
                 " AND A4.ENDCASEDATE BETWEEN '" + MStartDate + "' AND '" + MEndDate + "' AND A4.RGTTYPE='1' AND (A4.RGTSTATE<>'14' or A4.cancledate > '"+MEndDate+"')  ) E, " +
                 "( SELECT COUNT ( B5.SUBRPTNO ) FROM LLCASE A5,LLCASERELA B5 WHERE B5.CASENO=A5.CASENO AND SUBSTR ( A5.MNGCOM , 1 , 4 )=SUBSTR ( A.COMCODE , 1 , 4 ) " +
                 " AND A5.ENDCASEDATE BETWEEN '" + YStartDate + "' AND '" + YEndDate + "' AND A5.RGTTYPE='1' AND (A5.RGTSTATE<>'14' or A5.cancledate > '"+YEndDate+"')  ) F, " +
                 "( SELECT SUM ( B6.REALPAY ) FROM LLCLAIMDETAIL B6,LLCASE A6 WHERE  A6.ENDCASEDATE IS NOT NULL AND SUBSTR ( A6.MNGCOM , 1 , 4 )=SUBSTR ( A.COMCODE , 1 , 4 )  and A6.CASENO=B6.CASENO " +
                 " AND A6.ENDCASEDATE BETWEEN '" + MStartDate + "' AND '" + MEndDate + "' AND A6.RGTTYPE='1' AND (A6.RGTSTATE<>'14' or A6.cancledate > '"+MEndDate+"')  ) G, " +
                 "( SELECT SUM ( B7.REALPAY ) FROM LLCLAIMDETAIL B7,LLCASE A7 WHERE  A7.ENDCASEDATE IS NOT NULL AND SUBSTR ( A7.MNGCOM , 1 , 4 )=SUBSTR ( A.COMCODE , 1 , 4 )  and A7.CASENO=B7.CASENO " +
                 " AND A7.ENDCASEDATE BETWEEN '" + YStartDate + "' AND '" + YEndDate + "' AND A7.RGTTYPE='1' AND (A7.RGTSTATE<>'14' or A7.cancledate > '"+YEndDate+"') ) H, " +

                 "  ( select sum(B) from (select ManageCom,case when payintv<>0 then  (days('"+MEndDate+"')-days(cvalidate)+1)*prem*12*0.65/payintv/365 when payintv=0 then (days('"+MEndDate+"')-days(cvalidate)+1)*prem*0.65/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate between '"+MStartDate+"' and '"+MEndDate+"' and enddate>'"+MEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='1'    union all "+

                 " select ManageCom,case when payintv<>0 then (days(enddate)-days('"+MStartDate+"')+1)*prem*12*0.65/payintv/365 when  payintv=0  then (days(enddate)-days('"+MStartDate+"')+1)*prem*0.65/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate<'"+MStartDate+"' and enddate  between '"+MStartDate+"' and '"+MEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='1'  union all "+

                 " select ManageCom,case  when payintv<>0 then (days('"+MEndDate+"')-days('"+MStartDate+"')+1)*prem*12*0.65/payintv/365  when payintv=0 then (days('"+MEndDate+"')-days('"+MStartDate+"')+1)*prem*0.65/(days(enddate)-days(cvalidate)+1)  end B from lcpol where cvalidate<'"+MStartDate+"' and enddate >'"+MEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='1'  union all "+

                 " select ManageCom,case  when payintv<>0  then (days(enddate)-days(cvalidate)+1+1)*prem*12*0.65/payintv/365  when payintv=0  then (days(enddate)-days(cvalidate)+1+1)*prem*0.65/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate between '"+MStartDate+"' and '"+MEndDate+"' and enddate<='"+MEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7')  and conttype='1'  union all "+

                 " select ManageCom,case when payintv<>0 then  (days('"+MEndDate+"')-days(cvalidate)+1)*prem*12*0.75/payintv/365 when payintv=0 then (days('"+MEndDate+"')-days(cvalidate)+1)*prem*0.75/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate between '"+MStartDate+"' and '"+MEndDate+"' and enddate>'"+MEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='2'   union all "+

                 " select ManageCom,case when payintv<>0 then (days(enddate)-days('"+MStartDate+"')+1)*prem*12*0.75/payintv/365 when  payintv=0  then (days(enddate)-days('"+MStartDate+"')+1)*prem*0.75/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate<'"+MStartDate+"' and enddate  between '"+MStartDate+"' and '"+MEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='2'  union all "+

                 " select ManageCom,case  when payintv<>0 then (days('"+MEndDate+"')-days('"+MStartDate+"')+1)*prem*12*0.75/payintv/365  when payintv=0 then (days('"+MEndDate+"')-days('"+MStartDate+"')+1)*prem*0.75/(days(enddate)-days(cvalidate)+1)  end B from lcpol where cvalidate<'"+MStartDate+"' and enddate >'"+MEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='2'  union all "+

                 " select ManageCom,case  when payintv<>0  then (days(enddate)-days(cvalidate)+1+1)*prem*12*0.75/payintv/365  when payintv=0  then (days(enddate)-days(cvalidate)+1+1)*prem*0.75/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate between '"+MStartDate+"' and '"+MEndDate+"' and enddate<='"+MEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7')  and conttype='2'  " +

                 " ) Y  where   SUBSTR(ManageCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE   ) I , "+

                 " ( select sum(B) from (select ManageCom,case when payintv<>0 then  (days('"+YEndDate+"')-days(cvalidate)+1)*prem*12*0.65/payintv/365 when payintv=0 then (days('"+YEndDate+"')-days(cvalidate)+1)*prem*0.65/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate between '"+YStartDate+"' and '"+YEndDate+"' and enddate>'"+YEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='1'   union all "+

                 " select ManageCom,case when payintv<>0 then (days(enddate)-days('"+YStartDate+"')+1)*prem*12*0.65/payintv/365 when  payintv=0  then (days(enddate)-days('"+YStartDate+"')+1)*prem*0.65/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate<'"+YStartDate+"' and enddate  between '"+YStartDate+"' and '"+YEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='1'   union all "+

                 " select ManageCom,case  when payintv<>0 then (days('"+YEndDate+"')-days('"+YStartDate+"')+1)*prem*12*0.65/payintv/365  when payintv=0 then (days('"+YEndDate+"')-days('"+YStartDate+"')+1)*prem*0.65/(days(enddate)-days(cvalidate)+1)  end B from lcpol where cvalidate<'"+YStartDate+"' and enddate >'"+YEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='1' union all "+

                 " select ManageCom,case  when payintv<>0  then (days(enddate)-days(cvalidate)+1+1)*prem*12*0.65/payintv/365  when payintv=0  then (days(enddate)-days(cvalidate)+1+1)*prem*0.65/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate between '"+YStartDate+"' and '"+YEndDate+"' and enddate<='"+YEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7')  and conttype='1'   union all "+

                 " select ManageCom,case when payintv<>0 then  (days('"+YEndDate+"')-days(cvalidate)+1)*prem*12*0.75/payintv/365 when payintv=0 then (days('"+YEndDate+"')-days(cvalidate)+1)*prem*0.75/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate between '"+YStartDate+"' and '"+YEndDate+"' and enddate>'"+YEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='2'     union all "+

                 " select ManageCom,case when payintv<>0 then (days(enddate)-days('"+YStartDate+"')+1)*prem*12*0.75/payintv/365 when  payintv=0  then (days(enddate)-days('"+YStartDate+"')+1)*prem*0.75/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate<'"+YStartDate+"' and enddate  between '"+YStartDate+"' and '"+YEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='2'   union all "+

                 " select ManageCom,case  when payintv<>0 then (days('"+YEndDate+"')-days('"+YStartDate+"')+1)*prem*12*0.75/payintv/365  when payintv=0 then (days('"+YEndDate+"')-days('"+YStartDate+"')+1)*prem*0.75/(days(enddate)-days(cvalidate)+1)  end B from lcpol where cvalidate<'"+YStartDate+"' and enddate >'"+YEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='2'  union all "+

                 " select ManageCom,case  when payintv<>0  then (days(enddate)-days(cvalidate)+1+1)*prem*12*0.75/payintv/365  when payintv=0  then (days(enddate)-days(cvalidate)+1+1)*prem*0.75/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate between '"+YStartDate+"' and '"+YEndDate+"' and enddate<='"+YEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7')  and conttype='2'  "+

                 " ) Y    where   SUBSTR ( ManageCom , 1 , 4 )=SUBSTR ( A.COMCODE , 1 , 4 ) ) J, "+
                 "( SELECT SUM(TO_DATE(j.makedate)-TO_DATE(A10.RGTDATE)+1) FROM LLCASE A10,ljagetclaim j WHERE  SUBSTR ( A10.MNGCOM , 1 , 4 )=SUBSTR ( A.COMCODE , 1 , 4 ) " +
                 " AND j.makedate IS NOT NULL AND j.makedate BETWEEN '" + MStartDate +
                 "' AND '" + MEndDate + "' AND A10.RGTTYPE='1' AND (A10.RGTSTATE<>'14' or A10.cancledate > '"+MEndDate+"') and A10.caseno=j.otherno  ) K, " +
                 "( SELECT SUM(TO_DATE(j.makedate)-TO_DATE(A11.RGTDATE)+1) FROM LLCASE A11,ljagetclaim j WHERE  SUBSTR ( A11.MNGCOM , 1 , 4 )=SUBSTR ( A.COMCODE , 1 , 4 ) " +
                 " AND j.makedate IS NOT NULL AND j.makedate BETWEEN '" + YStartDate +
                 "' AND '" + YEndDate + "' AND A11.RGTTYPE='1' AND (A11.RGTSTATE<>'14' or A11.cancledate > '"+YEndDate+"') and A11.caseno=j.otherno  ) L, " +
                 "( SELECT SUM ( B16.REALPAY ) FROM LLCLAIMDETAIL B16,LLCASE A16 WHERE  A16.ENDCASEDATE IS NOT NULL AND SUBSTR ( A16.MNGCOM , 1 , 4 )=SUBSTR ( A.COMCODE , 1 , 4 )  and A16.CASENO=B16.CASENO and B16.riskcode  not in (select riskcode from lmriskapp where risktype3='7') " +
                 " AND A16.ENDCASEDATE BETWEEN '" + MStartDate + "' AND '" + MEndDate + "' AND A16.RGTTYPE='1'  AND (A16.RGTSTATE<>'14' or A16.cancledate > '"+MEndDate+"') ) G1, " +
                 "( SELECT SUM ( B17.REALPAY ) FROM LLCLAIMDETAIL B17,LLCASE A17 WHERE  A17.ENDCASEDATE IS NOT NULL AND SUBSTR ( A17.MNGCOM , 1 , 4 )=SUBSTR ( A.COMCODE , 1 , 4 )  and A17.CASENO=B17.CASENO and B17.riskcode  not in (select riskcode from lmriskapp where risktype3='7') " +
                 " AND A17.ENDCASEDATE BETWEEN '" + YStartDate + "' AND '" + YEndDate + "' AND A17.RGTTYPE='1' AND (A17.RGTSTATE<>'14' or A17.cancledate > '"+YEndDate+"') ) H1 " +

                 "FROM LDCOM A WHERE LENGTH ( trim( A.COMCODE) )=4  AND A.SIGN='1' ORDER BY A ) AS X with ur";
   System.out.println("lawalawalawa:"+Sql);
    SSRS COUNT = exesql.execSQL(Sql);
    String[][] temdata = null;
    if (COUNT != null && COUNT.getMaxRow() > 0) {
        temdata = COUNT.getAllData();
    }
    for (int i = 0; i < COUNT.getMaxRow(); i++) {

        strCol = new String[12];
        strCol[0] = temdata[i][0];
        strCol[1] = temdata[i][1];
        strCol[2] = temdata[i][2];
        strCol[3] = temdata[i][4];
        strCol[4] = temdata[i][6];
        double mrealpay = 0.0;
        if (temdata[i][12].equals("null"))
            temdata[i][12]="0";
        mrealpay = Double.parseDouble(temdata[i][12]);
         double mpassprem = 0.0;
         String tmpassprem = "";
         tmpassprem = temdata[i][8];
         if (tmpassprem.equals("null"))
             tmpassprem="0";
         mpassprem = Double.parseDouble(tmpassprem);
         if (mpassprem<0.001)
             strCol[5] = "0";
         else
         {
             double claimrate = mrealpay / mpassprem*100;
             claimrate = Arith.round(claimrate, 2);
             strCol[5] = claimrate + "";
         }

        if(temdata[i][2].equals("0"))
        {
            strCol[6] ="0";
        }else{
        strCol[6] = String.valueOf(PubFun.setPrecision(Double.parseDouble(temdata[i][10])/Double.parseDouble(temdata[i][2]), "0.0"));
        }
        strCol[7] = temdata[i][3];
        strCol[8] = temdata[i][5];
        strCol[9] = temdata[i][7];
        double yrealpay = 0.0;
        if (temdata[i][13].equals("null"))
           temdata[i][13]="0";
        yrealpay = Double.parseDouble(temdata[i][13]);
         double ypassprem = 0.0;
         String typassprem = "";
         typassprem = temdata[i][9];
         if (typassprem.equals("null"))
             typassprem="0";
         ypassprem = Double.parseDouble(typassprem);
         if (ypassprem<0.001)
             strCol[10] = "0";
         else
         {
              System.out.println("P-yrealpay="+yrealpay+"ypassprem="+ypassprem+"diff="+diff);
             double claimrate = yrealpay / ypassprem*100;
             claimrate = Arith.round(claimrate, 2);
             strCol[10] = claimrate + "";
         }
         if(temdata[i][3].equals("0"))
         {
             strCol[11]="0";
         }else{
         strCol[11] =  String.valueOf(PubFun.setPrecision(Double.parseDouble(temdata[i][11])/Double.parseDouble(temdata[i][3]), "0.0"));
         }

        appColListTable.add(strCol);
    }

    //ListTable
  ListTable sumTable = new ListTable();
  sumTable.setName("Sum");
  String[] Title1 = {
                   "ComCode", "ComName", "MCaseCount", "MSubRptCount",
                   "MPayAmnt", "MPayRate", "MSpan","YCaseCount",
                   "YSubRptCount","YPayAmnt", "YPayRate","YSpan"};
  ExeSQL exesql1 = new ExeSQL();
  String Sql1 = "select A,B,C,D,E,F,coalesce(G,0),coalesce(H,0),ROUND(I,2),ROUND(J,2),coalesce(K,0),coalesce(L,0),G1,H1 FROM ( SELECT '' A ,'总   计' B, " +
                "( SELECT COUNT ( A2.CASENO ) FROM LLCASE A2 WHERE A2.ENDCASEDATE IS NOT NULL  " +
                " AND A2.ENDCASEDATE BETWEEN '" + MStartDate + "' AND '" + MEndDate +"' AND A2.RGTTYPE='1' AND (A2.RGTSTATE<>'14' or A2.cancledate > '"+MEndDate+"')  )  C, " +
                "( SELECT COUNT ( A3.CASENO ) FROM LLCASE A3 WHERE  A3.ENDCASEDATE IS NOT NULL  " +
                " AND A3.ENDCASEDATE BETWEEN '" + YStartDate + "' AND '" + YEndDate + "' AND A3.RGTTYPE='1' AND (A3.RGTSTATE<>'14' or A3.cancledate > '"+YEndDate+"')  ) D, " +
                "( SELECT COUNT ( B4.SUBRPTNO ) FROM LLCASE A4,LLCASERELA B4 WHERE B4.CASENO=A4.CASENO " +
                " AND A4.ENDCASEDATE BETWEEN '" + MStartDate + "' AND '" + MEndDate + "' AND A4.RGTTYPE='1'  AND (A4.RGTSTATE<>'14' or A4.cancledate > '"+MEndDate+"') ) E, " +
                "( SELECT COUNT ( B5.SUBRPTNO ) FROM LLCASE A5,LLCASERELA B5 WHERE B5.CASENO=A5.CASENO " +
                " AND A5.ENDCASEDATE BETWEEN '" + YStartDate + "' AND '" + YEndDate + "' AND A5.RGTTYPE='1'  AND (A5.RGTSTATE<>'14' or A5.cancledate > '"+YEndDate+"') ) F, " +
                "( SELECT SUM ( B6.REALPAY ) FROM LLCLAIMDETAIL B6,LLCASE A6 WHERE  A6.ENDCASEDATE IS NOT NULL  and A6.CASENO=B6.CASENO " +
                " AND A6.ENDCASEDATE BETWEEN '" + MStartDate + "' AND '" + MEndDate + "' AND A6.RGTTYPE='1'  AND (A6.RGTSTATE<>'14' or A6.cancledate > '"+MEndDate+"') ) G, " +
                "( SELECT SUM ( B7.REALPAY ) FROM LLCLAIMDETAIL B7,LLCASE A7 WHERE  A7.ENDCASEDATE IS NOT NULL and A7.CASENO=B7.CASENO " +
                " AND A7.ENDCASEDATE BETWEEN '" + YStartDate + "' AND '" + YEndDate + "' AND A7.RGTTYPE='1' AND (A7.RGTSTATE<>'14' or A7.cancledate > '"+YEndDate+"') ) H, " +

                "  ( select sum(B) from (select case when payintv<>0 then  (days('"+MEndDate+"')-days(cvalidate)+1)*prem*12*0.65/payintv/365 when payintv=0 then (days('"+MEndDate+"')-days(cvalidate)+1)*prem*0.65/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate between '"+MStartDate+"' and '"+MEndDate+"' and enddate>'"+MEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='1'    union all "+

                " select case when payintv<>0 then (days(enddate)-days('"+MStartDate+"')+1)*prem*12*0.65/payintv/365 when  payintv=0  then (days(enddate)-days('"+MStartDate+"')+1)*prem*0.65/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate<'"+MStartDate+"' and enddate  between '"+MStartDate+"' and '"+MEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='1'  union all "+

                " select case  when payintv<>0 then (days('"+MEndDate+"')-days('"+MStartDate+"')+1)*prem*12*0.65/payintv/365  when payintv=0 then (days('"+MEndDate+"')-days('"+MStartDate+"')+1)*prem*0.65/(days(enddate)-days(cvalidate)+1)  end B from lcpol where cvalidate<'"+MStartDate+"' and enddate >'"+MEndDate+"' and appflag='1'and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='1'  union all "+

                " select case  when payintv<>0  then (days(enddate)-days(cvalidate)+1+1)*prem*12*0.65/payintv/365  when payintv=0  then (days(enddate)-days(cvalidate)+1+1)*prem*0.65/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate between '"+MStartDate+"' and '"+MEndDate+"' and enddate<='"+MEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7')  and conttype='1'  union all "+

                " select case when payintv<>0 then  (days('"+MEndDate+"')-days(cvalidate)+1)*prem*12*0.75/payintv/365 when payintv=0 then (days('"+MEndDate+"')-days(cvalidate)+1)*prem*0.75/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate between '"+MStartDate+"' and '"+MEndDate+"' and enddate>'"+MEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='2'   union all "+

                " select case when payintv<>0 then (days(enddate)-days('"+MStartDate+"')+1)*prem*12*0.75/payintv/365 when  payintv=0  then (days(enddate)-days('"+MStartDate+"')+1)*prem*0.75/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate<'"+MStartDate+"' and enddate  between '"+MStartDate+"' and '"+MEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='2'  union all "+

                " select case  when payintv<>0 then (days('"+MEndDate+"')-days('"+MStartDate+"')+1)*prem*12*0.75/payintv/365  when payintv=0 then (days('"+MEndDate+"')-days('"+MStartDate+"')+1)*prem*0.75/(days(enddate)-days(cvalidate)+1)  end B from lcpol where cvalidate<'"+MStartDate+"' and enddate >'"+MEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='2'  union all "+

                " select case  when payintv<>0  then (days(enddate)-days(cvalidate)+1+1)*prem*12*0.75/payintv/365  when payintv=0  then (days(enddate)-days(cvalidate)+1+1)*prem*0.75/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate between '"+MStartDate+"' and '"+MEndDate+"' and enddate<='"+MEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7')  and conttype='2'  " +

                " ) Y   ) I , "+

                " ( select sum(B) from (select case when payintv<>0 then  (days('"+YEndDate+"')-days(cvalidate)+1)*prem*12*0.65/payintv/365 when payintv=0 then (days('"+YEndDate+"')-days(cvalidate)+1)*prem*0.65/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate between '"+YStartDate+"' and '"+YEndDate+"' and enddate>'"+YEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='1'   union all "+

                " select case when payintv<>0 then (days(enddate)-days('"+YStartDate+"')+1)*prem*12*0.65/payintv/365 when  payintv=0  then (days(enddate)-days('"+YStartDate+"')+1)*prem*0.65/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate<'"+YStartDate+"' and enddate  between '"+YStartDate+"' and '"+YEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='1'   union all "+

                " select case  when payintv<>0 then (days('"+YEndDate+"')-days('"+YStartDate+"')+1)*prem*12*0.65/payintv/365  when payintv=0 then (days('"+YEndDate+"')-days('"+YStartDate+"')+1)*prem*0.65/(days(enddate)-days(cvalidate)+1)  end B from lcpol where cvalidate<'"+YStartDate+"' and enddate >'"+YEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='1' union all "+

                " select case  when payintv<>0  then (days(enddate)-days(cvalidate)+1+1)*prem*12*0.65/payintv/365  when payintv=0  then (days(enddate)-days(cvalidate)+1+1)*prem*0.65/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate between '"+YStartDate+"' and '"+YEndDate+"' and enddate<='"+YEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7')  and conttype='1'   union all "+

                " select case when payintv<>0 then  (days('"+YEndDate+"')-days(cvalidate)+1)*prem*12*0.75/payintv/365 when payintv=0 then (days('"+YEndDate+"')-days(cvalidate)+1)*prem*0.75/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate between '"+YStartDate+"' and '"+YEndDate+"' and enddate>'"+YEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='2'     union all "+

                " select case when payintv<>0 then (days(enddate)-days('"+YStartDate+"')+1)*prem*12*0.75/payintv/365 when  payintv=0  then (days(enddate)-days('"+YStartDate+"')+1)*prem*0.75/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate<'"+YStartDate+"' and enddate  between '"+YStartDate+"' and '"+YEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='2'   union all "+

                " select case  when payintv<>0 then (days('"+YEndDate+"')-days('"+YStartDate+"')+1)*prem*12*0.75/payintv/365  when payintv=0 then (days('"+YEndDate+"')-days('"+YStartDate+"')+1)*prem*0.75/(days(enddate)-days(cvalidate)+1)  end B from lcpol where cvalidate<'"+YStartDate+"' and enddate >'"+YEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='2'  union all "+

                " select case  when payintv<>0  then (days(enddate)-days(cvalidate)+1+1)*prem*12*0.75/payintv/365  when payintv=0  then (days(enddate)-days(cvalidate)+1+1)*prem*0.75/(days(enddate)-days(cvalidate)+1) end B from lcpol where cvalidate between '"+YStartDate+"' and '"+YEndDate+"' and enddate<='"+YEndDate+"' and appflag='1' and riskcode  not in (select riskcode from lmriskapp where risktype3='7') and conttype='2'  "+

                " ) Y   ) J, "+
                "( SELECT SUM(TO_DATE(j.makedate)-TO_DATE(A10.RGTDATE)+1) FROM LLCASE A10,ljagetclaim j WHERE   " +
                "  j.makedate IS NOT NULL AND j.makedate BETWEEN '" + MStartDate +
                "' AND '" + MEndDate + "' AND A10.RGTTYPE='1'  AND (A10.RGTSTATE<>'14' or A10.cancledate > '"+MEndDate+"') and A10.caseno=j.otherno ) K, " +
                "( SELECT SUM(TO_DATE(j.makedate)-TO_DATE(A11.RGTDATE)+1) FROM LLCASE A11,ljagetclaim j WHERE  " +
                "  j.makedate IS NOT NULL AND j.makedate BETWEEN '" + YStartDate +
                "' AND '" + YEndDate + "' AND A11.RGTTYPE='1'  AND (A11.RGTSTATE<>'14' or A11.cancledate > '"+YEndDate+"') and A11.caseno=j.otherno) L, " +
                "( SELECT SUM ( B16.REALPAY ) FROM LLCLAIMDETAIL B16,LLCASE A16 WHERE  A16.ENDCASEDATE IS NOT NULL   and A16.CASENO=B16.CASENO and B16.riskcode  not in (select riskcode from lmriskapp where risktype3='7') " +
                " AND A16.ENDCASEDATE BETWEEN '" + MStartDate + "' AND '" + MEndDate + "' AND A16.RGTTYPE='1'  AND (A16.RGTSTATE<>'14' or A16.cancledate > '"+MEndDate+"') ) G1, " +
                "( SELECT SUM ( B17.REALPAY ) FROM LLCLAIMDETAIL B17,LLCASE A17 WHERE  A17.ENDCASEDATE IS NOT NULL   and A17.CASENO=B17.CASENO and B17.riskcode  not in (select riskcode from lmriskapp where risktype3='7') " +
                " AND A17.ENDCASEDATE BETWEEN '" + YStartDate + "' AND '" + YEndDate + "' AND A17.RGTTYPE='1' AND (A17.RGTSTATE<>'14' or A17.cancledate > '"+YEndDate+"') ) H1 " +
                 "FROM DUAL ORDER BY A ) AS X with ur";
  System.out.println("lawalawa2:"+Sql1);
  SSRS COUNT1 = exesql1.execSQL(Sql1);
  String[][] temdata1 = null;
  if (COUNT1 != null && COUNT1.getMaxRow() > 0) {
      temdata1 = COUNT1.getAllData();
  }
  for (int i = 0; i < COUNT1.getMaxRow(); i++) {

      strCol = new String[12];
      strCol[0] = temdata1[i][0];
      strCol[1] = temdata1[i][1];
      strCol[2] = temdata1[i][2];
      strCol[3] = temdata1[i][4];
      strCol[4] = temdata1[i][6];
      double mrealpay = 0.0;
      if (temdata[i][12].equals("null"))
            temdata[i][12]="0";
      mrealpay = Double.parseDouble(temdata[i][12]);
       double mpassprem = 0.0;
       String tmpassprem = "";
       tmpassprem = temdata1[i][8];
       if (tmpassprem.equals("null"))
           tmpassprem="0";
       mpassprem = Double.parseDouble(tmpassprem);
       if (mpassprem<0.001)
           strCol[5] = "0";
       else
       {
           double claimrate = mrealpay / mpassprem*100;
           claimrate = Arith.round(claimrate, 2);
           strCol[5] = claimrate + "";
       }
       if(temdata1[i][2].equals("0"))
       {
           strCol[6] ="0";
      }else{
      strCol[6] = String.valueOf(PubFun.setPrecision(Double.parseDouble(temdata1[i][10])/Double.parseDouble(temdata1[i][2]), "0.0"));       }
      strCol[7] = temdata1[i][3];
      strCol[8] = temdata1[i][5];
      strCol[9] = temdata1[i][7];
      double yrealpay = 0.0;
      if (temdata[i][13].equals("null"))
         temdata[i][13]="0";
        yrealpay = Double.parseDouble(temdata[i][13]);
       double ypassprem = 0.0;
       String typassprem = "";
       typassprem = temdata1[i][9];
       if (typassprem.equals("null"))
           typassprem="0";
       ypassprem = Double.parseDouble(typassprem);
       if (ypassprem<0.001)
           strCol[10] = "0";
       else
       {
           System.out.print("yrealpay="+yrealpay+"ypassprem="+ypassprem+"diff="+diff);
           double claimrate = yrealpay / ypassprem*100;
           claimrate = Arith.round(claimrate, 2);
           strCol[10] = claimrate + "";
       }
       if(temdata1[i][3].equals("0"))
       {
           strCol[11]="0";
       }else{
       strCol[11] =  String.valueOf(PubFun.setPrecision(Double.parseDouble(temdata1[i][11])/Double.parseDouble(temdata1[i][3]), "0.0"));
       }

      sumTable.add(strCol);
  }


    //其它模版上单独不成块的信息

    XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
    xmlexport.createDocument("LLReportCollection.vts", "printer"); //最好紧接着就初始化xml文档
    //生成-年-月-日格式的日期
    StrTool tSrtTool = new StrTool();
    String SysDate = tSrtTool.getYear() + "年" + tSrtTool.getMonth() + "月" +
                     tSrtTool.getDay() + "日";
    String StaDate = Year+"年"+Month+"月";
    //PayColPrtBL模版元素
    texttag.add("SysDate", SysDate);
    texttag.add("StaDate",StaDate);
    String Maker=mGlobalInput.Operator;
    System.out.print(Maker);
    texttag.add("Maker",Maker);
    String CurrentDate = PubFun.getCurrentDate();
    mMakeDate = CurrentDate;
    CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");
    String YYMMDD="";
    YYMMDD = CurrentDate.substring(0, CurrentDate.indexOf("-")) + "年"
       + CurrentDate.substring(CurrentDate.indexOf("-") + 1,CurrentDate.lastIndexOf("-")) + "月"
       + CurrentDate.substring(CurrentDate.lastIndexOf("-") + 1) + "日";
    texttag.add("MakeDate",YYMMDD);
    if (texttag.size() > 0) {
        xmlexport.addTextTag(texttag);
    }
    //保存信息
        xmlexport.addListTable(appColListTable, Title); //保存申请资料信息
         xmlexport.addListTable(sumTable, Title1);
    xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
    mResult.clear();
    mResult.addElement(xmlexport);

    return true;
}

public static void main(String[] args) {
    TransferData PrintElement = new TransferData();
    PrintElement.setNameAndValue("MStartDate", "2006-03-01");
    PrintElement.setNameAndValue("MEndDate", "2006-03-31");
    PrintElement.setNameAndValue("YStartDate", "2006-1-1");
    PrintElement.setNameAndValue("YEndDate", "2006-03-31");
    PrintElement.setNameAndValue("Year", "2006");
    PrintElement.setNameAndValue("Month", "03");

    GlobalInput tG = new GlobalInput();
    tG.ManageCom = "86";
    tG.Operator = "xuxin";
    VData tVData = new VData();
    tVData.addElement(PrintElement);
    tVData.addElement(tG);

    LLReportCollectionBL tLLReportCollectionBL = new LLReportCollectionBL();
    tLLReportCollectionBL.submitData(tVData, "PRINT");
}
}
