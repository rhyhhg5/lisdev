/**
 * 
 */
package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * @author sino
 *
 */
public class PrintChannelBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData inputData;
    private MMap map = new MMap();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData(); //返回结果
    
    private GlobalInput globalInput = new GlobalInput(); //存储用户信息
    private String operate = ""; //单证数量
    
    private LDCode1Schema printChannel;

    /**
     * 传输数据的公共方法
     * 
     * @param inputData
     * @param operate
     * @return
     */
	public boolean submitData(VData inputData, String operate)
    {
		this.operate = operate;
		if (!getInputData(inputData))
        {
            return false;
        }
		if (!checkDate())
        {
            return false;
        }
		if (!dealDate())
        {
            return false;
        }
        if (!prepareData()) {
            return false;
        }
        
		return true;
    }
	
	/**
	 * 接收从客户端传来的参数
	 * 
	 * @param inputData
	 * @return
	 */
	private boolean getInputData(VData inputData) {
		globalInput = (GlobalInput) inputData.getObjectByObjectName("GlobalInput", 0);
		printChannel = (LDCode1Schema) inputData.getObjectByObjectName("LDCode1Schema", 0);
		if (globalInput == null || printChannel == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "IndiDueFeeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
		return true;
	}

	/**
	 * 校验数据
	 * 
	 * @return
	 */
	private boolean checkDate() {
		
		return true;
	}

	/**
	 * 数据处理
	 * @return
	 */
	private boolean dealDate() {
		LDCode1DB tLDCode1DB = new LDCode1DB();
		tLDCode1DB.setCodeType(printChannel.getCodeType());
		tLDCode1DB.setCode(printChannel.getCode());
		tLDCode1DB.setCode1(printChannel.getCode1());
		if(operate.equals(SysConst.INSERT))
		{
			tLDCode1DB.setCodeName(printChannel.getCodeName());
			tLDCode1DB.setCodeAlias(printChannel.getCodeAlias());
			tLDCode1DB.insert();
		}
		else if(operate.equals(SysConst.UPDATE))
		{
			tLDCode1DB.setCodeName(printChannel.getCodeName());
			tLDCode1DB.setCodeAlias(printChannel.getCodeAlias());
			tLDCode1DB.update();
		}
		else if(operate.equals(SysConst.DELETE))
		{
			tLDCode1DB.delete();
		}
		return true;
	}
	
	/*
	 * 
	 * 
	 */
	private boolean prepareData() {
		
		return true;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO 自动生成方法存根

	}

}
