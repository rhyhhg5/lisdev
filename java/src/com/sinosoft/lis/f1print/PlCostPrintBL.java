package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author 刘岩松
 * function ://打印“银行代扣正确清单包括（首期和续期）”
 * @version 1.0
 * @date 2004-6-28
 */

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LBPolSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.StrTool;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.pubfun.*;

public class PlCostPrintBL {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String placeno = "";        //职场编码
    private String strComLevel = "";    //机构级别
    private String strMngCom = "";      //机构代码
    private String strBeginDate="";     //统计起期
    private String strEndDate="";       //统计止期
    private GlobalInput mG = new GlobalInput();
    private String mFlag = "";

    public PlCostPrintBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {

        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();
        if (!getPrintData()) {
            return false;
        }

        return true;
    }

    private boolean getInputData(VData cInputData) {
    	placeno = (String) cInputData.get(0);
    	strMngCom = (String) cInputData.get(1);
    	strComLevel=(String) cInputData.get(2);
    	strBeginDate=(String) cInputData.get(3);
    	strEndDate=(String) cInputData.get(4);
    	
    	System.out.println("strBeginDate:"+strBeginDate);
    	System.out.println("strEndDate:"+strEndDate);

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "PlCostPrintBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    private boolean getPrintData() 
    {

		SSRS tSSRS = new SSRS();

		String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
//		String tServerPath= "D:/workspace/picc/WebContent/";


		TextTag texttag = new TextTag(); // 新建一个TextTag的实例
		XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
		xmlexport.createDocument("PlCostPrint.vts", "printer"); // 最好紧接着就初始化xml文档
		String strArr[] = new String[25];
		if (texttag.size() > 0) 
		{
			xmlexport.addTextTag(texttag);
		}
		String[] detailArr = new String[] {"序号","机构名称","机构级别","编码","职场地址","租赁起止期","合计","团险销售部","个险销售部","银行保险部","健康管理部","运行管理部","小计","总经理室","人事行政部","计划财务部","公共区域","联合办公","其他","小计","租金","物业费","装修成本","当期变动情况","上一职场编码","状态"};
		{
			String sql = " select * from (SELECT char(rownumber() over(order by Placeno)) as rolnum, a.Comname, (case a.comlevel when '1' then '省级分公司' when '2' then '地市级公司' when '3' then '四级机构-区' else '四级机构-县' end) , " +
					     " a.Placeno, a.Address, trim(char(a.Begindate) || '至' || char(a.Actualenddate)),(nvl(sigarea,0) + nvl(grparea,0) + nvl(bankarea,0) + nvl(healtharea,0) + nvl(servicearea,0) +nvl(manageroffice,0) + nvl(departpersno,0) + nvl(planfinance,0) + nvl(jointarea,0) + nvl(publicarea,0) +nvl(otherarea,0)) as sumarea, " +
					     " (select nvl(sum(Money),0) from LIPlaceRentFee b where a.placeno = b.placeno and b.Feetype in('01','02','03')), "+
					     " (select nvl(sum(Money),0) from LIPlaceRentFee b where a.placeno = b.placeno and b.Feetype = '01'), " +
					     " (select nvl(sum(Money),0) from LIPlaceRentFee b where a.placeno = b.placeno and b.Feetype = '02'), " +
					     " (select nvl(sum(Money),0) from LIPlaceRentFee b where a.placeno = b.placeno and b.Feetype = '03'), ";
			if(!"".equals(strBeginDate) && !"".equals(strEndDate))
			{
				sql += " LF_PLACECHANGE(a.placeno,date('"+strBeginDate+"'),date('"+strEndDate+"')),";
			}
//			sql=sql+" (case when '"+strEndDate+"'>a.actualenddate or  '"+strEndDate+"'<a.Begindate then '停用' else '在用' END) FROM LIPlaceRentInfo a where 1 = 1";
			sql=sql+" (case when '"+strEndDate+"'>a.actualenddate or  '"+strEndDate+"'<a.Begindate then '停用' else '在用' END)," +
					"nvl(a.Grparea, 0),nvl(a.Sigarea, 0),nvl(a.Bankarea, 0),nvl(a.Healtharea, 0),nvl(a.Servicearea, 0)," +
					"nvl(a.Manageroffice + a.Departpersno + a.Planfinance, 0)," +
					"nvl(a.Manageroffice, 0),nvl(a.Departpersno, 0),nvl(a.Planfinance, 0)," +
					"nvl(a.Publicarea, 0),nvl(a.Jointarea, 0),nvl(a.Otherarea, 0)," +
					"(SELECT placenorefer FROM LIPlaceChangeTrace c WHERE changestate='03' AND placenorefer is not null AND c.placeno=a.placeno) " +
					" FROM LIPlaceRentInfo a where 1 = 1";
			if(!"".equals(placeno))
			{
				sql += " and placeno='"+placeno+"'";
			}
			if(!"".equals(strMngCom))
			{
				sql += " and managecom like'"+strMngCom+"%'";
			}
			if(!"".equals(strComLevel))
			{
				sql += " and comlevel='"+strComLevel+"'";
			}
			if(!"".equals(strBeginDate) && !"".equals(strEndDate))
			{
				sql += " and actualenddate>='"+strBeginDate+"' and Begindate<='"+strEndDate+"'";
			}
			sql += " and state<>'02' ) as aa";
			
			System.out.println("sql:"+sql);
			ExeSQL main_exesql = new ExeSQL();
		    SSRS main_ssrs = main_exesql.execSQL(sql);
			ListTable tDetailListTable = new ListTable(); // 明细ListTable
			tDetailListTable.setName("INFO");
			System.out.println("MaxRow---------"+main_ssrs.MaxRow);
            System.out.println("MaxCol---------"+main_ssrs.MaxCol);
			for(int i=1;i<=main_ssrs.MaxRow;i++)
			{
				strArr = new String[26];
				
				for(int j=1;j<=main_ssrs.MaxCol;j++)
				{
					if(j==9 || j==10 || j==11){
            			strArr[j - 1] = main_ssrs.GetText(i, j);
            			String strSum="";
        				if(strArr[j - 1]!=null && (!"".equals(strArr[j - 1])))
        				{
        					if (j==9) {
        						strSum = new DecimalFormat("0.00").format(Double.valueOf(getMoney(main_ssrs.GetText(i, 4),"01")));
							}else if (j==10) {
								strSum = new DecimalFormat("0.00").format(Double.valueOf(getMoney(main_ssrs.GetText(i, 4),"02")));
							}else if (j==11) {
								strSum = new DecimalFormat("0.00").format(Double.valueOf(getMoney(main_ssrs.GetText(i, 4),"03")));
							}
        					strArr[j - 1] = strSum;
        				 }
            		}else{
            			if(main_ssrs.GetText(i, j)==null){
            				strArr[j - 1] = "";
            			}
            			strArr[j - 1] = main_ssrs.GetText(i, j);
            		}
				}
				strArr[7]=new DecimalFormat("0.00").format(Double.valueOf(Double.valueOf(strArr[8]).doubleValue()+Double.valueOf(strArr[9]).doubleValue()+Double.valueOf(strArr[10]).doubleValue()));
				tDetailListTable.add(strArr);
			}
			xmlexport.addListTable(tDetailListTable, detailArr); // 明细ListTable
																	// 放入xmlexport对象
		}

		mResult.clear();
		mResult.addElement(xmlexport);

		return true;

    }
    
    private double getMoney(String placeNo,String state) {
    	double sumFee=0.00;
    	  	
    	String actualEndDataSQL = "select actualenddate from LIPlaceRentInfo where placeno='"+placeNo+"'";
	    System.out.println("actualEndDataSQL:"+actualEndDataSQL);
	    ExeSQL actualEnd_exesql = new ExeSQL();
	    SSRS actualEnd_ssrs = actualEnd_exesql.execSQL(actualEndDataSQL);
	    String actualEndDate = actualEnd_ssrs.GetText(1, 1);
	    System.out.println("actualEndDate---"+actualEndDate);
    	
    	String getMoneySQL="select * from LIPlaceRentFee where placeno='"+placeNo+"' and Feetype='"+state+"' and (year(Paybegindate)*100+month(Paybegindate))<=(year('"+strEndDate+"')*100+month('"+strEndDate+"')) and (year(Payenddate)*100+month(Payenddate))>=(year('"+strBeginDate+"')*100+month('"+strBeginDate+"')) and Paybegindate <='"+actualEndDate+"'";
    	System.out.println("getMoneySQL:"+getMoneySQL);
		ExeSQL getMoney_exesql = new ExeSQL();
	    SSRS getMoney_ssrs = getMoney_exesql.execSQL(getMoneySQL);
	    System.out.println("费用getMoney_ssrs----"+getMoney_ssrs.MaxRow);
	    
	    for(int i=1;i<=getMoney_ssrs.MaxRow;i++)
		{
	    	double tMoney=Double.valueOf(getMoney_ssrs.GetText(i, 5)).doubleValue();
	    	String tPaybegindate=getMoney_ssrs.GetText(i, 6);
	    	String tPayenddate=getMoney_ssrs.GetText(i, 7);
	    	String tBeginDate=strBeginDate;
	    	String tEndDate=strEndDate;
	    	if(compareDate(strEndDate,tPayenddate)==1){
	    		tEndDate=tPayenddate;	
	    	}
	    	if(compareDate(tEndDate, actualEndDate)==1){
	    		tEndDate=actualEndDate;
	    	}
	    	if(compareDate(tPaybegindate,tBeginDate)==1){
	    		tBeginDate=tPaybegindate;
	    	}
	    	
	    	System.out.println("tEndDate----"+tEndDate);
	    	System.out.println("tBeginDate----"+tBeginDate);
	    	if(compareDate(actualEndDate, tBeginDate)!= -1){
	    		sumFee=sumFee+((tMoney/betwDate(tPayenddate,tPaybegindate))*betwDate(tEndDate,tBeginDate));
	    		System.out.println("sumFee----"+sumFee);
	    	}
		}
		return sumFee;
	}
    
    /*
     *作用：比较日期m和n的大小
     *返回值：如果m>n 返回1； 如果m=n 返回0 ；如果m<n 返回-1
    */
    private int compareDate(String m,String n){
    	int mYear=Integer.parseInt(m.substring(0,4));
    	int nYear=Integer.parseInt(n.substring(0,4));
    	int mMonth=Integer.parseInt(m.substring(m.indexOf("-")+1,m.lastIndexOf("-")));
    	int nMonth=Integer.parseInt(n.substring(n.indexOf("-")+1,n.lastIndexOf("-")));
    	if(mYear>nYear){
    		return 1;
    	}else if(mYear==nYear){
    		if(mMonth>nMonth){
    			return 1;
    		}else if(mMonth==nMonth){
    			return 0;
    		}else{
    			return -1;
    		}
    		
    	}else{
    		return -1;
    	}
    	
    }
    private int betwDate(String m,String n){
    	int mYear=Integer.parseInt(m.substring(0,4));
    	int nYear=Integer.parseInt(n.substring(0,4));
    	int mMonth=Integer.parseInt(m.substring(m.indexOf("-")+1,m.lastIndexOf("-")));
    	int nMonth=Integer.parseInt(n.substring(n.indexOf("-")+1,n.lastIndexOf("-")));
    	int months=12*(mYear-nYear)+(mMonth-nMonth)+1;
    	return months;
    	
    }
   public static void main(String[] args) {
		//System.out.println(compareDate("2011-1-1","2013-01-01"));
	    //String mString="2011-11-1";
		//System.out.println("2011-1-1".lastIndexOf("-"));
		//System.out.println(mString.substring(mString.indexOf("-")+1,mString.lastIndexOf("-")));
	}
}
