/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.f1print;

import java.io.*;
import java.math.BigDecimal;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.Date;

import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>
 * ClassName: LCGrpContF1PBL
 * </p>
 * <p>
 * Description: 保单xml文件生成，数据准备类
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: sinosoft
 * </p>
 * 
 * @Database: LIS
 * @CreateDate：2002-11-04 以后增加补充协议时需要将getScanDoc函数,private String[][] mDocFile =
 *                        null;注释取消
 */
public class LCJXGrpContF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private String mOperate = "";

    private File mFile = null;

    // 业务处理相关变量

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    private LCGrpAppntSchema mLCGrpAppntSchema = new LCGrpAppntSchema();

    private LCGrpAddressSchema mLCGrpAddressSchema = new LCGrpAddressSchema();

    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

    private LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupSchema();
    
    private LAComSchema mLAComSchema = new LAComSchema();
    private LAAgenttempSchema mLAAgenttempSchema = new LAAgenttempSchema();

    private LDComSchema mLDComSchema = new LDComSchema();

    private String mTemplatePath = null;

    private String mOutXmlPath = null;

    private XMLDatasets mXMLDatasets = null;

    private String[][] mScanFile = null;

    private String mSQL;

    private String ContPrintType = null;

    private String[] PubRiskcode = new String[10];

    private String PubRiskcodePrintFlag = "0";

    private String printInsureDetail = "1";// 被保险人清单是否打印标记：0，不打印；1，打印

    private String contPrintFlag = "1";// 保单是否打印标记：0，不打印；1，打印
    
    private String mJetFormType = "J104";
    
    private String testFlag = "new";//试点打印标记 

    // private String[][] mDocFile = null;

    /*
     * 对于同时传入主险和附加险保单号的情况，如果它们是同一个印刷号的， 将被存在同一个保单数据块中。所以将打印过的保单号存放在这个Vector中。
     */
    // private Vector m_vGrpPolNo = new Vector();
    public LCJXGrpContF1PBL()
    {
    }

    private String getDate(Date date)
    {

        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日24时止");
        return df.format(date);
    }

    /**
     * 主程序
     * 
     * @param cInputData
     *            VData
     * @param cOperate
     *            String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            // 判定打印类型
            if (!cOperate.equals("PRINT") && !cOperate.equals("REPRINT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }
            // 全局变量赋值
            mOperate = cOperate;
            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }
            // 数据校验

            if (!checkdate())
            {
                return false;
            }

            ExeSQL tExeSQL = new ExeSQL();
            ContPrintType = tExeSQL.getOneValue("select ContPrintType from lcgrpcont where grpcontno='"
                    + this.mLCGrpContSchema.getGrpContNo() + "'");
            // 正常打印处理
            if (mOperate.equals("PRINT"))
            {
                // 新建一个xml对象
                mXMLDatasets = new XMLDatasets();
                mXMLDatasets.createDocument();

                // 数据准备
                if (!getPrintData())
                {
                    return false;
                }
            }
            // 重打处理
            if (mOperate.equals("REPRINT"))
            {
                if (!getRePrintData())
                {
                    return false;
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submit", ex.getMessage());
            return false;
        }
    }

    /**
     * 从输入数据中得到所有对象
     * 
     * @param cInputData
     *            VData
     * @return boolean 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        // 正常打印
        if (mOperate.equals("PRINT"))
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
            mLCGrpContSchema.setSchema((LCGrpContSchema) cInputData.getObjectByObjectName("LCGrpContSchema", 0));
            mTemplatePath = (String) cInputData.get(2);
            mOutXmlPath = (String) cInputData.get(3);
            try
            {
                printInsureDetail = (String) cInputData.get(4);
                contPrintFlag = (String) cInputData.get(5);
            }
            catch (Exception e)
            {
                // do nothing
            }
        }
        // 重打模式
        if (mOperate.equals("REPRINT"))
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
            mLCGrpContSchema.setSchema((LCGrpContSchema) cInputData.getObjectByObjectName("LCGrpContSchema", 0));
        }
        return true;
    }

    /**
     * 返回信息
     * 
     * @return VData
     */
    public VData getResult()
    {
        return this.mResult;
    }

    /**
     * 出错处理
     * 
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LCGrpContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 正常打印处理
     * 
     * @return boolean
     * @throws Exception
     */
    private boolean getPrintData() throws Exception
    {
        // 团单Schema
        LCGrpContSchema tLCGrpContSchema = null;
        // 原则上一次只打印一个团单，当然可以一次多打
        tLCGrpContSchema = this.mLCGrpContSchema;

        // 取得该团单下的所有投保险种信息
        LCGrpPolSet tLCGrpPolSet = getRiskList(tLCGrpContSchema.getGrpContNo());
        // 正常打印流程，目前只关心这个
        if (!getGrpPolDataSet(tLCGrpPolSet))
        {
            return false;
        }

        // 将准备好的数据，放入mResult对象，传递给BLS进行后台操作
        // mResult.add(tLCGrpPolSet);//其实没必要更新这个表
        this.mLCGrpContSchema.setPrintCount(1);
        mResult.add(this.mLCGrpContSchema); // 只更新这个表就好拉
        mResult.add(mXMLDatasets); // xml数据流
        mResult.add(mGlobalInput); // 全局变量
        mResult.add(mOutXmlPath); // 输出目录
        mResult.add(mScanFile);
        mResult.add(contPrintFlag); // 保单是否打印标记：0，不打印；1，打印
        mResult.add(testFlag);
        // mResult.add(mDocFile); //协议影印件数组

        // 后台提交
        LCGrpContF1PBLS tLCGrpContF1PBLS = new LCGrpContF1PBLS();
        tLCGrpContF1PBLS.submitData(mResult, "PRINT");
        // 判错处理
        if (tLCGrpContF1PBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tLCGrpContF1PBLS.mErrors);
            buildError("saveData", "提交数据库出错！");
            return false;
        }
        // 返回数据容器清空
        mResult.clear();
        System.out.println("add inputstream to mResult");
        // 将xml信息放入返回容器
        mResult.add(mXMLDatasets.getInputStream());
        return true;
    }

   
    /**
     * 查询影印件数据
     * 
     * @param cXmlDataset
     *            XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean getScanPic(XMLDataset cXmlDataset) throws Exception
    {
        XMLDataList tXmlDataList = new XMLDataList();
        // 标签对象，团单需要的影印件有两种类型
        tXmlDataList.setDataObjectID("PicFile");

        // 标签中的Header
        tXmlDataList.addColHead("FileUrl");
        tXmlDataList.addColHead("HttpUrl");
        tXmlDataList.addColHead("PageIndex");
        tXmlDataList.addColHead("FileName");
        tXmlDataList.buildColHead();
        if (ContPrintType.equals("0"))
            mSQL = "select * from es_doc_main where doccode = '" + this.mLCGrpContSchema.getPrtNo()
                    + "' order by doccode";
        else
            mSQL = "select * from es_doc_main where doccode = '" + this.mLCGrpContSchema.getPrtNo()
                    + "' and busstype<>'HM' order by doccode";
        System.out.println("mSQL is " + mSQL);
        ES_DOC_MAINDB tES_DOC_MAINDB = new ES_DOC_MAINDB();
        ES_DOC_MAINSet tES_DOC_MAINDBSet = new ES_DOC_MAINSet();
        tES_DOC_MAINDBSet = tES_DOC_MAINDB.executeQuery(mSQL);
        int piccount = 1;
        if (tES_DOC_MAINDBSet.size() > 0)
        {
            ES_SERVER_INFODB tES_SERVER_INFODB = new ES_SERVER_INFODB();
            ES_SERVER_INFOSet tES_SERVER_INFOSet = new ES_SERVER_INFOSet();
            tES_SERVER_INFOSet = tES_SERVER_INFODB.query();
            String thttphead = "http://" + tES_SERVER_INFOSet.get(1).getServerPort() + "/";
//            String trealpath = this.getClass().getResource("/").getPath();
            String trealpath = new ExeSQL().getOneValue("select SysVarValue from LDSysVar where SysVar = 'UIRoot'");
            
            int k = 0;
            for (int i = 0; i < tES_DOC_MAINDBSet.size(); i++)
            {
                k = k + (int) tES_DOC_MAINDBSet.get(i + 1).getNumPages();
            }
            String tStrUrl = "";
            mScanFile = new String[k][3];
            for (int j = 0; j < tES_DOC_MAINDBSet.size(); j++)
            {
                ES_DOC_PAGESDB tES_DOC_PAGESDB = new ES_DOC_PAGESDB();
                ES_DOC_PAGESSet tES_DOC_PAGESSet = new ES_DOC_PAGESSet();
                // tES_DOC_PAGESDB.setDocID(tES_DOC_MAINDBSet.get(j +
                // 1).getDocID());
                // tES_DOC_PAGESSet = tES_DOC_PAGESDB.query();
                tES_DOC_PAGESSet = tES_DOC_PAGESDB.executeQuery("select * from es_doc_pages where docid="
                        + tES_DOC_MAINDBSet.get(j + 1).getDocID() + " order by pagecode asc");
                if (tES_DOC_PAGESSet.size() > 0)
                {
                    for (int m = 0; m < tES_DOC_PAGESSet.size(); m++)
                    {
                    	
                    	String tpath = tES_DOC_PAGESSet.get(m + 1).getPicPath();
                    	String tcheckhead = trealpath+tpath;
                        String tCheckUrl = tcheckhead
                                + tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                        File cScanFile = new File(tCheckUrl);
                        if(!cScanFile.exists()){
                    	  String tnewpath = tpath.replaceAll("EasyScan2016","EasyScan2014");
                          tcheckhead = trealpath+tnewpath;
                       	  tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){
                        	     tnewpath = tpath.replaceAll("EasyScan2016","EasyScan2012");
                          tcheckhead = trealpath+tnewpath;
                          tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){
                            	 tnewpath = tpath.replaceAll("EasyScan2016","EasyScan2011");
                          tcheckhead = trealpath+tnewpath;
                          tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){
                    	         tnewpath = tpath.replaceAll("EasyScan2016","EasyScan");
                          tcheckhead = trealpath+tnewpath;
                          tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){//以EasyScan2014为基础
                                 tnewpath = tpath.replaceAll("EasyScan2014","EasyScan2012");
                          tcheckhead = trealpath+tnewpath;
                          tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){
                            	 tnewpath = tpath.replaceAll("EasyScan2014","EasyScan2011");
                          tcheckhead = trealpath+tnewpath;
                          tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){
                                 tnewpath = tpath.replaceAll("EasyScan2014","EasyScan");
                          tcheckhead = trealpath+tnewpath;
                          tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){//以EasyScan2012为基础
                                 tnewpath = tpath.replaceAll("EasyScan2012","EasyScan2011");
                          tcheckhead = trealpath+tnewpath;
                          tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){
                                 tnewpath = tpath.replaceAll("EasyScan2012","EasyScan");
                          tcheckhead = trealpath+tnewpath;
                          tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){//以EasyScan2011为基础
                                 tnewpath = tpath.replaceAll("EasyScan2011","EasyScan");
                          tcheckhead = trealpath+tnewpath;
                          tCheckUrl = tcheckhead+ tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                              cScanFile=new File(tCheckUrl);
                          if(!cScanFile.exists()){
                          	System.out.println("新老路径都不存在文件");
                          	int count=j+1;
                          	buildError("getScanPic", "第"+count+"条影印件未查询到！");
                          	return false;
                          	                    }else{
                                                	tpath=tnewpath;
                                                	System.out.println("2011-EasyScan新路径存在文件");
                                                }
                                                }else{
                                            	tpath=tnewpath;
                                            	System.out.println("2012-EasyScan新路径存在文件");
                                                }
                                                }else{
                                        	    tpath=tnewpath;
                                        	    System.out.println("2012-2011新路径存在文件");
                                                }
                                                }else{
                                    	        tpath=tnewpath;
                                    	        System.out.println("2014-EasyScan新路径存在文件");
                                                }
                                                }else{ 
                                                tpath=tnewpath;
                                	            System.out.println("2014-2011新路径存在文件");
                                                }
                                                }else{
                            	                tpath=tnewpath;
                            	                System.out.println("2014-2012新路径存在文件");
                                                }
                                                }else{	tpath=tnewpath;
                          	                    System.out.println("2016-EasyScan新路径存在文件");
                          	                    } 
                                                }else{
                     		                    tpath=tnewpath;
                            	                System.out.println("2016-2011新路径存在文件");
                            	                }
                                                }else{
                     		                    tpath=tnewpath;
                            	                System.out.println("2016-2012新路径存在文件");
                            	                }
                                                }else{
                    		                    tpath=tnewpath;
                           	                    System.out.println("2016-2014新路径存在文件");
                           	                    }
                                                }
                        else{
                        	System.out.println("未变更路径存在文件");
                        }
                        tStrUrl = thttphead + tpath
                                + tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                        mScanFile[piccount - 1][0] = tStrUrl;
                        mScanFile[piccount - 1][1] = this.mLCGrpContSchema.getGrpContNo() + "_" + k + "_" + piccount
                                + ".tif";
                        mScanFile[piccount - 1][2] = tES_DOC_PAGESSet.get(m + 1).getPageName() + ".tif";
                        piccount++;
                    }
                }
            }
        }
        System.out.println("ddddfdf");
        if (mScanFile != null)
        {
            System.out.println("sdsf");
            for (int i = 0; i < mScanFile.length; i++)
            {
                tXmlDataList.setColValue("FileUrl", mScanFile[i][1]);
                tXmlDataList.setColValue("HttpUrl", mScanFile[i][0]);
                tXmlDataList.setColValue("PageIndex", i + 1);
                tXmlDataList.setColValue("FileName", mScanFile[i][2]);
                tXmlDataList.insertRow(0);
            }
        }
        cXmlDataset.addDataObject(tXmlDataList);
        // 添加图片标记节点
        if (piccount > 1)
            cXmlDataset.addDataObject(new XMLDataTag("PicFileFlag", "1"));
        else
            cXmlDataset.addDataObject(new XMLDataTag("PicFileFlag", "0"));
        return true;
    }

    /**
     * 条款信息查询
     * 
     * @param cXmlDataset
     *            XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean getSpecDoc(XMLDataset cXmlDataset) throws Exception
    {
        XMLDataList xmlDataList = new XMLDataList();
        // 添加xml一个新的对象Term
        xmlDataList.setDataObjectID("LCCGrpSpec");
        // 添加Head单元内的信息
        xmlDataList.addColHead("RowID");
        xmlDataList.addColHead("SpecContent");
        xmlDataList.buildColHead();
        // 查询合同下的特约信息
        LCCGrpSpecDB tLCCGrpSpecDB = new LCCGrpSpecDB();
        tLCCGrpSpecDB.setProposalGrpContNo(this.mLCGrpContSchema.getProposalGrpContNo());
        LCCGrpSpecSet tLCCGrpSpecSet = tLCCGrpSpecDB.query();
        for (int i = 1; i <= tLCCGrpSpecSet.size(); i++)
        {
            xmlDataList.setColValue("RowID", i);
            xmlDataList.setColValue("SpecContent", tLCCGrpSpecSet.get(i).getSpecContent());
            xmlDataList.insertRow(0);
        }
        cXmlDataset.addDataObject(xmlDataList);
        return true;
    }

    /**
     * 条款信息查询
     * 
     * @param cXmlDataset
     *            XMLDataset
     * @param cLCGrpPolSet
     *            LCGrpPolSet
     * @return boolean
     * @throws Exception
     */
    private boolean getTerm(XMLDataset cXmlDataset, LCGrpPolSet cLCGrpPolSet) throws Exception
    {
        XMLDataList tXmlDataList = new XMLDataList();
        // 添加xml一个新的对象Term
        tXmlDataList.setDataObjectID("Term");
        // 添加Head单元内的信息
        tXmlDataList.addColHead("PrintIndex");
        tXmlDataList.addColHead("TermName");
        tXmlDataList.addColHead("FileName");
        tXmlDataList.addColHead("DocumentName");
        tXmlDataList.buildColHead();

        // 判断保单是否为分公司打印
        String sql = "select count(1) from LDCode1 where CodeType = 'printchannel' and CodeName = '1' "
                + "and Code = '" + this.mLCGrpContSchema.getManageCom().substring(0, 4) + "' and Code1 = '2' ";
        boolean isFilialePrint = new ExeSQL().getOneValue(sql).equals("1");

        // 设置查询对象
        ExeSQL tExeSQL = new ExeSQL();
        // 查询保单下的险种条款，根据条款级别排序
        // 查询合同下的全部唯一主条款信息
        String tSql;
        if (isFilialePrint)
        {
            // 分公司打印排序规则（投保顺序）为险种编码
            tSql = "select distinct ItemName,FileName,a.RiskCode, b.GrpProposalNo " + "from LDRiskPrint a, LCGrpPol b "
                    + "where a.RiskCode = b.RiskCode and b.GrpContNo = '" + this.mLCGrpContSchema.getGrpContNo()
                    + "' and ItemType = '0' order by a.RiskCode";
        }
        else
        {
            tSql = "select distinct ItemName,FileName,a.RiskCode, b.GrpProposalNo " + "from LDRiskPrint a, LCGrpPol b "
                    + "where a.RiskCode = b.RiskCode and b.GrpContNo = '" + this.mLCGrpContSchema.getGrpContNo()
                    + "' and ItemType = '0' order by b.GrpProposalNo";
        }
        SSRS tSSRS = new SSRS();
        SSRS tSSRS2 = new SSRS();
        tSSRS = tExeSQL.execSQL(tSql);
        System.out.println("tSSRS" + tSql);
        if (tSSRS.getMaxRow() > 0)
        {
            HashMap map = new HashMap();
            for (int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                if (map.get(tSSRS.GetText(i, 2)) == null)
                {
                    tXmlDataList.setColValue("PrintIndex", i);
                    tXmlDataList.setColValue("TermName", tSSRS.GetText(i, 1));
                    tXmlDataList.setColValue("FileName", "0");
                    tXmlDataList.setColValue("DocumentName", tSSRS.GetText(i, 2));
                    tXmlDataList.insertRow(0);

                    if (isFilialePrint)
                    {
                        map.put(tSSRS.GetText(i, 2), "1");
                    }
                }
                // 查询当前主条款下的责任条款信息
                tSql = "select distinct ItemName,FileName from LDRiskPrint where RiskCode in (select RiskCode from LCGrpPol where GrpContNo = '"
                        + this.mLCGrpContSchema.getGrpContNo()
                        + "') and ItemType = '1' and RiskCode in (select RiskCode from LDRiskPrint where RiskCode = '"
                        + tSSRS.GetText(i, 3) + "')";
                tSSRS2 = tExeSQL.execSQL(tSql);
                System.out.println("tSSRS2" + tSql);
                for (int j = 1; j <= tSSRS2.getMaxRow(); j++)
                {
                    tXmlDataList.setColValue("PrintIndex", i);
                    tXmlDataList.setColValue("TermName", tSSRS2.GetText(j, 1));
                    tXmlDataList.setColValue("FileName", "1");
                    tXmlDataList.setColValue("DocumentName", tSSRS2.GetText(j, 2));
                    tXmlDataList.insertRow(0);
                }
            }
        }
        else
        {
            // 查询当前主条款下的责任条款信息
            tSql = "select distinct ItemName,FileName, b.GrpProposalNo " + "from LDRiskPrint a, LCGrpPol b "
                    + "where a.RiskCode = b.RiskCode " + "   and b.GrpContNo = '" + mLCGrpContSchema.getGrpContNo()
                    + "' " + "  and ItemType = '1' " + "order by b.GrpProposalNo ";
            tSSRS2 = tExeSQL.execSQL(tSql);
            for (int j = 1; j <= tSSRS2.getMaxRow(); j++)
            {
                tXmlDataList.setColValue("PrintIndex", j);
                tXmlDataList.setColValue("TermName", tSSRS2.GetText(j, 1));
                tXmlDataList.setColValue("FileName", "1");
                tXmlDataList.setColValue("DocumentName", tSSRS2.GetText(j, 2));
                tXmlDataList.insertRow(0);
            }

        }
        cXmlDataset.addDataObject(tXmlDataList);
        return true;
    }

    /**
     * 正常打印流程
     * 
     * @param cLCGrpPolSet
     *            LCGrpPolSet
     * @return boolean
     * @throws Exception
     */
    private boolean getGrpPolDataSet(LCGrpPolSet cLCGrpPolSet) throws Exception
    {
        // xml对象
        XMLDataset tXMLDataset = mXMLDatasets.createDataset();

        // 查询个单合同表信息
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        // 根据合同号查询
        tLCGrpContDB.setGrpContNo(this.mLCGrpContSchema.getGrpContNo());
        // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLCGrpContDB.getInfo())
        {
            buildError("getInfo", "查询个单合同信息失败！");
            return false;
        }
        this.mLCGrpContSchema = tLCGrpContDB.getSchema();

        String tJFType = " select 1 from ldcode1 ld,lccontplandutyparam lc where ld.codetype='J105' "
        	           + " and ld.code=lc.riskcode and ld.code1=lc.calfactorvalue and lc.calfactor = 'InsuPlanCode' "
        	           + " and lc.grpcontno='"+this.mLCGrpContSchema.getGrpContNo()+"'";
        ExeSQL tJFTypeSQL = new ExeSQL();
        SSRS tSSRJFType = tJFTypeSQL.execSQL(tJFType);
        
        if(tSSRJFType.getMaxRow() > 0){
        	mJetFormType="J105";
        }
        
        //根据合同号在保障计划责任表中查询方案，如若是F12，则打印类型标记为J106 add by cz 2016-2-27
        String tInsuPlanCodeSQL = "select distinct calfactorvalue "
        						+ "from lccontplandutyparam "
        						+ "where grpcontno='"+this.mLCGrpContSchema.getGrpContNo()+"' "
        						+ "and calfactor='InsuPlanCode'";
        SSRS tSSRInsuType = new ExeSQL().execSQL(tInsuPlanCodeSQL);
        if( tSSRInsuType.getMaxRow()==1 && "F12".equals(tSSRInsuType.GetText(1, 1)) ){
        	mJetFormType="J106";
        }
        
        // 团单类型节点
        //        if ("86650105".equals(mLCGrpContSchema.getManageCom())) // 如果判断是新疆该机构保单，则采用新疆共保专用模版打印
        //        {
        //            tXMLDataset.addDataObject(new XMLDataTag("JetFormType", "J103"));
        //        }
        //        else if (jugdeUliByGrpContNo())
        tXMLDataset.addDataObject(new XMLDataTag("JetFormType", mJetFormType));
        tXMLDataset.addDataObject(new XMLDataTag("ManageComLength4", mLCGrpContSchema.getManageCom().substring(0, 4)));
        // 被保险人清单是否打印节点：0，不打印；1，打印
        tXMLDataset.addDataObject(new XMLDataTag("LCInsuredDetailFlag", printInsureDetail));

        // 团个单标志，1个单，2团单
        tXMLDataset.addDataObject(new XMLDataTag("ContType", "2"));
        if (jugdeUliByGrpContNo())
        {
            tXMLDataset.addDataObject(new XMLDataTag("FinishDate", "每个被保险人老年护理保险金领取起始日止"));
        }
        else
        {
            // 保险合同终止日期
            ExeSQL mExeSQL = new ExeSQL();
            SSRS mSSRS = new SSRS();
            String mSql = "select cinvalidate from LCGrpCont where GrpContNo = '"
                    + this.mLCGrpContSchema.getGrpContNo() + "'";

            mSSRS = mExeSQL.execSQL(mSql);

            if (mSSRS.GetText(1, 1) != null)
            {
                if ("9999".equals(mSSRS.GetText(1, 1).substring(0, 4)))
                {
                    tXMLDataset.addDataObject(new XMLDataTag("FinishDate", "终身"));
                }
                else
                {
                    String EndDate = getDate((new FDate()).getDate(mSSRS.GetText(1, 1)));
                    tXMLDataset.addDataObject(new XMLDataTag("FinishDate", EndDate));
                }
            }
        }

        // 缴费年期
        // 如果是团单终身重疾并且缴费方式不为趸交的时候显示缴费年期
        String sSql = "select max(payyears) from LCPol where GrpContNo = '" + this.mLCGrpContSchema.getGrpContNo()
                + "' and riskcode in(select code from ldcode where " + " codetype = 'payyears') and payintv != 0";

        String PayYears = new ExeSQL().getOneValue(sSql);

        if (!"".equals(PayYears) && !"null".equals(PayYears) && !"1".equals(PayYears))
        {
            tXMLDataset.addDataObject(new XMLDataTag("PayYears", "缴费年期:" + PayYears + "年"));
        }

        // 获取打印类型，团险无法按险种划分打印类型，故此统一使用000000代替
        LMRiskFormDB tLMRiskFormDB = new LMRiskFormDB();
        tLMRiskFormDB.setRiskCode("000000");
        tLMRiskFormDB.setFormType("PG");
        if (!tLMRiskFormDB.getInfo())
        {
            // 只有当查询出错的时候才报错，如果是空记录无所谓
            if (tLMRiskFormDB.mErrors.needDealError())
            {
                buildError("getInfo", "查询打印模板信息失败！");
                return false;
            }
            // 团个单标志，1个单，2团单
            tXMLDataset.setContType("2");
            // 默认填入空
            tXMLDataset.setTemplate("");
        }
        else
        {
            // 团个单标志，1个单，2团单
            tXMLDataset.setContType("2");
            // 打印模板信息
            tXMLDataset.setTemplate(tLMRiskFormDB.getFormName());
        }

        if (StrTool.cTrim(mLCGrpContSchema.getCardFlag()).equals("0"))
        {
            mXMLDatasets.setDTDName(this.mLCGrpContSchema.getGrpContNo() + ".dtd");
        }
        
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(this.mLCGrpContSchema.getAgentCode());
        // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLAAgentDB.getInfo())
        {
            buildError("getInfo", "查询销售人员信息失败！");
            return false;
        }
        this.mLAAgentSchema = tLAAgentDB.getSchema();
        String groupagent=mLAAgentSchema.getGroupAgentCode();
        String agent=mLAAgentSchema.getAgentCode();
        mLCGrpContSchema.setAgentCode(groupagent);
        mLAAgentSchema.setGroupAgentCode(agent);
        mLAAgentSchema.setAgentCode(groupagent);
        
        // 将查询出的schema信息放入到xml对象中
        tXMLDataset.addSchema(this.mLCGrpContSchema);
        mLCGrpContSchema.setAgentCode(agent);
        // 如果是卡单型险种需要查询抵达国家
        if (!StrTool.cTrim(mLCGrpContSchema.getCardFlag()).equals(""))
        {
            LCNationDB tLCNationDB = new LCNationDB();
            tLCNationDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            LCNationSet tLCNationSet = tLCNationDB.query();
            String Nation = "";
            for (int i = 1; i <= tLCNationSet.size(); i++)
            {
                Nation += tLCNationSet.get(i).getEnglishName();
                if (i != tLCNationSet.size())
                {
                    Nation += ",";
                }
            }
            tXMLDataset.addDataObject(new XMLDataTag("Nation", Nation));
            LDGrpDB tLDGrpDB = new LDGrpDB();
            tLDGrpDB.setCustomerNo(mLCGrpContSchema.getAppntNo());
            tLDGrpDB.getInfo();
            tXMLDataset.addDataObject(new XMLDataTag("GrpEnglithName", tLDGrpDB.getGrpEnglishName()));
        }

        // 查询投保人信息
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        // 根据合同号查询
        tLCGrpAppntDB.setGrpContNo(tLCGrpContDB.getGrpContNo());
        // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLCGrpAppntDB.getInfo())
        {
            buildError("getInfo", "查询个单投保人信息失败！");
            return false;
        }
        this.mLCGrpAppntSchema = tLCGrpAppntDB.getSchema();
        // 将查询出的schema信息放入到xml对象中
        tXMLDataset.addSchema(this.mLCGrpAppntSchema);

        // 查询投保人的地址信息
        LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
        // 根据投保人编码和地址编码查询
        tLCGrpAddressDB.setCustomerNo(tLCGrpAppntDB.getCustomerNo());
        tLCGrpAddressDB.setAddressNo(tLCGrpAppntDB.getAddressNo());
        // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLCGrpAddressDB.getInfo())
        {
            buildError("getInfo", "查询个单投保人地址信息失败！");
            return false;
        }
        this.mLCGrpAddressSchema = tLCGrpAddressDB.getSchema();
        // 将查询出的schema信息放入到xml对象中
        tXMLDataset.addSchema(this.mLCGrpAddressSchema);

        // 查询管理机构信息
        LDComDB tLDComDB = new LDComDB();
        // 根据管理机构查询，管理机构的选取可根据个保险公司不同而不同
        tLDComDB.setComCode(tLCGrpContDB.getManageCom());
        // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLDComDB.getInfo())
        {
            buildError("getInfo", "查询管理机构信息失败！");
            return false;
        }
        this.mLDComSchema = tLDComDB.getSchema();
        // 将查询出的schema信息放入到xml对象中
        tXMLDataset.addSchema(this.mLDComSchema);

        
        // 将查询出的schema信息放入到xml对象中
        tXMLDataset.addSchema(this.mLAAgentSchema);

        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(this.mLCGrpContSchema.getAgentGroup());
        // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
        if (!tLABranchGroupDB.getInfo())
        {
            buildError("getInfo", "查询销售机构信息失败！");
            return false;
        }
        this.mLABranchGroupSchema = tLABranchGroupDB.getSchema();
        // 将查询出的schema信息放入到xml对象中
        tXMLDataset.addSchema(this.mLABranchGroupSchema);
        
//      by gzh 20120521
        //增加中介机构及中介机构销售人员信息
        String tAgentCom = this.mLCGrpContSchema.getAgentCom();
        if(tAgentCom != null && !"".equals(tAgentCom)){
        	LAComDB tLAComDB = new LAComDB();
            tLAComDB.setAgentCom(tAgentCom);
//          如果查询失败，无论是查询为空，还是真正的查询失败，则返回
            if (!tLAComDB.getInfo())
            {
            	buildError("getInfo", "查询中介机构信息失败！");
                return false;
            }
            this.mLAComSchema = tLAComDB.getSchema();
        }
//      将查询出的schema信息放入到xml对象中
        tXMLDataset.addSchema(this.mLAComSchema);
        
        String tAgentSaleCode = this.mLCGrpContSchema.getAgentSaleCode();
        if(tAgentSaleCode != null && !"".equals(tAgentSaleCode)){
        	LAAgenttempDB tLAAgenttempDB = new LAAgenttempDB();
            tLAAgenttempDB.setAgentCode(tAgentSaleCode);
            // 如果查询失败，无论是查询为空，还是真正的查询失败，则返回
            if (!tLAAgenttempDB.getInfo())
            {
                buildError("getInfo", "查询中介机构代理业务员信息失败！");
                return false;
            }
            this.mLAAgenttempSchema = tLAAgenttempDB.getSchema();
        }
        // 将查询出的schema信息放入到xml对象中
        tXMLDataset.addSchema(this.mLAAgenttempSchema);
        // by gzh end 20120521

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String tSql = "select cvalidate,cinvalidate from LCGrpCont where GrpContNo = '"
                + this.mLCGrpContSchema.getGrpContNo() + "'";

        tSSRS = tExeSQL.execSQL(tSql);

        tXMLDataset.addDataObject(new XMLDataTag("BeginDate", tSSRS.GetText(1, 1)));
        tXMLDataset.addDataObject(new XMLDataTag("EndDate", tSSRS.GetText(1, 2)));

        // 暂交费收据号
        tSql = "select distinct TempFeeNo from LJTempFee where OtherNo = '" + this.mLCGrpContSchema.getGrpContNo()
                + "' and OtherNoType = '7'";
        tSSRS = tExeSQL.execSQL(tSql);
        if (!mLCGrpContSchema.getAppFlag().equals("9"))
        {
            tXMLDataset.addDataObject(new XMLDataTag("TempFeeNo", tSSRS.GetText(1, 1))); // 收据号
        }
        else
        {
            tXMLDataset.addDataObject(new XMLDataTag("TempFeeNo", "")); // 收据号
        }

        // 总保费
        tSql = "select prem from LCGrpCont where GrpContNo = '" + this.mLCGrpContSchema.getGrpContNo() + "'";
        tSSRS = tExeSQL.execSQL(tSql);
        tXMLDataset.addDataObject(new XMLDataTag("SumPrem", cancelKeXueCount(format(Double.parseDouble(tSSRS.GetText(1,
                1))))));

        String cDate = PubFun.getCurrentDate().split("-")[0] + "年" + PubFun.getCurrentDate().split("-")[1] + "月"
                + PubFun.getCurrentDate().split("-")[2] + "日";
        tXMLDataset.addDataObject(new XMLDataTag("Today", cDate));

        // 增加：保单打印时间/签单时间/收费时间
        String tTmpGrpContNo = mLCGrpContSchema.getGrpContNo();
        String tTmpPrtNo = mLCGrpContSchema.getPrtNo();

        String tContPrintDate = cDate;
        String tContPrintTime = PubFun.getCurrentTime();
        tContPrintTime = tContPrintTime.split(":")[0] + "时" + tContPrintTime.split(":")[1] + "分";
        tXMLDataset.addDataObject(new XMLDataTag("ContPrintDateTime", tContPrintDate + tContPrintTime));

        String tContSignDate = tExeSQL.getOneValue("select SignDate from LCGrpCont where GrpContNo = '" + tTmpGrpContNo
                + "'");
        if (tContSignDate != null && !tContSignDate.equals(""))
        {
            tContSignDate = tContSignDate.split("-")[0] + "年" + tContSignDate.split("-")[1] + "月"
                    + tContSignDate.split("-")[2] + "日";
            String tContSignTime = tExeSQL
                    .getOneValue("select replace(varchar(SignTime), '.', ':') from LCGrpCont where GrpContNo = '"
                            + tTmpGrpContNo + "'");
            if (tContSignTime == null || tContSignTime.equals(""))
                tContSignTime = PubFun.getCurrentTime();
            tContSignTime = tContSignTime.split(":")[0] + "时" + tContSignTime.split(":")[1] + "分";
            tXMLDataset.addDataObject(new XMLDataTag("ContSignDateTime", tContSignDate + tContSignTime));
        }
        else
        {
            tXMLDataset.addDataObject(new XMLDataTag("ContSignDateTime", "----"));
        }

        String tPayConfDate = tExeSQL.getOneValue("select max(ConfMakeDate) from LJTempFee where (OtherNo = '"
                + tTmpGrpContNo + "' or OtherNo = '" + tTmpPrtNo + "') ");
        if (tPayConfDate != null && !tPayConfDate.equals(""))
        {
            tPayConfDate = tPayConfDate.split("-")[0] + "年" + tPayConfDate.split("-")[1] + "月"
                    + tPayConfDate.split("-")[2] + "日";
            String tPayConfTime = tExeSQL
                    .getOneValue("select replace(varchar(max(confMakeTime)), '.', ':') from LJTempFee where (OtherNo = '"
                            + tTmpGrpContNo
                            + "' or OtherNo = '"
                            + tTmpPrtNo
                            + "') and ConfMakeDate = (select max(ConfMakeDate) from LJTempFee where (OtherNo = '"
                            + tTmpGrpContNo + "'or OtherNo = '" + tTmpPrtNo + "'))");
            if (tPayConfTime == null || tPayConfTime.equals(""))
            {
                tPayConfTime = tExeSQL
                        .getOneValue("select replace(varchar(max(MakeTime)), '.', ':') from LJTempFee where (OtherNo = '"
                                + tTmpGrpContNo
                                + "' or OtherNo = '"
                                + tTmpPrtNo
                                + "') and ConfMakeDate = (select max(ConfMakeDate) from LJTempFee where (OtherNo = '"
                                + tTmpGrpContNo + "'or OtherNo = '" + tTmpPrtNo + "'))");
            }
            tPayConfTime = tPayConfTime.split(":")[0] + "时" + tPayConfTime.split(":")[1] + "分";
            tXMLDataset.addDataObject(new XMLDataTag("PayConfDateTime", tPayConfDate + tPayConfTime));
        }
        else
        {
            tXMLDataset.addDataObject(new XMLDataTag("PayConfDateTime", "----"));
        }
        // --------------------

        // 操作人员编码和操作人员姓名
        tSql = "select UserCode,UserName from LDUser where UserCode in (Select Operator from LJTempFee where OtherNo = '"
                + this.mLCGrpContSchema.getGrpContNo() + "' and OtherNoType = '7')";
        tSSRS = tExeSQL.execSQL(tSql);
        if (!((String) mLCGrpContSchema.getAppFlag()).equals("9"))
        {
            tXMLDataset.addDataObject(new XMLDataTag("OperatorCode", tSSRS.GetText(1, 1)));
            tXMLDataset.addDataObject(new XMLDataTag("Operator", tSSRS.GetText(1, 2)));
        }
        else
        {
            tXMLDataset.addDataObject(new XMLDataTag("OperatorCode", mGlobalInput.Operator));
            String tempSql = "select UserName from lduser where usercode='" + mGlobalInput.Operator + "'";
            tSSRS = tExeSQL.execSQL(tempSql);
            tXMLDataset.addDataObject(new XMLDataTag("Operator", tSSRS.GetText(1, 1)));

        }
        // 产生团单下明细信息（责任信息、特约信息、个人险种明细、险种条款信息等）
        if (!genInsuredList(tXMLDataset))
        {
            return false;
        }

        // 获取特约信息
        if (!getSpecDoc(tXMLDataset))
        {
            return false;
        }

        // 获取条款信息
        if (!getTerm(tXMLDataset, cLCGrpPolSet))
        {
            return false;
        }

        // 获取协议影印件信息,以后打印协议时用
        // if (!getScanDoc(tXMLDataset))
        // {
        // return false;
        // }

        // 产生团单下被保人清单
        if (!genInsuredDetail(tXMLDataset))
        {
            return false;
        }

        // 获取保单影印件信息
        // 下面4行信息注掉，用于以后的补充协议打印
        if (!getScanPic(tXMLDataset))
        {
            return false;
        }
        // 获取被保险人急救医疗卡信息
        if (!StrTool.cTrim(this.mLCGrpContSchema.getCardFlag()).equals(""))
        {
            if (!genInsuredCard(tXMLDataset))
            {
                return false;
            }
        }
        
        
        // 获取定点医院信息
        // if (!getHospital(tXMLDataset))
        // {
        // return false;
        // }
        // System.out.println("定点医院明细准备完毕。。。");
        
        
        //获取收款凭证
        if (!getReceiptNote(tXMLDataset)){
        	return false;
        }
        
        
        String tttSql = "select 1 from loprtmanager2 "
        			  + "where code='35' and otherno in ( "
        			  + "select payno from ljapay "
        			  + "where incomeno='"+ this.mLCGrpContSchema.getGrpContNo() + "') ";
        SSRS tttSSRS = new ExeSQL().execSQL(tttSql);
        String tttsql1 = "select 1 from LYOutPayDetail "
        		       + "where insureno='"+ this.mLCGrpContSchema.getPrtNo() +"' and billprintdate is not null ";
        SSRS tttSSRS1 = new ExeSQL().execSQL(tttsql1);
        String tttsql2 = "select 1 from ljapay "
        			   + "where incomeno='"+this.mLCGrpContSchema.getGrpContNo() + "' ";
        SSRS tttSSRS2 = new ExeSQL().execSQL(tttsql2);
        if(tttSSRS.getMaxRow()>0||tttSSRS1.getMaxRow()>0||tttSSRS2.getMaxRow()==0){
        	tXMLDataset.addDataObject(new XMLDataTag("ReceiptNoteFlag", "0"));
        }else{
        	
        	//保费收据字段标识 0：不打收据 1：打收据
        	tXMLDataset.addDataObject(new XMLDataTag("ReceiptNoteFlag", "1"));
        	
        	 //保单号
        	tXMLDataset.addDataObject(new XMLDataTag("ReceiptPolicyNo", this.mLCGrpContSchema.getGrpContNo()));
            
        	//实收号
        	String tStrSql0= "select payno from ljapay "
        				   + "where incomeno='"+this.mLCGrpContSchema.getGrpContNo() + "' ";
        	SSRS tSSRS0 = new ExeSQL().execSQL(tStrSql0);
        	if(tSSRS0.getMaxRow()>0){
        		tXMLDataset.addDataObject(new XMLDataTag("ReceiptPayNo",  tSSRS0.GetText(1,1)));

        	}
        	
            //保单收费时间
        	 String tStrSql1= "select min(ConfMakeDate) from LJTempFee where (OtherNo = '"
      			   +this.mLCGrpContSchema.getGrpContNo() +"' or OtherNo = '" +this.mLCGrpContSchema.getPrtNo() + "') ";       
            String payenddate=tExeSQL.getOneValue(tStrSql1);
            if (payenddate != null && !payenddate.equals("")){
            	payenddate = payenddate.split("-")[0] + "年" + payenddate.split("-")[1] + "月"
            			+ payenddate.split("-")[2] + "日";
            	tXMLDataset.addDataObject(new XMLDataTag("ReceiptPayTime", payenddate));
            }
            
            
            //投保单位/投保人
            String tStrSql2= "select name from lcgrpappnt "
            			   + "where grpcontno='"+this.mLCGrpContSchema.getGrpContNo() + "' ";
            SSRS tSSRS2 = new ExeSQL().execSQL(tStrSql2);
            if(tSSRS2.getMaxRow()>0){
            	tXMLDataset.addDataObject(new XMLDataTag("ReceiptAppName",  tSSRS2.GetText(1,1)));
            }
            
            //交费方式
            String tStrSql3= "select codename from ldcode "
            			   + "where codetype='paymode' and code in ("
            			   + "select paymode from lcgrpcont "
            			   + "where grpcontno='"+this.mLCGrpContSchema.getGrpContNo() + "')";
            SSRS tSSRS3 = new ExeSQL().execSQL(tStrSql3);
            if(tSSRS3.getMaxRow()>0){
            	tXMLDataset.addDataObject(new XMLDataTag("ReceiptWayToPay",  tSSRS3.GetText(1,1)));
            }	
            
            //总保费金额 人民币大小写
            String tStrSql4= "select sumactupaymoney from ljapay "
            			   + "where incomeno='"+this.mLCGrpContSchema.getGrpContNo() + "' and duefeetype='0'";
            String a=tExeSQL.getOneValue(tStrSql4);    
            if(a != null && !a.equals("")){
            	double p=Double.parseDouble(a);
            	tXMLDataset.addDataObject(new XMLDataTag("ReceiptTotalPrem",  new java.text.DecimalFormat("#.00").format(p)));		
            	tXMLDataset.addDataObject(new XMLDataTag("ReceiptTotalPremC",  PubFun.getChnMoney(p)));		
            }	
            
            //收款方名称和地址
            String tStrSql5= "select name,address from ldcom "
            			   + "where comcode in ( "
            			   + "select managecom from lcgrpcont "
            			   + "where grpcontno='"+this.mLCGrpContSchema.getGrpContNo() + "')";
            SSRS tSSRS5 = new ExeSQL().execSQL(tStrSql5);
            if(tSSRS5.getMaxRow()>0){
            	tXMLDataset.addDataObject(new XMLDataTag("ReceiptReceivingName",  tSSRS5.GetText(1,1)));
            	tXMLDataset.addDataObject(new XMLDataTag("ReceiptReceivingAdd",  tSSRS5.GetText(1,2)));
            }
            
            //获取险种名称以及险种保费
            if(!getPremDetail(tXMLDataset)){
            	return false;
            }
        }
        
        
        
        // 获取中文保费的信息
        if (!getMoney(tXMLDataset))
        {
            return false;
        }
        // 获取发票信息
        if (!this.mLCGrpContSchema.getAppFlag().equals("9"))
        {
            if (!getInvoice(tXMLDataset))
            {
                return false;
            }
        }

        // 生成团体保险期间
        if (!getGrpInsuYear(tXMLDataset))
        {
            return false;
        }
        //如果为真，则执行新增的这三个方法，反之则不执行
        if (jugdeUliByGrpContNo())
        {
            if (!getGrpNewCont(tXMLDataset))
            {
                return false;
            }
            if (!getFCRiskZTFee(tXMLDataset))
            {
                return false;
            }
            // 生成归属规则：
            if (!getGrpascriptionrule(tXMLDataset))
            {
                return false;
            }
        }

        //		 生成归属规则：
        if (!CreateEndDate(tXMLDataset))
        {
            return false;
        }
        return true;
    }

    /**
     * getGrpInsuYear
     * 
     * @return boolean
     */
    private boolean getGrpInsuYear(XMLDataset tXmlDataset)
    {
        String start = this.mLCGrpContSchema.getCValiDate();
        String end = this.mLCGrpContSchema.getCInValiDate();
        StringBuffer sql = new StringBuffer(255);
        sql.append("select (date('").append(end).append("') + 1 day - date('").append(start).append(
                "'))/10000 from dual");
        String insuYear = (new ExeSQL()).getOneValue(sql.toString());
        if (insuYear != null && !insuYear.equals(""))
        {
            double year = PubFun.setPrecision(Double.parseDouble(insuYear), "0.00");
            if (year > 0)
            {
                tXmlDataset.addDataObject(new XMLDataTag("GrpInsuYear", String.valueOf(year)));
            }
        }
        return true;
    }

    /**
     * 根据个单合同号，查询明细信息
     * 
     * @param tXmlDataset
     *            XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean getHospital(XMLDataset tXmlDataset) throws Exception
    {
        // 数据流
        // InnerFormater tInnerFormater = null;
        String tTemplateFile = "";
        // 团险默认配置文件，mszTemplatePath为模板所在的应用路径
        tTemplateFile = mTemplatePath + "Hospital.xml";
        // 校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists())
        {
            buildError("getHospital", "XML配置文件不存在！");
            return false;
        }
        try
        {
            Hashtable tHshData = new Hashtable();
            // 将变量_ContNo的值赋给xml文件
            // tHshData.put("_AREACODE",
            // this.mLDComSchema.getRegionalismCode());
            tHshData.put("_AREACODE", "11%%");
            // 根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(tTemplateFile), tHshData);
            // tXmlDataMine.setDataFormater(tInnerFormater);
            tXmlDataset.addDataObject(tXmlDataMine);
        }
        catch (Exception e)
        {
            // 出错处理
            buildError("genHospital", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     * 根据团单合同号，查询明细信息 责任信息、特约信息、个人险种明细、险种条款信息等
     * 
     * @param cXmlDataset
     *            XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean genInsuredList(XMLDataset cXmlDataset) throws Exception
    {
        // String tSql =
        // "select ContPlanCode,ContPlanName from LCContPlan where GrpContNo =
        // '" +
        // this.mLCGrpContSchema.getGrpContNo() +
        // "' and ContPlanCode != '00'";

        StringBuffer tSBql2 = new StringBuffer(256);
        tSBql2.append("select 1 from lcgrppol where GrpContNo = '");
        tSBql2.append(this.mLCGrpContSchema.getGrpContNo());
        tSBql2.append("' and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G')");
        System.out.println("查询险种的啊啊啊啊啊啊啊" + tSBql2.toString());

        ExeSQL tExeSQL5 = new ExeSQL();
        SSRS tSSRS5 = tExeSQL5.execSQL(tSBql2.toString());
        System.out.println("查询险种" + tExeSQL5.toString());
        if (tSSRS5.getMaxRow() < 1)
        {
            StringBuffer tSBql = new StringBuffer(256);
            tSBql.append("select ContPlanCode,ContPlanName from LCContPlan where GrpContNo = '");
            tSBql.append(this.mLCGrpContSchema.getGrpContNo());
            tSBql.append("' and ContPlanCode != '00' and ContPlanCode != '11'");
            tSBql.append(" order by ContPlanCode ");

            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(tSBql.toString());

            StringBuffer tContPlanAabstractInfo = new StringBuffer();
            // 循环获取计划信息
            for (int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                cXmlDataset.addDataObject(new XMLDataTag(tSSRS.GetText(i, 1), tSSRS.GetText(i, 2)));

                // 保障计划概述
                tContPlanAabstractInfo.append("计划 ");
                tContPlanAabstractInfo.append(tSSRS.GetText(i, 1));
                tContPlanAabstractInfo.append(".");
                tContPlanAabstractInfo.append(tSSRS.GetText(i, 2));
                tContPlanAabstractInfo.append(" ");
                // ----------------------------------
            }

            // 保障计划概述节点：<ContPlanAabstractInfo />
            cXmlDataset.addDataObject(new XMLDataTag("ContPlanAabstractInfo", tContPlanAabstractInfo.toString()));
            // --------------------------------------

            // String flag="1";
//            XMLDataList xmlDataList = new XMLDataList();
//            // 添加xml一个新的对象Term
//            //update here
//            xmlDataList.setDataObjectID("LCPol");
//           
            
            Element tElmPol = new Element("LCPol");
            StringBuffer tPlanCodeSQL = new StringBuffer();
            //获取保障计划，参保人数
            tPlanCodeSQL.append(" select contplancode,peoples3  from lccontplan where " +
            		"	grpcontno='"+this.mLCGrpContSchema.getGrpContNo()+"' and contplancode<>'11' with ur ");

            ExeSQL tExePlanSQL = new ExeSQL();
            SSRS tSSRSPlan = tExePlanSQL.execSQL(tPlanCodeSQL.toString());
            if (tSSRSPlan.getMaxRow() > 0)
              {
            	 for (int j = 1; j <= tSSRSPlan.getMaxRow(); j++)
                 {
            		 Element tRow = new Element("Row");
            		 String tContPlanCode = tSSRSPlan.GetText(j, 1);
            		 String tRiskCode = null ;
                     String tRiskName = null ;
                     String tInsuArea = null ;
                     String tYearRiskAmnt = null ;
                     String tSpecialGetRate = null ;
                     String tPrem = null ;
                     String tInsuPlanCode = null;
                     boolean isType = false;
            		 //获取险种代码，年度总限额，保险费
            		 StringBuffer tPlanRiskSQL = new StringBuffer();
            		 tPlanRiskSQL.append(" select riskcode,riskamnt,riskprem  from lccontplanrisk where " +
                     		"	grpcontno='"+this.mLCGrpContSchema.getGrpContNo()+"' " +
                     				"and contplancode='"+tContPlanCode+"' with ur ");
                     ExeSQL tEPlanRiskSQL = new ExeSQL();
                     SSRS tSSRSPlanR = tEPlanRiskSQL.execSQL(tPlanRiskSQL.toString());
                     if(tSSRSPlanR.getMaxRow() > 0){
                    	 
                    	 tRiskCode	= tSSRSPlanR.GetText(1, 1);
                    	 tYearRiskAmnt = tSSRSPlanR.GetText(1, 2);
                    	 tPrem = tSSRSPlanR.GetText(1, 3);
                    	 
                     }
//                     by gzh 20121101 对于表定费率，保费根据产品计算得出，不在保障计划表，年度限额也取产品描述
//                     因此对tYearRiskAmnt和tPrem重置
                     String tSQL= " select 1 from LCContPlanDutyParam where grpcontno='"
         				+ this.mLCGrpContSchema.getGrpContNo() + "' and contplancode='11' and riskcode='162001' " 
                     	+ " and calfactor = 'CalRule' and calfactorvalue = '0'" ;
                     String tJXQQB = new ExeSQL().getOneValue(tSQL);
                     if("1".equals(tJXQQB)){
                    	 StringBuffer tYearAmntSQL = new StringBuffer();
                    	 tYearAmntSQL.append(" select distinct calfactorvalue  from lccontplandutyparam " +
                         			"	where grpcontno = '"+this.mLCGrpContSchema.getGrpContNo()+"' and " +
                         					" riskcode = '"+tRiskCode+"' " +
                         					" and contplancode = '"+tContPlanCode+"' " +
                         					"and calfactor = 'YearAmnt' with ur ");
                         ExeSQL tEYearAmntSQL = new ExeSQL();
                         SSRS tSSRYearAmnt = tEYearAmntSQL.execSQL(tYearAmntSQL.toString());
                         if(tSSRYearAmnt.getMaxRow() > 0){
                        	 
                        	 tYearRiskAmnt = tSSRYearAmnt.GetText(1, 1);
                                            	 
                         }
                         StringBuffer tPremSQL = new StringBuffer();
                         tPremSQL.append(" select sum(prem)  from lcpol " +
                         			"	where grpcontno = '"+this.mLCGrpContSchema.getGrpContNo()+"' and " +
                         					" riskcode = '"+tRiskCode+"' " +
                         					" and contplancode = '"+tContPlanCode+"' " );
                         ExeSQL tEPremSQL = new ExeSQL();
                         SSRS tSSRPrem = tEPremSQL.execSQL(tPremSQL.toString());
                         if(tSSRPrem.getMaxRow() > 0){
                        	 
                        	 tPrem = tSSRPrem.GetText(1, 1);
                                            	 
                         }
                     }
                     
                     //获取险种名称
                     StringBuffer tRiskNameSQL = new StringBuffer();
                     tRiskNameSQL.append(" select riskname  from lmriskapp " +
                     			"	where  riskcode ='"+tRiskCode+"' with ur ");
                     ExeSQL tERiskNameSQL = new ExeSQL();
                     SSRS tSSRRiskName = tERiskNameSQL.execSQL(tRiskNameSQL.toString());
                     if(tSSRRiskName.getMaxRow() > 0){
                    	 
                    	 tRiskName = tSSRRiskName.GetText(1, 1);
                                        	 
                     }
                   //获取保障地区
                     StringBuffer tInsuAreaSQL = new StringBuffer();
                     tInsuAreaSQL.append(" select distinct calfactorvalue  from lccontplandutyparam " +
                     			"	where grpcontno = '"+this.mLCGrpContSchema.getGrpContNo()+"' and " +
                     					" riskcode = '"+tRiskCode+"' " +
                     					" and contplancode = '"+tContPlanCode+"' " +
                     					"and calfactor = 'InsuArea' with ur ");
                     ExeSQL tEInsuAreaSQL = new ExeSQL();
                     SSRS tSSRInsuArea = tEInsuAreaSQL.execSQL(tInsuAreaSQL.toString());
                     
                     StringBuffer tInsuPlanCodeSQL = new StringBuffer();
                     tInsuPlanCodeSQL.append(" select distinct calfactorvalue  from lccontplandutyparam " +
                     			"	where grpcontno = '"+this.mLCGrpContSchema.getGrpContNo()+"' and " +
                     					" riskcode = '"+tRiskCode+"' " +
                     					" and contplancode = '"+tContPlanCode+"' " +
                     					"and calfactor = 'InsuPlanCode' with ur ");
                     ExeSQL tEInsuPlanCodeSQL = new ExeSQL();
                     SSRS tSSRInsuPlanCode = tEInsuPlanCodeSQL.execSQL(tInsuPlanCodeSQL.toString());
                     
                     if(tSSRInsuPlanCode.getMaxRow() > 0){
                    	 tInsuPlanCode=tSSRInsuPlanCode.GetText(1, 1);
                    	 tSSRInsuPlanCode = tEInsuPlanCodeSQL.execSQL("select 1 from ldcode1 where codetype='J105' and code='"+tRiskCode+"' and code1='"+tInsuPlanCode+"'");
                    	 if(tSSRInsuPlanCode.getMaxRow() > 0){
                    		 isType = true;
                    	 }                   	 
                     }
                     
                     //获取特定医院给付比例
                     StringBuffer tSGRSQL = new StringBuffer();
                     tSGRSQL.append(" select distinct calfactorvalue  from lccontplandutyparam " +
                     			"	where grpcontno = '"+this.mLCGrpContSchema.getGrpContNo()+"' and " +
                     					" riskcode = '"+tRiskCode+"' " +
                     					" and contplancode = '"+tContPlanCode+"' " +
                     					"and calfactor = 'SpecialGetRate' with ur ");
                     ExeSQL tESGRSQL = new ExeSQL();
                     SSRS tSSRSGR = tESGRSQL.execSQL(tSGRSQL.toString());
                     if(tSSRInsuArea.getMaxRow() > 0){
                    	 
                    	 tInsuArea = tSSRInsuArea.GetText(1, 1);
                                        	 
                     }
                     if(tSSRSGR.getMaxRow() > 0){
                    	 
                    	 tSpecialGetRate = tSSRSGR.GetText(1, 1);
                                        	 
                     }
                     
                     //保障计划层级显示部分
            		 Element tElmPlanCode = new Element("ContPlanCode");
                     tElmPlanCode.setText(tContPlanCode);
                     Element tElmInCount = new Element("InsuredCount");
                     tElmInCount.setText(tSSRSPlan.GetText(j, 2));
                     Element tElmRiskCode = new Element("RiskCode");
                     tElmRiskCode.setText(tRiskCode);
                     Element tElmRiskName = new Element("RiskName");
                     tElmRiskName.setText(tRiskName);
                     Element tElmInsuArea = new Element("InsuArea");
                     tElmInsuArea.setText(tInsuArea);
                     Element tElmYAmnt = new Element("YearRiskAmnt");
                     tElmYAmnt.setText(tYearRiskAmnt);
                     Element tElmSGRate = new Element("SpecialGetRate");
                     tElmSGRate.setText(tSpecialGetRate);
                     Element tElmPrem = new Element("Prem");
                     tElmPrem.setText(tPrem);
                     tRow.addContent(tElmPlanCode);
                     tRow.addContent(tElmInCount);
                     tRow.addContent(tElmRiskCode);
                     tRow.addContent(tElmRiskName);
                     tRow.addContent(tElmInsuArea);
                     tRow.addContent(tElmYAmnt);
                     if(!isType){
                    	 tRow.addContent(tElmSGRate);
                     }                      
                     tRow.addContent(tElmPrem);
                     
                     //F12方案需要定义新的打印模板，在此区分。 add by zc 2016-2-22
                     if(tInsuPlanCode!=null && "F12".equals(tInsuPlanCode)){
                    	 
                    	 //住院保障显示部分	HBFund	676009
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676009")){
                        	 Element tElmHBFund = addHBFundF12(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676009",tInsuPlanCode);
                        	 tRow.addContent(tElmHBFund);
                         }
                         
                         //门急诊保障显示部分	OECFund	676010
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676010")){
                        	 Element tElmOECFund = addOECFundF12(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676010",tInsuPlanCode);
                        	 tRow.addContent(tElmOECFund);
                         }
                         
                         //紧急医疗运送显示部分 	UMEFund	676011
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676011")){
                        	 Element tElmUMEFund = addUMEFundF12(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676011",tInsuPlanCode);
                        	 tRow.addContent(tElmUMEFund);
                         }
                         
                         //牙科保障显示部分	DCFund	676012
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676012")){
                        	 Element tDCFund = addDCFundF12(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676012",tInsuPlanCode);
                        	 tRow.addContent(tDCFund);
                         }
                         
                         //生育保障显示部分	MCFund	676014
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676014")){
                        	 Element tElmMCFund = addMCFundF12(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676014",tInsuPlanCode);
                        	 tRow.addContent(tElmMCFund);
                         }
                         
                         //特殊责任保障显示部分      SDFund  676015
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676015")){
                        	 Element tElmSDFund = addSDFundF12(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676015",tInsuPlanCode);
                        	 tRow.addContent(tElmSDFund);
                         }
                         
                         //中医治疗显示部分      DTFund  676016
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676016")){
                        	 Element tElmDTFund = addDTFundF12(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676016",tInsuPlanCode);
                        	 tRow.addContent(tElmDTFund);
                         }
                         
                     }else{//除了F12方案，原有的方案的打印模板
                    	//住院医疗保险金显示部分	HMIFund	676001
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676001")){
                        	 Element tElmHMIFund = addHMIFund(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676001");
                        	 tRow.addContent(tElmHMIFund);
                         }
                         
                         //门急诊医疗保险金显示部分	OEMFund	676002
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676002")){
                        	 Element tElmOEMFund = addOEMFund(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676002");
                        	 tRow.addContent(tElmOEMFund);
                         }
                         
                         //生育保险金显示部分		MIFund	676003
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676003")){
                        	 Element tElmMIFund = addMIFund(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676003");
                        	 tRow.addContent(tElmMIFund);
                         }
                         
                         //牙科医疗保险显示部分	DentalInsur	676004
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676004")){
                        	 Element tDentalInsur = addDentalInsur(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676004");
                        	 tRow.addContent(tDentalInsur);
                         }
                         
                         //中医治疗保险金显示部分	CMTFund	676005
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676005")){
                        	 Element tElmCMTFund = addCMTFund(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676005");
                        	 tRow.addContent(tElmCMTFund);
                         }
                         
                         //特殊责任显示部分 		SDuty	676006
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676006")){
                        	 Element tElmSDuty = addSDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676006");
                        	 tRow.addContent(tElmSDuty);
                         }
                         
                         //紧急救援服务显示部分	EServices	676007
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676007")){
                        	 Element tElmEServices = addEServices(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676007");
                        	 tRow.addContent(tElmEServices);
                         }
                         
                         //健康管理服务显示部分	HMService	676008
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676008")){
                        	 Element tElmHMService = addHMService(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676008");
                        	 tRow.addContent(tElmHMService);
                         }
                         
                         //住院保障显示部分	HBFund	676009
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676009")){
                        	 Element tElmHBFund = addHBFund(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676009",tInsuPlanCode);
                        	 tRow.addContent(tElmHBFund);
                         }
                         
                         //门急诊保障显示部分	OECFund	676010
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676010")){
                        	 Element tElmOECFund = addOECFund(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676010",tInsuPlanCode);
                        	 tRow.addContent(tElmOECFund);
                         }
                         
                         //生育保障显示部分		MCFund	676014
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676014")){
                        	 Element tElmMCFund = addMCFund(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676014",tInsuPlanCode);
                        	 tRow.addContent(tElmMCFund);
                         }
                         
                         //牙科保障显示部分	DCFund	676012
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676012")){
                        	 Element tDCFund = addDCFund(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676012",tInsuPlanCode);
                        	 tRow.addContent(tDCFund);
                         }
                         
                         //体检或疫苗保障显示部分	RPEFund	676013
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676013")){
                        	 Element tElmRPEFund = addRPEFund(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676013",tInsuPlanCode);
                        	 tRow.addContent(tElmRPEFund);
                         }
                         
                         //紧急医疗运送显示部分 	UMEFund	676011
                         if(!checkDuty(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676011")){
                        	 Element tElmUMEFund = addUMEFund(this.mLCGrpContSchema.getGrpContNo(),tRiskCode,tContPlanCode,"676011",tInsuPlanCode);
                        	 tRow.addContent(tElmUMEFund);
                         }
                     }
                     
                     tElmPol.addContent(tRow);
                     
                  }
              }
 
            Element tElmRoot = cXmlDataset.getElementNoClone();
            tElmRoot.addContent(tElmPol);
            


            //xmlDataList.buildColHead();
            System.out.println("ContPrintType" + ContPrintType);

        }
        else
        {

        }
        return true;
    }

    private boolean checkDuty(String tGrpContNo, String tRiskCode,
			String tContPlanCode, String tDutyCode) {
    	StringBuffer tDutySQL = new StringBuffer();
    	tDutySQL.append(" select 1  from lccontplandutyparam " +
        			"	where grpcontno = '"+tGrpContNo+"' and " +
        					" riskcode = '"+tRiskCode+"' " +
        					" and contplancode = '"+tContPlanCode+"' " +
        					"and dutycode = '"+tDutyCode+"' with ur ");
        ExeSQL tEDutySQL = new ExeSQL();
        SSRS tSSRSDuty = tEDutySQL.execSQL(tDutySQL.toString());
        if(tSSRSDuty.getMaxRow() > 0){
       	 
        	return false;
                           	 
        }
		return true;
	}

	private Element addHMService(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode) {
    	String tParam = null;
		Element tElmDetail = new Element("HMService");
        Element tAmnt = new Element("HMMs");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"HMMs");
        tAmnt.setText(tParam);
       
        tElmDetail.addContent(tAmnt);
       
		return tElmDetail;
	}

	private Element addEServices(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode) {
		String tParam = null;
		Element tElmDetail = new Element("EServices");
        Element tAmnt = new Element("Amnt");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"Amnt");
        tAmnt.setText(tParam);
       
        tElmDetail.addContent(tAmnt);
       
		return tElmDetail;
	}

	private Element addSDuty(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode) {
		String tParam = null;
		Element tElmDetail = new Element("SDuty");
        Element tOrganTransFee = new Element("OrganTransFee");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"OrganTransFee");
        tOrganTransFee.setText(tParam);
        Element tOrganTransRate = new Element("OrganTransRate");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"OrganTransRate");
        tOrganTransRate.setText(tParam);
        Element tTouXiFee = new Element("TouXiFee");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"TouXiFee");
        tTouXiFee.setText(tParam);
        Element tTouXiRate = new Element("TouXiRate");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"TouXiRate");
        tTouXiRate.setText(tParam);
        Element tCTFee = new Element("CTFee");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"CTFee");
        tCTFee.setText(tParam);
        Element tCTRate = new Element("CTRate");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"CTRate");
        tCTRate.setText(tParam);
        
        tElmDetail.addContent(tOrganTransFee);
        tElmDetail.addContent(tOrganTransRate);
        tElmDetail.addContent(tTouXiFee);
        tElmDetail.addContent(tTouXiRate);
        tElmDetail.addContent(tCTFee);
        tElmDetail.addContent(tCTRate);
       
		return tElmDetail;
	}

	private Element addCMTFund(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode) {
		String tParam = null;
		Element tElmDetail = new Element("CMTFund");
        Element tCHealFee = new Element("CHealFee");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"CHealFee");
        tCHealFee.setText(tParam);
        Element tMaxPayTimes = new Element("MaxPayTimes");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"MaxPayTimes");
        tMaxPayTimes.setText(tParam);
       
        
        tElmDetail.addContent(tCHealFee);
        tElmDetail.addContent(tMaxPayTimes);
       
       
		return tElmDetail;
	}

	private Element addDentalInsur(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode) {
		String tParam = null;
		Element tElmDetail = new Element("DentalInsur");
        Element tPrepHealGetFee = new Element("PrepHealGetFee");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"PrepHealGetFee");
        tPrepHealGetFee.setText(tParam);
        Element tPrepHealGetRate = new Element("PrepHealGetRate");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"PrepHealGetRate");
        tPrepHealGetRate.setText(tParam);
        Element tBaseHealFee = new Element("BaseHealFee");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"BaseHealFee");
        tBaseHealFee.setText(tParam);
        Element tBaseHealGetRate = new Element("BaseHealGetRate");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"BaseHealGetRate");
        tBaseHealGetRate.setText(tParam);
        
        tElmDetail.addContent(tPrepHealGetFee);
        tElmDetail.addContent(tPrepHealGetRate);
        tElmDetail.addContent(tBaseHealFee);
        tElmDetail.addContent(tBaseHealGetRate);
       
		return tElmDetail;
	}

	private Element addMIFund(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode) {
		String tParam = null;
		Element tElmDetail = new Element("MIFund");
        Element tGetRate = new Element("GetRate");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate");
        tGetRate.setText(tParam);
        Element tChildbirthFee = new Element("ChildbirthFee");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"ChildbirthFee");
        tChildbirthFee.setText(tParam);
        Element tNurseryFee = new Element("NurseryFee");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"NurseryFee");
        tNurseryFee.setText(tParam);
       
        
        tElmDetail.addContent(tGetRate);
        tElmDetail.addContent(tChildbirthFee);
        tElmDetail.addContent(tNurseryFee);
        
       
		return tElmDetail;
	}

	private Element addOEMFund(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode) {
		String tParam = null;
		Element tElmDetail = new Element("OEMFund");
        Element tGetRate = new Element("GetRate");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate");
        tGetRate.setText(tParam);
        Element tRemDoorFee = new Element("RemDoorFee");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"RemDoorFee");
        tRemDoorFee.setText(tParam);
        Element tRegistrationFee = new Element("RegistrationFee");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"RegistrationFee");
        tRegistrationFee.setText(tParam);
        Element tMentalDoorFee = new Element("MentalDoorFee");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"MentalDoorFee");
        tMentalDoorFee.setText(tParam);
        
        tElmDetail.addContent(tGetRate);
        tElmDetail.addContent(tRemDoorFee);
        tElmDetail.addContent(tRegistrationFee);
        tElmDetail.addContent(tMentalDoorFee);
       
		return tElmDetail;
	}
	
	/**
	 * 住院医疗保险金责任部分展现
	 * @param GrpContNo 团单号
	 * @param tRiskCode	险种号
	 * @param tPlanCode	保障计划号	
	 * @param tDutyCode	责任编码
	 * @return tElmDetail
	 */
	private Element addHMIFund(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode) {
		String tParam = null;
		Element tElmDetail = new Element("HMIFund");
        Element tGetRate = new Element("GetRate");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate");
        tGetRate.setText(tParam);
        Element tDaysInHosFee = new Element("DaysInHosFee");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"DaysInHosFee");
        tDaysInHosFee.setText(tParam);
        Element tBedFee = new Element("BedFee");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"BedFee");
        tBedFee.setText(tParam);
        Element tInstrumentFee = new Element("InstrumentFee");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"InstrumentFee");
        tInstrumentFee.setText(tParam);
        Element tMealsFee = new Element("MealsFee");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"MealsFee");
        tMealsFee.setText(tParam);
        Element tMentalInHosFee = new Element("MentalInHosFee");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"MentalInHosFee");
        tMentalInHosFee.setText(tParam);
        Element tMaxDays = new Element("MaxDays");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"MaxDays");
        tMaxDays.setText(tParam);
        Element tWuLIPei = new Element("WuLIPei");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"WuLIPei");
        tWuLIPei.setText(tParam);
        Element tWuLIPeiDays = new Element("WuLIPeiDays");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"WuLIPeiDays");
        tWuLIPeiDays.setText(tParam);
        tElmDetail.addContent(tGetRate);
        tElmDetail.addContent(tDaysInHosFee);
        tElmDetail.addContent(tBedFee);
        tElmDetail.addContent(tInstrumentFee);
        tElmDetail.addContent(tMealsFee);
        tElmDetail.addContent(tMentalInHosFee);
        tElmDetail.addContent(tMaxDays);
        tElmDetail.addContent(tWuLIPei);
        tElmDetail.addContent(tWuLIPeiDays);
		return tElmDetail;
	}
	
	private Element addHBFund(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode,String tInsuPlanCode) {
		String tParam = null;
		Element tElmDetail = new Element("HBFund");
//		if("F11".equals(tInsuPlanCode)){
//			Element tHosPhysicalFee = new Element("HosPhysicalFee");
//	        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"HosPhysicalFee");
//	        tHosPhysicalFee.setText(tParam);
//	        
//	        tElmDetail.addContent(tHosPhysicalFee);
//		}else{
			    Element tInHosFee = new Element("InHosFee");
		        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"InHosFee");
		        tInHosFee.setText(tParam);
		        Element tICU = new Element("ICU");
		        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"ICU");
		        tICU.setText(tParam);
		        Element tHosMisFee = new Element("HosMisFee");
		        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"HosMisFee");
		        tHosMisFee.setText(tParam);
		        Element tHosPhysicalFee = new Element("HosPhysicalFee");
		        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"HosPhysicalFee");
		        tHosPhysicalFee.setText(tParam);
		        Element tReProFee = new Element("ReProFee");
		        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"ReProFee");
		        tReProFee.setText(tParam);
		        Element tReProFeeDays = new Element("ReProFeeDays");
		        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"ReProFeeDays");
		        tReProFeeDays.setText(tParam);
		        Element tPsyTreatmentfee = new Element("PsyTreatmentfee");
		        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"PsyTreatmentfee");
		        tPsyTreatmentfee.setText(tParam);
		        Element tLocalAmbulance = new Element("LocalAmbulance");
		        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"LocalAmbulance");
		        tLocalAmbulance.setText(tParam);
		        Element tOrganTransplant = new Element("OrganTransplant");
		        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"OrganTransplant");
		        tOrganTransplant.setText(tParam);
		        
		        tElmDetail.addContent(tInHosFee);
		        tElmDetail.addContent(tICU);
		        tElmDetail.addContent(tHosMisFee);
		        tElmDetail.addContent(tHosPhysicalFee);
		        tElmDetail.addContent(tReProFee);
		        tElmDetail.addContent(tReProFeeDays);
		        tElmDetail.addContent(tPsyTreatmentfee);
		        tElmDetail.addContent(tLocalAmbulance);
		        tElmDetail.addContent(tOrganTransplant);
//		}
       
		return tElmDetail;
	}
	
	private Element addOECFund(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode,String tInsuPlanCode) {
		String tParam = null;
		Element tElmDetail = new Element("OECFund");
		
		Element tLimintAmnt = new Element("LimintAmnt");
	    tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"LimintAmnt");
	    tLimintAmnt.setText(tParam);
	    Element tGetRate = new Element("GetRate");
	    tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate1");
	    tGetRate.setText(tParam);
	    Element tOrdinaryFee = new Element("OrdinaryFee");
	    tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"OrdinaryFee");
	    tOrdinaryFee.setText(tParam);
	    Element tPhysicalTreat = new Element("PhysicalTreat");
	    tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"PhysicalTreat");
	    tPhysicalTreat.setText(tParam);
	    Element tXFee = new Element("XFee");
	    tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"XFee");
	    tXFee.setText(tParam);
	    Element tTCMFee = new Element("TCMFee");
	    tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"TCMFee");
	    tTCMFee.setText(tParam);
	    Element tCancerFee = new Element("CancerFee");
	    tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"CancerFee");
	    tCancerFee.setText(tParam);
	    Element tCancerFeeLife = new Element("CancerFeeLife");
	    tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"CancerFeeLife");
	    tCancerFeeLife.setText(tParam);
	    Element tAccDentalFee = new Element("AccDentalFee");
	    tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"AccDentalFee");
	    tAccDentalFee.setText(tParam);
	    Element tChildFee = new Element("ChildFee");
	    tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"ChildFee");
	    tChildFee.setText(tParam);
	    Element tLocalAmbulance = new Element("LocalAmbulance");
	    tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"LocalAmbulance");
	    tLocalAmbulance.setText(tParam);
	        	    
	    tElmDetail.addContent(tLimintAmnt);
	    tElmDetail.addContent(tGetRate);
	    tElmDetail.addContent(tOrdinaryFee);
	    tElmDetail.addContent(tPhysicalTreat);
	    tElmDetail.addContent(tXFee);
	    tElmDetail.addContent(tTCMFee);
	    tElmDetail.addContent(tCancerFee);
	    tElmDetail.addContent(tCancerFeeLife);
	    tElmDetail.addContent(tAccDentalFee);
	    tElmDetail.addContent(tChildFee);
	    tElmDetail.addContent(tLocalAmbulance);
       
		return tElmDetail;
	}
	
	private Element addMCFund(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode,String tInsuPlanCode) {
		String tParam = null;
		Element tElmDetail = new Element("MCFund");
		
        Element tWaitingPeriod = new Element("WaitingPeriod");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"WaitingPeriod");
        tWaitingPeriod.setText(tParam);
        Element tProduction = new Element("Production");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"Production");
        tProduction.setText(tParam);
        Element tNeonatalProtection = new Element("NeonatalProtection");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"NeonatalProtection");
        tNeonatalProtection.setText(tParam);
        Element tComplication = new Element("Complication");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"Complication");
        tComplication.setText(tParam);
              
        tElmDetail.addContent(tWaitingPeriod);
        tElmDetail.addContent(tProduction);
        tElmDetail.addContent(tNeonatalProtection);
        tElmDetail.addContent(tComplication);
       
		return tElmDetail;
	}
	
	private Element addDCFund(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode,String tInsuPlanCode) {
		String tParam = null;
		Element tElmDetail = new Element("DCFund");
		
        Element tLimintAmnt = new Element("LimintAmnt");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"LimintAmnt");
        tLimintAmnt.setText(tParam);
        Element tGetRate = new Element("GetRate");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate1");
        tGetRate.setText(tParam);
        Element tWaitingPeriod = new Element("WaitingPeriod");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"WaitingPeriod");
        tWaitingPeriod.setText(tParam);
        Element tPrepHeal = new Element("PrepHeal");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"PrepHeal");
        tPrepHeal.setText(tParam);       
        Element tBaseHeal = new Element("BaseHeal");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"BaseHeal");
        tBaseHeal.setText(tParam);
        Element tImportantHeal = new Element("ImportantHeal");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"ImportantHeal");
        tImportantHeal.setText(tParam);
        Element tToothwash = new Element("Toothwash");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"Toothwash");
        tToothwash.setText(tParam);        
        Element tHosList = new Element("HosList");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"HosList");
        tHosList.setText(tParam);
        
        tElmDetail.addContent(tLimintAmnt);
        tElmDetail.addContent(tGetRate);
        tElmDetail.addContent(tWaitingPeriod);
        tElmDetail.addContent(tPrepHeal);       
        tElmDetail.addContent(tBaseHeal);
        tElmDetail.addContent(tImportantHeal);
        tElmDetail.addContent(tToothwash);
        tElmDetail.addContent(tHosList);
       
		return tElmDetail;
	}
	
	private Element addRPEFund(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode,String tInsuPlanCode) {
		String tParam = null;
		Element tElmDetail = new Element("RPEFund");
		
        Element tPhysical = new Element("Physical");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"Physical");
        tPhysical.setText(tParam);
               
        tElmDetail.addContent(tPhysical);

        return tElmDetail;
	}
	
	private Element addUMEFund(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode,String tInsuPlanCode) {
		String tParam = null;
		Element tElmDetail = new Element("UMEFund");
		
        Element tEmerTranFee = new Element("EmerTranFee");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"EmerTranFee");
        tEmerTranFee.setText(tParam);
        Element tAccommodationFee = new Element("AccommodationFee");
        tParam = getText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"AccommodationFee");
        tAccommodationFee.setText(tParam);

        tElmDetail.addContent(tEmerTranFee);
        tElmDetail.addContent(tAccommodationFee);

       
		return tElmDetail;
	}
	/**
	 * 获取责任下要素值
	 * @param tGrpContNo
	 * @param tRiskCode
	 * @param tPlanCode
	 * @param tDutyCode
	 * @param tParam
	 * @return 
	 */
	private String getText(String tGrpContNo, String tRiskCode, String tPlanCode,
		String tDutyCode, String tParam) {
		StringBuffer tParamSQL = new StringBuffer();
		tParamSQL.append(" select distinct trim(calfactorvalue)  from lccontplandutyparam " +
        			"	where grpcontno = '"+tGrpContNo+"' and " +
        					" riskcode = '"+tRiskCode+"' " +
        					" and contplancode = '"+tPlanCode+"' " +
        					" and dutycode = '"+tDutyCode+"' " +
        					" and calfactor = '"+tParam+"' ");
        ExeSQL tEParamSQL = new ExeSQL();
        SSRS tSSRParam = tEParamSQL.execSQL(tParamSQL.toString());
        if (tSSRParam.getMaxRow() > 0){
        	if(tSSRParam.GetText(1, 1)!=null&&!"".equals(tSSRParam.GetText(1, 1))){
        		if("GetRate1".equals(tParam)&&"676012".equals(tDutyCode)){
        			double getrate=Double.parseDouble(tSSRParam.GetText(1, 1));
        			NumberFormat num = NumberFormat.getPercentInstance();
        			num.setMaximumFractionDigits(2);
        			String result=num.format(getrate);
        			return result;
        		}else{
        			return tSSRParam.GetText(1, 1);
        		}        		
        	}        		
        	else{
        		return "--";
        	}        		
        }
        else
        	return "--";
        
	
}

	/**
     * setPrem 绑定型主险显示主附险的保费合计，附险不显示保费 若当前险种是绑定型主险，则对主附险进行求和
     * 
     * @param cGrpContNo
     *            String
     * @param cContPlanCode
     *            String
     * @param cRiskCode
     *            String
     * @return String 主附险保费合计，若险种是附险，则返回“”
     */
    private String setPrem(String cGrpContNo, String cContPlanCode, String cRiskCode)
    {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(cRiskCode);
        if (!tLMRiskAppDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "LCGrpContF1PBL";
            tError.functionName = "setPrem";
            tError.errorMessage = "没有查询到险种定义信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        if ("0".equals(tLMRiskAppDB.getNotPrintPol()))
        {
            return "";
        }

        String premSumPart = "  and RiskCode = '" + cRiskCode + "' ";
        if ("1".equals(tLMRiskAppDB.getNotPrintPol()))
        {
            premSumPart = " and (X.RiskCode='" + cRiskCode + "' "
                    + "   or X.RiskCode in(select RiskCode from LDRiskParamPrint "
                    + "              where ParamType1 = 'sumprem' " + "                 and ParamValue1='" + cRiskCode
                    + "')) ";
        }

        // 查询非绑定附险种的保费，包括绑定主险的附险保费
        ExeSQL tExeSQL = new ExeSQL();

        StringBuffer sql2 = new StringBuffer();
        sql2.append(" select nvl(sum(n), 0) from ").append("(").append(
                "    select sum(b.Prem) n,a.contplancode,b.RiskCode,a.grpcontno ").append(
                "    from lccontplan a,lcpol b,lcinsured c ").append(
                "    where  a.grpcontno=b.grpcontno  and b.insuredno=c.insuredno  and b.contno=c.contno ").append(
                "      and a.contplancode=c.contplancode  and a.grpcontno=c.grpcontno ").append(
                "      and b.grpcontno=c.grpcontno ").append("      and b.insuredname not like ('公共账户') ").append(
                "      and b.appflag in('1','9') ").append("    group by a.contplancode,b.RiskCode,a.grpcontno ")
                .append(" )  as X ").append(" where X.contplancode='" + cContPlanCode + "' ").append(premSumPart)
                .append("   and X.grpcontno='" + cGrpContNo + "' ");
        String prem = tExeSQL.getOneValue(sql2.toString());

        if (tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LCGrpContF1PBL";
            tError.functionName = "setPrem";
            tError.errorMessage = "查询邦定主险保费出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        if ("".equals(prem) || "null".equals(prem))
        {
            CError tError = new CError();
            tError.moduleName = "LCGrpContF1PBL";
            tError.functionName = "setPrem";
            tError.errorMessage = "查询主险保费失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return format(Double.parseDouble(prem));
    }

    //增加一个方法用来判断是否是万能险还是之前的险种，根据此方法区分
    public boolean jugdeUliByGrpContNo()
    {
        StringBuffer tSBql2 = new StringBuffer(256);
        tSBql2.append("select 1 from lcgrppol where GrpContNo = '");
        tSBql2.append(this.mLCGrpContSchema.getGrpContNo());
        tSBql2.append("' and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G')");
        ExeSQL tExeSQL5 = new ExeSQL();
        SSRS tSSRS5 = tExeSQL5.execSQL(tSBql2.toString());
        if (tSSRS5.getMaxRow() < 1)
        {
            return false;
        }
        return true;
    }

    /**
     * 根据团单合同号，查询被保人信息
     * 
     * @param cXmlDataset
     *            XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean genInsuredDetail(XMLDataset cXmlDataset) throws Exception
    {
        // 数据流
        String tTemplateFile = "";
        // 团险默认配置文件

        if (!jugdeUliByGrpContNo())
        {
            tTemplateFile = mTemplatePath + "GrpInsured.xml";
        }
        else
        {
            tTemplateFile = mTemplatePath + "GrpUliInsured.xml";
        }
        // 校验配置文件是否存在
        System.out.println("tTemplateFile" + tTemplateFile);
        mFile = new File(tTemplateFile);
        if (!mFile.exists())
        {
            // throw new Exception("缺少配置文件：" + tTemplateFile);
            buildError("genInsuredDetail", "XML配置文件不存在！");
            return false;
        }
        try
        {
            String mGrpContNo = this.mLCGrpContSchema.getGrpContNo();
            // 如不打印被保险人清单，将团体保单号置空
            if ("0".equals(printInsureDetail))
                mGrpContNo = "00000000000000";

            Hashtable thashData = new Hashtable();
            // 将变量GrpContNo的值赋给xml文件
            System.out.println("mGrpContNomGrpContNomGrpContNo" + mGrpContNo);
            thashData.put("_GRPCONTNO", mGrpContNo);
            // 根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(tTemplateFile), thashData);
            // tXmlDataMine.setDataFormater(tInnerFormater);
            cXmlDataset.addDataObject(tXmlDataMine);
        }
        catch (Exception e)
        {
            buildError("genInsuredList", "根据XML文件生成报表数据失败！");
            return false;
        }

        // 增加被保险人列表节点。0：无被保险人；1：有被保险人
        String sql = "select count(1) from LCInsured where GrpContNo = '" + this.mLCGrpContSchema.getGrpContNo()
                + "' and name not in ('公共账户','无名单')";
        if ("0".equals(new ExeSQL().getOneValue(sql)))
            cXmlDataset.addDataObject(new XMLDataTag("LCInsuredDetailExistFlag", "0"));
        else
            cXmlDataset.addDataObject(new XMLDataTag("LCInsuredDetailExistFlag", "1"));

        return true;
    }

    /**
     * 根据团单合同号，查询被保人急救医疗卡信息
     * 
     * @param cXmlDataset
     *            XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean genInsuredCard(XMLDataset cXmlDataset) throws Exception
    {
        // 数据流
        // InnerFormater tInnerFormater = null;
        String tTemplateFile = "";
        // 团险默认配置文件
        tTemplateFile = mTemplatePath + "GrpInsuredCard.xml";
        // 校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists())
        {
            // throw new Exception("缺少配置文件：" + tTemplateFile);
            buildError("genInsuredCard", "XML配置文件不存在！");
            return false;
        }
        try
        {
            Hashtable thashData = new Hashtable();
            // 将变量GrpContNo的值赋给xml文件
            thashData.put("_GRPCONTNO", this.mLCGrpContSchema.getGrpContNo());
            // 根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(tTemplateFile), thashData);
            // tXmlDataMine.setDataFormater(tInnerFormater);
            cXmlDataset.addDataObject(tXmlDataMine);
        }
        catch (Exception e)
        {
            buildError("genInsuredList", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    /**
     *一个空值节点
     * 
     * @param cXmlDataset
     * @return boolean
     */
    private boolean getReceiptNote(XMLDataset tXmlDataset){
    	XMLDataList xmlDataList = new XMLDataList();
    	xmlDataList.setDataObjectID("ReceiptNote");
    	tXmlDataset.addDataObject(xmlDataList);
    	return true;
    }
    
    /**
     * 获取险种名称以及保费
     * 
     * @param cXmlDataset
     * @return boolean
     */
    private boolean getPremDetail(XMLDataset tXmlDataset)
    {
    	XMLDataList xmlDataList = new XMLDataList();
    	xmlDataList.setDataObjectID("PremDetail");
    	xmlDataList.addColHead("RiskName");
    	xmlDataList.addColHead("RiskPrem");
    	ExeSQL tExeSQL = new ExeSQL();
    	String tStrSql= "select distinct(riskcode) from lcpol "
    				  +	"where grpcontno='"+this.mLCGrpContSchema.getGrpContNo()+"'";
    	SSRS tSSRS = new ExeSQL().execSQL(tStrSql);
    	if(tSSRS.getMaxRow()>0){
    		for(int i=1;i<=tSSRS.getMaxRow();i++){ 
    			String tStrSql1= "select riskname from lmriskapp "
    						   + "where riskcode='"+tSSRS.GetText(i,1)+"'";
    			String riskname=tExeSQL.getOneValue(tStrSql1);	
                xmlDataList.setColValue("RiskName", riskname);

    			String tStrSql2= "select sum(sumactupaymoney) from ljapaygrp "
     				           + "where payno = (select payno from ljapay "
     				           + "where incomeno='" +this.mLCGrpContSchema.getGrpContNo()+ "' and duefeetype='0') "
     				           + "and riskcode ='"+tSSRS.GetText(i,1)+"'";	 
                 String riskprem = tExeSQL.getOneValue(tStrSql2);
                 xmlDataList.setColValue("RiskPrem", riskprem);
                 xmlDataList.insertRow(0);  
    		}
    	}   
    	tXmlDataset.addDataObject(xmlDataList);
    	return true;
    }
  
    /**
     * 获取中文保费数据
     * 
     * @param cXmlDataset
     * @return boolean
     */
    private boolean getMoney(XMLDataset cXmlDataset)
    {
        try
        {
            XMLDataList tXMLDataList = new XMLDataList();
            tXMLDataList.setDataObjectID("Money");
            tXMLDataList.addColHead("ChinaMoney");
            tXMLDataList.buildColHead();

            tXMLDataList.setColValue("ChinaMoney", PubFun.getChnMoney(this.mLCGrpContSchema.getPrem()));
            tXMLDataList.insertRow(0);

            cXmlDataset.addDataObject(tXMLDataList);
        }
        catch (Exception e)
        {
            buildError("getMoney", "添加保费数据失败！");
            return false;
        }
        return true;
    }

    /**
     * 获取发票信息
     * 
     * @param cXmlDataset
     *            XMLDataset
     * @return boolean
     * @throws Exception
     */
    private boolean getInvoice(XMLDataset cXmlDataset) throws Exception
    {
        // 数据流
        // InnerFormater tInnerFormater = null;
        String tTemplateFile = "";
        // 团险默认配置文件
        tTemplateFile = mTemplatePath + "GrpContInvoice.xml";
        // 校验配置文件是否存在
        mFile = new File(tTemplateFile);
        if (!mFile.exists())
        {
            buildError("getInvoice", "XML配置文件不存在！");
            return false;
        }
        try
        {
            Hashtable tHashData = new Hashtable();
            // 将变量GrpContNo的值赋给xml文件
            tHashData.put("_GRPCONTNO", this.mLCGrpContSchema.getGrpContNo());
            // 根据配置文件生成xml数据
            XMLDataMine tXmlDataMine = new XMLDataMine(new FileInputStream(tTemplateFile), tHashData);
            // tXmlDataMine.setDataFormater(tInnerFormater);
            cXmlDataset.addDataObject(tXmlDataMine);

            // cXmlDataset.addDataObject(new XMLDataTag("RMB",
            // PubFun.getChnMoney(this.
            // mLCGrpContSchema.getPrem()))); //保费大写
            // 收款人，目前系统提供操作员信息

            // 查询缴费方式信息
            // 根据合同号，查询缴费记录信息
            // String tSql =
            // "select distinct TempFeeNo from LJTempFee where OtherNo = '" +
            // this.mLCGrpContSchema.getGrpContNo() +
            // "' and OtherNoType = '7'";

            XMLDataList tXMLDataList = new XMLDataList();

            StringBuffer tSBql = new StringBuffer(256);
            tSBql.append("select distinct TempFeeNo from LJTempFee where OtherNo = '");
            tSBql.append(this.mLCGrpContSchema.getGrpContNo());
            tSBql.append("' and OtherNoType = '7'");

            // String tWhere = " where a.TempFeeNo in ('";
            StringBuffer tBWhere = new StringBuffer(" where a.TempFeeNo in ('");

            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(tSBql.toString());
            for (int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                // 将每笔缴费的缴费号串连起来作为查询条件
                if (i == tSSRS.getMaxRow())
                {
                    // tWhere = tWhere + tSSRS.GetText(i, 1) + "')";
                    tBWhere.append(tSSRS.GetText(i, 1));
                    tBWhere.append("')");
                }
                else
                {
                    // tWhere = tWhere + tSSRS.GetText(i, 1) + "','";
                    tBWhere.append(tSSRS.GetText(i, 1));
                    tBWhere.append("','");
                }
            }
            // cXmlDataset.addDataObject(new XMLDataTag("TempFeeNo",
            // tSSRS.GetText(1, 1))); //收据号

            // 根据上面的查询条件，查询缴费方式为现金的数据
            // tSql = "select a.ConfMakeDate from LJTempFeeClass a" +
            // tWhere + " and a.PayMode = '1' order by a.ConfMakeDate";
            tSBql.setLength(0);
            tSBql.append("select a.ConfMakeDate from LJTempFeeClass a");
            tSBql.append(tBWhere);
            tSBql.append(" and a.PayMode = '1' order by a.ConfMakeDate");

            tSSRS = tExeSQL.execSQL(tSBql.toString());
            if (tSSRS.getMaxRow() > 0)
            {
                // 添加缴费方式为现金的标签
                tXMLDataList = new XMLDataList();
                // 添加xml一个新的对象Term
                tXMLDataList.setDataObjectID("Cash");
                // 添加Head单元内的信息
                tXMLDataList.addColHead("PayMode");
                tXMLDataList.addColHead("ConfMakeDate");
                tXMLDataList.buildColHead();
                // 此缴费方式只需知道确认日期
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    tXMLDataList.setColValue("PayMode", "现金");
                    tXMLDataList.setColValue("ConfMakeDate", tSSRS.GetText(i, 1));
                    tXMLDataList.insertRow(0);
                }
                cXmlDataset.addDataObject(tXMLDataList);
            }

            // 根据上面的查询条件，查询缴费方式不是现金的数据
            // tSql =
            // "select b.CodeName,a.BankAccNo,a.AccName,a.ConfMakeDate from
            // LJTempFeeClass a,LDCode b" +
            // tWhere + " and a.PayMode <> '1' and a.BankCode = b.Code and
            // b.CodeType = 'bank' order by a.BankAccNo,a.ConfMakeDate";
            tSBql.setLength(0);
            tSBql.append("select b.BankName,a.BankAccNo,a.AccName,a.ConfMakeDate from LJTempFeeClass a, LDBank b");
            tSBql.append(tBWhere);
            tSBql.append(" and a.PayMode <> '1' and a.BankCode = b.BankCode order by a.BankAccNo,a.ConfMakeDate");
            System.out.println(tSBql.toString());
            tSSRS = tExeSQL.execSQL(tSBql.toString());
            if (tSSRS.getMaxRow() > 0)
            {
                // 添加缴费方式为支票的标签
                tXMLDataList = new XMLDataList();
                // 添加xml一个新的对象Term
                tXMLDataList.setDataObjectID("Check");
                // 添加Head单元内的信息
                tXMLDataList.addColHead("PayMode");
                tXMLDataList.addColHead("Bank");
                tXMLDataList.addColHead("AccName");
                tXMLDataList.addColHead("BankAccNo");
                tXMLDataList.addColHead("ConfMakeDate");
                tXMLDataList.buildColHead();
                // 此标签需要获得银行、户名、帐户的信息
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    tXMLDataList.setColValue("PayMode", "银行转帐");
                    tXMLDataList.setColValue("Bank", tSSRS.GetText(i, 1));
                    tXMLDataList.setColValue("AccName", tSSRS.GetText(i, 2));
                    tXMLDataList.setColValue("BankAccNo", tSSRS.GetText(i, 3));
                    tXMLDataList.setColValue("ConfMakeDate", tSSRS.GetText(i, 4));
                    tXMLDataList.insertRow(0);
                }
                cXmlDataset.addDataObject(tXMLDataList);
            }

            // cXmlDataset.addDataObject(new XMLDataTag("End", "1"));
        }
        catch (Exception e)
        {
            buildError("getInvoice", "根据XML文件生成报表数据失败！");
            return false;
        }
        return true;
    }

    private boolean CreateEndDate(XMLDataset cXmlDataset)
    {
        XMLDataList tXMLDataList = new XMLDataList();
        tXMLDataList = new XMLDataList();
        tXMLDataList.setDataObjectID("End");
        tXMLDataList.addColHead("Flag");
        tXMLDataList.buildColHead();

        tXMLDataList.setColValue("Flag", "1");
        tXMLDataList.insertRow(0);
        cXmlDataset.addDataObject(tXMLDataList);
        return true;
    }

    /**
     * 取得团单下的全部LCGrpPol表数据
     * 
     * @param cGrpContNo
     *            String
     * @return LCGrpPolSet
     * @throws Exception
     */
    private static LCGrpPolSet getRiskList(String cGrpContNo) throws Exception
    {
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();

        tLCGrpPolDB.setGrpContNo(cGrpContNo);
        // 由于LCGrpCont和LCGrpPol为一对多的关系，所以采用query方法
        tLCGrpPolSet = tLCGrpPolDB.query();

        return tLCGrpPolSet;
    }

    /**
     * 获取补打保单的数据
     * 
     * @return boolean
     * @throws Exception
     */
    private boolean getRePrintData() throws Exception
    {
        // String tCurDate = PubFun.getCurrentDate();
        // String tCurTime = PubFun.getCurrentTime();

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = null;

        // 查询LCPolPrint表，获取要补打的保单数据
        String tSql = "SELECT PrtTimes + 1 FROM LCPolPrint WHERE MainPolNo = '" + mLCGrpContSchema.getGrpContNo() + "'";
        System.out.println(tSql);
        tSSRS = tExeSQL.execSQL(tSql);

        if (tExeSQL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tExeSQL.mErrors);
            throw new Exception("获取打印数据失败");
        }
        if (tSSRS.MaxRow < 1)
        {
            throw new Exception("找不到原来的打印数据，可能传入的不是主险保单号！");
        }

        mResult.clear();
        mResult.add(mLCGrpContSchema);

        LCGrpContF1PBLS tLCGrpContF1PBLS = new LCGrpContF1PBLS();
        if (!tLCGrpContF1PBLS.submitData(mResult, "REPRINT"))
        {
            if (tLCGrpContF1PBLS.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tLCGrpContF1PBLS.mErrors);
            }
            throw new Exception("保存数据失败");
        }

        // 取打印数据
        Connection conn = null;

        try
        {
            DOMBuilder tDOMBuilder = new DOMBuilder();
            Element tRootElement = new Element("DATASETS");

            conn = DBConnPool.getConnection();

            if (conn == null)
            {
                throw new Exception("连接数据库失败！");
            }

            // String tSql = "";
            java.sql.Blob tBlob = null;
            // CDB2Blob tCDB2Blob = new CDB2Blob();

            tSql = " and MainPolNo = '" + mLCGrpContSchema.getGrpContNo() + "'";
            tBlob = CBlob.SelectBlob("LCPolPrint", "PolInfo", tSql, conn);

            if (tBlob == null)
            {
                throw new Exception("找不到打印数据");
            }

            // BLOB blob = (oracle.sql.BLOB)tBlob; --Fanym
            Element tElement = tDOMBuilder.build(tBlob.getBinaryStream()).getRootElement();
            tElement = new Element("DATASET").setMixedContent(tElement.getMixedContent());
            tRootElement.addContent(tElement);

            ByteArrayOutputStream tByteArrayOutputStream = new ByteArrayOutputStream();
            XMLOutputter tXMLOutputter = new XMLOutputter("  ", true, "UTF-8");
            tXMLOutputter.output(tRootElement, tByteArrayOutputStream);

            // mResult.clear();
            // mResult.add(new
            // ByteArrayInputStream(tByteArrayOutputStream.toByteArray()));
            ByteArrayInputStream tByteArrayInputStream = new ByteArrayInputStream(tByteArrayOutputStream.toByteArray());

            // 仅仅重新生成xml打印数据，影印件信息暂时不重新获取
            String FilePath = mOutXmlPath + "/printdata";
            mFile = new File(FilePath);
            if (!mFile.exists())
            {
                mFile.mkdir();
            }
            // 根据合同号生成打印数据存放文件夹
            FilePath = mOutXmlPath + "/printdata" + "/" + mLCGrpContSchema.getGrpContNo();
            mFile = new File(FilePath);
            if (!mFile.exists())
            {
                mFile.mkdir();
            }

            // 根据团单合同号生成文件
            String XmlFile = FilePath + "/" + mLCGrpContSchema.getGrpContNo() + ".xml";
            System.out.println("XmlFileXmlFile" + XmlFile);
            FileOutputStream fos = new FileOutputStream(XmlFile);
            // 此方法写入数据准确，但是相对效率比较低下
            int n = 0;
            while ((n = tByteArrayInputStream.read()) != -1)
            {
                fos.write(n);
            }
            fos.close();

            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();

            try
            {
                if (conn != null)
                {
                    conn.close();
                }
            }
            catch (Exception e)
            {
                // do nothing
            }

            throw ex;
        }
        return true;
    }

    /**
     * 格式化浮点型数据
     * 
     * @param dValue
     *            double
     * @return String
     */
    private static String format(double dValue)
    {
        return new DecimalFormat("0.00").format(dValue);
    }

    private boolean checkdate() throws Exception
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        tLCGrpContDB.setGrpContNo(this.mLCGrpContSchema.getGrpContNo());
        tLCGrpContSet = tLCGrpContDB.query();
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        SSRS tSSRS1 = new SSRS();
        SSRS tSSRS2 = new SSRS();
        tSSRS = tExeSQL.execSQL("select distinct Grpname,customerno,payintv from lcgrppol where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "'");
        tSSRS1 = tExeSQL.execSQL("select distinct AppntName,AppntNo,payintv from lccont where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "'  and  appflag in('1','9') ");
        tSSRS2 = tExeSQL.execSQL("select distinct AppntName,AppntNo,payintv from lcpol where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "' and appflag in('1','9') ");
        if (tSSRS.getMaxRow() != 1 || tSSRS1.getMaxRow() != 1 || tSSRS2.getMaxRow() != 1)
        {
            buildError("checkdate", "取投保客户数据失败!");
            return false;
        }
        if (!(tLCGrpContSet.get(1).getGrpName().equals(tSSRS.GetText(1, 1)))
                || !(tLCGrpContSet.get(1).getGrpName().equals(tSSRS1.GetText(1, 1)))
                || !(tLCGrpContSet.get(1).getGrpName().equals(tSSRS2.GetText(1, 1))))
        {
            buildError("checkdate", "取投保客户名称失败!");
            return false;
        }
        if (!(tLCGrpContSet.get(1).getAppntNo().equals(tSSRS.GetText(1, 2)))
                || !(tLCGrpContSet.get(1).getAppntNo().equals(tSSRS1.GetText(1, 2)))
                || !(tLCGrpContSet.get(1).getAppntNo().equals(tSSRS2.GetText(1, 2))))
        {
            buildError("checkdate", "取投保客户号码失败!");
            return false;
        }
        if (!(String.valueOf(tLCGrpContSet.get(1).getPayIntv()).equals(tSSRS.GetText(1, 3)))
                || !(String.valueOf(tLCGrpContSet.get(1).getPayIntv()).equals(tSSRS1.GetText(1, 3)))
                || !(String.valueOf(tLCGrpContSet.get(1).getPayIntv()).equals(tSSRS2.GetText(1, 3))))
        {
            buildError("checkdate", "取缴费频次失败!");
            return false;
        }
        tSSRS.Clear();
        tSSRS1.Clear();
        tSSRS2.Clear();
        tSSRS = tExeSQL.execSQL("select count(insuredno) from lccont where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo()
                + "' and appflag in('1','9') and  insuredname not in ('公共账户','无名单')");
        tSSRS1 = tExeSQL.execSQL("select count(insuredno) from lcinsured where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo()
                + "'  and  name not in ('公共账户','无名单') and relationtomaininsured='00' ");
        if (!tSSRS.GetText(1, 1).equals(tSSRS1.GetText(1, 1)))
        {
            buildError("checkdate", "取被保人数失败!");
            return false;
        }
        tSSRS2 = tExeSQL
                .execSQL("SELECT count(1) FROM LCGrpCont  WHERE AppFlag in ('1','9') and  PrintCount =1   and grpcontno='"
                        + this.mLCGrpContSchema.getGrpContNo() + "' ");
        System.out.println("tSSRS2.GetText(1,1)" + tSSRS2.GetText(1, 1));
        if (!StrTool.cTrim(tSSRS2.GetText(1, 1)).equals("0"))
        {
            buildError("checkdate", "该保单已经进行过打印!");
            return false;
        }
        tSSRS.Clear();
        tSSRS1.Clear();
        tSSRS2.Clear();
        tSSRS2 = tExeSQL.execSQL("select count(1) from lcinsured where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "'  and  name in ('无名单') ");
        System.out.println("tSSRS2.GetText(1,1)" + tSSRS2.GetText(1, 1));
        if (StrTool.cTrim(tSSRS2.GetText(1, 1)).equals("0"))
        {
            tSSRS = tExeSQL.execSQL("select sum(peoples) from lccont where grpcontno='"
                    + this.mLCGrpContSchema.getGrpContNo()
                    + "' and appflag in('1','9') and  insuredname not in ('公共账户','无名单')");
            tSSRS1 = tExeSQL.execSQL("select count(insuredno) from lcinsured where grpcontno='"
                    + this.mLCGrpContSchema.getGrpContNo() + "'  and  name not in ('公共账户','无名单') ");
            if (!(String.valueOf(tLCGrpContSet.get(1).getPeoples2()).equals(tSSRS.GetText(1, 1)))
                    || !((String.valueOf(tLCGrpContSet.get(1).getPeoples2()).equals(tSSRS1.GetText(1, 1)))))
            {
                buildError("checkdate", "取团体被保人数失败!");
                return false;
            }
        }
        if (StrTool.cTrim(tLCGrpContSet.get(1).getCustomerReceiptNo()).equals(""))
        {
            buildError("checkdate", "取保险合同送达回执号失败!");
            return false;
        }
        if (StrTool.cTrim(tLCGrpContSet.get(1).getContPrintType()).equals(""))
        {
            buildError("checkdate", "取保单打印类型失败!");
            return false;
        }

        tSSRS.Clear();
        tSSRS1.Clear();
        tSSRS2.Clear();
        tSSRS = tExeSQL.execSQL("select sum(prem) from lcgrppol where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "' and appflag in('1','9')");
        tSSRS1 = tExeSQL.execSQL("select sum(prem) from lcpol where grpcontno='" + this.mLCGrpContSchema.getGrpContNo()
                + "' and appflag in('1','9')");
        tSSRS2 = tExeSQL.execSQL("select sum(prem) from lccont where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "' and appflag in('1','9')");
        if (tLCGrpContSet.get(1).getPrem() != Double.parseDouble(tSSRS.GetText(1, 1))
                || tLCGrpContSet.get(1).getPrem() != Double.parseDouble(tSSRS1.GetText(1, 1))
                || tLCGrpContSet.get(1).getPrem() != Double.parseDouble(tSSRS2.GetText(1, 1)))
        {
            System.out.println("zzzzz" + tLCGrpContSet.get(1).getPrem());
            System.out.println("bbbbbb" + tSSRS2.GetText(1, 1));
            buildError("checkdate", "取保单费用失败!");
            return false;
        }
        if (StrTool.cTrim(tLCGrpContSet.get(1).getPayMode()).equals("4"))
        {
            if (StrTool.cTrim(tLCGrpContSet.get(1).getAccName()).equals(""))
            {
                buildError("checkdate", "取银行帐户名失败！");
                return false;
            }

            if (StrTool.cTrim(tLCGrpContSet.get(1).getBankAccNo()).equals(""))
            {
                buildError("checkdate", "取银行帐号失败！");
                return false;
            }
            if (StrTool.cTrim(tLCGrpContSet.get(1).getBankCode()).equals(""))
            {
                buildError("checkdate", "取银行代码失败！");
                return false;
            }
        }

        LDComDB tLDComDB = new LDComDB();
        LDComSet tLDComSet = new LDComSet();
        tLDComDB.setComCode(tLCGrpContSet.get(1).getManageCom());
        tLDComSet = tLDComDB.query();
        if (StrTool.cTrim(tLDComSet.get(1).getName()).equals(""))
        {
            buildError("checkdate", "取分公司名称失败！");
            return false;
        }
        if (StrTool.cTrim(tLDComSet.get(1).getClaimReportPhone()).equals(""))
        {
            buildError("checkdate", "取理赔报案电话失败！");
            return false;
        }

        if (StrTool.cTrim(tLDComSet.get(1).getLetterServiceName()).equals(""))
        {
            buildError("checkdate", "取信函服务名称失败！");
            return false;
        }

        if (StrTool.cTrim(tLDComSet.get(1).getLetterServicePostAddress()).equals(""))
        {
            buildError("checkdate", "取地址信函服务地址失败！");
            return false;
        }

        if (StrTool.cTrim(tLDComSet.get(1).getLetterServicePostZipcode()).equals(""))
        {
            buildError("checkdate", "取地址信函服务邮编失败！");
            return false;
        }

        if (StrTool.cTrim(tLDComSet.get(1).getServicePostAddress()).equals(""))
        {
            buildError("checkdate", "取客户服务中心地址失败！");
            return false;
        }

        if (StrTool.cTrim(tLDComSet.get(1).getServicePostZipcode()).equals(""))
        {
            buildError("checkdate", "取客户服务中心邮编失败！");
            return false;
        }
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSet tLAAgentSet = new LAAgentSet();
        tLAAgentDB.setAgentCode(tLCGrpContSet.get(1).getAgentCode());
        tLAAgentSet = tLAAgentDB.query();
        if (StrTool.cTrim(tLAAgentSet.get(1).getName()).equals(""))
        {
            buildError("checkdate", "取业务员名称失败！");
            return false;
        }
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupDB.setAgentGroup(tLCGrpContSet.get(1).getAgentGroup());
        tLABranchGroupSet = tLABranchGroupDB.query();
        if (StrTool.cTrim(tLABranchGroupSet.get(1).getBranchAttr()).equals(""))
        {
            buildError("checkdate", "取营业单位代码失败！");
            return false;
        }
        if (StrTool.cTrim(tLABranchGroupSet.get(1).getName()).equals(""))
        {
            buildError("checkdate", "取营业单位名称失败！");
            return false;
        }
        
        SSRS tSSRSJX = tExeSQL
                .execSQL("SELECT count(1) FROM LCGrpPol  WHERE RiskCode <> '162001'   and grpcontno='"
                        + this.mLCGrpContSchema.getGrpContNo() + "' ");
        System.out.println("tSSRSJX.GetText(1,1)" + tSSRSJX.GetText(1, 1));
        if (!StrTool.cTrim(tSSRSJX.GetText(1, 1)).equals("0"))
        {
            buildError("checkdate", "该保单存在其它非健享全球的产品，不能进行打印!");
            return false;
        }
        tSSRSJX.Clear();
       

        return true;
    }

    /**
     * 根据险种，分别生成对应的：保费/限额/档次，节点
     * 
     * @param cXmlDataList
     * @param cAmntStr
     */
    private void dealAmntEx(XMLDataList cXmlDataList, String cAmntStr)
    {
        if (cAmntStr != null && !cAmntStr.equals(""))
        {

            String tAmntOrMult = cAmntStr.substring(0, cAmntStr.length() - 1);
            String tLastFlagOfAmnt = cAmntStr.substring(cAmntStr.length() - 1, cAmntStr.length());

            if ("X".equals(tLastFlagOfAmnt))
            {
                cXmlDataList.setColValue("DutyAmnt", tAmntOrMult);
            }
            else if ("B".equals(tLastFlagOfAmnt))
            {
                cXmlDataList.setColValue("Amnt", tAmntOrMult);
            }
            else if ("D".equals(tLastFlagOfAmnt))
            {
                cXmlDataList.setColValue("Mult", tAmntOrMult);
            }
        }
    }

    private boolean getGrpascriptionrule(XMLDataset tXmlDataset)
    {
        String str = "select distinct Ascriptionrulecode,Ascriptionrulename from lcAscriptionrulefactory a where a.GrpContNo='"
                + this.mLCGrpContSchema.getGrpContNo() + "'";
        ExeSQL tExeSQLScri = new ExeSQL();
        SSRS tSSRSScri = tExeSQLScri.execSQL(str.toString());
        XMLDataList tXMLDataList = new XMLDataList();
        if (tSSRSScri != null && tSSRSScri.getMaxRow() > 0)
        {
            tXMLDataList = new XMLDataList();
            // 添加xml一个新的对象Ascriptionrule
            tXMLDataList.setDataObjectID("LCAscriptionRuleFactory");
            // 添加Head单元内的信息
            tXMLDataList.addColHead("AscriptionRuleName");
            tXMLDataList.addColHead("AscriptionRule");
            tXMLDataList.buildColHead();
            for (int i = 1; i <= tSSRSScri.getMaxRow(); i++)
            {
                tXMLDataList.setColValue("AscriptionRuleName", tSSRSScri.GetText(i, 1));
                String str1 = "select  Ascriptionrulecode,Ascriptionrulename,Calremark,inerserialno from lcAscriptionrulefactory a where a.GrpContNo='"
                        + this.mLCGrpContSchema.getGrpContNo()
                        + "' and Ascriptionrulecode='"
                        + tSSRSScri.GetText(i, 1)
                        + "' order by InerSerialNo";

                ExeSQL tExeSQLScri1 = new ExeSQL();
                SSRS tSSRSScri1 = tExeSQLScri1.execSQL(str1.toString());
                StringBuffer tAscriptionRuleInfo = new StringBuffer();
                System.out.println("tSSRSScri1.getMaxRow()" + tSSRSScri1.getMaxRow());
                // 循环获取计划信息
                for (int j = 1; j <= tSSRSScri1.getMaxRow(); j++)
                {
                    String str2 = "select  Paramname,param from LCAscriptionRuleParams a where a.GrpContNo='"
                            + this.mLCGrpContSchema.getGrpContNo() + "' and Ascriptionrulecode='"
                            + tSSRSScri.GetText(i, 1) + "'and inerserialno ='" + tSSRSScri1.GetText(j, 4) + "'";
                    ExeSQL tExeSQLScri2 = new ExeSQL();
                    SSRS tSSRSScri2 = tExeSQLScri2.execSQL(str2.toString());
                    String SourceString = tSSRSScri1.GetText(j, 3);
                    String TargetString = "";
                    for (int m = 1; m <= tSSRSScri2.getMaxRow(); m++)
                    {
                        System.out.println(tSSRSScri2.GetText(m, 1) + "代替了" + tSSRSScri2.GetText(m, 2));
                        TargetString = SourceString.replaceAll(tSSRSScri2.GetText(m, 1), tSSRSScri2.GetText(m, 2));
                        System.out.println(TargetString);
                        SourceString = TargetString;

                    }
                    tAscriptionRuleInfo.append(SourceString);
                    tAscriptionRuleInfo.append(";");
                    System.out.println("SourceStringSourceString" + SourceString);
                    // 保障计划概述		

                    tXMLDataList.setColValue("AscriptionRule", tAscriptionRuleInfo.toString());

                }
                tXMLDataList.insertRow(0);
            }
            tXmlDataset.addDataObject(tXMLDataList);
        }

        return true;
    }

    private boolean getGrpNewCont(XMLDataset tXmlDataset)
    {
        XMLDataList tXMLDataList = new XMLDataList();
        tXMLDataList.setDataObjectID("LCNewGrpCont");
        // 添加Head单元内的信息
        tXMLDataList.addColHead("SumInsureds");
        tXMLDataList.addColHead("RiskCode");
        tXMLDataList.addColHead("RiskName");
        tXMLDataList.addColHead("PubPrem");
        tXMLDataList.addColHead("SelfPrem");
        tXMLDataList.addColHead("SelfGrp");
        tXMLDataList.addColHead("InitFee");
        tXMLDataList.addColHead("ManageFee");
        tXMLDataList.addColHead("SumPrem1");
        tXMLDataList.buildColHead();
        String str = "select peoples2 from lcgrpcont where grpcontno='" + this.mLCGrpContSchema.getGrpContNo() + "'";
        ExeSQL tExeSQLNewCont = new ExeSQL();
        SSRS tSSRSSNewCont = tExeSQLNewCont.execSQL(str.toString());
        tXMLDataList.setColValue("SumInsureds", tSSRSSNewCont.GetText(1, 1));

        ExeSQL mExeSQLRisk = new ExeSQL();
        SSRS mSSRSRisk = new SSRS();
        String mSqlRisk = "select l.riskcode,m.riskname from LcGrppol l,lmrisk m where l.riskcode=m.riskcode and  GrpContNo = '"
                + this.mLCGrpContSchema.getGrpContNo() + "'";
        mSSRSRisk = mExeSQLRisk.execSQL(mSqlRisk);

        if (mSSRSRisk.GetText(1, 1) != null & mSSRSRisk.GetText(1, 2) != null)
        {
            tXMLDataList.setColValue("RiskCode", mSSRSRisk.GetText(1, 1));
            tXMLDataList.setColValue("RiskName", mSSRSRisk.GetText(1, 2));
        }

        ExeSQL mExeSQLPub = new ExeSQL();
        SSRS mSSRSPub = new SSRS();
        String mSqlPub = "select sum(l.prem) from lcprem l,lmdutypay m where l.payplancode=m.payplancode and m.AccPayClass='3' and grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "'";
        mSSRSPub = mExeSQLPub.execSQL(mSqlPub);
        if (mSSRSPub.GetText(1, 1) != null)
        {
            tXMLDataList.setColValue("PubPrem", mSSRSPub.GetText(1, 1));
        }
        ExeSQL mExeSQLSelf = new ExeSQL();
        SSRS mSSRSSelf = new SSRS();
        String mSqlSelf = "select sum(l.prem) from lcprem l,lmdutypay m where l.payplancode=m.payplancode and m.AccPayClass in('5','6') and grpcontno='"
            + this.mLCGrpContSchema.getGrpContNo() + "'";
        mSSRSSelf = mExeSQLSelf.execSQL(mSqlSelf);
        if (mSSRSSelf.GetText(1, 1) != null)
        {
            tXMLDataList.setColValue("SelfPrem", mSSRSSelf.GetText(1, 1));
        }

        ExeSQL mExeSQLGrp = new ExeSQL();
        SSRS mSSRSGrp = new SSRS();
        String mSqlGrp = "select sum(l.prem) from lcprem l,lmdutypay m where l.payplancode=m.payplancode and m.AccPayClass='4' and grpcontno='"
            + this.mLCGrpContSchema.getGrpContNo() + "'";
        mSSRSGrp = mExeSQLGrp.execSQL(mSqlGrp);
        if (mSSRSGrp.GetText(1, 1) != null)
        {
            tXMLDataList.setColValue("SelfGrp", mSSRSGrp.GetText(1, 1));
        }

        ExeSQL mExeSQLInit = new ExeSQL();
        SSRS mSSRSInit = new SSRS();
        String mSqlInit = "select nvl(sum(fee),0) from lcinsureaccfeetrace where grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "'";
        mSSRSInit = mExeSQLInit.execSQL(mSqlInit);
        if (mSSRSInit.GetText(1, 1) != null)
        {
            tXMLDataList.setColValue("InitFee", mSSRSInit.GetText(1, 1));
        }

        ExeSQL mExeSQLManage = new ExeSQL();
        SSRS mSSRSManage = new SSRS();
        String mSqlManage = "select feevalue from lcgrpfee where feecode in (select feecode from lmriskfee where FeeItemType='03') and grpcontno='"
                + this.mLCGrpContSchema.getGrpContNo() + "'";
        mSSRSManage = mExeSQLManage.execSQL(mSqlManage);
        if (mSSRSManage.GetText(1, 1) != null)
        {
            tXMLDataList.setColValue("ManageFee", mSSRSManage.GetText(1, 1));
        }
        double SumPrem1 = Double.parseDouble(mSSRSGrp.GetText(1, 1)) + Double.parseDouble(mSSRSSelf.GetText(1, 1))
                + Double.parseDouble(mSSRSPub.GetText(1, 1));
        tXMLDataList.setColValue("SumPrem1", String.valueOf(SumPrem1));
        tXMLDataList.insertRow(0);
        tXmlDataset.addDataObject(tXMLDataList);
        return true;
    }

    private boolean getFCRiskZTFee(XMLDataset tXmlDataset)
    {

        String str2 = "SELECT EXTRACTRATE from LCRISKZTFEE where GrpContNo='" + this.mLCGrpContSchema.getGrpContNo()
                + "' order by Beginpolyear";

        ExeSQL tExeSQLScri = new ExeSQL();
        SSRS tSSRSScri = tExeSQLScri.execSQL(str2.toString());
        XMLDataList tXMLDataList = new XMLDataList();
        /*	if (tSSRSScri.getMaxRow() > 0) {
         // 添加退保利率的标签
         tXMLDataList = new XMLDataList();
         // 添加xml一个新的对象LCRiskZTFee
         tXMLDataList.setDataObjectID("LCRiskZTFee");
         // 添加Head单元内的信息
         tXMLDataList.addColHead("RiskZTRate");
         tXMLDataList.buildColHead();
         for (int i = 1; i <= tSSRSScri.getMaxRow(); i++) {
         tXMLDataList.setColValue("RiskZTRate",tSSRSScri.GetText(i, 1));

         tXMLDataList.insertRow(0);
         }
         tXmlDataset.addDataObject(tXMLDataList);
         
         }*/
        if (tSSRSScri.getMaxRow() > 0)
        {
            // 添加退保利率的标签
            //	tXMLDataList = new XMLDataList();
            // 添加xml一个新的对象LCRiskZTFee
            tXMLDataList.setDataObjectID("LCRiskZTFee");
            // 添加Head单元内的信息
            tXMLDataList.addColHead("RiskZTRate1");
            tXMLDataList.addColHead("RiskZTRate2");
            tXMLDataList.addColHead("RiskZTRate3");
            tXMLDataList.addColHead("RiskZTRate4");
            tXMLDataList.addColHead("RiskZTRate5");
            tXMLDataList.addColHead("RiskZTRate6");
            tXMLDataList.buildColHead();
            for (int i = 1; i <= tSSRSScri.getMaxRow(); i++)
            {

                NumberFormat nt = NumberFormat.getPercentInstance();
                nt.setMaximumFractionDigits(6);
                String tRiskZTRate = nt.format(Double.parseDouble(tSSRSScri.GetText(i, 1)));
                if (i == 1)
                {
                    tXMLDataList.setColValue("RiskZTRate1", tRiskZTRate);
                }
                if (i == 2)
                {
                    tXMLDataList.setColValue("RiskZTRate2", tRiskZTRate);
                }
                if (i == 3)
                {
                    tXMLDataList.setColValue("RiskZTRate3", tRiskZTRate);
                }
                if (i == 4)
                {
                    tXMLDataList.setColValue("RiskZTRate4", tRiskZTRate);
                }
                if (i == 5)
                {
                    tXMLDataList.setColValue("RiskZTRate5", tRiskZTRate);
                }
                if (i == 6)
                {
                    tXMLDataList.setColValue("RiskZTRate6", tRiskZTRate);
                }

            }
            tXMLDataList.insertRow(0);
            tXmlDataset.addDataObject(tXMLDataList);
        }
        return true;
    }

    private String cancelKeXueCount(String moneyString)
    {
        DecimalFormat df = new DecimalFormat("#,##0.00");
        BigDecimal b = new BigDecimal(String.valueOf(moneyString));
        return df.format(b.doubleValue());
    }
    
    //start F12打印模板部分节点处理 add by CZ 2016-2-22 
    //F12 住院保障显示部分	HBFund 676009
    private Element addHBFundF12(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode,String tInsuPlanCode) {
    	
		String tParam = null;
		Element tElmDetail = new Element("HBFund");

		Element tGetRate = new Element("GetRate");
		tParam = getValueText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate1");
		tGetRate.setText(tParam);
		Element tGetRateReMark = new Element("GetRateReMark");
		tParam = getRemarkText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate1");
		tGetRateReMark.setText(tParam);
		Element tGetLimit = new Element("GetLimit");
		tParam = getValueText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"LimintAmnt");
		tGetLimit.setText(tParam);
		Element tGetLimitReMark = new Element("GetLimitReMark");
		tParam = getRemarkText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"LimintAmnt");
		tGetLimitReMark.setText(tParam);
		        
		tElmDetail.addContent(tGetRate);
		tElmDetail.addContent(tGetRateReMark);
		tElmDetail.addContent(tGetLimit);
		tElmDetail.addContent(tGetLimitReMark);
       
		return tElmDetail;
	}
    //F12 门急诊保障显示部分 OECFund	676010
    private Element addOECFundF12(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode,String tInsuPlanCode) {
    	
		String tParam = null;
		Element tElmDetail = new Element("OECFund");

		Element tGetRate = new Element("GetRate");
		tParam = getValueText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate1");
		tGetRate.setText(tParam);
		Element tGetRateReMark = new Element("GetRateReMark");
		tParam = getRemarkText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate1");
		tGetRateReMark.setText(tParam);
		Element tGetLimit = new Element("GetLimit");
		tParam = getValueText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"LimintAmnt");
		tGetLimit.setText(tParam);
		Element tGetLimitReMark = new Element("GetLimitReMark");
		tParam = getRemarkText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"LimintAmnt");
		tGetLimitReMark.setText(tParam);
		        
		tElmDetail.addContent(tGetRate);
		tElmDetail.addContent(tGetRateReMark);
		tElmDetail.addContent(tGetLimit);
		tElmDetail.addContent(tGetLimitReMark);
       
		return tElmDetail;
	}
    //F12 紧急医疗运送显示部分 UMEFund 676011
    private Element addUMEFundF12(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode,String tInsuPlanCode) {
    	
		String tParam = null;
		Element tElmDetail = new Element("UMEFund");

		Element tGetRate = new Element("GetRate");
		tParam = getValueText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate1");
		tGetRate.setText(tParam);
		Element tGetRateReMark = new Element("GetRateReMark");
		tParam = getRemarkText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate1");
		tGetRateReMark.setText(tParam);
		Element tGetLimit = new Element("GetLimit");
		tParam = getValueText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"LimintAmnt");
		tGetLimit.setText(tParam);
		Element tGetLimitReMark = new Element("GetLimitReMark");
		tParam = getRemarkText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"LimintAmnt");
		tGetLimitReMark.setText(tParam);
		        
		tElmDetail.addContent(tGetRate);
		tElmDetail.addContent(tGetRateReMark);
		tElmDetail.addContent(tGetLimit);
		tElmDetail.addContent(tGetLimitReMark);
       
		return tElmDetail;
	}
    //F12 牙科保障显示部分	DCFund 676012
    private Element addDCFundF12(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode,String tInsuPlanCode) {
    	
		String tParam = null;
		Element tElmDetail = new Element("DCFund");

		Element tGetRate1 = new Element("GetRate1");
		tParam = getValueText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate1");
		tGetRate1.setText(tParam);
		Element tGetRateReMark1 = new Element("GetRateReMark1");
		tParam = getRemarkText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate1");
		tGetRateReMark1.setText(tParam);
		Element tGetRate2 = new Element("GetRate2");
		tParam = getValueText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate2");
		tGetRate2.setText(tParam);
		Element tGetRateReMark2 = new Element("GetRateReMark2");
		tParam = getRemarkText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate2");
		tGetRateReMark2.setText(tParam);
		Element tGetLimit = new Element("GetLimit");
		tParam = getValueText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"LimintAmnt");
		tGetLimit.setText(tParam);
		Element tGetLimitReMark = new Element("GetLimitReMark");
		tParam = getRemarkText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"LimintAmnt");
		tGetLimitReMark.setText(tParam);
		        
		tElmDetail.addContent(tGetRate1);
		tElmDetail.addContent(tGetRateReMark1);
		tElmDetail.addContent(tGetRate2);
		tElmDetail.addContent(tGetRateReMark2);
		tElmDetail.addContent(tGetLimit);
		tElmDetail.addContent(tGetLimitReMark);
       
		return tElmDetail;
	}
    //F12 生育保障显示部分	MCFund 676014
    private Element addMCFundF12(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode,String tInsuPlanCode) {
    	
		String tParam = null;
		Element tElmDetail = new Element("MCFund");

		Element tGetRate = new Element("GetRate");
		tParam = getValueText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate1");
		tGetRate.setText(tParam);
		Element tGetRateReMark = new Element("GetRateReMark");
		tParam = getRemarkText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate1");
		tGetRateReMark.setText(tParam);
		Element tGetLimit = new Element("GetLimit");
		tParam = getValueText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"LimintAmnt");
		tGetLimit.setText(tParam);
		Element tGetLimitReMark = new Element("GetLimitReMark");
		tParam = getRemarkText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"LimintAmnt");
		tGetLimitReMark.setText(tParam);
		        
		tElmDetail.addContent(tGetRate);
		tElmDetail.addContent(tGetRateReMark);
		tElmDetail.addContent(tGetLimit);
		tElmDetail.addContent(tGetLimitReMark);
       
		return tElmDetail;
	}
    //F12 特殊责任保障显示部分 SDFund 676015
    private Element addSDFundF12(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode,String tInsuPlanCode) {
    	
		String tParam = null;
		Element tElmDetail = new Element("SDFund");

		Element tGetRate = new Element("GetRate");
		tParam = getValueText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate1");
		tGetRate.setText(tParam);
		Element tGetRateReMark = new Element("GetRateReMark");
		tParam = getRemarkText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate1");
		tGetRateReMark.setText(tParam);
		Element tGetLimit = new Element("GetLimit");
		tParam = getValueText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"LimintAmnt");
		tGetLimit.setText(tParam);
		Element tGetLimitReMark = new Element("GetLimitReMark");
		tParam = getRemarkText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"LimintAmnt");
		tGetLimitReMark.setText(tParam);
		        
		tElmDetail.addContent(tGetRate);
		tElmDetail.addContent(tGetRateReMark);
		tElmDetail.addContent(tGetLimit);
		tElmDetail.addContent(tGetLimitReMark);
       
		return tElmDetail;
	}
    //F12 中医治疗显示部分	DTFund 676016
    private Element addDTFundF12(String tGrpContNo, String tRiskCode,
			String tPlanCode, String tDutyCode,String tInsuPlanCode) {
    	
		String tParam = null;
		Element tElmDetail = new Element("DTFund");

		Element tGetRate = new Element("GetRate");
		tParam = getValueText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate1");
		tGetRate.setText(tParam);
		Element tGetRateReMark = new Element("GetRateReMark");
		tParam = getRemarkText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"GetRate1");
		tGetRateReMark.setText(tParam);
		Element tGetLimit = new Element("GetLimit");
		tParam = getValueText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"LimintAmnt");
		tGetLimit.setText(tParam);
		Element tGetLimitReMark = new Element("GetLimitReMark");
		tParam = getRemarkText(tGrpContNo,tRiskCode,tPlanCode,tDutyCode,"LimintAmnt");
		tGetLimitReMark.setText(tParam);
		        
		tElmDetail.addContent(tGetRate);
		tElmDetail.addContent(tGetRateReMark);
		tElmDetail.addContent(tGetLimit);
		tElmDetail.addContent(tGetLimitReMark);
       
		return tElmDetail;
	}
    
    /**
	 * 获取责任下要素值
	 * @param tGrpContNo
	 * @param tRiskCode
	 * @param tPlanCode
	 * @param tDutyCode
	 * @param tParam
	 * @return 
	 */
	private String getValueText(String tGrpContNo, String tRiskCode, String tPlanCode,
		String tDutyCode, String tParam) {
		StringBuffer tParamSQL = new StringBuffer();
		tParamSQL.append(" select distinct trim(calfactorvalue)  from lccontplandutyparam " +
        				 " where grpcontno = '"+tGrpContNo+"' and " +
        				 " riskcode = '"+tRiskCode+"' " +
        				 " and contplancode = '"+tPlanCode+"' " +
        				 " and dutycode = '"+tDutyCode+"' " +
        				 " and calfactor = '"+tParam+"' ");
        ExeSQL tEParamSQL = new ExeSQL();
        SSRS tSSRParam = tEParamSQL.execSQL(tParamSQL.toString());
        if (tSSRParam.getMaxRow() > 0){
        	if(tSSRParam.GetText(1, 1)!=null&&!"".equals(tSSRParam.GetText(1, 1))){
        		return tSSRParam.GetText(1, 1);        		
        	}else{
        		return "--";
        	}        		
        }else{
        	return "--";
        }
	}
	/**
	 * 获取责任下要素值
	 * @param tGrpContNo
	 * @param tRiskCode
	 * @param tPlanCode
	 * @param tDutyCode
	 * @param tParam
	 * @return 
	 */
	private String getRemarkText(String tGrpContNo, String tRiskCode, String tPlanCode,
		String tDutyCode, String tParam) {
		StringBuffer tParamSQL = new StringBuffer();
		tParamSQL.append(" select distinct trim(remark) from lccontplandutyparam " +
        				 " where grpcontno = '"+tGrpContNo+"' and " +
        				 " riskcode = '"+tRiskCode+"' " +
        				 " and contplancode = '"+tPlanCode+"' " +
        				 " and dutycode = '"+tDutyCode+"' " +
        				 " and calfactor = '"+tParam+"' ");
        ExeSQL tEParamSQL = new ExeSQL();
        SSRS tSSRParam = tEParamSQL.execSQL(tParamSQL.toString());
        if (tSSRParam.getMaxRow() > 0){
        	if(tSSRParam.GetText(1, 1)!=null&&!"".equals(tSSRParam.GetText(1, 1))){
        		return tSSRParam.GetText(1, 1);        		
        	}else{
        		return "--";
        	}        		
        }else{
        	return "--";
        }
	}//end F12打印模板部分节点处理 
    
    public static void main(String[] args)
    {
    	LCJXGrpContF1PBL bl = new LCJXGrpContF1PBL();
        bl.mGlobalInput.ComCode = "86";
        bl.mGlobalInput.ManageCom = bl.mGlobalInput.ComCode;
        bl.mGlobalInput.Operator = "group";
        bl.mTemplatePath = "D:\\vssroot\\ui\\f1print\\template\\";
        bl.mOutXmlPath = "F:";

        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo("00106766000003");
        tLCGrpContDB.getInfo();
        bl.mLCGrpContSchema = tLCGrpContDB.getSchema();

        VData aInputData = new VData();
        aInputData.add(bl.mGlobalInput);
        aInputData.add(bl.mLCGrpContSchema);
        aInputData.add(bl.mTemplatePath);
        aInputData.add(bl.mOutXmlPath);
        aInputData.add("1");
        aInputData.add("1");

        if (!bl.submitData(aInputData, "PRINT"))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
