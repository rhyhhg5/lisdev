package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author 刘岩松
 * @version 1.0
 * @date 2003-04-04
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ShowXQPremSuccUI
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    private String strStartDate = ""; //开始日期
    private String strEndDate = ""; //结束日期
    private String strAgentState = ""; //业务员的状态(1为在职单，0为孤儿单)
    private String strPremType = ""; //首续期的标志
    private String strFlag = ""; //S or F(S为银行代收，F为银行代付)
    private String strComCode = ""; //系统登陆的机构(查询银行日志表)
    private String strStation = ""; //界面上录入的管理机构
    private GlobalInput mG = new GlobalInput();
    public ShowXQPremSuccUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
//      if( !cOperate.equals("SHOW") )
//      {
//        buildError("submitData", "不支持的操作字符串");
//        return false;
//      }
//      if( !getInputData(cInputData) )
//      {
//        return false;
//      }


//VData vData = new VData();
//      if( !prepareOutputData(vData) )
//      {
//        return false;
//      }


            ShowXQPremSuccBL tShowXQPremSuccBL = new ShowXQPremSuccBL();
            System.out.println("Start XQPremBankSuccUI Submit ...");

            if (!tShowXQPremSuccBL.submitData(cInputData, cOperate))
            {
                if (tShowXQPremSuccBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tShowXQPremSuccBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData",
                               "FinChargeDayModeF1PBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tShowXQPremSuccBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "PLPsqsUI";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
    }

    public VData getResult()
    {
        return mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FinChargeDayModeF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

//  public boolean getInputData(VData tInputData)
//  {
//    strStartDate = (String)tInputData.get(0);
//    strEndDate = (String)tInputData.get(1);
//    strAgentState = (String)tInputData.get(2);
//    strPremType = (String)tInputData.get(3);//首期还是续期
//    strFlag = (String)tInputData.get(4);//F or S
//    strStation = (String)tInputData.get(5);//界面上录入的管理机构
//    System.out.println("111");
//    mG.setSchema((GlobalInput)tInputData.getObjectByObjectName("GlobalInput",0));
//    System.out.println("2");
//    strComCode=mG.ManageCom.trim();
//    System.out.println("strComCode"+strComCode);
//    return true;
//  }

//  private boolean prepareOutputData(VData vData)
//  {
//    vData.clear();
//    vData.addElement(strStartDate);
//    vData.addElement(strEndDate);
//    vData.addElement(strAgentState);
//    vData.addElement(strPremType);
//    vData.addElement(strFlag);
//    vData.addElement(strComCode);
//    vData.addElement(strStation);
//    return true;
//  }
//

    public static void main(String[] args)
    {
//    String strStartDate="2004-4-1";                //开始日期
//    String strEndDate="2004-6-30";                  //结束日期
//    String strAgentState = "1";             //业务员的状态(1为在职单，0为孤儿单)
//    String strPremType = "X";               //首续期的标志
//    String strFlag = "S";                   //S or F(S为银行代收，F为银行代付)
//    String strStation = "8611";
//    GlobalInput tG = new GlobalInput();
//    tG.Operator="001";
//    tG.ManageCom="86";
//
//    VData tVData = new VData();
//    tVData.addElement(strStartDate);
//    tVData.addElement(strEndDate);
//    tVData.addElement(strAgentState);
//    tVData.addElement(strPremType);
//    tVData.addElement(strFlag);
//    tVData.addElement(strStation);
//    tVData.addElement(tG);
//
//    ShowXQPremSuccUI tShowXQPremSuccUI = new ShowXQPremSuccUI();
//    if(tShowXQPremSuccUI.submitData(tVData,"SHOW"))
//    {
//      System.out.println("执行完毕");
//    }
    }
}