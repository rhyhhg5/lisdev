package com.sinosoft.lis.f1print;

/**
 * X10-再保月结单（总公司1000）
 * @date 2013-09-23
 * @author yan
 */
import java.text.DecimalFormat;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class ReinsureMonthParentExcelBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	private String sWageNo = ""; 
	private GlobalInput mGlobalInput = new GlobalInput(); // 全局变量
	private String tOutXmlPath = "";
	private String msql = "";
	public ReinsureMonthParentExcelBL() {
	}
	public static void main(String[] args) {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		
		System.out.println("进入 ReinsureMonthParentExcelBL...X10");
		if (!cOperate.equals("PRINT")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}
		if (!getInputData(cInputData)) {
			return false;
		}
		if (cOperate.equals("PRINT")) { // 打印提数
			if (!getPrintData()) {
				return false;
			}
		}
		return true;
	}

	
	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * @return  
	 */
	private boolean getInputData(VData cInputData) {
		sWageNo = (String) cInputData.get(0);// 获得WageNo
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		tOutXmlPath = (String)cInputData.get(2);
		
		System.out.println("打印日期-->"+sWageNo+";tOutXmlPath-->"+tOutXmlPath);
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		if (tOutXmlPath == null || tOutXmlPath.equals("")) {
			buildError("getInputData", "没有得到文件路径的信息！");
			return false;
		}
		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "ReinsureMonthParentExcelBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
    
    /**
     * 获得结果集的记录总数
     * @return
     */
    private int getNumber(){

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
      
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
        tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
        tGetSQLFromXML.setParameters("BeginDate", sWageNo);

        String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
      //tServerPath = "E:/lisdev/ui/";
        
        int no=7;   //初始化打印行数

        // 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
        String[] getTypes = new String[] { 
                "ZBYSYF",   //  X10再保月结单（总公司1000）再保月结单-应收-应付分保账款
                "ZBYSYFZ",  // X10再保月结单（总公司1000）再保月结单-再保应收应付合计
        };
        
        for (int i = 0; i < getTypes.length; i++) {
            msql = tGetSQLFromXML.getSql(tServerPath + "f1print/picctemplate/FeePrintSql.xml", getTypes[i]);
            tSSRS = tExeSQL.execSQL(msql);
            if(getTypes[i].equals("ZBYSYF")){
                no=no+tSSRS.MaxRow+2;
            }else if(getTypes[i].equals("ZBYSYFZ")){
                if(tSSRS.MaxRow == 0){
                    no = no+1;
                }else{
                    no=no+tSSRS.MaxRow;
                }   
            }
        }    
        System.out.println("tMaxRowNum:" + no);
        
        return no;
    }

	/**
	 * 新的打印数据方法
	 * @return
	 */
	private boolean getPrintData() {
		
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();

		GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();
		tGetSQLFromXML.setParameters("ManageCom", mGlobalInput.ManageCom);
		tGetSQLFromXML.setParameters("BeginDate", sWageNo);

		String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
		//tServerPath = "E:/lisdev/ui/";
		String nsql = "select Name from LDCom where ComCode='" + mGlobalInput.ManageCom + "'"; // 管理机构
		tSSRS = tExeSQL.execSQL(nsql);
		String manageCom = tSSRS.GetText(1, 1);
		
		int tMaxRowNum = getNumber();
        
        String[][] mToExcel = new String[tMaxRowNum+10][13];
		mToExcel[1][0] = "再保月结单";
		mToExcel[2][0] = "机构："+manageCom;
		mToExcel[2][3] = "月度："+sWageNo;
		mToExcel[4][0] = "再保公司";
		mToExcel[4][1] = "应收分保账款";
		mToExcel[4][7] = "应付分保账款";
		mToExcel[5][1] = "合同";
		mToExcel[5][3] = "临时";
		mToExcel[5][5] = "法定";
		mToExcel[5][7] = "合同";
		mToExcel[5][9] = "临时";
		mToExcel[5][11] = "法定";
		mToExcel[6][1] = "比例";
		mToExcel[6][2] = "非比例";
		mToExcel[6][3] = "比例";
		mToExcel[6][4] = "非比例";
		mToExcel[6][5] = "比例";
		mToExcel[6][6] = "非比例";
		mToExcel[6][7] = "比例";
		mToExcel[6][8] = "非比例";
		mToExcel[6][9] = "比例";
		mToExcel[6][10] = "非比例";
		mToExcel[6][11] = "比例";
		mToExcel[6][12] = "非比例";
		
		int no=7;	//初始化打印行数

		// 要提取的类型，对应FeePrintSql.xml里的SQL语句节点。
		String[] getTypes = new String[] { 
				"ZBYSYF",	//  X10再保月结单（总公司1000）再保月结单-应收-应付分保账款
				"ZBYSYFZ",	// X10再保月结单（总公司1000）再保月结单-再保应收应付合计
		};
		
		for (int i = 0; i < getTypes.length; i++) {
			msql = tGetSQLFromXML.getSql(tServerPath + "f1print/picctemplate/FeePrintSql.xml", getTypes[i]);
			tSSRS = tExeSQL.execSQL(msql);
			if(getTypes[i].equals("ZBYSYF")){
				for(int row = 1; row <= tSSRS.MaxRow; row++){
					mToExcel[no+row-1][0] = tSSRS.GetText(row, 1);
					if((!tSSRS.GetText(row, 2).equals(""))&&(!tSSRS.GetText(row, 2).equals("null"))&&(tSSRS.GetText(row, 2)==null)){
						mToExcel[no+row-1][1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 2)));
					}
					if((!tSSRS.GetText(row, 3).equals(""))&&(!tSSRS.GetText(row, 3).equals("null"))&&(tSSRS.GetText(row, 3)==null)){
						mToExcel[no+row-1][7] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 2)));
					}
				}
				no=no+tSSRS.MaxRow+2;
			}else if(getTypes[i].equals("ZBYSYFZ")){
				if(tSSRS.MaxRow == 0){
					mToExcel[no][0] = "合计";
					mToExcel[no][1] = "0.00";
					mToExcel[no][7] = "0.00";
					no = no+1;
				}else{
					for(int row = 1; row <= tSSRS.MaxRow; row++){
						mToExcel[no+row-1][0] = tSSRS.GetText(row, 1);
						if((!tSSRS.GetText(row, 2).equals(""))&&(!tSSRS.GetText(row, 2).equals("null"))&&(tSSRS.GetText(row, 2)==null)){
							mToExcel[no+row-1][1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 2)));
						}else{
							mToExcel[no+row-1][1] = "0.00";
						}
						if((!tSSRS.GetText(row, 3).equals(""))&&(!tSSRS.GetText(row, 3).equals("null"))&&(tSSRS.GetText(row, 3)==null)){
							mToExcel[no+row-1][7] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(row, 2)));
						}else{
							mToExcel[no+row-1][7] = "0.00";
						}
					}
					no=no+tSSRS.MaxRow;
				}	
			}
		}

		// 往来余额 提取 从xml文件中要提取的SQL的节点 
		String[] ZBYJWLYESQLNode = new String[]{
				"ZBYSYFWLYE",//  X10再保月结单（总公司1000）- 往来余额
		};
		
		// 提取数据的SQL语句执行结果只有1个汇总金额，所以都取的是第1行第1列，故写在一起。
		for(int i=0; i<ZBYJWLYESQLNode.length; i++){
			msql  = tGetSQLFromXML.getSql(tServerPath+ "f1print/picctemplate/FeePrintSql.xml",ZBYJWLYESQLNode[i]);
			tSSRS = tExeSQL.execSQL(msql);
			if(tSSRS.MaxRow == 0){
				mToExcel[no][0] = "往来余额";
				mToExcel[no][1] = "0.00";
			}else{
				mToExcel[no][0] = "往来余额";
				if((!tSSRS.GetText(1, 1).equals(""))&&(!tSSRS.GetText(1, 1).equals("null"))&&(tSSRS.GetText(1, 1)!= null)){
					mToExcel[no][1] = new DecimalFormat("0.00").format(Double.valueOf(tSSRS.GetText(1, 1)));
				}else{
					mToExcel[no][1] = "0.00";
				}
			}
		}
		try{
			System.out.println("X10--tOutXmlPath:"+tOutXmlPath);
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = {PubFun.getCurrentDate()};
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(tOutXmlPath);
			System.out.println("生成X10文件完成");
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			buildError("getPrintData", e.toString());
		}
		return true;
	}

}
