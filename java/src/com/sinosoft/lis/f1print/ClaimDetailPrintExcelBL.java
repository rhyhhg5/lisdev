package com.sinosoft.lis.f1print;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimDetailSchema;
import com.sinosoft.lis.schema.LLClaimSchema;
import com.sinosoft.lis.schema.LLFeeMainSchema;
import com.sinosoft.lis.vschema.LLCaseReceiptSet;
import com.sinosoft.lis.vschema.LLClaimDetailSet;
import com.sinosoft.lis.vschema.LLCaseRelaSet;
import com.sinosoft.lis.vschema.LLClaimSet;
import com.sinosoft.lis.vschema.LLFeeMainSet;
import com.sinosoft.lis.vschema.LLSecurityReceiptSet;
import com.sinosoft.lis.vschema.LLCaseCureSet;
import com.sinosoft.lis.schema.LLSecurityReceiptSchema;
import com.sinosoft.lis.vschema.LLCaseDrugSet;
import com.sinosoft.lis.schema.LLCaseDrugSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LLAppealSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.io.InputStream;
import java.text.DecimalFormat;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.llcase.LLCaseCommon;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 理赔给付细目表（Excel）打印类</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author：Xx
 * @version：1.0
 */
public class ClaimDetailPrintExcelBL {
    public CErrors mErrors = new CErrors();
    /** 前台传入的封装数据，本次未使用*/
    private VData mInputData;
    private VData mResult = new VData();
    /** 前台传入的操作符，本次未使用*/
    private String mOperate;
    
    /** 数据封装集合 */
    private TransferData mTransferData = new TransferData();

    /** xml生成工具 */
    private XmlExport xml = new XmlExport();
    /** 赔案记录 */
    private LLClaimSchema mLLClaimSchema = new LLClaimSchema();
    /** 理赔号 */
    private String mCaseNo;
    /** 赔案号 */
    private String mClmNo;
    /** 立案号 */
    private String mRgtNo;
    private String mRgtDate;
    /** 案件类型 */
    private String mRgtClass;
    /** 特别提示栏 */
    private String mRemarkDesc = "　";
    /** 案件事件关联号 */
    private String mCaseRelaNo;
    /** 被保人姓名 */
    private String mCustomerName;
    /** 备注信息-第一行逻辑数组 */
    private int[] mremarktag = {0, 0, 0, 0, 0};
    /** 备注信息-第一行内容 */
    private String mremark = "";
    /** 低段责任金额 */
    private double lowamnt = 0;
    /** 中段责任金额 */
    private double midamnt = 0;
    /** 高段一责任金额 */
    private double highamnt1 = 0;
    /** 高段二责任金额 */
    private double highamnt2 = 0;
    /** 超高段责任金额 */
    private double supinhosamnt = 0;
    /** 大额门诊责任金额 */
    private double supdooramnt = 0;
    /** 门急诊费用 */
    private double emergdooramnt = 0;
    /** 小额门诊费用 */
    private double smalldooramnt = 0; 
    /**被保人客户号*/
    private String mCustomerIDNo;
    /** 赔付明细信息 */
    private LLClaimDetailSet mLLClaimDetailSet = new LLClaimDetailSet();
    /** 帐单信息 */
    private LLFeeMainSet mLLFeeMainSet = new LLFeeMainSet();
    private LLFeeMainDB mLLFeeMainDB = new LLFeeMainDB();
    private LLFeeMainSchema mLLFeeMainSchema = new LLFeeMainSchema();
    /** 帐单费用明细信息 */
    private LLCaseReceiptSet mLLCaseReceiptSet = new LLCaseReceiptSet();
    private LLSecurityReceiptSet mLLSecurityReceiptSet = new
            LLSecurityReceiptSet();
    /** 系统全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 执行SQL的容器 */
    ExeSQL tExeSQL = new ExeSQL();
    /** 存放查询结果 */
    SSRS tSSRS = new SSRS();
    /** 案件受理机构 */
    private String MngCom = "";
    /** 被保险人年龄 */
    String mAge = "";
    /** 投保单位 */
    String mGrpName = "";
    /** 立案人/申请人地址 */
    String mRgtantAddress = "";
    /** 批次打印标记,excel打印并未使用 */
    private String mflag = null;
    /** 团体批次导入标记 */
    private String applyertype = "";
    
    /**分类自负*/
    private double selfpay2 = 0;
    /**自负*/
    private double selfamnt = 0;
    
    /** 批次导入的‘扣除明细’ */
    private String mNewRemark = "";
    
    public ClaimDetailPrintExcelBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * @param: cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mInputData = (VData) cInputData.clone();
        System.out.println("mInputData:"+mInputData.toString());
        mOperate = cOperate;
        System.out.println("mOperate:"+mOperate);
        mflag = cOperate;
        System.out.println("mflag:"+mflag);
        
        //从前端获取数据
        if (!getInputData(cInputData)) {
            return false;
        }

        //从数据库获得数据
        if (!getBaseData()) {
            return false;
        }

        //准备需要打印的数据
        if (!preparePrintData()) {
            return false;
        }

        //准备打印管理表数据
        if (!dealPrintMag()) {
            return false;
        }

        return true;
    }

    private boolean getBaseData() {
        //查询赔案信息
        LLClaimSet tLLClaimSet = new LLClaimSet();
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setCaseNo(mCaseNo);
        tLLClaimSet.set(tLLClaimDB.query());
        if (tLLClaimDB.mErrors.needDealError() == true ||
            tLLClaimSet == null || tLLClaimSet.size() == 0) {
            CError.buildErr(this, "赔案信息查询失败");
            return false;
        }

        mLLClaimSchema.setSchema(tLLClaimSet.get(1));
        mClmNo = mLLClaimSchema.getClmNo();
        mRgtNo = mLLClaimSchema.getRgtNo();

        //查询立案信息
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if (!tLLRegisterDB.getInfo()) {
            CError.buildErr(this, "立案信息查询失败");
            return false;
        }
        mRgtClass = tLLRegisterDB.getRgtClass();
        System.out.println("案件类型："+mRgtClass);
        mGrpName += tLLRegisterDB.getGrpName();
        mRgtantAddress += tLLRegisterDB.getRgtantAddress();
        applyertype= tLLRegisterDB.getApplyerType();
        
        //查询分案信息
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mCaseNo);
        if (!tLLCaseDB.getInfo()) {
            CError.buildErr(this, "案件信息查询失败");
            return false;
        }
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setSchema(tLLCaseDB.getSchema());
        MngCom = tLLCaseSchema.getMngCom();
        mCustomerName = tLLCaseSchema.getCustomerName();
        mCustomerIDNo = tLLCaseSchema.getIDNo();
        mRgtDate = tLLCaseSchema.getRgtDate();
        mAge = String.valueOf(tLLCaseSchema.getCustomerAge());
        if(!"".equals(tLLCaseSchema.getRemark()) && tLLCaseSchema.getRemark()!=null && 
        		!"null".equals(tLLCaseSchema.getRemark())){
        	mNewRemark = tLLCaseSchema.getRemark();
        }
        
        //查询赔付明细信息
        LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
        tLLClaimDetailDB.setClmNo(mClmNo);
        tLLClaimDetailDB.setCaseRelaNo(mCaseRelaNo);
        mLLClaimDetailSet.set(tLLClaimDetailDB.query());
        if (tLLClaimDetailDB.mErrors.needDealError() == true) {
            CError.buildErr(this, "赔付明细信息查询失败");
            return false;
        }

        //查询帐单信息
        LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
        tLLFeeMainDB.setCaseNo(mCaseNo);
        tLLFeeMainDB.setCaseRelaNo(mCaseRelaNo);
        mLLFeeMainSet.set(tLLFeeMainDB.query());
        if (tLLFeeMainDB.mErrors.needDealError() == true) {
            CError.buildErr(this, "帐单信息查询失败");
            return false;
        }
        
        //社保手工报销明细
        LLSecurityReceiptDB tLLSecurityReceiptDB = new LLSecurityReceiptDB();
        tLLSecurityReceiptDB.setCaseNo(mCaseNo);
        String sql = "";
        if (mCaseRelaNo != null && !mCaseRelaNo.equals("") &&
            !mCaseRelaNo.equals("null")) {
            sql = "select * from LLSecurityReceipt where mainfeeno in " +
                  "(select mainfeeno from llfeemain where caseno='" + mCaseNo +
                  "' and caserelano='" + mCaseRelaNo + "')";
        } else {
            sql = "select * from LLSecurityReceipt where mainfeeno in " +
                  "(select mainfeeno from llfeemain where caseno='" + mCaseNo +
                  "')";
        }
        mLLSecurityReceiptSet.set(tLLSecurityReceiptDB.executeQuery(sql));

        //查询帐单费用（分案收据）明细信息
        LLCaseReceiptDB tLLCaseReceiptDB = new LLCaseReceiptDB();
        tLLCaseReceiptDB.setCaseNo(mCaseNo);
        mLLCaseReceiptSet.set(tLLCaseReceiptDB.query());
        if (tLLCaseReceiptDB.mErrors.needDealError() == true) {
            CError.buildErr(this, "帐单费用明细信息查询失败");
            return false;
        }

        return true;
    }

    private boolean preparePrintData() {
        System.out.println("开始打印了");
        String Age = "　";//年龄
        String insuredstat = "　　";//被保人状态
        String titleInpatient = "　";//（社保）发生费用时间
        String FeeDate = "　";//账单日期
        String SubDate = "";//出险事件信息，发生时间
        String InpatientDate = "　";//住院起始日期
        String titledays = "　";//（社保）持续天数
        String days = "　";//实际住院天数
        String hospitalname = "";//医院名称
        String hosgrade = "";//医院级别
        String feeatti = "　";//医院属性
        LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
        LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
        LLCaseCureDB tLLCaseCureDB = new LLCaseCureDB();
        LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
        tLLCaseCureDB.setCaseNo(mCaseNo);
        tLLCaseCureDB.setCaseRelaNo(mCaseRelaNo);
        tLLCaseCureSet.set(tLLCaseCureDB.query());

        //申诉、纠错处理
        boolean tAppealFlag = false;
        LLAppealDB tLLAppealDB = new LLAppealDB();
        LLAppealSet tLLAppealSet = new LLAppealSet();
        String tOrigCaseNo = "";

        tLLAppealDB.setCaseNo(mCaseNo);
        tLLAppealSet = tLLAppealDB.query();
        if (tLLAppealSet.size() > 0) {
            tOrigCaseNo = mCaseNo;
            mCaseNo = tLLAppealSet.get(1).getAppealNo();
            LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
            tLLCaseRelaDB.setCaseNo(mCaseNo);
            mCaseRelaNo = tLLCaseRelaDB.query().get(1).getCaseRelaNo();
            tAppealFlag = true;
        }

        if (mCaseNo.substring(0, 1).equals("R") ||
            mCaseNo.substring(0, 1).equals("S")) {
            tLLAppealDB = new LLAppealDB();
            tLLAppealDB.setAppealNo(mCaseNo);
            tLLAppealSet = tLLAppealDB.query();
            tOrigCaseNo = tLLAppealSet.get(1).getCaseNo();
            tAppealFlag = true;
        }

        int count_1 = mLLClaimDetailSet.size();
        int count_2 = mLLFeeMainSet.size();
        ListTable tEndListTable = new ListTable();
        tEndListTable.setName("End");

        if (count_2 > 0) {
            Age = String.valueOf(mLLFeeMainSet.get(1).getAge());
        }
        if (Age.equals("0")) {

            Age = mAge;
        }
        if (count_2 > 0) {
            if (mLLFeeMainSet.get(1).getInsuredStat() != null) {
                if (mLLFeeMainSet.get(1).getInsuredStat().equals("1")) {
                    insuredstat = "在职";
                }
                if (mLLFeeMainSet.get(1).getInsuredStat().equals("2")) {
                    insuredstat = "退休";
                }
            }
        }
        if (insuredstat.equals("")) {
            String sql1 = "select insuredstat from lcinsured where insuredno=(select customerno from llcase where caseno='" +
                          mCaseNo + "')";
            String tinsuredstat = tExeSQL.getOneValue(sql1);
            if (tinsuredstat.equals("1")) {
                insuredstat = "在职";
            }
            if (tinsuredstat.equals("2")) {
                insuredstat = "退休";
            }
        }
        if (count_2 > 0) {
            FeeDate = mLLFeeMainSet.get(1).getFeeDate();
        }
        LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
        LLCaseRelaSet tLLCaseRelaSet = new LLCaseRelaSet();
        tLLCaseRelaDB.setCaseNo(mCaseNo);
        tLLCaseRelaDB.setCaseRelaNo(mCaseRelaNo);
        tLLCaseRelaSet.set(tLLCaseRelaDB.query());

        String sql2 = "select accdate from llsubreport where subrptno='" +
                      tLLCaseRelaSet.get(1).getSubRptNo() + "'";
        SubDate = tExeSQL.getOneValue(sql2);
        
        //#1077 
        //add by GY 2013-1-24
        //1.查询案件号llclaimdetail表的contno字段
        String CaseNoSQL="select contno from llclaimdetail where caseno='"+mCaseNo+"'";
        String ContNo=tExeSQL.getOneValue(CaseNoSQL);
        //2.查询contno是否为卡折
        String CheckCardSQL="(select 1 from lcgrpcont where grpcontno = " +
        		"(select grpcontno from lccont where contno = '"+ContNo+"') and cardflag = '2')";
        String CheckCard=tExeSQL.getOneValue(CheckCardSQL);

        if (count_2 > 0) {
            hospitalname = mLLFeeMainSet.get(1).getHospitalName();
            InpatientDate = mLLFeeMainSet.get(1).getHospStartDate();
            days = String.valueOf(mLLFeeMainSet.get(1).getRealHospDate());
        } else {
            if (tLLCaseCureSet.size() > 0) {
                if (tLLCaseCureSet.get(1).getHospitalName() != null) {
                    hospitalname = tLLCaseCureSet.get(1).getHospitalName();
                }
                if (tLLCaseCureSet.get(1).getDiagnoseDate() != null) {
                    InpatientDate = tLLCaseCureSet.get(1).getDiagnoseDate();
                }
            }

        }
        if (count_2 > 0) {

            String CaseRela = "";

            if (!(mCaseRelaNo.equals("") || mCaseRelaNo == null)) {
                CaseRela = " and caserelano='" + mCaseRelaNo + "'";
            }
            String sqlx1 =
                "select distinct FeeAtti from llfeemain where caseno='" +
                mCaseNo + "' " + CaseRela;
            tSSRS = tExeSQL.execSQL(sqlx1);

            for (int i = 1; i <= tSSRS.MaxRow; i++) {
                if (tSSRS.GetText(i, 1).equals("0")) {
                    feeatti += "  医院  ";
                }
                if (tSSRS.GetText(i, 1).equals("1")) {
                    feeatti += "  社保结算  ";
                }
                if (tSSRS.GetText(i, 1).equals("2")) {
                    feeatti += "  手工报销  ";
                }
                if (tSSRS.GetText(i, 1).equals("3")) {
                    feeatti += "  社保补充  ";
                }
            }

            if ("1".equals(mLLFeeMainSet.get(1).getFeeType())) {
                titleInpatient = "（社保）发生费用时间";
                titledays = "连续天数";
            } else {
                titleInpatient = "入院时间";
                titledays = "住院天数";
            }
        }
        String sql = "";
        if (count_2 > 0) {
            if (mLLFeeMainSet.get(1).getHosGrade() != null &&
                !mLLFeeMainSet.get(1).getHosGrade().equals("")
                    ) {
                sql =
                        "select codename from ldcode where codetype='levelcode' and code ='" +
                        mLLFeeMainSet.get(1).getHosGrade().substring(0, 1) +
                        "'";
                ExeSQL exesql = new ExeSQL();
                hosgrade = exesql.getOneValue(sql);
            } else {

            }
        } else {
            if (tLLCaseCureSet.size() > 0) {
                if (tLLCaseCureSet.get(1).getHospitalCode() != null) {
                    String sql3 = "select codename from ldcode where codetype='levelcode' and code =(select substr(levelcode,1,1) from ldhospital where HospitCode='" +
                                  tLLCaseCureSet.get(1).getHospitalCode() +
                                  "')";
                    hosgrade = tExeSQL.getOneValue(sql3);
                }
            }
        }

        int count = count_1;
        double selfpay1 = 0;
        double planfee = 0;
        double FeeInSecurity = 0;
        double getlimit = 0;
        double supInHosFee = 0;
        if (mLLSecurityReceiptSet != null && mLLSecurityReceiptSet.size() > 0 &&
            !tAppealFlag) {
            getlimit = mLLSecurityReceiptSet.get(1).getGetLimit();
            if (mLLSecurityReceiptSet.size() > 0) {
                int SRcount = mLLSecurityReceiptSet.size();
                for (int i = 1; i <= SRcount; i++) {
                    LLSecurityReceiptSchema tLLSecurityReceiptSchema = new
                            LLSecurityReceiptSchema();
                    tLLSecurityReceiptSchema = mLLSecurityReceiptSet.get(i);
                    selfpay1 += tLLSecurityReceiptSchema.getSelfPay1();
                    selfpay2 += tLLSecurityReceiptSchema.getSelfPay2();
                    selfamnt += tLLSecurityReceiptSchema.getSelfAmnt();
                    planfee += tLLSecurityReceiptSchema.getPlanFee();
                    FeeInSecurity += tLLSecurityReceiptSchema.getFeeInSecu();
                    lowamnt += tLLSecurityReceiptSchema.getLowAmnt();
                    midamnt += tLLSecurityReceiptSchema.getMidAmnt();
                    highamnt1 += tLLSecurityReceiptSchema.getHighAmnt1();
                    highamnt2 += tLLSecurityReceiptSchema.getHighAmnt2();
                    supinhosamnt += tLLSecurityReceiptSchema.getSuperAmnt();
                    smalldooramnt += tLLSecurityReceiptSchema.getSmallDoorPay();
                    emergdooramnt += tLLSecurityReceiptSchema.getEmergencyPay();
                    supdooramnt += tLLSecurityReceiptSchema.getHighDoorAmnt();
                    supInHosFee += tLLSecurityReceiptSchema.getSupInHosFee();
                }
            }
        }
        xml = new XmlExport();
        //此处其实只有一个模板，其他都是唬人的
        if (mLLClaimDetailSet.get(1).getRiskCode().substring(0,
                4).equals("1603")) {
            System.out.println("mLLFeeMainSet:" + mLLFeeMainSet.size());
            if (!MngCom.equals("86310000") ||
                (mLLFeeMainSet.size() != 0 &&
                 !"1".equals(mLLFeeMainSet.get(1).getFeeType()))) {
                System.out.println("上海案件-1603险种");
                xml.createDocuments("ClaimDetailPrintExcel.htm",
                                           mGlobalInput); //初始化xml文档
            } else {
                System.out.println("其他案件-1603险种");                   
                xml.createDocuments("ClaimDetailPrintExcel.htm",
                                           mGlobalInput); //初始化xml文档
            }
        } else {
            System.out.println("其他案件");
            xml.createDocuments("ClaimDetailPrintExcel.htm", mGlobalInput); //初始化xml文档
        }
        System.out.println("*.htm文件加载完毕");
        
        String[] tRiskListTitle = new String[2];
        tRiskListTitle[0] = "ContNo"; //责任项目
        tRiskListTitle[1] = "RiskName"; //帐单日期
        ListTable tRiskListTable = new ListTable();
        tRiskListTable.setName("RiskInfo");
        String strLine0[] = null;
        String sqlw =
                "select distinct grpcontno,contno from llclaimdetail where caseno = '" +
                mCaseNo + "'";
        ExeSQL exesql1 = new ExeSQL();
        SSRS resu = exesql1.execSQL(sqlw);
        if (resu != null) {
            for (int j = 1; j <= resu.getMaxRow(); j++) {
                strLine0 = new String[2];
                if (resu.GetText(j, 1).equals("00000000000000000000")) {
                    strLine0[0] = resu.GetText(j, 2);
                } else {
                    strLine0[0] = resu.GetText(j, 1);
                }
                sql =
                        "select distinct B.RiskName from llclaimdetail A,lmrisk B "
                        + " where B.Riskcode =A.riskcode and caseno = '" +
                        mCaseNo
                        + "' and contno = '" + resu.GetText(j, 2) + "'";
                SSRS risktb = exesql1.execSQL(sql);
                String RiskName = "";
                if (resu != null) {
                    for (int k = 1; k <= risktb.getMaxRow(); k++) {
                        RiskName += "《" + risktb.GetText(k, 1) + "》";
                        if (k != risktb.getMaxRow()) {
                            RiskName += "、";
                        }
                    }
                    strLine0[1] = RiskName;
                }
                tRiskListTable.add(strLine0);
            }
        }
        String grpno = mLLClaimDetailSet.get(1).getGrpContNo();
        String riskname = exesql1.getOneValue(sql);

        String[] tClaimListTitle = new String[17];
        tClaimListTitle[0] = "GetDutyCode"; //责任项目
        tClaimListTitle[1] = "FeeDate"; //帐单日期
        tClaimListTitle[2] = "TabFee"; //帐单金额
        tClaimListTitle[3] = "FeeInSecurity"; //社保内
        tClaimListTitle[4] = "FeePayed"; //已报销
        tClaimListTitle[5] = "RefuseAmnt"; //不合理费用
        tClaimListTitle[6] = "SelfPay1"; //自负一
        tClaimListTitle[7] = "RiskSeqNo"; //险种序号
        tClaimListTitle[8] = "PayFreeAmnt"; //免赔额
        tClaimListTitle[9] = "ClaimMoney"; //理算金额
        tClaimListTitle[10] = "RealPay"; //实赔金额
        tClaimListTitle[11] = "Remark"; //备注
        tClaimListTitle[12] = "GetRate"; //给付比例
        tClaimListTitle[13] = "FeeOutSecurity"; //社保外
        tClaimListTitle[14] = "TabFee2"; //上海社保账单
        tClaimListTitle[15] = "FeeOutSecu2"; //上海社保账单
        tClaimListTitle[16] = "tempx"; //临时
        ListTable tClaimListTable = new ListTable();
        tClaimListTable.setName("ClaimInfo");// add new 中间那一列
        String strLine[] = null;
        double AllFee = 0;
        double AllClaimMoney = 0;
        double AllRealPay = 0;

        if (!tAppealFlag) {
            for (int i = 1; i <= count; i++) {
                strLine = new String[17];

                //赔付明细还有记录
                tLLClaimDetailSchema = mLLClaimDetailSet.get(i);
                String strGetDutyName = getGetDutyName(
                        tLLClaimDetailSchema.getGetDutyCode());
                String strGetDutyKind = "";
                if (tLLClaimDetailSchema.getGetDutyKind().substring(0,
                        1).equals("1")) {
                    strGetDutyKind = " and feetype='2' ";
                }
                if (tLLClaimDetailSchema.getGetDutyKind().substring(0,
                        1).equals("2")) {
                    strGetDutyKind = "and feetype='1'";
                }

                if (strGetDutyName == null) {
                    return false;
                }

                int indexb = -1;
                indexb = String.valueOf(strGetDutyName).indexOf("保");
                if (indexb < 0) {
                    strLine[0] = String.valueOf(strGetDutyName);
                } else {
                    strLine[0] = String.valueOf(strGetDutyName).substring(0,
                            indexb); //责任项目
                }
                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setPolNo(tLLClaimDetailSchema.getPolNo());
                if (!tLCPolDB.getInfo()) {
                    LBPolDB tLBPolDB = new LBPolDB();
                    tLBPolDB.setPolNo(tLLClaimDetailSchema.getPolNo());
                    if (!tLBPolDB.getInfo()) {
                        CError.buildErr(this, "保单信息查询失败");
                        return false;
                    } else {
                        tLCPolDB.setRiskSeqNo(tLBPolDB.getRiskSeqNo());
                    }
                }
                strLine[7] = "" + tLCPolDB.getRiskSeqNo();
                strLine[1] = "　　　";
                strLine[2] = "0.00";
                strLine[3] = "0.00";
                strLine[4] = "0.00";
                strLine[5] = "0.00";
                strLine[13] = "0.00";
                strLine[14] = "0.00";
                strLine[15] = "0.00";
                strLine[16] = "　　　";

                LLFeeMainSet tLLFeeMainSet = new LLFeeMainSet();
                mLLFeeMainDB.setCaseRelaNo(tLLClaimDetailSchema.getCaseRelaNo());
                tLLFeeMainSet.set(mLLFeeMainDB.query());
                if (tLLFeeMainSet.size() > 0) {
                    tLLFeeMainSchema = tLLFeeMainSet.get(1);
                }
                //帐单还有记录备注信息
                if (!tLLFeeMainSchema.equals("")) {
                    if (tLLFeeMainSchema.getFeeDate() != null) {
                        strLine[1] = tLLFeeMainSchema.getFeeDate(); //帐单日期
                    }
                }

                double acountMoney = 0;
                double FIS = 0;
                double FOS = 0;
                double RefuseFee = 0;
                double PayedFee = 0;
                double OtherOrganAmnt =0;//“第三方支付”OtherOrganAmnt#2620
                
                //get OtherOrganAmnt from llfeemain #2620
                String sql101 ="select coalesce(sum(OtherOrganAmnt),0) from llfeemain where caseno='" + mCaseNo
                	+ "' and  mainfeeno in ( "
                	+
                	" select mainfeeno from llfeemain where caserelano ='" +
                	tLLClaimDetailSchema.getCaseRelaNo()
                	+ "' " + strGetDutyKind + " )";;
            	SSRS mSSRS = new SSRS();
                mSSRS = tExeSQL.execSQL(sql101);
                OtherOrganAmnt = Double.parseDouble(mSSRS.GetText(1, 1));
                
                String sql10 =
                        "select coalesce(sum(fee),0),coalesce(sum(PreAmnt),0),coalesce(sum(SelfAmnt),0),coalesce(sum(RefuseAmnt),0) from  LLCaseReceipt where caseno='" +
                        tLLClaimDetailSchema.getCaseNo() + "' "
                        +
                        " and  mainfeeno in ( select mainfeeno from llfeemain where caserelano ='" +
                        tLLClaimDetailSchema.getCaseRelaNo()
                        + "' " + strGetDutyKind + " )";

                ExeSQL tExeSQL = new ExeSQL();
                SSRS tSSRS = new SSRS();
                tSSRS = tExeSQL.execSQL(sql10);
                acountMoney = Double.parseDouble(tSSRS.GetText(1, 1));
                FIS = Double.parseDouble(tSSRS.GetText(1, 2));
                FOS = Double.parseDouble(tSSRS.GetText(1, 3));
                RefuseFee = Double.parseDouble(tSSRS.GetText(1, 4));
                
                if (acountMoney == 0) {
                    String sql11 =
                            "select coalesce(sum(fee),0),coalesce(sum(PreAmnt),0),coalesce(sum(SelfAmnt),0),coalesce(sum(RefuseAmnt),0) from  LLCaseReceipt where caseno='" +
                            tLLClaimDetailSchema.getCaseNo()
                            + "' and  mainfeeno in ( "
                            +
                            " select mainfeeno from llfeemain where caserelano ='" +
                            tLLClaimDetailSchema.getCaseRelaNo()
                            + "' " + strGetDutyKind + " )";

                    SSRS fSSRS = new SSRS();
                    fSSRS = tExeSQL.execSQL(sql11);
                    acountMoney = Double.parseDouble(fSSRS.GetText(1, 1));
                    FIS = Double.parseDouble(fSSRS.GetText(1, 2));
                    FOS = Double.parseDouble(fSSRS.GetText(1, 3));
                    RefuseFee = Double.parseDouble(fSSRS.GetText(1, 4));

                }
                if (acountMoney == 0) {

                    String sql12 =
                            "select coalesce(sum(sumfee),0) from llfeemain where caseno='" +
                            mCaseNo
                            + "' and  mainfeeno in ( "
                            +
                            " select mainfeeno from llfeemain where caserelano ='" +
                            tLLClaimDetailSchema.getCaseRelaNo()
                            + "' " + strGetDutyKind + " )";

                    SSRS fSSRS = new SSRS();
                    fSSRS = tExeSQL.execSQL(sql12);
                    acountMoney = Double.parseDouble(fSSRS.GetText(1, 1));

                }

                if (acountMoney == 0) {

                    if (tLLClaimDetailSchema.getTabFeeMoney() > 0) {
                        acountMoney = tLLClaimDetailSchema.getTabFeeMoney();
                    }

                }
                if (tLLClaimDetailSchema.getGetDutyCode().equals("1605")) {
                    if (!tLLFeeMainSchema.equals("")) {
                        PayedFee += tLLFeeMainSchema.getSumFee() -
                                tLLFeeMainSchema.getRemnant();
                    }
                }
                strLine[2] = String.valueOf(acountMoney); //帐单金额
                strLine[14] = String.valueOf(Arith.round(acountMoney, 2));
                strLine[13] = String.valueOf(Arith.round(FOS, 2)); //社保外
                double fos2 = Arith.round(acountMoney - FOS - RefuseFee, 2);
                strLine[15] = String.valueOf(fos2); //上海社保账单->存放社保外
                
                if("5".equals(applyertype)||"2".equals(applyertype)){
                	strLine[5] = String.valueOf(tLLFeeMainSchema.getRefuseAmnt());//上海修改，全国通用 不合理费用,批次取导入模板中的“不合理费用”。
                	strLine[3] = String.valueOf(Arith.round(OtherOrganAmnt, 2));//批次导入的，取导入的已报销费用取“三方支付”字段#26202015-11-4
                }else{
                	strLine[5] = String.valueOf(Arith.round(RefuseFee, 2)); //不合理费用
                	strLine[3] = String.valueOf(Arith.round(FIS, 2)); //已报销费用
                }
                
                strLine[13] = String.valueOf(Arith.round(FOS, 2)); //社保外
                
                if("8631".equals(mLLClaimSchema.getMngCom().substring(0, 4))){
                	if("5".equals(applyertype)||"2".equals(applyertype)){
                		strLine[3] = String.valueOf(Arith.round(planfee+supInHosFee, 2)); //实际对应模板中的“已报销费用”(批次导入的，取导入的“统筹支付”和“大额救助支付”字段合计金额)    		
                	}else{
                		strLine[3] = String.valueOf(Arith.round(planfee+supInHosFee, 2)); //已报销费用(个案处理的，取“统筹”、“附加”，也相当于批次导入 modify by Houyd 20140422)		     		
                	}
                }
                System.out.println("机构："+mLLClaimSchema.getMngCom().substring(0, 4));
                if("8631".equals(mLLClaimSchema.getMngCom().substring(0, 4))){               	
                	strLine[13] = String.valueOf(Arith.round(selfamnt+selfpay2, 2)); //社保外 modify by Houyd 20140422
                	if("5".equals(applyertype)||"2".equals(applyertype)){
                		//strLine[4] = String.valueOf(Arith.round(planfee+supInHosFee, 2)); //不对应模板中的任一字段
                	}else{
                		//strLine[4] = String.valueOf(Arith.round(planfee+supInHosFee, 2)); //不对应模板中的任一字段
                		strLine[5] = String.valueOf("0.00");//上海修改 不合理费用,个案不合理费用不对应
                	}
                }else{
                	strLine[4] = String.valueOf(Arith.round(PayedFee, 2)); //不对应模板中的任一字段
                }
                
                AllFee += acountMoney;

                if (count_2 <= 0) {
                    strLine[1] = "　　　";
                    strLine[2] = "0.00";
                    strLine[3] = "0.00";
                    strLine[4] = "0.00";
                    strLine[5] = "0.00";
                    strLine[13] = "0.00";
                }
                strLine[6] = "　　　";
                if (tLLClaimDetailSchema.getGetDutyCode().trim().equals(
                        "207201")) {
                    strLine[8] = "3天"; //免赔天数
                    strLine[3] = "0.00";
                    strLine[5] = "0.00";
                    strLine[13] = "0.00";
                    strLine[15] = "0.00";

                } else {
                    strLine[8] = String.valueOf(tLLClaimDetailSchema.
                                                getOutDutyAmnt()); //免赔
                }

                strLine[9] = String.valueOf(tLLClaimDetailSchema.getClaimMoney()); //理算金额
                strLine[10] = String.valueOf(tLLClaimDetailSchema.getRealPay()); //实赔金额
                strLine[11] = trunGiveType(tLLClaimDetailSchema.getGiveType()); //备注
                strLine[12] = String.valueOf(tLLClaimDetailSchema.
                                             getOutDutyRate()); //给付比例
                AllClaimMoney += tLLClaimDetailSchema.getClaimMoney();
                AllRealPay += tLLClaimDetailSchema.getRealPay();
                
//                String tREMARK = ""+tLLClaimDetailSchema.getREMARK();
//                if(tLLClaimDetailSchema.getREMARK()==null){
//                	tREMARK = "";
//                }
                
                if (tLLClaimDetailSchema.getGiveType().equals("3") ||
                    tLLClaimDetailSchema.getGiveType().equals("4") ||
                    tLLClaimDetailSchema.getGiveType().equals("5")) {
                    mRemarkDesc += strLine[0] + "责任" +
                            tLLClaimDetailSchema.getGiveTypeDesc() + "原因:"
                            + tLLClaimDetailSchema.getGiveReasonDesc() + "\n";
                } else {

                    System.out.println("LLClaimDetail:" +
                                       tLLClaimDetailSchema.getGiveReasonDesc());
                    if (tLLClaimDetailSchema.getGiveType().equals("1")) {
                        if (tLLClaimDetailSchema.getGiveReasonDesc() != null) {
                            mRemarkDesc += strLine[0] + "责任" +
                                    tLLClaimDetailSchema.getGiveTypeDesc() +
                                    "原因:"
                                    + tLLClaimDetailSchema.getGiveReasonDesc() +
                                    "\n"+"备注框:"+"\n";
                        }
                    }
                }

                if (LLCaseCommon.chenkWN(tLLClaimDetailSchema.getRiskCode())) {
                    String tWNAccSQL = "select elementname,elementvalue from "
                                       +
                            " LLElementDetail a,ldcode1 b where b.codetype='ClaimCalParam' "
                                       +
                            " and a.calcode=b.code and b.othersign='1' "
                                       + " and a.elementcode=b.code1 and caseno = '" +
                                       tLLClaimDetailSchema.getCaseNo()
                                       + "' and rgtno = '" +
                                       tLLClaimDetailSchema.getRgtNo()
                                       + "' and caserelano = '" +
                                       tLLClaimDetailSchema.getCaseRelaNo()
                                       + "' and polno = '" +
                                       tLLClaimDetailSchema.getPolNo()
                                       + "' and dutycode = '" +
                                       tLLClaimDetailSchema.getDutyCode()
                                       + "' and getdutycode = '" +
                                       tLLClaimDetailSchema.getGetDutyCode()
                                       + "' and getdutykind = '" +
                                       tLLClaimDetailSchema.getGetDutyKind()
                                       + "' with ur";
                    SSRS tWNSSRS = tExeSQL.execSQL(tWNAccSQL);
                    for (int m = 1; m <= tWNSSRS.getMaxRow(); m++) {
                        if (Double.parseDouble(tWNSSRS.GetText(m, 2)) > 0) {
                            mRemarkDesc += strLine[0] + "责任给付" +
                                    tLLClaimDetailSchema.getRealPay()
                                    + "元";
                            break;
                        }
                    }

                    for (int n = 1; n <= tWNSSRS.getMaxRow(); n++) {
                        if (n == 1) {
                            mRemarkDesc += ",其中";
                        }
                        if (Double.parseDouble(tWNSSRS.GetText(n, 2)) > 0 &&
                            n != tWNSSRS.getMaxRow()) {
                            mRemarkDesc += tWNSSRS.GetText(n, 1) +
                                    tWNSSRS.GetText(n, 2) + "元";
                        }
                        if (n == tWNSSRS.getMaxRow()) {
                            mRemarkDesc += tWNSSRS.GetText(n, 1) +
                                    tWNSSRS.GetText(n, 2) + "元。\n";
                        }
                    }
                }

                tClaimListTable.add(strLine);
            }
        } else {
            strLine = new String[17];
            strLine[1] = "　　　";
            strLine[2] = "0.00";
            strLine[3] = "";
            strLine[4] = "";
            strLine[5] = "";
            strLine[6] = "";
            strLine[7] = "";
            strLine[8] = "";
            strLine[9] = "";
            strLine[10] = "0.0";
            strLine[11] = "";
            strLine[12] = "";
            strLine[13] = "";
            strLine[14] = "0.00";
            strLine[15] = "";
            strLine[16] = "　　　";

            String tFeeMainSQL =
                    "select coalesce(sum(sumfee),0) from llfeemain where caseno='" +
                    mCaseNo + "'";
            strLine[2] = tExeSQL.getOneValue(tFeeMainSQL);
            strLine[14] = strLine[2];

            String tRealPaySQL =
                    "select coalesce(sum(realpay),0) from llclaim where caseno =('" +
                    tOrigCaseNo + "')";
            AllRealPay = Double.parseDouble(tExeSQL.getOneValue(
                    tRealPaySQL));

            mRemarkDesc = "特别提示栏：\n    本次给付是对案件号为" + tOrigCaseNo
                          + "的补充给付。案件号为" + tOrigCaseNo +
                          "的给付金额为" + Arith.round(AllRealPay, 2) + "元。";

            String tAppealSQL = "SELECT b.appealno,coalesce(sum(c.realpay),0) FROM llappeal a,llappeal b,llclaim c "
                                +
                    "WHERE a.caseno=b.caseno AND b.appealno=c.caseno AND a.appealno='"
                                + mCaseNo + "' AND b.appealno<>'" + mCaseNo +
                                "' group by b.appealno WITH UR";
            SSRS tAppSSRS = tExeSQL.execSQL(tAppealSQL);
            for (int j = 1; j <= tAppSSRS.getMaxRow(); j++) {
                AllRealPay += Double.parseDouble(tAppSSRS.GetText(j, 2));
                mRemarkDesc += "已纠错案件" + tAppSSRS.GetText(j, 1) + ",给付金额" +
                        tAppSSRS.GetText(j, 2) + "元。";
            }
            tRealPaySQL =
                    "select coalesce(sum(realpay),0) from llclaim where caseno =('" +
                    mCaseNo + "')";

            double tRealPay = Double.parseDouble(tExeSQL.getOneValue(
                    tRealPaySQL));
            AllRealPay += tRealPay;

            DecimalFormat tDF = new DecimalFormat("0.##");
            String tRturn = tDF.format(AllRealPay);

            strLine[10] = tRturn;

            tClaimListTable.add(strLine);

            mRemarkDesc += "本次给付" + tRealPay + "元，合计实付" + tRturn + "元。";
        }

        LLCaseDrugDB tLLCaseDrugDB = new LLCaseDrugDB();
        LLCaseDrugSet tLLCaseDrugSet = new LLCaseDrugSet();
        String tCaseDrugSQL = "select * from llcasedrug where caseno='"
                              + mCaseNo +"' and inputtype is null";
        tLLCaseDrugSet = tLLCaseDrugDB.executeQuery(tCaseDrugSQL);
        String drugdesc = "";
        if (tLLCaseDrugSet.size() > 0 ) {
            drugdesc = "\n扣除费用：\n";
            for (int k = 1; k <= mLLFeeMainSet.size(); k++) {
                tLLCaseDrugSet = new LLCaseDrugSet();
                tLLCaseDrugDB.setCaseNo(mCaseNo);
                tLLCaseDrugDB.setMainFeeNo(mLLFeeMainSet.get(k).getMainFeeNo());
                tLLCaseDrugSet = tLLCaseDrugDB.query();
                if (tLLCaseDrugSet.size() > 0) {
                    drugdesc += mLLFeeMainSet.get(k).getFeeDate() + "的"
                            + mLLFeeMainSet.get(k).getReceiptNo() + "号账单：\n";
                    for (int kk = 1; kk <= tLLCaseDrugSet.size(); kk++) {
                        LLCaseDrugSchema tLLCaseDrugSchema = new
                                LLCaseDrugSchema();
                        tLLCaseDrugSchema = tLLCaseDrugSet.get(kk);
                        drugdesc += "    " + tLLCaseDrugSchema.getDrugName();
                        if (tLLCaseDrugSchema.getSelfPay2() >= 0.01) {
                            drugdesc += "  扣除 " + tLLCaseDrugSchema.getSelfPay2() +
                                    "元 属于 部分自付 ";
                        }

                        if (tLLCaseDrugSchema.getSelfFee() >= 0.01) {
                            drugdesc += "  扣除 " + tLLCaseDrugSchema.getSelfFee() +
                                    "元 属于 自费 ";
                        }

                        if (tLLCaseDrugSchema.getUnReasonableFee() >= 0.01) {
                        	 String tREMARK = ""+tLLCaseDrugSchema.getRemark();
                             if(tLLCaseDrugSchema.getRemark()==null){
                             	tREMARK = "";
                             }
                             
                             drugdesc += "  扣除 " +
                                     tLLCaseDrugSchema.getUnReasonableFee() +
                                     "元 属于 不合理费用,原因: " + tREMARK;

                        }

                        drugdesc += "\n";
                    }
                }
            }           
        }
//      #2162 批次导入的“扣除明细”内容，请在“理赔给付细目表”中增加显示
        if("".equals(drugdesc) && !"".equals(mNewRemark)){
        	drugdesc = "\n扣除费用：\n" + mNewRemark + "\n";
        }else if(!"".equals(drugdesc) && !"".equals(mNewRemark)){
        	drugdesc += mNewRemark + "\n";
        }
        
        mRemarkDesc += drugdesc;

        String tGrpNameSQL =
                "SELECT DISTINCT appntname FROM llclaimpolicy WHERE caseno='" +
                mCaseNo + "'AND grpcontno<>'00000000000000000000' WITH UR";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tGrpNameSQL);
        String tGrpName = "";
        if (tSSRS.getMaxRow() > 0) {
            tGrpName = tSSRS.GetText(1, 1);
            if("".equals(tGrpName)||"null".equals(tGrpName)||tGrpName==null){
            	tGrpName="";
            }
        }
        ListTable tAgentTable = new ListTable();
        tAgentTable.setName("Agent");
        String[] Title = {"AgentName", "AgentCode", "AgentGroup"};
        String[] strCol;
        String tAgentSQL = "SELECT DISTINCT b.name,getUniteCode(a.agentcode),c.name "
                           + " FROM llclaimdetail a,laagent b,labranchgroup c "
                           +
                           " WHERE a.agentcode=b.agentcode AND b.agentgroup=c.agentgroup "
                           + " AND a.caseno='" + mCaseNo +
                           "' WITH UR";

        SSRS tAgentSSRS = tExeSQL.execSQL(tAgentSQL);
        if (tAgentSSRS.getMaxRow() <= 0) {
            strCol = new String[3];
            strCol[0] = "";
            strCol[1] = "";
            strCol[2] = "";
            tAgentTable.add(strCol);
        }
        for (int j = 1; j <= tAgentSSRS.getMaxRow(); j++) {
            strCol = new String[3];
            strCol[0] = tAgentSSRS.GetText(j, 1);
            strCol[1] = tAgentSSRS.GetText(j, 2);
            strCol[2] = tAgentSSRS.GetText(j, 3);
            tAgentTable.add(strCol);
        }

        String SysDate = StrTool.getYear() + "年" + StrTool.getMonth() + "月" +
                         StrTool.getDay() + "日";

        TextTag tTextTag = new TextTag();
        tTextTag.add("JetFormType", "ClaimDetailPrintExcelBL");
        //借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            CError.buildErr("getInputData", "操作员机构查询出错！");
            return false;
        }
        String printcom =
                "select codename from ldcode where codetype='pdfprintcom' and code='" +
                comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        tTextTag.add("ManageComLength4", printcode);

        tTextTag.add("BarCode1", mCaseNo);
        tTextTag.add("BarCodeParam1"
                     , "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        tTextTag.add("CustomerName", mCustomerName); //被保人姓名
        tTextTag.add("CustomerIDNo", mCustomerIDNo);//客户证件号码

        tTextTag.add("Age", Age);//年龄
        tTextTag.add("InsuredStat", insuredstat);//人员类别
        tTextTag.add("StartDate", FeeDate);
        tTextTag.add("SubDate", SubDate);//事件时间
        tTextTag.add("CompName", mGrpName);
        tTextTag.add("WorkPlace", mRgtantAddress);
        tTextTag.add("TitleDays", titledays);
        tTextTag.add("TInpatientDate", titleInpatient);
        tTextTag.add("InpatientDate", InpatientDate);
        tTextTag.add("RealDays", days);
        tTextTag.add("HospitName", hospitalname);
        tTextTag.add("HospitGrade", hosgrade);
        tTextTag.add("GrpContNo", grpno);
        tTextTag.add("RiskName", riskname);
        tTextTag.add("SelfPay1", selfpay1);
        tTextTag.add("SelfPay2", selfpay2);
        tTextTag.add("SelfAmnt", selfamnt);
        tTextTag.add("PlanFee", planfee);
        tTextTag.add("getlimit", getlimit);
        tTextTag.add("RgtDate", mRgtDate);
        System.out.println("起付线" + getlimit);
        tTextTag.add("FeeInSecurity", FeeInSecurity);
        System.out.println("医保内" + FeeInSecurity);
        tTextTag.add("FeeAtti", feeatti);//医院属性
        tTextTag.add("Remark", mremark);
        tTextTag.add("RemarkDesc", mRemarkDesc);

        tTextTag.add("XI_ContNo", "");
        tTextTag.add("XI_ManageCom", MngCom);

        System.out.println(mCustomerName);
        tTextTag.add("CaseNo", mCaseNo); //理赔号

        AllRealPay = Double.parseDouble(new DecimalFormat("0.00").format(
                AllRealPay));
        AllClaimMoney = Double.parseDouble(new DecimalFormat("0.00").format(
                AllClaimMoney));
        tTextTag.add("AllCountMoney",
                     String.valueOf(PubFun.getChnMoney(AllRealPay)));
        tTextTag.add("AllClaimMoney", String.valueOf(AllClaimMoney));
        tTextTag.add("AllRealMoney", new DecimalFormat("0.00").format(AllRealPay));
        tTextTag.add("Today", SysDate);
        tTextTag.add("GrpName", tGrpName);
        if(CheckCard.equals("1")){
            tTextTag.add("CustomerName", mCustomerName);
        }else{
            tTextTag.add("GrpName", tGrpName);
        }
        
        if (tTextTag.size() > 0) {
            xml.addTextTag(tTextTag);
        }

        xml.addListTable(tClaimListTable, tClaimListTitle);
        xml.addListTable(tRiskListTable, tRiskListTitle);

//        String[] tRemarkDesc = {mRemarkDesc,""};
//        ListTable tRemarkTable = new ListTable();
//        tRemarkTable.setName("RemarkDesc");
//        tRemarkTable.add(tRemarkDesc);
//        String[] tTitle = {"Desc","Ex"};
//        xml.addListTable(tRemarkTable, tTitle);

        xml.addListTable(tAgentTable, Title);

        xml.outputDocumentToFile("d:\\", "ClaimDetailPrintExcel");
        mResult.clear();
        mResult.addElement(xml);
        System.out.println("传输结束");
        return true;
    }

    private boolean getInputData(VData cInputData) {
        mGlobalInput.setSchema((GlobalInput)
                cInputData.getObjectByObjectName("GlobalInput",0));
        System.out.println("处理全局变量结束");
        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        mCaseNo  = (String) mTransferData.getValueByName("caseno");// 理赔号
        mCaseRelaNo = (String) mTransferData.getValueByName("caserelano");//关联号
      
        mLLClaimSchema.setCaseNo(mCaseNo);

        mLLFeeMainSchema.setCaseRelaNo(mCaseRelaNo);
        mLLFeeMainSchema.setCaseNo(mCaseNo);
        System.out.println("CaseNo:"+mCaseNo);
        System.out.println("CaseRelaNo:"+mCaseRelaNo);
        
        if (mCaseNo == null || mCaseNo.equals("")) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ClaimDetailPrintExcelBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有传入理赔号！";
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public String getGetDutyName(String strGetDutyCode) {
        LMDutyGetDB tLMDutyGetDB = new LMDutyGetDB();
        tLMDutyGetDB.setGetDutyCode(strGetDutyCode);
        if (!tLMDutyGetDB.getInfo()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMDutyGetDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ClaimDetailPrintBL";
            tError.functionName = "getBaseData";
            tError.errorMessage = "给付责任名称查询失败!";
            this.mErrors.addOneError(tError);

            return null;
        }
        return tLMDutyGetDB.getGetDutyName();
    }

    public String trunGiveType(String GiveType) {
        String reGiveType = "";
        if ("1".equals(GiveType)) {
            reGiveType = "A";
            if (mremarktag[0] < 1) {
                mremark += "A、正常给付   ";
            }
            mremarktag[0] = 1;
        }
        if ("2".equals(GiveType)) {
            reGiveType = "B";
            if (mremarktag[1] < 1) {
                mremark += "B、部分给付   ";
            }
            mremarktag[1] = 1;
        }
        if ("3".equals(GiveType)) {
            reGiveType = "C";
            if (mremarktag[2] < 1) {
                mremark += "C、全额拒付   ";
            }
            mremarktag[2] = 1;
        }
        if ("5".equals(GiveType)) {
            reGiveType = "D";
            if (mremarktag[3] < 1) {
                mremark += "D、通融给付   ";
            }
            mremarktag[3] = 1;
        }
        if ("4".equals(GiveType)) {
            reGiveType = "E";
            if (mremarktag[4] < 1) {
                mremark += "E、协议给付   ";
            }
            mremarktag[4] = 1;
        }

        return reGiveType;
    }

    public double getselfamnt(String getdutyname) {
        System.out.println("&&&&&" + getdutyname);
        double amnt = 0.0;
        if (getdutyname.equals("低段保险金")) {
            amnt = lowamnt;
        }
        if (getdutyname.equals("中段保险金")) {
            amnt = midamnt;
        }
        if (getdutyname.equals("高段保险金1")) {
            amnt = highamnt1;
        }
        if (getdutyname.equals("高段保险金2")) {
            amnt = highamnt2;
        }
        if (getdutyname.equals("超高段保险金")) {
            amnt = supinhosamnt;
        }
        if (getdutyname.equals("大额门急诊保险金")) {
            amnt = supdooramnt;
        }
        if (getdutyname.equals("小额门急诊保险金")) {
            amnt = smalldooramnt;
        }
        if (getdutyname.equals("门急诊保险金")) {
            amnt = emergdooramnt;
        }
        System.out.println("******" + amnt);
        amnt = Double.parseDouble(new DecimalFormat("0.00").format(amnt));
        return amnt;
    }

    public VData getResult() {
        return mResult;
    }

    private boolean dealPrintMag() {
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        tLOPRTManagerSchema.setOtherNo(mCaseNo); //存放前台传来的caseno
        tLOPRTManagerSchema.setOtherNoType("5");
        tLOPRTManagerSchema.setCode("lp001");
        tLOPRTManagerSchema.setManageCom(this.mGlobalInput.ManageCom);
        tLOPRTManagerSchema.setAgentCode("");
        tLOPRTManagerSchema.setReqCom(this.mGlobalInput.ManageCom);
        tLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
        tLOPRTManagerSchema.setPrtType("0");
        tLOPRTManagerSchema.setStateFlag("0");
        tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        tLOPRTManagerSchema.setStandbyFlag1(mCaseRelaNo);

        mResult.addElement(tLOPRTManagerSchema);
        return true;
    }
    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return xml.getInputStream();
    }

    public static void main(String[] args) {

        ClaimDetailPrintExcelBL tClaimDetailPrintBL = new ClaimDetailPrintExcelBL();

        VData tVData = new VData();

        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "cm0001";
        String caserelano = "86310000718515";
        String caseno = "C3100140829000001";
        TransferData PrintElement = new TransferData();
        PrintElement.setNameAndValue("caserelano", caserelano);
        PrintElement.setNameAndValue("caseno", caseno);
        tVData.add(tGlobalInput);
        tVData.add(PrintElement);
        tClaimDetailPrintBL.submitData(tVData, null);
    }

    private void jbInit() throws Exception {
    }

}
