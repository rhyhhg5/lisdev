/*
 * <p>ClassName: OLAAccountsBL </p>
 * <p>Description: OLAAccountsBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:03:36
 */
package com.sinosoft.lis.f1print;
 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;


import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LAChannelReportBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    //统计管理机构
    private String  managecom= "";
    //统计层级
    private String mLevel = "";
    //统计险种
    private String mRiskCode = "";
    //统计起期
    private String  mStartDate= "";
    //统计止期
    private String  mEndDate = "";
    //交费方式
    private String  mPayIntv = "";
    //交费年限
    private String  mPayYears = "";
    //需要调用的模版
    private String mFlag = "";
    //转化交费方式
    private int mPayTime;
    //转换交费年限
    private int mpayyears;
    //零时存放的数组

    //当前时间
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private  ListTable tListTable = new ListTable();
    private  ListTable mListTable = new ListTable();//合计行
    public LAChannelReportBL() {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //将操作数据拷贝到本类中
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        System.out.println("dealData:" + mOperate);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        System.out.println("dealData:" + mOperate);
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAAccountsBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLAAccountsBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        tListTable = null;
        mListTable = null;
        return true;
    }
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("dealData:" + mOperate);
        mFlag = getflag(mLevel);
        ExeSQL tExeSQL = new ExeSQL();
        int j=1;
        String[] Add = {"0", "0", "0", "0", "0" ,"0","0","0","0","0","0","0","0","0","0","0"};
        if(this.mLevel.equals("0"))
        {
           String manageSQL = "select comcode,name from ldcom where comgrade='02' and sign='1' and comcode like '"+managecom+"%' order  by comcode";
           SSRS managSSRS = new SSRS();
           managSSRS = tExeSQL.execSQL(manageSQL);
           for(int i = 1;i<=managSSRS.getMaxRow();i++)
           {//进行机构循环
               getDataList(managSSRS.GetText(i, 1),
                       managSSRS.GetText(i, 2),managSSRS.GetText(i, 1),"分公司",j,Add);
               j++;
           }
       System.out.println(manageSQL);
        }

        if(this.mLevel.equals("1"))
        {
            String manageSQL = "select comcode,name from ldcom where comgrade='03' and sign='1' and comcode<>'86000000' and substr(comcode,5)<>'0000'   and comcode like '"+managecom+"%' order  by comcode";
            SSRS managSSRS = new SSRS();
            managSSRS = tExeSQL.execSQL(manageSQL);
            for(int i = 1;i<=managSSRS.getMaxRow();i++)
            {//进行机构循环

//              查询所属分公司名称
                String comcode = managSSRS.GetText(i, 1);
                String nameSQL = "select name from ldcom where comcode='"+comcode.substring(0, 4)+"'";
                String name = tExeSQL.getOneValue(nameSQL);
                getDataList(managSSRS.GetText(i, 1),managSSRS.GetText(i, 2),name,"支公司",j,Add);
                j++;
            }
          System.out.println(manageSQL);
        }
        if(this.mLevel.equals("2"))//营业部,统计区间内为营业
        {
            String branchSQL = "select agentgroup,name,managecom from labranchgroup where branchtype='3' and branchtype2='01' " +
                    "and founddate<='"+this.mEndDate+"' and (enddate is null or enddate>='"
                    +this.mStartDate+"')  and branchlevel='42' and managecom like '"+managecom+"%' order by managecom,branchattr";
                        System.out.println(branchSQL);
            SSRS tSSRS = new SSRS();
            tSSRS = tExeSQL.execSQL(branchSQL);
            for(int i = 1;i<=tSSRS.getMaxRow();i++)
            {//进行营业部循环

                //查询所属分公司
                String managecom =  tSSRS.GetText(i, 3);
                String SQL = "select name from ldcom where comcode = '"+managecom.substring(0,4)+"'";
                String name = tExeSQL.getOneValue(SQL);
                getDataList(tSSRS.GetText(i, 1),tSSRS.GetText(i, 2),name,"营业部",j,Add);
                j++;
            }

        }
        if(this.mLevel.equals("3"))
        {
            String agentSQL = "select agentcode,name,agentgroup,managecom from laagent where branchtype='3' and branchtype2='01' " +
                    "and employdate<='"+this.mEndDate+"' and (outworkdate is null or outworkdate>='"+this.mStartDate+"')" +
                            "  and managecom like '"+managecom+"%'  order by managecom";
         System.out.println(agentSQL);
            SSRS tSSRS = new SSRS();
            tSSRS = tExeSQL.execSQL(agentSQL);
            for(int i = 1;i<=tSSRS.getMaxRow();i++)
            {//进行人员循环

                String managecom =  tSSRS.GetText(i, 4);
                String branchSQL = "select name from labranchgroup where agentgroup='"+tSSRS.GetText(1, 3)+"'";
                String branchName = tExeSQL.getOneValue(branchSQL);
                String manageSQL = "select name from ldcom where comcode = '"+managecom.substring(0, 4)+"'";
                String manageName = tExeSQL.getOneValue(manageSQL);
                getDataList(tSSRS.GetText(i, 1),tSSRS.GetText(i, 2),branchName,manageName,j,Add);
                j++;
            }

        }

        if(tListTable==null||tListTable.equals(""))
        {
            CError tCError = new CError();
            tCError.moduleName = "MakeXMLBL";
            tCError.functionName = "creatFile";
            tCError.errorMessage = "没有符合条件的信息！";
            this.mErrors.addOneError(tCError);
            return false;
        }
        //进行模版的区分
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
        if(mFlag.equals("1"))
        {
            txmlexport.createDocument("LAChannel1.vts","printer");
        }
        if(mFlag.equals("2"))
        {
            txmlexport.createDocument("LAChannel2.vts","printer");
        }
        if(mFlag.equals("3"))
        {
            txmlexport.createDocument("LAChannel3.vts","printer");
        }
        if(mFlag.equals("4"))
        {
            txmlexport.createDocument("LAChannel4.vts","printer");
        }
        mListTable.add(Add);
        tListTable.setName("LAChannel");
        mListTable.setName("Add");
        String[] title = {"", "", "", "", "" ,"","","","","","","","","","",""};
        //放入一些基本值
        texttag.add("makedate", currentDate);
        texttag.add("maketime", currentTime);
        texttag.add("company", getName());
        texttag.add("StartDate",mStartDate);
        texttag.add("EndDate",mEndDate);
        texttag.add("riskcode",mRiskCode);
        texttag.add("payIntv",mPayIntv);
        texttag.add("payyears",mPayYears);
        if (texttag.size() > 0)
        {
            txmlexport.addTextTag(texttag);
        }
//       将数据放到LISTTABLE中
        txmlexport.addListTable(tListTable, title);
        txmlexport.addListTable(mListTable, title);
        txmlexport.outputDocumentToFile("c:\\", "lach");
        mResult.clear();
        mResult.addElement(txmlexport);
        return true;
    }
    /**
     * @string
     * 模版调用标记
     */
    private String getflag(String level)
    {
        String tflag = "";
        if(level.equals("0"))
        {
            tflag = "1";
        }
        if(level.equals("1"))
        {
            tflag = "2";
        }
        if(level.equals("2"))
        {
            tflag = "3";
        }
        if(level.equals("3"))
        {
            tflag = "4";
        }
        return tflag;
    }
    /**
     * 获取模版
     */
    private boolean getStencil(String cflag)
    {
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
        if(cflag.equals("1"))
        {
            txmlexport.createDocument("LAChannel1.vts","printer");
        }
        if(cflag.equals("2"))
        {
            txmlexport.createDocument("LAChannel2.vts","printer");
        }
        if(cflag.equals("3"))
        {
            txmlexport.createDocument("LAChannel3.vts","printer");
        }
        if(cflag.equals("4"))
        {
            txmlexport.createDocument("LAChannel4.vts","printer");
        }

        tListTable.setName("LAChannel");
        String[] title = {"", "", "", "", "" ,"","","","","","","","","","",""};
        //放入一些基本值
        texttag.add("makedate", currentDate);
        texttag.add("maketime", currentTime);
        texttag.add("company", getName());
        if (texttag.size() > 0)
        {
            txmlexport.addTextTag(texttag);
        }
//       将数据放到LISTTABLE中
        txmlexport.addListTable(tListTable, title);
        mResult.clear();
        mResult.addElement(txmlexport);
        return true;
    }

    /**
     *查询各列值
     *cCode-循环参数；Name1、Name2、Name3---不同的统计层级传入的参数不一致
     */
    private boolean getDataList(String cCode,String Name1,String Name2,String Name3,int j,String[] Add)
    {
        String mCode = cCode;
        try
        {
            DecimalFormat tDF = new DecimalFormat("0.######");
            String data[] = new String[16];
            data[0] = String.valueOf(j);
            data[1] = Name1;
            data[2] = Name2;
            data[3] = Name3;
            data[4] = getPrem(mCode);
            data[5] = getWTPrem(mCode);
            data[7] = String.valueOf(getNum(mCode));
            data[8] = String.valueOf(getWTNum(mCode));
            data[10]= getTBPrem(mCode);
            data[11]= String.valueOf(TBNum(mCode));
//           data[6] = String.valueOf(Double.parseDouble(data[4])+Double.parseDouble(data[5])+Double.parseDouble(data[10]));
            data[6] = tDF.format(Double.parseDouble(data[4])+Double.parseDouble(data[5])+Double.parseDouble(data[10]));
            data[9] = String.valueOf(Integer.parseInt(data[7])-Integer.parseInt(data[8])-Integer.parseInt(data[11]));
            if(this.mLevel.equals("3"))
            {
                data[12]= " ";
                data[13]= " ";
                data[14]= " ";
                data[15]= " ";
                Add[12] = " ";
                Add[13] = " ";
                Add[14] = " ";
                Add[15] = " ";
            }
            else
            {
                data[12]= String.valueOf(AgentcomNum(mCode));
                data[13]= String.valueOf(YXAgentcomNum(mCode));
                data[14]= String.valueOf(AgentNum(mCode));
                data[15]= String.valueOf(getRLNum(mCode));
                Add[12] = String.valueOf(Integer.parseInt(Add[12])+Integer.parseInt(data[12]));
                Add[13] = String.valueOf(Integer.parseInt(Add[13])+Integer.parseInt(data[13]));
                Add[14] = String.valueOf(Integer.parseInt(Add[14])+Integer.parseInt(data[14]));
                Add[15] = String.valueOf(Integer.parseInt(Add[15])+Integer.parseInt(data[15]));
            }
            //合计
            Add[0] = String.valueOf(j+1);
            Add[1] = "合  计";
            Add[2] = "";
            Add[3] = "";
            Add[4] = tDF.format(Double.parseDouble(Add[4])+Double.parseDouble(data[4])) ;
            Add[5] = tDF.format(Double.parseDouble(Add[5])+Double.parseDouble(data[5]));
            Add[6] = tDF.format(Double.parseDouble(Add[6])+Double.parseDouble(data[6]));
            Add[7] = String.valueOf(Integer.parseInt(Add[7])+Integer.parseInt(data[7]));
            Add[8] = String.valueOf(Integer.parseInt(Add[8])+Integer.parseInt(data[8]));
            Add[9] = String.valueOf(Integer.parseInt(Add[9])+Integer.parseInt(data[9]));
            Add[10]= tDF.format(Double.parseDouble(Add[10])+Double.parseDouble(data[10])) ;
            Add[11] = String.valueOf(Integer.parseInt(Add[11])+Integer.parseInt(data[11]));

            tListTable.add(data);
        }
        catch(Exception ex)
        {
            CError.buildErr("getDataList", "准备数据时出错！");
            System.out.println(ex.toString());
            return false;
        }
        return true;
    }
    /**
     * 承保保费查询
     */
    private String getPrem(String mCode)
    {
        String Prem = "";
        String tSQL ="";
        DecimalFormat tDF = new DecimalFormat("0.######");
         tSQL = "select value(sum(b.sumactupaymoney),0)/10000 from LJAPayPerson b where b.payType='ZC'" +

                " and b.makedate >='"+this.mStartDate+"' and b.makedate<='"
                +this.mEndDate+"' and b.GrpPolNo = '00000000000000000000'" ;
         tSQL +=  " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04'" ;

         if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
         {
             tSQL += " and c.payintv = "+this.mPayTime+" ";
         }
         if(this.mPayYears!=null&&!this.mPayYears.equals(""))
         {
             if(this.mPayYears.equals("4"))
                 tSQL += " and c.payyears > "+this.mpayyears+"";
                else
                    tSQL += " and c.payyears = "+this.mpayyears+"";
         }
         tSQL += " ) " ;
         tSQL += "or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'" ;
         if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
         {
             tSQL += " and c.payintv = "+this.mPayTime+" ";
         }
         if(this.mPayYears!=null&&!this.mPayYears.equals(""))
         {
             if(this.mPayYears.equals("4"))
                 tSQL += " and c.payyears > "+this.mpayyears+"";
                else
                    tSQL += " and c.payyears = "+this.mpayyears+"";
         }
         tSQL += " ))";

        if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
        {
            tSQL += " and b.riskcode = '"+mRiskCode+"'";
        }

         if(this.mLevel.equals("0"))//管理机构
         {
             tSQL += " and substr(b.managecom,1,4)='"+mCode+"'";
         }
         if(this.mLevel.equals("1"))
         {
             tSQL += " and b.managecom='"+mCode+"'";
         }
        if(this.mLevel.equals("2"))//营业部
        {
            tSQL += " and  b.agentgroup in (select agentgroup from labranchgroup where agentgroup='"
            	+mCode+"' or upbranch='"+mCode+"') ";
        }
        if(this.mLevel.equals("3"))
        {
            tSQL += " and b.agentcode ='"+mCode+"' ";
        }
        try{
            Prem = tDF.format(execQuery(tSQL));
        }catch(Exception ex)
        {
            System.out.println("getFirstYearMoneyByManageCom 出错！");
        }

        return Prem;
    }
    /**
     * 撤保保费
     */
    private String getWTPrem(String mCode)
    {
        String WTPrem = "";
        String tSQL ="";
        DecimalFormat tDF = new DecimalFormat("0.######");
         tSQL = "select value(sum(getmoney),0)/10000 from LJAGetEndorse b where " +
                " b.FeeOperationType='WT' AND b.FEEFINATYPE='TF' " +
                " and b.makedate >='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"' and b.GrpPolNo = '00000000000000000000'";
         tSQL +=  " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04'" ;

         if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
         {
             tSQL += " and c.payintv = "+this.mPayTime+" ";
         }
         if(this.mPayYears!=null&&!this.mPayYears.equals(""))
         {
             if(this.mPayYears.equals("4"))
                 tSQL += " and c.payyears > "+this.mpayyears+"";
                else
                    tSQL += " and c.payyears = "+this.mpayyears+"";
         }
         tSQL += " ) " ;
         tSQL += "or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'" ;
         if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
         {
             tSQL += " and c.payintv = "+this.mPayTime+" ";
         }
         if(this.mPayYears!=null&&!this.mPayYears.equals(""))
         {
             if(this.mPayYears.equals("4"))
                 tSQL += " and c.payyears > "+this.mpayyears+"";
                else
                    tSQL += " and c.payyears = "+this.mpayyears+"";
         }
         tSQL += " ))";

         if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
         {
             tSQL += " and b.riskcode = '"+mRiskCode+"'";
         }
         if(this.mLevel.equals("0"))//管理机构
         {
             tSQL += " and substr(b.managecom,1,4)='"+mCode+"'";
         }
         if(this.mLevel.equals("1"))
         {
             tSQL += " and b.managecom='"+mCode+"'";
         }
        if(this.mLevel.equals("2"))//营业部
        {
            tSQL += " and  b.agentgroup in (select agentgroup from labranchgroup where agentgroup='"
            	+mCode+"' or upbranch='"+mCode+"') ";
        }
        if(this.mLevel.equals("3"))
        {
            tSQL += " and b.agentcode ='"+mCode+"' ";
        }
        try{
            WTPrem = tDF.format(execQuery(tSQL));
        }catch(Exception ex)
        {
            System.out.println("getFirstYearMoneyByManageCom 出错！");
        }

        return WTPrem;
    }
    /**
     * 承保件数
     */
    private int getNum(String mCode)
    {
        int CBNum = 0;
        String tSQL ="";
        tSQL = "select count(distinct b.contno) from LJAPayPerson b  where   b.payType = 'ZC' " +
                "and  b.MakeDate>='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"'  and b.GrpPolNo = '00000000000000000000' " ;
        tSQL +=  " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04'" ;

        if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
        {
            tSQL += " and c.payintv = "+this.mPayTime+" ";
        }
        if(this.mPayYears!=null&&!this.mPayYears.equals(""))
        {
            if(this.mPayYears.equals("4"))
                tSQL += " and c.payyears > "+this.mpayyears+"";
               else
                   tSQL += " and c.payyears = "+this.mpayyears+"";
        }
        tSQL += " ) " ;
        tSQL += "or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'" ;
        if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
        {
            tSQL += " and c.payintv = "+this.mPayTime+" ";
        }
        if(this.mPayYears!=null&&!this.mPayYears.equals(""))
        {
            if(this.mPayYears.equals("4"))
                tSQL += " and c.payyears > "+this.mpayyears+"";
               else
                   tSQL += " and c.payyears = "+this.mpayyears+"";
        }
        tSQL += " ))";

        if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
        {
            tSQL += " and b.riskcode = '"+mRiskCode+"'";
        }

        if(this.mLevel.equals("0"))//管理机构
        {
            tSQL += " and substr(b.managecom,1,4)='"+mCode+"'";
        }
        if(this.mLevel.equals("1"))
        {
            tSQL += " and b.managecom='"+mCode+"'";
        }
       if(this.mLevel.equals("2"))//营业部
       {
           tSQL += " and  b.agentgroup in (select agentgroup from labranchgroup where agentgroup='"
           	+mCode+"' or upbranch='"+mCode+"') ";
       }
       if(this.mLevel.equals("3"))
       {
           tSQL += " and b.agentcode ='"+mCode+"' ";
       }
       try{
           CBNum = (int) execQuery(tSQL);
       }catch(Exception ex)
       {
           System.out.println("getAgentCountByManageCom 出错！");
       }
        return CBNum;
    }
    /**
     * 撤保件数
     */
  private   int getWTNum(String mCode)
  {
      int WTNum = 0;
      String tSQL ="";
      tSQL = "select count(distinct b.contno) from LJAGetEndorse b " +
            " where  b.FeeOperationType='WT' AND b.FEEFINATYPE='TF'" +
              "and  b.MakeDate>='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"'  and b.GrpPolNo = '00000000000000000000' " ;

      tSQL +=  " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04'" ;

      if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
      {
          tSQL += " and c.payintv = "+this.mPayTime+" ";
      }
      if(this.mPayYears!=null&&!this.mPayYears.equals(""))
      {
          if(this.mPayYears.equals("4"))
              tSQL += " and c.payyears > "+this.mpayyears+"";
             else
                 tSQL += " and c.payyears = "+this.mpayyears+"";
      }
      tSQL += " ) " ;
      tSQL += "or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'" ;
      if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
      {
          tSQL += " and c.payintv = "+this.mPayTime+" ";
      }
      if(this.mPayYears!=null&&!this.mPayYears.equals(""))
      {
          if(this.mPayYears.equals("4"))
              tSQL += " and c.payyears > "+this.mpayyears+"";
             else
                 tSQL += " and c.payyears = "+this.mpayyears+"";
      }
      tSQL += " ))";

      if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
      {
          tSQL += " and b.riskcode = '"+mRiskCode+"'";
      }
      if(this.mLevel.equals("0"))//管理机构
      {
          tSQL += " and substr(b.managecom,1,4)='"+mCode+"'";
      }
      if(this.mLevel.equals("1"))
      {
          tSQL += " and b.managecom='"+mCode+"'";
      }
     if(this.mLevel.equals("2"))//营业部
     {
         tSQL += " and  b.agentgroup in (select agentgroup from labranchgroup where agentgroup='"
         	+mCode+"' or upbranch='"+mCode+"') ";
     }
     if(this.mLevel.equals("3"))
     {
         tSQL += " and b.agentcode ='"+mCode+"' ";
     }
     try{
         WTNum = (int) execQuery(tSQL);
     }catch(Exception ex)
     {
         System.out.println("getAgentCountByManageCom 出错！");
     }
      return WTNum;
  }
  /**
   * 退保保费
   */
  private String getTBPrem(String mCode)
  {
      String TBPrem = "";
      String tSQL ="";
      DecimalFormat tDF = new DecimalFormat("0.######");
       tSQL = "select value(sum(getmoney),0)/10000 from LJAGetEndorse b " +
            " where  b.FeeOperationType in('CT','XT') AND b.FEEFINATYPE='TF'" +
              " and b.makedate >='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"' and b.GrpPolNo = '00000000000000000000'" ;

       tSQL +=  " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04'" ;

       if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
       {
           tSQL += " and c.payintv = "+this.mPayTime+" ";
       }
       if(this.mPayYears!=null&&!this.mPayYears.equals(""))
       {
           if(this.mPayYears.equals("4"))
               tSQL += " and c.payyears > "+this.mpayyears+"";
              else
                  tSQL += " and c.payyears = "+this.mpayyears+"";
       }
       tSQL += " ) " ;
       tSQL += "or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'" ;
       if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
       {
           tSQL += " and c.payintv = "+this.mPayTime+" ";
       }
       if(this.mPayYears!=null&&!this.mPayYears.equals(""))
       {
           if(this.mPayYears.equals("4"))
               tSQL += " and c.payyears > "+this.mpayyears+"";
              else
                  tSQL += " and c.payyears = "+this.mpayyears+"";
       }
       tSQL += " ))";

       if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
       {
           tSQL += " and b.riskcode = '"+mRiskCode+"'";
       }

       if(this.mLevel.equals("0"))//管理机构
       {
           tSQL += " and substr(b.managecom,1,4)='"+mCode+"'";
       }
       if(this.mLevel.equals("1"))
       {
           tSQL += " and b.managecom='"+mCode+"'";
       }
      if(this.mLevel.equals("2"))//营业部
      {
          tSQL += " and  b.agentgroup in (select agentgroup from labranchgroup where agentgroup='"
          	+mCode+"' or upbranch='"+mCode+"') ";
      }
      if(this.mLevel.equals("3"))
      {
          tSQL += " and b.agentcode ='"+mCode+"' ";
      }
      try{
          TBPrem = tDF.format(execQuery(tSQL));
      }catch(Exception ex)
      {
          System.out.println("getFirstYearMoneyByManageCom 出错！");
      }

      return TBPrem;
  }
  /**
   * 退保件数
   */
  private int TBNum(String mCode)
  {
      int TBNum = 0;
      String tSQL ="";
      tSQL = "select count(distinct b.contno) from LJAGetEndorse b " +
            " where  b.FeeOperationType in ('CT','XT') AND b.FEEFINATYPE='TF' " +
              "and  b.MakeDate>='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"'  and b.GrpPolNo = '00000000000000000000' " ;
      tSQL +=  " and ( exists (select contno from lcpol c where b.contno=c.contno and c.salechnl='04'" ;

      if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
      {
          tSQL += " and c.payintv = "+this.mPayTime+" ";
      }
      if(this.mPayYears!=null&&!this.mPayYears.equals(""))
      {
          if(this.mPayYears.equals("4"))
              tSQL += " and c.payyears > "+this.mpayyears+"";
             else
                 tSQL += " and c.payyears = "+this.mpayyears+"";
      }
      tSQL += " ) " ;
      tSQL += "or exists (select contno from lbpol c where b.contno=c.contno and c.salechnl='04'" ;
      if(this.mPayIntv!=null&&!this.mPayIntv.equals(""))
      {
          tSQL += " and c.payintv = "+this.mPayTime+" ";
      }
      if(this.mPayYears!=null&&!this.mPayYears.equals(""))
      {
          if(this.mPayYears.equals("4"))
              tSQL += " and c.payyears > "+this.mpayyears+"";
             else
                 tSQL += " and c.payyears = "+this.mpayyears+"";
      }
      tSQL += " ))";

      if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
      {
          tSQL += " and b.riskcode = '"+mRiskCode+"'";
      }

      if(this.mLevel.equals("0"))//管理机构
      {
          tSQL += " and substr(b.managecom,1,4)='"+mCode+"'";
      }
      if(this.mLevel.equals("1"))
      {
          tSQL += " and b.managecom='"+mCode+"'";
      }
     if(this.mLevel.equals("2"))//营业部
     {
         tSQL += " and  b.agentgroup in (select agentgroup from labranchgroup where agentgroup='"
         	+mCode+"' or upbranch='"+mCode+"') ";
     }
     if(this.mLevel.equals("3"))
     {
         tSQL += " and b.agentcode ='"+mCode+"' ";
     }
     try{
         TBNum = (int) execQuery(tSQL);
     }catch(Exception ex)
     {
         System.out.println("getAgentCountByManageCom 出错！");
     }
      return TBNum;
  }
  /**
   * 网点数量
   */
  private int AgentcomNum(String mCode)
  {
      int AgentcomNum = 0;
      String tSQL ="";
      tSQL = "select count(b.agentcom) from lacom b where b.branchtype='3' and b.branchtype2='01' and b.sellflag='Y'  and b.makedate<='"+this.mEndDate+"' " +
      		" and (enddate is null or enddate>'"+this.mEndDate+"')";
      if(this.mLevel.equals("0"))//管理机构
      {
          tSQL += " and substr(b.managecom,1,4)='"+mCode+"'";
      }
      if(this.mLevel.equals("1"))
      {
          tSQL += " and b.managecom='"+mCode+"'";
      }
     if(this.mLevel.equals("2"))//营业部" 
     {
         tSQL += " and b.agentcom in (select agentcom from lacomtoagent where agentgroup in("
        	 +"select agentgroup from labranchgroup where agentgroup='"+mCode+"' or upbranch='"+mCode+"'))";
     }
     if(this.mLevel.equals("3"))//客户经理不用统计网点数量
     {
         tSQL += " and b.agentcom = '000000' ";
     }
     try{
         AgentcomNum = (int) execQuery(tSQL);
     }catch(Exception ex)
     {
         System.out.println("getAgentCountByManageCom 出错！");
     }
      return AgentcomNum;
  }
  /**
   * 有效网点数量
   */
  private int YXAgentcomNum(String mCode)
  {
      int YXAgentcomNum = 0;
      String tSQL ="";
      tSQL = "select count(distinct(agentcom)) from LJAPayPerson b " +
            "where b.grpcontno='00000000000000000000' and  b.makedate>='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"'" +
                    " and (exists (select contno from lccont c where b.contno=c.contno and c.salechnl='04')" +
                    " or exists (select contno from lbcont d where d.contno=b.contno and d.salechnl='04'))";
      if(this.mLevel.equals("0"))//管理机构
      {
          tSQL += " and substr(b.managecom,1,4)='"+mCode+"'";
      }
      if(this.mLevel.equals("1"))
      {
          tSQL += " and b.managecom='"+mCode+"'";
      }
     if(this.mLevel.equals("2"))//营业部
     {
         tSQL += " and  b.agentgroup in (select agentgroup from labranchgroup where agentgroup='"
         	+mCode+"' or upbranch='"+mCode+"') ";
     }
     if(this.mLevel.equals("3"))
     {
         tSQL += " and b.agentcom='000000' ";
     }
     try{
         YXAgentcomNum = (int) execQuery(tSQL);
     }catch(Exception ex)
     {
         System.out.println("getAgentCountByManageCom 出错！");
     }
      return YXAgentcomNum;
  }
  /**
   * 人员数量
   */
  private int AgentNum(String mCode)
  {
      int AgentNum = 0;
      String tSQL = "";
      tSQL = "select count(agentcode) from laagent b where b.branchtype='3' and b.branchtype2='01'" +
            " and b.employdate<='"+this.mEndDate+"' and (b.outworkdate is null or b.outworkdate>'"+this.mEndDate+"')";
      if(this.mLevel.equals("0"))
      {
          tSQL += " and substr(b.managecom,1,4)='"+mCode+"'";
      }
      if(this.mLevel.equals("1"))
      {
          tSQL += " and b.managecom like '"+mCode+"'";
      }
      if(this.mLevel.equals("2"))
      {
          tSQL += " and  b.agentgroup in (select agentgroup from labranchgroup where agentgroup='"
          	+mCode+"' or upbranch='"+mCode+"') ";
      }
      if(this.mLevel.equals("3"))
      {
          tSQL += " and agentcode = '000000'";
      }
      try{
          AgentNum = (int) execQuery(tSQL);
      }catch(Exception ex)
      {
          System.out.println("getAgentCountByManageCom 出错！");
      }
      return AgentNum;
  }
  /**
   * 出单人力
   */
  private int getRLNum(String mCode)
  {
      int RLNum = 0;
      String tSQL = "";
      tSQL = "select count(distinct agentcode) from LJAPayPerson b where b.grpcontno='00000000000000000000'" +
            " and b.makedate >='"+this.mStartDate+"' and b.makedate<='"+this.mEndDate+"'" +
                    " and (exists (select contno from lccont c where c.contno= b.contno and c.salechnl='04')" +
                    " or exists (select contno from lbcont d where d.contno=b.contno and d.salechnl='04'))";
      if(this.mLevel.equals("0"))
      {
          tSQL += " and substr(b.managecom,1,4)='"+mCode+"'";
      }
      if(this.mLevel.equals("1"))
      {
          tSQL += " and b.managecom = '"+mCode+"'";
      }
      if(this.mLevel.equals("2"))
      {
          tSQL += " and  b.agentgroup in (select agentgroup from labranchgroup where agentgroup='"
          	+mCode+"' or upbranch='"+mCode+"') ";
      }
      if(this.mLevel.equals("3"))
      {
          tSQL += " and b.agentcode='000000'";
      }
      try{
          RLNum = (int) execQuery(tSQL);
      }catch(Exception ex)
      {
          System.out.println("getRLNum 出错！");
      }
      return RLNum ;

  }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {

        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        mLevel = (String)cInputData.get(0);
        System.out.println("统计层级mLevel:"+mLevel);
        managecom=this.mGlobalInput.ManageCom;
        System.out.println("管理机构为:"+managecom);
        mRiskCode = (String)cInputData.get(1);
        mStartDate = (String)cInputData.get(2);
        mEndDate = (String)cInputData.get(3);
        mPayIntv = (String)cInputData.get(4);
        mPayYears = (String)cInputData.get(5);
       if(mPayIntv!=null&&!mPayIntv.equals(""))
       {
        if(mPayIntv.equals("0"))
        {
            this.mPayTime = 0 ;
        }
        if(mPayIntv.equals("1"))
        {
            this.mPayTime = 12;
        }
        if(mPayIntv.equals("2"))
        {
            this.mPayTime = 1;
        }
       }
       if(mPayYears!=null&&!mPayYears.equals(""))
       {
        if(mPayYears.equals("0"))
            mpayyears = 3;
        else if(mPayYears.equals("1"))
            mpayyears = 5;
        else if(mPayYears.equals("2"))
            mpayyears = 8;
        else if(mPayYears.equals("3"))
            mpayyears = 10;
        else
            mpayyears = 10;
       }
       System.out.println("mPayTime="+mPayTime);
       System.out.println("mpayyears="+mpayyears);
        return true;
    }
    /**
     * 得到登录机构的名称
     */
    private String getName()
    {
        String name = "";
        String SQL = "select name from ldcom where comcode = '"+this.mGlobalInput.ManageCom+"'";
        ExeSQL tExeSQL = new ExeSQL();
        if(tExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "LAChannelReport";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败--->getName()";
            this.mErrors.addOneError(tError);
            return "";
        }
        name = tExeSQL.getOneValue(SQL);
        return name;
    }
    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return double
     */
    private double execQuery(String sql)

    {

        Connection conn;

        conn = null;

        conn = DBConnPool.getConnection();

 

        System.out.println(sql);

 

        PreparedStatement st = null;

        ResultSet rs = null;

        try {

            if (conn == null)return 0.00;

            st = conn.prepareStatement(sql);

            if (st == null)return 0.00;

            rs = st.executeQuery();

            if (rs.next()) {

                return rs.getDouble(1);

            }

            return 0.00;

        } catch (Exception ex) {

            ex.printStackTrace();

            return -1;

        } finally {

            try {

              if (!conn.isClosed()) {

                  conn.close();

              }

              try {

                  st.close();

                  rs.close();

              } catch (Exception ex2) {

                  ex2.printStackTrace();

              }

              st = null;

                rs = null;

                conn = null;

            } catch (Exception e) {}

        }

      }
    public VData getResult()
    {
        return mResult;
    }
}
