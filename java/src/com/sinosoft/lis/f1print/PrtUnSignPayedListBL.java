package com.sinosoft.lis.f1print;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.util.HashMap;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 生成需要被撤销的被保人清单
 * 若险种产生过理赔，则退费为0
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.0
 */
public class PrtUnSignPayedListBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = null;
    private LJTempFeeSet mLJTempFeeSet=null;
    private XmlExport xmlexport = null;
    private VData mResult = null;
    ListTable mtListTable = new ListTable();
    private ExeSQL mExeSQL = new ExeSQL();
    private HashMap mIDType = new HashMap();

    public PrtUnSignPayedListBL()
    {
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }

        //校验数据合法性
        if(!checkData())
        {
            return false;
        }

        //获取打印所需数据
        if(!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            mTransferData = (TransferData) cInputData.
                            getObjectByObjectName("TransferData", 0);
            mLJTempFeeSet = (LJTempFeeSet) cInputData.
                               getObjectByObjectName("LJTempFeeSet", 0);
        }
        catch(Exception e)
        {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }

        return true;
    }

    //获取打印所需数据
    private boolean getPrintData()
    {
        TextTag tag = new TextTag();
        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PrintUnSignPayedList.vts", "printer"); //最好紧接着就初始化xml文档

        //得到公司名
        tag.add("ManageCom", (String) mTransferData.getValueByName("ManageCom"));
        tag.add("StartDate", (String) mTransferData.getValueByName("StartDate"));
        tag.add("EndDate", (String) mTransferData.getValueByName("EndDate"));
        xmlexport.addTextTag(tag);

        ListTable tListTable = getListTable();
        if (tListTable == null)
        {
            return false;
        }
        String[] title = {"序号", "管理机构", "印刷号", "财务收费日期", "投保人号",
                         "投保人姓名", "业务员代码"};

        xmlexport.addListTable(tListTable, title);
//        xmlexport.outputDocumentToFile("C:\\", "");

        mResult = new VData();
        mResult.addElement(xmlexport);

        return true;
    }


    /**
     * 生成清单数据列表
     * @return ListTable
     */
    private ListTable getListTable()
    {
        String[] info = null;
        for (int i = 1; i <= this.mLJTempFeeSet.size(); i++)
        {
            //查询团单下的个人保单信息
            StringBuffer sql = new StringBuffer();
            sql.append("select distinct managecom,otherno,confmakedate,");
            sql.append("(select appntno from lccont where prtno=ljtempfee.otherno union all ");
            sql.append("select appntno from lcgrpcont where prtno=ljtempfee.otherno fetch first 1 rows only), ");
            sql.append("appntname,getUniteCode(agentCode) from ljtempfee  where tempfeeno='"+mLJTempFeeSet.get(i).getTempFeeNo()+"'");
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(sql.toString());
            info = new String[7];
            info[0]=String.valueOf(i);
            if(tSSRS != null && tSSRS.getMaxRow()>0)
            {
                info[1]=tSSRS.GetText(1,1);
                info[2]=tSSRS.GetText(1,2);
                info[3]=tSSRS.GetText(1,3);
                info[4]=tSSRS.GetText(1,4);
                info[5]=tSSRS.GetText(1,5);
                info[6]=tSSRS.GetText(1,6);
                
            }
            mtListTable.add(info);
        }
            
        mtListTable.setName("List");
        return mtListTable;
    }






    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    private boolean checkData()
    {
        return true;
    }

    public static void main(String[] args)
    {
    }
}

