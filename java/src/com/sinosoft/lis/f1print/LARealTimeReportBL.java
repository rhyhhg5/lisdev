package com.sinosoft.lis.f1print;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author LH
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

public class LARealTimeReportBL{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput() ;
    private String mStartDay = "";
    private String mEndDay = "";
    private String mYear = "";
    private String[][] mShowDataList = null;
    private String[] mDataList = null;
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
  /**
   * 传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate)
  {
      // 得到外部传入的数据，将数据备份到本类中
      if (!getInputData(cInputData)) {
          return false;
      }

      // 进行数据的必要验证
      if(!check())
      {
          return false;
      }

      // 进行数据查询
      if (!queryData()) {
          return false;
      }

      return true;
  }

  /**
   * 验证
   * @return boolean
   */
  private boolean check()
  {
      if(!mStartDay.substring(0,4).equals(mEndDay.substring(0,4)))
      {
          buildError("check", "[期初]与[期末]必须是同一年！");
          return false;
      }

      mYear = mStartDay.substring(0,4);

      return true;
  }

  /**
   * 得到表示数据列表
   * @return boolean
   */
  private boolean getDataList()
  {
      String tSQL = "";

      // 1、得到全部已开业的机构
      tSQL  = "select * from ldcom where Sign='1'";
      tSQL += "   and comcode like '86%' and length(trim(comcode))=4";
      tSQL += "   and comcode <> '86000000' and   comgrade='02' order by comcode";
      LDComDB tLDComDB = new LDComDB();
      LDComSet tLDComSet = new LDComDBSet();
      tLDComSet = tLDComDB.executeQuery(tSQL);
      if(tLDComSet.size()==0)
      {
          buildError("queryData", "没有附和条件的机构！");
          return false;
      }

      // 2、查询需要表示的数据
      String[][] tShowDataList = new String[tLDComSet.size()][3];
      for(int i=1;i<=tLDComSet.size();i++)
      {// 循环机构进行统计
          LDComSchema tLDComSchema = new LDComSchema();
          tLDComSchema = tLDComSet.get(i);
          tShowDataList[i-1][0] = tLDComSchema.getName() + "分公司";
          System.out.println(tLDComSchema.getComCode()+"/"+tShowDataList[i-1][0]);

          // 查询需要表示的数据
          if(!queryOneDataList(tLDComSchema.getComCode(),tShowDataList[i-1]))
          {
              System.out.println("["+tLDComSchema.getComCode()+"] 本机构数据查询失败！");
              return false;
          }
          mShowDataList = tShowDataList;
      }

      // 3、进行排序整理

      return true;
  }
/**
 * 查询机构下的保费
 */
  private String getPremByManageCom(String pmStartDate,
                                    String pmEndDate,
                                    String pmComCode)
  {
	 String pmValue="";
	 DecimalFormat tDF = new DecimalFormat("0.##");
	 String SQL = "select sum(Prem) from lcpol " +
	 		        " where managecom like '"+pmComCode+"%'" +
	 				" and signdate>='"+pmStartDate+"'" +
	 				" and signdate<='"+pmEndDate+"'" +
	 				" and renewcount=0" +
	 				" and agentcode in (select agentcode from laagent where branchtype='1' and branchtype2='01')";
	 try{
		 pmValue = "" + tDF.format(execQuery(SQL));
     }catch(Exception ex)
     {
         System.out.println("getFirstYearCaseCount 出错！");
     }
     System.out.println(pmValue);
	 return pmValue;
  }

  /**
   * 查询首年件数
   * @param pmStartDate String
   * @param pmEndDate String
   * @param pmManageCom String
   * @return String
   */
  private String getFirstYearCaseCount(String pmStartDate,String pmEndDate,String pmManageCom)
  {
      String tSQL = "";
      String tRtValue = "";
      DecimalFormat tDF = new DecimalFormat("0.##");

      tSQL  = "select count(distinct ContNo) from lcpol";
      tSQL += " where ManageCom like '" + pmManageCom + "%'";
      tSQL += "   and signdate >= '" + pmStartDate + "'";
      tSQL += "   and signdate <= '" + pmEndDate + "'";
      tSQL += "   and renewCount=0 ";
      tSQL += "   and agentcode in (select agentcode from laagent where branchtype='1' and branchtype2='01')";
      try{
          tRtValue = "" + tDF.format(execQuery(tSQL));
      }catch(Exception ex)
      {
          System.out.println("getFirstYearCaseCount 出错！");
      }

      return tRtValue;
  }



  /**
   * 查询填充表示数据
   * @param pmComCode String
   * @param pmOneDataList String[]
   * @return boolean
   */
  private boolean queryOneDataList(String pmComCode,String[] pmOneDataList)
  {
      try{
          // 0、机构名称
          // 1、保费收入
          pmOneDataList[1] = "" + getPremByManageCom(mStartDay, mEndDay,pmComCode);
          // 2、件数
          pmOneDataList[2] = "" + getFirstYearCaseCount(mStartDay,mEndDay, pmComCode);

      }catch(Exception ex)
      {
          buildError("queryOneDataList", "准备数据时出错！");
          System.out.println(ex.toString());
          return false;
      }

      return true;
  }

  /**
   * 进行数据查询
   * @return boolean
   */
  private boolean queryData()
  {
      String tSQL = "";
      String CurrentDate = PubFun.getCurrentDate();//得到当天日期

      try{
          // 1、查询数据
          if(!getDataList())
          {
              return false;
          }

       System.out.println(mShowDataList.length);
       System.out.println(mShowDataList[0].length);
          this.mDataList = new String[mShowDataList[0].length];

//          // 2、进行排序  先进行保费计划达成率排序 再 进行活动率排名
//          LISComparator tLISComparator = new LISComparator();
//          tLISComparator.setNum(15);
//          Arrays.sort(mShowDataList, tLISComparator);
//          for (int j = 0; j < mShowDataList.length; j++) { // 追加序号
//              mShowDataList[j][16] = "" + (j + 1);
//          }
//
//          tLISComparator = new LISComparator();
//          tLISComparator.setNum(6);
//          Arrays.sort(mShowDataList,tLISComparator);
//          for(int j=0;j<mShowDataList.length;j++)
//          {// 追加序号
//              mShowDataList[j][7] = "" + (j + 1);
//          }

          // 3、追加 合计 行
          if(!setAddRow(mShowDataList.length + 1))
          {
             buildError("queryData", "进行合计计算时出错！");
             return false;
          }

          // 4、设置报表属性
          ListTable tlistTable = new ListTable();
          tlistTable.setName("Report");

          for(int i=0;i<mShowDataList.length;i++)
          {
              tlistTable.add(mShowDataList[i]);
          }
          tlistTable.add(mDataList);

          TextTag texttag = new TextTag();    //新建一个TextTag的实例
          texttag.add("StartYear", this.mStartDay.substring(0,4));   //输入制表时间
          texttag.add("StartMonth", this.mStartDay.substring(5,7));  //输入制表时间
          texttag.add("StartDay", this.mStartDay.substring(8,10));   //输入制表时间
          texttag.add("EndYear", this.mEndDay.substring(0,4));       //输入制表时间
          texttag.add("EndMonth", this.mEndDay.substring(5,7));      //输入制表时间
          texttag.add("EndDay", this.mEndDay.substring(8,10));       //输入制表时间
          texttag.add("CurrentDate", this.currentDate);              //输入制表时间
          texttag.add("CurrentTime", this.currentTime);              //输入制表时间

          XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
          xmlexport.createDocument("LARealTimeReport.vts", "printer"); //最好紧接着就初始化xml文档
          if (texttag.size() > 0)
              xmlexport.addTextTag(texttag);     //添加动态文本标签
          xmlexport.addListTable(tlistTable, mShowDataList[0]); //添加列表
          mResult.addElement(xmlexport);
      }catch (Exception ex)
      {
          buildError("queryData", "LAStatisticReportBL发生错误，准备数据时出错！");
          return false;
      }

      return true;
  }

  /**
   * 追加一条合计行
   * @return boolean
   */
  private boolean setAddRow(int pmRow)
  {
      System.out.println("合计行的行数是："+pmRow);

      mDataList[0] = "合  计";
      mDataList[1] = dealSum(1);
      mDataList[2] = dealSum(2);
      return true;
  }

  /**
   * 对传入的数组进行求和处理
   * @param pmArrNum int
   * @return String
   */
  private String dealSum(int pmArrNum)
  {
      String tReturnValue = "";
      DecimalFormat tDF = new DecimalFormat("0.##");
      String tSQL = "select 0";
      System.out.println("mShowDataList.length" + mShowDataList.length);
      for(int i=0;i<this.mShowDataList.length;i++)
      {
          tSQL += " + " + this.mShowDataList[i][pmArrNum];
      }

      tSQL += " + 0 from dual";

      tReturnValue = "" + tDF.format(execQuery(tSQL));

      return tReturnValue;
  }

  /**
   * 取得传入的数据
   * @return boolean
   */
  private boolean getInputData(VData pmInputData)
  {
      //全局变量
//      mGlobalInput.setSchema((GlobalInput) pmInputData.getObjectByObjectName(
//              "GlobalInput", 0));
      mStartDay = (String) pmInputData.get(0);
      mEndDay = (String) pmInputData.get(1);
      System.out.println(mStartDay+" / "+mEndDay);

      return true;
  }
  /**
   * 追加错误信息
   * @param szFunc String
   * @param szErrMsg String
   */
  private void buildError(String szFunc, String szErrMsg)
  {
      CError cError = new CError();
      cError.moduleName = "LAStatisticReportBL";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      System.out.println(szFunc + "--" + szErrMsg);
      this.mErrors.addOneError(cError);
  }

  /**
   * 执行SQL文查询结果
   * @param sql String
   * @return double
   */
  private double execQuery(String sql)
  {
      Connection conn;
      conn = null;
      conn = DBConnPool.getConnection();

      System.out.println(sql);

      PreparedStatement st = null;
      ResultSet rs = null;
      try {
          if (conn == null)return 0.00;
          st = conn.prepareStatement(sql);
          if (st == null)return 0.00;
          rs = st.executeQuery();
          if (rs.next()) {
              return rs.getDouble(1);
          }
          return 0.00;
      } catch (Exception ex) {
          ex.printStackTrace();
          return -1;
      } finally {
          try {
             if (!conn.isClosed()) {
                 conn.close();
             }
             try {
                 st.close();
                 rs.close();
             } catch (Exception ex2) {
                 ex2.printStackTrace();
             }
             st = null;
             rs = null;
             conn = null;
           } catch (Exception e) {}

      }
    }

  /**
   * 取得返回处理过的结果
   * @return VData
   */
  public VData getResult()
  {
      return this.mResult;
  }
}
