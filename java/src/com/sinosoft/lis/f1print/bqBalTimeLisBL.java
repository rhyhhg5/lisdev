package com.sinosoft.lis.f1print;


import com.sinosoft.lis.bq.ChangeCodeBL;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LPEdorPrintSchema;
import java.io.InputStream;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.bq.BQ;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class bqBalTimeLisBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private ListTable tlistTable = new ListTable();
    private VData mResult = new VData();

    private TextTag texttag = new TextTag(); //新建一个TextTag的实例
    private XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例

    private String mStartDate = "";
    private String mEndDate = "";
    private String mGrpContNo = "";
    private String mWorkNo = "";
    private String mPayMode = "";
    private String mPayEndDate = "";
    private String mBank = "";
    private String mBankAccno = "";
    private String mAccName = "";
    private String mPayDate = "";
    private MMap mMap = new MMap();

    public bqBalTimeLisBL() {
    }

    /**
         传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }
        if (!submit()) {
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "bqBalTimeLisBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        TransferData tTransferData = (TransferData) cInputData.
                                     getObjectByObjectName("TransferData", 0);
        mStartDate = (String) tTransferData.getValueByName("StartDate");
        mEndDate = (String) tTransferData.getValueByName("EndDate");
        mGrpContNo = (String) tTransferData.getValueByName("GrpContNo");
        mWorkNo = (String) tTransferData.getValueByName("WorkNo");
        mPayMode = (String) tTransferData.getValueByName("PayMode");
        mPayEndDate = (String) tTransferData.getValueByName("PayEndDate");
        mBank = (String) tTransferData.getValueByName("Bank");
        mBankAccno = (String) tTransferData.getValueByName("BankAccno");
        mAccName = (String) tTransferData.getValueByName("AccName");
        mPayDate = (String) tTransferData.getValueByName("PayDate");
        System.out.println(mStartDate);
        System.out.println(mEndDate);
        System.out.println(mGrpContNo);
        System.out.println(mWorkNo);
        System.out.println(mPayMode);
        System.out.println(mPayEndDate);
        System.out.println(mBank);
        System.out.println(mBankAccno);
        System.out.println(mAccName);
        System.out.println(mPayDate);
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }


    private boolean getPrintData() {
        System.out.println("\n\nStart Write Print Data\n\n");
        xmlexport.createDocument("bqBalTimeList.vts", "printer"); //最好紧接着就初始化xml文档


        //设置公司名称，地址等固定信息
        this.setFixedInfo();

        texttag.add("StartDate", CommonBL.decodeDate(this.mStartDate));
        texttag.add("EndDate", CommonBL.decodeDate(this.mEndDate));
        texttag.add("GrpContNo", mGrpContNo);
        //根据集体合同号查询集体保单表
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(this.mGrpContNo);
        tLCGrpContDB.getInfo();
        if (tLCGrpContDB.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);
            return false;
        }
        //根据客户号和地址号查询团体客户地址表
        LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
        tLCGrpAddressDB.setCustomerNo(tLCGrpContDB.getAppntNo());
        tLCGrpAddressDB.setAddressNo(tLCGrpContDB.getAddressNo());
        tLCGrpAddressDB.getInfo();
        if (tLCGrpAddressDB.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tLCGrpAddressDB.mErrors);
            return false;
        }

        //qulq 070125 modify 添加业务员信息
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(tLCGrpContDB.getAgentCode());
        tLaAgentDB.getInfo();
        texttag.add("AgentName", tLaAgentDB.getName());
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(tLCGrpContDB.getAgentGroup());
        tLABranchGroupDB.getInfo();
        texttag.add("AgentGroup", tLABranchGroupDB.getName());
        //
        xmlexport.addDisplayControl("displayHead");
        texttag.add("EdorAppZipCode", tLCGrpAddressDB.getGrpZipCode()); //邮政编码
        texttag.add("EdorAppAddress", tLCGrpAddressDB.getGrpAddress()); //通讯地址
        texttag.add("EdorAppName", tLCGrpAddressDB.getLinkMan1()); //收件人
        texttag.add("AppntName", tLCGrpContDB.getGrpName()); //投保单位名称
        texttag.add("CustomerNo", tLCGrpContDB.getAppntNo()); //投保单位名称
        texttag.add("Operator", this.mGlobalInput.Operator); //经办人
        texttag.add("ConfDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        texttag.add("BarCode1", mWorkNo);
        texttag.add("BarCodeParam1", "BarHeight=25&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        //根据集体合同号获取定期结算单数据
        if (!this.setBalTimeLisInfo()) {
            this.buildError("setBalTimeLisInfo", "获取定期结算单数据出错!");
            return false;
        }
        xmlexport.addTextTag(texttag);
        xmlexport.outputDocumentToFile("d:\\", "TaskPrint"); //输出xml文档到文件
        //生成主打印批单schema
        LPEdorPrintSchema tLPEdorPrintSchemaMain = new LPEdorPrintSchema();
        tLPEdorPrintSchemaMain.setEdorNo(this.mWorkNo);
        tLPEdorPrintSchemaMain.setManageCom(mGlobalInput.ManageCom);
        tLPEdorPrintSchemaMain.setPrtFlag("N");
        tLPEdorPrintSchemaMain.setPrtTimes(0);
        tLPEdorPrintSchemaMain.setMakeDate(PubFun.getCurrentDate());
        tLPEdorPrintSchemaMain.setMakeTime(PubFun.getCurrentTime());
        tLPEdorPrintSchemaMain.setOperator(mGlobalInput.Operator);
        tLPEdorPrintSchemaMain.setModifyDate(PubFun.getCurrentDate());
        tLPEdorPrintSchemaMain.setModifyTime(PubFun.getCurrentTime());
        InputStream ins = xmlexport.getInputStream();
        tLPEdorPrintSchemaMain.setEdorInfo(ins);
        mMap.put("delete from lpedorprint where edorno='" + this.mWorkNo + "'",
                 "DELETE");
        mMap.put(tLPEdorPrintSchemaMain, "BLOBINSERT");

        System.out.println("BLOBINSERT");

        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    //设置公司名称，地址等固定信息
    private void setFixedInfo() {
        //查询受理人
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(mWorkNo);
        tLGWorkDB.getInfo();

        //查询受理机构
        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(tLGWorkDB.getAcceptorNo());
        tLDUserDB.getInfo();

        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLDUserDB.getComCode());
        tLDComDB.getInfo();

        texttag.add("ServicePhone", tLDComDB.getServicePhone());
        texttag.add("ServiceFax", tLDComDB.getFax());
        texttag.add("ServiceAddress", tLDComDB.getLetterServicePostAddress());
        texttag.add("ServiceZip", tLDComDB.getLetterServicePostZipcode());
        texttag.add("ComName", tLDComDB.getLetterServiceName());
        texttag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
    }

    //根据集体合同号获取定期结算单数据
    private boolean setBalTimeLisInfo() {
        LCGrpBalPlanDB tLCGrpBalPlanDB = new LCGrpBalPlanDB();
        tLCGrpBalPlanDB.setGrpContNo(this.mGrpContNo);
        tLCGrpBalPlanDB.getInfo();
        if (tLCGrpBalPlanDB.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tLCGrpBalPlanDB.mErrors);
            return false;
        }
        texttag.add("BalTimes",
                    ChangeCodeBL.getCodeName("balintv",
                                             String.
                                             valueOf(tLCGrpBalPlanDB.getBalIntv())));
        texttag.add("NextDate", CommonBL.decodeDate(tLCGrpBalPlanDB.getNextDate()));
        String sql = "select ActuGetNo no,'1',BankCode,AccName,BankAccNo,StartGetDate from ljaget where OtherNo='" +
                     this.mWorkNo +
                     "' and OtherNoType='13' "
                     + " union "
                     + "select GetNoticeNo no,'0',BankCode,AccName,BankAccNo,StartPayDate from ljspay where OtherNo='" +
                     this.mWorkNo + "' and OtherNoType='13' order by no desc";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);
        System.out.println(sql);
        //判断是否有新生成的定期结算单
        if (tSSRS.getMaxRow() > 0) {
            String tActuGetNo = tSSRS.GetText(1, 1);
            String tFlag = tSSRS.GetText(1, 2); //此字符可判断收退费通知书
//            String tBankCode = tSSRS.GetText(1, 3); //银行代码
//            String tAccName = tSSRS.GetText(1, 4); //银行帐号
//            String tBankAccNo = tSSRS.GetText(1, 5); //帐户名
//            String tDate = tSSRS.GetText(1, 6); //最早收退费日期
            sql =
                    "select b.EdorAppDate,b.edorno,b.EdorType,b.getMoney Money from lpgrpedoritem b "
                    + "where b.grpcontno='" + this.mGrpContNo +
                    "' and b.EdorAcceptNo in "
                    + "( select IncomeNo from ljapay where PayNo in( "
                    + "select BTActuNo from LJAEdorBalDetail where ActuNo='" +
                    tActuGetNo + "' and flag ='" + tFlag + "' and BTFlag='0'"
                    + ")  and IncomeType='J' union"
                    + " select OtherNo from LJAGet where ActuGetNo in("
                    + " select BTActuNo from LJAEdorBalDetail where ActuNo='" +
                    tActuGetNo + "' and flag ='" + tFlag + "' and BTFlag='1'"
                    + " )  and paymode='J') "+
            		" union all " +
            		" select b.edorappdate,b.edoracceptno,'投保人帐户抵充',a.getmoney Money from ljagetendorse a,lpedorapp b " +
            		" where   a.endorsementno=b.edoracceptno" +
            		" and ((a.feefinatype='YEO' and a.feeoperationtype='YEO') or (a.feefinatype='YEI' and a.feeoperationtype='YEI')) " +
            		" and exists (select 1 from lcappacctrace where otherno=a.endorsementno)" +
            		" and a.endorsementno in " 
            		+ "( select IncomeNo from ljapay where PayNo in( "
                    + "select BTActuNo from LJAEdorBalDetail where ActuNo='" +
                    tActuGetNo + "' and flag ='" + tFlag + "' and BTFlag='0'"
                    + ")  and IncomeType='J' union"
                    + " select OtherNo from LJAGet where ActuGetNo in("
                    + " select BTActuNo from LJAEdorBalDetail where ActuNo='" +
                    tActuGetNo + "' and flag ='" + tFlag + "' and BTFlag='1'"
                    + " )  and paymode='J') " +
                    		"  " ;
            tlistTable.setName("BAL");
            tSSRS = tExeSQL.execSQL(sql);
            for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                String[] strArr = new String[4];
                strArr[0] = tSSRS.GetText(i, 1);
                strArr[1] = tSSRS.GetText(i, 2);
                strArr[2] = tSSRS.GetText(i, 3);
                strArr[3] = tSSRS.GetText(i, 4);
                tlistTable.add(strArr);
            }
            sql = "select sum(Money) from (" + sql + ") as y ";
            String money = tExeSQL.getOneValue(sql);
            texttag.add("SumMoney", money);
            texttag.add("ActSumMoney", money); // 此处应为本期实际收退费 0-交费，1-退费
            if (tFlag.equals("0")) {
                /**
                 *收费,需要先判断原来的交费方式,原来不是银行转帐,现在不是,直接修改ljspay 的StartPayDate
                 *                                         现在是银行转帐则要插入ljtempfee,ljtempfeeclass
                 *原来是银行转帐,现在也是,直接修改ljtempfee的PayDate
                 *             现在不是的话,删除ljtempfee,ljtempfeeclass,修改ljspay的StartPayDate
                 */
                String strSQL = "select * from ljtempfee where TempFeeNo = '" +
                                tActuGetNo + "'";
                LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
                LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.executeQuery(strSQL);
                if (tLJTempFeeSet.size() != 1) {
                    //原来不是银行转帐
                    if (this.mPayMode.equals("4")) {
                        //现在是银行转帐则要插入ljtempfee,ljtempfeeclass
                        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
                        tLJTempFeeSchema.setTempFeeNo(tActuGetNo);
                        tLJTempFeeSchema.setTempFeeType("4"); //保全
                        tLJTempFeeSchema.setOtherNo(this.mWorkNo);
                        tLJTempFeeSchema.setRiskCode(BQ.FILLDATA);
                        tLJTempFeeSchema.setOtherNoType("13");
                        tLJTempFeeSchema.setPayDate(mPayDate);
                        tLJTempFeeSchema.setPayMoney(money);
                        tLJTempFeeSchema.setManageCom(mGlobalInput.ManageCom);
                        tLJTempFeeSchema.setConfFlag("0");
                        tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
                        tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
                        tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
                        tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
                        tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
                        mMap.put(tLJTempFeeSchema, "INSERT");

                        //设置暂交费分类表
                        LJTempFeeClassSchema tLJTempFeeClassSchema = new
                                LJTempFeeClassSchema();
                        tLJTempFeeClassSchema.setTempFeeNo(tActuGetNo);
                        tLJTempFeeClassSchema.setPayMode(mPayMode); //4是银行转帐
                        tLJTempFeeClassSchema.setPayDate(mPayDate);
                        tLJTempFeeClassSchema.setPayMoney(money);
                        tLJTempFeeClassSchema.setManageCom(mGlobalInput.
                                ManageCom);
                        tLJTempFeeClassSchema.setBankCode(mBank);
                        tLJTempFeeClassSchema.setBankAccNo(mBankAccno);
                        tLJTempFeeClassSchema.setAccName(mAccName);
                        tLJTempFeeClassSchema.setOperator(mGlobalInput.Operator);
                        tLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
                        tLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
                        tLJTempFeeClassSchema.setModifyDate(PubFun.
                                getCurrentDate());
                        tLJTempFeeClassSchema.setModifyTime(PubFun.
                                getCurrentTime());
                        mMap.put(tLJTempFeeClassSchema, "INSERT");
                        mMap.put("update ljspay set PayDate = '" +
                                 this.mPayDate + "' where GetNoticeNo = '" +
                                 tActuGetNo + "'", "UPDATE");
                    } else {
                        mMap.put("update ljspay set PayDate = '" +
                                 this.mPayEndDate + "' where GetNoticeNo = '" +
                                 tActuGetNo + "'", "UPDATE");
                    }
                } else {
                    //原来是银行转帐
                    if (this.mPayMode.equals("4")) {
                        mMap.put("update ljtempfee set paydate = '" +
                                 this.mPayDate + "' where tempfeeno = '" +
                                 tActuGetNo + "'", "UPDATE");
                    } else {
                        mMap.put("update ljspay set PayDate = '" +
                                 this.mPayEndDate +
                                 "', BankCode = null , AccName = null , BankAccNo = null " +
                                 " where GetNoticeNo = '" +
                                 tActuGetNo + "'", "UPDATE");
                        mMap.put("delete from ljtempfee where tempfeeno = '" +
                                 tActuGetNo + "'", "DELETE");
                        mMap.put(
                                "delete from ljtempfeeclass where tempfeeno = '" +
                                tActuGetNo + "'", "DELETE");
                    }
                }
                addContMoneyInfo(money);
                addPayNoticeNo(tActuGetNo);
                addPayMoneyInfo(Double.parseDouble(money));
                addPayMode(Double.parseDouble(money), mBank, mAccName,
                           mBankAccno, mPayDate);

            } else if (tFlag.equals("1")) {
                /**
                 * 退费,直接修改ljaget中的paymode ,BankCode,AccName,BankAccNo,StartGetDate
                 *
                 */
                if (this.mPayMode.equals("4")) {
                    mMap.put("update ljaget set paymode = '" + this.mPayMode +
                             "' , BankCode = '" + this.mBank +
                             "' , AccName = '" + this.mAccName
                             + "' , BankAccNo = '" + this.mBankAccno +
                             "' , StartGetDate = '" + this.mPayDate +
                             "' where ActuGetNo = '" + tActuGetNo + "'",
                             "UPDATE");
                } else {
                    mMap.put("update ljaget set paymode = '" + this.mPayMode +
                             "' where ActuGetNo = '" + tActuGetNo + "'",
                             "UPDATE");
                }
                addContMoneyInfo(money);
                addGetNoticeNo(tActuGetNo);
                addGetMoneyInfo(Double.parseDouble(money));
                addGetMode(Double.parseDouble(money), mBank, mAccName,
                           mBankAccno, mPayDate);

            } else {
                this.texttag.add("Blank_Info", "（以下信息空白）");
                return true;
            }
            String[] strArrHead1 = new String[4];
            strArrHead1[0] = "发生日期";
            strArrHead1[1] = "批单号";
            strArrHead1[2] = "保全项目";
            strArrHead1[3] = "收退费金额";
            xmlexport.addListTable(this.tlistTable, strArrHead1);

        } else {
            texttag.add("SumMoney", "0");
            texttag.add("ActSumMoney", "0"); // 此处应为本期实际收退费
            this.texttag.add("Blank_Info", "（以下信息空白）");
            return true;
        }

        return true;
    }

    private void addContMoneyInfo(String money) {
        this.xmlexport.addDisplayControl("displayCash");
        String contMoney = new String();
        if (Double.parseDouble(money) > 0) {
            contMoney = "保单号：" + mGrpContNo + "，应收" + money + "元。";
        } else if (Double.parseDouble(money) < 0) {
            contMoney = "保单号：" + mGrpContNo + "，应退" +
                        Math.abs(Double.parseDouble(money)) + "元。";
        }
        this.texttag.add("FeeTotalInfo", contMoney);
    }

    private boolean addPayNoticeNo(String aActuGetNo) {
        //得到收费记录号
        texttag.add("GetNoticeNo", "收费通知书号：" + aActuGetNo);
        return true;
    }

    private void addGetNoticeNo(String aActuGetNo) {
        //得到退费记录号
        texttag.add("GetNoticeNo", "退费通知书号：" + aActuGetNo);
    }

    private void addPayMoneyInfo(double money) {
        texttag.add("FeeInfo", "收费通知书");
        texttag.add("SumMoney", "本次应收费合计:" + money + "元。");
    }

    private void addGetMoneyInfo(double money) {
        texttag.add("FeeInfo", "退费通知书");
        texttag.add("SumMoney", "本次应退费合计:" + Math.abs(money) + "元。");
    }

    /**
     * 添加退费方式
     * @param money double
     */
    private void addGetMode(double money, String tBankCode, String tAccName,
                            String tBankAccNo, String tDate) {
        String payMode = this.mPayMode;
        String payDate = tDate; //转帐日期
        //String endDate = ; //截止日期
        String bank = tBankCode;
        String bankAccno = tBankAccNo;
        String outPayMode = ""; //退费方式
        //现金
        if (payMode.equals("1")) {
            outPayMode = "退费方式为：现金，请凭本通知书到我公司领款。";
        }
        //现金支票
        if (payMode.equals("2")) {
            outPayMode = "退费方式为：现金支票，请凭本通知书到我公司领款。";
        }
        //转帐支票
        if (payMode.equals("3")) {
            outPayMode = "退费方式为：转帐支票，请凭本通知书到我公司领款。";
        }
        //银行转帐
        else if (payMode.equals("4")) {
            outPayMode = "退费方式为：银行转帐，交易银行为：" +
                         ChangeCodeBL.getCodeName("Bank", bank, "BankCode") +
                         "，交易账号为：" + bankAccno +
                         "，转帐总金额为：" + Math.abs(money) +
                         "元。\n我公司将在" + CommonBL.decodeDate(payDate) +
                         "进行转账，请注意查收。";
        }
        texttag.add("PayMode", outPayMode);
    }

    /**
     * 添加交费方式
     * @param money double
     */
    private void addPayMode(double money, String tBankCode, String tAccName,
                            String tBankAccNo, String tDate) {
        String payMode = this.mPayMode;
        String payDate = tDate; //转帐日期
        String endDate = this.mPayEndDate; //截止日期
        String bank = tBankCode;
        String bankAccno = tBankAccNo;
        String outPayMode = ""; //交费方式
        //现金
        if (payMode.equals("1")) {
            outPayMode = "您的交费方式为：现金，请凭本通知书在" +
                         CommonBL.decodeDate(endDate) +
                         "前到我公司交费。";
        }
        //现金支票
        else if (payMode.equals("2")) {
            outPayMode = "您的交费方式为：现金支票，请凭本通知书在" +
                         CommonBL.decodeDate(endDate) +
                         "前到我公司交费。";
        }
        //转帐支票
        else if (payMode.equals("3")) {
            outPayMode = "您的交费方式为：转帐支票，请凭本通知书在" +
                         CommonBL.decodeDate(endDate) +
                         "前到我公司交费。";
        }
        //银行转帐
        else if (payMode.equals("4")) {
            outPayMode = "您的交费方式为：银行转帐，交费银行为：" +
                         ChangeCodeBL.getCodeName("Bank", bank, "BankCode") +
                         "，账号为：" + bankAccno +
                         "，转帐总金额为：" + Math.abs(money) +
                         "元。\n我公司将在" + CommonBL.decodeDate(payDate) +
                         "进行转账，请在转账日前确认该账号是否有足够金额。";
        }
        texttag.add("PayMode", outPayMode);
    }

    private boolean submit() {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }


    public static void main(String[] args) {

        bqBalTimeLisBL tbqBalTimeLisBL = new bqBalTimeLisBL();
        VData tVData = new VData();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("StartDate", "2006-01-17");
        tTransferData.setNameAndValue("EndDate", "2006-02-17");
        tTransferData.setNameAndValue("GrpContNo", "0000002701");
        tVData.addElement(tTransferData);
        GlobalInput tGlobalInput = new GlobalInput();

        tGlobalInput.ManageCom = "8611";
        tGlobalInput.Operator = "endor";
        tVData.addElement(tGlobalInput);

        tbqBalTimeLisBL.submitData(tVData, "PRINT");

        VData vdata = tbqBalTimeLisBL.getResult();

    }

}
