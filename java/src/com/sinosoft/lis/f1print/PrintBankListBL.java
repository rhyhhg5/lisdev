package com.sinosoft.lis.f1print;

/**
 * <p>Title:PrintBankListBL </p>
 *
 * <p>Description: 打印所有银行列表生成XML程序</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author: zhangjun
 * @version 1.0
 */
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.StrTool;
import com.sinosoft.lis.pubfun.GlobalInput;
public class PrintBankListBL {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mCanSendBank;
    public PrintBankListBL()
    {
    }
    /**
      传输数据的公共方法
      */
     public boolean submitData(VData cInputData, String cOperate)
     {
         if (!cOperate.equals("PRINT"))
         {
             buildError("submitData", "不支持的操作字符串");
             return false;
         }

         // 得到外部传入的数据，将数据备份到本类中
         if (!getInputData(cInputData))
         {
             return false;
         }
         mResult.clear();
         // 准备所有要打印的数据
         if (!getPrintData())
         {
             return false;
         }
         return true;
     }


     private boolean getInputData(VData cInputData)
     {
         System.out.println("getInputData begin");
         mCanSendBank = (String) cInputData.get(0);
         mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                 "GlobalInput", 0));
         mCanSendBank = StrTool.cTrim(mCanSendBank);
         if (mGlobalInput == null)
         {
             buildError("getInputData", "没有得到足够的信息！");
             return false;
         }
        return true;
     }

     public VData getResult()
     {
         return this.mResult;
     }

     private void buildError(String szFunc, String szErrMsg)
     {
         CError cError = new CError();
         cError.moduleName = "PayNoticeF1PBL";
         cError.functionName = szFunc;
         cError.errorMessage = szErrMsg;
         this.mErrors.addOneError(cError);
     }

     private boolean getPrintData()
     {

         System.out.println("查询银行名称的SQL！！！");
         String MangeComNum="";
         if (mGlobalInput.ManageCom.length()>2)
         {
             MangeComNum = mGlobalInput.ManageCom.substring(0,4);
         }
         else
         {
             MangeComNum = mGlobalInput.ManageCom;
         }
         String sql = "select a.bankcode,a.bankname,b.Name from ldbank a,ldcom b where 1=1 "
                    + "and a.comcode like '"+MangeComNum+"%' "
                    + "and b.comcode = a.comcode ";
               if (!mCanSendBank.equals(""))
               {
                   sql= sql + "and cansendflag = '" + mCanSendBank + "' "
                            + "order by a.bankcode";
               }else
               {
                   sql = sql + "order by a.bankcode";
               }
         ExeSQL mExeSQL = new ExeSQL();
         SSRS mSSRS = new SSRS();
         mSSRS = mExeSQL.execSQL(sql);
         int bank_count = mSSRS.getMaxNumber();
         if (bank_count == 0)
         {
             CError tError = new CError();
             tError.moduleName = "PrintBillBL";
             tError.functionName = "submitData";
             tError.errorMessage = "银行信息表查询失败，没有该银行代码！！！";
             this.mErrors.addOneError(tError);
             return false;
         }
            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("PrintBankList.vts", "printer");
            ListTable alistTable = new ListTable();
            String strArr[] = null;
            alistTable.setName("BANK");
         for (int i=1;i<=mSSRS.getMaxRow();i++)
         {
             strArr = new String[3];
             for(int j=1;j<=mSSRS.getMaxCol();j++)
             {
                 if (j == 1)
                 {
                    strArr[j - 1] = mSSRS.GetText(i, j);
                 }
                 if (j == 2)
                 {
                    strArr[j - 1] = mSSRS.GetText(i, j);
                 }
                 if (j == 3)
                 {
                    strArr[j - 1] = mSSRS.GetText(i, j);
                 }
             }
              alistTable.add(strArr);
          }
          strArr = new String[3];
          strArr[0] = "mBankCode";
          strArr[1] = "mBankName";
          strArr[2] = "mMngName";
          if (texttag.size() > 0)
          {
             xmlexport.addTextTag(texttag);
          }
          xmlexport.addListTable(alistTable, strArr);
          xmlexport.outputDocumentToFile("e:\\","PrintBankListBL");//输出xml文档到文件
          mResult.clear();
          mResult.addElement(xmlexport);

         return true;
     }


     public static void main(String[] args)
     {
         GlobalInput mG = new GlobalInput();
         mG.Operator = "001";
         mG.ManageCom = "8611";
         String tCanSendBank = "";

         String tMngCom = "86";
         VData tVData = new VData();
         tVData.addElement(tCanSendBank);
         tVData.addElement(mG);

         PrintBankListBL tPrintBankListBL = new PrintBankListBL();
         tPrintBankListBL.submitData(tVData, "PRINT");
    }
}
