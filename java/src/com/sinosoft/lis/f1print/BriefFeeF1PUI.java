package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LOPRTManager2Schema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.vschema.LJAPayPersonSet;

public class BriefFeeF1PUI implements PrintService
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //private VData mInputData = new VData();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LCContSchema mLCContSchema = new LCContSchema();

    private LJAPaySchema mLJAPaySchema = new LJAPaySchema();//实收总表

    private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();//个人实收表

    private TransferData mTransferData = new TransferData();//接受数据

    private String mOperate = "";//操作类型

    private LOPRTManager2Schema mLOPRTManager2Schema = new LOPRTManager2Schema();

    public BriefFeeF1PUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("44行！");
        mOperate = cOperate;
        System.out.println("mOperate=" + mOperate);
        if (!cOperate.equals("CONFIRM") && !cOperate.equals("PRINT")
                && !cOperate.equals("REPRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        // 进行业务处理
        if (!dealData())
        {
            return false;
        }

        // 准备传往后台的数据
        VData vData = new VData();

        if (!prepareOutputData(vData))
        {
            return false;
        }

        BriefFeeF1PBL tBriefFeeF1PBL = new BriefFeeF1PBL();
        System.out.println("Start tBriefFeeF1P UI Submit ...");

        if (!tBriefFeeF1PBL.submitData(vData, cOperate))
        {
            if (tBriefFeeF1PBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tBriefFeeF1PBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "tBriefFeeF1PBL发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tBriefFeeF1PBL.getResult();
            return true;
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            if (mOperate.equals("PRINT") || mOperate.equals("REPRINT"))
            {
                vData.add(mGlobalInput);
                vData.add(mLOPRTManager2Schema);
                vData.add(mTransferData);
            }
            if (mOperate.equals("CONFIRM"))
            {
                vData.add(mGlobalInput);
                vData.add(mLCContSchema);
                vData.add(mTransferData);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        if (mOperate.equals("PRINT") || mOperate.equals("REPRINT"))
        {
            mGlobalInput.setSchema((GlobalInput) cInputData
                    .getObjectByObjectName("GlobalInput", 0));
            mLOPRTManager2Schema.setSchema((LOPRTManager2Schema) cInputData
                    .getObjectByObjectName("LOPRTManager2Schema", 0));
        }

        if (mOperate.equals("CONFIRM"))
        {
            //全局变量
            System.out.println("UI 137行");
            mGlobalInput.setSchema((GlobalInput) cInputData
                    .getObjectByObjectName("GlobalInput", 0));
            System.out.println("UI 140行");
            mLCContSchema.setSchema((LCContSchema) cInputData
                    .getObjectByObjectName("LCContSchema", 0));
            System.out.println("LCContSchema  OK !");

        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "BriefFeeF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        LJAPaySchema a = new LJAPaySchema();
        LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
        a.setPayNo("32000308751");
        LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
        tLJAPayPersonSchema.setPayNo("32000308751");
        tLJAPayPersonSchema.setContNo("23000000720");
        tLJAPayPersonSet.add(tLJAPayPersonSchema);
        // tLJAPayPersonSchema = new LJAPayPersonSchema();
        // tLJAPayPersonSchema.setPayNo("32000308166");
        //tLJAPayPersonSchema.setContNo("17000011165");
        // tLJAPayPersonSet.add(tLJAPayPersonSchema);

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("AppntName", "李引平");
        tTransferData.setNameAndValue("RiskName", "福佑专家交通工具个人意外伤害保险");
        tTransferData.setNameAndValue("HPerson", "");
        tTransferData.setNameAndValue("CPerson", "");
        tTransferData.setNameAndValue("Remark", "福佑专家交通工具个人意外伤害保险");
        GlobalInput g = new GlobalInput();
        g.ManageCom = "8632";
        VData tVData = new VData();
        tVData.addElement(a);
        tVData.addElement(g);
        tVData.addElement(tLJAPayPersonSet);

        tVData.addElement(tTransferData);

        BriefFeeF1PUI u = new BriefFeeF1PUI();
        u.submitData(tVData, "CONFIRM");
        VData result = new VData();
        result = u.getResult();
    }
}
