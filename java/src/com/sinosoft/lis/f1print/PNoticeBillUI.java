package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author 刘岩松
 * @version 1.0
 * @date 2003-04-16
 * @function print notice bill User Interface Layer
 */

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PNoticeBillUI
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String strMngCom;
    //private String strAgentCode;
    private String strStartDate;
    private String strEndDate;
    //private String strFlag;
    //private String strStateFlag;
    private String strSQL;


    public PNoticeBillUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("PRINT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            if (!getInputData(cInputData))
            {
                return false;
            }
            VData vData = new VData();

            if (!prepareOutputData(vData))
            {
                return false;
            }

            PNoticeBillBL tPNoticeBillBL = new PNoticeBillBL();
            System.out.println("Start PNoticeBill UI Submit ...");
            if (!tPNoticeBillBL.submitData(vData, cOperate))
            {
                if (tPNoticeBillBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tPNoticeBillBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData", "PNoticeBillBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tPNoticeBillBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "PEfficiencyUI";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
    }

    private boolean prepareOutputData(VData vData)
    {
        vData.clear();
        vData.add(strMngCom);
        //vData.add(strAgentCode);
        vData.add(strStartDate);
        vData.add(strEndDate);
        // vData.add(strFlag);
        //vData.add(strStateFlag);
        vData.add(strSQL);

        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        strMngCom = (String) cInputData.get(0);
        //strAgentCode = (String)cInputData.get(1);
        strStartDate = (String) cInputData.get(1);
        strEndDate = (String) cInputData.get(2);
        //strFlag = (String)cInputData.get(4);
        // strStateFlag = (String)cInputData.get(5);
        strSQL = (String) cInputData.get(3);

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FinChargeDayModeF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
    }
}