package com.sinosoft.lis.f1print;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author JavaBean
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;

import java.util.Vector;
import java.text.DecimalFormat;
import com.sinosoft.lis.f1print.*;
import java.lang.*;

public class LLCustomerPayRateBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    private String mFileNameB="";
	private String mMakeDate = "";
	private PubFun mPubFun = new PubFun();
	private String mOperator = "";
    public LLCustomerPayRateBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {

        System.out.println("Come to RiskPayRateBL submitData()..........");

        if (!cOperate.equals("PRINT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }else{
			TransferData tTransferData= new TransferData();
			tTransferData.setNameAndValue("tFileNameB",mFileNameB );
			tTransferData.setNameAndValue("tMakeDate", mMakeDate);
			tTransferData.setNameAndValue("tOperator", mOperator);
			LLPrintSave tLLPrintSave = new LLPrintSave();
			VData tVData = new VData();
			tVData.addElement(tTransferData);
			if(!tLLPrintSave.submitData(tVData,"")){
				this.buildError("LLPrintSave-->submitData", "数据不完整");
				return false;
			     }
		}
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("Come to RiskPayRateBL getInputData()..........");
        //全局变量
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        if (mTransferData == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        mFileNameB = (String)mTransferData.getValueByName("tFileNameB");
        mOperator = mGlobalInput.Operator;
        mMakeDate = PubFun.getCurrentDate();
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LLRiskPayRateBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData() {
        System.out.println("Come to LLRiskPayRateBL getPrintData().................");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        //统计的起讫时间
        String StartDate   = (String) mTransferData.getValueByName("Begin");
        String EndDate     = (String) mTransferData.getValueByName("End");
        String RiskCode    = (String) mTransferData.getValueByName("RiskCode");
        String RiskName    = (String) mTransferData.getValueByName("RiskName");
        String ManageCom   = (String) mTransferData.getValueByName("ManageCom");
        String HandlerN    = (String) mTransferData.getValueByName("Handler");
        String CurrentDateN   = (String) mTransferData.getValueByName("CurrentDate");
        String RiskNameN   = "";

        System.out.println("***********HandlerN  "+HandlerN);
        System.out.println("***********CurrentDateN "+CurrentDateN);
        try{
            SSRS tSSRS = new SSRS();
            String sql="select riskname from lmrisk where riskcode='"+ RiskCode +"'";
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            for (int i = 0; i < count; i++) {
                String temp[][] = tSSRS.getAllData();
                System.out.println("Execute testSql ..............." + i);
                String[] strCol;
                strCol = new String[1];
                strCol[0] = temp[i][0];

                RiskNameN=strCol[0];
            }
        }
        catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "LLRiskPayRateBL";
            tError.functionName = "";
            tError.errorMessage = "险种名称查询失败";
            this.mErrors.addOneError(tError);
        }

        //险种档次信息
        ListTable MultTable = new ListTable();
        MultTable.setName("MULT");
        String[] Title = {"col1", "col2", "col3"};//, "col4","col5", "col6","col7","col8", "col9", "col10", "col11","col12", "col13","col14"};

        try {
            SSRS tSSRS = new SSRS();
            String sql =  "select B , M ,"
            + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
            + " from "
            + " (select distinct a.customerno A, a.grpname B, "
            + "  (select name from ldcom a where a.comcode=b.managecom)  M, "
            + "   (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
            + "   where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
            + "    and c1.appntno=b.appntno and b1.riskcode='" + RiskCode + "' "
            + "    and a1.rgtdate between '"+ StartDate+"' and '"+EndDate+"' ) C, "
            + "   (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+EndDate+"')-to_date('"+StartDate+"'))/365 "
            + "    from lcgrpcont a1 , lcpol b1 "
            + "    where a1.grpcontno=b1.grpcontno and b1.riskcode='" + RiskCode + "' "
            + "     and a1.appntno=a.customerno and DATE(b1.ENDDATE)>=DATE('"+StartDate+"') AND DATE(b1.CVALIDATE)<=DATE('"+EndDate+"') "
            + "   ) D "
            + " from ldgrp a, lcgrpcont b, lcpol c "
            + " where a.customerno = b.appntno and b.managecom like '"+ManageCom+"%' and b.appflag='1' "
            + "   and c.grpcontno=b.grpcontno and c.riskcode='"+RiskCode+"' "
            + " order by a.customerno "
            + " ) as x "
            ;


            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();

            for (int i = 0; i < count; i++) {
                String temp[][] = tSSRS.getAllData();
                System.out.println("Execute testSql ..............."+i);
                String[] strCol;
                strCol = new String[3];
                strCol[0] = temp[i][0];
                strCol[1] = temp[i][1];
                strCol[2] = temp[i][2];
                MultTable.add(strCol);
            }
        } catch (Exception e) {
            CError tError = new CError();
            tError.moduleName = "LLRiskPayRateBL";
            tError.functionName = "";
            tError.errorMessage = "客户基本信息查询失败";
            this.mErrors.addOneError(tError);
        }

        //得到行数
        int rowNum=0;
        try{
           SSRS tSSRS = new SSRS();
           String sql=
                    "select  distinct a.customerno A, a.grpname B from ldgrp a, lcgrpcont b ,lcpol c "
                    + "   where a.customerno = b.appntno and b.managecom like '"+ManageCom+"%' and b.appflag='1' "
                    + "  and c.grpcontno=b.grpcontno and c.riskcode='"+RiskCode+"' " ;
           ExeSQL tExeSQL = new ExeSQL();
           tSSRS = tExeSQL.execSQL(sql);
           rowNum = tSSRS.getMaxRow();
       }
       catch (Exception e) {
           CError tError = new CError();
           tError.moduleName = "LLRiskPayRateBL";
           tError.functionName = "";
           tError.errorMessage = "险种名称查询失败";
           this.mErrors.addOneError(tError);
       }



        ListTable MonMult1Table = new ListTable();
        MonMult1Table.setName("MONMULT1");

        ListTable MonMult2Table = new ListTable();
        MonMult2Table.setName("MONMULT2");
        ListTable MonMult3Table = new ListTable();
        MonMult3Table.setName("MONMULT3");
        ListTable MonMult4Table = new ListTable();
        MonMult4Table.setName("MONMULT4");
        ListTable MonMult5Table = new ListTable();
        MonMult5Table.setName("MONMULT5");
        ListTable MonMult6Table = new ListTable();
        MonMult6Table.setName("MONMULT6");
        ListTable MonMult7Table = new ListTable();
        MonMult7Table.setName("MONMULT7");
        ListTable MonMult8Table = new ListTable();
        MonMult8Table.setName("MONMULT8");
        ListTable MonMult9Table = new ListTable();
        MonMult9Table.setName("MONMULT9");
        ListTable MonMult10Table = new ListTable();
        MonMult10Table.setName("MONMULT10");
        ListTable MonMult11Table = new ListTable();
        MonMult11Table.setName("MONMULT11");
        ListTable MonMult12Table = new ListTable();
        MonMult12Table.setName("MONMULT12");

        String[] Title_1 = {"a1"};
        String[] Title_2 = {"a1"};
        String[] Title_3 = {"a1"};
        String[] Title_4 = {"a1"};
        String[] Title_5 = {"a1"};
        String[] Title_6 = {"a1"};
        String[] Title_7 = {"a1"};
        String[] Title_8 = {"a1"};
        String[] Title_9 = {"a1"};
        String[] Title_10 = {"a1"};
        String[] Title_11 = {"a1"};
        String[] Title_12 = {"a1"};

        String[][] monthLong = new String[12][2];
        monthLong=getMonthLong(StartDate,EndDate);
/**************************************************************************************************************/

//1月赔付率
MonMult1Table.setName("MONMULT1");
try {
     if(!monthLong[0][0].equals("-")||!monthLong[0][1].equals("-")) // //+ monthLong[0][0] + "' and '" + monthLong[0][1] + "' "
     {
         SSRS tSSRS = new SSRS();
         String sql =  "select B , M ,"
            + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
            + " from "
            + " (select distinct a.customerno A, a.grpname B, "
            + "  (select name from ldcom a where a.comcode=b.managecom)  M, "
            + "   (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
            + "   where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
            + "    and c1.appntno=b.appntno and b1.riskcode='"+RiskCode+"' "
            + "    and a1.rgtdate between '"+ monthLong[0][0]+"' and '"+monthLong[0][1]+"' ) C, "
            + "   (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[0][1]+"')-to_date('"+monthLong[0][0]+"'))/365 "
            + "    from lcgrpcont a1 , lcpol b1 "
            + "    where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
            + "     and a1.appntno=a.customerno and DATE(b1.ENDDATE)>=DATE('"+monthLong[0][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[0][1]+"') "
            + "   ) D "
            + " from ldgrp a, lcgrpcont b, lcpol c "
            + " where a.customerno = b.appntno and b.managecom like '"+ManageCom+"%' and b.appflag='1' "
            + "   and c.grpcontno=b.grpcontno and c.riskcode='"+RiskCode+"' "
            + " order by a.customerno "
            + " ) as x "
            ;


            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            System.out.println(count);
            for (int i = 0; i < count; i++) {
              String temp[][] = tSSRS.getAllData();

            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult1Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult1Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}

//2月赔付率
MonMult2Table.setName("MONMULT2");
try {
    if(!monthLong[1][0].equals("-")||!monthLong[1][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql =  "select B , M ,"
            + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
            + " from "
            + " (select distinct a.customerno A, a.grpname B, "
            + "  (select name from ldcom a where a.comcode=b.managecom)  M, "
            + "   (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
            + "   where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
            + "    and c1.appntno=b.appntno and b1.riskcode='"+RiskCode+"' "
            + "    and a1.rgtdate between '"+ monthLong[1][0]+"' and '"+monthLong[1][1]+"' ) C, "
            + "   (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[1][1]+"')-to_date('"+monthLong[1][0]+"'))/365 "
            + "    from lcgrpcont a1 , lcpol b1 "
            + "    where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
            + "     and a1.appntno=a.customerno and DATE(b1.ENDDATE)>=DATE('"+monthLong[1][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[1][1]+"') "
            + "   ) D "
            + " from ldgrp a, lcgrpcont b, lcpol c "
            + " where a.customerno = b.appntno and b.managecom like '"+ManageCom+"%' and b.appflag='1' "
            + "   and c.grpcontno=b.grpcontno and c.riskcode='"+RiskCode+"' "
            + " order by a.customerno "
            + " ) as x "
            ;


        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult2Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult2Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}


//3月赔付率
MonMult3Table.setName("MONMULT3");
try {
    if(!monthLong[2][0].equals("-")||!monthLong[2][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql =  "select B , M ,"
            + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
            + " from "
            + " (select distinct a.customerno A, a.grpname B, "
            + "  (select name from ldcom a where a.comcode=b.managecom)  M, "
            + "   (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
            + "   where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
            + "    and c1.appntno=b.appntno and b1.riskcode='"+RiskCode+"' "
            + "    and a1.rgtdate between '"+ monthLong[2][0]+"' and '"+monthLong[2][1]+"' ) C, "
            + "   (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[2][1]+"')-to_date('"+monthLong[2][0]+"'))/365 "
            + "    from lcgrpcont a1 , lcpol b1 "
            + "    where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
            + "     and a1.appntno=a.customerno and DATE(b1.ENDDATE)>=DATE('"+monthLong[2][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[2][1]+"') "
            + "   ) D "
            + " from ldgrp a, lcgrpcont b, lcpol c "
            + " where a.customerno = b.appntno and b.managecom like '"+ManageCom+"%' and b.appflag='1' "
            + "   and c.grpcontno=b.grpcontno and c.riskcode='"+RiskCode+"' "
            + " order by a.customerno "
            + " ) as x "
            ;


        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult3Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult3Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}


//4月赔付率
MonMult4Table.setName("MONMULT4");
try {
    if(!monthLong[3][0].equals("-")||!monthLong[3][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql =  "select B , M ,"
            + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
            + " from "
            + " (select distinct a.customerno A, a.grpname B, "
            + "  (select name from ldcom a where a.comcode=b.managecom)  M, "
            + "   (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
            + "   where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
            + "    and c1.appntno=b.appntno and b1.riskcode='"+RiskCode+"' "
            + "    and a1.rgtdate between '"+ monthLong[3][0]+"' and '"+monthLong[3][1]+"' ) C, "
            + "   (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[3][1]+"')-to_date('"+monthLong[3][0]+"'))/365 "
            + "    from lcgrpcont a1 , lcpol b1 "
            + "    where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
            + "     and a1.appntno=a.customerno and DATE(b1.ENDDATE)>=DATE('"+monthLong[3][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[3][1]+"') "
            + "   ) D "
            + " from ldgrp a, lcgrpcont b, lcpol c "
            + " where a.customerno = b.appntno and b.managecom like '"+ManageCom+"%' and b.appflag='1' "
            + "   and c.grpcontno=b.grpcontno and c.riskcode='"+RiskCode+"' "
            + " order by a.customerno "
            + " ) as x "
            ;


        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult4Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult4Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}

//5月赔付率
MonMult5Table.setName("MONMULT5");
try {
    if(!monthLong[4][0].equals("-")||!monthLong[4][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql =  "select B , M ,"
            + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
            + " from "
            + " (select distinct a.customerno A, a.grpname B, "
            + "  (select name from ldcom a where a.comcode=b.managecom)  M, "
            + "   (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
            + "   where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
            + "    and c1.appntno=b.appntno and b1.riskcode='"+RiskCode+"' "
            + "    and a1.rgtdate between '"+ monthLong[4][0]+"' and '"+monthLong[4][1]+"' ) C, "
            + "   (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[4][1]+"')-to_date('"+monthLong[4][0]+"'))/365 "
            + "    from lcgrpcont a1 , lcpol b1 "
            + "    where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
            + "     and a1.appntno=a.customerno and DATE(b1.ENDDATE)>=DATE('"+monthLong[4][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[4][1]+"') "
            + "   ) D "
            + " from ldgrp a, lcgrpcont b, lcpol c "
            + " where a.customerno = b.appntno and b.managecom like '"+ManageCom+"%' and b.appflag='1' "
            + "   and c.grpcontno=b.grpcontno and c.riskcode='"+RiskCode+"' "
            + " order by a.customerno "
            + " ) as x "
            ;


        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult5Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult5Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}

//6月赔付率
MonMult6Table.setName("MONMULT6");
try {
    if(!monthLong[5][0].equals("-")||!monthLong[5][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql =  "select B , M ,"
            + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
            + " from "
            + " (select distinct a.customerno A, a.grpname B, "
            + "  (select name from ldcom a where a.comcode=b.managecom)  M, "
            + "   (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
            + "   where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
            + "    and c1.appntno=b.appntno and b1.riskcode='"+RiskCode+"' "
            + "    and a1.rgtdate between '"+ monthLong[5][0]+"' and '"+monthLong[5][1]+"' ) C, "
            + "   (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[5][1]+"')-to_date('"+monthLong[5][0]+"'))/365 "
            + "    from lcgrpcont a1 , lcpol b1 "
            + "    where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
            + "     and a1.appntno=a.customerno and DATE(b1.ENDDATE)>=DATE('"+monthLong[5][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[5][1]+"') "
            + "   ) D "
            + " from ldgrp a, lcgrpcont b, lcpol c "
            + " where a.customerno = b.appntno and b.managecom like '"+ManageCom+"%' and b.appflag='1' "
            + "   and c.grpcontno=b.grpcontno and c.riskcode='"+RiskCode+"' "
            + " order by a.customerno "
            + " ) as x "
            ;


        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult6Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult6Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}

//7月赔付率
MonMult7Table.setName("MONMULT7");
try {
     if(!monthLong[6][0].equals("-")||!monthLong[6][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql =  "select B , M ,"
            + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
            + " from "
            + " (select distinct a.customerno A, a.grpname B, "
            + "  (select name from ldcom a where a.comcode=b.managecom)  M, "
            + "   (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
            + "   where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
            + "    and c1.appntno=b.appntno and b1.riskcode='"+RiskCode+"' "
            + "    and a1.rgtdate between '"+ monthLong[6][0]+"' and '"+monthLong[6][1]+"' ) C, "
            + "   (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[6][1]+"')-to_date('"+monthLong[6][0]+"'))/365 "
            + "    from lcgrpcont a1 , lcpol b1 "
            + "    where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
            + "     and a1.appntno=a.customerno and DATE(b1.ENDDATE)>=DATE('"+monthLong[6][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[6][1]+"') "
            + "   ) D "
            + " from ldgrp a, lcgrpcont b, lcpol c "
            + " where a.customerno = b.appntno and b.managecom like '"+ManageCom+"%' and b.appflag='1' "
            + "   and c.grpcontno=b.grpcontno and c.riskcode='"+RiskCode+"' "
            + " order by a.customerno "
            + " ) as x "
            ;


            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            int count = tSSRS.getMaxRow();
            System.out.println(count);
            for (int i = 0; i < count; i++) {
              String temp[][] = tSSRS.getAllData();

            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult7Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult7Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}

//8月赔付率
MonMult8Table.setName("MONMULT8");
try {
    if(!monthLong[7][0].equals("-")||!monthLong[7][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql =  "select B , M ,"
            + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
            + " from "
            + " (select distinct a.customerno A, a.grpname B, "
            + "  (select name from ldcom a where a.comcode=b.managecom)  M, "
            + "   (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
            + "   where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
            + "    and c1.appntno=b.appntno and b1.riskcode='"+RiskCode+"' "
            + "    and a1.rgtdate between '"+ monthLong[7][0]+"' and '"+monthLong[7][1]+"' ) C, "
            + "   (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[7][1]+"')-to_date('"+monthLong[7][0]+"'))/365 "
            + "    from lcgrpcont a1 , lcpol b1 "
            + "    where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
            + "     and a1.appntno=a.customerno and DATE(b1.ENDDATE)>=DATE('"+monthLong[7][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[7][1]+"') "
            + "   ) D "
            + " from ldgrp a, lcgrpcont b, lcpol c "
            + " where a.customerno = b.appntno and b.managecom like '"+ManageCom+"%' and b.appflag='1' "
            + "   and c.grpcontno=b.grpcontno and c.riskcode='"+RiskCode+"' "
            + " order by a.customerno "
            + " ) as x "
            ;


        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult8Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult8Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}

//9月赔付率
MonMult9Table.setName("MONMULT9");
try {
    if(!monthLong[8][0].equals("-")||!monthLong[8][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql =  "select B , M ,"
            + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
            + " from "
            + " (select distinct a.customerno A, a.grpname B, "
            + "  (select name from ldcom a where a.comcode=b.managecom)  M, "
            + "   (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
            + "   where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
            + "    and c1.appntno=b.appntno and b1.riskcode='"+RiskCode+"' "
            + "    and a1.rgtdate between '"+ monthLong[8][0]+"' and '"+monthLong[8][1]+"' ) C, "
            + "   (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[8][1]+"')-to_date('"+monthLong[8][0]+"'))/365 "
            + "    from lcgrpcont a1 , lcpol b1 "
            + "    where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
            + "     and a1.appntno=a.customerno and DATE(b1.ENDDATE)>=DATE('"+monthLong[8][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[8][1]+"') "
            + "   ) D "
            + " from ldgrp a, lcgrpcont b, lcpol c "
            + " where a.customerno = b.appntno and b.managecom like '"+ManageCom+"%' and b.appflag='1' "
            + "   and c.grpcontno=b.grpcontno and c.riskcode='"+RiskCode+"' "
            + " order by a.customerno "
            + " ) as x "
            ;


        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult9Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult9Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}

//10月赔付率
MonMult10Table.setName("MONMULT10");
try {
    if(!monthLong[9][0].equals("-")||!monthLong[9][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql =  "select B , M ,"
            + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
            + " from "
            + " (select distinct a.customerno A, a.grpname B, "
            + "  (select name from ldcom a where a.comcode=b.managecom)  M, "
            + "   (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
            + "   where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
            + "    and c1.appntno=b.appntno and b1.riskcode='"+RiskCode+"' "
            + "    and a1.rgtdate between '"+ monthLong[9][0]+"' and '"+monthLong[9][1]+"' ) C, "
            + "   (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[9][1]+"')-to_date('"+monthLong[9][0]+"'))/365 "
            + "    from lcgrpcont a1 , lcpol b1 "
            + "    where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
            + "     and a1.appntno=a.customerno and DATE(b1.ENDDATE)>=DATE('"+monthLong[9][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[9][1]+"') "
            + "   ) D "
            + " from ldgrp a, lcgrpcont b, lcpol c "
            + " where a.customerno = b.appntno and b.managecom like '"+ManageCom+"%' and b.appflag='1' "
            + "   and c.grpcontno=b.grpcontno and c.riskcode='"+RiskCode+"' "
            + " order by a.customerno "
            + " ) as x "
            ;



        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult10Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult10Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}

//11月赔付率
MonMult11Table.setName("MONMULT11");
try {
    if(!monthLong[10][0].equals("-")||!monthLong[10][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql =  "select B , M ,"
            + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
            + " from "
            + " (select distinct a.customerno A, a.grpname B, "
            + "  (select name from ldcom a where a.comcode=b.managecom)  M, "
            + "   (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
            + "   where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
            + "    and c1.appntno=b.appntno and b1.riskcode='"+RiskCode+"' "
            + "    and a1.rgtdate between '"+ monthLong[10][0]+"' and '"+monthLong[10][1]+"' ) C, "
            + "   (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[10][1]+"')-to_date('"+monthLong[10][0]+"'))/365 "
            + "    from lcgrpcont a1 , lcpol b1 "
            + "    where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
            + "     and a1.appntno=a.customerno and DATE(b1.ENDDATE)>=DATE('"+monthLong[10][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[10][1]+"') "
            + "   ) D "
            + " from ldgrp a, lcgrpcont b, lcpol c "
            + " where a.customerno = b.appntno and b.managecom like '"+ManageCom+"%' and b.appflag='1' "
            + "   and c.grpcontno=b.grpcontno and c.riskcode='"+RiskCode+"' "
            + " order by a.customerno "
            + " ) as x "
            ;


        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult11Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult11Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}

//12月赔付率
MonMult12Table.setName("MONMULT12");
try {
    if(!monthLong[11][0].equals("-")||!monthLong[11][1].equals("-"))
    {
        SSRS tSSRS = new SSRS();
        String sql =  "select B , M ,"
             + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
             + " from "
             + " (select distinct a.customerno A, a.grpname B, "
             + "  (select name from ldcom a where a.comcode=b.managecom)  M, "
             + "   (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
             + "   where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
             + "    and c1.appntno=b.appntno and b1.riskcode='"+RiskCode+"' "
             + "    and a1.rgtdate between '"+ monthLong[11][0]+"' and '"+monthLong[11][1]+"' ) C, "
             + "   (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[11][1]+"')-to_date('"+monthLong[11][0]+"'))/365 "
             + "    from lcgrpcont a1 , lcpol b1 "
             + "    where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
             + "     and a1.appntno=a.customerno and DATE(b1.ENDDATE)>=DATE('"+monthLong[11][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[11][1]+"') "
             + "   ) D "
             + " from ldgrp a, lcgrpcont b, lcpol c "
             + " where a.customerno = b.appntno and b.managecom like '"+ManageCom+"%' and b.appflag='1' "
             + "   and c.grpcontno=b.grpcontno and c.riskcode='"+RiskCode+"' "
             + " order by a.customerno "
             + " ) as x "
             ;


        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        System.out.println(count);

        for (int i = 0; i < count; i++) {
            String temp[][] = tSSRS.getAllData();
            String[] strCol;
            strCol = new String[1];
            strCol[0] = temp[i][2];

            MonMult12Table.add(strCol);
        }
    }
    else
    {
        for(int j=0;j<rowNum;j++)
        {
            String[] strCol;
            strCol = new String[1];
            strCol[0] = "-";
            MonMult12Table.add(strCol);
        }
    }
} catch (Exception e) {
    CError tError = new CError();
    tError.moduleName = "LLRiskPayRateBL";
    tError.functionName = "";
    tError.errorMessage = "客户基本信息查询失败";
    this.mErrors.addOneError(tError);
}


/**************************************************************************************************************/
 /*      //月度给付
        try {
            String[][] strCont;
            strCont = new String[12][rowNum];


            for(int k=0;k<12;k++)
            {
                System.out.println("come to k loop..............."+k);
                if(!monthLong[k][0].equals("-"))
                {
                    System.out.println(monthLong[k][0]+" 日期不为空。");
                    SSRS tSSRS = new SSRS();
                    String sql = " select A,B,"
                          + " CASE WHEN (D=0 OR D IS NULL OR C IS NULL) THEN 0 ELSE DECIMAL(DECIMAL(C)/DECIMAL(D)*100,12,2) END FF "
                          + " from ( "
                          + " select a.comcode A, a.name B,"
                          + " (select sum(a1.realpay) from llclaim a1, llcase b1 "
                          + " where a1.caseno=b1.caseno and SUBSTR(A1.MNGCOM,1,LENGTH(TRIM(A.COMCODE)))=A.COMCODE "
                          + " and b1.rgtdate between '"+ monthLong[k][0] +"' and '"+ monthLong[k][1] +"' "
                          + " ) C,"
                          + " (select (1-0.25)*sum(prem)*decimal(date('"+ monthLong[k][1] +"')-date('"+ monthLong[k][0] +"'))/365 "
                          + " from lcpol a2 where a2.appflag='1' and substr(a2.managecom,1,length(trim(a.comcode)))=a.comcode "
                          + " and DATE(A2.ENDDATE)>=DATE('"+ monthLong[k][0] +"') AND DATE(A2.CVALIDATE)<=DATE('"+ monthLong[k][1] +"') "
                          + " and a2.riskcode='"+ RiskCode +"' "
                          + "  ) D "
                          + " from ldcom a "
                          + " where substr(a.comcode,1,length('"+ ManageCom +"'))='"+ ManageCom +"' and LENGTH(TRIM(A.COMCODE))=8 "
                          + "  and sign='1' "
                          + " ) as x order by A";
                    ExeSQL tExeSQL = new ExeSQL();
                    tSSRS = tExeSQL.execSQL(sql);
                    int count = tSSRS.getMaxRow();
                    //String temp[][] = tSSRS.getAllData();

                    for (int i = 0; i < count; i++)
                    {
                        System.out.println("come to i loop..............."+ i);
                        String temp[][] = tSSRS.getAllData();
                        strCont[k][i] = temp[i][2];
                        System.out.println("*****  "+temp[i][0]+"*****  "+temp[i][2]);
                    }
                }
                else
                {
                    for(int j=0;j<rowNum;j++)
                    {
                        System.out.println("come to loop 空.........."+j);
                        strCont[k][j]="-";
                    }
                }
            }


            MonMult1Table.add(strCont[0]);
            MonMult2Table.add(strCont[1]);
            MonMult3Table.add(strCont[2]);
            MonMult4Table.add(strCont[3]);
            MonMult5Table.add(strCont[4]);
            MonMult6Table.add(strCont[5]);
            MonMult7Table.add(strCont[6]);
            MonMult8Table.add(strCont[7]);
            MonMult9Table.add(strCont[8]);
            MonMult10Table.add(strCont[9]);
            MonMult11Table.add(strCont[10]);
            MonMult12Table.add(strCont[11]);

            for(int i=0;i<strCont.length;i++)
            {
                for(int j=0;j<strCont[i].length;j++)
                {
                    System.out.print("strCont["+i+"]["+j+"] = "+strCont[i][j]+"  ");
                }
                System.out.println();
            }
        }
        catch (Exception e)
        {
            CError tError = new CError();
            tError.moduleName = "LLRiskPayRateBL";
            tError.functionName = "";
            tError.errorMessage = "客户基本信息查询失败";
            this.mErrors.addOneError(tError);
        }
*/

 //总计险种档次信息
 ListTable SumMultTable = new ListTable();
 SumMultTable.setName("SUMMULT");
 String[] Title_s = {"col1", "col2", "col3"};//, "col4","col5", "col6","col7","col8", "col9", "col10", "col11","col12", "col13","col14"};

 try {
     SSRS tSSRS = new SSRS();
     String sql = " select distinct '总计', '-',"
                  + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
                  + " from (select "
                  + " (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
                  + " where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
                  + " and b1.riskcode='"+RiskCode+"' and a1.rgtdate between '"+StartDate+"' and '"+EndDate+"' "
                  + " ) C, "
                  + " (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+EndDate+"')-to_date('"+StartDate+"'))/365 "
                  + " from lcgrpcont a1 , lcpol b1 "
                  + " where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
                  + " and DATE(b1.ENDDATE)>=DATE('"+StartDate+"') AND DATE(b1.CVALIDATE)<=DATE('"+EndDate+"') "
                  + " ) D from dual ) as x "
                  ;

     ExeSQL tExeSQL = new ExeSQL();
     tSSRS = tExeSQL.execSQL(sql);
     int count = tSSRS.getMaxRow();

     for (int i = 0; i < count; i++) {
         String temp[][] = tSSRS.getAllData();
         System.out.println("Execute testSql ..............."+i);
         String[] strCol;
         strCol = new String[3];
         strCol[0] = temp[i][0];
         strCol[1] = temp[i][1];
         strCol[2] = temp[i][2];
         SumMultTable.add(strCol);
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }


 ListTable SumMonMult1Table = new ListTable();
 SumMonMult1Table.setName("SUMMONMULT1");
 ListTable SumMonMult2Table = new ListTable();
 SumMonMult2Table.setName("SUMMONMULT2");
 ListTable SumMonMult3Table = new ListTable();
 SumMonMult3Table.setName("SUMMONMULT3");
 ListTable SumMonMult4Table = new ListTable();
 SumMonMult4Table.setName("SUMMONMULT4");
 ListTable SumMonMult5Table = new ListTable();
 SumMonMult5Table.setName("SUMMONMULT5");
 ListTable SumMonMult6Table = new ListTable();
 SumMonMult6Table.setName("SUMMONMULT6");
 ListTable SumMonMult7Table = new ListTable();
 SumMonMult7Table.setName("SUMMONMULT7");
 ListTable SumMonMult8Table = new ListTable();
 SumMonMult8Table.setName("SUMMONMULT8");
 ListTable SumMonMult9Table = new ListTable();
 SumMonMult9Table.setName("SUMMONMULT9");
 ListTable SumMonMult10Table = new ListTable();
 SumMonMult10Table.setName("SUMMONMULT10");
 ListTable SumMonMult11Table = new ListTable();
 SumMonMult11Table.setName("SUMMONMULT11");
 ListTable SumMonMult12Table = new ListTable();
 SumMonMult12Table.setName("SUMMONMULT12");

 String[] Title_s1 = {"a1"};
 String[] Title_s2 = {"a1"};
 String[] Title_s3 = {"a1"};
 String[] Title_s4 = {"a1"};
 String[] Title_s5 = {"a1"};
 String[] Title_s6 = {"a1"};
 String[] Title_s7 = {"a1"};
 String[] Title_s8 = {"a1"};
 String[] Title_s9 = {"a1"};
 String[] Title_s10 = {"a1"};
 String[] Title_s11 = {"a1"};
 String[] Title_s12 = {"a1"};

 //月度合计
 //1月赔付率合计
 try {
      if(!monthLong[0][0].equals("-")||!monthLong[0][1].equals("-"))
      {
          SSRS tSSRS = new SSRS();
          String sql = " select distinct '总计', '-',"
                  + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
                  + " from (select "
                  + " (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
                  + " where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
                  + " and b1.riskcode='"+RiskCode+"' and a1.rgtdate between '"+monthLong[0][0]+"' and '"+monthLong[0][1]+"' "
                  + " ) C, "
                  + " (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[0][1]+"')-to_date('"+monthLong[0][0]+"'))/365 "
                  + " from lcgrpcont a1 , lcpol b1 "
                  + " where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
                  + " and DATE(b1.ENDDATE)>=DATE('"+monthLong[0][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[0][1]+"') "
                  + " ) D from dual ) as x "
                  ;

             ExeSQL tExeSQL = new ExeSQL();
             tSSRS = tExeSQL.execSQL(sql);
             int count = tSSRS.getMaxRow();
             System.out.println(count);
             for (int i = 0; i < count; i++) {
               String temp[][] = tSSRS.getAllData();

             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult1Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult1Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }


 //2月赔付率合计
 try {
     if(!monthLong[1][0].equals("-")||!monthLong[1][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '总计', '-',"
                  + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
                  + " from (select "
                  + " (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
                  + " where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
                  + " and b1.riskcode='"+RiskCode+"' and a1.rgtdate between '"+monthLong[1][0]+"' and '"+monthLong[1][1]+"' "
                  + " ) C, "
                  + " (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[1][1]+"')-to_date('"+monthLong[1][0]+"'))/365 "
                  + " from lcgrpcont a1 , lcpol b1 "
                  + " where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
                  + " and DATE(b1.ENDDATE)>=DATE('"+monthLong[1][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[1][1]+"') "
                  + " ) D from dual ) as x "
                  ;



         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult2Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult2Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }

 //3月赔付率合计
 try {
     if(!monthLong[2][0].equals("-")||!monthLong[2][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '总计', '-',"
                  + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
                  + " from (select "
                  + " (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
                  + " where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
                  + " and b1.riskcode='"+RiskCode+"' and a1.rgtdate between '"+monthLong[2][0]+"' and '"+monthLong[2][1]+"' "
                  + " ) C, "
                  + " (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[2][1]+"')-to_date('"+monthLong[2][0]+"'))/365 "
                  + " from lcgrpcont a1 , lcpol b1 "
                  + " where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
                  + " and DATE(b1.ENDDATE)>=DATE('"+monthLong[2][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[2][1]+"') "
                  + " ) D from dual ) as x "
                  ;

         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult3Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult3Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }

 //4月赔付率合计
 try {
     if(!monthLong[3][0].equals("-")||!monthLong[3][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '总计', '-',"
                  + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
                  + " from (select "
                  + " (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
                  + " where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
                  + " and b1.riskcode='"+RiskCode+"' and a1.rgtdate between '"+monthLong[3][0]+"' and '"+monthLong[3][1]+"' "
                  + " ) C, "
                  + " (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[3][1]+"')-to_date('"+monthLong[3][0]+"'))/365 "
                  + " from lcgrpcont a1 , lcpol b1 "
                  + " where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
                  + " and DATE(b1.ENDDATE)>=DATE('"+monthLong[3][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[3][1]+"') "
                  + " ) D from dual ) as x "
                  ;



         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult4Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult4Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }

 //5月赔付率合计
 try {
     if(!monthLong[4][0].equals("-")||!monthLong[4][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '总计', '-',"
                   + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
                   + " from (select "
                   + " (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
                   + " where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
                   + " and b1.riskcode='"+RiskCode+"' and a1.rgtdate between '"+monthLong[4][0]+"' and '"+monthLong[4][1]+"' "
                   + " ) C, "
                   + " (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[4][1]+"')-to_date('"+monthLong[4][0]+"'))/365 "
                   + " from lcgrpcont a1 , lcpol b1 "
                   + " where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
                   + " and DATE(b1.ENDDATE)>=DATE('"+monthLong[4][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[4][1]+"') "
                   + " ) D from dual ) as x "
                   ;



         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult5Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult5Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }

 //6月赔付率合计
 try {
     if(!monthLong[5][0].equals("-")||!monthLong[5][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '总计', '-',"
                  + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
                  + " from (select "
                  + " (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
                  + " where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
                  + " and b1.riskcode='"+RiskCode+"' and a1.rgtdate between '"+monthLong[5][0]+"' and '"+monthLong[5][1]+"' "
                  + " ) C, "
                  + " (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[5][1]+"')-to_date('"+monthLong[5][0]+"'))/365 "
                  + " from lcgrpcont a1 , lcpol b1 "
                  + " where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
                  + " and DATE(b1.ENDDATE)>=DATE('"+monthLong[5][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[5][1]+"') "
                  + " ) D from dual ) as x "
                  ;



         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult6Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult6Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }



 //7月赔付率合计
 try {
      if(!monthLong[6][0].equals("-")||!monthLong[6][1].equals("-"))
      {
          SSRS tSSRS = new SSRS();
          String sql = " select distinct '总计', '-',"
                  + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
                  + " from (select "
                  + " (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
                  + " where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
                  + " and b1.riskcode='"+RiskCode+"' and a1.rgtdate between '"+monthLong[6][0]+"' and '"+monthLong[6][1]+"' "
                  + " ) C, "
                  + " (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[6][1]+"')-to_date('"+monthLong[6][0]+"'))/365 "
                  + " from lcgrpcont a1 , lcpol b1 "
                  + " where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
                  + " and DATE(b1.ENDDATE)>=DATE('"+monthLong[6][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[6][1]+"') "
                  + " ) D from dual ) as x "
                  ;



             ExeSQL tExeSQL = new ExeSQL();
             tSSRS = tExeSQL.execSQL(sql);
             int count = tSSRS.getMaxRow();
             System.out.println(count);
             for (int i = 0; i < count; i++) {
               String temp[][] = tSSRS.getAllData();

             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult7Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult7Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }


 //8月赔付率合计
 try {
     if(!monthLong[7][0].equals("-")||!monthLong[7][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '总计', '-',"
                  + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
                  + " from (select "
                  + " (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
                  + " where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
                  + " and b1.riskcode='"+RiskCode+"' and a1.rgtdate between '"+monthLong[7][0]+"' and '"+monthLong[7][1]+"' "
                  + " ) C, "
                  + " (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[7][1]+"')-to_date('"+monthLong[7][0]+"'))/365 "
                  + " from lcgrpcont a1 , lcpol b1 "
                  + " where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
                  + " and DATE(b1.ENDDATE)>=DATE('"+monthLong[7][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[7][1]+"') "
                  + " ) D from dual ) as x "
                  ;




         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult8Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult8Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }

 //9月赔付率合计
 try {
     if(!monthLong[8][0].equals("-")||!monthLong[8][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '总计', '-',"
                  + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
                  + " from (select "
                  + " (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
                  + " where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
                  + " and b1.riskcode='"+RiskCode+"' and a1.rgtdate between '"+monthLong[8][0]+"' and '"+monthLong[8][1]+"' "
                  + " ) C, "
                  + " (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[8][1]+"')-to_date('"+monthLong[8][0]+"'))/365 "
                  + " from lcgrpcont a1 , lcpol b1 "
                  + " where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
                  + " and DATE(b1.ENDDATE)>=DATE('"+monthLong[8][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[8][1]+"') "
                  + " ) D from dual ) as x "
                  ;



         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult9Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult9Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }

  //10月赔付率合计
 try {
     if(!monthLong[9][0].equals("-")||!monthLong[9][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '总计', '-',"
                  + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
                  + " from (select "
                  + " (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
                  + " where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
                  + " and b1.riskcode='"+RiskCode+"' and a1.rgtdate between '"+monthLong[9][0]+"' and '"+monthLong[9][1]+"' "
                  + " ) C, "
                  + " (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[9][1]+"')-to_date('"+monthLong[9][0]+"'))/365 "
                  + " from lcgrpcont a1 , lcpol b1 "
                  + " where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
                  + " and DATE(b1.ENDDATE)>=DATE('"+monthLong[9][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[9][1]+"') "
                  + " ) D from dual ) as x "
                  ;



         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult10Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult10Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }

  //11月赔付率合计
 try {
     if(!monthLong[10][0].equals("-")||!monthLong[10][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '总计', '-',"
                  + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
                  + " from (select "
                  + " (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
                  + " where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
                  + " and b1.riskcode='"+RiskCode+"' and a1.rgtdate between '"+monthLong[10][0]+"' and '"+monthLong[10][1]+"' "
                  + " ) C, "
                  + " (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[10][1]+"')-to_date('"+monthLong[10][0]+"'))/365 "
                  + " from lcgrpcont a1 , lcpol b1 "
                  + " where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
                  + " and DATE(b1.ENDDATE)>=DATE('"+monthLong[10][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[10][1]+"') "
                  + " ) D from dual ) as x "
                  ;



         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];

             SumMonMult11Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult11Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }

 //12月赔付率合计
 try {
     if(!monthLong[11][0].equals("-")||!monthLong[11][1].equals("-"))
     {
         SSRS tSSRS = new SSRS();
         String sql = " select distinct '总计', '-',"
                  + " case when (C is null or D is null or D=0) then 0 else decimal(decimal(C)/decimal(D)*100,12,2) end FF "
                  + " from (select "
                  + " (select sum(b1.realpay) from llcase a1, llclaimpolicy b1, lcgrpcont c1 "
                  + " where a1.caseno=b1.caseno and c1.grpcontno=b1.grpcontno and a1.endcasedate is not null "
                  + " and b1.riskcode='"+RiskCode+"' and a1.rgtdate between '"+monthLong[11][0]+"' and '"+monthLong[11][1]+"' "
                  + " ) C, "
                  + " (select (1-0.25)*sum(b1.prem)*decimal(to_date('"+monthLong[11][1]+"')-to_date('"+monthLong[11][0]+"'))/365 "
                  + " from lcgrpcont a1 , lcpol b1 "
                  + " where a1.grpcontno=b1.grpcontno and b1.riskcode='"+RiskCode+"' "
                  + " and DATE(b1.ENDDATE)>=DATE('"+monthLong[11][0]+"') AND DATE(b1.CVALIDATE)<=DATE('"+monthLong[11][1]+"') "
                  + " ) D from dual ) as x "
                  ;

         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sql);
         int count = tSSRS.getMaxRow();
         System.out.println(count);

         for (int i = 0; i < count; i++) {
             String temp[][] = tSSRS.getAllData();
             String[] strCol;
             strCol = new String[1];
             strCol[0] = temp[i][2];
             System.out.println("monthLong[11][0] "+monthLong[11][0]+"  *************** strCol[0]  " + strCol[0]);
             SumMonMult12Table.add(strCol);
         }
     }
     else
     {
         for(int j=0;j<1;j++)
         {
             String[] strCol;
             strCol = new String[1];
             strCol[0] = "-";
             SumMonMult12Table.add(strCol);
         }
     }
 } catch (Exception e) {
     CError tError = new CError();
     tError.moduleName = "LLRiskPayRateBL";
     tError.functionName = "";
     tError.errorMessage = "客户基本信息查询失败";
     this.mErrors.addOneError(tError);
 }

 /*******************/



        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LLCustomerPayRate.vts", "printer"); //最好紧接着就初始化xml文档

        //LLRiskPayRateBL模版元素

        StartDate = StartDate.replace('-', '月');
        StartDate = StartDate.replaceFirst("月", "年") + "日";
        EndDate = EndDate.replace('-', '月');
        EndDate = EndDate.replaceFirst("月", "年") + "日";
        texttag.add("StartDateN", StartDate);
        texttag.add("EndDateN", EndDate);
        texttag.add("CurrentDateN", CurrentDateN);
        texttag.add("HandlerN", HandlerN);
        texttag.add("RiskNameN", RiskNameN);

        SSRS tSSRS_M = new SSRS();
        String sql_M = "select Name from ldcom where comcode = '" + ManageCom + "'";
        System.out.println(sql_M);
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS_M = tExeSQL.execSQL(sql_M);
        String temp_M[][] = tSSRS_M.getAllData();
        texttag.add("ManageComName", temp_M[0][0]);

        if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
        }

        //保存信息

        xmlexport.addListTable(MultTable, Title);

        xmlexport.addListTable(MonMult1Table, Title_1);
        xmlexport.addListTable(MonMult2Table, Title_2);
        xmlexport.addListTable(MonMult3Table, Title_3);
        xmlexport.addListTable(MonMult4Table, Title_4);
        xmlexport.addListTable(MonMult5Table, Title_5);
        xmlexport.addListTable(MonMult6Table, Title_6);
        xmlexport.addListTable(MonMult7Table, Title_7);
        xmlexport.addListTable(MonMult8Table, Title_8);
        xmlexport.addListTable(MonMult9Table, Title_9);
        xmlexport.addListTable(MonMult10Table, Title_10);
        xmlexport.addListTable(MonMult11Table, Title_11);
        xmlexport.addListTable(MonMult12Table, Title_12);

        xmlexport.addListTable(SumMultTable, Title_s);

        xmlexport.addListTable(SumMonMult1Table, Title_s1);
        xmlexport.addListTable(SumMonMult2Table, Title_s2);
        xmlexport.addListTable(SumMonMult3Table, Title_s3);
        xmlexport.addListTable(SumMonMult4Table, Title_s4);
        xmlexport.addListTable(SumMonMult5Table, Title_s5);
        xmlexport.addListTable(SumMonMult6Table, Title_s6);
        xmlexport.addListTable(SumMonMult7Table, Title_s7);
        xmlexport.addListTable(SumMonMult8Table, Title_s8);
        xmlexport.addListTable(SumMonMult9Table, Title_s9);
        xmlexport.addListTable(SumMonMult10Table, Title_s10);
        xmlexport.addListTable(SumMonMult11Table, Title_s11);
        xmlexport.addListTable(SumMonMult12Table, Title_s12);

        /*xmlexport.addListTable(RefuseTable, Title_3);
        xmlexport.addListTable(XYTable, Title_4);
        xmlexport.addListTable(TRTable, Title_5);
        xmlexport.outputDocumentToFile("e:\\", "testHZM"); //输出xml文档到文件
        */
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    /**
     * 输入起始日期终止日期
     * 输出：起期与止期之间月经过日期
     */

    private String[][] getMonthLong(String startDate,String endDate)
    {
        System.out.println(startDate+"   "+endDate);
        String[][] monthLong = new String[12][2];
        if(startDate.length()!=0||endDate.length()!=0)
        {
            String StartYear = startDate.substring(0, 4);
            String EndYear = endDate.substring(0, 4);
            String StartMonth = startDate.substring(5, 7);
            String EndMonth = endDate.substring(5, 7);
            String StartDay = startDate.substring(8, 10);
            String EndDay = endDate.substring(8, 10);

            int iStartYear = Integer.parseInt(StartYear);
            int iEndYear = Integer.parseInt(EndYear);
            int iStartMonth = Integer.parseInt(StartMonth);
            int iEndMonth = Integer.parseInt(EndMonth);
            int iStartDay = Integer.parseInt(StartDay);
            int iEndDay = Integer.parseInt(EndDay);

            int marYear = iEndYear - iStartYear;
            int marMonth = iEndMonth - iStartMonth;

            int monNum = 0;
            if ((marYear) != 0) {
                monNum = marYear * 12 + marMonth + 1;
            } else {
                monNum = marMonth + 1;
            }
            System.out.println("月度数：" + monNum);

            String[] MonthNo = new String[12];

            if (monNum == 1) {
                monthLong[0][0] = iStartYear + "-" + iStartMonth + "-" + iStartDay;
                monthLong[0][1] = iEndYear + "-" + iEndMonth + "-" + iEndDay;
                for (int i = 2; i < 13; i++) {
                    monthLong[i - 1][0] = "-";
                    monthLong[i - 1][1] = "-";
                }
            } else if (monNum == 2) {
                monthLong[0][0]=iStartYear+"-"+iStartMonth+"-"+iStartDay;
                monthLong[0][1] = iStartYear+"-"+iStartMonth+"-"+getDays(iStartMonth,iStartYear);
                monthLong[1][0] = iEndYear+"-"+iEndMonth+"-"+1;
                monthLong[1][1] = iEndYear+"-"+iEndMonth+"-"+iEndDay;

                for (int i = 3; i < 13; i++) {
                    monthLong[i-1][0]="-";
                    monthLong[i-1][1]="-";
                }
            } else if (monNum > 2) {
                int staMonth = iStartMonth;
                monthLong[0][0]=iStartYear+"-"+iStartMonth+"-"+iStartDay;
                monthLong[0][1]=iStartYear+"-"+iStartMonth+"-"+getDays(iStartMonth,iStartYear);
                int i;
                for (i = 2; i < monNum; i++) {
                    staMonth = staMonth + 1;

                    if (staMonth > 12) {
                        monthLong[i-1][0]=iEndYear+"-"+staMonth % 12 +"-"+1;
                        monthLong[i-1][1]=iEndYear+"-"+staMonth%12+"-"+getDays(staMonth%12,iEndYear);
                    } else {
                        monthLong[i-1][0]=iStartYear+"-"+staMonth+"-"+1;
                        monthLong[i-1][1]=iStartYear+"-"+staMonth+"-"+getDays(staMonth,iStartYear);;
                    }
                }
                monthLong[monNum-1][0]=iEndYear+"-"+iEndMonth+"-"+1;
                monthLong[monNum-1][1]=iEndYear + "-"+iEndMonth+"-"+iEndDay;

                for (int j=monNum;j< 12; j++) {
                    monthLong[j][0] = "-";
                    monthLong[j][1] = "-";
                }
            }
            return monthLong;
        }
        else
        {
            return null;
        }
    }

    private int getDays(int month,int year)
    {
        int days=0;
        int[] daysInMonth={0,31, 28, 31, 30, 31, 30, 31, 31,30, 31, 30, 31};
        if (2 == month)
        {
            return ((0==year%4)&&(0!=(year % 100)))||(0 == year%400)?29:28;
        }
        return daysInMonth[month];
    }



    public static void main(String[] args) {


//PubSubmit ps = new PubSubmit();
//if (ps.submitData(vdata, ""))
//{
//    System.out.println("succeed in pubsubmit");
//}

    }

    private void jbInit() throws Exception {
    }

}
