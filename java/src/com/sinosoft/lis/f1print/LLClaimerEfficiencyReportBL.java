package com.sinosoft.lis.f1print;

/**
 * <p>Title: LLClaimerEfficiencyReportBL</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author MN
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

public class LLClaimerEfficiencyReportBL {
    public LLClaimerEfficiencyReportBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mStartDate = "";

    private String mEndDate = "";

    private String mManageCom = "";

    private String mManageComName = "";

    private String moperator = "";

    private String[][] mShowDataList = null;

    private XmlExport mXmlExport = null;

    private String[] mDataList = null;

    private ListTable mListTable = new ListTable();

    private String currentDate = PubFun.getCurrentDate();

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 进行数据查询
        if (!queryData()) {
            return false;
        }

        return true;
    }

    /**
     * 处理案件数
     * @param aClaimerCode String
     * @return double
     */
    private double getDealCase(String aClaimerCode) {
        String tSql = "SELECT COUNT(1) FROM llcase WHERE "
                      + "handler='" + aClaimerCode +
                      "' AND endcasedate BETWEEN '"
                      + mStartDate + "' AND '" + mEndDate
                      + "' AND accdentdesc<>'磁盘导入' WITH UR";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        return tValue;
    }

    /**
     * 审批案件数
     * @param aClaimerCode String
     * @return double
     */
    private double getUWCase(String aClaimerCode) {
        String tSql = "SELECT COUNT(1) FROM llcase a,llcaseoptime b WHERE "
                      + "a.caseno=b.caseno AND endcasedate BETWEEN '"
                      + mStartDate + "' AND '" + mEndDate
                      + "' AND b.operator='" + aClaimerCode +
                      "' AND b.rgtstate='06' "
                      + "AND a.accdentdesc<>'磁盘导入' WITH UR";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        return tValue;
    }

    /**
     * 抽检案件数
     * @param aClaimerCode String
     * @return double
     */
    private double getSignCase(String aClaimerCode) {
        String tSql = "SELECT COUNT(1) FROM llcase a,llcaseoptime b WHERE "
                      + "a.caseno=b.caseno AND endcasedate BETWEEN '"
                      + mStartDate + "' AND '" + mEndDate
                      + "' AND b.operator='" + aClaimerCode +
                      "' AND b.rgtstate='06' "
                      + "AND a.accdentdesc<>'磁盘导入' WITH UR";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        return tValue;
    }

    /**
     * 回退案件数
     * @param aClaimerCode String
     * @return double
     */
    private double getBackCase(String aClaimerCode) {
        String tSql = "SELECT COUNT(1) FROM llcase a WHERE a.handler='"
                      + aClaimerCode + "' AND a.endcasedate BETWEEN '" +
                      mStartDate + "' and '" + mEndDate
                      + "' AND EXISTS (SELECT 1 FROM llcaseback WHERE caseno=a.caseno AND backtype IS NULL) "
                      + " AND a.accdentdesc<>'磁盘导入' WITH UR";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        return tValue;
    }

    /**
     * 申诉、纠错案件数
     * @param aClaimerCode String
     * @param aRgtType String
     * @return double
     */
    private double getAppealCase(String aClaimerCode, String aRgtType) {
        String tSql = "SELECT COUNT(1) FROM llcase WHERE "
                      + "handler='" + aClaimerCode +
                      "' AND endcasedate BETWEEN '"
                      + mStartDate + "' AND '" + mEndDate
                      + "' AND rgttype='" + aRgtType + "' WITH UR";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        return tValue;
    }

    /**
     * 案件处理天数
     * @param aClaimerCode String
     * @return double
     */
    private double getCaseDay(String aClaimerCode) {
        String tSql = "SELECT SUM(endcasedate-rgtdate+1) FROM llcase a WHERE " +
                      " a.endcasedate BETWEEN '" +
                      mStartDate + "' AND '" + mEndDate + "' AND a.handler='" +
                      aClaimerCode + "' AND a.accdentdesc<>'磁盘导入' WITH UR";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        return tValue;
    }

    /**
     * 3日结案案件
     * @param aClaimerCode String
     * @return double
     */
    private double get3DayCase(String aClaimerCode) {
        String tSql = "SELECT COUNT(1) FROM llcase WHERE endcasedate BETWEEN '" +
                      mStartDate + "' AND '" + mEndDate + "' AND handler = '" +
                      aClaimerCode +
                      "' AND endcasedate - rgtdate <= 3 AND accdentdesc <> '磁盘导入' WITH UR";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        return tValue;
    }

    /**
     * 受理案件数
     * @param aClaimerCode String
     * @return double
     */
    private double getRegistCase(String aClaimerCode) {
        String tSql = "SELECT COUNT(1) FROM llcase WHERE rgtdate BETWEEN '" +
                      mStartDate + "' AND '" + mEndDate + "' AND rigister = '" +
                      aClaimerCode + "' AND accdentdesc <> '磁盘导入' WITH UR";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        return tValue;
    }

    /**
     * 撤件数
     * @param aClaimerCode String
     * @return double
     */
    private double getCancleCase(String aClaimerCode) {
        String tSql = "SELECT COUNT(1) FROM llcase WHERE rgtdate BETWEEN '" +
                      mStartDate + "' AND '" + mEndDate + "' AND rigister = '" +
                      aClaimerCode +
                      "' AND accdentdesc <> '磁盘导入' AND rgtstate='14' WITH UR";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        return tValue;
    }

    /**
     * 调查案件数
     * @param aClaimerCode String
     * @return double
     */
    private double getSurveyCase(String aClaimerCode) {
        String tSql =
                "SELECT COUNT(DISTINCT a.caseno) FROM llcase a,llsurvey b "
                + "WHERE a.caseno=b.otherno AND b.surveyoperator='" +
                aClaimerCode + "' AND a.endcasedate BETWEEN '" + mStartDate +
                "' AND '" + mEndDate +
                "' AND a.accdentdesc <> '磁盘导入' WITH UR";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        return tValue;
    }

    /**
     * 调查后拒付案件
     * @param aClaimerCode String
     * @return double
     */
    private double getSRefuseCase(String aClaimerCode) {
        String tSql =
                "SELECT COUNT(DISTINCT a.caseno) FROM llcase a,llsurvey b,llclaim c "
                +
                "WHERE a.caseno=b.otherno AND a.caseno=c.caseno AND b.surveyoperator='" +
                aClaimerCode + "' AND a.endcasedate BETWEEN '" + mStartDate +
                "' AND '" + mEndDate +
                "' AND a.accdentdesc <> '磁盘导入' AND c.givetype='3' WITH UR";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        return tValue;
    }

    /**
     * 七日查讫案件数
     * @param aClaimerCode String
     * @return double
     */
    private double get7DaySCase(String aClaimerCode) {
        String tSql =
                "SELECT COUNT(DISTINCT a.caseno) FROM llcase a,llsurvey b "
                + "WHERE a.caseno=b.otherno AND b.surveyoperator='" +
                aClaimerCode + "' AND a.endcasedate BETWEEN '" + mStartDate +
                "' AND '" + mEndDate +
                "' AND a.accdentdesc <> '磁盘导入' AND b.surveyenddate-b.surveystartdate<=7 WITH UR";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        return tValue;
    }

    /**
     * 调查天数
     * @param aClaimerCode String
     * @return double
     */
    private double getSCaseDay(String aClaimerCode) {
        String tSql =
                "SELECT SUM(b.surveyenddate-b.surveystartdate+1) FROM llcase a,llsurvey b "
                + "WHERE a.caseno=b.otherno AND b.surveyoperator='" +
                aClaimerCode + "' AND a.endcasedate BETWEEN '" + mStartDate +
                "' AND '" + mEndDate +
                "' AND a.accdentdesc <> '磁盘导入' WITH UR";

        ExeSQL tExeSQL = new ExeSQL();
        String tCount1 = tExeSQL.getOneValue(tSql);

        double tValue = 0;
        if (tCount1 == null || tCount1.equals("") || tCount1.equals("0")) {
            tValue = 0;
        } else {
            tValue = Double.parseDouble(tCount1);
        }
        return tValue;
    }

    private boolean getDataList() {
        DecimalFormat tDF = new DecimalFormat("0.##");
        String tClaimerCode = "";
        String tClaimerName = "";

        double tSumDealCase = 0;
        double tSumUWCase = 0;
        double tSumSignCase = 0;
        double tSumBackCase = 0;
        double tSumAppeal5Case = 0;
        double tSumAppeal4Case = 0;
        double tSumCaseDay = 0;
        double tSum3DayCase = 0;
        double tSumRegistCase = 0;
        double tSumCancleCase = 0;
        double tSumSurveyCase = 0;
        double tSumSRefuseCase = 0;
        double tSum7DaySCase = 0;
        double tSumSCaseDay = 0;

        String tSQL = null;
        tSQL =
                "SELECT usercode,username FROM  llclaimuser WHERE  comcode like '"
                + mManageCom + "%' ORDER BY usercode WITH UR";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) { // 循环理赔人进行统计
            tClaimerCode = tSSRS.GetText(i, 1);
            tClaimerName = tSSRS.GetText(i, 2);

            double tDealCase = getDealCase(tClaimerCode);
            tSumDealCase += tDealCase;
            double tUWCase = getUWCase(tClaimerCode);
            tSumUWCase += tUWCase;
            double tSignCase = getSignCase(tClaimerCode);
            tSumSignCase += tSignCase;
            double tBackCase = getBackCase(tClaimerCode);
            tSumBackCase += tBackCase;
            double tAppeal5Case = getAppealCase(tClaimerCode, "5");
            tSumAppeal5Case += tAppeal5Case;
            double tAppeal4Case = getAppealCase(tClaimerCode, "4");
            tSumAppeal4Case += tAppeal4Case;
            double tCaseDay = getCaseDay(tClaimerCode);
            tSumCaseDay += tCaseDay;
            double t3DayCase = get3DayCase(tClaimerCode);
            tSum3DayCase += t3DayCase;
            double tRegistCase = getRegistCase(tClaimerCode);
            tSumRegistCase += tRegistCase;
            double tCancleCase = getCancleCase(tClaimerCode);
            tSumCancleCase += tCancleCase;
            double tSurveyCase = getSurveyCase(tClaimerCode);
            tSumSurveyCase += tSurveyCase;
            double tSRefuseCase = getSRefuseCase(tClaimerCode);
            tSumSRefuseCase += tSRefuseCase;
            double t7DaySCase = get7DaySCase(tClaimerCode);
            tSum7DaySCase += t7DaySCase;
            double tSCaseDay = getSCaseDay(tClaimerCode);
            tSumSCaseDay += tSCaseDay;

            String Info[] = new String[15];
            Info[0] = tClaimerCode;
            Info[1] = tClaimerName;
            Info[2] = tDF.format(tDealCase);
            Info[3] = tDF.format(tUWCase);
            Info[4] = tDF.format(tSignCase);

            if (tDealCase == 0) {
                Info[5] = "0";
                Info[6] = "0";
                Info[7] = "0";
                Info[8] = "0";
                Info[9] = "0";
            } else {
                Info[5] = tDF.format(tBackCase / tDealCase);
                Info[6] = tDF.format(tAppeal5Case / tDealCase);
                Info[7] = tDF.format(tAppeal4Case / tDealCase);
                Info[8] = tDF.format(tCaseDay / tDealCase);
                Info[9] = tDF.format(t3DayCase / tDealCase);
            }

            Info[10] = tDF.format(tRegistCase);

            if (tRegistCase == 0) {
                Info[11] = "0";
            } else {
                Info[11] = tDF.format(tCancleCase / tRegistCase);
            }

            if (tSurveyCase == 0) {
                Info[12] = "0";
                Info[13] = "0";
                Info[14] = "0";
            } else {
                Info[12] = tDF.format(tSRefuseCase / tSurveyCase);
                Info[13] = tDF.format(t7DaySCase / tSurveyCase);
                Info[14] = tDF.format(tSCaseDay / tSurveyCase);
            }
            mListTable.add(Info);
        }

        String SumInfo[] = new String[15];
        SumInfo[0] = "合计";
        SumInfo[1] = "";
        SumInfo[2] = tDF.format(tSumDealCase);
        SumInfo[3] = tDF.format(tSumUWCase);
        SumInfo[4] = tDF.format(tSumSignCase);
        if (tSumDealCase == 0) {
            SumInfo[5] = "0";
            SumInfo[6] = "0";
            SumInfo[7] = "0";
            SumInfo[8] = "0";
            SumInfo[9] = "0";
        } else {
            SumInfo[5] = tDF.format(tSumBackCase / tSumDealCase);
            SumInfo[6] = tDF.format(tSumAppeal5Case / tSumDealCase);
            SumInfo[7] = tDF.format(tSumAppeal4Case / tSumDealCase);
            SumInfo[8] = tDF.format(tSumCaseDay / tSumDealCase);
            SumInfo[9] = tDF.format(tSum3DayCase / tSumDealCase);
        }

        SumInfo[10] = tDF.format(tSumRegistCase);

        if (tSumRegistCase == 0) {
            SumInfo[11] = "0";
        } else {
            SumInfo[11] = tDF.format(tSumCancleCase / tSumRegistCase);
        }

        if (tSumSurveyCase == 0) {
            SumInfo[12] = "0";
            SumInfo[13] = "0";
            SumInfo[14] = "0";
        } else {
            SumInfo[12] = tDF.format(tSumSRefuseCase / tSumSurveyCase);
            SumInfo[13] = tDF.format(tSum7DaySCase / tSumSurveyCase);
            SumInfo[14] = tDF.format(tSumSCaseDay / tSumSurveyCase);
            }
        mListTable.add(SumInfo);

        return true;
    }

    /**
     * 进行数据查询
     *
     * @return boolean
     */
    private boolean queryData() {
        TextTag tTextTag = new TextTag();
        mXmlExport = new XmlExport();
        // 设置模版名称
        mXmlExport.createDocument("LLClaimerEfficiencyReport.vts", "printer");
        System.out.print("dayin252");
        tTextTag.add("ManageComName", mManageComName);
        tTextTag.add("StartDate", mStartDate);
        tTextTag.add("EndDate", mEndDate);
        tTextTag.add("Operator", moperator);
        tTextTag.add("MakeDate", currentDate);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] title = {"", "", "", "", "", "", "", "", "", "", "", "", "",
                         "", ""};
        if (!getDataList()) {
            return false;
        }
        mListTable.setName("ENDOR");
        System.out.println("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.println("121");
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 取得传入的数据
     *
     * @return boolean
     */
    private boolean getInputData(VData pmInputData) {
        // 全局变量
        // mGlobalInput.setSchema((GlobalInput)
        // pmInputData.getObjectByObjectName(
        // "GlobalInput", 0));
        mStartDate = (String) pmInputData.get(0);
        mEndDate = (String) pmInputData.get(1);
        mManageCom = (String) pmInputData.get(2);
        moperator = (String) pmInputData.get(3);
        mManageComName = (String) pmInputData.get(4);

        return true;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLCaseStatisticsBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 取得返回处理过的结果
     *
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    private void jbInit() throws Exception {
    }
}
