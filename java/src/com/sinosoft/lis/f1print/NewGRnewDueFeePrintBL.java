package com.sinosoft.lis.f1print;


import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.f1print.*;
import java.util.*;
import com.sinosoft.lis.bq.*;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class NewGRnewDueFeePrintBL implements PrintService{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private VData mResult = new VData();

    private LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
    private LCGrpAddressSchema tLCGrpAddressSchema = new LCGrpAddressSchema();
    private LJSPayGrpSchema tLJSPayGrpSchema = new LJSPayGrpSchema();
    private LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
    private LJSPayBDB tLJSPayBDB = new LJSPayBDB();
    private SSRS mSSRS;
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    //交费信息
    private int needPrt = -1 ;
    private String mOperate = "";
    private String showPaymode = "";
    private String strPayToDate = "";
    private String strPayDate = "";
    private String mDif = "";
    private String strPayCanDate = "";
    TextTag textTag = new TextTag(); //新建一个TextTag的实例tLJSPaySchema

    private Object tExeSQL;
    
    private String mflag = null;
    public NewGRnewDueFeePrintBL() {

    }


    //By WangJH

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        System.out.println("IndiDueFeePrintBL begin");
        mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mflag = cOperate;
        mResult.clear();
        if (!getListData()) {
            return false;
        }

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }

        System.out.println("IndiDueFeePrintBL end");
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);

        LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();
        ttLOPRTManagerSchema = (LOPRTManagerSchema)cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setStandbyFlag2(ttLOPRTManagerSchema.getStandbyFlag2());
        LOPRTManagerSet tLOPRTManagerSet = tLOPRTManagerDB.query();
        if(tLOPRTManagerSet!=null && tLOPRTManagerSet.size()>0){
        mLOPRTManagerSchema = tLOPRTManagerSet.get(1);
        }
        if(ttLOPRTManagerSchema == null)
        {//为空--VTS打印入口
            System.out.println("---------为空--VTS打印入口！");
            tLCGrpContSchema = (LCGrpContSchema) cInputData.getObjectByObjectName("LCGrpContSchema", 0);
            tLJSPayBDB = (LJSPayBDB) cInputData.getObjectByObjectName("LJSPayBDB", 0);
            if (tLCGrpContSchema == null)
            {
                buildError("getInputData", "为空--VTS打印入口没有得到足够的信息！");
                return false;
            }
            return true;
        }
        else
        {//PDF入口
            //取得LJSPayBDB
            System.out.println("---------不为空--PDF入口！");
            LJSPayBDB mLJSPayBDB = new LJSPayBDB();
            mLJSPayBDB.setGetNoticeNo(ttLOPRTManagerSchema.getStandbyFlag2());
            if(!mLJSPayBDB.getInfo())
            {
                mErrors.addOneError("PDF入口LJSPayBDB传入的数据不完整。");
                return false;
            }
            tLJSPayBDB = mLJSPayBDB.getDB();

            //取得tLCGrpContSchema
            LCGrpContDB mLCGrpContDB = new LCGrpContDB();
            mLCGrpContDB.setGrpContNo(mLJSPayBDB.getOtherNo());
            if(!mLCGrpContDB.getInfo())
            {
                buildError("getInputData", "LCContDB没有得到足够的信息！");
                return false;
            }
            tLCGrpContSchema = mLCGrpContDB.getSchema();
            return true;
        }

    }

    public VData getResult() {
        return this.mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }


    private boolean getPrintData() {
        String GrpContNo = tLCGrpContSchema.getGrpContNo();
        System.out.println("GrpContNo=" + GrpContNo);
        String GetNoticeNo = "";
        String sql = "";
        String sqlone = "";
        String sqltwo = "";
        String sql3 = "";
        String s1 = "";

        //获取投保人地址
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(GrpContNo);

        if (!tLCGrpAppntDB.getInfo()) {
            CError.buildErr(this,
                            "投保人表中缺少数据");
            return false;
        }

        sql = "select * from LCGrpAddress where CustomerNo=(select appntno from LCGrpCont where GrpContNo='" +
              GrpContNo + "') AND AddressNo = '" + tLCGrpAppntDB.getAddressNo() +
              "'";
        System.out.println("sql=" + sql);
        LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
        LCGrpAddressSet tLCGrpAddressSet = tLCGrpAddressDB.executeQuery(sql);
        tLCGrpAddressSchema = tLCGrpAddressSet.get(tLCGrpAddressSet.size()).
                           getSchema();

        sqlone = "select * from lCGrpCont where GrpContNo='" + GrpContNo + "'";
        System.out.println("sqlone=" + sqlone);
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.executeQuery(sqlone);
        tLCGrpContSchema = tLCGrpContSet.get(1).getSchema();

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例

        switch (Integer.parseInt(tLCGrpContSchema.getPayMode()))
        {
        case 1:
            showPaymode = "现金";
            xmlexport.createDocument("GrpDueFeePrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;

        case 2:
            showPaymode = "现金支票";
            xmlexport.createDocument("GrpDueFeePrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;
        case 3:
            showPaymode = "支票";
            xmlexport.createDocument("GrpDueFeePrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;

        case 4:
            showPaymode = "银行转账";
            xmlexport.createDocument("GrpDueFeeBankPrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;
        case 11:
            showPaymode = "银行汇款";
            xmlexport.createDocument("GrpDueFeePrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;
        default:
            break;
        };
        
        String sqlusercom = "select comcode from lduser where usercode='" +
                          mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        textTag.add("JetFormType", "93");
        if (comcode.equals("86") || comcode.equals("8600")||comcode.equals("86000000")) {
        	comcode = "86";
        } else if (comcode.length() >= 4) {
        	comcode = comcode.substring(0, 4);
        } else {
        	buildError("getInputData", "操作员机构查询出错！");
        	return false;
        }
        String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"+comcode+"' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        textTag.add("ManageComLength4", printcode);
        textTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.","_"));
        if(mflag.equals("batch")){
        	textTag.add("previewflag", "0");
        }else{
        	textTag.add("previewflag", "1");
        }
        setFixedInfo();

        textTag.add("GrpZipCode", tLCGrpAddressSchema.getGrpZipCode());
        System.out.println("GrpZipCode=" + tLCGrpAddressSchema.getGrpZipCode());
        textTag.add("GrpAddress", tLCGrpAddressSchema.getGrpAddress());
        String appntPhoneStr = " ";
        if(tLCGrpAddressSchema.getPhone1() != null&&!tLCGrpAddressSchema.getPhone1().equals("")){
            appntPhoneStr += tLCGrpAddressSchema.getPhone1() + "、";
        }
        else if (tLCGrpAddressSchema.getMobile1() != null&&!tLCGrpAddressSchema.getMobile1().equals("")){
            appntPhoneStr += tLCGrpAddressSchema.getMobile1() + "、";
        }
        appntPhoneStr = appntPhoneStr.substring(0,appntPhoneStr.length()-1);
        textTag.add("AppntPhone", appntPhoneStr);
        textTag.add("LinkMan1", tLCGrpAddressSchema.getLinkMan1());
        textTag.add("AppntNo", tLCGrpContSchema.getAppntNo());
        textTag.add("GrpContNo", GrpContNo);
        System.out.println("GrpContNo=" + "" + GrpContNo);
        textTag.add("GrpName", tLCGrpContSchema.getGrpName());
        textTag.add("GetNoticeNo", tLJSPayBDB.getGetNoticeNo());
        textTag.add("MakeDate", tLJSPayBDB.getMakeDate());
        textTag.add("BankAcc", tLCGrpContSchema.getBankAccNo());
        textTag.add("AccName", tLCGrpContSchema.getAccName());
        textTag.add("XI_ManageCom", tLCGrpContSchema.getManageCom());

        textTag.add("LinkManSex", "先生/女士");

        if(!getBale()){
            return false;
        }
        textTag.add("Dif", mDif);
        textTag.add("Paymode",showPaymode);
        //取得银行名称
        if ("1".equals(showPaymode)){
            LDBankDB tLDBankDB = new LDBankDB();
            tLDBankDB.setBankCode(tLCGrpContSchema.getBankCode());
            if (!tLDBankDB.getInfo()){
                textTag.add("BankName", "-");
            }else{
                textTag.add("BankName", tLDBankDB.getBankName());
            }
        }else{
            textTag.add("BankName", "-");
        }

        //取得交费频次
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("payintv");
        tLDCodeDB.setCode(Integer.toString(tLCGrpContSchema.getPayIntv()));

        if (!tLDCodeDB.getInfo()) {
            CError.buildErr(this,
                            "公用代码表表中缺少数据");
            return false;
        }
        textTag.add("PayIntvName", tLDCodeDB.getCodeName());

        //应收日期
        FDate tD = new FDate();
        System.out.println("PayDate" + tLJSPayBDB.getPayDate());
        Date newBaseDate = tD.getDate(tLJSPayBDB.getPayDate());
        Date PayToDate = tD.getDate(getPayToDate());
        strPayToDate = tD.getString(PayToDate);
        strPayDate = tD.getString(newBaseDate);
        if ("1".equals(showPaymode)) {
            textTag.add("PayDate",CommonBL.decodeDate(tLJSPayBDB.getStartPayDate())
                        +"至"+CommonBL.decodeDate(strPayDate)+"");
        } else {
            textTag.add("PayDate", strPayDate);
        }
        textTag.add("PayEndDate", strPayDate);
        textTag.add("PayToDate", strPayToDate);
        textTag.add("SumPrem",CommonBL.bigDoubleToCommonString(tLJSPayBDB.getSumDuePayMoney(), "0.00"));
        textTag.add("Prem", getPrem());

        //业务员信息
        String agntPhone = "";
        String temPhone = "";
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(tLCGrpContSchema.getAgentCode());
        tLaAgentDB.getInfo();
        textTag.add("AgentName", tLaAgentDB.getName());
        textTag.add("AgentCode", tLaAgentDB.getGroupAgentCode());
        temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") || temPhone.equals("null")) {
            agntPhone = tLaAgentDB.getPhone();
        } else {
            agntPhone = temPhone;
        }
        textTag.add("Phone", agntPhone);

        //机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();

        String branchSQL = " select * from LABranchGroup where agentgroup = "
			+" (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
			+" (select agentgroup from laagent where agentcode ='"
                        + tLaAgentDB.getAgentCode()+"'))"
			;
	LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(branchSQL);
        if(tLABranchGroupSet==null||tLABranchGroupSet.size()==0)
        {
            CError.buildErr(this, "查询业务员机构失败");
            return false;
        }
        textTag.add("AgentGroup", tLABranchGroupSet.get(1).getName());

        textTag.add("BarCode1", tLJSPayBDB.getGetNoticeNo());
        textTag.add("BarCodeParam1","BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        System.out.println("GetNoticeNo" + tLJSPayBDB.getGetNoticeNo());

        if (textTag.size() > 0) {
            xmlexport.addTextTag(textTag);
        }

        String[] title = {"险种序号", "险种名称", "被保险人", "投保时间", "保险期间",
                         "保险金额", "期交保费", "保费性质", "续保说明"};
        xmlexport.addListTable(getListTable(), title);
        
        ListTable tEndTable = new ListTable();
        tEndTable.setName("END");
        String[] tEndTitle = new String[0];
        xmlexport.addListTable(tEndTable, tEndTitle);
        
        xmlexport.outputDocumentToFile("C:\\","海外医疗团单续保打印通知书");
        mResult.clear();
        mResult.addElement(xmlexport);


       
            //放入打印列表
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            tLOPRTManagerDB.setStandbyFlag2(tLJSPayBDB.getGetNoticeNo());
            tLOPRTManagerDB.setCode("93");
            needPrt = tLOPRTManagerDB.query().size();
            System.out.println("-------------------------the size is "+needPrt+"--------------------------");
            if (needPrt == 0)
            { //没有数据，进行封装
                String tLimit = PubFun.getNoLimit(tLCGrpContSchema.getManageCom());
                String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
                mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
                mLOPRTManagerSchema.setOtherNo(tLCGrpContSchema.getGrpContNo());
                mLOPRTManagerSchema.setOtherNoType("00");
                mLOPRTManagerSchema.setCode(PrintPDFManagerBL.CODE_INDIINFORM);
                mLOPRTManagerSchema.setManageCom(tLCGrpContSchema.getManageCom());
                mLOPRTManagerSchema.setAgentCode(tLCGrpContSchema.getAgentCode());
                mLOPRTManagerSchema.setReqCom(tLCGrpContSchema.getManageCom());
                mLOPRTManagerSchema.setReqOperator(tLCGrpContSchema.getOperator());
                mLOPRTManagerSchema.setPrtType("0");
                mLOPRTManagerSchema.setStateFlag("0");
                mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
                mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
                mLOPRTManagerSchema.setStandbyFlag2(tLJSPayBDB.getGetNoticeNo()); //这里存放 （也为交费收据号）
            }else{
            	mLOPRTManagerSchema.setPrintTimes(mLOPRTManagerSchema.getPrintTimes()+1);
            }
               mResult.add(mLOPRTManagerSchema);


        return true;
    }

    /**
     * 得到险种未续保之前的交至日期
     * @return String
     */
    private String getPayToDate()
    {
        //当前应收记录的lastPayToDate即是未续保之前的险种交至日期
        String sql = "  select lastPayToDate "
                     + "from LJSPayGrpB "
                     + "where getNoticeNo = '" + tLJSPayBDB.getGetNoticeNo()
                     + "' ";
        ExeSQL e = new ExeSQL();
        String payToDate = e.getOneValue(sql);
        if(payToDate.equals("") || payToDate.equals("null")){
        	payToDate = e.getOneValue("select paytodate from lcgrppol where grpcontno = '"+tLCGrpContSchema.getGrpContNo()+"' fetch first 1 rows only");
        }
        return payToDate;
    }

    /**
     * 查询总期缴保费
     * @return String
     */
    private String getPrem()
    {
        StringBuffer sql = new StringBuffer();
        sql.append(" select sum(sumDuePayMoney) ")
            .append("from LJSPayPersonB ")
            .append("where getNoticeNo = '")
            .append(tLJSPayBDB.getGetNoticeNo())
            .append("'  and sumDuePayMoney > 0 ")
            .append("   and payType = 'ZC' ");
        ExeSQL e = new ExeSQL();
        String prem = e.getOneValue(sql.toString());
        if(prem.equals("") || prem.equals("null")){
            CError.buildErr(this, "没有查询到财务数据");
            return "0.00";
        }
        return CommonBL.bigDoubleToCommonString(Double.parseDouble(prem),
                                                "0.00");
    }

    private void setFixedInfo() {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLCGrpContSchema.getManageCom());
        tLDComDB.getInfo();
        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("letterservicename", tLDComDB.getLetterServiceName());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getLetterServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        textTag.add("Fax", tLDComDB.getFax());
    }

    /**
     *  获取列表数据
     * @param 无
     * @return ListTable
     */
    private ListTable getListTable() {

        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[10];
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                info[j - 1] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }
        tListTable.setName("ZT");
        return tListTable;
    }

    /**
     * 查询列表显示数据
     * @param schema LCGrpContSchema
     * @return String
     */

    private boolean getListData() {
        StringBuffer sql = new StringBuffer();
        sql.append("select a.SignDate ")
            .append("from LBCont a, LCRnewStateLog b ")
            .append("where a.GrpContNo = b.newGrpContNo ")
            .append("   and b.GrpContNo = '")
            .append(tLJSPayBDB.getOtherNo())
            .append("' order by b.newGrpContNo ");
        ExeSQL e = new ExeSQL();
        String sourceSignDate = e.getOneValue(sql.toString());

        if(sourceSignDate.equals("null")){
            sourceSignDate = "";
        }

        StringBuffer sqlSB = new StringBuffer();
        sqlSB.append("select distinct a.RiskSeqNo rsn,(select riskname  from lmrisk where riskcode=a.riskcode) ,a.InsuredName , ")
            .append("   case when b.FactorValue='1' then trim(to_char(a.Amnt)) || '.00' when b.FactorValue='3' then to_char(a.Mult) end, ");
        if(sourceSignDate.equals("")){
            sqlSB.append("   (select polApplyDate from LCGrpCont where GrpContNo = '")
                .append(tLJSPayBDB.getOtherNo()).append("'), ");
        }else{
            sqlSB.append("'").append(sourceSignDate).append("', ");
        }
        sqlSB.append("   char(c.lastpaytodate)||'～'||char((select max(curpaytodate)- 1 day from ljspaypersonb where polno=a.polno)), ")
                .append("   (select codeName from LDCode where codeType = 'payintv' and code = char(a.payIntv)), (select varchar(sum(sumDuePayMoney)) from LJSPayPersonB where polNo = c.polNo and getNoticeNo = c.getNoticeNo and sumDuePayMoney >= 0 and payType = 'ZC'),")
                .append("   case when c.PayTypeFlag = '1' then (SELECT case rnewFlag when 'B' then '保证续保' else '同意续保' end from LMRisk where riskCode = a.riskCode) else '' end, ") //续保说明
                .append("   case when c.PayTypeFlag = '1' then '续保保费' else '续期保费' end ")
                .append("from LCPol a,LDRiskParamPrint b, LJSPayPersonB c ")
                .append("where a.polNo = c.polNo ")
                .append("  and a.RiskCode=b.RiskCode")
                .append("  and c.getNoticeNo = '")
                .append(tLJSPayBDB.getGetNoticeNo()).append("' ");

        //只处理续保终止
        sqlSB.append(" union ")
            .append("select a.RiskSeqNo rsn,(select riskname  from lmrisk where riskcode=a.riskcode),")
            .append("   a.insuredName, ")
            .append("   (select case min(FactorValue) when '1' then trim(to_char(a.Amnt)) || '.00' when '3' then to_char(a.Mult) end from LDRiskParamPrint where riskCode = a.riskCode), ")
            .append("   a.polApplyDate, ")
            .append("   (select char(max(lastPayToDate)) || '～' || char(max(curpaytodate) - 1 day ) from ljApayperson where polno=a.polno), ")
            .append("   (select codeName from LDCode where codeType = 'payintv' and code = char(a.payIntv)),")
            .append("   varchar(a.prem), '终止续保', '' ")
            .append("from LCPol a, LCRnewStateLog b, LPUWMaster c, LGWork d ")
            .append("where a.polNo = b.polNo ")
            .append("   and b.newPolNo = c.polNo ")
            .append("   and c.edorNo = d.workNo ")
            .append("   and (c.passFlag = '").append(BQ.PASSFLAG_STOPXB)  //续保终止、
            .append("'  or c.passFlag = '").append(BQ.PASSFLAG_CONDITION)  //条件通过且客户不同意则续保终止
            .append("'     and CustomerReply = '2' and DisagreeDeal='3') ")
            .append("   and c.edorType = '")
            .append(BQ.EDORTYPE_XB).append("' ")
            .append("   and d.innersource = '")
            .append(tLJSPayBDB.getGetNoticeNo())
            .append("' order by rsn");

        String tSQL = sqlSB.toString();
        System.out.println(tSQL);
        ExeSQL tExeSQL = new ExeSQL();

        mSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 查询保单余额
     * @param schema LCGrpContSchema
     * @return String
     */

    private boolean getBale() {
        //得到账户余额
        String sql = "  select abs(sum(sumActuPayMoney)) "
                     + "from LJSPayPersonB "
                     + "where getNoticeNo = '"
                     + tLJSPayBDB.getGetNoticeNo() + "' "
                     + "   and payType = 'YEL' ";
        ExeSQL tExeSQL = new ExeSQL();
        String count = tExeSQL.getOneValue(sql);
        if((count != null) && !count.equals("")){
            mDif = CommonBL.bigDoubleToCommonString(Double.parseDouble(count),"0.00");
        }else{
            mDif = "0.00";
        }

        return true;
    }

    public static void main(String[] a)
    {
        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBDB.setGetNoticeNo("31000629469");
        tLJSPayBDB.getInfo();

        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo(tLJSPayBDB.getOtherNo());

        GlobalInput tG = new GlobalInput();
        tG.Operator = "pa0001";
        tG.ComCode = "86";
        tG.ClientIP = "10.253.33.6";

        VData tVData = new VData();
        tVData.addElement(tLCGrpContSchema);
        tVData.addElement(tG);
        tVData.addElement(tLJSPayBDB);
        
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setStandbyFlag2("31000629469");
        tVData.addElement(tLOPRTManagerSchema);
        
        
        NewGRnewDueFeePrintBL bl = new NewGRnewDueFeePrintBL();
        if(bl.submitData(tVData, "PRINT")){
            System.out.println(bl.mErrors.getErrContent());
        }
    }

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealPrintMag() {
        MMap tMap = new MMap();
        tMap.put(mLOPRTManagerSchema, "INSERT");
        VData tVData = new VData();
        tVData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tVData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
            return false;
        }
        return true;
    }
}
