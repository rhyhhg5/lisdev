package com.sinosoft.lis.charts;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 
 * Xml模板样例如下： <?xml version="1.0" encoding="UTF-8"?> <graph
 * xaxisname='Continent' hovercapbg='DEDEBE' hovercapborder='889E6D'
 * rotateNames='0' yAxisMaxValue='100' numdivlines='9' divLineColor='CCCCCC'
 * divLineAlpha='80' decimalPrecision='0' showAlternateHGridColor='1'
 * AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC' caption='作业量即时展示'
 * subcaption='全系统日受理-完成理赔案件量'> <categories font='Arial' fontSize='11'
 * fontColor='000000'> <category name='#MonthData' /> </categories> <dataset
 * seriesname='已受理' color='FDC12E'> <set value='#AcceptData' /> </dataset>
 * <dataset seriesname='已完成' color='56B9F9'> <set value='#DoneData' />
 * </dataset> </graph> 其中#MonthData 表示动态Sql，会根据动态Sql进行节点扩展 例如： <category
 * name='1月' /> <category name='2月' /> <category name='3月' /> <category
 * name='4月' />
 * 
 * @author yangming
 * 
 * 
 */
public class ChartsPaser {

	// 图表Xml模板
	private String mXmlName = null;
	// 数据参数
	private HashMap mParam = null;
	// Doc
	private Document doc = null;

	public ChartsPaser(String tXmlName) {
		this.mXmlName = tXmlName;
	}

	/**
	 * HashMap中保存着xml模板中需要的Sql
	 * 
	 * @param map
	 */
	public void setData(HashMap tParam) {
		mParam = tParam;
	}

	public boolean paser() {

		if (this.mXmlName == null || this.mXmlName.equals("")) {
			//
			System.err.println("mXmlName 为空");
			return false;
		}

		File tFile = new File(this.mXmlName);
		if (!tFile.exists()) {

			System.err.println("Xml文件路径不存在");
			return false;
		}

		/**
		 * 开始使用Dom4j解析xml文件，读取相应变量，获取相应数据
		 */
		if (!domParser(tFile)) {
			return false;
		}
		return true;
	}

	/**
	 * 解析xml文件
	 * 
	 * @return
	 */
	private boolean domParser(File tFile) {
		SAXReader saxReader = new SAXReader();
		Document dom = null;
		try {
			dom = saxReader.read(new BufferedReader(new InputStreamReader(new FileInputStream(tFile),"GBK")));
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Element el = dom.getRootElement();
		if (!dealDom(el)) {
			return false;
		}
		
		this.doc = el.getDocument();

		return true;
	}

	public String getDocument() {
		
		OutputFormat format = OutputFormat.createPrettyPrint();
		format.setEncoding("GBK");
		StringWriter xml = new StringWriter();
		String strXml = "";
		XMLWriter output = new XMLWriter(xml,format);
		try {
			output.write(doc);
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xml.toString();
	}
	/**
	 * 递归解析dom
	 * 
	 * @param el
	 * @return
	 */
	private boolean dealDom(Element el) {
		for (Iterator i = el.elementIterator(); i.hasNext();) {
			Element element = (Element) i.next();
			System.out.println(element.getName());

			/**
			 * 将所有的属性组成一个二维数组例如： AttributeName AttributeValue
			 * 
			 * seriesname Rice color FDC12E
			 * 
			 * 其目的是保证在new Element 时可以做到统一维度，避免出现一个Node中有多个动态参数属性的情况。
			 * 
			 */
			HashMap arParam = new HashMap();

			/**
			 * 用于校验查询结构是否维度相同
			 */
			int elCount = -1;

			/**
			 * 校验当前检点是否需要被Remove
			 */
			boolean needRemove = false;

			/**
			 * 获取当前Node的全部属性
			 */
			for (Iterator ai = element.attributeIterator(); ai.hasNext();) {
				Attribute at = (Attribute) ai.next();
				String atName = at.getName();
				String atValue = at.getValue();
				if (atValue != null && !atValue.equals("")) {
					// 判断如果属性值以$开头，表明当前element是需要动态扩展生成。
					if (atValue.startsWith("$")) {

						// 获取参数对应的Sql
						String sql = (String) this.mParam.get(atValue);
						if (sql == null) {
							System.err.println("没有参数" + atValue + "所对应的Sql");
							return false;
						}

						// 获取结果
						String[] arrResult = getResult(sql);
						if (elCount == -1) {

							elCount = arrResult.length;

						} else if (elCount > -1 && elCount != arrResult.length) {

							System.err.println("参数维度不统一");
							return false;

						}

						if (arrResult == null) {
							System.err.println("查询Sql：" + sql + "为空；");
							return false;
						}

						// 保存数据结果
						arParam.put(atName, arrResult);

						needRemove = true;

					} else if(atValue.startsWith("#")){
						//$代表静态数据。
						String data = (String) this.mParam.get(atValue);
						at.setValue(data);
					}
				}
				System.out.println("\t" + at.getName() + "=" + at.getValue());
			}

			if (elCount != -1) {
				/**
				 * new n个Element
				 */
				Element[] nElement = new Element[elCount];

				/**
				 * 遍历全部参数，将参数对应的数据全部获取。
				 */
				for (Iterator it = arParam.keySet().iterator(); it.hasNext();) {
					String tKey = (String) it.next();
					String[] arrResult = (String[]) arParam.get(tKey);
					for (int j = 0; j < arrResult.length; j++) {
						if (nElement[j] == null) {
							nElement[j] = element.getParent().addElement(
									element.getName());
						}
						if (nElement[j].attribute(tKey) == null) {
							nElement[j].addAttribute(tKey, arrResult[j]);
						}
					}
				}
			}


			if(needRemove){
				/**
				 * 删除当前节点
				 */
				Element parent = element.getParent();
				parent.remove(element);

			}else{

				dealDom(element);

			}

		}

		return true;
	}

	/**
	 * 将Sql查询结果返回数组
	 * 
	 * @param sql
	 * @return
	 */
	private String[] getResult(String sql) {
		SSRS ssrs = (new ExeSQL()).execSQL(sql);
		if (ssrs != null && ssrs.getMaxRow() > 0) {
			String[] data = new String[ssrs.getMaxRow()];
			for (int i = 0; i < data.length; i++) {
				data[i] = ssrs.GetText(i+1, 1);
			}
			return data;
		} else {
			return null;
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ChartsPaser cp = new ChartsPaser(
		"E:\\W_公司项目\\12.新PICC\\C_代码\\ui\\claimcharts\\template\\ClaimCharts.xml");
		HashMap tParam = new HashMap();
		tParam .put( "$MonthData", "select distinct rgtdate from " +
				"llcase " +
				"where " +
		"rgtdate between '2009-8-20' and '2009-8-31' with ur");
		tParam.put("#Accept","受理量");
		tParam .put( "$AcceptData", "select count(1) from " +
				"llcase " +
				"where " +
				"rgtdate between '2009-8-20' and '2009-8-31' " +
		"group by rgtdate with ur");
		tParam .put( "$DoneData", "select count(1) from " +
				"llcase " +
				"where " +
				"rgtdate between '2009-8-20' and '2009-8-31' " +
		"group by endcasedate with ur");
		tParam .put( "#Done", "完成量");
		cp.setData(tParam);
		cp.paser();
	}

}
