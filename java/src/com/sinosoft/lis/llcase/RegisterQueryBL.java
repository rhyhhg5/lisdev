package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title      : RegisterQueryBL.java</p>
 * <p>Description:将报案中的一些信息返回到立案主界面的程序</p>
 * <p>Copyright  : Copyright (c) 2002</p>
 * <p>Company    : Sinosoft</p>
 * @author       :LiuYansong
 * @version :    1.0
 */

public class RegisterQueryBL
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData ;
  private VData mResult = new VData();
  private String mOperate;

  private LLReportSchema mLLReportSchema = new LLReportSchema();
  public RegisterQueryBL() {}

  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    if (!getInputData(cInputData))
      return false;

    if (cOperate.equals("QUERY||MAIN"))
    {
      if(!queryData())
        return false;
    }
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  private boolean getInputData(VData cInputData)
  {
    if (mOperate.equals("QUERY||MAIN"))
    {
      mLLReportSchema.setSchema((LLReportSchema)cInputData.getObjectByObjectName("LLReportSchema",0));
      System.out.println("报案号码是"+mLLReportSchema.getRptNo());
    }
    return true;
  }

  /****************************************************************************
   * Name    :queryData();
   * Function:将报案信息返回到立案主界面上;
   * 借用LLReport中的RptMode来存放CaseState（理赔状态）;
   */
  private boolean queryData()
  {
    LLReportDB tLLReportDB = new LLReportDB();
    LLReportSchema tLLReportSchema = new LLReportSchema();
    tLLReportDB.setRptNo(mLLReportSchema.getRptNo());
    LLReportSet tLLReportSet = new LLReportSet();
    tLLReportSet.set(tLLReportDB.query());
    //报案分案信息的查询
    LLSubReportDB tLLSubReportDB = new LLSubReportDB();
    LLSubReportSet tLLSubReportSet = new LLSubReportSet();
    String t_sql = "select * from LLSubReport where RptNo = '"
                 +mLLReportSchema.getRptNo()+"' and CaseNo ='00000000000000000000' ";
    tLLSubReportSet.set(tLLSubReportDB.executeQuery(t_sql));
    System.out.println("");
    if(tLLReportSet.size()==0||tLLReportSet.size()==0)
    {
      this.mErrors.copyAllErrors(tLLReportDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "LLReportBL";
      tError.functionName = "queryData";
      tError.errorMessage = "没有该条报案信息！！！";
      this.mErrors.addOneError(tError);
      tLLReportSet.clear();
      return false;
    }
    if(tLLReportSet.size()>0)
    {
      tLLReportSchema.setSchema(tLLReportSet.get(1));
      String CaseState = CaseFunPub.getCaseStateByRptNo(mLLReportSchema.getRptNo());
      tLLReportSchema.setRptMode(CaseState);
    }
    mResult.clear();
    mResult.add(tLLReportSchema);
    mResult.add(tLLSubReportSet);
    return true;
  }
}