package com.sinosoft.lis.llcase;


import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

/**
 * 理赔Insurance提数，将提数结果打包并邮件发送
 * @author Houyd
 *
 */

public class SegentLiAnExtraction {

	public String mCurrentDate = PubFun.getCurrentDate();
	    private BufferedWriter mBufferedWriter;
	    
	    /**获取当前的工作路径*/
		private String mURL = "";
	    public CErrors mErrors = new CErrors();
	    private static final int BUFFER = 2048;
	    
	    /**对提数结果压缩成功标志：1-成功;0-失败;-1-失败终止*/
	    private int mFlag;
	    /**压缩文件的路径*/
	    private String mZipFilePath;
	    /** SQL结果存放文件名*/
	    private String mZipDocFileName = "";
	    
	    
	    private String mSQL ;
	    /** 邮件收件人 */
	    private String mAddress = "";
	    /** 邮件抄送人 */
	    private String mCcAddress = "";
	    /** 邮件暗抄人 */
	    private String mBccAddress = "";
	    
	public String getMAddress() {
			return mAddress;
		}

		public void setMAddress(String address) {
			mAddress = address;
		}

		public String getMBccAddress() {
			return mBccAddress;
		}

		public void setMBccAddress(String bccAddress) {
			mBccAddress = bccAddress;
		}

		public String getMCcAddress() {
			return mCcAddress;
		}

		public void setMCcAddress(String ccAddress) {
			mCcAddress = ccAddress;
		}
	    
	/**给定目录下创建文件夹*/
//	public static boolean createDir(String destDirName) {
//	    File dir = new File(destDirName);
//	    if(dir.exists()) {
//	     System.out.println("创建目录" + destDirName + "失败，目标目录已存在！");
//	     return false;
//	    }
//	    if(!destDirName.endsWith(File.separator))
//	     destDirName = destDirName + File.separator;
//	    // 创建单个目录
//	    if(dir.mkdirs()) {
//	     System.out.println("创建目录" + destDirName + "成功！");
//	     return true;
//	    } else {
//	     System.out.println("创建目录" + destDirName + "成功！");
//	     return false;
//	    }
//	}
//
	
	/**
	 * 执行批处理，并发送邮件
	 * @return
	 */
    public boolean getSagentData()
    {
    	Date date1=new Date();
    	SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd"); 
    	mZipDocFileName=df.format(date1)+".xls";
    	String tSQL = "select sysvarvalue from ldsysvar where sysvar = 'UIRoot' with ur";
    	mURL = new ExeSQL().getOneValue(tSQL);//生成文件的存放路径
    	
    	mURL += "temp_lp/segent/";
        //this.mURL = "D:\\test\\";
        System.out.println("网盘路径："+mURL);
        if(PubDocument.save(mURL)){
        	mURL = PubDocument.getSavePath(mURL);
        }
        if (mURL == null || mURL.equals(""))
        {
            System.out.println("没有找到理赔提数文件夹！");
            return false;
        }
        writeToExcel();
        sendSagentMail();
       
   		return true;
    }
    
    /**
     * 将查询结果的压缩包发送邮件
     *
     */
    private void sendSagentMail(){
    		MeiRiSendMail tSendMail = new MeiRiSendMail();
        	try {
    			tSendMail.SendMail();
    			if(!"".equals(mAddress)){
    				tSendMail.setToAddress(mAddress);
    			}
    			if(!"".equals(mCcAddress)){
    				tSendMail.setCcAddress(mCcAddress);
    			}
    			if(!"".equals(mBccAddress)){
    				tSendMail.setBccAddress(mBccAddress);
    			}
    		} catch (Exception ex) {
    			System.out.println("邮件配置失败："+ex.toString());
    		}
    		try {
    			//邮件正文
    			String tMailContent = "<b>胡涌哲,您好:</b>" +
    					"<br>&nbsp&nbsp&nbsp&nbsp相关提数详见附件</br>" ;
    			tSendMail.setFile(mURL+mZipDocFileName);
    			tSendMail.send(tMailContent, "河南每日提数");
    		} catch (Exception ex) {
    			System.out.println("邮件发送失败："+ex.toString());
    			ex.printStackTrace();
    		}
    	
    }
	  public static String getWeek(Date date){   
	        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");  
	        String week = sdf.format(date);  
	        return week;  
	    }  

    
    /**
     * 通过SQL语句查询结果，并写入EXCEL文件
     *
     */
    private void writeToExcel(){   	   
       	try{
       		Date date=new Date();  int a = 0;
         	String biaoqian="";     SSRS tSSRS = null;
           	WritableWorkbook book;
    			book = Workbook.createWorkbook( 
    					 new  File(mURL+mZipDocFileName));
    			if("星期一".equals(getWeek(date))){
    				a=3;
    			}else{
    				a=1;
    			}
    			//增加险种名称  add riskname 2018-10-25
    			mSQL="select (select name from ldcom where comcode=a.mngcom) 地区,c.contno 保单号,a.caseno 案件号,(select case   when riskcode in  ('730101', '332401', '730201', '332501') then  "
     			 	+ "'分红'   else  case   when risktype = 'A' then  '意外'   when risktype = 'H' then  '健康'   else  '其他'  end end  "
     			 	+ "from lmriskapp where riskcode = c.riskcode) 险种,"
     			 	+"(select riskname from lmriskapp where riskcode=c.riskcode)险种名称,"
     			 	+"b.RgtantName 申请人姓名,b.idno 申请人身份证号,b.RgtantMobile 申请人手机号,a.rgtdate 立案时间," 
     			 	+ "db2inst1.CodeName('llrgtreason',(select ReasonCode from LLAppClaimReason where caseno=a.caseno fetch  first row only )) 出险类型," 
     			 	+ "a.customername 被保险人姓名,a.mobilephone 被保险人手机号 from llcase a,llregister b,llclaimpolicy c where a.rgtno=b.rgtno and a.caseno=c.caseno and (a.mngcom = '86410000' or a.mngcom like '864101%' or a.mngcom like '864113%') and "
   					+ "a.rgtdate  >= current date - "+a+" DAYS and a.rgtdate  < current date with ur";
    				//
                 tSSRS = new ExeSQL().execSQL(mSQL);
       			biaoqian="立案"; 
                	//在文档对象中创建第一个工作薄 并起名叫第一页
           		WritableSheet sheet  =  book.createSheet( biaoqian ,  1 );
           	//操作行和列
           	//  以及单元格内容为test 参数1列 参数2行  参数3内容
  			  Label label1  =   new  Label( 0 ,  0 ,  "地区" );
           	  Label label2  =   new  Label( 1 ,  0 ,  "保单号" );
              Label label3  =   new  Label( 2 ,  0 ,  "案件号" );
              Label label4  =   new  Label( 3,   0 ,  "险种" );
              
              Label label5  =   new  Label( 4,    0,  "险种名称");
              
              Label label6  =   new  Label( 5 ,  0 ,  "申请人姓名" );
              Label label7  =   new  Label( 6 ,  0 ,  "申请人身份证号" );
              Label label8  =   new  Label( 7 ,  0 ,  "申请人手机号" );
              Label label9  =   new  Label( 8 ,  0 ,  "立案时间" );
              Label label10  =   new  Label( 9 ,  0 ,  "出险类型" );
              Label label11  =   new  Label( 10 ,  0 ,  "被保险人姓名" );
              Label label12  =   new  Label( 11 ,  0 ,  "被保险人手机号" );
              
              
           //  将定义好的单元格添加到工作表中 
              sheet.addCell(label1);
              sheet.addCell(label2);
              sheet.addCell(label3);
              sheet.addCell(label4);
              sheet.addCell(label5);
              sheet.addCell(label6);
              sheet.addCell(label7);
              sheet.addCell(label8);
              sheet.addCell(label9);
              sheet.addCell(label10);
              sheet.addCell(label11);
              sheet.addCell(label12);
             

              for(int i=1;i<=tSSRS.getMaxRow();i++){
            	  Label name = new  Label( 0 ,  i ,tSSRS.GetText(i, 1));
            	  Label contno = new  Label( 1 ,  i ,tSSRS.GetText(i, 2));
           	      Label caseno  =   new  Label( 2 ,  i ,tSSRS.GetText(i, 3));
                  Label risktype =   new  Label( 3 ,  i ,tSSRS.GetText(i, 4) );
                  
           	      Label riskname =  new   Label( 4 , i  ,tSSRS.GetText(i, 5));
           	      
                  Label appname =   new  Label( 5 ,  i ,tSSRS.GetText(i, 6) );
                  Label appidno =   new  Label( 6 ,  i ,tSSRS.GetText(i, 7) );
                  Label appphoneno  =   new  Label( 7 , i,tSSRS.GetText(i, 8) );
                  Label rgtdate  =   new  Label( 8 ,  i,tSSRS.GetText(i, 9) );
                  Label llrgtreason=   new  Label( 9 ,  i ,tSSRS.GetText(i, 10) );
                  Label customername  =   new  Label( 10 , i,tSSRS.GetText(i, 11) );
                  Label phoneno  =   new  Label( 11 ,  i,tSSRS.GetText(i, 12) );
                 

                  //System.out.println("到这里1" + customername);
                  
              //  将定义好的单元格添加到工作表中 
                  sheet.addCell(name);
                  sheet.addCell(contno);
                  sheet.addCell(caseno);
                  sheet.addCell(risktype);
                  
                  sheet.addCell(riskname);
                  
                  sheet.addCell(appname);
                  sheet.addCell(appidno);
                  sheet.addCell(appphoneno);
                  sheet.addCell(rgtdate);
                  sheet.addCell(llrgtreason);
                  sheet.addCell(customername);
                  sheet.addCell(phoneno);
                  
                 
                  

              }
       		
       	       mSQL  =  "select (select name from ldcom where comcode=a.mngcom) 地区,c.contno 保单号,a.caseno 案件号,(select case   when riskcode in  ('730101', '332401', '730201', '332501') then  "
     			        + "'分红'   else  case   when risktype = 'A' then  '意外'   when risktype = 'H' then  '健康'   else  '其他'  end end  from lmriskapp where riskcode = c.riskcode) 险种," 
       	    		    +"(select riskname from lmriskapp where riskcode=c.riskcode)险种名称,"
     			        + "b.RgtantName 申请人姓名,b.RgtantMobile 申请人手机号,a.customername 被保险人姓名,b.idno 被保险人身份证号,c.GiveTypeDesc 理赔结果,sum(c.realpay) 赔付金额,a.endcasedate 结案时间 ,(select  HospitalName from LLCaseCure where caseno=a.caseno fetch first rows  only) 出险医院   " 
     			        + "from llcase a,llregister b,llclaimpolicy c where a.rgtno=b.rgtno and a.caseno=c.caseno and a.rgtstate in('09','11','12') and (a.mngcom = '86410000' or a.mngcom like '864101%' or a.mngcom like '864113%') and "
      			 		+ "a.endcasedate  >= current date - "+a+" DAYS and a.endcasedate  < current date  group by a.mngcom,c.contno,a.caseno,c.riskcode,b.RgtantName,b.RgtantMobile, a.customername,b.idno,c.GiveTypeDesc,a.endcasedate with ur";

       			
               
                SSRS tSSRS1 = null;
                tSSRS1 = new ExeSQL().execSQL(mSQL);
                String ff= "否";
               	//在文档对象中创建第一个工作薄 并起名叫第一页
      			biaoqian="结案";
          		 sheet  =  book.createSheet( biaoqian ,  2 );
          	//操作行和列
          	//  以及单元格内容为test 参数1列 参数2行  参数3内容
          		label1  =   new  Label( 0 ,  0 ,  "地区" );
          		label2  =   new  Label( 1 ,  0 ,  "保单号" );
          		label3  =   new  Label( 2 ,  0 ,  "案件号" );
          		label4  =   new  Label( 3 ,   0 ,  "险种" );
          		
          		label5  =   new  Label(4  ,  0 ,  "险种名称");
          		
          		label6  =   new  Label( 5 ,  0 ,  "申请人姓名" );
          		label7  =   new  Label( 6 ,  0 ,  "申请人身份证号" );
          		label8  =   new  Label( 7 ,  0 ,  "被保险人姓名" );
          		label9  =   new  Label( 8 ,  0 ,  "被保险人身份证号" );
          		label10  =   new  Label( 9 ,  0 ,  "理赔结果" );
          		label11  =   new  Label( 10 ,  0 ,  "赔付金额" );
          		label12  =   new  Label( 11 ,  0 ,  "结案时间" );
                Label label13  =   new  Label( 12 ,  0 ,  "出险医院" );
                Label label14 =  new Label( 13 , 0 ,"重开案件" );

          //  将定义好的单元格添加到工作表中 
             sheet.addCell(label1);
             sheet.addCell(label2);
             sheet.addCell(label3);
             sheet.addCell(label4);
             sheet.addCell(label5);
             sheet.addCell(label6);
             sheet.addCell(label7);
             sheet.addCell(label8);
             sheet.addCell(label9);
             sheet.addCell(label10);
             sheet.addCell(label11);
             sheet.addCell(label12);
             sheet.addCell(label13);
             
             sheet.addCell(label14);
             
             System.out.println(tSSRS1.getMaxRow());
             for(int l=1;l<=tSSRS1.getMaxRow();l++){
          	     Label name =   new  Label( 0 ,  l ,tSSRS1.GetText(l, 1));
                 Label contno =   new  Label( 1 ,  l ,tSSRS1.GetText(l, 2) );
                 Label caseno =   new  Label( 2 ,  l ,tSSRS1.GetText(l, 3) );
                 Label risktype =   new  Label( 3 , l,tSSRS1.GetText(l, 4) );
                 Label riskname  =  new  Label( 4,  l  , tSSRS1.GetText(l, 5) );
                 Label rgtantname =   new  Label( 5 ,  l,tSSRS1.GetText(l, 6) );
                 Label rgtantmobile =   new  Label( 6 ,  l ,tSSRS1.GetText(l, 7) );
                 Label customername =   new  Label( 7 , l,tSSRS1.GetText(l, 8) );
                 Label idno  =   new  Label( 8 ,  l,tSSRS1.GetText(l, 9) );
                 Label giveTypeDesc  =   new  Label( 9 ,  l,tSSRS1.GetText(l, 10) );
                 Label realpay  =   new  Label( 10 ,  l,tSSRS1.GetText(l, 11) );
                 Label endcasedate  =   new  Label( 11 ,  l,tSSRS1.GetText(l, 12) );
                 Label hospital  =   new  Label( 12 ,  l,tSSRS1.GetText(l, 13) );
                 
                 Label rtcase = new Label ( 13,  l , ff );
                 
                 System.out.println("ZZZZZZZZZZZZZZZZZZZZZZ"+ff);

             //  将定义好的单元格添加到工作表中 
                 sheet.addCell(name);
                 sheet.addCell(contno);
                 sheet.addCell(caseno);
                 sheet.addCell(risktype);
                 sheet.addCell(riskname);
                 sheet.addCell(rgtantname);
                 sheet.addCell(rgtantmobile);
                 sheet.addCell(customername);
                 sheet.addCell(idno);
                 sheet.addCell(giveTypeDesc);
                 sheet.addCell(realpay);
                 sheet.addCell(endcasedate);
                 sheet.addCell(hospital);
                 
                 sheet.addCell(rtcase);

             }
             System.out.println("ZZZZZZZZZZZZZZZZZZZZZZZ");
             //(select riskname from lmriskapp where riskcode=lcp.riskcode) 险种名称,
             String lsql="";
             lsql="select contno 保单号,  trim(risktype2) 险种, riskname 险种名称,"
                        +" comname 地区,  appntname 投保人姓名,  idno 投保人证件号码,  "
     			 		+ "case  when appmobile is not null then  appmobile  else  appphone  end 投保人移动电话,  "
     			 		+ "insuname 被保人姓名,  insuidno 被保人证件号码,  "
     			 		+ "case  when insumobile is not null then  insumobile  else  insuphone  end 被保人移动电话,  "
     			 		+ "sum(amnt) 保额,  cvalidate 生效日期,  salechnl 销售渠道  "
     			 		+ "from (select lcc.contno contno, (select name from ldcom where comcode=lcc.ManageCom) comname , (select case   when riskcode in  ('730101', '332401', '730201', '332501') then  "
     			 		+ "'分红'   else  case   when risktype = 'A' then  '意外'   when risktype = 'H' then  '健康'   else  '其他'  end end  "
     			 		+ "from lmriskapp  where riskcode = lcp.riskcode) risktype2, "
     			 		+"(select riskname from lmriskapp where riskcode=lcp.riskcode) riskname,"
     			 		+" lcc.appntname appntname,  lcc.appntidno idno,  "
     			 		+ "(select mobile  from lcaddress  where customerno = lca.appntno  and addressno = lca.addressno) appmobile,  "
     			 		+ "(select phone  from lcaddress  where customerno = lca.appntno  and addressno = lca.addressno) appphone,  "
     			 		+ "lci.name insuname,  lci.idno insuidno,  (select mobile  from lcaddress  where customerno = lci.insuredno  and "
     			 		+ "addressno = lci.addressno) insumobile,  (select phone  from lcaddress  where customerno = lci.insuredno  and "
     			 		+ "addressno = lci.addressno) insuphone,  lcp.amnt amnt,  lcp.cvalidate cvalidate,  case  when lcc.salechnl in "
     			 		+ "('01', '10') then  '个险'  else  '银保'  end salechnl  from lccont lcc, lcpol lcp, lcappnt lca, lcinsured lci "
     			 		+ " where 1=1 and lcc.conttype = '1'  and (lcc.managecom = '86410000' or lcc.managecom like '864101%' or lcc.managecom like '864113%')  and lcc.appflag = '1'  "
     			 		+ "and lcc.cvalidate >= current date - "+a+"  DAYS  and lcc.cvalidate < current  date  and lcc.contno = lcp.contno  and "
     			 		+ "lcc.contno = lca.contno  and lcp.contno = lci.contno  and lcp.insuredno = lci.insuredno  and lcc.salechnl in "
     			 		+ "('01', '10', '04', '13')) temp  group by contno, comname, risktype2, riskname, appntname,  idno,  appmobile,  appphone,  insuname,  "
     			 		+ "insuidno,  insumobile,  insuphone,  cvalidate,  salechnl with ur ";

             SSRS tSSRS2 = null;
             tSSRS2 = new ExeSQL().execSQL(lsql);
                	//在文档对象中创建第一个工作薄 并起名叫第一页
       			biaoqian="承保";
           		 sheet  =  book.createSheet( biaoqian ,  3 );
           	//操作行和列
           	//  以及单元格内容为test 参数1列 参数2行  参数3内容
               label1  =   new  Label( 0 ,  0 ,  "保单号" );
               label2  =   new  Label( 1 ,  0 ,  "险种" );
               label3  =   new  Label( 2 ,  0 ,  "险种名称");
               label4  =   new  Label( 3 ,  0 ,  "地区" );
               label5  =   new  Label( 4 ,  0 , "投保人姓名"  );
               label6  =   new  Label( 5 ,  0 , "投保人证件号码"  );
               label7  =   new  Label( 6 ,  0 , "投保人移动电话"  );
               label8  =   new  Label( 7 ,  0 , "被保人姓名"  );
               label9  =   new  Label( 8 ,  0 , "被保人证件号码"  );
               label10  =   new  Label( 9 ,  0 , "被保人移动电话"  );
               label11  =   new  Label( 10 ,  0 , "保额"  );
               label12  =   new  Label( 11 ,  0 , "生效日期" );
               label13  =   new  Label( 12 , 0 , "销售渠道" );

           //  将定义好的单元格添加到工作表中 
              sheet.addCell(label1);
              sheet.addCell(label2);
              sheet.addCell(label3);
              sheet.addCell(label4);
              sheet.addCell(label5);
              sheet.addCell(label6);
              sheet.addCell(label7);
              sheet.addCell(label8);
              sheet.addCell(label9);
              sheet.addCell(label10);
              sheet.addCell(label11);
              sheet.addCell(label12);
              sheet.addCell(label13);

              for(int k=1;k<=tSSRS2.getMaxRow();k++){  
           	   Label caseno  =   new  Label( 0 ,  k ,tSSRS2.GetText(k, 1));  // 第二个参数的k，表示是第几行。如果用了k+1，那么结果就是从第二行开始了，第一行的结果会是空
                  Label risktype =   new  Label( 1 ,  k ,tSSRS2.GetText(k, 2) );
                  
                  Label riskname =  new Label (2 , k , tSSRS2.GetText(k, 3) );
                  
                  
                  Label comname =   new  Label( 3 ,  k ,tSSRS2.GetText(k, 4) );
                  Label appntname  =   new  Label( 4 , k,tSSRS2.GetText(k, 5) );
                  Label idno  =   new  Label( 5 ,  k,tSSRS2.GetText(k, 6) );
                  Label appphone =   new  Label( 6 ,  k ,tSSRS2.GetText(k, 7) );
                  Label insuname  =   new  Label( 7 , k,tSSRS2.GetText(k, 8) );
                  Label insuidno  =   new  Label( 8 ,  k,tSSRS2.GetText(k, 9) );
                  Label insuphone  =   new  Label( 9 ,  k,tSSRS2.GetText(k, 10) );
                  Label amnt  =   new  Label( 10 ,  k,tSSRS2.GetText(k, 11) );
                  Label cvalidate  =   new  Label( 11 ,  k,tSSRS2.GetText(k, 12) );
                  Label salechnl  =   new  Label( 12 ,  k,tSSRS2.GetText(k, 13) );

                  //System.out.println("到这里结束" + cvalidate);
              //  将定义好的单元格添加到工作表中 
                  sheet.addCell(caseno);
                  sheet.addCell(risktype);
                  
                  sheet.addCell(riskname);
                  
                  sheet.addCell(comname);
                  sheet.addCell(appntname);
                  sheet.addCell(idno);
                  sheet.addCell(appphone);
                  sheet.addCell(insuname);
                  sheet.addCell(insuidno);
                  sheet.addCell(insuphone);
                  sheet.addCell(amnt);
                  sheet.addCell(cvalidate);
                  sheet.addCell(salechnl);

              }
       	
       	book.write();//输出到本地
       	book.close();//关闭流
       	} catch (IOException e) {
       	// TODO Auto-generated catch block
       	e.printStackTrace();
       } catch (RowsExceededException e) {
       	// TODO Auto-generated catch block
       	e.printStackTrace();
       } catch (WriteException e) {
       	// TODO Auto-generated catch block
       	e.printStackTrace();
       }
        
}
    
    /**
     * 将给定目录下的文件打包为压缩包
     *
     */
    private void getZipFile()
    {
    	 
        FileOutputStream f = null;
        mZipFilePath = mURL+"InsuranceDataExtraction.zip";
    	System.out.println("压缩路径名：" + mZipFilePath);
        ZipOutputStream out = null;
        String tDocFileName = mURL + mZipDocFileName; //文件绝对路径 + 文件名
        try
        {
            f = new FileOutputStream(mZipFilePath);
            System.out.println("f：" + f);
        }
        catch (FileNotFoundException ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("无法创建zip文件");
            System.out.println(mErrors.getLastError());
            return ;
        }
        out = new ZipOutputStream(new BufferedOutputStream(f));
        System.out.println("out：" + out);
        mFlag = compressIntoZip(mZipFilePath, out, mZipDocFileName,tDocFileName);
		   
        try {
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		    mErrors.addOneError("流关闭异常");
		    System.out.println(mErrors.getLastError());
		    return ;
		} 	
    }
    
    /**
     * 添加到压缩文件
     * @param zipfilepath String
     * @param out ZipOutputStream
     * @param tZipDocFileName String：zip文件名
     * @param tDocFileName String：待压缩的文件路径名
     * @return int：1：成功、0：失败，但不中断整个程序、-1：失败，中断程序
     */
    private  int compressIntoZip(String zipfilepath, ZipOutputStream out,
            String tZipDocFileName, String tDocFileName)
    {
    System.out.println("zipfilepath:" + zipfilepath);
    System.out.println("out:" + out);
    System.out.println("tZipDocFileName:" + tZipDocFileName);
    System.out.println("tDocFileName待压缩文件压缩路径:" + tDocFileName);

        try
        {
            FileInputStream fi = new FileInputStream(tDocFileName);
            BufferedInputStream origin = new BufferedInputStream(fi, BUFFER);

            ZipEntry entry = new ZipEntry(tZipDocFileName);
            out.putNextEntry(entry);

            int count;
            byte data[] = new byte[BUFFER];
            while ((count = origin.read(data, 0, BUFFER)) != -1)
            {
                out.write(data, 0, count);
            }
            origin.close();
        }
        catch (FileNotFoundException ex)
        {
            ex.printStackTrace();
            this.mErrors.addOneError("找不到待压缩文件压缩路径" + tDocFileName);
            System.out.println("找不到待压缩文件压缩路径" + tDocFileName);

            //某些页找不到可不用单独处理
            return 0;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "BPOSentScanBL";
            tError.functionName = "dealData";
            tError.errorMessage = "无法生成压缩包";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            deleteFile(zipfilepath);
            return -1;
        }
        return 1;
    }
    
    /**
     * 删除生成的ZIP文件和XML文件
     * @param cFilePath String：完整文件名
     */
    private boolean deleteFile(String cFilePath)
    {
        File f = new File(cFilePath);
        if (!f.exists())
        {
            CError tError = new CError();
            tError.moduleName = "MonthDataCatch";
            tError.functionName = "deleteFile";
            tError.errorMessage = "没有找到需要删除的文件";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        f.delete();
        return true;
    }

}
