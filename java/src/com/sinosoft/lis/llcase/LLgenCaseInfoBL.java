package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: ClientRegisterBL </p>
 * <p>Description: ClientRegisterBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author:Xx
 * @CreateDate：2005-02-18 13:58:55
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Date;

public class LLgenCaseInfoBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private FDate fDate = new FDate();

    /** 业务处理相关变量 */
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private LLSubReportSchema mLLSubReportSchema = new LLSubReportSchema();
    private LLCaseRelaSchema mLLCaseRelaSchema = new LLCaseRelaSchema();
    private LLFeeMainSchema mLLFeeMainSchema = new LLFeeMainSchema();
    private LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
    private LLSecurityReceiptSchema mLLSecurityReceiptSchema = new
            LLSecurityReceiptSchema();
    private LLCaseCureSchema mLLCaseCureShema = new LLCaseCureSchema();
    private LLAppClaimReasonSchema mLLAppClaimReasonSchema = new
            LLAppClaimReasonSchema();
    private LLToClaimDutySet mLLToClaimDutySet = new LLToClaimDutySet();
    private LLCaseReceiptSchema mLLCaseReceiptShema = new LLCaseReceiptSchema();
    private LLCaseImportSet mLLCaseImportSet = new LLCaseImportSet();
    private LLCasePolicySet mLLCasePolicySet = new LLCasePolicySet();
    private LLCaseImportSchema mLLCaseImportSchema = new LLCaseImportSchema();
    private String aCaseNo = "";
    private String aRgtNo = "";
    private String aSubRptNo = "";
    private String aCaseRelaNo = "";
    private String aCureNo = "";
    private String aMainFeeNo = "";
    private String aDate = "";
    private String aTime = "";
    private String oDate = "";
    private String oTime = "";
    private String aGrpContNo="";
    private String aCustomerNo = "";
    private String aIDNo = "";
    private String aSecurityNo = "";
    private String tSex="";

    public LLgenCaseInfoBL() {
    }

    public static void main(String[] args) {

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        aRgtNo = mLLRegisterSchema.getRgtNo();
        tLLRegisterDB.setRgtNo(aRgtNo);
        if(!tLLRegisterDB.getInfo()){
            return false;
        }

        aGrpContNo=tLLRegisterDB.getRgtObjNo();
        String strSQL0 =
                "select distinct insuredno,name,sex,birthday,idno,othidno ";
        String sqlpart1 = "from lcinsured a where ";
        String sqlpart2 = "from lbinsured a where ";
        String wherePart = "a.GrpContNo = '" + aGrpContNo + "' ";
        LLCaseImportDB tLLCaseImportDB = new LLCaseImportDB();
        tLLCaseImportDB.setRgtNo(aRgtNo);
        tLLCaseImportDB.setImportState("01");
        mLLCaseImportSet=tLLCaseImportDB.query();
        if(mLLCaseImportSet.size()<1){
            return false;
        }else{
            for(int i=1;i<=mLLCaseImportSet.size();i++){
                map = new MMap();
                mLLCaseImportSchema=mLLCaseImportSet.get(i);
                String wherePart1="";
                if(mLLCaseImportSchema.getIDNo()!=null){
                    String tIDNo=mLLCaseImportSchema.getIDNo();
                    wherePart1 = " and a.idno='" + tIDNo + "'";
                }

                String strSQL = strSQL0 + sqlpart1 + wherePart +wherePart1+ " union " + strSQL0 +
                        sqlpart2 + wherePart+wherePart1;
        System.out.println(strSQL);

        ExeSQL texesql = new ExeSQL();
        SSRS tssrs = new SSRS();
        tssrs = texesql.execSQL(strSQL);
        System.out.println(strSQL);
        aCustomerNo = tssrs.GetText(1, 1);
        aIDNo = tssrs.GetText(1, 5);
        aSecurityNo = tssrs.GetText(1, 6);
        tSex = tssrs.GetText(1, 3).equals("null") ? "" :
               tssrs.GetText(1, 3);
        String tBirthday = tssrs.GetText(1, 4).equals("null") ? "" :
                           tssrs.GetText(1, 4);

                if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLgenCaseInfoBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LLgenCaseInfoBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("Start ClientRegisterBL Submit...");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDDrugBL";
            tError.functionName = "ClientRegisterBL";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
            }
        }

        //进行业务处理

        System.out.println("End SimpleClaimBL Submit...");
        mInputData = null;

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        if (mGlobalInput.ManageCom.length() == 2) {
            mGlobalInput.ManageCom = mGlobalInput.ManageCom + "000000";
        }
        if (mGlobalInput.ManageCom.length() == 4) {
            mGlobalInput.ManageCom = mGlobalInput.ManageCom + "0000";
        }
        mLLRegisterSchema = (LLRegisterSchema) cInputData.getObjectByObjectName(
                    "LLRegisterSchema", 0);
        aDate = PubFun.getCurrentDate();
        aTime = PubFun.getCurrentTime();
        oDate = PubFun.getCurrentDate();
        oTime = PubFun.getCurrentTime();
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("---Star dealData---mOperate---" + mOperate);
//        if (this.mOperate.equals("INSERT||MAIN")) {

        if (!insertData()) {
                return false;
            }

        if (!fillCasePolicy()) {
            CError.buildErr(this, "没有符合条件的保单！");
            return false;
        }
//        if (mLLToClaimDutySet.size() <= 0) {
//            CError.buildErr(this, "没有符合条件的责任！");
//            return false;
//        }


        return true;
    }

    private boolean insertData() {

        /*生成理赔号*/
        System.out.println("---Star Insert case---");
        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        VData tno = new VData();
        tno.add(0, "1");
        tno.add(1, aRgtNo);
        tno.add(2, "STORE");
        aCaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit, tno);
        if (aCaseNo.length() < 17) {
            aCaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit);
        }
        aSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);
        aCaseRelaNo = PubFun1.CreateMaxNo("CASERELANO", tLimit);
        aMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO", tLimit);

        if (!getCaseInfo()) {
            return false;
        }

        mLLCaseOpTimeSchema.setCaseNo(aCaseNo);
        mLLCaseOpTimeSchema.setOperator(mGlobalInput.Operator);
        mLLCaseOpTimeSchema.setManageCom(mGlobalInput.ManageCom);
//        LLCaseCommon tLLCaseCommon = new LLCaseCommon();
//        LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(
//                mLLCaseOpTimeSchema);
//        map.put(tLLCaseOpTimeSchema, "INSERT");
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */


    private boolean getCaseInfo() {
        int CustAge = 0;
        try {
            CustAge = calAge();
        } catch (Exception ex) {
            System.out.println("计算年龄错误");
        }
        mLLCaseSchema.setCustomerNo(aCustomerNo);
        mLLCaseSchema.setRgtType("1");
        mLLCaseSchema.setIDNo(aIDNo);
        mLLCaseSchema.setRgtState("03");
        mLLCaseSchema.setCustomerName(mLLCaseImportSchema.getCustomerName());
        mLLCaseSchema.setCustomerAge(CustAge);
        mLLCaseSchema.setRgtNo(aRgtNo);
        mLLCaseSchema.setCaseNo(aCaseNo);
        mLLCaseSchema.setAccidentDate(mLLCaseImportSchema.getAccDate());
        mLLCaseSchema.setHandler(mGlobalInput.Operator);
        mLLCaseSchema.setDealer(mGlobalInput.Operator);
        mLLCaseSchema.setRgtDate(PubFun.getCurrentDate());
        mLLCaseSchema.setOperator(mGlobalInput.Operator);
        mLLCaseSchema.setMngCom(mGlobalInput.ManageCom);
        mLLCaseSchema.setMakeDate(aDate);
        mLLCaseSchema.setMakeTime(aTime);
        mLLCaseSchema.setModifyDate(oDate);
        mLLCaseSchema.setModifyTime(oTime);

        if (!genSubReport()) {
            CError.buildErr(this, "生成事件失败！");
            return false;
        }

        String aFeeType = mLLCaseImportSchema.getFeeType();
        if (aFeeType.trim().equals("1")) {
            mLLAppClaimReasonSchema.setReasonCode("01");
            mLLAppClaimReasonSchema.setReason("门诊"); //申请原因
        } else if(aFeeType.trim().equals("2")){
            mLLAppClaimReasonSchema.setReasonCode("02"); //原因代码
            mLLAppClaimReasonSchema.setReason("住院"); //申请原因
        }else if(aFeeType.trim().equals("3")){
            mLLAppClaimReasonSchema.setReasonCode("03"); //原因代码
            mLLAppClaimReasonSchema.setReason("特种病"); //申请原因
        }else{
            return false;

        }
        mLLAppClaimReasonSchema.setCustomerNo(aCustomerNo);
        mLLAppClaimReasonSchema.setReasonType("0");
        mLLAppClaimReasonSchema.setRgtNo(aRgtNo);
        mLLAppClaimReasonSchema.setCaseNo(aCaseNo);
        mLLAppClaimReasonSchema.setOperator(mGlobalInput.Operator);
        mLLAppClaimReasonSchema.setMngCom(mGlobalInput.ManageCom);
        mLLAppClaimReasonSchema.setMakeDate(aDate);
        mLLAppClaimReasonSchema.setMakeTime(aTime);
        mLLAppClaimReasonSchema.setModifyDate(oDate);
        mLLAppClaimReasonSchema.setModifyTime(oTime);

        mLLFeeMainSchema.setAge(CustAge);
        mLLFeeMainSchema.setReceiptNo(mLLCaseImportSchema.getReceiptNo());
        mLLFeeMainSchema.setMainFeeNo(aMainFeeNo);
        mLLFeeMainSchema.setCustomerName(mLLCaseSchema.getCustomerName());
        mLLFeeMainSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
        mLLFeeMainSchema.setRgtNo(aRgtNo);
        mLLFeeMainSchema.setCaseNo(aCaseNo);
        mLLFeeMainSchema.setFeeType(mLLCaseImportSchema.getFeeType());
        mLLFeeMainSchema.setRefuseAmnt(mLLCaseImportSchema.getSelfPay2());
        mLLFeeMainSchema.setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());
        mLLFeeMainSchema.setOperator(mGlobalInput.Operator);
        mLLFeeMainSchema.setMngCom(mGlobalInput.ManageCom);
        mLLFeeMainSchema.setMakeDate(aDate);
        mLLFeeMainSchema.setMakeTime(aTime);
        mLLFeeMainSchema.setModifyDate(oDate);
        mLLFeeMainSchema.setModifyTime(oTime);




        if (!calSecu()) {
            CError.buildErr(this, "生成社保帐单失败！");
            return false;
        }
        mLLSecurityReceiptSchema.setMidAmnt(mLLCaseImportSchema.getMidAmnt());
        mLLSecurityReceiptSchema.setLowAmnt(mLLCaseImportSchema.
                                            getGetLimit());
        mLLSecurityReceiptSchema.setHighAmnt2(mLLCaseImportSchema.
                                              getSupInHosFee());
        mLLSecurityReceiptSchema.setMainFeeNo(aMainFeeNo);
        mLLSecurityReceiptSchema.setFeeDetailNo(aMainFeeNo);
        mLLSecurityReceiptSchema.setRgtNo(aRgtNo);
        mLLSecurityReceiptSchema.setCaseNo(aCaseNo);
        mLLSecurityReceiptSchema.setOperator(mGlobalInput.Operator);
        mLLSecurityReceiptSchema.setMngCom(mGlobalInput.ManageCom);
        mLLSecurityReceiptSchema.setMakeDate(aDate);
        mLLSecurityReceiptSchema.setMakeTime(aTime);
        mLLSecurityReceiptSchema.setModifyDate(oDate);
        mLLSecurityReceiptSchema.setModifyTime(oTime);
        mLLSecurityReceiptSchema.setApplyAmnt(mLLCaseImportSchema.getApplyAmnt());
        mLLSecurityReceiptSchema.setSupInHosFee(mLLCaseImportSchema.getSupInHosFee());
        mLLSecurityReceiptSchema.setSelfPay1(mLLCaseImportSchema.getSelfPay1());
        mLLSecurityReceiptSchema.setSelfPay2(mLLCaseImportSchema.getSelfPay2());
        mLLSecurityReceiptSchema.setSelfAmnt(mLLCaseImportSchema.getSelfAmnt());
        mLLSecurityReceiptSchema.setGetLimit(mLLCaseImportSchema.getGetLimit());
        mLLSecurityReceiptSchema.setPlanFee(mLLCaseImportSchema.getPlanFee());
        mLLSecurityReceiptSchema.setHighAmnt1(mLLCaseImportSchema.getHighAmnt1());
        if (aFeeType.trim().equals("1")) {
            mLLSecurityReceiptSchema.setHighDoorAmnt(mLLCaseImportSchema.
                    getMidAmnt());
            mLLSecurityReceiptSchema.setMidAmnt(0);
            mLLSecurityReceiptSchema.setSmallDoorPay(mLLCaseImportSchema.
                    getGetLimit());
            mLLSecurityReceiptSchema.setGetLimit(0);

        }

        mLLSecurityReceiptSchema.setLowAmnt(mLLCaseImportSchema.
                                            getGetLimit());
        mLLSecurityReceiptSchema.setAccountPay(mLLCaseImportSchema.getAccountPay());
        mLLSecurityReceiptSchema.setHighAmnt2(mLLCaseImportSchema.
                                              getSupInHosFee());
            String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
            aCureNo = PubFun1.CreateMaxNo("CURENO", tLimit);
            mLLCaseCureShema.setSerialNo(aCureNo);
            mLLCaseCureShema.setCustomerNo(mLLCaseSchema.getCustomerNo());
            mLLCaseCureShema.setCustomerName(mLLCaseSchema.getCustomerName());
            mLLCaseCureShema.setDiagnose(mLLCaseCureShema.
                                              getDiseaseName());
            mLLCaseCureShema.setHospitalCode(mLLFeeMainSchema.
                                                  getHospitalCode());
            mLLCaseCureShema.setHospitalName(mLLFeeMainSchema.
                                                  getHospitalName());
            mLLCaseCureShema.setCaseNo(aCaseNo);
            mLLCaseCureShema.setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());
            mLLCaseCureShema.setOperator(mGlobalInput.Operator);
            mLLCaseCureShema.setMngCom(mGlobalInput.ManageCom);
            mLLCaseCureShema.setMakeDate(aDate);
            mLLCaseCureShema.setMakeTime(aTime);
            mLLCaseCureShema.setModifyDate(oDate);
            mLLCaseCureShema.setModifyTime(oTime);
            String tDiseaseCode = mLLCaseImportSchema.getDiseaseCode();
            String tDiseaseName = mLLCaseImportSchema.getDiseaseName();
            System.out.println(tDiseaseCode);
            System.out.println(tDiseaseName);
            if(tDiseaseCode!=null&&tDiseaseCode!=null){
                if (!tDiseaseCode.equals("") || !tDiseaseName.equals("")) {
                    mLLCaseCureShema.setDiseaseCode(tDiseaseCode);
                    mLLCaseCureShema.setDiseaseName(tDiseaseName);
                    mLLCaseCureShema.setMainDiseaseFlag("1");
                }
            }

            //插入明细表，以便理算普通的住院或者门诊险时，可以处理。
            String tLimit1 = PubFun.getNoLimit("86");
            mLLCaseReceiptShema.setFeeDetailNo(
                    PubFun1.CreateMaxNo("FEEDETAILNO", tLimit1));
            mLLCaseReceiptShema.setMainFeeNo(aMainFeeNo);
            mLLCaseReceiptShema.setRgtNo(aRgtNo);
            mLLCaseReceiptShema.setCaseNo(aCaseNo);
            mLLCaseReceiptShema.setFeeItemCode("201");
            mLLCaseReceiptShema.setFeeItemName("西药费");
            mLLCaseReceiptShema.setFee(mLLFeeMainSchema.getSumFee());
            mLLCaseReceiptShema.setAvaliFlag("0");
            mLLCaseReceiptShema.setMngCom(mGlobalInput.ManageCom);
            mLLCaseReceiptShema.setOperator(mGlobalInput.Operator);
            mLLCaseReceiptShema.setMakeDate(aDate);
            mLLCaseReceiptShema.setMakeTime(aTime);
            mLLCaseReceiptShema.setModifyDate(aDate);
            mLLCaseReceiptShema.setModifyTime(aTime);
            mLLCaseReceiptShema.setSelfAmnt(mLLSecurityReceiptSchema.getSelfAmnt());
            mLLCaseReceiptShema.setRefuseAmnt(mLLSecurityReceiptSchema.getSelfPay2());

            mLLCaseImportSchema.setImportState("02");
            map.put(mLLCaseImportSchema, "UPDATE");
            map.put(mLLCaseSchema, "DELETE&INSERT");
            map.put(mLLFeeMainSchema, "DELETE&INSERT");
            map.put(mLLSecurityReceiptSchema, "DELETE&INSERT");
            map.put(mLLCaseCureShema, "DELETE&INSERT");
            map.put(mLLAppClaimReasonSchema, "DELETE&INSERT");
            map.put(mLLCaseReceiptShema, "DELETE&INSERT");

        return true;
    }

    /**
     * 生成事件及与案件的关联
     * @return boolean
     */
    private boolean genSubReport() {
        //立案分案,即事件
        mLLSubReportSchema.setSubRptNo(aSubRptNo);
        mLLSubReportSchema.setCustomerName(mLLCaseSchema.getCustomerName());
        mLLSubReportSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
        mLLSubReportSchema.setAccDate(mLLCaseSchema.getAccidentDate());
//        if(mLLFeeMainSchema.getHospStartDate()!=null)
        mLLSubReportSchema.setInHospitalDate(mLLFeeMainSchema.getHospStartDate());
//        if(mLLFeeMainSchema.getHospEndDate()!=null)
        mLLSubReportSchema.setOutHospitalDate(mLLFeeMainSchema.getHospEndDate());
        mLLSubReportSchema.setAccDesc(mLLCaseCureShema.getDiseaseName());
        mLLSubReportSchema.setOperator(mGlobalInput.Operator);
        mLLSubReportSchema.setMngCom(mGlobalInput.ManageCom);
        mLLSubReportSchema.setMakeDate(PubFun.getCurrentDate());
        mLLSubReportSchema.setMakeTime(PubFun.getCurrentTime());
        mLLSubReportSchema.setModifyDate(PubFun.getCurrentDate());
        mLLSubReportSchema.setModifyTime(PubFun.getCurrentTime());

        mLLCaseRelaSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        mLLCaseRelaSchema.setSubRptNo(aSubRptNo);
        mLLCaseRelaSchema.setCaseRelaNo(aCaseRelaNo);
        map.put(mLLSubReportSchema, "DELETE&INSERT");
        map.put(mLLCaseRelaSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 校验计算社保信息
     * @return boolean
     */
    private boolean calSecu() {
        mLLFeeMainSchema.setRgtNo(aRgtNo);
        mLLFeeMainSchema.setCaseNo(aCaseNo);
        mLLFeeMainSchema.setCustomerNo(aCustomerNo);
        mLLFeeMainSchema.setOriginFlag(mLLCaseImportSchema.getOriginFlag());
        mLLFeeMainSchema.setCustomerName(mLLCaseImportSchema.getCustomerName());
        mLLFeeMainSchema.setCustomerSex(tSex);
        mLLFeeMainSchema.setRealHospDate(mLLCaseImportSchema.getRealHospDate());
        mLLFeeMainSchema.setFeeDate(mLLCaseImportSchema.getFeeDate());
        mLLFeeMainSchema.setHospStartDate(mLLCaseImportSchema.getHospStartDate());
        mLLFeeMainSchema.setHospitalCode(mLLCaseImportSchema.getHospitalCode());
        mLLFeeMainSchema.setFeeAtti("1");
        mLLFeeMainSchema.setOperator(mLLCaseImportSchema.getOperator());
        mLLFeeMainSchema.setMngCom(mLLCaseImportSchema.getManageCom());
        mLLFeeMainSchema.setMakeDate(PubFun.getCurrentDate());
        mLLFeeMainSchema.setMakeTime(aTime);
        mLLFeeMainSchema.setSecurityNo(aSecurityNo);
        mLLFeeMainSchema.setModifyDate(PubFun.getCurrentDate());
        mLLFeeMainSchema.setModifyTime(aTime);
        mLLFeeMainSchema.setCaseRelaNo(aCaseRelaNo);
        mLLFeeMainSchema.setHospEndDate(mLLCaseImportSchema.getHospEndDate());
        mLLFeeMainSchema.setAffixNo(String.valueOf(mLLCaseImportSchema.getAffixNo()));
        mLLFeeMainSchema.setSumFee(mLLCaseImportSchema.getApplyAmnt());
        LCGrpContSchema kLCGrpContSchema = new LCGrpContSchema();
        LCGrpContDB kLCGrpContDB = new LCGrpContDB();
        kLCGrpContDB.setGrpContNo(aGrpContNo);
        kLCGrpContDB.getInfo();
        kLCGrpContSchema = kLCGrpContDB.getSchema();

        String tMngCom = kLCGrpContSchema.getManageCom();
        System.out.println(tMngCom.substring(0, 4));





        System.out.println(tMngCom.substring(0, 4));
        if (mLLCaseImportSchema.getFeeType().trim().equals("1")) {
            mLLSecurityReceiptSchema.setHighDoorAmnt(mLLCaseImportSchema.
                    getMidAmnt());
            mLLSecurityReceiptSchema.setMidAmnt(0);
            mLLSecurityReceiptSchema.setSmallDoorPay(mLLCaseImportSchema.
                    getGetLimit());
            mLLSecurityReceiptSchema.setGetLimit(0);

        }
        mLLSecurityReceiptSchema.setRgtNo(aRgtNo);
        mLLSecurityReceiptSchema.setCaseNo(aCaseNo);

        mLLSecurityReceiptSchema.setLowAmnt(mLLCaseImportSchema.
                                            getGetLimit());
        mLLSecurityReceiptSchema.setHighAmnt2(mLLCaseImportSchema.
                                              getSupInHosFee());

        double aSelfPay1 = mLLSecurityReceiptSchema.getGetLimit()
                           + mLLSecurityReceiptSchema.getMidAmnt()
                           + mLLSecurityReceiptSchema.getHighAmnt1();
        mLLSecurityReceiptSchema.setSelfPay1(aSelfPay1);
        return true;
    }

    /**
     * 过滤要赔付的责任
     * @return boolean
     */
    private boolean getClaimDuty(LCPolSchema tpolschema) {
        String aCaseNo = mLLCaseSchema.getCaseNo();
        String aCaseRelaNo = mLLCaseRelaSchema.getCaseRelaNo();
        String aSubRptNo = mLLSubReportSchema.getSubRptNo();
        SynLCLBGetBL aSynLCLBGetBL = new SynLCLBGetBL();
        aSynLCLBGetBL.setPolNo(tpolschema.getPolNo());

        aSynLCLBGetBL.setGrpContNo(mLLRegisterSchema.getRgtObjNo());
        LCGetSet tLCGetSet = aSynLCLBGetBL.query();
        LLToClaimDutySet tLLToClaimDutySet = new LLToClaimDutySet();
        for (int i = 1; i <= tLCGetSet.size(); i++) {
            LLToClaimDutySchema row = new LLToClaimDutySchema();
            LCGetSchema lcGet = tLCGetSet.get(i);
            row.setCaseNo(aCaseNo);
            row.setCaseRelaNo(aCaseRelaNo);
            row.setSubRptNo(aSubRptNo);
            row.setPolNo(lcGet.getPolNo()); /*保单号*/
            row.setGetDutyCode(lcGet.getGetDutyCode()); /* 给付责任编码 */
            row.setDutyCode(lcGet.getDutyCode());
            row.setGetDutyKind("100");
            System.out.println(mLLCaseSchema.getMngCom());
            if (mLLCaseSchema.getMngCom().substring(0, 4).trim().equals("8612")) {
                System.out.println(mLLSecurityReceiptSchema.getMidAmnt());
                System.out.println(mLLSecurityReceiptSchema.getHighDoorAmnt());
                if ((mLLSecurityReceiptSchema.getMidAmnt() == 0) &&
                    (mLLSecurityReceiptSchema.getHighAmnt1() == 0)) {
                    row.setGetDutyKind("200");
                }
            }
            System.out.println(mLLFeeMainSchema.getFeeType());
            System.out.println(mLLFeeMainSchema.getOriginFlag());

                if (mLLCaseImportSchema.getFeeType().trim().equals("1")) {
                    row.setGetDutyKind("200");
                }

            //OriginFlag标志表示批次录入时，该项是否是特需险，1位是。
            if(mLLFeeMainSchema.getOriginFlag()!=null){
                if (mLLFeeMainSchema.getOriginFlag() != null) {
                    if (mLLFeeMainSchema.getOriginFlag().trim().equals("1")) {
                        row.setGetDutyKind("000");
                    }
                }
            }
            /* 给付责任类型 */
            row.setGrpContNo(lcGet.getGrpContNo()); /* 集体合同号 */
            row.setGrpPolNo(tpolschema.getGrpContNo()); /* 集体保单号 */
            row.setContNo(lcGet.getContNo()); /* 个单合同号 */
            row.setKindCode(tpolschema.getKindCode()); /* 险类代码 */
            row.setRiskCode(tpolschema.getRiskCode()); /* 险种代码 */
            row.setRiskVer(tpolschema.getRiskVersion()); /* 险种版本号 */
            row.setPolMngCom(tpolschema.getManageCom()); /* 保单管理机构 */
            row.setClaimCount(0); /* 理算次数 */
            tLLToClaimDutySet.add(row);
        }
        map.put(tLLToClaimDutySet, "DELETE&INSERT");
        return true;
    }

    /**
     * 查询保单信息包括C表和B表的信息
     * 以后要考虑保单状态表里的信息
     * @param string String
     * @return LCPolSchema
     */
    private LCPolSchema getLCPol(String polNo) {
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setPolNo(polNo);
        if (!tLCPolBL.getInfo()) {
            mErrors.addOneError("险种保单LCPol查询失败,请契约运维核实保单数据是否有误!");
            return null;
        } else {
            return tLCPolBL.getSchema();
        }
    }

    /**
     * 案件保单信息
     * @return boolean
     */
    private boolean fillCasePolicy() {
        String aCaseNo = mLLCaseSchema.getCaseNo();
        String aCaseRelaNo = mLLCaseRelaSchema.getCaseRelaNo();
        String aRgtNo = mLLCaseSchema.getRgtNo();
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setInsuredNo(mLLCaseSchema.getCustomerNo());
        tLCPolBL.setGrpContNo(aGrpContNo);
        LCPolSet tLCPolSet = new LCPolSet();
        tLCPolSet = tLCPolBL.query();
        int n = tLCPolSet.size();
        String tdate = PubFun.getCurrentDate();
        String ttime = PubFun.getCurrentTime();
        String strAccidentDate = mLLCaseSchema.getAccidentDate();
        if (strAccidentDate == null || strAccidentDate.equals("")) {
            strAccidentDate = mLLFeeMainSchema.getFeeDate();
        }
        try {
            LLCasePolicySet tLLCasePolicySet = new LLCasePolicySet();
            for (int i = 1; i <= n; i++) {
                //处理保单状态
                String opolstate = "" + tLCPolSet.get(i).getPolState();
                String npolstate = "";
                if (opolstate.equals("null") || opolstate.equals("")) {
                    npolstate = "0400";
                } else {
                    if (opolstate.substring(0, 2).equals("04")) {
                        npolstate = "0400" + opolstate.substring(4);
                    } else {
                        if (opolstate.length() < 4) {
                            npolstate = "0400" + opolstate;
                        } else {
                            npolstate = "0400" + opolstate.substring(0, 4);
                        }
                    }
                }
                tLCPolSet.get(i).setPolState(npolstate);

                LLCasePolicySchema tLLCasePolicySchema = new LLCasePolicySchema();
                LCPolSchema tLCPolSchema = tLCPolSet.get(i);
                if (!(strAccidentDate == null || strAccidentDate.equals(""))) {
                    if (!(CaseFunPub.checkDate(tLCPolSchema.getCValiDate(),
                                               strAccidentDate))) {
                        this.mErrors.copyAllErrors(tLCPolBL.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "LLgenCaseInfoBL";
                        tError.functionName = "fillCasePolicy";
                        tError.errorMessage = "保单号码是" + tLCPolSchema.getPolNo() +
                                              "的出险日期" + strAccidentDate
                                              + "在保单生效日期" +
                                              tLCPolSchema.getCValiDate()
                                              + "之前，不能参加理赔！！！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
                tLLCasePolicySchema.setCaseNo(aCaseNo);
                tLLCasePolicySchema.setRgtNo(aRgtNo);
                tLLCasePolicySchema.setCaseRelaNo(aCaseRelaNo);
                tLLCasePolicySchema.setOperator(this.mGlobalInput.Operator);
                tLLCasePolicySchema.setMngCom(this.mGlobalInput.ManageCom);
                tLLCasePolicySchema.setMakeDate(tdate);
                tLLCasePolicySchema.setModifyDate(tdate);
                tLLCasePolicySchema.setMakeTime(ttime);
                tLLCasePolicySchema.setModifyTime(ttime);

                if ((tLCPolSchema.getInsuredNo().equals(mLLCaseSchema.
                        getCustomerNo()))) {
                    tLLCasePolicySchema.setCasePolType("0");
                } else {
                    if (tLCPolSchema.getAppntNo().equals(mLLCaseSchema.
                            getCustomerNo())) {
                        tLLCasePolicySchema.setCasePolType("1");
                    }
                }
                tLLCasePolicySchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                tLLCasePolicySchema.setContNo(tLCPolSchema.getContNo());
                tLLCasePolicySchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                tLLCasePolicySchema.setPolNo(tLCPolSchema.getPolNo());
                tLLCasePolicySchema.setKindCode(tLCPolSchema.getKindCode());
                tLLCasePolicySchema.setRiskCode(tLCPolSchema.getRiskCode());
                tLLCasePolicySchema.setRiskVer(tLCPolSchema.getRiskVersion());
                tLLCasePolicySchema.setPolMngCom(tLCPolSchema.getManageCom());
                tLLCasePolicySchema.setSaleChnl(tLCPolSchema.getSaleChnl());
                tLLCasePolicySchema.setAgentCode(tLCPolSchema.getAgentCode());
                tLLCasePolicySchema.setAgentGroup(tLCPolSchema.getAgentGroup());
                tLLCasePolicySchema.setInsuredNo(tLCPolSchema.getInsuredNo());
                tLLCasePolicySchema.setInsuredName(tLCPolSchema.getInsuredName());
                tLLCasePolicySchema.setInsuredSex(tLCPolSchema.getInsuredSex());
                tLLCasePolicySchema.setInsuredBirthday(tLCPolSchema.
                        getInsuredBirthday());
                tLLCasePolicySchema.setAppntNo(tLCPolSchema.getAppntNo());
                tLLCasePolicySchema.setAppntName(tLCPolSchema.getAppntName());
                tLLCasePolicySchema.setCValiDate(tLCPolSchema.getCValiDate());

                tLLCasePolicySchema.setPolType("1"); //正式保单
                tLLCasePolicySet.add(tLLCasePolicySchema);
                getClaimDuty(tLCPolSchema);
            }
            String delSql = "delete from llcasepolicy where caseno='" +
                            aCaseNo + "'";
            System.out.println(delSql);
            map.put(delSql, "DELETE");
            map.put(tLLCasePolicySet, "DELETE&INSERT");
            map.put(tLCPolSet, "UPDATE");
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            CError.buildErr(this, "准备数据出错:" + ex.getMessage());
            return false;
        }
    }

    /**
     * 计算年龄的函数
     * @return int
     */
    private int calAge() {
        Date Birthday = fDate.getDate(mLLCaseSchema.getCustBirthday());
        String strNow = PubFun.getCurrentDate();
        Date aNow = fDate.getDate(strNow);
        int CustomerAge = PubFun.calInterval(Birthday, aNow, "Y");
        System.out.println("年龄计算函数。。。" + CustomerAge);
        return CustomerAge;
    }

    /**
     * 查询功能
     * @return boolean
     */
    private boolean submitquery() {
        return false;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLLRegisterSchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLLCaseSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLgenCaseInfoBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
