/*
 * @(#)ICaseCureBL.java	2005-02-20
 *
 * Copyright 2005 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/*
 * <p>Title: Web业务系统</p>
 * <p>ClassName:ICaseCureBL </p>
 * <p>Description: 理赔案件-药品录入 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft </p>
 * @author：Xx
 * @version：1.0
 * @CreateDate：2006-01-20
 */
public class LLCaseChargeDetailBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private MMap map = new MMap();
    private String mOperate = "";

    //全局数据
    private GlobalInput mGlobalInput = new GlobalInput();
    //账单信息
    //账单费用明细
    LLCaseChargeDetailSet mLLCaseChargeDetailSet = new LLCaseChargeDetailSet();
    LLFeeMainSchema mLLFeeMainSchema = new LLFeeMainSchema();

    private String mCaseNo = "";
    //private String mRgtNo = "";
    private String mMainFeeNo = "";

    public LLCaseChargeDetailBL()
    {
    }

    public static void main(String[] args)
    {

    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData 输入数据
     * @param cOperate String 操作类型
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        //得到输入数据
        if (!getInputData())
        {
            return false;
        }
//          mOperate = cOperate;
        //检查数据合法性
        if (!checkInputData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData(cOperate))
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLCaseChargeDetailBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有数据对象
     * @return boolean
     */
    private boolean getInputData()
    {
        System.out.println("getInputData()..."+mOperate);

        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLLCaseChargeDetailSet = (LLCaseChargeDetailSet) mInputData.getObjectByObjectName(
                "LLCaseChargeDetailSet", 0);
        mLLFeeMainSchema = (LLFeeMainSchema) mInputData.getObjectByObjectName(
                "LLFeeMainSchema", 0);
//        if(mCaseDrugSet.size()<=0){
//            mErrors.copyAllErrors(mCaseDrugSet.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "ICaseCureBL";
//            tError.functionName = "getInputData";
//            tError.errorMessage = "您未录入有效的药品信息!" ;
//            mErrors.addOneError(tError);
 //           return false;
//        }
        System.out.println(mLLCaseChargeDetailSet.size());

        return true;
    }

    /**
     * 对输入数据做必要的校验
     * @return boolean
     */
    private boolean checkInputData()
    {
        System.out.println("checkInputData()..."+mOperate);
        try
        {
//            LLCaseDrugSchema tLLCaseDrugSchema = mCaseDrugSet.get(1);
            mCaseNo = ""+mLLFeeMainSchema.getCaseNo();
            mMainFeeNo = ""+mLLFeeMainSchema.getMainFeeNo();
//            String sHandler = "";
            //查询案件状态
            LLCaseDB tLLCaseDB = new LLCaseDB();
            tLLCaseDB.setCaseNo(mCaseNo);
            if (tLLCaseDB.getInfo() == false)
            {
                // @@错误处理
                mErrors.copyAllErrors(tLLCaseDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LLCaseChargeDetailBL";
                tError.functionName = "checkInputData";
                tError.errorMessage = "理赔案件查询失败!" +
                                      "理赔号：" + mCaseNo;
                mErrors.addOneError(tError);
                return false;
            }
            String RgtState  =tLLCaseDB.getRgtState();
            if(!(RgtState.equals("01")||RgtState.equals("02")||RgtState.equals("08")))
            {
                CError tError = new CError();
                tError.moduleName = "ICaseCureBL";
                tError.functionName = "checkInputData";
                tError.errorMessage = "该案件状态下不能账单录入！";
                mErrors.addOneError(tError);
                return false;
            }
//            sHandler = tLLCaseDB.getHandler();
//            if (!sHandler.equals(mGlobalInput.Operator)) {
//                //当前操作者不是指定处理人
//                CError tError = new CError();
//                tError.moduleName = "ICaseCureBL";
//                tError.functionName = "checkInputData";
//                tError.errorMessage = "对不起，您没有权限处理该案件!" +
//                                      "指定处理人：" + sHandler;
//                mErrors.addOneError(tError);
//                return false;
//            }

            LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
            tLLFeeMainDB.setCaseNo(mCaseNo);
            tLLFeeMainDB.setMainFeeNo(mMainFeeNo);
            LLFeeMainSet tLLFeeMainSet = tLLFeeMainDB.query();
            if (tLLFeeMainSet.size()<=0)
            {
                // @@错误处理
                mErrors.copyAllErrors(tLLFeeMainDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LLCaseChargeDetailBL";
                tError.functionName = "checkInputData";
                tError.errorMessage = "账单信息查询失败!";
                mErrors.addOneError(tError);
                return false;
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseChargeDetailBL";
            tError.functionName = "checkInputData";
            tError.errorMessage = "在校验输入的数据时出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @param cOperate String 操作类型
     * @return boolean
     */
    private boolean dealData(String cOperate)
    {
        System.out.println("dealData()..."+mOperate);
        mOperate = cOperate;
        boolean tReturn = false;

        //保存录入
        if (mOperate.equals("INSERT"))
        {
            StringBuffer sbSql = new StringBuffer();
            sbSql
            	.append(" DELETE FROM LLCaseChargeDetail D ")
            	.append(" WHERE D.CASENO = '")
            	.append(mCaseNo)
            	.append("' AND D.MainFeeNo = '")
            	.append(mMainFeeNo)
            	.append("'");
            map.put(sbSql.toString(), "DELETE");
            String tLimit = PubFun.getNoLimit("86");

            LLCaseChargeDetailSchema tLLCaseChargeDetailSchema = null;
            LLCaseChargeDetailSet tLLCaseChargeDetailSet = new LLCaseChargeDetailSet();

            for (int i=1; i <= mLLCaseChargeDetailSet.size(); i ++ )
            {
                tLLCaseChargeDetailSchema = mLLCaseChargeDetailSet.get(i);
                tLLCaseChargeDetailSchema.setChargeDetailNo(
                        PubFun1.CreateMaxNo("ChargeDetailNo", tLimit));
                tLLCaseChargeDetailSchema.setMngCom(mGlobalInput.ManageCom);
                tLLCaseChargeDetailSchema.setOperator(mGlobalInput.Operator);
                tLLCaseChargeDetailSchema.setMakeDate(PubFun.getCurrentDate());
                tLLCaseChargeDetailSchema.setMakeTime(PubFun.getCurrentTime());
                tLLCaseChargeDetailSchema.setModifyDate(tLLCaseChargeDetailSchema.getMakeDate());
                tLLCaseChargeDetailSchema.setModifyTime(tLLCaseChargeDetailSchema.getMakeTime());

                tLLCaseChargeDetailSet.add(tLLCaseChargeDetailSchema);

            }

            map.put(tLLCaseChargeDetailSet, "INSERT");

            mResult.add(tLLCaseChargeDetailSet);
            tReturn = true;
        }

        //修改录入
        if (mOperate.equals("UPDATE"))
        {
        }

        //删除录入
        if (cOperate.equals("DELETE"))
        {
            StringBuffer sbSql = new StringBuffer();
            sbSql
            .append(" DELETE FROM LLCaseChargeDetail D ")
            .append(" WHERE D.CASENO = '")
            .append(mCaseNo)
            .append("' AND D.RGTNO = '")
            .append(mMainFeeNo)
            .append("'");

            map.put(sbSql.toString(), "DELETE");

 //           mResult.add(mFeeMainSchema);
            tReturn = true;
        }

        return tReturn;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        System.out.println("prepareOutputData()...");

        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseChargeDetailBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 得到处理后的结果数据
     * @return boolean
     */
    public VData getResult()
    {
        return this.mResult;
    }

}
