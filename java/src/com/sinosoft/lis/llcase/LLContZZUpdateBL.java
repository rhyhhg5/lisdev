package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.LJAGetClaimDB;
import com.sinosoft.lis.db.LLContDealDB;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.vschema.LJAGetClaimSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LLContZZUpdateBL {
	public CErrors mErrors = new CErrors();
	/** 错误处理类 */
	private VData mInputData = new VData();
	/** 输入数据的容器 */
	private VData mResult = new VData();
	/** 输出数据的容器 */
	private MMap mMMap = new MMap();

	/** 数据操作字符串 */
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mCurrentTime = PubFun.getCurrentTime();
	private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();

	public LLContZZUpdateBL() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 数据提交的公共方法
	 * 
	 * @param: cInputData 传入的数据
	 * @param: cOperate 数据操作字符串
	 * @return: boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将传入的数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();

		// 将外部传入的数据分解到本类的属性中，准备处理
		if (!getInputData()) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		// 根据业务逻辑对数据进行处理
		if (!dealData()) {
			return false;
		}

		if (!prepareOutputData()) {
			return false;
		}
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, "")) {
			// @@错误处理
			CError.buildErr(this, "数据保存失败");
			return false;
		}
		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	private boolean getInputData() {
		System.out.println("LLContZZUpdateBL============Begin===========");
		mLPEdorItemSchema = (LPEdorItemSchema) mInputData
				.getObjectByObjectName("LPEdorItemSchema", 0);
		return true;
	}

	/**
	 * 校验传入的数据
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	private boolean checkData() {
		if (mLPEdorItemSchema.getEdorAcceptNo() == null
				|| "".equals(mLPEdorItemSchema.getEdorAcceptNo())) {
			mErrors.addOneError("获取合同处理号失败");
			return false;
		}

		return true;
	}

	/**
	 * 根据业务逻辑对数据进行处理
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	private boolean dealData() {

		// 处理需要自动终止的保单
		LLContDealDB tLLContDealDB = new LLContDealDB();
		tLLContDealDB.setEdorNo(mLPEdorItemSchema.getEdorAcceptNo());
		if (!tLLContDealDB.getInfo()) {
			mErrors.addOneError("合同处理查询失败");
		}
		if (!"HZ".equals(tLLContDealDB.getEdorType())) {
			return true;
		}

		String ljsSql = "update ljsgetendorse set getmoney =0 "
				+ " where EndorsementNo='"
				+ mLPEdorItemSchema.getEdorAcceptNo() + "'";
		String ljsgSql = "update ljsget set sumgetmoney =0 "
				+ " where otherno = '" + mLPEdorItemSchema.getEdorAcceptNo()
				+ "'";
		String lpeitemSql = "update lpedoritem set getmoney =0 "
				+ " where EdorAcceptNo='" + mLPEdorItemSchema.getEdorAcceptNo()
				+ "'";
		String lpemainSql = "update LPEdorMain set getmoney =0 "
				+ " where EdorAcceptNo='" + mLPEdorItemSchema.getEdorAcceptNo()
				+ "'";
		String lpeappSql = "update LPEdorApp set getmoney =0 "
				+ " where EdorAcceptNo='" + mLPEdorItemSchema.getEdorAcceptNo()
				+ "'";
		mMMap.put(ljsSql, "UPDATE");
		mMMap.put(ljsgSql, "UPDATE");
		mMMap.put(lpeitemSql, "UPDATE");
		mMMap.put(lpemainSql, "UPDATE");
		mMMap.put(lpeappSql, "UPDATE");

		return true;
	}

	private boolean prepareOutputData() {
		mInputData.clear();
		mInputData.add(mMMap);
		return true;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
