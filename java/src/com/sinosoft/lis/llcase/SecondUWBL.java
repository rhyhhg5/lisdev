package com.sinosoft.lis.llcase;

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: CaseCureBL.java</p>
 * <p>Description: 理赔－审核－保存及显示</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @date   2003-8-4
 * @version 1.0
 */
public class SecondUWBL
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData ;
  private VData mResult = new VData();
  private String mOperate;
  private String CustomerNo="";
  private String RgtNo = "";
  private String UWRegister="";
  private String OperateFlag="";
  private LCPolSet mLCPolSet = new LCPolSet();
  private LCPolSet endLCPolSet = new LCPolSet();//存放初始化信息
  private LDSysTraceSet mLDSysTraceSet = new LDSysTraceSet();
  private String pos = "";
  private int count = 0;

  private GlobalInput mGlobalInput = new GlobalInput();
  private GlobalInput mG = new GlobalInput();

  public SecondUWBL() {}

  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;
    if (!getInputData(cInputData))
      return false;
    if(cOperate.equals("SHOW"))
    {
      if(!showData())
        return false;
    }
    if(cOperate.equals("INIT"))
    {
      pos = "案件审核二次核保";
      initData();//初始化已经有的信息
    }
    if(cOperate.equals("INITEND"))
    {
      pos="案件审核解约暂停";
      initData();
    }
    //校验保存的条件
    if (cOperate.equals("INSERT"))
    {
      LLRegisterDB tLLRegisterDB = new LLRegisterDB();
      tLLRegisterDB.setRgtNo(RgtNo);
      if(!tLLRegisterDB.getInfo())
      {
        this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "RegisterUpdateBL";
        tError.functionName = "submitData";
        tError.errorMessage = "立案信息查询失败，系统中没有该案件的记录！！";
        this.mErrors .addOneError(tError) ;
        return false;
      }
      if(tLLRegisterDB.getClmState().equals("5"))
      {
        this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "RegisterUpdateBL";
        tError.functionName = "submitData";
        tError.errorMessage = "该案件正处于调查之中，您无法进行保存操作！！";
        this.mErrors .addOneError(tError) ;
        return false;
      }
      if(tLLRegisterDB.getEndCaseFlag().equals("2"))
      {
        this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "RegisterUpdateBL";
        tError.functionName = "submitData";
        tError.errorMessage = "该案件已经结案，您无法进行保存操作！！";
        this.mErrors .addOneError(tError) ;
        return false;
      }
      if (!dealData())
        return false;
    }
    return true;
  }

  private boolean getInputData(VData cInputData)
  {
    if(mOperate.equals("INSERT"))
    {
      LCPolSet tLCPolSet = new LCPolSet();
      RgtNo       = (String)cInputData.get(0);
      UWRegister  = (String)cInputData.get(1);
      OperateFlag = (String)cInputData.get(2);
      //mLCPolSet:承载界面上的保单信息的集合
      mLCPolSet.set((LCPolSet)cInputData.getObjectByObjectName("LCPolSet",0));
      mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));

      int n = mLCPolSet.size();
      for (int i = 1; i <= n; i++)
      {
        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSchema tLCPolSchema = mLCPolSet.get(i);
        tLCPolDB.setPolNo( tLCPolSchema.getPolNo());
        System.out.println("--BL--Pol--"+tLCPolSchema.getPolNo());
        String temp = tLCPolSchema.getPolNo();
        System.out.println("temp"+temp);
        if (tLCPolDB.getInfo() == false)
        {
          // @@错误处理
          this.mErrors.copyAllErrors( tLCPolDB.mErrors );
          CError tError = new CError();
          tError.moduleName = "SecondUWBL";
          tError.functionName = "getInputData";
          tError.errorMessage = temp+"投保单查询失败!";
          this.mErrors .addOneError(tError) ;
          return false;
        }
        else
        {
          tLCPolSchema.setSchema( tLCPolDB );
          tLCPolSet.add(tLCPolSchema);
        }
      }
      mLCPolSet.clear() ;
      mLCPolSet.set(tLCPolSet);
    }
    if(mOperate.equals("SHOW"))
    {
      CustomerNo = (String)cInputData.get(0);
    }
    if(mOperate.equals("INIT")||mOperate.equals("INITEND"))
    {
      CustomerNo = (String)cInputData.get(0);
      System.out.println("SecondUWBL客户号码是"+CustomerNo);
    }
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  private boolean dealData()
  {
    prepareOutputData();
    SecondUWBLS tSecondUWBLS = new SecondUWBLS();
    if (!tSecondUWBLS.submitData(mInputData,mOperate))
    {
      this.mErrors.copyAllErrors(tSecondUWBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "CaseCureBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据保存失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }


  private void prepareOutputData()
  {
    try
    {
      LCPolSet tLCPolSet = new LCPolSet();
      int count=mLCPolSet.size();
      if(count>0)
      {
        for(int h=1;h<=count;h++)
        {
          //tLCPolSchema 承载的是界面上传递来的数据
          LCPolSchema tLCPolSchema = new LCPolSchema();
          tLCPolSchema.setSchema(mLCPolSet.get(h));
          //再对LDSysTrace表进行赋值之前，先再LDSysTrace表中对该保单号码进行查询。

          LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
          tLDSysTraceSchema.setPolNo(tLCPolSchema.getPolNo());
          tLDSysTraceSchema.setOperator(mG.Operator);
          tLDSysTraceSchema.setMakeDate(PubFun.getCurrentDate());
          tLDSysTraceSchema.setMakeTime(PubFun.getCurrentTime());
          tLDSysTraceSchema.setManageCom(mG.ManageCom);
          if(OperateFlag.equals("UW"))
          {
            tLCPolSchema.setPolState("0405"+tLCPolSchema.getPolState().substring(0,4) ) ;
            tLDSysTraceSchema.setCreatePos("案件审核二次核保");
            tLDSysTraceSchema.setPolState("4002");//sxy-2003-09-17待与保单表相应字段统一?
          }
          if(OperateFlag.equals("END"))
          {
            tLCPolSchema.setPolState("0402"+tLCPolSchema.getPolState().substring(0,4) ) ;
            tLDSysTraceSchema.setCreatePos("案件审核解约暂停");
            tLDSysTraceSchema.setPolState("4003");
          }
          tLDSysTraceSchema.setOperator2(mG.Operator);
          tLDSysTraceSchema.setManageCom2(mG.ManageCom);
          tLDSysTraceSchema.setStartDate(PubFun.getCurrentDate());
          tLDSysTraceSchema.setModifyDate(PubFun.getCurrentDate());
          tLDSysTraceSchema.setModifyTime(PubFun.getCurrentTime());
          tLDSysTraceSchema.setValiFlag("1");
          tLDSysTraceSchema.setRemark(UWRegister);
          tLCPolSet.add(tLCPolSchema);
          mLDSysTraceSet.add(tLDSysTraceSchema);
        }
      }
      //对分案诊疗信息表的处理
      mInputData.clear();
      mInputData.addElement(mLDSysTraceSet);
      mInputData.add(tLCPolSet);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  private boolean showData()
  {
    String sql_c = "select * from LCPol where Appflag = '1' and ( InsuredNo= '"+CustomerNo+"' or AppntNo = '"+CustomerNo+"' )";
    String sql_b = "select * from LBPol where Appflag = '1' and ( InsuredNo= '"+CustomerNo+"' or AppntNo = '"+CustomerNo+"' )";
    LCPolSet tLCPolSet = new LCPolSet();
    LCPolDB tLCPolDB = new LCPolDB();
    tLCPolSet.add(tLCPolDB.executeQuery(sql_c));
    LBPolDB tLBPolDB = new LBPolDB();
    LBPolSet tLBPolSet = new LBPolSet();
    tLBPolSet.add(tLBPolDB.executeQuery(sql_b));
    LCPolSet rLCPolSet = new LCPolSet();
    LBPolSet rLBPolSet = new LBPolSet();
    //Remark中放入险种名称；AccName：放入事故者类型
    if(tLCPolSet.size()>0)
    {
      for(int count=1;count<=tLCPolSet.size();count++)
      {
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setSchema(tLCPolSet.get(count));
        String sql_riskname = " select RiskName from LMRisk where RiskCode = '"+tLCPolSchema.getRiskCode()+"'";
        ExeSQL exesql = new ExeSQL();
        SSRS ssrs = new SSRS();
        ssrs = exesql.execSQL(sql_riskname);
        if(ssrs.getMaxRow()>0)
        {
          tLCPolSchema.setRemark(ssrs.GetText(1,1));
        }
        /*Lis5.3 upgrade set
        if(tLCPolSchema.getAppntNo().equals(tLCPolSchema.getInsuredNo()))
        {
          tLCPolSchema.setAccName("即是被保人又是投保人");
        }
        if(tLCPolSchema.getAppntNo().equals(CustomerNo)&&(!tLCPolSchema.getInsuredNo().equals(CustomerNo)))
        {
          tLCPolSchema.setAccName("投保人");
        }
        if(tLCPolSchema.getInsuredNo().equals(CustomerNo)&&(!tLCPolSchema.getAppntNo().equals(CustomerNo)))
        {
          tLCPolSchema.setAccName("被保人");
        }
        */
        rLCPolSet.add(tLCPolSchema);
      }
    }
    if(tLBPolSet.size()>0)
   {
     for(int count=1;count<=tLBPolSet.size();count++)
     {
       LBPolSchema tLBPolSchema = new LBPolSchema();
       tLBPolSchema.setSchema(tLBPolSet.get(count));
       String sql_riskname = " select RiskName from LMRisk where RiskCode = '"+tLBPolSchema.getRiskCode()+"'";
       ExeSQL exesql = new ExeSQL();
       SSRS ssrs = new SSRS();
       ssrs = exesql.execSQL(sql_riskname);
       if(ssrs.getMaxRow()>0)
       {
         tLBPolSchema.setRemark(ssrs.GetText(1,1));
       }
       /*Lis5.3 upgrade set
       if(tLBPolSchema.getAppntNo().equals(tLBPolSchema.getInsuredNo()))
       {
         tLBPolSchema.setAccName("即是被保人又是投保人");
       }
       if(tLBPolSchema.getAppntNo().equals(CustomerNo)&&(!tLBPolSchema.getInsuredNo().equals(CustomerNo)))
       {
         tLBPolSchema.setAccName("投保人");
       }
       if(tLBPolSchema.getInsuredNo().equals(CustomerNo)&&(!tLBPolSchema.getAppntNo().equals(CustomerNo)))
       {
         tLBPolSchema.setAccName("被保人");
       }
*/
       rLBPolSet.add(tLBPolSchema);
     }
   }
    mResult.clear();
    mResult.addElement(rLCPolSet);
    mResult.addElement(rLBPolSet);
    return true;
  }
  /*******************************************************************
   * Remark-----"核保提示"
   * BankAccNo----"事故者类型"
   * AccName----“险种名称”
  */
  public LCPolSet initData()
  {
    //取出该客户的所有的保单号码
    String end_sql = " select PolNo,RiskCode,AppntName,InsuredName,Mult ,AppntNo,InsuredNo,Amnt"
                   +" from LCPol where AppFlag = '1' and (InsuredNo = '"
                   +CustomerNo+"' or appntno = '"+CustomerNo+"')"
                   +" and polno in (select polno from LDSysTrace) union "
                   +" select PolNo,RiskCode,AppntName,InsuredName,Mult ,AppntNo,InsuredNo,Amnt"
                   +" from LBPol where AppFlag = '1' and (InsuredNo = '"
                   +CustomerNo+"' or appntno = '"+CustomerNo+"')"
                   +" and polno in (select polno from LDSysTrace )  ";
    System.out.println("最终的查询语句是"+end_sql);
    ExeSQL exesql = new ExeSQL();
    SSRS ssrs = exesql.execSQL(end_sql);
    System.out.println("该客户的保单个数是"+ssrs.getMaxRow());
    if(ssrs.getMaxRow()>0)
    {
      for(int i=1;i<=ssrs.getMaxRow();i++)
      {
        getLDSysTrace(ssrs.GetText(i,1),pos);
        if (count>1)
        {
          LCPolSchema endLCPolSchema = new LCPolSchema();
          endLCPolSchema.setPolNo(ssrs.GetText(i,1));//保单号码
          endLCPolSchema.setRiskCode(ssrs.GetText(i,2));//险种代码
          endLCPolSchema.setAppntName(ssrs.GetText(i,3).trim());//投保人姓名
          endLCPolSchema.setInsuredName(ssrs.GetText(i,4).trim());//被保人姓名
          endLCPolSchema.setMult(ssrs.GetText(i,5));//份数
          /*Lis5.3 upgrade set
          endLCPolSchema.setAccName(getRiskName(ssrs.GetText(i,2)));//险种名称
          endLCPolSchema.setBankAccNo(getInsureType(ssrs.GetText(i,6),ssrs.GetText(i,7)));//事故者类型
          */
          endLCPolSchema.setAmnt(Integer.parseInt(ssrs.GetText(i,8)));
          endLCPolSchema.setRemark(getLDSysTrace(ssrs.GetText(i,1),pos));//结论
          System.out.println("保单号码"+endLCPolSchema.getPolNo());
          System.out.println("险种代码"+endLCPolSchema.getRiskCode());
          System.out.println("投保人姓名"+endLCPolSchema.getAppntName());
          System.out.println("被保人姓名"+endLCPolSchema.getInsuredName());
          System.out.println("份数"+endLCPolSchema.getMult());
          System.out.println("结论"+endLCPolSchema.getRemark());
          endLCPolSet.add(endLCPolSchema);
        }
        mResult.add(endLCPolSet);
      }
    }
    return endLCPolSet;
  }
  //查询险种代码的函数
  public String getRiskName(String aRiskCode)
  {
    String RiskName = "";
    LMRiskDB aLMRiskDB = new LMRiskDB();
    aLMRiskDB.setRiskCode(aRiskCode);
    if(aLMRiskDB.getInfo())
    {
      RiskName = aLMRiskDB.getRiskName();
    }
    return RiskName;
  }
  public String getInsureType(String aAppntNo,String aInsuredNo)
  {
    String InsureType = "";
    if(aInsuredNo.trim().equals(aAppntNo.trim()))
    {
      InsureType = "即是被保人又是投保人";
    }
    else
    {
      if(CustomerNo.equals(aInsuredNo.trim()))
        InsureType = "被保人";
      if(CustomerNo.equals(aAppntNo.trim()))
        InsureType = "投保人";
    }
    return InsureType;
  }
  //查询LDSysTrace的信息
  public String getLDSysTrace(String aPolNo,String pos)
  {
    String Remark = "无";
    String a_sql = "select Remark from LDSysTrace where PolNo = '"+aPolNo
                 +"' and Createpos like '%"+pos.trim()+"%'order by makedate desc ,maketime desc";
    System.out.println("LDSysTrace的查询语句是"+a_sql);
    ExeSQL exesql = new ExeSQL();
    SSRS ssrs = exesql.execSQL(a_sql);
    count=ssrs.getMaxRow();
    System.out.println("查询的个数是"+count);
    if(ssrs.getMaxRow()>0)
    {
      Remark = ssrs.GetText(1,1);
    }
    return Remark;
  }
  public static void main(String[] args)
  {
    SecondUWBL aSecondUWBL = new SecondUWBL();
    String aRgtNo = "00100020030510000002";
    String aUWRegister = "2004-3-1二次核保测试数据aaaaaaaaaa";
    String aOperateFlag = "INSERT";
    String tG = "000012";
    //LCPolSchema
    //String
    String tCustomerNo = "0000001166";
    VData tVData = new VData();
    tVData.addElement(tCustomerNo);
    aSecondUWBL.submitData(tVData,"INIT");
  }
}
