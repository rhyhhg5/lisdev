package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.task.CommonBL;
import com.sinosoft.task.Task;

/**
 * <p>Title: 工单管理系统</p>
 * <p>Description: 工单录入BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 * @date 2005-01-17
 */

public class LLContDealUWBL {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;
    private String mEdorNo = "";
    private String mCaseNo = "";
    private LLContDealUWDetailSchema mLLContDealUWDetailSchema = new LLContDealUWDetailSchema();
    private MMap map = new MMap();

    /** 全局参数 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 统一更新日期 */
    private String mCurrentDate = PubFun.getCurrentDate();
    /** 统一更新时间 */
    private String mCurrentTime = PubFun.getCurrentTime();
    private String mOperator = "";
    private String mUpFlag = "";

    public LLContDealUWBL() {
    }

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public boolean submitData(VData cInputData, String cOperate) 
    {
    	// 将传入的数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        // 将外部传入的数据分解到本类的属性中，准备处理
        if (!getInputData()) {
            return false;
        }
        
        if (!checkData()) {
            return false;
        }

        // 根据业务逻辑对数据进行处理
        if (!dealData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "TaskInputBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData() {
    	mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mLLContDealUWDetailSchema.setSchema((LLContDealUWDetailSchema) mInputData.getObjectByObjectName("LLContDealUWDetailSchema", 0));
        
        mEdorNo = mLLContDealUWDetailSchema.getEdorNo();
        mCaseNo = mLLContDealUWDetailSchema.getCaseNo();
        mOperator = mLLContDealUWDetailSchema.getAppOperator();
        if(mEdorNo == null ||mCaseNo == null)
        {
        	 CError.buildErr(this, "合同处理工单号或案件号查询失败！");
             return false;
        }
        System.out.println("工单号mEdorNo====="+mEdorNo);
        System.out.println("案件号mCaseNo====="+mCaseNo);
        return true;
    }

    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData() { 
    	//得到理赔保全处理用户
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("llcontdeal");
        tLDCodeDB.setCode("user");
        if (!tLDCodeDB.getInfo()) {
            mErrors.addOneError("未配置理赔合同处理保全用户！");
            return false;
        } 
        
        LLCaseDB tLLCaseDB = new LLCaseDB();        
        tLLCaseDB.setCaseNo(mCaseNo);
        if (!tLLCaseDB.getInfo()) {
            mErrors.addOneError("案件信息查询失败！");
            return false;
        }        
        
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData() {
    	int tCount;    	
    	String uwSql="select max(ClmUWNo) from LLContDealUWDetail where EdorNo='"+ mEdorNo +"'";
    	ExeSQL uwexesql = new ExeSQL();
        System.out.println(uwSql);
        SSRS uwss = uwexesql.execSQL(uwSql);
    	if(uwss == null ||uwss.getMaxRow()<=0)
    	{
    		CError.buildErr(this, "合同处理轨迹表信息查询失败！");
            return false;
    	}
    	if(uwss.GetText(1, 1).equals("0"))
    	{
    		tCount = 1;
    	}    		    	
    	else{
    		tCount = Integer.parseInt(uwss.GetText(1, 1));
    		tCount++;
    	}
    	         
    	String tsql = "select b.upusercode,c.limitmoney,c.ClaimPopedom "
    			+" from LLClaimUser b,LLClaimPopedom c "
    			+" where b.ClaimPopedom=c.ClaimPopedom and b.stateflag='1'"
    			+" and c.GetDutyKind='4' and c.GetDutyType='0' and b.usercode='"+mOperator+"'";
    	ExeSQL texesql = new ExeSQL();
        System.out.println(tsql);
        SSRS tss = texesql.execSQL(tsql);
        if (tss == null || tss.getMaxRow()<=0) 
        {
        	CError.buildErr(this, "合同处理轨迹表信息查询失败！");
            return false;
        } 
             
        //合同处理案件状态修改
        LLCaseDB tLLCaseDB = new LLCaseDB();
    	LLCaseSchema tLLCaseSchema = new LLCaseSchema();
    	tLLCaseDB.setCaseNo(mCaseNo);
    	if(tLLCaseDB.getInfo())
    	tLLCaseSchema = tLLCaseDB.getSchema();
    	
    	LLContDealDB tLLContDealDB = new LLContDealDB();
    	LLContDealSchema tLLContDealSchema = new LLContDealSchema();
    	tLLContDealDB.setEdorNo(mEdorNo);
    	if(tLLContDealDB.getInfo()) tLLContDealSchema = tLLContDealDB.getSchema();
    	
        LLContDealUWDetailSchema tLLContDealUWDetailSchema = new LLContDealUWDetailSchema();
        if (tss.GetText(1, 2).equals("1")) 
        {        
        	mUpFlag = "0";
        	tLLContDealUWDetailSchema.setClmUWNo(tCount);
        	tLLContDealUWDetailSchema.setEdorNo(mEdorNo);
        	tLLContDealUWDetailSchema.setEdorType(mLLContDealUWDetailSchema.getEdorType());
        	tLLContDealUWDetailSchema.setCaseNo(mCaseNo);
        	tLLContDealUWDetailSchema.setCustomerNo(mLLContDealUWDetailSchema.getCustomerNo());
        	tLLContDealUWDetailSchema.setAppDate(mCurrentDate);
        	tLLContDealUWDetailSchema.setAppOperator(mLLContDealUWDetailSchema.getAppOperator());
        	tLLContDealUWDetailSchema.setAppGrade(tss.GetText(1, 3));
        	tLLContDealUWDetailSchema.setClmUWer(mLLContDealUWDetailSchema.getAppOperator());
        	tLLContDealUWDetailSchema.setClmUWGrade(tss.GetText(1, 3));
        	tLLContDealUWDetailSchema.setClmDecision("1"); //1-确认 2-上报    
        	tLLContDealUWDetailSchema.setManageCom(mGlobalInput.ComCode);
        	tLLContDealUWDetailSchema.setOperator(mLLContDealUWDetailSchema.getAppOperator());
        	tLLContDealUWDetailSchema.setMakeDate(mCurrentDate);
        	tLLContDealUWDetailSchema.setMakeTime(mCurrentTime);
        	tLLContDealUWDetailSchema.setModifyDate(mCurrentDate);
        	tLLContDealUWDetailSchema.setModifyTime(mCurrentTime);
        	        	
        	tLLCaseSchema.setContDealFlag("2");
        	tLLContDealSchema.setDealer(mLLContDealUWDetailSchema.getAppOperator());
        	tLLContDealSchema.setDealerGrade(tss.GetText(1, 3));
        	
        }else if(!tss.GetText(1, 2).equals("1"))
        {
        	mUpFlag = "1";	        	
        	tLLContDealUWDetailSchema.setClmUWNo(tCount);
        	tLLContDealUWDetailSchema.setEdorNo(mEdorNo);
        	tLLContDealUWDetailSchema.setEdorType(mLLContDealUWDetailSchema.getEdorType());
        	tLLContDealUWDetailSchema.setCaseNo(mCaseNo);
        	tLLContDealUWDetailSchema.setCustomerNo(mLLContDealUWDetailSchema.getCustomerNo());
        	tLLContDealUWDetailSchema.setAppDate(mCurrentDate);
        	tLLContDealUWDetailSchema.setAppOperator(mLLContDealUWDetailSchema.getAppOperator());
        	tLLContDealUWDetailSchema.setAppGrade(tss.GetText(1, 3));
        	tLLContDealUWDetailSchema.setClmUWer(tss.GetText(1, 1));
        	tLLContDealUWDetailSchema.setClmUWGrade("");
        	tLLContDealUWDetailSchema.setClmDecision("2"); //1-确认 2-上报
        	tLLContDealUWDetailSchema.setManageCom(mGlobalInput.ComCode);
        	tLLContDealUWDetailSchema.setOperator(mLLContDealUWDetailSchema.getAppOperator());
        	tLLContDealUWDetailSchema.setMakeDate(mCurrentDate);
        	tLLContDealUWDetailSchema.setMakeTime(mCurrentTime);
        	tLLContDealUWDetailSchema.setModifyDate(mCurrentDate);
        	tLLContDealUWDetailSchema.setModifyTime(mCurrentTime);
        	        	
        	tLLCaseSchema.setContDealFlag("1");
        	tLLContDealSchema.setDealer(tss.GetText(1, 1));
        	
        }
        map.put(tLLContDealUWDetailSchema, "DELETE&INSERT");
    	map.put(tLLCaseSchema, "DELETE&INSERT");
    	map.put(tLLContDealSchema, "DELETE&INSERT");
    	mInputData.clear();
    	mInputData.add(map);
        return true;
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public String getResult() {
        return mUpFlag;
    }

    public static void main(String arg[]) {
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "LP";
        
        LLContDealUWDetailSchema tLLContDealUWDetailSchema=new LLContDealUWDetailSchema();
    	tLLContDealUWDetailSchema.setEdorNo("20101020000005");
    	tLLContDealUWDetailSchema.setEdorType("HZ");
    	tLLContDealUWDetailSchema.setCustomerNo("000944215");
    	tLLContDealUWDetailSchema.setCaseNo("C1100100608000004");
    	tLLContDealUWDetailSchema.setAppOperator("cm1102");
    	
    	VData uVData = new VData();
    	uVData.add(tLLContDealUWDetailSchema);
    	uVData.add(tGI);
    	LLContDealUWBL tLLContDealUWBL = new LLContDealUWBL();
    	if (!tLLContDealUWBL.submitData(uVData, "INSERT"))
    	{
    		System.out.println("-------------------");
    	}
    }
}
