package com.sinosoft.lis.llcase;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author cc
 * @version 1.0
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LLGrpAccModifyUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();

    private String strUrl = "";

    public LLGrpAccModifyUI()
    {
    }

    /**
         传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("MODIFY"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        // 进行业务处理
        if (!dealData())
        {
            return false;
        }

        // 准备传往后台的数据
        VData vData = new VData();

        if (!prepareOutputData(vData))
        {
            return false;
        }

        LLGrpAccModifyBL tLLGrpAccModifyBL = new LLGrpAccModifyBL();
        System.out.println("Start tLLGrpAccModify UI Submit ...");

        if (!tLLGrpAccModifyBL.submitData(vData, cOperate))
        {
            if (tLLGrpAccModifyBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tLLGrpAccModifyBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "LLAccModifyBL发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tLLGrpAccModifyBL.getResult();
            return true;
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.add(mGlobalInput);
            vData.add(mLLRegisterSchema);
            vData.add(mLJAGetSchema);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLJAGetSchema = (LJAGetSchema) cInputData.getObjectByObjectName("LJAGetSchema", 0);
        mLLRegisterSchema = (LLRegisterSchema) cInputData.getObjectByObjectName("LLRegisterSchema", 0);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "DerferAppF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    public static void main(String[] args)
    {
        VData tVData = new VData();

        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86210000";
        tG.Operator = "UW0013";
        String url = "D:\\DEVELOP\\ui";
        tVData.addElement(tG);
        tVData.addElement(url);

    }
}
