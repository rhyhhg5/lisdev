package com.sinosoft.lis.llcase;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: CaseCureBLS.java</p>
 * <p>Description:理赔－审核－二次核保的保存</p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author 刘岩松
 * @date :2003-08-04
 * @version 1.0
 */

public class SecondUWBLS
{
  private VData mInputData ;
  public  CErrors mErrors = new CErrors();
  public String mOperate;
  private LDSysTraceSet mLDSysTraceSet = new LDSysTraceSet();
  private LCPolSet mLCPolSet = new LCPolSet();
  public SecondUWBLS() {}

  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;
    if (!getInputData(cInputData))
      return false;
    if (cOperate.equals("INSERT"))
    {
      try
      {
        if (!this.saveData())
          return false;
      }
      catch(Exception ex)
      {
      }
      return true;
    }
    mInputData=(VData)cInputData.clone();
    String mOperate;
    mOperate=cOperate;
    mInputData=null;
    return true;
  }
  private boolean getInputData(VData cInputData)
  {
    mLDSysTraceSet.set((LDSysTraceSet)cInputData.getObjectByObjectName("LDSysTraceSet",0));
    mLCPolSet.set((LCPolSet)cInputData.getObjectByObjectName("LCPolSet",0));
    return true;
  }

  private boolean saveData()
  {
    Connection conn = DBConnPool.getConnection();
    if (conn==null)
    {
      CError tError = new CError();
      tError.moduleName = "CaseCureBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    try
    {
      conn.setAutoCommit(false);
      if(mLDSysTraceSet.size()==0)
      {
        this.mErrors.copyAllErrors(mLDSysTraceSet.mErrors);
        CError tError = new CError();
        tError.moduleName = "CaseCureBL";
        tError.functionName = "submitData";
        tError.errorMessage = "请您选择要进行保存的数据!";
        this.mErrors .addOneError(tError) ;
        return false;
      }
      else
      {
        LDSysTraceDBSet tLDSysTraceDBSet = new LDSysTraceDBSet(conn);
        tLDSysTraceDBSet.set(mLDSysTraceSet);
        if(!tLDSysTraceDBSet.insert())
        {
          this.mErrors.copyAllErrors(tLDSysTraceDBSet.mErrors);
          CError tError = new CError();
          tError.moduleName = "LLCaseCureBL";
          tError.functionName = "DeleteData";
          tError.errorMessage = "删除失败!";
          this.mErrors.addOneError(tError);
          conn.rollback() ;
          conn.close();
          return false;
        }
        conn.commit() ;
        conn.close();
      }
    }
    catch (Exception ex)
    {
      CError tError = new CError();
      tError.moduleName = "CaseCureBLS";
      tError.functionName = "submitData";
      tError.errorMessage = ex.toString();
      this.mErrors .addOneError(tError);
      try
      {
        conn.rollback() ;
        conn.close();
      }
      catch(Exception e)
      {
        ex.printStackTrace();
      }
      return false;
    }

	try
	{
	  conn.setAutoCommit(false);
	  LCPolDBSet tLCPolDBSet = new LCPolDBSet(conn);
	  tLCPolDBSet.set(mLCPolSet);
	  if(!tLCPolDBSet.update())
	  {
		this.mErrors.copyAllErrors(tLCPolDBSet.mErrors);
		CError tError = new CError();
		tError.moduleName = "SecondUWBLS";
		tError.functionName = "saveData()";
		tError.errorMessage = "更新失败!";
		this.mErrors.addOneError(tError);
		conn.rollback() ;
		conn.close();
		return false;
	  }
	  conn.commit() ;
	  conn.close();
	}

	catch (Exception ex)
	{
	  CError tError = new CError();
	  tError.moduleName = "SecondUWBLS";
	  tError.functionName = "saveData()";
	  tError.errorMessage = ex.toString();
	  this.mErrors .addOneError(tError);
	  try
	  {
		conn.rollback() ;
		conn.close();
	  }
	  catch(Exception e)
	  {
		ex.printStackTrace();
	  }
	  return false;
    }
    return true;
  }
}