package com.sinosoft.lis.llcase;


import java.io.*;

import java.util.zip.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;

/**
 * 理赔Insurance提数，将提数结果打包并邮件发送
 * @author Houyd
 *
 */

public class SegentInfeeDataExtraction {

	public String mCurrentDate = PubFun.getCurrentDate();
	    private BufferedWriter mBufferedWriter;
	    
	    /**获取当前的工作路径*/
		private String mURL = "";
	    public CErrors mErrors = new CErrors();
	    private static final int BUFFER = 2048;
	    
	    /**对提数结果压缩成功标志：1-成功;0-失败;-1-失败终止*/
	    private int mFlag;
	    /**压缩文件的路径*/
	    private String mZipFilePath;
	    /** SQL结果存放文件名*/
	    private String mZipDocFileName = "Infee.txt";
	    
	    /** 邮件收件人 */
	    private String mAddress = "";
	    /** 邮件抄送人 */
	    private String mCcAddress = "";
	    /** 邮件暗抄人 */
	    private String mBccAddress = "";
	    
	public String getMAddress() {
			return mAddress;
		}

		public void setMAddress(String address) {
			mAddress = address;
		}

		public String getMBccAddress() {
			return mBccAddress;
		}

		public void setMBccAddress(String bccAddress) {
			mBccAddress = bccAddress;
		}

		public String getMCcAddress() {
			return mCcAddress;
		}

		public void setMCcAddress(String ccAddress) {
			mCcAddress = ccAddress;
		}
	    
	public static void main(String[] args) throws IOException{
		/**
		Date currentTime = new Date();
		System.out.println("Start at：" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentTime));
//		
//		String tTxtDirectory = "D:\\test\\";  //默认txt文件目录，若不传参数，则用此目录
//		String tLogDirectory = "D:\\test\\";  //默认日志文件目录，若不传参数，则用此目录
//		
//		if(args.length == 1){
//			tTxtDirectory = args[0];
//		}
//		else if(args.length == 2){
//			tTxtDirectory = args[0];
//			tLogDirectory = args[1];
//		}
//		
//		System.out.println("TxtDirectory:" + tTxtDirectory);
//		System.out.println("LogDirectory:" + tLogDirectory);
//		
//		MonthDataCatch tMonthDataCatch = new MonthDataCatch();
//		tMonthDataCatch.createCRMLog(tTxtDirectory, tLogDirectory);
		
		
		MonthDataCatch tMonthDataCatch = new MonthDataCatch();
		tMonthDataCatch.getSagentData();
		
		System.out.println("End at：" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		*/
		SegentInfeeDataExtraction tSegentDateDataExtraction = new SegentInfeeDataExtraction();
		tSegentDateDataExtraction.getSagentData();
	}
	
	/**给定目录下创建文件夹*/
	public static boolean createDir(String destDirName) {
	    File dir = new File(destDirName);
	    if(dir.exists()) {
	     System.out.println("创建目录" + destDirName + "失败，目标目录已存在！");
	     return false;
	    }
	    if(!destDirName.endsWith(File.separator))
	     destDirName = destDirName + File.separator;
	    // 创建单个目录
	    if(dir.mkdirs()) {
	     System.out.println("创建目录" + destDirName + "成功！");
	     return true;
	    } else {
	     System.out.println("创建目录" + destDirName + "成功！");
	     return false;
	    }
	}

	
	/**
	 * 执行批处理，并发送邮件
	 * @return
	 */
    public boolean getSagentData()
    {
    	String tSQL = "select sysvarvalue from ldsysvar where sysvar = 'UIRoot' with ur";
    	mURL = new ExeSQL().getOneValue(tSQL);//生成文件的存放路径
    	
    	mURL += "temp_lp/segent/";
        //this.mURL = "D:\\test\\";
        System.out.println("网盘路径："+mURL);
        if(PubDocument.save(mURL)){
        	mURL = PubDocument.getSavePath(mURL);
        }
        if (mURL == null || mURL.equals(""))
        {
            System.out.println("没有找到理赔提数文件夹！");
            return false;
        }

        getDataResult();
        getZipFile();
        sendSagentMail();
       
        // ---------------------
       // FtpTransferFiles();
//        try
//        {
//            del(mURL);
//        } catch (IOException ex)
//        {
//            ex.printStackTrace();
//        }
   		return true;
    }
    
    /**
     * 将查询结果的压缩包发送邮件
     *
     */
    private void sendSagentMail(){
    	if(mFlag == 1){//压缩文件生成成功，开始发送邮件
    		SagentMail tSendMail = new SagentMail();
        	try {
    			tSendMail.SendMail();
    			if(!"".equals(mAddress)){
    				tSendMail.setToAddress(mAddress);
    			}
    			if(!"".equals(mCcAddress)){
    				tSendMail.setCcAddress(mCcAddress);
    			}
    			if(!"".equals(mBccAddress)){
    				tSendMail.setBccAddress(mBccAddress);
    			}
    		} catch (Exception ex) {
    			System.out.println("邮件配置失败："+ex.toString());
    		}
    		try {
    			tSendMail.setFile(mZipFilePath);
    			tSendMail.send("<b>Infee提数结果请见附件</b>", "理赔批处理-Infee提数");
    		} catch (Exception ex) {
    			System.out.println("邮件发送失败："+ex.toString());
    			ex.printStackTrace();
    		}
    	}
    	
    }
    
    /**
     * 通过SQL语句查询结果，并写入txt文件
     *
     */
    private void getDataResult()
    {
        try
        {
            String tSQL ="select distinct a.caseno 理赔号,  b.HospitalName  医院名称, b.HospStartDate 入院日期,                b.HospEndDate   出院日期,                                (select sum(SumFee)                   from llfeemain                  where MainFeeNo = b.MainFeeNo) 总金额,                                (select sum(Fee)                   from LLCaseReceipt                  where MainFeeNo = b.MainFeeNo                    and FeeItemCode in ('101', '202')) 西药费,                                (select sum(Fee)                   from LLCaseReceipt                  where MainFeeNo = b.MainFeeNo                    and FeeItemCode in ('102', '203')) 中成药,                                (select sum(Fee)                   from LLCaseReceipt                  where MainFeeNo = b.MainFeeNo                    and FeeItemCode in ('204', '121')) 中草药,                                (select sum(Fee)                   from LLCaseReceipt                  where MainFeeNo = b.MainFeeNo                    and FeeItemCode in ('107', '205')) 检查费,                                (select sum(Fee)                   from LLCaseReceipt                  where MainFeeNo = b.MainFeeNo                    and FeeItemCode in ('104', '206')) 治疗费,                                (select sum(Fee)                   from LLCaseReceipt                  where MainFeeNo = b.MainFeeNo                    and FeeItemCode in ('112', '209')) 化验费  from llcase a, llfeemain b where a.caseno = b.caseno   and b.feetype = '2'   and a.endcasedate >= current date - 1 MONTHS   and a.endcasedate <= current date - 1 DAYS order by a.caseno with ur";
            int start = 1;
            int nCount = 200;
            RSWrapper rswrapper = new RSWrapper();
            rswrapper.prepareData(null, tSQL);
            SSRS tSSRS = null;
            do
            {
                tSSRS = rswrapper.getSSRS();

                if (tSSRS.getMaxRow() <= 0)
                {
                	break;
                }
                
                String tempString = rswrapper.encode();
                String[] tempStringArr = tempString.split("\\^");
                
//              此处必须使用特殊构造器，这样可以持续向文件中写入内容，缺点是必须进行文件分类，否则该文件会持续增大
                FileOutputStream  tFileOutputStream = new FileOutputStream(mURL+mZipDocFileName,true);
                
                OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                        tFileOutputStream, "GBK");
                mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    String t = tempStringArr[i].replaceAll("\\|", ",");
                    String[] tArr = t.split("\\,");
                
                    for (int j = 0; j < tArr.length - 1; j++)
                    {
                        mBufferedWriter.write(tArr[j] + ",");
                    }
                    mBufferedWriter.write(tArr[tArr.length - 1] );
                    mBufferedWriter.newLine();
                    mBufferedWriter.flush();
                }
                
                mBufferedWriter.close();
                tOutputStreamWriter.close();
                start += nCount;
            }
            while (tSSRS.getMaxRow() > 0);
        } catch (IOException ex2)
        {
        	this.mErrors.addOneError("数据提取或写文件失败：" +ex2.getStackTrace() );
        	System.out.println(ex2.getStackTrace());
        }
    }
    
    /**
     * 将给定目录下的文件打包为压缩包
     *
     */
    private void getZipFile()
    {
    	 
        FileOutputStream f = null;
        mZipFilePath = mURL+"InfeeDataExtraction.zip";
    	System.out.println("压缩路径名：" + mZipFilePath);
        ZipOutputStream out = null;
        String tDocFileName = mURL + mZipDocFileName; //文件绝对路径 + 文件名
        try
        {
            f = new FileOutputStream(mZipFilePath);
            System.out.println("f：" + f);
        }
        catch (FileNotFoundException ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("无法创建zip文件");
            System.out.println(mErrors.getLastError());
            return ;
        }
        out = new ZipOutputStream(new BufferedOutputStream(f));
        System.out.println("out：" + out);
        mFlag = compressIntoZip(mZipFilePath, out, mZipDocFileName,tDocFileName);
		   
        try {
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		    mErrors.addOneError("流关闭异常");
		    System.out.println(mErrors.getLastError());
		    return ;
		} 	
    }
    
    /**
     * 添加到压缩文件
     * @param zipfilepath String
     * @param out ZipOutputStream
     * @param tZipDocFileName String：zip文件名
     * @param tDocFileName String：待压缩的文件路径名
     * @return int：1：成功、0：失败，但不中断整个程序、-1：失败，中断程序
     */
    private  int compressIntoZip(String zipfilepath, ZipOutputStream out,
            String tZipDocFileName, String tDocFileName)
    {
    System.out.println("zipfilepath:" + zipfilepath);
    System.out.println("out:" + out);
    System.out.println("tZipDocFileName:" + tZipDocFileName);
    System.out.println("tDocFileName待压缩文件压缩路径:" + tDocFileName);

        try
        {
            FileInputStream fi = new FileInputStream(tDocFileName);
            BufferedInputStream origin = new BufferedInputStream(fi, BUFFER);

            ZipEntry entry = new ZipEntry(tZipDocFileName);
            out.putNextEntry(entry);

            int count;
            byte data[] = new byte[BUFFER];
            while ((count = origin.read(data, 0, BUFFER)) != -1)
            {
                out.write(data, 0, count);
            }
            origin.close();
        }
        catch (FileNotFoundException ex)
        {
            ex.printStackTrace();
            this.mErrors.addOneError("找不到待压缩文件压缩路径" + tDocFileName);
            System.out.println("找不到待压缩文件压缩路径" + tDocFileName);

            //某些页找不到可不用单独处理
            return 0;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "BPOSentScanBL";
            tError.functionName = "dealData";
            tError.errorMessage = "无法生成压缩包";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            deleteFile(zipfilepath);
            return -1;
        }
        return 1;
    }
    
    /**
     * 删除生成的ZIP文件和XML文件
     * @param cFilePath String：完整文件名
     */
    private boolean deleteFile(String cFilePath)
    {
        File f = new File(cFilePath);
        if (!f.exists())
        {
            CError tError = new CError();
            tError.moduleName = "MonthDataCatch";
            tError.functionName = "deleteFile";
            tError.errorMessage = "没有找到需要删除的文件";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        f.delete();
        return true;
    }

}
