package com.sinosoft.lis.llcase;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLAccountReturnBL {
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private MMap map = new MMap();
    private String mRemark="";

    private GlobalInput mGlobalInput = new GlobalInput();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private LLCaseBackSchema mLLCaseBackSchema = new LLCaseBackSchema();
    ExeSQL tExeSQL = new ExeSQL();

    public LLAccountReturnBL() {
    }

    public static void main(String[] args) {
        LLCaseSet tLLCaseSet = new LLCaseSet();
        LLAccountReturnBL tLLAccountReturnBL   = new LLAccountReturnBL();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        LLCaseBackSchema tLLCaseBackSchema = new LLCaseBackSchema();
        tLLCaseSchema.setCaseNo("C1100060908000004");


        tLLCaseBackSchema.setCaseNo("C1100060908000004");
        tLLCaseBackSchema.setReason("2");
        tLLCaseBackSchema.setRemark("帐单录入错误");
         GlobalInput tG = new GlobalInput();
         tG.ManageCom="86";
         tG.Operator="cm0002";

        VData tVData = new VData();
        tVData.add(tLLCaseSchema);
         tVData.add(tLLCaseBackSchema);
        tVData.add(tG);

        tLLAccountReturnBL.submitData(tVData, "RETURN");


    }

    public VData getResult()
{
      return mResult;
}


    public boolean submitData(VData cInputData, String cOperate)
{
    //首先将数据在本类中做一个备份
    mInputData = (VData) cInputData.clone();

    //得到输入数据
    if (!getInputData())
    {
        return false;
    }
    //检查数据合法性
    if (!checkInputData())
    {
        return false;
    }

    //进行业务处理
    if (!dealData())
    {
        return false;
    }

    //准备往后台的数据
    if (!prepareOutputData())
    {
        return false;
    }



    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, cOperate))
    {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        CError tError = new CError();
        tError.moduleName = "CaseTransferBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";
        this.mErrors.addOneError(tError);
        return false;
    }
    mInputData = null;

    return true;
}

private boolean getInputData()
{
    System.out.println("getInputData()...");

    mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
            "GlobalInput", 0);

    mLLCaseSchema = (LLCaseSchema) mInputData.getObjectByObjectName(
            "LLCaseSchema", 0);
    mLLCaseBackSchema = (LLCaseBackSchema) mInputData.getObjectByObjectName(
            "LLCaseBackSchema", 0);

    LLCaseDB tLLCaseDB = new LLCaseDB();
    tLLCaseDB.setCaseNo(mLLCaseSchema.getCaseNo());
    tLLCaseDB.getInfo();
    mLLCaseSchema = tLLCaseDB.getSchema();


    return true;
    }


    private boolean prepareOutputData()
   {
       System.out.println("prepareOutputData()...");

       try
       {
           mInputData.clear();
           mInputData.add(map);

       }
       catch (Exception ex)
       {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "ICaseCureBL";
           tError.functionName = "prepareOutputData";
           tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
           this.mErrors.addOneError(tError);
           return false;
       }
       return true;
   }


   private boolean checkInputData()
      {
          if (!mLLCaseSchema.getRgtState().equals("01") && !mLLCaseSchema.getRgtState().equals("02")) {
              CError.buildErr(this, "当前状态下不能回退账单");
              return false;
          }
          String sql1 = "select upusercode from llclaimuser where usercode="
                        +"(select claimer from llcase where caseno='"+mLLCaseSchema.getCaseNo()+"')";
          String UpUser = tExeSQL.getOneValue(sql1);
          String sql2 = "select handler from llcase where caseno='" + mLLCaseSchema.getCaseNo() + "')";
          String Handler = tExeSQL.getOneValue(sql2);

          if (!(mGlobalInput.Operator.equals(UpUser)||mGlobalInput.Operator.equals(Handler))) {
              CError.buildErr(this, "非案件处理人或录入人员上级，不能回退账单!");
              return false;
          }

          if (!mLLCaseSchema.getReceiptFlag().equals("1")) {
              CError.buildErr(this, "未录入账单或账单录入中");
              return false;
          }
          return true;
    }

    private boolean dealData(){

        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mLLCaseSchema.getCaseNo());
        tLLCaseDB.getInfo();
        mLLCaseSchema = tLLCaseDB.getSchema();
        mLLCaseSchema.setReceiptFlag("0");
        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        String CaseBackNo = PubFun1.CreateMaxNo("CaseBack", tLimit);
        mLLCaseBackSchema.setCaseBackNo(CaseBackNo);
        mLLCaseBackSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        mLLCaseBackSchema.setRgtNo(mLLCaseSchema.getRgtNo());
        mLLCaseBackSchema.setBeforState(mLLCaseSchema.getRgtState());
        mLLCaseBackSchema.setAfterState(mLLCaseSchema.getRgtState());
        mLLCaseBackSchema.setOHandler(mLLCaseSchema.getHandler());
        mLLCaseBackSchema.setNHandler(mLLCaseSchema.getHandler());
        mLLCaseBackSchema.setMngCom(mGlobalInput.ManageCom);
        mLLCaseBackSchema.setOperator(mGlobalInput.Operator);
        mLLCaseBackSchema.setMakeDate(PubFun.getCurrentDate());
        mLLCaseBackSchema.setMakeTime(PubFun.getCurrentTime());
        mLLCaseBackSchema.setModifyDate(PubFun.getCurrentDate());
        mLLCaseBackSchema.setModifyTime(PubFun.getCurrentTime());
        mLLCaseBackSchema.setBackType("8");



        map.put(mLLCaseSchema, "UPDATE");
        map.put(mLLCaseBackSchema, "INSERT");

        this.mResult.add(map);
        return true;
    }



}
