package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 案件－立案－分案保单明细查询逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @version 1.0
 */

public class CaseReceiptQueryBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  /**要查询的表命要进行改动  */
  private LLCaseReceiptBL mLLCaseReceiptBL = new LLCaseReceiptBL();
  private LLCaseReceiptBLSet mLLCaseReceiptBLSet = new LLCaseReceiptBLSet();
  public CaseReceiptQueryBL() {}

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;

    //数据查询业务处理
    if (cOperate.equals("QUERY||MAIN"))
    {
      if(!queryData())
	return false;
    }

    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    // 暂交费查询条件
    if (mOperate.equals("QUERY||MAIN"))
    {
      mLLCaseReceiptBL.setSchema((LLCaseReceiptSchema)cInputData.getObjectByObjectName("LLCaseReceiptSchema",0));
    }
    return true;
  }

  /**
   * 查询符合条件的暂交费信息
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
   //修改表名
  private boolean queryData()
  {

    LLCaseReceiptDB tLLCaseReceiptDB = new LLCaseReceiptDB();
    tLLCaseReceiptDB.setCaseNo(mLLCaseReceiptBL.getCaseNo());
    mLLCaseReceiptBLSet.set(tLLCaseReceiptDB.query());
    if (tLLCaseReceiptDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLCaseReceiptDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "LLCaseReceiptBL";
      tError.functionName = "queryData";
      tError.errorMessage = "保单查询失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

	mResult.clear();
	      mResult.add(mLLCaseReceiptBLSet);
	return true;
  }
  }