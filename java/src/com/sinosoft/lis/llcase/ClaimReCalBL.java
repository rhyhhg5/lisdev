package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 理赔重算类</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author Xx
 * @version 1.0
 */
public class ClaimReCalBL
{
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private String mOperate;

    private String mClmNo = "";
    private String mCaseNo = "";
    private String mRgtNo = "";

    //用于理赔状态的判断
    private String mGiveType = "";
    private String backMsg = "";

    /**  */
    private LLClaimSchema mLLClaimSchema = new LLClaimSchema();
    private LLClaimPolicySet mLLClaimPolicySet = new LLClaimPolicySet();
    private LLClaimPolicySet deleteLLClaimPolicySet = new LLClaimPolicySet();
    private LLClaimPolicySet saveLLClaimPolicySet = new LLClaimPolicySet();
    private LLClaimDetailSet mLLClaimDetailSet = new LLClaimDetailSet();
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet();
    private LCInsureAccTraceSchema mLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
    private LLCaseSchema mLLCaseSchema = null;

    private GlobalInput mG = new GlobalInput();
    
    private String mPolNo;
    private String mCaseRelaNo;
    private String mAccDate;
    private String mEndDate;
    private String sRiskcode;

    //保留赔付结论
    private MMap tmpMap = new MMap();
    public ClaimReCalBL()
    {}


    public String getBackMsg(){
        return backMsg;
    }
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        System.out.println("BL begin");
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;

        // 数据操作业务处理
        if (cOperate.equals("SAVE"))
        {
            if (!checkData())
                return false;
            if (!dealData())
                return false;
            if (!prepareOutputData())
                return false;
            System.out.print("ClaimSaveBL prepare data end");
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }


    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData()
    {
        //准备给后台的数据
        System.out.println("---dealData---");
        double tSumCalPay = 0;
        double tSumRealPay = 0;
        int n = 0;

        LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
        //赔付明细
        n = mLLClaimDetailSet.size();
        double tempStandPay = 0;
        double tempRealPay = 0;
        System.out.println("mLLClaimDetailSet.size:" + n);
        for (int i = 1; i <= n; i++) {
            LLClaimDetailSchema tLLClaimDetailSchema = mLLClaimDetailSet.get(i);
            System.out.println("提交LLClaimDetailSet.........." + i);

            //查询赔案明细
            tLLClaimDetailDB.setClmNo(tLLClaimDetailSchema.getClmNo());
            tLLClaimDetailDB.setPolNo(tLLClaimDetailSchema.getPolNo());
            this.mPolNo=tLLClaimDetailSchema.getPolNo(); // add new 
            tLLClaimDetailDB.setGetDutyKind(tLLClaimDetailSchema.getGetDutyKind());
            tLLClaimDetailDB.setGetDutyCode(tLLClaimDetailSchema.getGetDutyCode());
            tLLClaimDetailDB.setCaseNo(tLLClaimDetailSchema.getCaseNo());
            tLLClaimDetailDB.setCaseRelaNo(tLLClaimDetailSchema.getCaseRelaNo());
            this.mCaseRelaNo=tLLClaimDetailSchema.getCaseRelaNo(); // add new
            if (!tLLClaimDetailDB.getInfo()) {
                CError.buildErr(this, "赔付明细查询错误,请先做理算");
                return false;
            }
            LLClaimDetailSchema qLLClaimDetailSchema = tLLClaimDetailDB.getSchema();
            double agetrate = tLLClaimDetailSchema.getOutDutyRate();
            double agetlimit = tLLClaimDetailSchema.getOutDutyAmnt();
            double aclmmoney = tLLClaimDetailSchema.getClaimMoney();
            System.out.println(agetrate);
            System.out.println(agetlimit);
            System.out.println(aclmmoney);
            qLLClaimDetailSchema.setOutDutyAmnt(agetlimit);
            qLLClaimDetailSchema.setOutDutyRate(agetrate);
            qLLClaimDetailSchema.setClaimMoney(aclmmoney);
            double tpaymoney = (aclmmoney - agetlimit) * agetrate;
            if(tpaymoney<0){
                tpaymoney=0;
                qLLClaimDetailSchema.setOutDutyAmnt(aclmmoney);
            }
            if (tpaymoney > 0&&tpaymoney<0.01) {
                tpaymoney = 0.01;
            }
                        // 新增地点
            if(!check())
            {
            	tpaymoney=0.00;
            	
            }
            System.out.println("................"+tpaymoney);
            tpaymoney = Arith.round(tpaymoney,4);
            tpaymoney = Arith.round(tpaymoney,2);
            System.out.println("****************"+tpaymoney);
            LMRiskDB tLMRiskDB = new LMRiskDB();
            tLMRiskDB.setRiskCode(qLLClaimDetailSchema.getRiskCode());
            String tpolno = qLLClaimDetailSchema.getPolNo();
            String insuaccflag = "";
            if(tLMRiskDB.getInfo())
                insuaccflag += tLMRiskDB.getInsuAccFlag();
            double tPayafter = tpaymoney;
//            #2668
            if(insuaccflag.equals("Y")&& !"1".equals(sRiskcode)){
                double caledpay = getAccPay(tpolno);
                GetAccAmnt tgetaccamnt = new GetAccAmnt();
                tPayafter = tgetaccamnt.getAccPay(tpolno,mCaseNo,caledpay+tPayafter)-caledpay;
            }
            double declinemoney = tpaymoney - tPayafter;
            qLLClaimDetailSchema.setDeclineAmnt(declinemoney);
            qLLClaimDetailSchema.setStandPay(tpaymoney);
            qLLClaimDetailSchema.setRealPay(tPayafter);
            qLLClaimDetailSchema.setOperator(mG.Operator);
            qLLClaimDetailSchema.setGiveReason("");
            qLLClaimDetailSchema.setGiveReasonDesc("");
            qLLClaimDetailSchema.setGiveType("");
            qLLClaimDetailSchema.setGiveTypeDesc("");
            qLLClaimDetailSchema.setModifyDate(PubFun.getCurrentDate());
            qLLClaimDetailSchema.setModifyTime(PubFun.getCurrentTime());
            mLLClaimDetailSet.set(i, qLLClaimDetailSchema);

            tempStandPay += qLLClaimDetailSchema.getStandPay();
            tempRealPay += qLLClaimDetailSchema.getRealPay();
            System.out.println("tempRealPay: " + tempStandPay);
            System.out.println("qLLClaimDetailSchema.getRealPay(): "
                               + qLLClaimDetailSchema.getRealPay());
        }
        tempRealPay = Arith.round(tempRealPay, 4);
        tempRealPay = Arith.round(tempRealPay, 2);
        mLLClaimSchema.setRealPay(tempRealPay);
        System.out.println("tempRealPay : " + tempRealPay);
        tempStandPay = Arith.round(tempStandPay, 2);
        mLLClaimSchema.setStandPay(tempStandPay);
        mLLClaimSchema.setGiveType("");
        mLLClaimSchema.setGiveTypeDesc("");
        System.out.println("tempStandPay : " + tempStandPay);
        tmpMap.put(mLLClaimSchema, "DELETE&INSERT");

        //处理保单赔付明细LLClaimPolicy
        if (mLLClaimPolicySet != null) {
            n = mLLClaimPolicySet.size();
        }
        for (int i = 1; i <= n; i++) {
            LLClaimPolicySchema qLLClaimPolicySchema = mLLClaimPolicySet.get(i);
            double prealpay = 0.0;
            double pstandpay = 0.0;
            boolean IsDetail = false;
            for (int k = 1; k <= mLLClaimDetailSet.size(); k++) {
                LLClaimDetailSchema ttmLLClaimDetailSChema = mLLClaimDetailSet.
                        get(k);
                if (qLLClaimPolicySchema.getGetDutyKind().equals(
                        ttmLLClaimDetailSChema.getGetDutyKind())
                    &&
                    qLLClaimPolicySchema.getCaseRelaNo().equals(ttmLLClaimDetailSChema.
                        getCaseRelaNo())
                    &&
                    qLLClaimPolicySchema.getPolNo().equals(ttmLLClaimDetailSChema.
                        getPolNo())) {
                    prealpay += ttmLLClaimDetailSChema.getRealPay();
                    pstandpay += ttmLLClaimDetailSChema.getStandPay();
                    IsDetail = true;
                }
            }
            if (!IsDetail) {
                deleteLLClaimPolicySet.add(qLLClaimPolicySchema);
                continue;
            }
            prealpay = Arith.round(prealpay, 2);
            pstandpay = Arith.round(pstandpay, 2);
            qLLClaimPolicySchema.setRealPay(prealpay);
            qLLClaimPolicySchema.setStandPay(pstandpay);
            qLLClaimPolicySchema.setGiveReason("");
            qLLClaimPolicySchema.setGiveReasonDesc("");
            qLLClaimPolicySchema.setGiveType("");
            qLLClaimPolicySchema.setGiveTypeDesc("");
            qLLClaimPolicySchema.setModifyDate(PubFun.getCurrentDate());
            qLLClaimPolicySchema.setModifyTime(PubFun.getCurrentTime());

            saveLLClaimPolicySet.add(qLLClaimPolicySchema);

            tSumCalPay = tSumCalPay + qLLClaimPolicySchema.getStandPay();
            tSumRealPay = tSumRealPay + qLLClaimPolicySchema.getRealPay();
        }
        n = 0;
        System.out.println("mLLClaimPolicySet.size()" + mLLClaimPolicySet.size());

        String delsql = "delete from llclaimdetail where clmno='" + this.mClmNo +
                        "'";
        tmpMap.put(delsql, "DELETE");
        System.out.println("mLLClaimPolicySet.size()" + mLLClaimPolicySet.size());
        tmpMap.put(this.deleteLLClaimPolicySet, "DELETE");
        tmpMap.put(this.saveLLClaimPolicySet, "DELETE&INSERT");
        tmpMap.put(this.mLLClaimDetailSet, "DELETE&INSERT");
//        #2668
        if(!"1".equals(sRiskcode)){
        getInsuAcc();
        }
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mCaseNo);
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        if(tLLCaseDB.getInfo()){
            tLLCaseSchema = tLLCaseDB.getSchema();
            tLLCaseSchema.setCalFlag("2");
            tmpMap.put(tLLCaseSchema,"UPDATE");
        }
        this.mResult.add(tmpMap);
        return true;
    }

    private double getAccPay(String aPolNo){
        double accpay = 0;
        for(int i=1;i<=mLLClaimDetailSet.size();i++){
            LLClaimDetailSchema tclmdetail = mLLClaimDetailSet.get(i);
            if(tclmdetail.getPolNo().equals(aPolNo))
                accpay += tclmdetail.getRealPay();
        }
        return accpay;
    }
    //帐户存取轨迹保存
    private boolean getInsuAcc() {
        String tsql = "select a.polno from lcpol a,lmrisk b where a.riskcode=b.riskcode "
                      +" and b.insuaccflag = 'Y' and a.insuredno = '"
                      +mLLCaseSchema.getCustomerNo()+"'";
        ExeSQL tesql = new ExeSQL();
        SSRS tss = tesql.execSQL(tsql);
        
        LCInsureAccTraceSet sinsuacctraceset = new LCInsureAccTraceSet();
        for(int i=1;i<=tss.getMaxRow();i++){
            String tPolNo = tss.GetText(i, 1);
            double totalclaim = 0;
            for(int j=1;j<=mLLClaimDetailSet.size();j++){
                LLClaimDetailSchema tclmdetail = mLLClaimDetailSet.get(j);
                if(tclmdetail.getPolNo().equals(tPolNo))
                    totalclaim += tclmdetail.getStandPay();
            }
            totalclaim = Arith.round(totalclaim,2);
            if (totalclaim > 0) {
            	GetAccAmnt tgetaccamnt = new GetAccAmnt();
                tgetaccamnt.getAccPay(tPolNo, mCaseNo, totalclaim);
                sinsuacctraceset.add(tgetaccamnt.getTrace());
                backMsg += tgetaccamnt.getAccMsg();
            }
        }
        tmpMap.put(sinsuacctraceset, "DELETE&INSERT");
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        System.out.println("getInputData......");
        System.out.println("mOperate===" + mOperate);
        if (mOperate.equals("SAVE") )
        {
            mLLClaimDetailSet = (LLClaimDetailSet) cInputData.
                                getObjectByObjectName("LLClaimDetailSet", 0);
            mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0));
            if (mLLClaimDetailSet != null && mLLClaimDetailSet.size() > 0)
            {
                this.mClmNo = mLLClaimDetailSet.get(1).getClmNo();
                this.mCaseNo = mLLClaimDetailSet.get(1).getCaseNo();
                this.mRgtNo = mLLClaimDetailSet.get(1).getRgtNo();
            }
            if ("".equals(this.mClmNo))
            {
                CError.buildErr(this, "请先进行理赔计算！");
                return false;
            }
            LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
            tLLClaimPolicyDB.setClmNo(this.mClmNo);
            mLLClaimPolicySet = tLLClaimPolicyDB.query();
            if (mLLClaimPolicySet == null || mLLClaimPolicySet.size() < 0)
            {
                CError.buildErr(this, "赔案明细查询失败");
                return false;
            }else{
            	 for (int i = 1; i <= mLLClaimPolicySet.size(); i++) 
            	 {
            		 LLClaimPolicySchema aLLClaimPolicySchema = new LLClaimPolicySchema();
            		 aLLClaimPolicySchema = mLLClaimPolicySet.get(i);
//            		 #2668
            		 String SQL = "select 1 from lmriskapp where taxoptimal='Y' and  riskcode ='"+aLLClaimPolicySchema.getRiskCode()+"'";
            	        ExeSQL tExeSQL = new ExeSQL();
            	        sRiskcode = tExeSQL.getOneValue(SQL);
            		 if(LLCaseCommon.chenkWN(aLLClaimPolicySchema.getRiskCode()) && !"1".equals(sRiskcode))
            		 {
            			 CError.buildErr(this, "万能险不能进行重算!");
                         return false;
            		 }
            	 }
            }
        }
        return true;
    }

    /**
     * 判断赔案是否已经核赔或结案
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkData()
    {
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setClmNo(this.mClmNo);
        if (!tLLClaimDB.getInfo())
        {
            CError.buildErr(this, "赔案信息查找错误");
            return false;
        }
        this.mLLClaimSchema = tLLClaimDB.getSchema();

        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if (!tLLRegisterDB.getInfo())
        {
            CError.buildErr(this, "立案信息查询失败");
            return false;
        }
        mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(this.mCaseNo);
        if (!tLLCaseDB.getInfo())
        {
            CError.buildErr(this, "案件信息查询错误");
            return false;
        }
        
        if (!LLCaseCommon.checkHospCaseState(mCaseNo)) {
            CError.buildErr(this, "医保通案件待确认，不能进行处理");
            return false;
        }
        
        this.mLLCaseSchema = tLLCaseDB.getSchema();
        if (!"03".equals(this.mLLCaseSchema.getRgtState())
            && !"04".equals(this.mLLCaseSchema.getRgtState())
            && !"02".equals(this.mLLCaseSchema.getRgtState())
            && !"06".equals(this.mLLCaseSchema.getRgtState()))
        {
            CError.buildErr(this, "该案件状态不能重算");
            return false;
        }
        return true;
    }

    /**准备提交后台的数据
     * prepareOutputData
     */

    private boolean prepareOutputData() {
        System.out.print("ClaimSaveBL prepare data begin");
        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(this.mResult, null))
        {
            CError.buildErr(this, "数据保存失败");
            return false;
        }
        return true;
    }

    private String getGiveDesc(String agivetype){
        String agivedesc="";
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("llclaimdecision");
        tLDCodeDB.setCode(agivetype);
        if(tLDCodeDB.getInfo())
            agivedesc = tLDCodeDB.getCodeName();
        return agivedesc;
    }

    public static void main(String[] args)
    {
        String aPolNo = "21000103034";
        String contNO = "2300073774";
        String aRgtNo = "P1100060804000034";
        String aRiskCode = "160306";
        String aCaseNo = "C1100060804000109";
        String aGetDutyKind = "200";
        String aClmNo = "52000002038";

        LLClaimDetailSet aLLClaimDetailSet = new LLClaimDetailSet();
        LLClaimDetailSchema aLLClaimDetailSchema = new LLClaimDetailSchema();
        aLLClaimDetailSchema.setRgtNo(aRgtNo);
        aLLClaimDetailSchema.setContNo("2300163944");
        aLLClaimDetailSchema.setGetDutyCode("605208");
        aLLClaimDetailSchema.setGetDutyKind(aGetDutyKind);
        aLLClaimDetailSchema.setStandPay(0);
        aLLClaimDetailSchema.setRealPay(0);
        aLLClaimDetailSchema.setPolNo("21000223472");
        aLLClaimDetailSchema.setClmNo(aClmNo);
        aLLClaimDetailSchema.setDutyCode("615009");
        aLLClaimDetailSchema.setGiveType("4");
        aLLClaimDetailSchema.setGiveTypeDesc("3");
        aLLClaimDetailSchema.setGiveReason("3");
        aLLClaimDetailSchema.setGiveReasonDesc("3");
        aLLClaimDetailSchema.setCaseRelaNo("86000000002119");
        aLLClaimDetailSchema.setCaseNo(aCaseNo);
        aLLClaimDetailSchema.setDeclineNo("1");
        aLLClaimDetailSchema.setApproveAmnt(1.5);
        aLLClaimDetailSet.add(aLLClaimDetailSchema);
        aLLClaimDetailSchema = new LLClaimDetailSchema();

        GlobalInput tG = new GlobalInput();
        tG.Operator = "cm0002";
        tG.ManageCom = "86";

        VData aVData = new VData();

        aVData.addElement(aLLClaimDetailSet);
        aVData.addElement(tG);
        ClaimReCalBL tClaimReCalBL = new ClaimReCalBL();
        tClaimReCalBL.submitData(aVData, "SAVE");
    }
    
      /* 查询保单信息
     * @return boolean
     */
    private boolean check()
    {
    	if(!getSubReportSchema()) return false;
        // 续保新增代码,LCPolBL同时查C表和B表
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setPolNo(mPolNo);
        if (!tLCPolBL.getInfo()) {
            CError.buildErr(this, "险种保单信息查询错误!");
            return false;
        }

        //保全提供的保单失效日期
        mEndDate = CommonBL.getPolInvalidate(mPolNo);
        if (mEndDate.equals(null)||mEndDate.equals("")||mEndDate.equals("null")){
            CError.buildErr(this, mPolNo+"险种保单失效日查询失败!");
            return false;
        }

        int validays = PubFun.calInterval(tLCPolBL.getCValiDate(),
                                          mAccDate, "D");
        System.out.println(validays);
        int remdays = PubFun.calInterval(mAccDate,
                                         mEndDate, "D");
        String SQL0 = "select riskcode from lmriskapp where risktype3='7' and risktype <>'M' and riskcode ='"+tLCPolBL.getRiskCode()+"'";
        ExeSQL tExeSQL = new ExeSQL();
        String riskcode = tExeSQL.getOneValue(SQL0);

        if ((validays < 0 || remdays <= 0)&&StrTool.cTrim(riskcode).equals("")) {
            String errmsg = "保单" + tLCPolBL.getPolNo() + "的生效日期："
                            + tLCPolBL.getCValiDate() + ",失效日期："
                            + mEndDate + "；保单在出险日期："
                            + mAccDate + " 无效";
           System.out.println("特需医疗险种关于事件时间的校验结果："+errmsg);
            return false;
    }	
    	return true;
    }
    /**
     * getSubReportSchema
     *
     * @param caseRelaNo String
     * @return LLSubReportSchema
     */
    private boolean getSubReportSchema() {
		String srSQL = "select a.accdate from llsubreport a,llcaserela b "
				+ " where a.subrptno=b.subrptno and b.caserelano='"
				+ mCaseRelaNo + "' and b.caseno='" + mCaseNo + "'";
        ExeSQL texesql = new ExeSQL();
        mAccDate = texesql.getOneValue(srSQL);
        if(mAccDate.equals("")||mAccDate.equals("null")){
            CError.buildErr(this,"事件信息查询失败!");
            System.out.println("CaseRelaNo"+mCaseRelaNo+"对应的事件查询失败!");
            return false;
        }
        return true;
    }
}
