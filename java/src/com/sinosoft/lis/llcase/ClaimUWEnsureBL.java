package com.sinosoft.lis.llcase;

import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 理赔赔案核赔确认业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author：zhangtao
 * @version：1.0
 * @CreateDate：2005-02-20
 */
public class ClaimUWEnsureBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  /** 往后面传输数据的容器 */
  private VData mInputData;

  /** 往界面传输数据的容器 */
  private VData mResult = new VData();


  /** 数据操作字符串 */

  private String mOperate;

  private GlobalInput mGlobalInput = new GlobalInput();


  /** 业务处理相关变量 */
  /** 赔案名细 */
  //案件赔案信息
  private LLClaimSchema mLLClaimSchema = new LLClaimSchema();

  //案件立案信息表
  private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();

  //保单赔案
  private LLClaimPolicySet mLLClaimPolicySet = new LLClaimPolicySet();

  //保单赔案明细表
  private LLClaimDetailSet mLLClaimDetailSet = new LLClaimDetailSet();

  //保单核赔主表
  private LLClaimUnderwriteSet mLLClaimUnderwriteSet = new
          LLClaimUnderwriteSet();

  //案件核赔主表
  private LLClaimUWMainSchema mLLClaimUWMainSchema = new
          LLClaimUWMainSchema();

  //上次案件核赔表
  private LLClaimUWMainSchema mLastLLClaimUWMainSchema = new
          LLClaimUWMainSchema();

  //案件核赔履历表
  private LLClaimUWMDetailSchema mLLClaimUWMDetailSchema = new
          LLClaimUWMDetailSchema();


  /**用户表信息 */
  //private LDUserSchema mLDUserSchema = new LDUserSchema();
  private LLClaimUserSchema mLLClaimUserSchema = new LLClaimUserSchema();

  //是否有拒赔的保单标志
  private String mDeclineFlag = "N";

  public ClaimUWEnsureBL()
  {

  }

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   * @param: cOperate 数据操作
   * @return: boolean
   */
  public boolean submitData(VData cInputData, String cOperate)
  {

      //将操作数据拷贝到本类中
      mInputData = (VData) cInputData.clone();

      //得到外部传入的数据,将数据备份到本类中
      if (!getInputData(cInputData))
      {
          return false;
      }

System.out.println("after getInputData");

      //根据赔案信息查询相关信息
      if (!getBaseData())
      {
          return false;
      }

System.out.println("after getBaseData");

      //校验数据
      if (!checkApprove())
      {
          return false;
      }

System.out.println("after checkApprove");

      //准备给后台的数据
      if (!prepareOutputData())
      {
          return false;
      }

System.out.println("after prepareOutputData");

      //数据提交
      ClaimUWEnsureBLS tClaimUWEnsureBLS = new ClaimUWEnsureBLS();
      if (!tClaimUWEnsureBLS.submitData(mInputData, mOperate))
      {
          // @@错误处理
          this.mErrors.copyAllErrors(tClaimUWEnsureBLS.mErrors);
          CError tError = new CError();
          tError.moduleName = "ClaimUWEnsureBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors.addOneError(tError);
          return false;
      }

      return true;
  }

  /**
   * 从输入数据中得到所有对象
   */
  private boolean getInputData(VData cInputData)
  {
    mGlobalInput.setSchema(
            (GlobalInput)cInputData.
            getObjectByObjectName("GlobalInput",0));

    mOperate = "INSERT";

    mLLClaimUWMainSchema =
            (LLClaimUWMainSchema)cInputData.
            getObjectByObjectName("LLClaimUWMainSchema",0);

    return true;
  }

  private boolean getBaseData()
  {
      //查询赔案主表LLClaim
      LLClaimDB tLLClaimDB = new LLClaimDB();
      tLLClaimDB.setClmNo(mLLClaimUWMainSchema.getClmNo());
      if (!tLLClaimDB.getInfo())
      {
          // @@错误处理
          this.mErrors.copyAllErrors(tLLClaimDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ClaimUWEnsureBL";
          tError.functionName = "getBaseData";
          tError.errorMessage = "赔案表LLClaim查询失败!";
          this.mErrors.addOneError(tError);
          return false;
      }
      mLLClaimSchema.setSchema(tLLClaimDB.getSchema());

      //查询立案表LLRegister
      LLRegisterDB tLLRegisterDB = new LLRegisterDB();
      tLLRegisterDB.setRgtNo(mLLClaimSchema.getRgtNo());
      if (!tLLRegisterDB.getInfo())
      {
          this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ClaimUWEnsureBL";
          tError.functionName = "getBaseData";
          tError.errorMessage = "立案表LLRegister查询失败!";
          this.mErrors.addOneError(tError);
          return false;
      }
      //对立案的赔案状态进行判断
      if (tLLRegisterDB.getClmState() != null &&
          tLLRegisterDB.getClmState().equals("5"))
      {
          CError tError = new CError();
          tError.moduleName = "ClaimUWEnsureBL";
          tError.functionName = "getBaseData";
          tError.errorMessage = "案件已经提起调查，不能作核赔确认!";
          this.mErrors.addOneError(tError);
          return false;
      }
      mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());

      //查询保单赔案表LLClaimPolicy
      LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
      tLLClaimPolicyDB.setClmNo(mLLClaimSchema.getClmNo());
      mLLClaimPolicySet.set(tLLClaimPolicyDB.query());
      if (tLLClaimPolicyDB.mErrors.needDealError() == true)
      {
          // @@错误处理
          this.mErrors.copyAllErrors(tLLClaimPolicyDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ClaimUWEnsureBL";
          tError.functionName = "getBaseData";
          tError.errorMessage = "保单赔案表LLClaimPolicy查询失败!";
          this.mErrors.addOneError(tError);
          return false;
      }

      if (mLLClaimSchema.getClmState().equals("2")) //核赔成功
      {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "ClaimUWEnsureBL";
          tError.functionName = "checkApprove";
          tError.errorMessage = "已经核赔成功!";
          this.mErrors.addOneError(tError);
          return false;
      }
      if (mLLClaimSchema.getClmState().equals("3"))
      {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "ClaimUWEnsureBL";
          tError.functionName = "checkApprove";
          tError.errorMessage = "已经完成给付!";
          this.mErrors.addOneError(tError);
          return false;
      }
      if (mLLClaimSchema.getClmState().equals("4"))
      {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "ClaimUWEnsureBL";
          tError.functionName = "checkApprove";
          tError.errorMessage = "已经拒赔!";
          this.mErrors.addOneError(tError);
          return false;
      }
      if (mLLClaimSchema.getClmState().equals("5"))
      {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "ClaimUWEnsureBL";
          tError.functionName = "checkApprove";
          tError.errorMessage = "已提起调查!";
          this.mErrors.addOneError(tError);
          return false;
      }

      LLClaimUnderwriteDB tLLClaimUnderwriteDB = new LLClaimUnderwriteDB();
      tLLClaimUnderwriteDB.setClmNo(mLLClaimSchema.getClmNo());
      mLLClaimUnderwriteSet.set(tLLClaimUnderwriteDB.query());
      if (tLLClaimUnderwriteDB.mErrors.needDealError() == true)
      {
          // @@错误处理
          this.mErrors.copyAllErrors(tLLClaimUnderwriteDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ClaimUWEnsureBL";
          tError.functionName = "getBaseData";
          tError.errorMessage = "核赔表LLClaimUnderwrite查询失败!";
          this.mErrors.addOneError(tError);
          return false;
      }

      LLClaimUWMainDB tLLClaimUWMainDB = new LLClaimUWMainDB();
      tLLClaimUWMainDB.setClmNo(mLLClaimSchema.getClmNo());
      int tCount = tLLClaimUWMainDB.getCount();
      if (tCount > 0)
      {
          tLLClaimUWMainDB.getInfo();
          if (tLLClaimUWMainDB.mErrors.needDealError() == true)
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tLLClaimUWMainDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "ClaimUWEnsureBL";
              tError.functionName = "getBaseData";
              tError.errorMessage = "案件核赔表LLClaimUWMain查询失败!";
              this.mErrors.addOneError(tError);
              return false;
          }
      }
      mLastLLClaimUWMainSchema.setSchema(tLLClaimUWMainDB.getSchema());

      //查询用户信息
      LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
      tLLClaimUserDB.setUserCode(mGlobalInput.Operator);
      tLLClaimUserDB.getInfo();
      if (tLLClaimUserDB.mErrors.needDealError() == true)
      {
          // @@错误处理
          this.mErrors.copyAllErrors(tLLClaimUserDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ClaimManChkBL";
          tError.functionName = "getUser";
          tError.errorMessage = "用户核赔权限信息查询失败!";
          this.mErrors.addOneError(tError);
          return false;
      }
      mLLClaimUserSchema.setSchema(tLLClaimUserDB.getSchema());
      return true;

  }

  //对于核赔拒赔的保单，更新LLClaim、LLClaimPolicy、LLClaimDetail、LJSGet、tLJSGetClaim
  /**
   * 校验保单是否已经完成核赔
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkApprove( )
  {

    //判断用户是否有权限
    String tClaimPopedom = mLLClaimUserSchema.getClaimPopedom();
//====== del for test ===== 2005-02-23 ==== start ============================
//    if (!checkClaimPopedom(tClaimPopedom.trim()))
//    {
//        return false;
//    }
//====== del for test ===== 2005-02-23 ==== end ============================
    if (mLastLLClaimUWMainSchema.getAppActionType() != null &&
        !mLastLLClaimUWMainSchema.getAppActionType().equals(""))
    {
      if (mLastLLClaimUWMainSchema.getAppActionType().equals("1"))
      {
        CError tError = new CError();
        tError.moduleName = "ClaimUWEnsureBL";
        tError.functionName = "checkApprove";
        tError.errorMessage = "案件已经签批!" ;
        this.mErrors.addOneError(tError) ;
        return false ;

      }
      //本次审核为退回重申
      if (mLastLLClaimUWMainSchema.getAppActionType().equals("2"))
      {
          //如果上次复核制定审核人员，
          //检查本次审核人员是否与制定审核人员一致
          if (mLastLLClaimUWMainSchema.getAppClmUWer() != null &&
              !mLastLLClaimUWMainSchema.getAppClmUWer().equals(""))
          {
              if (!mLastLLClaimUWMainSchema.getAppClmUWer().trim().equals(
                      mGlobalInput.Operator))
              {
                  CError tError = new CError();
                  tError.moduleName = "ClaimManChkBL";
                  tError.functionName = "submitData";
                  tError.errorMessage = "审核人员不符，复核人员指定的审核人员为：" +
                                        mLastLLClaimUWMainSchema.
                                        getAppClmUWer().trim();
                  this.mErrors.addOneError(tError);
                  return false;
              }
          }
          else
          {
              if (!mLLRegisterSchema.getHandler().trim().equals(
                      mGlobalInput.Operator))
              //取立案时确定的案件审核人员编码与当前用户比较
              {
                  CError tError = new CError();
                  tError.moduleName = "ClaimManChkBL";
                  tError.functionName = "submitData";
                  tError.errorMessage = "审核人员不符，立案指定的重审的人员为：" +
                                        mLLRegisterSchema.getHandler().trim();
                  this.mErrors.addOneError(tError);
                  return false;
              }
          }

          //设定本次保单核赔的审核类型为[签批退回审核]
          mLLClaimUWMainSchema.setCheckType("1");
      }
      if (mLastLLClaimUWMainSchema.getAppActionType().equals("3"))
      {
        CError tError = new CError();
        tError.moduleName = "ClaimManChkBL";
        tError.functionName = "submitData";
        tError.errorMessage = "案件上报，不允许修改核赔信息";
        this.mErrors .addOneError(tError) ;
        return false;
      }

    }
    else
    {
            //初次审核
            if (mLLRegisterSchema.getHandler() == null)
            {
                CError tError = new CError();
                tError.moduleName = "ClaimUWEnsureBL";
                tError.functionName = "checkApprove";
                tError.errorMessage = "立案时未指定审核人员";
                this.mErrors.addOneError(tError);
                return false;
            }
            //取立案时确定的案件审核人员编码与当前用户比较
            if (!mLLRegisterSchema.getHandler().trim().
                equals(mGlobalInput.Operator))
            {
                CError tError = new CError();
                tError.moduleName = "ClaimUWEnsureBL";
                tError.functionName = "checkApprove";
                tError.errorMessage = "审核人员不符，本案件系统指定审核人员为：" +
                                      mLLRegisterSchema.getHandler().trim();
                this.mErrors.addOneError(tError);
                return false;
            }

            //设定本次保单核赔的审核类型为[初次审核]
            mLLClaimUWMainSchema.setCheckType("0");
    }

    //检查是否所有的保单都已经核赔确认
    String tChkEnsureFlag = "Y";
    for (int i = 1; i <= mLLClaimPolicySet.size(); i++)
    {
        LLClaimPolicySchema tLLClaimPolicySchema = new
                LLClaimPolicySchema();
        tLLClaimPolicySchema.setSchema(mLLClaimPolicySet.get(i));
        String tFlag = "N";
        for (int j = 1; j <= mLLClaimUnderwriteSet.size(); j++)
        {
            LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new
                    LLClaimUnderwriteSchema();
            tLLClaimUnderwriteSchema.setSchema(mLLClaimUnderwriteSet.get(j));
            if (tLLClaimUnderwriteSchema.getPolNo().
                equals(tLLClaimPolicySchema.getPolNo()) &&
                tLLClaimUnderwriteSchema.getRealPay() ==
                tLLClaimPolicySchema.getRealPay())
            {
                tFlag = "Y";
                break;
            }
        }
        if (tFlag.equals("N"))
        {
            tChkEnsureFlag = "N";
            break;
        }
    }

    if (tChkEnsureFlag == "N")
    {
      CError tError = new CError();
      tError.moduleName = "ClaimUWEnsureBL";
      tError.functionName = "checkApprove";
      tError.errorMessage = "案件保单并未全部核赔确认!" ;
      this.mErrors.addOneError(tError) ;
      return false ;
    }


    //检查历次赔付是否已经超过保单保额
    for (int i=1;i<=mLLClaimPolicySet.size();i++)
    {
        LLClaimPolicySchema tLLClaimPolicySchema = new
                LLClaimPolicySchema();
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLLClaimPolicySchema.setSchema(mLLClaimPolicySet.get(i));
        //添加续保的处理方法
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setPolNo(tLLClaimPolicySchema.getPolNo());
        tLCPolBL.getInfo();

        if (tLCPolBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCPolBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ClaimUWEnsureBL";
            tError.functionName = "checkApprove";
            tError.errorMessage = "LCPol和LBPol表查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLCPolBL.getPolNo() == null || tLCPolBL.getPolNo().equals(""))
        {
            this.mErrors.copyAllErrors(tLCPolBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ClaimUWEnsureBL";
            tError.functionName = "checkApprove";
            tError.errorMessage = "在LCPol和LBPol中没有" +
                                  tLLClaimPolicySchema.getPolNo() +
                                  "号保险单";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLCPolSchema.setSchema(tLCPolBL.getSchema());

        //添加续保的处理方法
        LCGetSet tLCGetSet = new LCGetSet();
        SynLCLBGetBL tSynLCLBGetBL = new SynLCLBGetBL();
        tSynLCLBGetBL.setPolNo(tLLClaimPolicySchema.getPolNo());
        tLCGetSet.set(tSynLCLBGetBL.query());
        if (tSynLCLBGetBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSynLCLBGetBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ClaimUWEnsureBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "LCGet和LBGet表查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLCGetSet.size() == 0)
        {
            this.mErrors.copyAllErrors(tSynLCLBGetBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ClaimUWEnsureBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "LCGet和LBGet表中未查询到" +
                                  tLLClaimPolicySchema.getPolNo() +
                                  "号保险单！";
            this.mErrors.addOneError(tError);
            return false;
        }
        double tSumPay = 0;
        for (int j = 1; j <= tLCGetSet.size(); j++)
        {
            LCGetSchema tLCGetSchema = new LCGetSchema();
            tLCGetSchema.setSchema(tLCGetSet.get(j));
            if (tLCGetSchema.getLiveGetType().equals("1"))
            {
                tSumPay = tSumPay + tLCGetSchema.getSumMoney();
            }
        }
        tSumPay = tSumPay + tLLClaimPolicySchema.getRealPay();
        if (tSumPay > tLCPolSchema.getRiskAmnt())
        {
            CError tError = new CError();
            tError.moduleName = "ClaimUWEnsureBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "历次赔付超过保单保额，保单：" +
                                  tLLClaimPolicySchema.getPolNo();
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    //检查是否为需要强制调查案件，如果是，判断是否已经作过调查
    double tSWmoney=0;
    double tSCmoney=0;
    double tYLmoney=0;
    String tSurFlag = "0";
    System.out.println("检验是否需要强制调查");
    LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
    tLLClaimDetailDB.setClmNo(this.mLLClaimSchema.getClmNo());
    this.mLLClaimDetailSet=tLLClaimDetailDB.query();
    for (int i=1;i<=this.mLLClaimDetailSet.size();i++)
    {
        LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
        tLLClaimDetailSchema.setSchema(mLLClaimDetailSet.get(i));
        if (tLLClaimDetailSchema.getStatType() != null)
        {
            if (tLLClaimDetailSchema.getStatType().equals("SW"))
            {
                tSWmoney = tSWmoney + tLLClaimDetailSchema.getRealPay();
            }
            if (tLLClaimDetailSchema.getStatType().equals("SC"))
            {
                tSCmoney = tSCmoney + tLLClaimDetailSchema.getRealPay();
            }
            if (tLLClaimDetailSchema.getStatType().equals("YL"))
            {
                tYLmoney = tYLmoney + tLLClaimDetailSchema.getRealPay();
            }
        }
    }
    this.mLLClaimSchema.setCasePayType("0"); //一般给付金


    if (tSWmoney >= 50000 || tSCmoney >= 30000 || tYLmoney >= 10000)
    {
        //死亡保险金 >= 5万 或 伤残保险金 >= 3万 或 健康保险金 >= 1万
        //需要强制调查
        tSurFlag="1";
    }

    //长险合同生效或复效两年内，发生疾病死亡或重大疾病给付
    for (int i = 1; i <= this.mLLClaimPolicySet.size(); i++)
    {
        LLClaimPolicySchema tLLClaimPolicySchema = new LLClaimPolicySchema();
        tLLClaimPolicySchema.setSchema(mLLClaimPolicySet.get(i));
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setPolNo(tLLClaimPolicySchema.getPolNo());
        tLCPolBL.getInfo();
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(tLCPolBL.getRiskCode());
        if (tLMRiskAppDB.getInfo())
        {
            if (tLLClaimPolicySchema.getGetDutyKind().equals("202") &&
                tLMRiskAppDB.getRiskPeriod().equals("L") &&
                PubFun.calInterval(
                            tLCPolBL.getLastRevDate(),
                            this.mLLRegisterSchema.getAccidentDate(), "Y")
                    <= 2) //长险
            {
                tSurFlag = "1"; //需要强制调查
                this.mLLClaimSchema.setCasePayType("1"); //短期给付件
                break;
            }
        }
    }

    //责任免除件
    for (int i=1;i<=this.mLLClaimUnderwriteSet.size();i++)
    {
        LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new
                LLClaimUnderwriteSchema();
        tLLClaimUnderwriteSchema.setSchema(mLLClaimUnderwriteSet.get(i));
        if (tLLClaimUnderwriteSchema.getClmDecision().equals("0")) //拒赔
        {
            tSurFlag = "1"; //需要强制调查
            this.mLLClaimSchema.setCasePayType("3"); //责任免除件
            break;
        }
    }

    if (tSurFlag.equals("1"))
    {
        LLSurveyDB tLLSurveyDB = new LLSurveyDB();
        int tCount = tLLSurveyDB.getCount();
        if (tCount == 0)
        {
            CError tError = new CError();
            tError.moduleName = "ClaimUWEnsureBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "该案件属于强制调查案件，请先作调查后再审核";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    //根据审核的案件类型作案件的自动核赔，
    //自动核赔根据传入的案件给付类型和案件的赔付金额确定案件的复核级别
    //晚成自动核赔后，审核确认根据自动核赔的结果确定复核人
    VData tVData = new VData();
    String ttransact = "AutoChkCase";  //案件核赔
    ClaimAutoChkUI ttClaimAutoChkUI   = new ClaimAutoChkUI();
    try
    {
        tVData.clear();
        tVData.addElement(this.mLLClaimSchema);
        System.out.println("案件自动核赔开始");
        tVData.addElement(this.mGlobalInput);
        ttClaimAutoChkUI.submitData(tVData, ttransact);
    }
    catch(Exception ex)
    {
        CError tError = new CError();
        tError.moduleName = "ClaimUWEnsureBL";
        tError.functionName = "prepareOutputData";
        tError.errorMessage = "案件自动核赔失败，错误原因：" +
                              ex.toString();
        this.mErrors.addOneError(tError);
        return false;
    }
    CErrors tErrors = ttClaimAutoChkUI.mErrors;

    if (tErrors.needDealError())
    {
      CError tError = new CError();
      tError.moduleName = "ClaimUWEnsureBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "案件自动核赔失败，错误原因：" +
                            tErrors.getFirstError() ;
      this.mErrors.addOneError(tError) ;
      return false ;
    }

    return true;
  }


  /**
   * 核保险种信息校验,准备核保算法
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData()
  {

    //mLLClaimSchema.setClmState("2");   //核赔确认

    //准备案件核赔记录数据
    mLLClaimUWMainSchema.setRgtNo(mLLRegisterSchema.getRgtNo());
    mLLClaimUWMainSchema.setAppPhase("0"); //审核
    mLLClaimUWMainSchema.setClmUWer(mGlobalInput.Operator);
    mLLClaimUWMainSchema.setClmUWGrade(mLLClaimUserSchema.getClaimPopedom());
    mLLClaimUWMainSchema.setMngCom(mGlobalInput.ManageCom);
    mLLClaimUWMainSchema.setMakeDate(PubFun.getCurrentDate());
    mLLClaimUWMainSchema.setMakeTime(PubFun.getCurrentTime());
    mLLClaimUWMainSchema.setModifyDate(PubFun.getCurrentDate());
    mLLClaimUWMainSchema.setModifyTime(PubFun.getCurrentTime());

    //查询保单核赔表在审核过程中确定的最高核赔级别
    String tSQL = "select max(Uwgrade) from LLClaimError where clmno='"
                + mLLClaimUWMainSchema.getClmNo().trim()+"'";
    System.out.println(tSQL);
    ExeSQL tExeSQL = new ExeSQL();
    String tThisGrade="";
    tThisGrade=tExeSQL.getOneValue(tSQL);

    //如果为空，不设申请级别，完全由权限树确定申请人员；
    //否则，按照权限树与申请级别共同决定申请审核人员
    mLLClaimUWMainSchema.setAppGrade(tThisGrade);

    //申请审核人员由权限树与本次审核的申请级别确定；
    String tAppClmUWer = CaseFunPub.getAppClmUWer(
            mLLClaimUWMainSchema.getAppGrade(),
            mGlobalInput.Operator);

    if (tAppClmUWer.length() == 0)
    {
        CError tError = new CError();
        tError.moduleName = "ClaimUWEnsureBL";
        tError.functionName = "prepareOutputData";
        tError.errorMessage = "无法确定签批人";
        this.mErrors.addOneError(tError);
        return false;
    }

    mLLClaimUWMainSchema.setAppClmUWer(tAppClmUWer);
    //备注由前台传入；
    mLLClaimUWMainSchema.setAppActionType("3"); //上报

    //准备案件核赔履历表
    LLClaimUWMDetailDB tLLClaimUWMDetailDB = new LLClaimUWMDetailDB();
    tLLClaimUWMDetailDB.setClmNo(mLLClaimUWMainSchema.getClmNo());
    int tCount=tLLClaimUWMDetailDB.getCount();
    tCount++;
    LLClaimUWMDetailSchema tLLClaimUWMDetailSchema = new LLClaimUWMDetailSchema();
    tLLClaimUWMDetailSchema.setClmNo(mLLClaimUWMainSchema.getClmNo());
    tLLClaimUWMDetailSchema.setClmUWNo(String.valueOf(tCount));
    tLLClaimUWMDetailSchema.setClmUWer(mLLClaimUWMainSchema.getClmUWer());
    tLLClaimUWMDetailSchema.setMngCom(mLLClaimUWMainSchema.getMngCom());
    tLLClaimUWMDetailSchema.setMakeDate(PubFun.getCurrentDate());
    tLLClaimUWMDetailSchema.setMakeTime(PubFun.getCurrentTime());
    tLLClaimUWMDetailSchema.setModifyDate(PubFun.getCurrentDate());
    tLLClaimUWMDetailSchema.setModifyTime(PubFun.getCurrentTime());
    tLLClaimUWMDetailSchema.setClmUWGrade(
            mLLClaimUWMainSchema.getClmUWGrade());
    tLLClaimUWMDetailSchema.setAppGrade(
            mLLClaimUWMainSchema.getAppGrade());
    tLLClaimUWMDetailSchema.setCheckType(
            mLLClaimUWMainSchema.getCheckType());
    tLLClaimUWMDetailSchema.setAppPhase("0"); //审核
    tLLClaimUWMDetailSchema.setRgtNo(mLLRegisterSchema.getRgtNo());

    if (mLLClaimUWMainSchema.getAppClmUWer() != null)
    {
      tLLClaimUWMDetailSchema.setAppClmUWer(
            mLLClaimUWMainSchema.getAppClmUWer());
    }

    tLLClaimUWMDetailSchema.setAppActionType(
            mLLClaimUWMainSchema.getAppActionType());
    tLLClaimUWMDetailSchema.setRemark(
            mLLClaimUWMainSchema.getRemark());

    mInputData.clear();
    mInputData.add(mLLClaimSchema);
    mInputData.add(mLLClaimUWMainSchema);
    mInputData.add(tLLClaimUWMDetailSchema);

    mResult.clear();
    mResult.add(mLLClaimSchema);
    mResult.add(mLLClaimUWMainSchema);

    return true;
  }

  public VData getResult()
  {
    return mResult;
  }
  //检查核赔级别是否为空；检查核赔级别是否合法
  private boolean checkClaimPopedom(String tClaimPopedom)
  {

      if (tClaimPopedom == null)
      {
          CError tError = new CError();
          tError.moduleName = "ClaimManChkBL";
          tError.functionName = "submitData";
          tError.errorMessage = "权限级别为空!";
          this.mErrors.addOneError(tError);
          return false;
      }

      if (!tClaimPopedom.equals("A") &
          !tClaimPopedom.equals("B") &
          !tClaimPopedom.equals("C") &
          !tClaimPopedom.equals("D") &
          !tClaimPopedom.equals("E") &
          !tClaimPopedom.equals("F") &
          !tClaimPopedom.equals("G") &
          !tClaimPopedom.equals("H") &
          !tClaimPopedom.equals("I")) //代码待定，表示核赔人员的多种权限
      {
          CError tError = new CError();
          tError.moduleName = "ClaimManChkBL";
          tError.functionName = "submitData";
          tError.errorMessage = "没有以下权限级别：" + tClaimPopedom.trim();
          this.mErrors.addOneError(tError);
          return false;
      }
      return true;
  }

  private String getUperGrade(String tString)
  {
    if (tString.equals("A")) return "B";
    if (tString.equals("B")) return "C";
    if (tString.equals("C")) return "D";
    if (tString.equals("D")) return "E";
    if (tString.equals("E")) return "F";
    if (tString.equals("F")) return "G";
    if (tString.equals("G")) return "H";
    if (tString.equals("H")) return "I";
    return tString;
  }

  public static void main(String[] args)
  {
      ClaimUWEnsureBL aClaimUWEnsureBL = new ClaimUWEnsureBL();
      GlobalInput tG = new GlobalInput();
      tG.Operator = "000012";
      tG.ManageCom = "8611";
      LLClaimUWMainSchema aLLClaimUWMainSchema = new
                                                 LLClaimUWMainSchema();
      aLLClaimUWMainSchema.setRgtNo("510000000000160"); //立案号
      aLLClaimUWMainSchema.setClmNo("520000000000022"); //赔案号

      VData aVData = new VData();
      aVData.addElement(aLLClaimUWMainSchema);
      aVData.addElement(tG);
      aClaimUWEnsureBL.submitData(aVData, "INSERT");
  }

}
