package com.sinosoft.lis.llcase;

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: Save temporary pol info Business Logic Layer
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @date :2003-04-10
 * @version 1.0
 */
public class TemPolInfoBL
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData ;
  private VData mResult = new VData();
  private String mOperate;

  private LLCasePolicySet mLLCasePolicySet = new LLCasePolicySet();
  private LCPolSet mLCPolSet = new LCPolSet();
  private LLCasePolicySet tLLCasePolicySet = new LLCasePolicySet();
  private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
  String strMakeDate;
  String strMakeTime;
  String strAccidentDate;
  int i_count;
  private GlobalInput mGlobalInput = new GlobalInput();
  private GlobalInput mG = new GlobalInput();
  public TemPolInfoBL() {}

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;
    if (!getInputData(cInputData))
      return false;

    if (cOperate.equals("INSERT"))
    {
     i_count = mLLCasePolicySet.size();
     if(i_count==0)
     {
       CError tError = new CError();
       tError.moduleName = "RegisterUpdateBL";
       tError.functionName = "submitData";
       tError.errorMessage = "没有要保存的信息！！";
       this.mErrors .addOneError(tError) ;
       return false;
     }

     LLRegisterDB tLLRegisterDB = new LLRegisterDB();
     tLLRegisterDB.setRgtNo(mLLCasePolicySet.get(1).getRgtNo());
     System.out.println("****立案号码是"+mLLCasePolicySet.get(1).getRgtNo());
     if(!tLLRegisterDB.getInfo())
       return false;
     //对正处于调查中的案件进行控制
     if(tLLRegisterDB.getClmState().equals("5"))
      {
        this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "RegisterUpdateBL";
        tError.functionName = "submitData";
        tError.errorMessage = "该案件正处于调查之中，您无法进行保存操作！！";
        this.mErrors .addOneError(tError) ;
        return false;
     }
     //对结案的案件进行控制
     if(tLLRegisterDB.getEndCaseFlag().equals("2"))
     {
       this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
       CError tError = new CError();
       tError.moduleName = "RegisterUpdateBL";
       tError.functionName = "submitData";
       tError.errorMessage = "该案件已经结案，您无法进行保存操作！！";
       this.mErrors .addOneError(tError) ;
       return false;
     }
     strAccidentDate = tLLRegisterDB.getAccidentDate();

     if(!checkCValiDate())
     {
       return false;
     }
     if (!dealData())
       return false;
     System.out.println("-------dealData--------");
    }
    return true;
  }

  private boolean getInputData(VData cInputData)
  {
    mLLCasePolicySet.set((LLCasePolicySet)cInputData.getObjectByObjectName("LLCasePolicySet",0));
    mLCPolSet.set((LCPolSet)cInputData.getObjectByObjectName("LCPolSet",0));
    mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    System.out.println("￥￥￥￥￥管理机构是"+mG.ManageCom);
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }
  /****************
   * 判断保单的生效日期和出险日期的函数
   * 判断的过程是现在LCPol表中进行查询，若出险日期早于生效日期，则在LBPol表中进行查询
   * 若在LBPol表中查询的出险日期仍然早于生效日期，则返回错误的信息。
  **********/
   public boolean checkCValiDate()
  {
    for(int j = 1;j <= i_count;j++)
    {
      LLCasePolicySchema aLLCasePolicySchema = new LLCasePolicySchema();
      aLLCasePolicySchema.setSchema(mLLCasePolicySet.get(j));
      LCPolDB aLCPolDB = new LCPolDB();
      aLCPolDB.setPolNo(aLLCasePolicySchema.getPolNo());
      if(!aLCPolDB.getInfo())
      {
        this.mErrors.copyAllErrors(aLCPolDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "RegisterUpdateBL";
        tError.functionName = "submitData";
        tError.errorMessage = "在保单表中没有保单号为"+aLCPolDB.getPolNo()+"的记录，该操作被取消！！！";
        this.mErrors .addOneError(tError) ;
        return false;
      }
      System.out.println("￥￥￥￥￥￥￥￥￥￥该保单的保单号码是"+aLCPolDB.getPolNo());

      String t_cvalidate = aLCPolDB.getCValiDate();
      String t_firstpaydate = aLCPolDB.getFirstPayDate();
      String t_polapplydate = aLCPolDB.getPolApplyDate();
      //添加查询系统描述表的操作，若是0则执行以下程序
      //若是1则执行新增加的程序
      System.out.println("开始执行对系统描述表的查询操作！！");
      LDSysVarDB tLDSysVarDB = new LDSysVarDB();
      tLDSysVarDB.setSysVar("temppolvalidate");
      if(!tLDSysVarDB.getInfo())
      {
        this.mErrors.copyAllErrors(tLDSysVarDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "RegisterUpdateBL";
        tError.functionName = "submitData";
        tError.errorMessage = "系统描述表查询失败，该操作被取消！！！";
        this.mErrors .addOneError(tError) ;
        return false;
      }
      System.out.println("在系统描述表中所取得的描述值是"+tLDSysVarDB.getSysVarType());
      if(tLDSysVarDB.getSysVarType().equals("0"))
      {
        System.out.println("您的开始日期取的是首期交费日期（FirstPayDate）");
        if(t_cvalidate==null||t_cvalidate.equals("")
           ||t_firstpaydate==null||t_firstpaydate.equals(""))
        {
          CError tError = new CError();
          tError.moduleName = "TemPolInfoBL";
          tError.functionName = "submitData";
          tError.errorMessage = "在保单表中保单号为"+aLCPolDB.getPolNo()+"的记录无首期交费日期或生效日期，该操作被取消！！！";
          this.mErrors .addOneError(tError) ;
          return false;
        }

        if(CaseFunPub.checkDate(strAccidentDate,aLCPolDB.getFirstPayDate())||
           CaseFunPub.checkDate(aLCPolDB.getCValiDate(),strAccidentDate))
        {
          CError tError = new CError();
          tError.errorMessage = "保单号是"+aLCPolDB.getPolNo()+"的保单不是临时保单，不能进行保存！！！";
          this.mErrors .addOneError(tError) ;
          return false;
        }
     }
      else
      {
        System.out.println("您的开始日期取的是保单申请日期（PolApplyDate）");
        if(t_cvalidate==null||t_cvalidate.equals("")
           ||t_polapplydate==null||t_polapplydate.equals(""))
        {
          CError tError = new CError();
          tError.moduleName = "TemPolInfoBL";
          tError.functionName = "submitData";
          tError.errorMessage = "在保单表中保单号为"+aLCPolDB.getPolNo()+"的记录保单申请日期日期或生效日期，该操作被取消！！！";
          this.mErrors .addOneError(tError) ;
          return false;
        }
        System.out.println("保单号码时"+aLCPolDB.getPolNo()+"的保单的首期交费日期是"+t_polapplydate);
        System.out.println("保单号码是"+aLCPolDB.getPolNo()+"的保单的终结日期是"+t_cvalidate);

        if(CaseFunPub.checkDate(strAccidentDate,aLCPolDB.getPolApplyDate())||
           CaseFunPub.checkDate(aLCPolDB.getCValiDate(),strAccidentDate))
        {
          CError tError = new CError();
          tError.errorMessage = "保单号是"+aLCPolDB.getPolNo()+"的保单不是临时保单，不能进行保存！！！";
          this.mErrors .addOneError(tError) ;
          return false;
        }
      }
    }

    return true;
   }
  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
   //先要调用prepareOutputData()函数
  private boolean dealData()
  {
    prepareOutputData();
    TemPolInfoBLS tTemPolInfoBLS = new TemPolInfoBLS();
    System.out.println("Start TemPolInfobls Submit...");
    if (!tTemPolInfoBLS.submitData(mInputData,mOperate))
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tTemPolInfoBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "TemPolInfoBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据保存失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */

  private void prepareOutputData()
  {
   System.out.println("执行到prepareOutputData()2");
   try
   {
     for(int h = 1;h<=i_count;h++)
     {
       LLCasePolicySchema cLLCasePolicySchema = mLLCasePolicySet.get(h);
       System.out.println("￥￥￥214保单号码是"+cLLCasePolicySchema.getPolNo());
       LLCasePolicySchema bLLCasePolicySchema = new LLCasePolicySchema();
       LLCasePolicyDB tLLCasePolicyDB = new LLCasePolicyDB();
       tLLCasePolicyDB.setCaseNo(cLLCasePolicySchema.getCaseNo());
       tLLCasePolicyDB.setPolNo(cLLCasePolicySchema.getPolNo());
       tLLCasePolicyDB.getInfo();
       if (tLLCasePolicyDB.getMakeDate()==null)
         bLLCasePolicySchema.setMakeDate(PubFun.getCurrentDate());
       else
         bLLCasePolicySchema.setMakeDate(tLLCasePolicyDB.getMakeDate());
       if (tLLCasePolicyDB.getMakeTime()==null)
         bLLCasePolicySchema.setMakeTime(PubFun.getCurrentTime());
       else
         bLLCasePolicySchema.setMakeTime(tLLCasePolicyDB.getMakeTime());

       LCPolDB tLCPolDB = new LCPolDB();
       tLCPolDB.setPolNo(cLLCasePolicySchema.getPolNo());
       if(tLCPolDB.getInfo())
       {
         System.out.println("￥￥233查询的结果是险种编码是"+tLCPolDB.getRiskCode());
         bLLCasePolicySchema.setRiskCode(tLCPolDB.getRiskCode());
         bLLCasePolicySchema.setInsuredNo(tLCPolDB.getInsuredNo());
         bLLCasePolicySchema.setInsuredSex(tLCPolDB.getInsuredSex());
         bLLCasePolicySchema.setCValiDate(tLCPolDB.getCValiDate());
         bLLCasePolicySchema.setRiskVer(tLCPolDB.getRiskVersion());

         bLLCasePolicySchema.setCaseNo(cLLCasePolicySchema.getCaseNo());
         bLLCasePolicySchema.setPolNo(tLCPolDB.getPolNo());
         bLLCasePolicySchema.setContNo(tLCPolDB.getContNo());
         bLLCasePolicySchema.setGrpPolNo(tLCPolDB.getGrpPolNo());
         bLLCasePolicySchema.setKindCode(tLCPolDB.getKindCode());
         bLLCasePolicySchema.setPolMngCom(tLCPolDB.getManageCom());
         bLLCasePolicySchema.setSaleChnl(tLCPolDB.getSaleChnl());
         bLLCasePolicySchema.setAgentGroup(tLCPolDB.getAgentGroup());
         bLLCasePolicySchema.setRgtNo(cLLCasePolicySchema.getRgtNo());
         bLLCasePolicySchema.setInsuredName(cLLCasePolicySchema.getInsuredName());
         bLLCasePolicySchema.setInsuredBirthday(tLCPolDB.getInsuredBirthday());
         bLLCasePolicySchema.setAppntNo(tLCPolDB.getAppntNo());
         bLLCasePolicySchema.setAppntName(cLLCasePolicySchema.getAppntName());
         bLLCasePolicySchema.setPolState(tLCPolDB.getPolState());
         bLLCasePolicySchema.setOperator(mG.Operator);
         bLLCasePolicySchema.setCasePolType("1");
         bLLCasePolicySchema.setMngCom(mG.ManageCom);
         bLLCasePolicySchema.setPolType("0");
         bLLCasePolicySchema.setModifyDate(PubFun.getCurrentDate());
         bLLCasePolicySchema.setModifyTime(PubFun.getCurrentTime());
         tLLCasePolicySet.add(bLLCasePolicySchema);
         System.out.println("bl中的h的值是"+h);
       }
     }//end of for  对LLSubReport表的保存完成
     mInputData.clear();
     mInputData.add(tLLCasePolicySet);
     mResult.clear();
     mResult.add(tLLCasePolicySet);
   }
   catch (Exception ex)
   {
     ex.printStackTrace();
   }
 }
 public static void main(String[] args)
 {
   TemPolInfoUI tTemPolInfoUI=new TemPolInfoUI();
   VData tVData=new VData();
   LLCasePolicySchema tLLCasePolicySchema = new LLCasePolicySchema();
   tLLCasePolicySchema.setCasePolType("1");
   tLLCasePolicySchema.setAppntName("中国机电协会");
   tLLCasePolicySchema.setCaseNo("00100020030550000015");
   tLLCasePolicySchema.setPolNo("86110020030210000034");
   tLLCasePolicySchema.setRgtNo("00100020030510000015");
   tLLCasePolicySchema.setMngCom("ddd");
   tLLCasePolicySchema.setOperator("ddd");
   tLLCasePolicySchema.setMakeDate("2003-04-01");
   tLLCasePolicySchema.setMakeTime("12:12:12");
   tLLCasePolicySchema.setModifyDate("2002-10-10");
   tLLCasePolicySchema.setModifyTime("12:12:12");
   LLCasePolicySet tLLCasePolicySet = new LLCasePolicySet();
   tLLCasePolicySet.add(tLLCasePolicySchema);
   tVData.addElement(tLLCasePolicySet);
   tTemPolInfoUI.submitData(tVData,"INSERT");
 }

}
