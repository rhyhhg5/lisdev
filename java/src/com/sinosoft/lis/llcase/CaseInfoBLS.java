package com.sinosoft.lis.llcase;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/*******************************************************************************
 * Name     :CaseInfoBLS.java
 * Function :案件－立案－分案疾病伤残明细保存的数据库处理类
 * Author   :LiuYansong
 * Date     :2003-7-16
 */

public class CaseInfoBLS
{
  private VData mInputData ;
  public  CErrors mErrors = new CErrors();
  public String mOperate;
  public String DisplayFlag = "";//判断是在立案中进入还是在理算中进入
  public String AccidentType = "";//判断类型是YW还是JB
  LLCaseInfoSet mLLCaseInfoSet = new  LLCaseInfoSet();
  LLCaseInfoSchema mLLCaseInfoSchema = new LLCaseInfoSchema();
  public CaseInfoBLS() {}
  public static void main(String[] args)
  {
  }
  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;
    DisplayFlag = (String)mInputData.get(0);
    AccidentType = (String)mInputData.get(1);
    System.out.println("得到的DisplayFlag的值是"+DisplayFlag);
    mLLCaseInfoSet=(LLCaseInfoSet)mInputData.getObjectByObjectName("LLCaseInfoSet",0);
    mLLCaseInfoSchema.setSchema((LLCaseInfoSchema)cInputData.getObjectByObjectName("LLCaseInfoSchema",0));
    if (cOperate.equals("INSERT"))
    {
      //在同一事务中处理两个表的操作要把下条语句添加上
      try
      {
        if(mLLCaseInfoSet.size()==0)
        {
          if(!deleteData())
            return false;
        }
        if(mLLCaseInfoSet.size()>0)
        {
          if (!this.saveData())
            return false;
        }

      }
      catch(Exception ex)
      {
      }
      return true;
    }

    mInputData=(VData)cInputData.clone();
    String mOperate;
    mOperate=cOperate;
    mInputData=null;
    return true;
  }
  private boolean saveData()
  {
    Connection conn = DBConnPool.getConnection();
    if (conn==null)
    {
      CError tError = new CError();
      tError.moduleName = "CaseInfoBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    try
    {
      conn.setAutoCommit(false);
      if(mLLCaseInfoSet.size()>0)
      {
        LLCaseInfoSchema tLLCaseInfoSchema = mLLCaseInfoSet.get(1);
        if(DisplayFlag.equals("0"))
        {
          LLCaseInfoDB tLLCaseInfoDB = new LLCaseInfoDB(conn);
          tLLCaseInfoDB.setCaseNo(tLLCaseInfoSchema.getCaseNo());
          tLLCaseInfoDB.setType(tLLCaseInfoSchema.getType());
          System.out.println("类型是"+tLLCaseInfoDB.getType());
          if (!tLLCaseInfoDB.deleteSQL())
          {
            this.mErrors.copyAllErrors(tLLCaseInfoDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLCaseInfoBL";
            tError.functionName = "DeleteData";
            tError.errorMessage = "删除失败!";
            this.mErrors.addOneError(tError);
            conn.rollback() ;
            conn.close();
            return false;
          }
          conn.commit();
          conn.close();
        }
        if(DisplayFlag.equals("1"))
        {
          String sql = "delete from LLCaseInfo where CaseNo = '"+tLLCaseInfoSchema.getCaseNo()+"' and AffixSerialNo like 'SC%'";
          System.out.println("执行的伤残操作是"+sql);
          ExeSQL exesql = new ExeSQL();
          exesql.execUpdateSQL(sql);
        }
      }
      LLCaseInfoDBSet mLLCaseInfoDBSet=new LLCaseInfoDBSet(conn);
      mLLCaseInfoDBSet.set(mLLCaseInfoSet);
      if (mLLCaseInfoDBSet.insert() == false)
      {
        this.mErrors.copyAllErrors(mLLCaseInfoDBSet.mErrors);
        CError tError = new CError();
        tError.moduleName = "CaseInfoBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "保存数据失败!";
        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
        return false;
      }
      conn.commit() ;
      conn.close();
    }
    catch (Exception ex)
    {
      CError tError = new CError();
      tError.moduleName = "CaseInfoBLS";
      tError.functionName = "submitData";
      tError.errorMessage = ex.toString();
      this.mErrors .addOneError(tError);
      try
      {
        conn.rollback() ;
        conn.close();
      }
      catch(Exception e)
      {
      }
      return false;
    }
    return true;
  }

  private boolean deleteData()
  {
    Connection conn = DBConnPool.getConnection();

    if(DisplayFlag.equals("0"))
    {
      try
      {
        conn.setAutoCommit(false);
        String sql_del = "";
        //对残疾的处理
        if(!(AccidentType==null)||AccidentType.equals(""))
        {
          if(AccidentType.equals("YW"))
          {
            sql_del = " Delete from LLCaseInfo where CaseNo = '"+mLLCaseInfoSchema.getCaseNo()
                    +"' and AffixSerialNo like 'YW%' ";
          }
          if(AccidentType.equals("JB"))
          {
            sql_del = " Delete from LLCaseInfo where CaseNo = '"+mLLCaseInfoSchema.getCaseNo()
                    +"' and AffixSerialNo like 'JB%' ";
          }
          System.out.println("BLS中执行的sql语句是"+sql_del);
          ExeSQL exesql = new ExeSQL(conn);
          exesql.execUpdateSQL(sql_del);
          conn.commit();
          conn.close();
        }
      }
      catch (Exception ex)
      {
        CError tError = new CError();
        tError.moduleName = "CasePolicyBLS";
        tError.functionName = "submitData";
        tError.errorMessage = ex.toString();
        this.mErrors .addOneError(tError);
        try
        {
          conn.rollback() ;
          conn.close();
        }
        catch(Exception e)
        {
        }
        return false;
      }
    }//end of 0
    if(DisplayFlag.equals("1"))
    {
      try
      {
        conn.setAutoCommit(false);
        String sql_del = "";
        //对残疾的处理
        if(!(AccidentType==null)||AccidentType.equals(""))
        {
          if(AccidentType.equals("YW"))
          {
            sql_del = " Delete from LLCaseInfo where CaseNo = '"+mLLCaseInfoSchema.getCaseNo()
                    +"' and AffixSerialNo like 'SC%' and Type = 'YW'";
          }
          if(AccidentType.equals("JB"))
          {
            sql_del = " Delete from LLCaseInfo where CaseNo = '"+mLLCaseInfoSchema.getCaseNo()
                    +"' and AffixSerialNo like 'SC%' and Type = 'JB'";
          }
          System.out.println("BLS中执行的sql语句是"+sql_del);
          ExeSQL exesql = new ExeSQL(conn);
          exesql.execUpdateSQL(sql_del);
          conn.close();
          conn.commit();
        }
      }
      catch (Exception ex)
      {
        CError tError = new CError();
        tError.moduleName = "CasePolicyBLS";
        tError.functionName = "submitData";
        tError.errorMessage = ex.toString();
        this.mErrors .addOneError(tError);
        try
        {
          conn.rollback() ;
          conn.close();
        }
        catch(Exception e)
        {
        }
        return false;
      }

    }//end of 1
    return true;
  }
}