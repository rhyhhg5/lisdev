/**
 * 
 */
package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.CachedRiskInfo;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * 案件审批
 * 
 * @author maning
 * @version 1.0
 */
public class LLCaseAutoCommonUnderWrite extends LLCaseProcess {

	/**
	 * 
	 */
	public LLCaseAutoCommonUnderWrite() {
		// TODO Auto-generated constructor stub
	}

	// 业务逻辑错误
	public CErrors mErrors = new CErrors();

	// 获取的业务数据
	private VData mInputData = new VData();

	// 返回处理后数据
	private VData mResult = new VData();

	// 返回的提交数据库MMap
	private MMap mMMap = new MMap();

	// 用户登录信息
	private GlobalInput mGlobalInput = new GlobalInput();

	// 操作类型
	private String mOperate = "";

	// 案件信息
	private LLCaseSchema mLLCaseSchema = null;

	// 理算汇总
	private LLClaimSchema mLLClaimSchema = null;

	// 理算信息
	private LLClaimPolicySet mLLClaimPolicySet = null;

	// 理赔明细
	private LLClaimDetailSet mLLClaimDetailSet = null;

	// 核赔信息
	private LLClaimUWMainSchema mLLClaimUWMainSchema = null;

	// 核赔明细
	private LLClaimUnderwriteSet mLLClaimUnderwriteSet = null;

	// 核赔赔付明细
	private LLClaimUWDetailSet mLLClaimUWDetailSet = null;

	// 核赔轨迹
	private LLClaimUWMDetailSchema mLLClaimUWMDetailSchema = null;

	// 理赔人上级
	private String mUpUserCode = "";

	// 理赔人上级权限
	private String mUpClaimPopedom = "";

	// 理赔人权限
	private String mClaimPopedom = "";

	// 理赔人类别
	private String mClaimType = "";

	// 当前日期
	private String mCurrentDate = PubFun.getCurrentDate();

	// 当前事件
	private String mCurrentTime = PubFun.getCurrentTime();

	// 险种缓存
	private CachedRiskInfo mCachedRiskInfo = CachedRiskInfo.getInstance();
	
	private String mClmNo = ""; /** 赔案号 */
	
	 /** 社保案件实赔（用于抽检）*/
    private double mSocialSecurityRealPay = 0.0;
    
    /** 社保案件抽检金额（由于抽检）*/
    private double mSocialSecuritySpotLimit = 0.0;
    
    private String mClaimGiveType = ""; /** 赔付结论 */
    
    private String mReturnMessage = "";
    

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sinosoft.lis.llcase.LLCaseProcess#getMMap()
	 */
	public MMap getMMap() throws Exception {
		mMMap.put(mLLCaseSchema, "DELETE&INSERT");
		mMMap.put(mLLClaimUWMainSchema, "DELETE&INSERT");
		if (mLLClaimUnderwriteSet != null && mLLClaimUnderwriteSet.size() > 0) {
			mMMap.put(mLLClaimUnderwriteSet.get(1), "INSERT");
		}
		if (mLLClaimUWDetailSet != null && mLLClaimUWDetailSet.size() > 0) {
			//只取一条到险种层级
			mMMap.put(mLLClaimUWDetailSet.get(1), "INSERT");
		}
		mMMap.put(mLLClaimUWMDetailSchema, "INSERT");

		return mMMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sinosoft.lis.llcase.LLCaseProcess#getResult()
	 */
	public VData getResult() throws Exception {
		mResult.add(mLLCaseSchema);
		mResult.add(mLLClaimUWMainSchema);
		if (mLLClaimUnderwriteSet != null && mLLClaimUnderwriteSet.size() > 0) {
			mResult.add(mLLClaimUnderwriteSet.get(1));
		}
		if (mLLClaimUWDetailSet != null && mLLClaimUWDetailSet.size() > 0) {
			mResult.add(mLLClaimUWDetailSet.get(1));
		}
		mResult.add(mLLClaimUWMDetailSchema);
		return mResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sinosoft.lis.llcase.LLCaseProcess#submitData(com.sinosoft.utility.VData,
	 *      java.lang.String)
	 */
	public boolean submitData(VData aInputData, String aOperate)
			throws Exception {
		System.out.println("开始案件审批处理");
		mInputData = aInputData;
		mOperate = aOperate;

		if (!getInputData()) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		
		if(!endCase()){
			return false;
		}
		 //提交数据库
        PubSubmit tPubSubmit = new PubSubmit();
        mInputData.clear();       
        mInputData.add(getMMap());
        if (!tPubSubmit.submitData(mInputData, ""))
        {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LLCaseUnderWrite";
                tError.functionName = "dealData";
                tError.errorMessage = "理赔审批审定数据保存失败!";
                this.mErrors.addOneError(tError);
                return false;
        }
		System.out.println("案件审批处理结束");
		return true;
	}

	/**
	 * 解析获取的对象
	 * 
	 * @return 获取数据成功标志
	 */
	private boolean getInputData() throws Exception {
		mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0);

		if (mGlobalInput == null) {
			mErrors.addOneError("用户登录信息获取失败");
			return false;
		}

		mLLCaseSchema = (LLCaseSchema) mInputData.getObjectByObjectName(
				"LLCaseSchema", 0);

		if (mLLCaseSchema == null) {
			mErrors.addOneError("案件信息获取失败");
			return false;
		}

		mLLClaimSchema = (LLClaimSchema) mInputData.getObjectByObjectName(
				"LLClaimSchema", 0);
		if (mLLClaimSchema == null) {
			mErrors.addOneError("案件理算汇总获取失败");
			return false;
		}
		mClmNo = mLLClaimSchema.getClmNo();

		mLLClaimPolicySet = (LLClaimPolicySet) mInputData
				.getObjectByObjectName("LLClaimPolicySet", 0);

		if (mLLClaimPolicySet == null) {
			mErrors.addOneError("案件理算信息获取失败");
			return false;
		}

		mLLClaimUWMainSchema = (LLClaimUWMainSchema) mInputData
				.getObjectByObjectName("LLClaimUWMainSchema", 0);
		if (mLLClaimUWMainSchema == null) {
			mErrors.addOneError("案件核赔信息获取失败");
			return false;
		}

		return true;
	}

	/**
	 * 基本校验
	 * 
	 * @return 成功标志
	 * @throws Exception
	 */
	private boolean checkData() throws Exception {

		if (mGlobalInput.Operator == null || "".equals(mGlobalInput.Operator)) {
			mErrors.addOneError("操作用户登录信息获取失败");
			return false;
		}

		if (mGlobalInput.ManageCom == null || "".equals(mGlobalInput.ManageCom)) {
			mErrors.addOneError("操作用户登录信息获取失败");
			return false;
		}

		if (!"ENDCASE".equals(mOperate) && !"UWCASE".equals(mOperate)) {
			mErrors.addOneError("获取的操作类型错误");
			return false;
		}

		if (mLLCaseSchema.getCaseNo() == null
				|| "".equals(mLLCaseSchema.getCaseNo())) {
			mErrors.addOneError("理赔案件号获取失败");
			return false;
		}

		if (mLLCaseSchema.getRgtNo() == null
				|| "".equals(mLLCaseSchema.getRgtNo())) {
			mErrors.addOneError("理赔批次号获取失败");
			return false;
		}

		if (!"04".equals(mLLCaseSchema.getRgtState())
				&& !"05".equals(mLLCaseSchema.getRgtState())
				&& !"06".equals(mLLCaseSchema.getRgtState())
				&& !"10".equals(mLLCaseSchema.getRgtState())) {
			mErrors.addOneError("案件状态错误，不能进行处理");
			return false;
		}

		if (mLLCaseSchema.getHandler() == null
				|| "".equals(mLLCaseSchema.getHandler())) {
			mErrors.addOneError("案件没有分配处理人，不能进行操作");
			return false;
		}

		// 查询用户核赔权限及上级信息
		String tClaimUserSQL = "SELECT a.usercode,a.claimpopedom,a.upusercode,"
				+ "(SELECT b.claimpopedom FROM LLClaimUser b WHERE b.usercode=a.upusercode),a.uwflag "
				+ " FROM LLClaimUser a WHERE a.usercode= '"
				+ mGlobalInput.Operator + "'";
		System.out.println(tClaimUserSQL);
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(tClaimUserSQL);
		
        String tClaimUserSQL1 = "SELECT a.HandleFlag "
            + " FROM LLSocialClaimUser a WHERE a.usercode= '"
            + mGlobalInput.Operator + "' and StateFlag='1' ";
    
	    System.out.println(tClaimUserSQL1);
	    ExeSQL tExeSQL1 = new ExeSQL();
	    SSRS tSSRS1 = tExeSQL1.execSQL(tClaimUserSQL1);
    
		if (tSSRS.getMaxRow() <= 0 && tSSRS1.getMaxRow() <= 0) {
			mErrors.addOneError("用户核赔权限查询失败!用户代码：" + mGlobalInput.Operator);
			return false;
		}
		if(tSSRS.getMaxRow() > 0){
		mUpUserCode = tSSRS.GetText(1, 3);
		mClaimPopedom = tSSRS.GetText(1, 2);
		mClaimType = tSSRS.GetText(1, 5);
		if (mClaimPopedom.equals("")) {
			mErrors.addOneError("用户无核赔权限!");
			return false;
		}
//		if (mClaimType.equals("")) {
//			mErrors.addOneError("用户无人员类别!");
//			return false;
//		}
		mUpClaimPopedom = tSSRS.GetText(1, 4);
		}
//       查询用户核赔权限及上级信息
        /*String tClaimUserSQL = "SELECT a.usercode,a.claimpopedom,a.upusercode,"
                + "(SELECT b.claimpopedom FROM LLClaimUser b WHERE b.usercode=a.upusercode),a.uwflag "
                + " FROM LLClaimUser a WHERE a.usercode= '"
                + mGlobalInput.Operator + "'";*/
        
        //mUpUserCode = tSSRS.GetText(1, 3);
        //mClaimType = tSSRS.GetText(1, 5);
        if(tSSRS1.getMaxRow() > 0){
        mClaimPopedom = tSSRS1.GetText(1, 1);
        
        if (mClaimPopedom.equals("")) {
            mErrors.addOneError("用户无核赔权限!");
            return false;
        }
        }

		if (mLLClaimSchema.getClmNo() == null
				|| "".equals(mLLClaimSchema.getClmNo())) {
			mErrors.addOneError("获取案件赔付信息失败");
			return false;
		}

		if (mLLClaimSchema.getGiveType() == null
				|| "".equals(mLLClaimSchema.getGiveType())) {
			mErrors.addOneError("获取案件赔付结论失败");
			return false;
		}
		mClaimGiveType = mLLClaimSchema.getGiveType();

		if (mLLClaimPolicySet.size() <= 0) {
			mErrors.addOneError("获取理算数据失败");
			return false;
		}

		if (mLLClaimUWMainSchema.getRemark1() == null
				|| "".equals(mLLClaimUWMainSchema.getRemark1())) {
			mErrors.addOneError("获取审批意见失败");
			return false;
		}

		return true;
	}

	/**
	 * 业务处理方法
	 * 
	 * @return 处理成功标志
	 * @throws Exception
	 */
	private boolean dealData() throws Exception {
		if ("ENDCASE".equals(mOperate)) {
			if (!ENDCase()) {
				return false;
			}
		}

		if ("UWCASE".equals(mOperate)) {
			if (!UWCase()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 直接结案
	 * 
	 * @return
	 * @throws Exception
	 */
	private boolean ENDCase() throws Exception {
		if (!mLLCaseSchema.getHandler().equals(mGlobalInput.Operator)) {
			mErrors.addOneError("当前操作人不是案件处理人，不能自动结案");
			return false;
		}

		mLLClaimUWDetailSet = new LLClaimUWDetailSet();
		mLLClaimUnderwriteSet = new LLClaimUnderwriteSet();

		for (int i = 1; i <= mLLClaimPolicySet.size(); i++) {
			LLClaimPolicySchema tLLClaimPolicySchema = mLLClaimPolicySet.get(i);
			if (tLLClaimPolicySchema.getGiveType() == null
					|| "".equals(tLLClaimPolicySchema.getGiveType())) {
				mErrors.addOneError("保单无赔付结论,请先做理算确认！");
				return false;
			}

			if (!mLLClaimSchema.getClmNo().equals(
					tLLClaimPolicySchema.getClmNo())) {
				mErrors.addOneError("案件险种赔付信息与案件汇总不符");
				return false;
			}

			/**
			 * 处理核赔明细
			 */
			LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new LLClaimUnderwriteSchema();

			// 理算核赔号
			tLLClaimUnderwriteSchema.setClmNo(tLLClaimPolicySchema.getClmNo());

			// 事件关联号
			tLLClaimUnderwriteSchema.setCaseRelaNo(tLLClaimPolicySchema
					.getCaseRelaNo());

			// 批次号
			tLLClaimUnderwriteSchema.setRgtNo(tLLClaimPolicySchema.getRgtNo());

			// 案件号
			tLLClaimUnderwriteSchema
					.setCaseNo(tLLClaimPolicySchema.getCaseNo());

			// 团体保单号
			tLLClaimUnderwriteSchema.setGrpContNo(tLLClaimPolicySchema
					.getGrpContNo());

			// 团体险种号
			tLLClaimUnderwriteSchema.setGrpPolNo(tLLClaimPolicySchema
					.getGrpContNo());

			// 个人保单号
			tLLClaimUnderwriteSchema
					.setContNo(tLLClaimPolicySchema.getContNo());

			// 个人险种号
			tLLClaimUnderwriteSchema.setPolNo(tLLClaimPolicySchema.getPolNo());

			// 险类代码
			tLLClaimUnderwriteSchema.setKindCode(tLLClaimPolicySchema
					.getKindCode());

			// 险种代码
			tLLClaimUnderwriteSchema.setRiskCode(tLLClaimPolicySchema
					.getRiskCode());

			// 险种版本号
			tLLClaimUnderwriteSchema.setRiskVer(tLLClaimPolicySchema
					.getRiskVer());

			// 保单管理机构
			tLLClaimUnderwriteSchema.setPolMngCom(tLLClaimPolicySchema
					.getPolMngCom());

			// 销售渠道
			tLLClaimUnderwriteSchema.setSaleChnl(tLLClaimPolicySchema
					.getSaleChnl());

			// 代理人代码
			tLLClaimUnderwriteSchema.setAgentCode(tLLClaimPolicySchema
					.getAgentCode());

			// 代理人组别
			tLLClaimUnderwriteSchema.setAgentGroup(tLLClaimPolicySchema
					.getAgentGroup());

			// 被保人客户号
			tLLClaimUnderwriteSchema.setInsuredNo(tLLClaimPolicySchema
					.getInsuredNo());

			// 被保人名称
			tLLClaimUnderwriteSchema.setInsuredName(tLLClaimPolicySchema
					.getInsuredName());

			// 投保人客户号
			tLLClaimUnderwriteSchema.setAppntNo(tLLClaimPolicySchema
					.getAppntNo());

			// 投保人名称
			tLLClaimUnderwriteSchema.setAppntName(tLLClaimPolicySchema
					.getAppntName());

			// 保单生效日期
			tLLClaimUnderwriteSchema.setCValiDate(tLLClaimPolicySchema
					.getCValiDate());

			// 保单状态
			tLLClaimUnderwriteSchema.setPolState(tLLClaimPolicySchema
					.getPolState());

			// 核算赔付金额
			tLLClaimUnderwriteSchema.setStandPay(tLLClaimPolicySchema
					.getStandPay());

			// 核赔赔付金额
			tLLClaimUnderwriteSchema.setRealPay(tLLClaimPolicySchema
					.getRealPay());

			// 核赔结论
			tLLClaimUnderwriteSchema.setClmDecision(tLLClaimPolicySchema
					.getGiveType());

			// 核赔依据
			tLLClaimUnderwriteSchema.setClmDepend(tLLClaimPolicySchema
					.getGiveReason());

			// 核赔员
			tLLClaimUnderwriteSchema.setClmUWer(mGlobalInput.Operator);

			// 核赔级别
			tLLClaimUnderwriteSchema.setClmUWGrade(mClaimPopedom);

			// 设定本次保单核赔的审核类型为[初次审核]
			tLLClaimUnderwriteSchema.setCheckType("0"); // 审批审定类型

			// 操作员
			tLLClaimUnderwriteSchema.setOperator(mGlobalInput.Operator);

			// 管理机构
			tLLClaimUnderwriteSchema.setMngCom(mGlobalInput.ManageCom);

			// 入机日期
			tLLClaimUnderwriteSchema.setMakeDate(mCurrentDate);

			// 入机时间
			tLLClaimUnderwriteSchema.setMakeTime(mCurrentTime);

			// 修改日期
			tLLClaimUnderwriteSchema.setModifyDate(mCurrentDate);

			// 修改时间
			tLLClaimUnderwriteSchema.setModifyTime(mCurrentTime);
			mLLClaimUnderwriteSet.add(tLLClaimUnderwriteSchema);

			/**
			 * 准备核赔履历记录数据LLClaimUWDetail
			 */
			LLClaimUWDetailSchema tLLClaimUWDetailSchema = new LLClaimUWDetailSchema();
			Reflections tReflections = new Reflections();
			// 根据LLClaimUnderwriteSchema获取信息
			tReflections.transFields(tLLClaimUWDetailSchema,
					tLLClaimUnderwriteSchema);

			// 给付责任类型
			tLLClaimUWDetailSchema.setGetDutyKind(tLLClaimPolicySchema
					.getGetDutyKind());

			// 给付责任编码
			tLLClaimUWDetailSchema.setGetDutyCode(tLLClaimPolicySchema
					.getGetDutyKind());
			// 核赔次数
			tLLClaimUWDetailSchema.setClmUWNo(String.valueOf(i));

			mLLClaimUWDetailSet.add(tLLClaimUWDetailSchema);
		}

		/**
		 * 准备核赔履历记录数据LLClaimUWMain
		 */
		LLClaimUWMainSchema tLLClaimUWMainSchema = new LLClaimUWMainSchema();
		tLLClaimUWMainSchema.setSchema(mLLClaimUWMainSchema);
		mLLClaimUWMainSchema = new LLClaimUWMainSchema();

		// 入机日期
		mLLClaimUWMainSchema.setMakeDate(mCurrentDate);

		// 入机时间
		mLLClaimUWMainSchema.setMakeTime(mCurrentTime);

		// 理算核赔号
		mLLClaimUWMainSchema.setClmNo(mLLClaimSchema.getClmNo());

		// 案件号
		mLLClaimUWMainSchema.setRgtNo(mLLCaseSchema.getCaseNo());

		// 批次号
		mLLClaimUWMainSchema.setCaseNo(mLLCaseSchema.getRgtNo());

		// 操作人
		mLLClaimUWMainSchema.setOperator(mGlobalInput.Operator);

		// 管理机构
		mLLClaimUWMainSchema.setMngCom(mGlobalInput.ManageCom);

		// 修改日期
		mLLClaimUWMainSchema.setModifyDate(mCurrentDate);

		// 修改时间
		mLLClaimUWMainSchema.setModifyTime(mCurrentTime);

		// 申请阶段
		mLLClaimUWMainSchema.setAppPhase("0"); // 审核

		// 核赔员
		mLLClaimUWMainSchema.setClmUWer(mGlobalInput.Operator);

		// 核赔级别
		mLLClaimUWMainSchema.setClmUWGrade(mClaimPopedom);

		// 核赔结论
		mLLClaimUWMainSchema.setClmDecision(mLLClaimSchema.getGiveType());

		// 审批结论
		mLLClaimUWMainSchema.setcheckDecision1("1");

		// 审批意见
		mLLClaimUWMainSchema.setRemark1(tLLClaimUWMainSchema.getRemark1());

		/**
		 * 准备案件核赔履历信息LLClaimUWMDetail
		 */
		LLClaimUWMDetailDB tLLClaimUWMDetailDB = new LLClaimUWMDetailDB();
		tLLClaimUWMDetailDB.setClmNo(mLLClaimSchema.getClmNo());
		int tCount = tLLClaimUWMDetailDB.getCount();
		tCount++;
		Reflections ref = new Reflections();
		mLLClaimUWMDetailSchema = new LLClaimUWMDetailSchema();
		// 根据LLClaimUWMainSchema获取信息
		ref.transFields(mLLClaimUWMDetailSchema, mLLClaimUWMainSchema);

		// 核赔次数
		mLLClaimUWMDetailSchema.setClmUWNo(String.valueOf(tCount));

		// 备注
		mLLClaimUWMDetailSchema.setRemark(mLLClaimUWMainSchema.getRemark1());

		// 操作员
		mLLClaimUWMDetailSchema.setOperator(mGlobalInput.Operator);

		// 管理机构
		mLLClaimUWMDetailSchema.setMngCom(mGlobalInput.ManageCom);

		// 入机日期
		mLLClaimUWMDetailSchema.setMakeDate(mCurrentDate);

		// 入机时间
		mLLClaimUWMDetailSchema.setMakeTime(mCurrentTime);

		// 修改日期
		mLLClaimUWMDetailSchema.setModifyDate(mCurrentDate);

		// 修改时间
		mLLClaimUWMDetailSchema.setModifyTime(mCurrentTime);

		/**
		 * 处理案件信息表LLCase
		 */

		// 案件状态
		mLLCaseSchema.setRgtState("09");

		// 审批人
		mLLCaseSchema.setUWer(mGlobalInput.Operator);

		// 审批日期
		mLLCaseSchema.setUWDate(mCurrentDate);

		// 签批人
		mLLCaseSchema.setSigner(mGlobalInput.Operator);

		// 签批日期
		mLLCaseSchema.setSignerDate(mCurrentDate);
		
		// 结案日期
		mLLCaseSchema.setEndCaseDate(mCurrentDate);

		// 修改日期
		mLLCaseSchema.setModifyDate(mCurrentDate);

		// 修改时间
		mLLCaseSchema.setModifyTime(mCurrentTime);

		return true;
	}

	/**
	 * 通过审批流程处理案件
	 * 
	 * @return
	 * @throws Exception
	 */
	private boolean UWCase() throws Exception {
		if ("04".equals(mLLCaseSchema.getRgtState())) {
			if (!dealLS()) {
				return true;
			}
		}

		// if ("05".equals(mLLCaseSchema.getRgtState())) {
		// if (!dealSP()) {
		// return true;
		// }
		// }
		//
		// if ("06".equals(mLLCaseSchema.getRgtState())) {
		// if (!dealSD()) {
		// return true;
		// }
		// }
		//
		// if ("10".equals(mLLCaseSchema.getRgtState())) {
		// if (!dealCJ()) {
		// return true;
		// }
		// }
		//
		// if ("09".equals(mLLCaseSchema.getRgtState())) {
		// if (!dealEndCase()) {
		// return true;
		// }
		// }
		return true;
	}

	/**
	 * 理算状态审批
	 * 
	 * @return
	 */
	private boolean dealLS() {
		boolean tIsPower = true;

		String tHandler = mLLCaseSchema.getHandler();
		if (tHandler == null || "".equals(tHandler)) {
			CError.buildErr(this, "案件受理时未指定审批人！");
			return false;
		}
		if (!tHandler.equals(mGlobalInput.Operator)) {
			CError.buildErr(this, "审批人员不符，案件受理时指定审批人为：" + tHandler);
			return false;
		}

		LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
		tLLClaimUserDB.setUserCode(mGlobalInput.Operator);

		if (!tLLClaimUserDB.getInfo()) {
			CError.buildErr(this, "操作人非理赔用户");
			return false;
		}

		for (int i = 1; i <= mLLClaimPolicySet.size(); i++) {
			LLClaimPolicySchema tLLClaimPolicySchema = mLLClaimPolicySet.get(i);
			if (tLLClaimPolicySchema.getGiveType() == null
					|| "".equals(tLLClaimPolicySchema.getGiveType())) {
				CError.buildErr(this, "保单无赔付结论,请先做理算确认！");
				return false;
			}

			LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new LLClaimUnderwriteSchema();
			tLLClaimUnderwriteSchema.setClmNo(tLLClaimPolicySchema.getClmNo());
			tLLClaimUnderwriteSchema.setCaseRelaNo(tLLClaimPolicySchema
					.getCaseRelaNo());
			tLLClaimUnderwriteSchema.setRgtNo(tLLClaimPolicySchema.getRgtNo());
			tLLClaimUnderwriteSchema
					.setCaseNo(tLLClaimPolicySchema.getCaseNo());
			tLLClaimUnderwriteSchema.setGrpContNo(tLLClaimPolicySchema
					.getGrpContNo());
			tLLClaimUnderwriteSchema.setGrpPolNo(tLLClaimPolicySchema
					.getGrpContNo());
			tLLClaimUnderwriteSchema.setPolNo(tLLClaimPolicySchema.getPolNo());
			tLLClaimUnderwriteSchema.setKindCode(tLLClaimPolicySchema
					.getKindCode());
			tLLClaimUnderwriteSchema.setRiskCode(tLLClaimPolicySchema
					.getRiskCode());
			tLLClaimUnderwriteSchema.setRiskVer(tLLClaimPolicySchema
					.getRiskVer());
			tLLClaimUnderwriteSchema.setPolMngCom(tLLClaimPolicySchema
					.getPolMngCom());
			tLLClaimUnderwriteSchema.setSaleChnl(tLLClaimPolicySchema
					.getSaleChnl());
			tLLClaimUnderwriteSchema.setAgentCode(tLLClaimPolicySchema
					.getAgentCode());
			tLLClaimUnderwriteSchema.setAgentGroup(tLLClaimPolicySchema
					.getAgentGroup());
			tLLClaimUnderwriteSchema.setInsuredNo(tLLClaimPolicySchema
					.getInsuredNo());
			tLLClaimUnderwriteSchema.setInsuredName(tLLClaimPolicySchema
					.getInsuredName());
			tLLClaimUnderwriteSchema.setAppntNo(tLLClaimPolicySchema
					.getAppntNo());
			tLLClaimUnderwriteSchema.setAppntName(tLLClaimPolicySchema
					.getAppntName());
			tLLClaimUnderwriteSchema.setCValiDate(tLLClaimPolicySchema
					.getCValiDate());
			tLLClaimUnderwriteSchema.setPolState(tLLClaimPolicySchema
					.getPolState());
			tLLClaimUnderwriteSchema.setStandPay(tLLClaimPolicySchema
					.getStandPay());
			tLLClaimUnderwriteSchema.setRealPay(tLLClaimPolicySchema
					.getRealPay());

			tLLClaimUnderwriteSchema.setClmDecision(tLLClaimPolicySchema
					.getGiveType());
			tLLClaimUnderwriteSchema.setClmDepend(tLLClaimPolicySchema
					.getGiveReason());

			tLLClaimUnderwriteSchema.setClmUWer(tLLClaimPolicySchema
					.getClmUWer());
			tLLClaimUnderwriteSchema.setClmUWGrade(mClaimPopedom);

			if (!checkUserRight(tLLClaimPolicySchema)) {
				tIsPower = false;
				tLLClaimUnderwriteSchema.setAppGrade(mUpClaimPopedom);
				tLLClaimUnderwriteSchema.setAppClmUWer(mUpUserCode);
				tLLClaimUnderwriteSchema.setAppActionType("3"); // 申请动作 ** 待定 **
			}

			// 设定本次保单核赔的审核类型为[初次审核]
			tLLClaimUnderwriteSchema.setCheckType("0"); // 审批审定类型
			tLLClaimUnderwriteSchema.setOperator(mGlobalInput.Operator);
			tLLClaimUnderwriteSchema.setMngCom(mGlobalInput.ManageCom);
			tLLClaimUnderwriteSchema.setMakeDate(mCurrentDate);
			tLLClaimUnderwriteSchema.setMakeTime(mCurrentTime);
			tLLClaimUnderwriteSchema.setModifyDate(mCurrentDate);
			tLLClaimUnderwriteSchema.setModifyTime(mCurrentTime);
			mLLClaimUnderwriteSet.add(tLLClaimUnderwriteSchema);
		}

		return true;
	}

	// 逐单校验核赔人核赔权限
	private boolean checkUserRight(LLClaimPolicySchema aLLClaimPolicySchema) {

		String tMarketType = getMarketType(aLLClaimPolicySchema.getGrpContNo());

		// 人员类别与保单市场类型不符 直接上报
		if (!"3".equals(mClaimType) && !mClaimType.equals(tMarketType)) {
			return false;
		}

		String tGiveKind = getGiveKind(tMarketType, aLLClaimPolicySchema
				.getRiskCode());
		double tRealpay = 0.0;
		if ("3".equals(aLLClaimPolicySchema.getGiveType())
				&& aLLClaimPolicySchema.getRealPay() == 0) {
			tRealpay = 0.0;
			for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
				LLClaimDetailSchema tLLClaimDetailSchema = mLLClaimDetailSet
						.get(i);
				if (aLLClaimPolicySchema.getCaseNo().equals(
						tLLClaimDetailSchema.getCaseNo())
						&& aLLClaimPolicySchema.getPolNo().equals(
								tLLClaimDetailSchema.getPolNo())
						&& aLLClaimPolicySchema.getCaseRelaNo().equals(
								tLLClaimDetailSchema.getCaseRelaNo())) {
					tRealpay += tLLClaimDetailSchema.getDeclineAmnt();
				}
			}
		} else {
			tRealpay = aLLClaimPolicySchema.getRealPay();
		}
		String tLimitMoney = Double
				.toString(Math.abs(Arith.round(tRealpay, 2)));

		// 目前系统中只有一条核赔规则，就懒得查了，-_-||(表情很好)
		String tCalCode = "QX0001";
		Calculator mCalculator = new Calculator();
		mCalculator.setCalCode(tCalCode);
		// 增加基本要素{核赔级别,给付类别,给付结论,给付金额}
		mCalculator.addBasicFactor("ClaimPopedom", mClaimPopedom);
		mCalculator.addBasicFactor("GetDutyKind", tGiveKind);
		mCalculator.addBasicFactor("GetDutyType", aLLClaimPolicySchema
				.getGiveType());
		mCalculator.addBasicFactor("LimitMoney", tLimitMoney);

		String tStr = mCalculator.calculate();
		if (tStr.trim().equals("") || tStr.trim().equals("0")) {
			return false;
		}
		return true;
	}

	/**
	 * 根据市场类型进行分类 1-商保 2-社保
	 * 
	 * @param aGrpContNo
	 * @return
	 */
	private String getMarketType(String aGrpContNo) {
		String tSQL = "SELECT b.code FROM LCGrpCont a,LDCode1 b WHERE "
				+ " a.markettype=b.code1 AND b.codetype='llmarkettype' AND a.grpcontno='"
				+ aGrpContNo
				+ "' UNION SELECT b.code FROM LBGrpCont a,LDCode1 b WHERE "
				+ " a.markettype=b.code1 AND b.codetype='llmarkettype' AND a.grpcontno='"
				+ aGrpContNo + "'";
		ExeSQL tExeSQL = new ExeSQL();
		String tMarketType = tExeSQL.getOneValue(tSQL);
		if ("2".equals(tMarketType)) {
			return "2";
		} else {
			return "1";
		}
	}

	/**
	 * 根据险种判断给付类型 1-医疗险类给付 2-重疾险、意外险类给付 3-结合型市场业务给付
	 * 
	 * @param aMarketType
	 * @param aRiskCode
	 * @return
	 */
	private String getGiveKind(String aMarketType, String aRiskCode) {
		if ("2".equals(aMarketType)) {
			return "5";
		}

		LMRiskAppSchema tLMRiskAppSchema = mCachedRiskInfo
				.findRiskAppByRiskCode(aRiskCode);

		if (tLMRiskAppSchema == null) {
			mErrors.addOneError("险种" + aRiskCode + "查询失败");
			System.out.println("险种" + aRiskCode + "查询失败");
		}

		String tRiskType = tLMRiskAppSchema.getRiskType1();
		if (tRiskType.equals("2") || tRiskType.equals("5")) {
			return ("2");
		} else {
			return "1";
		}
	}
	
	/**
	 * 社保案件审批
	 * 
	 * @param aMarketType
	 * @param aRiskCode
	 * @return
	 */	
	   private boolean checkSocialSecuritySP(LLSocialClaimUserSchema aLLSocialClaimUserSchema){
	    	System.out.println("<-Go Into checkSocialSecuritySP()->");

	    	//是否有权限的标志，true-有权限
	        boolean IsPower = true;
	        //社保案件特殊标志 
	        boolean IsSocialSecurity = true;
	    	//社保业务结案金额
	        double tSocialMoney = aLLSocialClaimUserSchema.getSocialMoney();
	        //社保上级
	        String tUpUserCode = aLLSocialClaimUserSchema.getUpUserCode();
	        LLSocialClaimUserSchema tLLSocialClaimUserSchema = getLLSocialClaimUserSchema(tUpUserCode);
	        //社保上级权限
	        String tUpClaimPopedom = tLLSocialClaimUserSchema.getClaimPopedom();
	        String tUpStateFlag = tLLSocialClaimUserSchema.getStateFlag();

	    	LLClaimUnderwriteSet tLLClaimUnderwriteSet = new LLClaimUnderwriteSet();
	        LLClaimUWDetailSet tLLClaimUWDetailSet = new LLClaimUWDetailSet();
	    	
	    	LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
	        tLLClaimPolicyDB.setClmNo(mClmNo);
	        LLClaimPolicySet tLLClaimPolicySet = tLLClaimPolicyDB.query();
	        
	        //社保案件层次实赔金额
	        LLClaimDB tLLClaimDB = new LLClaimDB();
	        tLLClaimDB.setClmNo(mClmNo);
	        LLClaimSet tLLClaimSet = tLLClaimDB.query();
	        //实赔金额，需要取到案件级别
	        double tRealPay = 0.0;        
	        for (int i = 1; i <= tLLClaimSet.size(); i++) {
	            LLClaimSchema tCPolSchema = tLLClaimSet.get(i);    
	            //实赔金额，需要取到案件级别
	            tRealPay = tCPolSchema.getRealPay();
	            mSocialSecurityRealPay = tRealPay;
	            System.out.println("社保案件实赔金额："+tRealPay);
	        }
	        
	        for (int i = 1; i <= tLLClaimPolicySet.size(); i++) {

	            LLClaimPolicySchema tCPolSchema = tLLClaimPolicySet.get(i);
	            if (tCPolSchema.getGiveType() == null) {
	                CError.buildErr(this, "保单无赔付结论,请先做理算确认！");
	                return false;
	            }
	            Reflections trf = new Reflections();
	            LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new
	                    LLClaimUnderwriteSchema();
	            trf.transFields(tLLClaimUnderwriteSchema, tCPolSchema);
	            
	            System.out.println("社保案件险种"+tCPolSchema.getRiskCode()+"的实赔金额："+tRealPay);
	            if (tSocialMoney < tRealPay && !"".equals(tUpUserCode) && tUpUserCode != null) {//社保超权限且有社保上级
	                tLLClaimUnderwriteSchema.setAppGrade(tUpClaimPopedom);
	                tLLClaimUnderwriteSchema.setAppClmUWer(tUpUserCode);
	                tLLClaimUnderwriteSchema.setAppActionType("3"); //申请动作 ** 待定 **
	                
	                //上报社保上级
	                //用户没有结案权限，上报处理     
		        	
					if (!"1".equals(tUpStateFlag)) {
						CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");
						return false;
					}
	                
	                mLLClaimUWMainSchema.setAppGrade(tUpClaimPopedom); //申请级别
	                mLLClaimUWMainSchema.setAppClmUWer(tUpUserCode); //申请审定人员
	                mLLClaimUWMainSchema.setAppActionType("3"); //申请动作 ** 待定 **
	                //设置案件状态为[审批状态]
	                mLLCaseSchema.setRgtState("05");
	                mLLCaseSchema.setUWer(mGlobalInput.Operator);
	                mLLCaseSchema.setUWDate(mCurrentDate);
	                mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
	                mReturnMessage = "审批完毕,您权限不够,请等待上级审定,审定人:" +
                    mLLClaimUWMainSchema.getAppClmUWer();  
	                IsSocialSecurity = false;
	            }else if(tSocialMoney < tRealPay && ("".equals(tUpUserCode) || tUpUserCode == null)){//社保超权且无社保上级
	            	IsPower = false;
	            	CError.buildErr(this, "该用户无权审定该社保案件，且无上级，案件上报失败！");
	                return false;
	            }
	            //准备核赔记录数据LLClaimUnderwrite
	            tLLClaimUnderwriteSchema.setClmDecision(tCPolSchema.getGiveType()); //核赔结论
	            tLLClaimUnderwriteSchema.setClmDepend(tCPolSchema.getGiveReason()); //核赔依据
	            tLLClaimUnderwriteSchema.setClmUWer(mGlobalInput.Operator); //核赔员
	            tLLClaimUnderwriteSchema.setClmUWGrade(mClaimPopedom);
	            //设定本次保单核赔的审核类型为[初次审核]
	            tLLClaimUnderwriteSchema.setCheckType("0"); //审批审定类型
	            tLLClaimUnderwriteSchema.setOperator(mGlobalInput.Operator);
	            tLLClaimUnderwriteSchema.setMngCom(mGlobalInput.ManageCom);
	            tLLClaimUnderwriteSchema.setMakeDate(mCurrentDate);
	            tLLClaimUnderwriteSchema.setMakeTime(mCurrentTime);
	            tLLClaimUnderwriteSchema.setModifyDate(mCurrentDate);
	            tLLClaimUnderwriteSchema.setModifyTime(mCurrentTime);
	            tLLClaimUnderwriteSet.add(tLLClaimUnderwriteSchema);

	            //准备核赔履历记录数据LLClaimUWDetail
	            //获取履历最大序号
	            LLClaimUWDetailSchema tLLClaimUWDetailSchema = new
	                    LLClaimUWDetailSchema();
	            LLClaimUWDetailDB tmLLClaimUWDetailDB = new LLClaimUWDetailDB();
	            tmLLClaimUWDetailDB.setClmNo(mClmNo);
	            tmLLClaimUWDetailDB.setPolNo(tCPolSchema.getPolNo());
	            int tCount;
	            tCount = tmLLClaimUWDetailDB.getCount();
	            tCount++;
	            trf.transFields(tLLClaimUWDetailSchema, tLLClaimUnderwriteSchema);
	            tLLClaimUWDetailSchema.setGetDutyKind(tCPolSchema.getGetDutyKind());
	            tLLClaimUWDetailSchema.setGetDutyCode(tCPolSchema.getGetDutyKind());
	            tLLClaimUWDetailSchema.setClmUWNo(String.valueOf(tCount));
	            tLLClaimUWDetailSchema.setGetDutyCode(tCPolSchema.getGetDutyKind());
	            tLLClaimUWDetailSet.add(tLLClaimUWDetailSchema);
	        }
	        mMMap.put(tLLClaimUnderwriteSet.get(1), "DELETE&INSERT");
	        mMMap.put(tLLClaimUWDetailSet.get(1), "INSERT");
			mLLClaimUWMainSchema.setAppPhase("0"); //审核
			mLLClaimUWMainSchema.setClmUWer(mGlobalInput.Operator);
			mLLClaimUWMainSchema.setClmUWGrade(mClaimPopedom);
			
//			校验用户结案权限
	        if (IsPower && IsSocialSecurity) {
	            //用户具有结案权限
	            //设置核赔结论为赔付结论
	            mLLClaimUWMainSchema.setClmDecision(mClaimGiveType);
	            //设置案件状态为[结案状态]
	            mLLCaseSchema.setRgtState("09");
	            mLLCaseSchema.setUWer(mGlobalInput.Operator);
	            mLLCaseSchema.setUWDate(mCurrentDate);
	            mLLCaseSchema.setSigner(mGlobalInput.Operator);
	            mLLCaseSchema.setSignerDate(mCurrentDate);
	            mReturnMessage = "审批完毕,已经结案";
	        } 
			return true;
	    }

	    /**
	     * 传入用户编号，获得LLSocialClaimUserSchema
	     */
	    private LLSocialClaimUserSchema getLLSocialClaimUserSchema(String aUserCode){
	    	System.out.println("<-Go Into getLLSocialClaimUserSchema()->");
	    	LLSocialClaimUserSchema tLLSocialClaimUserSchema = new LLSocialClaimUserSchema();
	    	if(!"".equals(aUserCode) && aUserCode != null){
	    		LLSocialClaimUserDB tLLSocialClaimUserDB = new LLSocialClaimUserDB();
	    		LLSocialClaimUserSet tLLSocialClaimUserSet = new LLSocialClaimUserSet();
	    		tLLSocialClaimUserDB.setUserCode(aUserCode);
	    		System.out.println("++++");
	    		tLLSocialClaimUserSet = tLLSocialClaimUserDB.query();
	    		if(tLLSocialClaimUserSet.size()==0){
	    			return tLLSocialClaimUserSchema;
	    		}else{
	    			tLLSocialClaimUserSchema = tLLSocialClaimUserSet.get(1);
	    		}
	    	}
	    	return tLLSocialClaimUserSchema;
	    }
	    
	    
	    private boolean endCase() {
	        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
	        tLLRegisterDB.setRgtNo(mLLCaseSchema.getRgtNo());
	        if (!tLLRegisterDB.getInfo()) {
	            CError.buildErr(this, "团体受理信息查询失败！");
	            return false;
	        }
	        if (("02".equals(tLLRegisterDB.getRgtState()) ||
	             "01".equals(tLLRegisterDB.getRgtState())) &&
	            !"1".equals(tLLRegisterDB.getApplyerType())) {
	            //尚未受理完毕，不能批次结案  简易理赔的（applyertype为1）手工批次结案
	            String tsql = "select count(CaseNo) from llcase where rgtno='"
	                          + mLLCaseSchema.getRgtNo() + "'";
	            ExeSQL texesql = new ExeSQL();
	            String count = texesql.getOneValue(tsql);
	            int totalcount = Integer.parseInt(count);
	            tsql = "select count(CaseNo) from llcase where rgtno='"
	                   + mLLCaseSchema.getRgtNo() + "' and rgtstate in ('09','11','12','14')";
	            count = texesql.getOneValue(tsql);
	            int endcount = Integer.parseInt(count);
	            if (totalcount <= endcount + 1) {
	                //该团体批次下所有案件都已经结案，则团体结案
	                LLRegisterSchema tLLRegisterSchema = tLLRegisterDB.getSchema();
	                tLLRegisterSchema.setRgtState("03");
	                tLLRegisterSchema.setEndCaseDate(mCurrentDate);
//	                tLLRegisterSchema.setHandler1(mG.Operator); //团体结案人
	                tLLRegisterSchema.setModifyDate(mCurrentDate);
	                tLLRegisterSchema.setModifyTime(mCurrentTime);
	                mMMap.put(tLLRegisterSchema, "UPDATE");
	            }
	        }
	        return true;
	    }
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
