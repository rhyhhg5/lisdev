package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.util.Vector;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LMDutyGetClmSchema;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.bl.LCGetBL;

/**
 * <p>Title: 核心业务系统</p>
 * <p>Description: lis</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: SinoSoft Co. Ltd,</p>
 * @author AppleWood
 * @version 6.0
 */
public class GetDutyGetImpl implements GetDutyGet {
    public TransferData mTransferData = null;

    /**
     * 过滤出出险日期的有效保单
     * @param customerNo String
     * @param accdate String
     * @param appntNo String 团体批次理赔，如果传入了投保人，则只过滤出投保人投保的保单
     * @return LCPolSet
     */
    public VData getPols(String customerNo,String accdate,String appntNo){
        VData result = new VData();
        LCPolSet aLCPolSet = new LCPolSet();
        LCPolDB tLCPolDB = new LCPolDB();
        String sql = "select * from lcpol where appflag='1' and insuredno='"
                     +customerNo+"' and cvalidate <='"+accdate
                     +"' and enddate >='"+accdate+"' ";
        if (!appntNo.equals(""))
            sql += " and appntno='"+appntNo+"'";
        aLCPolSet = tLCPolDB.executeQuery(sql);
        sql = "select * from lbpol a where appflag='1' and stateflag<>'4' and a.insuredno = '"
              +customerNo+"' and cvalidate <='"+accdate
              +"' and (select b.startdate from lccontstate b where b.polno=a.polno "
              +" and statetype='Terminate')>='"+accdate+"'";
        if (!appntNo.equals(""))
            sql += " and appntno='"+appntNo+"'";
        aLCPolSet.add(tLCPolDB.executeQuery(sql));
        result.add(aLCPolSet);
        return result;
    }

    /**
        接口 getGetDutyGets
        给付责任初步筛选：初步筛选出客户所有的给付责任和给付责任给付
        String customerNo 客户号
        String accdate   出险日期
        说明: 从C表或b表中取得给付项记录
     */
    public VData getGetDutyGets(String customerNo,String accdate,String appntNo) {
        VData result = new VData();
        LCGetSet lcGetSet = new LCGetSet();
        LCGetDB tLCGetDB = new LCGetDB();
        String sql = "select * from lcget where polno in "
                     +"(select polno from lcpol where appflag='1' and insuredno='"
                     +customerNo+"' and cvalidate <='"+accdate
                     +"' and enddate >='"+accdate+"' ";
        if (!appntNo.equals(""))
            sql += " and appntno='"+appntNo+"')";
        else
            sql +=")";
        lcGetSet = tLCGetDB.executeQuery(sql);

        sql = "select * from lbget where polno in "
              +"(select polno from lbpol a where appflag='1' and stateflag<>'4' and a.insuredno = '"
              +customerNo+"' and cvalidate <='"+accdate
              +"' and (select b.startdate from lccontstate b where b.polno=a.polno "
              +" and statetype='Terminate')>='"+accdate+"'";
        if (!appntNo.equals(""))
            sql += " and appntno='"+appntNo+"')";
        else
            sql +=")";
        lcGetSet.add(tLCGetDB.executeQuery(sql));
        result.add(lcGetSet);
        return result;
    }

    /**根据保单号查给付责任*/
    public LCGetSet getGetDutyGets(String polNo) {
        LCGetBL tlcgetbl = new LCGetBL();
        tlcgetbl.setPolNo(polNo);
        LCGetSet lcGetSet = tlcgetbl.query();

        return lcGetSet;
    }

    /**
     接口 getGetDutyGetsFilter
     给付责任过滤:过滤掉不属于该给付责任的
     VData dutygets 给付责任
     LDGetDutyKindSet getdutykindes　给付责任类型
     */
    public VData getGetDutyGetsFilter(VData dutygets,
                                      LDGetDutyKindSet getdutykindes) {
        VData result = new VData();
        LCGetSet aLCGetSet = new LCGetSet();
        LCGetSet lcGetSet = (LCGetSet) dutygets.getObjectByObjectName(
                "LCGetSet", 0);
        System.out.println("过滤前的LCGetSet.size()=" + lcGetSet.size());

        LMDutyGetClmDB lmDutyGetClmDB = new LMDutyGetClmDB();

        for (int i = 1; i <= getdutykindes.size(); i++) {
            String getDutyKind = getdutykindes.get(i).getGetDutyKind();
            lmDutyGetClmDB.setGetDutyKind(getDutyKind);
            for (int j = 1; j <= lcGetSet.size(); j++) {
                LCGetSchema lcGet = new LCGetSchema();
                lcGet.setSchema(lcGetSet.get(j));
                boolean hasget = false;
                for (int k = 1; k <= aLCGetSet.size(); k++) {
                    if (lcGet.getGetDutyCode().equals(aLCGetSet.get(k).
                            getGetDutyCode())) {
                        hasget = true;
                        break;
                    }
                }
                if (hasget) {
                    continue;
                }
                lmDutyGetClmDB.setGetDutyCode(lcGet.getGetDutyCode());
                if(lmDutyGetClmDB.getInfo()){
                    if (calInclude(lmDutyGetClmDB.getCalCode())) {
                        lcGet.setGetDutyKind(getDutyKind); //将GetDutyKind设置到LCGet表中
                        result.add(lcGet);
                        aLCGetSet.add(lcGet);
                        System.out.println("added:" + lcGet.getGetDutyCode() +
                                           ":" + getDutyKind);
                    }
                }
            }
        }
        return result;
    }

    /**
     * calTrue
     *
     * @param string String
     * @return boolean
     */
    private boolean calInclude(String calCode) {
        if (null == calCode || "".equals(calCode)) {
            return true;
        }

        Calculator calculator = new Calculator();
        if (mTransferData == null) {
            System.out.println("没有计算参数");
        } else {
            Vector tv = mTransferData.getValueNames();
            for (int i = 0; i < tv.size(); i++) {
                String tName = (String) tv.get(i);
                String tValue = (String) mTransferData.getValueByName(tName);
                //System.out.println("tName:" + tName + "  tValue:" + tValue);
                calculator.addBasicFactor(tName, tValue);
            }
        }

        calculator.setCalCode(calCode);
        return true;
//          return!"0".equals(calculator.calculate());

    }

    /**
     * 设置计算要素
     * @param transferData TransferData
     */
    public void setCalFactor(TransferData transferData) {
        this.mTransferData = transferData;
    }

    public static void main(String[] args) {
        GetDutyGetImpl getdutygetimpl = new GetDutyGetImpl();
//        getdutygetimpl.getGetDutyGets("000000133", "2005-05-20", "1605","");
        getdutygetimpl.getGetDutyGets("000013825", "2006-3-1", "00000094");

    }
}
