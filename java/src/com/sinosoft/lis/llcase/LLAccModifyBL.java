package com.sinosoft.lis.llcase;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author cc
 * @version 1.0
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LLCaseExtSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;

public class LLAccModifyBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private String mOperate = "";
    private String SQL = "";
    private String Type = "";
    private String Rgtno = "";
    //业务处理相关变量
    /** 全局数据 */

    MMap map = new MMap();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private LLCaseExtSchema mLLCaseExtSchema = new LLCaseExtSchema();
    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();

    private LLRegisterSchema mmLLRegisterSchema = new LLRegisterSchema();
    private LLCaseSchema mmLLCaseSchema = new LLCaseSchema();
    private LLCaseExtSchema mmLLCaseExtSchema = new LLCaseExtSchema();
    private LJAGetSchema mmLJAGetSchema = new LJAGetSchema();

    private String mDate = PubFun.getCurrentDate();
    private String mTime = PubFun.getCurrentTime();

    private String strUrl = "";

    public LLAccModifyBL() {
    }

    /**
         传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("MODIFY") && !cOperate.equals("CONFIRM")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        mOperate = cOperate;

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 进行业务处理
        if (!dealData()) {
            return false;
        }

        // 准备传往后台的数据
        VData vData = new VData();
        MMap mMap = lockTable(mLJAGetSchema.getActuGetNo(), mGlobalInput);
        if (mMap != null) {
            map.add(mMap);
        } else {
            return false;
        }

        if (!prepareOutputData(vData)) {
            return false;
        }

        //　数据提交、保存

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start PubSubmit Submit...");

        if (!tPubSubmit.submitData(vData, "")) {
            // @@错误处理ssss
//            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "LLClaimRecpConfBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!请每次修改或确认操作后等待60秒后再继续操作";

            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("---commitData---");
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData) {
        try {
            vData.clear();
            System.out.println("放入 map 之前......");

//查询数据现有数据，然后作修改。
            LLRegisterDB tLLRegisterDB = new LLRegisterDB();
            LLCaseDB tLLCaseDB = new LLCaseDB();
            LLCaseExtDB tLLCaseExtDB = new LLCaseExtDB();
            LJAGetDB tLJAGetDB = new LJAGetDB();
            if (mLJAGetSchema.getSerialNo() != null &&
                mLJAGetSchema.getSerialNo().equals("Tuan")) {
                Type = "Tuan";
            } else {
                Type = "Ge";
            }
            if (mLJAGetSchema.getSaleChnl() != null) {
                Rgtno = mLJAGetSchema.getSaleChnl();
            }
            tLJAGetDB.setActuGetNo(mLJAGetSchema.getActuGetNo());
            mmLJAGetSchema = tLJAGetDB.query().get(1);

            if (mOperate.equals("MODIFY")) {

                if (mLLRegisterSchema != null) {

                    tLLRegisterDB.setRgtNo(mLLRegisterSchema.getRgtNo());
                    mmLLRegisterSchema = tLLRegisterDB.query().get(1);
                    if ("3".equals(mmLLRegisterSchema.getTogetherFlag())) {
                        if (mmLLRegisterSchema.getHandler1().equals(
                                mGlobalInput.
                                Operator) ||
                            LLCaseCommon.checkUPUpUser(mGlobalInput.
                                Operator, mmLLRegisterSchema.getHandler1())) {
                            System.out.println("LLRegister更新");
                            mmLLRegisterSchema.setRgtNo(mLLRegisterSchema.
                                    getRgtNo());
                            mmLLRegisterSchema.setCaseGetMode(mLLRegisterSchema.
                                    getCaseGetMode());
                            mmLLRegisterSchema.setBankCode(mLLRegisterSchema.
                                    getBankCode());
                            mmLLRegisterSchema.setBankAccNo(mLLRegisterSchema.
                                    getBankAccNo());
                            mmLLRegisterSchema.setAccName(mLLRegisterSchema.
                                    getAccName());
                            map.put(mmLLRegisterSchema, "UPDATE");
                        } else {
                            buildError("submitData", "没有修改权限！");
                            return false;
                        }
                    }
                }

                if (mLLCaseSchema != null) {

                    tLLCaseDB.setCaseNo(mLLCaseSchema.getCaseNo());
                    mmLLCaseSchema = tLLCaseDB.query().get(1);

                    if (mmLLCaseSchema.getRigister().equals(mGlobalInput.
                            Operator) ||
                        LLCaseCommon.checkUPUpUser(mGlobalInput.
                            Operator, mmLLCaseSchema.getRigister())) {
                        System.out.print("LLCase更新");
                        mmLLCaseSchema.setCaseNo(mLLCaseSchema.getCaseNo());
                        mmLLCaseSchema.setGetMode(mLLCaseSchema.getGetMode());
                        mmLLCaseSchema.setBankCode(mLLCaseSchema.getBankCode());
                        mmLLCaseSchema.setBankAccNo(mLLCaseSchema.getBankAccNo());
                        mmLLCaseSchema.setAccName(mLLCaseSchema.getAccName());
                        map.put(mmLLCaseSchema, "UPDATE");
                      //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
                        if (mLLCaseExtSchema != null) {
                        	tLLCaseExtDB.setCaseNo(mLLCaseSchema.getCaseNo());
                        	mmLLCaseExtSchema = tLLCaseExtDB.query().get(1);
                        	if(mmLLCaseExtSchema!=null){
                        	mmLLCaseExtSchema.setDrawerIDType(mLLCaseExtSchema.getDrawerIDType());
                        	mmLLCaseExtSchema.setDrawerID(mLLCaseExtSchema.getDrawerID());
                            map.put(mmLLCaseExtSchema, "UPDATE");
                        
                        	}
                        }//#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
                    } else {
                        buildError("submitData", "没有修改权限！");
                        return false;
                    }
                }

                
                
                
                mmLJAGetSchema.setActuGetNo(mLJAGetSchema.getActuGetNo());
                mmLJAGetSchema.setOtherNo(mLJAGetSchema.getOtherNo());
                mmLJAGetSchema.setPayMode(mLJAGetSchema.getPayMode());
                mmLJAGetSchema.setSumGetMoney(mLJAGetSchema.getSumGetMoney());
                mmLJAGetSchema.setDrawer(mLJAGetSchema.getDrawer());
                mmLJAGetSchema.setDrawerID(mLJAGetSchema.getDrawerID());
                mmLJAGetSchema.setBankCode(mLJAGetSchema.getBankCode());
                mmLJAGetSchema.setBankAccNo(mLJAGetSchema.getBankAccNo());
                mmLJAGetSchema.setCanSendBank(mLJAGetSchema.getCanSendBank());
                mmLJAGetSchema.setAccName(mLJAGetSchema.getAccName());
                mmLJAGetSchema.setOperator(mGlobalInput.Operator);
                mmLJAGetSchema.setModifyDate(mDate);
                mmLJAGetSchema.setModifyTime(mTime);
                map.put(mmLJAGetSchema, "UPDATE");
            }

            else { // mOperate.equals("CONFIRM")
            	//modify by Houyd 社保上级对修改后的付费信息进行确认
				SQL = "select upusercode from llclaimuser "
						+ "where usercode = '" + mmLJAGetSchema.getOperator()
						+ "' union select upusercode from llsocialclaimuser where usercode = '"+mmLJAGetSchema.getOperator()+"'";
				ExeSQL tExeSQL = new ExeSQL();
				SSRS tSSRS = new SSRS();
				tSSRS = tExeSQL.execSQL(SQL);
				if (tSSRS != null
						&& tSSRS.getMaxRow() != 0
						&& (tSSRS.GetText(1, 1).equals(mGlobalInput.Operator) || LLCaseCommon
								.checkUPUpUser(mGlobalInput.Operator, tSSRS
										.GetText(1, 1)))) {
					mmLJAGetSchema.setCanSendBank("0");
					mmLJAGetSchema.setShouldDate(PubFun.getCurrentDate());
					mmLJAGetSchema.setOperator(mGlobalInput.Operator);
					mmLJAGetSchema.setModifyDate(mDate);
					mmLJAGetSchema.setModifyTime(mTime);
					map.put(mmLJAGetSchema, "UPDATE");
					
					
				} else {
					buildError("submitData", "没有权限对该案件进行确认操作！");
					return false;
				}
			}

            vData.add(map);
        }

        catch (Exception ex) {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }


    /**
	 * 根据前面的输入数据，进行UI逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLJAGetSchema = (LJAGetSchema) cInputData.getObjectByObjectName(
                "LJAGetSchema", 0);
        mLLRegisterSchema = (LLRegisterSchema) cInputData.getObjectByObjectName(
                "LLRegisterSchema", 0);
        mLLCaseSchema = (LLCaseSchema) cInputData.getObjectByObjectName(
                "LLCaseSchema", 0);
        mLLCaseExtSchema = (LLCaseExtSchema) cInputData.getObjectByObjectName(
                "LLCaseExtSchema", 0);
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "DerferAppF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private MMap lockTable(String lockNoKey, GlobalInput globalInput) {
        MMap map = null;

        //案件给付类型：“LJ”
        String tLockNoType = "LX";

        //锁定有效时间
        String tAIS = "60";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", lockNoKey);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(globalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        map = tLockTableActionBL.getSubmitMap(tVData, null);
        if (map == null) {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }

        return map;
    }

}
