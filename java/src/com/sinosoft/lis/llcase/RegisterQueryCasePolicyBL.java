package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class RegisterQueryCasePolicyBL
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData ;
  private VData mResult = new VData();
  private String mOperate;
  private String str1MngCom;//用来记录当前的登陆机构
  private String str1MngCom1;//记录当前的登陆机构的前四位；

  private GlobalInput mG = new GlobalInput();//当前的登陆信息
  //  private String MCom="";
  private LCPolSchema mLCPolSchema = new LCPolSchema();
  private String InsuredNo = "";
  private String InsuredName = "";

  public RegisterQueryCasePolicyBL() {}

  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;
    if (!getInputData(cInputData))
      return false;
    if(!queryData())
      return false;
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  private boolean getInputData(VData cInputData)
  {
    InsuredNo = (String)cInputData.get(0);
    InsuredName = (String)cInputData.get(1);
    mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    return true;
  }

  /*****************************************************************************
   * 根据客户号码进行保单明显查询的函数；
   * 1.GrpPolNo用来判断险种性质（主险还是附加险）；
   * 2.ContNo用来记录险种的状态；
   * 3.Remark用来记录导致状态的原因；
   * 4.lcpol中的1用来表示是在lcpol中查询出的记录；
   * 5.lbpol中的2用来表示是在lbpol中查询的记录；
   * 6.tLCPolSet中装载的是原始的查询数据；
   * 7.yLCPolSet中装载的是经过处理后最终要进行显示的数据；
   * 8.BankAccNo：表示险种名称
   * 9.ManageCom：表示销售渠道
   * 10.t_sql1……查询出即是被保险人又是投保人的记录
   * 11.t_sql2……查询出被保险人的信息；
   * 12.t_sql3……查询出是投保人的信息；
   * 13.b_sql1……查询出即是被保险人又是投保人的记录（B）
   * 14.b_sql2……查询出被保险人的信息；
   * 15.b_sql3……查询出是投保人的信息；
   */

    private boolean queryData()
    {
      str1MngCom = mG.ManageCom;
     //str1MngCom = MCom;
      if(str1MngCom.length()<=2)
        str1MngCom1 = str1MngCom;
      else

        str1MngCom1 = str1MngCom.substring(0,4);//当前登陆信息的前四位信息
      LCPolDB tLCPolDB = new LCPolDB();
      LCPolSet bLCPolSet = new LCPolSet();
      LCPolSet tLCPolSet = new LCPolSet();
      LCPolSet yLCPolSet = new LCPolSet();

      String t_sql1="select * from  lcpol where  ManageCom like '"+str1MngCom1+"%' and AppFlag = '1' and AppntNo =  InsuredNo and  InsuredNo = '"+InsuredNo + "' order by MainPolNo,PolNo";
      String t_sql2="select * from  lcpol where  ManageCom like '"+str1MngCom1+"%' and AppFlag = '1' and AppntNo != InsuredNo and  InsuredNo = '"+InsuredNo + "' order by MainPolNo,PolNo";
      String t_sql3="select * from  lcpol where  ManageCom like '"+str1MngCom1+"%' and AppFlag = '1' and AppntNo != InsuredNo and  AppntNo   = '"+InsuredNo + "' and AppntName   = '"+InsuredName.trim()+"'  order by MainPolNo,PolNo";
      String b_sql1="select * from  lbpol where  ManageCom like '"+str1MngCom1+"%' and AppFlag = '1' and AppntNo =  InsuredNo and  InsuredNo = '"+InsuredNo + "' order by MainPolNo,PolNo";
      String b_sql2="select * from  lbpol where  ManageCom like '"+str1MngCom1+"%' and AppFlag = '1' and AppntNo != InsuredNo and  InsuredNo = '"+InsuredNo + "' order by MainPolNo,PolNo";
      String b_sql3="select * from  lbpol where  ManageCom like '"+str1MngCom1+"%' and AppFlag = '1' and AppntNo != InsuredNo and  AppntNo   = '"+InsuredNo + "' and AppntName   = '"+InsuredName.trim()+"'  order by MainPolNo,PolNo";
      System.out.println("2003-10-22");
      System.out.println("执行的t_sql1是"+t_sql1);
      System.out.println("执行的t_sql2是"+t_sql2);
      System.out.println("执行的t_sql3是"+t_sql3);
      System.out.println("执行的b_sql1是"+b_sql1);
      System.out.println("执行的b_sql2是"+b_sql2);
      System.out.println("执行的b_sql3是"+b_sql3);

      tLCPolSet.add(tLCPolDB.executeQuery(t_sql1));
      tLCPolSet.add(tLCPolDB.executeQuery(t_sql2));
      tLCPolSet.add(tLCPolDB.executeQuery(t_sql3));
      tLCPolSet.add(tLCPolDB.executeQuery(b_sql1));
      tLCPolSet.add(tLCPolDB.executeQuery(b_sql2));
      tLCPolSet.add(tLCPolDB.executeQuery(b_sql3));

      if(tLCPolSet.size()==0)
      {
        this.mErrors.copyAllErrors(tLCPolDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "ShowPolInfoBL";
        tError.functionName = "getDutyGetClmInfo";
        tError.errorMessage = "该客户号没有保单信息，请您确认录入的客户号码是否正确！！！" ;
        this.mErrors.addOneError(tError) ;
        return false;
      }
      //对tLCPolSet中的数据进行处理
      for(int count=1;count<=tLCPolSet.size();count++)
      {
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setSchema(tLCPolSet.get(count));
        LCPolSchema yLCPolSchema = new LCPolSchema();
        yLCPolSchema.setSchema(tLCPolSchema);
        //确定保单性质
        if(!(tLCPolSchema.getMainPolNo()==null||tLCPolSchema.getMainPolNo().equals("")))
        {
          String PolNo = tLCPolSchema.getPolNo().trim();
          String MainPolNo = tLCPolSchema.getMainPolNo().trim();
          if(PolNo.equals(MainPolNo))
          {
            yLCPolSchema.setGrpPolNo("主险");
          }
          else
          {
            yLCPolSchema.setGrpPolNo("附加险");
          }
        }
        //确定类别（销售渠道）
        if(!(tLCPolSchema.getSaleChnl()==null||tLCPolSchema.getSaleChnl().equals("")))
        {
          if(tLCPolSchema.getSaleChnl().equals("01"))
          {
            yLCPolSchema.setManageCom("团险");
          }
          else
          {
            yLCPolSchema.setManageCom("个险");
          }
        }
        //确定险种名称
        if(!(tLCPolSchema.getRiskCode()==null||tLCPolSchema.getRiskCode().equals("")))
        {
          LMRiskDB tLMRiskDB = new LMRiskDB();
          LMRiskSet tLMRiskSet = new LMRiskSet();
          tLMRiskDB.setRiskCode(tLCPolSchema.getRiskCode());
          tLMRiskSet.set(tLMRiskDB.query());
          /*Lis5.3 upgrade set  需要恢复逻辑
          if(tLMRiskSet.size()>0)
          {
            yLCPolSchema.setBankAccNo(tLMRiskSet.get(1).getRiskName());
          }
          else
          {
            yLCPolSchema.setBankAccNo("");
          }
          */
        }

        String tState[] = new String[4];
        tState =CaseFunPub.getPolState(tLCPolSchema.getPolNo());

        //确定险种状态的方法
         yLCPolSchema.setContNo(tState[0]);
        //确定出险人是被保人还是投保人
        if(!(tLCPolSchema.getInsuredNo()==null||tLCPolSchema.getInsuredNo().equals(""))&&
           !(tLCPolSchema.getAppntNo()==null||tLCPolSchema.getAppntNo().equals("")))
        {
          if(tLCPolSchema.getInsuredNo().equals(tLCPolSchema.getAppntNo()))
          {
            yLCPolSchema.setAgentCom("即是被保人又是投保人");
          }
          if((!tLCPolSchema.getInsuredNo().equals(tLCPolSchema.getAppntNo()))
             &&tLCPolSchema.getInsuredNo().equals(InsuredNo))
          {
            yLCPolSchema.setAgentCom("被保人");
          }
          if((!tLCPolSchema.getInsuredNo().equals(tLCPolSchema.getAppntNo()))
             &&tLCPolSchema.getAppntNo().equals(InsuredNo))
          {
            yLCPolSchema.setAgentCom("投保人");
          }
        }
        yLCPolSet.add(yLCPolSchema);
      }
      mResult.clear();
      mResult.add(yLCPolSet);
      return true;
    }
    public static void main(String[] args)
   {
      RegisterQueryCasePolicyBL tRegisterQueryCasePolicyBL = new RegisterQueryCasePolicyBL();
     String tInsuredNo = "0000000105";
      //GlobalInput tG = new GlobalInput();
     String tG = "86";
      //tG = "86";
     VData tVData = new VData();
     tVData.addElement(tInsuredNo);
     tVData.addElement(tG);
     tRegisterQueryCasePolicyBL.submitData(tVData,"Query");
   }

}


