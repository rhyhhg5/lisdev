/*
 * @(#)LLCasePrescriptionBL.java	2015-10-29 
 *
 * Copyright 2005 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/*
 * <p>Title: Web业务系统</p>
 * <p>ClassName: LLCasePrescriptionBL</p>
 * <p>Description: 理赔案件-理赔处理-账单录入-处方明细录入 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft </p>
 * @author：zqs
 * @version：1.0
 * @CreateDate：2015-10-29
 */
public class LLCasePrescriptionBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private MMap map = new MMap();
    private String mOperate = "";

    //全局数据
    private GlobalInput mGlobalInput = new GlobalInput();
    //账单信息
    //账单费用明细
    LLCasePrescriptionSet mLLCasePrescriptionSet = new LLCasePrescriptionSet();
    //LLFeeMainSchema mLLFeeMainSchema = new LLFeeMainSchema();

    private String mCaseNo = "";
    //private String mRgtNo = "";
    //private String mMainFeeNo = "";

    public LLCasePrescriptionBL()
    {
    }

    public static void main(String[] args)
    {
    	/*LLCasePrescriptionBL tLLCasePrescriptionBL = new LLCasePrescriptionBL();
    	LLCasePrescriptionSchema tLLCasePrescriptionSchema   = new LLCasePrescriptionSchema();
    	  LLCasePrescriptionSet tLLCasePrescriptionSet=new LLCasePrescriptionSet();
    	  GlobalInput tG = new GlobalInput();
    	  
    	  VData tVData = new VData();

    	   //此处需要根据实际情况修改
    	   tVData.addElement(tLLCasePrescriptionSet);
    	   tVData.addElement(tG);
    	   
    	tLLCasePrescriptionSchema=new LLCasePrescriptionSchema();
     	  tLLCasePrescriptionSchema.setCaseNo("111111");
     	  tLLCasePrescriptionSchema.setRgtNo(strRgtNo);
     	  tLLCasePrescriptionSchema.setDiagnoseType(strDiagnoseType);
     	  tLLCasePrescriptionSchema.setDrugName(strDrugName);
     	  tLLCasePrescriptionSchema.setDrugQuantity(strDrugQuantity);
     	  tLLCasePrescriptionSchema.setTimesPerDay(strTimesPerDay);
     	  tLLCasePrescriptionSchema.setQuaPerTime(strQuaPerTime);
    	
    	tLLCasePrescriptionBL.submitData(tVData,transact);
    	*/
    	
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData 输入数据
     * @param cOperate String 操作类型
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        //得到输入数据
        if (!getInputData())
        {
            return false;
        }
//          mOperate = cOperate;
        //检查数据合法性
        if (!checkInputData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData(cOperate))
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLCasePrescriptionBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有数据对象
     * @return boolean
     */
    private boolean getInputData()
    {
        System.out.println("getInputData()..."+mOperate);

        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLLCasePrescriptionSet = (LLCasePrescriptionSet) mInputData.getObjectByObjectName(
                "LLCasePrescriptionSet", 0);
        mCaseNo += (String) mInputData.getObjectByObjectName(
        		"String",0);
        
        System.out.println("mLLCasePrescriptionSet.size:" + mLLCasePrescriptionSet.size());

        return true;
    }

    /**
     * 对输入数据做必要的校验
     * @return boolean
     */
    private boolean checkInputData()
    {
        System.out.println("checkInputData()..."+mOperate);
        try
        {
            
            LLCaseDB tLLCaseDB = new LLCaseDB();
            tLLCaseDB.setCaseNo(mCaseNo);
            LLCaseSet tLLCaseSet = tLLCaseDB.query();
            LLCaseSchema mLLCaseSchema = tLLCaseSet.get(1);
            String RgtState = mLLCaseSchema.getRgtState();
            System.out.println(RgtState);
            //验证案件号
            if (tLLCaseDB.getInfo() == false)
            {
                // @@错误处理
                //mErrors.copyAllErrors(tLLCaseDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LLCasePrescriptionBL";
                tError.functionName = "checkInputData";
                tError.errorMessage = "理赔案件查询失败!" +
                                      "理赔号：" + mCaseNo;
                mErrors.addOneError(tError);
                return false;
            }
            //	案件状态为 受理，扫描，查讫 可以进行录入。
            if(!(RgtState.equals("01")||RgtState.equals("02")||RgtState.equals("08")))
            {
                CError tError = new CError();
                tError.moduleName = "LLCasePrescriptionBL";
                tError.functionName = "checkInputData";
                tError.errorMessage = "该案件状态下不能账单录入！";
                mErrors.addOneError(tError);
                return false;
            }
//            sHandler = tLLCaseDB.getHandler();
//            if (!sHandler.equals(mGlobalInput.Operator)) {
//                //当前操作者不是指定处理人
//                CError tError = new CError();
//                tError.moduleName = "ICaseCureBL";
//                tError.functionName = "checkInputData";
//                tError.errorMessage = "对不起，您没有权限处理该案件!" +
//                                      "指定处理人：" + sHandler;
//                mErrors.addOneError(tError);
//                return false;
//            }

            //账单信息验证
            /*LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
            tLLFeeMainDB.setCaseNo(mCaseNo);
            tLLFeeMainDB.setMainFeeNo(mMainFeeNo);
            LLFeeMainSet tLLFeeMainSet = tLLFeeMainDB.query();
            if (tLLFeeMainSet.size()<=0)
            {
                // @@错误处理
                mErrors.copyAllErrors(tLLFeeMainDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LLCaseChargeDetailBL";
                tError.functionName = "checkInputData";
                tError.errorMessage = "账单信息查询失败!";
                mErrors.addOneError(tError);
                return false;
            }*/
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseChargeDetailBL";
            tError.functionName = "checkInputData";
            tError.errorMessage = "在校验输入的数据时出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @param cOperate String 操作类型
     * @return boolean
     */
    private boolean dealData(String cOperate)
    {
        System.out.println("dealData()..."+mOperate);
        mOperate = cOperate;
        boolean tReturn = false;

        //保存录入
        if (mOperate.equals("INSERT"))
        {
            StringBuffer sbSql = new StringBuffer();
            sbSql
            	.append(" DELETE FROM LLCasePrescription D ")
            	.append(" WHERE D.CASENO = '")
            	.append(mCaseNo)
            	.append("'");
            map.put(sbSql.toString(), "DELETE");
            String tLimit = PubFun.getNoLimit("86");

            LLCasePrescriptionSchema tLLCasePrescriptionSchema = null;
            LLCasePrescriptionSet tLLCasePrescriptionSet = new LLCasePrescriptionSet();

            for (int i=1; i <= mLLCasePrescriptionSet.size(); i ++ )
            {
            	//System.out.println(tLLCasePrescriptionSchema.toString());
            	tLLCasePrescriptionSchema = mLLCasePrescriptionSet.get(i);
            	
            	tLLCasePrescriptionSchema.setCasePrescriptionNo(
                        PubFun1.CreateMaxNo("CasePrescriptionNo", tLimit));
            	tLLCasePrescriptionSchema.setMngCom(mGlobalInput.ManageCom);
            	tLLCasePrescriptionSchema.setOperator(mGlobalInput.Operator);
            	tLLCasePrescriptionSchema.setMakeDate(PubFun.getCurrentDate());
            	tLLCasePrescriptionSchema.setMakeTime(PubFun.getCurrentTime());
            	tLLCasePrescriptionSchema.setModifyDate(tLLCasePrescriptionSchema.getMakeDate());
            	tLLCasePrescriptionSchema.setModifyTime(tLLCasePrescriptionSchema.getMakeTime());

                tLLCasePrescriptionSet.add(tLLCasePrescriptionSchema);

            }
            map.put(tLLCasePrescriptionSet, "INSERT");
            mResult.add(tLLCasePrescriptionSet);
            tReturn = true;
        }

        //修改录入
        if (mOperate.equals("UPDATE"))
        {
        }

        //删除录入
        if (cOperate.equals("DELETE"))
        {
            StringBuffer sbSql = new StringBuffer();
            sbSql
            .append(" DELETE FROM LLCasePrescription D ")
            .append(" WHERE D.CASENO = '")
            .append(mCaseNo)
            .append("'");

            map.put(sbSql.toString(), "DELETE");

 //           mResult.add(mFeeMainSchema);
            tReturn = true;
        }

        return tReturn;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        System.out.println("prepareOutputData()...");

        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCasePrescriptionBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 得到处理后的结果数据
     * @return boolean
     */
    public VData getResult()
    {
        return this.mResult;
    }

}
