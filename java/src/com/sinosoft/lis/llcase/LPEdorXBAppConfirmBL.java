package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.xb.PRnewPolAppBL;
import com.sinosoft.lis.xb.XBConst;
import com.sinosoft.lis.xb.PRnewAppCancelPolBL;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 续保保全理算类
 * 续保时，核保完毕后，对核保结论做相应的后续处理
 * 1、批准申请：不做任何处理，通过
 * 2、终止申请：自动撤销对应险种的续保申请
 * 4、附加条件：
 *    a：风险加费：得到费信息，生成对应的临时数据
 *    b：免除责任：不需要做任何操作
 *    c：降低档次、降低保额：使用最新的险种信息，重算保费，替换原来的临时险种信息
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class LPEdorXBAppConfirmBL implements EdorAppConfirm
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private LPEdorItemSchema mLPEdorItemSchema = null;
//    private LPPolSet mLPPolSet = null;   //发生核保的险种
    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    private Reflections ref = new Reflections();
    
    private String queryType = "2"; //2-普通单， 3-少儿单


    private MMap map = new MMap();

    public LPEdorXBAppConfirmBL()
    {
    }

    /**
     * getResult
     *
     * @return VData
     * @todo Implement this com.sinosoft.lis.bq.EdorConfirm method
     */
    public VData getResult()
    {
        VData data = new VData();
        data.add(map);

        return data;
    }

    /**
     * submitData
     * submitData
     * 处理续保保全确认操作
     * @param inputData VData：包括：LPEdorItemSchema, GlobalInput
     * GlobalInput：操作员信息
     * @param operate String，在此为“”
     * @return boolean：操作成功true：否则false
     * @todo Implement this com.sinosoft.lis.bq.EdorAppConfirm method
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }
        if(!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 处理业务逻辑
     * @return boolean：成功true，否则false
     */
    private boolean dealData()
    {
        //得到核保结论
        LPUWMasterDB tLPUWMasterDB = new LPUWMasterDB();
        tLPUWMasterDB.setEdorNo(mLPEdorItemSchema.getEdorNo());
        tLPUWMasterDB.setEdorType(mLPEdorItemSchema.getEdorType());
//        tLPUWMasterDB.setContNo(mLPEdorItemSchema.getContNo());
        LPUWMasterSet tLPUWMasterSet = tLPUWMasterDB.query();
        if(tLPUWMasterSet.size() == 0)
        {
            mErrors.addOneError("还没有核保完毕");
            return false;
        }
        
        String queryType = "2"; //2-普通单，3-少儿单
        
        for(int i = 1; i <= 1; i++)
        {
        	String sql = "select count(1) from lcpol where polno = '" +  tLPUWMasterSet.get(i).getPolNo() + "' and riskcode in ('320106','120706') " ;
        	String tCount = new ExeSQL().getOneValue(sql);
        	if(Integer.parseInt(tCount)>0)
        	{
        		queryType = "3";
        	}
        	else
        	{
        		queryType = "2";
        	}
        }

        for(int i = 1; i <= tLPUWMasterSet.size(); i++)
        {
        	if(tLPUWMasterSet.get(i).getPassFlag().equals(BQ.PASSFLAG_PASS)){
        		
        		if(!dealPASS(tLPUWMasterSet.get(i),queryType)){
        			return false;
        		}
        	}
            //处理续保终止
            if(tLPUWMasterSet.get(i).getPassFlag().equals(BQ.PASSFLAG_STOPXB))
            {
                if(!dealStop(tLPUWMasterSet.get(i),queryType))
                {
                    return false;
                }
            }
            if(tLPUWMasterSet.get(i).getPassFlag().equals(BQ.PASSFLAG_CONDITION)){
            	if(!dealPASS(tLPUWMasterSet.get(i),queryType)){
        			return false;
        		}
            	//处理降档、降额
                if(tLPUWMasterSet.get(i).getSubAmntFlag() != null && tLPUWMasterSet.get(i).getSubAmntFlag().equals("1")
                   ||
                   tLPUWMasterSet.get(i).getSubMultFlag() != null && tLPUWMasterSet.get(i).getSubMultFlag().equals("1"))
                {
                    if(!dealSubAmntAndMult(tLPUWMasterSet.get(i)))
                    {
                        return false;
                    }
                }

                //处理加费
                if(tLPUWMasterSet.get(i).getAddPremFlag() != null && tLPUWMasterSet.get(i).getAddPremFlag().equals("1"))
                {
//                	if(queryType.equals("3"))
//                	{
//                		if(!setGetEndorse(tLPUWMasterSet.get(i))) //少儿险加费直接生成收费
//                		{
//                			return false;
//                		}
//                	}
//                	else 
//                	{
                		if(!dealAddFee(tLPUWMasterSet.get(i)))
                        {
                            return false;
                        }
//                	}
                 }

                 //处理免责
                 if(tLPUWMasterSet.get(i).getSpecFlag() != null
                         && tLPUWMasterSet.get(i).getSpecFlag().equals("1"))
                 {
                     if(!dealSpec(tLPUWMasterSet.get(i)))
                     {
                         return false;
                     }
                 }
            }
            
             
        }

//        String sql = "update LCRnewStateLog "
//                     + "set state = '"
//                     + XBConst.RNEWSTATE_UNHASTEN + "', "
//                     + "   operator = '" + this.mGlobalInput.Operator + "', "
//                     + "   modifyDate = '" + this.mCurDate + "', "
//                     + "   modifyTime = '" + this.mCurTime + "' "
//                     + "where newContNo = '"
//                     + tLPUWMasterSet.get(1).getContNo() + "' ";
//        map.put(sql, "UPDATE");

        //sumMoney(tLPUWMasterSet.get(1).getContNo());

        return true;
    }

    /**
     * 保单保费求和
     */
    private void sumMoney(String newContNo)
    {
        //责任
        String sql = "update LCDuty a "
                     + "set (prem, sumPrem) = "
                     + "   (select sum(prem), sum(sumPrem) from LCPrem "
                     + "    where polNo = a.polNo "
                     + "       and dutyCode = a.dutyCode) "
                     + "where contNo = '" + newContNo + "' ";
        map.put(sql, SysConst.UPDATE);

        //险种
        sql = "update LCPol a "
                     + "set (prem, sumPrem) = "
                     + "   (select sum(prem), sum(sumPrem) from LCDuty "
                     + "    where polNo = a.polNo) "
                     + "where contNo = '" + newContNo + "' ";
        map.put(sql, SysConst.UPDATE);

        //保单
        sql = "update LCCont a "
                     + "set (prem, sumPrem) = "
                     + "   (select sum(prem), sum(sumPrem) from LCPol "
                     + "    where contNo = a.contNo) "
                     + "where contNo = '" + newContNo + "' ";
        map.put(sql, SysConst.UPDATE);

    }

    /**
     * 处理续保终止
     * @param tLPUWMasterSchema LPUWMasterSchema：核保结论
     * @return boolean：处理成功true
     */
    private boolean dealStop(LPUWMasterSchema tLPUWMasterSchema,String queryType)
    {

//        String sql = "select polNo from LCRnewStateLog "
//                     + "where newPolNo = '"
//                     + tLPUWMasterSchema.getPolNo() + "' ";
//        String polNo = new ExeSQL().getOneValue(sql);
    	if(queryType.equals("3")){
    		
    		 map.put("update LCPol set polState = '" + XBConst.POLSTATE_LPXBSTOP + "' "
    	                + "where contno = '" + tLPUWMasterSchema.getContNo() + "' ",  "UPDATE");
    		
    	}
    	else{
    		map.put("update LCPol set polState = '" + XBConst.POLSTATE_LPXBSTOP + "' "
                    + "where polNo = '" + tLPUWMasterSchema.getPolNo() + "' ",  "UPDATE");
    	}
        

        return true;
    }
    
    private boolean dealPASS(LPUWMasterSchema tLPUWMasterSchema,String queryType)
    {

//        String sql = "select polNo from LCRnewStateLog "
//                     + "where newPolNo = '"
//                     + tLPUWMasterSchema.getPolNo() + "' ";
//        String polNo = new ExeSQL().getOneValue(sql);
    	if(queryType.equals("3")){
    		
    		 map.put("update LCPol set polState = '00019999' "
    	                + "where contno = '" + tLPUWMasterSchema.getContNo() + "' ",  "UPDATE");
    		
    	}
    	else{
    		map.put("update LCPol set polState = '00019999' "
                    + "where polNo = '" + tLPUWMasterSchema.getPolNo() + "' ",  "UPDATE");
    	}
        

        return true;
    }


    /**
     * 为添加免责信息准备数据
     */
    private boolean dealSpec(LPUWMasterSchema tLPUWMasterSchema)
    {
        LPSpecDB db = new LPSpecDB();
        db.setPolNo(tLPUWMasterSchema.getPolNo());
        db.setEdorNo(tLPUWMasterSchema.getEdorNo());
        db.setEdorType(tLPUWMasterSchema.getEdorType());
        LPSpecSet tLPSpecSet = db.query();

        LCSpecSet tLCSpecSet = new LCSpecSet();

//        for(int i = 1; i <= tLPSpecSet.size(); i++)
//        {
//            LCSpecSchema tLCSpecSchema = new LCSpecSchema();
//            ref.transFields(tLCSpecSchema, tLPSpecSet.get(i));
//            tLCSpecSchema.setOperator(mGlobalInput.Operator);
//            tLCSpecSchema.setModifyDate(mCurDate);
//            tLCSpecSchema.setModifyTime(mCurTime);
//
//            tLCSpecSet.add(tLCSpecSchema);
//        }
//        map.put(tLCSpecSet, "DELETE&INSERT");
        LPSpecSet ttLPSpecSet = new LPSpecSet();
        for(int i = 1; i <= tLPSpecSet.size(); i++)
        {
        	LPSpecSchema ttLPSpecSchema = new LPSpecSchema();
            ref.transFields(ttLPSpecSchema, tLPSpecSet.get(i));
            ttLPSpecSchema.setEdorType("LB");
            ttLPSpecSchema.setOperator(mGlobalInput.Operator);
            ttLPSpecSchema.setModifyDate(mCurDate);
            ttLPSpecSchema.setModifyTime(mCurTime);

            ttLPSpecSet.add(ttLPSpecSchema);
        }
        map.put(ttLPSpecSet, "DELETE&INSERT");

        return true;
    }


    /**
     * 处理降额和降档
     * 用核保后的险种责任信息进行保费重算
     * @param tLPPolSchema LPPolSchema
     * @return boolean
     */
    private boolean dealSubAmntAndMult(LPUWMasterSchema tLPUWMasterSchema)
    {
        LCPolSchema tLCPolSchema = getLCPol(tLPUWMasterSchema);  //临时险种信息
        if(tLCPolSchema == null)
        {
            return false;
        }

        TransferData td = new TransferData();
        td.setNameAndValue(XBConst.AFTERUW, XBConst.AFTERUW);

        VData data = new VData();
        data.add(mGlobalInput);
        data.add(tLCPolSchema);
        data.add(tLPUWMasterSchema);
        data.add(td);
        td.setNameAndValue("LCPolUWed",tLCPolSchema);

        System.out.println("开始重算保费");
        LPRnewPolAppBL tPRnewPolAppBL = new LPRnewPolAppBL();
        MMap tMMap = tPRnewPolAppBL.getSubmitMap(data, "");
        if(tMMap == null)
        {
            mErrors.copyAllErrors(tPRnewPolAppBL.mErrors);
            return false;
        }

        //将重算得到的险种信息相关号码变更为核保险种的号码
        if(!changeNo(tMMap, tLPUWMasterSchema))
        {
            return false;
        }

        return true;
    }

    /**
     * 准备保费重算的险种信息
     * @param schema LPPolSchema: 正续保的险种临时数据
     * @return LCPolSchema: 原险种信息
     */
    private LCPolSchema getLCPol(LPUWMasterSchema tLPUWMasterSchema)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select a.* ")
            .append("from LCPol a ")
            .append(" where a.polNo =  '")
            //.append("   and b.newPolNo = '")
            .append(tLPUWMasterSchema.getPolNo())
            //.append("'  and b.state != '").append(XBConst.RNEWSTATE_DELIVERED)
            .append("' ");
        System.out.println(sql.toString());

        LCPolSet tLCPolSet = new LCPolDB().executeQuery(sql.toString());
        if(tLCPolSet.size() == 0)
        {
            mErrors.addOneError("没有查询到核保险种"
                                + tLPUWMasterSchema.getPolNo()
                                + "对应的原险种信息。");
            return null;
        }

        LCPolSchema tLCPolSchema = tLCPolSet.get(1);

        if(tLPUWMasterSchema.getSubAmntFlag() != null
           && tLPUWMasterSchema.getSubAmntFlag().equals("1"))
        {
            tLCPolSchema.setAmnt(tLPUWMasterSchema.getAmnt());
        }
        if(tLPUWMasterSchema.getSubMultFlag() != null
           && tLPUWMasterSchema.getSubMultFlag().equals("1"))
        {
            tLCPolSchema.setMult(tLPUWMasterSchema.getMult());
        }


        return tLCPolSchema;
    }

    /**
     * 将重算得到的险种信息相关号码变更为核保险种的号码
     * LCPolSchema：polNo、proposalNo
     * @param tMMap MMap
     * @return boolean
     */
    private boolean changeNo(MMap tMMap, LPUWMasterSchema tLPUWMasterSchema)
    {
    	LPPolSchema dealtLPPolSchema = (LPPolSchema) tMMap
                                       .getObjectByObjectName("LPPolSchema", 0);
        Object[] Pduty = tMMap.getAllObjectByObjectName("LPDutySchema", 0);
        Object[] Pprem = tMMap.getAllObjectByObjectName("LPPremSchema", 0);
        Object[] Pget = tMMap.getAllObjectByObjectName("LPGetSchema", 0);
        if(dealtLPPolSchema == null
           || Pduty == null || Pduty.length == 0
           || Pprem == null || Pprem.length == 0
           || Pget == null || Pget.length == 0)
        {
            mErrors.addOneError("核保结束后重算保费出错");
            return false;
        }

        LCPolSchema tLCPolSchema = getTempPolSchema(tLPUWMasterSchema.getPolNo());

        dealtLPPolSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
        dealtLPPolSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
        dealtLPPolSchema.setContNo(tLCPolSchema.getContNo());
        dealtLPPolSchema.setProposalContNo(tLCPolSchema.getProposalContNo());
        dealtLPPolSchema.setPolNo(tLCPolSchema.getPolNo());
        dealtLPPolSchema.setProposalNo(tLPUWMasterSchema.getProposalNo());
        dealtLPPolSchema.setOperator(mGlobalInput.Operator);
        dealtLPPolSchema.setModifyDate(mCurDate);
        dealtLPPolSchema.setModifyTime(mCurTime);
        map.put(dealtLPPolSchema, "DELETE&INSERT");

        for(int i = 0; i < Pduty.length; i++)
        {
            LPDutySchema tLPDutySchema = (LPDutySchema)Pduty[i];
            tLPDutySchema.setContNo(tLCPolSchema.getContNo());
            tLPDutySchema.setPolNo(tLCPolSchema.getPolNo());
            tLPDutySchema.setOperator(mGlobalInput.Operator);
            tLPDutySchema.setModifyDate(mCurDate);
            tLPDutySchema.setModifyTime(mCurTime);

            map.put(tLPDutySchema, "DELETE&INSERT");
        }

        for(int i = 0; i < Pprem.length; i++)
        {
            LPPremSchema tLPPremSchema = (LPPremSchema)Pprem[i];
            tLPPremSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
            tLPPremSchema.setContNo(tLCPolSchema.getContNo());
            tLPPremSchema.setPolNo(tLCPolSchema.getPolNo());
            tLPPremSchema.setOperator(mGlobalInput.Operator);
            tLPPremSchema.setModifyDate(mCurDate);
            tLPPremSchema.setModifyTime(mCurTime);

            map.put(tLPPremSchema, "DELETE&INSERT");
        }

        for(int i = 0; i < Pget.length; i++)
        {
            LPGetSchema tLPGetSchema = (LPGetSchema) Pget[i];
            tLPGetSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
            tLPGetSchema.setContNo(tLCPolSchema.getContNo());
            tLPGetSchema.setPolNo(tLCPolSchema.getPolNo());
            tLPGetSchema.setOperator(mGlobalInput.Operator);
            tLPGetSchema.setModifyDate(mCurDate);
            tLPGetSchema.setModifyTime(mCurTime);

            map.put(tLPGetSchema, "DELETE&INSERT");
        }

        return true;
    }

    /**
     * 查询续保险种临时记录
     * @param polNo String
     * @return LCPolSchema
     */
    private LCPolSchema getTempPolSchema(String polNo)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(polNo);
        if(!tLCPolDB.getInfo())
        {
            mErrors.addOneError("没有查询到续保险种临时记录" + polNo);
            return null;
        }
        return tLCPolDB.getSchema();
    }

    /**
     * 处理加费核保结论
     * @return boolean
     */
    private boolean dealAddFee(LPUWMasterSchema tLPUWMasterSchema)
    {
        String sql = "  select * "
                     + "from LPPrem "
                     + "where edorNo = '" + tLPUWMasterSchema.getEdorNo() + "' "
                     + "   and edorType = '" + tLPUWMasterSchema.getEdorType() + "' "
                     + "   and polNo = '" + tLPUWMasterSchema.getPolNo() + "' "
                     + "   and payPlanCode like '000000%' ";  //加费
        LPPremDB tLPPremDB = new LPPremDB();
        LPPremSet tLPPremSet = tLPPremDB.executeQuery(sql);
        if(tLPPremSet.size() == 0)
        {
            mErrors.addOneError("险种" + tLPUWMasterSchema.getPolNo()
                                + "核保结论为加费，但没有查询到加费信息，加费数据可能已丢失");
            return false;
        }

//        LCPremSet tLCPremSet = new LCPremSet();
//        for(int i = 1; i <= tLPPremSet.size(); i++)
//        {
//            LCPremSchema tLCPremSchema = new LCPremSchema();
//            ref.transFields(tLCPremSchema, tLPPremSet.get(i));
//            tLCPremSchema.setOperator(mGlobalInput.Operator);
//            tLCPremSchema.setModifyDate(mCurDate);
//            tLCPremSchema.setModifyTime(mCurTime);
//
//            tLCPremSet.add(tLCPremSchema);
//        }
//        map.put(tLCPremSet, "DELETE&INSERT");
        
      LPPremSet ttLPPremSet = new LPPremSet();
      for(int i = 1; i <= tLPPremSet.size(); i++)
      {
          LPPremSchema ttLPPremSchema = new LPPremSchema();
          ref.transFields(ttLPPremSchema, tLPPremSet.get(i));
          ttLPPremSchema.setEdorType("LB");
          ttLPPremSchema.setOperator(mGlobalInput.Operator);
          ttLPPremSchema.setModifyDate(mCurDate);
          ttLPPremSchema.setModifyTime(mCurTime);

          ttLPPremSet.add(ttLPPremSchema);
      }
      map.put(ttLPPremSet, "DELETE&INSERT");

        return true;
    }

    /**
     * 得到传入的数据
     * @param data VData：submitData中传入的VData集合
     * @return boolean：成功true，否则false
     */
    private boolean getInputData(VData data)
    {
        mGlobalInput = (GlobalInput) data
                       .getObjectByObjectName("GlobalInput", 0);
        mLPEdorItemSchema = (LPEdorItemSchema) data
                            .getObjectByObjectName("LPEdorItemSchema", 0);
        if(mLPEdorItemSchema == null || mGlobalInput == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        return true;
    }
    
    /**
     * 设置批改补退费表
     * @return boolean
     */
    private boolean setGetEndorse(LPUWMasterSchema aLPUWMasterSchema)
    {
    	String sql = "  select * "
    			+ "from LPPrem "
    			+ "where edorNo = '" + aLPUWMasterSchema.getEdorNo() + "' "
    			+ "   and edorType = '" + aLPUWMasterSchema.getEdorType() + "' "
    			+ "   and polNo = '" + aLPUWMasterSchema.getPolNo() + "' "
    			+ "   and payPlanCode like '000000%' ";  //加费
    	LPPremDB tLPPremDB = new LPPremDB();
    	LPPremSet tLPPremSet = tLPPremDB.executeQuery(sql);
    	if(tLPPremSet.size() == 0)
    	{
    	   System.out.println("险种" + aLPUWMasterSchema.getPolNo() + "没有查询到加费信息，不需要生成LJSGetEndorse");
    	   return false;
    	}
    	
    	double addPrem = 0.0;
    	
    	for(int i=1 ;i<=tLPPremSet.size();i++ )
    	{
    		addPrem+=tLPPremSet.get(i).getPrem();
    	}
    	
    	LCPolSchema tLCPolSchema = getLCPol(aLPUWMasterSchema);

        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();
        tLJSGetEndorseSchema.setGetNoticeNo(aLPUWMasterSchema.getEdorNo()); //给付通知书号码，没意义
        tLJSGetEndorseSchema.setEndorsementNo(aLPUWMasterSchema.getEdorNo());
        tLJSGetEndorseSchema.setFeeOperationType(aLPUWMasterSchema.getEdorType());
        tLJSGetEndorseSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
        tLJSGetEndorseSchema.setContNo(tLCPolSchema.getContNo());
        tLJSGetEndorseSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
        tLJSGetEndorseSchema.setPolNo(tLCPolSchema.getPolNo());
        tLJSGetEndorseSchema.setGetDate(mLPEdorItemSchema.getEdorValiDate());
        tLJSGetEndorseSchema.setGetMoney(Math.abs(addPrem));
        tLJSGetEndorseSchema.setRiskCode(tLCPolSchema.getRiskCode());
        tLJSGetEndorseSchema.setRiskVersion(tLCPolSchema.getRiskVersion());
        tLJSGetEndorseSchema.setAgentCom(tLCPolSchema.getAgentCom());
        tLJSGetEndorseSchema.setAgentType(tLCPolSchema.getAgentType());
        tLJSGetEndorseSchema.setAgentCode(tLCPolSchema.getAgentCode());
        tLJSGetEndorseSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
        tLJSGetEndorseSchema.setApproveCode(tLCPolSchema.getApproveCode());
        tLJSGetEndorseSchema.setApproveDate(tLCPolSchema.getApproveDate());
        tLJSGetEndorseSchema.setApproveTime(tLCPolSchema.getApproveTime());
        tLJSGetEndorseSchema.setFeeFinaType(BQ.FEEFINATYPE_BF);
        tLJSGetEndorseSchema.setAppntNo(tLCPolSchema.getAppntNo());
        tLJSGetEndorseSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
        tLJSGetEndorseSchema.setDutyCode(BQ.FILLDATA);
        tLJSGetEndorseSchema.setPayPlanCode(aLPUWMasterSchema.getPayPlanCode());
        tLJSGetEndorseSchema.setOtherNo(aLPUWMasterSchema.getEdorNo());
        tLJSGetEndorseSchema.setOtherNoType(BQ.NOTICETYPE_P);
        tLJSGetEndorseSchema.setGetFlag("0"); //0-收费 ， 1-付费
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setManageCom(tLCPolSchema.getManageCom());
        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLJSGetEndorseSchema, "DELETE&INSERT");
        return true;
    }


    public static void main(String[] args)
    {
        GlobalInput g = new GlobalInput();
        g.Operator = "pa0001";
        g.ComCode = "86";

        LPEdorItemDB db = new LPEdorItemDB();
        db.setEdorAcceptNo("20060822000012");

        VData d = new VData();
        d.add(g);
        d.add(db.query().get(1));

        PEdorXBAppConfirmBL bl = new PEdorXBAppConfirmBL();

        if(!bl.submitData(d, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
