package com.sinosoft.lis.llcase;

import java.io.*;
import java.util.*;
import org.jdom.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.task.GetMemberInfo;
import com.sinosoft.lis.bq.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Tjj
 * @version 1.0
 */

public class LLContDealCalPrintBL {
    private Document myDocument;

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    private XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //保全受理号 By WangJH
    private String mEdorAcceptNo = "";

    //批单号
    private String mEdorNo = "";
    private String mContNo = "";
    private int mSameCM = 0;
    private String sqlSelectAddress ="" ;
    //业务处理相关变量
    private TextTag textTag = new TextTag(); //新建一个TextTag的实例

    /** 全局数据 */
    private GlobalInput mGlobalInput = null;
    private ListTable tlistTable;
    private MMap mMap = new MMap();
    private LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();
    private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();

    //By WangJH
    public LLContDealCalPrintBL(String aEdorAcceptNo) {
        mEdorAcceptNo = aEdorAcceptNo;
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!getInputData(cInputData)) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        if (!submit()) {
            return false;
        }
        return true;
    }

    /**
     * 生成数据但不提交
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean getSubmitData(VData cInputData, String cOperate) {
        if (!getInputData(cInputData)) {
            return false;
        }

        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 得到MMap
     * @return MMap
     */
    public MMap getMMap() {
        return mMap;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit() {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("\n\nStart Write Print Data\n\n");
        boolean tFlag = false;

        //最好紧接着就初始化xml文档
        xmlexport.createDocuments("LLContDealPrint.vts", mGlobalInput);

        //从2层查询变更为3层查询--By WangJH
        //增加了保全受理号，提升到定层的保全申请主表LPEdorApp开始查找
        //保全申请主表LPEdorApp->个险保全批改表LPEdorMain->个险保全项目表LPEdorItem
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
        tLPEdorAppDB.setEdorAcceptNo(mEdorAcceptNo);
        LPEdorAppSet tLPEdorAppSet = new LPEdorAppSet();
        tLPEdorAppSet.set(tLPEdorAppDB.query());
        if (tLPEdorAppSet == null || tLPEdorAppSet.size() < 1) {
            buildError("dealData", "在LPEdorApp中无相关保全受理号的数据");
            return false;
        }
        mLPEdorAppSchema = tLPEdorAppSet.get(1);

        xmlexport.addDisplayControl("displayHead");

        String tCustomerNo = "";
        textTag.add("EdorAcceptNo", mLPEdorAppSchema.getEdorAcceptNo());
        textTag.add("BarCode1", mLPEdorAppSchema.getEdorAcceptNo());
        textTag.add("BarCodeParam1", "BarHeight=25&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        textTag.add("AppDate",
                    CommonBL.decodeDate(mLPEdorAppSchema.getEdorAppDate()));
        textTag.add("ConfDate",
                    CommonBL.decodeDate(mLPEdorAppSchema.getMakeDate()));
        setFixedInfo();

        if (mLPEdorAppSchema.getOtherNoType().equals("1")) {
            tCustomerNo = mLPEdorAppSchema.getOtherNo();
            String sql = "select * from LPAppnt where edorno in (select edorno from lpedormain where edoracceptno='" +
                         mLPEdorAppSchema.getEdorAcceptNo() + "')";
            System.out.println("sql 1 : " + sql);
            LPAppntDB tLPAppntDB = new LPAppntDB();
            LPAppntSet tLPAppntSet = tLPAppntDB.executeQuery(sql);
            if (tLPAppntSet != null && tLPAppntSet.size() > 0) {
                tCustomerNo = tLPAppntSet.get(1).getAppntNo();
            }
            textTag.add("CustomerNo", tCustomerNo);
        } else if (mLPEdorAppSchema.getOtherNoType().equals("3")) {
            String sql = "select * from lpcont where contno='" +
                         mLPEdorAppSchema.getOtherNo() +
                         "' and edorno in (select edorno from lpedormain where edoracceptno='" +
                         mLPEdorAppSchema.getEdorAcceptNo() + "')";
            System.out.println("sql 2 : " + sql);
            LPContDB tLPContDB = new LPContDB();
            LPContSet tLPContSet = tLPContDB.executeQuery(sql);
            if (tLPContSet == null || tLPContSet.size() < 1) {
                LCContDB tLCContDB = new LCContDB();
                tLCContDB.setContNo(mLPEdorAppSchema.getOtherNo());
                if (!tLCContDB.getInfo()) {
                    buildError("dealData", "保全申请的合同不存在");
                    return false;
                }
                tCustomerNo = tLCContDB.getAppntNo();
            } else {
                tCustomerNo = tLPContSet.get(1).getAppntNo();
            }
            textTag.add("CustomerNo", tCustomerNo);
        }

        String sql = "select * from LPPerson where CustomerNo='" + tCustomerNo +
                     "' and edorno in (select edorno from lpedormain where edoracceptno='" +
                     mLPEdorAppSchema.getEdorAcceptNo() + "')";
        System.out.println("sql 3 : " + sql);
        LPPersonSchema tLPPersonSchema = new LPPersonSchema();
        LPPersonDB tLPPersonDB = new LPPersonDB();
        LPPersonSet tLPPersonSet = tLPPersonDB.executeQuery(sql);
        if (tLPPersonSet == null || tLPPersonSet.size() < 1) {
            LDPersonDB tLDPersonDB = new LDPersonDB();
            tLDPersonDB.setCustomerNo(tCustomerNo);
            if (!tLDPersonDB.getInfo()) {
                CError tError = new CError();
                tError.moduleName = "LLContDealCalPrintBL";
                tError.functionName = "getInsuredCM";
                tError.errorMessage = "LDPersonDB不存在相应记录!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LDPersonSchema tLDPersonSchema = tLDPersonDB.getSchema();
            Reflections aReflections = new Reflections();
            aReflections.transFields(tLPPersonSchema, tLDPersonSchema);
        } else {
            tLPPersonSchema = tLPPersonSet.get(1).getSchema();
        }
        textTag.add("EdorAppName", tLPPersonSchema.getName());
        textTag.add("EdorAppSex", CommonBL.decodeSex(tLPPersonSchema.getSex()));
        textTag.add("AppntName", tLPPersonSchema.getName());

        sql = "select * from LPAddress where CustomerNo='" + tCustomerNo +
              "' and edorno in (select edorno from lpedormain where edoracceptno='" +
              mLPEdorAppSchema.getEdorAcceptNo() + "')";
        System.out.println("sql 4 : " + sql);
        LPAddressSchema tLPAddressSchema = new LPAddressSchema();
        LPAddressDB tLPAddressDB = new LPAddressDB();
        LPAddressSet tLPAddressSet = tLPAddressDB.executeQuery(sql);
        if (tLPAddressSet == null || tLPAddressSet.size() < 1) {
            //得到第一个保全申请保单的合同号
            LPEdorMainDB db = new LPEdorMainDB();
            db.setEdorAcceptNo(mEdorAcceptNo); ;
            LPEdorMainSet set = db.query();
            if (set.size() <= 0) {
                mErrors.addOneError("没有查到受理号" + mEdorAcceptNo
                                    + "所对应的保全申请LPEdorApp记录");
                return false;
            }

            //得到保单的地址信息(地址号)
            String sqledortyp = " select edortype From lpedoritem where edoracceptno ='"+mLPEdorAppSchema.getEdorAcceptNo()+"'";
            String edorType = new ExeSQL().getOneValue(sqledortyp);
            if(edorType.equals("RB"))
            {
                 sqlSelectAddress =
                   "select * "
                   + "from LCAddress "
                   + "where customerNo = '" + tCustomerNo + "' "
//                   + "    and addressNo ='" + tLCAppntDB.getAddressNo() + "' "
                   ;

            }else
            {
                LCAppntDB tLCAppntDB = new LCAppntDB();
                tLCAppntDB.setContNo(set.get(1).getContNo());
                tLCAppntDB.getInfo();

                //得到该合同该地址号的地址信息
                 sqlSelectAddress =
                        "select * "
                        + "from LCAddress "
                        + "where customerNo = '" + tCustomerNo + "' "
                        + "    and addressNo ='" + tLCAppntDB.getAddressNo() + "' ";
            }
            System.out.println("sql 5 : " + sqlSelectAddress);
            LCAddressDB tLCAddressDB = new LCAddressDB();

            LCAddressSet tLCAddressSet = tLCAddressDB.executeQuery(
                    sqlSelectAddress);
            if (tLCAddressSet == null || tLCAddressSet.size() < 1) {
                CError tError = new CError();
                tError.moduleName = "LLContDealCalPrintBL";
                tError.functionName = "getInsuredCM";
                tError.errorMessage = "未找到投保人地址信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LCAddressSchema tLCAddressSchema =
                    tLCAddressSet.get(1).getSchema();
            Reflections aReflections = new Reflections();
            aReflections.transFields(tLPAddressSchema, tLCAddressSchema);
        } else {
            tLPAddressSchema = tLPAddressSet.get(1).getSchema();
        }
        textTag.add("EdorAppZipCode", tLPAddressSchema.getZipCode());
        textTag.add("EdorAppAddress", tLPAddressSchema.getPostalAddress());

        //查询个险保全项目表中保全受理号为mEdorAcceptNo的记录
        String AppSQL = "select * from LPEdorMain where EdorAcceptNo ='" +
                        mEdorAcceptNo +
                        "'order by MakeDate,MakeTime";
        System.out.println("sql 6 : " + AppSQL);
        //--End

        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        //tLPEdorMainDB.setEdorNo(mEdorNo);    //By WangJH
        LPEdorMainSet tLPEdorMainSet = tLPEdorMainDB.executeQuery(AppSQL); //By WangJH
        if (0 == tLPEdorMainSet.size()) {
            buildError("dealData", "在LPEdorMain中无相关批单号的数据");
            return false;
        }

        //增加了一层循环 By WangJH
        for (int i = 1; i <= tLPEdorMainSet.size(); i++) {
            LPEdorMainSchema tLPEdorMainSchema = tLPEdorMainSet.get(i);
            mContNo = tLPEdorMainSchema.getContNo(); //取得保全记录的合同号
            mEdorNo = tLPEdorMainSchema.getEdorNo(); //取得保全记录的批单号 By WangJH
            LCContSchema tLCContSchema = new LCContSchema();
            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setContNo(mContNo);
            if (!tLCContDB.getInfo()) {
                LBContDB tLBContDB = new LBContDB();
                tLBContDB.setContNo(mContNo);
                if (!tLBContDB.getInfo()) {
                    return false;
                }
                Reflections aReflections = new Reflections();
                aReflections.transFields(tLCContSchema, tLBContDB.getSchema());
            } else {
                tLCContSchema.setSchema(tLCContDB.getSchema());
            }
            //业务员信息
            String agntPhone = "";
            String temPhone = "";
            String agentCode = null;
            LPContDB tLPContDB = new LPContDB();
            tLPContDB.setContNo(mContNo);
            tLPContDB.setEdorNo(mEdorNo);
            tLPContDB.setEdorType("PR");//个单迁移
            if (tLPContDB.getInfo()) {
                agentCode = tLPContDB.getAgentCode();
            }
            if (agentCode == null) {
                agentCode = tLCContSchema.getAgentCode();
            }
            LAAgentDB tLaAgentDB = new LAAgentDB();
            tLaAgentDB.setAgentCode(agentCode);
            tLaAgentDB.getInfo();
            textTag.add("AgentName", tLaAgentDB.getName());
            textTag.add("AgentCode", tLaAgentDB.getGroupAgentCode());
            temPhone = tLaAgentDB.getMobile();
            if (temPhone == null || temPhone.equals("") ||
                temPhone.equals("null")) {
                agntPhone = tLaAgentDB.getPhone();
            } else {
                agntPhone = temPhone;
            }
            textTag.add("Phone", agntPhone);

            //机构信息
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tLCContSchema.getAgentGroup());
            tLABranchGroupDB.getInfo();
            String branchSQL =
                    " select * from LABranchGroup where agentgroup = "
                    +
                    " (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
                    + " (select agentgroup from laagent where agentcode ='"
                    + tLaAgentDB.getAgentCode() + "'))"
                    ;
            LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(
                    branchSQL);
            if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
                CError.buildErr(this, "查询业务员机构失败");
                return false;
            }

            textTag.add("AgentGroup", tLABranchGroupSet.get(1).getName());

            LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
            tLPEdorItemDB.setEdorNo(mEdorNo);
            tLPEdorItemDB.setContNo(mContNo);
            LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
            LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(1);   
            
            LLContDealDB tLLContDealDB = new LLContDealDB();
            tLLContDealDB.setEdorNo(mEdorNo);
            tLLContDealDB.setContNo(mContNo);
            if(tLLContDealDB.getInfo())
            {
            	LLContDealSchema tLLContDealSchema = tLLContDealDB.getSchema();
            	   
                if (tLLContDealSchema.getEdorType().equals("CT")) { 
                    xmlexport.addDisplayControl("displayCT");
                    if (!this.getDetailCT(tLPEdorItemSchema)) {
                        return false;
                    }
                    tFlag = true;
                }

                else if(tLLContDealSchema.getEdorType().equals("RB")){
                    xmlexport.addDisplayControl("displayRB");
                    if(!this.getDetailRB(tLPEdorItemSchema))
                    {
                        return false ;
                    }
                    tFlag = true ;
                }
                else if(tLLContDealSchema.getEdorType().equals("HZ")){
                    xmlexport.addDisplayControl("displayHZ");
                    if(!this.getDetailHZ(tLPEdorItemSchema))
                    {
                        return false ;
                    }
                    tFlag = true ;
                }
                else if(tLLContDealSchema.getEdorType().equals("CD")){
                    xmlexport.addDisplayControl("displayCD");
                    if(!this.getDetailCD(tLPEdorItemSchema))
                    {
                        return false ;
                    }
                    tFlag = true ;
                }
            }                            
        } 
        //增加的一层循环 By WangJH
        sql = "select * from ljsgetendorse where 1=1 and EndorsementNo ='" +
              this.mLPEdorAppSchema.getEdorAcceptNo() + "'";
        LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
        System.out.println("SQL 电风扇  " + sql);
        LJSGetEndorseSet tLJSGetEndorseSet = tLJSGetEndorseDB.executeQuery(sql);
        System.out.println(tLJSGetEndorseSet.size());
        if (tLJSGetEndorseSet == null || tLJSGetEndorseSet.size() < 1) {
            System.out.println("没有收退费记录");
        } else {
            this.xmlexport.addDisplayControl("displayMN");
            double tmoney = this.mLPEdorAppSchema.getGetMoney();
            if (tmoney < 0.0) {
                this.textTag.add("MN_Type", "应退");
                this.textTag.add("MN_Money", Math.abs(tmoney));
            } else if (tmoney > 0.0) {
                this.textTag.add("MN_Type", "应收");
                this.textTag.add("MN_Money", tmoney);
            } else {
                this.textTag.add("MN_Type", "应收");
                this.textTag.add("MN_Money", 0);
            }
        }
        //   if ((this.mLPEdorAppSchema.getGetMoney()-0.0)>-0.000001&&(this.mLPEdorAppSchema.getGetMoney()-0.0)<0.00001)
        //   {
        //       this.xmlexport.addDisplayControl("displayMN");
        //   }
        AppAcc tAppAcc = new AppAcc();
        String noticeType = "";
        if (mLPEdorAppSchema.getOtherNoType().equals("1")) {
            noticeType = BQ.NOTICETYPE_P;
        }
        if (mLPEdorAppSchema.getOtherNoType().equals("2")) {
            noticeType = BQ.NOTICETYPE_G;
        }
        LCAppAccTraceSchema tLCAppAccTraceSchema = tAppAcc.getLCAppAccTrace(
                mLPEdorAppSchema.
                getOtherNo(), mLPEdorAppSchema.getEdorAcceptNo(), noticeType);
        if (tLCAppAccTraceSchema != null) {
            if (tLCAppAccTraceSchema.getAccType().equals("0")) {
                if (!this.getDetailAppAccTakeOut(tLCAppAccTraceSchema)) {
                    return false;
                }
            }
            if (tLCAppAccTraceSchema.getAccType().equals("1")) {
                if (!this.getDetailAppAccShiftTo(tLCAppAccTraceSchema)) {
                    return false;
                }
            }
        } else {
            if (!tAppAcc.mErrors.getLastError().equals("查询投保人帐户轨迹表记录不存在!")) {
                // @@错误处理
                this.mErrors.copyAllErrors(tAppAcc.mErrors);
                return false;
            }
        }

        if (!tFlag) {
            buildError("dealData", "发生一个未知错误使主批单不能打印！");
            return false;
        }

        if (textTag.size() > 0) {
            myDocument = xmlexport.addTextTag(textTag);
        }

        mResult.clear();

        //生成主打印批单schema
        LPEdorAppPrintSchema tLPEdorAppPrintSchemaMain = new
                LPEdorAppPrintSchema();
        tLPEdorAppPrintSchemaMain.setEdorAcceptNo(mEdorAcceptNo);
        tLPEdorAppPrintSchemaMain.setManageCom(mGlobalInput.ManageCom);
        tLPEdorAppPrintSchemaMain.setPrtFlag("N");
        tLPEdorAppPrintSchemaMain.setPrtTimes(0);
        tLPEdorAppPrintSchemaMain.setMakeDate(PubFun.getCurrentDate());
        tLPEdorAppPrintSchemaMain.setMakeTime(PubFun.getCurrentTime());
        tLPEdorAppPrintSchemaMain.setOperator(mGlobalInput.Operator);
        tLPEdorAppPrintSchemaMain.setModifyDate(PubFun.getCurrentDate());
        tLPEdorAppPrintSchemaMain.setModifyTime(PubFun.getCurrentTime());
        InputStream ins = xmlexport.getInputStream();
        xmlexport.outputDocumentToFile("D:\\", "bqxml_ZHJ");
        tLPEdorAppPrintSchemaMain.setEdorInfo(ins);
        sql = "delete from LPEdorAppPrint " +
              "where EdorAcceptNo = '" + mEdorAcceptNo + "'";
        System.out.println(sql);
        mMap.put(sql, "DELETE");
        mMap.put(tLPEdorAppPrintSchemaMain, "BLOBINSERT");
        mResult.addElement(mMap);
        try {
            ins.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true ;
        }

        /**
         * 工单回退 add by fuxin
         * @param aLPEdorItemSchema LPEdorItemSchema
         * @return boolean
         */

        private boolean getDetailRB(LPEdorItemSchema aLPEdorItemSchema)
        {
            String edorNo = aLPEdorItemSchema.getEdorAcceptNo();
            String edorType = aLPEdorItemSchema.getEdorType();
            String contNo = aLPEdorItemSchema.getContNo();
            String edorAppDate = aLPEdorItemSchema.getEdorAppDate();
            String edorValidate = aLPEdorItemSchema.getEdorValiDate();
            tlistTable = new ListTable();
            tlistTable.setName("RB");
            String[] strArray = new String[5] ;
            String sql =" select c.innersource,a.insuredname,d.polno,d.getmoney ,(select edorvalidate from lpedoritem where edorno = c.innersource) from lppol a,lpedorapp b,lgwork c , ljsgetendorse d "
                       +" where a.edorno = b.edoracceptno "
                       +" and b.edoracceptno = c.workno  "
                       +" and a.polno = d.polno "
                       +" and edorno='"+edorNo+"' order by a.insuredname";
            SSRS result = new ExeSQL().execSQL(sql);
            if (result == null || result.getMaxRow()==0)
            {
                mErrors.addOneError("查询数据失败！");
                return false;
            }
            for (int i=1; i<=result.getMaxRow() ; i++)
            {
                strArray = new String[5];
                strArray[0] = result.GetText(i, 1);
                strArray[1] = result.GetText(i, 2);
                strArray[2] = result.GetText(i, 3);
                strArray[3] = result.GetText(i, 4);
                strArray[4] = result.GetText(i, 5);
                tlistTable.add(strArray);
            }
            String odlsql = " select edorValidate from lpedoritem where  edoracceptno =(select innersource from lgwork where workno ='"+edorNo+"') ";
            String odlEdorValidate = new ExeSQL().getOneValue(odlsql);
            textTag.add("Money",100);
            textTag.add("EdorNo",edorNo);
            textTag.add("EdorValidate",CommonBL.decodeDate(PubFun.calDate(edorValidate,0,"D",null)));  //本次保全生效日期
            textTag.add("odlEdorValidate",odlEdorValidate);
            xmlexport.addListTable(tlistTable,strArray);
        return true;
        }

    // 各险终止缴费
	// add by hyy
	private boolean getDetailZF(LPEdorItemSchema aLPEdorItemSchema) {
		String edorNo = aLPEdorItemSchema.getEdorNo();
		String edorType = aLPEdorItemSchema.getEdorType();
		String contNo = aLPEdorItemSchema.getContNo();
		String date = aLPEdorItemSchema.getEdorAppDate();
                tlistTable = new ListTable();
                tlistTable.setName("ZF");
                String[] strArray = new String[4];
		// 险种号码
		// 查询出表格要显示的内容
		String sql2 = "select riskseqno,lp.RiskCode ,(select lm.RiskName from lmrisk lm where lm.RiskCode = lp.RiskCode ) ,lp.PaytoDate "
				+ "from LPPol lp "
				+ "where lp.edorNo ='"
				+ edorNo
				+ "'"
				+ "and lp.edortype ='"
				+ edorType
				+ "'"
				+ "and lp.contNo ='"
				+ contNo + "'";
		SSRS result = new ExeSQL().execSQL(sql2);
		if (result == null || result.getMaxRow() == 0) {
			mErrors.addOneError("为找到查询信息！");
			return false;
		}
		for (int i = 1; i <= result.getMaxRow(); i++) {
                    strArray = new String[4];
			strArray[0] = result.GetText(i, 1);
			strArray[1] = result.GetText(i, 2);
			strArray[2] = result.GetText(i, 3);
			strArray[3] = result.GetText(i, 4);
                        tlistTable.add(strArray);
		}
		String sql = "select AppntName from LCAppnt where contNo = '" + contNo
				+ "'";
		String AppntName = new ExeSQL().getOneValue(sql);
		if (AppntName == null) {
			mErrors.addOneError("为找到查询信息！");
			return false;
		}
		textTag.add("ZFnumber", contNo);
		textTag.add("ZFdate", date);
		textTag.add("ZFclientName", AppntName);
                xmlexport.addListTable(tlistTable,strArray);

		return true;
	}
    /**
     * 设置批单显示的固定信息
     */
    private void setFixedInfo() {
        //查询受理人
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(mEdorAcceptNo);
        tLGWorkDB.getInfo();
        textTag.add("ApplyName", tLGWorkDB.getApplyName());		//申请代办人

        //查询受理渠道
        if (tLGWorkDB.getAcceptWayNo() != null) {
            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("acceptwayno");
            tLDCodeDB.setCode(tLGWorkDB.getAcceptWayNo());
            tLDCodeDB.getInfo();
            textTag.add("AcceptWay", tLDCodeDB.getCodeName());
        }

        //查询保单机构地址联系电话
        String tSQL="select distinct a.managecom from lccont a,llcontdeal b where a.contno=b.contno and b.edorno='"+ mEdorAcceptNo +"'"
        		+" union select distinct a.managecom from lbcont a,llcontdeal b where a.contno=b.contno and b.edorno='"+ mEdorAcceptNo +"' ";
        String re = new ExeSQL().getOneValue(tSQL);
        
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(re);
        tLDComDB.getInfo(); 

        textTag.add("ServicePhone", tLDComDB.getServicePhone());			//电话
        textTag.add("ServiceFax", tLDComDB.getFax());						//传真
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());  //地址
        textTag.add("ServiceZip", tLDComDB.getServicePostZipcode());		//邮编
        textTag.add("ComName", tLDComDB.getLetterServiceName());			
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));		

        textTag.add("Operator",
                    GetMemberInfo.getMemberName(mGlobalInput.Operator));
        textTag.add("Acceptor",
                    GetMemberInfo.getMemberName(getDealer(mLPEdorAppSchema)));	//受理人
    }   
    
    //受理人
    private String getDealer(LPEdorAppSchema aLPEdorAppSchema) {
        LLContDealDB tLLContDealDB = new LLContDealDB();
        tLLContDealDB.setEdorNo(aLPEdorAppSchema.getEdorAcceptNo());
        String Dealer="";
        if(tLLContDealDB.getInfo())
        {
        	Dealer = tLLContDealDB.getSchema().getAppOperator();
        }    
        return Dealer;
    }
    
    //合同处理解约
    private boolean getDetailCT(LPEdorItemSchema aLPEdorItemSchema) {
        textTag.add("CT_ContNo", aLPEdorItemSchema.getContNo());
        textTag.add("CT_EdorValiDate",
                    CommonBL.decodeDate(aLPEdorItemSchema.getEdorValiDate()));
        ListTable tlistTable = new ListTable();
        tlistTable.setName("CT");
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(aLPEdorItemSchema.getContNo());
        if (!tLCContDB.getInfo()) {
            mErrors.addOneError(new CError("查询保单号为" + tLCContDB.getContNo() +
                                           "的保单信息时失败!"));
            return false;
        }
        String msql =
                "select sum(getmoney)  from LJSGetendorse where endorsementno ='" +
                aLPEdorItemSchema.getEdorNo() + "' and FeeOperationType ='" +
                aLPEdorItemSchema.getEdorType() + "'";
        ExeSQL aExeSQL = new ExeSQL();
        String tReturn = aExeSQL.getOneValue(msql);
        System.out.println("msql:" + msql);
        if (tReturn == null || StrTool.cTrim(tReturn).equals("")) {
            mErrors.addOneError(new CError("没有相关的合同期退保记录！"));
            return false;
        }
        textTag.add("CT_Total", Math.abs(Double.parseDouble(tReturn)));

        //这里对万能险退保进行特殊处理
        if(CommonBL.hasULIRisk(aLPEdorItemSchema.getContNo())){
            String sql =
                    "select MoneyType, nvl(abs(sum(Money)), 0) from LPInsureAccTrace "
                    + "where OtherType = '10' "
                    + "   and OtherNo = '"
                    + aLPEdorItemSchema.getEdorNo() + "' "
                    + "   and ContNo = '" + aLPEdorItemSchema.getContNo() + "' "
                    + "   and MoneyType in('LX', 'MF', 'RP') "
                    + "group by MoneyType ";
            SSRS tSSRS = new ExeSQL().execSQL(sql);
            if(tSSRS.getMaxRow()==0){
                System.out.println("查询结算信息失败！");
            }else{
                String ULIInfo = "补计利息：" + tSSRS.GetText(1, 2) +
                                 "元，补扣保单管理费:" +tSSRS.GetText(2, 2)+ "元，补扣风险保费:"+tSSRS.GetText(3, 2) +
                                 "元";
                sql = "select sum(insuaccbala) from lpinsureacc where edorno = '" + aLPEdorItemSchema.getEdorNo() 
                	+ "' and edortype = '" + aLPEdorItemSchema.getEdorType() + "' and contno = '" + aLPEdorItemSchema.getContNo() + "' with ur";
                String tBala= new ExeSQL().getOneValue(sql);
                if(tSSRS.getMaxRow()>0)
                {
                	ULIInfo = ULIInfo + ",当前账户金额为："+tBala+"元。"
                    		+"解约手续费:"+CommonBL.bigDoubleToCommonString((Double.parseDouble(tBala)+Double.parseDouble(tReturn)),"0.00")
                    		+"元，";
                }
                textTag.add("CT_ULIInfo", ULIInfo);
            }
        }

        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPPolDB.setEdorType(aLPEdorItemSchema.getEdorType());
        tLPPolDB.setContNo(aLPEdorItemSchema.getContNo());
        LPPolSet tLPPolSet = tLPPolDB.query();
        if (tLPPolSet == null || tLPPolSet.size() <= 0) {
            CError tError = new CError();
            tError.moduleName = "PrtEndorsementBL";
            tError.functionName = "getDetailCT";
            tError.errorMessage = "LCPol不存在相应的记录!";
            this.mErrors.addOneError(tError);
            return false;

        }

        //险种退费情况
        StringBuffer sql = new StringBuffer();
        sql.append("  select polNo, riskCode, insuredNo, sum(getMoney) ")
                .append("from LJSGetEndorse ")
                .append("where endorsementNo = '")
                .append(aLPEdorItemSchema.getEdorNo())
                .append("'    and feeOperationType = '")
                .append(aLPEdorItemSchema.getEdorType())
                .append("'    and contNo = '")
                .append(aLPEdorItemSchema.getContNo())
                .append("' group by polNo, riskCode, insuredNo");
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql.toString());

        if (tSSRS != null && tSSRS.getMaxRow() > 0) {
            for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                String printContent[] = new String[1];

                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setPolNo(tSSRS.GetText(i, 1));
                if (!tLCPolDB.getInfo()) {
                    mErrors.addOneError("没有查到险种号" + tSSRS.GetText(i, 1)
                                        + "所对应的险种");
                    return false;
                }
                LMRiskDB tLMRiskDB = new LMRiskDB();
                tLMRiskDB.setRiskCode(tSSRS.GetText(i, 2));
                if (!tLMRiskDB.getInfo()) {
                    mErrors.addOneError("没有查到险种编码" + tSSRS.GetText(i, 1)
                                        + "所对应的险种信息");
                    return false;
                }
                printContent[0] =
                        "被保险人" + CommonBL.getInsuredName(tSSRS.GetText(i, 3))
                        + "，险种（"
                        + tLMRiskDB.getRiskName() + "）解约，" + "应退保险费"
                        + Math.abs(CommonBL.carry(tSSRS.GetText(i, 4)))
                        + "元。";
                tlistTable.add(printContent);
            }
        }
        String strhead[] = new String[1];
        strhead[0] = "解约";
        xmlexport.addListTable(tlistTable, strhead);

        //如果客户无有效保单显示余额信息
        createAppAccList(aLPEdorItemSchema);
        return true;
    }
    //合同终止 
    private boolean getDetailHZ(LPEdorItemSchema aLPEdorItemSchema) {
        textTag.add("CT_ContNo", aLPEdorItemSchema.getContNo());
        textTag.add("CT_EdorValiDate",
                    CommonBL.decodeDate(aLPEdorItemSchema.getEdorValiDate()));
        ListTable tlistTable = new ListTable();
        tlistTable.setName("HZ");
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(aLPEdorItemSchema.getContNo());
        if (!tLCContDB.getInfo()) {
            mErrors.addOneError(new CError("查询保单号为" + tLCContDB.getContNo() +
                                           "的保单信息时失败!"));
            return false;
        }       
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPPolDB.setEdorType(aLPEdorItemSchema.getEdorType());
        tLPPolDB.setContNo(aLPEdorItemSchema.getContNo());
        LPPolSet tLPPolSet = tLPPolDB.query();
        if (tLPPolSet == null || tLPPolSet.size() <= 0) {
            CError tError = new CError();
            tError.moduleName = "PrtEndorsementBL";
            tError.functionName = "getDetailCT";
            tError.errorMessage = "LCPol不存在相应的记录!";
            this.mErrors.addOneError(tError);
            return false;

        }      
        String strhead[] = new String[1];
        strhead[0] = "解约";
        xmlexport.addListTable(tlistTable, strhead);

        //如果客户无有效保单显示余额信息
        createAppAccList(aLPEdorItemSchema);
        return true;
    }
    //理赔合同终止退费
    private boolean getDetailCD(LPEdorItemSchema aLPEdorItemSchema) {
        textTag.add("CD_ContNo", aLPEdorItemSchema.getContNo());
        textTag.add("CD_EdorValiDate",
                    CommonBL.decodeDate(aLPEdorItemSchema.getEdorValiDate()));
        ListTable tlistTable = new ListTable();
        tlistTable.setName("CD");
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(aLPEdorItemSchema.getContNo());
        if (!tLCContDB.getInfo()) {
            mErrors.addOneError(new CError("查询保单号为" + tLCContDB.getContNo() +
                                           "的保单信息时失败!"));
            return false;
        }
        String msql =
                "select sum(getmoney)  from LJSGetendorse where endorsementno ='" +
                aLPEdorItemSchema.getEdorNo() + "' and FeeOperationType ='" +
                aLPEdorItemSchema.getEdorType() + "'";
        ExeSQL aExeSQL = new ExeSQL();
        String tReturn = aExeSQL.getOneValue(msql);
        System.out.println("msql:" + msql);
        if (tReturn == null || StrTool.cTrim(tReturn).equals("")) {
            mErrors.addOneError(new CError("没有相关的合同期退保记录！"));
            return false;
        }
        textTag.add("CD_Total", Math.abs(Double.parseDouble(tReturn)));

        //这里对万能险退保进行特殊处理
        if(CommonBL.hasULIRisk(aLPEdorItemSchema.getContNo())){
            String sql =
                    "select MoneyType, nvl(abs(sum(Money)), 0) from LPInsureAccTrace "
                    + "where OtherType = '10' "
                    + "   and OtherNo = '"
                    + aLPEdorItemSchema.getEdorNo() + "' "
                    + "   and ContNo = '" + aLPEdorItemSchema.getContNo() + "' "
                    + "   and MoneyType in('LX', 'MF', 'RP') "
                    + "group by MoneyType ";
            SSRS tSSRS = new ExeSQL().execSQL(sql);
            if(tSSRS.getMaxRow()==0){
                System.out.println("查询结算信息失败！");
            }else{
                String ULIInfo = "补计利息：" + tSSRS.GetText(1, 2) +
                                 "元，补扣保单管理费:" +tSSRS.GetText(2, 2)+ "元，补扣风险保费:"+tSSRS.GetText(3, 2) +
                                 "元";
                sql = "select sum(insuaccbala) from lpinsureacc where edorno = '" + aLPEdorItemSchema.getEdorNo() 
                	+ "' and edortype = '" + aLPEdorItemSchema.getEdorType() + "' and contno = '" + aLPEdorItemSchema.getContNo() + "' with ur";
                String tBala= new ExeSQL().getOneValue(sql);
                if(tSSRS.getMaxRow()>0)
                {
                	ULIInfo = ULIInfo + ",当前账户金额为："+tBala+"元。"
                    		+"解约手续费:"+CommonBL.bigDoubleToCommonString((Double.parseDouble(tBala)+Double.parseDouble(tReturn)),"0.00")
                    		+"元，";
                }
                textTag.add("CD_ULIInfo", ULIInfo);
            }
        }

        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPPolDB.setEdorType(aLPEdorItemSchema.getEdorType());
        tLPPolDB.setContNo(aLPEdorItemSchema.getContNo());
        LPPolSet tLPPolSet = tLPPolDB.query();
        if (tLPPolSet == null || tLPPolSet.size() <= 0) {
            CError tError = new CError();
            tError.moduleName = "PrtEndorsementBL";
            tError.functionName = "getDetailCD";
            tError.errorMessage = "LCPol不存在相应的记录!";
            this.mErrors.addOneError(tError);
            return false;

        }

        //险种退费情况
        StringBuffer sql = new StringBuffer();
        sql.append("  select polNo, riskCode, insuredNo, sum(getMoney) ")
                .append("from LJSGetEndorse ")
                .append("where endorsementNo = '")
                .append(aLPEdorItemSchema.getEdorNo())
                .append("'    and feeOperationType = '")
                .append(aLPEdorItemSchema.getEdorType())
                .append("'    and contNo = '")
                .append(aLPEdorItemSchema.getContNo())
                .append("' group by polNo, riskCode, insuredNo");
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql.toString());

        if (tSSRS != null && tSSRS.getMaxRow() > 0) {
            for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                String printContent[] = new String[1];

                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setPolNo(tSSRS.GetText(i, 1));
                if (!tLCPolDB.getInfo()) {
                    mErrors.addOneError("没有查到险种号" + tSSRS.GetText(i, 1)
                                        + "所对应的险种");
                    return false;
                }
                LMRiskDB tLMRiskDB = new LMRiskDB();
                tLMRiskDB.setRiskCode(tSSRS.GetText(i, 2));
                if (!tLMRiskDB.getInfo()) {
                    mErrors.addOneError("没有查到险种编码" + tSSRS.GetText(i, 1)
                                        + "所对应的险种信息");
                    return false;
                }
                printContent[0] =
                        "被保险人" + CommonBL.getInsuredName(tSSRS.GetText(i, 3))
                        + "，险种（"
                        + tLMRiskDB.getRiskName() + "）合同终止退费，" + "应退保险费"
                        + Math.abs(CommonBL.carry(tSSRS.GetText(i, 4)))
                        + "元。";
                tlistTable.add(printContent);
            }
        }
        String strhead[] = new String[1];
        strhead[0] = "合同终止退费";
        xmlexport.addListTable(tlistTable, strhead);

        //如果客户无有效保单显示余额信息
        createAppAccList(aLPEdorItemSchema);
        return true;
    }
  
    /**
     * 查询特殊数据信息
     * @param tLPGrpEdorItemSchema LPGrpEdorItemSchema：保全项目
     * @param polNo String：险种号
     * @param detailType String：类型
     * @return String
     */
    private String getSpecialData(LPEdorItemSchema tLPEdorItemSchema,
                                  String polNo, String detailType) {
        EdorItemSpecialData tEdorItemSpecialData
                = new EdorItemSpecialData(tLPEdorItemSchema);
        tEdorItemSpecialData.setPolNo(polNo);
        if (!tEdorItemSpecialData.query()) {
            return null;
        }
        return tEdorItemSpecialData.getEdorValue(detailType);
    }

    /**
     * 交费频次变更
     * @param aLPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailPC(LPEdorItemSchema aLPEdorItemSchema) {
        ListTable tlistTable = new ListTable();
        tlistTable.setName("PC");

        String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();

        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(edorNo);
        tLPPolDB.setEdorType(edorType);
        tLPPolDB.setContNo(contNo);
        LPPolSet tLPPolSet = tLPPolDB.query();

        for (int i = 1; i <= tLPPolSet.size(); i++) {
            LPPolSchema tLPPolSchema = tLPPolSet.get(i);
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(tLPPolSchema.getPolNo());
            if (!tLCPolDB.getInfo()) {
                mErrors.addOneError("未找到险种信息！");
                return false;
            }
            int oldPayIntv = tLCPolDB.getPayIntv();
            int newPayIntv = tLPPolSchema.getPayIntv();
            if (oldPayIntv != newPayIntv) {
                String content = "序号" + tLPPolSchema.getRiskSeqNo() +
                                 "险种" + tLPPolSchema.getRiskCode() +
                                 "交费频次从" +
                                 ChangeCodeBL.getCodeName("payintv",
                        String.valueOf(oldPayIntv)) +
                                 "变为" +
                                 ChangeCodeBL.getCodeName("payintv",
                        String.valueOf(newPayIntv)) +
                                 "，期交保费变更为" + tLPPolSchema.getPrem() +
                                 "元，从下期生效。";
//                LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
//                tLJSGetEndorseDB.setEndorsementNo(edorNo);
//                tLJSGetEndorseDB.setFeeOperationType(edorType);
//                tLJSGetEndorseDB.setContNo(contNo);
//                tLJSGetEndorseDB.setPolNo(tLPPolSchema.getPolNo());
//                LJSGetEndorseSet tLJSGetEndorseSet = tLJSGetEndorseDB.query();
//                if (tLJSGetEndorseSet.size() > 0)
//                {
//                    double getMoney = tLJSGetEndorseSet.get(1).getGetMoney();
//                    if (getMoney < 0)
//                    {
//                        content += "，应退费" + Math.abs(getMoney) + "元\n";
//                    }
//                    else if (getMoney > 0)
//                    {
//                        content += "，应补费" + getMoney + "元\n";
//                    }
//                }

                String[] column = new String[1];
                column[0] = content;
                tlistTable.add(column);
            }
        }
        xmlexport.addListTable(tlistTable, new String[1]);
        return true;
    }

    /**
     * 减少保额
     * @param aLPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailPT(LPEdorItemSchema aLPEdorItemSchema) {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(aLPEdorItemSchema.getContNo());
        if (!tLCContDB.getInfo()) {
            mErrors.addOneError(new CError("查询保单号为" + tLCContDB.getContNo() +
                                           "的保单信息时失败!"));
            return false;
        }
        textTag.add("PT_ContNo", aLPEdorItemSchema.getContNo());
        tlistTable = new ListTable();
        tlistTable.setName("PT");
        //查找相应的保全项目
        String sql = "select * from LPEdorItem " +
                     "where EdorAcceptNo = '" +
                     aLPEdorItemSchema.getEdorAcceptNo() + "' " +
                     "and EdorType='" +
                     aLPEdorItemSchema.getEdorType() + "' " +
                     "order by edorno,insuredno";
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        if (tLPEdorItemSet == null || tLPEdorItemSet.size() < 1) {
            CError tError = new CError();
            tError.moduleName = "LLContDealCalPrintBL";
            tError.functionName = "getInsuredPT";
            tError.errorMessage = "申请号" + aLPEdorItemSchema.getEdorAcceptNo() +
                                  "下无相应的保全项目信息!";
            this.mErrors.addOneError(tError);
            return false;
        }

        for (int i = 1; i <= tLPEdorItemSet.size(); i++) {
            LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(i); ;
            //取险种名称
            LPPolDB tLPPolDB = new LPPolDB();
            tLPPolDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPPolDB.setEdorType(tLPEdorItemSchema.getEdorType());
            tLPPolDB.setContNo(tLPEdorItemSchema.getContNo());
            tLPPolDB.setPolNo(tLPEdorItemSchema.getPolNo());

            LPPolSet tLPPolSet = tLPPolDB.query();
            LPPolSchema tLPPolSchema = tLPPolSet.get(1);

            sql = "select GetMoney from LJSGetEndorse " +
                  "where EndorsementNo = '" +
                  tLPEdorItemSchema.getEdorNo() + "' " +
                  "and FeeOperationType = '" +
                  tLPEdorItemSchema.getEdorType() + "' " +
                  "and InsuredNo = '" +
                  tLPPolSchema.getInsuredNo() + "' " +
                  "and RiskCode = '" +
                  tLPPolSchema.getRiskCode() + "'";
            ExeSQL tExeSQL = new ExeSQL();
            tExeSQL = new ExeSQL();
            String chgPrem = tExeSQL.getOneValue(sql);

            LMRiskDB tLMRiskDB = new LMRiskDB();
            tLMRiskDB.setRiskCode(tLPPolSchema.getRiskCode());
            if (!tLMRiskDB.getInfo()) {
                CError tError = new CError();
                tError.moduleName = "LLContDealCalPrintBL";
                tError.functionName = "getInsuredCM";
                tError.errorMessage = "LMRisk不存在相应的记录!";
                this.mErrors.addOneError(tError);
                return false;
            }

            String[] fee = new String[1];
            fee[0] = "保单" + tLPEdorItemSchema.getContNo() + "序号" +
                     tLPPolSchema.getRiskSeqNo() +
                     "险种（" + tLMRiskDB.getRiskName() + "）" +
                     "保额变更为" + CommonBL.carry(tLPPolSchema.getAmnt()) + "元。\n从" +
                     CommonBL.decodeDate(tLPEdorItemSchema.getEdorValiDate()) +
                     "起，期交保费变更为：" + tLPPolSchema.getPrem() +
                     "元，应退费" + CommonBL.carry(chgPrem) + "元。";
            tlistTable.add(fee);
        }

        String[] feeHead = new String[1];
        feeHead[0] = "费用";
        xmlexport.addListTable(tlistTable, feeHead);
        return true;
    }

    /**
     * 预收保费
     * @param aLPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailYS(LPEdorItemSchema aLPEdorItemSchema) {
        String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(contNo);
        if (!tLCAppntDB.getInfo()) {
            mErrors.addOneError("未找到投保人信息！");
            return false;
        }
        AppAcc appAcc = new AppAcc();
        LCAppAccTraceSchema tLCAppAccTraceSchema =
                appAcc.getLCAppAccTrace(tLCAppntDB.getAppntNo(), edorNo,
                                        BQ.NOTICETYPE_P);
        textTag.add("YS_Money", String.valueOf(tLCAppAccTraceSchema.getMoney()));
        textTag.add("YS_AccBala",
                    String.valueOf(tLCAppAccTraceSchema.getAccBala()));
        textTag.add("YS_EdorValiDate",
                    CommonBL.decodeDate(aLPEdorItemSchema.getEdorValiDate()));
        return true;
    }

    /**
     * 个单迁移
     * @param aLPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailPR(LPEdorItemSchema aLPEdorItemSchema) {
        String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(contNo);
        if (!tLCAppntDB.getInfo()) {
            mErrors.addOneError("未找到投保人信息！");
            return false;
        }
        EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(
                aLPEdorItemSchema);
        tEdorItemSpecialData.query();
        String inCom = tEdorItemSpecialData.getEdorValue("incom");
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(inCom);
        if (!tLDComDB.getInfo()) {
            CError.buildErr(this, "查询转移机构失败");
            return false;
        }
        LPAddressDB tLPAddressDB = new LPAddressDB();
        tLPAddressDB.setEdorNo(edorNo);
        tLPAddressDB.setEdorType(edorType);
        LPAddressSet tLPAddressSet = tLPAddressDB.query();

        textTag.add("PR_CONTNO", contNo);
        textTag.add("PR_INCOM", tLDComDB.getName());
        if (tLPAddressSet.size() > 0) {
            textTag.add("PR_ADDRESS", tLPAddressSet.get(1).getPostalAddress());
            textTag.add("PR_ZIPCODE", tLPAddressSet.get(1).getZipCode());
        }
        if (!inCom.substring(0,
            4).equals(tEdorItemSpecialData.getEdorValue("outcom").substring(0,
                4))) {
            textTag.add("PR_COM", "迁移后本保单的指定医院清单将发生变化，敬请留意");
        }

        String tsql = "select count(0) from lmriskapp where risktype ='M' "
                      +
                "and riskcode in(select riskcode from lcpol where contno ='" +
                      contNo + "')"
                      ;

        String re = new ExeSQL().getOneValue(tsql);
        if (!re.equals("0")) {
            textTag.add("PR_HEL",
                    "“您所享受的健康管理服务计划所包含的服务项目和服务标准，将根据保单迁移有关规则进行调整，具体调整情况可向您的业务员咨询。”");
        }

        return true;
    }

    //  万能部分领取 080414 zhanggm
    private boolean getDetailLQ(LPEdorItemSchema aLPEdorItemSchema)
    {
    	String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();
        String edorValiDate = aLPEdorItemSchema.getEdorValiDate();
        textTag.add("LQ_ContNo", contNo);
        textTag.add("LQ_EdorValiDate", CommonBL.decodeDate(edorValiDate));

        String content = "";
        String appMoney = "";
        String manageMoney = "";
        String leftMoney = "";
        String getMoney = "";
        EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorNo,edorType);
        if(!tSpecialData.query())
        {
        	mErrors.addOneError("未找到本次领取金额！");
            return false;
        }
        String [] polnos = tSpecialData.getGrpPolNos();
        String polno = polnos[0];
        tSpecialData.setPolNo(polno);

        appMoney = tSpecialData.getEdorValue("AppMoney");
    	manageMoney = tSpecialData.getEdorValue("ManageMoney");
    	leftMoney = tSpecialData.getEdorValue("LeftMoney");
    	getMoney = tSpecialData.getEdorValue("GetMoney");

        if (appMoney != null && !("0").equals(appMoney) && !("").equals(appMoney))
        {
            content += "万能险账户：本次领取金额" + appMoney + "元，"
                     + "领取后帐户余额：" + leftMoney + "元，\n"
                     + "扣除手续费金额：" + manageMoney + "元，"
                     + "本次实际领取金额：" + getMoney + "元。\n\n";
        }
        else
        {
        	mErrors.addOneError("领取信息出错，请重新理算！");
            return false;
        }

        content += "本变更自" + CommonBL.decodeDate(edorValiDate) + "开始生效";
        String[] column = new String[1];
        column[0] = content;
        ListTable listTable = new ListTable();
        listTable.setName("LQ");
        listTable.add(column);
        xmlexport.addListTable(listTable, new String[1]);
        return true;
    }
    //   个险万能保障调整 080514 pst
    private boolean getDetailBA(LPEdorItemSchema aLPEdorItemSchema)
    {
        String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();
        String edorValiDate = aLPEdorItemSchema.getEdorValiDate();
        textTag.add("BA_ContNo", contNo);
        textTag.add("BA_EdorValiDate", CommonBL.decodeDate(edorValiDate));

        String polNo = null;
        LCPolDB tLCPolDB = new LCPolDB();
        String tSql="select * from lcpol where contno='"+contNo+"' and polno=mainPolNo";
        LCPolSchema tLCPolSchema= tLCPolDB.executeQuery(tSql).get(1);
        if(tLCPolSchema==null)
        {
            CError tError = new CError();
            tError.moduleName = "LLContDealCalPrintBL";
            tError.functionName = "getDetailBA";
            tError.errorMessage = "申请号" + aLPEdorItemSchema.getEdorAcceptNo() +
                                  "下无相应的保单信息!";
            this.mErrors.addOneError(tError);
        }

        String content = "";
        LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorAcceptNo(edorNo);
        tLPEdorEspecialDataDB.setEdorType(edorType);
        //tLPEdorEspecialDataDB.setDetailType("BA");
        tLPEdorEspecialDataSchema = tLPEdorEspecialDataDB.query().get(1);
        double appMoney = 0.0;
        try
        {
        	appMoney = Double.parseDouble(tLPEdorEspecialDataSchema.getEdorValue());
        }
        catch (NumberFormatException ex)
        {
            ex.printStackTrace();
            appMoney = 0;
        }
        if (appMoney != 0)
        {
            content += "个险万能基本保额调整：原基本保额" + tLCPolSchema.getAmnt() + "元，"
                     + "调整后基本保额：" + appMoney + "元\n";
        }

        textTag.add("tOldAmnt", tLCPolSchema.getAmnt());
        textTag.add("tNewAmnt", appMoney);
        content += "本变更自" + CommonBL.decodeDate(edorValiDate) + "开始生效";
        String[] column = new String[1];
        column[0] = content;
        ListTable listTable = new ListTable();
        listTable.setName("BA");
        listTable.add(column);
        xmlexport.addListTable(listTable, new String[1]);
      //  textTag.add("LQ_GetMoney",CommonBL.bigDoubleToCommonString(grpMoney + grpFixMoney + insuredGetMoney, "0.00"));
        return true;
    }

    //   个险万能保障调整 080514 pst
    private boolean getDetailZB(LPEdorItemSchema aLPEdorItemSchema)
    {
        String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();
        String edorValiDate = aLPEdorItemSchema.getEdorValiDate();
        String typeName="";
        textTag.add("ZB_ContNo", contNo);
        String polNo = null;
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(contNo);
        LCContSchema tLCContSchema= tLCContDB.query().get(1);
        if(tLCContSchema==null)
        {
            CError tError = new CError();
            tError.moduleName = "LLContDealCalPrintBL";
            tError.functionName = "getDetailZB";
            tError.errorMessage = "申请号" + aLPEdorItemSchema.getEdorAcceptNo() +
                                  "下无相应的保单信息!";
            this.mErrors.addOneError(tError);
        }

        String content = "";
        LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorAcceptNo(edorNo);
        tLPEdorEspecialDataDB.setEdorType(edorType);
        tLPEdorEspecialDataSchema = tLPEdorEspecialDataDB.query().get(1);
        double appMoney = 0.0;
        double tSumFee=0.0;
        //本次是补费
        if("BF".equals(tLPEdorEspecialDataSchema.getDetailType()))
        {
        	typeName="补交保费";
            try
            {
            	appMoney = Double.parseDouble(tLPEdorEspecialDataSchema.getEdorValue());
            }
            catch (NumberFormatException ex)
            {
                ex.printStackTrace();
                appMoney = 0;
            }
        }
        //追加保费
        else if("ZF".equals(tLPEdorEspecialDataSchema.getDetailType()))
        {
        	typeName="追加保费";
            try
            {
            	appMoney = Double.parseDouble(tLPEdorEspecialDataSchema.getEdorValue());

            }
            catch (NumberFormatException ex)
            {
                ex.printStackTrace();
                appMoney = 0;
            }
        }
        //扣费金额
        String tSql="select value(sum(money),0) from lpinsureacctrace  where edorno='"+edorNo+"' and edortype='ZB' and moneytype in('GL','KF') ";
        ExeSQL tExeSQL=new ExeSQL();
        tSumFee=Double.parseDouble(tExeSQL.getOneValue(tSql));
        tSql="select value(insuaccbala,0) from lcinsureacc where contno='"+contNo+"'";
        //当前帐户价值
        String accBala=tExeSQL.getOneValue(tSql);
        //追加后的帐户价值
        tSql="select value(insuaccbala,0) from lpinsureacc where  edorno='"+edorNo+"' and edortype='ZB'";
        String afterBala=tExeSQL.getOneValue(tSql);
        //持续奖励
        tSql="select value(sum(money),0) from lpinsureacctrace  where edorno='"+edorNo+"' and edortype='ZB' and moneytype in('B')";
        String JL=tExeSQL.getOneValue(tSql);

        if (appMoney != 0)
        {
            content += "个险万能追加保费/补交保费：交费金额" + appMoney + "元\n" +
            		"    扣除费用累计:" + tSumFee  +"";
        }
        textTag.add("TypeName", typeName);
        textTag.add("SumFee", tSumFee);
        textTag.add("tSumMoney", appMoney);
        textTag.add("AccBala", accBala);
        textTag.add("AfterBala", afterBala);
        textTag.add("JL", JL);
        content += "本变更自" + CommonBL.decodeDate(edorValiDate) + "开始生效";
        String[] column = new String[1];
        column[0] = content;
        ListTable listTable = new ListTable();
        listTable.setName("ZB");
        listTable.add(column);
        xmlexport.addListTable(listTable, new String[1]);

        return true;
    }
    
    
//  万能险帐户金额转健管服务(尊享人生) 100729	xp
    private boolean getDetailZX(LPEdorItemSchema aLPEdorItemSchema)
    {
    	String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();
        String edorValiDate = aLPEdorItemSchema.getEdorValiDate();
        textTag.add("ZX_ContNo", contNo);
        textTag.add("ZX_EdorValiDate", CommonBL.decodeDate(edorValiDate));
        
        
        tlistTable = new ListTable();
        tlistTable.setName("ZX");
        String[] strArray = new String[6];
        String sql = " select db2inst1.codename('serclass1',a.serclass1), db2inst1.codename('serclass2',a.serclass2),  "
        			+" (case a.comcode when '8611' then '北京' when '8621' then '辽宁' when '8631' then '上海' when '8612' then '天津' when '8633' then '浙江' end), " 
        			+" db2inst1.codename('serclass3',a.serclass3), a.amount, db2inst1.codename('quantifiers',a.quantifiers), (select edorvalue  from lpedorespecialdata where edorno='"+edorNo+"' and edortype='ZX' and detailtype='ZXFWXM' and polno=char(a.seqno)) "
        			+" from ldzxhealth a where seqno in (select int(polno) from lpedorespecialdata  where edorno='"+edorNo+"' and edortype='ZX' and detailtype='ZXFWXM') with ur";
        SSRS result = new ExeSQL().execSQL(sql);
        if (result == null || result.getMaxRow() == 0) {
            mErrors.addOneError("查询数据失败！");
            return false;
        }
        for (int i = 1; i <= result.getMaxRow(); i++) {
            strArray = new String[7];
            strArray[0] = result.GetText(i, 1);
            strArray[1] = result.GetText(i, 2);
            strArray[2] = result.GetText(i, 3);
            strArray[3] = result.GetText(i, 4);
            strArray[4] = result.GetText(i, 5);
            strArray[5] = result.GetText(i, 6);
            strArray[6] = result.GetText(i, 7);
            tlistTable.add(strArray);
        }
        xmlexport.addListTable(tlistTable, strArray);
        
        

        String content = "";
        String appMoney = "";
        String leftMoney = "";
        String getMoney = "";
        EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorNo,edorType);
        if(!tSpecialData.query())
        {
        	mErrors.addOneError("未找到本次领取金额！");
            return false;
        }
//        String [] polnos = tSpecialData.getGrpPolNos();
//        String polno = polnos[0];
        tSpecialData.setPolNo("000000");

        appMoney = tSpecialData.getEdorValue("AppMoney");
    	leftMoney = tSpecialData.getEdorValue("LeftMoney");
    	getMoney = tSpecialData.getEdorValue("GetMoney");
    	
        if (appMoney != null && !("0").equals(appMoney) && !("").equals(appMoney))
        {
            content += "万能险账户：本次转出金额" + appMoney + "元，"
                     + "消费后帐户余额：" + leftMoney + "元，\n"
                     + "本次实际消费金额：" + getMoney + "元。\n";
        }
        else
        {
        	mErrors.addOneError("领取信息出错，请重新理算！");
            return false;
        }

        content += "本变更自" + CommonBL.decodeDate(edorValiDate) + "开始生效";
        textTag.add("ZX_Detail", content);
        
        return true;
    }


    private boolean getDetailAppAccShiftTo(LCAppAccTraceSchema
                                           pLCAppAccTraceSchema) {
        xmlexport.addDisplayControl("displayAppAccShiftTo");
        textTag.add("AppAcc_CustomerName",
                    getCustomerName(pLCAppAccTraceSchema.getCustomerNo()));
        textTag.add("AppAcc_Bala", pLCAppAccTraceSchema.getAccBala());
        return true;
    }

    private boolean getDetailAppAccTakeOut(LCAppAccTraceSchema
                                           pLCAppAccTraceSchema) {
        xmlexport.addDisplayControl("displayAppAccTakeOut");
        textTag.add("AppAcc_Money", pLCAppAccTraceSchema.getMoney());
        textTag.add("AppAcc_CustomerName",
                    getCustomerName(pLCAppAccTraceSchema.getCustomerNo()));
        textTag.add("AppAcc_Bala", pLCAppAccTraceSchema.getAccBala());
        textTag.add("AppAcc_RealPay",
                    mLPEdorAppSchema.getGetMoney() +
                    pLCAppAccTraceSchema.getMoney());

        return true;
    }

    //查询客户名称
    private String getCustomerName(String customerNo) {
        LCAddressSchema tLCAddressSchema = null;
        LDPersonDB tLDPersonDB = new LDPersonDB();
        tLDPersonDB.setCustomerNo(customerNo);
        if (!tLDPersonDB.getInfo()) {
            mErrors.addOneError("没有查询到受理信息。");
            return "";
        }
        return tLDPersonDB.getName();
    }

    //对没有的保全项目类型生成空打迎数据
    private boolean getDetailForBlankType(LPEdorItemSchema
                                          aLPEdorItemSchema) {
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "PrtEndorsementBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 生成帐户余额清单
     */
    private boolean createAppAccList(LPEdorItemSchema aLPEdorItemSchema) {
        //判断客户是否有有效保单
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(aLPEdorItemSchema.getContNo());
        if (!tLCAppntDB.getInfo()) {
            mErrors.addOneError("查询投保人信息出错！");
            return false;
        }

        //若本次保全后投保人无有效保单，则需要提醒客户领取投保人账户余额
        //这种情况只可能出像在保全项目为CT,XT,WT且选择了全部险种的时候
        String sql = "select * from LCCont a " +
                     "where Appflag = '1' " +
                     "and ContType = '1' " +
                     "and AppntNo = '" + tLCAppntDB.getAppntNo() + "' " +
                     "and not exists "
                     + "  (select * from LPCont b " +
                     "    where b.EdorNo = '" + aLPEdorItemSchema.getEdorNo() +
                     "' " +
                     "      and b.ContNo = a.ContNo " +
                     "      and b.EdorType in ('CT', 'XT', 'WT') "
                     + "    and (select count(1) from LPPol " //选择全部险种
                     + "      where edorNo = b.edorNo "
                     + "         and edorType = b.edorType "
                     + "         and contNo = b.contNo)  "
                     + "      = "
                     +
                     "      (select count(1) from LCPol where contNo = b.contNo) "
                     + " ) ";
        System.out.println("acc sql:" + sql);
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = tLCContDB.executeQuery(sql);
        if (tLCContSet.size() == 0) {
            String customerNo = tLCAppntDB.getAppntNo();
            String customerName = tLCAppntDB.getAppntName();
            textTag.add("CustomerNo", customerNo);
            textTag.add("CustomerName", customerName);
            tlistTable = new ListTable();
            LCAppAccTraceDB tLCAppAccTraceDB = new LCAppAccTraceDB();
            tLCAppAccTraceDB.setCustomerNo(customerNo);
            LCAppAccTraceSet tLCAppAccTraceSet = tLCAppAccTraceDB.query();
            if (tLCAppAccTraceSet.size() > 0) {
                xmlexport.addDisplayControl("displayAcc");
            }
            for (int i = 1; i <= tLCAppAccTraceSet.size(); i++) {
                LCAppAccTraceSchema tLCAppAccTraceSchema = tLCAppAccTraceSet.
                        get(i);
                String[] info = new String[6];
                info[0] = String.valueOf(i);
                info[1] = CommonBL.decodeDate(tLCAppAccTraceSchema.
                                              getConfirmDate());
                info[2] = String.valueOf(tLCAppAccTraceSchema.getMoney());
                info[3] = StrTool.cTrim(ChangeCodeBL.getCodeName(
                        "appaccdestsource",
                        tLCAppAccTraceSchema.getDestSource()));
                info[4] = StrTool.cTrim(tLCAppAccTraceSchema.getOtherNo());
                info[5] = String.valueOf(tLCAppAccTraceSchema.getAccBala());
                tlistTable.add(info);
            }
            tlistTable.setName("APPACC");
            xmlexport.addListTable(tlistTable, new String[6]);
        }
        return true;
    }

//  20070717 zhanggm TB批单显示增加人工核保意见
    private String getSugUWIdea(LPEdorItemSchema aLPEdorItemSchema)
    {
    	String idea = "";
    	LPUWMasterDB tLPUWMasterDB = new LPUWMasterDB();
    	tLPUWMasterDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
    	tLPUWMasterDB.setEdorType(aLPEdorItemSchema.getEdorType());
    	LPUWMasterSet tLPUWMasterSet = tLPUWMasterDB.query();
    	for(int i=1;i<=tLPUWMasterSet.size();i++)
    	{
    		LPUWMasterSchema tLPUWMasterSchema = tLPUWMasterSet.get(i).getSchema();
    		LPPolDB tLPPolDB = new LPPolDB();
            tLPPolDB.setEdorNo(tLPUWMasterSchema.getEdorNo());
//            tLPPolDB.setEdorType(tLPUWMasterSchema.getEdorType());
            tLPPolDB.setPolNo(tLPUWMasterSchema.getPolNo());
            LPPolSet tLPPolSet = new LPPolSet();
            tLPPolSet = tLPPolDB.query();
            if(tLPPolSet.size() >= 1)
            {
            	for(int j=1;j<=tLPPolSet.size();j++)
            	{
            		LPPolSchema tLPPolSchema = tLPPolSet.get(j);
        		    String tSugUWIdea = null;
        	    	tSugUWIdea = tLPUWMasterSchema.getSugUWIdea().trim();
        	    	if((null != tSugUWIdea) && (!"".equals(tSugUWIdea)) && (!"null".equals(tSugUWIdea)))
        	    	{
        		    	idea += "序号" + tLPPolSchema.getRiskSeqNo()
        		    	      + "，险种名称 "  + CommonBL.getRiskName(tLPPolSchema.getRiskCode())
                              + "，代码" + tLPPolSchema.getRiskCode()
                              + "（被保险人" + tLPPolSchema.getInsuredName() + "）的核保意见是：" + tSugUWIdea + "\n";
        		    }
            	}
            }
    	}
    	LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPPolDB.setEdorType(aLPEdorItemSchema.getEdorType());
        LPPolSet tLPPolSet = tLPPolDB.query();
        for (int i = 1; i <= tLPPolSet.size(); i++)
        {
            LPPolSchema tLPPolSchema = tLPPolSet.get(i).getSchema();
    		String sql = "select mult from LCPol where PolNo = '" + tLPPolSchema.getPolNo() + "' "
	                   + "union all "
	                   + "select mult from LBPol where PolNo = '" + tLPPolSchema.getPolNo() + "' ";
    		double oldMult = Double.parseDouble((new ExeSQL().getOneValue(sql).trim()));
    		double newMult = tLPPolSchema.getMult();
            if(oldMult!=newMult)
            {
            	idea += "序号" + tLPPolSchema.getRiskSeqNo()
			         + "，险种名称 "  + CommonBL.getRiskName(tLPPolSchema.getRiskCode())
                     + "，代码" + tLPPolSchema.getRiskCode()
                     + "（被保险人" + tLPPolSchema.getInsuredName()
                     + "）档次由" + String.valueOf(oldMult) + "档调整为" + String.valueOf(newMult) + "档，"
                     + "保费调整为" + tLPPolSchema.getPrem() + "元\n";
            }
        }
        /*
    	LPContDB tLPContDB = new LPContDB();
    	tLPContDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
    	tLPContDB.setEdorType(aLPEdorItemSchema.getEdorType());
    	tLPContDB.setContNo(aLPEdorItemSchema.getContNo());
    	LCContDB tLCContDB = new LCContDB();
    	tLCContDB.setContNo(aLPEdorItemSchema.getContNo());
    	if(tLPContDB.getInfo()&&tLCContDB.getInfo()&&(tLPContDB.getPrem()!=tLCContDB.getPrem()))
    	{
    		idea += "保单期交保费变更为" + tLPContDB.getPrem() + "元";
    	}
    	*/
    	return idea;
    }

    private boolean getDetailFC(LPEdorItemSchema aLPEdorItemSchema)
    {
        String edorNo = aLPEdorItemSchema.getEdorAcceptNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String ContNo = aLPEdorItemSchema.getContNo();
        String edorAppDate = aLPEdorItemSchema.getEdorAppDate();
        String edorValidate = aLPEdorItemSchema.getEdorValiDate();
        tlistTable = new ListTable();
        tlistTable.setName("FC");
        String[] strArray = new String[4];
        String sql = " select AppntName,b.polno,(select riskname from lmrisk where b.riskcode = riskcode),edorvalue "
                   + " From lpedorespecialdata a,lppol b where b.edorno ='"+edorNo+"' "
                   + "  and a.polno = b.polno and a.edorno = b.edorno "
                   ;
        SSRS result = new ExeSQL().execSQL(sql);
        if (result == null || result.getMaxRow() == 0) {
            mErrors.addOneError("查询数据失败！");
            return false;
        }
        for (int i = 1; i <= result.getMaxRow(); i++) {
            strArray = new String[4];
            strArray[0] = result.GetText(i, 1);
            strArray[1] = result.GetText(i, 2);
            strArray[2] = result.GetText(i, 3);
            strArray[3] = result.GetText(i, 4);
//            strArray[4] = result.GetText(i, 5);
            tlistTable.add(strArray);
        }
        String odlsql = " select sum(cast(edorvalue as decimal (12,2))) "
                      + " from lpedorespecialdata where edorno ='"+edorNo+"' and polno !='000000' ";
//        LPEdorEspecialDataSchema tLPEdorEspecialDataSchema
        String sumMoney = new ExeSQL().getOneValue(odlsql);
        textTag.add("ContNo", ContNo);
        textTag.add("EdorNo", edorNo);
        textTag.add("sumMoney",Math.abs(Double.parseDouble(sumMoney)));
        textTag.add("EdorValidate",CommonBL.decodeDate(PubFun.calDate(edorValidate, 0, "D", null))); //本次保全生效日期
        xmlexport.addListTable(tlistTable, strArray);
        return true;

    }

    private boolean getDetailBP(LPEdorItemSchema aLPEdorItemSchema)
    {
        String tEdorNo = aLPEdorItemSchema.getEdorNo();
        String tEdorType = aLPEdorItemSchema.getEdorType();
        String tContNo = aLPEdorItemSchema.getContNo();
        String tEdorValiDate = aLPEdorItemSchema.getEdorValiDate();
        textTag.add("BP_ContNo", tContNo);
        textTag.add("BP_EdorValiDate", CommonBL.decodeDate(tEdorValiDate));
        String tNewPrem = "";
        String content = "";
        EdorItemSpecialData tSpecialData = new EdorItemSpecialData(tEdorNo,tEdorType);
        if(tSpecialData.query())
        {
        	tNewPrem = tSpecialData.getEdorValue("NEWPREM");
        }
        String sql = "select prem from lccont where contno = '" + tContNo + "' ";
        String tOldPrem = new ExeSQL().getOneValue(sql);

        content += "万能险期交保费变更：由" + tOldPrem + "元，变更为：" + tNewPrem + "元。\n\n";
        content += "本变更自" + CommonBL.decodeDate(tEdorValiDate) + "开始生效";
        String[] column = new String[1];
        column[0] = content;
        ListTable listTable = new ListTable();
        listTable.setName("BP");
        listTable.add(column);
        xmlexport.addListTable(listTable, new String[1]);
        return true;
    }
    
    /**
     * 续保核保
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private boolean getDetailXB(LPEdorItemSchema aLPEdorItemSchema) 
    {
        String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();
        LCContSchema tLCContSchema = CommonBL.getLCCont(contNo);
        if (tLCContSchema == null) {
            mErrors.addOneError("未找到保单信息！");
            return false;
        }
        textTag.add("XB_ContNo", contNo);
        //理算结果显示投保告知补遗和重审承保条件内容，但批单中不显示
        String content = "";
        String sugUWIdea = getSugUWIdea(aLPEdorItemSchema);
        content += sugUWIdea;
        tlistTable = new ListTable();
        tlistTable.setName("XB");
        String[] column = new String[1];
        column[0] = content;
        tlistTable.add(column);
        xmlexport.addListTable(tlistTable, new String[1]);

        String confirmcontent = "";
        confirmcontent += sugUWIdea;
        tlistTable = new ListTable();
        tlistTable.setName("XB_Confirm");
        String[] confirmColumn = new String[1];
        confirmColumn[0] = confirmcontent;
        tlistTable.add(confirmColumn);
        xmlexport.addListTable(tlistTable, new String[1]);
        return true;
    }

    private boolean getDetailGF(LPEdorItemSchema aLPEdorItemSchema){

        String ContNo = aLPEdorItemSchema.getContNo();
        tlistTable = new ListTable();
        tlistTable.setName("GF");
        String[] strArray = new String[3];
        String sql = " select AppntName,polno,(select riskname from lmrisk where a.riskcode = riskcode)  From lcpol a where contno ='"+aLPEdorItemSchema.getContNo()+"'"
                   ;
        SSRS result = new ExeSQL().execSQL(sql);
        if (result == null || result.getMaxRow() == 0) {
            mErrors.addOneError("查询数据失败！");
            return false;
        }
        for (int i = 1; i <= result.getMaxRow(); i++) {
            strArray = new String[3];
            strArray[0] = result.GetText(i, 1);
            strArray[1] = result.GetText(i, 2);
            strArray[2] = result.GetText(i, 3);
            tlistTable.add(strArray);
        }
        textTag.add("ContNo", ContNo);
        xmlexport.addListTable(tlistTable, strArray);
        return true;
    }
    
//  常无忧B给付补费 20100722 zhanggm
    private boolean getDetailCB(LPEdorItemSchema aLPEdorItemSchema)
    {
    	String edorNo = aLPEdorItemSchema.getEdorNo();
        String edorType = aLPEdorItemSchema.getEdorType();
        String contNo = aLPEdorItemSchema.getContNo();
        String edorValiDate = aLPEdorItemSchema.getEdorValiDate();
        textTag.add("CB_ContNo", contNo);

        String content = "";
        EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(edorNo,edorType);
        if(!tEdorItemSpecialData.query())
        {
        	mErrors.addOneError("SpecialData未找到本次金额！");
            return false;
        }
        String [] polnos = tEdorItemSpecialData.getGrpPolNos();
        String polno = polnos[0];
        tEdorItemSpecialData.setPolNo(polno);

        String tGetMoney = tEdorItemSpecialData.getEdorValue("GETMONEY");
        String tOldBonus = tEdorItemSpecialData.getEdorValue("OLDBONUS");
        String tNewBonus = tEdorItemSpecialData.getEdorValue("NEWBONUS");

        if (tGetMoney != null && !("0").equals(tGetMoney) && !("").equals(tGetMoney))
        {
            content += "保单"+contNo+"按照标准公式计算，满期忠诚奖为" + tOldBonus + "元，"
                     + "补充给付忠诚奖金额为" + tGetMoney + "元。\n"
                     + "本次实际领取金额：" + tGetMoney + "元。\n\n";
        }
        else
        {
        	mErrors.addOneError("补费信息出错，请重新理算！");
            return false;
        }

        String[] column = new String[1];
        column[0] = content;
        ListTable listTable = new ListTable();
        listTable.setName("CB");
        listTable.add(column);
        xmlexport.addListTable(listTable, new String[1]);
        return true;
    }

    public static void main(String[] args) {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ManageCom = "86";

        VData tVData = new VData();
        tVData.add(gi);
        LLContDealCalPrintBL tPrtAppEndorsementBL =
                new LLContDealCalPrintBL("20061226000008");
        if (!tPrtAppEndorsementBL.submitData(tVData, "")) {
            System.out.println(tPrtAppEndorsementBL.mErrors.getErrContent());
        }

    }
}
