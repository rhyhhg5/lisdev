/*
 * <p>ClassName: OLLMainAskBL </p>
 * <p>Description: 咨询通知保存 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-12 16:05:52
 */
package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLLMainAskBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private String CNo = "";

    /** 业务处理相关变量 */
    private LLMainAskSchema mLLMainAskSchema = new LLMainAskSchema();
    private LLConsultSchema mLLConsultSchema = new LLConsultSchema();
    private LLSubReportSet tLLSubReportSet = new LLSubReportSet();
    private LLSubReportSet saveLLSubReportSet = new LLSubReportSet();
    private LLCommendHospitalSet tLLCommendHospitalSet = new
            LLCommendHospitalSet();
    private LLCommendHospitalSet saveLLCommendHospitalSet = new
            LLCommendHospitalSet();
    private LLAnswerInfoSchema mLLAnswerInfoSchema = new LLAnswerInfoSchema();
    private LLNoticeRelaSet mLLNoticeRelaSet = new LLNoticeRelaSet();
    private LLAskRelaSet mLLAskRelaSet = new LLAskRelaSet();
    /** 其余变量*/
    private TransferData mTransferData = new TransferData();


//private LLMainAskSet mLLMainAskSet=new LLMainAskSet();
    public OLLMainAskBL() {
    }

    public static void main(String[] args) {
        LLMainAskSchema tLLMainAskSchema = new LLMainAskSchema();
        LLConsultSchema tLLConsultSchema = new LLConsultSchema();
        LLAnswerInfoSchema tLLAnswerInfoSchema = new LLAnswerInfoSchema();
        OLLMainAskUI tOLLMainAskUI = new OLLMainAskUI();
        GlobalInput tG = new GlobalInput();
        tG.ComCode = "86110000";
        tG.ManageCom = "86110000";
        tG.Operator = "claim";
        tLLMainAskSchema.setLogNo("");
        tLLMainAskSchema.setLogState("0");
        tLLMainAskSchema.setAskType("1");
        tLLConsultSchema.setCustomerNo("34234");
        tLLConsultSchema.setCustomerName("2343");
        TransferData mf = new TransferData();
        mf.setNameAndValue("AskType", "1");
        VData tVData = new VData();
        tVData.add(tLLMainAskSchema);
        tVData.add(tLLConsultSchema);
        tVData.add(tLLAnswerInfoSchema);
        tVData.add(tG);
        tVData.add(mf);
        tOLLMainAskUI.submitData(tVData, "INSERT||MAIN");
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLLMainAskBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLLMainAskBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start OLLMainAskBL Submit...");
            String wfFlag = null;
            if (mTransferData != null) {
                wfFlag = (String)this.mTransferData.getValueByName(
                        "WFFLAG");

            }
            if (wfFlag == null || !"1".equals(wfFlag)) { //工作流调用标记
                PubSubmit pubSubmit = new PubSubmit();
                if (!pubSubmit.submitData(this.mResult, "")) {
                    CError.buildErr(this, "提交事务失败!");
                    return false;
                }
            }

//      OLLMainAskBLS tOLLMainAskBLS=new OLLMainAskBLS();
//      tOLLMainAskBLS.submitData(mInputData,mOperate);
//      System.out.println("End OLLMainAskBL Submit...");
//      //如果有需要处理的错误，则返回
//      if (tOLLMainAskBLS.mErrors.needDealError())
//      {
//        // @@错误处理
//        this.mErrors.copyAllErrors(tOLLMainAskBLS.mErrors);
//        CError tError = new CError();
//        tError.moduleName = "OLLMainAskBL";
//        tError.functionName = "submitDat";
//        tError.errorMessage ="数据提交失败!";
//        this.mErrors .addOneError(tError) ;
//        return false;
//      }
        }
        mInputData = null;
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //加入结果集
        MMap tmpMap = new MMap();
        String op = "UPDATE";
        String cDate = PubFun.getCurrentDate();
        String cTime = PubFun.getCurrentTime();
        if ("INSERT||MAIN".equals(this.mOperate)) {

            String tLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);

            String evNo = PubFun1.CreateMaxNo("SERIALNO", "");
            String CNo = "";
            this.mLLMainAskSchema.setLogNo(evNo);
            this.mLLMainAskSchema.setMngCom(this.mGlobalInput.ManageCom);
            this.mLLMainAskSchema.setMakeDate(cDate);
            this.mLLMainAskSchema.setMakeTime(cTime);
            this.mLLMainAskSchema.setOperator(this.mGlobalInput.Operator);
            this.mTransferData.setNameAndValue("logNo", evNo);
            if ("0".equals((String) mTransferData.getValueByName("AskType"))) {
                CNo = PubFun1.CreateMaxNo("COSULTNO", tLimit);
                System.out.println("咨询通知号" + CNo);
            }
            if ("1".equals((String) mTransferData.getValueByName("AskType"))) {
                CNo = PubFun1.CreateMaxNo("NOTICENO", tLimit);
                System.out.println("咨询通知号" + CNo);
            }
            if ("2".equals((String) mTransferData.getValueByName("AskType"))) {
                CNo = PubFun1.CreateMaxNo("CNNO", tLimit);
                System.out.println("咨询通知号" + CNo);
            }
            if (CNo.equals("")) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LLMainAskBL";
                tError.functionName = "submitData";
                tError.errorMessage = "咨询通知号未生成!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //操作咨询表
            this.mLLConsultSchema.setConsultNo(CNo);
            this.mLLConsultSchema.setLogNo(evNo);
            this.mLLConsultSchema.setMngCom(this.mGlobalInput.ManageCom);
            this.mLLConsultSchema.setMakeDate(cDate);
            this.mLLConsultSchema.setMakeTime(cTime);
            this.mLLConsultSchema.setModifyDate(cDate);
            this.mLLConsultSchema.setModifyTime(cTime);
            this.mLLConsultSchema.setOperator(this.mGlobalInput.Operator);
            //插入回复表
            //String serialno = PubFun1.CreateMaxNo("SerialNo",9);
            mLLAnswerInfoSchema.setConsultNo(CNo);
            mLLAnswerInfoSchema.setLogNo(evNo);
            mLLAnswerInfoSchema.setSerialNo("1");
            mLLAnswerInfoSchema.setMakeDate(cDate);
            mLLAnswerInfoSchema.setMakeTime(cTime);
            mLLAnswerInfoSchema.setModifyDate(cDate);
            mLLAnswerInfoSchema.setModifyTime(cTime);
            mLLAnswerInfoSchema.setAnswerDate(cDate);
            mLLAnswerInfoSchema.setAnswerTime(cTime);
            mLLAnswerInfoSchema.setAnswerOperator(mGlobalInput.Operator);
            mLLAnswerInfoSchema.setManageCom(mGlobalInput.ManageCom);
            mLLAnswerInfoSchema.setAskState("1");
            mLLAnswerInfoSchema.setSendFlag("0");

            //插入推荐医院
            if (tLLCommendHospitalSet != null) {
//                String SubRptNo = "";
                System.out.println("CommendHospital:" + CNo);
                for (int i = 1; i <= tLLCommendHospitalSet.size(); i++) {
                    System.out.println(
                            "######tLLCommendHospitalSchema保存#######");
                    LLCommendHospitalSchema tLLCommendHospitalSchema =
                            tLLCommendHospitalSet.get(i);
                    //不能没有医院代码
                    if (tLLCommendHospitalSchema.getHospitalCode() == null ||
                        tLLCommendHospitalSchema.getHospitalCode().equals("")) {
                        CError tError = new CError();
                        tError.moduleName = "LLMainAskBL";
                        tError.functionName = "submitData";
                        tError.errorMessage = "医院代码不能为空!";
                        this.mErrors.addOneError(tError);
                        return false;

                    }
                    tLLCommendHospitalSchema.setConsultNo(CNo);
                    tLLCommendHospitalSchema.setOperator(mGlobalInput.Operator);
                    tLLCommendHospitalSchema.setMngCom(mGlobalInput.ManageCom);
                    tLLCommendHospitalSchema.setMakeDate(cDate);
                    tLLCommendHospitalSchema.setMakeTime(cTime);
                    tLLCommendHospitalSchema.setModifyDate(cDate);
                    tLLCommendHospitalSchema.setModifyTime(cTime);
                    saveLLCommendHospitalSet.add(tLLCommendHospitalSchema);
                    System.out.println("mLLConsultSchema.getConsultNo()" + CNo);
                }
            }

            if (tLLSubReportSet != null) {
                String SubRptNo = "";
                System.out.println("yyyyyyyyyyy:");
                for (int i = 1; i <= tLLSubReportSet.size(); i++) {
                    LLSubReportSchema tLLSubReportSchema = tLLSubReportSet.get(
                            i);
                    //如果事件没有，则创建事件关联
                    if ("".equals(StrTool.cTrim(tLLSubReportSchema.getSubRptNo()))) {
                        SubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);
                        tLLSubReportSchema.setSubRptNo(SubRptNo);
                        System.out.println("SubRptNo" + SubRptNo);
                        tLLSubReportSchema.setMakeDate(PubFun.getCurrentDate());
                        tLLSubReportSchema.setMakeTime(PubFun.getCurrentTime());
                        tLLSubReportSchema.setOperator(this.mGlobalInput.
                                Operator);
                        tLLSubReportSchema.setMngCom(this.mGlobalInput.
                                ManageCom);
                        tLLSubReportSchema.setModifyDate(tLLSubReportSchema.
                                getMakeDate());
                        tLLSubReportSchema.setModifyTime(tLLSubReportSchema.
                                getMakeTime());
                        saveLLSubReportSet.add(tLLSubReportSchema);
                    } else {
                        SubRptNo = tLLSubReportSchema.getSubRptNo();
                    }
                    System.out.println("mLLConsultSchema.getConsultNo()" + CNo);
                    LLAskRelaSchema mLLAskRelaSchema = new LLAskRelaSchema();
                    mLLAskRelaSchema.setConsultNo(CNo);
                    mLLAskRelaSchema.setSubRptNo(SubRptNo);
                    mLLAskRelaSet.add(mLLAskRelaSchema);
                }
            }

            op = "DELETE&INSERT";
        }

        String delSql = null;
        if ("DELETE||MAIN".equals(this.mOperate)) {
            op = "DELETE";
            String wherepart = " where logno='" +
                               this.mLLMainAskSchema.getLogNo() + "'";
            if (this.mLLMainAskSchema.getAskType().equals("0")
                || this.mLLMainAskSchema.getAskType().equals("2")) {
                //关联表数据
                delSql = "delete  from LLAskRela where ConsultNo in"
                         + " (select consultno from llconsult " + wherepart +
                         ")";
                tmpMap.put(delSql, "DELETE");
                //咨询表数据
                delSql = "delete  from llconsult " + wherepart;
                tmpMap.put(delSql, "DELETE");

            }
            if (this.mLLMainAskSchema.getAskType().equals("1")
                || this.mLLMainAskSchema.getAskType().equals("2")) {
                //关联表数据
                delSql = "delete  from LLNoticeRela where NoticeNo in"
                         + " (select NoticeNo from llnotice " + wherepart + ")";
                tmpMap.put(delSql, "DELETE");
                //咨询表数据
                delSql = "delete  from llnotice " + wherepart;
                tmpMap.put(delSql, "DELETE");

            }

        }
        if ("UPDATE||MAIN".equals(this.mOperate)) {
            op = "UPDATE";
            LLMainAskDB tLLMainAskDB = new LLMainAskDB();
            tLLMainAskDB.setLogNo(this.mLLMainAskSchema.getLogNo());
            if (!tLLMainAskDB.getInfo()) {
                CError.buildErr(this, "查询咨询登记信息有误!");
                return false;
            }
            LLMainAskSchema tLLMainAskSchema = tLLMainAskDB.getSchema();
            this.mLLMainAskSchema.setMakeDate(tLLMainAskSchema.getMakeDate());
            this.mLLMainAskSchema.setMakeTime(tLLMainAskSchema.getMakeTime());
            this.mLLMainAskSchema.setOperator(tLLMainAskSchema.getOperator());
            this.mLLMainAskSchema.setMngCom(tLLMainAskSchema.getMngCom());

        }

        //更新时间
        this.mLLMainAskSchema.setModifyDate(cDate);
        this.mLLMainAskSchema.setModifyTime(cTime);
        System.out.println("**************************zhenzhengdeop" + op);
        tmpMap.put(this.mLLMainAskSchema, op);
        tmpMap.put(this.mLLConsultSchema, op);
        tmpMap.put(this.mLLNoticeRelaSet, op);
        tmpMap.put(this.mLLAnswerInfoSchema, op);
        tmpMap.put(this.saveLLCommendHospitalSet, op);
        tmpMap.put(this.saveLLSubReportSet, op);
        tmpMap.put(this.mLLAskRelaSet, op);
        this.mResult.add(tmpMap);
        this.mResult.add(mLLConsultSchema);
        this.mResult.add(mTransferData);
        System.out.println(mResult);

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLLMainAskSchema.setSchema((LLMainAskSchema) cInputData.
                                        getObjectByObjectName("LLMainAskSchema",
                0));
        this.mLLConsultSchema.setSchema((LLConsultSchema) cInputData.
                                        getObjectByObjectName("LLConsultSchema",
                0));
        this.tLLCommendHospitalSet.set((LLCommendHospitalSet) cInputData.
                                       getObjectByObjectName(
                "LLCommendHospitalSet", 0));
        this.tLLSubReportSet.set((LLSubReportSet) cInputData.
                                 getObjectByObjectName("LLSubReportSet",
                0));
        this.mLLAnswerInfoSchema.setSchema((LLAnswerInfoSchema) cInputData.
                                           getObjectByObjectName(
                "LLAnswerInfoSchema",
                0));

        //this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData) cInputData.
                        getObjectByObjectName("TransferData", 0);

        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LLMainAskDB tLLMainAskDB = new LLMainAskDB();
        tLLMainAskDB.setSchema(this.mLLMainAskSchema);
        //如果有需要处理的错误，则返回
        if (tLLMainAskDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLLMainAskDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLMainAskBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLMainAskBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
