/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: 社保导入文件通用解析类，从一个XML数据流中解析出批次下分案信息</p>
 * <p>Copyright: Copyright (c) 2007 </p>
 * <p>Company: Sinosoft </p>
 * @author Xx
 * @version 6.0
 */

package com.sinosoft.lis.llcase;

import java.lang.reflect.Method;

import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import org.apache.xpath.XPathAPI;
import org.apache.xpath.objects.XObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.sinosoft.lis.pubfun.*;

class LLSCParser implements SIParser {

    // 下面是一些常量定义
    private static String PATH_INSURED = "ROW";

    //案件
    private String mRgtNo = "";
    private String mGrpContNo = "";
    private String mPath = "";
    private String[] Title = null;
    private String vtsName = "ClaimErrList.vts";
    private ListTable FalseListTable = new ListTable();
    private GlobalInput mG = new GlobalInput();
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = new GlobalInput();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    private String mOperate;
    LLCaseImportSchema tLLCaseImportSchema = new LLCaseImportSchema();

    LLSCParser() {
    }

    public boolean setGlobalInput(GlobalInput aG) {
        mG = aG;
        FalseListTable.setName("FAULT");
        return true;
    }

    /**
     * 解析案件结点(公用函数)
     * @param node Node
     * @return VData
     */
    public VData parseOneInsuredNode(Node node) {
        VData tReturn = new VData();
        // 得到被保人信息
        NodeList nodeList = null;
        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_INSURED);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
            return null;
        }
        int nLength = nodeList == null ? 0 : nodeList.getLength();
        int FailureCount = 0;
        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeCase = nodeList.item(nIndex);
            if (!genCase(getValue(nodeCase))) {
                System.out.println("导入这个人失败");
                FailureCount++;
                //写导出错误xml的函数
            }
        }

        /**
         * 根据xml生成title 不写死在程序里，另外需要生成动态数组
         */
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument(vtsName, "print");
        xmlexport.addListTable(FalseListTable, Title);
        xmlexport.outputDocumentToFile(mPath, mRgtNo); //输出xml文档到文件
        String FailureNum = FailureCount + "";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("FailureNum", FailureNum);
        tReturn.clear();
        tReturn.addElement(xmlexport);
        tReturn.add(tTransferData);
        return tReturn;
    }

    public boolean setParam(String aRgtNo, String aGrpContNo, String aPath) {
        mRgtNo = aRgtNo;
        mGrpContNo = aGrpContNo;
        mPath = aPath;
        return true;
    }

    public void setFormTitle(String[] cTitle, String cvtsName) {
        Title = cTitle;
        vtsName = cvtsName;
    }

    /**
     * 利用XPathAPI取得某个节点的节点值
     * @param node Node
     * @param strPath String
     * @return String
     */
    private static String parseNode(Node node, String strPath) {
        String strValue = "";
        try {
            XObject xobj = XPathAPI.eval(node, strPath);
            strValue = xobj == null ? "" : xobj.toString();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return strValue;
    }

    private TransferData getValue(Node nodeCase) {
        TransferData td = new TransferData();
        for (int i = 0; i < Title.length; i++) {
            System.out.println(Title[i]);
            String tvalue = "" + parseNode(nodeCase, Title[i]);
            td.setNameAndValue(Title[i], tvalue);
        }
        return td;
    }

    private boolean genCase(TransferData cTD) {
        //确认客户信息
        String aSecurityNo = "" + (String) cTD.getValueByName("SecurityNo");
        String aCustomerNo = "" + (String) cTD.getValueByName("CustomerNo");
        String aIDNo = "" + (String) cTD.getValueByName("IDNo");
        String aCustomerName = "" + (String) cTD.getValueByName("CustomerName");
        String aSex = "" + (String) cTD.getValueByName("Sex");
        String aBirthday = FormatDate((String) cTD.getValueByName("Birthday"));
        String tSupInHosFee = FormatDate((String) cTD.getValueByName("SupInHosFee"));
        System.out.println(tSupInHosFee);
        String strSQL0 =
                "select distinct insuredno,name,sex,birthday,idno,othidno ";
        String sqlpart1 = "from lcinsured a where ";
        String sqlpart2 = "from lbinsured a where ";
        String wherePart = "a.GrpContNo = '" + mGrpContNo + "' ";
        String msgPart = "";
        String ErrMessage = "";
        if (!aCustomerNo.equals("null") && !aCustomerNo.equals("")) {
            wherePart += " and a.insuredno='" + aCustomerNo + "' ";
            msgPart = "客户号为" + aCustomerNo;
        } else if (!aSecurityNo.equals("null") && !aSecurityNo.equals("")) {
            wherePart += " and a.othidno='" + aSecurityNo + "'";
            msgPart = "社保号为" + aSecurityNo;
        } else if (!aIDNo.equals("null") && !aIDNo.equals("")) {
            wherePart += " and a.idno='" + aIDNo + "'";
            msgPart = "身份证号为" + aIDNo;
        } else {
            ErrMessage = "未提供客户号，社保号或身份证号等信息，无法确认客户身份！";
            getFalseList(cTD, ErrMessage);
            return false;
        }

        String strSQL = strSQL0 + sqlpart1 + wherePart + " union " + strSQL0 +
                        sqlpart2 + wherePart;
        ExeSQL texesql = new ExeSQL();
        SSRS tssrs = new SSRS();
        tssrs = texesql.execSQL(strSQL);
        System.out.println(strSQL);
        if (tssrs == null || tssrs.getMaxRow() <= 0) {
            ErrMessage = "团单" + mGrpContNo + "下没有" + msgPart + "的被保险人";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        aCustomerNo = tssrs.GetText(1, 1);
        aIDNo = tssrs.GetText(1, 5);
        aSecurityNo = tssrs.GetText(1, 6);
        if (aCustomerName.equals("") || aCustomerName.equals("null")) {
            aCustomerName = tssrs.GetText(1, 2);
        } else if (!tssrs.GetText(1, 2).equals(aCustomerName)) {
            ErrMessage = msgPart + "的客户投保时姓名为" + tssrs.GetText(1, 2)
                         + ",与导入姓名" + aCustomerName + "不符！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        String tSex = tssrs.GetText(1, 3).equals("null") ? "" :
                      tssrs.GetText(1, 3);
        if (aSex.equals("") || aSex.equals("null")) {
            aSex = tSex;
        } else if (!tSex.equals("") && !tSex.equals(aSex)) {
            ErrMessage = msgPart + "的客户投保时性别为" + tSex
                         + ",与本次导入性别" + aSex + "不符！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        String tBirthday = tssrs.GetText(1, 4).equals("null") ? "" :
                           tssrs.GetText(1, 4);
        if (aBirthday.equals("") || aBirthday.equals("null")) {
            aBirthday = tBirthday;
        } else if (!tBirthday.equals("") && !tBirthday.equals(aBirthday)) {
            ErrMessage = msgPart + "的客户投保时生日为" + tBirthday
                         + ",与本次导入生日" + aBirthday + "不符！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        String strCustomerSQL="select * from lpedorapp a,lpgrpedoritem b where a.edoracceptno in (select max(edorno) from lpdiskimport where  insuredno='"+aCustomerNo+"' and grpcontno ='"+mGrpContNo+"') and a.edorstate !='0' and a.edoracceptno = b.edoracceptno and b.edortype ='ZT'";
        ExeSQL customersql = new ExeSQL();
        SSRS customertssrs = new SSRS();
        customertssrs = texesql.execSQL(strCustomerSQL);
        if(customertssrs.getMaxRow()>0){
        	ErrMessage = msgPart + "的客户正在进行保全减人操作！";
            getFalseList(cTD, ErrMessage);
            return false;

        }



        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();

        tLLCaseSchema.setRgtType("1");
        tLLRegisterSchema.setRgtNo(mRgtNo);
        tLLRegisterSchema.setRgtObjNo(mGrpContNo);

        String aFeeType = "" + cTD.getValueByName("FeeType");
        System.out.println("aFeeTypeaFeeTypeaFeeTypeaFeeTypeaFeeTypeaFeeTypeaFeeTypeaFeeTypeaFeeType="+aFeeType);
        LLAppClaimReasonSchema tLLAppClaimReasonSchema = new
                LLAppClaimReasonSchema();
        if (aFeeType.trim().equals("1")) {
            tLLAppClaimReasonSchema.setReasonCode("01");
            tLLAppClaimReasonSchema.setReason("门诊"); //申请原因
        } else if(aFeeType.trim().equals("2")){
            tLLAppClaimReasonSchema.setReasonCode("02"); //原因代码
            tLLAppClaimReasonSchema.setReason("住院"); //申请原因
        }else if(aFeeType.trim().equals("3")){
            tLLAppClaimReasonSchema.setReasonCode("03"); //原因代码
            tLLAppClaimReasonSchema.setReason("特种病"); //申请原因
        }else{
            ErrMessage = "帐单类型错误，请输入正确的帐单类型1，2或3";
            getFalseList(cTD, ErrMessage);
            return false;
        }

        /*生成理赔号*/
       String tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 12);
//       String tSql="select max(caseno) from LLCaseImport";
//       ExeSQL exeMaxNo = new ExeSQL();
//       String resRealPay = exeMaxNo.getOneValue(tSql);
//       if(resRealPay.equals("")){
//           resRealPay="1";
//       }else{
//           resRealPay=String.valueOf((Integer.parseInt(resRealPay)+1));
//       }

        tLLCaseImportSchema = (LLCaseImportSchema)
                                   fillCommonField(tLLCaseImportSchema,
                cTD);
        String aAccDate = FormatDate((String) cTD.getValueByName("AccDate"));
        tLLCaseImportSchema.setHospStartDate(FormatDate((String) cTD.
                getValueByName("HospStartDate")));
        tLLCaseImportSchema.setHospEndDate(FormatDate((String) cTD.getValueByName(
                "HospEndDate")));
            tLLCaseImportSchema.setFeeDate(FormatDate((String) cTD.getValueByName(
                "FeeDate")));
        tLLCaseImportSchema.setRgtNo(mRgtNo);
        tLLCaseImportSchema.setAccDate(aAccDate);
        tLLCaseImportSchema.setCaseNo(tSerielNo);
        tLLCaseImportSchema.setMakeDate(PubFun.getCurrentDate());
        tLLCaseImportSchema.setImportState("01");
        tLLCaseImportSchema.setMakeTime(PubFun.getCurrentTime());
        tLLCaseImportSchema.setManageCom("86");
        tLLCaseImportSchema.setOperator("lptest");
        System.out.println(mG.ManageCom);
        System.out.println(mG.Operator);
        if (!prepareOutputData()) {
           return false;
       }

        System.out.println("Start ClientRegisterBL Submit...");
       PubSubmit tPubSubmit = new PubSubmit();
       if (!tPubSubmit.submitData(mInputData, mOperate)) {
           this.mErrors.copyAllErrors(tPubSubmit.mErrors);
           CError tError = new CError();
           tError.moduleName = "LDDrugBL";
           tError.functionName = "ClientRegisterBL";
           tError.errorMessage = "数据提交失败!";

           this.mErrors.addOneError(tError);
           return false;
       }
        return true;
    }

    private Object fillCommonField(Object cSchema, TransferData cTD) {
        int n = cTD.getValueNames().size();
        for (int i = 0; i < n; i++) {
            String fieldName = (String) cTD.getNameByIndex(i);
            String fieldValue = (String) cTD.getValueByIndex(i);
            System.out.println(fieldName);
            System.out.println(fieldValue);
            Class[] c = new Class[1];
            Method m = null;
            fieldName = "set" + fieldName;
            String[] tValue = {fieldValue};
            try {
                c[0] = Class.forName("java.lang.String");
            } catch (Exception ex) {
            }
            try {
                m = cSchema.getClass().getMethod(fieldName, c);
            } catch (Exception ex) {
            }
            try {
                m.invoke(cSchema, tValue);
            } catch (Exception ex) {
            }
        }
        return cSchema;
    }


    private boolean getFalseList(TransferData cTD, String ErrMessage) {
        int n = Title.length;
        String[] strCol = new String[n + 1];
        for (int i = 0; i < n; i++) {
            strCol[i] = (String) cTD.getValueByIndex(i);
        }
        strCol[n] = ErrMessage;
        System.out.println("ErrMessage:" + ErrMessage);
        FalseListTable.add(strCol);
        return true;
    }

    private String FormatDate(String aDate) {
        String rDate = "";
        rDate += aDate;
        if (rDate.equals("") || rDate.equals("null")) {
            return "";
        }
        if (aDate.length() < 8) {
            int days = 0;
            try {
                days = Integer.parseInt(aDate) - 2;
            } catch (Exception ex) {
                return "";
            }
            rDate = PubFun.calDate("1900-1-1", days, "D", null);
        }
        return rDate;
    }
    private boolean prepareOutputData() {
    try {
        this.mInputData.clear();
        map.put(tLLCaseImportSchema,"INSERT");
        mInputData.add(map);
        mResult.clear();
        mResult.add(tLLCaseImportSchema);
    } catch (Exception ex) {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "genCaseInfoBL";
        tError.functionName = "prepareData";
        tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
        this.mErrors.addOneError(tError);
        return false;
    }
    return true;
}

}
