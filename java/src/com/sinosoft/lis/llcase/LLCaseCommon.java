package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.operfee.IndiLJSCancelUI;
import com.sinosoft.lis.pubfun.*;

import java.util.HashMap;
import java.lang.reflect.*;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.bl.LCPolBL;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.SimpleDateFormat;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:案件通用类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Xx
 * @version 1.0`
 */

public class LLCaseCommon {

    private VData mInputData;
    public CErrors mErrors = new CErrors();
    public LLCaseCommon() {

    }

    public static void main(String[] args) {
    	System.out.println("aaa"+checkGrp("00110116000001"));
//        LLCaseCommon tLLCaseCommon = new LLCaseCommon();
//    	String caseno="C1100160620000005";
//    	String aPolNo="21106657268";
////    	String getdutycode="701201";
//    	String untilDate ="2016-06-20";
//    	String ExeLocation = "01";
//    	LLCaseCommon.getAmntFromAcc(caseno, aPolNo, untilDate, ExeLocation);
////    	getdutycode="701203";
////    	LLCaseCommon.getAmntFromAcc(caseno, aPolNo, untilDate, ExeLocation);
//    	System.out.println(LLCaseCommon.getBasicAmnt(caseno, aPolNo, "2014-01-01",untilDate, ExeLocation));
    	
//        System.out.println(tLLCaseCommon.ChooseAssessor("86210500", "000756017",
//                "01"));
//        tLLCaseCommon.SocialSecurityChooseUser("86110000");
//        LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
//        mLLCaseOpTimeSchema.setCaseNo("C0000060701000001");
//        mLLCaseOpTimeSchema.setOperator("xuxin");
//        mLLCaseOpTimeSchema.setManageCom("86");
//        mLLCaseOpTimeSchema.setRgtState("02");
//        mLLCaseOpTimeSchema.setStartDate("2006-7-1");
//        mLLCaseOpTimeSchema.setStartTime("16:08:16");
//        LLCaseCommon1.CalTimeSpan(mLLCaseOpTimeSchema);
//        LLCaseCommon1.ChooseAssessor("86110000","02");
//    System.out.println(LLCaseCommon1.getClaimState("21000007682"));
//    System.out.println("over");
//        GlobalInput tg = new GlobalInput();
//        tg.ComCode = "86";
//        tg.Operator = "claim";
//        tg.ManageCom = "86";
//        MMap tmap = tLLCaseCommon.gotoBack("C2100060814000003", tg);
//        tmap.notify();

//        String tPolNo = "21001730680";
//        if (tLLCaseCommon.checkClaimState(tPolNo)) {
//            System.out.println("无理赔");
//        } else {
//            System.out.println("有理赔");
//        }
    }
    
    // #3500 增加发生地点信息**start**
    	/***
    	 * 发生地点省、市、县三者信息校验
    	 * 
    	 */
    public static String  checkAccPlace(String tAccProvinceCode ,String tAccCityCode,String tAccCountyCode){
    	String tAccError = "";//阻断错误
		if("".equals(tAccProvinceCode) || "null".equals(tAccProvinceCode) || tAccProvinceCode==null
   				|| "".equals(tAccCityCode) || "null".equals(tAccCityCode) || tAccCityCode==null
   				|| "".equals(tAccCountyCode) || "null".equals(tAccCountyCode) ||tAccCountyCode==null){
			
			return  tAccError +"发生地点省、市、县不能为空";     
 		}else{
 		      Pattern tPattern = Pattern.compile("(\\d)+");
       		  Matcher isNum = tPattern.matcher(tAccProvinceCode);
       		  if(!isNum.matches()){
             	  	return tAccError +"发生地点(省)只能录入编码类型";
               }
       		  isNum = tPattern.matcher(tAccCityCode);
	       		  if(!isNum.matches()){
	       			return tAccError +"发生地点(市)只能录入编码类型";
	               }
	       	  isNum = tPattern.matcher(tAccCountyCode);
	       		  if(!isNum.matches()){
	       			return tAccError +"发生地点(县)只能录入编码类型";
	               }
 		}
   		// 根据省、市、县三者联动查询录入的编码是否正确
   		String accSQL="select 1 from ldcode1 where codetype='province1' and code='"+tAccProvinceCode+"' "
   				+ "and code in (select code1 from ldcode1 where codetype='city1' and code='"+tAccCityCode+"' "
   				+ "and code in (select code1 from ldcode1 where codetype='county1' and code='"+tAccCountyCode+"')) with ur";
   		ExeSQL tAccPlaceExe =new ExeSQL();
   		String tAccCode =tAccPlaceExe.getOneValue(accSQL);
   		if(!"1".equals(tAccCode)){
   			return tAccError +"发生地点省、市、县三者关系不属于同一个省、市、县，请重新查询录入。";
   		}
    	return tAccError;
    }
    
    /***
     * 校验录入的医保类型信息
     */
    public static String checkMedicareType(String aMedicareType){
    	String aResult="";
    	if("".equals(aMedicareType) || "null".equals(aMedicareType) || aMedicareType==null){
    		return  aResult +"医保类型不能为空";  
    	}else{
   		      Pattern tPattern = Pattern.compile("(\\d)+");
         	  Matcher isNum = tPattern.matcher(aMedicareType);
         	  if(!isNum.matches()){
               	 return aResult +"医保类型只能录入编码类型";
              }
    	}
    	//判断医保类型是否正确
    	if(!"1".equals(aMedicareType) && !"2".equals(aMedicareType) && !"3".equals(aMedicareType) 
    			&& !"4".equals(aMedicareType) && !"9".equals(aMedicareType)){
    		return aResult +"医保类型录入错误，请重新查询再录。";
    	}
    	
    	return aResult;
    }
    
    // #3500 增加发生地点信息**end**
    
    // #3337 团体管理式补充医疗保险(B款)**start**
    
    /***
	 * 管理式补充B赔付顺序
	 * 
	 */
    public static String getGetClaimNum(String aPolNo){
		String tClaimSQL="select a.claimnum from lcgrpfee a, lcpol b where a.grpcontno=b.grpcontno and b.polno='"+aPolNo+"' union "
				+ "select a.claimnum from lbgrpfee a,lbpol b where a.grpcontno=b.grpcontno and b.polno='"+aPolNo+"' union "
				+ "select a.claimnum from lcgrpfee a,lbpol b where a.grpcontno=b.grpcontno and b.polno='"+aPolNo+"' with ur";
		ExeSQL tClaimExe = new ExeSQL();
		String aClaimNum= tClaimExe.getOneValue(tClaimSQL);
		return aClaimNum;
    }
    
    
     /***
      * 获取公共账户金额 ，账户轨迹表中金额+每一笔金额到某一天的利息
      * @param aCaseNo
      * @param aPolNo
      * @param untilDate
      * @param ExeLocation 位置
      * @return
      */
    public static double getPooledAccount(String aCaseNo,String aPolNo,String untilDate,String ExeLocation){
    	double amnt=0.0;
    	String tRiskSQL ="select riskcode from lcpol where polno='"+aPolNo+"' "
    			+ "union select riskcode from lbpol where polno='"+aPolNo+"' with ur";
    	ExeSQL tExe =  new ExeSQL();
    	String ariskcode = tExe.getOneValue(tRiskSQL);
    	
    	//根据险种号和帐户类型查InsuAccNo--001 为公共账户
    	String tsql1 = "select b.insuaccno from lmrisktoacc a,LMRiskInsuAcc b "
            +" where a.insuaccno = b.insuaccno and a.riskcode='"
            +ariskcode+"' and b.acctype='001'";
		String aInsuAccNo = tExe.getOneValue(tsql1);
		aInsuAccNo =(aInsuAccNo==null ||aInsuAccNo.equals("null"))?"":aInsuAccNo;
		
		 //LCPolBL同时查C表和B表
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setPolNo(aPolNo);
        if (!tLCPolBL.getInfo()) {
           // CError.buildErr(this, "险种保单信息查询错误!");
        }
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setSchema(tLCPolBL.getSchema());
        String riskcode = tLCPolSchema.getRiskCode();
		
		//根据险种号和类型找算法编码
        LMCalModeDB tLMCalModeDB=new LMCalModeDB();
      	tLMCalModeDB.setRiskCode(riskcode);
      	tLMCalModeDB.setType("J");
      	LMCalModeSet set = tLMCalModeDB.query();
      	if(set.size() == 0)
          {
      		//CError.buildErr(this,"获取保额计算公式失败！");
          }
      	String mCalCode=set.get(1).getCalCode();
		
      	LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
        tLCInsureAccTraceDB.setPolNo(aPolNo);
        tLCInsureAccTraceDB.setInsuAccNo(aInsuAccNo);
        tLCInsureAccTraceDB.setState("0");
        LCInsureAccTraceSet tLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
        String batchNo = PubFun.getCurrentDate2()+PubFun1.CreateMaxNo("AMNTPA"+PubFun.getCurrentDate2(),6);
        double tOldMoney=0.0; //原本金
        double tNewMoney=0.0; //本金+利息
		if(tLCInsureAccTraceSet.size()>0 && tLCInsureAccTraceSet!=null){
			for(int i=1;i<=tLCInsureAccTraceSet.size();i++){
				String aPayDate =tLCInsureAccTraceSet.get(i).getPayDate();
				FDate tFDate = new FDate();
				String tFirstDate=null;
				if(tFDate.getDate(untilDate).compareTo(tFDate.getDate(aPayDate))>=0){
					if("".equals(tFirstDate) || tFirstDate==null){
						tFirstDate=aPayDate;
					}
					//找出最早的日期
					if(tFDate.getDate(tFirstDate).compareTo(tFDate.getDate(aPayDate))>0){
						tFirstDate=aPayDate;
					}
					//对于费用发生日期晚于结算日的将其置为结算日
		    		if(tFDate.getDate(aPayDate).compareTo(tFDate.getDate(untilDate))>0){
		    			aPayDate=untilDate;
		    		}
		    		//计算 公共保额 本金+利息 
		    		Calculator tCalculator = new Calculator();
		        	tCalculator.setCalCode(mCalCode);
		        	tCalculator.addBasicFactor("StartDate", aPayDate);
		        	tCalculator.addBasicFactor("EndDate", untilDate);
		        	tCalculator.addBasicFactor("Money", String.valueOf(tLCInsureAccTraceSet.get(i).getMoney()));
		        	String money = tCalculator.calculate();
		        	if(tCalculator.mErrors.needDealError())
		            {
		        		//CError.buildErr(this, "计算失败", tCalculator.mErrors);
		            }
		        	if(null != money && !"".equals(money)){
		        		tNewMoney += Double.parseDouble(money);
		        	}else{
		        		//CError.buildErr(this,"保额记录"+tLCInsureAccTraceSet.get(m).getSerialNo()+"计算失败！");
		        	}
		        	tOldMoney += tLCInsureAccTraceSet.get(i).getMoney();
		        	// 3207 账户记录表 修改为核赔规则和给付确认调用时候进行数据的存储。其他都是只计算不存储  核赔规则=QR 通知给付=JF 。
//		        	if("QR".equals(ExeLocation) ||"JF".equals(ExeLocation)){
//		        	recordAccAmntTrace(aCaseNo,batchNo,aPayDate,untilDate,tLCInsureAccTraceSet.get(i),money,ExeLocation);//记录账户计价轨迹
//		        	}
		        	//如果是最后一条记录那么记录计算合计
		        	if(i==tLCInsureAccTraceSet.size()){
		        		LCInsureAccTraceSchema tLCInsureAccTraceSchema = tLCInsureAccTraceSet.get(i);
		        		tLCInsureAccTraceSchema.setMoneyType("PM");
		        		if("QR".equals(ExeLocation)||"JF".equals(ExeLocation)){
		        		recordAccAmntTraceSum(aCaseNo,batchNo,tOldMoney,tNewMoney,tFirstDate,untilDate,tLCInsureAccTraceSchema,ExeLocation);
		        		}
		            }
					
				}
				
			}
			
		}
		amnt=tNewMoney;
        amnt = Arith.round(amnt, 2);
    	return amnt;
    }
    
    /**
     * 从账户计算累计新增基本保险金额,(补充A 管A 管B)
     * 账户轨迹表中金额,目前对补充A向账户轨迹表分配的业务只有"团体公共保额分配"后续新增业务需要再添加
     * @param aCaseNo
     * @param aPolNo	 
     * @param startDate  计算开始时间
     * @param endDate	   计算截止时间
     * @param ExeLocation	执行操作位置
     * @return amnt 保险金额
     */

    public static double getBasicAppendAmnt(String aCaseNo,String aPolNo,String startDate,String endDate,String ExeLocation){
    	double amnt = 0;
    	String tsql = "select riskcode from lcpol where polno = '"+aPolNo+"' union select riskcode from lbpol where polno = '"+aPolNo+"'";
        ExeSQL texesql = new ExeSQL();
        String ariskcode = texesql.getOneValue(tsql);
    	
    	//根据险种号和帐户类型查InsuAccNo--002为个人账户
    	String tsql1 = "select b.insuaccno from lmrisktoacc a,LMRiskInsuAcc b "
            +" where a.insuaccno = b.insuaccno and a.riskcode='"
            +ariskcode+"' and b.acctype='002'";
		ExeSQL texesql1 = new ExeSQL();
		String aInsuAccNo = texesql1.getOneValue(tsql1);
		aInsuAccNo = (aInsuAccNo==null||aInsuAccNo.equals("null"))?"":aInsuAccNo;
        String tsql2 = " select nvl(sum(money),0) from LCInsureAccTrace " +
        		"where polno='"+aPolNo+"' and insuaccno='"+aInsuAccNo+"' and state = '0' "+
        		"and (moneytype='GD' or moneytype='BF' ) and othertype='3' " + 
        		//GD保全"团体公共保额分配"  BF包括othertype=1是承包时分配的,othertype=3是保全向账户轨迹表分的(团险补充A追加保费)
        		"and paydate between '"+startDate+"' and '"+endDate+"'";
		ExeSQL texesql2 = new ExeSQL();
		String StringAmnt = texesql2.getOneValue(tsql2);
		amnt = Double.parseDouble(StringAmnt);
		
		String batchNo = PubFun.getCurrentDate2()+PubFun1.CreateMaxNo("AMNTFA"+PubFun.getCurrentDate2(),7);
		LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
		LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
        tLCInsureAccTraceDB.setPolNo(aPolNo);
        tLCInsureAccTraceDB.setInsuAccNo(aInsuAccNo);
        tLCInsureAccTraceDB.setState("0");
        LCInsureAccTraceSet tLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
        if(tLCInsureAccTraceSet!=null&&tLCInsureAccTraceSet.size()>0){
        	tLCInsureAccTraceSchema=tLCInsureAccTraceSet.get(1);
        	tLCInsureAccTraceSchema.setMoneyType("JA");  //新增保费
    		recordAccAmntTraceSum(aCaseNo,batchNo,amnt,amnt,startDate, endDate,tLCInsureAccTraceSchema,ExeLocation);
        }
        return amnt;
    }
    
    
    // #3337 团体管理式补充医疗保险(B款)**end**
    
  //#3309北京健康险平台数据报送优化 start
    /**
     * 根据条件从ldcode表中查询code值，并将查询的每一个code值，拼接成以“，”分割的字符串
     * @param strSQL SQL查询语句
     * @param strCode 结果字符串
     */
    public static String codeQuery(String strSQL) {
    	String  strCode = "";
    	ExeSQL tExeSQL = new ExeSQL();
		SSRS tStrSQLSSRS = tExeSQL.execSQL(strSQL);
		if(tStrSQLSSRS != null && tStrSQLSSRS.getMaxRow()>0){
			for(int i=1;i<=tStrSQLSSRS.getMaxRow();i++){
				strCode += "'"+ tStrSQLSSRS.GetText(i, 1) +"',";
			}
		}
		System.out.println("strCode="+strCode);
		strCode = strCode.substring(0,strCode.length()-1);
    	return strCode;
    }
   //#3309北京健康险平台数据报送优化 end
   
    //2713补充A**start**新增  
    /**
     * 从账户计算基本保险金额,(补充A)
     * 账户轨迹表中金额,目前对补充A向账户轨迹表分配的业务只有"团体公共保额分配"后续新增业务需要再添加
     * @param aCaseNo
     * @param aPolNo	 
     * @param startDate  计算开始时间
     * @param endDate	   计算截止时间
     * @param ExeLocation	执行操作位置
     * @return amnt 保险金额
     */

    public static double getBasicAmnt(String aCaseNo,String aPolNo,String startDate,String endDate,String ExeLocation){
    	double amnt = 0;
    	String tsql = "select riskcode from lcpol where polno = '"+aPolNo+"' union select riskcode from lbpol where polno = '"+aPolNo+"'";
        ExeSQL texesql = new ExeSQL();
        String ariskcode = texesql.getOneValue(tsql);
    	
    	//根据险种号和帐户类型查InsuAccNo--002为个人账户
    	String tsql1 = "select b.insuaccno from lmrisktoacc a,LMRiskInsuAcc b "
            +" where a.insuaccno = b.insuaccno and a.riskcode='"
            +ariskcode+"' and b.acctype='002'";
		ExeSQL texesql1 = new ExeSQL();
		String aInsuAccNo = texesql1.getOneValue(tsql1);
		aInsuAccNo = (aInsuAccNo==null||aInsuAccNo.equals("null"))?"":aInsuAccNo;
        String tsql2 = " select nvl(sum(money),0) from LCInsureAccTrace " +
        		"where polno='"+aPolNo+"' and insuaccno='"+aInsuAccNo+"' and state = '0' "+
        		"and (moneytype='GD' or moneytype='BF' )  " + 
        		//GD保全"团体公共保额分配"  BF包括othertype=1是承包时分配的,othertype=3是保全向账户轨迹表分的(团险补充A追加保费)
        		"and paydate between '"+startDate+"' and '"+endDate+"'";
		ExeSQL texesql2 = new ExeSQL();
		String StringAmnt = texesql2.getOneValue(tsql2);
		amnt = Double.parseDouble(StringAmnt);
		
		String batchNo = PubFun.getCurrentDate2()+PubFun1.CreateMaxNo("AMNTFA"+PubFun.getCurrentDate2(),7);
		LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
		LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
        tLCInsureAccTraceDB.setPolNo(aPolNo);
        tLCInsureAccTraceDB.setInsuAccNo(aInsuAccNo);
        tLCInsureAccTraceDB.setState("0");
        LCInsureAccTraceSet tLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
        if(tLCInsureAccTraceSet!=null&&tLCInsureAccTraceSet.size()>0){
        	tLCInsureAccTraceSchema=tLCInsureAccTraceSet.get(1);
        	tLCInsureAccTraceSchema.setMoneyType("JB");
    		recordAccAmntTraceSum(aCaseNo,batchNo,amnt,amnt,startDate, endDate,tLCInsureAccTraceSchema,ExeLocation);
        }
        return amnt;
    }
    
    /**
     * 从账户计算保险金额,(补充A)
     * 账户轨迹表中金额+每一笔金额到某一天的利息
     * @return amnt 保险金额
     */
    public static double getAmntFromAcc(String aCaseNo,String aPolNo,String untilDate,String ExeLocation){
    	double amnt = 0;
    	String tsql = "select riskcode from lcpol where polno = '"+aPolNo+"' union select riskcode from lbpol where polno = '"+aPolNo+"'";
        ExeSQL texesql = new ExeSQL();
        String ariskcode = texesql.getOneValue(tsql);
        
    	//根据险种号和帐户类型查InsuAccNo--002为个人账户
    	String tsql1 = "select b.insuaccno from lmrisktoacc a,LMRiskInsuAcc b "
            +" where a.insuaccno = b.insuaccno and a.riskcode='"
            +ariskcode+"' and b.acctype='002'";
		ExeSQL texesql1 = new ExeSQL();
		String aInsuAccNo = texesql1.getOneValue(tsql1);
		aInsuAccNo = (aInsuAccNo==null||aInsuAccNo.equals("null"))?"":aInsuAccNo;
		

        //LCPolBL同时查C表和B表
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setPolNo(aPolNo);
        if (!tLCPolBL.getInfo()) {
           // CError.buildErr(this, "险种保单信息查询错误!");
        }
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setSchema(tLCPolBL.getSchema());
        String riskcode = tLCPolSchema.getRiskCode();
		
		//根据险种号和类型找算法编码
        LMCalModeDB tLMCalModeDB=new LMCalModeDB();
      	tLMCalModeDB.setRiskCode(riskcode);
      	tLMCalModeDB.setType("J");
      	LMCalModeSet set = tLMCalModeDB.query();
      	if(set.size() == 0)
          {
      		//CError.buildErr(this,"获取保额计算公式失败！");
          }
      	String mCalCode=set.get(1).getCalCode();
		
    	double resultMoney =0.0;//计算后的总本金+利息
    	double oldMoney =0.0;//算利息的总本金
        LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
        tLCInsureAccTraceDB.setPolNo(aPolNo);
        tLCInsureAccTraceDB.setInsuAccNo(aInsuAccNo);
        tLCInsureAccTraceDB.setState("0");
        LCInsureAccTraceSet tLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
        String batchNo = PubFun.getCurrentDate2()+PubFun1.CreateMaxNo("AMNTFA"+PubFun.getCurrentDate2(),6);
        String firstPayDate = "";
        if(tLCInsureAccTraceSet!=null&&tLCInsureAccTraceSet.size()>0){
	        for (int m = 1; m<=tLCInsureAccTraceSet.size(); m++){
	
	    		FDate fdate=new FDate();
	    		String mPayDate=tLCInsureAccTraceSet.get(m).getPayDate();
	    		if(fdate.getDate(untilDate).compareTo(fdate.getDate(mPayDate))>=0){

		    		if(firstPayDate==null || "".equals(firstPayDate)){
		    		      firstPayDate=mPayDate;
		    		 }
		    		//找出最早的日期
		    		if(fdate.getDate(firstPayDate).compareTo(fdate.getDate(mPayDate))>0){
		    			firstPayDate = mPayDate;
		    		}
		    		//对于费用发生日期晚于结算日的将其置为结算日
		    		if(fdate.getDate(mPayDate).compareTo(fdate.getDate(untilDate))>0){
		    			mPayDate=untilDate;
		    		}
		    		Calculator tCalculator = new Calculator();
		        	tCalculator.setCalCode(mCalCode);
		        	tCalculator.addBasicFactor("StartDate", mPayDate);
		        	tCalculator.addBasicFactor("EndDate", untilDate);
		        	tCalculator.addBasicFactor("Money", String.valueOf(tLCInsureAccTraceSet.get(m).getMoney()));
		        	String money = tCalculator.calculate();
		        	if(tCalculator.mErrors.needDealError())
		            {
		        		//CError.buildErr(this, "计算失败", tCalculator.mErrors);
		            }
		        	if(null != money && !"".equals(money)){
		        		resultMoney += Double.parseDouble(money);
		        	}else{
		        		//CError.buildErr(this,"保额记录"+tLCInsureAccTraceSet.get(m).getSerialNo()+"计算失败！");
		        	}
		        	
		        	oldMoney += tLCInsureAccTraceSet.get(m).getMoney();
		        	// 3207 账户记录表 修改为核赔规则和给付确认调用时候进行数据的存储。其他都是只计算不存储  核赔规则=QR 通知给付=JF 。
//		        	if("QR".equals(ExeLocation) ||"JF".equals(ExeLocation)){
//		        	recordAccAmntTrace(aCaseNo,batchNo,mPayDate,untilDate,tLCInsureAccTraceSet.get(m),money,ExeLocation);//记录账户计价轨迹
//		        	}
		        	//如果是最后一条记录那么记录计算合计
		        	if(m==tLCInsureAccTraceSet.size()){
		        		LCInsureAccTraceSchema tLCInsureAccTraceSchema = tLCInsureAccTraceSet.get(m);
		        		tLCInsureAccTraceSchema.setMoneyType("PM");
		        		if("QR".equals(ExeLocation)||"JF".equals(ExeLocation)){
		        		recordAccAmntTraceSum(aCaseNo,batchNo,oldMoney,resultMoney,firstPayDate,untilDate,tLCInsureAccTraceSchema,ExeLocation);
		        		}
		            }
	    		}
	        }
        }
        amnt=resultMoney;
        amnt = Arith.round(amnt, 2);
        return amnt;
    }
    
    
    /**
     *  以下方法是拷贝的getAmntFromAcc方法，本方法只为查询个人历史保额而存在
     *  
     * @return amnt 保险金额
     */
     
    
    /**
     * 账户轨迹表中每笔金额记录的利息计算记录
     * @param aCaseNo
     * @param batchNo
     * @param mPayDate 	计算开始时间
     * @param untilDate	计算截止时间
     * @param lcInsureAccTraceSchema
     * @param money		计算结果金额
     * @param exeLocation 操作执行位置
     */
    private static void recordAccAmntTrace(String aCaseNo, String batchNo,String mPayDate, String untilDate,
			LCInsureAccTraceSchema lcInsureAccTraceSchema, String money,String exeLocation) {
    	String SerialNo = PubFun.getCurrentDate2()+PubFun1.CreateMaxNo("AmntFromAcc"+PubFun.getCurrentDate2(),8);
    	System.out.println("流水号:"+SerialNo+",  批次号"+batchNo+", 计算结果:"+money);
    	
    	LLAccAmntTraceSchema tLLAccAmntTraceSchema = new LLAccAmntTraceSchema();
    	Reflections tR=new Reflections();
		tR.transFields(tLLAccAmntTraceSchema,lcInsureAccTraceSchema);
		tLLAccAmntTraceSchema.setCaseNo(aCaseNo);
		tLLAccAmntTraceSchema.setSerialNo(SerialNo);
		tLLAccAmntTraceSchema.setPatchNo(batchNo);
		tLLAccAmntTraceSchema.setExeLocation(exeLocation);//
		tLLAccAmntTraceSchema.setResultMoney(money);//计算结果金额
		tLLAccAmntTraceSchema.setPayDate(mPayDate);//计算开始时间
		tLLAccAmntTraceSchema.setCalToDate(untilDate);
		tLLAccAmntTraceSchema.setSumFlag("DE");
		tLLAccAmntTraceSchema.setMarkup("保险金额计算明细");
		tLLAccAmntTraceSchema.setMakeDate(PubFun.getCurrentDate());
		tLLAccAmntTraceSchema.setMakeTime(PubFun.getCurrentTime());
		tLLAccAmntTraceSchema.setModifyDate(PubFun.getCurrentDate());
		tLLAccAmntTraceSchema.setModifyTime(PubFun.getCurrentTime());
		VData mResult = new VData();
		MMap tmpMap = new MMap();
		tmpMap.put(tLLAccAmntTraceSchema, "INSERT");
		mResult.add(tmpMap);
        PubSubmit ps = new PubSubmit();
		if (!ps.submitData(mResult, null)) {
			System.out.println("数据库保存失败");
		}	

	}

	/**
     * 在账户计价记录表中记录计算团体补充A的保额的记录
     * @param aCaseNo 
     * @param batchNo		批次编码
     * @param oldMoney      计算本金合计
     * @param resultMoney   若是保险金额=sum(每次计算本金+利息即保额)
     * 						若是基本保险金额=sum(每次计算本金)
     * @param untilDate		计算至日期
     * @param lcInsureAccTraceSchema
     * @param exeLocation	操作执行位置
     */
    private static void recordAccAmntTraceSum(String aCaseNo,String batchNo, double oldMoney,double resultMoney,String StartDate, String untilDate, 
    		LCInsureAccTraceSchema lcInsureAccTraceSchema, String exeLocation) {
		// TODO Auto-generated method stub
    	
    	String SerialNo = PubFun.getCurrentDate2()+PubFun1.CreateMaxNo("AmntFromAcc"+PubFun.getCurrentDate2(),8);
    	System.out.println("流水号:"+SerialNo+",  批次号"+batchNo+", 计算结果:"+resultMoney);
    	LLAccAmntTraceSchema tLLAccAmntTraceSchema = new LLAccAmntTraceSchema();
    	Reflections tR=new Reflections();
    	if(lcInsureAccTraceSchema!=null){
    		tR.transFields(tLLAccAmntTraceSchema,lcInsureAccTraceSchema);
    	}
		tLLAccAmntTraceSchema.setCaseNo(aCaseNo);
		tLLAccAmntTraceSchema.setSerialNo(SerialNo);
		tLLAccAmntTraceSchema.setPatchNo(batchNo);
		tLLAccAmntTraceSchema.setExeLocation(exeLocation);//
		tLLAccAmntTraceSchema.setMoney(oldMoney);//计算本金合计
		tLLAccAmntTraceSchema.setResultMoney(resultMoney);//计算结果金额
		tLLAccAmntTraceSchema.setPayDate(StartDate);//计算开始时间
		tLLAccAmntTraceSchema.setCalToDate(untilDate);//计算至日期
		tLLAccAmntTraceSchema.setOtherNo("");
		tLLAccAmntTraceSchema.setOtherType("");
		tLLAccAmntTraceSchema.setSumFlag("Y");
		tLLAccAmntTraceSchema.setMarkup("保险金额总计或基本保险金额总计");
		tLLAccAmntTraceSchema.setMakeDate(PubFun.getCurrentDate());
		tLLAccAmntTraceSchema.setMakeTime(PubFun.getCurrentTime());
		tLLAccAmntTraceSchema.setModifyDate(PubFun.getCurrentDate());
		tLLAccAmntTraceSchema.setModifyTime(PubFun.getCurrentTime());
		VData mResult = new VData();
		MMap tmpMap = new MMap();
		tmpMap.put(tLLAccAmntTraceSchema, "INSERT");
		mResult.add(tmpMap);
        PubSubmit ps = new PubSubmit();
		if (!ps.submitData(mResult, null)) {
			System.out.println("数据库保存失败");
		}	
	}
    /*以上4个方法是2713                       任务新增方法*/

    /**
     * 普通案件为申诉纠错案件选择处理人，若原handler失效，查找其有效上级
     * @param ClaimHandler
     * @return
     */
    public static String getAppealHandler(String ClaimHandler) {
        String Handler = ClaimHandler;
        //创建  LLClaimUserDB
        LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
        tLLClaimUserDB.setUserCode(Handler);
        //得到ClaimUser信息
        tLLClaimUserDB.getInfo();
        LLClaimUserSchema tLLClaimUserSchema = tLLClaimUserDB.getSchema();

        if (tLLClaimUserSchema.getStateFlag().equals("1")) {
            //原案件理赔员当前状态合法
            return Handler;
        }

        else { //原案件理赔员当前状态非法,查找其上级理赔员
            while ((!("").equals(StrTool.cTrim(tLLClaimUserSchema.
                                               getUpUserCode()))) &&
                   (!tLLClaimUserSchema.getUpUserCode().equals(
                           tLLClaimUserSchema.getUserCode())) &&
                   tLLClaimUserSchema.getStateFlag().equals("0")) {
                tLLClaimUserDB.setUserCode(tLLClaimUserSchema.getUpUserCode());
                if (!tLLClaimUserDB.getInfo()) {
                    break;
                }
                tLLClaimUserDB.getInfo();
                tLLClaimUserSchema = tLLClaimUserDB.getSchema();
            }

            if (Handler.equals("")) { //所有理赔员当前状态均不合法
                return null;
            } else { //查找到合法上级理赔员
                return Handler;
            }
        }
    }
    
    /**
     * 社保案件为申诉纠错案件选择处理人，若原handler失效，查找其有效上级
     * @param ClaimHandler
     * @return
     */
    public static String getSSAppealHandler(String ClaimHandler) {
        String Handler = ClaimHandler;
        //创建  LLClaimUserDB
        LLSocialClaimUserDB tLLSocialClaimUserDB = new LLSocialClaimUserDB();
        tLLSocialClaimUserDB.setUserCode(Handler);
        //得到ClaimUser信息
        tLLSocialClaimUserDB.getInfo();
        LLSocialClaimUserSchema tSocialLLClaimUserSchema = tLLSocialClaimUserDB.getSchema();

        if ("1".equals(tSocialLLClaimUserSchema.getStateFlag())) {
            //原案件理赔员当前状态合法
        	System.out.println("原理赔员合法，Handler："+Handler);
            return Handler;
        }

        else { //原案件理赔员当前状态非法,查找其上级理赔员
        	Handler = "";
            while ((!("").equals(StrTool.cTrim(tSocialLLClaimUserSchema.
                                               getUpUserCode()))) &&
                   (!tSocialLLClaimUserSchema.getUpUserCode().equals(
                		   tSocialLLClaimUserSchema.getUserCode())) &&
                		   !tSocialLLClaimUserSchema.getStateFlag().equals("1")) {
            	tLLSocialClaimUserDB.setUserCode(tSocialLLClaimUserSchema.getUpUserCode());
                if (!tLLSocialClaimUserDB.getInfo()) {
                    break;
                }
                tSocialLLClaimUserSchema = tLLSocialClaimUserDB.getSchema();
                Handler = tSocialLLClaimUserSchema.getUserCode();
            }
            System.out.println("原理赔员非法，合法上级的Handler："+Handler);
            if ("".equals(Handler)) { //所有理赔员当前状态均不合法
                return null;
            } else { //查找到合法上级理赔员
                return Handler;
            }
        }
    }
    
    /**
     * 社保案件分配
     * @param ManageCom	前台传入的受理机构
     * @return 社保权限中，有案件分配权限，且案件数最少的用户
     */
    public String SocialSecurityChooseUser(String ManageCom) {
    	System.out.println("<-Go Into SocialSecurityChooseUser->");
        if (ManageCom == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseCommon";
            tError.functionName = "SocialSecurityChooseUser";
            tError.errorMessage = "传入参数不能为空!";
            this.mErrors.addOneError(tError);
            return null;
        }

        System.out.println("传入的受理机构："+ManageCom);
        String usrPart = "";
        
        //社保案件分配支持机构处理、分公司处理 ，优先机构处理       
        usrPart = " and comcode='" + ManageCom + "'";

        String tCurrentDate = PubFun.getCurrentDate();
        String AheadDays = "-120";
        FDate tD = new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(tCurrentDate),
                                        Integer.parseInt(AheadDays), "D", null);
        FDate fdate = new FDate();
        String tAfterDate = fdate.getString(AfterDate);
        
        //判断逻辑：最近120天内，社保用户下待处理案件由小到大、社保案件分配上限由大到小，参见案件分配，用户有效的用户
        String sqlStr =
                "select usercode a,(select count(caseno) from llcase b " +
                " where b.handler=a.usercode and b.endcasedate is null AND RGTSTATE<>'14' "
                + " and rgtdate between '" + tAfterDate + "' and '" +
                tCurrentDate + "') b,"
                + "dispatchrate c from LLSocialClaimUser a where HandleFlag='1' and "
                + "  StateFlag='1' and usercode!='001' and " +
                		" exists(select 1 from lduser where userstate='0' and usercode=a.usercode) "
                + usrPart + " order by b asc,c desc";

        System.out.println(sqlStr);
        SSRS tss = new SSRS();
        ExeSQL texesql = new ExeSQL();
        tss = texesql.execSQL(sqlStr);
        if (tss.getMaxRow() <= 0) {//添加社保分机构处理
        	if(ManageCom.length() == 8){
        		ManageCom = ManageCom.substring(0, 4);
        		return SocialSecurityChooseUser(ManageCom);
        	}else{
        		//@@错误处理
                CError tError = new CError();
                tError.moduleName = "LLCaseCommon";
                tError.functionName = "SocialSecurityChooseUser";
                tError.errorMessage = "没有符合条件的处理人!";
                this.mErrors.addOneError(tError);
                return null;
        	}       	
            
        }
        String saveOper = "";
        double minrate = 0.00;
        String minOper = "";
        for (int i = 1; i <= tss.getMaxRow(); i++) {
            int actucount = 0;
            int dispatchlimit = 0;
            actucount = Integer.parseInt(tss.GetText(i, 2));
            dispatchlimit = Integer.parseInt(tss.GetText(i, 3));
            if (actucount < dispatchlimit) {
                saveOper = tss.GetText(i, 1);
                break;
            } else {
                if (dispatchlimit <= 0) {
                    continue;
                }
                double trate = actucount * 1.0 / dispatchlimit;
                if (i == 1) {
                    minrate = trate;
                    minOper = tss.GetText(i, 1);
                } else {
                    if (minrate > trate) {
                        minrate = trate;
                        minOper = tss.GetText(i, 1);
                    }
                }
                saveOper = minOper;
            }
        }
        System.out.println(saveOper);
        return saveOper;
    }

    /**
     * 返回工作量最少的核赔员编码
     * @param ManageCom 登陆机构
     * @return  核赔员编码
     */
    public String ChooseAssessor(String ManageCom, String customerNo, String SC) {
        String saveOper = ChooseUser(ManageCom, customerNo, SC, "");
        return saveOper;
    }

    public String ChooseUser(String ManageCom, String customerNo, String SC,
                             String Operate) {
        if (ManageCom == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseCommon";
            tError.functionName = "ChooseAssessor";
            tError.errorMessage = "传入参数不能为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
       System.out.println("customerNo:" + customerNo);
//   String ComCode=ManageCom.trim();
        String comCode = "86";
        String usrPart = "";
        String partSQL = "";
    //    String partSQL1 = "";
        if ("01".equals(SC)) {
            comCode = ManageCom;
            partSQL = " comcode='" + ManageCom + "' ";
            usrPart = " and comcode='" + ManageCom + "'";
        } else if("04".equals(SC)){
            if (ManageCom.length() > 4) {
            	comCode = ManageCom.substring(0, 4);
                //partSQL = "comcode in('" + ManageCom.substring(0, 4) + "','" + ManageCom.substring(0, 4) + "0000')";
            	partSQL = "comcode ='" + ManageCom + "' ";
            } else {
            	comCode = ManageCom ;
                partSQL = "comcode in('" + ManageCom + "','" + ManageCom + "0000')";
            }
     //       partSQL1 = "comcode like '" + ManageCom.substring(0, 4) + "%' ";
            usrPart = "and comcode= '"+ManageCom.substring(0, 4)+"' ";
        }else {
            if (ManageCom.length() > 4) {
                partSQL = "comcode like '" + ManageCom.substring(0, 4) + "%'";
            } else {
                partSQL = "comcode like '" + ManageCom + "%'";
            }

            usrPart = "and comcode= '86' ";
        }
        usrPart += "and usercode!= '" + Operate + "'  ";
        String riskType = "";
        //校验客户属于个险用户还是团险用户
        if (isSingle(customerNo)) {
            riskType = "1";
        } else {
            riskType = "2";
        }
        String sql_1 =
                "select distinct code from ldcode1 d where codetype='LLCaseAssign' and "
                + partSQL + " and othersign='"
                + riskType +
                "' and exists (select 1 from llclaimuser where comcode='" +
                comCode + "' and usercode=d.code) with ur";
        System.out.println(sql_1);
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql_1);
        System.out.println("result:" + (tSSRS != null));
        if (tSSRS.getMaxRow() > 0) {
            usrPart = usrPart + " and a.usercode in (" + linkString(tSSRS) +
                      ") ";
        } else {
            String sql_2 =
                    "select distinct code from ldcode1 d where codetype='LLCaseAssign' and  "
                    + partSQL +
                    "  and exists (select 1 from llclaimuser where comcode='" +
                    comCode + "' and usercode=d.code) with ur";
            SSRS tSSRS1 = tExeSQL.execSQL(sql_2);
            if (tSSRS1.getMaxRow() > 0) {
                usrPart = usrPart + " and a.usercode in (" + linkString(tSSRS1) +
                          ") ";
            }
        }

        String tCurrentDate = PubFun.getCurrentDate();
        String AheadDays = "-120";
        FDate tD = new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(tCurrentDate),
                                        Integer.parseInt(AheadDays), "D", null);
        FDate fdate = new FDate();
        String tAfterDate = fdate.getString(AfterDate);

        String sqlStr =
                "select usercode a,(select count(caseno) from llcase b " +
                " where b.handler=a.usercode and b.endcasedate is null AND RGTSTATE<>'14' "
                + " and rgtdate between '" + tAfterDate + "' and '" +
                tCurrentDate + "') b,"
                + "dispatchrate c from llclaimuser a where HandleFlag='1' and "
                + " ClaimDeal='1' and StateFlag='1' and usercode!='001'"
                + usrPart + " order by b asc,c desc";

        System.out.println(sqlStr);
        SSRS tss = new SSRS();
        ExeSQL texesql = new ExeSQL();
        tss = texesql.execSQL(sqlStr);
        if (tss.getMaxRow() <= 0) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseCommon";
            tError.functionName = "ChooseAssessor";
            tError.errorMessage = "没有符合条件的处理人!";
            this.mErrors.addOneError(tError);
            return null;
        }
        String saveOper = "";
        double minrate = 0.00;
        String minOper = "";
        for (int i = 1; i <= tss.getMaxRow(); i++) {
            int actucount = 0;
            int dispatchlimit = 0;
            actucount = Integer.parseInt(tss.GetText(i, 2));
            dispatchlimit = Integer.parseInt(tss.GetText(i, 3));
            if (actucount < dispatchlimit) {
                saveOper = tss.GetText(i, 1);
                break;
            } else {
                if (dispatchlimit <= 0) {
                    continue;
                }
                double trate = actucount * 1.0 / dispatchlimit;
                if (i == 1) {
                    minrate = trate;
                    minOper = tss.GetText(i, 1);
                } else {
                    if (minrate > trate) {
                        minrate = trate;
                        minOper = tss.GetText(i, 1);
                    }
                }
                saveOper = minOper;
            }
        }
        System.out.println(saveOper);
        return saveOper;
    }

    public String ChooseUserNew(String ManageCom, String customerNo, String SC,
            String Operate,String caseFlag) {
    	if (ManageCom == null) {
    		// @@错误处理
				CError tError = new CError();
				tError.moduleName = "LLCaseCommon";
				tError.functionName = "ChooseAssessor";
				tError.errorMessage = "传入参数不能为空!";
				this.mErrors.addOneError(tError);
				return null;
			}
				System.out.println("customerNo:" + customerNo);
				//String ComCode=ManageCom.trim();
				String comCode = "86";
				String usrPart = "";
				String riskType = "";
//				校验客户属于个险用户还是团险用户
				if ("I".equals(caseFlag)) {
					riskType = "1";
					} else {
					riskType = "2";
					}	
				String partSQL = "";
				String usrPart1 = "";
				String partSQL1 = "";
			if ("01".equals(SC)) {
				comCode = ManageCom;
				partSQL = " comcode =substr('" + ManageCom + "',1,length(trim(comcode))) and length(trim(comcode))<=length('" + ManageCom + "') ";
				usrPart = " and comcode='" + ManageCom + "'";
				partSQL1 = " d.comcode =substr('" + ManageCom + "',1,length(trim(d.comcode))) and length(trim(d.comcode))<=length('" + ManageCom + "') and c.comcode='" + ManageCom + "' ";
					
			} else {
				if (ManageCom.length() > 4) {
				partSQL = "comcode =substr('" + ManageCom + "',1,length(trim(comcode))) and length(trim(comcode))<=length('" + ManageCom + "') ";
				partSQL1 = "d.comcode =substr('" + ManageCom + "',1,length(trim(d.comcode))) and length(trim(d.comcode))<=length('" + ManageCom + "')  " ;
				} else {
				partSQL = "comcode =substr('" + ManageCom + "',1,length(trim(comcode))) and length(trim(comcode))<=length('" + ManageCom + "') ";
				partSQL1 = "d.comcode =substr('" + ManageCom + "',1,length(trim(d.comcode))) and length(trim(d.comcode))<=length('" + ManageCom + "')  " ;
				}
				
				usrPart = "and comcode =  '86' ";
			}
			usrPart += "and usercode!= '" + Operate + "'  ";
			String sql_1 =
			        "select distinct code from ldcode1 d,llclaimuser c where c.usercode=d.code and d.codetype='LLCaseAssign' and "
					+ partSQL1 + " and d.othersign='"
					+ riskType +
					"' and c.comcode =substr('" + comCode + "',1,length(trim(c.comcode))) "
	                +" and length(trim(c.comcode))<=length('" + comCode + "') and  c.HandleFlag = '1' and c.ClaimDeal = '1' and c.StateFlag = '1' with ur";
//					"'  and exists (select 1 from llclaimuser where  comcode =substr('" + comCode + "',1,length(trim(comcode))) and length(trim(comcode))<=length('" + comCode + "')  " 
//					  +" and usercode=d.code) with ur";
					System.out.println(sql_1);
					ExeSQL tExeSQL = new ExeSQL();
					SSRS tSSRS = tExeSQL.execSQL(sql_1);
					System.out.println("result:" + (tSSRS != null));
					if (tSSRS.getMaxRow() > 0) {
					usrPart = usrPart + " and a.usercode in (" + linkString(tSSRS) +
					     ") ";
					} else {
					String sql_2 =
					"select distinct code from ldcode1 d,llclaimuser c where c.usercode=d.code and  c.HandleFlag = '1' and c.ClaimDeal = '1' and c.StateFlag = '1'  and d.codetype='LLCaseAssign' and "
					+ partSQL1 + 
					" and c.comcode =substr('" + comCode + "',1,length(trim(c.comcode))) "
	                +" and length(trim(c.comcode))<=length('" + comCode + "') with ur";
					SSRS tSSRS1 = tExeSQL.execSQL(sql_2);
					if (tSSRS1.getMaxRow() > 0) {
					usrPart = usrPart + " and a.usercode in (" + linkString(tSSRS1) +
					         ") ";
					}
					
			}
	
			
			
				
				String tCurrentDate = PubFun.getCurrentDate();
				String AheadDays = "-120";
				FDate tD = new FDate();
				Date AfterDate = PubFun.calDate(tD.getDate(tCurrentDate),
				                       Integer.parseInt(AheadDays), "D", null);
				FDate fdate = new FDate();
				String tAfterDate = fdate.getString(AfterDate);
				
				String sqlStr =
				"select usercode a,(select count(caseno) from llcase b " +
				" where b.handler=a.usercode and b.endcasedate is null AND RGTSTATE<>'14' "
				+ " and rgtdate between '" + tAfterDate + "' and '" +
				tCurrentDate + "') b,"
				+ "dispatchrate c from llclaimuser a where HandleFlag='1' and "
				+ " ClaimDeal='1' and StateFlag='1' and usercode!='001'"
				+ usrPart + " order by b asc,c desc";
				
				System.out.println(sqlStr);
				SSRS tss = new SSRS();
				ExeSQL texesql = new ExeSQL();
				tss = texesql.execSQL(sqlStr);
				if (tss.getMaxRow() <= 0) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "LLCaseCommon";
				tError.functionName = "ChooseAssessor";
				tError.errorMessage = "没有符合条件的处理人!";
				this.mErrors.addOneError(tError);
				return null;
				}
				String saveOper = "";
				double minrate = 0.00;
				String minOper = "";
				for (int i = 1; i <= tss.getMaxRow(); i++) {
				int actucount = 0;
				int dispatchlimit = 0;
				actucount = Integer.parseInt(tss.GetText(i, 2));
				dispatchlimit = Integer.parseInt(tss.GetText(i, 3));
				if (actucount < dispatchlimit) {
				saveOper = tss.GetText(i, 1);
				break;
				} else {
				if (dispatchlimit <= 0) {
				   continue;
				}
				double trate = actucount * 1.0 / dispatchlimit;
				if (i == 1) {
				   minrate = trate;
				   minOper = tss.GetText(i, 1);
				} else {
				   if (minrate > trate) {
				       minrate = trate;
				       minOper = tss.GetText(i, 1);
				   }
				}
				saveOper = minOper;
				}
				}
				System.out.println(saveOper);
				return saveOper;
				}

    public String chooseInputer(String ManageCom, String SC) {
        String saveInputer = "";
        if (ManageCom == null) {
            // @@错误处理
            CError.buildErr(this, "传入的管理机构为空!");
            return null;
        }
        String usrPart = "";
        if ("01".equals(SC)) {
            usrPart = " and comcode='" + ManageCom + "'";
        } else {
            usrPart = "and comcode= '86' ";
        }
        String statePart = " and exists (select 1 from llclaimuser where usercode=lduser.usercode and handleflag='1' and stateflag='1' ) ";

        String tCurrentDate = PubFun.getCurrentDate();
        String AheadDays = "-120";
        FDate tD = new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(tCurrentDate),
                                        Integer.parseInt(AheadDays), "D", null);
        FDate fdate = new FDate();
        String tAfterDate = fdate.getString(AfterDate);

        String sqlStr = "select * from (select usercode a,"
                        + "(select count(caseno) from llcase "
                        +
                        " where claimer=usercode and rgtstate in ('01','02','13','07','08')"
                        + " and receiptflag<>'1' "
                        + " and rgtdate between '" + tAfterDate + "' and '" +
                        tCurrentDate + "') b "
                        +
                        " from lduser where claimpopedom='2' and userstate<>'1' "
                        + statePart
                        + usrPart
                        + ") as x order by b";

        System.out.println(sqlStr);
        ExeSQL exeSQL = new ExeSQL();
        SSRS ssrs = exeSQL.execSQL(sqlStr);
        if (ssrs == null || ssrs.getMaxRow() <= 0) {
            // @@错误处理
            CError.buildErr(this, "用户表中没有查到符合条件的纪录!");
            sqlStr =
                    "select usercode from lduser where claimpopedom='2' and userstate<>'1' " +
                    statePart + usrPart;
            ssrs = exeSQL.execSQL(sqlStr);
            if (ssrs == null || ssrs.getMaxRow() <= 0) {
                return null;
            } else {
                return ssrs.GetText(ssrs.getMaxNumber(), 1);
            }
        }
        saveInputer = ssrs.GetText(1, 1);
        return saveInputer;
    }

    /**
     * 给付确认后把案件下的赔付明细表的中的
     * realpay加到lcget中的summoney
     */
    public static MMap addSumMoney(String caseNo, String operator) {
        String tCaseNo = caseNo;
        String tOperator = operator;
        String tPolNo;
        String tDutyCode;
        String tGetDutyCode;
        String tDate = PubFun.getCurrentDate();
        String tTime = PubFun.getCurrentTime();

        LLClaimDetailSchema tLLClaimDetailSchema;
        LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
        LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();

        LCGetSchema tLCGetSchema;
        LCGetDB tLCGetDB;

        LBGetSchema tLBGetSchema;
        LBGetDB tLBGetDB;

        MMap tMMap = new MMap();
//        tLLClaimDetailDB.setCaseNo(tCaseNo);
//        tLLClaimDetailSet.set(tLLClaimDetailDB.query());
//        if (tLLClaimDetailSet != null && tLLClaimDetailSet.size() != 0) {
//            for (int i = 1; i <= tLLClaimDetailSet.size(); i++) {
//                tLLClaimDetailSchema = new LLClaimDetailSchema();
//                tLLClaimDetailSchema = tLLClaimDetailSet.get(i);
        String tSQL = "";
        //处理批次统一给付的情况
        if ("P".equals(tCaseNo.substring(0, 1))) {
            tSQL =
                    "select polno,dutycode,getdutycode,sum(realpay) from llclaimdetail a,llcase b "
                    + "where a.caseno=b.caseno and b.rgtno='" + tCaseNo +
                    "' and b.rgtstate='09' "
                    + "group by polno,dutycode,getdutycode";
        } else {
            //纠错申诉案件一个给付责任会有多条
            tSQL =
                    "select polno,dutycode,getdutycode,sum(realpay) from llclaimdetail"
                    + " where caseno='" + tCaseNo +
                    "' group by polno,dutycode,getdutycode";
        }
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(tSQL);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            tPolNo = new String();
            tDutyCode = new String();
            tGetDutyCode = new String();
            tPolNo = tSSRS.GetText(i, 1);
            tDutyCode = tSSRS.GetText(i, 2);
            tGetDutyCode = tSSRS.GetText(i, 3);
            double tRealPay = Arith.round(Double.parseDouble(tSSRS.GetText(i, 4)),
                                          2);
            tLCGetDB = new LCGetDB();
            tLCGetDB.setPolNo(tPolNo);
            tLCGetDB.setDutyCode(tDutyCode);
            tLCGetDB.setGetDutyCode(tGetDutyCode);
            if (tLCGetDB.getInfo()) {
                tLCGetSchema = new LCGetSchema();
                tLCGetSchema.setSchema(tLCGetDB.getSchema());
                //更新领取金额
                tLCGetSchema.setSumMoney(Arith.round(tLCGetSchema.getSumMoney() +
                        tRealPay, 2));
                tLCGetSchema.setOperator(tOperator);
                tLCGetSchema.setModifyDate(tDate);
                tLCGetSchema.setModifyTime(tTime);
                tMMap.put(tLCGetSchema, "UPDATE");
            } else {
                //如果没有找到lcget，则到备份表中查找
                tLBGetDB = new LBGetDB();
                tLBGetDB.setPolNo(tPolNo);
                tLBGetDB.setDutyCode(tDutyCode);
                tLBGetDB.setGetDutyCode(tGetDutyCode);
                if (tLBGetDB.getInfo()) {
                    tLBGetSchema = new LBGetSchema();
                    tLBGetSchema = tLBGetDB.getSchema();
                    //更新领取金额
                    tLBGetSchema.setSumMoney(Arith.round(tLBGetSchema.
                            getSumMoney() +
                            tRealPay, 2));
                    tLBGetSchema.setOperator(tOperator);
                    tLBGetSchema.setModifyDate(tDate);
                    tLBGetSchema.setModifyTime(tTime);
                    tMMap.put(tLBGetSchema, "UPDATE");
                } else {
                    //错误处理，返回false；
                }
            }
        }
        return tMMap;
    }

    public static MMap gotoBack(String caseNo, GlobalInput tG) {
        System.out.println("begin gotoback");
        ContCancel tContCancel = new ContCancel();
        tContCancel.setEdorType("04");
        MMap mMMap = new MMap();
        HashMap saveCont = new HashMap();
        String strsql = "select distinct a.ContNo,a.grpcontno "
                        + " from  LLClaimDetail a,LMDutyGetClm b "
                        + " where b.getdutycode = a.getdutycode "
                        + " and b.getdutykind = a.getdutykind "
                        + " and b.afterget='003' "
                        + " and a.caseno = '" + caseNo + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();

        tSSRS = tExeSQL.execSQL(strsql);
        if (tSSRS != null) {
            for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                //如果没有处理过消户,则调用消户
                String contno = tSSRS.GetText(i, 1);
                if (!saveCont.containsKey(contno)) {
                    saveCont.put(contno, contno);
                    System.out.println(contno);
                    mMMap.add(tContCancel.prepareContData(contno, caseNo));
                    System.out.println(mMMap.toString());
                }
                strsql = "select getnoticeno from ljspay where otherno ='"
                         + contno + "' and othernotype='2' or otherno='"
                         + tSSRS.GetText(i, 2) + "' and othernotype = '1'";
                SSRS tss = new SSRS();
                tss = tExeSQL.execSQL(strsql);
                IndiLJSCancelUI tIndiLJSCancelUI = new IndiLJSCancelUI();
                String backMsg = "";
                for (int j = 1; j <= tss.getMaxRow(); j++) {
                    try {
                        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
                        tLJSPaySchema.setGetNoticeNo(tss.GetText(i, 1));
                        TransferData tTransferData = new TransferData();
                        //作废原因为收费失败
                        tTransferData.setNameAndValue("CancelMode", "5");
                        VData tVData = new VData();
                        tVData.addElement(tLJSPaySchema);
                        tVData.addElement(tG);
                        tVData.addElement(tTransferData);

                        if (!tIndiLJSCancelUI.submitData(tVData, "INSERT")) {
                            backMsg += "作废合同" + contno
                                    + "的应收失败,请手动作废催收记录！<br>";
                        }
                    } catch (Exception ex) {
                    }

                }
            }
        }
        System.out.println("after gotoback");
        return mMMap;
    }

    /**计算案件时效*/
    public LLCaseOpTimeSchema CalTimeSpan(LLCaseOpTimeSchema
                                          mLLCaseOpTimeSchema) {
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        String CaseNo = mLLCaseOpTimeSchema.getCaseNo();
        String RgtState = mLLCaseOpTimeSchema.getRgtState();
        String ManageCom = mLLCaseOpTimeSchema.getManageCom();
        String Operator = mLLCaseOpTimeSchema.getOperator();
        int Seq = mLLCaseOpTimeSchema.getSequance();
        Seq = Seq + 1;
        int CountTimes;
        LLCaseOpTimeDB tLLCaseOpTimeDB = new LLCaseOpTimeDB();
        LLCaseOpTimeSet tLLCaseOpTimeSet = new LLCaseOpTimeSet();
        tLLCaseOpTimeDB.setCaseNo(CaseNo);
        tLLCaseOpTimeDB.setRgtState(RgtState);
        if (RgtState == "07") {
            tLLCaseOpTimeDB.setSequance(Seq);
        }
        tLLCaseOpTimeSet = tLLCaseOpTimeDB.query();
        CountTimes = tLLCaseOpTimeSet.size();
        if (CountTimes == 0) {
            if (RgtState != "07") {
                mLLCaseOpTimeSchema.setSequance(Seq);
            }
            //添加理赔二核状态“16”，Houyd修改于2013-12-17
            if (RgtState == "07" || RgtState == "10" || RgtState == "09" || RgtState == "16") {
                mLLCaseOpTimeSchema.setStartDate(PubFun.getCurrentDate());
                mLLCaseOpTimeSchema.setStartTime(PubFun.getCurrentTime());
            }
            if (RgtState == "02" || RgtState == "03" || RgtState == "04" ||
                RgtState == "05" ||
                RgtState == "06") {
                String preRgtState = "";
                String tSql =
                        "select docid from es_doc_main where doccode='" +
                        CaseNo + "' ";
                String ScanFlag = tExeSQL.getOneValue(tSql);
                if (RgtState.equals("03") && StrTool.cTrim(ScanFlag).equals("")) {
                    preRgtState = "01";
                } else {
                    int rgts = Integer.parseInt(RgtState) - 1;
                    preRgtState = "0" + rgts;
                }

                LLCaseOpTimeDB preLLCaseOpTimeDB = new LLCaseOpTimeDB();
                preLLCaseOpTimeDB.setCaseNo(CaseNo);
                //preLLCaseOpTimeDB.setSequance(1);
                preLLCaseOpTimeDB.setRgtState(preRgtState);

                String SQLX = "select EndDate,EndTime from llcaseoptime where " +
                              " caseno ='" + CaseNo + "' and rgtstate='" +
                              preRgtState +
                              "' and Sequance=(select max(Sequance) from " +
                              " llcaseoptime where caseno ='" + CaseNo +
                              "' and rgtstate='" + preRgtState + "')";
                tSSRS = tExeSQL.execSQL(SQLX);
                if (preLLCaseOpTimeDB.query().size() < 1) {
                    System.out.println("时效表查询失败！");
                } else {
                    if (tSSRS != null && tSSRS.getMaxRow() > 0 &&
                        !tSSRS.GetText(1, 1).equals("")) {
                        mLLCaseOpTimeSchema.setStartDate(tSSRS.GetText(1, 1));
                        mLLCaseOpTimeSchema.setStartTime(tSSRS.GetText(1, 2));
                    } else {
                        mLLCaseOpTimeSchema.setStartDate(PubFun.
                                getCurrentDate());
                        mLLCaseOpTimeSchema.setStartTime(PubFun.
                                getCurrentTime());
                    }
                }

            }
            if (RgtState != "07" && RgtState != "01" && RgtState != "10" && RgtState != "16") {
                mLLCaseOpTimeSchema.setEndDate(PubFun.getCurrentDate());
                mLLCaseOpTimeSchema.setEndTime(PubFun.getCurrentTime());
            }

        } else {
            LLCaseOpTimeSchema preLLCaseOpTimeSchema = tLLCaseOpTimeSet.get(
                    CountTimes);
            String preOperator = StrTool.cTrim(preLLCaseOpTimeSchema.
                                               getOperator());
            String tempOperator = StrTool.cTrim(Operator);
            if (tempOperator.equals("")) {
                tempOperator = preOperator;
            }
            LLCaseBackDB tLLCaseBackDB = new LLCaseBackDB();
            String SQLX = "select * from llcaseback where " +
                          " caseno ='" + CaseNo + "'and BackType is null ";
            LLCaseBackSet tLLCaseBackSet = tLLCaseBackDB.executeQuery(SQLX);
            if ((RgtState.equals("02") || RgtState.equals("06") ||
                 tLLCaseBackSet.size() > 0 || !preOperator.equals(tempOperator))
                && !RgtState.equals("07") && !RgtState.equals("10") &&
                (RgtState.equals("01") && preLLCaseOpTimeSchema.getEndDate() != null)) {
                mLLCaseOpTimeSchema.setSequance(Seq);
                mLLCaseOpTimeSchema.setStartDate(preLLCaseOpTimeSchema.
                                                 getEndDate());
                mLLCaseOpTimeSchema.setStartTime(preLLCaseOpTimeSchema.
                                                 getEndTime());
            } else {
                mLLCaseOpTimeSchema = tLLCaseOpTimeSet.get(CountTimes);
                mLLCaseOpTimeSchema.setSequance(Seq);
                if (!StrTool.cTrim(Operator).equals("")) {
                    mLLCaseOpTimeSchema.setOperator(Operator);
                }
                if (!StrTool.cTrim(ManageCom).equals("")) {
                    mLLCaseOpTimeSchema.setManageCom(ManageCom);
                }
            }
            if (mLLCaseOpTimeSchema.getOperator() == null ||
                mLLCaseOpTimeSchema.getOperator().equals("")) {
                mLLCaseOpTimeSchema.setOperator(tempOperator);
            }
            if (mLLCaseOpTimeSchema.getManageCom() == null ||
                mLLCaseOpTimeSchema.getManageCom().equals("")) {
                mLLCaseOpTimeSchema.setManageCom(preLLCaseOpTimeSchema.
                                                 getManageCom());
            }
            mLLCaseOpTimeSchema.setEndDate(PubFun.getCurrentDate());
            mLLCaseOpTimeSchema.setEndTime(PubFun.getCurrentTime());
        }
        if (mLLCaseOpTimeSchema.getEndDate() != null && mLLCaseOpTimeSchema.getStartDate() !=null) {
            System.out.println("EndCaseDate不为空");
            String strSql = "select sum(to_date('" +
                            mLLCaseOpTimeSchema.getEndDate() + "')-to_date('"
                            + mLLCaseOpTimeSchema.getStartDate() +
                            "'))* 86400+sum(MIDNIGHT_SECONDS('"
                            + mLLCaseOpTimeSchema.getEndTime() +
                            "')- MIDNIGHT_SECONDS('" +
                            mLLCaseOpTimeSchema.getStartTime()
                            + "')) FROM dual";
            ExeSQL exesql = new ExeSQL();
            String Timespan = exesql.getOneValue(strSql);
            double timespan = 0;
            if (Timespan != null && !"".equals(Timespan)) {
                timespan = Double.parseDouble(Timespan);
            }
            int hh = (int) Math.floor(timespan / 3600);
            int mm = (int) Math.floor((timespan - hh * 3600) / 60);
            int ss = (int) timespan - hh * 3600 - mm * 60;
            String tspan = hh + ":" + mm + ":" + ss;
            mLLCaseOpTimeSchema.setOpTime(tspan);
        } else {
            mLLCaseOpTimeSchema.setOpTime("0:00:00");
        }
        //导致审批轨迹缺失 2012-01-16 mn
//        mLLCaseOpTimeSchema.setSequance(1);//防止出现enddate为空的情况
        System.out.println("end=" + mLLCaseOpTimeSchema.getEndDate());
        System.out.println("start=" + mLLCaseOpTimeSchema.getStartDate());
        return mLLCaseOpTimeSchema;
    }

    //帐户结息
    public double getBonus(String sInsuAccNo, String sContNo, double base) {
        LMRiskInsuAccDB tLMRiskInsuAccDB = new LMRiskInsuAccDB();
        tLMRiskInsuAccDB.setInsuAccNo(sInsuAccNo);
        if (!tLMRiskInsuAccDB.getInfo()) {
            this.mErrors.copyAllErrors(tLMRiskInsuAccDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLCaseCommon";
            tError.functionName = "getBonus";
            tError.errorMessage = "帐户信息查询失败!";
            this.mErrors.addOneError(tError);
            return 0;
        }
        String sInterestTable = tLMRiskInsuAccDB.getAccRateTable();
        String ISql = "select * from " + sInterestTable +
                      " where InsuAccNo = '"
                      + sInsuAccNo + "'";
        ExeSQL exesql = new ExeSQL();
        SSRS ssrs = exesql.execSQL(ISql);
        if (ssrs == null || ssrs.getMaxRow() <= 0) {
            this.mErrors.copyAllErrors(tLMRiskInsuAccDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLCaseCommon";
            tError.functionName = "getBonus";
            tError.errorMessage = "利率表信息查询失败!";
            this.mErrors.addOneError(tError);
            return 0;
        }
        String[] rowdata = ssrs.getRowData(1);
        String sRateType = rowdata[2];
        String sRateInteval = rowdata[3];
        String sRate = rowdata[5];
        double rate = Double.parseDouble(sRate);
        double principle = 10000;

        return 0;
    }

    /**
     * 返回险种保单状态，标识该保单是否进行过理赔
     * 0 未进行过理赔，1 理赔立案但未结案，2 理赔结案但未确认给付， 3 理赔给付确认完毕，false 该保单不存在
     * @param PolNo String
     * @return String
     */
    public String getClaimState(String aPolNo) {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(aPolNo);
        if (!tLCPolDB.getInfo()) {
            return "false";
        }
        LJAGetClaimDB tLJAGetClaimDB = new LJAGetClaimDB();
        tLJAGetClaimDB.setPolNo(aPolNo);
        LJAGetClaimSet tLJAGetClaimSet = new LJAGetClaimSet();
        tLJAGetClaimSet = tLJAGetClaimDB.query();
        //理赔实付表里有数，所以该保单理赔过
        if (tLJAGetClaimSet.size() > 0) {
            return "3";
        }
        LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB();
        tLJSGetClaimDB.setPolNo(aPolNo);
        LJSGetClaimSet tLJSGetClaimSet = new LJSGetClaimSet();
        tLJSGetClaimSet = tLJSGetClaimDB.query();
        //理赔应付付表里有数，所以该保单已经核赔
        if (tLJSGetClaimSet.size() > 0) {
            return "2";
        }

        String tsql = "select a.caseno from llcase a where a.customerno='"
                      + tLCPolDB.getInsuredNo() +
                      "' and a.rgtstate not in ('14','12','11')";
        ExeSQL exesql = new ExeSQL();
        String tmcaseno = exesql.getOneValue(tsql);
        //如果查询为空表示该保单客户无理赔，如果查询不为空则该保单有可能理赔，但未最终确认
        if (tmcaseno.equals("null") || tmcaseno.equals("")) {
            return "0";
        } else {
            return "1";
        }
    }

    /**
     * 返回工作量最少的核赔员编码
     * @param ManageCom 登陆机构
     * @return  核赔员编码
     */
    public String ChooseUser1(String ManageCom, String SC, String Operate) {
        if (ManageCom == null) {
            // @@错误处理
            CError.buildErr(this, "传入的管理机构为空!");
            return null;
        }
        String usrPart = "";
        if ("01".equals(SC)) {
            usrPart = " and (comcode='" + ManageCom + "' or comcode='"
                      + ManageCom.substring(0, 4) + "')";
        } else {
            usrPart = "and comcode= '86' ";
        }
        usrPart += "and usercode!= '" + Operate + "'  ";

        String sqlStr = "select * from llclaimuser where HandleFlag='1' and ClaimDeal='1' and StateFlag='1' and usercode!='001'" +
                        usrPart;

        System.out.println(sqlStr);
        LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
        LLClaimUserSet tLLClaimUserSet = new LLClaimUserSet();
        tLLClaimUserSet = tLLClaimUserDB.executeQuery(sqlStr);
        if (tLLClaimUserDB.mErrors.needDealError()) {
            // @@错误处理
            CError.buildErr(this, "用户表中查询失败!");
            return null;
        }
        if (tLLClaimUserSet.size() == 0) {
            // @@错误处理
            CError.buildErr(this, "用户表中没有查到符合条件的纪录!");
            return null;
        }
        int count = 0;
        String saveOper = "";
        ExeSQL exesql = new ExeSQL();
        sqlStr = "select count(*) from llcase where Handler='" +
                 tLLClaimUserSet.get(1).getUserCode() +
                 "' and  EndCaseDate is null";
        SSRS ssrs = exesql.execSQL(sqlStr);
        if (ssrs != null && ssrs.getMaxRow() > 0) {
            String s = ssrs.GetText(1, 1);
            if (s != null && !s.equals("") && s != "0") {
                count = Integer.parseInt(s);
            }
            saveOper = tLLClaimUserSet.get(1).getUserCode();
        }
        for (int n = 1; n <= tLLClaimUserSet.size(); n++) {
            //查找未结案中工作量最少的
            sqlStr = "select count(*) from llcase where Handler='" +
                     tLLClaimUserSet.get(n).getUserCode() +
                     "' and  EndCaseDate is null";
            ssrs = exesql.execSQL(sqlStr);
            if (ssrs == null || ssrs.getMaxRow() <= 0) {
                saveOper = tLLClaimUserSet.get(n).getUserCode();
                break;
            } else {
                String s = ssrs.GetText(1, 1);
                if (s == null || s.equals("") || s == "0") {
                    saveOper = tLLClaimUserSet.get(n).getUserCode();
                    break;
                } else {
                    int tcount = Integer.parseInt(s);
                    if (count > tcount) {
                        count = tcount;
                        saveOper = tLLClaimUserSet.get(n).getUserCode();
                    }
                }
            }
        }
        return saveOper;
    }

    /**
     * 将Schema中的MakeDate,MakeTime等默认信息填充
     * @param o Object
     */
    public void fillDefaultField(Object o, String[] fieldvalue) {
        Class[] c = new Class[1];
        Method m = null;
        String[] mngcom = {fieldvalue[0]};
        String[] operator = {fieldvalue[1]};
        String[] odate = {fieldvalue[2]};
        String[] otime = {fieldvalue[3]};
        String[] adate = {fieldvalue[4]};
        String[] atime = {fieldvalue[5]};
        String[] caseno = {fieldvalue[6]};
        String[] rgtno = {fieldvalue[7]};
        try {
            c[0] = Class.forName("java.lang.String");
        } catch (Exception ex) {
        }
        try {
            m = o.getClass().getMethod("setMakeDate", c);
        } catch (Exception ex) {
        }
        try {
            m.invoke(o, odate);
        } catch (Exception ex) {
        }
        try {
            m = o.getClass().getMethod("setModifyDate", c);
        } catch (Exception ex) {
        }
        try {
            m.invoke(o, adate);
        } catch (Exception ex) {
        }
        try {
            m = o.getClass().getMethod("setMakeTime", c);
        } catch (Exception ex) {
        }
        try {
            m.invoke(o, otime);
        } catch (Exception ex) {
        }
        try {
            m = o.getClass().getMethod("setModifyTime", c);
        } catch (Exception ex) {
        }
        try {
            m.invoke(o, atime);
        } catch (Exception ex) {
        }
        try {
            m = o.getClass().getMethod("setOperator", c);
        } catch (Exception ex) {
        }
        try {
            m.invoke(o, operator);
        } catch (Exception ex) {
        }
        try {
            m = o.getClass().getMethod("setMngCom", c);
        } catch (Exception ex) {
        }
        try {
            m.invoke(o, mngcom);
        } catch (Exception ex) {
        }
        try {
            m = o.getClass().getMethod("setCaseNo", c);
        } catch (Exception ex) {
        }
        try {
            m.invoke(o, caseno);
        } catch (Exception ex) {
        }
        try {
            m = o.getClass().getMethod("setRgtNo", c);
        } catch (Exception ex) {
        }
        try {
            m.invoke(o, rgtno);
        } catch (Exception ex) {
        }
    }

    /**
     * 按照getdutycode对LCGetSet排序
     * @param aLCGetSet LCGetSet
     * @return LCGetSet
     */
    public static LCGetSet orderLCGetSet(LCGetSet aLCGetSet) {
        LCGetSchema tLCGetSchema = new LCGetSchema();

        for (int i = 1; i < aLCGetSet.size(); i++) {
            for (int j = i + 1; j <= aLCGetSet.size(); j++) {
                if (aLCGetSet.get(j).getGetDutyCode().compareTo(aLCGetSet.get(i).
                        getGetDutyCode()) < 0) {
                    tLCGetSchema = aLCGetSet.get(j);
                    aLCGetSet.set(j, aLCGetSet.get(i));
                    aLCGetSet.set(i, tLCGetSchema);
                }
            }
        }
        return aLCGetSet;
    }

    /**
     * 按照polno查询是否有理赔案件正在处理
     * @param aPolNo String
     * @return boolean
     */
    public static boolean checkClaimState(String aPolNo) {
        boolean tClaimFlag = true;
        String tSQL = "SELECT 1 FROM llcasepolicy a,llcase b WHERE a.caseno=b.caseno AND b.rgtstate NOT IN ('11','12','14')"
                      + " AND a.polno='" + aPolNo + "'";
        ExeSQL exesql = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = exesql.execSQL(tSQL);
        if (tSSRS.getMaxRow() > 0) {
            tClaimFlag = false;
        }
        return tClaimFlag;
    }


    public String getRegisterState(String aRgtNo) {
        String rgtstate = "";
        return rgtstate;
    }

    /**
     * 用客户号校验个案是否能受理
     * @param aCustomerNo String
     * @return boolean
     */
    public static String checkPerson(String aCustomerNo) {
    	String tZDError = "";//阻断错误
    	String tEdorno = "";//阻断错误
    	String tTorG = "";//若无对应的保全项目，该变量仅赋值，不使用。
    	String customerno=aCustomerNo;
//    	正在做的保全项目
    	String tIngSql = "select * from ( "
			+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
			+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
			+ " from lcinsured a,lpgrpedoritem b,lpedorapp c"
			+ " where a.grpcontno=b.grpcontno "
			+ " and a.insuredno='"+customerno+"' "
			+ " and a.grpcontno!='00000000000000000000' "
			+ " and c.edoracceptno=b.edorno"
			+ " and c.edorstate != '0' "
			+ " and b.edortype in ('WT','XT','GA','TQ','TA','SG','CT','TF','LQ','ZB') "//团单保全
			+ " union all "
			+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
			+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
			+ " from lcinsured a,lpedoritem b,lpedorapp c "
			+ " where a.contno=b.contno "
			+ " and a.insuredno='"+customerno+"' "
			+ " and a.grpcontno='00000000000000000000' "
			+ " and c.edoracceptno=b.edorno"
			+ " and c.edorstate != '0' "
			+ " and b.edortype in ('PR','BP','BA','FC','FX','WT','GF','RF','LN','BF','XT','BC','CM','TB','NS','TF','CT','WX','ZB','LQ','RB')"//个单保全
			+ " ) as temp "
			+ " group by temp.grpcontno,temp.contno,temp.edortype,temp.edorno,temp.edorname "
			+ " order by temp.grpcontno,temp.contno,temp.edortype,temp.edorno,temp.edorname ";
    	SSRS tIngArr = new ExeSQL().execSQL(tIngSql);
     
    	if(tIngArr != null && tIngArr.MaxRow>0){//存在正在进行中的保全项目
    		for(int i=1;i<=tIngArr.MaxRow;i++){
    			if(tIngArr.GetText(i, 1) != null && !"".equals(tIngArr.GetText(i, 1)) && !"00000000000000000000".equals(tIngArr.GetText(i, 1))){
    				tTorG = "团单(保单号码："+tIngArr.GetText(i, 1)+")";
    			}else{
    				tTorG = "个单(保单号码："+tIngArr.GetText(i, 2)+")";
    			}
    			tZDError +="该客户所在"+tTorG+"正在进行"+tIngArr.GetText(i, 5)+"保全操作,工单号为："+tIngArr.GetText(i, 4)+"。 \n";
    			if("".equals(tEdorno)){
    				tEdorno = tIngArr.GetText(i, 4);
    			}else{
    				tEdorno = tEdorno+"、"+tIngArr.GetText(i, 4);
    			}
    		}
    	}
//      该被保人所在分单的保全项目
    	String tIngSql1 = " select a.grpcontno,a.contno,b.edortype,b.edorno, "
			+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
			+ " from lcinsured a,lpgrpedoritem b,lpedorapp c"
			+ " where a.grpcontno=b.grpcontno "
			+ " and a.insuredno='"+customerno+"' "
			+ " and a.grpcontno!='00000000000000000000' "
			+ " and c.edoracceptno=b.edorno"
			+ " and c.edorstate != '0' "
			+ " and b.edortype in ('ZT','BC','CM','WD','JM')"
			+ " group by a.grpcontno,a.contno,b.edortype,b.edorno "
			+ " order by a.grpcontno,a.contno,b.edortype,b.edorno ";
    	SSRS tIngArr1 = new ExeSQL().execSQL(tIngSql1);
    	if(tIngArr1 != null && tIngArr1.MaxRow>0){
    		for(int i=1;i<=tIngArr1.MaxRow;i++){
    			tTorG = "团单(保单号码："+tIngArr1.GetText(i, 1)+")";
    			String tempSql = " select 1 from lpinsured a "
   				 + " where a.grpcontno='"+tIngArr1.GetText(i, 1)+"' "
   				 + " and a.contno = '"+tIngArr1.GetText(i, 2)+"' "
   				 + " and a.edortype = '"+tIngArr1.GetText(i, 3)+"' "
   				 + " and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate<>'0') "
   				 + " union all "
   				 + " select 1 from lpbnf a "
   				 + " where 1=1 "
   				 + " and a.contno = '"+tIngArr1.GetText(i, 2)+"' "
   				 + " and a.edortype = '"+tIngArr1.GetText(i, 3)+"' "
   				 + " and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate<>'0') "
   				 + " with ur ";
	   			String tempArr = new ExeSQL().getOneValue(tempSql);
	   			if("1".equals(tempArr)){
	   				tZDError +="该客户正在"+tTorG+"中进行"+tIngArr1.GetText(i, 5)+"保全操作,工单号为："+tIngArr1.GetText(i, 4)+"。 \n";
	   				if("".equals(tEdorno)){
	    				tEdorno = tIngArr1.GetText(i, 4);
	    			}else{
	    				tEdorno = tEdorno+"、"+tIngArr1.GetText(i, 4);
	    			}
	   			}
    		}
    	}
			
//    	已完成的保全项目
    	String tDoneSql = " select a.grpcontno,a.contno,b.edortype,b.edorno, "
    		+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
    		+ " from lcinsured a,lpgrpedoritem b,lpedorapp c"
    		+ " where a.grpcontno=b.grpcontno "
    		+ " and a.insuredno='"+customerno+"' "
    		+ " and a.grpcontno!='00000000000000000000' "
    		+ " and c.edoracceptno=b.edorno"
    		+ " and c.edorstate = '0' "
    		+ " and b.edortype in ('WT','XT','CT') "
    		+ " group by a.grpcontno,a.contno,b.edortype,b.edorno "
    		+ " union all "
    		+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
    		+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
    		+ " from lcinsured a,lpedoritem b,lpedorapp c "
    		+ " where a.contno=b.contno "
    		+ " and a.insuredno='"+customerno+"' "
    		+ " and a.grpcontno='00000000000000000000' "
    		+ " and c.edoracceptno=b.edorno"
    		+ " and c.edorstate = '0' "
    		+ " and b.edortype in ('FC','WT','XT','CT') "
    		+ " group by a.grpcontno,a.contno,b.edortype,b.edorno "
    		+ " union all "
    		+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
    		+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
    		+ " from lbinsured a,lpgrpedoritem b,lpedorapp c"
    		+ " where a.grpcontno=b.grpcontno "
    		+ " and a.insuredno='"+customerno+"' "
    		+ " and a.grpcontno!='00000000000000000000' "
    		+ " and c.edoracceptno=b.edorno"
    		+ " and c.edorstate = '0' "
    		+ " and b.edortype in ('WT','XT','CT') "
    		+ " group by a.grpcontno,a.contno,b.edortype,b.edorno "
    		+ " union all "
    		+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
    		+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
    		+ " from lbinsured a,lpedoritem b,lpedorapp c "
    		+ " where a.contno=b.contno "
    		+ " and a.insuredno='"+customerno+"' "
    		+ " and a.grpcontno='00000000000000000000' "
    		+ " and c.edoracceptno=b.edorno"
    		+ " and c.edorstate = '0' "
    		+ " and b.edortype in ('FC','WT','XT','CT') "
    		+ " group by a.grpcontno,a.contno,b.edortype,b.edorno ";
//		20121105 因需求变更，已做完的保全项目仅提示不阻断
    	/*SSRS tDoneArr = new ExeSQL().execSQL(tDoneSql);
    	if(tDoneArr != null && tDoneArr.MaxRow>0){//存在已完成的保全项目
    		for(int i=1;i<=tDoneArr.MaxRow;i++){
    			if(tDoneArr.GetText(i, 1) != null && !"".equals(tDoneArr.GetText(i, 1)) && !"00000000000000000000".equals(tDoneArr.GetText(i, 1))){
    				tTorG = "团单(保单号码："+tDoneArr.GetText(i, 1)+")";
    			}else{
    				tTorG = "个单(保单号码："+tDoneArr.GetText(i, 2)+")";
    			}
    			tZDError +="该客户已在"+tTorG+"中做过"+tDoneArr.GetText(i, 5)+"保全操作,工单号为："+tDoneArr.GetText(i, 4)+"。 \n";
    			if("".equals(tEdorno)){
    				tEdorno = tDoneArr.GetText(i, 4);
    			}else{
    				tEdorno = tEdorno+"、"+tDoneArr.GetText(i, 4);
    			}
    		}
    	}*/
    	//没有保全号的保全项目(各种满期给付各种特殊处理)。。。。//0 给付完成；1 正在给付。
    	//常无忧B满期给付
    	String tCWMJSql = " select temp.ContNo,temp.edorstate "
    			   + " from "
    			   + " ( "
    			   + " select a.contno," 
    			   + " (case when exists (select 1 from ljaget where actugetno=a.getnoticeno) then '0' else '1' end) edorstate "
    			   + " from ljsgetdraw a where insuredno='"+customerno+"' and feefinatype='TF' and riskcode='330501' "
    			   + " ) as temp"
    			   + " group by temp.ContNo,temp.edorstate ";
    	SSRS tCWMJArr = new ExeSQL().execSQL(tCWMJSql);
    	if(tCWMJArr != null && tCWMJArr.MaxRow>0){
    		for(int i=1;i<=tCWMJArr.MaxRow;i++){
    			tTorG = "个单(保单号码："+tCWMJArr.GetText(i, 1)+")";
    			if("1".equals(tCWMJArr.GetText(i, 2))){
    				tZDError +="该客户所在"+tTorG+"正在进行常无忧B满期给付保全操作。 \n";
    				if("".equals(tEdorno)){
        				tEdorno = tCWMJArr.GetText(i, 1);
        			}else{
        				tEdorno = tEdorno+"、"+tCWMJArr.GetText(i, 1);
        			}
    			}
    		}
    	}
    	//个单满期给付
    	String tGDMJSql = " select temp.ContNo,temp.edorstate "
    			   + " from "
    			   + " ( "
    			   + " select a.contno," 
    			   + " (case when exists (select 1 from ljaget where actugetno=a.getnoticeno) then '0' else '1' end) edorstate "
    			   + " from ljsgetdraw a where insuredno='"+customerno+"' and feefinatype='TF' and riskcode not in ('170206','330501') "
    			   + " ) as temp"
    			   + " group by temp.ContNo,temp.edorstate ";
    	SSRS tGDMJArr = new ExeSQL().execSQL(tGDMJSql);
    	if(tGDMJArr != null && tGDMJArr.MaxRow>0){
    		for(int i=1;i<=tGDMJArr.MaxRow;i++){
    			tTorG = "个单(保单号码："+tGDMJArr.GetText(i, 1)+")";
    			if("0".equals(tGDMJArr.GetText(i, 2))){
    				tZDError +="该客户已在"+tTorG+"中做过个单满期给付保全操作。 \n";
    				//因做过的保全项目不再处理，因此不对tEdorno赋值
    			}else{
    				tZDError +="该客户所在"+tTorG+"正在进行个单满期给付保全操作。 \n";
    				if("".equals(tEdorno)){
        				tEdorno = tGDMJArr.GetText(i, 1);
        			}else{
        				tEdorno = tEdorno+"、"+tGDMJArr.GetText(i, 1);
        			}
    			}
    		}
    	}   
    	String tMJSql = " select temp.GrpContNo,temp.edorName,temp.edorstate "
	           + " from "
	           + " ( "
	           + " select a.grpcontno GrpContNo,'团单满期给付' edorName," //团单满期给付
			   + " (case when exists (select 1 from ljaget where actugetno=a.getnoticeno) then '0' else '1' end) edorstate"
			   + " from ljsgetdraw a,lcinsured b "
			   + " where a.grpcontno = b.grpcontno and b.grpcontno != '00000000000000000000' "
			   + " and a.feefinatype='TF' and a.riskcode='170206' "
			   + " and b.insuredno='"+customerno+"' "
//			   + " union all "
//			   + " select a.contno GrpContNo,'特需险满期给付' edorName,'1' edorstate" //特需险满期给付
//			   + " from lgwork a,lcinsured b " 
//			   + " where a.contno = b.grpcontno and b.grpcontno != '00000000000000000000' "
//			   + " and a.typeno='070015' "
//			   + " and b.insuredno='"+customerno+"' "
//			   + " and a.statusno != '5' "
//			   + " union all "
//			   + " select a.contno GrpContNo,'特需险满期给付' edorName,'0' edorstate" //特需险满期给付
//			   + " from lgwork a,lcinsured b " 
//			   + " where a.contno = b.grpcontno and b.grpcontno != '00000000000000000000' "
//			   + " and a.typeno='070015' "
//			   + " and b.insuredno='"+customerno+"' "
//			   + " and a.statusno = '5' "
			   + " ) as temp "
			   + " group by temp.GrpContNo,temp.edorName,temp.edorstate "
			   + " with ur ";  
    	SSRS tMJArr = new ExeSQL().execSQL(tMJSql);
    	if(tMJArr != null && tMJArr.MaxRow>0){
    		for(int i=1;i<=tMJArr.MaxRow;i++){
    			tTorG = "团单(保单号码："+tMJArr.GetText(i, 1)+")";
    			if("0".equals(tMJArr.GetText(i, 3))){
    				tZDError +="该客户已在"+tTorG+"中做过"+tMJArr.GetText(i, 2)+"保全操作。 \n";
    			}else{
    				tZDError +="该客户所在"+tTorG+"正在进行"+tMJArr.GetText(i, 2)+"保全操作。 \n";
    				if("".equals(tEdorno)){
        				tEdorno = tMJArr.GetText(i, 1);
        			}else{
        				tEdorno = tEdorno+"、"+tMJArr.GetText(i, 1);
        			}
    			}
    		}
    	}
//    	所在团单正在定期结算
    	String tDJSql = " select distinct a.contno "
    			   + " from lgwork a,lcinsured b "
    			   + " where a.contno = b.grpcontno and b.grpcontno != '00000000000000000000' "
    			   + " and b.grpcontno not in (select code from ldcode where codetype='lp_dqjs_pass') "
    			   + " and a.typeno like '06%' and a.statusno not in ('5','8') "
    			   + " and b.insuredno='"+customerno+"' ";
    	SSRS tDJArr = new ExeSQL().execSQL(tDJSql);
    	if(tDJArr != null && tDJArr.MaxRow >0){
    		for(int i=1;i<=tDJArr.MaxRow;i++){
    			tTorG = "团单(保单号码："+tDJArr.GetText(i, 1)+")";
    			tZDError +="该客户所在"+tTorG+"正在进行定期结算保全操作。 \n";
    			if("".equals(tEdorno)){
    				tEdorno = tDJArr.GetText(i, 1);
    			}else{
    				tEdorno = tEdorno+"、"+tDJArr.GetText(i, 1);
    			}
    		}
    	}
//    	if(tZDError != ""){//提示不阻断处理
//    		return tZDError;
//    	}
//    	若存在多个保单，则仅提示不阻断
    	String tCSQL = "select count(1) from lcinsured where insuredno = '"+customerno+"' "
		  + " and exists (select 1 from lccont where prtno = lcinsured.prtno and appflag = '1') "
		  + "having count(1) >1 ";
    	String tCArr = new ExeSQL().getOneValue(tCSQL);
    	if(tCArr != null && !"".equals(tCArr) && Integer.parseInt(tCArr)>0){
    		tEdorno = "";
    	}
    	return tEdorno;
    }

    /**
     * 用团单号校验批次是否能够受理
     * @param aGrpContNo String
     * @return boolean
     */
    public static String checkGrp(String aGrpContNo) {
        //校验是否有团体保全项目，除去保单遗失补发、新增被保险人、无名单实名化、无名单增加被保人

		String tSQL = "SELECT b.edorno FROM lpedorapp a , lpgrpedoritem b "
        +"WHERE a.edoracceptno= b.edoracceptno AND a.edorstate !='0' "
              + "AND grpcontno ='" + aGrpContNo + "' "
              + "AND b.edortype NOT IN ('LR','NI','WS','WZ','FP','RR','RS','AC','WJ','LP','YS','ZT','BC','CM','WD','JM')";
		//req00000111 批次受理 减人不控制
				ExeSQL tExeSQL = new ExeSQL();
				SSRS tSSRS = tExeSQL.execSQL(tSQL);
					System.out.println(tSQL);
					boolean tPassFlag = false;
					boolean mPassFlag = true;
					String edornoa="";
					if (tSSRS.getMaxRow() > 0) {
						for(int i=1;i<=tSSRS.getMaxRow();i++){
							String sSQL="select edortype from lpgrpedoritem a where edorno='" + tSSRS.GetText(i, 1) + "'  AND "
									+ "exists (select 1 from lpedorapp where edoracceptno =a.edorno and edorstate!='0' ) ";
							String tSSRS2 = tExeSQL.getOneValue(sSQL);
							System.out.println(tSSRS2);
								if("DJ".equals(tSSRS2)){
									edornoa=tSSRS.GetText(i, 1);
									String rSQL="select 1 from ldcode where codetype='lp_dqjs_pass' and code= '" + aGrpContNo + "'";
									String tSSRS3 = tExeSQL.getOneValue(rSQL);
									System.out.println("ccc"+ tSSRS3.equals("1"));
									if("1".equals(tSSRS3)){
										System.out.println("bb");
										tPassFlag = true;
//										return "";
									}
								}else{
									mPassFlag=false;
								}
						}
						if((tPassFlag)&&(mPassFlag)){
							return "";
						}
    return linkStringR(tSSRS).replace(edornoa, "");
}

return "";

    }

    private String linkString(SSRS tSSRS) {
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            sb.append("'");
            sb.append(tSSRS.GetText(i, 1));
            sb.append("'");
            if (i != tSSRS.getMaxRow()) {
                sb.append(",");
            }
        }
        System.out.println(sb.toString());
        return sb.toString();
    }

    private boolean isSingle(String customerNo) {
        String sql_0 = "select 1 from lcpol where grpcontno='00000000000000000000' and appflag='1' and conttype='1' and insuredno='"
                       + customerNo + "' " +
                       "union select 1 from lbpol where grpcontno='00000000000000000000' and appflag='1' and conttype='1' " +
                       "and insuredno='" + customerNo + "' with ur";
        System.out.println(sql_0);
        ExeSQL tExeSQL = new ExeSQL();
        String result_1 = tExeSQL.getOneValue(sql_0);
        if (result_1 != null && !"".equals(result_1)) {
            return true;
        } else {
            return false;
        }
    }

    public static String linkStringR(SSRS tSSRS) {
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            sb.append(tSSRS.GetText(i, 1));
            if (i != tSSRS.getMaxRow()) {
                sb.append("、");
            }
        }
        String res = sb.toString();
        System.out.println(sb.toString());
        return res;
    }

    /**
     * 校验该责任是否已经满期给付
     * @param aPolNo String
     * @param aGetDutyCode String
     * @return boolean
     */
    public static boolean checkMJ(String aPolNo, String aGetDutyCode) {
        String tMJSQL = "select coalesce(sum(b.sumgetmoney),0) "
                        + " From ljsgetdraw a,ljaget b "
                        + " where  a.getnoticeno = b.actugetno "
                        + " and a.getdutycode ='" + aGetDutyCode
                        + "' and a.polno ='" + aPolNo + "'";
        ExeSQL tExeSQL = new ExeSQL();
        double tMoney = Double.parseDouble(tExeSQL.getOneValue(tMJSQL));
        if (tMoney > 0) {
            return false;
        }
        return true;
    }

    /**
     * 判断是否为万能主副险
     * @param aRiskCode String
     * @return boolean
     */
    public static boolean chenkWN(String aRiskCode) {
        LDCode1DB tLDCode1DB = new LDCode1DB();
        String tRiskSQL =
                "select * from ldcode1 where codetype='checkappendrisk' "
                + " and code='" + aRiskCode + "'";
        LDCode1Set tLDCode1Set = tLDCode1DB.executeQuery(tRiskSQL);
        if (tLDCode1Set.size() > 0) {
            return checkWNRisk(tLDCode1Set.get(1).getCode1());
        } else {
            return checkWNRisk(aRiskCode);
        }
    }

    /**
     * 判断是否为万能险种
     * @param aRiskCode String
     * @return boolean
     */
    private static boolean checkWNRisk(String aRiskCode) {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(aRiskCode);
        tLMRiskAppDB.getInfo();
        if ("4".equals(tLMRiskAppDB.getRiskType4())) {
            return true;
        }
        return false;
    }

    /**
     * 判断aUpUser是否为aUser有效上级 aUser为可能失效的用户,支持社保用户上下级判断
     * @param aUpUser String
     * @param aUser String
     * @return boolean
     */
    public static boolean checkUPUpUser(String aUpUser, String aUser) {
        String tStateFlag = "";
        String tUpUser = "";
        while (true) {
            LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
            tLLClaimUserDB.setUserCode(aUser);
            LLSocialClaimUserDB tSocialClaimUserDB = new LLSocialClaimUserDB();
            tSocialClaimUserDB.setUserCode(aUser);
            if (!tLLClaimUserDB.getInfo() && !tSocialClaimUserDB.getInfo()) {
                return false;
            }
            if(tLLClaimUserDB.getInfo()){
            	tStateFlag = tLLClaimUserDB.getStateFlag();
                if ("1".equals(tStateFlag)) {
                    break;
                }
                tUpUser = tLLClaimUserDB.getUpUserCode();
                aUser = tUpUser;
            }else {
            	tStateFlag = tSocialClaimUserDB.getStateFlag();
                if ("1".equals(tStateFlag)) {
                    break;
                }
                tUpUser = tSocialClaimUserDB.getUpUserCode();
                aUser = tUpUser;
            }
            
        }
        if (tUpUser.equals(aUpUser)) {
            return true;
        }
        return false;
    }

    /**
     * 校验出险日期是否在保单有效期内
     * @param aPolNo String
     * @param aAccDate String
     * @return boolean
     */
    public boolean checkPolValid(String aPolNo, String aAccDate) {
        String tEndDate = CommonBL.getPolInvalidate(aPolNo);
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setPolNo(aPolNo);
        if (!tLCPolBL.getInfo()) {
            CError.buildErr(this, "险种保单信息查询错误!");
            return false;
        }

        if (tEndDate.equals(null) || tEndDate.equals("") ||
            tEndDate.equals("null")) {
            CError.buildErr(this, aPolNo + "险种保单失效日查询失败!");
            return false;
        }

        if (tEndDate.equals(null) || tEndDate.equals("") ||
            tEndDate.equals("null")) {
            CError.buildErr(this, aPolNo + "险种保单失效日查询失败!");
            return false;
        }

        int validays = PubFun.calInterval(tLCPolBL.getCValiDate(), aAccDate,
                                          "D");
        int remdays = PubFun.calInterval(aAccDate, tEndDate, "D");
        
//        String SQL0 = "select riskcode from lmriskapp where risktype3='7' and risktype <>'M' and riskcode ='"
//                      + tLCPolBL.getRiskCode() + "'";
//        ExeSQL tExeSQL = new ExeSQL();
//        String riskcode = tExeSQL.getOneValue(SQL0);
        //cbs00006365 取消特需医疗险种关于事件时间的校验
        //#2258 增加特需险出险事件时间是否在保险有效期间内的校验功能
        //#2188 关于2012年内蒙古自治区本级城镇职工大额补充医疗保险项目理赔事项的说明,配置过的保单，不校验出险日期
        String tCheckSql = "select 1 from ldcode where codetype='llcheckgrpcont' and codename='"
            + tLCPolBL.getGrpContNo() + "'";
        ExeSQL tCheckExeSql = new ExeSQL();
        String tFlag = tCheckExeSql.getOneValue(tCheckSql);
        // #2739 <补充A> **start**
        String aCheckSQL ="select riskcode from lcpol where polno='"+aPolNo+"' union select riskcode from lbpol where polno='"+aPolNo+"' with ur";
        ExeSQL aCheckExe = new ExeSQL();
        String aRiskCode =aCheckExe.getOneValue(aCheckSQL);
        // #2739 <补充A> **end** 
        
        if("170501".equals(aRiskCode)){//# 2739 <补充A>
          	
        	return true;
          }else if("".equals(StrTool.cTrim(tFlag))){//未配置的保单，校验出险日期  
            if ((validays < 0 || remdays <= 0) 
//            		&& StrTool.cTrim(riskcode).equals("")
            	) {
    			String errmsg = "出险日期" + aAccDate
    					+ "不在保单" + tLCPolBL.getContNo() + "有效期内,不能理赔";
                CError.buildErr(this, errmsg);
                return false;
            }
        }
        
        return true;
    }

    /**
     * 判断是否为万能险种
     * @param aPolNo String
     * @return boolean
     */
    public static boolean chechWNPol(String aPolNo) {
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setPolNo(aPolNo);
        if (!tLCPolBL.getInfo()) {
            return false;
        }
        return chenkWN(tLCPolBL.getRiskCode());
    }

    /**
     * 校验用户是否有对该案件进行合同处理的权限
     * @param aCaseNo String
     * @param aUserCode String
     * @return boolean
     */
    public static boolean checkContDealRight(String aCaseNo, String aUserCode) {
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(aCaseNo);
        if (tLLCaseDB.getInfo()) {
            if (aUserCode.equals(tLLCaseDB.getHandler())) {
                String tSQL = "select 1 From llclaimuser a,llclaimpopedom b "
                              + " where a.claimpopedom = b.claimpopedom "
                              +
                        " and b.getdutykind='4' and b.getdutytype='0' and b.limitmoney=1 "
                              + " and usercode='" + aUserCode + "'";
                ExeSQL tExeSQL = new ExeSQL();
                SSRS tSSRS = tExeSQL.execSQL(tSQL);
                if (tSSRS.getMaxRow() > 0) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 输入生日，计算年龄
     * @param aCustomerBirhday String
     * @return int
     */
    public static int calAge(String aCustomerBirhday) throws Exception {
        FDate fDate = new FDate();
        Date Birthday = fDate.getDate(aCustomerBirhday);
        String strNow = PubFun.getCurrentDate();
        Date aNow = fDate.getDate(strNow);
        int CustomerAge = PubFun.calInterval(Birthday, aNow, "Y");
        System.out.println("年龄计算函数。。。" + CustomerAge);
        return CustomerAge;
    }

    /**
     * 返回赔付结论
     * @param agivetype String
     * @return String
     * @throws Exception
     */
    public static String getGiveDesc(String agivetype) throws Exception {
        String agivedesc = "";
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("llclaimdecision");
        tLDCodeDB.setCode(agivetype);
        if (tLDCodeDB.getInfo()) {
            agivedesc = tLDCodeDB.getCodeName();
        }
        return agivedesc;
    }

    /**
     * 校验案件如果是医保通案件，是否可以进行操作
     * @param aCaseNo String
     * @return boolean
     */
    public static boolean checkHospCaseState(String aCaseNo) {
        LLHospCaseDB tLLHospCaseDB = new LLHospCaseDB();
        tLLHospCaseDB.setCaseNo(aCaseNo);
        if (tLLHospCaseDB.getInfo()) {
            if ("1".equals(tLLHospCaseDB.getDealType())) {
                if ("0".equals(tLLHospCaseDB.getConfirmState())) {
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * 锁定预付保单
     * @return
     */
    public static boolean lockFY(String aGrpContNo,GlobalInput aGlobalInput) {
    	// 预付赔款：“YF”
        String tLockNoType = "YF";

        //锁定有效时间
        String tAIS = "30";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", aGrpContNo);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(aGlobalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        
    	return tLockTableActionBL.submitData(tVData, null);    	
    }
    
    /**
     * 进行案件处理人分配
     * @param aGlobalInput
     * @param aCustomerNo
     * @param aSimpleCase
     * @return
     */
    public String dealHandler(GlobalInput aGlobalInput, String aCustomerNo,
			String aSimpleCase) throws Exception {
		System.out.println("开始进行案件处理人分配，管理机构：" + aGlobalInput.ManageCom
				+ ",登录人：" + aGlobalInput.Operator + ",客户号：" + aCustomerNo
				+ ",分配类型：" + aSimpleCase);
		String tHandler = "";
		String tSimpleCase = "";

		// 案件分配类型转换 06-机构处理 10-医保通 
		if (aSimpleCase.equals("06")) {
			tSimpleCase = "01";
		} else if ("07".equals(aSimpleCase) || "09".equals(aSimpleCase)
				|| "10".equals(aSimpleCase)) {
			tSimpleCase = "03";
		} else {
			tSimpleCase = "02";
		}

		if ("03".equals(tSimpleCase)) {
			tHandler = aGlobalInput.Operator;
		} else {
			tHandler = ChooseAssessor(aGlobalInput.ManageCom, aCustomerNo,
					tSimpleCase);
		}
		System.out.println("分配的理赔用户："+tHandler);
		return tHandler;
	}
    
    /**
     * 进行案件录入人分配
     * @param aGlobalInput 
     * @param aSimpleCase
     * @return
     * @throws Exception
     */
    public String dealInputer(GlobalInput aGlobalInput, String aSimpleCase)
			throws Exception {
    	System.out.println("开始进行案件录入人分配，管理机构：" + aGlobalInput.ManageCom
				+ ",登录人：" + aGlobalInput.Operator + ",分配类型：" + aSimpleCase);
    	
		String tInputer = "";

		String tSimpleCase = "";

		// 案件分配类型转换 06-机构处理 10-医保通
		if (aSimpleCase.equals("06")) {
			tSimpleCase = "01";
		} else if ("07".equals(aSimpleCase) || "09".equals(aSimpleCase)
				|| "10".equals(aSimpleCase)) {
			tSimpleCase = "03";
		} else {
			tSimpleCase = "02";
		}

		if ("03".equals(tSimpleCase)) {
			tInputer = aGlobalInput.Operator;
		} else if ("01".equals(tSimpleCase)) {
			tInputer = "";
		} else {
			tInputer = chooseInputer(aGlobalInput.Operator, tSimpleCase);
		}
		
		return tInputer;
	}
    
    /**
     * 通过传入的团单/案件号，判断该团单是否为“社保业务保单”
     * @param aNo 团单/案件号
     * @param aType grpno 传入的是团单号 rgtno 传入批次号 null 传入案件号
     * @return flase：传入的团单/案件号为空；0：非社保保单；1：社保保单
     */
    public static String checkSocialSecurity(String aNo,String aType){
    	if("".equals(aNo) || aNo == null){
    		return "false";
    	}
    	if("grpno".equals(aType)){
    		String tSql = "select CHECKGRPCONT('"+aNo+"') from dual with ur";
			ExeSQL tExeSQL = new ExeSQL();
			String tSSMark = tExeSQL.getOneValue(tSql);
			if("Y".equals(tSSMark)){
				return "1";
			}else {
				return "0";
			}  	
    	}else if("rgtno".equals(aType)){
    		String tSql = "select CHECKGRPCONT(rgtobjno) from llregister where rgtno ='"+aNo+"' with ur";
			ExeSQL tExeSQL = new ExeSQL();
			String tSSMark = tExeSQL.getOneValue(tSql);
			if("Y".equals(tSSMark)){
				return "1";
			}else {
				return "0";
			}  	
    	}else{
    		String tSql = "select CHECKGRPCONT(rgtobjno) from llregister where rgtno = (select rgtno from llcase where caseno='"+aNo+"') with ur"; 
    		ExeSQL tExeSQL = new ExeSQL();
			String tSSMark = tExeSQL.getOneValue(tSql);
			if("Y".equals(tSSMark)){
				return "1";
			}else {
				return "0";
			}  	
    	} 	
    }
    
    /**
     * 通过案件号和案件当前状态判断其完成的调查（按调查号排序）的提调状态
     * @param caseNo
     * @param rgtState
     * @return
     */
    public static String checkSurveyRgtState(String caseNo, String rgtState, String operate) {
    	String tMsg = "";
    	//案件状态为查讫，判断当前处理人
    	if("08".equals(rgtState)){
    		String tSql = "select * from llsurvey a where a.otherno='"+caseNo
    		+"' and a.surveyflag='3' and a.rgtstate <> '08' order by surveyno desc fetch first 1 rows only with ur";
    	
	    	LLSurveyDB tLLSurveyDB = new LLSurveyDB();
	        LLSurveySet tLLSurveySet = new LLSurveySet();
	        tLLSurveySet = tLLSurveyDB.executeQuery(tSql);
	        System.out.println("tSql:"+tSql);
	        if (tLLSurveySet.size() <= 0) {
	        	tMsg = "调查信息查询失败";
	            return tMsg;
	        }
	        LLSurveySchema tLLSurveySchema = new LLSurveySchema();
	        tLLSurveySchema = tLLSurveySet.get(1);
	        String tRgtState = "" + tLLSurveySchema.getRgtState();
	        String tRgtName = "";
	        String tBakErr = "";
	        LDCodeDB tLDCodeDB = new LDCodeDB();
	        tLDCodeDB.setCodeType("llrgtstate");
	        tLDCodeDB.setCode(tRgtState);
	        if (tLDCodeDB.getInfo()) {
	        	tRgtName = tLDCodeDB.getCodeName();
	        }
	         
	        System.out.println("tRgtState，tRgtName:" + tRgtState+tRgtName);
	        
	        //案件提调前状态对应返回的错误信息
	        if("01".equals(tRgtState) || "0".equals(tRgtState)//历史调查，0为受理状态    	 
	        		|| "".equals(tRgtState)){//核心目前只有3条数据，此分支只是为了体现数据情况，以后系统不会出现该类数据
	        	tRgtState = "01";
	        	tBakErr = "案件提调状态为‘受理状态’，请您去‘账单录入’或‘检录’页面进行操作";	        	
	        }else if("02".equals(tRgtState)){
	        	tBakErr = "案件提调状态为‘扫描状态’，请您去‘账单录入’或‘检录’页面进行操作";
	        }else if("03".equals(tRgtState) || "1".equals(tRgtState)){//历史调查，1为检录状态，咨询、通知类案件均为检录状态
	        	tRgtState = "03";
	        	tBakErr = "案件提调状态为‘检录状态’，请您去‘理算’或‘团体审定’页面进行操作";
	        }else if("04".equals(tRgtState) || "05".equals(tRgtState)
	        		|| "06".equals(tRgtState) || "10".equals(tRgtState)){
	        	tBakErr = "案件提调状态为‘"+tRgtName+"’，请您去‘审批审定’页面进行操作";
	        }
	        
	        if (operate.equals(tRgtState)) {
	        	tMsg = ""; //案件提调状态与当前操作状态一致
	        }else{
	        	tMsg = tBakErr; //案件提调状态与当前操作状态不一致
	        }
    	}
    	    	
    	return tMsg;
		
	}
    /**
     * 判断是否为分红险种
     * @param aRiskCode String
     * @return boolean
     */
    public static boolean checkFHRisk(String aRiskCode) {
    	LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(aRiskCode);
        if (!tLMRiskAppDB.getInfo())
        {
            return false;
        }
        String riskType4 = tLMRiskAppDB.getRiskType4();
        if ((riskType4 != null) && (riskType4.equals(BQ.RISKTYPE4_BONUS)))
        {
            return true;
        }
        return false;
    }
    /**
     * 不许“06-案件上载”状态的批次进行操作
     * @param aRgtNo String
     * @return boolean
     */
    public static boolean checkGrpRgtState(String aRgtNo) {
		String tSql = "select rgtstate from llregister where rgtno ='"+aRgtNo+"' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		String tSSMark = tExeSQL.getOneValue(tSql);
		if("06".equals(tSSMark)){
			return false;
		}else {
			return true;
		} 
    }
    /**
     * 校验录入日期不能大于当前日期，且日期第一位为"2"
     * @param aRgtNo String
     * @return boolean
     */
    public static boolean llcheckdate(String date) {
    	String current = PubFun.getCurrentDate();
    	FDate tD = new FDate();
    	Date CurrentDate = tD.getDate(current);
    	Date date1 = tD.getDate(date);
    	int days = PubFun.calInterval(date1, CurrentDate, "D");
    	String a = date.substring(0, 1);
//    	System.out.println(a);
    	if(!a.equals("2")){
    		return false;
    	}
    	if(days<0){
    		return false;
    	}
    	return true;
    }
}
