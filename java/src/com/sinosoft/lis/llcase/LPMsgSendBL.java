/*
 * <p>Title: PICCH业务系统</p>
 * <p>ClassName:LLInqFeeBL </p>
 * <p>Description: 直接调查费录入 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author：Xx
 * @version：1.0
 * @CreateDate：2005-02-20
 */

package com.sinosoft.lis.llcase;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.SendMsgMail;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun1;
import java.io.IOException;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.GlobalInput;
import java.net.*;


public class LPMsgSendBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private String mOperate = "";

    //全局数据
    private GlobalInput mGlobalInput = new GlobalInput();
    //账单费用明细
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    SSRS tSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();

    private String ReturnMode = "";
    private String RgtanMobile = "";
    private String EMail="";
    String Content = "";
    public LPMsgSendBL() {
    }

    //用于调试
    public static void main(String[] args) {
        LPMsgSendBL tLPMsgSendBL = new LPMsgSendBL();
        LLInqFeeSchema tLLInqFeeSchema = new LLInqFeeSchema();

        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "cm3103";
        tGI.ManageCom = "8631";
        VData tVData = new VData();
        tVData.add(tGI);

        tVData.add(tLLInqFeeSchema);
        tLPMsgSendBL.submitData(tVData, "INSERT");
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData 输入数据
     * @param cOperate String 操作类型
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到输入数据
        if (!getInputData()) {
            return false;
        }


        Content=getMsg(mLLCaseSchema.getCaseNo(),mLLCaseSchema.getRgtNo(),mOperate);
        TransferData PrintElement = new TransferData();
        PrintElement.setNameAndValue("mobile", RgtanMobile);
        PrintElement.setNameAndValue("content", Content);
        PrintElement.setNameAndValue("strFrom", "service@picchealth.com");
        PrintElement.setNameAndValue("strTo", EMail);
        PrintElement.setNameAndValue("strTitle", "待处理案件提醒");
        PrintElement.setNameAndValue("strPassword", "");



        VData aVData = new VData();
        aVData.add(mGlobalInput);
        aVData.add(PrintElement);

        SendMsgMail tSendMsgMail = new SendMsgMail();
        if (ReturnMode.equals("4")) {
            System.out.print("send Messane");
            try {
                if (!tSendMsgMail.submitData(aVData, "Message")) {
                    System.out.println("短信发送失败,接受者：" + mLLCaseSchema.getCaseNo() +
                                       " ；手机号：" + RgtanMobile);

                }
            } catch (MalformedURLException ex) {
            } catch (IOException ex) {
            }

        }
        if (ReturnMode.equals("3")) {
             System.out.print("send EMail");
            try {
                if (!tSendMsgMail.submitData(aVData, "Email")) {
                    System.out.println("邮件发送失败,接受者：" + mLLCaseSchema.getCaseNo() +
                                       " ；邮箱：" + EMail);

                }
            } catch (MalformedURLException ex) {
            } catch (IOException ex) {
            }

        }


        return true;
    }

    /**
     * 从输入数据中得到所有数据对象
     * @return boolean
     */
    private boolean getInputData() {
        System.out.println("getInputData()..." + mOperate);

        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLLCaseSchema = (LLCaseSchema) mInputData.getObjectByObjectName(
                "LLCaseSchema", 0);

        return true;
    }

    /**
     * 对输入数据做必要的校验
     * @return boolean
     */
    private String getMsg(String CaseNo,String RgtNo,String Flag) {
        String Msg="";

        if(Flag.equals("Conf")){
            String SQL1 = "select customername,endcasedate,CaseGetMode from llcase where CaseNo='" +CaseNo + "' with ur ";
            tSSRS = tExeSQL.execSQL(SQL1);
            String CustomerName=tSSRS.GetText(1,1);
            String EndCaseDate=tSSRS.GetText(1,2);
            String PayMode=tSSRS.GetText(1,3);
            System.out.println("111111111111111:"+PayMode);
            String SQL2 = "select returnmode,rgtantmobile,email,rgtantname from llregister where rgtno='" +RgtNo + "' with ur ";
            tSSRS = tExeSQL.execSQL(SQL2);
            ReturnMode = tSSRS.GetText(1, 1);
            RgtanMobile = tSSRS.GetText(1, 2);
            EMail = tSSRS.GetText(1, 3);
            String  RgtanName=tSSRS.GetText(1,4);
            String SQL3 = "select Drawer from ljaget where otherno='" +CaseNo + "' with ur ";
            tSSRS = tExeSQL.execSQL(SQL3);
            String Drawer=tSSRS.GetText(1,1);
        if(PayMode.equals("1")||PayMode.equals("2"))
        {
            Msg="尊敬的"+RgtanName+"先生/女士,您关于"+CustomerName+"先生/女士的理赔申请已于"
                       +EndCaseDate+"受理完毕,请领取人"+Drawer+"携带有效证件到财务处领取保险金."
                       +"如有任何疑问，请拨打95518（北京）或4006695518（全国其他地区）进行咨询。";
        }else{
            Msg ="尊敬的"+RgtanName+"先生/女士,您关于"+CustomerName+"先生/女士的理赔申请已于"
                     +EndCaseDate+"受理完毕,请请于近期注意查询您的保险金接收帐户."
                     +"如有任何疑问，请拨打95518（北京）或4006695518（全国其他地区）进行咨询。";

            }
        }
        return Msg;
    }


    public VData getResult() {
        return this.mResult;
    }

}
