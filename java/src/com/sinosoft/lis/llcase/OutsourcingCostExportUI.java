package com.sinosoft.lis.llcase;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LLOutcoucingAverageSchema;
import com.sinosoft.lis.schema.LLOutsourcingTotalSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
public class OutsourcingCostExportUI {
	 /**错误信息容器*/
    public CErrors mErrors = new CErrors();

	
   public boolean submitData(VData cInputData, String cOperate) {
	   
	 
	   OutsourcingCostExportBL tOutsourcingCostExportBL=new OutsourcingCostExportBL();
	   if(!tOutsourcingCostExportBL.submitData(cInputData, cOperate)){
	         mErrors.copyAllErrors(tOutsourcingCostExportBL.mErrors);
	         return false;
	   }
       if (tOutsourcingCostExportBL.mErrors.needDealError()) {
           // @@错误处理
           this.mErrors.copyAllErrors(tOutsourcingCostExportBL.mErrors);
           CError tError = new CError();
           tError.moduleName = "OutsourcingCostExportUI";
           tError.functionName = "submitData";
           tError.errorMessage = "数据提交失败!";
           this.mErrors.addOneError(tError);
           return false;
       }
	   return true;
   }
  

}


