package com.sinosoft.lis.llcase;

import com.sinosoft.lis.bl.LLRegisterBL;
import com.sinosoft.lis.db.LLCasePolicyDB;
import com.sinosoft.lis.db.LLClaimDB;
import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.lis.db.LLRenewOpinionDB;
import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLRenewOpinionSchema;
import com.sinosoft.lis.schema.LLCasePolicySchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.vbl.LLCasePolicyBLSet;
import com.sinosoft.lis.vbl.LLRegisterBLSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LLClaimSet;
import com.sinosoft.lis.vschema.LLRegisterSet;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 立案业务逻辑保存处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Xx
 * @version 1.0
 */
public class LLContUWBL
{
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private String mOperate;

    private LLRenewOpinionSchema mLLRenewOpinionSchema = new LLRenewOpinionSchema();
    private LLRenewOpinionSchema updateLLRenewOpinionSchema = new LLRenewOpinionSchema();
    private LLRegisterBL mLLRegisterBL = new LLRegisterBL();
    private LLRegisterBLSet mLLRegisterBLSet = new LLRegisterBLSet();

    private LLCasePolicyBLSet mLLCasePolicyBLSet = new LLCasePolicyBLSet();

    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCasePolicySchema mLLCasePolicySchema = new LLCasePolicySchema();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private GlobalInput mGlobalInput = new GlobalInput();

    private GlobalInput mG = new GlobalInput();
    String strHandler = "";
    String Can_RgtNo = "";
    String mCancleReason = "";
    String RgtReason = "";
    String Can_CaseNo = "";
    String mCancleRemark = "";
    // String RgtObj="";
    // String RgtObjNo = "";

    public LLContUWBL()
    {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        if (!getInputData(cInputData))
            return false;
        if (cOperate.equals("INSERT") || cOperate.equals("UPDATE"))
        {
            System.out.println("abcdefg");
            if (!dealData())
                return false;
            System.out.println("---dealData---");

            PubSubmit ps = new PubSubmit();
            ps.submitData(this.mResult, "");
            if (!ps.submitData(this.mResult, ""))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(ps.mErrors);
                return false;
            }

        }
        if (cOperate.equals("UPDATE"))
        {
            if (!updateData())
                return false;
        }
        if (cOperate.equals("ReturnData"))
        {
            if (!ReturnData())
                return false;
        }
        if (cOperate.equals("INSERT||RENEWS"))
        {
            System.out.println("INSERT");
            if (!insertRenewOpinion())
                return false;
        }
        if (cOperate.equals("UPDATE||RENEWS"))
        {
            System.out.println("UPDATE");
            if (!updateRenewOpinion())
                return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData()
    {
        prepareOutputData();

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        if (mOperate.equals("INSERT") || mOperate.equals("UPDATE"))
        {
        }
        if (mOperate.equals("INSERT||RENEWS"))
        {
            LLRenewOpinionDB tLLRenewOpinionDB = new LLRenewOpinionDB();
            mLLRenewOpinionSchema = (LLRenewOpinionSchema) cInputData.getObjectByObjectName("LLRenewOpinionSchema",0);
            tLLRenewOpinionDB.setCaseNo(mLLRenewOpinionSchema.getCaseNo());
            tLLRenewOpinionDB.setCustomerNo(mLLRenewOpinionSchema.getCustomerNo());
            tLLRenewOpinionDB.setRemarkType(mLLRenewOpinionSchema.getRemarkType());
            if(tLLRenewOpinionDB.getInfo())
            {
                mOperate = "UPDATE||RENEWS";
                updateLLRenewOpinionSchema = tLLRenewOpinionDB.getSchema();
            }
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",0);
        }
        return true;
    }

    private boolean queryData()
    {
        return true;
    }

    private boolean QueryRgtInfo()
    {
        return true;
    }

    private boolean updateData()
    {
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        tLLRegisterSchema = mLLRegisterBLSet.get(1);
        mLLRegisterBL.setSchema(tLLRegisterSchema);
        mLLRegisterBL.setDefaultValue();
        tLLRegisterSchema = mLLRegisterBL.getSchema();
        tLLRegisterDB.setSchema(tLLRegisterSchema);
        tLLRegisterDB.update();
        if (tLLRegisterDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLRegisterBL";
            tError.functionName = "updateData";
            tError.errorMessage = "保单查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean insertRenewOpinion()
    {
        System.out.println("insert.....");
        mLLRenewOpinionSchema.setMakeDate(PubFun.getCurrentDate());
        mLLRenewOpinionSchema.setMakeTime(PubFun.getCurrentTime());
        mLLRenewOpinionSchema.setModifyDate(PubFun.getCurrentDate());
        mLLRenewOpinionSchema.setModifyTime(PubFun.getCurrentTime());
        mLLRenewOpinionSchema.setOperator(mGlobalInput.Operator);
        MMap tmpMap = new MMap();
        tmpMap.put(mLLRenewOpinionSchema,"INSERT");
        this.mResult.clear();
        this.mResult.add(tmpMap);

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(this.mResult, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 撤销分案
     * @return boolean
     */
    private boolean updateRenewOpinion()
    {
        updateLLRenewOpinionSchema.setRemark(mLLRenewOpinionSchema.getRemark());
        updateLLRenewOpinionSchema.setModifyDate(PubFun.getCurrentDate());
        updateLLRenewOpinionSchema.setModifyTime(PubFun.getCurrentTime());
        updateLLRenewOpinionSchema.setOperator(mGlobalInput.Operator);
        MMap tmpMap = new MMap();
        tmpMap.put(updateLLRenewOpinionSchema,"UPDATE");
        this.mResult.clear();
        this.mResult.add(tmpMap);

        PubSubmit ps = new PubSubmit();
        ps.submitData(this.mResult, "");
        if (!ps.submitData(this.mResult, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }
    /**
     * 准备需要保存的数据
     */
    private void prepareOutputData()
    {
    }

    //选取审核人的函数
    private boolean ReturnData()
    {
        String a_RgtName = "";
        String a_HandlerName = "";
        LLRegisterDB aLLRegisterDB = new LLRegisterDB();
        aLLRegisterDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        LLRegisterSet aLLRegisterSet = new LLRegisterSet();
        aLLRegisterSet.set(aLLRegisterDB.query());
        String CaseState = CaseFunPub.getCaseStateByRgtNo(mLLRegisterSchema.
                getRgtNo());
        System.out.println(CaseState);
        if (aLLRegisterSet.size() == 0)
        {
            this.mErrors.copyAllErrors(aLLRegisterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLRegisterBL";
            tError.functionName = "updateData";
            tError.errorMessage = "数据返回失败！！！";
            this.mErrors.addOneError(tError);
            return false;
        }
        mResult.clear();
        a_RgtName = CaseFunPub.show_People(aLLRegisterSet.get(1).getOperator());
        a_HandlerName = CaseFunPub.show_People(aLLRegisterSet.get(1).getHandler());
        mResult.addElement(CaseState);
        mResult.addElement(a_RgtName);
        mResult.addElement(a_HandlerName);
        mResult.add(aLLRegisterSet);
        return true;
    }

    private boolean dealHandler()
    {
        LLCaseCommon tLLCaseCommon = new LLCaseCommon();
        //String strMngCom = "8611";
        String strMngCom;

        strMngCom = mG.ManageCom;
        System.out.println("当前的登陆机构是＝＝＝＝" + strMngCom);
        strHandler = tLLCaseCommon.ChooseAssessor(strMngCom,"01");
        if (strHandler == null || strHandler.equals(""))
        {
            return false;
        }
        System.out.println("本次操作的审核人是＝＝＝＝" + strHandler);
        return true;
    }

    /*******************************************************************************
     * Name     :isRegister
     * Function :判断该报案信息是否已经立案，若已经立案则不能对此进行重复立案操作
     * Author   :LiuYansong
     * Date     :2003-8-6
     */

    private boolean isRegister()
    {
        return true;
    }

    /*****************************************************************************
     * Name     :checkData()
     * Function :对立案中所有的日期进行判断
     * Author   :LiuYansong
     * Date     :2003-8-6
     */
    private boolean checkData()
    {
        return true;
    }

    public static void main(String[] args)
    {
        LLContUWBL aLLContUWBL = new LLContUWBL();
        LLRenewOpinionSchema tLLRenewOpinionSchema = new LLRenewOpinionSchema();
        tLLRenewOpinionSchema.setCaseNo("C0000050609000005");
        tLLRenewOpinionSchema.setCustomerNo("000000566");
        tLLRenewOpinionSchema.setCustomerName("赵贵龙");
        tLLRenewOpinionSchema.setRemarkType("1");
        tLLRenewOpinionSchema.setRemark("能续保");
        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86";
        mGlobalInput.ComCode = "86";
        mGlobalInput.Operator = "ocm001";
        VData aVData = new VData();
        aVData.addElement(mGlobalInput);
        aVData.addElement(tLLRenewOpinionSchema);
        aLLContUWBL.submitData(aVData,"INSERT||RENEWS");
    }
}
