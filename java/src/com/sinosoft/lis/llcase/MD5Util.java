package com.sinosoft.lis.llcase;

import java.security.MessageDigest;

/**
 * 
 * Title: [MD5加密工具类]
 */
public class MD5Util {
    /**
     * 
     * Discription:[对字符进行加密]
     * @param inStr 待加密字符串
     * @return 密文
     */
    public final static String encodeMD5(String inStr) {
        MessageDigest md5 = null;
        System.out.println("MD5Util==========MD5加密密码开始");
        try {
            md5 = MessageDigest.getInstance("MD5");
            byte[] byteArray = inStr.getBytes("UTF-8");
            byte[] md5Bytes = md5.digest(byteArray);
            StringBuffer hexValue = new StringBuffer();
            for (int i = 0; i < md5Bytes.length; i++) {
                int val = ((int) md5Bytes[i]) & 0xff;
                if (val < 16) {
                    hexValue.append("0");
                }
                hexValue.append(Integer.toHexString(val));
            }
            System.out.println("MD5Util==========MD5加密密码完毕");
            return hexValue.toString().toUpperCase();
        }
        catch (Exception e) {
            System.out.println("MD5Util==========MD5加密密码异常");
        	e.printStackTrace();
            return null;
        }
    }

    /**
     *  
     * Discription:[对字符进行加密，用与系统密码认证和系统数据篡改校验] 
     * @param inStr 待加密字符串
     *  @param secret 系统加密私钥
     * @return 密文
     */
    public final static String encodeMD5WithSecret(String inStr, String secret) {
        return encodeMD5(inStr + secret);
    }
}
