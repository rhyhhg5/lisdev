package com.sinosoft.lis.llcase;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLCaseTreatmentInfoSchema;
import com.sinosoft.lis.vschema.LLCaseTreatmentInfoSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LLCaseTreatmentInfoBL {

	//错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private LLCaseTreatmentInfoSet mLLCaseTreatmentInfoSet;
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private MMap map = new MMap();
    
    private LLCaseTreatmentInfoSchema mLLCaseTreatmentInfoSchema = new LLCaseTreatmentInfoSchema();
    
    public LLCaseTreatmentInfoBL() {
    }

    public static void main(String[] args) {
    }
    
    public boolean submitData(VData cInputData, String cOperate){
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        //得到输入数据
        if (!getInputData()) {
            return false;
        }

        //进行业务处理
        if (!dealData(cOperate)) {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLCaseDrugInfoBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }
    
    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData() 
    {
    	mLLCaseTreatmentInfoSchema = (LLCaseTreatmentInfoSchema) mInputData.getObjectByObjectName(
                "LLCaseTreatmentInfoSchema", 0);
    	if (mLLCaseTreatmentInfoSchema == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "mLLCaseTreatmentInfoBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "在诊疗项目信息接受时没有得到足够的数据，请您确认有：诊疗项目完整信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    
    /**
     * 根据前面的输入数据，进行UI逻辑处理
      * 如果在处理过程中出错，则返回false,否则返回true
      */
     private boolean dealData(String cOperate)
     {
          boolean tReturn =false;
          System.out.println(mLLCaseTreatmentInfoSchema.getMakeDate());
          if("UPDATE".equals(cOperate))
          {
        	  map.put(mLLCaseTreatmentInfoSchema, "DELETE&INSERT");
              tReturn=true;
              return tReturn ;
          }
          else
          {
        	  map.put(mLLCaseTreatmentInfoSchema, cOperate);
              tReturn=true;
              return tReturn ;
          }
    }
     
     /**
      * 准备往后层输出所需要的数据
      * 输出：如果准备数据时发生错误则返回false,否则返回true
      */
     private boolean prepareOutputData()
     {
        try
        {
          mInputData.clear();
          mInputData.add(map);
          mResult.add(mLLCaseTreatmentInfoSchema);
        }
        catch(Exception ex)
        {
          // @@错误处理
          CError tError =new CError();
          tError.moduleName="LLCaseTreatmentInfoUI";
          tError.functionName="prepareData";
          tError.errorMessage="在准备往后层处理所需要的数据时出错。";
          this.mErrors .addOneError(tError) ;
          return false;
        }
        return true;
    }
     
   //得到结果
     public VData getResult() {
         return this.mResult;
     }

}
