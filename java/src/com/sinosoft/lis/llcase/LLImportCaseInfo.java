package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.LLCaseSet;

/**
 * <p>Title: 理赔案件信息导入类</p>
 * <p>Description: 把Excel里的理赔信息添加到数据库 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author Xx
 * @version 1.0
 */

public class LLImportCaseInfo
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGlobalInput;
    private VData mResult = new VData();
    private VData mInputData = new VData();

    /** 批次号 */
    private String mBatchNo = null;
    private String mRgtNo = "";
    /**文件路径*/
    private String FilePath="";
    /**文件名*/
    private String FileName="";
    /**导入标志*/
    private String ImportFlag="";

    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    MMap map = new MMap();

    /**
     * 构造函数
     * @param gi GlobalInput
     */
    public LLImportCaseInfo()
    {
    }

    /**
     * 导入入口
     * @param path String
     * @param fileName String
     */
    public boolean submitData(VData mVData, String cOperate) {
        System.out.println("submitData Begin....");
        if (!getInputData(mVData))
            return false;
        if(!checkData())
            return false;
        System.out.println(FilePath);
        System.out.println(FileName);
        if (!doAdd(FilePath, FileName,ImportFlag))
        {
        	return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        mInputData.add(map);
        if (!tPubSubmit.submitData(mInputData, "")) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return buildErr("submitData","LLRegister数据更新失败！");
        }
        return true;
    }

    /**
     * 从save页面获取数据
     * @param map MMap
     * @return boolean
     */
    private boolean getInputData(VData mInputData) {
        System.out.println("getInputData ......");
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        TransferData tTransferData = new TransferData();
        tTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData",0);
        FilePath = (String) tTransferData.getValueByName("FilePath");
        FileName = (String) tTransferData.getValueByName("FileName");
        mRgtNo = (String) tTransferData.getValueByName("RgtNo");
        ImportFlag = (String) tTransferData.getValueByName("ImportFlag");
        return true;
    }

    // @@错误处理
    private boolean buildErr(String FucName,String ErrMsg){
        CError tError = new CError();
        tError.moduleName = "LLImportCaseInfo";
        tError.functionName = FucName;
        tError.errorMessage = ErrMsg;
        this.mErrors.addOneError(tError);
        return false;
    }
    /**
     * 处理前检查数据是否合理
     * @return boolean
     */
    private boolean checkData() {
    	LLClaimUserDB tLLClaimUserDB=new LLClaimUserDB();
  	  	tLLClaimUserDB.setUserCode(mGlobalInput.Operator);//校验操作人是否具有理赔权限
  	  	if(!tLLClaimUserDB.getInfo())
  		  return buildErr("checkData","操作人不具有理赔权限!");
    	
        if(mRgtNo==null||mRgtNo.equals("")){
        	return buildErr("checkData","团体批次号不能为空!");
        }

        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if (!tLLRegisterDB.getInfo()) {
        	return buildErr("checkData", "团体立案信息查询失败!批次号：" + mRgtNo);
		}
		mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
		String declineflag = "" + mLLRegisterSchema.getDeclineFlag();
		if (declineflag.equals("1")) {
			return buildErr("checkData", "该团体申请已撤件，不能再导入个人客户!");
		}
		if ("02".equals(mLLRegisterSchema.getRgtState())) {
			return buildErr("checkData", "团体申请下所有个人案件已经受理完毕，" + "不能再导入个人客户!");
		}
		if ("03".equals(mLLRegisterSchema.getRgtState())) {
			return buildErr("checkData", "团体申请下所有个人案件已经结案完毕，" + "不能再导入个人客户!");
		}
		if ("04".equals(mLLRegisterSchema.getRgtState())
				|| "05".equals(mLLRegisterSchema.getRgtState())) {
			return buildErr("checkData", "团体申请下所有个人案件已经给付确认，" + "不能再导入个人客户!");
		}
		LLCaseSet tLLCaseSet = new LLCaseSet();
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        tLLCaseSet.set(tLLCaseDB.query());
        if (tLLCaseDB.mErrors.needDealError()) {
            // @@错误处理
            CError.buildErr(this, "个人案件查询失败");
            return false;
        }
        int realPeoples = tLLCaseSet.size();
        int appPeoples = mLLRegisterSchema.getAppPeoples();
        if (realPeoples >= appPeoples) {
            mLLRegisterSchema.setAppPeoples(realPeoples + 1);
        }
        mLLRegisterSchema.setApplyerType("5");
        if ("PR".equals(ImportFlag)) {
            mLLRegisterSchema.setApplyerType("2");
        }
        mLLRegisterSchema.setOperator(mGlobalInput.Operator);
        mLLRegisterSchema.setMngCom(mGlobalInput.ManageCom);
        mLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
        mLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLLRegisterSchema, "UPDATE");
        return true;
    }

    public boolean doAdd(String ImportPath, String FileName,String aImportFlag) {
        //处理批次号：批次号码为文件名（不含扩展名）
        mBatchNo = FileName.substring(0, FileName.lastIndexOf("."));

        //从磁盘导入数据
        LLImportFile tLLimportFile = new LLImportFile(ImportPath, FileName,mRgtNo,mGlobalInput,aImportFlag);
        VData data = tLLimportFile.doImport();
        if (data == null) {
            System.out.println("全部导入成功！");
        }
        this.mResult=data;
//        TransferData tTransferData=(TransferData)data.getObjectByObjectName("TransferData",0);
//        String tFailureNum=(String)tTransferData.getValueByName("FailureNum");
//        if(tFailureNum!=null && Integer.parseInt(tFailureNum)>0)//错误数大于0时
//        	return false;
//        this.mResult=data;
        return true;
    }

    /**
     * 返回执行结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args) {
        VData tVData = new VData();
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "8612";
        tGI.Operator = "claim";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("FileName", "Book1.xls");
        tTransferData.setNameAndValue("FilePath", "D:/DEVELOP/ui/temp_lp/");
        tTransferData.setNameAndValue("RgtNo","P1200070410000001");

        tVData.add(tTransferData);
        tVData.add(tGI);
        LLImportCaseInfoNew tLLImportCaseInfo = new LLImportCaseInfoNew();
        tLLImportCaseInfo.submitData(tVData, "");
    }
}
