package com.sinosoft.lis.llcase;

import org.w3c.dom.Node;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.VData;

public interface SIParser {
    public VData parseOneInsuredNode(Node node) ;
    public boolean setParam(String aRgtNo,String aGrpContNo,String aPath);
	public boolean setGlobalInput(GlobalInput aG);
	public void setFormTitle(String[] cTitle,String vtsName);
}
