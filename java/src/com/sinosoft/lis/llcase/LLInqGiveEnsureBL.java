/*
 * <p>Title: PICCH业务系统</p>
 * <p>ClassName:LLInqGiveEnsureBL </p>
 * <p>Description: 调查费确认给付 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author：Xx
 * @version：1.0
 * @CreateDate：2005-02-20
 */

package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bl.LCPolBL;

public class LLInqGiveEnsureBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    private VData mResult = new VData();
    private MMap map = new MMap();
//    private String mOperate = "";

    //全局数据
    private GlobalInput mG = new GlobalInput();
    //账单费用明细
    private LLInqFeeStaSet mLLInqFeeStaSet = new LLInqFeeStaSet();

//    private String mcOtherNo = "";
//    private String mfOtherNo = "";
    private String mFeeOtherNo = "";
    private String mOtherNo = "";
//    private String ActuGetNoC = "";
//    private String ActuGetNoF = "";
    private String mNoLimit = "";
    private String aDate = "";
    private String aTime = "";
    private String mMangeCom = "";
    //调查费领款人
    private String mUserName = "";
    private int Days = 0; 
    private String aMonth = "";
    private String strDate = ""; 

    public LLInqGiveEnsureBL() {
    }

    //用于调试
    public static void main(String[] args) {
        LLInqGiveEnsureBL tLLInqGiveEnsureBL = new LLInqGiveEnsureBL();
        LLInqFeeStaSet tLLInqFeeStaSet = new LLInqFeeStaSet();
        LLInqFeeStaSchema tLLInqFeeStaSchema = new LLInqFeeStaSchema();
        tLLInqFeeStaSchema.setOtherNo("C1100090421000001");
        tLLInqFeeStaSchema.setInqFee(250);
        tLLInqFeeStaSchema.setIndirectFee(0);
        tLLInqFeeStaSchema.setSurveyFee(250);
        tLLInqFeeStaSet.add(tLLInqFeeStaSchema);
        tLLInqFeeStaSchema = new LLInqFeeStaSchema();
        tLLInqFeeStaSchema.setOtherNo("C1100100608000003");
        tLLInqFeeStaSchema.setInqFee(400);
        tLLInqFeeStaSchema.setIndirectFee(0);
        tLLInqFeeStaSchema.setSurveyFee(400);
        tLLInqFeeStaSchema.setSurveyNo("C11001006080000031");
        tLLInqFeeStaSet.add(tLLInqFeeStaSchema);
        tLLInqFeeStaSchema = new LLInqFeeStaSchema();
        tLLInqFeeStaSchema.setOtherNo("C1100100608000003");
        tLLInqFeeStaSchema.setInqFee(606.50);
        tLLInqFeeStaSchema.setIndirectFee(0);
        tLLInqFeeStaSchema.setSurveyFee(606.50);
        tLLInqFeeStaSet.add(tLLInqFeeStaSchema);
        TransferData aTransferData = new TransferData();
        aTransferData.setNameAndValue("Year", "2010");
        aTransferData.setNameAndValue("Month", "05");
        aTransferData.setNameAndValue("BatchNo", "310000200609");
        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "cm1201";
        tGI.ComCode = "86";
        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tLLInqFeeStaSet);
        tVData.add(aTransferData);
        tLLInqGiveEnsureBL.submitData(tVData, "GIVE||ENSURE");
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData 输入数据
     * @param cOperate String 操作类型
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //得到输入数据
        if (!getInputData(cInputData)) {
            return false;
        }

        //检查数据合法性
        if (!checkInputData()) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate)) {
            CError.buildErr(this, "数据提交失败!");
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有数据对象
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("getInputData()...");
        mG = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mLLInqFeeStaSet = (LLInqFeeStaSet) cInputData.getObjectByObjectName(
                "LLInqFeeStaSet", 0);
        TransferData tTD = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        String aYear = (String) tTD.getValueByName("Year");
        aMonth = (String) tTD.getValueByName("Month");
        int Month = Integer.parseInt(aMonth) + 1;
        String limitDate = aYear + "-" + Month + "-25";
        strDate = Month + "月25日";
        Days = PubFun.calInterval(limitDate, PubFun.getCurrentDate(), "D");
       
        String aBatchNo = "" + (String) tTD.getValueByName("BatchNo");
        if (aBatchNo.equals("") || aBatchNo.equals("null")) {
            CError.buildErr(this, "请不要连续点击，单击即可！");
            return false;
        }
//        mcOtherNo = "C" + aBatchNo;
//        mfOtherNo = "F" + aBatchNo;
        aDate = PubFun.getCurrentDate();
        aTime = PubFun.getCurrentTime();
        if (mG.ComCode.length() == 4) {
            mMangeCom = mG.ComCode + "0000";
        } else if (mG.ComCode.length() == 2) {
            mMangeCom = mG.ComCode + "000000";
        } else if (mG.ComCode.length() == 8) {
            mMangeCom = mG.ComCode;
        }

        if(aMonth.length()==1)
        {
        	aMonth = "0"+aMonth;
        }
        mNoLimit = mMangeCom+aYear.substring(2,4)+aMonth;
        mFeeOtherNo =PubFun1.CreateMaxNo("FeeOtherNo", mNoLimit);
//        ActuGetNoC = PubFun1.CreateMaxNo("ACTUGETNO", sNoLimit);
//        ActuGetNoF = PubFun1.CreateMaxNo("ACTUGETNO", sNoLimit);

        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(mG.Operator);
        if (tLDUserDB.getInfo()) {
            mUserName = tLDUserDB.getUserName();
        }
        return true;
    }

    /**
     * 对输入数据做必要的校验
     * @return boolean
     */
    private boolean checkInputData() {
        System.out.println("checkInputData()...");
        String othernopart = "";
        String indpart = "";
        LLInqFeeStaSet tLLInqFeeStaSet = mLLInqFeeStaSet;
        mLLInqFeeStaSet = new LLInqFeeStaSet();
        for (int i = 1; i <= tLLInqFeeStaSet.size(); i++) {
            LLInqFeeStaSchema tinqfeesta = tLLInqFeeStaSet.get(i);
            System.out.println(tinqfeesta.getSurveyNo());
            if (tinqfeesta.getInqFee() > 0.001) {
                othernopart += "'" + tinqfeesta.getSurveyNo() + "',";
            }
            if (tinqfeesta.getIndirectFee() > 0.001) {
                tinqfeesta.setRemark("JJ");
                indpart += "'" + tinqfeesta.getOtherNo() + "',";
                mLLInqFeeStaSet.add(tinqfeesta);
                if (Days < 0) {
                    CError.buildErr(this, "今天尚不能提取" + aMonth + "月的间接调查费用，请等到"
                                    + strDate + "再进行结算。");
                    return false;
                }
            }
        }
        //判断直接调查费重复给付确认
        String pinqsql="select 1 from llinqfeesta where surveyno in (" + othernopart + "'') and payed='1'";
        SSRS qss = new SSRS();
        ExeSQL qexesql = new ExeSQL();
        qss = qexesql.execSQL(pinqsql);
        if(qss.getMaxRow()>=1)
        {
        	CError.buildErr(this, "存在直接调查费已经给付确认的案件!");
            return false;
        }
        //判断间接调查费重复给付确认
        String pindsql="select 1 from llcaseindfee where otherno in (" + indpart + "'') and payed='1'";
        SSRS dss = new SSRS();
        ExeSQL dexesql = new ExeSQL();
        dss = qexesql.execSQL(pindsql);
        if(dss.getMaxRow()>=1)
        {
        	CError.buildErr(this, "存在间接调查费已经给付确认的案件!");
            return false;
        }
        String usql = "update llinqfeesta set payed='1' "
                      + " where surveyno in (" + othernopart + "'')";
        map.put(usql, "UPDATE");
        String indsql = "update llcaseindfee set payed='1' "
                        + " where otherno in (" + indpart + "'')";
        map.put(indsql, "UPDATE");
        String sql =
                "select distinct otherno, sum(inqfee),surveyno from llinqfeesta "
                + " where surveyno in (" + othernopart +
                "'') group by otherno,surveyno";
        SSRS tss = new SSRS();
        ExeSQL texesql = new ExeSQL();
        tss = texesql.execSQL(sql);
        System.out.println(sql);
        for (int i = 1; i <= tss.getMaxRow(); i++) {
            LLInqFeeStaSchema tLLInqFeeStaSchema = new LLInqFeeStaSchema();
            tLLInqFeeStaSchema.setOtherNo(tss.GetText(i, 1));
            tLLInqFeeStaSchema.setInqFee(tss.GetText(i, 2));
            tLLInqFeeStaSchema.setSurveyFee(tss.GetText(i, 2));
            tLLInqFeeStaSchema.setSurveyNo(tss.GetText(i, 3));
            tLLInqFeeStaSchema.setIndirectFee(0);
            tLLInqFeeStaSchema.setRemark("ZJ");
            mLLInqFeeStaSet.add(tLLInqFeeStaSchema);
        }
        if (mLLInqFeeStaSet.size() <= 0) {
            CError.buildErr(this, "请您确认有：需要财务结算的查勘费用!");
            return false;
        }

//        String tCheckSQL = "SELECT 1 FROM ljaget WHERE otherno IN ('" +
//                           mcOtherNo + "','" + mfOtherNo + "')";
//        SSRS tCheckSSRS = texesql.execSQL(tCheckSQL);
//        if (tCheckSSRS.getMaxRow() > 0) {
//            CError.buildErr(this, "该月调查费已结算！");
//            return false;
//        }

//        String delsql1 = "delete from LJAGET where OTHERNO IN ('" + this.mcOtherNo +
//                        "','"+this.mfOtherNo+"')";
//        map.put(delsql1, "DELETE");
//        String delsql2 = "delete from LJAGETCLAIM where GETNOTICENO IN ('" + this.mcOtherNo +
//                        "','"+this.mfOtherNo+"')";
//        map.put(delsql2, "DELETE");
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * 只有理赔健管险种非特需的不会做咨询通知、不会产生理赔费用
     * @param cOperate String 操作类型
     * @return boolean
     */
    private boolean dealData() {
        System.out.println("dealData()...");      
        for (int i = 1; i <= mLLInqFeeStaSet.size(); i++) {
            LJAGetClaimSet mLJAGetClaimSet = new LJAGetClaimSet();
            LJAGetSet mLJAGetSet = new LJAGetSet();
            double claimpay = 0;
            LLInqFeeStaSchema tinqfeesta = mLLInqFeeStaSet.get(i);
            
            String aOtherNo = tinqfeesta.getOtherNo();
            mOtherNo = tinqfeesta.getSurveyNo();
            String aFType = tinqfeesta.getRemark();
            double aFee = tinqfeesta.getSurveyFee();
                        
            if ("ZJ".equals(aFType)) {
            	String tSQL = "select inputer from LLInqFeeSta where surveyno='"+mOtherNo
            	            + "' and inqfee > 0 ";
            	ExeSQL tExeSQL = new ExeSQL();
            	String tUserCode = tExeSQL.getOneValue(tSQL);
            	LDUserDB tLDUserDB = new LDUserDB();
            	tLDUserDB.setUserCode(tUserCode);
            	if (tLDUserDB.getInfo()) {
            		mUserName = tLDUserDB.getUserName();
            	}
            }
            
            if (aOtherNo.substring(0, 1).equals("Z") ||
                aOtherNo.substring(0, 1).equals("T")) {
                if (!getAppealCost(aOtherNo, aFType, aFee)) {
                    return false;
                }
                continue;
            }
            LLCaseDB tLLCaseDB = new LLCaseDB();
            tLLCaseDB.setCaseNo(mLLInqFeeStaSet.get(i).getOtherNo());
            tLLCaseDB.setRgtState("14");
            LLCaseSet tLLCaseSet= tLLCaseDB.query();
            if(tLLCaseSet.size()>=1)
            {       
            	String aCustomerNo = tLLCaseSet.get(1).getCustomerNo();
            	if(!getCancelCase(aOtherNo, aFType, aFee,aCustomerNo))
            	{
            		return false;
            	}
            	continue;
            }
            
            LJAGetClaimDB tLJAGetClaimDB = new LJAGetClaimDB();
            
            LJAGetClaimSet tLJAGetClaimSet = new LJAGetClaimSet();
            String sql = "select * from ljagetclaim where otherno = '"+aOtherNo+"' and othernotype = '5' and feefinatype <> 'FC' ";
            System.out.println(sql);
            tLJAGetClaimSet = tLJAGetClaimDB.executeQuery(sql);
            LJAGetClaimSet ttLJAGetClaimSet = new LJAGetClaimSet();
            if(tLJAGetClaimSet.size()<=0) {
            	return false;
            } else {
            	ttLJAGetClaimSet.add(tLJAGetClaimSet.get(1));
            	
            	boolean flag = true;
                for(int k = 2; k <= tLJAGetClaimSet.size(); k++) {
                	    
                	    
					for (int z = 1; z <= ttLJAGetClaimSet.size(); z++) {
						if (tLJAGetClaimSet.get(k).getActuGetNo()
								.equals(ttLJAGetClaimSet.get(z).getActuGetNo())
								&& tLJAGetClaimSet
										.get(k)
										.getFeeOperationType()
										.equals(ttLJAGetClaimSet.get(z)
												.getFeeOperationType())
								&& tLJAGetClaimSet
										.get(k)
										.getOtherNo()
										.equals(ttLJAGetClaimSet.get(z)
												.getOtherNo())
								&& tLJAGetClaimSet
										.get(k)
										.getOtherNoType()
										.equals(ttLJAGetClaimSet.get(z)
												.getOtherNoType())
								&& tLJAGetClaimSet
										.get(k)
										.getPolNo()
										.equals(ttLJAGetClaimSet.get(z)
												.getPolNo())) {
							ttLJAGetClaimSet.get(z).setPay(
									Arith.round(ttLJAGetClaimSet.get(z)
											.getPay()
											+ tLJAGetClaimSet.get(k).getPay(),
											2));
							flag = false;
							break;
						}
					}
                		if(flag) {
                			
                			ttLJAGetClaimSet.add(tLJAGetClaimSet.get(k));
                		}
                		
                 }
            }
            
            
            double sumpay = 0.0;
            double tSumPay = 0.0;

            for (int m = 1; m <= ttLJAGetClaimSet.size(); m++) {
                tSumPay += ttLJAGetClaimSet.get(m).getPay();
                sumpay += Math.abs(ttLJAGetClaimSet.get(m).getPay());
                System.out.println(sumpay);
            }

            if (tSumPay == 0) {
                if (!getRefuseCost(aOtherNo, aFType, aFee)) {
                    return false;
                }
                continue;
            }

            double total_ZJ = aFee;
            double rem_ZJ = total_ZJ;
            String tActuGetNo = PubFun1.CreateMaxNo("ACTUGETNO", mNoLimit);
            LJAGetSchema mcLJAGetSchema = new LJAGetSchema();
            Reflections tref = new Reflections();

            for (int j = 1; j <= ttLJAGetClaimSet.size(); j++) {
                LJAGetClaimSchema tzLJAGetClaimSchema = new LJAGetClaimSchema();
                tzLJAGetClaimSchema = ttLJAGetClaimSet.get(j);
                double inqfeed = 0.0;
                if (j < ttLJAGetClaimSet.size()) {
                    //直接调查费
                    inqfeed = total_ZJ * Math.abs(tzLJAGetClaimSchema.getPay()) /
                              sumpay;
                    inqfeed = Arith.round(inqfeed, 2);
                    rem_ZJ -= inqfeed;
                } else {
                    inqfeed = rem_ZJ;
                }
                tzLJAGetClaimSchema.setActuGetNo(tActuGetNo);
                tzLJAGetClaimSchema.setOtherNo(aOtherNo);
                tzLJAGetClaimSchema.setGetNoticeNo(mOtherNo);
                tzLJAGetClaimSchema.setOtherNoType("C");
                tzLJAGetClaimSchema.setFeeFinaType(aFType);
                tzLJAGetClaimSchema.setPay(inqfeed);
                tzLJAGetClaimSchema.setOPConfirmCode(mG.Operator);
                tzLJAGetClaimSchema.setOPConfirmDate(aDate);
                tzLJAGetClaimSchema.setOPConfirmTime(aTime);
                tzLJAGetClaimSchema.setConfDate("");
                tzLJAGetClaimSchema.setEnterAccDate("");
                tzLJAGetClaimSchema.setOperator(mG.Operator);
                tzLJAGetClaimSchema.setMakeDate(aDate);
                tzLJAGetClaimSchema.setMakeTime(aTime);
                tzLJAGetClaimSchema.setModifyDate(aDate);
                tzLJAGetClaimSchema.setModifyTime(aTime);
               
                mLJAGetClaimSet.add(tzLJAGetClaimSchema);	
               
                claimpay += inqfeed;
                tref.transFields(mcLJAGetSchema, tzLJAGetClaimSchema);
            }

            mcLJAGetSchema.setOtherNo(mFeeOtherNo);
            mcLJAGetSchema.setOtherNoType("C");
            mcLJAGetSchema.setPayMode("1");
            mcLJAGetSchema.setSumGetMoney(claimpay);
            mcLJAGetSchema.setGetNoticeNo("");
            mcLJAGetSchema.setOperator(mG.Operator);
            mcLJAGetSchema.setManageCom(mMangeCom);
            mcLJAGetSchema.setDrawer(mUserName);
            if (claimpay >= 0.01) {
                mLJAGetSet.add(mcLJAGetSchema);
            }

            if (mLJAGetClaimSet.size() <= 0) {
                CError.buildErr(this, "生成财务应付数据失败！");
                return false;
            }

            if (mLJAGetSet.size() <= 0) {
                CError.buildErr(this, "生成财务应付数据失败！");
                return false;
            }

            map.put(mLJAGetSet, "INSERT");
            map.put(mLJAGetClaimSet, "INSERT");
        }

        return true;
    }


    /**
     * 咨询通知案件调查费分配
     * @param aOtherNo String
     * @param aFType String
     * @param aFee double
     * @param isCost boolean
     * @return boolean
     */
    private boolean getAppealCost(String aOtherNo, String aFType,
                                  double aFee) {
        LLConsultDB tLLConsultDB = new LLConsultDB();
        tLLConsultDB.setConsultNo(aOtherNo);
        LLConsultSet tLLConsultSet = tLLConsultDB.query();
        if (tLLConsultSet.size() <= 0) {
            CError.buildErr(this, "案件查询失败！");
            return false;
        }
        String tCustomerNo = tLLConsultSet.get(1).getCustomerNo();
        String strSql="select distinct a.polno from lcpol a where a.insuredno='"+tCustomerNo+"' and a.appflag='1'"
					+" and not exists (select 1 from lmriskapp where riskcode=a.riskcode and risktype='M' and risktype3<>'7')"
					+" union "
					+" select distinct a.polno from lbpol a where a.insuredno='"+tCustomerNo+"' and a.appflag='1'"
					+" and not exists (select 1 from lmriskapp where riskcode=a.riskcode and risktype='M' and risktype3<>'7')";
		ExeSQL exeSQL = new ExeSQL();
		SSRS ssrs = exeSQL.execSQL(strSql);
		if (exeSQL.mErrors.getErrorCount() > 0 || ssrs == null ||ssrs.getMaxRow() <= 0) 
		{
			CError.buildErr(this, "保单查询失败!");
			return false;
		}

        double tTotalFee = aFee;
        double tRemFee = tTotalFee;
        System.out.println(ssrs.getMaxRow());
        for (int i = 1; i <= ssrs.getMaxRow(); i++) 
        {           
            String tActuGetNo = PubFun1.CreateMaxNo("ACTUGETNO", mNoLimit);
            System.out.println("POLNO:" + ssrs.GetText(i, 1));
            double inqfeed = 0.0;
            if (i < ssrs.getMaxRow()) {
                inqfeed = tTotalFee /ssrs.getMaxRow();
                inqfeed = Arith.round(inqfeed, 2);
                tRemFee -= inqfeed;
            } else {
                inqfeed = tRemFee;
            }

            LCPolBL tLCPolBL = new LCPolBL();
            tLCPolBL.setPolNo(ssrs.GetText(i, 1));
            if (!tLCPolBL.getInfo()) {
                CError.buildErr(this, "保单查询失败[" + ssrs.GetText(i, 1) + "]");
                return false;
            }
            LCPolSchema tLCPolSchema = tLCPolBL.getSchema();
            
            LJAGetClaimSchema ajagetclm = new LJAGetClaimSchema();
            Reflections tref = new Reflections();
            tref.transFields(ajagetclm, tLCPolSchema);
            ajagetclm.setOtherNo(aOtherNo);
            ajagetclm.setFeeFinaType(aFType);
            ajagetclm.setPay(inqfeed);
            ajagetclm.setActuGetNo(tActuGetNo);
            ajagetclm.setGetNoticeNo(mOtherNo);
            ajagetclm.setOtherNoType("F");
            ajagetclm.setFeeOperationType("000");
            ajagetclm.setOPConfirmCode(mG.Operator);
            ajagetclm.setOPConfirmDate(aDate);
            ajagetclm.setOPConfirmTime(aTime);
            ajagetclm.setConfDate("");
            ajagetclm.setEnterAccDate("");

            ajagetclm.setOperator(mG.Operator);
            ajagetclm.setMakeDate(aDate);
            ajagetclm.setMakeTime(aTime);
            ajagetclm.setModifyDate(aDate);
            ajagetclm.setModifyTime(aTime);

            LJAGetSchema tLJAGetSchema = new LJAGetSchema();
            tref.transFields(tLJAGetSchema, ajagetclm);
            tLJAGetSchema.setOtherNo(mFeeOtherNo);
            tLJAGetSchema.setPayMode("1");
            tLJAGetSchema.setGetNoticeNo("");
            tLJAGetSchema.setSumGetMoney(inqfeed);
            tLJAGetSchema.setOperator(mG.Operator);
            tLJAGetSchema.setManageCom(mMangeCom);
            tLJAGetSchema.setDrawer(mUserName);

            map.put(ajagetclm, "INSERT");
            map.put(tLJAGetSchema, "INSERT");
        }
        return true;
    }
    /**
     * 撤件类案件
     * @param aOtherNo String
     * @param aFType String
     * @param aFee double
     * @param isCost boolean
     * @return boolean
     */
    private boolean getCancelCase(String aOtherNo, String aFType,double aFee,String aCustomerNo) 
    {    	
    	String strSql="select distinct a.polno from lcpol a where a.insuredno='"+aCustomerNo+"' and a.appflag='1'"
					+" and not exists (select 1 from lmriskapp where riskcode=a.riskcode and risktype='M' and risktype3<>'7')"
					+" union "
					+" select distinct a.polno from lbpol a where a.insuredno='"+aCustomerNo+"' and a.appflag='1'"
					+" and not exists (select 1 from lmriskapp where riskcode=a.riskcode and risktype='M' and risktype3<>'7')";
    	ExeSQL exeSQL = new ExeSQL();
    	SSRS ssrs = exeSQL.execSQL(strSql);
    	if (exeSQL.mErrors.getErrorCount() > 0 || ssrs == null ||ssrs.getMaxRow() <= 0) 
    	{
		 CError.buildErr(this, "保单查询失败!");
		 return false;
		}
		
        double tTotalFee = aFee;
        double tRemFee = tTotalFee;
        System.out.println(ssrs.getMaxRow());
        for (int i = 1; i <= ssrs.getMaxRow(); i++) 
        {        	
            String tActuGetNo = PubFun1.CreateMaxNo("ACTUGETNO", mNoLimit);
            System.out.println("POLNO:" + ssrs.GetText(i, 1));
            double inqfeed = 0.0;
            if (i < ssrs.getMaxRow()) 
            {
                inqfeed = tTotalFee / ssrs.getMaxRow();
                inqfeed = Arith.round(inqfeed, 2);
                tRemFee -= inqfeed;
            } 
            else 
            {
                inqfeed = tRemFee;
            }
            LJAGetClaimSchema ajagetclm = new LJAGetClaimSchema();
            Reflections tref = new Reflections();
            LCPolBL tLCPolBL = new LCPolBL();
            tLCPolBL.setPolNo(ssrs.GetText(i, 1));
            if (!tLCPolBL.getInfo()) {
                CError.buildErr(this, "保单查询失败[" + ssrs.GetText(i, 1) + "]");
                return false;
            }
            LCPolSchema tLCPolSchema = tLCPolBL.getSchema();
            tref.transFields(ajagetclm, tLCPolSchema);                
          
            ajagetclm.setOtherNo(aOtherNo);
            ajagetclm.setFeeFinaType(aFType);
            ajagetclm.setPay(inqfeed);
            ajagetclm.setActuGetNo(tActuGetNo);
            ajagetclm.setGetNoticeNo(mOtherNo);
            ajagetclm.setOtherNoType("F");
            ajagetclm.setFeeOperationType("000");
            ajagetclm.setOPConfirmCode(mG.Operator);
            ajagetclm.setOPConfirmDate(aDate);
            ajagetclm.setOPConfirmTime(aTime);
            ajagetclm.setConfDate("");
            ajagetclm.setEnterAccDate("");

            ajagetclm.setOperator(mG.Operator);
            ajagetclm.setMakeDate(aDate);
            ajagetclm.setMakeTime(aTime);
            ajagetclm.setModifyDate(aDate);
            ajagetclm.setModifyTime(aTime);

            LJAGetSchema tLJAGetSchema = new LJAGetSchema();
            tref.transFields(tLJAGetSchema, ajagetclm);
            tLJAGetSchema.setOtherNo(mFeeOtherNo);
            tLJAGetSchema.setPayMode("1");
            tLJAGetSchema.setGetNoticeNo("");
            tLJAGetSchema.setSumGetMoney(inqfeed);
            tLJAGetSchema.setOperator(mG.Operator);
            tLJAGetSchema.setManageCom(mMangeCom);
            tLJAGetSchema.setDrawer(mUserName);

            map.put(ajagetclm, "INSERT");
            map.put(tLJAGetSchema, "INSERT");
        }
    	return true;
    }
    /**
     * 拒付案件调查费
     * @param aOtherNo String
     * @param aFType String
     * @param aFee double
     * @param isCost boolean
     * @return boolean
     */
    private boolean getRefuseCost(String aOtherNo, String aFType,
                                  double aFee) {
        String sql ="select distinct a.polno from llclaimdetail a where a.caseno='"+aOtherNo+ "'"
                  +" and not exists (select 1 from lmriskapp where risktype='M' and risktype3<>'7' and riskcode=a.riskcode)";
        ExeSQL exeSQL = new ExeSQL();
        SSRS ssrs = exeSQL.execSQL(sql);
        if (exeSQL.mErrors.getErrorCount() > 0 || ssrs == null ||
            ssrs.getMaxRow() <= 0) {
            CError.buildErr(this, "案件理算数据错误");
            return false;
        }

        LJAGetSchema tLJAGetSchema = new LJAGetSchema();
        String tActuGetNo = PubFun1.CreateMaxNo("ACTUGETNO", mNoLimit);
        double tTotalFee = aFee;
        double tRemFee = tTotalFee;
        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
            double inqfeed = 0.0;
            if (i < ssrs.getMaxRow()) {
                inqfeed = tTotalFee / ssrs.getMaxRow();
                inqfeed = Arith.round(inqfeed, 2);
                tRemFee -= inqfeed;
            } else {
                inqfeed = tRemFee;
            }

            LCPolBL tLCPolBL = new LCPolBL();
            tLCPolBL.setPolNo(ssrs.GetText(i, 1));
            if (!tLCPolBL.getInfo()) {
                CError.buildErr(this, "保单查询失败[" + ssrs.GetText(i, 1) + "]");
                return false;
            }

            LCPolSchema tLCPolSchema = tLCPolBL.getSchema();

            LJAGetClaimSchema ajagetclm = new LJAGetClaimSchema();
            Reflections tref = new Reflections();
            tref.transFields(ajagetclm, tLCPolSchema);
            ajagetclm.setOtherNo(aOtherNo);
            ajagetclm.setFeeFinaType(aFType);
            ajagetclm.setPay(inqfeed);
            ajagetclm.setActuGetNo(tActuGetNo);
            ajagetclm.setGetNoticeNo(mOtherNo);
            ajagetclm.setOtherNoType("F");
            ajagetclm.setFeeOperationType("000");

            ajagetclm.setOPConfirmCode(mG.Operator);
            ajagetclm.setOPConfirmDate(aDate);
            ajagetclm.setOPConfirmTime(aTime);
            ajagetclm.setConfDate("");
            ajagetclm.setEnterAccDate("");

            ajagetclm.setOperator(mG.Operator);
            ajagetclm.setMakeDate(aDate);
            ajagetclm.setMakeTime(aTime);
            ajagetclm.setModifyDate(aDate);
            ajagetclm.setModifyTime(aTime);

            tref.transFields(tLJAGetSchema, ajagetclm);
            tLJAGetSchema.setOtherNo(mFeeOtherNo);
            tLJAGetSchema.setPayMode("1");
            tLJAGetSchema.setGetNoticeNo("");
            tLJAGetSchema.setSumGetMoney(aFee);
            tLJAGetSchema.setOperator(mG.Operator);
            tLJAGetSchema.setManageCom(mMangeCom);
            tLJAGetSchema.setDrawer(mUserName);

            map.put(ajagetclm, "INSERT");
        }
        map.put(tLJAGetSchema, "INSERT");
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错。");
            return false;
        }
        return true;
    }

    /**
     * 得到处理后的结果数据
     * @return boolean
     */
    public VData getResult() {
        return this.mResult;
    }

}
