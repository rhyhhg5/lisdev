package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.llcase.*;

/**
 * <p>Title: 医保通结算批次申请</p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author
 * @version 1.0
 * @date
 */

public class LLHospFinanceBL {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    private LLHospGetSchema mLLHospGetSchema = new LLHospGetSchema();

    private String mHospitCode = "";

    private String mHCNo = "";

    private MMap map = new MMap();

    /** 全局参数 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 统一更新日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 统一更新时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    private String mOperator = "";

    private String mManageCom = "";

    public LLHospFinanceBL() {
    }

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        // 将外部传入的数据分解到本类的属性中，准备处理
        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        // 根据业务逻辑对数据进行处理
        if (!dealData()) {
            return false;
        }

        // 装配处理好的数据，准备给后台进行保存
        prepareOutputData();

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLHospFinanceBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 返回产生的结算批次号
     * @return String
     */
    public String getHCNo() {
        return mLLHospGetSchema.getHCNo();
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData() {
        mGlobalInput.setSchema((GlobalInput) mInputData.
                               getObjectByObjectName("GlobalInput", 0));
        mLLHospGetSchema.setSchema((LLHospGetSchema) mInputData.
                                   getObjectByObjectName("LLHospGetSchema", 0));

        mOperator = mGlobalInput.Operator;
        mManageCom = mGlobalInput.ManageCom;

        if (mManageCom.length() == 2) {
            mManageCom = mManageCom + "000000";
        } else if (mManageCom.length() == 4) {
            mManageCom = mManageCom + "0000";
        }

        mHospitCode = mLLHospGetSchema.getHospitCode();

        if ("INSERT".equals(mOperate)) {
            if (mHospitCode == null || "".equals(mHospitCode)) {
                buildError("getInputData", "未获得医院编码");
                return false;
            }
        }

        mHCNo = mLLHospGetSchema.getHCNo();
        if ("DELETE".equals(mOperate)) {
            if (mHCNo == null || "".equals(mHCNo)) {
                buildError("getInputData", "未获取结算批次号");
                return false;
            }
        }
        return true;
    }

    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData() {
        if (mOperate == null || "".equals(mOperate)) {
            buildError("checkData", "Operate为空");
            return false;
        }
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData() {
        if ("INSERT".equals(mOperate)) {
            LDHospitalDB tLDHospitalDB = new LDHospitalDB();
            tLDHospitalDB.setHospitCode(mHospitCode);
            if (!tLDHospitalDB.getInfo()) {
                buildError("dealData", "医院查询失败");
                return false;
            }

            LLHospGetDB tLLHospGetDB = new LLHospGetDB();
            tLLHospGetDB.setHospitCode(mHospitCode);
            tLLHospGetDB.setState("0");
            LLHospGetSet tLLHospGetSet = tLLHospGetDB.query();
            if (tLLHospGetSet.size() > 0) {
                buildError("dealData", "该医院有未确认的批次");
                return false;
            }

            LLHospGetSchema tLLHospGetSchema = new LLHospGetSchema();
            String tLimit = PubFun.getNoLimit(mManageCom);
            System.out.println("管理机构代码是 : " + tLimit);
            String tHCNo = PubFun1.CreateMaxNo("HCNO", tLimit);

            tLLHospGetSchema.setHCNo(tHCNo);
            tLLHospGetSchema.setState("0");
            tLLHospGetSchema.setHospitCode(mHospitCode);
            tLLHospGetSchema.setApplyer(mOperator);
            tLLHospGetSchema.setAppDate(mCurrentDate);
            tLLHospGetSchema.setManageCom(mManageCom);
            tLLHospGetSchema.setOperator(mOperator);
            tLLHospGetSchema.setMakeDate(mCurrentDate);
            tLLHospGetSchema.setMakeTime(mCurrentTime);
            tLLHospGetSchema.setModifyDate(mCurrentDate);
            tLLHospGetSchema.setModifyTime(mCurrentTime);

            map.put(tLLHospGetSchema, "INSERT");
            mResult.clear();
            mResult.add(tLLHospGetSchema);
        }

        if ("DELETE".equals(mOperate)) {
            LLHospGetDB tLLHospGetDB = new LLHospGetDB();
            tLLHospGetDB.setHCNo(mHCNo);
            if (!tLLHospGetDB.getInfo()) {
                buildError("dealData", "结算批次出查询失败");
                return false;
            }

            LLHospGetSchema tLLHospGetSchema = tLLHospGetDB.getSchema();

            if (!"0".equals(tLLHospGetSchema.getState())) {
                buildError("dealData", "该结算批次不能进行撤销");
                return false;
            }

            LLHospCaseDB tLLHospCaseDB = new LLHospCaseDB();
            tLLHospCaseDB.setHCNo(mHCNo);
            LLHospCaseSet tLLHospCaseSet = tLLHospCaseDB.query();
            if (tLLHospCaseSet.size() > 0) {
                buildError("dealData", "请先删除该批次下案件");
                return false;
            }

            map.put(tLLHospGetSchema, "DELETE");
            mResult.clear();
            mResult.add(tLLHospGetSchema);
        }

        return true;
    }


    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData() {
        mInputData.clear();
        mInputData.add(map);
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult() {
        return mResult;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLHospFinanceBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }


    public static void main(String arg[]) {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "001";
    }
}
