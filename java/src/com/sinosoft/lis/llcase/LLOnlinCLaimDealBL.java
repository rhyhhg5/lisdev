package com.sinosoft.lis.llcase;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLCASEPROBLEMSchema;
import com.sinosoft.lis.vschema.LLCASEPROBLEMSet;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.util.FTPReplyCodeName;
import com.sinosoft.wasforwardxml.project.pad.util.FTPTool;

public class LLOnlinCLaimDealBL {

	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mG = new GlobalInput();
    private PubFun1 pf1 = new PubFun1();
    private PubFun pf =new PubFun();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LLCASEPROBLEMSet mLLCASEPROBLEMSet = new LLCASEPROBLEMSet();
    private LLCASEPROBLEMSet mmLLCASEPROBLEMSet = new LLCASEPROBLEMSet();
    LLCASEPROBLEMSchema mLLCASEPROBLEMSchema = new LLCASEPROBLEMSchema();
    private String mReturnMessage ;
    private String aDate ;
    private String aTime ;
    private FTPTool tFTPTool;
    private String mCaseno ;
    private String mFTPPath;
    private String mTransactionNum;
    private String tvVersionCode = "";
    public LLOnlinCLaimDealBL() {
    }

    public static void main(String[] args) {
        
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        
    	//将操作数据拷贝到本类中
        this.mOperate = cOperate;
        
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)){
        	buildError("getInputData()", "获取数据失败");
        	return false;
        }
        
        //进行数据基本校验
        if (!checkData()){        	
        	return false;
        }
        
        //进行业务处理
        if (!dealData()) {
            mReturnMessage += "数据处理失败LLOnlinCLaimDealBL-->dealData!";
            buildError("dealData()", mReturnMessage);
            return false;
        }
        
        //上传XML
        if(!uploadXMLToFTP()) {
        	return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, null)) {
            buildError("submitData()", "数据提交失败!");
            return false;
        }
        System.out.println("End LLOnlinCLaimDealBL Submit...");
        return true;
    }
    
    /**
     * 上传问题件到FTP
     * @return
     */
    private boolean uploadXMLToFTP() {
    	System.out.println("--Star uploadFTP--");
    	
    	if(!getConn()) {
    		return false;
    	}
    	
    	if(!uploadXML()) {
    		return false;
    	}
    	
    	return true;
    }
    
    /**
     * 上载XML到Ftp
     * @return
     */
    private boolean uploadXML() {
    	System.out.println("--Star uploadXML--");
    	
    	String RgtTypeSQL = "select rgttype from llregister where 1=1 and rgtno = '" + mCaseno + "' with ur";
		ExeSQL RgtTypeExeSQL = new ExeSQL();
		SSRS RgtTypeSSRS = RgtTypeExeSQL.execSQL(RgtTypeSQL);
		if(RgtTypeSSRS.getMaxRow()<1) {
			buildError("uploadXML()", "案件受理方式不明确");
			return false;
		}
    	String tRgtType = RgtTypeSSRS.GetText(1, 1);
    	String getPath = "";
    	if("13".equals(tRgtType)) {
    		getPath = "select codename ,codealias from ldcode where codetype='LLWXUpload' and code='FTPPath/LocalPath' ";//若有约定好目录保存FTPPath
    		tvVersionCode = "WXCLAIM";
    	}else if("15".equals(tRgtType)) {
    		getPath = "select codename ,codealias from ldcode where codetype='LLAPPUpload' and code='FTPPath/LocalPath' ";//若有约定好目录保存FTPPath
    		tvVersionCode = "APPWXCLAIM";
    	}else {
    		buildError("uploadXML()", "案件受理方式不是微信（PICC健康生活）线上受理或集团APP线上受理");
			return false;
    	}
    		
		SSRS tPathSSRS = new ExeSQL().execSQL(getPath);
		if (tPathSSRS.getMaxRow() < 1) {
			buildError("uploadXML()", "ftp服务器路径未配置");
			return false;
		}
		mFTPPath = tPathSSRS.GetText(1, 2) + pf.getCurrentDate2() + "/";
		//获取核心根目录
		String tUIRoot= CommonBL.getUIRoot();
		if(tUIRoot==null){
			buildError("uploadXML()", "没有查到应用目录");
			return false;
		}
		System.out.println(tUIRoot);
		//备份数据核心系统存储路径
		String tFileCorePath = tUIRoot + mFTPPath;	        			
			
		Document WXDocument = createXML();
		String pName=mTransactionNum + ".xml";
		StringBuffer mFilePath = new StringBuffer();
		mFilePath.append(tFileCorePath);
		File mFileDir = new File(mFilePath.toString());
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录失败！" + mFileDir.getPath());
				buildError("uploadXML()", "创建目录失败！" + mFileDir.getPath());
				return false;
			}
		}
		File tempFile=new File(mFilePath.toString() + pName);
		if(!tempFile.exists()){
			try{
				if(!tempFile.createNewFile()){
					System.err.println("创建文件失败！" + tempFile.getPath());
					buildError("uploadXML()", "创建文件失败！" + tempFile.getPath());
					return false;
				}
			}catch(Exception ex){
				System.err.println("创建文件时出现异常！" + tempFile.getPath());
				ex.printStackTrace();
				buildError("uploadXML()", "创建文件时出现异常！" + tempFile.getPath());
				return false;
			}
				
		}
		try {
			FileOutputStream tFos = new FileOutputStream(mFilePath.toString()+ pName);
			JdomUtil.output(WXDocument, tFos);
			tFos.close();
			//FTP上创建文件夹
			if(!tFTPTool.makeDirectory("/" + mFTPPath)){
            	System.out.println("新建目录已存在");
            }
			tFTPTool.upload("/" + mFTPPath, mFilePath.toString() + pName);
			
		} catch (IOException ex) {
			ex.printStackTrace();
			buildError("uploadXML()", "上传文件出现异常");
			return false;			
		}
		//断开FTP连接
		tFTPTool.logoutFTP();
		
		return true;
    }
    
    /**
     * 创建问题件XML
     * @return
     */
    private Document createXML() {
    	System.out.println("--Star createXML--");
    	
    	Element RootData = new Element("PACKET");
    	RootData.addAttribute("type", "REQUEST");
    	RootData.addAttribute("version", "1.0");
    	
    	//创建头部
    	Element HeadData = new Element("HEAD");
    	Element RequestType = new Element("REQUEST_TYPE");
    	RequestType.setText("OC03");
    	HeadData.addContent(RequestType);
    	Element TransactionNum = new Element("TRANSACTION_NUM");
    	String strOC = "OC0300000000"+pf.getCurrentDate2();
		mTransactionNum = strOC + pf1.CreateMaxNo("OC03" + pf.getCurrentDate2(), 10);
		System.out.println("mTransactionNum:"+mTransactionNum);
    	TransactionNum.setText(mTransactionNum);
    	HeadData.addContent(TransactionNum);
    	RootData.addContent(HeadData);
    	//创建BODY
    	Element BodyData = new Element("BODY");
    	Element tVersionData = new Element("VERSION_DATA");
    	Element tVersionCode = new Element("VERSIONCODE");
    	tVersionCode.setText(tvVersionCode);
    	tVersionData.addContent(tVersionCode);
    	BodyData.addContent(tVersionData);
    	Element tCaseData = new Element("CASE_DATA");
    	Element tCaseNo = new Element("CASENO");
    	tCaseNo.setText(mCaseno);
    	tCaseData.addContent(tCaseNo);
    	Element tReasonCode = new Element("REASONCODE");
    	String reasonCode = mLLCASEPROBLEMSchema.getREASONCODE();
    	tReasonCode.setText(reasonCode);
    	tCaseData.addContent(tReasonCode);
    	Element tReason = new Element("REASON");
    	String reasonSQL = "select codename from ldcode where 1=1 and codetype = 'ImageDeal' and code = '" + reasonCode + "' with ur";
    	ExeSQL reasonExeSQL = new ExeSQL();
    	SSRS reasonSSRS = reasonExeSQL.execSQL(reasonSQL);
    	int number = reasonSSRS.getMaxRow();
    	if(number<1) {
    		
    	}
    	String reasonStr = reasonSSRS.GetText(1, 1);
    	tReason.setText(reasonStr);
    	tCaseData.addContent(tReason);
    	Element tRemark = new Element("REMARK");
    	String remarkStr = mLLCASEPROBLEMSchema.getUPREMARK();
    	tRemark.setText(remarkStr);
    	tCaseData.addContent(tRemark);
    	BodyData.addContent(tCaseData);
    	RootData.addContent(BodyData);
    	Document tDocument = new Document(RootData);
    	
    	return tDocument;
    }
    
    /**
     * 连接FTP
     * @return
     */
    private boolean getConn() {
    	System.out.println("--Star getConn--");
    	String getIPPort = "select codename ,codealias from ldcode where codetype='LLTYScan' and code='IP/Port' ";
		String getUserPs = "select codename ,codealias from ldcode where codetype='LLTYScan' and code='User/Pass' ";
		SSRS tIPSSRS = new ExeSQL().execSQL(getIPPort);
		SSRS tUPSSRS = new ExeSQL().execSQL(getUserPs);
		if (tIPSSRS.getMaxRow() < 1 || tUPSSRS.getMaxRow() < 1) { 
			buildError("getConn()", "ftp配置有误");
			return false;
		}
		tFTPTool = new FTPTool(tIPSSRS.GetText(1, 1), tUPSSRS.GetText(1, 1),
				tUPSSRS.GetText(1, 2), Integer.parseInt(tIPSSRS.GetText(1, 2)));
		try {
			if (!tFTPTool.loginFTP()) {
				buildError("getConn()", tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE));
				return false;
			}
		} catch (SocketException e) {
			buildError("getConn()", "FTP连接异常");
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			buildError("getConn()", "FTP连接异常");
			e.printStackTrace();
			return false;
		}
		return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("--Star GetInputData--");
        mLLCASEPROBLEMSet = ((LLCASEPROBLEMSet) cInputData.getObjectByObjectName("LLCASEPROBLEMSet", 0));
        mG = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        aDate = PubFun.getCurrentDate();
        aTime = PubFun.getCurrentTime();
        if(mLLCASEPROBLEMSet.size()<1) {
        	return false;
        }
        mLLCASEPROBLEMSchema = mLLCASEPROBLEMSet.get(1);
        return true;
    }

    /**
     * 处理前检查数据是否合理
     * @return boolean
     */
    private boolean checkData(){
        System.out.println("---start checkData---");
        
        mCaseno = mLLCASEPROBLEMSchema.getCASENO();
        String StrSQL = "select state from LLCASEPROBLEM where 1=1 and caseno = '" + mCaseno + "' with ur";
        ExeSQL StrExeSQL = new ExeSQL();
        SSRS StrSSRS = StrExeSQL.execSQL(StrSQL);
        int StrSize = StrSSRS.getMaxRow();
        if(StrSize>0) {
        	for (int j = 1; j <= StrSize; j++) {
				String StrState = StrSSRS.GetText(j, 1);
				if("1".equals(StrState)) {
					buildError("checkData()", "该案件存在已下发的问题件不能再次下发");
			        return false;
				}
			}
        } 
        String RgtStateSQL = "select rgtstate from llcase where 1=1 and caseno = '" + mCaseno + "' with ur";
        SSRS RgtStateSSRS = StrExeSQL.execSQL(RgtStateSQL);
        if(RgtStateSSRS.getMaxRow()>0) {
        	String rgtState = RgtStateSSRS.GetText(1, 1);
        	if(!"13".equals(rgtState)) {
        		buildError("checkData()", "该案件非延迟状态不能进行问题件下发");
		        return false;
        	}
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("---Star dealData---mOperate: " + mOperate);
    
        String strES = "WT"+pf.getCurrentDate2();
		String serialno = strES + pf1.CreateMaxNo(strES, 8);
		System.out.println("serialno:"+serialno);
		mLLCASEPROBLEMSchema.setSERIALNO(serialno);
		String RgtStateSQL = "select rgtstate from llcase where 1=1 and caseno = '" + mCaseno + "' with ur";
		ExeSQL StrExeSQL = new ExeSQL();
        SSRS StrSSRS = StrExeSQL.execSQL(RgtStateSQL);
        int StrSize = StrSSRS.getMaxRow();
        if(StrSize<1) {
	        return false;
        }
        String rgtState = StrSSRS.GetText(1, 1);
        mLLCASEPROBLEMSchema.setRGTSTATE(rgtState);
        mLLCASEPROBLEMSchema.setSTATE("1");
        mLLCASEPROBLEMSchema.setMNGCOM(mG.ManageCom);
        mLLCASEPROBLEMSchema.setOPERATOR(mG.Operator);
        mLLCASEPROBLEMSchema.setMAKEDATE(aDate);
        mLLCASEPROBLEMSchema.setMAKETIME(aTime);
        mLLCASEPROBLEMSchema.setMODIFYDATE(aDate);
        mLLCASEPROBLEMSchema.setMODIFYTIME(aTime);
        mmLLCASEPROBLEMSet.add(mLLCASEPROBLEMSchema);
        map.put(mmLLCASEPROBLEMSet, "INSERT");
        
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            buildError("dealData()", "在准备往后层处理所需要的数据时出错。");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
    
    /***
	 * 错误信息
	 * @param args
	 */
	private void buildError(String szFunc,String szErrMsg){
		CError tCError = new CError();
		tCError.moduleName="LLOnlinCLaimDealBL";
		tCError.functionName=szFunc;
		tCError.errorMessage=szErrMsg;
		System.out.println(szFunc+"--"+szErrMsg);
		this.mErrors.addOneError(tCError);
	}

}
