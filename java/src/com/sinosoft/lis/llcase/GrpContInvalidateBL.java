package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.LLAppClaimReasonDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLAppClaimReasonSchema;
import com.sinosoft.lis.vschema.LLAppClaimReasonSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class GrpContInvalidateBL extends GrpContInvalidateUI {
    public CErrors mErrors = new CErrors();

    private VData mInputData;

    private VData mResult = new VData();

    private String mOperate;

    private LLAppClaimReasonSchema mLLAppClaimReasonSchema = new
            LLAppClaimReasonSchema();

    private LLAppClaimReasonSet mLLAppClaimReasonSet = new LLAppClaimReasonSet();

    private LLAppClaimReasonDB mLLAppClaimReasonDB = new LLAppClaimReasonDB();

    private GlobalInput mG = new GlobalInput();

    private MMap map = new MMap();

    public GrpContInvalidateBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("GrpContInvalidateBL");
        if (!getInputData(cInputData, cOperate)) {
            return false;
        }

        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit pubSubmit = new PubSubmit();
        if (!pubSubmit.submitData(this.mInputData, "")) {
            this.mErrors.copyAllErrors(pubSubmit.mErrors);
            return false;
        }
        return true;

    }

    private boolean getInputData(VData cInputData, String cOperate) {
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLLAppClaimReasonSchema = (LLAppClaimReasonSchema) cInputData
                                  .getObjectByObjectName(
                "LLAppClaimReasonSchema", 0);
        if (mLLAppClaimReasonSchema == null || mInputData == null || mG == null) {
            CError tError = new CError();
            tError.moduleName = "GrpContInvalidateBL";
            tError.functionName = "submitData_getInputData";
            tError.errorMessage = "读取数据失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mLLAppClaimReasonSchema.getRgtNo() == null || "".equals(mLLAppClaimReasonSchema.getRgtNo())) {
            CError tError = new CError();
            tError.moduleName = "GrpContInvalidateBL";
            tError.functionName = "submitData_getInputData";
            tError.errorMessage = "请选择保单！";
            this.mErrors.addOneError(tError);
            return false;
        }


        return true;

    }

    private boolean dealData() {
        System.out.println("mOperate:" + mOperate);
        if ("insert".equals(mOperate)) {
            mLLAppClaimReasonSchema.setMakeDate(PubFun.getCurrentDate());
            mLLAppClaimReasonSchema.setMakeTime(PubFun.getCurrentTime());
            mLLAppClaimReasonSchema.setModifyDate(PubFun.getCurrentDate());
            mLLAppClaimReasonSchema.setModifyTime(PubFun.getCurrentTime());
            mLLAppClaimReasonSchema.setOperator(mG.Operator);
            String signcomSQL =
                    "select managecom from lcgrpcont where grpcontno='" +
                    mLLAppClaimReasonSchema.getRgtNo() +
                    "' union select managecom from lbgrpcont where grpcontno='" +
                    mLLAppClaimReasonSchema.getRgtNo() + "'";
            ExeSQL tExeSQL = new ExeSQL();
            String comName = tExeSQL.getOneValue(signcomSQL);
            mLLAppClaimReasonSchema.setMngCom(comName);
            /*	String sql="select grpname from ldcom where comcode='"+mG.ManageCom+"'";
             System.out.println("mylovelysql:"+sql);
             ExeSQL tExeSQL = new ExeSQL();
             String comName=tExeSQL.getOneValue(sql);*/
            mLLAppClaimReasonSet.add(mLLAppClaimReasonSchema);
            map.put(mLLAppClaimReasonSet, "INSERT");
            return true;
        } else if ("del".equals(mOperate)) {
//            String sql = "select name from ldcom where comcode='" +
//                         mG.ManageCom + "'";
//            System.out.println("mylovelysql:" + sql);
//            ExeSQL tExeSQL = new ExeSQL();
//            String comName = tExeSQL.getOneValue(sql);

            mLLAppClaimReasonDB.setCaseNo(mLLAppClaimReasonSchema.getCaseNo());
            mLLAppClaimReasonDB.setRgtNo(mLLAppClaimReasonSchema.getRgtNo());
            mLLAppClaimReasonDB.setReasonCode(mLLAppClaimReasonSchema.getReasonCode());

            if (!mLLAppClaimReasonDB.getInfo()){
                CError.buildErr(this,"查询保单失败！");
                return false;
            }
            LLAppClaimReasonSchema tLLAppClaimReasonSchema =
                    mLLAppClaimReasonDB.getSchema();
            System.out.println(tLLAppClaimReasonSchema.getCaseNo() +
                               "   caseno");
            map.put(tLLAppClaimReasonSchema, "DELETE");
            return true;
        } else {
            CError tError = new CError();
            tError.moduleName = "GrpContInvalidateBL";
            tError.functionName = "submitData_dealData";
            tError.errorMessage = "处理数据失败！";
            this.mErrors.addOneError(tError);
            return false;

        }
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpContInvalidateBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

}
