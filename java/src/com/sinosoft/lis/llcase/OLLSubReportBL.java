package com.sinosoft.lis.llcase;


/*
 * <p>ClassName: OLLSubReportBL </p>
 * <p>Description: OLLSubReportBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft </p>
 * @Database:
 * @CreateDate：2005-01-12 09:36:55
 */

import com.sinosoft.lis.db.LLSubReportDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLAskRelaSchema;
import com.sinosoft.lis.schema.LLCaseRelaSchema;
import com.sinosoft.lis.schema.LLNoticeRelaSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.vschema.LLAskRelaSet;
import com.sinosoft.lis.vschema.LLCaseRelaSet;
import com.sinosoft.lis.vschema.LLNoticeRelaSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class OLLSubReportBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();


    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();


    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();


    /** 其余变量*/
    private TransferData mTransferData = new TransferData();


    /** 数据操作字符串 */
    private String mOperate;


    /** 业务处理相关变量 */
    private LLSubReportSchema mLLSubReportSchema = new LLSubReportSchema();


    /**关联*/
    private LLAskRelaSet mLLAskRelaSet = null;
    private LLNoticeRelaSet mLLNoticeRelaSet = null;
    private LLCaseRelaSet mLLCaseRelaSet = null;


//private LLSubReportSet mLLSubReportSet=new LLSubReportSet();
    public OLLSubReportBL()
    {
    }

    public static void main(String[] args)
    {
      VData tVData = new VData();
      LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
      GlobalInput tGlobalInput = new GlobalInput();
      LLAskRelaSchema tLLAskRelaSchma = new LLAskRelaSchema();
      LLAskRelaSet tLLAskRelaSet = new LLAskRelaSet();
      tLLSubReportSchema.setSubRptNo("3535345");
      tLLAskRelaSchma.setConsultNo("23234234");
      tLLAskRelaSchma.setSubRptNo("123412341234");
      tLLAskRelaSet.add(tLLAskRelaSchma);
      OLLSubReportBL tOLLSubReportBL = new OLLSubReportBL();
      TransferData tTransferData = new TransferData();
      tTransferData.setNameAndValue("tEventNo","123412341234");
      tTransferData.setNameAndValue("AskType","2");
      tVData.add(tTransferData);
      tVData.add(tLLSubReportSchema);
      tVData.add(tGlobalInput);
      tVData.add(tLLAskRelaSet);
      System.out.println(tVData);
      tOLLSubReportBL.submitData(tVData,"DELETE||RELA");

    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLLSubReportBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLLSubReportBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start OLLSubReportBL Submit...");
            String wfFlag = null;
            if (mTransferData != null)
            {
                wfFlag = (String)this.mTransferData.getValueByName(
                        "WFFLAG");

            }
            if (wfFlag == null || !"1".equals(wfFlag)) //工作流调用标记
            {
                PubSubmit pubSubmit = new PubSubmit();
                if (!pubSubmit.submitData(this.mResult, ""))
                {
                    CError.buildErr(this, "提交事务失败!");
                    return false;
                }
            }
            // OLLSubReportBLS tOLLSubReportBLS=new OLLSubReportBLS();
            // tOLLSubReportBLS.submitData(mInputData,mOperate);
            // System.out.println("End OLLSubReportBL Submit...");
            // //如果有需要处理的错误，则返回
            // if (tOLLSubReportBLS.mErrors.needDealError())
            // {
            //   // @@错误处理
            //   this.mErrors.copyAllErrors(tOLLSubReportBLS.mErrors);
            //   CError tError = new CError();
            //   tError.moduleName = "OLLSubReportBL";
            //   tError.functionName = "submitDat";
            //   tError.errorMessage ="数据提交失败!";
            //   this.mErrors .addOneError(tError) ;
            //   return false;
            // }
        }
        mInputData = null;
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
      System.out.println("-Star Deal-");
        if ( this.mGlobalInput == null)
        {
            CError.buildErr(this, "传入全局数据集不能为空!");
            return false;
        }
        MMap tmpMap = new MMap();
        String op = "UPDATE";
        String cDate = PubFun.getCurrentDate();
        String cTime = PubFun.getCurrentTime();
        String AskType = (String) mTransferData.getValueByName("AskType");


        if ("INSERT||MAIN".equals(this.mOperate))
        {
            if ( this.mLLSubReportSchema == null )
            {
                CError.buildErr(this,"传入事件信息不能为空");
                return false;
            }
            op = "INSERT";
            MMap tMap = this.InsertMain(AskType, cDate, cTime, op);
            if (tMap == null) return false;
            tmpMap.add(tMap);
        }

        if ("UPDATE||MAIN".equals(this.mOperate))
        {
            op = "UPDATE";
            //暂时不支持
        }

        //增加或删除关联
        if ("DELETE||RELA".equals(this.mOperate)
            || "INSERT||RELA".equals(this.mOperate))
        {   System.out.println("开始删除  "+this.mOperate);
            op = mOperate.substring(0,6);
            System.out.println("OP : "+op);
            if ("0".equals(StrTool.cTrim(AskType)) ||
                "2".equals(StrTool.cTrim(AskType)))
            {
              /*if (op.equals("DELETE"))
              {
                String tEventNo = (String) mTransferData.getValueByName("tEventNo");
                System.out.println("tEventNo : "+tEventNo);
                String strSQL = "delete from LLAskRela where SubRptNo='" + tEventNo +
                    "'";
                tmpMap.put(strSQL, "DELETE");
              }*/

                if (this.mLLAskRelaSet == null)
                {
                  CError.buildErr(this, "传入数据集不能为空:咨询事件关联");
                  return false;
                }
                tmpMap.put(this.mLLAskRelaSet, op);
              }

            if ("1".equals(StrTool.cTrim(AskType)) ||
                "2".equals(StrTool.cTrim(AskType)))
            {
              /*if (op.equals("DELETE"))
              {
                String tEvent = (String) mTransferData.getValueByName("tEventNo");
                System.out.print(tEvent);
                String strSQL = "delete from LLNoticeRela where SubRptNo='" + tEvent +
                    "'";
                tmpMap.put(strSQL, "DELETE");
              }*/
              if (this.mLLNoticeRelaSet == null)
              {
                CError.buildErr(this, "传入数据集不能为空:通知事件关联");
                return false;
              }
              tmpMap.put(this.mLLNoticeRelaSet, op);
            }



        }

        //事故关联
       if ( "INSERT||CASE".equals(this.mOperate))
       {   System.out.println("开始删除  "+this.mOperate);
           op = mOperate.substring(0,6);
           if ("3".equals(StrTool.cTrim(AskType)))
           {

               if (this.mLLCaseRelaSet == null)
               {
                   CError.buildErr(this, "传入数据集不能为空:事件信息");
                   return false;
               }
               String caseNo = (String)this.mTransferData.getValueByName("CASENO");
//               if (caseNo==null )
//               {
//                    CError.buildErr(this,"传入了案件号为空");
//                    return false;
//               }
                //删除掉原来的。
                  String del_sql = "delete from LLCaseRela where caseno='"+ caseNo +"'" ;
                  tmpMap.put(del_sql,"DELETE");
               //创建事故号
               String limit = PubFun.getNoLimit( this.mGlobalInput.ManageCom );
               System.out.println("管理机构名称"+this.mGlobalInput.ManageCom);
               for ( int i=1;i<= mLLCaseRelaSet.size();i++)
               {
                   LLCaseRelaSchema tSchema = mLLCaseRelaSet.get(i);
                   String CaseRelaNo = PubFun1.CreateMaxNo("CASERELANO",limit);
                   tSchema.setCaseRelaNo( CaseRelaNo );
               }
               tmpMap.put(this.mLLCaseRelaSet, op);
           }



       }

        this.mResult.add(tmpMap);
        return true;
    }

    private MMap InsertMain(String AskType, String cDate, String cTime,
                            String op)
    {
        MMap tmpMap = new MMap();

        String ConsultNo = (String) mTransferData.getValueByName( "ConsultNo");

        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        String evNo = "";
        if ("0".equals(StrTool.cTrim(AskType)) )

        {
            if ("".equals(StrTool.cTrim(ConsultNo)))
            {
                CError.buildErr(this, "咨询号不能为空");
                return null;
            }
            evNo = PubFun1.CreateMaxNo("EVENTNO", tLimit);
            LLAskRelaSchema tLLAskRelaSchema = new LLAskRelaSchema();
            tLLAskRelaSchema.setConsultNo(ConsultNo);
            tLLAskRelaSchema.setSubRptNo(evNo);
            tmpMap.put(tLLAskRelaSchema, op);
        }

        String NoticeNo = (String) mTransferData.getValueByName("NoticeNo");
        if ("1".equals(StrTool.cTrim(AskType)) )

        {
            if ("".equals(StrTool.cTrim(NoticeNo)))
            {
                CError.buildErr(this, "通知号不能为空");
                return null;
            }
            evNo = PubFun1.CreateMaxNo("EVENTNO", tLimit);
            LLNoticeRelaSchema tLLNoticeRelaSchema = new LLNoticeRelaSchema();
            tLLNoticeRelaSchema.setNoticeNo(NoticeNo);
            tLLNoticeRelaSchema.setSubRptNo(evNo);
            tmpMap.put(tLLNoticeRelaSchema, op);
        }


        String CNNo = (String) mTransferData.getValueByName("CNNo");

        if ("2".equals(StrTool.cTrim(AskType)) )
        {
            if ("".equals(StrTool.cTrim(CNNo)))
            {
                CError.buildErr(this, "咨询通知号不能为空");
                return null;
            }
            evNo = PubFun1.CreateMaxNo("EVENTNO", tLimit);

            LLAskRelaSchema tLLAskRelaSchema = new LLAskRelaSchema();
            tLLAskRelaSchema.setConsultNo(CNNo);
            tLLAskRelaSchema.setSubRptNo(evNo);
            tmpMap.put(tLLAskRelaSchema, op);



            LLNoticeRelaSchema tLLNoticeRelaSchema = new LLNoticeRelaSchema();
            tLLNoticeRelaSchema.setNoticeNo(CNNo);
            tLLNoticeRelaSchema.setSubRptNo(evNo);
            tmpMap.put(tLLNoticeRelaSchema, op);
        }



        this.mLLSubReportSchema.setSubRptNo(evNo);
        this.mLLSubReportSchema.setMakeDate(cDate);
        this.mLLSubReportSchema.setMakeTime(cTime);
        this.mLLSubReportSchema.setOperator(this.mGlobalInput.Operator);
        this.mLLSubReportSchema.setMngCom(this.mGlobalInput.ManageCom);
        this.mLLSubReportSchema.setModifyDate(cDate);
        this.mLLSubReportSchema.setModifyTime(cTime);

        tmpMap.put(this.mLLSubReportSchema, op);
        this.mResult.add(this.mLLSubReportSchema);
        return tmpMap;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData()
    {
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
      System.out.println("-Star GetInput-");
        this.mLLSubReportSchema=(LLSubReportSchema) cInputData.
                                          getObjectByObjectName(
                "LLSubReportSchema", 0);
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData) cInputData.
                        getObjectByObjectName("TransferData", 0);

        //可以为空
        this.mLLAskRelaSet = (LLAskRelaSet) cInputData.
                             getObjectByObjectName(
                "LLAskRelaSet", 0);
        this.mLLNoticeRelaSet = (LLNoticeRelaSet) cInputData.
                                getObjectByObjectName(
                "LLNoticeRelaSet", 0);
        mLLCaseRelaSet = ( LLCaseRelaSet ) cInputData.getObjectByObjectName("LLCaseRelaSet",0);
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        LLSubReportDB tLLSubReportDB = new LLSubReportDB();
        tLLSubReportDB.setSchema(this.mLLSubReportSchema);
        //如果有需要处理的错误，则返回
        if (tLLSubReportDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLLSubReportDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLSubReportBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
//            this.mInputData.clear();
//            this.mInputData.add(this.mLLSubReportSchema);
//
//            mResult.clear();
//            mResult.add(this.mLLSubReportSchema);
//            String op = "INSERT";
//            if ("INSERT||MAIN".equals(this.mOperate))
//            {
//                op = "DELETE&INSERT";
//            }
//            else
//            if ("DELETE||MAIN".equals(this.mOperate))
//            {
//                op = "DELETE";
//            }
//            if ("UPDATE||MAIN".equals(this.mOperate))
//            {
//                op = "UPDATE";
//            }
//
//            //加入结果集
//            MMap tmpMap = new MMap();
//            tmpMap.put(this.mLLSubReportSchema, op);
//            this.mResult.add(tmpMap);
//            this.mResult.add(this.mLLSubReportSchema);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLSubReportBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
