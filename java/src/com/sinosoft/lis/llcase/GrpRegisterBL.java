package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.LLClaimDetailDBSet;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.CommonBL;

public class GrpRegisterBL {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();
  private VData mResult = new VData();

  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  private MMap map = new MMap();

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();

  /** 数据操作字符串 */
  private String mOperate;

  /** 业务处理相关变量 */
  private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
  private LLRegisterSchema aLLRegisterSchema = new LLRegisterSchema();
  
  private boolean checkSocial=false;
  private String mSocialUser ;

//private LDDrugSet mLDDrugSet=new LDDrugSet();
  public GrpRegisterBL() {
  }

  public static void main(String[] args) {
      String tLimit = PubFun.getNoLimit("86210000");
      String RGTNO = PubFun1.CreateMaxNo("CASENO", tLimit);
      VData tno = new VData();
      tno.add(0,12+"");
      tno.add(1,RGTNO);
      tno.add(2,"MAX");
      PubFun1.CreateMaxNo("CaseNo",tLimit,tno);
      System.out.println("getInputData start......");
  }

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
    {
      return false;
    }
    else
    {
        System.out.println("getinputdata success!");
    }

    if(!checkData())
    {
        return false;
    }
    //进行业务处理
    if (!dealData()) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OLDDrugBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据处理失败OLDDrugBL-->dealData!";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("wwwwmOperate:"+this.mOperate+"++++"+mGlobalInput.ManageCom.substring(0, 4));
    if(this.mOperate.equals("UPDATE||MAIN"))
    {
    if (!uwCheck()) {
        return false;
    }
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;

    else {
      System.out.println("Start GrpRegisterBL Submit...");
      PubSubmit tPubSubmit = new PubSubmit();
      if (!tPubSubmit.submitData(mInputData, mOperate))
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        CError tError = new CError();
        tError.moduleName = "GrpRegisterBL";
        tError.functionName = "GrpRegisterBL";
        tError.errorMessage = "数据提交失败!";
        this.mErrors.addOneError(tError);
        return false;
      }
      System.out.println("End GrpRegisterBL Submit...");
    }
    mInputData = null;
    return true;
  }

  /**
   * 
   * 校验数据，错误返回false,否则返回true
   */
  private boolean checkData() {
	  System.out.println("团体批次号为："+this.mLLRegisterSchema.getRgtNo());
	  String sql = "";
	  if("INSERT||MAIN".equals(this.mOperate))
	  {
		  sql = "select CHECKGRPCONT('"+this.mLLRegisterSchema.getRgtObjNo()+"') from dual where 1=1  ";
	  }else{
		  sql = "select CHECKGRPCONT((select RgtObjNo from LLRegister where 1=1 and rgtno ='"+mLLRegisterSchema.getRgtNo()+"' )) from dual where 1=1  ";
		  
	  }
	  
	  ExeSQL tExeSQL = new ExeSQL();
	  String tResult = tExeSQL.getOneValue(sql);
	  if(tResult!=null&&tResult.equals("Y"))
	  {	
		  String checkSql = "Select '1' From LLSocialClaimUser where UserCode = '"+this.mGlobalInput.Operator+"' and StateFlag = '1' and HandleFlag ='0'";
		  String tOper_Result = tExeSQL.getOneValue(checkSql);
		  if(tOper_Result!=null&&"1".equals(tOper_Result))
		  {
			  return true;
			  
		  }else{
			  
			  CError tError = new CError();
		      tError.moduleName = "GrpRegisterBL";
		      tError.functionName = "checkData";
		      tError.errorMessage = "您不具有社保案件理赔操作权限！";
		      this.mErrors.addOneError(tError);
			  return false;  
		  }
		  
	  }else{
		  
		  LLClaimUserDB tLLClaimUserDB=new LLClaimUserDB();
		  tLLClaimUserDB.setUserCode(mGlobalInput.Operator);//校验操作人是否具有理赔权限
		  if(!tLLClaimUserDB.getInfo())
		  {
			    CError tError = new CError();
		        tError.moduleName = "GrpRegisterBL";
		        tError.functionName = "checkData";
		        tError.errorMessage = "您不具有理赔权限！";
		        this.mErrors.addOneError(tError);
			    return false;
		  }
	  }

	  return true;
  }
  
  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
      System.out.println("===========mOperate=========="+mOperate);
    if (this.mOperate.equals("INSERT||MAIN"))
    {
        String tGrpContNo = mLLRegisterSchema.getRgtObjNo();
//        if(CommonBL.afterMJCalculate(tGrpContNo)){
//            CError tError = new CError();
//            tError.moduleName = "GrpRegisterBL";
//            tError.functionName = "dealData";
//            tError.errorMessage = "保单管理正在对该团单进行满期结算、追加保费、部分领取或账户分配操作，请先通知保全撤销相关操作，再进行理赔!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
        String result = LLCaseCommon.checkGrp(tGrpContNo);
        if(!"".equals(result)){
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "工单号为"+result+"的保单管理现正对该团单进行保全操作，请先通知保全撤销相关操作，再进行理赔!";
            this.mErrors.addOneError(tError);
            return false;
        }
      String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
      System.out.println("管理机构代码是 : "+tLimit);
      String RGTNO = PubFun1.CreateMaxNo("RGTNO", tLimit);
      VData tno = new VData();
      tno.add(0,mLLRegisterSchema.getAppPeoples()+"");
      tno.add(1,RGTNO);
      tno.add(2,"MAX");
      PubFun1.CreateMaxNo("CaseNo",tLimit,tno);
      System.out.println("getInputData start......");
      mLLRegisterSchema.setRgtNo(RGTNO);
      mLLRegisterSchema.setRgtState("01");
      mLLRegisterSchema.setHandler1(mGlobalInput.Operator);
      mLLRegisterSchema.setApplyerType("0");
      mLLRegisterSchema.setRgtDate(PubFun.getCurrentDate());
      mLLRegisterSchema.setMngCom(mGlobalInput.ManageCom);
      mLLRegisterSchema.setOperator(mGlobalInput.Operator);
      mLLRegisterSchema.setMakeDate(PubFun.getCurrentDate());
      mLLRegisterSchema.setMakeTime(PubFun.getCurrentTime());
      mLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
      mLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());
      map.put(mLLRegisterSchema, "INSERT");
    }
    if (this.mOperate.equals("UPDATE||MAIN")) {
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        if(!tLLRegisterDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "团体立案信息查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLLRegisterDB.getRgtState().equals("02"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "团体立案已经全部受理申请完毕!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLLRegisterDB.getRgtState().equals("03"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "团体立案已经全部结案完毕!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLLRegisterDB.getRgtState().equals("04"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "团体立案已经全部给付确认完毕!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!tLLRegisterDB.getRgtState().equals("01") && !tLLRegisterDB.getRgtState().equals("07"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "团体立案状态错误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String countSql = "select count(caseno) from llcase where rgtno ='"
                          +mLLRegisterSchema.getRgtNo()+"' and rgtstate not in "
                          +"('09','11','12','14')";
        ExeSQL texesql = new ExeSQL();
        String casecount = texesql.getOneValue(countSql);
        //点击“团体申请完毕”时将批次下所有理赔号对应理算日期置为操作日当日
        //GY 2012-12-19
        String sql = "select * from llcase where rgtno='"+mLLRegisterSchema.getRgtNo()+"'";
        LLCaseSet mLLCaseSet = new LLCaseSet();
        LLCaseSet mmLLCaseSet = new LLCaseSet();
        LLCaseDB mLLCaseDB = new LLCaseDB();
        mLLCaseSet = mLLCaseDB.executeQuery(sql);
        
//      modify by Houyd	2014-4-25 取消社保案件的案件分配
        // add new 2014-4-12
//        if(!checkSocial()){ return false;}
        
        
        for (int k=1;k<=mLLCaseSet.size();k++){
            LLCaseSchema mLLCaseShema = mLLCaseSet.get(k);    
            // add new  2014-4-12 
//            if(checkSocial)
//            {
//              mLLCaseShema.setHandler(this.mSocialUser);
//              mLLCaseShema.setDealer(this.mSocialUser);
//              mLLCaseShema.setRigister(this.mSocialUser);
//            }
            //add end
            mLLCaseShema.setClaimCalDate(PubFun.getCurrentDate());
            mLLCaseShema.setModifyDate(PubFun.getCurrentDate());
            mLLCaseShema.setModifyTime(PubFun.getCurrentTime());
            mmLLCaseSet.add(mLLCaseShema);
        }
        map.put(mmLLCaseSet,"UPDATE");
        
        
        int count = Integer.parseInt(casecount);
        if(count>0)
            mLLRegisterSchema.setRgtState("02");
        else
            mLLRegisterSchema.setRgtState("03");
        LLRegisterSchema tLLRegisterSchema = tLLRegisterDB.getSchema();
        tLLRegisterSchema.setRgtState(mLLRegisterSchema.getRgtState());
        tLLRegisterSchema.setAppPeoples(mLLRegisterSchema.getAppPeoples());
        map.put(tLLRegisterSchema, "UPDATE");
    }
    /*
     * 此处为未导入时，修改团体立案信息的处理
     */
    if(this.mOperate.equals("UPDATE||REGISTER")){
  	  	//#2169 批次导入前后台数据同步问题方案
  	  	if(!LLCaseCommon.checkGrpRgtState(aLLRegisterSchema.getRgtNo())){
  	  		//@@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "批次号:"+aLLRegisterSchema.getRgtNo()+"还在处理中，请稍候再处理该批次的案件!";
            this.mErrors.addOneError(tError);
            return false;
  	  	}
        if (!aLLRegisterSchema.getRgtState().equals("01") && !aLLRegisterSchema.getRgtState().equals("07"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "该状态下不允许修改团体批次信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!aLLRegisterSchema.getOperator().equals(mGlobalInput.Operator))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "您不是该批次的原始录入人员，无权修改该批次信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!aLLRegisterSchema.getCustomerNo().equals(mLLRegisterSchema.getCustomerNo())||
            !aLLRegisterSchema.getRgtObjNo().equals(mLLRegisterSchema.getRgtObjNo()))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "不允许修改批次下团体客户信息及保单信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if(aLLRegisterSchema.getAppPeoples()!=mLLRegisterSchema.getAppPeoples()){
            CError.buildErr(this,"已经按第一次操作确认时的申请人数预留了理赔号，因此不能修改申请人数！");
            return false;
        }
        LLCaseDB tLLCaseDB = new LLCaseDB();
        LLCaseSet tLLCaseSet = new LLCaseSet();
        tLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        tLLCaseSet = tLLCaseDB.query();
        if(mLLRegisterSchema.getAppPeoples()<tLLCaseSet.size()){
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "该案件下已受理了"+tLLCaseSet.size()
                                  +"人，申请人数不能小于该值！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if(mLLRegisterSchema.getAppPeoples()==tLLCaseSet.size()){
            mLLRegisterSchema.setRgtState("02");
        }
        mLLRegisterSchema.setRgtState(aLLRegisterSchema.getRgtState());
        mLLRegisterSchema.setHandler1(aLLRegisterSchema.getHandler1());
        mLLRegisterSchema.setApplyerType(aLLRegisterSchema.getApplyerType()); 
        mLLRegisterSchema.setRgtDate(aLLRegisterSchema.getRgtDate());
        mLLRegisterSchema.setMngCom(aLLRegisterSchema.getMngCom());
        mLLRegisterSchema.setOperator(aLLRegisterSchema.getOperator());
        mLLRegisterSchema.setMakeDate(aLLRegisterSchema.getMakeDate());
        mLLRegisterSchema.setMakeTime(aLLRegisterSchema.getMakeTime());
        mLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
        mLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLLRegisterSchema, "UPDATE");

    }
    if (this.mOperate.equals("DELETE||MAIN")) {
      map.put(mLLRegisterSchema, "DELETE");
    }

    if (this.mOperate.equals("UPDATE||RgtPayMode")){
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mLLRegisterSchema.getRgtNo());

        if (!tLLRegisterDB.getInfo()){
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "团体批次查询失败！";
            this.mErrors.addOneError(tError);
            return false;
        }

        tLLRegisterSchema = tLLRegisterDB.getSchema();
        if(!tLLRegisterSchema.getHandler1().equals(mGlobalInput.Operator)){
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "您无权限修改！";
            this.mErrors.addOneError(tError);
            return false;
        }

        String tSQL = "SELECT * FROM llcase WHERE rgtno = '"+tLLRegisterSchema.getRgtNo()+"' AND rgtstate NOT IN ('11','12','14') WITH UR";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tSQL);
        if (tSSRS.getMaxRow()<=0){
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "批次下案件已经全部给付完毕，不予修改！";
            this.mErrors.addOneError(tError);
            return false;
        }
        tSQL = "SELECT * FROM llcase WHERE rgtno = '"+tLLRegisterSchema.getRgtNo()+"' AND rgtstate IN ('11','12') WITH UR";
        tSSRS = tExeSQL.execSQL(tSQL);
        if (("3".equals(mLLRegisterSchema.getTogetherFlag()) || "4"
					.equals(mLLRegisterSchema.getTogetherFlag()))
					&& tSSRS.getMaxRow() > 0) {
				CError tError = new CError();
				tError.moduleName = "GrpRegisterBL";
				tError.functionName = "dealData";
				tError.errorMessage = "批次下有案件已经给付完毕，不能修改为全部统一给付！";
				this.mErrors.addOneError(tError);
				return false;
			}
        if (!mLLRegisterSchema.getTogetherFlag().equals("1")){
            tLLRegisterSchema.setCaseGetMode(mLLRegisterSchema.getCaseGetMode());
            tLLRegisterSchema.setBankCode(mLLRegisterSchema.getBankCode());
            tLLRegisterSchema.setBankAccNo(mLLRegisterSchema.getBankAccNo());
            tLLRegisterSchema.setAccName(mLLRegisterSchema.getAccName());
        }else{
            LLCaseSet tLLCaseSet = new LLCaseSet();
            LLCaseSet aLLCaseSet = new LLCaseSet();
            LLCaseDB tLLCaseDB = new LLCaseDB();
            tSQL = "SELECT * FROM llcase WHERE rgtno = '"+tLLRegisterSchema.getRgtNo()+"' AND rgtstate NOT IN ('11','12','14') WITH UR";
            tLLCaseSet = tLLCaseDB.executeQuery(tSQL);

            for (int i=1;i<=tLLCaseSet.size();i++){
                LLCaseSchema tLLCaseShema = tLLCaseSet.get(i);
                tSQL = "SELECT Bankcode,Bankaccno,Accname FROM lcinsured WHERE insuredno='"+tLLCaseShema.getCustomerNo()+"' and Bankcode is not null and Bankaccno is not null and Accname is not null ORDER BY ModifyDate DESC,ModifyTime DESC FETCH FIRST 1 ROWS ONLY WITH UR";
                tSSRS=tExeSQL.execSQL(tSQL);
                tLLCaseShema.setCaseGetMode(mLLRegisterSchema.getCaseGetMode());
                if(tSSRS.getMaxRow()>0){
                tLLCaseShema.setBankCode(tSSRS.GetText(1,1));
                tLLCaseShema.setBankAccNo(tSSRS.GetText(1,2));
                tLLCaseShema.setAccName(tSSRS.GetText(1,3));
                }
                tLLCaseShema.setModifyDate(PubFun.getCurrentDate());
                tLLCaseShema.setMakeTime(PubFun.getCurrentTime());
                aLLCaseSet.add(tLLCaseShema);
                
            }
            map.put(aLLCaseSet,"UPDATE");
        }
        tLLRegisterSchema.setCaseGetMode(mLLRegisterSchema.getCaseGetMode());
        tLLRegisterSchema.setTogetherFlag(mLLRegisterSchema.getTogetherFlag());
        tLLRegisterSchema.setOperator(mGlobalInput.Operator);
        tLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
        tLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLLRegisterSchema, "UPDATE");
    }
    return true;
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean updateData() {
    return true;
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean deleteData() {
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
      System.out.println("getInputData start......");
    this.mLLRegisterSchema.setSchema( (LLRegisterSchema) cInputData.
                                 getObjectByObjectName("LLRegisterSchema", 0));
    this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    if(mLLRegisterSchema.getRgtNo()!=null&&this.mOperate.equals("INSERT||MAIN")){
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        if(tLLRegisterDB.getInfo()){
            this.mOperate = "UPDATE||REGISTER";
            aLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
        }
    }
    if (mGlobalInput.ManageCom.trim().length() == 2)
        mGlobalInput.ManageCom = mGlobalInput.ManageCom.trim() + "000000";
    if (mGlobalInput.ManageCom.trim().length() == 4)
        mGlobalInput.ManageCom = mGlobalInput.ManageCom.trim() + "0000";
    return true;
  }



  private boolean prepareOutputData() {
    try {
      this.mInputData.clear();
      this.mInputData.add(this.mLLRegisterSchema);
      mInputData.add(map);
      mResult.clear();
      mResult.add(this.mLLRegisterSchema);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LDDrugBL";
      tError.functionName = "prepareData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  public VData getResult() {
    return this.mResult;
  }
  
  /**
   * 调用核赔规则
   * @return boolean
   */
  private boolean uwCheck() {

      String tsql="";
      int tErrNO=0;
      String tErr=null;
      ExeSQL exesql = new ExeSQL();
      SSRS ssrs = new SSRS();
      System.out.println(this.mLLRegisterSchema.getRgtNo());
      if(this.mLLRegisterSchema.getRgtNo()!=null&&!("".equals(this.mLLRegisterSchema.getRgtNo()))){
       tsql="select distinct customerno from llcase where rgtno='"+this.mLLRegisterSchema.getRgtNo()+"'  with ur";
       System.out.println("GrpRegisterBL中通过批量号获取客户号："+tsql);
       ssrs = exesql.execSQL(tsql);
      }
      
      
      System.out.println(ssrs.getMaxRow());
      for(int i=1;i<=ssrs.getMaxRow();i++){
    	  VData tVData = new VData();
    	  LLUWCheckBL tLLUWCheckBL = new LLUWCheckBL();
    	  LLCaseSchema tLLCaseSchema=new LLCaseSchema();
    	  LLCaseSet tLLCaseSet=new LLCaseSet();
    	  LLCaseRelaSet tLLCaseRelaSet=new LLCaseRelaSet();
    	  LLSubReportSet tLLSubReportSet=new LLSubReportSet();
    	  LLClaimDetailSet tLLClaimDetailSet=new LLClaimDetailSet();
    	  LLCaseDB tLLCaseDB=new LLCaseDB();
    	  tLLCaseDB.setCustomerNo(ssrs.GetText(i, 1));
    	  tLLCaseDB.setRgtNo(this.mLLRegisterSchema.getRgtNo());
    	  tLLCaseSet=tLLCaseDB.query();
    	  String casenos="";
    	  if(tLLCaseSet!=null&&tLLCaseSet.size()>0)
    	  {
    	  //tLLCaseSchema=tLLCaseSet.get(1);
    	  for(int a=1;a<=tLLCaseSet.size();a++){
    		  System.out.println("GrpRegisterBL中通过客户号获取理赔业务表信息Start！");
    		  casenos=casenos+","+tLLCaseSet.get(a).getCaseNo();
    		  LLClaimDetailDB tLLClaimDetailDB=new LLClaimDetailDB();
    		  tLLClaimDetailDB.setCaseNo(tLLCaseSet.get(a).getCaseNo());
    		  tLLClaimDetailSet.add(tLLClaimDetailDB.query());
    		  LLCaseRelaDB tLLCaseRelaDB=new LLCaseRelaDB();
    		  tLLCaseRelaDB.setCaseNo(tLLCaseSet.get(a).getCaseNo());
    		  tLLCaseRelaSet.add(tLLCaseRelaDB.query());
    		  System.out.println("GrpRegisterBL中通过客户号获取理赔业务表信息END！");
    	  }
    	  if(tLLCaseRelaSet!=null&&tLLCaseRelaSet.size()>0){
    		  for(int b=1;b<=tLLCaseRelaSet.size();b++){
    			  LLSubReportDB tLLSubReportDB=new LLSubReportDB();
    			  tLLSubReportDB.setSubRptNo(tLLCaseRelaSet.get(b).getSubRptNo());
    			  tLLSubReportSet.add(tLLSubReportDB.query());
    		  }
    		  System.out.println("GrpRegisterBL中获取理赔LLSubReport表信息END！");
    	  }
    	  }
      
    	  tVData.addElement(tLLCaseSchema);
    	  tVData.addElement(tLLCaseRelaSet);
    	  tVData.addElement(tLLSubReportSet);
    	  tVData.addElement(tLLClaimDetailSet);
    	  tVData.addElement(mGlobalInput);
          if (!tLLUWCheckBL.submitData(tVData,"")) {
        	  tErrNO=tErrNO+1;
        	  if(tErr==null){
        		  tErr=String.valueOf(tErrNO)+"、客户号为"+ssrs.GetText(i, 1)+"的客户，"+tLLUWCheckBL.getErrors().getFirstError()+"(本次批量导入该客户所属案件号"+casenos+");";  
        	  }else{ 
        		  tErr=tErr+String.valueOf(tErrNO)+"、客户号为"+ssrs.GetText(i, 1)+"的客户，"+tLLUWCheckBL.getErrors().getFirstError()+"(本次批量导入该客户所属案件号"+casenos+");";
             // CError.buildErr(this,"客户号为"+ssrs.GetText(i, 1)+"的客户，"+tLLUWCheckBL.getErrors().getFirstError()+"(本次批量导入该客户所属案件号"+casenos+")");
        	  }
          }
         
      }
      if(tErr!=null){
    	  CError.buildErr(this,tErr);
    	  return false;
      }
      return true;
  }
  
  //校验团单是否为社保保单，如果是获取对用社保业务操作人员
  private boolean checkSocial()
  {
        System.out.println("团体批次号为："+this.mLLRegisterSchema.getRgtNo());
	    String check_sql = "select CHECKGRPCONT((select RgtObjNo from LLRegister where 1=1 and rgtno ='"+mLLRegisterSchema.getRgtNo()+"' )) from dual where 1=1  ";
	    ExeSQL tExeSQL = new ExeSQL();
	    String tResult = tExeSQL.getOneValue(check_sql);
	    if(tResult!=null&&tResult.equals("Y"))
	    {  
	    	String temp_ManageCom = this.mGlobalInput.ManageCom;
	    	LLCaseCommon tLLCaseCommon = new LLCaseCommon();
	    	mSocialUser = tLLCaseCommon.SocialSecurityChooseUser(temp_ManageCom);
	        if(mSocialUser==null||"".equals(mSocialUser))
	        {
	        	 // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "GrpRegisterBL";
	            tError.functionName = "dealData";
	            tError.errorMessage = "未找到对应社保业务理赔人员!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
	    	checkSocial = true;
	    }

	  return true;
  }
}
