package com.sinosoft.lis.llcase;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Date;


//import utils.system;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LLCaseSZSchema;
import com.sinosoft.lis.vschema.LLCaseSZSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * 提数FTP服务器
 * 根据SQL语句查询结果，将结果保存在Excel中，并上传FTP服务器。
 * 
 * 目前：
 * 其中团单提数每个工作表6万条数据，即最大数据量为：6W+6W+6W+...
 * 其他3个提数最大数据量为：65536条。 
 * 
 * 入口：dealData()
 * 
 * @author Administrator
 * 
 */
public class LLSZLPExcelTransFtp {
	public CErrors mErrors = new CErrors();
	private MMap map = new MMap();
	private MMap rmap = new MMap();
	private ExeSQL tExeSQL=new ExeSQL();
	/*
	 * 开始处理
	 */
	public boolean dealData(){
		//创建每日文件夹，并用于修改数据库配置，每日下标从1开始。
		String curDate=PubFun.getCurrentDate();
		if(!makeDirectory(curDate)){
			return false;
		}
		//理赔进度
		String staURL=getSagentData("ClaimStatus",curDate);
		if("".equals(staURL)){
			System.out.println("核心没有理赔提数ClaimStatus文件夹！");
			return false;
		}
		//获取数据
		int n=getIndex("ClaimStatus");
		if(!writerToExcCS("ClaimStatus",staURL,n)) {
			return false;
		}
		if(!transToFtp("ClaimStatus",staURL,n,curDate)) {
			return false;
		}
		
		
		//上传成功后在LLCASESZ表插入已存数据，修改数据库配置，改变excel下标。
		//理赔结果
		String resURL=getSagentData("ClaimResault",curDate);
		if("".equals(resURL)){
			System.out.println("核心没有理赔提数ClaimResault文件夹！");
			return false;
		}
		int r=getIndex("ClaimResault");
		if(!writerToExcCR("ClaimResault",resURL,r)) {
			return false;
		}
		//上传FTP
		if(!transToFtp("ClaimResault",resURL,r,curDate)) {
			return false;
		}
		
		//上传成功后在LLCASESZ表插入已存数据，修改数据库配置，改变excel下标。
		
		
		return true;
	
	
	}
	/**
	 * 执行批处理
	 */
	public String getSagentData(String ttype,String curDate) {
		// 查询根路径
		String tSQL = "select sysvarvalue from ldsysvar where sysvar = 'UIRoot' with ur";
		String tRootPath = new ExeSQL().getOneValue(tSQL);
//		tRootPath="E:/wdx/66/";
		// 提数目录
		tSQL = "select codename from ldcode where codetype='LLWXClaim' and code='ExcelPath'";
		String tTempPath =  new ExeSQL().getOneValue(tSQL);
		
		String mURL = tRootPath + tTempPath;
		
		// 创建归档文件夹
		if (!save(mURL,ttype,curDate)) {
			System.out.println("创建归档文件夹失败！");
			return "";
		}
		
		return getSavePath(mURL,ttype,curDate);
		
	}
	
	/**
	 * 根据格式输出日期
	 * @param pDateFormat
	 * @return
	 */
	private String getCurrentDate(String pDateFormat) {
		return new SimpleDateFormat(pDateFormat).format(new Date());
	}
	/**
	 * 创建文件夹
	 * @param cRootPath
	 * @return
	 */
	public boolean save(String cRootPath,String ctype,String curDate){
        StringBuffer mFilePath = new StringBuffer();

        mFilePath.append(getSavePath(cRootPath,ctype,curDate));

        File mFileDir = new File(mFilePath.toString());
        if (!mFileDir.exists()){
            if (!mFileDir.mkdirs()){
                System.err.println("创建目录[" + mFilePath.toString() + "]失败！" + mFileDir.getPath());
                return false;
            }
        }
        return true;
        
    }
	/**
	 * 在配置表中获取文件下标，返回n。
	 */
	private int getIndex(String type){
		if("ClaimStatus".equals(type)){
			String mSQL="select code+1 from ldcode where codetype='llcaseszsta' ";
			Double f = Double.valueOf(new ExeSQL().getOneValue(mSQL));
			int ClaimStatusNo=(int)Math.ceil(f);
			String tmSQL="update ldcode set code=("+mSQL+") where codetype='llcaseszsta'";
			tExeSQL.execUpdateSQL(tmSQL);
			return ClaimStatusNo;
		}else{
			String mSQL="select code+1 from ldcode where codetype='llcaseszres' ";
			Double f = Double.valueOf(new ExeSQL().getOneValue(mSQL));
			int ClaimResaultNo=(int)Math.ceil(f);
			String tmSQL="update ldcode set code=("+mSQL+") where codetype='llcaseszres'";
			tExeSQL.execUpdateSQL(tmSQL);
			return ClaimResaultNo;
		}
	}
	/**
	 * 通过SQL语句查询结果，并写入EXCEL文件，理赔进度与结果分开。
	 */
	private boolean writerToExcCS(String type,String strURL,int n){
		// 用于查询大量数据。不要用ExeSql execsql()方法，这个只能查1W以内的数据量  #3366大数据量优化
		RSWrapper rsWrapper;
		
		//需从配置表中获取，每次提交成功后自增1，第二天凌晨恢复为1.
		
		
		// 用于查询时，循环的次数标志
		int k;
		// 存放每次循环时的最大行数。但实际jxl一个sheet只能存65536行 --数组定长100  2017-05-19
		int[] maxRows = new int[100]; 
		
		try {
					
			String allCaseNo =" select a.caseno from llcase a  where  "
			+" a.mngcom like '8695%' "
			+" and a.modifydate >= current date - 30 days "
			+" and a.rgtstate<>'14'  "
			+" and not exists ( "
			+" select 1 from llcasesz  where uploadtype='clmsta' and makedate>=current date -30 days  "
			+" and a.caseno=caseno  and a.rgtstate=rgtstate) " 
			+" fetch first 5000 rows only "
			+" with ur ";
			
			SSRS allCase=tExeSQL.execSQL(allCaseNo);
			if(allCase!=null && allCase.getMaxRow()>0){
				String allCaseStrs="";
				for (int i = 1; i <= allCase.getMaxRow(); i++) {
					String allCaseStr="'"+allCase.GetText(i, 1).trim()+"'";
					if(i==1){
						allCaseStrs=allCaseStr;
					}else{
						allCaseStrs=allCaseStr+","+allCaseStrs;
					}
					
				}
			
			
			// Excel工作簿的名子 -- sheet名
			String biaoqian = "";
			k = 1;// 每个文件都从1开始
					
					// 保险理赔进度 --增加3个字段 2017-05-19   ***2017-5-26*** 提数口径变更  #3366 star
					String mExcelFileName = type+n+".xls";
					WritableWorkbook book = Workbook.createWorkbook(new File(strURL + mExcelFileName));
					String mSQL=" SELECT a.caseno 案件号, "
						+"       a.rgtstate, "
						+"       a.mngcom, "
						+"       a.customerno 被保险人客户号, "
						+"       a.customername 被保险人姓名, "
						+"       (SELECT codename "
						+"          FROM ldcode "
						+"         WHERE codetype = 'idtype' "
						+"           AND code = a.idtype) 被保险人证件类型, "
						+"       a.idno 被保人身份证号, "
						+"       b.RgtantName 申请人姓名, "
						+"       (SELECT codename "
						+"          FROM ldcode "
						+"         WHERE codetype = 'idtype' "
						+"           AND code = b.idtype) 申请人证件类型, "
						+"       b.idno 申请人证件号码, "
						+"       b.RgtantPhone 申请人手机, "
						+"       a.rgtdate 受理时间, "
						+"       (SELECT makedate "
						+"          FROM llfeemain "
						+"         WHERE caseno = a.caseno FETCH first 1 rows ONLY) 录入时间, "
						+"       a.endcasedate 结案时间, "
						+"       (SELECT confdate "
						+"          FROM ljaget "
						+"         WHERE (otherno = a.caseno OR otherno = a.rgtno) "
						+"           AND othernotype = '5' FETCH first 1 rows ONLY) 给付时间, "
						+"       (SELECT codename "
						+"          FROM ldcode "
						+"         WHERE 1 = 1 "
						+"           AND codetype = 'llrgtstate' "
						+"           AND code = a.rgtstate) 案件状态 "
						+"  FROM llcase a, llregister b "
						+" WHERE a.rgtno = b.rgtno "
						+"   AND a.mngcom LIKE '8695%' "
						+"   AND 1 = 1 "
						+"   and a.caseno in ("+allCaseStrs+") "
						+"   AND (a.rgtstate not in ( '14' ,'12' ) "
						+"   or (a.rgtstate = '12' and EXISTS "
						+"        (SELECT 1 "
						+"           FROM ljaget "
						+"          WHERE (otherno = a.caseno OR otherno = a.rgtno) "
						+"            AND othernotype = '5' "
						+"            AND confdate >= CURRENT DATE - 2 days "
//						+"            and maketime >= current time - 20 minutes "
						+"          "
						+"         )) ) "
						+"       "
						+"   AND NOT EXISTS (SELECT 1 "
						+"          FROM llcasesz "
						+"         WHERE caseno = a.caseno "
						+"           AND uploadtype = 'clmsta' "
						+"           AND rgtstate = a.rgtstate) fetch first 5000 rows only with ur ";
						
			rsWrapper = new RSWrapper();
			if (!rsWrapper.prepareData(null, mSQL)) {
			    System.out.println("保险理赔进度__数据准备失败! ");
			    return false;
			}
			biaoqian = "保险理赔进度";
			WritableSheet sheet = book.createSheet(biaoqian, 1);
					
			Label label1 = new Label(0, 0, "案件号");
			Label label2 = new Label(1, 0, "案件状态");
			
			Label label3 = new Label(2, 0, "被保险人客户号");
			Label label4 = new Label(3, 0, "被保险人姓名");
			Label label5 = new Label(4, 0, "被保险人证件类型");
			Label label6 = new Label(5, 0, "被保人证件号码");
			Label label7 = new Label(6, 0, "申请人姓名");
			Label label8 = new Label(7, 0, "申请人证件类型");
			Label label9 = new Label(8, 0, "申请人证件号码");
			Label label10 = new Label(9, 0, "申请人手机");
			Label label11 = new Label(10, 0, "受理时间");
			Label label12 = new Label(11, 0, "录入时间");
			Label label13 = new Label(12, 0, "结案时间");
			Label label14 = new Label(13, 0, "给付时间");
			
			

			sheet.addCell(label1);
			sheet.addCell(label2);
			sheet.addCell(label3);
			sheet.addCell(label4);
			sheet.addCell(label5);
			sheet.addCell(label6);
			sheet.addCell(label7);
			sheet.addCell(label8);
			sheet.addCell(label9);
			sheet.addCell(label10);
			sheet.addCell(label11);
			sheet.addCell(label12);
			sheet.addCell(label13);
			sheet.addCell(label14);
					
			k = 1;
			SSRS tSSRS =null;
			LLCaseSZSet mLLCaseSZSet = new LLCaseSZSet();
			do {
				tSSRS = rsWrapper.getSSRS();
			    int maxRow = tSSRS.getMaxRow();
			    maxRows[k] = maxRow;
			    for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			        int j = i + maxRows[1]*(k-1);
					label1 = new Label(0, j, tSSRS.GetText(i, 1));
					label2 = new Label(1, j, tSSRS.GetText(i, 16));
					
					label3 = new Label(2, j, tSSRS.GetText(i, 4));
					label4 = new Label(3, j, tSSRS.GetText(i, 5));
					label5 = new Label(4, j, tSSRS.GetText(i, 6));
					label6 = new Label(5, j, tSSRS.GetText(i, 7));
					label7 = new Label(6, j, tSSRS.GetText(i, 8));
					label8 = new Label(7, j, tSSRS.GetText(i, 9));
					label9 = new Label(8, j, tSSRS.GetText(i, 10));
					label10 = new Label(9, j, tSSRS.GetText(i, 11));
					label11 = new Label(10, j, tSSRS.GetText(i, 12));
					label12 = new Label(11, j, tSSRS.GetText(i, 13));
					label13 = new Label(12, j, tSSRS.GetText(i, 14));
					label14 = new Label(13, j, tSSRS.GetText(i, 15));
					
					
					if(i<tSSRS.getMaxRow()){
						if(!tSSRS.GetText(i, 1).equals(tSSRS.GetText(i+1, 1))){
							mLLCaseSZSet.add(getInputData(tSSRS,i,"clmsta"));
						}
					}else{
						mLLCaseSZSet.add(getInputData(tSSRS,i,"clmsta"));
					}
					
					sheet.addCell(label1);
					sheet.addCell(label2);
					sheet.addCell(label3);
					sheet.addCell(label4);
					sheet.addCell(label5);
					sheet.addCell(label6);
					sheet.addCell(label7);
					sheet.addCell(label8);
					sheet.addCell(label9);
					sheet.addCell(label10);
					sheet.addCell(label11);
					sheet.addCell(label12);
					sheet.addCell(label13);
					sheet.addCell(label14);
			        }
			     k = k + 1;
				} while (tSSRS != null && tSSRS.MaxRow > 0);
					book.write();
					book.close();
					map.put(mLLCaseSZSet, "INSERT");
					
			}
	} catch (IOException e) {
		e.printStackTrace();
		return false;
	} catch (RowsExceededException e) {
		e.printStackTrace();
		return false;
	} catch (WriteException e) {
		e.printStackTrace();
		return false;
	}
	
	return true;
}
	
	private boolean writerToExcCR(String type,String resURL,int r){
		// 用于查询大量数据。不要用ExeSql execsql()方法，这个只能查1W以内的数据量  #3366大数据量优化
		RSWrapper rsWrapper;
		// 用于查询时，循环的次数标志
		int k;
		// 存放每次循环时的最大行数。但实际jxl一个sheet只能存65536行 --数组定长100  2017-05-19
		int[] maxRows = new int[100]; 
		try {
			
			
			String allCaseNo =" select a.caseno from llcase a  where  "
				+" a.mngcom like '8695%' "
				+" and a.modifydate >= current date - 30 days "
				+" and a.rgtstate='12'  "
				+" and exists (select 1  "
				+" from ljaget  "
				+" where (otherno = a.caseno or otherno = a.rgtno) "
				+" and confdate >= current date - 2 days ) "
				+" and not exists ( "
				+" select 1 from llcasesz  where uploadtype='clmres' and makedate>=current date -30 days  "
				+" and a.caseno=caseno  ) " 
				+" fetch first 5000 rows only "
				+" with ur ";
				
				SSRS allCase=tExeSQL.execSQL(allCaseNo);
				if(allCase!=null && allCase.getMaxRow()>0){
					String allCaseStrs="";
					for (int i = 1; i <= allCase.getMaxRow(); i++) {
						String allCaseStr="'"+allCase.GetText(i, 1).trim()+"'";
						if(i==1){
							allCaseStrs=allCaseStr;
						}else{
							allCaseStrs=allCaseStr+","+allCaseStrs;
						}
						
					}
			// Excel工作簿的名子 -- sheet名
			String biaoqian = "";
			k = 1;// 每个文件都从1开始
			
		String mExcelFileName = type+r+".xls";
		WritableWorkbook book = Workbook.createWorkbook(new File(resURL + mExcelFileName));
		String mSQL=" select a.caseno 案件号, "
		+"       a.rgtstate, "
		+"       a.mngcom, "
		+"       a.customerno 被保险人客户号, "
		+"       a.customername 被保险人姓名, "
		+"       (select codename "
		+"          from ldcode "
		+"         where codetype = 'idtype' "
		+"           and code = a.idtype) 被保险人证件类型, "
		+"       a.idno 被保人身份证号, "
		+"       (select RgtantName "
		+"          from llregister "
		+"         where rgtno = a.rgtno fetch first 1 rows only) 申请人姓名, "
		+"       (select codename "
		+"          from ldcode "
		+"         where codetype = 'idtype' "
		+"           and code in "
		+"               (select idtype "
		+"                  from llregister "
		+"                 where rgtno = a.rgtno fetch first 1 rows only)) 申请人证件类型, "
		+"       (select idno "
		+"          from llregister "
		+"         where rgtno = a.rgtno fetch first 1 rows only) 申请人证件号码, "
		+"       (select RgtantPhone "
		+"          from llregister "
		+"         where rgtno = a.rgtno fetch first 1 rows only) 申请人手机, "
		+"       (select AccDate "
		+"          from LLSubReport "
		+"         where subrptno in (select subrptno "
		+"                              from llcaserela "
		+"                             where caseno = a.caseno "
		+"                               and caserelano = m.caserelano) fetch "
		+"         first 1 rows only) 发生日期, "
		+"       (select HospStartDate "
		+"          from llfeemain "
		+"         where caseno = a.caseno fetch first 1 rows only) 入院日期, "
		+"       (select HospEndDate "
		+"          from llfeemain "
		+"         where caseno = a.caseno fetch first 1 rows only) 出院日期, "
		+"       (select accdesc "
		+"          from LLSubReport "
		+"         where subrptno in (select subrptno "
		+"                              from llcaserela "
		+"                             where caseno = a.caseno "
		+"                               and caserelano = m.caserelano) fetch "
		+"         first 1 rows only) 事件信息, "
		+"       sum(m.TabFeeMoney) 账单金额, "
		+"       (select riskname "
		+"          from lmriskapp "
		+"         where riskcode = m.riskcode fetch first 1 rows only) 赔付险种, "
		+"       m.riskcode 赔付险种代码, "
		+"       (select GiveTypeDesc "
		+"          from llclaimpolicy "
		+"         where caseno = a.caseno "
		+"           and riskcode = m.riskcode "
		+"           and caserelano = m.caserelano fetch first 1 rows only) 赔付结论, "
		+"       (select GiveReasonDesc "
		+"          from llclaimpolicy "
		+"         where caseno = a.caseno "
		+"           and riskcode = m.riskcode "
		+"           and caserelano = m.caserelano fetch first 1 rows only) 赔付结论依据, "
		+"       sum(m.realpay) 实陪金额, "
		+"       (select remark "
		+"          from LLCaseDrug "
		+"         where caseno = a.caseno fetch first 1 rows only) 扣除明细, "
		+"       (select bankname "
		+"          from ldbank "
		+"         where bankcode = "
		+"               (select BankCode "
		+"                  from ljaget "
		+"                 where (otherno = a.caseno or otherno = a.rgtno) "
		+"                   and othernotype = '5' fetch first 1 rows only)) 赔款银行, "
		+"       (select BankAccNo "
		+"          from ljaget "
		+"         where (otherno = a.caseno or otherno = a.rgtno) "
		+"           and othernotype = '5' fetch first 1 rows only) 赔款账号, "
		+"       (select AccName "
		+"          from ljaget "
		+"         where (otherno = a.caseno or otherno = a.rgtno) "
		+"           and othernotype = '5' fetch first 1 rows only) 赔款账户名, "
		+"       (select reason from llclaimdecline where caseno = a.caseno) 拒付原因 "
		+"  from llcase a, llclaimdetail m "
		+" where a.caseno = m.caseno "
		+"   and a.caseno in ("+allCaseStrs+") "
		+"   and a.mngcom like '8695%' "
		+"   and a.rgtstate = '12' "
		+"   and exists (select 1 "
		+"          from ljaget "
		+"         where (otherno = a.caseno or otherno = a.rgtno) "
		+"           and confdate >= current date - 2 days "
//		+"           and maketime >=current time - 20 minutes  "
		+"            ) "
		+"   and not exists (select 1 "
		+"          from llcasesz "
		+"         where caseno = a.caseno "
		+"           and uploadtype = 'clmres') "
		+" group by a.caseno, "
		+"          m.caserelano, "
		+"          a.rgtno, "
		+"          a.idtype, "
		+"          m.riskcode, "
		+"          a.customerno, "
		+"          a.customername, "
		+"          a.idno, "
		+"          a.rgtstate, "
		+"          a.mngcom "
		+" order by a.caseno fetch first 5000 rows only with ur  ";
		
		rsWrapper = new RSWrapper();
		if (!rsWrapper.prepareData(null, mSQL)) {
            System.out.println("保险理赔结果 __数据准备失败! ");
            return false;
        }

		biaoqian = "保险理赔结果";
		WritableSheet sheet = book.createSheet(biaoqian, 1);
		
		Label label1 = new Label(0, 0, "案件号");
		Label label2 = new Label(1, 0, "被保险人客户号");
		Label label3 = new Label(2, 0, "被保险人姓名");
		Label label4 = new Label(3, 0, "被保险人证件类型");
		Label label5 = new Label(4, 0, "被保人证件号码");
		Label label6 = new Label(5, 0, "申请人姓名");
		Label label7 = new Label(6, 0, "申请人证件类型");
		Label label8 = new Label(7, 0, "申请人证件号码");
		Label label9 = new Label(8, 0, "申请人手机");
		Label label10 = new Label(9, 0, "发生日期");
		Label label11 = new Label(10, 0, "入院日期");
		Label label12 = new Label(11, 0, "出院日期");
		Label label13 = new Label(12, 0, "事件信息");
		Label label14 = new Label(13, 0, "账单金额");
		Label label15 = new Label(14, 0, "赔付险种名称");
		Label label16 = new Label(15, 0, "赔付险种代码");
		Label label17 = new Label(16, 0, "赔付结论");
		Label label18 = new Label(17, 0, "赔付结论依据");
		Label label19 = new Label(18, 0, "实赔金额");
		Label label20 = new Label(19, 0, "扣除明细");
		Label label21 = new Label(20, 0, "赔款银行");
		Label label22 = new Label(21, 0, "赔款账号");
		Label label23 = new Label(22, 0, "赔款账户名");
		Label label24 = new Label(23, 0, "拒付原因");
		
		sheet.addCell(label1);
		sheet.addCell(label2);
		sheet.addCell(label3);
		sheet.addCell(label4);
		sheet.addCell(label5);
		sheet.addCell(label6);
		sheet.addCell(label7);
		sheet.addCell(label8);
		sheet.addCell(label9);
		sheet.addCell(label10);
		sheet.addCell(label11);
		sheet.addCell(label12);
		sheet.addCell(label13);
		sheet.addCell(label14);
		sheet.addCell(label15);
		sheet.addCell(label16);
		sheet.addCell(label17);
		sheet.addCell(label18);
		sheet.addCell(label19);
		sheet.addCell(label20);
		sheet.addCell(label21);
		sheet.addCell(label22);
		sheet.addCell(label23);
		sheet.addCell(label24);
		LLCaseSZSet mLLCaseSZSet = new LLCaseSZSet();
		SSRS tSSRS=null;
		k = 1;
		do {
        	tSSRS = rsWrapper.getSSRS();
        	System.out.println(tSSRS.MaxRow);
        	int maxRow = tSSRS.getMaxRow();
        	maxRows[k] = maxRow;
        	for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
        		int j = i + maxRows[1]*(k-1);
				label1 = new Label(0, j, tSSRS.GetText(i, 1));
				label2 = new Label(1, j, tSSRS.GetText(i, 4));
				label3 = new Label(2, j, tSSRS.GetText(i, 5));
				label4 = new Label(3, j, tSSRS.GetText(i, 6));
				label5 = new Label(4, j, tSSRS.GetText(i, 7));
				label6 = new Label(5, j, tSSRS.GetText(i, 8));
				label7 = new Label(6, j, tSSRS.GetText(i, 9));
				label8 = new Label(7, j, tSSRS.GetText(i, 10));
				label9 = new Label(8, j, tSSRS.GetText(i, 11));
				label10 = new Label(9, j, tSSRS.GetText(i, 12));
				label11 = new Label(10, j, tSSRS.GetText(i, 13));
				label12 = new Label(11, j, tSSRS.GetText(i, 14));
				label13 = new Label(12, j, tSSRS.GetText(i, 15));
				label14 = new Label(13, j, tSSRS.GetText(i, 16));
				label15 = new Label(14, j, tSSRS.GetText(i, 17));
				label16 = new Label(15, j, tSSRS.GetText(i, 18));
				label17 = new Label(16, j, tSSRS.GetText(i, 19));
				label18 = new Label(17, j, tSSRS.GetText(i, 20));
				label19 = new Label(18, j, tSSRS.GetText(i, 21));
				label20 = new Label(19, j, tSSRS.GetText(i, 22));
				label21 = new Label(20, j, tSSRS.GetText(i, 23));
				label22 = new Label(21, j, tSSRS.GetText(i, 24));
				label23 = new Label(22, j, tSSRS.GetText(i, 25));
				label24 = new Label(23, j, tSSRS.GetText(i, 26));
				
				
				if(i<tSSRS.MaxRow){
					if(!tSSRS.GetText(i, 1).equals(tSSRS.GetText(i+1, 1))){
						mLLCaseSZSet.add(getInputData(tSSRS,i,"clmres"));
					}
				}else{
					mLLCaseSZSet.add(getInputData(tSSRS,i,"clmres"));
				}
				
				sheet.addCell(label1);
				sheet.addCell(label2);
				sheet.addCell(label3);
				sheet.addCell(label4);
				sheet.addCell(label5);
				sheet.addCell(label6);
				sheet.addCell(label7);
				sheet.addCell(label8);
				sheet.addCell(label9);
				sheet.addCell(label10);
				sheet.addCell(label11);
				sheet.addCell(label12);
				sheet.addCell(label13);
				sheet.addCell(label14);
				sheet.addCell(label15);
				sheet.addCell(label16);
				sheet.addCell(label17);
				sheet.addCell(label18);
				sheet.addCell(label19);
				sheet.addCell(label20);
				sheet.addCell(label21);
				sheet.addCell(label22);
				sheet.addCell(label23);
				sheet.addCell(label24);
        	}
        	k = k + 1;
		} while (tSSRS != null && tSSRS.MaxRow > 0);
		book.write();
		book.close();
		rmap.put(mLLCaseSZSet, "INSERT");
		}
	} catch (IOException e) {
		e.printStackTrace();
		return false;
	} catch (RowsExceededException e) {
		e.printStackTrace();
		return false;
	} catch (WriteException e) {
		e.printStackTrace();
		return false;
	}
	
	return true;
	}

	/**
	 * 返回目录路径
	 * @param cRootPath
	 * @return cRootPath/yyyy/yyyyMM/yyyyMMdd/
	 */
	private boolean transToFtp(String ttype,String strURL,int n,String curDate){
        ExeSQL tExeSql = new ExeSQL();

        String tServerIP = tExeSql.getOneValue("select codename from ldcode where codetype='LLWXClaim' and code='IP/Port'");
//        String tServerIP ="10.252.4.88"; //外测地址
        String tPort = tExeSql.getOneValue("select codealias from ldcode where codetype='LLWXClaim' and code='IP/Port'");
        String tUsername = tExeSql.getOneValue("select codename from ldcode where codetype='LLWXClaim' and code='User/Pass'");
        String tPassword = tExeSql.getOneValue("select codealias from ldcode where codetype='LLWXClaim' and code='User/Pass'");
        FTPTool tFTPTool = null;
        try {
            
            // 取得本地存放文件的目录
            String tFilePathLocal = strURL+ttype+n+".xls";
            System.out.println(tFilePathLocal);
            // 用于本地测试*******end
            String tFileTransPath = "/01PH/8695/WXPolicyInfo/" + curDate+"/"+ttype+"/";
            System.out.println(tFileTransPath);
            
            File ft=new File(tFilePathLocal);
            if(ft.exists()){
            	Sheet sheet=null;
            try {
            	Workbook  book=Workbook.getWorkbook(ft);
            	sheet=book.getSheet(0); 
            	System.out.println(sheet.getRows()+"行！！！"+tFilePathLocal);
			} catch (BiffException e) {
				e.printStackTrace();
			}  
            
            
            // 上传文件
            if(sheet!=null && sheet.getRows()>1){
            	System.out.println("文件不为空！"+tFilePathLocal);
            	 tFTPTool = new FTPTool(tServerIP, tUsername, tPassword, Integer.parseInt(tPort), "aaActiveMode");
                 if (!tFTPTool.loginFTP()) {
                     CError tError = new CError();
                     tError.moduleName = "CardActiveBatchImportBL";
                     tError.functionName = "getXls";
                     tError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                     mErrors.addOneError(tError);
                     System.out.println(tError.errorMessage);
                     return false;
                 }
              // 改变ftp服务器的当前工作目录
                 tFTPTool.changeWorkingDirectory("/01PH/8695/WXPolicyInfo/");
			    if(!tFTPTool.upload(tFileTransPath, tFilePathLocal)) {
			    	System.out.println("文件上传失败！！！"+tFilePathLocal);
			    	tFTPTool.logoutFTP();
			    	return false;
			    }
			    //上传完成后，修改名称OK
			    tFTPTool.changeWorkingDirectory("/01PH/8695/WXPolicyInfo/"+ttype);
			    tFTPTool.rename(ttype+n+".xls", ttype+n+"OK.xls");
			   
			    tFTPTool.logoutFTP();
			    VData mInputData=new VData();
			    if("ClaimStatus".equals(ttype)){
					mInputData.add(map);
				}else if("ClaimResault".equals(ttype)){
					mInputData.add(rmap);   
				}
			    submitData(mInputData);
			}else{
            	System.out.println("文件为空，不上传"+tFilePathLocal);
            	return true;
            }
        }
        }
        catch (SocketException ex) {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "ExcelToFtpBL";
            tError.functionName = "transToFtp";
            tError.errorMessage = "获取连接失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        catch (IOException ex) {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "ExcelToFtpBLBL";
            tError.functionName = "transToFtp";
            tError.errorMessage = "连接ftp失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }finally{
        	
        }
        return true;
        
    }
	private boolean makeDirectory(String curDate){
		// 创建根目录，如果创建成功，将配置表下标修改为1。
		ExeSQL tExeSql = new ExeSQL();
		
        String tServerIP = tExeSql.getOneValue("select codename from ldcode where codetype='LLWXClaim' and code='IP/Port'");
//        String tServerIP ="10.252.4.88"; //外测地址
        String tPort = tExeSql.getOneValue("select codealias from ldcode where codetype='LLWXClaim' and code='IP/Port'");
        String tUsername = tExeSql.getOneValue("select codename from ldcode where codetype='LLWXClaim' and code='User/Pass'");
        String tPassword = tExeSql.getOneValue("select codealias from ldcode where codetype='LLWXClaim' and code='User/Pass'");
        FTPTool tFTPTool = new FTPTool(tServerIP, tUsername, tPassword, Integer.parseInt(tPort), "aaActiveMode");
        
        try {
        	if (!tFTPTool.loginFTP()) {
                CError tError = new CError();
                tError.moduleName = "CardActiveBatchImportBL";
                tError.functionName = "getXls";
                tError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
        	//创建根目录
        	 tFTPTool.changeWorkingDirectory("/01PH/8695/WXPolicyInfo/");
			if(tFTPTool.makeDirectory(curDate)){
				System.out.println("FTP新建目录");		
			}
			//创建子目录
            if(tFTPTool.makeDirectory(curDate+"/ClaimStatus")&&tFTPTool.makeDirectory(curDate+"/ClaimResault")){
            	MMap codemap=new MMap();
            	VData mInputData = new VData();
            	System.out.println("FTP上新建子目录");
            	String sqlSta="update ldcode set code='0' where codetype='llcaseszsta'";
            	String sqlRes="update ldcode set code='0' where codetype='llcaseszres'";
				codemap.put(sqlSta, "UPDATE");
				codemap.put(sqlRes, "UPDATE");
				mInputData.add(codemap);	
				submitData(mInputData);
            }
  
		} catch (IOException e) {
			
			e.printStackTrace();
		} finally {
			tFTPTool.logoutFTP();
		}
        return true;
	}
	
	private String getSavePath(String cRootPath,String ctype,String curDate){
        StringBuffer tFilePath = new StringBuffer();

        tFilePath.append(cRootPath);
        tFilePath.append(curDate.replace("-", ""));
        tFilePath.append('/');
        tFilePath.append(ctype);
        tFilePath.append('/');
        return tFilePath.toString();
        
    }
	//获取需要存入的数据
	private LLCaseSZSchema getInputData(SSRS tSSRS,int i,String type){
		
		LLCaseSZSchema mLLCaseSZScheam=new LLCaseSZSchema();
		mLLCaseSZScheam.setCaseNo(tSSRS.GetText(i, 1));
		mLLCaseSZScheam.setRgtState(tSSRS.GetText(i, 2));
		mLLCaseSZScheam.setUploadType(type);
		mLLCaseSZScheam.setMngCom(tSSRS.GetText(i, 3));
		mLLCaseSZScheam.setMakeDate(PubFun.getCurrentDate());
		mLLCaseSZScheam.setMakeTime(PubFun.getCurrentTime());
		mLLCaseSZScheam.setModifyDate(PubFun.getCurrentDate());
		mLLCaseSZScheam.setModifyTime(PubFun.getCurrentTime());
		return mLLCaseSZScheam;
	}
	public boolean submitData(VData mInputData) {
		PubSubmit tPubSubmit=new PubSubmit();
		if(!tPubSubmit.submitData(mInputData, null)){
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			     return buildErr("submitData","数据更新失败！");
			
		}
		
		return true;
	 }
	 // @@错误处理
    private boolean buildErr(String FucName,String ErrMsg){
        CError tError = new CError();
        tError.moduleName = "LLImportCaseInfo";
        tError.functionName = FucName;
        tError.errorMessage = ErrMsg;
        this.mErrors.addOneError(tError);
        return false;
    }
	/**
	 * 测试
	 * @param args
	 */
	public static void main(String[] args) {
		LLSZLPExcelTransFtp test = new LLSZLPExcelTransFtp();
		test.dealData();
		System.out.println("执行完毕！！！！");
	}
	
}
