package com.sinosoft.lis.llcase;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class LLCoreInvokeBL {

	/**
	 * @param args
	 */
//	private static final Logger cLogger = Logger
	//		.getLogger(YibaotongServlet.class);
	
	public CErrors mCErrors = new CErrors();

	public LLCoreInvokeBL() {

	}

	public boolean dealData() {
		System.out.println("客户风险等级lccustomerriskclass开始。。。。。。");
		
		//删除以前有的主键数据
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String tdeleteSQL = "delete from lccustomerriskclass a where exists "
                 +" (select 1 from lccont where signdate = current date - 1 days and appntno=a.customerno and conttype='1' and grpcontno='00000000000000000000') ";

		System.out.println("开始删除lccustomerriskclass的个单主键数据"
				+ tdeleteSQL);
		if (!pubSubmit(tdeleteSQL)) {
			System.out.println("执行失败：" + tdeleteSQL);
			mCErrors.addOneError("执行失败：" + tdeleteSQL);
			return false;
		}
		System.out.println("删除lccustomerriskclass的个单主键数据成功");
		
		String adeleteSQL = "delete from lccustomerriskclass a where exists "
            +" (select 1 from lcgrpcont where signdate = current date - 1 days and appntno=a.customerno) ";

		System.out.println("开始删除lccustomerriskclass的团单主键数据"
			+ adeleteSQL);
		if (!pubSubmit(adeleteSQL)) {
		System.out.println("执行失败：" + adeleteSQL);
		mCErrors.addOneError("执行失败：" + adeleteSQL);
		return false;
		}
		System.out.println("删除lccustomerriskclass的团单主键数据成功");
	
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		
		//开始导入数据
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		//个单导入
		String tLCCustomerRiskSQL = " insert into lccustomerriskclass (select Y.* from (select distinct (a.appntno) appntno "
			+" from lccont a,lcappnt b where a.appntno=b.appntno and a.conttype='1' and "
			+" a.grpcontno='00000000000000000000' and a.contno=b.contno and a.signdate= current date - 1 days) as X, "
			+" lateral (select b.appntno CustomerNo,'1' CustomerType,'0' CustomerKind,'' CustomerGrade,'' ClassReason, "
			+" '' ClassReasonDesc,'0' ClassState,b.appntname CustomerName,b.appntsex CustomerSex,b.appntbirthday CustomerBirthday, "
			+" b.idtype IDType,b.idno IDNo,b.nativeplace NativePlace,a.contno ContNo,a.salechnl SaleChnl,a.agentcom AgentCom, "
			+" getUniteCode(a.agentcode) AgentCode,a.agentgroup AgentGroup,a.signdate SignDate,current date HandleDate, "
			+" (select current date from dual where 1=2) ConfirmDate,'' ConfirmOperator,a.managecom ManageCom,'' Remark, "
			+" 'SYS' Operator,current date MakeDate,current time MakeTime,current date ModifyDate,current time ModifyTime, "
			+" a.prem Prem,a.payintv PayIntv,a.paymode PayMode,b.occupationtype OccupationType,b.occupationcode OccupationCode " 
			+" from lccont a,lcappnt b where a.appntno=b.appntno and a.appntno=X.appntno and a.conttype='1' and "
			+" a.grpcontno='00000000000000000000' and a.contno=b.contno and a.signdate= current date - 1 days fetch first 1 rows only) as Y) ";
		System.out.println("开始导入个险客户风险表：" + tLCCustomerRiskSQL);
		if (!pubSubmit(tLCCustomerRiskSQL)) {
			System.out.println("执行失败：" + tLCCustomerRiskSQL);
			mCErrors.addOneError("执行失败：" + tLCCustomerRiskSQL);
			return false;
		}
		System.out.println("导入个险客户风险表成功");
		
		
		//团单导入
		String tLCGrpCustomerRiskSQL = " insert into lccustomerriskclass (select Y.* from (select distinct (b.customerno) customerno from "
		+" lcgrpcont a,lcgrpappnt b where a.appntno = b.customerno and a.grpcontno = b.grpcontno and "
		+" a.signdate= current date - 1 days) as X,lateral (select b.customerno CustomerNo,'0' CustomerType, "
		+" '0' CustomerKind,'' CustomerGrade,'' ClassReason,'' ClassReasonDesc,'0' ClassState,b.name CustomerName, "
		+" '' CustomerSex,(select current date from dual where 1 = 2) CustomerBirthday,'' IDType,'' IDNo,'' NativePlace, "
		+" a.grpcontno ContNo,a.salechnl SaleChnl,a.agentcom AgentCom,getUniteCode(a.agentcode) AgentCode,a.agentgroup AgentGroup, "
		+" a.signdate SignDate,current date HandleDate,(select current date from dual where 1 = 2) ConfirmDate, "
		+" '' ConfirmOperator,a.managecom ManageCom,'' Remark,'SYS' Operator,current date MakeDate,current time MakeTime, "
		+" current date ModifyDate,current time ModifyTime,a.prem Prem,a.payintv PayIntv,a.paymode PayMode,'' OccupationType, "
		+" '' OccupationCode from lcgrpcont a,lcgrpappnt b where a.appntno = b.customerno and a.grpcontno = b.grpcontno "
		+" and a.appntno=X.customerno and a.signdate= current date - 1 days fetch first 1 rows only) as Y) ";
		System.out.println("开始导入团险客户风险表：" + tLCGrpCustomerRiskSQL);
		if (!pubSubmit(tLCGrpCustomerRiskSQL)) {
			System.out.println("执行失败：" + tLCGrpCustomerRiskSQL);
			mCErrors.addOneError("执行失败：" + tLCGrpCustomerRiskSQL);
			return false;
		}
		System.out.println("导入团险客户风险表成功");
		return true;
	}

	/**
	 * 提交数据
	 * 
	 * @param tSQL
	 * @return
	 */
	private boolean pubSubmit(String aSQL) {
		ExeSQL tExeSQL = new ExeSQL();
		if (!tExeSQL.execUpdateSQL(aSQL)) {
			System.out.println("提交失败");
			//			cLogger.error("提交失败");
			return false;
		}
		return true;
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		LLCoreInvokeBL tLLCoreInvokeBL = new LLCoreInvokeBL();
		try {
			tLLCoreInvokeBL.dealData();
//			System.out.println();
		} catch (Exception ex) {
			System.out.println("客户风险表导入错误");
		
			ex.printStackTrace();
		}
	}

}
