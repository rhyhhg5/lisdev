package com.sinosoft.lis.llcase;

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;


/*******************************************************************************
 * Name     :CaseInfoBL.java
 * Function :案件－立案－分案疾病伤残明细业务逻辑处理类
 * Author   :LiuYansong
 * Date     :2003-7-16
 */
public class CaseInfoBL
{
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private String mOperate;

    private LLCaseInfoSet mLLCaseInfoSet = new LLCaseInfoSet();
    private LLCaseInfoSchema mLLCaseInfoSchema = new LLCaseInfoSchema();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();


    private LLCaseCureSet mLLCaseCureSet = new LLCaseCureSet();
    private LLOperationSet mLLOperationSet = new LLOperationSet();
    private LLOtherFactorSet mLLOtherFactorSet = new LLOtherFactorSet();
    private LLAccidentSet mLLAccidentSet = new LLAccidentSet();


    /** 全局数据 */
    String strMakeDate;
    String strMakeTime;
    String DisplayFlag = ""; //判断是在立案中进入还是在理算中进入
    String Set_count = "";
    String AccidentType = "";
    private GlobalInput mGlobalInput = new GlobalInput();
    private GlobalInput mG = new GlobalInput();
    String mCaseRelaNo = "";
    public CaseInfoBL()
    {}

    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        if (!getInputData(cInputData))
            return false;

        if (cOperate.equals("INSERT"))
        {
            if (!dealDataInsert())return false;

            //   mLLCaseInfoSchema = (LLCaseInfoSchema)mInputData.getObjectByObjectName("LLCaseInfoSchema",0);
//      LLCaseDB tLLCaseDB = new LLCaseDB();
//      tLLCaseDB.setCaseNo(mLLCaseSchema.getCaseNo());
//      if(!tLLCaseDB.getInfo())
//        return false;
//      LLRegisterDB tLLRegisterDB = new LLRegisterDB();
//      tLLRegisterDB.setRgtNo(tLLCaseDB.getRgtNo());
//      if(!tLLRegisterDB.getInfo())
//        return false;
//     //若该案件正处于调查之中，则无法进行保存操作
//      if(tLLRegisterDB.getClmState().equals("5"))
//      {
//        this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
//        CError tError = new CError();
//        tError.moduleName = "RegisterUpdateBL";
//        tError.functionName = "submitData";
//        tError.errorMessage = "该案件正处于调查之中，您无法进行保存操作！！";
//        this.mErrors .addOneError(tError) ;
//        return false;
//      }
//     //若该案件已经结案，则不能进行保存操作
//      if(tLLRegisterDB.getEndCaseFlag().equals("2"))
//      {
//        this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
//        CError tError = new CError();
//        tError.moduleName = "RegisterUpdateBL";
//        tError.functionName = "submitData";
//        tError.errorMessage = "该案件已经结案，您无法进行保存操作！！";
//        this.mErrors .addOneError(tError) ;
//        return false;
//      }
            if (!dealData())
                return false;
            System.out.println("-------dealData--------");
        }

        PubSubmit ps = new PubSubmit();
        if ( !ps.submitData( this.mResult,null) )
        {
            CError.buildErr(this,"提交数据保存失败");
            return false;
        }
//
//        if (cOperate.equals("SHOW"))
//        {
//            if (!showCaseInfo())
//                return false;
//        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    private boolean dealDataInsert()
    {
System.out.println("---------DealDataInsert--------");
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mLLCaseSchema.getCaseNo());
        if (!tLLCaseDB.getInfo())
            return false;
//  LLRegisterDB tLLRegisterDB = new LLRegisterDB();
//  tLLRegisterDB.setRgtNo(tLLCaseDB.getRgtNo());
//  if(!tLLRegisterDB.getInfo())
//    return  ;
        String cdate = PubFun.getCurrentDate();
        String dtime = PubFun.getCurrentTime() ;
        String noLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        System.out.println("---------mLLCaseCureSet--------");
        if (this.mLLCaseCureSet != null && this.mLLCaseCureSet.size() > 0)
        {
            for (int i = 1; i <= this.mLLCaseCureSet.size(); i++)
            {
                LLCaseCureSchema tLLCaseCureSchema = mLLCaseCureSet.get(i);
                tLLCaseCureSchema.setCustomerName(tLLCaseDB.getCustomerName());
                tLLCaseCureSchema.setCustomerNo(tLLCaseDB.getCustomerNo());
                tLLCaseCureSchema.setMngCom( this.mG.ManageCom);
                tLLCaseCureSchema.setMakeDate( cdate );
                tLLCaseCureSchema.setMakeTime( dtime );
                tLLCaseCureSchema.setModifyDate(cdate );
                tLLCaseCureSchema.setModifyTime( dtime );
                tLLCaseCureSchema.setOperator( this.mG.Operator );
                String cureNo = PubFun1.CreateMaxNo("CURENO", noLimit);
                tLLCaseCureSchema.setSerialNo(cureNo);
            }
        }
        System.out.println("---------mLLCaseInfoSet--------");
        if (this.mLLCaseInfoSet != null && this.mLLCaseInfoSet.size() > 0)
        {
            for (int i = 1; i <= this.mLLCaseInfoSet.size(); i++)
            {
                LLCaseInfoSchema tLLCaseInfoSchema = mLLCaseInfoSet.get(i);
                tLLCaseInfoSchema.setCustomerName(tLLCaseDB.getCustomerName());
                tLLCaseInfoSchema.setCustomerNo(tLLCaseDB.getCustomerNo());
                String cureNo = PubFun1.CreateMaxNo("CASEINFO", noLimit);
                tLLCaseInfoSchema.setRgtNo( tLLCaseDB.getRgtNo());
                tLLCaseInfoSchema.setSerialNo(cureNo);
                tLLCaseInfoSchema.setMakeDate( cdate );
                tLLCaseInfoSchema.setMakeTime(dtime);
                tLLCaseInfoSchema.setModifyDate(cdate);
                tLLCaseInfoSchema.setModifyTime(dtime);
                tLLCaseInfoSchema.setMngCom( this.mG.ManageCom);
                tLLCaseInfoSchema.setOperator(this.mG.Operator);

            }
        }
        System.out.println("---------mLLOperationSet--------");
        if (this.mLLOperationSet != null && this.mLLOperationSet.size() > 0)
        {
            for (int i = 1; i <= this.mLLOperationSet.size(); i++)
            {
                LLOperationSchema tLLOperationSchema = mLLOperationSet.get(i);

                String cureNo = PubFun1.CreateMaxNo("OPERATION", noLimit);
                tLLOperationSchema.setSerialNo(cureNo);
                tLLOperationSchema.setMngCom( this.mG.ManageCom);
                tLLOperationSchema.setMakeDate( cdate );
                tLLOperationSchema.setMakeTime(dtime);
                tLLOperationSchema.setModifyDate(cdate);
                tLLOperationSchema.setModifyTime(dtime);
                tLLOperationSchema.setOperator(this.mG.Operator);

            }
        }
//        if (this.mLLOtherFactorSet != null && this.mLLOtherFactorSet.size() > 0)
//        {
//            for (int i = 1; i <= this.mLLOtherFactorSet.size(); i++)
//            {
//                LLOtherFactorSchema mLLOtherFactorSchema = mLLOtherFactorSet.get(i);
//                String CaseRelaNo = PubFun1.CreateMaxNo("CaseRelaNo", noLimit);
//                mLLOtherFactorSchema.setCaseRelaNo(CaseRelaNo);
//            }
//        }
        System.out.println("---------mLLAccidentSet--------");
        if (this.mLLAccidentSet != null && this.mLLAccidentSet.size() > 0)
        {
            for (int i = 1; i <= this.mLLAccidentSet.size(); i++)
            {
                LLAccidentSchema mLLAccidentSchema = mLLAccidentSet.get(i);
                String AccidentNo = PubFun1.CreateMaxNo("ACCIDENTNO", noLimit);
                mLLAccidentSchema.setAccidentNo(AccidentNo);
              //  mLLAccidentSchema.setCaseRelaNo(CaseRelaNo);
            }
        }
        System.out.println("---------mLLOtherFactorSet--------");
        MMap tmpMap = new MMap();

        String[] deltable = {"llcaseinfo","llcasecure" ,"lloperation","LLOtherFactor","LLAccident"};
        for (int i=0;i< deltable.length;i++)
        {
         String delsql ="delete from " + deltable[i] +" where caseno ='" + tLLCaseDB.getCaseNo() +"'"
                        +" and caserelano='" + this.mCaseRelaNo +"'";
         tmpMap.put( delsql,"DELETE");
        }

        tmpMap.put( this.mLLCaseCureSet, "INSERT");
        tmpMap.put( this.mLLCaseInfoSet, "INSERT");
        tmpMap.put( this.mLLOperationSet, "INSERT");
        tmpMap.put( this.mLLOtherFactorSet, "INSERT");
        tmpMap.put( this.mLLAccidentSet, "INSERT");
        this.mResult.add( tmpMap );
        return true;
    }


    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    //先要调用prepareOutputData()函数
    private boolean dealData()
    {

        //判断传递来的Set中的值是否为0；若为0则执行删除操作
//    if(mLLCaseInfoSet.size()==0)
//    {
//      Set_count="0";
//    }
//    if(mLLCaseInfoSet.size()>0)
//    {
//      Set_count="1";
//    }
//    System.out.println("set的个数是"+Set_count);
//    if(Set_count.equals("0"))
//    {
//        //直接进入BLS中进行处理(不论上在立案中进入还是在理算中进入，因为操作的是同一个表)
//        System.out.println("直接向后台传递数据");
//        mInputData.clear();
//        mInputData.addElement(DisplayFlag);//是从立案还是从理算界面中进入系统的
//        mInputData.addElement(AccidentType);//事故类型是JB还是YW
//        mInputData.addElement(mLLCaseInfoSet);//将空的Set向BLS传递
//        mInputData.addElement(mLLCaseInfoSchema);
//        CaseInfoBLS tCaseInfoBLS = new CaseInfoBLS();
//        if (!tCaseInfoBLS.submitData(mInputData,mOperate))
//        {
//          // @@错误处理
//          this.mErrors.copyAllErrors(tCaseInfoBLS.mErrors);
//          CError tError = new CError();
//          tError.moduleName = "CaseInfoBL";
//          tError.functionName = "submitData";
//          tError.errorMessage = "数据保存失败!";
//          this.mErrors .addOneError(tError) ;
//          return false;
//        }
//      }

//    prepareOutputData();
//    CaseInfoBLS tCaseInfoBLS = new CaseInfoBLS();
//    System.out.println("Start CasePolicybls Submit...");
//    if (!tCaseInfoBLS.submitData(mInputData,mOperate))
//    {
//      // @@错误处理
//      this.mErrors.copyAllErrors(tCaseInfoBLS.mErrors);
//      CError tError = new CError();
//      tError.moduleName = "CaseInfoBL";
//      tError.functionName = "submitData";
//      tError.errorMessage = "数据保存失败!";
//      this.mErrors .addOneError(tError) ;
//      return false;
//    }

        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        //  DisplayFlag=(String)cInputData.get(0);

        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
            mGlobalInput= mG;
        mLLCaseSchema = (LLCaseSchema) cInputData.getObjectByObjectName(
                "LLCaseSchema", 0);

        if (mOperate.equals("INSERT"))
        {
            mCaseRelaNo= (String)cInputData.getObjectByObjectName("String",0) ;
            mLLCaseInfoSet = (LLCaseInfoSet) cInputData.getObjectByObjectName(
                    "LLCaseInfoSet", 0);
            //AccidentType = (String)cInputData.get(1);
            this.mLLCaseCureSet = (LLCaseCureSet) cInputData.
                                  getObjectByObjectName("LLCaseCureSet", 0);
            this.mLLOperationSet = (LLOperationSet) cInputData.
                                   getObjectByObjectName("LLOperationSet", 0);
            this.mLLOtherFactorSet = (LLOtherFactorSet) cInputData.
                                   getObjectByObjectName("LLOtherFactorSet", 0);
            this.mLLAccidentSet = (LLAccidentSet) cInputData.
                                   getObjectByObjectName("LLAccidentSet", 0);
                               // System.out.println("事故类型是"+AccidentType);
        }
//    if(mOperate.equals("SHOW"))
//    {
//      mLLCaseSchema.setSchema((LLCaseSchema)cInputData.getObjectByObjectName("LLCaseSchema",0));
//    }
        return true;
    }

    private void prepareOutputData()
    {
        int n = mLLCaseInfoSet.size();
        try
        {
            for (int i = 1; i <= n; i++)
            {
                LLCaseInfoSchema tLLCaseInfoSchema = mLLCaseInfoSet.get(i);
                LLCaseInfoDB tLLCaseInfoDB = new LLCaseInfoDB();
                tLLCaseInfoDB.setCaseNo(tLLCaseInfoSchema.getCaseNo());
//                tLLCaseInfoDB.setAffixSerialNo(tLLCaseInfoSchema.
//                                               getAffixSerialNo());
                tLLCaseInfoDB.getInfo();
                if (tLLCaseInfoDB.getMakeDate() == null)
                    tLLCaseInfoSchema.setMakeDate(PubFun.getCurrentDate());
                else
                    tLLCaseInfoSchema.setMakeDate(tLLCaseInfoDB.getMakeDate());
                if (tLLCaseInfoDB.getMakeTime() == null)
                    tLLCaseInfoSchema.setMakeTime(PubFun.getCurrentTime());
                else
                    tLLCaseInfoSchema.setMakeTime(tLLCaseInfoDB.getMakeTime());
                tLLCaseInfoSchema.setOperator(mG.Operator);
                tLLCaseInfoSchema.setMngCom(mG.ManageCom);
                tLLCaseInfoSchema.setModifyDate(PubFun.getCurrentDate());
                tLLCaseInfoSchema.setModifyTime(PubFun.getCurrentTime());
                mLLCaseInfoSet.set(i, tLLCaseInfoSchema);
            }
            mInputData.clear();
            mInputData.addElement(DisplayFlag);
            mInputData.addElement(AccidentType);
            mInputData.add(mLLCaseInfoSet);
            mInputData.addElement(mLLCaseInfoSchema);
            mResult.clear();
            mResult.add(mLLCaseInfoSet);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private boolean showCaseInfo()
    {
        String sql = "";
        LLCaseInfoSet tLLCaseInfoSet = new LLCaseInfoSet();
        LLCaseInfoDB tLLCaseInfoDB = new LLCaseInfoDB();
        if (DisplayFlag.equals("0"))
        {
            //对立案中的事故明细进行查询显示
            sql = "select * from LLCaseInfo where MngCom like '" + mG.ManageCom +
                  "%' and CaseNo = '" + mLLCaseSchema.getCaseNo() +
                    "'and ( AffixSerialNo  like 'YW%' or AffixSerialNo like 'JB%' ) ";
        }
        if (DisplayFlag.equals("1"))
        {
            //对理算中的伤残明细进行查询显示
            sql = "select * from LLCaseInfo where MngCom like '" + mG.ManageCom +
                  "%' and   CaseNo = '" + mLLCaseSchema.getCaseNo() +
                  "'and AffixSerialNo like 'SC%' ";
        }
        System.out.println("所执行的sql语句是" + sql);
        tLLCaseInfoSet.set(tLLCaseInfoDB.executeQuery(sql));
        System.out.println("查询到的结果是" + tLLCaseInfoSet.size());
        mResult.clear();
        mResult.addElement(tLLCaseInfoSet);
        return true;
    }

    public static void main(String[] args)
    {
//   // int i = 7;
//    CaseInfoBL aCaseInfoBL = new CaseInfoBL();
//    //LLCaseInfoSchema aLLCaseInfoSchema = new LLCaseInfoSchema();
//    LLCaseInfoSet aLLCaseInfoSet = new LLCaseInfoSet();
//    //aLLCaseInfoSchema.setCaseNo("86000020030550000002");
//    //aLLCaseInfoSchema.setAffixSerialNo("YW"+String.valueOf(7));
//    //System.out.println("AffixSerialNo的值是"+aLLCaseInfoSchema.getAffixSerialNo());
//    //aLLCaseInfoSchema.setType("YW");
//    //aLLCaseInfoSchema.setCode("001");
//    //aLLCaseInfoSchema.setName("1111111111");
//    //aLLCaseInfoSet.add(aLLCaseInfoSchema);
//    VData tVData = new VData();
//    //tVData.addElement(aLLCaseInfoSet);
//    //tVData.addElement(aLLCaseInfoSchema);
//    aCaseInfoBL.submitData(tVData,"INSERT");
//    CaseInfoBL aCaseInfoBL = new CaseInfoBL();
//    LLCaseSchema aLLCaseSchema = new LLCaseSchema();
//    aLLCaseSchema.setCaseNo("00100020030550000090");
//    GlobalInput tG = new GlobalInput();
//    String tDisplayFlag = "0";
//    VData tVData = new VData();
//    tVData.addElement(tDisplayFlag);
//    tVData.addElement(tG);
//    tVData.addElement(aLLCaseSchema);
//    aCaseInfoBL.submitData(tVData,"SHOW");
    }
}
