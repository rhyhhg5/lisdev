package com.sinosoft.lis.llcase;

import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public class HttpClientTest {
//    private static String filePath = "d:\\testing\\";

    public static void main(String[] args)   {
    	
    	HttpClientTest hct= new HttpClientTest();
		String tDoc = "<XMLTable>	<XMLCom>		<BUSINESSID>G0001</BUSINESSID>		<SYSCODE>CORESYSTEM</SYSCODE>	</XMLCom>"+
		"	<XMLRecs>		<XMLRec>			<NAME>李XX</NAME>			<CUSTOMERCODE>321458</CUSTOMERCODE>			"+"<IDCARDTYPE>01</IDCARDTYPE>			<IDCARD>210112198801020854</IDCARD>			<BIRTHDAY>1988-01-02</BIRTHDAY>			<GROUPCONTNO>45896</GROUPCONTNO>			<CONTNO>4589611</CONTNO>			<RISKCODE>01</RISKCODE>			<CASENO>7895461</CASENO>			<RESPONSIBILITYCODE>01</RESPONSIBILITYCODE>			<CLAIMDATE>2017-03-26 13:10:11</CLAIMDATE>			<PAYMENTMONEY>2000.00</PAYMENTMONEY>			<ACCOUNTMONEY>1000.00</ACCOUNTMONEY>			<HOSPITALNAME>盛京医院</HOSPITALNAME>			<HOSPITALCODE>001</HOSPITALCODE>			<RESPONSIBILITYCESSER>责任终止</RESPONSIBILITYCESSER>		</XMLRec>	"+
		"	<XMLRec>			<NAME>张XX</NAME>			<CUSTOMERCODE>3958447</CUSTOMERCODE>			<IDCARDTYPE>01</IDCARDTYPE>			<IDCARD>210112198701120014</IDCARD>			<BIRTHDAY>1987-01-12</BIRTHDAY>			<GROUPCONTNO>123456</GROUPCONTNO>			<CONTNO>456987</CONTNO>			<RISKCODE>01</RISKCODE>			<CASENO>7411251</CASENO>			<RESPONSIBILITYCODE>01</RESPONSIBILITYCODE>			<CLAIMDATE>2017-03-26 13:10:11</CLAIMDATE>			<PAYMENTMONEY>2000.00</PAYMENTMONEY>			<ACCOUNTMONEY>1000.00</ACCOUNTMONEY>			<HOSPITALNAME>盛京医院</HOSPITALNAME>			<HOSPITALCODE>001</HOSPITALCODE>			<RESPONSIBILITYCESSER>责任终止</RESPONSIBILITYCESSER>		</XMLRec>	</XMLRecs></XMLTable>";

//    	String tDoc="AAA";
    	hct.getResponse(tDoc);
    }

    public String getResponse(String tDoc)  {
		// TODO Auto-generated method stub
    	String  responseStr="";
System.out.println("HttpClientTest=======交互即将开始");
        HttpClient httpclient = new DefaultHttpClient();
        System.out.println("HttpClientTest=======初始化HttpClient完毕");
        HttpHost proxy = new HttpHost("10.252.18.1", 7001, "http");
        System.out.println("HttpClientTest=======初始化HttpHost完毕11111111111111");
        try {
            httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
            System.out.println("HttpClientTest=======配置HttpClient完毕");
            HttpPost post = new HttpPost("http://10.252.18.1:7001/onecardws/services/vipservice?wsdl");
            System.out.println("HttpClientTest=======初始化HttpHost完毕222222222222222");
//            String xmldata = FileUtil.readFile(filePath + "VIPHttp.xml");
//            String xmldata="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservice.onecard.neusoft.com/\"><soapenv:Header>   <authrity>   <syscode>CORESYSTEM</syscode>   <password>9F17F5B73D1C0CB255333E96F991A8BE</password>   </authrity>   </soapenv:Header>   <soapenv:Body>      <web:VIPClaimData>         <!--Optional:-->         <arg0> <![CDATA[<XMLTable>	<XMLCom>		<BUSINESSID>G0001</BUSINESSID>		<SYSCODE>CORESYSTEM</SYSCODE>	</XMLCom>	<XMLRecs>		<XMLRec>			<NAME>李XX</NAME>			<CUSTOMERCODE>321458</CUSTOMERCODE>			<IDCARDTYPE>01</IDCARDTYPE>			<IDCARD>210112198801020854</IDCARD>			<BIRTHDAY>1988-01-02</BIRTHDAY>			<GROUPCONTNO>45896</GROUPCONTNO>			<CONTNO>4589611</CONTNO>			<RISKCODE>01</RISKCODE>			<CASENO>789512</CASENO>			<RESPONSIBILITYCODE>01</RESPONSIBILITYCODE>			<CLAIMDATE>2017-03-26 13:10:11</CLAIMDATE>			<PAYMENTMONEY>2000.00</PAYMENTMONEY>			<ACCOUNTMONEY>1000.00</ACCOUNTMONEY>			<HOSPITALNAME>盛京医院</HOSPITALNAME>			<HOSPITALCODE>001</HOSPITALCODE>			<RESPONSIBILITYCESSER>责任终止</RESPONSIBILITYCESSER>		</XMLRec>	</XMLRecs></XMLTable> ]]>                  </arg0>      </web:VIPClaimData>   </soapenv:Body></soapenv:Envelope>";
            System.out.println("HttpClientTest=======准备组合soap+发送报文");
            String xmldata=getRequestXml(tDoc);
            System.out.println("HttpClientTest=======组合soap+发送报文完毕"+xmldata);
            StringEntity entiy = new StringEntity(xmldata, "UTF-8");

            post.setEntity(entiy);
            post.setHeader("Content-Type", "text/xml; charset=UTF-8");
            System.out.println("HttpClientTest=======准备调用一卡通");
            HttpResponse response = httpclient.execute(post);
            System.out.println("HttpClientTest=======调用一卡通完毕");
            HttpEntity e = response.getEntity();

            System.out.println(response.getStatusLine());
            InputStream in = e.getContent();

            byte[] b = new byte[1024];
            int len = 0;
            String s = "";
            while ((len = in.read(b)) != -1) {
                String ss = new String(b, 0, len, "UTF-8");
                s += ss;
            }
            responseStr=s;
            System.out.println("HttpClientTest=======一卡通返回结果："+responseStr);
        }catch (Exception e) {
			// TODO: handle exception
        	System.out.println("HttpClientTest=======调用一卡通接口异常");
        	e.printStackTrace();
        	
		}
        finally {
            httpclient.getConnectionManager().shutdown();
        }
        System.out.println("HttpClientTest=======交互结束");
    return responseStr;
	}

	/** 
     * 获得请求XML 
     * @return 
     */  
    public String getRequestXml(String tDoc){  
        StringBuilder sb = new StringBuilder();  
        
        sb.append(" <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservice.onecard.neusoft.com/\"> ");
            sb.append(" <soapenv:Header> ");
            sb.append("    <authrity> ");
            sb.append("    <syscode>CORESYSTEM</syscode> ");
            sb.append("    <password>"+MD5Util.encodeMD5WithSecret("COREpassword1@3","DA8B503894671AB56635B1DADCD05270")+"</password> ");
            sb.append("    </authrity> ");
            sb.append("    </soapenv:Header> ");
            sb.append("    <soapenv:Body> ");
            sb.append("       <web:VIPClaimData> ");
            sb.append("          <!--Optional:--> ");
            sb.append("          <arg0> ");
            sb.append("   <![CDATA[ ");
            
            sb.append(""+tDoc+"");
//     <XMLTable>
//     	<XMLCom>
//     		<BUSINESSID>G0001</BUSINESSID>
//     		<SYSCODE>CORESYSTEM</SYSCODE>
//     	</XMLCom>
//     	<XMLRecs>
//     		<XMLRec>
//     			<NAME>李XX</NAME>
//     			<CUSTOMERCODE>321458</CUSTOMERCODE>
//     			<IDCARDTYPE>01</IDCARDTYPE>
//     			<IDCARD>210112198801020854</IDCARD>
//     			<BIRTHDAY>1988-01-02</BIRTHDAY>
//     			<GROUPCONTNO>45896</GROUPCONTNO>
//     			<CONTNO>4589611</CONTNO>
//     			<RISKCODE>01</RISKCODE>
//     			<CASENO>789512</CASENO>
//     			<RESPONSIBILITYCODE>01</RESPONSIBILITYCODE>
//     			<CLAIMDATE>2017-03-26 13:10:11</CLAIMDATE>
//     			<PAYMENTMONEY>2000.00</PAYMENTMONEY>
//     			<ACCOUNTMONEY>1000.00</ACCOUNTMONEY>
//     			<HOSPITALNAME>盛京医院</HOSPITALNAME>
//     			<HOSPITALCODE>001</HOSPITALCODE>
//     			<RESPONSIBILITYCESSER>责任终止</RESPONSIBILITYCESSER>
//     		</XMLRec>
//     	</XMLRecs>
//     </XMLTable>


            sb.append("   ]]>      ");
              
            sb.append("   </arg0> ");
            sb.append("   </web:VIPClaimData> ");
            sb.append("   </soapenv:Body> ");
            sb.append("   </soapenv:Envelope> ");
        
          
        return sb.toString();  
    }  
    
}
