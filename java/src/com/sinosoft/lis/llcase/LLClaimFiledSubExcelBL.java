/*
 * <p>ClassName: OLLMainAskBL </p>
 * <p>Description: 咨询通知保存 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-12 16:05:52
 */



package com.sinosoft.lis.llcase;



/**
 * <p>Title: LLBranchCaseReportBL</p>
 * <p>Description: 承保赔付汇总表</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author MN
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.CSVFileWrite;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.LLPrintSave;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.io.InputStream;
import java.sql.*;

public class LLClaimFiledSubExcelBL {
    public LLClaimFiledSubExcelBL () {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    
    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mStartDate = "";	//生效起期
    private String mEndDate = "";	//生效止期
    private String organcode = "";	//登录机构
	
	private VData mInputData = new VData();
    private XmlExport mXmlExport = null;

    private TransferData mTransferData = new TransferData();
    private CSVFileWrite mCSVFileWrite = null;
    private String mFilePath = "";
    private String mFileName = "";
    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
    	mInputData = (VData) cInputData;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(mInputData)) {
            return false;
        }
       
        

        // 进行数据查询
        if (!queryData()) {
            return false;
        } 
     

        return true;
    }

    /**
     * 取得传入的数据
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
       // mOperator = mGlobalInput.Operator;
        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        mResult = (VData) cInputData.getObjectByObjectName("VData", 0);
       
        mStartDate = (String) mTransferData.getValueByName("tStartDate");//结案起期
        mEndDate = (String) mTransferData.getValueByName("tEndDate");//结案止期
        organcode = (String) mTransferData.getValueByName("organcode");//结案止期
        
        return true;
    }
   
    
    /**
     * 进行数据查询
     * @return boolean
     */
    private boolean queryData() {

        
        
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
      	tLDSysVarDB.setSysVar("LPCSVREPORT");
//
      	if (!tLDSysVarDB.getInfo()) {
      		buildError("queryData", "查询文件路径失败");
      		return false;
      	}
      	
      	mFilePath = tLDSysVarDB.getSysVarValue(); 
          //   	本地测试
      	//mFilePath="E:\\picch\\ui\\vtsfile\\";
      	if (mFilePath == null || "".equals(mFilePath)) {
      		buildError("queryData", "查询文件路径失败");
      		return false;
      	}
      	System.out.println("XXXXXXXXXXXXXXXXXXXmFilePath:"+mFilePath);
      	String tTime = PubFun.getCurrentTime3().replaceAll(":", "");
      	String tDate = PubFun.getCurrentDate2();

      	mFileName = "Main" + mGlobalInput.Operator + tDate + tTime ;
      	System.out.println("XXXXXXXXXXXXXXXXXXXmFileName:"+mFileName);
      	mCSVFileWrite = new CSVFileWrite(mFilePath, mFileName);   
      	
      	
      	String[][] tTitle = new String[2][];	
      	tTitle[0] = new String[] { mStartDate+"--"+mEndDate ,"理赔案卷存档清单"};
      	System.out.println("tTitle000:"+tTitle[0]);
      	tTitle[1] = new String[] { "序号","案卷编号","客户姓名","理赔结论","处理人","结案时间","申请材料页数","非申请材料页数","备注"};
       	System.out.println("tTitle111:"+tTitle[1]);
      	String[] tContentType =  {"Number","String","String","String","String","String","Number","Number","String"};
      	if (!mCSVFileWrite.addTitle(tTitle,tContentType)) {
      		return false;
      	}
        if (!getDataList())
        {
          return false;
        }

        mCSVFileWrite.closeFile();

      	return true;
    }

   
    private boolean getDataList(){
    	String com = " ";
    	if(!(organcode==null||organcode.equals(""))){
			String  tManage="";
			if(organcode.length()>=4){
			  tManage = organcode.substring(0,4)+ "0000" ;
			  System.out.println("tManage="+tManage);
			  System.out.println("organcode="+organcode);
			}else{
			  tManage=organcode+ "0000" ;
			}
			
			if(organcode.equals(tManage)){
				if(organcode.equals("86000000")){
				   com="and a.MngCom like '86%' ";
				}else{
	 		     com="and a.MngCom ='"+tManage+"'";
	 		  }
	 	  }else{
	 		  com="and a.MngCom like '"+organcode+"%'";
	 		}
}
System.out.println(com);
    	
    	
    	String SQL = 
    		"select to_char(x),a,b,c,d,e,'','',''  from " 
    		+" (select rownumber() over() x," 
    		+" a.caseno a," 
    		+" a.customername b," 
    		+" b.givetypedesc c," 
    		+" (select username from lduser where usercode=a.Handler) d," 
    		+" a.EndCaseDate e " 
    		+" from llcase a," 
    		+" llclaim b " 
    		+" where a.caseno=b.caseno  and a.RgtState in ('12','11','09') " 
    		+" "+com+" "
    		+" and a.rgtdate between  '"+mStartDate+"'and '"+mEndDate+"' ) " 	
    		+" z order by a,b,c,d with ur";
    	 ExeSQL tExeSQL = new ExeSQL();
        
        	 
         try{
        
         //SSRS tSSRS = new SSRS();
         SSRS tSSRS = tExeSQL.execSQL(SQL);
        	
             if (tSSRS != null || tSSRS.MaxRow > 0){  
            	 String tContent[][] = new String[tSSRS.getMaxRow()][];
             	 for (int i = 1; i <= tSSRS.getMaxRow(); i++){
             		 String ListInfo[] = new String[9];
             		 
            	 //String ListInfo[] = new String[8];

            		// tEndDate = tSSRS.GetText(i, 5);
        		 ListInfo[0] = tSSRS.GetText(i, 1);
        		 ListInfo[1] = tSSRS.GetText(i, 2);
        		 ListInfo[2] = tSSRS.GetText(i, 3);
        		 ListInfo[3] = tSSRS.GetText(i, 4);
        		 ListInfo[4] = tSSRS.GetText(i, 5);
        		 ListInfo[5] = tSSRS.GetText(i, 6);
        		 ListInfo[6] = tSSRS.GetText(i, 7);
        		 ListInfo[7] = tSSRS.GetText(i, 8);
        		 ListInfo[8] = tSSRS.GetText(i, 9);
        	 
        		 
        		 tContent[i - 1] = ListInfo;
            	 }
                mCSVFileWrite.addContent(tContent);
             	//mCSVFileWrite.writeFile();
             	if (!mCSVFileWrite.writeFile()) {
        			mErrors.addOneError(mCSVFileWrite.mErrors.getFirstError());
        			return false;
        		}
         	}
             else{
            	// String ListInfo[]=new String[8];
            	 String ListInfo[]={"","","","","","","","",""};	
            	 ListInfo[0]="";
            	 ListInfo[1]="";
            	 ListInfo[2]="";
            	 ListInfo[3]="";
            	 ListInfo[4]="";
            	 ListInfo[5]="";
            	 ListInfo[6]="";
            	 ListInfo[7]="";
            	 ListInfo[8]="";
             }
         
    
         }catch (Exception ex){
        	 System.out.println("error");
         }
         return true;
     }


    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "OLLMainAskBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 取得返回处理过的结果
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }
	public String getMFilePath() {
		return mFilePath;
	}

	public void setMFilePath(String filePath) {
		mFilePath = filePath;
	}

	public String getMFileName() {
		return mFileName;
	}

	public void setMFileName(String fileName) {
		mFileName = fileName;
	}
    
	public InputStream getInputStream() {
        return mXmlExport.getInputStream();
    }

    private void jbInit() throws Exception {
    }
}





