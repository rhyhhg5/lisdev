package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;

public class PEdorUWManuRReportQueryUI
{
    private PEdorUWManuRReportQueryBL mPEdorUWManuRReportQueryBL = null;

    /**
     * 构造函数
     */
    public PEdorUWManuRReportQueryUI()
    {
        mPEdorUWManuRReportQueryBL = new PEdorUWManuRReportQueryBL();
    }

    /**
     * 调用业务逻辑
     * @param data VData
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!mPEdorUWManuRReportQueryBL.submitData(data))
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误信息
     * @return String
     */
    public String getError()
    {
        return mPEdorUWManuRReportQueryBL.mErrors.getFirstError();
    }
}
