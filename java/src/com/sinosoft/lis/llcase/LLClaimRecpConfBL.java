package com.sinosoft.lis.llcase;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author cc
 * @version 1.0
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;

public class LLClaimRecpConfBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private String mOperate = "";
    private String SQL = "";
    //业务处理相关变量
    /** 全局数据 */

    MMap map = new MMap();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LDCodeSet mAddLDCodeSet = new LDCodeSet();
    private LDCodeSet mDelLDCodeSet = new LDCodeSet();

    private String strUrl = "";

    public LLClaimRecpConfBL() {
    }

    /**
         传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!cOperate.equals("DELETE&INSERT")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 进行业务处理
        if (!dealData()) {
            return false;
        }

        // 准备传往后台的数据
        VData vData = new VData();

        if (!prepareOutputData(vData)) {
            return false;
        }

        //　数据提交、保存

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start PubSubmit Submit...");

        if (!tPubSubmit.submitData(vData, "")) {
            // @@错误处理ssss
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "LLClaimRecpConfBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("---commitData---");
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData) {
        try {
            vData.clear();
            System.out.println("放入 map 之前......");

            mOperate = "('";
            for (int d = 1; d <= mAddLDCodeSet.size(); d++) {
                if (d == 1) {
                    mOperate = mOperate + mAddLDCodeSet.get(d).getCodeAlias();
                } else {
                    mOperate = mOperate + "','" +
                               mAddLDCodeSet.get(d).getCodeAlias();
                }
            }
            mOperate = mOperate + "')";
            SQL =
                    "delete from ldcode where codetype = 'llsecufeeitem' and comcode = '" +
                    mAddLDCodeSet.get(1).getComCode() + "' and codealias in "+ mOperate;
            map.put(SQL, "DELETE");
            map.put(mAddLDCodeSet, "INSERT");
            vData.add(map);
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mAddLDCodeSet = (LDCodeSet) cInputData.getObjectByObjectName(
                "LDCodeSet", 0);
        System.out.println("##############");
        System.out.println("##############");
        System.out.println(mAddLDCodeSet.get(1).getCode());
        System.out.println("##############");
        System.out.println("##############");
        mDelLDCodeSet = (LDCodeSet) cInputData.getObjectByObjectName(
                "LDCodeSet", 1);
        System.out.println(mAddLDCodeSet.get(1).getCodeAlias());
        if (mAddLDCodeSet == null || mDelLDCodeSet == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "DerferAppF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
