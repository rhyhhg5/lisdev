/**
 * 
 */
package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * 检录-保存出险详细信息
 * 
 * @author maning
 * @version 1.0
 */
public class LLCaseRegisterCheck extends LLCaseProcess {

	/**
	 * 
	 */
	public LLCaseRegisterCheck() {
		// TODO Auto-generated constructor stub
	}

	// 理赔信息缓存
	private CachedLPInfo mCachedLPInfo = CachedLPInfo.getInstance();

	// 业务逻辑错误
	public CErrors mErrors = new CErrors();

	// 获取的业务数据
	private VData mInputData = new VData();

	// 返回处理后数据
	private VData mResult = new VData();

	// 返回的提交数据库MMap
	private MMap mMMap = new MMap();

	// 用户登录信息
	private GlobalInput mGlobalInput = new GlobalInput();

	// 操作类型
	private String mOperate = null;

	// 案件信息
	private LLCaseSchema mLLCaseSchema = null;

	// 事件关联
	private LLCaseRelaSchema mLLCaseRelaSchema = null;

	// 账单信息
	private LLFeeMainSet mLLFeeMainSet = null;

	// 疾病信息
	private LLCaseCureSet mLLCaseCureSet = null;

	// 意外信息
	private LLAccidentSet mLLAccidentSet = null;

	// 残疾信息
	private LLCaseInfoSet mLLCaseInfoSet = null;

	// 当前日期
	private String mCurrentDate = PubFun.getCurrentDate();

	// 当前时间
	private String mCurrentTime = PubFun.getCurrentTime();

	private String mNoLimit = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sinosoft.lis.llcase.LLCaseProcess#getMMap()
	 */
	public MMap getMMap() throws Exception {
		if (mLLFeeMainSet != null && mLLFeeMainSet.size() > 0) {
			mMMap.put(mLLFeeMainSet, "DELETE&INSERT");
		}
		if (mLLCaseCureSet != null && mLLCaseCureSet.size() > 0) {
			mMMap.put(mLLCaseCureSet, "INSERT");
		}
		if (mLLAccidentSet != null && mLLAccidentSet.size() > 0) {
			mMMap.put(mLLAccidentSet, "INSERT");
		}
		if (mLLCaseInfoSet != null && mLLCaseInfoSet.size() > 0) {
			mMMap.put(mLLCaseInfoSet, "INSERT");
		}
		return mMMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sinosoft.lis.llcase.LLCaseProcess#getResult()
	 */
	public VData getResult() throws Exception {
		if (mLLFeeMainSet != null && mLLFeeMainSet.size() > 0) {
			mResult.add(mLLFeeMainSet);
		}
		if (mLLCaseCureSet != null && mLLCaseCureSet.size() > 0) {
			mResult.add(mLLCaseCureSet);
		}
		if (mLLAccidentSet != null && mLLAccidentSet.size() > 0) {
			mResult.add(mLLAccidentSet);
		}
		if (mLLCaseInfoSet != null && mLLCaseInfoSet.size() > 0) {
			mResult.add(mLLCaseInfoSet);
		}
		return mResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sinosoft.lis.llcase.LLCaseProcess#submitData(com.sinosoft.utility.VData,
	 *      java.lang.String)
	 */
	public boolean submitData(VData aInputData, String aOperate)
			throws Exception {
		System.out.println("开始案件检录处理");
		mInputData = aInputData;
		mOperate = aOperate;

		if (!getInputData()) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		 //提交数据库
        PubSubmit tPubSubmit = new PubSubmit();
        mInputData.clear();       
        mInputData.add(getMMap());
        if (!tPubSubmit.submitData(mInputData, ""))
        {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LLCaseRegisterCheck";
                tError.functionName = "dealData";
                tError.errorMessage = "理赔检录数据保存失败!";
                this.mErrors.addOneError(tError);
                return false;
        }
		System.out.println("案件检录处理结束");
		return true;
	}

	/**
	 * 解析获取的对象
	 * 
	 * @return 获取数据成功标志
	 */
	private boolean getInputData() throws Exception {
		mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0);

		if (mGlobalInput == null) {
			mErrors.addOneError("用户登录信息获取失败");
			return false;
		}

		mLLCaseSchema = (LLCaseSchema) mInputData.getObjectByObjectName(
				"LLCaseSchema", 0);

		if (mLLCaseSchema == null) {
			mErrors.addOneError("案件信息获取失败");
			return false;
		}

		mLLCaseRelaSchema = (LLCaseRelaSchema) mInputData
				.getObjectByObjectName("LLCaseRelaSchema", 0);

		if (mLLCaseRelaSchema == null) {
			mErrors.addOneError("事件关联信息获取失败");
			return false;
		}

		mLLFeeMainSet = (LLFeeMainSet) mInputData.getObjectByObjectName(
				"LLFeeMainSet", 0);

		mLLCaseCureSet = (LLCaseCureSet) mInputData.getObjectByObjectName(
				"LLCaseCureSet", 0);

		mLLAccidentSet = (LLAccidentSet) mInputData.getObjectByObjectName(
				"LLAccidentSet", 0);

		mLLCaseInfoSet = (LLCaseInfoSet) mInputData.getObjectByObjectName(
				"LLCaseInfoSet", 0);

		return true;
	}

	/**
	 * 基本校验
	 * 
	 * @return 成功标志
	 * @throws Exception
	 */
	private boolean checkData() throws Exception {

		if (mGlobalInput.Operator == null || "".equals(mGlobalInput.Operator)) {
			mErrors.addOneError("操作用户登录信息获取失败");
			return false;
		}

		if (mGlobalInput.ManageCom == null || "".equals(mGlobalInput.ManageCom)) {
			mErrors.addOneError("操作用户登录信息获取失败");
			return false;
		}

		mNoLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
		if (mNoLimit == null || "".equals(mNoLimit)) {
			mErrors.addOneError("序列号生成失败");
			return false;
		}

		if (mLLCaseSchema.getCaseNo() == null
				|| "".equals(mLLCaseSchema.getCaseNo())) {
			mErrors.addOneError("案件号获取失败");
			return false;
		}

		if (mLLCaseSchema.getRgtNo() == null
				|| "".equals(mLLCaseSchema.getRgtNo())) {
			mErrors.addOneError("批次号获取失败");
			return false;
		}

		if (!"01".equals(mLLCaseSchema.getRgtState())
				&& !"02".equals(mLLCaseSchema.getRgtState())
				&& !"08".equals(mLLCaseSchema.getRgtState())) {
			mErrors.addOneError("该案件状态下不能进行检录操作");
			return false;
		}

		if (mLLCaseRelaSchema.getCaseNo() == null
				|| "".equals(mLLCaseRelaSchema.getCaseNo())
				|| mLLCaseRelaSchema.getCaseRelaNo() == null
				|| "".equals(mLLCaseRelaSchema.getCaseRelaNo())
				|| mLLCaseRelaSchema.getSubRptNo() == null
				|| "".equals(mLLCaseRelaSchema.getSubRptNo())) {
			mErrors.addOneError("事件关联信息获取不完整");
			return false;
		}

		return true;
	}

	/**
	 * 业务处理
	 * 
	 * @return 处理成功标志
	 * @throws Exception
	 */
	private boolean dealData() throws Exception {
		// 处理LLFeeMain表
		if (!dealLLFeeMain()) {
			return false;
		}

		// 处理LLCaseCure表
		if (!dealLLCaseCure()) {
			return false;
		}

		// 处理LLAccident表
		if (!dealLLAccident()) {
			return false;
		}

		// 处理LLCaseInfo表
		if (!dealLLCaseInfo()) {
			return false;
		}

		return true;
	}

	/**
	 * 处理账单主表
	 * 
	 * @return
	 * @throws Exception
	 */
	private boolean dealLLFeeMain() throws Exception {
		if (mLLFeeMainSet == null) {
			return true;
		}

		LLFeeMainSet tLLFeeMainSet = new LLFeeMainSet();
		tLLFeeMainSet.set(mLLFeeMainSet);
		mLLFeeMainSet = new LLFeeMainSet();

		for (int i = 1; i <= tLLFeeMainSet.size(); i++) {
			LLFeeMainSchema tLLFeeMainSchema = tLLFeeMainSet.get(i);
			tLLFeeMainSchema.setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());
			tLLFeeMainSchema.setOperator(mGlobalInput.Operator);
			tLLFeeMainSchema.setModifyDate(mCurrentDate);
			tLLFeeMainSchema.setModifyTime(mCurrentTime);
			mLLFeeMainSet.add(tLLFeeMainSchema);
		}

		return true;
	}

	/**
	 * 处理LLCaseCure
	 * 
	 * @return
	 * @throws Exception
	 */
	private boolean dealLLCaseCure() throws Exception {
		if (mLLCaseCureSet == null) {
			return true;
		}

		LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
		tLLCaseCureSet.set(mLLCaseCureSet);
		mLLCaseCureSet = new LLCaseCureSet();

		for (int i = 1; i <= tLLCaseCureSet.size(); i++) {
			LLCaseCureSchema tTempLLCaseCureSchema = tLLCaseCureSet.get(i);
			LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();

			if (tTempLLCaseCureSchema.getDiseaseCode() == null
					|| "".equals(tTempLLCaseCureSchema.getDiseaseCode())) {
				mErrors.addOneError("疾病代码获取失败");
				return false;
			}

			LDDiseaseDB tLDDiseaseDB = new LDDiseaseDB();
			tLDDiseaseDB.setICDCode(tTempLLCaseCureSchema.getDiseaseCode());
			if (!tLDDiseaseDB.getInfo()) {
				mErrors.addOneError("疾病代码错误");
				return false;
			}

			// 案件号
			tLLCaseCureSchema.setCaseNo(mLLCaseSchema.getCaseNo());

			// 事件关联号
			tLLCaseCureSchema.setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());

			// 医院编码
			tLLCaseCureSchema.setHospitalCode(tTempLLCaseCureSchema
					.getHospitalCode());

			// 医院名称
			tLLCaseCureSchema.setHospitalName(tTempLLCaseCureSchema
					.getHospitalName());

			// 医生代码
			tLLCaseCureSchema.setDoctorNo(tTempLLCaseCureSchema.getDoctorNo());

			// 医生姓名
			tLLCaseCureSchema.setDoctorName(tTempLLCaseCureSchema
					.getDoctorName());

			// 客户号
			tLLCaseCureSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());

			// 客户姓名
			tLLCaseCureSchema.setCustomerName(mLLCaseSchema.getCustomerName());

			// 出险类型
			tLLCaseCureSchema.setAccidentType(tTempLLCaseCureSchema
					.getAccidentType());

			// 门诊/住院号码
			tLLCaseCureSchema.setCureNo(tTempLLCaseCureSchema.getCureNo());

			// 住院收据号码
			tLLCaseCureSchema
					.setReceiptNo(tTempLLCaseCureSchema.getReceiptNo());

			// 入院日期
			tLLCaseCureSchema.setInHospitalDate(tTempLLCaseCureSchema
					.getInHospitalDate());

			// 出院日期
			tLLCaseCureSchema.setOutHospitalDate(tTempLLCaseCureSchema
					.getOutHospitalDate());

			// 实际住院天数
			tLLCaseCureSchema.setInHospitalDays(tTempLLCaseCureSchema
					.getInHospitalDays());

			// 重疾标记 0-一般疾病
			tLLCaseCureSchema.setSeriousFlag("0");

			// 疾病代码
			tLLCaseCureSchema.setDiseaseCode(tLDDiseaseDB.getICDCode());

			// 疾病名称
			tLLCaseCureSchema.setDiseaseName(tLDDiseaseDB.getICDName());

			// 确诊日期
			tLLCaseCureSchema.setDiagnoseDate(tTempLLCaseCureSchema
					.getDiagnoseDate());

			// 确诊依据
			tLLCaseCureSchema.setDiagnoseDesc(tTempLLCaseCureSchema
					.getDiagnoseDesc());

			// 临床诊断
			tLLCaseCureSchema.setDiagnose(tTempLLCaseCureSchema.getDiagnose());

			// 鉴定单位
			tLLCaseCureSchema.setJudgeOrgan(tTempLLCaseCureSchema
					.getJudgeOrgan());

			// 鉴定单位名称
			tLLCaseCureSchema.setJudgeOrganName(tTempLLCaseCureSchema
					.getJudgeOrganName());

			// 鉴定时间
			tLLCaseCureSchema
					.setJudgeDate(tTempLLCaseCureSchema.getJudgeDate());

			// 鉴定依据
			tLLCaseCureSchema.setJudgeDepend(tTempLLCaseCureSchema
					.getJudgeDepend());

			// 主要疾病标志
			tLLCaseCureSchema.setMainDiseaseFlag(tTempLLCaseCureSchema
					.getMainDiseaseFlag());

			// 赔付标志
			tLLCaseCureSchema.setClmFlag(tTempLLCaseCureSchema.getClmFlag());

			// 申请赔付金额
			tLLCaseCureSchema.setApplyFee(tTempLLCaseCureSchema.getApplyFee());

			// 管理机构
			tLLCaseCureSchema.setMngCom(mGlobalInput.ManageCom);

			// 操作员
			tLLCaseCureSchema.setOperator(mGlobalInput.Operator);

			// 入机日期
			tLLCaseCureSchema.setMakeDate(mCurrentDate);

			// 入机时间
			tLLCaseCureSchema.setMakeTime(mCurrentTime);

			// 修改日期
			tLLCaseCureSchema.setModifyDate(mCurrentDate);

			// 修改时间
			tLLCaseCureSchema.setModifyTime(mCurrentTime);

			// 有无手术标志
			tLLCaseCureSchema.setOperationFlag(tTempLLCaseCureSchema
					.getOperationFlag());

			// 重疾责任标记
			tLLCaseCureSchema.setSeriousDutyFlag(tTempLLCaseCureSchema
					.getSeriousDutyFlag());

			// 风险等级
			tLLCaseCureSchema.setRiskLevel(tLDDiseaseDB.getRiskLevel());

			// 风险存续期
			tLLCaseCureSchema.setRiskContinueFlag(tLDDiseaseDB
					.getRiskContinueFlag());

			// 序号
			String tCureNo = PubFun1.CreateMaxNo("CURENO", mNoLimit);
			if (tCureNo == null || "".equals("tCureNo")) {
				mErrors.addOneError("序号生成失败");
				return false;
			}
			tLLCaseCureSchema.setSerialNo(tCureNo);

			mLLCaseCureSet.add(tLLCaseCureSchema);

		}
		return true;
	}

	/**
	 * 处理意外信息 LLAccident
	 * 
	 * @return
	 * @throws Exception
	 */
	private boolean dealLLAccident() throws Exception {
		if (mLLAccidentSet == null) {
			return true;
		}

		LLAccidentSet tLLAccidentSet = new LLAccidentSet();
		tLLAccidentSet.set(mLLAccidentSet);
		mLLAccidentSet = new LLAccidentSet();

		for (int i = 1; i <= tLLAccidentSet.size(); i++) {
			LLAccidentSchema tTempLLAccidentSchema = tLLAccidentSet.get(i);
			LLAccidentSchema tLLAccidentSchema = new LLAccidentSchema();
			if (tTempLLAccidentSchema.getCode() == null
					|| "".equals(tTempLLAccidentSchema.getCode())) {
				mErrors.addOneError("意外代码获取失败");
				return false;
			}

			LLAccidentTypeDB tLLAccidentTypeDB = new LLAccidentTypeDB();
			tLLAccidentTypeDB.setAccidentNo(tTempLLAccidentSchema.getCode());
			if (!tLLAccidentTypeDB.getInfo()) {
				mErrors.addOneError("意外代码错误");
				return false;
			}

			// 意外代码
			tLLAccidentSchema.setCode(tLLAccidentTypeDB.getAccidentNo());

			// 意外名称
			tLLAccidentSchema.setName(tLLAccidentTypeDB.getAccName());

			// 案件号
			tLLAccidentSchema.setCaseNo(mLLCaseSchema.getCaseNo());

			// 事件关联号
			tLLAccidentSchema.setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());

			// 分案事_案件号
			tLLAccidentSchema.setLLC_CaseNo(tTempLLAccidentSchema
					.getLLC_CaseNo());

			// 事件号
			tLLAccidentSchema.setSubRptNo(tTempLLAccidentSchema.getSubRptNo());

			// 意外类型
			tLLAccidentSchema.setType(tTempLLAccidentSchema.getType());

			// 意外类型名称
			tLLAccidentSchema.setTypeName(tTempLLAccidentSchema.getTypeName());

			// 意外原因代码
			tLLAccidentSchema.setReasonCode(tTempLLAccidentSchema
					.getReasonCode());

			// 意外原因名称
			tLLAccidentSchema.setReason(tTempLLAccidentSchema.getReason());

			// 备注
			tLLAccidentSchema.setRemark(tTempLLAccidentSchema.getRemark());

			// 序号

			String tAccidentNo = PubFun1.CreateMaxNo("ACCIDENTNO", mNoLimit);
			if (tAccidentNo == null || "".equals(tAccidentNo)) {
				mErrors.addOneError("意外序号生成失败");
				return false;
			}
			tLLAccidentSchema.setAccidentNo(tAccidentNo);
			mLLAccidentSet.add(tLLAccidentSchema);

		}

		return true;
	}

	/**
	 * 处理残疾信息 LLCaseInfo
	 * 
	 * @return
	 * @throws Exception
	 */
	private boolean dealLLCaseInfo() throws Exception {
		if (mLLCaseInfoSet == null) {
			return true;
		}

		LLCaseInfoSet tLLCaseInfoSet = new LLCaseInfoSet();
		tLLCaseInfoSet.set(mLLCaseInfoSet);
		mLLCaseInfoSet = new LLCaseInfoSet();
		for (int i = 1; i <= tLLCaseInfoSet.size(); i++) {
			LLCaseInfoSchema tTempLLCaseInfoSchema = tLLCaseInfoSet.get(i);
			LLCaseInfoSchema tLLCaseInfoSchema = new LLCaseInfoSchema();

			if (tTempLLCaseInfoSchema.getCode() == null
					|| "".equals(tTempLLCaseInfoSchema.getCode())) {
				mErrors.addOneError("残疾代码获取失败");
				return false;
			}

			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("desc_wound");
			tLDCodeDB.setCode(tTempLLCaseInfoSchema.getCode());
			if (!tLDCodeDB.getInfo()) {
				mErrors.addOneError("残疾代码错误");
				return false;
			}

			// 案件号
			tLLCaseInfoSchema.setCaseNo(mLLCaseSchema.getCaseNo());

			// 批次号
			tLLCaseInfoSchema.setRgtNo(mLLCaseSchema.getRgtNo());

			// 事件关联号
			tLLCaseInfoSchema.setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());

			// 客户号
			tLLCaseInfoSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());

			// 客户姓名
			tLLCaseInfoSchema.setCustomerName(mLLCaseSchema.getCustomerName());

			// 类型
			tLLCaseInfoSchema.setType(tLDCodeDB.getCode().substring(0, 1));

			// 残疾代码
			tLLCaseInfoSchema.setCode(tLDCodeDB.getCode());

			// 诊断
			tLLCaseInfoSchema
					.setDiagnosis(tTempLLCaseInfoSchema.getDiagnosis());

			// 赔付标志
			tLLCaseInfoSchema.setClmFlag(tTempLLCaseInfoSchema.getClmFlag());

			// 残疾类型
			tLLCaseInfoSchema.setDeformityKind(tTempLLCaseInfoSchema
					.getDeformityKind());

			// 伤残级别
			if (tTempLLCaseInfoSchema.getDeformityGrade() == null
					|| "".equals(tTempLLCaseInfoSchema.getDeformityGrade())) {
				tLLCaseInfoSchema.setDeformityGrade(tLDCodeDB.getComCode());
			} else {
				tLLCaseInfoSchema.setDeformityGrade(tTempLLCaseInfoSchema
						.getDeformityGrade());
			}

			// 伤残级别名称
			tLLCaseInfoSchema
					.setGradeName(tTempLLCaseInfoSchema.getGradeName());

			// 残疾部位编码
			tLLCaseInfoSchema.setPartCode(tTempLLCaseInfoSchema.getPartCode());

			// 残疾原因
			tLLCaseInfoSchema.setDeformityReason(tTempLLCaseInfoSchema
					.getDeformityReason());

			// 残疾给付比例
			if (tTempLLCaseInfoSchema.getDeformityRate() <= 0) {
				tLLCaseInfoSchema.setDeformityRate(tLDCodeDB.getOtherSign());
			} else {
				tLLCaseInfoSchema.setDeformityRate(tTempLLCaseInfoSchema
						.getDeformityRate());
			}

			// 申请给付比例
			tLLCaseInfoSchema.setAppDeformityRate(tTempLLCaseInfoSchema
					.getAppDeformityRate());

			// 实际给付比例
			tLLCaseInfoSchema.setRealRate(tTempLLCaseInfoSchema.getRealRate());

			// 病例号码
			tLLCaseInfoSchema.setNo(tTempLLCaseInfoSchema.getNo());

			// 确诊日期
			if (tTempLLCaseInfoSchema.getDianoseDate() != null
					&& !PubFun.checkDateForm(tTempLLCaseInfoSchema
							.getDianoseDate())) {
				mErrors.addOneError("确诊日期格式错误");
				return false;
			}
			tLLCaseInfoSchema.setDianoseDate(tTempLLCaseInfoSchema
					.getDianoseDate());

			// 确诊依据
			tLLCaseInfoSchema.setDiagnoseDesc(tTempLLCaseInfoSchema
					.getDiagnoseDesc());

			// 诊断医院代码
			tLLCaseInfoSchema.setHospitalCode(tTempLLCaseInfoSchema
					.getHospitalCode());

			// 诊断医院名称
			tLLCaseInfoSchema.setHospitalName(tTempLLCaseInfoSchema
					.getHospitalName());

			// 诊断医生号码
			tLLCaseInfoSchema.setDoctorNo(tTempLLCaseInfoSchema.getDoctorNo());

			// 诊断医生名称
			tLLCaseInfoSchema.setDoctorName(tTempLLCaseInfoSchema
					.getDoctorName());

			// 鉴定单位
			tLLCaseInfoSchema.setJudgeOrgan(tTempLLCaseInfoSchema
					.getJudgeOrgan());

			// 鉴定单位名称
			tLLCaseInfoSchema.setJudgeOrganName(tTempLLCaseInfoSchema
					.getJudgeOrganName());

			// 鉴定时间
			if (tTempLLCaseInfoSchema.getJudgeDate() != null
					&& !PubFun.checkDateForm(tTempLLCaseInfoSchema
							.getJudgeDate())) {
				mErrors.addOneError("鉴定日期格式错误");
				return false;
			}
			tLLCaseInfoSchema
					.setJudgeDate(tTempLLCaseInfoSchema.getJudgeDate());

			// 鉴定依据
			tLLCaseInfoSchema.setJudgeDepend(tTempLLCaseInfoSchema
					.getJudgeDepend());

			// 管理机构
			tLLCaseInfoSchema.setMngCom(mGlobalInput.ManageCom);

			// 操作人
			tLLCaseInfoSchema.setOperator(mGlobalInput.Operator);

			// 入机日期
			tLLCaseInfoSchema.setMakeDate(mCurrentDate);

			// 入机时间
			tLLCaseInfoSchema.setMakeTime(mCurrentTime);

			// 修改日期
			tLLCaseInfoSchema.setModifyDate(mCurrentDate);

			// 修改时间
			tLLCaseInfoSchema.setModifyTime(mCurrentTime);

			// 序号
			String tCureNo = PubFun1.CreateMaxNo("CASEINFO", mNoLimit);
			if (tCureNo == null || "".equals(tCureNo)) {
				mErrors.addOneError("残疾序号生成失败");
				return false;
			}
			tLLCaseInfoSchema.setSerialNo(tCureNo);

			mLLCaseInfoSet.add(tLLCaseInfoSchema);
		}
		return true;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
