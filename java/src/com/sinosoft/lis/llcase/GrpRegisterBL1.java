package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.CommonBL;

public class GrpRegisterBL1 {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();
  private VData mResult = new VData();

  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  private MMap map = new MMap();

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();

  /** 数据操作字符串 */
  private String mOperate;

  /** 业务处理相关变量 */
  private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
  private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
  private LLRegisterSchema aLLRegisterSchema = new LLRegisterSchema();
  private LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();

//private LDDrugSet mLDDrugSet=new LDDrugSet();
  public GrpRegisterBL1() {
  }

  public static void main(String[] args) {
      String tLimit = PubFun.getNoLimit("86210000");
      String RGTNO = PubFun1.CreateMaxNo("CASENO", tLimit);
      VData tno = new VData();
      tno.add(0,12+"");
      tno.add(1,RGTNO);
      tno.add(2,"MAX");
      PubFun1.CreateMaxNo("CaseNo",tLimit,tno);
      System.out.println("getInputData start......");
  }

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
    {
      return false;
    }
    else
    {
        System.out.println("getinputdata success!");
    }

    //进行业务处理
    if (!dealData()) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OLDDrugBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据处理失败OLDDrugBL-->dealData!";
      this.mErrors.addOneError(tError);
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;

    else {
      System.out.println("Start GrpRegisterBL Submit...");
      PubSubmit tPubSubmit = new PubSubmit();
      if (!tPubSubmit.submitData(mInputData, mOperate))
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        CError tError = new CError();
        tError.moduleName = "GrpRegisterBL";
        tError.functionName = "GrpRegisterBL";
        tError.errorMessage = "数据提交失败!";
        this.mErrors.addOneError(tError);
        return false;
      }
      System.out.println("End GrpRegisterBL Submit...");
    }
    mInputData = null;
    return true;
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
      System.out.println("===========mOperate=========="+mOperate);
    if (this.mOperate.equals("INSERT||MAIN"))
    {
        String tGrpContNo = mLLRegisterSchema.getRgtObjNo();
        if(CommonBL.afterMJCalculate(tGrpContNo)){
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "保单管理正在对该团单进行满期结算、追加保费、部分领取或账户分配操作，请先通知保全撤销相关操作，再进行理赔!";
            this.mErrors.addOneError(tError);
            return false;
        }
      LLCaseCommon tLLCaseCommon = new LLCaseCommon();
      String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
      System.out.println("管理机构代码是 : "+tLimit);
      String RGTNO = PubFun1.CreateMaxNo("RGTNO", tLimit);
      String CaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit);
      VData tno = new VData();
      tno.add(0,mLLRegisterSchema.getAppPeoples()+"");
      tno.add(1,RGTNO);
      tno.add(2,"MAX");
      PubFun1.CreateMaxNo("CaseNo",tLimit,tno);
      System.out.println("getInputData start......");
      mLLRegisterSchema.setRgtNo(RGTNO);
      mLLRegisterSchema.setRgtState("01");
      mLLRegisterSchema.setHandler1(mGlobalInput.Operator);
      mLLRegisterSchema.setRgtDate(PubFun.getCurrentDate());
      mLLRegisterSchema.setMngCom(mGlobalInput.ManageCom);
      mLLRegisterSchema.setOperator(mGlobalInput.Operator);
      mLLRegisterSchema.setMakeDate(PubFun.getCurrentDate());
      mLLRegisterSchema.setMakeTime(PubFun.getCurrentTime());
      mLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
      mLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());
      map.put(mLLRegisterSchema, "INSERT");
      //在插入llregister标的同时插入llcase
      mLLCaseSchema.setCaseNo(CaseNo);
      mLLCaseSchema.setRgtNo(RGTNO);
      mLLCaseSchema.setRgtType("5");              //rgttype=5定义为弹性拨付
      mLLCaseSchema.setRgtState("01");
      mLLCaseSchema.setCustomerNo(tGrpContNo);
      mLLCaseSchema.setCustomerName(mLLRegisterSchema.getGrpName());
      mLLCaseSchema.setRgtDate(PubFun.getCurrentDate());
      mLLCaseSchema.setHandler(mGlobalInput.Operator);           //下阶段处理人即为立案人员
      mLLCaseSchema.setDealer(mGlobalInput.Operator);

      mLLCaseSchema.setRigister(mGlobalInput.ManageCom);         //立案人员
      mLLCaseSchema.setOperator(mGlobalInput.Operator);
      mLLCaseSchema.setMngCom(mGlobalInput.ManageCom);
      mLLCaseSchema.setRgtDate(PubFun.getCurrentDate());
      mLLCaseSchema.setMakeDate(PubFun.getCurrentDate());
      mLLCaseSchema.setMakeTime(PubFun.getCurrentTime());
      mLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
      mLLCaseSchema.setModifyTime(PubFun.getCurrentTime());
      map.put(mLLCaseSchema, "INSERT");

      mLLCaseOpTimeSchema.setCaseNo(CaseNo);
      mLLCaseOpTimeSchema.setRgtState("01");
      mLLCaseOpTimeSchema.setSequance(1);
      mLLCaseOpTimeSchema.setStartDate(PubFun.getCurrentDate());
      mLLCaseOpTimeSchema.setStartTime(PubFun.getCurrentTime());
      mLLCaseOpTimeSchema.setOpTime("1");
      mLLCaseOpTimeSchema.setEndDate(PubFun.getCurrentDate());
      mLLCaseOpTimeSchema.setEndTime(PubFun.getCurrentTime());
      mLLCaseOpTimeSchema.setOperator(mGlobalInput.Operator);
      mLLCaseOpTimeSchema.setManageCom(mGlobalInput.ManageCom);

      map.put(mLLCaseOpTimeSchema, "INSERT");


    }
    if (this.mOperate.equals("UPDATE||MAIN")) {
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        if(!tLLRegisterDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "团体立案信息查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLLRegisterDB.getRgtState().equals("02"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "团体立案已经全部受理申请完毕!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLLRegisterDB.getRgtState().equals("03"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "团体立案已经全部结案完毕!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLLRegisterDB.getRgtState().equals("04"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "团体立案已经全部给付确认完毕!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!tLLRegisterDB.getRgtState().equals("01"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "团体立案状态错误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String countSql = "select count(caseno) from llcase where rgtno ='"
                          +mLLRegisterSchema.getRgtNo()+"' and rgtstate not in "
                          +"('09','11','12','14')";
        ExeSQL texesql = new ExeSQL();
        String casecount = texesql.getOneValue(countSql);
        int count = Integer.parseInt(casecount);
        if(count>0)
            mLLRegisterSchema.setRgtState("02");
        else
            mLLRegisterSchema.setRgtState("03");
        LLRegisterSchema tLLRegisterSchema = tLLRegisterDB.getSchema();
        tLLRegisterSchema.setRgtState(mLLRegisterSchema.getRgtState());
        tLLRegisterSchema.setAppPeoples(mLLRegisterSchema.getAppPeoples());
        map.put(tLLRegisterSchema, "UPDATE");
    }
    if(this.mOperate.equals("UPDATE||REGISTER")){
        if (!aLLRegisterSchema.getRgtState().equals("01"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "该状态下不允许修改团体批次信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!aLLRegisterSchema.getOperator().equals(mGlobalInput.Operator))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "您不是该批次的原始录入人员，无权修改该批次信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!aLLRegisterSchema.getCustomerNo().equals(mLLRegisterSchema.getCustomerNo())||
            !aLLRegisterSchema.getRgtObjNo().equals(mLLRegisterSchema.getRgtObjNo()))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "不允许修改批次下团体客户信息及保单信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if(aLLRegisterSchema.getAppPeoples()!=mLLRegisterSchema.getAppPeoples()){
            CError.buildErr(this,"已经按第一次操作确认时的申请人数预留了理赔号，因此不能修改申请人数！");
            return false;
        }
        LLCaseDB tLLCaseDB = new LLCaseDB();
        LLCaseSet tLLCaseSet = new LLCaseSet();
        tLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        tLLCaseSet = tLLCaseDB.query();
        if(mLLRegisterSchema.getAppPeoples()<tLLCaseSet.size()){
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "该案件下已受理了"+tLLCaseSet.size()
                                  +"人，申请人数不能小于该值！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if(mLLRegisterSchema.getAppPeoples()==tLLCaseSet.size()){
            mLLRegisterSchema.setRgtState("02");
        }
        mLLRegisterSchema.setRgtState(aLLRegisterSchema.getRgtState());
        mLLRegisterSchema.setHandler1(aLLRegisterSchema.getHandler1());
        mLLRegisterSchema.setRgtDate(aLLRegisterSchema.getRgtDate());
        mLLRegisterSchema.setMngCom(aLLRegisterSchema.getMngCom());
        mLLRegisterSchema.setOperator(aLLRegisterSchema.getOperator());
        mLLRegisterSchema.setMakeDate(aLLRegisterSchema.getMakeDate());
        mLLRegisterSchema.setMakeTime(aLLRegisterSchema.getMakeTime());
        mLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
        mLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLLRegisterSchema, "UPDATE");

    }
    if (this.mOperate.equals("DELETE||MAIN")) {
      map.put(mLLRegisterSchema, "DELETE");
    }

    return true;
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean updateData() {
    return true;
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean deleteData() {
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
      System.out.println("getInputData start......");
    this.mLLRegisterSchema.setSchema( (LLRegisterSchema) cInputData.
                                 getObjectByObjectName("LLRegisterSchema", 0));
    this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    if(mLLRegisterSchema.getRgtNo()!=null&&this.mOperate.equals("INSERT||MAIN")){
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        if(tLLRegisterDB.getInfo()){
            this.mOperate = "UPDATE||REGISTER";
            aLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
        }
    }
    if (mGlobalInput.ManageCom.length() == 2)
        mGlobalInput.ManageCom = mGlobalInput.ManageCom + "000000";
    if (mGlobalInput.ManageCom.length() == 4)
        mGlobalInput.ManageCom = mGlobalInput.ManageCom + "0000";
    return true;
  }



  private boolean prepareOutputData() {
    try {
      this.mInputData.clear();
      this.mInputData.add(this.mLLRegisterSchema);
      mInputData.add(map);
      mResult.clear();
      mResult.add(this.mLLRegisterSchema);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LDDrugBL";
      tError.functionName = "prepareData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  public VData getResult() {
    return this.mResult;
  }
}
