package com.sinosoft.lis.llcase;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统投保暂交费功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author YT
 * @version 1.0
 */

public class CaseBLS
{
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();


  public CaseBLS() {}

  public static void main(String[] args)
  {
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone();

    System.out.println("Start LLCase BLS Submit...");
    if (!this.saveData())
      return false;
    System.out.println("End LLCase BLS Submit...");
    mInputData=null;
    return true;
  }

  private boolean saveData()
  {
    LLCaseSet mLLCaseSet = (LLCaseSet)mInputData.getObjectByObjectName("LLCaseBLSet",0);
//10-17修改
Connection conn = DBConnPool.getConnection();
   // Connection conn = null;
   // conn = PubFun1.getDefaultConnection();
    if (conn==null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "CaseBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    try
    {
      conn.setAutoCommit(false);
/*
      // 先把原来关联的部分取消关联
      LLSubReportDB tLLSubReportDB = new LLSubReportDB( conn );
      LLSubReportSet tLLSubReportSet = tLLSubReportDB.query();
      if (tLLSubReportDB.mErrors.needDealError() == true)
			{
				// @@错误处理
		    this.mErrors.copyAllErrors(tLLSubReportDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "LLSubReportBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "数据库操作失败!";
				this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
				return false;
			}
			int n = tLLSubReportSet.size();
			for (int i = 1; i <= n; i++)
			{
				LLSubReportSchema tLLSubReportSchema = tLLSubReportSet.get(i);
				tLLSubReportSet.set(i,tLLSubReportSchema);
			}
			LLSubReportDBSet tLLSubReportDBSet = new LLSubReportDBSet( conn );
			tLLSubReportDBSet.set(tLLSubReportSet);
			if (tLLSubReportDBSet.update() == false)
			{
				// @@错误处理
		    this.mErrors.copyAllErrors(tLJTempFeeDBSet.mErrors);
				CError tError = new CError();
				tError.moduleName = "ProposalFeeBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "更新原数据失败!";
				this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
        
				return false;
			}
*/
			// 保存现在的关联
			LLCaseDBSet mLLCaseDBSet = new LLCaseDBSet( conn );
			mLLCaseDBSet.set(mLLCaseSet);
			if (mLLCaseDBSet.insert() == false)
			{
				// @@错误处理
			  this.mErrors.copyAllErrors(mLLCaseDBSet.mErrors);
				CError tError = new CError();
				tError.moduleName = "CaseBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "保存数据失败!";
				this.mErrors .addOneError(tError) ;
	       conn.rollback() ;
	       conn.close();
				return false;
			}
			conn.commit() ;
			conn.close();
		} // end of try
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CaseBLS";
			tError.functionName = "submitData";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);
			try
			{
				conn.rollback() ;
				conn.close();
			} 
			catch(Exception e)
			{
			}
			
			return false;
		}
    return true;
  }

}