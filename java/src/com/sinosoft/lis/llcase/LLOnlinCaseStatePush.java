package com.sinosoft.lis.llcase;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;


import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;


import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.db.LDCodeDB;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LLCASESTATEPUSHSchema;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LLCASESTATEPUSHSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LLOnlinCaseStatePush {
	
	public CErrors mErrors = new CErrors();
	// 根路径
	private String mURL;
	//文件存放路径
	private String mSaveUrl;
	// Excel文件的文件名
	private String mExcelFileName;
	// sql语句
	private String mSQL;
	// 保存sql查询结果
	private SSRS tSSRS = null;
	//有没有数据
	int maxRow = 0;
	// Excel文件
	private WritableWorkbook book;
	// Excel工作簿
	private WritableSheet sheet;
	// Excel表格
	private Label label1;
	private Label label2;
	private Label label3;
	//推送的项目配置
	private LDCodeSchema mLDCodeSchema = new LDCodeSchema();
	private LDCodeSet mLDCodeSet = new LDCodeSet();
	//推送数据存储表
	private LLCASESTATEPUSHSet mLLCASESTATEPUSHSet = new LLCASESTATEPUSHSet();
	
	//FTP目录下的文件个数
	private int num = 0;
	private VData mInputData = new VData();
	/*
	 * 开始处理
	 */
	public boolean dealData() {
		//System.out.println(System.currentTimeMillis());
		//获取推送项目与推送路径 LDCODE
		if(getPush()){
			
			for(int i=1;i<=mLDCodeSet.size();i++){
				//开始推送数据
				//每次保证set中没有数据
				mLLCASESTATEPUSHSet.clear();
				mExcelFileName = "";
				mLDCodeSchema = mLDCodeSet.get(i);
				mSaveUrl = mURL+mLDCodeSchema.getCodeName()+PubFun.getCurrentDate()+"/";
				if (!this.save(mSaveUrl)) {
					return false;
				}
				num = getFileNum(mSaveUrl);
				
				if(!deal()){
					mLLCASESTATEPUSHSet.clear();
					continue;
				}
				
			}
		}

	
		//System.out.println("SQL->Excel->FTP 执行成功！");
		return true;
	}
	/**
	 * 对数据进行处理
	 */
	
	public boolean deal(){
		if(!writeToExcel()) {
			System.out.println("创建文件失败");
			return false;
		}
		if(!transToFtp()) {
			System.out.println("上传FTP失败");
			return false;
		}
		if(!submitData()){
			System.out.println("数据提交失败");
			return false;
		}
		return true;
	}
	/**
	 * 获取已上传的文件个数
	 */
	public int getFileNum(String cRootPath){
		int fileCount = 0;
		File d = new File(cRootPath);
		String list[] = d.list();
		fileCount = list.length;
		System.out.println("文件个数:"+fileCount);
		return fileCount+1;
	}
	
	/**
	 * 执行批处理
	 */
	public boolean getPush() {
		// 查询根路径
//		String tSQL = "select sysvarvalue from ldsysvar where sysvar = 'UIRoot' with ur";

		String tRootPath = CommonBL.getUIRoot();
		// 提数目录
		LDCodeDB mLDCodeDB = new LDCodeDB();
		mLDCodeSchema.setCodeType("CasePush");
		mLDCodeDB.setSchema(mLDCodeSchema);
		mLDCodeSet = mLDCodeDB.query();
		System.out.println("需推送的项目个数："+mLDCodeSet.size());
		// 本地测试目录地址
        // mURL = "c:/excel/";
		
		mURL = tRootPath;
		
		// 创建归档文件夹
	
		
		if (mURL == null || mURL.equals("")) {
			System.out.println("没有找到理赔提数文件夹！");
			return false;
		}
		return true;
		
	}
	
	/**
	 * 通过SQL语句查询结果，并写入EXCEL文件
	 */
	private boolean writeToExcel() {
		// 用于查询大量数据。不要用ExeSql execsql()方法，这个只能查1W以内的数据量  #3366大数据量优化
		RSWrapper rsWrapper;
		// 用于查询时，循环的次数标志
		int k;
		// 存放每次循环时的最大行数。但实际jxl一个sheet只能存65536行 --数组定长100  2017-05-19
		int[] maxRows = new int[100]; 
		
		try {
			// Excel工作簿的名子 -- sheet名
			String biaoqian = "";

			
			mSQL = "select t.* from ( "
					+ "select a.caseno caseno, "
					+ " case when a.rgtstate='11' then '11' when a.rgtstate='12' then '12' when a.rgtstate='01' then '01' end rgtstate,"
					+ " '' remark,"
					+ " b.rgttype rgttype,"
					+ " a.mngcom mngcom"
					+ " from llcase a, llregister b where a.caseno=b.rgtno and b.rgttype='"+mLDCodeSchema.getCode()+"' and a.rgtstate not in('13','14') and a.makedate between current date -1 day and current date  "
					+ " ) t where not exists (select 1 from LLCASESTATEPUSH where caseno=t.caseno and rgtstate=t.rgtstate) "
					+ " with ur";
			rsWrapper = new RSWrapper();
			if (!rsWrapper.prepareData(null, mSQL)) {
	            System.out.println("理赔进度__数据准备失败! ");
	            return false;
	        }

			

			
			k = 1;// 每个文件都从1开始
			do {
				
	        	// 获取定量数据，目前每次5000条数据
				tSSRS = rsWrapper.getSSRS();
				maxRow = tSSRS.getMaxRow();
//	        	System.out.println(maxRow);
	        	maxRows[k] = maxRow;
	        	String CaseNO = "";
	        	String RgtState = "";
	        	String Remark = "";
	        	String RgtType = "";
	        	String MngCom = "";
	        	String CurrentDate = PubFun.getCurrentDate();
	        	String CurrentTime = PubFun.getCurrentTime();
				// 操作第二行以后,，即数据行
	        	if(k == 1 && maxRow>0){
	    			// 有数据，再创建文件
	    			mExcelFileName = "WXClaimStatus"+num+".xls";
	    			// 创建一个Excel文件
	    			book = Workbook.createWorkbook(new File(mSaveUrl + mExcelFileName));
	    			biaoqian = "进度提数";
	    			// 第一个工作簿
	    			sheet = book.createSheet(biaoqian, 1);
	    			// 操作第一行
	    			// 参数1：列， 参数2：行， 参数3：内容
	    			label1 = new Label(0, 0, "案件号");
	    			label2 = new Label(1, 0, "案件状态");
	    			label3 = new Label(2, 0, "备注信息");
	    			

	    			// 将定义好的单元格添加到工作表中
	    			sheet.addCell(label1);
	    			sheet.addCell(label2);
	    			sheet.addCell(label3);
	        	}
				for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
					// 为了向后追加，这里只要第一次的最大值即可:maxRows[1]
					LLCASESTATEPUSHSchema mLLCASESTATEPUSHSchema = new LLCASESTATEPUSHSchema();
					int j = i + maxRows[1]*(k-1);
					CaseNO = tSSRS.GetText(i, 1);
					RgtState = tSSRS.GetText(i, 2);
					Remark = tSSRS.GetText(i, 3);
					RgtType = tSSRS.GetText(i, 4);
					MngCom = tSSRS.GetText(i, 5);
					
					label1 = new Label(0, j, CaseNO);
					label2 = new Label(1, j, RgtState);
					label3 = new Label(2, j, Remark);
					
	
					sheet.addCell(label1);
					sheet.addCell(label2);
					sheet.addCell(label3);
					mLLCASESTATEPUSHSchema.setCASENO(CaseNO);
					mLLCASESTATEPUSHSchema.setRGTSTATE(RgtState);
					mLLCASESTATEPUSHSchema.setRGTTYPE(RgtType);
					mLLCASESTATEPUSHSchema.setMNGCOM(MngCom);
					mLLCASESTATEPUSHSchema.setMAKEDATE(CurrentDate);
					mLLCASESTATEPUSHSchema.setMAKETIME(CurrentTime);
					mLLCASESTATEPUSHSchema.setMODIFYDATE(CurrentDate);
					mLLCASESTATEPUSHSchema.setMODIFYTIME(CurrentTime);
					mLLCASESTATEPUSHSet.add(mLLCASESTATEPUSHSchema);
					
				}	
				k = k + 1;
			} while (tSSRS != null && tSSRS.MaxRow > 0);	
			if(k>2||maxRow>0){
				book.write();
				book.close();
			}
			
	
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		}
		
		return true;
	}
	/**
	 * 本地文件上传至Ftp
	 * @return
	 */
	private boolean transToFtp(){
        ExeSQL tExeSql = new ExeSQL();

        String tServerIP = tExeSql.getOneValue("select codename from ldcode where codetype='LLWXClaim' and code='IP/Port'");
//        String tServerIP ="10.252.4.88"; //外测地址
        String tPort = tExeSql.getOneValue("select codealias from ldcode where codetype='LLWXClaim' and code='IP/Port'");
        String tUsername = tExeSql.getOneValue("select codename from ldcode where codetype='LLWXClaim' and code='User/Pass'");
        String tPassword = tExeSql.getOneValue("select codealias from ldcode where codetype='LLWXClaim' and code='User/Pass'");
  
        try {
            FTPTool tFTPTool = new FTPTool(tServerIP, tUsername, tPassword, Integer.parseInt(tPort), "aaActiveMode");
            if (!tFTPTool.loginFTP()) {
                CError tError = new CError();
                tError.moduleName = "LLOnlinCaseStatePush";
                tError.functionName = "getXls";
                tError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
            
            // 本地测试
            // 本地文件的目录
//            String tFilePathLocal = "c:/666/";
            
            // 取得本地存放文件的目录
            String tFilePathLocal = mSaveUrl+mExcelFileName;
            // 要上传的文件在ftp上的存放目录
            String tFileTransPath = "/"+mLDCodeSchema.getCodeName()+PubFun.getCurrentDate();

            tFTPTool.makeDirectory(tFileTransPath);

            // 有数据再上载
            if(!"".equals(mExcelFileName)){
            	
	            if(!tFTPTool.upload(tFileTransPath, tFilePathLocal)) {
	            	System.out.println("文件上传失败！！！");
	            	return false;
	            }
            }
            // 关闭FTP连接
            //System.out.println(System.currentTimeMillis());
            tFTPTool.logoutFTP();
            
        }
        catch (SocketException ex) {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "ExcelToFtpBL";
            tError.functionName = "transToFtp";
            tError.errorMessage = "获取连接失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        catch (IOException ex) {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "ExcelToFtpBLBL";
            tError.functionName = "transToFtp";
            tError.errorMessage = "连接ftp失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        return true;
        
    }
	
	/**
	 * 创建文件夹
	 * @param cRootPath
	 * @return
	 */
	public boolean save(String cRootPath){

        File mFileDir = new File(cRootPath);
        if (!mFileDir.exists()){
            if (!mFileDir.mkdirs()){
                System.err.println("创建目录[" + cRootPath + "]失败！" + mFileDir.getPath());
                return false;
            }
        }
        return true;
        
    }
	

	public boolean submitData() {
		PubSubmit tPubSubmit=new PubSubmit();
		MMap mMMap = new MMap();
		mMMap.put(mLLCASESTATEPUSHSet, "INSERT");
		mInputData.add(mMMap);
		if(!tPubSubmit.submitData(mInputData, null)){
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			     return buildErr("submitData","LLCASESTATEPUSH数据更新失败！");
			
		}
		
		return true;
	 }
	 // @@错误处理
    private boolean buildErr(String FucName,String ErrMsg){
        CError tError = new CError();
        tError.moduleName = "LLOnlinCaseStatePush";
        tError.functionName = FucName;
        tError.errorMessage = ErrMsg;
        this.mErrors.addOneError(tError);
        return false;
    }
	
	/**
	 * 测试
	 * @param args
	 */
	public static void main(String[] args) {
		LLOnlinCaseStatePush test = new LLOnlinCaseStatePush();
		test.dealData();
		System.out.println("执行完毕！！！！");
	}
	
	


}
