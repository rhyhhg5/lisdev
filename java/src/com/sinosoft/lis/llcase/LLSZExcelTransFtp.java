package com.sinosoft.lis.llcase;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Date;

//import utils.system;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;

/**
 * 提数FTP服务器
 * 根据SQL语句查询结果，将结果保存在Excel中，并上传FTP服务器。
 * 
 * 目前：
 * 其中团单提数每个工作表6万条数据，即最大数据量为：6W+6W+6W+...
 * 其他3个提数最大数据量为：65536条。 
 * 
 * 入口：dealData()
 * 
 * @author Administrator
 * 
 */

public class LLSZExcelTransFtp {
	
	public CErrors mErrors = new CErrors();
	// 导出的Excel文件目录地址
	private String mURL;
	// Excel文件的文件名
	private String mExcelFileName;
	// sql语句
	private String mSQL;
	// 保存sql查询结果
	private SSRS tSSRS = null;
	// Excel文件
	private WritableWorkbook book;
	// Excel工作簿
	private WritableSheet sheet;
	// Excel表格
	private Label label1;
	private Label label2;
	private Label label3;
	private Label label4;
	private Label label5;
	private Label label6;
	private Label label7;
	private Label label8;
	private Label label9;
	private Label label10;
	private Label label11;
	private Label label12;
	private Label label13;
	private Label label14;
	private Label label15;
	private Label label16;
	private Label label17;
	private Label label18;
	private Label label19;
	private Label label20;
	private Label label21;
	private Label label22;
	private Label label23;
	private Label label24;
	private Label label25;
	private Label label26;
	private Label label27;
	private Label label28;
	private Label label29;
	private Label label30;
	private Label label31;
	/*
	 * 开始处理
	 */
	public boolean dealData() {
		//System.out.println(System.currentTimeMillis());
		if(!getSagentData()) {
			return false;
		}
		if(!writeToExcel()) {
			return false;
		}
		if(!transToFtp()) {
			return false;
		}
		System.out.println("SQL->Excel->FTP 执行成功！");
		return true;
	}
	
	/**
	 * 执行批处理
	 */
	public boolean getSagentData() {
		// 查询根路径
		String tSQL = "select sysvarvalue from ldsysvar where sysvar = 'UIRoot' with ur";
		String tRootPath = new ExeSQL().getOneValue(tSQL);
		// 提数目录
		tSQL = "select codename from ldcode where codetype='LLWXClaim' and code='ExcelPath'";
		String tTempPath =  new ExeSQL().getOneValue(tSQL);
		// 本地测试目录地址
//         mURL = "E:/哈哈/666/";
		
		mURL = tRootPath + tTempPath;
		
		// 创建归档文件夹
		if (this.save(mURL)) {
			mURL = this.getSavePath(mURL);
		}
		
		if (mURL == null || mURL.equals("")) {
			System.out.println("没有找到理赔提数文件夹！");
			return false;
		}
		return true;
		
	}
	
	/**
	 * 通过SQL语句查询结果，并写入EXCEL文件
	 */
	private boolean writeToExcel() {
		// 用于查询大量数据。不要用ExeSql execsql()方法，这个只能查1W以内的数据量  #3366大数据量优化
		RSWrapper rsWrapper;
		// 用于查询时，循环的次数标志
		int k;
		// 存放每次循环时的最大行数。但实际jxl一个sheet只能存65536行 --数组定长100  2017-05-19
		int[] maxRows = new int[100]; 
		
		try {
			// Excel工作簿的名子 -- sheet名
			String biaoqian = "";
			// 个单提数
			mExcelFileName = "PolicyInfo.xls";
			// 创建一个Excel文件
			book = Workbook.createWorkbook(new File(mURL + mExcelFileName));
			
			mSQL = "select insuredname 被保险人姓名,"
				+ "	(select codename from ldcode where codetype='idtype' and code in (select insuredidtype from lccont where contno = lcp.contno fetch first 1 row only)) 被保险人证件类型,			    "
			    + "(select insuredidno from lccont where contno = lcp.contno fetch first 1 row only) 被保险人证件号码,"
			    + "(select mobile from lcaddress where customerno = lcp.InsuredNo fetch first 1 row only) 被保险人手机号码,"
			    + "lcp.contno 保单号,lcp.AppntName 投保人姓名,"
			    + "(select codename from ldcode where codetype='idtype' and code in (select appntidtype from lccont where contno = lcp.contno fetch first 1 row only)) 投保人证件类型,"
			    + "(select appntidno from lccont where contno = lcp.contno fetch first 1 row only)投保人证件号码,"
			    + "(select Mobile from lcaddress where CustomerNo = lcp.appntno fetch first 1 row only)投保人手机号码,"
			    + "(select riskname from lmriskapp where riskcode = lcp.riskcode) 险种名称,lcp.amnt 险种保额,lcp.Mult 险种档次,lcp.prem 险种保费,"
			    + "(select dutyname from lmduty where dutycode = lcd.dutycode) 责任名称,"
			    + "lcd.amnt 责任保额,"
			    + "'个单' 客户性质,lcp.CValiDate 生效日期,lcp.EndDate 满期日期,lcp.PaytoDate 保费交至日,lcp.EndDate 终止日期,lcp.PayYears 缴费年期,"
			    + "(select codename from ldcode where codetype = 'payendyearflag' and code = lcp.payendyearflag ) 交费年期标志,"
			    + "(select codename from ldcode where codetype = 'payintv' and code = lcp.payintv ) 交费频次,"
			    + "(select GracePeriod from LMRiskPay where riskcode = lcp.riskcode) || '天' 宽限期, lcp.prem 应缴但未缴保费,"
			    + "(select name from lcbnf where polno = lcp.polno fetch first 1 row only) 受益人姓名,"
			    + "(select codename from ldcode where codetype = 'bnftype' and code in"
				+ "(select BnfType from lcbnf where polno = lcp.polno fetch first 1 row only) ) 类别,"
			    + "(select IDNo from lcbnf where polno = lcp.polno fetch first 1 row only) 证件号码,"
			    + "(select codename from ldcode where codetype = 'relation' and code in"
			    + "(select RelationToInsured from lcbnf where polno = lcp.polno fetch first 1 row only)) 与被保险人关系,"
			    + "(select BnfLot from lcbnf where polno = lcp.polno fetch first 1 row only) 受益比例,"
			    + "(select BnfGrade from lcbnf where polno = lcp.polno fetch first 1 row only) || '类' 受益顺序 "
				+ "from lcpol lcp,lcduty lcd " 
				+ "where managecom like '8695%' and conttype = '1' and appflag = '1'  and stateflag = '1' and signdate=(current date - 1 day)  "
				+ "and lcp.polno = lcd.polno with ur";
			rsWrapper = new RSWrapper();
			//#4002 深圳大客户平台对个单数据溢出进行修改 start
			// 每个sheet放6W条数据
			int iMaxLines1 = 60000;
			if (!rsWrapper.prepareData(null, mSQL)) {
	            System.out.println("个单提数__数据准备失败! ");
	            return false;
	        }
			biaoqian = "个单提数";
			// 第一个工作簿
			sheet = book.createSheet(biaoqian, 1);
			// 操作第一行
			label1 = new Label(0, 0, "被保险人姓名");
			label2 = new Label(1, 0, "被保险人证件类型");
			label3 = new Label(2, 0, "被保险人证件号码");
			label4 = new Label(3, 0, "被保险人手机号码");
			label5 = new Label(4, 0, "保单号");
			label6 = new Label(5, 0, "投保人姓名");
			label7 = new Label(6, 0, "投保人证件类型");
			label8 = new Label(7, 0, "投保人证件号码");
			label9 = new Label(8, 0, "投保人手机号码");
			label10 = new Label(9, 0, "险种名称");
			label11 = new Label(10, 0, "险种保额");
			label12 = new Label(11, 0, "险种档次");
			label13 = new Label(12, 0, "险种保费");
			label14 = new Label(13, 0, "责任名称");
			label15 = new Label(14, 0, "责任保额");
			label16 = new Label(15, 0, "客户性质");
			label17 = new Label(16, 0, "生效日期");
			label18 = new Label(17, 0, "满期日期");
			label19 = new Label(18, 0, "保费交至日");
			label20 = new Label(19, 0, "终止日期");
			label21 = new Label(20, 0, "交费年期");
			label22 = new Label(21, 0, "交费年期标志");
			label23 = new Label(22, 0, "交费频次");
			label24 = new Label(23, 0, "宽限期");
			label25 = new Label(24, 0, "应缴但未缴保费");
			label26 = new Label(25, 0, "受益人姓名");
			label27 = new Label(26, 0, "类别");
			label28 = new Label(27, 0, "受益人证件号码");
			label29 = new Label(28, 0, "与被保险人关系");
			label30 = new Label(29, 0, "受益比例");
			label31 = new Label(30, 0, "受益顺序");
			// 参数1：列， 参数2：行， 参数3：内容
			sheet.addCell(label1);
			sheet.addCell(label2);
			sheet.addCell(label3);
			sheet.addCell(label4);
			sheet.addCell(label5);
			sheet.addCell(label6);
			sheet.addCell(label7);
			sheet.addCell(label8);
			sheet.addCell(label9);
			sheet.addCell(label10);
			sheet.addCell(label11);
			sheet.addCell(label12);
			sheet.addCell(label13);
			sheet.addCell(label14);
			sheet.addCell(label15);
			sheet.addCell(label16);
			sheet.addCell(label17);
			sheet.addCell(label18);
			sheet.addCell(label19);
			sheet.addCell(label20);
			sheet.addCell(label21);
			sheet.addCell(label22);
			sheet.addCell(label23);
			sheet.addCell(label24);
			sheet.addCell(label25);
			sheet.addCell(label26);
			sheet.addCell(label27);
			sheet.addCell(label28);
			sheet.addCell(label29);
			sheet.addCell(label30);
			sheet.addCell(label31);
			
			k = 1;// 每个文件都从1开始
			int sum1 = 0;// 数据总行数
			int page1 = 1;// 工作簿个数
			int maxRow1;// 每次查出的数据行数
			do {
	        	tSSRS = rsWrapper.getSSRS();// 获取定量数据，目前每次5000条数据
				maxRow1 = tSSRS.getMaxRow();
	        	maxRows[k] = maxRow1;// 把每次的数据量存入数组
	        	for(int x = 0; x < k; x++) {
	        		sum1 = maxRows[k] + sum1;
	        		if(k != 1) { // 保留第一次的数据量
	        			maxRows[k] = 0; // 其他的清空
	        		}
	        	}
				// 工作表数量>2时
	        	if(sum1 > iMaxLines1) {
	        		// 重新赋值
	        		k = 1;
	        	 maxRow1 = tSSRS.getMaxRow();
//	        	System.out.println(maxRow);
	        	maxRows[k] = maxRow1;
				page1 = page1 + 1;
	        		sheet = book.createSheet(biaoqian + page1, page1);
	        		label1 = new Label(0, 0, "被保险人姓名");
	    			label2 = new Label(1, 0, "被保险人证件类型");
	    			label3 = new Label(2, 0, "被保险人证件号码");
	    			label4 = new Label(3, 0, "被保险人手机号码");
	    			label5 = new Label(4, 0, "保单号");
	    			label6 = new Label(5, 0, "投保人姓名");
	    			label7 = new Label(6, 0, "投保人证件类型");
	    			label8 = new Label(7, 0, "投保人证件号码");
	    			label9 = new Label(8, 0, "投保人手机号码");
	    			label10 = new Label(9, 0, "险种名称");
	    			label11 = new Label(10, 0, "险种保额");
	    			label12 = new Label(11, 0, "险种档次");
	    			label13 = new Label(12, 0, "险种保费");
	    			label14 = new Label(13, 0, "责任名称");
	    			label15 = new Label(14, 0, "责任保额");
	    			label16 = new Label(15, 0, "客户性质");
	    			label17 = new Label(16, 0, "生效日期");
	    			label18 = new Label(17, 0, "满期日期");
	    			label19 = new Label(18, 0, "保费交至日");
	    			label20 = new Label(19, 0, "终止日期");
	    			label21 = new Label(20, 0, "交费年期");
	    			label22 = new Label(21, 0, "交费年期标志");
	    			label23 = new Label(22, 0, "交费频次");
	    			label24 = new Label(23, 0, "宽限期");
	    			label25 = new Label(24, 0, "应缴但未缴保费");
	    			label26 = new Label(25, 0, "受益人姓名");
	    			label27 = new Label(26, 0, "类别");
	    			label28 = new Label(27, 0, "受益人证件号码");
	    			label29 = new Label(28, 0, "与被保险人关系");
	    			label30 = new Label(29, 0, "受益比例");
	    			label31 = new Label(30, 0, "受益顺序");

	    			// 将定义好的单元格添加到工作表中
	    			sheet.addCell(label1);
	    			sheet.addCell(label2);
	    			sheet.addCell(label3);
	    			sheet.addCell(label4);
	    			sheet.addCell(label5);
	    			sheet.addCell(label6);
	    			sheet.addCell(label7);
	    			sheet.addCell(label8);
	    			sheet.addCell(label9);
	    			sheet.addCell(label10);
	    			sheet.addCell(label11);
	    			sheet.addCell(label12);
	    			sheet.addCell(label13);
	    			sheet.addCell(label14);
	    			sheet.addCell(label15);
	    			sheet.addCell(label16);
	    			sheet.addCell(label17);
	    			sheet.addCell(label18);
	    			sheet.addCell(label19);
	    			sheet.addCell(label20);
	    			sheet.addCell(label21);
	    			sheet.addCell(label22);
	    			sheet.addCell(label23);
	    			sheet.addCell(label24);
	    			sheet.addCell(label25);
	    			sheet.addCell(label26);
	    			sheet.addCell(label27);
	    			sheet.addCell(label28);
	    			sheet.addCell(label29);
	    			sheet.addCell(label30);
	    			sheet.addCell(label31);
	    			// 重新赋值
	    			sum1 = maxRow1;
	        	}
				// 操作第二行以后,，即数据行
				for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
					// 为了向后追加，这里只要第一次的最大值即可:maxRows[1]
					int j = i + maxRows[1]*(k-1);
					label1 = new Label(0, j, tSSRS.GetText(i, 1));
					label2 = new Label(1, j, tSSRS.GetText(i, 2));
					label3 = new Label(2, j, tSSRS.GetText(i, 3));
					label4 = new Label(3, j, tSSRS.GetText(i, 4));
					label5 = new Label(4, j, tSSRS.GetText(i, 5));
					label6 = new Label(5, j, tSSRS.GetText(i, 6));
					label7 = new Label(6, j, tSSRS.GetText(i, 7));
					label8 = new Label(7, j, tSSRS.GetText(i, 8));
					label9 = new Label(8, j, tSSRS.GetText(i, 9));
					label10 = new Label(9, j, tSSRS.GetText(i, 10));
					label11 = new Label(10, j, tSSRS.GetText(i, 11));
					label12 = new Label(11, j, tSSRS.GetText(i, 12));
					label13 = new Label(12, j, tSSRS.GetText(i, 13));
					label14 = new Label(13, j, tSSRS.GetText(i, 14));
					label15 = new Label(14, j, tSSRS.GetText(i, 15));
					label16 = new Label(15, j, tSSRS.GetText(i, 16));
					label17 = new Label(16, j, tSSRS.GetText(i, 17));
					label18 = new Label(17, j, tSSRS.GetText(i, 18));
					label19 = new Label(18, j, tSSRS.GetText(i, 19));
					label20 = new Label(19, j, tSSRS.GetText(i, 20));
					label21 = new Label(20, j, tSSRS.GetText(i, 21));
					label22 = new Label(21, j, tSSRS.GetText(i, 22));
					label23 = new Label(22, j, tSSRS.GetText(i, 23));
					label24 = new Label(23, j, tSSRS.GetText(i, 24));
					label25 = new Label(24, j, tSSRS.GetText(i, 25));
					label26 = new Label(25, j, tSSRS.GetText(i, 26));
					label27 = new Label(26, j, tSSRS.GetText(i, 27));
					label28 = new Label(27, j, tSSRS.GetText(i, 28));
					label29 = new Label(28, j, tSSRS.GetText(i, 29));
					label30 = new Label(29, j, tSSRS.GetText(i, 30));
					label31 = new Label(30, j, tSSRS.GetText(i, 31));
					
					// 判断Label是否有"null"字符串
					this.validateLabel();
	
					sheet.addCell(label1);
					sheet.addCell(label2);
					sheet.addCell(label3);
					sheet.addCell(label4);
					sheet.addCell(label5);
					sheet.addCell(label6);
					sheet.addCell(label7);
					sheet.addCell(label8);
					sheet.addCell(label9);
					sheet.addCell(label10);
					sheet.addCell(label11);
					sheet.addCell(label12);
					sheet.addCell(label13);
					sheet.addCell(label14);
					sheet.addCell(label15);
					sheet.addCell(label16);
					sheet.addCell(label17);
					sheet.addCell(label18);
					sheet.addCell(label19);
					sheet.addCell(label20);
					sheet.addCell(label21);
					sheet.addCell(label22);
					sheet.addCell(label23);
					sheet.addCell(label24);
					sheet.addCell(label25);
					sheet.addCell(label26);
					sheet.addCell(label27);
					sheet.addCell(label28);
					sheet.addCell(label29);
					sheet.addCell(label30);
					sheet.addCell(label31);

	    			
				}	
				k = k + 1;
			
	        	} while (tSSRS != null && tSSRS.MaxRow > 0);
			
			book.write();
			book.close();
			//#4002 深圳大客户平台对个单数据溢出进行修改  end
			
			// 团单提数
			mExcelFileName = "GrpPolicyInfo.xls";
			book = Workbook.createWorkbook(new File(mURL + mExcelFileName));
			mSQL = "select lcp.insuredname 被保险人姓名,"
				+ "(select codename from ldcode where codetype='idtype' and code in (select insuredidtype from lccont where contno = lcp.contno fetch first 1 row only)) 被保险人证件类型,"
				+ "(select insuredidno from lccont where contno = lcp.contno) 被保险人证件号码,lcp.grpcontno 保单号,lcp.appntname 投保人姓名,'' 投保人证件类型,'' 投保人证件号码, "
				+ "(select riskname from lmriskapp where riskcode = lcp.riskcode) 险种名称,lcp.amnt 险种保额,"
				+ "(select dutyname from lmduty where dutycode = lcd.dutycode ) 责任名称,"
			    + "lcd.amnt 责任保额,"
				+ "'团单' 客户性质,'团险普通保单' 保单性质,lcp.CValiDate 生效日期,"
				+ "(select cinvalidate from lccont where contno = lcp.contno) 满期日期,lcp.paytodate 交至日期,"
				+ "(select calfactorvalue from lccontplandutyparam a where calfactor = 'GetLimit' and a.grpcontno = lcp.grpcontno "
				+ "and a.riskcode = lcp.riskcode and a.contplancode = lcp.contplancode fetch first 1 rows only) 免赔额,"
				+ "(select calfactorvalue from lccontplandutyparam a where calfactor = 'GetRate' and a.grpcontno = lcp.grpcontno "
				+ "and a.riskcode = lcp.riskcode and a.contplancode = lcp.contplancode fetch first 1 rows only ) 赔付比例 "
				+ "from lcpol lcp,lcduty lcd "
//				+ "inner join lcgrpcont lcg on lcg.grpcontno=lcp.grpcontno and lcg.managecom like '8695%' and lcg.appflag='1' and lcg.stateflag='1' "
				+ "where 1=1 and lcp.appflag='1' and lcp.conttype='2' and lcp.signdate=(current date - 1 day) "
				+ "and exists(select 1 from lcgrpcont where grpcontno=lcp.grpcontno and managecom like '8695%' and appflag='1' and stateflag='1')"
				+ "and lcp.polno = lcd.polno with ur";
			rsWrapper = new RSWrapper();
			// 每个sheet放6W条数据
			int iMaxLines = 60000;
			if (!rsWrapper.prepareData(null, mSQL)) {
	            System.out.println("团单提数__数据准备失败! ");
	            return false;
	        }
			biaoqian = "团单提数";
			sheet = book.createSheet(biaoqian, 1);
			
			label1 = new Label(0, 0, "被保险人姓名");
			label2 = new Label(1, 0, "被保险人证件类型");
			label3 = new Label(2, 0, "被保险人证件号码");
			label4 = new Label(3, 0, "保单号");
			label5 = new Label(4, 0, "投保人姓名");
			label6 = new Label(5, 0, "投保人证件类型");
			label7 = new Label(6, 0, "投保人证件号码");
			label8 = new Label(7, 0, "险种名称");
			label9 = new Label(8, 0, "险种保额");
			label10 = new Label(9, 0, "责任名称");
			label11 = new Label(10, 0, "责任保额");
			label12 = new Label(11, 0, "客户性质");
			label13 = new Label(12, 0, "保单性质");
			label14 = new Label(13, 0, "生效日期");
			label15 = new Label(14, 0, "满期日期");
			label16 = new Label(15, 0, "交至日期");
			label17 = new Label(16, 0, "免赔额");
			label18 = new Label(17, 0, "赔付比例");

			sheet.addCell(label1);
			sheet.addCell(label2);
			sheet.addCell(label3);
			sheet.addCell(label4);
			sheet.addCell(label5);
			sheet.addCell(label6);
			sheet.addCell(label7);
			sheet.addCell(label8);
			sheet.addCell(label9);
			sheet.addCell(label10);
			sheet.addCell(label11);
			sheet.addCell(label12);
			sheet.addCell(label13);
			sheet.addCell(label14);
			sheet.addCell(label15);
			sheet.addCell(label16);
			sheet.addCell(label17);
			sheet.addCell(label18);


			k = 1;
			int sum = 0;// 数据总行数
			int page = 1;// 工作簿个数
			int maxRow;// 每次查出的数据行数
			do {
	        	tSSRS = rsWrapper.getSSRS();
	        	maxRow = tSSRS.getMaxRow();
	        	maxRows[k] = maxRow;// 把每次的数据量存入数组
	        	for(int x = 0; x < k; x++) {
	        		sum = maxRows[k] + sum;
	        		if(k != 1) { // 保留第一次的数据量
	        			maxRows[k] = 0; // 其他的清空
	        		}
	        	}
	        	// 工作表数量>2时
	        	if(sum > iMaxLines) {
	        		// 重新赋值
	        		k = 1;
	        		maxRow = tSSRS.getMaxRow();
		        	maxRows[k] = maxRow;
	        		page = page + 1;
	        		sheet = book.createSheet(biaoqian + page, page);
	        		label1 = new Label(0, 0, "被保险人姓名");
	    			label2 = new Label(1, 0, "被保险人证件类型");
	    			label3 = new Label(2, 0, "被保险人证件号码");
	    			label4 = new Label(3, 0, "保单号");
	    			label5 = new Label(4, 0, "投保人姓名");
	    			label6 = new Label(5, 0, "投保人证件类型");
	    			label7 = new Label(6, 0, "投保人证件号码");
	    			label8 = new Label(7, 0, "险种名称");
	    			label9 = new Label(8, 0, "险种保额");
	    			label10 = new Label(9, 0, "责任名称");
	    			label11 = new Label(10, 0, "责任保额");
	    			label12 = new Label(11, 0, "客户性质");
	    			label13 = new Label(12, 0, "保单性质");
	    			label14 = new Label(13, 0, "生效日期");
	    			label15 = new Label(14, 0, "满期日期");
	    			label16 = new Label(15, 0, "交至日期");
	    			label17 = new Label(16, 0, "免赔额");
	    			label18 = new Label(17, 0, "赔付比例");

	    			sheet.addCell(label1);
	    			sheet.addCell(label2);
	    			sheet.addCell(label3);
	    			sheet.addCell(label4);
	    			sheet.addCell(label5);
	    			sheet.addCell(label6);
	    			sheet.addCell(label7);
	    			sheet.addCell(label8);
	    			sheet.addCell(label9);
	    			sheet.addCell(label10);
	    			sheet.addCell(label11);
	    			sheet.addCell(label12);
	    			sheet.addCell(label13);
	    			sheet.addCell(label14);
	    			sheet.addCell(label15);
	    			sheet.addCell(label16);
	    			sheet.addCell(label17);
	    			sheet.addCell(label18);
	    			
	    			// 重新赋值
	    			sum = maxRow;
	        	}
	        	//System.out.println("团单提数_目前工作表" + page + "数据量：" + sum);
	        	
	        	for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
	        		int j = i + maxRows[1]*(k-1);
					label1 = new Label(0, j, tSSRS.GetText(i, 1));
					label2 = new Label(1, j, tSSRS.GetText(i, 2));
					label3 = new Label(2, j, tSSRS.GetText(i, 3));
					label4 = new Label(3, j, tSSRS.GetText(i, 4));
					label5 = new Label(4, j, tSSRS.GetText(i, 5));
					label6 = new Label(5, j, tSSRS.GetText(i, 6));
					label7 = new Label(6, j, tSSRS.GetText(i, 7));
					label8 = new Label(7, j, tSSRS.GetText(i, 8));
					label9 = new Label(8, j, tSSRS.GetText(i, 9));
					label10 = new Label(9, j, tSSRS.GetText(i, 10));
					label11 = new Label(10, j, tSSRS.GetText(i, 11));
					label12 = new Label(11, j, tSSRS.GetText(i, 12));
					label13 = new Label(12, j, tSSRS.GetText(i, 13));
					label14 = new Label(13, j, tSSRS.GetText(i, 14));
					label15 = new Label(14, j, tSSRS.GetText(i, 15));
					label16 = new Label(15, j, tSSRS.GetText(i, 16));
					label17 = new Label(16, j, tSSRS.GetText(i, 17));
					label18 = new Label(17, j, tSSRS.GetText(i, 18));

					this.validateLabel();
					
					sheet.addCell(label1);
					sheet.addCell(label2);
					sheet.addCell(label3);
					sheet.addCell(label4);
					sheet.addCell(label5);
					sheet.addCell(label6);
					sheet.addCell(label7);
					sheet.addCell(label8);
					sheet.addCell(label9);
					sheet.addCell(label10);
					sheet.addCell(label11);
					sheet.addCell(label12);
					sheet.addCell(label13);
					sheet.addCell(label14);
	    			sheet.addCell(label15);
	    			sheet.addCell(label16);
	    			sheet.addCell(label17);
	    			sheet.addCell(label18);
				}
	        	k = k + 1;
	        } while (tSSRS != null && tSSRS.MaxRow > 0);
			book.write();
			book.close();
			
			// 保险理赔进度 --增加3个字段 2017-05-19   ***2017-5-26*** 提数口径变更  #3366 star
//			mExcelFileName = "ClaimStatus.xls";
//			book = Workbook.createWorkbook(new File(mURL + mExcelFileName));
//			mSQL = "select a.caseno 案件号,a.customerno 被保险人客户号,a.customername 被保险人姓名,a.idno 被保人身份证号,a.rgtdate 受理时间,"
//				+ "(select makedate from llfeemain where caseno=a.caseno fetch first 1 rows only  ) 录入时间,"
//				+ "a.endcasedate 结案时间,(select confdate from ljaget where (otherno=a.caseno or otherno=a.rgtno) and othernotype='5' fetch first 1 rows only) 给付时间  "
//				+ "from llcase a where a.rgtstate!='12' and a.mngcom like '8695%' "
//				+ "union all "
//				+ "select a.caseno,a.customerno,a.customername,a.idno,a.rgtdate,(select makedate from llfeemain where caseno=a.caseno fetch first 1 rows only )," 
//				+ "a.endcasedate,(select confdate from ljaget where (otherno=a.caseno or otherno=a.rgtno) and othernotype='5' fetch first 1 rows only )"
//				+ "from llcase a where a.rgtstate='12' and a.mngcom like '8695%' "
//				+ "and exists(select 1 from ljaget where (otherno=a.caseno or otherno=a.rgtno) and othernotype='5' and confdate=current date - 1 day) fetch first 500 rows only ";
//			rsWrapper = new RSWrapper();
//			if (!rsWrapper.prepareData(null, mSQL)) {
//	            System.out.println("保险理赔进度__数据准备失败! ");
//	            return false;
//	        }
//			biaoqian = "保险理赔进度";
//			sheet = book.createSheet(biaoqian, 1);
//			
//			label1 = new Label(0, 0, "案件号");
//			label2 = new Label(1, 0, "被保险人客户号");
//			label3 = new Label(2, 0, "被保险人姓名");
//			label4 = new Label(3, 0, "被保人身份证号");
//			label5 = new Label(4, 0, "受理时间");
//			label6 = new Label(5, 0, "录入时间");
//			label7 = new Label(6, 0, "结案时间");
//			label8 = new Label(7, 0, "给付时间");
//			
//			this.validateLabel();
//
//			sheet.addCell(label1);
//			sheet.addCell(label2);
//			sheet.addCell(label3);
//			sheet.addCell(label4);
//			sheet.addCell(label5);
//			sheet.addCell(label6);
//			sheet.addCell(label7);
//			sheet.addCell(label8);
//			
//			k = 1;
//			do {
//	        	tSSRS = rsWrapper.getSSRS();
//	        	maxRow = tSSRS.getMaxRow();
//	        	maxRows[k] = maxRow;
//	        	for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
//	        		int j = i + maxRows[1]*(k-1);
//					label1 = new Label(0, j, tSSRS.GetText(i, 1));
//					label2 = new Label(1, j, tSSRS.GetText(i, 2));
//					label3 = new Label(2, j, tSSRS.GetText(i, 3));
//					label4 = new Label(3, j, tSSRS.GetText(i, 4));
//					label5 = new Label(4, j, tSSRS.GetText(i, 5));
//					label6 = new Label(5, j, tSSRS.GetText(i, 6));
//					label7 = new Label(6, j, tSSRS.GetText(i, 7));
//					label8 = new Label(7, j, tSSRS.GetText(i, 8));
//	
//					sheet.addCell(label1);
//					sheet.addCell(label2);
//					sheet.addCell(label3);
//					sheet.addCell(label4);
//					sheet.addCell(label5);
//					sheet.addCell(label6);
//					sheet.addCell(label7);
//					sheet.addCell(label8);
//	        	}
//	        	k = k + 1;
//			} while (tSSRS != null && tSSRS.MaxRow > 0);
//			book.write();
//			book.close();
//			
//			
//			mExcelFileName = "ClaimResault.xls";
//			book = Workbook.createWorkbook(new File(mURL + mExcelFileName));
//			mSQL = "select a.caseno 案件号,"
//				+"a.customerno 被保险人客户号,a.customername 被保险人姓名,a.idno 被保人身份证号,"
//				+ "(select AccDate from LLSubReport where subrptno in (select subrptno from llcaserela where caseno = a.caseno and caserelano = m.caserelano) fetch first 1 rows only) 发生日期,"
//				+ "(select HospStartDate from llfeemain where caseno=a.caseno  fetch first 1 rows only) 入院日期,"
//				+ "(select HospEndDate  from llfeemain where caseno=a.caseno fetch first 1 rows only) 出院日期,"
//				+ "(select accdesc from LLSubReport where subrptno in (select subrptno from llcaserela where caseno = a.caseno and caserelano = m.caserelano) fetch first 1 rows only) 事件信息,"
//				+ "sum(m.TabFeeMoney) 账单金额, (select riskname from lmriskapp where riskcode=m.riskcode fetch first 1 rows only) 赔付险种, m.GiveTypeDesc 赔付结论, m.GiveReasonDesc 赔付结论依据,"
//				+ "sum(m.realpay) 实陪金额, (select remark from LLCaseDrug where caseno=a.caseno fetch first 1 rows only ) 扣除明细,"
//				+ "(select bankname from ldbank where bankcode =(select BankCode  from ljaget where (otherno=a.caseno or otherno=a.rgtno) and othernotype='5' fetch first 1 rows only)) 赔款银行, "
//				+ "(select BankAccNo  from ljaget where (otherno=a.caseno or otherno=a.rgtno) and othernotype='5' fetch first 1 rows only) 赔款账号,"
//				+ "(select AccName  from ljaget where (otherno=a.caseno or otherno=a.rgtno) and othernotype='5' fetch first 1 rows only) 赔款账户名 "
//				+ "from llcase a ,llclaimdetail m "
//				+ "where a.caseno=m.caseno and exists(select 1 from ljaget where (otherno=a.caseno or otherno=a.rgtno) and confdate=(current date - 1 day)) and a.mngcom like '8695%'"
// 				+ "group by a.caseno,m.caserelano,a.rgtno,m.GiveTypeDesc,m.GiveReasonDesc,m.riskcode,a.customerno,a.customername,a.idno fetch first 500 rows only ";
//			rsWrapper = new RSWrapper();
//			if (!rsWrapper.prepareData(null, mSQL)) {
//	            System.out.println("保险理赔结果 __数据准备失败! ");
//	            return false;
//	        }
//			biaoqian = "保险理赔结果";
//			sheet = book.createSheet(biaoqian, 1);
//			
//			label1 = new Label(0, 0, "案件号");
//			label2 = new Label(1, 0, "被保险人客户号");
//			label3 = new Label(2, 0, "被保险人姓名");
//			label4 = new Label(3, 0, "被保人身份证号");
//			label5 = new Label(4, 0, "发生日期");
//			label6 = new Label(5, 0, "入院日期");
//			label7 = new Label(6, 0, "出院日期");
//			label8 = new Label(7, 0, "事件信息");
//			label9 = new Label(8, 0, "账单金额");
//			label10 = new Label(9, 0, "赔付险种");
//			label11 = new Label(10, 0, "赔付结论");
//			label12 = new Label(11, 0, "赔付结论依据");
//			label13 = new Label(12, 0, "实赔金额");
//			label14 = new Label(13, 0, "扣除明细");
//			label15 = new Label(14, 0, "赔款银行");
//			label16 = new Label(15, 0, "赔款账号");
//			label17 = new Label(16, 0, "赔款账户名");
//			//label15 = new Label(14, 0, "险种编码");
//			
//			sheet.addCell(label1);
//			sheet.addCell(label2);
//			sheet.addCell(label3);
//			sheet.addCell(label4);
//			sheet.addCell(label5);
//			sheet.addCell(label6);
//			sheet.addCell(label7);
//			sheet.addCell(label8);
//			sheet.addCell(label9);
//			sheet.addCell(label10);
//			sheet.addCell(label11);
//			sheet.addCell(label12);
//			sheet.addCell(label13);
//			sheet.addCell(label14);
//			sheet.addCell(label15);
//			sheet.addCell(label16);
//			sheet.addCell(label17);
//			//sheet.addCell(label15);
//			
//			k = 1;
//			do {
//	        	tSSRS = rsWrapper.getSSRS();
//	        	maxRow = tSSRS.getMaxRow();
//	        	maxRows[k] = maxRow;
//	        	for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
//	        		int j = i + maxRows[1]*(k-1);
//					label1 = new Label(0, j, tSSRS.GetText(i, 1));
//					label2 = new Label(1, j, tSSRS.GetText(i, 2));
//					label3 = new Label(2, j, tSSRS.GetText(i, 3));
//					label4 = new Label(3, j, tSSRS.GetText(i, 4));
//					label5 = new Label(4, j, tSSRS.GetText(i, 5));
//					label6 = new Label(5, j, tSSRS.GetText(i, 6));
//					label7 = new Label(6, j, tSSRS.GetText(i, 7));
//					label8 = new Label(7, j, tSSRS.GetText(i, 8));
//					label9 = new Label(8, j, tSSRS.GetText(i, 9));
//					label10 = new Label(9, j, tSSRS.GetText(i, 10));
//					label11 = new Label(10, j, tSSRS.GetText(i, 11));
//					label12 = new Label(11, j, tSSRS.GetText(i, 12));
//					label13 = new Label(12, j, tSSRS.GetText(i, 13));
//					label14 = new Label(13, j, tSSRS.GetText(i, 14));
//					label15 = new Label(14, j, tSSRS.GetText(i, 15));
//					label16 = new Label(15, j, tSSRS.GetText(i, 16));
//					label17 = new Label(16, j, tSSRS.GetText(i, 17));
//					//label15 = new Label(14, i, tSSRS.GetText(i, 15));
//					
//					this.validateLabel();
//				
//					sheet.addCell(label1);
//					sheet.addCell(label2);
//					sheet.addCell(label3);
//					sheet.addCell(label4);
//					sheet.addCell(label5);
//					sheet.addCell(label6);
//					sheet.addCell(label7);
//					sheet.addCell(label8);
//					sheet.addCell(label9);
//					sheet.addCell(label10);
//					sheet.addCell(label11);
//					sheet.addCell(label12);
//					sheet.addCell(label13);
//					sheet.addCell(label14);
//					sheet.addCell(label15);
//					sheet.addCell(label16);
//					sheet.addCell(label17);
//					//sheet.addCell(label15);
//	        	}
//	        	k = k + 1;
//			} while (tSSRS != null && tSSRS.MaxRow > 0);
//			book.write();
//			book.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		}
		
		return true;
	}
	// 保险理赔进度 --增加3个字段 2017-05-19   ***2017-5-26*** 提数口径变更  #3366 end
	/**
	 * 本地文件上传至Ftp
	 * @return
	 */
	private boolean transToFtp(){
        ExeSQL tExeSql = new ExeSQL();

        String tServerIP = tExeSql.getOneValue("select codename from ldcode where codetype='LLWXClaim' and code='IP/Port'");
//        String tServerIP ="10.252.4.88"; //外测地址
        String tPort = tExeSql.getOneValue("select codealias from ldcode where codetype='LLWXClaim' and code='IP/Port'");
        String tUsername = tExeSql.getOneValue("select codename from ldcode where codetype='LLWXClaim' and code='User/Pass'");
        String tPassword = tExeSql.getOneValue("select codealias from ldcode where codetype='LLWXClaim' and code='User/Pass'");
  
        try {
            FTPTool tFTPTool = new FTPTool(tServerIP, tUsername, tPassword, Integer.parseInt(tPort), "aaActiveMode");
            if (!tFTPTool.loginFTP()) {
                CError tError = new CError();
                tError.moduleName = "CardActiveBatchImportBL";
                tError.functionName = "getXls";
                tError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
            
            // 本地测试
            // 本地文件的目录
//            String tFilePathLocal = "E:/哈哈/666/";
            
            // 取得本地存放文件的目录
            String tFilePathLocal1 = mURL+"PolicyInfo.xls";
            String tFilePathLocal2 = mURL+"GrpPolicyInfo.xls";
            // 要上传的文件在ftp上的存放目录
            String tFileTransPath = "/01PH/8695/WXPolicyInfo/" + PubFun.getCurrentDate();
            // 改变ftp服务器的当前工作目录
            tFTPTool.changeWorkingDirectory("/01PH/8695/WXPolicyInfo/");
            //tFTPTool.makeDirectory("ceshi");
            // 创建目录
            if(!tFTPTool.makeDirectory(PubFun.getCurrentDate())){
            	System.out.println("FTP上新建目录已存在");
            }
            // 上传所有目录下的文件
            if(!tFTPTool.upload(tFileTransPath, tFilePathLocal1)||!tFTPTool.upload(tFileTransPath, tFilePathLocal2)) {
            	System.out.println("文件上传失败！！！");
            	return false;
            }
            // 关闭FTP连接
            //System.out.println(System.currentTimeMillis());
            tFTPTool.logoutFTP();
            
        }
        catch (SocketException ex) {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "ExcelToFtpBL";
            tError.functionName = "transToFtp";
            tError.errorMessage = "获取连接失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        catch (IOException ex) {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "ExcelToFtpBLBL";
            tError.functionName = "transToFtp";
            tError.errorMessage = "连接ftp失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        return true;
        
    }
	
	/**
	 * 判断Excel表中数据为"null"时，将其修改为""。
	 */
	public void validateLabel() {
		if(label1.getString().equals("null")) {label1.setString("");}
		if(label2.getString().equals("null")) {label2.setString("");}
		if(label3.getString().equals("null")) {label3.setString("");}
		if(label4.getString().equals("null")) {label4.setString("");}
		if(label5.getString().equals("null")) {label5.setString("");}
		if(label6.getString().equals("null")) {label6.setString("");}
		if(label7.getString().equals("null")) {label7.setString("");}
		if(label8.getString().equals("null")) {label8.setString("");}
		if(label9.getString().equals("null")) {label9.setString("");}
		if(label10.getString().equals("null")) {label10.setString("");}
		if(label11.getString().equals("null")) {label11.setString("");}
		if(label12.getString().equals("null")) {label12.setString("");}
		if(label13.getString().equals("null")) {label13.setString("");}
		if(label14.getString().equals("null")) {label14.setString("");}
		if(label15.getString().equals("null")) {label15.setString("");}
		if(label16.getString().equals("null")) {label16.setString("");}
		if(label17.getString().equals("null")) {label17.setString("");}
		if(label18.getString().equals("null")) {label18.setString("");}
		if(label19.getString().equals("null")) {label19.setString("");}
		if(label20.getString().equals("null")) {label20.setString("");}
		if(label21.getString().equals("null")) {label21.setString("");}
		if(label22.getString().equals("null")) {label22.setString("");}
		if(label23.getString().equals("null")) {label23.setString("");}
		if(label24.getString().equals("null")) {label24.setString("");}
		if(label25.getString().equals("null")) {label25.setString("");}
		if(label26.getString().equals("null")) {label26.setString("");}
		if(label27.getString().equals("null")) {label27.setString("");}
		if(label28.getString().equals("null")) {label28.setString("");}
		if(label29.getString().equals("null")) {label29.setString("");}
		if(label30.getString().equals("null")) {label30.setString("");}
		if(label31.getString().equals("null")) {label31.setString("");}

		
	}
	
	/**
	 * 创建文件夹
	 * @param cRootPath
	 * @return
	 */
	public boolean save(String cRootPath){
        StringBuffer mFilePath = new StringBuffer();

        mFilePath.append(getSavePath(cRootPath));

        File mFileDir = new File(mFilePath.toString());
        if (!mFileDir.exists()){
            if (!mFileDir.mkdirs()){
                System.err.println("创建目录[" + mFilePath.toString() + "]失败！" + mFileDir.getPath());
                return false;
            }
        }
        return true;
        
    }
	
	/**
	 * 返回目录路径
	 * @param cRootPath
	 * @return cRootPath/yyyy/yyyyMM/yyyyMMdd/
	 */
	public String getSavePath(String cRootPath){
        StringBuffer tFilePath = new StringBuffer();

        tFilePath.append(cRootPath);
        /*tFilePath.append(getCurrentDate("yyyy"));
        tFilePath.append('/');
        tFilePath.append(getCurrentDate("yyyyMM"));
        tFilePath.append('/');*/
        tFilePath.append(getCurrentDate("yyyyMMdd"));
        tFilePath.append('/');
        return tFilePath.toString();
        
    }
	
	/**
	 * 根据格式输出日期
	 * @param pDateFormat
	 * @return
	 */
	private String getCurrentDate(String pDateFormat) {
		return new SimpleDateFormat(pDateFormat).format(new Date());
	}
	
	
	/**
	 * 测试
	 * @param args
	 */
	public static void main(String[] args) {
		LLSZExcelTransFtp test = new LLSZExcelTransFtp();
		test.dealData();
		System.out.println("执行完毕！！！！");
	}
	
	
}
