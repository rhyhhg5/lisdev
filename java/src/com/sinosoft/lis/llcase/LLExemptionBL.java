package com.sinosoft.lis.llcase;

import java.util.Date;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.llcase.*;

/**
 * <p>Title: 医保通结算批次申请</p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author
 * @version 1.0
 * @date
 */

public class LLExemptionBL {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;
    
    private LLExemptionSet mLLExemptionSet = new LLExemptionSet();
    
    private LLExemptionSchema mLLExemptionSchema = new LLExemptionSchema();
    private LLExemptionSchema mULLExemptionSchema = new LLExemptionSchema();

    private MMap map = new MMap();
    
    /** LLExemption主键*/
    private String mCaseNo;
    private String mPolNo;
    private String mCaseType;
    
    /** 全局参数 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();

    /** 统一更新日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 统一更新时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    private String mOperator = "";

    private String mManageCom = "";

    public LLExemptionBL() {
    }

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        // 将外部传入的数据分解到本类的属性中，准备处理
        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        // 根据业务逻辑对数据进行处理
        if (!dealData()) {
            return false;
        }

        // 装配处理好的数据，准备给后台进行保存
        prepareOutputData();

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLExemptionBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData() {
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mLLExemptionSchema.setSchema((LLExemptionSchema) mInputData.getObjectByObjectName("LLExemptionSchema", 0));
        mLLExemptionSet.set((LLExemptionSet) mInputData.getObjectByObjectName("LLExemptionSet", 0));
        mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData",0);
        
        mOperator = mGlobalInput.Operator;
        mManageCom = mGlobalInput.ManageCom;

        if (mManageCom.length() == 2) {
            mManageCom = mManageCom + "000000";
        } else if (mManageCom.length() == 4) {
            mManageCom = mManageCom + "0000";
        }

        mCaseNo = mLLExemptionSchema.getCaseNo();
        mCaseType = mCaseNo.substring(0,1);
       
        return true;
    }

    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData() {
        if (mOperate == null || "".equals(mOperate)) {
            buildError("checkData", "Operate为空");
            return false;
        }

        if (!"SAVE".equals(mOperate)&&!"CANCEL".equals(mOperate)) {
            if (mLLExemptionSet.size() < 1) {
                buildError("checkData", "未获取案件信息");
                return false;
            }
        }

        if (mCaseNo == null || "".equals(mCaseNo)) {
            buildError("checkData", "未获取赔案信息");
            return false;
        }

        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData() {
    	 
        if ("SAVE".equals(mOperate)) {
        	
        	if("R".equals(mCaseType) || "S".equals(mCaseType))
        	{
        		System.out.println("申诉纠错案件换为正常的C案件");
                String sql = "select caseno from LLAppeal where appealno ='"+mCaseNo+"'";
                ExeSQL exeSQL = new ExeSQL();
                mCaseNo = exeSQL.getOneValue(sql);
        	}
            System.out.println("确认豁免保费");
            
            LLExemptionSet tLLExemptionSet = new LLExemptionSet();
            
            for (int i = 1; i <= mLLExemptionSet.size(); i++) {
                
                String tRiskCode = mLLExemptionSet.get(i).getRiskCode();
                String tPolNo = mLLExemptionSet.get(i).getPolNo();
                String tContNo = mLLExemptionSet.get(i).getContNo();
                String tPayIntv = mLLExemptionSet.get(i).getPayIntv();
                String tAccDate = "";
                String tAccPayToDate = "";//出险日期所在缴至日
                String tCount = "";//多缴保费期数
                
                
                //检查是否做过"保费豁免"
                String tSQL = "select caseno,state,riskcode from llexemption where contno='"+tContNo+"' and polno = '"+tPolNo+"'"; 
                ExeSQL mExeSQL = new ExeSQL();
                SSRS mSSRS = new SSRS();
                mSSRS = mExeSQL.execSQL(tSQL);
                if (mSSRS == null || mSSRS.getMaxRow() <= 0) {
                    //没有记录正常进行
                }else{
                    for(int k=1;k <=mSSRS.getMaxRow();k++){
                        String aCaseNo = mSSRS.GetText(k, 1);
                        String aState = mSSRS.GetText(k, 2);
                        String aRiskCode = mSSRS.GetText(k, 3);
                        if("temp".equals(aState)){
                            CError.buildErr(this, "案件号："+ aCaseNo +"下险种代码:"+ aRiskCode +"处于豁免待审批状态，不能继续豁免保费");
                            return false;
                        }
//                        else if("1".equals(aState)){
//                            CError.buildErr(this, "案件号："+ aCaseNo +"下险种代码:"+ aRiskCode +"处于申诉纠错状态，不能继续豁免保费");
//                            return false;
//                        }
                        else if("0".equals(aState)){
                            CError.buildErr(this, "案件号："+ aCaseNo +"下险种代码:"+ aRiskCode +"已经完成豁免保费，不能再次豁免");
                            return false;
                        }
                        
                    }
                }
                
                //判断是否多交保费
                //查询出险日期
                String SQL = "select accdate from LLSubReport where subrptno in( "+
                    "select subrptno from llcaserela where caseno='"+mCaseNo+"')";
                ExeSQL tExeSQL = new ExeSQL();
                SSRS tSSRS = new SSRS();
                tSSRS = tExeSQL.execSQL(SQL);
                if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
                    CError.buildErr(this, "获取出险日期失败");
                    return false;
                }

                //计次
                int count = 0;
                for(int k=1;k <=tSSRS.getMaxRow();k++){
                    tAccDate = tSSRS.GetText(k, 1);
                    System.out.println("tAccDate:"+tAccDate);
                    //首先判断其出险日期是否存在退保费情况
                    
                    String SQL1 = "select curpaytodate from ljapayperson a where "+
                    "exists (select 1 from ljapay b where a.contno = b.incomeno) "+
                    "and contno ='"+tContNo+"' and riskcode ='"+tRiskCode+"' "+
                    "and '"+tAccDate+"' between lastpaytodate and curpaytodate "+
                    "order by contno ";
                    ExeSQL tExeSQL1 = new ExeSQL();
                    SSRS tSSRS1 = new SSRS();
                    tSSRS1 = tExeSQL1.execSQL(SQL1);
                    if (tSSRS1 == null || tSSRS1.getMaxRow() <= 0) {
                        CError.buildErr(this, "获取出险日期所在缴至日期失败");
                        return false;
                    }
                    for(int m=1;m <=tSSRS1.getMaxRow();m++){
                        tAccPayToDate = tSSRS1.GetText(m, 1);
                    }
                    //计算退缴保费
                    String SQL2 = "select payno from ljapayperson c," +
                            "(select curpaytodate,contno,riskcode from ljapayperson a where " +
                            "exists (select 1 from ljapay b where a.contno = b.incomeno and duefeetype ='1') " +
                            "and contno ='"+tContNo+"' and riskcode ='"+tRiskCode+"' " +
                            "and '"+tAccPayToDate+"' > lastpaytodate and '"+tAccPayToDate+"' <= curpaytodate) aa " +
                            "where c.contno=aa.contno and c.curpaytodate >aa.curpaytodate " +
                            "and c.riskcode = aa.riskcode group by payno";
                    ExeSQL tExeSQL2 = new ExeSQL();
                    SSRS tSSRS2 = new SSRS();
                    tSSRS2 = tExeSQL2.execSQL(SQL2);
                    if(tSSRS2 != null && tSSRS.getMaxRow() > 0){
                        count++;
                    }
                }
                if(count >= 2){
                    CError.buildErr(this, "案件下关联多个事件，请回退至检录状态，关联正确的事件");
                    return false;
                }
                
//              处理豁免险种间关系
                //1.豁免被保人
                String aSQL = "select riskcode from lcpol where contno='"+tContNo+"' and polno='"+tPolNo+"'" +
                        "and riskcode in( select riskcode from lmriskapp where risktype8='8')";
                ExeSQL aExeSQL = new ExeSQL();
                SSRS aSSRS = new SSRS();
                aSSRS = aExeSQL.execSQL(aSQL);
                
                //2.豁免投保人
                String bSQL = "select riskcode from lcpol where contno='"+tContNo+"' and polno='"+tPolNo+"'" +
                "and riskcode in( select riskcode from lmriskapp where risktype8='9' and kindcode='S')";
                ExeSQL bExeSQL = new ExeSQL();
                SSRS bSSRS = new SSRS();
                bSSRS = bExeSQL.execSQL(bSQL);
                
                //逻辑判断
                if(aSSRS != null && aSSRS.getMaxRow() > 0){
                    
                    String SQL0 = "select riskcode,polno from lcpol where contno='"+tContNo+"' and " +
                    "riskcode in(select distinct code1 from ldcode1 where " +
                    "(code1='"+tRiskCode+"' or code='"+tRiskCode+"') and codetype='checkappendrisk' union " +
                    "select distinct code from ldcode1 where (code1='"+tRiskCode+"' or code='"+tRiskCode+"') " +
                    "and codetype='checkappendrisk' union select riskcode from lmriskapp where risktype8='8' and riskcode='"+tRiskCode+"')";
                    ExeSQL tExeSQL0 = new ExeSQL();
                    SSRS tSSRS0 = new SSRS();
                    tSSRS0 = tExeSQL0.execSQL(SQL0);
                    if (tSSRS0 == null || tSSRS0.getMaxRow() <= 0) {
                        CError.buildErr(this, "保单下豁免险种获取失败");
                        return false;
                    }
                    for(int j = 1; j <= tSSRS0.getMaxRow();j++){
                     LLExemptionSchema tLLExemptionSchema = new LLExemptionSchema();
                     String tInsuredNo = mLLExemptionSchema.getInsuredNo();
                     String tInsuredName = mLLExemptionSchema.getInsuredName();
                     String tRemark = mLLExemptionSchema.getRemark();
                     String tPayEndDate = mLLExemptionSet.get(i).getPayEndDate();  
                     String tFreeStartDate = mLLExemptionSet.get(i).getFreeStartDate();
                     String tFreePriedDate = "";//记录豁免日期所在的缴至日
                     String tFreeEndDate = mLLExemptionSet.get(i).getFreeEndDate();
                     String tFreePried = mLLExemptionSet.get(i).getFreePried();
                     double tFreeAmnt = 0.00;//多缴保费
                     tRiskCode = tSSRS0.GetText(j, 1);
                     tPolNo = tSSRS0.GetText(j, 2);
                     tLLExemptionSchema.setRiskCode(tRiskCode);
                     tLLExemptionSchema.setPolNo(tPolNo);
                     System.out.println("1.tRiskCode:"+tRiskCode);
                     System.out.println("1.tPolNo:"+tPolNo);
                     //期数处理 
                     String SQL1 = "select curpaytodate from ljapayperson a where "+
                         "exists (select 1 from ljapay b where a.contno = b.incomeno) "+
                         "and contno ='"+tContNo+"' and riskcode ='"+tRiskCode+"' "+
                         "and '"+tAccDate+"' between lastpaytodate and curpaytodate "+
                         "order by contno ";
                     ExeSQL tExeSQL1 = new ExeSQL();
                     SSRS tSSRS1 = new SSRS();
                     tSSRS1 = tExeSQL1.execSQL(SQL1);
                     if (tSSRS1 == null || tSSRS1.getMaxRow() <= 0) {
                         CError.buildErr(this, "获取出险日期所在缴至日期失败");
                         return false;
                     }
                     for(int k=1;k <=tSSRS1.getMaxRow();k++){
                         tAccPayToDate = tSSRS1.GetText(k, 1);
                         tLLExemptionSchema.setPayToDate(tAccPayToDate);
                         System.out.println("tAccPayToDate:"+tAccPayToDate);
                     }
                     
                     //改变期数计算规则，以豁免起期所在的缴至日期开始算起
                     String tFreePriedSql = "select curpaytodate from ljapayperson a where "+
                         "exists (select 1 from ljapay b where a.contno = b.incomeno) "+
                         "and contno ='"+tContNo+"' and riskcode ='"+tRiskCode+"' "+
                         "and '"+tFreeStartDate+"' between lastpaytodate and curpaytodate "+
                         "order by contno ";
                     ExeSQL tFreePriedExeSQL = new ExeSQL();
                     SSRS tFreePriedSSRS = new SSRS();
                     tFreePriedSSRS = tFreePriedExeSQL.execSQL(tFreePriedSql);
                     if (tFreePriedSSRS == null || tFreePriedSSRS.getMaxRow() <= 0) {
                         CError.buildErr(this, "获取豁免开始日期所在缴至日期失败");
                         return false;
                     }
                     
                     for(int k=1;k <=tFreePriedSSRS.getMaxRow();k++){
                         tFreePriedDate = tFreePriedSSRS.GetText(k, 1);
                         System.out.println("tFreePriedDate:"+tFreePriedDate);
                     }
                     
                     //计算退缴保费
                     String SQL2 = "select payno,sum(sumduepaymoney) from ljapayperson c," +
                             "(select curpaytodate,contno,riskcode from ljapayperson a where " +
                             "exists (select 1 from ljapay b where a.contno = b.incomeno and duefeetype ='1') " +
                             "and contno ='"+tContNo+"' and riskcode ='"+tRiskCode+"' " +
                             "and '"+tAccPayToDate+"' > lastpaytodate and '"+tAccPayToDate+"' <= curpaytodate) aa " +
                             "where c.contno=aa.contno and c.curpaytodate >aa.curpaytodate " +
                             "and c.riskcode = aa.riskcode group by payno";
                     ExeSQL tExeSQL2 = new ExeSQL();
                     SSRS tSSRS2 = new SSRS();
                     tSSRS2 = tExeSQL2.execSQL(SQL2);
                     if (tSSRS2 != null && tSSRS2.getMaxRow() > 0) {
                         //退费期数tCount
                         tCount = tSSRS2.getMaxRow()+"";
                         System.out.println("tCount:"+tCount );
                         for(int k=1;k <=tSSRS2.getMaxRow();k++){
                             double temp = Double.parseDouble(tSSRS2.GetText(k, 2));
                             tFreeAmnt += temp; 
                             System.out.println("tFreeAmnt:"+tFreeAmnt);
                         }
                     }
                     tLLExemptionSchema.setFreeAmnt(tFreeAmnt);

                     //计算豁免期数
                     int num = 12;
                     
                     if((PubFun.calInterval(tFreePriedDate, tFreeEndDate, "M"))%num == 0){
                         tFreePried = (PubFun.calInterval(tFreePriedDate, tFreeEndDate, "M"))/num+"";
                     }else{
                         tFreePried = ((PubFun.calInterval(tFreePriedDate, tFreeEndDate, "M"))/num)+1+"";
                     }
                     System.out.println("tFreePried:"+tFreePried);
                     
                     
                     tLLExemptionSchema.setCaseNo(mCaseNo);
                     tLLExemptionSchema.setContNo(tContNo);
                     tLLExemptionSchema.setInsuredNo(tInsuredNo);
                     tLLExemptionSchema.setInsuredName(tInsuredName);
                     tLLExemptionSchema.setPayEndDate(tPayEndDate);
                     tLLExemptionSchema.setState("temp");
                     tLLExemptionSchema.setPayIntv(tPayIntv);
                     tLLExemptionSchema.setFreeStartDate(tFreeStartDate);
                     tLLExemptionSchema.setFreeEndDate(tFreeEndDate);
                     tLLExemptionSchema.setRemark(tRemark);
                     tLLExemptionSchema.setMngCom(mManageCom);
                     tLLExemptionSchema.setOperator(mOperator);
                     tLLExemptionSchema.setMakeDate(mCurrentDate);
                     tLLExemptionSchema.setMakeTime(mCurrentTime);
                     tLLExemptionSchema.setModifyDate(mCurrentDate);
                     tLLExemptionSchema.setModifyTime(mCurrentTime);
                     tLLExemptionSchema.setLocation("RE"); // #3310 人工豁免的标志
                     
                     //重要字段
                     tLLExemptionSchema.setFreePried(tFreePried);//豁免期数
                     
                     tLLExemptionSet.add(tLLExemptionSchema);
                    }
                }else if(bSSRS != null && bSSRS.getMaxRow() > 0)
                
                {//2.险种出险，自身豁免同时豁免其相关险种
                    String SQL0 = "select riskcode,polno,insuredno,insuredname from lcpol where contno='"+tContNo+"' and " +
                        "riskcode in(select distinct a.code1 from ldcode1 a,ldcode1 b where a.code1=b.code " +
                        "and b.code1='"+tRiskCode+"' and b.codetype='checkexemptionrisk' and " +
                        "a.codetype='checkappendrisk' union select distinct a.code from ldcode1 a,ldcode1 b " +
                        "where a.code1=b.code and b.code1='"+tRiskCode+"' and b.codetype='checkexemptionrisk' " +
                        "and a.codetype='checkappendrisk' union select distinct b.code1 from ldcode1 a," +
                        "ldcode1 b where a.code1=b.code and b.code1='"+tRiskCode+"' and b.codetype='checkexemptionrisk' " +
                        "and a.codetype='checkappendrisk' union select riskcode from lmriskapp where risktype8='9' and kindcode='S' and riskcode='"+tRiskCode+"')"; 
                    ExeSQL tExeSQL0 = new ExeSQL();
                    SSRS tSSRS0 = new SSRS();
                    tSSRS0 = tExeSQL0.execSQL(SQL0);
                    if (tSSRS0 == null || tSSRS0.getMaxRow() <= 0) {
                        CError.buildErr(this, "保单下豁免险种获取失败");
                        return false;
                    }
                    for(int j=1;j <=tSSRS0.getMaxRow();j++){
                        LLExemptionSchema tLLExemptionSchema = new LLExemptionSchema();
                        String tInsuredNo = mLLExemptionSchema.getInsuredNo();
                        String tInsuredName = mLLExemptionSchema.getInsuredName();
                        String tRemark = mLLExemptionSchema.getRemark();
                        String tPayEndDate = mLLExemptionSet.get(i).getPayEndDate();  
                        String tFreeStartDate = mLLExemptionSet.get(i).getFreeStartDate();
                        String tFreePriedDate = "";//记录豁免日期所在的缴至日
                        String tFreeEndDate = mLLExemptionSet.get(i).getFreeEndDate();
                        String tFreePried = mLLExemptionSet.get(i).getFreePried();
                        double tFreeAmnt = 0.00;//多缴保费
                        
                        tRiskCode = tSSRS0.GetText(j, 1);
                        tPolNo = tSSRS0.GetText(j, 2);
                        tInsuredNo = tSSRS0.GetText(j, 3);
                        tInsuredName = tSSRS0.GetText(j, 4);
                        tLLExemptionSchema.setRiskCode(tRiskCode);
                        tLLExemptionSchema.setPolNo(tPolNo);
                        tLLExemptionSchema.setInsuredNo(tInsuredNo);
                        tLLExemptionSchema.setInsuredName(tInsuredName);
                        System.out.println("2.tRiskCode:"+tRiskCode);
                        System.out.println("2.tPolNo:"+tPolNo);
                        System.out.println("2.tInsuredNo:"+tInsuredNo);
                        System.out.println("2.tInsuredName:"+tInsuredName);
                        //期数处理 
                        String SQL1 = "select curpaytodate from ljapayperson a where "+
                            "exists (select 1 from ljapay b where a.contno = b.incomeno) "+
                            "and contno ='"+tContNo+"' and riskcode ='"+tRiskCode+"' "+
                            "and '"+tAccDate+"' between lastpaytodate and curpaytodate "+
                            "order by contno ";
                        ExeSQL tExeSQL1 = new ExeSQL();
                        SSRS tSSRS1 = new SSRS();
                        tSSRS1 = tExeSQL1.execSQL(SQL1);
                        if (tSSRS1 == null || tSSRS1.getMaxRow() <= 0) {
                            CError.buildErr(this, "获取出险日期所在缴至日期失败");
                            return false;
                        }
                        for(int k=1;k <=tSSRS1.getMaxRow();k++){
                            tAccPayToDate = tSSRS1.GetText(k, 1);
                            tLLExemptionSchema.setPayToDate(tAccPayToDate);
                            System.out.println("tAccPayToDate:"+tAccPayToDate);
                        }
                        
//                      改变期数计算规则，以豁免起期所在的缴至日期开始算起
                        String tFreePriedSql = "select curpaytodate from ljapayperson a where "+
                            "exists (select 1 from ljapay b where a.contno = b.incomeno) "+
                            "and contno ='"+tContNo+"' and riskcode ='"+tRiskCode+"' "+
                            "and '"+tFreeStartDate+"' between lastpaytodate and curpaytodate "+
                            "order by contno ";
                        ExeSQL tFreePriedExeSQL = new ExeSQL();
                        SSRS tFreePriedSSRS = new SSRS();
                        tFreePriedSSRS = tFreePriedExeSQL.execSQL(tFreePriedSql);
                        if (tFreePriedSSRS == null || tFreePriedSSRS.getMaxRow() <= 0) {
                            CError.buildErr(this, "获取豁免开始日期所在缴至日期失败");
                            return false;
                        }
                        
                        for(int k=1;k <=tFreePriedSSRS.getMaxRow();k++){
                            tFreePriedDate = tFreePriedSSRS.GetText(k, 1);
                            System.out.println("tFreePriedDate:"+tFreePriedDate);
                        }
                        
                        //计算退缴保费
                        String SQL2 = "select payno,sum(sumduepaymoney) from ljapayperson c," +
                                "(select curpaytodate,contno,riskcode from ljapayperson a where " +
                                "exists (select 1 from ljapay b where a.contno = b.incomeno and duefeetype ='1') " +
                                "and contno ='"+tContNo+"' and riskcode ='"+tRiskCode+"' " +
                                "and '"+tAccPayToDate+"' > lastpaytodate and '"+tAccPayToDate+"' <= curpaytodate) aa " +
                                "where c.contno=aa.contno and c.curpaytodate >aa.curpaytodate " +
                                "and c.riskcode = aa.riskcode group by payno";
                        ExeSQL tExeSQL2 = new ExeSQL();
                        SSRS tSSRS2 = new SSRS();
                        tSSRS2 = tExeSQL2.execSQL(SQL2);
                        if (tSSRS2 != null && tSSRS2.getMaxRow() > 0) {
                            //退费期数tCount
                            tCount = tSSRS2.getMaxRow()+"";
                            System.out.println("tCount:"+tCount );
                            for(int k=1;k <=tSSRS2.getMaxRow();k++){
                                double temp = Double.parseDouble(tSSRS2.GetText(k, 2));
                                tFreeAmnt += temp; 
                                System.out.println("tFreeAmnt:"+tFreeAmnt);
                            }
                        }
                        tLLExemptionSchema.setFreeAmnt(tFreeAmnt);
                       
                        //计算豁免期数
                        int num = 12;
   
                        if((PubFun.calInterval(tFreePriedDate, tFreeEndDate, "M"))%num == 0){
                            tFreePried = (PubFun.calInterval(tFreePriedDate, tFreeEndDate, "M"))/num+"";
                        }else{
                            tFreePried = ((PubFun.calInterval(tFreePriedDate, tFreeEndDate, "M"))/num)+1+"";
                        }
                        
                        System.out.println("tFreePried:"+tFreePried);
                        
                        tLLExemptionSchema.setCaseNo(mCaseNo);
                        tLLExemptionSchema.setContNo(tContNo);
                        tLLExemptionSchema.setPayEndDate(tPayEndDate);
                        tLLExemptionSchema.setState("temp");
                        tLLExemptionSchema.setPayIntv(tPayIntv);
                        tLLExemptionSchema.setFreeStartDate(tFreeStartDate);
                        tLLExemptionSchema.setFreeEndDate(tFreeEndDate);
                        tLLExemptionSchema.setRemark(tRemark);
                        tLLExemptionSchema.setMngCom(mManageCom);
                        tLLExemptionSchema.setOperator(mOperator);
                        tLLExemptionSchema.setMakeDate(mCurrentDate);
                        tLLExemptionSchema.setMakeTime(mCurrentTime);
                        tLLExemptionSchema.setModifyDate(mCurrentDate);
                        tLLExemptionSchema.setModifyTime(mCurrentTime);
                        tLLExemptionSchema.setLocation("RE"); //# 3310 人工豁免标志 
                        
                        //重要字段
                        tLLExemptionSchema.setFreePried(tFreePried);//豁免期数
                        
                        tLLExemptionSet.add(tLLExemptionSchema);
                    }
                }else{//A主险B附加险，B带有豁免责任。被保人出险豁免A不豁免B；（系统目前不存在）
                
                }
            }
            map.put(tLLExemptionSet, "DELETE&INSERT");
        }

        if ("CANCEL".equals(mOperate)) {
            
        
            System.out.println("取消豁免保费");
            //LLExemptionSet uLLExemptionSet = new LLExemptionSet();
            
            if("C".equals(mCaseType)){
            for (int i = 1; i <= mLLExemptionSet.size(); i++) { 
                LLExemptionSet tLLExemptionSet = new LLExemptionSet();
                System.out.println("开始处理！");
                String tPolNo = mLLExemptionSet.get(i).getPolNo();
                
                LLExemptionDB tLLExemptionDB = new LLExemptionDB();
                tLLExemptionDB.setCaseNo(mCaseNo);
                tLLExemptionDB.setPolNo(tPolNo);
                tLLExemptionSet = tLLExemptionDB.query();
                
                for(int j=1;j<=tLLExemptionSet.size();j++){
//                    LLExemptionSchema tLLExemptionSchema = new LLExemptionSchema();
//                    tLLExemptionSchema.setCaseNo(mCaseNo);
//                    tLLExemptionSchema.setPolNo(tPolNo);
//                    tLLExemptionSchema.setState(tLLExemptionSet.get(j).getState());
//                    tLLExemptionSchema.setFreePried(tLLExemptionSet.get(j).getFreePried());
//                    tLLExemptionSchema.setOperator(tLLExemptionSet.get(j).getOperator());
//                    tLLExemptionSchema.setModifyDate(tLLExemptionSet.get(j).getModifyDate());
//                    tLLExemptionSchema.setModifyTime(tLLExemptionSet.get(j).getModifyTime());
                    tLLExemptionSet.get(j).setState("2");
                    tLLExemptionSet.get(j).setOperator(mOperator);
                    tLLExemptionSet.get(j).setModifyDate(mCurrentDate);
                    tLLExemptionSet.get(j).setModifyTime(mCurrentTime);
                    
                }
                map.put(tLLExemptionSet, "UPDATE");
            }
           }
            if("R".equals(mCaseType) || "S".equals(mCaseType)){

                String sql = "select caseno from LLAppeal where appealno ='"+mCaseNo+"'";
                ExeSQL exeSQL = new ExeSQL();
                mCaseNo = exeSQL.getOneValue(sql);
                
                for (int i = 1; i <= mLLExemptionSet.size(); i++) { 
                    LLExemptionSet tLLExemptionSet = new LLExemptionSet();
                    System.out.println("开始处理！");
                    String tPolNo = mLLExemptionSet.get(i).getPolNo();
                    
                    LLExemptionDB tLLExemptionDB = new LLExemptionDB();
                    tLLExemptionDB.setCaseNo(mCaseNo);
                    tLLExemptionDB.setPolNo(tPolNo);
                    tLLExemptionSet = tLLExemptionDB.query();
                    
                    for(int j=1;j<=tLLExemptionSet.size();j++){
                        tLLExemptionSet.get(j).setState("1");
                        tLLExemptionSet.get(j).setOperator(mOperator);
                        tLLExemptionSet.get(j).setModifyDate(mCurrentDate);
                        tLLExemptionSet.get(j).setModifyTime(mCurrentTime);
                        
                    }
                    map.put(tLLExemptionSet, "UPDATE");
                }
            }
            
        }
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData() {
        mInputData.clear();
        mInputData.add(map);
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult() {
        return mResult;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLExemptionBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    public static void main(String arg[]) {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "001";
        LLExemptionSchema tLLExemptionSchema = new LLExemptionSchema();
        LLExemptionSchema uLLExemptionSchema = new LLExemptionSchema();
        LLExemptionSet tLLExemptionSet = new LLExemptionSet();
        tLLExemptionSchema.setCaseNo("C1100131204000001");

        
    }
}
