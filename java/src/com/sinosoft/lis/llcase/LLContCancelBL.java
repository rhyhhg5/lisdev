/*
 * <p>ClassName: LLContCancelBL </p>
 * <p>Description: LLContCancelBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2005-02-26 15:27:43
 */
package com.sinosoft.lis.llcase;

import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LLContCancelBL {    
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mResult = new VData();
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String mEdorNo = "";

    /** 业务处理相关变量 */
   
    private LLContDealSchema mLLContDealSchema = new LLContDealSchema();
    public LLContCancelBL() {
    }
/**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * @cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //取出操作符
        this.mOperate = cOperate;
        System.out.println("begin submit");

        //取出页面传入数据
        if (!getInputData(cInputData)) {
            return false;
        }

        if (!checkInputData()) {
        	 return false;
        }

        //进行业务处理
        if (!dealData()) {
        	CError.buildErr(this, "撤销续期催收异常");
            return false;
        }
        System.out.println("after dealdata");
        //数据提交
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
        	CError.buildErr(this, "数据提交失败");
            return false;
        }

        System.out.println("after tPubSubmit");
        mInputData = null;
        return true;
    }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("LLContCancelBL.getinputData......");
        mLLContDealSchema = (LLContDealSchema) cInputData.getObjectByObjectName("LLContDealSchema", 0);
      
        if (mLLContDealSchema.getEdorNo() == null) {
            CError.buildErr(this, "获取合同处理受理号失败！");
            return false;
        }       
        mEdorNo = mLLContDealSchema.getEdorNo();
        return true;
    }
    private boolean checkInputData() {
    	return true;
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {

    	LLContDealDB aLLContDealDB =new LLContDealDB();
    	LLContDealSchema aLLContDealSchema =new LLContDealSchema();
    	aLLContDealDB.setEdorNo(mEdorNo);
    	if(aLLContDealDB.getInfo())
    	{
    		aLLContDealSchema = aLLContDealDB.getSchema();
    		aLLContDealSchema.setEdorType("");
    	}
    	map.put(aLLContDealSchema, "DELETE&INSERT");
    	mInputData.add(map);
    	System.out.println("LLContCancelBL.dealData......");
    	return true;
    }
    
    public VData getResult() {
        return mResult;
    }
}