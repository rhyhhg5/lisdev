package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.utility.*;
import com.sinosoft.utility.StrTool;
import org.jdom.Element;
import java.io.File;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 理赔批量打印</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Xx
 * @version 1.0
 */
public class LLBatchPrtBL {
    public CErrors mErrors = new CErrors();
    private MMap tmpMap = new MMap();
    private VData mResult = new VData();
    private String mOperate;
    private GlobalInput mG = new GlobalInput();
    public LLBatchPrtBL() {}

    private LOBatchPRTManagerSet mLOBatchPRTManagerSet = new
            LOBatchPRTManagerSet();
    private TransferData mPrintFactor = new TransferData();
    private LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();
    private String mUserCode = "";
    // add new parameters
    private boolean operFlag = true;
    private String strLogs = "";
    private String Content = "";
    private int iFlag = 0;
    private String mxmlFileName = "";
    private int mCount = 0;
    private String ActuGetNo;

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        cInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        System.out.println(mOperate + "LL");
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        //存储打印管理表时,执行下面操作
        if (mOperate.equals("INSERT")) {
            PubSubmit ps = new PubSubmit();
            if (!ps.submitData(this.mResult, null)) {
                CError.buildErr(this, "数据保存失败");
                return false;
            }
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    private boolean getInputData(VData cInputData) {
        mPrintFactor = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLDSysVarSchema.setSchema((LDSysVarSchema) cInputData.
                                  getObjectByObjectName("LDSysVarSchema", 0));
        return true;
    }

    //
    /*****************************************************************************************
     * Name     :checkdate
     * Function :判断数据逻辑上的正确性
     * 1:判断用户代码和用户姓名的关联是否正确
     * 2:校验用户和录入的管理机构是否匹配
     * 3:若不是最高的核赔权限的用户一定要有上级用户代码
     * 4:若是新增，判断该用户代码是否已经存在。
     *
     */
    private boolean checkdata() {
        return true;
    }

    private boolean querydata() {
        LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
        LLClaimUserSchema tLLClaimUserSchema = new LLClaimUserSchema();
        tLLClaimUserDB.setUserCode(mUserCode);
        if (!tLLClaimUserDB.getInfo()) {
            buildError("ClaimUserOperateBL", "该用户的查询信息出错！请及时对其进行维护！");
            return false;
        }
        tLLClaimUserSchema.setSchema(tLLClaimUserDB.getSchema());
        LDComDB tLDComDB = new LDComDB();
        if (!(tLLClaimUserSchema.getComCode() == null ||
              tLLClaimUserSchema.getComCode().equals(""))) {

            tLDComDB.setComCode(tLLClaimUserSchema.getComCode());
            if (!tLDComDB.getInfo()) {
                buildError("ClaimUserOperateBL", "LDCom查询出错，请及时维护!");
                return false;
            }
        }
        mResult.clear();
        mResult.addElement(tLDComDB.getSchema().getName());
        mResult.add(tLLClaimUserSchema);
        return true;
    }

    public int getCount() {
        return mCount;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData() {
        if (!prepareOutputData()) {
            return false;
        }
        if (mOperate.equals("PRINT")) {
            String tGrpDetailPrt = (String) mPrintFactor.getValueByName(
                    "GrpDetailPrt");
            String tDetailPrt = (String) mPrintFactor.getValueByName(
                    "DetailPrt");
            String tNoticePrt = (String) mPrintFactor.getValueByName(
                    "NoticePrt");
            String tGetDetailPrt = (String) mPrintFactor.getValueByName(
                    "GetDetailPrt");
            LOBatchPRTManagerSchema nLOBatchPRTManagerSchema = new
                    LOBatchPRTManagerSchema();
            nLOBatchPRTManagerSchema.setSchema(mLOBatchPRTManagerSet.get(1));
            LLBatchPrtBL tLLBatchPrtBL;
            VData tVData;

            XmlExport txmlExportAll = new XmlExport();

            txmlExportAll.createDocument("ON");
            mCount = mLOBatchPRTManagerSet.size();
            String mXmlFileName[] = new String[mCount];
            for (int i = 1; i <= mLOBatchPRTManagerSet.size(); i++) {
                LOBatchPRTManagerSchema tLOBatchPRTManagerSchema = new
                        LOBatchPRTManagerSchema();
                tLLBatchPrtBL = new LLBatchPrtBL();
                tVData = new VData();
                tLOBatchPRTManagerSchema.setSchema(mLOBatchPRTManagerSet.get(i));
                mxmlFileName = StrTool.unicodeToGBK("0" +
                        tLOBatchPRTManagerSchema.
                        getCode()) + "-" + tLOBatchPRTManagerSchema.getPrtSeq() +
                               "-" + tLOBatchPRTManagerSchema.getOtherNo();
                mXmlFileName[i - 1] = mxmlFileName;
                if (tLOBatchPRTManagerSchema.getStateFlag() == null) {
                    buildError("dealData", "无效的打印状态");
                    return false;
                } else if (!tLOBatchPRTManagerSchema.getStateFlag().equals("0")) {
                    buildError("dealData", "该打印请求不是在请求状态");
                    return false;
                }

                tVData.addElement(tLOBatchPRTManagerSchema);
                tVData.addElement(mG);

                if (!callPrintService(tLOBatchPRTManagerSchema)) {
                    Content = " 印刷号" + tLOBatchPRTManagerSchema.getPrtSeq() +
                              "：" +
                              tLLBatchPrtBL.mErrors.getFirstError().toString();
                    strLogs = strLogs + Content;
                    continue;
                }
                XmlExport txmlExport = (XmlExport) mResult.
                                       getObjectByObjectName("XmlExport", 0);
                if (txmlExport == null) {
                    Content = "印刷号" + tLOBatchPRTManagerSchema.getPrtSeq() +
                              "没有得到要显示的数据文件！";
                    strLogs = strLogs + Content;
                    continue;
                }
                Element telement = txmlExport.getDocument().getRootElement();
                if (iFlag == 0) {
                    txmlExportAll.setTemplateName(txmlExport.getDocument().
                                                  getRootElement(), telement);
                    //给付凭证的打印没有改这段程序的里面，对iFlag标记工还不清楚用处...LL
                    if (!(tGrpDetailPrt.equals("on") && tDetailPrt.equals("on") &&
                          tNoticePrt.equals("on") && tGetDetailPrt.equals("on"))) {
                        if (!(tGrpDetailPrt.equals("on") &&
                              tDetailPrt.equals("on") ||
                              tDetailPrt.equals("on") && tNoticePrt.equals("on") ||
                              tGrpDetailPrt.equals("on") &&
                              tNoticePrt.equals("on"))) {
                            iFlag = 1;
                        }
                    }
                }
                if (operFlag == true) {
                    File f = new File(mLDSysVarSchema.getSysVarValue());
                    f.mkdir();
                    System.out.println("PATH : " +
                                       mLDSysVarSchema.getSysVarValue());
                    txmlExport.outputDocumentToFile(mLDSysVarSchema.
                            getSysVarValue() + File.separator + "printdata"
                            + File.separator + "data" + File.separator +
                            "brief" + File.separator, mxmlFileName);
                }
                tLLBatchPrtBL = null;
                tVData = null;
            }
            tVData = new VData();
            tVData.add(mXmlFileName);
            mResult = tVData;
        }
        return true;
    }

    private boolean prepareOutputData() {
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String tLimit = PubFun.getNoLimit(this.mG.ManageCom);
        String BatchCode = PubFun1.CreateMaxNo("BatchCode", tLimit);
        String tBatchType = (String) mPrintFactor.getValueByName("BatchType");
        String tGrpDetailPrt = (String) mPrintFactor.getValueByName(
                "GrpDetailPrt");
        System.out.println(tGrpDetailPrt);
        String tDetailPrt = (String) mPrintFactor.getValueByName("DetailPrt");
        String tNoticePrt = (String) mPrintFactor.getValueByName("NoticePrt");
        String tGetDetailPrt = (String) mPrintFactor.getValueByName(
                "GetDetailPrt");
        LLCaseSet tLLCaseSet = new LLCaseSet();
        if (tBatchType.equals("1")) {
            String tRgtNo = (String) mPrintFactor.getValueByName("RgtNo");
            if (tRgtNo == null || tRgtNo == "") {
                CError tError = new CError();
                tError.moduleName = "LLBatchPrtBL";
                tError.functionName = "checkInputData";
                tError.errorMessage = "批次号为空出错";
                this.mErrors.addOneError(tError);
                return false;
            }
            LLCaseDB tLLCaseDB = new LLCaseDB();
//            tLLCaseDB.setRgtNo(tRgtNo);
//            tLLCaseSet = tLLCaseDB.query();
            //liuli修改于2008/06/26  解决默认的排序比较各乱的问题
            String sqlstr = " select * from LLCase where RgtNo='"+tRgtNo+"' order by caseno";
           // System.out.println("--liuli--: "+sqlstr);
            tLLCaseSet = tLLCaseDB.executeQuery(sqlstr);
            //打印团体汇总细目表，前台判断案件状态，此处不做判断，如状态不正确也能出当前已有数据的细目表
            if (tGrpDetailPrt.equals("on")) {
                String tSql =
                        "select caseno from llcase where rgtstate in ('09','11','12') "
                        + " and rgtno='" + tRgtNo + "' order by caseno";
                tSSRS = tExeSQL.execSQL(tSql);
                if (tSSRS != null && tSSRS.getMaxRow() > 0) {
                    LOBatchPRTManagerSchema tLOBatchPRTManagerSchema = new
                            LOBatchPRTManagerSchema();
                    String prtSeq = PubFun1.CreateMaxNo("prtSeq", tLimit);
                    tLOBatchPRTManagerSchema.setBatchTimescode(BatchCode);
                    tLOBatchPRTManagerSchema.setPrtSeq(prtSeq);
                    tLOBatchPRTManagerSchema.setOtherNo(tRgtNo);
                    tLOBatchPRTManagerSchema.setOtherNoType("7");
                    tLOBatchPRTManagerSchema.setPrtType("1");
                    tLOBatchPRTManagerSchema.setStateFlag("0");
                    tLOBatchPRTManagerSchema.setManageCom(mG.ManageCom);
                    System.out.println("~~~~" + tRgtNo.substring(1, 3));
                    if (tRgtNo.substring(1, 3).equals("12")) {
                        tLOBatchPRTManagerSchema.setCode("lp011");
                    } else if (tRgtNo.substring(1, 3).equals("11")) {
                        tLOBatchPRTManagerSchema.setCode("lp003bj");
                    } else {
                        tLOBatchPRTManagerSchema.setCode("lp003");
                    }
                    tLOBatchPRTManagerSchema.setReqCom(mG.ManageCom);
                    tLOBatchPRTManagerSchema.setReqOperator(mG.Operator);
                    tLOBatchPRTManagerSchema.setExeOperator(mG.Operator);
                    tLOBatchPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
                    tLOBatchPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
                    mLOBatchPRTManagerSet.add(tLOBatchPRTManagerSchema);
                }

            }

        }
        if (tBatchType.equals("2")) {
            String tScaseno = (String) mPrintFactor.getValueByName("SCaseNo");
            String tEcaseno = (String) mPrintFactor.getValueByName("ECaseNo");
            LLCaseDB tLLCaseDB = new LLCaseDB();
            String sql =
                    "select * from llcase where substr(caseno,6)>=substr('" +
                    tScaseno + "',6) and substr(caseno,6)<=substr('" + tEcaseno +
                    "',6) and rgtstate in ('09','11','12') and MngCom like '" +
                    mG.ManageCom + "%' order by CaseNo";
            System.out.println(sql);
            tLLCaseSet = tLLCaseDB.executeQuery(sql);
        }
        if (tBatchType.equals("3")) {
            String tempstr = (String) mPrintFactor.getValueByName("CaseNoBatch");
            String tCaseNoBatch = tempstr.trim();
            while (tCaseNoBatch.length() >= 17) {
                LLCaseSchema tLLCaseSchema = new LLCaseSchema();
                int Cindex = tCaseNoBatch.indexOf("C")<0?(tCaseNoBatch.indexOf("R")<0?tCaseNoBatch.indexOf("S"):tCaseNoBatch.indexOf("R")):tCaseNoBatch.indexOf("C");
                String tCaseNo = tCaseNoBatch.substring(Cindex, Cindex + 17);
                LLCaseDB tLLCaseDB = new LLCaseDB();
                tLLCaseDB.setCaseNo(tCaseNo);
                if (tLLCaseDB.getInfo()) {
                    tLLCaseSchema.setSchema(tLLCaseDB.getSchema());
                    tLLCaseSet.add(tLLCaseSchema);
                }
                tCaseNoBatch = tCaseNoBatch.substring(Cindex + 17);
            }
        }
        //原先VTS的打印目前走PDF格式,所以这里新增类型4.用来支持从客户信箱中的打印 added by huxl at 20070810
        if (tBatchType.equals("4")) {
            String tCaseNo = (String) mPrintFactor.getValueByName("CaseNoBatch");
            LLCaseDB tLLCaseDB = new LLCaseDB();
            tLLCaseDB.setCaseNo(tCaseNo);
            if (tLLCaseDB.getInfo()) {
                tLLCaseSet.add(tLLCaseDB.getSchema());
            }
        }
//获取了需要进行打印的所有LLCase信息后,下面进行处理
        int count = tLLCaseSet.size();
        for (int i = 1; i <= count; i++) {
            String trgtstate = tLLCaseSet.get(i).getRgtState();
            if (!trgtstate.equals("09") && !trgtstate.equals("10") &&
                !trgtstate.equals("11") && !trgtstate.equals("12")) {
                continue;
            } else {
                if (tNoticePrt.equals("on")) {
                    LOBatchPRTManagerSchema tLOBatchPRTManagerSchema = new
                            LOBatchPRTManagerSchema();
                    String prtSeq = PubFun1.CreateMaxNo("prtSeq", tLimit);
                    tLOBatchPRTManagerSchema.setBatchTimescode(BatchCode);
                    tLOBatchPRTManagerSchema.setPrtSeq(prtSeq);
                    tLOBatchPRTManagerSchema.setOtherNo(tLLCaseSet.get(i).
                            getCaseNo());
                    tLOBatchPRTManagerSchema.setOtherNoType("5");
                    tLOBatchPRTManagerSchema.setPrtType("1");
                    tLOBatchPRTManagerSchema.setStateFlag("0");
                    tLOBatchPRTManagerSchema.setManageCom(mG.ManageCom);
                    tLOBatchPRTManagerSchema.setCode("lp000");
                    tLOBatchPRTManagerSchema.setReqCom(mG.ManageCom);
                    tLOBatchPRTManagerSchema.setReqOperator(mG.Operator);
                    tLOBatchPRTManagerSchema.setExeOperator(mG.Operator);
                    tLOBatchPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
                    tLOBatchPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
                    mLOBatchPRTManagerSet.add(tLOBatchPRTManagerSchema);
                }
                //给付凭证的批次打印
                if (tGetDetailPrt.equals("on")) {
                    LOBatchPRTManagerSchema tLOBatchPRTManagerSchema = new
                            LOBatchPRTManagerSchema();
                    String prtSeq = PubFun1.CreateMaxNo("prtSeq", tLimit);
                    tLOBatchPRTManagerSchema.setBatchTimescode(BatchCode);
                    tLOBatchPRTManagerSchema.setPrtSeq(prtSeq);
                    tLOBatchPRTManagerSchema.setOtherNo(tLLCaseSet.get(i).
                            getCaseNo());
                    tLOBatchPRTManagerSchema.setOtherNoType("5");
                    tLOBatchPRTManagerSchema.setPrtType("1");
                    tLOBatchPRTManagerSchema.setStateFlag("0");
                    tLOBatchPRTManagerSchema.setManageCom(mG.ManageCom);
                    tLOBatchPRTManagerSchema.setCode("lp008");
                    tLOBatchPRTManagerSchema.setReqCom(mG.ManageCom);
                    tLOBatchPRTManagerSchema.setReqOperator(mG.Operator);
                    tLOBatchPRTManagerSchema.setExeOperator(mG.Operator);
                    tLOBatchPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
                    tLOBatchPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
                    String strSQL =
                            "select ActuGetNo from LJAGet where OtherNo='" +
                            tLLCaseSet.get(i).
                            getCaseNo() + "'" + "and OtherNoType='5' and (finstate<>'FC' or finstate is null)";
                    //由于理赔可能出现团体统一给付,部分给付的情况,这里对于这种情况不处理,只打印个人给付.
                    String tActuGetNo = new ExeSQL().getOneValue(strSQL);
                    if (tActuGetNo != "") {
                        ActuGetNo = tActuGetNo; //实付号码
                    } else {
                        //对于团体理赔
                        continue;
                    }
                    tLOBatchPRTManagerSchema.setStandbyFlag1(ActuGetNo);
                    tLOBatchPRTManagerSchema.setStandbyFlag2("0");
                    mLOBatchPRTManagerSet.add(tLOBatchPRTManagerSchema);
                }

                if (tDetailPrt.equals("on")) {
                    //这里只允许打印理赔已经赔付的细目表 modified by huxl @ 2008-8-22
                    String SQL =
                            "select distinct b.caserelano from llclaimdetail a ,llcaserela b where a.caseno ='" +
                            tLLCaseSet.get(i).getCaseNo() +
                            "' and a.Caseno = b.CaseNo  and a.CaseRelaNo = b.CaseRelaNo with ur";
                    tSSRS = tExeSQL.execSQL(SQL);
                    if (tSSRS != null && tSSRS.getMaxRow() > 0) {
                        for (int k = 1; k <= tSSRS.getMaxRow(); k++) {
                            LOBatchPRTManagerSchema tLOBatchPRTManagerSchema = new
                                    LOBatchPRTManagerSchema();
                            String prtSeq = PubFun1.CreateMaxNo("prtSeq",
                                    tLimit);
                            tLOBatchPRTManagerSchema.setBatchTimescode(
                                    BatchCode);
                            tLOBatchPRTManagerSchema.setPrtSeq(prtSeq);
                            tLOBatchPRTManagerSchema.setOtherNo(tLLCaseSet.get(
                                    i).getCaseNo());
                            tLOBatchPRTManagerSchema.setStandbyFlag1(
                                    tSSRS.GetText(k, 1));
                            tLOBatchPRTManagerSchema.setOtherNoType("5");
                            tLOBatchPRTManagerSchema.setPrtType("1");
                            tLOBatchPRTManagerSchema.setStateFlag("0");
                            tLOBatchPRTManagerSchema.setManageCom(mG.ManageCom);
                            tLOBatchPRTManagerSchema.setCode("lp001");
                            tLOBatchPRTManagerSchema.setReqCom(mG.ManageCom);
                            tLOBatchPRTManagerSchema.setReqOperator(mG.Operator);
                            tLOBatchPRTManagerSchema.setExeOperator(mG.Operator);
                            tLOBatchPRTManagerSchema.setMakeDate(PubFun.
                                    getCurrentDate());
                            tLOBatchPRTManagerSchema.setMakeTime(PubFun.
                                    getCurrentTime());
                            mLOBatchPRTManagerSet.add(tLOBatchPRTManagerSchema);

                        }
                    }
                }
            }
        }

        if (mLOBatchPRTManagerSet == null || mLOBatchPRTManagerSet.size() <= 0) {
            if(count == 0){
                buildError("LLBatchPrtBL", "没有符合打印条件的数据，请确认是否有结案案件！");
            }else if(tGetDetailPrt.equals("on")){
                buildError("LLBatchPrtBL", "没有符合打印条件的数据，请确认是否为团体统一给付！");
            }
            return false;
        } else {
            tmpMap.put(mLOBatchPRTManagerSet, "INSERT");
        }
        mResult.clear();
        mResult.add(tmpMap);

        return true;
    }


    public void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLBatchPrtBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args) {

        TransferData PrintElement = new TransferData();
        PrintElement.setNameAndValue("RgtNo", "P1100070225000019");
//      PrintElement.setNameAndValue("SCaseNo","C0000051107000002");
//      PrintElement.setNameAndValue("ECaseNo","C0000051107000002");
//      PrintElement.setNameAndValue("CaseNoBatch","C1100061116000095");
        PrintElement.setNameAndValue("BatchType", "1");
        PrintElement.setNameAndValue("DetailPrt", "on");
        PrintElement.setNameAndValue("GrpDetailPrt", "on");
        PrintElement.setNameAndValue("NoticePrt", "off");

        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ComCode = "86";
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "cm0001";
        LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();
        //mLDSysVarSchema.setSysVar("xmlrealpath");
        mLDSysVarSchema.setSysVarValue("D://62//ui");

        VData aVData = new VData();
        aVData.add(tGlobalInput);
        aVData.add(PrintElement);
        aVData.add(mLDSysVarSchema);
        /*
             String aUserCode = "000011";
             VData aVData = new VData();
             aVData.addElement(aUserCode);
         */
        LLBatchPrtBL tLLBatchPrtBL = new LLBatchPrtBL();
        tLLBatchPrtBL.submitData(aVData, "PRINT");
        int tCount = tLLBatchPrtBL.getCount();
        System.out.println("~~~~~~~~~~~~~~~~~~~" + tCount);
        String tFileName[] = new String[tCount];
        VData tResult = new VData();
        tResult = tLLBatchPrtBL.getResult();
        tFileName = (String[]) tResult.getObject(0);
        System.out.println("~~~~~~~~~~~~~~~~~~~2" + tFileName);

    }

    private boolean callPrintService(LOBatchPRTManagerSchema
                                     aLOBatchPRTManagerSchema) {
        // 查找打印服务
        String strSQL = "SELECT * FROM LDCode WHERE CodeType = 'print_service'";
        strSQL += " AND Code = '" + aLOBatchPRTManagerSchema.getCode() + "'";
        strSQL += " AND OtherSign = '0'";
        System.out.println(strSQL);
        LDCodeSet tLDCodeSet = new LDCodeDB().executeQuery(strSQL);

        if (tLDCodeSet.size() == 0) {
            buildError("dealData",
                       "找不到对应的打印服务类(Code = '" +
                       aLOBatchPRTManagerSchema.getCode() +
                       "')");
            return false;
        }

        // 调用打印服务
        LDCodeSchema tLDCodeSchema = tLDCodeSet.get(1);
        try {
            Class cls = Class.forName(tLDCodeSchema.getCodeAlias());
            PrintService ps = (PrintService) cls.newInstance();

            // 准备数据
            String strOperate = tLDCodeSchema.getCodeName();
            VData vData = new VData();

            vData.add(mG);
            vData.add(aLOBatchPRTManagerSchema);

            if (!ps.submitData(vData, strOperate)) {
                mErrors.copyAllErrors(ps.getErrors());
                return false;
            }
            mResult = ps.getResult();
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("callPrintService", ex.toString());
            return false;
        }

        return true;
    }
}
