/*
 * <p>Title: Web业务系统</p>
 * <p>ClassName:LLIndirectFeeBL </p>
 * <p>Description: 理赔案件-间接调查费录入 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft </p>
 * @author：Xx
 * @version：1.0
 * @CreateDate：2005-02-20
 */

package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LLReasonAffixBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private MMap map = new MMap();
    private String mOperate = "";
    //全局数据
    private GlobalInput mGlobalInput = new GlobalInput();
    ExeSQL tExeSQL = new ExeSQL();
    //账单费用明细
    private LLMAppReasonAffixSet mLLMAppReasonAffixSet = new LLMAppReasonAffixSet();
    private LLMAffixSet mLLMAffixSet = new LLMAffixSet();
    private LLMAffixSet tLLMAffixSet = new LLMAffixSet();
    public LLReasonAffixBL() {
    }

    public static void main(String[] args) {

        LLReasonAffixBL tLLReasonAffixBL = new LLReasonAffixBL();
        LLMAffixSet mLLMAffixSet = new LLMAffixSet();
        LLMAffixSet tLLMAffixSet = new LLMAffixSet();
        LLMAffixSchema tLLMAffixSchema =new  LLMAffixSchema();
        tLLMAffixSchema.setAffixTypeCode("01");
        tLLMAffixSchema.setAffixTypeName("核赔记录");
        tLLMAffixSchema.setAffixCode("1500");
        tLLMAffixSchema.setAffixName("理赔给付协议书");
        tLLMAffixSchema.setManageCome("86");
        mLLMAffixSet.add(tLLMAffixSchema);

        tLLMAffixSchema =new  LLMAffixSchema();
        tLLMAffixSchema.setAffixTypeCode("01");
        tLLMAffixSchema.setAffixTypeName("核赔记录");
        tLLMAffixSchema.setAffixCode("1600");
        tLLMAffixSchema.setAffixName("1111111");
        tLLMAffixSchema.setManageCome("86");
        mLLMAffixSet.add(tLLMAffixSchema);

        tLLMAffixSchema =new  LLMAffixSchema();
        tLLMAffixSchema.setAffixTypeCode("01");
        tLLMAffixSchema.setAffixTypeName("核赔记录");
        tLLMAffixSchema.setAffixCode("66600");
        tLLMAffixSchema.setAffixName("理赔给付协议书");
        tLLMAffixSchema.setManageCome("86");
        mLLMAffixSet.add(tLLMAffixSchema);

//        tLLMAffixSet.add(tLLMAffixSchema);

        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "cm2101";
        tGI.ManageCom = "8621";
        VData tVData = new VData();
        tVData.add(mLLMAffixSet);
        tVData.add(tLLMAffixSet);
        tVData.add(tGI);


        tLLReasonAffixBL.submitData(tVData, "DELETE");
        //用于调试
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData 输入数据
     * @param cOperate String 操作类型
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到输入数据
        if (!getInputData()) {
            return false;
        }

        //检查数据合法性
        if (!checkInputData()) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLInqFeeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有数据对象
     * @return boolean
     */
    private boolean getInputData() {
        System.out.println("getInputData()..." + mOperate);

        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLLMAffixSet = (LLMAffixSet) mInputData.getObjectByObjectName(
                "LLMAffixSet", 0);
       if(mOperate.equals("DELETE")) {
        tLLMAffixSet = (LLMAffixSet) mInputData.getObjectByObjectName(
                   "LLMAffixSet", 1);
       }
//        TransferData tTr = (TransferData) mInputData.getObjectByObjectName(
//            "TransferData",0);
//        mOperate = (String) tTr.getValueByName("FeeDate");


        return true;
    }

    /**
     * 对输入数据做必要的校验
     * @return boolean
     */
    private boolean checkInputData() {
        System.out.println("checkInputData()..." + mOperate);

       int mAffixCount = mLLMAffixSet.size();
         for(int i=1; i<=mAffixCount; i++){
             LLMAffixSet xLLMAffixSet = new LLMAffixSet();
             LLMAffixSchema xLLMAffixSchema = new LLMAffixSchema();
             LLMAffixSchema tLLMAffixSchema = mLLMAffixSet.get(i);
             LLMAffixDB tLLMAffixDB = new LLMAffixDB();
             tLLMAffixDB.setAffixTypeCode(tLLMAffixSchema.getAffixTypeCode());
             tLLMAffixDB.setManageCome("86");
             xLLMAffixSet = tLLMAffixDB.query();
             if (xLLMAffixSet.size() > 0) {
                 xLLMAffixSchema = xLLMAffixSet.get(1);
                 String tempTypeName=tLLMAffixSchema.getAffixTypeName();
//                 try {
//                     tempTypeName = new String(tempTypeName.getBytes("ISO-8859-1"), "GB2312");
//                 } catch (Exception ex) {}
                  System.out.print("看这里"+tempTypeName);
                 if (!xLLMAffixSchema.getAffixTypeName().equals(tempTypeName)) {
                     CError tError = new CError();
                     tError.moduleName = "LLReasonAffixBL";
                     tError.functionName = "checkInputData";
                     tError.errorMessage = "材料类型名称与系统中不符";
                     mErrors.addOneError(tError);
                     return false;
                 }
             }
             xLLMAffixSet = new LLMAffixSet();
             tLLMAffixDB = new LLMAffixDB();
             tLLMAffixDB.setAffixTypeCode(tLLMAffixSchema.getAffixTypeCode());
             tLLMAffixDB.setAffixCode(tLLMAffixSchema.getAffixCode());
             tLLMAffixDB.setManageCome("86");
             xLLMAffixSet = tLLMAffixDB.query();
             if (xLLMAffixSet.size()== 0) {
                 String tempName=tLLMAffixSchema.getAffixName();
                 try {
                     tempName = new String(tempName.getBytes("ISO-8859-1"), "GB2312");
                 } catch (Exception ex) {}
                 System.out.print("看这里"+tempName);
                 String sql = "select affixtypecode from LLMAffix where  AffixName='" +
                              tempName + "'";
                 ExeSQL exesql = new ExeSQL();
                 String res = exesql.getOneValue(sql);
                if (res != null && res.length() > 0) {
                    CError tError = new CError();
                    tError.moduleName = "LLReasonAffixBL";
                    tError.functionName = "checkInputData";
                    tError.errorMessage = "系统中已有该材料名称！";
                    mErrors.addOneError(tError);
                    return false;

                }
             }
         }

        if (mOperate.equals("DELETE")) {
            int AffixCount = tLLMAffixSet.size();
            for (int i = 1; i <= AffixCount; i++) {
                LLMAffixSchema tLLMAffixSchema = tLLMAffixSet.get(i);
                String sql = "select affixno from LLAffix where  Affixcode='" +
                             tLLMAffixSchema.getAffixCode() + "'";
                ExeSQL exesql = new ExeSQL();
                String res = exesql.getOneValue(sql);
                if (res != null && res.length() > 0) {
                    CError tError = new CError();
                    tError.moduleName = "LLReasonAffixBL";
                    tError.functionName = "checkInputData";
                    tError.errorMessage = "材料已在实际业务中被使用,不能删除！";
                    mErrors.addOneError(tError);
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @param cOperate String 操作类型
     * @return boolean
     */
    private boolean dealData() {
        System.out.println("dealData()..." + mOperate);

        //保存录入
        if (mOperate.equals("INSERT")) {
            int AffixCount = mLLMAffixSet.size();
            LLMAffixSet xLLMAffixSet = new LLMAffixSet();
            for (int i = 1; i <= AffixCount; i++) {
                LLMAffixSchema tLLMAffixSchema = mLLMAffixSet.get(i);
                LLMAffixDB tLLMAffixDB = new LLMAffixDB();
                tLLMAffixDB.setAffixCode(tLLMAffixSchema.getAffixCode());
                tLLMAffixDB.setAffixTypeCode(tLLMAffixSchema.getAffixTypeCode());
                tLLMAffixDB.setManageCome("86");
                if (!tLLMAffixDB.getInfo()) {
                    xLLMAffixSet.add(tLLMAffixSchema);
                }
            }
            map.put(xLLMAffixSet, "INSERT");
        }
        if (mOperate.equals("DELETE")) {

            map.put(tLLMAffixSet, "DELETE");
}


        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        System.out.println("prepareOutputData()...");

        try {
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ICaseCureBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 得到处理后的结果数据
     * @return boolean
     */
    public VData getResult() {
        return this.mResult;
    }


}
