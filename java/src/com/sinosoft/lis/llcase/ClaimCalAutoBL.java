package com.sinosoft.lis.llcase;

import java.text.*;
import java.util.*;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bq.CommonBL;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单理赔给付计算业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HE
 * @version 1.0
 */
public class ClaimCalAutoBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    private MMap tmpMap = new MMap();

    private GlobalInput mGlobalInput = new GlobalInput();

//描述信息
//    private LMDutyGetClmSet mLMDutyGetClmSet = new LMDutyGetClmSet();

//保单信息
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    private LCDutySchema mLCDutySchema = new LCDutySchema();
    private LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet();
    private LCInsureAccTraceSchema mLCInsureAccTraceSchema = new
            LCInsureAccTraceSchema();

//案件信息
    private LLSecurityReceiptSchema mLLSecurityReceiptSchema = new
            LLSecurityReceiptSchema();

//赔案信息
    private LLClaimDetailSet mSaveLLClaimDetailSet = new LLClaimDetailSet();
    private LLClaimPolicySchema mLLClaimPolicySchema = null;
    private LLClaimSchema mLLClaimSchema = null;
    private LLClaimPolicySet mLLClaimPolicySet = new LLClaimPolicySet();

    private String mClmNo = "";
    private String mCaseNo = "";
    private String mRgtNo = "";
    private String mCaseRelaNo = "";
    private String mMngCom = "";
    private String mAccDate = null;
    private String mEndDate = null;
    private String mGetDutyKind;
    private String mGetDutyCode;
    private boolean havetab = false;
    private double t_standmoney = 0.0;

    public ClaimCalAutoBL() {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData,cOperate)) {
            return false;
        }

        //查询分案信息
        if (!this.getLLCaseInfo()) {
            return false;
        }

        //查询赔案信息
        if (!getLLClaimInfo()) {
            return false;
        }

        //理赔计算
        if (!calpay()) {
            CError.buildErr(this, "理赔计算错误!");
            return false;
        }
        //帐户险处理
        getInsuAcc();

        tmpMap.put(this.mSaveLLClaimDetailSet, "INSERT");

        mResult.clear();
        mResult.add(tmpMap);

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(mResult, null)) {
            CError.buildErr(this, "数据库保存失败");
            return false;
        }
        return true;
    }

    /**
     * 取参数信息
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData,String cOperate) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LLCaseSchema aLLCaseSchema = (LLCaseSchema) cInputData.
                                     getObjectByObjectName("LLCaseSchema", 0);
        mCaseNo = aLLCaseSchema.getCaseNo();
        if (mCaseNo == null||mCaseNo.equals("")) {
            CError.buildErr(this, "案件号为空,无法理算!");
            return false;
        }
//        if(cOperate.equals("cal")){
//            mLLClaimPolicySet = (LLClaimPolicySet) cInputData.getObjectByObjectName(
//                    "LLClaimPolicySet", 0);
//
//            //去掉重复的
//            if (mLLClaimPolicySet == null || mLLClaimPolicySet.size() < 1) {
//                CError.buildErr(this, "请选出需要理算的保单");
//                return false;
//            }
//
//            for (int i = 2; i <= mLLClaimPolicySet.size(); i++) {
//                String tPolNo = mLLClaimPolicySet.get(i).getPolNo();
//                String tGetDutyKind = mLLClaimPolicySet.get(i).getGetDutyKind();
//                for (int j = 1; j < i; j++) {
//                    if (tPolNo.equals(mLLClaimPolicySet.get(j).getPolNo())
//                        && tGetDutyKind.equals(
//                                mLLClaimPolicySet.get(j).getGetDutyKind())) {
//                        mLLClaimPolicySet.removeRange(i, i);
//                        i = i - 1;
//                        break;
//                    }
//                }
//            }
//        }
        return true;
    }

    //从分案保单明细表中取得赔案信息
    private boolean getLLCaseInfo() {
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mCaseNo);
        if (!tLLCaseDB.getInfo()) {
            CError.buildErr(this, "案件信息查询失败");
            return false;
        }
        LLCaseSchema aLLCaseSchema = new LLCaseSchema();
        mRgtNo = tLLCaseDB.getRgtNo();
        mMngCom = tLLCaseDB.getMngCom();
        aLLCaseSchema.setSchema(tLLCaseDB.getSchema());
        if (!"03".equals(aLLCaseSchema.getRgtState())
            && !"04".equals(aLLCaseSchema.getRgtState())
            && !"02".equals(aLLCaseSchema.getRgtState())
            && !"05".equals(aLLCaseSchema.getRgtState())
            && !"06".equals(aLLCaseSchema.getRgtState())) {
            CError.buildErr(this, "该案件状态不能理算");
            return false;
        }
        //更新案件信息
        aLLCaseSchema.setHandleDate(PubFun.getCurrentDate());
        aLLCaseSchema.setRgtState("03");
        aLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
        aLLCaseSchema.setModifyTime(PubFun.getCurrentTime());
        tmpMap.put(aLLCaseSchema, "UPDATE");
        //加多事件理算
        LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
        tLLCaseRelaDB.setCaseNo(this.mCaseNo);
        LLCaseRelaSet tLLCaseRelaSet = new LLCaseRelaSet();
        tLLCaseRelaSet = tLLCaseRelaDB.query();
        if (tLLCaseRelaSet == null) {
            CError.buildErr(this, "该案件没有关联出险事件，不能理算");
            return false;
        }
        return true;
    }

    /**
     * 查询赔案信息
     * @return boolean
     */
    private boolean getLLClaimInfo() {
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setCaseNo(this.mCaseNo);
        LLClaimSet tLLClaimSet = tLLClaimDB.query();
        if (tLLClaimDB.mErrors.needDealError()) {
            mErrors.copyAllErrors(tLLClaimDB.mErrors);
            return false;
        }
        if (tLLClaimSet != null && tLLClaimSet.size() > 0) {
            mLLClaimSchema = tLLClaimSet.get(1);
            mLLClaimSchema.setGiveType("");
            mLLClaimSchema.setGiveTypeDesc("");
            mClmNo = mLLClaimSchema.getClmNo();
        } else {
            String limit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
            this.mClmNo = PubFun1.CreateMaxNo("CLMNO", limit);
            //创建赔案信息
            createLLClaim();
        }
        //先删除已经计算过的
        String strSQL1 = "delete from LLClaimPolicy where caseno='" + mCaseNo +
                         "'";
        String strSQL2 = "delete from LLClaimDetail where caseno='" + mCaseNo +
                         "'";

        tmpMap.put(strSQL1, "DELETE");
        tmpMap.put(strSQL2, "DELETE");
        return true;
    }

    /**
     * 理赔计算
     * @return boolean
     */
    private boolean calpay() {
        //AppleWood 根据待理算责任计算
        String sql =
                "select polno,caserelano,getdutykind,count(*) from lltoclaimduty where caseno='"
                + mCaseNo + "' group by polno, caserelano,getdutykind";
        //逐张保单计算
        ExeSQL es = new ExeSQL();
        SSRS ssrs = es.execSQL(sql);
        if (ssrs.getMaxRow() <= 0) {
            CError.buildErr(this, "没有过滤出符合理赔条件的责任");
            return false;
        }
        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
            String polNo = ssrs.GetText(i, 1);
            mCaseRelaNo = ssrs.GetText(i, 2);
            String getdutykind = ssrs.GetText(i, 3);
            calDutyPay(polNo, getdutykind);
        }
        return true;
    }

    /**
     * 按照保单,受理事故,getDutyKind 计算赔付,及明细 AppleWood
     * //按责任给付计算赔付明细
     *
     */
    private boolean calDutyPay(String polNo, String getDutyKind) {
        System.out.println("开始计算每个给付责任。。。");
        double t_sum_gf_je = 0;
        double t_real_gf_je = 0;
        double t_gf_je_base = 0;

        double outAmnt = 0;
        LLSubReportSchema aLLSubReportSchema = new LLSubReportSchema();
        aLLSubReportSchema = getSubReportSchema(mCaseRelaNo);
        mAccDate = aLLSubReportSchema.getAccDate();
        //初始化
        mLLClaimPolicySchema = new LLClaimPolicySchema();
        mLLClaimPolicySchema.setCaseNo(mCaseNo);
        mLLClaimPolicySchema.setCaseRelaNo(mCaseRelaNo);
        mLLClaimPolicySchema.setPolNo(polNo);
        mLLClaimPolicySchema.setGetDutyKind(getDutyKind);
        mLLClaimPolicySchema.setRgtNo(mRgtNo);

        //取得保单信息
        if (!getLCPolInfo()) {
            return false;
        }

        //查询或创建赔案明细信息
        if (!getLLClaimPolicyInfo()) {
            return false;
        }

        //按LLToClaimDuty(过滤出的责任理算)
        LLToClaimDutyDB llToClaimDutyDB = new LLToClaimDutyDB();
        llToClaimDutyDB.setCaseNo(mCaseNo);
        llToClaimDutyDB.setPolNo(polNo);
        llToClaimDutyDB.setCaseRelaNo(mCaseRelaNo);
        llToClaimDutyDB.setGetDutyKind(getDutyKind);
        LLToClaimDutySet llToClaimDutySet = llToClaimDutyDB.query();
        for (int i = 1; i <= llToClaimDutySet.size(); i++) {
            double mSum_gf_je = 0;
            double mReal_gf_je = 0;
            t_standmoney = 0;
            LLToClaimDutySchema llToClaimDuty = llToClaimDutySet.get(i);
            String aDutyCode = llToClaimDuty.getDutyCode();
            this.mGetDutyKind = llToClaimDuty.getGetDutyKind();
            this.mGetDutyCode = llToClaimDuty.getGetDutyCode();

            //判断给付金代码
            t_gf_je_base = 0;
            double t_daylimitfee = 0.0;

            //创建赔付明细记录
            LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();

            //取出责任给付赔付计算代码
            LMDutyGetClmSchema dutyGetClm = getDutyGetClm(this.mGetDutyCode,
                    this.mGetDutyKind);
            if(dutyGetClm==null)
                return false;

            //添加续保程序
            SynLCLBDutyBL tSynLCLBDutyBL = new SynLCLBDutyBL();
            tSynLCLBDutyBL.setDutyCode(aDutyCode);
            tSynLCLBDutyBL.setPolNo(polNo);
            if (!tSynLCLBDutyBL.getInfo()) {
                CError.buildErr(this, "LCDuty表查询失败");
                continue;
            }
            mLCDutySchema.setSchema(tSynLCLBDutyBL.getSchema());

            String t_CalCode = dutyGetClm.getCalCode();
            //算出起付线
            double t_getlimit = getGetLimit(dutyGetClm.getYearGetLimitFlag());

            if (mGetDutyCode.equals("000001")) {
                //退保费
                //查询保单的保单责任表
                t_gf_je_base = mLCPolSchema.getSumPrem();
                mSum_gf_je = t_gf_je_base;
                if (!t_CalCode.equals("")) {
                    mReal_gf_je = executepay(t_gf_je_base, t_CalCode,
                                             t_getlimit);
                }
            } else if (mGetDutyCode.equals("000002")) {
                //加入退保金计算公式
            } else {
                //正常计算方式
                if (dutyGetClm.getInpFlag() == null) {
                    dutyGetClm.setInpFlag("0");
                }
                LCGetSet tLCGetSet = new LCGetSet();
                String tv_where = " where PolNo='" + polNo
                                  + "' and GetDutyCode='" + mGetDutyCode
                                  + "' and DutyCode='" + aDutyCode
                                  + "' and  GetStartDate <='" + mAccDate + "'"
                                  + " and ((GetEndDate >='" + mAccDate
                                  + "' and GetEndDate is not null) "
                                  + "or GetEndDate is null)";
                String tv_sql = "select * from LCGet " + tv_where.trim();
                String tv_sqlb = "select * from LBGet " + tv_where.trim();
                System.out.println(tv_sql);
                System.out.println(tv_sqlb);
                SynLCLBGetBL aSynLCLBGetBL = new SynLCLBGetBL();
                tLCGetSet = aSynLCLBGetBL.executeQuery(tv_sql, tv_sqlb);
                if (tLCGetSet.size() <= 0) {
                    CError.buildErr(this, "客户在出险时间没有生效的保单责任！");
                    return false;
                }
                LMDutyGetAddFactorDB tLMDutyGetAddFactorDB = new
                        LMDutyGetAddFactorDB();
                tLMDutyGetAddFactorDB.setLimitType("01");
                tLMDutyGetAddFactorDB.setGetDutyCode(this.mGetDutyCode);
                int multd = (int) mLCDutySchema.getMult();
                String mult = "" + multd;
                tLMDutyGetAddFactorDB.setMult(mult);
                if (tLMDutyGetAddFactorDB.getInfo()) {
                    t_daylimitfee += tLMDutyGetAddFactorDB.
                            getLimitValue();
                }
                t_standmoney += tLCGetSet.get(1).getStandMoney();
                String tempRealHosDays = "0";
                if (!getRealHospitalDays().equals(""))
                    tempRealHosDays = getRealHospitalDays();
                int realhosdays = Integer.parseInt(tempRealHosDays);
                double t_standmoney2 = t_daylimitfee * realhosdays;
                if (t_standmoney2 < t_standmoney && t_standmoney2 > 0.1)
                    t_standmoney = t_standmoney2;
                System.out.println("=== 花费限额：===" + t_standmoney);

                if (dutyGetClm.getInpFlag().equals("2")) {
                    //给付金由出险时发生的金额确定(帐单费用总和)，如医疗类给付
                    //   t_gf_je_base = getInpVal();

                    //new add 账单金额
                    //账单金额处理
                    double[] receiptfee = getAvaliFee();

                    tLLClaimDetailSchema.setTabFeeMoney(receiptfee[0]);
                    tLLClaimDetailSchema.setSelfGiveAmnt(receiptfee[1]);
                    tLLClaimDetailSchema.setPreGiveAmnt(receiptfee[2]);
                    tLLClaimDetailSchema.setRefuseAmnt(receiptfee[3]);
                    //拒付金额=自费+不合理
                    if (((String) mLLClaimPolicySchema.getRiskCode()).equals(
                            "1203")) {
                        tLLClaimDetailSchema.setDeclineAmnt(
                                PubFun.setPrecision(tLLClaimDetailSchema.
                                getRefuseAmnt(), "0.00"));
                    } else {
                        tLLClaimDetailSchema.setDeclineAmnt(
                                PubFun.setPrecision(tLLClaimDetailSchema.
                                getSelfGiveAmnt()
                                + tLLClaimDetailSchema.getRefuseAmnt() +
                                getDayOverFee(), "0.00"));
                        outAmnt = getDayOverFee();
                    }
                    //理算金额=账单金额-拒付金额- 先期给付
                    t_gf_je_base = PubFun.setPrecision(
                            tLLClaimDetailSchema.getTabFeeMoney()
                            - tLLClaimDetailSchema.getDeclineAmnt()
                            - tLLClaimDetailSchema.getPreGiveAmnt(), "0.00");
                    tLLClaimDetailSchema.setClaimMoney(t_gf_je_base);
                    if(t_gf_je_base>t_standmoney){
                        t_gf_je_base = t_standmoney;
                    }
                } else { //承保时确定给付金金额
                    //添加续保的程序
                    t_gf_je_base += t_standmoney;
                    if (!getSecuReceipt())
                        System.out.println("社保账单填充失败");
                }

                System.out.println("t_gf_je_base===" + t_gf_je_base);
                String extrAmntFlag = "" + dutyGetClm.getExtraAmntFlag();
                //额外保险金
                if (extrAmntFlag.equals("Y"))
                    t_gf_je_base = getAdditionalPay(polNo);
                if (dutyGetClm.getNeedReCompute() != null &&
                    dutyGetClm.getNeedReCompute().equals("N")) {
                    mSum_gf_je = t_gf_je_base;
                } else {
                    if (t_CalCode != null && !t_CalCode.equals("")) {
                        mSum_gf_je = executepay(t_gf_je_base, t_CalCode,
                                                t_getlimit);
                        mReal_gf_je = mSum_gf_je;
                    }
                }
            }
            mSum_gf_je = (mSum_gf_je<0)?0:mSum_gf_je;
            mReal_gf_je = (mReal_gf_je<0)?0:mReal_gf_je;

//            if (mReal_gf_je <= 0 && yeargetlimitflag.equals("1"))
//                t_getlimit = mSum_gf_je;

            System.out.println("=== 比例前的给付金：===" + mReal_gf_je);
            //确定该保险金的赔付比例
            double tPayRate = 0;
            tPayRate = getCaseRate(dutyGetClm);
            mReal_gf_je = mReal_gf_je * tPayRate;
            System.out.println("=== 比例后的给付金：===" + mReal_gf_je);

            LMRiskDB tLMRiskDB = new LMRiskDB();
            tLMRiskDB.setRiskCode(mLCPolSchema.getRiskCode());
            String insuaccflag = "";
            if (tLMRiskDB.getInfo())
                insuaccflag += tLMRiskDB.getInsuAccFlag();
            if (insuaccflag.equals("Y")) {
                //帐户险保额控制
                havetab = false;
                tLLClaimDetailSchema.setTabFeeMoney(Arith.round(getClaimMoney(
                        t_CalCode), 2));
                tLLClaimDetailSchema.setClaimMoney(Arith.round(getClaimMoney(
                        t_CalCode), 2));
                double InsuAccBala = getInsuAccBala(mReal_gf_je);
                if (mReal_gf_je > InsuAccBala) {
                    double exp_getmoney = mReal_gf_je - InsuAccBala;
                    double act_getmoney = GrpAccToInsuAcc(exp_getmoney);
                    mReal_gf_je = act_getmoney + InsuAccBala;
                    double declinemoney = mSum_gf_je - mReal_gf_je;
                    tLLClaimDetailSchema.setDeclineAmnt(declinemoney);
                }
            } else {
                if (!dutyGetClm.getInpFlag().equals("3")
                    &&mCaseNo.substring(0,1).equals("C")) {
                    //需要保额控制的给付责任
                    double aAmnt = mLCDutySchema.getAmnt();
                    if (aAmnt > 0.01 && mReal_gf_je > aAmnt) {
                        outAmnt += Arith.round(mReal_gf_je - aAmnt,2);
                        mReal_gf_je = aAmnt;
                    }
                    //每种责任的给付金不能超过保单的保额
                    double temGet = getacupay(mGetDutyCode,mCaseRelaNo);
                    if (mReal_gf_je > temGet) {
                        outAmnt = Arith.round(mReal_gf_je - temGet, 2);
                        mReal_gf_je = temGet;
                    }

                    double declinemoney = mSum_gf_je - mReal_gf_je;
                    tLLClaimDetailSchema.setDeclineAmnt(declinemoney);
                    double aclmmoney = getClaimMoney(t_CalCode);
                    if (aclmmoney > 0)
                        tLLClaimDetailSchema.setTabFeeMoney(aclmmoney);
                    tLLClaimDetailSchema.setClaimMoney(aclmmoney);
                } else {
                    havetab = false;
                    tLLClaimDetailSchema.setTabFeeMoney(Arith.round(
                            getClaimMoney(t_CalCode), 2));
                    tLLClaimDetailSchema.setClaimMoney(tLLClaimDetailSchema.
                            getTabFeeMoney());
                    if (mLCDutySchema.getAmnt() > 0.01)
                        outAmnt = tLLClaimDetailSchema.getClaimMoney() -
                                  mLCDutySchema.getAmnt();
                    if (outAmnt < 0)
                        outAmnt = 0;
                    double declinemoney = mSum_gf_je - mReal_gf_je;
                    declinemoney = Arith.round(declinemoney, 2);
                    tLLClaimDetailSchema.setDeclineAmnt(declinemoney);
                }
            }
            //取分
            mSum_gf_je = Arith.round(mSum_gf_je, 2);
            mReal_gf_je = Arith.round(mReal_gf_je, 2);
            t_sum_gf_je = t_sum_gf_je + mSum_gf_je;
            t_real_gf_je = t_real_gf_je + mReal_gf_je;
            System.out.println("实赔金额：" + mReal_gf_je);
            System.out.println("总实赔金额：" + t_real_gf_je);

            //险种总给付
            if (!dutyGetClm.getInpFlag().equals("3") && !insuaccflag.equals("Y")) {
                if (t_real_gf_je > mLCPolSchema.getAmnt()) {
                    t_real_gf_je = mLCPolSchema.getAmnt();
                }
            }
            //免赔额

            String yeargetlimitflag = "" + dutyGetClm.getYearGetLimitFlag();
            if (mReal_gf_je <= 0 && yeargetlimitflag.equals("1") &&
                mLCDutySchema.getGetRate() > 0)
                t_getlimit = tLLClaimDetailSchema.getClaimMoney();
            System.out.println("+++++免赔额++++++" + t_getlimit);
            //理算金额
            if (tLLClaimDetailSchema.getClaimMoney() < 0.001)
                tLLClaimDetailSchema.setClaimMoney(mSum_gf_je);

            tLLClaimDetailSchema.setGetDutyCode(llToClaimDuty.getGetDutyCode());
            tLLClaimDetailSchema.setClmNo(this.mClmNo);
            tLLClaimDetailSchema.setRgtNo(mRgtNo);
            tLLClaimDetailSchema.setGetDutyKind(llToClaimDuty.getGetDutyKind());
            tLLClaimDetailSchema.setCaseNo(mLLClaimPolicySchema.getCaseNo());
            tLLClaimDetailSchema.setStatType(dutyGetClm.getStatType());
            tLLClaimDetailSchema.setContNo(mLCPolSchema.getContNo());
            tLLClaimDetailSchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
            tLLClaimDetailSchema.setGrpContNo(mLCPolSchema.getGrpContNo());
            tLLClaimDetailSchema.setPolNo(mLCPolSchema.getPolNo());
            tLLClaimDetailSchema.setKindCode(mLCPolSchema.getKindCode());
            tLLClaimDetailSchema.setRiskCode(mLCPolSchema.getRiskCode());
            tLLClaimDetailSchema.setRiskVer(mLCPolSchema.getRiskVersion());
            tLLClaimDetailSchema.setPolMngCom(mLCPolSchema.getManageCom());
            tLLClaimDetailSchema.setSaleChnl(mLCPolSchema.getSaleChnl());
            tLLClaimDetailSchema.setOutDutyAmnt(t_getlimit);
            tLLClaimDetailSchema.setOutDutyRate(mLCDutySchema.getGetRate());
            tLLClaimDetailSchema.setAgentCode(mLCPolSchema.getAgentCode());
            tLLClaimDetailSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
            tLLClaimDetailSchema.setStandPay(Arith.round(mSum_gf_je, 2));
            tLLClaimDetailSchema.setOverAmnt(outAmnt);
            tLLClaimDetailSchema.setRealPay(mReal_gf_je);
            tLLClaimDetailSchema.setDutyCode(mLCDutySchema.getDutyCode());
            tLLClaimDetailSchema.setMakeDate(PubFun.getCurrentDate());
            tLLClaimDetailSchema.setModifyDate(PubFun.getCurrentDate());
            tLLClaimDetailSchema.setMakeTime(PubFun.getCurrentTime());
            tLLClaimDetailSchema.setModifyTime(PubFun.getCurrentTime());
            tLLClaimDetailSchema.setOperator(this.mGlobalInput.Operator);
            tLLClaimDetailSchema.setMngCom(this.mGlobalInput.ManageCom);
            tLLClaimDetailSchema.setCaseRelaNo(mCaseRelaNo);
            //账单金额
            double tabfee = this.getTabFee();
            System.out.println(tabfee);
            if (tLLClaimDetailSchema.getTabFeeMoney() < 0.001 && !havetab) {
                tLLClaimDetailSchema.setTabFeeMoney(tabfee);
                if (tLLClaimDetailSchema.getOutDutyRate() > 0.0001) {
                    double tcalmny = tLLClaimDetailSchema.getRealPay() /
                                     tLLClaimDetailSchema.getOutDutyRate() +
                                     tLLClaimDetailSchema.getOutDutyAmnt();
                    tcalmny = Arith.round(Arith.round(tcalmny, 4), 2);
                    tLLClaimDetailSchema.setClaimMoney(tcalmny);
                }
            }
            double[] receiptfee = getAvaliFee();

            tLLClaimDetailSchema.setSelfGiveAmnt(receiptfee[1]);
            tLLClaimDetailSchema.setPreGiveAmnt(receiptfee[2]);
            tLLClaimDetailSchema.setRefuseAmnt(receiptfee[3]);

            //tLLClaimDetailSet.add(tLLClaimDetailSchema);
            mSaveLLClaimDetailSet.add(tLLClaimDetailSchema);
        } //for 结束

        //创建赔案明细记录
        mLLClaimPolicySchema.setClmNo(this.mClmNo);
        mLLClaimPolicySchema.setGrpContNo(mLCPolSchema.getGrpContNo());
        t_sum_gf_je = Arith.round(t_sum_gf_je, 2);
        t_real_gf_je = Arith.round(t_real_gf_je, 2);
        mLLClaimPolicySchema.setStandPay(t_sum_gf_je);
        mLLClaimPolicySchema.setRealPay(t_real_gf_je);
        mLLClaimPolicySchema.setModifyDate(PubFun.getCurrentDate());
        mLLClaimPolicySchema.setModifyTime(PubFun.getCurrentTime());
        mLLClaimPolicySchema.setGiveType("");
        mLLClaimPolicySchema.setGiveTypeDesc("");

        tmpMap.put(mLLClaimPolicySchema, "INSERT");
        return true;
    }

    /**
     * 查询保单信息
     * @return boolean
     */
    private boolean getLCPolInfo() {
        //续保新增代码,LCPolBL同时查C表和B表
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setPolNo(mLLClaimPolicySchema.getPolNo());
        if (!tLCPolBL.getInfo()) {
            CError.buildErr(this, "保单查询错误");
            return false;
        }

        //保全提供的保单失效日期
        mEndDate = CommonBL.getPolInvalidate(mLLClaimPolicySchema.getPolNo());
        if (mEndDate.equals(null)||mEndDate.equals("")||mEndDate.equals("null")){
            CError.buildErr(this, mLLClaimPolicySchema.getPolNo()+"险种保单失效日查询失败!");
            return false;
        }

        int validays = PubFun.calInterval(tLCPolBL.getCValiDate(),
                                          mAccDate, "D");
        int remdays = PubFun.calInterval(mAccDate,
                                         mEndDate, "D");
        if (validays < 0 || remdays < 0) {
            CError.buildErr(this, "保单" + tLCPolBL.getPolNo() + "的生效日期："
                            + tLCPolBL.getCValiDate() + ",失效日期："
                            + tLCPolBL.getEndDate() + "；保单在出险日期："
                            + mAccDate + " 无效"
                    );
            return false;
        }
        mLCPolSchema.setSchema(tLCPolBL.getSchema());
        mLLClaimPolicySchema.setRiskCode(mLCPolSchema.getRiskCode());
        return true;
    }

    /**
     * 查询或创建赔案明细
     * @param tLLClaimPolicySchema LLClaimPolicySchema
     * @return boolean
     */
    private boolean getLLClaimPolicyInfo() {
        LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
        tLLClaimPolicyDB.setClmNo(mClmNo);
        tLLClaimPolicyDB.setPolNo(mLLClaimPolicySchema.getPolNo());
        tLLClaimPolicyDB.setGetDutyKind(mLLClaimPolicySchema.getGetDutyKind());
        tLLClaimPolicyDB.setCaseRelaNo(mCaseRelaNo);
        if (tLLClaimPolicyDB.getInfo()) {
            //赔案明细存在，说明以前理算过
            mLLClaimPolicySchema = tLLClaimPolicyDB.getSchema();
        } else {
            //目前没有了CasePolicy

            //从分案保单明细表中取得赔案信息
            if (!getPolicyInfo()) {
                return false;
            }
            mLLClaimPolicySchema.setMakeDate(PubFun.getCurrentDate());
            mLLClaimPolicySchema.setMakeTime(PubFun.getCurrentTime());
            mLLClaimPolicySchema.setOperator(this.mGlobalInput.Operator);
            mLLClaimPolicySchema.setMngCom(this.mGlobalInput.ManageCom);
        }
        return true;
    }

    private boolean getPolicyInfo() {
        LLCasePolicyDB tLLCasePolicyDB = new LLCasePolicyDB();
        tLLCasePolicyDB.setCaseNo(mLLClaimPolicySchema.getCaseNo());
        tLLCasePolicyDB.setPolNo(mLLClaimPolicySchema.getPolNo());
        if (!tLLCasePolicyDB.getInfo()) {
            CError.buildErr(this, "案件关联保单查询失败");
            return false;
        }
        Reflections rf = new Reflections();
        rf.transFields(mLLClaimPolicySchema, tLLCasePolicyDB.getSchema());
        return true;
    }

    /**
     * getSubReportSchema
     *
     * @param caseRelaNo String
     * @return LLSubReportSchema
     */
    private LLSubReportSchema getSubReportSchema(String caseRelaNo) {
        String sql = "select * from llsubreport where subrptno = (select subrptno from llcaserela where caseRelaNo='" +
                     caseRelaNo + "')";
        LLSubReportDB db = new LLSubReportDB();
        return db.executeQuery(sql).get(1);

    }

    /**
     * 查询LMDutyGlm
     *
     * @param getDutyCode String
     * @param getDutyKind String
     * @return String
     */
    private LMDutyGetClmSchema getDutyGetClm(String getDutyCode,
                                             String getDutyKind) {
        LMDutyGetClmDB db = new LMDutyGetClmDB();
        db.setGetDutyCode(getDutyCode);
        db.setGetDutyKind(getDutyKind);
        if (!db.getInfo()) {
            mErrors.addOneError("查询LMDutyGetClm错误");
            System.out.println("查询LMDutyGetClm错误");
            return null;
        }

        return db.getSchema();
    }

    /**
     * 创建赔案记录
     * @return boolean
     */
    private boolean createLLClaim() {
        //判断是否产生赔案信息
        if (this.mLLClaimSchema == null) {
            this.mLLClaimSchema = new LLClaimSchema();
            this.mLLClaimSchema.setClmNo(this.mClmNo);

            String tGetDutyKind = "000000";
            mLLClaimSchema.setGetDutyKind(tGetDutyKind);
            mLLClaimSchema.setCaseNo(mCaseNo);
            mLLClaimSchema.setClmState("1"); //结算
            mLLClaimSchema.setRgtNo(mRgtNo);
            mLLClaimSchema.setClmUWer(mGlobalInput.Operator);
            mLLClaimSchema.setCheckType("0");
            mLLClaimSchema.setMngCom(mMngCom);
            mLLClaimSchema.setOperator(mGlobalInput.Operator);
            mLLClaimSchema.setMakeDate(PubFun.getCurrentDate());
            mLLClaimSchema.setMakeTime(PubFun.getCurrentTime());
            mLLClaimSchema.setModifyDate(PubFun.getCurrentDate());
            mLLClaimSchema.setModifyTime(PubFun.getCurrentTime());

            String tSQL="DELETE FROM llclaim WHERE caseno='"+mCaseNo+"'";
            tmpMap.put(tSQL,"DELETE");
            tmpMap.put(mLLClaimSchema, "INSERT");
        }
        return true;
    }


    /**
     * 理赔计算
     * @param t_gf_je_base double
     * @param t_CalCode String
     * @return double
     */
    private double executepay(double t_gf_je_base, String t_CalCode,
                              double t_GetLimit) {
        double rValue;
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(t_CalCode);

        //增加基本要素,计算给付金
        TransferData tTransferData = new TransferData();

        tTransferData.setNameAndValue("Je_gf", String.valueOf(t_gf_je_base));
        double aOwnFee = getOwnFee();
        tTransferData.setNameAndValue(
                "PubFee", String.valueOf(t_gf_je_base - aOwnFee));

        //社保外费用
        tTransferData.setNameAndValue("OwnFee", String.valueOf(aOwnFee));

        //险种保单号
        tTransferData.setNameAndValue(
                "PolNo", String.valueOf(mLCPolSchema.getPolNo()));

        //份数
        tTransferData.setNameAndValue(
                "Mult", String.valueOf(mLCDutySchema.getMult()));
        //总保费
        tTransferData.setNameAndValue(
                "Prem", String.valueOf(mLCPolSchema.getPrem()));
        //总保额
        tTransferData.setNameAndValue(
                "Amnt", String.valueOf(mLCPolSchema.getAmnt()));
        //限额
        tTransferData.setNameAndValue("StandMoney",String.valueOf(t_standmoney));
        //交费间隔
        tTransferData.setNameAndValue(
                "PayIntv", String.valueOf(mLCPolSchema.getPayIntv()));
        //被保人投保年龄
        tTransferData.setNameAndValue(
                "GetIntv", String.valueOf(mLCPolSchema.getInsuredAppAge()));
        //已交费年期
        tTransferData.setNameAndValue(
                "AppAge", String.valueOf(PubFun.calInterval(
                        mLCPolSchema.getCValiDate(), mLCPolSchema.getPaytoDate(),
                        "Y")));
        //交费年期
        tTransferData.setNameAndValue(
                "PayYears", String.valueOf(mLCDutySchema.getPayYears()));
        //出险时已保年期
        // tTransferData.setNameAndValue("RgtYears",String.valueOf(PubFun.calInterval(mLCPolSchema.getCValiDate(),mLLRegisterSchema.getAccidentDate(),"Y")) );
        tTransferData.setNameAndValue("RgtYears", String.valueOf(
                PubFun.calInterval(mLCPolSchema.getCValiDate(),mAccDate, "Y")));
        //出险时已保天数
        tTransferData.setNameAndValue("RgtDays", String.valueOf(
                PubFun.calInterval(mLCPolSchema.getCValiDate(),mAccDate, "D")));
        //被保人性别
        tTransferData.setNameAndValue(
                "Sex", String.valueOf(mLCPolSchema.getInsuredSex()));
        //住院天数
        tTransferData.setNameAndValue("DaysInHos", getRealHospitalDays());
        //重症监护天数 wujs
        tTransferData.setNameAndValue("SeriousWard", getSeriousWard());
        //保险年期
        tTransferData.setNameAndValue(
                "Years", String.valueOf(mLCDutySchema.getYears()));
        //保单生效日期
        tTransferData.setNameAndValue(
                "ValiDate", String.valueOf(mLCPolSchema.getCValiDate()));
        //出险日期
        tTransferData.setNameAndValue("AccidentDate",this.mAccDate);
        //累计保费
        tTransferData.setNameAndValue(
                "SumPrem", String.valueOf(mLCPolSchema.getSumPrem()));
        //住院费用
        tTransferData.setNameAndValue(
                "InHospFee", String.valueOf(getInHospFee()));
        //门诊费用
        tTransferData.setNameAndValue(
                "DoorPostFee", String.valueOf(getDoorPostFee()));
        //体检费
        tTransferData.setNameAndValue("PEFee", String.valueOf(getPEFee()));
        //救护车费
        tTransferData.setNameAndValue("AmbuFee", String.valueOf(getAmbuFee()));
        //床位费
        tTransferData.setNameAndValue("BedFee", String.valueOf(getBedFee()));
        //续保次数
        tTransferData.setNameAndValue(
                "RenewCount", String.valueOf(mLCPolSchema.getRenewCount()));
        //被保人0岁保单生效对应日
        tTransferData.setNameAndValue("InsuredvalidBirth",
                                      getInsuredvalideBirth());
        //保险人个人帐户的帐户金额
        tTransferData.setNameAndValue(
                "InsuAccBala", String.valueOf(getInsuAccBala(t_gf_je_base)));
        //指定医院 new
        tTransferData.setNameAndValue(
                "hospital", getFixHospital());
//        手术重要程度OprateGrade
        tTransferData.setNameAndValue("OprateGrade",
                                      String.valueOf(getOprateGrade()));
//        重疾责任成立标志SeriousDisease
        tTransferData.setNameAndValue("SeriousDisease", getSeriousDiseaseFlag());
//        慢性或急性病
        tTransferData.setNameAndValue("Disease1Class1", getDisease1Class());
//        意外信息
        tTransferData.setNameAndValue("Incident", getIncidentInfo());
//        免赔额
        tTransferData.setNameAndValue("GetLimit", String.valueOf(t_GetLimit));
//        赔付比例
        tTransferData.setNameAndValue("GetRate",
                                      String.valueOf(mLCDutySchema.getGetRate()));
//        备用属性1
        tTransferData.setNameAndValue("StandbyFlag1",
                                      String.valueOf(mLCPolSchema.
                getStandbyFlag1()));
//        备用属性2
        tTransferData.setNameAndValue("StandbyFlag2",
                                      String.valueOf(mLCPolSchema.
                getStandbyFlag2()));
//        备用属性3
        tTransferData.setNameAndValue("StandbyFlag3",
                                      String.valueOf(mLCPolSchema.
                getStandbyFlag3()));
//        大额门诊
        tTransferData.setNameAndValue("SupDoorFee",
                                      String.valueOf(mLLSecurityReceiptSchema.
                getHighDoorAmnt()));
//        小额门诊
        tTransferData.setNameAndValue("SmallDoorPay",
                                      String.valueOf(mLLSecurityReceiptSchema.
                getSmallDoorPay()));
//        门急诊
        tTransferData.setNameAndValue("EmergencyPay",
                                      String.valueOf(mLLSecurityReceiptSchema.
                getEmergencyPay()));
//        自付一
        tTransferData.setNameAndValue(
                "SelfPay1", String.valueOf(mLLSecurityReceiptSchema.getSelfPay1()));
//        自付二
        tTransferData.setNameAndValue(
                "SelfPay2", String.valueOf(mLLSecurityReceiptSchema.getSelfPay2()));
//        自费
        tTransferData.setNameAndValue(
                "SelfAmnt", String.valueOf(mLLSecurityReceiptSchema.getSelfAmnt()));
//        社保起付限
        tTransferData.setNameAndValue(
                "SGetLimit",
                String.valueOf(mLLSecurityReceiptSchema.getGetLimit()));
//        低段社保未赔金额
        tTransferData.setNameAndValue(
                "LowAmnt", String.valueOf(mLLSecurityReceiptSchema.getLowAmnt()));
//        中段社保未赔金额
        tTransferData.setNameAndValue(
                "MidAmnt", String.valueOf(mLLSecurityReceiptSchema.getMidAmnt()));
        //高段社保未赔金额1
        tTransferData.setNameAndValue("HighAmnt1",
                                      String.valueOf(mLLSecurityReceiptSchema.
                getHighAmnt1()));
        //高段社保未赔金额2
        tTransferData.setNameAndValue("HighAmnt2",
                                      String.valueOf(mLLSecurityReceiptSchema.
                getHighAmnt2()));
        //大单重特病医疗支付段,暂存在HighAmnt2中
        tTransferData.setNameAndValue("SPlanPart",
                                      String.valueOf(mLLSecurityReceiptSchema.
                getStandbyAmnt()));
        //超高段社保未赔金额
        tTransferData.setNameAndValue("SuperAmnt",
                                      String.valueOf(mLLSecurityReceiptSchema.
                getSuperAmnt()));
        //特需医疗门诊待赔金额
        tTransferData.setNameAndValue("RemDoorFee",
                                      String.valueOf(getRemainDFee()));
        //特需医疗住院待赔金额
        tTransferData.setNameAndValue("RemPatientFee",
                                      String.valueOf(getRemainPFee()));

        if (!mLCPolSchema.getGrpPolNo().equals("00000000000000000000")) {
            LCGrpPolBL tLCGrpPolBL = new LCGrpPolBL();
            tLCGrpPolBL.setGrpPolNo(mLCPolSchema.getGrpPolNo());
            if (tLCGrpPolBL.getInfo()) {
                LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
                tLCGrpPolSchema.setSchema(tLCGrpPolBL.getSchema());
                //赔付比例
                tTransferData.setNameAndValue("GetRate",
                                              String.valueOf(tLCGrpPolSchema.
                        getGetRate()));
                //免赔额
                tTransferData.setNameAndValue("GetLimit",
                                              String.valueOf(t_GetLimit));
                //备用属性1
                tTransferData.setNameAndValue("StandbyFlag1",
                                              String.valueOf(tLCGrpPolSchema.
                        getStandbyFlag1()));
                //备用属性2
                tTransferData.setNameAndValue("StandbyFlag2",
                                              String.valueOf(tLCGrpPolSchema.
                        getStandbyFlag2()));
                //备用属性3
                tTransferData.setNameAndValue("StandbyFlag3",
                                              String.valueOf(tLCGrpPolSchema.
                        getStandbyFlag3()));
            }
            //历次保费本息和
            tTransferData.setNameAndValue("PremIntSum",
                                          String.valueOf(getPremIntSum()));
            //开始领取但未领完的年金和
//            tTransferData.setNameAndValue("RemainAnn",
//                                          String.valueOf(getRemainAnn()));
        }
        //保单的备用属性1
        tTransferData.setNameAndValue("PolStandbyFlag1",
                                      String.valueOf(mLCPolSchema.
                getStandbyFlag1()));
        //该责任已经赔付金额累计
        double tSumPay = getSumDutyPay(mLCPolSchema.getPolNo(),
                                       mLCDutySchema.getDutyCode());
        tTransferData.setNameAndValue("SumPay", String.valueOf(tSumPay));
        //套餐编码
        tTransferData.setNameAndValue("RiskWrapCode", String.valueOf(getRiskWrapCode()));

        Vector tv = tTransferData.getValueNames();
        PubCalculator tPubCalculator = new PubCalculator();
        for (int i = 0; i < tv.size(); i++) {
            String tName = (String) tv.get(i);
            String tValue = (String) tTransferData.getValueByName(tName);
            System.out.println("tName:" + tName + "  tValue:" + tValue);
            tPubCalculator.addBasicFactor(tName, tValue);
            mCalculator.addBasicFactor(tName, tValue);
        }

        String tStr = "";
        System.out.println("CalSQL=" + mCalculator.getCalSQL());
        tStr = mCalculator.calculate();
        if (tStr.trim().equals("")) {
            rValue = 0;
        } else {
            rValue = Double.parseDouble(tStr);
        }
        return rValue;
    }

    /**
     * 计算实际住院天数
     * @return int
     */
    private String getRealHospitalDays() {
        String days = "0";
        String sql = "select sum(RealHospDate) from llfeemain where caseno='" +
                     mCaseNo + "' and caserelano='"
                     + this.mCaseRelaNo + "' and feetype='2'";
        ExeSQL exeSQL = new ExeSQL();
        days = exeSQL.getOneValue(sql);
        return days;
    }

    /**
     * 获取指定医院标志(根据推荐医院生效日期及失效日期来判断暂缺)
     * @return String
     */
    private String getFixHospital() {
        String sql =
                "select max(associateclass) from ldhospital where HospitCode "
                + " in (select HospitalCode from LLFeeMain where caseno='"
                + mCaseNo + "' and caserelano='" + this.mCaseRelaNo + "')";
        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0 && res.equals("1")) {
            return res;
        }
        return "2"; //非定点
    }

    /**
     * 查询重症监护天数
     * @return int
     */
    private String getSeriousWard() {
        String days = "0";
        LLOtherFactorSet tLLOtherFactorSet = new LLOtherFactorSet();
        LLOtherFactorDB tLLOtherFactorDB = new LLOtherFactorDB();
        tLLOtherFactorDB.setCaseNo(mCaseNo);
        tLLOtherFactorDB.setCaseRelaNo(this.mCaseRelaNo);
        tLLOtherFactorSet = tLLOtherFactorDB.query();
        String Serious = "";
        int Count = 0;
        for (int i = 1; i <= tLLOtherFactorSet.size(); i++) {
            LLOtherFactorSchema tSchema = tLLOtherFactorSet.get(i);
            if (tSchema.getFactorCode().equals("1")) { //重症监护
                Serious = "" + tSchema.getValue();
                System.out.println("重症疾病天数" + Serious);
                try {
                    Integer tInteger = new Integer(Serious);
                    Count += tInteger.intValue();
                } catch (Exception ex) {
                    CError.buildErr(this, "重症监护天数输入的不是数字");
                    continue;
                }
            }
        }
        System.out.println("重症疾病天数总和" + Count);
        days = Count + "";
        return days;
    }

    /**
     * 查询手术级别
     * @return String
     */
    private String getOprateGrade() {
        String sql = "select min( oplevel )  from lloperation where caseno='" +
                     mCaseNo + "' and caserelano='" + this.mCaseRelaNo + "'";
        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0)
            return res;
        return "0";
    }

    /**
     * 查询慢性病，急性病
     * @return String
     */
    private String getDisease1Class() {
        String sql = "select case when ChronicMark='0' then '1' else '2' end "
                     + " from LDDisease where ICDCode in "
                     + "(select DiseaseCode from LLCaseCure where caseno='"
                     + mCaseNo + "' and caserelano='"
                     + this.mCaseRelaNo + "')";
        ExeSQL exesql = new ExeSQL();
        System.out.println("慢性病标志查询语句：" + sql);
        SSRS ssrs = exesql.execSQL(sql);
        if (ssrs != null && ssrs.getMaxRow() > 0) {
            return ssrs.GetText(1, 1);
        }
        return "1";
    }

    /**
     * 是否录入意外信息
     * @return String
     */
    private String getIncidentInfo() {
        String Incident = "0";
        LLAccidentDB llAccidentDB = new LLAccidentDB();
        llAccidentDB.setCaseNo(mCaseNo);
        llAccidentDB.setCaseRelaNo(this.mCaseRelaNo);
        LLAccidentSet set = llAccidentDB.query();
        int accidentCount = set.size();
        if (accidentCount > 0)
            Incident = "1";
        return Incident;
    }

    /**
     * 获取重疾责任标志
     * @return String
     */
    private String getSeriousDiseaseFlag() {
        String SDDutyFlag = "0";
        LLCaseCureDB tLLCaseCureDB = new LLCaseCureDB();
        tLLCaseCureDB.setCaseNo(mCaseNo);
        tLLCaseCureDB.setCaseRelaNo(this.mCaseRelaNo);
        tLLCaseCureDB.setSeriousFlag("1");
        LLCaseCureSet tLLCaseCureSet = tLLCaseCureDB.query();
        int SDCount = tLLCaseCureSet.size();
        for (int i = 1; i <= SDCount; i++) {
            String SDFlag = "" + tLLCaseCureSet.get(i).getSeriousDutyFlag();
            if (SDFlag.equals("1")) {
                SDDutyFlag = "1";
                break;
            }
        }
        return SDDutyFlag;
    }

    /**
     * 计算伤残级别给付比例
     * @param tLMDutyGetClmSchema LMDutyGetClmSchema
     * @return double
     */
    private double getCaseRate(LMDutyGetClmSchema tLMDutyGetClmSchema) {
        double tCaseRate = 0;

        if (tLMDutyGetClmSchema.getDeformityGrade() == null ||
            tLMDutyGetClmSchema.getDeformityGrade().equals("0")) {
            //身故
            if (tLMDutyGetClmSchema.getDeadValiFlag().equals("Y")) {
                LLAppClaimReasonSet tLLAppClaimReasonSet = new
                        LLAppClaimReasonSet();
                LLAppClaimReasonDB tLLAppClaimReasonDB = new LLAppClaimReasonDB();
                tLLAppClaimReasonDB.setCaseNo(mCaseNo);
                tLLAppClaimReasonSet = tLLAppClaimReasonDB.query();
                for (int i = 1; i <= tLLAppClaimReasonSet.size(); i++) {
                    String aReason = tLLAppClaimReasonSet.get(i).getReasonCode();
                    System.out.println(aReason);
                    if ("05".equals(aReason)) //身故
                        return 1;
                }
            } else
                //给付金无比例要素
                return 1;
        }
        LLCaseInfoSet tLLCaseInfoSet = new LLCaseInfoSet();
        LLCaseInfoDB tLLCaseInfoDB = new LLCaseInfoDB();
        tLLCaseInfoDB.setCaseNo(mCaseNo);
        tLLCaseInfoDB.setCaseRelaNo(this.mCaseRelaNo);
        String CDeformityGrade = tLMDutyGetClmSchema.getDeformityGrade();
        tLLCaseInfoSet = tLLCaseInfoDB.query();
        for (int i = 1; i <= tLLCaseInfoSet.size(); i++) {
            String aDeformityType = tLLCaseInfoSet.get(i).getType();
            if (aDeformityType.equals(CDeformityGrade)) {
                if (aDeformityType.equals("1"))
                    tCaseRate += tLLCaseInfoSet.get(i).getDeformityRate();
                if (aDeformityType.equals("2") &&
                    tLLCaseInfoSet.get(i).getDeformityRate() > tCaseRate) {
                    tCaseRate = tLLCaseInfoSet.get(i).getDeformityRate();
                }
            }
        }
        if (tCaseRate > 1) {
            tCaseRate = 1;
        }
        return tCaseRate;
    }

    /**
     * 从分案收据明细中查询事故者发生床位费用总金额
     * @return double
     */
    private double getBedFee() {
        double tInpval = 0;
        int i;
        String sql = "select sum(fee-preamnt-refuseamnt-selfamnt) "
                + " from LLCaseReceipt where MainFeeNo in "
                + " (select MainFeeNo from LLFeeMain where  caseno='"
                + mCaseNo + "' and caserelano='" + this.mCaseRelaNo
                + "') and FeeItemCode ='201' ";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0)
            return Double.parseDouble(res);

        return tInpval;
    }

    /**
     * 查询各有效性费用项目金额
     * @return double
     */
    private double[] getAvaliFee() {
        String sql =
                "select sum(fee),sum(selfamnt),sum(preamnt),sum(refuseamnt)"
                + " from LLCaseReceipt where MainFeeNo in "
                + " (select MainFeeNo from LLFeeMain where caseno='"
                + mCaseNo + "' and caserelano='" +mCaseRelaNo
                + "') and FeeItemCode in "
                + " (select Feecode from LMDutyGetFeeRela where getdutykind='"
                + mGetDutyKind + "' and getdutycode='" + mGetDutyCode + "')";

        ExeSQL exesql = new ExeSQL();
        SSRS tssrs = exesql.execSQL(sql);
        double[] afee = {0,0,0,0};
        if(tssrs!=null){
            for (int i = 0; i < tssrs.getMaxCol();i++) {
                try{
                    afee[i] = Double.parseDouble(tssrs.GetText(1,i+1));
                 }catch(Exception ex){
                     System.out.println("第"+i+"项费用不存在");
                 }
            }
        }
        return afee;
    }

    /**
     * 住院费用
     * @return double
     */
    private double getInHospFee() {
        double hospFee = 0;
        String sql = "select sum(fee-preamnt-selfamnt-refuseamnt) "
                     + "from LLCaseReceipt where MainFeeNo in "
                     + "(select MainFeeNo from LLFeeMain where  caseno='"
                     + mCaseNo + "' and caserelano='" + this.mCaseRelaNo
                     + "' and FeeType='2' and FeeItemCode in "
                     //加描述表中的信息
                     + "(select Feecode from LMDutyGetFeeRela where getdutykind='"
                     + this.mGetDutyKind + "' and getdutycode='" +
                     this.mGetDutyCode + "'))";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0)
            return Double.parseDouble(res);
        return hospFee;
    }

    /**
     * 帐单金额
     * @return double
     */
    private double getTabFee() {
        double hospFee = 0;
        String sql = "select sum(sumfee) from LLFeeMain where  caseno='" +
                     mCaseNo + "' and caserelano='" +
                     this.mCaseRelaNo + "'";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0) {
            return Double.parseDouble(res);
        }
        return hospFee;
    }

    /**
     * 门诊费用
     * @return double
     */
    private double getDoorPostFee() {
        double hospFee = 0;
        String sql = "select sum(fee-preamnt-selfamnt-refuseamnt) "
                + " from LLCaseReceipt where MainFeeNo in (select MainFeeNo "
                + "from LLFeeMain where  caseno='" + mCaseNo
                + "' and caserelano='" + mCaseRelaNo
                + "' and FeeType='1' and FeeItemCode<>'120')";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0) {
            return Double.parseDouble(res);
        }
        return hospFee;
    }

    /**
     * 救护车费用
     * @return double
     */
    private double getAmbuFee() {
        double AmbuFee = 0;
        String sql =
                "select sum(fee-preamnt-selfamnt-refuseamnt) from "
                + " LLCaseReceipt where MainFeeNo in (select MainFeeNo from "
                + "LLFeeMain where  caseno='" + mCaseNo + "' and caserelano='"
                + mCaseRelaNo + "') and FeeItemCode='120'";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0) {
            return Double.parseDouble(res);
        }
        return AmbuFee;
    }

    /**
     * 从分案收据明细中查询事故者发生床位费用总金额
     * @return double
     */
    private double getPEFee() {
        double tInpval = 0;
        String sql = "select sum(fee-preamnt-refuseamnt-selfamnt) from "
                + " LLCaseReceipt where MainFeeNo in (select MainFeeNo from "
                + " LLFeeMain where  caseno='" + mCaseNo
                + "' and caserelano='" + this.mCaseRelaNo
                + "') and FeeItemCode ='227' ";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0)
            return Double.parseDouble(res);

        return tInpval;
    }

    /**
     * 费用险的责任每分到各个费用细项，但是某个费用有日均限额，查询相关的拒付金额
     * @return double
     */
    private double getDayOverFee() {
        double OverFlow = 0;

        double hospFee = 0;
        hospFee = Arith.round(hospFee, 2);
        String strSql = "select * from LMDutyGetFeeRela where getdutykind='"
                        + this.mGetDutyKind + "' and getdutycode='" +
                        this.mGetDutyCode + "' and Dayfeemaxctrl is not null";
        LMDutyGetFeeRelaDB tLMDutyGetFeeRelaDB = new LMDutyGetFeeRelaDB();
        LMDutyGetFeeRelaSet tLMDutyGetFeeRelaSet = new LMDutyGetFeeRelaSet();
        tLMDutyGetFeeRelaSet = tLMDutyGetFeeRelaDB.executeQuery(strSql);
        System.out.println("查询日限额" + strSql);
        if (tLMDutyGetFeeRelaSet == null || tLMDutyGetFeeRelaSet.size() <= 0) {
            return 0;
        } else {
            for (int i = 1; i <= tLMDutyGetFeeRelaSet.size(); i++) {
                LMDutyGetFeeRelaSchema tLMDutyGetFeeRelaSchema = new
                        LMDutyGetFeeRelaSchema();
                tLMDutyGetFeeRelaSchema = tLMDutyGetFeeRelaSet.get(i);
                double Daylimit = 0.0;
                if (tLMDutyGetFeeRelaSchema.getDayFeeMAXCtrl().equals("01")) {
                    Daylimit = tLMDutyGetFeeRelaSchema.getDayFeeMaxValue();
                }
                System.out.println(Daylimit);
                String sql =
                        "select sum(fee-selfamnt-preamnt-refuseamnt) from LLCaseReceipt "
                        +
                        " where MainFeeNo in (select MainFeeNo from LLFeeMain where caseno='"
                        + mCaseNo + "' and caserelano='" +
                        this.mCaseRelaNo + "')"
                        + " and FeeItemCode ='" +
                        tLMDutyGetFeeRelaSchema.getFeecode() + "'";
                ExeSQL exesql = new ExeSQL();
                String spefee = exesql.getOneValue(sql);
                if (!spefee.equals("null") && !spefee.equals(""))
                    hospFee = Double.parseDouble(spefee);
                String D = getRealHospitalDays();
                double hosdays = Integer.parseInt(D);
                double tDlimit = Daylimit * hosdays;
                if (hospFee > tDlimit) {
                    OverFlow += (hospFee - tDlimit);
                }
            }
            return OverFlow;
        }
    }

    /**
     * 计算免赔额
     * @param GetDutyCode String
     * @param YearGetLimitFlag String
     * @return double
     */
    private double getGetLimit(String YearGetLimitFlag) {
        double getlimit = mLCDutySchema.getGetLimit();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLCPolSchema.getGrpContNo());
        String adutyYearFlag = "";
        //暂时使用契约GetFlag字段，非标准业务实施后从新取值
        if (tLCGrpContDB.getInfo())
            adutyYearFlag += tLCGrpContDB.getGetFlag();
        if (getlimit < 0.001) {
            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            tLCInsuredDB.setInsuredNo(mLCPolSchema.getInsuredNo());
            tLCInsuredDB.setContNo(mLCPolSchema.getContNo());
            String workstate = "";
            if (tLCInsuredDB.getInfo())
                workstate = tLCInsuredDB.getInsuredStat();
            LDRiskParamPrintDB tLDRiskParamPrintDB = new LDRiskParamPrintDB();
            LDRiskParamPrintSet tLDRiskParamPrintSet = new LDRiskParamPrintSet();
            tLDRiskParamPrintDB.setRiskCode(mLCPolSchema.getRiskCode());
            tLDRiskParamPrintDB.setParamValue2(mGetDutyCode);
            tLDRiskParamPrintSet.set(tLDRiskParamPrintDB.query());
            for (int k = 1; k <= tLDRiskParamPrintSet.size(); k++) {
                LDRiskParamPrintSchema tLDRiskParamPrintSchema = new
                        LDRiskParamPrintSchema();
                tLDRiskParamPrintSchema = tLDRiskParamPrintSet.get(k);
                if (tLDRiskParamPrintSchema.getParamValue3() == null ||
                    tLDRiskParamPrintSchema.getParamValue3().equals("") ||
                    tLDRiskParamPrintSchema.getParamValue3().equals(workstate)) {
                    getlimit = Double.parseDouble(tLDRiskParamPrintSchema.
                                                  getFactorValue());
                    break;
                }
            }
        }
        System.out.println("adutyYearFlag:" + adutyYearFlag);
        if (adutyYearFlag.equals("1")) {
            String adutycode = mLCDutySchema.getDutyCode();
            for (int k = 1; k <= mSaveLLClaimDetailSet.size(); k++) {
                if (mSaveLLClaimDetailSet.get(k).getDutyCode().equals(
                        adutycode)) {
                    if (!mSaveLLClaimDetailSet.get(k).getCaseRelaNo().equals(
                            mCaseRelaNo))
                        getlimit -= mSaveLLClaimDetailSet.get(k).getOutDutyAmnt();
                    else {
                        if (!mSaveLLClaimDetailSet.get(k).getGetDutyCode().
                            equals(mGetDutyCode) ||
                            !mSaveLLClaimDetailSet.get(k).getGetDutyKind().
                            equals(mGetDutyKind))
                            getlimit -= mSaveLLClaimDetailSet.get(k).
                                    getOutDutyAmnt();
                    }
                }
            }
            LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
            tLLClaimDetailDB.setPolNo(mLCPolSchema.getPolNo());
            tLLClaimDetailDB.setDutyCode(adutycode);
            LLClaimDetailSet tLLClaimDetailSet = tLLClaimDetailDB.query();
            for (int k = 1; k <= tLLClaimDetailSet.size(); k++) {
                LLCaseDB tLLCaseDB = new LLCaseDB();
                tLLCaseDB.setCaseNo(tLLClaimDetailSet.get(k).getCaseNo());
                if (!tLLCaseDB.getInfo())
                    System.out.println("查询不到案件信息");
                if (tLLCaseDB.getRgtState().equals("09") ||
                    tLLCaseDB.getRgtState().equals("11")
                    || tLLCaseDB.getRgtState().equals("12"))
                    getlimit -= tLLClaimDetailSet.get(k).getOutDutyAmnt();
            }
        } else {
            if (YearGetLimitFlag == null)
                YearGetLimitFlag = "";
            if (YearGetLimitFlag.equals("1")) {
                for (int k = 1; k <= mSaveLLClaimDetailSet.size(); k++) {
                    if (mSaveLLClaimDetailSet.get(k).getGetDutyCode().equals(
                            mGetDutyCode) &&
                        !mSaveLLClaimDetailSet.get(k).
                        getCaseRelaNo().equals(mCaseRelaNo))
                        getlimit -= mSaveLLClaimDetailSet.get(k).getOutDutyAmnt();
                }
                LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
                tLLClaimDetailDB.setPolNo(mLCPolSchema.getPolNo());
                tLLClaimDetailDB.setGetDutyCode(mGetDutyCode);
                LLClaimDetailSet tLLClaimDetailSet = tLLClaimDetailDB.query();
                for (int k = 1; k <= tLLClaimDetailSet.size(); k++) {
                    LLCaseDB tLLCaseDB = new LLCaseDB();
                    tLLCaseDB.setCaseNo(tLLClaimDetailSet.get(k).getCaseNo());
                    if (!tLLCaseDB.getInfo())
                        System.out.println("查询不到案件信息");
                    if (tLLCaseDB.getRgtState().equals("09") ||
                        tLLCaseDB.getRgtState().equals("11")
                        || tLLCaseDB.getRgtState().equals("12"))
                        getlimit -= tLLClaimDetailSet.get(k).getOutDutyAmnt();
                }
            }
        }
        if (getlimit < 0.01)
            getlimit = 0;
        System.out.println("************免赔额*************：" +
                           Arith.round(getlimit, 2));
        return Arith.round(getlimit, 2);
    }

    /**
     * 查询帐单中自费部分
     * @return double
     */
    private double getOwnFee() {
        double OwnFee = 0.0;
        String sql =
                "select sum(selfamnt) from LLCaseReceipt where MainFeeNo in "
                + " (select MainFeeNo from LLFeeMain where  caseno='" +
                mCaseNo + "' and caserelano='" +
                this.mCaseRelaNo + "'"
                //  +"' and FeeType='0'"
                //加描述表中的信息
                +
                " and FeeItemCode in (select Feecode from LMDutyGetFeeRela where getdutykind='"
                + this.mGetDutyKind + "' and getdutycode='" +
                this.mGetDutyCode + "'))";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0) {
            OwnFee = Double.parseDouble(res);
        }
        return OwnFee;
    }

    /**
     * 取社保帐单中各分段值（理算前）
     * @return boolean
     */
    private boolean getSecuReceipt() {
        mLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
        String sql = "select * from LLSecurityReceipt where MainFeeNo in " +
                     " (select MainFeeNo from LLFeeMain where  caseno='" +
                     mCaseNo + "' and caserelano='" +
                     this.mCaseRelaNo + "')";
        System.out.println(sql);
        LLSecurityReceiptDB tLLSecurityReceiptDB = new LLSecurityReceiptDB();
        LLSecurityReceiptSet tLLSecurityReceiptSet = new LLSecurityReceiptSet();
        tLLSecurityReceiptSet = tLLSecurityReceiptDB.executeQuery(sql);
        if (tLLSecurityReceiptSet != null && tLLSecurityReceiptSet.size() > 0) {
            int srcount = tLLSecurityReceiptSet.size();
            mLLSecurityReceiptSchema = tLLSecurityReceiptSet.get(1);
            double lowamnt = 0.0;
            double midamnt = 0.0;
            double highamnt1 = 0.0;
            double highamnt2 = 0.0;
            double supamnt = 0.0;
            double sDoorpay = 0.0;
            double cDoorpay = 0.0;
            double bDoorpay = 0.0;
            double appamnt = 0.0;
            double overamnt = 0.0;
            double selfpay2 = 0.0;
            double selfamnt = 0.0;
            double standbyamnt = 0.0;
            for (int i = 1; i <= srcount; i++) {
                lowamnt += tLLSecurityReceiptSet.get(i).getLowAmnt();
                midamnt += tLLSecurityReceiptSet.get(i).getMidAmnt();
                highamnt1 += tLLSecurityReceiptSet.get(i).getHighAmnt1();
                highamnt2 += tLLSecurityReceiptSet.get(i).getHighAmnt2();
                supamnt += tLLSecurityReceiptSet.get(i).getSuperAmnt();
                sDoorpay += tLLSecurityReceiptSet.get(i).getSmallDoorPay();
                cDoorpay += tLLSecurityReceiptSet.get(i).getEmergencyPay();
                bDoorpay += tLLSecurityReceiptSet.get(i).getHighDoorAmnt();
                appamnt += tLLSecurityReceiptSet.get(i).getFeeInSecu();
                selfpay2 += tLLSecurityReceiptSet.get(i).getSelfPay2();
                selfamnt += tLLSecurityReceiptSet.get(i).getSelfAmnt();
                standbyamnt += tLLSecurityReceiptSet.get(i).getStandbyAmnt();
            }
//            if (appamnt>mLCDutySchema.getAmnt())
//            {
//                if(mLCDutySchema.getAmnt()>0.001)
//                    overamnt = appamnt - mLCDutySchema.getAmnt();
//                supamnt = supamnt - overamnt;
//            }
            mLLSecurityReceiptSchema.setLowAmnt(lowamnt);
            mLLSecurityReceiptSchema.setMidAmnt(midamnt);
            mLLSecurityReceiptSchema.setHighAmnt1(highamnt1);
            mLLSecurityReceiptSchema.setHighAmnt2(highamnt2);
            mLLSecurityReceiptSchema.setSuperAmnt(supamnt);
            mLLSecurityReceiptSchema.setSmallDoorPay(sDoorpay);
            mLLSecurityReceiptSchema.setEmergencyPay(cDoorpay);
            mLLSecurityReceiptSchema.setHighDoorAmnt(bDoorpay);
            mLLSecurityReceiptSchema.setSecuRefuseFee(overamnt);
            mLLSecurityReceiptSchema.setSelfPay2(selfpay2);
            mLLSecurityReceiptSchema.setSelfAmnt(selfamnt);
            mLLSecurityReceiptSchema.setStandbyAmnt(standbyamnt);
        }
        return true;
    }

    /**
     * 取社保帐单分段金额（理算后）
     * @return double
     */
    private double getClaimMoney(String CalCode) {
        double claimmoney = 0.0;
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("llcalclaimamnt");
        tLDCodeDB.setCode(CalCode);
        String ClaimName = "";
        if (!tLDCodeDB.getInfo()) {
            havetab = false;
            return 0;
        } else {
            havetab = true;
            ClaimName = "" + tLDCodeDB.getCodeName();
        }
        if (ClaimName.equals("DoorPostFee")) {
            claimmoney = getDoorPostFee();
        } else if (ClaimName.equals("RemDoorFee")) {
            claimmoney = getRemainDFee();
        } else if (ClaimName.equals("RemPatientFee")) {
            claimmoney = getRemainPFee();
        } else {
            String sql = "select " + ClaimName +
                         " from LLSecurityReceipt where MainFeeNo " +
                         " in (select MainFeeNo from LLFeeMain where  caseno='" +
                         mCaseNo + "' and caserelano='" +
                         this.mCaseRelaNo + "')";
            System.out.println(sql);
            ExeSQL execsql = new ExeSQL();
            SSRS ssrs = execsql.execSQL(sql);
            if (ssrs != null) {
                for (int j = 1; j <= ssrs.getMaxRow(); j++) {
                    claimmoney += Double.parseDouble(ssrs.GetText(j, 1));
                }
            }
        }
        return claimmoney;
    }

    //计算额外保险金
    private double getAdditionalPay(String polno) {
        double additionalpay = 0.0;
        for (int k = 1; k <= mSaveLLClaimDetailSet.size(); k++) {
            LLClaimDetailSchema DetailSchema = mSaveLLClaimDetailSet.get(k);
            if (!DetailSchema.getGetDutyCode().equals(mGetDutyCode) &&
                DetailSchema.getCaseRelaNo().equals(mCaseRelaNo) &&
                DetailSchema.getPolNo().equals(polno))
                additionalpay += DetailSchema.getRealPay();
        }
        return additionalpay;
    }

    /**
     * 计算特需待赔的门诊费用
     * @return boolean
     */
    private double getRemainDFee() {
        double remaindoorfee = 0.0;
        LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
        LLFeeMainSet tLLFeeMainSet = new LLFeeMainSet();
        tLLFeeMainDB.setCaseNo(mCaseNo);
        tLLFeeMainDB.setCaseRelaNo(mCaseRelaNo);
        tLLFeeMainDB.setFeeType("1");
        tLLFeeMainSet = tLLFeeMainDB.query();
        for (int i = 1; i <= tLLFeeMainSet.size(); i++) {
            if (tLLFeeMainSet.get(i).getFeeAtti().equals("3"))
                remaindoorfee += tLLFeeMainSet.get(i).getRemnant();
            if (tLLFeeMainSet.get(i).getFeeAtti().equals("0"))
                remaindoorfee += tLLFeeMainSet.get(i).getSumFee();
        }
        return remaindoorfee;
    }

    /**
     * 计算特需待赔的住院费用
     * @return boolean
     */
    private double getRemainPFee() {
        double remainpatientfee = 0.0;
        LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
        LLFeeMainSet tLLFeeMainSet = new LLFeeMainSet();
        tLLFeeMainDB.setCaseNo(mCaseNo);
        tLLFeeMainDB.setCaseRelaNo(mCaseRelaNo);
        tLLFeeMainDB.setFeeType("2");
        tLLFeeMainSet = tLLFeeMainDB.query();
        for (int i = 1; i <= tLLFeeMainSet.size(); i++) {
            if (tLLFeeMainSet.get(i).getFeeAtti().equals("3"))
                remainpatientfee += tLLFeeMainSet.get(i).getRemnant();
            if (tLLFeeMainSet.get(i).getFeeAtti().equals("0"))
                remainpatientfee += tLLFeeMainSet.get(i).getSumFee();
        }
        return remainpatientfee;
    }

    /**
     * 计算保单在责任下的累计赔付金额
     * @param tPolNo String
     * @param tDutyCode String
     * @return double
     */
    private double getSumDutyPay(String tPolNo, String tDutyCode) {
        LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
        LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
        String tSql = "select * from llclaimdetail where polno='" +
                      tPolNo + "' and dutycode ='" + tDutyCode + "' ";
        tSql = tSql +
               " and clmno in(select clmno from llclaim where ClmState='2' or ClmState='3')";
        System.out.println(tSql);
        tLLClaimDetailSet = tLLClaimDetailDB.executeQuery(tSql);
        System.out.println("LLClaimDetailSet.size=" + tLLClaimDetailSet.size());
        double tSumDuty = 0;
        for (int k = 1; k <= tLLClaimDetailSet.size(); k++) {
            LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
            tLLClaimDetailSchema = tLLClaimDetailSet.get(k);
            tSumDuty = tSumDuty + tLLClaimDetailSchema.getRealPay();
        }
        return tSumDuty;
    }

    /**
     * 检查该张保单下所有责任赔付是否超过该责任的保额
     * 查询保单所有责任记录
     * 添加续保的方法
     */
    private double getacupay(String aGetDutyCode, String aCaseRelaNo) {
        double acuLimit = 0;
        //查询保单所有的给付记录（包含本次赔付）
        LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
        LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
        String tSql = "select * from llclaimdetail where polno='" +
                      mLCPolSchema.getPolNo() + "' ";
        tSql = tSql + " and caseno in (select caseno from llcase where "
               + " customerno ='" + mLCPolSchema.getInsuredNo()
               + "' and rgtstate in ('09','10','11','12')) ";
        System.out.println(tSql); //不包括本次理赔
        tLLClaimDetailSet = tLLClaimDetailDB.executeQuery(tSql);
        System.out.println("LLClaimDetailSet.size=" + tLLClaimDetailSet.size());
        double tSumDuty = 0;
        for (int k = 1; k <= tLLClaimDetailSet.size(); k++) {
            LLClaimDetailSchema tLLClaimDetailSchema = new
                    LLClaimDetailSchema();
            tLLClaimDetailSchema = tLLClaimDetailSet.get(k);
            if (tLLClaimDetailSchema.getDutyCode().equals(mLCDutySchema.
                    getDutyCode())) {
                tSumDuty = tSumDuty + tLLClaimDetailSchema.getRealPay();
            }
        }
        double tThisSumDuty = 0; //本次理赔赔款
        for (int k = 1; k <= this.mSaveLLClaimDetailSet.size(); k++) {
            LLClaimDetailSchema tLLClaimDetailSchema = new
                    LLClaimDetailSchema();
            tLLClaimDetailSchema = this.mSaveLLClaimDetailSet.get(k);
            if (tLLClaimDetailSchema.getDutyCode().equals(mLCDutySchema.
                    getDutyCode())
                && !tLLClaimDetailSchema.getGetDutyCode().equals(aGetDutyCode)
                && !tLLClaimDetailSchema.getCaseRelaNo().equals(aCaseRelaNo)) {
                tThisSumDuty = tThisSumDuty + tLLClaimDetailSchema.getRealPay();
            }
        }
        System.out.println(mLCDutySchema.getDutyCode() + "已赔付=" + tSumDuty +
                           tThisSumDuty);
        System.out.println(mLCDutySchema.getDutyCode() + "保额=" +
                           mLCDutySchema.getAmnt());
        acuLimit = mLCDutySchema.getAmnt() - tSumDuty - tThisSumDuty;
        //检查保单是否超过保单保额
        //添加续保的方法
        SynLCLBGetBL tSynLCLBGetBL = new SynLCLBGetBL();
        tSynLCLBGetBL.setPolNo(mLCPolSchema.getPolNo());
        LCGetSet tLCGetSet = new LCGetSet();
        tLCGetSet.set(tSynLCLBGetBL.query());
        if (tSynLCLBGetBL.mErrors.needDealError()) {
            CError.buildErr(this, "险种保单给付责任查询失败");
            return 0;
        }
        //添加续保的处理方法
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setPolNo(mLCPolSchema.getPolNo());
        if (!tLCPolBL.getInfo()) {
            CError.buildErr(this, "险种保单查询失败");
            return 0;
        }
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setSchema(tLCPolBL.getSchema());

        double tSumPay = 0;
        for (int j = 1; j <= tLCGetSet.size(); j++) {
            LCGetSchema tLCGetSchema = new LCGetSchema();
            tLCGetSchema.setSchema(tLCGetSet.get(j));
            if ("1".equals(tLCGetSchema.getLiveGetType())) {
                tSumPay = tSumPay + tLCGetSchema.getSumMoney();
            }
        }
        System.out.println("tSumPay=" + tSumPay);
        System.out.println("Amnt=" + tLCPolSchema.getAmnt());
        double tempGet = tLCPolSchema.getAmnt() - tSumPay;
        if (tempGet < acuLimit || mLCDutySchema.getAmnt() < 0.01)
            acuLimit = tempGet;
        return acuLimit;
    }

    /**
     * 计算契约协议中定的特殊理赔算法
     * @return boolean
     */
    private boolean calSpecial(TransferData tTransferData) {
        //查找计算要素
        LCFactoryDB tLCFactoryDB = new LCFactoryDB();
        PubCalculator tPubCalculator = new PubCalculator();
        LCFactorySet tResultLCFactorySet = new LCFactorySet();

        String tIndivSql = "select * from LCFactory where GrpPolNo='" +
                           this.mLCPolSchema.getGrpPolNo() + "' and polno='" +
                           this.mLCPolSchema.getPolNo() + "' ";
        String tCollSql = "select * from LCFactory where GrpPolNo='" +
                          this.mLCPolSchema.getGrpPolNo() +
                          "' and polno='00000000000000000000'";
        String tGetDutySql = " and factorytype='000002' and otherno in(select GetDutyCode from LMDutyGetRela where DutyCode='" +
                             this.mLCDutySchema.getDutyCode() + "')";
        String tDutySql = " and factorytype='000001' and otherno='" +
                          this.mLCDutySchema.getDutyCode() + "'";
        String tPolSql = " and factorytype='000000' and otherno='" +
                         this.mLCPolSchema.getPolNo() + "'";
        String tGrpPolSql = " and factorytype='000000' and otherno='" +
                            this.mLCPolSchema.getGrpPolNo() + "'";
        //处理个单协议中的给付信息
        String tsql = tIndivSql + tGetDutySql;
        System.out.println(tsql);
        LCFactorySet t0LCFactorySet = tLCFactoryDB.executeQuery(tsql);
        tResultLCFactorySet.add(t0LCFactorySet);

        //处理个单协议中的责任信息
        tsql = tIndivSql + tDutySql;
        System.out.println(tsql);
        LCFactorySet t1LCFactorySet = tLCFactoryDB.executeQuery(tsql);
        tResultLCFactorySet.add(t1LCFactorySet);

        //处理个单协议中的保单信息
        tsql = tIndivSql + tPolSql;
        System.out.println(tsql);
        LCFactorySet t2LCFactorySet = tLCFactoryDB.executeQuery(tsql);
        tResultLCFactorySet.add(t2LCFactorySet);

        //处理团单协议中的给付信息
        tsql = tCollSql + tGetDutySql;
        System.out.println(tsql);
        LCFactorySet t3LCFactorySet = tLCFactoryDB.executeQuery(tsql);
        tResultLCFactorySet.add(t3LCFactorySet);

        //处理团单协议中的责任信息
        tsql = tCollSql + tDutySql;
        System.out.println(tsql);
        LCFactorySet tLCFactorySet = tLCFactoryDB.executeQuery(tsql);
        tResultLCFactorySet.add(tLCFactorySet);

        //处理团单协议中的保单信息
        tsql = tCollSql + tGrpPolSql;
        System.out.println(tsql);
        LCFactorySet t5LCFactorySet = tLCFactoryDB.executeQuery(tsql);
        tResultLCFactorySet.add(t5LCFactorySet);

        Vector tv = tTransferData.getValueNames();
        for (int i = 0; i < tv.size(); i++) {
            String tName = (String) tv.get(i);
            String tValue = (String) tTransferData.getValueByName(tName);
            System.out.println("tName:" + tName + "  tValue:" + tValue);
            tPubCalculator.addBasicFactor(tName, tValue);
        }
        System.out.println("load data to PubCalculator end");

        for (int i = 1; i <= tResultLCFactorySet.size(); i++) {
            LCFactorySchema tLCFactorySchema = new LCFactorySchema();
            tLCFactorySchema = tResultLCFactorySet.get(i);
            tPubCalculator.setCalSql(tLCFactorySchema.getCalSql());
            String tResult = tPubCalculator.calculate();
            if (tResult != null && !tResult.trim().equals("")) {
                if (tTransferData.findIndexByName(tLCFactorySchema.
                                                  getFactoryName()) ==
                    -1) {
                    tTransferData.setNameAndValue(tLCFactorySchema.
                                                  getFactoryName(),
                                                  tResult);
                }
            }
        }
        return true;
        //如果没有协议要素，取default值
    }

    //被保人0岁保单生效对应日
    private String getInsuredvalideBirth() {
        FDate tInsuredBirthday = new FDate();
        FDate tCValidate = new FDate();
        tCValidate.getDate(mLCPolSchema.getCValiDate());
        Date tInsuredvalideBirth = null;
        tInsuredvalideBirth = PubFun.calDate(tInsuredBirthday.getDate(
                mLCPolSchema.getInsuredBirthday()), 0, "Y",
                                             tCValidate.getDate(mLCPolSchema.
                getCValiDate()));
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        return df.format(tInsuredvalideBirth);
    }

    /**
     * 开始领取但尚未领完的年金
     * @return double
     */
    private double getRemainAnn() {
        SynLCLBGetBL aSynLCLBGetBL = new SynLCLBGetBL();
        aSynLCLBGetBL.setPolNo(this.mLCPolSchema.getPolNo());

        LCGetSet tLCGetSet = new LCGetSet();
        tLCGetSet = aSynLCLBGetBL.query();
        double tReturnMoney = 0;
        for (int i = 1; i <= tLCGetSet.size(); i++) {
            LCGetSchema tLCGetSchema = new LCGetSchema();
            tLCGetSchema = tLCGetSet.get(i);
            if (tLCGetSchema.getLiveGetType().equals("1"))
                continue;
            FDate tFDate = new FDate();
            if (tFDate.getDate(tLCGetSchema.getGetStartDate()).
                after(tFDate.getDate(mAccDate)))
                continue;

            if (tLCGetSchema.getGetIntv() == 0)
                continue;

            int NoneGetNum = PubFun.calInterval(tLCGetSchema.getGettoDate(),
                                                tLCGetSchema.getGetEndDate(),
                                                "M") / tLCGetSchema.getGetIntv();
            tReturnMoney += NoneGetNum * tLCGetSchema.getStandMoney();
        }
        return tReturnMoney;
    }

    /**
     * 保险人个人帐户的帐户金额
     * @return double
     * 需要补充扣除管理费和利息，但是相应表结构未设计且产品为做相应要求，待做
     */
    private double getInsuAccBala(double exp_getmoney) {
        if (exp_getmoney < 0.01)
            return 0.0;
        double actmoney = 0.0;
        double tem_money = 0.0;
        LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
        tLCInsureAccTraceDB.setPolNo(mLCPolSchema.getPolNo());
        LCInsureAccTraceSet tLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
        String toSerialNo = "";
        if (tLCInsureAccTraceSet.size() > 0) {
            for (int i = 1; i <= tLCInsureAccTraceSet.size(); i++) {
                if (tLCInsureAccTraceSet.get(i).getOtherNo().equals(mCaseNo)) {
                    //如果该案件在轨迹表里已经有记录,则serialno不变
                    toSerialNo = tLCInsureAccTraceSet.get(i).getSerialNo();
                    continue;
                }
                tem_money += tLCInsureAccTraceSet.get(i).getMoney();
            }
        } else
            return 0.0;
        //本次案件的每条责任的给付记录
        for (int i = 1; i <= mLCInsureAccTraceSet.size(); i++) {
            tem_money += mLCInsureAccTraceSet.get(i).getMoney();
        }
        if (tem_money < 0.01)
            return 0.0;
        if (tem_money > exp_getmoney)
            actmoney = exp_getmoney;
        else
            actmoney = tem_money;
        double decrease = 0 - actmoney;
        String tSerialNo = mRgtNo.substring(1);
        if (toSerialNo.equals("")) {
            String countsql =
                    "select count(otherno)+1 from lcinsureacctrace where"
                    + " serialno like '" + tSerialNo + "%'";
            ExeSQL exesql = new ExeSQL();
            String subSerial = exesql.getOneValue(countsql);
            if (subSerial.length() == 1)
                subSerial = "00" + subSerial;
            if (subSerial.length() == 2)
                subSerial = "0" + subSerial;
            tSerialNo = tSerialNo + subSerial;
        } else
            tSerialNo = toSerialNo;
        LCInsureAccTraceSchema tLCInsureAccTraceSchema = tLCInsureAccTraceSet.
                get(1);
        tLCInsureAccTraceSchema.setSerialNo(tSerialNo);
        tLCInsureAccTraceSchema.setState("temp");
        tLCInsureAccTraceSchema.setMoney(decrease);
        tLCInsureAccTraceSchema.setMoneyType("PK");
        tLCInsureAccTraceSchema.setOtherNo(mCaseNo);
        tLCInsureAccTraceSchema.setOtherType("5");
        mLCInsureAccTraceSet.add(tLCInsureAccTraceSchema);

        return actmoney;
    }

    double GrpAccToInsuAcc(double exp_getmoney) {
        double actmoney = 0.0;
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setGrpContNo(mLCPolSchema.getGrpContNo());
        tLCPolDB.setPolTypeFlag("2");
        LCPolSet tLCPolSet = tLCPolDB.query();
        double tem_money = 0.0;
        if (tLCPolSet.size() > 0) {
            String tGrpAccPolNo = tLCPolSet.get(1).getPolNo();
            LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
            tLCInsureAccTraceDB.setPolNo(tGrpAccPolNo);
            LCInsureAccTraceSet tLCInsureAccTraceSet = tLCInsureAccTraceDB.
                    query();
            String toSerialNo = "";
            if (tLCInsureAccTraceSet.size() > 0) {
                for (int i = 1; i <= tLCInsureAccTraceSet.size(); i++) {
                    if (tLCInsureAccTraceSet.get(i).getOtherNo().equals(mCaseNo)) {
                        toSerialNo = tLCInsureAccTraceSet.get(i).getSerialNo();
                        continue;
                    }
                    tem_money += tLCInsureAccTraceSet.get(i).getMoney();
                }
            } else
                return 0.0;
            for (int i = 1; i <= mLCInsureAccTraceSet.size(); i++) {
                tem_money += mLCInsureAccTraceSet.get(i).getMoney();
            }
            if (tem_money > exp_getmoney)
                actmoney = exp_getmoney;
            else
                actmoney = tem_money;
            double decrease = 0 - actmoney;
            String tSerialNo = "";
            if (toSerialNo.equals("")) {
                String subSerial = "" + tLCInsureAccTraceSet.size();
                if (subSerial.length() == 1)
                    subSerial = "0" + subSerial;
                if (subSerial.length() == 2)
                    subSerial = "00" + subSerial;
                tSerialNo = mRgtNo.substring(1) +
                            subSerial;
            } else
                tSerialNo = toSerialNo;
            LCInsureAccTraceSchema tLCInsureAccTraceSchema =
                    tLCInsureAccTraceSet.get(1);
            tLCInsureAccTraceSchema.setSerialNo(tSerialNo);
            tLCInsureAccTraceSchema.setState("temp");
            tLCInsureAccTraceSchema.setMoney(decrease);
            tLCInsureAccTraceSchema.setMoneyType("PK");
            tLCInsureAccTraceSchema.setOtherNo(mCaseNo);
            tLCInsureAccTraceSchema.setOtherType("5");
            mLCInsureAccTraceSet.add(tLCInsureAccTraceSchema);
        }

        return actmoney;
    }

    //帐户存取轨迹保存
    private boolean getInsuAcc() {
        if (mLCInsureAccTraceSet.size() > 0) {
            mLCInsureAccTraceSchema.setSchema(mLCInsureAccTraceSet.get(1));
            double tMoney = 0;
            for (int i = 1; i <= mLCInsureAccTraceSet.size(); i++) {
                tMoney += mLCInsureAccTraceSet.get(i).getMoney();
            }
            mLCInsureAccTraceSchema.setMoney(tMoney);
            mLCInsureAccTraceSchema.setMakeDate(PubFun.getCurrentDate());
            mLCInsureAccTraceSchema.setMakeTime(PubFun.getCurrentTime());
            mLCInsureAccTraceSchema.setModifyDate(PubFun.getCurrentDate());
            mLCInsureAccTraceSchema.setModifyTime(PubFun.getCurrentTime());
            mLCInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
            mLCInsureAccTraceSchema.setManageCom(mGlobalInput.ManageCom);
            tmpMap.put(mLCInsureAccTraceSchema, "DELETE&INSERT");
        }
        return true;
    }

    //计算历次交费的本息和
    private double getPremIntSum() {
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if (!tLLRegisterDB.getInfo()) {
            CError.buildErr(this, "立案信息查询失败");
            return 0.0;
        }
        double tReturnGet = 0.0;
        LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
        LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
        tLJAPayPersonDB.setPolNo(mLCPolSchema.getPolNo());
        tLJAPayPersonDB.setPayType("ZC");
        tLJAPayPersonSet = tLJAPayPersonDB.query();
//    LMLoanDB tLMLoanDB = new LMLoanDB();
//    tLMLoanDB.setRiskCode(mLCPolSchema.getRiskCode());
//    tLMLoanDB.getInfo();
        double rate = 0.025;
        AccountManage tAccountManage = new AccountManage();
        for (int i = 1; i <= tLJAPayPersonSet.size(); i++) {
            double interest = 0.0;
            LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
            tLJAPayPersonSchema = tLJAPayPersonSet.get(i);
            interest = tAccountManage.getInterest(
                    tLJAPayPersonSchema.getSumActuPayMoney(),
                    tLJAPayPersonSchema.getConfDate(),
                    tLLRegisterDB.getAccidentDate(), rate, "1", "2", "D");
            tReturnGet += tLJAPayPersonSchema.getSumActuPayMoney() + interest;
        }
        return tReturnGet;
    }

    /**
     * 套餐编码
     * @return String
     */
    private String getRiskWrapCode(){
    String tRiskWrapCode = "";
        String tSQL = "SELECT DISTINCT riskwrapcode FROM LCRiskDutyWrap WHERE prtno='"+mLCPolSchema.getPrtNo()
                    + "' AND riskcode='"+mLCPolSchema.getRiskCode()+"' WITH UR";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tSQL);
        if (tSSRS.getMaxRow()>0){
            tRiskWrapCode = tSSRS.GetText(1,1);
        }
        return tRiskWrapCode;
    }


    public static void main(String[] args) {
        String caseno = "C0000060821000003";
        ClaimCalAutoBL aClaimCalAutoBL = new ClaimCalAutoBL();

        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setCaseNo(caseno);
        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86";
        mGlobalInput.ComCode = "86";
        mGlobalInput.Operator = "cm0005";
        VData aVData = new VData();
        aVData.addElement(mGlobalInput);
        aVData.addElement(tLLCaseSchema);
        aClaimCalAutoBL.submitData(aVData, "Cal");
    }
}
