package com.sinosoft.lis.llcase;

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 民生人寿业务系统</p>
 * <p>Description: 立案阶段的保单效力终止业务处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @version 1.0
 */
public class RestartPolPowerBL
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData ;
  private VData mResult = new VData();
  private String mOperate;
  private LDSysTraceSet tLDSysTraceSet = new LDSysTraceSet();//向bls传递数据
  private LDSysTraceSet mLDSysTraceSet = new LDSysTraceSet();//接收ui数据
  private LDSysTraceSchema mLDSysTraceSchema = new LDSysTraceSchema();//接收ui数据

  private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
  private LLCasePolicySet mLLCasePolicySet = new LLCasePolicySet();
  private LLCasePolicySchema mLLCasePolicySchema = new LLCasePolicySchema();
  private GlobalInput mG = new GlobalInput();
  int n;  //界面上所要处理的保单的个数；

  public RestartPolPowerBL() {}

  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;
    mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    mLLCasePolicySet = (LLCasePolicySet)cInputData.getObjectByObjectName("LLCasePolicySet",0);
    mLLCaseSchema = (LLCaseSchema)cInputData.getObjectByObjectName("LLCaseSchema",0);
    n = mLLCasePolicySet.size();
    System.out.println("共有"+n+"张保单要进行效力终止处理！");
    if(!CaseStation())
      return false;
    if(!dealData())
      return false;
    return true;
  }

  private boolean dealData()
  {
    for(int i=1;i<=n;i++)
    {
      System.out.println("i的数值是"+i);
      LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();//向bls传递数据
      mLLCasePolicySchema=mLLCasePolicySet.get(i);
      System.out.println("要进行保单效力终止操作的保单号码是"+mLLCasePolicySchema.getPolNo());
      LDSysTraceDB tLDSysTraceDB = new LDSysTraceDB();
      tLDSysTraceDB.setPolNo(mLLCasePolicySchema.getPolNo());
      System.out.println("保单号是"+mLLCasePolicySchema.getPolNo());
      mLDSysTraceSet=tLDSysTraceDB.query();
      int m = mLDSysTraceSet.size();
      System.out.println("bl中m的值是"+m);
      if(m==0)
      {
        this.mErrors.copyAllErrors(tLDSysTraceDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "RegisterUpdateBL";
        tError.functionName = "submitData";
        tError.errorMessage = "该案保单没有被终止，您不能进行保单恢复操作！！";
        this.mErrors .addOneError(tError) ;
        return false;
      }
      else
      {
        tLDSysTraceSchema.setSchema(mLDSysTraceSet.get(1));
        System.out.println("要进行恢复效力的保单号码是"+tLDSysTraceSchema.getPolNo());
        System.out.println("原来的日期和时间分别是"+tLDSysTraceSchema.getMakeDate());
        System.out.println("原来的日期和时间分别是"+tLDSysTraceSchema.getMakeTime());
        tLDSysTraceSchema.setOperator2(mG.Operator);
        tLDSysTraceSchema.setManageCom2(mG.ManageCom);
        tLDSysTraceSchema.setCreatePos2("立案阶段");
        tLDSysTraceSchema.setStartDate(PubFun.getCurrentDate());
        tLDSysTraceSchema.setModifyDate(PubFun.getCurrentDate());
        tLDSysTraceSchema.setModifyTime(PubFun.getCurrentTime());
        tLDSysTraceSchema.setValiFlag("0");
        tLDSysTraceSchema.setRemark("保单效力恢复！");
        tLDSysTraceSet.add(tLDSysTraceSchema);
        tLDSysTraceSet.set(i,tLDSysTraceSchema);
      }
    }
    mInputData.clear();
    mInputData.add(tLDSysTraceSet);

    EndPolPowerBLS tEndPolPowerBLS = new EndPolPowerBLS();
    System.out.println("Start EndPolPower BL Submit...");

    if (!tEndPolPowerBLS.submitData(mInputData,mOperate))
    {
      this.mErrors.copyAllErrors(tEndPolPowerBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "EndPolPowerBL";
      tError.functionName = "submitData";
      tError.errorMessage = "保单效力终止失败！";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  private boolean CaseStation()
  {
    LLCaseDB tLLCaseDB = new LLCaseDB();
    tLLCaseDB.setCaseNo(mLLCaseSchema.getCaseNo());
    if(!tLLCaseDB.getInfo())
    {
      this.mErrors.copyAllErrors(tLLCaseDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "LLCaseDB";
      tError.functionName = "submitData";
      tError.errorMessage = "该案件的分案号码不正确！";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    else
    {
      LLRegisterDB tLLRegisterDB = new LLRegisterDB();
      tLLRegisterDB.setRgtNo(tLLCaseDB.getRgtNo());
      if(!tLLRegisterDB.getInfo())
        return false;
      if(tLLRegisterDB.getClmState().equals("5"))
      {
        this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "RegisterUpdateBL";
        tError.functionName = "submitData";
        tError.errorMessage = "该案件正处于调查之中，您无法进行保单效力恢复操作！！";
        this.mErrors .addOneError(tError) ;
        return false;
      }
      if(tLLRegisterDB.getClmState().equals("2"))
      {
        this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "RegisterUpdateBL";
        tError.functionName = "submitData";
        tError.errorMessage = "该案件已经结案，您无法进行保单效力恢复操作！！";
        this.mErrors .addOneError(tError) ;
        return false;
      }
    }
    return true;
  }
}