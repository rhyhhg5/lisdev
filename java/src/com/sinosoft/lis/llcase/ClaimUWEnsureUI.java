package com.sinosoft.lis.llcase;

import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:理赔给付确认功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 */
public class ClaimUWEnsureUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public ClaimUWEnsureUI() {}

  public static void main(String[] args)
  {
    GlobalInput tG = new GlobalInput();
            tG.Operator = "001";
            tG.ComCode  = "001";
            String transact = "INSERT";
              LLClaimSchema tLLClaimSchema = new LLClaimSchema();
              tLLClaimSchema.setClmNo("86000020030520000005");
              System.out.println("GF_ensure.ClmNo==="+tLLClaimSchema.getClmNo());
              LLClaimUWMainSchema tLLClaimUWMainSchema = new LLClaimUWMainSchema();
            tLLClaimUWMainSchema.setClmNo("86000020030520000005");
            tLLClaimUWMainSchema.setRgtNo("00100020030510000014");
            tLLClaimUWMainSchema.setRemark("OK");

              ClaimUWEnsureUI tClaimUWEnsureUI = new ClaimUWEnsureUI();
              // 准备传输数据 VData
               VData tVData = new VData();
                      //此处需要根据实际情况修改
                       tVData.addElement(tLLClaimSchema);
                       tVData.addElement(tLLClaimUWMainSchema);
                       tVData.addElement(tG);
                       //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
                       System.out.println("add end");
   		tClaimUWEnsureUI.submitData(tVData,transact);


  }


  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    System.out.println("ui begin");
    this.mOperate = cOperate;
    System.out.println("operate end");
    ClaimUWEnsureBL tClaimUWEnsureBL = new ClaimUWEnsureBL();
    System.out.println("bl begin");
    if (tClaimUWEnsureBL.submitData(cInputData,mOperate) == false)
	{
  		// @@错误处理
      this.mErrors.copyAllErrors(tClaimUWEnsureBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimUWEnsureBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
	}
      mResult.clear();
      mResult = tClaimUWEnsureBL.getResult();
    return true;
  }
  public VData getResult()
  {
           return mResult;
  }
}