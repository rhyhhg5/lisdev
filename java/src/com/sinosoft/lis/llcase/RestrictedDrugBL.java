/*
 * @(#)LLCasePrescriptionBL.java	2015-10-29 
 *
 * Copyright 2005 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/*
 * <p>Title: Web业务系统</p>
 * <p>ClassName: RestrictedDrugBL</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft </p>
 * @author：zqs
 * @version：1.0
 * @CreateDate：2015-11-17
 */
public class RestrictedDrugBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private MMap map = new MMap();
    private String mOperate = "";

    //全局数据
    private GlobalInput mGlobalInput = new GlobalInput();
    //门诊限制或禁用药品
    LDCodeSet mLDCodeSet = new LDCodeSet();
    //LLFeeMainSchema mLLFeeMainSchema = new LLFeeMainSchema();

    private String mCodeName = "";
    //private String mRgtNo = "";
    //private String mMainFeeNo = "";

    public RestrictedDrugBL()
    {
    }

    public static void main(String[] args)
    {
    	/*LLCasePrescriptionBL tLLCasePrescriptionBL = new LLCasePrescriptionBL();
    	LLCasePrescriptionSchema tLLCasePrescriptionSchema   = new LLCasePrescriptionSchema();
    	  LLCasePrescriptionSet tLLCasePrescriptionSet=new LLCasePrescriptionSet();
    	  GlobalInput tG = new GlobalInput();
    	  
    	  VData tVData = new VData();

    	   //此处需要根据实际情况修改
    	   tVData.addElement(tLLCasePrescriptionSet);
    	   tVData.addElement(tG);
    	   
    	tLLCasePrescriptionSchema=new LLCasePrescriptionSchema();
     	  tLLCasePrescriptionSchema.setCaseNo("111111");
     	  tLLCasePrescriptionSchema.setRgtNo(strRgtNo);
     	  tLLCasePrescriptionSchema.setDiagnoseType(strDiagnoseType);
     	  tLLCasePrescriptionSchema.setDrugName(strDrugName);
     	  tLLCasePrescriptionSchema.setDrugQuantity(strDrugQuantity);
     	  tLLCasePrescriptionSchema.setTimesPerDay(strTimesPerDay);
     	  tLLCasePrescriptionSchema.setQuaPerTime(strQuaPerTime);
    	
    	tLLCasePrescriptionBL.submitData(tVData,transact);
    	*/
    	
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData 输入数据
     * @param cOperate String 操作类型
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        //得到输入数据
        if (!getInputData())
        {
            return false;
        }
         mOperate = cOperate;
        //检查数据合法性
        if (!checkInputData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData(cOperate))
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "RestrictedDrugBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有数据对象
     * @return boolean
     */
    private boolean getInputData()
    {
        System.out.println("getInputData()..."+mOperate);

        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLDCodeSet = (LDCodeSet) mInputData.getObjectByObjectName(
                "LDCodeSet", 0);
        
        mCodeName = (String) mInputData.getObjectByObjectName(
                "String", 0);
        
        System.out.println("BL层得到的set中的mCodeName的值"+mCodeName);
 
        //System.out.println("mLDCodeSet.size:" + mLDCodeSet.size());

        return true;
    }

    /**
     * 对输入数据做必要的校验
     * @return boolean
     */
    private boolean checkInputData()
    {
        System.out.println("checkInputData()..."+mOperate);
        try
        {
        	//在执行删除操作的时候传入code值校验
        	if (mOperate.equals("DELETE"))
            {
	        	/*LDCodeDB tLDCodeDB = new LDCodeDB();
	        	tLDCodeDB.setCodeType("RestrictedDrug");
	        	tLDCodeDB.setCode(mCodeName);
	        	LDCodeSet tLDCodeSet = tLDCodeDB.query();
	        	if (tLDCodeDB.getInfo() == false)
	            {
	                // @@错误处理
	                //mErrors.copyAllErrors(tLLCaseDB.mErrors);
	                CError tError = new CError();
	                tError.moduleName = "RestrictedDrugBL";
	                tError.functionName = "checkInputData";
	                tError.errorMessage = "门诊限制或禁用药品查询失败!" +
	                                      "限制药品名：" + mCodeName;
	                mErrors.addOneError(tError);
	                return false;
	            }*/
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "RestrictedDrugBL";
            tError.functionName = "checkInputData";
            tError.errorMessage = "在校验输入的数据时出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @param cOperate String 操作类型
     * @return boolean
     */
    private boolean dealData(String cOperate)
    {
        System.out.println("dealData()...cOperate"+cOperate);

        boolean tReturn = false;

        //保存录入
        if (cOperate.equals("INSERT"))
        {
            map.put(mLDCodeSet, "INSERT");
            mResult.add(mLDCodeSet);
            tReturn = true;
        }

        //修改录入
        if (cOperate.equals("UPDATE"))
        {
        }

        //删除录入
        if (cOperate.equals("DELETE"))
        {
            StringBuffer sbSql = new StringBuffer();
            sbSql
            .append(" DELETE FROM LDCode D ")
            .append(" WHERE D.CODETYPE = 'RestrictedDrug'")
            .append(" AND")
            .append(" D.CODENAME = '")
            .append(mCodeName)
            .append("'");

            map.put(sbSql.toString(), "DELETE");

 //           mResult.add(mFeeMainSchema);
            tReturn = true;
        }

        return tReturn;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        System.out.println("prepareOutputData()...");

        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "RestrictedDrugBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 得到处理后的结果数据
     * @return boolean
     */
    public VData getResult()
    {
        return this.mResult;
    }

}
