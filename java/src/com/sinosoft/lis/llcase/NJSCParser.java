/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: 南京理赔磁盘导入的解析类，从一个XML数据流中解析出批次下分案信息</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author Xx
 * @version 6.0
*/

package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import org.apache.xpath.XPathAPI;
import org.apache.xpath.objects.XObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.sinosoft.lis.f1print.ExportExcelXML;

public class NJSCParser implements SIParser{

    //案件
    private static String PATH_INSURED = "ROW";

    // 下面是一些常量定义
    private String mRgtNo = "";
    private String mGrpContNo = "";
    private String mPath = "";
    private String[] Title = null;
    private String vtsName = "ClaimErrList.vts";
    private ListTable  FalseListTable = new ListTable();
    private GlobalInput mG = new GlobalInput();
    public CErrors mErrors = new CErrors();

    public NJSCParser() {
    }
    public boolean setGlobalInput(GlobalInput aG) {
        this.mG = aG;
        FalseListTable.setName("FAULT");
        return true;
    }

    public void setFormTitle(String[] cTitle,String cvtsName){
        Title = cTitle;
        vtsName = cvtsName;
    }

    /**
     * 解析案件结点(公用函数)
     * @param node Node
     * @return VData
     */
    public VData parseOneInsuredNode(Node node) {
        VData tReturn = new VData();
        NodeList nodeList = null;
        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_INSURED);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
            return null;
        }
        int nLength = nodeList == null ? 0 : nodeList.getLength();
        int FailureCount = 0;
        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeCase = nodeList.item(nIndex);
            if(!genCase(nodeCase)){
                System.out.println("导入这个人失败");
                FailureCount++;
                //写导出错误xml的函数
            }
        }
        String[] tErrorTitle = { "姓名", "医统号",
                         "身份证号码", "医院名称","医院编码","人员类别","门诊/住院", "帐单金额", "赔付金额",
                         "就诊时间（门诊）", "入院日期", "出院日期", "临床诊断",
                         "ICD编码","给付责任编码", "支付限额","错误原因"};
        
        String[] tErrorType = { "String", "String",
                "String", "String","String","String","String", "Number", "Number",
                "DateTime", "DateTime", "DateTime", "String",
                "String","String", "Number","String"};
        
        LLExportErrorList xmlexport = new LLExportErrorList(); //新建一个XmlExport的实例
        xmlexport.createDocument(mRgtNo);
        xmlexport.addListTable(FalseListTable, tErrorTitle, tErrorType);
        xmlexport.outputDocumentToFile(mPath, mRgtNo); //输出xml文档到文件
        String FailureNum = FailureCount+"";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("FailureNum",FailureNum);
        tReturn.clear();
//        tReturn.addElement(xmlexport);
        tReturn.add(tTransferData);
        return tReturn;
    }

    public boolean setParam(String aRgtNo,String aGrpContNo,String aPath){
        mRgtNo = aRgtNo;
        mGrpContNo = aGrpContNo;
        mPath = aPath;
        return true;
    }

    /**
     * 利用XPathAPI取得某个节点的节点值
     * @param node Node
     * @param strPath String
     * @return String
     */
    private static String parseNode(Node node, String strPath) {
        String strValue = "";
        try {
            XObject xobj = XPathAPI.eval(node, strPath);
            strValue = xobj == null ? "" : xobj.toString();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return strValue;
    }

    private boolean genCase(Node nodeCase){
        String aCustomerName = parseNode(nodeCase,"CustomerName");
        String aSecurityNo = parseNode(nodeCase,"SecurityNo");
        String aIDNo = parseNode(nodeCase,"IDNo");
        String hospitalName = ""+parseNode(nodeCase,"HospitalName");
        String hospitalCode = ""+parseNode(nodeCase,"HospitalCode");
        String tInsuredStat = "" + parseNode(nodeCase,"InsuredStat");
       // String hospitalName_Sql = "select hospitname from ldhospital where hospitcode=' "+hospitalCode+"' with ur";
        String strSQL0 ="select distinct a.insuredno,a.sex,a.birthday,a.idno,insuredstat,a.name,idtype  ";
        String sqlpart1="from lcinsured a where ";
        String sqlpart2="from lbinsured a where ";
        String wherePart = " (a.othidno='"+aSecurityNo+"' or a.idno='"
                           +aIDNo+"') and a.GrpContNo = '"+mGrpContNo
                         +"' and a.name='"+aCustomerName+"'";
        String strSQL=strSQL0+sqlpart1+ wherePart+" union "+strSQL0+sqlpart2+wherePart;
        ExeSQL texesql = new ExeSQL();
        SSRS tssrs = new SSRS();
        tssrs = texesql.execSQL(strSQL);
        System.out.println(strSQL);
        if(tssrs==null||tssrs.getMaxRow()<=0){
            String ErrMessage = "团单"+mGrpContNo+"下没有社保号为"+aSecurityNo
                                +"或身份证号为"+aIDNo+"的被保险人";
            getFalseList(nodeCase,ErrMessage);
            return false;
        }
        if (aCustomerName.equals("") || aCustomerName.equals("null")) {
            aCustomerName = tssrs.GetText(1, 6);
        } else if (!tssrs.GetText(1, 6).equals(aCustomerName)) {
            String ErrMessage =  "该客户投保时姓名为" + tssrs.GetText(1, 6)
                         + ",与导入姓名" + aCustomerName + "不符！";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }
        
        /*   String tIdtype=tssrs.GetText(1, 7).equals("null") ? "" :
            tssrs.GetText(1, 7);;//获得证件类型
             if(tIdtype.equals("0"))//是身份证时，对身份证号进行校验
        {
	        String tId=tssrs.GetText(1, 4).equals("null") ? "" :
	            tssrs.GetText(1, 4);;//获得身份证号
	        if(!PubFun.checkIDNo(tId))//校验数据库里存储的身份证号，by zzh 20100925
	        {	
	        	String ErrMessage ="社保号为"+aSecurityNo
	            +"或身份证号为"+aIDNo+"的客户身份证号错误，请先做客户资料变更！";
	            getFalseList(nodeCase,ErrMessage);
	        	return false;
	        }
	           
        }*/
        
        String aFeeType = parseNode(nodeCase,"FeeType");
//        String tfeedate = parseNode(nodeCase, "FeeDate");
        String tfeedate = FormatDate(parseNode(nodeCase, "FeeDate"));//by zzh 0930
        if (aFeeType.equals("1") && (tfeedate == null ||tfeedate.equals(""))) {
            String ErrMessage = "帐单日期为空。";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }
        if (aFeeType.equals("2") && (parseNode(nodeCase, "HospStartDate") == null ||
                                     parseNode(nodeCase, "HospStartDate").equals(""))) {
            String ErrMessage = "入院日期为空。";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }
        if(tfeedate == null ||tfeedate.equals("")){
//            tfeedate = parseNode(nodeCase, "HospStartDate");
            tfeedate = FormatDate(parseNode(nodeCase, "HospStartDate"));//by zzh 0930
        }
        if(parseNode(nodeCase,"Remnant")==null||
           parseNode(nodeCase,"Remnant").equals("")){
            String ErrMessage = "赔付金额为空，无法进行理算。";
            getFalseList(nodeCase,ErrMessage);
            return false;
        }
        String insts = tssrs.GetText(1, 5).equals("null") ? "" :
            tssrs.GetText(1, 5);
        if (tInsuredStat.equals("") || tInsuredStat.equals("null")) {
            tInsuredStat = insts;
        } else if (!insts.equals("") && !insts.equals(tInsuredStat)) {
              String ErrMessage = "该客户投保时为" + insts
              + "状态,与本次导入" + tInsuredStat + "状态不符！";
              getFalseList(nodeCase, ErrMessage);
              return false;
        }
        String strCustomerSQL="select * from lpedorapp a,lpgrpedoritem b where a.edoracceptno in (select max(edorno) from lpdiskimport where  insuredno='"+aCustomerName+"' and grpcontno ='"+mGrpContNo+"') and a.edorstate !='0' and a.edoracceptno = b.edoracceptno and b.edortype ='ZT'";
        SSRS customertssrs = new SSRS();
        customertssrs = texesql.execSQL(strCustomerSQL);
        if(customertssrs.getMaxRow()>0){
            String ErrMessage ="该客户正在进行保全减人操作！";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }
        String result = LLCaseCommon.checkPerson(aCustomerName);
        if(!"".equals(result)){
            String ErrMessage =  "该客户正在进行保全操作,工单号为"+result+"！";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }
        String aCustomerNo = tssrs.GetText(1,1);
        String aSex = tssrs.GetText(1,2);
        String aBirthday = tssrs.GetText(1,3);
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
        LLSecurityReceiptSchema tLLSecRecSchema = new LLSecurityReceiptSchema();
        LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
        VData tVData = new VData();

        genSimClaimBL tgenSimClaimBL = new genSimClaimBL();

        tLLCaseSchema.setRgtType("1");

        LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
        tLLAppClaimReasonSchema.setReasonCode("02");        //原因代码
        tLLAppClaimReasonSchema.setReason("住院");                //申请原因
        tLLAppClaimReasonSchema.setCustomerNo(aCustomerNo);
        tLLAppClaimReasonSchema.setReasonType("0");

        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setRgtNo(mRgtNo);
        tLLCaseDB.setCustomerNo(aCustomerNo);
        LLCaseSet temLLCaseSet = tLLCaseDB.query();
        if(temLLCaseSet.size()>0){
            tLLCaseSchema.setUWState("7");
        }
        
        //modify by zhangyige 2012-07-09
        String aPrePaidFlag = "";
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if(tLLRegisterDB.getInfo())
		{
			aPrePaidFlag = tLLRegisterDB.getSchema().getPrePaidFlag();
		}
        tLLCaseSchema.setPrePaidFlag(aPrePaidFlag);

        tLLCaseSchema.setRgtState("03");//案件状态
        tLLCaseSchema.setCustomerName(aCustomerName);
        tLLCaseSchema.setCustomerNo(aCustomerNo);
        tLLCaseSchema.setCaseNo("");

        tLLCaseSchema.setRgtNo(mRgtNo);
        tLLCaseSchema.setSurveyFlag("0");
        tLLCaseSchema.setAccdentDesc("磁盘导入");
        tLLCaseSchema.setRgtState("03");//案件状态
        tLLCaseSchema.setCustBirthday(aBirthday);
        tLLCaseSchema.setCustomerSex(aSex);
        tLLCaseSchema.setIDType("0");
        if(parseNode(nodeCase,"IDNo")!=null
           &&!parseNode(nodeCase,"IDNo").equals(""))
            tLLCaseSchema.setIDNo(parseNode(nodeCase,"IDNo"));
        else
            tLLCaseSchema.setIDNo(tssrs.GetText(1,4));
        tLLCaseSchema.setOtherIDType("5");
        tLLCaseSchema.setOtherIDNo(aSecurityNo);
        tLLCaseSchema.setAccidentSite(mGrpContNo);
        tLLCaseSchema.setPhone(parseNode(nodeCase,"GetDutyCode"));
        tLLCaseSchema.setAccidentDate(tfeedate);
        if(tLLCaseSchema.getAccidentDate()==null)//by zzh 0930
        {
        	String ErrMessage =  "账单日期格式不对！";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }
        tLLCaseSchema.setSurveyFlag("0");
        
        String tAccSQL = "select 1 from lcgrppol a,lmriskapp b where a.riskcode=b.riskcode"
            + " and b.risktype3='7' and b.risktype <>'M'"
            + " and a.grpcontno='"+ mGrpContNo +"'";
        SSRS tAccSSRS = texesql.execSQL(tAccSQL);
        boolean tDiseaseFlag = true;
        if (tAccSSRS.getMaxRow()>0) {
        	tDiseaseFlag = false;
        }
        
        if (("".equals(hospitalCode)||"null".equals(hospitalCode))&&tDiseaseFlag) {
            String ErrMessage = "医院编码不能为空";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }
        if (("".equals(hospitalName)||"null".equals(hospitalName))&&tDiseaseFlag) {
            String ErrMessage = "医院名称不能为空";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }

        tLLFeeMainSchema.setRgtNo(mRgtNo);
        tLLFeeMainSchema.setCaseNo("");
        tLLFeeMainSchema.setCustomerNo(aCustomerNo);
        tLLFeeMainSchema.setCustomerName(aCustomerName);
        tLLFeeMainSchema.setCustomerSex(aSex);
        tLLFeeMainSchema.setInsuredStat(parseNode(nodeCase,"InsuredStat"));
        tLLFeeMainSchema.setReceiptNo("32000");
//        tLLFeeMainSchema.setFeeAtti("1");
        tLLFeeMainSchema.setSecurityNo(aSecurityNo);
        tLLFeeMainSchema.setFeeType(parseNode(nodeCase,"FeeType"));
        tLLFeeMainSchema.setFeeAtti("4");
        tLLFeeMainSchema.setFeeDate(FormatDate(parseNode(nodeCase,"FeeDate")));
        tLLFeeMainSchema.setHospitalCode(hospitalCode);
        tLLFeeMainSchema.setHospitalName(hospitalName);
        tLLFeeMainSchema.setHospStartDate(FormatDate(parseNode(nodeCase,"HospStartDate")));
        tLLFeeMainSchema.setHospEndDate(FormatDate(parseNode(nodeCase,"HospEndDate")));
        tLLFeeMainSchema.setFeeType(parseNode(nodeCase,"FeeType"));
        String tSumFee=parseNode(nodeCase,"TabFee");
        if (tSumFee==null||"".equals(tSumFee)||"null".equals(tSumFee)) {
            String ErrMessage = "总费用不能为空";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }
        tLLFeeMainSchema.setSumFee(parseNode(nodeCase,"TabFee"));
        tLLFeeMainSchema.setRemnant(parseNode(nodeCase,"Remnant"));
        tLLSecRecSchema.setRgtNo(mRgtNo);
        tLLSecRecSchema.setCaseNo("");
        tLLSecRecSchema.setApplyAmnt(parseNode(nodeCase,"TabFee"));

        String tDiseaseCode = "" + parseNode(nodeCase,"ICDCode");
        String tDiseaseName = "" + parseNode(nodeCase,"ICDName");
        System.out.println(tDiseaseCode);
        System.out.println(tDiseaseName);
        if (("".equals(tDiseaseCode)||"null".equals(tDiseaseCode))&&tDiseaseFlag) {
            String ErrMessage = "疾病编码不能为空";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }
        if (("".equals(tDiseaseName)||"null".equals(tDiseaseName))&&tDiseaseFlag) {
            String ErrMessage = "疾病名称不能为空";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }

        if (!"".equals(tDiseaseCode) && !"null".equals(tDiseaseCode)) {
            LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
            tLLCaseCureSchema.setDiseaseCode(tDiseaseCode);
            tLLCaseCureSchema.setDiseaseName(tDiseaseName);
            tLLCaseCureSchema.setHospitalCode(hospitalCode);
            tLLCaseCureSchema.setHospitalName(hospitalName);
            tLLCaseCureSchema.setMainDiseaseFlag("1");
            tLLCaseCureSet.add(tLLCaseCureSchema);
        }
        //保存案件时效
        LLSecurityReceiptSchema tSRSchema = new LLSecurityReceiptSchema();
        System.out.println("<--Star Submit VData-->");
        tVData.add(tLLAppClaimReasonSchema);
        tVData.add(tLLCaseSchema);
        tVData.add(tLLFeeMainSchema);
        tVData.add(tSRSchema);
        tVData.add(tLLCaseCureSet);

        tVData.add(mG);
        System.out.println("<--Into tSimpleClaimUI-->");
        if (!tgenSimClaimBL.submitData(tVData, "INSERT||MAIN")){
            CErrors tError = tgenSimClaimBL.mErrors;
            String ErrMessage = tError.getFirstError();
            getFalseList(nodeCase,ErrMessage);
            return false;
        }
        return true;
    }

    private boolean getFalseList(Node nodeCase,String ErrMessage){
        String[] strCol = new String[17];
        strCol[0] = parseNode(nodeCase,"CustomerName");
        strCol[1] = parseNode(nodeCase,"SecurityNo");
        strCol[2] = parseNode(nodeCase,"IDNo");
        strCol[3] = parseNode(nodeCase,"HospitalName");
        strCol[4] = parseNode(nodeCase,"HospitalCode");
        strCol[5] = parseNode(nodeCase,"InsuredStat");
        strCol[6] = parseNode(nodeCase,"FeeType");
        strCol[7] = parseNode(nodeCase,"TabFee");
        strCol[8] = parseNode(nodeCase,"Remnant");
        strCol[9] = FormatDate(parseNode(nodeCase,"FeeDate"));
        strCol[10] = FormatDate(parseNode(nodeCase,"HospStartDate"));
        strCol[11] = FormatDate(parseNode(nodeCase,"HospEndDate"));
        strCol[12] = parseNode(nodeCase,"ICDName");
        strCol[13] = parseNode(nodeCase,"ICDCode");
        strCol[14] = parseNode(nodeCase,"GetDutyCode");
        strCol[15] = parseNode(nodeCase,"PeakLine");
        strCol[16] = ErrMessage;
        System.out.println("ErrMessage:"+ErrMessage);
        FalseListTable.add(strCol);
        return true;
    }

    private String FormatDate(String aDate){
        String rDate = aDate;
        if(aDate.length()<8){
            int days = 0;
            try{
                days = Integer.parseInt(aDate)-2;
            }catch(Exception ex){
                return "";
            }
            rDate = PubFun.calDate("1900-1-1",days,"D",null);
        }
        return rDate;
    }
}
