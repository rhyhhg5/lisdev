package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class SubReportBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  /**  */
  private LLSubReportBL  mLLSubReportBL = new LLSubReportBL();
  private LLSubReportBLSet mLLSubReportBLSet = new LLSubReportBLSet();

  public SubReportBL() {}

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;

    //数据查询业务处理
    if (cOperate.equals("QUERY||MAIN"))
    {
      if(!queryData())
	return false;
      System.out.println("---queryData---");
    }
    // 数据操作业务处理
    if (cOperate.equals("INSERT"))
    {
      if (!dealData())
	return false;
      System.out.println("---dealData---");
    }
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealData()
  {
/*
    if (!mLLSubReportSet.get(1).getRptNo().equals("NULL"))
    {
      // 校验数据
      if (!checkData())
        return false;

      //准备给后台的数据
      prepareOutputData();
    }
*/
		//准备给后台的数据
    prepareOutputData();
    //数据提交
    SubReportBLS tSubReportBLS = new SubReportBLS();
    System.out.println("Start SubReport BL Submit...");
    if (!tSubReportBLS.submitData(mInputData,mOperate))
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tSubReportBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "SubReportBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    // 暂交费查询条件
    if (mOperate.equals("QUERY||MAIN"))
    {
      mLLSubReportBL.setSchema((LLSubReportSchema)cInputData.getObjectByObjectName("LLSubReportSchema",0));
    }
    if (mOperate.equals("INSERT"))
    {
      mLLSubReportBLSet.set((LLSubReportSet)cInputData.getObjectByObjectName("LLSubReportSet",0));
    }

    return true;
  }

  /**
   * 查询符合条件的暂交费信息
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean queryData()
  {

    LLSubReportDB tLLSubReportDB = new LLSubReportDB();
    LLSubReportSchema tLLSubReportSchema=new LLSubReportSchema();
    tLLSubReportSchema=mLLSubReportBL.getSchema();
    tLLSubReportDB.setSchema(tLLSubReportSchema);
    mLLSubReportBLSet.set(tLLSubReportDB.query());
    if (tLLSubReportDB.mErrors.needDealError() == true)
    {
      // @@错误处理
	  this.mErrors.copyAllErrors(tLLSubReportDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "LLSubReportBL";
      tError.functionName = "queryData";
      tError.errorMessage = "保单查询失败!";
      this.mErrors.addOneError(tError);
      mLLSubReportBLSet.clear();
      return false;
    }
    if (mLLSubReportBLSet.size() == 0)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LLSubReportBL";
      tError.functionName = "queryData";
      tError.errorMessage = "未找到相关数据!";
      this.mErrors.addOneError(tError);
      mLLSubReportBLSet.clear();
      return false;
	}

	mResult.clear();
	mResult.add( mLLSubReportBLSet );

	return true;
  }

  /**
   * 校验传入的暂交费收据号是否合法
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkData()
  {

	boolean flag = true;
/*	int n = mLLSubReportSet.size();
	for (int i = 1; i <= n; i++)
	{
	  	// 是否存在此收据
	  	LLSubReportSchema tLLSubReportSchema = mLLSubReportSet.get(i);
	  	LLSubReportDB tLLSubReportDB = new LLSubReportDB();
	  	//tLLSubReportDB.setTempFeeNo( tLJTempFeeSchema.getTempFeeNo() );
	  	//tLLRpeortDB.setTempFeeType( "1" );

		if (tLLSubReportDB.getInfo() == false)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLSubReportBL";
			tError.functionName = "checkData";
			tError.errorMessage = "没有此暂交费收据!(" + tLLSubReportDB.getTempFeeNo().trim() + ")";
			this.mErrors .addOneError(tError) ;
			flag = false;
			continue;
		}

		// 收据是否已经核销
		if (tLLSubReportDB.getConfFlag() != null && tLLSubReportDB.getConfFlag().equals( "1" ))
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLSubReportBL";
			tError.functionName = "checkData";
			tError.errorMessage = "此暂交费收据已经核销，不能关联到此投保单!(" + tLLSubReportDB.getTempFeeNo().trim() + ")";
			this.mErrors .addOneError(tError) ;
			flag = false;
			continue;
		}

		// 收据是否已经被别的投保单关联
		if ((tLLSubReportDB.getOtherNo() != null && tLLSubReportDB.getOtherNo().trim().length() > 0 && !tLJTempFeeDB.getOtherNo().trim().equals(mPolNo))
		|| (tLLSubReportDB.getOtherNoType() != null && tLLSubReportDB.getOtherNoType().trim().length() > 0 && !tLJTempFeeDB.getOtherNoType().trim().equals("0")))
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLSubReportBL";
			tError.functionName = "checkData";
			tError.errorMessage = "此暂交费收据已经关联到别的投保单，此处不能使用!(" + tLLSubReportDB.getTempFeeNo().trim() + ")";
			this.mErrors .addOneError(tError) ;
			flag = false;
			continue;
		}

		tLLSubReportSchema.setSchema( tLLSubReportDB );
		mLLSubReportSet.set( i, tLLSubReportSchema );
	} // end of for
*/
	return flag;
  }

  /**
  * 准备需要保存的数据
  */
  private void prepareOutputData()
  {

    int n = mLLSubReportBLSet.size();
    for (int i = 1; i <= n; i++)
    {
      LLSubReportSchema tLLSubReportSchema = mLLSubReportBLSet.get(i);
      tLLSubReportSchema.setMngCom("ddd");
      tLLSubReportSchema.setOperator("ddd");
      tLLSubReportSchema.setMakeDate(PubFun.getCurrentDate());
      tLLSubReportSchema.setMakeTime(PubFun.getCurrentTime());
      tLLSubReportSchema.setModifyDate(PubFun.getCurrentDate());
      tLLSubReportSchema.setModifyTime(PubFun.getCurrentTime());
      mLLSubReportBLSet.set(i,tLLSubReportSchema);
    }

    mInputData.clear();
    mInputData.add(mLLSubReportBLSet);
    mResult.clear();
    mResult.add(mLLSubReportBLSet);

  }

}