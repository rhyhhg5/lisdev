package com.sinosoft.lis.llcase;

import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title:理赔核赔规则 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: SinoSoft </p>
 * @author MN
 * @version 1.0
 */

public class LLUWCheckBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    MMap mMap = new MMap();
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperator;
    private String mManageCom;

    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private LLCaseRelaSet mLLCaseRelaSet = new LLCaseRelaSet();
    private LLSubReportSet mLLSubReportSet = new LLSubReportSet();
    private LLClaimDetailSet mLLClaimDetailSet = new LLClaimDetailSet();
    private LLClaimDetailSet mLLClaimDetailGetDutySet = new LLClaimDetailSet();
    private LCDutySet mLCDutySet = new LCDutySet();
    private LCPolSet mLCPolSet = new LCPolSet();
    private LCRiskDutyWrapSet mLCRiskDutyWrapSet = new LCRiskDutyWrapSet();
    //private LCPolSet mShareAmntSet = new LCPolSet();
    private LCDutySet mShareAmntDetailSet = new LCDutySet();

    private String mCalCode; //计算编码
    private double mValue;
    //计算要素
    private String mContNo = "";
    private String mPolNo = "";
    private String mRiskCode = "";
    private String mDutyCode = "";
    private String mGetDutyCode = "";
    private String mGetDutyKind = "";
    private String mRiskWrapCode = "";
    private String mSumPay = "";
    private String mRealPay = "";
    private String mARealPay = "";//2713补充A本次赔付
    private String mShareAmnt = "";
    private String mLGSumPay="";
    //#2781 xuyunpeng start
    private String AnnualClaimSa = "";
    private String WholeLifeClaimSa="";
    private String mHealthDuty="";
    //#2781 xuyunpeng end
    
    //#2011 团体保险单个被保险人多险种共用保额契约录入功能开发
    //#3561 健康金福 star
    private String mShareSumPay = ""; //共用历史赔付
    private String mShareRealPay = ""; //共用本次赔付
    private String mComSumPay = ""; //一般医疗历史
    private String mComRealPay = ""; //一般医疗本次赔付
    //#3561 健康金福 star
    private String mGrpContNo = "";
    private String mContPlanCode="";
    
    //#3661 《健康守望补充医疗保险（B款）》 start
    private String mPrem="";  
    private String mCustomername="";
    private String mGetDutyName="";
    //#3661 《健康守望补充医疗保险（B款）》 end
    /** 共用保额校验标记 */
    private int mShareAmntFlag;
    
    public LLUWCheckBL() {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate)) {
            return false;
        }
        System.out.println("---LLUWCheckBL getInputData---");

        if (!checkData()) {
            return false;
        }
        

        if (!dealData()) {
            return false;
        }
        System.out.println("---LLUWCheckBL dealData END---");

        //准备给后台的数据
        if (prepareOutputData()) {
            return false;
        }
        System.out.println("Start  Submit...");

        mResult.clear();
        mResult.add(mMap);

        return true;
    }


    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData() {
        //获得给付责任
        getGetDuty();
        //获得责任
        getDuty();
        //获得险种
        //#2011 团体保险单个被保险人多险种共用保额契约录入功能开发，对险种层额外分类，区分是否存在“共用保额”的险种
        getRiskCode();
        //获得套餐
        getWrap();

        LMUWSet tLMUWSetAll = null; //所有核赔规则

        //校验给付责任的核赔规则
        for (int i = 1; i <= mLLClaimDetailGetDutySet.size(); i++) {
        	LLClaimDetailSchema tLLClaimDetailGetDutySchema = mLLClaimDetailGetDutySet.get(i);

            tLMUWSetAll = checkGetDuty(tLLClaimDetailGetDutySchema);

            if (tLMUWSetAll == null || tLMUWSetAll.size() == 0) {
                continue; //无核赔规则则置标志为通过
            } else {
                for (int j = 1; j <= tLMUWSetAll.size(); j++) {
                    //取计算编码
                    LMUWSchema tLMUWSchema = new LMUWSchema();
                    tLMUWSchema = tLMUWSetAll.get(j);
                    mCalCode = tLMUWSchema.getCalCode();
                    if (uwGetDuty(tLLClaimDetailGetDutySchema) >= 1) {
                        buildError("dealData", parseUWResult(tLMUWSchema));
                        return false;
                    }
                   
                }
            }

        }
        
        //校验责任的核赔规则  C1100130614000001
        for (int i = 1; i <= mLCDutySet.size(); i++) {
            LCDutySchema tLCDutySchema = mLCDutySet.get(i);

            tLMUWSetAll = checkDuty(tLCDutySchema);

            if (tLMUWSetAll == null || tLMUWSetAll.size() == 0) {
                continue; //无核赔规则则置标志为通过
            } else{
         		  for (int j = 1; j <= tLMUWSetAll.size(); j++) {
                      //取计算编码
                      LMUWSchema tLMUWSchema = new LMUWSchema();
                      tLMUWSchema = tLMUWSetAll.get(j);
                      mCalCode = tLMUWSchema.getCalCode();
                      //判断是不是个单续保保单
               	   String tXbao="select 1 from lcpol a,lbpol b "
               		   +" where a.prtno=b.prtno and a.grpcontno='00000000000000000000' "
               		   +" and a.polno='"+tLCDutySchema.getPolNo()+"' "
               		   +" union "
               		   +"select 1 from lcpol a,lbpol b "
               		   +" where a.prtno=b.prtno and a.grpcontno='00000000000000000000'"
               		   +" and b.polno='"+tLCDutySchema.getPolNo()+"' "
               		   +" with ur";
               	   ExeSQL tExeSQL=new ExeSQL();
               	   SSRS tXbaoS=tExeSQL.execSQL(tXbao);
               	   System.out.println(tXbaoS.getMaxRow());
                      if(mCalCode.equals("LP0007")&&tXbaoS.getMaxRow() > 0){  
                    	 
                    	  
                    	  //本次实赔金额
                       	  double tRealPay=0.0;
                          for(int k=1;k<=mLLClaimDetailSet.size();k++){
                           	   LLClaimDetailSchema tLLClaimDetailSchema=mLLClaimDetailSet.get(k);
                           	   if(tLLClaimDetailSchema.getPolNo().equals(tLCDutySchema.getPolNo())
                           			   &&tLLClaimDetailSchema.getDutyCode().equals(tLCDutySchema.getDutyCode())){
                           		   tRealPay+=tLLClaimDetailSchema.getRealPay();
                           	   }
                          }
                              //责任有效日期
                       	   String tDate=" select lp.Cvalidate ,ld.enddate,ld.amnt"
                       		           +" from lcpol lp,lcduty ld "
                       		           +" where ld.polno='"+tLCDutySchema.getPolNo()+"' "
                       		           +" and ld.polno=lp.polno "
                       		           +" and ld.dutycode='"+tLCDutySchema.getDutyCode()+"' "
                       		           +" union "
                       		           +" select lp.Cvalidate ,ld.enddate,ld.amnt"
                       		           +" from lbpol lp,lbduty ld "
                       		           +" where ld.polno='"+tLCDutySchema.getPolNo()+"' "
                       		           +" and ld.polno=lp.polno "
                       		           +" and ld.dutycode='"+tLCDutySchema.getDutyCode()+"' "
                       		           +" with ur ";
                       	   SSRS tDateS=tExeSQL.execSQL(tDate);
                       	   tLCDutySchema.setAmnt(Double.parseDouble(tDateS.GetText(1, 3)));
                              //判断是不是续保之前的保单
                       	   String tXbaoD="select a.polno from lcpol a,lbpol b "
                       		   +" where a.prtno=b.prtno and b.polno='"+tLCDutySchema.getPolNo()+"' "
                       		   +" with ur";
                       	   SSRS tXbaoDS=tExeSQL.execSQL(tXbaoD);
                       	   if(tXbaoDS.getMaxRow() > 0 ){
                       		   tLCDutySchema.setPolNo(tXbaoDS.GetText(1, 1));  
                       	   }
                       	   //#2925 续保的保单无法获取到申诉纠错中申诉拒付的实付金额
                          	   //历史赔付金额
                              String tMoney=" select nvl(sum(a.realpay),0)  "
                                           +" from llclaimdetail a,llcase b "
                                           +" where a.caseno=b.caseno "
                                           +" and b.rgtstate in ('09','10','11','12') "
                                           +" and a.polno='"+tLCDutySchema.getPolNo()+"' "
                                           +" and a.dutycode='"+tLCDutySchema.getDutyCode()+"' "
                                           +" and a.caseno in "
                                           +" ( "
                                           +" select c.caseno " 
                                           +" from llcaserela c,llsubreport d "
                                           +" where c.caseno=a.caseno "
                                           +" and d.subrptno=c.subrptno "
                                           +" and d.accdate between '"+tDateS.GetText(1, 1)+"' and '"+tDateS.GetText(1, 2)+"'"
                                           +" ) with ur ";
                              SSRS tMoneyS=tExeSQL.execSQL(tMoney);
                              double tSumM=Double.parseDouble(tMoneyS.GetText(1, 1)); 
                              
                           // # 2781 **start**
                        	  //判断是否是税优健管责任
                        	 String aSQL = "select distinct riskcode from llclaimdetail where caseno='"+ mLLCaseSchema.getCaseNo() +"'" ;
                        	 ExeSQL aExeSQL = new ExeSQL();
                        	 SSRS aSSRS = aExeSQL.execSQL(aSQL);
                        	 if(aSSRS.getMaxRow()>0){
                        		 String aRiskCode = aSSRS.GetText(1, 1);
                        		 if(checkSyPoint(aRiskCode)){
                        		 		//查询是否是健管责任
                                    	mHealthDuty=String.valueOf(getHealthDuty(aRiskCode));
                                    	
                        		 	}
                        	 }
                 
                              if(tLCDutySchema.getAmnt()>0&&tSumM+tRealPay>tLCDutySchema.getAmnt() && !mHealthDuty.equals("0")){
                            	  checkShareAmntNew(tLCDutySchema.getContNo());
                            	  if(mShareAmntFlag == 2){//共用保额校验失败
                         			  return false;
                         		  }else if(mShareAmntFlag == 0){
                         			  String tDutyName="select dutyname from lmduty where dutycode='"+tLCDutySchema.getDutyCode()+"' with ur";
                         			  String tErr="保单："+tLCDutySchema.getContNo()+",责任："+tExeSQL.execSQL(tDutyName).GetText(1, 1)
                              	               +"下保额："+tLCDutySchema.getAmnt()+"。本次赔付："+tRealPay
                              	               +",历史赔付："+tSumM+",已超保额！";
                              	      buildError("dealData",tErr);
                              	      return false;
                         		  }
                            	  
                              } 
                              // # 2781 **end**
                      }else{
                    	   if (uwDuty(tLCDutySchema) == 1) {//若责任核赔失败，进行共用保额层次的校验
                    		  checkShareAmntNew(tLCDutySchema.getContNo());
                        	  if(mShareAmntFlag == 2){//共用保额校验失败
                     			  return false;
                     		  }else if(mShareAmntFlag == 0){
                     			  buildError("dealData", parseUWResult(tLMUWSchema));
                                  return false;
                     		  }                              
                          } 
                      }                                       
         		  }                
              }
         }

        //校验共用保额的核赔规则
        //取消此部分校验
//        for (int i = 1; i <= mShareAmntSet.size(); i++) {
//            LCPolSchema tLCPolSchema = mShareAmntSet.get(i);
//
//            tLMUWSetAll = checkShareAmnt(tLCPolSchema);
//
//            if (tLMUWSetAll == null || tLMUWSetAll.size() == 0) {
//                continue; //无核赔规则则置标志为通过
//            } else {
//                for (int j = 1; j <= tLMUWSetAll.size(); j++) {
//                    //取计算编码
//                    LMUWSchema tLMUWSchema = new LMUWSchema();
//                    tLMUWSchema = tLMUWSetAll.get(j);
//                    mCalCode = tLMUWSchema.getCalCode();
//                    if (uwShareAmnt(tLCPolSchema) == 1) {
//                        buildError("dealData", parseUWResult(tLMUWSchema));
//                        return false;
//                    }
//                }
//            }
//
//        }

        //校验险种核赔规则
        for (int i = 1; i <= mLCPolSet.size(); i++) {
            LCPolSchema tLCPolSchema = mLCPolSet.get(i);

            tLMUWSetAll = checkRiskCode(tLCPolSchema);

            if (tLMUWSetAll == null || tLMUWSetAll.size() == 0) {
                continue; //无核赔规则则置标志为通过
            } else {            	
          		 for (int j = 1; j <= tLMUWSetAll.size(); j++) {
                     //取计算编码
                     LMUWSchema tLMUWSchema = new LMUWSchema();
                     tLMUWSchema = tLMUWSetAll.get(j);
                     mCalCode = tLMUWSchema.getCalCode();
                     //判断是不是续保保单
                     String tXbao="select 1 from lcpol a,lbpol b "
                 		   +" where a.prtno=b.prtno and a.grpcontno='00000000000000000000' "
                 		   +" and a.polno='"+tLCPolSchema.getPolNo()+"' "
                 		   +" union "
                 		   +" select 1 from lcpol a,lbpol b "
                 		   +" where a.prtno=b.prtno and a.grpcontno='00000000000000000000'"
                 		   +" and b.polno='"+tLCPolSchema.getPolNo()+"' "
                 		   +" with ur";
              	   ExeSQL tExeSQL=new ExeSQL();
              	   SSRS tXbaoS=tExeSQL.execSQL(tXbao);
                     if(mCalCode.equals("LP0008")&& tXbaoS.getMaxRow() > 0 ){
     
                           //本次实赔金额
                      	   double tRealPay=0.0;
                             for(int k=1;k<=mLLClaimDetailSet.size();k++){
                          	   LLClaimDetailSchema tLLClaimDetailSchema=mLLClaimDetailSet.get(k);
                          	   if(tLLClaimDetailSchema.getPolNo().equals(tLCPolSchema.getPolNo())){
                          		   tRealPay+=tLLClaimDetailSchema.getRealPay();
                          	   }
                             }
                             //险种有效日期
                      	   String tDate=" select lp.Cvalidate ,lp.enddate,lp.amnt,lp.riskcode"
                      		           +" from lcpol lp "
                      		           +" where lp.polno='"+tLCPolSchema.getPolNo()+"' "
                      		           +" union "
                      		           +" select lp.Cvalidate ,lp.enddate,lp.amnt,lp.riskcode"
                      		           +" from lbpol lp"
                      		           +" where lp.polno='"+tLCPolSchema.getPolNo()+"' "
                      		           +" with ur ";
                      	   SSRS tDateS=tExeSQL.execSQL(tDate);
                      	   tLCPolSchema.setAmnt(Double.parseDouble(tDateS.GetText(1, 3)));
                      	   tLCPolSchema.setRiskCode(tDateS.GetText(1, 4));
                      	   //判断是不是续保之前的保单
                      	   String tXbaoD="select a.polno from lcpol a,lbpol b "
                      		   +" where a.prtno=b.prtno and b.polno='"+tLCPolSchema.getPolNo()+"' "
                      		   +" with ur";
                      	   SSRS tXbaoDS =tExeSQL.execSQL(tXbaoD);
                      	   if(tXbaoDS.getMaxRow() > 0 ){
                      		   tLCPolSchema.setPolNo(tXbaoDS.GetText(1, 1));  
                      	   }
                      	 //#2925 续保的保单无法获取到申诉纠错中申诉拒付的实付金额
                         	   //历史赔付金额
                             String tMoney=" select nvl(sum(a.realpay),0)  "
                                          +" from llclaimdetail a,llcase b "
                                          +" where a.caseno=b.caseno "
                                          +" and b.rgtstate in ('09','10','11','12') "
                                          +" and a.polno='"+tLCPolSchema.getPolNo()+"' "
                                          +" and a.caseno in "
                                          +" ( "
                                          +" select c.caseno " 
                                          +" from llcaserela c,llsubreport d "
                                          +" where c.caseno=a.caseno "
                                          +" and d.subrptno=c.subrptno "
                                          +" and d.accdate between '"+tDateS.GetText(1, 1)+"' and '"+tDateS.GetText(1, 2)+"'"
                                          +" ) with ur ";
                             SSRS tMoneyS=tExeSQL.execSQL(tMoney);                            
                             double tSumM=Double.parseDouble(tMoneyS.GetText(1, 1));
                             
                             if(tLCPolSchema.getAmnt()>0&&tSumM+tRealPay>tLCPolSchema.getAmnt()){
                            	 checkShareAmntNew(tLCPolSchema.getContNo());
                            	 if(mShareAmntFlag == 2){//共用保额校验失败
                        			 return false;
                        		 }else if(mShareAmntFlag == 0){
                        		     String tRiskN="select riskname from lmriskapp where riskcode='"+tLCPolSchema.getRiskCode()+"' with ur";
                                	 String tErr="保单："+tLCPolSchema.getContNo()+",险种："+tExeSQL.execSQL(tRiskN).GetText(1, 1)
                                	               +"下保额："+tLCPolSchema.getAmnt()+"。本次赔付："+tRealPay
                                	               +",历史赔付："+tSumM+",已超保额！";
                                	 buildError("dealData",tErr);
                                     return false;
                        		 } 
                          	   
                             }
                               
                     }else{                   	 
                    	 if (uwRiskCode(tLCPolSchema) == 1) {//若险种核赔失败，进行共用保额层次的校验
                    		 checkShareAmntNew(tLCPolSchema.getContNo());
                    		 if(mShareAmntFlag == 2){//共用保额校验失败
                    			 return false;
                    		 }else if(mShareAmntFlag == 0){
                    			 buildError("dealData", parseUWResult(tLMUWSchema));
                                 return false;
                    		 }                         
                         }
                     }                                     
          		 }                
            }
        }

        //校验套餐下核赔规则
        for (int i = 1; i <= mLCRiskDutyWrapSet.size(); i++) {
            LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = mLCRiskDutyWrapSet.get(
                    i);

            //准备算法，获取某险种的所有核赔规则的集合
            tLMUWSetAll = checkWraps(tLCRiskDutyWrapSchema);

            if (tLMUWSetAll == null || tLMUWSetAll.size() == 0) {
                continue; //无核赔规则则置标志为通过
            } else {
                for (int j = 1; j <= tLMUWSetAll.size(); j++) {
                    //取计算编码
                    LMUWSchema tLMUWSchema = new LMUWSchema();
                    tLMUWSchema = tLMUWSetAll.get(j);
                    mCalCode = tLMUWSchema.getCalCode();
                    if (uwWrap(tLMUWSchema.getRiskCode(), tLCRiskDutyWrapSchema) ==
                        1) {
                        buildError("dealData", parseUWResult(tLMUWSchema));
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * 准备返回前台统一存储数据
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        return false;
    }

    /**
     * 校验业务数据
     * @return
     */
    private boolean checkData() {
    	// #3207 管理式补充医疗保险（A B款）以及补充团体医疗保险（A款）赔付身故责任之后 该整个保单终止，申诉纠错除外
    	 for (int i=1;i<=mLLClaimDetailSet.size();i++){
    		LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
    		tLLClaimDetailSchema=mLLClaimDetailSet.get(i);
    		String tCaseNo=tLLClaimDetailSchema.getCaseNo();
    		if(tCaseNo.substring(0,1).equals("S") || tCaseNo.substring(0,1).equals("R")){
    			return true;
    		}else{
	    		String tRiskcode=tLLClaimDetailSchema.getRiskCode();
	    		String tContNo=tLLClaimDetailSchema.getContNo();
	    		String tPolNo=tLLClaimDetailSchema.getPolNo();
	    		String endsql="select codename, code1 from ldcode1 where code='"+tRiskcode+"' "
	    				+ " and codetype='lpdutyend' with ur";
	    		ExeSQL aExeSQL=new ExeSQL();
	    		SSRS tsql =aExeSQL.execSQL(endsql);
	    		if(tsql.getMaxRow()>0){
	    			String sql13="select  l.getdutycode from llclaimdetail l ,llcase c where l.caseno=c.caseno " 
							+" and l.contno='"+tContNo+"' and l.riskcode='"+tRiskcode+"' and l.getdutykind='"+tsql.GetText(1, 1)+"' " 
							+" and l.polno='"+tPolNo+"' and  c.rgtstate in ('11','12') with ur"; 
	    			String tGetDutyCode=aExeSQL.getOneValue(sql13);
	    			if(tGetDutyCode!=null && !"".equals(tGetDutyCode)){
	    				if(tGetDutyCode.equals(tsql.GetText(1,2))){
	    					 String tRiskN="select riskname from lmriskapp where riskcode='"+tRiskcode+"' with ur";
	    					 String triskName=aExeSQL.getOneValue(tRiskN);
	                    	 String tErr="保单："+tContNo+"下的险种："+triskName+"疾病身故保险金责任终止不能再次理赔！";
	                    	 buildError("checkData",tErr);
	                    	 return false;
	    				}
	    			}
	    		  }
	    		
	    	   } 
    	}
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData, String cOperate) {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput.setSchema((GlobalInput) cInputData.
                               getObjectByObjectName(
                                       "GlobalInput", 0));

        mLLCaseSchema.setSchema((LLCaseSchema) cInputData.getObjectByObjectName(
                "LLCaseSchema", 0));
        
        mLLCaseRelaSet.set((LLCaseRelaSet) cInputData.getObjectByObjectName(
                "LLCaseRelaSet", 0));
        
        mLLSubReportSet.set((LLSubReportSet) cInputData.getObjectByObjectName(
                "LLSubReportSet", 0));

        if (mLLCaseSchema == null) {
            buildError("getInputData", "传输全局公共数据失败!");
            return false;
        }
        
        if (mLLCaseRelaSet == null ) {
        	buildError("getInputData", "传输全局公共数据LLCaseRela失败!");
            return false;
        }
        
        if (mLLSubReportSet == null) {
        	buildError("getInputData", "传输全局公共数据LLSubReportSet失败!");
            return false;
        }

        if (mGlobalInput == null) {
            buildError("getInputData", "传输全局公共数据失败!");
            return false;
        }

        //获得操作员编码
        mOperator = mGlobalInput.Operator;
        if (mOperator == null || mOperator.trim().equals("")) {
            buildError("getInputData", "传输全局公共数据失败!");
            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
        if (mManageCom == null || mManageCom.trim().equals("")) {
            buildError("getInputData", "传输全局公共数据失败!");
            return false;
        }

        mLLClaimDetailSet.add((LLClaimDetailSet) cInputData.
                              getObjectByObjectName(
                                      "LLClaimDetailSet", 0));
        if (mLLClaimDetailSet == null) {
            buildError("getInputData", "获取理算数据失败!");
            return false;
        }
        return true;
    }


    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLUWCheckBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 根据理算保存数据获取需要核赔的给付责任
     */
    private void getGetDuty() {
        outer:
         for (int i = 1; i <= mLLClaimDetailSet.size(); i++) 
         {
	            LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
	            tLLClaimDetailSchema = mLLClaimDetailSet.get(i);
	            if ("5".equals(tLLClaimDetailSchema.getGiveType())||
	            	"3".equals(tLLClaimDetailSchema.getGiveType())||
	            	("1".equals(tLLClaimDetailSchema.getGiveType())&&"07".equals(tLLClaimDetailSchema.getGiveReason()))) {
	                continue outer;
	            }
	            for (int j = 1; j <= mLLClaimDetailGetDutySet.size(); j++) 
	            {
	            	LLClaimDetailSchema tLLClaimDetailGetDutySchema = new LLClaimDetailSchema();
	                tLLClaimDetailGetDutySchema = mLLClaimDetailGetDutySet.get(j);
	                if (tLLClaimDetailSchema.getGetDutyCode().equals(
	                        tLLClaimDetailGetDutySchema.getGetDutyCode()) &&
	                    tLLClaimDetailSchema.getGetDutyKind().equals(
	                            tLLClaimDetailGetDutySchema.getGetDutyKind())) {
	                    continue outer;
	                }
	            }
	            LLClaimDetailSchema tLLClaimDetailGetDutySchema = new LLClaimDetailSchema();
	            tLLClaimDetailGetDutySchema.setGetDutyCode(tLLClaimDetailSchema.getGetDutyCode());
	            tLLClaimDetailGetDutySchema.setGetDutyKind(tLLClaimDetailSchema.getGetDutyKind());
	            tLLClaimDetailGetDutySchema.setContNo(tLLClaimDetailSchema.getContNo());
	            tLLClaimDetailGetDutySchema.setPolNo(tLLClaimDetailSchema.getPolNo());
	            //#2781  xuyunpeng add 续保支持，获得险种，判断税优 start
	            tLLClaimDetailGetDutySchema.setRiskCode(tLLClaimDetailSchema.getRiskCode());
	         	//#2781  xuyunpeng add 续保支持，获得险种，判断税优 end
	            // 
	            mLLClaimDetailGetDutySet.add(tLLClaimDetailGetDutySchema);
        }
    }

    /**
     * 根据理算保存数据获取需要核赔的责任
     */
    private void getDuty() {
        outer:
                for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
            LLClaimDetailSchema tLLClaimDetailSchema = new
                    LLClaimDetailSchema();
            tLLClaimDetailSchema = mLLClaimDetailSet.get(i);
            if ("5".equals(tLLClaimDetailSchema.getGiveType()) ||
                "3".equals(tLLClaimDetailSchema.getGiveType())||
            	("1".equals(tLLClaimDetailSchema.getGiveType())&&"07".equals(tLLClaimDetailSchema.getGiveReason()))) {
                continue outer;
            }
            for (int j = 1; j <= mLCDutySet.size(); j++) {
                LCDutySchema tLCDutySchema = new LCDutySchema();
                tLCDutySchema = mLCDutySet.get(j);
                if (tLLClaimDetailSchema.getDutyCode().equals(tLCDutySchema.
                        getDutyCode())) {
                    continue outer;
                }
            }
            LCDutySchema tLCDutySchema = new LCDutySchema();
            tLCDutySchema.setContNo(tLLClaimDetailSchema.getContNo());
            tLCDutySchema.setPolNo(tLLClaimDetailSchema.getPolNo());
            tLCDutySchema.setDutyCode(tLLClaimDetailSchema.getDutyCode());
            mLCDutySet.add(tLCDutySchema);
        }
    }

    /**
     * 根据理算保存数据获取需要核赔的险种
     * 如果有共用保额的情况，不再判断险种级别
     * #2011 团体保险单个被保险人多险种共用保额契约录入功能开发
     * 1）首先取消原有的共用保额判断
     * 2）在获取险种过程中，均存放于mLCPolSet
     * 3）新的共用保额校验，在责任层、险种层判断失败后进行
     */
    private void getRiskCode() {
        outer:
                for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
            LLClaimDetailSchema tLLClaimDetailSchema = new
                    LLClaimDetailSchema();
            tLLClaimDetailSchema = mLLClaimDetailSet.get(i);
            if ("5".equals(tLLClaimDetailSchema.getGiveType()) ||
                "3".equals(tLLClaimDetailSchema.getGiveType())||
            	("1".equals(tLLClaimDetailSchema.getGiveType())&&"07".equals(tLLClaimDetailSchema.getGiveReason()))) {
                continue outer;
            }

//            for (int m = 1; m <= mShareAmntSet.size(); m++) {
//                LCPolSchema tLCPolSchema = mShareAmntSet.get(m);
//                if (tLLClaimDetailSchema.getRiskCode().equals(tLCPolSchema.
//                        getRiskCode()) &&
//                    tLLClaimDetailSchema.getPolNo().
//                    equals(tLCPolSchema.getPolNo())) {
//                    continue outer;
//                }
//            }

            for (int j = 1; j <= mLCPolSet.size(); j++) {
                LCPolSchema tLCPolSchema = new LCPolSchema();
                tLCPolSchema = mLCPolSet.get(j);
                if (tLLClaimDetailSchema.getRiskCode().equals(tLCPolSchema.
                        getRiskCode())) {
                    continue outer;
                }
            }
            
            LCPolSchema tLCPolSchema = new LCPolSchema();
            tLCPolSchema.setContNo(tLLClaimDetailSchema.getContNo());
            tLCPolSchema.setPolNo(tLLClaimDetailSchema.getPolNo());
            tLCPolSchema.setRiskCode(tLLClaimDetailSchema.getRiskCode());
            mLCPolSet.add(tLCPolSchema);

//            String tShareAmntSQL =
//                    "select a.grpcontno,a.contno,a.riskcode,a.polno,b.dutycode,"
//                    + " b.calfactorvalue "
//                    + " from lcpol a,lccontplandutyparam b "
//                    + " where a.grpcontno=b.grpcontno "
//                    + " and a.grppolno=b.grppolno and a.riskcode=b.riskcode "
//                    + " and a.contplancode=b.contplancode "
//                    +
//                    " and b.calfactor='ShareAmnt' and b.calfactorvalue is not null "
//                    + " and b.calfactorvalue != '' "
//                    + " and a.polno='" + tLLClaimDetailSchema.getPolNo()
//                    + "' union all"
//                    +
//                    " select a.grpcontno,a.contno,a.riskcode,a.polno,b.dutycode,"
//                    + " b.calfactorvalue "
//                    + " from lbpol a,lccontplandutyparam b "
//                    + " where a.grpcontno=b.grpcontno "
//                    + " and a.grppolno=b.grppolno and a.riskcode=b.riskcode "
//                    + " and a.contplancode=b.contplancode "
//                    +
//                    " and b.calfactor='ShareAmnt' and b.calfactorvalue is not null "
//                    + " and b.calfactorvalue != '' "
//                    + " and a.polno='" + tLLClaimDetailSchema.getPolNo()
//                    + "' union all"
//                    +
//                    " select a.grpcontno,a.contno,a.riskcode,a.polno,b.dutycode,"
//                    + " b.calfactorvalue "
//                    + " from lbpol a,lbcontplandutyparam b "
//                    + " where a.grpcontno=b.grpcontno "
//                    + " and a.grppolno=b.grppolno and a.riskcode=b.riskcode "
//                    + " and a.contplancode=b.contplancode "
//                    +
//                    " and b.calfactor='ShareAmnt' and b.calfactorvalue is not null "
//                    + " and b.calfactorvalue != '' "
//                    + " and a.polno='" + tLLClaimDetailSchema.getPolNo() + "'";
//            ExeSQL tExeSQL = new ExeSQL();
//            System.out.println(tShareAmntSQL);
//            SSRS tSADSSRS = tExeSQL.execSQL(tShareAmntSQL);
//            if (tSADSSRS.getMaxRow() > 0) {
//                LCPolSchema tLCPolSchema = new LCPolSchema();
//                tLCPolSchema.setGrpContNo(tSADSSRS.GetText(1, 1));
//                tLCPolSchema.setContNo(tSADSSRS.GetText(1, 2));
//                tLCPolSchema.setRiskCode(tSADSSRS.GetText(1, 3));
//                tLCPolSchema.setPolNo(tSADSSRS.GetText(1, 4));
//                mShareAmntSet.add(tLCPolSchema);
//
//                for (int n = 1; n <= tSADSSRS.getMaxRow(); n++) {
//                    LCDutySchema tLCDutySchema = new LCDutySchema();
//                    tLCDutySchema.setContNo(tSADSSRS.GetText(n, 2));
//                    tLCDutySchema.setPolNo(tSADSSRS.GetText(n, 4));
//                    tLCDutySchema.setDutyCode(tSADSSRS.GetText(n, 5));
//                    //String类型的，先存这
//                    tLCDutySchema.setStandbyFlag1(tSADSSRS.GetText(n, 6));
//                    mShareAmntDetailSet.add(tLCDutySchema);
//                }
//            } else {
//                LCPolSchema tLCPolSchema = new LCPolSchema();
//                tLCPolSchema.setContNo(tLLClaimDetailSchema.getContNo());
//                tLCPolSchema.setPolNo(tLLClaimDetailSchema.getPolNo());
//                tLCPolSchema.setRiskCode(tLLClaimDetailSchema.getRiskCode());
//                mLCPolSet.add(tLCPolSchema);
//            }
        }
    }

    /**
     * 根据理算保存数据获取需要核赔的套餐
     */
    private void getWrap() {
        ExeSQL tExeSQL = new ExeSQL();
        outer:
                for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
            LLClaimDetailSchema tLLClaimDetailSchema = new
                    LLClaimDetailSchema();
            tLLClaimDetailSchema = mLLClaimDetailSet.get(i);
            if ("5".equals(tLLClaimDetailSchema.getGiveType()) ||
                "3".equals(tLLClaimDetailSchema.getGiveType())||
            	("1".equals(tLLClaimDetailSchema.getGiveType())&&"07".equals(tLLClaimDetailSchema.getGiveReason()))) {
                continue outer;
            }
            String tSQL = "";
            if ("00000000000000000000".equals(tLLClaimDetailSchema.getGrpContNo())) {
                tSQL =
                        "select riskwrapcode from lcriskdutywrap where contno='" +
                        tLLClaimDetailSchema.getContNo() +
                        "' and riskcode='" +
                        tLLClaimDetailSchema.getRiskCode() +
                        "' fetch first 1 rows only";
            } else {
            	LCPolBL tLCPolBL = new LCPolBL();
            	tLCPolBL.setPolNo(tLLClaimDetailSchema.getPolNo());
            	if (!tLCPolBL.getInfo()) {
            		System.out.println(tLLClaimDetailSchema.getPolNo()+"险种查询失败");
            	}
            	LCPolSchema tLCPolSchema = tLCPolBL.getSchema();
            	tSQL = "select DISTINCT riskwrapcode from lccontplanrisk where riskwrapflag='Y' and grpcontno='"
            		 + tLCPolSchema.getGrpContNo()+"' and contplancode='"+tLCPolSchema.getContPlanCode()
            		 + "' and riskcode = '"+tLCPolSchema.getRiskCode()+"' ";
            }

            SSRS tSSRS = tExeSQL.execSQL(tSQL);
            if (tSSRS.getMaxRow() > 0) {
                String tWrapCode = tSSRS.GetText(1, 1);
                for (int j = 1; j <= mLCRiskDutyWrapSet.size(); j++) {
                    LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new
                            LCRiskDutyWrapSchema();
                    tLCRiskDutyWrapSchema = mLCRiskDutyWrapSet.get(j);
                    if (tWrapCode.equals(tLCRiskDutyWrapSchema.
                                         getRiskWrapCode()) &&
                        tLLClaimDetailSchema.getContNo().equals(
                                tLCRiskDutyWrapSchema.getContNo())) {
                        continue outer;
                    }
                }

                LCRiskDutyWrapSchema tLCRiskDutyWrapAddSchema = new
                        LCRiskDutyWrapSchema();
                tLCRiskDutyWrapAddSchema.setRiskWrapCode(tWrapCode);
                tLCRiskDutyWrapAddSchema.setContNo(tLLClaimDetailSchema.
                        getContNo());
                tLCRiskDutyWrapAddSchema.setInsuredNo(mLLCaseSchema.
                        getCustomerNo());
                mLCRiskDutyWrapSet.add(tLCRiskDutyWrapAddSchema);
            }
        }
    }

    /**
     * 获取给付责任核赔规则
     * @param aLCDutySchema LCDutySchema
     * @return LMUWSet
     */
    private LMUWSet checkGetDuty(LLClaimDetailSchema aLLClaimDetailGetDutySchema) {
        String tsql = "";
        //查询算法编码
        tsql = "select * from lmuw where riskcode = '" +
        		aLLClaimDetailGetDutySchema.getGetDutyCode().trim() +
               "' and relapoltype = 'LP' and uwtype = 'L' and uwresult='"+ 
                aLLClaimDetailGetDutySchema.getGetDutyKind() +"' order by calcode";

        LMUWDB tLMUWDB = new LMUWDB();

        LMUWSet tLMUWSet = tLMUWDB.executeQuery(tsql);
        if (tLMUWSet == null || tLMUWSet.size() <= 0) {
            tsql = "select * from lmuw where riskcode = '000000' and relapoltype='LP' and uwtype='L4' order by calcode";
            tLMUWSet = tLMUWDB.executeQuery(tsql);
        }
        if (tLMUWDB.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMUWDB.mErrors);
            buildError("checkGetDuty",
                       "获取给付责任" + aLLClaimDetailGetDutySchema.getGetDutyCode() +
                       "核赔规则失败");
            tLMUWSet.clear();
            return null;
        }

        return tLMUWSet;
    }

    
    /**
     * 获取责任核赔规则
     * @param aLCDutySchema LCDutySchema
     * @return LMUWSet
     */
    private LMUWSet checkDuty(LCDutySchema aLCDutySchema) {
        String tsql = "";
        //查询算法编码
        tsql = "select * from lmuw where riskcode = '" +
               aLCDutySchema.getDutyCode().trim() +
               "' and relapoltype = 'LP' and uwtype = 'L' order by calcode";

        LMUWDB tLMUWDB = new LMUWDB();

        LMUWSet tLMUWSet = tLMUWDB.executeQuery(tsql);
        if (tLMUWSet == null || tLMUWSet.size() <= 0) {
            tsql = "select * from lmuw where riskcode = '000000' and relapoltype='LP' and uwtype='L1' order by calcode";
            tLMUWSet = tLMUWDB.executeQuery(tsql);
        }
        if (tLMUWDB.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMUWDB.mErrors);
            buildError("checkDuty",
                       "获取责任" + aLCDutySchema.getDutyCode() +
                       "核赔规则失败");
            tLMUWSet.clear();
            return null;
        }

        return tLMUWSet;
    }

    /**
     * 获取共用保额核赔规则的计算编码
     * @param aLCPolSchema LCPolSchema
     * @return LMUWSet
     */
    private LMUWSet checkShareAmnt(String aPolNo) {
        String tsql = "";
        //查询算法编码，共用保额的核赔规则只有公用的，不存在特殊核赔规则
        tsql = "select * from lmuw where riskcode = '" +
        		aPolNo +
                "' and relapoltype = 'LP' and uwtype = 'LS' order by calcode";

        LMUWDB tLMUWDB = new LMUWDB();

        LMUWSet tLMUWSet = tLMUWDB.executeQuery(tsql);
        if (tLMUWSet == null || tLMUWSet.size() <= 0) {
            tsql = "select * from lmuw where riskcode = '000000' and relapoltype='LP' and uwtype='L3' order by calcode";
            tLMUWSet = tLMUWDB.executeQuery(tsql);
        }
        if (tLMUWDB.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMUWDB.mErrors);
            buildError("checkShareAmnt",
                       "获取险种" + aPolNo +
                       "共用保额核赔规则失败");
            tLMUWSet.clear();
            return null;
        }

        return tLMUWSet;
    }


    /**
     * 获取责任核赔规则
     * @param aLCDutySchema LCDutySchema
     * @return LMUWSet
     */
    private LMUWSet checkRiskCode(LCPolSchema aLCPolSchema) {
        String tsql = "";
        //查询算法编码
        tsql = "select * from lmuw where riskcode = '" +
               aLCPolSchema.getRiskCode().trim() +
               "' and relapoltype = 'LP' and uwtype = 'L' order by calcode";

        LMUWDB tLMUWDB = new LMUWDB();

        LMUWSet tLMUWSet = tLMUWDB.executeQuery(tsql);
        if (tLMUWSet == null || tLMUWSet.size() <= 0) {
            tsql = "select * from lmuw where riskcode = '000000' and relapoltype='LP' and uwtype='L2' order by calcode";
            tLMUWSet = tLMUWDB.executeQuery(tsql);
        }
        if (tLMUWDB.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMUWDB.mErrors);
            buildError("checkDuty",
                       "获取责任" + aLCPolSchema.getRiskCode() +
                       "核赔规则失败");
            tLMUWSet.clear();
            return null;
        }

        return tLMUWSet;
    }

    /**
     * 获取套餐核赔规则
     * @param aLCRiskDutyWrapSchema LCRiskDutyWrapSchema
     * @return LMUWSet
     */
    private LMUWSet checkWraps(LCRiskDutyWrapSchema aLCRiskDutyWrapSchema) {
        String tsql = "";
        //查询算法编码
        tsql = "select * from lmuw where riskcode = '" +
               aLCRiskDutyWrapSchema.getRiskWrapCode().trim() +
               "' and relapoltype = 'LP' and uwtype = 'L' order by calcode";

        LMUWDB tLMUWDB = new LMUWDB();

        LMUWSet tLMUWSet = tLMUWDB.executeQuery(tsql);

//        if (tLMUWSet == null || tLMUWSet.size() <= 0) {
//            tsql = "select * from lmuw where riskcode = '000000' and relapoltype='LP' and uwtype='L3' order by calcode";
//            tLMUWSet = tLMUWDB.executeQuery(tsql);
//        }

        if (tLMUWDB.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMUWDB.mErrors);
            buildError("checkWraps",
                       "获取套餐" + aLCRiskDutyWrapSchema.getRiskWrapCode() +
                       "核赔规则失败");
            tLMUWSet.clear();
            return null;
        }
        return tLMUWSet;
    }
    /**
     * 给付责任核赔
     * @param aDutyCode String
     * @param aLCDutySchema LCDutySchema
     * @return double
     */
    private double uwGetDuty(LLClaimDetailSchema aLLClaimDetailGetDutySchema) {
        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);
        //增加基本要素

        mGetDutyCode = aLLClaimDetailGetDutySchema.getGetDutyCode();
        mGetDutyKind = aLLClaimDetailGetDutySchema.getGetDutyKind();
        mCalculator.addBasicFactor("GetDutyCode", mGetDutyCode); 
        mCalculator.addBasicFactor("GetDutyKind", mGetDutyKind);
        mPolNo = aLLClaimDetailGetDutySchema.getPolNo();
        mCalculator.addBasicFactor("PolNo", mPolNo);
        mContNo = aLLClaimDetailGetDutySchema.getContNo();
        mCalculator.addBasicFactor("ContNo", mContNo);
        //老年关爱领取金额
        mLGSumPay = String.valueOf(getLGSumPay(aLLClaimDetailGetDutySchema));
        mCalculator.addBasicFactor("LGSumPay", mLGSumPay);
        //本次给付责任赔付
        mRealPay = String.valueOf(getGetDutyRealPay(aLLClaimDetailGetDutySchema));
        mCalculator.addBasicFactor("RealPay", mRealPay);        
        //历史给付责任赔付  #2781 xuyunpeng add 税优险在此处有续保处理 start 
        //修改了税优险再保时的历史赔付总额的求取逻辑
        mSumPay = String.valueOf(getGetDutySumPay(aLLClaimDetailGetDutySchema));
        
        mCalculator.addBasicFactor("SumPay", mSumPay);
        //份数
        mCalculator.addBasicFactor("Copys", getWrapCopys());
        // #3661 《健康守望补充医疗保险（B款）》**start**       
        ExeSQL tExeSQL1 = new ExeSQL();
        String tRiskcodeBSQL = "select 1 from ldcode where codetype ='jiankangB' and code='"+aLLClaimDetailGetDutySchema.getRiskCode()+"' and othersign='"+aLLClaimDetailGetDutySchema.getGetDutyKind()+"' with ur ";
        String tExeSQLQuery = tExeSQL1.getOneValue(tRiskcodeBSQL);
		//加险种
		if("1".equals(tExeSQLQuery)){
			String mCustomernameSQL = "select Customername from llcase where caseno in (select caseno from llclaimdetail where polno='"+mPolNo+"') ";
			mCustomername = tExeSQL1.getOneValue(mCustomernameSQL);
			String mGetDutyNameSQL = "select getdutyname from lmdutygetclm where getdutycode='"+mGetDutyCode+"' with ur ";
			mGetDutyName = tExeSQL1.getOneValue(mGetDutyNameSQL);
			mCalculator.addBasicFactor("GetDutyName", mGetDutyName);
			mCalculator.addBasicFactor("Customername", mCustomername);
			mPrem = String.valueOf(getRiskcodePrem(aLLClaimDetailGetDutySchema));
            mCalculator.addBasicFactor("Prem", mPrem);
        }       
        // #3661 《健康守望补充医疗保险（B款）》**end**
		
    //  2713补充团体医疗保险（A款） 个人保险金额**start**新增
        	//补充A给付责任层核赔规则:医疗保险金赔付限额以个人保险金额余额 为限，身故保险金以个人保险金额的1.05倍为限
        if("701201".equals(mGetDutyCode)||"701202".equals(mGetDutyCode)||"701203".equals(mGetDutyCode)||"701204".equals(mGetDutyCode)){
        	//补充A本次赔付
        	 String mAGetRealPay = String.valueOf(getAgetRealPay(aLLClaimDetailGetDutySchema));
             mCalculator.addBasicFactor("AGetRealPay", mAGetRealPay);
            //补充A医疗保险金额操作当日个人保险金额
            String tGetAccAmnt = String.valueOf(getGetAccAmnt(aLLClaimDetailGetDutySchema,PubFun.getCurrentDate()));
            mCalculator.addBasicFactor("GetAccAmnt", tGetAccAmnt);
            //补充A身故保险金额身故日个人保险金额
            if("701204".equals(mGetDutyCode)){
	            String tDeathAccAmnt = String.valueOf(getGetAccAmnt(aLLClaimDetailGetDutySchema,mLLCaseSchema.getDeathDate()));
	            mCalculator.addBasicFactor("DeathAccAmnt", tDeathAccAmnt);
            }
        }
        //  2713补充团体医疗保险（A款） 个人保险金额**end**新增
        // 3207 管理式补充A **start**
        if("162401".equals(aLLClaimDetailGetDutySchema.getRiskCode())){
        String sql="select codename from ldcode where codetype='lluwcheck' and code='"+mGetDutyCode+"'"
        		+ " and codename='"+mGetDutyKind+"' with ur";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tS=tExeSQL.execSQL(sql);
        if(tS.getMaxRow()>0){
        	if(!tS.GetText(1, 1).equals("501")){
        		//管理式补充A当日保险金额
        		String tAccAmnt=String.valueOf(getGetAccAmnt(aLLClaimDetailGetDutySchema,PubFun.getCurrentDate()));
        		 mCalculator.addBasicFactor("CurrentAmnt", tAccAmnt);
        	}else{
        		//管理式补充A身故日保险金额
        		if(mLLCaseSchema.getDeathDate()==null || "".equals(mLLCaseSchema.getDeathDate())){
        			buildError("uwGetDuty","当给付责任为身故时,获取死亡日期失败！");
        			return 1;
        		}
        		String tDeathAmnt=String.valueOf(getGetAccAmnt(aLLClaimDetailGetDutySchema,mLLCaseSchema.getDeathDate()));
       		    mCalculator.addBasicFactor("DeathAmnt", tDeathAmnt);
        	   }
        	}
        }
        // 3207 管理式补充A **end**
        
        // 3337 管理式补充B **start**
        if("162501".equals(aLLClaimDetailGetDutySchema.getRiskCode())){
        	
        	String tClaimNum= LLCaseCommon.getGetClaimNum(mPolNo);

        	String tSql="select codename from ldcode where codetype='lluwcheck' and code='"+mGetDutyCode+"'"
             	    + " and codename='"+mGetDutyKind+"' with ur";
        	ExeSQL tClaimExe= new ExeSQL();
        	SSRS tSR =tClaimExe.execSQL(tSql);
        	// 仅从公共账户扣
        	if(tSR.getMaxRow()>0){
        		// 管理式补充B当日账户保额
        		 if(!tSR.GetText(1, 1).equals("501")){
        			if("3".equals(tClaimNum)){
        			 //管理式补充B公共账户当日保额
        			 String tPoolAmntB=String.valueOf(getGetManageAmntB(aLLClaimDetailGetDutySchema,PubFun.getCurrentDate()));
            		 mCalculator.addBasicFactor("CurrentAmntB", tPoolAmntB); 
        			}else if("1".equals(tClaimNum)){
        			 //管理式补充B个人账户保额
           			 String tAccAmntB=String.valueOf(getGetAccAmnt(aLLClaimDetailGetDutySchema,PubFun.getCurrentDate()));
               		 mCalculator.addBasicFactor("CurrentAmntB", tAccAmntB);	
        			}else{
        			 //先个人账户后公共账户 
        				String tManageBPersonPoolAmnt=String.valueOf(getManageBGetPersonPoolAmnt(aLLClaimDetailGetDutySchema,PubFun.getCurrentDate()));
            			mCalculator.addBasicFactor("CurrentAmntB", tManageBPersonPoolAmnt);
        			}
        		}else{
           			//管理式补充B身故日个人账户保额
           			 String tDeathAmntB=String.valueOf(getGetAccAmnt(aLLClaimDetailGetDutySchema,mLLCaseSchema.getDeathDate()));
               		 mCalculator.addBasicFactor("DeathAmntB", tDeathAmntB);
        		    
        		 }
        	}
        }
        // 3337 管理式补充B **end**
        // # 1500 康乐人生 **start**
        if("LP0159".equals(mCalCode)){
        //出险时已保年期
         String mRgtYears =String.valueOf(getRgtYears(aLLClaimDetailGetDutySchema));
         mCalculator.addBasicFactor("RgtYears",mRgtYears);
        }
        // # 1500 康乐人生 **end**
        // #3561健康金福2017 star
        if("123701".equals(aLLClaimDetailGetDutySchema.getRiskCode())){
        	//共用保额下的实赔金额   一般医疗   癌症
        	mShareSumPay = String.valueOf(getShareGetDutySumPay(aLLClaimDetailGetDutySchema));
        	mCalculator.addBasicFactor("ShareSumPay", mShareSumPay);
        	
        	//共用保额下的本次赔付  一般医疗
        	mShareRealPay =  String.valueOf(getShareDutyRealPay(aLLClaimDetailGetDutySchema));
        	mCalculator.addBasicFactor("ShareRealPay", mShareRealPay);
        	//保单下一般医疗的本次赔付+历史赔付金额
        	mComSumPay = String.valueOf(getComSumPay(aLLClaimDetailGetDutySchema));
        	mCalculator.addBasicFactor("ComSumPay", mComSumPay);
        	mComRealPay = String.valueOf(getComRealPay(aLLClaimDetailGetDutySchema));
        	mCalculator.addBasicFactor("ComRealPay", mComRealPay);
        }
        // #3561健康金福2017 end
        String tStr = "";
        tStr = mCalculator.calculate();
        if (tStr == null || tStr.trim().equals("")) {
            mValue = 1;
        } else {
            mValue = Double.parseDouble(tStr);
        }

        System.out.println(mValue);
        return mValue;
    }

//  2713补充团体医疗保险（A款） 个人保险金额**start**新增
    /**
     * 给付责任层补充A个人保险金额
     * @param aLLClaimDetailGetDutySchema
     * @param 日期的格式字符串,日期格式为"yyyy-MM-dd"(操作当日或身故日)
     * @return
     */
    private String getGetAccAmnt(LLClaimDetailSchema aLLClaimDetailGetDutySchema,String tdate) {
    	//批导的案件缺乏LLCASESchema信息
    	String aCaseNo =getGetDutyCaseNum(aLLClaimDetailGetDutySchema);
    	double AccAmnt = LLCaseCommon.getAmntFromAcc(aCaseNo, aLLClaimDetailGetDutySchema.getPolNo(), tdate, "QR");

    	return String.valueOf(AccAmnt);
	}
    

	/**
     * 给付责任层医疗赔款本次赔付//2713
     * @param aLLClaimDetailGetDutySchema
     * @return
     */
	private double getAgetRealPay(LLClaimDetailSchema aLLClaimDetailGetDutySchema) {
		double ASumPay=0.0;
        String tPolNo = aLLClaimDetailGetDutySchema.getPolNo();
        for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
            LLClaimDetailSchema tLLClaimDetailSchema = new  LLClaimDetailSchema();
            tLLClaimDetailSchema = mLLClaimDetailSet.get(i);
            //医疗保险金 
            if (tPolNo.equals(tLLClaimDetailSchema.getPolNo()) && "000".equals(tLLClaimDetailSchema.getGetDutyKind())) {
            	ASumPay += tLLClaimDetailSchema.getRealPay();
            }
        }
        return Arith.round(ASumPay, 2);
	}
	//  2713补充团体医疗保险（A款） 个人保险金额**end**新增
	
	/**
     * 责任核赔
     * @param aDutyCode String
     * @param aLCDutySchema LCDutySchema
     * @return double
     */
    private double uwDuty(LCDutySchema aLCDutySchema) {
        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);
        //增加基本要素
        mDutyCode = aLCDutySchema.getDutyCode();
        mCalculator.addBasicFactor("DutyCode", mDutyCode);
        mPolNo = aLCDutySchema.getPolNo();
        mCalculator.addBasicFactor("PolNo", mPolNo);
        mContNo = aLCDutySchema.getContNo();
        mCalculator.addBasicFactor("ContNo", mContNo);   	 
        //历史赔付
        mSumPay = String.valueOf(getDutySumPay(aLCDutySchema));
        mCalculator.addBasicFactor("SumPay", mSumPay);
        //本次赔付
        mRealPay = String.valueOf(getDutyRealPay(aLCDutySchema));
        mCalculator.addBasicFactor("RealPay", mRealPay);
        // 保单关联事件数
        LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
        tLLClaimDetailSchema.setDutyCode(mDutyCode);
        tLLClaimDetailSchema.setPolNo(mPolNo);
        mCalculator.addBasicFactor("CaseRelaCount", String.valueOf(getCaseRelaCount(tLLClaimDetailSchema)));
        // 保单经过年度
        mCalculator.addBasicFactor("PolYears", getPolYears(mPolNo));
        //份数
        mCalculator.addBasicFactor("Copys", getWrapCopys());
//        是否享有社会医疗保险或公费医疗
        mCalculator.addBasicFactor("SecurityFlag", getSecurityFlag());
        //  2713补充团体医疗保险（A款） 个人保险金额**start**新增
        if("701003".equals(mDutyCode)||"701001".equals(mDutyCode)||"701002".equals(mDutyCode)){
        	//补充A历史赔付
        	String mASumPay  =  String.valueOf(getASumPay());
        	mCalculator.addBasicFactor("ASumPay", mASumPay);
        	//补充A本次赔付
        	 mARealPay = String.valueOf(getARealPay(aLCDutySchema));
             mCalculator.addBasicFactor("ARealPay", mARealPay);
            //最近一次理赔医疗保险金时的个人基本保险金额
            String tBasicAmnt = String.valueOf(getBasicAmnt(aLCDutySchema));
            mCalculator.addBasicFactor("BasicAmnt", tBasicAmnt);
        }
        //  2713补充团体医疗保险（A款） 个人保险金额**end**新增
        
        //3207 团体管理式补充医疗（A款）**start**
    
        String sql="select comcode,codename from ldcode where codetype='lluwcheck' and othersign = '"+mDutyCode+"' with ur";
        ExeSQL mExeSQL = new ExeSQL();
        SSRS tCode =mExeSQL.execSQL(sql);
        if(tCode.getMaxRow()>0){
        	 if(tCode.GetText(1,1).equals("162401")){
	        	if(!tCode.GetText(1,2).equals("501")){
	        		//管理式补充A当日保险金额
	        		String tAccAmnt=String.valueOf(getDutyAccAmnt(aLCDutySchema,PubFun.getCurrentDate()));
	        		 mCalculator.addBasicFactor("CurrentAmnt", tAccAmnt);
	        	 }else{
	        		String tDeathAmnt=String.valueOf(getDutyAccAmnt(aLCDutySchema,mLLCaseSchema.getDeathDate()));
	       		    mCalculator.addBasicFactor("DeathAmnt", tDeathAmnt);
	        		
	        	}
	        	//团体管理式补充医疗（A款）本次赔付
	        	 mARealPay = String.valueOf(getMRealPay(aLCDutySchema));
	             mCalculator.addBasicFactor("ARealPay", mARealPay);
	          // #3337 团体管理式补充医疗(B款)**start**
           }else if(tCode.GetText(1,1).equals("162501")){
			    	 String tClaimNum= LLCaseCommon.getGetClaimNum(mPolNo);
			    	 if(!tCode.GetText(1, 2).equals("501")){
		       			if("3".equals(tClaimNum)){
		       			 //管理式补充B公共账户当日保额
		       			 String tPoolAmntB=String.valueOf(getDutyManageAmntB(aLCDutySchema,PubFun.getCurrentDate()));
		           		 mCalculator.addBasicFactor("CurrentAmntB", tPoolAmntB); 
		       			}else if("1".equals(tClaimNum)){
		       			 //管理式补充B个人账户保额
		          			 String tAccAmntB=String.valueOf(getDutyAccAmnt(aLCDutySchema,PubFun.getCurrentDate()));
		              		 mCalculator.addBasicFactor("CurrentAmntB", tAccAmntB);	
		       			}else{
		       			 //先个人账户后公共账户 
		       				String tManageBPersonPoolAmnt=String.valueOf(getManageBDutyPersonPoolAmnt(aLCDutySchema,PubFun.getCurrentDate()));
		           			mCalculator.addBasicFactor("CurrentAmntB", tManageBPersonPoolAmnt);
		       			}
			    	 }else{
		      			//管理式补充B身故日个人账户保额
		      			 String tDeathAmntB=String.valueOf(getDutyAccAmnt(aLCDutySchema,mLLCaseSchema.getDeathDate()));
		          		 mCalculator.addBasicFactor("DeathAmntB", tDeathAmntB);
		   		    
			    	 }
			    	 	String mBRealPay = String.valueOf(getMRealPay(aLCDutySchema));
			    	 	mCalculator.addBasicFactor("BRealPay", mBRealPay);
           }
  
    }
        // #3337 团体管理式补充医疗(B款)**end**
        
        //2781 税优产品专用 **start**
        String tSQL ="select riskcode from llclaimdetail where caseno='"+ mLLCaseSchema.getCaseNo() +"' and dutycode='"+ mDutyCode +"'" ; 
        ExeSQL  tExeSQL = new ExeSQL();
        SSRS tSSRS =tExeSQL.execSQL(tSQL);
        if(tSSRS.getMaxRow()> 0){
        	String oRiskCode=tSSRS.GetText(1, 1);
        	if(checkSyPoint(oRiskCode)){
        		//查询是否是健管责任
            	mHealthDuty=String.valueOf(getHealthDuty(oRiskCode));
            	if(mHealthDuty.equals("0")){
            		return 0;
            	}
            }
        }
        //2781 税优产品专用 **end**
        String tStr = "";
        tStr = mCalculator.calculate();
        if (tStr == null || tStr.trim().equals("")) {
            mValue = 1;
        } else {
            mValue = Double.parseDouble(tStr);
        }

        System.out.println(mValue);
        return mValue;
    }
    //  2713补充团体医疗保险（A款） 个人保险金额**start**新增
    /**
     * 历史赔付金额  医疗保险金 合计补充团体医疗保险（A款）
     * @param aLCDutySchema
     * @return
     */
	  private String getASumPay() {
		  String ARealPay="0";
		  String sql = "select nvl(sum(b.realpay), 0) from llcase a, llclaimdetail b " +
		  		"where a.caseno = b.caseno " +
		  		"and a.rgtstate in ('11', '12') " +
		  		"and polno = '"+mPolNo+"' " +
		  		"and getdutykind = '000'";
		  ExeSQL exeSQL = new ExeSQL();
			String result = exeSQL.getOneValue(sql);
			if(result!=null && !"".equals(result)){
				ARealPay=result;
			}
		  
		return ARealPay;
	}
	  /**
	     * 本次金额  医疗保险金 合计补充团体医疗保险（A款）
	     * @param aLCDutySchema
	     * @return
	     */
	private double getARealPay(LCDutySchema tLCDutySchema) {
		double ASumPay=0.0;
        String tPolNo = tLCDutySchema.getPolNo();
        for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
            LLClaimDetailSchema tLLClaimDetailSchema = new  LLClaimDetailSchema();
            tLLClaimDetailSchema = mLLClaimDetailSet.get(i);
            //医疗保险金 
            if (tPolNo.equals(tLLClaimDetailSchema.getPolNo()) && "000".equals(tLLClaimDetailSchema.getGetDutyKind())) {
            	ASumPay += tLLClaimDetailSchema.getRealPay();
            }
        }

        return Arith.round(ASumPay, 2);
	}

	/**
     * 最近一次理赔医疗保险金时的个人基本保险金额 2713 补充团体医疗保险（A款）
     * @param aLCDutySchema
     * @return
     */
    
    private String getBasicAmnt(LCDutySchema aLCDutySchema) {
    	LCPolBL tLCPolBL = new LCPolBL();
		tLCPolBL.setPolNo(aLCDutySchema.getPolNo());
		if (!tLCPolBL.getInfo()) {
			System.out.println("保单查询失败");
			return null;
		}
		String cvalidate=tLCPolBL.getCValiDate();
		String endDate=PubFun.getCurrentDate();
		//若理赔过取最近 一次理赔时的
		String sql = "select paydate from lcinsureacctrace " +
				"where  moneytype='PK' and state='0' " +
				"and polno='"+aLCDutySchema.getPolNo()+"' " +
				"order by paydate desc fetch first row only";
		ExeSQL exeSQL = new ExeSQL();
		String result = exeSQL.getOneValue(sql);
		if(result!=null && !"".equals(result)){
			endDate=result;
		}
		//批次导入时，llcase暂无数据
		String aCaseNo = getDutyCaseNum(aLCDutySchema);
		double BasicAmnt =LLCaseCommon.getBasicAmnt(aCaseNo, aLCDutySchema.getPolNo(), cvalidate, endDate, "QR");
		return String.valueOf(BasicAmnt);

	}
    //  2713补充团体医疗保险（A款） 个人保险金额**end**新增
	/**
     * 稍微修改历史的校验共用保额是否成立的方法
     * @param aLCPolSchema
     * @return
     */
    private double uwShareAmntNew(String aContNo) {
        int tCount = 0;
        String tShareAmntValue = "";
        double tSumPay = 0.0;
        double tSumRealPay = 0.0;
        double tCalFactorValue = 0.0;

        for (int i = 1; i <= mShareAmntDetailSet.size(); i++) {
            LCDutySchema tLCDutySchema = mShareAmntDetailSet.get(i);

            if (aContNo.equals(tLCDutySchema.getContNo())) {
                if (tCount == 0) {
                    tShareAmntValue = tLCDutySchema.getStandbyFlag1();
                } else {
                    if (!tShareAmntValue.equals(tLCDutySchema.getStandbyFlag1())) {
                        buildError("uwShareAmnt", "共用保额要素录入不一致，请联系契约维护岗位进行维护");
                        return 1;
                    }
                }


                tCount++;

                try {
                    tCalFactorValue = Double.parseDouble(tLCDutySchema.
                            getStandbyFlag1());
                } catch (Exception ex) {
                    buildError("uwShareAmnt", "共用保额要素录入错误，请联系契约维护岗位进行维护");
                    return 1;
                }

                if (tCalFactorValue == 0) {
                    return 0;
                }

                //处理公共保额的情况，以契约录入的“共用计划”为主
                SynLCLBDutyBL tSynLCLBDutyBL = new SynLCLBDutyBL();
                tSynLCLBDutyBL.setDutyCode(tLCDutySchema.getDutyCode());
                tSynLCLBDutyBL.setPolNo(tLCDutySchema.getPolNo());
                if (!tSynLCLBDutyBL.getInfo()) {
                    buildError("uwShareAmnt", "险种下责任查询失败，请联系契约维护岗位进行维护");
                    return 1;
                }

                tLCDutySchema.setSchema(tSynLCLBDutyBL.getSchema());

//                if (tCalFactorValue > 0) {
//                    if (tLCDutySchema.getAmnt() > tCalFactorValue) {
//                        tCalFactorValue = tLCDutySchema.getAmnt();
//                    }
//                }
                //本次先统计险种下的共用保额
                //责任下历史赔付同样无误
                tSumPay += getDutySumPay(tLCDutySchema);
                //责任下本次赔款无问题
                tSumRealPay += getDutyRealPay(tLCDutySchema);
            }
        
        }

        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);
        //增加基本要素
        mCalculator.addBasicFactor("GrpContNo", mGrpContNo);
        mCalculator.addBasicFactor("ContPlanCode", mContPlanCode);
        
        mShareAmnt = String.valueOf(tCalFactorValue);
        mCalculator.addBasicFactor("ShareAmnt", mShareAmnt);
        //历史赔付
        mSumPay = String.valueOf(Arith.round(tSumPay, 2));
        mCalculator.addBasicFactor("SumPay", mSumPay);
        //本次赔付
        mRealPay = String.valueOf(Arith.round(tSumRealPay, 2));
        mCalculator.addBasicFactor("RealPay", mRealPay);
        //份数
        mCalculator.addBasicFactor("Copys", getWrapCopys());
        String tStr = "";
        tStr = mCalculator.calculate();
        if (tStr == null || tStr.trim().equals("")) {
            mValue = 1;
        } else {
            mValue = Double.parseDouble(tStr);
        }

        System.out.println(mValue);
        return mValue;
    }

    /**
     * 险种核赔
     * 0-表示通过核赔
     * 1-表示阻断核赔
     * @param aLCPolSchema LCPolSchema
     * @return double
     */
    private double uwRiskCode(LCPolSchema aLCPolSchema) {
        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);
        //增加基本要素
        mRiskCode = aLCPolSchema.getRiskCode();
        mCalculator.addBasicFactor("RiskCode", mRiskCode);
        mContNo = aLCPolSchema.getContNo();
        mCalculator.addBasicFactor("ContNo", mContNo);
        mPolNo = aLCPolSchema.getPolNo();
        mCalculator.addBasicFactor("PolNo", mPolNo);
        //历史赔付
        mSumPay = String.valueOf(getRiskCodeSumPay(aLCPolSchema));
        mCalculator.addBasicFactor("SumPay", mSumPay);
        //本次赔付
        mRealPay = String.valueOf(getRiskCodeRealPay(aLCPolSchema));
        mCalculator.addBasicFactor("RealPay", mRealPay);
        //份数
        mCalculator.addBasicFactor("Copys", getWrapCopys());
//      是否享有社会医疗保险或公费医疗
        mCalculator.addBasicFactor("SecurityFlag", getSecurityFlag());
        //2713补充团体医疗保险（A款） 个人保险金额    (3207修改)
        if("170501".equals(mRiskCode) ||"162401".equals(mRiskCode)){
            //个人保险金额余额
            String AccAmnt = String.valueOf(getAccAmnt(aLCPolSchema));
            mCalculator.addBasicFactor("AccAmnt", AccAmnt); 
        }
        //以上修改是  2713补充团体医疗保险（A款） 个人保险金额
        
        //#3337 团体管理式补充医疗保险(B款)**start**
        if("162501".equals(mRiskCode)){
        	String tDeath= String.valueOf(getDeathDate(mLLCaseSchema.getCaseNo()));
        	mCalculator.addBasicFactor("Death", tDeath);
        	String tClaimNum= LLCaseCommon.getGetClaimNum(mPolNo);
	    	 if(tDeath.equals("0")){
      			if("3".equals(tClaimNum)){
      			 //管理式补充B公共账户当日保额
      			 String tPoolAmntB=String.valueOf(getRiskManageAmntB(aLCPolSchema));
          		 mCalculator.addBasicFactor("CurrentAmntB", tPoolAmntB); 
          		 String tDeathAmntB="1";
         		 mCalculator.addBasicFactor("DeathAmntB", tDeathAmntB);
  		    
      			}else if("1".equals(tClaimNum)){
      			 //管理式补充B个人账户保额
         			 String tAccAmntB=String.valueOf(getAccAmnt(aLCPolSchema));
             		 mCalculator.addBasicFactor("CurrentAmntB", tAccAmntB);	
             		 String tDeathAmntB="1";
             		 mCalculator.addBasicFactor("DeathAmntB", tDeathAmntB);
      			}else{
      			 //先个人账户后公共账户 
      				String tManageBPersonPoolAmnt=String.valueOf(getManageBRiskPersonPoolAmnt(aLCPolSchema));
          			mCalculator.addBasicFactor("CurrentAmntB", tManageBPersonPoolAmnt);
          			 String tDeathAmntB="1";
             		 mCalculator.addBasicFactor("DeathAmntB", tDeathAmntB);
      			}
	    	 }else{
     			//管理式补充B身故日个人账户保额
     			 String tDeathAmntB=String.valueOf(getAccAmnt(aLCPolSchema));
         		 mCalculator.addBasicFactor("DeathAmntB", tDeathAmntB);
         		 String tAccAmntB="1";
        		 mCalculator.addBasicFactor("CurrentAmntB", tAccAmntB);
  		    
	    	 }
        }
        //#3337 团体管理式补充医疗保险(B款)**end**
        
        //#2781 xuyunpeng add 税优产品专用，查询中保信
        if(checkSyPoint(mRiskCode)){
        	System.out.println("税优险："+mRiskCode+"查询中保信累计赔付情况开始。。。");
        	SyZhBXQuery tSyZhBXQuery = new SyZhBXQuery();
        	//查询是否是健管责任
        	mHealthDuty=String.valueOf(getHealthDuty(mRiskCode));
        	if(mHealthDuty.equals("0")){
        		return 0;
        	}
        	
        	if(tSyZhBXQuery.submitData(mContNo, "ZBXPT")){
        		
        		AnnualClaimSa = String.valueOf(tSyZhBXQuery.getAnnualClaimSa());
        		
        		WholeLifeClaimSa = String.valueOf(tSyZhBXQuery.getWholeLifeClaimSa());
        		//年度累计赔付保险金
        		mCalculator.addBasicFactor("AnnualClaimSa", AnnualClaimSa);
        		//终身累计赔付保险金
        		mCalculator.addBasicFactor("WholeLifeClaimSa", WholeLifeClaimSa);
        	}else{
        		
        		this.mErrors.addOneError(tSyZhBXQuery.getCErrors().getFirstError());
        		return 1;
        	}
        }
        //#2781 xuyunpeng add 税优产品专用，查询中保信 end 
        String tStr = "";
        tStr = mCalculator.calculate();
        if (tStr == null || tStr.trim().equals("")) {
            mValue = 1;
        } else {
            mValue = Double.parseDouble(tStr);
        }
        System.out.println(mValue);
        return mValue;
    }
    /**
     * 获取个当日人保险金额  2713 补充A
     * @param aLCPolSchema
     * @return
     */
    private String getAccAmnt(LCPolSchema aLCPolSchema) {
    	String aCaseNo = getPolCaseNum(aLCPolSchema);
    	double AccAmnt =  LLCaseCommon.getAmntFromAcc(aCaseNo, aLCPolSchema.getPolNo(), PubFun.getCurrentDate(), "QR");
		return String.valueOf(AccAmnt);
	}

	/**
     * 新共用保额层级校验
     * 0-无共用保额，对险种层错误提示
     * 1-有共用保额，且通过，不再对险种层错误提示
     * 2-有共用保额，且不通过，对共用保额层错误提示
     * @param polSchema
     */
    private double checkShareAmntNew(String aContNo) {
    	//1.判断该险种是否有共用保额，无共用保额，返回0
    	mShareAmntFlag = 0;
    	mShareAmntDetailSet.clear();
    	
    	//需要先查询其团单号及该保单的保障计划
    	String tGrpContNoSQL = "select grpcontno,contplancode,riskcode from lcpol where contno='"+aContNo+"'" +
    			" union select grpcontno,contplancode,riskcode from lbpol where contno='"+aContNo+"' with ur";
    	ExeSQL tGrpExeSQL = new ExeSQL();
    	String aGrpContNo = "";
    	String aContPlanCode = "";
    	String aRiskCode = "";
    	SSRS tGrpSSRS = tGrpExeSQL.execSQL(tGrpContNoSQL);
    	if(tGrpSSRS.getMaxRow() > 0){
    		for (int n = 1; n <= tGrpSSRS.getMaxRow(); n++) {
    			aGrpContNo = tGrpSSRS.GetText(n, 1);
    			aContPlanCode = tGrpSSRS.GetText(n, 2);
    			aRiskCode += tGrpSSRS.GetText(n, 3)+"、";
    		}
    	}
    	aRiskCode = aRiskCode.substring(0, aRiskCode.lastIndexOf("、"));
    	System.out.println("aRiskCode:"+aRiskCode);

    	//查询lcpol/lbpol、lcgrpshareamnt 共用保额主表、lcgrpriskshareamnt 共用保额分表
    	String tShareAmntSQL =
    		"select a.contno,a.polno,c.dutycode,c.shareamnt"
    		+ " from lcpol a,lcgrpshareamnt b,lcgrpriskshareamnt c "
    		+ " where a.grpcontno=b.grpcontno and a.contplancode=b.contplancode and b.contplancode=c.contplancode "
    		+ " and b.grpcontno=c.grpcontno and b.shareplancode=c.shareplancode and a.riskcode=c.riskcode "
    		+ " and a.grpcontno='" + aGrpContNo+"' and b.contplancode='"+aContPlanCode+"' " 
    		+ " union all"
    		+
    		" select a.contno,a.polno,c.dutycode,c.shareamnt"
    		+ " from lbpol a,lcgrpshareamnt b,lcgrpriskshareamnt c "
    		+ " where a.grpcontno=b.grpcontno and a.contplancode=b.contplancode and b.contplancode=c.contplancode "
    		+ " and b.grpcontno=c.grpcontno and b.shareplancode=c.shareplancode and a.riskcode=c.riskcode "
    		+ " and a.grpcontno='" + aGrpContNo+"' and b.contplancode='"+aContPlanCode+"' " ;
    	
    	ExeSQL tExeSQL = new ExeSQL();
    	LMUWSet tLMUWSetAll = new LMUWSet();//存放核赔规则的临时容器
    	SSRS tSASSRS = tExeSQL.execSQL(tShareAmntSQL);
    	if (tSASSRS.getMaxRow() > 0) {//存在共用保额
    		//2.将查询出的责任共用保额计划存入临时数组
    		//将需要判断共用保额的责任整理到LCDuty
    		for (int n = 1; n <= tSASSRS.getMaxRow(); n++) {
    			LCDutySchema tLCDutySchema = new LCDutySchema();
    			//将共用保额的限额临时存储，String类型的
    			tLCDutySchema.setStandbyFlag1(tSASSRS.GetText(n, 4));
    			System.out.println("共用保额额度："+tSASSRS.GetText(n, 4));
    			tLCDutySchema.setContNo(tSASSRS.GetText(n, 1));
    			tLCDutySchema.setPolNo(tSASSRS.GetText(n, 2));
    			tLCDutySchema.setDutyCode(tSASSRS.GetText(n, 3));
    			mShareAmntDetailSet.add(tLCDutySchema);
    		}
    		//3.调用原来的方法，获取该险种的计算编码
    		tLMUWSetAll = checkShareAmnt(aContNo);
    		if (tLMUWSetAll == null || tLMUWSetAll.size() == 0) {
    			mShareAmntFlag = 1;
    			return mShareAmntFlag; //无核赔规则则置标志为通过，返回1
    		} else {
    			for (int j = 1; j <= tLMUWSetAll.size(); j++) {
    				//取计算编码
    				LMUWSchema tLMUWSchema = new LMUWSchema();
    				tLMUWSchema = tLMUWSetAll.get(j);
    				mCalCode = tLMUWSchema.getCalCode();
    				
    				//传入团体保单号、保障计划，为错误信息提供参数
    				mGrpContNo = aGrpContNo;
    				mContPlanCode = aContPlanCode;
    				mRiskCode = aRiskCode;
    				
    				//4.调用原来的方法，校验共用保额是否成立
    				if (uwShareAmntNew(aContNo) == 1) {//共用保额校验失败，返回2
                      	buildError("dealData", parseUWResult(tLMUWSchema));
                      	mShareAmntFlag = 2;
                      	return mShareAmntFlag;
    				}else{//校验通过
    					mShareAmntFlag = 1;
    				}
    			}
    		}
    	}
    	else {
    		return mShareAmntFlag;
    	}
    	return mShareAmntFlag;
	}

	/**
     * 套餐核赔
     * 输出：如果发生错误则返回false,否则返回true
     */
    private double uwWrap(String aWrapCode,
                          LCRiskDutyWrapSchema tLCRiskDutyWrapSchema) {
        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);
        //增加基本要素
        mRiskWrapCode = aWrapCode;
        mCalculator.addBasicFactor("RiskWrapCode", mRiskWrapCode);
        mContNo = tLCRiskDutyWrapSchema.getContNo();
        mCalculator.addBasicFactor("ContNo", mContNo);
        //历史赔付
        mSumPay = String.valueOf(getWrapSumPay(tLCRiskDutyWrapSchema));
        mCalculator.addBasicFactor("SumPay", mSumPay);
        //本次赔付
        mRealPay = String.valueOf(getWrapRealPay(tLCRiskDutyWrapSchema));
        mCalculator.addBasicFactor("RealPay", mRealPay);

        //份数
        mCalculator.addBasicFactor("Copys", getWrapCopys());
        
        String tStr = "";
        tStr = mCalculator.calculate();
        if (tStr == null || tStr.trim().equals("")) {
            mValue = 1;
        } else {
            mValue = Double.parseDouble(tStr);
        }

        System.out.println(mValue);
        return mValue;
    }

    /**
     * 套餐下历史赔款
     * @param tLCRiskDutyWrapSchema LCRiskDutyWrapSchema
     * @return double
     */
    private double getWrapSumPay(LCRiskDutyWrapSchema tLCRiskDutyWrapSchema) {
        ExeSQL tExeSQL = new ExeSQL();
        double sumPay = 0.0;
        String aWrapCode = tLCRiskDutyWrapSchema.getRiskWrapCode();
        String aContNo = tLCRiskDutyWrapSchema.getContNo();

        String tSQL =
                "select sum(realpay) from llclaimdetail a,llcase b where "
                + "a.caseno=b.caseno and a.contno='" +
                aContNo
                + "' and b.customerno='" + mLLCaseSchema.getCustomerNo()
                + "' and b.rgtstate in ('09','10','11','12') "
                + " and exists (select 1 from ldriskwrap  where riskcode=a.riskcode and riskwrapcode='" +
                aWrapCode + "') ";

        String tPay = tExeSQL.getOneValue(tSQL);
        if (tPay != null && tPay.length() > 0) {
            sumPay += Double.parseDouble(tPay);
        }

        return Arith.round(sumPay, 2);
    }

    /**
     * 套餐下本次赔款
     * @param tLCRiskDutyWrapSchema LCRiskDutyWrapSchema
     * @return double
     */
    private double getWrapRealPay(LCRiskDutyWrapSchema tLCRiskDutyWrapSchema) {
        ExeSQL tExeSQL = new ExeSQL();
        double sumPay = 0.0;
        String aWrapCode = tLCRiskDutyWrapSchema.getRiskWrapCode();
        String aContNo = tLCRiskDutyWrapSchema.getContNo();
        for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
            LLClaimDetailSchema tLLClaimDetailSchema = new
                    LLClaimDetailSchema();
            tLLClaimDetailSchema = mLLClaimDetailSet.get(i);
            String tSQL = "";
            if ("00000000000000000000".equals(tLLClaimDetailSchema.getGrpContNo())) {
                tSQL =
                        "select riskwrapcode from lcriskdutywrap where contno='" +
                        tLLClaimDetailSchema.getContNo() +
                        "' and riskcode='" +
                        tLLClaimDetailSchema.getRiskCode() + "'";
            } else {
            	LCPolBL tLCPolBL = new LCPolBL();
            	tLCPolBL.setPolNo(tLLClaimDetailSchema.getPolNo());
            	if (!tLCPolBL.getInfo()) {
            		System.out.println(tLLClaimDetailSchema.getPolNo()+"险种查询失败");
            	}
            	LCPolSchema tLCPolSchema = tLCPolBL.getSchema();
            	tSQL = "select DISTINCT riskwrapcode from lccontplanrisk where riskwrapflag='Y' and grpcontno='"
            		 + tLCPolSchema.getGrpContNo()+"' and contplancode='"+tLCPolSchema.getContPlanCode()
            		 + "' and riskcode = '"+tLCPolSchema.getRiskCode()+"' ";
            }
            SSRS tSSRS = tExeSQL.execSQL(tSQL);
            if (tSSRS.getMaxRow() > 0) {
                String tWrapCode = tSSRS.GetText(1, 1);

                if (aWrapCode.equals(tWrapCode) &&
                    aContNo.equals(tLLClaimDetailSchema.getContNo())) {
                    sumPay += tLLClaimDetailSchema.getRealPay();
                }
            }
        }

        return Arith.round(sumPay, 2);
    }
    /**
     * 给付责任下本次赔付金额
     * @param tLLClaimDetailSchema LLClaimDetailSchema
     * @return double
     */
    private double getGetDutyRealPay(LLClaimDetailSchema tLLClaimDetailGetDutySchema) {
    	
    	double sumPay = 0.0;
         String tPolNo = tLLClaimDetailGetDutySchema.getPolNo();
         String tGetDutyCode = tLLClaimDetailGetDutySchema.getGetDutyCode();
         String tGetDutyKind = tLLClaimDetailGetDutySchema.getGetDutyKind();
         System.out.println(mLLClaimDetailSet.size()+"----------");
         for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
             LLClaimDetailSchema tLLClaimDetailSchema = new
                     LLClaimDetailSchema();
             tLLClaimDetailSchema = mLLClaimDetailSet.get(i);

             if (tPolNo.equals(tLLClaimDetailSchema.getPolNo()) &&
            		 tGetDutyCode.equals(tLLClaimDetailSchema.getGetDutyCode())&&
            		 tGetDutyKind.equals(tLLClaimDetailSchema.getGetDutyKind())) {
            	 System.out.println(tLLClaimDetailSchema.getRealPay()+"----------"+tLLClaimDetailSchema.getCaseNo());
                 sumPay += tLLClaimDetailSchema.getRealPay();
             }
         }

         return Arith.round(sumPay, 2);
    }
    /**
     * 老年关爱领取金额
     * @param tLLClaimDetailSchema LLClaimDetailSchema
     * @return double
     */
    private double getLGSumPay(LLClaimDetailSchema tLLClaimDetailGetDutySchema) {
        ExeSQL tExeSQL = new ExeSQL();
        double sumPay = 0.0;
        String tContNo = tLLClaimDetailGetDutySchema.getContNo();        
        
        String tMJSQL = "select coalesce(sum(b.sumgetmoney),0) "
            + " From ljsgetdraw a,ljaget b "
            + " where  a.getnoticeno = b.actugetno and a.contno ='" + tContNo + "' and a.getdutykind='0'";

        String tPay = tExeSQL.getOneValue(tMJSQL);
        if (tPay != null && tPay.length() > 0) {
            sumPay += Double.parseDouble(tPay);
        }

        return Arith.round(sumPay, 2);
    }
    /**
     * 责任下合计赔款
     * @param tLCDutySchema LCDutySchema
     * @return double
     */
    private double getGetDutySumPay(LLClaimDetailSchema aLLClaimDetailGetDutySchema) {
        ExeSQL tExeSQL = new ExeSQL();
        double sumPay = 0.0;
        String tPolNo = aLLClaimDetailGetDutySchema.getPolNo();
        String tGetDutyCode = aLLClaimDetailGetDutySchema.getGetDutyCode();
        String tGetDutyKind = aLLClaimDetailGetDutySchema.getGetDutyKind();
        //xuyunpeng #2781 add 税优专用责任下续保支持 start
        String sql = "select 1 from lcpol a,lbpol b "
  		   +" where a.prtno=b.prtno and a.grpcontno='00000000000000000000' "
     		   +" and a.polno='"+tPolNo+"'";
         SSRS tXbaoS=tExeSQL.execSQL(sql); 
        //首先会检查是否是税优险，其他险种不会走此逻辑
        if(checkSyPoint(aLLClaimDetailGetDutySchema.getRiskCode())&&tXbaoS.getMaxRow()>0){
        	sumPay = checkSyXB(aLLClaimDetailGetDutySchema);
        }else{
        	
        	String tSQL ="select coalesce(sum(realpay),0) from llclaimdetail a,llcase b where "
        		+ "a.caseno=b.caseno and a.polno='" +tPolNo+ "' and a.getdutycode='" + tGetDutyCode
        		+ "' and a.getdutykind='"+tGetDutyKind+"' and b.rgtstate in ('09','10','11','12') ";
        	String tPay = tExeSQL.getOneValue(tSQL);
        	if (tPay != null && tPay.length() > 0) {
        		sumPay += Double.parseDouble(tPay);
        	}
        }
      //xuyunpeng #2781 add 税优专用责任下续保支持 end

        return Arith.round(sumPay, 2);
    }
    
    
  //#2781 xuyunpeng add  检查是否是税优续保保单 start
    private double checkSyXB(LLClaimDetailSchema aLLClaimDetailGetDutySchema){
    	double tSumM=0.0;
    	//String triskCode = mLLClaimPolicySchema.getRiskCode();
    	String tPolNo = aLLClaimDetailGetDutySchema.getPolNo();
        String tGetDutyCode = aLLClaimDetailGetDutySchema.getGetDutyCode();
        String tGetDutyKind = aLLClaimDetailGetDutySchema.getGetDutyKind();
    	
    	ExeSQL tExeSQL=new ExeSQL();
    	
    
	    		String tDate=" select lp.Cvalidate ,lp.enddate "
			           +" from lcpol lp "
			           +" where lp.polno='"+tPolNo+"' "   
			        
			           +" union "
			           +" select lp.Cvalidate ,lp.enddate "
			           +" from lbpol lp "
			           +" where lp.polno='"+tPolNo+"' "
			           +" with ur ";
		   SSRS tDateS=tExeSQL.execSQL(tDate);
		   System.out.println(tDateS.GetText(1,1)+"******"+tDateS.GetText(1,2));
		   
		  
	   	   //历史赔付金额
	       String tMoney=" select nvl(sum(a.realpay),0)  "
	                    +" from llclaimdetail a,llcase b "
	                    +" where a.caseno=b.caseno "
	                    +" and b.rgtstate in ('09','10','11','12') "
	                    +" and a.polno='"+tPolNo+"' "
	                    +" and a.getdutycode='" + tGetDutyCode
	                    +"' and a.getdutykind='"+tGetDutyKind+"'"
	                    +" and a.caseno in "
	                    +" ( "
	                    +" select c.caseno " 
	                    +" from llcaserela c,llsubreport d "
	                    +" where c.caseno=a.caseno "
	                    +" and d.subrptno=c.subrptno "
	                    +" and d.accdate between '"+tDateS.GetText(1,1)+"' and '"+tDateS.GetText(1,2)+"'"
	                    +" ) with ur ";
	       SSRS tMoneyS=tExeSQL.execSQL(tMoney);
	       tSumM=Double.parseDouble(tMoneyS.GetText(1, 1)); 
   
    	return tSumM;
    }
    
    //#2781 xuyunpeng add  检查是否是税优续保保单 end
  
    // # 1500 康乐人生 **start**
    /**
     * 出险时已保年期
     * 
     */
    private String getRgtYears(LLClaimDetailSchema aLLClaimDetailGetDutySchema){
    	String uContNo=aLLClaimDetailGetDutySchema.getContNo();
    	String uPolNo =aLLClaimDetailGetDutySchema.getPolNo();
    	String srSQL = "select a.accdate from llsubreport a,llcaserela b "
				+ " where a.subrptno=b.subrptno  and b.caseno='" + mLLCaseSchema.getCaseNo() + "' with ur";
        ExeSQL texesql = new ExeSQL();
        String mAccDate = texesql.getOneValue(srSQL);
    	
        String cVSQL ="select cvaliDate from lcpol where contno='"+ uContNo+"' and polno='"+ uPolNo +"' with ur";
        ExeSQL cVsql = new ExeSQL();
        String mCValiDate =cVsql.getOneValue(cVSQL);
        int mRgtYears= PubFun.calInterval(mCValiDate,mAccDate, "Y");
    	return String.valueOf(mRgtYears);

    }
    // # 1500 康乐人生 **end**
    /**
     * 责任下合计赔款
     * @param tLCDutySchema LCDutySchema
     * @return double
     */
    private double getDutySumPay(LCDutySchema tLCDutySchema) {
        ExeSQL tExeSQL = new ExeSQL();
        double sumPay = 0.0;
        String tPolNo = tLCDutySchema.getPolNo();
        String tDutyCode = tLCDutySchema.getDutyCode();

        String tSQL =
                "select sum(realpay) from llclaimdetail a,llcase b where "
                + "a.caseno=b.caseno and a.polno='" +
                tPolNo
                + "' and a.dutycode='" + tDutyCode
                + "' and b.rgtstate in ('09','10','11','12') ";

        String tPay = tExeSQL.getOneValue(tSQL);
        if (tPay != null && tPay.length() > 0) {
            sumPay += Double.parseDouble(tPay);
        }

        return Arith.round(sumPay, 2);
    }

    /**
     * 责任下本次赔款
     * @param tLCDutySchema LCDutySchema
     * @return double
     */
    private double getDutyRealPay(LCDutySchema tLCDutySchema) {
        double sumPay = 0.0;
        String tPolNo = tLCDutySchema.getPolNo();
        String tDutyCode = tLCDutySchema.getDutyCode();
        for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
            LLClaimDetailSchema tLLClaimDetailSchema = new
                    LLClaimDetailSchema();
            tLLClaimDetailSchema = mLLClaimDetailSet.get(i);

            if (tPolNo.equals(tLLClaimDetailSchema.getPolNo()) &&
                tDutyCode.equals(tLLClaimDetailSchema.getDutyCode())) {
                sumPay += tLLClaimDetailSchema.getRealPay();
            }
        }

        return Arith.round(sumPay, 2);
    }

    /**
     * 险种下合计赔款
     * @param aLCPolSchema LCPolSchema
     * @return double
     */
    private double getRiskCodeSumPay(LCPolSchema aLCPolSchema) {
        ExeSQL tExeSQL = new ExeSQL();
        double sumPay = 0.0;
        String tPolNo = aLCPolSchema.getPolNo();

        String tSQL =
                "select sum(realpay) from llclaimdetail a,llcase b where "
                + "a.caseno=b.caseno and a.polno='" +
                tPolNo
                + "' and b.rgtstate in ('09','10','11','12') ";

        String tPay = tExeSQL.getOneValue(tSQL);
        if (tPay != null && tPay.length() > 0) {
            sumPay += Double.parseDouble(tPay);
        }

        return Arith.round(sumPay, 2);
    }

    /**
     * 险种下本次赔款
     * @param aLCPolSchema LCPolSchema
     * @return double
     */
    private double getRiskCodeRealPay(LCPolSchema aLCPolSchema) {
        double sumPay = 0.0;
        String tPolNo = aLCPolSchema.getPolNo();
        for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
            LLClaimDetailSchema tLLClaimDetailSchema = new
                    LLClaimDetailSchema();
            tLLClaimDetailSchema = mLLClaimDetailSet.get(i);

            if (tPolNo.equals(tLLClaimDetailSchema.getPolNo())) {
                sumPay += tLLClaimDetailSchema.getRealPay();
            }
        }
        return Arith.round(sumPay, 2);
    }

    /**
     * 返回核赔结果
     * @param tLMUWSchema LMUWSchema
     * @return String
     */
     
    private String parseUWResult(LMUWSchema tLMUWSchema) {
        PubCalculator tPubCalculator = new PubCalculator();
        tPubCalculator.addBasicFactor("ContNo", mContNo);
        tPubCalculator.addBasicFactor("PolNo", mPolNo);
        tPubCalculator.addBasicFactor("RiskCode", mRiskCode);
        tPubCalculator.addBasicFactor("DutyCode", mDutyCode);
        tPubCalculator.addBasicFactor("GetDutyCode", mGetDutyCode);
        tPubCalculator.addBasicFactor("GetDutyKind", mGetDutyKind);
        tPubCalculator.addBasicFactor("RiskWrapCode", mRiskWrapCode);
        tPubCalculator.addBasicFactor("SumPay", mSumPay);
        tPubCalculator.addBasicFactor("RealPay", mRealPay);
        tPubCalculator.addBasicFactor("ARealPay", mARealPay);//2713补充A 本次赔付金额的错误提示
        tPubCalculator.addBasicFactor("ShareAmnt", mShareAmnt);
        tPubCalculator.addBasicFactor("LGSumPay", mLGSumPay);
        tPubCalculator.addBasicFactor("PolYears", getPolYears(mPolNo));
        tPubCalculator.addBasicFactor("GrpContNo", mGrpContNo);
        tPubCalculator.addBasicFactor("ContPlanCode", mContPlanCode);
        
    	//xuyunpeng #2781 中保信信息 start
    	tPubCalculator.addBasicFactor("AnnualClaimSa", AnnualClaimSa);
    	tPubCalculator.addBasicFactor("WholeLifeClaimSa", WholeLifeClaimSa);        	
    	//xuyunpeng #2781 中保信信息 end
    	// #3561 健康金福 star
    	tPubCalculator.addBasicFactor("ShareSumPay", mShareSumPay);
    	tPubCalculator.addBasicFactor("ShareRealPay", mShareRealPay);
    	tPubCalculator.addBasicFactor("ComSumPay", mComSumPay);
    	tPubCalculator.addBasicFactor("ComRealPay", mComRealPay);
    	// #3561 健康金福 end

    	// #3661 **start**
    	tPubCalculator.addBasicFactor("Prem", mPrem);
    	tPubCalculator.addBasicFactor("Customername", mCustomername);
    	tPubCalculator.addBasicFactor("GetDutyName", mGetDutyName);
    	// #3661 **end**
    	
        String tSql = tLMUWSchema.getRemark().trim();
        String tStr = "";
        String tStr1 = "";
        try {
            while (true) {
                tStr = PubFun.getStr(tSql, 2, "$");
                if (tStr.equals("")) {
                    break;
                }
                tPubCalculator.setCalSql(tStr);
                tStr1 = "$" + tStr.trim() + "$";
                //替换变量

                tSql = StrTool.replaceEx(tSql, tStr1, tPubCalculator.calculate());
            }
        } catch (Exception ex) {
            // @@错误处理
            buildError("parseUWResult", "解释" + tSql + "的变量:" + tStr + "时出错。");
            return "";
        }
        if ("0".equals(tSql.trim())) {
        	tSql = "解析"+tLMUWSchema.getUWCode()+"提示信息出错。";
        }
        if (tSql.trim().equals("")) {
            return tLMUWSchema.getRemark();
        }
        return tSql;
    }
    
    /**
     * 根据传入的保单信息查询该保单本次赔付事件数
     * @param aLLClaimDetailSchema
     * @return
     */
    private int getCaseRelaCount(LLClaimDetailSchema aLLClaimDetailSchema) {
		int tCount = 0;

		String tPolNo = aLLClaimDetailSchema.getPolNo();
		String tDutyCode = aLLClaimDetailSchema.getDutyCode();

		if (tPolNo == null || "".equals(tPolNo)) {
			return 0;
		}

		for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
			boolean tFlag = false;
			if (tDutyCode != null && !"".equals(tDutyCode)) {
				if (tDutyCode.equals(mLLClaimDetailSet.get(i).getDutyCode())
						&& tPolNo.equals(mLLClaimDetailSet.get(i).getPolNo())
						&& mLLClaimDetailSet.get(i).getRealPay() > 0) {
					for (int j = 1; j <= mLLCaseRelaSet.size(); j++) {
						if (mLLClaimDetailSet.get(i).getCaseRelaNo().equals(
								mLLCaseRelaSet.get(j).getCaseRelaNo())) {
							tFlag = true;
							break;
						}
					}
				}
			} else {
				if (tPolNo.equals(mLLClaimDetailSet.get(i).getPolNo())
						&& mLLClaimDetailSet.get(i).getRealPay() > 0) {
					for (int j = 1; j <= mLLCaseRelaSet.size(); j++) {
						if (mLLClaimDetailSet.get(i).getCaseRelaNo().equals(
								mLLCaseRelaSet.get(j).getCaseRelaNo())) {
							tFlag = true;
							break;
						}
					}
				}
			}

			if (tFlag) {
				tCount++;
			}
		}
		return tCount;
	}
    
    /**
     * 保单经过年度
     * @return String
     */
    private String getPolYears(String aPolNo) {
		ExeSQL tExeSQL = new ExeSQL();
		int tYears = 0;
		String tSQL = "select max(b.edorvalidate) From lpedorapp a,lpedoritem b where a.edoracceptno = b.edoracceptno "
				+ " and edortype in('FX','TF') and a.edorstate ='0' "
				+ " and b.contno ='" + mContNo + "'";
		String tValiDate = tExeSQL.getOneValue(tSQL);
		String tAccDate = "";
		for (int j = 1; j <= mLLClaimDetailSet.size(); j++) {
			if (mLLClaimDetailSet.get(j).getRealPay() > 0) {
				for (int m = 1; m <= mLLCaseRelaSet.size(); m++) {
					if (mLLClaimDetailSet.get(j).getCaseRelaNo().equals(
							mLLCaseRelaSet.get(m).getCaseRelaNo())) {
						for (int i = 1; i <= mLLSubReportSet.size(); i++) {
							if (mLLCaseRelaSet.get(m).getSubRptNo().equals(
									mLLSubReportSet.get(i).getSubRptNo())) {
								tAccDate = mLLSubReportSet.get(i).getAccDate();
								break;
							}
						}
					}
				}
			}
		}
		if (tValiDate == null || "".equals(tValiDate)) {
			LCPolBL tLCPolBL = new LCPolBL();
			tLCPolBL.setPolNo(aPolNo);
			if (!tLCPolBL.getInfo()) {
				System.out.println("保单查询失败");
				return String.valueOf(tYears);
			}
			tYears = PubFun.calInterval(tLCPolBL.getCValiDate(), tAccDate, "Y");
		} else {
			tYears = PubFun.calInterval(tValiDate, tAccDate, "Y");
		}
		return String.valueOf(tYears);
	}
    
	/**
	 * 套餐份数默认为0
	 * @return
	 */
    private String getWrapCopys() {
		String tCopys = "0";
		LICertifyDB tLICertifyDB = new LICertifyDB();
		tLICertifyDB.setCardNo(mContNo);
		if (tLICertifyDB.getInfo()) {
			try {
				tCopys = String
						.valueOf(Arith.round(tLICertifyDB.getCopys(), 0));
			} catch (Exception ex) {
				System.out.println("套餐Copys转换失败");
			}
		}
		return tCopys;
	}
    
    /**
     * 是否享有社会医疗保险或公费医疗 0-是 1-否
     * @return
     */
    private String getSecurityFlag() {
		String tSecurityFlag = "1";

    	String sql = "select Value from LLOtherFactor where Value='0' and FactorType='9' and FactorCode='9921' and caseno='"+mLLCaseSchema.getCaseNo()+"' with ur";
    	ExeSQL exeSQL = new ExeSQL();
        String result = exeSQL.getOneValue(sql);
        if(result!=null && !"".equals(result)){
        	tSecurityFlag = result;
		}
		return tSecurityFlag;
	}
    /** #2781 xuyunpeng
     * 校验险种是否为税优险
     */
    private boolean checkSyPoint(String triskCode){
    	String sql = "select taxoptimal from lmriskapp where riskcode='"+triskCode+"'";

    	ExeSQL exesql = new ExeSQL();
    	String res = exesql.getOneValue(sql); 
    	if("Y".equals(res)){
    		return true;
    	}else{
    		return false;
    	}
    }
    /**
     * #2781 校验税优健管责任
     * @param args
     * @return 
     */
   private  int getHealthDuty( String tRiskcode){
	   String tSQL="select getdutykind from llclaimdetail where caseno='"+ mLLCaseSchema.getCaseNo() +"' and riskcode='"+tRiskcode+"'" ;
	   
	   	ExeSQL tExeSQL = new ExeSQL();
	   	SSRS tSSRS = tExeSQL.execSQL(tSQL);
	   	if(tSSRS.getMaxRow()>0){
	   		String tGetDutyKind =tSSRS.GetText(1, 1);
	   		if(tGetDutyKind.equals("900") || tGetDutyKind.equals("700")){
	   			return 0;
	   		}
	   	}
	   	
	    return 1 ;
   }
   
   /**
    * 3207 管理式补充A**start**
    * @param args
    */
    private String getDutyAccAmnt(LCDutySchema oLCDutySchema,String tdate){
    	//批次导入llcase暂无数据
    	String aCaseNo =getDutyCaseNum(oLCDutySchema);
    	double AccAmnt =  LLCaseCommon.getAmntFromAcc(aCaseNo, oLCDutySchema.getPolNo(), tdate, "QR");
		return String.valueOf(AccAmnt); 
    }
    
    /**
     *  3207 管理式补充A 本次赔付合计
     * @param args
     */
    private double getMRealPay(LCDutySchema tLCDutySchema){
    	double MSumPay=0.0;
        String tPolNo = tLCDutySchema.getPolNo();
        for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
            LLClaimDetailSchema tLLClaimDetailSchema = new  LLClaimDetailSchema();
            tLLClaimDetailSchema = mLLClaimDetailSet.get(i);
            //医疗保险金 
            if (tPolNo.equals(tLLClaimDetailSchema.getPolNo()) && ("900".equals(tLLClaimDetailSchema.getGetDutyKind())||
            		 "100".equals(tLLClaimDetailSchema.getGetDutyKind())||"200".equals(tLLClaimDetailSchema.getGetDutyKind()))) {
            	MSumPay += tLLClaimDetailSchema.getRealPay();
            }
        }

        return Arith.round(MSumPay, 2);
    }
    
    // 3207 管理式补充A**end**
    
    // 3337 管理式补充B**start**
    	
    	/***
    	 * 管理式补充B公共账户保额 
    	 * @param tLLClaimDetailGetDutySchema
    	 * @param tDate
    	 * @return
    	 */
    private String getGetManageAmntB(LLClaimDetailSchema tLLClaimDetailGetDutySchema,String tDate){
    	//获取到公共账户的保单险种号
    	String tPolNo= getPoolPolNo(tLLClaimDetailGetDutySchema.getPolNo());
    	//批导时LLCASESchema为空
    	String aCaseNo = getGetDutyCaseNum(tLLClaimDetailGetDutySchema);
    	double tPoolAmnt=LLCaseCommon.getPooledAccount(aCaseNo, tPolNo, tDate, "QR");
    
    	return  String.valueOf(tPoolAmnt);
    }
    
    /***
     * 获取到公共账户的保单号
     */
    private String getPoolPolNo (String aPolNo){
    	String tGRPSQL = "select grppolno from lcpol "
                +" where polno='"+aPolNo+"' union select grppolno from lbpol where polno='"+aPolNo+"'";
                
    	ExeSQL texesql = new ExeSQL();
    	String tGrpPolNo = texesql.getOneValue(tGRPSQL);

    	String tsql = "select polno from lcpol "
                + "where grppolno='"+tGrpPolNo+"' and poltypeflag = '2' and acctype='1' ";
    	String tPolNo = texesql.getOneValue(tsql);
    	
    	return tPolNo;
    }
    
    	/***
    	 * 管理式补充B个人保额+公共保额
    	 * @param args
    	 */
    private String getManageBGetPersonPoolAmnt(LLClaimDetailSchema tLLClaimDetailGetDutySchema,String tDate){
     	//获取到公共账户的保单号
        String tPolNo= getPoolPolNo(tLLClaimDetailGetDutySchema.getPolNo());
        //批导时LLCASESchema 为空
        String aCaseNo =getGetDutyCaseNum(tLLClaimDetailGetDutySchema);
    	double tGetPoolAmnt=LLCaseCommon.getPooledAccount(aCaseNo, tPolNo, tDate, "QR");
    	
    	double tGetPersonAmnt=LLCaseCommon.getAmntFromAcc(aCaseNo, tLLClaimDetailGetDutySchema.getPolNo(), tDate, "QR");
    	
    	return String.valueOf(tGetPoolAmnt+tGetPersonAmnt);
		
    }
    
    /***
     * 管理式补充B公共保额 责任层
     * @param args
     */
    private String getDutyManageAmntB(LCDutySchema aLCDutySchema,String tDate){
    	//获取到公共账户的保单号
        String tPolNo= getPoolPolNo(aLCDutySchema.getPolNo());
        String aCaseNo = getDutyCaseNum(aLCDutySchema);
    	double tPoolAmnt =  LLCaseCommon.getPooledAccount(aCaseNo, tPolNo, tDate, "QR");
		return String.valueOf(tPoolAmnt); 

    }
    
    /***
     * 管理式补充B个人保额+公共保额 责任层
     * @param args
     */
    private String getManageBDutyPersonPoolAmnt(LCDutySchema tLCDutySchema ,String tDate){
    	//获取到公共账户的保单号
        String tPolNo= getPoolPolNo(tLCDutySchema.getPolNo());
        String aCaseNo = getDutyCaseNum(tLCDutySchema);
    	double tPersonAmntB =LLCaseCommon.getAmntFromAcc(aCaseNo, tLCDutySchema.getPolNo(), tDate, "QR") ;
    	double tPoolAmntB =LLCaseCommon.getPooledAccount(aCaseNo, tPolNo, tDate, "QR");
    	
    	return String.valueOf(tPersonAmntB+tPoolAmntB);
    }
    
    /***
     * 管理式补充医疗保险(B款) 死亡日期  险种层   1--死亡  0 --非死亡
     * @param args
     */
    private  int getDeathDate(String aCaseNo){
    	String tDeathSQL="select deathdate from llcase where caseno='"+aCaseNo+"' with ur";
    	ExeSQL tExe = new ExeSQL();
    	String tSRDeath =tExe.getOneValue(tDeathSQL);
    	if(!tSRDeath.equals("") && tSRDeath!=null){
    		return 1;
    	}else{
    		return 0;
    	}
    	
    }
    
    /***
     * 管理式补充医疗保险B款 险种层下 公共保额
     * 
     */
    private double getRiskManageAmntB(LCPolSchema tLCPolSchema){
    	//获取到公共账户的保单号
        String tPolNo= getPoolPolNo(tLCPolSchema.getPolNo());
        String aCaseNo = getPolCaseNum(tLCPolSchema);
    	double tPersonAmntB =LLCaseCommon.getPooledAccount(aCaseNo, tPolNo, PubFun.getCurrentDate(), "QR");
    	
    	return tPersonAmntB;
    }
    
    /***
     * 管理式补充医疗保险B款 险种层下 先公共保额后个人保额
     * @param args
     */
    private double getManageBRiskPersonPoolAmnt(LCPolSchema tLCPolSchema){
    	//获取到公共账户的保单号
        String tPolNo= getPoolPolNo(tLCPolSchema.getPolNo());
        String aCaseNo = getPolCaseNum(tLCPolSchema);
    	double tPersonAmntB =LLCaseCommon.getAmntFromAcc(aCaseNo, tLCPolSchema.getPolNo(), PubFun.getCurrentDate(), "QR") ;
    	double tPoolAmntB =LLCaseCommon.getPooledAccount(aCaseNo, tPolNo, PubFun.getCurrentDate(), "QR");
    	
    	return tPersonAmntB+tPoolAmntB;
    }
    
    /**
     * 针对健康金福下共用保额的情况   #3561
     * 给付责任下一般医疗或癌症合计赔款   一般医疗  GetDutyKind 100  200  癌症  400
     * @param tLCDutySchema LCDutySchema
     * @return double
     */
    private double getShareGetDutySumPay(LLClaimDetailSchema aLLClaimDetailGetDutySchema) {
        ExeSQL tExeSQL = new ExeSQL();
        double sumPay = 0.0;
        String tSQL = "";
        String tPolNo = aLLClaimDetailGetDutySchema.getPolNo();
        String tGetDutyKind = aLLClaimDetailGetDutySchema.getGetDutyKind();
        if("400".equals(tGetDutyKind)){
        	tSQL ="select coalesce(sum(realpay),0) from llclaimdetail a,llcase b where "
        		+ "a.caseno=b.caseno and a.polno='" +tPolNo+ "'"
        		+ "and a.getdutykind='"+tGetDutyKind+"' and b.rgtstate in ('09','10','11','12') ";
        }else{
        	tSQL ="select coalesce(sum(realpay),0) from llclaimdetail a,llcase b where "
            		+ "a.caseno=b.caseno and a.polno='" +tPolNo+ "' "
            		+ "and a.getdutykind in('100','200') and b.rgtstate in ('09','10','11','12') ";
        }
        String tPay = tExeSQL.getOneValue(tSQL);
        if (tPay != null && tPay.length() > 0) {
        		sumPay += Double.parseDouble(tPay);
        }
        return Arith.round(sumPay, 2);
    }
    
    /**针对健康金福下共用保额的情况  #3561  本次赔付 一般与癌症分开
     * 给付责任下本次赔付金额   一般医疗  GetDutyKind 100  200  癌症  400
     * @param tLLClaimDetailSchema LLClaimDetailSchema
     * @return double
     */
    private double getShareDutyRealPay(LLClaimDetailSchema tLLClaimDetailGetDutySchema) {
    	double sumPay = 0.0;
    	double sumPay1 = 0.0;
    	double sumPay2 = 0.0;
         String tPolNo = tLLClaimDetailGetDutySchema.getPolNo();
         String tGetDutyKind = tLLClaimDetailGetDutySchema.getGetDutyKind();
         System.out.println(mLLClaimDetailSet.size()+"----------");
         for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
             LLClaimDetailSchema tLLClaimDetailSchema = new
                     LLClaimDetailSchema();
             tLLClaimDetailSchema = mLLClaimDetailSet.get(i);
             if(tPolNo.equals(tLLClaimDetailSchema.getPolNo())&&"400".equals(tLLClaimDetailSchema.getGetDutyKind())){
            	 System.out.println(tLLClaimDetailSchema.getRealPay()+"----------"+tLLClaimDetailSchema.getCaseNo());
            	 sumPay1 += tLLClaimDetailSchema.getRealPay();
             }else if(tPolNo.equals(tLLClaimDetailSchema.getPolNo())&&!"400".equals(tLLClaimDetailSchema.getGetDutyKind())){
            	 sumPay2 += tLLClaimDetailSchema.getRealPay();
             }   
         }
         if("400".equals(tGetDutyKind)){
        	 sumPay = sumPay1;
         }else{
        	 sumPay = sumPay2;
         }
         return Arith.round(sumPay, 2);
    }
    
    
    /**针对健康金福下共用保额的情况  #3561  本次赔付 一般医疗历史赔付赔付
     * 给付责任下本次赔付金额   一般医疗  GetDutyKind 100  200  
     * @param tLLClaimDetailSchema LLClaimDetailSchema
     * @return double
     */
    private double getComSumPay(LLClaimDetailSchema tLLClaimDetailGetDutySchema) {
    	 ExeSQL tExeSQL = new ExeSQL();
         double sumPay = 0.0;
         String tSQL = "";
         String tPolNo = tLLClaimDetailGetDutySchema.getPolNo();
         tSQL ="select coalesce(sum(realpay),0) from llclaimdetail a,llcase b where "
        		+ "a.caseno=b.caseno and a.polno='" +tPolNo+ "' "
        		+ "and a.getdutykind in('100','200') and b.rgtstate in ('09','10','11','12') ";
    
         String tPay = tExeSQL.getOneValue(tSQL);
         if (tPay != null && tPay.length() > 0) {
    		sumPay += Double.parseDouble(tPay);
         }
   
         return Arith.round(sumPay, 2);
    }
    
    /**针对健康金福下共用保额的情况  #3561  本次赔付 一般医疗本次赔付
     * 给付责任下本次赔付金额   一般医疗  GetDutyKind 100  200  
     * @param tLLClaimDetailSchema LLClaimDetailSchema
     * @return double
     */
    private double getComRealPay(LLClaimDetailSchema tLLClaimDetailGetDutySchema) {
         double sumPay = 0.0;
         String tPolNo = tLLClaimDetailGetDutySchema.getPolNo();
         System.out.println(mLLClaimDetailSet.size()+"----------");
         for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
             LLClaimDetailSchema tLLClaimDetailSchema = new
                     LLClaimDetailSchema();
             tLLClaimDetailSchema = mLLClaimDetailSet.get(i);
             if(tPolNo.equals(tLLClaimDetailSchema.getPolNo())&&!"400".equals(tLLClaimDetailSchema.getGetDutyKind())){
            	 System.out.println(tLLClaimDetailSchema.getRealPay()+"----------"+tLLClaimDetailSchema.getCaseNo());
            	 sumPay += tLLClaimDetailSchema.getRealPay();
             }
         } 
         return Arith.round(sumPay, 2);
    }
    
    
    // 3337 管理式补充B**end**
    
    // #3661 《健康守望补充医疗保险（B款）》**start**
    /**
     * 获得该险种下保费
     */
    private double getRiskcodePrem(LLClaimDetailSchema aLLClaimDetailGetDutySchema){
    	ExeSQL tExeSQL = new ExeSQL();
    	double riskcodePrem = 0.0;
    	String tSQL = "select prem from lcpol where polno='"+aLLClaimDetailGetDutySchema.getPolNo()+"' and riskcode='"+aLLClaimDetailGetDutySchema.getRiskCode()+"' " +
    				  "union select prem from lbpol where polno='"+aLLClaimDetailGetDutySchema.getPolNo()+"' and riskcode='"+aLLClaimDetailGetDutySchema.getRiskCode()+"' with ur";    	
    	String tPay = tExeSQL.getOneValue(tSQL);
    	if (tPay != null && tPay.length() > 0) {
    		riskcodePrem += Double.parseDouble(tPay);
    	}
    	return Arith.round(riskcodePrem, 2);
    }
    // #3661 《健康守望补充医疗保险（B款）》**end**
    
    
      /***
       * 管A、管B、补充A批导时暂无LLCASE数据
       * @param aLCDutySchema
       * @return
       */
    //给付责任层
    private String getGetDutyCaseNum(LLClaimDetailSchema aLLClaimDetailSchema){
     	String tCaseno="";
    	tCaseno = tCaseno+mLLCaseSchema.getCaseNo();
    	if("".equals(tCaseno) || "null".equals(tCaseno)){
    		for(int i=1;i<=mLLClaimDetailSet.size();i++){
    			LLClaimDetailSchema nLLClaimDetailSchema =	mLLClaimDetailSet.get(i);
    			String aPolNo = nLLClaimDetailSchema.getPolNo();
    			String aGetDutyCode = nLLClaimDetailSchema.getGetDutyCode();
    			String aGetDutyKind = nLLClaimDetailSchema.getGetDutyKind();
    			if(aPolNo.equals(aLLClaimDetailSchema.getPolNo()) 
    					&& aGetDutyKind.equals(aLLClaimDetailSchema.getGetDutyKind())
    					&& aGetDutyCode.equals(aLLClaimDetailSchema.getGetDutyCode())){
    				tCaseno=  nLLClaimDetailSchema.getCaseNo();
    	    	}
    			}
    		}
    	return tCaseno;

    }
    
    // 责任层
    private String getDutyCaseNum(LCDutySchema aLCDutySchema){
    	String  bCaseNo ="";
    	bCaseNo = bCaseNo + mLLCaseSchema.getCaseNo();
    	if("".equals(bCaseNo)||"null".equals(bCaseNo)){
    		for(int i=1;i<=mLLClaimDetailSet.size();i++){
    			 LLClaimDetailSchema aLLClaimDetailSchema = mLLClaimDetailSet.get(i);
    			 String aPolNo = aLLClaimDetailSchema.getPolNo();
    			 String aDutyCode = aLLClaimDetailSchema.getDutyCode();
    			 if(aPolNo.equals(aLCDutySchema.getPolNo())
    					 && aDutyCode.equals(aLCDutySchema.getDutyCode()) ){
    				 bCaseNo =aLLClaimDetailSchema.getCaseNo();
    			 }
    		}
    	}
    	return bCaseNo;
    	
    }
    
    // 险种层
    private String getPolCaseNum(LCPolSchema aLCPolSchema){
    	
    	String  bCaseNo ="";
    	bCaseNo = bCaseNo + mLLCaseSchema.getCaseNo();
    	if("".equals(bCaseNo)||"null".equals(bCaseNo)){
    		for(int i=1;i<=mLLClaimDetailSet.size();i++){
    			 LLClaimDetailSchema aLLClaimDetailSchema = mLLClaimDetailSet.get(i);
    			 String aPolNo = aLLClaimDetailSchema.getPolNo();
    			 String aRiskCode = aLLClaimDetailSchema.getRiskCode();
    			 if(aPolNo.equals(aLCPolSchema.getPolNo())
    					 && aRiskCode.equals(aLCPolSchema.getRiskCode())){
    				 bCaseNo =aLLClaimDetailSchema.getCaseNo();
    			 }
    		}
    	}
    	return bCaseNo;
    	
    }
    
    
    public static void main(String[] args)
    {
    	LLClaimDetailSet aLLClaimDetailSet = new LLClaimDetailSet();
        LLClaimDetailSchema aLLClaimDetailSchema = new LLClaimDetailSchema();
        LLCaseSchema aLLCaseSchema = new LLCaseSchema();
        aLLCaseSchema.setCaseNo("C1100101104000001");
        aLLClaimDetailSchema.setCaseNo("C1100101104000001");
        aLLClaimDetailSchema.setGetDutyCode("208201");
        aLLClaimDetailSchema.setGetDutyKind("602");
        aLLClaimDetailSchema.setDutyCode("208001");
        aLLClaimDetailSchema.setPolNo("21000583370");
        aLLClaimDetailSchema.setContNo("000948278000001");
        aLLClaimDetailSchema.setRiskCode("332101");
        aLLClaimDetailSet.add(aLLClaimDetailSchema);

        VData tVData = new VData();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "cm0009";
        tG.ManageCom = "86";
        tVData.addElement(aLLClaimDetailSet);
        tVData.addElement(aLLCaseSchema);
        tVData.addElement(tG);
    	
        LLUWCheckBL tLLUWCheckBL = new LLUWCheckBL();
        if (!tLLUWCheckBL.submitData(tVData,"")) {
            System.out.println("错……错……错……");
        }

    }
}
