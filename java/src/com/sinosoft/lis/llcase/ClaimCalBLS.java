package com.sinosoft.lis.llcase;

import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.sql.*;


/**
 * <p>Title: Web业务系统核赔功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 */

public class ClaimCalBLS
{
	//传输数据类
	private VData mInputData ;
	//错误处理类，每个需要错误处理的类中都放置该类
	public  CErrors mErrors = new CErrors();

	//
	private LCUWErrorDB mLCUWErrorDB = new LCUWErrorDB();
	private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();

	private LLClaimPolicySchema mLLClaimPolicySchema = new LLClaimPolicySchema();
	private LLClaimDetailSet mLLClaimDetailSet = new LLClaimDetailSet();




	public ClaimCalBLS() {}

	public static void main(String[] args)
	{
	}

	//传输数据的公共方法
	public boolean submitData(VData cInputData,String cOperate)
	{
	    //首先将数据在本类中做一个备份
	    mInputData=(VData)cInputData.clone() ;

		if (!this.saveData())
			return false;

		mInputData=null;
	    	return true;
	}

	private boolean saveData()
	{
	  	LLClaimPolicySet tLLClaimPolicySet = (LLClaimPolicySet)mInputData.getObjectByObjectName("LLClaimPolicySet",0);
		mLLClaimDetailSet = (LLClaimDetailSet)mInputData.getObjectByObjectName("LLClaimDetailSet",0);
		mLLClaimPolicySchema = tLLClaimPolicySet.get(0);


		Connection conn = null;
		conn = DBConnPool.getConnection();

	    	if (conn==null)
	    	{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProposalFeeBLS";
			tError.functionName = "saveData";
			tError.errorMessage = "数据库连接失败!";
			this.mErrors .addOneError(tError) ;
			return false;
	    	}

		try
		{
			conn.setAutoCommit(false);

			// 删除部分



			LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB( conn );
			tLLClaimPolicyDB.setClmNo(mLLClaimPolicySchema.getClmNo());
			tLLClaimPolicyDB.setPolNo(mLLClaimPolicySchema.getPolNo());

			if (tLLClaimPolicyDB.deleteSQL() == false)
			{
					// @@错误处理
			    	this.mErrors.copyAllErrors(tLLClaimPolicyDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "ClaimCalBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "LLClaimPolicy表删除失败!";
				this.mErrors .addOneError(tError) ;
		        	conn.rollback() ;
				return false;
			}


			tLLClaimPolicyDB.setSchema(mLLClaimPolicySchema);
			if(tLLClaimPolicyDB.insert() == false)
			{
				// @@错误处理
			    	this.mErrors.copyAllErrors(tLLClaimPolicyDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "ClaimCalBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "LLClaimUPolicy表保存失败!";
				this.mErrors .addOneError(tError) ;
		        	conn.rollback() ;
				return false;
			}

			//赔案子表
			LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB( conn );
			tLLClaimDetailDB.setClmNo(mLLClaimPolicySchema.getClmNo());
			tLLClaimDetailDB.setPolNo(mLLClaimPolicySchema.getPolNo());

			if (tLLClaimDetailDB.deleteSQL() == false)
			{
					// @@错误处理
			    	this.mErrors.copyAllErrors(tLLClaimDetailDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "ClaimCalBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "LLClaimDetail表删除失败!";
				this.mErrors .addOneError(tError) ;
		        	conn.rollback() ;
				return false;
			}
			int n = mLLClaimDetailSet.size();
			for (int i = 1;i<=n;i++)
			{
				//LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
				tLLClaimDetailDB.setSchema(mLLClaimDetailSet.get(0));
				if(tLLClaimDetailDB.insert() == false)
				{
					// @@错误处理
				    	this.mErrors.copyAllErrors(tLLClaimDetailDB.mErrors);
					CError tError = new CError();
					tError.moduleName = "ClaimCalBLS";
					tError.functionName = "saveData";
					tError.errorMessage = "LLClaimDetail表保存失败!";
					this.mErrors .addOneError(tError) ;
		        		conn.rollback() ;
					return false;
				}
			}
			conn.commit() ;
		} // end of try
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ClaimCalBLS";
			tError.functionName = "submitData";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);
			try{conn.rollback() ;} catch(Exception e){}
			return false;
		}
	    return true;
	}

}