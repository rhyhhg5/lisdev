package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: LLSpringGiveResultBL </p>
 * <p>Description: LLSpringGiveResultBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author:Xx
 * @CreateDate：2005-02-18 13:58:55
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Date;

public class LLSpringGiveResultBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mG = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private String strHandler;
    private  String SimpleCase="";
    private FDate fDate = new FDate();

    /** 业务处理相关变量 */
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
    private LLSpringGiveResultSchema mLLSpringGiveResultSchema = new LLSpringGiveResultSchema();
    private LLClaimDetailSchema mLLClaimDetailSchema = new LLClaimDetailSchema();
    private LLClaimSchema mLLClaimSchema = new LLClaimSchema();
    
    /** 社保保单标志 0：非社保；1：社保*/
    private String mSocialSecurity = "0";


    public LLSpringGiveResultBL() {
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * @cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        if (!dealHandler())
            return false;
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError.buildErr(this, "数据处理失败LLSpringGiveResultBL-->dealData!");
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;

            System.out.println("Start LLSpringGiveResultBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                CError.buildErr(this, "数据提交失败!");
                return false;
            }
            System.out.println("End LLSpringGiveResultBL Submit...");

        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
     private boolean getInputData(VData cInputData) {
        System.out.println("--Star GetInputData--");
        mLLCaseSchema.setSchema((LLCaseSchema) cInputData.
                                getObjectByObjectName("LLCaseSchema", 0));
        mLLSpringGiveResultSchema = (LLSpringGiveResultSchema) cInputData.
                              getObjectByObjectName("LLSpringGiveResultSchema", 0);
        this.mG.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        if (mG.ManageCom.length() == 2)
            mG.ManageCom = mG.ManageCom + "000000";
        if (mG.ManageCom.length() == 4)
            mG.ManageCom = mG.ManageCom + "0000";
        System.out.println(mLLCaseSchema.getCaseNo());
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("---Star dealData---mOperate---" + mOperate);
        String CaseNo = "";
        String RgtNo = "";
        CaseNo=mLLCaseSchema.getCaseNo();
        if(CaseNo==null||CaseNo.trim().equals("")){
            CError tError = new CError();
            tError.moduleName = "LLSpringGiveResultBL";
            tError.functionName = "caseno";
            tError.errorMessage = "没有传入相应的理赔号!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(CaseNo);
        tLLCaseDB.getInfo();
        tLLCaseSchema=tLLCaseDB.getSchema();
        tLLCaseSchema.setClaimer(mG.Operator);//设置案件的理算人员为当前操作人

        //#2439 青岛大额弹性拨付社保相关业务功能开发 cbs00073241
        //CaseNo="C9400140821009515";
        mSocialSecurity = LLCaseCommon.checkSocialSecurity(CaseNo, null);
        String tStringHandler="";//当前处理人的上级

        if("1".equals(mSocialSecurity)){
        	LLSocialClaimUserDB aLLSocialClaimUserDB = new LLSocialClaimUserDB();
        	aLLSocialClaimUserDB.setUserCode(mG.Operator);
        	aLLSocialClaimUserDB.setStateFlag("1");//社保用户有效
        	if(!aLLSocialClaimUserDB.getInfo()){
                CError tError = new CError();
                tError.moduleName = "LLSpringGiveResultBL";
                tError.functionName = "dealData";
                tError.errorMessage = "您不是社保相关处理人或已失效，无权作此操作";
                this.mErrors.addOneError(tError);
                return false;
        	}
        	
        	LLSocialClaimUserSchema tLLSocialClaimUserSchema = aLLSocialClaimUserDB.getSchema();
        	tStringHandler = tLLSocialClaimUserSchema.getUpUserCode();

        }else{
        	LLClaimUserSchema tLLClaimUserSchema = new LLClaimUserSchema();
            LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
            tLLClaimUserDB.setUserCode(mG.Operator);
            tLLClaimUserDB.setStateFlag("1");//用户有效
            
            if(!tLLClaimUserDB.getInfo()){
                CError tError = new CError();
                tError.moduleName = "LLSpringGiveResultBL";
                tError.functionName = "dealData";
                tError.errorMessage = "您不是运营相关处理人或已失效，无权作此操作";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLLClaimUserSchema=tLLClaimUserDB.getSchema();
            tStringHandler = tLLClaimUserSchema.getUpUserCode();
        }
        
        if("".equals(tStringHandler) || "null".equals(tStringHandler) || tStringHandler == null){
            CError tError = new CError();
            tError.moduleName = "LLSpringGiveResultBL";
            tError.functionName = "dealData";
            tError.errorMessage = "您无上级用户,无法进行[拨付确认]操作";
            this.mErrors.addOneError(tError);
            return false;
        }
            
        tLLCaseSchema.setHandler(tStringHandler);
        tLLCaseSchema.setRgtState("04");      //设置案件状态为理算状态
        tLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
        tLLCaseSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLLCaseSchema, "UPDATE");

        //插入案件轨迹
        mLLCaseOpTimeSchema.setCaseNo(CaseNo);
        mLLCaseOpTimeSchema.setRgtState("04");
        mLLCaseOpTimeSchema.setSequance(1);
        mLLCaseOpTimeSchema.setStartDate(PubFun.getCurrentDate());
        mLLCaseOpTimeSchema.setStartTime(PubFun.getCurrentTime());
        mLLCaseOpTimeSchema.setOpTime("1");
        mLLCaseOpTimeSchema.setEndDate(PubFun.getCurrentDate());
        mLLCaseOpTimeSchema.setEndTime(PubFun.getCurrentTime());
        mLLCaseOpTimeSchema.setOperator(mG.Operator);
        mLLCaseOpTimeSchema.setManageCom(mG.ManageCom);
        map.put(mLLCaseOpTimeSchema, "INSERT");
//目前青岛弹性拨付只涉及这两个给付责任，若后期有新的，建议在前台选择相关责任
        String mRiskCode = mLLSpringGiveResultSchema.getRiskCode();
        String tDutyCode = "";
        String tGetDutyCode = "";
        String tGetDutyKind = "";
        String tpolmngcom = "";
        String tgrppolno = "";
        if("690201".equals(mRiskCode)){
        	String SqlGet ="select distinct dutycode,getdutycode from lcget where dutycode in('670002') and getdutycode in('670203') and grpcontno='"+ mLLSpringGiveResultSchema.getGrpContNo() +"' "
			+" union select distinct dutycode,getdutycode from lbget where dutycode in('670002') and getdutycode in('670203') and grpcontno='"+ mLLSpringGiveResultSchema.getGrpContNo() +"'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(SqlGet);        
			if(tSSRS.getMaxRow()!= 1)
			{
				CError tError = new CError();
			    tError.moduleName = "LLSpringGiveResultBL";
			    tError.functionName = "getdutycode";
			    tError.errorMessage = "险种690201仅支持(医疗费用的审核与拨付)责任的弹性拨付";
			    this.mErrors.addOneError(tError);
			    return false;
			}
			 tDutyCode = tSSRS.GetText(1, 1);
			 tGetDutyCode = tSSRS.GetText(1, 2);
			 
			 String SqlKind ="select distinct getdutykind,polmngcom,grppolno from llclaimdetail where dutycode in('670002') and getdutycode in('670203') and grpcontno='"+ mLLSpringGiveResultSchema.getGrpContNo() +"' "
				+" union select distinct getdutykind,polmngcom,grppolno from llclaimdetail where dutycode in('670002') and getdutycode in('670203') and grpcontno='"+ mLLSpringGiveResultSchema.getGrpContNo() +"'";
		ExeSQL tSQLKind = new ExeSQL();
		SSRS tSSRSKind = tSQLKind.execSQL(SqlKind);		
			if(tSSRSKind.getMaxRow()!= 1)
			{
				CError tError = new CError();
				tError.moduleName = "LLSpringGiveResultBL";
				tError.functionName = "Grppol";
				tError.errorMessage = "保单给付责任类型查询失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
			 tGetDutyKind = tSSRSKind.GetText(1, 1);
			 tpolmngcom = tSSRSKind.GetText(1, 2);
			 tgrppolno = tSSRSKind.GetText(1, 3);
        }else{
        String SqlGet ="select distinct dutycode,getdutycode from lcget where  grpcontno='"+ mLLSpringGiveResultSchema.getGrpContNo() +"' "
        			+" union select distinct dutycode,getdutycode from lbget where grpcontno='"+ mLLSpringGiveResultSchema.getGrpContNo() +"'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(SqlGet);        
        if(tSSRS.getMaxRow()!= 1)
        {
        	CError tError = new CError();
            tError.moduleName = "LLSpringGiveResultBL";
            tError.functionName = "getdutycode";
            tError.errorMessage = "保单有且只能有一个给付责任！";
            this.mErrors.addOneError(tError);
            return false;
        }
         tDutyCode = tSSRS.GetText(1, 1);
         tGetDutyCode = tSSRS.GetText(1, 2);
         
         String SqlKind ="select distinct getdutykind,polmngcom,grppolno from llclaimdetail where  grpcontno='"+ mLLSpringGiveResultSchema.getGrpContNo() +"' "
			+" union select distinct getdutykind,polmngcom,grppolno from llclaimdetail where  grpcontno='"+ mLLSpringGiveResultSchema.getGrpContNo() +"'";
		ExeSQL tSQLKind = new ExeSQL();
		SSRS tSSRSKind = tSQLKind.execSQL(SqlKind);		
			if(tSSRSKind.getMaxRow()!= 1)
			{
				CError tError = new CError();
				tError.moduleName = "LLSpringGiveResultBL";
				tError.functionName = "Grppol";
				tError.errorMessage = "保单给付责任类型查询失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
			 tGetDutyKind = tSSRSKind.GetText(1, 1);	
			 tpolmngcom = tSSRSKind.GetText(1, 2);
			 tgrppolno = tSSRSKind.GetText(1, 3);
        }
        	
        
        //插入赔付总表以及赔付明细表
        String limit = PubFun.getNoLimit(this.mG.ManageCom);
        String mClmNo = PubFun1.CreateMaxNo("CLMNO", limit);
        mLLClaimSchema.setClmNo(mClmNo);
        mLLClaimSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        mLLClaimSchema.setRgtNo(mLLCaseSchema.getRgtNo());
        mLLClaimSchema.setGetDutyKind(tGetDutyKind);
        mLLClaimSchema.setClmState("3");
        mLLClaimSchema.setStandPay(mLLSpringGiveResultSchema.getRealPay());
        mLLClaimSchema.setRealPay(mLLSpringGiveResultSchema.getRealPay());
        mLLClaimSchema.setGiveType("8");                  //8代表弹性拨付
        mLLClaimSchema.setGiveTypeDesc("弹性拨付");
        mLLClaimSchema.setClmUWer(mG.Operator);
        mLLClaimSchema.setCheckType("0");
        mLLClaimSchema.setMngCom(mG.ManageCom);
        mLLClaimSchema.setOperator(mG.Operator);
        mLLClaimSchema.setMakeDate(PubFun.getCurrentDate());
        mLLClaimSchema.setMakeTime(PubFun.getCurrentTime());
        mLLClaimSchema.setModifyDate(PubFun.getCurrentDate());
        mLLClaimSchema.setModifyTime(PubFun.getCurrentTime());
       
        mLLClaimDetailSchema.setRgtNo(mLLCaseSchema.getRgtNo());
        mLLClaimDetailSchema.setClmNo(mClmNo);
        mLLClaimDetailSchema.setPolNo("000000");
        mLLClaimDetailSchema.setGetDutyCode(tGetDutyCode);
        mLLClaimDetailSchema.setGetDutyKind(tGetDutyKind);
        mLLClaimDetailSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        mLLClaimDetailSchema.setCaseRelaNo("000000");
        mLLClaimDetailSchema.setStatType("YL");
        mLLClaimDetailSchema.setGrpContNo(mLLSpringGiveResultSchema.getGrpContNo());
        mLLClaimDetailSchema.setGrpPolNo(tgrppolno);
        mLLClaimDetailSchema.setContNo("000000");
        mLLClaimDetailSchema.setRiskCode(mLLSpringGiveResultSchema.getRiskCode());
        mLLClaimDetailSchema.setKindCode("H");
        mLLClaimDetailSchema.setRiskVer("2002");
        mLLClaimDetailSchema.setPolMngCom(tpolmngcom);
        mLLClaimDetailSchema.setTabFeeMoney(mLLSpringGiveResultSchema.getRealPay());
        mLLClaimDetailSchema.setStandPay(mLLSpringGiveResultSchema.getRealPay());
        mLLClaimDetailSchema.setClaimMoney(mLLSpringGiveResultSchema.getRealPay());
        mLLClaimDetailSchema.setRealPay(mLLSpringGiveResultSchema.getRealPay());
        mLLClaimDetailSchema.setDutyCode(tDutyCode);
        mLLClaimDetailSchema.setGiveType("8");                //8代表弹性拨付
        mLLClaimDetailSchema.setGiveTypeDesc("弹性拨付");
        mLLClaimDetailSchema.setGiveReasonDesc("弹性拨付");
        mLLClaimDetailSchema.setMngCom(mG.ManageCom);
        mLLClaimDetailSchema.setOperator(mG.Operator);
        mLLClaimDetailSchema.setMakeDate(PubFun.getCurrentDate());
        mLLClaimDetailSchema.setMakeTime(PubFun.getCurrentTime());
        mLLClaimDetailSchema.setModifyDate(PubFun.getCurrentDate());
        mLLClaimDetailSchema.setModifyTime(PubFun.getCurrentTime());
        
        LLClaimPolicySchema mLLClaimPolicySchema = new LLClaimPolicySchema();    
        Reflections LLClaimPolicytref = new Reflections();
        LLClaimPolicytref.transFields(mLLClaimPolicySchema, mLLClaimDetailSchema);
        
        LLCasePolicySchema mLLCasePolicySchema = new LLCasePolicySchema();    
        Reflections LLCasePolicytref = new Reflections();
        LLCasePolicytref.transFields(mLLCasePolicySchema, mLLClaimDetailSchema);

        map.put(mLLClaimSchema, "DELETE&INSERT");
        map.put(mLLClaimDetailSchema, "DELETE&INSERT");
        map.put(mLLClaimPolicySchema, "DELETE&INSERT");
        map.put(mLLCasePolicySchema, "DELETE&INSERT");

        //插入弹性拨付表
        LLSpringGiveResultSchema tLLSpringGiveResultSchema = new LLSpringGiveResultSchema();
        LLSpringGiveResultDB tLLSpringGiveResultDB = new LLSpringGiveResultDB();
        tLLSpringGiveResultDB.setCaseNo(mLLCaseSchema.getCaseNo());
        tLLSpringGiveResultDB.setRgtNo(mLLCaseSchema.getRgtNo());
        tLLSpringGiveResultDB.getInfo();
        tLLSpringGiveResultSchema=tLLSpringGiveResultDB.getSchema();
        String _strGrpContNo=mLLSpringGiveResultSchema.getGrpContNo();

        String countSql = "select count(grpcontno) from LLSpringGiveResult where grpcontno ='"
                         +_strGrpContNo+"'";
       ExeSQL texesql = new ExeSQL();
       String casecount = texesql.getOneValue(countSql);
       int count = Integer.parseInt(casecount);

        mLLSpringGiveResultSchema.setGetDutyCode(tGetDutyCode);
        mLLSpringGiveResultSchema.setGetDutyKind(tGetDutyKind);
        mLLSpringGiveResultSchema.setGiveTimes(count+1);
        mLLSpringGiveResultSchema.setMngCom(mG.ManageCom);
        mLLSpringGiveResultSchema.setUWDate(PubFun.getCurrentDate());
        mLLSpringGiveResultSchema.setUWTime(PubFun.getCurrentTime());
        map.put(mLLSpringGiveResultSchema, "INSERT");


        return true;
}



    /**
     * 案件分配
     * @return boolean
     */
    private boolean dealHandler() {
//        LLCaseCommon tLLCaseCommon = new LLCaseCommon();
//        String strMngCom;
//        SimpleCase="";
//
//        String tcaseprop = mLLCaseSchema.getCaseProp()+"";
//        System.out.println("案件属性"+tcaseprop);
//        if(tcaseprop.equals("06"))
//            SimpleCase = "01";
//        else
//            SimpleCase = "02";
//
//        strMngCom = mG.ManageCom;
//        strHandler = tLLCaseCommon.ChooseAssessor(strMngCom, SimpleCase);
//        if (strHandler == null || strHandler.equals("")) {
//            CError tError = new CError();
//            tError.moduleName = "LLSpringGiveResultBL";
//            tError.functionName = "dealHandler";
//            tError.errorMessage = "机构" + strMngCom + "没有符合条件的理赔人!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
        return true;
    }


    /**
     * 准备提交数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLLRegisterSchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLLCaseSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDDrugBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */


    public VData getResult() {
        return this.mResult;
    }
}
