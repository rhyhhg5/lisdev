package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.task.CommonBL;
import com.sinosoft.task.Task;

/**
 * <p>Title: 工单管理系统</p>
 * <p>Description: 工单录入BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 * @date 2005-01-17
 */

public class LLContDealBL {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    private LGWorkSchema mLGWorkSchema = new LGWorkSchema();

    private LGWorkTraceSchema mLGWorkTraceSchema = new LGWorkTraceSchema();

    private LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();

    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();

    private String mWorkBoxNo = null;

    private String mContNo = "";

    private MMap map = new MMap();

    /** 全局参数 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 统一更新日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 统一更新时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    private String mOperator = "";

    public LLContDealBL() {
    }

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (getSubmitData(cInputData, cOperate) == null) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "TaskInputBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public MMap getSubmitData(VData cInputData, String cOperate) {
        // 将传入的数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        // 将外部传入的数据分解到本类的属性中，准备处理
        if (!getInputData()) {
            return null;
        }
        
        if (!checkData()) {
            return null;
        }

        // 根据业务逻辑对数据进行处理
        if (!dealData()) {
            return null;
        }

        // 装配处理好的数据，准备给后台进行保存
        prepareOutputData();

        return map;
    }

    /**
     * 返回产生的工单号
     * @return String
     */
    public String getWorkNo() {
        return mLGWorkSchema.getWorkNo();
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData() {
        mGlobalInput.setSchema((GlobalInput) mInputData.
                               getObjectByObjectName("GlobalInput", 0));
        mLGWorkSchema.setSchema((LGWorkSchema) mInputData.
                                getObjectByObjectName("LGWorkSchema", 0));
        mLLCaseSchema.setSchema((LLCaseSchema)mInputData.getObjectByObjectName("LLCaseSchema",0));

        mWorkBoxNo = (String) mInputData.getObjectByObjectName("String", 0);

        mOperator = mGlobalInput.Operator;

        mContNo = mLLCaseSchema.getGrpNo();
        return true;
    }

    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData() {
    	
    	/*新建时自动作废该保单下续期催收及电话催缴工单
    	 * 如果续期催收已经向银行发盘，暂不进行作废，并提示受理人员。
    	 * 作废原因为暂缓收费*/
    	
    	TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("ContNo", mContNo);
		tTransferData.setNameAndValue("InsuredNo", "");
		VData tVData = new VData();
		tVData.addElement(mGlobalInput);
		tVData.addElement(tTransferData);
		
    	LLIndiLJSCancelBL tLLIndiLJSCancelBL = new LLIndiLJSCancelBL();
		if (!tLLIndiLJSCancelBL.submitData(tVData, "")) {
			CError.buildErr(this, "撤销续期催收异常");
            return false;
		}
		
		String tIndiInfo = "";		
		LJSPaySet tLJSPaySet = ((LJSPaySet) tLLIndiLJSCancelBL.getResult().getObjectByObjectName(
                "LJSPaySet", 0));
		for (int i=1;i<=tLJSPaySet.size();i++) {
			LJSPaySchema tLJSPaySchema = tLJSPaySet.get(i);
			tIndiInfo = "保单" + mContNo
				      + "，续期应收"+tLJSPaySchema.getGetNoticeNo()
			          + "："+tLJSPaySchema.getAccName()
			          + "。";
		}
		
		if (!"".equals(tIndiInfo)) {
			TransferData tTrans = new TransferData();
			tTrans.setNameAndValue("IndiInfo", tIndiInfo);
			mResult.addElement(tTrans);
		}		
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData() {
        String sql;
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();

        if (!checkCaseInfo()) {
            return false;
        }

        //得到理赔保全处理用户
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("llcontdeal");
        tLDCodeDB.setCode("user");
        if (!tLDCodeDB.getInfo()) {
            mErrors.addOneError("未配置理赔合同处理保全用户！");
            return false;
        }
        mGlobalInput.Operator = tLDCodeDB.getCodeName();

        String tWorkNo = mLGWorkSchema.getWorkNo();
        String tWorkBoxNo = mWorkBoxNo;
        String tAcceptorNo = mGlobalInput.Operator;
        String tAcceptCom;

        //新建
        if (!mOperate.equals("UPDATE||MAIN")) {
            //得到受理机构
            sql = "select GroupNo from LGGroupMember " +
                  "where  MemberNo = '" + tAcceptorNo + "' ";
            tSSRS = tExeSQL.execSQL(sql);
            if (tSSRS != null && tSSRS.getMaxRow() > 0) {
                tAcceptCom = tSSRS.GetText(1, 1);
            } else {
                tAcceptCom = "";
            }
            //产生作业号和受理号,作业号和受理号相同,都为日期加上流水号
            if(tWorkNo == null || tWorkNo.equals(""))
            {
                tWorkNo = CommonBL.createWorkNo();
            }

            //得到用户信箱号
            if (mWorkBoxNo == null) {
                sql = "select WorkBoxNo from LGWorkBox " +
                      "where  OwnerTypeNo = '2' " + //个人信箱
                      "and    OwnerNo = '" + mGlobalInput.Operator + "'";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.getMaxRow() == 0) {
                    mErrors.addOneError("没有查询到用户"
                                        + mGlobalInput.Operator + "的信箱。");
                    return false;
                }
                System.out.println(tSSRS.GetText(1, 1));
                tWorkBoxNo = tSSRS.GetText(1, 1);
            }

            //得到客户姓名
            String customerName = mLGWorkSchema.getCustomerName();
            if (customerName == null) {
                customerName = getCustomerName(mLGWorkSchema.getCustomerNo());
            }

            //设置数据
            mLGWorkSchema.setWorkNo(tWorkNo);
            mLGWorkSchema.setAcceptNo(tWorkNo);
            mLGWorkSchema.setNodeNo("0"); //当前是起始结点
            if (mLGWorkSchema.getStatusNo() == null
                || mLGWorkSchema.getStatusNo().equals("")) {
                mLGWorkSchema.setStatusNo("2"); //新建是未经办状态
            }
            mLGWorkSchema.setCustomerName(customerName);
            mLGWorkSchema.setAcceptorNo(tAcceptorNo);
            mLGWorkSchema.setAcceptCom(tAcceptCom);
            mLGWorkSchema.setAcceptDate(mCurrentDate); //受理日期为当前日期
            mLGWorkSchema.setMakeDate(mCurrentDate);
            mLGWorkSchema.setMakeTime(mCurrentTime);
            mLGWorkSchema.setModifyDate(mCurrentDate);
            mLGWorkSchema.setModifyTime(mCurrentTime);
            if (mLGWorkSchema.getOperator() == null
                || mLGWorkSchema.getOperator().equals("")) {
                mLGWorkSchema.setOperator(mGlobalInput.Operator);
            }
            mLGWorkSchema.setUniting("0");
            mLGWorkSchema.setCurrDoing("0");

            //生成历史节点
            mLGWorkTraceSchema.setWorkNo(tWorkNo);
            mLGWorkTraceSchema.setNodeNo("0"); //起始结点
            mLGWorkTraceSchema.setWorkBoxNo(tWorkBoxNo); //信箱编号
            mLGWorkTraceSchema.setInMethodNo("0"); //新单录入
            mLGWorkTraceSchema.setInDate(mCurrentDate);
            mLGWorkTraceSchema.setInTime(mCurrentTime);
            mLGWorkTraceSchema.setSendComNo(tAcceptCom);
            mLGWorkTraceSchema.setSendPersonNo(mGlobalInput.Operator);
            mLGWorkTraceSchema.setMakeDate(mCurrentDate);
            mLGWorkTraceSchema.setMakeTime(mCurrentTime);
            mLGWorkTraceSchema.setModifyDate(mCurrentDate);
            mLGWorkTraceSchema.setModifyTime(mCurrentTime);
            mLGWorkTraceSchema.setOperator(mGlobalInput.Operator);
            map.put(mLGWorkTraceSchema, "INSERT"); //插入

            //生成具体历史信息
            String nodeOpRemark = "自动批注：生成新的工单";
            if (mLGWorkSchema.getRemark() != null
                && !mLGWorkSchema.getRemark().equals("")) {
                nodeOpRemark = mLGWorkSchema.getRemark();
            }
            LGTraceNodeOpSchema tLGTraceNodeOpSchema = new LGTraceNodeOpSchema();
            tLGTraceNodeOpSchema.setWorkNo(tWorkNo);
            tLGTraceNodeOpSchema.setNodeNo("0");
            tLGTraceNodeOpSchema.setOperatorNo("0");
            tLGTraceNodeOpSchema.setOperatorType("0");
            tLGTraceNodeOpSchema.setRemark(nodeOpRemark);
            tLGTraceNodeOpSchema.setFinishDate(mCurrentDate);
            tLGTraceNodeOpSchema.setFinishTime(mCurrentTime);
            tLGTraceNodeOpSchema.setOperator(mGlobalInput.Operator);
            tLGTraceNodeOpSchema.setMakeDate(mCurrentDate);
            tLGTraceNodeOpSchema.setMakeTime(mCurrentTime);
            tLGTraceNodeOpSchema.setModifyDate(mCurrentDate);
            tLGTraceNodeOpSchema.setModifyTime(mCurrentTime);

            map.put(tLGTraceNodeOpSchema, "INSERT");

            //录入业务信息
            String tTypeNo = mLGWorkSchema.getTypeNo();
            String tTopTypeNo = tTypeNo.substring(0, 2);
            //咨询
            if (tTopTypeNo.equals("01")) {
                //设置具体业务号，暂定为工单号
                mLGWorkSchema.setDetailWorkNo(tWorkNo);
            }
            //投诉
            else if (tTopTypeNo.equals("02")) {
                mLGWorkSchema.setDetailWorkNo(tWorkNo);
            }
            //保全||续期续保
            else if (tTopTypeNo.equals("03") || tTypeNo.equals("070001")|| tTypeNo.equals("070002")) {
                String tEdorState = "1";
                if(Task.WORKSTATUS_DONE.equals(mLGWorkSchema.getStatusNo())
                   || Task.WORKSTATUS_ARCHIVE.equals(mLGWorkSchema.getStatusNo())){
                    tEdorState = "0";
                }
                mLPEdorAppSchema.setEdorState(tEdorState);

                //个单
                if (mLGWorkSchema.getCustomerNo().length() == 9) {
                    mLPEdorAppSchema.setEdorAcceptNo(tWorkNo); //工单号为保全申请号
                    mLPEdorAppSchema.setOtherNo(mLGWorkSchema.getCustomerNo()); //申请号码
                    mLPEdorAppSchema.setOtherNoType("1"); //申请号码类型 1-个人客户号
                    mLPEdorAppSchema.setEdorAppName(mLGWorkSchema.getApplyName()); //申请人名称
                    mLPEdorAppSchema.setAppType(mLGWorkSchema.getAcceptWayNo()); //申请方式
                    mLPEdorAppSchema.setEdorAppDate(mLGWorkSchema.getAcceptDate()); //申请日期

                    VData tVData = new VData();
                    tVData.add(mLPEdorAppSchema);
                    tVData.add(mGlobalInput);
                    PEdorAppMainBL tPEdorAppMainBL = new PEdorAppMainBL();
                    MMap tMMap = tPEdorAppMainBL
                                 .getSubmitMap(tVData, "INSERT||EDORAPP");
                    if (tMMap == null) {
                        CError tError = new CError();
                        tError.moduleName = "TaskInputBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "保全数据提交失败!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    map.add(tMMap);
                    VData tResult = tPEdorAppMainBL.getResult();
                    LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
                    tLPEdorAppSchema.setSchema((LPEdorAppSchema)
                                               tResult.getObjectByObjectName(
                            "LPEdorAppSchema", 0));
                }
                //团单
                else if (mLGWorkSchema.getCustomerNo().length() == 8) {
                    //团单用保单号
                    mLPEdorAppSchema.setEdorAcceptNo(tWorkNo);
                    mLPEdorAppSchema.setOtherNo(mLGWorkSchema.getCustomerNo()); //申请号码
                    mLPEdorAppSchema.setOtherNoType("2"); //申请号码类型 2-团体客户号
                    mLPEdorAppSchema.setEdorAppName(mLGWorkSchema.getApplyName()); //申请人名称
                    mLPEdorAppSchema.setAppType(mLGWorkSchema.getAcceptWayNo()); //申请方式
                    mLPEdorAppSchema.setEdorAppDate(mLGWorkSchema.getAcceptDate()); //申请日期
                    mLPEdorAppSchema.setManageCom(mGlobalInput.ManageCom); //管理机构
                    mLPEdorAppSchema.setOperator(mGlobalInput.Operator);
                    mLPEdorAppSchema.setMakeDate(mCurrentDate);
                    mLPEdorAppSchema.setMakeTime(mCurrentTime);
                    mLPEdorAppSchema.setModifyDate(mCurrentDate);
                    mLPEdorAppSchema.setModifyTime(mCurrentTime);
                    map.put(mLPEdorAppSchema, "INSERT");
                }
                //得到保全号受理号
                mLGWorkSchema.setDetailWorkNo(mLPEdorAppSchema.getEdorAcceptNo());
            } else if (tTopTypeNo.equals("04")) {
                //理赔
            } else if (tTopTypeNo.equals("05")||(tTopTypeNo.equals("07")&&!tTypeNo.equals("070001")&&!tTypeNo.equals("070002"))) {
                //电话回访
                mLGWorkSchema.setDetailWorkNo(tWorkNo);
            } else if (tTopTypeNo.equals("06")) {
                //定期结算
                mLPEdorAppSchema.setEdorAcceptNo(tWorkNo);
                mLPEdorAppSchema.setOtherNo(mLGWorkSchema.getCustomerNo()); //申请号码
                mLPEdorAppSchema.setOtherNoType("2"); //申请号码类型 2-团体客户号
                mLPEdorAppSchema.setEdorAppName(mLGWorkSchema.getApplyName()); //申请人名称
                mLPEdorAppSchema.setAppType(mLGWorkSchema.getAcceptWayNo()); //申请方式
                mLPEdorAppSchema.setEdorAppDate(mLGWorkSchema.getAcceptDate()); //申请日期
                mLPEdorAppSchema.setManageCom(mGlobalInput.ManageCom); //管理机构
                mLPEdorAppSchema.setEdorState("1");
                mLPEdorAppSchema.setOperator(mGlobalInput.Operator);
                mLPEdorAppSchema.setMakeDate(mCurrentDate);
                mLPEdorAppSchema.setMakeTime(mCurrentTime);
                mLPEdorAppSchema.setModifyDate(mCurrentDate);
                mLPEdorAppSchema.setModifyTime(mCurrentTime);
                map.put(mLPEdorAppSchema, "INSERT");
                mLGWorkSchema.setDetailWorkNo(tWorkNo);
            }

            map.put(mLGWorkSchema, "INSERT"); //插入

            //合同处理关联表
            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setContNo(mContNo);
            if (!tLCContDB.getInfo()) {
            	LBContDB tLBContDB = new LBContDB();
            	tLBContDB.setContNo(mContNo);
            	if(!tLBContDB.getInfo())
            	{
            		 mErrors.addOneError("保单查询失败！");
                     return false;
            	}               
            }

            LLContDealSchema tLLContDealSchema = new LLContDealSchema();
            tLLContDealSchema.setEdorNo(mLGWorkSchema.getWorkNo());
            tLLContDealSchema.setGrpContNo(tLCContDB.getGrpContNo());
            tLLContDealSchema.setContNo(tLCContDB.getContNo());
            tLLContDealSchema.setCaseNo(mLLCaseSchema.getCaseNo());
            tLLContDealSchema.setRgtNo(mLLCaseSchema.getRgtNo());
            tLLContDealSchema.setInsuredNo(mLLCaseSchema.getCustomerNo());
            tLLContDealSchema.setInsuredName(mLLCaseSchema.getCustomerName());
            tLLContDealSchema.setAppDate(mCurrentDate);
            tLLContDealSchema.setAppOperator(mOperator);

            LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
            tLLClaimUserDB.setUserCode(mOperator);
            if (!tLLClaimUserDB.getInfo()) {
                mErrors.addOneError("客户权限查询失败！");
                return false;
            }

            tLLContDealSchema.setAppGrade(tLLClaimUserDB.getClaimPopedom());
            tLLContDealSchema.setManageCom(mGlobalInput.ManageCom);
            tLLContDealSchema.setOperator(mOperator);
            tLLContDealSchema.setMakeDate(mCurrentDate);
            tLLContDealSchema.setMakeTime(mCurrentTime);
            tLLContDealSchema.setModifyDate(mCurrentDate);
            tLLContDealSchema.setModifyTime(mCurrentTime);

            mLLCaseSchema.setContDealFlag("0");//null-未申请、0-已申请 1-待审批 2-已完成

            map.put(tLLContDealSchema, "INSERT");
            map.put(mLLCaseSchema,"UPDATE");

            return true;
        } else { //if, 修改
            String date = mLGWorkSchema.getAcceptDate();
            if (date == null) {
                date = mCurrentDate;
            }
            String sqlUpdate =
                    "update LGWork " +
                    "set nodeNo=to_char(to_number(nodeNo)), " +
                    "StatusNo='3', " +
                    "PriorityNo='" + mLGWorkSchema.getPriorityNo() + "', " +
                    "TypeNo='" + mLGWorkSchema.getTypeNo() + "', " +
                    "CustomerNo='" + mLGWorkSchema.getCustomerNo() + "', " +
                    "DateLimit='" + mLGWorkSchema.getDateLimit() + "', " +
                    "ApplyTypeNo='" + mLGWorkSchema.getApplyTypeNo() + "', " +
                    "ApplyName='" + mLGWorkSchema.getApplyName() + "', " +
                    "AcceptWayNo='" + mLGWorkSchema.getAcceptWayNo() + "', " +
                    "AcceptDate='" + date + "', " +
                    "AcceptCom='" + mLGWorkSchema.getAcceptCom() + "', " +
                    "AcceptorNo='" + mLGWorkSchema.getAcceptorNo() + "', " +
                    "Remark='" + mLGWorkSchema.getRemark() + "' " +
                    "where workNo='" + mLGWorkSchema.getWorkNo() + "' ";

            //获得当前节点号
            String sqlStr =
                    "select to_char(max(to_number(nodeNo)) + 1) from LGWorkTrace " +
                    "where workNo='" + mLGWorkSchema.getWorkNo() + "' ";
            SSRS tSSRS2 = new SSRS();
            ExeSQL tExeSQL2 = new ExeSQL();

            tSSRS2 = tExeSQL2.execSQL(sqlStr);
            String strNodeNo = tSSRS2.GetText(1, 1);

            //获得当前信箱号
            sqlStr = "select WorkBoxNo from LGWorkBox " +
                     "where OwnerNo='" + mGlobalInput.Operator + "' ";
            tSSRS2 = tExeSQL2.execSQL(sqlStr);
            String workBoxNo = tSSRS2.GetText(1, 1);

            LGWorkTraceSchema wt = new LGWorkTraceSchema();
            wt.setWorkNo(mLGWorkSchema.getWorkNo());
            wt.setNodeNo(strNodeNo);
            wt.setWorkBoxNo(workBoxNo);
            wt.setInMethodNo("5");
            wt.setInDate(mCurrentDate);
            wt.setInTime(mCurrentTime);
            wt.setOperator(mGlobalInput.Operator);
            wt.setMakeDate(mCurrentDate);
            wt.setMakeTime(mCurrentTime);
            wt.setModifyDate(mCurrentDate);
            wt.setModifyTime(mCurrentTime);

            map.put(sqlUpdate, "UPDATE"); //修改
            map.put(wt, "INSERT");

            return true;
        }
    }

    //查询客户名称
    private String getCustomerName(String customerNo) {
        //个人客户
        if (customerNo.length() == 9) {
            LDPersonDB db = new LDPersonDB();
            db.setCustomerNo(customerNo);
            db.getInfo();
            if (db != null && db.getName() != null && !db.getName().equals("")) {
                return db.getName();
            }
        }
        //团体客户
        else if (customerNo.length() == 8) {
            LDGrpDB db = new LDGrpDB();
            db.setCustomerNo(customerNo);
            db.getInfo();
            if (db != null && db.getGrpName() != null
                && !db.getGrpName().equals("")) {
                return db.getGrpName();
            }
        }

        //没有查到相应的客户名
        mErrors.addOneError("没有查到客户号为" + customerNo + "的客户名");

        return "";
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData() {
        mInputData.clear();
        mInputData.add(map);
        mResult.add(mLGWorkSchema);
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult() {
        return mResult;
    }

    /**
     * 校验权限
     * @return boolean
     */
    private boolean checkCaseInfo() {
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mLLCaseSchema.getCaseNo());
        if (!tLLCaseDB.getInfo()) {
            mErrors.addOneError("案件信息查询失败！");
            return false;
        }
        mLLCaseSchema = tLLCaseDB.getSchema();

//        if (!"04".equals(mLLCaseSchema.getRgtState())){
//            mErrors.addOneError("该状态不能进行合同处理！");
//            return false;
//        }

//        if (!mOperator.equals(mLLCaseSchema.getHandler())) {
//            mErrors.addOneError("该用户没有处理权限！");
//            return false;
//        }

        return true;
    }

    public static void main(String arg[]) {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "001";

        //输入参数
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setCustomerNo("00000001");
        tLGWorkSchema.setTypeNo("03");
        tLGWorkSchema.setDateLimit("6");
        tLGWorkSchema.setApplyTypeNo("3");
        tLGWorkSchema.setApplyName("请求权");
        tLGWorkSchema.setPriorityNo("3");
        tLGWorkSchema.setAcceptWayNo("6");
        tLGWorkSchema.setAcceptDate("2006-3-6");
        tLGWorkSchema.setAcceptCom("86");
        tLGWorkSchema.setAcceptorNo("001");
        tLGWorkSchema.setRemark("aaaaaaaaaaaaaaaaaa");

        VData tVData = new VData();
        tVData.add(tLGWorkSchema);
        tVData.add(tGI);

//        TaskInputBL tTaskInputBL = new TaskInputBL();
//        if (tTaskInputBL.submitData(tVData, "INSERT||MAIN") == false) {
//            System.out.println("false");
//        }
    }
}
