package com.sinosoft.lis.llcase;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 案件－立案－分案保单明细的查询处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @version 1.0
 */


public class CaseReceiptQueryBLS
{
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();


  public CaseReceiptQueryBLS() {}

  public static void main(String[] args)
  {
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    System.out.println("Start CaseReceiptQuery BLS Submit...");
    mInputData=null;
    return true;
  }

}