package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:拒赔案件类
 * </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author Xx
 * @version 1.0
 */
public class ClaimDeclineUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;


  public ClaimDeclineUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    ClaimDeclineBL tClaimDeclineBL = new ClaimDeclineBL();

    System.out.println("---UI BEGIN---");
    if (tClaimDeclineBL.submitData(cInputData,mOperate) == false)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tClaimDeclineBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimDeclineUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
        mResult = tClaimDeclineBL.getResult();
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

 /* public static void main(String[] args)
  {
    ClaimDeclineUI tClaimDeclineUI=new ClaimDeclineUI();
    VData tVData=new VData();
    LLClaimDeclineSchema tLLClaimDeclineSchema=new LLClaimDeclineSchema();
    tLLClaimDeclineSchema.setCaseNo("1");
    tLLClaimDeclineSchema.setRgtNo("sdf");
    tVData.add(tLLClaimDeclineSchema);
    tClaimDeclineUI.submitData(tVData,"UPDATE");
  }
  */
}
