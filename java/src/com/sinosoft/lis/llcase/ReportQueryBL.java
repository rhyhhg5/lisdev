package com.sinosoft.lis.llcase;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: ReportQueryBL</p>
 * <p>Description: 根据号码和号码类型进行查询数据并且显示的文件</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author :LiuYansong
 * @version 1.0
 */
public class ReportQueryBL
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData ;
  private VData mResult = new VData();
  private String mOperate;

  //接收从jsp中传递来的变量
  String RptObj="";
  String RptObjNo="";
  String PeopleType="";
  private GlobalInput mG = new GlobalInput();
  String aMngCom="";
  public ReportQueryBL() {}

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    if (!getInputData(cInputData))
      return false;

    if (cOperate.equals("QUERY||MAIN"))
    {
      if(!queryData())
        return false;
    }
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    if (mOperate.equals("QUERY||MAIN"))
    {
      RptObj       = (String)cInputData.get(0);
      RptObjNo     = (String)cInputData.get(1);
      PeopleType   = (String)cInputData.get(2);
      mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
      //aMngCom      = (String)cInputData.get(3);
    }
     return true;
  }

  private boolean queryData()
  {
    String tsql="";
    ExeSQL exesql = new ExeSQL();
    SSRS ssrs = new SSRS();
    if (RptObj.equals("0"))
    {
      tsql = "select InsuredNo,InsuredName from LCPol where GrpPolNo = '"+RptObjNo+"' and ManageCom like '"+mG.ManageCom.trim()+"%' ";
      //tsql = "select InsuredNo,InsuredName from LCPol where GrpPolNo = '"+RptObjNo+"' and ManageCom like '"+aMngCom+"%' ";
      System.out.println("团单时的查询语句是"+tsql);
    }
    if(RptObj.equals("1"))
    {
      //添加对无名单及公共账户的控制,若是无名单或者是公共账户，则直接返回错误信息，提示其先进行补名单，在进行报案操作
      System.out.println("添加无名单的处理");
      LCPolBL aLCPolBL = new LCPolBL();
      aLCPolBL.setPolNo(RptObjNo);

      if(!aLCPolBL.getInfo())
      {
        CError tError = new CError();
        tError.moduleName = "PrintBillBL";
        tError.functionName = "submitData";
        tError.errorMessage = "该客户在本管理机构下没有相应的保单！";
        this.mErrors .addOneError(tError) ;
        return false;
      }
      if(!(aLCPolBL.getSchema().getPolTypeFlag()==null||aLCPolBL.getSchema().getPolTypeFlag().equals("")))
      {
        //当无为空时对其进行判断
        if(!aLCPolBL.getSchema().getPolTypeFlag().equals("0"))
        {
          CError tError = new CError();
          tError.moduleName = "PrintBillBL";
          tError.functionName = "submitData";
          tError.errorMessage = "该保单是无名单或者是公共账户，请先对其进行补名单处理，在进行报案！";
          this.mErrors .addOneError(tError) ;
          return false;
        }
      }
      tsql = "select InsuredNo,InsuredName,AppntNo,AppntName from LCPol where PolNo = '"+RptObjNo+"' and ManageCom like '"+mG.ManageCom.trim()+"%' "
           +" union select InsuredNo,InsuredName,AppntNo,AppntName from LBPol where PolNo = '"+RptObjNo+"' and ManageCom like '"+mG.ManageCom.trim()+"%' ";
      System.out.println("个单时的查询语句是"+tsql);
    }
    if(RptObj.equals("2"))
    {
      SSRS query_ssrs = new SSRS();

      String query_sql = "select polno from LCPol where ManageCom like '"+mG.ManageCom.trim()+"%' "
                       +"and ( InsuredNo = '"+RptObjNo+"' or AppntNo = '"+RptObjNo+"' )"
                       +" union "
                       + "select polno from LBPol where ManageCom like '"+mG.ManageCom.trim()+"%' "
                       +"and ( InsuredNo = '"+RptObjNo+"' or AppntNo = '"+RptObjNo+"' )";

      System.out.println("是客户号码时，查询的语句是"+query_sql);
      query_ssrs = exesql.execSQL(query_sql);
      if(query_ssrs.getMaxRow()==0)
      {
        CError tError = new CError();
        tError.moduleName = "PrintBillBL";
        tError.functionName = "submitData";
        tError.errorMessage = "该客户在本管理机构下没有相应的保单！";
        this.mErrors .addOneError(tError) ;
        return false;
      }
      if(query_ssrs.getMaxRow()>0)
      {
        tsql = "select CustomerNo,Name from LDPerson where CustomerNo = '"+RptObjNo+"'";
        System.out.println("当录入的是客户号码时的查询语句是"+tsql);
      }
    }
    ssrs = exesql.execSQL(tsql);
    if(ssrs.getMaxRow()==0)
    {
      CError tError = new CError();
      tError.moduleName = "PrintBillBL";
      tError.functionName = "submitData";
      tError.errorMessage = "没有到相关数据，请您确认数据的正确性或者是该号码不属于本管理机构！！！";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    mResult.clear();
    mResult.add(ssrs);
    return true;
  }
  public static void main(String[] args)
  {
    String aPeople = "0";
    String aRptObj = "2";
    String aRptObjNo = "0000000175";
    String aMngCom = "8633";
    VData tVData = new VData();

    tVData.addElement(aRptObj);
    tVData.addElement(aRptObjNo);
    tVData.addElement(aPeople);
    tVData.addElement(aMngCom);
    ReportQueryBL aReportQueryBL = new ReportQueryBL();
    aReportQueryBL.submitData(tVData,"QUERY||MAIN");
  }
}
