package com.sinosoft.lis.llcase;

import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 理赔赔案保单核赔业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author：zhangtao
 * @version：1.0
 * @CreateDate：2005-02-20
 */
public class ClaimManChkBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  /** 业务处理相关变量 */
  /** 保单明细 */
  private LCPolSchema mLCPolSchema = new LCPolSchema();
  /** 赔案号码 */
  private String mClmNo = "";
  /** 核赔员代码 */
  private String mOperator;
  /** 赔案名细 */
  private LLClaimPolicySchema mLLClaimPolicySchema = new LLClaimPolicySchema();
  /** 赔案主表 */
  private LLClaimSchema mLLClaimSchema = new LLClaimSchema();
  //本次核赔信息
  private LLClaimUnderwriteSchema mLLClaimUnderwriteSchema = new
          LLClaimUnderwriteSchema();
  private LLClaimUnderwriteSet mLLClaimUnderwriteSet = new
          LLClaimUnderwriteSet();
  //上一次案件核赔信息
  private LLClaimUWMainSchema mLastLLClaimUWMainSchema = new
          LLClaimUWMainSchema();
  /** 核赔子表 */
  private LLClaimUWDetailSchema mLLClaimUWDetailSchema = new
          LLClaimUWDetailSchema();
  private LLClaimUWDetailSet mLLClaimUWDetailSet = new LLClaimUWDetailSet();
  /**用户表信息 */
  private LLClaimUserSchema mLLClaimUserSchema = new LLClaimUserSchema();
  /**用户登陆信息 */
  private GlobalInput mGlobalInput = new GlobalInput();
  /**核赔错误信息表 */
  private LLClaimErrorSet mLLClaimErrorSet = new LLClaimErrorSet();
  /**立案主表 */
  private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();

  public ClaimManChkBL() {}

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   * @param: cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone();

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
    {
        return false;
    }

System.out.println("after getInputData");

    //根据赔案号、保单号查询保单、赔案、核赔、核赔错误、用户权限信息
    if (!getBaseData())
    {
        return  false;
    }

System.out.println("after getBaseData");

    //校验数据
    if (!checkApprove())
    {
      return false;
    }

System.out.println("after checkApprove");

    //数据操作业务处理
    if (!dealData())
    {
        return false;
    }

System.out.println("after dealData");

    //准备给后台的数据
    if (!prepareOutputData())
    {
       return false;
    }

System.out.println("after prepareOutputData");

    //数据提交
    ClaimManChkBLS tClaimManChkBLS = new ClaimManChkBLS();

    if (!tClaimManChkBLS.submitData(mInputData, "INSERT"))
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tClaimManChkBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimManChkBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }

  private boolean dealData()
  {
      //********************************
      // 确定当前核赔员是否有核赔权限；
      // 确定本次核赔后是否能完成保单核赔；
      // 确定下次核赔的核赔员级别；
      //********************************

    //判断核赔人员是否有核赔权限
    String tClaimPopedom = mLLClaimUserSchema.getClaimPopedom();
    System.out.println(tClaimPopedom);
//====== del for test ===== 2005-02-24 ==== end ==============================
//    if (!checkClaimPopedom(tClaimPopedom.trim()))
//    {
//        return false;
//    }
//====== del for test ===== 2005-02-24 ==== end ==============================

    //核赔级别
    mLLClaimUnderwriteSchema.setClmUWGrade(tClaimPopedom);

    if (mLastLLClaimUWMainSchema.getAppActionType() == null ||
        mLastLLClaimUWMainSchema.getAppActionType().equals(""))
    {
        //上次申请动作为空表示初次审核
        System.out.println("初次审核");

        if (mLLRegisterSchema.getHandler() == null ||
            mLLRegisterSchema.getHandler().equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "ClaimManChkBL";
            tError.functionName = "dealData";
            tError.errorMessage = "立案时未指定审核人员";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!mLLRegisterSchema.getHandler().trim().
            equals(mOperator))
        {
            //取立案时确定的案件审核人员编码与当前用户比较
            CError tError = new CError();
            tError.moduleName = "ClaimManChkBL";
            tError.functionName = "dealData";
            tError.errorMessage = "审核人员不符，立案指定审核人员为：" +
                                  mLLRegisterSchema.getHandler().trim();
            this.mErrors.addOneError(tError);
            return false;
        }

        //确定本次保单核赔的申请级别
        if (!getAppGrade())
        {
            return false;
        }

        //设定本次保单核赔的审核类型为[初次审核]
        mLLClaimUnderwriteSchema.setCheckType("0");
    }
    else
    {
            //上次申请动作不为空，
            //表示已作过审核待复核签批或者是复核时退回重新审核

            if (mLastLLClaimUWMainSchema.getAppActionType().equals("1"))
            {
                //已经完成签批
                CError tError = new CError();
                tError.moduleName = "ClaimManChkBL";
                tError.functionName = "dealData";
                tError.errorMessage = "已经完成签批";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (mLastLLClaimUWMainSchema.getAppActionType().equals("2"))
            {
                //上次复核时退回重新审核，本次核赔为签批退回审核

                //如果上次复核制定审核人员，
                //检查本次审核人员是否与制定审核人员一致
                if (mLastLLClaimUWMainSchema.getAppClmUWer() != null &&
                    !mLastLLClaimUWMainSchema.getAppClmUWer().equals(""))
                {
                    if (!mLastLLClaimUWMainSchema.getAppClmUWer().trim().
                        equals(mOperator))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ClaimManChkBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "审核人员不符，复核人员指定的审核人员为：" +
                                              mLastLLClaimUWMainSchema.
                                              getAppClmUWer().trim();
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
                else
                {
                        //取立案时确定的案件审核人员编码与当前用户比较
                        if (!mLLRegisterSchema.getHandler().trim().equals(
                                mGlobalInput.Operator))
                        {
                            CError tError = new CError();
                            tError.moduleName = "ClaimManChkBL";
                            tError.functionName = "submitData";
                            tError.errorMessage = "审核人员不符，立案指定的重审的人员为：" +
                                                  mLLRegisterSchema.
                                                  getHandler().trim();
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                }

                //确定本次保单核赔的申请级别
                if (!getAppGrade())
                {
                    return false;
                }

                //设定本次保单核赔的审核类型为[签批退回审核]
                mLLClaimUnderwriteSchema.setCheckType("1");
            }

            if (mLastLLClaimUWMainSchema.getAppActionType().equals("3"))
            {
                //上次核赔的申请动作为上报
                CError tError = new CError();
                tError.moduleName = "ClaimManChkBL";
                tError.functionName = "submitData";
                tError.errorMessage = "案件上报，不允许修改核赔信息";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

    return true;
  }

  private boolean getInputData(VData cInputData)
  {
      mGlobalInput.setSchema(
              (GlobalInput)
              cInputData.getObjectByObjectName("GlobalInput", 0));

      mOperator = mGlobalInput.Operator;

      mLLClaimUnderwriteSchema =
            (LLClaimUnderwriteSchema)
            cInputData.getObjectByObjectName("LLClaimUnderwriteSchema", 0);

      return true;
  }

  private boolean getBaseData()
  {
      //查询保单表LCPol、LBPol
      LCPolBL tLCPolBL = new LCPolBL();
      tLCPolBL.setPolNo(mLLClaimUnderwriteSchema.getPolNo());
      if (tLCPolBL.getInfo() == false)
      {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCPolBL.mErrors);
          CError tError = new CError();
          tError.moduleName = "UWAutoChkBL";
          tError.functionName = "getInputData";
          tError.errorMessage = "在LCPol和LBPol表中查询数据失败！";
          this.mErrors.addOneError(tError);
          return false;
      }
      if (tLCPolBL.getPolNo() == null || tLCPolBL.getPolNo().equals(""))
      {
          this.mErrors.copyAllErrors(tLCPolBL.mErrors);
          CError tError = new CError();
          tError.moduleName = "UWAutoChkBL";
          tError.functionName = "getInputData";
          tError.errorMessage = "在LCPol和LBPol表中均查询不到" +
                                mLLClaimUnderwriteSchema.getPolNo() + "号保单！";
          this.mErrors.addOneError(tError);
          return false;
      }
      mLCPolSchema.setSchema(tLCPolBL.getSchema());

      //查询赔案主表LLClaim
      mClmNo = mLLClaimUnderwriteSchema.getClmNo();
      LLClaimDB tLLClaimDB = new LLClaimDB();
      tLLClaimDB.setClmNo(mClmNo);
      tLLClaimDB.getInfo();
      if (tLLClaimDB.getInfo() == false)
      {
          // @@错误处理
          this.mErrors.copyAllErrors(tLLClaimDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ClaimManChkBL";
          tError.functionName = "submitData";
          tError.errorMessage = "赔案主表信息LLClaim查询失败!";
          this.mErrors.addOneError(tError);
          return false;
      }
      mLLClaimSchema.setSchema(tLLClaimDB.getSchema());

      //查询赔案明细表LLClaimPolicy
      LLClaimPolicyDB mLLClaimPolicyDB = new LLClaimPolicyDB();
      mLLClaimPolicyDB.setClmNo(mClmNo);
      mLLClaimPolicyDB.setPolNo(mLLClaimUnderwriteSchema.getPolNo());
      if (mLLClaimPolicyDB.getInfo() == false)
      {
          // @@错误处理
          this.mErrors.copyAllErrors(mLLClaimPolicyDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ClaimManChkBL";
          tError.functionName = "submitData";
          tError.errorMessage = "赔案明细信息LLClaimPolicy查询失败!";
          this.mErrors.addOneError(tError);
          return false;
      }
      mLLClaimPolicySchema.setSchema(mLLClaimPolicyDB.getSchema());

      //查询案件核赔表LLClaimUWMain
      LLClaimUWMainDB mLLClaimUWMainDB = new LLClaimUWMainDB();
      mLLClaimUWMainDB.setClmNo(mClmNo);
      int tCount = mLLClaimUWMainDB.getCount();
      if (tCount > 0)
      {
          if (mLLClaimUWMainDB.getInfo() == false)
          {
              // @@错误处理
              this.mErrors.copyAllErrors(mLLClaimUWMainDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "ClaimManChkBL";
              tError.functionName = "submitData";
              tError.errorMessage = "案件核赔信息LLClaimUWMain查询失败!";
              this.mErrors.addOneError(tError);

          }
      }
      mLastLLClaimUWMainSchema.setSchema(mLLClaimUWMainDB.getSchema());

      //查询核赔错误信息LLClaimError
      LLClaimErrorDB tLLClaimErrorDB = new LLClaimErrorDB();
      tLLClaimErrorDB.setClmNo(mClmNo);
      tLLClaimErrorDB.setPolNo(mLLClaimPolicySchema.getPolNo());
      mLLClaimErrorSet.set(tLLClaimErrorDB.query());
      if (tLLClaimErrorDB.mErrors.needDealError())
      {
          this.mErrors.copyAllErrors(tLLClaimErrorDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ClaimManChkBL";
          tError.functionName = "submitData";
          tError.errorMessage = "核赔错误信息LLClaimError查询失败!";
          this.mErrors.addOneError(tError);
      }

      //查询用户信息LLClaimUser
      LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
      tLLClaimUserDB.setUserCode(mOperator);
      tLLClaimUserDB.getInfo();
      if (tLLClaimUserDB.mErrors.needDealError() == true)
      {
          // @@错误处理
          this.mErrors.copyAllErrors(tLLClaimUserDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ClaimManChkBL";
          tError.functionName = "getUser";
          tError.errorMessage = "用户信息LLClaimUser查询失败!";
          this.mErrors.addOneError(tError);
          return false;
      }
      mLLClaimUserSchema.setSchema(tLLClaimUserDB.getSchema());

//====== del for test ===== 2005-02-23 ==== start ============================
//    if (mLLClaimUserSchema.getClaimPopedom() == null)
//    {
//      // @@错误处理
//      CError tError = new CError();
//      tError.moduleName = "ClaimManChkBL";
//      tError.functionName = "getUser";
//      tError.errorMessage = "用户无核赔权限!";
//      this.mErrors.addOneError(tError);
//      return false;
//    }
//====== del for test ===== 2005-02-23 ==== end ==============================

      return true;

  }

  /**
   * 校验保单是否已经完成核赔
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkApprove( )
  {
      //查询立案信息
      LLRegisterDB tLLRegisterDB = new LLRegisterDB();
      tLLRegisterDB.setRgtNo(mLLClaimUnderwriteSchema.getRgtNo());
      if (!tLLRegisterDB.getInfo())
      {
        CError tError = new CError();
        tError.moduleName = "ClaimManChkBL";
        tError.functionName = "checkApprove";
        tError.errorMessage = "立案信息查询失败";
        this.mErrors .addOneError(tError) ;
        return false;
      }

//====== del for test ===== 2005-02-23 ==== start ============================
//    if (tLLRegisterDB.getClmState().equals("5"))
//    {
//      CError tError = new CError();
//      tError.moduleName = "ClaimManChkBL";
//      tError.functionName = "checkApprove";
//      tError.errorMessage = "案件已提起调查，不能核赔";
//      this.mErrors .addOneError(tError) ;
//      return false;
//    }
//====== del for test ===== 2005-02-23 ==== end ==============================

      mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());

    if (!mLLClaimSchema.getClmState().equals("1"))
    {
      CError tError = new CError();
      tError.moduleName = "ClaimManChkBL";
      tError.functionName = "checkApprove";
      tError.errorMessage = "案件在当前状态下，不能做保单核赔";
      this.mErrors .addOneError(tError) ;
      return false;
    }

//====== del for test ===== 2005-02-23 ==== start ============================
//    if (mLLClaimPolicySchema.getClmState().equals("1") )
//    {
//      // @@错误处理
//      CError tError = new CError();
//      tError.moduleName = "ClaimAutoChkBL";
//      tError.functionName = "checkApprove";
//      tError.errorMessage = "请先进行自动核赔!（保单号：" +
//                            mLLClaimPolicySchema.getPolNo().trim() + "）";
//      this.mErrors .addOneError(tError) ;
//      return false;
//    }
//====== del for test ===== 2005-02-23 ==== end ==============================

    if (mLLClaimPolicySchema.getClmState().equals("2") )
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ClaimAutoChkBL";
      tError.functionName = "checkApprove";
      tError.errorMessage = "已经完成核赔!（保单号：" +
                            mLLClaimPolicySchema.getPolNo().trim() + "）";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    if (mLLClaimPolicySchema.getClmState().equals("3") )
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ClaimAutoChkBL";
      tError.functionName = "checkApprove";
      tError.errorMessage = "已经完成给付!（保单号：" +
                            mLLClaimPolicySchema.getPolNo().trim() + "）";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    if (mLLClaimPolicySchema.getClmState().equals("4") )
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ClaimAutoChkBL";
      tError.functionName = "checkApprove";
      tError.errorMessage = "已经拒赔!（保单号：" +
                            mLLClaimPolicySchema.getPolNo().trim() + "）";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    if (mLLClaimPolicySchema.getClmState().equals("5") )
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ClaimAutoChkBL";
      tError.functionName = "checkApprove";
      tError.errorMessage = "已提起调查!（保单号：" +
                            mLLClaimPolicySchema.getPolNo().trim() + "）";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;

  }

  private boolean prepareOutputData()
  {
    int tCount;

    //准备核赔记录数据LLClaimUnderwrite
    mLLClaimUnderwriteSchema.setClmNo(mClmNo);
    mLLClaimUnderwriteSchema.setRgtNo(mLLClaimSchema.getRgtNo());
    mLLClaimUnderwriteSchema.setContNo(mLCPolSchema.getContNo());
    mLLClaimUnderwriteSchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
    mLLClaimUnderwriteSchema.setPolNo(mLCPolSchema.getPolNo());
    mLLClaimUnderwriteSchema.setKindCode(mLCPolSchema.getKindCode());
    mLLClaimUnderwriteSchema.setRiskCode(mLCPolSchema.getRiskCode());
    mLLClaimUnderwriteSchema.setRiskVer(mLCPolSchema.getRiskVersion());
    mLLClaimUnderwriteSchema.setPolMngCom(mLCPolSchema.getManageCom());
    mLLClaimUnderwriteSchema.setSaleChnl(mLCPolSchema.getSaleChnl());
    mLLClaimUnderwriteSchema.setAgentCode(mLCPolSchema.getAgentCode());
    mLLClaimUnderwriteSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
    mLLClaimUnderwriteSchema.setInsuredNo(mLCPolSchema.getInsuredNo());
    mLLClaimUnderwriteSchema.setInsuredName(mLCPolSchema.getInsuredName());
    mLLClaimUnderwriteSchema.setAppntNo(mLCPolSchema.getAppntNo());
    mLLClaimUnderwriteSchema.setAppntName(mLCPolSchema.getAppntName());
    mLLClaimUnderwriteSchema.setCValiDate(mLCPolSchema.getCValiDate());
    mLLClaimUnderwriteSchema.setPolState(mLCPolSchema.getPolState());
    mLLClaimUnderwriteSchema.setStandPay(mLLClaimPolicySchema.getStandPay());
    mLLClaimUnderwriteSchema.setRealPay(mLLClaimPolicySchema.getRealPay());
    mLLClaimUnderwriteSchema.setClmUWer(mOperator);
    //mLLClaimUnderwriteSchema.setClmUWGrade 之前已经付值
    //mLLClaimUnderwriteSchema.AutoClmDecision 之前已经付值
    //mLLClaimUnderwriteSchema.ClmDecision  之前已经付值
    mLLClaimUnderwriteSchema.setMngCom(mLCPolSchema.getManageCom());
    mLLClaimUnderwriteSchema.setMakeDate(PubFun.getCurrentDate());
    mLLClaimUnderwriteSchema.setMakeTime(PubFun.getCurrentTime());
    mLLClaimUnderwriteSchema.setModifyDate(PubFun.getCurrentDate());
    mLLClaimUnderwriteSchema.setModifyTime(PubFun.getCurrentTime());
    //mLLClaimUnderwriteSchema.setAppGrade() 之前已经付值
    //mLLClaimUnderwriteSchema.setCheckType() 之前已经付值
    //mLLClaimUnderwriteSchema.setAppClmUWer() 案件核赔确认时重置
    //mLLClaimUnderwriteSchema.setAppActionType() 案件核赔确认时重置

    //准备核赔履历记录数据LLClaimUWDetail
    LLClaimUWDetailDB tmLLClaimUWDetailDB = new LLClaimUWDetailDB();
    tmLLClaimUWDetailDB.setClmNo(mClmNo);
    tmLLClaimUWDetailDB.setPolNo(mLCPolSchema.getPolNo());
    tCount = tmLLClaimUWDetailDB.getCount();
    tCount++;
    mLLClaimUWDetailSchema.setClmNo(mClmNo);
    mLLClaimUWDetailSchema.setClmUWNo(String.valueOf(tCount));
    mLLClaimUWDetailSchema.setContNo(mLCPolSchema.getContNo());
    mLLClaimUWDetailSchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
    mLLClaimUWDetailSchema.setPolNo(mLCPolSchema.getPolNo());
    mLLClaimUWDetailSchema.setKindCode(mLCPolSchema.getKindCode());
    mLLClaimUWDetailSchema.setRiskVer(mLCPolSchema.getRiskVersion());
    mLLClaimUWDetailSchema.setRiskCode(mLCPolSchema.getRiskCode());
    mLLClaimUWDetailSchema.setPolMngCom(mLCPolSchema.getManageCom());
    mLLClaimUWDetailSchema.setSaleChnl(mLCPolSchema.getSaleChnl());
    mLLClaimUWDetailSchema.setAgentCode(mLCPolSchema.getAgentCode());
    mLLClaimUWDetailSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
    mLLClaimUWDetailSchema.setStandPay(mLLClaimPolicySchema.getStandPay());
    mLLClaimUWDetailSchema.setRealPay(mLLClaimPolicySchema.getRealPay());
    //mLLClaimUWDetailSchema.setGetDutyCode(mLLClaimPolicySchema.getGetDutyKind());

    mLLClaimUWDetailSchema.setGetDutyKind(mLLClaimPolicySchema.getGetDutyKind());
    mLLClaimUWDetailSchema.setClmUWer(mOperator);
    mLLClaimUWDetailSchema.setMngCom(mLCPolSchema.getManageCom());
    mLLClaimUWDetailSchema.setMakeDate(PubFun.getCurrentDate());
    mLLClaimUWDetailSchema.setMakeTime(PubFun.getCurrentTime());
    mLLClaimUWDetailSchema.setModifyDate(PubFun.getCurrentDate());
    mLLClaimUWDetailSchema.setModifyTime(PubFun.getCurrentTime());
    mLLClaimUWDetailSchema.setCheckType(mLLClaimUnderwriteSchema.getCheckType());

    mLLClaimUnderwriteSet.add(mLLClaimUnderwriteSchema);
    mLLClaimUWDetailSet.add(mLLClaimUWDetailSchema);

    mInputData.clear();
    mInputData.add( mLLClaimUnderwriteSet );
    mInputData.add( mLLClaimUWDetailSet );

    mResult.clear();
    mResult.add(mLLClaimUnderwriteSchema);

    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  //取得上一级的核赔级别
  private String getUperGrade(String tString)
  {
    if (tString.equals("A")) return "B";
    if (tString.equals("B")) return "C";
    if (tString.equals("C")) return "D";
    if (tString.equals("D")) return "E";
    if (tString.equals("E")) return "F";
    if (tString.equals("F")) return "G";
    if (tString.equals("G")) return "H";
    if (tString.equals("H")) return "I";
    return tString;
  }

  private boolean getAppGrade()
  {
      if (mLLClaimErrorSet.size() == 0)
      {
          //首次核赔，无错误信息
          //确定保单核赔的申请级别为本次核赔级别的上一级别
          mLLClaimUnderwriteSchema.setAppGrade(
                  getUperGrade(mLLClaimUnderwriteSchema.getClmUWGrade()));
          //检查申请级别是否合法
          if (!checkClaimPopedom(mLLClaimUnderwriteSchema.getAppGrade().trim()))
          {
              CError tError = new CError();
              tError.moduleName = "ClaimManChkBL";
              tError.functionName = "submitData";
              tError.errorMessage = "申请级别不合法：" +
                                    mLLClaimUnderwriteSchema.
                                    getAppGrade().trim();
              this.mErrors.addOneError(tError);
              return false;
          }
      }
      else
      {
          //首次核赔，有错误信息，
          //确定本核赔申请级别为错误表中的最高核赔级别;
          System.out.println(
                  "首次核赔，有错误信息，确定本核赔申请级别为错误表中的最高核赔级别");

          String tSQL = "select max(uwgrade) from llclaimerror where clmno='"
                        + mLLClaimPolicySchema.getClmNo().trim() +
                        "' and polno='"
                        + mLLClaimPolicySchema.getPolNo().trim() + "'";
          System.out.println(tSQL);
          ExeSQL tExeSQL = new ExeSQL();
          String tThisGrade = tExeSQL.getOneValue(tSQL);
          if ((mLLClaimUnderwriteSchema.getClmUWGrade().trim().
               equals(tThisGrade.trim())) ||
              mLLClaimUnderwriteSchema.getClmUWGrade().trim().
              compareTo(tThisGrade.trim()) < 0)
          {
              //错误表最高核赔级别与当前核赔级别一致
              //或者本次核赔级别高于错误表中的最高核赔级别，
              //取本次核赔级别上一级别为申请级别；
              mLLClaimUnderwriteSchema.setAppGrade(getUperGrade(
                      mLLClaimUnderwriteSchema.getClmUWGrade()));

              //检查申请级别是否合法
              if (!checkClaimPopedom(mLLClaimUnderwriteSchema.getAppGrade().
                                     trim()))
              {
                  CError tError = new CError();
                  tError.moduleName = "ClaimManChkBL";
                  tError.functionName = "submitData";
                  tError.errorMessage = "申请级别不合法：" +
                                        mLLClaimUnderwriteSchema.
                                        getAppGrade().trim();
                  this.mErrors.addOneError(tError);
                  return false;
              }
          }
          else
          {
                  //错误表的最高核赔级别与当前核赔级别不一致
                  //取最高级别为申请级别
                  mLLClaimUnderwriteSchema.setAppGrade(tThisGrade.trim());

                  //检查申请级别是否合法
                  if (!checkClaimPopedom(mLLClaimUnderwriteSchema.
                                         getAppGrade().trim()))
                  {
                      CError tError = new CError();
                      tError.moduleName = "ClaimManChkBL";
                      tError.functionName = "submitData";
                      tError.errorMessage = "申请级别不合法：" +
                                            mLLClaimUnderwriteSchema.getAppGrade().
                                            trim();
                      this.mErrors.addOneError(tError);
                      return false;
                  }
          }
      }
      return true;
  }

  //检查核赔级别是否为空；
  //检查核赔级别是否合法
  private boolean checkClaimPopedom(String tClaimPopedom)
  {
    if (tClaimPopedom == null)
    {
      CError tError = new CError();
      tError.moduleName = "ClaimManChkBL";
      tError.functionName = "submitData";
      tError.errorMessage = "权限级别为空!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    if (!tClaimPopedom.equals("A")&
        !tClaimPopedom.equals("B")&
        !tClaimPopedom.equals("C")&
        !tClaimPopedom.equals("D")&
        !tClaimPopedom.equals("E")&
        !tClaimPopedom.equals("F")&
        !tClaimPopedom.equals("G")&
        !tClaimPopedom.equals("H")&
        !tClaimPopedom.equals("I")) //代码待定，表示核赔人员的多种权限
    {
        CError tError = new CError();
        tError.moduleName = "ClaimManChkBL";
        tError.functionName = "submitData";
        tError.errorMessage = "没有以下权限级别：" + tClaimPopedom.trim();
        this.mErrors.addOneError(tError);
        return false;
    }

    return true;
  }

  public static void main (String[] args)
  {
      LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new
              LLClaimUnderwriteSchema();

      tLLClaimUnderwriteSchema.setClmNo("520000000000022"); //赔案号
      tLLClaimUnderwriteSchema.setRgtNo("510000000000160"); //立案号
      tLLClaimUnderwriteSchema.setPolNo("110110000019409"); //保单号
      tLLClaimUnderwriteSchema.setAutoClmDecision("1");
      tLLClaimUnderwriteSchema.setClmDecision("1"); //核赔结论


      GlobalInput tG = new GlobalInput();
      tG.Operator = "000012";
      tG.ManageCom = "86";

      VData aVData = new VData();
      aVData.addElement(tLLClaimUnderwriteSchema);
      aVData.addElement(tG);
      ClaimManChkBL aClaimManChkBL = new ClaimManChkBL();
      aClaimManChkBL.submitData(aVData, "INSERT");
  }
}
