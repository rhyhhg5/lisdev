package com.sinosoft.lis.llcase;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.rmi.*;
import javax.xml.rpc.*;
import com.sinosoft.lis.message.*;

import java.util.*;

/**
 * <p>Title: 客户理赔短信通知</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLCustomerMsgBL {
    private GlobalInput mG = new GlobalInput();
    private String mManageCom = "";
    private VData mResult = new VData();
    private MMap mMap = new MMap();
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mLastDate = "";
    private String mCurrentTime = PubFun.getCurrentTime();
    private ExeSQL tExeSQL = new ExeSQL();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private LJSPaySchema mLJSPaySchema = null;

    public LLCustomerMsgBL() {}

    private boolean getInputData(VData cInputData) {
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mManageCom = mG.ManageCom;
        String AheadDays = "-1";
        FDate tD = new FDate();
        Date tLastDate = PubFun.calDate(tD.getDate(mCurrentDate),
                                        Integer.parseInt(AheadDays), "D", null);
       mLastDate = tD.getString(tLastDate);
       // mLastDate="2013-03-20";

        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) {
        cInputData = (VData) cInputData.clone();

        if (!getInputData(cInputData)) {
            return false;
        }
        if (cOperate != null && cOperate.trim().equals("LPMSG")) {
            sendMsg();
        } else {
            mErrors.addOneError(new CError("不支持的操作字符串！"));
            return false;
        }
        return true;
    }

    private boolean sendMsg() {
        System.out.println("客户理赔短信通知批处理开始......");
        SmsServiceSoapBindingStub binding = null;
        try {
            binding = (SmsServiceSoapBindingStub)new SmsServiceServiceLocator().
                      getSmsService(); //创建binding对象
        } catch (javax.xml.rpc.ServiceException jre) {
            jre.printStackTrace();
        }

        binding.setTimeout(60000);
        Response value = null; //声明Response对象，该对象将在提交短信后包含提交的结果。
        SmsMessages msgs = new SmsMessages(); //创建SmsMessages对象，该对象对应于上文下行短信格式中的Messages元素
        msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
        msgs.setExtension("024"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
        msgs.setServiceType("lipei"); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
        msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
        msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
        msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
        msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素

        Vector vec = new Vector();
        vec = getMessage();

        if (!vec.isEmpty()) {
            msgs.setMessages((SmsMessage[]) vec.toArray(new SmsMessage[vec.size()])); //设置该批短信的每一条短信，一批短信可以包含多条短信
            try {
                value = binding.sendSMS(msgs); //提交该批短信，UserName是短信服务平台管理员分配的用户名， Password则是其对应的密码，用户名和密码用于验证发送者，只有验证通过才可能提交短信，msgs即为刚才创建的短信对象。
                System.out.println(value.getStatus());
                System.out.println(value.getMessage());
            } catch (RemoteException ex) {
                ex.printStackTrace();
            }
        } else {
            System.out.print("无符合条件案件！");
        }
        return true;
    }

    private Vector getMessage() {
        Vector tVector = new Vector();

        String tSQL =
//                "SELECT r.mngcom,'0',c.caseno,r.RgtantMobile,r.RgtantName,'0000','001' "
//                + " FROM llcase c,llregister r WHERE "
//                + " c.caseno=r.rgtno AND r.rgtobjno=r.customerno "
//                + " AND c.rgtstate NOT IN ('07','11','12','13','14')"
//                + " AND c.rgttype='1' AND r.rgtantmobile IS NOT NULL "
//                + " AND EXISTS(SELECT 1 FROM lcpol p,lmriskapp ap "
//                + " WHERE p.riskcode=ap.riskcode AND ap.riskprop='I' "
//                + " AND c.customerno=p.insuredno AND p.conttype='1') "
//                + " AND days('" + mLastDate
//                + "')-days(c.rgtdate)=13 union all"
//                +
        	" SELECT r.mngcom, "
        	+ " g.paymode, "
        	+ " c.caseno, "
        	+ " r.RgtantMobile, "
        	+ " r.RgtantName, "
        	+ " g.bankaccno, "
        	+ " g.drawer, "
        	+ " '' ,"
        	+ " g.sumgetmoney "
        	+ " FROM llcase c, llregister r, ljaget g, llclaim cl "
        	+ " WHERE c.caseno = r.rgtno "
        	+ " AND r.rgtobjno = r.customerno "
        	+ " AND c.caseno = cl.caseno "
        	+ " AND  ((g.paymode ='1' and c.rgtstate in('11','12') and g.makedate ='"+ mLastDate+ "') or (g.paymode in ('3', '4', '11') and c.rgtstate='12' and g.confdate ='"+ mLastDate+ "')) "
        	+ " AND c.caseno = g.otherno "
        	+ " AND r.rgtantmobile IS NOT NULL "
        	+ " AND EXISTS (SELECT 1 "
        	+ " FROM llclaimdetail clt, lmriskapp ap "
        	+ " WHERE clt.caseno = c.caseno "
        	+ " AND ap.riskcode = clt.riskcode "
        	+ " AND ap.riskprop = 'I' "
        	+ " AND NOT EXISTS (select 1 from lcgrpcont where grpcontno = clt.grpcontno and cardflag in ('2', '3')))"
        	+ " AND c.rgttype = '1'  "
        	+ " AND c.caseno = cl.caseno "
        	+ " AND cl.givetype in ('1', '2')   "
        	+ " and g.paymode in ('1', '3', '4','11')   "
        	+ " and  not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "
        	+ " union all "
        	+ " SELECT r.mngcom, "
        	+ " g.paymode, "
        	+ " c.caseno, "
        	+ " c.MobilePhone, "
        	+ " c.CustomerName, "
        	+ " g.bankaccno, "
        	+ " g.drawer, "
        	+ " c.customersex, "
        	+ " g.sumgetmoney "
        	+ " FROM llcase c, llregister r, ljaget g, llclaim cl "
        	+ " WHERE c.rgtno = r.rgtno "
        	+ " AND c.caseno = cl.caseno "
        	+ " AND c.rgtstate = '12' "
        	+ " AND c.caseno = g.otherno "
        	+ " AND c.mobilephone IS NOT NULL "
        	+ " AND EXISTS "
        	+ " (SELECT 1 FROM llclaimdetail clt, lmriskapp ap, LLAppClaimReason cr "
        	+ " WHERE clt.caseno = c.caseno "
        	+ " and cr.rgtno = clt.grpcontno "
        	+ " and cr.reasoncode = '97' "
        	+ " and cr.reasontype = '9' "
        	+ " AND ap.riskcode = clt.riskcode "
        	+ " AND ap.riskprop = 'G' "
        	+ " and not exists (select 1 from lcgrpcont where grpcontno=clt.grpcontno and cardflag in('2','3'))) "
        	+ " AND NOT EXISTS (SELECT 1 "
        	+ " FROM llclaimdetail clt, lmriskapp ap "
        	+ " WHERE clt.caseno = c.caseno "
        	+ " AND ap.riskcode = clt.riskcode "
        	+ " AND ap.riskprop = 'I') "
        	+ " AND c.rgttype = '1' "
        	+ " AND cl.givetype in ('1', '2') "
        	+ " and g.paymode in ('3', '4', '11')  "
        	+ " AND g.confdate ='"+ mLastDate+ "' "
        	+ " and  not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "
        	+ " union all "
        	+ " SELECT r.mngcom, "
        	+ " g.paymode, "
        	+ " c.caseno, "
        	+ " r.RgtantMobile, "
        	+ " r.RgtantName,  "
        	+ " g.bankaccno, "
        	+ " g.drawer, "
        	+ " '', "
        	+ " g.sumgetmoney "
        	+ " FROM llcase c, llregister r, ljaget g, llclaim cl "
        	+ " WHERE c.rgtno = r.rgtno "
        	+ " AND c.caseno = cl.caseno "
        	+ " AND  ((g.paymode='1' and c.rgtstate in('11','12') and g.makedate ='"+ mLastDate+ "') or(g.paymode in ('3', '4', '11') and c.rgtstate='12' and g.confdate ='"+ mLastDate+ "')) "
        	+ " AND c.caseno = g.otherno "
        	+ " AND r.RgtantMobile IS NOT NULL "
        	+ " AND EXISTS "
        	+ "  (SELECT 1 "
        	+ " FROM llclaimdetail clt, lcgrpcont cp "
        	+ " WHERE clt.grpcontno = cp.grpcontno and clt.caseno=c.caseno "
        	+ " AND cp.cardflag in ('2','3')) "
        	+ " AND NOT EXISTS (SELECT 1 "
        	+ " FROM llclaimdetail clt, lmriskapp ap "
        	+ " WHERE clt.caseno = c.caseno "
        	+ " AND ap.riskcode = clt.riskcode "
        	+ " AND ap.riskprop = 'I' "
        	+ " AND NOT EXISTS (select 1 from lcgrpcont where grpcontno = clt.grpcontno and cardflag in ('2', '3')))"
        	+ " AND c.rgttype = '1' "
        	+ " AND cl.givetype in ('1', '2') "
        	+ " and g.paymode in ('1','3', '4', '11')  "
        	+ " and  not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "     	
        	+ "union all"
        	+" SELECT r.mngcom, " 
        	+" g.paymode, "
        	+" c.caseno, "
        	+" ins.grpinsuredphone, "
        	+" c.CustomerName, "
        	+" g.bankaccno, "
        	+" g.drawer, "
        	+" c.customersex, "
        	+" g.sumgetmoney "
        	+" FROM llcase c, llregister r, ljaget g, llclaim cl,lcinsured ins "
        	+" WHERE c.rgtno = r.rgtno "
        	+" AND c.caseno = cl.caseno "
        	+" AND c.rgtstate = '12' "
        	+" AND c.caseno = g.otherno "
        	+" AND c.mobilephone IS NULL "
        	+ " AND EXISTS "
        	+ " (SELECT 1 FROM llclaimdetail clt, lmriskapp ap, LLAppClaimReason cr "
        	+ " WHERE clt.caseno = c.caseno "
        	+ " and cr.rgtno = clt.grpcontno "
        	+ " and cr.reasoncode = '97' "
        	+ " and cr.reasontype = '9' "
        	+ " AND ap.riskcode = clt.riskcode "
        	+ " AND ap.riskprop = 'G' "
        	+ " and not exists (select 1 from lcgrpcont where grpcontno=clt.grpcontno and cardflag in('2','3'))) "
        	+" and c.customerno = ins.insuredno "
        	+" and (select contno from llclaimdetail where caseno=c.caseno fetch first 1 rows only)=ins.contno " 
        	+" and ins.grpinsuredphone is not null "
        	+" and g.sumgetmoney>0 "
        	+" AND NOT EXISTS (SELECT 1 " 
        	+" FROM llclaimdetail clt, lmriskapp ap " 
        	+" WHERE clt.caseno = c.caseno "
        	+" AND ap.riskcode = clt.riskcode " 
        	+" AND ap.riskprop = 'I') "
        	+" AND c.rgttype = '1' " 
        	+" AND cl.givetype in ('1', '2') " 
        	+" and g.paymode in ('3', '4', '11') " 
        	+" and r.togetherflag='1' "  
        	+" and r.applyertype in ('2','5') "
        	+" AND g.confdate ='"+ mLastDate+ "' "
        	+" and  not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "
        	+ " with ur";
        
        System.out.println(tSQL);

        SSRS tMsgSSRS = tExeSQL.execSQL(tSQL);

        for (int i = 1; i <= tMsgSSRS.getMaxRow(); i++) {
            String tManageCom = tMsgSSRS.GetText(i, 1);
            String tPayMode = tMsgSSRS.GetText(i, 2);
            String tCaseNo = tMsgSSRS.GetText(i, 3);
            String tMobile = tMsgSSRS.GetText(i, 4);
            //String tMobile ="18600365715";
            String tCustomerName = tMsgSSRS.GetText(i, 5);
            String tBankAccNo = tMsgSSRS.GetText(i, 6);
            String tDrawer = tMsgSSRS.GetText(i, 7);
            String tSex = tMsgSSRS.GetText(i, 8);
            String tPay = tMsgSSRS.GetText(i, 9);

            if (tManageCom == null || "".equals(tManageCom) ||
                "null".equals(tManageCom)) {
                continue;
            }

            if (tPayMode == null || "".equals(tPayMode) ||
                "null".equals(tPayMode)) {
                continue;
            }

            if (tMobile == null || "".equals(tMobile) ||
                "null".equals(tMobile)) {
                continue;
            }

            if (tCustomerName == null || "".equals(tCustomerName) ||
                "null".equals(tCustomerName)) {
                continue;
            }

            if (tDrawer == null || "".equals(tDrawer) ||
                "null".equals(tDrawer)) {
                continue;
            }
            
            if (tPay == null || "".equals(tPay) ||
                    "null".equals(tPay)) {
                    continue;
                }
            String tSexContent = "女士/先生";
            if ("0".equals(tSex)) {
            	tSexContent = "先生";
            } else if ("1".equals(tSex)) {
            	tSexContent = "女士";
            }

            String tContents = "";

//            if ("0".equals(tPayMode)) {
//                tContents = tCustomerName +
//                            "女士/先生，您的理赔申请正在处理中，处理完毕后将及时通知您。";
//            } else 
            if ("1".equals(tPayMode)) {
//                tContents = tCustomerName + tSexContent +
//                            "，您的理赔申请已处理完毕，请" + tDrawer +
//                            "于即日起到客户服务中心柜面领取保险金。";
            	
            	 tContents = tCustomerName + tSexContent +
            	             "，您的理赔申请已处理完毕，请及时受领保险金。【人保健康】";
            } else if ("4".equals(tPayMode) || "11".equals(tPayMode)
            		   || "3".equals(tPayMode)) {
                if (tBankAccNo == null || "".equals(tBankAccNo) ||
                    "null".equals(tBankAccNo) || tBankAccNo.length() < 4) {
                    continue;
                }
                tContents = tCustomerName + tSexContent +
                            "，您的理赔申请已处理完毕，请查收尾号为" +
                            tBankAccNo.substring(tBankAccNo.length() - 4,
                                                 tBankAccNo.length()) + "的账户收到的保险金"+CommonBL.bigDoubleToCommonString((Double.parseDouble(tPay)),"0.00")+"元。";
            }

            System.out.println("发送短信：" + tPayMode + tManageCom + tCaseNo +
                               tMobile + tCustomerName);
            //tMobile="18610964563";
            SmsMessage msg = new SmsMessage(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
            msg.setReceiver(tMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
            msg.setContents(tContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
            //和IT讨论后，归类到二级机构
            msg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
            tVector.add(msg);
        }   
        
        //对于广东团体批次案件，团单不需要通过“短信发送团体保单配置”菜单进行配置，只要符合团单短信发送条件，系统自动发送短信。  2013-09-03
        String aSQL =           
        	" SELECT r.mngcom, "
        	+ " g.paymode, "
        	+ " c.caseno, "
        	+ " c.MobilePhone, "
        	+ " c.CustomerName, "
        	+ " g.bankaccno, "
        	+ " g.drawer, "
        	+ " c.customersex, "
        	+ " g.sumgetmoney "
        	+ " FROM llcase c, llregister r, ljaget g, llclaim cl "
        	+ " WHERE c.rgtno = r.rgtno "
        	+ " AND c.caseno = cl.caseno "
        	+ " AND c.rgtstate = '12' "
        	+ " AND c.caseno = g.otherno "
        	+ " AND r.mngcom like '8644%' "
        	+ " AND c.mobilephone IS NOT NULL "
        	+ " AND NOT EXISTS (SELECT 1 "
        	+ " FROM llclaimdetail clt, lmriskapp ap "
        	+ " WHERE clt.caseno = c.caseno "
        	+ " AND ap.riskcode = clt.riskcode "
        	+ " AND ap.riskprop = 'I') "
        	+ " AND c.rgttype = '1' "
        	+ " AND cl.givetype in ('1', '2') "
        	+ " and g.paymode in ('3', '4', '11')  "
        	+ " AND g.confdate ='"+ mLastDate+ "' "
        	+ " and  not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "
        	+ " union all"
        	+" SELECT r.mngcom, " 
        	+" g.paymode, "
        	+" c.caseno, "
        	+" ins.grpinsuredphone, "
        	+" c.CustomerName, "
        	+" g.bankaccno, "
        	+" g.drawer, "
        	+" c.customersex, "
        	+" g.sumgetmoney "
        	+" FROM llcase c, llregister r, ljaget g, llclaim cl,lcinsured ins "
        	+" WHERE c.rgtno = r.rgtno "
        	+" AND c.caseno = cl.caseno "
        	+" AND c.rgtstate = '12' "
        	+" AND c.caseno = g.otherno "
        	+" AND c.mobilephone IS NULL "
        	+" AND r.mngcom like '8644%' "
        	+" and c.customerno = ins.insuredno "
        	+" and (select contno from llclaimdetail where caseno=c.caseno fetch first 1 rows only)=ins.contno " 
        	+" and ins.grpinsuredphone is not null "
        	+" and g.sumgetmoney>0 "
        	+" AND NOT EXISTS (SELECT 1 " 
        	+" FROM llclaimdetail clt, lmriskapp ap " 
        	+" WHERE clt.caseno = c.caseno "
        	+" AND ap.riskcode = clt.riskcode " 
        	+" AND ap.riskprop = 'I') "
        	+" AND c.rgttype = '1' " 
        	+" AND cl.givetype in ('1', '2') " 
        	+" and g.paymode in ('3', '4', '11') " 
        	+" and r.togetherflag='1' "  
        	+" and r.applyertype in ('2','5') "
        	+" AND g.confdate ='"+ mLastDate+ "' "
        	+" and  not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "
        	+ " with ur";
        
        System.out.println(aSQL);

        SSRS mMsgSSRS = tExeSQL.execSQL(aSQL);

        for (int i = 1; i <= mMsgSSRS.getMaxRow(); i++) {
            String mManageCom = mMsgSSRS.GetText(i, 1);
            String mPayMode = mMsgSSRS.GetText(i, 2);
            String mCaseNo = mMsgSSRS.GetText(i, 3);
            String mMobile = mMsgSSRS.GetText(i, 4);
            String mCustomerName = mMsgSSRS.GetText(i, 5);
            String mBankAccNo = mMsgSSRS.GetText(i, 6);
            String mDrawer = mMsgSSRS.GetText(i, 7);
            String mSex = mMsgSSRS.GetText(i, 8);
            String mPay = mMsgSSRS.GetText(i, 9);

            if (mManageCom == null || "".equals(mManageCom) ||
                "null".equals(mManageCom)) {
                continue;
            }

            if (mPayMode == null || "".equals(mPayMode) ||
                "null".equals(mPayMode)) {
                continue;
            }

            if (mMobile == null || "".equals(mMobile) ||
                "null".equals(mMobile)) {
                continue;
            }

            if (mCustomerName == null || "".equals(mCustomerName) ||
                "null".equals(mCustomerName)) {
                continue;
            }

            if (mDrawer == null || "".equals(mDrawer) ||
                "null".equals(mDrawer)) {
                continue;
            }
            
            if (mPay == null || "".equals(mPay) ||
                    "null".equals(mPay)) {
                    continue;
                }
            String mSexContent = "女士/先生";
            if ("0".equals(mSex)) {
            	mSexContent = "先生";
            } else if ("1".equals(mSex)) {
            	mSexContent = "女士";
            }

            String mContents = "";

            if ("1".equals(mPayMode)) {
            	
            	 mContents = mCustomerName + mSexContent +
            	             "，您的理赔申请已处理完毕，请及时受领保险金。【人保健康】";
            } else if ("4".equals(mPayMode) || "11".equals(mPayMode)
            		   || "3".equals(mPayMode)) {
                if (mBankAccNo == null || "".equals(mBankAccNo) ||
                    "null".equals(mBankAccNo) || mBankAccNo.length() < 4) {
                    continue;
                }
                mContents = mCustomerName + mSexContent +
                            "，您的理赔申请已处理完毕，请查收尾号为" +
                            mBankAccNo.substring(mBankAccNo.length() - 4,
                                                 mBankAccNo.length()) + "的账户收到的保险金"+CommonBL.bigDoubleToCommonString((Double.parseDouble(mPay)),"0.00")+"元。";
            }

            System.out.println("发送短信：" + mPayMode + mManageCom + mCaseNo +
                               mMobile + mCustomerName);
            //mMobile="18610964563";
            SmsMessage msg = new SmsMessage(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
            msg.setReceiver(mMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
            msg.setContents(mContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
            //和IT讨论后，归类到二级机构
            msg.setOrgCode(mManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
            tVector.add(msg);
        }   
        
        
        String strSQL = "select c.RgtantMobile,a.contno,b.mngcom from lcinsured a,llcase b,llregister c "
				+ " where a.insuredno = b.customerno and b.caseno = c.rgtno  and c.RgtantMobile IS NOT NULL "
				+ " and b.makedate ='"+ mLastDate+ "'and b.rgtstate in ('01','02','03') and b.caseno like 'C%' "
				+ " and exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(b.mngcom, 1,6)) "
				+ " group by c.RgtantMobile,a.contno ,b.mngcom "
				+ " union all "
				+ " select c.RgtantMobile,a.contno ,b.mngcom from llclaimdetail a,llcase b,llregister c "
				+ " where a.caseno = b.caseno  and b.caseno = c.rgtno  and c.RgtantMobile IS NOT NULL  and b.makedate ='"+ mLastDate+"'"
				+ " and b.rgtstate not in ('01','02','03')  and b.caseno like 'C%' "
				+ " and exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(b.mngcom, 1, 6)) "
				+ " group by c.RgtantMobile,a.contno ,b.mngcom " 
				+ " union all "
				+ " select distinct RgtantMobile,rgtobjno,mngcom from llregister " 
				+ " where makedate ='"+ mLastDate+ "' and RgtantMobile IS NOT NULL " 
				+ " and exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(mngcom, 1, 6)) " 
				+ " and rgtno like 'P%' with ur";

		SSRS tSSRS = tExeSQL.execSQL(strSQL);

		if (tSSRS.getMaxRow() == 0) {

			System.out.println("找不到相应的案件");

		} else {
			
			String thMobile = "";

			for (int n = 1; n <= tSSRS.getMaxRow(); n++) {

				String mMobile = tSSRS.GetText(n, 1);
				String tContno = tSSRS.GetText(n, 2);
				String tManageCom = tSSRS.GetText(n, 3);

				if (thMobile.equals(mMobile)) {

					continue;
				} else {

					thMobile = mMobile;
					tContno = tContno.substring(tContno.length() - 6);
					for (int m = n + 1; m <= tSSRS.getMaxRow(); m++) {
						if (mMobile.equals(tSSRS.GetText(m, 1))) {

							tContno = tContno+ "、"+ tSSRS.GetText(m, 2).substring(tSSRS.GetText(m, 2).length() - 6);
							continue;
						}
					}
					String nSQL = "select RISKWRAPPLANNAME  from ldcode1 where codetype='LPMsg' and code1='01'and code= '"
							+ tManageCom.substring(0, 6) + "'";

					String tContents = tExeSQL.getOneValue(nSQL);
					tContents = tContents
							.replace("?contno?", "" + tContno + "");

					System.out.println("发送短信：" + tContents);
//					mMobile="18600365715";
					SmsMessage Smsg = new SmsMessage(); // 创建SmsMessage对象，定义同上文下行短信格式中的Message元素
					Smsg.setReceiver(mMobile); // 设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
					Smsg.setContents(tContents); // 设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
					tVector.add(Smsg);

				}

			}
		}
		

		String paySQL = " select a.contno,a.caseno,sum(a.realpay),b.RgtantMobile ,b.mngcom  from llclaimdetail a,llregister b "
				+ " where a.caseno = b.rgtno  and exists (select 1 from llcase where a.caseno = caseno and rgtstate in ('11','12')) "
				+ " and exists (select 1 from ljaget where a.caseno = otherno and makedate = '"+ mLastDate+ "') "
				+ " and a.grpcontno = '00000000000000000000' and a.givetype <> '3' and b.RgtantMobile IS NOT NULL  "
				+ " and exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(b.mngcom, 1, 6)) "
				+ " group by a.contno,a.caseno,b.RgtantMobile ,b.mngcom"
				+ " union "
				+ " select a.grpcontno, a.rgtno, sum(a.realpay),b.RgtantMobile ,b.mngcom from llclaimdetail a,llregister b"
				+ " where a.rgtno = b.rgtno"
				+ " and exists (select 1 from llcase where a.caseno = caseno and rgtstate in ('11','12'))"
				+ " and exists (select 1 from ljaget where a.rgtno = otherno and makedate = '"+ mLastDate+ "')"
				+ " and b.RgtantMobile IS NOT NULL  and a.givetype <> '3' and a.grpcontno <> '00000000000000000000'"
				+ " and exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(b.mngcom, 1, 6)) "
				+ " group by a.grpcontno,a.rgtno,b.RgtantMobile ,b.mngcom "
				+ " union "
				+ " select a.grpcontno, a.rgtno, sum(a.realpay),b.RgtantMobile ,b.mngcom from llclaimdetail a,llregister b"
				+ " where a.rgtno = b.rgtno"
				+ " and exists (select 1 from llcase where a.caseno = caseno and rgtstate in ('11','12'))"
				+ " and exists (select 1 from ljaget where a.caseno = otherno and makedate = '"+ mLastDate+ "')"
				+ " and b.RgtantMobile IS NOT NULL  and a.givetype <> '3' and a.grpcontno <> '00000000000000000000'"
				+ " and exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(b.mngcom, 1, 6)) "
				+ " group by a.grpcontno,a.rgtno,b.RgtantMobile ,b.mngcom with ur";
		SSRS mSSRS = tExeSQL.execSQL(paySQL);

		if (mSSRS.getMaxRow() == 0) {

			System.out.println("找不到相应的案件");

		} else {
			for (int m = 1; m <= mSSRS.getMaxRow(); m++) {

				String tContno = mSSRS.GetText(m, 1);
				String tMoney = mSSRS.GetText(m, 3);
				String mMobile = mSSRS.GetText(m, 4);
				String tManageCom = mSSRS.GetText(m, 5);
				tContno = tContno.substring(tContno.length() - 6);
				
				String mSQL = "select RISKWRAPPLANNAME  from ldcode1 where codetype='LPMsg' and code1='11'and code= '"+ tManageCom.substring(0, 6) + "'";
				String tContents = tExeSQL.getOneValue(mSQL);

				tContents = tContents.replace("?contno?", "" + tContno + "");
				tContents = tContents.replace("?money?", "" + tMoney + "");

				System.out.println("发送短信："+ tContents);
//				mMobile="18600365715";
				SmsMessage Tmsg = new SmsMessage(); // 创建SmsMessage对象，定义同上文下行短信格式中的Message元素
				Tmsg.setReceiver(mMobile); // 设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
				Tmsg.setContents(tContents); // 设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
				tVector.add(Tmsg);
			}
		}
        return tVector;
    }


    /* 数据处理 */
    private boolean SubmitMap() {
        PubSubmit tPubSubmit = new PubSubmit();
        mResult.clear();
        mResult.add(mMap);
        if (!tPubSubmit.submitData(mResult, "")) {
            System.out.println("提交数据失败！");
//            return false;
        }
        this.mMap = new MMap();
        return true;
    }

    public static void main(String[] args) {
        GlobalInput mGlobalInput = new GlobalInput();
        LCContSchema tLCContSchema = new LCContSchema();
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);

        LLCustomerMsgBL tLLCustomerMsgBL = new LLCustomerMsgBL();

        tLLCustomerMsgBL.submitData(mVData, "LPMSG");
//       System.out.print(mPolicyAbateDealBL.edorseState("00000619801"));
//         mPolicyAbateDealBL.edorseState("0000126302");
    }

}
