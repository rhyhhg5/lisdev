package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.utility.*;

public class LLRegisterPrintBL {
     public CErrors mErrors = new CErrors();
     private String mOperate;
     private GlobalInput mG = new GlobalInput();
     public LLRegisterPrintBL() {}

     private LOPRTManagerSet mLOPRTManagerSet = new
             LOPRTManagerSet();
     private TransferData mPrintFactor = new TransferData();
     private int mCount = 0;

     /**
      * 传输数据的公共方法
      * @param: cInputData 输入的数据
      *         cOperate 数据操作
      * @return:
      */
     public boolean submitData(VData cInputData, String cOperate) {
         cInputData = (VData) cInputData.clone();
         this.mOperate = cOperate;
         System.out.println(mOperate + "--LLRegisterPrintBL--");
         if (!getInputData(cInputData)) {
             return false;
         }
         if (!dealData()) {
             return false;
         }
         return true;
     }

     private boolean getInputData(VData cInputData) {
         mPrintFactor = (TransferData) cInputData.getObjectByObjectName(
                 "TransferData", 0);
         mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                 "GlobalInput", 0));
         return true;
     }

     public int getCount() {
         return mCount;
     }

     /**
      * 数据操作类业务处理
      * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
      */
     private boolean dealData() {
         if (!prepareOutputData()) {
             return false;
         }
         mCount = mLOPRTManagerSet.size();
         VData tVData = new VData();
         tVData.add(mG);
         tVData.add(mLOPRTManagerSet);
         PDFPrintBatchManagerBL tPDFPrintBatchManagerBL = new PDFPrintBatchManagerBL();
         if(!tPDFPrintBatchManagerBL.submitData(tVData,mOperate)){
              mErrors.addOneError(tPDFPrintBatchManagerBL.mErrors.getError(1));
              return false;
         }
         return true;
     }

     private boolean prepareOutputData() {
         String tBatchType = (String) mPrintFactor.getValueByName("BatchType");
         LLCaseSet tLLCaseSet = new LLCaseSet();
         if (tBatchType.equals("1")) {
             String tRgtNo = (String) mPrintFactor.getValueByName("RgtNo");
             if (tRgtNo == null || tRgtNo == "") {
                 CError tError = new CError();
                 tError.moduleName = "LLRegisterPrintBL";
                 tError.functionName = "checkInputData";
                 tError.errorMessage = "批次号为空出错";
                 this.mErrors.addOneError(tError);
                 return false;
             }
             LLCaseDB tLLCaseDB = new LLCaseDB();
             tLLCaseDB.setRgtNo(tRgtNo);
             tLLCaseSet = tLLCaseDB.query();
         }
         if (tBatchType.equals("2")) {
             String tScaseno = (String) mPrintFactor.getValueByName("SCaseNo");
             String tEcaseno = (String) mPrintFactor.getValueByName("ECaseNo");
             LLCaseDB tLLCaseDB = new LLCaseDB();
             String sql =
                     "select * from llcase where substr(caseno,6)>=substr('" +
                     tScaseno + "',6) and substr(caseno,6)<=substr('" + tEcaseno +
                     "',6)  and MngCom like '" +
                     mG.ManageCom + "%' order by CaseNo";
             System.out.println(sql);
             tLLCaseSet = tLLCaseDB.executeQuery(sql);
         }
         if (tBatchType.equals("3")) {
             String tempstr = (String) mPrintFactor.getValueByName("CaseNoBatch");
             String tCaseNoBatch1 = tempstr.trim();
             String tCaseNoBatch2 = tempstr.trim();
             String tCaseNoBatch3 = tempstr.trim();
             while (tCaseNoBatch1.length() >= 17) {
                 LLCaseSchema tLLCaseSchema = new LLCaseSchema();
                 int Cindex = tCaseNoBatch1.indexOf("C");
                 if(Cindex<0){
                	 break;
                 }
                 String tCaseNo = tCaseNoBatch1.substring(Cindex, Cindex + 17);
                 LLCaseDB tLLCaseDB = new LLCaseDB();
                 tLLCaseDB.setCaseNo(tCaseNo);
                 if (tLLCaseDB.getInfo()) {
                     tLLCaseSchema.setSchema(tLLCaseDB.getSchema());
                     tLLCaseSet.add(tLLCaseSchema);
                 }
                 tCaseNoBatch1 = tCaseNoBatch1.substring(Cindex + 17);
             }
         
         while (tCaseNoBatch2.length() >= 17) {
             LLCaseSchema tLLCaseSchema = new LLCaseSchema();
             int Cindex = tCaseNoBatch2.indexOf("R");
             if(Cindex<0){
            	 break;
             }
             String tCaseNo = tCaseNoBatch2.substring(Cindex, Cindex + 17);
             LLCaseDB tLLCaseDB = new LLCaseDB();
             tLLCaseDB.setCaseNo(tCaseNo);
             if (tLLCaseDB.getInfo()) {
                 tLLCaseSchema.setSchema(tLLCaseDB.getSchema());
                 tLLCaseSet.add(tLLCaseSchema);
             }
             tCaseNoBatch2 = tCaseNoBatch2.substring(Cindex + 17);
         }
     
         while (tCaseNoBatch3.length() >= 17) {
             LLCaseSchema tLLCaseSchema = new LLCaseSchema();
             int Cindex = tCaseNoBatch3.indexOf("S");
             if(Cindex<0){
            	 break;
             }
             String tCaseNo = tCaseNoBatch3.substring(Cindex, Cindex + 17);
             LLCaseDB tLLCaseDB = new LLCaseDB();
             tLLCaseDB.setCaseNo(tCaseNo);
             if (tLLCaseDB.getInfo()) {
                 tLLCaseSchema.setSchema(tLLCaseDB.getSchema());
                 tLLCaseSet.add(tLLCaseSchema);
             }
             tCaseNoBatch3 = tCaseNoBatch3.substring(Cindex + 17);
         }
     }
         //获取了需要进行打印的所有LLCase信息后,下面进行处理
         int count = tLLCaseSet.size();
         for (int i = 1; i <= count; i++) {
             String sqla = "select * from llcase where caseno='"+tLLCaseSet.get(i).
                             getCaseNo()+"' order by CaseNo with ur";
             SSRS tSSRS = new ExeSQL().execSQL(sqla);
             if (tSSRS!= null) {
                  LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
                  String tLimit = PubFun.getNoLimit(this.mG.ManageCom);
                  String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
                  tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
                  tLOPRTManagerSchema.setOtherNo(tLLCaseSet.get(i).getCaseNo());//存放前台传来的caseno
                  tLOPRTManagerSchema.setOtherNoType("09");
                  tLOPRTManagerSchema.setCode("lp005");
                  tLOPRTManagerSchema.setManageCom(this.mG.ManageCom);
                  tLOPRTManagerSchema.setAgentCode("");
                  tLOPRTManagerSchema.setReqCom(this.mG.ManageCom);
                  tLOPRTManagerSchema.setReqOperator(this.mG.Operator);
                  tLOPRTManagerSchema.setPrtType("0");
                  tLOPRTManagerSchema.setStateFlag("0");
                  tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
                  tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
                  tLOPRTManagerSchema.setStandbyFlag2(tLLCaseSet.get(i).getCaseNo());
                  mLOPRTManagerSet.add(tLOPRTManagerSchema);
                 }
         }

         if (mLOPRTManagerSet == null || mLOPRTManagerSet.size() <= 0) {
             buildError("LLRegisterPrintBL", "没有可以打印的数据！");
             return false;
         }
         return true;
     }


     public void buildError(String szFunc, String szErrMsg) {
         CError cError = new CError();
         cError.moduleName = "LLRegisterPrintBL";
         cError.functionName = szFunc;
         cError.errorMessage = szErrMsg;
         this.mErrors.addOneError(cError);
     }

     public static void main(String[] args) {

         TransferData PrintElement = new TransferData();
         PrintElement.setNameAndValue("RgtNo", "P1100070225000019");
         PrintElement.setNameAndValue("BatchType", "1");
         PrintElement.setNameAndValue("DetailPrt", "on");
         PrintElement.setNameAndValue("GrpDetailPrt", "on");
         PrintElement.setNameAndValue("NoticePrt", "off");

         GlobalInput tGlobalInput = new GlobalInput();
         tGlobalInput.ClientIP = "10.252.130.163";
         tGlobalInput.ManageCom = "86";
         tGlobalInput.Operator = "cm0001";

         VData aVData = new VData();
         aVData.add(tGlobalInput);
         aVData.add(PrintElement);

         LLRegisterPrintBL tLLRegisterPrintBL = new LLRegisterPrintBL();
         tLLRegisterPrintBL.submitData(aVData, "batch");


     }
}
