package com.sinosoft.lis.llcase;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * 申请材料选择
 * 
 * @author maning
 * @version 1.0
 */
public class LLCaseAffix extends LLCaseProcess {

	/**
	 * 
	 */
	public LLCaseAffix() {

	}

	// 业务逻辑错误
	public CErrors mErrors = new CErrors();

	// 获取的业务数据
	private VData mInputData = new VData();

	// 返回处理后数据
	private VData mResult = new VData();

	// 返回的提交数据库MMap
	private MMap mMMap = new MMap();

	// 用户登录信息
	private GlobalInput mGlobalInput = new GlobalInput();

	// 操作类型
	private String mOperate = "";

	// 案件信息
	private LLCaseSchema mLLCaseSchema = null;

	// 申请材料信息
	private LLAffixSet mLLAffixSet = null;

	// 当前日期
	private String mCurrentDate = PubFun.getCurrentDate();

	// 当前事件
	private String mCurrentTime = PubFun.getCurrentTime();

	// 材料齐备标志 0-缺少 1-齐备
	private String mShortFlag = "1";

	// 材料齐备日期
	private String mAffixGetDate = "";

	private CachedLPInfo mCachedLPInfo = CachedLPInfo.getInstance();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sinosoft.lis.llcase.LLCaseProcess#getMMap()
	 */
	public MMap getMMap() throws Exception {
		String tDelAffixSQL = "DELETE FROM LLAffix WHERE CaseNo='"
				+ mLLCaseSchema.getCaseNo() + "'";
		mMMap.put(tDelAffixSQL, "DELETE");
		if (mLLAffixSet != null && mLLAffixSet.size() > 0) {
			mMMap.put(mLLAffixSet, "INSERT");
		}
//		mMMap.put(mLLCaseSchema, "UPDATE");
		return mMMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sinosoft.lis.llcase.LLCaseProcess#getResult()
	 */
	public VData getResult() throws Exception {
		if (mLLAffixSet != null && mLLAffixSet.size() > 0) {
			mResult.add(mLLAffixSet);
		}
		mResult.add(mLLCaseSchema);
		return mResult;
	}

	/**
	 * 获取数据VData，经过业务处理，返回包含业务数据的VData。不进行业务逻辑的校验，由外层类处理，默认所有数据都正确
	 * 
	 * @param aInputData
	 *            <p>
	 *            LLAffixSet &ReasonCode
	 * @param aOperate
	 * @return 处理成功标志
	 * @throws Exception
	 *             所有的异常都由外层调用类处理
	 */
	public boolean submitData(VData aInputData, String aOperate)
			throws Exception {
		System.out.println("开始申请材料选择");
		mInputData = aInputData;
		mOperate = aOperate;

		if (!getInputData()) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		 //提交数据库
        PubSubmit tPubSubmit = new PubSubmit();
        mInputData.clear();       
        mInputData.add(getMMap());
        if (!tPubSubmit.submitData(mInputData, ""))
        {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LLCaseAffix";
                tError.functionName = "dealData";
                tError.errorMessage = "理赔申请材料保存失败!";
                this.mErrors.addOneError(tError);
                return false;
        }
		System.out.println("申请材料选择结束");

		return true;
	}

	/**
	 * 解析获取的对象
	 * 
	 * @return 处理成功标志
	 * @throws Excepiton
	 */
	private boolean getInputData() throws Exception {
		mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0);

		if (mGlobalInput == null) {
			mErrors.addOneError("用户登录信息获取失败");
			return false;
		}

		mLLCaseSchema = (LLCaseSchema) mInputData.getObjectByObjectName(
				"LLCaseSchema", 0);

		if (mLLCaseSchema == null) {
			mErrors.addOneError("案件信息获取失败");
			return false;
		}

		mLLAffixSet = (LLAffixSet) mInputData.getObjectByObjectName(
				"LLAffixSet", 0);

		if (mLLAffixSet == null) {
			mErrors.addOneError("申请材料信息获取失败");
			return false;
		}
		return true;
	}

	/**
	 * 基本数据校验
	 * 
	 * @return 校验成功标志
	 * @throws Exception
	 */
	private boolean checkData() throws Exception {
		if (mGlobalInput.Operator == null || "".equals(mGlobalInput.Operator)) {
			mErrors.addOneError("操作用户登录信息获取失败");
			return false;
		}

		if (mGlobalInput.ManageCom == null || "".equals(mGlobalInput.ManageCom)) {
			mErrors.addOneError("操作用户登录信息获取失败");
			return false;
		}

		if (mLLCaseSchema.getCaseNo() == null
				|| "".equals(mLLCaseSchema.getCaseNo())) {
			mErrors.addOneError("案件号获取失败");
			return false;
		}

		if (mLLCaseSchema.getRgtNo() == null
				|| "".equals(mLLCaseSchema.getRgtNo())) {
			mErrors.addOneError("批次号获取失败");
			return false;
		}

		if (!"01".equals(mLLCaseSchema.getRgtState())
				&& !"02".equals(mLLCaseSchema.getRgtState())
				&& !"13".equals(mLLCaseSchema.getRgtState())) {
			mErrors.addOneError("该案件状态下不能进行申请材料选择");
			return false;
		}

		return true;
	}

	/**
	 * 业务处理
	 * 
	 * @return 处理成功标志
	 * @throws Exception
	 */
	private boolean dealData() throws Exception {
		// 处理Affix表
		if (!dealAffix()) {
			return false;
		}

		// 处理LLCase表
		if (!dealLLCase()) {
			return false;
		}

		return true;
	}

	/**
	 * 处理Affix表数据
	 * 
	 * @return 处理成功标志
	 * @throws Exception
	 */
	private boolean dealAffix() throws Exception {
		// 备份获取数据
		LLAffixSet tLLAffix = new LLAffixSet();
		tLLAffix.set(mLLAffixSet);
		mLLAffixSet = new LLAffixSet();

		for (int i = 1; i <= tLLAffix.size(); i++) {
			LLAffixSchema tTempLLAffixSchema = tLLAffix.get(i);
			LLAffixSchema tLLAffixSchema = new LLAffixSchema();

			// 批次号
			tLLAffixSchema.setRgtNo(mLLCaseSchema.getRgtNo());

			// 案件号
			tLLAffixSchema.setCaseNo(mLLCaseSchema.getCaseNo());

			// 材料编码
			String tAffixCode = tTempLLAffixSchema.getAffixCode();
			if (tAffixCode == null || "".equals(tAffixCode)) {
				mErrors.addOneError("未获得申请材料编码");
				return false;
			}

			LLMAffixSet tLLMAffixSet = mCachedLPInfo
					.findLLMAffixByAffixCode(tAffixCode);

			if (tLLMAffixSet.size() <= 0) {
				mErrors.addOneError("申请材料编码错误");
				return false;
			}

			// 目前一个申请材料编码只有一个
			LLMAffixSchema tLLMAffixSchema = tLLMAffixSet.get(1);

			tLLAffixSchema.setAffixCode(tLLMAffixSchema.getAffixCode());

			// 材料类型
			tLLAffixSchema.setAffixType(tLLMAffixSchema.getAffixTypeCode());

			// 材料名称
			tLLAffixSchema.setAffixName(tLLMAffixSchema.getAffixName());

			// 申请原因
			if (tTempLLAffixSchema.getReasonCode() == null
					|| "".equals(tTempLLAffixSchema.getReasonCode())) {
				LLMAppReasonAffixSet tLLMAppReasonAffixSet = mCachedLPInfo
						.findLLMAppReasonAffixByAffixCode(tAffixCode);
				if (tLLMAppReasonAffixSet.size() <= 0) {
					tLLAffixSchema.setReasonCode("");
				} else {
					tLLAffixSchema.setReasonCode(tLLMAppReasonAffixSet.get(1)
							.getReasonCode());
				}

			} else {
				tLLAffixSchema
						.setReasonCode(tTempLLAffixSchema.getReasonCode());
			}

			// 份数
			tLLAffixSchema.setCount(tTempLLAffixSchema.getCount());

			// 缺少份数
			if (tTempLLAffixSchema.getShortCount() > tTempLLAffixSchema
					.getCount()) {
				mErrors.addOneError("申请材料缺少分数不能大于分数");
				return false;
			}
			tLLAffixSchema.setShortCount(tTempLLAffixSchema.getShortCount());

			// 材料未准备齐全
			if (tTempLLAffixSchema.getShortCount() > 0) {
				mShortFlag = "0";
			}

			// 提供日期 如果数据包括则直接获取 否则如果缺少分数为0，提供日期为当天
			if (tTempLLAffixSchema.getSupplyDate() != null
					&& !"".equals(tTempLLAffixSchema.getSupplyDate())) {
				if (!PubFun.checkDateForm(tTempLLAffixSchema.getSupplyDate())) {
					mErrors.addOneError("申请材料"
							+ tTempLLAffixSchema.getAffixCode() + "提供日期错误");
					return false;
				}
				tLLAffixSchema
						.setSupplyDate(tTempLLAffixSchema.getSupplyDate());
			} else if (tTempLLAffixSchema.getShortCount() == 0) {
				tLLAffixSchema.setSupplyDate(mCurrentDate);
			}

			// 如果没有缺少件数，取最大的提供日期
			if (tTempLLAffixSchema.getShortCount() == 0) {
				mAffixGetDate = PubFun.getLaterDate(mAffixGetDate,
						tLLAffixSchema.getSupplyDate());
			}

			// 管理机构
			tLLAffixSchema.setMngCom(mGlobalInput.ManageCom);

			// 操作人
			tLLAffixSchema.setOperator(mGlobalInput.Operator);

			// 生成日期
			tLLAffixSchema.setMakeDate(mCurrentDate);

			// 生成时间
			tLLAffixSchema.setMakeTime(mCurrentTime);

			// 修改日期
			tLLAffixSchema.setModifyDate(mCurrentDate);

			// 修改时间
			tLLAffixSchema.setModifyTime(mCurrentTime);

			// 申请材料流水号
			String tLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);

			String tAffixNo = PubFun1.CreateMaxNo("rgtaffixno", tLimit);

			tLLAffixSchema.setAffixNo(tAffixNo);

			mLLAffixSet.add(tLLAffixSchema);
		}

		return true;
	}

	/**
	 * 处理LLCase表
	 * 
	 * @return 处理成功标志
	 * @throws Exception
	 */
	private boolean dealLLCase() throws Exception {

		// 材料齐备案件状态01-受理 否则 13-延迟
		if ("1".equals(mShortFlag)) {
			mLLCaseSchema.setRgtState("01");
			mLLCaseSchema.setAffixGetDate(mAffixGetDate);
		} else {
			mLLCaseSchema.setRgtDate("13");
			mLLCaseSchema.setAffixGetDate("");
		}

		mLLCaseSchema.setOperator(mGlobalInput.Operator);
		mLLCaseSchema.setModifyDate(mCurrentDate);
		mLLCaseSchema.setModifyTime(mCurrentDate);

		return true;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
