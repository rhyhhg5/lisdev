package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.task.CommonBL;
import com.sinosoft.task.Task;

/**
 * <p>Title: 工单管理系统</p>
 * <p>Description: 工单录入BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 * @date 2005-01-17
 */

public class LLPrepaidClaimConfirmBL {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();
    private MMap map = new MMap();

    /** 数据操作字符串 */
    private LLPrepaidUWMainSchema mLLPrepaidUWMainSchema = new LLPrepaidUWMainSchema();
    private LLPrepaidClaimSchema mLLPrepaidClaimSchema = new LLPrepaidClaimSchema();
    private LJSGetSchema mLJSGetSchema = new LJSGetSchema();
    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();  
    private String mPrepaidNo = ""; 
    private String mGrpContNo ="";
    private String mRgtType = "";		//审批金额
    private String mOperate;
    private String mReturnMessage = "";

    /** 全局参数 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    public LLPrepaidClaimConfirmBL() {
    }

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
    	
    	//将传入的数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //将外部传入的数据分解到本类的属性中，准备处理
        if (!getInputData()) {
            return false;
        }
        
        if (!checkData()) {
            return false;
        }

        //根据业务逻辑对数据进行处理
        if (!dealData()) {
            return false;
        }
        prepareOutputData();
        
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
        	CError.buildErr(this, "数据保存失败");
            return false;
          }
        return true;
    }
    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData() {
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mLLPrepaidUWMainSchema.setSchema((LLPrepaidUWMainSchema)mInputData.getObjectByObjectName("LLPrepaidUWMainSchema",0));
        mLLPrepaidClaimSchema.setSchema((LLPrepaidClaimSchema)mInputData.getObjectByObjectName("LLPrepaidClaimSchema",0));
        
        mPrepaidNo = mLLPrepaidUWMainSchema.getPrepaidNo();
        if(mPrepaidNo.equals("")||mPrepaidNo==null)
        {
        	CError.buildErr(this, "预付赔款号为空!");
            return false;
        }
        System.out.println("LLPrepaidClaimConfirmBL.getInputData()的预付赔款号======"+mPrepaidNo);
        System.out.println("mLLPrepaidClaimSchema的预付赔款号======"+mLLPrepaidClaimSchema.getPrepaidNo());
        return true;
    }

    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData() {
    	
    	String grpsql = "select a.grpcontno from LCGrpCont a, LLPrepaidClaim b"
            +" where a.GrpContNo = b.GrpContNo"
            +" and b.PrepaidNo = '" + mPrepaidNo + "' ";
    	ExeSQL grpexesql = new ExeSQL();
	    SSRS grpSSRS = grpexesql.execSQL(grpsql);
	    System.out.println(grpsql);
        if (grpSSRS.getMaxRow()<=0) 
        {
           CError.buildErr(this, "预付案件保单信息查询失败");
           return false;
        }
        mGrpContNo = grpSSRS.GetText(1, 1);
        GlobalInput tGI= new GlobalInput();
        tGI = mGlobalInput;
    	if(!LLCaseCommon.lockFY(mGrpContNo,tGI))
    	{
    		CError.buildErr(this, "预付保单信息不能同时处理多条，请稍等30秒!");
            return false;
    	}
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData() {
    	System.out.println("LLPrepaidClaimConfirmBL.dealData()");    	
    	LLPrepaidClaimDB tLLPrepaidClaimDB = new LLPrepaidClaimDB();
    	LLPrepaidClaimSchema tLLPrepaidClaimSchema = new LLPrepaidClaimSchema();
    	
    	tLLPrepaidClaimDB.setPrepaidNo(mPrepaidNo);
    	if(!tLLPrepaidClaimDB.getInfo())
    	{
    		CError.buildErr(this, "预付赔款信息查询失败!");
            return false;
    	}else{
    		tLLPrepaidClaimSchema = tLLPrepaidClaimDB.getSchema();
        	if(!tLLPrepaidClaimSchema.getRgtState().equals("04"))
        	{
        		CError.buildErr(this, "审定状态的预付案件才能进行通知给付!");
                return false;
        	}
        	LLPrepaidClaimDetailDB tLLPrepaidClaimDetailDB = new LLPrepaidClaimDetailDB();
        	LLPrepaidClaimDetailSet tLLPrepaidClaimDetailSet = new LLPrepaidClaimDetailSet();
        	LLPrepaidClaimDetailSchema tLLPrepaidClaimDetailSchema = new LLPrepaidClaimDetailSchema();
        	tLLPrepaidClaimDetailDB.setPrepaidNo(mPrepaidNo);
        	tLLPrepaidClaimDetailSet = tLLPrepaidClaimDetailDB.query();
        	if(tLLPrepaidClaimDetailSet.size()<=0)
        	{
        		CError.buildErr(this, "查询预付明细信息失败!");
                return false;
        	}
        	
	    	mRgtType = tLLPrepaidClaimSchema.getRgtType();
	    	//mRgtType=1 预付赔款、mRgtType=2 回收预付
	    	if(mRgtType.equals("1"))
	    	{
	    		//修改预付保单信息
	    		LLPrepaidGrpContDB tLLPrepaidGrpContDB = new LLPrepaidGrpContDB();
	    		LLPrepaidGrpContSchema tLLPrepaidGrpContSchema = new LLPrepaidGrpContSchema();
	    		tLLPrepaidGrpContDB.setGrpContNo(tLLPrepaidClaimSchema.getGrpContNo());
	    		if(!tLLPrepaidGrpContDB.getInfo())
	    		{
	    			CError.buildErr(this, "预付保单信息查询失败!");
	                return false;
	    		}else{
	    			tLLPrepaidGrpContSchema = tLLPrepaidGrpContDB.getSchema();
	    			tLLPrepaidGrpContSchema.setBalaDate(mCurrentDate);
	    			tLLPrepaidGrpContSchema.setBalaTime(mCurrentTime);
	    			System.out.println("账户保单余额=="+tLLPrepaidGrpContSchema.getPrepaidBala());
	    			System.out.println("账户保单已经申请金额=="+tLLPrepaidGrpContSchema.getApplyAmount());
	    			tLLPrepaidGrpContSchema.setPrepaidBala(Arith.round(tLLPrepaidGrpContSchema.getPrepaidBala()+tLLPrepaidClaimSchema.getSumMoney(), 2));
	    			tLLPrepaidGrpContSchema.setApplyAmount(Arith.round(tLLPrepaidGrpContSchema.getApplyAmount()+tLLPrepaidClaimSchema.getSumMoney(), 2));
	    			tLLPrepaidGrpContSchema.setModifyDate(mCurrentDate);
	    			tLLPrepaidGrpContSchema.setModifyTime(mCurrentTime);
	    			map.put(tLLPrepaidGrpContSchema, "DELETE&INSERT");
	    		}	    		
	    		for(int index =1; index<=tLLPrepaidClaimDetailSet.size() ;index ++)
	    		{
	    			//修改预付保单险种信息
	    			tLLPrepaidClaimDetailSchema = tLLPrepaidClaimDetailSet.get(index);
	    			LLPrepaidGrpPolDB tLLPrepaidGrpPolDB = new LLPrepaidGrpPolDB();
		    		LLPrepaidGrpPolSchema tLLPrepaidGrpPolSchema = new LLPrepaidGrpPolSchema();
		    		tLLPrepaidGrpPolDB.setGrpPolNo(tLLPrepaidClaimDetailSchema.getGrpPolNo());
		    		
		    		double tPrepaidBala =0.00;		    		
		    		if(!tLLPrepaidGrpPolDB.getInfo())
		    		{
		    			CError.buildErr(this, "预付保单信息查询失败!");
		                return false;
		    		}else{
		    			tLLPrepaidGrpPolSchema = tLLPrepaidGrpPolDB.getSchema();
		    			tPrepaidBala = tLLPrepaidGrpPolSchema.getPrepaidBala();		    			
		    			tLLPrepaidGrpPolSchema.setBalaDate(mCurrentDate); 
		    			tLLPrepaidGrpPolSchema.setBalaTime(mCurrentTime);	
		    			System.out.println("账户险种余额=="+tLLPrepaidGrpPolSchema.getPrepaidBala());
		    			System.out.println("账户险种已经申请金额=="+tLLPrepaidGrpPolSchema.getApplyAmount());
		    			tLLPrepaidGrpPolSchema.setPrepaidBala(Arith.round(tLLPrepaidGrpPolSchema.getPrepaidBala()+tLLPrepaidClaimDetailSchema.getMoney(), 2));
		    			tLLPrepaidGrpPolSchema.setApplyAmount(Arith.round(tLLPrepaidGrpPolSchema.getApplyAmount()+tLLPrepaidClaimDetailSchema.getMoney(), 2));
		    			tLLPrepaidGrpPolSchema.setModifyDate(mCurrentDate);
		    			tLLPrepaidGrpPolSchema.setModifyTime(mCurrentTime);
		    			map.put(tLLPrepaidGrpPolSchema, "DELETE&INSERT");
		    		}
		    		//修改预付轨迹信息
		    		LLPrepaidTraceDB tLLPrepaidTraceDB = new LLPrepaidTraceDB();
		    		LLPrepaidTraceSet tLLPrepaidTraceSet = new LLPrepaidTraceSet();
		    		LLPrepaidTraceSchema tLLPrepaidTraceSchema = new LLPrepaidTraceSchema();
		    		tLLPrepaidTraceDB.setOtherNo(mPrepaidNo);
		    		tLLPrepaidTraceDB.setOtherNoType("Y");
		    		tLLPrepaidTraceDB.setState("0");
		    		tLLPrepaidTraceDB.setGrpPolNo(tLLPrepaidClaimDetailSchema.getGrpPolNo());
		    		tLLPrepaidTraceSet = tLLPrepaidTraceDB.query();
		    		if(tLLPrepaidTraceSet.size()<=0)
		    		{
		    			CError.buildErr(this, "预付轨迹信息查询失败!");
		                return false;
		    		}else{
		    			for (int j = 1;j <= tLLPrepaidTraceSet.size();j++)
		    			{
		    				tLLPrepaidTraceSchema = tLLPrepaidTraceSet.get(j);		    				
		    				tLLPrepaidTraceSchema.setBalaDate(mCurrentDate);
		    				tLLPrepaidTraceSchema.setBalaTime(mCurrentTime);
		    				System.out.println("账户余额tPrepaidBala=="+tPrepaidBala);
		    				tLLPrepaidTraceSchema.setLastBala(tPrepaidBala);		    						    				
		    				tLLPrepaidTraceSchema.setAfterBala(Arith.round(tPrepaidBala+tLLPrepaidClaimDetailSchema.getMoney(), 2));
		    				System.out.println("账户余额AfterBala=="+tLLPrepaidTraceSchema.getAfterBala());
		    				tLLPrepaidTraceSchema.setState("1");
		    				tLLPrepaidTraceSchema.setModifyDate(mCurrentDate);
		    				tLLPrepaidTraceSchema.setModifyTime(mCurrentTime);
		    				map.put(tLLPrepaidTraceSchema, "DELETE&INSERT");
		    			}
		    		}
	    		}
	    		LJSGetDB tLJSGetDB = new LJSGetDB();
	    		LJSGetSet tLJSGetSet = new LJSGetSet();
	    		tLJSGetDB.setOtherNo(mPrepaidNo);
	    		tLJSGetDB.setOtherNoType("Y");
	    		tLJSGetSet = tLJSGetDB.query();
	    		if(tLJSGetSet.size()<=0)
	    		{
	    			CError.buildErr(this, "财务信息生成失败!");
	                return false;
	    		}
	    		if(tLJSGetSet.get(1).getSumGetMoney()>=0)
	    		{
	    			mLJSGetSchema = tLJSGetSet.get(1);
	    			if(!getFinanceLJAGet(mLJSGetSchema))
		    		{
		    			CError.buildErr(this, "财务信息生成失败!");
		                return false;
		    		}
	    		}
	    		tLLPrepaidClaimSchema.setRgtState("05");
	    		tLLPrepaidClaimSchema.setNoticer(mGlobalInput.Operator);
	    		tLLPrepaidClaimSchema.setNoticeDate(mCurrentDate);
	    		tLLPrepaidClaimSchema.setModifyDate(mCurrentDate);
	    		tLLPrepaidClaimSchema.setModifyTime(mCurrentTime);
	    		map.put(tLLPrepaidClaimSchema, "DELETE&INSERT");
	    	}else if(mRgtType.equals("2")){	
	    		//回收生成应收数据   		    			    		
	    		if (!getFinanceLJSPay(tLLPrepaidClaimSchema)) {
		             CError.buildErr(this, "生成业务应收数据失败");
		             return false;
		         }
		    	     		
	    		tLLPrepaidClaimSchema.setRgtState("05");
	    		tLLPrepaidClaimSchema.setNoticer(mGlobalInput.Operator);
	    		tLLPrepaidClaimSchema.setNoticeDate(mCurrentDate);
	    		tLLPrepaidClaimSchema.setModifyDate(mCurrentDate);
	    		tLLPrepaidClaimSchema.setModifyTime(mCurrentTime);
	    		map.put(tLLPrepaidClaimSchema, "DELETE&INSERT");
	    	}
    	}
        return true;
    }   
    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData() {
        mInputData.clear();
        mInputData.add(map);
    }
    /**
     * 生成预付赔款财务明细表
     * @param: 无
     * @return: void
     */
    private boolean getFinanceLJAGet(LJSGetSchema mLJSGetSchema)
    {
    	 String delsql1 = "delete from ljsget where otherno='"+ mPrepaidNo +"' and othernotype='Y'";
    	 String delsql2 = "delete from ljsgetclaim where otherno='"+ mPrepaidNo +"' and othernotype='Y'";
    	 map.put(delsql1, "DELETE");
    	 map.put(delsql2, "DELETE");
    	
    	 LJAGetSchema tLJAGetSchema = new LJAGetSchema();
   	     String sNoLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
         String ActuGetNo = PubFun1.CreateMaxNo("ACTUGETNO", sNoLimit);
         System.out.println("财务实付号ActuGetNo====="+ActuGetNo);
    	 Reflections rf = new Reflections();
         rf.transFields(tLJAGetSchema, mLJSGetSchema);
         tLJAGetSchema.setActuGetNo(ActuGetNo);        
         tLJAGetSchema.setOperator(mGlobalInput.Operator);
         
         LLPrepaidClaimDB mLLPrepaidClaimDB = new LLPrepaidClaimDB();
     	 LLPrepaidClaimSchema mLLPrepaidClaimSchema = new LLPrepaidClaimSchema();
     	
     	 mLLPrepaidClaimDB.setPrepaidNo(mPrepaidNo);
     	 if(!mLLPrepaidClaimDB.getInfo())
     	 {
     		CError.buildErr(this, "预付赔款信息查询失败!");
             return false;
     	 }
     	 mLLPrepaidClaimSchema = mLLPrepaidClaimDB.getSchema();
     		
         tLJAGetSchema.setManageCom(mLLPrepaidClaimSchema.getManageCom());
         tLJAGetSchema.setDrawer(mLLPrepaidClaimSchema.getAccName());
         tLJAGetSchema.setMakeDate(mCurrentDate);
         tLJAGetSchema.setMakeTime(mCurrentTime);
         tLJAGetSchema.setModifyDate(mCurrentDate);         
         tLJAGetSchema.setModifyTime(mCurrentTime);
         tLJAGetSchema.setShouldDate(mCurrentDate);
         map.put(tLJAGetSchema, "INSERT");
         
    	//生成ljsgetclaim
		LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB();
		LJSGetClaimSet tLJSGetClaimSet = new LJSGetClaimSet();
		tLJSGetClaimDB.setGetNoticeNo(mLJSGetSchema.getGetNoticeNo());
		tLJSGetClaimDB.setOtherNo(mPrepaidNo);
		tLJSGetClaimDB.setOtherNoType("Y");
		tLJSGetClaimSet = tLJSGetClaimDB.query();
		if(tLJSGetClaimSet.size()<=0)
		{
			CError.buildErr(this, "财务明细信息查询失败!");
            return false;
		}else{
			for (int m = 1; m<=tLJSGetClaimSet.size(); m++)
			{		    
				LJAGetClaimSchema tLJAGetClaimSchema = new LJAGetClaimSchema();
	    		Reflections tref = new Reflections();
	    	    tref.transFields(tLJAGetClaimSchema, tLJSGetClaimSet.get(m));
	  
	            tLJAGetClaimSchema.setActuGetNo(ActuGetNo);
	            tLJAGetClaimSchema.setOPConfirmDate(mCurrentDate);
                tLJAGetClaimSchema.setOPConfirmCode(mGlobalInput.Operator);
                tLJAGetClaimSchema.setOPConfirmTime(mCurrentTime);
	            tLJAGetClaimSchema.setOperator(mGlobalInput.Operator);
	            tLJAGetClaimSchema.setMakeDate(mCurrentDate);
	    	    tLJAGetClaimSchema.setMakeTime(mCurrentTime);
	    	    tLJAGetClaimSchema.setModifyDate(mCurrentDate);
	    	    tLJAGetClaimSchema.setModifyTime(mCurrentTime);
	    	    map.put(tLJAGetClaimSchema,"INSERT");
			}
		}
	        return true;
    }
    /**
     * 预付回收时生成财务ljspay信息
     * @param: 无
     * @return: void
     */
    private boolean getFinanceLJSPay(LLPrepaidClaimSchema aLLPrepaidClaimSchema)
    {
    	String delsql1 = "delete from ljspay where otherno='"+ mPrepaidNo +"' and othernotype='Y'";
    	String delsql2 = "delete from ljspaygrp a where exists (select 1 from ljspay where GetNoticeNo=a.GetNoticeNo"
    				+" and otherno='"+ mPrepaidNo +"' and othernotype='Y')";
    	map.put(delsql1, "DELETE");
    	map.put(delsql2, "DELETE");
    	
    	LLPrepaidClaimDetailDB tLLPrepaidClaimDetailDB = new LLPrepaidClaimDetailDB();
    	LLPrepaidClaimDetailSet tLLPrepaidClaimDetailSet = new LLPrepaidClaimDetailSet();
    	LLPrepaidClaimDetailSchema tLLPrepaidClaimDetailSchema = new LLPrepaidClaimDetailSchema();
    	
    	tLLPrepaidClaimDetailDB.setPrepaidNo(mPrepaidNo);
    	tLLPrepaidClaimDetailSet = tLLPrepaidClaimDetailDB.query();
    	if(tLLPrepaidClaimDetailSet.size()<=0)
    	{
    		CError.buildErr(this, "预付赔款明细信息查询失败!");
            return false;
    	}else{    		
    		double tSumMoney =0.00; 
    		LJSPayGrpSet tLJSPayGrpSet = new LJSPayGrpSet();    		
   	        String tGetNoticeNo = PubFun1.CreateMaxNo("PAYNOTICENO", null);
   	        
    		for (int i = 1; i <= tLLPrepaidClaimDetailSet.size(); i++) 
    		{        		     	       
    			tLLPrepaidClaimDetailSchema = tLLPrepaidClaimDetailSet.get(i);
    			LJSPayGrpSchema tLJSPayGrpSchema = new LJSPayGrpSchema();     			 
	    	
	   	    	 LCGrpPolBL tLCGrpPolBL = new LCGrpPolBL();
	   	    	 tLCGrpPolBL.setGrpPolNo(tLLPrepaidClaimDetailSchema.getGrpPolNo());
	   	         if (!tLCGrpPolBL.getInfo()) {
	   	             CError.buildErr(this, "保单查询" +tLLPrepaidClaimDetailSchema.getGrpPolNo()+"查询失败");
	   	             return false;
	   	         } 	   	 
	     		
	   	         tLJSPayGrpSchema.setGetNoticeNo(tGetNoticeNo);	   	         
	   	         tLJSPayGrpSchema.setPayCount("1");
	   	         tLJSPayGrpSchema.setGrpContNo(tLCGrpPolBL.getGrpContNo());
	   	         tLJSPayGrpSchema.setGrpPolNo(tLCGrpPolBL.getGrpPolNo());
	   	         tLJSPayGrpSchema.setManageCom(tLCGrpPolBL.getManageCom());
	   	         tLJSPayGrpSchema.setAgentCom(tLCGrpPolBL.getAgentCom());	  
	   	         tLJSPayGrpSchema.setAgentType(tLCGrpPolBL.getAgentType());
	   	         tLJSPayGrpSchema.setRiskCode(tLCGrpPolBL.getRiskCode());	   	         
	   	         tLJSPayGrpSchema.setAgentCode(tLCGrpPolBL.getAgentCode());
	   	         tLJSPayGrpSchema.setAgentGroup(tLCGrpPolBL.getAgentGroup());	   	         
	   	         tLJSPayGrpSchema.setSumDuePayMoney(-(tLLPrepaidClaimDetailSchema.getMoney()));	 
	   	         tLJSPayGrpSchema.setSumActuPayMoney(-(tLLPrepaidClaimDetailSchema.getMoney()));
	   	         tSumMoney += -Arith.round(tLLPrepaidClaimDetailSchema.getMoney(), 2);
	   	         tLJSPayGrpSchema.setPayDate(mCurrentDate);	   	        
	   	         tLJSPayGrpSchema.setPayType("YF");
	   	         tLJSPayGrpSchema.setOperator(mGlobalInput.Operator);	   	         
	   	         tLJSPayGrpSchema.setMakeDate(mCurrentDate);
	   	         tLJSPayGrpSchema.setMakeTime(mCurrentTime);
	   	         tLJSPayGrpSchema.setModifyDate(mCurrentDate); 
	   	         tLJSPayGrpSchema.setModifyTime(mCurrentTime);
	   	         tLJSPayGrpSet.add(tLJSPayGrpSchema);
	   	         map.put(tLJSPayGrpSchema, "INSERT");
	   	         
    		}
    		LJSPaySchema tLJSPaySchema = new LJSPaySchema();
			LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
			if(aLLPrepaidClaimSchema.getPayMode().equals("4"))
	         {
	        	tLJSPaySchema.setBankCode(aLLPrepaidClaimSchema.getBankCode());
	        	tLJSPaySchema.setBankAccNo(aLLPrepaidClaimSchema.getBankAccNo());
	        	tLJSPaySchema.setAccName(aLLPrepaidClaimSchema.getAccName());
	         }
   		    if(aLLPrepaidClaimSchema.getPayMode().equals("3")
	   	            ||aLLPrepaidClaimSchema.getPayMode().equals("11"))
	         {
	        	tLJSPaySchema.setBankAccNo(aLLPrepaidClaimSchema.getBankAccNo());
	        	tLJSPaySchema.setAccName(aLLPrepaidClaimSchema.getAccName());
	         }
			 Reflections tref = new Reflections();
	 		 tref.transFields(tLJSPaySchema, tLJSPayGrpSet.get(1));
	 		 
	   	     tLJSPaySchema.setGetNoticeNo(tGetNoticeNo);
	   	     tLJSPaySchema.setStartPayDate(mCurrentDate);
			 tLJSPaySchema.setOtherNo(mPrepaidNo);	  
			 tLJSPaySchema.setOtherNoType("Y");	  	  			 
			 tLJSPaySchema.setPayDate(mCurrentDate);	  			   			  			
			 tLJSPaySchema.setRiskCode("000000");	   	         	  			 	   	        
			 tLJSPaySchema.setSumDuePayMoney(tSumMoney);		  					
			 
			 Reflections Btref = new Reflections();
		     Btref.transFields(tLJSPayBSchema, tLJSPaySchema);
		     tLJSPayBSchema.setDealState("0");
	    
	         map.put(tLJSPaySchema, "INSERT");
	         map.put(tLJSPayBSchema, "INSERT");
	         
	         if(aLLPrepaidClaimSchema.getPayMode().equals("4"))
	         {
	             LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
	             Reflections Temptref = new Reflections();
	             Temptref.transFields(tLJTempFeeSchema, tLJSPaySchema);
		         tLJTempFeeSchema.setTempFeeNo(tGetNoticeNo);
		         tLJTempFeeSchema.setRiskCode("000000");
		         tLJTempFeeSchema.setTempFeeType("Y");
		         tLJTempFeeSchema.setPayMoney(tLJSPaySchema.getSumDuePayMoney());
		         tLJTempFeeSchema.setMakeDate(mCurrentDate);
		         tLJTempFeeSchema.setModifyTime(mCurrentTime);
		         tLJTempFeeSchema.setModifyDate(mCurrentDate);
		         tLJTempFeeSchema.setModifyTime(mCurrentTime);
		         tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
		         tLJTempFeeSchema.setPolicyCom(tLJSPaySchema.getManageCom());
		         tLJTempFeeSchema.setManageCom(mGlobalInput.ComCode);
		         map.put(tLJTempFeeSchema, "INSERT");
		         
		         LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
		         Reflections TempClasstref = new Reflections();
		         TempClasstref.transFields(tLJTempFeeClassSchema, tLJSPaySchema);
		         tLJTempFeeClassSchema.setTempFeeNo(tGetNoticeNo);
		         tLJTempFeeClassSchema.setPayMode("4");		 
		         tLJTempFeeClassSchema.setPayMoney(tLJSPaySchema.getSumDuePayMoney());
		         tLJTempFeeClassSchema.setPayDate(mCurrentDate);		         
		         tLJTempFeeClassSchema.setMakeDate(mCurrentDate);
		         tLJTempFeeClassSchema.setModifyTime(mCurrentTime);
		         tLJTempFeeClassSchema.setModifyDate(mCurrentDate);
		         tLJTempFeeClassSchema.setModifyTime(mCurrentTime);
		         tLJTempFeeClassSchema.setOperator(mGlobalInput.Operator);
		         tLJTempFeeClassSchema.setPolicyCom(tLJSPaySchema.getManageCom());
		         tLJTempFeeClassSchema.setManageCom(mGlobalInput.ComCode);
		         map.put(tLJTempFeeClassSchema, "INSERT");
	         }
	   	    		    			
    	}
    	 return true;
    }
    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult() {
        return mResult;
    }
   
    public static void main(String arg[]) {
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "862100";
        tGI.Operator = "cm1102";

        //输入参数
        LLClaimUserSchema tLLClaimUserSchema = new LLClaimUserSchema();
        tLLClaimUserSchema.setUserCode("cm1102");        
        tLLClaimUserSchema.setUpUserCode("cm1101");
        tLLClaimUserSchema.setPrepaidFlag("1");
        tLLClaimUserSchema.setPrepaidLimit("200");
        tLLClaimUserSchema.setPrepaidUpUserCode("3");
        
        LLPrepaidUWMainSchema mLLPrepaidUWMainSchema = new LLPrepaidUWMainSchema();
        mLLPrepaidUWMainSchema.setPrepaidNo("Y001");
        
        VData tVData = new VData();
        tVData.add(tLLClaimUserSchema);
        tVData.add(mLLPrepaidUWMainSchema);
        tVData.add(tGI);

        LLPrepaidClaimConfirmBL tLLPrepaidClaimConfirmBL = new LLPrepaidClaimConfirmBL();
        if (!tLLPrepaidClaimConfirmBL.submitData(tVData, "BACK")) {
        	 CError.buildErr(tLLClaimUserSchema, "当前操作员没有预付审批权限!");
	         System.out.println();
        }
    }
}
