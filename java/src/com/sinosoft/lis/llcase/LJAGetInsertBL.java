/*
 * <p>ClassName: LJAGetInsertBL </p>
 * <p>Description: LJAGetInsertBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2005-02-26 15:27:43
 */
package com.sinosoft.lis.llcase;
import java.text.DecimalFormat;
import com.sinosoft.lis.bq.AppAcc;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LJAGetInsertBL  {

/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
/** 往后面传输数据的容器 */
private VData mResult = new VData();
private VData mInputData= new VData();
private MMap map = new MMap();
/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
private String mLocked;
/** 数据操作字符串 */
private String mOperate;
private String mCaseNo="";
private String mBackMsg = "";
private String mPrePaidFlag = "";
/** 业务处理相关变量 */
private LJAGetSchema mLJAGetSchema=new LJAGetSchema();
private LJAGetSchema saveLJAGetSchema=new LJAGetSchema();
private LJAGetSet saveLJAGetSet=new LJAGetSet();
private LJSGetSet bnfLJSGetSet = new LJSGetSet();
private LJAGetClaimSet mLJAGetClaimSet = new LJAGetClaimSet();
private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
private LLCaseExtSchema mLLCaseExtSchema = new LLCaseExtSchema();
private LLCaseExtSchema tLLCaseExtSchema = new LLCaseExtSchema();
private LJSGetSchema mLJSGetSchema = new LJSGetSchema();
/** 2366理赔红利应付记录 */
//private LJSGetSchema mHLLJSGetSchema = new LJSGetSchema();
private LJSGetClaimSet mLJSGetClaimSet =new LJSGetClaimSet();
private LLPrepaidGrpPolSet mLLPrepaidGrpPolSet = new LLPrepaidGrpPolSet();
private LLPrepaidGrpContSet mLLPrepaidGrpContSet = new LLPrepaidGrpContSet();
private LLPrepaidTraceSet mLLPrepaidTraceSet = new LLPrepaidTraceSet();

private String mCurrentData = PubFun.getCurrentDate();

private String mCurrentTime = PubFun.getCurrentTime();

public LJAGetInsertBL() {
}

/**
* 传输数据的公共方法
* @param: cInputData 输入的数据
* @cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate) {
     //取出操作符
    this.mOperate =cOperate;
    System.out.println("mOperate:"+mOperate);
    System.out.println("begin submit");

    
    //取出页面传入数据
    if (!getInputData(cInputData))
         return false;
    if (!checkInputData())
         return false;

    //进行业务处理
    if (!dealData()) {
        CError tError = new CError();
        tError.moduleName = "LJAGetInsertBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据处理失败LJAGetInsertBL-->dealData!";
        this.mErrors.addOneError(tError);
        return false;
    }
//    if (!checkMoney()) {
//    	return false;
//    }
    System.out.println("after dealdata");
    //数据提交
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, ""))
    {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        CError tError = new CError();
        tError.moduleName = "ICaseCureBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";
        this.mErrors.addOneError(tError);
        return false;
    }

    System.out.println("after tPubSubmit");
  
    mInputData=null;
    return true;
}

/**
* 从输入数据中得到所有对象
*输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
*/
private boolean getInputData(VData cInputData) {
   System.out.println("getinputData......");

   this.mLJAGetSchema = (LJAGetSchema) cInputData.getObjectByObjectName("LJAGetSchema", 0);
   this.mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
   this.mLLCaseSchema = (LLCaseSchema) cInputData.getObjectByObjectName("LLCaseSchema",0);
   this.mLLCaseExtSchema = (LLCaseExtSchema) cInputData.getObjectByObjectName("LLCaseExtSchema",0);
   if(mLJAGetSchema.getChequeNo() != null && !"".equals(mLJAGetSchema.getChequeNo())) {
      mLocked = mLJAGetSchema.getChequeNo();
      mLJAGetSchema.setChequeNo("");
   }else {
	   mLocked = "unLocked";
   }
   System.out.println("mLocked===" + mLocked);
   if (mLJAGetSchema.getOtherNo() == null) {
       CError.buildErr(this, "理赔号为空，查询不到案件信息！");
       return false;
   }
   mCaseNo = mLJAGetSchema.getOtherNo();
   
   //mLLCaseSchema.getPrePaidFlag().toString()一定不要这样写！！！会出空指针错
   System.out.println("是否回销预付赔款========="+mLLCaseSchema.getPrePaidFlag());  
   
   if(""!= mLLCaseSchema.getPrePaidFlag() && null!= mLLCaseSchema.getPrePaidFlag()&& !("").equals(mLLCaseSchema.getPrePaidFlag()))
   mPrePaidFlag = mLLCaseSchema.getPrePaidFlag();
   return true;
}

 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
    System.out.println("dealData......");
    MMap tmpMap = new MMap();
    /**取得不包括受益人的应付信息*/
    if (!getPay()) {
        return false;
    }
    if(!dealCaseInfo()){
        return false;
    }
    //TODO：判断案件是否进行保费豁免
    if(!dealExemption()){
        return false;
    };
   
    //增加案件号锁定
    MMap mMap = lockTable(mCaseNo, mGlobalInput);
    if(mMap != null){
        map.add(mMap);
    } else {
        return false;
    }
    //#2304 （需求编号：2014-215）关于“幸福一家”家庭综合保障计划理赔模块增加保单终止缴费标记的需求
    LLSpecialWrapDealBL tLLSpecialWrapDealBL = new LLSpecialWrapDealBL(mCaseNo,mGlobalInput.Operator,mLJSGetSchema.getSumGetMoney());
    if(tLLSpecialWrapDealBL.submitData("DEAL")){
    	mBackMsg += "<br>"+tLLSpecialWrapDealBL.getBackMsg();
    }
//    else{
//    	return false;
//    }
        
    //生成实付号码
    String sNoLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
    String ActuGetNo = PubFun1.CreateMaxNo("ACTUGETNO", sNoLimit);
    //应付总表钱数为正时
    if (mLJSGetSchema.getSumGetMoney() >= 0) {
        Reflections rf = new Reflections();
        rf.transFields(saveLJAGetSchema, mLJSGetSchema);
        saveLJAGetSchema.setActuGetNo(ActuGetNo);
        saveLJAGetSchema.setDrawer(mLJAGetSchema.getDrawer());
        saveLJAGetSchema.setDrawerID(mLJAGetSchema.getDrawerID());
        saveLJAGetSchema.setPayMode(mLJAGetSchema.getPayMode());
        saveLJAGetSchema.setAccName(mLJAGetSchema.getAccName());
        saveLJAGetSchema.setBankAccNo(mLJAGetSchema.getBankAccNo());
        saveLJAGetSchema.setBankCode(mLJAGetSchema.getBankCode());
        saveLJAGetSchema.setOperator(mGlobalInput.Operator);
        saveLJAGetSchema.setManageCom(mLLCaseSchema.getMngCom());
        saveLJAGetSchema.setMakeDate(mCurrentData);
        saveLJAGetSchema.setMakeTime(mCurrentTime);
        saveLJAGetSchema.setModifyDate(saveLJAGetSchema.getMakeDate());
        saveLJAGetSchema.setShouldDate(mCurrentData);
        saveLJAGetSchema.setModifyTime(saveLJAGetSchema.getMakeTime());

        for (int i = 1; i <= mLJSGetClaimSet.size(); i++) {
            LJAGetClaimSchema tLJAGetClaimSchema = new LJAGetClaimSchema();
            rf.transFields(tLJAGetClaimSchema, mLJSGetClaimSet.get(i));
            tLJAGetClaimSchema.setActuGetNo(ActuGetNo);
            tLJAGetClaimSchema.setOPConfirmDate(mCurrentData);
            tLJAGetClaimSchema.setOPConfirmCode(mGlobalInput.Operator);
            tLJAGetClaimSchema.setOPConfirmTime(mCurrentTime);
            tLJAGetClaimSchema.setOperator(mGlobalInput.Operator);
//            tLJAGetClaimSchema.setManageCom(mLLCaseSchema.getMngCom());
            tLJAGetClaimSchema.setMakeDate(mCurrentData);
            tLJAGetClaimSchema.setMakeTime(mCurrentTime);
            tLJAGetClaimSchema.setModifyDate(tLJAGetClaimSchema.getMakeDate());
            tLJAGetClaimSchema.setModifyTime(tLJAGetClaimSchema.getMakeTime());

            mLJAGetClaimSet.add(tLJAGetClaimSchema);
        }
        //处理申诉纠错，与受益人分配相关
        if (mLLCaseSchema.getRgtType().equals("4") ||
            mLLCaseSchema.getRgtType().equals("5")) {
            if(!dealAppeal()){
                CError.buildErr(this, "处理申诉信息失败！");
                return false;
            }
        }

        //更新lcget表中的领取现金（此分配不受受益人影响）
        tmpMap = LLCaseCommon.addSumMoney(mCaseNo,mGlobalInput.Operator);
        /*if (tmpMap == null) {
            CError.buildErr(this, "更新lcget中的领取现金失败！");
            return false;
        }*/
		if (tmpMap == null||tmpMap.size()==0) {
            	
            	for(int m=1;m<=mLJAGetClaimSet.size();m++)
            	{
            		LBRnewStateLogDB tLBRnewStateLogDB=new LBRnewStateLogDB();
                	LBRnewStateLogSet tLBRnewStateLogSet=new LBRnewStateLogSet();
            		tLBRnewStateLogDB.setNewContNo(mLJAGetClaimSet.get(m).getContNo());
            		tLBRnewStateLogSet=tLBRnewStateLogDB.query();
            		if(tLBRnewStateLogSet.size()>0)
            		{
            			CError.buildErr(this, "保单在理赔过程中做了续保回退,请将案件回退到检录，重新选择保单进行理算！");
                        return false;
            		}
            	}
                CError.buildErr(this, "给付责任查询失败！");
                return false;
        }
        //处理账户金额，需要加条件判断（此分支不受受益人分配影响）
        String delSql5 =
                "delete from LCInsureAccTrace where othertype ='5' "
                +" and otherno='" +mCaseNo + "'";
        map.put(delSql5,"DELETE");
        if(!dealInsureAcc()){
            CError.buildErr(this, "账户金额处理失败！");
            return false;
        }
        if (!dealWNAcc()) {
        	CError.buildErr(this, "万能账户金额处理失败！");
            return false;
        }
        
        if (!dealPrapaid()) {
        	CError.buildErr(this, "预付赔款处理失败！");
            return false;
        }
        if(bnfLJSGetSet.size()>0){
            if(!dealBnf()){
                return false;
            }
        }else{
           saveLJAGetSet.add(saveLJAGetSchema);
        }
        String delSql1 =
                "delete from LJAGetclaim where othernotype='5' and otherno='" +
                mCaseNo + "' and feeoperationtype<>'FC'";//处理财务反冲
        String delSql2 =
                "delete from LJAGet where othernotype='5' and otherno='" +
                mCaseNo + "' and (finstate<>'FC' or finstate is null)";//处理财务反冲
        map.put(delSql1, "DELETE");
        map.put(delSql2, "DELETE");

        //把应付表插入实付表
        map.put(saveLJAGetSet, "INSERT");
        map.put(mLJAGetClaimSet, "INSERT");
        //删除应付表
        String delSql3 =
                "delete from LJSGetclaim where othernotype in ('5','B','H') "//2366添加对分红险ljs应付明细表H分红类型数据
                +" and otherno='" +mCaseNo + "'";
        String delSql4 =
                "delete from LJSGet where othernotype in ('5','B') "
                +" and otherno='" +mCaseNo + "'";
        map.put(delSql3, "DELETE");
        map.put(delSql4, "DELETE");
    } else { //应付总表的钱数为正时处理完毕
        //开始处理钱数为负的情况，把应付总表中为负的数据转到应收总表
        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
        tLJSPaySchema.setGetNoticeNo(mLJSGetSchema.
                                     getGetNoticeNo());
        tLJSPaySchema.setOtherNo(mCaseNo);
        tLJSPaySchema.setOtherNoType("5"); //待确定号码类型
        tLJSPaySchema.setAppntNo(mLJSGetSchema.getAppntNo());
        tLJSPaySchema.setSumDuePayMoney(0 - mLJSGetSchema.getSumGetMoney());
        map.put(tLJSPaySchema, "INSERT"); //插入应收表
    }
    
    //2366处理红利的应付数据
    if (checkFenHong(mCaseNo)) {
    	if (!dealHL()){
	    	CError.buildErr(this, "理赔红利金处理失败！");
	        return false;
    	}
    }
    
    map.add(tmpMap);
    //销户处理
    //map.add(LLCaseCommon.gotoBack(mCaseNo,mGlobalInput));

    try {
        mInputData.clear();
        mInputData.add(map);
    } catch (Exception ex) {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "ICaseCureBL";
        tError.functionName = "prepareOutputData";
        tError.errorMessage = "在准备往后层处理所需要的数据时出错:" + ex.toString();
        mErrors.addOneError(tError);
        return false;
    }
    return true;
}

/**
 * TODO:#2366 通过 传入aCaseno判断是否赔了分红险,在应付表中有数据,且赔付金额大于0
 *
 */
private boolean checkFenHong(String aCaseno){
	ExeSQL tExeSQL = new ExeSQL();
	String tSQL11 =  "";
	tSQL11 = "select 1 from ljsgetclaim a where a.otherno='"+aCaseno+
			"' and othernotype='H' and pay >0 "+
			" and exists (select 1 from lmriskapp where risktype4 = '2' and riskcode=a.riskcode)";
	SSRS tMsgSSRS = tExeSQL.execSQL(tSQL11);
	if(tMsgSSRS==null||tMsgSSRS.getMaxRow()<=0){
		return false;
	}else{
        for (int i = 1; i <= tMsgSSRS.getMaxRow(); i++) {
        	if("1".equals(tMsgSSRS.GetText(i, 1))){
        		return true;
        	}
        }
        return false;
	}
}
/**
 * TODO：2366给付确认处对红利进行处理
 * @return
 */
private boolean dealHL() {
		//2366分红险ljsgetclaim中有红利保费与红利利息明细,ljaget中为红利与赔款的合计
		//应该向投保人支付红利		 
    	//只处理申请类案件
		System.out.println("dealHL");
        if ("1".equals(mLLCaseSchema.getRgtType())) {
       
            LJAGetClaimSet aHLLJAGetClaimSet = new LJAGetClaimSet();
            //红利需生成两笔赔款  star  2017-07-18
            LJAGetSet aHLLJAGetSet = new LJAGetSet();
            
          
            //需要重新生成实付号 --红利与赔款分开
            String sNoLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
            String ActuGetNo = PubFun1.CreateMaxNo("ACTUGETNO", sNoLimit);//一笔需要同一个actugetno
//            String ActuGetNo =saveLJAGetSchema.getActuGetNo();
            Reflections rf = new Reflections();
            
            LJSGetDB tLJSGetDB = new LJSGetDB();
            tLJSGetDB.setOtherNo(mCaseNo);
            tLJSGetDB.setOtherNoType("H");
            if(tLJSGetDB.query().size()<=0){
            	CError.buildErr(this, "红利财务账户信息生成失败，请重新进行理算!");
                return false;
            }
            LJSGetSet aLJSGetSet = new LJSGetSet();
            aLJSGetSet = tLJSGetDB.query();
            for(int i = 1; i <= aLJSGetSet.size(); i++){
            	LJAGetSchema tLJAGetSchema = new LJAGetSchema();
                rf.transFields(tLJAGetSchema,aLJSGetSet.get(i));
                tLJAGetSchema.setActuGetNo(ActuGetNo);
                tLJAGetSchema.setDrawer(mLJAGetSchema.getDrawer());
                tLJAGetSchema.setDrawerID(mLJAGetSchema.getDrawerID());
                tLJAGetSchema.setPayMode(mLJAGetSchema.getPayMode());
                tLJAGetSchema.setAccName(mLJAGetSchema.getAccName());
                tLJAGetSchema.setBankAccNo(mLJAGetSchema.getBankAccNo());
                tLJAGetSchema.setBankCode(mLJAGetSchema.getBankCode());
                tLJAGetSchema.setOperator(mGlobalInput.Operator);
                tLJAGetSchema.setManageCom(mLLCaseSchema.getMngCom());
                tLJAGetSchema.setMakeDate(mCurrentData);
                tLJAGetSchema.setMakeTime(mCurrentTime);
                tLJAGetSchema.setModifyDate(saveLJAGetSchema.getMakeDate());
                tLJAGetSchema.setShouldDate(mCurrentData);
                tLJAGetSchema.setModifyTime(saveLJAGetSchema.getMakeTime());
                aHLLJAGetSet.add(tLJAGetSchema);
            }
        	LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB(); 
        	tLJSGetClaimDB.setOtherNo(mCaseNo);
        	tLJSGetClaimDB.setOtherNoType("H");
        	if(tLJSGetClaimDB.query().size()<=0)
        	{
        		CError.buildErr(this, "红利财务账户信息生成失败，请重新进行理算!");
                return false; 
        	}
            LJSGetClaimSet aLJSGetClaimSet = new LJSGetClaimSet();
            aLJSGetClaimSet = tLJSGetClaimDB.query();
            
           
            
            for (int j = 1; j <= aLJSGetClaimSet.size(); j++) {
                LJAGetClaimSchema tLJAGetClaimSchema = new LJAGetClaimSchema();
                rf.transFields(tLJAGetClaimSchema, aLJSGetClaimSet.get(j));
                tLJAGetClaimSchema.setActuGetNo(ActuGetNo);
                tLJAGetClaimSchema.setOtherNoType("H");
                tLJAGetClaimSchema.setOPConfirmDate(mCurrentData);
                tLJAGetClaimSchema.setOPConfirmCode(mGlobalInput.Operator);
                tLJAGetClaimSchema.setOPConfirmTime(mCurrentTime);
                tLJAGetClaimSchema.setOperator(mGlobalInput.Operator);
                tLJAGetClaimSchema.setMakeDate(mCurrentData);
                tLJAGetClaimSchema.setMakeTime(mCurrentTime);
                tLJAGetClaimSchema.setModifyDate(tLJAGetClaimSchema.getMakeDate());
                tLJAGetClaimSchema.setModifyTime(tLJAGetClaimSchema.getMakeTime());

                aHLLJAGetClaimSet.add(tLJAGetClaimSchema);
            }   
            
            //对于累计生息的分红保单，将投保人账户清零--待开发
                      
            String delSql1 =
                "delete from LJAGetclaim where othernotype='H' and otherno='" +
                mCaseNo + "' and feeoperationtype<>'FC'";//处理财务反冲
	        
	        map.put(delSql1, "DELETE");
            map.put(aHLLJAGetClaimSet,"INSERT");
            map.put(aHLLJAGetSet, "INSERT");
            return true;
            //红利需生成两笔赔款  end  2017-07-18  	
        }          		
	
	return true;
}

/**
 * 查询保单赔付相关数据,其中ljsgetclaim中只查询理赔赔款
 */
public boolean getPay() {
  System.out.println("Get pay from LJSGetClaim......");
  //查询赔付应付表
  LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB();
  tLJSGetClaimDB.setOtherNo(mCaseNo);//理赔号
  tLJSGetClaimDB.setOtherNoType("5");//类型为理赔赔款 
  mLJSGetClaimSet.set(tLJSGetClaimDB.query());
  if (tLJSGetClaimDB.mErrors.needDealError() == true) {
    // @@错误处理
    CError.buildErr(this, "赔付应付表LJSGetClaim查询失败!");
    return false;
  }
  if(mLJSGetClaimSet==null||mLJSGetClaimSet.size()==0)
  {
    CError.buildErr(this, "赔付应付表LJSGetClaim无数据!");
    return false;
  }
  
  String tMngCom = "";
  for (int j = 1; j <= mLJSGetClaimSet.size(); j++) {
  	if (mLJSGetClaimSet.get(j).getPay() == 0) {
  		continue;
  	}
  	if (tMngCom.equals("")) {
  		tMngCom = "" + mLJSGetClaimSet.get(j).getManageCom();
  	}
  	if (!tMngCom.equals("" + mLJSGetClaimSet.get(j).getManageCom())) {
  		CError.buildErr(this, "一个案件下不能进行多机构保单赔付，请分案件处理！");
  		return false;
  	}
  }
  
  //查询应付总表LJSGet,#1617 对于一个案件来说，只需要有一个给付通知书号，但是理赔红利需要新的给付通知书号
  LJSGetDB tLJSGetDB = new LJSGetDB();
  //业务应付号码(此处与理赔业务‘5’对应，但是查询出的应付包含整个理赔的总应付，不含红利)
  tLJSGetDB.setGetNoticeNo(mLJSGetClaimSet.get(1).getGetNoticeNo());
  if (!tLJSGetDB.getInfo()) {
      CError.buildErr(this, "应付总表LJSGet查询失败!");
      return false;
  }
  mLJSGetSchema.setSchema(tLJSGetDB.getSchema());
  
  //查询应付总表LJSGet
  tLJSGetDB = new LJSGetDB();
  tLJSGetDB.setOtherNo(mCaseNo);
  tLJSGetDB.setOtherNoType("B");
  bnfLJSGetSet = tLJSGetDB.query();
  return true;
}

    /**
     * 受益人给付处理处理
     * @return boolean
     */
    private boolean dealBnf() {
      System.out.println("dealBnf......");
      LJAGetSet bnfLJAGetSet = new LJAGetSet();
      LJAGetClaimSet bnfLJAGetClaimSet = new LJAGetClaimSet();
      for(int i=1;i<=bnfLJSGetSet.size();i++){
          double subpay = saveLJAGetSchema.getSumGetMoney()-bnfLJSGetSet.get(i).getSumGetMoney();
          saveLJAGetSchema.setSumGetMoney(subpay);
          String sNoLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
          String ActuGetNo = PubFun1.CreateMaxNo("ACTUGETNO", sNoLimit);
          Reflections rf = new Reflections();
          LJAGetSchema aLJAGetSchema = new LJAGetSchema();
          rf.transFields(aLJAGetSchema, bnfLJSGetSet.get(i));
          aLJAGetSchema.setActuGetNo(ActuGetNo);
          aLJAGetSchema.setOtherNoType("5");
          aLJAGetSchema.setOperator(mGlobalInput.Operator);
          aLJAGetSchema.setManageCom(mLLCaseSchema.getMngCom());
          aLJAGetSchema.setMakeDate(mCurrentData);
          aLJAGetSchema.setMakeTime(mCurrentTime);
          aLJAGetSchema.setModifyDate(saveLJAGetSchema.getMakeDate());
          aLJAGetSchema.setShouldDate(mCurrentData);
          aLJAGetSchema.setModifyTime(saveLJAGetSchema.getMakeTime());
          bnfLJAGetSet.add(aLJAGetSchema);
          LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB();
          LJSGetClaimSet aLJSGetClaimSet = new LJSGetClaimSet();
          tLJSGetClaimDB.setGetNoticeNo(bnfLJSGetSet.get(i).getGetNoticeNo());
          aLJSGetClaimSet = tLJSGetClaimDB.query();
          for (int j = 1; j <= aLJSGetClaimSet.size(); j++) {
              LJAGetClaimSchema tLJAGetClaimSchema = new LJAGetClaimSchema();
              rf.transFields(tLJAGetClaimSchema, aLJSGetClaimSet.get(j));
              tLJAGetClaimSchema.setActuGetNo(ActuGetNo);
              tLJAGetClaimSchema.setOtherNoType("5");
              tLJAGetClaimSchema.setOPConfirmDate(mCurrentData);
              tLJAGetClaimSchema.setOPConfirmCode(mGlobalInput.Operator);
              tLJAGetClaimSchema.setOPConfirmTime(mCurrentTime);
              tLJAGetClaimSchema.setOperator(mGlobalInput.Operator);
//              tLJAGetClaimSchema.setManageCom(mLLCaseSchema.getMngCom());
              tLJAGetClaimSchema.setMakeDate(mCurrentData);
              tLJAGetClaimSchema.setMakeTime(mCurrentTime);
              tLJAGetClaimSchema.setModifyDate(tLJAGetClaimSchema.getMakeDate());
              tLJAGetClaimSchema.setModifyTime(tLJAGetClaimSchema.getMakeTime());

              bnfLJAGetClaimSet.add(tLJAGetClaimSchema);
          }
      }

      saveLJAGetSet.add(bnfLJAGetSet);
      LJAGetClaimSet tempLJAGetClaimSet = mLJAGetClaimSet;
      mLJAGetClaimSet = new LJAGetClaimSet();
      if(saveLJAGetSchema.getSumGetMoney()>0.001){
          for (int k = 1; k <= tempLJAGetClaimSet.size(); k++) {
              if (!tempLJAGetClaimSet.get(k).getGetDutyCode().equals("BNF")){
                  mLJAGetClaimSet.add(tempLJAGetClaimSet.get(k));
              }
          }
          saveLJAGetSet.add(saveLJAGetSchema);
      }
      mLJAGetClaimSet.add(bnfLJAGetClaimSet);
      return true;
    }

    /**
     * 处理案件状态及案件的给付信息
     * @return boolean
     */
    private boolean dealCaseInfo() {
//        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mCaseNo);
        if (!tLLCaseDB.getInfo()) {
            CError.buildErr(this, "理赔案件信息查询失败！");
            return false;
        }
//        if (!tLLCaseDB.getRgtState().equals("09")) {
//            CError tError = new CError();
//            tError.moduleName = "LJAGetInsertBL";
//            tError.functionName = "dealData";
//            tError.errorMessage = "此案件状态不能做给付确认！";
//            mErrors.addOneError(tError);
//            return false;
//        }
//        String tAccModifyReason = mLLCaseSchema.getAccModifyReason();
        mLLCaseSchema = tLLCaseDB.getSchema();
        //更新是否回销预付赔款标记
        mLLCaseSchema.setPrePaidFlag(mPrePaidFlag);
        //更新登记表中的银行账户字段
        mLLCaseSchema.setCaseGetMode(mLJAGetSchema.getPayMode());
        mLLCaseSchema.setAccName(mLJAGetSchema.getAccName());
        mLLCaseSchema.setBankAccNo(mLJAGetSchema.getBankAccNo());
        mLLCaseSchema.setBankCode(mLJAGetSchema.getBankCode());
//      mLLCaseSchema.setAccModifyReason(tAccModifyReason);
//      tLLCaseSchema.setEndCaseDate(mCurrentData); //结案日期
        //更新案件的的状态为“通知状态”
        mLLCaseSchema.setRgtState("11");
        mLLCaseSchema.setModifyDate(mCurrentData);
        mLLCaseSchema.setModifyTime(mCurrentTime);
        map.put(mLLCaseSchema, "UPDATE");
      //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
        
        if(this.mLLCaseExtSchema!=null){
        	
        	LLCaseExtDB tLLCaseExtDB = new LLCaseExtDB();
            tLLCaseExtDB.setCaseNo(mCaseNo);
            if (tLLCaseExtDB.getInfo()) {
            	tLLCaseExtSchema = tLLCaseExtDB.getSchema();
            	if(!"".equals(mLLCaseExtSchema.getDrawerIDType())&& mLLCaseExtSchema.getDrawerIDType()!=null){
            		tLLCaseExtSchema.setDrawerIDType(mLLCaseExtSchema.getDrawerIDType());
            	}else{
            		tLLCaseExtSchema.setDrawerIDType(tLLCaseExtSchema.getDrawerIDType());
            	}
                tLLCaseExtSchema.setDrawerID(mLLCaseExtSchema.getDrawerID());
                tLLCaseExtSchema.setModifyTime(PubFun.getCurrentTime());
                tLLCaseExtSchema.setModifyDate(PubFun.getCurrentDate());
                
                
                
                map.put(tLLCaseExtSchema, "UPDATE");
            }else{
            	if(!"".equals(mLLCaseExtSchema.getDrawerIDType())&& mLLCaseExtSchema.getDrawerIDType()!=null){
            		tLLCaseExtSchema.setDrawerIDType(mLLCaseExtSchema.getDrawerIDType());
            	 
            	tLLCaseExtSchema.setCaseNo(mCaseNo);
            	tLLCaseExtSchema.setOperator(mGlobalInput.Operator);
            	tLLCaseExtSchema.setMngCom(mGlobalInput.ManageCom);
            	tLLCaseExtSchema.setMakeDate(PubFun.getCurrentDate());
            	tLLCaseExtSchema.setMakeTime(PubFun.getCurrentTime());
            	tLLCaseExtSchema.setModifyTime(PubFun.getCurrentTime());
            	tLLCaseExtSchema.setModifyDate(PubFun.getCurrentDate());
                map.put(tLLCaseExtSchema, "INSERT");
            	}
            }
            
        }
        
      //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
        //更新赔案状态--WUJS把赔案置为给付状态
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setCaseNo(mCaseNo);
        LLClaimSet tLLClaimSet = tLLClaimDB.query();
        if (tLLClaimSet != null && tLLClaimSet.size() > 0) {
            for (int i = 1; i < tLLClaimSet.size(); i++) {
                //置给付状态
                tLLClaimSet.get(i).setClmState("3");
            }
            map.put(tLLClaimSet, "UPDATE");
        }
        //立案信息处理
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mLLCaseSchema.getRgtNo());
        if (!tLLRegisterDB.getInfo()) {
            CError.buildErr(this, "立案信息查询失败！");
            return false;
        }
        LLRegisterSchema mLLRegisterSchema = tLLRegisterDB.getSchema();
        //团单下最后一个人做完给付确认应更新团单案件的状态为“05”－团体的给付通知状态
        if ("1".equals(mLLRegisterSchema.getRgtClass())) {
            boolean tFlag = true;
            LLCaseDB tmpLLCaseDB = new LLCaseDB();
            LLCaseSet tmpLLCaseSet = new LLCaseSet();
            tmpLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
            tmpLLCaseSet.set(tmpLLCaseDB.query());
            if (tmpLLCaseSet != null && tmpLLCaseSet.size() != 0) {
                for (int m = 1; m <= tmpLLCaseSet.size(); m++) {
                    if (tmpLLCaseSet.get(m).getCaseNo().equals(mCaseNo))
                        continue;
                    if (!("12".equals(tmpLLCaseSet.get(m).getRgtState())) &&
                        !("11".equals(tmpLLCaseSet.get(m).getRgtState())) &&
                        !("14".equals(tmpLLCaseSet.get(m).getRgtState()))) {
                        tFlag = false;
                        break;
                    }
                }
            }
            if (tFlag) {
                mLLRegisterSchema.setRgtState("05");
                mLLRegisterSchema.setModifyDate(mCurrentData);
                mLLRegisterSchema.setModifyTime(mCurrentTime);
                map.put(mLLRegisterSchema, "UPDATE");
            }
        } else {
            //个人申请则将给付方式及银行账户信息与付费表同步
            mLLRegisterSchema.setCaseGetMode(mLJAGetSchema.getPayMode());
            mLLRegisterSchema.setAccName(mLJAGetSchema.getAccName());
            mLLRegisterSchema.setBankAccNo(mLJAGetSchema.getBankAccNo());
            mLLRegisterSchema.setBankCode(mLJAGetSchema.getBankCode());
            mLLRegisterSchema.setModifyDate(mCurrentData);
            mLLRegisterSchema.setModifyTime(mCurrentTime);
            map.put(mLLRegisterSchema, "UPDATE");
        }
        return true;
    }
    
    /**
     * 豁免保费处理
     * @return boolean
     */
    
    private boolean dealExemption(){
        System.out.println("给付确认-开始豁免保费处理");
        
        //数据交换的容器
        Reflections ref = new Reflections();
        
        //豁免险判断，如果是申诉纠错案件换回C的案件进行判断
        String HMCaseNo = mCaseNo;
        if("R".equals(mCaseNo.substring(0,1)) || "S".equals(mCaseNo.substring(0,1)))
        {
            System.out.println("申诉纠错案件换为正常的C案件");
            String sql = "select caseno from LLAppeal where appealno ='"+mCaseNo+"'";
            ExeSQL exeSQL = new ExeSQL();
            HMCaseNo = exeSQL.getOneValue(sql);
        }
        String tSQL1 = "select contno from llexemption where caseno='"+HMCaseNo+"' group by contno";
        ExeSQL tExeSQL1 = new ExeSQL();
        SSRS tSSRS1 = new SSRS();
        tSSRS1 = tExeSQL1.execSQL(tSQL1);
        
        if(tSSRS1==null || tSSRS1.getMaxRow()<=0){
            //没有需要豁免的案件
            return true;
        }
        for(int i=1;i <= tSSRS1.getMaxRow();i++){//按照保单进行最外层循环
            String tContNo = tSSRS1.GetText(i, 1);//保单号
            
            LLExemptionDB tLLExemptionDB = new LLExemptionDB();
            LLExemptionSet tLLExemptionSet = new LLExemptionSet();
            tLLExemptionDB.setCaseNo(HMCaseNo);
            tLLExemptionDB.setContNo(tContNo);
            tLLExemptionSet = tLLExemptionDB.query();
            
            String tPayToDate = tLLExemptionSet.get(1).getPayToDate();//缴至日期
            String tState = tLLExemptionSet.get(1).getState();//豁免记录状态
            double tFreeAmnt = tLLExemptionSet.get(1).getFreeAmnt();//豁免金额
            String zPolNo = tLLExemptionSet.get(1).getPolNo(); //豁免保单号    
            if("temp".equals(tState)){
                //正常案件的豁免给付
                if(tFreeAmnt != 0.00 && tFreeAmnt > 0.00){//多缴保费
                    
                    //求退费期数以及payno            
                    String tSQL3 = "select payno from ljapayperson c," +
                            "(select curpaytodate,contno,riskcode ,polno from ljapayperson a where " +
                            "exists (select 1 from ljapay b where a.contno = b.incomeno and duefeetype ='1') " +
                            "and contno ='"+tContNo+"' and polno='"+ zPolNo +"' " +  //3081 康乐豁免调整
                            "and '"+tPayToDate+"' > lastpaytodate and '"+tPayToDate+"' <= curpaytodate) aa " +
                            "where c.contno=aa.contno and c.curpaytodate >aa.curpaytodate " +
                            "and c.riskcode = aa.riskcode  and c.polno=aa.polno group by payno";
                    ExeSQL tExeSQL3 = new ExeSQL();
                    SSRS tSSRS3 = new SSRS();
                    tSSRS3 = tExeSQL3.execSQL(tSQL3);
                    //投保人账户转出
                    double mZCMoney = 0.0;
                
                    LJAPayPersonSet tLJAPayPersonSetDealt = new LJAPayPersonSet();
                    LJAPaySet tLJAPaySetDealt = new LJAPaySet();
                    for(int k=1;k <= tSSRS3.getMaxRow();k++){
                        //生成新的实收号
                        String limit = PubFun.getNoLimit(mGlobalInput.ManageCom);
                        String mPayNo = PubFun1.CreateMaxNo("PayNo",limit);
                        //汇总反冲金额
                        double tMoney = 0.00;
                        //实收号
                        String tPayNo = tSSRS3.GetText(k, 1);
                        
                        for(int n = 1; n <= tLLExemptionSet.size(); n++){
                            String mPolNo = tLLExemptionSet.get(n).getPolNo();//险种号
                            
                            LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
                            LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
                            tLJAPayPersonDB.setPolNo(mPolNo);
                            tLJAPayPersonDB.setPayNo(tPayNo);
                            tLJAPayPersonSet = tLJAPayPersonDB.query();
                              
                            for(int m = 1; m <= tLJAPayPersonSet.size(); m++)
                            {
                                LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
                                ref.transFields(tLJAPayPersonSchema, tLJAPayPersonSet.get(m));

                                //计算实收表需要反冲的金额
                                double temp = -tLJAPayPersonSchema.getSumDuePayMoney();
                                tMoney += temp;
                                tLJAPayPersonSchema.setSumDuePayMoney(temp);
                                tLJAPayPersonSchema.setSumActuPayMoney(temp);
                                
                                //#3272 保费豁免相关价税分离功能完善
                                tLJAPayPersonSchema.setMoneyNoTax(null);
                                tLJAPayPersonSchema.setMoneyTax(null);
                                tLJAPayPersonSchema.setBusiType(null);
                                tLJAPayPersonSchema.setTaxRate(null);
                                //#3272 保费豁免相关价税分离功能完善
                                
                                tLJAPayPersonSchema.setPayDate(this.mCurrentData);
                                tLJAPayPersonSchema.setEnterAccDate(this.mCurrentData);
                                tLJAPayPersonSchema.setPayNo(mPayNo);
                                tLJAPayPersonSchema.setConfDate(mCurrentData);
                                tLJAPayPersonSchema.setEnterAccDate(mCurrentData);
                                tLJAPayPersonSchema.setMakeDate(this.mCurrentData);
                                tLJAPayPersonSchema.setMakeTime(this.mCurrentTime);
                                tLJAPayPersonSchema.setModifyDate(this.mCurrentData);
                                tLJAPayPersonSchema.setModifyTime(this.mCurrentTime);
                                tLJAPayPersonSchema.setOperator(mGlobalInput.Operator);
                                
                                System.out.println("Person-SumActuPayMoney:" + tMoney);
                                tLJAPayPersonSetDealt.add(tLJAPayPersonSchema);
                            }
                        }
                        
                        //反冲ljapay的金额，为之前ljapayperson的汇总,对应的实收号
                        LJAPayDB tLJAPayDB = new LJAPayDB();
                        LJAPaySchema mLJAPaySchema = new LJAPaySchema();
                        LJAPaySet mLJAPaySet = new LJAPaySet();
                        tLJAPayDB.setPayNo(tPayNo);
                        mLJAPaySet = tLJAPayDB.query();
                        mLJAPaySchema = mLJAPaySet.get(1).getSchema();
                        //总实收表回退
                        LJAPaySchema tLJAPaySchema = new LJAPaySchema();
                        ref.transFields(tLJAPaySchema, mLJAPaySchema);
                        tLJAPaySchema.setSumActuPayMoney(tMoney);
                        tLJAPaySchema.setPayDate(mCurrentData);
                        tLJAPaySchema.setEnterAccDate(mCurrentData);
                        tLJAPaySchema.setConfDate(mCurrentData);
                        tLJAPaySchema.setPayNo(mPayNo);
                        tLJAPaySchema.setMakeDate(mCurrentData);
                        tLJAPaySchema.setMakeTime(mCurrentTime);
                        tLJAPaySchema.setModifyDate(mCurrentData);
                        tLJAPaySchema.setModifyTime(mCurrentTime);
                        tLJAPaySchema.setOperator(mGlobalInput.Operator);
                        tLJAPaySchema.setDueFeeType("1");//续期续保；modify by fuxin 2008-7-3 新财务接口提数要求。
                        
                        System.out.println("SumActuPayMoney:"+ tMoney);
                        tLJAPaySetDealt.add(tLJAPaySchema);
                        
                        mZCMoney = (-tMoney);
//                      2.将多缴保费退出至投保人账户：LCAppAccTrace添加轨迹，并将金额增加到LCAppAcc表
                        //1)根据保单号查找到投保人,循环判断有多少投保人
                        
                        String tSQL4 = "select appntno from lccont where contno='"+tContNo+"'";
                        ExeSQL tExeSQL4 = new ExeSQL();
                        SSRS tSSRS4 = new SSRS();
                        tSSRS4 = tExeSQL4.execSQL(tSQL4);
                     
                        for(int j=1;j <=tSSRS4.getMaxRow();j++){
                            String tAppntNo = tSSRS4.GetText(j, 1);                 
                            System.out.println("tAppntNo:"+tAppntNo);
                            System.out.println("mZCMoney:"+mZCMoney);
                            if(!dealAcc(tAppntNo,HMCaseNo,mZCMoney)){
                                CError.buildErr(this, "保费退出至投保人账户失败！");
                                return false; 
                            }
                        }
                    } 
                    
                    map.put(tLJAPayPersonSetDealt, "INSERT");
                    map.put(tLJAPaySetDealt, "INSERT");
 
                    //3.将LCPol,lcduty中paytodate置为payenddate
                    for(int n = 1; n <= tLLExemptionSet.size(); n++){
                        String tPolNo = tLLExemptionSet.get(n).getPolNo();//险种号
                        LCPolDB tLCPolDB = new LCPolDB();
                        LCPolSet tLCPolSet = new LCPolSet();
                        
                        tLCPolDB.setPolNo(tPolNo);
                        tLCPolSet = tLCPolDB.query();
                        for(int j = 1;j <= tLCPolSet.size();j++){
                            String newPayToDate = tLCPolSet.get(j).getPayEndDate();
                            System.out.println("newPolPayToDate:"+newPayToDate);
                            tLCPolSet.get(j).setPaytoDate(newPayToDate);
                            tLCPolSet.get(j).setOperator(mGlobalInput.Operator);
                            tLCPolSet.get(j).setModifyDate(mCurrentData);
                            tLCPolSet.get(j).setModifyTime(mCurrentTime);
                            map.put(tLCPolSet, "UPDATE"); 
                        }
                        LCDutyDB tLCDutyDB = new LCDutyDB();
                        LCDutySet tLCDutySet = new LCDutySet();
                        
                        tLCDutyDB.setPolNo(tPolNo);
                        tLCDutySet = tLCDutyDB.query();
                        for(int j = 1;j <= tLCDutySet.size();j++){
                            String newPayToDate = tLCDutySet.get(j).getPayEndDate();
                            System.out.println("newDutyPayToDate:"+newPayToDate);
                            tLCDutySet.get(j).setPaytoDate(newPayToDate);
                            tLCDutySet.get(j).setOperator(mGlobalInput.Operator);
                            tLCDutySet.get(j).setModifyDate(mCurrentData);
                            tLCDutySet.get(j).setModifyTime(mCurrentTime);
                            map.put(tLCDutySet, "UPDATE"); 
                        }
                        
                    }
                    
                    //判断是否保单下所有险种均被豁免
                    String tSQL5 = "select payenddate from lcpol where contno = '"+tContNo+"' and polno in(select polno from llexemption where caseno='"+HMCaseNo+"')" ;
                    ExeSQL tExeSQL5 = new ExeSQL();
                    SSRS tSSRS5 = new SSRS();
                    tSSRS5 = tExeSQL5.execSQL(tSQL5);
                    if(tLLExemptionSet.size() == tSSRS5.getMaxRow()){//保单下均被豁免，将LCCont中时间改变
                        for(int j=1 ; j <= tSSRS5.getMaxRow() ; j++){
                            String tPayEndDate = tSSRS5.GetText(j, 1);//缴至日期
                            LCContDB tLCContDB = new LCContDB();
                            LCContSet tLCContSet = new LCContSet();
                            tLCContDB.setContNo(tContNo);
                            tLCContSet = tLCContDB.query();
                            for(int k=1 ;k <= tLCContSet.size(); k++){
                                tLCContSet.get(k).setPaytoDate(tPayEndDate);
                                tLCContSet.get(k).setOperator(mGlobalInput.Operator);
                                tLCContSet.get(k).setModifyDate(mCurrentData);
                                tLCContSet.get(k).setModifyTime(mCurrentTime);
                                map.put(tLCContSet, "UPDATE"); 
                            }
                        } 
                    }
                }else{//无多缴保费
                    //3.将LCPol中paytodate置为payenddate
                    for(int n = 1; n <= tLLExemptionSet.size(); n++){
                        String tPolNo = tLLExemptionSet.get(n).getPolNo();//险种号
                        LCPolDB tLCPolDB = new LCPolDB();
                        LCPolSet tLCPolSet = new LCPolSet();
                        
                        tLCPolDB.setPolNo(tPolNo);
                        tLCPolSet = tLCPolDB.query();
                        for(int j = 1;j <= tLCPolSet.size();j++){
                            String newPayToDate = tLCPolSet.get(j).getPayEndDate();
                            System.out.println("newPayToDate:"+newPayToDate);
                            tLCPolSet.get(j).setPaytoDate(newPayToDate);
                            tLCPolSet.get(j).setOperator(mGlobalInput.Operator);
                            tLCPolSet.get(j).setModifyDate(mCurrentData);
                            tLCPolSet.get(j).setModifyTime(mCurrentTime);
                            map.put(tLCPolSet, "UPDATE"); 
                        }
                        LCDutyDB tLCDutyDB = new LCDutyDB();
                        LCDutySet tLCDutySet = new LCDutySet();
                        
                        tLCDutyDB.setPolNo(tPolNo);
                        tLCDutySet = tLCDutyDB.query();
                        for(int j = 1;j <= tLCDutySet.size();j++){
                            String newPayToDate = tLCDutySet.get(j).getPayEndDate();
                            System.out.println("newDutyPayToDate:"+newPayToDate);
                            tLCDutySet.get(j).setPaytoDate(newPayToDate);
                            tLCDutySet.get(j).setOperator(mGlobalInput.Operator);
                            tLCDutySet.get(j).setModifyDate(mCurrentData);
                            tLCDutySet.get(j).setModifyTime(mCurrentTime);
                            map.put(tLCDutySet, "UPDATE"); 
                        }
                    }
                    //判断是否保单下所有险种均被豁免
                    String tSQL3 = "select payenddate from lcpol where contno = '"+tContNo+"' and polno in(select polno from llexemption where caseno='"+HMCaseNo+"')" ;
                    ExeSQL tExeSQL3 = new ExeSQL();
                    SSRS tSSRS3 = new SSRS();
                    tSSRS3 = tExeSQL3.execSQL(tSQL3);
                    if(tLLExemptionSet.size() == tSSRS3.getMaxRow()){//保单下均被豁免，将LCCont中时间改变
                        for(int j=1 ; j <= tSSRS3.getMaxRow() ; j++){
                            String tPayEndDate = tSSRS3.GetText(j, 1);//缴至日期
                            LCContDB tLCContDB = new LCContDB();
                            LCContSet tLCContSet = new LCContSet();
                            tLCContDB.setContNo(tContNo);
                            tLCContSet = tLCContDB.query();
                            for(int k=1 ;k <= tLCContSet.size(); k++){
                                tLCContSet.get(k).setPaytoDate(tPayEndDate);
                                tLCContSet.get(k).setOperator(mGlobalInput.Operator);
                                tLCContSet.get(k).setModifyDate(mCurrentData);
                                tLCContSet.get(k).setModifyTime(mCurrentTime);
                                map.put(tLCContSet, "UPDATE"); 
                            }
                        } 
                    }
                }    
            }
            if("1".equals(tState)){
//              3.将LCPol中paytodate恢复为之前未豁免的状态
                for(int n = 1; n <= tLLExemptionSet.size(); n++){
                    String tPolNo = tLLExemptionSet.get(n).getPolNo();//险种号
                    LCPolDB tLCPolDB = new LCPolDB();
                    LCPolSet tLCPolSet = new LCPolSet();
                    
                    tLCPolDB.setPolNo(tPolNo);
                    tLCPolSet = tLCPolDB.query();
                    for(int j = 1;j <= tLCPolSet.size();j++){
                        String sql = "select PayToDate from LLExemption where CaseNo ='"+HMCaseNo+"' and PolNo ='"+tLCPolSet.get(j).getPolNo()+"'";
                        ExeSQL exeSQL = new ExeSQL();
                        String newPayToDate = exeSQL.getOneValue(sql);
                        System.out.println("newPayToDate:"+newPayToDate);
                        tLCPolSet.get(j).setPaytoDate(newPayToDate);
                        tLCPolSet.get(j).setOperator(mGlobalInput.Operator);
                        tLCPolSet.get(j).setModifyDate(mCurrentData);
                        tLCPolSet.get(j).setModifyTime(mCurrentTime);
                        map.put(tLCPolSet, "UPDATE"); 
                    }
                    LCDutyDB tLCDutyDB = new LCDutyDB();
                    LCDutySet tLCDutySet = new LCDutySet();
                    tLCDutyDB.setPolNo(tPolNo);
                    tLCDutySet = tLCDutyDB.query();
                    for(int j = 1;j <= tLCDutySet.size();j++){
                        String sql = "select PayToDate from LLExemption where CaseNo ='"+HMCaseNo+"' and PolNo ='"+tLCDutySet.get(j).getPolNo()+"'";
                        ExeSQL exeSQL = new ExeSQL();
                        String newPayToDate = exeSQL.getOneValue(sql);
                        System.out.println("newDutyPayToDate:"+newPayToDate);
                        tLCDutySet.get(j).setPaytoDate(newPayToDate);
                        tLCDutySet.get(j).setOperator(mGlobalInput.Operator);
                        tLCDutySet.get(j).setModifyDate(mCurrentData);
                        tLCDutySet.get(j).setModifyTime(mCurrentTime);
                        map.put(tLCDutySet, "UPDATE"); 
                    }
                    LCContDB tLCContDB = new LCContDB();
                    LCContSet tLCContSet = new LCContSet();
                    tLCContDB.setContNo(tContNo);
                    tLCContSet = tLCContDB.query();
                    for(int k=1 ;k <= tLCContSet.size(); k++){
                        String sql = "select min(PayToDate) from LLExemption where CaseNo ='"+HMCaseNo+"' and ContNo ='"+tLCContSet.get(k).getContNo()+"' and State = '1'";
                        ExeSQL exeSQL = new ExeSQL();
                        String tPayEndDate = exeSQL.getOneValue(sql);
                        System.out.println("min(PayToDate):"+tPayEndDate);
                        tLCContSet.get(k).setPaytoDate(tPayEndDate);
                        tLCContSet.get(k).setOperator(mGlobalInput.Operator);
                        tLCContSet.get(k).setModifyDate(mCurrentData);
                        tLCContSet.get(k).setModifyTime(mCurrentTime);
                        map.put(tLCContSet, "UPDATE"); 
                    }
                }
            }
        }
//      TODO:保费转出后，保费豁免表数据处理
        if(!afterDealExemption()){
            return false;
        }
        return true;
    }
    
    /**
     * 将转出的实收保费转入帐户
     * @param 
     * @return boolean
     */
    private boolean dealAcc(String tAppntNo,String tCaseNo,double tFreeAmnt)
    {
    	System.out.println("开始将转出的实收保费转入账户");
        LCAppAccDB tLCAppAccDB =new LCAppAccDB();
        tLCAppAccDB.setCustomerNo(tAppntNo);
        LCAppAccSet tLCAppAccSet = tLCAppAccDB.query();

        if(tLCAppAccSet.size()==0)
        {
            mErrors.addOneError("无投保人账户!");
            return false;
        }

        AppAcc tAppAcc = new AppAcc();

        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
        tLCAppAccTraceSchema.setCustomerNo(tAppntNo);
        tLCAppAccTraceSchema.setAccType("1");
        tLCAppAccTraceSchema.setOtherNo(tCaseNo);
        tLCAppAccTraceSchema.setBakNo(tCaseNo);
        tLCAppAccTraceSchema.setOtherType("5");
        tLCAppAccTraceSchema.setMoney(tFreeAmnt);
        tLCAppAccTraceSchema.setOperator(mGlobalInput.Operator);
        MMap tMap = tAppAcc.accShiftToSSZC(tLCAppAccTraceSchema);
        if(tMap == null)
        {
            mErrors.copyAllErrors(tAppAcc.mErrors);
            return false;
        }
        VData tVData = new VData();
        tVData.addElement(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tVData, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ICaseCureBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        System.out.println("结束转出的实收保费转入账户");
        return true;
    }
    
    private boolean afterDealExemption(){
        LLExemptionDB tLLExemptionDB = new LLExemptionDB();
        LLExemptionSet tLLExemptionSet = new LLExemptionSet();
        
        //豁免险判断，如果是申诉纠错案件换回C的案件进行判断
        String HMCaseNo = mCaseNo;
        if("R".equals(mCaseNo.substring(0,1)) || "S".equals(mCaseNo.substring(0,1)))
        {
            System.out.println("申诉纠错案件换为正常的C案件");
            String sql = "select caseno from LLAppeal where appealno ='"+mCaseNo+"'";
            ExeSQL exeSQL = new ExeSQL();
            HMCaseNo = exeSQL.getOneValue(sql);
        }
        tLLExemptionDB.setCaseNo(HMCaseNo);
        tLLExemptionSet = tLLExemptionDB.query();
        
        String tState = tLLExemptionSet.get(1).getState();
        if(tLLExemptionSet != null && tLLExemptionSet.size() > 0){
            System.out.println("豁免表状态改变");
            if("temp".equals(tState)){
                for(int i=1;i <= tLLExemptionSet.size();i++){
                    tLLExemptionSet.get(i).setState("0");
                    tLLExemptionSet.get(i).setOperator(mGlobalInput.Operator);
                    tLLExemptionSet.get(i).setModifyDate(mCurrentData);
                    tLLExemptionSet.get(i).setModifyTime(mCurrentTime);
                }
            }
            if("1".equals(tState)){
                for(int i=1;i <= tLLExemptionSet.size();i++){
                    tLLExemptionSet.get(i).setState("2");
                    tLLExemptionSet.get(i).setOperator(mGlobalInput.Operator);
                    tLLExemptionSet.get(i).setModifyDate(mCurrentData);
                    tLLExemptionSet.get(i).setModifyTime(mCurrentTime);
                    
                }
            }
            map.put(tLLExemptionSet, "UPDATE");
        }
        return true;
    }
    /**
     * 帐户处理
     * @return boolean
     */
    private boolean dealInsureAcc(){
    	 String tsql = "select distinct A.grppolno,A.polno,A.grpcontno,a.contno,a.riskcode "
             + " from llclaimdetail a,lMrisk b,lmdutyget c "
             + " where a.riskcode = b.riskcode and a.getdutycode=c.getdutycode and a.realpay != 0 "
             + " and b.insuaccflag='Y' and c.needacc='1' AND CASENO ='" + mCaseNo + "'";
    	ExeSQL texesql = new ExeSQL();
        System.out.println(tsql);
        SSRS tss = texesql.execSQL(tsql);
        if(tss==null||tss.getMaxRow()<=0){
            //没有帐户型险种产生的有效赔款
            return true;
        }
        
        for(int i=1;i<=tss.getMaxRow();i++){
        	  //判断是否为万能
            if (LLCaseCommon.chenkWN(tss.GetText(i, 5))) {
                continue;
            }            
            //遍历该赔案发生赔付的所有帐户型险种保单
            String tGrpPolNo = tss.GetText(i, 1);
            String tPolNo = tss.GetText(i, 2);
            double polpay= 0.0;

            LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
            tLLClaimDetailDB.setCaseNo(mCaseNo);
            LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
            tLLClaimDetailSet=tLLClaimDetailDB.query();
            if (tLLClaimDetailSet.size() <= 0) {
                CError.buildErr(this, "理算时发生错误，没有生成理赔明细，请回退重新理算！");
                return false;
            }


            for (int k = 1; k <= tLLClaimDetailSet.size(); k++) {
                if (tLLClaimDetailSet.get(k).getPolNo().equals(tPolNo)) {
                    LMDutyGetDB tLMDutyGetDB = new LMDutyGetDB();
                    tLMDutyGetDB.setGetDutyCode(tLLClaimDetailSet.get(k).getGetDutyCode());
                    if(!tLMDutyGetDB.getInfo())
                    {
                    	CError.buildErr(this, "给付责任查询失败！");
                        return false;
                    }
                    if ("0".equals(tLMDutyGetDB.getNeedAcc())) {
                        continue;
                    }
                    if(!tLLClaimDetailSet.get(k).getGiveTypeDesc().equals("申诉拒付"))
                    {
	                    LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB(); 
	                	tLJSGetClaimDB.setOtherNo(mCaseNo);
	                	tLJSGetClaimDB.setPolNo(tPolNo);
	                	tLJSGetClaimDB.setFeeFinaType("ZH");
	                	if(tLJSGetClaimDB.query().size()<=0)
	                	{
	                		CError.buildErr(this, "特需财务账户信息生成失败，请重新进行理算!");
	                        return false; 
	                	}
                    }
                    polpay += tLLClaimDetailSet.get(k).getRealPay();
                }
            }
            double remnant = polpay;
            double tPayMoney =0;
            tsql = "select polno,insuaccno from lcinsureacctrace where otherno = '"
                   + mCaseNo+"' and grppolno='"+tGrpPolNo
                   + "' group by polno,insuaccno order by insuaccno desc";
            SSRS tSSRS = texesql.execSQL(tsql);
            if(tSSRS==null||tSSRS.getMaxRow()<=0){
                CError.buildErr(this, "理算时发生错误，没有生成帐户轨迹，请回退重新理算！");
                return false;
            }
            for(int j=1;j<=tSSRS.getMaxRow();j++){
                LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
                tLCInsureAccDB.setPolNo(tSSRS.GetText(j,1));
                tLCInsureAccDB.setInsuAccNo(tSSRS.GetText(j,2));
                if (!tLCInsureAccDB.getInfo()) {
                    CError.buildErr(this, "查询不到轨迹对应账户信息！");
                    return false;
                }
                double tInsuAccPay = 0;
                LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
                tLCInsureAccTraceDB.setOtherNo(mCaseNo);
                tLCInsureAccTraceDB.setPolNo(tSSRS.GetText(j,1));
                tLCInsureAccTraceDB.setInsuAccNo(tSSRS.GetText(j,2));
                LCInsureAccTraceSet tLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
                if (tLCInsureAccTraceSet!=null&&tLCInsureAccTraceSet.size()>0){
                    for (int index = 1; index <= tLCInsureAccTraceSet.size();index++) {
                        tInsuAccPay += tLCInsureAccTraceSet.get(index).getMoney();
                    }
                    
                    // 优化 #3284 特需险170106超赔问题  start 
                    if ("170106".equals(tss.GetText(i, 5))) {
                    	LLAccClaimTraceSchema tLLAccClaimTraceSchema = new LLAccClaimTraceSchema();
                    	LCInsureAccTraceSchema tLCInsureAccTraceSchema = tLCInsureAccTraceSet.get(1);
                    	// 通过轨迹表查询的账户余额是否足够，不够为1，够为0
                    	String sql = "select case when "
                    		+ "(select nvl(sum(money),0) from LCInsureAccTrace where PolNo='"+tSSRS.GetText(j,1)+"' and InsuAccNo='"+tSSRS.GetText(j,2)+"' and state='0')+ "
                    		+ "(select nvl(sum(money),0) from LCInsureAccTrace where PolNo='"+tSSRS.GetText(j,1)+"' and InsuAccNo='"+tSSRS.GetText(j,2)+"' and state='temp'and moneytype='PK') "
                    		+ "<0 then 1 else 0 end from dual ";
		                String acc = texesql.getOneValue(sql);
		                // 查询账户余额时间
		                tLLAccClaimTraceSchema.setBeforeClaimTime(PubFun.getCurrentTime());
                    	if ("1".equals(acc)) {
                    		String tSql = "select distinct customername from llcase where caseno='"+mCaseNo+"'";
                    		ExeSQL tExeSql = new ExeSQL();
                    		String tCustomername = tExeSql.getOneValue(tSql);
                    		CError.buildErr(this, "被保险人姓名：" + tCustomername + "，账户余额不足，无法给付！请回退重新理算");
                            return false;
                    	}
                    	// 账户余额，不包括理赔的temp
                    	sql = "select sum(money) from LCInsureAccTrace where PolNo='"+tSSRS.GetText(j,1)+"' " +
                			"and InsuAccNo='"+tSSRS.GetText(j,2)+"' and state = '0' ";
                    	String realbala = String.valueOf(Double.parseDouble(texesql.getOneValue(sql)) + tInsuAccPay);
                    	tLLAccClaimTraceSchema.setGrpContNo(tLCInsureAccTraceSchema.getGrpContNo());
                    	tLLAccClaimTraceSchema.setGrpPolNo(tLCInsureAccTraceSchema.getGrpPolNo());
                    	tLLAccClaimTraceSchema.setContNo(tLCInsureAccTraceSchema.getContNo());
                    	tLLAccClaimTraceSchema.setPolNo(tLCInsureAccTraceSchema.getPolNo());
                    	tLLAccClaimTraceSchema.setCaseNo("OtherNo");
                    	tLLAccClaimTraceSchema.setSerialNo(PubFun.getCurrentDate2()+PubFun1.CreateMaxNo("llaccclaimtrace",12));
                    	tLLAccClaimTraceSchema.setPatchNo(realbala);// 暂时存入理赔后的正确余额
                    	tLLAccClaimTraceSchema.setExeLocation("0");
                    	tLLAccClaimTraceSchema.setInsuAccNo(tLCInsureAccTraceSchema.getInsuAccNo());
                    	tLLAccClaimTraceSchema.setRiskCode(tLCInsureAccTraceSchema.getRiskCode());
                    	tLLAccClaimTraceSchema.setOtherNo(mCaseNo);
                    	tLLAccClaimTraceSchema.setOtherType("5");
                    	tLLAccClaimTraceSchema.setMoneyType("PK");
                    	tLLAccClaimTraceSchema.setBeforeClaimMoney(tLCInsureAccDB.getSchema().getInsuAccBala());// 错误的理赔前余额
                    	tLLAccClaimTraceSchema.setAfterClaimMoney(tLCInsureAccDB.getSchema().getInsuAccBala() + tInsuAccPay);// 错误的理赔后余额
                    	tLLAccClaimTraceSchema.setOperator(mGlobalInput.Operator);
                    	tLLAccClaimTraceSchema.setMakeDate(mCurrentData);
                    	tLLAccClaimTraceSchema.setMakeTime(mCurrentTime);
                    	tLLAccClaimTraceSchema.setModifyDate(mCurrentData);
                    	tLLAccClaimTraceSchema.setModifyTime(mCurrentTime);
                    	map.put(tLLAccClaimTraceSchema, "INSERT");
                    	
                    }
                    // 优化 #3284 特需险170106超赔问题  end
                    
                    
                    //2713补充A余额可能小于0**修改  =3207修
                    String sql="select 1 from ldcode where codetype='lprisk' and code='"+tss.GetText(i, 5)+"' with ur ";
                    ExeSQL tRiskcode= new ExeSQL();
                    String code=tRiskcode.getOneValue(sql);
                    if (tLCInsureAccDB.getSchema().getInsuAccBala() + tInsuAccPay < 0&&!"1".equals(code)) {
                        CError.buildErr(this, "账户余额不足，无法给付！请回退重新理算");
                        return false;
                    }
                    //2713补充A "本次赔款金额<=个人保险金额-当日完成给付确认的理赔案件赔款金额之和"**start** 新增
                    else if ("170501".equals(tss.GetText(i, 5))){
                    	String tDeathDate= mLLCaseSchema.getDeathDate();
                    	if(tDeathDate!=null&&!"".equals(tDeathDate)){//赔身故责任的话可以赔付(身故日当天的)个人保险金额的1.05倍
                    		if(tInsuAccPay + LLCaseCommon.getAmntFromAcc(mCaseNo, tPolNo, tDeathDate, "JF")*1.05 <0){
                                CError.buildErr(this, "赔付金额大于个人保险金额，无法给付！请回退重新理算");
                                return false;	
                        	}
                    	}
                    	else if(tInsuAccPay + LLCaseCommon.getAmntFromAcc(mCaseNo, tPolNo, mCurrentData, "JF") <0){
                            CError.buildErr(this, "赔付金额大于个人保险金额，无法给付！请回退重新理算");
                            return false;	
                    	}
                    }
                  //2713补充A "本次赔款金额<=个人保险金额-当日完成给付确认的理赔案件赔款金额之和"**end** 新增
                    else if ("162401".equals(tss.GetText(i, 5))){ // 3207 管理式补充A的赔款金额
                    	String tDeathDate =mLLCaseSchema.getDeathDate();
                    	if(tDeathDate!=null&&!"".equals(tDeathDate)){//赔身故责任的话可以赔付(身故日当天的)保险金额的1.1倍
                    		if(tInsuAccPay + Arith.round(LLCaseCommon.getAmntFromAcc(mCaseNo, tPolNo, tDeathDate, "JF")*1.1,2)<0){
                    			  CError.buildErr(this, "赔付金额大于保险金额，无法给付！请回退重新理算");
                                  return false;	
                    		}   
                    	}else if(tInsuAccPay + Arith.round(LLCaseCommon.getAmntFromAcc(mCaseNo, tPolNo, mCurrentData, "JF"),2) <0){
                    		CError.buildErr(this, "赔付金额大于保险金额，无法给付！请回退重新理算");
                            return false;	
                        	}
                    }else if ("162501".equals(tss.GetText(i, 5))){ // #3337 管理式补充医疗保险(B)款 
                    	String tDeathDate=mLLCaseSchema.getDeathDate();
                    	if(tDeathDate!=null&&!"".equals(tDeathDate)){//赔身故责任的话可以赔付(身故日当天的)保险金额的1.1倍
                    		if(tInsuAccPay + Arith.round(LLCaseCommon.getAmntFromAcc(mCaseNo, tPolNo, tDeathDate, "JF")*1.05,2)<0){
                    			  CError.buildErr(this, "赔付金额大于保险金额，无法给付！请回退重新理算");
                                  return false;	
                    		}   
                    	}else{
                    		// 赔付顺序 查询 
                    		 String tClaimSQL="select a.claimnum from lcgrpfee a, lcpol b where a.grpcontno=b.grpcontno and b.polno='"+tPolNo+"' union "
                        			+ "select a.claimnum from lbgrpfee a,lbpol b where a.grpcontno=b.grpcontno and b.polno='"+tPolNo+"' union "
                        			+ "select a.claimnum from lcgrpfee a,lbpol b where a.grpcontno=b.grpcontno and b.polno='"+tPolNo+"' with ur";
                        	ExeSQL tClaimExe = new ExeSQL();
                        	String aClaimNum= tClaimExe.getOneValue(tClaimSQL);
                        	if(aClaimNum.equals("3")){ // 仅从公共账户扣除
                        		String aPolNo=getPoolPolNo(tPolNo); //获取公共保单险种号
                        		if(tInsuAccPay + Arith.round(LLCaseCommon.getPooledAccount(mCaseNo, aPolNo, mCurrentData, "JF"), 2)<0){
                        			 CError.buildErr(this, "赔付金额大于保险金额，无法给付！请回退重新理算");
                                     return false;	
                        		}
                        	}else if(aClaimNum.equals("2")){ //先从个人后公共扣除
                        		String aPolNo=getPoolPolNo(tPolNo);
                        		double aPoolAmnt =LLCaseCommon.getPooledAccount(mCaseNo, aPolNo, mCurrentData, "JF");
                        		double aPersonAmnt = LLCaseCommon.getAmntFromAcc(mCaseNo, tPolNo, mCurrentData, "JF");
                        		if(tInsuAccPay + Arith.round(aPoolAmnt + aPersonAmnt , 2)<0){
                        			 CError.buildErr(this, "赔付金额大于保险金额，无法给付！请回退重新理算");
                                     return false;
                        		}
                        		
                        	}else{// 仅从个人账户扣除
                        		if(tInsuAccPay + Arith.round(LLCaseCommon.getAmntFromAcc(mCaseNo, tPolNo, mCurrentData, "JF"),2) <0){
                            		CError.buildErr(this, "赔付金额大于保险金额，无法给付！请回退重新理算");
                                    return false;	
                        	}
                        	
                    	}
                      }
                    }
                    
                    
                if (tInsuAccPay!=0){
                    tPayMoney += tInsuAccPay;
                    LCInsureAccTraceSchema tLCInsureAccTraceSchema =
                            tLCInsureAccTraceSet.get(1);
                    tLCInsureAccTraceSchema.setMoney(tInsuAccPay);
                    tLCInsureAccTraceSchema.setState("0");
                    tLCInsureAccTraceSchema.setModifyDate(PubFun.
                            getCurrentDate());
                    tLCInsureAccTraceSchema.setModifyTime(PubFun.
                            getCurrentTime());
                    tLCInsureAccTraceSchema.setOperator(mGlobalInput.
                            Operator);
                    //2713补充A 赔医疗保险金PAYDATE为通知给付时间(mCurrentData),赔身故责任PAYDATE为身故日期**START修改
                    if("1".equals(code)){ // 3207 修
                    	tLCInsureAccTraceSchema.setPayDate(mCurrentData);
                	    String tsgSQL = "select 1 from llclaimdetail where caseno='"+mCaseNo+"' and getdutykind in('500','501') with ur";
			            ExeSQL tExeSQL = new ExeSQL();
			            String shengu = tExeSQL.getOneValue(tsgSQL);
			            if ("1".equals(shengu)){
			            	//赔了身故责任701204的话,paydate赋值为身故日期
			            	String tdeathSQL = "select DeathDate from llcase where caseno='"+mCaseNo+"' with ur";
				            ExeSQL tdExeSQL = new ExeSQL();
				            String deathdate= tdExeSQL.getOneValue(tdeathSQL);//身故日期
				            if (deathdate!=null&&!"".equals(deathdate)){
				            	tLCInsureAccTraceSchema.setPayDate(deathdate);
				            }
			            }
                    }
                    //2713补充A 赔医疗保险金PAYDATE为通知给付时间(mCurrentData),赔身故责任PAYDATE为身故日期**END
                    map.put(tLCInsureAccTraceSchema, "INSERT");
                    LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccDB.
                            getSchema();
                    tLCInsureAccSchema.setSumPaym(tLCInsureAccSchema.
                                                  getSumPaym() - tInsuAccPay);
                    tLCInsureAccSchema.setInsuAccBala(tLCInsureAccSchema.getInsuAccBala()+tInsuAccPay);
                    tLCInsureAccSchema.setOperator(mGlobalInput.Operator);
                    tLCInsureAccSchema.setModifyDate(mCurrentData);
                    tLCInsureAccSchema.setModifyTime(mCurrentTime);
                    map.put(tLCInsureAccSchema, "UPDATE");
                }
            }
        }
        if (Arith.round(tPayMoney,2)+Arith.round(remnant,2)!=0){
            CError.buildErr(this,"帐户轨迹与理算结果不符，请回退重新理算");
            return false;
        }

//            tsql = "select * from lcinsureacctrace where otherno = '"
//                          +mCaseNo+"' and grppolno='"+tGrpPolNo
//                          +"' order by insuaccno desc ";
//            LCInsureAccTraceDB tAccTraceDB = new LCInsureAccTraceDB();
//            LCInsureAccTraceSet tAccTraceSet = new LCInsureAccTraceSet();
//            tAccTraceSet = tAccTraceDB.executeQuery(tsql);
//            if (tAccTraceSet.size() <= 0) {
//                CError.buildErr(this, "理算时发生错误，没有生成帐户轨迹，请回退重新理算！");
//                return false;
//            }
//            String tinsuaccno = "";
//            for (int j = 1; j <= tAccTraceSet.size(); j++) {
//                //理算时可能会产生多条轨迹（发生错误），但此处可以挽回，一个案件一个账户只需存一条轨迹即可。
//                LCInsureAccTraceSchema tAccTraceSchema = new
//                        LCInsureAccTraceSchema();
//                tAccTraceSchema = tAccTraceSet.get(j);
//                if(tAccTraceSchema.getInsuAccNo().equals(tinsuaccno))
//                    continue;
//                else
//                    tinsuaccno = tAccTraceSchema.getInsuAccNo();
//                LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
//                tLCInsureAccDB.setPolNo(tAccTraceSchema.getPolNo());
//                tLCInsureAccDB.setInsuAccNo(tinsuaccno);
//                if (!tLCInsureAccDB.getInfo()) {
//                    CError.buildErr(this, "查询不到轨迹对应账户信息！");
//                    return false;
//                }
//                double caccbala = tLCInsureAccDB.getInsuAccBala();
//                if(getAccType(tinsuaccno).equals("002"))
//                    ibala = caccbala;
//                else
//                    gbala = caccbala;
//                double tracepay = 0;
//                if (caccbala > remnant) {
//                    tracepay = ( -1) * remnant;
//                } else {
//                    tracepay = ( -1) * caccbala;
//                }
//                caccbala += tracepay;
//                remnant += tracepay;
//                double sumpay = tLCInsureAccDB.getSumPaym() - tracepay;
//                tAccTraceSchema.setMoney(tracepay);
//                tAccTraceSchema.setState("0");
//                tAccTraceSchema.setModifyDate(mCurrentData);
//                tAccTraceSchema.setModifyTime(mCurrentTime);
//                map.put(tAccTraceSchema, "INSERT");
//                LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccDB.getSchema();
//                tLCInsureAccSchema.setSumPaym(sumpay);
//                tLCInsureAccSchema.setInsuAccBala(caccbala);
//                tLCInsureAccSchema.setOperator(mGlobalInput.Operator);
//                tLCInsureAccSchema.setModifyDate(mCurrentData);
//                tLCInsureAccSchema.setModifyTime(mCurrentTime);
//                map.put(tLCInsureAccSchema, "UPDATE");
//            }
//            if (remnant > 0) {
//                String tgrpcontno = tss.GetText(i,3);
//                String tcontno = tgrpcontno.equals("00000000000000000000")?tss.GetText(i,4):tgrpcontno;
//                CError.buildErr(this,"账户余额不足，无法给付！目前保单"+tcontno
//                                +"个人账户余额"+ibala+"元，团体账户余额"+gbala
//                                +"元，本次需赔付"+polpay+"元。<br>");
//                return false;
//            }
        }
    return true;
}

//private String getAccType(String tInsuAccNo) {
//    String tType = "";
//    LMRiskInsuAccDB triskaccdb = new LMRiskInsuAccDB();
//    triskaccdb.setInsuAccNo(tInsuAccNo);
//    if (triskaccdb.getInfo()) {
//        tType = triskaccdb.getAccType();
//    }
//    return tType;
//}
    /**
     * 处理申诉类案件的财务数据
     * 1. 生成原案件的负值冲账
     * 2. 团体统一给付，需要生成整个批次新的财务数据。
     * 3. 该流程不考虑重复操作的可能性
     * @return boolean
     */
    private boolean dealAppeal(){
        //查询原来的案件号
        String tCaseNo = "";
        LJAGetClaimSet aLJAGetClaimSet = new LJAGetClaimSet();
        if (mCaseNo.substring(0,1).equals("S")||mCaseNo.substring(0,1).equals("R")){
            String tAppealSQL = "SELECT caseno FROM llappeal where appealno='"+mCaseNo
                         +"' UNION "
                         + "SELECT b.appealno FROM llappeal a,llappeal b "
                         + "WHERE a.caseno=b.caseno AND a.appealno='"
                         + mCaseNo +"' AND b.appealno<>'"+mCaseNo+"'";
          ExeSQL tExeSQL = new ExeSQL();
          SSRS tAppealSSRS = tExeSQL.execSQL(tAppealSQL);
          if (tAppealSSRS.getMaxRow()<=0){
              CError.buildErr(this, "申诉案件查询失败！");
              return false;
          }

          //查询原案件的LJAGetClaim
          for (int i = 1; i<=tAppealSSRS.getMaxRow();i++){
              LJAGetClaimDB tLJAGetClaimDB = new LJAGetClaimDB();
              tCaseNo = tAppealSSRS.GetText(i,1);
              tLJAGetClaimDB.setOtherNo(tCaseNo);
              tLJAGetClaimDB.setOtherNoType("5");
              aLJAGetClaimSet.add(tLJAGetClaimDB.query());
          }
        }
        if (aLJAGetClaimSet.size() <= 0) {
            System.out.println("该申诉错误案件原赔付结论为拒付，没有生成财务数据");
            return false;
        }
        double oldSummoney = 0.0;
        for(int i=1;i<=aLJAGetClaimSet.size();i++){
            LJAGetClaimSchema oldLJAGetClaimSchema = aLJAGetClaimSet.get(i);
            String oldFeeFinaType = oldLJAGetClaimSchema.getFeeFinaType();
            String oldGetDutyKind = oldLJAGetClaimSchema.getFeeOperationType();
            String oldPolNo = oldLJAGetClaimSchema.getPolNo();
//            if(oldLJAGetClaimSchema.getPay()<0)
//                continue;
            oldSummoney += oldLJAGetClaimSchema.getPay();
            boolean is = true;

            for(int j=1;j<=mLJAGetClaimSet.size();j++){
               LJAGetClaimSchema newLJAGetClaimSchema = mLJAGetClaimSet.get(j);
               String newFeeFinaType = newLJAGetClaimSchema.getFeeFinaType();
               String newGetDutyKind = newLJAGetClaimSchema.getFeeOperationType();
               String newPolNo = newLJAGetClaimSchema.getPolNo();
               //财务类型、给付责任类型、险种号均相等，求出实际赔付金额
               if(oldFeeFinaType.equals(newFeeFinaType)&&
                  oldGetDutyKind.equals(newGetDutyKind)&&
                  oldPolNo.equals(newPolNo)){
                   double newrealpay = newLJAGetClaimSchema.getPay()-oldLJAGetClaimSchema.getPay();
                   newLJAGetClaimSchema.setPay(newrealpay);
                   is = false;
                   break;
               }
            }
            if(is){//申诉案件选择了新的险种、给付责任等
                oldLJAGetClaimSchema.setActuGetNo(saveLJAGetSchema.getActuGetNo());
                oldLJAGetClaimSchema.setOtherNo(mCaseNo);
                oldLJAGetClaimSchema.setMakeDate(mCurrentData);
                oldLJAGetClaimSchema.setMakeTime(mCurrentTime);
                oldLJAGetClaimSchema.setModifyDate(mCurrentData);
                oldLJAGetClaimSchema.setModifyTime(mCurrentTime);
                oldLJAGetClaimSchema.setConfDate("");
                oldLJAGetClaimSchema.setEnterAccDate("");
                oldLJAGetClaimSchema.setOPConfirmDate(mCurrentData);
                oldLJAGetClaimSchema.setOPConfirmTime(mCurrentTime);
                double tnewrealpay = (-1)*oldLJAGetClaimSchema.getPay();
                oldLJAGetClaimSchema.setPay(tnewrealpay);
                mLJAGetClaimSet.add(oldLJAGetClaimSchema);
            }
        }
        double newSummoney = saveLJAGetSchema.getSumGetMoney()-oldSummoney;
        newSummoney = Arith.round(newSummoney,2);
        saveLJAGetSchema.setSumGetMoney(newSummoney);
        if(newSummoney<0){
            saveLJAGetSchema.setPayMode("1");
            saveLJAGetSchema.setBankAccNo("");
            saveLJAGetSchema.setAccName("");
            saveLJAGetSchema.setBankCode("");
            mLLCaseSchema.setCaseGetMode("1");
            mLLCaseSchema.setBankAccNo("");
            mLLCaseSchema.setAccName("");
            mLLCaseSchema.setBankCode("");
            map.put(mLLCaseSchema, "UPDATE");
        }
        return true;
    }

public VData getResult() {
    return this.mResult;
}

/**
 * 校验财务合计提交金额与业务合计金额是否相等
 * */
//private boolean checkMoney()
//{
//	double pay = 0 ;
//	for (int k = 1; k <= saveLJAGetSet.size() ;k++)
//	{
//		LJAGetSchema tLJAGetSchema = saveLJAGetSet.get(k);
//		pay += tLJAGetSchema.getSumGetMoney();
//	}
//	 String tclaimSQL ="select nvl(sum(realpay),0) from llclaim"
// 		+" where caseno='"+ mCaseNo +"' with ur" ;
//	 ExeSQL tclaimExeSQL = new ExeSQL();
//	 SSRS tclaimSSRS = tclaimExeSQL.execSQL(tclaimSQL);
//    if(tclaimSSRS.getMaxRow()<= 0)
//    {
//    	 CError.buildErr(this, "理赔业务生成数据失败！");
//         return false;
//    }else{
//    	  if( pay - Double.parseDouble(tclaimSSRS.GetText(1, 1))> 0 )
//    	  {
//    		  CError.buildErr(this, "财务实收金额合计与业务合计金额不等！");
//              return false;
//    	  }
//    }
//  
//	return true;
//}

private boolean checkInputData(){
	 LLCaseDB tLLCaseDB = new LLCaseDB();
	 tLLCaseDB.setCaseNo(mCaseNo);
	 if (!tLLCaseDB.getInfo()) {
		 CError.buildErr(this, "理赔案件信息查询失败！");
		 return false;
	 }else{
		 if(!"09".equals(tLLCaseDB.getRgtState())){
			 CError.buildErr(this, "该案件状态下不允许此操作！");
			 return false;
		 }
     LLHospCaseDB tLLHospCaseDB = new LLHospCaseDB();
     tLLHospCaseDB.setCaseNo(mCaseNo);
     	if(tLLHospCaseDB.getInfo()) {
     		return true;
     	}
	 }
	 
	 	//判断是否为社保的批次,若为社保批次，不允许社保人员操作
	 	//add by Houyd 同时需要考虑申诉类案件
     	String tCaseNo = "";
     	if("R".equals(mCaseNo.substring(0,1)) || "S".equals(mCaseNo.substring(0,1)))
     	{
     		System.out.println("申诉纠错案件换为正常的C案件");
     		String sql = "select caseno from LLAppeal where appealno ='"+mCaseNo+"'";
     		ExeSQL exeSQL = new ExeSQL();
     		tCaseNo = exeSQL.getOneValue(sql);
     	}else{
     		tCaseNo = mCaseNo;
     	}
	    if("1".equals(LLCaseCommon.checkSocialSecurity(tCaseNo, null))){
	      LLSocialClaimUserDB tLLSocialClaimUserDB = new LLSocialClaimUserDB();
	      tLLSocialClaimUserDB.setUserCode(mGlobalInput.Operator.trim());
	      tLLSocialClaimUserDB.setStateFlag("1");//社保用户有效
	      tLLSocialClaimUserDB.setHandleFlag("1");//参与案件分配
	      LLSocialClaimUserSet tLLSocialClaimUserSet = tLLSocialClaimUserDB.query();
	      
	      if (tLLSocialClaimUserSet.size() > 0) {
	        mErrors.addOneError("参与案件分配的社保人员不能进行给付确认操作");
	        return false;
	      }
	      LLSocialClaimUserDB ttLLSocialClaimUserDB = new LLSocialClaimUserDB();
	      ttLLSocialClaimUserDB.setUserCode(mGlobalInput.Operator.trim());
	      if(!ttLLSocialClaimUserDB.getInfo()){
	        mErrors.addOneError("非社保配置用户不能进行给付确认操作");
	        return false;
	      }	      
	    //add lyc #2113 2014-08-29
	    }else{
	    	String sql1 = "select rigister from llcase where caseno ='"+mCaseNo+"'";
	    	ExeSQL exeSQL = new ExeSQL();
	    	String tRigister = exeSQL.getOneValue(sql1);
	    	if (mGlobalInput.Operator.trim().equals(tRigister)) {
	    		mErrors.addOneError("案件受理人不能进行给付确认操作");
	    		return false;
	    	}
	    //add lyc
	    }
   
   try {
       if (!mCaseNo.substring(0, 1).equals("S") && !mCaseNo.substring(0, 1).equals("R")) 
       {
	        String tDetailSQL ="select 1 from"
	        		+" (select sum(realpay) real,caseno caseno from llclaimdetail where caseno ='"+ mCaseNo +"' group by caseno) a, "
	        		+" (select sum(pay) pay,otherno otherno from ljsgetclaim where otherno ='"+ mCaseNo +"' and othernotype<>'H' group by otherno) b "  //2366 分红险处理
	        		+" where a.caseno=b.otherno and a.caseno='"+ mCaseNo +"' and a.real<>b.pay"
	        		+" with ur" ;
	        ExeSQL tDetailExeSQL = new ExeSQL();
	        System.out.println("tDetailSQL====="+tDetailSQL);
	        SSRS tDetailSSRS = tDetailExeSQL.execSQL(tDetailSQL);
	        if(tDetailSSRS.getMaxRow()>=1)
	        {
	        	 CError.buildErr(this, "理赔业务明细信息和财务明细信息合计金额不等！");
	             return false;
	        }	        	      
       }
       String tClaimSQL ="select 1 from"
   		+" (select sum(realpay) real,caseno caseno from llclaimdetail where caseno ='"+ mCaseNo +"' group by caseno) a, "
   		+" (select sum(realpay) real,caseno caseno from llclaim where caseno ='"+ mCaseNo +"' group by caseno) b "
   		+" where a.caseno=b.caseno and a.caseno='"+ mCaseNo +"' and a.real<>b.real"
   		+" with ur" ;
	    ExeSQL tClaimExeSQL = new ExeSQL();
	    System.out.println("tClaimSQL====="+tClaimSQL);
	    SSRS tClaimSSRS = tClaimExeSQL.execSQL(tClaimSQL);
	    if(tClaimSSRS.getMaxRow()>=1)
	    {
	    	 CError.buildErr(this, "理赔业务明细信息和业务汇总合计金额不等！");
	         return false;
	    }
	    
	    String tljsclaimSQL ="select 1 from"
   		+" (select sum(sumgetmoney) real,otherno otherno from ljsget where otherno ='"+ mCaseNo +"' group by otherno) a, "
   		+" (select sum(pay) real,otherno otherno from ljsgetclaim where otherno ='"+ mCaseNo +"' group by otherno) b "
   		+" where a.otherno=b.otherno and a.otherno='"+ mCaseNo +"' and a.real<>b.real"
   		+" with ur" ;
	    ExeSQL tljsclaimExeSQL = new ExeSQL();
	    System.out.println("tljsclaimSQL====="+tljsclaimSQL);
	    SSRS tljsclaimSSRS = tljsclaimExeSQL.execSQL(tljsclaimSQL);
	    if(tljsclaimSSRS.getMaxRow()>=1)
	    {
	    	 CError.buildErr(this, "财务明细信息和财务汇总合计金额不等！");
	         return false;
	    }
   }
   catch (Exception ex) {
   	 // @@错误处理
   	CError.buildErr(this, "校验理赔金额出错！");
       return false;
   }
   return true;
}
 public static void main(String[] args) {

   LJAGetInsertBL tLJAGetInsertBL = new LJAGetInsertBL();
   LLCaseSchema tLLCaseSchema = new LLCaseSchema();
   tLLCaseSchema.setCaseNo("C1100070114000110");
   VData tVData = new VData();
   GlobalInput tGlobalInput = new GlobalInput();
   tGlobalInput.ManageCom = "86";
   tGlobalInput.Operator = "001";
   tVData.addElement(tGlobalInput);
   LJAGetSchema tLJAGetSchema = new LJAGetSchema();
   tLJAGetSchema.setOtherNo("C1100070114000110");
   tLJAGetSchema.setOtherNoType("5");
   tLJAGetSchema.setAccName("");
   tLJAGetSchema.setBankAccNo("");
   tLJAGetSchema.setPayMode("1");
   tLJAGetSchema.setBankCode("");
   tLJAGetSchema.setDrawer("");
   tLJAGetSchema.setDrawerID("");
   tVData.addElement(tLJAGetSchema);
   tVData.addElement(tLLCaseSchema);
   tLJAGetInsertBL.submitData(tVData, "INSERT");

 }
 
 /**
     * 锁定案件给付
     * @param LockNoKey
     * @param tGlobalInput
     * @return
     */
    private MMap lockTable(String lockNoKey, GlobalInput globalInput)
    {
        MMap map = null;

        //案件给付类型：“LJ”
        String tLockNoType = "LJ";

        //锁定有效时间
        String tAIS = "60";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", lockNoKey);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(globalInput);
        tVData.add(tTransferData);

        LLLockTableActionBL tLockTableActionBL = new LLLockTableActionBL();
        map = tLockTableActionBL.getSubmitMap(tVData, null);
        if(map == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }

        return map;
    }
    
    private boolean dealWNAcc() { 
    	//只处理申请类案件
        if ("1".equals(mLLCaseSchema.getRgtType())) {
            String tContNo = "";
            String tRiskCode = "";
            LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();

            //判断是否有万能需要处理账户的险种赔付且赔付大于0
            //这里认为所有万能附加险都会赔付账户价值
            String tWNSQL = "select * from llclaimdetail where caseno='" + mCaseNo + "'";
            LLClaimDetailSet tLLClaimDetailSet = tLLClaimDetailDB.executeQuery(tWNSQL);
            double tPayMoney  =0;
            for (int i = 1; i <= tLLClaimDetailSet.size(); i++) 
            {
            	LLClaimDetailSchema tLLClaimDetailSchema =new LLClaimDetailSchema();
            	tLLClaimDetailSchema = tLLClaimDetailSet.get(i);
                tPayMoney += tLLClaimDetailSchema.getRealPay();
                tRiskCode = tLLClaimDetailSchema.getRiskCode();
                tContNo = tLLClaimDetailSchema.getContNo();
                
//              福泽一生重疾反冲
                String tPolNo = tLLClaimDetailSchema.getPolNo();
                String tGetDutyCode = tLLClaimDetailSchema.getGetDutyCode();
                String tFZSQL = "select ElementValue from LLElementDetail where caseno='" + mCaseNo + "' "
        		+" and polno='"+tPolNo+"' and getdutycode='"+tGetDutyCode+"'"
        		+" and elementcode='SeriousDisease'";
                
                ExeSQL tFZExeSQL = new ExeSQL();
                String tFZ = tFZExeSQL.getOneValue(tFZSQL); 
                
                //福泽180天之外重疾不赔付账户，跳过账户处理
                String tFZAccRDSQL = "select 1 from LLElementDetail where caseno='" + mCaseNo + "' "
        		+" and polno='"+tPolNo+"' and getdutycode='"+tGetDutyCode+"'"
        		+" and elementcode='RgtDays' and bigint(ElementValue)>180";
                
                ExeSQL tFZAccRDExeSQL = new ExeSQL();
                String tFZAccRD = tFZAccRDExeSQL.getOneValue(tFZAccRDSQL); 
                
//       		 #2668
       		 String SQL = "select 1 from lmriskapp where taxoptimal='Y' and  riskcode ='"+tRiskCode+"'";
       	        ExeSQL mExeSQL = new ExeSQL();
       	        String riskcode = mExeSQL.getOneValue(SQL);
                
                if(tFZAccRD.equals("1") && tRiskCode.equals("231201"))
                {
                	System.out.println("福泽180天之外重疾不赔付账户，跳过账户处理");
                }
                else
                {
                if ((LLCaseCommon.chenkWN(tRiskCode) && tPayMoney > 0 && !riskcode.equals("1")) || (tRiskCode.equals("231201") && tPayMoney == 0 && tFZ.equals("1")) ) 
                {       
                	LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB(); 
                	tLJSGetClaimDB.setOtherNo(mCaseNo);
                	tLJSGetClaimDB.setOtherNoType("5");
                	tLJSGetClaimDB.setFeeFinaType("ZH");
                	if(tLJSGetClaimDB.query().size()<=0)
                	{
                		CError.buildErr(this, "万能财务账户信息生成失败，请重新进行理算!");
                        return false; 
                	}                	 
//                	if(!updateLCContState(tLLClaimDetailSchema))
//                	{
//                		CError.buildErr(this, "万能保单状态更新失败!");
//                        return false; 
//                	}                                     
         
                    //查询万能账户
                    LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
                    String tAccSQL = "select * from lcinsureacc where contno='" + tContNo + "'";
                    LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.executeQuery(tAccSQL);

                    double tInsuAccBala = 0; //初始账户余额
                    double tAccMoney = 0;	//本次赔款
                    double tFCMoney = 0;	//反冲结算金额
                    if (tLCInsureAccSet.size() > 0) 
                    {
                    	//针对保单多万能险修改
                    	for (int tTr = 1; tTr<= tLCInsureAccSet.size() ; tTr++)
                    	{
                    		//万能只有一个账户 
                            LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccSet.get(tTr);
                            String tTrPolNo =tLCInsureAccSchema.getPolNo();
                            String tTrInsuAccNo = tLCInsureAccSchema.getInsuAccNo();
                            
                          //福泽附加重疾180天出险，扣除风险保费即可
                            if (tRiskCode.equals("231201") && tPayMoney == 0 && tFZ.equals("1"))
                            {
                            	System.out.println("福泽附加重疾180天出险，扣除风险保费即可");
                            }
                            else{
                            String TraceAccSql="select sum(money) from lcinsureacctrace where polno='"+ tTrPolNo +"'"
                    			+" and insuaccno='"+ tTrInsuAccNo +"'";
    		                ExeSQL tAccExeSQL = new ExeSQL();
    		                String TraceAccMoney = tAccExeSQL.getOneValue(TraceAccSql);
    		                System.out.println("账户轨迹合计金额TraceAccMoney=="+TraceAccMoney);
    		                if(!TraceAccMoney.equals("0.00"))
    		                {
    		                	CError.buildErr(this, "账户轨迹合计金额不等于0！");
    		                    return false;
    		                }  
                            }
    		                
                            tInsuAccBala = Arith.round(tLCInsureAccSchema.getInsuAccBala(),2);
                            String tTraceSQL ="select coalesce(sum(money),0) from lcinsureacctrace "
                                    + " where polno='" + tTrPolNo +
                                    "' and insuaccno='" + tTrInsuAccNo +
                                    "' and otherno='" + mCaseNo +
                                    "' and state='temp' and moneytype='PK'";
                            ExeSQL tExeSQL = new ExeSQL();
                            tAccMoney = Arith.round(Double.parseDouble(tExeSQL.getOneValue(tTraceSQL)), 2);
                            System.out.println("账户余额tInsuAccBala=="+tInsuAccBala);
                            System.out.println("账户轨迹本次需要赔付金额=="+tAccMoney);
                            if(tInsuAccBala < tAccMoney)
                            {
                            	CError.buildErr(this, "账户余额无法支付本次赔款！");
                                return false;
                            }
                           
                            //修改临时状态账户轨迹LCInsureAccTrace                          
                            LCInsureAccTraceDB aLCInsureAccTraceDB = new LCInsureAccTraceDB();
                            String aAccTraceSQL = "select * from lcinsureacctrace "
                                    + " where polno='" + tTrPolNo +
                                    "' and insuaccno='" + tTrInsuAccNo +
                                    "' and state='temp' and othertype='5'";
                            System.out.println("修改临时状态账户轨迹查询aAccTraceSQL==="+ aAccTraceSQL);
                            LCInsureAccTraceSet aLCInsureAccTraceSet = aLCInsureAccTraceDB.executeQuery(aAccTraceSQL);
                            if(aLCInsureAccTraceSet.size()>=1)
                            {
                            	  for (int m = 1; m <= aLCInsureAccTraceSet.size(); m++) 
                                  {
                                  	 LCInsureAccTraceSchema aLCInsureAccTraceSchema = aLCInsureAccTraceSet.get(m);
                                       aLCInsureAccTraceSchema.setState("0");                                                                          
                                       aLCInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
                                       aLCInsureAccTraceSchema.setPayDate(mCurrentData);
                                       aLCInsureAccTraceSchema.setModifyDate(mCurrentData);
                                       aLCInsureAccTraceSchema.setModifyTime(mCurrentTime);                                 
                                       map.put(aLCInsureAccTraceSchema, "DELETE&INSERT");
                                  }                            
                            }
                            
                            //福泽附加重疾180天出险，扣除风险保费即可
                            if (tRiskCode.equals("231201") && tPayMoney == 0 && tFZ.equals("1"))
                            {
                            	String aAccTraceFZSQL = "select sum(money) from lcinsureacctrace "
                                    + " where polno='" + tTrPolNo +
                                    "' and insuaccno='" + tTrInsuAccNo +
                                    "' and state='temp' and othertype='5' and payplancode = '231201'";
                            	
                            	ExeSQL tAccFZExeSQL = new ExeSQL();
                                String tAccTraceFZ = tAccFZExeSQL.getOneValue(aAccTraceFZSQL); 
                                double iAccTraceFZ=Double.parseDouble(tAccTraceFZ); 
                                
                                tLCInsureAccSchema.setInsuAccBala(Arith.round(tLCInsureAccSchema.getInsuAccBala() + iAccTraceFZ,2));
                                tLCInsureAccSchema.setSumPay(Arith.round(tLCInsureAccSchema.getSumPay() + iAccTraceFZ,2));                        
                                tLCInsureAccSchema.setOperator(mGlobalInput.Operator);
                                tLCInsureAccSchema.setBalaDate(mCurrentData);
                                tLCInsureAccSchema.setModifyDate(mCurrentData);
                                tLCInsureAccSchema.setModifyTime(mCurrentTime);
                                map.put(tLCInsureAccSchema, "DELETE&INSERT");
                            }
                            else{
                            //修改账户LCInsureAcc
                            tLCInsureAccSchema.setInsuAccBala(0);
                            tLCInsureAccSchema.setSumPaym(Arith.round(tLCInsureAccSchema.getSumPaym() - tAccMoney,2));                        
                            tLCInsureAccSchema.setOperator(mGlobalInput.Operator);
                            tLCInsureAccSchema.setBalaDate(mCurrentData);
                            tLCInsureAccSchema.setModifyDate(mCurrentData);
                            tLCInsureAccSchema.setModifyTime(mCurrentTime);
                            map.put(tLCInsureAccSchema, "DELETE&INSERT");
                            }
                            //修改补充上个月和几天的月结信息                                  
                            String aBalSQL = "select * from lcinsureacctrace "
                                    + " where polno ='" + tTrPolNo +
                                    "' and insuaccno='" + tTrInsuAccNo +"'"
                                    +" and state='temp' and othertype='6'";                     
                            LCInsureAccTraceSet aBalLCInsureAccTraceSet = aLCInsureAccTraceDB.executeQuery(aBalSQL);
                            if(aBalLCInsureAccTraceSet.size()>=1)
                            {
                            	for(int aBal =1 ; aBal<= aBalLCInsureAccTraceSet.size(); aBal++)
                            	{
                            		LCInsureAccTraceSchema aBalLCInsureAccTraceSchema = aBalLCInsureAccTraceSet.get(aBal);
                            		aBalLCInsureAccTraceSchema.setState("0");   
                            		aBalLCInsureAccTraceSchema.setOtherType("5");                        		
                            		aBalLCInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
                            		aBalLCInsureAccTraceSchema.setPayDate(mCurrentData);
                            		aBalLCInsureAccTraceSchema.setModifyDate(mCurrentData);
                            		aBalLCInsureAccTraceSchema.setModifyTime(mCurrentTime);                                 
                                    map.put(aBalLCInsureAccTraceSchema, "DELETE&INSERT");                                                       
                            	}                        	
                            } 
                            LCInsureAccBalanceDB tLCInsureAccBalanceDB =new LCInsureAccBalanceDB();                         
                            tLCInsureAccBalanceDB.setPolNo(tTrPolNo);
                            tLCInsureAccBalanceDB.setInsuAccNo("000000");
                            LCInsureAccBalanceSet tLCInsureAccBalanceSet = tLCInsureAccBalanceDB.query();
                            if(tLCInsureAccBalanceSet.size()>=1)
                            {
                            	 for(int bal= 1; bal<=tLCInsureAccBalanceSet.size(); bal++)
                                 {
                                 	LCInsureAccBalanceSchema tLCInsureAccBalanceSchema = tLCInsureAccBalanceSet.get(bal);
                                 	tLCInsureAccBalanceSchema.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo());
                                 	tLCInsureAccBalanceSchema.setDueBalaDate(mCurrentData);
                                 	tLCInsureAccBalanceSchema.setRunDate(mCurrentData);
                                 	tLCInsureAccBalanceSchema.setOperator(mGlobalInput.Operator);
                                 	tLCInsureAccBalanceSchema.setModifyDate(mCurrentData);
                                 	tLCInsureAccBalanceSchema.setModifyTime(mCurrentTime);
                                 	map.put(tLCInsureAccBalanceSchema, "DELETE&INSERT");
                                 }
                            }        
                           
                            //反冲账户管理费结算信息
                            LCInsureAccFeeTraceDB aLCInsureAccFeeTraceDB = new LCInsureAccFeeTraceDB();
                            LCInsureAccFeeTraceSet aLCInsureAccFeeTraceSet = new LCInsureAccFeeTraceSet();
                            LCInsureAccFeeTraceSchema aLCInsureAccFeeTraceSchema= new LCInsureAccFeeTraceSchema();
                            
                            aLCInsureAccFeeTraceDB.setPolNo(tTrPolNo);
                            aLCInsureAccFeeTraceDB.setState("temp");
                            aLCInsureAccFeeTraceSet = aLCInsureAccFeeTraceDB.query();
                            if(aLCInsureAccFeeTraceSet.size()>=1)
                            {
                            	 for (int m = 1; m <= aLCInsureAccFeeTraceSet.size(); m++) 
                            	 {
                            		 aLCInsureAccFeeTraceSchema = aLCInsureAccFeeTraceSet.get(m).getSchema();    
                            		  tFCMoney += aLCInsureAccFeeTraceSchema.getFee();
                                     aLCInsureAccFeeTraceSchema.setState("0");
                                     aLCInsureAccFeeTraceSchema.setOtherType("5");
                                     aLCInsureAccFeeTraceSchema.setPayDate(mCurrentData);
                                     aLCInsureAccFeeTraceSchema.setModifyDate(mCurrentData);
                                     aLCInsureAccFeeTraceSchema.setModifyTime(mCurrentTime);                                 
                                     aLCInsureAccFeeTraceSchema.setOperator(mGlobalInput.Operator);
                                     map.put(aLCInsureAccFeeTraceSchema, "DELETE&INSERT");
                                     	 
                            	 }
                            	
                            }
                            LCInsureAccFeeDB aLCInsureAccFeeDB = new LCInsureAccFeeDB();
                            LCInsureAccFeeSchema aLCInsureAccFeeSchema = new LCInsureAccFeeSchema();
                            aLCInsureAccFeeDB.setPolNo(tTrPolNo);
                            if(aLCInsureAccFeeDB.query().size()<=0 )
                            {          
                            	CError.buildErr(this, "账户管理费查询失败！");
                                return false;
                            }   
                            aLCInsureAccFeeSchema = aLCInsureAccFeeDB.query().get(1).getSchema();	
                        	aLCInsureAccFeeSchema.setFee(Arith.round(aLCInsureAccFeeSchema.getFee()+tFCMoney, 2));
                        	aLCInsureAccFeeSchema.setBalaDate(mCurrentData);
                        	aLCInsureAccFeeSchema.setModifyDate(mCurrentData);
                        	aLCInsureAccFeeSchema.setModifyTime(mCurrentTime);                        	
                        	aLCInsureAccFeeSchema.setOperator(mGlobalInput.Operator);
                        	map.put(aLCInsureAccFeeSchema, "DELETE&INSERT");
                        	
                            LCInsureAccClassDB aLCInsureAccClassDB = new LCInsureAccClassDB();
                            LCInsureAccClassSchema aLCInsureAccClassSchema = new LCInsureAccClassSchema();
                            aLCInsureAccClassDB.setPolNo(tTrPolNo);
                            if(aLCInsureAccClassDB.query().size()>=1)
                            {
                            	aLCInsureAccClassSchema = aLCInsureAccClassDB.query().get(1).getSchema();	
                            	aLCInsureAccClassSchema.setInsuAccBala(0);
                            	aLCInsureAccClassSchema.setSumPaym(tLCInsureAccSchema.getSumPaym());
                            	aLCInsureAccClassSchema.setBalaDate(mCurrentData);
                            	aLCInsureAccClassSchema.setModifyDate(mCurrentData);
                            	aLCInsureAccClassSchema.setModifyTime(mCurrentTime);                        	
                            	aLCInsureAccClassSchema.setOperator(mGlobalInput.Operator);
                            	map.put(aLCInsureAccClassSchema, "DELETE&INSERT");
                            }
                            
                            LCInsureAccClassFeeDB aLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
                            LCInsureAccClassFeeSchema aLCInsureAccClassFeeSchema = new LCInsureAccClassFeeSchema();
                            aLCInsureAccClassFeeDB.setPolNo(tTrPolNo);
                            if(aLCInsureAccClassFeeDB.query().size()>=1)
                            {
                            	aLCInsureAccClassFeeSchema = aLCInsureAccClassFeeDB.query().get(1).getSchema();	
                            	aLCInsureAccClassFeeSchema.setFee(Arith.round(aLCInsureAccClassFeeSchema.getFee()+tFCMoney, 2));
                            	aLCInsureAccClassFeeSchema.setBalaDate(mCurrentData);
                            	aLCInsureAccClassFeeSchema.setModifyDate(mCurrentData);
                            	aLCInsureAccClassFeeSchema.setModifyTime(mCurrentTime);                        	
                            	aLCInsureAccClassFeeSchema.setOperator(mGlobalInput.Operator);
                            	map.put(aLCInsureAccClassFeeSchema, "DELETE&INSERT");
                            }  
                    	}                                                                                           
                    } else {
                        CError.buildErr(this, "保单"+tContNo+"账户信息查询失败！");
                        return false;
                    }
                } else {
                    continue;
                }
                }
            }
        }
        return true;
    }
//    private boolean updateLCContState(LLClaimDetailSchema aLLClaimDetailSchema) {
//    	
//    	//万能险赔付后保单状态为终止状态，避免保全月结    
//    	LCPolDB tLCPolDB = new LCPolDB();                 		   	      
//        tLCPolDB.setPolNo(aLLClaimDetailSchema.getPolNo());
//        if(!tLCPolDB.getInfo())
//        {
//        	 CError.buildErr(this, "保单" + aLLClaimDetailSchema.getContNo() + "险种信息查询失败");
//             return false; 
//        }
//        LCPolSchema tLCPolSchema = tLCPolDB.getSchema();
//        tLCPolSchema.setStateFlag("3");
//        map.put(tLCPolSchema, "DELETE&INSERT");
//        
//        String tPolno ="";
//        LCContStateSchema tLCPolStateSchema = new LCContStateSchema();
//        tLCPolStateSchema.setContNo(aLLClaimDetailSchema.getContNo());
//        tLCPolStateSchema.setGrpContNo("00000000000000000000");
//        tLCPolStateSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
//        tLCPolStateSchema.setPolNo(tLCPolSchema.getPolNo());
//        tLCPolStateSchema.setStateType("Terminate");
//        tLCPolStateSchema.setOtherNoType("LP");
//        tLCPolStateSchema.setOtherNo(mCaseNo);
//        tLCPolStateSchema.setState("1");
//        tLCPolStateSchema.setStateReason("LZ");
//        tLCPolStateSchema.setStartDate(mCurrentData);
//        tLCPolStateSchema.setMakeDate(mCurrentData);
//        tLCPolStateSchema.setMakeTime(mCurrentTime);
//        tLCPolStateSchema.setModifyDate(mCurrentData);
//        tLCPolStateSchema.setModifyTime(mCurrentTime);
//        tLCPolStateSchema.setOperator(mGlobalInput.Operator);
//        map.put(tLCPolStateSchema, "INSERT");
//        
//        LDCode1DB tLDCode1DB =new LDCode1DB();
//        String mRiskSQL ="select * from ldcode1 where codetype='checkappendrisk' "
//        			+" and code1='" + aLLClaimDetailSchema.getRiskCode() + "'";
//        LDCode1Set mLDCode1Set = tLDCode1DB.executeQuery(mRiskSQL);        
//        
//        if(mLDCode1Set.size()<=0)
//        {
//        	String sRiskSQL ="select * from ldcode1 where codetype='checkappendrisk' "
//    			+" and code='" + aLLClaimDetailSchema.getRiskCode() + "'";
//        	LDCode1Set sLDCode1Set = tLDCode1DB.executeQuery(sRiskSQL);
//        	
//        	for(int s =1 ; s <= sLDCode1Set.size() ; s++)
//        	{
//        		LDCode1Schema sLDCode1Schema = sLDCode1Set.get(s);
//        		LCPolDB sLCPolDB = new LCPolDB();  
//        		sLCPolDB.setRiskCode(sLDCode1Schema.getCode1());
//        		sLCPolDB.setContNo(aLLClaimDetailSchema.getContNo());
//        		if(sLCPolDB.query().size()>=1)
//        		{
//        			LCPolSchema sLCPolSchema = sLCPolDB.query().get(1).getSchema();
//        			tPolno = sLCPolSchema.getPolNo();
//        			sLCPolSchema.setStateFlag("3");
//        			map.put(sLCPolSchema, "DELETE&INSERT");
//        			
//        			LCContStateSchema sLCPolStateSchema = new LCContStateSchema();
//        			sLCPolStateSchema.setContNo(aLLClaimDetailSchema.getContNo());
//        			sLCPolStateSchema.setGrpContNo("00000000000000000000");
//        			sLCPolStateSchema.setInsuredNo(sLCPolSchema.getInsuredNo());
//        			sLCPolStateSchema.setPolNo(sLCPolSchema.getPolNo());
//        			sLCPolStateSchema.setStateType("Terminate");
//        			sLCPolStateSchema.setOtherNoType("LP");
//        			sLCPolStateSchema.setOtherNo(mCaseNo);
//        			sLCPolStateSchema.setState("1");
//        			sLCPolStateSchema.setStateReason("LZ");
//        			sLCPolStateSchema.setStartDate(mCurrentData);
//        			sLCPolStateSchema.setMakeDate(mCurrentData);
//        			sLCPolStateSchema.setMakeTime(mCurrentTime);
//        			sLCPolStateSchema.setModifyDate(mCurrentData);
//        			sLCPolStateSchema.setModifyTime(mCurrentTime);
//        			sLCPolStateSchema.setOperator(mGlobalInput.Operator);
//    		        map.put(sLCPolStateSchema, "INSERT");
//        		}
//        			
//        	}            	
//        }else if(mLDCode1Set.size() >=1 ){
//        	for(int m =1 ; m<=mLDCode1Set.size() ; m++)
//        	{
//        		LDCode1Schema mLDCode1Schema = mLDCode1Set.get(m);
//        		LCPolDB mLCPolDB = new LCPolDB();  
//        		mLCPolDB.setRiskCode(mLDCode1Schema.getCode());
//        		mLCPolDB.setContNo(aLLClaimDetailSchema.getContNo());
//        		if(mLCPolDB.query().size()>=1)
//        		{
//        			LCPolSchema mLCPolSchema = mLCPolDB.query().get(1).getSchema();
//        			tPolno = mLCPolSchema.getPolNo();
//        			mLCPolSchema.setStateFlag("3");
//        			map.put(mLCPolSchema, "DELETE&INSERT");
//        			
//        			LCContStateSchema mLCPolStateSchema = new LCContStateSchema();
//        			mLCPolStateSchema.setContNo(aLLClaimDetailSchema.getContNo());
//        			mLCPolStateSchema.setGrpContNo("00000000000000000000");
//        			mLCPolStateSchema.setInsuredNo(mLCPolSchema.getInsuredNo());
//        			mLCPolStateSchema.setPolNo(mLCPolSchema.getPolNo());
//        			mLCPolStateSchema.setStateType("Terminate");
//        			mLCPolStateSchema.setOtherNoType("LP");
//        			mLCPolStateSchema.setOtherNo(mCaseNo);
//        			mLCPolStateSchema.setState("1");
//        			mLCPolStateSchema.setStateReason("LZ");
//        			mLCPolStateSchema.setStartDate(mCurrentData);
//        			mLCPolStateSchema.setMakeDate(mCurrentData);
//        			mLCPolStateSchema.setMakeTime(mCurrentTime);
//        			mLCPolStateSchema.setModifyDate(mCurrentData);
//        			mLCPolStateSchema.setModifyTime(mCurrentTime);
//        			mLCPolStateSchema.setOperator(mGlobalInput.Operator);
//    		        map.put(mLCPolStateSchema, "INSERT");
//        		}
//        			
//        	}
//        }                               	                                    		                     	                            
//               
//    	String contSql=" select 1 from lccont a where a.contno='"+ aLLClaimDetailSchema.getContNo() +"'"
//    			+" and exists (select 1 from lcpol where contno=a.contno and stateflag<>'3' "
//    			+" and polno not in ('"+aLLClaimDetailSchema.getPolNo()+"','"+ tPolno +"'))";    
//    	ExeSQL tExeSQL = new ExeSQL();
//        SSRS tSSRS = tExeSQL.execSQL(contSql);
//        if(tSSRS.getMaxRow()<=0)
//        {
//        	LCContDB tLCContDB =new LCContDB();
//        	tLCContDB.setContNo(aLLClaimDetailSchema.getContNo());
//        	if(!tLCContDB.getInfo())
//        	{
//           	 	CError.buildErr(this, "保单" + aLLClaimDetailSchema.getContNo() + "保单信息查询失败");
//                return false; 
//        	}
//        	LCContSchema tLCContSchema = tLCContDB.getSchema();
//        	tLCContSchema.setStateFlag("3");
//        	map.put(tLCContSchema, "DELETE&INSERT");
//            
//            LCContStateSchema tLCContStateSchema = new LCContStateSchema();
//            tLCContStateSchema.setContNo(aLLClaimDetailSchema.getContNo());
//            tLCContStateSchema.setGrpContNo("00000000000000000000");
//            tLCContStateSchema.setInsuredNo(tLCContSchema.getInsuredNo());
//            tLCContStateSchema.setPolNo("000000");
//            tLCContStateSchema.setStateType("Terminate");
//            tLCContStateSchema.setOtherNoType("LP");
//            tLCContStateSchema.setOtherNo(mCaseNo);
//            tLCContStateSchema.setState("1");
//            tLCContStateSchema.setStateReason("LZ");
//            tLCContStateSchema.setStartDate(mCurrentData);
//            tLCContStateSchema.setMakeDate(mCurrentData);
//            tLCContStateSchema.setMakeTime(mCurrentTime);
//            tLCContStateSchema.setModifyDate(mCurrentData);
//            tLCContStateSchema.setModifyTime(mCurrentTime);
//            tLCContStateSchema.setOperator(mGlobalInput.Operator);
//            map.put(tLCContStateSchema, "INSERT");
//        }            	
//                                   
//    	return true;
//    	
//    } 
    /**
     * 扣除预付赔款
     * @return
     */
    private boolean dealPrapaid() {
		for (int i = 1; i <= mLJAGetClaimSet.size(); i++) {
			LJAGetClaimSchema tLJAGetClaimSchema = mLJAGetClaimSet.get(i);
			System.out.println("是否回销预付赔款====="+mPrePaidFlag);
			if ("00000000000000000000"
					.equals(tLJAGetClaimSchema.getGrpContNo())|| !mPrePaidFlag.equals("1")) {
				System.out.println("预付赔款个单或未回销预付赔款不处理：" + tLJAGetClaimSchema.getContNo());
				continue;
			}

			LLPrepaidGrpContDB tLLPrepaidGrpContDB = new LLPrepaidGrpContDB();
			tLLPrepaidGrpContDB.setGrpContNo(tLJAGetClaimSchema.getGrpContNo());

			if (!tLLPrepaidGrpContDB.getInfo()) {
				continue;
			}

			boolean tLockFlag = true;

			for (int j = 1; j <= mLLPrepaidGrpContSet.size(); j++) {
				LLPrepaidGrpContSchema tLLPrepaidGrpContSchema = mLLPrepaidGrpContSet
						.get(j);
				if (tLLPrepaidGrpContSchema.getGrpContNo().equals(
						tLJAGetClaimSchema.getGrpContNo())) {
					tLockFlag = false;
					break;
				}
			}

			if (tLockFlag) {
				/*
				if(mLocked.equals("locked") || mLocked == "locked") {
					if (!LLCaseCommon.lockFY(tLJAGetClaimSchema.getGrpContNo(),
							mGlobalInput)) {
						CError.buildErr(this, "为防止并发操作，团体保单"
								+ tLJAGetClaimSchema.getGrpContNo() + "被锁定，请稍后操作");
						return false;
					}
				}
				*/
				if (Arith.round(tLLPrepaidGrpContDB.getApplyAmount()
						+ tLLPrepaidGrpContDB.getRegainAmount()
						+ tLLPrepaidGrpContDB.getSumPay(), 2) != tLLPrepaidGrpContDB
						.getPrepaidBala()) {
					CError.buildErr(this, "保单"
							+ tLJAGetClaimSchema.getGrpContNo() + "预付赔款余额错误");
					return false;
				}
				
				mLLPrepaidGrpContSet.add(tLLPrepaidGrpContDB.getSchema());
			}
			
			boolean tTraceFlag = true;
			
			for (int j = 1; j <= mLLPrepaidTraceSet.size(); j++) {
				LLPrepaidTraceSchema tLLPrepaidTraceSchema = mLLPrepaidTraceSet.get(j);
				if (tLLPrepaidTraceSchema.getGrpContNo().equals(tLJAGetClaimSchema.getGrpContNo())
					&& tLLPrepaidTraceSchema.getGrpPolNo().equals(tLJAGetClaimSchema.getGrpPolNo())) {
					tLLPrepaidTraceSchema.setMoney(Arith.round(
							tLLPrepaidTraceSchema.getMoney()
									- tLJAGetClaimSchema.getPay(), 2));
					tTraceFlag = false;
					break;
				}
			}
			
			if (tTraceFlag) {
				LLPrepaidGrpPolDB tLLPrepaidGrpPolDB = new LLPrepaidGrpPolDB();
				tLLPrepaidGrpPolDB
						.setGrpPolNo(tLJAGetClaimSchema.getGrpPolNo());
				if (!tLLPrepaidGrpPolDB.getInfo()) {
					CError.buildErr(this, "保单"
							+ tLJAGetClaimSchema.getGrpContNo() + "下险种"
							+ tLJAGetClaimSchema.getRiskCode() + "预付信息查询失败");
					return false;
				}

				if (Arith.round(tLLPrepaidGrpPolDB.getApplyAmount()
						+ tLLPrepaidGrpPolDB.getRegainAmount()
						+ tLLPrepaidGrpPolDB.getSumPay(), 2) != tLLPrepaidGrpPolDB
						.getPrepaidBala()) {
					CError.buildErr(this, "保单"
							+ tLJAGetClaimSchema.getGrpContNo() + "下险种"
							+ tLJAGetClaimSchema.getRiskCode() + "预付赔款余额错误");
					return false;
				}

				mLLPrepaidGrpPolSet.add(tLLPrepaidGrpPolDB.getSchema());

				
				
				LLPrepaidTraceSchema tLLPrepaidTraceSchema = new LLPrepaidTraceSchema();
				
				String tSerialNo = PubFun1.CreateMaxNo("PREPAIDTRACENO",
						tLJAGetClaimSchema.getManageCom());
				
				Reflections tReflections = new Reflections();
				tReflections.transFields(tLLPrepaidTraceSchema,tLJAGetClaimSchema);
				tLLPrepaidTraceSchema.setSerialNo(tSerialNo);
				tLLPrepaidTraceSchema.setOtherNoType("C");
				tLLPrepaidTraceSchema.setBalaDate(mCurrentData);
				tLLPrepaidTraceSchema.setBalaTime(mCurrentTime);
				tLLPrepaidTraceSchema.setMoneyType("PK");
				tLLPrepaidTraceSchema.setMoney(Arith.round(-tLJAGetClaimSchema.getPay(),2));
				tLLPrepaidTraceSchema.setState("1");
				
				mLLPrepaidTraceSet.add(tLLPrepaidTraceSchema);
			} 
		}
		
		if (mLLPrepaidTraceSet.size() != mLLPrepaidGrpPolSet.size()) {
			CError.buildErr(this, "预付险种轨迹查询错误");
			return false;
		}
		
		for (int j = 1; j<=mLLPrepaidTraceSet.size();j++) {
			LLPrepaidTraceSchema tLLPrepaidTraceSchema = mLLPrepaidTraceSet.get(j);
			for (int m = 1; m <= mLLPrepaidGrpPolSet.size(); m++) {
				LLPrepaidGrpPolSchema tLLPrepaidGrpPolSchema = mLLPrepaidGrpPolSet
						.get(m);
				if (tLLPrepaidGrpPolSchema.getGrpPolNo().equals(
						tLLPrepaidTraceSchema.getGrpPolNo())) {
					//取消部分回销，部分走赔款的情况
					double tMoney = 0;
//					tMoney = tLLPrepaidGrpPolSchema.getPrepaidBala() > -tLLPrepaidTraceSchema
//							.getMoney() ? -tLLPrepaidTraceSchema.getMoney()
//							: tLLPrepaidGrpPolSchema.getPrepaidBala();
							
					if(tLLPrepaidGrpPolSchema.getPrepaidBala() < -tLLPrepaidTraceSchema.getMoney()){
						CError.buildErr(this, "团体保单下险种:"+tLLPrepaidGrpPolSchema.getGrpPolNo()+",预付赔款余额不足回销该案件/批次！");
						return false;
					}else{
						tMoney = -tLLPrepaidTraceSchema.getMoney();
					}
					tLLPrepaidTraceSchema.setLastBala(tLLPrepaidGrpPolSchema
							.getPrepaidBala());
					tLLPrepaidGrpPolSchema.setPrepaidBala(Arith
							.round(tLLPrepaidGrpPolSchema.getPrepaidBala()
									- tMoney, 2));
					tLLPrepaidTraceSchema.setMoney(Arith.round(-tMoney, 2));
					tLLPrepaidTraceSchema.setAfterBala(tLLPrepaidGrpPolSchema
							.getPrepaidBala());
					tLLPrepaidGrpPolSchema.setSumPay(Arith.round(
							tLLPrepaidGrpPolSchema.getSumPay()
									+ tLLPrepaidTraceSchema.getMoney(), 2));
					tLLPrepaidGrpPolSchema.setBalaDate(tLLPrepaidTraceSchema
							.getBalaDate());
					tLLPrepaidGrpPolSchema.setBalaTime(tLLPrepaidTraceSchema
							.getBalaTime());
					tLLPrepaidGrpPolSchema.setOperator(tLLPrepaidTraceSchema
							.getOperator());
					tLLPrepaidGrpPolSchema.setModifyDate(tLLPrepaidTraceSchema
							.getModifyDate());
					tLLPrepaidGrpPolSchema.setModifyTime(tLLPrepaidTraceSchema
							.getModifyTime());
					break;
				}
			}
			
			for (int m = 1; m <= mLLPrepaidGrpContSet.size(); m++) {
				LLPrepaidGrpContSchema tLLPrepaidGrpContSchema = mLLPrepaidGrpContSet
						.get(m);
				if (tLLPrepaidGrpContSchema.getGrpContNo().equals(
						tLLPrepaidTraceSchema.getGrpContNo())) {
					tLLPrepaidGrpContSchema.setPrepaidBala(Arith.round(
							tLLPrepaidGrpContSchema.getPrepaidBala()
									+ tLLPrepaidTraceSchema.getMoney(), 2));
					tLLPrepaidGrpContSchema.setSumPay(Arith.round(
							tLLPrepaidGrpContSchema.getSumPay()
									+ tLLPrepaidTraceSchema.getMoney(), 2));
					tLLPrepaidGrpContSchema.setBalaDate(tLLPrepaidTraceSchema.getBalaDate());
					tLLPrepaidGrpContSchema.setBalaTime(tLLPrepaidTraceSchema.getBalaTime());
					tLLPrepaidGrpContSchema.setOperator(tLLPrepaidTraceSchema.getOperator());
					tLLPrepaidGrpContSchema.setModifyDate(tLLPrepaidTraceSchema.getModifyDate());
					tLLPrepaidGrpContSchema.setModifyTime(tLLPrepaidTraceSchema.getModifyTime());
					break;
				}
			}
		}
		
		//扣除的预付赔款金额
		double tSumMoney = 0;
		
		for (int l = 1; l <= mLLPrepaidTraceSet.size(); l++) {
			LLPrepaidTraceSchema tLLPrepaidTraceSchema = mLLPrepaidTraceSet
					.get(l);
			if (tLLPrepaidTraceSchema.getMoney() != 0) {
				mBackMsg += "<br>保单" + tLLPrepaidTraceSchema.getGrpContNo()
						+ "下险种" + tLLPrepaidTraceSchema.getRiskCode()
						+ ":预付赔款余额" + new DecimalFormat("0.00").format(tLLPrepaidTraceSchema.getLastBala())
						+ ",本次回销预付赔款" + new DecimalFormat("0.00").format(-tLLPrepaidTraceSchema.getMoney());
				
				LJAGetClaimSchema tLJAGetClaimSchema = new LJAGetClaimSchema();
				tLJAGetClaimSchema
						.setActuGetNo(saveLJAGetSchema.getActuGetNo());
				tLJAGetClaimSchema.setFeeFinaType("YF");
				tLJAGetClaimSchema.setFeeOperationType("000");
				tLJAGetClaimSchema.setOtherNo(saveLJAGetSchema.getOtherNo());
				tLJAGetClaimSchema.setOtherNoType("Y");
				tLJAGetClaimSchema.setGetDutyKind("000");
				tLJAGetClaimSchema.setGrpContNo(tLLPrepaidTraceSchema
						.getGrpContNo());
				tLJAGetClaimSchema.setContNo("00000"+l);
				tLJAGetClaimSchema.setGrpPolNo(tLLPrepaidTraceSchema
						.getGrpPolNo());
				tLJAGetClaimSchema.setPolNo("00000"+l);

				LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
				tLMRiskAppDB.setRiskCode(tLLPrepaidTraceSchema.getRiskCode());

				if (!tLMRiskAppDB.getInfo()) {
					CError.buildErr(this, "险种信息查询失败");
					return false;
				}

				tLJAGetClaimSchema.setRiskCode(tLMRiskAppDB.getRiskCode());
				tLJAGetClaimSchema.setRiskVersion(tLMRiskAppDB.getRiskVer());
				tLJAGetClaimSchema.setKindCode(tLMRiskAppDB.getKindCode());

				LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
				tLCGrpPolDB.setGrpPolNo(tLLPrepaidTraceSchema.getGrpPolNo());
				if (!tLCGrpPolDB.getInfo()) {
					LBGrpPolDB tLBGrpPolDB = new LBGrpPolDB();
					tLBGrpPolDB
							.setGrpPolNo(tLLPrepaidTraceSchema.getGrpPolNo());
					if (!tLBGrpPolDB.getInfo()) {
						CError.buildErr(this, tLLPrepaidTraceSchema
								.getGrpContNo()
								+ "保单查询失败");
						return false;
					}
					tLJAGetClaimSchema.setSaleChnl(tLBGrpPolDB.getSaleChnl());
					tLJAGetClaimSchema.setAgentCode(tLBGrpPolDB.getAgentCode());
					tLJAGetClaimSchema.setAgentGroup(tLBGrpPolDB
							.getAgentGroup());
					tLJAGetClaimSchema.setAgentCom(tLBGrpPolDB.getAgentCom());
					tLJAGetClaimSchema.setManageCom(tLBGrpPolDB.getManageCom());
					tLJAGetClaimSchema.setAgentType(tLBGrpPolDB.getAgentType());

				} else {
					tLJAGetClaimSchema.setSaleChnl(tLCGrpPolDB.getSaleChnl());
					tLJAGetClaimSchema.setAgentCode(tLCGrpPolDB.getAgentCode());
					tLJAGetClaimSchema.setAgentGroup(tLCGrpPolDB
							.getAgentGroup());
					tLJAGetClaimSchema.setAgentCom(tLCGrpPolDB.getAgentCom());
					tLJAGetClaimSchema.setManageCom(tLCGrpPolDB.getManageCom());
					tLJAGetClaimSchema.setAgentType(tLCGrpPolDB.getAgentType());
				}

				tLJAGetClaimSchema.setPay(tLLPrepaidTraceSchema.getMoney());
				tLJAGetClaimSchema.setOPConfirmCode(mGlobalInput.Operator);
				tLJAGetClaimSchema.setOPConfirmDate(saveLJAGetSchema
						.getMakeDate());
				tLJAGetClaimSchema.setOPConfirmTime(saveLJAGetSchema
						.getMakeTime());
				tLJAGetClaimSchema.setOperator(mGlobalInput.Operator);
				tLJAGetClaimSchema.setMakeDate(saveLJAGetSchema.getMakeDate());
				tLJAGetClaimSchema.setMakeTime(saveLJAGetSchema.getMakeTime());
				tLJAGetClaimSchema
						.setModifyDate(saveLJAGetSchema.getMakeDate());
				tLJAGetClaimSchema
						.setModifyTime(saveLJAGetSchema.getMakeTime());

				tSumMoney = Arith.round(
						tSumMoney + tLJAGetClaimSchema.getPay(), 2);

				mLJAGetClaimSet.add(tLJAGetClaimSchema);
			}
		}
		
		if (tSumMoney != 0) {
			saveLJAGetSchema.setSumGetMoney(Arith.round(saveLJAGetSchema
					.getSumGetMoney()
					+ tSumMoney, 2));
			mBackMsg += "<br>案件实际给付" + new DecimalFormat("0.00").format(saveLJAGetSchema.getSumGetMoney());
		}
				
		if (mLLPrepaidTraceSet.size() > 0) {
			map.put(mLLPrepaidTraceSet, "INSERT");
			map.put(mLLPrepaidGrpPolSet, "UPDATE");
			map.put(mLLPrepaidGrpContSet, "UPDATE");
		}
		
		return true;
	}
    
    public String getBackMsg() {
    	return mBackMsg;
    }
    
 // # 3337 管理式补充医疗保险(B款)**start**
    
    /***
     * 获取到公共账户的保单号
     */
    private String getPoolPolNo (String aPolNo){
    	String tGRPSQL = "select grppolno from lcpol "
                +" where polno='"+aPolNo+"' union select grppolno from lbpol where polno='"+aPolNo+"'";
                
    	ExeSQL texesql = new ExeSQL();
    	String tGrpPolNo = texesql.getOneValue(tGRPSQL);

    	String tsql = "select polno from lcpol "
                + "where grppolno='"+tGrpPolNo+"' and poltypeflag = '2' and acctype='1' ";
    	String tPolNo = texesql.getOneValue(tsql);
    	
    	return tPolNo;
    }
    
    // 管理式补充医疗保险(B款)**end**
}
