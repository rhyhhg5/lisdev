package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:理赔功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 */
public class ClaimAutoChkUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public ClaimAutoChkUI() {}

// @Main

  public static void main(String[] args)
  {
      LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new LLClaimUnderwriteSchema();
        tLLClaimUnderwriteSchema.setClmNo("86000020020520000031");
        tLLClaimUnderwriteSchema.setRgtNo("00100020020510000030");
        tLLClaimUnderwriteSchema.setPolNo("86120020020110000002");

         //读取Session中的全局类
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ComCode  = "86";
        LLClaimSchema tLLClaimSchema = new LLClaimSchema();
        tLLClaimSchema.setClmNo("86000020030520000008");
        tLLClaimSchema.setRealPay("10000");

        ClaimAutoChkUI tClaimAutoChkUI   = new ClaimAutoChkUI();
          // 准备传输数据 VData
        VData tVData = new VData();
        tVData.addElement(tLLClaimSchema);
        tVData.addElement(tG);
        //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
        tClaimAutoChkUI.submitData(tVData,"AutoChkCase");
        tVData.clear();

  }

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    System.out.println("chkbl begin");
    ClaimAutoChkBL tClaimAutoChkBL = new ClaimAutoChkBL();

    System.out.println("---ClaimAutoChkBL UI BEGIN---");
    if (tClaimAutoChkBL.submitData(cInputData,mOperate) == false)
	{
  		// @@错误处理
      this.mErrors.copyAllErrors(tClaimAutoChkBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimAutoChkUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
	}
      mResult.clear();
       mResult = tClaimAutoChkBL.getResult();
    return true;
  }
  public VData getResult()
  {
          return mResult;
  }

}