/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: 青岛社保理赔磁盘导入的解析类，从一个XML数据流中解析出批次下分案信息</p>
 * <p>Description: 青岛社保理赔磁盘导入的解析类，修改解析的方式，但如果需要增加字段，需要同步修改xml</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author Xx
 * @version 6.0
*/

package com.sinosoft.lis.llcase;

import java.util.Date;
import java.util.Vector;

import org.w3c.dom.Node;

import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.diskimport.Sheet;
import com.sinosoft.lis.pubfun.diskimport.XlsImporter;
import com.sinosoft.lis.schema.LLAppClaimReasonSchema;
import com.sinosoft.lis.schema.LLCaseCureSchema;
import com.sinosoft.lis.schema.LLCaseOpTimeSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLFeeMainSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLSecurityReceiptSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.vschema.LLCaseCureSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class QDSCParserNew implements SIParser{


    // 下面是一些常量定义
    private String mRgtNo = "";
    private String mGrpContNo = "";
    private String mPath = "";
    private String[] Title = null;
    private String vtsName = "ClaimErrList.vts";
    private ListTable  FalseListTable = new ListTable();
    private GlobalInput mG = new GlobalInput();
    public CErrors mErrors = new CErrors();

    /** xls文件名*/
    private String fileName = null;
    /** xml文件名*/
    private String configFileName = null;
    /** Sheet的名字，主要是解析Excel时，会查找对应xml的节点，Sheet1是全部xml共有的节点 */
    private String[] mSheetNames = {"Sheet1"};
    /** 存放数据的容器 */
    private Vector mVector = new Vector();
    
    public QDSCParserNew() {
    }

    /**
     * 为了不改动其抽象类，在此加入其构造器，配置传入的文件名及解析xml名
     * @param aFileName
     * @param aXmlName
     */
    public QDSCParserNew(String aFileName,String aXmlName) {
    	this.fileName = aFileName;
        this.configFileName = aXmlName;
    }
    
    public boolean setGlobalInput(GlobalInput aG) {
        this.mG = aG;
        FalseListTable.setName("FAULT");
        return true;
    }

    public void setFormTitle(String[] cTitle,String cvtsName){
        Title = cTitle;
        vtsName = cvtsName;
        System.out.println(vtsName);
    }
    
    /**
     * 执行磁盘导入
     * @return boolean
     */
    public boolean doImport() {
    	//fileName = "E:\\lisdev\\ui\\temp_lp\\8661.xls";
    	//configFileName = "E:\\lisdev\\ui\\temp_lp\\LL61CaseImport.xml";
    	System.out.println(" Into NewPRSCParser doImport");
        
		XlsImporter importer = new XlsImporter(fileName, configFileName,
                mSheetNames);
		
		importer.doImport();
        Sheet[] sheets = importer.getSheets();
        if (!dealSheets(sheets)) {
            return false;
        }
        return true;
    }
    
    /**
     * 处理所有导入的sheet
     * @param sheets Sheet[]
     * @return boolean
     */
    protected boolean dealSheets(Sheet[] sheets) {
        for (int i = 0; i < sheets.length; i++) {
            if (!dealOneSheet(sheets[i], i)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * 处理一个sheet中全部的数据，包装为一个存放数据的List，用于后续逻辑处理
     * @param sheet
     * @param numberOfSheet
     * @return
     */
    protected boolean dealOneSheet(Sheet sheet,int numberOfSheet) {
   
    	int size = sheet.getRowSize();//Excel中数据量的多少（无版本号），读到空行结束
    	
    	for (int i = 0; i < size; i++) { 
//    		读sheet中的一行记录，TransferData
    		 TransferData td = new TransferData();
    		 int col = sheet.getColSize();
             for (int j = 0; j < col; j++) {
                 String head = sheet.getHead(j);
                 System.out.println("head:" + head);
                 if ((head == null) || (head.equals(""))) {
                     continue;
                 }
                 String value = sheet.getText(i, j, 1);
                 Object[] args = new Object[1];
                 args[0] = StrTool.cTrim(value);
                 td.setNameAndValue(head, value);
             }  
             mVector.add(td);
         }
    	
    	return true;
    }

    /**
     * 解析案件结点(公用函数)
     * @param node Node
     * @return VData
     */
    public VData parseOneInsuredNode(Node node) {
    	VData tReturn = new VData();
        System.out.println("<-Go Into NewPRSCParser.parseOneInsuredNode()->");
        System.out.println("fileName:"+fileName);
        System.out.println("configFileName:"+configFileName);

        if(!doImport()){
        	CError.buildErr(this,"磁盘导入失败，Excel文件解析错误！");
        }
        int tLength = mVector.size();
        int FailureCount = 0;
        
        for (int nIndex = 0; nIndex < tLength; nIndex++) {
        	TransferData tTransferData = (TransferData)mVector.get(nIndex);
            if(!genCase(tTransferData)){
                System.out.println("导入这个人失败");
                FailureCount++;
                //写导出错误xml的函数
            }
        }
        // 3500 模版新增字段类型
        String[] tErrorTitle = {"职工电脑编号", "姓名", "性别", "出生日期",
                         "公民身份号码", "医院编码", "医院名称",
                          "入院日期", "出院日期",
                         "入院诊断疾病编码", "出院疾病编码", "结算方式",
                          "医保结算时间", "医疗总额", "纳入统筹额",
                         "统筹支付额", "大额支付额", "现金支付额",
                         "实赔金额","银行编码","账户名","账号","给付责任编码","给付责任类型","发生地点(省)"
                         ,"发生地点(市)","发生地点(县)","医保类型","就诊类别","错误原因"};
        String[] tErrorType = {"String", "String", "String", "DateTime",
                "String", "String", "String",
                 "DateTime", "DateTime",
                "String", "String", "String",
                "DateTime", "Number", "Number",
                "Number", "Number", "Number",
                "Number","String", "String", "String", "String","String",
                "String","String","String","String","String","String"};
        
        LLExportErrorList xmlexport = new LLExportErrorList(); //新建一个XmlExport的实例
        xmlexport.createDocument(mRgtNo);
        xmlexport.addListTable(FalseListTable, tErrorTitle, tErrorType);
        xmlexport.outputDocumentToFile(mPath, mRgtNo); //输出xml文档到文件
        String FailureNum = FailureCount+"";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("FailureNum",FailureNum);
        tReturn.clear();
//        tReturn.addElement(xmlexport);
        tReturn.add(tTransferData);
        return tReturn;
    }

    public boolean setParam(String aRgtNo,String aGrpContNo,String aPath){
        mRgtNo = aRgtNo;
        mGrpContNo = aGrpContNo;
        mPath = aPath;
        return true;
    }

    private boolean genCase(TransferData cTD){
        String aCustomerName = "" + (String) cTD.getValueByName("CustomerName");
        String aSecurityNo = "" + (String) cTD.getValueByName("SecurityNo");
        if("".equals("aCustomerName")||null==aCustomerName||
           "".equals("aSecurityNo")||null==aSecurityNo)
        {
        	String ErrMessage = "导入模板中的客户姓名或职工电脑编号不能为空!";
        	getFalseList(cTD,ErrMessage);
            return false;
        }
        String strSQL0 ="select distinct a.insuredno,a.sex,a.birthday,a.idno,a.insuredstat,a.idtype ";
        String sqlpart1="from lcinsured a where ";
        String sqlpart2="from lbinsured a where ";
        String wherePart = " a.othidno='"+aSecurityNo+"' and a.GrpContNo = '"+mGrpContNo
                         + "' and a.name='"+aCustomerName+"' ";
        String strSQL=strSQL0+sqlpart1+ wherePart+" union "+strSQL0+sqlpart2+wherePart;
        ExeSQL texesql = new ExeSQL();
        SSRS tssrs = new SSRS();
        tssrs = texesql.execSQL(strSQL);
        System.out.println(strSQL);
        if(tssrs==null||tssrs.getMaxRow()<=0){
            String ErrMessage = "团单"+mGrpContNo+"下没有社保号为"+aSecurityNo+"的被保险人,请检查证件号码和姓名是否正确。";
            getFalseList(cTD,ErrMessage);
            return false;
        }
        String result = LLCaseCommon.checkPerson(tssrs.GetText(1, 1));
        if(!"".equals(result)){
            String ErrMessage = "团单"+mGrpContNo+"下"+ "该客户正在进行保全操作！";
            getFalseList(cTD,ErrMessage);
            return false;
        }
        String strCustomerSQL="select * from lpedorapp a,lpgrpedoritem b where a.edoracceptno "
        		+" in (select max(edorno) from lpdiskimport where  insuredno='"+tssrs.GetText(1, 1)+"' "
        		+" and grpcontno ='"+mGrpContNo+"') and a.edorstate !='0' and a.edoracceptno = b.edoracceptno "
        		+" and b.edortype ='ZT'";
        SSRS customertssrs = new SSRS();
        customertssrs = texesql.execSQL(strCustomerSQL);
        if(customertssrs.getMaxRow()>0){
        	String ErrMessage ="团单"+mGrpContNo+"下"+ "该客户正在进行保全减人操作！";
            getFalseList(cTD, ErrMessage);
            return false;

        }
        String aApplyDate = "" + (String) cTD.getValueByName("ApplyDate");
        if(aApplyDate == null || "null".equals(aApplyDate) || 
        		"".equals(aApplyDate)){
            String ErrMessage = "社保结算日期为空。";
            getFalseList(cTD,ErrMessage);
            return false;
        }

        String aSupInHosFee = "" + (String) cTD.getValueByName("SupInHosFee");
        if(aSupInHosFee == null || "null".equals(aSupInHosFee) || 
        		"".equals(aSupInHosFee)){
            String ErrMessage = "大额支付额为空，无法进行理算。";
            getFalseList(cTD,ErrMessage);
            return false;
        }

        //校验客户基本信息姓名、性别、出生日期、身份证号是否与系统一致
        String aCustomerNo = tssrs.GetText(1,1);
        String aSex = "" + (String) cTD.getValueByName("Sex");
        if(aSex != null && !"".equals(aSex) && !"null".equals(aSex))           
        {
        	if(!aSex.equals(tssrs.GetText(1,2)))
        	{
        		String ErrMessage ="出险人"+aCustomerNo+"的性别与系统中的性别不一致!";
	            getFalseList(cTD,ErrMessage);
	        	return false;
        	}
        }else{
        	aSex = tssrs.GetText(1,2);  
        } 
        
        String aBirthday = "" + (String) cTD.getValueByName("Birthday");
        if(aBirthday != null && !"".equals(aBirthday) && !"null".equals(aBirthday))
        {
        	 aBirthday = FormatDate(aBirthday);
 	        if(!aBirthday.equals(tssrs.GetText(1,3)))
 	    	{
 	    		String ErrMessage ="出险人"+aCustomerNo+"的出生日期与系统中的出生日期不一致!";
 	            getFalseList(cTD,ErrMessage);
 	        	return false;
 	    	}
        }           
        else{
        	aBirthday = tssrs.GetText(1,3);
        }
        //3501 保单登记平台，证件类型为空时，默认修改为 4-其他 star
        String tIdType=tssrs.GetText(1, 6);
        if("null".equals(tIdType) ||tIdType==null||tIdType==""){
        	tIdType="4";
        }
//        String tIdType=tssrs.GetText(1, 6).equals("null") ? "" : tssrs.GetText(1, 6);;//获得证件类型
        System.out.println("tIdType:"+tIdType);
        String aIDNo = "" + (String) cTD.getValueByName("IDNo");
        if(aIDNo != null && !"".equals(aIDNo) && !"null".equals(aIDNo))
        {
	        if(!aIDNo.equals(tssrs.GetText(1,4)))
	    	{
	    		String ErrMessage ="出险人"+aCustomerNo+"的证件号码与系统中的证件号码不一致!";
	            getFalseList(cTD,ErrMessage);
	        	return false;
	    	}
        }        
        else{
        	aIDNo = tssrs.GetText(1,4);
        } 
	                
        /* if(tIdtype.equals("0"))//是身份证时，对身份证号进行校验
        {	       
	        if(!PubFun.checkIDNo(tId))//校验数据库里存储的身份证号，by zzh 20100925
	        {	
	        	String ErrMessage ="社保号为"+aSecurityNo+"的客户身份证号错误，请先做客户资料变更！";
	            getFalseList(nodeCase,ErrMessage);
	        	return false;
	        }
        }*/
        
        
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
        LLSecurityReceiptSchema tLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
        LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
        LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
        VData tVData = new VData();

        genCaseInfoBL tgenCaseInfoBL  = new genCaseInfoBL();

        tLLCaseSchema.setRgtType("1");
        tLLRegisterSchema.setRgtNo(mRgtNo);
        tLLRegisterSchema.setRgtObjNo(mGrpContNo);

        LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
        tLLAppClaimReasonSchema.setReasonCode("02");        //原因代码
        tLLAppClaimReasonSchema.setReason("住院");                //申请原因
        tLLAppClaimReasonSchema.setCustomerNo(aCustomerNo);
        tLLAppClaimReasonSchema.setReasonType("0");

        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setRgtNo(mRgtNo);
        tLLCaseDB.setCustomerNo(aCustomerNo);
        LLCaseSet temLLCaseSet = tLLCaseDB.query();
        if(temLLCaseSet.size()>0){
            tLLCaseSchema.setUWState("7");
        }
        //2774
        String aBankCode = "" + (String) cTD.getValueByName("BankCode");
        String aBankAccNo = "" + (String) cTD.getValueByName("BankAccNo");
        String aAccName = "" + (String) cTD.getValueByName("AccName");
        
//      modify by zhangyige 2012-07-09
        String aPrePaidFlag = "";
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if(tLLRegisterDB.getInfo())
		{
			aPrePaidFlag = tLLRegisterDB.getSchema().getPrePaidFlag();
		}
        tLLCaseSchema.setPrePaidFlag(aPrePaidFlag);
        
        tLLCaseSchema.setRgtState("03");//案件状态
        tLLCaseSchema.setCustomerName(aCustomerName);
        tLLCaseSchema.setCustomerNo(aCustomerNo);
//        tLLCaseSchema.setDeathDate(request.getParameter("DeathDate"));
//        tLLCaseSchema.setCustState(request.getParameter("CustStatus"));
//待补入数据
        tLLCaseSchema.setCaseNo("");

        tLLCaseSchema.setRgtNo(mRgtNo);
        tLLCaseSchema.setSurveyFlag("0");
        tLLCaseSchema.setAccdentDesc("磁盘导入");
        tLLCaseSchema.setRgtState("03");//案件状态
        tLLCaseSchema.setCustBirthday(aBirthday);
        tLLCaseSchema.setCustomerSex(aSex);
        tLLCaseSchema.setIDType(tIdType);

        tLLCaseSchema.setIDNo(aIDNo);//被保人证件号码
        tLLCaseSchema.setOtherIDType("5");
        tLLCaseSchema.setOtherIDNo(aSecurityNo);
        tLLCaseSchema.setBankCode(aBankCode);
        tLLCaseSchema.setBankAccNo(aBankAccNo);
        tLLCaseSchema.setAccName(aAccName);
        //3501 保单登记平台，证件类型为空时，默认修改为 4-其他 end
        //2774
        /*String aGrpNo = "" + (String) cTD.getValueByName("GrpNo");
        tLLCaseSchema.setGrpNo(aGrpNo);*/
        String GetDutyCode = "" + (String) cTD.getValueByName("GetDutyCode");
        tLLCaseSchema.setPhone(GetDutyCode);
        String aAccDate = FormatDate("" + (String) cTD.getValueByName("ApplyDate"));
        System.out.println(aAccDate);
        if(aAccDate==null || "".equals(aAccDate) ||"null".equals(aAccDate)){
            String ErrMessage = "团单"+mGrpContNo+"下" + "该客户出险日期不能为空，或出险日期格式不正确应为'YYYY-MM-DD'！";
             getFalseList(cTD, ErrMessage);
             return false;
         }
        tLLCaseSchema.setAccidentDate(aAccDate);
        if(tLLCaseSchema.getAccidentDate()==null)//by zzh 0930
        {
        	String ErrMessage = "团单"+mGrpContNo+"下" + "该客户出险日期格式不正确,应为'YYYY-MM-DD'！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        if(!LLCaseCommon.llcheckdate(tLLCaseSchema.getAccidentDate())){
        	String ErrMessage = "请核实发生日期录入是否正确";
        	getFalseList(cTD, ErrMessage);
            return false;
        }
        System.out.println(tLLCaseSchema.getAccidentDate());
        tLLCaseSchema.setSurveyFlag("0");
        String tPayModeSQL = "SELECT casegetmode FROM llregister WHERE rgtno='"+mRgtNo+"'";
        ExeSQL tPMExSQL = new ExeSQL();
        SSRS tPMSSRS = tPMExSQL.execSQL(tPayModeSQL);
        if (tPMSSRS.getMaxRow() > 0) {
           try {
               tLLCaseSchema.setCaseGetMode(tPMSSRS.GetText(1, 1));
           } catch (Exception ex) {
               System.out.println("插入给付方式失败！");
           }
        }
        ExeSQL texesql1 = new ExeSQL();
        
        
        String tAccSQL = "select 1 from lcgrppol a,lmriskapp b where a.riskcode=b.riskcode"
            + " and b.risktype3='7' and b.risktype <>'M'"
            + " and a.grpcontno='"+ mGrpContNo +"'";
        SSRS tAccSSRS = texesql1.execSQL(tAccSQL);
        boolean tDiseaseFlag = true;
        if (tAccSSRS.getMaxRow()>0) {
        	tDiseaseFlag = false;
        }
        
        String tHospitalCode = "" + (String) cTD.getValueByName("HosSecNo");
        String tHospitalName = "" + (String) cTD.getValueByName("HospitalName");
        
        if (("".equals(tHospitalCode)||"null".equals(tHospitalCode))&&tDiseaseFlag) {
            CError.buildErr(this, "医院编码不能为空！");
            getFalseList(cTD, "医院编码不能为空！");
            return false;
        }
        
        if (("".equals(tHospitalName)||"null".equals(tHospitalName))&&tDiseaseFlag) {
            CError.buildErr(this, "医院名称不能为空！");
            getFalseList(cTD, "医院名称不能为空！");
            return false;
        }
        //2774
        String aAffixNo = "" + (String)cTD.getValueByName("AffixNo");//实赔金额
        
        tLLFeeMainSchema.setRgtNo(mRgtNo);
        tLLFeeMainSchema.setCaseNo("");
        tLLFeeMainSchema.setCustomerNo(aCustomerNo);
        tLLFeeMainSchema.setCustomerName(aCustomerName);
        tLLFeeMainSchema.setCustomerSex(aSex);
        tLLFeeMainSchema.setInsuredStat(tssrs.GetText(1, 5));
//        tLLFeeMainSchema.setHospitalCode(parseNode(nodeCase,QDSCParser.PATH_HospitalCode));
//       tLLFeeMainSchema.setRealHospDate(request.getParameter("RealHospDate"));
//        tLLFeeMainSchema.setInsuredStat(request.getParameter("InsuredStat"));
        tLLFeeMainSchema.setHospitalName(tHospitalName);
        tLLFeeMainSchema.setHospitalCode(tHospitalCode);
        tLLFeeMainSchema.setHosAtti(tHospitalCode);
        tLLFeeMainSchema.setReceiptNo("53000");
        tLLFeeMainSchema.setFeeAtti("4");
        tLLFeeMainSchema.setSecurityNo(aSecurityNo);
        tLLFeeMainSchema.setReceiptNo("94000");
        tLLFeeMainSchema.setAffixNo(aAffixNo);
        
        //LLFeemain中数据整理//2774
        String tFeeType = "" + (String) cTD.getValueByName("FeeType");
        //String tFeeDate = "" + (String) cTD.getValueByName("FeeDate");
        String tHospStartDate = "" + (String) cTD.getValueByName("HospStartDate");
        String tHospEndDate = "" + (String) cTD.getValueByName("HospEndDate");
        String tInpatientNo = "" + (String) cTD.getValueByName("inpatientNo");
        String tBudgetType = "" + (String) cTD.getValueByName("BudgetType");
        //String tHosFlag = "" + (String) cTD.getValueByName("HosFlag");
        String tFeeInSecu = "" + (String) cTD.getValueByName("FeeInSecu");
        String tPlanFee = "" + (String) cTD.getValueByName("PlanFee");
        String tSecuRefuseFee = "" + (String) cTD.getValueByName("SecuRefuseFee");
        String tGetDutyKind = "" + (String) cTD.getValueByName("GetDutyKind");
        // #3558 保单登记平台日期校验 star
        if(tFeeType=="2"||"2".equals(tFeeType)){
        	tHospStartDate = FormatDate(tHospStartDate);
        	tHospEndDate = FormatDate(tHospEndDate);
        	if(tHospStartDate==null||tHospEndDate==null||"".equals(tHospStartDate)||"".equals(tHospEndDate)){
        		String ErrMessage =aCustomerName+"的客户入出院日期有误,请核实！" ;
                getFalseList(cTD, ErrMessage);
                return false;
        	}
//        	String BeforeDate = PubFun.getBeforeDate(tHospStartDate, tHospEndDate);
        	FDate fd = new FDate();
        	Date HospStartDate = fd.getDate(tHospStartDate);
        	Date HospEndDate = fd.getDate(tHospEndDate);
        	if(HospStartDate!=null&&HospEndDate!=null&&!"".equals(HospStartDate)&&!"".equals(HospEndDate)){
	        	if(HospStartDate.after(HospEndDate)){
	        		String ErrMessage =aCustomerName+"的客户入出院日期有误,请核实！" ;
	                getFalseList(cTD, ErrMessage);
	                return false;
	        	}
        	}else{
        		String ErrMessage =aCustomerName+"的客户入出院日期有误,请核实！" ;
                getFalseList(cTD, ErrMessage);
                return false;
        	}
        	
        	
        }
        // #3558 保单登记平台日期校验  end
//        tLLFeeMainSchema.setFeeType(tFeeType);
        tLLFeeMainSchema.setFeeAtti("4");
        //tLLFeeMainSchema.setFeeDate(FormatDate(tFeeDate));
        tLLFeeMainSchema.setHospStartDate(FormatDate(tHospStartDate));
        tLLFeeMainSchema.setHospEndDate(FormatDate(tHospEndDate));
        tLLFeeMainSchema.setInHosNo(tInpatientNo);
        //tLLFeeMainSchema.setMedicalType(tFeeType);
        tLLFeeMainSchema.setSecuPayType(tBudgetType);
        tLLFeeMainSchema.setSecuPayDate(aApplyDate);
        //tLLFeeMainSchema.setOriginFlag(tHosFlag);
        tLLFeeMainSchema.setOldMainFeeNo(tGetDutyKind);

        tLLSecurityReceiptSchema.setApplyAmnt(aSupInHosFee);
        tLLSecurityReceiptSchema.setSupInHosFee(aSupInHosFee);
        tLLSecurityReceiptSchema.setHighAmnt2(aSupInHosFee);
        tLLSecurityReceiptSchema.setFeeInSecu(tFeeInSecu);
        tLLSecurityReceiptSchema.setPlanFee(tPlanFee);
        tLLSecurityReceiptSchema.setSecuRefuseFee(tSecuRefuseFee);
        if(tLLFeeMainSchema.getSecuPayDate()!=null&&tLLFeeMainSchema.getSecuPayDate()!=""){
        	 if(!LLCaseCommon.llcheckdate(tLLFeeMainSchema.getSecuPayDate())){
             	String ErrMessage = "请核实医保结算日期录入是否正确";
                 getFalseList(cTD,ErrMessage);
                 return false;
             }
        }
       
        if(tLLFeeMainSchema.getHospStartDate()!=null && tLLFeeMainSchema.getHospStartDate()!=""){
        	if(!LLCaseCommon.llcheckdate(tLLFeeMainSchema.getHospStartDate())){
        		String ErrMessage = "请核实入院日期录入是否正确";
                getFalseList(cTD,ErrMessage);
                return false;
        	}
        }
        if(tLLFeeMainSchema.getHospEndDate()!=null && tLLFeeMainSchema.getHospEndDate()!=""){
        	if(!LLCaseCommon.llcheckdate(tLLFeeMainSchema.getHospEndDate())){
        		String ErrMessage = "请核实出院日期录入是否正确";
                getFalseList(cTD,ErrMessage);
                return false;
        	}
        }
        String tDiseaseCode = "" + (String) cTD.getValueByName("InICDCode");
//        String tDiseaseName = ""+parseNode(nodeCase,QDSCParser.PATH_MICDCode);
        if (("".equals(tDiseaseCode)||"null".equals(tDiseaseCode))&&tDiseaseFlag) {
            CError.buildErr(this, "疾病编码不能为空！");
            getFalseList(cTD, "疾病编码不能为空！");
            return false;
        }

        if (!"".equals(tDiseaseCode)) {
            LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
            tLLCaseCureSchema.setDiseaseCode(tDiseaseCode);
        //        tLLCaseCureSchema.setDiseaseName(parseNode(nodeCase,QDSCParser.PATH_MICDCode));
            tLLCaseCureSchema.setMainDiseaseFlag("1");
            tLLCaseCureSet.add(tLLCaseCureSchema);
        }
        
        
        String tDiseaseCode2 = "" + (String) cTD.getValueByName("OutICDCode");
		if (!tDiseaseCode.equals(tDiseaseCode2)) {
			if (("".equals(tDiseaseCode2) || "null".equals(tDiseaseCode2))
					&& tDiseaseFlag) {
				CError.buildErr(this, "疾病编码不能为空！");
				getFalseList(cTD, "疾病编码不能为空！");
				return false;
			}

			if (!"".equals(tDiseaseCode2)) {
				LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
				tLLCaseCureSchema.setDiseaseCode(tDiseaseCode2);
				// tLLCaseCureSchema.setDiseaseName(parseNode(nodeCase,QDSCParser.PATH_SICDCode));
				tLLCaseCureSchema.setMainDiseaseFlag("0");
				tLLCaseCureSet.add(tLLCaseCureSchema);
			}
		}
		// #3500 增加发生地点信息**start**
		LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
		 String tProvinceAccPlace = "" + (String) cTD.getValueByName("ProvinceAccPlace");  // 省级编码
		 String tCityAccPlace = "" + (String)cTD.getValueByName("CityAccPlace");           // 市级编码
		 String tCountyAccPlace ="" +(String)cTD.getValueByName("CountyAccPlace");         // 县级编码
		 
		String tAccCode = LLCaseCommon.checkAccPlace(tProvinceAccPlace, tCityAccPlace, tCountyAccPlace);
		if(!"".equals(tAccCode)){
			String ErrMessage = "出险人"+aCustomerName+",录入 "+tAccCode+"！";
            getFalseList(cTD,ErrMessage);
            return false;
		}else{
			tLLSubReportSchema.setAccProvinceCode(tProvinceAccPlace);
			tLLSubReportSchema.setAccCityCode(tCityAccPlace);
			tLLSubReportSchema.setAccCountyCode(tCountyAccPlace);
		}
		
		String tMedicareType = "" + (String) cTD.getValueByName("MedicareType");
		String tMedicareCode= LLCaseCommon.checkMedicareType(tMedicareType);
		if(!"".equals(tMedicareCode)){
			String ErrMessage = "出险人"+aCustomerName+",录入 "+tMedicareCode+"！";
            getFalseList(cTD,ErrMessage);
            return false;
		}else{
			tLLFeeMainSchema.setMedicareType(tMedicareType);
		}
		// #3500增加发生地点信息**end**
		
        // 保存案件时效
        tLLCaseOpTimeSchema.setRgtState("01");
        tLLCaseOpTimeSchema.setStartDate(PubFun.getCurrentDate());
        tLLCaseOpTimeSchema.setStartTime(PubFun.getCurrentTime());

        System.out.println("<--Star Submit VData-->");
        tVData.add(tLLAppClaimReasonSchema);
        tVData.add(tLLCaseSchema);
        tVData.add(tLLRegisterSchema);
        tVData.add(tLLFeeMainSchema);
        tVData.add(tLLSecurityReceiptSchema);
        tVData.add(tLLCaseCureSet);
        tVData.add(tLLCaseOpTimeSchema);
        if(tLLSubReportSchema!=null){   // 3500 增加发生地点信息
            tVData.add(tLLSubReportSchema);
        }
        tVData.add(mG);
        System.out.println("<--Into tSimpleClaimUI-->");
        if (!tgenCaseInfoBL.submitData(tVData, "INSERT||MAIN")){
            CErrors tError = tgenCaseInfoBL.mErrors;
            String ErrMessage = tError.getFirstError();
            getFalseList(cTD,ErrMessage);
            return false;
        }
        return true;
    }

    private boolean getFalseList(TransferData cTD,String ErrMessage){
        int n = Title.length;
        String[] strCol = new String[n + 1];
        for (int i = 0; i < n; i++) {
            strCol[i] = (String) cTD.getValueByIndex(i);
        }
        strCol[n] = ErrMessage;
        System.out.println("ErrMessage:" + ErrMessage);
        FalseListTable.add(strCol);
        return true;
    }

    private String FormatDate(String aDate){
        String rDate = "";
        rDate = aDate;
        if(aDate.length()<8){
            int days = 0;
            try{
                days = Integer.parseInt(aDate)-2;
            }catch(Exception ex){
                return "";
            }
            rDate = PubFun.calDate("1900-1-1",days,"D",null);
        }
        return rDate;
    }
}
