package com.sinosoft.lis.llcase;

import java.util.Date;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.llcase.*;

/**
 * <p>Title: </p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author
 * @version 1.0
 * @date
 */

public class LLCaseRemarkBL {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;
    
    private LLCaseRemarkSchema mLLCaseRemarkSchema = new LLCaseRemarkSchema();

    private MMap map = new MMap();
    
    /** LLCaseRemark主键*/
    private String mCaseRemarkNo;
    
    /** 全局参数 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 统一更新日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 统一更新时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    private String mOperator = "";
    
    private String mOperatorName = "";

    private String mManageCom = "";

    public LLCaseRemarkBL() {
    }

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        // 将外部传入的数据分解到本类的属性中，准备处理
        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        // 根据业务逻辑对数据进行处理
        if (!dealData()) {
            return false;
        }

        // 装配处理好的数据，准备给后台进行保存
        prepareOutputData();

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLCaseRemarkBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData() {
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mLLCaseRemarkSchema.setSchema((LLCaseRemarkSchema) mInputData.getObjectByObjectName("LLCaseRemarkSchema", 0));
        
        mOperator = mGlobalInput.Operator;
        ExeSQL tExeSQL = new ExeSQL();
        mOperatorName = tExeSQL.getOneValue("select username from lduser where usercode='"+mOperator+"' with ur");
        
        mManageCom = mGlobalInput.ManageCom;
       
        return true;
    }

    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData() {


        if (!"INSERT||MAIN".equals(mOperate)) {
        	buildError("checkData", "不支持的操作！");
        	return false;
        }

        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData() {
    	 
        if ("INSERT||MAIN".equals(mOperate)) {        	
            System.out.println("存储案件备注信息");
            LLCaseDB tLLCaseDB = new LLCaseDB();
            tLLCaseDB.setCaseNo(mLLCaseRemarkSchema.getCaseNo());

            if (!tLLCaseDB.getInfo()) {
                this.mErrors.copyAllErrors(tLLCaseDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LLCaseRemarkBL";
                tError.functionName = "dealData";
                tError.errorMessage = "数据查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            //生成案件记录的流水号，借用案件回退表中的流水号生成规则
            String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
            mCaseRemarkNo = PubFun1.CreateMaxNo("CaseBack", tLimit);
            mLLCaseRemarkSchema.setCaseRemarkNo(mCaseRemarkNo);
            mLLCaseRemarkSchema.setRgtNo(tLLCaseDB.getRgtNo());
            mLLCaseRemarkSchema.setInputerCode(mOperator);
            mLLCaseRemarkSchema.setInputerName(mOperatorName);
            mLLCaseRemarkSchema.setInputerComCode(mManageCom);
            mLLCaseRemarkSchema.setMakeDate(mCurrentDate);
            mLLCaseRemarkSchema.setMakeTime(mCurrentTime);
            mLLCaseRemarkSchema.setModifyDate(mCurrentDate);
            mLLCaseRemarkSchema.setModifyTime(mCurrentTime);
            mLLCaseRemarkSchema.setOperator(mOperator);
            mLLCaseRemarkSchema.setMngCom(mManageCom);
            
            map.put(mLLCaseRemarkSchema, "INSERT");
            
        }
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData() {
        mInputData.clear();
        mInputData.add(map);
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult() {
        return mResult;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLCaseRemarkBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    public static void main(String arg[]) {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "001";    
    }
}
