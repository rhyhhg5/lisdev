package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.xb.XBConst;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: “幸福一家”家庭综合保障计划理赔模块增加保单终止缴费标记</p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author
 * @version 1.0
 * @date
 */

public class LLSpecialWrapDealBL {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;
    private MMap map = new MMap();
    
    /** 关键参数 */
    private String mCaseNo;
    private String BackMsg="";
    private double mSumGetMoney;
    private String mContNo;

    /** 统一更新日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 统一更新时间 */
    private String mCurrentTime = PubFun.getCurrentTime();
    private String mOperator = "";

    public LLSpecialWrapDealBL() {
    }
    
    /**
     * 构造函数
     *
     */
    public LLSpecialWrapDealBL(String aCaseNo,String aOperator,double aSumGetMoney) {
    	this.mCaseNo = aCaseNo;
    	this.mOperator = aOperator;
    	this.mSumGetMoney = aSumGetMoney;
    }

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public boolean submitData(String cOperate) {
        this.mOperate = cOperate;

        // 将外部传入的数据分解到本类的属性中，准备处理
        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        // 根据业务逻辑对数据进行处理
        if (!dealData()) {
            return false;
        }

        // 装配处理好的数据，准备给后台进行保存
        prepareOutputData();

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLSpecialWrapDealBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData() {
                 
        return true;
    }

    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData() {
        if (mCaseNo == null || "".equals(mCaseNo)) {
            buildError("checkData", "案件号为空！");
            return false;
        }

        if (!"DEAL".equals(mOperate)) {
            buildError("checkData", "操作符错误，不支持的操作！");
            return false;
        }
        
        //1. 赔付结论及保单险种信息
        ExeSQL tExeSQL = new ExeSQL();
        LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
        LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
        LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
        tLLClaimDetailDB.setCaseNo(mCaseNo);
        tLLClaimDetailSet = tLLClaimDetailDB.query();
        if(tLLClaimDetailSet.size()<=0){
        	buildError("checkData", "查询赔付结论信息失败！");
            return false;
        }
        
        boolean flagStop=false;
        
        for(int i=1; i<=tLLClaimDetailSet.size(); i++){
        	tLLClaimDetailSchema = tLLClaimDetailSet.get(i);
        	String tGiveType = tLLClaimDetailSchema.getGiveType();
        	
        	//2. 给付结论是“全额拒付”或赔付金额小于等于0
            if(!(mSumGetMoney <= 0 || "3".equals(tGiveType))){
            	//3. 确认该险种是否为约定险种，目前支持幸福一家（WR0296）
            	String tSQL = "";
                if ("00000000000000000000".equals(tLLClaimDetailSchema.getGrpContNo())) {
                    tSQL =
                             "select riskwrapcode from lcriskdutywrap where contno='" +
                             tLLClaimDetailSchema.getContNo() +
                             "' and riskcode='" +
                             tLLClaimDetailSchema.getRiskCode() +
                             "' fetch first 1 rows only";
                }else{
                	return false;
                } 
                
                //#2621,若保单其中任意一个被保险人发生重大疾病保险金、疾病身故保险金、意外身故、伤残保险金赔付的，
                //在案件给付确认时，系统自动增加保单终止缴费标记，实现该保单不能自动续保。
                //判断保单业务
                if(!flagStop){
                	if("303201,303202,356201,356202".indexOf(tLLClaimDetailSchema.getGetDutyCode())>-1){
                			flagStop=true;
                			//System.out.println("-----------成功");
                	}
                }
                		
                
                SSRS tSSRS = tExeSQL.execSQL(tSQL);
                if (tSSRS.getMaxRow() > 0) {
                    String tWrapCode = tSSRS.GetText(1, 1);
                    if("WR0296".equals(tWrapCode)){
                    	mContNo = tLLClaimDetailSchema.getContNo();
                    	//4. 是否已经添加了“终止续保”的标记
                    	String tCheckSql = "select 1 from lcpol where polno='"+tLLClaimDetailSchema.getPolNo()+"'" +
                    			" and (polstate is null or (polstate is not null and polstate not like '02%' and polstate not like '03%')) with ur";
                    	String tPolState = tExeSQL.getOneValue(tCheckSql);
                    	if(!"1".equals(tPolState)){//已经添加
                    		return false;
                    	}
                    	
                    }else{
                    	return false;
                    }
                    
                }            	
            }
        }  
        if(!flagStop){
        	return false;
        }
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData() {
    	 
        if ("DEAL".equals(mOperate)) {
        	
        	if("".equals(mContNo) || mContNo == null)
        	{
        		buildError("checkData", "查询保单信息失败！");
                return false;
        	}
        	String tSQL = "update LCPol set polState = '" + XBConst.POLSTATE_LPXBSTOP + "'," +
        			"operator='"+mOperator+"',modifydate='"+mCurrentDate+"'," +
        			"modifytime='"+mCurrentTime+"' "
                    + "where contno = '" + mContNo + "' ";
        	//BackMsg = "保单"+mContNo+"由于责任出险，该保单终止续保！";
            map.put(tSQL, "UPDATE");
        }

        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData() {
        mInputData.clear();
        mInputData.add(map);
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult() {
        return mResult;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLExemptionBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    public static void main(String arg[]) {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "001";       
    }

	public String getBackMsg() {
		return BackMsg;
	}

	public void setBackMsg(String backMsg) {
		BackMsg = backMsg;
	}
}
