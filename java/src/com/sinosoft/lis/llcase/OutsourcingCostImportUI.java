package com.sinosoft.lis.llcase;

import java.util.Iterator;
import java.util.List;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LLOutcoucingAverageSchema;
import com.sinosoft.lis.schema.LLOutsourcingTotalSchema;
import com.sinosoft.lis.vschema.LLOutcoucingAverageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
public class OutsourcingCostImportUI {
	
	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private String mBatchNo;
    private String mRiskCode;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
//  接收类（天津外包费用总额）
	LLOutsourcingTotalSchema mLLOutsourcingTotalSchema   = new LLOutsourcingTotalSchema();
	//	接收类（本保费分摊费用）
	LLOutcoucingAverageSchema mLLOutcoucingAverageSchema   = new LLOutcoucingAverageSchema();
	
	LLOutcoucingAverageSet tLLOutcoucingAverageSet= new LLOutcoucingAverageSet();
	
	OutsourcingCostImportBL tOutsourcingCostImportBL= new OutsourcingCostImportBL();
	
	String mCount ;
	
	public String getMCount(){
		return mCount;
	}
	
	   /**
    传输数据的公共方法
    */
   public boolean submitData(VData cInputData, String cOperate) {
	   
	   this.mOperate = cOperate;
       //得到外部传入的数据,将数据备份到本类中
       if (!getInputData(cInputData)) {
           return false;
       }
       //进行业务处理
       if (!dealData()) {
           return false;
       }
       //准备往后台的数据
       if (!prepareOutputData()) {
           return false;
       }
       if(!tOutsourcingCostImportBL.submitData(mInputData, mOperate)){
    	   return false;
       }
       
       
       
       if (tOutsourcingCostImportBL.mErrors.needDealError()) {
           // @@错误处理
           this.mErrors.copyAllErrors(tOutsourcingCostImportBL.mErrors);
           CError tError = new CError();
           tError.moduleName = "OutsourcingCostImportUI";
           tError.functionName = "submitData";
           tError.errorMessage = "数据提交失败!";
           this.mErrors.addOneError(tError);
           return false;
       }
	   return true;
   }
   /**
    * 从输入数据中得到所有对象
    *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    */
	 private boolean getInputData(VData cInputData) {
		 mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
	                "GlobalInput", 0));
		 tLLOutcoucingAverageSet.set((LLOutcoucingAverageSet)cInputData.getObjectByObjectName(
	                "LLOutcoucingAverageSet", 0));
		 
		
		 if (mGlobalInput == null) {
	            // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "OutsourcingCostImportUI";
	            tError.functionName = "getInputData";
	            tError.errorMessage = "没有得到足够的信息！";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
		 return true;
	 }
	 
	 /**
	     * 根据前面的输入数据，进行UI逻辑处理
	     * 如果在处理过程中出错，则返回false,否则返回true
	     */
	    private boolean dealData() {
	    	
	    	
	    	return true;
	    }
	  /**
	     * 准备往后层输出所需要的数据
	     * 输出：如果准备数据时发生错误则返回false,否则返回true
	     */
	    private boolean prepareOutputData() {
	    	 try {
	             mInputData.clear();
	             mInputData.add(this.mGlobalInput);
	             mInputData.add(this.tLLOutcoucingAverageSet);
	         } catch (Exception ex) {
	             // @@错误处理
	             CError tError = new CError();
	             tError.moduleName = "OutsourcingCostImportUI";
	             tError.functionName = "prepareOutputData";
	             tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
	             this.mErrors.addOneError(tError);
	             return false;
	         }
	    	return true;
	    }
	    public String checkGrpContno(List grpContnoList,String RiskCode){
	    	ExeSQL tExeSQL = new ExeSQL();
	 		String Result = "";
	    	for(int i=0;i<grpContnoList.size();i++){
	    		// #4193
	    		String sql = "select GrpContno from LCGrpPol where GrpContno = '"+grpContnoList.get(i)+"' and RiskCode = '"+RiskCode+"' union select Contno from LCPol where Contno = '"+grpContnoList.get(i)+"' and RiskCode = '"+RiskCode+"'";
	    		Result =  tExeSQL.getOneValue(sql);
	    		if(Result==null||"".equals(Result)){
	    			return (String)grpContnoList.get(i);
	    		}
	    	}
	    	return null;
	    }
	    
	   public boolean checkBatchNo(String BatchNo ){
		   //判断如果批次号存在于LLOUTCOUCINGAVERAGE表中则返回false

		   String sql = "select distinct 1 from LLOUTCOUCINGAVERAGE  where BatchNo='"+BatchNo+"' and P1='0'";
		   ExeSQL tExeSQL = new ExeSQL();
		   String Result = "";
		   Result =  tExeSQL.getOneValue(sql);
		   if(Result==null||"".equals(Result))
		   {
			   return true;
		   }
		   return false;
		   
	   }	 
	   //对保单号的长度进行校验
	   public boolean checkBatchNoLength(List batchnoList){
		   for(int i =0;i<batchnoList.size();i++){
			  
			   if(batchnoList.get(i).toString().length()>=20){
				   mCount=i+1+"";
				   return false;
			   }
			   
		   }
		   return true;
	   }
	   
/*	    
	   public boolean checkGrpContno(String BatchNo ,List grpContnoList,String RiskCode){
		   //首先对grpContnoList中的grpContno进行数据库中的存在校验
		  Iterator grpConno = grpContnoList.iterator();
		 if(RiskCode==""||RiskCode==null){
		   // 然后在lcgrpcont校验存在性，如果不存在则返回false（正确的保单号是存在的）
		   while(grpConno.hasNext()){
				this.mBatchNo=BatchNo;
				String grpconno = (String)grpConno.next();
	
				String LCsql = "select 1 from lcgrpcont where  grpcontno='"+grpconno+"'";
				ExeSQL tExeSQL = new ExeSQL();
				String Result = "";
				Result =  tExeSQL.getOneValue(LCsql);
				if(Result==null||"".equals(Result))
				{
					return false;
				}
		   }
		 }else{
			//最后在lcgrppol中校验存在性
		   //RiskCode不为空，则进行校验RiskCode和grpconno是否对应，如果对应则返回1.(正确的保单号应该是存在)
			   while(grpConno.hasNext()){
				   String grpconno = (String)grpConno.next();
				   this.mRiskCode=RiskCode;
				   String sqlGrp= "select 1 from lcgrppol where RiskCode='"+RiskCode+"' and grpcontno='"+grpconno+"'";
				   ExeSQL tExeSQL = new ExeSQL();
				   String Result = "";
				   Result =  tExeSQL.getOneValue(sqlGrp);
				   if(Result==null||"".equals(Result))
				   {
						return false;
				   }
			   }   
		   }
		
		   return true;
	   }
	   */
}


