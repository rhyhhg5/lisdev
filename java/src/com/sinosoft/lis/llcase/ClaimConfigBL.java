package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: ClaimConfigBL </p>
 * <p>Description: ClaimConfigBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author:Xx
 * @CreateDate：2005-02-18 13:58:55
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Date;

public class ClaimConfigBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();


    /** 全局数据 */
    private GlobalInput mG = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private String strHandler;
    private String SimpleCase = "";
    private FDate fDate = new FDate();

    /** 业务处理相关变量 */
    private LDCode1Set mLDCode1Set = new LDCode1Set();

    public ClaimConfigBL() {
    }

    public static void main(String[] args) {

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * @cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError.buildErr(this, "数据处理失败ClaimConfigBL-->dealData!");
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        System.out.println("Start ClaimConfigBL Submit...");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            CError.buildErr(this, "数据提交失败!");
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mLDCode1Set = (LDCode1Set) cInputData.
                      getObjectByObjectName("LDCode1Set", 0);
        this.mG.setSchema((GlobalInput) cInputData.
                          getObjectByObjectName("GlobalInput", 0));
        System.out.println(mLDCode1Set.size());
        if (mLDCode1Set.size() <= 0) {
            CError.buildErr(this, "数据提交失败!");
            return false;
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {

        /*************************工用********************************/
        String tSql =
                "delete from ldcode1 where codetype='llimportfeeitem' and code='" +
                mLDCode1Set.get(1).getCode() + "'";
        map.put(tSql, "DELETE");
        //将申请原因填充
        if (mLDCode1Set != null && mLDCode1Set.size() > 0) {
            for (int i = 1; i <= mLDCode1Set.size(); i++) {
                LDCodeDB tLDCodeDB = new LDCodeDB();
                tLDCodeDB.setCodeType("claimreasoncode");
                tLDCodeDB.setCode(mLDCode1Set.get(i).getCode1());
                if (!tLDCodeDB.getInfo()) {
                    CError tError = new CError();
                    tError.moduleName = "ClaimConfigBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "查询配置项出错";
                    this.mErrors.addOneError(tError);
                }
                System.out.println(tLDCodeDB.getCodeName());
                mLDCode1Set.get(i).setCodeName(tLDCodeDB.getCodeAlias().trim());
                mLDCode1Set.get(i).setCodeAlias(tLDCodeDB.getCodeName());
                mLDCode1Set.get(i).setRiskWrapPlanName(mG.Operator);
                mLDCode1Set.get(i).setComCode(mG.ManageCom);
                mLDCode1Set.get(i).setOtherSign(tLDCodeDB.getOtherSign());
            }
            map.put(mLDCode1Set, "INSERT");

            LDComDB tLDComDB = new LDComDB();
            tLDComDB.setComCode(mLDCode1Set.get(1).getCode());
            tLDComDB.getInfo();
            String tComName = tLDComDB.getName().trim(); //提取机构代码对应的机构名称
            String tXmlName = "LL" +
                              mLDCode1Set.get(1).getCode().substring(2, 4) +
                              "CaseImport.xml"; //配置的xml文件名称

            LDCode1Schema tLDCode1Schema = new LDCode1Schema();
            LDCode1DB tLDCode1DB = new LDCode1DB();
            tLDCode1DB.setCodeType("llimport");
            tLDCode1DB.setCode(mLDCode1Set.get(1).getCode().substring(2, 4));
            tLDCode1DB.getInfo();
            tLDCode1Schema = tLDCode1DB.getSchema();
            tLDCode1Schema.setCodeType("llimport");
            tLDCode1Schema.setCode(mLDCode1Set.get(1).getCode().substring(2, 4));
            tLDCode1Schema.setCode1("SI");
            tLDCode1Schema.setCodeName(tComName + "导入");
            tLDCode1Schema.setCodeAlias(tXmlName);
            tLDCode1Schema.setComCode(mLDCode1Set.get(1).getCode());
            tLDCode1Schema.setOtherSign(mLDCode1Set.get(1).getCode().substring(
                    2, 4));
            map.put(tLDCode1Schema, "DELETE&INSERT");
        }
        //该方法公用

        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            mInputData.add(map);
            mResult.clear();
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDDrugBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    public VData getResult() {
        return this.mResult;
    }
}
