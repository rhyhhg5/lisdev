package com.sinosoft.lis.llcase;
 

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.axis2.AxisFault;
import org.jdom.Document;
import org.jdom.Element;
import org.w3c.dom.Node;

import com.sinosoft.lis.llcase.HttpClientTest;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.taskservice.TaskThread;
import com.sinosoft.lis.vschema.LDSysVarSet;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/**
 * <p>
 * Title: 自动立案理算
 * </p>
 * 
 * <p>
 * Description: 生成案件至理算状态
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2007
 * </p>
 * 
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class YKTForward extends TaskThread {
	private String mCurrentDate = PubFun.getCurrentDate();
	private String mFilePath = null;
    
	public YKTForward() {
	}

	/**
	 * 解析报文主要部分
	 * 
	 * @param cInXml
	 *            Document
	 * @return boolean
	 */
	public boolean  createDomData() {
		Document tDocument = null;
		String makedate="current date - 1 day"; //定义查询SQL变量
//		String makedate="'2017-04-18'"; //定义查询SQL变量
		
		String riskcode="'162401'";
		try {
			System.out.println("YKTForward--createDomData--");
			
			 
			Element tRootData = null;
			// XML 信息描述部分
			tRootData = new Element("XMLTable");
			// XML Head部分
			Element tHeadData = new Element("XMLCom");
			Element tRequestType = new Element("BUSINESSID");
			tRequestType.setText("G0001"); // 这个地方仍需调整
			tHeadData.addContent(tRequestType);
			Element tRequestCode = new Element("SYSCODE");
			tRequestCode.setText("CORESYSTEM"); // 这个地方仍需调整
			tHeadData.addContent(tRequestCode);
			
			Element tBodyData = new Element("XMLRecs");

			System.out.println("YKTForward--createJDomData");
			String tSQL = " select a.customername 客户姓名,a.customerno 客户号, "  
				+ " a.idtype 证件类型, " 
				+ " a.idno 证件号, " 
				+ " (select birthday from ldperson where customerno =a.customerno fetch first row only) 出生日期, " 
				+ " b.grpcontno 团单号,b.contno 保单号,b.riskcode 险种编码,a.caseno 案件号, " 
				+ " (select accdate from LLSubReport where subrptno in(select subrptno from llcaserela where caseno=a.caseno) fetch first row only) 出险日期, " 
				+ " sum(b.realpay), " 
				+ " (select HOSPITALNAME from llcasecure where caseno=a.caseno fetch first row only) 医院名称, " 
				+ " (select HOSPITALcode from llcasecure where caseno=a.caseno fetch first row only) 医院编码 " 
				+ " from llcase a ,llclaimdetail b,ljaget c " 
				+ " where a.caseno=b.caseno  " 
				+ " and a.rgtstate in ('11','12') "
				+ " and (a.caseno = c.otherno or a.rgtno=c.otherno) " 
				+ " and c.othernotype='5' " 
				+ " and b.riskcode in ("+riskcode+")  " 
//				+ " and c.makedate < current date - 1 days//通知日期为上报的前一天 " 
				+ " and c.makedate = "+makedate+" "
				+ " group by a.customername,a.customerno,a.idtype ,a.idno,b.grpcontno,b.contno,b.riskcode,a.caseno "
				;
			System.out.println("查询" + mCurrentDate + "前一天数据========" + tSQL);
			SSRS tSSRS = new SSRS();
			ExeSQL tExeSQL = new ExeSQL();
			tSSRS = tExeSQL.execSQL(tSQL);

			
			String pattern = "yyyyMMddHHmmssSSS";
	        SimpleDateFormat df = new SimpleDateFormat(pattern);
	        Date today = new Date();
	        String tString = df.format(today);
	        String fileName=tString;
			
	         
	        
			System.out.println("文件名，年月日时分秒毫秒："+fileName);
			
			if (tSSRS != null && tSSRS.getMaxRow() > 0) {
				//客户号
				
				//保单号
				
				for (int index = 1; index <= tSSRS.getMaxRow(); index++) {
					Element tXMLRec = new Element("XMLRec");
					
					Element NAME = new Element("NAME");
					NAME.setText(tSSRS.GetText(index, 1));
					tXMLRec.addContent(NAME);

					Element CUSTOMERCODE = new Element("CUSTOMERCODE");
					CUSTOMERCODE.setText(tSSRS.GetText(index, 2));
					tXMLRec.addContent(CUSTOMERCODE);
					 
					Element IDCARDTYPE = new Element("IDCARDTYPE");
					IDCARDTYPE.setText(tSSRS.GetText(index, 3));
					tXMLRec.addContent(IDCARDTYPE);
					
					Element IDCARD = new Element("IDCARD");
					IDCARD.setText(tSSRS.GetText(index, 4));
					tXMLRec.addContent(IDCARD);
					
					Element BIRTHDAY = new Element("BIRTHDAY");
					BIRTHDAY.setText(tSSRS.GetText(index, 5));
					tXMLRec.addContent(BIRTHDAY);
					
					Element GROUPCONTNO = new Element("GROUPCONTNO");
					GROUPCONTNO.setText(tSSRS.GetText(index, 6));
					tXMLRec.addContent(GROUPCONTNO);
					
					Element CONTNO = new Element("CONTNO");
					CONTNO.setText(tSSRS.GetText(index, 7));
					tXMLRec.addContent(CONTNO);
					
					Element RISKCODE = new Element("RISKCODE");
					RISKCODE.setText(tSSRS.GetText(index, 8));
					tXMLRec.addContent(RISKCODE);
					
					Element CASENO = new Element("CASENO");
					CASENO.setText(tSSRS.GetText(index, 9));
					tXMLRec.addContent(CASENO);
					//给付责任
					String SQL=" select distinct b.getdutycode "
						+ " from llcase a ,llclaimdetail b,ljaget c "
						+ " where a.caseno=b.caseno   "
						+ " and (a.caseno = c.otherno or a.rgtno=c.otherno)  "
						+ " and a.rgtstate in ('11','12') "
						+ " and c.othernotype='5'  "
						+ " and b.riskcode in ("+riskcode+")  " 
//						+ " and c.makedate < current date - 1 days//通知日期为上报的前一天 " 
						+ " and c.makedate = "+makedate+" "
						+ " and b.caseno='"+tSSRS.GetText(index, 9)+"' "
						+ " and a.customerno='"+tSSRS.GetText(index, 2)+"' "
						+ " and b.contno='"+tSSRS.GetText(index, 7)+"' "
						+ " and  b.grpcontno='"+tSSRS.GetText(index, 6)+"' ";
					System.out.println("给付责任SQL:"+SQL);
					SSRS ss=new SSRS();
					ExeSQL ex= new ExeSQL();
					ss=ex.execSQL(SQL);
					Element RESPONSIBILITYCODE = new Element("RESPONSIBILITYCODE");
					String getdutycodes="";
					if(ss!=null && ss.getMaxRow()>0){
						if(ss.getMaxRow()==1){
							//只有1行且不为空
							System.out.println("1个责任编码："+ss.GetText(1, 1));
							RESPONSIBILITYCODE.setText(ss.GetText(1, 1));
							tXMLRec.addContent(RESPONSIBILITYCODE);
						}else{
							//为多行且不为空
							
							for (int i = 1; i <= ss.getMaxRow(); i++) {
								String getdutycode=ss.GetText(i, 1);
								if(i==ss.getMaxRow()){
									getdutycodes=getdutycodes+getdutycode+"";
								}else {
									getdutycodes=getdutycodes+getdutycode+",";
								}
							}
							System.out.println("多个责任编码："+getdutycodes);
							RESPONSIBILITYCODE.setText(getdutycodes.trim());
							tXMLRec.addContent(RESPONSIBILITYCODE);
						}
					}else{
						System.out.println("封装责任异常，未查询到责任");
						return false;
					}
					
					
					
					
					Element CLAIMDATE = new Element("CLAIMDATE");
					CLAIMDATE.setText(tSSRS.GetText(index, 10));
					tXMLRec.addContent(CLAIMDATE);
					
					Element PAYMENTMONEY = new Element("PAYMENTMONEY");
					PAYMENTMONEY.setText(tSSRS.GetText(index, 11));
					tXMLRec.addContent(PAYMENTMONEY);
					
					//账户余额
					
					 
					String SQLmon=" select distinct b.polno ,c.makedate "
						+ " from llcase a ,llclaimdetail b,ljaget c "
						+ " where a.caseno=b.caseno   "
						+ " and (a.caseno = c.otherno or a.rgtno=c.otherno)  "
						+ " and a.rgtstate in ('11','12') "
						+ " and c.othernotype='5'  "
						+ " and b.riskcode in ("+riskcode+")  " 
//						+ " and c.makedate < current date - 1 days//通知日期为上报的前一天 " 
						+ " and c.makedate = "+makedate+" "
						+ " and b.caseno='"+tSSRS.GetText(index, 9)+"' "
						+ " and a.customerno='"+tSSRS.GetText(index, 2)+"' "
						+ " and b.contno='"+tSSRS.GetText(index, 7)+"' "
						+ " and  b.grpcontno='"+tSSRS.GetText(index, 6)+"' ";
					System.out.println("账户余额SQL:"+SQLmon);
					SSRS sspol=new SSRS();
					sspol=ex.execSQL(SQLmon);
					Element ACCOUNTMONEY = new Element("ACCOUNTMONEY");
					LLCaseCommon llcc = new LLCaseCommon();
					if(sspol!=null && sspol.getMaxRow()>0){
						if(sspol.getMaxRow()==1){
							//只有1行且不为空
							System.out.println("1个pol："+sspol.GetText(1, 1));
							String cus=tSSRS.GetText(index, 2);
							String pol=sspol.GetText(1, 1);
							String mak=sspol.GetText(1, 2);
							Double Tmoney=llcc.getAmntFromAcc(cus,pol,mak,"CB") ;
							System.out.println("1个pol的金额："+Tmoney);
							ACCOUNTMONEY.setText(Tmoney.toString());
							tXMLRec.addContent(ACCOUNTMONEY);
						}else{
							//为多行且不为空
							Double tmoneys=0.0;
							for (int i = 1; i <= sspol.getMaxRow(); i++) {
								
								Double tmoney=llcc.getAmntFromAcc(tSSRS.GetText(index, 2),sspol.GetText(i, 1),sspol.GetText(i, 2),"CB") ;
								System.out.println("多个pol的情况，先算单个pol："+sspol.GetText(i, 1)+",金额："+tmoney);
								tmoneys=tmoneys+tmoney;
							}
							System.out.println("多个pol的总金额："+tmoneys);
							ACCOUNTMONEY.setText(tmoneys.toString());
							tXMLRec.addContent(ACCOUNTMONEY);
						}
					}else{
						System.out.println("封装个人账户余额异常，未查询到polno");
						return false;
					}
					
					Element HOSPITALNAME = new Element("HOSPITALNAME");
					HOSPITALNAME.setText(tSSRS.GetText(index, 12));
					tXMLRec.addContent(HOSPITALNAME);
					
					Element HOSPITALCODE = new Element("HOSPITALCODE");
					HOSPITALCODE.setText(tSSRS.GetText(index, 13));
					tXMLRec.addContent(HOSPITALCODE);
					
					 String SQLduty=" select "

					 +" distinct b.getdutycode    "

					 +" from llcase a ,llclaimdetail b,ljaget c  "
					 +" where a.caseno=b.caseno   "
					 +" and (a.caseno = c.otherno or a.rgtno=c.otherno)  "
					 + " and a.rgtstate in ('11','12') "
					 +" and a.caseno='"+tSSRS.GetText(index, 9)+"' "
					 +" and c.othernotype='5'  "
					 + " and b.riskcode in ("+riskcode+")  " 
//					 + " and c.makedate < current date - 1 days//通知日期为上报的前一天 " 
					 + " and c.makedate = "+makedate+" ";
					 System.out.println("责任编码SQL:"+SQLduty);
					 SSRS ssduty=new SSRS();
					 ssduty=ex.execSQL(SQLduty);
					 boolean falses=false; //用来判断是否包括身故给付责任
					 System.out.println(ssduty.getMaxRow());
					if(ssduty!=null && ssduty.getMaxRow()>0){
						for (int i = 1; i <= ssduty.getMaxRow(); i++) {
							System.out.println("责任编码："+ssduty.GetText(i, 1));
							if(ssduty.GetText(i, 1).equals("741204") || ssduty.GetText(i, 1)=="741204"){
								//责任终止 当这个案件的给付责任中包含身故给付责任，则传‘责任终止’，否则为空 
								 //疾病身故保险金  给付责任编码  getdutycode ='741204'  
								falses=true;
							}
						}
					}
					
					if(falses){
						Element RESPONSIBILITYCESSER = new Element("RESPONSIBILITYCESSER");
						RESPONSIBILITYCESSER.setText("0");
						tXMLRec.addContent(RESPONSIBILITYCESSER);
					}else{
						Element RESPONSIBILITYCESSER = new Element("RESPONSIBILITYCESSER");
						RESPONSIBILITYCESSER.setText("1");
						tXMLRec.addContent(RESPONSIBILITYCESSER);
					}
					
					tBodyData.addContent(tXMLRec);
					
				}
				tRootData.addContent(tHeadData);
				tRootData.addContent(tBodyData);
				tDocument=new Document(tRootData);
				if(tDocument==null){
					System.out.println("封装发送报文为空");
					return false;
				}
//				//文件保存
				 LDSysVarDB tLDSysVarDB = new LDSysVarDB();
				 tLDSysVarDB.setSysVar("llonecardxml");
				 LDSysVarSet tLDSysVarSet = tLDSysVarDB.query();
				 if(tLDSysVarSet.size()< 1)
				 {
				 System.err.println("报文保存路径查询失败！");
				 return false;
				 }
				 this.mFilePath = tLDSysVarSet.get(1).getSysVarValue(); //文件保存路径
//				 this.mFilePath="D://onecard/"+PubFun.getCurrentDate().toString().replaceAll("-", "")+"/";
				 System.out.println("调用一卡通接口的发送报文保存路径："+this.mFilePath );

				if (!saveInput(tDocument, fileName)) {
					System.out.println("保存发送报文时失败");
					return false;
				}

				 String tDoc = JdomUtil.outputToString(tDocument);
				 if("".equals(tDoc) || tDoc==null || tDoc==""){
					 System.out.println("将org.jdom.Document格式报文转换成String后的结果为空");
				 return false;
				 }
				 System.out.println("YKTForward==========发送报文："+tDoc);
				 tDoc=tDoc.replace("<?xml version=\"1.0\" encoding=\"GBK\"?>", "");
				 //cxf报送
				HttpClientTest hct=new HttpClientTest();
				String tOutStr =hct.getResponse(tDoc);
				tOutStr=tOutStr.replace("&lt;", "<");
				tOutStr=tOutStr.replace("&gt;", ">");
				tOutStr=tOutStr.replace("&quot;", "\"");
				tOutStr=tOutStr.replace("&amp;", "&");
				
				int sta=tOutStr.lastIndexOf("<return>");
				int end=tOutStr.lastIndexOf("</return>");
				
				tOutStr=tOutStr.substring(sta+8, end);
				if("".equals(tOutStr) || tOutStr==""){
					System.out.println("返回报文为空");
					return false;
				}
				System.out.println("YKTForward==========处理后的返回报文："+tOutStr);
				 if(("").equals(tOutStr) || tOutStr=="" ){
					 System.err.println("返回报文为空");
					 return false;
				 }

				 Document tOutXmlDoc = JdomUtil.buildFromXMLString(tOutStr);
				 if (tOutXmlDoc == null) {
				 System.err.println("将String报文转换成org.jdom.Document后的结果为空");
				 return false;
				 }else{
					 if(!saveOutput(tOutXmlDoc,fileName)){
					 System.out.println("保存返回报文时失败");
					 return false;
					 }else{
						 int sta1=tOutStr.lastIndexOf("<RESULTFLAG>");
						 int end1=tOutStr.lastIndexOf("</RESULTFLAG>");
						 String  rescode=tOutStr.substring(sta1+12, end1);
						 System.out.println("YKTForward==========一卡通返回编码："+rescode);
						 if("000000".equals(rescode) || rescode=="000000"){
							 System.out.println("YKTForward==========处理成功");
							 return true;
						 }else{
							 System.out.println("YKTForward==========处理失败");
							 return false;
						 }
					 }
				 }
				
			} else {
				System.out.println("未查询到核心系统理赔数据");
				return false;
			}
		} catch (Exception ex) {
			System.err.println("封装发送报文异常！");
			ex.printStackTrace();
			return false;
		}
	}

	 
	
	
	public boolean saveInput(Document pXmlDoc, String mTranscationNum) {
		String pName = mTranscationNum  + ".xml";
		StringBuffer mFilePath = new StringBuffer();
		mFilePath.append(this.mFilePath);
		mFilePath.append("request/");
		System.out.println(mFilePath);

		File mFileDir = new File(mFilePath.toString());

		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建请求报文目录失败！" + mFileDir.getPath());
			}
		}
		File tempFile=new File(mFilePath.toString() + pName);
		System.out.println("请求报文文件地址:" + tempFile);
		if (!tempFile.exists()) {
			try {
				if (!tempFile.createNewFile()) {
					System.err.println("创建请求报文文件失败！" + tempFile.getPath());
				}
			} catch (Exception ex) {
				System.err.println("创建请求报文文件时出现异常！" + tempFile.getPath());
				ex.printStackTrace();
			}

		}
		try {
			FileOutputStream tFos = new FileOutputStream(mFilePath.toString()+ pName);
			JdomUtil.output(pXmlDoc, tFos);
			tFos.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			return false;

		}
		return true;
	}

	public boolean saveOutput(Document pXmlDoc, String mTranscationNum ) {
		String pName = mTranscationNum  + ".xml";
		StringBuffer mFilePath = new StringBuffer();
		mFilePath.append(this.mFilePath);
		mFilePath.append("response/");
		File mFileDir = new File(mFilePath.toString());
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建返回报文的目录失败！" + mFileDir.getPath());
			}
		}
		File tempFile = new File(mFilePath.toString() + pName);
		System.out.println("返回报文文件地址:"+tempFile);
		if (!tempFile.exists()) {
			try {
				if (!tempFile.createNewFile()) {
					System.err.println("创建返回报文文件失败！" + tempFile.getPath());
				}
			} catch (Exception ex) {
				System.err.println("创建返回报文文件时出现异常！" + tempFile.getPath());
				ex.printStackTrace();
			}

		}
		try {
			FileOutputStream tFos = new FileOutputStream(mFilePath.toString()
					+ pName);
			JdomUtil.output(pXmlDoc, tFos);
			tFos.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			return false;

		}
		return true;
	}

	/**
	 * 1、客户端准备数据;2、调用WebService服务;3、获取服务端处理结果数据
	 * 
	 * @param pReader
	 * @param pServiceURL
	 * @return org.w3c.dom.Document
	 * @throws AxisFault
	 * @throws Exception
	 */
//	public String callService(String tDoc){
//		
//System.out.println("准备调用一卡通，发送报文："+tDoc);
////		webservice 地址
////		String  tStrTargetEendPoint = "http://10.252.130.99:8080/wasforwardxml/services/ComplexServiceProxy";
//		String  tStrTargetEendPoint ="http://10.252.18.1:7001/onecardws/services/vipservice?wsdl";
//		String tStrNamespace = "http://webservice.onecard.neusoft.com/";
//		
//		try {
//			RPCServiceClient client = new RPCServiceClient();
//			// 向Soap Header中添加校验信息  
//			client.addHeader(HeaderOMElement.createHeaderOMElement());
//			EndpointReference erf = new EndpointReference(tStrTargetEendPoint);
//			Options option = client.getOptions();
//			option.setTo(erf);
//			option.setAction("");//调用方法名
//			QName name = new QName(tStrNamespace, "VIPClaimData");//调用方法名
//			Object[] object = new Object[] { tDoc };
//			Class[] returnTypes = new Class[] { String.class };
//			Object[] response = client.invokeBlocking(name, object, returnTypes);
//			String result = (String) response[0];
//			System.out.println("一卡通返回报文:" + result);
//			return result;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return "发送请求报文过程异常";
//		}
//	}

	public boolean runSer() {
		if (createDomData()) {
			System.out.println("调用一卡通远程service接口成功完成");
		}else{
			System.out.println("调用一卡通远程service接口异常");
			return false;
		}
		return true;
	}

	/**
	 * 将org.w3c.dom.Document转化为文件输出流
	 * 
	 * @param pNode
	 * @param pOutputStream
	 * @throws Exception
	 */
	public void outputDOM(Node pNode, OutputStream pOutputStream)
			throws Exception {
		DOMSource mDOMSource = new DOMSource(pNode);

		StreamResult mStreamResult = new StreamResult(pOutputStream);

		Transformer mTransformer = cTransformerFactory.newTransformer();
		mTransformer.setOutputProperty(OutputKeys.INDENT, "yes");
		mTransformer.setOutputProperty(OutputKeys.ENCODING, "GB2312");

		mTransformer.transform(mDOMSource, mStreamResult);
	}

	private TransformerFactory cTransformerFactory = TransformerFactory
			.newInstance();

	public static void main(String[] args) {
			YKTForward tYKTForward = new YKTForward();
//			String xmldata = "<XMLTable>	<XMLCom>		<BUSINESSID>G0001</BUSINESSID>		<SYSCODE>HEALTHAPP</SYSCODE>	</XMLCom>"+
//			"	<XMLRecs>		<XMLRec>			<NAME>李XX</NAME>			<CUSTOMERCODE>321458</CUSTOMERCODE>			"+"<IDCARDTYPE>01</IDCARDTYPE>			<IDCARD>210112198801020854</IDCARD>			<BIRTHDAY>1988-01-02</BIRTHDAY>			<GROUPCONTNO>45896</GROUPCONTNO>			<CONTNO>4589611</CONTNO>			<RISKCODE>01</RISKCODE>			<CASENO>7895461</CASENO>			<RESPONSIBILITYCODE>01</RESPONSIBILITYCODE>			<CLAIMDATE>2017-03-26 13:10:11</CLAIMDATE>			<PAYMENTMONEY>2000.00</PAYMENTMONEY>			<ACCOUNTMONEY>1000.00</ACCOUNTMONEY>			<HOSPITALNAME>盛京医院</HOSPITALNAME>			<HOSPITALCODE>001</HOSPITALCODE>			<RESPONSIBILITYCESSER>责任终止</RESPONSIBILITYCESSER>		</XMLRec>	"+
//			"	<XMLRec>			<NAME>张XX</NAME>			<CUSTOMERCODE>3958447</CUSTOMERCODE>			<IDCARDTYPE>01</IDCARDTYPE>			<IDCARD>210112198701120014</IDCARD>			<BIRTHDAY>1987-01-12</BIRTHDAY>			<GROUPCONTNO>123456</GROUPCONTNO>			<CONTNO>456987</CONTNO>			<RISKCODE>01</RISKCODE>			<CASENO>7411251</CASENO>			<RESPONSIBILITYCODE>01</RESPONSIBILITYCODE>			<CLAIMDATE>2017-03-26 13:10:11</CLAIMDATE>			<PAYMENTMONEY>2000.00</PAYMENTMONEY>			<ACCOUNTMONEY>1000.00</ACCOUNTMONEY>			<HOSPITALNAME>盛京医院</HOSPITALNAME>			<HOSPITALCODE>001</HOSPITALCODE>			<RESPONSIBILITYCESSER>责任终止</RESPONSIBILITYCESSER>		</XMLRec>	</XMLRecs></XMLTable>";
//			System.out.println("&lt");  
			tYKTForward.runSer(); 
			
	}
}