package com.sinosoft.lis.llcase;

import java.util.Vector;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.bq.EdorCalZT;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsureAccDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LLCaseCureDB;
import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.db.LLClaimUserDB;
import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.lis.db.LMDutyGetClmDB;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCInsureAccFeeTraceSchema;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.lis.schema.LCInsureAccTraceSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.schema.LGErrorLogSchema;
import com.sinosoft.lis.schema.LJSGetEndorseSchema;
import com.sinosoft.lis.schema.LPBudgetResultSchema;
import com.sinosoft.lis.vschema.LCInsureAccFeeTraceSet;
import com.sinosoft.lis.vschema.LCInsureAccTraceSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.lis.vschema.LGErrorLogSet;
import com.sinosoft.lis.vschema.LJSGetEndorseSet;
import com.sinosoft.lis.vschema.LLCaseCureSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LPBudgetResultSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LLContDealCDBudgetBL {
	private String mContNo = "";
	private String mRiskCode = "";
	private String mEdorNo = "";
	private String mEdorType = "";
	private  String mEdorValiDate = "";
	private String mPolNo = "";
	private String mCaseNo = "";
	
	public double rValue = 0.00;
	 /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 全局变量 */
    private GlobalInput mGlobalInput;
    private VData mResult = new VData();
    private VData mInputData = new VData();
   // MMap map = new MMap();
    private LPBudgetResultSchema mLPBudgetResultSchema = null;  //试算结果
    private LPBudgetResultSet mLPBudgetResultSet = null;
  //  private LPEdorEspecialDataSchema mLPEdorEspecialDataSchema=null;//试算结果直接保存为结果
  //  private LPEdorEspecialDataSet mLPEdorEspecialDataSet=null;
    /**
     * 导入入口
     * @param path String
     * @param fileName String
     */
    public boolean submitData(VData mVData, String cOperate) {
        System.out.println("submitData Begin....");
        mInputData = (VData)mVData.clone() ;
        if (!getInputData(mVData))
            return false;
        if(!checkData())
            return false;

        if(!getGetMoney())
        {
        	return false;
        }
        if (!getLPBudgetResult()) {
        	System.out.println("LPBudgetResult更新失败！"   );
            return buildErr("submitData","LLRegister数据更新失败！");
        }
        return true;
    }
    /**
     * 从save页面获取数据
     * @param map MMap
     * @return boolean
     */
    private boolean getInputData(VData mInputData) {
        System.out.println("getInputData ......");
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        TransferData tTransferData = new TransferData();
        tTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData",0);
        mContNo = (String)mInputData.get(0);
        mRiskCode = (String)mInputData.get(1);
        mEdorNo=(String)mInputData.get(2);
        mEdorType=(String)mInputData.get(3);
        mEdorValiDate=(String)mInputData.get(4);
        mPolNo=(String)mInputData.get(5);
        mCaseNo=(String)mInputData.get(6);
 
        return true;
    }

	   /**
     * 处理前检查数据是否合理
     * @return boolean
     */
    private boolean checkData() {
    	if("".equals(this.mRiskCode)){
    		System.out.println("mRiskCode:" + mRiskCode + "险种编码为空"  );
    		  return buildErr("checkData","险种编码为空!");
    	  }
    	if("".equals(this.mContNo)){
    		System.out.println("mContNo:" + mContNo + "保单号为空"  );
    		  return buildErr("checkData","保单号为空!");
    	  }
    	if("".equals(this.mEdorNo)){
    		System.out.println("mEdorNo:" + mEdorNo + "工单号为空"  );
    		  return buildErr("checkData","工单号为空!");
    	  }
        return true;
    }
	public boolean getGetMoney(){
		
		
		LDCode1DB tLDCode1DB = new LDCode1DB();
        tLDCode1DB.setCodeType("LPCD");
        tLDCode1DB.setCode(mRiskCode);
        LDCode1Set tLDCode1Set =  tLDCode1DB.query();
        Calculator mCalculator = new Calculator();
		for(int j = 1 ; j <= tLDCode1Set.size(); j++) {
        	LDCode1Schema tLDCode1Schema = tLDCode1Set.get(j);
            mCalculator.setCalCode(tLDCode1Schema.getCode1());
            System.out.println("longlong");
//          增加基本要素,计算给付金
            TransferData tTransferData = new TransferData();
//          个人保单号
            tTransferData.setNameAndValue("ContNo", String.valueOf(mContNo));
//          个人险种号
            tTransferData.setNameAndValue("PolNo", String.valueOf(mPolNo));
//          部分领取总金额
            tTransferData.setNameAndValue("PartGetMoney", String.valueOf(getPartGetMoney()));
//          账户现金价值
            tTransferData.setNameAndValue("AccValue",String.valueOf(getAccValue()));
//          是否失能
            tTransferData.setNameAndValue("DisabilityFlag", String.valueOf(getDisabilityFlag()));
            Vector tv = tTransferData.getValueNames();
            for (int i = 0; i < tv.size(); i++) {
            	String tName = (String) tv.get(i);
                String tValue = (String) tTransferData.getValueByName(tName);
                System.out.println("tName:" + tName + "  tValue:" + tValue);
                mCalculator.addBasicFactor(tName, tValue);
            }
            String tStr = "";
            System.out.println("CalSQL=" + mCalculator.getCalSQL());
            tStr = mCalculator.calculate();
            if (tStr.trim().equals("")) {
                rValue = 0;
                System.out.println("rValue=" + rValue);
            } else {
                rValue = Double.parseDouble(tStr);
                System.out.println("rValue=" + rValue);
                return true;
            }
		}
		return true; 
	}
	
	 public boolean getLPBudgetResult()
	    {
		    mLPBudgetResultSet=new LPBudgetResultSet();
            mLPBudgetResultSchema = new LPBudgetResultSchema();
            mLPBudgetResultSchema.setEdorNo(mEdorNo);
            mLPBudgetResultSchema.setEdorType(mEdorType);
            mLPBudgetResultSchema.setGrpContNo("00000000000000000000");
            mLPBudgetResultSchema.setGrpPolNo("00000000000000000000");
            mLPBudgetResultSchema.setContNo(mContNo);
            mLPBudgetResultSchema.setPolNo(mPolNo);
            mLPBudgetResultSchema.setDutyCode("000000");
            mLPBudgetResultSchema.setPayPlanCode("000000");
            mLPBudgetResultSchema.setOperator("LP");
            mLPBudgetResultSchema.setRiskCode(mRiskCode);
            mLPBudgetResultSchema.setLastPayToDate(mEdorValiDate);
            mLPBudgetResultSchema.setCurPayToDate(mEdorValiDate);
            mLPBudgetResultSchema.setEdorValiDate(mEdorValiDate);
            mLPBudgetResultSchema.setFeeFinaType("TB");
            mLPBudgetResultSchema.setGetMoney(rValue);
            String currDate = PubFun.getCurrentDate();
            String currTime = PubFun.getCurrentTime();
            mLPBudgetResultSchema.setBudgetDate(currDate);
            mLPBudgetResultSchema.setBudgetTime(currTime); 
            PubFun.fillDefaultField(mLPBudgetResultSchema);
            
            mLPBudgetResultSet.add(mLPBudgetResultSchema);
            
            
            MMap map = new MMap();
            map.put(mLPBudgetResultSet, "DELETE&INSERT");
            VData tVData = new VData();
            tVData.add(map);

            PubSubmit p = new PubSubmit();
            if (!p.submitData(tVData, "DELETE&INSERT")) {
                mErrors.copyAllErrors(p.mErrors);
            }
         return true;
}
	 public boolean getLPEdorEspecialData()
	    {
		    mLPBudgetResultSet=new LPBudgetResultSet();
         mLPBudgetResultSchema = new LPBudgetResultSchema();
         mLPBudgetResultSchema.setEdorNo(mEdorNo);
         mLPBudgetResultSchema.setEdorType(mEdorType);
         mLPBudgetResultSchema.setGrpContNo("00000000000000000000");
         mLPBudgetResultSchema.setGrpPolNo("00000000000000000000");
         mLPBudgetResultSchema.setContNo(mContNo);
         mLPBudgetResultSchema.setPolNo(mPolNo);
         mLPBudgetResultSchema.setDutyCode("000000");
         mLPBudgetResultSchema.setPayPlanCode("000000");
         mLPBudgetResultSchema.setOperator("LP");
         mLPBudgetResultSchema.setRiskCode(mRiskCode);
         mLPBudgetResultSchema.setLastPayToDate(mEdorValiDate);
         mLPBudgetResultSchema.setCurPayToDate(mEdorValiDate);
         mLPBudgetResultSchema.setEdorValiDate(mEdorValiDate);
         mLPBudgetResultSchema.setFeeFinaType("TB");
         mLPBudgetResultSchema.setGetMoney(rValue);
         String currDate = PubFun.getCurrentDate();
         String currTime = PubFun.getCurrentTime();
         mLPBudgetResultSchema.setBudgetDate(currDate);
         mLPBudgetResultSchema.setBudgetTime(currTime); 
         PubFun.fillDefaultField(mLPBudgetResultSchema);
         
         mLPBudgetResultSet.add(mLPBudgetResultSchema);
         
         
         MMap map = new MMap();
         map.put(mLPBudgetResultSet, "DELETE&INSERT");
         VData tVData = new VData();
         tVData.add(map);

         PubSubmit p = new PubSubmit();
         if (!p.submitData(tVData, "DELETE&INSERT")) {
             mErrors.copyAllErrors(p.mErrors);
         }
      return true;
}
	 /**
	     * 计算部分领取的总金额
	     * @return
	     */
	    private double getPartGetMoney(){
	    	String sql = "select -SUM(MONEY) from lCinsureacctrace where CONTNO='" +mContNo+ "' and moneytype='LQ'" ;
	    	ExeSQL exeSQL = new ExeSQL();
	        String result = exeSQL.getOneValue(sql);
	        if(result!=null && !"".equals(result)){
	    	    return Double.parseDouble(result);
	        }else{
	        	return 0.00;
	        }
	    }
	    
	    /**
	     *
	     * @return 返回是否失能
	     */
	    private String getDisabilityFlag(){
	    	String sql = "select count(1) from lldisability where caseno = '"+mCaseNo+"' and DisabilityFlag = '1'";
	    	 ExeSQL exeSQL = new ExeSQL();
	         String result = exeSQL.getOneValue(sql);
	         if(result.equals("0"))
	         {
	        	 return result;
	         }
	         else
	         {
	        	 result = "1";
	        	 return result;
	         }
	    }
	public String getMContNo() {
		return mContNo;
	}
	public void setMContNo(String contNo) {
		mContNo = contNo;
	}
	public String getMRiskCode() {
		return mRiskCode;
	}
	public void setMRiskCode(String riskCode) {
		mRiskCode = riskCode;
	}
    // @@错误处理
    private boolean buildErr(String FucName,String ErrMsg){
        CError tError = new CError();
        tError.moduleName = "LLContDealCDBudgetBL";
        tError.functionName = FucName;
        tError.errorMessage = ErrMsg;
        this.mErrors.addOneError(tError);
        return false;
    }
	public double getRValue() {
		return rValue;
	}
	public void setRValue(double value) {
		rValue = value;
	}
	public String getMEdorNo() {
		return mEdorNo;
	}
	public void setMEdorNo(String edorNo) {
		mEdorNo = edorNo;
	}
	public String getMEdorType() {
		return mEdorType;
	}
	public void setMEdorType(String edorType) {
		mEdorType = edorType;
	}
	public String getMEdorValiDate() {
		return mEdorValiDate;
	}
	public void setMEdorValiDate(String edorValiDate) {
		mEdorValiDate = edorValiDate;
	}
	public String getMPolNo() {
		return mPolNo;
	}
	public void setMPolNo(String polNo) {
		mPolNo = polNo;
	}
	
	/**
     * 计算万能账户价值
     * @return double
     */
    private double getAccValue() {
        ExeSQL tExeSQL = new ExeSQL();  
        //账户价值
        double tAccValue = 0.0;
        //计算日期
        String tCalDate = "";
        
        LCPolSchema mLCPolSchema = new LCPolSchema();
        String mRiskSQL = "select * from lcpol where polno = '"+mPolNo+"' ";
        LCPolDB mLCPolDB = new LCPolDB();
        LCPolSet mLCPolSet = mLCPolDB.executeQuery(mRiskSQL);
        mLCPolSchema = mLCPolSet.get(1);
        
        //判断是否万能险种，包括附加险
        String tWNRiskCodeSQL = "select riskcode From lmriskapp where riskcode='"
                           + mLCPolSchema.getRiskCode()
                           + "' and risktype4='4' "
                           + " union all select b.code1 from lmriskapp a,ldcode1 b "
                           + " where a.riskcode=b.code1 and b.code='"
                           + mLCPolSchema.getRiskCode()
                           + "' and b.codetype='checkappendrisk' and a.risktype4='4' ";
        String tRiskCode = tExeSQL.getOneValue(tWNRiskCodeSQL);
        if (tRiskCode == null || "".equals(tRiskCode) || "null".equals(tRiskCode)) {
            return 0.0;
        }

        //获得账户polno
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mLCPolSchema.getContNo());
        tLCPolDB.setInsuredNo(mLCPolSchema.getInsuredNo());
        tLCPolDB.setRiskCode(tRiskCode);
        LCPolSet tLCPolSet = tLCPolDB.query();
        if (tLCPolSet.size() < 1) {
            return 0.0;
        }
        LCPolSchema tLCPolSchema = tLCPolSet.get(1);

        //获得险种账户类型
        String tWNAccNoSQL = "select b.insuaccno from lmrisktoacc a,LMRiskInsuAcc b "
                           + " where a.insuaccno = b.insuaccno and a.riskcode='"
                           + tRiskCode + "' and b.acctype='002' ";
        String tInsuAccNo = tExeSQL.getOneValue(tWNAccNoSQL);

        if (tInsuAccNo == null || "".equals(tInsuAccNo) || "null".equals(tInsuAccNo)) {
            return 0.0;
        }

        //查询账户
        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
        tLCInsureAccDB.setPolNo(tLCPolSchema.getPolNo());
        tLCInsureAccDB.setInsuAccNo(tInsuAccNo);
        if (!tLCInsureAccDB.getInfo()) {
            return 0.0;
        }
        LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccDB.getSchema();

        //获取计算日期
        String tSQL = "select rgtdate from llcase where caseno = '"+mCaseNo+"' ";
        tCalDate = new ExeSQL().getOneValue(tSQL);
        //将计算日期赋值到账户表中
        tLCInsureAccSchema.setBalaDate(tCalDate);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tLCInsureAccSchema);
        System.out.println("计算账户:"+tLCInsureAccSchema.getPolNo()
                           + "," + tLCInsureAccSchema.getBalaDate() + "账户价值");
        LLAccValueCal tLLAccValueCal = new LLAccValueCal();
        tLLAccValueCal.setMOtherNO(this.mCaseNo);
        try
        {
            if(!tLLAccValueCal.submitData(tVData, ""))
            {
                CError.buildErr(this, tLLAccValueCal.mErrors.getErrContent());
                return 0.0;
            }

         }catch(Exception ex)
         {
             ex.printStackTrace();
             CError.buildErr(this, "结算出现未知异常");
             return 0.0; 
         }

         System.out.println("计算出的账户价值AccValue=="+tLLAccValueCal.getAccValue());
         tAccValue = Arith.round(tLLAccValueCal.getAccValue(),2);
         return tAccValue; 
    }
    
}
