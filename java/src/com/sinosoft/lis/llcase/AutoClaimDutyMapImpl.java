package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.LCPolBL;

/**
 * <p>Description: 满足多层结构和xml的应用</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: SinoSoft Co. Ltd,</p>
 * @author not attributable
 * @version 1.0
 */
public class AutoClaimDutyMapImpl implements AutoClaimDutyMap {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
//    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    GetDutyGet getDutyGet;

    CErrors mErrors = new CErrors();

    public AutoClaimDutyMapImpl() {
    }

    public LLToClaimDutySet autoChooseGetDuty(String caseNo, String caseRelaNo) {
        try {
            getDutyGet = (GetDutyGet) Class.forName("com.sinosoft.lis.llcase." +
                    SysConst.GETDUTYGET).newInstance();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("加载GetDutyGet实现类错误");
        }

        String tsql = "select b.subrptno,b.accidenttype,b.accdate from "
                      +" llcaserela a,llsubreport b where a.caseno ='"
                      +caseNo+"' and a.caserelano='"+caseRelaNo
                      +"' and b.subrptno=a.subrptno";
        System.out.println(tsql);
        ExeSQL texesql = new ExeSQL();
        SSRS tssrs = new SSRS();
        tssrs = texesql.execSQL(tsql);
        String subrptNo = "";
        String claimCause ="";
        String accdate="";
        if(tssrs.getMaxRow()>0){
            subrptNo = tssrs.GetText(1,1);
            claimCause = tssrs.GetText(1,2);
            accdate = tssrs.GetText(1,3);
        }else{
            mErrors.addOneError("查询关联事件失败！");
        }
        //获取案件原因
        if (claimCause.equals("") || claimCause.equals("null"))
            claimCause = "0";
        String newclaimCause = getCaseReasons(caseNo, caseRelaNo);
        System.out.println("newclaimCause:" + newclaimCause);
        if (!"0".equals(newclaimCause))
            claimCause = newclaimCause;

        System.out.println("ClaimCause:" + claimCause);
        //获取案件结果(申请原因)
        LLAppClaimReasonDB reasonDB = new LLAppClaimReasonDB();
        reasonDB.setCaseNo(caseNo);
        LLAppClaimReasonSet reasonSet = reasonDB.query();
        String reasonList = "''";

        if (reasonSet.size() < 1) {
            System.out.println("没有填写申请原因!");
            mErrors.addOneError("自动责任匹配时错误:没有填写申请原因!");
        }

        for (int i = 1; i <= reasonSet.size(); i++) {
            reasonList += ",'" + reasonSet.get(i).getReasonCode() + "'";
        }
        reasonList += getCaseResult(caseNo, caseRelaNo);

        LDGetDutyKindSet ldGetDutyKindSet = getGetDutyKinds(reasonList,claimCause);
        LLCaseDB tllcasedb = new LLCaseDB();
        tllcasedb.setCaseNo(caseNo);
        //给付责任查询
        if (!tllcasedb.getInfo()) {
            return null;
        }
        String appntNo = "";
        if(!caseNo.equals(tllcasedb.getRgtNo())){
            LLRegisterDB tllregisterdb = new LLRegisterDB();
            tllregisterdb.setRgtNo(tllcasedb.getRgtNo());
            if(tllregisterdb.getInfo())
                appntNo = tllregisterdb.getCustomerNo();
        }
        System.out.println("ldGetDutyKindSet.size()==" + ldGetDutyKindSet.size());

        //参数设置
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("caseno", caseNo);
        tTransferData.setNameAndValue("caserelano", caseRelaNo);
        getDutyGet.setCalFactor(tTransferData);
        VData allGetDuty = getDutyGet.getGetDutyGets(tllcasedb.getCustomerNo(),
                accdate, appntNo);
        //根据GetDutyKind筛选
        VData result = getDutyGet.getGetDutyGetsFilter(allGetDuty,
                ldGetDutyKindSet);

        LLToClaimDutySet llToClaimDutySet = new LLToClaimDutySet();
        //save data
        for (int i = 0; i < result.size(); i++) {
            LCGetSchema lcGet = (LCGetSchema) result.get(i);
            LCPolSchema lcPol = getLCPol(lcGet.getPolNo());

            LLToClaimDutySchema row = new LLToClaimDutySchema();
            row.setGetDutyCode(lcGet.getGetDutyCode()); /* 给付责任编码 */
            row.setCaseNo(caseNo); /* 分案号 */
            row.setGetDutyKind(lcGet.getGetDutyKind()); /* 给付责任类型 */
            row.setCaseRelaNo(caseRelaNo); /* 受理事故号 */
            row.setSubRptNo(subrptNo); /* 事件号 */
            row.setDutyCode(lcGet.getDutyCode());
            row.setPolNo(lcPol.getPolNo()); /*保单号*/
            row.setContNo(lcPol.getContNo()); /* 个单合同号 */
            row.setGrpPolNo(lcPol.getGrpPolNo()); /* 集体保单号 */
            row.setGrpContNo(lcPol.getGrpContNo()); /* 集体合同号 */
            row.setKindCode(lcPol.getKindCode()); /* 险类代码 */
            row.setRiskCode(lcPol.getRiskCode()); /* 险种代码 */
            row.setRiskVer(lcPol.getRiskVersion()); /* 险种版本号 */
            row.setPolMngCom(lcPol.getManageCom()); /* 保单管理机构 */
            row.setClaimCount(0); /* 理算次数 */
//      row.setEstClaimMoney(); /* 预估金额 */

            llToClaimDutySet.add(row);

        }
        System.out.println(llToClaimDutySet);
        return llToClaimDutySet;

    }


    /**
     * getLCPol
     *
     * @param string String
     * @return LCPolSchema
     */
    private LCPolSchema getLCPol(String polNo) {
        LCPolDB db = new LCPolDB();
         LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setPolNo(polNo);
        if (!tLCPolBL.getInfo()) {
            mErrors.addOneError("查询LCPol出错");
            return null;
        } else {
            return tLCPolBL.getSchema();
        }

    }

    /**
     接口 getCaseReasons 获取案件的原因
     String caseNo 案件号
     String caseRelaNo 受理事故号
     返回: 事件发生原因:疾病1，意外2, 0不区分
     说明: 通过案件号和受理事故号,查询意外信息,如有意外,返回2 ;无意外有疾病,返回1 ;即无意外又无疾病,返回0
     */
    public String getCaseReasons(String caseNo, String caseRelaNo) {
        LLAccidentDB llAccidentDB = new LLAccidentDB();
        llAccidentDB.setCaseNo(caseNo);
        llAccidentDB.setCaseRelaNo(caseRelaNo);
        LLAccidentSet set = llAccidentDB.query();
        int accidentCount = set.size();
        if (accidentCount > 0) {
            LLCaseInfoDB tLLCaseInfoDB = new LLCaseInfoDB();
            tLLCaseInfoDB.setCaseNo(caseNo);
            tLLCaseInfoDB.setCaseRelaNo(caseRelaNo);
            LLCaseInfoSet tLLCaseInfoSet = tLLCaseInfoDB.query();
            boolean ax = false; //伤残标志
            boolean fire = false; //烧伤标志
            for (int i = 1; i <= tLLCaseInfoSet.size(); i++) {
                String tType = tLLCaseInfoSet.get(i).getType();
                if (tType.equals("1"))
                    ax = true;
                else
                    fire = true;
            }
            if (ax && !fire)
                return "6";
            if (!ax && fire)
                return "5";
            return "2";
        } else {
            LLCaseCureDB llCaseCureDB = new LLCaseCureDB();
            llCaseCureDB.setCaseNo(caseNo);
            llCaseCureDB.setCaseRelaNo(caseRelaNo);
            LLCaseCureSet cureSet = llCaseCureDB.query();
            int cureCount = cureSet.size();
            if (cureCount > 0) {
                return "1";
            }
        }

        return "0";
    }

    /**
     接口 getCaseResult 获取案件的结果
     String caseNo 案件号
     String caseRelaNo 受理事故号
     返回: 事件发生结果:门诊费用01，住院费用02，医疗津贴03，重大疾病04，身故05，护理06，失能07，伤残08
     说明: 通过案件号和受理事故号,查询检录内容，从而补充受理时没有录入赔付原因
     */
    public String getCaseResult(String caseNo, String caseRelaNo) {
        String CaseResult = "";
        //重疾
        LLCaseCureDB tLLCaseCureDB = new LLCaseCureDB();
        tLLCaseCureDB.setCaseNo(caseNo);
        tLLCaseCureDB.setCaseRelaNo(caseRelaNo);
        tLLCaseCureDB.setSeriousFlag("1");
        LLCaseCureSet tLLCaseCureSet = tLLCaseCureDB.query();
        int SDCount = tLLCaseCureSet.size();
        if (SDCount > 0) {
            CaseResult = ",'04' ";
        }
        //住院费用
        return CaseResult;
    }

    /**
      接口getGetDutyKinds由原因和结果确定给付责任类型
     results String 结果,从LLAppClaimReason中取得,为申请原因(理赔类型),有:门诊费用 住院费用 医疗津贴 重大疾病 身故 护理 失能
     reason  String   原因:意外1，疾病2,0或null不区分
     说明: 根据原因和结果从表LDGetDutyKind中读取给付责任类型即可
     */
    public LDGetDutyKindSet getGetDutyKinds(String results, String reason) {
        LDGetDutyKindDB ldGetDutyKindDB = new LDGetDutyKindDB();
        ldGetDutyKindDB.setCauseCode(reason);
        //    ldGetDutyKindDB.setClaimReason(results);
        String sql = "select * from LDGetDutyKind where causecode='" + reason +
                     "' and ClaimReason in(" + results + ")";
        System.out.println(sql);
        return ldGetDutyKindDB.executeQuery(sql);
    }

    public static void main(String[] args) {
        AutoClaimDutyMap acdm = new AutoClaimDutyMapImpl();
        acdm.autoChooseGetDuty("C1100070126000001","86110000016842");
    }
}
