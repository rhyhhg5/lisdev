package com.sinosoft.lis.llcase;

import java.math.BigDecimal;

import com.sinosoft.lis.db.LLOutcoucingAverageDB;
import com.sinosoft.lis.db.LLOutsourcingTotalDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LLOutcoucingAverageSchema;
import com.sinosoft.lis.schema.LLOutsourcingTotalSchema;
import com.sinosoft.lis.vschema.LLOutcoucingAverageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
public class OutsourcingCostBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    
    private MMap map = new MMap();
    /** 数据操作字符串 */
    private String mOperate;
    private GlobalInput mGlobalInput = new GlobalInput();
//  接收类（天津外包费用总额）
	LLOutsourcingTotalSchema mLLOutsourcingTotalSchema   = new LLOutsourcingTotalSchema();
	//	接收类（本保费分摊费用）
	LLOutcoucingAverageSchema mLLOutcoucingAverageSchema   = new LLOutcoucingAverageSchema();
	
	LLOutcoucingAverageSet  tLLOutcoucingAverageSet = new LLOutcoucingAverageSet();
	
	LJAGetSchema tLJAGetSchema=new LJAGetSchema();
	
	/**
    传输数据的公共方法
    */
   public boolean submitData(VData cInputData, String cOperate) {
	   //将操作数据拷贝到本类中
       this.mOperate = cOperate;
       //得到外部传入的数据,将数据备份到本类中
       if (!getInputData(cInputData)) {
           return false;
       }
       //进行业务处理
       if (!dealData()) {
           return false;
       }
       //准备往后台的数据
       if (!prepareOutputData()) {
           return false;
       }
       PubSubmit tPubSubmit = new PubSubmit();
       
       if (!tPubSubmit.submitData(mInputData, mOperate)) {
           // @@错误处理
           this.mErrors.copyAllErrors(tPubSubmit.mErrors);
           CError tError = new CError();
           tError.moduleName = "OutsourcingCostBL";
           tError.functionName = "submitData";
           tError.errorMessage = "数据提交失败!";
           this.mErrors.addOneError(tError);
           return false;
       }
       return true;
   }
	
   
   /**
    * 从输入数据中得到所有对象
    *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    */
   private boolean getInputData(VData cInputData) {
	   mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
               "GlobalInput", 0));
	  
	 mLLOutsourcingTotalSchema.setSchema((LLOutsourcingTotalSchema)cInputData.getObjectByObjectName(
               "LLOutsourcingTotalSchema", 0));
	 
	 
	  if (mGlobalInput == null) {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "OutsourcingCostBL";
           tError.functionName = "getInputData";
           tError.errorMessage = "没有得到足够的信息！";
           this.mErrors.addOneError(tError);
           return false;
       }
	   return true;
   }

   /**
    * 业务处理主函数
    * @return boolean
    */
   public boolean dealData() {
	   String currentDate = PubFun.getCurrentDate();
       String currentTime = PubFun.getCurrentTime();
       
       if("INSERT||MAIN".equals(mOperate)){
    	  
		   mLLOutsourcingTotalSchema.setMakedate(currentDate);
		   mLLOutsourcingTotalSchema.setMaketime(currentTime);
		   mLLOutsourcingTotalSchema.setModifydate(currentDate);
		   mLLOutsourcingTotalSchema.setModifytime(currentTime);
		   String BatchNo=mLLOutsourcingTotalSchema.getBatchno();
		   //得到费用总额
		   double Ostotal = mLLOutsourcingTotalSchema.getOstotal();
		   //平均费用
		   double OSAverage =0.0;
		   double OSAverageLast =0.0;
		   int Contcount;
		   
		   String sql = "select count(1) from LLOUTCOUCINGAVERAGE  where BatchNo='"+BatchNo+"' and P1='0'";
		   ExeSQL tExeSQL = new ExeSQL();
		   SSRS tSSRS = new SSRS();
		   tSSRS=tExeSQL.execSQL(sql);
		   if(tSSRS!=null){
			   Contcount=Integer.parseInt(tSSRS.GetText(1, 1));
			   mLLOutsourcingTotalSchema.setContcount(tSSRS.GetText(1, 1));
			 
		   }else{
			   return false;
		   }
		   //得到平均数
			OSAverage=Ostotal/Contcount;
		   
		 //通过查询得到所有的符合条件的数据进行更新
		   String sqlAverage = "select * from LLOUTCOUCINGAVERAGE  where BatchNo='"+BatchNo+"' and P1='0'";
		   LLOutcoucingAverageDB tLLOutcoucingAverageDB = new LLOutcoucingAverageDB();
		   tLLOutcoucingAverageSet=tLLOutcoucingAverageDB.executeQuery(sqlAverage);
		   
		   if (tLLOutcoucingAverageSet.mErrors.needDealError()) {
	           this.mErrors.copyAllErrors(tLLOutcoucingAverageSet.mErrors);
	           CError tError = new CError();
	           tError.moduleName = "OutsourcingCostBL";
	           tError.functionName = "dealData";
	           tError.errorMessage = "查询平均保费存在是否出错！";
	           this.mErrors.addOneError(tError);
	           return false;
	       }
			   //		 能够整除
		   if((Ostotal*100)%Contcount==0){
			   for(int i=1;i<=tLLOutcoucingAverageSet.size();i++){
				   tLLOutcoucingAverageSet.get(i).setOsaverage(OSAverage);
			   }
			   
		   }else{
			   //不能够整除
			   //BigDecimal newOSAverage= new BigDecimal(OSAverage);
			   //OSAverage = newOSAverage.setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
			   OSAverage=Arith.round(OSAverage,2);
			   OSAverageLast=Arith.round(Ostotal-(Contcount-1)*OSAverage,2);
			   //BigDecimal newOSAverageLast= new BigDecimal(OSAverageLast);
			   //OSAverageLast = newOSAverageLast.setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
			   for(int i=1;i<=tLLOutcoucingAverageSet.size();i++){
				   if(i!=tLLOutcoucingAverageSet.size()){
					   tLLOutcoucingAverageSet.get(i).setOsaverage(OSAverage);
				   }else{
					   tLLOutcoucingAverageSet.get(i).setOsaverage(OSAverageLast);
				   }
			   }
		   }
		   this.map.put(this.mLLOutsourcingTotalSchema, "INSERT");
		   this.map.put(this.tLLOutcoucingAverageSet, "UPDATE");
       }else if("DELETE||MAIN".equals(mOperate)){
    	   LLOutsourcingTotalDB tLLOutsourcingTotalDB = new  LLOutsourcingTotalDB();
    	   tLLOutsourcingTotalDB.setBatchno(mLLOutsourcingTotalSchema.getBatchno());
    	   mLLOutsourcingTotalSchema=tLLOutsourcingTotalDB.query().get(1);
    	   mLLOutsourcingTotalSchema.setAuditstate("3");
		   mLLOutcoucingAverageSchema.setModifydate(currentDate);
		   mLLOutcoucingAverageSchema.setModifytime(currentTime);
    	   
    	   this.map.put(this.mLLOutsourcingTotalSchema,"UPDATE");
       }else if("UPDATE||MAIN".equals(mOperate)){
    	   //审核功能，改变状态
    	   LLOutsourcingTotalDB tLLOutsourcingTotalDB = new  LLOutsourcingTotalDB();
    	   tLLOutsourcingTotalDB.setBatchno(mLLOutsourcingTotalSchema.getBatchno());
    	   String AuditState =mLLOutsourcingTotalSchema.getAuditstate();
    	   String AuditOpinion=mLLOutsourcingTotalSchema.getAuditopinion();
    	  
    	   mLLOutsourcingTotalSchema=tLLOutsourcingTotalDB.query().get(1);
    	   
    	   mLLOutsourcingTotalSchema.setAuditopinion(AuditOpinion);
    	   mLLOutsourcingTotalSchema.setAuditstate(AuditState);
    	   mLLOutcoucingAverageSchema.setModifydate(currentDate);
		   mLLOutcoucingAverageSchema.setModifytime(currentTime);
		   //如果是1则表示审核通过，并且向财务流转
		   if("1".equals(AuditState)){
			   //向财务流转
			   tLJAGetSchema.setActuGetNo("");
			   String tLimit = PubFun.getNoLimit(mLLOutsourcingTotalSchema.getManagecom());
			   tLJAGetSchema.setActuGetNo(PubFun1.CreateMaxNo("ACGETNO", tLimit));
			   tLJAGetSchema.setOtherNo(mLLOutsourcingTotalSchema.getBatchno());
			   tLJAGetSchema.setOtherNoType("19");
			   tLJAGetSchema.setPayMode(mLLOutsourcingTotalSchema.getTransfertype());
			   tLJAGetSchema.setDrawer(mLLOutsourcingTotalSchema.getOsaccount());
			   tLJAGetSchema.setAccName(mLLOutsourcingTotalSchema.getOsaccount());
			   tLJAGetSchema.setBankCode(mLLOutsourcingTotalSchema.getOsbank());
			   tLJAGetSchema.setBankAccNo(mLLOutsourcingTotalSchema.getOsaccountno());
			   tLJAGetSchema.setSumGetMoney(mLLOutsourcingTotalSchema.getOstotal());
			   tLJAGetSchema.setManageCom(mLLOutsourcingTotalSchema.getManagecom());
			   tLJAGetSchema.setOperator(mLLOutsourcingTotalSchema.getOperator());
			   tLJAGetSchema.setMakeDate(currentDate);
			   tLJAGetSchema.setMakeTime(currentTime);
			   tLJAGetSchema.setModifyDate(currentDate);
			   tLJAGetSchema.setModifyTime(currentTime);
			   tLJAGetSchema.setShouldDate(currentDate);
			   this.map.put(this.tLJAGetSchema,"INSERT");
		   }
		   this.map.put(this.mLLOutsourcingTotalSchema,"UPDATE");
       }
		  return true;
   }
	
   /**
    * 准备往后层输出所需要的数据
    * 输出：如果准备数据时发生错误则返回false,否则返回true
    */
   private boolean prepareOutputData() {
	   try {
           System.out.println("Begin LAAgentBLF.prepareOutputData.........");
           mInputData.clear();
           mInputData.add(map);
       } catch (Exception ex) {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "OutsourcingCostBL";
           tError.functionName = "prepareOutputData";
           tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
           this.mErrors.addOneError(tError);
           return false;
       }
       return true;
   }
   
   
   public static void main(String args[]){
       System.out.println(PubFun.getNoLimit("86950000"));//86950000  86120000
       double Ostotal=10335.63;
       int Contcount=170;
       double OSAverage=Ostotal/Contcount;
       OSAverage=Arith.round(OSAverage,2);
       System.out.println("1=="+OSAverage);
       //BigDecimal newOSAverage= new BigDecimal(OSAverage);
	   //OSAverage = newOSAverage.setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
	   //System.out.println("2=="+OSAverage);
	   double OSAverageLast =Ostotal-(Contcount-1)*OSAverage;
	   OSAverageLast=Arith.round(OSAverageLast,2);
	   System.out.println("3=="+OSAverageLast);
	   //BigDecimal newOSAverageLast= new BigDecimal(OSAverageLast);
	   //OSAverageLast = newOSAverageLast.setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
	   //System.out.println("4"+OSAverageLast);

   }

}

