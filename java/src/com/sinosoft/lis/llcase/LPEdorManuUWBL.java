package com.sinosoft.lis.llcase;

import java.util.*;
import java.lang.*;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.BqCalBL;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.bq.PEdorNSDetailDelPolBL;
import com.sinosoft.lis.bq.PrtManuUWInfo;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
//import com.sinosoft.lis.bq.*;

/**
 * <p>Title: 保全人工核保</p>
 * <p>Description:个人保全人工核保核保完毕确认</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class LPEdorManuUWBL
{
    /** 错误处理类*/
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private TransferData mTransferData = null;

    private String mEdorNo = null;

    private String mMissionId = null;

    private String mSubMissionId = null;

    private String mActivityId = null;

    private GlobalInput mGlobalInput = null;

    private LPUWMasterSet mLPUWMasterSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();
    
    private LPEdorItemSet mLPEdorItemSet = null;

    public LPEdorManuUWBL(GlobalInput gi, TransferData td)
    {
        this.mGlobalInput = gi;
        this.mTransferData = td;
    }

    /**mTransferData
     * 数据提交的公共方法
     * @param gi GlobalInput
     * @param edorAcceptNo String
     * @return boolean
     */
    public boolean submitData()
    {
        if (!getInputData())
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        return true;
    }

    private boolean getInputData()
    {
        this.mEdorNo = (String) mTransferData.getValueByName("EdorNo");
        this.mMissionId = (String) mTransferData.getValueByName("MissionId");
        this.mSubMissionId = (String) mTransferData.getValueByName("SubMissionId");
        this.mActivityId = (String) mTransferData.getValueByName("ActivityId");
        LPUWMasterDB tLPUWMasterDB = new LPUWMasterDB();
        tLPUWMasterDB.setEdorNo(mEdorNo);
        this.mLPUWMasterSet = tLPUWMasterDB.query();
        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        //校验核保结论是否都已录入
        String sql = "select * from LPUWError a " +
                "where EdorNo = '" +  mEdorNo + "' " +
                "and not exists(select * from LPUWMaster " +
                "    where EdorNo = '" + mEdorNo + "' " +
                "    and PolNo = a.PolNo " +
                "    and EdorType = a.EdorType) ";
        System.out.println(sql);
        LPUWErrorDB tLPUWErrorDB = new LPUWErrorDB();
        LPUWErrorSet tLPUWErrorSet = tLPUWErrorDB.executeQuery(sql);
        for (int i = 1; i <= tLPUWErrorSet.size(); i++)
        {
            LPUWErrorSchema tLPUWErrorSchema = tLPUWErrorSet.get(i);
            sql = "select RiskCode from LPPol where PolNo = '"
                  +  tLPUWErrorSchema.getPolNo() + "' ";
            String riskCode = new ExeSQL().getOneValue(sql);

            mErrors.addOneError("序号" + riskCode + "险种未保存核保结论!");
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if(!vlidateIdea())
        {
            return false;
        }
        setUWIdea();       //把核保意见存为文本格式
        setUWState();
        setLGLetter();     //生成核保函件
        createWorkTrace(); //生成作业历史信息
        //setUWVts();      //把核保信息显示到批单上
        bakWorkflow();
        setEdorState();
        if (!submit())
        {
            return false;
        }
        rebuildVts();
        return true;
    }

    /**
     * 使核保决定生效，这里降档和加费拆成两个事物，要不然很难处理
     * @return boolean
     */
    private boolean vlidateIdea()
    {
    	String tEdorType = mLPUWMasterSet.get(1).getEdorType();
    	if(tEdorType.equals(BQ.EDORTYPE_XB))
    	{
    		LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
    		tLPEdorItemDB.setEdorAcceptNo(mEdorNo);
    		tLPEdorItemDB.setEdorType(tEdorType);
    		LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
    		tLPEdorItemSet = tLPEdorItemDB.query();
    		
    		LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
    		tLPEdorItemSchema.setSchema(tLPEdorItemSet.get(1).getSchema());
    		
    		TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue(BQ.EDORTYPE_UW, BQ.EDORTYPE_UW);  //可以在项目理算类中知道是否核保调用
            
            LPEdorXBAppConfirmBL tLPEdorXBAppConfirmBL = new LPEdorXBAppConfirmBL();

            VData data = new VData();
            data.add(mGlobalInput);
            data.add(tLPEdorItemSchema);
            data.add(tTransferData);
            if (!tLPEdorXBAppConfirmBL.submitData(data, "APPCONFIRM||" + tEdorType))
            {
                mErrors.copyAllErrors(tLPEdorXBAppConfirmBL.mErrors);
                return false;
            }
            VData ret = tLPEdorXBAppConfirmBL.getResult();
            MMap map = (MMap) ret.getObjectByObjectName("MMap", 0);
            if (map == null)
            {
                mErrors.addOneError(tEdorType + "项目理算失败!");
                return false;
            }
            mMap.add(map);
    	}
    	
    	
        calPrem();        //计算期交保费
        updateItemMoney();
        updateMainMoney();
        updateAppMoney();
        return true;
    }

    /**
     * 拒绝申请，如果项目下所有险种都拒绝申请则项目终止，拆成多个事务便于处理
     * @param polNo String
     */
    private boolean stopApply()
    {
        for (int i = 1; i <= mLPUWMasterSet.size(); i++)
        {
            LPUWMasterSchema tLPUWMasterSchema = mLPUWMasterSet.get(i);
            String edorType = tLPUWMasterSchema.getEdorType();
            String contNo = tLPUWMasterSchema.getContNo();
            String polNo = tLPUWMasterSchema.getPolNo();
            String passFlag = tLPUWMasterSchema.getPassFlag();
            if ((passFlag != null) && ((passFlag.equals(BQ.PASSFLAG_STOP))
                    || (passFlag.equals(BQ.PASSFLAG_CANCEL))))
            {
                String sql = "update LPPol " +
                        "set EdorType = 'DL' " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + edorType + "' " +
                        "and PolNo = '" + polNo + "'";
                mMap.put(sql, "DELETE");
                sql = "update LPDuty " +
                        "set EdorType = 'DL' " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + edorType + "' " +
                        "and PolNo = '" + polNo + "'";
                mMap.put(sql, "DELETE");
                sql = "update LPPrem " +
                        "set EdorType = 'DL' " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + edorType + "' " +
                        "and PolNo = '" + polNo + "'";
                mMap.put(sql, "DELETE");
                sql = "update LPGet " +
                        "set EdorType = 'DL' " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + edorType + "' " +
                        "and PolNo = '" + polNo + "'";
                mMap.put(sql, "DELETE");
                //删除批改补退费表
                sql = "delete from LJSGetEndorse " +
                      "where EndorseMentNo = '" + mEdorNo + "' " +
                      "and FeeOperationType = '" + edorType + "' " +
                      "and PolNo = '" + polNo + "' ";
                mMap.put(sql, "DELETE");
//                //计算期交保费差额
//                String oldPremSql = "select Prem from LCPol " +
//                        "where PolNo = '" + polNo + "' ";
//                double oldPrem = Double.parseDouble((new ExeSQL()).getOneValue(oldPremSql));
//                String newPremSql = "select Prem from LPPol " +
//                        "where EdorNo = '" + mEdorNo + "' " +
//                        "and EdorType = '" + edorType + "' " +
//                        "and PolNo = '" + polNo + "' ";
//                double newPrem = Double.parseDouble((new ExeSQL()).getOneValue(newPremSql));
//                //更新LPCont的保费
//                double changePrem = newPrem - oldPrem;
//                String contSql = "update LPCont " +
//                        "set Prem = Prem - " + changePrem + ", " +
//                        "    SumPrem = SumPrem - " + changePrem + " " +
//                        "where EdorNo = '" + mEdorNo + "' " +
//                        "and EdorType = '" + edorType + "' " +
//                        "and ContNo = '" + contNo + "' ";
//                mMap.put(contSql, "UPDATE");
//                System.out.println(contSql);

                if(edorType != null && edorType.equals(BQ.EDORTYPE_NS))
                {
                    LPEdorItemSchema schema = new LPEdorItemSchema();
                    schema.setEdorAcceptNo(tLPUWMasterSchema.getEdorNo());
                    schema.setEdorNo(tLPUWMasterSchema.getEdorNo());
                    schema.setEdorType(tLPUWMasterSchema.getEdorType());
                    schema.setContNo(tLPUWMasterSchema.getContNo());
                    schema.setInsuredNo(tLPUWMasterSchema.getInsuredNo());
                    schema.setPolNo(tLPUWMasterSchema.getPolNo());

                    VData data = new VData();
                    data.add(schema);
                    data.add(mGlobalInput);

                    PEdorNSDetailDelPolBL bl = new PEdorNSDetailDelPolBL();
                    MMap tMMap = bl.getSubmitMap(data, "DELETE||INSUREDRISK");
                    mMap.add(tMMap);
                }
            }
        }
        //如果所有LPPol都被撤销则撤销LPCont

        //处理Item表，如果所有险种都拒绝申请则项目终止
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorNo(mEdorNo);
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        for (int i = 1; i <= tLPEdorItemSet.size(); i++)
        {
            LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(i);
            String edorType = tLPEdorItemSchema.getEdorType();
            String insuredNo = tLPEdorItemSchema.getInsuredNo();
            String sql;
            if ((edorType != null) && (edorType.equals(BQ.EDORTYPE_CM))) //这里先写死，以后改掉
            {
                sql = "select * from LPUWMaster " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + edorType + "' " +
                        "and InsuredNo = '" + insuredNo + "' ";
            }
            else
            {
                sql = "select * from LPUWMaster " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + edorType + "' ";
            }
            LPUWMasterDB tLPUWMasterDB = new LPUWMasterDB();
            LPUWMasterSet tLPUWMasterSet = tLPUWMasterDB.executeQuery(sql);
            if (tLPUWMasterSet.size() > 0)
            {
                boolean stopAll = true;
                for (int j = 1; j <= tLPUWMasterSet.size(); j++)
                {
                    LPUWMasterSchema tLPUWMasterSchema = tLPUWMasterSet.get(
                            j);
                    String passflag = tLPUWMasterSchema.getPassFlag();
                    if ((passflag != null) &&
                            (!passflag.equals(BQ.PASSFLAG_STOP)))
                    {
                        stopAll = false;
                    }
                }
                if (stopAll == true)
                {
                    mMap.put(tLPEdorItemSchema, "DELETE");
                    LOBEdorItemSchema tLOBEdorItemSchema = new LOBEdorItemSchema();
                    Reflections ref = new Reflections();
                    ref.transFields(tLOBEdorItemSchema, tLPEdorItemSchema);
                    tLOBEdorItemSchema.setReason("核保终止");
                    tLOBEdorItemSchema.setReasonCode(BQ.REASONCODE_HBZZ);
                    mMap.put(tLOBEdorItemSchema, "DELETE&INSERT");
                }
            }
        }
        return true;
    }

    /**
     * 设置核保状态
     */
    private void setUWState()
    {
        //更新App表
        String sql = "update LPEdorApp " +
                "set UwState = '" + BQ.UWFLAG_MANU + "', " + //经过人工核保
                "    EdorState = '" + BQ.EDORSTATE_CAL + "', " + //理算完成
                "    UwOperator = '" + mGlobalInput.Operator + "', " +
                "    UwDate = '" + mCurrentDate + "', " +
                "    UwTime = '" + mCurrentTime + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + mCurrentDate + "', " +
                "    ModifyTime = '" + mCurrentTime + "' " +
                "where EdorAcceptNo = '" + mEdorNo + "'";
        mMap.put(sql, "UPDATE");
        //更新Main表
        sql = "update LPEdorMain " +
                "set UWState = '" + BQ.UWFLAG_MANU + "', " + //经过人工核保
                "    EdorState = '" + BQ.EDORSTATE_CAL + "', " + //理算完成
                "    UwOperator = '" + mGlobalInput.Operator + "', " +
                "    UwDate = '" + mCurrentDate + "', " +
                "    UwTime = '" + mCurrentTime + "', " +
                "    Operator = '" + mGlobalInput.Operator + "', " +
                "    ModifyDate = '" + mCurrentDate + "', " +
                "    ModifyTime = '" + mCurrentTime + "' " +
                "where EdorAcceptNo = '" + mEdorNo + "'";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 把工作流表里的数据放入B表
     * @return boolean
     */
    private boolean bakWorkflow()
    {
        LWMissionDB tLWMissionDB = new LWMissionDB();
        tLWMissionDB.setMissionID(mMissionId);
        tLWMissionDB.setSubMissionID(mSubMissionId);
        tLWMissionDB.setActivityID(mActivityId);
        if (!tLWMissionDB.getInfo())
        {
            mErrors.addOneError("未找到人工核保工作流信息！");
            return false;
        }
        LBMissionSchema tLBMissionSchema = new LBMissionSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLBMissionSchema, tLWMissionDB.getSchema());
        String serielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
        tLBMissionSchema.setSerialNo(serielNo);
        tLBMissionSchema.setModifyDate(mCurrentDate);
        tLBMissionSchema.setModifyTime(mCurrentTime);
        mMap.put(tLBMissionSchema, "DELETE&INSERT");
        mMap.put(tLWMissionDB.getSchema(), "DELETE");
        return true;
    }

    /**
     * 生成加费数据,这段代码如果放在保存核保结论时(PEdorManuUWPolBL)则无法做回退操作
     * @return boolean
     */
    private boolean calAddPrem()
    {
        for (int i = 1; i <= mLPUWMasterSet.size(); i++)
        {
            LPUWMasterSchema tLPUWMasterSchema = mLPUWMasterSet.get(i);
            String passFlag = tLPUWMasterSchema.getPassFlag();
            String addPremFlag = tLPUWMasterSchema.getAddPremFlag();
            if ((passFlag != null) && (passFlag.equals(BQ.PASSFLAG_CONDITION))
                    && (addPremFlag != null) && (addPremFlag.equals(BQ.TRUE)))
            {
                double edorAddPrem = 0;
                try
                {
                    edorAddPrem = calEdorAddPrem(tLPUWMasterSchema); //计算保全加费
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    return false;
                }
                if (!setEdorAddPrem(tLPUWMasterSchema, edorAddPrem))
                {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 根据加费计算期交保费
     * @param tLPUWMasterSchema LPUWMasterSchema
     * @return boolean
     */
    private boolean calPrem()
    {
        String sql = "update LPDuty a " +
                "set (Prem, SumPrem) = (select sum(Prem), sum(SumPrem) from LPPrem " +
                "    where EdorNo = '" + mEdorNo + "' " +
                "    and EdorType = a.EdorType " +
                "    and ContNo = a.ContNo " +
                "    and PolNo = a.PolNo " +
                "    and DutyCode = a.DutyCode) " +
                "where EdorNo = '" + mEdorNo + "' ";
        mMap.put(sql, "UPDATE");
        LPPolDB tLPPolDB = new LPPolDB();
        tLPPolDB.setEdorNo(mEdorNo);
        LPPolSet tLPPolSet = tLPPolDB.query();
        for (int i = 1; i <= tLPPolSet.size(); i++)
        {
            LPPolSchema tLPPolSchema = tLPPolSet.get(i);
            String polNo = tLPPolSchema.getPolNo();
            String edorType = tLPPolSchema.getEdorType();
            LPPremDB tLPPremDB = new LPPremDB();
            tLPPremDB.setEdorNo(mEdorNo);
            tLPPremDB.setEdorType(edorType);
            tLPPremDB.setPolNo(polNo);
            if (tLPPremDB.query().size() > 0)
            {
                sql = "update LPPol a " +
                        "set (Prem, SumPrem) = (select a.prem + sum(Prem), a.SumPrem + sum(SumPrem) from LPPrem " +
                        "    where EdorNo = '" + mEdorNo + "' " +
                        "    and EdorType = a.EdorType " +
                        "    and ContNo = a.ContNo " +
                        "    and PolNo = a.PolNo and payPlanCode like '000000%' ) " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = 'LB' " +
                        "and PolNo = '" + polNo + "'";
                mMap.put(sql, "UPDATE");
            }
        }
        //计算LPCont的保费要同时计算LPol和LCPol，LPol没有才取LCPol的Prem
//        sql = "update LPCont a " +
//                "set Prem = nvl((select (select nvl(sum(Prem),0) from LCPol b "
//                +" where ContNo = a.ContNo and not exists "
//                +" (select 1 from LPPol where EdorNo = '" + mEdorNo + "' "
//                +" and EdorType = a.EdorType and PolNo = b.PolNo)) + "
//                +" (select sum(Prem) from LPPol where EdorNo = '" + mEdorNo + "' "
//                + " and EdorType = a.EdorType) from dual),0), "
//                + " SumPrem = nvl((select (select nvl(sum(SumPrem),0) from LCPol c "
//                + " where ContNo = a.ContNo and not exists "
//                +" (select * from LPPol where EdorNo = '" + mEdorNo + "' "
//                +" and EdorType = a.EdorType and PolNo = c.PolNo)) + "
//                +" (select sum(SumPrem) from LPPol where EdorNo = '" + mEdorNo + "' "
//                +" and EdorType = a.EdorType) from dual),0) "
//                +" where EdorNo = '" + mEdorNo + "' " ;
//        System.out.println(sql);
//        mMap.put(sql, "UPDATE");
        return true;
    }

    /**
     * 计算保全加费金额
     * @param tLPUWMasterSchema LPUWMasterSchema
     * @return double
     */
    private double calEdorAddPrem(LPUWMasterSchema tLPUWMasterSchema)
    {
        String edorType = tLPUWMasterSchema.getEdorType();
        String polNo = tLPUWMasterSchema.getPolNo();
        String payPlanCode = tLPUWMasterSchema.getPayPlanCode();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(polNo);
        if (!tLCPolDB.getInfo())
        {
            mErrors.addOneError("未找到险种" + polNo +"信息！");
            throw new RuntimeException();
        }
        Date curPayToDate = CommonBL.stringToDate(tLCPolDB.getPaytoDate());
        Date lastPayToDate = CommonBL.stringToDate(CommonBL.getLastPayToDate(
                tLCPolDB.getPolNo(), tLCPolDB.getPaytoDate()));
        System.out.println(curPayToDate.toString());
              System.out.println(lastPayToDate.toString());
        if (curPayToDate == null)
        {
            mErrors.addOneError("未找到序号" + tLCPolDB.getRiskSeqNo() + "险种交至日期！");
            throw new RuntimeException();
        }
        if (lastPayToDate == null)
        {
            mErrors.addOneError("未找到序号" + tLCPolDB.getRiskSeqNo() + "险种交费起始日期！");
            throw new RuntimeException();
        }
        LPPremDB tLPPremDB = new LPPremDB();
        tLPPremDB.setEdorNo(mEdorNo);
        tLPPremDB.setEdorType(edorType);
        tLPPremDB.setPolNo(polNo);
        tLPPremDB.setPayPlanCode(payPlanCode);
        LPPremSet tLPPremSet = tLPPremDB.query();
        if (tLPPremSet.size() == 0)
        {
            mErrors.addOneError("未找到序号" + tLCPolDB.getRiskSeqNo() + "险种加费信息！");
            throw new RuntimeException();
        }

        LPPremSchema tLPPremSchema = tLPPremSet.get(1);
        Date payStartDate = CommonBL.stringToDate(tLPPremSchema.getPayStartDate());
//        Date payEndDate = CommonBL.stringToDate(tLPPremSchema.getPayEndDate());
        System.out.println(payStartDate.toString());
              //System.out.println(payEndDate.toString());
        if (payStartDate == null)
        {
            mErrors.addOneError("未找到序号" + tLCPolDB.getRiskSeqNo() + "险种加费起始日期！");
            throw new RuntimeException();
        }
//        if (payEndDate == null)
//        {
//            mErrors.addOneError("未找到序号" + tLCPolDB.getRiskSeqNo() + "险种加费终止日期！");
//            throw new RuntimeException();
//        }
        double edorAddPrem = 0;
        if (payStartDate.before(lastPayToDate)) //如果加费起始日期在上期，则追溯前几期保费
        {
            edorAddPrem = CommonBL.carry(tLPPremSchema.getPrem() *
                    (PubFun.calInterval(payStartDate, curPayToDate, "M") / tLCPolDB.getPayIntv()));
        }
        else if (payStartDate.after(curPayToDate)) //如果加费起始日期在下期，则本期交费为0
        {
            edorAddPrem = 0;
        }
        else //如果加费起始日期在本期，则算未满期
        {
           // System.out.println((double)PubFun.calInterval(payStartDate, curPayToDate, "D") /  (double)PubFun.calInterval(lastPayToDate, curPayToDate, "D"));
          //  System.out.println(PubFun.calInterval(lastPayToDate, curPayToDate, "D"));
            edorAddPrem = CommonBL.carry(tLPPremSchema.getPrem() *
                    ((double) PubFun.calInterval(payStartDate, curPayToDate, "D")
                    / (double) PubFun.calInterval(lastPayToDate, curPayToDate, "D")));
        }
        
        //如果是长期险复效并且收取续期保费的情况进行的核保,需要加费时会少收取一期的加费. 2010-1-19 by xp
        if(edorType.equals("FX"))
        {
        	String checkmoney= new ExeSQL().getOneValue("select 1 from ljsgetendorse where endorsementno = '"+mEdorNo+"' " +
        			" and feeoperationtype='FX' and feefinatype='BF' and getmoney>0 and polno= '"+polNo+"' ");
        	if(checkmoney!=null&&checkmoney.equals("1")){
        		edorAddPrem+=CommonBL.carry(tLPPremSchema.getPrem());
    		}
        }
        
        return edorAddPrem;
    }

    /**
     * 设置保全加费
     * @param tLPUWMasterSchema LPUWMasterSchema
     * @param edorAddPrem double
     * @return boolean
     */
    private boolean setEdorAddPrem(LPUWMasterSchema tLPUWMasterSchema, double edorAddPrem)
    {
        String edorType = tLPUWMasterSchema.getEdorType();
        String polNo = tLPUWMasterSchema.getPolNo();
        LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
        tLJSGetEndorseDB.setEndorsementNo(mEdorNo);
        tLJSGetEndorseDB.setFeeOperationType(edorType);
        tLJSGetEndorseDB.setPolNo(polNo);
        LJSGetEndorseSet tLJSGetEndorseSet = tLJSGetEndorseDB.query();
        if (tLJSGetEndorseSet.size() == 0)
        {
            LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
            tLPEdorItemDB.setEdorNo(mEdorNo);
            tLPEdorItemDB.setEdorType(edorType);
            LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(polNo);
            if (!tLCPolDB.getInfo())
            {
                mErrors.addOneError("未找到险种" + polNo +"信息！");
                return false;
            }
            BqCalBL bqCalBL = new BqCalBL();
            LJSGetEndorseSchema tLJSGetEndorseSchema = bqCalBL.
                    initLJSGetEndorse(
                    tLPEdorItemSet.get(1), tLCPolDB.getSchema(), null, "BF", edorAddPrem,
                    mGlobalInput);
            tLJSGetEndorseSchema.setManageCom(tLCPolDB.getManageCom());
            mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
        }
        else
        {
            LJSGetEndorseSchema tLJSGetEndorseSchema = tLJSGetEndorseSet.get(1);
            double getMoney = CommonBL.carry(tLJSGetEndorseSchema.getGetMoney() + edorAddPrem);
            tLJSGetEndorseSchema.setGetMoney(getMoney);
            tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
            tLJSGetEndorseSchema.setModifyDate(mCurrentDate);
            tLJSGetEndorseSchema.setModifyTime(mCurrentTime);
            mMap.put(tLJSGetEndorseSchema, "DELETE&INSERT");
        }
        return true;
    }

    /**
     * 更新Item表的保费，分为客户和保单两种情况
     */
    private boolean updateItemMoney()
    {
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setEdorAcceptNo(mEdorNo);
        LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.query();
        for (int i = 1; i <= tLPEdorItemSet.size(); i++)
        {
            LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(i);
            String edorType = tLPEdorItemSchema.getEdorType();
            String contNo = tLPEdorItemSchema.getContNo();
            String insuredNo = tLPEdorItemSchema.getInsuredNo();
            String sql;
            if (insuredNo.equals(BQ.FILLDATA))
            {
                sql = "update LPEdorItem a " +
                        "set GetMoney = (select case when sum(GetMoney) is null  " +
                       "     then 0 else sum(GetMoney) end " +
                        "    from LJSGetEndorse " +
                        "    where EndorseMentNo = '" + mEdorNo + "' " +
                        "    and FeeOperationType = '" + edorType + "' " +
                        "    and ContNo = a.ContNo) " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + edorType + "' " +
                        "and ContNo = '" + contNo + "' ";
            }
            else
            {
                sql = "update LPEdorItem a " +
                        "set GetMoney = (select case when sum(GetMoney) is null " +
                        "    then 0 else sum(GetMoney) end from LJSGetEndorse " +
                        "    where EndorseMentNo = '" + mEdorNo + "' " +
                        "    and FeeOperationType = '" + edorType + "' " +
                        "    and InsuredNo = '" + insuredNo + "' " +
                        "    and ContNo = a.ContNo) " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + edorType + "' " +
                        "and InsuredNo = '" + insuredNo + "' " +
                        "and ContNo = '" + contNo + "' ";
            }
            System.out.println(sql);
            mMap.put(sql, "UPDATE");
        }
        return true;
    }

    /**
     * 统计Item表中的保费变化,同步更新Main,表其中Main表按ContNo和Item表对应
     */
    private void updateMainMoney()
    {
        LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
        tLPEdorMainDB.setEdorAcceptNo(mEdorNo);
        LPEdorMainSet tLPEdorMainSet = tLPEdorMainDB.query();
        for (int i = 1; i <= tLPEdorMainSet.size(); i++)
        {
            String contNo = tLPEdorMainSet.get(i).getContNo();
            StringBuffer sql = new StringBuffer("update LPEdorMain ");
            sql.append("set (ChgPrem, ChgAmnt, GetMoney, GetInterest) = ")
                    .append("(select (case when sum(ChgPrem) is null then 0 else sum(ChgPrem) end), ")
                    .append("(case when sum(ChgAmnt) is null then 0 else sum(ChgAmnt) end), ")
                    .append("(case when sum(GetMoney) is null then 0 else sum(GetMoney) end), ")
                    .append("(case when sum(GetInterest) is null then 0 else sum(GetInterest) end) ")
                    .append(" from LPEdorItem ")
                    .append("where EdorAcceptNo = '").append(mEdorNo).
                    append("' ")
                    .append("and ContNo = '").append(contNo).append("') ")
                    .append("where EdorAcceptNo = '").append(mEdorNo).
                    append("' ")
                    .append("and ContNo = '").append(contNo).append("'");
            mMap.put(sql.toString(), "UPDATE");
        }
    }

    /**
     * 统计Main表中的保费变化,同步更新App表
     */
    private void updateAppMoney()
    {
        String sql = "update LPEdorApp " +
                "set (ChgPrem, ChgAmnt, GetMoney, GetInterest) = " +
                "    (select sum(ChgPrem), sum(ChgAmnt), sum(GetMoney), " +
                "    sum(GetInterest) from LPEdorMain " +
                "    where EdorAcceptNo = '" + mEdorNo + "') " +
                "where EdorAcceptNo = '" + mEdorNo + "' ";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 降档或减额，两个互斥，只算一个
     * @return boolean
     */
    private boolean calMultAndAmnt()
    {
        LSubMultAndAmnt tSubMultAndAmnt =
                new LSubMultAndAmnt(mGlobalInput, mEdorNo);
        if (!tSubMultAndAmnt.submitData())
        {
            mErrors.copyAllErrors(tSubMultAndAmnt.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 获得当前函件的序号
     * @return String
     */
    private String getSerialNumber()
    {
        //得到序号
        String sql = "select * from LGLetter " +
                "where EdorAcceptNo = '" + mEdorNo + "' " +
                "order by int(SerialNumber) desc ";
        LGLetterDB db = new LGLetterDB();
        LGLetterSet set = db.executeQuery(sql);
        if (set.size() == 0)
        {
            return "1";
        }
        int sn = Integer.parseInt(set.get(1).getSerialNumber());
        return String.valueOf(sn + 1);
    }

    //生成核保函件
    private boolean setLGLetter()
    {
        LGLetterSchema tLGLetterSchema = new LGLetterSchema();
        tLGLetterSchema.setEdorAcceptNo(mEdorNo);
        tLGLetterSchema.setSerialNumber(getSerialNumber());
        tLGLetterSchema.setLetterType("0"); //核保函件
        tLGLetterSchema.setLetterSubType("1"); //补充类型
        tLGLetterSchema.setState("0"); //待下发
        tLGLetterSchema.setOperator(mGlobalInput.Operator);
        tLGLetterSchema.setMakeDate(mCurrentDate);
        tLGLetterSchema.setMakeTime(mCurrentTime);
        tLGLetterSchema.setModifyDate(mCurrentDate);
        tLGLetterSchema.setModifyTime(mCurrentTime);
        mMap.put(tLGLetterSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 把核保信息显示到批单上
     * @return boolean
     */
    private boolean setUWVts()
    {
        PrtManuUWInfo tPrtManuUWInfo = new PrtManuUWInfo(mEdorNo);
        MMap map = tPrtManuUWInfo.getSubmitData();
        if (map == null)
        {
            mErrors.copyAllErrors(tPrtManuUWInfo.mErrors);
            return false;
        }
        mMap.add(map);
        return true;
    }

    private boolean setUWIdea()
    {
        for (int i = 1; i <= mLPUWMasterSet.size(); i++)
        {
            String uwIdea = "";
            LPUWMasterSchema tLPUWMasterSchema = mLPUWMasterSet.get(i);
            String passFlag = tLPUWMasterSchema.getPassFlag();
            if (passFlag.equals(BQ.PASSFLAG_PASS))
            {
                uwIdea = "批准申请";
            }
            else if (passFlag.equals(BQ.PASSFLAG_STOP))
            {
                uwIdea = "终止申请";
            }
            else if (passFlag.equals(BQ.PASSFLAG_CANCEL))
            {
                uwIdea = "险种解约";
            }
            else if (passFlag.equals(BQ.PASSFLAG_STOPXB))
            {
                uwIdea = "续保终止";
            }
            else if (passFlag.equals(BQ.PASSFLAG_CONDITION))
            {
                if (tLPUWMasterSchema.getSubMultFlag() != null &&
                        tLPUWMasterSchema.getSubMultFlag().equals(BQ.TRUE))
                {
                    uwIdea += "档次变为" + tLPUWMasterSchema.getMult() + "档；";
                }

                if (tLPUWMasterSchema.getSubAmntFlag() != null &&
                        tLPUWMasterSchema.getSubAmntFlag().equals(BQ.TRUE))
                {
                    uwIdea += "保额变为" + tLPUWMasterSchema.getAmnt() + "元；";
                }
                if (tLPUWMasterSchema.getAddPremFlag() != null &&
                        tLPUWMasterSchema.getAddPremFlag().equals(BQ.TRUE))
                {
                    String addPremsql =
                            "select * from lpprem " +
                            " where payplancode like '000000%' " +
                            "and edorno='" + tLPUWMasterSchema.getEdorNo() +
                            "' and edortype='" + tLPUWMasterSchema.getEdorType() + "'" +
                            " and polno='" + tLPUWMasterSchema.getPolNo() + "'";
                    LPPremDB tLPPremDB = new LPPremDB();
                    LPPremSet tLPPremSet = tLPPremDB.executeQuery(addPremsql);
                    if (tLPPremSet.size() > 0)
                    {
                        LPPremSchema tLPPremSchema = tLPPremSet.get(1);
                        uwIdea += "加费" + tLPPremSchema.getPrem() + "元，";
                        if (tLPPremSchema.getRate() > 0)
                        {
                            uwIdea += "加费比例" + String.valueOf(tLPPremSchema.getRate()) + "，";
                        }
                        uwIdea += "生效日期" + tLPPremSchema.getPayStartDate();
                        if (tLPPremSchema.getPayEndDate() != null)
                        {
                            uwIdea += "，终止日期" + tLPPremSchema.getPayEndDate();
                        }
                        uwIdea += "；";
                    }
                }
                if (tLPUWMasterSchema.getSpecFlag() != null &&
                        tLPUWMasterSchema.getSpecFlag().equals(BQ.TRUE))
                {
                    uwIdea += "特别约定：";
                    String Specsql =
                            "select speccontent from lpspec " +
                            " where edorno='" + tLPUWMasterSchema.getEdorNo() +
                            "' and edortype='" + tLPUWMasterSchema.getEdorType() + "'" +
                            " and polno='" + tLPUWMasterSchema.getPolNo() + "'";
                    SSRS tSSRS = (new ExeSQL()).execSQL(Specsql);
                    for (int k = 1; k <= tSSRS.getMaxRow(); k++)
                    {
                        String SpecTemp = tSSRS.GetText(k, 1);
                        uwIdea += SpecTemp;
                        if (k < tSSRS.getMaxRow())
                        {
                            uwIdea += "，";
                        }
                    }
                    uwIdea += "；";
                }
            }
            String sql = "update LPUWMaster " +
                    "set SugUWIdea = '" + uwIdea + "',EdorType = 'LB',state='0' " +
                    "where EdorNo = '" + mEdorNo + "' " +
                    "and EdorType = '" + tLPUWMasterSchema.getEdorType() + "' " +
                    "and PolNo = '" + tLPUWMasterSchema.getPolNo() + "'";
            mMap.put(sql, "UPDATE");
            
            sql = "update LPUWMaster " +
            "set state='1' " +
            "where EdorNo <> '" + mEdorNo + "' " +
            "and EdorType = 'LB' " +
            "and PolNo = '" + tLPUWMasterSchema.getPolNo() + "' and state='0'";
            mMap.put(sql, "UPDATE");
            
            sql = "update LGWork " +
            "set StatusNo='5' " +
            "where WORKNO <> '" + mEdorNo + "' " +
            "and TypeNo = '070001' " +
            "and InnerSource = '070002' ";
            mMap.put(sql, "UPDATE");
        }
        return true;
    }

    /**
     * 生成作业历史
     * @return boolean
     */
    private void createWorkTrace()
    {
        String sql = "select max(int(NodeNo)) from LGTraceNodeOp " +
                "where WorkNo = '" + mEdorNo + "'";
        String nodeNo = (new ExeSQL()).getOneValue(sql);
        sql = "select max(int(OperatorNo)) + 1 from LGTraceNodeOp " +
                "where WorkNo = '" + mEdorNo + "'";
        String operatorNo = (new ExeSQL()).getOneValue(sql);

        //生成具体历史信息
        LGTraceNodeOpSchema tLGTraceNodeOpSchema = new LGTraceNodeOpSchema();
        tLGTraceNodeOpSchema.setWorkNo(mEdorNo);
        tLGTraceNodeOpSchema.setNodeNo(nodeNo);
        tLGTraceNodeOpSchema.setOperatorNo(operatorNo);
        tLGTraceNodeOpSchema.setOperatorType("5"); //人工核保
        tLGTraceNodeOpSchema.setRemark("自动批注：人工核保完毕");
        tLGTraceNodeOpSchema.setFinishDate(mCurrentDate);
        tLGTraceNodeOpSchema.setFinishTime(mCurrentTime);
        tLGTraceNodeOpSchema.setOperator(mGlobalInput.Operator);
        tLGTraceNodeOpSchema.setMakeDate(mCurrentDate);
        tLGTraceNodeOpSchema.setMakeTime(mCurrentTime);
        tLGTraceNodeOpSchema.setModifyDate(mCurrentDate);
        tLGTraceNodeOpSchema.setModifyTime(mCurrentTime);
        mMap.put(tLGTraceNodeOpSchema, "DELETE&INSERT");
    }

    /**
     * 设置为核保完毕状态
     * @return boolean
     */
    private boolean setEdorState()
    {
        String sql = "update LPEdorApp "
                + "set EdorState = '" + BQ.EDORSTATE_CONFIRM + "', "
                + "    Operator = '" + mGlobalInput.Operator + "', "
                + "    ModifyDate = '" + PubFun.getCurrentDate() + "', "
                + "    ModifyTime = '" + PubFun.getCurrentTime() + "' "
                + "where EdorAcceptNo = '" + mEdorNo + "' ";
        mMap.put(sql, "UPDATE");
        return true;
    }

    /**
     * 重新生成批单
     * @return boolean
     */
    private boolean rebuildVts()
    {
        //生成打印数据
//        VData data = new VData();
//        data.add(mGlobalInput);
//        PrtAppEndorsementBL tPrtAppEndorsementBL = new PrtAppEndorsementBL(
//                mEdorNo);
//        if (!tPrtAppEndorsementBL.submitData(data, ""))
//        {
//            mErrors.addOneError("重新生成保全批单错误！" + tPrtAppEndorsementBL.mErrors.getFirstError());
//            return false;
//        }
        return true;
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
