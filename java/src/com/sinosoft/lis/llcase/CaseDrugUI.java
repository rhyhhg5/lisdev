package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:案件－立案－分案保单明细保存的接收数据处理类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @version 1.0
 */
public class CaseDrugUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public CaseDrugUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    CaseDrugBL tCaseDrugBL = new CaseDrugBL();
    System.out.println("---UI BEGIN---");
    if (!tCaseDrugBL.submitData(cInputData,mOperate) )
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tCaseDrugBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "CaseReceiptUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据保存失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
      mResult = tCaseDrugBL.getResult();
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public static void main(String[] args)
  {
    CaseDrugUI tCaseDrugUI = new CaseDrugUI();
    VData tVData=new VData();
    LLCaseDrugSchema tLLCaseDrugSchema = new LLCaseDrugSchema();

 //      tLLCaseReceiptSchema.setSerialNo("1");
   //  tLLCasePolicySchema.setInsuredName("2");
   //  tLLCasePolicySchema.setInsuredNo("3");
    tLLCaseDrugSchema.setCaseNo("1");
    //tLLCasePolicySchema.setPolNo("1");
    //tLLCasePolicySchema.setRgtNo("11111111");
    LLCaseDrugSet tLLCaseDrugSet = new LLCaseDrugSet();
    tLLCaseDrugSet.add(tLLCaseDrugSchema);

    tVData.addElement(tLLCaseDrugSet);

    tCaseDrugUI.submitData(tVData,"INSERT||MAIN");
  }
}
