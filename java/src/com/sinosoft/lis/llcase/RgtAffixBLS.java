package com.sinosoft.lis.llcase;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统投保暂交费功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author YT
 * @version 1.0
 */

public class RgtAffixBLS
{
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();


  public RgtAffixBLS() {}

  public static void main(String[] args)
  {
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    System.out.println("Start RgtAffix BLS Submit...");
    if (!this.saveData())
      return false;
    System.out.println("End RgtAffix BLS Submit...");
    mInputData=null;
    return true;
  }

  private boolean saveData()
  {
    LLRgtAffixSet mLLRgtAffixSet = (LLRgtAffixSet)mInputData.getObjectByObjectName("LLRgtAffixBLSet",0);
//10-17修改
Connection conn = DBConnPool.getConnection();
    //Connection conn = null;
    //conn = PubFun1.getDefaultConnection();
    if (conn==null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "RgtAffixBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    try
    {
      conn.setAutoCommit(false);

			// 保存现在的关联
			LLRgtAffixDBSet mLLRgtAffixDBSet = new LLRgtAffixDBSet( conn );
			mLLRgtAffixDBSet.set(mLLRgtAffixSet);
			if (mLLRgtAffixDBSet.insert() == false)
			{
				// @@错误处理
			  this.mErrors.copyAllErrors(mLLRgtAffixDBSet.mErrors);
				CError tError = new CError();
				tError.moduleName = "RgtAffixBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "保存数据失败!";
				this.mErrors .addOneError(tError) ;
	       conn.rollback() ;
	       conn.close();
				return false;
			}
			conn.commit() ;
			conn.close();
		} // end of try
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "RgtAffixBLS";
			tError.functionName = "submitData";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);
			try
			{
				conn.rollback() ;
				conn.close();
			}
			 catch(Exception e)
			 {
			}
			
			return false;
		}
    return true;
  }

}