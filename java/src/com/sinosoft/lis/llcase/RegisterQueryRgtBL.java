package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
//该类的名称要该成Register1QueryBL
public class RegisterQueryRgtBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  /**要查询的表命要进行改动  */
  private LCPolBL  tLCPolBL = new LCPolBL();
  private LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
  private LCPolBLSet mLCPolBLSet = new LCPolBLSet();
  private LCPolSchema tLCPolSchema = new LCPolSchema();
  public RegisterQueryRgtBL() {}

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;

    //数据查询业务处理
    if (cOperate.equals("QUERY||MAIN"))
    {
      System.out.println("1");
      if(!queryData())
	return false;
      System.out.println("---query       Data---");
    }
    // 数据操作业务处理
    if (cOperate.equals("INSERT"))
    {
      if (!dealData())
        return false;
      System.out.println("---dealData---");
    }
    
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealData()
  {
    RegisterQueryRgtBLS tRegisterQueryRgtBLS = new RegisterQueryRgtBLS();
    System.out.println("Start RegisterQueryRgt Submit...");
    if (!tRegisterQueryRgtBLS.submitData(mInputData,mOperate))
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tRegisterQueryRgtBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "RegisterQueryRgtBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    // 暂交费查询条件
    if (mOperate.equals("QUERY||MAIN"))
    {
      tLCPolBL.setSchema((LCPolSchema)cInputData.getObjectByObjectName("LCPolSchema",0));
      tLLRegisterSchema.setSchema((LLRegisterSchema)cInputData.getObjectByObjectName("LLRegisterSchema",0));
      System.out.println("InputData函数！");
    }
     

    return true;
  }

  /**
   * 查询符合条件的暂交费信息
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
   //修改表名
   //在此进行判断
  private boolean queryData()
  {

    System.out.println("执行至此");
    LCPolDB tLCPolDB = new LCPolDB();
    System.out.println("执行至此131");

    if (tLLRegisterSchema.getRgtObj().equals("0")) {
             tLCPolDB.setContNo( tLLRegisterSchema.getRgtObjNo());
             System.out.println("tLCPolDB.setContNo " + tLCPolDB.getContNo());
             //tLCPolDB.setContNo( mLCPolBL.getContNo());
    } else if (tLLRegisterSchema.getRgtObj().equals("1")) {
             tLCPolDB.setGrpPolNo( tLLRegisterSchema.getRgtObjNo());
             System.out.println("tLCPolDB.setGrpPolNo " + tLCPolDB.getGrpPolNo());
             //tLCPolDB.setGrpPolNo( mLCPolBL.getGrpPolNo());
    } else if (tLLRegisterSchema.getRgtObj().equals("2")) {
             tLCPolDB.setPolNo( tLLRegisterSchema.getRgtObjNo());
             System.out.println("tLCPolDB.setPolNo " + tLCPolDB.getPolNo());
             //tLCPolDB.setPolNo( mLCPolBL.getPolNo());
    } else {
             tLCPolDB.setInsuredNo( tLLRegisterSchema.getRgtObjNo());
             System.out.println("tLCPolDB.setInsuredNo " + tLCPolDB.getInsuredNo());
    }
             

             System.out.println("4");
    mLCPolBLSet.set(tLCPolDB.query());
    System.out.println("5");
    if (tLCPolDB.mErrors.needDealError() == true)
    {
      // @@错误处理
	  this.mErrors.copyAllErrors(tLCPolDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "LCPolBL";
      tError.functionName = "queryData";
      tError.errorMessage = "保单查询失败!";
      this.mErrors.addOneError(tError);
      mLCPolBLSet.clear();
      return false;
    }
    if (mLCPolBLSet.size() == 0)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LCPolBL";
      tError.functionName = "queryData";
      tError.errorMessage = "未找到相关数据!";
      this.mErrors.addOneError(tError);
      mLCPolBLSet.clear();
      return false;
	}

	mResult.clear();
	mResult.add( mLCPolBLSet );

	return true;
  }
  }