package com.sinosoft.lis.llcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CodeJudge;

public class CaseFunPub
{

    public CaseFunPub()
    {
    }

    public static void main(String[] args)
    {
//        String tcode = "000012";
//        CaseFunPub.show_People(tcode);
    }

    //根据报案号判断案件状态
    public static String getCaseStateByRptNo(String tRgtNo)
    {
        String tState = "";
        if (tRgtNo == null)
            return "未报案";
        LLReportDB tLLReportDB = new LLReportDB();
        tLLReportDB.setRptNo(tRgtNo);
        if (!tLLReportDB.getInfo())
            return "未报案";
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(tRgtNo);
        LLRegisterSet tLLRegisterSet = new LLRegisterSet();
        tLLRegisterSet = tLLRegisterDB.query();
        if (tLLRegisterSet.size() == 0)
        {
            return "待立案";
        }
        else
        {
            tState = getCaseStateByRgtNo(tLLRegisterSet.get(1).getRgtNo());
        }
        return tState;
    }

    //根据案件号判断案件的状态
    public static String getCaseStateByRgtNo(String tRgtNo)
    {
        String tState = "";
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(tRgtNo);
        if (!tLLRegisterDB.getInfo())
        {
            tState = "待立案";
            return tState;
        }
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        tLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
        if (tLLRegisterSchema.getDeclineFlag() != null &&
            tLLRegisterSchema.getDeclineFlag().equals("1"))
        {
            tState = "已撤件";
            return tState;
        }
        if (tLLRegisterSchema.getClmState().equals("5"))
        {
            tState = "调查中";
            return tState;
        }
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setRgtNo(tRgtNo);
        LLClaimSet tLLClaimSet = new LLClaimSet();
        tLLClaimSet = tLLClaimDB.query();
        if (tLLClaimSet.size() == 0)
        {
            tState = "待理算";
            return tState;
        }
        LLClaimSchema tLLClaimSchema = new LLClaimSchema();
        tLLClaimSchema.setSchema(tLLClaimSet.get(1));

        LLClaimUWMainDB tLLClaimUWMainDB = new LLClaimUWMainDB();
        tLLClaimUWMainDB.setRgtNo(tRgtNo);
        LLClaimUWMainSet tLLClaimUWMainSet = tLLClaimUWMainDB.query();
        if (tLLClaimUWMainSet.size() == 0)
        {
            tState = "待审核";
            return tState;
        }

        LLClaimUWMainSchema tLLClaimUWMainSchema = new LLClaimUWMainSchema();
        tLLClaimUWMainSchema.setSchema(tLLClaimUWMainSet.get(1));
        if (tLLClaimUWMainSchema.getAppPhase() != null &&
            tLLClaimSchema.getClmState().equals("1"))
        {
            if (tLLClaimUWMainSchema.getAppPhase().equals("0"))
            {
                tState = "待签批";
                return tState;
            }
        }

        if (tLLClaimUWMainSchema.getAppActionType() == null)
        {
            tState = "待审核";
            return tState;
        }
        if (tLLClaimUWMainSchema.getAppActionType().equals("2"))
        {
            tState = "签批退回";
            return tState;
        }
        if (tLLClaimUWMainSchema.getAppActionType().equals("3"))
        {
            tState = "签批上报";
            return tState;
        }
        if (tLLClaimUWMainSchema.getAppActionType().equals("1"))
        {
            LJAGetDB tLJAGetDB = new LJAGetDB();
            tLJAGetDB.setOtherNo(tLLClaimUWMainSchema.getClmNo());
            LJAGetSet tLJAGetSet = tLJAGetDB.query();
            if (tLJAGetSet.size() > 0)
            {
                if (tLJAGetSet.get(1).getConfDate() != null)
                {
                    tState = "已给付";
                    return tState;
                }
                else
                {
                    tState = "已通知";
                    return tState;
                }
            }
            tState = "签批同意";
            return tState;
        }
        else
        {
            tState = "待签批"; //上报
            return tState;
        }
    }

    //根据立案号判断案件的审核类型
    public static String getCheckTypeByRgtNo(String tRgtNo)
    {
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setRgtNo(tRgtNo);
        LLClaimSet tLLClaimSet = tLLClaimDB.query();
        if (tLLClaimSet.size() == 0)
        { //尚未理算，如果将来有申述审核，判断条件会有改变
            return "0"; //初次审核
        }
        else
        {
            return tLLClaimSet.get(1).getCheckType();
        }
    }

    //根据客户号查询以该客户为被保人的所有保单，并且按照主附险顺序加入Set
    public static LCPolSet getInsuredPolicy(String tInsuredNo)
    {
        LCPolSet tLCPolSet = new LCPolSet();
        LCPolSet ttLCPolSet = new LCPolSet();
        LCPolDB tLCPolDB = new LCPolDB();
        String tSql = "select * from lcpol where insuredno='" + tInsuredNo +
                      "' and polno=mainpolno";
        ttLCPolSet = tLCPolDB.executeQuery(tSql);
        for (int i = 1; i <= ttLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = new LCPolSchema();
            tLCPolSchema = ttLCPolSet.get(i);
            tLCPolSet.add(tLCPolSchema);
            String tSql1 = "select * from lcpol where mainpolno='" +
                           tLCPolSchema.getPolNo() + "' and polno<>mainpolno";
            LCPolDB ttLCPolDB = new LCPolDB();
            tLCPolSet.add(ttLCPolDB.executeQuery(tSql1));
        }
        return tLCPolSet;
    }

    //判断案件的审核类型
    public static String getCaseCheckType(String tRgtNo)
    {
        LLClaimUWMainDB tLLClaimUWMainDB = new LLClaimUWMainDB();
        tLLClaimUWMainDB.setRgtNo(tRgtNo);
        LLClaimUWMainSet tLLClaimUWMainSet = tLLClaimUWMainDB.query();
        if (tLLClaimUWMainSet.size() == 0)
        {
            return "初次审核";
        }
        else
        {
            if (tLLClaimUWMainSet.get(1).getCheckType() == null)
            {
                return "初次审核";
            }
            else
            {
                if (tLLClaimUWMainSet.get(1).getCheckType().equals("1"))
                {
                    return "初次审核";
                }
                if (tLLClaimUWMainSet.get(1).getCheckType().equals("2"))
                {
                    return "签批退回审核";
                }
                if (tLLClaimUWMainSet.get(1).getCheckType().equals("3"))
                {
                    return "申诉审核";
                }
            }
        }
        return "初次审核";
    }

    //判断保单状态，其中0:有效 1：终止 2：暂停 3：失效 4：复效
    //数组的第一列：状态；第二列：导致该状态的原因；第三列：生效的开始日期；第四列：保单终止日期/失效日期
    public static String[] getPolState(String tPolNo)
    {
        String tState[] = new String[4];
        if (CodeJudge.judgeCodeType(tPolNo, "21"))
        { //个人保单号
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(tPolNo);
            if (tLCPolDB.getInfo())
            {
                LCPolSchema tLCPolSchema = new LCPolSchema();
                tLCPolSchema.setSchema(tLCPolDB.getSchema());
                if (!(tLCPolSchema.getPaytoDate().equals(tLCPolSchema.
                        getPayEndDate()) ||
                      tLCPolSchema.getPayIntv() == -1))
                {
                    FDate tFDate = new FDate();
                    Date EndDate = PubFun.calDate(tFDate.getDate(tLCPolSchema.
                            getPaytoDate()), 60, "D", new Date());
                    Date EndForeverDate = PubFun.calDate(tFDate.getDate(
                            tLCPolSchema.
                            getPaytoDate()), 2, "Y", new Date());
                    if (EndForeverDate.before(tFDate.getDate(PubFun.
                            getCurrentDate())))
                    {
                        tState[0] = "终止"; //终止
                        tState[1] = "永久失效";
                        tState[2] = tLCPolSchema.getCValiDate();
                        String pattern = "yyyy-MM-dd";
                        SimpleDateFormat df = new SimpleDateFormat(pattern);
                        tState[3] = df.format(EndForeverDate);
                        return tState;
                    }
                    if (EndDate.before(tFDate.getDate(PubFun.getCurrentDate())))
                    {
                        //EndDate在当前日期的前面
                        tState[0] = "失效"; //失效
                        tState[1] = "暂时失效";
                        tState[2] = tLCPolSchema.getCValiDate();
                        String pattern = "yyyy-MM-dd";
                        SimpleDateFormat df = new SimpleDateFormat(pattern);
                        tState[3] = df.format(EndDate);
                        return tState;
                    }
                }
                LDSysTraceDB tLDSysTraceDB = new LDSysTraceDB();
                tLDSysTraceDB.setPolNo(tPolNo);
                tLDSysTraceDB.setValiFlag("1");
                LDSysTraceSet tLDSysTraceSet = new LDSysTraceSet();
                tLDSysTraceSet = tLDSysTraceDB.query();
                if (tLDSysTraceSet.size() > 0)
                {
                    String tSysTrace = "";
                    for (int i = 1; i <= tLDSysTraceSet.size(); i++)
                    {
                        tSysTrace = tSysTrace +
                                    tLDSysTraceSet.get(i).getCreatePos();
                    }
                    tState[0] = "暂停";
                    tState[1] = tSysTrace;
                    tState[2] = tLCPolSchema.getCValiDate();
                    tState[3] = tLCPolSchema.getEndDate();
                    return tState;
                }
                if (!tLCPolSchema.getCValiDate().equals(tLCPolSchema.
                        getLastRevDate()))
                {
                    tState[0] = "复效";
                    tState[1] = "";
                    tState[2] = tLCPolSchema.getCValiDate();
                    tState[3] = tLCPolSchema.getEndDate();
                    return tState;

                }
                else
                {
                    tState[0] = "有效";
                    tState[1] = "";
                    tState[2] = tLCPolSchema.getCValiDate();
                    tState[3] = tLCPolSchema.getEndDate();
                    return tState;

                }

            }
            else
            {
                LBPolDB tLBPolDB = new LBPolDB();
                tLBPolDB.setPolNo(tPolNo);
                if (tLBPolDB.getInfo())
                {
                    tState[0] = "终止";
                    String tEdor = "";
//          if (CodeJudge.judgeCodeType(tLBPolDB.getEdorNo(), "42") ||
//              CodeJudge.judgeCodeType(tLBPolDB.getEdorNo(), "43")) { //个人批单号码
//            LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
//            tLPEdorMainDB.setPolNo(tPolNo);
//            tLPEdorMainDB.setEdorNo(tLBPolDB.getEdorNo());
//            LPEdorMainSet tLPEdorMainSet = new LPEdorMainSet();
//            tLPEdorMainSet = tLPEdorMainDB.query();
//            if (tLPEdorMainSet.size() == 0) {
//              tLPEdorMainDB = new LPEdorMainDB();
//              tLPEdorMainDB.setPolNo(tLBPolDB.getMainPolNo());
//              tLPEdorMainDB.setEdorNo(tLBPolDB.getEdorNo());
//              tLPEdorMainSet = tLPEdorMainDB.query();
//            }
//
//            for (int i = 1; i <= tLPEdorMainSet.size(); i++) {
//              LDCodeDB tLDCodeDB = new LDCodeDB();
//              tLDCodeDB.setCode(tLPEdorMainSet.get(i).getEdorType());
//              tLDCodeDB.setCodeType("type_pg");
//              tLDCodeDB.getInfo();
//              tEdor = tEdor + tLDCodeDB.getCodeName();
//            }
//          }
//          if (CodeJudge.judgeCodeType(tLBPolDB.getEdorNo(), "52")) { //理赔
//            tEdor = tEdor + "理赔给付终止";
//          }
                    tState[1] = tEdor;
                    tState[2] = tLBPolDB.getCValiDate();
                    tState[3] = tLBPolDB.getMakeDate();
                    return tState;
                }
                else
                {
                    tState[0] = "无此保单";
                    tState[1] = "";
                    tState[2] = "";
                    tState[3] = "";
                    return tState;
                }

            }

        }
        else
        { //集体保单号
            tState[0] = "集体保单";
            tState[1] = "";
            tState[2] = "";
            tState[3] = "";
            return tState;
        }
    }

    public static boolean checkDate(String StateDate, String EndDate)
    {
        String flag = "";
        FDate tFDate = new FDate();
        Date tStateDate = tFDate.getDate(StateDate);
        Date tEndDate = tFDate.getDate(EndDate);
        if (tStateDate.after(tEndDate))
        {
            flag = "0";
            System.out.println("开始日期晚于结束日期！！！！");
            return false;
        }
        else
        {
            flag = "1";
            System.out.println("开始日期早于结束日期！！！");
            return true;
        }
    }

    public static LMDutyGetClmSchema getGetDutyName(String tGetDutyCode,
            String tGetDutyKind)
    {
        LMDutyGetClmSchema tLMDutyGetClmSchema = new LMDutyGetClmSchema();
        LMDutyGetClmDB tLMDutyGetClmDB = new LMDutyGetClmDB();
        tLMDutyGetClmDB.setGetDutyCode(tGetDutyCode);
        tLMDutyGetClmDB.setGetDutyKind(tGetDutyKind);
        if (tLMDutyGetClmDB.getInfo())
        {
            tLMDutyGetClmSchema.setSchema(tLMDutyGetClmDB.getSchema());
        }
        return tLMDutyGetClmSchema;
    }

    public static String getAppClmUWer(String tAppGrade, String tOperator)
    {
        LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
        tLLClaimUserDB.setUserCode(tOperator);
        if (!tLLClaimUserDB.getInfo())
        {
            return "";
        }
        LLClaimUserSchema tLowerLLClaimUserSchema = new LLClaimUserSchema();
        tLowerLLClaimUserSchema.setSchema(tLLClaimUserDB.getSchema());

        if (tLowerLLClaimUserSchema.getUpUserCode() == null ||
            tLowerLLClaimUserSchema.getUpUserCode().length() == 0)
        {
            return "";
        }
        tLLClaimUserDB = new LLClaimUserDB();
        tLLClaimUserDB.setUserCode(tLowerLLClaimUserSchema.getUpUserCode());
        if (!tLLClaimUserDB.getInfo())
        {
            return "";
        }
        LLClaimUserSchema tUpperLLClaimUserSchema = new LLClaimUserSchema();
        tUpperLLClaimUserSchema.setSchema(tLLClaimUserDB.getSchema());

        if (tAppGrade == null || tAppGrade.length() == 0)
        { //如果没有申请级别要求，按照权限树方向确定审核人
            return tUpperLLClaimUserSchema.getUserCode();
        }
        else
        { //如果有申请级别要求
            if (tUpperLLClaimUserSchema.getClaimPopedom().toUpperCase().
                compareTo(
                        tAppGrade.toUpperCase()) >= 0)
            //上级审核人权限符合
            {
                return tUpperLLClaimUserSchema.getUserCode();
            }
            else
            { //上级审核人员权限不符
//================delete for test == zhangtao ===
//        if (tUpperLLClaimUserSchema.getHandleFlag() != null &&
//            tUpperLLClaimUserSchema.getHandleFlag().equals("1"))
//        //如果是必须经手人员
//        {
//          return tUpperLLClaimUserSchema.getUserCode();
//        }
//================delete for test == zhangtao ===
                return getAppClmUWer(tAppGrade,
                                     tUpperLLClaimUserSchema.getUserCode());
            }
        }
    }

    public static String Display_Year(String date)
    {
        String EndDate = "";
        if (!((date == null) || date.equals("")))
        {
            String t = "";
            String t1 = date.substring(0, 4);
            String t2;
            if (date.substring(5, 6).equals("0"))
                t2 = date.substring(6, 7);
            else
                t2 = date.substring(5, 7);
            String t3;
            if (date.substring(8, 9).equals("0"))
                t3 = date.substring(9, 10);
            else
                t3 = date.substring(8, 10);
            t = t1 + "年" + t2 + "月" + t3 + "日";
            System.out.println("转换后的日期是" + t);
            EndDate = t;
        }
        return EndDate;
    }

    public static String show_People(String code)
    {
        String Name = "";
        code = code.trim();
        LDUserDB tLDUserDB = new LDUserDB();
        if (!(code == null || code.equals("")))
        {
            tLDUserDB.setUserCode(code);
            if (tLDUserDB.getInfo())
                Name = tLDUserDB.getUserName().trim();
            System.out.println("当前的操作人员是" + Name.trim());
        }
        return Name;
    }

    /**************************************************************************************
     * Name         :getStr
     * Function     :在进行前台打印时，将后台存储的换行符号@@Enter去掉。
     * parameter    :strin----需要进行转换的字段
     * return value :字符串
     * Author       :LiuYansong
     */
    public static String getStr(String strin)
    {
        String StrOut = "";
        String array[] = new String[100];
        String str1 = "@@Enter" + strin.trim() + "@@Enter";
        int t = 0;
        for (int i = 0; i < str1.length() - 6; i++)
        {
            if ((str1.substring(i, i + 7).trim().equals("@@Enter")))
            {
                array[t] = String.valueOf(i);
                t = t + 1;
            }
        }
        //对该数组进行循环
        for (int h = 0; h < t - 1; h++)
        {
            int count1 = Integer.parseInt(array[h]);
            int count2 = Integer.parseInt(array[h + 1]);
            StrOut = StrOut + str1.substring(count1 + 7, count2);
            System.out.println(StrOut);
        }
        return StrOut;
    }
}
