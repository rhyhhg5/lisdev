package com.sinosoft.lis.llcase;

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author LiuYansong
 * @version 1.0
 */
public class ReportBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  String People = "";
  String tRptNo = "";
  String CaseState = "";
  String Name = "";
  private LLReportBL  mLLReportBL = new LLReportBL();
  private LLReportSet mLLReportSet = new LLReportSet();
  private LLSubReportBL mLLSubReportBL=new LLSubReportBL();
  private LLSubReportBLSet mLLSubReportBLSet=new LLSubReportBLSet();

  private GlobalInput mG = new GlobalInput();
  private LCPolSet tLCPolSet = new LCPolSet();
  private LDSysTraceSet mLDSysTraceSet = new LDSysTraceSet();

  public ReportBL() {}

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
      System.out.println("djsklfsjdl......."+cOperate);
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;

    //数据查询业务处理
    if (cOperate.equals("QUERY||MAIN"))
    {
      mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
      if(!queryData())
        return false;
      System.out.println("---queryData---");
    }
    if(cOperate.equals("RgtQueryRpt"))
    {
      System.out.println("开始接收数据");
      mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
      System.out.println("接收的数据是"+mG.ManageCom);
      if(!RgtQueryRpt())
        return false;
    }
    // 数据操作业务处理
    if (cOperate.equals("INSERT") || cOperate.equals("UPDATE||MAIN"))
    {
       System.out.println(".......");
      if(!checkData())
        return false;
//      if (!dealData())
//        return false;
    boolean deal = true;
     if ( cOperate.equals("INSERT"))
     {
         deal = dealInsert();
     }else
     {
        deal = dealUpdate();
     }
      if (!deal)
      {
          CError.buildErr(this,"处理失败");
          return false;
      }
      System.out.println("---dealData---");
      PubSubmit pubSubmit = new PubSubmit();
    if (!pubSubmit.submitData(this.mResult, ""))
    {
        this.mErrors.copyAllErrors( pubSubmit.mErrors );
        return false;
    }


    }
    if(cOperate.equals("RETURN"))
    {
      if(!returnData())
        return false;
    }
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealData()
  {
    //准备给后台的数据
    if(!dealInsert())
	{
	  // @@错误处理
	  CError tError = new CError();
	  tError.moduleName = "ReportBL";
	  tError.functionName = "prepareOutputData";
	  tError.errorMessage = "准备给后台的数据!";
	  this.mErrors .addOneError(tError) ;
	  return false;
    }
    //数据提交
//    ReportBLS tReportBLS = new ReportBLS();
//    System.out.println("Start Report BL Submit...");
//
//    if (!tReportBLS.submitData(mInputData,mOperate))
//
//    {
//      // @@错误处理
//      this.mErrors.copyAllErrors(tReportBLS.mErrors);
//      CError tError = new CError();
//      tError.moduleName = "ReportBL";
//      tError.functionName = "submitData";
//      tError.errorMessage = "数据提交失败!";
//      this.mErrors .addOneError(tError) ;
//      return false;
//    }

    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    // 暂交费查询条件
    System.out.println("此时的操作符号是"+mOperate);
    if(mOperate.equals("RgtQueryRpt"))
    {
      People = (String)cInputData.get(0);
      mLLReportBL.setSchema((LLReportSchema)cInputData.getObjectByObjectName("LLReportSchema",0));
      System.out.println("接收的报案号码是"+mLLReportBL.getRptNo());
    }
    if (mOperate.equals("QUERY||MAIN"))
    {
      People = (String)cInputData.get(0);
      mLLReportBL.setSchema((LLReportSchema)cInputData.getObjectByObjectName("LLReportSchema",0));
    }
    if (mOperate.equals("INSERT")|| mOperate.equals("UPDATE||MAIN"))
    {
      mLLReportSet.set((LLReportSet)cInputData.getObjectByObjectName("LLReportSet",0));
      mLLSubReportBLSet.set((LLSubReportSet)cInputData.getObjectByObjectName("LLSubReportSet",0));
      mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    }
    if(mOperate.equals("RETURN"))
    {
      tRptNo = (String)cInputData.get(0);
      System.out.println("报案号码是"+tRptNo);
    }
    return true;
  }

  /**
   * 查询符合条件的暂交费信息
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean queryData()
  {
    LLReportDB tLLReportDB = new LLReportDB();
    LLReportSchema tLLReportSchema=new LLReportSchema();
    tLLReportSchema=mLLReportBL.getSchema();
    tLLReportDB.setSchema(tLLReportSchema);
    mLLReportSet.set(tLLReportDB.query());
    //显示出查询语句的函数
    SQLString sqlObj = new SQLString("LLReport");
    LLReportSchema aLLReportSchema = mLLReportBL.getSchema();
    sqlObj.setSQL(5,aLLReportSchema);
    String sql = sqlObj.getSQL();

    if (sql.toLowerCase().indexOf("where")==-1)
    {
      System.out.println("管理机构是"+mG.ManageCom.trim());
      sql = sql + " where 1=1  and MngCom like '"+mG.ManageCom.trim()+"%' ";
      System.out.println("没有添加People的查询语句是"+sql);
    }
    else
    {
      sql = sql + " and MngCom like '"+mG.ManageCom.trim()+"%' ";
    }
    if (People.length()>0)
    {
      sql = sql + " and rptno in(select rptno from LLSubReport where CustomerName='"+People+"') order by rptno ";
      System.out.println("添加了People查询语句是"+sql);
    }
    ExeSQL exesql = new ExeSQL();
    SSRS ssrs = new SSRS();

    ssrs=exesql.execSQL(sql);
    LLReportSet yLLReportSet = new LLReportSet();

    for(int count=1;count<=ssrs.getMaxRow();count++)
    {
      LLReportSchema yLLReportSchema = new LLReportSchema();
      yLLReportSchema.setRptNo(ssrs.GetText(count,1));
   //   yLLReportSchema.setRptObj(ssrs.GetText(count,2));
   //   yLLReportSchema.setRptObjNo(ssrs.GetText(count,3));
      yLLReportSchema.setMakeDate(ssrs.GetText(count,22));

      SSRS ssrs2 = new SSRS();
      String sql2 = "select CustomerNo,CustomerName,AccidentType from LLSubReport where RptNo = '"+ssrs.GetText(count,1)+"'";
      ssrs2 = exesql.execSQL(sql2);
      if(ssrs2.getMaxRow()>0)
      {
        yLLReportSchema.setRptorAddress(ssrs2.GetText(1,1));
        yLLReportSchema.setRptorName(ssrs2.GetText(1,2));
   //     yLLReportSchema.setAgentCode(ssrs.GetText(1,3));
      }
      else
        yLLReportSchema.setRptorName("无记录");
      yLLReportSet.add(yLLReportSchema);
    }

    if (tLLReportDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLReportDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "LLReportBL";
      tError.functionName = "queryData";
      tError.errorMessage = "保单查询失败!";
      this.mErrors.addOneError(tError);
      mLLReportSet.clear();
      return false;
    }
    if (ssrs.getMaxRow() == 0)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LLReportBL";
      tError.functionName = "queryData";
      tError.errorMessage = "未找到相关数据!";
      this.mErrors.addOneError(tError);
      mLLReportSet.clear();
      return false;
	}

	mResult.clear();
	mResult.add(yLLReportSet);

	return true;

  }

  /**
   * 校验传入的暂交费收据号是否合法
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkData()
  {
    int a = mLLSubReportBLSet.size();
    if(a==0)
    {
      this.mErrors.copyAllErrors(mLLSubReportBLSet.mErrors);
      CError tError = new CError();
      tError.moduleName = "LLReportBL";
      tError.functionName = "queryData";
      tError.errorMessage = "没有分案信息，不能进行保存！";
      this.mErrors.addOneError(tError);
      mLLReportSet.clear();
      return false;
    }
    for(int b=1;b<=a;b++)
    {
      LLSubReportSchema aaLLSubReportSchema = new LLSubReportSchema();
      aaLLSubReportSchema.setSchema(mLLSubReportBLSet.get(b));
      System.out.println("事故类型是＝＝＝＝"+aaLLSubReportSchema.getAccidentType());
      if(aaLLSubReportSchema.getAccidentType().equals(null)||aaLLSubReportSchema.getAccidentType().equals(""))
      {
        this.mErrors.copyAllErrors(mLLSubReportBLSet.mErrors);
        CError tError = new CError();
        tError.moduleName = "LLReportBL";
        tError.functionName = "queryData";
        tError.errorMessage = "事故类型不能是空，请您录入出险人的事故类型！！！！";
        this.mErrors.addOneError(tError);
        mLLReportSet.clear();
        return false;
      }
      System.out.println("开始执行判断事故者投保信息的语句");
      if(aaLLSubReportSchema.getCustomerName().trim()==null||
         aaLLSubReportSchema.getCustomerName().trim().equals(""))
      {
        this.buildError("ReportBL","事故信息中的事故者名称不能为空");
        return false;
      }
      LDPersonDB tLDPersonDB = new LDPersonDB();
      tLDPersonDB.setCustomerNo(aaLLSubReportSchema.getCustomerNo());
      if(!tLDPersonDB.getInfo())
      {
        this.buildError("ReportBL","原因是：1.该客户是以无名单形式进行投保的，请先做补名单操作；2.该客户不存在！");
        return false;
      }

      System.out.println("事故者姓名是"+tLDPersonDB.getSchema().getName());
      if(tLDPersonDB.getSchema().getName().trim().equals("无名单"))
      {
        this.buildError("ReportBL","该客户是以无名单的形式投保的，请先做补名单操作，再进行报案处理！！");
        return false;
      }
    }
    //对出险日期和当前日期的校验
//    String AccidentDate = mLLReportSet.get(1).getAccidentDate();
//    if(AccidentDate==null||AccidentDate.equals(""))
//      return false;
//    if(!CaseFunPub.checkDate(AccidentDate,PubFun.getCurrentDate()))
//    {
//        CError tError = new CError();
//        tError.moduleName = "LLReportBL";
//        tError.functionName = "queryData";
//        tError.errorMessage = "出险日期晚于报案受理日期，不能进行保存！！！！";
//        this.mErrors.addOneError(tError);
//        mLLReportSet.clear();
//        return false;
//    }
    return true;
  }
  /**
   * 准备需要保存的数据
   * @return boolean
   */
  private boolean prepareOutputData()
{
    return true;
}
/**
 * 插入数据准备
 * @return boolean
 */
private boolean dealInsert()
  {
  MMap tmpMap = new MMap();
    LLReportSchema tLLReportSchema = mLLReportSet.get(1);

    String strLimit=PubFun.getNoLimit(mG.ManageCom);
    tLLReportSchema.setRptNo(PubFun1.CreateMaxNo("RPTNO",strLimit));
    tLLReportSchema.setOperator(mG.Operator);
    tLLReportSchema.setMngCom(mG.ManageCom);
    tLLReportSchema.setRgtFlag("0");                 //立案标志为0表示未立案
    tLLReportSchema.setRptDate(PubFun.getCurrentDate());
    tLLReportSchema.setMakeDate(PubFun.getCurrentDate());
    tLLReportSchema.setMakeTime(PubFun.getCurrentTime());
    tLLReportSchema.setModifyDate(PubFun.getCurrentDate());
    tLLReportSchema.setModifyTime(PubFun.getCurrentTime());
    tLLReportSchema.setAvaiFlag("1");
    //mLLReportSet.set(1,tLLReportSchema);
    Name = CaseFunPub.show_People(mG.Operator);

    int n=mLLSubReportBLSet.size();
    LLReportRelaSet tLLReportRelaSet = new LLReportRelaSet();
    LLSubReportSet tLLSubReportSet = new LLSubReportSet();
    LDSysTraceSet tLDSysTraceSet = new LDSysTraceSet();
    for(int i=1;i<=n;i++)
    {


      LLSubReportSchema tLLSubReportSchema=mLLSubReportBLSet.get(i);
      //判断以该客户为投保人或被保人的保单是否属于该管理机构

      System.out.println("该出险人的出险事故类型为"+tLLSubReportSchema.getAccidentType());
      mLDSysTraceSet.clear();

      if(tLLSubReportSchema.getAccidentType().equals("0"))
      { //死亡
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setInsuredNo(tLLSubReportSchema.getCustomerNo());
        tLCPolSet=tLCPolDB.query();
        int y = tLCPolSet.size();
        System.out.println("该客户共有"+y+"张保单;");

        for(int t=1;t<=y;t++)
        {

          LCPolSchema tLCPolSchema = new LCPolSchema();
          LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
          tLCPolSchema.setSchema(tLCPolSet.get(t));
          System.out.println("该客户的客户号为"+tLCPolSchema.getInsuredNo());
          System.out.println("该客户的保单为"+tLCPolSchema.getPolNo());
          tLDSysTraceSchema.setPolNo(tLCPolSchema.getPolNo());
          System.out.println("2003-1-15"+mG.Operator);

          tLDSysTraceSchema.setOperator(mG.Operator);
          System.out.println("aa"+mG.ManageCom);
          tLDSysTraceSchema.setMakeDate(PubFun.getCurrentDate());
          tLDSysTraceSchema.setMakeTime(PubFun.getCurrentTime());
          tLDSysTraceSchema.setManageCom(mG.ManageCom);
          tLDSysTraceSchema.setCreatePos("报案阶段");
          tLDSysTraceSchema.setPolState("4001");
          tLDSysTraceSchema.setValiFlag("1");
          tLDSysTraceSchema.setRemark("报案阶段死亡报案");
          tLDSysTraceSchema.setModifyDate(PubFun.getCurrentDate());
          tLDSysTraceSchema.setModifyTime(PubFun.getCurrentTime());
          tLDSysTraceSchema.setOperator2(mG.Operator);
          System.out.println("数据添加成功");
          tLDSysTraceSet.add(tLDSysTraceSchema);

          //sxy-2003-09-10修改该保单状态为终止(死亡报案终止).
          VData tVData = new VData();
          System.out.println("bbb");
          System.out.println(tLCPolSchema.getPolState());
          if (tLCPolSchema.getPolState()==null||tLCPolSchema.getPolState().equals(""))
          {
            tLCPolSchema.setPolState( "0401");
          }
          else
          {
            if (tLCPolSchema.getPolState().length()<4)
            {
              tLCPolSchema.setPolState( "0401" + tLCPolSchema.getPolState());
            }
            else
            {
              tLCPolSchema.setPolState( "0401" + tLCPolSchema.getPolState().substring(0,4));
            }
          }

          tmpMap.put( tLCPolSchema ,"UPDATE");


        }

      }

      String subRptNo = PubFun1.CreateMaxNo("SUBRPTNO",strLimit);
      //关联表
      LLReportRelaSchema tLLReportRelaSchema = new LLReportRelaSchema();
      tLLReportRelaSchema.setRptNo(tLLReportSchema.getRptNo());
      tLLReportRelaSchema.setSubRptNo(subRptNo);
      tLLReportRelaSet.add(tLLReportRelaSchema);

     // System.out.println("sxydddd");
      LLSubReportBL tLLSubReportBL=new LLSubReportBL();
      tLLSubReportBL.setSchema(tLLSubReportSchema);
      strLimit=PubFun.getNoLimit(mG.ManageCom);
      tLLSubReportBL.setSubRptNo(subRptNo);
    //  tLLSubReportBL.setRptNo(tLLReportSchema.getRptNo());
      tLLSubReportBL.setDefaultValue();
      tLLSubReportBL.setMngCom(mG.ManageCom);
      tLLSubReportBL.setOperator(mG.Operator);
      tLLSubReportSchema=tLLSubReportBL.getSchema();
      tLLSubReportSet.add(tLLSubReportSchema);
    }
    mInputData.clear();
    tmpMap.put(tLDSysTraceSet,"DELETE&INSERT" );
    tmpMap.put(tLLReportSchema,"INSERT");
    tmpMap.put(tLLReportRelaSet,"INSERT");
    tmpMap.put( tLLSubReportSet, "INSERT");

    mResult.clear();
    mResult.add(Name);
    mResult.add(tLLReportSchema);
    mResult.add(tLLSubReportSet);
    mResult.add( tmpMap );

    return true;
  }

  /**
 * 插入数据准备
 * @return boolean
 */
private boolean dealUpdate()
  {
  MMap tmpMap = new MMap();
    LLReportSchema tLLReportSchema = mLLReportSet.get(1);

    String strLimit=PubFun.getNoLimit(mG.ManageCom);
  //  tLLReportSchema.setRptNo(PubFun1.CreateMaxNo("RPTNO",strLimit));
    tLLReportSchema.setOperator(mG.Operator);
    tLLReportSchema.setMngCom(mG.ManageCom);
    tLLReportSchema.setRgtFlag("0");                 //立案标志为0表示未立案
    tLLReportSchema.setRptDate(PubFun.getCurrentDate());
    tLLReportSchema.setMakeDate(PubFun.getCurrentDate());
    tLLReportSchema.setMakeTime(PubFun.getCurrentTime());
    tLLReportSchema.setModifyDate(PubFun.getCurrentDate());
    tLLReportSchema.setModifyTime(PubFun.getCurrentTime());
    tLLReportSchema.setAvaiFlag("1");
    //mLLReportSet.set(1,tLLReportSchema);
    Name = CaseFunPub.show_People(mG.Operator);

    int n=mLLSubReportBLSet.size();
    LLReportRelaSet tLLReportRelaSet = new LLReportRelaSet();
    LLSubReportSet tLLSubReportSet = new LLSubReportSet();
    for(int i=1;i<=n;i++)
    {
      LLSubReportSchema tLLSubReportSchema=mLLSubReportBLSet.get(i);
      //判断以该客户为投保人或被保人的保单是否属于该管理机构

      System.out.println("该出险人的出险事故类型为"+tLLSubReportSchema.getAccidentType());
      mLDSysTraceSet.clear();
      if(tLLSubReportSchema.getAccidentType().equals("0"))
      { //死亡

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setInsuredNo(tLLSubReportSchema.getCustomerNo());
        tLCPolSet=tLCPolDB.query();
        int y = tLCPolSet.size();
        System.out.println("该客户共有"+y+"张保单;");
        LDSysTraceSet tLDSysTraceSet = new LDSysTraceSet();
        for(int t=1;t<=y;t++)
        {

          LCPolSchema tLCPolSchema = new LCPolSchema();
          LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
          tLCPolSchema.setSchema(tLCPolSet.get(t));
          System.out.println("该客户的客户号为"+tLCPolSchema.getInsuredNo());
          System.out.println("该客户的保单为"+tLCPolSchema.getPolNo());

          tLDSysTraceSchema.setPolNo(tLCPolSchema.getPolNo());
          System.out.println("2003-1-15"+mG.Operator);

          tLDSysTraceSchema.setOperator(mG.Operator);
          System.out.println("aa"+mG.ManageCom);
          tLDSysTraceSchema.setMakeDate(PubFun.getCurrentDate());
          tLDSysTraceSchema.setMakeTime(PubFun.getCurrentTime());
          tLDSysTraceSchema.setManageCom(mG.ManageCom);
          tLDSysTraceSchema.setCreatePos("报案阶段");
          tLDSysTraceSchema.setPolState("4001");
          tLDSysTraceSchema.setValiFlag("1");
          tLDSysTraceSchema.setRemark("报案阶段死亡报案");
          tLDSysTraceSchema.setModifyDate(PubFun.getCurrentDate());
          tLDSysTraceSchema.setModifyTime(PubFun.getCurrentTime());
          tLDSysTraceSchema.setOperator2(mG.Operator);
          System.out.println("数据添加成功");
          tLDSysTraceSet.add(tLDSysTraceSchema);

          //sxy-2003-09-10修改该保单状态为终止(死亡报案终止).
          VData tVData = new VData();
          System.out.println("bbb");
          System.out.println(tLCPolSchema.getPolState());
          if (tLCPolSchema.getPolState()==null||tLCPolSchema.getPolState().equals(""))
          {
            tLCPolSchema.setPolState( "0401");
          }
          else
          {
            if (tLCPolSchema.getPolState().length()<4)
            {
              tLCPolSchema.setPolState( "0401" + tLCPolSchema.getPolState());
            }
            else
            {
              tLCPolSchema.setPolState( "0401" + tLCPolSchema.getPolState().substring(0,4));
            }
          }

          tmpMap.put( tLCPolSchema ,"UPDATE");
        }
        tmpMap.put(tLDSysTraceSet,"DELETE&INSERT" );
      }
      //看是否需要创建
      String subRptNo= tLLSubReportSchema.getSubRptNo();
      if ( "".equals( StrTool.cTrim( subRptNo)))
      {
          subRptNo = PubFun1.CreateMaxNo("SUBRPTNO", strLimit);
      }

      //关联表
      LLReportRelaSchema tLLReportRelaSchema = new LLReportRelaSchema();
      tLLReportRelaSchema.setRptNo(tLLReportSchema.getRptNo());
      tLLReportRelaSchema.setSubRptNo(subRptNo);
      tLLReportRelaSet.add(tLLReportRelaSchema);

     // System.out.println("sxydddd");
      LLSubReportBL tLLSubReportBL=new LLSubReportBL();
      tLLSubReportBL.setSchema(tLLSubReportSchema);
      strLimit=PubFun.getNoLimit(mG.ManageCom);
      tLLSubReportBL.setSubRptNo(subRptNo);

      tLLSubReportBL.setDefaultValue();
      tLLSubReportBL.setMngCom(mG.ManageCom);
      tLLSubReportBL.setOperator(mG.Operator);
      tLLSubReportSchema=tLLSubReportBL.getSchema();
      tLLSubReportSet.add(tLLSubReportSchema);
    }
    mInputData.clear();
    String sql0= "delete from LLSubReport where subrptno in ( select subrptno from LLReportRela where rptno='" +tLLReportSchema.getRptNo()+"')";
    String sql = "delete from LLReportRela where rptno='" +tLLReportSchema.getRptNo()+"'";
    tmpMap.put(sql0, "DELETE");
    tmpMap.put( sql,"DELETE");
    tmpMap.put(tLLReportSchema,"UPDATE");
    tmpMap.put(tLLReportRelaSet,"INSERT");
    tmpMap.put( tLLSubReportSet, "INSERT");

    mResult.clear();
    mResult.add(Name);
    mResult.add(tLLReportSchema);
    mResult.add(tLLSubReportSet);
    mResult.add( tmpMap );

    return true;
  }



  /*****************************************************************************
   * returnData函数的说明
   * Function：将选中的数据进行返回到报案的主界面上
   * 返回值：报案表－LLReportSchema
   *        报案分案表－LLSubReportSet
   * 对于案件状态的附值采用CaseFunPub中的方法
   */
  private boolean returnData()
  {
    LLReportDB bLLReportDB = new LLReportDB();
    bLLReportDB.setRptNo(tRptNo);
    LLReportSet bLLReportSet = new LLReportSet();
    bLLReportSet.set(bLLReportDB.query());

    if(bLLReportSet.size()==0)
    {
      this.mErrors.copyAllErrors(bLLReportSet.mErrors);
      CError tError = new CError();
      tError.moduleName = "LLReportBL";
      tError.functionName = "queryData";
      tError.errorMessage = "报案数据返回失败！！！！";
      this.mErrors.addOneError(tError);
    //  mLLReportBLSet.clear();
      return false;
    }
    CaseState = CaseFunPub.getCaseStateByRptNo(tRptNo);
    LLReportSchema bLLReportSchema = new LLReportSchema();
    bLLReportSchema.setSchema(bLLReportSet.get(1));
    String a_Name =CaseFunPub.show_People(bLLReportSchema.getOperator()) ;

    LLSubReportDB bLLSubReportDB = new LLSubReportDB();
 //   bLLSubReportDB.setRptNo(tRptNo);
    LLSubReportSet bLLSubReportSet = new LLSubReportSet();
    bLLSubReportSet.set(bLLSubReportDB.query());
    if(bLLSubReportSet.size()==0)
    {
      this.mErrors.copyAllErrors(bLLSubReportSet.mErrors);
      CError tError = new CError();
      tError.moduleName = "LLReportBL";
      tError.functionName = "queryData";
      tError.errorMessage = "报案分案数据返回失败！！！！";
      this.mErrors.addOneError(tError);
    //  mLLReportBLSet.clear();
      return false;
    }
    mResult.clear();
    mResult.addElement(CaseState);
    mResult.add(a_Name);
    mResult.add(bLLReportSchema);
    mResult.add(bLLSubReportSet);
    return true;
  }
 /*****************************************************************************
  * Name    :RgtQueryRpt
  * Function:立案中的报案查询
  * Notice  ：只查询出案件状态是待立案的（通过报案号码来查询）
  * Author  ：LiuYansong
  * Data    ：2003-7-15
  * 23      :通过indexOf方法的返回值来判断sql语句中是否有where
  */
  private boolean RgtQueryRpt()
  {
    String rpt_managecom="";
    rpt_managecom=mG.ManageCom;

    SQLString sqlObj = new SQLString("LLReport");
    LLReportSchema aLLReportSchema = mLLReportBL.getSchema();
    sqlObj.setSQL(5,aLLReportSchema);
    String sql = sqlObj.getSQL();
    if (sql.toLowerCase().indexOf("where")==-1)
    {
      sql = sql + " where 1=1  and MngCom like '"+mG.ManageCom.trim()+"%' ";
    }
    else
    {
      sql = sql + " and MngCom like '"+mG.ManageCom.trim()+"%' ";
    }

    sql = sql+ " and (RgtFlag <> '1' or  RgtFlag is null) ";
    System.out.println("最终的查询语句是"+sql);
    if (People.length()>0)
    {
      sql = sql + " and rptno in ( select rptno from LLSubReport where CustomerName='"+People+"') order by rptno ";
    }
     ExeSQL exesql = new ExeSQL();
     SSRS ssrs = new SSRS();
     ssrs=exesql.execSQL(sql);
     LLReportSet yLLReportSet = new LLReportSet();
     for(int count=1;count<=ssrs.getMaxRow();count++)
     {
       LLReportSchema yLLReportSchema = new LLReportSchema();
       yLLReportSchema.setRptNo(ssrs.GetText(count,1));
//       yLLReportSchema.setRptObj(ssrs.GetText(count,2));
//       yLLReportSchema.setRptObjNo(ssrs.GetText(count,3));
       yLLReportSchema.setMakeDate(ssrs.GetText(count,22));
       yLLReportSchema.setMngCom(ssrs.GetText(count,20));
       yLLReportSchema.setOperator(ssrs.GetText(count,21));
       SSRS ssrs2 = new SSRS();
       String sql2 = "select CustomerNo,CustomerName,AccidentType from LLSubReport where RptNo = '"+ssrs.GetText(count,1)+"'";
       ssrs2 = exesql.execSQL(sql2);
       if(ssrs2.getMaxRow()>0)
       {
         yLLReportSchema.setRptorAddress(ssrs2.GetText(1,1));
         yLLReportSchema.setRptorName(ssrs2.GetText(1,2));
//         yLLReportSchema.setAgentCode(ssrs2.GetText(1,3));
       }
       else
         yLLReportSchema.setRptorName("无记录");
       yLLReportSet.add(yLLReportSchema);
     }

     if (mLLReportBL.mErrors.needDealError() == true)
     {
       // @@错误处理
       this.mErrors.copyAllErrors(mLLReportBL.mErrors);
       CError tError = new CError();
       tError.moduleName = "LLReportBL";
       tError.functionName = "queryData";
       tError.errorMessage = "保单查询失败!";
       this.mErrors.addOneError(tError);
 //      mLLReportBLSet.clear();
       return false;
     }
     if (ssrs.getMaxRow() == 0)
     {
       // @@错误处理
       CError tError = new CError();
       tError.moduleName = "LLReportBL";
       tError.functionName = "queryData";
       tError.errorMessage = "未找到相关数据!";
       this.mErrors.addOneError(tError);
 //      mLLReportBLSet.clear();
       return false;
   }
   mResult.clear();
   mResult.add(yLLReportSet);
   return true;
  }

  public static void main(String[] args)
  {
    ReportBL aReportBL = new ReportBL();
    LLReportSchema aLLReportSchema = new LLReportSchema();
    String RptNo = "86000020030500000001";
    VData tVData = new VData();
    tVData.addElement(RptNo);
    aLLReportSchema.setRptNo(RptNo);
    tVData.add(aLLReportSchema);
    aReportBL.submitData(tVData,"RgtQueryRpt");
   }
   public void buildError(String szFunc, String szErrMsg)
   {
     CError cError = new CError( );
     cError.moduleName = "PayNoticeF1PBL";
     cError.functionName = szFunc;
     cError.errorMessage = szErrMsg;
     this.mErrors.addOneError(cError);
   }

}
