/**
 * 
 */
package com.sinosoft.lis.llcase;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.operfee.*;

/**
 * @author maning 理赔终止续期应收
 */
public class LLIndiLJSCancelBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往界面传输数据的容器 */
	MMap mMap = new MMap();

	private VData mResult = new VData();

	private TransferData mTransferData = new TransferData();

	private GlobalInput mGlobalInput = new GlobalInput();

	// 被保险人客户号
	private String mInsuredNo;
	private String mContNo;

	// 作废续期应收
	private LJSPaySet mLJSPaySet = new LJSPaySet();

	/**
	 * 
	 */
	public LLIndiLJSCancelBL() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData, cOperate)) {
			return false;
		}
		System.out.println("---LLIndiLJSCancelBL getInputData---");

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		System.out.println("---LLIndiLJSCancelBL dealData END---");

		// 准备给后台的数据
		if (prepareOutputData()) {
			return false;
		}
		return true;
	}

	/**
	 * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean dealData() {
		String tIndiSQL="";
		if(!"".equals(mInsuredNo))
		{
			//查询被保险人所有的续期催收
			 tIndiSQL = "select distinct b.getnoticeno "
					+ " from lcinsured a,ljspay b,ljspayperson c,lmrisk d "
					+ " where a.contno = b.otherno and b.othernotype='2' "
					+ " and b.getnoticeno = c.getnoticeno "
					+ " and c.riskcode = d.riskcode " + " and d.cpayflag = 'Y' "
					+ " and a.insuredno='" + mInsuredNo 
					+ "' and not exists (select 1 from lcrnewstatelog where contno=a.contno and state != '6' )";			
		}else if(!"".equals(mContNo)){
			//查询保单所有的续期催收
			 tIndiSQL = "select distinct b.getnoticeno "
					+ " from lcinsured a,ljspay b,ljspayperson c,lmrisk d "
					+ " where a.contno = b.otherno and b.othernotype='2' "
					+ " and b.getnoticeno = c.getnoticeno "
					+ " and c.riskcode = d.riskcode " + " and d.cpayflag = 'Y' "
					+ " and a.contno='" + mContNo 
					+ "' and not exists (select 1 from lcrnewstatelog where contno=a.contno and state != '6' )";
		}
		ExeSQL tExeSQL = new ExeSQL();
		System.out.println(tIndiSQL);
		SSRS tGetnoticenoSSRS = tExeSQL.execSQL(tIndiSQL);

		for (int i = 1; i <= tGetnoticenoSSRS.getMaxRow(); i++) {
			String tGetNoticeNo = tGetnoticenoSSRS.GetText(i, 1);

			if ("".equals(tGetNoticeNo)) {
				continue;
			}
			
			System.out.println("作废续期应收："+tGetNoticeNo);
			
			LJSPaySchema tLJSPaySchema = new LJSPaySchema();
			tLJSPaySchema.setGetNoticeNo(tGetNoticeNo);

			TransferData tTransferData = new TransferData();
			// 理赔作废催收原因
			tTransferData.setNameAndValue("CancelMode", "2");

			VData tVData = new VData();
			tVData.addElement(tLJSPaySchema);
			tVData.addElement(mGlobalInput);
			tVData.addElement(tTransferData);

			try {
				IndiLJSCancelUI tIndiLJSCancelUI = new IndiLJSCancelUI();
				if (tIndiLJSCancelUI.submitData(tVData, "INSERT")) {
					tLJSPaySchema.setAccName("撤销成功");
				} else {
					String tErrorInfo = "撤销失败,"
							+ tIndiLJSCancelUI.mCErrors.getFirstError();
					if (tErrorInfo.length() > 60) {
						tErrorInfo = tErrorInfo.substring(0, 60);
					}
					tLJSPaySchema.setAccName(tErrorInfo);
				}
			} catch (Exception ex) {
				tLJSPaySchema.setAccName("撤销异常");
			}

			mLJSPaySet.add(tLJSPaySchema);
		}

		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData, String cOperate) {
		// 从输入数据中得到所有对象
		// 获得全局公共数据
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);

		if (mGlobalInput == null || mTransferData == null) {
			this.buildError("getInputData", "数据不完整");
			return false;
		}

		mInsuredNo = (String) mTransferData.getValueByName("InsuredNo");
		mContNo = (String) mTransferData.getValueByName("ContNo");

		if (("".equals(mInsuredNo)||"null".equals(mInsuredNo))
				&&("".equals(mContNo)||"null".equals(mContNo))) {
			this.buildError("getInputData", "未获得被保险人客户号或合同号失败");
			return false;
		}

		return true;
	}

	/**
	 * 准备返回前台统一存储数据 输出：如果发生错误则返回false,否则返回true
	 */
	private boolean prepareOutputData() {
		mResult.clear();
		mResult.addElement(mLJSPaySet);
		return false;
	}

	/**
	 * 校验业务数据
	 * 
	 * @return
	 */
	private boolean checkData() {
		return true;
	}

	public VData getResult() {
		return mResult;
	}

	public CErrors getErrors() {
		return mErrors;
	}

	/**
	 * 追加错误信息
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLIndiLJSCancelBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86";
        mGlobalInput.ComCode = "86";
        mGlobalInput.Operator = "cm0003";

		TransferData tTransferData = new TransferData();
		VData tVData = new VData();
		tVData.add(mGlobalInput);
		tTransferData.setNameAndValue("ContNo", "000935825000001");
		tTransferData.setNameAndValue("InsuredNo", "");
		tVData.add(tTransferData);
		LLIndiLJSCancelBL tLLIndiLJSCancelBL = new LLIndiLJSCancelBL();
		if(!tLLIndiLJSCancelBL.submitData(tVData, ""))
		{
			System.out.println("错误====");
		}
	}

}
