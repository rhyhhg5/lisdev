package com.sinosoft.lis.llcase;


import com.sinosoft.lis.db.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class LLDealApplicationBL {
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	/** 数据操作字符串 */
	private String mOperate;

	/** 业务处理相关变量 */
	private LDCode1Set tLDCode1Set = new LDCode1Set();

	private String mOperator = "";

	private MMap mMap = new MMap();

	public LLDealApplicationBL() {

	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = cInputData;
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {		
			return false;
		}

	//	if (!prepareOutputData()) {
			//return false;
		//}

		PubSubmit tPubSubmit = new PubSubmit();

		System.out.println("Start LGGROUPScheamBL Submit...");

		if (!tPubSubmit.submitData(mInputData, null)) {
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}

		mInputData = null;
		System.out.println("End LGGROUPScheamBL Submit...");
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * 
	 */
	private boolean getInputData() {
		if ("".equals(mOperate)) {
			buildError("getInputData", "获取操作类型失败");
			return false;
		}

		if (mInputData == null) {
			buildError("getInputData", "获取页面传输数据失败");
			return false;
		}

		mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0);

		if (mGlobalInput == null) {
			buildError("getInputData", "获取操作人信息失败");
			return false;
		}

		mOperator = mGlobalInput.Operator;

		if ("".equals(mOperator)) {
			buildError("getInputData", "获取操作人信息失败");
			return false;
		}

		this.tLDCode1Set = (LDCode1Set) mInputData
				.getObjectByObjectName("LDCode1Set", 0);

		if (tLDCode1Set == null) {
			buildError("getInputData", "没有获取的LDCode1Set");
			return false;
		}


		return true;
	}



	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		
		if("INSERT||MAIN".equals(mOperate)){
			if(!create()){
				return false;
			}
		}
		if("UPDATE||MAIN".equals(mOperate)){
            if(!update ()){
               return false;
            }
    }
		return false;

     //   if("DELETE||MAIN".equals(mOperate)){
       //     if(!delete()){
    //        return false;
        //    }
   //}
}			 
	private boolean create(){
		 delete();
		 for(int i = 1; i <=tLDCode1Set.size();i++){
	    	   LDCode1DB tLDCode1DB = new LDCode1DB();
	    	   LDCode1Schema tLDCode1Schema = new LDCode1Schema();
	    	   tLDCode1Schema = tLDCode1Set.get(i);
	    	   tLDCode1DB.setSchema(tLDCode1Schema);
	    	   if(!tLDCode1DB.insert()){
	    		    
	    		     this.mErrors.copyAllErrors(tLDCode1DB.mErrors);
	    	         CError tError = new CError();
	    	         tError.moduleName = "LLDealApplicationBL";
	    	         tError.functionName = "create";
	    	         tError.errorMessage = "保存数据失败!";
	    	         this.mErrors .addOneError(tError) ;

	    	         return false; 
	    	   }
	    	   
	       }
		 return true;
		
	}
	private boolean update(){
		  delete();
		 for(int i = 1; i <=tLDCode1Set.size();i++){
	    	   LDCode1DB tLDCode1DB = new LDCode1DB();
	    	   LDCode1Schema tLDCode1Schema = new LDCode1Schema();
	    	   tLDCode1Schema = tLDCode1Set.get(i);
	    	   tLDCode1DB.setSchema(tLDCode1Schema);
	    	   if(!tLDCode1DB.insert()){
	    		    
	    		     this.mErrors.copyAllErrors(tLDCode1DB.mErrors);
	    	         CError tError = new CError();
	    	         tError.moduleName = "LLDealApplicationBL";
	    	         tError.functionName = "update";
	    	         tError.errorMessage = "保存数据失败!";
	    	         this.mErrors .addOneError(tError) ;

	    	         return false; 
	    	   }
	    	   
	       }
        return true;      
}
	
	private boolean delete(){ 

		 for(int i = 1; i <=tLDCode1Set.size();i++){
	    	   LDCode1DB tLDCode1DB = new LDCode1DB();
	    	   LDCode1Schema tLDCode1Schema = new LDCode1Schema();
	    	   tLDCode1Schema = tLDCode1Set.get(i);
	    	   tLDCode1DB.setCodeType("LLRegStyle");
	    	   tLDCode1DB.setCode(tLDCode1Schema.getCode());
	    	   if(!tLDCode1DB.deleteSQL()){
	    		    
	    		     this.mErrors.copyAllErrors(tLDCode1DB.mErrors);
	    	         CError tError = new CError();
	    	         tError.moduleName = "LLDealApplicationBL";
	    	         tError.functionName = "delete";
	    	         tError.errorMessage = "保存数据失败!";
	    	         this.mErrors .addOneError(tError) ;

	    	         return false; 
	    	   }
	    	   
	       }
       return true;      
}
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLPrepaidGrpContBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {

	}



}
