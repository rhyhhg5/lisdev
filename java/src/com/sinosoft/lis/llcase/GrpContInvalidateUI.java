package com.sinosoft.lis.llcase;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class GrpContInvalidateUI {
	
	 public  CErrors mErrors = new CErrors();
	  private VData mInputData = new VData();
	  private VData mResult = new VData();
	  private String mOperate;

	  public GrpContInvalidateUI() {}
	  public boolean submitData(VData cInputData,String cOperate)
	  {
	    this.mOperate = cOperate;
	    GrpContInvalidateBL mGrpContInvalidateBL = new GrpContInvalidateBL();

	    System.out.println("---UI BEGIN---");
	    if (mGrpContInvalidateBL.submitData(cInputData,mOperate) == false)
	    {
	      this.mErrors.copyAllErrors(mGrpContInvalidateBL.mErrors);
	      CError tError = new CError();
	      tError.moduleName = "GrpContInvalidateUI";
	      tError.functionName = "submitData";
	      tError.errorMessage = "����ʧ�ܣ�";
	      this.mErrors .addOneError(tError) ;
	      mResult.clear();
	      return false;
	    }
	    return true;
	  }

	  public VData getResult()
	  {
	  	return mResult;
	  }


}
