package com.sinosoft.lis.llcase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.f1j.ss.BookModelImpl;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.DOMBuilder;
import org.jdom.output.XMLOutputter;

/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: 将社保类理赔信息导入文件解析成指定格式的XML文件 </p>
 * <p>Copyright: Copyright (c) 2004 </p>
 * <p>Company: Sinosoft </p>
 * @author Xx
 * @version 6.0
 * @date 2005-09-27
 */
public class SIXmlParser
{
    public CErrors mErrors = new CErrors();

    private String m_strBatchNo = "";
    private String mRgtNo = "";
    // 保存磁盘投保文件的文件名
    private String m_strFileName = "";
    private String m_strPathName = "";

    // 保存列名元素名映射文件的文件名
    private String m_strConfigFileName = "";

    // 保存Sheet相关的信息，如最大行，最大列，当前处理到的行等
    private Hashtable[] m_hashInfo = null;
    private String[] mTitle = null;

    // 常量定义
    private static String PROP_MAX_ROW = "max_row";
    private static String PROP_MAX_COL = "max_col";
    private static String PROP_CUR_ROW = "cur_row";
    private static String PROP_COL_NAME = "col_name";

    private static final int SHEET_COUNT = 1;

    /**sheet代表的数据集,以及索引所在的列*/
    //案件信息
    private final int SHEET_CASEINFO = 0;
    private final int STRID_COL_INSURED = 0;

    private static final int ROW_LIMIT = 50; // 一次处理的行数
    private int contIndex = 0; //一次最多处理的合同数

    private BookModelImpl m_book = new BookModelImpl();

    // 用来保存每一个小部分生成的XML文件名
    private List m_listFiles = new ArrayList();

    /**
     * 构造函数
     */
    public SIXmlParser() {
        m_hashInfo = new Hashtable[SHEET_COUNT];
        for (int nIndex = 0; nIndex < SHEET_COUNT; nIndex++) {
            m_hashInfo[nIndex] = new Hashtable();
            m_hashInfo[nIndex].put(PROP_CUR_ROW, "1"); // 从第二行开始解析
        }
    }

    // 设置要处理的文件名，并获取导入批次号
    public boolean setFileName(String strFileName) {
        File file = new File(strFileName);
        if (!file.exists()) {
            buildError("setFileName", "请先上传导入文件！");
            return false;
        }
        m_strFileName = strFileName;
        m_strPathName = file.getParent();

        int nPos = strFileName.lastIndexOf('.');

        if (nPos == -1) {
            nPos = strFileName.length();
        }

        m_strBatchNo = strFileName.substring(strFileName.lastIndexOf("/") + 1,
                                             nPos);
        nPos = m_strBatchNo.lastIndexOf('\\');
        if (nPos != -1) {
            m_strBatchNo = m_strBatchNo.substring(nPos + 1);
        }
        nPos = m_strBatchNo.lastIndexOf('/');
        if (nPos != -1) {
            m_strBatchNo = m_strBatchNo.substring(nPos + 1);
        }
        return true;
    }

    // 设置配置文件名
    public boolean setConfigFileName(String strConfigFileName) {
        File file = new File(strConfigFileName);
        if (!file.exists()) {
            buildError("setFileName", "请上传配置文件到指定路径！");
            return false;
        }

        m_strConfigFileName = strConfigFileName;
        return true;
    }

    public String getFileName() {
        return m_strFileName;
    }

    public String getConfigFileName() {
        return m_strConfigFileName;
    }

    /**
     *  转换操作，将磁盘投保文件转换成指定格式的XML文件。
     * @return boolean
     */
    public boolean transform() {
        String strFileName = "";
        int nCount = 0;
        try {
            verify();
//            while (hasMoreData()) {
                strFileName = m_strPathName + File.separator
                              + m_strBatchNo + String.valueOf(nCount++)
                              + ".xml";
                System.out.println("strFileName:" + strFileName);
                genPart(strFileName);

                m_listFiles.add(strFileName);
//            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            buildError("transfrom", ex.getMessage());
            return false;
        }
        return true;
    }

    public String[] getDataFiles() {
        String[] files = new String[m_listFiles.size()];
        for (int nIndex = 0; nIndex < m_listFiles.size(); nIndex++) {
            files[nIndex] = (String) m_listFiles.get(nIndex);
        }
        return files;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "GrpPolVTSParser";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 校验格式
     * 要求磁盘投保文件中的数据按照唯一标识，从小到大排列
     * @throws Exception
     */
    private void verify() throws Exception {
        if (m_strFileName.equals("")) {
            throw new Exception("请先调用setFileName函数设置要处理的文件名。");
        }

        m_book.read(m_strFileName, new com.f1j.ss.ReadParams());
        if (m_book.getNumSheets() < SHEET_COUNT) {
            throw new Exception("磁盘投保文件不完整，缺少Sheet。");
        }

        for (int nIndex = 0; nIndex < SHEET_COUNT; nIndex++) {
            int nMaxsRow = getMaxRow(nIndex);
        }
        // 将第一行中元素的值设为对应的XML元素的名字
        // 如在Sheet0中，每行的第一列对应的XML元素名字为ID。
        DOMBuilder db = new DOMBuilder();
        Document doc = db.build(new FileInputStream(m_strConfigFileName));
        Element eleRoot = doc.getRootElement();
        Element ele = null;
        String strColName = "";

        for (int nIndex = 0; nIndex < SHEET_COUNT; nIndex++) {
            String sheetId = "Sheet" + String.valueOf(nIndex + 1);
            try {
                ele = eleRoot.getChild(sheetId);
                //获取每张sheet的列数
                int nMaxCol = getMaxCol(nIndex);
                String[] strArr = new String[nMaxCol];

                for (int nCol = 0; nCol < nMaxCol; nCol++) {
                    strColName = ele.getChildText("COL" + String.valueOf(nCol));
                    strArr[nCol] = strColName;
                }

                setPropArray(nIndex, PROP_COL_NAME, strArr);
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new Exception("找不到对应的配置信息，Sheet" +
                                    String.valueOf(nIndex + 1));
            }
        }
    }

    /**
     * 判断是否还有数据没有处理
     * 如果在存放保单信息的Sheet中，已经处理到了最大行，返回false；
     * 否则，返回true;
     * @return boolean
     */
    private boolean hasMoreData() {
        int nCurRow = getPropNumber(0, PROP_CUR_ROW);
        int nMaxRow = getPropNumber(0, PROP_MAX_ROW);

        if (nCurRow >= nMaxRow) {
            return false;
        } else {
            return true;
        }
    }

    private int getMaxRow(int nSheetIndex) throws Exception {
        String str = "";
        int nMaxRow = getPropNumber(nSheetIndex, PROP_MAX_ROW);

        if (nMaxRow == -1) {
            for (nMaxRow = 0; nMaxRow < m_book.getMaxRow(); nMaxRow++) {
                str = m_book.getText(nSheetIndex, nMaxRow, 0);
                if (str == null || str.trim().equals("")) {
                    break;
                }
            }
            setPropNumber(nSheetIndex, PROP_MAX_ROW, nMaxRow);
        }

        return nMaxRow;
    }

    private int getMaxCol(int nSheetIndex) throws Exception {
        String str = "";
        int nMaxCol = getPropNumber(nSheetIndex, PROP_MAX_COL);

        if (nMaxCol == -1) {
            for (nMaxCol = 0; nMaxCol < m_book.getMaxRow(); nMaxCol++) {
                str = m_book.getText(nSheetIndex, 0, nMaxCol);
                if (str == null || str.trim().equals("")) {
                    break;
                }
            }
            setPropNumber(nSheetIndex, PROP_MAX_COL, nMaxCol);
        }
        return nMaxCol;
    }

    private int getPropNumber(int nSheetIndex, String strPropName) {
        Hashtable hash = m_hashInfo[nSheetIndex];
        String str = (String) hash.get(strPropName);
        if (str != null && !str.equals("")) {
            return Integer.parseInt(str);
        } else {
            return -1;
        }
    }

    private void setPropNumber(int nSheetIndex, String strPropName, int nValue) {
        Hashtable hash = m_hashInfo[nSheetIndex];
        hash.put(strPropName, String.valueOf(nValue));
    }

    private String[] getPropArray(int nSheetIndex, String strPropName) {
        Hashtable hash = m_hashInfo[nSheetIndex];
        String[] strArr = (String[]) hash.get(strPropName);
        return strArr;
    }

    private void setPropArray(int nSheetIndex, String strPropName,String[] strArr) {
        Hashtable hash = m_hashInfo[nSheetIndex];
        hash.put(strPropName, strArr);
    }

    /**
     * 执行一次生成，将ROW_LIMIT行数的VTS数据生成一个Document
     * @param strFileName String
     * @throws Exception
     */
    private void genPart(String strFileName) throws Exception {
        Element root = new Element("DATASET");
        Document doc = new Document(root);

        Element ele = new Element("CASETABLE");
        root.addContent(ele);
        genXMLPart(ele, this.SHEET_CASEINFO);

        XMLOutputter xo = new XMLOutputter("  ", true, "GBK");
        xo.output(doc, new FileOutputStream(strFileName));
    }

    /**
     * 生成xml
     * @param eleParent Element
     * @param sheetNo int
     * @throws Exception
     */
    private void genXMLPart(Element eleParent, int sheetNo) throws Exception {
        int nRow = 0;
        int nCount = 0;

        //nCount控制xml中合同行递增
        //如果超出最小行限制并且合同ID已变 则跳出生成新文件
        if (++nCount > SIXmlParser.ROW_LIMIT)
            return;

        // 案件信息
        genPartSubTable(eleParent, this.SHEET_CASEINFO);
        setPropNumber(sheetNo, PROP_CUR_ROW, nRow);
    }

    /**
     * 处理子表
     * @param eleParent Element
     * @param nSheetIndex int
     * @throws Exception
     */
    private void genPartSubTable(Element eleParent, int nSheetIndex) throws
            Exception {
        int nCurRow = getPropNumber(nSheetIndex, PROP_CUR_ROW);
        int nMaxRow = getMaxRow(nSheetIndex);
        int nMaxCol = getMaxCol(nSheetIndex);

        String[] strColName = getPropArray(nSheetIndex, PROP_COL_NAME);
        mTitle = strColName;
        int nRow = 0;

        for (nRow = nCurRow; nRow < nMaxRow; nRow++) {
            Element eleRow = new Element("ROW");
            eleParent.addContent(eleRow);
            int nCol;
            for (nCol = 0; nCol < nMaxCol; nCol++) {
                if (strColName[nCol] == null) {
                    break;
                }
                Element ele = new Element(strColName[nCol]);
                ele.setText(m_book.getText(nSheetIndex, nRow, nCol).trim());
                eleRow.addContent(ele);
            }
//            mTitle = new String[nCol];
//            for(int i=0;i<nCol;i++){
//            	mTitle[i] = strColName[i];
//            }
        }
        setPropNumber(nSheetIndex, PROP_CUR_ROW, nRow);
    }

    public String[] getTitle(){
        return mTitle;
    }
}
