package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:承保暂交费功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class ClaimCalUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  private String backMsg = "";
  /** 数据操作字符串 */
  private String mOperate;




  public ClaimCalUI() {}

 public static void main(String[] args)
  {
       String transact = "QUERY";
  	LLClaimPolicySchema tLLClaimPolicySchema = new LLClaimPolicySchema();
  	//判断Radio被选中

	tLLClaimPolicySchema.setRgtNo("86110020040510000012");
	tLLClaimPolicySchema.setPolNo("86110020040210000166");
  	tLLClaimPolicySchema.setRiskCode("211801");
  	tLLClaimPolicySchema.setCaseNo("86110020040550000015");
  	tLLClaimPolicySchema.setInsuredNo("0000001235");
  	tLLClaimPolicySchema.setCValiDate("2004-09-06");
        tLLClaimPolicySchema.setGetDutyKind("200");
        tLLClaimPolicySchema.setCasePolType("0");
  	ClaimCalUI tClaimCalUI   = new ClaimCalUI();
  	// 准备传输数据 VData
   	VData tVData = new VData();
	//此处需要根据实际情况修改
   	tVData.addElement(tLLClaimPolicySchema);
   	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   	tClaimCalUI.submitData(tVData,transact);

   }




  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    ClaimCalBL tClaimCalBL = new ClaimCalBL();

    System.out.println("---UI BEGIN---");
    if (tClaimCalBL.submitData(cInputData,mOperate) == false)
    {
  		// @@错误处理
      this.mErrors.copyAllErrors(tClaimCalBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "CaseUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else{
        mResult = tClaimCalBL.getResult();
        backMsg = tClaimCalBL.getBackMsg();
    }
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public String getBackMsg(){
      return backMsg;
  }

}
