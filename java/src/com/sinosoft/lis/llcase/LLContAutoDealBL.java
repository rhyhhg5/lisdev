package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.llcase.*;

/**
 * <p>Title: 工单管理系统</p>
 * <p>Description: 工单录入BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 * @date 2005-01-17
 */

public class LLContAutoDealBL {
    
    public CErrors mErrors = new CErrors(); 	/** 错误处理类 */   
    private VData mInputData = new VData();		/** 输入数据的容器 */    
    private VData mResult = new VData();		/** 输出数据的容器 */
    private MMap mMMap = new MMap();

    /** 数据操作字符串 */
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    private String mCaseNo = "";
    private String mCurrentOperator = "";
    private String mHZBackMsg = "";
    private String tInsuredNo = "";
    
    private GlobalInput mGlobalInput = new GlobalInput();
    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema(); 
    private LLContDealSchema mLLContDealSchema = new LLContDealSchema();
    private LGWorkSchema mLGWorkSchema = new LGWorkSchema();
    private LPPolSet mLPPolSet=new LPPolSet();

    public LLContAutoDealBL() {
    }


    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 将传入的数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();        

        // 将外部传入的数据分解到本类的属性中，准备处理
        if (!getInputData()) {
            return false;
        }
        
        if (!checkData()) {
            return false;
        }

        // 根据业务逻辑对数据进行处理
        if (!dealData()) {
            return false;
        }
        
        prepareOutputData();        
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
        	CError.buildErr(this, "数据保存失败");
            return false;
          }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData() {
    	System.out.println("LLContAutoDealBL============Begin===========");
    	 mLJAGetSchema = (LJAGetSchema) mInputData.getObjectByObjectName("LJAGetSchema", 0);
         mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
         mLLCaseSchema = (LLCaseSchema) mInputData.getObjectByObjectName("LLCaseSchema", 0);
        
         if (mLJAGetSchema.getOtherNo() == null) {
        	 mHZBackMsg += "<br>合同终止传入理赔号为空!";                
             return false;
         }
         mCurrentOperator = mGlobalInput.Operator;
         mCaseNo = mLJAGetSchema.getOtherNo();
         System.out.println("LLContAutoDealBL============End===========");
        return true;
    }

    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData() {
    	LJAGetClaimDB mLJAGetClaimDB = new LJAGetClaimDB();
    	LJAGetClaimSet mLJAGetClaimSet = new LJAGetClaimSet();
    	mLJAGetClaimDB.setOtherNo(mCaseNo);
    	mLJAGetClaimDB.setOtherNoType("5");
    	mLJAGetClaimDB.setEnterAccDate("");
    	mLJAGetClaimSet = mLJAGetClaimDB.query();
    	if(mLJAGetClaimSet.size()<=0 )
    	{
    		 mHZBackMsg += "<br>案件"+mCaseNo+"还未通知给付不能进行合同终止";    	
             return false;
    	}
    	return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData() {
    	
    	//处理需要自动终止的保单
    	if(!queryAutoHZ()){    	    		
    		return false;
    	}  
    	    	
    	return true;
    }
    /**
     * 处理需要自动终止的保单
     * */
    private boolean queryAutoHZ()
    {    	
    	String tDetSql = "";
    	String uPolNo="";
    	String uRealPay="";
    	
        //福泽一生重疾反冲
        String tFZSQL = "select ElementValue from LLElementDetail where caseno='" + mCaseNo + "' "
		+" and getdutycode='321201'"
		+" and elementcode='SeriousDisease'";
        
        ExeSQL tFZExeSQL = new ExeSQL();
        String tFZ = tFZExeSQL.getOneValue(tFZSQL); 
        
        //福泽180天之内
        String tFZAccRDSQL = "select 1 from LLElementDetail where caseno='" + mCaseNo + "' "
		+" and getdutycode='321201'"
		+" and elementcode='RgtDays' and bigint(ElementValue)<=180";
        
        ExeSQL tFZAccRDExeSQL = new ExeSQL();
        String tFZAccRD = tFZAccRDExeSQL.getOneValue(tFZAccRDSQL); 
        
        //福泽重疾险种
        String tFZRiskCodeSQL = "select 1 from llclaimdetail where caseno='" + mCaseNo + "' and riskcode = '231201' and getdutycode='321201'";
        
        ExeSQL tFZRiskCodeExeSQL = new ExeSQL();
        String tFZRiskCode = tFZRiskCodeExeSQL.getOneValue(tFZRiskCodeSQL); 
    //# 1500 康乐人生险种
        ExeSQL tRiskCodeSQL =new ExeSQL();
        String tCode ="select 1 from llclaimdetail where caseno='"+mCaseNo+"' and riskcode in ('231701','333801')";
        String tRiskCode = tRiskCodeSQL.getOneValue(tCode);
        //# 健康金福
        ExeSQL tJKRiskCodeSQL =new ExeSQL();
        String tJKRiskCodeExeSQL = "select 1 from llclaimdetail where caseno='" + mCaseNo + "' and riskcode = '123701'";
        String tJKRiskCode = tJKRiskCodeSQL.getOneValue(tJKRiskCodeExeSQL);         
        
    	if(tFZ.equals("1") && tFZAccRD.equals("1") && tFZRiskCode.equals("1")){
    		tDetSql =" select b.grpcontno,b.contno,b.riskcode,b.polno,a.customerno,b.getdutycode,"
    			+" (select sum(realpay) from llclaimdetail where polno=b.polno),a.handler"
    			+" from llcase a,llclaimdetail b "
    			+" where 1=1 and a.caseno=b.caseno and a.rgtstate='11'"
    			+" and exists (select 1 from lmdutygetclm m,ldcode1 n where n.codetype='AutoHZ' "
    			+" and m.AfterGet='003' and m.getdutycode=n.code and n.code=b.getdutycode)"  
    			+" and a.caseno='"+ mCaseNo +"'"
    			+" and exists (select 1 from lcpol where polno=b.polno and conttype='1' and appflag='1' and stateflag='1')"
    			+" group by b.grpcontno,b.contno,b.riskcode,b.polno,a.customerno,b.getdutycode,a.handler";
    	
    	}
    	// # 1500
    	else if("1".equals(tRiskCode)){
    		
            //康乐人生重大疾病且属于特殊疾病
            String tKLSpecialDisease="select 1 from ldcode1 where codetype ='SpecialDisease' " +
            		// othersign:险种号（RiskCode）
    				"and  othersign='231701' and code in " 
    				+"(select DiseaseCode from LLCaseCure where caseno='"+mCaseNo+"') ";
            
            ExeSQL tKLDiseaseExeSQL = new ExeSQL();
            String tKLDisease = tKLDiseaseExeSQL.getOneValue(tKLSpecialDisease);
           
            //康乐人生重大疾病给付责任
            String tKLSpecialDiseaseCode="select 1 from llclaimdetail where riskcode='231701' and getdutycode='350201' and caseno='"+mCaseNo+"'";
            
            ExeSQL tKLSQL= new ExeSQL();
            String tKLDiseaseCode=tKLSQL.getOneValue(tKLSpecialDiseaseCode);
         
            //康乐人生身故保险
            String tKLRiskCodeSQL = "select 1 from llclaimdetail where caseno='" + mCaseNo + "' and riskcode = '231701' and getdutycode='350203'";
            
            ExeSQL tKLRiskCodeExeSQL = new ExeSQL();
            String tKLRiskCode = tKLRiskCodeExeSQL.getOneValue(tKLRiskCodeSQL); 
    		
            //康乐人生特定疾病医疗的polno
            String tKLPolNoSQL ="select distinct polno from llclaimdetail where caseno='"+ mCaseNo +"' and getdutycode='350202' and riskcode='231701'";
            ExeSQL tKLPolNo =new ExeSQL();
            uPolNo = tKLPolNo.getOneValue(tKLPolNoSQL);
            
            //康乐人生特定疾病医疗保险
            String tKLDutCode ="select count(1) from llclaimdetail where caseno='"+ mCaseNo +"' and getdutycode ='350202' and "
            		+ "((select coalesce(sum(realpay),0) from llclaimdetail a,llcase b where "
                + "a.caseno=b.caseno and a.polno='"+ uPolNo +"' and a.getdutycode='350202'"
                + "and a.getdutykind='405' and b.rgtstate in ('09','11','12')) >=(select double(actuget)*0.3 from lcget where "
                + "polno ='"+uPolNo+"' and getdutycode='350201'))"; 
            
            ExeSQL tKLDutyCodeSQL = new ExeSQL();
            String tKLCode = tKLDutyCodeSQL.getOneValue(tKLDutCode);
            
            //附加险康乐人生
            String tFKLGetDutyCode ="select 1 from llclaimdetail where caseno='"+ mCaseNo +"' and riskcode ='333801' and getdutycode='351201'";
            
            ExeSQL tFKLGetDutyCodeSQL = new ExeSQL();
            String tFKLGetDuty = tFKLGetDutyCodeSQL.getOneValue(tFKLGetDutyCode);
            
            //重大疾病且不属于特定疾病
            if(!tKLDisease.equals("1") &&tKLDiseaseCode.equals("1") ){
            	tDetSql =" select b.grpcontno,b.contno,b.riskcode,b.polno,a.customerno,b.getdutycode,"
            			+" (select sum(realpay) from llclaimdetail where polno=b.polno),a.handler"
            			+" from llcase a,llclaimdetail b "
            			+" where 1=1 and a.caseno=b.caseno and a.rgtstate='11'"
            			+" and exists (select 1 from lmdutygetclm m,ldcode1 n where n.codetype='AutoHZ' "
            			+" and m.getdutycode=n.code and n.code=b.getdutycode)"  
            			+" and a.caseno='"+ mCaseNo +"'"
            			+" and exists (select 1 from lcpol where polno=b.polno and conttype='1' and appflag='1' and stateflag='1')"
            			+" group by b.grpcontno,b.contno,b.riskcode,b.polno,a.customerno,b.getdutycode,a.handler"
            			+" having (select sum(realpay) from llclaimdetail m , llcase n where m.caseno = n.caseno and n.rgtstate in ('11','12') and m.polno=b.polno and m.getdutycode = b.getdutycode) >0 ";
            }
            //身故
            if(tKLRiskCode.equals("1")){
            	tDetSql =" select b.grpcontno,b.contno,b.riskcode,b.polno,a.customerno,b.getdutycode,"
            			+" (select sum(realpay) from llclaimdetail where polno=b.polno),a.handler"
            			+" from llcase a,llclaimdetail b "
            			+" where 1=1 and a.caseno=b.caseno and a.rgtstate='11'"
            			+" and exists (select 1 from lmdutygetclm m,ldcode1 n where n.codetype='AutoHZ' "
            			+" and m.AfterGet='003' and m.getdutycode=n.code and n.code=b.getdutycode)"  
            			+" and a.caseno='"+ mCaseNo +"'"
            			+" and exists (select 1 from lcpol where polno=b.polno and conttype='1' and appflag='1' and stateflag='1')"
            			+" group by b.grpcontno,b.contno,b.riskcode,b.polno,a.customerno,b.getdutycode,a.handler"
            			+" having (select sum(realpay) from llclaimdetail m , llcase n where m.caseno = n.caseno and n.rgtstate in ('11','12') and m.polno=b.polno and m.getdutycode = b.getdutycode) >0 ";
            }
            //特定疾病已超约定保额
            if(tKLCode.equals("1")){
            	tDetSql =" select b.grpcontno,b.contno,b.riskcode,b.polno,a.customerno,b.getdutycode,"
            			+" (select sum(realpay) from llclaimdetail where polno=b.polno),a.handler"
            			+" from llcase a,llclaimdetail b "
            			+" where 1=1 and a.caseno=b.caseno and a.rgtstate='11'"
            			+" and exists (select 1 from lmdutygetclm m,ldcode1 n where n.codetype='AutoHZ' "
            			+" and m.getdutycode=n.code and n.code=b.getdutycode)"  
            			+" and a.caseno='"+ mCaseNo +"'"
            			+" and exists (select 1 from lcpol where polno=b.polno and conttype='1' and appflag='1' and stateflag='1')"
            			+" group by b.grpcontno,b.contno,b.riskcode,b.polno,a.customerno,b.getdutycode,a.handler"
            			+" having (select sum(realpay) from llclaimdetail m , llcase n where m.caseno = n.caseno and n.rgtstate in ('11','12') and m.polno=b.polno and m.getdutycode = b.getdutycode) >0 ";
            }
            //附加康乐人生个人护理保险
            if(tFKLGetDuty.equals("1")){
            	tDetSql =" select b.grpcontno,b.contno,b.riskcode,b.polno,a.customerno,b.getdutycode,"
            			+" (select sum(realpay) from llclaimdetail where polno=b.polno),a.handler"
            			+" from llcase a,llclaimdetail b "
            			+" where 1=1 and a.caseno=b.caseno and a.rgtstate='11'"
            			+" and exists (select 1 from lmdutygetclm m,ldcode1 n where n.codetype='AutoHZ' "
            			+" and m.AfterGet='003' and m.getdutycode=n.code and n.code=b.getdutycode)"  
            			+" and a.caseno='"+ mCaseNo +"'"
            			+" and exists (select 1 from lcpol where polno=b.polno and conttype='1' and appflag='1' and stateflag='1')"
            			+" group by b.grpcontno,b.contno,b.riskcode,b.polno,a.customerno,b.getdutycode,a.handler"
            			+" having (select sum(realpay) from llclaimdetail m , llcase n where m.caseno = n.caseno and n.rgtstate in ('11','12') and m.polno=b.polno and m.getdutycode = b.getdutycode) >0 ";
            }
    	}else if("1".equals(tJKRiskCode)){
    		// 健康金福
    		//恶性肿瘤确诊日期
    		ExeSQL tSQL =new ExeSQL();

    		String DSdate = "select min(DiagnoseDate) from LLCaseCure where caseno = '"+mCaseNo+"' and DiseaseCode in(select code from ldcode where codetype='llserialsdiease') and SeriousFlag = '1' with ur";
    		String tDSdate = tSQL.getOneValue(DSdate);
    		//保单失效效期
    		String tJFPolNoSQL ="select distinct polno from llclaimdetail where caseno='"+ mCaseNo +"' and riskcode='123701'";
            
            uPolNo = tSQL.getOneValue(tJFPolNoSQL);
            

    		String EndDate = "select enddate from lcpol where polno='"+uPolNo+"' with ur";
    		String tEndDate = tSQL.getOneValue(EndDate);
    		
    		//住院天数
    		//String RealHospdateSQL = "select sum(realhospdate) from llfeemain where caseno='"+mCaseNo+"' and feetype='2' with ur";
    		String HospStartdateSQL = "select min(hospStartdate) from llfeemain where caseno='"+mCaseNo+"' and feetype='2' with ur";
    		String HospenddateSQL = "select max(hospenddate) from llfeemain where caseno='"+mCaseNo+"' and feetype='2' with ur";
    		//String RealHospdate = tSQL.getOneValue(RealHospdateSQL);
    		String HospStartdate = tSQL.getOneValue(HospStartdateSQL);
    		String Hospenddate= tSQL.getOneValue(HospenddateSQL);
    		if(tDSdate!=null&&tDSdate!=""&&HospStartdate!=null&&HospStartdate!=""&&Hospenddate!=null&&Hospenddate!=""){
    			int tPolday = PubFun.calInterval2(tDSdate, tEndDate, "D"); //确诊日期距保单失效效日期小于180天
    			int checkdate = PubFun.calInterval2(Hospenddate, tEndDate, "D"); //出院日期晚于保单失效日期的，合同自动终止
//    			int checkdate2 = PubFun.calInterval2(HospStartdate, tEndDate, "D");;//入院日期距保单失效效日期小于180天
	    		if(tPolday>=0&&tPolday<180&&checkdate<0){
	    			tDetSql =" select b.grpcontno,b.contno,b.riskcode,b.polno,a.customerno,b.getdutycode,"
	            			+" (select sum(realpay) from llclaimdetail where polno=b.polno),a.handler"
	            			+" from llcase a,llclaimdetail b "
	            			+" where 1=1 and a.caseno=b.caseno and a.rgtstate='11'"
	            			+" and exists (select 1 from lmdutygetclm m,ldcode1 n where n.codetype='AutoHZ' "
	            			+" and m.getdutycode=n.code and n.code=b.getdutycode)"  
	            			+" and a.caseno='"+ mCaseNo +"'"
	            			+" and exists (select 1 from lcpol where polno=b.polno and conttype='1' and appflag='1' )"
	            			+" group by b.grpcontno,b.contno,b.riskcode,b.polno,a.customerno,b.getdutycode,a.handler"
	            			+" having (select sum(realpay) from llclaimdetail m , llcase n where m.caseno = n.caseno and n.rgtstate in ('11','12') and m.polno=b.polno and m.getdutycode = b.getdutycode) >0 ";
	    		}
    		}
    		
    	// 健康金福end
    	}else{
       tDetSql =" select b.grpcontno,b.contno,b.riskcode,b.polno,a.customerno,b.getdutycode,"
			+" (select sum(realpay) from llclaimdetail where polno=b.polno),a.handler"
			+" from llcase a,llclaimdetail b "
			+" where 1=1 and a.caseno=b.caseno and a.rgtstate='11'"
			+" and exists (select 1 from lmdutygetclm m,ldcode1 n where n.codetype='AutoHZ' "
			+" and m.AfterGet='003' and m.getdutycode=n.code and n.code=b.getdutycode)"  
			+" and a.caseno='"+ mCaseNo +"'"
			+" and exists (select 1 from lcpol where polno=b.polno and conttype='1' and appflag='1' and stateflag='1')"
			// 目前不存在一个在B表，关联险种在C表的情况
//			+" union select 1 from lbpol where polno=b.polno and conttype='1' and appflag='1' and stateflag='1')"
			+" group by b.grpcontno,b.contno,b.riskcode,b.polno,a.customerno,b.getdutycode,a.handler"
			+" having (select sum(realpay) from llclaimdetail m , llcase n where m.caseno = n.caseno and n.rgtstate in ('11','12') and m.polno=b.polno and m.getdutycode = b.getdutycode) >0 "
			+" union "
			+" select b.grpcontno,b.contno,b.riskcode,b.polno,a.customerno,b.getdutycode, "
			+" (select sum(realpay) from llclaimdetail where polno=b.polno),a.handler "
			+" from llcase a,llclaimdetail b "
			+" where 1=1 "
			+" and a.caseno=b.caseno and a.rgtstate='11' "
			+" and exists (select 1 from ldcode1 n where n.codetype='AutoHZHM' and n.code=b.getdutycode) "
			+" and a.caseno='"+ mCaseNo +"' "
			+" and exists (select 1 from lcpol where polno=b.polno and conttype='1' and appflag='1' and stateflag='1') "
			+" group by b.grpcontno,b.contno,b.riskcode,b.polno,a.customerno,b.getdutycode,a.handler ";
    	}
    	System.out.println("tContSql==="+tDetSql);
    	ExeSQL tExeSQL = new ExeSQL();
    	SSRS tSSRS = tExeSQL.execSQL(tDetSql);  
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			System.out.println("案件" + mCaseNo + "没有合同自动终止的保单!");
			mHZBackMsg = "";
			return true;
		}
    		mCurrentOperator = tSSRS.GetText(1, 8);
			tInsuredNo = tSSRS.GetText(1, 5) ;
    	String tempContSql ="";
    		for ( int i = 1 ; i <= tSSRS.getMaxRow() ; i ++)
    		{    			
    			tempContSql += "contno = '" + tSSRS.GetText(i, 2) + "' or ";
    		}
    		tempContSql = "(" + tempContSql.substring(0, tempContSql.lastIndexOf("or")) + ") ";
    		String tContSql ="select distinct grpcontno,contno,appntno from lccont where conttype='1' and appflag='1' and stateflag='1' and "+ tempContSql ;    		
    		SSRS tContSSRS = tExeSQL.execSQL(tContSql); 
    		if(tContSSRS.getMaxRow()<=0) {
    			mHZBackMsg += "<br>保单为非有效状态不能合同自动终止!";
    			return false;
    		}
    		for(int j = 1 ; j <= tContSSRS.getMaxRow() ; j++)
    		{
    			 mLPPolSet=new LPPolSet();
    			String tContNo = tContSSRS.GetText(j, 2);
    			
    			//1、申请合同处理 保单层
    	    	LLContDealBL mLLContDealBL = new LLContDealBL();  
    	    	mLGWorkSchema.setWorkNo(null); 
    	    	mLGWorkSchema.setCustomerNo(tContSSRS.GetText(j, 3));
    	    	mLGWorkSchema.setStatusNo("3");
    	    	mLGWorkSchema.setTypeNo("03");
    	    	mLGWorkSchema.setApplyTypeNo("3");
    	    	mLGWorkSchema.setRemark("理赔合同处理");
    	    	
    	    	mLLCaseSchema.setGrpNo(tContNo);
    	    	mGlobalInput.Operator = mCurrentOperator;
    	    	VData aVData = new VData();
    	    	aVData.add(mLGWorkSchema);
    	    	aVData.add(mLLCaseSchema);
    	    	aVData.add(mGlobalInput);
    	    	if(!mLLContDealBL.submitData(aVData, "")){
    	    		mHZBackMsg += "<br>保单"+tContNo+"合同终止申请失败";
    	    		System.out.println(mHZBackMsg);
    	    		return false;
    	    	}else{    		    		
    	    		mLGWorkSchema.setSchema((LGWorkSchema) mLLContDealBL.getResult().getObjectByObjectName("LGWorkSchema", 0));
    	    		TransferData mTransferData = new TransferData();
    	    		mTransferData = (TransferData) mLLContDealBL.getResult().getObjectByObjectName("TransferData", 0);
    	    		if (mTransferData != null) {
    	    			  String tIndiInfo = (String)mTransferData.getValueByName("IndiInfo");
    	    			  mHZBackMsg += "<br>合同处理终止时,"+tIndiInfo;
    	    			  System.out.println(mHZBackMsg);    			      			 
    	    		}
    	    	}
    	    	
    	    	//2、默认添加合同处理项目 HZ-合同终止 保单层
    	    	PEdorAppItemUI tPEdorAppItemUI   = new PEdorAppItemUI();
    	    	LDCodeDB tLDCodeDB = new LDCodeDB();
    	        tLDCodeDB.setCodeType("llcontdeal");
    	        tLDCodeDB.setCode("user");
    	        if (tLDCodeDB.getInfo()) {
    	        	mGlobalInput.Operator = tLDCodeDB.getCodeName();
    	        }else{
    	        	mHZBackMsg += "<br>未配置合同终止理赔操作人!";
    	     		return false;
    	        }
    	        LPEdorItemSet mLPEdorItemSet = new LPEdorItemSet();
    	        LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();
    	        mLPEdorItemSchema.setEdorAcceptNo(mLGWorkSchema.getAcceptNo());
    	        mLPEdorItemSchema.setGrpContNo(tContSSRS.GetText(j, 1)); 
    	        mLPEdorItemSchema.setDisplayType("1");      
    	        mLPEdorItemSchema.setEdorType("CT");	//默认合同终止 
    	        mLPEdorItemSchema.setContNo(tContNo); 
    	        mLPEdorItemSchema.setInsuredNo("000000");
    	        mLPEdorItemSchema.setPolNo("000000");
    	        mLPEdorItemSchema.setEdorValiDate(mCurrentDate);
    	        mLPEdorItemSchema.setEdorAppDate(mCurrentDate);
    	        mLPEdorItemSet.add(mLPEdorItemSchema); 
    	        
    	        mLLContDealSchema.setEdorNo(mLGWorkSchema.getAcceptNo());
    	        mLLContDealSchema.setEdorType("HZ");
    	        
    	        VData bVData = new VData();        
    	        bVData.add(mLPEdorItemSet);
    	        bVData.add(mLLContDealSchema);        
    	        bVData.add(mGlobalInput);
    	    	if (!tPEdorAppItemUI.submitData(bVData,"INSERT||EDORITEM"))
    	    	{
    	    		mHZBackMsg += "<br>保单"+tContNo+"合同终止项目添加失败："+tPEdorAppItemUI.mErrors.getFirstError();
    	    		return false;
    	    	}else{
    	    		mLPEdorItemSet = (LPEdorItemSet)tPEdorAppItemUI.getResult().getObjectByObjectName("LPEdorItemSet", 0);
    	    	}
    	    	 
    	    	String tempPolSql ="";     
    	    	
        		for(int i = 1; i <= tSSRS.getMaxRow() ; i++)
        		{
        			tempPolSql += "riskcode in (select code1 from ldcode1 where codetype='AutoHZ' and code='" + tSSRS.GetText(i, 6) + "') or ";
        		}	
        		tempPolSql = "(" + tempPolSql.substring(0, tempPolSql.lastIndexOf("or")) + ") ";
        		
        			String tPolSql = "select distinct polno from lcpol where 1=1 and appflag='1' and stateflag='1' and insuredno='"
					+ tInsuredNo
					+ "' and contno='"
					+ tContNo
					+ "' and "
					+ tempPolSql;
        		
        		System.out.println("tPolSql=="+tPolSql);
        		SSRS tPolSSRS = tExeSQL.execSQL(tPolSql); 
        		if(tPolSSRS.getMaxRow()<=0)
        		{
        			mHZBackMsg += "<br>保单险种为非有效状态不能合同自动终止!";
        			return false;
        		}     		
        		for (int k =1; k <= tPolSSRS.getMaxRow() ; k++)
        		{
        			String tPolNo = tPolSSRS.GetText(k, 1) ;
        			
        			//3、默认录入合同处理项目明细 HZ-合同终止  险种层级        	    	
        	    	LPPolSchema mLPPolSchema=new LPPolSchema();
        	        mLPPolSchema.setEdorNo(mLGWorkSchema.getAcceptNo());
        	        mLPPolSchema.setEdorType("CT");
        	        mLPPolSchema.setContNo(tContNo);
        	        mLPPolSchema.setInsuredNo(tInsuredNo);
        	        mLPPolSchema.setPolNo(tPolNo);
        	        System.out.println("++++++++++++++++"+tPolNo);
        	        mLPPolSet.add(mLPPolSchema);
        		}       
    	        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
    	    	tLPEdorItemSchema.setEdorAcceptNo(mLGWorkSchema.getAcceptNo());
    	    	tLPEdorItemSchema.setEdorNo(mLGWorkSchema.getAcceptNo());
    	    	tLPEdorItemSchema.setContNo(tContNo);
    	    	tLPEdorItemSchema.setEdorType("CT");
    	    	tLPEdorItemSchema.setInsuredNo("000000");
    	    	tLPEdorItemSchema.setPolNo("000000");
    	    	//tLPEdorItemSchema.setGetMoney(getMoney);
    	    	tLPEdorItemSchema.setReasonCode("076");
    	    	
    	    	// #3002 
    	    	String lCSQL ="select l.bankcode,l.bankaccno,l.accname from lccont l where contno = '"+tLPEdorItemSchema.getContNo()+"' ";
    	    	ExeSQL lESQL = new ExeSQL();
    	    	SSRS  lCSSRS =lESQL.execSQL(lCSQL);
    	    	LPContSchema uLPContSchema =new LPContSchema();
    	    	if(lCSSRS.getMaxRow()>0){
    	    			uLPContSchema.setBankCode(lCSSRS.GetText(1, 1));
    	    			uLPContSchema.setBankAccNo(lCSSRS.GetText(1, 2));
    	    			uLPContSchema.setAccName(lCSSRS.GetText(1, 3));

    	    	}
    	    	
    	    	VData cVData = new VData();  
    	    	cVData.add(mGlobalInput);
    	    	cVData.add(tLPEdorItemSchema);
    	    	cVData.add(uLPContSchema);
    	    	cVData.add(mLPPolSet);
    	    	
    	    	PEdorCTDetailUI tPEdorCTDetailUI = new PEdorCTDetailUI();
    	    	if (!tPEdorCTDetailUI.submitData(cVData, ""))
    	    	{
    	    		mHZBackMsg += "<br>保单"+tContNo+"合同终止明细信息保存失败："+tPEdorCTDetailUI.mErrors.getFirstError();
    	    		return false;
    	    	}        		
        		
        		//4、合同处理理算 		    
            	PEdorAppConfirmUI tPEdorAppConfirmUI = new PEdorAppConfirmUI(mGlobalInput, mLGWorkSchema.getAcceptNo());
        	    if (!tPEdorAppConfirmUI.submitData())
        	    {
        	    	mHZBackMsg += "<br>保单"+tContNo+"合同终止理算失败！原因是：" + tPEdorAppConfirmUI.getError();
            		return false;
        	    }   
        	    if(!updateMoney(tContNo))
        	    {
        	    	mHZBackMsg += "<br>保单"+tContNo+"合同终止理算失败";
            		return false;
        	    }
        	    //生成打印数据
    	    	VData dVData = new VData();
    	    	dVData.add(mGlobalInput);
    			LLContDealCalPrintBL tLLContDealCalPrintBL = new LLContDealCalPrintBL(mLGWorkSchema.getAcceptNo());
    			if (!tLLContDealCalPrintBL.submitData(dVData, ""))
    			{
    				mHZBackMsg += "<br>保全理算成功，但生成批单失败！";
    				return false;
    			}
        	    //5、合同处理确认
        	    EdorItemSpecialData tSpecialData = new EdorItemSpecialData(mLGWorkSchema.getAcceptNo(), "YE");
        		tSpecialData.add("CustomerNo", tInsuredNo);
        		tSpecialData.add("AccType", "1");
        		tSpecialData.add("OtherType", "10");
        		tSpecialData.add("OtherNo", mLGWorkSchema.getAcceptNo());
        		tSpecialData.add("DestSource", "11");
        		tSpecialData.add("ContType", "1");
        		tSpecialData.add("Fmtransact2", "NOTUSEACC");
        		
        	    LCAppAccTraceSchema tLCAppAccTraceSchema=new LCAppAccTraceSchema();
        		tLCAppAccTraceSchema.setCustomerNo(tInsuredNo);
        		tLCAppAccTraceSchema.setAccType("1");
        		tLCAppAccTraceSchema.setOtherType("10");//个人批改号
        		tLCAppAccTraceSchema.setOtherNo(mLGWorkSchema.getAcceptNo());
        		tLCAppAccTraceSchema.setDestSource("11");
        		tLCAppAccTraceSchema.setOperator(mGlobalInput.Operator);
        		
        		LJAGetDB tLJAGetDB = new LJAGetDB();
        		tLJAGetDB.setOtherNo(mCaseNo);
        		tLJAGetDB.setOtherNoType("5");
        		LJAGetSet tLJAGetSet = tLJAGetDB.query();
        		LJAGetSchema tLJAGetSchema = tLJAGetSet.get(1);
        		
        	    VData eVData = new VData();
        	    eVData.add(tLCAppAccTraceSchema);
        	    eVData.add(tSpecialData);
        	    eVData.add(mGlobalInput);
        	 	PEdorAppAccConfirmBL tPEdorAppAccConfirmBL = new PEdorAppAccConfirmBL();
        	 	if (!tPEdorAppAccConfirmBL.submitData(eVData, "INSERT||Param"))
        	 	{	 		
        	 		mHZBackMsg += "<br>保单"+tContNo+"合同自动终止时处理帐户余额失败！" ;
            		return false;
        	 	}
        	  	else{    	  		 
        	     BqConfirmUI tBqConfirmUI = new BqConfirmUI(mGlobalInput, mLGWorkSchema.getAcceptNo(), BQ.CONTTYPE_P,"");
        	     if (!tBqConfirmUI.submitData())
        	     {
        	    	 mHZBackMsg += "<br>保单"+tContNo+"合同终止失败："+tBqConfirmUI.getError();	 	    	 				
        	 	     return false;
        	     }
        	     else
        	     {		        	        	    	  	   	   
        	     	System.out.println("交退费通知书" + mLGWorkSchema.getAcceptNo());
        	 		TransferData tTransferData = new TransferData();
        	 		tTransferData.setNameAndValue("payMode", tLJAGetSchema.getPayMode());
         			tTransferData.setNameAndValue("endDate", "");
         	        tTransferData.setNameAndValue("payDate", mCurrentDate);
         	        tTransferData.setNameAndValue("bank", tLJAGetSchema.getBankCode());
         	        tTransferData.setNameAndValue("bankAccno",tLJAGetSchema.getBankAccNo());
         	        tTransferData.setNameAndValue("accName", tLJAGetSchema.getAccName());
         	        tTransferData.setNameAndValue("chkYesNo", "no");
        	   		
        	   		//生成交退费通知书
        	 		FeeNoticeVtsUI tFeeNoticeVtsUI = new FeeNoticeVtsUI(mLGWorkSchema.getAcceptNo());
        	 		if (!tFeeNoticeVtsUI.submitData(tTransferData))
        	 		{
        	 			mHZBackMsg = "<br>保单"+tContNo+"生成批单失败！原因是：" + tFeeNoticeVtsUI.getError();
        	 			return false;
        	 		} 		
        	    
        	 		VData fVData = new VData();
        	 		fVData.add(mGlobalInput);
        	 		fVData.add(tTransferData);
        	 		SetPayInfo spi = new SetPayInfo(mLGWorkSchema.getAcceptNo());
        	 		if (!spi.submitDate(fVData, "0"))
        	 		{
        	 			System.out.println("设置收退费方式失败！");	 			
        	 			mHZBackMsg = "<br>保单"+tContNo+"设置收退费方式失败！原因是：" + spi.mErrors.getFirstError();        	 		
        	 			return false;
        	 		}
        	 		
        	 		LLCaseDB tLLCaseDB = new LLCaseDB();
        	    	LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        	    	tLLCaseDB.setCaseNo(mCaseNo);
					if (tLLCaseDB.getInfo()) {
						tLLCaseSchema = tLLCaseDB.getSchema();
						tLLCaseSchema.setContDealFlag("2");
						mMMap.put(tLLCaseSchema, "DELETE&INSERT");
					}
        	    	
        	    	LLContDealDB tLLContDealDB = new LLContDealDB();
        	    	tLLContDealDB.setEdorNo(mLLContDealSchema.getEdorNo());
        	    	if (tLLContDealDB.getInfo()) {
        	    		LLContDealSchema tLLContDealSchema = tLLContDealDB.getSchema();
        	    		tLLContDealSchema.setRemark("合同自动终止");
        	    		mMMap.put(tLLContDealSchema, "UPDATE");
        	    	}
        	    }
        	  		String message = tBqConfirmUI.getMessage();
        	 	if ((message != null) && (!message.equals("")))
        	 	{
        	 		mHZBackMsg += tBqConfirmUI.getMessage();
        	 	}
        	 	 mHZBackMsg = PubFun.changForHTML(mHZBackMsg);        	 	
        		
        	     System.out.println(mHZBackMsg);
        	     String failType = tBqConfirmUI.getFailType();  //是否审批通过
        	     String autoUWFail = tBqConfirmUI.autoUWFail();  //是否自核通过
        	     autoUWFail = autoUWFail == null ? "" : autoUWFail;
        	     System.out.println("failType:" + failType);
        	     System.out.println("autoUWFail:" + autoUWFail);
        	   }	
        	 	mHZBackMsg += "<br>保单"+tContNo+"合同自动终止成功!";
    		}
    	return true;
    }
    private boolean updateMoney(String tContNo){
    	    	
    	for(int i =1 ;i<= mLPPolSet.size() ;i++)
  	    {
  	    	String ljsSql ="update ljsgetendorse set getmoney =0 "
  	    			+" where EndorsementNo='"+mLGWorkSchema.getAcceptNo()+"' and polno='"+mLPPolSet.get(i).getPolNo()+"'"; 
  	    	mMMap.put(ljsSql, "UPDATE");
  	    }   
    	String ljsgSql ="update ljsget set sumgetmoney =0 "
			+" where otherno = '"+mLGWorkSchema.getAcceptNo()+"' ";
	    String lpeitemSql ="update lpedoritem set getmoney =0 "
			+" where EdorAcceptNo='"+mLGWorkSchema.getAcceptNo()+"' and contno='"+tContNo+"'";
	    String lpemainSql ="update LPEdorMain set getmoney =0 "
			+" where EdorAcceptNo='"+mLGWorkSchema.getAcceptNo()+"' and contno='"+tContNo+"'";
	    String lpeappSql ="update LPEdorApp set getmoney =0 "
			+" where EdorAcceptNo='"+mLGWorkSchema.getAcceptNo()+"' ";
	    mMMap.put(ljsgSql, "UPDATE");
	    mMMap.put(lpeitemSql, "UPDATE");
	    mMMap.put(lpemainSql, "UPDATE");
	    mMMap.put(lpeappSql, "UPDATE");
	    mInputData.clear();
	    mInputData.add(mMMap);
	    PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
        	CError.buildErr(this, "数据保存失败");
            return false;
          }
    	return true;
    }
    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData() {
        mInputData.clear();
        mInputData.add(mMMap);
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult() {
        return mResult;
    }
    public String getBackMsg() {
    	return mHZBackMsg;
    }
    public static void main(String arg[]) {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "cm1101";

        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        LJAGetSchema tLJAGetSchema = new LJAGetSchema();
        tLLCaseSchema.setCaseNo("C1100180416000006");
        tLLCaseSchema.setAccModifyReason("");
        tLJAGetSchema.setOtherNo("C1100180416000006");
        tLJAGetSchema.setOtherNoType("5");        

        VData tVData = new VData();
        tVData.add(tLJAGetSchema);
        tVData.add(tLLCaseSchema);
        tVData.add(tGI);

        LLContAutoDealBL tLLContAutoDealBL = new LLContAutoDealBL();
        if(!tLLContAutoDealBL.submitData(tVData, ""))
        System.out.println(tLLContAutoDealBL.getBackMsg());
//        LGWorkSchema mLGWorkSchema = new LGWorkSchema();
//        mLGWorkSchema.setAcceptNo("20110705000030");
//        BqConfirmUI tBqConfirmUI = new BqConfirmUI(tGI, mLGWorkSchema.getAcceptNo(), BQ.CONTTYPE_P,"");
//	     if (!tBqConfirmUI.submitData())
//	     {	    	 	 	    	 			
//	 	     System.out.println(tBqConfirmUI.getError()) ;
//	     }
        
//        String tDetSql =" select b.grpcontno,b.contno,b.riskcode,b.polno,a.customerno,b.getdutycode,"
//			+" (select sum(realpay) from llclaimdetail where polno=b.polno) summoney"
//			+" from llcase a,llclaimdetail b "
//			+" where 1=1 and a.caseno=b.caseno and a.rgtstate='11'"
//			+" and exists (select 1 from lmdutygetclm m,ldcode1 n where n.codetype='AutoHZ' "
//			+" and m.AfterGet='003' and m.getdutycode=n.code and n.code1=b.riskcode)"  
//			+" and a.caseno='C1100111024000001'"
//			+" and exists (select 1 from lcpol where polno=b.polno and appflag='1' and stateflag='1' union select 1 from lbpol where appflag='1' and stateflag='1')"
//			+" group by b.grpcontno,b.contno,b.riskcode,b.polno,a.customerno,b.getdutycode"
//			+" having summoney >0";
//        System.out.println("tContSql==="+tDetSql);
    }
}
