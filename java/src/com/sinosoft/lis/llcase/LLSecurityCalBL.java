package com.sinosoft.lis.llcase;

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;
import java.text.DecimalFormat;

/**
 * <p>Title: 简易社保结算账单推算</p>
 *
 * <p>Description: 目前只用于北分医保门诊刷卡</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLSecurityCalBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private MMap map = new MMap();
    private String mOperate = "";
    private String mManageCom = "";
    private String mOperator = "";

    //全局数据
    private GlobalInput mGlobalInput = new GlobalInput();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    //账单信息
    private LLFeeMainSchema mLLFeeMainSchema = new LLFeeMainSchema();
    private LLSecurityReceiptSchema mLLSecurityReceiptSchema = new
            LLSecurityReceiptSchema();
    private LLFeeOtherItemSet mLLFeeOtherItemSet = new LLFeeOtherItemSet();

    private String mCaseNo = "";
    private String mMainFeeNo = "";
    private String mInsuredStat = "";
    private int mAge = 0;
    private String mFeeAtti = "";
    private String mFeeType = "";
    private String mFeeDate = "";
    private double mGetRate = 0.0;
    private double mRetireGetRate = 0.0;
    private double mPeakLine = 0.0;

    //合计金额
    private double mApplyAmnt = 0.0;
    //医疗保险范围内金额
    private double mFeeInSecu = 0.0;
    //统筹基金支付金额
    private double mPlanFee = 0.0;
    //本次医疗保险基金支付
    private double mSecurityFee = 0.0;
    //退休人员补充医疗费用
    private double mRetireAddFee = 0.0;
    //个人负担
    private double mSecuRefuseFee = 0.0;
    //年度门诊大额医疗互助基金累计支付
    private double mYearSupDoorFee = 0.0;
    //自付一
    private double mSelfPay1 = 0.0;
    //小额门急诊
    private double mSmallDoorPay = 0.0;
    //大额门急诊
    private double mHighDoorAmnt = 0.0;
    //医疗保险范围外金额
    private double mFeeOutSecu = 0.0;

    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    public LLSecurityCalBL() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData 输入数据
     * @param cOperate String 操作类型
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到输入数据
        if (!getInputData()) {
            return false;
        }

        //检查数据合法性
        if (!checkInputData()) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLSecurityCalBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有数据对象
     * @return boolean
     */
    private boolean getInputData() {
        System.out.println("getInputData()..." + mOperate);

        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLLFeeMainSchema = (LLFeeMainSchema) mInputData.getObjectByObjectName(
                "LLFeeMainSchema", 0);
        if (mGlobalInput == null) {
            buildError("getInputData", "登录信息获取失败");
            return false;
        }

        mOperator = mGlobalInput.Operator;

        if (mLLFeeMainSchema == null) {
            buildError("getInputData", "账单信息获取失败！");
            return false;
        }

        mCaseNo = mLLFeeMainSchema.getCaseNo();

        if (mCaseNo == null || "".equals(mCaseNo)) {
            buildError("getInputData", "案件号获取失败！");
            return false;
        }

        mMainFeeNo = mLLFeeMainSchema.getMainFeeNo();

        if (mMainFeeNo == null || "".equals(mMainFeeNo)) {
            buildError("getInputData", "账单号获取失败！");
            return false;
        }

        return true;
    }

    /**
     * 对输入数据做必要的校验
     * @return boolean
     */
    private boolean checkInputData() {
        System.out.println("checkInputData()..." + mOperate);

        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mCaseNo);
        if (!tLLCaseDB.getInfo()) {
            buildError("checkInputData", "案件查询失败！");
            return false;
        }

        mLLCaseSchema = tLLCaseDB.getSchema();

        String tRgtState = mLLCaseSchema.getRgtState();

        if (!"batchCal".equals(mOperate)) {
            if (!("01".equals(tRgtState) || "02".equals(tRgtState) ||
                  "08".equals(tRgtState))) {
                buildError("checkInputData", "该案件状态不允许进行操作！");
                return false;
            }
        }
        if (!mOperator.equals(mLLCaseSchema.getHandler()) &&
            !mOperator.equals(mLLCaseSchema.getClaimer())) {
            buildError("checkInputData", "您不是案件处理人或账单录入人，没有操作权限！");
            return false;
        }

        mManageCom = mLLCaseSchema.getMngCom();

        LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
        tLLFeeMainDB.setMainFeeNo(mMainFeeNo);
        if (!tLLFeeMainDB.getInfo()) {
            buildError("checkInputData", "账单查询失败！");
            return false;
        }

        mLLFeeMainSchema = tLLFeeMainDB.getSchema();

        mInsuredStat = mLLFeeMainSchema.getInsuredStat();

        if (mInsuredStat == null || "".equals(mInsuredStat)) {
            buildError("checkInputData", "账单未录入参保人员类别，请先保存后再进行推算！");
            return false;
        }

        mAge = mLLFeeMainSchema.getAge();

        mFeeAtti = mLLFeeMainSchema.getFeeAtti();
        if (!"batchCal".equals(mOperate)) {
            if (!"4".equals(mFeeAtti)) {
                buildError("checkInputData", "目前推算只针对简易社保结算账单！");
                return false;
            }
        }
        mFeeType = mLLFeeMainSchema.getFeeType();
        if (!"1".equals(mFeeType)) {
            buildError("checkInputData", "目前推算只针对门诊账单！");
            return false;
        }

        mFeeDate = mLLFeeMainSchema.getFeeDate();
        if (mFeeDate == null || "".equals(mFeeDate)) {
            buildError("checkInputData", "结算日期不能为空！");
            return false;
        }

        LLSecurityReceiptDB tLLSecurityReceiptDB = new LLSecurityReceiptDB();
        tLLSecurityReceiptDB.setFeeDetailNo(mMainFeeNo);
        if (!tLLSecurityReceiptDB.getInfo()) {
            buildError("checkInputData", "账单社保信息查询失败！");
            return false;
        }

        mLLSecurityReceiptSchema = tLLSecurityReceiptDB.getSchema();

        mApplyAmnt = mLLSecurityReceiptSchema.getApplyAmnt();
        if (mApplyAmnt <= 0) {
            buildError("checkInputData", "账单合计金额错误！");
            return false;
        }

        mFeeInSecu = mLLSecurityReceiptSchema.getFeeInSecu();
        if (mFeeInSecu < 0) {
            buildError("checkInputData", "医疗保险范围内金额不能为负！");
            return false;
        }

        mSecurityFee = mLLSecurityReceiptSchema.getSecurityFee();
        if (mSecurityFee < 0) {
            buildError("checkInputData", "本次医疗保险金额不能为负！");
            return false;
        }

        mSecuRefuseFee = mLLSecurityReceiptSchema.getSecuRefuseFee();
        if (mSecuRefuseFee < 0) {
            buildError("checkInputData", "个人负担金额错误！");
            return false;
        }

        mYearSupDoorFee = mLLSecurityReceiptSchema.getYearSupDoorFee();
        if (mYearSupDoorFee < 0) {
            buildError("checkInputData", "年度门诊大额医疗互助基金累计支付金额不能为负！");
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @param cOperate String 操作类型
     * @return boolean
     */
    private boolean dealData() {
        System.out.println("dealData()..." + mOperate);

        if (!getSecurityFactor()) {
            return false;
        }

        if ("1".equals(mFeeType)) {
            if (!calDoorPay()) {
                return false;
            }
        }

        if (!dealSecurityReceipt()) {
            return false;
        }

        if (!dealOtherFeeItem()) {
            return false;
        }

        mResult.add(mLLFeeMainSchema);

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        System.out.println("prepareOutputData()...");

        try {
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ICaseCureBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 门诊账单推算
     * @return boolean
     */
    private boolean calDoorPay() {
        mSelfPay1 = Arith.round(mFeeInSecu - mSecurityFee, 2);

        if (mSelfPay1 < 0) {
            buildError("calDoorPay", "自付一计算错误，请检查医疗保险范围内金额、统筹基金支付金额是否录入正确！");
            return false;
        }

        if ("1".equals(mInsuredStat)) {
            mPlanFee = Arith.round(mSecurityFee, 2);
            mRetireAddFee = 0.0;
        } else {
            if (!getRetireRate()) {
                return false;
            }
            mPlanFee = Arith.round(mSecurityFee * mGetRate /
                                   (mGetRate * (1 - mRetireGetRate) +
                                    mRetireGetRate), 2);
            mRetireAddFee = Arith.round(mSecurityFee - mPlanFee, 2);
        }

        if (mPlanFee < 0) {
            buildError("calDoorPay", "统筹基金支付推算错误！");
            return false;
        }

        if (mYearSupDoorFee < mPlanFee) {
            buildError("calDoorPay", "年度门诊大额医疗互助基金累计支付金额不能小于本次统筹基金支付金额！");
            return false;
        }

        if (mRetireAddFee < 0) {
            buildError("calDoorPay", "退休人员补充医疗费用推算错误！");
            return false;
        }

        //年度门诊大额医疗互助基金累计支付为0，则小额门急诊金额为医疗保险范围内金额
        if (mYearSupDoorFee == 0) {
            mSmallDoorPay = Arith.round(mFeeInSecu, 2);
            mHighDoorAmnt = 0.0;
        }

        //年度门诊大额医疗互助基金累计支付大于0小于大额封顶线，且大于统筹基金支付，则小额门急诊金额为0，大额门急诊金额为自付一金额。
        //年度门诊大额医疗互助基金累计支付大于0小于大额封顶线，且等于统筹基金支付，则大额门急诊金额为本次统筹基金支付金额/该人对应医保门诊赔付比例*（1-该人对应医保门诊赔付比例）-退休人员补充医疗费用，小额门急诊金额为自付一-大额门急诊金额。
        //如果年度门诊大额医疗互助基金累计支付大于等于大额封顶线，且统筹基金支付为0，则小额门急诊金额为0，大额门急诊金额为0。
        //如果年度门诊大额医疗互助基金累计支付大于等于大额封顶线，且统筹基金支付大于0，则小额门急诊金额为0，大额门急诊金额为本次统筹基金支付金额/该人对应医保门诊赔付比例（考虑在职、退休、年龄、年度等因素）*（1-该人对应医保门诊赔付比例）-退休人员补充医疗费用。
        if (mYearSupDoorFee > 0) {
            if (mYearSupDoorFee < mPeakLine) {
                if (mYearSupDoorFee > mPlanFee) {
                    mSmallDoorPay = 0.0;
                    mHighDoorAmnt = Arith.round(mSelfPay1, 2);
                } else {
                    mHighDoorAmnt = Arith.round(mPlanFee / mGetRate *
                                                (1 - mGetRate) - mRetireAddFee,
                                                2);
                    mSmallDoorPay = Arith.round(mSelfPay1 - mHighDoorAmnt, 2);
                }
            } else {
                if (mPlanFee == 0) {
                    mSmallDoorPay = 0.0;
                    mHighDoorAmnt = 0.0;
                } else {
                    mSmallDoorPay = 0.0;
                    mHighDoorAmnt = Arith.round(mPlanFee / mGetRate *
                                                (1 - mGetRate) - mRetireAddFee,
                                                2);
                }
            }
        }

        if (mSmallDoorPay < 0) {
            buildError("calDoorPay", "小额门急诊金额计算错误！");
            return false;
        }

        if (mHighDoorAmnt < 0) {
            buildError("calDoorPay", "大额门急诊金额计算错误！");
            return false;
        }

        mFeeOutSecu = Arith.round(mApplyAmnt - mFeeInSecu, 2);
        if (mFeeOutSecu < 0) {
            buildError("calDoorPay", "社保外金额计算错误！");
            return false;
        }
        return true;
    }

    /**
     * 修改社保账单
     * @return boolean
     */
    private boolean dealSecurityReceipt() {
        mLLSecurityReceiptSchema.setSelfPay1(mSelfPay1);
        mLLSecurityReceiptSchema.setFeeOutSecu(mFeeOutSecu);
        mLLSecurityReceiptSchema.setPlanFee(mPlanFee);
        mLLSecurityReceiptSchema.setRetireAddFee(mRetireAddFee);

        if ("1".equals(mFeeType)) {
            mLLSecurityReceiptSchema.setSmallDoorPay(mSmallDoorPay);
            mLLSecurityReceiptSchema.setHighDoorAmnt(mHighDoorAmnt);
        }

        mLLSecurityReceiptSchema.setOperator(mOperator);
        mLLSecurityReceiptSchema.setModifyDate(mCurrentDate);
        mLLSecurityReceiptSchema.setModifyTime(mCurrentTime);

        map.put(mLLSecurityReceiptSchema, "UPDATE");

        return true;
    }

    /**
     * 修改分段费用明细
     * @return boolean
     */
    private boolean dealOtherFeeItem() {
        String tLimit = PubFun.getNoLimit(mManageCom);

        LLFeeOtherItemDB tLLFeeOtherItemDB = new LLFeeOtherItemDB();
        String[] tItem = null;
        double[] tFeeMoney = null;
        if ("1".equals(mFeeType)) {
            tItem = new String[] {"SelfPay1", "FeeOutSecu", "SmallDoorPay",
                    "HighDoorAmnt", "PlanFee", "RetireAddFee"};
            tFeeMoney = new double[] {mSelfPay1, mFeeOutSecu, mSmallDoorPay,
                        mHighDoorAmnt, mPlanFee, mRetireAddFee};
        }

        for (int i = 0; i < tItem.length; i++) {
            tLLFeeOtherItemDB.setCaseNo(mLLFeeMainSchema.getCaseNo());
            tLLFeeOtherItemDB.setRgtNo(mLLFeeMainSchema.getRgtNo());
            tLLFeeOtherItemDB.setMainFeeNo(mLLFeeMainSchema.getMainFeeNo());
            tLLFeeOtherItemDB.setAvliReason(tItem[i]);

            LLFeeOtherItemSet tSelfPay1Set = tLLFeeOtherItemDB.query();
            if (tSelfPay1Set.size() > 0) {
                LLFeeOtherItemSchema tFeeOtherItemSchema = tSelfPay1Set.get(1);
                tFeeOtherItemSchema.setFeeMoney(tFeeMoney[i]);
                tFeeOtherItemSchema.setOperator(mOperator);
                tFeeOtherItemSchema.setModifyDate(mCurrentDate);
                tFeeOtherItemSchema.setModifyTime(mCurrentTime);

                map.put(tFeeOtherItemSchema, "UPDATE");
            } else {
                LLFeeOtherItemSchema tFeeOtherItemSchema = new
                        LLFeeOtherItemSchema();
                tFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo(
                        "FEEDETAILNO",
                        tLimit));
                tFeeOtherItemSchema.setMainFeeNo(mLLFeeMainSchema.getMainFeeNo());
                tFeeOtherItemSchema.setCaseNo(mLLFeeMainSchema.getCaseNo());
                tFeeOtherItemSchema.setRgtNo(mLLFeeMainSchema.getRgtNo());
                tFeeOtherItemSchema.setItemClass("S");

                LDCodeDB tLDCodeDB = new LDCodeDB();
                tLDCodeDB.setCodeType("llsecufeeitem");
                tLDCodeDB.setCodeAlias(tItem[i]);
                tLDCodeDB.setComCode(mManageCom.substring(0, 4));
                LDCodeSet tLDCodeSet = tLDCodeDB.query();
                if (tLDCodeSet.size() <= 0) {
                    buildError("dealOtherFeeItem", "社保账单项目未配置！");
                    return false;
                }
                String tCode = tLDCodeSet.get(1).getCode();
                String tCodeName = tLDCodeSet.get(1).getCodeName();

                tFeeOtherItemSchema.setItemCode(tCode);
                tFeeOtherItemSchema.setDrugName(tCodeName);
                tFeeOtherItemSchema.setFeeMoney(tFeeMoney[i]);
                tFeeOtherItemSchema.setAvliFlag("1");
                tFeeOtherItemSchema.setAvliReason(tItem[i]);
                tFeeOtherItemSchema.setMngCom(mGlobalInput.ManageCom);
                tFeeOtherItemSchema.setOperator(mOperator);
                tFeeOtherItemSchema.setMakeDate(mCurrentDate);
                tFeeOtherItemSchema.setMakeTime(mCurrentTime);
                tFeeOtherItemSchema.setModifyDate(mCurrentDate);
                tFeeOtherItemSchema.setModifyTime(mCurrentTime);

                map.put(tFeeOtherItemSchema, "INSERT");
            }
        }
        return true;
    }

    /**
     * 获取社保政策
     * @return boolean
     */
    private boolean getSecurityFactor() {
        LLSecurityFactorDB tLLSecurityFactorDB = new LLSecurityFactorDB();

        String tSecurityType = "";
        String tSecuritySQL = "";
        if ("1".equals(mFeeType)) {
            tSecurityType = "2001";
            if (mAge >= 70) {
                mInsuredStat = "3";
            }

            tSecuritySQL = "select * From LLSecurityFactor where feetype = '1'"
                           + " and comcode='" + mManageCom.substring(0, 4)
                           + "' and insuredstat='" + mInsuredStat +
                           "' and securitytype='" + tSecurityType + "'"
                           + " and startdate <= '" + mFeeDate +
                           "' and (enddate >= '" +
                           mFeeDate +
                           "' or enddate is null) ";
        }

        LLSecurityFactorSet tLLSecurityFactorSet = tLLSecurityFactorDB.
                executeQuery(tSecuritySQL);

        if (tLLSecurityFactorSet == null || tLLSecurityFactorSet.size() <= 0) {
            buildError("getSecurityFactor", "该人对应的社保政策查询失败！");
            return false;
        }

        mGetRate = tLLSecurityFactorSet.get(1).getGetRate();
        mPeakLine = tLLSecurityFactorSet.get(1).getPeakLine();
        return true;
    }

    /**
     * 获得退休人员额外赔付金额
     * @return boolean
     */
    private boolean getRetireRate() {
        LLSecurityFactorDB tLLSecurityFactorDB = new LLSecurityFactorDB();
        String tSecuritySQL = "select * From LLSecurityFactor where "
                              + " comcode='" + mManageCom.substring(0, 4)
                              + "' and insuredstat='2' and securitytype='4001'"
                              + " and startdate <= '" + mFeeDate +
                              "' and (enddate >= '" +
                              mFeeDate +
                              "' or enddate is null) ";

        LLSecurityFactorSet tLLSecurityFactorSet = tLLSecurityFactorDB.
                executeQuery(tSecuritySQL);

        if (tLLSecurityFactorSet.size() > 0) {
            mRetireGetRate = tLLSecurityFactorSet.get(1).getGetRate();
        }
        return true;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLSecurityCalBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 得到处理后的结果数据
     * @return boolean
     */
    public VData getResult() {
        return this.mResult;
    }

    public static void main(String[] args) {

    }
}
