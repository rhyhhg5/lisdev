package com.sinosoft.lis.llcase;

import java.util.StringTokenizer;

import com.sinosoft.lis.db.LLBnfDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLBnfSchema;
import com.sinosoft.lis.vschema.LLBnfSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LLBnfBL
{
  public CErrors mErrors = new CErrors();
  private VData mResult = new VData();

  private VData mInputData = new VData();
  private MMap map = new MMap();

  private GlobalInput mGlobalInput = new GlobalInput();
  private String mOperate;
  private LLBnfSchema mLLBnfSchema = new LLBnfSchema();
  private LLBnfSet mLLBnfSet = new LLBnfSet();
  private String mCaseNo;

  public static void main(String[] args)
  {
  }
  
  public LLBnfBL() {
  }

  public boolean submitData(VData cInputData, String cOperate)
  {
    this.mOperate = cOperate;

    System.out.println("begin getinputdata");
    if (!(getInputData(cInputData))) {
      return false;
    }
    if (!(dealData()))
    {
      CError tError = new CError();
      tError.moduleName = "LLBnfBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据处理失败LLBnfBL-->dealData!";
      this.mErrors.addOneError(tError);
      return false;
    }

    if (!(prepareOutputData()))
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      submitquery();
    }
    else
    {
      PubSubmit tPubSubmit = new PubSubmit();
      if (!(tPubSubmit.submitData(this.mInputData, this.mOperate)))
      {
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        CError tError = new CError();
        tError.moduleName = "LDDrugBL";
        tError.functionName = "OLDDrugBL";
        tError.errorMessage = "数据提交失败!";

        this.mErrors.addOneError(tError);
        return false;
      }
      System.out.println("End OLDDrugBL Submit...");
    }
    this.mInputData = null;
    return true;
  }

  private boolean dealData()
  {
    int i;
    if ((this.mOperate.equals("INSERT||MAIN")) && 
      (this.mLLBnfSet != null) && (this.mLLBnfSet.size() > 0))
    {
      for (i = 1; i <= this.mLLBnfSet.size(); ++i)
      {
        String tCaseNo = this.mLLBnfSet.get(i).getCaseNo();
        mCaseNo = tCaseNo;
    	if(tCaseNo=="" || "".equals(tCaseNo) || tCaseNo==null){
            CError tError = new CError();
            tError.moduleName = "LLBnfBL";
            tError.functionName = "dealData()";
            tError.errorMessage = "案件号为空，请核对数据!";
            this.mErrors.addOneError(tError);
            return false;
        }  
        
    	String tIDStartDate = this.mLLBnfSet.get(i).getIDStartDate();
    	String tIDEndDate = this.mLLBnfSet.get(i).getIDEndDate();
    	if(!validateDate(tIDStartDate) && !"".equals(tIDStartDate)){
            System.out.println("证件生效日期格式错误，应为‘yyyy-mm-dd’");
            CError tError = new CError();
            tError.moduleName = "LLBnfBL";
            tError.functionName = "dealData()";
            tError.errorMessage = "证件生效日期格式错误，应为‘yyyy-mm-dd’!";
            this.mErrors.addOneError(tError);
            return false;
        }  
    	
    	if(!validateDate(tIDEndDate) && !"".equals(tIDEndDate)){
            System.out.println("证件失效日期格式错误，应为‘yyyy-mm-dd’");
            CError tError = new CError();
            tError.moduleName = "LLBnfBL";
            tError.functionName = "dealData()";
            tError.errorMessage = "证件失效日期格式错误，应为‘yyyy-mm-dd’!";
            this.mErrors.addOneError(tError);
            return false;
        }
    	  
        this.mLLBnfSet.get(i).setBnfNo(PubFun1.CreateMaxNo("BnfNo", 20));
        this.mLLBnfSet.get(i).setMakeDate(PubFun.getCurrentDate());
        this.mLLBnfSet.get(i).setMakeTime(PubFun.getCurrentTime());
        this.mLLBnfSet.get(i).setModifyDate(PubFun.getCurrentDate());
        this.mLLBnfSet.get(i).setModifyTime(PubFun.getCurrentTime());
        this.mLLBnfSet.get(i).setOperator(this.mGlobalInput.Operator);
        
        
      }
      String tSQL = "DELETE FROM llbnf WHERE caseno='"+mCaseNo+"'";
      this.map.put(tSQL,"DELETE");

      this.map.put(this.mLLBnfSet, "DELETE&INSERT");
    }

    if ((this.mOperate.equals("UPDATE||MAIN")) && 
      (this.mLLBnfSet != null) && (this.mLLBnfSet.size() > 0))
    {
      for (i = 1; i <= this.mLLBnfSet.size(); ++i)
      {
        this.mLLBnfSet.get(i).setBnfNo(PubFun1.CreateMaxNo("BnfNo", 20));
        this.mLLBnfSet.get(i).setMakeDate(PubFun.getCurrentDate());
        this.mLLBnfSet.get(i).setMakeTime(PubFun.getCurrentTime());
        this.mLLBnfSet.get(i).setModifyDate(PubFun.getCurrentDate());
        this.mLLBnfSet.get(i).setModifyTime(PubFun.getCurrentTime());
        this.mLLBnfSet.get(i).setOperator(this.mGlobalInput.Operator);
      }
      this.map.put(this.mLLBnfSet, "UPDATE");
    }

    return true;
  }

//  private boolean updateData()
//  {
//    return true;
//  }
//
//  private boolean deleteData()
//  {
//    return true;
//  }

  private boolean getInputData(VData cInputData)
  {
    this.mLLBnfSet.set((LLBnfSet)cInputData.getObjectByObjectName("LLBnfSet", 0));
    this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput", 0));
    return true;
  }

  private boolean submitquery()
  {
    this.mResult.clear();
    LLBnfDB tLLBnfDB = new LLBnfDB();
    tLLBnfDB.setSchema(this.mLLBnfSchema);

    if (tLLBnfDB.mErrors.needDealError())
    {
      this.mErrors.copyAllErrors(tLLBnfDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "LLBnfBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    this.mInputData = null;
    return true;
  }

  private boolean prepareOutputData()
  {
    try {
      this.mInputData.clear();
      this.mInputData.add(this.mLLBnfSchema);
      this.mInputData.add(this.map);
      this.mResult.clear();
      this.mResult.add(this.mLLBnfSchema);
    }
    catch (Exception ex)
    {
      CError tError = new CError();
      tError.moduleName = "LLBnfBL";
      tError.functionName = "prepareData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
	 * 校验日期
	 * @param strDate
	 * @return
	 */
	public boolean validateDate(String strDate)
  {
      int yyyy = 0000;
      int mm = 00;
      int dd = 00;
      // 校验是否有非法字符
      if (!PubFun.validateNumber(strDate))
      {
          return false;
      }

      if (strDate.indexOf("-") >= 0)
      {
          StringTokenizer token = new StringTokenizer(strDate, "-");
          int i = 0;
          while (token.hasMoreElements())
          {
              if (i == 0)
              {
                  yyyy = Integer.parseInt(token.nextToken());
              }
              if (i == 1)
              {
                  mm = Integer.parseInt(token.nextToken());
              }
              if (i == 2)
              {
                  dd = Integer.parseInt(token.nextToken());
              }
              i++;
          }
      }
      else
      {
          if (strDate.length() != 8)
          {
              return false;
          }
          yyyy = Integer.parseInt(strDate.substring(0, 4));
          mm = Integer.parseInt(strDate.substring(4, 6));
          dd = Integer.parseInt(strDate.substring(6, 8));
      }
      if (yyyy > 9999 || yyyy < 1800)
      {
          return false;
      }
      if (mm > 12 || mm < 1)
      {
          return false;
      }
      if (dd > 31 || dd < 1)
      {
          return false;
      }
      if ((mm == 4 || mm == 6 || mm == 9 || mm == 11) && (dd == 31))
      {
          return false;
      }
      if (mm == 2)
      {
          // 校验闰年的情况下__日期格式
          boolean leap = (yyyy % 4 == 0 && (yyyy % 100 != 0 || yyyy % 400 == 0));
          if (dd > 29 || (dd == 29 && !leap))
          {
              return false;
          }
      }

      return true;
  }

  public VData getResult() {
    return this.mResult;
  }
}