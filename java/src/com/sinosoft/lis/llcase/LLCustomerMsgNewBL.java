package com.sinosoft.lis.llcase;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import java.util.*;

/**
 * <p>Title: 客户理赔短信通知(新)</p>
 *
 * <p>Description: 首先支持#1631 江苏分公司理赔短信发送功能</p>
 *
 * <p>Copyright: Copyright (c) 2014-01-13</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLCustomerMsgNewBL {
    private GlobalInput mG = new GlobalInput();
    private String mManageCom = "";
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mLastDate = "";
    private ExeSQL tExeSQL = new ExeSQL();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public LLCustomerMsgNewBL() {}

    private boolean getInputData(VData cInputData) {
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mManageCom = mG.ManageCom;
        String AheadDays = "-1";
        FDate tD = new FDate();
        Date tLastDate = PubFun.calDate(tD.getDate(mCurrentDate),
                                        Integer.parseInt(AheadDays), "D", null);
       mLastDate = tD.getString(tLastDate);
       // mLastDate="2013-03-20";

        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) {
        cInputData = (VData) cInputData.clone();

        if (!getInputData(cInputData)) {
            return false;
        }
        if (cOperate != null && cOperate.trim().equals("LPMSG")) {
            sendMsg();
        } else {
            mErrors.addOneError(new CError("不支持的操作字符串！"));
            return false;
        }
        return true;
    }

    private boolean sendMsg() {
        System.out.println("客户理赔短信通知批处理开始......");

        if(!getMessage()){
        	mErrors.addOneError(new CError("短信批处理失败"));
            return false;
        }

        return true;
    }

    private boolean getMessage() {

        String tSQL =
        	//查询个险
        	" SELECT r.mngcom, "
        	+ " g.paymode, "
        	+ " c.caseno, "
        	+ " r.RgtantMobile, "//申请人手机
        	+ " r.RgtantName, "//申请人姓名
        	+ " g.bankaccno, "
        	+ " g.drawer, "
        	+ " '' ,"
        	+ " g.sumgetmoney, "
        	+ " g.othernotype " //红利领取分为两笔数据   2017-07-18
        	+ " FROM llcase c, llregister r, ljaget g, llclaim cl "
        	+ " WHERE c.caseno = r.rgtno "
        	+ " AND r.rgtobjno = r.customerno "
        	+ " AND c.caseno = cl.caseno "
        	+ " AND  ((g.paymode ='1' and c.rgtstate in('11','12') and g.makedate ='"+ mLastDate+ "') or (g.paymode in ('3', '4', '11') and c.rgtstate='12' and g.confdate ='"+ mLastDate+ "')) "
        	+ " AND c.caseno = g.otherno "
        	+ " AND r.rgtantmobile IS NOT NULL "//申请人预留电话
        	+ " AND EXISTS (SELECT 1 "//为个险，不包含个险卡折
        	+ " FROM llclaimdetail clt, lmriskapp ap "
        	+ " WHERE clt.caseno = c.caseno "
        	+ " AND ap.riskcode = clt.riskcode "
        	+ " AND ap.riskprop = 'I' "
        	+ " AND NOT EXISTS (select 1 from lcgrpcont where grpcontno = clt.grpcontno and cardflag in ('2', '3')))"
        	+ " AND c.rgttype = '1'  "//申请类
        	+ " AND cl.givetype in ('1', '2')   "//全额、部分给付
        	+ " and g.paymode in ('1', '3', '4','11')   "//现金、银行转账、转账支票、银行汇款
        	+ " and  not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "//不查询特殊地区
        	+ " union all "
        	//配置团体，导入时导入被保人手机，短信发送被保人
        	+ " SELECT r.mngcom, "
        	+ " g.paymode, "
        	+ " c.caseno, "
        	+ " c.MobilePhone, "
        	+ " c.CustomerName, "
        	+ " g.bankaccno, "
        	+ " g.drawer, "
        	+ " c.customersex, "
        	+ " g.sumgetmoney, "
        	+ " g.othernotype " 
        	+ " FROM llcase c, llregister r, ljaget g, llclaim cl "
        	+ " WHERE c.rgtno = r.rgtno "
        	+ " AND c.caseno = cl.caseno "
        	+ " AND c.rgtstate = '12' "//结案
        	+ " AND c.caseno = g.otherno "
        	+ " AND c.mobilephone IS NOT NULL "//导入被保人手机
        	+ " AND EXISTS "
        	+ " (SELECT 1 FROM llclaimdetail clt, lmriskapp ap "
        	//#2259 （需求编号：2014-218）团体保单理赔案件短信发送功能调整
        	//+ ", LLAppClaimReason cr "//团体
        	+ " WHERE clt.caseno = c.caseno "
        	//+ " and cr.rgtno = clt.grpcontno "
        	//+ " and cr.reasoncode = '97' "//在页面配置过的团单，不含卡折
        	//+ " and cr.reasontype = '9' "
        	+ " AND ap.riskcode = clt.riskcode "
        	+ " AND ap.riskprop = 'G' "
        	+ " and not exists (select 1 from lcgrpcont where grpcontno=clt.grpcontno and cardflag in('2','3'))) "//不发送卡折
        	+ " AND  r.mngcom not like '8644%' and r.mngcom not like '8632%' "
        	+ " AND NOT EXISTS (SELECT 1 "//不包含个险
        	+ " FROM llclaimdetail clt, lmriskapp ap "
        	+ " WHERE clt.caseno = c.caseno "
        	+ " AND ap.riskcode = clt.riskcode "
        	+ " AND ap.riskprop = 'I') "
        	+ " AND c.rgttype = '1' "//申请类
        	+ " AND cl.givetype in ('1', '2') "
        	+ " and g.paymode in ('3', '4', '11')  "
        	+ " AND g.confdate ='"+ mLastDate+ "' "
        	+ " and  not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "
        	+ " union all "
        	//为卡折，不包括个险险种，只发送团险险种，发送给申请人
        	+ " SELECT r.mngcom, "
        	+ " g.paymode, "
        	+ " c.caseno, "
        	+ " r.RgtantMobile, "
        	+ " r.RgtantName,  "
        	+ " g.bankaccno, "
        	+ " g.drawer, "
        	+ " '', "
        	+ " g.sumgetmoney, "
        	+ " g.othernotype " 
        	+ " FROM llcase c, llregister r, ljaget g, llclaim cl "
        	+ " WHERE c.rgtno = r.rgtno "
        	+ " AND c.caseno = cl.caseno "
        	+ " AND  ((g.paymode='1' and c.rgtstate in('11','12') and g.makedate ='"+ mLastDate+ "') or(g.paymode in ('3', '4', '11') and c.rgtstate='12' and g.confdate ='"+ mLastDate+ "')) "
        	+ " AND c.caseno = g.otherno "
        	+ " AND r.RgtantMobile IS NOT NULL "
        	+ " AND EXISTS "
        	+ "  (SELECT 1 "
        	+ " FROM llclaimdetail clt, lcgrpcont cp "
        	+ " WHERE clt.grpcontno = cp.grpcontno and clt.caseno=c.caseno "
        	+ " AND cp.cardflag in ('2','3')) "
        	+ " AND NOT EXISTS (SELECT 1 "
        	+ " FROM llclaimdetail clt, lmriskapp ap "
        	+ " WHERE clt.caseno = c.caseno "
        	+ " AND ap.riskcode = clt.riskcode "
        	+ " AND ap.riskprop = 'I' "
        	+ " AND NOT EXISTS (select 1 from lcgrpcont where grpcontno = clt.grpcontno and cardflag in ('2', '3')))"
        	+ " AND c.rgttype = '1' "
        	+ " AND cl.givetype in ('1', '2') "
        	+ " and g.paymode in ('1','3', '4', '11')  "
        	+ " and  not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "     	
        	+ "union all"
        	//配置团体，导入时未导入被保人手机，短信发送被保险人在录单时录入的手机号
        	+" SELECT r.mngcom, " 
        	+" g.paymode, "
        	+" c.caseno, "
        	+" ins.grpinsuredphone, "
        	+" c.CustomerName, "
        	+" g.bankaccno, "
        	+" g.drawer, "
        	+" c.customersex, "
        	+" g.sumgetmoney, "
        	+" g.othernotype " 
        	+" FROM llcase c, llregister r, ljaget g, llclaim cl,lcinsured ins "
        	+" WHERE c.rgtno = r.rgtno "
        	+" AND c.caseno = cl.caseno "
        	+" AND c.rgtstate = '12' "
        	+" AND c.caseno = g.otherno "
        	+" AND c.mobilephone IS NULL "
        	+ " AND EXISTS "//配置过的团体，不含卡折
        	+ " (SELECT 1 FROM llclaimdetail clt, lmriskapp ap "
        	//#2259 （需求编号：2014-218）团体保单理赔案件短信发送功能调整
        	//+ " , LLAppClaimReason cr "
        	+ " WHERE clt.caseno = c.caseno "
        	//+ " and cr.rgtno = clt.grpcontno "
        	//+ " and cr.reasoncode = '97' "
        	//+ " and cr.reasontype = '9' "
        	+ " AND ap.riskcode = clt.riskcode "
        	+ " AND ap.riskprop = 'G' "
        	+ " and not exists (select 1 from lcgrpcont where grpcontno=clt.grpcontno and cardflag in('2','3'))) "
        	+ " AND  r.mngcom not like '8644%' and r.mngcom not like '8632%' "
        	+" and c.customerno = ins.insuredno "
        	+" and (select contno from llclaimdetail where caseno=c.caseno fetch first 1 rows only)=ins.contno " 
        	+" and ins.grpinsuredphone is not null "
        	+" and g.sumgetmoney>0 "
        	+" AND NOT EXISTS (SELECT 1 " //不含个单
        	+" FROM llclaimdetail clt, lmriskapp ap " 
        	+" WHERE clt.caseno = c.caseno "
        	+" AND ap.riskcode = clt.riskcode " 
        	+" AND ap.riskprop = 'I') "
        	+" AND c.rgttype = '1' " 
        	+" AND cl.givetype in ('1', '2') " 
        	+" and g.paymode in ('3', '4', '11') " 
        	+" and r.togetherflag='1' "  //个人给付
        	+" and r.applyertype in ('2','5') "
        	+" AND g.confdate ='"+ mLastDate+ "' "
        	+" and  not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "
        	+ " union all "	
        	//2014-01-13添加，#1631 江苏分公司团体理赔短信发送功能，‘个人给付’，领取方式为现金
        	+ " SELECT r.mngcom, "
        	+ " g.paymode, "
        	+ " c.caseno, "
        	+ " c.MobilePhone, "
        	+ " c.CustomerName,  "
        	+ " g.bankaccno, "
        	+ " g.drawer, "
        	+ " '', "
        	+ " g.sumgetmoney, "
        	+ " g.othernotype " 
        	+ " FROM llcase c, llregister r, ljaget g, llclaim cl "
        	+ " WHERE c.rgtno = r.rgtno "
        	+ " AND c.caseno = cl.caseno "
        	+ " AND (g.paymode='1' and c.rgtstate in('11','12') and g.makedate ='"+ mLastDate+ "') "//现金，已结案
        	+ " AND c.caseno = g.otherno "
        	+ " AND c.MobilePhone IS NOT NULL "
        	+" AND ( r.mngcom like '8632%') "
        	+ " AND NOT EXISTS (SELECT 1 "//不含个单、卡折
        	+ " FROM llclaimdetail clt, lmriskapp ap "
        	+ " WHERE clt.caseno = c.caseno "
        	+ " AND ap.riskcode = clt.riskcode "
        	+ " AND ap.riskprop = 'I' "
        	+ " AND NOT EXISTS (select 1 from lcgrpcont where grpcontno = clt.grpcontno and cardflag in ('2', '3')))"
        	+ " AND c.rgttype = '1' "
        	+ " AND cl.givetype in ('1', '2') "
        	+" and r.togetherflag='1' "  
        	+ " and g.paymode in ('1') "
        	+ " and g.sumgetmoney > 0 "
        	+ " and not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "     	
        	+ " union all "
        	//2014-01-13添加，‘个人给付’，领取方式为现金，导入时未导入手机号，发送短信给录单时预留的被保人手机
        	+" SELECT r.mngcom, " 
        	+" g.paymode, "
        	+" c.caseno, "
        	+" ins.grpinsuredphone, "
        	+" c.CustomerName, "
        	+" g.bankaccno, "
        	+" g.drawer, "
        	+" '', "
        	+" g.sumgetmoney, "
        	+" g.othernotype " 
        	+" FROM llcase c, llregister r, ljaget g, llclaim cl,lcinsured ins "
        	+" WHERE c.rgtno = r.rgtno "
        	+" AND c.caseno = cl.caseno "
        	+" AND c.rgtstate = '11' "//在“通知状态”
        	+" AND c.caseno = g.otherno "
        	+" AND c.mobilephone IS NULL "
        	+" AND ( r.mngcom like '8632%') "
        	+ " AND NOT EXISTS (SELECT 1 "//不含个单、卡折
        	+ " FROM llclaimdetail clt, lmriskapp ap "
        	+ " WHERE clt.caseno = c.caseno "
        	+ " AND ap.riskcode = clt.riskcode "
        	+ " AND ap.riskprop = 'I' "
        	+ " AND NOT EXISTS (select 1 from lcgrpcont where grpcontno = clt.grpcontno and cardflag in ('2', '3')))"
        	+" and c.customerno = ins.insuredno "
        	+" and (select contno from llclaimdetail where caseno=c.caseno fetch first 1 rows only)=ins.contno " 
        	+" and ins.grpinsuredphone is not null "
        	+" and g.sumgetmoney>0 "
        	+" AND c.rgttype = '1' " 
        	+" AND cl.givetype in ('1', '2') " 
        	+ " and g.paymode in ('1') "
        	+" and r.togetherflag='1' "  
        	+" AND g.makedate ='"+ mLastDate+ "' "//财务生成日期在前一天
        	+" and  not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "
//        	深圳外包短信发送 
        	+ " union all "
        	+ " SELECT distinct r.mngcom, "
        	+ " g.paymode, "
        	+ " c.rgtno, "
        	+ " c.mobilephone, "
        	+ "  c.CustomerName,  "
        	+ " g.bankaccno, "
        	+ " g.drawer, "
        	+ " c.mobilephone, "
        	+ " g.sumgetmoney, "
        	+ " g.othernotype " 
        	+ " FROM llcase c, llregister r, ljaget g, llclaim cl "
        	+ " WHERE c.rgtno = r.rgtno "
        	+ " AND c.caseno = cl.caseno "
        	+ " AND  ((g.paymode='1' and c.rgtstate in('11','12') and g.makedate ='"+ mLastDate+ "') or(g.paymode in ('3', '4', '11') and c.rgtstate='12' and g.confdate ='"+ mLastDate+ "')) "
        	+ " AND r.rgtno = g.otherno "
        	+ " AND c.mobilephone IS NOT NULL "   
        	+ " AND cl.givetype in ('1', '2') "
        	+ " and g.paymode in ('1','3', '4', '11')  "
        	+ " and  exists(select 1 from llhospcase where caseno=c.caseno and casetype='04') " 
        	+ " with ur ";
        
        System.out.println(tSQL);

        SSRS tMsgSSRS = tExeSQL.execSQL(tSQL);

        for (int i = 1; i <= tMsgSSRS.getMaxRow(); i++) {

            String tManageCom = tMsgSSRS.GetText(i, 1);
            String tPayMode = tMsgSSRS.GetText(i, 2);
            String tCaseNo = tMsgSSRS.GetText(i, 3);
            String tMobile = tMsgSSRS.GetText(i, 4);
//            String tMobile ="18511791834";
            String tCustomerName = tMsgSSRS.GetText(i, 5);
            String tBankAccNo = tMsgSSRS.GetText(i, 6);
            String tDrawer = tMsgSSRS.GetText(i, 7);
            String tSex = tMsgSSRS.GetText(i, 8);
            String tPay = tMsgSSRS.GetText(i, 9);
            String tOtherNoType = tMsgSSRS.GetText(i, 10);

            if (tManageCom == null || "".equals(tManageCom) ||
                "null".equals(tManageCom)) {
                continue;
            }

            if (tPayMode == null || "".equals(tPayMode) ||
                "null".equals(tPayMode)) {
                continue;
            }

            if (tMobile == null || "".equals(tMobile) ||
                "null".equals(tMobile)) {
                continue;
            }

            if (tCustomerName == null || "".equals(tCustomerName) ||
                "null".equals(tCustomerName)) {
                continue;
            }

            if (tDrawer == null || "".equals(tDrawer) ||
                "null".equals(tDrawer)) {
                continue;
            }
            
            if (tPay == null || "".equals(tPay) ||
                    "null".equals(tPay)) {
                    continue;
                }
            //用户性别的判断
            String tSexContent = "女士/先生";
            if ("0".equals(tSex)) {
            	tSexContent = "先生";
            } else if ("1".equals(tSex)) {
            	tSexContent = "女士";
            }
            
            MsgSendBase tMsgSendBase = new MsgSendBase();
            tMsgSendBase.setAppntName(tCustomerName);
            tMsgSendBase.setAppntSex(tSexContent);
            tMsgSendBase.setPrem(CommonBL.bigDoubleToCommonString((Double.parseDouble(tPay)),"0.00"));
            tMsgSendBase.setMobilPhone(tMobile);
            tMsgSendBase.setManageCom(tManageCom);
            tMsgSendBase.setOperator(mG.Operator);
            tMsgSendBase.setSpecType("LPMSG");
            
            //判断短信发送类型
            String tTaskCode = "";

            if ("1".equals(tPayMode)) {
            	//团体申请，领取方式为现金
            	tTaskCode = "LP001";
            } else if ("4".equals(tPayMode) || "11".equals(tPayMode)
            		   || "3".equals(tPayMode)) {
            	//团体申请，领取方式为银行转账，转账支票，银行汇款
                if (tBankAccNo == null || "".equals(tBankAccNo) ||
                    "null".equals(tBankAccNo) || tBankAccNo.length() < 4) {
                    continue;
                }
                //银行账户后4位
                tMsgSendBase.setBankAccNo(tBankAccNo.substring(tBankAccNo.length() - 4,
                        tBankAccNo.length()));
                tTaskCode = "LP002";
            }
            //#2366判断该案件是否赔付了分红险    #3088 红利金额分两笔赔款给付,修改短信模板  star
            if(checkFenHong(tCaseNo)){
            	//#2366分红险发短信tMsgSendBase中添加分红金额信息
            	tMsgSendBase=dealHLMSG(tCaseNo,tMsgSendBase);
            	//根据领取方式使用不同模板
            	
            	if ("1".equals(tPayMode)) {
                	//红利现金专属短信模板
            		if("H".equals(tOtherNoType)){
            			tTaskCode = "LP008";
            		} 
//            		else{
//            			tTaskCode = "LP001";
//            		}
                } else if ("4".equals(tPayMode) || "11".equals(tPayMode)|| "3".equals(tPayMode)){
                	//对采用转账支票、银行转账和银行汇款的,用红利专属短信模板LP007
                    if (tBankAccNo == null || "".equals(tBankAccNo) || "null".equals(tBankAccNo) || tBankAccNo.length() < 4) {
                        continue;
                    }
                    String aNeedBankAccNo = "";
                    //银行账户后4位
                    aNeedBankAccNo += tBankAccNo.substring(tBankAccNo.length() - 4,tBankAccNo.length());
                    tMsgSendBase.setBankAccNo(aNeedBankAccNo);
                    if("H".equals(tOtherNoType)){
                    	tTaskCode = "LP009";
                    } 
//                    else{
//                    	tTaskCode = "LP002";
//                    }
               }
            }
            //#3088 红利金额分两笔赔款给付,修改短信模板  end
            if(!"".equals(tTaskCode)){
            	//任务号不为空，调用打印接口类
            	AnalyzeMSG tAnalyzeMSG = new AnalyzeMSG();
            	tAnalyzeMSG.setmMsgSendBase(tMsgSendBase);//接口类必须的参数集合
            	tAnalyzeMSG.applySend(tTaskCode);//发送短信入口
            	
            }
            System.out.println("发送短信：" + tPayMode + mManageCom + tCaseNo +
                    tMobile + tCustomerName);
        }   
        
        //对于广东团体批次案件，团单不需要通过“短信发送团体保单配置”菜单进行配置，只要符合团单短信发送条件，系统自动发送短信。  2013-09-03
        //2014-01-13新增江苏团体批次案件,批次导入时导入被保人手机，发送被保人
        String aSQL =           
        	" SELECT r.mngcom, "
        	+ " g.paymode, "
        	+ " c.caseno, "
        	+ " c.MobilePhone, "
        	+ " c.CustomerName, "
        	+ " g.bankaccno, "
        	+ " g.drawer, "
        	+ " c.customersex, "
        	+ " g.sumgetmoney "
        	+ " FROM llcase c, llregister r, ljaget g, llclaim cl "
        	+ " WHERE c.rgtno = r.rgtno "
        	+ " AND c.caseno = cl.caseno "
        	+ " AND c.rgtstate = '12' "
        	+ " AND c.caseno = g.otherno "
//        	2014-01-13添加，#1631 江苏分公司理赔短信发送功能
        	+ " AND ( r.mngcom like '8644%' or r.mngcom like '8632%') "
        	+ " AND c.mobilephone IS NOT NULL "
        	+ " AND NOT EXISTS (SELECT 1 "
        	+ " FROM llclaimdetail clt, lmriskapp ap "
        	+ " WHERE clt.caseno = c.caseno "
        	+ " AND ap.riskcode = clt.riskcode "
        	+ " AND ap.riskprop = 'I') "
        	+ " AND c.rgttype = '1' "
        	+ " AND cl.givetype in ('1', '2') "
        	+ " and g.paymode in ('3', '4', '11')  "//银行转账、转账支票、银行汇款
        	+ " AND g.confdate ='"+ mLastDate+ "' "
        	+ " and  not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "
        	+ " union all"
        	//导入时未导入被保人手机
        	+" SELECT r.mngcom, " 
        	+" g.paymode, "
        	+" c.caseno, "
        	+" ins.grpinsuredphone, "
        	+" c.CustomerName, "
        	+" g.bankaccno, "
        	+" g.drawer, "
        	+" c.customersex, "
        	+" g.sumgetmoney "
        	+" FROM llcase c, llregister r, ljaget g, llclaim cl,lcinsured ins "
        	+" WHERE c.rgtno = r.rgtno "
        	+" AND c.caseno = cl.caseno "
        	+" AND c.rgtstate = '12' "
        	+" AND c.caseno = g.otherno "
        	+" AND c.mobilephone IS NULL "
        	//2014-01-13添加，#1631 江苏分公司理赔短信发送功能
        	+" AND ( r.mngcom like '8644%' or r.mngcom like '8632%') "
        	+" and c.customerno = ins.insuredno "
        	+" and (select contno from llclaimdetail where caseno=c.caseno fetch first 1 rows only)=ins.contno " 
        	+" and ins.grpinsuredphone is not null "
        	+" and g.sumgetmoney>0 "
        	+" AND NOT EXISTS (SELECT 1 " 
        	+" FROM llclaimdetail clt, lmriskapp ap " 
        	+" WHERE clt.caseno = c.caseno "
        	+" AND ap.riskcode = clt.riskcode " 
        	+" AND ap.riskprop = 'I') "
        	+" AND c.rgttype = '1' " 
        	+" AND cl.givetype in ('1', '2') " 
        	+" and g.paymode in ('3', '4', '11') " 
        	+" and r.togetherflag='1' "  //个人给付
        	+" and r.applyertype in ('2','5') "
        	+" AND g.confdate ='"+ mLastDate+ "' "
        	+" and  not exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(r.mngcom, 1, 6)) "
        	+ " with ur";
        
        System.out.println(aSQL);

        SSRS mMsgSSRS = tExeSQL.execSQL(aSQL);

        for (int i = 1; i <= mMsgSSRS.getMaxRow(); i++) {
            String mManageCom = mMsgSSRS.GetText(i, 1);
            String mPayMode = mMsgSSRS.GetText(i, 2);
            String mCaseNo = mMsgSSRS.GetText(i, 3);
            String mMobile = mMsgSSRS.GetText(i, 4);
           
            String mCustomerName = mMsgSSRS.GetText(i, 5);
            String mBankAccNo = mMsgSSRS.GetText(i, 6);
            String mDrawer = mMsgSSRS.GetText(i, 7);
            String mSex = mMsgSSRS.GetText(i, 8);
            String mPay = mMsgSSRS.GetText(i, 9);
            String mOtherNoType = mMsgSSRS.GetText(i, 10);

            if (mManageCom == null || "".equals(mManageCom) ||
                "null".equals(mManageCom)) {
                continue;
            }

            if (mPayMode == null || "".equals(mPayMode) ||
                "null".equals(mPayMode)) {
                continue;
            }

            if (mMobile == null || "".equals(mMobile) ||
                "null".equals(mMobile)) {
                continue;
            }

            if (mCustomerName == null || "".equals(mCustomerName) ||
                "null".equals(mCustomerName)) {
                continue;
            }

            if (mDrawer == null || "".equals(mDrawer) ||
                "null".equals(mDrawer)) {
                continue;
            }
            
            if (mPay == null || "".equals(mPay) ||
                    "null".equals(mPay)) {
                    continue;
                }
            // 两笔红利金额增加
            if (mOtherNoType == null || "".equals(mOtherNoType) ||
                    "null".equals(mOtherNoType)) {
                    continue;
                }
            //用户性别的判断
            String mSexContent = "女士/先生";
            if ("0".equals(mSex)) {
            	mSexContent = "先生";
            } else if ("1".equals(mSex)) {
            	mSexContent = "女士";
            }
            
            MsgSendBase mMsgSendBase = new MsgSendBase();
            mMsgSendBase.setAppntName(mCustomerName);
            mMsgSendBase.setAppntSex(mSexContent);
            mMsgSendBase.setPrem(CommonBL.bigDoubleToCommonString((Double.parseDouble(mPay)),"0.00"));
            mMsgSendBase.setMobilPhone(mMobile);
            mMsgSendBase.setManageCom(mManageCom);
            mMsgSendBase.setOperator(mG.Operator);
            mMsgSendBase.setSpecType("LPMSG");

//          判断短信发送类型
            String mTaskCode = "";

            if ("1".equals(mPayMode)) {
            	//团体申请，选择“个人给付”，领取方式为现金
            	mTaskCode = "LP001";
            } else if ("4".equals(mPayMode) || "11".equals(mPayMode)
            		   || "3".equals(mPayMode)) {
            	//团体申请，选择“个人给付”，领取方式为银行转账，转账支票，银行汇款
                if (mBankAccNo == null || "".equals(mBankAccNo) ||
                    "null".equals(mBankAccNo) || mBankAccNo.length() < 4) {
                    continue;
                }
                //银行账户后4位
                mMsgSendBase.setBankAccNo(mBankAccNo.substring(mBankAccNo.length() - 4,
                        mBankAccNo.length()));
                mTaskCode = "LP002";
            }
          //#2366判断该案件是否赔付了分红险
            if(checkFenHong(mCaseNo)){
            	//#2366分红险发短信tMsgSendBase中添加分红金额信息
            	mMsgSendBase=dealHLMSG(mCaseNo,mMsgSendBase);
            	//根据领取方式使用不同模板
            	if ("1".equals(mPayMode)) {
                	//红利现金专属短信模板
            		if("H".equals(mOtherNoType)){
            			mTaskCode = "LP008";
            		}
//            		else{
//            			mTaskCode = "LP001";
//            		}
                } else if ("4".equals(mPayMode) || "11".equals(mPayMode)|| "3".equals(mPayMode)){
                	//对采用转账支票、银行转账和银行汇款的,用红利专属短信模板LP007
                    if (mBankAccNo == null || "".equals(mBankAccNo) || "null".equals(mBankAccNo) || mBankAccNo.length() < 4) {
                        continue;
                    }
                    String aNeedBankAccNo = "";
                    //银行账户后4位
                    aNeedBankAccNo += mBankAccNo.substring(mBankAccNo.length() - 4,mBankAccNo.length());
                    mMsgSendBase.setBankAccNo(aNeedBankAccNo);
                    if("H".equals(mOtherNoType)){
                    	mTaskCode = "LP009";
                    }
//                    else{
//                    	mTaskCode = "LP002";
//                    }
                }
            }
            if(!"".equals(mTaskCode)){
            	//任务号不为空，调用打印接口类
            	AnalyzeMSG mAnalyzeMSG = new AnalyzeMSG();
            	mAnalyzeMSG.setmMsgSendBase(mMsgSendBase);//接口类必须的参数集合
            	mAnalyzeMSG.applySend(mTaskCode);//发送短信入口
            	
            }

            System.out.println("发送短信：" + mPayMode + mManageCom + mCaseNo +
                               mMobile + mCustomerName);
        }   
        
        //特殊地区的配置发送
        String strSQL = "select c.RgtantMobile,a.contno,b.mngcom from lcinsured a,llcase b,llregister c "
			+ " where a.insuredno = b.customerno and b.caseno = c.rgtno  and c.RgtantMobile IS NOT NULL "
			+ " and b.makedate ='"+ mLastDate+ "'and b.rgtstate in ('01','02','03') and b.caseno like 'C%' "
			+ " and exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(b.mngcom, 1,6)) "
			+ " group by c.RgtantMobile,a.contno ,b.mngcom "
			+ " union all "
			+ " select c.RgtantMobile,a.contno ,b.mngcom from llclaimdetail a,llcase b,llregister c "
			+ " where a.caseno = b.caseno  and b.caseno = c.rgtno  and c.RgtantMobile IS NOT NULL  and b.makedate ='"+ mLastDate+"'"
			+ " and b.rgtstate not in ('01','02','03')  and b.caseno like 'C%' "
			+ " and exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(b.mngcom, 1, 6)) "
			+ " group by c.RgtantMobile,a.contno ,b.mngcom " 
			+ " union all "
			+ " select distinct RgtantMobile,rgtobjno,mngcom from llregister " 
			+ " where makedate ='"+ mLastDate+ "' and RgtantMobile IS NOT NULL " 
			+ " and exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(mngcom, 1, 6)) " 
			+ " and rgtno like 'P%' with ur";

        SSRS tSSRS = tExeSQL.execSQL(strSQL);

        if (tSSRS.getMaxRow() == 0) {
        	System.out.println("找不到相应的案件");
        } 
        else {
        	String thMobile = "";

        	for (int n = 1; n <= tSSRS.getMaxRow(); n++) {

    			String mMobile = tSSRS.GetText(n, 1);
    			String tContno = tSSRS.GetText(n, 2);
    			String tManageCom = tSSRS.GetText(n, 3);

    			if (thMobile.equals(mMobile)) {
    				continue;
    			} else {

    				thMobile = mMobile;
    				tContno = tContno.substring(tContno.length() - 6);
    				for (int m = n + 1; m <= tSSRS.getMaxRow(); m++) {
    					if (mMobile.equals(tSSRS.GetText(m, 1))) {
    						tContno = tContno+ "、"+ tSSRS.GetText(m, 2).substring(tSSRS.GetText(m, 2).length() - 6);
    						continue;
    					}
    				}
    				
    				MsgSendBase mMsgSendBase = new MsgSendBase();
    				mMsgSendBase.setContno(tContno);
    		        mMsgSendBase.setMobilPhone(mMobile);
    		        mMsgSendBase.setManageCom(mManageCom);
    		        mMsgSendBase.setOperator(mG.Operator);
    		        mMsgSendBase.setSpecType("LPMSG");
	
    		        //特殊配置地区
    				if("863706".equals(tManageCom.substring(0, 6)) || "863710".equals(tManageCom.substring(0, 6))){
    					//判断短信发送类型
    		            String mTaskCode = "";
    		            mTaskCode = "LP003";
    		            
    		            if(!"".equals(mTaskCode)){
    		            	//任务号不为空，调用打印接口类
    		            	AnalyzeMSG mAnalyzeMSG = new AnalyzeMSG();
    		            	mAnalyzeMSG.setmMsgSendBase(mMsgSendBase);//接口类必须的参数集合
    		            	mAnalyzeMSG.applySend(mTaskCode);//发送短信入口
    		            	
    		            }
    				}
    			}
        	}
        }
	
        //特殊地区配置发送2
		String paySQL = " select a.contno,a.caseno,sum(a.realpay),b.RgtantMobile ,b.mngcom  from llclaimdetail a,llregister b "
				+ " where a.caseno = b.rgtno  and exists (select 1 from llcase where a.caseno = caseno and rgtstate in ('11','12')) "
				+ " and exists (select 1 from ljaget where a.caseno = otherno and makedate = '"+ mLastDate+ "') "
				+ " and a.grpcontno = '00000000000000000000' and a.givetype <> '3' and b.RgtantMobile IS NOT NULL  "
				+ " and exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(b.mngcom, 1, 6)) "
				+ " group by a.contno,a.caseno,b.RgtantMobile ,b.mngcom"
				+ " union "
				+ " select a.grpcontno, a.rgtno, sum(a.realpay),b.RgtantMobile ,b.mngcom from llclaimdetail a,llregister b"
				+ " where a.rgtno = b.rgtno"
				+ " and exists (select 1 from llcase where a.caseno = caseno and rgtstate in ('11','12'))"
				+ " and exists (select 1 from ljaget where a.rgtno = otherno and makedate = '"+ mLastDate+ "')"
				+ " and b.RgtantMobile IS NOT NULL  and a.givetype <> '3' and a.grpcontno <> '00000000000000000000'"
				+ " and exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(b.mngcom, 1, 6)) "
				+ " group by a.grpcontno,a.rgtno,b.RgtantMobile ,b.mngcom "
				+ " union "
				+ " select a.grpcontno, a.rgtno, sum(a.realpay),b.RgtantMobile ,b.mngcom from llclaimdetail a,llregister b"
				+ " where a.rgtno = b.rgtno"
				+ " and exists (select 1 from llcase where a.caseno = caseno and rgtstate in ('11','12'))"
				+ " and exists (select 1 from ljaget where a.caseno = otherno and makedate = '"+ mLastDate+ "')"
				+ " and b.RgtantMobile IS NOT NULL  and a.givetype <> '3' and a.grpcontno <> '00000000000000000000'"
				+ " and exists (select 1 from ldcode1 where codetype='LPMsg' and code= Substr(b.mngcom, 1, 6)) "
				+ " group by a.grpcontno,a.rgtno,b.RgtantMobile ,b.mngcom with ur";
		SSRS mSSRS = tExeSQL.execSQL(paySQL);
	
		if (mSSRS.getMaxRow() == 0) {
	
			System.out.println("找不到相应的案件");
	
		} else {
			for (int m = 1; m <= mSSRS.getMaxRow(); m++) {
	
				String tContno = mSSRS.GetText(m, 1);
				String tMoney = mSSRS.GetText(m, 3);
				String mMobile = mSSRS.GetText(m, 4);
				String tManageCom = mSSRS.GetText(m, 5);
				tContno = tContno.substring(tContno.length() - 6);
				
				MsgSendBase mMsgSendBase = new MsgSendBase();
				mMsgSendBase.setContno(tContno);
				mMsgSendBase.setPrem(CommonBL.bigDoubleToCommonString((Double.parseDouble(tMoney)),"0.00"));
				mMsgSendBase.setMobilPhone(mMobile);
		        mMsgSendBase.setManageCom(mManageCom);
		        mMsgSendBase.setOperator(mG.Operator);
		        mMsgSendBase.setSpecType("LPMSG");

		        //特殊配置地区
				if("863706".equals(tManageCom.substring(0, 6)) || "863710".equals(tManageCom.substring(0, 6))){
					//判断短信发送类型
		            String mTaskCode = "";
		            mTaskCode = "LP004";
		            
		            if(!"".equals(mTaskCode)){
		            	//任务号不为空，调用打印接口类
		            	AnalyzeMSG mAnalyzeMSG = new AnalyzeMSG();
		            	mAnalyzeMSG.setmMsgSendBase(mMsgSendBase);//接口类必须的参数集合
		            	mAnalyzeMSG.applySend(mTaskCode);//发送短信入口
		            	
		            }
				}
			}
		}
		
		//3898关于湖北分公司孝感中支大病保险理赔回访短信通过总公司短信平台自动发送的申请 START
		String XGSQL = "select a.customername,a.MobilePhone,a.customersex,a.idno,d.hospitalname,sum(d.SumFee),sum(d.SelfAmnt)," + 
					   "sum(b.RealPay),c.bankaccno,(select bankname from ldbank where 1=1 and bankcode = c.BankCode),b.contno,a.handler,a.mngcom " + 
					   "from llcase a,llclaimdetail b ,ljaget c ,llfeemain d where 1=1 " + 
					   "and a.caseno = b.caseno and a.caseno = c.otherno and a.caseno = d.caseno " + 
					   "and a.rgtstate = '12' " + 				//给付状态
					   "and a.rgttype = '1' " + 				//申请类
					   "and c.paymode in ('3', '4', '11') " + 	//银行转账，银行支票，银行汇款
					   "and b.RealPay > 0 " + 					//赔款金额大于0
					   "and a.MobilePhone is not null " +		//被保人手机号不能为空
					   "and c.makedate = '" + mLastDate + "'" +	//给付前一天
					   "and a.mngcom = '86420900'	" + 			//孝感机构
					   "and b.givetype = '1' " + 				//正常给付
					   "and a.idtype='0' " + 					//身份证
					   "and b.riskcode = '162201' " + 
					   "and exists (select 1 from llregister where ((rgtno = a.caseno) or (rgtno = a.rgtno and togetherflag = '1'))) " + 
					   "group by a.customername,a.MobilePhone,a.customersex,a.idno,d.hospitalname," +
					   "c.bankaccno,c.BankCode,b.contno,a.handler,a.mngcom with ur";
		SSRS XGSSRS = tExeSQL.execSQL(XGSQL);
		System.out.println(XGSQL);
		int XGLength = XGSSRS.getMaxRow();
		if(XGLength > 0) {
			for (int i = 1; i <= XGLength; i++) {
				String mContno = XGSSRS.GetText(i, 11);
				String mOperator = XGSSRS.GetText(i, 12);
				String mMngcom = XGSSRS.GetText(i, 13);
				String mCustomerName = XGSSRS.GetText(i, 1);//被保险人
				String mMobilePhone = XGSSRS.GetText(i, 2);//手机号
				String mCustomerSex = XGSSRS.GetText(i, 3);//性别
				String mIdNo = XGSSRS.GetText(i, 4);//身份证号
				String mHospitalName = XGSSRS.GetText(i, 5);//医院名称
				String mSumFee = XGSSRS.GetText(i, 6);//费用总额
				String mSelfAmnt = XGSSRS.GetText(i, 7);//个人自付
				String mRealPay = XGSSRS.GetText(i, 8);//赔款金额
				String mBankAccno = XGSSRS.GetText(i, 9);//银行卡号
				String mBankName = XGSSRS.GetText(i, 10);//银行名称
						
				if (mCustomerName == null || "".equals(mCustomerName) || "null".equals(mCustomerName)) {
                    continue;
                }
				if (mIdNo == null || "".equals(mIdNo) || "null".equals(mIdNo) || mIdNo.length() < 4) {
                    continue;
                }
				if (mHospitalName == null || "".equals(mHospitalName) || "null".equals(mHospitalName)) {
                    continue;
                }
				if (mSumFee == null || "".equals(mSumFee) || "null".equals(mSumFee)) {
                    continue;
                }
				if (mSelfAmnt == null || "".equals(mSelfAmnt) || "null".equals(mSelfAmnt)) {
                    continue;
                }
				if (mRealPay == null || "".equals(mRealPay) || "null".equals(mRealPay)) {
                    continue;
                }
				if (mBankName == null || "".equals(mBankName) || "null".equals(mBankName)) {
                    continue;
                }
				if (mBankAccno == null || "".equals(mBankAccno) || "null".equals(mBankAccno) || mBankAccno.length() < 4) {
                    continue;
                }
				
				//用户性别的判断
	            String tSexContent = "女士/先生";
	            if ("0".equals(mCustomerSex)) {
	            	tSexContent = "先生";
	            } else if ("1".equals(mCustomerSex)) {
	            	tSexContent = "女士";
	            }
	            
	            MsgSendBase mMsgSendBase = new MsgSendBase();
	            
	            mMsgSendBase.setInsuredName(mCustomerName);
	            mMsgSendBase.setAppntSex(tSexContent);            
				mMsgSendBase.setMobilPhone(mMobilePhone);
//				mMsgSendBase.setMobilPhone("13488780906");			
				mMsgSendBase.setIDNo(mIdNo.substring(mIdNo.length() - 4,mIdNo.length()));
				mMsgSendBase.setHospitalName(mHospitalName);
				mMsgSendBase.setSumFee(mSumFee);
				mMsgSendBase.setPrem(CommonBL.bigDoubleToCommonString((Double.parseDouble(mSumFee)),"0.00"));
				mMsgSendBase.setSelfAmnt(mSelfAmnt);
				mMsgSendBase.setPrem(CommonBL.bigDoubleToCommonString((Double.parseDouble(mSelfAmnt)),"0.00"));
				mMsgSendBase.setPrem(mRealPay);
				mMsgSendBase.setPrem(CommonBL.bigDoubleToCommonString((Double.parseDouble(mRealPay)),"0.00"));
				mMsgSendBase.setBankAccNo(mBankAccno.substring(mBankAccno.length() - 4,mBankAccno.length()));
				mMsgSendBase.setBankName(mBankName);
				mMsgSendBase.setManageCom(mMngcom);
				mMsgSendBase.setOperator(mOperator);
				mMsgSendBase.setSpecType("LPMSG");
				mMsgSendBase.setContno(mContno);
				
				AnalyzeMSG tAnalyzeMSG = new AnalyzeMSG();
            	tAnalyzeMSG.setmMsgSendBase(mMsgSendBase);//接口类必须的参数集合
            	tAnalyzeMSG.applySend("LP010");//发送短信入口
            	
            	System.out.println("湖北孝感短信通知：" + mCustomerName + tSexContent + mHospitalName + mSumFee + mSelfAmnt + mRealPay + mBankName);
			}
		}
		//3898关于湖北分公司孝感中支大病保险理赔回访短信通过总公司短信平台自动发送的申请 END
		
        return true;
    }
    /**
     * TODO:#2366 通过 传入aCaseno判断是否赔了分红险,且赔付金额大于0
     *
     */
    private boolean checkFenHong(String aCaseno){
    	String tSQL11 =  "";
    	tSQL11 = "select 1 from ljagetclaim a where a.otherno='"+aCaseno+
    			"' and othernotype='H' and pay >0 "+
    			" and exists (select 1 from lmriskapp where risktype4 = '2' and riskcode=a.riskcode)";
    	SSRS tMsgSSRS = tExeSQL.execSQL(tSQL11);
    	if(tMsgSSRS==null||tMsgSSRS.getMaxRow()<=0){
    		return false;
    	}else{
	        for (int i = 1; i <= tMsgSSRS.getMaxRow(); i++) {
	        	if("1".equals(tMsgSSRS.GetText(i, 1))){
	        		return true;
	        	}
	        }
	        return false;
    	}
    }
    /**
     * TODO:#2366 分红险理赔案件处理调整,给付通知后短信通知
     * 此案件aCaseno发送短信内容
     *
     */
    public MsgSendBase dealHLMSG(String tCaseNo,MsgSendBase tMsgSendBase) {


    	String tSQL =  "";
    	tSQL = "select sum(pay),sum(case when othernotype='H' then pay else 0 end),sum(pay)-sum(case when othernotype='H' then pay else 0 end) "+
    			" from ljagetclaim where otherno='"+tCaseNo+"'";
//    	tSQL = "select sum(pay) from ljagetclaim where otherno='"+tCaseNo+"' and othernotype='"+OtherNoType+"' " ;
  	
    	SSRS tMsgSSRS = tExeSQL.execSQL(tSQL);

        for (int i = 1; i <= tMsgSSRS.getMaxRow(); i++) {
        	
            String totalAmnt = tMsgSSRS.GetText(i, 1);
            String HLAmnt = tMsgSSRS.GetText(i, 2);
            String nomalAmnt= tMsgSSRS.GetText(i, 3);
            if (totalAmnt == null || "".equals(totalAmnt) ||
                "null".equals(totalAmnt)) {
                continue;
            }

            if (HLAmnt == null || "".equals(HLAmnt) ||
                "null".equals(HLAmnt)) {
                continue;
            }
            if (nomalAmnt == null || "".equals(nomalAmnt) ||
                "null".equals(nomalAmnt)) {
                continue;
            }
            
            tMsgSendBase.setPrem(CommonBL.bigDoubleToCommonString((Double.parseDouble(nomalAmnt)),"0.00"));
            //把分红金额放在BusinessType字段中
            tMsgSendBase.setBusinessType(CommonBL.bigDoubleToCommonString((Double.parseDouble(HLAmnt)),"0.00"));

        }   
        return tMsgSendBase;
	}

	public static void main(String[] args) {
        GlobalInput mGlobalInput = new GlobalInput();
        VData mVData = new VData();
        mGlobalInput.Operator = "cm0001";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);

        LLCustomerMsgNewBL tLLCustomerMsgBL = new LLCustomerMsgNewBL();
        tLLCustomerMsgBL.mG.ManageCom = "86";
        tLLCustomerMsgBL.mG.Operator = "cm0001";
//        String t = "01";
//        String m = t.substring(0,4);
//        System.out.println(m);

        tLLCustomerMsgBL.submitData(mVData, "LPMSG");
//        tLLCustomerMsgBL.dealHLMSG();

    }

}
