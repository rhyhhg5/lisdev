package com.sinosoft.lis.llcase;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class CaseTransferBL {
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private MMap map = new MMap();
    private String mRemark = "";

    private GlobalInput mGlobalInput = new GlobalInput();
    private LLCaseSet mLLCaseSet = new LLCaseSet();

    ExeSQL tExeSQL = new ExeSQL();

    public CaseTransferBL() {
    }

    public static void main(String[] args) {
        LLCaseSet tLLCaseSet = new LLCaseSet();
        CaseTransferBL tCaseTransferBL = new CaseTransferBL();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setCaseNo("C1100060617000001");
        tLLCaseSet.add(tLLCaseSchema);
//         tLLCaseSchema = new LLCaseSchema();
//        tLLCaseSchema.setCaseNo("C3300060713000003");
//        tLLCaseSet.add(tLLCaseSchema);
//        tLLCaseSchema = new LLCaseSchema();
//        tLLCaseSchema.setCaseNo("C1100060616000003");
//        tLLCaseSet.add(tLLCaseSchema);
//        tLLCaseSchema = new LLCaseSchema();
//        tLLCaseSchema.setCaseNo("C9400060418000001");
//        tLLCaseSet.add(tLLCaseSchema);
//        tLLCaseSchema = new LLCaseSchema();
//        tLLCaseSchema.setCaseNo("C9400060418000009");
//        tLLCaseSet.add(tLLCaseSchema);



        String Operate = "cm0001";
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "cm0002";

        VData tVData = new VData();
        tVData.add(tLLCaseSet);
        tVData.add(tG);
        tVData.add(Operate);
        tCaseTransferBL.submitData(tVData, Operate);

    }

    public VData getResult() {
        return mResult;
    }


    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        //得到输入数据
        if (!getInputData()) {
            return false;
        }
//          mOperate = cOperate;
        //检查数据合法性
        if (!checkInputData(cOperate)) {
            return false;
        }

        //进行业务处理
        if (!dealData(cOperate)) {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "CaseTransferBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;

        return true;
    }

    private boolean getInputData() {
        System.out.println("getInputData()...");

        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);

        mLLCaseSet = (LLCaseSet) mInputData.getObjectByObjectName(
                "LLCaseSet", 0);
        mRemark = (String) mInputData.lastElement();

        return true;
    }


    private boolean prepareOutputData() {
        System.out.println("prepareOutputData()...");

        try {
            mInputData.clear();
            mInputData.add(map);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ICaseCureBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    private boolean checkInputData(String cOperate) {
    	// #3901 理赔人员直系亲属回避**start**
    	LLCaseDB tLLCaseDB = new LLCaseDB();
    	for (int i=1;i<=mLLCaseSet.size();i++){
    		LLRelationCaseSet oLLRelationCaseSet = new LLRelationCaseSet(); 
    		LLCaseSchema aLLCaseSchema = mLLCaseSet.get(i);
    		tLLCaseDB.setCaseNo(aLLCaseSchema.getCaseNo());
    		if(!tLLCaseDB.getInfo()){
    			 CError tError = new CError();
                 tError.moduleName = "Casetransfetrbl";
                 tError.functionName = "prepareOutputData";
                 tError.errorMessage = "没有查询到相应的案件";
                 this.mErrors.addOneError(tError);
    			return false;
    		}else{
    			aLLCaseSchema=tLLCaseDB.getSchema();
    		}
    		//校验该理赔人姓名与变更案件的被保险人姓名及身份证号
    		String checkSQL="select relation from llrelationclaim where usercode='"+cOperate+"' "
    				+ " and relationname='"+aLLCaseSchema.getCustomerName()+"' and relationidno='"+aLLCaseSchema.getIDNo()+"' with ur";
    		ExeSQL checkExe = new ExeSQL();
    		SSRS  checkRela = checkExe.execSQL(checkSQL);
    		if(checkRela.getMaxRow()>0){
    			LLRelationCaseSchema tLLRelationCaseSchema = new LLRelationCaseSchema();
    			tLLRelationCaseSchema.setCaseNo(aLLCaseSchema.getCaseNo());
    			tLLRelationCaseSchema.setRgtState(aLLCaseSchema.getRgtState());
    			tLLRelationCaseSchema.setRgtNo(aLLCaseSchema.getRgtNo());
    			tLLRelationCaseSchema.setRelation(checkRela.GetText(1, 1));
    			tLLRelationCaseSchema.setRelationName(aLLCaseSchema.getCustomerName());
    			tLLRelationCaseSchema.setDealer(cOperate);
    			tLLRelationCaseSchema.setOperator(mGlobalInput.Operator);
    			tLLRelationCaseSchema.setSerialNo(PubFun1.CreateMaxNo("RELA_CASE", 20));  
    			tLLRelationCaseSchema.setMakeDate(PubFun.getCurrentDate());
    			tLLRelationCaseSchema.setMakeTime(PubFun.getCurrentTime());
    			tLLRelationCaseSchema.setModifyDate(PubFun.getCurrentDate());
    			tLLRelationCaseSchema.setModifyTime(PubFun.getCurrentTime());
    			tLLRelationCaseSchema.setMngCom(mGlobalInput.ManageCom);
    			
    			oLLRelationCaseSet.add(tLLRelationCaseSchema);
    			
    		}
    		//校验该理赔人姓名与变更案件的赔款领取人姓名
    		String accSQL ="select relation from llrelationclaim where usercode='"+cOperate+"' "
    				+ " and relationname='"+aLLCaseSchema.getAccName()+"' with ur" ;
    		SSRS checkAcc = checkExe.execSQL(accSQL);
    		if(checkAcc.getMaxRow()>0){
    			LLRelationCaseSchema aLLRelationCaseSchema = new LLRelationCaseSchema();
    			aLLRelationCaseSchema.setCaseNo(aLLCaseSchema.getCaseNo());
    			aLLRelationCaseSchema.setRgtState(aLLCaseSchema.getRgtState());
    			aLLRelationCaseSchema.setRgtNo(aLLCaseSchema.getRgtNo());
    			aLLRelationCaseSchema.setRelation(checkAcc.GetText(1, 1));
    			aLLRelationCaseSchema.setRelationName(aLLCaseSchema.getAccName());
    			aLLRelationCaseSchema.setDealer(cOperate);
    			aLLRelationCaseSchema.setOperator(mGlobalInput.Operator);
    			aLLRelationCaseSchema.setSerialNo(PubFun1.CreateMaxNo("RELA_CASE", 20));
    			aLLRelationCaseSchema.setMakeDate(PubFun.getCurrentDate());
    			aLLRelationCaseSchema.setMakeTime(PubFun.getCurrentTime());
    			aLLRelationCaseSchema.setModifyDate(PubFun.getCurrentDate());
    			aLLRelationCaseSchema.setModifyTime(PubFun.getCurrentTime());
    			aLLRelationCaseSchema.setMngCom(mGlobalInput.ManageCom);
    			
    			oLLRelationCaseSet.add(aLLRelationCaseSchema);
    		}
    		
    		if(oLLRelationCaseSet.size()!=0 ){
    			map.put(oLLRelationCaseSet, "INSERT");
				mInputData.clear();
				mInputData.add(map);
				PubSubmit ps = new PubSubmit();
		        if (!ps.submitData(mInputData, null)) {
		            CError.buildErr(this, "理赔人员直系亲属数据库保存失败");
		            return false;
		        }
		        //错误处理
		        CError.buildErr(this, "保存失败，请与总公司理赔人员联系");
		        return false;
    		}
    		
    	}
    	// # 3091 理赔人员直系亲属回避**end**
    	
        return true;
    }

    private boolean dealData(String cOperate) {

        LLCaseBackSet mLLCaseBackSet = new LLCaseBackSet();
        LLCaseDB tLLCaseDB = new LLCaseDB();
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        LLCaseSet tLLCaseSet = new LLCaseSet();
        LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
        LLClaimUserSchema tLLClaimUserSchema = new LLClaimUserSchema();
        tLLClaimUserDB.setUserCode(cOperate);
        tLLClaimUserDB.getInfo();
        tLLClaimUserSchema = tLLClaimUserDB.getSchema();
        String AppGrade = tLLClaimUserSchema.getClaimPopedom();
        LLClaimUnderwriteSet tLLClaimUnderwriteSet = new LLClaimUnderwriteSet();
        LLClaimUWMainSet tLLClaimUWMainSet = new LLClaimUWMainSet();
        LLCaseCommon tLLCaseCommon = new LLCaseCommon();

        LLCaseOpTimeSet tLLCaseOpTimeSet = new LLCaseOpTimeSet();
        String tRgtNo = null;
        int count = mLLCaseSet.size();
        
        for (int j = 1; j <= count; j++) {
            LLCaseSchema tLLCaseSchema = new LLCaseSchema();
            tLLCaseSchema.setCaseNo(mLLCaseSet.get(j).getCaseNo());
            tLLCaseDB.setCaseNo(tLLCaseSchema.getCaseNo());
            tLLCaseDB.getInfo();
            tLLCaseSchema = tLLCaseDB.getSchema();
            
            LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
            tRgtNo = tLLCaseSchema.getRgtNo();   
            String mRgtno = (String) tRgtNo.subSequence(0, 1) ;
            //#2246 审批审定页面的案件回退功能调整需求--前期上线功能有误，修正#740
            if(mRgtno.equals("C")){
            	tLLRegisterSchema.setRgtNo(mLLCaseSet.get(j).getCaseNo());
            	tLLRegisterDB.setRgtNo(tLLCaseSchema.getCaseNo());
                tLLRegisterDB.getInfo();
                tLLRegisterSchema = tLLRegisterDB.getSchema();
            }
            else if(mRgtno.equals("P")){
            	tLLRegisterSchema.setRgtNo(mLLCaseSet.get(j).getCaseNo());
            	tLLRegisterDB.setRgtNo(tLLCaseSchema.getRgtNo());
                tLLRegisterDB.getInfo();
                tLLRegisterSchema = tLLRegisterDB.getSchema();
            }

            if (!LLCaseCommon.checkHospCaseState(tLLCaseSchema.getCaseNo())) {
                CError.buildErr(this,
                                tLLCaseSchema.getCaseNo() + "医保通案件待确认，不能进行处理");
                return false;
            }
            String ApplyerType = "";
            String RgtState = tLLCaseSchema.getRgtState();
            if(!"".equals(tLLRegisterSchema.getApplyerType()) && tLLRegisterSchema.getApplyerType()!=null && 
            		!"null".equals(tLLRegisterSchema.getApplyerType())){
            	ApplyerType = tLLRegisterSchema.getApplyerType();
            }
            
            String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
            String CaseBackNo = PubFun1.CreateMaxNo("CaseBack", tLimit);
            LLCaseBackSchema mLLCaseBackSchema = new LLCaseBackSchema();
            mLLCaseBackSchema.setCaseBackNo(CaseBackNo);
            mLLCaseBackSchema.setRemark(mRemark);
            mLLCaseBackSchema.setCaseNo(tLLCaseSchema.getCaseNo());
            mLLCaseBackSchema.setRgtNo(tLLCaseSchema.getRgtNo());
            mLLCaseBackSchema.setBeforState(tLLCaseSchema.getRgtState());
            mLLCaseBackSchema.setAfterState(tLLCaseSchema.getRgtState());
            mLLCaseBackSchema.setOHandler(tLLCaseSchema.getHandler());
            mLLCaseBackSchema.setNHandler(cOperate);
            mLLCaseBackSchema.setMngCom(mGlobalInput.ManageCom);
            mLLCaseBackSchema.setOperator(mGlobalInput.Operator);
            mLLCaseBackSchema.setMakeDate(PubFun.getCurrentDate());
            mLLCaseBackSchema.setMakeTime(PubFun.getCurrentTime());
            mLLCaseBackSchema.setModifyDate(PubFun.getCurrentDate());
            mLLCaseBackSchema.setModifyTime(PubFun.getCurrentTime());
            System.out.print("mLLCaseBackSchema" +
                             mLLCaseBackSchema.getModifyDate());
            mLLCaseBackSchema.setBackType("9");
            mLLCaseBackSchema.setRemark("信箱处理-理赔人变更功能回退");
            mLLCaseBackSet.add(mLLCaseBackSchema);

            if (!(RgtState.equals("05") || RgtState.equals("10"))) {
                tLLCaseSchema.setHandler(cOperate);
            }
            tLLCaseSchema.setDealer(cOperate);

            String sql = "select comcode from llclaimuser where usercode='" +
                         cOperate + "'";
            String comname = tExeSQL.getOneValue(sql);

            if (tLLCaseSchema.getRgtState().equals("05")) {
                LLClaimUWMainSchema tLLClaimUWMainSchema = new
                        LLClaimUWMainSchema();
                LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new
                        LLClaimUnderwriteSchema();
                LLClaimUWMainDB mLLClaimUWMainDB = new LLClaimUWMainDB();
                LLClaimUWMainSet mLLClaimUWMainSet = new LLClaimUWMainSet();
                mLLClaimUWMainDB.setCaseNo(tLLCaseSchema.getCaseNo());
                mLLClaimUWMainSet = mLLClaimUWMainDB.query();
                tLLClaimUWMainSchema = mLLClaimUWMainSet.get(1);
                tLLClaimUWMainSchema.setAppClmUWer(cOperate);
                tLLClaimUWMainSchema.setAppGrade(AppGrade);

                LLClaimUnderwriteDB tLLClaimUnderwriteDB = new
                        LLClaimUnderwriteDB();
                LLClaimUnderwriteSet mLLClaimUnderwriteSet = new
                        LLClaimUnderwriteSet();
                tLLClaimUnderwriteDB.setCaseNo(tLLCaseSchema.getCaseNo());
                mLLClaimUnderwriteSet = tLLClaimUnderwriteDB.query();
                tLLClaimUnderwriteSchema = mLLClaimUnderwriteSet.get(1);
                tLLClaimUnderwriteSchema.setAppClmUWer(cOperate);

                tLLClaimUnderwriteSet.add(tLLClaimUnderwriteSchema);
                tLLClaimUWMainSet.add(tLLClaimUWMainSchema);
            }
            if (RgtState.equals("10")) {
                LLClaimUWMainSchema tLLClaimUWMainSchema = new
                        LLClaimUWMainSchema();
                LLClaimUWMainDB mLLClaimUWMainDB = new LLClaimUWMainDB();
                LLClaimUWMainSet mLLClaimUWMainSet = new LLClaimUWMainSet();
                mLLClaimUWMainDB.setCaseNo(tLLCaseSchema.getCaseNo());
                mLLClaimUWMainSet = mLLClaimUWMainDB.query();
                tLLClaimUWMainSchema = mLLClaimUWMainSet.get(1);
                tLLClaimUWMainSchema.setAppClmUWer(cOperate);
                tLLClaimUWMainSchema.setAppGrade(AppGrade);
                tLLClaimUWMainSet.add(tLLClaimUWMainSchema);
            }
            if (!RgtState.equals("02")&&!ApplyerType.equals("2")&&!ApplyerType.equals("5")) {
                LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();

                String strSQL0 =
                        "select  sequance from LLCaseOpTime where caseno='" +
                        tLLCaseSchema.getCaseNo() + "' and rgtstate='" +
                        tLLCaseSchema.getRgtState() +
                        "' order by sequance desc";
                ExeSQL texesql = new ExeSQL();
                SSRS tssrs = new SSRS();
                tssrs = texesql.execSQL(strSQL0);
                if (tssrs == null || tssrs.getMaxRow() <= 0) {
                    CError tError = new CError();
                    tError.moduleName = "Casetransfetrbl";
                    tError.functionName = "prepareOutputData";
                    tError.errorMessage = "没有查询到相应的案件轨迹";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                String tSequance = tssrs.GetText(1, 1);
                System.out.println("案件轨迹表序号：" +
                                   tSequance);
                int _tSequance = Integer.parseInt(tSequance);
                _tSequance = _tSequance + 1;

                tLLCaseOpTimeSchema.setCaseNo(tLLCaseSchema.getCaseNo());
                tLLCaseOpTimeSchema.setRgtState(tLLCaseSchema.getRgtState());
                tLLCaseOpTimeSchema.setOperator(cOperate);

                tLLCaseOpTimeSchema.setManageCom(comname);
                tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(
                        tLLCaseOpTimeSchema);
                tLLCaseOpTimeSchema.setSequance(_tSequance);
                tLLCaseOpTimeSet.add(tLLCaseOpTimeSchema);
            }
            if (comname.length() > 2) {
                tLLCaseSchema.setCaseProp("06");
            }

            tLLCaseSet.add(tLLCaseSchema);

        }

        map.put(tLLCaseSet, "UPDATE");
        map.put(mLLCaseBackSet, "INSERT");
        if (tLLCaseOpTimeSet != null &&
            tLLCaseOpTimeSet.size() != 0) {
            map.put(tLLCaseOpTimeSet, "INSERT");
        }
        if (tLLClaimUWMainSet != null &&
            tLLClaimUWMainSet.size() != 0) {
            map.put(tLLClaimUWMainSet, "UPDATE");
        }
        if (tLLClaimUnderwriteSet != null &&
            tLLClaimUnderwriteSet.size() != 0) {
            map.put(tLLClaimUnderwriteSet, "UPDATE");
        }

        this.mResult.add(map);
        return true;
    }


}
