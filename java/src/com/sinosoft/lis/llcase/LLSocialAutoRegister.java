package com.sinosoft.lis.llcase;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.lis.yibaotong.ServiceInterface;
import com.sinosoft.lis.schema.*;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.llcase.GetAccAmnt;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.llcase.ClaimCalBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.llcase.LLUWCheckBL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.utility.ExeSQL;
import java.text.DecimalFormat;
import java.io.FileInputStream;

/**
 * <p>Title: 自动立案理算</p>
 *
 * <p>Description: 生成案件至理算状态</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLSocialAutoRegister implements ServiceInterface {
    public LLSocialAutoRegister() {
    }

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    public VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap mMMap = new MMap();

    //传入报文
    private Document mInXmlDoc;
    //报文类型
    private String mDocType = "";
    //处理标志
    private boolean mDealState = true;

    //返回类型代码
    private String mResponseCode = "1";
    //请求类型
    private String mRequestType = "";
    //错误描述
    private String mErrorMessage = "";
    //交互编码
    private String mTransactionNum = "";
    
    
    //报文批次部分
    private Element mRGTList;
    //报文案件部分
    private Element mLLCaseList;
    //报文账单费用部分
    private Element mLLFeeMainList;
    //报文疾病部分
    private Element mDiseaseList;
    //报文重疾部分
    private Element mSeriousList;
    //报文手术部分
    private Element mOperationList;
    //报文理赔明细部分
    private Element mPayList;
    
    //批次流水号
    private String mSocialRgtNo;
    
    //理赔信息序号
    private String mTransacTionNo;
    
    //保单号
    private String mGrpContNo;
    
    //案件流水号
    private String mSocialCaseNo;
    
    
    
    public Document service(Document pInXmlDoc) {
        //兄弟别怨我，我也是被逼的，时间太紧，都写到一个类里了
        System.out.println("LLSocialAutoRegister--service");
        mInXmlDoc = pInXmlDoc;

        try {
            if (!getInputData(mInXmlDoc)) {
                mDealState = false;
            } else {
                if (!checkData()) {
                    mDealState = false;
                } else {
                    if (!dealData()) {
                    	//cancelCase();
                        mDealState = false;
                    }
                }
            }
        } catch (Exception ex) {

            System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
        } finally {
        		return pInXmlDoc;
        }
    }

    /**
     * 校验基本数据
     * @return boolean
     */
    private boolean checkData() throws Exception {
        System.out.println("LLSocialAutoRegister--checkData");
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
        }

        if (!"PICCSOCIAL".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误");
            return false;
        }

        if (mTransactionNum == null || mTransactionNum == "" ||
            mTransactionNum.length() < 9) {
            buildError("checkData()", "【交互编码】编码错误");
            return false;
        }

        /*if (!mRequestType.equals(mTransactionNum.substring(0, 2))) {
            buildError("checkData()", "【交互编码】与【请求类型】匹配错误");
            return false;
        }

        mTranHospCode = mTransactionNum.substring(2, 9);
        
        LLHospComRightDB tLLHospComRightDB = new LLHospComRightDB();
        tLLHospComRightDB.setHospitCode(mTranHospCode);
        tLLHospComRightDB.setRequestType(mRequestType);
        tLLHospComRightDB.setState("0");
        LLHospComRightSet tLLHospComRightSet = new LLHospComRightSet();
        tLLHospComRightSet = tLLHospComRightDB.query();

        if (tLLHospComRightSet.size() < 1) {
            buildError("checkData()", "该医院无通信权限");
            return false;
        }

        mLDHospitalDB.setHospitCode(mTranHospCode);
        if (!mLDHospitalDB.getInfo()) {
            buildError("checkData()", "医院查询失败");
            return false;
        }
        mManageCom = mLDHospitalDB.getManageCom();
        if ("".equals(mManageCom) || mManageCom.length() < 4) {
            buildError("checkData()", "医院所属机构错误");
            return false;
        }*/
        
        return true;
    }

    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
        System.out.println("LLSocialAutoRegister--getInputData");
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");

        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");

        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");

        System.out.println("LLSocialAutoRegister--TransactionNum:" +
                           mTransactionNum);

        //获取BODY部分内容
        Element tBodyData = tRootData.getChild("BODY");
        mRGTList = tBodyData.getChild("RGTLIST");
        mLLCaseList = tBodyData.getChild("LLCASELIST");
        return true;
    }

    /**
     * 处理报文
     * @return boolean
     */
    private boolean dealData() throws Exception {
        System.out.println("LLSocialAutoRegister--dealData");
        //社保批次信息表	LLRgtSocial相关		
        if (!RgtSocial()) {
            return false;
        }
        //社保案件信息表	LLCaseSocial相关
        if (!CaseSocial()) {
            return false;
        }

        return true;
    }

/*
    *//**
     * 生成返回的报文信息
     * @return Document
     *//*
    private Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState && "".equals(mOldCaseNo)) {
            return createFalseXML();
        } else {
        	try {
        	if (!"".equals(mOldCaseNo)) {
        		return createResultXML(mOldCaseNo);
        	} else {
        		return createResultXML(mCaseNo);
        	}
        	} catch (Exception ex) {
        		mResponseCode = "0";
                buildError("createXML()", "生成返回报文错误");
        		return createFalseXML();
        	}
        }
    }

    *//**
     * 生成错误信息报文
     * @return Document
     *//*
    private Document createFalseXML() {
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }
*/
    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLSocialAutoRegister";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }


    /**
     * 解析校验社保批次部分
     * @return boolean
     */
//  社保批次信息	LLRgtSocial		
    private boolean RgtSocial() {
    	System.out.println("RgtSocial--start");
    	List tRGTList = new ArrayList();
    	tRGTList = (List) mRGTList.getChildren();
    	for (int i = 0; i < tRGTList.size(); i++) {
            Element tRGTdata = (Element) tRGTList.get(i);
            
            mGrpContNo = tRGTdata.getChildText("GRPCONTNO");
            if (mGrpContNo == null || "".equals(mGrpContNo)) {
                buildError("RgtSocial", "【保单号】的值不能为空");
                return false;
            }
            
            String mAppPeoples = tRGTdata.getChildText("APPPEOPLES");
            if (mAppPeoples == null || "".equals(mAppPeoples)) {
                buildError("RgtSocial", "【申请人数】的值不能为空");
                return false;
            }
            
            String mTogetherFlag = tRGTdata.getChildText("TOGETHERFLAG");
            if (mTogetherFlag == null || "".equals(mTogetherFlag)) {
                buildError("RgtSocial", "【给付方式】的值不能为空");
                return false;
            }
            
            String mCaseGetMode = tRGTdata.getChildText("CASEGETMODE");
            if (mCaseGetMode == null || "".equals(mCaseGetMode)) {
                buildError("RgtSocial", "【保险金领取方式】的值不能为空");
                return false;
            }
            
            String mAppAmnt = tRGTdata.getChildText("APPAMNT");
            if (mAppAmnt == null || "".equals(mAppAmnt)) {
                buildError("RgtSocial", "【申请金额】的值不能为空");
                return false;
            }
            String mBankCode = tRGTdata.getChildText("BANKCODE");
            if (mBankCode == null || "".equals(mBankCode)) {
                buildError("RgtSocial", "【银行编码】的值不能为空");
                return false;
            }
            String mAccName = tRGTdata.getChildText("ACCNAME");
            if (mAccName == null || "".equals(mAccName)) {
                buildError("RgtSocial", "【银行编码】的值不能为空");
                return false;
            }
            String mBankAccNo = tRGTdata.getChildText("BANKACCNO");
            if (mBankAccNo == null || "".equals(mBankAccNo)) {
                buildError("RgtSocial", "【银行帐号】的值不能为空");
                return false;
            }
            
            System.out.println(mGrpContNo
            		+mAppPeoples
            		+mTogetherFlag
            		+mCaseGetMode
            		+mAppAmnt
            		+mBankCode
            		+mAccName
            		+mBankAccNo);
            
            
            
    	}
    	return true;
    }
    //社保案件信息相关	LLCaseSocial
    private boolean CaseSocial() {
    	System.out.println("CaseSocial--start");
    	List tLLCaseList = new ArrayList();
    	tLLCaseList = (List) mLLCaseList.getChildren();
    	for (int i = 0; i < tLLCaseList.size(); i++) {
            Element tLLCasedata = (Element) tLLCaseList.get(i);
            
            String tCustomerNo = tLLCasedata.getChildText("CUSTOMERNO");
            if (tCustomerNo == null || "".equals(tCustomerNo)) {
                buildError("CaseSocial", "【出险人客户号】的值不能为空");
                return false;
            }
            System.out.println(tCustomerNo);
            String tCustomerName = tLLCasedata.getChildText("CUSTOMERNAME");
            if (tCustomerName == null || "".equals(tCustomerName)) {
            	buildError("CaseSocial", "【出险人名称】的值不能为空");
            	return false;
            }
            //证件类型固定为0-身份证
            String tIDType ="0";
            
            String tIDNo = tLLCasedata.getChildText("IDNO");
            if (tIDNo == null || "".equals(tIDNo)) {
            	buildError("CaseSocial", "【证件号码】的值不能为空");
            	return false;
            }
            
            String tSex = tLLCasedata.getChildText("SEX");
            if (tSex == null || "".equals(tSex)) {
            	buildError("CaseSocial", "【出险人性别】的值不能为空");
            	return false;
            }
            
            String tCustBirthday = tLLCasedata.getChildText("CUSTBIRTHDAY");
            if (tCustBirthday == null || "".equals(tCustBirthday)) {
            	buildError("CaseSocial", "【出险人生日】的值不能为空");
            	return false;
            }
            
            String tReasonCode = tLLCasedata.getChildText("REASONCODE");
            if (tReasonCode == null || "".equals(tReasonCode)) {
            	buildError("CaseSocial", "【原因代码】的值不能为空");
            	return false;
            }
            
            String tAccDate = tLLCasedata.getChildText("ACCDATE");
            if (tAccDate == null || "".equals(tAccDate)) {
            	buildError("CaseSocial", "【发生日期】的值不能为空");
            	return false;
            }
            
            String tAccPlace = tLLCasedata.getChildText("ACCPLACE");
            if (tAccPlace == null || "".equals(tAccPlace)) {
            	buildError("CaseSocial", "【事故地点】的值不能为空");
            	return false;
            }
            
            String tInHospitalDate = tLLCasedata.getChildText("HOSPSTARTDATE");
            if (tInHospitalDate == null || "".equals(tInHospitalDate)) {
            	buildError("CaseSocial", "【入院日期】的值不能为空");
            	return false;
            }
            
            String tOutHospitalDate = tLLCasedata.getChildText("HOSPENDDATE");
            if (tOutHospitalDate == null || "".equals(tOutHospitalDate)) {
            	buildError("CaseSocial", "【出院日期】的值不能为空");
            	return false;
            }
            
            String tAccDesc = tLLCasedata.getChildText("ACCDESC");
            if (tAccDesc == null || "".equals(tAccDesc)) {
            	buildError("CaseSocial", "【事故描述】的值不能为空");
            	return false;
            }
            
            String tAccidentType = tLLCasedata.getChildText("ACCIDENTTYPE");
            if (tAccidentType == null || "".equals(tAccidentType)) {
            	buildError("CaseSocial", "【事故类型】的值不能为空");
            	return false;
            }
            
            String tCaseGetMode = tLLCasedata.getChildText("CASEGETMODE");
            if (tCaseGetMode == null || "".equals(tCaseGetMode)) {
            	buildError("CaseSocial", "【保险金领取方式】的值不能为空");
            	return false;
            }
            
            String tBankCode = tLLCasedata.getChildText("BANKCODE");
            if (tBankCode == null || "".equals(tBankCode)) {
            	buildError("CaseSocial", "【银行编码】的值不能为空");
            	return false;
            }
            String tAccName = tLLCasedata.getChildText("ACCNAME");
            if (tAccName == null || "".equals(tAccName)) {
            	buildError("CaseSocial", "【银行帐户名】的值不能为空");
            	return false;
            }
            
            String tBankAccNo = tLLCasedata.getChildText("BANKACCNO");
            if (tBankAccNo == null || "".equals(tBankAccNo)) {
            	buildError("CaseSocial", "【银行帐号】的值不能为空");
            	return false;
            }
            
            String tMobilePhone = tLLCasedata.getChildText("MOBILEPHONE");
            if (tMobilePhone == null || "".equals(tMobilePhone)) {
            	buildError("CaseSocial", "【被保人手机号】的值不能为空");
            	return false;
            }
            
            System.out.println(tCustomerNo
            		+tCustomerName
            		+tIDType
            		+tIDNo
            		+tSex
            		+tCustBirthday
            		+tReasonCode
            		+tAccDate
            		+tAccPlace
            		+tInHospitalDate
            		+tOutHospitalDate
            		+tAccDesc
            		+tAccidentType
            		+tCaseGetMode
            		+tBankCode
            		+tAccName
            		+tBankAccNo
            		+tMobilePhone
            		);
            
            mLLFeeMainList = tLLCasedata.getChild("LLFEEMAINLIST");
            if (!FeeMain()) {
                return false;
            }
            
            //出险内容
            mDiseaseList = tLLCasedata.getChild("DISEASELIST");
            mSeriousList= tLLCasedata.getChild("SERIOUSLIST");
            mOperationList = tLLCasedata.getChild("OPERATIONLIST");
            if (!AccDetailSocial()) {
                return false;
            }

            //理赔明细
            mPayList = tLLCasedata.getChild("PAYLIST");
            if (!ClaimDetailSocial()) {
                return false;
            }

            
            
            
    	}
    	return false;
    }
   
    private boolean FeeMain() {
    	System.out.println("PrescripSocial--start");
    	List tLLFeeMainList = new ArrayList();
    	tLLFeeMainList = (List) mLLFeeMainList.getChildren();
    	for (int i = 0; i < tLLFeeMainList.size(); i++) {
            Element tLLFeeMainData = (Element) tLLFeeMainList.get(i);
            
            String tHospitalCode = tLLFeeMainData.getChildText("HOSPITALNAME");
            if (tHospitalCode == null || "".equals(tHospitalCode)) {
            	buildError("RgtSocial", "【医院代码】的值不能为空");
            	return false;
            }
            System.out.println(tHospitalCode);
    	}
        return true;
    }
    //社保出险内容信息相关	LLAccDetailSocial
    private boolean AccDetailSocial() {
    	System.out.println("AccDetailSocial--start");
    	
        return true;
    }
    //社保费用明细相关 	LLFeeItemSocial
    private boolean FeeItemSocial() {
    	System.out.println("FeeItemSocial--start");
    	
        return true;
    }
    //社保理赔明细相关	LLClaimDetailSocial
    private boolean ClaimDetailSocial() {
        return true;
    }
    


    public static void main(String[] args) {
        Document tInXmlDoc;
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream(
                    "E:/ceshi.xml"), "GBK");

            LLSocialAutoRegister tBusinessDeal = new LLSocialAutoRegister();
            tBusinessDeal.service(tInXmlDoc);
          //  Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
//            JdomUtil.print(tInXmlDoc.getRootElement());
            //JdomUtil.print(tOutXmlDoc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    }
}
