package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.GlobalInput;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:案件－立案－分案疾病伤残明细信息的数据接收处理类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @version 1.0
 */
public class LLCaseReturnUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public LLCaseReturnUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    LLCaseReturnBL tLLCaseReturnBL = new LLCaseReturnBL();
    System.out.println("---UI BEGIN---");
    if (tLLCaseReturnBL.submitData(cInputData,mOperate) == false)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLCaseReturnBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "CaseInfoUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据保存失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
      mResult = tLLCaseReturnBL.getResult();
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public static void main(String[] args)
  {
      LLCaseSchema tLLCaseSchema   = new LLCaseSchema();
      LLCaseBackSchema tLLCaseBackSchema = new LLCaseBackSchema();
      LLCaseReturnUI tLLCaseReturnUI   = new LLCaseReturnUI();
//输出参数
      GlobalInput tGlobalInput = new GlobalInput();
      tGlobalInput.Operator = "hcm002";
      tGlobalInput.ManageCom = "86";


      tLLCaseSchema.setCaseNo("C1100050515000002");
      tLLCaseSchema.setHandler("ocm002");
      tLLCaseSchema.setRgtState("05");

      tLLCaseBackSchema.setCaseNo("C1100050515000002");
      tLLCaseBackSchema.setBeforState("05");
      tLLCaseBackSchema.setAfterState("04");
      //tLLCaseBackSchema.setOHandler();
      tLLCaseBackSchema.setNHandler("ocm002");
      tLLCaseBackSchema.setReason("1");
      tLLCaseBackSchema.setRemark("remark");

// 准备传输数据 VData
      VData tVData = new VData();
      tVData.add(tLLCaseSchema);
      tVData.add(tLLCaseBackSchema);
      tVData.add(tGlobalInput);
      tLLCaseReturnUI.submitData(tVData,"INSERT||MAIN");

  }
}
