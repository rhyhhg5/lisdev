package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.db.LLSocialClaimUserDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDSpotUWRateSchema;
import com.sinosoft.lis.schema.LLSocialClaimUserSchema;
import com.sinosoft.lis.vschema.LDUserSet;
import com.sinosoft.lis.vschema.LLSocialClaimUserSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 立案业务逻辑保存处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @version 1.0
 */
public class LLSocialClaimUserOperateBL
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData = new VData();
  private VData mResult = new VData();
  private String mOperate;
  private GlobalInput mG = new GlobalInput();
  private MMap mMMap = new MMap();

  private LLSocialClaimUserSchema mLLSocialClaimUserSchema = new LLSocialClaimUserSchema();
  private LDSpotUWRateSchema mLDSpotUWRateSchema = new LDSpotUWRateSchema();
  
  public  LLSocialClaimUserOperateBL()
  {
	  
  }
  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData,String cOperate)
  {
    cInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;
    // 數據信息獲取
    if (!getInputData(cInputData))
    {
        return false;
    }
    if(mOperate.equals("INSERT")||mOperate.equals("UPDATE")||mOperate.equals("DELETE"))
    {
      if(!checkdata())
      {
         return false;  
      }

      if(!dealData())
      {
    	 return false;  
      }
        
    }
    if(!prepareOutputData())
    {
    	
    	return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, ""))
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LLSocialClaimUserOperateBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mInputData = null;
    return true;
  }


  private boolean getInputData(VData cInputData)
  {

      mLLSocialClaimUserSchema.setSchema((LLSocialClaimUserSchema)cInputData.getObjectByObjectName("LLSocialClaimUserSchema",0));
      mLDSpotUWRateSchema.setSchema((LDSpotUWRateSchema)cInputData.getObjectByObjectName("LDSpotUWRateSchema",0));
      mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
      return true;
  }

  /**
   * Name :checkdate
   * Function :判断数据逻辑上的正确性
   * 1:判断用户代码和用户姓名的关联是否正确
   * 2:校验用户和录入的管理机构是否匹配
   */
  private boolean checkdata()
  {

    // 用户校验
    String UserName_sql = " select * from LDUser where UserCode = '"
                        +mLLSocialClaimUserSchema.getUserCode().trim()+"' and ComCode like '"
                        +mLLSocialClaimUserSchema.getComCode()+"%' ";

    System.out.println("UserName_sql"+UserName_sql);
    LDUserDB tLDUserDB = new LDUserDB();
    LDUserSet tLDUserSet = new LDUserSet();
    tLDUserSet.set(tLDUserDB.executeQuery(UserName_sql));
    System.out.println("tLDUserSet.size()="+tLDUserSet.size());

    if(tLDUserSet.size()==0)
    {
    	 CError tError = new CError();
         tError.moduleName = "LLSocialClaimUserOperateBL";
         tError.functionName = "checkData";
         tError.errorMessage ="在该管理机构为"+mLLSocialClaimUserSchema.getComCode()
                 +"下没用用户号码为"+mLLSocialClaimUserSchema.getUserCode()+"的用户";
         this.mErrors.addOneError(tError);
         return false;
    }
    if(!tLDUserSet.get(1).getUserName().trim().equals(mLLSocialClaimUserSchema.getUserName().trim()))
    {
    	CError tError = new CError();
        tError.moduleName = "LLSocialClaimUserOperateBL";
        tError.functionName = "checkData";
        tError.errorMessage ="您录入的用户代码和用户名称不匹配，系统中用户号码为"+mLLSocialClaimUserSchema.getUserCode()
                 +"的用户名称是"+tLDUserSet.get(1).getUserName()+"而您录入的用户名称是"+mLLSocialClaimUserSchema.getUserName();
        this.mErrors.addOneError(tError);
        return false;
    }
    
    // 校验操作人员
    
    String tMngCom = mLLSocialClaimUserSchema.getComCode();
    String tempComCode=tLDUserSet.get(1).getComCode();
    if(tMngCom.trim().length()<=2)
    {
     tMngCom = tMngCom+"00";
     tempComCode+="00";
    }

    if(!tempComCode.equals(tMngCom.trim()))
    {
    	 CError tError = new CError();
         tError.moduleName = "LLSocialClaimUserOperateBL";
         tError.functionName = "checkData";
         tError.errorMessage ="您录入的“用户管理机构”和“用户代码”不匹配，系统中用户号码为"+mLLSocialClaimUserSchema.getUserCode()
                 +"的用户管理机构是"+tLDUserSet.get(1).getComCode()+"而您录入的管理机构是"+mLLSocialClaimUserSchema.getComCode();
         this.mErrors.addOneError(tError);
         return false;
    }
    
    // 校验操作人员
    if(!checkOperator()) return false;
    
    String tUpUserCode = this.mLLSocialClaimUserSchema.getUpUserCode();
    if(tUpUserCode!=null&&!tUpUserCode.equals(""))
    {
    	LLSocialClaimUserDB tLLSocialClaimUserDB = new LLSocialClaimUserDB();
    	tLLSocialClaimUserDB.setUserCode(tUpUserCode);
    	if(!tLLSocialClaimUserDB.getInfo())
    	{
    		CError tError = new CError();
            tError.moduleName = "LLSocialClaimUserOperateBL";
            tError.functionName = "checkData";
            tError.errorMessage ="系统中尚无页面录入上级用户（编码："+tUpUserCode+"）信息，烦请进行信息核实！";
            this.mErrors.addOneError(tError);
            return false;
    	}
    	
//        //用户所属机构与上级用户所属机构相同
//    	String tComCode = tLLSocialClaimUserDB.getComCode();
//    	if(!tComCode.equals(this.mLLSocialClaimUserSchema.getComCode()))
//    	{
//    		CError tError = new CError();
//            tError.moduleName = "LLSocialClaimUserOperateBL";
//            tError.functionName = "checkData";
//            tError.errorMessage ="用户"+this.mLLSocialClaimUserSchema.getUpUserCode()+"所属机构"+mLLSocialClaimUserSchema.getComCode()+"与其上级用户"+tUpUserCode+"所属机构"+tComCode+"不一致,烦请重新录入！";
//            this.mErrors.addOneError(tError);
//            return false;
//    		
//    	}
        //下级用户社保业务结案金额不能高于上级
//    	System.out.println("上级用户社保业务结算金额为："+tLLSocialClaimUserDB.getSocialMoney());
//    	double up_SocialMoney = tLLSocialClaimUserDB.getSocialMoney();
//    	double SocialMoney = this.mLLSocialClaimUserSchema.getSocialMoney();
//    	if(SocialMoney>up_SocialMoney)
//    	{
//    		CError tError = new CError();
//            tError.moduleName = "LLSocialClaimUserOperateBL";
//            tError.functionName = "checkData";
//            tError.errorMessage ="用户"+mLLSocialClaimUserSchema.getUserCode()+"社保业务结案金额(金额："+SocialMoney+")高于其上级用户"+tLLSocialClaimUserDB.getUserCode()+"社保业务结案金额(金额："+up_SocialMoney+"),烦请重新录入！";
//            this.mErrors.addOneError(tError);
//            return false;
//    		
//    	}
    }
    //理赔用户修改为非有效状态时，校验是否有未处理完的案件，未完成的调查
    if((mOperate.equals("UPDATE")&&!this.mLLSocialClaimUserSchema.getStateFlag().equals("1"))||mOperate.equals("DELETE"))
    {
    	// 应还需加上社保保单条件控制  ********
    	String getCasenoSql=
    	" select caseno from llcase where handler='"+this.mLLSocialClaimUserSchema.getUserCode()+"' and endcasedate is null " 
		+" and rgtstate not in ('09','11','12','14') " 
		+" and exists(Select 1 From llregister where rgtno =llcase.rgtno and exists(Select 1 From LCGrpcont where grpcontno = llregister.rgtobjno and markettype not in ('1','9')) )" 
		+" union all " 
		+" select a.caseno from llcase a,LLClaimUWMain b where a.caseno=b.caseno " 
		+" and a.endcasedate is null and a.rgtstate in ('05','10') " 
		+" and exists(Select 1 From llregister where rgtno =a.rgtno and exists(Select 1 From LCGrpcont where grpcontno = llregister.rgtobjno and markettype not in ('1','9')) )"
		+" and b.AppClmUWer = '"+this.mLLSocialClaimUserSchema.getUserCode()+"' ";
    	System.out.println("未处理案件查询=="+getCasenoSql);
    	ExeSQL exesql2 = new ExeSQL();
    	SSRS tSSRS=exesql2.execSQL(getCasenoSql);
    	if(tSSRS.MaxRow>0)//有未处理完案件
    	{
    		CError tError = new CError();
            tError.moduleName = "LLSocialClaimUserOperateBL";
            tError.functionName = "checkData";
            tError.errorMessage = "用户"+mLLSocialClaimUserSchema.getUserCode()+"下还有未处理完的案件";
            this.mErrors.addOneError(tError);
    		return false;
    	}
    	/**
    	 *  调查案件部分暂时注释掉
    	 */
    	//修改用户状态、调查状态时校验是否存在未处理完的调查案件
    	if(!isSurveyCase())
    	{
    		return false;
    	}
    	if(!isPrepaidCase())
    	{
    		return false;
    	}
    	
    }else{
	    if((mOperate.equals("UPDATE")&&!this.mLLSocialClaimUserSchema.getPrepaidFlag().equals("1"))||mOperate.equals("DELETE"))
	    {
	    	if(!isPrepaidCase())
	    	{  
	    		return false;
	    	}
	    }
	    if((mOperate.equals("UPDATE")&&this.mLLSocialClaimUserSchema.getSurveyFlag().equals("0"))||mOperate.equals("DELETE"))
	    {
	    	if(!isSurveyCase())
	    	{
	    		return false;
	    	}
	    }
	    //针对在有效状态修改“预付赔款额度”的校验
//	    if(mOperate.equals("UPDATE")&&this.mLLSocialClaimUserSchema.getPrepaidFlag().equals("1")){
//	    	if(!testPrepaidCase())
//	    	{  
//	    		return false;
//	    	}
//	    }
    }
     tempComCode=null;//清除对象引用


    return true;

  }
  /**
   * 所有用户的预付赔款审批额度不能相同
   * @return
   */
//  private boolean testPrepaidCase(){
//	  //	校验预付额度
//		String tPrepaidMoney="select PrepaidMoney from llsocialclaimuser where stateflag='1' and PrepaidMoney is not null with ur";
//		ExeSQL tPrepaidMoneySQL=new ExeSQL();
//		SSRS tPrepaidMoneySSRS=tPrepaidMoneySQL.execSQL(tPrepaidMoney);
//		if(tPrepaidMoneySSRS.getMaxRow()>0)
//		{
//			double tMoney = mLLSocialClaimUserSchema.getPrepaidMoney();
//			for(int i=1;i<=tPrepaidMoneySSRS.getMaxRow();i++){
//				double tCheckMoney = Double.parseDouble(tPrepaidMoneySSRS.GetText(i, 1));
//				if(tCheckMoney == tMoney){
//					CError tError = new CError();
//		            tError.moduleName = "LLSocialClaimUserOperateBL";
//		            tError.functionName = "checkData";
//		            tError.errorMessage ="社保部用户下已经存在此审批额度，请重新录入";
//		            this.mErrors.addOneError(tError);
//		            return false;
//
//				}				
//			}
//		}
//	  return true;
//  }
  /**
   * 修改用户状态、预付状态时校验是否存在未处理完的预付案件
   * 
   * */
  private boolean isPrepaidCase()
  {
	    String tPrepaidUW="select PrepaidFlag,StateFlag from llsocialclaimuser where usercode='"+ mLLSocialClaimUserSchema.getUserCode() +"'";
		ExeSQL PreUWExeSQL=new ExeSQL();
		SSRS PreUWSSRS=PreUWExeSQL.execSQL(tPrepaidUW);
		if(PreUWSSRS.getMaxRow()<=0)
		{
			CError tError = new CError();
            tError.moduleName = "LLSocialClaimUserOperateBL";
            tError.functionName = "checkData";
            tError.errorMessage ="社保部用户"+mLLSocialClaimUserSchema.getUserCode()+"查询失败";
            this.mErrors.addOneError(tError);
            return false;
		}
		String tPrepaidFlag = PreUWSSRS.GetText(1, 1);
		if((tPrepaidFlag.equals("1") && !mLLSocialClaimUserSchema.getPrepaidFlag().equals("1"))
				|| mOperate.equals("DELETE"))
		{
			String tPrepaid="select 1 from llprepaidclaim a where Dealer='"+ mLLSocialClaimUserSchema.getUserCode() +"'"
						+" and RgtState in ('01','02','03')";									
			ExeSQL PreExeSQL=new ExeSQL();
			SSRS PreSSRS=PreExeSQL.execSQL(tPrepaid);
			if(PreSSRS.getMaxRow()>=1)
			{
				CError tError = new CError();
	            tError.moduleName = "LLSocialClaimUserOperateBL";
	            tError.functionName = "checkData";
	            tError.errorMessage ="预付赔款审批人"+mLLSocialClaimUserSchema.getUserCode()+"下存在未处理完的案件";
	            this.mErrors.addOneError(tError);
				return false;
			}
		}
		
		return true;
  }


  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealData()
  {
      System.out.println("dealData....");
      LLSocialClaimUserDB tLLSocialClaimUserDB = new LLSocialClaimUserDB();
      tLLSocialClaimUserDB.setUserCode(this.mLLSocialClaimUserSchema.getUserCode());
      if(tLLSocialClaimUserDB.getInfo())
      {
          if(mOperate.equals("INSERT")){
        	  CError tError = new CError();
              tError.moduleName = "LLSocialClaimUserOperateBL";
              tError.functionName = "checkData";
              tError.errorMessage ="系统中已有用户名为"+mLLSocialClaimUserSchema.getUserCode()+"的社保人員信息！";
              this.mErrors.addOneError(tError);
              return false;
          }
      }
      else{
          if(mOperate.equals("DELETE")||mOperate.equals("UPDATE")){
        	  CError tError = new CError();
              tError.moduleName = "LLSocialClaimUserOperateBL";
              tError.functionName = "checkData";
              tError.errorMessage ="系统中尚无该社保人員信息，无法进行该操作！";
              this.mErrors.addOneError(tError);
              return false;
          }
      }
    	  mLLSocialClaimUserSchema.setMakeDate(PubFun.getCurrentDate());
    	  mLLSocialClaimUserSchema.setMakeTime(PubFun.getCurrentTime());
    	  mLLSocialClaimUserSchema.setModifyDate(PubFun.getCurrentDate());
    	  mLLSocialClaimUserSchema.setModifyTime(PubFun.getCurrentTime());
    	  mLLSocialClaimUserSchema.setOperator(mG.Operator);
    	  mLLSocialClaimUserSchema.setManageCom(mG.ManageCom);
    	  mMMap.put(mLLSocialClaimUserSchema, mOperate);
    	  mMMap.put(mLDSpotUWRateSchema, mOperate);
      

         return true;
  }
  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData()
  {
    try {
     
      mInputData.clear();
      mInputData.add(mMMap);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LLSocialClaimUserOperateBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /*
   * 修改用户状态、调查状态时校验是否存在未处理完的调查案件
   * */
  private boolean isSurveyCase()
  {
	  ExeSQL tExeSQL=new ExeSQL();	 
	  String tSurveyFlag=tExeSQL.getOneValue("select SurveyFlag from LLClaimUser where usercode='"+this.mLLSocialClaimUserSchema.getUserCode()+"'");
	  
	  ExeSQL surExeSQL=new ExeSQL();
	  if((tSurveyFlag.trim().equals("1") && !mLLSocialClaimUserSchema.getStateFlag().equals("1"))
			  || mOperate.equals("DELETE"))//调查主管时
	  {
		  // 加上团险社保业务
			String getSurveynoSql="select a.surveyno from llInqapply a,llcase b,llsurvey c where a.otherno=b.caseno and b.caseno=c.otherno " 
					+" and a.surveyno=c.surveyno and  a.Dipatcher='"+mLLSocialClaimUserSchema.getUserCode()+"' and c.surveyflag <>'3' and b.rgtstate ='07' " 
					+" union all " 
					+" select a.surveyno from llsurvey a,llcase b where a.otherno=b.caseno and  a.confer='"+mLLSocialClaimUserSchema.getUserCode()+"' and a.surveyflag <>'3'"
					+" and b.rgtstate ='07' with ur";
			SSRS tSSRS2=surExeSQL.execSQL(getSurveynoSql);
			if(tSSRS2.MaxRow>0)//调查主管有未处理完的调查（包括调查未分配、调查未审核）时
			{
				CError tError = new CError();
	            tError.moduleName = "ClaimUserOperateBL";
	            tError.functionName = "checkData";
	            tError.errorMessage ="调查主管"+mLLSocialClaimUserSchema.getUserCode()+"下还有未处理完的调查";
	            this.mErrors.addOneError(tError);
	    		return false;
			}
			String getSurveynoSql2="select a.surveyno from llsurvey a,llinqfee b where a.surveyno = b.surveyno and a.confer='"+mLLSocialClaimUserSchema.getUserCode()+"'" 
					+" and b.uwstate <>'1' " 
					+"union all select a.surveyno from llsurvey a,llinqfee b,llinqapply c " 
					+" where a.surveyno=b.surveyno and a.surveyno=c.surveyno and a.confer<>c.Dipatcher " 
					+" and b.uwstate not in ('1','4') and c.Dipatcher='"+mLLSocialClaimUserSchema.getUserCode()+"' with ur";
			SSRS tSSRS3=surExeSQL.execSQL(getSurveynoSql2);
			if(tSSRS3.MaxRow>0)//调查主管有未处理完的调查（直接调查费包括本地调查费和异地调查费未审核）时
			{
				CError tError = new CError();
	            tError.moduleName = "ClaimUserOperateBL";
	            tError.functionName = "checkData";
	            tError.errorMessage ="调查主管"+mLLSocialClaimUserSchema.getUserCode()+"下还有未处理完的直接调查费审核";
	            this.mErrors.addOneError(tError);
	    		return false;
			}
		}
		else if((tSurveyFlag.trim().equals("2")&& !mLLSocialClaimUserSchema.getStateFlag().equals("2"))
				|| mOperate.equals("DELETE"))//调查员时
		{
			String getSurveynoSql2="select a.surveyno from llInqapply a,llcase b,llsurvey c where a.otherno=b.caseno and b.caseno=c.otherno " 
					+" and a.surveyno=c.surveyno and  a.inqper='"+mLLSocialClaimUserSchema.getUserCode()+"' and c.surveyflag <>'3' and b.rgtstate ='07' with ur";
			SSRS tSSRS3=surExeSQL.execSQL(getSurveynoSql2);
			if(tSSRS3.MaxRow>0)//调查员有未处理完的调查（包括调查未分配、调查未审核、直接调查费未审核）时
			{
				CError tError = new CError();
	            tError.moduleName = "ClaimUserOperateBL";
	            tError.functionName = "checkData";
	            tError.errorMessage ="调查员"+mLLSocialClaimUserSchema.getUserCode()+"下还有未处理完的调查";
	            this.mErrors.addOneError(tError);
	    		return false;
			}
		}
	  return true;
  }
  public VData getResult()
  {
    return mResult;
  }
  
  /**
   * 1、只能在用户状态是有效且社保业务理赔人员权限中“参加案件分配”为“是”的情况下才能配置其他参与案件分配的社保理赔人员；
     2、只有在用户状态有效且社保业务理赔人员权限中“参加案件分配”为“否”的情况下才能配置其他参与社保业务的运营部理赔人员；
     3、对于非总公司社保人，只能配置其本级及下级机构
   * @return
   */
  private boolean checkOperator()
  {
	  String tOperator = this.mG.Operator;
	  String tHandleFlag = this.mLLSocialClaimUserSchema.getHandleFlag();
	  String sql = "";
	  // 0-否 1-是
	  if(tHandleFlag!=null&&"0".equals(tHandleFlag))
	  {
		  LLSocialClaimUserDB tLLSocialClaimUserDB = new LLSocialClaimUserDB();
		  sql ="Select * From LLSocialClaimUser where UserCode = '"+tOperator+"' and StateFlag = '1' and HandleFlag = '0'";
		  LLSocialClaimUserSet tLLSocialClaimUserSet  = new LLSocialClaimUserSet();
		  tLLSocialClaimUserSet=tLLSocialClaimUserDB.executeQuery(sql);
		  
		  if(tLLSocialClaimUserSet.size()>0)
		  {
			  
		  }else{
				CError tError = new CError();
	            tError.moduleName = "LLSocialClaimUserOperateBL";
	            tError.functionName = "checkData";
	            tError.errorMessage ="操作员"+tOperator+"由于人员状态或者参加案件分配权限问题不能配制用户状态为有效且参加案件分配为否的社保理赔用户！";
	            this.mErrors.addOneError(tError);
	    		return false;	
			  
		  }
		  String tOper_MgC = tLLSocialClaimUserSet.get(1).getComCode();
		  if(!"86".equals(tOper_MgC))
		  {
			  int tOper_MgC_length = tOper_MgC.length();
			  int tUser_MgC_length = this.mLLSocialClaimUserSchema.getComCode().length();
			  if(tOper_MgC_length>tUser_MgC_length)
			  {
				  CError tError = new CError();
		          tError.moduleName = "LLSocialClaimUserOperateBL";
		          tError.functionName = "checkData";
		          tError.errorMessage ="对于非总公司社保人，只能配置其本级及下级机构！";
		          this.mErrors.addOneError(tError);
		    	  return false;
				  
			  }  
		  }
		  
	  }else{
		  LLSocialClaimUserDB tLLSocialClaimUserDB = new LLSocialClaimUserDB();
		  sql ="Select * From LLSocialClaimUser where UserCode = '"+tOperator+"' and StateFlag = '1' and HandleFlag = '1'";
		  LLSocialClaimUserSet tLLSocialClaimUserSet  = new LLSocialClaimUserSet();
		  tLLSocialClaimUserSet=tLLSocialClaimUserDB.executeQuery(sql);		  
		  if(tLLSocialClaimUserSet.size()>0)
		  {
			  
		  }else{
				CError tError = new CError();
	            tError.moduleName = "LLSocialClaimUserOperateBL";
	            tError.functionName = "checkData";
	            tError.errorMessage ="操作员"+tOperator+"由于人员状态或者参加案件分配权限问题不能配制用户状态为有效且参加案件分配为是的社保理赔用户！";
	            this.mErrors.addOneError(tError);
	    		return false;	
			  
		  }
		  String tOper_MgC = tLLSocialClaimUserSet.get(1).getComCode();
		  if(!"86".equals(tOper_MgC))
		  {
			  int tOper_MgC_length = tOper_MgC.length();
			  int tUser_MgC_length = this.mLLSocialClaimUserSchema.getComCode().length();
			  if(tOper_MgC_length>tUser_MgC_length)
			  {
				  CError tError = new CError();
		          tError.moduleName = "LLSocialClaimUserOperateBL";
		          tError.functionName = "checkData";
		          tError.errorMessage ="对于非总公司社保人，只能配置其本级及下级机构！";
		          this.mErrors.addOneError(tError);
		    	  return false;				  
			  }  			  
		  }
	  }
	  // 对于非总公司社保人，只能配置其本级及下级机构
	  
	  return true;
  }
  
  public static void main(String[] args)
  {
	  LLSocialClaimUserSchema tLLSocialClaimUserSchema = new LLSocialClaimUserSchema();
	  tLLSocialClaimUserSchema.setMakeTime(PubFun.getCurrentTime());
	  System.out.println("maketime--->>"+tLLSocialClaimUserSchema.getMakeTime());
  }
}
