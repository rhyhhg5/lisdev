package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 理赔-审批审定业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author：zhangtao
 * @version：1.0
 * @CreateDate：2005-02-25
 */
public class ClaimUnderwriteUI{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();

  public ClaimUnderwriteUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData, String cOperate){
    //将操作数据拷贝到本类中
    mInputData = (VData) cInputData.clone();
    ClaimUnderwriteBL tClaimUnderwriteBL = new ClaimUnderwriteBL();
    if (!tClaimUnderwriteBL.submitData(mInputData, cOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tClaimUnderwriteBL.mErrors);
      mResult.clear();
      return false;
    }
    else {
        mResult = tClaimUnderwriteBL.getResult();
    }
    return true;
  }

  public VData getResult() {
  	return mResult;
  }

  public static void main(String[] args) {
    GlobalInput tGlobalInput = new GlobalInput();
    tGlobalInput.Operator = "hcm002";
    tGlobalInput.ManageCom = "86";

    LLClaimUWMainSchema tLLClaimUWMainSchema = new LLClaimUWMainSchema();
    tLLClaimUWMainSchema.setRgtNo("P1100050928000001");
    tLLClaimUWMainSchema.setCaseNo("C1100050928000024");
    tLLClaimUWMainSchema.setClmNo("52000000109");
    tLLClaimUWMainSchema.setcheckDecision1("1");
    tLLClaimUWMainSchema.setRemark1("shen pi remark for test");
    tLLClaimUWMainSchema.setcheckDecision2("1");
    tLLClaimUWMainSchema.setRemark2("shen ding remark for test");

    VData aVData = new VData();
    aVData.add(tGlobalInput);
    aVData.add(tLLClaimUWMainSchema);

    ClaimUnderwriteUI tClaimUnderwriteUI = new ClaimUnderwriteUI();
    tClaimUnderwriteUI.submitData(aVData, "APPROVE|SP");
  }

}
