/*
 * @(#)ICaseCureBL.java	2005-02-20
 * Copyright 2005 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.llcase;

import java.lang.reflect.Method;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/*
 * <p>Title: Web业务系统</p>
 * <p>ClassName:ICaseCureBL </p>
 * <p>Description: 理赔案件-账单录入 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft </p>
 * @author：Xx
 * @version：1.0
 * @CreateDate：2005-02-20
 */
public class ICaseCureBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    private VData mResult = new VData();
    private MMap map = new MMap();
    private String mOperate = "";
    private String MngCom = "86110000";
    private String aDate = "";
    private String aTime = "";
    private String backMessage = "";

    //全局数据
    private GlobalInput mG = new GlobalInput();
    //账单信息
    private LLFeeMainSchema mFeeMainSchema = new LLFeeMainSchema();
    //账单费用明细
    private LLCaseReceiptSet mReceiptSet = new LLCaseReceiptSet();
    private LLFeeOtherItemSet mFeeOtherItemSet = new LLFeeOtherItemSet();
    private LLSecurityReceiptSchema mSeReceiptSchema = new
            LLSecurityReceiptSchema();
    private LLSecuDetailSet mLLSecuDetailSet = new LLSecuDetailSet();
    private String[] mfieldvalue = new String[8];

    public ICaseCureBL() {
    }

    public static void main(String[] args) {
        LLFeeMainSchema testFeeMainSchema = new LLFeeMainSchema();
        LLSecurityReceiptSchema tsLLSecurityReceiptSchema = new
                LLSecurityReceiptSchema();
        testFeeMainSchema.setMainFeeNo("86000000037719");
        testFeeMainSchema.setCaseNo("C1100070321000001");
        testFeeMainSchema.setRgtNo("C1100070321000001 ");
        testFeeMainSchema.setCustomerNo("000009515");
        testFeeMainSchema.setCustomerName("牛秀花");
        testFeeMainSchema.setCustomerSex("1");
        testFeeMainSchema.setHospitalCode("1101002");
        testFeeMainSchema.setHospitalName("中国医学科学院北京协和医院");
        testFeeMainSchema.setReceiptNo("654999999");
        testFeeMainSchema.setFeeType("1");
        testFeeMainSchema.setFeeAtti("1");
        testFeeMainSchema.setFeeAffixType("0");
        testFeeMainSchema.setFeeDate("2006-11-27");
        testFeeMainSchema.setHospStartDate("2006-11-24");
        testFeeMainSchema.setHospEndDate("2006-11-27");
        testFeeMainSchema.setRealHospDate("3");
        testFeeMainSchema.setInsuredStat("2");
        testFeeMainSchema.setHosGrade("31");
        testFeeMainSchema.setAge(74);
        testFeeMainSchema.setMngCom("8611");
        LLCaseReceiptSet testReceiptSet = new LLCaseReceiptSet();
        LLCaseReceiptSchema testReceiptSchema = null;

//        testReceiptSchema = new LLCaseReceiptSchema();
//        testReceiptSchema.setFeeItemCode("201");
//        testReceiptSchema.setFeeItemName("西药费");
//        testReceiptSchema.setFee(29843.23);
//        testReceiptSet.add(testReceiptSchema);

        LLFeeOtherItemSet testOtherItemSet = new LLFeeOtherItemSet();
        LLFeeOtherItemSchema tOtherItemSchema = null;
//        tOtherItemSchema = new LLFeeOtherItemSchema();
//        tOtherItemSchema.setItemCode("BJ301");
//        tOtherItemSchema.setAvliReason("FeeInSecu");
//        tOtherItemSchema.setDrugName("医疗保险范围内金额");
//        tOtherItemSchema.setFeeMoney(22645.11);
//        testOtherItemSet.add(tOtherItemSchema);

//        tOtherItemSchema = new LLFeeOtherItemSchema();
//        tOtherItemSchema.setItemCode("BJ302");
//        tOtherItemSchema.setAvliReason("FeeOutSecu");
//        tOtherItemSchema.setDrugName("医疗保险范围外金额");
//        tOtherItemSchema.setFeeMoney(7198.12);
//        testOtherItemSet.add(tOtherItemSchema);
//
//        tOtherItemSchema = new LLFeeOtherItemSchema();
//        tOtherItemSchema.setItemCode("BJ304");
//        tOtherItemSchema.setAvliReason("SupInHosFee");
//        tOtherItemSchema.setDrugName("大额医疗互助支付金额");
//        tOtherItemSchema.setFeeMoney(4862.64);
//        testOtherItemSet.add(tOtherItemSchema);
//
        tOtherItemSchema = new LLFeeOtherItemSchema();
        tOtherItemSchema.setItemCode("BJ305");
        tOtherItemSchema.setAvliReason("PlanFee");
        tOtherItemSchema.setDrugName("统筹基金支付金额");
        tOtherItemSchema.setFeeMoney(700);
        testOtherItemSet.add(tOtherItemSchema);

//        tOtherItemSchema = new LLFeeOtherItemSchema();
//        tOtherItemSchema.setItemCode("BJ306");
//        tOtherItemSchema.setAvliReason("SelfAmnt");
//        tOtherItemSchema.setDrugName("现金支付自费金额");
//        tOtherItemSchema.setFeeMoney(2201.81);
//        testOtherItemSet.add(tOtherItemSchema);

        tOtherItemSchema = new LLFeeOtherItemSchema();
        tOtherItemSchema.setItemCode("BJ307");
        tOtherItemSchema.setAvliReason("SelfPay1");
        tOtherItemSchema.setDrugName("自付一");
        tOtherItemSchema.setFeeMoney(1600.00);
        testOtherItemSet.add(tOtherItemSchema);

//        tOtherItemSchema = new LLFeeOtherItemSchema();
//        tOtherItemSchema.setItemCode("BJ308");
//        tOtherItemSchema.setAvliReason("SelfPay2");
//        tOtherItemSchema.setDrugName("自付二");
//        tOtherItemSchema.setFeeMoney(1837.44);
//        testOtherItemSet.add(tOtherItemSchema);

//        tOtherItemSchema = new LLFeeOtherItemSchema();
//        tOtherItemSchema.setItemCode("BJ310");
//        tOtherItemSchema.setAvliReason("ApplyAmnt");
//        tOtherItemSchema.setDrugName("合计金额");
//        tOtherItemSchema.setFeeMoney(22781.70);
//        testOtherItemSet.add(tOtherItemSchema);



//        tOtherItemSchema = new LLFeeOtherItemSchema();
//        tOtherItemSchema.setItemCode("SH307");
//        tOtherItemSchema.setAvliReason("SecuRefuseFee");
//        tOtherItemSchema.setDrugName("现金支付");
//        tOtherItemSchema.setFeeMoney(253.83);
//        testOtherItemSet.add(tOtherItemSchema);
//        tOtherItemSchema = new LLFeeOtherItemSchema();
//        tOtherItemSchema.setItemCode("SH308");
//        tOtherItemSchema.setAvliReason("AccountPay");
//        tOtherItemSchema.setDrugName("帐户支付金额");
//        tOtherItemSchema.setFeeMoney(2441.52);
//        testOtherItemSet.add(tOtherItemSchema);
//            tOtherItemSchema = new LLFeeOtherItemSchema();
//            tOtherItemSchema.setItemCode("304");
//            tOtherItemSchema.setAvliReason("SupInHosFee");
//            tOtherItemSchema.setDrugName("大额医疗互助支付金额");
//            tOtherItemSchema.setFeeMoney(15568.27);
//            testOtherItemSet.add(tOtherItemSchema);
//        tOtherItemSchema = new LLFeeOtherItemSchema();
//        tOtherItemSchema.setItemCode("SH305");
//        tOtherItemSchema.setAvliReason("PlanFee");
//        tOtherItemSchema.setDrugName("统筹基金支付金额");
//        tOtherItemSchema.setFeeMoney(5097.32);
//        testOtherItemSet.add(tOtherItemSchema);
//        tOtherItemSchema = new LLFeeOtherItemSchema();
//        tOtherItemSchema.setItemCode("SH302");
//        tOtherItemSchema.setAvliReason("SelfAmnt");
//        tOtherItemSchema.setDrugName("现金支付自费金额");
//        tOtherItemSchema.setFeeMoney(247.68);
//        testOtherItemSet.add(tOtherItemSchema);
//        tOtherItemSchema = new LLFeeOtherItemSchema();
//        tOtherItemSchema.setItemCode("HZ304");
//        tOtherItemSchema.setAvliReason("SelfPay1");
//        tOtherItemSchema.setDrugName("自负一");
//        tOtherItemSchema.setFeeMoney(43.92);
//        testOtherItemSet.add(tOtherItemSchema);
//        tOtherItemSchema = new LLFeeOtherItemSchema();
//        tOtherItemSchema.setItemCode("SH303");
//        tOtherItemSchema.setAvliReason("SelfPay2");
//        tOtherItemSchema.setDrugName("自负二");
//        tOtherItemSchema.setFeeMoney(527.11);
//        testOtherItemSet.add(tOtherItemSchema);
//            tOtherItemSchema = new LLFeeOtherItemSchema();
//            tOtherItemSchema.setItemCode("309");
//            tOtherItemSchema.setAvliReason("OfficialSubsidy");
//            tOtherItemSchema.setDrugName("公务员医疗补助支付金额");
//            tOtherItemSchema.setFeeMoney(15568.27);
//            testOtherItemSet.add(tOtherItemSchema);
//            tOtherItemSchema = new LLFeeOtherItemSchema();
//            tOtherItemSchema.setItemCode("SH301");
//            tOtherItemSchema.setAvliReason("ApplyAmnt");
//            tOtherItemSchema.setDrugName("费用合计");
//            tOtherItemSchema.setFeeMoney(8313.63);
//            testOtherItemSet.add(tOtherItemSchema);
//            tOtherItemSchema = new LLFeeOtherItemSchema();
//            tOtherItemSchema.setItemCode("311");
//            tOtherItemSchema.setAvliReason("GetLimit");
//            tOtherItemSchema.setDrugName("起付线");
//            tOtherItemSchema.setFeeMoney(1300);
//            testOtherItemSet.add(tOtherItemSchema);
//            tOtherItemSchema = new LLFeeOtherItemSchema();
//            tOtherItemSchema.setItemCode("312");
//            tOtherItemSchema.setAvliReason("RetireAddFee");
//            tOtherItemSchema.setDrugName("退休人员补充医疗费用");
//            tOtherItemSchema.setFeeMoney(700);
//            testOtherItemSet.add(tOtherItemSchema);
//     tsLLSecurityReceiptSchema.setApplyAmnt("6300.78");
//     tsLLSecurityReceiptSchema.setFeeInSecu("6212.25");
//     tsLLSecurityReceiptSchema.setFeeOutSecu("88.53");
//     tsLLSecurityReceiptSchema.setTotalSupDoorFee("0.0");
//     tsLLSecurityReceiptSchema.setPayReceipt("43");
//     tsLLSecurityReceiptSchema.setSupDoorFee("3698.04");
//     tsLLSecurityReceiptSchema.setYearSupDoorFee("3698.04");
//     tsLLSecurityReceiptSchema.setPlanFee("5479.52");
//     tsLLSecurityReceiptSchema.setYearPlayFee("27978.96");
//     tsLLSecurityReceiptSchema.setSecurityFee("5479.52");
//     tsLLSecurityReceiptSchema.setSelfAmnt("39.56");
//     tsLLSecurityReceiptSchema.setSelfPay1("732.73");
//     tsLLSecurityReceiptSchema.setSelfPay2("48.97");
//     tsLLSecurityReceiptSchema.setSecuRefuseFee("821.26");
//     tsLLSecurityReceiptSchema.setRetireAddFee("792.44");
//     tsLLSecurityReceiptSchema.setYearRetireAddFee("792.44");
            testReceiptSchema = new LLCaseReceiptSchema();
            testReceiptSchema.setFeeItemCode("401");
            testReceiptSchema.setFeeItemName("药费");
            testReceiptSchema.setRefuseAmnt(2.97);
            testReceiptSchema.setFee(2300);
//            testReceiptSchema.setWholePlanFee(1202.00);
//            testReceiptSchema.setPartPlanFee(3000);
//            testReceiptSchema.setSelfPay2(2000);
//            testReceiptSchema.setSelfFee(2300);
            testReceiptSet.add(testReceiptSchema);
//            testReceiptSchema = new LLCaseReceiptSchema();
//            testReceiptSchema.setFeeItemCode("402");
//            testReceiptSchema.setFeeItemName("检查、治疗、化验");
//            testReceiptSchema.setRefuseAmnt(67.56);
//            testReceiptSchema.setFee(4969.78);
//            testReceiptSet.add(testReceiptSchema);
//            testReceiptSchema = new LLCaseReceiptSchema();
//           testReceiptSchema.setFeeItemCode("403");
//           testReceiptSchema.setFeeItemName("其他");
//           testReceiptSchema.setRefuseAmnt(18.00);
//           testReceiptSchema.setFee(129.00);
//           testReceiptSet.add(testReceiptSchema);

        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "xuxin";
        tGI.ManageCom = "86";
        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(testFeeMainSchema);
        tVData.add(testReceiptSet);
        tVData.add(testOtherItemSet);
        tVData.add(tsLLSecurityReceiptSchema);
        ICaseCureBL tICaseCureBL = new ICaseCureBL();
        tICaseCureBL.submitData(tVData, "INSERT");
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData 输入数据
     * @param cOperate String 操作类型
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //得到输入数据
        if (!getInputData(cInputData)) {
            return false;
        }
        //检查数据合法性
        if (!checkInputData()) {
            return false;
        }
        //进行业务处理
        if (!dealData(cOperate)) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate)) {
            CError.buildErr(this,"数据提交失败!");
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有数据对象
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("getInputData()..." + mOperate);
        mG = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mFeeMainSchema = (LLFeeMainSchema) cInputData.getObjectByObjectName(
                "LLFeeMainSchema", 0);
        mReceiptSet = (LLCaseReceiptSet) cInputData.getObjectByObjectName(
                "LLCaseReceiptSet", 0);
        mSeReceiptSchema = (LLSecurityReceiptSchema) cInputData.
                                   getObjectByObjectName(
                "LLSecurityReceiptSchema", 0);
        mFeeOtherItemSet = (LLFeeOtherItemSet) cInputData.getObjectByObjectName(
                "LLFeeOtherItemSet", 0);
        if (mFeeMainSchema == null) {
            CError.buildErr(this, "没有得到足够的数据，请您确认有账单信息!");
            return false;
        }
        MngCom = mFeeMainSchema.getMngCom();
        if(MngCom.length()<4)
            MngCom+="00";
        else
            MngCom = MngCom.substring(0, 4);
        aDate = PubFun.getCurrentDate();
        aTime = PubFun.getCurrentTime();
        return true;
    }

    /**
     * 对输入数据做必要的校验
     * @return boolean
     */
    private boolean checkInputData() {
        System.out.println("checkInputData()..." + mOperate);
        String sMainFeeNo = mFeeMainSchema.getMainFeeNo();
        String sReceiptNo = mFeeMainSchema.getReceiptNo();
        String sCaseNo = mFeeMainSchema.getCaseNo();
        String sHandler = "";
        if (sReceiptNo == null || "".equals(sReceiptNo)) {
            CError.buildErr(this, "账单号码不能为空!");
            return false;
        }
        if (sCaseNo == null) {
            CError.buildErr(this, "理赔号为空，无法找到账单对应的案件！");
            return false;
        }
        //查询案件状态
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(sCaseNo);
        if (tLLCaseDB.getInfo() == false) {
            CError.buildErr(this, "理赔案件查询失败," + "理赔号：" + sCaseNo);
            return false;
        }
        String RgtState = tLLCaseDB.getRgtState();
        if (!(RgtState.equals("01") || RgtState.equals("02") ||
              RgtState.equals("08"))) {
            CError.buildErr(this, "该案件状态下不能账单录入!");
            return false;
        }
//      #1738 社保调查 理赔各处理流程支持
        String tReturnMsg = LLCaseCommon.checkSurveyRgtState(sCaseNo,RgtState,"01");
        if(!"".equals(tReturnMsg)){
        	String tMsg = LLCaseCommon.checkSurveyRgtState(sCaseNo,RgtState,"02");
        	if(!"".equals(tMsg)){
            	CError.buildErr(this, tMsg);
                return false;
        	}
        }
//        String sql3 = "select ReceiptFlag from llcase where caseno='"+sCaseNo+"'";
//        String ReceiptFlag= tExeSQL.getOneValue(sql3);
//        System.out.print(ReceiptFlag+"1111"+sql3);
//        if (ReceiptFlag.equals("1")) {
//            CError.buildErr(this, "该案件账单已录入完成，如需修改，请回退账单!");
//            return false;
//        }

//        if (!tLLCaseDB.getMngCom().equals("86"))
//            MngCom = tLLCaseDB.getMngCom();
        sHandler = tLLCaseDB.getHandler();
//      modify by Houyd 社保案件在账单录入确认时，使用rigister来校验,支持申诉纠错案件
        String tCaseNo = "";
     	if("R".equals(sCaseNo.substring(0,1)) || "S".equals(sCaseNo.substring(0,1)))
     	{
     		System.out.println("申诉纠错案件换为正常的C案件");
     		String sql = "select caseno from LLAppeal where appealno ='"+sCaseNo+"'";
     		ExeSQL exeSQL = new ExeSQL();
     		tCaseNo = exeSQL.getOneValue(sql);
     	}else{
     		tCaseNo = sCaseNo;
     	}
        String tSocialFlag = LLCaseCommon.checkSocialSecurity(tCaseNo, null);
        if("1".equals(tSocialFlag)){
        	if(!mG.Operator.trim().equals(tLLCaseDB.getRigister())){
        		CError.buildErr(this, "对不起，您没有权限处理该社保案件!" +
                        "指定处理人：" + tLLCaseDB.getRigister());
                return false;
            }
        }else{
            sHandler = tLLCaseDB.getHandler();
            String sInputer = "" + tLLCaseDB.getClaimer();
            if (!sHandler.trim().equals(mG.Operator.trim())&& !sInputer.trim().equals(mG.Operator.trim())) {
                CError.buildErr(this, "对不起，您没有权限处理该案件!" +
                                "指定处理人：" + sInputer + "或" + sHandler);
                return false;
            }
        }

        if (sMainFeeNo != null && !sMainFeeNo.equals("")) {
            mOperate = "UPDATE";
        } else {
            mOperate = "INSERT";
        }
        return true;
    }

	/**
     * 根据前面的输入数据，进行逻辑处理
     * @param cOperate String 操作类型
     * @return boolean
     */
    private boolean dealData(String cOperate) {
        LLCaseCommon tcommon = new LLCaseCommon();
        System.out.println("dealData()..." + mOperate);
        mFeeMainSchema.setMngCom(mG.ManageCom);
        mFeeMainSchema.setOperator(mG.Operator);
        mFeeMainSchema.setModifyDate(aDate);
        mFeeMainSchema.setModifyTime(aTime);
        //保存录入
        if (mOperate.equals("INSERT")) {
            insertData();
        }
        if (mOperate.equals("UPDATE")) {
            updateData();
        }
        if (cOperate.equals("DELETE")) {
            updateData();
            return true;
        }
        mfieldvalue[0] = mG.ManageCom;
        mfieldvalue[1] = mG.Operator;
        mfieldvalue[2] = mFeeMainSchema.getMakeDate();
        mfieldvalue[3] = mFeeMainSchema.getMakeTime();
        mfieldvalue[4] = aDate;
        mfieldvalue[5] = aTime;
        mfieldvalue[6] = mFeeMainSchema.getCaseNo();
        mfieldvalue[7] = mFeeMainSchema.getRgtNo();
        String tMainFeeNo = mFeeMainSchema.getMainFeeNo();
        //创建社保账单记录
        String tfeeatti = mFeeMainSchema.getFeeAtti();
        if (!tfeeatti.equals("0") && !tfeeatti.equals("3") && !tfeeatti.equals("5")) {
            mSeReceiptSchema.setFeeDetailNo(tMainFeeNo);
            mSeReceiptSchema.setMainFeeNo(tMainFeeNo);
            tcommon.fillDefaultField(mSeReceiptSchema,mfieldvalue);

            if (!tfeeatti.equals("2")) {
                try {
                    fillSecuReceipt();
                }catch(Exception ex) {
                    backMessage = "填充社保账单失败,原因："+ex.getMessage();
                }
            }
            if(!tfeeatti.equals("4")){
                String tSFSQL = "SELECT * FROM llsecurityfactor WHERE comcode='"
                              + MngCom.substring(0, 4)+"'";
                ExeSQL tSFExeSQL = new ExeSQL();
                SSRS tSFSSRS = tSFExeSQL.execSQL(tSFSQL);
                if (tSFSSRS.getMaxRow()>0) {
                    if (MngCom.substring(0, 4).equals("8694")) {
                        calQDSecuDuty();
                    } else if (MngCom.substring(0, 4).equals("8631")) {
                        calSHSecuDuty();
                    } else if (MngCom.substring(0, 4).equals("8653")) {
                        calYNSecuDuty();
                    } else if (MngCom.substring(0, 4).equals("8695")) {
                        calSZSecuDuty();
                    } else if (MngCom.substring(0, 4).equals("8612")) {
                        if (mFeeMainSchema.getFeeType().equals("1"))
                            calDoorPay();
                        else if (mFeeMainSchema.getFeeType().equals("2"))
                            calYNSecuDuty();
                        else
                            calSpecialDoorPay();
                    } else {
                        calSecuDuty();
                    }
                }
            }
            map.put(mSeReceiptSchema, "INSERT");
            map.put(mLLSecuDetailSet, "INSERT");
        }
        //创建账单费用明细记录
        LLCaseReceiptSchema tReceiptSchema = null;
        LLCaseReceiptSet tReceiptSet = new LLCaseReceiptSet();
        double tsumFee = 0;
        double tpreamnt = 0;
        double tselfamnt = 0;
        double trefuseamnt = 0;
        String tLimit = PubFun.getNoLimit("86");
        for (int i = 1; i <= mReceiptSet.size(); i++) {
            tReceiptSchema = mReceiptSet.get(i);
            tReceiptSchema.setFeeDetailNo(
                    PubFun1.CreateMaxNo("FEEDETAILNO", tLimit));
            tReceiptSchema.setMainFeeNo(tMainFeeNo);
            String feetype = tReceiptSchema.getFeeItemCode().substring(0, 1);
            if (feetype.equals("1") || feetype.equals("2")) {
                tsumFee += tReceiptSchema.getFee();
                tpreamnt += tReceiptSchema.getPreAmnt();
                tselfamnt += tReceiptSchema.getSelfAmnt();
                trefuseamnt += tReceiptSchema.getRefuseAmnt();
            }
            tcommon.fillDefaultField(tReceiptSchema,mfieldvalue);
            tReceiptSet.add(tReceiptSchema);
        }
        if (mFeeMainSchema.getSumFee() < 0.01)
            mFeeMainSchema.setSumFee(tsumFee);
        mFeeMainSchema.setPreAmnt(tpreamnt);
        mFeeMainSchema.setSelfAmnt(tselfamnt);
        mFeeMainSchema.setRefuseAmnt(trefuseamnt);
        LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
        mLLCaseOpTimeSchema.setCaseNo(mFeeMainSchema.getCaseNo());
        mLLCaseOpTimeSchema.setRgtState("03");
        mLLCaseOpTimeSchema.setOperator(mG.Operator);
        mLLCaseOpTimeSchema.setManageCom(mG.ManageCom);
        LLCaseCommon tLLCaseCommon = new LLCaseCommon();
        try{
            LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(
                    mLLCaseOpTimeSchema);
            map.put(tLLCaseOpTimeSchema, "DELETE&INSERT");
        }catch(Exception ex){
            System.out.println("没有时效");
        }
        map.put(mFeeMainSchema, "INSERT");
        map.put(tReceiptSet, "INSERT");
        TransferData tipword = new TransferData();
        tipword.setNameAndValue("tipword",backMessage);
        mResult.add(mFeeMainSchema);
        mResult.add(tipword);
        //删除录入

        return true;
    }

    private boolean insertData() {
        String tLimit = PubFun.getNoLimit("86");
        String tMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO", tLimit);
        mFeeMainSchema.setMainFeeNo(tMainFeeNo);
        mFeeMainSchema.setMakeDate(aDate);
        mFeeMainSchema.setMakeTime(aTime);
        return true;
    }

    private boolean updateData() {
        String tMFNo = mFeeMainSchema.getMainFeeNo();
        String tCaseNo = mFeeMainSchema.getCaseNo();
        LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
        tLLFeeMainDB.setMainFeeNo(tMFNo);
        if (!tLLFeeMainDB.getInfo()) {
            CError.buildErr(this, "原账单信息查询失败！");
            return false;
        }
        mFeeMainSchema.setMakeDate(tLLFeeMainDB.getMakeDate());
        mFeeMainSchema.setMakeTime(tLLFeeMainDB.getMakeTime());
        mFeeMainSchema.setOldCaseNo(tLLFeeMainDB.getOldCaseNo());
        mFeeMainSchema.setOldMainFeeNo(tLLFeeMainDB.getOldMainFeeNo());
        mFeeMainSchema.setFirstInHos(tLLFeeMainDB.getFirstInHos());
        mFeeMainSchema.setRepayFlag(tLLFeeMainDB.getRepayFlag());
        String part = " where caseno='"+ tCaseNo + "' and mainfeeno='" + tMFNo +"'";
        String delFO = "DELETE FROM llfeeotheritem "+part;
        String delSR = "DELETE FROM LLSecurityReceipt "+part;
        String delSD = "DELETE FROM LLSecuDetail "+part;
        String delCR = "DELETE FROM LLCaseReceipt "+part;
        map.put(delFO, "DELETE");
        map.put(delSR, "DELETE");
        map.put(delSD, "DELETE");
        map.put(delCR, "DELETE");
        map.put(tLLFeeMainDB.getSchema(), "DELETE");
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        System.out.println("prepareOutputData()...");
        try {
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
            return false;
        }
        return true;
    }

    /**
     * 填充社保帐单表LLSecurityReceipt，页面以帐单细目的形式进行存储，有利于扩展。
     * @return boolean
     */
    private boolean fillSecuReceipt() {
        double tApplyAmnt = 0.0;
        double ttApplyAmnt = 0.0;
        double tSelfPay2 = 0.0;
        double ttSelfPay2 = 0.0;
        double tFeeInSecu = 0.0;
        double ttFeeInSecu = 0.0;
        double tSelfAmnt = 0.0;
        double ttSelfAmnt = 0.0;
        double ttSelfPay1 = 0.0;
        double tReimbursement = 0.0;
        String AreaFlag = "";
        String tmainfeeno = mFeeMainSchema.getMainFeeNo();
        LLFeeOtherItemSchema tOtherItemSchema = null;
        LLFeeOtherItemSet tOtherItemSet = new LLFeeOtherItemSet();
        LLCaseCommon tcommon = new LLCaseCommon();
        for (int i = 1; i <= mFeeOtherItemSet.size(); i++) {
            tOtherItemSchema = mFeeOtherItemSet.get(i);
            tOtherItemSchema.setCDSerialNo(
                    PubFun1.CreateMaxNo("FEEDETAILNO", "86"));
            tOtherItemSchema.setMainFeeNo(tmainfeeno);
            tcommon.fillDefaultField(tOtherItemSchema,mfieldvalue);
            tOtherItemSchema.setItemClass("S");
            tOtherItemSet.add(tOtherItemSchema);
            String sFee = ""+tOtherItemSchema.getFeeMoney();
            fillSReceiptField(tOtherItemSchema.getAvliReason(),sFee);
            System.out.println(tOtherItemSchema.getDrugName() + tOtherItemSchema.getFeeMoney());
            if (tOtherItemSchema.getAvliReason().equals("SupInHosFee")) {
                mSeReceiptSchema.setHighAmnt2(tOtherItemSchema.getFeeMoney());
                System.out.println("大额医疗互助" + tOtherItemSchema.getFeeMoney());
                continue;
            }
            if (tOtherItemSchema.getAvliReason().equals("ApplyAmnt")) {
                ttApplyAmnt = tOtherItemSchema.getFeeMoney();
                mFeeMainSchema.setSumFee(ttApplyAmnt);
                System.out.println("录入合计金额" + tOtherItemSchema.getFeeMoney());
                continue;
            }
            if (tOtherItemSchema.getAvliReason().equals("GetLimit")) {
                mSeReceiptSchema.setLowAmnt(tOtherItemSchema.
                        getFeeMoney());
                ttSelfPay1 += tOtherItemSchema.getFeeMoney();
                System.out.println("录入起付线" + tOtherItemSchema.getFeeMoney());
                continue;
            }
            if (tOtherItemSchema.getAvliReason().equals("MidAmnt")) {
                ttSelfPay1 += tOtherItemSchema.getFeeMoney();
                System.out.println("基本医疗个人自付部分" + tOtherItemSchema.getFeeMoney());
                continue;
            }
            if (tOtherItemSchema.getAvliReason().equals("HighAmnt1")) {
                ttSelfPay1 += tOtherItemSchema.getFeeMoney();
                System.out.println("大病医疗个人自付部分" + tOtherItemSchema.getFeeMoney());
                continue;
            }
            if (tOtherItemSchema.getAvliReason().equals("SuperAmnt")) {
                ttSelfPay1 += tOtherItemSchema.getFeeMoney();
                System.out.println("超大病限额自付" + tOtherItemSchema.getFeeMoney());
                continue;
            }
            if (tOtherItemSchema.getAvliReason().equals("RetireAddFee")) {
                mSeReceiptSchema.setYearRetireAddFee(tOtherItemSchema.
                        getFeeMoney());
                continue;
            }
            //可报销范围内金额
            if (tOtherItemSchema.getAvliReason().equals("Reimbursement")) {
                tReimbursement = tOtherItemSchema.getFeeMoney();
                mFeeMainSchema.setReimbursement(tReimbursement);
                System.out.println("录入合计金额" + tOtherItemSchema.getFeeMoney());
                continue;
            }
        }
        map.put(tOtherItemSet, "INSERT");

        LLCaseReceiptSchema tsReceiptSchema = null;
        for (int i = 1; i <= mReceiptSet.size(); i++) {
            tsReceiptSchema = mReceiptSet.get(i);
            if (tsReceiptSchema.getFeeItemCode().substring(0, 3).equals("QD2")) {
                tSelfPay2 += tsReceiptSchema.getSelfPay2();
                tFeeInSecu += tsReceiptSchema.getWholePlanFee();
                tFeeInSecu += tsReceiptSchema.getPartPlanFee();
                tSelfAmnt += tsReceiptSchema.getSelfFee();
                tApplyAmnt += tsReceiptSchema.getFee();
                AreaFlag = "QD";
                continue;
            }
            if (tsReceiptSchema.getFeeItemCode().equals("QD999")) {
                ttApplyAmnt = tsReceiptSchema.getFee();
                ttSelfPay2 = tsReceiptSchema.getSelfPay2();
                ttFeeInSecu = tsReceiptSchema.getWholePlanFee() +
                              tsReceiptSchema.getPartPlanFee();
                ttSelfAmnt += tsReceiptSchema.getSelfFee();
                AreaFlag = "QD";
                continue;
            }

            if (!tsReceiptSchema.getFeeItemCode().substring(0, 1).equals("4")) {
                tApplyAmnt += tsReceiptSchema.getFee();
                continue;
            }
        }
        if (ttApplyAmnt > 0)
            mSeReceiptSchema.setApplyAmnt(ttApplyAmnt);
        else
            mSeReceiptSchema.setApplyAmnt(tApplyAmnt);
        if (mSeReceiptSchema.getSelfPay1() <= 0 && ttSelfPay1 > 0)
            mSeReceiptSchema.setSelfPay1(ttSelfPay1);
        System.out.println("AreaFlag::::::::::::::::"+AreaFlag);
        if (AreaFlag.equals("QD")) {
            mSeReceiptSchema.setFeeInSecu(ttFeeInSecu);
            mSeReceiptSchema.setSelfAmnt(ttSelfAmnt);
            mSeReceiptSchema.setSelfPay2(ttSelfPay2);
        }
        if(mFeeMainSchema.getSumFee()<0.001)
            mFeeMainSchema.setSumFee(mSeReceiptSchema.getApplyAmnt());
        return true;
    }

    /**
     * 账单类型为“社保结算后”时，需要将普通账单项目表里的内容转为社保结算账单
     * @return boolean
     */
    private void fillSReceiptField(String fieldName, String fieldValue) {
        Class[] c = new Class[1];
        Method m = null;
        fieldName = "set"+fieldName;
        String[] tValue = {fieldValue};
        try {
            c[0] = Class.forName("java.lang.String");
        } catch (Exception ex) {
        }
        try {
            m = mSeReceiptSchema.getClass().getMethod(fieldName, c);
        } catch (Exception ex) {
        }
        try {
            m.invoke(mSeReceiptSchema, tValue);
        } catch (Exception ex) {
        }
    }

    /**
     * 计算社保各段赔付
     * @return boolean
     */
    private boolean calSecuDuty() {
        //社保住院类
        boolean tresult = true;
        String feetype = mFeeMainSchema.getFeeType();
        if (feetype.equals("2")||feetype.equals("3")) {
            tresult = calInHosSpan();
        } else {
            tresult = calDoorPay();
        }
        if(tresult)
            tresult = calRetireSecu();
        return tresult;
    }

    /**计算北京住院的分段
     * 可能出现的情况
     * 1。低段（没有统筹医疗和大额医疗的钱，自付一低于起付线）
     * 2。超高段（没有统筹医疗和大额医疗的钱）
     * 3。低段/中段（根据张立凯的算法能确定起付线/低段，自付一剩下的是中段）
     * 4。低段/中段/高段（先根据大额医疗确定高段，根据统筹的钱是否超过统筹封顶线，确定低段，自付一剩下的是中段）
     * 5。高段/超高段（没有统筹医疗，有大额医疗，自付一>高段）
     * 6。高段（没有统筹医疗，有大额医疗，自付一=高段）
     * 7。低段/中段/高段/超高段（最复杂的情况，可能推不出正确的答案，此时大额医疗是赔满的，并且有统筹医疗的钱）
     */
    private boolean calInHosSpan() {
        String ssql = "";
        double TA = mSeReceiptSchema.getApplyAmnt(); //本次住院医疗费用总额
        double SI = mSeReceiptSchema.getFeeInSecu(); //本次住院医保范围内金额
        double SO = mSeReceiptSchema.getFeeOutSecu(); //本次住院医保范围外金额
        double PM = mSeReceiptSchema.getSelfAmnt(); //本次住院自费金额
        double SP2 = mSeReceiptSchema.getSelfPay2(); //本次住院自付二金额
        double SP1 = mSeReceiptSchema.getSelfPay1(); //本次住院自付一金额
        double PSI = mSeReceiptSchema.getPlanFee(); //本次住院统筹支付金额
        double PL = mSeReceiptSchema.getSupInHosFee(); //本次住院大额互助支付金额
        double PGL = mSeReceiptSchema.getGetLimit();   //统筹起付线
        mFeeMainSchema.setSumFee(mSeReceiptSchema.getApplyAmnt());
        SO = (SO < 0.001)?(PM + SP2):SO;
        mSeReceiptSchema.setFeeOutSecu(SO);
        SI = (SI < 0.001)?(TA - SO):SI;
        mSeReceiptSchema.setFeeInSecu(SI);
        PSI = (PSI < 0.001)?(SI - SP1 - PL):PSI;
        mSeReceiptSchema.setPlanFee(PSI);
        SP1 = (SP1 < 0.001)?(SI - PSI - PL):SP1;
        mSeReceiptSchema.setPlanFee(PSI);
        if (SP1 < 0.001) {
            backMessage += "<br>自付一金额为0，无法计算分段金额！";
            return false;
        }
        if (Math.abs(TA - PSI - SP1 - PM - SP2 - PL) > 0.01 ||
            Math.abs(TA - SI - SO) > 0.01) {
            backMessage = "申报金额" + TA + "元不等于社保范围内费用与社保外"
                         + "费用之和，请检查各项数据是否录入正确";
            return false;
        }
        ssql = "select distinct getlimit,firstinhos,peakline from llsecurityfactor "
               +" where securitytype='1001' and comcode='"+MngCom
               +"' and getlimit>0 order by getlimit desc";
        ExeSQL texesql = new ExeSQL();
        SSRS tssrs = texesql.execSQL(ssql);
        System.out.println(ssql);
        if(tssrs.getMaxRow()<=0){
            backMessage += "<br>系统没有记录该机构的社保信息，无法进行反推计算！";
            return false;
        }
        if(PSI+PL<0.001){
            //第1种情况，只有低段(比首次住院起付线低)，该比较并不严密，但能解决大部分问题，
            //以后再通过查询既往理赔信息来解决，但是即便如此也不可能解决所有问题，而且麻烦
            double maxgetlimit = Double.parseDouble(tssrs.GetText(1,1));
            if(SP1<maxgetlimit){
                backMessage += "<br>本次医疗费用未超社保起付线。";
                mSeReceiptSchema.setGetLimit(maxgetlimit);
                mSeReceiptSchema.setLowAmnt(SP1);
                fillSecuSpan("1",0,maxgetlimit,0,0,SP1);
                return true;
            }else{
                //第2种情况，只有超高段
                backMessage += "<br>统筹医疗和大额互助医疗均无金额，且自付一高于社保起付线，所以将自付一划入超高段。";
                mSeReceiptSchema.setSuperAmnt(SP1);
                fillSecuSpan("99",0,0,0,0,SP1);
                return true;
            }
        }
        double ppeakline = Double.parseDouble(tssrs.GetText(1,3));
        String tLevelCode = "";//医院等级只分一二三等，不细分到甲乙丙丁
        try {
            tLevelCode = mFeeMainSchema.getHosGrade().substring(0, 1);
        } catch (Exception ex) {
            backMessage += "<br>医院没有级别!";
        }
        String tisustat = mFeeMainSchema.getInsuredStat() + "";
        //基本医疗保险统筹基金支付
        //查询除低端外的所有分段信息 这样写是为了查询有效时间段方便
        ssql = " and securitytype='1001' and getrate>0 and levelcode='" + tLevelCode
               + "' and insuredstat='" + tisustat + "' order by downlimit";
        LLSecurityFactorSchema tSFSchema = new LLSecurityFactorSchema();
        LLSecurityFactorSet tSFSet = getSeInfo(ssql);
        if (tSFSet.size() <= 0) {
            backMessage += "<br>查询社保信息失败，本次保存没有做社保反推运算！";
            return false;
        }
        int Fcount = tSFSet.size();
        if(PL<0.001){
            //第3种情况，没有大额医疗，则不可能有高段
            if(PGL>0.001){
                //如果录入了起付线，则不必反推
                mSeReceiptSchema.setLowAmnt(PGL);
                fillSecuSpan("1", 0, PGL, 0, 0, PGL);
                if(SP1>PGL){
                    double midamnt = SP1 - PGL;
                    mSeReceiptSchema.setMidAmnt(midamnt);
                    fillSecuSpan("2", PGL, ppeakline, 0, PSI, midamnt);
                }
                return true;
            }
            double Luplimit=0;
            double Lowrate = 0;
            double spanMoney = 0.0;
            double Midspan = 0.0;

            for (int i = 1; i <= Fcount; i++) {
                tSFSchema = tSFSet.get(i);
                double uplimit = tSFSchema.getUpLimit();
                double downlimit = tSFSchema.getDownLimit();

                String tFirstInHos = tSFSchema.getFirstInHos();
                //中段的钱先跳过起付线不确定的分段
                if (tFirstInHos != null) {
                    Luplimit = (SI >= uplimit)?uplimit:SI;
                    Lowrate = tSFSchema.getGetRate();
                    continue;
                }
                double tmpSpan = 0.0; //社保分段的钱
                double tgetrate = tSFSchema.getGetRate();
                if(SI<uplimit||Math.abs(uplimit-ppeakline)<0.001){
                    tmpSpan = SI - downlimit;
                } else {
                    tmpSpan = uplimit - downlimit;
                }
                if (tmpSpan > 0.0) {
                    spanMoney += tmpSpan * tgetrate;
                    Midspan += tmpSpan * (1 - tgetrate);
                    fillSecuSpan(i + "", downlimit, uplimit, tgetrate,
                                   tmpSpan * tgetrate, tmpSpan * (1 - tgetrate));
                } else {
                    spanMoney += 0.0;
                    Midspan += 0.0;
                    break;
                }
            }
            double firstspan = (PSI - spanMoney) / Lowrate* (1 - Lowrate);

            Midspan += firstspan;
            mSeReceiptSchema.setMidAmnt(Arith.round(Midspan, 2));
            double lowamnt = SP1 - Midspan;
            mSeReceiptSchema.setLowAmnt(lowamnt);
            double templimit = 0;
            double tempdiff = 0;
            String tempFIH = "";
            for (int xx = 1; xx <= tssrs.getMaxRow(); xx++) {
                double tl = Double.parseDouble(tssrs.GetText(xx,1));
                if (Math.abs(lowamnt - tl) < tempdiff||xx==1) {
                    tempdiff = Math.abs(lowamnt - tl);
                    templimit = tl;
                    tempFIH = tssrs.GetText(xx,2);
                }
            }
            mSeReceiptSchema.setGetLimit(templimit);
            mFeeMainSchema.setFirstInHos(tempFIH);
            fillSecuSpan("2", mSeReceiptSchema.getGetLimit(), Luplimit, Lowrate,
                           PSI - spanMoney, firstspan);
            fillSecuSpan("1",0,templimit,0,0,lowamnt);
        }else{
            LLSecurityFactorSet hSFSet = getSeInfo(" and securitytype='3001' ");
            if (hSFSet.size() <= 0) {
                backMessage += "<br>有大额医疗互助金，但是没有查询到大额医疗的社保政策！";
                return false;
            }
            tSFSchema = hSFSet.get(1);
            double hgetrate = tSFSchema.getGetRate();
            double hpeakline = tSFSchema.getPeakLine();
            if(PL>hpeakline){
                backMessage += "<br>大额医疗金额高于社保封顶线"+hpeakline;
                return false;
            }
            double highamnt = 0;
            double superamnt = 0;
            if (hgetrate > 0) {
                highamnt = Arith.round(PL / hgetrate * (1 - hgetrate),2);
                mSeReceiptSchema.setHighAmnt1(highamnt);
            }
            if(PSI<0.001){
                //统筹的钱已赔完，没有低段和中段,第5种和第6种情况
                mSeReceiptSchema.setLowAmnt(0);
                mSeReceiptSchema.setMidAmnt(0);
                fillSecuSpan("1",ppeakline,hpeakline,hgetrate,PL,highamnt);
                superamnt = SP1-highamnt;
                if(superamnt>0.001){
                    mSeReceiptSchema.setSuperAmnt(superamnt);
                    fillSecuSpan("2",hpeakline,0,0,0,superamnt);
                }
                return true;
            }
            if(PGL<0.001){
                if (PSI < ppeakline) {
                    PGL = tSFSet.get(1).getGetLimit();
                    mFeeMainSchema.setFirstInHos("N");
                } else {
                    //这里的判断是错的，如果第一次住院费用没有超起付线，则此次可能统筹赔满，但是第二次的起付线
                    PGL = tSFSet.get(2).getGetLimit();
                    mFeeMainSchema.setFirstInHos("Y");
                }
                mSeReceiptSchema.setGetLimit(PGL);
                mSeReceiptSchema.setLowAmnt(PGL);
            }
            if(PSI>0&&PL<hpeakline){
                //如果有统筹的钱，且大额医疗没有超封顶线，则不可能有超高段，第4种情况
                double midamnt = SP1-PGL-highamnt;
                if(midamnt<0){
                    PGL=0;
                    midamnt = SP1-highamnt;
                }
                mSeReceiptSchema.setGetLimit(PGL);
                mSeReceiptSchema.setLowAmnt(PGL);
                mSeReceiptSchema.setMidAmnt(midamnt);
                fillSecuSpan("1",0,PGL,0,0,PGL);
                fillSecuSpan("2",PGL,ppeakline,0,PSI,midamnt);
                fillSecuSpan("3",ppeakline,hpeakline,hgetrate,PL,highamnt);
                return true;
            }
            //最复杂的第7种情况，无法完美实现，只能够确定低段和高段
            fillSecuSpan("1",0,PGL,0,0,PGL);
            //后面的也会加序号是3的，先把这条注掉
//            fillSecuSpan("3",ppeakline,hpeakline,hgetrate,PL,highamnt);
            int spanCount = tSFSet.size();
            double midamnt = 0;
            double planf = ppeakline;
            boolean fullPSI = false;
            if(Math.abs(PSI-ppeakline)<0.001)
                fullPSI = true;
            for (int i = 2; i < spanCount; i++) {
                tSFSchema = tSFSet.get(i);
                double uplimit = tSFSchema.getUpLimit();
                double downlimit = tSFSchema.getDownLimit();
                double getrate = tSFSchema.getGetRate();
                if (i == 2)
                    downlimit = PGL;
                double spanm = (uplimit - downlimit) * getrate;
                double spans = (uplimit - downlimit) * (1 - getrate);
                if (fullPSI) {
                    fillSecuSpan(i + "", downlimit, uplimit, getrate, spanm,
                                   spans);
                    midamnt += spans;
                }
                planf -= spanm;
            }
            tSFSchema = tSFSet.get(spanCount);
            double mgetrate = tSFSchema.getGetRate();
            double mspans = planf/mgetrate*(1-mgetrate);
            if (fullPSI) {
                fillSecuSpan(spanCount + "", tSFSchema.getDownLimit(),
                               tSFSchema.getUpLimit(), mgetrate, planf, mspans);
                midamnt += mspans;
                mSeReceiptSchema.setMidAmnt(midamnt);
                fillSecuSpan(spanCount + 1 + "", ppeakline, hpeakline,
                               hgetrate, PL, highamnt);
                double supamnt = SP1 - PGL - midamnt - highamnt;
                mSeReceiptSchema.setSuperAmnt(supamnt);
                fillSecuSpan(spanCount + 2 + "", hpeakline, 0, 0, 0, supamnt);
                return true;
            }
            if(PSI>planf){
                backMessage = "分段计算太复杂，系统无法处理。。。";
                return false;
            }
            midamnt = PSI/mgetrate*(1-mgetrate);
            fillSecuSpan(spanCount + "", tSFSchema.getDownLimit(),
                           tSFSchema.getUpLimit(), mgetrate, PSI, midamnt);
            mSeReceiptSchema.setMidAmnt(midamnt);
            fillSecuSpan(spanCount + 1 + "", ppeakline, hpeakline,
                           hgetrate, PL, highamnt);
            double supamnt = SP1 - PGL - midamnt - highamnt;
            mSeReceiptSchema.setSuperAmnt(supamnt);
            fillSecuSpan(spanCount + 2 + "", hpeakline, 0, 0, 0, supamnt);
        }
        return true;
    }

    /**
     * 计算北京门诊分段
     * @return boolean
     */
    private boolean calDoorPay(){
        String tisustat = mFeeMainSchema.getInsuredStat() + "";
        //计算门诊费用
        LLSecurityFactorDB tSFDB = new LLSecurityFactorDB();
        LLSecurityFactorSet tSFSet = new LLSecurityFactorSet();
        LLSecurityFactorSchema tSFSchema = new LLSecurityFactorSchema();
        double DFee = mSeReceiptSchema.getApplyAmnt(); //门诊总费用
        double FIS = mSeReceiptSchema.getFeeInSecu();  //社保内费用
        double FOS = mSeReceiptSchema.getFeeOutSecu(); //社保外费用
        double PM = mSeReceiptSchema.getSelfAmnt();
        double SP2 = mSeReceiptSchema.getSelfPay2();
        double SP1 = mSeReceiptSchema.getSelfPay1();
        double DPL = mSeReceiptSchema.getSupDoorFee(); //大额门诊费用
        double TYSDF = mSeReceiptSchema.getTotalSupDoorFee(); //本次理赔前年度大额门诊总费用
        double YSDF = mSeReceiptSchema.getYearSupDoorFee();   //年度大额门诊费用累计金额
        double DGL = 0.0; //门诊起付限

        DPL = (DPL<0.001)?mSeReceiptSchema.getPlanFee():DPL;
        YSDF = (YSDF < DPL)?(DPL + TYSDF):YSDF;
        TYSDF = (TYSDF <= 0)?(YSDF - DPL):TYSDF;
        FIS = (FIS < 0.001) ? (DPL + SP1) : FIS;
        mSeReceiptSchema.setFeeInSecu(FIS);
        FOS = (FOS < 0.001) ? (SP2 + PM) : FOS;
        mSeReceiptSchema.setFeeOutSecu(FOS);
        DFee = (DFee < 0.001)?(FIS + FOS):DFee;
        mSeReceiptSchema.setApplyAmnt(DFee);

        mSeReceiptSchema.setSupDoorFee(DPL);
        mSeReceiptSchema.setPlanFee(0);
        int age = mFeeMainSchema.getAge();
        if (age >= 70 && tisustat.equals("2")&&MngCom.equals("8611"))
            tisustat = "3";

        //查询门诊社保政策
        tSFDB.setSecurityType("2001");
        tSFDB.setInsuredStat(tisustat);
        tSFDB.setComCode(MngCom);
        String sql = " and securitytype = '2001' and insuredstat='"+tisustat+"' ";
        if (MngCom.equals("8612"))
            sql += " and agestart <="+age+" and ageend>"+age;
        tSFSet = getSeInfo(sql);
        if(tSFSet.size()<=0){
            backMessage += "<br>系统没有找到符合条件的门诊社保政策.";
            return false;
        }
        tSFSchema = tSFSet.get(1);
        DGL = tSFSchema.getGetLimit();
        double dpeakline = tSFSchema.getPeakLine();
        double dgetrate = tSFSchema.getGetRate();
        double sDoorPay = 0.0; //小额门急诊保险金
        double bDoorPay = 0.0; //大额门急诊保险金
        double cDoorPay = 0.0; //门急诊保险金
        double ODoorPay = 0.0; //超过门诊赔付限额
        bDoorPay = DPL / dgetrate * (1 - dgetrate);
        bDoorPay = Arith.round(Arith.round(bDoorPay, 4), 2);
        if (YSDF <= dpeakline) {
            if (TYSDF >= 0.01) {
                sDoorPay = 0;
                DGL = 0;
            }else if (SP1 < DGL) {
                if (DPL < 0.01 ){ //后加，但是初次条款中规定大额为0时，不赔低段
                    sDoorPay = SP1;
                    DGL= SP1;
                }
                else {
                    DGL = FIS - DPL / dgetrate;
                    if (DGL < 0) DGL = 0;
                    sDoorPay = DGL;
                    backMessage += "本次费用非该年度的第一次门诊费用。";
                    System.out.println("本次费用非该年度的第一次门诊费用。");
                }
            } else {
                double tDGL = FIS - DPL / dgetrate;
                if (tDGL <= 0) DGL = 0;
                else if (DGL - tDGL > 1 && tDGL > 0) {
                    DGL = tDGL;
                    System.out.println("DGL" + DGL);
                    sDoorPay = DGL;
                    backMessage += "本次费用非该年度的第一次门诊费用。";
                    System.out.println("本次费用非该年度的第一次门诊费用。");
                } else
                    sDoorPay = DGL;
            }
            ODoorPay = SP1 - bDoorPay - sDoorPay;
        }else{
            backMessage += "门诊费用已超过年度最高限额。";
            System.out.println("门诊费用已超过年度最高限额。");

        }


        //门急诊费用不能超过当地封顶线（此处是限额，没有累计，没有减起付线）
        cDoorPay = DFee < dpeakline ? DFee : dpeakline;
        fillSecuSpan("1", 0,  tSFSchema.getGetLimit(), 0, 0, sDoorPay);
        fillSecuSpan("2", tSFSchema.getGetLimit(), dpeakline, dgetrate, DPL, bDoorPay);
        if (ODoorPay > 0.01)
            fillSecuSpan("3", 100000, 0, 0, 0, ODoorPay);
        mSeReceiptSchema.setSelfPay1(SP1);
        mSeReceiptSchema.setGetLimit(DGL);
        mSeReceiptSchema.setSmallDoorPay(sDoorPay);
        mSeReceiptSchema.setEmergencyPay(cDoorPay);
        mSeReceiptSchema.setHighDoorAmnt(bDoorPay);
//        mFeeMainSchema.setSumFee(DFee);
        return true;
    }

    private boolean calSpecialDoorPay(){
        double PGL = mSeReceiptSchema.getPlanFee();
        double PL = mSeReceiptSchema.getSupInHosFee(); //本次住院大额互助支付金额
        LLSecurityFactorSet hSFSet = new LLSecurityFactorSet();
        LLSecurityFactorSchema tSFSchema = new LLSecurityFactorSchema();
        if(PL>0){
            hSFSet = getSeInfo(" and securitytype='3001' ");
            if (hSFSet.size() <= 0) {
                backMessage += "<br>有大额医疗互助金，但是没有查询到大额医疗的社保政策！";
                return false;
            }
            tSFSchema = hSFSet.get(1);
            double hgetrate = tSFSchema.getGetRate();
            double hpeakline = tSFSchema.getPeakLine();
            if(PL>hpeakline){
                backMessage += "<br>大额医疗金额高于社保封顶线"+hpeakline;
                return false;
            }
            double highamnt = 0;
            if (hgetrate > 0) {
                highamnt = Arith.round(PL / hgetrate * (1 - hgetrate),2);
                mSeReceiptSchema.setHighAmnt1(highamnt);
                fillSecuSpan("3",0,tSFSchema.getPeakLine(),hgetrate,PL,highamnt);
            }
        }
        String tisustat = mFeeMainSchema.getInsuredStat() + "";
        String sql = " and securitytype='1002' and insuredstat='"+tisustat+"' ";
        LLSecurityFactorSet tSFSet = getSeInfo(sql);
        if(tSFSet.size()<=0){
            backMessage = "查询社保政策信息失败，请录入人员状态。";
            CError.buildErr(this,backMessage);
            return false;
        }
        double tgetlimit = mSeReceiptSchema.getGetLimit();
        tSFSchema = tSFSet.get(1);
        double tRate = tSFSchema.getGetRate();
        double midamnt = PGL/tRate*(1-tRate);
        midamnt = Arith.round(midamnt,2);
        mSeReceiptSchema.setMidAmnt(midamnt);
        double lowamnt = 0;
        if(tgetlimit>0.001){
            lowamnt = tgetlimit;
        }else{
            lowamnt = tSFSchema.getGetLimit();
            String tsql = "select * from llfeemain where customerno='"
                          + mFeeMainSchema.getCustomerNo() +
                          "' and mainfeeno <>'"
                          + mFeeMainSchema.getMainFeeNo() +
                          "' and feetype in ('2','3')";
            LLFeeMainDB tmainfeedb = new LLFeeMainDB();
            LLFeeMainSet omfset = tmainfeedb.executeQuery(tsql);
            if (omfset.size() > 0) {
                lowamnt = 0;
                backMessage += "<br>客户" + mFeeMainSchema.getCustomerName()
                        + "在此账单前发生过住院或门特的理赔:";
                for (int i = 1; i <= omfset.size(); i++) {
                    LLFeeMainSchema tfeemain = omfset.get(i);
                    String tname = tfeemain.getFeeType().equals("1") ? "住院" :
                                   "门诊特殊病";
                    backMessage += "<br>案件" + tfeemain.getCaseNo() + "有" +
                            tname
                            + "账单,账单号：" + tfeemain.getReceiptNo();
                }
                backMessage += "<br>因此系统将起付线置为0";
            }
            mSeReceiptSchema.setGetLimit(lowamnt);
        }
        mSeReceiptSchema.setLowAmnt(lowamnt);
        fillSecuSpan("1",0,lowamnt,0,0,lowamnt);
        fillSecuSpan("2",lowamnt,tSFSchema.getPeakLine(),tRate,PGL,midamnt);
        return true;
    }

    private boolean calRetireSecu(){
        double RA = mSeReceiptSchema.getRetireAddFee(); //退休费用
        String tisustat = mFeeMainSchema.getInsuredStat();
        String tfeetype = mFeeMainSchema.getFeeType();
        String sql = " and securitytype='4001' and insuredstat='"
                     + tisustat + "' and  startdate <= '"
                     + mFeeMainSchema.getFeeDate() +"'"; //查询退休人员补充政策
        LLSecurityFactorSet tSFSet = getSeInfo(sql);
        if (tSFSet.size() <= 0){
            if(RA>0.001){
                backMessage += "申报退休人员补充金额" + RA
                        + ",但住院期间没有退休医疗补充政策！";
                return false;
            }
            return true;
        }
        double RARate = tSFSet.get(1).getGetRate();
        double tRA = 0;
        if(tfeetype.equals("1")){
            tRA = mSeReceiptSchema.getHighDoorAmnt()*RARate;
            tRA = Arith.round(Arith.round(tRA, 3), 2);
            mSeReceiptSchema.setHighDoorAmnt(tRA);
        }else{
            double tRAm = mSeReceiptSchema.getMidAmnt()*RARate;
            tRAm = Arith.round(Arith.round(tRAm, 3), 2);
            mSeReceiptSchema.setMidAmnt(tRAm);
            double tRAh = mSeReceiptSchema.getHighAmnt1()*RARate;
            tRAh = Arith.round(Arith.round(tRAh, 3), 2);
            mSeReceiptSchema.setHighAmnt1(tRAh);
            tRA = tRAm+tRAh;
        }

        if (RA > 0.01) {
            if (Math.abs(tRA - RA) > 0.01) {
                String errMessage = "申报退休补充金" + RA + "元不等于计算出的退休补充金"
                                    + tRA + "元，请检查各项数据是否录入正确";
                CError.buildErr(this, errMessage);
                return false;
            }
        } else {
            mSeReceiptSchema.setRetireAddFee(tRA);
            mSeReceiptSchema.setYearRetireAddFee(tRA);
        }
        return true;
    }

    /**
    * 青岛社保账单理算
    * @return boolean
    */
    private boolean calQDSecuDuty() {
        System.out.println("开始计算青岛帐单");
        //初始化变量
        double TA = mSeReceiptSchema.getApplyAmnt(); //本次住院医疗费用总额
        double SI = mSeReceiptSchema.getFeeInSecu(); //本次住院医保范围内金额
        double PM = mSeReceiptSchema.getSelfAmnt(); //本次住院自费金额
        double PSI = mSeReceiptSchema.getPlanFee(); //本次住院统筹支付金额
        double PL = mSeReceiptSchema.getSupInHosFee(); //本次住院大额互助支付金额
        double SO = mSeReceiptSchema.getFeeOutSecu(); //本次住院医保范围外金额
        double SP1 = mSeReceiptSchema.getSelfPay1(); //本次住院自付一金额
        double SP2 = mSeReceiptSchema.getSelfPay2(); //本次住院自付二金额

        double tpeakline = 0.0; //封顶线
        String tLevelCode = "";
        if (SO < 0.001) {
            SO += PM + SP2;
            mSeReceiptSchema.setFeeOutSecu(SO);
        }
        if (SI < 0.001) {
            SI += TA - SO;
            mSeReceiptSchema.setFeeInSecu(SI);
        }
        if (PSI < 0.001) {
            PSI += SI - SP1;
        }
        if (SP1 < 0.001) {
            SP1 += SI - PSI;
            mSeReceiptSchema.setSelfPay1(SP1);
        }
        PSI -= PL;
        tLevelCode = mFeeMainSchema.getHosGrade().substring(0, 1); //医院等级只分一二三等，不细分到甲乙丙丁
        //取社保计算要素
        LLSecurityFactorSet tLLSecurityFactorSet = new LLSecurityFactorSet();
        //社保住院类
        String feetype = mFeeMainSchema.getFeeType();
        String insusta = mFeeMainSchema.getInsuredStat()+"";
        if (feetype.equals("2")||feetype.equals("3")) {
            mSeReceiptSchema.setPlanFee(PSI);
            //基本医疗保险统筹基金支付
            String sql=" and securitytype='1001' and getrate>0 and LevelCode='"
                       + tLevelCode +"' and insuredstat='"+ insusta +
                       "' order by downlimit";
            tLLSecurityFactorSet = getSeInfo(sql);
            double MidSpan = 0.0;
            double tSpan = 0.0;
            double ttSpan = 0.0;
            boolean HasDE = true;
            double downlimit = 0.0;
            double uplimit = 0.0;
            int i = 1;
            if (tLLSecurityFactorSet != null &&
                tLLSecurityFactorSet.size() != 0) {
                int Fcount = tLLSecurityFactorSet.size();
                tpeakline = tLLSecurityFactorSet.get(1).getPeakLine();
                for (i = 1; i <= Fcount; i++) {
                    double spangetrate = tLLSecurityFactorSet.get(i).getGetRate();
                    downlimit = tLLSecurityFactorSet.get(i).getDownLimit();
                    uplimit = tLLSecurityFactorSet.get(i).getUpLimit();
                    if (i == Fcount) {
                        uplimit = (tpeakline - ttSpan) / spangetrate +
                                  downlimit;
                    }
                    tSpan += (uplimit - downlimit) * spangetrate;
                    double spanPF = 0;
                    double spanSP = 0;
                    if (PSI >= tSpan) {
                        spanPF = (uplimit - downlimit) *spangetrate;
                        spanSP = (uplimit - downlimit) *(1 - spangetrate);
                        fillSecuSpan(i + 1 + "",downlimit,uplimit,spangetrate,spanPF,spanSP);
                        ttSpan += spanPF;
                        MidSpan += spanSP;
                    } else {
                        spanPF = PSI - ttSpan;
                        spanSP = Arith.round((PSI - ttSpan) /
                                             spangetrate * (1 - spangetrate), 2);
                        fillSecuSpan(i + 1 + "", downlimit, uplimit,
                                       spangetrate, spanPF, spanSP);
                        MidSpan += spanSP;
                        HasDE = false;
                        break;
                    }
                }
            }
            System.out.println("青岛自负1" + SP1);
            System.out.println("青岛中段" + MidSpan);
            mSeReceiptSchema.setMidAmnt(MidSpan);
            if (HasDE) {
                tLLSecurityFactorSet = getSeInfo(" and securitytype='3001'");
                if (tLLSecurityFactorSet.size() > 0) {
                    double supgetrate = tLLSecurityFactorSet.get(1).getGetRate();
                    double suplimit = tLLSecurityFactorSet.get(1).getPeakLine();
                    downlimit = uplimit;
                    uplimit = suplimit / supgetrate + downlimit;
                    PL = PSI - ttSpan;
                    double tmphamnt1 = PL / supgetrate * (1 - supgetrate);
                    mSeReceiptSchema.setHighAmnt1(tmphamnt1);
                    fillSecuSpan(i+1+"",downlimit,uplimit,supgetrate,PL,tmphamnt1);
                    System.out.println("青岛高段" + tmphamnt1);
                }
            }
            System.out.println("青岛超高段：" +mSeReceiptSchema.getSuperAmnt());
            if (mSeReceiptSchema.getSuperAmnt() > 0) {
                fillSecuSpan(i+2+"",uplimit,0,0,0,mSeReceiptSchema.getSuperAmnt());
            }
            double lowamnt = SP1 - MidSpan - mSeReceiptSchema.getHighAmnt1()
                             -mSeReceiptSchema.getSuperAmnt();
            mSeReceiptSchema.setLowAmnt(lowamnt);
            fillSecuSpan("1",0,lowamnt,0,0,lowamnt);
        }
        return true;
    }

    /**
     * 上海社保账单理算
     * @return boolean
     */
    private boolean calSHSecuDuty() {
        System.out.println("开始计算上海帐单");
        //初始化变量
        double TA = mSeReceiptSchema.getApplyAmnt(); //本次住院医疗费用总额
        double SI = mSeReceiptSchema.getFeeInSecu(); //本次住院医保范围内金额
        double PM = mSeReceiptSchema.getSelfAmnt(); //本次住院自费金额
        double PSI = mSeReceiptSchema.getPlanFee(); //本次住院统筹支付金额
        double PL = mSeReceiptSchema.getSupInHosFee(); //本次住院大额互助支付金额
        double SO = mSeReceiptSchema.getFeeOutSecu(); //本次住院医保范围外金额
        double SP1 = mSeReceiptSchema.getSelfPay1() +
                     mSeReceiptSchema.getAccountPay(); //本次住院自付一金额
        double SP2 = mSeReceiptSchema.getSelfPay2(); //本次住院自付二金额
        double tpeakline = 0.0; //封顶线
        String tLevelCode = "";
        SO = (SO < 0.001) ? (PM + SP2) : SO;
        mSeReceiptSchema.setFeeOutSecu(SO);
        SI = (SI < 0.001) ? (TA - SO) : SI;
        mSeReceiptSchema.setFeeInSecu(SI);
        System.out.println(PSI < 0.001);
        PSI =(PSI < 0.001)? (SI - SP1):PSI;
        SP1 = (SP1 < 0.001) ? (SI - PSI) : SP1;
        mSeReceiptSchema.setSelfPay1(SP1);
        PSI -= PL;
        String insustat = mFeeMainSchema.getInsuredStat()+"";
        tLevelCode = mFeeMainSchema.getHosGrade().substring(0, 1); //医院等级只分一二三等，不细分到甲乙丙丁
        //取社保计算要素
        LLSecurityFactorSet tSFSet = new LLSecurityFactorSet();
        //社保住院类
        String feetype = mFeeMainSchema.getFeeType()+"";
        if (feetype.equals("2")||feetype.equals("3")) {
            mSeReceiptSchema.setPlanFee(PSI);
            //基本医疗保险统筹基金支付
            String sql = " and securitytype='1001' and insuredstat='" + insustat
                         +"' and getrate>0 order by downlimit";
            tSFSet = getSeInfo(sql);
            double MidSpan = 0.0;
            double downlimit = 0.0;
            double uplimit = 0.0;
            double tmphamnt1 = 0.0;
            tpeakline = tSFSet.get(1).getPeakLine();
            uplimit = tpeakline;
            int i = 1;
            if (PL > 0) {
                sql = " and securitytype='3001' ";
                LLSecurityFactorSet hSFSet = getSeInfo(sql);
                if (hSFSet.size() > 0) {
                    double supgetrate = hSFSet.get(1).getGetRate();
                    downlimit = uplimit;
                    uplimit = Arith.round(PL / supgetrate, 3);
                    tmphamnt1 = PL / supgetrate * (1 - supgetrate);
                    tmphamnt1 = Arith.round(Arith.round(tmphamnt1,3), 2);
                    mSeReceiptSchema.setHighAmnt1(tmphamnt1);
                    fillSecuSpan("3",downlimit,uplimit,supgetrate,PL,tmphamnt1);
                    System.out.println("上海高段" + tmphamnt1);
                }
            }

            if (tSFSet != null &&tSFSet.size() != 0) {

                double spangetrate = tSFSet.get(i).getGetRate();
                MidSpan = Arith.round(PSI / spangetrate * (1 - spangetrate), 3);
                MidSpan = Arith.round(MidSpan, 2);
                uplimit = tpeakline;
                downlimit = SP1 - MidSpan - tmphamnt1;
                downlimit = Arith.round(downlimit, 2);
                if (downlimit < 0) {
                    downlimit = 0;
                    backMessage += "自付一与社保分段计算结果无法吻合,"
                            +"推算的自负一大于帐单自负一，请检查帐单或调整帐单统筹金额";
                }
                fillSecuSpan("2",downlimit,uplimit,spangetrate,PSI,MidSpan);
                fillSecuSpan("1",0,downlimit,0,0,downlimit);
            }
            mSeReceiptSchema.setMidAmnt(MidSpan);
            mSeReceiptSchema.setGetLimit(downlimit);
            mSeReceiptSchema.setLowAmnt(downlimit);
        } else { //上海门诊
            double sDoorPay = 0.0; //小额门急诊保险金
            double bDoorPay = 0.0; //大额门急诊保险金
            double cDoorPay = 0.0; //门急诊保险金
            double DGL = 0.0; //起付线
            cDoorPay = SP1;
            if (mFeeMainSchema.getInsuredStat().equals("1")) { //在职
                String sql = " and securitytype='2001' and LevelCode = '"
                             + tLevelCode +"' and insuredstat='"+ insustat + "'";
                tSFSet = getSeInfo(sql);
                DGL = tSFSet.get(1).getGetLimit();
                if (SP1 <= DGL) {
                    sDoorPay = SP1;
                    bDoorPay = 0;
                } else {
                    if (PSI < 0.01) {
                        sDoorPay = 0;
                        bDoorPay = 0;
                    } else {
                        sDoorPay = DGL;
                        bDoorPay = SP1 - DGL;
                    }
                }
            } else {
                if (PSI < 0.01) {
                    sDoorPay = SP1;
                    bDoorPay = 0;
                } else {
                    sDoorPay = 0;
                    bDoorPay = SP1;
                }
                DGL = sDoorPay;
            }
            mSeReceiptSchema.setSmallDoorPay(sDoorPay);
            mSeReceiptSchema.setHighDoorAmnt(bDoorPay);
            mSeReceiptSchema.setEmergencyPay(cDoorPay);
            mSeReceiptSchema.setGetLimit(DGL);
        }
        return true;
    }

    /**
     * 云南社保账单理算
     * @return boolean
     */
    private boolean calYNSecuDuty() {
        System.out.println("......YN or TJ");
        String feetype = mFeeMainSchema.getFeeType();
        if (feetype.equals("2")||feetype.equals("3")) {
            double lowamnt = mSeReceiptSchema.getLowAmnt();
            fillSecuSpan("1", 0, lowamnt, 0, 0, lowamnt);
            if(mFeeMainSchema.getHosGrade()==null || "".equals(mFeeMainSchema.getHosGrade())){
            	mFeeMainSchema.setHosGrade("00");
            }
            String tLevel = mFeeMainSchema.getHosGrade().substring(0, 1);
            String tisustat = mFeeMainSchema.getInsuredStat()+"";
            String tdistrict = mFeeMainSchema.getHosDistrict() + "";
            String sqlpart = "";
            if (tLevel.equals("1")&&!tdistrict.equals("")) {
                sqlpart = " and standbyflag2='" + tdistrict + "'";
            }
            if (mFeeMainSchema.getAge() >= 70 &&tisustat.equals("2") &&
                MngCom.equals("8653"))
                tisustat = "3";
            LLSecurityFactorSchema tSFSchema = new LLSecurityFactorSchema();
            double ppeakline = 0;
            double pgetrate = 0;
            //基本医疗保险统筹基金支付
            String sql = " and securitytype='1001' and LevelCode = '"
                         + tLevel+ "' and insuredstat='" + tisustat
                         +"' and getrate>0 " + sqlpart +" order by downlimit";
            LLSecurityFactorSet tSFSet = getSeInfo(sql);
            if (tSFSet == null || tSFSet.size() <= 0) {
                backMessage += "<br>查询社保信息失败，请核实客户及医院信息是否齐备！";
            }else{
                tSFSchema = tSFSet.get(1);
                ppeakline = tSFSchema.getPeakLine();
                pgetrate = tSFSchema.getGetRate();
            }
            double PSI = mSeReceiptSchema.getPlanFee();
            double midamnt = mSeReceiptSchema.getMidAmnt();
            fillSecuSpan("2", lowamnt, ppeakline, pgetrate, PSI, midamnt);
            double PL = mSeReceiptSchema.getSupInHosFee();
            double highamnt = mSeReceiptSchema.getHighAmnt1();
            tSFSet = getSeInfo(" and securitytype='3001' ");
            double hpeakline = 0;
            double hgetrate = 0;
            if (tSFSet.size() > 0) {
                tSFSchema = tSFSet.get(1);
                hpeakline = tSFSchema.getPeakLine();
                hgetrate = tSFSchema.getGetRate();
            }
            fillSecuSpan("3",ppeakline,hpeakline,hgetrate,PL,highamnt);
            fillSecuSpan("4",hpeakline,0,0,0,mSeReceiptSchema.getSuperAmnt());
        }
        return true;
    }

    /**
     * 深圳社保账单理算(目前只有住院)
     * @return boolean
     */
    private boolean calSZSecuDuty() {
        double PSI = mSeReceiptSchema.getPlanFee();
        double PL = mSeReceiptSchema.getSupInHosFee();
        double SP1 = mSeReceiptSchema.getSelfPay1();
        double SupA = mSeReceiptSchema.getSuperAmnt();
        String feetype = mFeeMainSchema.getFeeType();
        if (feetype.equals("2")||feetype.equals("3")) {
            //统筹的社保政策
            String sqlpart = " and securitytype='1001' and insuredstat='"
                         + mFeeMainSchema.getInsuredStat() + "'";
            LLSecurityFactorSet tSFSet = getSeInfo(sqlpart);
            if (tSFSet == null || tSFSet.size() <= 0) {
                backMessage = "查询统筹社保信息失败，请核实客户及医院信息是否齐备！";
                CError.buildErr(this,backMessage);
                return false;
            }
            LLSecurityFactorSchema tSFSchema = tSFSet.get(1);
            double pgetrate = tSFSchema.getGetRate();
            double amidamnt = PSI / pgetrate * (1 - pgetrate);
            amidamnt = Arith.round(amidamnt, 2);
            double ahighamnt = SP1 - amidamnt;
            ahighamnt = Arith.round(ahighamnt, 2);
            mSeReceiptSchema.setMidAmnt(amidamnt);
            mSeReceiptSchema.setHighAmnt1(ahighamnt);
            double puplimit = tSFSchema.getUpLimit();
            //大额医疗的社保政策
            if (amidamnt > 0) {
                fillSecuSpan("1", 0, puplimit,pgetrate, PSI, amidamnt);
            }
            if (ahighamnt > 0||SupA>0) {
                sqlpart = " and securitytype='3001' and insuredstat='"
                          + mFeeMainSchema.getInsuredStat() +"'";
                tSFSet = getSeInfo(sqlpart);
                if (tSFSet.size() <= 0) {
                    backMessage = "查询大额医疗社保信息失败！";
                    CError.buildErr(this,backMessage);
                    return false;
                }
                tSFSchema = tSFSet.get(1);
                double hgetrate = tSFSchema.getGetRate();
                double huplimit = tSFSchema.getUpLimit();
                if (SupA > 0) {
                    fillSecuSpan("3", huplimit, 0,0, 0, SupA);
                    ahighamnt -= SupA;
                }
                fillSecuSpan("2", puplimit, huplimit,hgetrate, PL, ahighamnt);
            }
            double selfpay1 = mSeReceiptSchema.getSelfPay1() +
                              mSeReceiptSchema.getSuperAmnt();
            mSeReceiptSchema.setSelfPay1(selfpay1);
        }
        return true;
    }

    private LLSecurityFactorSet getSeInfo(String sqlpart){
        LLSecurityFactorSet aSFset = new LLSecurityFactorSet();
        LLSecurityFactorDB tSFDB = new LLSecurityFactorDB();
        String sql = "select * from llsecurityfactor where comcode='"+MngCom
                     +"' "+sqlpart;
//        int num = sqlpart.length;
//        for (int i=0;i<sqlpart.length;i++){
//            sql += " and "+sqlpart[i][0]+"='"+sqlpart[i][1]+"' ";
//        }
        aSFset = tSFDB.executeQuery(sql);
        return aSFset;
    }

    private boolean fillSecuSpan(String sn, double dlimit, double ulimit,
                                 double payrate, double pfee, double spay) {
        LLCaseCommon tcommon = new LLCaseCommon();
        LLSecuDetailSchema tSDetailSchema = new LLSecuDetailSchema();
        tSDetailSchema.setDownLimit(dlimit);
        tSDetailSchema.setUpLimit(ulimit);
        tSDetailSchema.setPlanPayRate(payrate);
        tSDetailSchema.setSerialNo(sn);
        tSDetailSchema.setPlanFee(Arith.round(pfee,2));
        tSDetailSchema.setSelfPay(Arith.round(spay,2));
        tcommon.fillDefaultField(tSDetailSchema,mfieldvalue);
        tSDetailSchema.setMainFeeNo(mFeeMainSchema.getMainFeeNo());
        mLLSecuDetailSet.add(tSDetailSchema);
        return true;
    }

    /**
     * 得到处理后的结果数据
     * @return boolean
     */
    public VData getResult() {
        return this.mResult;
    }

}
