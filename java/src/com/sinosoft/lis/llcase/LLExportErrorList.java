package com.sinosoft.lis.llcase;

import java.io.File;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.ListTable;

public class LLExportErrorList {

	private WriteToExcel mWriteToExcel = null;

	private String mStrArr[][] = null;

	public LLExportErrorList() {
	}

	public boolean createDocument(String aFileName) {
		try {
			mWriteToExcel = new WriteToExcel("ErrorList" + aFileName + ".xls");
			mWriteToExcel.createExcelFile();
			String[] sheetName = { "sheet1" };
			mWriteToExcel.addSheet(sheetName);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return true;
	}

	public boolean addListTable(ListTable aListTable, String[] aTitle,
			String[] aType) {
		if (aTitle.length != aType.length) {
			System.out.println("标题与类型列数不一致！");
			return false;
		}

		int tRowNum = aListTable.size() + 1;
		int tColNum = aTitle.length;

		mStrArr = new String[tRowNum][tColNum];

		for (int i = 0; i < aTitle.length; i++) {
			mStrArr[0][i] = aTitle[i];
		}

		for (int j = 0; j < aListTable.size(); j++) {
			int tCOL = aListTable.get(j).length;
			for (int m = 0; m < tCOL; m++) {

				String tDataType = "String";

				if (aType[m] != null && aType[m].length() > 0) {
					tDataType = aType[m];
				}
				String tText = aListTable.getValue(m, j);

				//最后一列是错误原因
				if (m + 1 == tCOL) {
					tDataType = "String";
				}

				if ("DateTime".equals(tDataType)) {
					tDataType = "String";
					tText = FormatDate(tText);
				} else if ("Number".equals(tDataType)) {
					try {
						Double.parseDouble(tText);
					} catch (Exception ex) {
						tText = "";
					}
				}
				mStrArr[j + 1][m] = tText;
			}
		}
		mWriteToExcel.setData(0, mStrArr);
		return true;
	}

	/**
	 * 输出.xls文件
	 * 
	 * @param aPathName
	 * @param aFileName
	 *            不需要后缀
	 */
	public void outputDocumentToFile(String aPathName, String aFileName) {
		try {
			System.out.println("开始生成错误清单-LLExportErrorList");
			String tFile = aPathName + "ErrorList" + aFileName + ".xls";

			File f = new File(tFile);
			f.delete();
			mWriteToExcel.write(aPathName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String FormatDate(String aDate) {
		String rDate = "";
		rDate += aDate;

		if (!PubFun.checkDateForm(rDate)) {
			int days = 0;
			try {
				days = Integer.parseInt(aDate) - 2;
			} catch (Exception ex) {
				return "";
			}
			rDate = PubFun.calDate("1900-1-1", days, "D", null);
		}
		return rDate;
	}
}
