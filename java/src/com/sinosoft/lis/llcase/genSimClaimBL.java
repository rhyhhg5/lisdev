package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: genSimClaimBL </p>
 * <p>Description: genSimClaimBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @author:Xx
 * @CreateDate：2006-9-28 13:58:55
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Date;
import java.lang.reflect.Method;

public class genSimClaimBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
//    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private LLSubReportSchema mLLSubReportSchema = new LLSubReportSchema();
    private LLCaseRelaSchema mLLCaseRelaSchema = new LLCaseRelaSchema();
    private LLFeeMainSchema mLLFeeMainSchema = new LLFeeMainSchema();
    private LLSecurityReceiptSchema mLLSecurityReceiptSchema = new
            LLSecurityReceiptSchema();
    //社保明细
    private LLFeeOtherItemSet mLLFeeOtherItemSet = new LLFeeOtherItemSet();
    
    private LLCaseCureSet mLLCaseCureSet = new LLCaseCureSet();
    private LLAppClaimReasonSchema mLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
    private LLClaimDetailSet mLLClaimDetailSet = new LLClaimDetailSet();
    private LLClaimSchema  mLLClaimSchema = new LLClaimSchema();
    private String aCaseNo="";
    private String aRgtNo="";
    private String aSubRptNo = "";
    private String aCaseRelaNo = "";
    private String aCureNo = "";
    private String aMainFeeNo = "";
    private String aClmNo = "";
    private String aGetDutyCode = "";
    private String aGrpContNo = "";
    private String aDate = "";
    private String aTime = "";
    private String oDate = "";
    private String oTime = "";

    public genSimClaimBL() {
    }

    public static void main(String[] args) {
        String strOperate = "INSERT||MAIN";
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
        LLSecurityReceiptSchema tLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
        LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
        LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
        VData tVData = new VData();
        genSimClaimBL tgenSimClaimBL = new genSimClaimBL();

        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86110000";
        mGlobalInput.ComCode = "86110000";
        mGlobalInput.Operator = "bcm005";

        tLLCaseSchema.setRgtType("1");
        tLLRegisterSchema.setRgtNo("P0000060522000001");

        /**************************************************/
        tLLCaseSchema.setRgtState("01"); //案件状态
        tLLCaseSchema.setCaseNo("C0000060525000006");
        tLLCaseSchema.setCustomerName("刘欣");
        tLLCaseSchema.setCustomerNo("000310000");
        tLLCaseSchema.setPostalAddress("");
        tLLCaseSchema.setPhone("");
        tLLCaseSchema.setDeathDate("");
        tLLCaseSchema.setCustBirthday("1941-03-13");
        tLLCaseSchema.setCustomerSex("0");
        tLLCaseSchema.setIDType("0");
        tLLCaseSchema.setIDNo("110105194103131822");
        tLLCaseSchema.setCustState("04");
        tLLCaseSchema.setDeathDate("2005-5-1");
        tLLFeeMainSchema.setRgtNo("P0000060522000001");
        tLLFeeMainSchema.setCaseNo("");
        tLLFeeMainSchema.setCustomerNo("000310000");
        tLLFeeMainSchema.setCustomerSex("0");
        tLLFeeMainSchema.setHospitalCode("");
        tLLFeeMainSchema.setHospitalName("");
        tLLFeeMainSchema.setHosAtti("");
        tLLFeeMainSchema.setReceiptNo("53000");
        tLLFeeMainSchema.setFeeType("31");
        tLLFeeMainSchema.setFeeAtti("1");
        tLLFeeMainSchema.setFeeDate("2006-5-1");
        tLLFeeMainSchema.setHospStartDate("2006-5-1");
        tLLFeeMainSchema.setHospEndDate("2006-5-11");
        tLLFeeMainSchema.setRealHospDate("");
        tLLFeeMainSchema.setInHosNo("");
        tLLFeeMainSchema.setSecurityNo("");
        tLLFeeMainSchema.setInsuredStat("");
        tLLSecurityReceiptSchema.setApplyAmnt("300");
        tLLSecurityReceiptSchema.setSelfAmnt("2");
        tLLSecurityReceiptSchema.setSelfPay2("2");
        tLLSecurityReceiptSchema.setGetLimit("2");
        tLLSecurityReceiptSchema.setMidAmnt("2");
        tLLSecurityReceiptSchema.setPlanFee("2");
        tLLSecurityReceiptSchema.setHighAmnt2("22");
        tLLSecurityReceiptSchema.setSupInHosFee("2");
        tLLSecurityReceiptSchema.setHighAmnt1("222");
        tLLSecurityReceiptSchema.setOfficialSubsidy("");
        LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
        tLLAppClaimReasonSchema.setReasonCode("02"); //原因代码
        tLLAppClaimReasonSchema.setReason("住院"); //申请原因
        tLLAppClaimReasonSchema.setCustomerNo("000310000");
        tLLAppClaimReasonSchema.setReasonType("0");

        tLLCaseCureSchema.setDiseaseCode("333");
        tLLCaseCureSchema.setDiseaseName("对当地");
        tLLCaseOpTimeSchema.setRgtState("01");
        tLLCaseOpTimeSchema.setStartDate("2005-9-15");
        tLLCaseOpTimeSchema.setStartTime("20:30:30");
        tVData.add(tLLCaseSchema);
        tVData.add(tLLRegisterSchema);
        tVData.add(tLLFeeMainSchema);
        tVData.add(tLLSecurityReceiptSchema);
        tVData.add(tLLAppClaimReasonSchema);
        tVData.add(tLLCaseCureSchema);
        tVData.add(tLLCaseOpTimeSchema);

        tVData.add(mGlobalInput);
        tgenSimClaimBL.submitData(tVData, strOperate);
        tVData.clear();
        tVData = tgenSimClaimBL.getResult();
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "genCaseInfoBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败genCaseInfoBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
            System.out.println("Start ClientRegisterBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LDDrugBL";
                tError.functionName = "ClientRegisterBL";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("End SimpleClaimBL Submit...");
            mInputData = null;

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("--Star GetInputData--");
        this.mLLCaseSchema.setSchema((LLCaseSchema) cInputData.
                                     getObjectByObjectName("LLCaseSchema", 0));
        aRgtNo = mLLCaseSchema.getRgtNo();
        //为减少传的变量数,将GrpContNo暂存在AccidentSit中,将给付责任编码暂存在Phone里
        aGrpContNo = mLLCaseSchema.getAccidentSite();
        aGetDutyCode = mLLCaseSchema.getPhone();
        mLLCaseSchema.setAccidentSite("");
        mLLCaseSchema.setPhone("");
        this.mLLFeeMainSchema.setSchema((LLFeeMainSchema) cInputData.
                getObjectByObjectName("LLFeeMainSchema",0));
        this.mLLSecurityReceiptSchema.setSchema((LLSecurityReceiptSchema) cInputData.
                getObjectByObjectName("LLSecurityReceiptSchema",0));
        this.mLLCaseCureSet = (LLCaseCureSet) cInputData.
                                     getObjectByObjectName("LLCaseCureSet", 0);
        this.mLLAppClaimReasonSchema.setSchema((LLAppClaimReasonSchema) cInputData.
                                     getObjectByObjectName("LLAppClaimReasonSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        if (mGlobalInput.ManageCom.length() == 2)
            mGlobalInput.ManageCom = mGlobalInput.ManageCom + "000000";
        if (mGlobalInput.ManageCom.length() == 4)
            mGlobalInput.ManageCom = mGlobalInput.ManageCom + "0000";
        aDate = PubFun.getCurrentDate();
        aTime = PubFun.getCurrentTime();
        oDate = PubFun.getCurrentDate();
        oTime = PubFun.getCurrentTime();
        if (mLLCaseSchema.getCaseNo() != null) {
            LLCaseDB tLLCaseDB = new LLCaseDB();
            tLLCaseDB.setCaseNo(mLLCaseSchema.getCaseNo());
            if (tLLCaseDB.getInfo()) {
                if (!tLLCaseDB.getRgtState().equals("01")
                    && !tLLCaseDB.getRgtState().equals("02")
                    && !tLLCaseDB.getRgtState().equals("03")
                    && !tLLCaseDB.getRgtState().equals("04")
                    && !tLLCaseDB.getRgtState().equals("08")){
                    CError.buildErr(this, "该状态不允许修改案件信息！");
                    return false;
                }
                mOperate = "UPDATE||MAIN";
                oDate = tLLCaseDB.getMakeDate();
                oTime = tLLCaseDB.getMakeTime();
            }
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("---Star dealData---mOperate---" + mOperate);
        if (this.mOperate.equals("INSERT||MAIN")) {
            if(!insertData())
                return false;
        }

        if (this.mOperate.equals("UPDATE||MAIN")) {
            if(!updateData())
                return false;
        }

        if(!getLLClaimInfo()){
            CError.buildErr(this, "创建赔案失败！");
            return false;
        }
        if(!calClaim()){
            CError.buildErr(this, "理算失败！");
            return false;
        }

        return true;
    }

    private boolean insertData(){
        System.out.println("---Star Insert case---");
        /*生成理赔号*/
        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        VData tno = new VData();
        tno.add(0,"1");
        tno.add(1,aRgtNo);
        tno.add(2,"STORE");
        aCaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit,tno);
        if(aCaseNo.length()<17){
            aCaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit);
        }
        aSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);
        aCaseRelaNo = PubFun1.CreateMaxNo("CASERELANO", tLimit);
        aMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO",tLimit);
        aClmNo = PubFun1.CreateMaxNo("CLMNO", tLimit);
        if(!getCaseInfo())
            return false;

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        /*生成理赔号*/
        System.out.println("---Star update case---");
        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        aCaseNo = mLLCaseSchema.getCaseNo();
        LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
        tLLCaseRelaDB.setCaseNo(aCaseNo);
        LLCaseRelaSet tLLCaseRelaSet = tLLCaseRelaDB.query();
        if (tLLCaseRelaSet.size()>0){
            aCaseRelaNo = tLLCaseRelaSet.get(1).getCaseRelaNo();
            aSubRptNo = tLLCaseRelaSet.get(1).getSubRptNo();
        }else{
            aSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);
            aCaseRelaNo = PubFun1.CreateMaxNo("CASERELANO", tLimit);
        }
        LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
        tLLFeeMainDB.setCaseNo(aCaseNo);
        LLFeeMainSet tLLFeeMainSet = tLLFeeMainDB.query();
        if(tLLFeeMainSet.size()>0)
            aMainFeeNo = tLLFeeMainSet.get(1).getMainFeeNo();
        else
            aMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO",tLimit);
        LLCaseCureDB tLLCaseCureDB = new LLCaseCureDB();
        tLLCaseCureDB.setCaseNo(aCaseNo);
        LLCaseCureSet tLLCaseCureSet = tLLCaseCureDB.query();
        map.put(tLLCaseCureSet,"DELETE");

        if(!getCaseInfo())
            return false;

        return true;
    }

    private boolean getCaseInfo() {
        int CustAge = 0;
        try{
            CustAge = calAge();
        }catch(Exception ex){
            System.out.println("计算年龄错误,生日不是有效的日期格式");
        }
        mLLCaseSchema.setCustomerAge(CustAge);
        mLLCaseSchema.setRgtNo(aRgtNo);
        mLLCaseSchema.setCaseNo(aCaseNo);
        mLLCaseSchema.setHandler(mGlobalInput.Operator);
        mLLCaseSchema.setDealer(mGlobalInput.Operator);
        mLLCaseSchema.setRgtDate(PubFun.getCurrentDate());
        fillDefaultField(mLLCaseSchema);

        if(!genSubReport()){
            CError.buildErr(this, "生成事件失败！");
            return false;
        }

        mLLAppClaimReasonSchema.setRgtNo(aRgtNo);
        mLLAppClaimReasonSchema.setCaseNo(aCaseNo);
        fillDefaultField(mLLAppClaimReasonSchema);

        mLLFeeMainSchema.setAge(CustAge);
        mLLFeeMainSchema.setMainFeeNo(aMainFeeNo);
        mLLFeeMainSchema.setCustomerName(mLLCaseSchema.getCustomerName());
        mLLFeeMainSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
        mLLFeeMainSchema.setRgtNo(aRgtNo);
        mLLFeeMainSchema.setCaseNo(aCaseNo);
        mLLFeeMainSchema.setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());
        fillDefaultField(mLLFeeMainSchema);

        if(!calSecu()){
            CError.buildErr(this, "生成社保帐单失败！");
            return false;
        }
        mLLSecurityReceiptSchema.setMainFeeNo(aMainFeeNo);
        mLLSecurityReceiptSchema.setFeeDetailNo(aMainFeeNo);
        mLLSecurityReceiptSchema.setRgtNo(aRgtNo);
        mLLSecurityReceiptSchema.setCaseNo(aCaseNo);
        fillDefaultField(mLLSecurityReceiptSchema);
        
        if (!saveFeeItem()) {
        	CError.buildErr(this, "社保明细帐单生成失败！");
            return false;
        }

        for(int i=1;i<=mLLCaseCureSet.size();i++){
//            if (mLLCaseCureSet.get(i).getDiseaseCode() == null ||
//                mLLCaseCureSet.get(i).getDiseaseCode().equals(""))
//                continue;
            String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
            aCureNo = PubFun1.CreateMaxNo("CURENO", tLimit);
            mLLCaseCureSet.get(i).setSerialNo(aCureNo);
            mLLCaseCureSet.get(i).setCustomerNo(mLLCaseSchema.getCustomerNo());
            mLLCaseCureSet.get(i).setCustomerName(mLLCaseSchema.getCustomerName());
            mLLCaseCureSet.get(i).setDiagnose(mLLCaseCureSet.get(i).getDiseaseName());
            mLLCaseCureSet.get(i).setHospitalCode(mLLFeeMainSchema.getHospitalCode());
            mLLCaseCureSet.get(i).setHospitalName(mLLFeeMainSchema.getHospitalName());
            mLLCaseCureSet.get(i).setCaseNo(aCaseNo);
            mLLCaseCureSet.get(i).setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());
            fillDefaultField(mLLCaseCureSet.get(i));
        }
        map.put(mLLCaseSchema, "DELETE&INSERT");
        map.put(mLLFeeMainSchema, "DELETE&INSERT");
        map.put(mLLSecurityReceiptSchema, "DELETE&INSERT");
        map.put(mLLCaseCureSet,"DELETE&INSERT");
        map.put(mLLAppClaimReasonSchema,"DELETE&INSERT");
        map.put(mLLFeeOtherItemSet, "DELETE&INSERT");
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 查询赔案信息
     * @return boolean
     */
    private boolean getLLClaimInfo() {
        //创建赔案信息
        this.mLLClaimSchema = new LLClaimSchema();
        this.mLLClaimSchema.setClmNo(aClmNo);

        String tGetDutyKind = "000000";
        mLLClaimSchema.setGetDutyKind(tGetDutyKind);
        mLLClaimSchema.setCaseNo(aCaseNo);
        mLLClaimSchema.setClmState("1"); //结算
        mLLClaimSchema.setRgtNo(aRgtNo);
        mLLClaimSchema.setClmUWer(mGlobalInput.Operator);
        mLLClaimSchema.setCheckType("0");
        fillDefaultField(mLLClaimSchema);

        map.put(mLLClaimSchema, "DELETE&INSERT");
        //先删除已经计算过的
        String SQL1 = "delete from LLClaimPolicy where caseno='"+aCaseNo+"'";
        String SQL2 = "delete from LLClaimDetail where caseno='"+aCaseNo+"'";
        map.put(SQL1, "DELETE");
        map.put(SQL2, "DELETE");
        return true;
    }

    /**
     * 生成事件及与案件的关联
     * @return boolean
     */
    private boolean genSubReport() {
        //立案分案,即事件
        mLLSubReportSchema.setSubRptNo(aSubRptNo);
        mLLSubReportSchema.setCustomerName(mLLCaseSchema.getCustomerName());
        mLLSubReportSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
        mLLSubReportSchema.setAccDate(mLLCaseSchema.getAccidentDate());
        mLLSubReportSchema.setInHospitalDate(mLLFeeMainSchema.getHospStartDate());
        mLLSubReportSchema.setOutHospitalDate(mLLFeeMainSchema.getHospEndDate());
        if(mLLCaseCureSet.size()>0)
            mLLSubReportSchema.setAccDesc(mLLCaseCureSet.get(1).getDiseaseName());
        fillDefaultField(mLLSubReportSchema);

        mLLCaseRelaSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        mLLCaseRelaSchema.setSubRptNo(aSubRptNo);
        mLLCaseRelaSchema.setCaseRelaNo(aCaseRelaNo);
        map.put(mLLSubReportSchema,"DELETE&INSERT");
        map.put(mLLCaseRelaSchema,"DELETE&INSERT");
        return true;
    }

    /**
     * 校验计算社保信息
     * @return boolean
     */
    private boolean calSecu(){
        double aSelfPay1 = mLLSecurityReceiptSchema.getGetLimit()
                           +mLLSecurityReceiptSchema.getMidAmnt()
                           +mLLSecurityReceiptSchema.getHighAmnt1();
//                           +mLLSecurityReceiptSchema.getHighAmnt2();
        mLLSecurityReceiptSchema.setSelfPay1(aSelfPay1);
//        double aFeeIS = mLLSecurityReceiptSchema.getPlanFee()
//                        +mLLSecurityReceiptSchema.getSupInHosFee()
//                        +aSelfPay1;
//        double aFeeOS = mLLSecurityReceiptSchema.getSelfPay2()
//                        +mLLSecurityReceiptSchema.getSelfAmnt();
//        mLLSecurityReceiptSchema.setFeeInSecu(aFeeIS);
//        mLLSecurityReceiptSchema.setFeeOutSecu(aFeeOS);
        return true;
    }

    /**
     * 过滤要赔付的责任
     * @return boolean
     */
    private boolean getClaimDuty(LCPolSchema tpolschema){
        String aCaseRelaNo = mLLCaseRelaSchema.getCaseRelaNo();
        SynLCLBGetBL aSynLCLBGetBL = new SynLCLBGetBL();
        aSynLCLBGetBL.setPolNo(tpolschema.getPolNo());
        LCGetSet tLCGetSet = aSynLCLBGetBL.query();
        for(int i=1;i<=tLCGetSet.size();i++){
            LLClaimDetailSchema row = new LLClaimDetailSchema();
            LCGetSchema lcGet = tLCGetSet.get(i);
            row.setCaseNo(aCaseNo);
            row.setRgtNo(aRgtNo);
            row.setClmNo(this.aClmNo);
            row.setCaseRelaNo(aCaseRelaNo);
            row.setGrpContNo(lcGet.getGrpContNo());       /* 集体合同号 */
            row.setGrpPolNo(tpolschema.getGrpPolNo());   /* 集体保单号 */
            row.setContNo(lcGet.getContNo());             /* 个单合同号 */
            row.setPolNo(lcGet.getPolNo());               /* 保单号 */
            row.setDutyCode(lcGet.getDutyCode());         /* 责任编码 */
            row.setGetDutyCode(lcGet.getGetDutyCode());   /* 给付责任编码 */
            String tsql = "select distinct getdutykind from lmdutygetclm where getdutycode='"
                          +lcGet.getGetDutyCode()+"'";
            ExeSQL texesql = new ExeSQL();
            SSRS tss = texesql.execSQL(tsql);
            if(tss.getMaxRow()<=0){
                CError.buildErr(this,"查询责任类型失败！");
                return false;
            }
            row.setGetDutyKind(tss.GetText(1,1));                    /* 给付责任类型 */
            row.setKindCode(tpolschema.getKindCode());    /* 险类代码 */
            row.setRiskCode(tpolschema.getRiskCode());    /* 险种代码 */
            row.setRiskVer(tpolschema.getRiskVersion());  /* 险种版本号 */
            row.setPolMngCom(tpolschema.getManageCom());  /* 保单管理机构 */
            row.setTabFeeMoney(0);
            row.setSelfGiveAmnt(0);
            row.setPreGiveAmnt(0);
            row.setRefuseAmnt(0);
            row.setDeclineAmnt(0);
//            row.setStatType(dutyGetClm.getStatType());
            row.setSaleChnl(tpolschema.getSaleChnl());
            row.setAgentCode(tpolschema.getAgentCode());
            row.setAgentGroup(tpolschema.getAgentGroup());
//            row.setOutDutyAmnt(t_getlimit);
//            row.setOutDutyRate(mLCDutySchema.getGetRate());
            row.setOverAmnt(0);
//            row.setClaimMoney(tcalmny);
//            row.setStandPay(Arith.round(mSum_gf_je, 2));
//            row.setRealPay(mReal_gf_je);
            fillDefaultField(row);
            mLLClaimDetailSet.add(row);
        }
        return true;
    }

    /**
     * 案件保单信息
     * @return boolean
     */
    private boolean fillCasePolicy() {
        String aCaseNo = mLLCaseSchema.getCaseNo();
        String aCaseRelaNo = mLLCaseRelaSchema.getCaseRelaNo();
        String aRgtNo = mLLCaseSchema.getRgtNo();
        LLClaimPolicySet tclaimpolicyset = new LLClaimPolicySet();
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setInsuredNo(mLLCaseSchema.getCustomerNo());
        tLCPolBL.setGrpContNo(aGrpContNo);
        LCPolSet tLCPolSet = new LCPolSet();
        tLCPolSet = tLCPolBL.query();
        int n = tLCPolSet.size();
        String strAccidentDate=mLLCaseSchema.getAccidentDate();
        if (strAccidentDate == null || strAccidentDate.equals("")) {
            strAccidentDate = mLLFeeMainSchema.getFeeDate();
        }
        try {
            for (int i = 1; i <= n; i++) {
                //处理保单状态
                String opolstate = "" + tLCPolSet.get(i).getPolState();
                String npolstate = "";
               if (opolstate.equals("null") || opolstate.equals("")) {
                   npolstate = "0400";
               } else {
                   if(opolstate.substring(0,2).equals("04")){
                       npolstate = "0400"+opolstate.substring(4);
                   }else{
                       if (opolstate.length() < 4) {
                           npolstate = "0400" + opolstate;
                       } else {
                           npolstate = "0400" + opolstate.substring(0, 4);
                       }
                   }
               }
               tLCPolSet.get(i).setPolState(npolstate);
                LCPolSchema tLCPolSchema = tLCPolSet.get(i);
                if (!(strAccidentDate == null || strAccidentDate.equals(""))) {
                    if (!(CaseFunPub.checkDate(tLCPolSchema.getCValiDate(),
                                               strAccidentDate))) {
                        this.mErrors.copyAllErrors(tLCPolBL.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "genCaseInfoBL";
                        tError.functionName = "fillCasePolicy";
                        tError.errorMessage = "保单号码是" + tLCPolSchema.getPolNo() +
                                              "的出险日期"+ strAccidentDate
                                              +"在保单生效日期"+ tLCPolSchema.getCValiDate()
                                              +"之前，不能参加理赔！！！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
                LLClaimPolicySchema tcpolicyschema = new LLClaimPolicySchema();
                Reflections trf = new Reflections();
                trf.transFields(tcpolicyschema,tLCPolSchema);
                tcpolicyschema.setCaseNo(aCaseNo);
                tcpolicyschema.setRgtNo(aRgtNo);
                tcpolicyschema.setClmNo(aClmNo);
                tcpolicyschema.setCaseRelaNo(aCaseRelaNo);
                fillDefaultField(tcpolicyschema);
                tcpolicyschema.setGetDutyKind("100");
                if ((tLCPolSchema.getInsuredNo().equals(mLLCaseSchema.getCustomerNo()))) {
                    tcpolicyschema.setCasePolType("0");
                } else{
                    if (tLCPolSchema.getAppntNo().equals(mLLCaseSchema.
                            getCustomerNo())) {
                        tcpolicyschema.setCasePolType("1");
                    }
                }
                tcpolicyschema.setPolType("1"); //正式保单
                tclaimpolicyset.add(tcpolicyschema);
                getClaimDuty(tLCPolSchema);
            }
            String delSql = "delete from llclaimpolicy where caseno='" +
                            aCaseNo + "'";
            map.put(delSql, "DELETE");
            map.put(tclaimpolicyset, "INSERT");
//            map.put(tLCPolSet, "UPDATE");
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            CError.buildErr(this, "准备数据出错:" + ex.getMessage());
            return false;
        }
    }

    private boolean calClaim(){
        String aCaseNo = mLLCaseSchema.getCaseNo();
        String aCaseRelaNo = mLLCaseRelaSchema.getCaseRelaNo();
        String aRgtNo = mLLCaseSchema.getRgtNo();
        SynLCLBGetBL aSynLCLBGetBL = new SynLCLBGetBL();
        aSynLCLBGetBL.setInsuredNo(mLLCaseSchema.getCustomerNo());
        aSynLCLBGetBL.setGetDutyCode(aGetDutyCode);
        aSynLCLBGetBL.setGrpContNo(aGrpContNo);
        LCGetSet tLCGetSet = aSynLCLBGetBL.query();
        if(tLCGetSet.size()<=0){
            CError.buildErr(this,"被保人没有承保给付责任"+aGetDutyCode);
            return false;
        }
        LCPolBL tLCPolBL = new LCPolBL();
        String aPolNo = tLCGetSet.get(1).getPolNo();
        tLCPolBL.setPolNo(aPolNo);
        if(!tLCPolBL.getInfo()){
            CError.buildErr(this,"保单表数据与责任表数据不对应，请提交契约运维，核实承保数据是否有误。");
            return false;
        }
        LCPolSchema tpolSchema = tLCPolBL.getSchema();
        String strAccidentDate=mLLCaseSchema.getAccidentDate();
        if (strAccidentDate == null || strAccidentDate.equals("")) {
            strAccidentDate = mLLFeeMainSchema.getFeeDate();
        }
        if (!(strAccidentDate == null || strAccidentDate.equals(""))) {
            if (!(CaseFunPub.checkDate(tpolSchema.getCValiDate(),
                                       strAccidentDate))) {
                String errMessage = "保单号码是" + tpolSchema.getPolNo() +
                                    "的出险日期" + strAccidentDate
                                    + "在保单生效日期" + tpolSchema.getCValiDate()
                                    + "之前，不能参加理赔！！！";
                CError.buildErr(this, errMessage);
//                return false;
            }
        }
        LMDutyGetClmDB dutygetclmDB = new LMDutyGetClmDB();
        dutygetclmDB.setGetDutyCode(aGetDutyCode);
        LMDutyGetClmSet tMClmSet = dutygetclmDB.query();
        if(tMClmSet.size()<=0){
            CError.buildErr(this,"查询责任的产品描述失败，请提交契约运维，核实承保数据是否有误。");
            return false;
        }
        LMDutyGetClmSchema dutyGetClm = tMClmSet.get(1);
        LLClaimDetailSchema row = new LLClaimDetailSchema();
        LCGetSchema lcGet = tLCGetSet.get(1);
        row.setCaseNo(aCaseNo);
        row.setRgtNo(aRgtNo);
        row.setClmNo(this.aClmNo);
        row.setCaseRelaNo(aCaseRelaNo);
        row.setGrpContNo(lcGet.getGrpContNo());       /* 集体合同号 */
        row.setGrpPolNo(tpolSchema.getGrpPolNo());   /* 集体保单号 */
        row.setContNo(lcGet.getContNo());             /* 个单合同号 */
        row.setPolNo(lcGet.getPolNo());               /* 保单号 */
        row.setDutyCode(lcGet.getDutyCode());         /* 责任编码 */
        row.setGetDutyCode(lcGet.getGetDutyCode());   /* 给付责任编码 */
        row.setGetDutyKind(dutyGetClm.getGetDutyKind());                    /* 给付责任类型 */
        row.setKindCode(tpolSchema.getKindCode());    /* 险类代码 */
        row.setRiskCode(tpolSchema.getRiskCode());    /* 险种代码 */
        row.setRiskVer(tpolSchema.getRiskVersion());  /* 险种版本号 */
        row.setPolMngCom(tpolSchema.getManageCom());  /* 保单管理机构 */
        row.setTabFeeMoney(mLLFeeMainSchema.getSumFee());
        row.setSelfGiveAmnt(0);
        row.setPreGiveAmnt(0);
        row.setRefuseAmnt(0);
        row.setDeclineAmnt(0);
        row.setStatType(dutyGetClm.getStatType());
        row.setSaleChnl(tpolSchema.getSaleChnl());
        row.setAgentCode(tpolSchema.getAgentCode());
        row.setAgentGroup(tpolSchema.getAgentGroup());
        row.setOutDutyAmnt(0);
        row.setOutDutyRate(0);
        row.setOverAmnt(0);
        double tpay = mLLFeeMainSchema.getRemnant();
        row.setClaimMoney(tpay);
        row.setStandPay(tpay);
        row.setRealPay(tpay);
        fillDefaultField(row);
        map.put(row,"INSERT");
        LLClaimPolicySchema tcpolicyschema = new LLClaimPolicySchema();
        Reflections trf = new Reflections();
        trf.transFields(tcpolicyschema,tpolSchema);
        tcpolicyschema.setCaseNo(aCaseNo);
        tcpolicyschema.setRgtNo(aRgtNo);
        tcpolicyschema.setClmNo(aClmNo);
        tcpolicyschema.setCaseRelaNo(aCaseRelaNo);
        tcpolicyschema.setGetDutyKind(dutyGetClm.getGetDutyKind());
        tcpolicyschema.setCasePolType("1");
        tcpolicyschema.setStandPay(row.getStandPay());
        tcpolicyschema.setRealPay(row.getRealPay());
        fillDefaultField(tcpolicyschema);
        String opolstate = "" + tpolSchema.getPolState();
        String npolstate = "";
       if (opolstate.equals("null") || opolstate.equals("")) {
           npolstate = "0400";
       } else {
           if(opolstate.substring(0,2).equals("04")){
               npolstate = "0400"+opolstate.substring(4);
           }else{
               if (opolstate.length() < 4) {
                   npolstate = "0400" + opolstate;
               } else {
                   npolstate = "0400" + opolstate.substring(0, 4);
               }
           }
       }
       tpolSchema.setPolState(npolstate);
        map.put(tcpolicyschema, "INSERT");
//        map.put(tpolSchema, "UPDATE");
        return true;
    }

    /**
     * 计算年龄的函数
     * @return int
     */
    private int calAge() {
        FDate fDate = new FDate();
        Date Birthday = fDate.getDate(mLLCaseSchema.getCustBirthday());
        String strNow = PubFun.getCurrentDate();
        Date aNow = fDate.getDate(strNow);
        int CustomerAge = PubFun.calInterval(Birthday, aNow, "Y");
        System.out.println("年龄计算函数。。。"+CustomerAge);
        return CustomerAge;
    }

    /**
     * 将Schema中的MakeDate,MakeTime等默认信息填充
     * @param o Object
     */
    private void fillDefaultField(Object o) {
        Class[] c = new Class[1];
        Method m = null;
        String[] odate = {oDate};
        String[] otime = {oTime};
        String[] adate = {aDate};
        String[] atime = {aTime};
        String[] operator = {mGlobalInput.Operator};
        String[] mngcom = {mGlobalInput.ManageCom};
        try {
            c[0] = Class.forName("java.lang.String");
        } catch (Exception ex) {
        }
        try {
            m = o.getClass().getMethod("setMakeDate", c);
        } catch (Exception ex) {
        }
        try {
            m.invoke(o, odate);
        } catch (Exception ex) {
        }
        try {
            m = o.getClass().getMethod("setModifyDate", c);
        } catch (Exception ex) {
        }
        try {
            m.invoke(o, adate);
        } catch (Exception ex) {
        }
        try {
            m = o.getClass().getMethod("setMakeTime", c);
        } catch (Exception ex) {
        }
        try {
            m.invoke(o, otime);
        } catch (Exception ex) {
        }
        try {
            m = o.getClass().getMethod("setModifyTime", c);
        } catch (Exception ex) {
        }
        try {
            m.invoke(o, atime);
        } catch (Exception ex) {
        }
        try {
            m = o.getClass().getMethod("setOperator", c);
        } catch (Exception ex) {
        }
        try {
            m.invoke(o, operator);
        } catch (Exception ex) {
        }
        try {
            m = o.getClass().getMethod("setMngCom", c);
        } catch (Exception ex) {
        }
        try {
            m.invoke(o, mngcom);
        } catch (Exception ex) {
        }
    }
    
    /**
     * 保存社保明细
     * @return boolean
     * @throws Exception
     */
    private boolean saveFeeItem() {
        System.out.println("genCaseInfoBL--saveFeeItem");
        //合计金额
        if (mLLSecurityReceiptSchema.getApplyAmnt() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getApplyAmnt());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("301");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("301");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }

        }

        //统筹基金支付
        if (mLLSecurityReceiptSchema.getPlanFee() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getPlanFee());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("302");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("302");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }
        
        //大额医疗支付
        if (mLLSecurityReceiptSchema.getSupInHosFee() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.getSupInHosFee());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("303");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("303");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        //全自费
        if (mLLSecurityReceiptSchema.getSelfAmnt() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getSelfAmnt());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("305");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("305");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        //自付二
        if (mLLSecurityReceiptSchema.getSelfPay2() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getSelfPay2());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("306");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("306");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        //自付一
        if (mLLSecurityReceiptSchema.getSelfPay1() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getSelfPay1());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("307");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("307");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        //起付线
        if (mLLSecurityReceiptSchema.getGetLimit() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getGetLimit());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("308");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("308");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        //中段
        if (mLLSecurityReceiptSchema.getMidAmnt() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getMidAmnt());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("309");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("309");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        //高段1
        if (mLLSecurityReceiptSchema.getHighAmnt1() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getHighAmnt1());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("310");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("310");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }
        
        //高段2
        if (mLLSecurityReceiptSchema.getHighAmnt2() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getHighAmnt2());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("311");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("311");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }
        
        //超高
        if (mLLSecurityReceiptSchema.getSuperAmnt() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.getSuperAmnt());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("312");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("312");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }
        
        //小额门急诊
        if (mLLSecurityReceiptSchema.getSmallDoorPay() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.getSmallDoorPay());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("314");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("314");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }
        
        //大额门急诊
        if (mLLSecurityReceiptSchema.getHighDoorAmnt() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.getHighDoorAmnt());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("315");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("315");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        return true;
    }

    /**
     * 查询功能
     * @return boolean
     */
    private boolean submitquery() {
        return false;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLLCaseSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "genCaseInfoBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
