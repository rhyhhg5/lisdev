package com.sinosoft.lis.llcase;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author 刘岩松
 * @version 1.0
 * @date 2003-04-09
 * @function show temporary pol info User Interface Layer
 */

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class ShowTemPolInfoUI
{
  public CErrors mErrors=new CErrors();
  private VData mResult = new VData();
  private String strRgtNo;
  private String strInsuredNo;
  private String strMngCom;
  private GlobalInput mG = new GlobalInput();


  public ShowTemPolInfoUI()
  {
  }

  public boolean submitData(VData cInputData, String cOperate)
  {
    try
    {
      if( !getInputData(cInputData) )
      {
        return false;
      }
      VData vData = new VData();

      if( !prepareOutputData(vData) )
      {
        return false;
      }

      ShowTemPolInfoBL tShowTemPolInfoBL = new ShowTemPolInfoBL();
      System.out.println("Start ShowTemPolInfo UI Submit ...");
      if( !tShowTemPolInfoBL.submitData(vData, cOperate) )
      {
        if( tShowTemPolInfoBL.mErrors.needDealError() )
        {
          mErrors.copyAllErrors(tShowTemPolInfoBL.mErrors);
          return false;
        }
        else
        {
          buildError("submitData", "ShowTemPolInfoBL发生错误，但是没有提供详细的出错信息");
          return false;
        }
      }
      else
      {
        mResult = tShowTemPolInfoBL.getResult();
        return true;
      }
    }
    catch(Exception e)
    {
      e.printStackTrace();
      CError cError = new CError();
      cError.moduleName = "ShowTemPolInfoUI";
      cError.functionName = "submit";
      cError.errorMessage = e.toString();
      mErrors.addOneError(cError);
      return false;
    }
  }

  private boolean prepareOutputData(VData vData)
  {
    vData.clear();
    vData.add(strRgtNo);
    vData.add(strInsuredNo);
    vData.add(mG);
    return true;
  }

  private boolean getInputData(VData cInputData)
  {
    strRgtNo = (String)cInputData.get(0);
    strInsuredNo = (String)cInputData.get(1);
    mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    System.out.println("在ShowTemPolInfoUI中得到的数据如下所示：");
    System.out.println("立案号码是===="+strRgtNo);
    System.out.println("客户号码是===="+strInsuredNo);
    System.out.println("管理机构是===="+strMngCom);
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  private void buildError(String szFunc, String szErrMsg)
  {
    CError cError = new CError( );
    cError.moduleName = "ShowTemPolInfoUI";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }

  public static void main(String[] args)
  {
  }
}