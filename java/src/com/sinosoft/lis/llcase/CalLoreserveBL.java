package com.sinosoft.lis.llcase;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 立案业务逻辑保存处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @version 1.0
 */
public class CalLoreserveBL
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData ;
  private VData mResult = new VData();
  private String mOperate;
  private String tPaytoDate;

  //对即将使用的表进行初始化
  //责任准备金表
  private LOReserveSchema mLOReserveSchema = new LOReserveSchema();
  //个人保单表
  private LCPolSchema mLCPolSchema = new LCPolSchema();
  private LBPolSchema mLBPolSchema = new LBPolSchema();
  //个人保单备份表
  private LCPolSet mLCPolSet = new LCPolSet();
  private LBGetSet mLBGetSet = new LBGetSet();
  private LBPolSet mLBPolSet = new LBPolSet();
  private LMDutyGetSet mAllLMDutyGetSet = new LMDutyGetSet();
  private LMDutyGetClmSet mLMDutyGetClmSet = new LMDutyGetClmSet();
  private LCGetSet mLCGetSet ;

  public CalLoreserveBL() {}
  private void PrepareSaveData()
  {
    String tCheckSql="select polno,count(*) from lbpol where appflag='1' group by polno having count(*)>1";
    ExeSQL exesql = new ExeSQL();
    SSRS tssrs = exesql.execSQL(tCheckSql);
    if(tssrs.getMaxRow()!=0)
    {
      CError cError = new CError( );
      cError.moduleName = "CalLoreserveBL";
      cError.functionName = "PrepareSaveData";
      cError.errorMessage = "lbpol表中有重复记录";
      this.mErrors.addOneError(cError);
      return;
    }
    tCheckSql="select polno from lbpol where polno in(select polno from lcpol where appflag='1')";
    SSRS tssrs1 = exesql.execSQL(tCheckSql);
    if(tssrs1.getMaxRow()!=0)
    {
      CError cError = new CError( );
      cError.moduleName = "CalLoreserveBL";
      cError.functionName = "PrepareSaveData";
      cError.errorMessage = "lbpol,lcpol表中有重复记录";
      this.mErrors.addOneError(cError);
      return;
    }

    LOReserveSet tLOReserveSet = new LOReserveSet();
    LOReserveSet yLOReserveSet = new LOReserveSet();
    LCPolDB tLCPolDB = new LCPolDB();
    LCPolSet tLCPolSet = new LCPolSet();
    LMDutyGetDB ttLMDutyGetDB = new LMDutyGetDB();
    mAllLMDutyGetSet=ttLMDutyGetDB.query();
    LMDutyGetClmDB ttLMDutyGetClmDB = new LMDutyGetClmDB();
    mLMDutyGetClmSet=ttLMDutyGetClmDB.query();
   //对lcpol表的查询
    String c_sql = " select * from LCPol where AppFlag = '1' and signdate<='"+tPaytoDate+"' ";
    //对lbpol表的查询
    String b_sql = " select * from LBPol where AppFlag = '1' and signdate<='"+tPaytoDate+"'";
    tLCPolSet.set(tLCPolDB.executeQuery(c_sql));
    //以下是得到上个会计年度
    String current_date = PubFun.getCurrentDate();
    String current_year = current_date.substring(0,4);
    int year = Integer.parseInt(current_year);
    int last_year = year-1;
    String Last_year = String.valueOf(last_year);
    int tTotalCount=0;

    if(tLCPolSet.size()!=0)
    {
      for(int i=1;i<=tLCPolSet.size();i++)
      {
        tTotalCount++;
        System.out.println("第"+i+"条记录");
        LOReserveSchema tLOReserveSchema = new LOReserveSchema();
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setSchema(tLCPolSet.get(i));
        this.mLCPolSchema.setSchema(tLCPolSchema);
        String tPolNo = tLCPolSchema.getPolNo();
        //查询LCGet表中的GetDutyCode
        LCGetDB tLCGetDB = new LCGetDB();
        tLCGetDB.setPolNo(tPolNo);
        mLCGetSet=tLCGetDB.query();

        //养老金计算
        LCGetSet tYLLCGetSet= new LCGetSet();
        //加载保单所有的养老金
        for (int LCGetCount=1;LCGetCount<=mLCGetSet.size();LCGetCount++)
        {
          LCGetSchema tLCGetSchema= mLCGetSet.get(LCGetCount);
          for (int j=1;j<=mAllLMDutyGetSet.size();j++)
          {
            LMDutyGetSchema tLMDutyGetSchema = mAllLMDutyGetSet.get(j);
            if (tLMDutyGetSchema.getGetType2()==null)
              continue;
            if (!tLMDutyGetSchema.getGetType2().equals("1")) //非养老金
              continue;
            if (tLMDutyGetSchema.getGetDutyCode().equals(tLCGetSchema.getGetDutyCode()))
            {
              tYLLCGetSet.add(tLCGetSchema);
            }
          }
        }
        //选取最早领取的给付金
        int StartAge=10000;
        for (int j=1;j<=tYLLCGetSet.size();j++)
        {
          LCGetSchema tLCGetSchema = tYLLCGetSet.get(j);
          //养老金的起领年龄
          int ThisStartAge=this.CalAge(tLCPolSchema.getInsuredBirthday(),tLCGetSchema.getGetStartDate());
          if (StartAge>ThisStartAge)
          {
            //养老金起领日期
            tLOReserveSchema.setRevGetDate(tLCGetSchema.getGetStartDate());
            //养老金的起领年龄
            tLOReserveSchema.setRevGetAge(ThisStartAge);
            //养老金首次领取金额
            tLOReserveSchema.setRevGetMoney(tLCGetSchema.getActuGet());
            //养老的领取方式
            tLOReserveSchema.setRevGetIntv(tLCGetSchema.getGetIntv());
            StartAge=ThisStartAge;
          }
        }
        tLOReserveSchema.setManageCom(tLCPolSchema.getManageCom());
        tLOReserveSchema.setPolNo(tPolNo);
        tLOReserveSchema.setRiskCode(tLCPolSchema.getRiskCode());
        tLOReserveSchema.setSaleChnl(tLCPolSchema.getSaleChnl());
        tLOReserveSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
        tLOReserveSchema.setInsuredSex(tLCPolSchema.getInsuredSex());
        tLOReserveSchema.setInsuredBirthday(tLCPolSchema.getInsuredBirthday());
        tLOReserveSchema.setInsuredAppAge(tLCPolSchema.getInsuredAppAge());
        tLOReserveSchema.setCValiDate(tLCPolSchema.getCValiDate());
        tLOReserveSchema.setLastRevDate(tLCPolSchema.getLastRevDate());
        tLOReserveSchema.setMakeDate(tLCPolSchema.getMakeDate());
        //2003-08-20
        tLOReserveSchema.setOccupationType(tLCPolSchema.getOccupationType());
        //吸烟状态
        FDate tFD = new FDate();
        Date tMakeDate=tFD.getDate(tLCPolSchema.getMakeDate());
        Date tStdDate=tFD.getDate("2003-08-10");
        if (tMakeDate.compareTo(tStdDate)>0)
        {
          tLOReserveSchema.setSmokingFlag(getSmokingFlag(tLCPolSchema.getPolNo()));
        }


        tLOReserveSchema.setYears(tLCPolSchema.getInsuYear()); //保险年龄年期
//        if (tLCPolSchema.getPayIntv()==0 || tLCPolSchema.getPayIntv()==-1) //缴费年龄年期
//        {
//          tLOReserveSchema.setPayYears(1);
//        }
//        else
//        {
//          tLOReserveSchema.setPayYears(tLCPolSchema.getPayEndYear());
//        }
        tLOReserveSchema.setPayYears(tLCPolSchema.getPayEndYear());
        tLOReserveSchema.setInsuYearFlag(tLCPolSchema.getInsuYearFlag()); //保险年龄年期标志
        tLOReserveSchema.setPayEndYearFlag(tLCPolSchema.getPayEndYearFlag()); //缴费年龄年期标志

        //tLOReserveSchema.setYears(tLCPolSchema.getYears());
        //tLOReserveSchema.setPayYears(tLCPolSchema.getPayYears());
        tLOReserveSchema.setPayIntv(tLCPolSchema.getPayIntv());
        tLOReserveSchema.setStandPrem(tLCPolSchema.getStandPrem());
        tLOReserveSchema.setAmnt(tLCPolSchema.getAmnt());
        //对保单状态进行附值－0有效；1－失效；2解约
        FDate tFDate = new FDate();
        Date t_endDate = tFDate.getDate(tLCPolSchema.getEndDate());//得到保险责任终止日期
        Date paytodate =tFDate.getDate(tLCPolSchema.getPaytoDate());          //new Date(tLCPolSchema.getPaytoDate());
        Date EndDate = PubFun.calDate( paytodate, 60, "D", new Date());
        String tPolStatus = "0";
        if(t_endDate.before(tFDate.getDate(PubFun.getCurrentDate())))//将保险责任终止日期和当前的日期向比较，若早于当前日期则是满期解约
        {
          tPolStatus="2";
          tLOReserveSchema.setPolStatus(tPolStatus);
          //合同终止日期、原因
          tLOReserveSchema.setStopDate(tLCPolSchema.getEndDate());
          tLOReserveSchema.setStopReason("MQ");
        }
        else
        {
          tPolStatus=getPolStatus(tFDate.getDate(tLCPolSchema.getPaytoDate()),tFDate.getDate(tLCPolSchema.getPayEndDate()),String.valueOf(tLCPolSchema.getPayIntv()));
          tLOReserveSchema.setPolStatus(tPolStatus);
        }

        if (tPolStatus.equals("1")) //失效
          tLOReserveSchema.setInvalidDate(EndDate);//失效日期
        //免交开始日期
        tLOReserveSchema.setFreeStartDate(getFreeStartDate(tPolNo,"C"));
        //被保人死亡日期
        tLOReserveSchema.setDeadDate(getDeathDate(tLCPolSchema.getInsuredNo()));
        //该领但未领的金额
        tLOReserveSchema.setSouldGetMoney(getSouldGetMoney(tPolNo));
        //年金给付金额
        tLOReserveSchema.setAnnMoney(getMoney(tPolNo,1,"C"));
        //死亡伤残给付金额(函数的参数是1)
        tLOReserveSchema.setDeathMoney(getMoney_clm(tPolNo,1,"C"));
        //对医疗给付金额进行附值，参数是0
        tLOReserveSchema.setMedMoney(getMoney_clm(tPolNo,0,"C"));
        //对累计以付金额进行附值，参数是2
        tLOReserveSchema.setTotalMoney(getMoney_clm(tPolNo,2,"C"));
        //上个会计年度的保费收入
        tLOReserveSchema.setLastPayPrem(getLastPayPrem(tPolNo,Last_year));
        //保单年度
        tLOReserveSchema.setYearIdx(CalAge(tLCPolSchema.getCValiDate(),current_date));
        //计算年度
        tLOReserveSchema.setCalYear(year);
        tLOReserveSchema.setCheckFlag("0");
        tLOReserveSchema.setCalFlag("0");
        tLOReserveSchema.setModifyDate(current_date);
        //对满期给付金额的附值
        tLOReserveSchema.setExpMoney(getMoney(tPolNo,0,"C"));
        //职业加费进行附值
        tLOReserveSchema.setOccPrem(getPrem(tPolNo,1,"C"));
        //对健康加费进行附值
        tLOReserveSchema.setHealthPrem(getPrem(tPolNo,2,"C"));
        //签单日期
        tLOReserveSchema.setSignDate(tLCPolSchema.getSignDate());
        //本年度续期保费合计
        tLOReserveSchema.setThisPayPrem(this.getThisPayPrem(tPolNo,current_year));
        //交至日期
        tLOReserveSchema.setPaytoDate(tLCPolSchema.getPaytoDate());
        //管理费比例
        tLOReserveSchema.setManageFeeRate(tLCPolSchema.getManageFeeRate());
        //保单送达确认日期
//        tLOReserveSchema.setCustomGetPolDate(tLCPolSchema.getCustomGetPolDate());

        yLOReserveSet.add(tLOReserveSchema);


//        System.out.println("保单号码是"+tLOReserveSchema.getPolNo());
        System.out.println("共有记录"+yLOReserveSet.size()+"条");
        if (tTotalCount%1000==0)
        {
          LOReserveDBSet ttLOReserveDBSet = new LOReserveDBSet();
          ttLOReserveDBSet.set(yLOReserveSet);
          if (!ttLOReserveDBSet.insert())
            return;
          else
          {
            yLOReserveSet.clear();
          }
        }
      }
    }

    LOReserveDBSet tLOReserveDBSet = new LOReserveDBSet();
    tLOReserveDBSet.set(yLOReserveSet);
    if (!tLOReserveDBSet.insert())
    {
      return;
    }
    yLOReserveSet.clear();

    //对LBPol表进行查询
    LBPolDB tLBPolDB = new LBPolDB();
    LBPolSet tLBPolSet = new LBPolSet();
    tLBPolSet.set(tLBPolDB.executeQuery(b_sql));
    if(tLBPolSet.size()!=0)
    {
      for(int k=1;k<=tLBPolSet.size();k++)
      {
        System.out.println("第"+k+"条记录");
        LOReserveSchema tLOReserveSchema = new LOReserveSchema();
        LBPolSchema tLBPolSchema = new LBPolSchema();
        tLBPolSchema.setSchema(tLBPolSet.get(k));
        this.mLBPolSchema.setSchema(tLBPolSchema);
        String tPolNo = tLBPolSchema.getPolNo();
        //查询LCGet表中的GetDutyCode
        LBGetDB tLBGetDB = new LBGetDB();
        tLBGetDB.setPolNo(tPolNo);
        mLBGetSet.set(tLBGetDB.query());
        //养老金计算
        LBGetSet tYLLBGetSet= new LBGetSet();
        //加载保单所有的养老金
        for (int LBGetCount=1;LBGetCount<=mLBGetSet.size();LBGetCount++)
        {
          LBGetSchema tLBGetSchema= mLBGetSet.get(LBGetCount);
          for (int j=1;j<=mAllLMDutyGetSet.size();j++)
          {
            LMDutyGetSchema tLMDutyGetSchema = mAllLMDutyGetSet.get(j);
            if (tLMDutyGetSchema.getGetType2()==null) //非养老金
              continue;
            if (!tLMDutyGetSchema.getGetType2().equals("1")) //非养老金
              continue;
            if (tLMDutyGetSchema.getGetDutyCode().equals(tLBGetSchema.getGetDutyCode()))
            {
              tYLLBGetSet.add(tLBGetSchema);
            }
          }
        }
        //选取最早领取的给付金
        int StartAge=10000;
        for (int j=1;j<=tYLLBGetSet.size();j++)
        {
          LBGetSchema tLBGetSchema = tYLLBGetSet.get(j);
          //养老金的起领年龄
          int ThisStartAge=CalAge(tLBPolSchema.getInsuredBirthday(),tLBGetSchema.getGetStartDate());
          if (StartAge>ThisStartAge)
          {
            //养老金起领日期
            tLOReserveSchema.setRevGetDate(tLBGetSchema.getGetStartDate());
            //养老金的起领年龄
            tLOReserveSchema.setRevGetAge(ThisStartAge);
            //养老金首次领取金额
            tLOReserveSchema.setRevGetMoney(tLBGetSchema.getActuGet());
            //养老的领取方式
            tLOReserveSchema.setRevGetIntv(tLBGetSchema.getGetIntv());
            StartAge=ThisStartAge;
          }
        }
        tLOReserveSchema.setManageCom(tLBPolSchema.getManageCom());
        tLOReserveSchema.setPolNo(tPolNo);
        tLOReserveSchema.setRiskCode(tLBPolSchema.getRiskCode());
        tLOReserveSchema.setSaleChnl(tLBPolSchema.getSaleChnl());
        tLOReserveSchema.setInsuredNo(tLBPolSchema.getInsuredNo());
        tLOReserveSchema.setInsuredSex(tLBPolSchema.getInsuredSex());
        tLOReserveSchema.setInsuredBirthday(tLBPolSchema.getInsuredBirthday());
        tLOReserveSchema.setInsuredAppAge(tLBPolSchema.getInsuredAppAge());
        tLOReserveSchema.setCValiDate(tLBPolSchema.getCValiDate());
        tLOReserveSchema.setLastRevDate(tLBPolSchema.getLastRevDate());
        tLOReserveSchema.setMakeDate(tLBPolSchema.getMakeDate());
        tLOReserveSchema.setOccupationType(tLBPolSchema.getOccupationType());

        //吸烟状态
        FDate tFD = new FDate();
        Date tMakeDate=tFD.getDate(tLBPolSchema.getMakeDate());
        Date tStdDate=tFD.getDate("2003-08-10");
        if (tMakeDate.compareTo(tStdDate)>0)
        {
          tLOReserveSchema.setSmokingFlag(getSmokingFlag_b(tLBPolSchema.getPolNo()));
        }


        tLOReserveSchema.setYears(tLBPolSchema.getInsuYear()); //保险年龄年期
//        if (tLBPolSchema.getPayIntv()==0 || tLBPolSchema.getPayIntv()==-1) //缴费年龄年期
//        {
//          tLOReserveSchema.setPayYears(1);
//        }
//        else
//        {
//          tLOReserveSchema.setPayYears(tLBPolSchema.getPayEndYear());
//        }
        tLOReserveSchema.setPayYears(tLBPolSchema.getPayEndYear());
        tLOReserveSchema.setInsuYearFlag(tLBPolSchema.getInsuYearFlag()); //保险年龄年期标志
        tLOReserveSchema.setPayEndYearFlag(tLBPolSchema.getPayEndYearFlag()); //缴费年龄年期标志
//        tLOReserveSchema.setYears(tLBPolSchema.getYears());
//        tLOReserveSchema.setPayYears(tLBPolSchema.getPayYears());

        tLOReserveSchema.setPayIntv(tLBPolSchema.getPayIntv());
        tLOReserveSchema.setStandPrem(tLBPolSchema.getStandPrem());
        tLOReserveSchema.setAmnt(tLBPolSchema.getAmnt());
        //对保单状态进行附值－0有效；1－失效；2解约
        FDate tFDate = new FDate();
        Date paytodate =tFDate.getDate(tLBPolSchema.getPaytoDate());          //new Date(tLCPolSchema.getPaytoDate());
        Date EndDate = PubFun.calDate( paytodate, 60, "D", new Date());
        tLOReserveSchema.setPolStatus("2");
        //tLOReserveSchema.setStopReason("保单转储");
//        tLOReserveSchema.setInvalidDate(EndDate);//失效日期
//        //免交开始日期
      tLOReserveSchema.setFreeStartDate(getFreeStartDate(tPolNo,"B"));
        //被保人死亡日期
        tLOReserveSchema.setDeadDate(getDeathDate(tLBPolSchema.getInsuredNo()));
        //该领但未领的金额
        tLOReserveSchema.setSouldGetMoney(getSouldGetMoney(tPolNo));
        //合同终止原因和合同终止日期
        tLOReserveSchema.setStopDate(tLBPolSchema.getModifyDate());
        //add 03-8-5:合同终止原因
        tLOReserveSchema.setStopReason(this.getStopReason(tLBPolSchema));

        //年金给付金额
        tLOReserveSchema.setAnnMoney(getMoney(tPolNo,1,"B"));
        //死亡伤残给付金额(函数的参数是1)
        tLOReserveSchema.setDeathMoney(getMoney_clm(tPolNo,1,"B"));
        //对医疗给付金额进行附值，参数是0
        tLOReserveSchema.setMedMoney(getMoney_clm(tPolNo,0,"B"));
        //对累计以付金额进行附值，参数是2
        tLOReserveSchema.setTotalMoney(getMoney_clm(tPolNo,2,"B"));
        //上个会计年度的保费收入
        tLOReserveSchema.setLastPayPrem(getLastPayPrem(tPolNo,Last_year));
        //保单年度
        tLOReserveSchema.setYearIdx(CalAge(tLBPolSchema.getCValiDate(),current_date));
        //计算年度
        tLOReserveSchema.setCalYear(year);
        tLOReserveSchema.setCheckFlag("0");
        tLOReserveSchema.setCalFlag("0");
        tLOReserveSchema.setModifyDate(current_date);
        //对满期给付金额的附值
        tLOReserveSchema.setExpMoney(getMoney(tPolNo,0,"B"));
        //职业加费进行附值
        tLOReserveSchema.setOccPrem(getPrem(tPolNo,1,"B"));
        //对健康加费进行附值
        tLOReserveSchema.setHealthPrem(getPrem(tPolNo,2,"B"));
        //签单日期
        tLOReserveSchema.setSignDate(tLBPolSchema.getSignDate());
        //本年度续期保费合计
        tLOReserveSchema.setThisPayPrem(this.getThisPayPrem(tPolNo,current_year));
        //交至日期
        tLOReserveSchema.setPaytoDate(tLBPolSchema.getPaytoDate());
        //管理费比例
        tLOReserveSchema.setManageFeeRate(tLBPolSchema.getManageFeeRate());

        // ADD: 退保金额
        tLOReserveSchema.setSurMoney(this.getSurMoney(tLBPolSchema));
        //保单送达确认日期
//        tLOReserveSchema.setCustomGetPolDate(tLBPolSchema.getCustomGetPolDate());


        yLOReserveSet.add(tLOReserveSchema);
        System.out.println("共有记录"+yLOReserveSet.size()+"条");
      }
    }
//    mInputData.clear();
//    mInputData.add(yLOReserveSet);
    LOReserveDBSet ttLOReserveDBSet = new LOReserveDBSet();
    ttLOReserveDBSet.set(yLOReserveSet);
    if (!ttLOReserveDBSet.insert())
    {
      System.out.println(ttLOReserveDBSet.mErrors.getFirstError());
      return;
    }
    yLOReserveSet.clear();
  }


  /****************************************************************************
   * 计算保单状态的函数
   * 参数说明：PaytoDate：LCPol中的交至日期；PayEndDate:LCPol中的终交日期 ;
   *          PayIntv:LCPol中的交费间隔，若是1则是不定期交费
   * 返回值：String PolStatus＝0（有效）；PolStatus＝1（失效）
   * 逻辑说明：若“交至日期”＝“终交日期”，则是有效；
   *          若“交费间隔是1，不定期，则是有效”；
   * author:lys
   */
  private String getPolStatus(Date PaytoDate,Date PayEndDate,String PayIntv)
  {
    String PolStatus="0";//有效
    //对于有效的情况
    if(!(PaytoDate.equals(PayEndDate)||PayIntv.equals("-1")))
    {
      FDate tFDate = new FDate();
      Date EndDate = PubFun.calDate( PaytoDate, 60, "D", new Date());
      if(EndDate.before(tFDate.getDate(PubFun.getCurrentDate())))
      {
        //EndDate在当前日期的前面
        PolStatus = "1";
      }
    }
    return PolStatus;
  }

  //计算免交开始日期
  private  String getFreeStartDate(String tpolno,String b_c)
  {
    String FreeStartDate = "";
    if(b_c.equals("C"))
    {
//      if(!(this.mLCPolSchema.getFreeFlag()==null||this.mLCPolSchema.getFreeFlag().equals("")))
//      {
//        if (this.mLCPolSchema.getFreeFlag().equals("1"))
//        {
//          System.out.println("计算免交开始日期的查询是");
          String FreeStartDate_sql = " Select EdorValiDate From LPEdorMain where EdorType = 'AP' and PolNo = '"+tpolno+"' order by EdorValiDate desc";
          ExeSQL exesql = new ExeSQL();
          SSRS tssrs = exesql.execSQL(FreeStartDate_sql);
          if(tssrs.getMaxRow()!=0)
          {
            FreeStartDate=tssrs.GetText(1,tssrs.getMaxRow());
//            System.out.println("免交日期是"+FreeStartDate);
//          }
//        }
      }
    }
    if(b_c.equals("B"))
    {
//      if(!(this.mLBPolSchema.getFreeFlag()==null||this.mLBPolSchema.getFreeFlag().equals("")))
//      {
//        if (this.mLBPolSchema.getFreeFlag().equals("1"))
//        {
//          System.out.println("计算免交开始日期的查询是");
          String FreeStartDate_sql = " Select EdorValiDate From LPEdorMain where EdorType = 'AP' and PolNo = '"+tpolno+"' order by EdorValiDate desc";
          ExeSQL exesql = new ExeSQL();
          SSRS tssrs = exesql.execSQL(FreeStartDate_sql);
          if(tssrs.getMaxRow()!=0)
          {
            FreeStartDate=tssrs.GetText(1,tssrs.getMaxRow());
//            System.out.println("免交日期是"+FreeStartDate);
//          }
//        }
      }
    }
    return FreeStartDate;
  }
  //计算被保险人的死亡日期
  private String getDeathDate(String tInsuredNo)
  {
    String DeathDate = "";
    String DeathDate_sql = "Select DeathDate From LDPerson where CustomerNo = '"+tInsuredNo+"' ";
    ExeSQL exesql = new ExeSQL();
    SSRS ssrs = exesql.execSQL(DeathDate_sql);
    if(ssrs.getMaxRow()!=0)
    {
      DeathDate=ssrs.GetText(1,1);
//      System.out.println("死亡时间是"+DeathDate);
    }
    return DeathDate;
  }
  //计算该领却未领的金额
  private double getSouldGetMoney(String tpolno)
  {
    double SouldGetMoney = 0;
//    System.out.println("该领而未领的金额的查询结果是");
    String SouldGetMoney_sql = "select nvl(sum(sumgetmoney),0) from ljaget where confdate is null and otherno = '"+tpolno+"' ";
    ExeSQL exesql = new ExeSQL();
    SSRS ssrs = exesql.execSQL(SouldGetMoney_sql);
    SouldGetMoney=Double.parseDouble(ssrs.GetText(1,1));
//    System.out.println("该领却未领的金额是"+SouldGetMoney);
    return SouldGetMoney;
  }

//  //计算养老金的起领日期
//  private String getRevGetDate(String tpolno,String c_b)
//  {
//    String RevGetDate="";
//    String tEarlyDate="3000-01-01";
//    if(mLMDutyGetSet.size()!=0)
//    {
//      for(int dutygetCount=1;dutygetCount<=mLMDutyGetSet.size();dutygetCount++)
//      {
//        System.out.println("养老的领取日期的查询结果");
//        String getRevGetDate_sql="";
//        if(c_b.equals("C"))
//        {
//          getRevGetDate_sql = "select GetStartDate from LCGet where GetDutyCode = '"+mLMDutyGetSet.get(dutygetCount).getGetDutyCode()+"' order by GetStartDate";
//        }
//        if(c_b.equals("B"))
//        {
//          getRevGetDate_sql = "select GetStartDate from LBGet where GetDutyCode = '"+mLMDutyGetSet.get(dutygetCount).getGetDutyCode()+"' order by GetStartDate";
//        }
//        System.out.println("起领日期的查询结果是"+getRevGetDate_sql);
//        ExeSQL exesql = new ExeSQL();
//        SSRS ssrs_3 = exesql.execSQL(getRevGetDate_sql);
//        if(ssrs_3.getMaxRow()!=0)
//        {
//          RevGetDate=ssrs_3.GetText(1,1);
//          if (PubFun.calInterval(RevGetDate,tEarlyDate,"D")>0)
//          {
//            tEarlyDate=RevGetDate;
//          }
//          System.out.println("起领日期是＝＝＝＝"+RevGetDate);
//        }
//      }
//    }
//    else
//    {
//      tEarlyDate="";
//    }
//    return tEarlyDate;
//  }
//
//  //计算养老金领取年龄，参数是养老金的起领年龄
//  private int getRevGetAge(String InsuredBirthday,String RevGetDate)
//  {
//    int RevGetAge=0;
//    if(!(RevGetDate==null||RevGetDate.equals("")))
//    {
//      System.out.println("养老的领取年龄");
//      RevGetAge= PubFun.calInterval(InsuredBirthday,RevGetDate ,"Y");
//      System.out.println("年龄是"+RevGetAge);
//    }
//    return RevGetAge;
//  }
//  //计算养老金的首次领取金额,参数是保单号码,若有多条记录，则进行求和
//  private double getRevGetMoney(String tpolno,String RevGetDate,String c_b)
//  {
//    double RevGetMoney=0;
//    if(!(RevGetDate==null||RevGetDate.equals("")))
//    {
//      if(mLMDutyGetSet.size()!=0)
//      {
//        for(int dutygetCount=1;dutygetCount<=mLMDutyGetSet.size();dutygetCount++)
//        {
//          String getRevGetDate_sql="";
//          System.out.println("养老的领取金额");
//          if(c_b.equals("C"))
//          {
//            getRevGetDate_sql = "select GetStartDate,nvl(sum(StandMoney),0) from LCGet where GetDutyCode = '"+mLMDutyGetSet.get(dutygetCount).getGetDutyCode()+"' and GetStartDate = '"
//                           +RevGetDate+"' Group by GetStartDate order by GetStartDate";
//          }
//          if(c_b.equals("B"))
//          {
//            getRevGetDate_sql = "select GetStartDate,nvl(sum(StandMoney),0) from LBGet where GetDutyCode = '"+mLMDutyGetSet.get(dutygetCount).getGetDutyCode()+"' and GetStartDate = '"
//                           +RevGetDate+"' Group by GetStartDate order by GetStartDate";
//          }
//          ExeSQL exesql = new ExeSQL();
//          SSRS ssrs_3 = exesql.execSQL(getRevGetDate_sql);
//          RevGetMoney=Double.parseDouble(ssrs_3.GetText(1,2));
//          System.out.println("总金额是"+RevGetMoney);
//        }
//      }
//    }
//    return RevGetMoney;
//  }
//养老金的领取方式
//  private String RevGetIntv(String tpolno,String RevGetDate,String c_b)
//  {
//    String RevGetIntv="";
//    if(mLMDutyGetSet.size()!=0)
//    {
//      for(int dutygetCount=1;dutygetCount<=mLMDutyGetSet.size();dutygetCount++)
//      {
//        System.out.println("养老的领取方式是");
//        String getRevGetDate_sql="";
//        if(c_b.equals("C"))
//        {
//          getRevGetDate_sql = "select GetMode from LCGet where GetDutyCode = '"+mLMDutyGetSet.get(dutygetCount).getGetDutyCode()+"'and GetStartDate = '"+RevGetDate+"' order by GetStartDate";
//        }
//        if(c_b.equals("B"))
//        {
//          getRevGetDate_sql = "select GetMode from LBGet where GetDutyCode = '"+mLMDutyGetSet.get(dutygetCount).getGetDutyCode()+"'and GetStartDate = '"+RevGetDate+"' order by GetStartDate";
//        }
//        ExeSQL exesql = new ExeSQL();
//        SSRS ssrs_3 = exesql.execSQL(getRevGetDate_sql);
//        if(ssrs_3.getMaxRow()!=0)
//        {
//          RevGetIntv=ssrs_3.GetText(ssrs_3.getMaxRow(),1);
//          System.out.println("领取的方式是＝＝＝＝"+RevGetIntv);
//        }
//      }
//    }
//    return RevGetIntv;
//  }
  //将年金和满期同时处理 type=1--年金;type=0--满期
  private double getMoney(String tpolno,int type,String c_b)
  {
    double Money_count=0;
    String getdutycode_2_sql="";
    if(c_b.equals("C"))
    {
      for (int i=1;i<=this.mLCGetSet.size();i++)
      {
        LCGetSchema tLCGetSchema = mLCGetSet.get(i);
        for (int j=1;j<=this.mAllLMDutyGetSet.size();j++)
        {
          LMDutyGetSchema tLMDutyGetSchema = mAllLMDutyGetSet.get(j);
          if (tLMDutyGetSchema.getGetDutyCode().equals(tLCGetSchema.getGetDutyCode()))
          {
            if (tLMDutyGetSchema.getGetType1()==null && type==1)
            {
              Money_count=Money_count+tLCGetSchema.getActuGet();
              break;
            }
            if (tLMDutyGetSchema.getGetType1()==null)
              continue;
            if (tLMDutyGetSchema.getGetType1().equals("1") && type==1)
            {
              Money_count=Money_count+tLCGetSchema.getActuGet();
              break;
            }
            if (tLMDutyGetSchema.getGetType1().equals("0") && type==0)
            {
              Money_count=Money_count+tLCGetSchema.getActuGet();
              break;
            }
          }
        }
      }
    }
    if(c_b.equals("B"))
    {
      for (int i=1;i<=this.mLBGetSet.size();i++)
      {
        LBGetSchema tLBGetSchema = mLBGetSet.get(i);
        for (int j=1;j<=this.mAllLMDutyGetSet.size();j++)
        {
          LMDutyGetSchema tLMDutyGetSchema = mAllLMDutyGetSet.get(j);
          if (tLMDutyGetSchema.getGetDutyCode().equals(tLBGetSchema.getGetDutyCode()))
          {
            if (tLMDutyGetSchema.getGetType1()==null && type==1)
            {
              Money_count=Money_count+tLBGetSchema.getActuGet();
              break;
            }
            if (tLMDutyGetSchema.getGetType1()==null)
              continue;
            if (tLMDutyGetSchema.getGetType1().equals("1") && type==1)
            {
              Money_count=Money_count+tLBGetSchema.getActuGet();
              break;
            }

            if (tLMDutyGetSchema.getGetType1().equals("0") && type==0)
            {
              Money_count=Money_count+tLBGetSchema.getActuGet();
              break;
            }
          }
        }
      }
    }
    return Money_count;
  }
  //确认死亡级医疗的给付金额,参数是保单号码和clm_type（1死亡；0医疗;2累计以付金额），金额取实际给付金额
  private double getMoney_clm(String tpolno,int clm_type,String c_b)
  {
    double Money_clm = 0;
    String getmoney="";
    if(c_b.equals("C"))
    {
      if(clm_type==1)
      {
//        System.out.println("死亡的给付金额");
        for (int i=1;i<=this.mLMDutyGetClmSet.size();i++)
        {
          LMDutyGetClmSchema tLMDutyGetClmSchema = mLMDutyGetClmSet.get(i);
          if (!(tLMDutyGetClmSchema.getGetDutyKind().equals("202")||
              tLMDutyGetClmSchema.getGetDutyKind().equals("102") ||
              tLMDutyGetClmSchema.getGetDutyKind().equals("201") ||
              tLMDutyGetClmSchema.getGetDutyKind().equals("101") ||
              tLMDutyGetClmSchema.getGetDutyKind().equals("001")
              ))
          {
            continue;
          }
          for (int j=1;j<=this.mLCGetSet.size();j++)
          {
            LCGetSchema tLCGetSchema = mLCGetSet.get(j);
            if (tLCGetSchema.getGetDutyCode().equals(tLMDutyGetClmSchema.getGetDutyCode()))
              Money_clm=Money_clm+tLCGetSchema.getActuGet();
          }
        }
      }
      if(clm_type==0)
      {
        for (int i=1;i<=this.mLMDutyGetClmSet.size();i++)
        {
          LMDutyGetClmSchema tLMDutyGetClmSchema = mLMDutyGetClmSet.get(i);
          if (!(tLMDutyGetClmSchema.getGetDutyKind().equals("200")||
                tLMDutyGetClmSchema.getGetDutyKind().equals("100")
                ))
          {
            continue;
          }
          for (int j=1;j<=this.mLCGetSet.size();j++)
          {
            LCGetSchema tLCGetSchema = mLCGetSet.get(j);
            if (tLCGetSchema.getGetDutyCode().equals(tLMDutyGetClmSchema.getGetDutyCode()))
              Money_clm=Money_clm+tLCGetSchema.getActuGet();
          }
        }

      }
      if(clm_type==2)
      {
        for (int j=1;j<=this.mLCGetSet.size();j++)
        {
          LCGetSchema tLCGetSchema = mLCGetSet.get(j);
          Money_clm=Money_clm+tLCGetSchema.getSumMoney();
        }
      }
    }
      if(c_b.equals("B"))
      {
        if(clm_type==1)
        {
          for (int i=1;i<=this.mLMDutyGetClmSet.size();i++)
          {
            LMDutyGetClmSchema tLMDutyGetClmSchema = mLMDutyGetClmSet.get(i);
            if (!(tLMDutyGetClmSchema.getGetDutyKind().equals("202")||
                  tLMDutyGetClmSchema.getGetDutyKind().equals("102") ||
                  tLMDutyGetClmSchema.getGetDutyKind().equals("201") ||
                  tLMDutyGetClmSchema.getGetDutyKind().equals("101") ||
                  tLMDutyGetClmSchema.getGetDutyKind().equals("001")
                  ))
            {
              continue;
            }
            for (int j=1;j<=this.mLBGetSet.size();j++)
            {
              LBGetSchema tLBGetSchema = mLBGetSet.get(j);
              if (tLBGetSchema.getGetDutyCode().equals(tLMDutyGetClmSchema.getGetDutyCode()))
                Money_clm=Money_clm+tLBGetSchema.getActuGet();
            }
          }

        }
        if(clm_type==0)
        {
          for (int i=1;i<=this.mLMDutyGetClmSet.size();i++)
          {
            LMDutyGetClmSchema tLMDutyGetClmSchema = mLMDutyGetClmSet.get(i);
            if (!(tLMDutyGetClmSchema.getGetDutyKind().equals("200")||
                  tLMDutyGetClmSchema.getGetDutyKind().equals("100")
                  ))
            {
              continue;
            }
            for (int j=1;j<=this.mLBGetSet.size();j++)
            {
              LBGetSchema tLBGetSchema = mLBGetSet.get(j);
              if (tLBGetSchema.getGetDutyCode().equals(tLMDutyGetClmSchema.getGetDutyCode()))
                Money_clm=Money_clm+tLBGetSchema.getActuGet();
            }
          }
        }
        if(clm_type==2)
        {
          for (int j=1;j<=this.mLBGetSet.size();j++)
          {
            LBGetSchema tLBGetSchema = mLBGetSet.get(j);
            Money_clm=Money_clm+tLBGetSchema.getSumMoney();
          }
        }
      }
    return Money_clm;
  }
  //获得本年度的续期保费收入
  private double getThisPayPrem(String tpolno,String year)
  {
    double ThisPayPrem = 0;
    String StartDate = year+"-01-01";
    String EndDate = year+"-12-31";
    String ThisPayPrem_sql = "select nvl(sum(sumactupaymoney),0) from LJAPayPerson where ConfDate >='"+StartDate+"' and ConfDate<='"+EndDate+"' and PolNo = '"+tpolno+"' and PayCount!=1 ";
    ExeSQL exesql = new ExeSQL();
    SSRS ssrs = exesql.execSQL(ThisPayPrem_sql);
    ThisPayPrem=Double.parseDouble(ssrs.GetText(1,1));
//    System.out.println("上个会计年度的保费收入是"+LastPayPrem);
    return ThisPayPrem;
  }
  //获得上个会计年度的保费收入
  private double getLastPayPrem(String tpolno,String year)
  {
//    System.out.println("LastPayPrem上个会计年度的保费收入");
    double LastPayPrem = 0;
    String StartDate = year+"-01-01";
    String EndDate = year+"-12-31";
    String LastPayPrem_sql = "select nvl(sum(sumactupaymoney),0) from LJAPayPerson where ConfDate >='"+StartDate+"' and ConfDate<='"+EndDate+"' and PolNo = '"+tpolno+"' ";
    ExeSQL exesql = new ExeSQL();
    SSRS ssrs = exesql.execSQL(LastPayPrem_sql);
    LastPayPrem=Double.parseDouble(ssrs.GetText(1,1));
//    System.out.println("上个会计年度的保费收入是"+LastPayPrem);
    return LastPayPrem;
  }
  /*****************************************************************************
   * 计算职业加费和健康加费的函数；
   * 参数说明：保单号码；t＝1代表职业加费
   *                  t＝2代表健康加费
   */
  private double getPrem(String tpolno,int t,String c_b)
  {
    double Prem=0;
    String Prem_sql = "";
    if(c_b.equals("C"))
    {
      if(t==1)
      {
//        System.out.println("职业加费的查询语句是");
        Prem_sql = " select LORGetOccPrem('"+tpolno+"') from dual ";//职业加费
      }
      if(t==2)
      {
//        System.out.println("健康加费的查询语句是");
        Prem_sql = " select LORGetHealthPrem('"+tpolno+"') from dual ";
      }
    }
    if(c_b.equals("B"))
    {
      if(t==1)
      {
//        System.out.println("b表职业加费的查询语句是");
        Prem_sql = " select LORGetOccPrem_B('"+tpolno+"') from dual ";//职业加费
      }
      if(t==2)
      {
//        System.out.println("B表健康加费的查询语句是");
        Prem_sql = " select LORGetHealthPrem_B('"+tpolno+"') from dual ";
      }
    }
    ExeSQL exesql = new ExeSQL();
    SSRS ssrs = exesql.execSQL(Prem_sql);
    Prem = Double.parseDouble(ssrs.GetText(1,1));
//    System.out.println("加费金额是"+Prem);
    return Prem;
  }
/*******************************************************************************
  * Name     :getSmokingFlag
  * Function ：在LCCustomerImpart表中获取是否吸烟标志
  * Remark   :若是不吸烟则返回值是‘0’，若是吸烟，则返回值是‘1’，若无记录则返回‘2’
  * Date     :2003-08-20
  * Author   :LiuYansong
*/
  private String getSmokingFlag (String tPolNo)
  {
    String SmokingFlag = "0";
    LCCustomerImpartSet tLCCustomerImpartSet = new LCCustomerImpartSet();
    LCCustomerImpartDB tLCCustomerImpartDB = new LCCustomerImpartDB();
//    tLCCustomerImpartDB.setPolNo(tPolNo);
    tLCCustomerImpartDB.setImpartCode("020");
    tLCCustomerImpartDB.setImpartVer("001");
    tLCCustomerImpartDB.setCustomerNoType("I");
    tLCCustomerImpartSet.set(tLCCustomerImpartDB.query());
    if(tLCCustomerImpartSet.size()==0)
      SmokingFlag = "0";
    else
      SmokingFlag = "1";
    return SmokingFlag ;
  }
  private String getSmokingFlag_b (String tPolNo)
 {
   String SmokingFlag = "0";
   LBCustomerImpartSet tLBCustomerImpartSet = new LBCustomerImpartSet();
   LBCustomerImpartDB tLBCustomerImpartDB = new LBCustomerImpartDB();
//   tLBCustomerImpartDB.setPolNo(tPolNo);
   tLBCustomerImpartDB.setImpartCode("020");
   tLBCustomerImpartDB.setImpartVer("001");
   tLBCustomerImpartDB.setCustomerNoType("I");
   tLBCustomerImpartSet.set(tLBCustomerImpartDB.query());
   if(tLBCustomerImpartSet.size()==0)
     SmokingFlag = "0";
   else
     SmokingFlag = "1";
   return SmokingFlag ;
 }

    /**
     * 传输数据的公共方法
     * @param: tPolNo 保单号
     * @return: String
    */
     private String getStopReason(LBPolSchema tLBPolSchema){
          String tEdor="";
//          if(CodeJudge.judgeCodeType(tLBPolSchema.getEdorNo(),"42")||CodeJudge.judgeCodeType(tLBPolSchema.getEdorNo(),"43")){
               LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
      //         tLPEdorMainDB.setPolNo(tLBPolSchema.getPolNo());
//               tLPEdorMainDB.setEdorNo(tLBPolSchema.getEdorNo());
               LPEdorMainSet tLPEdorMainSet = new LPEdorMainSet();
               tLPEdorMainSet=tLPEdorMainDB.query();
               if (tLPEdorMainSet.size()==0)
               {
                 tLPEdorMainDB = new LPEdorMainDB();
            //wujs--     tLPEdorMainDB.setPolNo(tLBPolSchema.getMainPolNo());
//                 tLPEdorMainDB.setEdorNo(tLBPolSchema.getEdorNo());
                 tLPEdorMainSet=tLPEdorMainDB.query();
                 if (tLPEdorMainSet.size()>0)
                 {
           //wujs--        tEdor=tLPEdorMainSet.get(1).getEdorType();
                 }
               }
               else
               {
         //wujs--       tEdor=tLPEdorMainSet.get(1).getEdorType();
               }

//          }
//          if(CodeJudge.judgeCodeType(tLBPolSchema.getEdorNo(),"52")){
               LLClaimPolicyDB tLLClaimPolicyDB=new LLClaimPolicyDB();
//               tLLClaimPolicyDB.setClmNo(tLBPolSchema.getEdorNo());
               tLLClaimPolicyDB.setPolNo(tLBPolSchema.getPolNo());
               if(tLLClaimPolicyDB.getInfo()){
                   tEdor=tEdor+tLLClaimPolicyDB.getGetDutyKind();
//               }
          }
          return  tEdor;
   }
   /**
    * 传输数据的公共方法
    * @param: tPolNo 保单号
     * @return: String
   */
   private double getSurMoney(LBPolSchema tLBPolSchema)
   {
     double surMoney=0;
     LJAGetEndorseDB tLJAGetEndorseDB=new LJAGetEndorseDB();
     LJAGetEndorseSet tLJAGetEndorseSet =new LJAGetEndorseSet();
     tLJAGetEndorseDB.setPolNo(tLBPolSchema.getPolNo());
//     tLJAGetEndorseDB.setEndorsementNo(tLBPolSchema.getEdorNo());
     tLJAGetEndorseSet=tLJAGetEndorseDB.query();
     for(int i=1;i<=tLJAGetEndorseSet.size();i++)
     {
       if(tLJAGetEndorseSet.get(i).getFeeFinaType().equals("TB"))
       {
         surMoney=surMoney+tLJAGetEndorseSet.get(i).getGetMoney();
       }
     }
     return  surMoney;
   }
   /**
    * 传输数据的公共方法
    * @param: cInputData 输入的数据
    *         cOperate 数据操作
    * @return:
    */
   private int CalAge(String sDate,String eDate)
   {
     String tSql="select get_age('"+sDate+"','"+eDate+"') from dual";
     ExeSQL exesql = new ExeSQL();
     SSRS ssrs = exesql.execSQL(tSql);
     int age = Integer.parseInt(ssrs.GetText(1,1));
     return age;
   }
   public boolean submitData(VData cInputData,String cOperate)
   {
     mInputData = (VData)cInputData.clone();
     this.mOperate = cOperate;
     PrepareSaveData();
     //    CalLoreserveBLS tCalLoreserveBLS = new CalLoreserveBLS();
     //
     //    if(!tCalLoreserveBLS.submitData(mInputData,mOperate))
     //    {
     //      this.mErrors.copyAllErrors(tCalLoreserveBLS.mErrors);
     //      CError tError = new CError();
     //      tError.moduleName = "RegisterBL";
     //      tError.functionName = "submitData";
     //      tError.errorMessage = "数据提交失败!";
     //      this.mErrors .addOneError(tError) ;
     //      return false;
     //    }
     return true;
   }
   public void getPaytoDate(String mPaytoDate)
   {
     this.tPaytoDate=mPaytoDate;
   }
   private static void usageError()
   {
     System.out.println("填写日期：");
     System.exit(1);
   }
   public static void main(String[] args)
   {
     CalLoreserveBL tCalLoreserveBL = new CalLoreserveBL();

     if(args.length==0)
     {
       usageError();
     }
     else
     {
       tCalLoreserveBL.getPaytoDate(args[0]);
     }
     VData tVData = new VData();
     String t;
     tCalLoreserveBL.submitData(tVData,"t");
   }
}
