package com.sinosoft.lis.llcase;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: 保全变更投保人项目</p>
 * <p>Description: 录入保全明细</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorUWManuRReportUI
{
    private PEdorUWManuRReportBL mPEdorUWManuRReportBL = null;

    /**
     * 构造函数
     */
    public PEdorUWManuRReportUI()
    {
        mPEdorUWManuRReportBL = new PEdorUWManuRReportBL();
    }

    /**
     * 调用业务逻辑
     * @param data VData
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!mPEdorUWManuRReportBL.submitData(data))
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误信息
     * @return String
     */
    public String getError()
    {
        return mPEdorUWManuRReportBL.mErrors.getFirstError();
    }

    /**
     * 返回通知书号
     * @return String
     */
    public String getPrtNo()
    {
        return mPEdorUWManuRReportBL.getPrtNo();
    }
    
    /**
     * 返回保单印刷号
     * @return String
     */
    public String getContPrtNo()
    {
        return mPEdorUWManuRReportBL.getContPrtNo();
    }

    /**
     * 主函数，调试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor";
        gi.ManageCom = "86";
        LPPENoticeSchema tLPPENoticeSchema = new LPPENoticeSchema();
        tLPPENoticeSchema.setEdorNo("20051102000004");
        tLPPENoticeSchema.setContNo("00001085701");
        tLPPENoticeSchema.setCustomerNo("000010858");
        tLPPENoticeSchema.setPEAddress("");
        tLPPENoticeSchema.setPEDate("2006-2-17");
        tLPPENoticeSchema.setRemark("ffff");

        LPPENoticeItemSet tLPPENoticeItemSet = new LPPENoticeItemSet();
        for (int i = 0; i < 2; i++)
        {
            LPPENoticeItemSchema tLPPENoticeItemSchema = new
                    LPPENoticeItemSchema();
            tLPPENoticeItemSchema.setPEItemCode("ZH0001");
            tLPPENoticeItemSchema.setPEItemName("肝功能");
            tLPPENoticeItemSet.add(tLPPENoticeItemSchema);
        }

        VData data = new VData();
        data.add(gi);
        data.add(tLPPENoticeSchema);
        data.add(tLPPENoticeItemSet);
        PEdorUWManuHealthUI tPEdorUWManuHealthUI = new PEdorUWManuHealthUI();
        if (!tPEdorUWManuHealthUI.submitData(data))
        {
            System.out.println(tPEdorUWManuHealthUI.getError());
        }

    }
}
