package com.sinosoft.lis.llcase;


import java.io.*;

import java.util.zip.*;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;

/**
 * 理赔Region提数，将提数结果打包并邮件发送
 * @author Houyd
 *
 */

public class SegentRegionDataExtraction {

	public String mCurrentDate = PubFun.getCurrentDate();
	    private BufferedWriter mBufferedWriter;
	    
	    /**获取当前的工作路径*/
		private String mURL = "";
	    public CErrors mErrors = new CErrors();
	    private static final int BUFFER = 2048;
	    
	    /**对提数结果压缩成功标志：1-成功;0-失败;-1-失败终止*/
	    private int mFlag = 1;
	    /**压缩文件的路径*/
	    private String mZipFilePath;
	    /** SQL结果存放文件名*/
	    private String[] mZipDocFileName = new String[20];
	    /** SQL结果文件路径 */
	    private String[] mZipDocFile = new String[20];
	    /** SQL语句 */
	    private String mSQL ;
	    
	    /** 邮件收件人 */
	    private String mAddress = "";
	    /** 邮件抄送人 */
	    private String mCcAddress = "";
	    /** 邮件暗抄人 */
	    private String mBccAddress = "";
	    
	    public String getMAddress() {
			return mAddress;
		}

		public void setMAddress(String address) {
			mAddress = address;
		}

		public String getMBccAddress() {
			return mBccAddress;
		}

		public void setMBccAddress(String bccAddress) {
			mBccAddress = bccAddress;
		}

		public String getMCcAddress() {
			return mCcAddress;
		}

		public void setMCcAddress(String ccAddress) {
			mCcAddress = ccAddress;
		}
	    
	public static void main(String[] args) throws IOException{
		/**
		Date currentTime = new Date();
		System.out.println("Start at：" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentTime));
//		
//		String tTxtDirectory = "D:\\test\\";  //默认txt文件目录，若不传参数，则用此目录
//		String tLogDirectory = "D:\\test\\";  //默认日志文件目录，若不传参数，则用此目录
//		
//		if(args.length == 1){
//			tTxtDirectory = args[0];
//		}
//		else if(args.length == 2){
//			tTxtDirectory = args[0];
//			tLogDirectory = args[1];
//		}
//		
//		System.out.println("TxtDirectory:" + tTxtDirectory);
//		System.out.println("LogDirectory:" + tLogDirectory);
//		
//		MonthDataCatch tMonthDataCatch = new MonthDataCatch();
//		tMonthDataCatch.createCRMLog(tTxtDirectory, tLogDirectory);
		
		
		MonthDataCatch tMonthDataCatch = new MonthDataCatch();
		tMonthDataCatch.getSagentData();
		
		System.out.println("End at：" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		*/
		SegentRegionDataExtraction tSegentDateDataExtraction = new SegentRegionDataExtraction();
		tSegentDateDataExtraction.getSagentData();
	}
	
	/**给定目录下创建文件夹*/
	public static boolean createDir(String destDirName) {
	    File dir = new File(destDirName);
	    if(dir.exists()) {
	     System.out.println("创建目录" + destDirName + "失败，目标目录已存在！");
	     return false;
	    }
	    if(!destDirName.endsWith(File.separator))
	     destDirName = destDirName + File.separator;
	    // 创建单个目录
	    if(dir.mkdirs()) {
	     System.out.println("创建目录" + destDirName + "成功！");
	     return true;
	    } else {
	     System.out.println("创建目录" + destDirName + "成功！");
	     return false;
	    }
	}

	
	/**
	 * 执行批处理，并发送邮件
	 * @return
	 */
    public boolean getSagentData()
    {
    	String tSQL = "select sysvarvalue from ldsysvar where sysvar = 'UIRoot' with ur";
    	mURL = new ExeSQL().getOneValue(tSQL);//生成文件的存放路径
    	
    	mURL += "temp_lp/segent/";
        //this.mURL = "D:\\test\\";
        System.out.println("网盘路径："+mURL);
        if(PubDocument.save(mURL)){
        	mURL = PubDocument.getSavePath(mURL);
        }
        if (mURL == null || mURL.equals(""))
        {
            System.out.println("没有找到理赔提数文件夹！");
            return false;
        }

        getDataResult();
        getZipFile();
        sendSagentMail();
       
        // ---------------------
       // FtpTransferFiles();
//        try
//        {
//            del(mURL);
//        } catch (IOException ex)
//        {
//            ex.printStackTrace();
//        }
   		return true;
    }
    
    /**
     * 将查询结果的压缩包发送邮件
     *
     */
    private void sendSagentMail(){
    	if(mFlag == 1){//压缩文件生成成功，开始发送邮件
    		SagentMail tSendMail = new SagentMail();
        	try {
    			tSendMail.SendMail();
    			if(!"".equals(mAddress)){
    				tSendMail.setToAddress(mAddress);
    			}
    			if(!"".equals(mCcAddress)){
    				tSendMail.setCcAddress(mCcAddress);
    			}
    			if(!"".equals(mBccAddress)){
    				tSendMail.setBccAddress(mBccAddress);
    			}
    		} catch (Exception ex) {
    			System.out.println("邮件配置失败："+ex.toString());
    		}
    		try {
    			String tMailContent = "<b>Region提数结果请见附件</b>" +
    					"<br>RegionData1.txt = 区域本部本月.txt</br>" +
    					"<br>RegionData2.txt = 区域本部上月.txt</br>" +
    					"<br>RegionData3.txt = 区域本部本年.txt</br>" +
    					"<br>RegionData4.txt = 区域本部上年同期.txt</br>" +
    					"<br>RegionData5.txt = 区域本部上年同期累计.txt</br>";
    			tSendMail.setFile(mZipFilePath);
    			tSendMail.send(tMailContent, "理赔批处理-Region提数");
    		} catch (Exception ex) {
    			System.out.println("邮件发送失败："+ex.toString());
    			ex.printStackTrace();
    		}
    	}
    	
    }
    
    /**
     * 通过SQL语句查询结果，并写入txt文件
     *
     */
    private void getDataResult()
    {
    	System.out.println("开始区域SQL处理");
    	//区域本部本月.txt
    	mZipDocFileName[0] = new String (mURL+"RegionData1.txt");
    	mSQL ="select sum(num), double(ROUND(sum(pay), 2)), double(ROUND(sum(amnt), 2))  from (select count(distinct b.otherno) num,               sum(ROUND(b.pay / 10000, 2)) pay,               sum(ROUND(a.amnt / 10000, 2)) amnt          from lcpol a, ljagetclaim b         where a.polno = b.polno           and a.contno = b.contno           and a.managecom = '86210000'           and b.makedate between current date - 1 MONTHS - 1         DAYS           and current date - 2 DAYS           and b.othernotype = '5'           and exists (select 1                  from lfrisk                 where risktype = 'H'                   and riskcode = a.riskcode)        union        select count(distinct b.otherno) num,               sum(ROUND(b.pay / 10000, 2)) pay,               sum(ROUND(a.amnt / 10000, 2)) amnt          from lbpol a, ljagetclaim b         where a.polno = b.polno           and a.contno = b.contno           and a.managecom = '86210000'           and b.makedate between current date - 1 MONTHS - 1         DAYS           and current date - 2 DAYS           and b.othernotype = '5'           and exists (select 1                  from lfrisk                 where risktype = 'H'                   and riskcode = a.riskcode)) as aa with ur";
    	writeToTxt(mZipDocFileName[0],mSQL);
        //区域本部上月.txt
    	mZipDocFileName[1] = new String (mURL+"RegionData2.txt");
    	mSQL ="select sum(num), double(ROUND(sum(pay), 2)), double(ROUND(sum(amnt), 2))  from (select count(distinct b.otherno) num,               sum(ROUND(b.pay / 10000, 2)) pay,               sum(ROUND(a.amnt / 10000, 2)) amnt          from lcpol a, ljagetclaim b         where a.polno = b.polno           and a.contno = b.contno           and a.managecom = '86210000'           and b.makedate between current date - 2 MONTHS - 1         DAYS           and current date - 1 MONTHS - 2 DAYS           and b.othernotype = '5'           and exists (select 1                  from lfrisk                 where risktype = 'H'                   and riskcode = a.riskcode)        union        select count(distinct b.otherno) num,               sum(ROUND(b.pay / 10000, 2)) pay,               sum(ROUND(a.amnt / 10000, 2)) amnt          from lbpol a, ljagetclaim b         where a.polno = b.polno           and a.contno = b.contno           and a.managecom = '86210000'           and b.makedate between current date - 2 MONTHS - 1         DAYS           and current date - 1 MONTHS - 2 DAYS           and b.othernotype = '5'           and exists (select 1                  from lfrisk                 where risktype = 'H'                   and riskcode = a.riskcode)) as aa with ur";
    	writeToTxt(mZipDocFileName[1],mSQL);
    	//区域本部本年.txt
    	mZipDocFileName[2] = new String (mURL+"RegionData3.txt");
    	mSQL ="select sum(num), double(ROUND(sum(pay), 2)), double(ROUND(sum(amnt), 2))  from (select count(distinct b.otherno) num,               sum(ROUND(b.pay / 10000, 2)) pay,               sum(ROUND(a.amnt / 10000, 2)) amnt          from lcpol a, ljagetclaim b         where a.polno = b.polno           and a.contno = b.contno           and a.managecom = '86210000'           and b.makedate between               trim(char(YEAR(current date))) || char('-01') || char('-01') and               current date - 2 DAYS           and b.othernotype = '5'           and exists (select 1                  from lfrisk                 where risktype = 'H'                   and riskcode = a.riskcode)        union        select count(distinct b.otherno) num,               sum(ROUND(b.pay / 10000, 2)) pay,               sum(ROUND(a.amnt / 10000, 2)) amnt          from lbpol a, ljagetclaim b         where a.polno = b.polno           and a.contno = b.contno           and a.managecom = '86210000'           and b.makedate between               trim(char(YEAR(current date))) || char('-01') || char('-01') and               current date - 2 DAYS           and b.othernotype = '5'           and exists (select 1                  from lfrisk                 where risktype = 'H'                   and riskcode = a.riskcode)) as aa with ur";
    	writeToTxt(mZipDocFileName[2],mSQL);
    	//区域本部上年同期.txt
    	mZipDocFileName[3] = new String (mURL+"RegionData4.txt");
    	mSQL ="select sum(num), double(ROUND(sum(pay), 2)), double(ROUND(sum(amnt), 2))  from (select count(distinct b.otherno) num,               sum(ROUND(b.pay / 10000, 2)) pay,               sum(ROUND(a.amnt / 10000, 2)) amnt          from lcpol a, ljagetclaim b         where a.polno = b.polno           and a.contno = b.contno           and a.managecom = '86210000'           and b.makedate between current date - 1 YEAR - 1 MONTHS - 1         DAYS           and current date - 1 YEAR - 2 DAYS           and b.othernotype = '5'           and exists (select 1                  from lfrisk                 where risktype = 'H'                   and riskcode = a.riskcode)        union        select count(distinct b.otherno) num,               sum(ROUND(b.pay / 10000, 2)) pay,               sum(ROUND(a.amnt / 10000, 2)) amnt          from lbpol a, ljagetclaim b         where a.polno = b.polno           and a.contno = b.contno           and a.managecom = '86210000'           and b.makedate between current date - 1 YEAR - 1 MONTHS - 1         DAYS           and current date - 1 YEAR - 2 DAYS           and b.othernotype = '5'           and exists (select 1                  from lfrisk                 where risktype = 'H'                   and riskcode = a.riskcode)) as aa with ur";
    	writeToTxt(mZipDocFileName[3],mSQL);
    	//区域本部上年同期累计.txt
    	mZipDocFileName[4] = new String (mURL+"RegionData5.txt");
    	mSQL ="select sum(num), double(ROUND(sum(pay), 2)), double(ROUND(sum(amnt), 2))  from (select count(distinct b.otherno) num,               sum(ROUND(b.pay / 10000, 2)) pay,               sum(ROUND(a.amnt / 10000, 2)) amnt          from lcpol a, ljagetclaim b         where a.polno = b.polno           and a.contno = b.contno           and a.managecom = '86210000'           and b.makedate between trim(char(YEAR(current date - 1 YEAR))) ||               char('-01') || char('-01') and current date - 1         YEAR - 2 DAYS           and b.othernotype = '5'           and exists (select 1                  from lfrisk                 where risktype = 'H'                   and riskcode = a.riskcode)        union        select count(distinct b.otherno) num,               sum(ROUND(b.pay / 10000, 2)) pay,               sum(ROUND(a.amnt / 10000, 2)) amnt          from lbpol a, ljagetclaim b         where a.polno = b.polno           and a.contno = b.contno           and a.managecom = '86210000'           and b.makedate between trim(char(YEAR(current date - 1 YEAR))) ||               char('-01') || char('-01') and current date - 1         YEAR - 2 DAYS           and b.othernotype = '5'           and exists (select 1                  from lfrisk                 where risktype = 'H'                   and riskcode = a.riskcode)) as aa with ur";
    	writeToTxt(mZipDocFileName[4],mSQL);

    	System.out.println("结束区域SQL处理");

    }
    
    /**
     * 通过传入的文件名及SQL语句，生成对应的结果集合文件
     * @param tZipDocFileName
     * @param tSQL
     */
    
    private void writeToTxt(String tZipDocFileName,String tSQL)
    {
        try
        {	
            int start = 1;
            int nCount = 200;
            RSWrapper rswrapper = new RSWrapper();
            rswrapper.prepareData(null, tSQL);
            SSRS tSSRS = null;
            do
            {
                tSSRS = rswrapper.getSSRS();

                if (tSSRS.getMaxRow() <= 0)
                {
                	break;
                }
                
                String tempString = rswrapper.encode();
                String[] tempStringArr = tempString.split("\\^");
                
                FileOutputStream  tFileOutputStream = new FileOutputStream(tZipDocFileName);
                
                OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                        tFileOutputStream, "GBK");
                mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    String t = tempStringArr[i].replaceAll("\\|", ",");
                    String[] tArr = t.split("\\,");
                
                    for (int j = 0; j < tArr.length - 1; j++)
                    {
                        mBufferedWriter.write(tArr[j] + ",");
                    }
                    mBufferedWriter.write(tArr[tArr.length - 1] );
                    mBufferedWriter.newLine();
                    mBufferedWriter.flush();
                }
                
                mBufferedWriter.close();
                tOutputStreamWriter.close();
                start += nCount;
            }
            while (tSSRS.getMaxRow() > 0);
        } catch (IOException ex2)
        {
        	this.mErrors.addOneError("数据提取或写文件失败：" +ex2.getStackTrace() );
        	System.out.println(ex2.getStackTrace());
        }
    }
    	
    
    
    /**
     * 将给定目录下的文件集合打包为压缩包
     *
     */
    private void getZipFile()
    {
    	 
        mZipFilePath = mURL+"RegionDataExtraction.zip";
    	System.out.println("压缩路径名：" + mZipFilePath);
    	CreateZipFile(mZipDocFileName,mZipFilePath);
    }
    
    /**
     * 添加到压缩文件
     * @param zipfilepath String
     * @param out ZipOutputStream
     * @param tZipDocFileName String：zip文件名
     * @param tDocFileName String：待压缩的文件路径名
     * @return int：1：成功、0：失败，但不中断整个程序、-1：失败，中断程序
     */
    public boolean CreateZipFile(String[] tInputEntry, String tZipFile) {
		final int BUFFER = 2048;
		BufferedInputStream origin = null;
		FileOutputStream f = null;
		//org.apache.tools.zip.ZipOutputStream 
		ZipOutputStream out = null;

		int fileCount = tInputEntry.length;
		String[] tOutputEntry = new String[fileCount];
		try {
			origin = null;
			f = new FileOutputStream(tZipFile);			
			out = new ZipOutputStream(new BufferedOutputStream(f));
				//new org.apache.tools.zip.ZipOutputStream(new BufferedOutputStream(f));
			byte data[] = new byte[BUFFER];
			for (int i = 0; i < fileCount; i++) {
				FileInputStream fi = new FileInputStream(tInputEntry[i]);
				origin = new BufferedInputStream(fi, BUFFER);

				int index = 0;
				if (tInputEntry[i].lastIndexOf("/") != -1) {
					index = tInputEntry[i].lastIndexOf("/") + 1;
				} else {
					index = tInputEntry[i].lastIndexOf("\\") + 1;
				}
				tOutputEntry[i] = tInputEntry[i].substring(index);

				ZipEntry entry = new ZipEntry(tOutputEntry[i]);
				//out.putNextEntry(new org.apache.tools.zip.ZipEntry(tOutputEntry[i]));
				out.putNextEntry(entry);
				int count;
				while ((count = origin.read(data, 0, BUFFER)) != -1) {
					out.write(data, 0, count);
				}
				origin.close();
			}
			out.close();
		} catch (Exception ex) {
			return false;
		} finally {
			try {
				origin.close();
				out.close();
			} catch (Exception ex) {
			}
		}
		return true;
	}
    
    /**
     * 删除生成的ZIP文件和XML文件
     * @param cFilePath String：完整文件名
     */
    private boolean deleteFile(String cFilePath)
    {
        File f = new File(cFilePath);
        if (!f.exists())
        {
            CError tError = new CError();
            tError.moduleName = "MonthDataCatch";
            tError.functionName = "deleteFile";
            tError.errorMessage = "没有找到需要删除的文件";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        f.delete();
        return true;
    }

}
