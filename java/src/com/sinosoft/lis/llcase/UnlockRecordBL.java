package com.sinosoft.lis.llcase;


import com.sinosoft.lis.db.LLLockDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLLockRecordSchema;
import com.sinosoft.lis.schema.LLLockSchema;
import com.sinosoft.lis.vschema.LLLockRecordSet;
import com.sinosoft.lis.vschema.LLLockSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class UnlockRecordBL {

	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mG = new GlobalInput();
    private PubFun1 pf1 = new PubFun1();
    private PubFun pf =new PubFun();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LLLockSet mLLLockSet = new LLLockSet();
    private String mReturnMessage = "";
    private String aDate = "";
    private String aTime = "";
    public UnlockRecordBL() {
    }

    public static void main(String[] args) {
        
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行数据基本校验
        if (!checkData())
            return false;
        //进行业务处理
        if (!dealData()) {
            mReturnMessage += "数据处理失败SimpleClaimAuditBL-->dealData!";
            CError.buildErr(this,mReturnMessage);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, null)) {
            CError.buildErr(this,"数据提交失败!");
            return false;
        }
        System.out.println("End SimpleClaimAuditBL Submit...");
        return true;
    }
    

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("--Star GetInputData--");
        mLLLockSet = ((LLLockSet) cInputData.getObjectByObjectName("LLLockSet", 0));
        mG = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        aDate = PubFun.getCurrentDate();
        aTime = PubFun.getCurrentTime();
        return true;
    }

    /**
     * 处理前检查数据是否合理
     * @return boolean
     */
    private boolean checkData(){
        System.out.println("---start checkData---");
        LLLockSet tLLLockSet = mLLLockSet;
        mLLLockSet = new LLLockSet();
        for(int i=1;i<=tLLLockSet.size();i++) {
        	LLLockDB tLLLockDB = new LLLockDB();
        	String tGrpcontNo = tLLLockSet.get(i).getGrpContNo();
        	String tRgtno = tLLLockSet.get(i).getRgtno();
        	tLLLockDB.setGrpContNo(tGrpcontNo);
        	tLLLockDB.setRgtno(tRgtno);
        	if(!tLLLockDB.getInfo()) {
        		mReturnMessage += "<br>个人案件查询失败,保单号："+tGrpcontNo + ",批次号：" + tRgtno;
                continue;
        	}
        	mLLLockSet.add(tLLLockDB.getSchema());
        }
        if(mLLLockSet.size()<0) {
        	mReturnMessage +="<br>没有符合审定条件的案件。";
            CError.buildErr(this,mReturnMessage);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("---Star dealData---mOperate: " + mOperate);
        if(mOperate.equals("update")) {
        	System.out.println("---Star ---" + mLLLockSet.size());
        	LLLockSet tLLLockSet = new LLLockSet();
        	LLLockRecordSet tLLLockRecordSet = new LLLockRecordSet();
        	int tSize = mLLLockSet.size();
        	boolean tflag= false;
        	for(int i=1;i<=tSize;i++) {
        		LLLockSchema aLLLockSchema = mLLLockSet.get(i);
        		aLLLockSchema.setOperator(mG.Operator);
        		aLLLockSchema.setManagecom(mG.ManageCom);
        		aLLLockSchema.setLockDate(aDate);
        		aLLLockSchema.setLockTime(aTime);
            	aLLLockSchema.setState("2");
            	aLLLockSchema.setModifyDate(aDate);
            	aLLLockSchema.setModifyTime(aTime);
        		tLLLockSet.add(aLLLockSchema);
        		
        		LLLockRecordSchema tLLLockRecordSchema = new LLLockRecordSchema();
        		String fTimeNo = pf.getCurrentDate2();
        		String serialno = "LS"+fTimeNo+""+ pf1.CreateMaxNo(fTimeNo+"JS", 8);
        		System.out.println("serialno= " + serialno);
        		//流水号
        		//历史实收保费
        		tLLLockRecordSchema.setLLFlowNumber(serialno);
        		tLLLockRecordSchema.setSumGetFee(aLLLockSchema.getSumGetFee());
        		tLLLockRecordSchema.setGrpContNo(aLLLockSchema.getGrpContNo());
        		tLLLockRecordSchema.setRgtno(aLLLockSchema.getRgtno());
        		tLLLockRecordSchema.setState("2");
        		tLLLockRecordSchema.setLockDate(aDate);
        		tLLLockRecordSchema.setLockTime(aTime);
        		tLLLockRecordSchema.setOperator(mG.Operator);
        		tLLLockRecordSchema.setManagecom(mG.ManageCom);
        		tLLLockRecordSchema.setMakeDate(aDate);
        		tLLLockRecordSchema.setMakeTime(aTime);
        		tLLLockRecordSchema.setModifyDate(aDate);
        		tLLLockRecordSchema.setModifyTime(aTime);
        		tLLLockRecordSchema.setLockAttri("mnaual");//手动解锁
        		tLLLockRecordSet.add(tLLLockRecordSchema);
        		tflag = true;
        	}
        	if(tflag) {
        		map.put(tLLLockSet, "UPDATE");
        		map.put(tLLLockRecordSet, "INSERT");
        	}
        }
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            mInputData.add(map);
            mResult.clear();
            mResult.add(mReturnMessage);
        } catch (Exception ex) {
            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

}
