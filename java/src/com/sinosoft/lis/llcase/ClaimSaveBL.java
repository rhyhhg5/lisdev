package com.sinosoft.lis.llcase;

import java.util.Date;

import org.jdom.Document;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;

/**
 * <p>Title: ClaimSave</p>
 * <p>Description: 赔付结算保存业务逻辑处理类(自动核赔)</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Xx
 * @version 1.0
 */
public class ClaimSaveBL {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    private String mClmNo = "";
    private String mCaseNo = "";
    private String mRgtNo = "";
    private String aDate = "";
    private String aTime = "";
    private String mPolNo="";
    // 1500 康乐人生
    private String mOperator = "";
    private String mManageCom = "";
    //用于理赔状态的判断
    private String mGiveType = "";

    private LLClaimSchema mLLClaimSchema = new LLClaimSchema();
    private LLClaimDetailSet mLLClaimDetailSet = new LLClaimDetailSet();
    private LLClaimPolicySet mLLClaimPolicySet = new LLClaimPolicySet();
    private GlobalInput mG = new GlobalInput();
    private String backMsg = "";

    private LLCaseSchema mLLCaseSchema = null;
    private LLCaseRelaSet mLLCaseRelaSet = null;
    private LLSubReportSet mLLSubReportSet = new LLSubReportSet();
    private String mCaseProp = "";
    
    //#2366红利金额合计
    private double mHLtotal=0.0;
    //#1617 分红险理赔案件处理调整
    /** 为理赔红利设置的给付通知书号 */
    private String mGetNoticeNo = "";
    private MMap tmpMap = new MMap();
    public ClaimSaveBL() {}

    public String getBackMsg() {
        return backMsg;
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        System.out.println("ClaimSaveBL begin......");
      
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 检查案件信息
        if (!checkCaseInfo()) {
            return false;
        }
        if (!checkMoney()) {
            return false;
        }
        //4205 ***start*** 调用规则引擎校验重大疾病信息的录入
        if("SAVE".equals(cOperate)){
        	if(!checkRule()) {
            	return false;
            }
        }
        
        //4205 ***end***
//        if (!checkAmnt()) {
//            return false;
//        }
        System.out.println("---dealData---");
        if (!dealData()) {
            return false;
        }
        if (!getInsuAcc()) {
            return false;
        }
        if (mLLCaseSchema.getRgtType().equals("4") ||
            mLLCaseSchema.getRgtType().equals("5")) {
            if (!dealAppeal()) {
                return false;
            }
        }
        if (!dealWNAcc()) {
            return false;
        }
      
        //#1500 重大疾病且特定疾病的保单豁免
        if(!dealExemption()){
        	return false;
        }
        
        if (!uwCheck()) {
            return false;
        }

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(this.mResult, null)) {
            CError.buildErr(this, "数据保存失败");
            return false;
        }

        return true;
    }
    //2366关于理赔案件处理时红利处理**start**新增代码  
    /**
     * 红利理赔生成临时应付记录
     * @param aLPEdorItemSchema
     * @param aLCPolSchema
     * @param aLCDutySchema
     * @param aOperationType
     * @param aFeeType
     * @param aGetMoney
     * @param aGlobalInput
     * @return
     */
    public LJSGetClaimSchema initLJSGetClaim(LCPolSchema aLCPolSchema, 
    		String aFeeType, double aGetMoney, GlobalInput aGlobalInput)
    {
    	LJSGetClaimSchema tLJSGetClaimSchema = new LJSGetClaimSchema();
    	
    	try
        {
            tLJSGetClaimSchema.setFeeFinaType(aFeeType);//理赔红利的补退费财务类型定义为HLLX、HLBF
            tLJSGetClaimSchema.setGetNoticeNo(mGetNoticeNo); //给付通知书号码 红利为新生成给付号
            tLJSGetClaimSchema.setFeeOperationType("HL");//理赔红利的补退费业务类型定义为HL
            tLJSGetClaimSchema.setPay(aGetMoney);
            tLJSGetClaimSchema.setGrpContNo(aLCPolSchema.getGrpContNo());
            tLJSGetClaimSchema.setGrpPolNo(aLCPolSchema.getGrpPolNo());
            tLJSGetClaimSchema.setContNo(aLCPolSchema.getContNo());
            tLJSGetClaimSchema.setPolNo(aLCPolSchema.getPolNo());

            tLJSGetClaimSchema.setOtherNo(mCaseNo); //其他号码置为理赔案件号
            tLJSGetClaimSchema.setOtherNoType("H"); //理赔红利的实付类型
            tLJSGetClaimSchema.setAgentCode(aLCPolSchema.getAgentCode());
            tLJSGetClaimSchema.setAgentCom(aLCPolSchema.getAgentCom());
            tLJSGetClaimSchema.setAgentGroup(aLCPolSchema.getAgentGroup());
            tLJSGetClaimSchema.setAgentType(aLCPolSchema.getAgentType());

            tLJSGetClaimSchema.setKindCode(aLCPolSchema.getKindCode());
            tLJSGetClaimSchema.setRiskCode(aLCPolSchema.getRiskCode());
            tLJSGetClaimSchema.setRiskVersion(aLCPolSchema.getRiskVersion());
            tLJSGetClaimSchema.setManageCom(aLCPolSchema.getManageCom());
            tLJSGetClaimSchema.setOperator(aGlobalInput.Operator);
            tLJSGetClaimSchema.setMakeDate(PubFun.getCurrentDate());
            tLJSGetClaimSchema.setMakeTime(PubFun.getCurrentTime());
            tLJSGetClaimSchema.setModifyDate(PubFun.getCurrentDate());
            tLJSGetClaimSchema.setModifyTime(PubFun.getCurrentTime());
        }
        catch (Exception ex)
        {
            mErrors.addOneError(new CError("红利理赔生成临时应付记录异常！"));
            return null;
        }
        return tLJSGetClaimSchema;
    }

    
    /**
     * 获取分红险种年期SQL
     * @param tLCBonusPolSchema
     * @return
     */
    private String getPayYearsSQL(LCPolSchema tLCBonusPolSchema){
    	String payYearsSQL = "";
    	if("730101".equals(tLCBonusPolSchema.getRiskCode())){
    		payYearsSQL = "case when "+tLCBonusPolSchema.getInsuYear()+" in (5,6) then 1 else "+tLCBonusPolSchema.getPayEndYear()+" end ";
    	}else if ("730201".equals(tLCBonusPolSchema.getRiskCode())){
    		payYearsSQL = "case when "+tLCBonusPolSchema.getPayIntv()+" =0 then 1 else "+tLCBonusPolSchema.getPayEndYear()+" end ";
    	}
    	return payYearsSQL;
    }
    
    /**
     * 校验是否为分红险种及分红险种的分红率
     * 
     * @return
     */
    private boolean CaseHasBonusRisk(String tContNo,String tRiskCode){
    	ExeSQL mExeSQL=new ExeSQL();
    	if(!LLCaseCommon.checkFHRisk(tRiskCode)){
    		System.out.println("本次不涉及分红险种的理算!");
    		return false;
    	}else{
    		LCPolSet tLCBonusPolSet=new LCPolDB().executeQuery("select * from lcpol where contno='"+tContNo+"' and riskcode in (select riskcode from lmriskapp where risktype4='2') with ur");
    		if(tLCBonusPolSet.size()==1){
    			LCPolSchema tLCBonusPolSchema=tLCBonusPolSet.get(1);
    			String tchecksql1="select 1 from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
                    "  and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear,APPYEAR desc with ur";
    			String tcheckresult1=mExeSQL.getOneValue(tchecksql1);
    			//校验该险种是否有分红率
        		if(tcheckresult1==null||tcheckresult1.equals("")||tcheckresult1.equals("null")){
        			System.out.println("本次理赔理算分红险种未录入分红率,跳过分红处理!");
            		return false;
            	}
    		}else{
    			System.out.println("保全已经进行过解约操作，本次不涉及分红险种表的理赔理算!");
        		return false;
    		}
    	}
    	
    	return true;
    }

    /**
     * TODO:理算过程中红利计算规则，同保全退保时的红利计算规则
     * 1.理赔事件发生之后不计算累积生息的利息
     * 2.若理赔事件发生日期时该保单年度对应分红率尚未公布，则以最后一次（上一年度）公布的分红率来计算相关分红
     * @return
     */
    private boolean dealFHLX() {
    	  	
    	ExeSQL mExeSQL=new ExeSQL();
    	//1.理赔可能赔付多个险种，需要循环处理llclaimdetail中的险种信息

    	if(mLLClaimDetailSet!= null && mLLClaimDetailSet.size()>0){
    		outer:
    		for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
    			LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
    			tLLClaimDetailSchema = mLLClaimDetailSet.get(i);
    			String tGiveType=tLLClaimDetailSchema.getGiveType();
    			double tPayMoney= tLLClaimDetailSchema.getRealPay();
    			if ("3".equals(tGiveType)||tPayMoney ==0){//全额拒付不处理或实赔金额为0不处理分红
    	        	continue outer;
    	        }
    			String tContNo = tLLClaimDetailSchema.getContNo();
    			String tRiskCode = tLLClaimDetailSchema.getRiskCode();
    			String tPolNo = tLLClaimDetailSchema.getPolNo();
    			String tAppDate = "";//2366理赔受理页面录入的“申请日期”，相当于保全的退保日期
    			String srSQL = "select appdate from llregister where rgtno='"
    				+ mCaseNo+ "'";
    			tAppDate = mExeSQL.getOneValue(srSQL);
    			if(tAppDate == null || tAppDate.equals("")||tAppDate.equals("null")){
    				CError tError = new CError();
        			tError.moduleName = "ClaimSaveBL";
        			tError.functionName = "dealFHLX";
        			tError.errorMessage = "申请日期查询失败!";
        			System.out.println("mCaseNo："+mCaseNo+"对应的案件申请日期查询失败!");
        			mErrors.addOneError(tError);
        			return false;
    			}
    			Date tAppDatee = CommonBL.stringToDate(tAppDate);
    			//#2366若申请日期在保全领取分红日期之前提示不处理分红
    			//最后一次领取分红的时间
    			String sqllj = "select max(getdate) from ljabonusget where otherno='"+tContNo+"' with ur";
    			String lastdate1 = mExeSQL.getOneValue(sqllj);
    			
    	        if(lastdate1 != null && !"".equals(lastdate1)){
    	        	//申请日期早于最后分红领取时间
    	        	Date lastdate11 = CommonBL.stringToDate(lastdate1);
    	        	if(lastdate11.after(tAppDatee)){
        				System.out.println("申请日期在领取之前之前，本次案件不再计算分红信息");
            			return false;
    	        	}
    	        }
    		     //添加逻辑，若保全发生过红利领取方式变更，则不计算分红
    	        String sqlfhlq = "select 1 from lccont a, lpedoritem b, lpedorapp c " +
    	        		" where 1=1 " +
    	        		" and a.contno=b.contno" +
    	        		" and b.edorno=c.edoracceptno" +
    	        		" and b.edortype='DD' " +
    	        		" and c.edorstate='0' " +
    	        		" and a.conttype='1' " +
    	        		" and a.contno='" +tContNo+"' " +
    	        		" union " +
    	        		" select 1 " +
    	        		" from lbcont a, lpedoritem b, lpedorapp c " +
    	        		" where 1=1  and a.contno=b.contno  and a.edorno=b.edorno and b.edorno=c.edoracceptno " +
    	        		" and b.edortype='DD' " +
    	        		" and c.edorstate='0' " +
    	        		" and a.conttype='1' " +
    	        		" and a.contno='" +tContNo+"' ";
    	        String checkFHchange = mExeSQL.getOneValue(sqlfhlq);
    	        if(checkFHchange != null && !"".equals(checkFHchange)){
        				System.out.println("保全存在领取方式变更的情况，不计算分红");
            			return false;
    	        }
    	        
    	        
    			//有分红险，且存在分红利率
		        String delsql1 =
		            "delete from ljsgetclaim where othernotype in ('H') and otherno='"
		            + mCaseNo + "'";
		        String delsql2 =
		               "delete from ljsget where othernotype in ('H') and otherno='"
		               + mCaseNo + "'";
		        tmpMap.put(delsql1, "DELETE");
		        tmpMap.put(delsql2, "DELETE");
		        
    	    	LJSGetClaimSet aSaveLJSGetClaim = new LJSGetClaimSet();//存储临时应付
    	    	LJSGetSchema aSaveLJSGetSchema = new LJSGetSchema();
    	    	double aTotalReturn = 0;//将红利产生的金额汇总到LJSGet中
    	    	
    			if(CaseHasBonusRisk(tContNo,tRiskCode)){
//    			    String strLimit = PubFun.getNoLimit(mG.ManageCom);
    			    //String tGetNoticeNo = PubFun1.CreateMaxNo("GetNoticeNo", strLimit);	     
    			    //#1617 对于一个案件来说，只需要有一个给付通知书号，但是理赔红利需要新的给付通知书号
    			    //mGetNoticeNo = tGetNoticeNo;
    				LJSGetClaimSchema tBonusLJS = new LJSGetClaimSchema();
	                //查询LCPol表
	            	LCPolDB tLCBonusPolDB = new LCPolDB();
	            	tLCBonusPolDB.setPolNo(tPolNo);
	            	tLCBonusPolDB.getInfo();
	            	LCPolSchema tLCBonusPolSchema = new LCPolSchema();
	            	tLCBonusPolSchema.setSchema(tLCBonusPolDB.getSchema());
	            	//指最后一次分红日期
	            	String tLastBonusDate=mExeSQL.getOneValue("select max(SGetDate) from LOBonusPol where polno='"+tLCBonusPolSchema.getPolNo()+"' ");
	            	//指保单有效年期
	            	String tCvaliYear=mExeSQL.getOneValue("select year('"+tLCBonusPolSchema.getCValiDate()+"') from dual with ur");
	            	//指最后一次保单复效日期
	            	String tFXdate=mExeSQL.getOneValue("select max(enddate) from lccontstate where enddate is not null and polno='"+tLCBonusPolSchema.getPolNo()+"' and statetype='Available' ");
	            	if(tCvaliYear==null||tCvaliYear.equals("")||tCvaliYear.equals("null")){
	            		// @@错误处理
	        			CError tError = new CError();
	        			tError.moduleName = "ClaimSaveBL";
	        			tError.functionName = "dealFHLX";
	        			tError.errorMessage = "获取生效日期失败";
	        			mErrors.addOneError(tError);
	        			return false;
	            	}
	            	LMRiskBonusDB tLMRiskBonusDB=new LMRiskBonusDB();
	            	tLMRiskBonusDB.setRiskCode(tLCBonusPolSchema.getRiskCode());
	            	LMRiskBonusSet tLMRiskBonusSet=tLMRiskBonusDB.query();
	            	if(tLMRiskBonusSet.size()!=1){
	            		// @@错误处理
	        			CError tError = new CError();
	        			tError.moduleName = "ClaimSaveBL";
	        			tError.functionName = "dealFHLX";
	        			tError.errorMessage = "获取分红公式失败";
	        			mErrors.addOneError(tError);
	        			return false;
	            	}
	            	//从未进行过保全分红
	            	if(tLastBonusDate.equals("")){
	            		//直至理赔申请，保单有效期在1年及以上
	            		if(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),tAppDate, "Y")>0){
	            			LMRiskBonusRateDB tLMRiskBonusRateDB =new LMRiskBonusRateDB();
	                    	LMRiskBonusRateSet tLMRiskBonusRateSet=new LMRiskBonusRateSet();
	                    	//取红利，该保单年度对应分红率尚未公布，则以最后一次（上一年度）公布的分红率来计算相关分红
	                    	//保全+1指前一年保单红利
	                    	String tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"' and bonusyear=(case when month('"+tLCBonusPolSchema.getCValiDate()+"')>6 then char(year('"+tLCBonusPolSchema.getCValiDate()+"')+1) else char(year('"+tLCBonusPolSchema.getCValiDate()+"')) end) " +
	                			" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" with ur";
	                    	System.out.println("tratesql:"+tratesql);
	                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
	                    	if(tLMRiskBonusRateSet.size()<1){
	                    		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
	                    			" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear desc with ur";
	                        	System.out.println("tratesql:"+tratesql);
	                        	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
	                        	if(tLMRiskBonusRateSet.size()<1){
	                        		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
	                        		"  and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear,APPYEAR desc with ur";
	                            	System.out.println("tratesql:"+tratesql);
	                            	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
	                            	if(tLMRiskBonusRateSet.size()<1){
	                            		// @@错误处理
	        							CError tError = new CError();
	        							tError.moduleName = "ClaimSaveBL";
	        							tError.functionName = "dealFHLX";
	        							tError.errorMessage = "获取分红率失败!";
	        							mErrors.addOneError(tError);
	        							return false;
	                            	}
	                        	}
	                    	}
	                    	Calculator tCalculator = new Calculator();
	                        tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusGetCode());
	                        tCalculator.addBasicFactor("Prem",String.valueOf(tLCBonusPolSchema.getPrem()));
	                        tCalculator.addBasicFactor("Get",String.valueOf(tLCBonusPolSchema.getAmnt()));
	                        tCalculator.addBasicFactor("payendyear",String.valueOf(tLCBonusPolSchema.getPayEndYear()));
	                        tCalculator.addBasicFactor("payintv",String.valueOf(tLCBonusPolSchema.getPayIntv()));
	                        tCalculator.addBasicFactor("insuyear",String.valueOf(tLCBonusPolSchema.getInsuYear()));
	                        tCalculator.addBasicFactor("sex",tLCBonusPolSchema.getInsuredSex());
	                        tCalculator.addBasicFactor("appyear",tCvaliYear);
	                        tCalculator.addBasicFactor("insuredage",String.valueOf(tLCBonusPolSchema.getInsuredAppAge()));
	                        //保单生效日至理赔申请日期间的保险年期
	                        tCalculator.addBasicFactor("polyear",String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),tAppDate, "Y")-1));
	                        //tCalculator.addBasicFactor("rate1",String.valueOf(tLMRiskBonusRateSet.get(1).getHLRate()));
	                        tCalculator.addBasicFactor("bonusyear",tLMRiskBonusRateSet.get(1).getBonusYear());
	                        tCalculator.addBasicFactor("enddate",PubFun.calDate(tLCBonusPolSchema.getCValiDate(), 1, "Y", null));
	                        //红利金额
	                        double tHL=0;
	                        double tHLLXnoAcc=0;
	                        if((!tFXdate.equals(""))&&PubFun.getBeforeDate(tFXdate, PubFun.calDate(tLCBonusPolSchema.getCValiDate(), 1, "Y", null)).equals(tFXdate)){
	                        	tCalculator.addBasicFactor("startdate",tFXdate);
	                            tCalculator.addBasicFactor("caltype","FX");
	                            tHL= Double.parseDouble((tCalculator.calculate()));
	                            tHLLXnoAcc=tHL;
	                        }else if(tFXdate.equals("")){
	                        	tCalculator.addBasicFactor("startdate",tLCBonusPolSchema.getCValiDate());
	                            tCalculator.addBasicFactor("caltype","YX");
	                            tHL= Double.parseDouble((tCalculator.calculate()));
	                            tHLLXnoAcc=tHL;
	                        }else{
	                            tHL= 0;
	                            tHLLXnoAcc=tHL;
	                        }    
	                        
	                        //此处处理+2部分的问题，指当年分红红利
	                        tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"' and bonusyear=(case when month('"+tLCBonusPolSchema.getCValiDate()+"')>6 then char(year('"+tLCBonusPolSchema.getCValiDate()+"')+2) else char(year('"+tLCBonusPolSchema.getCValiDate()+"')+1) end) " +
	                			" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" with ur";
	                    	System.out.println("tratesql:"+tratesql);
	                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
	                    	if(tLMRiskBonusRateSet.size()<1){
	                    		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
	                    			" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear desc with ur";
	                        	System.out.println("tratesql:"+tratesql);
	                        	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
	                        	if(tLMRiskBonusRateSet.size()<1){
	                        		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
	                        			"  and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear,APPYEAR desc with ur";
	                            	System.out.println("tratesql:"+tratesql);
	                            	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
	                            	if(tLMRiskBonusRateSet.size()<1){
	                            		// @@错误处理
	        							CError tError = new CError();
	        							tError.moduleName = "ClaimSaveBL";
	        							tError.functionName = "dealFHLX";
	        							tError.errorMessage = "获取分红率失败!";
	        							mErrors.addOneError(tError);
	        							return false;
	                            	}
	                        	}
	                    	}
	                        
	                    	tCalculator = new Calculator();
	                        tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusGetCode());
	                        tCalculator.addBasicFactor("Prem",String.valueOf(tLCBonusPolSchema.getPrem()));
	                        tCalculator.addBasicFactor("Get",String.valueOf(tLCBonusPolSchema.getAmnt()));
	                        tCalculator.addBasicFactor("payendyear",String.valueOf(tLCBonusPolSchema.getPayEndYear()));
	                        tCalculator.addBasicFactor("payintv",String.valueOf(tLCBonusPolSchema.getPayIntv()));
	                        tCalculator.addBasicFactor("insuyear",String.valueOf(tLCBonusPolSchema.getInsuYear()));
	                        tCalculator.addBasicFactor("sex",tLCBonusPolSchema.getInsuredSex());
	                        tCalculator.addBasicFactor("appyear",tCvaliYear);
	                        tCalculator.addBasicFactor("insuredage",String.valueOf(tLCBonusPolSchema.getInsuredAppAge()));
	                        //保单生效日至理赔申请日期间的保险年期
	                        tCalculator.addBasicFactor("polyear",String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),tAppDate, "Y")));
	                        //tCalculator.addBasicFactor("rate1",String.valueOf(tLMRiskBonusRateSet.get(1).getHLRate()));
	                        tCalculator.addBasicFactor("bonusyear",tLMRiskBonusRateSet.get(1).getBonusYear());
	                        //考虑保单复效的情况，修改要素值
	                        if((!tFXdate.equals(""))&&PubFun.getLaterDate(tFXdate, PubFun.calDate(tLCBonusPolSchema.getCValiDate(), 1, "Y", null)).equals(tFXdate)&&PubFun.getBeforeDate(tFXdate, tAppDate).equals(tFXdate)){
	                        	tCalculator.addBasicFactor("startdate",tFXdate);
	                        }else{
	                        	tCalculator.addBasicFactor("startdate",PubFun.calDate(tLCBonusPolSchema.getCValiDate(), 1, "Y", null));                        	
	                        }  
	                        //理赔申请日，同保全退保时的生效日
	                        tCalculator.addBasicFactor("enddate",tAppDate);
	                        tCalculator.addBasicFactor("caltype","JY");
	                        tHL += Double.parseDouble(tCalculator.calculate());
	                        tHL=CommonBL.carry(tHL);
	                    	//生成理赔应付
	                        tBonusLJS = initLJSGetClaim(tLCBonusPolSchema,"HLBF",tHL,mG);
	                        aSaveLJSGetClaim.add(tBonusLJS);
	                        aTotalReturn=CommonBL.carry(aTotalReturn+tHL);
	                    	
	                        //存在累计生息的情况
		                	if(tLCBonusPolSchema.getBonusGetMode().equals(BQ.BONUSGET_GETINTEREST)){
		                		//保单生效日至理赔申请日期间的保险年期
		                		String tPolYear=String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),tAppDate, "Y"));
		                		//计算累计生息的利率
		                    	String trate=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+tLCBonusPolSchema.getPolNo()+"' " +
		                    				" and ((month(a.cvalidate)<=6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+"-1))) or (month(a.cvalidate)>6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+")))) ");
		                    	if(trate.equals("")){
		                    		trate=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+tLCBonusPolSchema.getPolNo()+"' " +
	                    				" order by bonusyear desc fetch first 1 row only with ur  ");
		                    	}
		                    	tCalculator=new Calculator();
		                    	tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusInterestCode());
		                    	tCalculator.addBasicFactor("caltype1","JY");
		                    	String JYCalCode=tCalculator.calculate();
		                    	Calculator tCalculatorLX=new Calculator();
		                    	tCalculatorLX.setCalCode(JYCalCode);
		                    	tCalculatorLX.addBasicFactor("lastriskbonus", String.valueOf(tHLLXnoAcc));
		                    	//理赔申请日，同保全退保时的生效日
		                    	tCalculatorLX.addBasicFactor("enddate", tAppDate);
		                    	tCalculatorLX.addBasicFactor("rate", trate);
		                    	if((!tFXdate.equals(""))&&PubFun.getLaterDate(tFXdate, PubFun.calDate(tLCBonusPolSchema.getCValiDate(), 1, "Y", null)).equals(tFXdate)&&PubFun.getBeforeDate(tFXdate, tAppDate).equals(tFXdate)){
		                    		tCalculatorLX.addBasicFactor("lastaccountdate",tFXdate);
		                        }else{
		                        	tCalculatorLX.addBasicFactor("lastaccountdate",PubFun.calDate(tLCBonusPolSchema.getCValiDate(), 1, "Y", null));                        	
		                        }	                    	
		                    	double tHLLX= CommonBL.carry(Double.parseDouble(tCalculatorLX.calculate())-tHLLXnoAcc);
		                    	//生成理赔应付
		                        tBonusLJS = initLJSGetClaim(tLCBonusPolSchema,"HLLX",tHLLX,mG);
		                        aSaveLJSGetClaim.add(tBonusLJS);
		                        aTotalReturn=CommonBL.carry(aTotalReturn+tHLLX);
		                    			                    	
		                	}                    	
	            		}else{//直至理赔申请，保单有效期在1年内，其余逻辑同上
		            		LMRiskBonusRateDB tLMRiskBonusRateDB =new LMRiskBonusRateDB();
		                	LMRiskBonusRateSet tLMRiskBonusRateSet=new LMRiskBonusRateSet();
		                	String tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"' and bonusyear=(case when month('"+tLCBonusPolSchema.getCValiDate()+"')>6 then char(year('"+tLCBonusPolSchema.getCValiDate()+"')+1) else char(year('"+tLCBonusPolSchema.getCValiDate()+"')) end) " +
		            		" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" with ur";
		                	System.out.println("tratesql:"+tratesql);
		                	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
		                	if(tLMRiskBonusRateSet.size()<1){
		                		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
		                		" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear desc with ur";
		                    	System.out.println("tratesql:"+tratesql);
		                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
		                    	if(tLMRiskBonusRateSet.size()<1){
		                    		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
			                		"  and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear,APPYEAR desc with ur";
			                    	System.out.println("tratesql:"+tratesql);
			                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
			                    	if(tLMRiskBonusRateSet.size()<1){
			                    		// @@错误处理
										CError tError = new CError();
										tError.moduleName = "ClaimSaveBL";
										tError.functionName = "dealFHLX";
										tError.errorMessage = "获取分红率失败!";
										mErrors.addOneError(tError);
										return false;
			                    	}
		                    	}
		                	}
		                	Calculator tCalculator = new Calculator();
		                    tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusGetCode());
		                    tCalculator.addBasicFactor("Prem",String.valueOf(tLCBonusPolSchema.getPrem()));
		                    tCalculator.addBasicFactor("Get",String.valueOf(tLCBonusPolSchema.getAmnt()));
		                    tCalculator.addBasicFactor("payendyear",String.valueOf(tLCBonusPolSchema.getPayEndYear()));
		                    tCalculator.addBasicFactor("payintv",String.valueOf(tLCBonusPolSchema.getPayIntv()));
		                    tCalculator.addBasicFactor("insuyear",String.valueOf(tLCBonusPolSchema.getInsuYear()));
		                    tCalculator.addBasicFactor("sex",tLCBonusPolSchema.getInsuredSex());
		                    tCalculator.addBasicFactor("appyear",tCvaliYear);
		                    tCalculator.addBasicFactor("insuredage",String.valueOf(tLCBonusPolSchema.getInsuredAppAge()));
		                    tCalculator.addBasicFactor("polyear",String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),tAppDate, "Y")));
		                    //tCalculator.addBasicFactor("rate1",String.valueOf(tLMRiskBonusRateSet.get(1).getHLRate()));
		                    tCalculator.addBasicFactor("bonusyear",tLMRiskBonusRateSet.get(1).getBonusYear());
		                    if((!tFXdate.equals(""))&&PubFun.getBeforeDate(tFXdate, tAppDate).equals(tFXdate)){
			                	tCalculator.addBasicFactor("startdate",tFXdate);
		                    }else{
			                	tCalculator.addBasicFactor("startdate",tLCBonusPolSchema.getCValiDate());
		                    }
		                    tCalculator.addBasicFactor("enddate",tAppDate);
		                    tCalculator.addBasicFactor("caltype","JY");
		                    double tHL= CommonBL.carry(tCalculator.calculate());
	                    	//生成理赔应付
	                        tBonusLJS = initLJSGetClaim(tLCBonusPolSchema,"HLBF",tHL,mG);
	                        aSaveLJSGetClaim.add(tBonusLJS);
	                        aTotalReturn=CommonBL.carry(aTotalReturn+tHL);
	            		}
	            	}else{//之前进行过保全分红,处理逻辑同上
	            		//从最后一次分红日期 至 理赔申请日 在一年以上
	            		if(PubFun.calInterval(tLastBonusDate,tAppDate, "Y")>0){
	            			//红利公布年份
	            			String tFiscalYear=mExeSQL.getOneValue("select max(FiscalYear)+1 from LOBonusPol where polno='"+tLCBonusPolSchema.getPolNo()+"' ");
		            		LMRiskBonusRateDB tLMRiskBonusRateDB =new LMRiskBonusRateDB();
		                	LMRiskBonusRateSet tLMRiskBonusRateSet=new LMRiskBonusRateSet();
		                	String tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"' and bonusyear='"+tFiscalYear+"' " +
		            			" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" with ur";
		                	System.out.println("tratesql:"+tratesql);
		                	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
		                	if(tLMRiskBonusRateSet.size()<1){
		                		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
		                			" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear desc with ur";
		                    	System.out.println("tratesql:"+tratesql);
		                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
		                    	if(tLMRiskBonusRateSet.size()<1){
		                    		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
			                			"  and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear,APPYEAR desc with ur";
			                    	System.out.println("tratesql:"+tratesql);
			                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
			                    	if(tLMRiskBonusRateSet.size()<1){
			                    		// @@错误处理
										System.out.println("PEdorCTAppConfirmBL+prepareData++--");
										CError tError = new CError();
										tError.moduleName = "ClaimSaveBL";
										tError.functionName = "dealFHLX";
										tError.errorMessage = "获取分红率失败!";
										mErrors.addOneError(tError);
										return false;
			                    	}
		                    	}
		                	}
		                	Calculator tCalculator = new Calculator();
		                    tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusGetCode());
		                    tCalculator.addBasicFactor("Prem",String.valueOf(tLCBonusPolSchema.getPrem()));
		                    tCalculator.addBasicFactor("Get",String.valueOf(tLCBonusPolSchema.getAmnt()));
		                    tCalculator.addBasicFactor("payendyear",String.valueOf(tLCBonusPolSchema.getPayEndYear()));
		                    tCalculator.addBasicFactor("payintv",String.valueOf(tLCBonusPolSchema.getPayIntv()));
		                    tCalculator.addBasicFactor("insuyear",String.valueOf(tLCBonusPolSchema.getInsuYear()));
		                    tCalculator.addBasicFactor("sex",tLCBonusPolSchema.getInsuredSex());
		                    tCalculator.addBasicFactor("appyear",tCvaliYear);
		                    tCalculator.addBasicFactor("insuredage",String.valueOf(tLCBonusPolSchema.getInsuredAppAge()));
		                    tCalculator.addBasicFactor("polyear",String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),tLastBonusDate, "Y")));
		                    //tCalculator.addBasicFactor("rate1",String.valueOf(tLMRiskBonusRateSet.get(1).getHLRate()));
		                    tCalculator.addBasicFactor("bonusyear",tLMRiskBonusRateSet.get(1).getBonusYear());
		                    tCalculator.addBasicFactor("enddate",PubFun.calDate(tLastBonusDate, 1, "Y", null));
	                        double tHL=0;
	                        double tHLbak=0;
	                        if((!tFXdate.equals(""))&&PubFun.getBeforeDate(tFXdate, PubFun.calDate(tLastBonusDate, 1, "Y", null)).equals(tFXdate)&&PubFun.getLaterDate(tFXdate, tLastBonusDate).equals(tFXdate)){
	                        	tCalculator.addBasicFactor("startdate",tFXdate);
	                            tCalculator.addBasicFactor("caltype","FX");
	                            tHL= Double.parseDouble((tCalculator.calculate()));
	                            tHLbak=tHL;
	                        }else if(tFXdate.equals("")){
	                        	tCalculator.addBasicFactor("startdate",tLastBonusDate);
	                            tCalculator.addBasicFactor("caltype","YX");
	                            tHL= Double.parseDouble((tCalculator.calculate()));
	                            tHLbak=tHL;
	                        }else if((!tFXdate.equals(""))&&PubFun.getLaterDate(tFXdate, tLastBonusDate).equals(tLastBonusDate)){
	                        	tCalculator.addBasicFactor("startdate",tLastBonusDate);
	                            tCalculator.addBasicFactor("caltype","YX");
	                            tHL= Double.parseDouble((tCalculator.calculate()));
	                            tHLbak=tHL;
	                        }else if((!tFXdate.equals(""))&&PubFun.getBeforeDate(tFXdate, PubFun.calDate(tLastBonusDate, 1, "Y", null)).equals(PubFun.calDate(tLastBonusDate, 1, "Y", null))){
	                        	tHL= 0;
	                            tHLbak=tHL;
	                        }
		                    
		                    tFiscalYear=mExeSQL.getOneValue("select max(FiscalYear)+2 from LOBonusPol where polno='"+tLCBonusPolSchema.getPolNo()+"' ");
		                    tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"' and bonusyear='"+tFiscalYear+"' " +
		            		" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" with ur";
		                	System.out.println("tratesql:"+tratesql);
		                	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
		                	if(tLMRiskBonusRateSet.size()<1){
		                		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
		                		" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear desc with ur";
		                    	System.out.println("tratesql:"+tratesql);
		                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
		                    	if(tLMRiskBonusRateSet.size()<1){
		                    		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
			                		"  and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear,APPYEAR desc with ur";
			                    	System.out.println("tratesql:"+tratesql);
			                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
			                    	if(tLMRiskBonusRateSet.size()<1){
			                    		// @@错误处理
										CError tError = new CError();
										tError.moduleName = "ClaimSaveBL";
										tError.functionName = "dealFHLX";
										tError.errorMessage = "获取分红率失败!";
										mErrors.addOneError(tError);
										return false;
			                    	}
		                    	}
		                	}
		                	tCalculator = new Calculator();
		                    tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusGetCode());
		                    tCalculator.addBasicFactor("Prem",String.valueOf(tLCBonusPolSchema.getPrem()));
		                    tCalculator.addBasicFactor("Get",String.valueOf(tLCBonusPolSchema.getAmnt()));
		                    tCalculator.addBasicFactor("payendyear",String.valueOf(tLCBonusPolSchema.getPayEndYear()));
		                    tCalculator.addBasicFactor("payintv",String.valueOf(tLCBonusPolSchema.getPayIntv()));
		                    tCalculator.addBasicFactor("insuyear",String.valueOf(tLCBonusPolSchema.getInsuYear()));
		                    tCalculator.addBasicFactor("sex",tLCBonusPolSchema.getInsuredSex());
		                    tCalculator.addBasicFactor("appyear",tCvaliYear);
		                    tCalculator.addBasicFactor("insuredage",String.valueOf(tLCBonusPolSchema.getInsuredAppAge()));
		                    tCalculator.addBasicFactor("polyear",String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),tLastBonusDate, "Y")+1));
		                    //tCalculator.addBasicFactor("rate1",String.valueOf(tLMRiskBonusRateSet.get(1).getHLRate()));
		                    tCalculator.addBasicFactor("bonusyear",tLMRiskBonusRateSet.get(1).getBonusYear());
	                    	if((!tFXdate.equals(""))&&PubFun.getLaterDate(tFXdate, PubFun.calDate(tLastBonusDate, 1, "Y", null)).equals(tFXdate)&&PubFun.getBeforeDate(tFXdate, tAppDate).equals(tFXdate)){
	                    		tCalculator.addBasicFactor("startdate",tFXdate);
	                        }else{
	    	                	tCalculator.addBasicFactor("startdate",PubFun.calDate(tLastBonusDate, 1, "Y", null));                        	
	                        }
	                    	//此处截止日期取理赔事件日期
		                    tCalculator.addBasicFactor("enddate",tAppDate);
		                    tCalculator.addBasicFactor("caltype","JY");
		                    tHL+= Double.parseDouble(tCalculator.calculate());
		                    tHL = CommonBL.carry(tHL);
	                    	//生成理赔应付
	                        tBonusLJS = initLJSGetClaim(tLCBonusPolSchema,"HLBF",tHL,mG);
	                        aSaveLJSGetClaim.add(tBonusLJS);
	                        aTotalReturn=CommonBL.carry(aTotalReturn+tHL);
	                        
		                	if(tLCBonusPolSchema.getBonusGetMode().equals(BQ.BONUSGET_GETINTEREST)){
		                		LCInsureAccSchema tLCInsureAccSchema=new LCInsureAccSchema();
		                		LCInsureAccDB tLCInsureAccDB=new LCInsureAccDB();
		                		tLCInsureAccDB.setPolNo(tLCBonusPolSchema.getPolNo());
		                		LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
		                		double tHLLX=  0;
		                		tLCInsureAccSet = tLCInsureAccDB.query();
		                		if(tLCInsureAccSet!=null&&tLCInsureAccSet.size()>0){
		                		tLCInsureAccSchema.setSchema(tLCInsureAccSet.get(1));
		                		String tPolYear=String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),tAppDate, "Y")-1);
		                    	String trate=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+tLCBonusPolSchema.getPolNo()+"' " +
		                    				" and ((month(a.cvalidate)<=6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+"-1))) or (month(a.cvalidate)>6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+")))) ");
		                    	if(trate.equals("")){
		                    		trate=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+tLCBonusPolSchema.getPolNo()+"' " +
	                    				" order by bonusyear desc fetch first 1 row only with ur  ");
		                    	}
		                    	String tbala=mExeSQL.getOneValue("select NVL(sum(money),0) from lcinsureacctrace where polno='"+tLCInsureAccSchema.getPolNo()+"' and paydate<='"+tLCInsureAccSchema.getBalaDate()+"' with ur");
		                    	tCalculator=new Calculator();
		                    	tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusInterestCode());
		                    	tCalculator.addBasicFactor("caltype1","YX");
		                    	String YXCalCode=tCalculator.calculate();
		                    	Calculator tCalculatorLX=new Calculator();
		                    	tCalculatorLX.setCalCode(YXCalCode);
		                    	tCalculatorLX.addBasicFactor("lastriskbonus", tbala);
		                    	tCalculatorLX.addBasicFactor("accountdate", PubFun.calDate(tLCBonusPolSchema.getCValiDate(), Integer.parseInt(tPolYear)+1, "Y", null));
		                    	tCalculatorLX.addBasicFactor("rate", trate);
		                    	double tHLLXYX=0;
		                        if((!tFXdate.equals(""))&&PubFun.getBeforeDate(tFXdate, PubFun.calDate(tLCBonusPolSchema.getCValiDate(), Integer.parseInt(tPolYear)+1, "Y", null)).equals(tFXdate)&&PubFun.getLaterDate(tFXdate, tLCInsureAccSchema.getBalaDate()).equals(tFXdate)){
			                    	tCalculatorLX.addBasicFactor("lastaccountdate",  tFXdate);
			                		tHLLXYX=Double.parseDouble(tCalculatorLX.calculate());
		                        }else if(tFXdate.equals("")){
		                        	//此种情况，为当月累计生息后分红，并在下月累计生息前出险，需要判断计算红利的起始时间，应该取分红的时间
			                    	tCalculatorLX.addBasicFactor("lastaccountdate",  PubFun.getLaterDate(tLCInsureAccSchema.getBalaDate(),tLastBonusDate));
			                		tHLLXYX=Double.parseDouble(tCalculatorLX.calculate());
		                        }else if((!tFXdate.equals(""))&&PubFun.getLaterDate(tFXdate, tLCInsureAccSchema.getBalaDate()).equals(tLCInsureAccSchema.getBalaDate())){
			                    	tCalculatorLX.addBasicFactor("lastaccountdate",  tLCInsureAccSchema.getBalaDate());
			                		tHLLXYX=Double.parseDouble(tCalculatorLX.calculate());
		                        }else if((!tFXdate.equals(""))&&PubFun.getBeforeDate(tFXdate, PubFun.calDate(tLCBonusPolSchema.getCValiDate(), Integer.parseInt(tPolYear)+1, "Y", null)).equals(PubFun.calDate(tLCBonusPolSchema.getCValiDate(), Integer.parseInt(tPolYear)+1, "Y", null))){
			                		tHLLXYX=Double.parseDouble(tbala);
		                        }
		                		tPolYear=String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),tAppDate, "Y"));
		                		trate=new String();
		                		trate=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+tLCBonusPolSchema.getPolNo()+"' " +
		                    				" and ((month(a.cvalidate)<=6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+"-1))) or (month(a.cvalidate)>6 and b.bonusyear=char((year(a.cvalidate)+"+tPolYear+")))) ");
		                    	if(trate.equals("")){
		                    		trate=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+tLCBonusPolSchema.getPolNo()+"' " +
	                    				" order by bonusyear desc fetch first 1 row only with ur  ");
		                    	}
		                    	tCalculator=new Calculator();
		                    	tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusInterestCode());
		                    	tCalculator.addBasicFactor("caltype1","JY");
		                    	String JYCalCode=tCalculator.calculate();
		                    	tCalculatorLX=new Calculator();
		                    	tCalculatorLX.setCalCode(JYCalCode);
		                    	tCalculatorLX.addBasicFactor("lastriskbonus", String.valueOf(tHLbak+tHLLXYX));
		                    	tCalculatorLX.addBasicFactor("enddate", tAppDate);
		                    	tCalculatorLX.addBasicFactor("rate", trate);
		                    	if((!tFXdate.equals(""))&&PubFun.getLaterDate(tFXdate, PubFun.calDate(tLCBonusPolSchema.getCValiDate(), Integer.parseInt(tPolYear), "Y", null)).equals(tFXdate)&&PubFun.getBeforeDate(tFXdate, tAppDate).equals(tFXdate)){
		                    		tCalculatorLX.addBasicFactor("lastaccountdate",tFXdate);
		                        }else{
		                        	tCalculatorLX.addBasicFactor("lastaccountdate",  PubFun.calDate(tLCBonusPolSchema.getCValiDate(), Integer.parseInt(tPolYear), "Y", null));
		                        }
		                    	 tHLLX= CommonBL.carry(Double.parseDouble(tCalculatorLX.calculate())-tHLbak);
		                		}
		                		//生成理赔应付
		                        tBonusLJS = initLJSGetClaim(tLCBonusPolSchema,"HLLX",tHLLX,mG);
		                        aSaveLJSGetClaim.add(tBonusLJS);
		                        aTotalReturn=CommonBL.carry(aTotalReturn+tHLLX);
		                	}
	            		}else{
	            			//从最后一次分红日期 到 理赔申请日在一年以内   或   理赔申请日早于最后一次分红日期
	            			//#2366理赔时分红添加修改  申请日期 在最近一次分红日期之前的计算分红和分红利息的计算方式
//		            		String tFiscalYear=mExeSQL.getOneValue("select max(FiscalYear)+1 from LOBonusPol where polno='"+tLCBonusPolSchema.getPolNo()+"' ");
	            			//取距理赔申请日期之前最近一次分红日期的年份为红利年份
		            		String tFiscalYear=mExeSQL.getOneValue("select  case when month('"+tAppDate+"')>6 then year('"+tAppDate+"') when month('"+tAppDate+"')<=6 then year('"+tAppDate+"')-1 end from dual");
		            		//取距理赔申请日期之前最近一次分红日期
		            		String tstartdate=mExeSQL.getOneValue("select max(sgetdate) from lobonuspol where polno='"+tLCBonusPolSchema.getPolNo()+"' and sgetdate<='"+tAppDate+"' ");
		                	//添加特殊情况的处理
		                	//当申请日期在第一次分红之前 de情况,开始时间取保单生效日期,前提是分红过(上面四大逻辑的限定)
		            		if(tstartdate==null || "".equals(tstartdate)){
		            			tstartdate=tLCBonusPolSchema.getCValiDate();
		            		}
		            		LMRiskBonusRateDB tLMRiskBonusRateDB =new LMRiskBonusRateDB();
		                	LMRiskBonusRateSet tLMRiskBonusRateSet=new LMRiskBonusRateSet();
		                	String tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"' and bonusyear='"+tFiscalYear+"' " +
		            			" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" with ur";
		                	System.out.println("tratesql:"+tratesql);
		                	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
		                	if(tLMRiskBonusRateSet.size()<1){
		                		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
		                		" and appyear='"+tCvaliYear+"' and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear desc with ur";
		                    	System.out.println("tratesql:"+tratesql);
		                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
		                    	if(tLMRiskBonusRateSet.size()<1){
		                    		tratesql="select * from LMRiskBonusRate where riskcode='"+tLCBonusPolSchema.getRiskCode()+"'  " +
			                		"  and insuyears="+tLCBonusPolSchema.getInsuYear()+" and payyears=("+getPayYearsSQL(tLCBonusPolSchema)+") and minage<="+tLCBonusPolSchema.getInsuredAppAge()+" and maxage>="+tLCBonusPolSchema.getInsuredAppAge()+" order by bonusyear,APPYEAR desc with ur";
			                    	System.out.println("tratesql:"+tratesql);
			                    	tLMRiskBonusRateSet =tLMRiskBonusRateDB.executeQuery(tratesql);
			                    	if(tLMRiskBonusRateSet.size()<1){
			                    		// @@错误处理
										CError tError = new CError();
										tError.moduleName = "ClaimSaveBL";
										tError.functionName = "dealFHLX";
										tError.errorMessage = "获取分红率失败!";
										mErrors.addOneError(tError);
										return false;
			                    	}
		                    	}
		                	}
		                	Calculator tCalculator = new Calculator();
		                    tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusGetCode());
		                    tCalculator.addBasicFactor("Prem",String.valueOf(tLCBonusPolSchema.getPrem()));
		                    tCalculator.addBasicFactor("Get",String.valueOf(tLCBonusPolSchema.getAmnt()));
		                    tCalculator.addBasicFactor("payendyear",String.valueOf(tLCBonusPolSchema.getPayEndYear()));
		                    tCalculator.addBasicFactor("payintv",String.valueOf(tLCBonusPolSchema.getPayIntv()));
		                    tCalculator.addBasicFactor("insuyear",String.valueOf(tLCBonusPolSchema.getInsuYear()));
		                    tCalculator.addBasicFactor("sex",tLCBonusPolSchema.getInsuredSex());
		                    tCalculator.addBasicFactor("appyear",tCvaliYear);
		                    tCalculator.addBasicFactor("insuredage",String.valueOf(tLCBonusPolSchema.getInsuredAppAge()));
		                    tCalculator.addBasicFactor("polyear",String.valueOf(PubFun.calInterval(tLCBonusPolSchema.getCValiDate(),tAppDate, "Y")));
		                    //tCalculator.addBasicFactor("rate1",String.valueOf(tLMRiskBonusRateSet.get(1).getHLRate()));
		                    tCalculator.addBasicFactor("bonusyear",tLMRiskBonusRateSet.get(1).getBonusYear());
		                    if((!tFXdate.equals(""))&&PubFun.getBeforeDate(tFXdate, tAppDate).equals(tFXdate)&&PubFun.getLaterDate(tFXdate, tAppDate).equals(tFXdate)){
			                	tCalculator.addBasicFactor("startdate",tFXdate);
		                    }else{
			                	tCalculator.addBasicFactor("startdate",tstartdate);
		                    }
		                    tCalculator.addBasicFactor("enddate",tAppDate);
		                    tCalculator.addBasicFactor("caltype","JY");
		                    double tHL= CommonBL.carry(tCalculator.calculate());
		                    
		                    //在计算分红开始日期之前的账户表里分红的合计
		                    String PreSumBonus =mExeSQL.getOneValue(" select NVL(sum(money),0) from lcinsureacctrace where moneytype='HL' and polno='"+tLCBonusPolSchema.getPolNo()+"' and paydate<='"+tstartdate+"' ");
		                    double PreSumHL=Double.parseDouble(PreSumBonus);
		                    tHL=tHL+PreSumHL;
		                    //生成理赔应付
	                        tBonusLJS = initLJSGetClaim(tLCBonusPolSchema,"HLBF",tHL,mG);
	                        aSaveLJSGetClaim.add(tBonusLJS);
	                        aTotalReturn=CommonBL.carry(aTotalReturn+tHL);
	                        
		                	if(tLCBonusPolSchema.getBonusGetMode().equals(BQ.BONUSGET_GETINTEREST)){
		                		LCInsureAccSchema tLCInsureAccSchema=new LCInsureAccSchema();
		                		LCInsureAccDB tLCInsureAccDB=new LCInsureAccDB();
		                		tLCInsureAccDB.setPolNo(tLCBonusPolSchema.getPolNo());
		                		LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
		                		tLCInsureAccSet = tLCInsureAccDB.query();
		                		double tHLLX= 0;
		                		if(tLCInsureAccSet!=null&&tLCInsureAccSet.size()>0){
			                		tLCInsureAccSchema.setSchema(tLCInsureAccSet.get(1));
			                		
			                		//计算利息的开始时间
			                		String tLXstartDate="";
			                		//申请日期之前最后一次生成利息的日期
			                    	String tLastLXDate=mExeSQL.getOneValue(" select MAX(paydate) from lcinsureacctrace where polno='"+tLCBonusPolSchema.getPolNo()+"' and moneytype='LX' and paydate<='"+tAppDate+"' ");
			                    	tLXstartDate=tLastLXDate;//一般情况为申请日期前最后一次生成利息的日期为计算利息的开始时间
			                    	//若没有申请日期前最近一次利息,
			                    	if((tLXstartDate==null||"".equals(tLXstartDate))){
			                    		//申请日期在最近分红日期后,及第一次计息月内,取最近一次分红日期为开始时间
			                    		if(PubFun.getBeforeDate(tstartdate, tAppDate).equals(tstartdate)){
			                    			tLXstartDate=tstartdate;
			                    		}
			                    	}
			                    	
			                    	
			                    	if(tLXstartDate!=null &&!"".equals(tLXstartDate)){
					                    //计算最近结息日到申请日的利息
				                    	String trateb=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+tLCBonusPolSchema.getPolNo()+"' " +
		                    					" and bonusyear='"+tFiscalYear+"' ");
				                    	if(trateb.equals("")){
				                    		trateb=mExeSQL.getOneValue("select b.hlinterestrate from lcpol a,ldbonusinterestrate b where char(year(a.cvalidate))=b.appyear and  a.riskcode=b.riskcode and a.polno='"+tLCBonusPolSchema.getPolNo()+"' " +
			                    				" order by bonusyear desc fetch first 1 row only with ur  ");
				                    	}
				                    	System.out.println("本年度利率："+1);
					                    double bHLLX = 0;
					                    //本金为到最近结息日的金额合计,若最近结息日到申请日期之间有分红,后面单独计算
					                    String bbala=mExeSQL.getOneValue("select NVL(sum(money),0) from lcinsureacctrace where polno='"+tLCInsureAccSchema.getPolNo()+"' and (moneytype='LX' or moneytype='HL')  and paydate<='"+tLXstartDate+"' with ur");	                  	
				                    	
					                    bHLLX=Double.parseDouble(bbala);
					                    tCalculator=new Calculator();
				                    	tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusInterestCode());
				                    	tCalculator.addBasicFactor("caltype1","JY");
				                    	String bJYCalCode=tCalculator.calculate();
				                    	Calculator bCalculatorLX=new Calculator();
				                    	bCalculatorLX.setCalCode(bJYCalCode);
				                    	bCalculatorLX.addBasicFactor("lastriskbonus", String.valueOf(bHLLX));
				                    	bCalculatorLX.addBasicFactor("enddate", tAppDate);
				                    	bCalculatorLX.addBasicFactor("rate", trateb);
					                    if((!tFXdate.equals(""))&&PubFun.getBeforeDate(tFXdate, tAppDate).equals(tFXdate)&&PubFun.getLaterDate(tFXdate, tLCInsureAccSchema.getBalaDate()).equals(tFXdate)){
					                    	bCalculatorLX.addBasicFactor("lastaccountdate", tFXdate);
					                    }else{
				                        	//此种情况，为当月累计生息后分红，并在下月累计生息前出险，需要判断计算红利的起始时间，应该取分红的时间
					                    	bCalculatorLX.addBasicFactor("lastaccountdate", tLXstartDate);
					                    }
					                    //计算出最后一次生息日 到 申请日期时间的利息+本金
					                    bHLLX= CommonBL.carry(bCalculatorLX.calculate());
					                    
					                    //若存在,最近结息日到申请日期之间有分红,单独计算
					                    String specialHL = mExeSQL.getOneValue("select NVL(sum(money),0) from lcinsureacctrace where polno='"+
					                    		tLCInsureAccSchema.getPolNo()+
					                    		"' and  moneytype='HL'  and paydate>'"+tLXstartDate+"' and paydate<='"+tAppDate+"' with ur");	                  	
					                  //减去本金
					                    bHLLX = bHLLX- Double.parseDouble(bbala);
					                    
					                    double cHLLX = 0;
					                    //最近结息日到申请日期之间存在分红,此处单独计算利息
					                    if(Double.parseDouble(specialHL)!=0){
						                    String cbala=mExeSQL.getOneValue("select money from lcinsureacctrace where polno='"+tLCInsureAccSchema.getPolNo()+"' and moneytype='HL' and paydate>'"+tLXstartDate+"' and paydate<='"+tAppDate+"' with ur");	                  	
						                    String tpaydate=mExeSQL.getOneValue("select paydate from lcinsureacctrace where polno='"+tLCInsureAccSchema.getPolNo()+"' and moneytype='HL' and paydate>'"+tLXstartDate+"' and paydate<='"+tAppDate+"' with ur");	                  	
						                    tCalculator=new Calculator();
					                    	tCalculator.setCalCode(tLMRiskBonusSet.get(1).getBonusInterestCode());
					                    	tCalculator.addBasicFactor("caltype1","JY");
					                    	String cJYCalCode=tCalculator.calculate();
					                    	Calculator cCalculatorLX=new Calculator();
					                    	cCalculatorLX.setCalCode(cJYCalCode);
					                    	cCalculatorLX.addBasicFactor("lastriskbonus", String.valueOf(cbala));
					                    	cCalculatorLX.addBasicFactor("enddate", tAppDate);
					                    	cCalculatorLX.addBasicFactor("rate", trateb);
						                    if((!tFXdate.equals(""))&&PubFun.getBeforeDate(tFXdate, tAppDate).equals(tFXdate)&&PubFun.getLaterDate(tFXdate, tLCInsureAccSchema.getBalaDate()).equals(tFXdate)){
						                    	cCalculatorLX.addBasicFactor("lastaccountdate", tFXdate);
						                    }else{
					                        	//此种情况，为当月累计生息后分红，并在下月累计生息前出险，需要判断计算红利的起始时间，应该取分红的时间
						                    	cCalculatorLX.addBasicFactor("lastaccountdate", tpaydate);
						                    }
						                    //计算出利息+本金
						                    cHLLX = CommonBL.carry(cCalculatorLX.calculate());
						                    //减本金剩利息
						                    cHLLX = cHLLX -Double.parseDouble(cbala);
					                    }
				                    
				                    
					                    //加上/最近结息日到申请日期之间存在分红
					                    bHLLX= bHLLX+cHLLX;
					                    
					                    //加上最近结息日之前利息的合计
					                    String sumpreLX=mExeSQL.getOneValue(" select NVL(sum(money),0) from lcinsureacctrace where moneytype='LX' and polno='"+tLCBonusPolSchema.getPolNo()+"' and paydate<='"+tAppDate+"' ");
					                    bHLLX = bHLLX + Double.parseDouble(sumpreLX);
					                    
				                    	tHLLX= CommonBL.carry(bHLLX);
				                    	System.out.println("红利利息："+tHLLX);
		                			}
		                		}
		                		
		                		//生成理赔应付
		                        tBonusLJS = initLJSGetClaim(tLCBonusPolSchema,"HLLX",tHLLX,mG);
		                        aSaveLJSGetClaim.add(tBonusLJS);
		                        aTotalReturn=CommonBL.carry(aTotalReturn+tHLLX);
		                	}
	            		}
	            	}
    	    	}
    			if(aSaveLJSGetClaim.size() > 0 && aTotalReturn > 0){
    				//整理到数据提交集合中 红利生成新的给付号
        			Reflections tref = new Reflections();
        		    tref.transFields(aSaveLJSGetSchema, aSaveLJSGetClaim.get(1));    		
        		    aTotalReturn = Arith.round(aTotalReturn, 2);
        		    aSaveLJSGetSchema.setSumGetMoney(aTotalReturn);
        		    System.out.println("分红总计金额："+aTotalReturn);
        		    tmpMap.put(aSaveLJSGetClaim, "INSERT");
					tmpMap.put(aSaveLJSGetSchema, "INSERT");	//#2366ljsget未领取保单红利与赔款一并给付
        		    mHLtotal=aTotalReturn;
    			}
    			
    					   
    		}
    	}
    	return true;
    }
    //2366关于理赔案件处理时红利处理**end**新增代码
	public VData getResult() {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("getInputData......");
        mLLClaimDetailSet = (LLClaimDetailSet) cInputData.
                            getObjectByObjectName("LLClaimDetailSet", 0);
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        LLCaseSchema tLLCaseSchema = (LLCaseSchema) cInputData.
                                     getObjectByObjectName("LLCaseSchema", 0);
        mCaseProp = tLLCaseSchema.getCaseProp();

        if (mLLClaimDetailSet == null || mLLClaimDetailSet.size() <= 0) {
            CError.buildErr(this, "请选中赔付责任！");
            return false;
        }
        this.mClmNo = mLLClaimDetailSet.get(1).getClmNo();
        this.mCaseNo = mLLClaimDetailSet.get(1).getCaseNo();
        this.mRgtNo = mLLClaimDetailSet.get(1).getRgtNo();
        if ("".equals(this.mClmNo)) {
            CError.buildErr(this, "该案件尚未理算");
            return false;
        }
        aDate = PubFun.getCurrentDate();
        aTime = PubFun.getCurrentTime();
        this.mPolNo="";
        // 1500康乐人生 
        mOperator =mG.Operator;
        mManageCom =mG.ManageCom;
        return true;
    }

    /**
     * 案件信息校验
     * @return boolean
     */
    private boolean checkCaseInfo() {
    	
    	LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mCaseNo);
        if (!tLLCaseDB.getInfo()) {
            CError.buildErr(this, "案件信息查询错误");
            return false;
        }
        this.mLLCaseSchema = tLLCaseDB.getSchema();
        String tRgtState = tLLCaseDB.getRgtState();
        String tHandler = tLLCaseDB.getHandler();//社保理算需要校验案件处理人
        //查讫状态的案件，可以点击理算确认	add by Houyd
        if (!"03".equals(tRgtState) && !"04".equals(tRgtState)
            && !"02".equals(tRgtState) && !"08".equals(tRgtState)) {
            CError.buildErr(this, "该案件状态不能理算保存");
            return false;
        }
        //#1738 社保调查 理赔各处理流程支持
        String tReturnMsg = LLCaseCommon.checkSurveyRgtState(mCaseNo,tRgtState,"03");
        if(!"".equals(tReturnMsg)){
        	CError.buildErr(this, tReturnMsg);
            return false;
        }
    	
    	//add by Houyd 社保类案件的理算，需要添加社保用户校验流程。支持社保的申诉纠错案件
    	String tCaseNo = "";
     	if("R".equals(mCaseNo.substring(0,1)) || "S".equals(mCaseNo.substring(0,1)))
     	{
     		System.out.println("申诉纠错案件换为正常的C案件");
     		String sql = "select caseno from LLAppeal where appealno ='"+mCaseNo+"'";
     		ExeSQL exeSQL = new ExeSQL();
     		tCaseNo = exeSQL.getOneValue(sql);
     	}else{
     		tCaseNo = mCaseNo;
     	}
        String tSSFlag = LLCaseCommon.checkSocialSecurity(tCaseNo, null);
        if("1".equals(tSSFlag)){
        	LLSocialClaimUserDB tLLSocialClaimUserDB = new LLSocialClaimUserDB();
        	tLLSocialClaimUserDB.setUserCode(mG.Operator);
        	tLLSocialClaimUserDB.setStateFlag("1");//社保用户有效
			tLLSocialClaimUserDB.setHandleFlag("1");//参与案件分配
			LLSocialClaimUserSet tSocialClaimUserSet = tLLSocialClaimUserDB.query();
        	if(tSocialClaimUserSet.size() <= 0){
        		CError.buildErr(this,"您不是社保参与案件分配的人员或已失效，无权作理算确认操作");
                return false;
        	}else if(!mG.Operator.equals(tHandler)){
        		CError.buildErr(this,"您不是社保案件分配的处理人，无权作理算确认操作");
                return false;
        	}
        }
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setClmNo(this.mClmNo);
        if (!tLLClaimDB.getInfo()) {
            CError.buildErr(this, "赔案信息查找错误");
            return false;
        }
        mLLClaimSchema = tLLClaimDB.getSchema();

        if (!LLCaseCommon.checkHospCaseState(mCaseNo)) {
            CError.buildErr(this, "医保通案件待确认，不能进行处理");
            return false;
        }
        //判断万能险不能进行手工修改实赔金额
//        for (int i = 1; i <= mLLClaimDetailSet.size(); i++) 
//        {
//            LLClaimDetailSchema tLLClaimDetailSchema = mLLClaimDetailSet.get(i);
//            if((tLLClaimDetailSchema.getDeclineNo().equals("1")||
//            	tLLClaimDetailSchema.getDeclineNo().equals("2")))
//            {
//            	 String WNsql="select distinct riskcode from llclaimdetail "
//            		 +" where polno='"+tLLClaimDetailSchema.getPolNo()+"'";
//            	 ExeSQL WNexesql = new ExeSQL();
//                 String tRiskCode = WNexesql.getOneValue(WNsql);
//                 if(LLCaseCommon.chenkWN(tRiskCode))
//                 {
//                	 CError.buildErr(this, "万能险赔款不能手工修改实赔金额!");
//                     return false;
//                 }            	
//            }
//        }

        //判断llclaimdetail中caserelano与llcaserela中是否相同
        LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
        LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
        LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
        
        tLLClaimDetailDB.setCaseNo(mCaseNo);
        tLLCaseRelaDB.setCaseNo(mCaseNo);
        System.out.println(mCaseNo);
        mLLCaseRelaSet = tLLCaseRelaDB.query();
        tLLClaimDetailSet = tLLClaimDetailDB.query();
        boolean tCaseRelaFlag = false;
        for (int i = 1; i <= tLLClaimDetailSet.size(); i++) {
			for (int j = 1; j <= mLLCaseRelaSet.size(); j++) {
				LLSubReportDB tLLSubReportDB = new LLSubReportDB();
				tLLSubReportDB.setSubRptNo(mLLCaseRelaSet.get(j).getSubRptNo());
				if (tLLSubReportDB.getInfo()) {
					mLLSubReportSet.add(tLLSubReportDB.getSchema());
				}
 
				if (tLLClaimDetailSet.get(i).getCaseRelaNo().equals(
						mLLCaseRelaSet.get(j).getCaseRelaNo())) {
					tCaseRelaFlag = true;

				}
			}
		}
        if (!tCaseRelaFlag) {
            CError.buildErr(this, "理算结果与事件无法关联，请重新计算");
            return false;
        }

        if (("" + mLLCaseSchema.getCalFlag()).equals("2")) {
            boolean havereason = false;
            for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
                String areason = "" + mLLClaimDetailSet.get(i).getGiveReason();
                if (!areason.equals("") && !areason.equals("null")) {
                    havereason = true;
                    break;
                }
            }
            if (!havereason) {
                CError.buildErr(this, "该案件重算过，为非标准业务，请给出赔付结论依据！");
                return false;
            }
        }
        //TODO:1)是否做过豁免；2）非豁免险不可选"6-保费豁免"、"7-不予豁免保费"；3）效验理算确认和豁免的是否为同一保单
        //豁免CaseNo 对申诉纠错来说应该使用原案件号。
        String sCaseNo = "";
        if("R".equals(mCaseNo.substring(0,1)) || "S".equals(mCaseNo.substring(0,1))){
			System.out.println("申诉纠错案件换为正常的C案件");
                String sql = "select caseno from LLAppeal where appealno ='"+mCaseNo+"'";
                ExeSQL exeSQL = new ExeSQL();
                sCaseNo = exeSQL.getOneValue(sql);
        	}else{
        		sCaseNo=mCaseNo;
        	}
        System.out.println("豁免CaseNo："+mCaseNo);
       
        
        for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
            String mGiveType = mLLClaimDetailSet.get(i).getGiveType();
            String mGiveReason = mLLClaimDetailSet.get(i).getGiveReason();
            String mPolNo = mLLClaimDetailSet.get(i).getPolNo();
            String mContNo = mLLClaimDetailSet.get(i).getContNo();
            String mRiskcode = mLLClaimDetailSet.get(i).getRiskCode();
            
            System.out.println("mGiveType:"+mGiveType);
            System.out.println("mGiveReason:"+mGiveReason);
            System.out.println("mPolNo:"+mPolNo);
            System.out.println("mContNo:"+mContNo);
            if("7".equals(mGiveType)){
            	if("231701".equals(mRiskcode) || "333801".equals(mRiskcode)){
            		CError.buildErr(this, "康乐人生不能选择'不予豁免保费'");
                    return false;
            	}
            }
            
            LLExemptionDB tLLExemptionDB = new LLExemptionDB();
            LLExemptionSet tLLExemptionSet = new LLExemptionSet();
            tLLExemptionDB.setCaseNo(sCaseNo);
            tLLExemptionSet = tLLExemptionDB.query();
            if(tLLExemptionSet != null && tLLExemptionSet.size() > 0){
                for(int j=1 ;j<=tLLExemptionSet.size();j++){
                    String tState = tLLExemptionSet.get(j).getState();
                    // 1500 康乐人生 **start**
                    String tRiskCode =tLLExemptionSet.get(j).getRiskCode();
                    String tlocation = tLLExemptionSet.get(j).getLocation();
                    if(("231701".equals(tRiskCode)&&"ZE".equals(tlocation)) || ("333801".equals(tRiskCode)&&"ZE".equals(tlocation))){
                    	return true;
                    }
                    // 1500 康乐人生 **end**
                    else if(!"6".equals(mGiveType) && !"7".equals(mGiveType)){
                        CError.buildErr(this, "请选择正确的赔付结论'保费豁免'或'不予豁免保费'");
                        return false; 
                    }
                    else if("6".equals(mGiveType) && ("temp".equals(tState) || "1".equals(tState))){
                        return true;
                    }
                    else if("7".equals(mGiveType) && ("temp".equals(tState) ||"0".equals(tState))){
                        CError.buildErr(this, "案件下豁免险状态不为失效，不可选择'不予豁免保费'");
                        return false; 
                    }
                    else if("6".equals(mGiveType) && "2".equals(tState)){
                        CError.buildErr(this, "案件下豁免险状态失效，请选择正确的结论或者确认豁免保费");
                        return false; 
                    }
                }
            }
            if("6".equals(mGiveType) || "7".equals(mGiveType)){//赔付结论为'6','7'
                //判断是否为豁免险
                String mSQL = "select 1 from lcpol" +
                		" d where d.contno='"+mContNo+"' " +
                    "and d.riskcode in(select distinct a.code1 from ldcode1 a,ldcode1 b " +
                    "where a.code1=b.code and b.code1=d.riskcode and b.codetype='checkexemptionrisk' " +
                    "and a.codetype='checkappendrisk' union select distinct a.code from ldcode1 a,ldcode1 b " +
                    "where a.code1=b.code and b.code1=d.riskcode and b.codetype='checkexemptionrisk' " +
                    "and a.codetype='checkappendrisk' union select distinct b.code1 from ldcode1 a,ldcode1 b " +
                    "where a.code1=b.code and b.code1=d.riskcode and b.codetype='checkexemptionrisk' and " +
                    "a.codetype='checkappendrisk' union select distinct code1 from ldcode1 where " +
                    "(code1=d.riskcode or code=d.riskcode) and codetype='checkappendrisk' union select distinct code " +
                    "from ldcode1 where (code1=d.riskcode or code=d.riskcode) and codetype='checkappendrisk' " +
                    "union select riskcode from lmriskapp where risktype8='8' and riskcode= D.RISKCODE " +
                    "union select riskcode from lmriskapp where risktype8='9' and kindcode='S' and riskcode=d.riskcode)";
               ExeSQL mExeSQL = new ExeSQL();
               SSRS mSSRS = mExeSQL.execSQL(mSQL);
               if(mSSRS != null && mSSRS.getMaxRow()>0){//是豁免险
                   String aSQL = "select contno from llexemption where caseno='"+sCaseNo+"' and polno = '"+mPolNo+"'"; 
                   ExeSQL aExeSQL = new ExeSQL();
                   SSRS aSSRS = new SSRS();
                   aSSRS = aExeSQL.execSQL(aSQL);
                   if (aSSRS == null || aSSRS.getMaxRow() <= 0) {
                       //没有记录，且赔付结论为'6'，提示并且阻断
                       if("6".equals(mGiveType)){
                           CError.buildErr(this, "保单："+mContNo+"下，保单险种号："+mPolNo+"为豁免险，请先进行保费豁免");
                           return false;           
                       }
                       
                   }else{
                       String uContNo = aSSRS.GetText(1, 1);
                       System.out.println("uContNo:"+uContNo);
                      //不准予豁免的 申诉纠错对于豁免来说，也使用原案件号。
                      aSQL="select state from llexemption where caseno='"+sCaseNo+"' and polno ='"+mPolNo+"' with ur";
                      aSSRS = aExeSQL.execSQL(aSQL);
                      String aState= aSSRS.GetText(1, 1);
                       if("7".equals(mGiveType)&&("temp".equals(aState)||"0".equals(aState))){
                    	   CError.buildErr(this, "案件下豁免险状态不为失效，不可选择'不予豁免保费'，请先在保费豁免处取消豁免！");
                           return false; 
                       }
                       if(!mContNo.equals(uContNo)){
                           CError.buildErr(this, "理算确认和保费豁免不是同一个保单，请重新理算！");
                           return false; 
                       }
                   }
               }else{//不是豁免险
                   CError.buildErr(this, "保单："+mContNo+"下，保单险种号："+mPolNo+"不是豁免险，赔付结论不可为'保费豁免'或'不予豁免保费'");
                   return false;  
               }
            }
        }
        return true;
    }
    //检查录入的LLClaimPolicy/LLClaimDetail/LJSGetClaim/的金额是否相等
    private boolean checkMoney() {
        double tPolicySum = 0;
        double tDetailSum = 0;
        boolean tValidFlag = false;
        String sumSql = "select sum(realpay) from llclaimdetail where clmno='"
                        + mClmNo + "'";
        ExeSQL texesql = new ExeSQL();
        String tsum = texesql.getOneValue(sumSql);
        if (tsum.equals("null")) {
            CError.buildErr(this, "理算出现问题，没有生成责任赔付明细，"
                            + "请尝试重新理算，如果依然出错，请向it人员发送错误报告。");
            return false;
        }
        tPolicySum = Double.parseDouble(tsum);
        for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
            LLClaimDetailSchema tLLClaimDetailSchema = mLLClaimDetailSet.get(i);
            if (tLLClaimDetailSchema.getApproveAmnt() < 0) {
                CError.buildErr(this, "通融/协议给付比例不能为负");
                return false;
            }
            
            if (tLLClaimDetailSchema.getGiveType().equals("3")) {
            	if(tLLClaimDetailSchema.getRealPay() > 0){
            		tPolicySum -= tLLClaimDetailSchema.getRealPay();
                    tLLClaimDetailSchema.setDeclineAmnt(tLLClaimDetailSchema.
                            getRealPay());
                    tLLClaimDetailSchema.setRealPay(0);
            	}else {
            		// #4205
                	String trisktype1 = "select 1 from lmriskapp where risktype1='1' and riskcode in (select riskcode from lcpol where polno='"+tLLClaimDetailSchema.getPolNo()+"' union select riskcode from lbpol where polno='"+tLLClaimDetailSchema.getPolNo()+"') with ur";
                	String trisktype1SQL = texesql.getOneValue(trisktype1);
                	if("1".equals(trisktype1SQL)){
                		tPolicySum -= tLLClaimDetailSchema.getRealPay();       
                		String tTabFeeMoneySQL = "select TabFeeMoney from llclaimdetail where caseno='"+tLLClaimDetailSchema.getCaseNo()+"' and polno='"+tLLClaimDetailSchema.getPolNo()+"' and getdutycode='"+tLLClaimDetailSchema.getGetDutyCode()+"' and caserelano='"+tLLClaimDetailSchema.getCaseRelaNo()+"' with ur";
                    	String tTabFeeMoney = texesql.getOneValue(tTabFeeMoneySQL);
                    	System.out.println(tTabFeeMoney);
                        tLLClaimDetailSchema.setDeclineAmnt(tTabFeeMoney);              
                        tLLClaimDetailSchema.setRealPay(0);
                	}else{
                		tPolicySum -= tLLClaimDetailSchema.getRealPay();            		
                		String tAmntSQL = "select amnt from lcduty a,LMDutyGetRela b where a.dutycode=b.dutycode and a.contno='"+tLLClaimDetailSchema.getContNo()+"' and a.polno='"+tLLClaimDetailSchema.getPolNo()+"' and b.getdutycode='"+tLLClaimDetailSchema.getGetDutyCode()+"' union select amnt from lbduty a,LMDutyGetRela b where a.dutycode=b.dutycode and a.contno='"+tLLClaimDetailSchema.getContNo()+"' and a.polno='"+tLLClaimDetailSchema.getPolNo()+"' and b.getdutycode='"+tLLClaimDetailSchema.getGetDutyCode()+"' with ur";
                    	String tGetdutyAmnt = texesql.getOneValue(tAmntSQL);
                        tLLClaimDetailSchema.setDeclineAmnt(tGetdutyAmnt);
                        tLLClaimDetailSchema.setRealPay(0);
                	}
            	}
                
            }
            tDetailSum = tDetailSum + tLLClaimDetailSchema.getRealPay();

            if (tLLClaimDetailSchema.getRealPay() > 0
                    || tLLClaimDetailSchema.getApproveAmnt() > 0) {
                LLCaseCommon tLLCaseCommon = new LLCaseCommon();
                String tPolNo = tLLClaimDetailSchema.getPolNo();
                String tCaseRelaNo = tLLClaimDetailSchema.getCaseRelaNo();

                String tAccSQL =
                        "select accdate from llsubreport a,llcaserela b"
                        + " where a.subrptno=b.subrptno and b.caserelano='"
                        + tCaseRelaNo + "'";
                ExeSQL tAccExeSQL = new ExeSQL();
                String tAccDate = tAccExeSQL.getOneValue(tAccSQL);

                if (!tLLCaseCommon.checkPolValid(tPolNo, tAccDate)) {
                    tValidFlag = true;
                    if (!"4".equals(tLLClaimDetailSchema.getGiveType())
                       &&!"5".equals(tLLClaimDetailSchema.getGiveType())
                       &&!("1".equals(tLLClaimDetailSchema.getGiveType())&&("04".equals(tLLClaimDetailSchema.getGiveReason())||"05".equals(tLLClaimDetailSchema.getGiveReason())||"06".equals(tLLClaimDetailSchema.getGiveReason())))
                    	)
                    {
                        CError.buildErr(this, "出险日期不在保单有效期内");
                        return false;
                    }
                }
            }
            if (tValidFlag) {
                backMsg += "<br>出险日期不在保单有效期内<br>";
            }
        }
        tDetailSum = Arith.round(tDetailSum, 2);

        if ((tPolicySum - tDetailSum > 0.01) ||
            (tDetailSum - tPolicySum) > 0.01) {
            CError.buildErr(this, "有责任理算金额不为0，但没有选择进行自动核赔");
            return false;
        }
        return true;
    }
    //当理赔责任为重疾一类的校验检录页面重大疾病信息是否录入  start
    public boolean checkRule() {
    	try{
    		LPRuleTransfer lpRuleTransfer = new LPRuleTransfer();
    		//获得规则引擎返回的校验信息 调用投保规则
    		System.out.println("开始调用规则引擎，时间"+ PubFun.getCurrentDate() + " "+ PubFun.getCurrentTime());
    		long startTime = System.currentTimeMillis();
			String xmlStr = lpRuleTransfer.getXmlStr("lis", "LP", "LS", mCaseNo, true);
			//将校验信息转换为Xml的document对象
			Document doc = lpRuleTransfer.stringToDoc(xmlStr);
			String approved = lpRuleTransfer.getApproved(doc);
			long endTime = System.currentTimeMillis();
			System.out.println("调用规则引擎结束，时间"+ PubFun.getCurrentDate() + " "+ PubFun.getCurrentTime() +"用时："+((endTime -startTime)/1000/60)+"分钟");
			System.out.println("打印传出报文============");
            JdomUtil.print(doc);
            System.out.println("规则引擎号" + approved);
            if(!"1".equals(approved)) {
            	String problemStr = lpRuleTransfer.getInForMation(doc);
            	CError.buildErr(this, problemStr);
            	return false;
            }
    	}catch(Exception e) {
    		System.out.println("执行规则引擎时异常");
    		CError.buildErr(this, "执行规则引擎时异常");
    		e.printStackTrace();
    		return false;
    	}
    	return true;
    }
    //当理赔责任为重疾一类的校验检录页面重大疾病信息是否录入  end

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData() {
        Reflections tref = new Reflections();
        //LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();
        LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
        String strSQL1 = "delete from LCInsureAccTrace where Otherno='" +
                         mCaseNo +
                         "' and othertype='5'";
        tmpMap.put(strSQL1, "DELETE");
        String strfeeSQL1 = "delete from LCInsureAccfeeTrace where Otherno='" +mCaseNo +"' and othertype='5'";
        tmpMap.put(strfeeSQL1, "DELETE");
        
        int n = mLLClaimDetailSet.size();
        double tempStandPay = 0;
        double tempRealPay = 0;
        String totalGiveType = "";
        //拒付标记
        boolean tClaimDeclineFlag = false;
        for (int i = 1; i <= n; i++) {
            LLClaimDetailSchema tCDetailSchema = mLLClaimDetailSet.get(i);
            String oldGiveType = "" + tCDetailSchema.getGiveType();
            String oldGiveReason = "" + tCDetailSchema.getGiveReason();
			String tCaseNo = "" + tCDetailSchema.getCaseNo(); //add liuyc 2014-06-25 #1811
            String oldReasonDesc = "" + tCDetailSchema.getGiveReasonDesc();
            String isRate = "" + tCDetailSchema.getDeclineNo();
            double oldApproveAmnt = tCDetailSchema.getApproveAmnt();
            double declnamnt = tCDetailSchema.getDeclineAmnt();

            //查询赔案明细
            tLLClaimDetailDB.setSchema(tCDetailSchema);
            if (!tLLClaimDetailDB.getInfo()) {
                CError.buildErr(this, "赔付明细查询错误,请先做理算");
                return false;
            }
            tCDetailSchema = tLLClaimDetailDB.getSchema();
            String newGiveType = oldGiveType;
            if (oldApproveAmnt > 0) {
                //有通融/协议给付比例时的处理
                if (!("4".equals(oldGiveType) || "5".equals(oldGiveType) ||
                      "1".equals(oldGiveType))) {
                    CError.buildErr(this, "有通融/协议给付比例值,赔付结论不符!");
                    return false;
                }
                if ("1".equals(oldGiveType) &&
                    !("01".equals(oldGiveReason) || "02".equals(oldGiveReason) ||
                      "03".equals(oldGiveReason)|| "04".equals(oldGiveReason)||
                      "05".equals(oldGiveReason)|| "06".equals(oldGiveReason)|| "07".equals(oldGiveReason))) {
                    CError.buildErr(this, "结论为正常给付且有通融/协议给付比例值,请给出改变给付比例的原因!");
                    return false;
                }
                /**为解决非标准业务tLLClaimDetailSchema中的DeclineNo
                 * 暂时存通融/协议给付比例/金额标志变量
                 * 1表示ApproveAmnt为通融/协议比例
                 * 2表示ApproveAmnt为通融/协议金额
                 * 该标志不提交到数据库
                 * 原来的设计是，ApproveAmnt小于1时为比例，大于1为金额
                 */
                if (isRate.equals("1")) {
                    double realpay = tCDetailSchema.getStandPay();
                    realpay *= oldApproveAmnt;
                    tCDetailSchema.setRealPay(Arith.round(realpay, 2));
                }
                if (isRate.equals("2")) {
                    tCDetailSchema.setRealPay(Arith.round(oldApproveAmnt, 2));
                }
                //有通融协议，其余因素不予考虑了
                tCDetailSchema.setDeclineAmnt("0");
                tCDetailSchema.setPreGiveAmnt("0");
            } else {
                if ("4".equals(oldGiveType) || "5".equals(oldGiveType)) {
                    CError.buildErr(this, "结论为通融/协议给付，但没有给出通融/协议给付比例值");
                    return false;
                }
            }
            
            if (tCDetailSchema.getRealPay()==0 && "1".equals(oldGiveType)) {
            	CError.buildErr(this, "实赔金额为0，不能选择正常给付");
                return false;            	            	
            }
            
            if ("".equals(oldGiveType)) {
                //double declineAmnt = tCDetailSchema.getDeclineAmnt();
                double aclmPay = tCDetailSchema.getTabFeeMoney();
                if (aclmPay < 0.001) {
                    aclmPay = tCDetailSchema.getClaimMoney();
                }
                newGiveType = genGiveType(tCDetailSchema.getRealPay(), aclmPay,
                                          declnamnt);
            }
            if (newGiveType.equals("3")) {
                tCDetailSchema.setRealPay(0);
                tCDetailSchema.setDeclineAmnt(declnamnt);
            }
            //非通融协议给付情况
            if (tCDetailSchema.getPreGiveAmnt() < 0) {
                CError.buildErr(this, "先期给付金额不能为负");
                return false;
            }
            if (tCDetailSchema.getDeclineAmnt() < 0) {
                CError.buildErr(this, "拒付金额不能为负");
                return false;
            }

            if ("".equals(StrTool.cTrim(oldGiveReason)) &&
                !"1".equals(newGiveType) && !"2".equals(newGiveType)) {
                //作拒付/通融/协议给付没有给出原因
                CError.buildErr(this, "有赔付责任明细不为正常给付或部分给付必须给出原因");
                return false;
            }
			//add liuyc 2014-06-25 #1811
            if (newGiveType.equals("3")) {
            	tClaimDeclineFlag = true;
            	 String tJFSQL = "select reason from LLClaimDecline where caseno='" + tCaseNo + "' "
         		+" order by makedate fetch first 1 rows only";
         		
                 ExeSQL tJFExeSQL = new ExeSQL();
                 String tJF = tJFExeSQL.getOneValue(tJFSQL); 
                 if(tJF.equals("")||tJF == null){
                	 CError.buildErr(this, "全额拒付案件须录入拒赔原因");
                     return false;
                 }
            }
            //add liuyc 2014-06-25 #1811			
            tCDetailSchema.setOperator(mG.Operator);
            tCDetailSchema.setModifyDate(aDate);
            tCDetailSchema.setModifyTime(aTime);
            tCDetailSchema.setGiveReason(oldGiveReason);
            tCDetailSchema.setGiveReasonDesc(oldReasonDesc);
            tCDetailSchema.setGiveType(newGiveType);
            tCDetailSchema.setGiveTypeDesc(getGiveDesc(newGiveType));

            //须提交的给付明细LLClaimDetailSet
            mLLClaimDetailSet.set(i, tCDetailSchema);

            tempStandPay += tCDetailSchema.getStandPay();
            tempRealPay += tCDetailSchema.getRealPay();

            totalGiveType = sumGiveType(totalGiveType, newGiveType);
            String tPolNo = tCDetailSchema.getPolNo();
            String tCaseRelaNo = tCDetailSchema.getCaseRelaNo();
            String tGetDutyKind = tCDetailSchema.getGetDutyKind();
            boolean newrec = true;
            for (int x = 1; x <= mLLClaimPolicySet.size(); x++) {
                LLClaimPolicySchema tCPolicySchema = mLLClaimPolicySet.get(x);
                if (tCPolicySchema.getPolNo().equals(tPolNo) &&
                    tCPolicySchema.getCaseRelaNo().equals(tCaseRelaNo) &&
                    tCPolicySchema.getGetDutyKind().equals(tGetDutyKind)) {
                    String tCPGiveType = sumGiveType(newGiveType,
                            tCPolicySchema.getGiveType());
                    tCPolicySchema.setGiveType(tCPGiveType);
                    double tCPrealPay = tCPolicySchema.getRealPay()
                                        + tCDetailSchema.getRealPay();
                    double tCPstandPay = tCPolicySchema.getStandPay()
                                         + tCDetailSchema.getStandPay();
                    tCPolicySchema.setRealPay(Arith.round(tCPrealPay, 2));
                    tCPolicySchema.setStandPay(Arith.round(tCPstandPay, 2));
                    newrec = false;
                    break;
                }
            }
            if (newrec) {
                LLClaimPolicySchema aCPolicySchema = new LLClaimPolicySchema();
                LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
                tref.transFields(aCPolicySchema, tCDetailSchema);
                tLLClaimPolicyDB.setSchema(aCPolicySchema);
                if (tLLClaimPolicyDB.getInfo()) {
                    aCPolicySchema.setInsuredName(tLLClaimPolicyDB.
                                                  getInsuredName());
                    aCPolicySchema.setInsuredNo(tLLClaimPolicyDB.getInsuredNo());
                    aCPolicySchema.setAppntName(tLLClaimPolicyDB.getAppntName());
                    aCPolicySchema.setAppntNo(tLLClaimPolicyDB.getAppntNo());
                    aCPolicySchema.setCValiDate(tLLClaimPolicyDB.getCValiDate());
                }
                aCPolicySchema.setClmState("1"); //已结算
                aCPolicySchema.setClmUWer(mG.Operator);
                mLLClaimPolicySet.add(aCPolicySchema);
            }
        }
        
        if(!tClaimDeclineFlag){
            String strSQLJF = "delete from LLClaimDecline where caseno='"+mCaseNo+"'";
            tmpMap.put(strSQLJF, "DELETE");
        }
        
        mGiveType = totalGiveType;
        mLLClaimSchema.setGiveType(mGiveType);
        if (mGiveType.equals("3")) {
            mLLCaseSchema.setDeclineFlag("1");
        } else {
            mLLCaseSchema.setDeclineFlag("");
        }
        mLLClaimSchema.setGiveTypeDesc(getGiveDesc(mGiveType));
        mLLClaimSchema.setClmState("2");
        tempRealPay = Arith.round(tempRealPay, 2);
        mLLClaimSchema.setRealPay(tempRealPay);
        tempStandPay = Arith.round(tempStandPay, 2);
        mLLClaimSchema.setStandPay(tempStandPay);
        tmpMap.put(mLLClaimSchema, "UPDATE");

        if (!genLJSGet()) {
            CError.buildErr(this, "生成业务应付数据失败");
            return false;
        }

        mLLCaseSchema.setRgtState("04"); //理算状态
        mLLCaseSchema.setCaseProp(mCaseProp);
        mLLCaseSchema.setClaimCalDate(aDate);
        mLLCaseSchema.setModifyDate(aDate);
        mLLCaseSchema.setModifyTime(aTime);

        tmpMap.put(this.mLLCaseSchema, "UPDATE");

        LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
        mLLCaseOpTimeSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        mLLCaseOpTimeSchema.setRgtState("04");
        mLLCaseOpTimeSchema.setOperator(mG.Operator);
        mLLCaseOpTimeSchema.setManageCom(mG.ManageCom);
        LLCaseCommon tLLCaseCommon = new LLCaseCommon();
        try {
            LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(
                    mLLCaseOpTimeSchema);
            tmpMap.put(tLLCaseOpTimeSchema, "DELETE&INSERT");
        } catch (Exception ex) {

        }

        String delsql1 = "delete from llclaimdetail where clmno='" +
                         this.mClmNo +
                         "'";
        String delsql2 = "delete from llclaimPolicy where clmno='" +
                         this.mClmNo +
                         "'";
        
        tmpMap.put(delsql1, "DELETE");
        tmpMap.put(delsql2, "DELETE");
        
        tmpMap.put(mLLClaimPolicySet, "INSERT");
        tmpMap.put(mLLClaimDetailSet, "INSERT");
        this.mResult.add(tmpMap);
        return true;
    }

    //帐户存取轨迹保存
    private boolean getInsuAcc() {
	   	 String tsql ="select a.polno,a.riskcode from lcpol a,lmrisk b where a.riskcode=b.riskcode "
             + " and b.insuaccflag = 'Y' and a.insuredno = '"
             + mLLCaseSchema.getCustomerNo() + "' union select a.polno,a.riskcode from lbpol a,lmrisk b where a.riskcode=b.riskcode "
             + " and b.insuaccflag = 'Y' and a.insuredno = '"
             + mLLCaseSchema.getCustomerNo() + "'";    
		ExeSQL tesql = new ExeSQL();
		SSRS tss = tesql.execSQL(tsql);
		
		LCInsureAccTraceSet sinsuacctraceset = new LCInsureAccTraceSet();
		for (int i = 1; i <= tss.getMaxRow(); i++) {
		   String tPolNo = tss.GetText(i, 1);
		   if (LLCaseCommon.chenkWN(tss.GetText(i, 2))) {
		       continue;
		   }
		   double totalclaim = 0;
		   for (int j = 1; j <= mLLClaimDetailSet.size(); j++) {
		       LLClaimDetailSchema tclmdetail = mLLClaimDetailSet.get(j);
		       LMDutyGetDB tLMDutyGetDB = new LMDutyGetDB();
		       tLMDutyGetDB.setGetDutyCode(tclmdetail.getGetDutyCode());
		       tLMDutyGetDB.setNeedAcc("1");
		       if(tLMDutyGetDB.query().size()>=1)
		       {
		       	if(tclmdetail.getPolNo().equals(tPolNo))
		           totalclaim += tclmdetail.getRealPay();
		       }
		      
		   }
		   totalclaim = Arith.round(totalclaim, 2);
		   if (totalclaim > 0) {
		   	GetAccAmnt tgetaccamnt = new GetAccAmnt();
		       double trealpay = tgetaccamnt.getAccPay(tPolNo, mCaseNo,totalclaim);
		       sinsuacctraceset.add(tgetaccamnt.getTrace());
		       backMsg += tgetaccamnt.getAccMsg();
		       if (Arith.round(trealpay, 2) < totalclaim) {
		           CError.buildErr(this, backMsg);
		           return false;
		       }
		   }
		}
		tmpMap.put(sinsuacctraceset, "DELETE&INSERT");
		return true;
    }

    private String genGiveType(double aRealPay, double aClmPay,
                               double aDeclnAmnt) {
        if (aRealPay < 0.001) {
            return "3";
        }
        if (aDeclnAmnt < 0.001) {
            return "1";
        }
        if (Math.abs(aClmPay - aDeclnAmnt) < 0.001) {
            return "3"; //全额拒付
        } else {
            return "2";
        }
    }

    private String sumGiveType(String fGiveType, String sGiveType) {
        if (fGiveType.equals("")) {
            return sGiveType;
        }
        if (sGiveType.equals("")) {
            return fGiveType;
        }
        if (fGiveType.equals("4") || fGiveType.equals("5")) {
            return fGiveType;
        }
        if (sGiveType.equals("4") || sGiveType.equals("5")) {
            return sGiveType;
        }
        boolean fRefuse = fGiveType.equals("3");
        boolean sRefuse = sGiveType.equals("3");
        if (fRefuse && sRefuse) {
            return "3"; //全拒
        }
        if (fRefuse || sRefuse) {
            return "2"; //拒一个
        }
        if (fGiveType.equals("2") || sGiveType.equals("2")) {
            return "2";
        } else {
            return "1";
        }
    }

    /**
     * 检查保单赔付金额是否超过保单的保额
     * @return boolean
     */
//    private boolean checkAmnt() {
//        boolean tFlag = true;
//        int getdutycount = mLLClaimDetailSet.size();
//        for (int w = 1; w <= getdutycount; w++) {
//
//            System.out.println(mLLClaimDetailSet.get(w).getGetDutyCode());
//            System.out.println(mLLClaimDetailSet.get(w).getGetDutyKind());
//            String tGetDutyCode = mLLClaimDetailSet.get(w).getGetDutyCode();
//            String tGetDutyKind = mLLClaimDetailSet.get(w).getGetDutyKind();
//            LMDutyGetClmDB tLMDutyGetClmDB = new LMDutyGetClmDB();
//            tLMDutyGetClmDB.setGetDutyCode(tGetDutyCode);
//            tLMDutyGetClmDB.setGetDutyKind(tGetDutyKind);
//            tLMDutyGetClmDB.getInfo();
//            LMDutyGetClmSchema tLMDutyGetClmSchema = new LMDutyGetClmSchema();
//            tLMDutyGetClmSchema = tLMDutyGetClmDB.getSchema();
//            if (tLMDutyGetClmSchema.getInpFlag().trim().equals("4")) {
//                tFlag = false;
//                break;
//            }
//
//        }
//
//        LCDutySet tDutySet = new LCDutySet();
//        SynLCLBDutyBL tSynLCLBDutyBL = new SynLCLBDutyBL();
//        tSynLCLBDutyBL.setDutyCode(mLLCaseSchema.getCustomerNo());
//        tDutySet = tSynLCLBDutyBL.query();
//        LCPolSet tPolSet = new LCPolSet();
//        LCPolBL tLCPolBL = new LCPolBL();
//        LCGetSet tGetSet = new LCGetSet();
//        SynLCLBGetBL tSynLCLBGetBL = new SynLCLBGetBL();
//        String tsql = "";
//        ExeSQL texesql = new ExeSQL();
//        for (int i = 1; i <= getdutycount; i++) {
//            LLClaimDetailSchema tCDetailSchema = mLLClaimDetailSet.get(i);
//
//            boolean haveget = false;
//            for (int j = 1; j <= tGetSet.size(); j++) {
//                LCGetSchema tLCGetSchema = tGetSet.get(j);
//                if (tLCGetSchema.getPolNo().equals(tCDetailSchema.getPolNo())
//                    &&
//                    tLCGetSchema.getDutyCode().equals(tCDetailSchema.getDutyCode())
//                    &&
//                    tLCGetSchema.getGetDutyCode().equals(tCDetailSchema.getGetDutyCode())) {
//                    haveget = true;
//                    System.out.println("本次拒付。。。" +
//                                       tCDetailSchema.getDeclineAmnt());
//                    double realpay1 = tCDetailSchema.getRealPay();
//                    String isRate = "" + tCDetailSchema.getDeclineNo();
//                    double oldApproveAmnt = tCDetailSchema.getApproveAmnt();
//                    if ("1".equals(isRate)) {
//                        realpay1 *= oldApproveAmnt;
//                    }
//                    if (isRate.equals("2")) {
//                        realpay1 = oldApproveAmnt;
//                    }
//
//                    double sumpay = Arith.round(tLCGetSchema.getGetLimit() + realpay1,2);
//                    System.out.println("本次赔付。。。" + sumpay);
//                    if (!"5".equals(tCDetailSchema.getGiveType())&& !"3".equals(tCDetailSchema.getGiveType())) {
//                        tLCGetSchema.setGetLimit(sumpay);
//                    }
//                    break;
//                }
//            }
//            if (!haveget) {
//                LCGetSchema tGetSchema = new LCGetSchema();
//                tSynLCLBGetBL.setPolNo(tCDetailSchema.getPolNo());
//                tSynLCLBGetBL.setDutyCode(tCDetailSchema.getDutyCode());
//                tSynLCLBGetBL.setGetDutyCode(tCDetailSchema.getGetDutyCode());
//                if (!tSynLCLBGetBL.getInfo()) {
//                    CError.buildErr(this, "保单信息查询失败！");
//                    return false;
//                }
//                tGetSchema = tSynLCLBGetBL.getSchema();
//                System.out.println("本次拒付0。。。" + tCDetailSchema.getDeclineAmnt());
//                double realpay1 = tCDetailSchema.getRealPay();
//                String isRate = "" + tCDetailSchema.getDeclineNo();
//                double oldApproveAmnt = tCDetailSchema.getApproveAmnt();
//                if ("1".equals(isRate)) {
//                    realpay1 *= oldApproveAmnt;
//                }
//                if (isRate.equals("2")) {
//                    realpay1 = oldApproveAmnt;
//                }
//                tGetSchema.setGetLimit(Arith.round(realpay1,2));
//                System.out.println("本次赔付0。。。" + tGetSchema.getGetLimit());
//                if (!"5".equals(tCDetailSchema.getGiveType())&& !"3".equals(tCDetailSchema.getGiveType())) {
//                    tGetSet.add(tGetSchema);
//                }
//            }
//
//            boolean haveduty = false;
//            for (int j = 1; j <= tDutySet.size(); j++) {
//                LCDutySchema tLCDutySchema = tDutySet.get(j);
//                if (tLCDutySchema.getPolNo().equals(tCDetailSchema.getPolNo())
//                    &&
//                    tLCDutySchema.getDutyCode().equals(tCDetailSchema.getDutyCode())) {
//                    haveduty = true;
//                    System.out.println("本次拒付。。。" +
//                                       tCDetailSchema.getDeclineAmnt());
//                    double realpay1 = tCDetailSchema.getRealPay();
//                    String isRate = "" + tCDetailSchema.getDeclineNo();
//                    double oldApproveAmnt = tCDetailSchema.getApproveAmnt();
//                    if ("1".equals(isRate)) {
//                        realpay1 *= oldApproveAmnt;
//                    }
//                    if (isRate.equals("2")) {
//                        realpay1 = oldApproveAmnt;
//                    }
//
//                    double sumpay = Arith.round(tLCDutySchema.getGetLimit() + realpay1,2);
//                    System.out.println("本次赔付。。。" + sumpay);
//                    tLCDutySchema.setGetLimit(sumpay);
//                    break;
//                }
//            }
//            if (!haveduty) {
//                LCDutySchema tDutySchema = new LCDutySchema();
//                tSynLCLBDutyBL.setPolNo(tCDetailSchema.getPolNo());
//                tSynLCLBDutyBL.setDutyCode(tCDetailSchema.getDutyCode());
//                if (!tSynLCLBDutyBL.getInfo()) {
//                    CError.buildErr(this, "保单信息查询失败！");
//                    return false;
//                }
//                tDutySchema = tSynLCLBDutyBL.getSchema();
//                System.out.println("本次拒付0。。。" + tCDetailSchema.getDeclineAmnt());
//                double realpay1 = tCDetailSchema.getRealPay();
//                String isRate = "" + tCDetailSchema.getDeclineNo();
//                double oldApproveAmnt = tCDetailSchema.getApproveAmnt();
//                if ("1".equals(isRate)) {
//                    realpay1 *= oldApproveAmnt;
//                }
//                if (isRate.equals("2")) {
//                    realpay1 = oldApproveAmnt;
//                }
//                tDutySchema.setGetLimit(Arith.round(realpay1,2));
//                System.out.println("本次赔付0。。。" + tDutySchema.getGetLimit());
//                if (!"5".equals(tCDetailSchema.getGiveType())&& !"3".equals(tCDetailSchema.getGiveType())) {
//                    tDutySet.add(tDutySchema);
//                }
//            }
//            boolean havepol = false;
//            for (int j = 1; j <= tPolSet.size(); j++) {
//                LCPolSchema tLCPolSchema = tPolSet.get(j);
//                if (tLCPolSchema.getPolNo().equals(tCDetailSchema.getPolNo())) {
//                    havepol = true;
//                    double realpay1 = tCDetailSchema.getRealPay();
//                    String isRate = "" + tCDetailSchema.getDeclineNo();
//                    double oldApproveAmnt = tCDetailSchema.getApproveAmnt();
//                    if ("1".equals(isRate)) {
//                        realpay1 *= oldApproveAmnt;
//                    }
//                    if (isRate.equals("2")) {
//                        realpay1 = oldApproveAmnt;
//                    }
//
//                    double sumpay = tLCPolSchema.getLeavingMoney() + Arith.round(realpay1,2);
//                    tLCPolSchema.setLeavingMoney(sumpay);
//                    break;
//                }
//            }
//            if (!havepol) {
//                LCPolSchema tPolSchema = new LCPolSchema();
//                tLCPolBL.setPolNo(tCDetailSchema.getPolNo());
//                if (!tLCPolBL.getInfo()) {
//                    CError.buildErr(this, "保单信息查询失败！");
//                    return false;
//                }
//                tPolSchema = tLCPolBL.getSchema();
//                double realpay1 = tCDetailSchema.getRealPay();
//                String isRate = "" + tCDetailSchema.getDeclineNo();
//                double oldApproveAmnt = tCDetailSchema.getApproveAmnt();
//                if ("1".equals(isRate)) {
//                    realpay1 *= oldApproveAmnt;
//                }
//                if (isRate.equals("2")) {
//                    realpay1 = oldApproveAmnt;
//                }
//                tPolSchema.setLeavingMoney(Arith.round(realpay1,2));
//
//                if (!"5".equals(tCDetailSchema.getGiveType())&& !"3".equals(tCDetailSchema.getGiveType())) {
//                    tPolSet.add(tPolSchema);
//                }
//            }
//        }
//        if (tFlag) {
//            for (int i = 1; i <= tGetSet.size(); i++) {
//                if (tGetSet.get(i).getStandMoney() < 0.001) {
//                    continue;
//                }
//                LCGetSchema tGetSchema = tGetSet.get(i);
//                tsql =
//                        "select sum(realpay) from llclaimdetail a,llcase b where "
//                        + "a.caseno=b.caseno and a.polno='" +
//                        tGetSchema.getPolNo()
//                        + "' and a.dutycode='" + tGetSchema.getDutyCode()
//                        + "' and a.getdutycode='" + tGetSchema.getGetDutyCode()
//                        + "' and b.rgtstate in ('09','10','11','12') "
//                        //处理多次纠错的问题，但是不能有多个案件同时纠错
//                        + "and not exists (select 1 from llappeal where caseno=b.caseno and appealno='"+mCaseNo
//                        + "') and not exists (select 1 from llappeal where appealno<>'"+mCaseNo
//                        + "' and appealno=b.caseno and caseno=(select caseno from llappeal where appealno='"+mCaseNo
//                        + "'))";
//                String ogetsum = texesql.getOneValue(tsql);
//                double Ogetsum = 0;
//                if (!ogetsum.equals("null") && !ogetsum.equals("")) {
//                    Ogetsum = Double.parseDouble(ogetsum);
//                }
//                System.out.println(tGetSchema.getDutyCode());
//                System.out.println(tGetSchema.getGetLimit());
//                System.out.println(tGetSchema.getStandMoney());
//                System.out.println(Ogetsum);
//                if (LLCaseCommon.chechWNPol(tGetSchema.getPolNo())) {
//                    continue;
//                }
//
//                if (Ogetsum + tGetSchema.getGetLimit() -
//                    tGetSchema.getStandMoney() > 0.001) {
//                    CError.buildErr(this, "给付责任" + tGetSchema.getDutyCode()
//                                    + "保额：" + tGetSchema.getStandMoney() +
//                                    ",历史赔付："
//                                    + Ogetsum + ",本次赔付：" +
//                                    tGetSchema.getGetLimit()
//                                    + ",已经超保额！");
//                    return false;
//                }
//            }
//
//            for (int i = 1; i <= tDutySet.size(); i++) {
//                if (tDutySet.get(i).getRiskAmnt() < 0.001) {
//                    continue;
//                }
//                LCDutySchema tDutySchema = tDutySet.get(i);
//                tsql =
//                        "select sum(realpay) from llclaimdetail a,llcase b where "
//                        + "a.caseno=b.caseno and a.polno='" +
//                        tDutySchema.getPolNo()
//                        + "' and a.dutycode='" + tDutySchema.getDutyCode()
//                        + "' and b.rgtstate in ('09','10','11','12') "
//                        //处理多次纠错的问题，但是不能有多个案件同时纠错
//                        + "and not exists (select 1 from llappeal where caseno=b.caseno and appealno='"+mCaseNo
//                        + "') and not exists (select 1 from llappeal where appealno<>'"+mCaseNo
//                        + "' and appealno=b.caseno and caseno=(select caseno from llappeal where appealno='"+mCaseNo
//                        + "'))";
//                String odutysum = texesql.getOneValue(tsql);
//                double Odutysum = 0;
//                if (!odutysum.equals("null") && !odutysum.equals("")) {
//                    Odutysum = Double.parseDouble(odutysum);
//                }
//                System.out.println(tDutySchema.getDutyCode());
//                System.out.println(tDutySchema.getGetLimit());
//                System.out.println(tDutySchema.getRiskAmnt());
//                System.out.println(Odutysum);
//                if (LLCaseCommon.chechWNPol(tDutySchema.getPolNo())) {
//                    continue;
//                }
//
//                if (Odutysum + tDutySchema.getGetLimit() -
//                    tDutySchema.getRiskAmnt() >
//                    0.001) {
//                    CError.buildErr(this, "责任" + tDutySchema.getDutyCode()
//                                    + "保额：" + tDutySchema.getRiskAmnt() +
//                                    ",历史赔付："
//                                    + Odutysum + ",本次赔付：" +
//                                    tDutySchema.getGetLimit()
//                                    + ",已经超保额！");
//                    return false;
//                }
//            }
//            for (int i = 1; i <= tPolSet.size(); i++) {
//                String riskamntflag_sql =
//                        "select riskamntflag from lmrisk where riskcode='" +
//                        tPolSet.get(i).getRiskCode() + "' with ur";
//                String riskamntflag = texesql.getOneValue(riskamntflag_sql);
//                if ("Y".equals(riskamntflag)) {
//                    if (tPolSet.get(i).getRiskAmnt() < 0.001) {
//                        continue;
//                    }
//                } else {
//                    if (tPolSet.get(i).getAmnt() < 0.001) {
//                        continue;
//                    }
//                }
//                LCPolSchema tPolSchema = tPolSet.get(i);
//                tsql =
//                        "select sum(realpay) from llclaimdetail a,llcase b where "
//                        + "a.caseno=b.caseno and a.polno='" +
//                        tPolSchema.getPolNo()
//                        + "' and b.rgtstate in ('09','10','11','12') "
//                        //处理多次纠错的问题，但是不能有多个案件同时纠错
//                        + "and not exists (select 1 from llappeal where caseno=b.caseno and appealno='"+mCaseNo
//                        + "') and not exists (select 1 from llappeal where appealno<>'"+mCaseNo
//                        + "' and appealno=b.caseno and caseno=(select caseno from llappeal where appealno='"+mCaseNo
//                        + "'))";
//
//                String opolsum = texesql.getOneValue(tsql);
//                double Opolsum = 0;
//                if (!opolsum.equals("null") && !opolsum.equals("")) {
//                    Opolsum = Double.parseDouble(opolsum);
//                }
//                if (LLCaseCommon.chechWNPol(tPolSchema.getPolNo())) {
//                    continue;
//                }
//
//                if ("Y".equals(riskamntflag)) {
//                    if (Opolsum + tPolSchema.getLeavingMoney() -
//                        tPolSchema.getRiskAmnt() >
//                        0.001) {
//                        CError.buildErr(this, "险种:" + tPolSchema.getRiskCode()
//                                        + "保额：" + tPolSchema.getRiskAmnt() +
//                                        ",历史赔付："
//                                        + Opolsum + ",本次赔付：" +
//                                        tPolSchema.getLeavingMoney()
//                                        + ",已经超保额！");
//                        return false;
//                    }
//
//                } else {
//                    if (Opolsum + tPolSchema.getLeavingMoney() -
//                        tPolSchema.getAmnt() >
//                        0.001) {
//                        CError.buildErr(this, "险种:" + tPolSchema.getRiskCode()
//                                        + "保额：" + tPolSchema.getAmnt() +
//                                        ",历史赔付："
//                                        + Opolsum + ",本次赔付：" +
//                                        tPolSchema.getLeavingMoney()
//                                        + ",已经超保额！");
//                        return false;
//                    }
//                }
//            }
//        }
//        return true;
//    }

    /**
     * 业务应付生成
     */
    private boolean genLJSGet() {
        String trisk="";
        for (int t = 1; t <= mLLClaimDetailSet.size(); t++) {
       	 	if(t==1){
       	 		trisk="'"+mLLClaimDetailSet.get(t).getRiskCode()+"'";
       	 	}else{
       	 		trisk=trisk+",'"+mLLClaimDetailSet.get(t).getRiskCode()+"'";
       	 	}
        }
    	 String delsql1 =
          	"delete from ljsgetclaim where othernotype='5' and otherno='"
          	+ mCaseNo + "'";
    	//2366关于理赔案件处理红利处理的系统功能**start**新增代码
    	 String delsql3 =
           	"delete from ljsgetclaim where othernotype='H' and otherno='"
           	+ mCaseNo + "'";
    	//2366关于理赔案件处理红利处理的系统功能**end**新增代码
	     String delsql2 =
	             "delete from ljsget where othernotype='5' and otherno='"
	             + mCaseNo + "'";
	     tmpMap.put(delsql1, "DELETE");
	     tmpMap.put(delsql2, "DELETE");
	     tmpMap.put(delsql3, "DELETE");//2366关于理赔案件处理红利处理的系统功能
	
	     LJSGetClaimSet mLJSGetClaimSet = new LJSGetClaimSet();
	     LJSGetSchema mLJSGetSchema = new LJSGetSchema();
	     LJSGetSet mLJSGetSet = new LJSGetSet();
	     String tsql = "select distinct riskcode from llclaimdetail where clmno='" +mClmNo +"' and riskcode in ("+trisk+") with ur" ;
	     ExeSQL texeSQL = new ExeSQL();
	     SSRS tssrs = texeSQL.execSQL(tsql);
	     if (texeSQL.mErrors.getErrorCount() > 0 || tssrs == null || tssrs.getMaxRow() <= 0) 
	     {
	    	 CError.buildErr(this, "生成应付统计错误");
	    	 return false;
	     }
		 System.out.println(tssrs.getMaxRow());
	     String strLimit = PubFun.getNoLimit(mG.ManageCom);
	     String tGetNoticeNo = PubFun1.CreateMaxNo("GetNoticeNo", strLimit);	
	     mGetNoticeNo = PubFun1.CreateMaxNo("GetNoticeNo", strLimit);;//2366关于理赔案件处理红利处理的系统功能(一笔应付actuno要一致)  需要修改为2比应付数据 
	     double tSumRealPay = 0;
	     for (int r = 1; r <= tssrs.getMaxRow(); r++) 
	     {	    	
//    		 #2668
    		 String SQL = "select 1 from lmriskapp where taxoptimal='Y' and  riskcode ='"+tssrs.GetText(r, 1)+"'";
    	        ExeSQL tExeSQL = new ExeSQL();
    	        String risktype = tExeSQL.getOneValue(SQL);
	    	 if (!LLCaseCommon.chenkWN(tssrs.GetText(r, 1))||"1".equals(risktype)) //非万能险
		     {
	    		 //非万能&&非特需账户
	    	     String sql = "select sum(a.realpay),a.stattype,a.GetDutyKind,a.polno,a.kindcode," 
	    	                +" a.riskcode,a.riskver from llclaimdetail a,lmdutyget b,lmriskapp c "
	    	     			+" where a.getdutycode=b.getdutycode and a.riskcode=c.riskcode and a.clmno='" + mClmNo +"' "
	    	     			+" and (c.risktype3<>'7' or b.needacc='0') and a.riskcode='"+tssrs.GetText(r, 1)+"'"
	    	                +" group by a.stattype,a.getdutykind,a.polno,a.kindcode,a.riskcode,a.riskver ";	                
	    	              
	    	     ExeSQL exeSQL = new ExeSQL();
	    	     System.out.println("非万能非特需账户责任赔款查询sql=="+sql);
	    	     SSRS ssrs = exeSQL.execSQL(sql);
	    	     if ( ssrs == null || ssrs.getMaxRow() <= 0) {
	    	    	 System.out.println("Line950=====非特需账户&&非万能数据不存在");
	    	     }	     
	    	     else if(ssrs.getMaxRow()>=1){ 				
	    		     System.out.println(ssrs.getMaxRow());		    
	    		     for (int i = 1; i <= ssrs.getMaxRow(); i++) {
	    		         LJSGetClaimSchema tLJSGetClaimSchema = new LJSGetClaimSchema();
	    		         tLJSGetClaimSchema.setFeeFinaType(ssrs.GetText(i, 2));
	    		         tLJSGetClaimSchema.setGetDutyKind(ssrs.GetText(i, 3));
	    		         tLJSGetClaimSchema.setFeeOperationType(ssrs.GetText(i, 3));
	    		         tLJSGetClaimSchema.setPay(0);
	    		         for (int k = 1; k <= mLLClaimDetailSet.size(); k++) {
	    		             LLClaimDetailSchema aLLClaimDetailSchema = mLLClaimDetailSet.get(k);
	    		             if (ssrs.GetText(i, 2).equals(aLLClaimDetailSchema.getStatType()) &&
	    		            	 ssrs.GetText(i, 3).equals(aLLClaimDetailSchema.getGetDutyKind()) &&
	    		                 ssrs.GetText(i, 4).equals(aLLClaimDetailSchema.getPolNo()) &&
	    		                 ssrs.GetText(i, 5).equals(aLLClaimDetailSchema.getKindCode()) &&
	    		                 ssrs.GetText(i, 6).equals(aLLClaimDetailSchema.getRiskCode()) &&
	    		                 ssrs.GetText(i, 7).equals(aLLClaimDetailSchema.getRiskVer())) 
	    		             {
	    		            	 LMDutyGetDB tLMDutyGetDB = new LMDutyGetDB();
	    		            	 tLMDutyGetDB.setGetDutyCode(aLLClaimDetailSchema.getGetDutyCode());
	    		            	 tLMDutyGetDB.setNeedAcc("0");
	    		            	 if(tLMDutyGetDB.query().size()>=1 ||"1".equals(risktype))//#2668税优不赔账户
	    		            	 {
	    		            		 double apaymoney = tLJSGetClaimSchema.getPay() + aLLClaimDetailSchema.getRealPay();
	    			                 apaymoney = Arith.round(apaymoney, 2);
	    			                 tLJSGetClaimSchema.setPay(apaymoney); 
	    		            	 }		
	    		             }
	    		         }
	    		         tLJSGetClaimSchema.setPolNo(ssrs.GetText(i, 4));
	    		         tLJSGetClaimSchema.setKindCode(ssrs.GetText(i, 5));
	    		         tLJSGetClaimSchema.setRiskCode(ssrs.GetText(i, 6));
	    		         tLJSGetClaimSchema.setRiskVersion(ssrs.GetText(i, 7));
	    		         LCPolBL tLCPolBL = new LCPolBL();
	    		         tLCPolBL.setPolNo(ssrs.GetText(i, 4));
	    		         if (!tLCPolBL.getInfo()) {
	    		             CError.buildErr(this, "保单查询失败[" + ssrs.GetText(i, 4) + "]");
	    		             return false;
	    		         }
	    		         tLJSGetClaimSchema.setGetNoticeNo(tGetNoticeNo);
	    		         tLJSGetClaimSchema.setContNo(tLCPolBL.getContNo());
	    		         tLJSGetClaimSchema.setGrpContNo(tLCPolBL.getGrpContNo());
	    		         tLJSGetClaimSchema.setGrpPolNo(tLCPolBL.getGrpPolNo());
	    		         tLJSGetClaimSchema.setAgentCode(tLCPolBL.getAgentCode());
	    		         tLJSGetClaimSchema.setAgentCom(tLCPolBL.getAgentCom());
	    		         tLJSGetClaimSchema.setAgentGroup(tLCPolBL.getAgentGroup());
	    		         tLJSGetClaimSchema.setAgentType(tLCPolBL.getAgentType());
	    		         tLJSGetClaimSchema.setOperator(this.mG.Operator);
	    		         tLJSGetClaimSchema.setManageCom(tLCPolBL.getManageCom());
	    		         tLJSGetClaimSchema.setMakeDate(aDate);
	    		         tLJSGetClaimSchema.setMakeTime(aTime);
	    		         tLJSGetClaimSchema.setModifyDate(aDate);
	    		         tLJSGetClaimSchema.setModifyTime(aTime);
	    		         tLJSGetClaimSchema.setOtherNo(mCaseNo);
	    		         tLJSGetClaimSchema.setOtherNoType("5");
	    		         System.out.println("非万能非特需账户责任赔款=="+tLJSGetClaimSchema.getPay());
	    		         
	    		         mLJSGetClaimSet.add(tLJSGetClaimSchema);
	    		         tSumRealPay += tLJSGetClaimSchema.getPay();
	    		     }
	    	     }
	    		//增加特需账户
			    String Accsql = "select sum(a.realpay),a.stattype,a.GetDutyKind,a.polno,a.kindcode," 
		         		+" a.riskcode,a.riskver from llclaimdetail a,lmdutyget b,lmriskapp c " 
		         		+" where a.getdutycode=b.getdutycode and a.riskcode=c.riskcode and a.clmno='" + mClmNo + "'"
		         		+" and b.needacc='1' and c.risktype4<>'4' and a.riskcode='"+tssrs.GetText(r, 1)+"'" 
		         		+" group by a.stattype,a.getdutykind,a.polno,a.kindcode,a.riskcode,a.riskver ";
				ExeSQL AccexeSQL = new ExeSQL();
				System.out.println("非万能赔账户责任Accsql===="+Accsql);
				SSRS Accssrs = AccexeSQL.execSQL(Accsql);
				if ( Accssrs == null || Accssrs.getMaxRow() <= 0) 
				{
					System.out.println("=====赔特需账户数据不存在");
				}		     
				else if(Accssrs.getMaxRow()>=1)
				{
				    for (int i = 1; i <= Accssrs.getMaxRow(); i++) 
					{
						 LCPolBL tLCPolBL = new LCPolBL();
				         tLCPolBL.setPolNo(Accssrs.GetText(i, 4));
				         if (!tLCPolBL.getInfo()) {
				             CError.buildErr(this, "保单查询失败[" + Accssrs.GetText(i, 4) + "]");
				             return false;
				         }
						 LJSGetClaimSchema aLJSGetClaimSchema = new LJSGetClaimSchema();
						 aLJSGetClaimSchema.setPay(0);
						 for (int k = 1; k <= mLLClaimDetailSet.size(); k++) {
							 LLClaimDetailSchema aLLClaimDetailSchema = mLLClaimDetailSet.get(k);
		    		         if (Accssrs.GetText(i, 2).equals(aLLClaimDetailSchema.getStatType()) &&
		    		            	 Accssrs.GetText(i, 3).equals(aLLClaimDetailSchema.getGetDutyKind()) &&
		    		            	 Accssrs.GetText(i, 4).equals(aLLClaimDetailSchema.getPolNo()) &&
		    		            	 Accssrs.GetText(i, 5).equals(aLLClaimDetailSchema.getKindCode()) &&
		    		            	 Accssrs.GetText(i, 6).equals(aLLClaimDetailSchema.getRiskCode()) &&
		    		            	 Accssrs.GetText(i, 7).equals(aLLClaimDetailSchema.getRiskVer())) 
		    		         {
		    		        	 LMDutyGetDB tLMDutyGetDB = new LMDutyGetDB();
		    		             tLMDutyGetDB.setGetDutyCode(aLLClaimDetailSchema.getGetDutyCode());
		    		             tLMDutyGetDB.setNeedAcc("1");
		    		             if(tLMDutyGetDB.query().size()>=1)
		    		             {
		    		            	 double apaymoney = aLJSGetClaimSchema.getPay() + aLLClaimDetailSchema.getRealPay();
		    			             apaymoney = Arith.round(apaymoney, 2);
		    			             aLJSGetClaimSchema.setPay(apaymoney); 
		    		             }		
		    		         }
						 }
						 aLJSGetClaimSchema.setFeeFinaType("ZH");
						 aLJSGetClaimSchema.setGetDutyKind(Accssrs.GetText(i, 3));
						 aLJSGetClaimSchema.setFeeOperationType(Accssrs.GetText(i, 3));
			
						 aLJSGetClaimSchema.setPolNo(Accssrs.GetText(i, 4));
				         aLJSGetClaimSchema.setKindCode(Accssrs.GetText(i, 5));
				         aLJSGetClaimSchema.setRiskCode(Accssrs.GetText(i, 6));
				         aLJSGetClaimSchema.setRiskVersion(Accssrs.GetText(i, 7));
				         aLJSGetClaimSchema.setGetNoticeNo(tGetNoticeNo);
				         aLJSGetClaimSchema.setContNo(tLCPolBL.getContNo());
				         aLJSGetClaimSchema.setGrpContNo(tLCPolBL.getGrpContNo());
				         aLJSGetClaimSchema.setGrpPolNo(tLCPolBL.getGrpPolNo());
				         aLJSGetClaimSchema.setAgentCode(tLCPolBL.getAgentCode());
				         aLJSGetClaimSchema.setAgentCom(tLCPolBL.getAgentCom());
				         aLJSGetClaimSchema.setAgentGroup(tLCPolBL.getAgentGroup());
				         aLJSGetClaimSchema.setAgentType(tLCPolBL.getAgentType());
				         aLJSGetClaimSchema.setOperator(this.mG.Operator);
				         aLJSGetClaimSchema.setManageCom(tLCPolBL.getManageCom());
				         aLJSGetClaimSchema.setMakeDate(aDate);
				         aLJSGetClaimSchema.setMakeTime(aTime);
				         aLJSGetClaimSchema.setModifyDate(aDate);
				         aLJSGetClaimSchema.setModifyTime(aTime);
				         aLJSGetClaimSchema.setOtherNo(mCaseNo);
				         aLJSGetClaimSchema.setOtherNoType("5");				         
				         mLJSGetClaimSet.add(aLJSGetClaimSchema);
				         System.out.println("特需账户责任赔款=="+aLJSGetClaimSchema.getPay());					         
				         tSumRealPay += aLJSGetClaimSchema.getPay();							
					}
				}
		     }
	    	 else if (LLCaseCommon.chenkWN(tssrs.GetText(r, 1))) //万能险
		     {
	    		//赔万能账户分成赔账户部分和赔保额部分
				String WNsql = "select sum(a.realpay),a.stattype,a.GetDutyKind,a.polno,a.kindcode," 
	                    +" a.riskcode,a.riskver,a.getdutycode from llclaimdetail a  "
		     			+" where a.clmno='" + mClmNo +"' and a.riskcode='"+tssrs.GetText(r, 1)+"'"		     			
		     		    +" group by a.stattype,a.getdutykind,a.polno,a.kindcode,a.riskcode,a.riskver,a.getdutycode ";
				ExeSQL WNexeSQL = new ExeSQL();
				System.out.println("赔万能险查询WNsql=="+WNsql);
				SSRS WNssrs = WNexeSQL.execSQL(WNsql);
				if ( WNssrs == null || WNssrs.getMaxRow() <= 0) {System.out.println("=====赔万能数据不存在");}		     
				else if(WNssrs.getMaxRow()>=1)
				{
					for (int i = 1; i <= WNssrs.getMaxRow(); i++) 
					{
						 LJSGetClaimSchema bLJSGetClaimSchema = new LJSGetClaimSchema();
						 bLJSGetClaimSchema.setFeeFinaType(WNssrs.GetText(i, 2));
						 bLJSGetClaimSchema.setGetDutyKind(WNssrs.GetText(i, 3));
						 bLJSGetClaimSchema.setFeeOperationType(WNssrs.GetText(i, 3));
						 bLJSGetClaimSchema.setPay(0);
						 bLJSGetClaimSchema.setPolNo(WNssrs.GetText(i, 4));
				         bLJSGetClaimSchema.setKindCode(WNssrs.GetText(i, 5));
				         bLJSGetClaimSchema.setRiskCode(WNssrs.GetText(i, 6));
				         bLJSGetClaimSchema.setRiskVersion(WNssrs.GetText(i, 7));
				         LCPolBL tLCPolBL = new LCPolBL();
				         tLCPolBL.setPolNo(WNssrs.GetText(i, 4));
				         if (!tLCPolBL.getInfo()) {
				             CError.buildErr(this, "保单查询失败[" + WNssrs.GetText(i, 4) + "]");
				             return false;
				         }
				         bLJSGetClaimSchema.setGetNoticeNo(tGetNoticeNo);
				         bLJSGetClaimSchema.setContNo(tLCPolBL.getContNo());
				         bLJSGetClaimSchema.setGrpContNo(tLCPolBL.getGrpContNo());
				         bLJSGetClaimSchema.setGrpPolNo(tLCPolBL.getGrpPolNo());
				         bLJSGetClaimSchema.setAgentCode(tLCPolBL.getAgentCode());
				         bLJSGetClaimSchema.setAgentCom(tLCPolBL.getAgentCom());
				         bLJSGetClaimSchema.setAgentGroup(tLCPolBL.getAgentGroup());
				         bLJSGetClaimSchema.setAgentType(tLCPolBL.getAgentType());
				         bLJSGetClaimSchema.setOperator(this.mG.Operator);
				         bLJSGetClaimSchema.setManageCom(tLCPolBL.getManageCom());
				         bLJSGetClaimSchema.setMakeDate(aDate);
				         bLJSGetClaimSchema.setMakeTime(aTime);
				         bLJSGetClaimSchema.setModifyDate(aDate);
				         bLJSGetClaimSchema.setModifyTime(aTime);
				         bLJSGetClaimSchema.setOtherNo(mCaseNo);
				         bLJSGetClaimSchema.setOtherNoType("5");
				         
				         LJSGetClaimSchema cLJSGetClaimSchema = new LJSGetClaimSchema();
				         for (int k = 1; k <= mLLClaimDetailSet.size(); k++) {
				             LLClaimDetailSchema aLLClaimDetailSchema = mLLClaimDetailSet.get(k);
				             if (WNssrs.GetText(i, 2).equals(aLLClaimDetailSchema.getStatType()) &&
			            		 WNssrs.GetText(i, 3).equals(aLLClaimDetailSchema.getGetDutyKind()) &&
			            		 WNssrs.GetText(i, 4).equals(aLLClaimDetailSchema.getPolNo()) &&
			            		 WNssrs.GetText(i, 5).equals(aLLClaimDetailSchema.getKindCode()) &&
			            		 WNssrs.GetText(i, 6).equals(aLLClaimDetailSchema.getRiskCode()) &&
			            		 WNssrs.GetText(i, 7).equals(aLLClaimDetailSchema.getRiskVer())) 
				             {			
				            	 double apaymoney = bLJSGetClaimSchema.getPay() + aLLClaimDetailSchema.getRealPay();
				            	 double aAccmoney = 0;
				            	 if(aLLClaimDetailSchema.getRealPay()>0 )
				            	 {
				            		 //180内赔保费的话，全部赔款算账户部分，并且反冲之前投保、月结产生的利息、账户管理费、风险保费
				            		 LLElementDetailDB tRgtDateLLElementDetailDB = new LLElementDetailDB();
			                         String tAccRDSQL = "select * from LLElementDetail where caseno='" + mCaseNo + "' "
	                            			+" and polno='"+WNssrs.GetText(i, 4)+"' and getdutykind='"+WNssrs.GetText(i, 3)+"'"
			                        		+" and elementcode='RgtDays' and bigint(ElementValue)<=180";
			                         LLElementDetailSet RdLLElementDetailSet = tRgtDateLLElementDetailDB.executeQuery(tAccRDSQL);
			                            
			                         String tIncidentSQL = "select * from LLElementDetail where caseno='" + mCaseNo + "' "
			                        		+" and polno='"+WNssrs.GetText(i, 4)+"' and getdutykind='"+WNssrs.GetText(i, 3)+"'"
			                        		+" and elementcode='Incident' and ElementValue<>'1'";
	                                 LLElementDetailSet tIncidentLLElementDetailSet = tRgtDateLLElementDetailDB.executeQuery(tIncidentSQL);
			                         if(RdLLElementDetailSet.size()>=1 && tIncidentLLElementDetailSet.size()>=1)
			                         {
			                        	 String tAccBSQL = "select nvl(nvl((select double(ElementValue) from LLElementDetail where caseno='" + mCaseNo + "' "
				                        		+" and polno='"+aLLClaimDetailSchema.getPolNo()+"' and getdutycode='"+aLLClaimDetailSchema.getGetDutyCode()+"'"
				                				+" and elementcode='AllPrem'),0)-nvl((select double(ElementValue) from LLElementDetail where caseno='" + mCaseNo + "' "
				                        		+" and polno='"+aLLClaimDetailSchema.getPolNo()+"' and getdutycode='"+aLLClaimDetailSchema.getGetDutyCode()+"'"
				                				+" and elementcode='PartGetMoney'),0),0) from dual ";
			                			 ExeSQL pkBExeSQL =new ExeSQL();
			                			 SSRS tBSSRS = pkBExeSQL.execSQL(tAccBSQL);                			
			                			 aAccmoney = Arith.round(Double.parseDouble(tBSSRS.GetText(1, 1)), 2);          		                                
			                          }else{		                            	
			                            String tAccASQL = "select nvl(nvl((select double(ElementValue) from LLElementDetail where caseno='" + mCaseNo + "' "
			                            		+" and polno='"+aLLClaimDetailSchema.getPolNo()+"' and getdutycode='"+aLLClaimDetailSchema.getGetDutyCode()+"'"
			                    				+" and elementcode='AccValue'),0)-nvl((select double(ElementValue) from LLElementDetail where caseno='" + mCaseNo + "' "
			                            		+" and polno='"+aLLClaimDetailSchema.getPolNo()+"' and getdutycode='"+aLLClaimDetailSchema.getGetDutyCode()+"'"
			                    				+" and elementcode='GetMoney'),0),0) from dual ";
			                    		ExeSQL pkExeSQL =new ExeSQL();
			                    		SSRS tDateSSRS = pkExeSQL.execSQL(tAccASQL);
			                    		aAccmoney = Arith.round(Double.parseDouble(tDateSSRS.GetText(1, 1)), 2);			                        								     			
			                          }
				            	 }				            	
		                            			                	 				                    			 
				                 bLJSGetClaimSchema.setPay(Arith.round(apaymoney-aAccmoney, 2));
				                 cLJSGetClaimSchema =bLJSGetClaimSchema.getSchema();
				                 cLJSGetClaimSchema.setFeeFinaType("ZH");
				                 cLJSGetClaimSchema.setPay(Arith.round(aAccmoney, 2));
						         tSumRealPay += apaymoney;	 			            		 	
				             }
				         }
				         if(bLJSGetClaimSchema.getPay()>= 0)				         
				         mLJSGetClaimSet.add(bLJSGetClaimSchema);
				         if (cLJSGetClaimSchema.getPay() >= 0
								&& cLJSGetClaimSchema.getRiskCode() != null
								&& !"".equals(cLJSGetClaimSchema.getRiskCode())) {
				        	 System.out.println("账户部分财务数据换成主险的");
							 // 如果赔付的是附加险，账户部分财务数据换成主险的
							 LDCode1DB tLDCode1DB = new LDCode1DB();
							 tLDCode1DB.setCodeType("checkappendrisk");
							 tLDCode1DB
									.setCode(cLJSGetClaimSchema.getRiskCode());

							 LDCode1Set tLDCode1Set = tLDCode1DB.query();

							 if (tLDCode1Set.size() > 0) {
								 LCPolSchema tLCPolSchema = new LCPolSchema();
								 LCRiskDutyWrapDB tLCRiskDutyWrapDB = new LCRiskDutyWrapDB();
								 tLCRiskDutyWrapDB.setContNo(cLJSGetClaimSchema
										.getContNo());

								 LCPolDB tLCPolDB = new LCPolDB();
								 if (tLCRiskDutyWrapDB.query().size() > 0) {
									tLCPolDB.setContNo(cLJSGetClaimSchema
											.getContNo());
									tLCPolDB.setRiskCode(tLDCode1Set.get(1)
											.getCode1());
									LCPolSet tLCPolSet = tLCPolDB.query();
									if (tLCPolSet.size() != 1) {
										CError.buildErr(this, "附加险的主险查询失败！");
										return false;
									}
									tLCPolSchema = tLCPolSet.get(1);
								 }else {
									tLCPolDB.setPolNo(tLCPolBL.getMainPolNo());
									if (!tLCPolDB.getInfo()) {
										CError.buildErr(this, "附加险的主险查询失败！");
										return false;
									}
									tLCPolSchema = tLCPolDB.getSchema();
								 }
								 cLJSGetClaimSchema.setContNo(tLCPolSchema
										.getContNo());
								 cLJSGetClaimSchema.setPolNo(tLCPolSchema
										.getPolNo());
								 cLJSGetClaimSchema.setGrpContNo(tLCPolSchema
										.getGrpContNo());
								 cLJSGetClaimSchema.setGrpPolNo(tLCPolSchema
										.getGrpPolNo());
								 cLJSGetClaimSchema.setAgentCode(tLCPolSchema
										.getAgentCode());
								 cLJSGetClaimSchema.setAgentCom(tLCPolSchema
										.getAgentCom());
								 cLJSGetClaimSchema.setAgentGroup(tLCPolSchema
										.getAgentGroup());
								 cLJSGetClaimSchema.setAgentType(tLCPolSchema
										.getAgentType());
								 cLJSGetClaimSchema.setManageCom(tLCPolSchema
										.getManageCom());
								 cLJSGetClaimSchema.setRiskCode(tLCPolSchema
										.getRiskCode());
								 cLJSGetClaimSchema.setRiskVersion(tLCPolSchema
										.getRiskVersion());
							 }
							
							 if(cLJSGetClaimSchema.getPay()>tSumRealPay){
								  CError.buildErr(this, "实赔金额不能少于账户价值！");
								  return false;
							 }
							
							 //如果万能账户价值超过规定金额，账户部分财务数据换成规定金额
							 LDCode1DB tLDCode1DB2 = new LDCode1DB();
							 tLDCode1DB2.setCodeType("checkdutymoney");
							 tLDCode1DB2
									.setCode(WNssrs.GetText(i, 8));

							 LDCode1Set tLDCode1Set2 = tLDCode1DB2.query();
							 if(tLDCode1Set2.size() > 0){
								 if(cLJSGetClaimSchema.getPay() >= 5000000){
									 cLJSGetClaimSchema.setPay(tLDCode1Set2.get(1).getCode1());
									 System.out.println("万能账户限额为："+tLDCode1Set2.get(1).getCode1());
								 }
							 }
							 mLJSGetClaimSet.add(cLJSGetClaimSchema);
						}				        					
					}
				 }
		     }
	    	 
	     }
//	     dealFHLX();
	     //#2366分红险明细添加了相应的LJSgetclaim(包括分红保费,分红利息),把分红金额合计汇总加入mLJSGetSchema
//	     tSumRealPay= tSumRealPay +mHLtotal;	//2366关于理赔案件处理红利处理的系统功能
		 Reflections tref = new Reflections();
	     tref.transFields(mLJSGetSchema, mLJSGetClaimSet.get(1));
	     tSumRealPay = Arith.round(tSumRealPay, 2);
	     mLJSGetSchema.setSumGetMoney(tSumRealPay);
//	     System.out.println(tSumRealPay);
	     tmpMap.put(mLJSGetClaimSet, "INSERT");
	     tmpMap.put(mLJSGetSchema, "INSERT");	     
	     return true;
   }

    /**
     * 处理申诉/纠错的理算确认
     * @return boolean
     */
    private boolean dealAppeal() {
        //LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();
        System.out.println("deal appeal...~~~~~~~~~~><~~~~~~~~~~~");
        String AppealNo = mCaseNo;
//        LLAppealDB tLLAppealDB = new LLAppealDB();
//        tLLAppealDB.setAppealNo(AppealNo);
//        if(!tLLAppealDB.getInfo()){
//            CError.buildErr(this, "申诉案件查询失败！");
//            return false;
//        }

        String tAppealSQL = "SELECT caseno FROM llappeal where appealno='" +
                            AppealNo
                            + "' UNION "
                            + "SELECT b.appealno FROM llappeal a,llappeal b "
                            + "WHERE a.caseno=b.caseno AND a.appealno='"
                            + AppealNo + "' AND b.appealno<>'" + AppealNo + "' and not exists (select 1 from llcase where caseno=b.appealno and rgtstate='14')";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tAppealSSRS = tExeSQL.execSQL(tAppealSQL);
        if (tAppealSSRS.getMaxRow() <= 0) {
            CError.buildErr(this, "申诉案件查询失败！");
            return false;
        }

        double paydiff = mLLClaimSchema.getRealPay();

        for (int j = 1; j <= tAppealSSRS.getMaxRow(); j++) {
            String OldCaseNo = tAppealSSRS.GetText(j, 1);
            LLClaimDB tLLClaimDB = new LLClaimDB();
            LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
            LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();

            LLClaimSet OldClaimSet = new LLClaimSet();
            LLClaimPolicySet OldClaimPolicySet = new LLClaimPolicySet();
            LLClaimDetailSet OldClaimDetailSet = new LLClaimDetailSet();
            tLLClaimDB.setCaseNo(OldCaseNo);
            OldClaimSet = tLLClaimDB.query();
            if (OldClaimSet.size() <= 0) {
                CError.buildErr(this, "原错误案件的赔案查询失败！");
                return false;
            }
            tLLClaimPolicyDB.setCaseNo(OldCaseNo);
            OldClaimPolicySet = tLLClaimPolicyDB.query();
            tLLClaimDetailDB.setCaseNo(OldCaseNo);
            OldClaimDetailSet = tLLClaimDetailDB.query();
            for (int i = 1; i <= OldClaimPolicySet.size(); i++) {
                LLClaimPolicySchema aLLClaimPolicySchema = new
                        LLClaimPolicySchema();
                aLLClaimPolicySchema = OldClaimPolicySet.get(i);
//              if (aLLClaimPolicySchema.getRealPay() < 0)
//                  continue;
                double arealpay = ( -1) * aLLClaimPolicySchema.getRealPay();
                paydiff += arealpay;
                aLLClaimPolicySchema.setRealPay(arealpay);
                aLLClaimPolicySchema.setCaseRelaNo(aLLClaimPolicySchema.
                        getCaseRelaNo().substring(0, 14) + j + i);
                aLLClaimPolicySchema.setCaseNo(mCaseNo);
                aLLClaimPolicySchema.setClmNo(mClmNo);
                aLLClaimPolicySchema.setRgtNo(mRgtNo);
                aLLClaimPolicySchema.setGiveType("3");
                aLLClaimPolicySchema.setGiveTypeDesc("申诉拒付");
                mLLClaimPolicySet.add(aLLClaimPolicySchema);
            }
            paydiff = Arith.round(paydiff, 2);
            mLLClaimSchema.setRealPay(paydiff);
            double tAccMoney= 0;
            for (int i = 1; i <= OldClaimDetailSet.size(); i++) {
                LLClaimDetailSchema aLLClaimDetailSchema = new
                        LLClaimDetailSchema();
                aLLClaimDetailSchema = OldClaimDetailSet.get(i);
//              if (aLLClaimDetailSchema.getRealPay() < 0)
//                  continue;
                double arealpay = ( -1) * aLLClaimDetailSchema.getRealPay();
                double aClaimMoney = ( -1) * aLLClaimDetailSchema.getClaimMoney();
                double aOutDutyAmnt = ( -1) * aLLClaimDetailSchema.getOutDutyAmnt();
                aLLClaimDetailSchema.setRealPay(arealpay);
                aLLClaimDetailSchema.setClaimMoney(aClaimMoney);
                aLLClaimDetailSchema.setOutDutyAmnt(aOutDutyAmnt);
                aLLClaimDetailSchema.setCaseRelaNo(aLLClaimDetailSchema.getCaseRelaNo().substring(0, 14) + j + i);
                aLLClaimDetailSchema.setCaseNo(mCaseNo);
                aLLClaimDetailSchema.setClmNo(mClmNo);
                aLLClaimDetailSchema.setRgtNo(mRgtNo);
                aLLClaimDetailSchema.setGiveType("3");
                aLLClaimDetailSchema.setGiveTypeDesc("申诉拒付");
                mLLClaimDetailSet.add(aLLClaimDetailSchema);
                //根据给付责任判断账户
                LMDutyGetDB tLMDutyGetDB = new LMDutyGetDB();
                tLMDutyGetDB.setGetDutyCode(aLLClaimDetailSchema.getGetDutyCode());
                tLMDutyGetDB.getInfo();
                if ("0".equals(tLMDutyGetDB.getNeedAcc())) {
                    continue;
                }
                tAccMoney += aLLClaimDetailSchema.getRealPay();
            }

            LCInsureAccTraceSet tOldLCInsureAccTraceSet = new
                    LCInsureAccTraceSet();
            LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
            LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
            tLCInsureAccTraceDB.setOtherNo(OldCaseNo);
            tLCInsureAccTraceDB.setState("0");
            tOldLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
            double mAllMoney =0.0;
            for (int m = 1; m <= tOldLCInsureAccTraceSet.size(); m++) {
                LCInsureAccTraceSchema tLCInsureAccTraceSchema = new
                        LCInsureAccTraceSchema();
                tLCInsureAccTraceSchema = tOldLCInsureAccTraceSet.get(m);

                //万能险申诉纠错不修改账户
                String tRiskCode = tLCInsureAccTraceSchema.getRiskCode();
                
                if("162501".equals(tRiskCode)){						// #3337 管B申诉纠错
               	 double fCMoney=(-1)*tLCInsureAccTraceSchema.getMoney();
               	  mAllMoney += fCMoney;
               	  tLCInsureAccTraceSchema.setMoney(fCMoney);
//               	  tLCInsureAccTraceSchema.setMoney(tAccMoney);
                     tLCInsureAccTraceSchema.setOtherNo(mCaseNo);
                     tLCInsureAccTraceSchema.setState("temp");
                     String tSerialNo = PubFun1.CreateMaxNo("LLACCTRACENO",
                             "86" + mCaseNo.substring(1, 5));
                     tLCInsureAccTraceSchema.setSerialNo(tSerialNo);
                     tLCInsureAccTraceSchema.setModifyDate(PubFun.getCurrentDate());
                     tLCInsureAccTraceSchema.setModifyTime(PubFun.getCurrentTime());
                     tLCInsureAccTraceSchema.setOperator(mG.Operator);
                     tLCInsureAccTraceSchema.setMoneyNoTax(null);
                     tLCInsureAccTraceSchema.setMoneyTax(null);
                     tLCInsureAccTraceSchema.setBusiType(null);
                     tLCInsureAccTraceSchema.setTaxRate(null);   
                     tLCInsureAccTraceSet.add(tLCInsureAccTraceSchema);
               	  if(m==tOldLCInsureAccTraceSet.size()){
               		  if(mAllMoney!=tAccMoney){
               			  System.out.println("实赔金额与账户金额不匹配。。。。。");
               		  }
               	  }
               } else if (!LLCaseCommon.chenkWN(tRiskCode)) {
                	tAccMoney = ( -1) * tAccMoney;
                    tLCInsureAccTraceSchema.setMoney(tAccMoney);
                    tLCInsureAccTraceSchema.setOtherNo(mCaseNo);
                    tLCInsureAccTraceSchema.setState("temp");
                    String tSerialNo = PubFun1.CreateMaxNo("LLACCTRACENO",
                            "86" + mCaseNo.substring(1, 5));
                    tLCInsureAccTraceSchema.setSerialNo(tSerialNo);
                    tLCInsureAccTraceSchema.setModifyDate(PubFun.getCurrentDate());
                    tLCInsureAccTraceSchema.setModifyTime(PubFun.getCurrentTime());
                    tLCInsureAccTraceSchema.setOperator(mG.Operator);
                    //#3209营改增价税分离理赔万能账户相关逻辑调整**start**
                    tLCInsureAccTraceSchema.setMoneyNoTax(null);
                    tLCInsureAccTraceSchema.setMoneyTax(null);
                    tLCInsureAccTraceSchema.setBusiType(null);
                    tLCInsureAccTraceSchema.setTaxRate(null);   
                    //#3209营改增价税分离理赔万能账户相关逻辑调整**end**
                    tLCInsureAccTraceSet.add(tLCInsureAccTraceSchema);
                }
            }
            tmpMap.put(tLCInsureAccTraceSet, "INSERT");
        }
        return true;
    }

    private String getGiveDesc(String agivetype) {
        String agivedesc = "";
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("llclaimdecision");
        tLDCodeDB.setCode(agivetype);
        if (tLDCodeDB.getInfo()) {
            agivedesc = tLDCodeDB.getCodeName();
        }
        return agivedesc;
    }

    /**
     * 处理万能帐户（名字中西结合一下）
     * @return boolean
     */
    private boolean dealWNAcc() {
        //只处理申请类案件
        if ("1".equals(mLLCaseSchema.getRgtType())) {
            String tContNo = "";
            String tPolNo = "";
            String tRiskCode = "";
            String tGetDutyCode = "";

            //判断是否有万能需要处理账户的险种赔付且赔付大于0
            //这里认为所有万能附加险都会赔付账户价值
            for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
            	LLClaimDetailSchema accLLClaimDetailSchema =mLLClaimDetailSet.get(i);
                double tPayMoney = accLLClaimDetailSchema.getRealPay();
                tRiskCode = accLLClaimDetailSchema.getRiskCode();                         
                tContNo = accLLClaimDetailSchema.getContNo();
                tPolNo = accLLClaimDetailSchema.getPolNo();
                tGetDutyCode = accLLClaimDetailSchema.getGetDutyCode();
                //add lyc 2014-07-07 理赔反冲万能账户payplancode
                String tPCodeSQL = "select payplancode from lcinsureaccclass where contno='" + tContNo + "' "
        		+"fetch first 1 rows only";
                
                ExeSQL tPCodeExeSQL = new ExeSQL();
                String tPCode = tPCodeExeSQL.getOneValue(tPCodeSQL);                 
                //福泽一生重疾反冲
                String tFZSQL = "select ElementValue from LLElementDetail where caseno='" + mCaseNo + "' "
										        		+" and polno='"+tPolNo+"' and getdutycode='"+tGetDutyCode+"'"
										        		+" and elementcode='SeriousDisease'";
                
                ExeSQL tFZExeSQL = new ExeSQL();
                String tFZ = tFZExeSQL.getOneValue(tFZSQL); 
                
                //福泽180天之外重疾不赔付账户，跳过账户处理
                String tFZAccRDSQL = "select 1 from LLElementDetail where caseno='" + mCaseNo + "' "
													        		+" and polno='"+tPolNo+"' and getdutycode='"+tGetDutyCode+"'"
													        		+" and elementcode='RgtDays' and bigint(ElementValue)>180";
                
                ExeSQL tFZAccRDExeSQL = new ExeSQL();
                String tFZAccRD = tFZAccRDExeSQL.getOneValue(tFZAccRDSQL); 
//       		 #2668
       		 String SQL = "select 1 from lmriskapp where taxoptimal='Y' and  riskcode ='"+tRiskCode+"'";
       	        ExeSQL tExeSQL = new ExeSQL();
       	        String risktype = tExeSQL.getOneValue(SQL);
                
                if(tFZAccRD.equals("1") && tRiskCode.equals("231201"))
                {
                	System.out.println("福泽180天之外重疾不赔付账户，跳过账户处理");
                }
                else
                {
                if ((LLCaseCommon.chenkWN(tRiskCode) && tPayMoney > 0 && !risktype.equals("1")) || (tRiskCode.equals("231201") && tPayMoney == 0 && tFZ.equals("1") )) {                                      
                    //查询万能当前账户价值----------------begin--#2668 税优险种理赔不处理账户
                    LLElementDetailDB tLLElementDetailDB = new LLElementDetailDB();
                    String tAccSQL = "select * from LLElementDetail where caseno='" + mCaseNo + "' and polno='"+tPolNo+"'"
                    			+" and getdutycode='"+tGetDutyCode+"' and elementcode='AccValue'";
                    LLElementDetailSet tLLElementDetailSet = tLLElementDetailDB.executeQuery(tAccSQL);

                    double tInsuAccBala = 0;
                    if (tLLElementDetailSet.size() > 0 || tRiskCode.equals("231201")) { 
                    if(tLLElementDetailSet.size() > 0) {                   
                    	LLElementDetailSchema tLLElementDetailSchema = tLLElementDetailSet.get(1);
                        tInsuAccBala = Double.parseDouble(tLLElementDetailSchema.getElementValue());
                        }
                    		else if (tRiskCode.equals("231201"))
                    		{
                    		System.out.println("福泽一生重疾反冲");
                    		}

                        if (tInsuAccBala > 0 || tRiskCode.equals("231201")) {
                        	//校验账户信息是否存在
                            LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
                            String tSQL ="select * from lcinsureacc where contno='"+ tContNo + "' ";
                            LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.executeQuery(tSQL);
                            if (tLCInsureAccSet.size() <= 0||tLCInsureAccSet.get(1).getInsuAccBala()<0) {
                            	 CError.buildErr(this, "账户余额为0或账户信息查询失败！");
                                 return false;
                            }
                            //取一条保费的轨迹
                            LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
                            String tAccTSQL ="select * from lcinsureacctrace where contno='"
                                    + tContNo + "' and moneytype='BF' and othertype='1'";
                            LCInsureAccTraceSet tLCInsureAccTraceSet =tLCInsureAccTraceDB.executeQuery(tAccTSQL);

                            if (tLCInsureAccTraceSet.size() <= 0) {
                            	CError.buildErr(this, "账户轨迹初始保费查询失败！");
                                return false;
                            }
                            //针对万能多保单
                            for(int tTr = 1; tTr <=tLCInsureAccTraceSet.size() ; tTr++)
                            {
                            	LCInsureAccTraceSchema tLCInsureAccTraceSchema =new LCInsureAccTraceSchema(); 
                                tLCInsureAccTraceSchema = tLCInsureAccTraceSet.get(tTr); 
                                String tTrPolNo = tLCInsureAccTraceSchema.getPolNo();
                                this.mPolNo=tTrPolNo;
                                String tTrInsuAccNo = tLCInsureAccTraceSchema.getInsuAccNo();
                                String tRgtDays = "";
                                
                                //180内赔保费的话，全部赔款算账户部分，并且反冲之前投保、月结产生的利息、账户管理费、风险保费
                                LLElementDetailDB tRgtDateLLElementDetailDB = new LLElementDetailDB();
                            	String tAccRDSQL = "select * from LLElementDetail where caseno='" + mCaseNo + "' "
                            		+" and polno='"+tPolNo+"' and getdutycode='"+tGetDutyCode+"'"
                            		+" and elementcode='RgtDays' and bigint(ElementValue)<=180";
                                LLElementDetailSet RdLLElementDetailSet = tRgtDateLLElementDetailDB.executeQuery(tAccRDSQL);
                                
                                String tIncidentSQL = "select * from LLElementDetail where caseno='" + mCaseNo + "' "
	                        		+" and polno='"+tPolNo+"' and getdutycode='"+tGetDutyCode+"'"
	                        		+" and elementcode='Incident' and ElementValue<>'1'";
                                LLElementDetailSet tIncidentLLElementDetailSet = tRgtDateLLElementDetailDB.executeQuery(tIncidentSQL);
                            
                                if(RdLLElementDetailSet.size()>=1 && tIncidentLLElementDetailSet.size() >= 1)
                                {        
                                	String tAccBSQL = "select sum(money) from lcinsureacctrace where polno='"+ tTrPolNo +"' "
                                		+" and insuaccno='"+ tTrInsuAccNo +"' and moneytype in ('BF','LQ','ZF')"
                                		+" and state='0'";
                    				ExeSQL pkBExeSQL =new ExeSQL();
                    				SSRS tBSSRS = pkBExeSQL.execSQL(tAccBSQL);                			
    	                            tInsuAccBala = Arith.round(Double.parseDouble(tBSSRS.GetText(1, 1)), 2);
    	                            if(tInsuAccBala <= 0)
    	                            {
    	                            	CError.buildErr(this, "账户轨迹初始保费查询失败！");
    	                                return false;
    	                            }
    	                            System.out.println("180内轨迹PK值等于赔付保费+领取+追加保费=="+tInsuAccBala);
    	                            
    	                            //反冲轨迹非'BF','LQ','ZF'所有数据
    	                            //如果福泽一生附加重疾，只反冲附加险的风险保费即可
    	                            if(tRiskCode.equals("231201") && tPayMoney == 0 && tFZ.equals("1"))
    	                            {
    	                            	LCInsureAccTraceDB fcLCInsureAccTraceDB =new LCInsureAccTraceDB();
        	                            String tAccfcSQL = "select * from lcinsureacctrace where polno='"+ tTrPolNo +"' "
    	                            		+" and insuaccno='"+ tTrInsuAccNo +"' and moneytype = 'RP'"
    	                            		+" and state='0' and money<>0 and payplancode = '231201'";
        	                            System.out.println("180内福泽一生附加重疾反冲轨迹LCInsureAccTrace==tAccfcSQL"+tAccfcSQL);
        	                            LCInsureAccTraceSet fcLCInsureAccTraceSet =  fcLCInsureAccTraceDB.executeQuery(tAccfcSQL);
        	                            if(fcLCInsureAccTraceSet.size()>= 1)
        	                            {    	                                	                           
    	    	                            for(int fc =1 ; fc <=fcLCInsureAccTraceSet.size(); fc ++ )
    	    	                            {
    	    	                            	LCInsureAccTraceSchema fcLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
    	    	                            	fcLCInsureAccTraceSchema = fcLCInsureAccTraceSet.get(fc);
    	                                		String aSerialNo = PubFun1.CreateMaxNo("SERIALNO",mLLCaseSchema.getMngCom());                            	
    	                                		fcLCInsureAccTraceSchema.setSerialNo(aSerialNo);
    	                                		fcLCInsureAccTraceSchema.setMoney(-fcLCInsureAccTraceSchema.getMoney());
    	                                		fcLCInsureAccTraceSchema.setOtherType("5");
    	                                		fcLCInsureAccTraceSchema.setOtherNo(mCaseNo);
    	                                		fcLCInsureAccTraceSchema.setState("temp");                                     		
    	                                		fcLCInsureAccTraceSchema.setOperator(mG.Operator);
    	                                		fcLCInsureAccTraceSchema.setMakeDate(aDate);
    	                                		fcLCInsureAccTraceSchema.setMakeTime(aTime);
    	                                		fcLCInsureAccTraceSchema.setModifyDate(aDate);
    	                                		fcLCInsureAccTraceSchema.setModifyTime(aTime);
                                                //#3209营改增价税分离理赔万能账户相关逻辑调整**start**
    	                                		fcLCInsureAccTraceSchema.setMoneyNoTax(null);
    	                                		fcLCInsureAccTraceSchema.setMoneyTax(null);
    	                                		fcLCInsureAccTraceSchema.setBusiType(null);
    	                                		fcLCInsureAccTraceSchema.setTaxRate(null);   
                                                //#3209营改增价税分离理赔万能账户相关逻辑调整**end**
    	                                        tmpMap.put(fcLCInsureAccTraceSchema, "INSERT");
    	    	                            }
        	                            }
        	                            LCInsureAccFeeTraceDB fcLCInsureAccFeeTraceDB =new LCInsureAccFeeTraceDB();
        	                            String tFeefcSQL = "select * from LCInsureAccFeeTrace where polno='"+ tTrPolNo +"' "
    	                            		+" and insuaccno='"+ tTrInsuAccNo +"' "
    	                            		+" and state='0' and fee<>0 and payplancode = '231201'";
        	                            System.out.println("180内反冲轨迹LCInsureAccFeeTrace==tFeefcSQL"+tFeefcSQL);
        	                            LCInsureAccFeeTraceSet fcLCInsureAccFeeTraceSet =  fcLCInsureAccFeeTraceDB.executeQuery(tFeefcSQL);
        	                            if(fcLCInsureAccFeeTraceSet.size()<=0)
        	                            {
        	                            	CError.buildErr(this, "保单账户管理费查询失败！");
        	                                return false;
        	                            }
        	                            for(int feefc =1 ; feefc <=fcLCInsureAccFeeTraceSet.size(); feefc ++)
        	                            {
        	                            	LCInsureAccFeeTraceSchema fcLCInsureAccFeeTraceSchema = new LCInsureAccFeeTraceSchema();
        	                            	fcLCInsureAccFeeTraceSchema = fcLCInsureAccFeeTraceSet.get(feefc);
                                    		String aSerialNo = PubFun1.CreateMaxNo("SERIALNO",mLLCaseSchema.getMngCom());                            	
                                    		fcLCInsureAccFeeTraceSchema.setSerialNo(aSerialNo);
                                    		fcLCInsureAccFeeTraceSchema.setFee(-fcLCInsureAccFeeTraceSchema.getFee());
                                    		fcLCInsureAccFeeTraceSchema.setOtherType("5");
                                    		fcLCInsureAccFeeTraceSchema.setOtherNo(mCaseNo);
                                    		fcLCInsureAccFeeTraceSchema.setState("temp");                                     		
                                    		fcLCInsureAccFeeTraceSchema.setOperator(mG.Operator);
                                    		fcLCInsureAccFeeTraceSchema.setMakeDate(aDate);
                                    		fcLCInsureAccFeeTraceSchema.setMakeTime(aTime);
                                    		fcLCInsureAccFeeTraceSchema.setModifyDate(aDate);
                                    		fcLCInsureAccFeeTraceSchema.setModifyTime(aTime);
                                            //#3209营改增价税分离理赔万能账户相关逻辑调整**start**
                                    		fcLCInsureAccFeeTraceSchema.setMoneyNoTax(null);
                                    		fcLCInsureAccFeeTraceSchema.setMoneyTax(null);
                                    		fcLCInsureAccFeeTraceSchema.setBusiType(null);
                                    		fcLCInsureAccFeeTraceSchema.setTaxRate(null);   
                                            //#3209营改增价税分离理赔万能账户相关逻辑调整**end**
                                            tmpMap.put(fcLCInsureAccFeeTraceSchema, "INSERT");
        	                            }
    	                            }
    	                            else {
    	                            LCInsureAccTraceDB fcLCInsureAccTraceDB =new LCInsureAccTraceDB();
    	                            String tAccfcSQL = "select * from lcinsureacctrace where polno='"+ tTrPolNo +"' "
	                            		+" and insuaccno='"+ tTrInsuAccNo +"' and moneytype not in ('BF','LQ','ZF')"
	                            		+" and state='0' and money<>0";
    	                            System.out.println("180内反冲轨迹LCInsureAccTrace==tAccfcSQL"+tAccfcSQL);
    	                            LCInsureAccTraceSet fcLCInsureAccTraceSet =  fcLCInsureAccTraceDB.executeQuery(tAccfcSQL);
    	                            if(fcLCInsureAccTraceSet.size()>= 1)
    	                            {    	                                	                           
	    	                            for(int fc =1 ; fc <=fcLCInsureAccTraceSet.size(); fc ++ )
	    	                            {
	    	                            	LCInsureAccTraceSchema fcLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
	    	                            	fcLCInsureAccTraceSchema = fcLCInsureAccTraceSet.get(fc);
	                                		String aSerialNo = PubFun1.CreateMaxNo("SERIALNO",mLLCaseSchema.getMngCom());                            	
	                                		fcLCInsureAccTraceSchema.setSerialNo(aSerialNo);
	                                		fcLCInsureAccTraceSchema.setMoney(-fcLCInsureAccTraceSchema.getMoney());
	                                		fcLCInsureAccTraceSchema.setOtherType("5");
	                                		fcLCInsureAccTraceSchema.setOtherNo(mCaseNo);
	                                		fcLCInsureAccTraceSchema.setState("temp");                                     		
	                                		fcLCInsureAccTraceSchema.setOperator(mG.Operator);
	                                		fcLCInsureAccTraceSchema.setMakeDate(aDate);
	                                		fcLCInsureAccTraceSchema.setMakeTime(aTime);
	                                		fcLCInsureAccTraceSchema.setModifyDate(aDate);
	                                		fcLCInsureAccTraceSchema.setModifyTime(aTime);
                                            //#3209营改增价税分离理赔万能账户相关逻辑调整**start**
	                                		fcLCInsureAccTraceSchema.setMoneyNoTax(null);
	                                		fcLCInsureAccTraceSchema.setMoneyTax(null);
                                    		fcLCInsureAccTraceSchema.setBusiType(null);
                                    		fcLCInsureAccTraceSchema.setTaxRate(null);   
                                            //#3209营改增价税分离理赔万能账户相关逻辑调整**end**
	                                        tmpMap.put(fcLCInsureAccTraceSchema, "INSERT");
	    	                            }
    	                            }
    	                            LCInsureAccFeeTraceDB fcLCInsureAccFeeTraceDB =new LCInsureAccFeeTraceDB();
    	                            String tFeefcSQL = "select * from LCInsureAccFeeTrace where polno='"+ tTrPolNo +"' "
	                            		+" and insuaccno='"+ tTrInsuAccNo +"' "
	                            		+" and state='0' and fee<>0";
    	                            System.out.println("180内反冲轨迹LCInsureAccFeeTrace==tFeefcSQL"+tFeefcSQL);
    	                            LCInsureAccFeeTraceSet fcLCInsureAccFeeTraceSet =  fcLCInsureAccFeeTraceDB.executeQuery(tFeefcSQL);
    	                            if(fcLCInsureAccFeeTraceSet.size()<=0)
    	                            {
    	                            	CError.buildErr(this, "保单账户管理费查询失败！");
    	                                return false;
    	                            }
    	                            for(int feefc =1 ; feefc <=fcLCInsureAccFeeTraceSet.size(); feefc ++)
    	                            {
    	                            	LCInsureAccFeeTraceSchema fcLCInsureAccFeeTraceSchema = new LCInsureAccFeeTraceSchema();
    	                            	fcLCInsureAccFeeTraceSchema = fcLCInsureAccFeeTraceSet.get(feefc);
                                		String aSerialNo = PubFun1.CreateMaxNo("SERIALNO",mLLCaseSchema.getMngCom());                            	
                                		fcLCInsureAccFeeTraceSchema.setSerialNo(aSerialNo);
                                		fcLCInsureAccFeeTraceSchema.setFee(-fcLCInsureAccFeeTraceSchema.getFee());
                                		fcLCInsureAccFeeTraceSchema.setOtherType("5");
                                		fcLCInsureAccFeeTraceSchema.setOtherNo(mCaseNo);
                                		fcLCInsureAccFeeTraceSchema.setState("temp");                                     		
                                		fcLCInsureAccFeeTraceSchema.setOperator(mG.Operator);
                                		fcLCInsureAccFeeTraceSchema.setMakeDate(aDate);
                                		fcLCInsureAccFeeTraceSchema.setMakeTime(aTime);
                                		fcLCInsureAccFeeTraceSchema.setModifyDate(aDate);
                                		fcLCInsureAccFeeTraceSchema.setModifyTime(aTime);
                                        //#3209营改增价税分离理赔万能账户相关逻辑调整**start**
                                		fcLCInsureAccFeeTraceSchema.setMoneyNoTax(null);
                                		fcLCInsureAccFeeTraceSchema.setMoneyTax(null);
                                		fcLCInsureAccFeeTraceSchema.setBusiType(null);
                                		fcLCInsureAccFeeTraceSchema.setTaxRate(null);   
                                        //#3209营改增价税分离理赔万能账户相关逻辑调整**end**
                                        tmpMap.put(fcLCInsureAccFeeTraceSchema, "INSERT");
    	                            }
                                 }   
                                }else{
                                	String tAccASQL = "select nvl(nvl((select double(ElementValue) from LLElementDetail where caseno='" + mCaseNo + "' "
                                		+" and polno='"+tPolNo+"' and getdutycode='"+tGetDutyCode+"'"
                        				+" and elementcode='AccValue'),0)-nvl((select double(ElementValue) from LLElementDetail where caseno='" + mCaseNo + "' "
                                		+" and polno='"+tPolNo+"' and getdutycode='"+tGetDutyCode+"'"
                        				+" and elementcode='GetMoney'),0),0) from dual ";
                        				ExeSQL pkExeSQL =new ExeSQL();
                        				SSRS tDateSSRS = pkExeSQL.execSQL(tAccASQL);
    		                            tInsuAccBala = Arith.round(Double.parseDouble(tDateSSRS.GetText(1, 1)), 2);
    		                            System.out.println("180外出险账户轨迹账户值=="+tInsuAccBala);
    		                            if(tInsuAccBala <= 0)
    		                            {
    		                            	CError.buildErr(this, "账户计算账户价值失败失败，请重新计算！");
        	                                return false;
    		                            }
    		                            String tAccDSQL = "select * from LLElementDetail where caseno='" + mCaseNo + "' "
    		                            	+" and polno='"+tPolNo+"' and elementcode='AccTraceDate'";
    	                                LLElementDetailSet dLLElementDetailSet = tLLElementDetailDB.executeQuery(tAccDSQL);  
    	                                if(dLLElementDetailSet.size()>=1)
    	                                tRgtDays = dLLElementDetailSet.get(1).getElementValue();
    	                                else {
    	                                	 CError.buildErr(this, "计算账户价值日期要素生成失败！");
    	                                     return false;
    	                                }     
    	                                
    	                                //180外反冲出险日之后的信息LCInsureAccTrace 
    	                                LCInsureAccTraceDB aLCInsureAccTraceDB = new LCInsureAccTraceDB(); 
    	                                LCInsureAccTraceSet aLCInsureAccTraceSet = new LCInsureAccTraceSet();                              
    	                            	 String traceSql="select * from lcinsureacctrace where polno='"+tTrPolNo+"'"
    	                         				+" and paydate>='"+tRgtDays+"' and money<>0 and state='0'";    	                         			
    	    	                         aLCInsureAccTraceSet = aLCInsureAccTraceDB.executeQuery(traceSql);	                       
    	    	                         System.out.println("180外出险反冲lcinsureacctrace的"+traceSql);
    	    	                        if(aLCInsureAccTraceSet.size()>=1)
    	                                {	 
    	                                	for (int m = 1; m <= aLCInsureAccTraceSet.size(); m++)
    	                                	{
    	                                		LCInsureAccTraceSchema aLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
    	                                		aLCInsureAccTraceSchema = aLCInsureAccTraceSet.get(m);
    	                                		String aSerialNo = PubFun1.CreateMaxNo("SERIALNO",mLLCaseSchema.getMngCom());                            	
    	                                    	aLCInsureAccTraceSchema.setSerialNo(aSerialNo);
    	                                        aLCInsureAccTraceSchema.setMoney(-aLCInsureAccTraceSchema.getMoney());
    	                                        aLCInsureAccTraceSchema.setOtherType("5");
    	                                        aLCInsureAccTraceSchema.setOtherNo(mCaseNo);
    	                                        aLCInsureAccTraceSchema.setState("temp");                                    
    	                                        aLCInsureAccTraceSchema.setOperator(mG.Operator);
    	                                        aLCInsureAccTraceSchema.setMakeDate(aDate);
    	                                        aLCInsureAccTraceSchema.setMakeTime(aTime);
    	                                        aLCInsureAccTraceSchema.setModifyDate(aDate);
    	                                        aLCInsureAccTraceSchema.setModifyTime(aTime);
    	                                        //#3209营改增价税分离理赔万能账户相关逻辑调整**start**
    	                                        aLCInsureAccTraceSchema.setMoneyNoTax(null);
    	                                        aLCInsureAccTraceSchema.setMoneyTax(null);
    	                                        aLCInsureAccTraceSchema.setBusiType(null);
    	                                        aLCInsureAccTraceSchema.setTaxRate(null);   
    	                                        //#3209营改增价税分离理赔万能账户相关逻辑调整**end**
    	                                        tmpMap.put(aLCInsureAccTraceSchema, "INSERT");                                                                      
    	                                	}                            	                            
    	                                } 
    	    	                        //反冲账户价值日之后的结算信息LCInsureAccFeeTrace
    	    	                        LCInsureAccFeeTraceDB  afeeLCInsureAccFeeTraceDB  = new LCInsureAccFeeTraceDB(); 
    	                                LCInsureAccFeeTraceSet afeeLCInsureAccFeeTraceSet = new LCInsureAccFeeTraceSet();                              
    	                            	String feetraceSql="select * from lcinsureaccfeetrace where polno='"+tTrPolNo+"'"
    	                         			+" and paydate>= '"+tRgtDays+"'  and fee<>0 and state='0'";    	                         			
    	                            	afeeLCInsureAccFeeTraceSet = afeeLCInsureAccFeeTraceDB.executeQuery(feetraceSql);	                       
    	    	                        System.out.println("180外出险反冲lcinsureaccfeetrace的"+feetraceSql);
    	    	                        if(afeeLCInsureAccFeeTraceSet.size()>=1)
    	                                {	 
    	                                	for (int m = 1; m <= afeeLCInsureAccFeeTraceSet.size(); m++)
    	                                	{
    	                                		LCInsureAccFeeTraceSchema aLCInsureAccFeeTraceSchema = new LCInsureAccFeeTraceSchema();
    	                                		aLCInsureAccFeeTraceSchema = afeeLCInsureAccFeeTraceSet.get(m);    	                            		        	                                        
    	                                        String aFeetraceSerialNo = PubFun1.CreateMaxNo("SERIALNO",mLLCaseSchema.getMngCom());    	                                        
    	                                        aLCInsureAccFeeTraceSchema.setSerialNo(aFeetraceSerialNo);  
    	                                        aLCInsureAccFeeTraceSchema.setFee(-aLCInsureAccFeeTraceSchema.getFee()); 
    	                                        aLCInsureAccFeeTraceSchema.setOtherType("5");
    	                                        aLCInsureAccFeeTraceSchema.setState("temp");
    	                                        aLCInsureAccFeeTraceSchema.setOtherNo(mCaseNo);                                    
    	                                        aLCInsureAccFeeTraceSchema.setOperator(mG.Operator);
    	                                        aLCInsureAccFeeTraceSchema.setMakeDate(aDate);
    	                                        aLCInsureAccFeeTraceSchema.setMakeTime(aTime);
    	                                        aLCInsureAccFeeTraceSchema.setModifyDate(aDate);
    	                                        aLCInsureAccFeeTraceSchema.setModifyTime(aTime);
    	                                        //#3209营改增价税分离理赔万能账户相关逻辑调整**start**
    	                                        aLCInsureAccFeeTraceSchema.setMoneyNoTax(null);
    	                                        aLCInsureAccFeeTraceSchema.setMoneyTax(null);
    	                                        aLCInsureAccFeeTraceSchema.setBusiType(null);
    	                                        aLCInsureAccFeeTraceSchema.setTaxRate(null);   
    	                                        //#3209营改增价税分离理赔万能账户相关逻辑调整**end**
    	                                        tmpMap.put(aLCInsureAccFeeTraceSchema, "INSERT");
    	                                	}
    	                                }	  
                                }  
                                //福泽附加险180内出险，不清空账户
                                String tAccFZSQL = "select ElementValue from LLElementDetail where caseno='" + mCaseNo + "' "
											                        		+" and polno='"+tPolNo+"' and getdutycode='"+tGetDutyCode+"'"
											                        		+" and elementcode='RgtDays' ";
                                ExeSQL tAccFZExeSQL = new ExeSQL();
                                String tFZRgtDays = tAccFZExeSQL.getOneValue(tAccFZSQL); 
                                int iFZRgtDays = 365;
                                if(!tFZRgtDays.equals(null)&&!tFZRgtDays.equals("null")&&!tFZRgtDays.equals("") ){
                                iFZRgtDays = Integer.parseInt(tFZRgtDays);
                                }
                                if(tRiskCode.equals("231201") && tPayMoney == 0 && tFZ.equals("1") && iFZRgtDays < 180)
                                {
                                	System.out.println("福泽附加险180内出险，不清空账户");
                                }
                                else {                                
                                String tSerialNo = PubFun1.CreateMaxNo("SERIALNO",mLLCaseSchema.getMngCom());
                                //tLCInsureAccTraceSchema.setRiskCode(tRiskCode); 万能附件险赔款账户存主险                            
                                tLCInsureAccTraceSchema.setPayPlanCode(tPCode);   //把原来的给付责任编码修改为保全的payplancode 2014-07-07   add lyc
                                tLCInsureAccTraceSchema.setOtherNo(mCaseNo);
                                tLCInsureAccTraceSchema.setOtherType("5");
                                tLCInsureAccTraceSchema.setSerialNo(tSerialNo);
                                tLCInsureAccTraceSchema.setMoneyType("PK");
                                tLCInsureAccTraceSchema.setState("temp");
                                tLCInsureAccTraceSchema.setMoney(Arith.round( - tInsuAccBala, 2));
                                tLCInsureAccTraceSchema.setPayDate(aDate); 
                                
                                tLCInsureAccTraceSchema.setOperator(mG.Operator);
                                tLCInsureAccTraceSchema.setMakeDate(aDate);
                                tLCInsureAccTraceSchema.setMakeTime(aTime);
                                tLCInsureAccTraceSchema.setModifyDate(aDate);
                                tLCInsureAccTraceSchema.setModifyTime(aTime);
                                //#3209营改增价税分离理赔万能账户相关逻辑调整**start**
                                tLCInsureAccTraceSchema.setMoneyNoTax(null);
                                tLCInsureAccTraceSchema.setMoneyTax(null);
                                tLCInsureAccTraceSchema.setBusiType(null);
                                tLCInsureAccTraceSchema.setTaxRate(null);   
                                //#3209营改增价税分离理赔万能账户相关逻辑调整**end**
                                tmpMap.put(tLCInsureAccTraceSchema, "INSERT"); 
                                                                                              
                                backMsg += "<br>万能险种个人账户清零。<br>";
                              }
                            }
                            
                        } else {
                            CError.buildErr(this, "理算时计算账户价值金额为0，无法赔付！");
                            return false;
                        }
                    } else {
                        CError.buildErr(this, "理算万能险生成"+tPolNo+"账户价值失败，请重新计算！");
                        return false;
                    }
                    //万能#80 cbs00049665合同自动终止失败，由于合同自动终止查询了LCInsureAccBalance表，对于小于180天的情况，之前没存，现在在计算时加上了，考虑到拒付金额为0时的情况，本类dealData中先删除，此处对于金额非0的，需要重新加上
                    if(!"".equals(this.mPolNo)) {
                    	String strbalanceSQL1 = "delete from LCInsureAccBalance where polno='" +mPolNo +"' and InsuAccNo='000000'";
                        tmpMap.put(strbalanceSQL1, "DELETE");
                        LCInsureAccBalanceDB tLCInsureAccBalanceDB =new LCInsureAccBalanceDB();                         
                        tLCInsureAccBalanceDB.setPolNo(this.mPolNo);
                        tLCInsureAccBalanceDB.setInsuAccNo("000000");
                        LCInsureAccBalanceSet tLCInsureAccBalanceSet = tLCInsureAccBalanceDB.query();
                        if(tLCInsureAccBalanceSet.size()>=1)
                        {   
                        	for(int bal= 1; bal<=tLCInsureAccBalanceSet.size(); bal++)
                        	{
                        		LCInsureAccBalanceSchema tLCInsureAccBalanceSchema = tLCInsureAccBalanceSet.get(bal);
	                         	tLCInsureAccBalanceSchema.setInsuAccNo("000000");
	                         	tLCInsureAccBalanceSchema.setDueBalaDate(PubFun.getCurrentDate());
	                         	tLCInsureAccBalanceSchema.setRunDate(PubFun.getCurrentDate());
	                         	tLCInsureAccBalanceSchema.setOperator(mG.Operator);
	                         	tLCInsureAccBalanceSchema.setModifyDate(PubFun.getCurrentDate());
	                         	tLCInsureAccBalanceSchema.setModifyTime(PubFun.getCurrentTime());
	                         	tmpMap.put(tLCInsureAccBalanceSchema, "INSERT");
                        	}
                        }
                    }
                } else if (LLCaseCommon.chenkWN(tRiskCode)) {
					String strSQLTrace = "delete from LCInsureAccTrace where Otherno='"
							+ mCaseNo
							+ "' and state='temp' and contno='"
							+ tContNo + "' ";
					tmpMap.put(strSQLTrace, "DELETE");
					String strSQLFeeTrace = "delete from LCInsureAccfeeTrace where Otherno='"
							+ mCaseNo
							+ "' and state='temp' and contno='"
							+ tContNo + "' ";
					tmpMap.put(strSQLFeeTrace, "DELETE");
				}
			}
            }
        }
        return true;
    }  
    
    // # 1500康乐人生个人重大疾病保险重大疾病且属于特定疾病的给予保费免赔
    /**
     * 保费豁免
     */
    private boolean dealExemption(){
    	// #3310  大调整   系统校验是否存在已豁免保单，当存在时不再进行豁免操作。
    	   String  zCaseNo =null; 
    		if("S".equals(mCaseNo.substring(0,1)) || "R".equals(mCaseNo.substring(0,1))){
    			String cSQL="select caseno from llappeal where appealno='"+mCaseNo+"' with ur";
    			ExeSQL cExe=new ExeSQL();
    			zCaseNo=cExe.getOneValue(cSQL);
    		  }else{
    			zCaseNo=mCaseNo;
    		}
    		// location :ZE 自动豁免标志     RE 人工豁免标志  (此时豁免不可进行逆删除，以后再定。。。)
    	    String eSQL="select 1 from llexemption where caseno='"+zCaseNo+"' and state='0' "
    	    		+ "and location='ZE' with ur"; 
    	    ExeSQL exeSQL = new ExeSQL();
    	    SSRS eSRS =exeSQL.execSQL(eSQL);
    	    if(eSRS.getMaxRow()>0){
    	    	return true;
    	    }else{
    	    	//豁免时保单状态处于temp，重复进行数据删除添加
    	    	System.out.println("康乐开始查询自动豁免数据。。。一千万次的改动。。。");
    	    	String dSQL = "select * from llexemption  where caseno='"+ zCaseNo+"'and state='temp' "
    	    			+ "and location='ZE' with ur";
    	    	SSRS dSRS =exeSQL.execSQL(dSQL);
    	    	if(dSRS.getMaxRow()>0){
    	    		String delsql3 = "delete from llexemption where caseno ='" +
							zCaseNo +"' and location='ZE' and state='temp' with ur";
		             tmpMap.put(delsql3, "DELETE");
		        System.out.println("康乐豁免只删除自动豁免的情况===============");
    	    	}
    	    	// 千呼万唤使出来，开始进行险种自动豁免
    	    	for(int k=1;k<= mLLClaimDetailSet.size(); k++){
    	    		LLClaimDetailSchema dLLClaimDetail =mLLClaimDetailSet.get(k);
    	    		String dRiskCode = dLLClaimDetail.getRiskCode();
    	    		String dGetDutyCode= dLLClaimDetail.getGetDutyCode();
    	    		String dContNo =dLLClaimDetail.getContNo(); // 要死的保单
    	    		if("231701".equals(dRiskCode) && dRiskCode != null && "350201".equals(dGetDutyCode) && dGetDutyCode !=null ){
    	    	        ExeSQL eExeSQL = new ExeSQL();
    	        		//约定特定疾病（重大疾病内）
    	    			String SpecialDiseaseSQL="select code from ldcode1 where codetype ='SpecialDisease' " +
    	    	    			// othersign:险种号（RiskCode）
    	    	    					"and  othersign='"+dRiskCode+"' and code in " 
    	    	    					+"(select DiseaseCode from LLCaseCure where caseno='"+mCaseNo+"')";
    	    			SSRS sSpectialDisease=eExeSQL.execSQL(SpecialDiseaseSQL);
    	    			if(sSpectialDisease == null || sSpectialDisease.getMaxRow()<=0){
    	    				System.out.println("没有豁免保费");
    	    				return true;
    	    			
    	    			}else if(sSpectialDisease.getMaxRow()>=1){
    	    		            System.out.println("确认豁免保费");

    	    		            String uSQL="select d.contno,d.riskcode,e.riskname,"
    	    		            	+ "(select FreeStartDate from llexemption where polno=d.polno and caseno='"+zCaseNo+"'fetch first 1 rows only),"
    	    		            	+ "d.payenddate,(select FreePried from llexemption where polno=d.polno and caseno='"+zCaseNo+"'fetch first 1 rows only),"
    	    		            	+ "f.codename,d.polno,d.contno,d.payintv, "+
    	  		    				  "(select remark from llexemption where caseno = '"+zCaseNo+"'fetch first 1 rows only), "
    	  		    				+ "(select freeEndDate from llexemption where caseno = '"+zCaseNo+"'fetch first 1 rows only), "
    	  		    				+ "d.insuredno,d.insuredname,"
    	  		    				+ "(select case when state='1' then '申诉纠错' when state='0' then '已豁免' when state='2' then '失效' when state='temp' then '待审批' else '' end from llexemption where polno=d.polno and caseno='"+zCaseNo+"')" +
    	  							" from lcpol d,lmrisk e,ldcode f " + 
    	  					"where d.riskcode = e.riskcode and d.payintv = f.code and f.codetype = 'payintv' " + 
    	  						// #3081康乐人生重疾A款保单保费豁免功能调整** start**
    	  						//原逻辑 d.contno in( select ContNo from llclaimdetail where caseno='"+dCaseNo+"' )
    	  					"and d.polno in( select t.polno from lcpol t,llcase a where t.insuredno =a.customerno and a.caseno='"+zCaseNo+"' )" +
    	  						// #3081康乐人生重疾A款保单保费豁免功能调整**end** 
    	  					" and contno='"+dContNo+"' and d.riskcode in('231701','333801')";
    	    		           
    	    		            ExeSQL uUSQL = new ExeSQL();
    	    		            SSRS uSSRS = uUSQL.execSQL(uSQL);  
    	    		            
    	    		            if(uSSRS.getMaxRow()>1){
    	    		            LLExemptionSet dLLExemptionSet = new LLExemptionSet(); 
    	    		             for (int w = 1; w <= uSSRS.getMaxRow(); w++) {
    			                	LLExemptionSchema tLLExemptionSchema= new LLExemptionSchema();
    			                	tLLExemptionSchema.setContNo(uSSRS.GetText(w,1));
    			                	tLLExemptionSchema.setRiskCode(uSSRS.GetText(w, 2));
    			                	tLLExemptionSchema.setFreeStartDate(uSSRS.GetText(w, 4));
    			                	tLLExemptionSchema.setPayEndDate(uSSRS.GetText(w, 5));
    			                	tLLExemptionSchema.setFreePried(uSSRS.GetText(w, 6));
    			                	tLLExemptionSchema.setPolNo(uSSRS.GetText(w, 8)); 
    			                	tLLExemptionSchema.setPayIntv(uSSRS.GetText(w, 10));
    			                	tLLExemptionSchema.setRemark(uSSRS.GetText(w, 11));
    			                	tLLExemptionSchema.setFreeEndDate(uSSRS.GetText(w, 5));
    			                	tLLExemptionSchema.setInsuredNo(uSSRS.GetText(w, 13));
    			                	tLLExemptionSchema.setInsuredName(uSSRS.GetText(w, 14));
    			                	
    			                	 String wRiskCode = tLLExemptionSchema.getRiskCode();
    			                     String wPolNo = tLLExemptionSchema.getPolNo();
    			                     String wContNo = tLLExemptionSchema.getContNo();
    			                     String wPayIntv = tLLExemptionSchema.getPayIntv();
    			                     String wFreeStartDate = tLLExemptionSchema.getFreeStartDate();
    			                     String wInsuredNo = tLLExemptionSchema.getInsuredNo();
    			                     String wInsuredName = tLLExemptionSchema.getInsuredName();
    	   	                    	 String wRemark = tLLExemptionSchema.getRemark();
    	       		                 String wPayEndDate = tLLExemptionSchema.getPayEndDate();
    	       		                 String wFreeEndDate = tLLExemptionSchema.getFreeEndDate();
    	       		                 String wFreePried = tLLExemptionSchema.getFreePried();
    	    		         
    	    		                 
    			                     if(wFreeStartDate == null || "".equals(wFreeStartDate)){
    			                    	String tSQL = "select max(accdate) from LLSubReport where subrptno in( "+
    			     		                    "select subrptno from llcaserela where caseno='"+zCaseNo+"')";
    			                    	 ExeSQL tExeSQL = new ExeSQL();
    			                    	 String uFreeStartDate= tExeSQL.getOneValue(tSQL);
    			      	                 tLLExemptionSchema.setFreeStartDate(uFreeStartDate);
    		
    			                        }
    			                     String wAccDate = "";
    			                     String wAccPayToDate = "";//出险日期所在缴至日
    			                     String wCount = "";//多缴保费期数
    			                    
    	    		                //查询出险日期
    	    		                String SQL = "select accdate from LLSubReport where subrptno in( "+
    	    		                        "select subrptno from llcaserela where caseno='"+zCaseNo+"')";
    	    		                    ExeSQL mExeSQL = new ExeSQL();
    	    		                    SSRS aSSRS = mExeSQL.execSQL(SQL);
    	    		                    if (aSSRS == null || aSSRS.getMaxRow() <= 0) {
    	    		                        CError.buildErr(this, "获取出险日期失败");
    	    		                        return false;
    	    		                    }
    	    		                
    	    		                    //计次
    	    		                    int count = 0;
    	    		                    for(int p=1; p<=aSSRS.getMaxRow();p++){
    	    		                        wAccDate = aSSRS.GetText(p, 1);
    	    		                        
    	    		                        System.out.println("wAccDate:"+wAccDate);
    	    		                    
    	    		                        //首先判断其出险日期是否存在退保费情况
    	    		                        
    	    		                        String SQL1 = "select curpaytodate from ljapayperson a where "+
    	    		                        "exists (select 1 from ljapay b where a.contno = b.incomeno) "+
    	    		                        "and contno ='"+wContNo+"' and riskcode ='"+wRiskCode+"' and polno='"+wPolNo+"'" +  // #3081
    	    		                        "and '"+wAccDate+"' between lastpaytodate and curpaytodate "+
    	    		                        "order by contno ";
    	    		                        ExeSQL tExeSQL1 = new ExeSQL();
    	    		                        SSRS tSSRS1 = new SSRS();
    	    		                        tSSRS1 = tExeSQL1.execSQL(SQL1);
    	    		                        if (tSSRS1 == null || tSSRS1.getMaxRow() <= 0) {
    	    		                            CError.buildErr(this, "获取出险日期所在缴至日期失败");
    	    		                            return false;
    	    		                        }
    	    		                        for(int m=1;m <=tSSRS1.getMaxRow();m++){
    	    		                            wAccPayToDate = tSSRS1.GetText(m, 1);
    	    		                        }
    	    		                      //计算退缴保费
    	    		                        String SQL2 = "select payno from ljapayperson c," +
    	    		                                "(select curpaytodate,contno,riskcode,polno from ljapayperson a where " +
    	    		                                "exists (select 1 from ljapay b where a.contno = b.incomeno and duefeetype ='1') " +
    	    		                                "and contno ='"+wContNo+"' and riskcode ='"+dRiskCode+"' and polno='"+wPolNo+"'" +   // #3081
    	    		                                "and '"+wAccPayToDate+"' > lastpaytodate and '"+wAccPayToDate+"' <= curpaytodate) aa " +
    	    		                                "where c.contno=aa.contno and c.curpaytodate >aa.curpaytodate " +
    	    		                                "and c.riskcode = aa.riskcode and c.polno=aa.polno group by payno";
    	    		                        ExeSQL tExeSQL2 = new ExeSQL();
    	    		                        SSRS tSSRS2 = new SSRS();
    	    		                        tSSRS2 = tExeSQL2.execSQL(SQL2);
    	    		                        if(tSSRS2 != null && aSSRS.getMaxRow() > 0){
    	    		                            count++;
    	    		                        }
    	    		                    }
    	    		                    
    	    		                    if(count >= 2){
    	    		                        CError.buildErr(this, "案件下关联多个事件，请回退至检录状态，关联正确的事件");
    	    		                        return false;
    	    		                    }

    	    		                    LLExemptionSchema vLLExemptionSchema = new LLExemptionSchema();
    	      	                    	String vFreePriedDate = "";//记录豁免日期所在的缴至日
    	      	                    	double vFreeAmnt = 0.00;//多缴保费
    	      	                    	String riskName =""; //险种名字
    	      	                        String vFreeStartDate =tLLExemptionSchema.getFreeStartDate();
    	      	                    	 
    			     		            //期数处理 
    			    		            String SQL11 = "select curpaytodate from ljapayperson a where "+
    			    		                            "exists (select 1 from ljapay b where a.contno = b.incomeno) "+
    			    		                             "and contno ='"+wContNo+"' and riskcode ='"+wRiskCode+"' and polno='"+wPolNo+"'"+   // #3081
    			    		                             "and '"+wAccDate+"' between lastpaytodate and curpaytodate "+
    			    		                             "order by contno ";
    			    		            ExeSQL tExeSQL11 = new ExeSQL();
    			    		            SSRS tSSRS11 = new SSRS();
    			    		            tSSRS11 = tExeSQL11.execSQL(SQL11);
    			    		            if (tSSRS11 == null || tSSRS11.getMaxRow() <= 0) {
    			    		                 CError.buildErr(this, "获取出险日期所在缴至日期失败");
    			    		                 return false;
    			    		             }
    			    		            for(int c=1;c <=tSSRS11.getMaxRow();c++){
    			    		            wAccPayToDate = tSSRS11.GetText(c, 1);
    			    		            vLLExemptionSchema.setPayToDate(wAccPayToDate);
    			    		            System.out.println("wAccPayToDate:"+wAccPayToDate);
    			    		            }
    			    		                       
    			    		          //改变期数计算规则，以豁免起期所在的缴至日期开始算起
    			    		            String tFreePriedSql = "select curpaytodate from ljapayperson a where "+
    			    		                 "exists (select 1 from ljapay b where a.contno = b.incomeno) "+
    			    		                 "and contno ='"+wContNo+"' and riskcode ='"+wRiskCode+"' and polno='"+wPolNo+"'"+    // #3081
    			    		                 "and '"+wAccDate+"' between lastpaytodate and curpaytodate "+
    			    		                 "order by contno ";
    			    		                 ExeSQL tFreePriedExeSQL = new ExeSQL();
    			                         SSRS tFreePriedSSRS = new SSRS();
    			                         tFreePriedSSRS = tFreePriedExeSQL.execSQL(tFreePriedSql);
    			                         if (tFreePriedSSRS == null || tFreePriedSSRS.getMaxRow() <= 0) {
    			                             CError.buildErr(this, "获取豁免开始日期所在缴至日期失败");
    			                             return false;
    			                         }
    			                         for(int r=1;r <=tFreePriedSSRS.getMaxRow();r++){
    			                             vFreePriedDate = tFreePriedSSRS.GetText(r, 1);
    			                             System.out.println("vFreePriedDate:"+vFreePriedDate);
    			                         }
    			                         
    			                       //计算退缴保费
    			                         String SQL2 = "select payno,sum(sumduepaymoney) from ljapayperson c," +
    			                                 "(select curpaytodate,contno,riskcode,polno from ljapayperson a where " +
    			                                 "exists (select 1 from ljapay b where a.contno = b.incomeno and duefeetype ='1') " +
    			                                 "and contno ='"+wContNo+"' and riskcode ='"+wRiskCode+"' and polno='"+wPolNo+"'" +    // #3081
    			                                 "and '"+wAccPayToDate+"' > lastpaytodate and '"+wAccPayToDate+"' <= curpaytodate) aa " +
    			                                 "where c.contno=aa.contno and c.curpaytodate >aa.curpaytodate " +
    			                                 "and c.riskcode = aa.riskcode and c.polno = aa.polno group by payno";
    			                         ExeSQL tExeSQL2 = new ExeSQL();
    			                         SSRS tSSRS2 = new SSRS();
    			                         tSSRS2 = tExeSQL2.execSQL(SQL2);
    			                         if (tSSRS2 != null && tSSRS2.getMaxRow() > 0) {
    			                             //退费期数wCount
    			                             wCount = tSSRS2.getMaxRow()+"";
    			                             System.out.println("wCount:"+wCount );
    			                             for(int u=1;u <=tSSRS2.getMaxRow();u++){
    			                                 double temp = Double.parseDouble(tSSRS2.GetText(u, 2));
    			                                 vFreeAmnt += temp; 
    			                                 System.out.println("vFreeAmnt:"+vFreeAmnt);
    			                               
    			                             }
    			                         }
    			                         vLLExemptionSchema.setFreeAmnt(vFreeAmnt);
    			                        if(vFreeAmnt>0){
    			                        	String SQL3= "select riskname from lmrisk where riskcode ='"+wRiskCode+"'";
    			                        	ExeSQL tExeSQL3= new ExeSQL();
    			                        	SSRS tSSRS3 = tExeSQL3.execSQL(SQL3);
    			                        	for(int l=1;l<=tSSRS3.getMaxRow();l++){
    			                        		riskName =tSSRS3.GetText(l,1);
    			                        	}
    			                        	backMsg += "<br>"+riskName+"应退保费金额:"+vFreeAmnt+"<br>";
    			                        }
    			                        
    			                        //计算豁免期数
    			                         int num = 12;
    			                         
    			                         if((PubFun.calInterval(vFreePriedDate, wFreeEndDate, "M"))%num == 0){
    			                             wFreePried = (PubFun.calInterval(vFreePriedDate, wFreeEndDate, "M"))/num+"";
    			                         }else{
    			                             wFreePried = ((PubFun.calInterval(vFreePriedDate, wFreeEndDate, "M"))/num)+1+"";
    			                         }
    			                         System.out.println("vFreePried:"+wFreePried);

    			                         vLLExemptionSchema.setCaseNo(zCaseNo);
    			                         vLLExemptionSchema.setContNo(wContNo);
    			                         vLLExemptionSchema.setPolNo(wPolNo);
    			                         vLLExemptionSchema.setRiskCode(wRiskCode);
    			                         vLLExemptionSchema.setInsuredNo(wInsuredNo);
    			                         vLLExemptionSchema.setInsuredName(wInsuredName);
    			                         vLLExemptionSchema.setPayEndDate(wPayEndDate);
    			                         vLLExemptionSchema.setState("temp");
    			                         vLLExemptionSchema.setPayIntv(wPayIntv);
    			                         vLLExemptionSchema.setFreeStartDate(vFreeStartDate);
    			                         vLLExemptionSchema.setFreeEndDate(wFreeEndDate);
    			                         vLLExemptionSchema.setRemark(wRemark);
    			                         vLLExemptionSchema.setMngCom(mManageCom);
    			                         vLLExemptionSchema.setOperator(mOperator);
    			                         vLLExemptionSchema.setMakeDate(aDate);
    			                         vLLExemptionSchema.setMakeTime(aTime);
    			                         vLLExemptionSchema.setModifyDate(aDate);
    			                         vLLExemptionSchema.setModifyTime(aTime);
    			                         
    			                         //重要字段
    			                         vLLExemptionSchema.setFreePried(wFreePried);//豁免期数
    			                         vLLExemptionSchema.setLocation("ZE"); // # 3310 自动豁免标志
    			                         dLLExemptionSet.add(vLLExemptionSchema);
    		                         
    	    		                    
    	    				}
    	    		             tmpMap.put(dLLExemptionSet, "DELETE&INSERT");
    	    		             backMsg += "<br>保费豁免康乐人生个人疾病保险(A款)和附加康乐人生个人护理保险（A款）。<br>";
    	    			}
    			               
    	    			}
    	    		
    	    		}else{
    	    			continue;
    	    		}
    	    	}
    	    }
    	return true;
    }
   
    
    /**
     * 调用核赔规则
     * @return boolean
     */
    private boolean uwCheck() {
    LLUWCheckBL tLLUWCheckBL = new LLUWCheckBL();

        VData tVData = new VData();

        tVData.addElement(mLLClaimDetailSet);
        tVData.addElement(mLLCaseSchema);
        tVData.add(mLLCaseRelaSet);
        tVData.add(mLLSubReportSet);
        tVData.addElement(mG);

        if (!tLLUWCheckBL.submitData(tVData,"")) {
            CError.buildErr(this,tLLUWCheckBL.getErrors().getFirstError());
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
//        String AppealNo = "S2100090324000001";
//        String tAppealSQL = "SELECT caseno FROM llappeal where appealno='" +
//                            AppealNo
//                            + "' UNION "
//                            + "SELECT b.appealno FROM llappeal a,llappeal b "
//                            + "WHERE a.caseno=b.caseno AND a.appealno='"
//                            + AppealNo + "' AND b.appealno<>'" + AppealNo + "' and not exists (select 1 from llcase where caseno=b.appealno and rgtstate='14')";
//        System.out.println(tAppealSQL);
        String aPolNo = "21092750089";
        String contNO = "008515181000002";
        String aRgtNo = "C6103160512000746";
        String aRiskCode = "730101";
        String aCaseNo = "C6103160512000746";
        String aGetDutyKind = "501";
        String aClmNo = "52022437932";
        String aCaseRelaNo = "86610001632885";

        LLClaimDetailSet aLLClaimDetailSet = new LLClaimDetailSet();
        LLClaimDetailSchema aLLClaimDetailSchema = new LLClaimDetailSchema();

        aLLClaimDetailSchema.setRgtNo(aRgtNo);
        aLLClaimDetailSchema.setContNo(contNO);
        aLLClaimDetailSchema.setGetDutyCode("311201");
        aLLClaimDetailSchema.setGetDutyKind(aGetDutyKind);
        aLLClaimDetailSchema.setStandPay(0);
        aLLClaimDetailSchema.setRealPay(0);
        aLLClaimDetailSchema.setPolNo(aPolNo);
        aLLClaimDetailSchema.setClmNo(aClmNo);
        aLLClaimDetailSchema.setDutyCode("311001");
        aLLClaimDetailSchema.setGiveType("1");
        aLLClaimDetailSchema.setGiveTypeDesc("231321");
        aLLClaimDetailSchema.setGiveReason("01");
        aLLClaimDetailSchema.setGiveReasonDesc("2342");
        aLLClaimDetailSchema.setCaseRelaNo(aCaseRelaNo);
        aLLClaimDetailSchema.setCaseNo(aCaseNo);
        aLLClaimDetailSchema.setDeclineNo("2");
        aLLClaimDetailSchema.setApproveAmnt(1000);
        aLLClaimDetailSchema.setDeclineAmnt(0);
        aLLClaimDetailSet.add(aLLClaimDetailSchema);

        LLClaimSchema aLLClaimSchema = new LLClaimSchema();
        aLLClaimSchema.setRgtNo(aRgtNo);
        aLLClaimSchema.setCasePayType("1");

        GlobalInput tG = new GlobalInput();
        tG.Operator = "cm0009";
        tG.ManageCom = "86";

        VData aVData = new VData();

        aVData.addElement(aLLClaimDetailSet);
        aVData.addElement(tG);
        ClaimSaveBL aClaimSaveBL = new ClaimSaveBL();
        aClaimSaveBL.submitData(aVData, "SAVE");
    }
}
