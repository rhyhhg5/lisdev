/**
 * 黑名单批次导入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  Houyd
 * @version 1.0
 */

package com.sinosoft.lis.llcase;

import java.io.File;
import java.sql.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class LLDescWoundBatchBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mResult = new VData();
    /** 接受前台数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    
    private LDCodeSchema mLDCodeSchema = new LDCodeSchema();

    /** 节点名 */
    private String[] mSheetName = {"Sheet1"};
    /** 配置文件名 */
    private String mConfigName = "LLDescWoundBatchDefiList.xml";
    /** 文件路径 */
    private String mPath;
    /**文件名*/
    private String mFileName="";
    /**导入成功的记录数*/
    private int mSuccNum = 0;
    /**批次号*/
    private String mBatchNo = ""; 
    
    private MMap mMap = new MMap();

    /**
     * 默认无参构造函数
     */
    public LLDescWoundBatchBL() {
    }

    /**
     * 解析Excel的构造函数
     * @param path String
     * @param fileName String
     */
    public LLDescWoundBatchBL(String aPath, String aFileName,
    		GlobalInput aGlobalInput) {
        this.mPath = aPath;
        this.mFileName = aFileName;
        this.mGlobalInput = aGlobalInput;
        
    }
    
    /**
     * 添加传入的一个Sheet数据
     * @param path String
     * @param fileName String
     */
	public boolean doAdd(String aPath, String aFileName) 
    {
   
        //从磁盘导入数据
		LLDescWoundBatchImportFile importFile = new LLDescWoundBatchImportFile(aPath + aFileName,
        		aPath + mConfigName, mSheetName);
        if (!importFile.doImport()) {
            this.mErrors.copyAllErrors(importFile.mErrors);
            return false;
        }

        LDCodeSet tLDCodeSet = (LDCodeSet) importFile.getLDCodeSet();
        
        MMap map = new MMap();

        mSuccNum = tLDCodeSet.size();
        System.out.println("导入数据容量："+tLDCodeSet.size());
        //对sheet中的数据进行再处理
        for (int i = 1; i <= tLDCodeSet.size(); i++) 
        {
        	LDCodeSchema tLDCodeSchema = new LDCodeSchema();
        	tLDCodeSchema = tLDCodeSet.get(i).getSchema();
        	
        	if(!checkdata(tLDCodeSchema)){
        		return false;
        	}
        	
        	tLDCodeSchema.setCodeType("desc_wound");//伤残信息配置类型
        	tLDCodeSchema.setCodeAlias(tLDCodeSchema.getCodeName());
        	       	
        	//将每一条数据放入返回的结果集
        	this.mResult.add(tLDCodeSchema);
            //添加一条黑客户信息
        	addOneBlackUser(map, tLDCodeSchema);
        	
        }

        //提交数据到数据库
        if (!submit(map)) {
            return false;
        }
        return true;
    }

	/**
	 * 数据完整性校验
	 * @param tLDCodeSchema
	 * @return
	 */
    private boolean checkdata(LDCodeSchema tLDCodeSchema) {
    	//数据完整性的校验
    	if(tLDCodeSchema.getCode() == null || "".equals(tLDCodeSchema.getCode())){
    		CError tError = new CError();
			tError.moduleName = "LLDescWoundBatchBL";
			tError.functionName = "submitData";
			tError.errorMessage = "存在残疾代码为空，终止数据提交!";
			this.mErrors.addOneError(tError);
			return false;
    	}
    	if(tLDCodeSchema.getCodeName() == null || "".equals(tLDCodeSchema.getCodeName())){
    		CError tError = new CError();
			tError.moduleName = "LLDescWoundBatchBL";
			tError.functionName = "submitData";
			tError.errorMessage = "存在残疾名称为空，终止数据提交!";
			this.mErrors.addOneError(tError);
			return false;
    	}
    	if(tLDCodeSchema.getComCode() == null || "".equals(tLDCodeSchema.getComCode())){
    		CError tError = new CError();
			tError.moduleName = "LLDescWoundBatchBL";
			tError.functionName = "submitData";
			tError.errorMessage = "存在残疾级别为空，终止数据提交!";
			this.mErrors.addOneError(tError);
			return false;
    	}
    	if(tLDCodeSchema.getOtherSign() == null || "".equals(tLDCodeSchema.getOtherSign())){
    		CError tError = new CError();
			tError.moduleName = "LLDescWoundBatchBL";
			tError.functionName = "submitData";
			tError.errorMessage = "存在给付比例为空，终止数据提交!";
			this.mErrors.addOneError(tError);
			return false;
    	}
    	return true;
	}

	public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if(!"DELETE".equals(mOperate) && !"UPDATE".equals(mOperate)){
        	CError tError = new CError();
			tError.moduleName = "LLDescWoundBatchBL";
			tError.functionName = "submitData";
			tError.errorMessage = "不支持的数据操作!";

			this.mErrors.addOneError(tError);
			return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        
        if(!checkdata(mLDCodeSchema)){
    		return false;
    	}
        //进行业务处理

        PubSubmit pb = new PubSubmit();
        MMap map = new MMap();
        map.put(mLDCodeSchema, mOperate);
		VData tVData = new VData();
		
		tVData.add(map);
		if(!pb.submitData(tVData,"")){
			// @@错误处理
			this.mErrors.copyAllErrors(pb.mErrors);
			CError tError = new CError();
			tError.moduleName = "BlackListBatchBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
    	mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLDCodeSchema = (LDCodeSchema) cInputData.getObjectByObjectName("LDCodeSchema",0);
        return true;

    }

        /**
         * 准备后台的数据
         * @return boolean
         */
        private boolean prepareOutputData() {
            try {
                mInputData = new VData();
                this.mInputData.add(mMap);
            } catch (Exception ex) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "BlackListPersonBL";
                tError.functionName = "prepareData";
                tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
                this.mErrors.addOneError(tError);
                return false;
            }
            return true;
        }

        public VData getResult() {
            return mResult;
        }
        
        /**
         * 添加一个黑名单用户
         *
         */
        private void addOneBlackUser(MMap aMap,LDCodeSchema aLDCodeSchema){
        	aMap.put(aLDCodeSchema,"DELETE&INSERT");
        }
        
        /**
         * 数据提交公共方法
         * @return
         */
        public boolean submit(MMap aMap ){
			PubSubmit pb = new PubSubmit();
			VData tVData = new VData();
			tVData.add(aMap);
			if(!pb.submitData(tVData,"")){
				// @@错误处理
				this.mErrors.copyAllErrors(pb.mErrors);

				CError tError = new CError();
				tError.moduleName = "BlackListBatchBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";

				this.mErrors.addOneError(tError);
				return false;
			}
			return true;
		}

		public int getSuccNum() {
			return mSuccNum;
		}

		public void setMSuccNum(int succNum) {
			mSuccNum = succNum;
		}

		public String getBatchNo() {
			return mBatchNo;
		}
}
