/*
 * @(#)LLBatchAffixUI.java	2005-01-05
 *
 * Copyright 2005 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/*
 * <p>Title: Web业务系统</p>
 * <p>ClassName:LLInqFeeUI </p>
 * <p>Description: 理赔案件-直接调查费录入 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft </p>
 * @author：Xx
 * @version：1.0
 * @CreateDate：2005-02-20
 */
public class LLBatchAffixUI {
    private VData mInputData;
    private VData mResult = new VData();
    public CErrors mErrors = new CErrors();

    public LLBatchAffixUI() {
    }

    public static void main(String[] args) {
        LLBatchAffixUI tLLInqFeeUI = new LLBatchAffixUI();
        LLInqFeeSchema tLLInqFeeSchema = new LLInqFeeSchema();
        LLInqFeeSet tLLInqFeeSet = new LLInqFeeSet();
        tLLInqFeeSchema.setFeeItem("01");
        tLLInqFeeSchema.setSurveyNo("86000000000001");
        tLLInqFeeSchema.setFeeSum("213");
        tLLInqFeeSchema.setOtherNo("C0000050526000001");
        tLLInqFeeSchema.setOtherNoType("1");
        tLLInqFeeSchema.setContSN("1");
        tLLInqFeeSchema.setInqDept("86");
        tLLInqFeeSet.add(tLLInqFeeSchema);
        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "claim";
        tGI.ManageCom = "86";
        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tLLInqFeeSet);
        tLLInqFeeUI.submitData(tVData, "UPDATE");
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData 输入数据
     * @param cOperate String 操作类型
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        LLBatchAffixBL tLLBatchAffixBL = new LLBatchAffixBL();
        tLLBatchAffixBL.submitData(mInputData, cOperate);

        //如果有需要处理的错误，则返回
        if (tLLBatchAffixBL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tLLBatchAffixBL.mErrors);
        }

        //取得结果返回前台
        this.mResult.clear();
        this.mResult = tLLBatchAffixBL.getResult();
        mInputData = null;
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

}
