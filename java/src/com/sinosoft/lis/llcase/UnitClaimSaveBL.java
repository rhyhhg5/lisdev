package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: SimpleClaimAuditBL </p>
 * <p>Description: 团体批量审定 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft </p>
 * @author:Xx
 * @CreateDate：2005-02-18 13:58:55
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bl.LCPolBL;

public class UnitClaimSaveBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mG = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private String aDate = "";
    private String aTime = "";

//    private String mRgtNo = "";

    public UnitClaimSaveBL() {
    }

    public static void main(String[] args) {
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLCaseSet tLLCaseSet = new LLCaseSet();
        VData tVData = new VData();
        UnitClaimSaveBL tUnitClaimSaveBL = new UnitClaimSaveBL();

        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "8632";
        mGlobalInput.ComCode = "8632";
        mGlobalInput.Operator = "cm3201";

        tLLRegisterSchema.setRgtNo("P3200061031000002");
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setCaseNo("C3200061228000002");
        tLLCaseSchema.setRgtState("02");
        tLLCaseSchema.setCancleReason("1");
        tLLCaseSchema.setCancleRemark("郁闷郁闷郁闷");
        tVData.add(tLLCaseSchema);
        tVData.add(tLLRegisterSchema);
        tVData.add(mGlobalInput);
        tUnitClaimSaveBL.submitData(tVData, "GIVE|ENSURE");
        tVData.clear();
        tVData = tUnitClaimSaveBL.getResult();
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行数据基本校验
        if(!cOperate.equals("BATCH")){
            if (!checkData())
                return false;
        }
        //进行业务处理
        if (!dealData())
            return false;

        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, null)) {
            CError.buildErr(this,"数据提交失败!");
            return false;
        }
        System.out.println("End UnitClaimSaveBL Submit...");

        mInputData.clear();
        map = new MMap();

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mLLCaseSchema = (LLCaseSchema) cInputData.getObjectByObjectName("LLCaseSchema", 0);
        mG = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        aDate = PubFun.getCurrentDate();
        aTime = PubFun.getCurrentTime();
        return true;
    }

    /**
     * 处理前检查数据是否合理
     * @return boolean
     */
    private boolean checkData(){
        System.out.println("---start checkData---");
        LLCaseDB tLLCaseDB = new LLCaseDB();
        String tCaseNo = mLLCaseSchema.getCaseNo();
        tLLCaseDB.setCaseNo(tCaseNo);
        if (!tLLCaseDB.getInfo()) {
            CError.buildErr(this,"案件" + tCaseNo + "查询失败" );
            return false;
        }
        if ("11".equals(tLLCaseDB.getRgtState()) ||
            "12".equals(tLLCaseDB.getRgtState())) {
            CError.buildErr(this, "案件" + tCaseNo + "已经确认给付，不能再做审定操作。");
            return false;
        }
        mLLCaseSchema = tLLCaseDB.getSchema();
        return true;
    }

    /**
     * 自动核配，生成赔付结论，和业务应付
     * @return boolean
     */
    private boolean dealData() {
        String aCaseNo = mLLCaseSchema.getCaseNo();
        String delsql1 = "delete from llclaimpolicy where caseno='"+aCaseNo+"'";
        String delsql2 = "delete from llclaimdetail where caseno='"+aCaseNo+"'";
        String delsql3 = "delete from ljsgetclaim where otherno='"+aCaseNo+"'";
        String delsql4 = "delete from ljsget where otherno='"+aCaseNo+"'";

        map.put(delsql1,"DELETE");
        map.put(delsql2,"DELETE");
        map.put(delsql3,"DELETE");
        map.put(delsql4,"DELETE");
        LLClaimDetailSet sLLClaimDetailSet = new LLClaimDetailSet();
        LJSGetClaimSet sLJSGetClaimSet = new LJSGetClaimSet();
        System.out.println("dealCase......");
        Reflections tref = new Reflections();
        String strLimit = PubFun.getNoLimit(mG.ManageCom);
        String tGetNoticeNo = PubFun1.CreateMaxNo("GetNoticeNo", strLimit);
        String aClmNo="";
        LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
        LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();
        tLLClaimPolicyDB.setCaseNo(aCaseNo);
        tLLClaimPolicySet = tLLClaimPolicyDB.query();
        //系统需要自动回退该案件

        double crealpay = 0.0;
        double cstandpay = 0.0;
        
        // # 2864 天津静海结算退费情况**start**
        String tJCaseType="";
        String tJJHSQL ="select casetype from llhospcase where caseno='"+mLLCaseSchema.getCaseNo()+"' with ur";
        ExeSQL tJJHExe = new ExeSQL();
        SSRS tJJHSSRS = tJJHExe.execSQL(tJJHSQL);
       
        if(tJJHSSRS.getMaxRow()>0){
        	tJCaseType = tJJHSSRS.GetText(1, 1);
        }
        // # 2864 天津静海结算退费情况**end**
        boolean bool=false;
        //考虑拒付的情况，不加金额为0的判断？？
        LLClaimPolicySet mLLClaimPolicySet = new LLClaimPolicySet();
        for(int i=1;i<=tLLClaimPolicySet.size();i++){
            LLClaimPolicySchema tClmPolicy = new LLClaimPolicySchema();
            tClmPolicy = tLLClaimPolicySet.get(i);
            aClmNo = tClmPolicy.getClmNo();
            String aPolNo = tClmPolicy.getPolNo();
            String aGetDutyKind = tClmPolicy.getGetDutyKind();
            LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
            LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
            tLLClaimDetailDB.setClmNo(aClmNo);
            tLLClaimDetailDB.setCaseRelaNo(tClmPolicy.getCaseRelaNo());
            tLLClaimDetailDB.setPolNo(aPolNo);
            tLLClaimDetailDB.setGetDutyKind(aGetDutyKind);
            tLLClaimDetailSet = tLLClaimDetailDB.query();
            if (tLLClaimDetailSet.size() <= 0)
                continue;
            double polstandpay = 0.0;
            double polrealpay = 0.0;
            String tstattype = "";
            for (int j = 1; j <= tLLClaimDetailSet.size(); j++) {
                LLClaimDetailSchema tClmDetailSchema = new
                        LLClaimDetailSchema();
                tClmDetailSchema = tLLClaimDetailSet.get(j);
                double dStandPay = tClmDetailSchema.getStandPay();
                double dRealPay = tClmDetailSchema.getRealPay();
                //如果机构为8694，并且菜单是青岛无名单，并且金额为负，需要特殊处理。
                boolean bool2=false;
                String mang=mG.ManageCom.length()>=4?mG.ManageCom.substring(0,4):mG.ManageCom;
//                String mng=tClmDetailSchema.getMngCom().substring(0, 4);
//                int index=pageName.indexOf("case/QDGrpSimpleClaimConf.jsp");
                String qdSQL=" select count(1) from LLFeeMain where 1=1 and ReceiptNo='94000' and FeeAtti='4' and caseno='"+mLLCaseSchema.getCaseNo() +"'  with ur ";
//                String mange=mGlobalInput.ManageCom.length()>=4?mGlobalInput.ManageCom.substring(0, 4):mGlobalInput.ManageCom;//当前机构是否为8694
                String index=new ExeSQL().getOneValue(qdSQL);
                
                if(!"0".equals(index) && "8694".equals(mang)){
                	bool=true;
                	bool2=true;
                }else{
                	bool2=false;
                }
                if( bool2 &&  dRealPay<0){
                	tClmDetailSchema.setGiveType("1");
                    tClmDetailSchema.setGiveTypeDesc("正常给付");
                } else
                // # 2864 天津静海结算退费 **start**
                if( dRealPay < 0 && tJCaseType.equals("03")){
                	tClmDetailSchema.setGiveType("1");
                    tClmDetailSchema.setGiveTypeDesc("正常给付");
                }
                // # 2864 天津静海结算退费 **end**
                // # 3853 天津大病结算退费 start
                else if(dRealPay < 0 && tJCaseType.equals("13")) {
                	tClmDetailSchema.setGiveType("1");
                    tClmDetailSchema.setGiveTypeDesc("正常给付");
                }
             // # 3853 天津大病结算退费 end
                /**没有考虑拒付的情况，因为拒付需要给出拒付原因，而且拒付的核赔规则与给付不同
                 * 考虑将拒付案件抛出，按正常的流程核赔，
                 */
                else if (dStandPay < 0.01 && dRealPay < 0.01 && !bool2){
                    tClmDetailSchema.setGiveType("3");
                    tClmDetailSchema.setGiveTypeDesc("全额拒付");
                } else if (dStandPay - dRealPay > 0.01) {
                    tClmDetailSchema.setGiveType("2");
                    tClmDetailSchema.setGiveTypeDesc("部分给付");
                } else {
                    tClmDetailSchema.setGiveType("1");
                    tClmDetailSchema.setGiveTypeDesc("正常给付");
                }

                polstandpay += dStandPay;
                polrealpay += dRealPay;
                tstattype = tClmDetailSchema.getStatType();
                
                LMDutyGetDB tLMDutyGetDB = new LMDutyGetDB();
                tLMDutyGetDB.setGetDutyCode(tClmDetailSchema.getGetDutyCode());
                if (!tLMDutyGetDB.getInfo()) {
                	CError.buildErr(this, "案件"+aCaseNo+"没有理算成功,查询给付责任失败，不能审定。");
                    return false;
                }
                
                if ("1".equals(tLMDutyGetDB.getNeedAcc())) {
                	tstattype = "ZH";
                }
                
                
                tClmDetailSchema.setOperator(mG.Operator);
                tClmDetailSchema.setModifyDate(aDate);
                tClmDetailSchema.setModifyTime(aTime);
                sLLClaimDetailSet.add(tClmDetailSchema);
                boolean hasjs = false;
                for(int k=1;k<=sLJSGetClaimSet.size();k++){
                    LJSGetClaimSchema tljsgetclm = sLJSGetClaimSet.get(k);
                    if(tljsgetclm.getFeeFinaType().equals(tstattype)&&
                       tljsgetclm.getFeeOperationType().equals(aGetDutyKind)&&
                       tljsgetclm.getPolNo().equals(aPolNo)){
                        double apaymoney = tljsgetclm.getPay()+
                                           tClmDetailSchema.getRealPay();
                        apaymoney = Arith.round(apaymoney,2);
                        tljsgetclm.setPay(apaymoney);
                        hasjs = true;
                    }
                }
                if(!hasjs){
                    LJSGetClaimSchema tljsgetclm = new LJSGetClaimSchema();
                    tref.transFields(tljsgetclm,tClmDetailSchema);
                    tljsgetclm.setFeeFinaType(tstattype);
                    tljsgetclm.setFeeOperationType(aGetDutyKind);
                    tljsgetclm.setRiskVersion(tClmPolicy.getRiskVer());
                    tljsgetclm.setGetNoticeNo(tGetNoticeNo);
                    //添加续保查询
                    LCPolBL tLCPolBL = new LCPolBL();
                    tLCPolBL.setPolNo(tClmDetailSchema.getPolNo());
                    if (!tLCPolBL.getInfo()) {
                        CError.buildErr(this, "保单查询失败[" + tClmDetailSchema.getPolNo() + "]");
                        return false;
                    }
                    tljsgetclm.setAgentCode(tLCPolBL.getAgentCode());
                    tljsgetclm.setAgentCom(tLCPolBL.getAgentCom());
                    tljsgetclm.setAgentGroup(tLCPolBL.getAgentGroup());
                    tljsgetclm.setAgentType(tLCPolBL.getAgentType());
                    tljsgetclm.setMakeDate(aDate);
                    tljsgetclm.setMakeTime(aTime);
                    tljsgetclm.setPay(tClmDetailSchema.getRealPay());
                    tljsgetclm.setOtherNo(aCaseNo);
                    tljsgetclm.setOtherNoType("5");
                    tljsgetclm.setManageCom(tClmDetailSchema.getPolMngCom());
                    sLJSGetClaimSet.add(tljsgetclm);
                }
            }
            if (polrealpay > 0.001 && polstandpay > 0.001) {
                tClmPolicy.setRealPay(Arith.round(polrealpay, 2));
                tClmPolicy.setStandPay(Arith.round(polstandpay, 2));
                crealpay += polrealpay;
                cstandpay += polstandpay;
                if (polstandpay - polrealpay > 0.01) {
                    tClmPolicy.setGiveType("2");
                    tClmPolicy.setGiveTypeDesc("部分给付");
                } else {
                    tClmPolicy.setGiveType("1");
                    tClmPolicy.setGiveTypeDesc("正常给付");
                }
                tClmPolicy.setClmUWer(mG.Operator);
                tClmPolicy.setModifyDate(aDate);
                tClmPolicy.setModifyTime(aTime);
                mLLClaimPolicySet.add(tClmPolicy);
            }else if (polrealpay == 0 && polstandpay == 0) {
                tClmPolicy.setRealPay(Arith.round(polrealpay, 2));
                tClmPolicy.setStandPay(Arith.round(polstandpay, 2));
                tClmPolicy.setGiveType("3");
                tClmPolicy.setGiveTypeDesc("全额拒付");
                tClmPolicy.setClmUWer(mG.Operator);
                tClmPolicy.setModifyDate(aDate);
                tClmPolicy.setModifyTime(aTime);
                mLLClaimPolicySet.add(tClmPolicy);
            }
            // #2864 天津静海结算退费情况**start**
            else if(polrealpay < 0  && tJCaseType.equals("03")){
            	tClmPolicy.setRealPay(Arith.round(polrealpay, 2));
            	tClmPolicy.setStandPay(Arith.round(polstandpay, 2));
            	crealpay += polrealpay;
                cstandpay += polstandpay;
            	tClmPolicy.setGiveType("1");
            	tClmPolicy.setGiveTypeDesc("正常给付");
            	tClmPolicy.setClmUWer(mG.Operator);
            	tClmPolicy.setModifyDate(aDate);
            	tClmPolicy.setModifyTime(aTime);
            	mLLClaimPolicySet.add(tClmPolicy);
            	
            }
            // # 2864 天津静海结算退费情况**end**
            // #3853 天津大病结算退费情况**start**
            else if(polrealpay < 0  && tJCaseType.equals("13")){
            	tClmPolicy.setRealPay(Arith.round(polrealpay, 2));
            	tClmPolicy.setStandPay(Arith.round(polstandpay, 2));
            	crealpay += polrealpay;
                cstandpay += polstandpay;
            	tClmPolicy.setGiveType("1");
            	tClmPolicy.setGiveTypeDesc("正常给付");
            	tClmPolicy.setClmUWer(mG.Operator);
            	tClmPolicy.setModifyDate(aDate);
            	tClmPolicy.setModifyTime(aTime);
            	mLLClaimPolicySet.add(tClmPolicy);
            	
            }
            // # 3853 天津大病结算退费情况**end**
            else if(bool && polrealpay < 0){ //3454青岛护理险
            	tClmPolicy.setRealPay(Arith.round(polrealpay, 2));
                tClmPolicy.setStandPay(Arith.round(polstandpay, 2));
                crealpay += polrealpay;
                cstandpay += polstandpay;
                tClmPolicy.setGiveType("1");
                tClmPolicy.setGiveTypeDesc("正常给付");
                tClmPolicy.setClmUWer(mG.Operator);
                tClmPolicy.setModifyDate(aDate);
                tClmPolicy.setModifyTime(aTime);
                mLLClaimPolicySet.add(tClmPolicy);
            }
        }
        if(mLLClaimPolicySet.size()<=0){
            CError.buildErr(this, "案件"+aCaseNo+"没有理算成功,不能审定，系统自动回退。");
            return false;
        }
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setClmNo(aClmNo);
        if(!tLLClaimDB.getInfo()){
            return false;
        }
        LLClaimSchema tLLClaimSchema = tLLClaimDB.getSchema();
        tLLClaimSchema.setStandPay(Arith.round(cstandpay, 2));
        tLLClaimSchema.setRealPay(Arith.round(crealpay, 2));
        if(crealpay <0 && bool ){ //3454青岛护理险
           	tLLClaimSchema.setGiveType("1");
        	tLLClaimSchema.setGiveTypeDesc("正常给付");
        } else 
        if (cstandpay - crealpay > 0.01) {
            tLLClaimSchema.setGiveType("2");
            tLLClaimSchema.setGiveTypeDesc("部分给付");
        } else  if (cstandpay == 0 && crealpay == 0) {
            tLLClaimSchema.setGiveType("3");
            tLLClaimSchema.setGiveTypeDesc("全额拒付");
        }
        //# 2864 天津静海结算退费情况    **start***
        else if( crealpay <0 && tJCaseType.equals("03")){ 
        	tLLClaimSchema.setGiveType("1");
        	tLLClaimSchema.setGiveTypeDesc("正常给付");
        // # 2864 天津静海结算退费情况 **end **
        }//3853 天津大病退费 start
        else if (crealpay <0 && tJCaseType.equals("13")) {
        	tLLClaimSchema.setGiveType("1");
        	tLLClaimSchema.setGiveTypeDesc("正常给付");
        }
         //3853 天津大病退费 end
        else {
            tLLClaimSchema.setGiveType("1");
            tLLClaimSchema.setGiveTypeDesc("正常给付");
        }
        tLLClaimSchema.setClmUWer(mG.Operator);
        tLLClaimSchema.setModifyDate(aDate);
        tLLClaimSchema.setModifyTime(aTime);
        map.put(tLLClaimSchema,"UPDATE");

        LJSGetSchema tLJSGetSchema = new LJSGetSchema();
        tref.transFields(tLJSGetSchema,sLJSGetClaimSet.get(1));
        tLJSGetSchema.setSumGetMoney(Arith.round(crealpay,2));
        map.put(sLJSGetClaimSet,"INSERT");
        map.put(tLJSGetSchema,"INSERT");
        map.put(mLLClaimPolicySet,"INSERT");
        map.put(sLLClaimDetailSet,"INSERT");
        mLLCaseSchema.setRgtState("04");
        return true;
    }


    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            mInputData.add(map);
            mResult.clear();
        } catch (Exception ex) {
            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
