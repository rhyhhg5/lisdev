/*
 * <p>ClassName: LLAffixBL </p>
 * <p>Description: LLAffixBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author:YangMing:
 * @CreateDate：2005-01-19 13:58:55
 */
package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class ReasonBL {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();
  private VData mResult = new VData();

  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  private MMap map = new MMap();

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();

  /** 数据操作字符串 */
  private String mOperate;
  private String strHandler;

  /** 业务处理相关变量 */
  //private LLAppClaimReasonSchema mLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
  private LLAppClaimReasonSet mLLAppClaimReasonSet = new LLAppClaimReasonSet();
  private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
  private TransferData mTransferData = new TransferData();

//private LDDrugSet mLDDrugSet=new LDDrugSet();
  public ReasonBL() {
  }

  public static void main(String[] args) {
  }

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
    if(!getClaimDeal())
      return false;
    //进行业务处理
    if (!dealData()) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ReasonBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据处理失败ReasonBL-->dealData!";
      this.mErrors.addOneError(tError);
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN")) {

    }
    else {
      System.out.println("Start ReasonBL Submit...");
      PubSubmit tPubSubmit = new PubSubmit();
      if (!tPubSubmit.submitData(mInputData, mOperate)) {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        CError tError = new CError();
        tError.moduleName = "ReasonBL";
        tError.functionName = "ReasonBL";
        tError.errorMessage = "数据提交失败!";

        this.mErrors.addOneError(tError);
        return false;
      }
      System.out.println("End ReasonBL Submit...");

    }
    mInputData = null;
    return true;
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData()
  {
    System.out.println("---Star dealData---");
    String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
    System.out.println("管理机构代码是 : " + tLimit);
    String CaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit);
    System.out.println("理赔号" + CaseNo);
    if (this.mOperate.equals("INSERT||MAIN"))
    {
      System.out.println("---Star Insert Operate ---");
      if (mLLAppClaimReasonSet != null && mLLAppClaimReasonSet.size() > 0)
      {
        for (int i = 1; i <= mLLAppClaimReasonSet.size(); i++)
        {
          mLLAppClaimReasonSet.get(i).setCaseNo(CaseNo);
          mLLAppClaimReasonSet.get(i).setMakeDate(PubFun.getCurrentDate());
          mLLAppClaimReasonSet.get(i).setMakeTime(PubFun.getCurrentTime());
          mLLAppClaimReasonSet.get(i).setModifyDate(PubFun.getCurrentDate());
          mLLAppClaimReasonSet.get(i).setModifyTime(PubFun.getCurrentTime());
          mLLAppClaimReasonSet.get(i).setOperator(mGlobalInput.Operator);
          mLLAppClaimReasonSet.get(i).setMngCom(mGlobalInput.ManageCom);
        }
        map.put(mLLAppClaimReasonSet,"INSERT");
      }
        mLLCaseSchema.setCaseNo(CaseNo);
        mLLCaseSchema.setMakeDate(PubFun.getCurrentDate());
        mLLCaseSchema.setMakeTime(PubFun.getCurrentTime());
        mLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
        mLLCaseSchema.setModifyTime(PubFun.getCurrentTime());
        mLLCaseSchema.setOperator(mGlobalInput.Operator);
        mLLCaseSchema.setSurveyFlag("0");
        mLLCaseSchema.setHandler(strHandler);
        mLLCaseSchema.setMngCom(mGlobalInput.ManageCom);
        map.put(mLLCaseSchema,"INSERT");
    }
    System.out.println("---End dealData---");
    return true;
  }
private boolean dealHandler()
{
  LLCaseCommon tLLCaseCommon = new LLCaseCommon();
  //String strMngCom = "8611";
  String strMngCom;

  strMngCom = mGlobalInput.ManageCom;
  System.out.println("当前的登陆机构是＝＝＝＝"+strMngCom);
  strHandler = tLLCaseCommon.ChooseAssessor(strMngCom,"1");
  if (strHandler==null || strHandler.equals(""))
  {
    return false;
  }
  System.out.println("本次操作的审核人是＝＝＝＝"+strHandler);
  return true;
}
//查询出一格又理赔权限的理赔师暂时代替Handler
private boolean getClaimDeal()
{
  LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
  LLClaimUserSet tLLClaimUserSet = new LLClaimUserSet();
  //String strMngCom = "8611";
  String strMngCom;

  strMngCom = mGlobalInput.ManageCom;
  System.out.println("当前的登陆机构是＝＝＝＝"+strMngCom);
   tLLClaimUserDB.setClaimDeal("1");
   tLLClaimUserDB.setStateFlag("1");
   tLLClaimUserSet =tLLClaimUserDB.query();
   strHandler = tLLClaimUserSet.get(1).getUserCode();
  if (strHandler==null || strHandler.equals(""))
  {
    return false;
  }
  System.out.println("暂时采用有理赔权限的理赔师代替Handler ："+strHandler);

  return true;
}


  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean updateData() {
    return true;
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean deleteData() {
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    System.out.println("---Star getInputData---");
    this.mLLAppClaimReasonSet.set( (LLAppClaimReasonSet) cInputData.
                                  getObjectByObjectName("LLAppClaimReasonSet",0));
    mLLCaseSchema.setSchema( (LLCaseSchema) cInputData.
                                 getObjectByObjectName("LLCaseSchema", 0));
    this.mGlobalInput.setSchema( (GlobalInput) cInputData.getObjectByObjectName(
        "GlobalInput", 0));
    System.out.println("---End getInputData---");
    return true;
  }

  private boolean prepareOutputData() {
    try {
      this.mInputData.clear();
      mInputData.add(map);
      mResult.clear();
      mResult.add(mLLCaseSchema);
      mResult.add(mLLAppClaimReasonSet);
      mResult.addElement(mTransferData);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ReasonBL";
      tError.functionName = "prepareData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  public VData getResult() {
    return this.mResult;
  }
}
