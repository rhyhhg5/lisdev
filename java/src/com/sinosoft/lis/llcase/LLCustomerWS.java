package com.sinosoft.lis.llcase;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CheckEdorItem;
import com.sinosoft.lis.bq.CheckFieldBL;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.bq.DisabledManageBL;
import com.sinosoft.lis.bq.GrpEdorWSAppConfirmBL;
import com.sinosoft.lis.bq.GrpEdorWSConfirmBL;
import com.sinosoft.lis.bq.OldCustomerCheck;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * 客户信息实名化
 * @author maning
 * @version 1.0
 */
public class LLCustomerWS extends LLCaseProcess {

	/**
	 * 
	 */
	public LLCustomerWS() {
		// TODO Auto-generated constructor stub
	}

	// 业务逻辑错误
	public CErrors mErrors = new CErrors();

	// 获取的业务数据
	private VData mInputData = new VData();

	// 返回处理后数据
	private VData mResult = new VData();

	// 返回的提交数据库MMap
	private MMap mMMap = new MMap();

	// 用户登录信息
	private GlobalInput mGlobalInput = new GlobalInput();

	// 操作类型
	private String mOperate = "";

	// 实名化信息
	LPDiskImportSchema mLPDiskImportSchema = new LPDiskImportSchema();

	// 团体保单信息
	LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private CachedLPInfo mCachedLPInfo = CachedLPInfo.getInstance();

	VData mVData = new VData();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sinosoft.lis.llcase.LLCaseProcess#getMMap()
	 */
	public MMap getMMap() throws Exception {
		// TODO Auto-generated method stub
		return mMMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sinosoft.lis.llcase.LLCaseProcess#getResult()
	 */
	public VData getResult() throws Exception {
		// TODO Auto-generated method stub
		return mResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sinosoft.lis.llcase.LLCaseProcess#submitData(com.sinosoft.utility.VData,
	 *      java.lang.String)
	 */
	public boolean submitData(VData aInputData, String aOperate)
			throws Exception {
		System.out.println("理赔客户信息实名化开始");
		mInputData = aInputData;
		mOperate = aOperate;

		if (!getInputData()) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		System.out.println("理赔客户信息实名化结束");

		return true;
	}

	/**
	 * 解析获取的对象
	 * 
	 * @return 处理成功标志
	 * @throws Excepiton
	 */
	private boolean getInputData() throws Exception {
		mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0);

		if (mGlobalInput == null) {
			mErrors.addOneError("用户登录信息获取失败");
			return false;
		}

		mLPDiskImportSchema = (LPDiskImportSchema) mInputData
				.getObjectByObjectName("LPDiskImportSchema", 0);

		if (mLPDiskImportSchema == null) {
			mErrors.addOneError("实名化信息获取失败");
			return false;
		}

		return true;
	}

	/**
	 * 基本数据校验
	 * 
	 * @return 校验成功标志
	 * @throws Exception
	 */
	private boolean checkData() throws Exception {
		if (mGlobalInput.Operator == null || "".equals(mGlobalInput.Operator)) {
			mErrors.addOneError("操作用户登录信息获取失败");
			return false;
		}

		if (mGlobalInput.ManageCom == null || "".equals(mGlobalInput.ManageCom)) {
			mErrors.addOneError("操作用户登录信息获取失败");
			return false;
		}

		if (mLPDiskImportSchema.getInsuredName() == null
				|| "".equals(mLPDiskImportSchema.getInsuredName())) {
			mErrors.addOneError("被保险人姓名获取失败");
			return false;
		}

		if (mLPDiskImportSchema.getSex() == null
				|| "".equals(mLPDiskImportSchema.getSex())) {
			mErrors.addOneError("被保险人性别获取失败");
			return false;
		}

		LDCodeSchema tLDCodeSchema = mCachedLPInfo
				.findSexByCode(mLPDiskImportSchema.getSex());
		if (tLDCodeSchema == null) {
			mErrors.addOneError("被保险人性别代码错误");
			return false;
		}

		if (mLPDiskImportSchema.getBirthday() == null
				|| "".equals(mLPDiskImportSchema.getBirthday())) {
			mErrors.addOneError("被保险人出生日期获取失败");
			return false;
		}

		if (!PubFun.checkDateForm(mLPDiskImportSchema.getBirthday())) {
			mErrors.addOneError("被保险人出生日期错误");
			return false;
		}

		if (mLPDiskImportSchema.getIDType() == null
				|| "".equals(mLPDiskImportSchema.getIDType())) {
			mErrors.addOneError("被保险人证件类型获取失败");
			return false;
		}

		tLDCodeSchema = mCachedLPInfo.findIDTypeByCode(mLPDiskImportSchema
				.getIDType());
		if (tLDCodeSchema == null) {
			mErrors.addOneError("被保险人证件类型代码错误");
			return false;
		}

		if (mLPDiskImportSchema.getIDNo() == null
				|| "".equals(mLPDiskImportSchema.getIDNo())) {
			mErrors.addOneError("被保险人证件号码获取失败");
			return false;
		}

		// 校验身份证号
		if ("0".equals(mLPDiskImportSchema.getIDType())) {
			if (!PubFun.checkIDNo(mLPDiskImportSchema.getIDNo())) {
				mErrors.addOneError("身份证号错误!");
				return false;
			}
			String tBirtyday = PubFun.getBirthdayFromId(mLPDiskImportSchema
					.getIDNo());
			if (!tBirtyday.equals(mLPDiskImportSchema.getBirthday())) {
				mErrors.addOneError("身份证号与出生日期不符!");
				return false;
			}

			String tSex = PubFun.getSexFromId(mLPDiskImportSchema.getIDNo());
			if (!tSex.equals(mLPDiskImportSchema.getSex())) {
				mErrors.addOneError("身份证号与性别不符!");
				return false;
			}
		}

		if (mLPDiskImportSchema.getEdorValiDate() == null
				|| "".equals(mLPDiskImportSchema.getEdorValiDate())) {
			mErrors.addOneError("生效日期获取失败");
			return false;
		}

		if (!PubFun.checkDateForm(mLPDiskImportSchema.getEdorValiDate())) {
			mErrors.addOneError("生效日期格式错误");
			return false;
		}

//		if (mLPDiskImportSchema.getRetire() == null
//				|| "".equals(mLPDiskImportSchema.getRetire())) {
//			mErrors.addOneError("人员状态获取失败");
//			return false;
//		}

		if (mLPDiskImportSchema.getGrpContNo() == null
				|| "".equals(mLPDiskImportSchema.getGrpContNo())) {
			mErrors.addOneError("团体保单号获取失败");
			return false;
		}

		if (mLPDiskImportSchema.getContPlanCode() == null
				|| "".equals(mLPDiskImportSchema.getContPlanCode())) {
			mErrors.addOneError("保障计划获取失败");
			return false;
		}

		if (mLPDiskImportSchema.getSerialNo() == null
				|| "".equals(mLPDiskImportSchema.getSerialNo())) {
			mErrors.addOneError("序列号获取失败");
			return false;
		}

		if (mLPDiskImportSchema.getImportFileName() == null
				|| "".equals(mLPDiskImportSchema.getImportFileName())) {
			mErrors.addOneError("导入文件名获取失败");
			return false;
		}

		LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
		tLPDiskImportDB.setGrpContNo(mLPDiskImportSchema.getGrpContNo());
		tLPDiskImportDB.setSerialNo(mLPDiskImportSchema.getSerialNo());
		tLPDiskImportDB.setEdorType("WS");
		tLPDiskImportDB.setState("1");
		if (tLPDiskImportDB.query().size() > 0) {
			mErrors.addOneError("上次操作异常，请与核心系统运维人员联系！");
			return false;
		}

		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		String tGrpContSQL = "select * from lcgrpcont where grpcontno='"
				+ mLPDiskImportSchema.getGrpContNo()
				+ "' and managecom like '"
				+ mGlobalInput.ManageCom
				+ "%' and appflag = '1' and (stateflag is null or stateflag not in('0'))";
		LCGrpContSet tLCGrpContSet = tLCGrpContDB.executeQuery(tGrpContSQL);
		if (tLCGrpContSet.size() <= 0) {
			mErrors.addOneError("实名化团体保单查询失败");
			return false;
		}
		mLCGrpContSchema = tLCGrpContSet.get(1);

		if ("2".equals(mLCGrpContSchema.getCardFlag())) {
			mErrors.addOneError("该保单为卡折业务，不能进行实名化");
			return false;
		}

		if (0 == mLCGrpContSchema.getPrintCount()) {
			mErrors.addOneError("保单未打印，不能进行实名化操作");
			return false;
		}

		String sql = "select * from LJSPay " + "where OtherNoType = '1' " + // 1是团单续期
				"and OtherNo = '" + mLCGrpContSchema.getGrpContNo() + "' ";
		LJSPayDB tLJSPayDB = new LJSPayDB();
		LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(sql);
		if (tLJSPaySet.size() > 0) {
			mErrors.addOneError("该保单处于续期待收费状态，续期核销之后才能进行实名化");
			return false;
		}

		String tBQSQL = "select b.* from LPEdorApp a, LPGrpEdorItem b "
				+ "where  a.EdorAcceptNo = b.EdorAcceptNo "
				+ "and a.EdorState != '0' " +
				// "and b.EdorType = 'WS' " +
				"and b.GrpContNo = '" + mLCGrpContSchema.getGrpContNo() + "' ";
		LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
		LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB
				.executeQuery(tBQSQL);
		if (tLPGrpEdorItemSet.size() > 0) {
			mErrors.addOneError("保单" + mLCGrpContSchema.getGrpContNo()
					+ "下有未结案的" + tLPGrpEdorItemSet.get(1).getEdorType()
					+ "项目受理，受理号为" + tLPGrpEdorItemSet.get(1).getEdorNo()
					+ "，该工单结案后才能继续受理。");

			return false;
		}

		String tDQSQL = "select state from lcgrpbalplan where grpcontno = '"
				+ mLCGrpContSchema.getGrpContNo() + "'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tDQSSRS = tExeSQL.execSQL(tDQSQL);
		if (tDQSSRS.getMaxRow() > 0) {
			if (!"0".equals(tDQSSRS.GetText(1, 1))) {
				mErrors.addOneError("该保单正在进行定期结算，不能进行客户实名化");
				return false;
			}
		}

		String tWSSQL = "select 1 from LMRiskEdoritem  a, LMEdorItem b "
				+ " where a.edorCode = b.edorCode and b.edorcode != 'XB'"
				+ " and a.riskCode in"
				+ " (select riskCode from LCGrpPol where grpContNo = '"
				+ mLCGrpContSchema.getGrpContNo()
				+ "') and (b.edorTypeFlag != 'N'or b.edorTypeFlag is null)"
				+ " and b.edorcode = 'WS'";
		SSRS tWSSSRS = tExeSQL.execSQL(tWSSQL);
		if (tWSSSRS.getMaxRow() <= 0) {
			if (!"0".equals(tDQSSRS.GetText(1, 1))) {
				mErrors.addOneError("该保单下没有可以进行实名化的保单");
				return false;
			}
		}

		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		tLPGrpEdorItemSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
		tLPGrpEdorItemSchema.setEdorNo(mLCGrpContSchema.getGrpContNo());
		tLPGrpEdorItemSchema.setEdorAcceptNo(mLCGrpContSchema.getGrpContNo());
		tLPGrpEdorItemSchema.setEdorType("WS");

		CheckEdorItem tCheckEdorItem = new CheckEdorItem();
		tCheckEdorItem.setContType(BQ.CONTTYPE_G);
		tCheckEdorItem.setManageCom(mGlobalInput.ManageCom);
		tCheckEdorItem.setGrpContNo(mLCGrpContSchema.getGrpContNo());
		tCheckEdorItem.setEdorType("WS");
		tCheckEdorItem.setLPGrpEdorItem(tLPGrpEdorItemSchema);
		if (!tCheckEdorItem.submitData()) {
			mErrors.addOneError(tCheckEdorItem.mErrors.getFirstError());
			return false;
		}

		VData inputCheckData = new VData();
		inputCheckData.add(mGlobalInput);
		inputCheckData.add(tLPGrpEdorItemSchema);
		inputCheckData.add("VERIFY||BEGIN");
		inputCheckData.add("GEDORINPUT#EDORTYPE");

		CheckFieldBL tCheckFieldBL = new CheckFieldBL();
		if (!tCheckFieldBL.submitData(inputCheckData, "")) {
			mErrors.addOneError(tCheckFieldBL.mErrors.getFirstError());
			return false;
		}

		LDCodeDB tLDCodeDB = new LDCodeDB();
		tLDCodeDB.setCodeType("TJAccident");
		//tLDCodeDB.setCode("GrpContNo");
		tLDCodeDB.setCodeAlias(mLCGrpContSchema.getGrpContNo());
		tLDCodeDB.setComCode(mLCGrpContSchema.getManageCom());
		if (tLDCodeDB.query().size() <= 0) {
			DisabledManageBL tDisabledManageBL = new DisabledManageBL();
			if (!tDisabledManageBL.dealDisabledcont(
					tLPGrpEdorItemSchema.getGrpContNo(),
					tLPGrpEdorItemSchema.getEdorType(), 2)) {
				mErrors.addOneError("保单"
						+ tLPGrpEdorItemSchema.getGrpContNo()
						+ "下"
						+ tLPGrpEdorItemSchema.getEdorType()
						+ "项目在"
						+ CommonBL.getCodeName("stateflag",
								tDisabledManageBL.getState()) + "状态下不能添加!");
				return false;
			}
		}

		LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
		tLCInsuredSchema.setGrpContNo(mLPDiskImportSchema.getGrpContNo());
		tLCInsuredSchema.setName(mLPDiskImportSchema.getInsuredName());
		tLCInsuredSchema.setSex(mLPDiskImportSchema.getSex());
		tLCInsuredSchema.setBirthday(mLPDiskImportSchema.getBirthday());
		tLCInsuredSchema.setIDType(mLPDiskImportSchema.getIDType());
		tLCInsuredSchema.setIDNo(mLPDiskImportSchema.getIDNo());
		OldCustomerCheck tOldCustomerCheck = new OldCustomerCheck(
				tLCInsuredSchema);

		if (tOldCustomerCheck.checkInsured() == OldCustomerCheck.OLD) {
			mErrors.addOneError("该保单下已存在该被保人！");
			return false;
		}

		return true;
	}

	/**
	 * 业务处理
	 * 
	 * @return 处理成功标志
	 * @throws Exception
	 */
	private boolean dealData() throws Exception {
		if (!importData()) {
			return false;
		}

		if (!claim()) {
			updateState();
			return false;
		}

		if (!confirm()) {
			return false;
		}
		return true;
	}

	/**
	 * 保存数据
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	private boolean importData() throws Exception {
		System.out.println("LLCustomerWS--importData");
		String tLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
		System.out.println("管理机构代码是 : " + tLimit);
		String tHWNo = PubFun1.CreateMaxNo("HWNO", tLimit);
		System.out.println("HWNo:" + tHWNo);
		mLPDiskImportSchema.setEdorNo(tHWNo);
		mLPDiskImportSchema.setEdorType("WS");
		mLPDiskImportSchema.setState("1");

		// 默认是本人
		if (mLPDiskImportSchema.getEmployeeName() == null
				|| "".equals(mLPDiskImportSchema.getEmployeeName())) {
			mLPDiskImportSchema.setEmployeeName(mLPDiskImportSchema
					.getInsuredName());
		}

		if (mLPDiskImportSchema.getRetire() == null
				|| "".equals(mLPDiskImportSchema.getRetire())) {
			mLPDiskImportSchema.setRetire("1");
		}

		if (mLPDiskImportSchema.getEdorValiDate() == null
				|| "".equals(mLPDiskImportSchema.getEdorValiDate())) {
			mLPDiskImportSchema
					.setEdorValiDate(mLCGrpContSchema.getCValiDate());
		}

		mLPDiskImportSchema.setOperator(mGlobalInput.Operator);
		mLPDiskImportSchema.setMakeDate(mCurrentDate);
		mLPDiskImportSchema.setMakeTime(mCurrentTime);
		mLPDiskImportSchema.setModifyDate(mCurrentDate);
		mLPDiskImportSchema.setModifyTime(mCurrentTime);

		MMap tMap = new MMap();
		tMap.put(mLPDiskImportSchema, "INSERT");

		VData tVData = new VData();
		tVData.clear();
		tVData.add(tMap);

		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(tVData, null)) {
			mErrors.addOneError("数据库保存失败！");
			return false;
		}
		return true;
	}

	/**
	 * 调用保全理算类
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	private boolean claim() throws Exception {
		System.out.println("LLCustomerWS--claim");
		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		tLPGrpEdorItemSchema.setEdorNo(mLPDiskImportSchema.getEdorNo());
		tLPGrpEdorItemSchema.setEdorType(mLPDiskImportSchema.getEdorType());
		tLPGrpEdorItemSchema.setGrpContNo(mLPDiskImportSchema.getGrpContNo());

		mVData.add(mGlobalInput);
		mVData.add(tLPGrpEdorItemSchema);

		GrpEdorWSAppConfirmBL tGrpEdorWSAppConfimBL = new GrpEdorWSAppConfirmBL();
		if (!tGrpEdorWSAppConfimBL.submitData(mVData, "")) {
			mErrors.addOneError(tGrpEdorWSAppConfimBL.mErrors.getFirstError());
			return false;
		}
		return true;
	}

	/**
	 * 理算失败维护
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	private boolean updateState() throws Exception {
		System.out.println("LLHospCustomerWS--updateState");
		String tUpdateSQL = "update LPDiskImport " + "set State = '0', "
				+ " ErrorReason = '" + mErrors.getFirstError() + "' "
				+ "where EdorNo = '" + mLPDiskImportSchema.getEdorNo() + "' "
				+ "and EdorType = 'WS' " + "and GrpContNo = '"
				+ mLPDiskImportSchema.getGrpContNo() + "' "
				+ "and SerialNo = '" + mLPDiskImportSchema.getSerialNo() + "' ";
		MMap tMap = new MMap();
		tMap.put(tUpdateSQL, "UPDATE");

		VData tVData = new VData();
		tVData.clear();
		tVData.add(tMap);

		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(tVData, null)) {
			mErrors.addOneError("数据库保存失败！");
			return false;
		}
		return true;
	}

	/**
	 * 调用保全确认
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	private boolean confirm() throws Exception {
		System.out.println("LLHospCustomerWS--confirm");
		GrpEdorWSConfirmBL tGrpEdorWSConfirmBL = new GrpEdorWSConfirmBL();
		if (!tGrpEdorWSConfirmBL.submitData(mVData, "")) {
			mErrors.addOneError(tGrpEdorWSConfirmBL.mErrors.getFirstError());
			return false;
		}

		return true;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LDCodeDB tLDCodeDB = new LDCodeDB();
		tLDCodeDB.setCodeType("TJAccident");
		//tLDCodeDB.setCode("GrpContNo");
		tLDCodeDB.setCodeAlias("00003935000001");
		tLDCodeDB.setComCode("86120000");
		System.out.print(tLDCodeDB.query().size());
//		if (tLDCodeDB.query().size() <= 0) {
//			
//		}

	}

}
