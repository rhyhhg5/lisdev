package com.sinosoft.lis.llcase;

import java.io.*;

import org.w3c.dom.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Excel文件解析类</p>
 * <p>Description: 把数据从磁盘中的XML文件导入，存到相应的类中 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author Xx
 * @version 1.0
 */

public class LLImportFileNew {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 保单信息在xml中的结点 */
    private static final String PARSE_PATH = "/DATASET";
    /** 配置文件名 */
    private String Config_File_Name = "LLSCCaseImport.xml";
    /** 文件路径 */
    private String path;
    /** Excel文件名 */
    private String fileName;
    private String mRgtNo;
    /** 批次版本 */
    private String VersionType="";
    /** 解析配置文件XML路径 */
    private String XMLPath = "";
    
    private GlobalInput mG = new GlobalInput();
    private String[] mTitle = null;
    private SIParser tCaseParser ;
    private String DistrictSign = "";

    /**
     * 构造函数
     * @param path String
     * @param fileName String
     */
    public LLImportFileNew(String path, String fileName,String aRgtNo,
    		GlobalInput aGlobalInput,String aImportFlag,String aVersionType,String aXMLPath) {
        this.path = path;
        this.fileName = fileName;
        this.mRgtNo = aRgtNo;
        this.mG = aGlobalInput;
        this.VersionType = aVersionType;
        this.XMLPath = aXMLPath;
        
        String tOtherSign = mRgtNo.substring(1,3);

        //由“理赔处理->受理申请（团体）”进入的导入标志，目前只有ImportFlag = "PR"
        if (aImportFlag!=null&&!"".equals(aImportFlag)) {
            tOtherSign = aImportFlag;
        }

        String SQL="select code,codealias from ldcode1 where codetype='llimport' "
        	+" and code1='SI' and othersign='"+tOtherSign+"'";
        ExeSQL texesql = new ExeSQL();
        System.out.println(SQL);
        SSRS tss = texesql.execSQL(SQL);
        if(tss.getMaxRow()>0){
        	DistrictSign = tss.GetText(1, 1);
        	Config_File_Name = tss.GetText(1, 2);
//          add by lyc #2026 2014-07-24  目前只是调整青岛若以后有其他机构再作调整不能写死
            String tSocialSql = "select CHECKGRPCONT((select RgtObjNo from LLRegister where 1=1 and rgtno ='"+mRgtNo+"' )) from dual where 1=1  ";
    		ExeSQL tExeSQL = new ExeSQL();
    		String tResult = tExeSQL.getOneValue(tSocialSql);
    		System.out.println("--------" +DistrictSign+ "");
    		System.out.println("++++++++" +Config_File_Name+ "");
    		if("94".equals(DistrictSign)){
    			if("Y".equals(tResult)){
    				DistrictSign="QD";
    				Config_File_Name = "LLQDCaseImport.xml";
    			}
    			
    		}else if("QD".equals(DistrictSign)){
    			if("N".equals(tResult)){
    				DistrictSign="94";
    				Config_File_Name = "LL94CaseImport.xml";
    			}
    		}
//        	add lyc
        	//若由“理赔处理->受理申请（团体）”导入，走新的处理类
        	if("PR".equals(DistrictSign)){
        		DistrictSign = "NewPR";
        	}
        }
    }

    /**
     * 从Excel文件导入数据，保存存到List中<br>
     * 先把数据保存到多个xml文件中，然后从xml读出存到VData中
     * @return VData 保存数据的容器
     */
    public VData doImport() {
        String[] xmlFiles = parseVts(); //解析Excel到xml
        if (xmlFiles == null) {//解析失败,将失败的标记记录到VData
        	TransferData tTransferData = new TransferData();
        	VData data = new VData();
        	tTransferData.setNameAndValue("Error-parseVts()", "Excel转换XML错误，请检查导入模板或选择正确模板版本");
        	data.add(tTransferData);
            return data;
        }

        VData data = new VData();

        data=parseXml(xmlFiles[0]); //解析xml到VData
        return data;
    }

    /**
     * 得到Excel文件路径
     * @return String
     */
    private String getFilePath() {
        return path + fileName;
    }

    /**
     * 得到配置文件路径
     * @return String
     */
    private String getConfigFilePath() {
        return XMLPath + Config_File_Name;
    }

    /**
     * 解析excel并转换成多个xml文件
     * @return String[] 存放xml文件名的数组
     */
    private String[] parseVts() {
        SIXmlParser rgtvp = new SIXmlParser();
        rgtvp.setFileName(getFilePath());
        rgtvp.setConfigFileName(getConfigFilePath());
        //转换excel到xml,版本必须为Excel 97-2003 工作薄（*.xls）
        if (!rgtvp.transform()) {
            mErrors.copyAllErrors(rgtvp.mErrors);
            CError.buildErr(this, "Excel转换XML错误，请检查导入模板或选择正确模板版本");
            return null;
        }
        mTitle = rgtvp.getTitle();
        return rgtvp.getDataFiles();
    }

    private VData parseXml(String xmlFileName) {
    	 
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if (!tLLRegisterDB.getInfo()) {
            CError.buildErr(this,"磁盘导入失败，未查到团体批次信息！");
        }

        try {
            System.out.println("调用处理类标识：DistrictSign="+DistrictSign);
            Class tParserClass = Class.forName(
                    "com.sinosoft.lis.llcase." + DistrictSign + "SCParser");
                    System.out.println(tParserClass);
            if("NewPR".equals(DistrictSign)){
            	tCaseParser = new NewPRSCParser(path+fileName,getConfigFilePath());           	
            }else if("QD".equals(DistrictSign)){
            	//#2115 青岛大额业务批量处理优化-青岛社保大额导入
            	tCaseParser = new QDSCParserNew(path+fileName,getConfigFilePath());  
            }else{//等待废除中
            	tCaseParser = (SIParser) tParserClass.newInstance();
            }            
        } catch (ClassNotFoundException ex) {
            System.out.println(
                    "未找到特别配置的解析类，标识为： " + DistrictSign + "SCParser实例化错误");
            System.out.println("准备调用公共的解析类SCParser");
            
            if("1".equals(VersionType)){
            	//调用通用的导入处理类，此处为新解析类的构造器
                tCaseParser = new NewSCParser(path+fileName,getConfigFilePath());	
            }else if("0".equals(VersionType)){
            	//原有的导入处理类
            	tCaseParser = new SCParser();
            }           
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError.buildErr(this, "找不到合适的解析类！请联系开发人员。");
            return null;
        }
        String tsq4 = "INSERT INTO ldtimetest VALUES ('rgtType','批导','"+mRgtNo+"','解析导入类结束','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	  new ExeSQL().execUpdateSQL(tsq4);
  	  
        //因为对文件的归档，此处应该创建归档文件夹ErrorList
        File mFileDir = new File(XMLPath+"ErrorList/");
        if (!mFileDir.exists())
        {
            if (!mFileDir.mkdirs())
            {
            	CError.buildErr(this,"创建目录[" + XMLPath+"ErrorList/" + "]失败！" + mFileDir.getPath());
            	return null;
            }
        }
        
        tCaseParser.setParam(mRgtNo,tLLRegisterDB.getRgtObjNo(),XMLPath+"ErrorList/");
        tCaseParser.setGlobalInput(mG);
        tCaseParser.setFormTitle(mTitle,DistrictSign+"ClaimErrList.vts");
        VData tVData = new VData();
        try {
        	
            //解析保单信息
            XMLPathTool xmlPT = new XMLPathTool(xmlFileName);
            NodeList nodeList = xmlPT.parseN(PARSE_PATH);
            System.out.println("长度 ： " + nodeList.getLength());
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                NodeList contNodeList = node.getChildNodes();
                System.out.println("长度 ： " + contNodeList.getLength());
                if (contNodeList.getLength() <= 0)
                    continue;
                for (int j = 0; j < contNodeList.getLength(); j++) {

                    Node contNode = contNodeList.item(j);
                    String nodeName = contNode.getNodeName();
                    if (nodeName.equals("#text")) {
                    }
                    //解析被保险人
                    else if (nodeName.equals("CASETABLE")) {	
                        tVData = tCaseParser.parseOneInsuredNode(contNode);
                    }
                }
            }

            //解析完成删除xml临时文件
            File xmlFile = new File(xmlFileName);
            xmlFile.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tVData;
    }


    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        tG.ComCode = "8694";
        tG.Operator = "cm9402";
        tG.ManageCom = "8694";
        LLImportFileNew tLLimportFile = new LLImportFileNew(
                "D:\\DEVELOP\\ui\\temp_lp", "data.xls","P9400060925000001",tG,"01","1","D:\\DEVELOP\\ui\\temp_lp\\");
        tLLimportFile.doImport();
    }
}
