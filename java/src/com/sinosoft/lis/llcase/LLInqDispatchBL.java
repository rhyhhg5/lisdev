/*
 * @(#)ICaseCureBL.java	2005-02-20
 *
 * Copyright 2005 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.llcase;

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;

/*
 * <p>Title: Web业务系统</p>
 * <p>ClassName:ICaseCureBL </p>
 * <p>Description: 理赔案件-调查分配 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft </p>
 * @author：zhangtao
 * @version：1.0
 * @CreateDate：2005-02-20
 */
public class LLInqDispatchBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private MMap map = new MMap();
    private String mOperate = "";
    ExeSQL tExeSQL = new ExeSQL();
    //全局数据
    private GlobalInput mGlobalInput = new GlobalInput();
    //账单费用明细
    private LLInqApplySet mLLInqApplySet= new LLInqApplySet();
    private LLInqApplySchema mLLInqApplySchema = new LLInqApplySchema();
    private LLSurveySchema mLLSurveySchema = new LLSurveySchema();
    private LLSurveyDispatchSchema mLLSurveyDispatchSchema = new LLSurveyDispatchSchema();
    
    //调查指导意见
    private String mRemark = "";
    
    /** 社保保单标志 0：非社保；1：社保*/
    private String mSocialSecurity = "0";

    public LLInqDispatchBL()
    {
    }

    public static void main(String[] args)
    {
        LLInqApplySchema tLLInqApplySchema   = new LLInqApplySchema();
        LLInqDispatchBL tLLInqDispatchBL   = new LLInqDispatchBL();
        tLLInqApplySchema.setOtherNo("C2100060815000010");
        tLLInqApplySchema.setOtherNoType("1");
        tLLInqApplySchema.setSurveyNo("C21000608150000101");
        tLLInqApplySchema.setInqNo("C210006081500001011");
        tLLInqApplySchema.setLocFlag("0");
        tLLInqApplySchema.setSurveySite("");
        tLLInqApplySchema.setInqDept("86210000");
        tLLInqApplySchema.setInqPer("cm2102");

        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "cm2101";
        tGI.ManageCom = "86210000";
        VData tVData = new VData();
        tVData.add(tLLInqApplySchema);
        tVData.add(tGI);
        tLLInqDispatchBL.submitData(tVData,"DISPATCH");
        //用于调试
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData 输入数据
     * @param cOperate String 操作类型
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        mOperate=cOperate;

        //得到输入数据
        if (!getInputData())
        {
            return false;
        }

        //检查数据合法性
        if (!checkInputData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData(cOperate))
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLInqFeeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有数据对象
     * @return boolean
     */
    private boolean getInputData()
    {
        System.out.println("getInputData()..."+mOperate);

        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLLInqApplySchema = (LLInqApplySchema) mInputData.getObjectByObjectName(
                "LLInqApplySchema", 0);
        if (mLLInqApplySchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLInqDispatchBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认有：调查消息!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 对输入数据做必要的校验
     * @return boolean
     */
    private boolean checkInputData()
    {
        System.out.println("checkInputData()..."+mOperate);
        String sHandler = mGlobalInput.Operator;
        //modify by Houyd 社保用户可以分配调查案件
        String sql = "select surveyflag from llclaimuser where usercode='"+sHandler+"' "+
        	"union select surveyflag from llsocialclaimuser where usercode='"+sHandler+"'";
        ExeSQL es = new ExeSQL();
        String SurveyFlag = es.getOneValue(sql);
        if(!"1".equals(SurveyFlag)){
            CError tError = new CError();
            tError.moduleName = "LLIndirectFeeBL";
            tError.functionName = "checkInputData";
            tError.errorMessage = "对不起，您不是调查主管，没有权限进行这项操作!" ;
            mErrors.addOneError(tError);
            return false;
        }
        String asql = "select surveyflag from llsurvey where surveyno='"+mLLInqApplySchema.getSurveyNo()+"'";
        ExeSQL aExeSQLs = new ExeSQL();
        String aSurveyFlag = aExeSQLs.getOneValue(asql);
        if("".equals(aSurveyFlag) || null == aSurveyFlag || Integer.parseInt(aSurveyFlag) >= 2)
        {
        	CError tError = new CError();
            tError.moduleName = "LLIndirectFeeBL";
            tError.functionName = "checkInputData";
            tError.errorMessage = "已经调查回复的案件不能重新操作下发或上报!" ;
            mErrors.addOneError(tError);
            return false;
        }
        
        String tCaseNo = mLLInqApplySchema.getOtherNo();
        if("R".equals(tCaseNo.substring(0,1)) || "S".equals(tCaseNo.substring(0,1)))
    	{
    		System.out.println("申诉纠错案件换为正常的C案件");
            String tSql = "select caseno from LLAppeal where appealno ='"+tCaseNo+"'";
            ExeSQL exeSQL = new ExeSQL();
            tCaseNo = exeSQL.getOneValue(tSql);
    	}
        mSocialSecurity = LLCaseCommon.checkSocialSecurity(tCaseNo, null);
        if("1".equals(mSocialSecurity) ||
        		(("sc".equals(mGlobalInput.Operator.substring(0, 2))
        				||"ss".equals(mGlobalInput.Operator.substring(0, 2))
        				||"si".equals(mGlobalInput.Operator.substring(0, 2)))
        		  &&("T".equals(mLLInqApplySchema.getOtherNo().substring(0, 1))
        		  		|| "Z".equals(mLLInqApplySchema.getOtherNo().substring(0, 1)))
        		)
        ){
        	//运营部用户不能操作社保部调查案件
        	LLSocialClaimUserDB tLLSocialClaimUserDB = new LLSocialClaimUserDB();
        	tLLSocialClaimUserDB.setUserCode(sHandler);
        	if(!tLLSocialClaimUserDB.getInfo()){
        		CError tError = new CError();
                tError.moduleName = "LLIndirectFeeBL";
                tError.functionName = "checkInputData";
                tError.errorMessage = "运营部用户不能操作社保部调查案件!" ;
                mErrors.addOneError(tError);
                return false;
        	}
        	
        	String tSQL = "select * from llsocialclaimuser where stateflag='1' ";
        	String tPartSql = "";
        	LLSocialClaimUserDB aLLSocialClaimUserDB = new LLSocialClaimUserDB();
			LLSocialClaimUserSet tSocialClaimUserSet = new LLSocialClaimUserSet();
        	
        	String tSubFlag = mLLInqApplySchema.getSubFlag();
            if ("0".equals(tSubFlag)) {//社保调查只能下发给配置过社保权限的人       		
        		if ("86".equals(mGlobalInput.ManageCom)) {//86机构登录时，下发给分公司社保部调查主管
        			String tDealer = mLLInqApplySchema.getDipatcher();//分公司社保部调查主管
        			tPartSql = " and (surveyflag='1' or surveyflag='2') and usercode='"+tDealer+"'";    
        			tSocialClaimUserSet = aLLSocialClaimUserDB.executeQuery(tSQL+tPartSql);
        			System.out.println("TSQL:"+tSQL+tPartSql);
        			if(tSocialClaimUserSet.size() <= 0){
        				CError tError = new CError();
        	            tError.moduleName = "LLInqDispatchBL";
        	            tError.functionName = "checkInputData";
        	            tError.errorMessage = "社保调查不能下发给未进行社保权限配置的调查用户" ;
        	            mErrors.addOneError(tError);
        	            return false;
        			}
        		} else {//普通机构登录时，下发给分或支机构的调查主管或调查员
        			String tSurveyer = mLLInqApplySchema.getInqPer();
        			tPartSql = " and (surveyflag='1' or surveyflag='2') and usercode='"+tSurveyer+"'";
        			tSocialClaimUserSet = aLLSocialClaimUserDB.executeQuery(tSQL+tPartSql);
        			System.out.println("TSQL:"+tSQL+tPartSql);
        			if(tSocialClaimUserSet.size() <= 0){
        				CError tError = new CError();
        	            tError.moduleName = "LLInqDispatchBL";
        	            tError.functionName = "checkInputData";
        	            tError.errorMessage = "社保调查不能下发给未进行社保权限配置的用户" ;
        	            mErrors.addOneError(tError);
        	            return false;
        			}
        		}
        	} else if ("1".equals(tSubFlag)) {//上报只能上报给配置过社保的社保部调查主管
        		String tSurveyer = mLLInqApplySchema.getDipatcher();
    			tPartSql = " and handleflag='1' and surveyflag='1' and usercode='"+tSurveyer+"'";
    			tSocialClaimUserSet = aLLSocialClaimUserDB.executeQuery(tSQL+tPartSql);
    			System.out.println("TSQL:"+tSQL+tPartSql);
    			if(tSocialClaimUserSet.size() <= 0){
    				CError tError = new CError();
    	            tError.moduleName = "LLInqDispatchBL";
    	            tError.functionName = "checkInputData";
    	            tError.errorMessage = "社保调查不能上报给未进行社保权限配置的非社保部调查主管" ;
    	            mErrors.addOneError(tError);
    	            return false;
    			}
        	}
        }else{//非社保的调查       	
        	//社保部用户不能操作运营部调查案件
        	LLClaimUserDB tClaimUserDB = new LLClaimUserDB();
        	tClaimUserDB.setUserCode(sHandler);
        	if(!tClaimUserDB.getInfo()){
        		CError tError = new CError();
                tError.moduleName = "LLIndirectFeeBL";
                tError.functionName = "checkInputData";
                tError.errorMessage = "社保部用户不能操作运营部调查案件!" ;
                mErrors.addOneError(tError);
                return false;
        	}
        	
        	String tOperator = "";
        	if("".equals(mLLInqApplySchema.getDipatcher()) || mLLInqApplySchema.getDipatcher()==null){
        		tOperator = mLLInqApplySchema.getInqPer();
        	}else{
        		tOperator = mLLInqApplySchema.getDipatcher();
        	}
        	LLSocialClaimUserDB aLLSocialClaimUserDB = new LLSocialClaimUserDB();
			LLSocialClaimUserSet tSocialClaimUserSet = new LLSocialClaimUserSet();
			aLLSocialClaimUserDB.setUserCode(tOperator);
			tSocialClaimUserSet = aLLSocialClaimUserDB.query();
			if(tSocialClaimUserSet.size() > 0){
				CError tError = new CError();
	            tError.moduleName = "LLInqDispatchBL";
	            tError.functionName = "checkInputData";
	            tError.errorMessage = "普通调查不能分配给社保权限配置的调查用户" ;
	            mErrors.addOneError(tError);
	            return false;
			}
        }       
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @param cOperate String 操作类型
     * @return boolean
     */
    private boolean dealData(String cOperate)
    {
        System.out.println("dealData()..."+mOperate);
        LLSurveyDB tLLSurveyDB = new LLSurveyDB();
        LLSurveySet tLLSurveySet = new LLSurveySet();
        
        String SurveyNo = mLLInqApplySchema.getSurveyNo();
        if(SurveyNo == null||"null".equals(SurveyNo)||"".equals(SurveyNo))
        {
        	CError.buildErr(this, "传入的调查号为空");
            return false;
        }
        
    	tLLSurveyDB.setSurveyNo(mLLInqApplySchema.getSurveyNo());
        tLLSurveySet.set(tLLSurveyDB.query());
        mLLSurveySchema.setSchema(tLLSurveySet.get(1));
        
        if (!DispatchTrace()) {
        	CError.buildErr(this, "调查轨迹生成失败");
            return false;
        }
        
        boolean tReturn = false;
        if (!makeInqNo())
        {
            CError.buildErr(this, "调查项目生成失败");
            return false;
        }
        //保存录入
        if (mOperate.equals("INSERT"))
        {
            mLLInqApplySchema.setInqState("0");
            mLLInqApplySchema.setConPer(mGlobalInput.Operator);
            mLLInqApplySchema.setInqStartDate(PubFun.getCurrentDate());
            mLLInqApplySchema.setOperator(mGlobalInput.Operator);
            mLLInqApplySchema.setMakeDate(PubFun.getCurrentDate());
            mLLInqApplySchema.setMakeTime(PubFun.getCurrentTime());
            mLLInqApplySchema.setModifyDate(PubFun.getCurrentDate());
            mLLInqApplySchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLLInqApplySchema, "INSERT");
            tReturn = true;
        }

        //修改录入
        if (mOperate.equals("UPDATE"))
        {
            mLLInqApplySchema.setInqState("0");
            mLLInqApplySchema.setConPer(mGlobalInput.Operator);
            mLLInqApplySchema.setInqStartDate(PubFun.getCurrentDate());
            mLLInqApplySchema.setModifyDate(PubFun.getCurrentDate());
            mLLInqApplySchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLLInqApplySchema, "UPDATE");
            tReturn = true;
        }

        //删除录入
        if (cOperate.equals("DELETE"))
        {
            String sbSql = "";
            String sbSql1="";

    map.put(sbSql, "DELETE");
    map.put(sbSql1, "DELETE");
            tReturn = true;
        }
        return tReturn;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        System.out.println("prepareOutputData()...");

        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ICaseCureBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 得到处理后的结果数据
     * @return boolean
     */
    public VData getResult()
    {
        return this.mResult;
    }

    public boolean makeInqNo()
    {
        LLInqApplyDB tLLInqApplyDB = new LLInqApplyDB();
        LLInqApplySet tLLInqApplySet = new LLInqApplySet();
        tLLInqApplyDB.setSurveyNo(mLLInqApplySchema.getSurveyNo());
        tLLInqApplyDB.setInqNo(mLLInqApplySchema.getInqNo());
        if(!tLLInqApplyDB.getInfo())
        {
            CError.buildErr(this, "调查项目查询失败");
            return false;
        }else{
//            if (tLLInqApplyDB.getInqPer() != null && !tLLInqApplyDB.equals("")) {
//                if (!tLLInqApplyDB.getInqPer().equals(mLLInqApplySchema.
//                        getInqPer())) {
//                   tLLInqApplySet=tLLInqApplyDB.query();
//                    int   index = tLLInqApplySet.size() + 1;
//                    String InqNo = mLLInqApplySchema.getOtherNo() + index;
//                    mLLInqApplySchema.setInqNo(InqNo);
//                    mOperate = "INSERT";
//
//                } else {
//                    mOperate = "UPDATE";
//                }
//            } else {
                if (mLLInqApplySchema.getDipatcher() == null ||
                    mLLInqApplySchema.getDipatcher().equals("")) {
                    mLLInqApplySchema.setDipatcher(tLLInqApplyDB.getDipatcher());
                }
                mLLInqApplySchema.setOperator(tLLInqApplyDB.getOperator());
                mLLInqApplySchema.setMakeDate(tLLInqApplyDB.getMakeDate());
                mLLInqApplySchema.setMakeTime(tLLInqApplyDB.getMakeTime());
                mOperate = "UPDATE";
//            }
            mRemark = tLLInqApplyDB.getRemark();

            if (!"".equals(mLLInqApplySchema.getRemark())
            		&&mLLInqApplySchema.getRemark()!=null) {
            	if (!"".equals(mRemark) && !"null".equals(mRemark) && mRemark!=null ) {
            		String tSeparator = StrTool.unicodeToGBK(System.getProperty("line.separator"));
            		mRemark = mRemark+tSeparator;
            	} else {
            		mRemark = "";
            	}
            	
            	String tTitle = "";
            	if ("86".equals(mGlobalInput.ComCode)) {
            		tTitle = "总公司调查管理员"+mGlobalInput.Operator+":";
            	} else {
            		LDComDB tLDComDB = new LDComDB();
            		tLDComDB.setComCode(mGlobalInput.ComCode);
            		if (!tLDComDB.getInfo()) {
            			CError.buildErr(this, "登陆信息查询失败");
            			return false;
            		}
            		tTitle = tLDComDB.getName()+"调查主管"+mGlobalInput.Operator+":";
            	}
            	mRemark = mRemark+tTitle+StrTool.unicodeToGBK(mLLInqApplySchema.getRemark());
            }
            mLLInqApplySchema.setRemark(mRemark);
        }
        mLLInqApplySchema.setInqDesc(tLLInqApplyDB.getInqDesc());
        return true;
    }
    
    /**
     * 生成调查轨迹
     * @return
     */
    private boolean DispatchTrace() {
    	       
        String tSubFlag = mLLInqApplySchema.getSubFlag();        
        if (!"".equals(tSubFlag)) {
        	String tSurveyDispatchNo = PubFun1.CreateMaxNo("SURVEYDISPATCH", mLLSurveySchema.getMngCom());
        	LLSurveyDispatchSchema tLLSurveyDispatchSchema = new LLSurveyDispatchSchema();
        	tLLSurveyDispatchSchema.setDispatchNo(tSurveyDispatchNo);
        	tLLSurveyDispatchSchema.setOtherNo(mLLSurveySchema.getOtherNo());
        	tLLSurveyDispatchSchema.setOtherNoType(mLLSurveySchema.getOtherNoType());
        	tLLSurveyDispatchSchema.setSurveyNo(mLLSurveySchema.getSurveyNo());
        	tLLSurveyDispatchSchema.setInqNo(mLLInqApplySchema.getInqNo());
        	tLLSurveyDispatchSchema.setDispatcher(mGlobalInput.Operator);
        	tLLSurveyDispatchSchema.setDispatchType(tSubFlag);
        	
        	if ("0".equals(tSubFlag)) {//下发
        		tLLSurveyDispatchSchema.setOldOperator(mGlobalInput.Operator);
        		if ("86".equals(mGlobalInput.ManageCom)) {       			
        			tLLSurveyDispatchSchema.setReceiver(mLLInqApplySchema.getDipatcher());
        		} else {
        			tLLSurveyDispatchSchema.setReceiver(mLLInqApplySchema.getInqPer());
        		}
        	} else if ("1".equals(tSubFlag)) {//上报
        		tLLSurveyDispatchSchema.setOldOperator(mGlobalInput.Operator);
        		tLLSurveyDispatchSchema.setReceiver(mLLInqApplySchema.getDipatcher());
        	}
        	tLLSurveyDispatchSchema.setRemark(mLLInqApplySchema.getRemark());
        	tLLSurveyDispatchSchema.setManageCom(mGlobalInput.ManageCom);
        	tLLSurveyDispatchSchema.setOperator(mGlobalInput.Operator);
        	tLLSurveyDispatchSchema.setMakeDate(PubFun.getCurrentDate());
        	tLLSurveyDispatchSchema.setMakeTime(PubFun.getCurrentTime());
        	tLLSurveyDispatchSchema.setModifyDate(PubFun.getCurrentDate());
        	tLLSurveyDispatchSchema.setModifyTime(PubFun.getCurrentTime());
        	map.put(tLLSurveyDispatchSchema,"INSERT");
        }
    	return true;
    }
}
