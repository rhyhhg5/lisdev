package com.sinosoft.lis.llcase;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统投保暂交费功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author YT
 * @version 1.0
 */

public class ClaimUWReEnsureBLS
{
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();
  private String mOperate;

  public ClaimUWReEnsureBLS() {}


  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
      //首先将数据在本类中做一个备份
      mInputData = (VData) cInputData.clone();
      mOperate = cOperate;
      if (!saveData())
      {
          return false;
      }
      mInputData = null;
      return true;
  }

  private boolean saveData()
  {
  	LLClaimSchema tLLClaimSchema = new LLClaimSchema();
  	LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();
        LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
        LJSGetClaimSet tLJSGetClaimSet = new LJSGetClaimSet();
        LJSGetSchema tLJSGetSchema = new LJSGetSchema();
        LLClaimUWMainSchema tLLClaimUWMainSchema = new
                                                   LLClaimUWMainSchema();
        LLClaimUWMDetailSchema tLLClaimUWMDetailSchema = new
                LLClaimUWMDetailSchema();


        tLLClaimSchema =
            (LLClaimSchema)
            mInputData.getObjectByObjectName("LLClaimSchema",0);
        tLLClaimPolicySet =
            (LLClaimPolicySet)
            mInputData.getObjectByObjectName("LLClaimPolicySet",0);
        tLLClaimDetailSet =
            (LLClaimDetailSet)
            mInputData.getObjectByObjectName("LLClaimDetailSet",0);
        tLJSGetClaimSet =
            (LJSGetClaimSet)
            mInputData.getObjectByObjectName("LJSGetClaimSet",0);
        tLJSGetSchema =
            (LJSGetSchema)
            mInputData.getObjectByObjectName("LJSGetSchema", 0);
        tLLClaimUWMainSchema =
            (LLClaimUWMainSchema)
            mInputData.getObjectByObjectName("LLClaimUWMainSchema",0);
        tLLClaimUWMDetailSchema =
            (LLClaimUWMDetailSchema)
            mInputData.getObjectByObjectName("LLClaimUWMDetailSchema",0);

    	Connection conn = DBConnPool.getConnection();
    	if (conn == null)
    	{
      		// @@错误处理
      		CError tError = new CError();
      		tError.moduleName = "ClaimSaveBLS";
      		tError.functionName = "saveData";
      		tError.errorMessage = "数据库连接失败!";
      		this.mErrors .addOneError(tError) ;
      		return false;
    	}

    	try
    	{
                conn.setAutoCommit(false);
                // 案件核赔表
                LLClaimUWMainDB tLLClaimUWMainDB = new LLClaimUWMainDB(conn);
                tLLClaimUWMainDB.setClmNo(tLLClaimUWMainSchema.getClmNo());
                if (tLLClaimUWMainDB.deleteSQL() == false)
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLLClaimUWMainDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ClaimUWEnsureBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "LLClaimUWMain表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
                tLLClaimUWMainDB.setSchema(tLLClaimUWMainSchema);
                if (tLLClaimUWMainDB.insert() == false)
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLLClaimUWMainDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ClaimUWEnsureBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "LLClaimUWMain表保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }

                //案件核赔履历表
                LLClaimUWMDetailDB tLLClaimUWMDetailDB = new LLClaimUWMDetailDB(
                        conn);
                tLLClaimUWMDetailDB.setSchema(tLLClaimUWMDetailSchema);
                if (tLLClaimUWMDetailDB.insert() == false)
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLLClaimUWMDetailDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ClaimManChkBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "LLClaimUWMDetail表保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }

                if (tLLClaimUWMainSchema.getAppActionType().equals("1"))
                //如果签批确认，执行以下赔付数据操作
                {
                    //赔案表
                    LLClaimDB tLLClaimDB = new LLClaimDB(conn);
                    tLLClaimDB.setSchema(tLLClaimSchema);
                    if (tLLClaimDB.update() == false)
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLLClaimDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "ClaimSaveBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "保存数据失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }

                    //赔案保单明细表
                    LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB(conn);
                    tLLClaimPolicyDB.setClmNo(tLLClaimSchema.getClmNo());
                    tLLClaimPolicyDB.deleteSQL();
                    if (tLLClaimPolicyDB.mErrors.needDealError())
                    {
                        this.mErrors.copyAllErrors(tLLClaimPolicyDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "ClaimSaveBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "保存数据失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                    LLClaimPolicyDBSet
                            tLLClaimPolicyDBSet = new LLClaimPolicyDBSet(conn);
                    tLLClaimPolicyDBSet.set(tLLClaimPolicySet);
                    if (!tLLClaimPolicyDBSet.insert())
                    {
                        this.mErrors.copyAllErrors(tLLClaimPolicyDBSet.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "ClaimSaveBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "保存数据失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }

                    //赔付明细表
                    LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB(conn);
                    tLLClaimDetailDB.setClmNo(tLLClaimSchema.getClmNo());
                    tLLClaimDetailDB.deleteSQL();
                    if (tLLClaimDetailDB.mErrors.needDealError())
                    {
                        this.mErrors.copyAllErrors(tLLClaimDetailDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "ClaimSaveBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "保存数据失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                    LLClaimDetailDBSet
                            tLLClaimDetailDBSet = new LLClaimDetailDBSet(conn);
                    tLLClaimDetailDBSet.set(tLLClaimDetailSet);
                    if (!tLLClaimDetailDBSet.insert())
                    {
                        this.mErrors.copyAllErrors(tLLClaimDetailDBSet.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "ClaimSaveBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "保存数据失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }

                    LJSGetDB tLJSGetDB = new LJSGetDB(conn);
                    tLJSGetDB.setSchema(tLJSGetSchema);
                    if (tLJSGetDB.update() == false)
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLJSGetDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "ClaimSaveBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "保存数据失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }

                    LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB(conn);
                    tLJSGetClaimDB.setOtherNo(tLLClaimSchema.getClmNo());
                    tLJSGetClaimDB.setOtherNoType("5");
                    tLJSGetClaimDB.deleteSQL();
                    if (tLJSGetClaimDB.mErrors.needDealError())
                    {
                        this.mErrors.copyAllErrors(tLJSGetClaimDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "ClaimSaveBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "保存数据失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                    LJSGetClaimDBSet tLJSGetClaimDBSet = new LJSGetClaimDBSet(conn);
                    tLJSGetClaimDBSet.set(tLJSGetClaimSet);
                    if (!tLJSGetClaimDBSet.insert())
                    {
                        this.mErrors.copyAllErrors(tLJSGetClaimDBSet.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "ClaimSaveBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "保存数据失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                }

                conn.commit();
                conn.close();
        } // end of try
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ClaimSaveBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {

            }

            return false;
        }
        return true;
    }

}
