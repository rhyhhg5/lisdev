package com.sinosoft.lis.llcase;

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LLOutsourcingTotalDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.f1print.PrintService;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LLOutsourcingTotalSchema;
import com.sinosoft.lis.schema.LOBatchPRTManagerSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
public class OutsourcingCostPrintBL implements PrintService{
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往数据存贮 */
    private VData mResult = new VData();
//  外部传入的操作符
    private String mflag = null;
    private MMap map = new MMap();
    /** 数据操作字符串 */
    private String mOperate;
    private String mRgtClass = "";
    private String batchNo;
    private GlobalInput mGlobalInput = new GlobalInput();
//  接收类（天津外包费用总额）
	LLOutsourcingTotalSchema mLLOutsourcingTotalSchema   = new LLOutsourcingTotalSchema();
	 private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
	 LJAGetSchema mLJAGetSchema=new LJAGetSchema();
	 LJAGetSet mLJAGetSet = new LJAGetSet();
	  private LOBatchPRTManagerSchema tLOBatchPRTManagerSchema = null;
	/**
    传输数据的公共方法
    */
   public boolean submitData(VData cInputData, String cOperate) {
	   //将操作数据拷贝到本类中
       this.mOperate = cOperate;
       mflag = cOperate;
       //得到外部传入的数据,将数据备份到本类中
       if (!getInputData(cInputData)) {
           return false;
       }
       
       // 准备所有要打印的数据
       if (!getPrintData())
       {
           return false;
       }
       if (cOperate.equals("INSERT")) {
           if (!dealPrintMag("INSERT")) {
               return false;
           }
       }
       
       return true;
   }
	
   
   /**
    * 从输入数据中得到所有对象
    *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    */
   private boolean getInputData(VData cInputData) {
	   
	   mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
               "GlobalInput", 0));
	   mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData.getObjectByObjectName(
               "LOPRTManagerSchema", 0));
	   //批次印管理表信息
       tLOBatchPRTManagerSchema = new
               LOBatchPRTManagerSchema();
       tLOBatchPRTManagerSchema = ((LOBatchPRTManagerSchema)
                                   cInputData.getObjectByObjectName(
                                           "LOBatchPRTManagerSchema", 0));
	   //mLLOutsourcingTotalSchema中的主键放进去
	   mLLOutsourcingTotalSchema.setBatchno(mLOPRTManagerSchema.getOtherNo());
	   LJAGetDB mLJAGetDB = new LJAGetDB();
	   mLJAGetDB.setOtherNo(mLOPRTManagerSchema.getOtherNo());
	   mLJAGetSet= mLJAGetDB.query();
	   mLJAGetSchema=mLJAGetSet.get(1);
	   if (mLOPRTManagerSchema == null) {
           if (tLOBatchPRTManagerSchema == null) {
               System.out.println("---------为空--初次打印入口！");
               mLJAGetSchema.setSchema((LJAGetSchema) cInputData.
                                       getObjectByObjectName("LJAGetSchema", 0));
               mRgtClass = mLJAGetSchema.getPayMode();
               System.out.println("mRgtClass:"+mRgtClass);

               if (mLJAGetSchema == null) {
                   buildError("getInputData", "为空--初次打印入口没有得到足够的信息！");
                   return false;
               }
           } else {
               //批次打印的调用入口
        	   batchNo = tLOBatchPRTManagerSchema.getOtherNo();
               mLJAGetSchema.setOtherNo(batchNo);
           }
           return true;
       }
	   
	  if (mGlobalInput == null) {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "OutsourcingCostPrintBL";
           tError.functionName = "getInputData";
           tError.errorMessage = "没有得到足够的信息！";
           this.mErrors.addOneError(tError);
           return false;
       }
	   return true;
   }

   /**
    * 业务处理主函数
    * @return boolean
    */
   private boolean getPrintData()
   {
	   String BatchNo = mLLOutsourcingTotalSchema.getBatchno();
	   LLOutsourcingTotalDB tLLOutsourcingTotalDB=new LLOutsourcingTotalDB();
	   tLLOutsourcingTotalDB.setBatchno(BatchNo);
	   
	   if(!tLLOutsourcingTotalDB.getInfo()){
		   CError tError = new CError();
	        tError.moduleName = "OutsourcingCostPrintBL";
	        tError.functionName = "getInputData";
	        tError.errorMessage = "获取LLOutsourcingTotalDB数据的时候发生错误";
	        this.mErrors.addOneError(tError);
	        return false;
	   }
	   mLLOutsourcingTotalSchema= tLLOutsourcingTotalDB.getSchema();
	   // 4193 XX分公司外包录入
	   String sql ="select lja.ActuGetNo,(select name from ldcom where comcode=substr(lja.managecom,1,4)),substr(lja.managecom,1,4) from LJAGet lja where lja.OtherNo='"+BatchNo+"'" +"and lja.OtherNoType='19'";
	   SSRS ActuGetNoSQL=new ExeSQL().execSQL(sql);
	   String ActuGetNo=ActuGetNoSQL.GetText(1, 1);//给付号码 	   
	   String sqlMode =
           "select CodeName from ldcode where CodeType = 'paymode' and Code='" +
           mLLOutsourcingTotalSchema.getTransfertype() + "'";
	   String PayMode = new ExeSQL().getOneValue(sqlMode);//领取方式
	   String Osaccount= mLLOutsourcingTotalSchema.getOsaccount();//账户名
	   String Osaccountno = mLLOutsourcingTotalSchema.getOsaccountno();//账户号
	   String OtherNo = BatchNo;//业务序号	   
	   String payItem=ActuGetNoSQL.GetText(1, 2)+"外包录入";//给付项目
	   if("8631".equals(ActuGetNoSQL.GetText(1, 3))){
		   payItem="上海分公司外包录入";
	   }
	   String Money =new DecimalFormat("0.00").format((double)(mLLOutsourcingTotalSchema.getOstotal()));//给付金额 小写
	   String MoneyCHN=PubFun.getChnMoney(Double.parseDouble(Money));//给付金额大写
	   String sqlOperatorname = "select username from lduser where usercode = '" +
       mGlobalInput.Operator + "'";
	   ExeSQL exesql = new ExeSQL();
	   String Operator = exesql.getOneValue(sqlOperatorname);//业务经办
	  
	   
	   TextTag texttag = new TextTag(); //新建一个TextTag的实例
       XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
       texttag.add("JetFormType", "lp018");
     
       String sqlusercom = "select comcode from lduser where usercode='" +
       mGlobalInput.Operator + "' with ur";
       String comcode = new ExeSQL().getOneValue(sqlusercom);
       if (comcode.equals("86") || comcode.equals("8600")||comcode.equals("86000000")) {
           comcode = "86";
            } else if (comcode.length() >= 4) {
           comcode = comcode.substring(0, 4);
         } else {
           buildError("getInputData", "操作员机构查询出错！");
           return false;
         }
         String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"+comcode+"' with ur";
         String printcode = new ExeSQL().getOneValue(printcom);
       
       
       texttag.add("ManageComLength4", printcode);
       
       texttag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.","_"));
       if (mflag.equals("batch")) {
           texttag.add("previewflag", "0");
       } else {
           texttag.add("previewflag", "1");
       }
       xmlexport.createDocument("OutsourcingCostPrint.vts", "printer");
       texttag.add("BarCode1", ActuGetNo);
       texttag.add("BarCodeParam1"
               , "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
       texttag.add("BarCode2", ActuGetNo);
       texttag.add("BarCodeParam2"
               , "BarHeight=30&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
       texttag.add("ActuGetNo",ActuGetNo);
       texttag.add("PayMode",PayMode); 
       texttag.add("AccountName", Osaccount);
       texttag.add("AccountNo", Osaccountno);
       texttag.add("OtherNo", OtherNo);
       texttag.add("PayItem", payItem);
       texttag.add("Money", Money);
       texttag.add("MoneyCHN", MoneyCHN);
       texttag.add("Drawer", Osaccount);
       texttag.add("Operator",Operator);
       texttag.add("PrePaidMess","");
       if (texttag.size() > 0) {
            xmlexport.addTextTag(texttag);
       }
       
//       System.out.println("liujianPrint"+System.getProperty("user.dir")+"/");
//       xmlexport.outputDocumentToFile(System.getProperty("user.dir")+"/", "os"); 
       mResult.clear();
       mResult.addElement(xmlexport);
       LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
       tLOPRTManagerDB.setStandbyFlag2(mLJAGetSchema.getActuGetNo());
       tLOPRTManagerDB.setCode("lp018");

       String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
       String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
       mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
       mLOPRTManagerSchema.setOtherNo(mLJAGetSchema.getOtherNo());
       mLOPRTManagerSchema.setOtherNoType("19");
       mLOPRTManagerSchema.setCode("lp018");
       mLOPRTManagerSchema.setManageCom(this.mGlobalInput.ManageCom);
       mLOPRTManagerSchema.setReqCom(this.mGlobalInput.ManageCom);
       mLOPRTManagerSchema.setReqOperator(this.mGlobalInput.Operator);
       mLOPRTManagerSchema.setPrtType("0");
       mLOPRTManagerSchema.setStateFlag("0");
       mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
       mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
       mLOPRTManagerSchema.setPrintTimes("1");
       
       mResult.addElement(mLOPRTManagerSchema);
	   return true ;
   }
	
   /**
    * 准备往后层输出所需要的数据
    * 输出：如果准备数据时发生错误则返回false,否则返回true
    */
   private boolean prepareOutputData() {
	   try {
           System.out.println("Begin LAAgentBLF.prepareOutputData.........");
           mResult.clear();
           mResult.add(map);
       } catch (Exception ex) {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "OutsourcingCostBL";
           tError.functionName = "prepareOutputData";
           tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
           this.mErrors.addOneError(tError);
           return false;
       }
       return true;
   }
   
   public VData getResult()
   {
       return mResult;
   }

   public CErrors getErrors()
   {
       return mErrors;
   }
   
   private boolean dealPrintMag(String cOperate) {
       MMap tMap = new MMap();
       tMap.put(mLOPRTManagerSchema, cOperate);
       VData tVData = new VData();
       tVData.add(tMap);
       PubSubmit tPubSubmit = new PubSubmit();
       if (tPubSubmit.submitData(tVData, "") == false) {
           this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
           return false;
       }
       return true;
   }
   private void buildError(String szFunc, String szErrMsg) {
       CError cError = new CError();

       cError.moduleName = "LCPolF1PBL";
       cError.functionName = szFunc;
       cError.errorMessage = szErrMsg;
       this.mErrors.addOneError(cError);
   }
}
