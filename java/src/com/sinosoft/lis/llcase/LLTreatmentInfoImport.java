package com.sinosoft.lis.llcase;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.diskimport.DefaultDiskImporter;
import com.sinosoft.lis.schema.LLCaseTreatmentInfoSchema;
import com.sinosoft.lis.vschema.LLCaseTreatmentInfoSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class LLTreatmentInfoImport {


    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGlobalInput;
    private VData mResult = new VData();
    
    private int mSuccessNum = 0;

    public int getmSuccessNum() {
		return mSuccessNum;
	}


	public void setmSuccessNum(int mSuccessNum) {
		this.mSuccessNum = mSuccessNum;
	}

    private static final String schemaClassName =
        "com.sinosoft.lis.schema.LLCaseTreatmentInfoSchema";
    private static final String schemaSetClassName =
        "com.sinosoft.lis.vschema.LLCaseTreatmentInfoSet";

    /** 节点名 */
    private String sheetName = "Sheet1";
    /** 配置文件名 */
    private String configName = "TreatmentInfo.xml";
    private LLCaseTreatmentInfoSet mLLCaseTreatmentInfoSet = new LLCaseTreatmentInfoSet(); //诊疗项目信息表
    /**
     * 构造函数，新契约入口
     * @param GrpContNo String
     * @param gi GlobalInputs
     */
    public LLTreatmentInfoImport(GlobalInput gi) {
        this.mGlobalInput = gi;
    }


    /**
     * 添加诊疗项目信息
     * @param path String
     * @param fileName String
     */
    public boolean doAdd(String path, String fileName) {

        //从磁盘导入数据
        DefaultDiskImporter importFile = new DefaultDiskImporter(path +
                fileName,
                path + configName,
                sheetName);
        importFile.setSchemaClassName(schemaClassName);
        importFile.setSchemaSetClassName(schemaSetClassName);
        if (!importFile.doImport()) {
            mErrors.copyAllErrors(importFile.mErrors);
            return false;
        }

        LLCaseTreatmentInfoSet tLLCaseTreatmentInfoSet = (LLCaseTreatmentInfoSet) importFile.getSchemaSet();

        //校验导入的数据
        if (!checkData(tLLCaseTreatmentInfoSet)) {
            return false;
        }

        //存放Insert Into语句的容器
        MMap map = new MMap();
        for (int i = 1; i <= tLLCaseTreatmentInfoSet.size(); i++) {
        	addOneTreatmentInfo(tLLCaseTreatmentInfoSet.get(i));
        	mSuccessNum = mSuccessNum + 1;
        }
        map.put(tLLCaseTreatmentInfoSet, "INSERT");
        this.mResult.add(tLLCaseTreatmentInfoSet);
        //提交数据到数据库
        if (!submitData(map)) {
            return false;
        }
        return true;
    }

    private void deletePayList(MMap map) {

    }

    /**
     * 校验导入数据
     * @param cLLCaseDiagnosticInfoSet
     * @return boolean
     */
    private boolean checkData(LLCaseTreatmentInfoSet cLLCaseTreatmentInfoSet) {
        for (int i = 1; i <= cLLCaseTreatmentInfoSet.size(); i++) {
        	LLCaseTreatmentInfoSchema schema = cLLCaseTreatmentInfoSet.get(i);
            String strTreatmentCode = schema.getTreatmentCode();
            if(strTreatmentCode==null||strTreatmentCode.equals("")){
                mErrors.addOneError("缺少诊疗项目编码！");
                return false;
            }
            String strTreatmentName = schema.getTreatmentName();
            if ((strTreatmentName == null) || (strTreatmentName.equals(""))) {
                mErrors.addOneError("缺少项目名称！");
                return false;
            }
//
//            String strTreatmentCategory = schema.getTreatmentCategory();
//            if ((strTreatmentCategory == null) || (strTreatmentCategory.equals(""))) {
//                mErrors.addOneError("缺少类别！");
//                return false;
//            }
            
            String strMngCom = schema.getMngCom();
            if ((strMngCom == null) || (strMngCom.equals(""))) {
                mErrors.addOneError("缺少管理机构！");
                return false;
            }
            
            String strAreaCode = schema.getAreaCode();
            if ((strAreaCode == null) || (strAreaCode.equals(""))) {
                mErrors.addOneError("缺少地区编码！");
                return false;
            }
            
            String tMngCom = schema.getMngCom();
            String tMngComSQL = "select name from ldcom where sign = '1' and comcode = '" + tMngCom + "'";
            String MngCom = new ExeSQL().getOneValue(tMngComSQL);
            if ((MngCom == null) || (MngCom.equals(""))) {
                mErrors.addOneError("管理机构不存在！");
                return false;
            }
            
            String tAreaCode = schema.getAreaCode();
            String tAreaCodeSQL = "select  codename from LDCode where codetype = 'hmareacode' and code = '" + tAreaCode + "'";
            String AreaCode = new ExeSQL().getOneValue(tAreaCodeSQL);
            if ((AreaCode == null) || (AreaCode.equals(""))) {
                mErrors.addOneError("地区编码不存在！");
                return false;
            }
        }
        return true;
    }

    /**
     * 处理诊疗项目信息数据
     * @param map MMap
     * @param LDHospitalSchema aLDHospitalSchema
     */
    private void addOneTreatmentInfo(LLCaseTreatmentInfoSchema cLLCaseTreatmentInfoSchema) {

    	LLCaseTreatmentInfoSchema tLLCaseTreatmentInfoSchema = cLLCaseTreatmentInfoSchema;
    	
    	tLLCaseTreatmentInfoSchema.setSerialNo(PubFun1.CreateMaxNo("TREMENT", 20));
    	tLLCaseTreatmentInfoSchema.setOperator(mGlobalInput.Operator);
    	tLLCaseTreatmentInfoSchema.setMakeDate(PubFun.getCurrentDate());
    	tLLCaseTreatmentInfoSchema.setMakeTime(PubFun.getCurrentTime());
    	tLLCaseTreatmentInfoSchema.setModifyDate(PubFun.getCurrentDate());
    	tLLCaseTreatmentInfoSchema.setModifyTime(PubFun.getCurrentTime());
    	mLLCaseTreatmentInfoSet.add(tLLCaseTreatmentInfoSchema);
    }


    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submitData(MMap map) {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 返回执行结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

}
