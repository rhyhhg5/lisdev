package com.sinosoft.lis.llcase;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LLPrintSave {
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 全局变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mFileNameB = "";

	private String mMakeDate = "";

	private String mUserCode = "";

	private VData mInputData = new VData();

	private String mOperate = "";

	private PubFun mPubFun = new PubFun();

	private TransferData mTransferData = new TransferData();

	private XmlExport mXmlExport = null;

	private MMap map = new MMap();

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		mInputData = (VData) cInputData;
		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(mInputData)) {
			return false;
		}

		// 进行数据查询
		if (checkData()) {
			mOperate = "UPDATE";
		} else {
			mOperate = "INSERT";
		}
	    if (!dealData())
	    {
	          // @@错误处理
	          CError tError = new CError();
	          tError.moduleName = "LLPRintSave";
	          tError.functionName = "submitData";
	          tError.errorMessage = "数据处理失败LLPRintSave-->dealData!";
	          this.mErrors .addOneError(tError) ;
	          return false;
	    }
	    //准备往后台的数据
	    if (!prepareOutputData())
	      return false;
	      PubSubmit tPubSubmit = new PubSubmit();
	      if (!tPubSubmit.submitData(mInputData, null)) {
	        // @@错误处理
	        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
	        CError tError = new CError();
	        tError.moduleName = "LLPRintSave";
	        tError.functionName = "submitData";
	        tError.errorMessage = "数据提交失败!";

	        this.mErrors.addOneError(tError);
	        return false;
	      }
	    mInputData=null;
	    return true;
	}

	private void bulidErrorB(String cFunction, String cErrorMsg) {

		CError tCError = new CError();

		tCError.moduleName = "LLSurveyBL";
		tCError.functionName = cFunction;
		tCError.errorMessage = cErrorMsg;

		this.mErrors.addOneError(tCError);

	}

	/**
	 * 取得传入的数据 如果没有传入管理机构和起止如期则查全部机构全年的信息
	 * 
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		// tCurrentDate = mPubFun.getCurrentDate();
		try {
			mTransferData = (TransferData) cInputData.getObjectByObjectName(
					"TransferData", 0);
			// 页面传入的数据 三个
			this.mFileNameB = (String) mTransferData
					.getValueByName("tFileNameB");
			this.mMakeDate = (String) mTransferData.getValueByName("tMakeDate");
			this.mUserCode = (String) mTransferData.getValueByName("tOperator");
		} catch (Exception ex) {
			this.mErrors.addOneError("");
			return false;
		}
		return true;
	}

	private boolean checkData() {
		boolean flag = false;
		String sql = "select 1 from ldcode where codetype='lpfileprint' and code='"
				+ mFileNameB + "' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		String result = tExeSQL.getOneValue(sql);
		if (result != null && !"".equals(result) && !"null".equals(result)) {
			flag = true;
		}
		return flag;
	}

	private boolean dealData() {
		LDCodeSchema tLDCodeSchema = new LDCodeSchema();
		tLDCodeSchema.setCodeType("lpfileprint");
		tLDCodeSchema.setCode(mFileNameB);
		tLDCodeSchema.setCodeName(mUserCode);
		tLDCodeSchema.setCodeAlias(mMakeDate);
		if ("INSERT".equals(mOperate)) {
			map.put(tLDCodeSchema, "INSERT");
		} else {
			map.put(tLDCodeSchema, "UPDATE");
		}
		return true;
	}

	private boolean prepareOutputData() {
		System.out.println("BL prepareOutputData");
		try {
			this.mInputData.clear();
			mInputData.add(map);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDCodeSchema";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * @return VData
	 */
	public VData getResult() {
		return mResult;
	}

}
