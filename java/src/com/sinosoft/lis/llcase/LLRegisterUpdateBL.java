package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.LCAccountDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAccountSchema;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
 import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.vschema.LCAccountSet;
import com.sinosoft.lis.vschema.LLRegisterSet;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class LLRegisterUpdateBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    public GlobalInput mGlobalInput;
    private VData mInputData;
    private VData mResult = new VData();
    private LLRegisterSet mLLRegisterSet;
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private MMap map = new MMap();

    // 个人信息
    //private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();

    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();

    public LLRegisterUpdateBL()
    {
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        //得到输入数据
        if (!getInputData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData(cOperate))
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLRegisterBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData()
    {
      // 个人信息
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
       // mLLRegisterSchema = (LLRegisterSchema) mInputData.getObjectByObjectName(
      //          "LLRegisterSchema", 0);
        mLLCaseSchema = (LLCaseSchema) mInputData.getObjectByObjectName(
                "LLCaseSchema", 0);

    //    if (mLLRegisterSchema == null)
   //     {
   //         // @@错误处理
   //         CError tError = new CError();
   //         tError.moduleName = "LLRegisterUpdateBL";
   //         tError.functionName = "getInputData";
   //         tError.errorMessage = "在个人信息接受时没有得到足够的数据，请您确认有：个人的完整信息!";
   //         this.mErrors.addOneError(tError);
  //          return false;
   //     }

        if (mLLCaseSchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLRegisterUpdateBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "在个人信息接受时没有得到足够的数据，请您确认有：个人的完整信息!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }





    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData(String cOperate)
    {
        boolean tReturn = false;
        //更新纪录
        System.out.println(cOperate);
        if (cOperate.equals("UPDATE"))
        {
            //更新个人信息表中的不能为空的字段(MakeDate,MakeTime,ModifyDate,ModifyTime)
            //入机日期,入机时间, 最后一次修改日期,最后一次修改时间
            String CaseNo = mLLCaseSchema.getCaseNo();
            LLCaseDB tLLCaseDB = new LLCaseDB();
            LLCaseSchema tLLCaseSchema =  new LLCaseSchema();
            tLLCaseDB.setCaseNo(CaseNo);
            if(tLLCaseDB.getInfo())
            {
              tLLCaseSchema=tLLCaseDB.getSchema();
              System.out.println(tLLCaseSchema.getCaseNo());
              tLLCaseSchema.setCaseNo(CaseNo);
              tLLCaseSchema.setCaseGetMode(mLLCaseSchema.getCaseGetMode());
              tLLCaseSchema.setAccModifyReason(mLLCaseSchema.getAccModifyReason());
              tLLCaseSchema.setAccName(mLLCaseSchema.getAccName());
              tLLCaseSchema.setBankAccNo(mLLCaseSchema.getBankAccNo());
              tLLCaseSchema.setBankCode(mLLCaseSchema.getBankCode());
              map.put(tLLCaseSchema, "UPDATE");
            tReturn = true;
            }else{
              CError tError = new CError();
              tError.moduleName = "ClaimUnderwriteBL";
              tError.functionName = "getBaseData";
              tError.errorMessage = "登记信息表查询失败" +
                                "分案号：" + CaseNo;
              mErrors.addOneError(tError);

              tReturn =  false;
            }
        }


        return tReturn;
    }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(map);
            mResult.add(mLLCaseSchema);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLRegisterUpdateBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    //得到结果
    public VData getResult()
    {
        return this.mResult;
    }
    public static void main(String[] args)
       {
           LLRegisterUpdateBL tLLRegisterUpdateBL = new LLRegisterUpdateBL();
           LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
            tLLRegisterSchema.setRgtNo("550000000000141");
            tLLRegisterSchema.setAccName("huang");
            tLLRegisterSchema.setBankAccNo("1111111111111");
            tLLRegisterSchema.setBankCode("01");
           VData tVData = new VData();
           tVData.addElement(tLLRegisterSchema);
           GlobalInput tG = new GlobalInput();
           tG.ManageCom = "86110000";
           tG.Operator = "001";
           tVData.addElement(tG);
           tLLRegisterUpdateBL.submitData(tVData, "UPDATE");
       }


}
