package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.LLCaseSet;

/**
 * <p>Title: 理赔案件信息导入类</p>
 * <p>Description: 把Excel里的理赔信息添加到数据库 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author Xx
 * @version 1.0
 */

public class LLImportCaseInfoNew
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGlobalInput;
    private VData mResult = new VData();
    private VData mInputData = new VData();

    private String mRgtNo = "";
    /**文件路径*/
    private String FilePath="";
    /**文件名*/
    private String FileName="";
    /**导入标志*/
    private String ImportFlag="";
    /** 批次版本 */
    private String VersionType="";
    /** 解析配置文件XML路径 */
    private String XMLPath = "";
    
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    /** #2169 批次导入前后台数据同步问题方案,导入时变更批次状态 */
    private LLRegisterSchema mmLLRegisterSchema = new LLRegisterSchema();
    MMap map = new MMap();

    /**
     * 构造函数
     * @param gi GlobalInput
     */
    public LLImportCaseInfoNew()
    {
    }

    /**
     * 导入入口
     * @param path String
     * @param fileName String
     */
    public boolean submitData(VData mVData, String cOperate) {
    	  
        System.out.println("submitData Begin....");
	    try{
	    	  
	        if (!getInputData(mVData))
	            return false;
	          String tsq2 = "INSERT INTO ldtimetest VALUES ('rgtType','批导','"+mRgtNo+"','理赔批导开始','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
	    	  new ExeSQL().execUpdateSQL(tsq2);
	    	  String tsq3 = "INSERT INTO ldtimetest VALUES ('rgtType','批导','"+mRgtNo+"','更新批状态校验操作人开始','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
	    	  new ExeSQL().execUpdateSQL(tsq3);
	        if(!checkData()){
	        	return false;
	        }
	        String tsq4 = "INSERT INTO ldtimetest VALUES ('rgtType','批导','"+mRgtNo+"','更新批状态校验操作人结束','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
	    	  new ExeSQL().execUpdateSQL(tsq4);
	        System.out.println(FilePath);
	        System.out.println(FileName);

        	if (!doAdd(FilePath, FileName,ImportFlag,VersionType,XMLPath))
            {
        		return false;
            }
        	String tsq9 = "INSERT INTO ldtimetest VALUES ('rgtType','批导','"+mRgtNo+"','理赔批导结束','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
            new ExeSQL().execUpdateSQL(tsq9);
        }catch(Exception e){

        }finally{
        	PubSubmit tPubSubmit = new PubSubmit();
            mInputData.add(map);
            if (!tPubSubmit.submitData(mInputData, "")) {
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                return buildErr("submitData","LLRegister数据更新失败！");
            }
        }
        
        return true;
    }

    /**
     * 从save页面获取数据
     * @param map MMap
     * @return boolean
     */
    private boolean getInputData(VData mInputData) {
        System.out.println("getInputData ......");
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        TransferData tTransferData = new TransferData();
        tTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData",0);
        FilePath = (String) tTransferData.getValueByName("FilePath");
        FileName = (String) tTransferData.getValueByName("FileName");
        mRgtNo = (String) tTransferData.getValueByName("RgtNo");
        ImportFlag = (String) tTransferData.getValueByName("ImportFlag");
        VersionType = (String) tTransferData.getValueByName("VersionType");
        XMLPath = (String) tTransferData.getValueByName("XmlPath");
        return true;
    }

    // @@错误处理
    private boolean buildErr(String FucName,String ErrMsg){
        CError tError = new CError();
        tError.moduleName = "LLImportCaseInfoNew";
        tError.functionName = FucName;
        tError.errorMessage = ErrMsg;
        this.mErrors.addOneError(tError);
        return false;
    }
    /**
     * 处理前检查数据是否合理
     * @return boolean
     */
    private boolean checkData() {
    	
        if(mRgtNo==null||mRgtNo.equals("")){
        	return buildErr("checkData","团体批次号不能为空!");
        }

        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if (!tLLRegisterDB.getInfo()) {
        	return buildErr("checkData", "团体立案信息查询失败!批次号：" + mRgtNo);
		}
        mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
		
		//特殊存储处理，上载时变更批次状态
		MMap tMap = new MMap();
        VData tVData = new VData();
        PubSubmit tPS = new PubSubmit();
		mmLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
		mmLLRegisterSchema.setRgtState("06");
		mmLLRegisterSchema.setOperator(mGlobalInput.Operator);
        mmLLRegisterSchema.setMngCom(mGlobalInput.ManageCom);
        mmLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
        mmLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());
        tMap.put(mmLLRegisterSchema, "UPDATE");  
        tVData.add(tMap);
        if (!tPS.submitData(tVData, null)) {
            return buildErr("checkData", "数据保存失败");
        }
        
        try {
    		//上载理赔信息时，会校验用户是否有理赔权限，这里“社保批次”需要使用社保的权限判断
    		String tSocialSql = "select CHECKGRPCONT((select RgtObjNo from LLRegister where 1=1 and rgtno ='"+mLLRegisterSchema.getRgtNo()+"' )) from dual where 1=1  ";
    		ExeSQL tExeSQL = new ExeSQL();
    		String tResult = tExeSQL.getOneValue(tSocialSql);
    		if(tResult!=null&&tResult.equals("Y"))
    		  {	
    			  String checkSql = "Select '1' From LLSocialClaimUser where UserCode = '"+this.mGlobalInput.Operator+"' and StateFlag = '1' and HandleFlag ='0'";
    			  String tOper_Result = tExeSQL.getOneValue(checkSql);
    			  if(tOper_Result!=null&&"1".equals(tOper_Result))
    			  {
    				  
    			  }else{
    				  return buildErr("checkData","您不具有社保案件理赔操作权限!");
    			  }			  
    		  }else{
    			  LLClaimUserDB tLLClaimUserDB=new LLClaimUserDB();
    		  	  tLLClaimUserDB.setUserCode(mGlobalInput.Operator);//校验操作人是否具有理赔权限
    		  	  if(!tLLClaimUserDB.getInfo())
    		  		 return buildErr("checkData","操作人不具有理赔权限!");
    		  }
        	
    		
    		String declineflag = "" + mLLRegisterSchema.getDeclineFlag();
    		if (declineflag.equals("1")) {
    			return buildErr("checkData", "该团体申请已撤件，不能再导入个人客户!");
    		}
    		if ("02".equals(mLLRegisterSchema.getRgtState())) {
    			return buildErr("checkData", "团体申请下所有个人案件已经受理完毕，" + "不能再导入个人客户!");
    		}
    		if ("03".equals(mLLRegisterSchema.getRgtState())) {
    			return buildErr("checkData", "团体申请下所有个人案件已经结案完毕，" + "不能再导入个人客户!");
    		}
    		if ("04".equals(mLLRegisterSchema.getRgtState())
    				|| "05".equals(mLLRegisterSchema.getRgtState())) {
    			return buildErr("checkData", "团体申请下所有个人案件已经给付确认，" + "不能再导入个人客户!");
    		}
        	
        }catch(Exception e){
        	
        }finally{
    		LLCaseSet tLLCaseSet = new LLCaseSet();
            LLCaseDB tLLCaseDB = new LLCaseDB();
            tLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
            tLLCaseSet.set(tLLCaseDB.query());
            if (tLLCaseDB.mErrors.needDealError()) {
                // @@错误处理
                CError.buildErr(this, "个人案件查询失败");
                return false;
            }
            int realPeoples = tLLCaseSet.size();
            int appPeoples = mLLRegisterSchema.getAppPeoples();
            if (realPeoples >= appPeoples) {
                mLLRegisterSchema.setAppPeoples(realPeoples + 1);
            }
            mLLRegisterSchema.setApplyerType("5");
            if ("PR".equals(ImportFlag)) {//由“理赔处理->受理申请（团体）”进入
                mLLRegisterSchema.setApplyerType("2");
            }
            mLLRegisterSchema.setRgtState("07");//批次上载完毕
            mLLRegisterSchema.setOperator(mGlobalInput.Operator);
            mLLRegisterSchema.setMngCom(mGlobalInput.ManageCom);
            mLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
            mLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLLRegisterSchema, "UPDATE");
        }
        
        return true;
    }

    public boolean doAdd(String ImportPath, String FileName,String aImportFlag,String aVersionType,String aXMLPath) {

        //从磁盘导入数据
   	 String tsq4 = "INSERT INTO ldtimetest VALUES ('rgtType','批导','"+mRgtNo+"','解析导入类开始','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
  	  new ExeSQL().execUpdateSQL(tsq4);
  	  
        LLImportFileNew tLLimportFileNew = new LLImportFileNew(ImportPath, FileName,mRgtNo,mGlobalInput,aImportFlag,aVersionType,aXMLPath);
        VData data = tLLimportFileNew.doImport();
        if (data == null) {
            System.out.println("全部导入成功！");
        }else{
        	TransferData tTransferData = new TransferData();
        	tTransferData = (TransferData)data.getObjectByObjectName("TransferData", 0);
        	String tErrorMSG = (String)tTransferData.getValueByName("Error-parseVts()");
        	if(!"".equals(tErrorMSG) && tErrorMSG != null){
        		return buildErr("doAdd",tErrorMSG);
        	}	
        }
        this.mResult=data;
        return true;
    }

    /**
     * 返回执行结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args) {
        VData tVData = new VData();
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "8612";
        tGI.Operator = "claim";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("FileName", "Book1.xls");
        tTransferData.setNameAndValue("FilePath", "D:/DEVELOP/ui/temp_lp/");
        tTransferData.setNameAndValue("RgtNo","P1200070410000001");

        tVData.add(tTransferData);
        tVData.add(tGI);
        LLImportCaseInfo tLLImportCaseInfo = new LLImportCaseInfo();
        tLLImportCaseInfo.submitData(tVData, "");
    }
}
