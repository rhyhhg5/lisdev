package com.sinosoft.lis.llcase;

import java.util.Date;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 保全人工核保</p>
 * <p>Description: 体检通知书录入</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorUWManuRReportQueryBL
{
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private String mPrtSeq = null;

    private LPRReportSchema mLPRReportSchema = null;

    private LPRReportResultSet mLPRReportResultSet = null;

    private LPRReportItemSet mLPRReportItemSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();
    
    /** 打印管理表 */
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!getInputData(data))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到传入参数
     * @param data VData
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                    "GlobalInput", 0);
            mLPRReportSchema = (LPRReportSchema) data.
                    getObjectByObjectName("LPRReportSchema", 0);
            mLPRReportItemSet = (LPRReportItemSet) data.
                    getObjectByObjectName("LPRReportItemSet", 0);
            mLPRReportResultSet = (LPRReportResultSet) data.
                    getObjectByObjectName("LPRReportResultSet", 0);
            mEdorNo = mLPRReportSchema.getEdorNo();
            mPrtSeq = mLPRReportSchema.getPrtSeq();
        }
        catch (Exception ex)
        {
            mErrors.addOneError("传入参数错误！");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 处理数据
     * @return boolean
     */
    private boolean dealData()
    {
        setLPRReport();
        setLPRReportItem();
        setLPRReportResult();
        changeEdorType();
        return true;
    }
    
    /**
     * 改变理赔工单的状态：待回复-》待处理
     *
     */
    private void changeEdorType(){
    	System.out.println("PEdorUWManuHealthBL-->changeEdorType");
    	//需要通过理赔续保二核的工单号，查询此次对应的任务流号，从而状态改变
    	String tSql = "select MissionId 工作流任务号, SubMissionId 工作流子任务号 " 
    		+ " from LWMission where ProcessId = '0000000003' and ActivityId = '0000001181' " 
    		+ "and Missionprop12 ='"+mEdorNo+"' "
    		+ "order by Missionprop1 desc ";
    	SSRS tSSRS = new SSRS();
    	ExeSQL tExeSQL = new ExeSQL();
    	tSSRS = tExeSQL.execSQL(tSql);
    	String tMissionId = "";
    	String tSubMissionId = "";
    	String tActivityId = "0000001181";
    	String tActivityStatus = "1";
    	if(tSSRS != null && tSSRS.getMaxRow() > 0){
    		for(int i=1;i<=tSSRS.getMaxRow();i++){		
    			tMissionId = tSSRS.GetText(i, 1);
    			tSubMissionId = tSSRS.GetText(i, 2);
    		}
    		
    	}else{
    		mErrors.addOneError("PEdorUWManuHealthBL中查询工单的对应工作流号失败");
    	}
    	
    	LWMissionSchema tLWMissionSchema   = new LWMissionSchema();
        UWIndStateUI tUWIndStateUI = new UWIndStateUI();
        tLWMissionSchema.setMissionID(tMissionId);
        tLWMissionSchema.setSubMissionID(tSubMissionId);
        tLWMissionSchema.setActivityID(tActivityId);
        tLWMissionSchema.setActivityStatus(tActivityStatus);
        String tOperate = "UPDATE||MAIN";
        VData tVData = new VData();
		tVData.add(tLWMissionSchema);
		tVData.add(mGlobalInput);
		tUWIndStateUI.submitData(tVData,tOperate);
    }

    private void setLPRReport()
    {
        LPRReportDB tLPRReportDB = new LPRReportDB();
        tLPRReportDB.setEdorNo(mLPRReportSchema.getEdorNo());
        tLPRReportDB.setPrtSeq(mLPRReportSchema.getPrtSeq());
        LPRReportSet tLPRReportSet = tLPRReportDB.query();
        if (tLPRReportSet.size() == 0)
        {
            return;
        }
        LPRReportSchema tLPRReportSchema = tLPRReportSet.get(1);
        tLPRReportSchema.setContente(mLPRReportSchema.getContente());
        tLPRReportSchema.setReplyContente(mLPRReportSchema.getReplyContente());
        tLPRReportSchema.setOperator(mGlobalInput.Operator);
        tLPRReportSchema.setMakeDate(mCurrentDate);
        tLPRReportSchema.setMakeTime(mCurrentTime);
        tLPRReportSchema.setModifyDate(mCurrentDate);
        tLPRReportSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPRReportSchema, "DELETE&INSERT");
    }

    private void setLPRReportResult()
    {
        for (int i = 1; i <= mLPRReportResultSet.size(); i++)
        {
            LPRReportResultSchema tLPRReportResultSchema =
                    mLPRReportResultSet.get(i);
            tLPRReportResultSchema.setGrpContNo(BQ.GRPFILLDATA);
            tLPRReportResultSchema.setICDCode("00");
            tLPRReportResultSchema.setOperator(mGlobalInput.Operator);
            tLPRReportResultSchema.setMakeDate(mCurrentDate);
            tLPRReportResultSchema.setMakeTime(mCurrentTime);
            tLPRReportResultSchema.setModifyDate(mCurrentDate);
            tLPRReportResultSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPRReportResultSchema, "DELETE&INSERT");
        }
    }

    /**
     * 设置体检项目表
     */
    private void setLPRReportItem()
    {
        for (int i = 1; i <= mLPRReportItemSet.size(); i++)
        {
            LPRReportItemSchema tLPRReportItemSchema = mLPRReportItemSet.get(i);
            LPRReportItemDB tLPRReportItemDB = new LPRReportItemDB();
            tLPRReportItemDB.setEdorNo(tLPRReportItemSchema.getEdorNo());
            tLPRReportItemDB.setPrtSeq(tLPRReportItemSchema.getPrtSeq());
            tLPRReportItemDB.setContNo(tLPRReportItemSchema.getContNo());
            tLPRReportItemDB.setRReportItemCode(tLPRReportItemSchema.getRReportItemCode());
            if (!tLPRReportItemDB.getInfo())
            {
                break;
            }
            tLPRReportItemDB.setRRItemResult(tLPRReportItemSchema.getRRItemResult());
            tLPRReportItemDB.setModifyDate(mCurrentDate);
            tLPRReportItemDB.setModifyTime(mCurrentTime);
            mMap.put(tLPRReportItemDB.getSchema(), "DELETE&INSERT");
        }
    }


    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
