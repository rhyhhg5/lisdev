package com.sinosoft.lis.llcase;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLOutcoucingAverageSchema;
import com.sinosoft.lis.schema.LLOutsourcingTotalSchema;
import com.sinosoft.lis.vschema.LLOutcoucingAverageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
public class OutsourcingCostImportBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    
    private MMap map = new MMap();
    /** 数据操作字符串 */
    private String mOperate;
    private GlobalInput mGlobalInput = new GlobalInput();
//  接收类（天津外包费用总额）
	LLOutsourcingTotalSchema mLLOutsourcingTotalSchema   = new LLOutsourcingTotalSchema();
	//	接收类（本保费分摊费用）
	LLOutcoucingAverageSchema mLLOutcoucingAverageSchema   = new LLOutcoucingAverageSchema();
	
	LLOutcoucingAverageSet mLLOutcoucingAverageSet= new LLOutcoucingAverageSet();
	
	
	/**
    传输数据的公共方法
    */
   public boolean submitData(VData cInputData, String cOperate) {
	   //将操作数据拷贝到本类中
       this.mOperate = cOperate;
       //得到外部传入的数据,将数据备份到本类中
       if (!getInputData(cInputData)) {
           return false;
       }
       //进行业务处理
       if (!dealData()) {
           return false;
       }
       //准备往后台的数据
       if (!prepareOutputData()) {
           return false;
       }
       PubSubmit tPubSubmit = new PubSubmit();
       if (!tPubSubmit.submitData(mInputData, mOperate)) {
           // @@错误处理
           this.mErrors.copyAllErrors(tPubSubmit.mErrors);
           CError tError = new CError();
           tError.moduleName = "OutsourcingCostImportBL";
           tError.functionName = "submitData";
           tError.errorMessage = "数据重复提交!";
           this.mErrors.addOneError(tError);
           return false;
       }
       return true;
   }
	
   
   /**
    * 从输入数据中得到所有对象
    *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    */
   private boolean getInputData(VData cInputData) {
	   mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
               "GlobalInput", 0));
	  
	   mLLOutcoucingAverageSet.set((LLOutcoucingAverageSet)cInputData.getObjectByObjectName(
               "LLOutcoucingAverageSet", 0));
	 
	 
	  if (mGlobalInput == null) {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "OutsourcingCostBL";
           tError.functionName = "getInputData";
           tError.errorMessage = "没有得到足够的信息！";
           this.mErrors.addOneError(tError);
           return false;
       }
	   return true;
   }

   /**
    * 业务处理主函数
    * @return boolean
    */
   public boolean dealData() {
	   String currentDate = PubFun.getCurrentDate();
       String currentTime = PubFun.getCurrentTime();
      
       for(int i=1;i<=mLLOutcoucingAverageSet.size();i++){
    	   mLLOutcoucingAverageSet.get(i).setMakedate(currentDate);
    	   mLLOutcoucingAverageSet.get(i).setMaketime(currentTime);
    	   mLLOutcoucingAverageSet.get(i).setModifydate(currentDate);
    	   mLLOutcoucingAverageSet.get(i).setModifytime(currentTime);
    	   mLLOutcoucingAverageSet.get(i).setOperator(mGlobalInput.Operator);
       }
	   
	   this.map.put(this.mLLOutcoucingAverageSet, "INSERT");
	   return true;
   }
	
   /**
    * 准备往后层输出所需要的数据
    * 输出：如果准备数据时发生错误则返回false,否则返回true
    */
   private boolean prepareOutputData() {
	   try {
           System.out.println("Begin LAAgentBLF.prepareOutputData.........");
           mInputData.clear();
           mInputData.add(map);
       } catch (Exception ex) {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "OutsourcingCostBL";
           tError.functionName = "prepareOutputData";
           tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
           this.mErrors.addOneError(tError);
           return false;
       }
       return true;
   }

}
