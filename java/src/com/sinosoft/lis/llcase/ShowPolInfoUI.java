package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title:ShowPolInfoUI</p>
 * <p>Description:客户保单查询类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author ：LiuYansong
 * @version 1.0
 */
public class ShowPolInfoUI
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData = new VData();
  private VData mResult = new VData();
  private String mOperate;

  public ShowPolInfoUI() {}

  public boolean submitData(VData cInputData,String cOperate)
  {
    this.mOperate = cOperate;
    ShowPolInfoBL tShowPolInfoBL = new ShowPolInfoBL();

    if (tShowPolInfoBL.submitData(cInputData,mOperate) == false)
    {
  		this.mErrors.copyAllErrors(tShowPolInfoBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "tShowPolInfoBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
			mResult = tShowPolInfoBL.getResult();
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }
}