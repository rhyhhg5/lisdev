package com.sinosoft.lis.llcase;

import java.io.ByteArrayOutputStream;

import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.f1print.AccessVtsFile;
import com.sinosoft.lis.f1print.CLaimDeclinePrtUI;
import com.sinosoft.lis.f1print.CombineVts;
import com.sinosoft.lis.f1print.Readhtml;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LOBatchPRTManagerSchema;
import com.sinosoft.lis.vschema.LLCaseSet;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description: 理赔批量打印
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Xx
 * @version 1.0
 */
public class NewLLBatchPrtCVSBL {

	public NewLLBatchPrtCVSBL() {
	}

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 全局变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mOperator;

	private String mMangeCom;

	private String mRgtNo = "";

	private TransferData mTransferData = new TransferData();

	ExeSQL tExeSQL = new ExeSQL();

	private String mrealpath = "";
	
	private Readhtml rh;
	
	private String mFileName;
	
	private String mSCaseNo ="" ;
	private String mECaseNo ="";
	private String mRgtDateS ="";
	private String mRgtDateE ="";
	private String mEndDateS ="";
	private String mEndDateE ="";
	private String mBatchType ="";
	private String mSingleCaseNo ="";

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!dealDate()) {
			return false;
		}

		return true;
	}
	
   private boolean dealDate(){
	  
	   if(("null").equals(mBatchType)||mBatchType==null||"".equals(mBatchType))
	   {
		   if(!dealDate2())
			   return false;
	   }
	   if("1".equals(mBatchType)||"2".equals(mBatchType)||"3".equals(mBatchType))
	   {
		   if(!dealDate1())
			   return false;
	   }
	   		
		return true;
		
	}
   
	private boolean dealDate1(){
		LLCaseSet tLLCaseSet = new LLCaseSet();
		String sqlc="";
		if(mBatchType.equals("1")){
			sqlc="select * from llcase a where a.rgtno='"+mRgtNo+"'"
			          + " and exists (select 1 from LLClaimDecline b where b.caseno=a.caseno) and a.rgtstate in ('11','12') with ur";
			LLCaseDB tLLCaseDB = new LLCaseDB();
			tLLCaseSet = tLLCaseDB.executeQuery(sqlc);
			
		}else if(mBatchType.equals("2")){
			String tSQL="";
			if (mSCaseNo != null && !"".equals(mSCaseNo)) {
				tSQL += " and substr(a.caseno,6)>=substr('" + mSCaseNo + "',6)";
			}
			
			if (mECaseNo != null && !"".equals(mECaseNo)) {
				tSQL += " and substr(a.caseno,6)<=substr('" + mECaseNo + "',6)";
			}
			
			if (mRgtDateS != null && !"".equals(mRgtDateS)) {
				tSQL += " and a.rgtdate >= '" + mRgtDateS + "'";
			}
			
			if (mRgtDateE != null && !"".equals(mRgtDateE)) {
				tSQL += " and a.rgtdate <= '" + mRgtDateE + "'";
			}
			
			if (mEndDateS != null && !"".equals(mEndDateS)&& mEndDateE != null && !"".equals(mEndDateE)) {
				tSQL+=" and exists(select 1 from ljagetclaim where otherno=a.caseno and othernotype='5'" 
							+" and feeoperationtype<>'FC' and opconfirmdate between '"+mEndDateS+"' and '"+mEndDateE+"')";	
			}
			
			if (!"".equals(tSQL)) {
				String tCountSQL = "select count(1) from llcase a,LLClaimDecline b where a.rgtstate in ('11','12') "
		              + " and  a.caseno=b.caseno and a.MngCom like '"
			          + mMangeCom + "%' " + tSQL;
	            int tCount = Integer.parseInt((tExeSQL.getOneValue(tCountSQL)));
	           if (tCount > 1000) {
		            buildError("LLBatchPrtBL","单次打印数量过大，可能造成打印异常，请减少打印数量到1000条以下。");
		            return false;
	           }
	           
	           if(tCount < 1){
	        	   buildError("LLBatchPrtBL","没有符合打印条件的数据！");
		           return false;
	           }
	           
			}
				
			sqlc =  "select * from llcase a where a.rgtstate in ('11','12') "
		              + " and  exists (select 1 from LLClaimDecline b where a.caseno=b.caseno ) and a.MngCom like '"
			          + mMangeCom + "%' " + tSQL;
			LLCaseDB tLLCaseDB = new LLCaseDB();
			tLLCaseSet = tLLCaseDB.executeQuery(sqlc);
			
		}else{
			
            String tCaseNoBatch = mSingleCaseNo.trim();
            String tCaseNoBatch1 = tCaseNoBatch;
            String tCaseNoBatch2 = tCaseNoBatch;
            String tCaseNoBatch3 = tCaseNoBatch;
            while (tCaseNoBatch1.length() >= 17) {
                LLCaseSchema tLLCaseSchema = new LLCaseSchema();
                int Cindex = tCaseNoBatch1.indexOf("C");
                if(Cindex<0){
               	 break;
                }
                String tCaseNo = tCaseNoBatch1.substring(Cindex, Cindex + 17);
                sqlc =  "select 1 from llcase a where a.rgtstate in ('11','12') "                                                               
		              + " and  exists (select 1 from LLClaimDecline b where a.caseno=b.caseno ) and a.caseno='"+tCaseNo+"' with ur";
                SSRS caseset = tExeSQL.execSQL(sqlc);                                                                           
                if(caseset.MaxRow==1){                                                                                          
              	   LLCaseDB tLLCaseDB = new LLCaseDB();                                                                         
                   tLLCaseDB.setCaseNo(tCaseNo);                                                                              
                   if (tLLCaseDB.getInfo()) {                                                                                 
                       tLLCaseSchema.setSchema(tLLCaseDB.getSchema());                                                        
                       tLLCaseSet.add(tLLCaseSchema);                                                                         
                   }                                                                                                          
              }   
                tCaseNoBatch1 = tCaseNoBatch1.substring(Cindex + 17);
            }
        
        while (tCaseNoBatch2.length() >= 17) {
            LLCaseSchema tLLCaseSchema = new LLCaseSchema();
            int Cindex = tCaseNoBatch2.indexOf("R");
            if(Cindex<0){
           	 break;
            }
            String tCaseNo = tCaseNoBatch2.substring(Cindex, Cindex + 17);
            sqlc =  "select 1 from llcase a where a.rgtstate in ('11','12') "                                                               
	              + " and  exists (select 1 from LLClaimDecline b where a.caseno=b.caseno ) and a.caseno='"+tCaseNo+"' with ur";
            SSRS caseset = tExeSQL.execSQL(sqlc);                                                                           
            if(caseset.MaxRow==1){                                                                                          
        	   LLCaseDB tLLCaseDB = new LLCaseDB();                                                                         
               tLLCaseDB.setCaseNo(tCaseNo);                                                                              
               if (tLLCaseDB.getInfo()) {                                                                                 
                 tLLCaseSchema.setSchema(tLLCaseDB.getSchema());                                                        
                 tLLCaseSet.add(tLLCaseSchema);                                                                         
              }                                                                                                          
           }
            tCaseNoBatch2 = tCaseNoBatch2.substring(Cindex + 17);
       }
    
        while (tCaseNoBatch3.length() >= 17) {
            LLCaseSchema tLLCaseSchema = new LLCaseSchema();
            int Cindex = tCaseNoBatch3.indexOf("S");
            if(Cindex<0){
           	 break;
            }
            String tCaseNo = tCaseNoBatch3.substring(Cindex, Cindex + 17);
            sqlc =  "select 1 from llcase a where a.rgtstate in ('11','12') "                                                               
	              + " and  exists (select 1 from LLClaimDecline b where a.caseno=b.caseno ) and a.caseno='"+tCaseNo+"' with ur";
            SSRS caseset = tExeSQL.execSQL(sqlc);                                                                           
            if(caseset.MaxRow==1){                                                                                          
      	       LLCaseDB tLLCaseDB = new LLCaseDB();                                                                         
               tLLCaseDB.setCaseNo(tCaseNo);                                                                              
               if (tLLCaseDB.getInfo()) {                                                                                 
                 tLLCaseSchema.setSchema(tLLCaseDB.getSchema());                                                        
                 tLLCaseSet.add(tLLCaseSchema);                                                                         
               }                                                                                                          
           }
            tCaseNoBatch3 = tCaseNoBatch3.substring(Cindex + 17);
        }
               
    }
			
		boolean operFlag = true;
		int numcase = tLLCaseSet.size();
		if(numcase<1){
			buildError("LLBatchPrtBL", "没有符合打印条件的数据！");
			return false;
		}
		String[] tInputEntry=new String[numcase];
		rh = new Readhtml();
		for(int m=1;m<=numcase;m++){
			LOBatchPRTManagerSchema tLOBatchPRTManagerSchema = new LOBatchPRTManagerSchema();
			tLOBatchPRTManagerSchema.setOtherNo(tLLCaseSet.get(m).getCaseNo());
			tLOBatchPRTManagerSchema.setStandbyFlag2("1");
			VData tVData = new VData();
			tVData.addElement(tLOBatchPRTManagerSchema);
			GlobalInput tG = new GlobalInput();
			tG=mGlobalInput;
		    tVData.addElement(tG);
		    CLaimDeclinePrtUI tCLaimDeclinePrtUI = new CLaimDeclinePrtUI();
		    XmlExport txmlExport = new XmlExport();    
		    if(!tCLaimDeclinePrtUI.submitData(tVData,"PRINT"))
		    {
		       	operFlag=false;
		       	break;
		    }
		    else
		    {    
				mResult = tCLaimDeclinePrtUI.getResult();			
			  	txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
			  	if(txmlExport==null)
			  	{
			   		operFlag=false;  
			  	}
			}
		    ExeSQL tExeSQL = new ExeSQL();
//		  获取临时文件名
		    String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
		    String strFilePath = tExeSQL.getOneValue(strSql);
		    String strVFFileName = strFilePath +tLLCaseSet.get(m).getCaseNo() +".vts";
            String strRealPath = mrealpath;
		    String strVFPathName = strRealPath +"/"+strVFFileName;
		    System.out.println("VTS的路径:"+strVFPathName);
            CombineVts tcombineVts = null;
		    if (operFlag==true){
//		    	合并VTS文件
			  	String strTemplatePath = mrealpath+ "f1print/picctemplate/";
//			  		application.getRealPath("f1print/picctemplate/") + "/";
			  	tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
			  	ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
			  	tcombineVts.output(dataStream);
			  	//把dataStream存储到磁盘文件
			  	//System.out.println("存储文件到"+strVFPathName);
			  	AccessVtsFile.saveToFile(dataStream,strVFPathName);
			  	System.out.println("==> Write VTS file to disk ");
			  	System.out.println("===strVFFileName : "+strVFFileName);
		    }
		    tInputEntry[m-1]=strVFPathName;
		  }
		mFileName="LLCD"+PubFun.getCurrentDate2()+PubFun.getCurrentTime2()+".zip";
		String tZipFile=mrealpath+ "vtsfile/"+mFileName;
	    rh.CreateZipFile(tInputEntry, tZipFile);
		return true;
	}


	private boolean dealDate2() {
		String rgtNo = mRgtNo;
		String sqlstr = "select distinct a.grpcontno ,b.mngcom from llclaimdetail a,llcase b where a.rgtno='"
			+ rgtNo
			+ "' "
			+ "and a.caseno=b.caseno and b.rgtstate in ('11','12') with ur";
	    SSRS grpcontno = tExeSQL.execSQL(sqlstr);
	    
	    mMangeCom=grpcontno.GetText(1, 2);

		if (mMangeCom.length()<4||!mMangeCom.substring(0, 4).equals("8631")) {//
			buildError("LLBatchPrtBL", "该功能只对上海地区开放！");
			return false;
		}
		
		

		if (rgtNo.toCharArray()[0] != 'P') {
			buildError("LLBatchPrtBL", "该功能只针对批次打印！");
			return false;
		}


		if (grpcontno.getMaxRow() < 1) {
			buildError("LLBatchPrtBL", "没有符合打印条件的数据，请确认是否有结案案件！");
			return false;
		}

		sqlstr = " select distinct a.customerno from llcase a,llclaimdetail b where a.RgtNo='"
				+ rgtNo
				+ "' and a.rgtno=b.rgtno"
				+ " and a.rgtstate in ('11','12') order by customerno with ur";
		SSRS customer = tExeSQL.execSQL(sqlstr);
		int num=customer.MaxRow;
		String[] tInputEntry=new String[num];
		for (int j = 1; j <= num; j++) {
			String customerno = customer.GetText(j, 1);
			VData tVData1 = new VData();
			TransferData PrintElement = new TransferData();
			PrintElement.setNameAndValue("rgtNo", rgtNo);
			PrintElement.setNameAndValue("grpcontno", grpcontno.GetText(1, 1));
			PrintElement.setNameAndValue("customerno", customerno);
			tVData1.add(PrintElement);
			ClaimDetailPrintCVSBL tClaimDetailPrintCVSBL = new ClaimDetailPrintCVSBL();
			if (!tClaimDetailPrintCVSBL.submitData(tVData1, "print")) {
				mErrors.addOneError(tClaimDetailPrintCVSBL.mErrors.getError(0));
				if (!mErrors.getError(0).functionName.equals("saveData")) {
					continue;
				} else {
					System.out.println("NewLLBatchPrtCVSBL报非中断错："
							+ mErrors.getFirstError());
				}
			}

			rh= new Readhtml();
			rh.XmlParse(tClaimDetailPrintCVSBL.getInputStream());
			String templatename = rh.getTempLateName();// 模板名字
			String templatepathname = mrealpath + "f1print/picctemplate/"
					+ templatename;// 模板名字和地址
			String outname = "LP" + rgtNo+"_"+customerno+ ".xls";
			String outpathname = mrealpath + "vtsfile/" 
					+ outname;// 该文件目录必须存在,应该约定好,统一存放,便于定期做文件清理工作
			// Commented
			// By
			// Qisl
			// At
			// 2008.10.23

			rh.setReadFileAddress(templatepathname);
			rh.setWriteFileAddress(outpathname);
			rh.start("vtsmuch");
			tInputEntry[j-1]=outpathname;

		}

		mFileName=rgtNo+"_"+PubFun.getCurrentDate()+".zip";
		String tZipFile=mrealpath+ "vtsfile/"+mFileName;
	    rh.CreateZipFile(tInputEntry, tZipFile);
		return true;
		
	}

	private boolean getInputData(VData mInputData) {
		mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mOperator = mGlobalInput.Operator;
		mMangeCom = mGlobalInput.ManageCom;
        System.out.println(mMangeCom);
		mTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);

		mRgtNo = (String) mTransferData.getValueByName("RgtNo");

		mrealpath = (String) mTransferData.getValueByName("realpath");
		
		mSCaseNo = (String) mTransferData.getValueByName("SCaseNo");
		mECaseNo = (String) mTransferData.getValueByName("ECaseNo");
		mRgtDateS = (String) mTransferData.getValueByName("RgtDateS"); 
		mRgtDateE = (String) mTransferData.getValueByName("RgtDateE"); 
		mEndDateS = (String) mTransferData.getValueByName("EndDateS"); 
		mEndDateE = (String) mTransferData.getValueByName("EndDateE");
		mSingleCaseNo = (String) mTransferData.getValueByName("SingleCaseNo");
		mBatchType = (String) mTransferData.getValueByName("BatchType");
		
		System.out.println("getInputData:" + mRgtNo + "--------");
		System.out.println("getInputData:" + mBatchType + "--------");


		return true;
	}
	
	public String getFileName() {
		return mFileName;
	}
	
	/**
	 * 追加错误信息
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLClaimCollectionBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	/**
	 * 取得返回处理过的结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		return this.mResult;
	}

	public static void main(String[] args) {

		GlobalInput tG = new GlobalInput();
		tG.Operator = "lptest";
		tG.ManageCom = "86";
		TransferData mTransferData = new TransferData();

		mTransferData.setNameAndValue("RgtNo", "P1100050707000001");
		VData tVData = new VData();
		tVData.addElement(tG);
		tVData.addElement(mTransferData);
		NewLLBatchPrtCVSBL tNewLLBatchPrtCVSBL = new NewLLBatchPrtCVSBL();
		tNewLLBatchPrtCVSBL.submitData(tVData, "PRINT");

	}

}
