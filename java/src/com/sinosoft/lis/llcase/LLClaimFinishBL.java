package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.task.*;
import com.sinosoft.lis.operfee.IndiCancelAndDueFeeBL;
import java.util.Date;

/**
 * <p>Title: 理赔结案</p>
 * <p>Description: 理赔结案时工单也要结案 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 */

public class LLClaimFinishBL
{
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mPrepaidNo = null;
    private String mGrpContNo = "";
    private String mMessage = null;    

    /**
     * 构造函数
     * @param gi GlobalInput
     * @param edorNo String
     */
    public LLClaimFinishBL(GlobalInput gi, String PrepaidNo)
    {
        this.mGlobalInput = gi;
        this.mPrepaidNo = PrepaidNo;
    }

    /**
     * 提交数据
     * @param operator String
     * @return boolean
     */
    public boolean submitData()
    {
        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }   

    /**
     * 得到错误信息
     * @return String
     */
    public String getError()
    {
        return mErrors.getFirstError();
    }

    /**
     * 返回提示信息
     * @return String
     */
    public String getMessage()
    {
        return mMessage;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {    	  
    	 String grpsql = "select a.grpcontno from LCGrpCont a, LLPrepaidClaim b"
             +" where a.GrpContNo = b.GrpContNo and b.RgtType='2'"
             +" and b.PrepaidNo = '" + this.mPrepaidNo + "' ";
    	ExeSQL grpexesql = new ExeSQL();
 	    SSRS grpSSRS = grpexesql.execSQL(grpsql);
 	    System.out.println(grpsql);
        if (grpSSRS.getMaxRow()<=0) 
        {
        	CError.buildErr(this, "预付案件保单信息查询失败");
            return false;
        }
        mGrpContNo = grpSSRS.GetText(1, 1);
    	LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
        tLJTempFeeDB.setOtherNo(mPrepaidNo);	
        tLJTempFeeSet = tLJTempFeeDB.query();
       
        if (tLJTempFeeSet.size()> 0)
        {
        	for (int j = 1 ; j <= tLJTempFeeSet.size() ; j++ )
            {
                LJTempFeeSchema tLJTempFeeSchema = tLJTempFeeSet.get(j);
                tLJTempFeeSchema.setConfDate(PubFun.getCurrentDate());
                tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
                tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
                tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());               
                mMap.put(tLJTempFeeSchema, "DELETE&INSERT");
                
                if (!getFinanceLJAPay(tLJTempFeeSchema)) {
		             CError.buildErr(this, "业务财务数据确认失败");
		             return false;
		         }                               
            }
        }
        LLPrepaidClaimDB tLLPrepaidClaimDB = new LLPrepaidClaimDB();
    	LLPrepaidClaimSchema tLLPrepaidClaimSchema = new LLPrepaidClaimSchema();
    	
    	tLLPrepaidClaimDB.setPrepaidNo(mPrepaidNo);
    	if(!tLLPrepaidClaimDB.getInfo())
    	{
    		CError.buildErr(this, "预付赔款信息查询失败!");
            return false;
    	}
    	tLLPrepaidClaimSchema = tLLPrepaidClaimDB.getSchema();
        if (!getLLPrepaid(tLLPrepaidClaimSchema)) {
            CError.buildErr(this, "业务处理数据确认失败");
            return false;
        }      	        		       	
        String sql = "update LLPrepaidClaim set RgtState = '06' where PrepaidNo ='"+ mPrepaidNo + "'";
        mMap.put(sql, "UPDATE");  
        return true;
    }
    /**
     * 财务实收数据
     * @return boolean
     */
    private boolean getFinanceLJAPay(LJTempFeeSchema aLJTempFeeSchema)
    {
    	 LJSPayDB tLJSPayDB = new LJSPayDB();
         LJSPaySet tLJSPaySet = new LJSPaySet();
         tLJSPayDB.setOtherNo(aLJTempFeeSchema.getOtherNo());         
         tLJSPayDB.setOtherNoType("Y");
         tLJSPaySet = tLJSPayDB.query();
         if(tLJSPaySet.size()>=0)
         {
  
     		String mPayNo = PubFun1.CreateMaxNo("PAYNO", null);     		
     		LJSPaySchema tLJSPaySchema = new LJSPaySchema();
     		tLJSPaySchema = tLJSPaySet.get(1);
     		LJAPaySchema tLJAPaySchema = new LJAPaySchema();	 	     
         	Reflections tref = new Reflections();
     	    tref.transFields(tLJAPaySchema, tLJSPaySchema);
     	  
     	    tLJAPaySchema.setPayNo(mPayNo);		   		       
     	    tLJAPaySchema.setIncomeNo(tLJSPaySchema.getOtherNo());
     	    tLJAPaySchema.setIncomeType(tLJSPaySchema.getOtherNoType());
     	    tLJAPaySchema.setSumActuPayMoney(tLJSPaySchema.getSumDuePayMoney());		        
     	    tLJAPaySchema.setEnterAccDate(CommonBL.getEnterAccDate(mPrepaidNo));
     	    tLJAPaySchema.setConfDate(PubFun.getCurrentDate());
     	    tLJAPaySchema.setOperator(mGlobalInput.Operator);
     	    tLJAPaySchema.setMakeDate(PubFun.getCurrentDate());
     	    tLJAPaySchema.setMakeTime(PubFun.getCurrentTime());
     	    tLJAPaySchema.setModifyDate(PubFun.getCurrentDate());
     	    tLJAPaySchema.setModifyTime(PubFun.getCurrentTime());
            mMap.put(tLJSPaySchema, "DELETE");
            mMap.put(tLJAPaySchema, "DELETE&INSERT");
            
            LJSPayBDB tLJSPayBDB = new LJSPayBDB();                
            tLJSPayBDB.setGetNoticeNo(tLJSPaySchema.getGetNoticeNo());
            if(tLJSPayBDB.getInfo())
            {
           	 LJSPayBSchema tLJSPayBSchema= tLJSPayBDB.getSchema();
           	 tLJSPayBSchema.setDealState("1");
           	 mMap.put(tLJSPayBSchema, "DELETE&INSERT");
            }
            
            LJSPayGrpDB tLJSPayGrpDB = new LJSPayGrpDB();
            LJSPayGrpSet tLJSPayGrpSet = new LJSPayGrpSet();
            tLJSPayGrpDB.setGetNoticeNo(tLJSPaySchema.getGetNoticeNo());
            tLJSPayGrpDB.setPayType("YF");                        
            tLJSPayGrpSet = tLJSPayGrpDB.query();
            if(tLJSPayGrpSet.size()>0)
            {
            	for (int m =1; m<=tLJSPayGrpSet.size(); m++)
             	{            		
                 	LJSPayGrpSchema tLJSPayGrpSchema = new LJSPayGrpSchema();
                 	tLJSPayGrpSchema = tLJSPayGrpSet.get(m);
             		LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();	 	     
                 	Reflections gtref = new Reflections();
                 	gtref.transFields(tLJAPayGrpSchema, tLJSPayGrpSchema);
             	  
                 	tLJAPayGrpSchema.setPayNo(mPayNo);		   		                               	
                 	tLJAPayGrpSchema.setSumActuPayMoney(tLJSPayGrpSchema.getSumDuePayMoney());		        
                 	tLJAPayGrpSchema.setEnterAccDate(CommonBL.getEnterAccDate(mPrepaidNo));
                 	tLJAPayGrpSchema.setConfDate(PubFun.getCurrentDate());
                 	tLJAPayGrpSchema.setOperator(mGlobalInput.Operator);
                 	tLJAPayGrpSchema.setMakeDate(PubFun.getCurrentDate());
                 	tLJAPayGrpSchema.setMakeTime(PubFun.getCurrentTime());
                 	tLJAPayGrpSchema.setModifyDate(PubFun.getCurrentDate());
                 	tLJAPayGrpSchema.setModifyTime(PubFun.getCurrentTime());
                    mMap.put(tLJSPayGrpSchema, "DELETE");
                    mMap.put(tLJAPayGrpSchema, "DELETE&INSERT");                                                
             	}
            }        	
         } 
    	return true;
    }
    /**
     * 业务账户信息实收确认
     * @return boolean
     */
    private boolean getLLPrepaid(LLPrepaidClaimSchema aLLPrepaidClaimSchema)
    {
		//修改预付保单信息
		LLPrepaidGrpContDB tLLPrepaidGrpContDB = new LLPrepaidGrpContDB();
		LLPrepaidGrpContSchema tLLPrepaidGrpContSchema = new LLPrepaidGrpContSchema();
		tLLPrepaidGrpContDB.setGrpContNo(aLLPrepaidClaimSchema.getGrpContNo());
		if(!tLLPrepaidGrpContDB.getInfo())
		{
			CError.buildErr(this, "预付保单信息查询失败!");
            return false;
		}else{
			tLLPrepaidGrpContSchema = tLLPrepaidGrpContDB.getSchema();
			tLLPrepaidGrpContSchema.setBalaDate(PubFun.getCurrentDate());
			tLLPrepaidGrpContSchema.setBalaTime(PubFun.getCurrentTime());
			System.out.println("账户保单余额=="+tLLPrepaidGrpContSchema.getPrepaidBala());
			System.out.println("账户保单已经回收金额=="+tLLPrepaidGrpContSchema.getRegainAmount());
			tLLPrepaidGrpContSchema.setPrepaidBala(Arith.round(tLLPrepaidGrpContSchema.getPrepaidBala()+aLLPrepaidClaimSchema.getSumMoney(), 2));
			tLLPrepaidGrpContSchema.setRegainAmount(Arith.round(tLLPrepaidGrpContSchema.getRegainAmount()+aLLPrepaidClaimSchema.getSumMoney(), 2));
			tLLPrepaidGrpContSchema.setModifyDate(PubFun.getCurrentDate());
			tLLPrepaidGrpContSchema.setModifyTime(PubFun.getCurrentTime());
			mMap.put(tLLPrepaidGrpContSchema, "DELETE&INSERT");
		}
		
		LLPrepaidClaimDetailDB tLLPrepaidClaimDetailDB = new LLPrepaidClaimDetailDB();
    	LLPrepaidClaimDetailSet tLLPrepaidClaimDetailSet = new LLPrepaidClaimDetailSet();
    	LLPrepaidClaimDetailSchema tLLPrepaidClaimDetailSchema = new LLPrepaidClaimDetailSchema();
    	tLLPrepaidClaimDetailDB.setPrepaidNo(mPrepaidNo);
    	tLLPrepaidClaimDetailSet = tLLPrepaidClaimDetailDB.query();
    	if(tLLPrepaidClaimDetailSet.size()<=0)
    	{
    		CError.buildErr(this, "查询预付明细信息失败!");
            return false;
    	}
		for(int index =1; index <= tLLPrepaidClaimDetailSet.size() ;index ++)
		{
			//修改预付保单险种信息
			tLLPrepaidClaimDetailSchema = tLLPrepaidClaimDetailSet.get(index);
			LLPrepaidGrpPolDB tLLPrepaidGrpPolDB = new LLPrepaidGrpPolDB();
    		LLPrepaidGrpPolSchema tLLPrepaidGrpPolSchema = new LLPrepaidGrpPolSchema();
    		tLLPrepaidGrpPolDB.setGrpPolNo(tLLPrepaidClaimDetailSchema.getGrpPolNo());
    		
    		double tPrepaidBala =0.00;
    		if(!tLLPrepaidGrpPolDB.getInfo())
    		{
    			CError.buildErr(this, "预付保单信息查询失败!");
                return false;
    		}else{
    			tLLPrepaidGrpPolSchema = tLLPrepaidGrpPolDB.getSchema();
    			tLLPrepaidGrpPolSchema.setBalaDate(PubFun.getCurrentDate());
    			tLLPrepaidGrpPolSchema.setBalaTime(PubFun.getCurrentTime());
    			tPrepaidBala = tLLPrepaidGrpPolSchema.getPrepaidBala();
    			double tRegainAmount = Arith.round(tLLPrepaidGrpPolSchema.getRegainAmount()+tLLPrepaidClaimDetailSchema.getMoney(), 2);
    			System.out.println("账户险种余额tPrepaidBala=="+tPrepaidBala);
    			System.out.println("账户险种已经回收金额tRegainAmount=="+tRegainAmount);
    			tLLPrepaidGrpPolSchema.setPrepaidBala(Arith.round(tPrepaidBala +tLLPrepaidClaimDetailSchema.getMoney(), 2));
    			tLLPrepaidGrpPolSchema.setRegainAmount(Arith.round(tRegainAmount, 2));
    			tLLPrepaidGrpPolSchema.setModifyDate(PubFun.getCurrentDate());
    			tLLPrepaidGrpPolSchema.setModifyTime(PubFun.getCurrentTime());
    			mMap.put(tLLPrepaidGrpPolSchema, "DELETE&INSERT");
    		}
    		//修改预付轨迹信息
    		LLPrepaidTraceDB tLLPrepaidTraceDB = new LLPrepaidTraceDB();
    		LLPrepaidTraceSet tLLPrepaidTraceSet = new LLPrepaidTraceSet();
    		LLPrepaidTraceSchema tLLPrepaidTraceSchema = new LLPrepaidTraceSchema();
    		tLLPrepaidTraceDB.setOtherNo(mPrepaidNo);
    		tLLPrepaidTraceDB.setOtherNoType("Y");
    		tLLPrepaidTraceDB.setState("0");
    		tLLPrepaidTraceDB.setGrpPolNo(tLLPrepaidClaimDetailSchema.getGrpPolNo());
    		tLLPrepaidTraceSet = tLLPrepaidTraceDB.query();
    		if(tLLPrepaidTraceSet.size()<=0)
    		{
    			CError.buildErr(this, "预付轨迹信息查询失败!");
                return false;
    		}else{
    			for (int j = 1;j <= tLLPrepaidTraceSet.size();j++)
    			{
    				tLLPrepaidTraceSchema = tLLPrepaidTraceSet.get(j);		    				
    				tLLPrepaidTraceSchema.setBalaDate(PubFun.getCurrentDate());
    				tLLPrepaidTraceSchema.setBalaTime(PubFun.getCurrentTime());
    				tLLPrepaidTraceSchema.setLastBala(tPrepaidBala);		    						    				
    				tLLPrepaidTraceSchema.setAfterBala(Arith.round(tPrepaidBala+tLLPrepaidClaimDetailSchema.getMoney(),2));
    				tLLPrepaidTraceSchema.setState("1");
    				tLLPrepaidTraceSchema.setModifyDate(PubFun.getCurrentDate());
    				tLLPrepaidTraceSchema.setModifyTime(PubFun.getCurrentTime());
    				mMap.put(tLLPrepaidTraceSchema, "DELETE&INSERT");
    			}
    		}
		}
    	return true;
    }
    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
    	 LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
         tLJTempFeeDB.setOtherNo(mPrepaidNo);
         LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.query();
         if (tLJTempFeeSet.size() == 0) //LJTempFee为空则不能理赔确认
         {
             mErrors.addOneError("需要财务交费！");
             return false;
         }
         String enterAccDate = tLJTempFeeSet.get(1).getEnterAccDate();
         if (enterAccDate == null) //到帐日期为空则没有交费
         {
             mErrors.addOneError("需要财务交费！");
             return false;
         }
         String tempMoney="select 1 from "
        	 +"(select otherno no,sum(SumDuePayMoney) a from ljspay where othernotype='Y' group by otherno) ljs,"
        	 +" (select otherno no,sum(PayMoney) a from ljtempfee where othernotype='Y' and enteraccdate is not null group by otherno) temp"
        	 +" where ljs.no=temp.no and ljs.a<>temp.a and ljs.no='"+ mPrepaidNo +"'";
         ExeSQL checkExeSQL = new ExeSQL();
         SSRS checkSSRS = checkExeSQL.execSQL(tempMoney);
         if(checkSSRS.getMaxRow()>0)
         {
        	 mErrors.addOneError("财务交费与实际应收金额不等！");
             return false;
         }
         
        return true;
    }
    /**
     * 检查是否已到了理赔生效日期
     * @return boolean
     */
    private boolean isValiDate(String tPrepaidNo)
    {
        String tSql =
                "select max(edorvalidate) from lpedoritem where PrepaidNo='" +
                tPrepaidNo + "'";
        ExeSQL tExeSQL = new ExeSQL();
        String lastValidate = tExeSQL.execSQL(tSql).GetText(1, 1);
        if (lastValidate == null || lastValidate.equals(""))
        {
            mErrors.addOneError("没有查到理赔生效日不能理赔确认");
            return false;
        }
        FDate chgdate = new FDate();
        Date dbdate = chgdate.getDate(PubFun.getCurrentDate());
        Date dedate = chgdate.getDate(lastValidate);

        if (dbdate.compareTo(dedate) < 0)
        {
            mErrors.addOneError("没有到理赔生效日,进入延期结案状态！");
            return false;
        }
        return true;
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    public static void main(String[] s)
    {

        GlobalInput gi = new GlobalInput();
        gi.Operator = "cm1101";
        gi.ComCode = "8611";
        gi.ManageCom = "8611";

        LLClaimFinishBL tLLClaimFinishBL = new LLClaimFinishBL(gi, "Y1100101215000007");
        if (!tLLClaimFinishBL.submitData())
        {
            System.out.println(tLLClaimFinishBL.mErrors.getErrContent());
        }

    }
}
