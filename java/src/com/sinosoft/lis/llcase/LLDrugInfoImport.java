package com.sinosoft.lis.llcase;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.diskimport.DefaultDiskImporter;
import com.sinosoft.lis.schema.LLCaseDrugInfoSchema;
import com.sinosoft.lis.vschema.LLCaseDrugInfoSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class LLDrugInfoImport {



    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGlobalInput;
    private VData mResult = new VData();
    
    private int mSuccessNum = 0;

    public int getmSuccessNum() {
		return mSuccessNum;
	}


	public void setmSuccessNum(int mSuccessNum) {
		this.mSuccessNum = mSuccessNum;
	}

    private static final String schemaClassName =
        "com.sinosoft.lis.schema.LLCaseDrugInfoSchema";
    private static final String schemaSetClassName =
        "com.sinosoft.lis.vschema.LLCaseDrugInfoSet";

    /** 节点名 */
    private String sheetName = "Sheet1";
    /** 配置文件名 */
    private String configName = "DrugInfo.xml";
    private LLCaseDrugInfoSet mLLCaseDrugInfoSet = new LLCaseDrugInfoSet(); //药品信息表
    /**
     * 构造函数，新契约入口
     * @param GrpContNo String
     * @param gi GlobalInputs
     */
    public LLDrugInfoImport(GlobalInput gi) {
        this.mGlobalInput = gi;
    }


    /**
     * 添加医药信息
     * @param path String
     * @param fileName String
     */
    public boolean doAdd(String path, String fileName) {

        //从磁盘导入数据
        DefaultDiskImporter importFile = new DefaultDiskImporter(path +
                fileName,
                path + configName,
                sheetName);
        importFile.setSchemaClassName(schemaClassName);
        importFile.setSchemaSetClassName(schemaSetClassName);
        if (!importFile.doImport()) {
            mErrors.copyAllErrors(importFile.mErrors);
            return false;
        }

        LLCaseDrugInfoSet LLCaseDrugInfoSet = (LLCaseDrugInfoSet) importFile.getSchemaSet();

        //校验导入的数据
        if (!checkData(LLCaseDrugInfoSet)) {
            return false;
        }

        //存放Insert Into语句的容器
        MMap map = new MMap();
        for (int i = 1; i <= LLCaseDrugInfoSet.size(); i++) {
        	addOneDrugInfo(LLCaseDrugInfoSet.get(i));
        	mSuccessNum = mSuccessNum + 1;
        }
        map.put(LLCaseDrugInfoSet, "INSERT");
        this.mResult.add(LLCaseDrugInfoSet);
        //提交数据到数据库
        if (!submitData(map)) {
            return false;
        }
        return true;
    }

    private void deletePayList(MMap map) {

    }

    /**
     * 校验导入数据
     * @param cLLCaseDiagnosticInfoSet
     * @return boolean
     */
    private boolean checkData(LLCaseDrugInfoSet cLLCaseDrugInfoSet) {
        for (int i = 1; i <= cLLCaseDrugInfoSet.size(); i++) {
        	LLCaseDrugInfoSchema schema = cLLCaseDrugInfoSet.get(i);
        	
            String strDrugCode = schema.getDrugCode();
            if(strDrugCode==null||strDrugCode.equals("")){
                mErrors.addOneError("缺少药品编码！");
                return false;
            }
            String strDrugCategory = schema.getDrugCategory();
            if ((strDrugCategory == null) || (strDrugCategory.equals(""))) {
                mErrors.addOneError("缺少药品类别！");
                return false;
            }

            String strDrugGenericName = schema.getDrugGenericName();
            if ((strDrugGenericName == null) || (strDrugGenericName.equals(""))) {
                mErrors.addOneError("缺少药品通用名称！");
                return false;
            }
            
//            String strFormulations = schema.getFormulations();
//            if ((strFormulations == null) || (strFormulations.equals(""))) {
//                mErrors.addOneError("缺少医保剂型！");
//                return false;
//            }
//            
//            String strSpecifications = schema.getSpecifications();
//            if ((strSpecifications == null) || (strSpecifications.equals(""))) {
//                mErrors.addOneError("缺少药品规格！");
//                return false;
//            }
//            
//            String strPackageMessage = schema.getPackageMessage();
//            if ((strPackageMessage == null) || (strPackageMessage.equals(""))) {
//                mErrors.addOneError("缺少包装信息！");
//                return false;
//            }
//            
//            String strCostLevel = schema.getCostLevel();
//            if ((strCostLevel == null) || (strCostLevel.equals(""))) {
//                mErrors.addOneError("缺少费用等级！");
//                return false;
//            }
//            
//            String strPaymentScope = schema.getPaymentScope();
//            if ((strPaymentScope == null) || (strPaymentScope.equals(""))) {
//                mErrors.addOneError("缺少限定支付范围！");
//                return false;
//            }
//            
//            String strOutpatientLimit = schema.getOutpatientLimit();
//            if ((strOutpatientLimit == null) || (strOutpatientLimit.equals(""))) {
//                mErrors.addOneError("缺少限门诊使用！");
//                return false;
//            }
//            
//            String strHospitalLimit = schema.getHospitalLimit();
//            if ((strHospitalLimit == null) || (strHospitalLimit.equals(""))) {
//                mErrors.addOneError("缺少限住院使用！");
//                return false;
//            }
//            
//            String strInjuriesMark = schema.getInjuriesMark();
//            if ((strInjuriesMark == null) || (strInjuriesMark.equals(""))) {
//                mErrors.addOneError("缺少工伤标识！");
//                return false;
//            }
//            
//            String strFertilityMark = schema.getFertilityMark();
//            if ((strFertilityMark == null) || (strFertilityMark.equals(""))) {
//                mErrors.addOneError("缺少生育标识！");
//                return false;
//            }
//            
//            String strPriceCeiling = String.valueOf(schema.getPriceCeiling());
//            if ((strPriceCeiling == null) || (strPriceCeiling.equals(""))) {
//                mErrors.addOneError("缺少最高限价！");
//                return false;
//            }
//            
            String strMngCom = schema.getMngCom();
            if ((strMngCom == null) || (strMngCom.equals(""))) {
                mErrors.addOneError("缺少管理机构！");
                return false;
            }
            
            String strAreaCode = schema.getAreaCode();
            if ((strAreaCode == null) || (strAreaCode.equals(""))) {
                mErrors.addOneError("缺少地区编码！");
                return false;
            }
            
            String tMngCom = schema.getMngCom();
            String tMngComSQL = "select name from ldcom where sign = '1' and comcode = '" + tMngCom + "'";
            String MngCom = new ExeSQL().getOneValue(tMngComSQL);
            if ((MngCom == null) || (MngCom.equals(""))) {
                mErrors.addOneError("管理机构不存在！");
                return false;
            }
            
            String tAreaCode = schema.getAreaCode();
            String tAreaCodeSQL = "select  codename from LDCode where codetype = 'hmareacode' and code = '" + tAreaCode + "'";
            String AreaCode = new ExeSQL().getOneValue(tAreaCodeSQL);
            if ((AreaCode == null) || (AreaCode.equals(""))) {
                mErrors.addOneError("地区编码不存在！");
                return false;
            }
        }
        return true;
    }

    /**
     * 处理医药信息数据
     * @param map MMap
     * @param LDHospitalSchema aLDHospitalSchema
     */
    private void addOneDrugInfo(LLCaseDrugInfoSchema cLLCaseDrugInfoSchema) {

    	LLCaseDrugInfoSchema tLLCaseDrugInfoSchema = cLLCaseDrugInfoSchema;
    	
    	tLLCaseDrugInfoSchema.setSerialNo(PubFun1.CreateMaxNo("DRUG", 20));
    	tLLCaseDrugInfoSchema.setOperator(mGlobalInput.Operator);
    	tLLCaseDrugInfoSchema.setMakeDate(PubFun.getCurrentDate());
    	tLLCaseDrugInfoSchema.setMakeTime(PubFun.getCurrentTime());
    	tLLCaseDrugInfoSchema.setModifyDate(PubFun.getCurrentDate());
    	tLLCaseDrugInfoSchema.setModifyTime(PubFun.getCurrentTime());
    	mLLCaseDrugInfoSet.add(tLLCaseDrugInfoSchema);
    }


    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submitData(MMap map) {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 返回执行结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }


}
