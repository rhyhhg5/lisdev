package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class GrpCardCvalidateModifyBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 往后面传输的数据库操作 */
    private MMap map = new MMap();
    /** 执行删除的数据库操作，放在最后 */
    private MMap mapDel = new MMap();
    /**用户登陆信息 */
    private GlobalInput mGlobalInput = new GlobalInput();
    //团体保单险种
    private LCContSchema mLCContSchema = new LCContSchema();

    private LCPolSet mLCPolSet = new LCPolSet();

    //团体保单号
    private String mGrpContNo = "";
    //被保人客户号
    private String mInsuredNo = "";
    //保单号
    private String mContNo = "";
    //生效日期
    private String mCValiDate = "";
    //失效日期
    private String mCInValiDate = "";

    public GrpCardCvalidateModifyBL() {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * @param: cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        System.out.println("after getInputData");

        //数据操作业务处理
        if (!dealData()) {
            return false;
        }

        System.out.println("after dealData");

        //准备给后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        System.out.println("after prepareOutputData");

        //数据提交
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpCardCvalidateModifyBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;

        return true;
    }

    private boolean dealData() {
        if ("UPDATE".equals(mOperate)) {
            ExeSQL tExeSQL = new ExeSQL();
            String tCInValiSQL = "select cinvalidate + CAST(days(date('" +
                                 mCValiDate +
                                 "'))-days(cvalidate) as int) days from lccont where grpcontno = '" +
                                 mGrpContNo
                                 + "' and insuredno = '" + mInsuredNo +
                                 "' and contno = '" + mContNo + "'";
            mCInValiDate = tExeSQL.getOneValue(tCInValiSQL);
            if (mCInValiDate == null || "".equals(mCInValiDate)) {
                CError tError = new CError();
                tError.moduleName = "GrpCardCvalidateModifyBL";
                tError.functionName = "getBaseData";
                tError.errorMessage = "客户" + mInsuredNo + "保单查询失败";
                mErrors.addOneError(tError);
                return false;
            }

            System.out.println("保单" + mGrpContNo + "下保单" + mContNo + "保障期维护为" +
                               mCValiDate + "至" + mCInValiDate);
            String tContSQL = "update LCCont set PayToDate = '" + mCInValiDate +
                              "', CValiDate = '" + mCValiDate +
                              "', CInValiDate = '" + mCInValiDate +
                              "', ModifyDate = Current Date, ModifyTime = Current Time,Operator = '" +
                              mGlobalInput.Operator + "' where contno='" +
                              mContNo + "'";
            String tLCPolSQL = "update LCPol set CValiDate = '" + mCValiDate +
                               "', PayEndDate = '" + mCInValiDate +
                               "', PayToDate = '" + mCInValiDate +
                               "', GetStartDate = '" + mCValiDate +
                               "', EndDate = '" + mCInValiDate +
                               "', ModifyDate = Current Date, ModifyTime = Current Time,Operator = '" +
                               mGlobalInput.Operator + "' where contno='" +
                               mContNo + "'";
            String tLCDutySQL = "update LCDuty set PayToDate = '" +
                                mCInValiDate + "', PayEndDate = '" +
                                mCInValiDate + "', EndDate = '" + mCInValiDate +
                                "', GetStartDate = '" + mCValiDate +
                                "', ModifyDate = Current Date, ModifyTime = Current Time,Operator = '" +
                                mGlobalInput.Operator + "' where contno='" +
                                mContNo + "'";
            String tLCGetSQL = "update LCGet  set GetToDate = '" + mCValiDate +
                               "', GetStartDate = '" + mCValiDate +
                               "', GetEndDate = '" + mCInValiDate +
                               "', ModifyDate = Current Date, ModifyTime = Current Time,Operator = '" +
                               mGlobalInput.Operator + "' where contno='" +
                               mContNo + "'";
            String tLCPremSQL = "update LCPrem set PayEndDate = '" +
                                mCInValiDate + "', PayToDate = '" +
                                mCInValiDate + "', paystartdate='" + mCValiDate +
                                "',ModifyDate = Current Date, ModifyTime = Current Time,Operator = '" +
                                mGlobalInput.Operator + "' where contno='" +
                                mContNo + "'";
            map.put(tContSQL, "UPDATE");
            map.put(tLCPolSQL, "UPDATE");
            map.put(tLCDutySQL, "UPDATE");
            map.put(tLCGetSQL, "UPDATE");
            map.put(tLCPremSQL, "UPDATE");
        }
        return true;
    }


    private boolean getInputData(VData cInputData) {
        mGlobalInput.setSchema(
                (GlobalInput)
                cInputData.getObjectByObjectName("GlobalInput", 0));

        mLCContSchema.setSchema(
                (LCContSchema)
                cInputData.getObjectByObjectName("LCContSchema", 0));
        mGrpContNo = mLCContSchema.getGrpContNo();

        if (mGrpContNo == null || mGrpContNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "GrpCardCvalidateModifyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "保单号空";
            mErrors.addOneError(tError);
            return false;
        }

        mInsuredNo = mLCContSchema.getInsuredNo();
        if (mInsuredNo == null || mInsuredNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "GrpCardCvalidateModifyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "被保人号码空";
            mErrors.addOneError(tError);
            return false;
        }

        mCValiDate = mLCContSchema.getCValiDate();
        if (mCValiDate == null || mCValiDate.equals("")) {
            CError tError = new CError();
            tError.moduleName = "GrpCardCvalidateModifyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "生效日期空";
            mErrors.addOneError(tError);
            return false;
        }

        mContNo = mLCContSchema.getContNo();
        if (mContNo == null || mContNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "GrpCardCvalidateModifyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "保单号空";
            mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpCardCvalidateModifyBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错:" +
                                  ex.toString();
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {

    }


}
