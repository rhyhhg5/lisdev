package com.sinosoft.lis.llcase;

import com.sinosoft.lis.vschema.LDGetDutyKindSet;
import com.sinosoft.lis.vschema.LLToClaimDutySet;

/**
 * 自动责任匹配接口
 * <p>Title: 核心业务系统</p>
 *
 * <p>Description: lis</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: SinoSoft Co. Ltd,</p>
 *
 * @author AppleWood
 * @version 6.0
 */
public interface AutoClaimDutyMap {

  /**
     自动给付责任匹配
   String caseNo 案件号
   String caseRelaNo 受理事故号
   说明: 通过案件号和受理事故号,自动匹配给付责任
   */
  public LLToClaimDutySet autoChooseGetDuty(String caseNo, String caseRelaNo);

  /**
   接口 getCaseReasons 获取案件的原因
   String caseNo 案件号
   String caseRelaNo 受理事故号
   返回: 事件发生原因:意外１，疾病２,0不区分
   说明: 通过案件号和受理事故号,查询意外信息,如有意外,返回1 ;无意外有疾病,返回2 ;即无意外又无疾病,返回0
   */
  public String getCaseReasons(String caseNo, String caseRelaNo);

  /**
   接口 getCaseResult 获取案件的原因
   String caseNo 案件号
   String caseRelaNo 受理事故号
   返回: 事件发生结果:门诊费用01，住院费用02，医疗津贴03，重大疾病04，身故05，护理06，失能07，伤残08
   说明: 通过案件号和受理事故号,查询检录内容，从而补充受理时没有录入赔付原因
   */
  public String getCaseResult(String caseNo, String caseRelaNo);

  /**
    接口getGetDutyKinds由原因和结果确定给付责任类型
   results String 结果,从LLAppClaimReason中取得,为申请原因(理赔类型),有:门诊费用 住院费用 医疗津贴 重大疾病 身故 护理 失能
   reason  String   原因:意外１，疾病２,0或null不区分
   说明: 根据原因和结果从表LDGetDutyKind中读取给付责任类型即可
   */
  public LDGetDutyKindSet getGetDutyKinds(String results, String reason);


}
