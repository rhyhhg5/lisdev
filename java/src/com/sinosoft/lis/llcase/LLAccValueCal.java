package com.sinosoft.lis.llcase;

import java.util.*;

import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: 万能险保单账户计算 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author maning
 * @version 1.1
 * @CreateDate：2010-03-23
 */
public class LLAccValueCal {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private MMap aMap = new MMap();
    private MMap tmpMap = new MMap();
    /** 全局变量 */
    private LCInsureAccSchema mLCInsureAccSchema = null;
    private LCInsureAccBalanceSet mLCInsureAccBalanceSet= new LCInsureAccBalanceSet();
    private String mOtherNO = null;
    private String mBalanceSeq = null;
    private String mStartDate = null;
    private String mEndDate = null;
    private int mPolYear = 1; //保单年度
    private int mPolMonth = 1; //保单月度
    //上次计算账户价值
    private double mLastBala = 0.0;
    //上次计算保证账户价值
    private double mLastBalaGurat = 0.0;

    private String mCalDate = "";
    private double mAccValue = 0.0;
    private double mAccValueGurat = 0.0;

    private Reflections ref = new Reflections();

    private LCPolSchema mLCPolSchema = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private GlobalInput mGI = new GlobalInput();

    public LLAccValueCal() {
    }

    /**
     * submitData
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!getInputData(cInputData)) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        return true;
    }

    /**
     * 业务处理不需要考虑保证利率，业务提供的利率不小于保证利率
     * @return boolean
     */
    public boolean dealData() {
        mLCInsureAccSchema = getCurrentAcc();
        if (mLCInsureAccSchema == null) {
            return false;
        }

        System.out.println("处理被保人" + mLCInsureAccSchema.getInsuredNo()
                           + ", 险种" + mLCInsureAccSchema.getRiskCode()
                           + ","
                           + mCalDate + "的账户价值");

        //查询帐户子表信息
        LCInsureAccClassSet tLCInsureAccClassSet = getAccClass(
                mLCInsureAccSchema);
        if (tLCInsureAccClassSet == null) {
            return false;
        }   
        
        //判断计算日所处月份与当前月份是否在同一月
        int tMonthCount = calMonth(mCalDate, mCurrentDate);
        
        //计算日在上月之前，默认是已经完成月结的，直接用该月利率计算
//        if (tMonthCount > 1) {
        //循环所有子类账户，万能只有一个
        for (int n = 1; n <= tLCInsureAccClassSet.size(); n++) {
            LCInsureAccClassSchema tLCInsureAccClassSchema =
                    tLCInsureAccClassSet.get(n);
            String tRateType = "1";
            
            //查询计算日当月利率
            LMInsuAccRateSet tLMInsuAccRateSet = getRate(mCalDate,
                    tLCInsureAccClassSchema);

            LMInsuAccRateSchema tLMInsuAccRateSchema = new LMInsuAccRateSchema();

            //取计算月第一天
            String tFirstDate[] = PubFun.calFLDate(PubFun.calDate(mCalDate, -1,
                    "D", null));
            mStartDate = tFirstDate[0];

            mEndDate = mCalDate;

            if (!calPolYearMonth()) {
                return false;
            }
            
            //#2382 持续奖金问题，万能险产品理算、审批是账户轨迹校验调整
            double tTrueLastBala = calChiXuJiangJing(mStartDate,mEndDate,tLCInsureAccClassSchema);
            
            //第一个月取账户创建日期
            if (calMonth(mCalDate, tLCInsureAccClassSchema.getAccFoundDate()) == 0) {
				mStartDate = tLCInsureAccClassSchema.getAccFoundDate();
			}

            // 查询计算日期上月的结算轨迹
            LCInsureAccBalanceSet tLCInsureAccBalanceSet = getLastBalance(
                    mStartDate, tLCInsureAccClassSchema);

            //计算日期在上个月之前，必定有利率
            if (tMonthCount > 1) {
                if (tLMInsuAccRateSet.size() == 0) {
                    buildError("dealData", "没有查询到查询计算日当月利率，不能进行结算");
                    return false;
                }
            
                tLMInsuAccRateSchema = tLMInsuAccRateSet.
                                       get(1);
                if (calMonth(mCalDate, tLMInsuAccRateSchema.getBalaDate()) != 1) {
                    buildError("dealData", "没有查询计算日当月利率，不能进行结算");
                    return false;
                }
            } else if (tMonthCount == 1) {
                //上个月如果还没有利率，用保证利率；
                if (tLMInsuAccRateSet.size() == 0) {
                    tRateType = "2";
                } else {
                    tLMInsuAccRateSchema = tLMInsuAccRateSet.
                                           get(1);
                    if (calMonth(mCalDate, tLMInsuAccRateSchema.getBalaDate()) !=
                        1) {
                        tRateType = "2";
                    }

                }
            }
           
            //不为同年同月,上个月之前必定有月结轨迹
            if (tMonthCount != 0) {
                if (tLCInsureAccBalanceSet.size() == 0) {
                    buildError("dealData", "没有查询到上一次结算信息");
                    return false;
                }
                //tLCInsureAccBalanceSet.get(1).setInsuAccBalaAfter(Double.parseDouble("23428.61"));
                mLastBala = tTrueLastBala;//计算过程见上
//                	tLCInsureAccBalanceSet.get(1).
//                            getInsuAccBalaAfter();
                mLastBalaGurat = tLCInsureAccBalanceSet.get(1).
                                 getInsuAccBalaGurat();
            } else {
                //计算当月的账户价值，如果上个月没月结，需要先结上个月
                if (tLCInsureAccBalanceSet.size() == 0) {
                    mEndDate = mStartDate;
                    mStartDate = PubFun.calDate(mStartDate, -1, "M", null);
                    if (!calPolYearMonth()) {
                        return false;
                    }
                    tLMInsuAccRateSet = getRate(mStartDate,
                                                tLCInsureAccClassSchema);
                    if (tLMInsuAccRateSet.size() == 0) {
                        tRateType = "2";
                    }
                    
                    //#2382 持续奖金问题，万能险产品理算、审批是账户轨迹校验调整
                    double tBTrueLastBala = calChiXuJiangJing(mStartDate,mEndDate,tLCInsureAccClassSchema);
                    
                    //上上个月的Balance记录
                    tLCInsureAccBalanceSet = getLastBalance(mStartDate,
                            tLCInsureAccClassSchema);
                    if (tLCInsureAccBalanceSet.size() == 0) {
                        buildError("dealData", "没有查询到上一次结算信息");
                        return false;
                    }
                    mLastBala = tBTrueLastBala;
//                    	tLCInsureAccBalanceSet.get(1).
//                                getInsuAccBalaAfter();
                    mLastBalaGurat = tLCInsureAccBalanceSet.get(1).
                                     getInsuAccBalaGurat();
                    if (!getAccValue(tLCInsureAccClassSchema, "1", tRateType)) {
                        return false;
                    }
                   
                    if (mPolMonth == 12) {
                        mAccValue = Arith.round(mAccValue +
                                                (mAccValueGurat > mAccValue ?
                                                 mAccValueGurat - mAccValue : 0),
                                                2);
                    }
                    
                    if(!getLCInsureAccBalance("1"))
                    {
                    	mErrors.addOneError("账户月结信息生成失败");
                        return false;
                    }                    
                    //准备这个月的计算数据
                    mEndDate = mCalDate;
                    mStartDate = tFirstDate[0];

                    if (!calPolYearMonth()) {
                        return false;
                    }

                    mLastBala = mAccValue;
                    //如果是计算本月账户价值且上月没结算，则mAccValue需要清0
                    mAccValue = 0.0;
                    mLastBalaGurat = mAccValueGurat;
                    //本月利率肯定没有
                    tRateType = "2";
                } else {
                    //上个月已经月结，采用保证利率算本月
                    mLastBala = tTrueLastBala;
//                    	tLCInsureAccBalanceSet.get(1).
//                                getInsuAccBalaAfter();
                    mLastBalaGurat = tLCInsureAccBalanceSet.get(1).
                                     getInsuAccBalaGurat();
                    tRateType = "2";
                }
            }

            if (!getAccValue(tLCInsureAccClassSchema, "2", tRateType)) {
                return false;
            }
        }
        return true;
    }

    /**
     * #2382 持续奖金问题，万能险产品理算、审批是账户轨迹校验调整
     * @param startDate
     * @param endDate
     * @return
     */
    private double calChiXuJiangJing(String startDate, String endDate,LCInsureAccClassSchema tLCInsureAccClassSchema) {
		// TODO 1.若出险日期所在月第一天至出险日期间存在"持续奖金",则在取账户余额时,排除该"持续奖金"
    	//#2629 同时也排除保费等其他,逻辑根据计算当月账户价值逻辑
    	String tCheckLastBalaSql = "select sum(money) from LCInsureAccTrace "
            + "where PayDate >= '" + startDate + "' "
            + "   and paydate < '" + endDate + "' "
            + "   and PolNo = '" + mLCInsureAccSchema.getPolNo() + "' "
            + "   and InsuAccNo = '"
            + mLCInsureAccSchema.getInsuAccNo() + "' "
            + "   and ((PayPlanCode = '"
            + tLCInsureAccClassSchema.getPayPlanCode() + "') or (PayPlanCode in ('231201','231001')))"
            + "   and ((OtherType not in('1', '6')) or (OtherType='6' and moneytype='B')) with ur";

        SSRS tCheckLastBalaSSRS = new ExeSQL().execSQL(tCheckLastBalaSql);
        double tBTrueLastBala = 0.00;
        if (tCheckLastBalaSSRS.getMaxRow() != 0) {
        	if(tCheckLastBalaSSRS.GetText(1, 1) == null || "null".equals(tCheckLastBalaSSRS.GetText(1, 1)) || "".equals(tCheckLastBalaSSRS.GetText(1, 1))){
        		tBTrueLastBala = 0.00;
        	}else{
        		tBTrueLastBala = Double.parseDouble(tCheckLastBalaSSRS.GetText(1, 1));//若存在持续奖金,其实际金额

        	}
        }
        //2.按照lcinsureacctrace表中和合计金额计算当前账户余额,不使用LCInsureAccBalance.InsuAccBalaAfter
        String tSumLastBalaSql = "select sum(money) from LCInsureAccTrace "
            + "where 1=1 "
            + "   and paydate < '" + endDate + "' "
            + "   and PolNo = '" + mLCInsureAccSchema.getPolNo() + "' "
            + "   and InsuAccNo = '"
            + mLCInsureAccSchema.getInsuAccNo() + "' "
            + " with ur";
        double tTrueLastBala = 0.00;
        SSRS tSumLastBalaSSRS = new ExeSQL().execSQL(tSumLastBalaSql);
        
        if (tSumLastBalaSSRS.getMaxRow() != 0) {
        	if(tSumLastBalaSSRS.GetText(1, 1) == null || "null".equals(tSumLastBalaSSRS.GetText(1, 1)) || "".equals(tSumLastBalaSSRS.GetText(1, 1))){
        		tTrueLastBala = 0.00;
        	}else{
        		tTrueLastBala = Double.parseDouble(tSumLastBalaSSRS.GetText(1, 1));

        	}
        }
        
        //3.此处只适用上个月存在月结,减去多余的"持续奖金"
        tTrueLastBala = tTrueLastBala - tBTrueLastBala;
		return tTrueLastBala;
	}

	/**
     * 计算结算的保单年度、月度
     * @return boolean
     */
    private boolean calPolYearMonth() {
        String sql = "select year(date('" + mStartDate +
                     "') - CValidate) + 1, "
                     + "   month(date('" + mStartDate + "') - CValidate) + 1 "
                     + "from LCPol "
                     + "where PolNo = '" + mLCInsureAccSchema.getPolNo() + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if (tSSRS.getMaxRow() == 0) {
            mErrors.addOneError("计算保单年度出错");
            return false;
        }
        mPolYear = Integer.parseInt(tSSRS.GetText(1, 1));
        mPolMonth = Integer.parseInt(tSSRS.GetText(1, 2));

        return true;
    }

    /**
     * getCurrentAcc
     *
     * @return LCInsureAccSchema
     */
    private LCInsureAccSchema getCurrentAcc() {
        LCInsureAccDB db = mLCInsureAccSchema.getDB();
        if (!db.getInfo()) {
            buildError("getCurrentAcc", "查询账户信息出错：客户号" + db.getInsuredNo()
                       + ", 险种号" + db.getRiskCode());
            return null;
        }

        return db.getSchema();
    }


    private MMap dealOneAccClass(LCInsureAccClassSchema aLCInsureAccClassSchema,
                                 String aBalaType, String aRatetype) {
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("StartDate", mStartDate);
        tTransferData.setNameAndValue("EndDate", mEndDate);
        tTransferData.setNameAndValue("RiskCode",
                                      aLCInsureAccClassSchema.getRiskCode());
        tTransferData.setNameAndValue("BalaType", aBalaType); //1：账户价值结算，2：保障价值计算
        tTransferData.setNameAndValue("RateType", aRatetype); //1：取公布利率，2：取保证利率
        tTransferData.setNameAndValue("PolYear", String.valueOf(mPolYear));
        tTransferData.setNameAndValue("PolMonth", String.valueOf(mPolMonth));
        tTransferData.setNameAndValue("LastBala", String.valueOf(mLastBala));
        tTransferData.setNameAndValue("LastBalaGurat",
                                      String.valueOf(mLastBalaGurat));

        VData data = new VData();
        data.add(mGI);
        data.add(aLCInsureAccClassSchema);
        data.add(tTransferData);

        MMap tMMapAV = balanceOneAccRate(data);
        return tMMapAV;
    }

    /**
     * balanceOneAccRate
     * 对子账户进行一个月的收益结算
     * @param cInputDate VData
     * @return MMap
     */
    public MMap balanceOneAccRate(VData cInputDate) {
        TransferData tTransferData = (TransferData) cInputDate
                                     .getObjectByObjectName("TransferData", 0);

        LCInsureAccClassSchema tLCInsureAccClassSchema
                = (LCInsureAccClassSchema) cInputDate
                  .getObjectByObjectName("LCInsureAccClassSchema", 0);

        tTransferData.setNameAndValue("LCInsureAccClass",
                                      tLCInsureAccClassSchema);
        tTransferData.setNameAndValue("GI", mGI);

        //计算账户收益
        AccountManage tAccountManage = new AccountManage(mLCPolSchema);
        MMap tMMapLX = tAccountManage.getBalaAccInterest(tTransferData);
        String tBalaType = (String)tTransferData.getValueByName("BalaType");
        if(tBalaType.equals("1"))
        aMap.add(tMMapLX);
        if (tMMapLX == null) {
            mErrors.copyAllErrors(tAccountManage.mErrors);
            return null;
        }

        LCInsureAccClassSchema accClassDelt
                = (LCInsureAccClassSchema) tMMapLX
                  .getObjectByObjectName("LCInsureAccClassSchema", 0);
        accClassDelt.setInsuAccBala(PubFun.setPrecision(accClassDelt.
                getInsuAccBala(), "0.00"));
        LCInsureAccTraceSchema accTraceDelt
                = (LCInsureAccTraceSchema) tMMapLX
                  .getObjectByObjectName("LCInsureAccTraceSchema", 0);
        accTraceDelt.setOtherType("6"); //月结算
        accTraceDelt.setOtherNo("");
        accTraceDelt.setMoney(PubFun.setPrecision(accTraceDelt.getMoney(),
                                                  "0.00"));
 
        //计算管理费
        MMap tMMapFee = balanceOneAccManageFee(accClassDelt, tTransferData);
        if(tBalaType.equals("1"))
        aMap.add(tMMapFee); 
        if (tMMapFee == null) {
            return null;
        }

        MMap tMMapAll = new MMap();
        tMMapAll.add(tMMapLX);
        tMMapAll.add(tMMapFee);

        return tMMapAll;
    }

    /**
     * 计算子帐户管理费，生成管理费轨迹和账户轨迹。
     * 取LMRiskFee对应账户的FeeTakePlace=5的管理费描述进行计算。
     * @param LCInsureAccClassSchema LCInsureAccClassSchema
     * @return MMap
     */
    private MMap balanceOneAccManageFee(LCInsureAccClassSchema
                                        cLCInsureAccClassSchema,
                                        TransferData tTransferData) {
        //计算管理费
        LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
        tLMRiskFeeDB.setInsuAccNo(cLCInsureAccClassSchema.getInsuAccNo());
        tLMRiskFeeDB.setPayPlanCode(cLCInsureAccClassSchema.getPayPlanCode());
        tLMRiskFeeDB.setFeeTakePlace("05"); //每月扣除
        tLMRiskFeeDB.setFeeKind("03"); //个单管理费
        LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
        if (tLMRiskFeeDB.mErrors.needDealError()) {
            System.out.println(tLMRiskFeeDB.mErrors.getErrContent());
            mErrors.addOneError("查询管理费时出现异常");
            return null;
        }

        tLMRiskFeeSet.add(getLMRiskFeeSetB(cLCInsureAccClassSchema,
                                           tTransferData));

        String tEndDate = (String) tTransferData.getValueByName("EndDate");
        if (tEndDate == null) {
            mErrors.addOneError("请传入截至日期");
            return null;
        }

        LCInsureAccClassFeeDB tAccClassFeeDB = new LCInsureAccClassFeeDB();
        tAccClassFeeDB.setPolNo(cLCInsureAccClassSchema.getPolNo());
        tAccClassFeeDB.setInsuAccNo(cLCInsureAccClassSchema.getInsuAccNo());
        tAccClassFeeDB.setPayPlanCode(cLCInsureAccClassSchema.getPayPlanCode());
        tAccClassFeeDB.setOtherNo(cLCInsureAccClassSchema.getPolNo());
        LCInsureAccClassFeeSet set = tAccClassFeeDB.query();
        if (set.size() == 0) {
            mErrors.addOneError("没有查询到账户管理费分类信息");
            return null;
        }

        MMap tMMap = new MMap();

        //计算管理费
        for (int i = 1; i <= tLMRiskFeeSet.size(); i++) {
            Calculator tCalculator = new Calculator();
            tCalculator.setCalCode(tLMRiskFeeSet.get(i).getFeeCalCode());
            tCalculator.addBasicFactor("ContNo",
                                       cLCInsureAccClassSchema.getContNo());
            tCalculator.addBasicFactor("PolNo",
                                       cLCInsureAccClassSchema.getPolNo());
            tCalculator.addBasicFactor("InsuredNo",
                                       cLCInsureAccClassSchema.getInsuredNo());
            tCalculator.addBasicFactor("InsuAccNo",
                                       cLCInsureAccClassSchema.getInsuAccNo());
            tCalculator.addBasicFactor("PayPlanCode",
                                       cLCInsureAccClassSchema.getPayPlanCode());
            
            // 计算持续奖金，新增计算要素 added by mn @20120130
            // 新增保单生效日期CValiDate 
			tCalculator
					.addBasicFactor("CValiDate", mLCPolSchema.getCValiDate());
			
			// 新增计算截止日期 CalEndDate
			tCalculator.addBasicFactor("CalEndDate", tEndDate);

//            LCPolDB tLCPolDB = new LCPolDB();
//            tLCPolDB.setPolNo(cLCInsureAccClassSchema.getPolNo());
//            tLCPolDB.getInfo();
            //新增变量Prem，支持常无忧万能的持续奖励计算。 added by huxl @20080616
            tCalculator.addBasicFactor("Prem",
                                       String.valueOf(mLCPolSchema.getPrem()));
            //新增变量Sex，保证计算风险保费的时候能够兼容保全的重算。 added by huxl @20080619
            tCalculator.addBasicFactor("Sex", mLCPolSchema.getInsuredSex());
            //新增变量Amnt，保证计算各险万能风险保费。 added by huxl @20080619
            tCalculator.addBasicFactor("Amnt",
                                       String.valueOf(mLCPolSchema.getAmnt()));
            //20101221 zhanggm 新增变量，附加险保额SubAmnt,计算附加重疾风险保费；如果存在风险加费比例，传入参数RiskRate
            String subRiskCode = ""; 
            //subRiskCode ,附加险险种代码，存储在附加险月结风险保费trace的PayPlanCode字段
			if(tLMRiskFeeSet.get(i).getFeeItemType().equals("07"))
			{
				subRiskCode = tLMRiskFeeSet.get(i).getFeeNoti();
				LCPolDB subLCPolDB = new LCPolDB();
				subLCPolDB.setContNo(mLCPolSchema.getContNo());
				subLCPolDB.setMainPolNo(mLCPolSchema.getPolNo());
				subLCPolDB.setRiskCode(subRiskCode);
				LCPolSet subLCPolSet = subLCPolDB.query();
				if(subLCPolSet==null || subLCPolSet.size()==0)
				{
					continue;
				}
				else
				{
					LCPolSchema subLCPolSchema = subLCPolSet.get(1).getSchema();
					String tSubAmnt = String.valueOf(subLCPolSchema.getAmnt());
					tCalculator.addBasicFactor("SubAmnt", tSubAmnt);
					
					String sql = "select Rate from LCPrem where PolNo = '" + subLCPolSchema.getPolNo() 
						+ "' and PayPlanCode like '00000%' order by PayPlanCode desc " ;
					String tRiskRate = new ExeSQL().getOneValue(sql);
					if(tRiskRate==null || tRiskRate.equals(""))
					{
						tCalculator.addBasicFactor("RiskRate", "0");
					}
					else
					{
						tCalculator.addBasicFactor("RiskRate", tRiskRate);
					}
				}
			}
            setCalculatorFactor(tTransferData, tCalculator);

            AccountManage tAccountManage = new AccountManage();
            String tInsuredAppAge = tAccountManage
                                    .getInsuredAppAge(mStartDate,
                    cLCInsureAccClassSchema);
            if (tInsuredAppAge == null) {
                mErrors.copyAllErrors(tAccountManage.mErrors);
                return null;
            }
            tCalculator.addBasicFactor("InsuredAppAge", tInsuredAppAge);

            double fee = Math.abs(PubFun.setPrecision(
                    Double.parseDouble(tCalculator.calculate()), "0.00"));
            if (tCalculator.mErrors.needDealError()) {
                System.out.println(tCalculator.mErrors.getErrContent());
                mErrors.addOneError("计算管理费出错");
                return null;
            }

            //管理费轨迹
            LCInsureAccFeeTraceSchema schema = new LCInsureAccFeeTraceSchema();
            ref.transFields(schema, cLCInsureAccClassSchema);

            String serNo = "";
            if(tLMRiskFeeSet.get(i).getFeeItemType().equals("07"))
			{
            	schema.setPayPlanCode(subRiskCode);
			}
            schema.setSerialNo(serNo);
            schema.setFeeCode(tLMRiskFeeSet.get(i).getFeeCode());
            schema.setFee(fee);
            schema.setOtherNo("");
            schema.setOtherType("6"); //月结算
            schema.setFeeRate(tLMRiskFeeSet.get(i).getFeeValue());
            schema.setPayDate(tEndDate);
            schema.setState("0");
            PubFun.fillDefaultField(schema);

            String moneyType = CommonBL.getCodeName("accmanagefee",
                    schema.getFeeCode());
            if (moneyType == null) {
                mErrors.addOneError("请先描述管理费的财务类型");
                return null;
            }
            schema.setMoneyType(moneyType);
            //持续奖金不进入管理费表。
            if (!moneyType.equals("B")) {
                tMMap.put(schema, SysConst.INSERT);
                //更新ClassFee
                set.get(1).setFee(set.get(1).getFee() + fee);
                set.get(1).setBalaDate(schema.getPayDate());
//                set.get(1).setOperator(mGI.Operator);
                set.get(1).setModifyDate(PubFun.getCurrentDate());
                set.get(1).setModifyTime(PubFun.getCurrentTime());
            }
            //管理费的账户轨迹
            LCInsureAccTraceSchema traceSchema = new LCInsureAccTraceSchema();
            ref.transFields(traceSchema, schema);

            String serNo2 = "";
            traceSchema.setSerialNo(serNo2);
            //只有持续奖金大于零的时候才进账户轨迹表。
            if (fee > 0 && moneyType.equals("B")) {
                traceSchema.setMoney(Math.abs(schema.getFee()));
                tMMap.put(traceSchema, SysConst.INSERT);
            } else if (!moneyType.equals("B")) {
                traceSchema.setMoney( -Math.abs(schema.getFee()));
                tMMap.put(traceSchema, SysConst.INSERT);
            }
            //更新账户分类轨迹
            cLCInsureAccClassSchema.setInsuAccBala(cLCInsureAccClassSchema.
                    getInsuAccBala()
                    + traceSchema.getMoney());
            cLCInsureAccClassSchema.setLastAccBala(cLCInsureAccClassSchema.
                    getInsuAccBala());
        }

        tMMap.put(set.get(1), SysConst.UPDATE);

        return tMMap;
    }


    /**
     * getLMRiskFeeSetB
     *
     * @param cLCInsureAccClassSchema LCInsureAccClassSchema
     * @return Schema
     */
    private LMRiskFeeSet getLMRiskFeeSetB(LCInsureAccClassSchema
                                          cLCInsureAccClassSchema,
                                          TransferData tTransferData) {
//        String tYear = (String) tTransferData.getValueByName("PolYear");
        String tMonth = (String) tTransferData.getValueByName("PolMonth");
        //满保单年度时才处理持续奖金.
		if (!"12".equals(tMonth)) {
			return new LMRiskFeeSet();
		}
		// 由于通过产品描述来判断较为复杂,暂时写死,先开发331201的持续奖金,后续调整为ldcode中描述
		if (!"331201".equals(cLCInsureAccClassSchema.getRiskCode())) {
			return new LMRiskFeeSet();
		}

        LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
        tLMRiskFeeDB.setInsuAccNo(cLCInsureAccClassSchema.getInsuAccNo());
        tLMRiskFeeDB.setPayPlanCode(cLCInsureAccClassSchema.getPayPlanCode());
        tLMRiskFeeDB.setFeeTakePlace("07"); //每月扣除
        tLMRiskFeeDB.setFeeKind("03"); //个单管理费
        LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();

        return tLMRiskFeeSet;
    }


    private void setCalculatorFactor(TransferData tTransferData,
                                     Calculator tCalculator) {
        Vector tVector = tTransferData.getValueNames();
        for (int i = 0; i < tVector.size(); i++) {
            Object tNameObject = null;
            Object tValueObject = null;
            try {
                tNameObject = tVector.get(i);
                tValueObject = tTransferData.getValueByName(tNameObject);
                tCalculator.addBasicFactor((String) tNameObject,
                                           (String) tValueObject);
            } catch (Exception ex) {
                //对象无法转换为字符串，不需要做处理
            }
        }
    }

    private LCInsureAccClassSet getAccClass(LCInsureAccSchema
                                            tLCInsureAccSchema) {
        LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
        tLCInsureAccClassDB.setPolNo(tLCInsureAccSchema.getPolNo());
        tLCInsureAccClassDB.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo());
        LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.query();
        if (tLCInsureAccClassDB.mErrors.needDealError()) {
            buildError("getAccClass", "子帐户信息查询失败");
            return null;
        }
        if (tLCInsureAccClassSet == null || tLCInsureAccClassSet.size() < 1) {
            buildError("getAccClass", "没有子帐户信息");
            return null;
        }
        return tLCInsureAccClassSet;
    }

    /**
     * 传递参数
     * 准备账户号码和账户结算日期
     * @param cInputData VData
     * @return boolean
     */
    public boolean getInputData(VData cInputData) {
        mLCInsureAccSchema = (LCInsureAccSchema) cInputData
                             .getObjectByObjectName("LCInsureAccSchema", 0);
        mGI = (GlobalInput) cInputData
              .getObjectByObjectName("GlobalInput", 0);
        if (mLCInsureAccSchema != null) {
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(mLCInsureAccSchema.getPolNo());
            if (tLCPolDB.getInfo()) {
                mLCPolSchema = tLCPolDB.getSchema();
            }

            mCalDate = mLCInsureAccSchema.getBalaDate();
        }
        return true;
    }

    /**
     * 错误构建方法
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LLAccValueCal";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    /**
     * 返回账户价值
     * @return double
     */
    public MMap getBalanceValue() {
        return tmpMap;
    }
    /**
     * 返回账户价值
     * @return double
     */
    public double getAccValue() {
        return mAccValue;
    }
    /**
     * 返回未结算上月账户轨迹信息
     * @return double
     */
    public MMap getAccTraceValue() {
        return aMap;
    }

    /**
     * 判断两个日期是否同一个自然月
     * @param startDate Date
     * @param endDate Date
     * @return int 相差次数
     */
    public int calMonth(String cstartDate, String cendDate) {
        String tStartDate = PubFun.calDate(cstartDate, -1, "D", null);

        FDate fDate = new FDate();
        Date startDate = fDate.getDate(tStartDate);
        Date endDate = fDate.getDate(cendDate);
        if (fDate.mErrors.needDealError()) {
            return 0;
        }

        int interval = 0;

        GregorianCalendar sCalendar = new GregorianCalendar();
        sCalendar.setTime(startDate);
        int sYears = sCalendar.get(Calendar.YEAR);
        int sMonths = sCalendar.get(Calendar.MONTH);

        GregorianCalendar eCalendar = new GregorianCalendar();
        eCalendar.setTime(endDate);
        int eYears = eCalendar.get(Calendar.YEAR);
        int eMonths = eCalendar.get(Calendar.MONTH);

        interval = eYears - sYears;
        interval = interval * 12;

        interval = eMonths - sMonths + interval;
        return interval;
    }

    /**
     * 查询指定月的利率
     * @param aCalDate String
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @return LMInsuAccRateSet
     */
    private LMInsuAccRateSet getRate(String aCalDate,
                                     LCInsureAccClassSchema
                                     aLCInsureAccClassSchema) {
        String tCurrentRateSQL = " select a.* from LMInsuAccRate a "
                                 + " where RateType = 'C' "
                                 + " and RateIntv = 1 "
                                 + " and RateIntvUnit = 'Y' "
                                 + " and a.BalaDate >= '"
                                 + aCalDate + "' "
                                 + " and RiskCode = '"
                                 + aLCInsureAccClassSchema.getRiskCode() +
                                 "' "
                                 + "   and InsuAccNo = '"
                                 + aLCInsureAccClassSchema.getInsuAccNo() +
                                 "' order by a.BalaDate "
                                 + " fetch first 1 rows only";
        System.out.println(tCurrentRateSQL);
        LMInsuAccRateDB tLMInsuAccRateDB = new LMInsuAccRateDB();
        LMInsuAccRateSet tLMInsuAccRateSet = tLMInsuAccRateDB.
                                             executeQuery(tCurrentRateSQL);
        return tLMInsuAccRateSet;
    }
    /**
     * 查询指定月的利率
     * @param aCalDate String
     * @param aLCInsureAccSchema LCInsureAccSchema
     * @return LMInsuAccRateSet
     */
    private boolean getLCInsureAccBalance(String atype)
    {
    	 LCInsureAccBalanceDB aLCInsureAccBalanceDB= new LCInsureAccBalanceDB();    	 
         LCInsureAccBalanceSchema aLCInsureAccBalanceSchema= new LCInsureAccBalanceSchema();
         
         String[] dateArr = PubFun.getCurrentDate().split("-");
         String date = dateArr[0] + dateArr[1] + dateArr[2];
         mBalanceSeq = date + PubFun1.CreateMaxNo("BalaSeq" + date, 10);
         aLCInsureAccBalanceDB.setPolNo(mLCInsureAccSchema.getPolNo());
         aLCInsureAccBalanceDB.setInsuAccNo(mLCInsureAccSchema.getInsuAccNo());
         LCInsureAccBalanceSet aLCInsureAccBalanceSet = aLCInsureAccBalanceDB.query();
         if(aLCInsureAccBalanceSet.size()>=1)
         {         	         
	         aLCInsureAccBalanceSchema = aLCInsureAccBalanceSet.get(1);
	         aLCInsureAccBalanceSchema.setSequenceNo(mBalanceSeq);
	         String BalaCount ="select max(balacount) from LCInsureAccBalance where polno='"+mLCInsureAccSchema.getPolNo()+"'"
	         			+" and InsuAccNo='"+mLCInsureAccSchema.getInsuAccNo()+ "'";
	         SSRS tBalSSRS = new ExeSQL().execSQL(BalaCount);
	         if(tBalSSRS.getMaxRow()<=0)
	         {
	        	 buildError("getAccClass", "账户月结信息查询失败");
	             return false;
	         }	 
	         aLCInsureAccBalanceSchema.setBalaCount(Integer.parseInt(tBalSSRS.GetText(1, 1))+1);
	         aLCInsureAccBalanceSchema.setDueBalaDate(mStartDate);
	         if(atype.equals("2"))
	         {
	        	 aLCInsureAccBalanceSchema.setDueBalaDate(mEndDate);
	         }	         
	         aLCInsureAccBalanceSchema.setRunDate(PubFun.getCurrentDate());
	         aLCInsureAccBalanceSchema.setInsuAccNo("000000");	//临时 给付确认时按照正常值600000
	         aLCInsureAccBalanceSchema.setRunTime(PubFun.getCurrentTime());
	         aLCInsureAccBalanceSchema.setPolYear(mPolYear);
	         aLCInsureAccBalanceSchema.setPolMonth(mPolMonth);
	         aLCInsureAccBalanceSchema.setInsuAccBalaBefore(mLastBala);
	         aLCInsureAccBalanceSchema.setInsuAccBalaAfter(mAccValue);
	         aLCInsureAccBalanceSchema.setInsuAccBalaGurat(mLastBalaGurat);
	         aLCInsureAccBalanceSchema.setOperator(mGI.Operator);                    
	         aLCInsureAccBalanceSchema.setMakeDate(PubFun.getCurrentDate());
	         aLCInsureAccBalanceSchema.setMakeTime(PubFun.getCurrentTime());
	         aLCInsureAccBalanceSchema.setModifyDate(PubFun.getCurrentDate());
	         aLCInsureAccBalanceSchema.setModifyTime(PubFun.getCurrentTime());	
	         tmpMap.put(aLCInsureAccBalanceSchema,"INSERT");
         }
         else return false;        	
       
    	return true;
    }
    /**
     * 查询指定月前一个月的结算轨迹
     * @param aStartDate String
     * @param aLCInsureAccClassSchema LCInsureAccClassSchema
     * @return LCInsureAccBalanceSet
     */
    private LCInsureAccBalanceSet getLastBalance(String aStartDate,
                                                 LCInsureAccClassSchema
                                                 aLCInsureAccClassSchema) {
        LCInsureAccBalanceDB tLCInsureAccBalanceDB = new
                LCInsureAccBalanceDB();
        tLCInsureAccBalanceDB.setPolNo(aLCInsureAccClassSchema.getPolNo());
        tLCInsureAccBalanceDB.setInsuAccNo(aLCInsureAccClassSchema.
                                           getInsuAccNo());
        tLCInsureAccBalanceDB.setDueBalaDate(aStartDate);
        LCInsureAccBalanceSet tLCInsureAccBalanceSet
                = tLCInsureAccBalanceDB.query();
        return tLCInsureAccBalanceSet;
    }

    /**
     * 计算账户价值
     * @param aLCInsureAccClassSchema LCInsureAccClassSchema
     * @param aBalaType String 1-不用计算保证收益 2- 理赔必须计算
     * @param aRateType String
     * @return boolean
     */
    private boolean getAccValue(LCInsureAccClassSchema
                                aLCInsureAccClassSchema, String aBalaType,
                                String aRateType) {
        System.out.println("计算账户价值");
        LCInsureAccClassSchema schemaAV = null; //处理后的账户分类价值
        LCInsureAccTraceSchema schemaTraceOne = null; //本次产生的任一个轨迹
        MMap tMMapAV = dealOneAccClass(aLCInsureAccClassSchema,
                                       "1", aRateType);
        if (tMMapAV == null) {
            return false;
        }

        schemaAV = (LCInsureAccClassSchema) tMMapAV
                   .getObjectByObjectName("LCInsureAccClassSchema", 0);   
        schemaTraceOne = (LCInsureAccTraceSchema) tMMapAV
        .getObjectByObjectName(
                "LCInsureAccTraceSchema", 0);
        if (schemaAV == null) {
            buildError("dealData", "没有计算出账户价值");
            return false;
        }
        double tAccValue = schemaAV.getInsuAccBala();
        mAccValue = Arith.round(mAccValue + tAccValue, 2);

        //如果是保单年度最后一个月且是最后一天，计算保证收益
        if (mPolMonth == 12 || "2".equals(aBalaType)) {

            System.out.println("计算账户保证价值");
            MMap tMMapGurat = dealOneAccClass(aLCInsureAccClassSchema,
                                              "2", "2");
            if (tMMapGurat == null) {
                mErrors.addOneError("计算保证收益出错");
                return false;
            }
            LCInsureAccClassSchema schema
                    = (LCInsureAccClassSchema) tMMapGurat
                      .getObjectByObjectName("LCInsureAccClassSchema",
                                             0);
            if (schema == null) {
                mErrors.addOneError("没有计算出保证账户价值");
                return false;
            }
            double tAccValueGurat = schema.getInsuAccBala();
            mAccValueGurat = Arith.round(tAccValueGurat, 2);
            if ("2".equals(aBalaType)) {
            	double dif =   tAccValueGurat-tAccValue;
                if (dif > 0)
                {
                	 mAccValue = Arith.round(mAccValue + (tAccValueGurat > tAccValue ?
                		                       tAccValueGurat - tAccValue : 0), 2);
                    dif = Math.abs(PubFun.setPrecision(dif, "0.00"));
                    LCInsureAccTraceSchema accTrace = new LCInsureAccTraceSchema();
                    ref.transFields(accTrace, schemaTraceOne);
                    accTrace.setMoney(dif);
                    accTrace.setMoneyType("VD"); //年度收益补差
                    accTrace.setState("temp");
                    accTrace.setOtherNo(this.mOtherNO);
                    accTrace.setOperator(mGI.Operator);


                    String tLimit = PubFun.getNoLimit(accTrace.getManageCom());
                    String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
                    accTrace.setSerialNo(serNo);
                    aMap.put(accTrace, SysConst.INSERT);
                }
            }
        }
        if(aBalaType.equals("2"))
        {
            if(!getLCInsureAccBalance("2"))
            {
            	mErrors.addOneError("账户月结信息生成失败");
                return false;
            }	
        }  
        return true;
    }

    public static void main(String arg[]) {
        LLAccValueCal t = new LLAccValueCal();
        System.out.println(t.calMonth("2010-3-2", "2010-3-10"));
        GlobalInput tGI = new GlobalInput();
        MMap ttMMap =new MMap();
        tGI.ManageCom = "86110000";
        tGI.Operator = "001";

        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
        tLCInsureAccDB.setPolNo("21000619643");
        tLCInsureAccDB.setInsuAccNo("700000");
   
        LCInsureAccSchema cLCInsureAccSchema = new LCInsureAccSchema();
        cLCInsureAccSchema.setInsuAccNo("700000");
        cLCInsureAccSchema.setPolNo("21000619643");
        cLCInsureAccSchema.setBalaDate("2011-10-1");
    	
    	TransferData tTransferData =new TransferData(); 
    	tTransferData.setNameAndValue("PolMonth", "12");
    	tTransferData.setNameAndValue("EndDate", "2011-10-01");
        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(cLCInsureAccSchema);
        t.submitData(tVData,""); 
        {
           
        }
    }

	public String getMOtherNO() {
		return mOtherNO;
	}

	public void setMOtherNO(String otherNO) {
		mOtherNO = otherNO;
	}
}
