package com.sinosoft.lis.llcase;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p>Title: Web业务系统投保暂交费功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author YT
 * @version 1.0
 */

public class RgtSurveyBLS
{
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();

  public RgtSurveyBLS() {}

  public static void main(String[] args)
  {
  }

  //传输数据的公共方法

  public boolean submitData(VData cInputData,String cOperate)
  {
  	boolean tReturn = false;
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    System.out.println("Start RgtSurvey BLS Submit...");

    if (!this.saveData())
      return false;
    System.out.println("End RgtSurvey BLS Submit...");
    mInputData=null;
    return true;
  }

  private boolean saveData()
  {
    LLSurveySchema mLLSurveySchema = (LLSurveySchema)mInputData.getObjectByObjectName("LLSurveySchema",0);
    Connection conn = DBConnPool.getConnection();

    if (conn==null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "RgtSurveyBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    try
    {
      conn.setAutoCommit(false);
			// 保存现在的关联
			LLSurveyDB mLLSurveyDB = new LLSurveyDB( conn );
      mLLSurveyDB.setSchema(mLLSurveySchema);
			if (mLLSurveyDB.insert() == false)
			{
        // @@错误处理
        this.mErrors.copyAllErrors(mLLSurveyDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "RgtSurveyBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "保存数据失败!";
				this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
				return false;
			}

      LLRegisterDB tLLRegisterDB = new LLRegisterDB(conn);
//      tLLRegisterDB.setRgtNo(mLLSurveySchema.getRgtNo());
      if(tLLRegisterDB.getInfo())
      {
        tLLRegisterDB.setClmState("5");
        if(!tLLRegisterDB.update())
        {
          this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "LLRegisterUpdateBL";
          tError.functionName = "DeleteData";
          tError.errorMessage = "立案中的案件状态置入失败";
          this.mErrors.addOneError(tError);
          conn.rollback() ;
          conn.close();
          return false;
        }
//       LLClaimDB tLLClaimDB = new LLClaimDB(conn);
//       LLClaimSet tLLClaimSet = new LLClaimSet();
//       tLLClaimDB.setRgtNo(mLLSurveySchema.getRgtNo());
//       tLLClaimSet=tLLClaimDB.query();
//       if (tLLClaimSet.get(1)!=null)
//       {
//         System.out.println("rgtno=="+tLLClaimSet.get(1).getRgtNo());
//         tLLClaimDB.setSchema(tLLClaimSet.get(1));
//         tLLClaimDB.setClmState("5");
//         if(!tLLClaimDB.update())
//         {
//           this.mErrors.copyAllErrors(tLLClaimDB.mErrors);
//           CError tError = new CError();
//           tError.moduleName = "RgtSurveyBLS";
//           tError.functionName = "saveData";
//           tError.errorMessage = "赔案中的案件状态置入失败";
//           this.mErrors.addOneError(tError);
//           conn.rollback() ;
//           conn.close();
//           return false;
//         }
//       }

      }

  		conn.commit() ;
			conn.close();
		} // end of try
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "RgtSurveyBLS";
			tError.functionName = "submitData";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);
                        try
                        {
                          conn.rollback() ;
                          conn.close();
                        }
                        catch(Exception e){}
                        return false;
                }
    		return true;
  }
}


