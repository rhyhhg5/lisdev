/*
 * <p>ClassName: LLCaseReturnBL </p>
 * <p>Description: LLCaseReturnBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author: YangMing
 * @CreateDate：2005-02-23 11:53:36
 */
package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LLCaseReturnBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private LLCaseBackSchema mLLCaseBackSchema = new LLCaseBackSchema();
    
    /** 社保保单标志 0：非社保；1：社保*/
    private String mSocialSecurity = "0";
//private LLCaseSet mLLCaseSet=new LLCaseSet();
    public LLCaseReturnBL() {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */

    public static void main(String[] args) {
        LLCaseSchema tmLLCaseSchema = new LLCaseSchema();
        LLCaseBackSchema tLLCaseBackSchema = new LLCaseBackSchema();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "cm0001";
        tG.ManageCom = "86";
        tmLLCaseSchema.setCaseNo("C3100060519000032");
        tmLLCaseSchema.setRgtState("01");
        tLLCaseBackSchema.setCaseNo("C3100060519000032");
        tLLCaseBackSchema.setBeforState("09");
        tLLCaseBackSchema.setAfterState("01");
        VData tVData = new VData();
        tVData.add(tmLLCaseSchema);
        tVData.add(tLLCaseBackSchema);
        tVData.add(tG);

        LLCaseReturnBL tLLCaseReturnBL = new LLCaseReturnBL();
        tLLCaseReturnBL.submitData(tVData, "INSERT||MAIN");
    }

    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        System.out.println("BL Submit getinputdata");
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        System.out.println("BL Submit dealData");
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseReturnBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LLCaseReturnBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, null)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLCaseReturnBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        if (mOperate.equals("INSERT||MAIN")) {
            LLCaseDB tLLCaseDB = new LLCaseDB();
            LLCaseSchema tLLCaseSchema = new LLCaseSchema();
            tLLCaseDB.setSchema(mLLCaseSchema);
            
            if (!tLLCaseDB.getInfo()) {
                this.mErrors.copyAllErrors(tLLCaseDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LLCaseReturnBL";
                tError.functionName = "dealData";
                tError.errorMessage = "数据查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLLCaseSchema = tLLCaseDB.getSchema();
            //对结案时间操作
            if (!(tLLCaseSchema.getEndCaseDate() == null ||
                  tLLCaseSchema.getEndCaseDate().equals(""))) {
                tLLCaseSchema.setEndCaseDate("");
            }

            if (!LLCaseCommon.checkHospCaseState(tLLCaseSchema.getCaseNo())) {
                CError.buildErr(this, "医保通案件待确认，不能进行处理");
                return false;
            }
            
            String sql = "select rgtstate from llcase where caseno='" +
            	mLLCaseSchema.getCaseNo() + "'";
            ExeSQL tExeSQL = new ExeSQL();
            String rgtState = tExeSQL.getOneValue(sql);
            System.out.println(rgtState);
            
            //判断案件是否是社保案件，若是采用社保的回退处理逻辑
            String tCaseNo = tLLCaseSchema.getCaseNo();
            if("R".equals(tCaseNo.substring(0,1)) || "S".equals(tCaseNo.substring(0,1)))
        	{
        		System.out.println("申诉纠错案件换为正常的C案件");
                String tSql = "select caseno from LLAppeal where appealno ='"+tCaseNo+"'";
                ExeSQL exeSQL = new ExeSQL();
                tCaseNo = exeSQL.getOneValue(tSql);
        	}
            mSocialSecurity = LLCaseCommon.checkSocialSecurity(tCaseNo, null);
            if("1".equals(mSocialSecurity)){
            	LLSocialClaimUserDB aLLSocialClaimUserDB = new LLSocialClaimUserDB();
    			LLSocialClaimUserSet tSocialClaimUserSet = new LLSocialClaimUserSet();
    			//社保部的人可以回退“理算、审批、审定、抽检、结案”状态的案件,可以回退到“理算”、“审批”
    			//社保案件还可以回退“查讫”案件，不能回退到提调状态之后的状态
    			if("08".equals(rgtState)){
    				//案件状态为“查讫”,需要查询案件提调时的状态,需要排除多次在”查讫状态“提调的问题
    				String tSql = "select * from llsurvey a where a.otherno='"+mLLCaseSchema.getCaseNo()
    	    		+"' and a.surveyflag='3' and a.rgtstate <> '08' order by surveyno desc fetch first 1 rows only with ur";
    	    	
    		    	LLSurveyDB tLLSurveyDB = new LLSurveyDB();
    		        LLSurveySet tLLSurveySet = new LLSurveySet();
    		        tLLSurveySet = tLLSurveyDB.executeQuery(tSql);
    		        System.out.println("tSql:"+tSql);
    		        if (tLLSurveySet.size() <= 0) {
    		        	
    		        	CError.buildErr(this, "调查信息查询失败");
    		            return false;
    		        }
    		        LLSurveySchema tLLSurveySchema = new LLSurveySchema();
    		        tLLSurveySchema = tLLSurveySet.get(1);
    		        String tRgtState = "" + tLLSurveySchema.getRgtState();
    		        System.out.println("调查案件提调前状态："+tRgtState);
    		        rgtState = tRgtState;
    			}
    			if(("04".equals(rgtState) || "05".equals(rgtState) || "06".equals(rgtState) || 
    					"10".equals(rgtState) || "09".equals(rgtState)) 
    					&& ("04".equals(mLLCaseSchema.getRgtState()) || "05".equals(mLLCaseSchema.getRgtState()))){
    				String tSQL = "select * from llsocialclaimuser where stateflag='1' and handleflag='1' " +
					" and usercode='"+mLLCaseBackSchema.getNHandler()+"' and upusercode='"+this.mGlobalInput.Operator+"'";
    				System.out.println("tSQL:"+tSQL);
    				tSocialClaimUserSet = aLLSocialClaimUserDB.executeQuery(tSQL);			
    				if (tSocialClaimUserSet == null || tSocialClaimUserSet.size() == 0) {
    					this.mErrors.copyAllErrors(tSocialClaimUserSet.mErrors);
    					CError tError = new CError();
    					tError.moduleName = "LLCaseReturnBL";
    					tError.functionName = "dealData";
    					tError.errorMessage = "你没有权限回退这个案件!";
    					this.mErrors.addOneError(tError);
    					return false;
    				}   
    				if(!checkReturnState(rgtState,mLLCaseSchema.getRgtState())){
    					return false;
    				}
    			}else if(("03".equals(rgtState)&&"01".equals(mLLCaseSchema.getRgtState())&&this.mGlobalInput.Operator.equals(tLLCaseSchema.getRigister())) || (!"03".equals(rgtState)&&"01".equals(mLLCaseSchema.getRgtState()))&&"ss".equals(this.mGlobalInput.Operator.substring(0, 2))){
    				//案件状态为检录时，只能由该案件运营部操作人进行回退，且只能回退给案件的受理人员,同时不可以更新handler
    				String tSQL = "select * from llsocialclaimuser a where a.stateflag='1' " +
						" and a.usercode='"+this.mGlobalInput.Operator+"' ";
    				System.out.println("tSQL:"+tSQL);
    				tSocialClaimUserSet = aLLSocialClaimUserDB.executeQuery(tSQL);			
    				if (tSocialClaimUserSet == null || tSocialClaimUserSet.size() == 0) {
    					this.mErrors.copyAllErrors(tSocialClaimUserSet.mErrors);
    					CError tError = new CError();
    					tError.moduleName = "LLCaseReturnBL";
    					tError.functionName = "dealData";
    					tError.errorMessage = "你没有权限回退这个案件!";
    					this.mErrors.addOneError(tError);
    					return false;
    				} else{
    					if(mLLCaseBackSchema.getNHandler().equals(tLLCaseSchema.getRigister())){
    						mLLCaseSchema.setHandler(tLLCaseSchema.getHandler());//案件原处理人
    					}else{
    						CError tError = new CError();
        					tError.moduleName = "LLCaseReturnBL";
        					tError.functionName = "dealData";
        					tError.errorMessage = "社保案件回退到受理状态时只能回退给案件受理人，本案件受理人为："+tLLCaseSchema.getRigister()+"!";
        					this.mErrors.addOneError(tError);
        					return false;
    					}
    					if(!checkReturnState(rgtState,mLLCaseSchema.getRgtState())){
        					return false;
        				}
    				}		
    			}else{
    				CError tError = new CError();
                	tError.moduleName = "LLCaseReturnBL";
                	tError.functionName = "dealData";
                	tError.errorMessage = "该案件状态或登录用户不准执行此操作!";
                	this.mErrors.addOneError(tError);
                	return false;
    			}    			
            }else{
                if ("07".equals(rgtState) || "11".equals(rgtState) ||
                		"12".equals(rgtState) || "14".equals(rgtState) ||"13".equals(rgtState) ||//新增延迟状态不能回退 by zhangzhonghao
                		"".equals(rgtState) || "null".equals(rgtState) || rgtState == null) {
                	CError tError = new CError();
                	tError.moduleName = "LLCaseReturnBL";
                	tError.functionName = "dealData";
                	tError.errorMessage = "该案件状态不准执行此操作!";
                	this.mErrors.addOneError(tError);
                	return false;
                }
               if("08".equals(rgtState)){
    				//案件状态为“查讫”,需要查询案件提调时的状态
    				String tSql = "select * from llsurvey a where a.otherno='"+mLLCaseSchema.getCaseNo()
    	    		+"' and a.surveyflag='3' and a.rgtstate <> '08' order by surveyno desc fetch first 1 rows only with ur";
    	    	
    		    	LLSurveyDB tLLSurveyDB = new LLSurveyDB();
    		        LLSurveySet tLLSurveySet = new LLSurveySet();
    		        tLLSurveySet = tLLSurveyDB.executeQuery(tSql);
    		        System.out.println("tSql:"+tSql);
    		        if (tLLSurveySet.size() <= 0) {
    		        	
    		        	CError.buildErr(this, "调查信息查询失败");
    		            return false;
    		        }
    		        LLSurveySchema tLLSurveySchema = new LLSurveySchema();
    		        tLLSurveySchema = tLLSurveySet.get(1);
    		        String tRgtState = "" + tLLSurveySchema.getRgtState();
    		        rgtState = tRgtState;    		        
               }
               if ("01".equals(rgtState) || "02".equals(rgtState) || "03".equals(rgtState) || "04".equals(rgtState)) {
            	   
            	    if(!this.mGlobalInput.Operator.equals(tLLCaseSchema.getHandler()) ||
            	    		!mLLCaseBackSchema.getNHandler().equals(tLLCaseSchema.getHandler())){
            	    	CError tError = new CError();
    	               	tError.moduleName = "LLCaseReturnBL";
    	               	tError.functionName = "dealData";
    	               	tError.errorMessage = "该案件状态下，只能由案件原处理人回退给其本身，案件原处理人为："+tLLCaseSchema.getHandler()+"!";
    	               	this.mErrors.addOneError(tError);
    	               	return false;
            	    }else{
            	    	mLLCaseSchema.setHandler(tLLCaseSchema.getHandler());//案件原处理人
            	    }
	               	
               }else if("05".equals(mLLCaseSchema.getRgtState())){

         	    	//2967 回退功能调整若回退到审批状态时，案件原处理人不变更
         	   		String tLLClaimUWMainSQL = "select * from LLClaimUWMain where caseno='"+mLLCaseSchema.getCaseNo()+"' with ur";
         	   		String tLDSpotTrackSQL = "select * from LDSpotTrack where OtherNo='"+mLLCaseSchema.getCaseNo()+"' and OtherType='CaseNo' with ur";
                	LLClaimUWMainDB tLLClaimUWMainDB = new LLClaimUWMainDB();
                	LLClaimUWMainSchema tLLClaimUWMainSchema = new LLClaimUWMainSchema();
                	LLClaimUWMainSet tLLClaimUWMainSet = new LLClaimUWMainSet();
                	LDSpotTrackDB tLDSpotTrackDB = new LDSpotTrackDB();
                	LDSpotTrackSchema tLDSpotTrackSchema = new LDSpotTrackSchema();
                	LDSpotTrackSet tLDSpotTrackSet= new LDSpotTrackSet();
                	tLLClaimUWMainSet = tLLClaimUWMainDB.executeQuery(tLLClaimUWMainSQL);
                	tLLClaimUWMainSchema = tLLClaimUWMainSet.get(1);
                	tLLClaimUWMainSchema.setAppClmUWer(mLLCaseSchema.getHandler()); //回退给上级审批人。
                	tLDSpotTrackSet = tLDSpotTrackDB.executeQuery(tLDSpotTrackSQL);
                	tLDSpotTrackSchema = tLDSpotTrackSet.get(1);
                	if(tLDSpotTrackSchema!=null){
                		map.put(tLDSpotTrackSchema, "DELETE");
                	}
            		//判断是不是由这个案件审核人的上级来回退这个案件，此处需要支持特需上级对其下级案件的回退
                	LLClaimUserSet tLLClaimUserSet = new LLClaimUserSet();
                	LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
                	String strSql = "select * from Llclaimuser where usercode = '"
                                + mLLCaseBackSchema.getNHandler() +
                                "' and (upusercode = '" + this.mGlobalInput.Operator +
                                "' or specialneedupusercode = '"+this.mGlobalInput.Operator+"')";
                	System.out.println(strSql);
                	tLLClaimUserSet = tLLClaimUserDB.executeQuery(strSql);
                	//System.out.println(strSql+tLLClaimUserSet+tLLClaimUserSet.size())
                	mLLCaseSchema.setHandler(tLLCaseSchema.getHandler());//案件原处理人
                	if (tLLClaimUserSet == null || tLLClaimUserSet.size() == 0) {
                		this.mErrors.copyAllErrors(tLLClaimUserDB.mErrors);
                		CError tError = new CError();
                		tError.moduleName = "LLCaseReturnBL";
                		tError.functionName = "dealData";
                		tError.errorMessage = "你没有权限回退这个案件!";
                		this.mErrors.addOneError(tError);
                		return false;
                	}
                	map.put(tLLClaimUWMainSchema, "UPDATE");
            
              }else{
               		//判断是不是由这个案件审核人的上级来回退这个案件，此处需要支持特需上级对其下级案件的回退
                   	LLClaimUserSet tLLClaimUserSet = new LLClaimUserSet();
                   	LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
                   	String strSql = "select * from Llclaimuser where usercode = '"
                                   + mLLCaseBackSchema.getNHandler() +
                                   "' and (upusercode = '" + this.mGlobalInput.Operator +
                                   "' or specialneedupusercode = '"+this.mGlobalInput.Operator+"')";
                   	System.out.println(strSql);
                   	tLLClaimUserSet = tLLClaimUserDB.executeQuery(strSql);
                   	//System.out.println(strSql+tLLClaimUserSet+tLLClaimUserSet.size())
                   	if (tLLClaimUserSet == null || tLLClaimUserSet.size() == 0) {
                   		this.mErrors.copyAllErrors(tLLClaimUserDB.mErrors);
                   		CError tError = new CError();
                   		tError.moduleName = "LLCaseReturnBL";
                   		tError.functionName = "dealData";
                   		tError.errorMessage = "你没有权限回退这个案件!";
                   		this.mErrors.addOneError(tError);
                   		return false;
                   	}
                   	
               }
               
               if(!checkReturnState(rgtState,mLLCaseSchema.getRgtState())){
					return false;
               }
            }

            //案件信息更新
            mLLCaseBackSchema.setOHandler(tLLCaseSchema.getHandler()); //旧审核人
            tLLCaseSchema.setRgtState(mLLCaseSchema.getRgtState());
            tLLCaseSchema.setHandler(mLLCaseSchema.getHandler());
            tLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
            tLLCaseSchema.setModifyTime(PubFun.getCurrentTime());
            String argtno = tLLCaseSchema.getRgtNo();
            if (argtno.substring(0, 1).equals("P")) {
                LLRegisterDB tLLRegisterDB = new LLRegisterDB();
                tLLRegisterDB.setRgtNo(argtno);
                if (!tLLRegisterDB.getInfo()) {
                    CError tError = new CError();
                    tError.moduleName = "LLCaseReturnBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "团体批次信息查询失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (tLLRegisterDB.getRgtState().equals("03")) {
                    LLRegisterSchema aLLRegisterSchema = new LLRegisterSchema();
                    aLLRegisterSchema = tLLRegisterDB.getSchema();
                    aLLRegisterSchema.setRgtState("02");
                    map.put(aLLRegisterSchema, "UPDATE");
                }
            }

            //案件回退信息插入
            String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
            String CaseBackNo = PubFun1.CreateMaxNo("CaseBack", tLimit);

            mLLCaseBackSchema.setCaseBackNo(CaseBackNo);
            mLLCaseBackSchema.setRgtNo(tLLCaseSchema.getRgtNo());

            mLLCaseBackSchema.setMngCom(mGlobalInput.ManageCom);
            mLLCaseBackSchema.setOperator(mGlobalInput.Operator);
            mLLCaseBackSchema.setMakeDate(PubFun.getCurrentDate());
            mLLCaseBackSchema.setMakeTime(PubFun.getCurrentTime());
            mLLCaseBackSchema.setModifyDate(PubFun.getCurrentDate());
            mLLCaseBackSchema.setModifyTime(PubFun.getCurrentTime());

            map.put(tLLCaseSchema, "UPDATE");
            map.put(mLLCaseBackSchema, "INSERT");
            return true;
        } else {
            return false;
        }

    }

    /**
     * 校验案件原状态和回退后状态关系，案件不能回退到当前状态之后的状态
     * @param rgtState	案件当前状态
     * @param rgtState2	案件回退后状态
     */
    private boolean checkReturnState(String rgtState, String rgtState2) {
		if("01".equals(rgtState2)){//案件回退到‘受理’
			return true;
		}else if("03".equals(rgtState2)){//案件回退到‘检录’
			if("01".equals(rgtState) || "02".equals(rgtState)){
                CError tError = new CError();
                tError.moduleName = "LLCaseReturnBL";
                tError.functionName = "checkReturnState";
                tError.errorMessage = "案件不能回退到当前状态之后的状态!";
                this.mErrors.addOneError(tError);
                return false;
			}
		}else if("04".equals(rgtState2)){//案件回退到‘理算’
			if("01".equals(rgtState) || "02".equals(rgtState) || "03".equals(rgtState)){
                CError tError = new CError();
                tError.moduleName = "LLCaseReturnBL";
                tError.functionName = "checkReturnState";
                tError.errorMessage = "案件不能回退到当前状态之后的状态!";
                this.mErrors.addOneError(tError);
                return false;
			}
		}else if("05".equals(rgtState2)){//案件回退到‘审批’
			if("01".equals(rgtState) || "02".equals(rgtState) || "03".equals(rgtState) || "04".equals(rgtState)){
                CError tError = new CError();
                tError.moduleName = "LLCaseReturnBL";
                tError.functionName = "checkReturnState";
                tError.errorMessage = "案件不能回退到当前状态之后的状态!";
                this.mErrors.addOneError(tError);
                return false;
			}
		}else{
			CError tError = new CError();
            tError.moduleName = "LLCaseReturnBL";
            tError.functionName = "checkReturnState";
            tError.errorMessage = "不支持的回退后状态!";
            this.mErrors.addOneError(tError);
            return false;
		}
		
		return true;
	}

	/**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLLCaseSchema.setSchema((LLCaseSchema) cInputData.
                                     getObjectByObjectName("LLCaseSchema", 0));
        this.mLLCaseBackSchema.setSchema((LLCaseBackSchema) cInputData.
                                         getObjectByObjectName(
                                                 "LLCaseBackSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */

    private boolean prepareOutputData() {
        System.out.println("BL prepareOutputData");
        try {
            this.mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseReturnBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
