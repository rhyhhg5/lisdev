package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:案件－审核－二次核保的显示和保存程序
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @date   2003-8-4
 * @version 1.0
 */
public class SecondUWUI
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData = new VData();
  private VData mResult = new VData();
  private String mOperate;

  public SecondUWUI() {}

  public boolean submitData(VData cInputData,String cOperate)
  {
    this.mOperate = cOperate;
    SecondUWBL tSeconcUWBL = new SecondUWBL();
    if (tSeconcUWBL.submitData(cInputData,mOperate) == false)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tSeconcUWBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "SeconcUWUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据保存失败！！！";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
      mResult = tSeconcUWBL.getResult();
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public static void main(String[] args)
  {
  }
}