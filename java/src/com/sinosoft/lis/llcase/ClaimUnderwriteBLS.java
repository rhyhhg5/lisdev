package com.sinosoft.lis.llcase;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: Web业务系统投保暂交费功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author YT
 * @version 1.0
 */

public class ClaimUnderwriteBLS
{
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();


  public ClaimUnderwriteBLS() {}

  public static void main(String[] args)
  {
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone();

    System.out.println("Start LLClaimUnderwrite BLS Submit...");
    if (!this.saveData())
      return false;
    System.out.println("End LLClaimUnderwrite BLS Submit...");
    mInputData=null;
    return true;
  }

  private boolean saveData()
  {
    LLClaimUnderwriteSet mLLClaimUnderwriteSet = (LLClaimUnderwriteSet)mInputData.getObjectByObjectName("LLClaimUnderwriteBLSet",0);
    LLClaimUWDetailSet mLLClaimUWDetailSet = (LLClaimUWDetailSet)mInputData.getObjectByObjectName("LLClaimUWDetailBLSet",0);
    Connection conn = null;
    conn = DBConnPool.getConnection();
    if (conn==null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ClaimUnderwriteBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    try
    {
      conn.setAutoCommit(false);
			// 保存现在的关联
			LLClaimUnderwriteDBSet mLLClaimUnderwriteDBSet = new LLClaimUnderwriteDBSet( conn );
			mLLClaimUnderwriteDBSet.set(mLLClaimUnderwriteSet);
			if (mLLClaimUnderwriteDBSet.insert() == false)
			{
				// @@错误处理
			  this.mErrors.copyAllErrors(mLLClaimUnderwriteDBSet.mErrors);
				CError tError = new CError();
				tError.moduleName = "ClaimUnderwriteBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "保存数据失败!";
				this.mErrors .addOneError(tError) ;
	       conn.rollback() ;
				return false;
			}

      LLClaimUWDetailDBSet mLLClaimUWDetailDBSet = new LLClaimUWDetailDBSet( conn );
			mLLClaimUWDetailDBSet.set(mLLClaimUWDetailSet);
			if (mLLClaimUWDetailDBSet.insert() == false)
			{
				// @@错误处理
			  this.mErrors.copyAllErrors(mLLClaimUWDetailDBSet.mErrors);
				CError tError = new CError();
				tError.moduleName = "ClaimUWDetailBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "保存数据失败!";
				this.mErrors .addOneError(tError) ;
	       conn.rollback() ;
				return false;
			}

			conn.commit() ;
		} // end of try
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ClaimUnderwriteBLS";
			tError.functionName = "submitData";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);
			try{conn.rollback() ;} catch(Exception e){}
			return false;
		}
    return true;
  }

}
