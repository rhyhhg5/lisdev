package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:简易理赔录入
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author Xx
 * @version 6.0
 */
public class SimpleClaimAuditUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public SimpleClaimAuditUI() {}

 public static void main(String[] args)
 {

 }

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    System.out.println("---UI BEGIN---");
    this.mOperate = cOperate;
    SimpleClaimAuditBL tSimpleClaimAuditBL = new SimpleClaimAuditBL();

    if (tSimpleClaimAuditBL.submitData(cInputData,mOperate) == false)
    {
      this.mErrors.copyAllErrors(tSimpleClaimAuditBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "CaseUI";
      tError.functionName = "submitData";
      tError.errorMessage = "处理失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
      mResult = tSimpleClaimAuditBL.getResult();
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

}
