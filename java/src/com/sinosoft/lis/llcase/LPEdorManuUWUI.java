package com.sinosoft.lis.llcase;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:个人保全人工核保确认功能类</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class LPEdorManuUWUI
{
    private LPEdorManuUWBL mLPEdorManuUWBL = null;
    

    public LPEdorManuUWUI(GlobalInput gi, TransferData td)
    {
    	mLPEdorManuUWBL = new LPEdorManuUWBL(gi, td);
    }

    /**
     * 调用业务逻辑处理
     * @param gi GlobalInput
     * @param edorAcceptNo String
     * @return boolean
     */
    public boolean submitData()
    {
        if (!mLPEdorManuUWBL.submitData())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public String getError()
    {
        return mLPEdorManuUWBL.mErrors.getFirstError();
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.ManageCom = "86";
        gi.Operator = "pa0001";
        //String edorNo = "20060223000006";
        TransferData td = new TransferData();
        td.setNameAndValue("EdorNo", "20061218000008");
        td.setNameAndValue("MissionId", "00000000000000009652");
        td.setNameAndValue("SubMissionId", "1");
        td.setNameAndValue("ActivityId", "0000001180");
        LPEdorManuUWUI tLPEdorManuUWUI = new LPEdorManuUWUI(gi, td);
        if (!tLPEdorManuUWUI.submitData())
        {
            System.out.println(tLPEdorManuUWUI.getError());
        }
    }
}
