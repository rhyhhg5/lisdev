package com.sinosoft.lis.llcase;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.db.LLClaimUserDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.schema.LDSpotUWRateSchema;
import com.sinosoft.lis.schema.LLClaimUserSchema;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.lis.vschema.LDSpotUWRateSet;
import com.sinosoft.lis.vschema.LDUserSet;
import com.sinosoft.lis.vschema.LLClaimUserSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

import com.sinosoft.lis.db.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 立案业务逻辑保存处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @version 1.0
 */
public class ClaimUserOperateBL
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData = new VData();
  private VData mResult = new VData();
  private String mOperate;
  private GlobalInput mG = new GlobalInput();
  public  ClaimUserOperateBL() {}
  private LLClaimUserSchema mLLClaimUserSchema = new LLClaimUserSchema();
  private LLClaimUserSet mLLClaimUserSet = new LLClaimUserSet();
  private LDSpotUWRateSchema mLDSpotUWRateSchema = new LDSpotUWRateSchema();
  private LDSpotUWRateSet mLDSpotUWRateSet = new LDSpotUWRateSet();
  private String mUserCode = "";
  private LDCode1Set mLDCode1Set = new LDCode1Set();
  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData,String cOperate)
  {
    cInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;
    if (!getInputData(cInputData))
      return false;
    if(mOperate.equals("QUERY"))
    {
      if(!querydata())
        return false;
    }
    /*if(mOperate.equals("DELETE"))
    {
      if(!dealData())
        return false;
    }*/
    if(mOperate.equals("INSERT")||mOperate.equals("UPDATE")||mOperate.equals("DELETE"))
    {
      if(!checkdata())
        return false;
      if(!dealData())
        return false;
    }
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }
  private boolean getInputData(VData cInputData)
  {
    if(mOperate.equals("QUERY"))
    {
      mUserCode = (String)cInputData.get(0);
      System.out.println("ClaimUserBL中的UserCode=="+mUserCode);
    }
    else
    {
      mLLClaimUserSchema.setSchema((LLClaimUserSchema)cInputData.getObjectByObjectName("LLClaimUserSchema",0));
      mLDSpotUWRateSchema.setSchema((LDSpotUWRateSchema)cInputData.getObjectByObjectName("LDSpotUWRateSchema",0));
      mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
      mLDCode1Set = (LDCode1Set) cInputData.getObjectByObjectName("LDCode1Set", 0);
    }
    return true;
  }
  //
  /*****************************************************************************************
   * Name     :checkdate
   * Function :判断数据逻辑上的正确性
   * 1:判断用户代码和用户姓名的关联是否正确
   * 2:校验用户和录入的管理机构是否匹配
   * 3:若不是最高的核赔权限的用户一定要有上级用户代码
   * 4:若是新增，判断该用户代码是否已经存在。
   *
   */
  private boolean checkdata()
  {


    String UserName_sql = " select * from LDUser where UserCode = '"
                        +mLLClaimUserSchema.getUserCode().trim()+"' and ComCode like '"
                        +mLLClaimUserSchema.getComCode()+"%' ";

    System.out.println("UserName_sql"+UserName_sql);
    LDUserDB tLDUserDB = new LDUserDB();
    LDUserSet tLDUserSet = new LDUserSet();
    tLDUserSet.set(tLDUserDB.executeQuery(UserName_sql));
    System.out.println("tLDUserSet.size()="+tLDUserSet.size());

    if(tLDUserSet.size()==0)
    {
    	 CError tError = new CError();
         tError.moduleName = "ClaimUserOperateBL";
         tError.functionName = "checkData";
         tError.errorMessage ="在该管理机构为"+mLLClaimUserSchema.getComCode()
                 +"下没用用户号码为"+mLLClaimUserSchema.getUserCode()+"的用户";
         this.mErrors.addOneError(tError);
      return false;
    }
    if(!tLDUserSet.get(1).getUserName().trim().equals(mLLClaimUserSchema.getUserName().trim()))
    {
    	CError tError = new CError();
        tError.moduleName = "ClaimUserOperateBL";
        tError.functionName = "checkData";
        tError.errorMessage ="您录入的用户代码和用户名称不匹配，系统中用户号码为"+mLLClaimUserSchema.getUserCode()
                 +"的用户名称是"+tLDUserSet.get(1).getUserName()+"而您录入的用户名称是"+mLLClaimUserSchema.getUserName();
        this.mErrors.addOneError(tError);
      return false;
    }
    String tMngCom = mLLClaimUserSchema.getComCode();
    String tempComCode=tLDUserSet.get(1).getComCode();
    if(tMngCom.trim().length()<=2)
   {
     tMngCom = tMngCom+"00";
     tempComCode+="00";
   }

     if(!tempComCode.equals(tMngCom.trim()))
    {
    	 CError tError = new CError();
         tError.moduleName = "ClaimUserOperateBL";
         tError.functionName = "checkData";
         tError.errorMessage ="您录入的“用户管理机构”和“用户代码”不匹配，系统中用户号码为"+mLLClaimUserSchema.getUserCode()
                 +"的用户管理机构是"+tLDUserSet.get(1).getComCode()+"而您录入的管理机构是"+mLLClaimUserSchema.getComCode();
         this.mErrors.addOneError(tError);
      return false;
    }
     tempComCode=null;//清除对象引用

    //查询出最高的核赔级别
//    String UwGrade_sql = "select * from lmuw where uwtype = '4' order by uwgrade desc ";
//    System.out.println("核赔权限的查询语句是"+UwGrade_sql);
//    LMUWDB tLMUWDB = new LMUWDB();
//    LMUWSet tLMUWSet = new LMUWSet();
//    tLMUWSet.set(tLMUWDB.executeQuery(UwGrade_sql));
//    if(tLMUWSet.size()==0)
//    {
//      buildError("ClaimUserOperateBL","核赔权限查询错误！请维护LMUW表");
//      return false;
//    }
//    if((!mLLClaimUserSchema.getClaimPopedom().trim().equals(tLMUWSet.get(1).getUWGrade()))
//       &&(mLLClaimUserSchema.getUpUserCode().trim().equals("")||mLLClaimUserSchema.getUpUserCode().trim()==null))
//    {
//      buildError("ClaimUserOperateBL","该用户的核赔级别不是最高级别时，请您录入该用户的“上级用户代码”");
//      return false;
//    }
//    if(mLLClaimUserSchema.getCheckFlag().equals("1")&&mLLClaimUserSchema.getHandleFlag().equals("1"))
//    {
//      buildError("ClaimUserOperateBL","该用户不可以同时拥有“理算签批”权限和“案件审核”权限，请改正！");
//      return false;
//    }
     
    //modify by Houyd	
    if(mOperate.equals("UPDATE")){
         //特需权限效验：下级用户“审批额度”小于其上级用户 Add By Houyd 20140225
    	LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
        tLLClaimUserDB.setUserCode(mLLClaimUserSchema.getUserCode());
        mLLClaimUserSet.set(tLLClaimUserDB.query());
         if(!testSpecialNeedLimit()){
      	   return false;
         }
         if(!testDSNeedLimit()) {
        	 return false;
         }
    }
     	
    if(mOperate.equals("INSERT"))
    {
      LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
      tLLClaimUserDB.setUserCode(mLLClaimUserSchema.getUserCode());
      mLLClaimUserSet.set(tLLClaimUserDB.query());
      System.out.println("tLLClaimUserDB.query()");
      String lowCodeLevel=mLLClaimUserSchema.getClaimPopedom();//取到从界面传来的用户核陪权限
      String maxSql = "select max(claimpopedom) from llclaimpopedom";
      ExeSQL exesql = new ExeSQL();
      String maxpope = exesql.getOneValue(maxSql);

      //如果核赔权限为最高的话不需要上级用户代码
      if(lowCodeLevel.compareTo(maxpope)==0){
        return true;
      }
      tLLClaimUserDB.setUserCode(mLLClaimUserSchema.getUpUserCode());
        if ( !tLLClaimUserDB.getInfo()) {
        	 CError tError = new CError();
             tError.moduleName = "ClaimUserOperateBL";
             tError.functionName = "checkData";
             tError.errorMessage ="在该管理机构为" +
                       mLLClaimUserSchema.getComCode() +
                       "下没有上级用户号码为" +
                       mLLClaimUserSchema.getUpUserCode()+"的用户";
             this.mErrors.addOneError(tError);
            return false;

        }
        LLClaimUserSchema sLLClaimUserSchema =tLLClaimUserDB.getSchema();

        System.out.println("lowCodeLevel="+lowCodeLevel);


       String upCodeLevel=sLLClaimUserSchema.getClaimPopedom();//根据上级代号查出上级权限
       System.out.println("upCodeLevel"+upCodeLevel);
       if(lowCodeLevel.compareTo(maxpope)==0){
           System.out.println("now the "+maxpope);
           System.out.println(upCodeLevel.compareTo(maxpope));
           if(upCodeLevel.compareTo(maxpope)!=0||upCodeLevel==""){
        	   CError tError = new CError();
               tError.moduleName = "ClaimUserOperateBL";
               tError.functionName = "checkData";
               tError.errorMessage = "下级用户权限不能高于上级用户权限";
               this.mErrors.addOneError(tError);
                 return false;
           }

       }else

            if(lowCodeLevel.compareTo(upCodeLevel)>0){
            	CError tError = new CError();
                tError.moduleName = "ClaimUserOperateBL";
                tError.functionName = "checkData";
                tError.errorMessage = "下级用户权限不能高于上级用户权限";
                this.mErrors.addOneError(tError);
                 return false;
            }
       
       //特需权限效验：下级用户“审批额度”小于其上级用户 Add By Houyd 20140225
       if(!testSpecialNeedLimit()){
    	   return false;
       }
       //电商权限校验
       if(!testDSNeedLimit()) {
    	   return false;
       }
       
    }
    //当修改为非有效状态或者删除用户时
    if((mOperate.equals("UPDATE")&&!mLLClaimUserSchema.getStateFlag().equals("1"))||mOperate.equals("DELETE"))
    {
    	String getCasenoSql=" select caseno from llcase where handler='"+mLLClaimUserSchema.getUserCode()+"' and endcasedate is null " +
		" and rgtstate not in ('09','11','12','14') union all select a.caseno from llcase a,LLClaimUWMain b where a.caseno=b.caseno " +
		" and a.endcasedate is null and a.rgtstate in ('05','10') and b.AppClmUWer = '"+mLLClaimUserSchema.getUserCode()+"' with ur";
    	System.out.println("未处理案件查询=="+getCasenoSql);
    	ExeSQL exesql2 = new ExeSQL();
    	SSRS tSSRS=exesql2.execSQL(getCasenoSql);
    	if(tSSRS.MaxRow>0)//有未处理完案件
    	{
    		CError tError = new CError();
            tError.moduleName = "ClaimUserOperateBL";
            tError.functionName = "checkData";
            tError.errorMessage = "用户"+mLLClaimUserSchema.getUserCode()+"下还有未处理完的案件";
            this.mErrors.addOneError(tError);
    		return false;
    	}
    	if(!isSurveyCase())
    	{
    		return false;
    	}
    	if(!isPrepaidCase())
    	{
    		return false;
    	}
    	if(!isSpecialNeedCase())
    	{
    		return false;
    	}
        if(!testSpecialNeedLimit()){
     	   return false;
        }
    }else{
    	  if((mOperate.equals("UPDATE")&&mLLClaimUserSchema.getSurveyFlag().equals("0"))||mOperate.equals("DELETE"))
    	    {
    	    	if(!isSurveyCase())
    	    	{
    	    		return false;
    	    	}
    	    }
    	    if((mOperate.equals("UPDATE")&&!mLLClaimUserSchema.getPrepaidFlag().equals("1"))||mOperate.equals("DELETE"))
    	    {
    	    	if(!isPrepaidCase())
    	    	{  
    	    		return false;
    	    	}
    	    }
    	    //TODO：将有特需权限的用户取消特需权限或删除该用户时，效验用户下是否有未完成的特需业务案件
    	    if((mOperate.equals("UPDATE")&&!mLLClaimUserSchema.getSpecialNeedFlag().equals("1"))||mOperate.equals("DELETE"))
    	    {
    	    	if(!isSpecialNeedCase())
    	    	{  
    	    		return false;
    	    	}
    	    }
    	    
    }
  
    return true;

  }
  
  /**
   * 电商权限效验
   * @return
   */
  private boolean testDSNeedLimit() {
	  if(mLLClaimUserSchema.getDSNeedFlag() != "1") {
		  return true;
	  }
	//电商权限校验
      String lowCodeLevel=mLLClaimUserSchema.getDSNeedLimitPopedom();//取到从界面传来的用户核陪权限
      String maxSql = "select max(claimpopedom) from llclaimpopedom";
      ExeSQL exesql = new ExeSQL();
      String maxpope = exesql.getOneValue(maxSql);

      //如果核赔权限为最高的话不需要上级用户代码
      if(lowCodeLevel.compareTo(maxpope)==0){
        return true;
      }
      //如果没有上级用户就不进行校验
      if("".equals(mLLClaimUserSchema.getDSNeedUpUserCode()) || "".equals(mLLClaimUserSchema.getDSNeedUpUserCode()) || mLLClaimUserSchema.getDSNeedUpUserCode() == null) {
    	  return true;
      }
  	LLClaimUserDB ttLLClaimUserDB = new LLClaimUserDB();
      ttLLClaimUserDB.setUserCode(mLLClaimUserSchema.getDSNeedUpUserCode());
      if ( !ttLLClaimUserDB.getInfo()) {
     	 CError tError = new CError();
          tError.moduleName = "ClaimUserOperateBL";
          tError.functionName = "checkData";
          tError.errorMessage ="在该管理机构为" +
                    mLLClaimUserSchema.getComCode() +
                    "下没有上级用户号码为" +
                    mLLClaimUserSchema.getUpUserCode()+"的用户";
          this.mErrors.addOneError(tError);
         return false;
     }
      LLClaimUserSchema sLLClaimUserSchema =ttLLClaimUserDB.getSchema();

      System.out.println("lowCodeLevel="+lowCodeLevel);
      String upCodeLevel=sLLClaimUserSchema.getDSNeedLimitPopedom();//根据上级代号查出上级权限
      String  ssDSNeedFlag= sLLClaimUserSchema.getDSNeedFlag();
      if(ssDSNeedFlag != "1") {
    	  return true;
      }
      System.out.println("upCodeLevel"+upCodeLevel);
      
      if(lowCodeLevel.compareTo(maxpope)==0){
          System.out.println("now the "+maxpope);
          System.out.println(upCodeLevel.compareTo(maxpope));
          if(upCodeLevel.compareTo(maxpope)!=0||upCodeLevel==""){
       	   CError tError = new CError();
              tError.moduleName = "ClaimUserOperateBL";
              tError.functionName = "checkData";
              tError.errorMessage = "电商下级用户权限不能高于上级用户权限";
              this.mErrors.addOneError(tError);
                return false;
          }

      }else  	
    		  if(lowCodeLevel.compareTo(upCodeLevel)>0){
    	           	CError tError = new CError();
    	               tError.moduleName = "ClaimUserOperateBL";
    	               tError.functionName = "checkData";
    	               tError.errorMessage = "电商下级用户权限不能高于电商上级用户权限";
    	               this.mErrors.addOneError(tError);
    	                return false;
    	           }
           
	  return true;
  }
  
  /**
   * 特需权限效验：下级用户“审批额度”小于其上级用户 Add By Houyd 20140225
   * @return
   */
  private boolean testSpecialNeedLimit() {
	  
	  System.out.println("<-Go Into testSpecialNeedLimit()->");
	  if("".equals(mLLClaimUserSchema.getSpecialNeedUpUserCode()) || mLLClaimUserSchema.getSpecialNeedUpUserCode() == null){//不设置特需上级
		  return true;
	  }
	  LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
      tLLClaimUserDB.setUserCode(mLLClaimUserSchema.getSpecialNeedUpUserCode());
      LLClaimUserSchema tLLClaimUserSchema =tLLClaimUserDB.query().get(1).getSchema();
      if(tLLClaimUserSchema.getSpecialNeedLimit() < mLLClaimUserSchema.getSpecialNeedLimit()){
    	  CError tError = new CError();
          tError.moduleName = "ClaimUserOperateBL";
          tError.functionName = "checkData";
          tError.errorMessage ="理赔用户"+mLLClaimUserSchema.getUserCode()+"的特需业务审批额度大于其特需上级的审批额度";
          this.mErrors.addOneError(tError);
		  return false; 
      }
      
	return true;
}

/**
   * 修改用户状态、特需权限状态时校验是否存在未处理完的特需案件 Add By Houyd 20140225
   * @return
   */
  private boolean isSpecialNeedCase() {
	  
	  System.out.println("<-Go Into isSpecialNeedCase()->");
	  String tSpecialNeedSQL="select specialneedflag from llclaimuser where usercode='"+ mLLClaimUserSchema.getUserCode() +"'";
		ExeSQL tSpecialNeedExe=new ExeSQL();
		SSRS tSpecialNeedSS=tSpecialNeedExe.execSQL(tSpecialNeedSQL);
		if(tSpecialNeedSS.getMaxRow()<=0)
		{
		  CError tError = new CError();
          tError.moduleName = "ClaimUserOperateBL";
          tError.functionName = "checkData";
          tError.errorMessage ="理赔用户"+mLLClaimUserSchema.getUserCode()+"查询失败";
          this.mErrors.addOneError(tError);
		  return false;
		}
		String tSpecialNeedFlag = tSpecialNeedSS.GetText(1, 1);
		if((tSpecialNeedFlag.equals("1") && !mLLClaimUserSchema.getSpecialNeedFlag().equals("1"))
				|| mOperate.equals("DELETE"))
		{
			String tSpecialNeed="select * from llcase a,llclaimdetail b " +
					"where a.caseno = b.caseno " +
					"and a.handler = '"+mLLClaimUserSchema.getUserCode()+"' " +
					"and b.riskcode in (select riskcode from lmriskapp where risktype3='7' ) " ;									
			ExeSQL tSpecialNeedExeSQL=new ExeSQL();
			SSRS tSpecialNeedSSRS=tSpecialNeedExeSQL.execSQL(tSpecialNeed);
			if(tSpecialNeedSSRS.getMaxRow()>=1)
			{
				CError tError = new CError();
	            tError.moduleName = "ClaimUserOperateBL";
	            tError.functionName = "checkData";
	            tError.errorMessage ="特需业务审批人"+mLLClaimUserSchema.getUserCode()+"下存在未处理完的案件";
	            this.mErrors.addOneError(tError);
				return false;
			}
		}
	  return true;
}

/*
   * 修改用户状态、调查状态时校验是否存在未处理完的调查案件
   * */
  private boolean isSurveyCase()
  {
	  ExeSQL tExeSQL=new ExeSQL();	 
	  String tSurveyFlag=tExeSQL.getOneValue("select SurveyFlag from LLClaimUser where usercode='"+mLLClaimUserSchema.getUserCode()+"'");
	  
	  ExeSQL surExeSQL=new ExeSQL();
	  if((tSurveyFlag.trim().equals("1") && !mLLClaimUserSchema.getStateFlag().equals("1"))
			  || mOperate.equals("DELETE"))//调查主管时
	  {
			String getSurveynoSql="select a.surveyno from llInqapply a,llcase b,llsurvey c where a.otherno=b.caseno and b.caseno=c.otherno " +
					" and a.surveyno=c.surveyno and  a.Dipatcher='"+mLLClaimUserSchema.getUserCode()+"' and c.surveyflag <>'3' and b.rgtstate ='07' union all " +
					" select a.surveyno from llsurvey a,llcase b where a.otherno=b.caseno and  a.confer='"+mLLClaimUserSchema.getUserCode()+"' and a.surveyflag <>'3'" +
					" and b.rgtstate ='07' with ur";
			SSRS tSSRS2=surExeSQL.execSQL(getSurveynoSql);
			if(tSSRS2.MaxRow>0)//调查主管有未处理完的调查（包括调查未分配、调查未审核）时
			{
				CError tError = new CError();
	            tError.moduleName = "ClaimUserOperateBL";
	            tError.functionName = "checkData";
	            tError.errorMessage ="调查主管"+mLLClaimUserSchema.getUserCode()+"下还有未处理完的调查";
	            this.mErrors.addOneError(tError);
	    		return false;
			}
			String getSurveynoSql2="select a.surveyno from llsurvey a,llinqfee b where a.surveyno = b.surveyno and a.confer='"+mLLClaimUserSchema.getUserCode()+"'" +
					" and b.uwstate <>'1' union all select a.surveyno from llsurvey a,llinqfee b,llinqapply c " +
					" where a.surveyno=b.surveyno and a.surveyno=c.surveyno and a.confer<>c.Dipatcher " +
					" and b.uwstate not in ('1','4') and c.Dipatcher='"+mLLClaimUserSchema.getUserCode()+"' with ur";
			SSRS tSSRS3=surExeSQL.execSQL(getSurveynoSql2);
			if(tSSRS3.MaxRow>0)//调查主管有未处理完的调查（直接调查费包括本地调查费和异地调查费未审核）时
			{
				CError tError = new CError();
	            tError.moduleName = "ClaimUserOperateBL";
	            tError.functionName = "checkData";
	            tError.errorMessage ="调查主管"+mLLClaimUserSchema.getUserCode()+"下还有未处理完的直接调查费审核";
	            this.mErrors.addOneError(tError);
	    		return false;
			}
		}
		else if((tSurveyFlag.trim().equals("2")&& !mLLClaimUserSchema.getStateFlag().equals("2"))
				|| mOperate.equals("DELETE"))//调查员时
		{
			String getSurveynoSql2="select a.surveyno from llInqapply a,llcase b,llsurvey c where a.otherno=b.caseno and b.caseno=c.otherno " +
					"and a.surveyno=c.surveyno and  a.inqper='"+mLLClaimUserSchema.getUserCode()+"' and c.surveyflag <>'3' and b.rgtstate ='07' with ur";
			SSRS tSSRS3=surExeSQL.execSQL(getSurveynoSql2);
			if(tSSRS3.MaxRow>0)//调查员有未处理完的调查（包括调查未分配、调查未审核、直接调查费未审核）时
			{
				CError tError = new CError();
	            tError.moduleName = "ClaimUserOperateBL";
	            tError.functionName = "checkData";
	            tError.errorMessage ="调查员"+mLLClaimUserSchema.getUserCode()+"下还有未处理完的调查";
	            this.mErrors.addOneError(tError);
	    		return false;
			}
		}
	  return true;
  }
  /*
   * 修改用户状态、预付状态时校验是否存在未处理完的预付案件
   * */
  private boolean isPrepaidCase()
  {
	    String tPrepaidUW="select PrepaidFlag,StateFlag from llclaimuser where usercode='"+ mLLClaimUserSchema.getUserCode() +"'";
		ExeSQL PreUWExeSQL=new ExeSQL();
		SSRS PreUWSSRS=PreUWExeSQL.execSQL(tPrepaidUW);
		if(PreUWSSRS.getMaxRow()<=0)
		{
			CError tError = new CError();
            tError.moduleName = "ClaimUserOperateBL";
            tError.functionName = "checkData";
            tError.errorMessage ="理赔用户"+mLLClaimUserSchema.getUserCode()+"查询失败";
            this.mErrors.addOneError(tError);
  		return false;
		}
		String tPrepaidFlag = PreUWSSRS.GetText(1, 1);
		if((tPrepaidFlag.equals("1") && !mLLClaimUserSchema.getPrepaidFlag().equals("1"))
				|| mOperate.equals("DELETE"))
		{
			String tPrepaid="select 1 from llprepaidclaim a where Dealer='"+ mLLClaimUserSchema.getUserCode() +"'"
						+" and RgtState in ('01','02','03')";									
			ExeSQL PreExeSQL=new ExeSQL();
			SSRS PreSSRS=PreExeSQL.execSQL(tPrepaid);
			if(PreSSRS.getMaxRow()>=1)
			{
				CError tError = new CError();
	            tError.moduleName = "ClaimUserOperateBL";
	            tError.functionName = "checkData";
	            tError.errorMessage ="预付赔款审批人"+mLLClaimUserSchema.getUserCode()+"下存在未处理完的案件";
	            this.mErrors.addOneError(tError);
				return false;
			}
		}
	  return true;
  }
  private boolean querydata()
  {
    LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
    LLClaimUserSchema tLLClaimUserSchema = new LLClaimUserSchema();
    tLLClaimUserDB.setUserCode(mUserCode);
    if(!tLLClaimUserDB.getInfo())
    {
    	CError tError = new CError();
        tError.moduleName = "ClaimUserOperateBL";
        tError.functionName = "checkData";
        tError.errorMessage ="该用户的查询信息出错！请及时对其进行维护！";
        this.mErrors.addOneError(tError);
      return false;
    }
    tLLClaimUserSchema.setSchema(tLLClaimUserDB.getSchema());
    LDComDB tLDComDB = new LDComDB();
    if(!(tLLClaimUserSchema.getComCode()==null||tLLClaimUserSchema.getComCode().equals("")))
    {

      tLDComDB.setComCode(tLLClaimUserSchema.getComCode());
      if(!tLDComDB.getInfo())
      {
    	  CError tError = new CError();
          tError.moduleName = "ClaimUserOperateBL";
          tError.functionName = "checkData";
          tError.errorMessage ="LDCom查询出错，请及时维护!";
          this.mErrors.addOneError(tError);
        return false;
      }
    }
    mResult.clear();
    mResult.addElement(tLDComDB.getSchema().getName());
    mResult.add(tLLClaimUserSchema);
       
    return true;
  }
  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealData()
  {
      System.out.println("dealData....");
      LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
      tLLClaimUserDB.setUserCode(mLLClaimUserSchema.getUserCode());
      if(tLLClaimUserDB.getInfo()){
          if(mOperate.equals("INSERT")){
        	  CError tError = new CError();
              tError.moduleName = "ClaimUserOperateBL";
              tError.functionName = "checkData";
              tError.errorMessage ="系统中已有用户名为"+mLLClaimUserSchema.getUserCode()+"的理赔员信息！";
              this.mErrors.addOneError(tError);
              return false;
          }
      }
      else{
          if(mOperate.equals("DELETE")||mOperate.equals("UPDATE")){
        	  CError tError = new CError();
              tError.moduleName = "ClaimUserOperateBL";
              tError.functionName = "checkData";
              tError.errorMessage ="系统中尚无该理赔员信息，无法进行该操作！";
              this.mErrors.addOneError(tError);
              return false;
          }
      }
    prepareOutputData();
    ClaimUserOperateBLS tClaimUserOperateBLS = new ClaimUserOperateBLS();
    if (!tClaimUserOperateBLS.submitData(mInputData,mOperate))
    {
      this.mErrors.copyAllErrors(tClaimUserOperateBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimUserOperateBL";
      tError.functionName = "checkData";
      tError.errorMessage ="ClaimUserOperateBLS的"+mOperate+"动作操作失败！";
      this.mErrors.addOneError(tError);
      return false;
    }
   return true;
  }

  private boolean prepareOutputData()
  {
    LLClaimUserSchema tLLClaimUserSchema = new LLClaimUserSchema();
    LDSpotUWRateSchema tLDSpotUWRateSchema = new LDSpotUWRateSchema();
    LDCode1Set saveLDCode1Set = new LDCode1Set();
    if(mOperate.equals("INSERT")||mOperate.equals("DELETE"))
    {
      tLLClaimUserSchema.setSchema(mLLClaimUserSchema);
      tLLClaimUserSchema.setMakeDate(PubFun.getCurrentDate());
      tLLClaimUserSchema.setMakeTime(PubFun.getCurrentTime());
      tLLClaimUserSchema.setModifyDate(PubFun.getCurrentDate());
      tLLClaimUserSchema.setModiftTime(PubFun.getCurrentTime());
      tLLClaimUserSchema.setOperator(mG.Operator);
      tLLClaimUserSchema.setManageCom(mG.ManageCom);
      tLDSpotUWRateSchema.setSchema(mLDSpotUWRateSchema);
     
    }
    if(mOperate.equals("UPDATE"))
    {
      if(mLLClaimUserSet.size()<=0)
      {
    	  CError tError = new CError();
          tError.moduleName = "ClaimUserOperateBL";
          tError.functionName = "checkData";
          tError.errorMessage ="系统中不存在该用户，不能执行修改操作！";
          this.mErrors.addOneError(tError);
        return false;
      }
      tLLClaimUserSchema.setSchema(mLLClaimUserSet.get(1));
      tLLClaimUserSchema.setSchema(mLLClaimUserSchema);
      tLLClaimUserSchema.setMakeDate(mLLClaimUserSet.get(1).getMakeDate());
      tLLClaimUserSchema.setMakeTime(mLLClaimUserSet.get(1).getMakeTime());
      tLLClaimUserSchema.setModifyDate(PubFun.getCurrentDate());
      tLLClaimUserSchema.setModiftTime(PubFun.getCurrentTime());
      tLLClaimUserSchema.setOperator(mG.Operator);
      tLLClaimUserSchema.setManageCom(mG.ManageCom);
      tLDSpotUWRateSchema.setSchema(mLDSpotUWRateSchema);
    }
    for(int i=1;i<=mLDCode1Set.size();i++){
      boolean flag = true;
  	  LDCode1Schema tLDCode1Schema = new LDCode1Schema();
  	  tLDCode1Schema = mLDCode1Set.get(i);
  	  System.out.println(tLDCode1Schema.getCode()+"pppp");
  	  for(int j = 1;j<i;j++){
  		 if(tLDCode1Schema.getComCode().equals(mLDCode1Set.get(j).getComCode())&& tLDCode1Schema.getOtherSign().equals(mLDCode1Set.get(j).getOtherSign())){
			   flag=false;
			   break;
		   }
  	  }
  	  if(flag){
  	  saveLDCode1Set.add(tLDCode1Schema);
  	  }
  	  
    }

    mInputData.clear();
    mInputData.add(tLLClaimUserSchema);
    mInputData.add(tLDSpotUWRateSchema);
    mInputData.add(saveLDCode1Set);
    mResult.clear();
    mResult.add(tLLClaimUserSchema);
    return true;
  }
  public void buildError(String szFunc, String szErrMsg)
  {
    CError cError = new CError( );
    cError.moduleName = "PayNoticeF1PBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }
  public static void main(String[] args)
  {

    LDSpotUWRateSchema mLDSpotUWRateSchema = new LDSpotUWRateSchema();
    LLClaimUserSchema aLLClaimUserSchema = new LLClaimUserSchema();
  //  aLLClaimUserSchema.setRemark("qqqqqqq");
    aLLClaimUserSchema.setUserCode("cm1101");
    aLLClaimUserSchema.setUpUserCode("cm0001");
    aLLClaimUserSchema.setUserName("李燕");
    aLLClaimUserSchema.setComCode("8611");
    aLLClaimUserSchema.setStateFlag("0");
    aLLClaimUserSchema.setClaimPopedom("F");
    //aLLClaimUserSchema.setRgtFlag("1");
    aLLClaimUserSchema.setSurveyFlag("1");
    aLLClaimUserSchema.setCheckFlag("0");
    aLLClaimUserSchema.setHandleFlag("0");
    aLLClaimUserSchema.setPrepaidFlag("0");
   
    mLDSpotUWRateSchema.setUserCode("cm1101");
    mLDSpotUWRateSchema.setUserName("李燕");
    mLDSpotUWRateSchema.setUWGrade("P");
    mLDSpotUWRateSchema.setUWType("30000");
    mLDSpotUWRateSchema.setUWRate("50");

    GlobalInput tG = new GlobalInput();
    tG.ManageCom = "86";
    tG.Operator = "claim";
    VData aVData = new VData();
    aVData.addElement(aLLClaimUserSchema);
    aVData.addElement(mLDSpotUWRateSchema);
    aVData.addElement(tG);

    /*
    String aUserCode = "000011";
    VData aVData = new VData();
    aVData.addElement(aUserCode);
    */
    ClaimUserOperateBL aClaimUserOperateBL = new ClaimUserOperateBL();
    aClaimUserOperateBL.submitData(aVData,"UPDATE");

    }
}
