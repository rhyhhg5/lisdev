package com.sinosoft.lis.llcase;

import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 理赔-审批审定-核赔业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author：zhangtao
 * @version：1.0
 * @CreateDate：2005-02-20
 */
public class ClaimManChkUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public ClaimManChkUI()
  {
  }

  public static void main(String[] args)
  {
      LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new LLClaimUnderwriteSchema();
        tLLClaimUnderwriteSchema.setClmNo("86000020030520000005");
        tLLClaimUnderwriteSchema.setRgtNo("00100020030510000014");
        tLLClaimUnderwriteSchema.setPolNo("86110020030210000081");
        tLLClaimUnderwriteSchema.setAutoClmDecision("0");
	tLLClaimUnderwriteSchema.setClmDecision("2");

         //读取Session中的全局类
//        Object t = new Object();
//        t.equals(tLLClaimUnderwriteSchema);
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ComCode  = "001";

        ClaimManChkUI tClaimManChkUI   = new ClaimManChkUI();
          // 准备传输数据 VData
        VData tVData = new VData();
        tVData.addElement(tLLClaimUnderwriteSchema);
        tVData.addElement(tG);
        //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
        tClaimManChkUI.submitData(tVData,"QUERY");
        tVData.clear();
        tVData = tClaimManChkUI.getResult();
        tLLClaimUnderwriteSchema=
            (LLClaimUnderwriteSchema)
                tVData.getObjectByObjectName("LLClaimUnderwriteSchema",0);
  }


  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    ClaimManChkBL tClaimManChkBL = new ClaimManChkBL();

    if (tClaimManChkBL.submitData(cInputData,mOperate) == false)
    {
  		// @@错误处理
      this.mErrors.copyAllErrors(tClaimManChkBL.mErrors);
      mResult.clear();
      return false;
    }
      mResult.clear();
      mResult = tClaimManChkBL.getResult();
    return true;
  }

  public VData getResult()
  {
           return mResult;
  }
}
