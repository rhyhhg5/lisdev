package com.sinosoft.lis.llcase;

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: CaseCureBL.java</p>
 * <p>Description: 理赔－费用明细保存及显示</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @version 1.0
 */
public class QueryCodeBL
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData ;
  private VData mResult = new VData();
  private String mOperate;
  private String strCodeType  = "";
  private String strSpellCode = "";
  private String strQueryCode = "";
  private String strQueryName = "";

  public QueryCodeBL() {}

  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;
    if (!getInputData(cInputData))
      return false;
    if(!QueryData())
      return false;
    return true;
  }

  private boolean getInputData(VData cInputData)
  {
    strCodeType  = (String)cInputData.get(0);
    strSpellCode = (String)cInputData.get(1);
    strQueryCode = (String)cInputData.get(2);
    strQueryName = (String)cInputData.get(3);
    System.out.println("号码类型是"+strCodeType);
    System.out.println("拼音简称是"+strSpellCode);
    System.out.println("代码是"+strQueryCode);
    System.out.println("代码名称是"+strQueryName);
    return true;
  }
  private boolean QueryData()
  {
    String tTabName = "LDCode";        //要进行查询的表的名称；
    String tColName = "CodeType";      //号码的类型；
    String tCodeType="";
    if(strCodeType.trim().equals("0"))
    {
      tCodeType = "diseasecode";
    }
    if(strCodeType.trim().equals("1"))
    {
      tCodeType = "desc_wound";
    }
    String sql =base_query(tTabName,tColName,tCodeType );
    System.out.println("在没有调用函数时的sql是"+sql);
    String tColName1 = "CodeAlias";              //拼音简称
    String tCodeName2 = "CodeName";              //汉语全称
    String tCodeName3 = "Code";                  //数字代码
    if(!(strSpellCode==null||strSpellCode.equals("")))
    {
      sql=sql+MH_query(tCodeName2,strSpellCode);
    }
    if(!(strQueryName==null||strQueryName.equals("")))
    {
      sql=sql+MH_query(tColName1,strQueryName);
    }
    if(!(strQueryCode==null||strQueryCode.equals("")))
    {
      sql=sql+MH_query(tCodeName3,strQueryCode);
    }
    System.out.println("调用后的sql是"+sql);
    ExeSQL exesql = new ExeSQL();
    SSRS ssrs = exesql.execSQL(sql);
    if(ssrs.getMaxRow()==0)
    {
      this.mErrors.copyAllErrors(this.mErrors);
      CError tError = new CError();
      tError.moduleName = "QueryPayPolBL";
      tError.functionName = "queryData";
      tError.errorMessage = "没有查询到相关的代码数据！";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("查询的结果是"+ssrs.getMaxRow());
    mResult.clear();
    mResult.add(ssrs);
    return true;
  }
  private String base_query(String tTabName,String tColName,String tCodeType )
  {
    String base_sql = "select * from "+tTabName+" where "+tColName+"  = '"+tCodeType.trim()+"' ";
    return base_sql;
  }
  private String MH_query(String ColName,String StrName)
  {
    //定义一个返回的字符串
    String strResult = "";
    //定义一个string形式的变量
    String tStrName =StrName;
    //截掉首未两端的空格
    tStrName=tStrName.trim();
    //将其转变成大写的变量
    tStrName=tStrName.toUpperCase().trim();
    //对其两端加分隔符号
    tStrName="+"+tStrName+"+";
    //求得变量的长度
    int length = tStrName.length();
    System.out.println("变量的长度是"+length);
    //求得该字符中分隔符号的个数-------tCount
    int tCount=0;
    for(int i=0;i<length;i++)
    {
      System.out.println("在变量中第"+i+"个位置存放的字符是"+tStrName.substring(i,i+1));
      if(tStrName.substring(i,i+1).equals("+"))
      {
        tCount=tCount+1;
      }
      System.out.println("在strSpellCode中共有"+tCount+"个分隔符号");
    }

    //初始化数组，数组的长度是tCount＋2个第一个存放的是0；而最后存放的是length
    int intArray[] = new int [tCount];
    int h = 0;
    //对数组的其他空间进行附值
    for(int i=0;i<length ;i++)
    {
      if(tStrName.substring(i,i+1).equals("+"))
      {
        intArray[h]=i;
        h=h+1;
      }
    }

    //对数组的空间个数进行循环
    for(int t=0;t<tCount-1;t++)
    {
      String add_sql = " and "+ColName+" like '%"+tStrName.substring(intArray[t]+1,intArray[t+1]).trim()
             +"%'";
      strResult = strResult+add_sql;
    }
    System.out.println("查询语句是"+strResult);
    return strResult;
  }
  public VData getResult()
  {
    return mResult;
  }
  public static void main(String[] args)
  {
    QueryCodeBL aQueryCodeBL = new QueryCodeBL();
    String strCodeType1 = "0";
    String strSpellCode1 = "";
    String strQueryCode1 = "";
    String strQueryName1 = "急性阑尾炎伴穿孔";
    VData tVData = new VData();
    tVData.addElement(strCodeType1);
    tVData.addElement(strSpellCode1);
    tVData.addElement(strQueryCode1);
    tVData.addElement(strQueryName1);
    if((aQueryCodeBL.submitData(tVData,"QueryData")))
    {
      System.out.println("查询成功！！！");
    }
  }
}