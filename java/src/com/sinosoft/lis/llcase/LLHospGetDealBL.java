package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.llcase.*;

/**
 * <p>Title: 医保通结算批次申请</p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author
 * @version 1.0
 * @date
 */

public class LLHospGetDealBL {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    private LLHospGetSchema mLLHospGetSchema = new LLHospGetSchema();

    private LLHospGetSchema mULLHospGetSchema = new LLHospGetSchema();

    private LLHospCaseSet mLLHospCaseSet = new LLHospCaseSet();

    private String mHospitCode = "";

    private String mHCNo = "";

    private MMap map = new MMap();

    /** 全局参数 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();

    /** 统一更新日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 统一更新时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    private String mOperator = "";

    private String mManageCom = "";

    public LLHospGetDealBL() {
    }

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        // 将外部传入的数据分解到本类的属性中，准备处理
        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        // 根据业务逻辑对数据进行处理
        if (!dealData()) {
            return false;
        }

        // 装配处理好的数据，准备给后台进行保存
        prepareOutputData();

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLHospFinanceBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 返回产生的结算批次号
     * @return String
     */
    public String getHCNo() {
        return mLLHospGetSchema.getHCNo();
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData() {
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mLLHospGetSchema.setSchema((LLHospGetSchema) mInputData.getObjectByObjectName("LLHospGetSchema", 0));
        mLLHospCaseSet.set((LLHospCaseSet) mInputData.getObjectByObjectName("LLHospCaseSet", 0));
        mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData",0);
        
        mOperator = mGlobalInput.Operator;
        mManageCom = mGlobalInput.ManageCom;

        if (mManageCom.length() == 2) {
            mManageCom = mManageCom + "000000";
        } else if (mManageCom.length() == 4) {
            mManageCom = mManageCom + "0000";
        }

        mHCNo = mLLHospGetSchema.getHCNo();

        return true;
    }

    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData() {
        if (mOperate == null || "".equals(mOperate)) {
            buildError("checkData", "Operate为空");
            return false;
        }

        if (!"CONFIRM".equals(mOperate)&&!"ALLINSERT".equals(mOperate)) {
            if (mLLHospCaseSet.size() < 1) {
                buildError("checkData", "未获取案件信息");
                return false;
            }
        }

        if (mHCNo == null || "".equals(mHCNo)) {
            buildError("checkData", "未获取批次信息");
            return false;
        }

        LLHospGetDB tLLHospGetDB = new LLHospGetDB();
        tLLHospGetDB.setHCNo(mHCNo);
        if (!tLLHospGetDB.getInfo()) {
            buildError("checkData", "批次查询失败");
            return false;
        }

        mULLHospGetSchema = tLLHospGetDB.getSchema();

        if (!mOperator.equals(tLLHospGetDB.getApplyer())) {
            buildError("checkData", "只有申请人可以进行操作");
            return false;
        }

        if (!"0".equals(mULLHospGetSchema.getState())) {
            buildError("dealData", "批次已经确认");
            return false;
        }

        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData() {
    	 if ("ALLINSERT".equals(mOperate)) {
    		 String tCaseNo = (String) mTransferData.getValueByName("CaseNo");
    		 String tCustomerNo = (String) mTransferData.getValueByName("CustomerNo");
    		 String tCustomerName = (String) mTransferData.getValueByName("CustomerName");
    		 String tEndCaseDataS = (String) mTransferData.getValueByName("EndCaseDataS");
    		 String tEndCaseDataE = (String) mTransferData.getValueByName("EndCaseDataE");
    		 String tHospitalCode = (String) mTransferData.getValueByName("HospitalCode");
    		 
    		 if(("".equals(tEndCaseDataS)||null==tEndCaseDataS)||("".equals(tEndCaseDataE)||null==tEndCaseDataE))
    		 {
    			 buildError("dealData", "查询未给付案件信息时必须录入结案起期和结案止期");
                 return false;
    		 }
    		 
             String varSql ="update llhospcase a set a.HCNo='"+mHCNo+"' "
             			+" where (a.hcno is null or a.hcno = '')  and a.hospitcode='"+tHospitalCode+"'"
             			+" and exists (select 1 from llcase b where a.caseno=b.caseno  "
            	 		+" and b.rgtstate='09' "
            	 		+" and b.endcasedate >='"+tEndCaseDataS+"' and b.endcasedate <='"+tEndCaseDataE+"')";
             if(tCaseNo != null && !tCaseNo.equals(""))
             {
            	 varSql +="and exists (select 1 from llcase where a.caseno=caseno and caseno='"+tCaseNo+"')";
             }
             if(tCustomerNo != null && !tCustomerNo.equals(""))
             {
            	 varSql +="and exists (select 1 from llcase where a.caseno=caseno and customerno='"+tCustomerNo+"')";
             }
             if(tCustomerName != null && !tCustomerName.equals(""))
             {
            	 varSql +="and exists (select 1 from llcase  where a.caseno=caseno and customername='"+tCustomerName+"')";
             }
             System.out.print(varSql);
             map.put(varSql, "UPDATE");
         }
    	 
        if ("INSERT".equals(mOperate)) {
            LLHospCaseSet tLLHospCaseSet = new LLHospCaseSet();
            for (int i = 1; i <= mLLHospCaseSet.size(); i++) {
                String tCaseNo = mLLHospCaseSet.get(i).getCaseNo();
                LLHospCaseDB tLLHospCaseDB = new LLHospCaseDB();
                tLLHospCaseDB.setCaseNo(tCaseNo);
                if (!tLLHospCaseDB.getInfo()) {
                    buildError("dealData", "案件查询失败");
                    return false;
                }

                LLHospCaseSchema tLLHospCaseSchema = new LLHospCaseSchema();
                tLLHospCaseSchema = tLLHospCaseDB.getSchema();
                tLLHospCaseSchema.setHCNo(mHCNo);
                tLLHospCaseSchema.setModifyDate(mCurrentDate);
                tLLHospCaseSchema.setModifyTime(mCurrentTime);
                tLLHospCaseSet.add(tLLHospCaseSchema);
            }
            map.put(tLLHospCaseSet, "UPDATE");
        }

        if ("DELETE".equals(mOperate)) {
            LLHospCaseSet tLLHospCaseSet = new LLHospCaseSet();
            for (int i = 1; i <= mLLHospCaseSet.size(); i++) {
                String tCaseNo = mLLHospCaseSet.get(i).getCaseNo();
                LLHospCaseDB tLLHospCaseDB = new LLHospCaseDB();
                tLLHospCaseDB.setCaseNo(tCaseNo);
                if (!tLLHospCaseDB.getInfo()) {
                    buildError("dealData", "案件查询失败");
                    return false;
                }

                LLHospCaseSchema tLLHospCaseSchema = tLLHospCaseDB.getSchema();
                tLLHospCaseSchema.setHCNo(null);
                tLLHospCaseSchema.setModifyDate(mCurrentDate);
                tLLHospCaseSchema.setModifyTime(mCurrentTime);
                tLLHospCaseSet.add(tLLHospCaseSchema);
            }
            map.put(tLLHospCaseSet, "UPDATE");

        }

        if ("CONFIRM".equals(mOperate)) {
            String tPayMode = mLLHospGetSchema.getPayMode();
            if (tPayMode == null || "".equals(tPayMode)) {
                buildError("dealData", "领取方式不能为空");
                return false;
            }

            String tBankCode = mLLHospGetSchema.getBankCode();
            String tBankAccNo = mLLHospGetSchema.getBankAccNo();
            String tAccName = mLLHospGetSchema.getAccName();
            String tPrePaidFlag = (String) mTransferData.getValueByName("PrePaidFlag");  //回销预付赔款

            if (!"1".equals(tPayMode) && !"2".equals(tPayMode)) {

                if (tBankCode == null || "".equals(tBankCode)) {
                    buildError("dealData", "银行编码不能为空");
                    return false;
                }

                if (tBankAccNo == null || "".equals(tBankAccNo)) {
                    buildError("dealData", "银行账号不能为空");
                    return false;
                }

                if (tAccName == null || "".equals(tAccName)) {
                    buildError("dealData", "银行账号名不能为空");
                    return false;
                }
            }
            String tDrawer = mLLHospGetSchema.getDrawer();

            LLHospCaseDB tLLHospCaseDB = new LLHospCaseDB();
            tLLHospCaseDB.setHCNo(mHCNo);
            LLHospCaseSet tLLHospCaseSet = tLLHospCaseDB.query();
            if (tLLHospCaseSet.size() < 1) {
                buildError("dealData", "批次下没有可结算案件");
                return false;
            }
            
            VData tVData = new VData();
            tVData.add(mLLHospGetSchema);
            tVData.add(mGlobalInput);
            LLHospGetDealPayBL tLLHospGetDealPayBL = new LLHospGetDealPayBL();
            if (!tLLHospGetDealPayBL.submitData(tVData, tPrePaidFlag)) {
            	buildError("dealData", tLLHospGetDealPayBL.mErrors.getFirstError());
            	return false;
            }
            
//            for (int i = 1; i <= tLLHospCaseSet.size(); i++) {
//                String tCaseNo = tLLHospCaseSet.get(i).getCaseNo();
//                LLCaseSchema tLLCaseSchema = new LLCaseSchema();
//                LJAGetSchema tLJAGetSchema = new LJAGetSchema();
//                tLLCaseSchema.setCaseNo(tCaseNo);
//                tLLCaseSchema.setRgtNo(tCaseNo);
//
//                tLJAGetSchema.setOtherNo(tCaseNo);
//                tLJAGetSchema.setOtherNoType("5");
//                tLJAGetSchema.setAccName(tAccName);
//                tLJAGetSchema.setBankAccNo(tBankAccNo);
//                tLJAGetSchema.setPayMode(tPayMode);
//                tLJAGetSchema.setBankCode(tBankCode);
//                tLJAGetSchema.setDrawer(tDrawer);
//
//                VData tVData = new VData();
//                tVData.add(tLLCaseSchema);
//                tVData.add(tLJAGetSchema);
//                tVData.add(mGlobalInput);
//                LJAGetInsertBL tLJAGetInsertBL = new LJAGetInsertBL();
//                if (!tLJAGetInsertBL.submitData(tVData, "")) {
//                    buildError("dealData", tLJAGetInsertBL.mErrors.getFirstError());
//                    return false;
//                }
//            }

            mULLHospGetSchema.setState("1");
            mULLHospGetSchema.setPayMode(tPayMode);
            mULLHospGetSchema.setBankCode(tBankCode);
            mULLHospGetSchema.setBankAccNo(tBankAccNo);
            mULLHospGetSchema.setAccName(tAccName);
            mULLHospGetSchema.setDrawer(tDrawer);
            mULLHospGetSchema.setModifyDate(mCurrentDate);
            mULLHospGetSchema.setModifyTime(mCurrentTime);

            map.put(mULLHospGetSchema, "UPDATE");
        }

        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData() {
        mInputData.clear();
        mInputData.add(map);
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult() {
        return mResult;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLHospFinanceBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    public static void main(String arg[]) {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "001";
    }
}
