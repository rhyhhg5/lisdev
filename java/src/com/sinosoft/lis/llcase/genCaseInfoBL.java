package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: ClientRegisterBL </p>
 * <p>Description: ClientRegisterBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author:Xx
 * @CreateDate：2005-02-18 13:58:55
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.CachedRiskInfo;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class genCaseInfoBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private FDate fDate = new FDate();

    /** 业务处理相关变量 */
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private LLCaseSchema aLLCaseSchema = new LLCaseSchema();
    private LLCaseExtSchema mLLCaseExtSchema = new LLCaseExtSchema();//2807领款人和被保人关
    private LLSubReportSchema mLLSubReportSchema = new LLSubReportSchema();
    private LLCaseRelaSchema mLLCaseRelaSchema = new LLCaseRelaSchema();
    private LLFeeMainSchema mLLFeeMainSchema = new LLFeeMainSchema();
    private LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
    private LLSecurityReceiptSchema mLLSecurityReceiptSchema = new
            LLSecurityReceiptSchema();
    private LLCaseCureSet mLLCaseCureSet = new LLCaseCureSet();
    private LLAppClaimReasonSchema mLLAppClaimReasonSchema = new
            LLAppClaimReasonSchema();
    private LLToClaimDutySet mLLToClaimDutySet = new LLToClaimDutySet();
    private LLCaseReceiptSchema mLLCaseReceiptShema = new LLCaseReceiptSchema();
    //社保明细
    private LLFeeOtherItemSet mLLFeeOtherItemSet = new LLFeeOtherItemSet();

    private LLCasePolicySet mLLCasePolicySet = new LLCasePolicySet();
    private String aCaseNo = "";
    private String aRgtNo = "";
    private String aSubRptNo = "";
    private String aCaseRelaNo = "";
    private String aCureNo = "";
    private String aMainFeeNo = "";
    private String aDate = "";
    private String aTime = "";
    private String oDate = "";
    private String oTime = "";
    ExeSQL tExeSQL = new ExeSQL();

    public genCaseInfoBL() {
    }
    public static void main(String[] args) {
        String strOperate = "INSERT||MAIN";
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
        LLSecurityReceiptSchema tLLSecurityReceiptSchema = new
                LLSecurityReceiptSchema();
        LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
        LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
        VData tVData = new VData();
        genCaseInfoBL tgenCaseInfoBL = new genCaseInfoBL();

        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86110000";
        mGlobalInput.ComCode = "86110000";
        mGlobalInput.Operator = "bcm005";

        tLLCaseSchema.setRgtType("1");
        tLLRegisterSchema.setRgtNo("P0000060522000001");

        /**************************************************/
        tLLCaseSchema.setRgtState("01"); //案件状态
        tLLCaseSchema.setCaseNo("C0000060525000006");
        tLLCaseSchema.setCustomerName("刘欣");
        tLLCaseSchema.setCustomerNo("000310000");
        tLLCaseSchema.setPostalAddress("");
        tLLCaseSchema.setPhone("");
        tLLCaseSchema.setDeathDate("");
        tLLCaseSchema.setCustBirthday("1941-03-13");
        tLLCaseSchema.setCustomerSex("0");
        tLLCaseSchema.setIDType("0");
        tLLCaseSchema.setIDNo("110105194103131822");
        tLLCaseSchema.setCustState("04");
        tLLCaseSchema.setDeathDate("2005-5-1");
        tLLFeeMainSchema.setRgtNo("P0000060522000001");
        tLLFeeMainSchema.setCaseNo("");
        tLLFeeMainSchema.setCustomerNo("000310000");
        tLLFeeMainSchema.setCustomerSex("0");
        tLLFeeMainSchema.setHospitalCode("");
        tLLFeeMainSchema.setHospitalName("");
        tLLFeeMainSchema.setHosAtti("");
        tLLFeeMainSchema.setReceiptNo("53000");
        tLLFeeMainSchema.setFeeType("31");
        tLLFeeMainSchema.setFeeAtti("1");
        tLLFeeMainSchema.setFeeDate("2006-5-1");
        tLLFeeMainSchema.setHospStartDate("2006-5-1");
        tLLFeeMainSchema.setHospEndDate("2006-5-11");
        tLLFeeMainSchema.setRealHospDate("");
        tLLFeeMainSchema.setInHosNo("");
        tLLFeeMainSchema.setSecurityNo("");
        tLLFeeMainSchema.setInsuredStat("");
        tLLSecurityReceiptSchema.setApplyAmnt("300");
        tLLSecurityReceiptSchema.setSelfAmnt("2");
        tLLSecurityReceiptSchema.setSelfPay2("2");
        tLLSecurityReceiptSchema.setGetLimit("2");
        tLLSecurityReceiptSchema.setMidAmnt("2");
        tLLSecurityReceiptSchema.setPlanFee("2");
        tLLSecurityReceiptSchema.setHighAmnt2("22");
        tLLSecurityReceiptSchema.setSupInHosFee("2");
        tLLSecurityReceiptSchema.setHighAmnt1("222");
        tLLSecurityReceiptSchema.setOfficialSubsidy("");
        LLAppClaimReasonSchema tLLAppClaimReasonSchema = new
                LLAppClaimReasonSchema();
        tLLAppClaimReasonSchema.setReasonCode("02"); //原因代码
        tLLAppClaimReasonSchema.setReason("住院"); //申请原因
        tLLAppClaimReasonSchema.setCustomerNo("000310000");
        tLLAppClaimReasonSchema.setReasonType("0");

        tLLCaseCureSchema.setDiseaseCode("333");
        tLLCaseCureSchema.setDiseaseName("对当地");
        tLLCaseOpTimeSchema.setRgtState("01");
        tLLCaseOpTimeSchema.setStartDate("2005-9-15");
        tLLCaseOpTimeSchema.setStartTime("20:30:30");
        tVData.add(tLLCaseSchema);
        tVData.add(tLLRegisterSchema);
        tVData.add(tLLFeeMainSchema);
        tVData.add(tLLSecurityReceiptSchema);
        tVData.add(tLLAppClaimReasonSchema);
        tVData.add(tLLCaseCureSchema);
        tVData.add(tLLCaseOpTimeSchema);

        tVData.add(mGlobalInput);
        tgenCaseInfoBL.submitData(tVData, strOperate);
        tVData.clear();
        tVData = tgenCaseInfoBL.getResult();
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "genCaseInfoBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败genCaseInfoBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("Start ClientRegisterBL Submit...");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDDrugBL";
            tError.functionName = "ClientRegisterBL";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("End SimpleClaimBL Submit...");
        mInputData = null;

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("--Star GetInputData--");
        this.mLLRegisterSchema.setSchema((LLRegisterSchema) cInputData.
                                         getObjectByObjectName(
                "LLRegisterSchema", 0));
        this.mLLCaseSchema.setSchema((LLCaseSchema) cInputData.
                                     getObjectByObjectName("LLCaseSchema", 0));
        LLCaseExtSchema tLLCaseExtSchema = new LLCaseExtSchema();//2807领款人和被保人关
        tLLCaseExtSchema = (LLCaseExtSchema)cInputData.getObjectByObjectName("LLCaseExtSchema", 0);
        if(tLLCaseExtSchema!=null){
        	this.mLLCaseExtSchema.setSchema(tLLCaseExtSchema);//2807领款人和被保人关
        }
        LLSubReportSchema tLLSubReportSchcema = new LLSubReportSchema(); // 3500 增加发生地点省、市、县 
        tLLSubReportSchcema=(LLSubReportSchema) cInputData.getObjectByObjectName("LLSubReportSchema", 0);
        if(tLLSubReportSchcema!=null){
        	this.mLLSubReportSchema.setSchema(tLLSubReportSchcema);
        }   
        this.mLLFeeMainSchema.setSchema((LLFeeMainSchema) cInputData.
                                        getObjectByObjectName("LLFeeMainSchema",
                0));
        this.mLLSecurityReceiptSchema.setSchema((LLSecurityReceiptSchema)
                                                cInputData.
                                                getObjectByObjectName(
                "LLSecurityReceiptSchema", 0));
        this.mLLCaseCureSet = (LLCaseCureSet) cInputData.
                              getObjectByObjectName("LLCaseCureSet", 0);
        System.out.println();
        LLAppClaimReasonSchema tLLAppClaimReasonSchema=(LLAppClaimReasonSchema)cInputData.getObjectByObjectName("LLAppClaimReasonSchema", 0);
        System.out.println("tLLAppClaimReasonSchema:"+tLLAppClaimReasonSchema);
        if(tLLAppClaimReasonSchema!=null&&!"null".equals(tLLAppClaimReasonSchema)){
        this.mLLAppClaimReasonSchema.setSchema((LLAppClaimReasonSchema)
                                               cInputData.
                                               getObjectByObjectName(
                "LLAppClaimReasonSchema", 0));
        }
        mLLCaseOpTimeSchema = (LLCaseOpTimeSchema) cInputData.
                              getObjectByObjectName("LLCaseOpTimeSchema", 0);
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        if (mGlobalInput.ManageCom.trim().length() == 2) {
            mGlobalInput.ManageCom = mGlobalInput.ManageCom.trim() + "000000";
        }
        if (mGlobalInput.ManageCom.trim().length() == 4) { 
            mGlobalInput.ManageCom = mGlobalInput.ManageCom.trim() + "0000";
        }
        aDate = PubFun.getCurrentDate();
        aTime = PubFun.getCurrentTime();
        oDate = PubFun.getCurrentDate();
        oTime = PubFun.getCurrentTime();
        if (mLLCaseSchema.getCaseNo() != null) {
            LLCaseDB tLLCaseDB = new LLCaseDB();
            tLLCaseDB.setCaseNo(mLLCaseSchema.getCaseNo());
            if (tLLCaseDB.getInfo()) {
                if (!tLLCaseDB.getRgtState().equals("01")
                    && !tLLCaseDB.getRgtState().equals("02")
                    && !tLLCaseDB.getRgtState().equals("03")
                    && !tLLCaseDB.getRgtState().equals("04")
                    && !tLLCaseDB.getRgtState().equals("08")) {
                    CError.buildErr(this, "该状态不允许修改案件信息！");
                    return false;
                }
                mOperate = "UPDATE||MAIN";
                aLLCaseSchema.setSchema(tLLCaseDB.getSchema());
                oDate = aLLCaseSchema.getMakeDate();
                oTime = aLLCaseSchema.getMakeTime();
            }
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("---Star dealData---mOperate---" + mOperate);
        if (this.mOperate.equals("INSERT||MAIN")) {
            if (!insertData()) {
                return false;
            }
        }

        if (this.mOperate.equals("UPDATE||MAIN")) {
            if (!updateData()) {
                return false;
            }
        }
        if (!fillCasePolicy()) {
            CError.buildErr(this, "没有符合条件的保单！");
            return false;
        }
        if (mLLToClaimDutySet.size() <= 0) {
            CError.buildErr(this, "没有符合条件的责任,请确认账单类型,医疗标志或者给付责任类型等是否正确");
            return false;
        }
        map.put(mLLToClaimDutySet, "DELETE&INSERT");

        return true;
    }

    private boolean insertData() {

        /*生成理赔号*/
        System.out.println("---Star Insert case---");
        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        VData tno = new VData();
        tno.add(0, "1");
        tno.add(1, aRgtNo);
        tno.add(2, "STORE");
        aCaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit, tno);
        if (aCaseNo.length() < 17) {
            aCaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit);
        }
        aSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);
        aCaseRelaNo = PubFun1.CreateMaxNo("CASERELANO", tLimit);
        aMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO", tLimit);
//        aCureNo = PubFun1.CreateMaxNo("CURENO", tLimit);
        aRgtNo = mLLRegisterSchema.getRgtNo();
        System.out.println("理赔号" + aCaseNo);
        System.out.println("申请号" + aRgtNo);

        if (!getCaseInfo()) {
            return false;
        }

        mLLCaseOpTimeSchema.setCaseNo(aCaseNo);
        mLLCaseOpTimeSchema.setRgtState("03");
        mLLCaseOpTimeSchema.setOperator(mGlobalInput.Operator);
        mLLCaseOpTimeSchema.setManageCom(mGlobalInput.ManageCom);
        LLCaseCommon tLLCaseCommon = new LLCaseCommon();
        LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(
                mLLCaseOpTimeSchema);
        map.put(tLLCaseOpTimeSchema, "INSERT");
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        /*生成理赔号*/
        System.out.println("---Star Insert case---");
        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        aCaseNo = aLLCaseSchema.getCaseNo();
        aRgtNo = mLLRegisterSchema.getRgtNo();
        LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
        tLLCaseRelaDB.setCaseNo(aCaseNo);
        LLCaseRelaSet tLLCaseRelaSet = tLLCaseRelaDB.query();
        if (tLLCaseRelaSet.size() > 0) {
            aCaseRelaNo = tLLCaseRelaSet.get(1).getCaseRelaNo();
            aSubRptNo = tLLCaseRelaSet.get(1).getSubRptNo();
        } else {
            aSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);
            aCaseRelaNo = PubFun1.CreateMaxNo("CASERELANO", tLimit);
        }
        LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
        tLLFeeMainDB.setCaseNo(aCaseNo);
        LLFeeMainSet tLLFeeMainSet = tLLFeeMainDB.query();
        if (tLLFeeMainSet.size() > 0) {
            aMainFeeNo = tLLFeeMainSet.get(1).getMainFeeNo();
        } else {
            aMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO", tLimit);
        }
        LLCaseCureDB tLLCaseCureDB = new LLCaseCureDB();
        tLLCaseCureDB.setCaseNo(aCaseNo);
        LLCaseCureSet tLLCaseCureSet = tLLCaseCureDB.query();
        map.put(tLLCaseCureSet, "DELETE");
        System.out.println("理赔号" + aCaseNo);
        System.out.println("申请号" + aRgtNo);

        if (!getCaseInfo()) {
            return false;
        }

        return true;
    }

    private boolean getCaseInfo() {
        int CustAge = 0;
        try {
            CustAge = calAge();
        } catch (Exception ex) {
            System.out.println("计算年龄错误");
        }
        mLLCaseSchema.setCustomerAge(CustAge);
        mLLCaseSchema.setRgtNo(aRgtNo);
        mLLCaseSchema.setCaseNo(aCaseNo);
        mLLCaseSchema.setHandler(mGlobalInput.Operator);
        mLLCaseSchema.setDealer(mGlobalInput.Operator);
        mLLCaseSchema.setRigister(mGlobalInput.Operator);
        mLLCaseSchema.setRgtDate(PubFun.getCurrentDate());
        mLLCaseSchema.setOperator(mGlobalInput.Operator);
        mLLCaseSchema.setMngCom(mGlobalInput.ManageCom);
        mLLCaseSchema.setMakeDate(aDate);
        mLLCaseSchema.setMakeTime(aTime);
        mLLCaseSchema.setModifyDate(oDate);
        mLLCaseSchema.setModifyTime(oTime);
        mLLCaseSchema.setCaseProp("05");
        
        //2807领款人和被保人关 固定在底层生成的数据**start**
        mLLCaseExtSchema.setCaseNo(aCaseNo);
        mLLCaseExtSchema.setOperator(mGlobalInput.Operator);
        mLLCaseExtSchema.setMngCom(mGlobalInput.ManageCom);
        mLLCaseExtSchema.setMakeDate(aDate);
        mLLCaseExtSchema.setMakeTime(aTime);
        mLLCaseExtSchema.setModifyDate(oDate);
        mLLCaseExtSchema.setModifyTime(oTime);
      //2807领款人和被保人关 **end**
        if (!genSubReport()) {
            CError.buildErr(this, "生成事件失败！");
            return false;
        }
        if(mLLAppClaimReasonSchema.getReasonCode()!=null&&!"".equals(mLLAppClaimReasonSchema.getReasonCode())){
        mLLAppClaimReasonSchema.setRgtNo(aRgtNo);
        mLLAppClaimReasonSchema.setCaseNo(aCaseNo);
        mLLAppClaimReasonSchema.setOperator(mGlobalInput.Operator);
        mLLAppClaimReasonSchema.setMngCom(mGlobalInput.ManageCom);
        mLLAppClaimReasonSchema.setMakeDate(aDate);
        mLLAppClaimReasonSchema.setMakeTime(aTime);
        mLLAppClaimReasonSchema.setModifyDate(oDate);
        mLLAppClaimReasonSchema.setModifyTime(oTime);
        }
        mLLFeeMainSchema.setAge(CustAge);
        mLLFeeMainSchema.setMainFeeNo(aMainFeeNo);
        mLLFeeMainSchema.setCustomerName(mLLCaseSchema.getCustomerName());
        mLLFeeMainSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
        mLLFeeMainSchema.setRgtNo(aRgtNo);
        mLLFeeMainSchema.setCaseNo(aCaseNo);
        //mLLFeeMainSchema.setRefuseAmnt(mLLSecurityReceiptSchema.getSelfPay2());
        mLLFeeMainSchema.setSelfAmnt(mLLSecurityReceiptSchema.getSelfAmnt());
        mLLFeeMainSchema.setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());
        mLLFeeMainSchema.setOperator(mGlobalInput.Operator);
        mLLFeeMainSchema.setMngCom(mGlobalInput.ManageCom);
        mLLFeeMainSchema.setMakeDate(aDate);
        mLLFeeMainSchema.setMakeTime(aTime);
        mLLFeeMainSchema.setModifyDate(oDate);
        mLLFeeMainSchema.setModifyTime(oTime);




        if (!calSecu()) {
            CError.buildErr(this, "生成社保帐单失败！");
            return false;
        }
        
        mLLSecurityReceiptSchema.setMainFeeNo(aMainFeeNo);
        mLLSecurityReceiptSchema.setFeeDetailNo(aMainFeeNo);
        mLLSecurityReceiptSchema.setRgtNo(aRgtNo);
        mLLSecurityReceiptSchema.setCaseNo(aCaseNo);
        mLLSecurityReceiptSchema.setOperator(mGlobalInput.Operator);
        mLLSecurityReceiptSchema.setMngCom(mGlobalInput.ManageCom);
        mLLSecurityReceiptSchema.setMakeDate(aDate);
        mLLSecurityReceiptSchema.setMakeTime(aTime);
        mLLSecurityReceiptSchema.setModifyDate(oDate);
        mLLSecurityReceiptSchema.setModifyTime(oTime);
	    if (!saveFeeItem()) {
	        	CError.buildErr(this, "社保明细帐单生成失败！");
	            return false;
        }
        for (int i = 1; i <= mLLCaseCureSet.size(); i++) {
//            if (mLLCaseCureSet.get(i).getDiseaseCode() == null ||
//                mLLCaseCureSet.get(i).getDiseaseCode().equals(""))
//                continue;
            String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
            aCureNo = PubFun1.CreateMaxNo("CURENO", tLimit);
            mLLCaseCureSet.get(i).setSerialNo(aCureNo);
            mLLCaseCureSet.get(i).setCustomerNo(mLLCaseSchema.getCustomerNo());
            mLLCaseCureSet.get(i).setCustomerName(mLLCaseSchema.getCustomerName());
            mLLCaseCureSet.get(i).setDiagnose(mLLCaseCureSet.get(i).
                                              getDiseaseName());
            mLLCaseCureSet.get(i).setHospitalCode(mLLFeeMainSchema.
                                                  getHospitalCode());
            mLLCaseCureSet.get(i).setHospitalName(mLLFeeMainSchema.
                                                  getHospitalName());
            mLLCaseCureSet.get(i).setCaseNo(aCaseNo);
            mLLCaseCureSet.get(i).setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());
            mLLCaseCureSet.get(i).setSeriousFlag("0");
            mLLCaseCureSet.get(i).setOperator(mGlobalInput.Operator);
            mLLCaseCureSet.get(i).setMngCom(mGlobalInput.ManageCom);
            mLLCaseCureSet.get(i).setMakeDate(aDate);
            mLLCaseCureSet.get(i).setMakeTime(aTime);
            mLLCaseCureSet.get(i).setModifyDate(oDate);
            mLLCaseCureSet.get(i).setModifyTime(oTime);

        }
            //插入明细表，以便理算普通的住院或者门诊险时，可以处理。
            String tLimit1 = PubFun.getNoLimit("86");
            mLLCaseReceiptShema.setFeeDetailNo(
                    PubFun1.CreateMaxNo("FEEDETAILNO", tLimit1));
            mLLCaseReceiptShema.setMainFeeNo(aMainFeeNo);
            mLLCaseReceiptShema.setRgtNo(aRgtNo);
            mLLCaseReceiptShema.setCaseNo(aCaseNo);
            mLLCaseReceiptShema.setFeeItemCode("DRZFY");
            mLLCaseReceiptShema.setFeeItemName("导入总费用");
            mLLCaseReceiptShema.setFee(mLLFeeMainSchema.getSumFee());
            mLLCaseReceiptShema.setAvaliFlag("0");
            mLLCaseReceiptShema.setMngCom(mGlobalInput.ManageCom);
            mLLCaseReceiptShema.setOperator(mGlobalInput.Operator);
            mLLCaseReceiptShema.setMakeDate(aDate);
            mLLCaseReceiptShema.setMakeTime(aTime);
            mLLCaseReceiptShema.setModifyDate(aDate);
            mLLCaseReceiptShema.setModifyTime(aTime);
            mLLCaseReceiptShema.setSelfAmnt(mLLSecurityReceiptSchema.getSelfAmnt());
            mLLCaseReceiptShema.setRefuseAmnt(mLLFeeMainSchema.getRefuseAmnt());
            mLLCaseReceiptShema.setSocialPlanAmnt(mLLSecurityReceiptSchema.getPlanFee()+mLLSecurityReceiptSchema.getSupInHosFee());

        map.put(mLLCaseSchema, "DELETE&INSERT");
        map.put(mLLCaseExtSchema, "DELETE&INSERT");//2807领款人和被保人关
        map.put(mLLFeeMainSchema, "DELETE&INSERT");
        map.put(mLLSecurityReceiptSchema, "DELETE&INSERT");
        map.put(mLLCaseCureSet, "DELETE&INSERT");
        if(mLLAppClaimReasonSchema.getReasonCode()!=null&&!"".equals(mLLAppClaimReasonSchema.getReasonCode())){
        map.put(mLLAppClaimReasonSchema, "DELETE&INSERT");
        }
        map.put(mLLCaseReceiptShema, "DELETE&INSERT");
        map.put(mLLFeeOtherItemSet, "DELETE&INSERT");

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 生成事件及与案件的关联
     * @return boolean
     */
    private boolean genSubReport() {
        //立案分案,即事件
        mLLSubReportSchema.setSubRptNo(aSubRptNo);
        mLLSubReportSchema.setCustomerName(mLLCaseSchema.getCustomerName());
        mLLSubReportSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
        mLLSubReportSchema.setAccDate(mLLCaseSchema.getAccidentDate());
        mLLSubReportSchema.setAccPlace(mLLFeeMainSchema.getHospitalName());
//        if(mLLFeeMainSchema.getHospStartDate()!=null)
        mLLSubReportSchema.setInHospitalDate(mLLFeeMainSchema.getHospStartDate());
//        if(mLLFeeMainSchema.getHospEndDate()!=null)
        mLLSubReportSchema.setOutHospitalDate(mLLFeeMainSchema.getHospEndDate());
        if (mLLCaseCureSet.size() > 0) {
            mLLSubReportSchema.setAccDesc(mLLCaseCureSet.get(1).getDiseaseName());
        }
        mLLSubReportSchema.setOperator(mGlobalInput.Operator);
        mLLSubReportSchema.setMngCom(mGlobalInput.ManageCom);
        mLLSubReportSchema.setMakeDate(PubFun.getCurrentDate());
        mLLSubReportSchema.setMakeTime(PubFun.getCurrentTime());
        mLLSubReportSchema.setModifyDate(PubFun.getCurrentDate());
        mLLSubReportSchema.setModifyTime(PubFun.getCurrentTime());

        mLLCaseRelaSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        mLLCaseRelaSchema.setSubRptNo(aSubRptNo);
        mLLCaseRelaSchema.setCaseRelaNo(aCaseRelaNo);
        map.put(mLLSubReportSchema, "DELETE&INSERT");
        map.put(mLLCaseRelaSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 校验计算社保信息
     * @return boolean
     */
    private boolean calSecu() {
        double aSelfPay1 = mLLSecurityReceiptSchema.getGetLimit()
                           + mLLSecurityReceiptSchema.getMidAmnt()
                           + mLLSecurityReceiptSchema.getHighAmnt1()
                           + mLLSecurityReceiptSchema.getSuperAmnt();
//                           +mLLSecurityReceiptSchema.getHighAmnt2();
        if (mLLSecurityReceiptSchema.getSelfPay1()<=0) {
        	mLLSecurityReceiptSchema.setSelfPay1(aSelfPay1);
        }
//        double aFeeIS = mLLSecurityReceiptSchema.getPlanFee()
//                        +mLLSecurityReceiptSchema.getSupInHosFee()
//                        +aSelfPay1;
//        double aFeeOS = mLLSecurityReceiptSchema.getSelfPay2()
//                        +mLLSecurityReceiptSchema.getSelfAmnt();
//        mLLSecurityReceiptSchema.setFeeInSecu(aFeeIS);
//        mLLSecurityReceiptSchema.setFeeOutSecu(aFeeOS);
        return true;
    }

    /**
     * 过滤要赔付的责任
     * @return boolean
     */
    private boolean getClaimDuty(LCPolSchema tpolschema) {
        String aCaseNo = mLLCaseSchema.getCaseNo();
        String aCaseRelaNo = mLLCaseRelaSchema.getCaseRelaNo();
        String aSubRptNo = mLLSubReportSchema.getSubRptNo();
        SynLCLBGetBL aSynLCLBGetBL = new SynLCLBGetBL();
        aSynLCLBGetBL.setPolNo(tpolschema.getPolNo());

        aSynLCLBGetBL.setGrpContNo(mLLRegisterSchema.getRgtObjNo());
        if(!(mLLCaseSchema.getPhone()==null||mLLCaseSchema.getPhone()=="")){
        	aSynLCLBGetBL.setGetDutyCode(mLLCaseSchema.getPhone());
        }
        LCGetSet tLCGetSet = aSynLCLBGetBL.query();
        for (int i = 1; i <= tLCGetSet.size(); i++) {
            LLToClaimDutySchema row = new LLToClaimDutySchema();
            LCGetSchema lcGet = tLCGetSet.get(i);
            row.setCaseNo(aCaseNo);
            row.setCaseRelaNo(aCaseRelaNo);
            row.setSubRptNo(aSubRptNo);
            row.setPolNo(lcGet.getPolNo()); /*保单号*/
            row.setGetDutyCode(lcGet.getGetDutyCode()); /* 给付责任编码 */
            row.setDutyCode(lcGet.getDutyCode());
            row.setGetDutyKind("100");
            System.out.println(mLLCaseSchema.getMngCom());
          /*  if (mLLCaseSchema.getMngCom().substring(0, 4).trim().equals("8612")) {
                System.out.println(mLLSecurityReceiptSchema.getMidAmnt());
                System.out.println(mLLSecurityReceiptSchema.getHighDoorAmnt());
                if ((mLLSecurityReceiptSchema.getMidAmnt() == 0) &&
                    (mLLSecurityReceiptSchema.getHighAmnt1() == 0)) {
                    row.setGetDutyKind("200");
                }
            }*/
            System.out.println(mLLFeeMainSchema.getFeeType());
            System.out.println(mLLFeeMainSchema.getOriginFlag());
            if(mLLFeeMainSchema.getFeeType()!=null){
            if ("1".equals(mLLFeeMainSchema.getFeeType().trim())) {
                row.setGetDutyKind("200");
            }
            }
            //OriginFlag标志表示批次录入时，该项是否是特需险，1位是。
            if(mLLFeeMainSchema.getOriginFlag()!=null){
                if(mLLFeeMainSchema.getOriginFlag().trim().equals("1")){
                   row.setGetDutyKind("000");
                }
            }
            if(mLLFeeMainSchema.getOldMainFeeNo()!=null){
            	row.setGetDutyKind(mLLFeeMainSchema.getOldMainFeeNo());
            	mLLFeeMainSchema.setOldMainFeeNo("");
            }
            
             String aAffixNo="" + mLLFeeMainSchema.getAffixNo();
            if(!(aAffixNo==null || "".equals(aAffixNo) ||"null".equals(aAffixNo))){
            	String ttGetDutyCode ="" + mLLCaseSchema.getPhone();
            	if(ttGetDutyCode==null || "".equals(ttGetDutyCode) ||"null".equals(ttGetDutyCode)){  	
                        String rsql="select 1 from(select getdutykind a,count(getdutycode) b from lmdutygetclm where getdutykind='"+row.getGetDutyKind() +"' and getdutycode in (select getdutycode from lcget where polno='"+lcGet.getPolNo()+"' and GrpContNo='"+mLLRegisterSchema.getRgtObjNo()+"' union select getdutycode from lbget where polno='"+lcGet.getPolNo()+"' and GrpContNo='"+mLLRegisterSchema.getRgtObjNo()+"') group by getdutykind )as aa where aa.b>1 with ur";
                        ExeSQL mExeSQL = new ExeSQL();
                        SSRS mTitleSSRS = mExeSQL.execSQL(rsql);
                        if (mTitleSSRS.getMaxRow() > 0) {
                        	mErrors.addOneError("‘给付责任编码’或‘就诊类别’，‘医疗标记’错误导致金额重复！");
                            return false;
                        }
                    }
            
            		
            }
            
            CachedRiskInfo tCachedRiskInfo = CachedRiskInfo.getInstance();
			LMDutyGetClmSchema tLMDutyGetClmSchema = tCachedRiskInfo
					.findLMDutyGetClm(row.getGetDutyCode(), row.getGetDutyKind());
			if (tLMDutyGetClmSchema == null) {
				continue;
			}

            /* 给付责任类型 */
            row.setGrpContNo(lcGet.getGrpContNo()); /* 集体合同号 */
            row.setGrpPolNo(tpolschema.getGrpContNo()); /* 集体保单号 */
            row.setContNo(lcGet.getContNo()); /* 个单合同号 */
            row.setKindCode(tpolschema.getKindCode()); /* 险类代码 */
            row.setRiskCode(tpolschema.getRiskCode()); /* 险种代码 */
            row.setRiskVer(tpolschema.getRiskVersion()); /* 险种版本号 */
            row.setPolMngCom(tpolschema.getManageCom()); /* 保单管理机构 */
            row.setClaimCount(0); /* 理算次数 */
            mLLToClaimDutySet.add(row);
        }
        return true;
    }

    /**
     * 查询保单信息包括C表和B表的信息
     * 以后要考虑保单状态表里的信息
     * @param string String
     * @return LCPolSchema
     */
    private LCPolSchema getLCPol(String polNo) {
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setPolNo(polNo);
        if (!tLCPolBL.getInfo()) {
            mErrors.addOneError("险种保单LCPol查询失败,请契约运维核实保单数据是否有误!");
            return null;
        } else {
            return tLCPolBL.getSchema();
        }
    }

    /**
     * 案件保单信息
     * @return boolean
     */
    private boolean fillCasePolicy() {
        String aCaseNo = mLLCaseSchema.getCaseNo();
        String aCaseRelaNo = mLLCaseRelaSchema.getCaseRelaNo();
        String aRgtNo = mLLCaseSchema.getRgtNo();
        LCPolBL tLCPolBL = new LCPolBL();
        try {
        String PolSQL="select polno from lcpol where insuredno='"+mLLCaseSchema.getCustomerNo()+"' and grpcontno='"+mLLRegisterSchema.getRgtObjNo()+"' " +
	                  " union " +
	                  "select polno from lbpol where insuredno='"+mLLCaseSchema.getCustomerNo()+"' and grpcontno='"+mLLRegisterSchema.getRgtObjNo()+"'";
        ExeSQL tExeSQL=new ExeSQL();
        SSRS PolResult=tExeSQL.execSQL(PolSQL);
        int num=0;
        CErrors tErrors = new CErrors();
        for(int mm=1;mm<=PolResult.getMaxNumber();mm++){
  //      	System.out.println("======"+PolResult.getMaxNumber());
        tLCPolBL.setPolNo(PolResult.GetText(mm, 1));
        tLCPolBL.setInsuredNo(mLLCaseSchema.getCustomerNo());
        tLCPolBL.setGrpContNo(mLLRegisterSchema.getRgtObjNo());
        LCPolSet tLCPolSet = new LCPolSet();
        tLCPolSet = tLCPolBL.query();
        int n = tLCPolSet.size();
        String tdate = PubFun.getCurrentDate();
        String ttime = PubFun.getCurrentTime();
        String strAccidentDate = mLLCaseSchema.getAccidentDate();
        if (strAccidentDate == null || strAccidentDate.equals("")) {
            strAccidentDate = mLLFeeMainSchema.getFeeDate();
        }
        
            for (int i = 1; i <= n; i++) {
                //处理保单状态
                String opolstate = "" + tLCPolSet.get(i).getPolState();
                String npolstate = "";
                if (opolstate.equals("null") || opolstate.equals("")) {
                    npolstate = "0400";
                } else {
                    if (opolstate.substring(0, 2).equals("04")) {
                        npolstate = "0400" + opolstate.substring(4);
                    } else {
                        if (opolstate.length() < 4) {
                            npolstate = "0400" + opolstate;
                        } else {
                            npolstate = "0400" + opolstate.substring(0, 4);
                        }
                    }
                }
                tLCPolSet.get(i).setPolState(npolstate);
                LLCasePolicySchema tLLCasePolicySchema = new LLCasePolicySchema();
                LCPolSchema tLCPolSchema = tLCPolSet.get(i);
                LLCaseCommon tLLCaseCommon = new LLCaseCommon();
                if (!(strAccidentDate == null || strAccidentDate.equals(""))) {
//                    if (!(CaseFunPub.checkDate(tLCPolSchema.getCValiDate(),
//                                               strAccidentDate))) {
                	if(!tLLCaseCommon.checkPolValid(tLCPolSchema.getPolNo(),strAccidentDate)) {
                        tErrors.addOneError(tLLCaseCommon.mErrors.getFirstError());
                        break;
                    }
                }
                num+=1;
                tLLCasePolicySchema.setCaseNo(aCaseNo);
                tLLCasePolicySchema.setRgtNo(aRgtNo);
                tLLCasePolicySchema.setCaseRelaNo(aCaseRelaNo);
                tLLCasePolicySchema.setOperator(this.mGlobalInput.Operator);
                tLLCasePolicySchema.setMngCom(this.mGlobalInput.ManageCom);
                tLLCasePolicySchema.setMakeDate(tdate);
                tLLCasePolicySchema.setModifyDate(tdate);
                tLLCasePolicySchema.setMakeTime(ttime);
                tLLCasePolicySchema.setModifyTime(ttime);

                if ((tLCPolSchema.getInsuredNo().equals(mLLCaseSchema.
                        getCustomerNo()))) {
                    tLLCasePolicySchema.setCasePolType("0");
                } else {
                    if (tLCPolSchema.getAppntNo().equals(mLLCaseSchema.
                            getCustomerNo())) {
                        tLLCasePolicySchema.setCasePolType("1");
                    }
                }
                tLLCasePolicySchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                tLLCasePolicySchema.setContNo(tLCPolSchema.getContNo());
                tLLCasePolicySchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                tLLCasePolicySchema.setPolNo(tLCPolSchema.getPolNo());
                tLLCasePolicySchema.setKindCode(tLCPolSchema.getKindCode());
                tLLCasePolicySchema.setRiskCode(tLCPolSchema.getRiskCode());
                tLLCasePolicySchema.setRiskVer(tLCPolSchema.getRiskVersion());
                tLLCasePolicySchema.setPolMngCom(tLCPolSchema.getManageCom());
                tLLCasePolicySchema.setSaleChnl(tLCPolSchema.getSaleChnl());
                tLLCasePolicySchema.setAgentCode(tLCPolSchema.getAgentCode());
                tLLCasePolicySchema.setAgentGroup(tLCPolSchema.getAgentGroup());
                tLLCasePolicySchema.setInsuredNo(tLCPolSchema.getInsuredNo());
                tLLCasePolicySchema.setInsuredName(tLCPolSchema.getInsuredName());
                tLLCasePolicySchema.setInsuredSex(tLCPolSchema.getInsuredSex());
                tLLCasePolicySchema.setInsuredBirthday(tLCPolSchema.
                        getInsuredBirthday());
                tLLCasePolicySchema.setAppntNo(tLCPolSchema.getAppntNo());
                tLLCasePolicySchema.setAppntName(tLCPolSchema.getAppntName());
                tLLCasePolicySchema.setCValiDate(tLCPolSchema.getCValiDate());

                tLLCasePolicySchema.setPolType("1"); //正式保单
                mLLCasePolicySet.add(tLLCasePolicySchema);
                getClaimDuty(tLCPolSchema);
                System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"+mm+"sssssssssssssssssssssssssssssssssssss"+i);
                String delSql = "delete from llcasepolicy where caseno='" +
                aCaseNo + "'";
                map.put(delSql, "DELETE");
                map.put(this.mLLCasePolicySet, "INSERT");
            }
            }
        
          if(num<1){
          	mErrors.addOneError(tErrors.getLastError());
        	  return false;
          }
            
            //map.put(tLCPolSet, "UPDATE");
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            CError.buildErr(this, "准备数据出错:" + ex.getMessage());
            return false;
        }
    }

    /**
     * 自动理算，生成赔案信息表。
     * @return boolean
     */
    private boolean calClaim() {
        ClaimCalAutoBL aClaimCalAutoBL = new ClaimCalAutoBL();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        VData aVData = new VData();
        aVData.addElement(mGlobalInput);
        aVData.addElement(tLLCaseSchema);
        if (!aClaimCalAutoBL.submitData(aVData, "Cal")) {
            CError.buildErr(this, "理算失败");
            return false;
        }
        return true;
    }

    /**
     * 计算年龄的函数
     * @return int
     */
    private int calAge() {
        Date Birthday = fDate.getDate(mLLCaseSchema.getCustBirthday());
        String strNow = PubFun.getCurrentDate();
        Date aNow = fDate.getDate(strNow);
        int CustomerAge = PubFun.calInterval(Birthday, aNow, "Y");
        System.out.println("年龄计算函数。。。" + CustomerAge);
        return CustomerAge;
    }
    
    /**
     * 保存社保明细
     * @return boolean
     * @throws Exception
     */
    private boolean saveFeeItem() {
        System.out.println("genCaseInfoBL--saveFeeItem");
        //为提高批导效率，特此优化
        //合计金额
        	 String llsecufeeitems=" select code,CodeName,CodeAlias from ldcode where 1=1 "
					+ " and codetype='llsecufeeitem' "
					+ " and code in ('301','302','303','305','306','307','308','309','310','311','312','314','315') ";
        	 Map<String, String> mmp= new HashMap<String, String>();
        	 Map<String, String> mmp2= new HashMap<String, String>();
			SSRS ssr=tExeSQL.execSQL(llsecufeeitems);
			if(ssr.getMaxRow()>0){
				for (int i = 1; i <= ssr.getMaxRow(); i++) {
					String code=ssr.GetText(i, 1);
					String CodeName=ssr.GetText(i, 2);
					String CodeAlias=ssr.GetText(i, 3);
					mmp.put(code, CodeName);
					mmp2.put(code, CodeAlias);
					
				}
			}
        // 2864 天津静海结算 由于特殊情况暂时这样控制入口
        if (mLLSecurityReceiptSchema.getApplyAmnt() > 0 ||(mLLCaseSchema.getAccdentDesc().equals("静海项目") 
        		&& mLLSecurityReceiptSchema.getApplyAmnt() < 0) || (mLLCaseSchema.getAccdentDesc().equals("天津大病") 
                		&& mLLSecurityReceiptSchema.getApplyAmnt() < 0)){ //2864  3853--费用总额
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getApplyAmnt());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("301");
            String v=mmp.get("301");
            String v2=mmp2.get("301");
            
//            LDCodeDB tLDCodeDB = new LDCodeDB();
//            tLDCodeDB.setCodeType("llsecufeeitem");
//            tLDCodeDB.setCode("301");
//            if (tLDCodeDB.getInfo()) {
            if(!"".equals(v) && v!=null){
                tLLFeeOtherItemSchema.setDrugName(v);
                tLLFeeOtherItemSchema.setAvliReason(v2);
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }

        }

        //统筹基金支付
        // # 2864 天津静海结算 由于特殊情况暂时这样控制入口
        if (mLLSecurityReceiptSchema.getPlanFee() > 0 || (mLLCaseSchema.getAccdentDesc().equals("静海项目") 
        		&& mLLSecurityReceiptSchema.getPlanFee() <0) || (mLLCaseSchema.getAccdentDesc().equals("天津大病") 
                		&& mLLSecurityReceiptSchema.getPlanFee() < 0)) { //2864 3853---统筹支付
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getPlanFee());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("302");

            String v=mmp.get("302");
            String v2=mmp2.get("302");
//            LDCodeDB tLDCodeDB = new LDCodeDB();
//            tLDCodeDB.setCodeType("llsecufeeitem");
//            tLDCodeDB.setCode("302");
//
//            if (tLDCodeDB.getInfo()) {
            if(!"".equals(v) && v!=null){
                tLLFeeOtherItemSchema.setDrugName(v);
                tLLFeeOtherItemSchema.setAvliReason(v2);
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }
        
        //大额医疗支付
        // # 2864 天津静海结算 由于特殊情况暂时这样控制入口
        if (mLLSecurityReceiptSchema.getSupInHosFee() > 0 || (mLLCaseSchema.getAccdentDesc().equals("静海项目")
        		&& mLLSecurityReceiptSchema.getSupInHosFee() < 0)) { //2864
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.getSupInHosFee());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("303");

            String v=mmp.get("303");
            String v2=mmp2.get("303");
//            LDCodeDB tLDCodeDB = new LDCodeDB();
//            tLDCodeDB.setCodeType("llsecufeeitem");
//            tLDCodeDB.setCode("303");
//
//            if (tLDCodeDB.getInfo()) {
            if(!"".equals(v) && v!=null){
                tLLFeeOtherItemSchema.setDrugName(v);
                tLLFeeOtherItemSchema.setAvliReason(v2);
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        //全自费
        // # 2864 天津静海结算 由于特殊情况暂时这样控制入口
        if (mLLSecurityReceiptSchema.getSelfAmnt() > 0 || (mLLCaseSchema.getAccdentDesc().equals("静海项目")
        		&& mLLSecurityReceiptSchema.getSelfAmnt() < 0) || (mLLCaseSchema.getAccdentDesc().equals("天津大病") 
                		&& mLLSecurityReceiptSchema.getSelfAmnt() < 0)) { //2864  3853---全自费
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getSelfAmnt());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("305");

            String v=mmp.get("305");
            String v2=mmp2.get("305");
//            LDCodeDB tLDCodeDB = new LDCodeDB();
//            tLDCodeDB.setCodeType("llsecufeeitem");
//            tLDCodeDB.setCode("305");
//
//            if (tLDCodeDB.getInfo()) {
            if(!"".equals(v) && v!=null){
                tLLFeeOtherItemSchema.setDrugName(v);
                tLLFeeOtherItemSchema.setAvliReason(v2);
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        //自付二
        // # 2864 天津静海结算 由于特殊情况暂时这样控制入口
        if (mLLSecurityReceiptSchema.getSelfPay2() > 0 || (mLLCaseSchema.getAccdentDesc().equals("静海项目")
        		&& mLLSecurityReceiptSchema.getSelfPay2() < 0) || (mLLCaseSchema.getAccdentDesc().equals("天津大病") 
                		&& mLLSecurityReceiptSchema.getSelfPay2() < 0)) { //2864  3853--部分支付
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getSelfPay2());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("306");

            String v=mmp.get("306");
            String v2=mmp2.get("306");
            
//            LDCodeDB tLDCodeDB = new LDCodeDB();
//            tLDCodeDB.setCodeType("llsecufeeitem");
//            tLDCodeDB.setCode("306");
//
//            if (tLDCodeDB.getInfo()) {
            if(!"".equals(v) && v!=null){
                tLLFeeOtherItemSchema.setDrugName(v);
                tLLFeeOtherItemSchema.setAvliReason(v2);
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        //自付一
        // # 2864 天津静海结算 由于特殊情况暂时这样控制入口
        if (mLLSecurityReceiptSchema.getSelfPay1() > 0 ||(mLLCaseSchema.getAccdentDesc().equals("静海项目")
        		&& mLLSecurityReceiptSchema.getSelfPay1() < 0)) {  //2864
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getSelfPay1());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("307");

            String v=mmp.get("307");
            String v2=mmp2.get("307");
//            LDCodeDB tLDCodeDB = new LDCodeDB();
//            tLDCodeDB.setCodeType("llsecufeeitem");
//            tLDCodeDB.setCode("307");
//
//            if (tLDCodeDB.getInfo()) {
            if(!"".equals(v) && v!=null){
                tLLFeeOtherItemSchema.setDrugName(v);
                tLLFeeOtherItemSchema.setAvliReason(v2);
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        //起付线
        // # 2864 天津静海结算 由于特殊情况暂时这样控制入口
        if (mLLSecurityReceiptSchema.getGetLimit() > 0 || (mLLCaseSchema.getAccdentDesc().equals("静海项目")
        		&& mLLSecurityReceiptSchema.getGetLimit() < 0) || (mLLCaseSchema.getAccdentDesc().equals("天津大病") 
                		&& mLLSecurityReceiptSchema.getGetLimit() < 0)) {  //2864 3853--起付线
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getGetLimit());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("308");

            String v=mmp.get("308");
            String v2=mmp2.get("308");
//            LDCodeDB tLDCodeDB = new LDCodeDB();
//            tLDCodeDB.setCodeType("llsecufeeitem");
//            tLDCodeDB.setCode("308");
//
//            if (tLDCodeDB.getInfo()) {
            if(!"".equals(v) && v!=null){
                tLLFeeOtherItemSchema.setDrugName(v);
                tLLFeeOtherItemSchema.setAvliReason(v2);
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        //中段
        // # 2864 天津静海结算 由于特殊情况暂时这样控制入口
        if (mLLSecurityReceiptSchema.getMidAmnt() > 0 || (mLLCaseSchema.getAccdentDesc().equals("静海项目")
        		&& mLLSecurityReceiptSchema.getMidAmnt() < 0) || (mLLCaseSchema.getAccdentDesc().equals("天津大病") 
                		&& mLLSecurityReceiptSchema.getMidAmnt() < 0)) {  //2864 3853--统筹自付
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getMidAmnt());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("309");

            String v=mmp.get("309");
            String v2=mmp2.get("309");
//            LDCodeDB tLDCodeDB = new LDCodeDB();
//            tLDCodeDB.setCodeType("llsecufeeitem");
//            tLDCodeDB.setCode("309");

//            if (tLDCodeDB.getInfo()) {
            if(!"".equals(v) && v!=null){
                tLLFeeOtherItemSchema.setDrugName(v);
                tLLFeeOtherItemSchema.setAvliReason(v2);
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        //高段1
        // # 2864 天津静海结算 由于特殊情况暂时这样控制入口
        if (mLLSecurityReceiptSchema.getHighAmnt1() > 0 || (mLLCaseSchema.getAccdentDesc().equals("静海项目")
        		&& mLLSecurityReceiptSchema.getHighAmnt1() <0 )) {  //2864
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getHighAmnt1());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("310");

            String v=mmp.get("310");
            String v2=mmp2.get("310");
//            LDCodeDB tLDCodeDB = new LDCodeDB();
//            tLDCodeDB.setCodeType("llsecufeeitem");
//            tLDCodeDB.setCode("310");
//
//            if (tLDCodeDB.getInfo()) {
            if(!"".equals(v) && v!=null){
                tLLFeeOtherItemSchema.setDrugName(v);
                tLLFeeOtherItemSchema.setAvliReason(v2);
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }
        
        //高段2
        if (mLLSecurityReceiptSchema.getHighAmnt2() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getHighAmnt2());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("311");

            String v=mmp.get("311");
            String v2=mmp2.get("311");
//            LDCodeDB tLDCodeDB = new LDCodeDB();
//            tLDCodeDB.setCodeType("llsecufeeitem");
//            tLDCodeDB.setCode("311");
//
//            if (tLDCodeDB.getInfo()) {
            if(!"".equals(v) && v!=null){
                tLLFeeOtherItemSchema.setDrugName(v);
                tLLFeeOtherItemSchema.setAvliReason(v2);
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }
        
        //超高
        if (mLLSecurityReceiptSchema.getSuperAmnt() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.getSuperAmnt());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("312");

            String v=mmp.get("312");
            String v2=mmp2.get("312");
//            LDCodeDB tLDCodeDB = new LDCodeDB();
//            tLDCodeDB.setCodeType("llsecufeeitem");
//            tLDCodeDB.setCode("312");
//
//            if (tLDCodeDB.getInfo()) {
            if(!"".equals(v) && v!=null){
                tLLFeeOtherItemSchema.setDrugName(v);
                tLLFeeOtherItemSchema.setAvliReason(v2);
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }
        
        //小额门急诊
        if (mLLSecurityReceiptSchema.getSmallDoorPay() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.getSmallDoorPay());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("314");

            String v=mmp.get("314");
            String v2=mmp2.get("314");
//            LDCodeDB tLDCodeDB = new LDCodeDB();
//            tLDCodeDB.setCodeType("llsecufeeitem");
//            tLDCodeDB.setCode("314");
//
//            if (tLDCodeDB.getInfo()) {
            if(!"".equals(v) && v!=null){
                tLLFeeOtherItemSchema.setDrugName(v);
                tLLFeeOtherItemSchema.setAvliReason(v2);
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }
        
        //大额门急诊
        if (mLLSecurityReceiptSchema.getHighDoorAmnt() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.getHighDoorAmnt());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("315");

            String v=mmp.get("315");
            String v2=mmp2.get("315");
//            LDCodeDB tLDCodeDB = new LDCodeDB();
//            tLDCodeDB.setCodeType("llsecufeeitem");
//            tLDCodeDB.setCode("315");
//
//            if (tLDCodeDB.getInfo()) {
            if(!"".equals(v) && v!=null){
                tLLFeeOtherItemSchema.setDrugName(v);
                tLLFeeOtherItemSchema.setAvliReason(v2);
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }
        
        
        //大额医疗支付
        // # 3454青岛负金额
        String mange=mGlobalInput.ManageCom.length()>=4?mGlobalInput.ManageCom.substring(0, 4):mGlobalInput.ManageCom;//当前机构是否为8694
        if ("8694".equals(mange) 
        		&&"4".equals(mLLFeeMainSchema.getFeeAtti())
        		&& "94000".equals(mLLFeeMainSchema.getReceiptNo())
        		&& mLLSecurityReceiptSchema.getSupInHosFee() < 0) { //
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLLSecurityReceiptSchema.getMngCom());
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.getSupInHosFee());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("303");

            String v=mmp.get("303");
            String v2=mmp2.get("303");
//            LDCodeDB tLDCodeDB = new LDCodeDB();
//            tLDCodeDB.setCodeType("llsecufeeitem");
//            tLDCodeDB.setCode("303");
//
//            if (tLDCodeDB.getInfo()) {
            if(!"".equals(v) && v!=null){
                tLLFeeOtherItemSchema.setDrugName(v);
                tLLFeeOtherItemSchema.setAvliReason(v2);
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        return true;
    }

    /**
     * 查询功能
     * @return boolean
     */
    private boolean submitquery() {
        return false;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLLRegisterSchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLLCaseSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "genCaseInfoBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
