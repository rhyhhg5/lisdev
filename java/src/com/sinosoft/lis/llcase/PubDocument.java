/**
 * <p>Title: 生成归档文件夹 </p>
 * <p>Description: 对理赔上传的文件进行归档处理</p>
 * <p>Copyright: Copyright (c) 2007 </p>
 * <p>Company: Sinosoft </p>
 * @author Houyd
 * @version 1.0
 */

package com.sinosoft.lis.llcase;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class PubDocument  {
	
	private PubDocument()
    {
    }
	
	/**
	 * 通过传入的格式要求，输出对应的日期字符串
	 * @param pDateFormat
	 * @return
	 */
	private static String getCurrentDate(String pDateFormat) {
		return new SimpleDateFormat(pDateFormat).format(new Date());
	}
	
	/**
	 * 通过根目录，获得文件实际归档的位置
	 * @param cRootPath
	 * @param cType
	 * @return
	 */
	public static String getSavePath(String cRootPath)
    {
        StringBuffer tFilePath = new StringBuffer();

        tFilePath.append(cRootPath);
        tFilePath.append(getCurrentDate("yyyy"));
        tFilePath.append('/');
        tFilePath.append(getCurrentDate("yyyyMM"));
        tFilePath.append('/');
        tFilePath.append(getCurrentDate("yyyyMMdd"));
        tFilePath.append('/');

        return tFilePath.toString();
    }

	/**
	 * 创建文件夹，根据传入的实际路径
	 * @param cRootPath
	 * @return
	 */
    public static boolean save(String cRootPath)
    {
        StringBuffer mFilePath = new StringBuffer();

        mFilePath.append(getSavePath(cRootPath));

        File mFileDir = new File(mFilePath.toString());
        if (!mFileDir.exists())
        {
            if (!mFileDir.mkdirs())
            {
                System.err.println("创建目录[" + mFilePath.toString() + "]失败！" + mFileDir.getPath());
                return false;
            }
        }

        return true;
    }
    
    public static void main(String[] args) {
		PubDocument.save("E:/");
	}
}
