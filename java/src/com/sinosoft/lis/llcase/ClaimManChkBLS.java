package com.sinosoft.lis.llcase;

import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.sql.*;


/**
 * <p>Title: Web业务系统核赔功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 */

public class ClaimManChkBLS
{
	//传输数据类
	private VData mInputData ;
	//错误处理类，每个需要错误处理的类中都放置该类
	public  CErrors mErrors = new CErrors();

	private LLClaimUnderwriteSchema
                mLLClaimUnderwriteSchema = new LLClaimUnderwriteSchema();
	private LLClaimUWDetailSchema mLLClaimUWDetailSchema = new LLClaimUWDetailSchema();

	public ClaimManChkBLS()
        {

        }

	public static void main(String[] args)
	{

	}

	//传输数据的公共方法
	public boolean submitData(VData cInputData,String cOperate)
	{
	    //首先将数据在本类中做一个备份
	    mInputData=(VData)cInputData.clone() ;
            if (!this.saveData()) {
                return false;
            }
            mInputData = null;
            return true;
        }

	private boolean saveData()
	{
            System.out.println("savedata begin");
            LLClaimUnderwriteSet tLLClaimUnderwriteSet =
                    (LLClaimUnderwriteSet) mInputData.
                    getObjectByObjectName("LLClaimUnderwriteSet", 0);
            LLClaimUWDetailSet tLLClaimUWDetailSet =
                    (LLClaimUWDetailSet) mInputData.
                    getObjectByObjectName("LLClaimUWDetailSet", 0);

            mLLClaimUnderwriteSchema.setSchema(tLLClaimUnderwriteSet.get(1));
            mLLClaimUWDetailSchema = tLLClaimUWDetailSet.get(1);

            Connection conn = DBConnPool.getConnection();

            if (conn == null)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ClaimManChkBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据库连接失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            try
            {
                conn.setAutoCommit(false);

                // 删除部分
                LLClaimUnderwriteDB tLLClaimUnderwriteDB =
                        new LLClaimUnderwriteDB(conn);
                tLLClaimUnderwriteDB.setClmNo(mLLClaimUnderwriteSchema.getClmNo());
                tLLClaimUnderwriteDB.setPolNo(mLLClaimUnderwriteSchema.getPolNo());
                if (tLLClaimUnderwriteDB.deleteSQL() == false)
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLLClaimUnderwriteDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ClaimManChkBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "LLClaimUnderwrite表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }

                tLLClaimUnderwriteDB.setSchema(mLLClaimUnderwriteSchema);
                if (tLLClaimUnderwriteDB.insert() == false)
                {
                    // @@错误处理
                    System.out.println("bbbbb");
                    this.mErrors.copyAllErrors(tLLClaimUnderwriteDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ClaimManChkBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "LLClaimUnderwrite表保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }

                //核赔履历
                LLClaimUWDetailDB tLLClaimUWDetailDB = new LLClaimUWDetailDB(conn);
                tLLClaimUWDetailDB.setSchema(mLLClaimUWDetailSchema);
                if (tLLClaimUWDetailDB.insert() == false)
                {
                    // @@错误处理

                    this.mErrors.copyAllErrors(tLLClaimUWDetailDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ClaimManChkBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "LLClaimUWDetailDB表保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }

                conn.commit();
                conn.close();
            } // end of try
            catch (Exception ex)
            {
                // @@错误处理
                ex.printStackTrace();
                CError tError = new CError();
                tError.moduleName = "ClaimManBLS";
                tError.functionName = "saveData";
                tError.errorMessage = ex.toString();
                this.mErrors.addOneError(tError);
                try
                {
                    conn.rollback();
                }
                catch (Exception e)
                {}
                return false;
            }
            return true;
        }

}
