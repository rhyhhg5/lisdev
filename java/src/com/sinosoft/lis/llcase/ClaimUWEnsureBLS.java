package com.sinosoft.lis.llcase;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统投保暂交费功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author YT
 * @version 1.0
 */

public class ClaimUWEnsureBLS
{
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();
  private String mOperate;

  public ClaimUWEnsureBLS() {}


  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;
    this.mOperate = cOperate;
    System.out.print("save claimsavebls data begin");
      if (!this.saveData())
      return false;
    mInputData=null;
    return true;
  }


  private boolean saveData()
  {
      LLClaimUWMainSchema tLLClaimUWMainSchema = new
              LLClaimUWMainSchema();
      LLClaimUWMDetailSchema tLLClaimUWMDetailSchema = new
              LLClaimUWMDetailSchema();
      LLClaimSchema tLLClaimSchema = new LLClaimSchema();

      tLLClaimUWMainSchema =
              (LLClaimUWMainSchema) mInputData.
              getObjectByObjectName("LLClaimUWMainSchema", 0);
      tLLClaimUWMDetailSchema =
              (LLClaimUWMDetailSchema) mInputData.
              getObjectByObjectName("LLClaimUWMDetailSchema", 0);
      tLLClaimSchema =
              (LLClaimSchema) mInputData.
              getObjectByObjectName("LLClaimSchema", 0);

    	Connection conn = null;
    	conn = DBConnPool.getConnection();
    	if (conn == null)
    	{
      		// @@错误处理
      		CError tError = new CError();
      		tError.moduleName = "ClaimSaveBLS";
      		tError.functionName = "saveData";
      		tError.errorMessage = "数据库连接失败!";
      		this.mErrors .addOneError(tError) ;
      		return false;
    	}
        try
        {
            conn.setAutoCommit(false);
            LLClaimUWMainDB tLLClaimUWMainDB = new LLClaimUWMainDB(conn);
            tLLClaimUWMainDB.setClmNo(tLLClaimUWMainSchema.getClmNo());
            if (tLLClaimUWMainDB.deleteSQL() == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLLClaimUWMainDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ClaimUWEnsureBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "LLClaimUWMain表删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("delete");
            // 案件核赔表
            tLLClaimUWMainDB.setSchema(tLLClaimUWMainSchema);
            if (tLLClaimUWMainDB.insert() == false)
            {
                // @@错误处理
                System.out.println("bbbbb");
                this.mErrors.copyAllErrors(tLLClaimUWMainDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ClaimUWEnsureBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "LLClaimUWMain表保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("insert end");
            //案件核赔履历表
            LLClaimUWMDetailDB tLLClaimUWMDetailDB = new
                    LLClaimUWMDetailDB(conn);
            tLLClaimUWMDetailDB.setSchema(tLLClaimUWMDetailSchema);
            if (tLLClaimUWMDetailDB.insert() == false)
            {
                // @@错误处理

                this.mErrors.copyAllErrors(tLLClaimUWMDetailDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ClaimManChkBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "LLClaimUWMDetail表保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            //修改赔案表中的给付类型
            LLClaimDB tLLClaimDB = new LLClaimDB(conn);
            tLLClaimDB.setSchema(tLLClaimSchema);
            if (!tLLClaimDB.update())
            {
                this.mErrors.copyAllErrors(tLLClaimDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ClaimManChkBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "LLClaim表保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            conn.commit();
            conn.close();
        } // end of try
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ClaimSaveBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}

            return false;
        }
    return true;
  }

}
