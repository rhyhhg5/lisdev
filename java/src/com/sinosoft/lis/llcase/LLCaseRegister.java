/**
 * 
 */
package com.sinosoft.lis.llcase;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.tb.*;

import java.sql.Time;
import java.util.TreeSet;

/**
 * 案件受理
 * 
 * @author maning
 * @version 1.0
 */
public class LLCaseRegister extends LLCaseProcess {

	/**
	 * 
	 */
	public LLCaseRegister() {
	}

	// 理赔信息缓存
	private CachedLPInfo mCachedLPInfo = CachedLPInfo.getInstance();

	// 业务逻辑错误
	public CErrors mErrors = new CErrors();

	// 获取的业务数据
	private VData mInputData = new VData();

	// 返回处理后数据
	private VData mResult = new VData();

	// 返回的提交数据库MMap
	private MMap mMMap = new MMap();

	// 用户登录信息
	private GlobalInput mGlobalInput = new GlobalInput();

	// 操作类型
	private String mOperate = "";

	// 案件信息
	private LLCaseSchema mLLCaseSchema = null;

	// 申请信息
	private LLRegisterSchema mLLRegisterSchema = null;

	// 事件信息
	private LLSubReportSet mLLSubReportSet = null;

	// 案件时间关联信息
	private LLCaseRelaSet mLLCaseRelaSet = null;

	// 申请原因
	private LLAppClaimReasonSet mLLAppClaimReasonSet = null;

	// 客户信息
	private LDPersonSchema mLDPersonSchema = null;

	// 当前日期
	private String mCurrentDate = PubFun.getCurrentDate();

	// 当前事件
	private String mCurrentTime = PubFun.getCurrentTime();

	private LCInsuredSet mLCInsuredSet = new LCInsuredSet();

	private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();

	private LCPolSet mLCPolSet = null;

	/**
	 * 获取数据VData，经过业务处理，返回包含业务数据的VData。不进行业务逻辑的校验，由外层类处理，默认所有数据都正确
	 * 
	 * @param aInputData:
	 *            <p>
	 *            GlobalInput（&Operator-操作人 &Managecom-登录机构）
	 *            </p>
	 *            <p>
	 *            LLCaseSchema(&CustomerNo-被保险人客户号 &CustomerName-被保险人姓名
	 *            &CustBirthday 被保险人出生日期
	 *            &Handler-处理人,如果处理人已有不进行分配,根据CaseProp进行控制，06分配给机构理赔人，07、09、10分配给本人，其他正常分配
	 *            &CaseProp-案件分配方式 06-机构处理 &)
	 *            </p>
	 *            <p>
	 *            LLRegisterSchema
	 *            </p>
	 *            <p>
	 *            LLSubReportSet &AccDate发生日期
	 *            </p>
	 *            <p>
	 *            LLAppClaimReasonSet 可以没有申请原因，如果有，必须包含&ReasonCode
	 *            </p>
	 * @param aOperate
	 * @return boolean 处理成功标志
	 * @throws Exception
	 *             所有的异常都由外层调用类处理
	 */
	public boolean submitData(VData aInputData, String aOperate)
			throws Exception {
		System.out.println("开始案件受理处理");
		mInputData = aInputData;
		mOperate = aOperate;

		if (!getInputData()) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		 //提交数据库
        PubSubmit tPubSubmit = new PubSubmit();
        mInputData.clear();       
        mInputData.add(getMMap());
        if (!tPubSubmit.submitData(mInputData, ""))
        {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LLCaseRegister";
                tError.functionName = "dealData";
                tError.errorMessage = "理赔受理申请数据保存失败!";
                this.mErrors.addOneError(tError);
                return false;
        }
		System.out.println("案件受理处理结束");

		return true;
	}

	/**
	 * 解析获取的对象
	 * 
	 * @return 获取数据成功标志
	 */
	private boolean getInputData() throws Exception {
		mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0);

		if (mGlobalInput == null) {
			mErrors.addOneError("用户登录信息获取失败");
			return false;
		}

		mLLCaseSchema = (LLCaseSchema) mInputData.getObjectByObjectName(
				"LLCaseSchema", 0);

		if (mLLCaseSchema == null) {
			mErrors.addOneError("案件信息获取失败");
			return false;
		}

		mLLRegisterSchema = (LLRegisterSchema) mInputData
				.getObjectByObjectName("LLRegisterSchema", 0);

		if (mLLRegisterSchema == null) {
			mErrors.addOneError("申请信息获取失败");
			return false;
		}

		mLLSubReportSet = (LLSubReportSet) mInputData.getObjectByObjectName(
				"LLSubReportSet", 0);

		if (mLLSubReportSet == null) {
			mErrors.addOneError("事件信息获取失败");
			return false;
		}

		mLLAppClaimReasonSet = (LLAppClaimReasonSet) mInputData
				.getObjectByObjectName("LLAppClaimReasonSet", 0);

		return true;
	}

	/**
	 * 基本校验
	 * 
	 * @return 成功标志
	 * @throws Exception
	 */
	private boolean checkData() throws Exception {

		if (mGlobalInput.Operator == null || "".equals(mGlobalInput.Operator)) {
			mErrors.addOneError("操作用户登录信息获取失败");
			return false;
		}

		if (mGlobalInput.ManageCom == null || "".equals(mGlobalInput.ManageCom)) {
			mErrors.addOneError("操作用户登录信息获取失败");
			return false;
		}

		if (mLLCaseSchema.getCustomerNo() == null
				|| "".equals(mLLCaseSchema.getCustomerNo())) {
			mErrors.addOneError("被保险人客户号获取失败");
			return false;
		}

		if (mLLCaseSchema.getCaseGetMode() == null
				|| "".equals(mLLCaseSchema.getCaseGetMode())) {
			mErrors.addOneError("领取方式获取失败");
			return false;
		}

		LDCodeSchema tLDCodeSchema = mCachedLPInfo
				.findLLGetModeByCode(mLLCaseSchema.getCaseGetMode());

		if (tLDCodeSchema == null) {
			mErrors.addOneError("领取方式编码错误");
			return false;
		}

		if (mLLRegisterSchema.getRelation() == null
				|| "".equals(mLLRegisterSchema.getRelation())) {
			mErrors.addOneError("申请人与被保险人关系获取失败");
			return false;
		}

		tLDCodeSchema = mCachedLPInfo.findLLRelationByCode(mLLRegisterSchema
				.getRelation());
		if (tLDCodeSchema == null) {
			mErrors.addOneError("申请人与被保险人关系代码错误");
			return false;
		}

		// 如果不是本人申请，需有申请人信息
		if (!"00".equals(mLLRegisterSchema.getRelation())) {
			if (mLLRegisterSchema.getRgtantName() == null
					|| "".equals(mLLRegisterSchema.getRgtantName())) {
				mErrors.addOneError("申请人姓名不能为空");
				return false;
			}
		}

		if (mLLRegisterSchema.getRgtType() == null
				|| "".equals(mLLRegisterSchema.getRgtType())) {
			mErrors.addOneError("受理方式不能为空");
			return false;
		}

		tLDCodeSchema = mCachedLPInfo.findLLAskModeByCode(mLLRegisterSchema
				.getRgtType());
		if (tLDCodeSchema == null) {
			mErrors.addOneError("受理方式代码错误");
			return false;
		}

		if (mLLSubReportSet.size() <= 0) {
			mErrors.addOneError("事件信息获取失败");
			return false;
		}

		return true;
	}

	/**
	 * 业务处理方法
	 * 
	 * @return 处理成功标志
	 * @throws Exception
	 */
	private boolean dealData() throws Exception {
		// 处理LLCase表
		if (!dealLLCase()) {
			return false;
		}

		// 处理LLRegister表
		if (!dealLLRegister()) {
			return false;
		}

		// 处理LLSubReport表
		if (!dealLLSubReport()) {
			return false;
		}

		// 处理LLAppClaimReason表
		if (!dealLLAppClaimReason()) {
			return false;
		}

		return true;
	}

	/**
	 * 处理LLCase表数据
	 * 
	 * @return 处理成功标志
	 * @throws Exception
	 */
	private boolean dealLLCase() throws Exception {
		System.out.println("dealLLCase......");
		// 存储获取的数据
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		tLLCaseSchema.setSchema(mLLCaseSchema);
		mLLCaseSchema = new LLCaseSchema();

		/**
		 * 客户信息部分
		 */
		// 被保险人客户号
		mLLCaseSchema.setCustomerNo(tLLCaseSchema.getCustomerNo());

		LCPolBL tLCPolBL = new LCPolBL();
		tLCPolBL.setInsuredNo(tLLCaseSchema.getCustomerNo());
		tLCPolBL.setAppFlag("1");
		String tStateflagSQL = " stateflag<>'4' ";
		mLCPolSet = tLCPolBL.executeQuery(tStateflagSQL);

		/*TreeSet a = new TreeSet();
		a.add(mLLCaseSchema);*/
		
		LLCaseSet a = new LLCaseSet();
		a.add(mLLCaseSchema);

		LCInsuredDB tLCInsuredDB = new LCInsuredDB();
		tLCInsuredDB.setInsuredNo(tLLCaseSchema.getCustomerNo());
		mLCInsuredSet = tLCInsuredDB.query();

		if (mLCInsuredSet.size() <= 0) {
			LBInsuredDB tLBInsuredDB = new LBInsuredDB();
			tLBInsuredDB.setInsuredNo(tLLCaseSchema.getCustomerNo());
			LBInsuredSet tLBInsuredSet = tLBInsuredDB.query();

			if (tLBInsuredSet.size() <= 0) {
				mErrors.addOneError("被保险人查询失败");
				return false;
			}
			Reflections tReflections = new Reflections();
			tReflections.transFields(mLCInsuredSet, tLBInsuredSet);
		}

		mLCInsuredSchema = mLCInsuredSet.get(1);
		for (int i = 2; i <= mLCInsuredSet.size(); i++) {
			LCInsuredSchema tLCInsuredSchema = mLCInsuredSet.get(i);
			if (PubFun.calInterval(tLCInsuredSchema.getModifyDate(),
					mLCInsuredSchema.getMakeDate(), "D") > 0) {
				mLCInsuredSchema = tLCInsuredSchema;
			} else if (PubFun.calInterval(tLCInsuredSchema.getModifyDate(),
					mLCInsuredSchema.getMakeDate(), "D") == 0) {
				Time tMModifyTime = Time.valueOf(mLCInsuredSchema
						.getModifyTime());
				Time tTModifyTime = Time.valueOf(tLCInsuredSchema
						.getModifyTime());
				if (tTModifyTime.after(tMModifyTime)) {
					mLCInsuredSchema = tLCInsuredSchema;
				}
			}
		}

		// 被保险人姓名
		if (tLLCaseSchema.getCustomerName() == null
				|| "".equals(tLLCaseSchema.getCustomerName())) {
			mLLCaseSchema.setCustomerName(mLCInsuredSchema.getName());
		} else {
			if (!tLLCaseSchema.getCustomerName().equals(
					mLCInsuredSchema.getName())) {
				mErrors.addOneError("提供的被保险人姓名与系统中不符");
				return false;
			}
			mLLCaseSchema.setCustomerName(tLLCaseSchema.getCustomerName());
		}

		// 用户当前状态
		String tCustState = tLLCaseSchema.getCustState();
		if (tCustState != null && !"".equals(tCustState)) {
			LDCodeSchema tLDCodeSchema = mCachedLPInfo
					.findLLCustStateByCode(tCustState);
			if (tLDCodeSchema == null) {
				mErrors.addOneError("被保险人现状代码错误");
				return false;
			}
		}
		mLLCaseSchema.setCustState(tCustState);

		// 被保险人出生日期
		if (tLLCaseSchema.getCustBirthday() == null
				|| "".equals(tLLCaseSchema.getCustBirthday())) {
			mLLCaseSchema.setCustBirthday(mLCInsuredSchema.getBirthday());
		} else {
			if (!PubFun.checkDateForm(tLLCaseSchema.getCustBirthday())) {
				mErrors.addOneError("出生日期代码错误");
				return false;
			}

			if (!tLLCaseSchema.getCustBirthday().equals(
					mLCInsuredSchema.getBirthday())) {
				mErrors.addOneError("提供的出生日期与系统中不符");
				return false;
			}

			mLLCaseSchema.setCustBirthday(tLLCaseSchema.getCustBirthday());
		}

		// 被保险人性别
		if (tLLCaseSchema.getCustomerSex() == null
				|| "".equals(tLLCaseSchema.getCustomerSex())) {
			mLLCaseSchema.setCustomerSex(mLCInsuredSchema.getSex());
		} else {
			if (!tLLCaseSchema.getCustomerSex().equals(
					mLCInsuredSchema.getSex())) {
				mErrors.addOneError("提供的性别与系统中不符");
				return false;
			}
			mLLCaseSchema.setCustomerSex(tLLCaseSchema.getCustomerSex());
		}

		// 被保险人当前年龄
		int tAge = LLCaseCommon.calAge(tLLCaseSchema.getCustBirthday());
		mLLCaseSchema.setCustomerAge(tAge);

		// 被保险人证件类型 被保险人证件号码
		if (tLLCaseSchema.getIDType() == null
				|| "".equals(tLLCaseSchema.getIDType())) {
			mLLCaseSchema.setIDType(mLCInsuredSchema.getIDType());
			mLLCaseSchema.setIDNo(mLCInsuredSchema.getIDNo());
		} else {
			LDCodeSchema tLDCodeSchema = mCachedLPInfo
					.findIDTypeByCode(tLLCaseSchema.getIDType());
			if (tLDCodeSchema == null) {
				mErrors.addOneError("证件类型编码错误");
				return false;
			}
			mLLCaseSchema.setIDType(tLLCaseSchema.getIDType());

			if (tLLCaseSchema.getIDType().equals(mLCInsuredSchema.getIDType())
					&& !tLLCaseSchema.getIDNo().equals(
							mLCInsuredSchema.getIDNo())) {
				mErrors.addOneError("提供的证件号码与系统中对应证件类型的证件号码不符");
				return false;
			}

			mLLCaseSchema.setIDNo(tLLCaseSchema.getIDNo());
		}

		// 被保险人其他证件类型 被保险人其他证件号码 可以为空
		if (tLLCaseSchema.getOtherIDType() == null
				|| "".equals(tLLCaseSchema.getOtherIDType())) {
			mLLCaseSchema.setOtherIDType(mLCInsuredSchema.getOthIDType());
			mLLCaseSchema.setOtherIDNo(mLCInsuredSchema.getOthIDNo());
		} else {
			LDCodeSchema tLDCodeSchema = mCachedLPInfo
					.findIDTypeByCode(tLLCaseSchema.getOtherIDType());
			if (tLDCodeSchema == null) {
				mErrors.addOneError("其它证件类型编码错误");
				return false;
			}
			mLLCaseSchema.setOtherIDType(tLLCaseSchema.getOtherIDType());
			mLLCaseSchema.setOtherIDNo(tLLCaseSchema.getOtherIDNo());
		}

		// 被保险人手机号
		mLLCaseSchema.setMobilePhone(tLLCaseSchema.getMobilePhone());

		// 出险日期
		String tAccDate = mLLSubReportSet.get(1).getAccDate();
		if (tAccDate == null || "".equals(tAccDate)
				|| !PubFun.checkDateForm(tAccDate)) {
			mErrors.addOneError("出险日期获取失败");
			return false;
		}

		for (int i = 2; i <= mLLSubReportSet.size(); i++) {
			LLSubReportSchema tLLSubReportSchema = mLLSubReportSet.get(i);
			if (tLLSubReportSchema.getAccDate() == null
					|| "".equals(tLLSubReportSchema.getAccDate())
					|| !PubFun.checkDateForm(tLLSubReportSchema.getAccDate())) {
				mErrors.addOneError("出险日期获取失败");
				return false;
			}

			if (PubFun.calInterval(tAccDate, tLLSubReportSchema.getAccDate(),
					"D") > 0) {
				tAccDate = tLLSubReportSchema.getAccDate();
			}
		}

		mLLCaseSchema.setAccidentDate(tAccDate);

		// 被保险人死亡日期
		mLLCaseSchema.setDeathDate(tLLCaseSchema.getDeathDate());
		if (mLLCaseSchema.getDeathDate() != null
				&& !"".equals(mLLCaseSchema.getDeathDate())) {
			if (!PubFun.checkDateForm(mLLCaseSchema.getDeathDate())) {
				mErrors.addOneError("死亡日期错误");
				return false;
			}
			if (!dealLDPerson()) {
				return false;
			}
		}

		/**
		 * 案件给付信息
		 */

		// 案件给付方式
		mLLCaseSchema.setCaseGetMode(tLLCaseSchema.getCaseGetMode());

		// 取最新的银行信息
		mLCInsuredSchema = mLCInsuredSet.get(1);
		for (int i = 1; i <= mLCInsuredSet.size(); i++) {
			LCInsuredSchema tLCInsuredSchema = mLCInsuredSet.get(i);
			if (tLCInsuredSchema.getBankCode() != null
					&& !"".equals(tLCInsuredSchema.getBankCode())
					&& tLLCaseSchema.getBankAccNo() != null
					&& !"".equals(tLLCaseSchema.getBankAccNo())
					&& tLLCaseSchema.getAccName() != null
					&& !"".equals(tLLCaseSchema.getAccName())) {
				if (PubFun.calInterval(tLCInsuredSchema.getModifyDate(),
						mLCInsuredSchema.getMakeDate(), "D") > 0) {
					mLCInsuredSchema = tLCInsuredSchema;
				} else if (PubFun.calInterval(tLCInsuredSchema.getModifyDate(),
						mLCInsuredSchema.getMakeDate(), "D") == 0) {
					Time tMModifyTime = Time.valueOf(mLCInsuredSchema
							.getModifyTime());
					Time tTModifyTime = Time.valueOf(tLCInsuredSchema
							.getModifyTime());
					if (tTModifyTime.after(tMModifyTime)) {
						mLCInsuredSchema = tLCInsuredSchema;
					}
				}
			}
		}

		// 如果传入的银行信息时空，且客户有投保时的银行信息，取投保时的，否则为传入的
		if ((tLLCaseSchema.getBankCode() == null || "".equals(tLLCaseSchema
				.getBankCode()))
				&& (tLLCaseSchema.getBankAccNo() == null || ""
						.equals(tLLCaseSchema.getBankAccNo()))
				&& (tLLCaseSchema.getAccName() == null || ""
						.equals(tLLCaseSchema.getAccName()))) {
			if (mLCInsuredSchema.getBankCode() != null
					&& !"".equals(mLCInsuredSchema.getBankCode())
					&& mLCInsuredSchema.getBankAccNo() != null
					&& !"".equals(mLCInsuredSchema.getBankAccNo())
					&& mLCInsuredSchema.getAccName() != null
					&& !"".equals(mLCInsuredSchema.getAccName())) {
				mLLCaseSchema.setBankCode(mLCInsuredSchema.getBankCode());
				mLLCaseSchema.setBankAccNo(mLCInsuredSchema.getBankAccNo());
				mLLCaseSchema.setAccName(mLCInsuredSchema.getAccName());
			}
		} else {
		   // LDBankDB tLDBankDB = new LDBankDB();
		   // tLDBankDB.setBankCode(tLLCaseSchema.getBankCode());
		   // tLDBankDB.setCanSendFlag("1");
		   // if(tLDBankDB.query().size()<= 0)
		   // {
		   // 	mErrors.addOneError("银行代码在系统中查询失败");
			 //	return false;
		   //  }
			 // 银行编码
			mLLCaseSchema.setBankCode(tLLCaseSchema.getBankCode());

			// 银行账户
			mLLCaseSchema.setBankAccNo(tLLCaseSchema.getBankAccNo());

			// 银行账户名
			mLLCaseSchema.setAccName(tLLCaseSchema.getAccName());
		}

		/**
		 * 案件信息部分
		 */

		// 案件类型1-申请类
		mLLCaseSchema.setRgtType("1");

		// 如果获取信息有账单标志，则直接获取，否则按默认规则获取
		if (tLLCaseSchema.getReceiptFlag() == null
				|| "".equals(tLLCaseSchema.getReceiptFlag())) {
			mLLCaseSchema.setReceiptFlag(dealReceiptFlag(mLLCaseSchema
					.getCustomerNo(), mLLCaseSchema.getAccidentDate()));

			if ("".equals(mLLCaseSchema.getReceiptFlag())) {
				return false;
			}

		} else {
			mLLCaseSchema.setReceiptFlag(tLLCaseSchema.getReceiptFlag());
		}

		// 调查状态 0-未调查
		mLLCaseSchema.setSurveyFlag("0");

		// 案件状态 13-延迟
		mLLCaseSchema.setRgtState("13");

		// 受理日期
		mLLCaseSchema.setRgtDate(mCurrentDate);

		// 案件管理机构，根据用户登录机构判断,补齐8位
		mLLCaseSchema.setMngCom(PubFun.RCh(mGlobalInput.ManageCom, "0", 8));

		// 案件受理类型 根据该字段分配处理人
		mLLCaseSchema.setCaseProp(tLLCaseSchema.getCaseProp());

		// 受理人 取登录人
		mLLCaseSchema.setRigister(mGlobalInput.Operator);

		LLCaseCommon tLLCaseCommon = new LLCaseCommon();
		// 案件处理人

		String tHandler = tLLCaseCommon.dealHandler(mGlobalInput, mLLCaseSchema
				.getCustomerNo(), mLLCaseSchema.getCaseProp());

		if (tHandler == null || "".equals(tHandler)) {
			mErrors.addOneError("案件处理人分配失败");
			return false;
		} else {
			mLLCaseSchema.setHandler(tHandler);
		}

		// 案件录入人 如果传入数据已经有录入人，直接获取，否则按规则分配
		String tInputer = tLLCaseCommon.dealInputer(mGlobalInput, mLLCaseSchema
				.getCaseProp());
		mLLCaseSchema.setClaimer(tInputer);

		// 生成案件号
		String tCaseNo = "";
		String tRgtNo = "";
		String tLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);

		// 如果是批次下案件，先调用批次下预存号，如果生成失败，调用常规生成规则 RgtClass 0-个案申请案件 1-批次下案件
		if (mLLRegisterSchema.getRgtNo() != null
				&& !"".equals(mLLRegisterSchema.getRgtNo())
				&& "1".equals(mLLRegisterSchema.getRgtClass())) {
			tRgtNo = mLLRegisterSchema.getRgtNo();
			VData tVData = new VData();
			tVData.add(0, "1");
			tVData.add(1, tRgtNo);
			tVData.add(2, "STORE");
			tCaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit, tVData);
			if (tCaseNo.length() < 17) {
				CError.buildErr(this, "预留号超限，不能在该批次下继续增加出险人！");
				tCaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit);
			}
		} else {
			tCaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit);
			tRgtNo = tCaseNo;
		}

		if ("".equals(tCaseNo)) {
			mErrors.addOneError("理赔案件号生成失败");
			return false;
		}

		if ("".equals(tRgtNo)) {
			mErrors.addOneError("理赔批次号生成失败");
			return false;
		}

		mLLCaseSchema.setCaseNo(tCaseNo);
		mLLCaseSchema.setRgtNo(tRgtNo);

		/**
		 * 数据操作信息
		 */

		// 操作人，取登录人
		mLLCaseSchema.setOperator(mGlobalInput.Operator);

		// 案件创建日期
		mLLCaseSchema.setMakeDate(mCurrentDate);

		// 案件创建时间
		mLLCaseSchema.setMakeTime(mCurrentTime);

		// 案件修改日期
		mLLCaseSchema.setModifyDate(mCurrentDate);

		// 案件修改时间
		mLLCaseSchema.setModifyTime(mCurrentTime);

		return true;
	}

	/**
	 * 处理llregister表
	 * 
	 * @return
	 * @throws Exception
	 */
	private boolean dealLLRegister() throws Exception {
		System.out.println("dealLLRegister......");
		// 存储获取的数据
		LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
		tLLRegisterSchema.setSchema(mLLRegisterSchema);
		mLLRegisterSchema = new LLRegisterSchema();

		/**
		 * 申请信息
		 */

		// 个案受理
		if (tLLRegisterSchema.getRgtNo() == null
				|| "".equals(tLLRegisterSchema.getRgtNo())) {

			// 申请类型 0-团单 1-个人
			mLLRegisterSchema.setRgtObj("1");

			// 申请被保险人客户号
			mLLRegisterSchema.setRgtObjNo(mLLCaseSchema.getCustomerNo());

			// 申请被保险人客户号
			mLLRegisterSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());

			// 受理方式
			mLLRegisterSchema.setRgtType(tLLRegisterSchema.getRgtType());
			
			//申请人数
       		mLLRegisterSchema.setAppPeoples(tLLRegisterSchema.getAppPeoples());
       		
       		//给付方式
       		mLLRegisterSchema.setTogetherFlag(tLLRegisterSchema.getTogetherFlag());

			// 申请人与被保险人关系
			mLLRegisterSchema.setRelation(tLLRegisterSchema.getRelation());

			// 如果是本人可以取被保险人信息
			if ("00".equals(tLLRegisterSchema.getRelation())) {

				// 申请人姓名
				if (tLLRegisterSchema.getRgtantName() == null
						|| "".equals(tLLRegisterSchema.getRgtantName())) {
					mLLRegisterSchema.setRgtantName(mLLCaseSchema
							.getCustomerName());
				} else {
					mLLRegisterSchema.setRgtantName(tLLRegisterSchema
							.getRgtantName());
				}

				// 申请人证件类型
				if (tLLRegisterSchema.getIDType() == null
						|| "".equals(tLLRegisterSchema.getIDType())) {
					mLLRegisterSchema.setIDType(mLLCaseSchema.getIDType());
					mLLRegisterSchema.setIDNo(mLLCaseSchema.getIDNo());
				} else {
					mLLRegisterSchema.setIDType(tLLRegisterSchema.getIDType());
					mLLRegisterSchema.setIDNo(tLLRegisterSchema.getIDNo());
				}

			} else {
				// 申请人姓名
				mLLRegisterSchema.setRgtantName(tLLRegisterSchema
						.getRgtantName());

				// 申请人证件类型
				mLLRegisterSchema.setIDType(tLLRegisterSchema.getIDType());

				// 申请人证件号码
				mLLRegisterSchema.setIDNo(tLLRegisterSchema.getIDNo());
			}

			// 申请人地址
			mLLRegisterSchema.setRgtantAddress(tLLRegisterSchema
					.getRgtantAddress());

			// 申请人电话
			mLLRegisterSchema.setRgtantPhone(tLLRegisterSchema.getPostCode());

			// 申请人手机
			mLLRegisterSchema.setRgtantMobile(tLLRegisterSchema
					.getRgtantMobile());

			// 申请人E-mail
			mLLRegisterSchema.setEmail(tLLRegisterSchema.getEmail());

			// 申请人邮编
			mLLRegisterSchema.setPostCode(tLLRegisterSchema.getPostCode());

			// 预付申请金额 个案目前没有
			mLLRegisterSchema.setAppAmnt(tLLRegisterSchema.getAppAmnt());

			// 领取方式
			mLLRegisterSchema.setGetMode(mLLCaseSchema.getGetMode());

			// 银行编码
			mLLRegisterSchema.setBankCode(mLLCaseSchema.getBankCode());

			// 银行账号
			mLLRegisterSchema.setBankAccNo(mLLCaseSchema.getBankAccNo());

			// 银行账户名
			mLLRegisterSchema.setAccName(mLLCaseSchema.getAccName());

			// 回执发送方式
			mLLRegisterSchema.setReturnMode(tLLRegisterSchema.getReturnMode());

			// 案件状态
			mLLRegisterSchema.setRgtState("13");

			// 申请类型 0-个案 1-批次
			mLLRegisterSchema.setRgtClass("0");

			/**
			 * 批次信息
			 */
			// 批次号
			mLLRegisterSchema.setRgtNo(mLLCaseSchema.getRgtNo());

			// 申请日期
			mLLRegisterSchema.setRgtDate(mLLCaseSchema.getRgtDate());

			// 处理人
			mLLRegisterSchema.setHandler(mLLCaseSchema.getHandler());

			// 管理机构
			mLLRegisterSchema.setMngCom(mLLCaseSchema.getMngCom());

			// 生成日期
			mLLRegisterSchema.setMakeDate(mCurrentDate);

			// 生成时间
			mLLRegisterSchema.setMakeTime(mCurrentTime);
		}

		// 操作人
		mLLRegisterSchema.setOperator(mGlobalInput.Operator);

		// 修改日期
		mLLRegisterSchema.setModifyDate(mCurrentDate);

		// 修改时间
		mLLRegisterSchema.setModifyTime(mCurrentTime);
		return true;
	}

	/**
	 * 处理LLSubReport表数据
	 * 
	 * @return
	 * @throws Exception
	 */
	private boolean dealLLSubReport() throws Exception {
		System.out.println("dealLLSubReport......");
		// 存储获取的数据
		LLSubReportSet tLLSubReportSet = new LLSubReportSet();
		tLLSubReportSet.set(mLLSubReportSet);
		mLLSubReportSet = new LLSubReportSet();
		mLLCaseRelaSet = new LLCaseRelaSet();

		for (int i = 1; i <= tLLSubReportSet.size(); i++) {
			LLSubReportSchema tTempLLSubReportSchema = tLLSubReportSet.get(i);

			LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();

			// 被保险人客户号
			tLLSubReportSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());

			// 被保险人姓名
			tLLSubReportSchema.setCustomerName(mLLCaseSchema.getCustomerName());

			// 事件类型 1-疾病 2-意外
			String tAccidentType = tTempLLSubReportSchema.getAccidentType();
			if (tAccidentType != null && !"".equals(tAccidentType)) {
				LDCodeSchema tLDCodeSchema = mCachedLPInfo
						.findLLAccTypeByCode(tAccidentType);
				if (tLDCodeSchema == null) {
					mErrors.addOneError("事件类型代码错误");
					return false;
				}
			}

			tLLSubReportSchema.setAccidentType(tAccidentType);

			// 发生日期
			tLLSubReportSchema.setAccDate(tTempLLSubReportSchema.getAccDate());

			// 事件信息
			tLLSubReportSchema.setAccDesc(tTempLLSubReportSchema.getAccDesc());

			// 发生地点
			tLLSubReportSchema
					.setAccPlace(tTempLLSubReportSchema.getAccPlace());

			// 入院日期
			tLLSubReportSchema.setInHospitalDate(tTempLLSubReportSchema
					.getInHospitalDate());

			// 出院日期
			tLLSubReportSchema.setOutHospitalDate(tTempLLSubReportSchema
					.getOutHospitalDate());

			// 操作人，取登录人
			tLLSubReportSchema.setOperator(mGlobalInput.Operator);

			// 事件创建日期
			tLLSubReportSchema.setMakeDate(mCurrentDate);

			// 事件创建时间
			tLLSubReportSchema.setMakeTime(mCurrentTime);

			// 事件修改日期
			tLLSubReportSchema.setModifyDate(mCurrentDate);

			// 事件修改时间
			tLLSubReportSchema.setModifyTime(mCurrentTime);
			
			// #3544 发生地点(省)
			tLLSubReportSchema.setAccProvinceCode(tTempLLSubReportSchema.getAccProvinceCode());
			
			// #3544发生地点(市)
			tLLSubReportSchema.setAccCityCode(tTempLLSubReportSchema.getAccCityCode());
			
			// #3544发生地点(县)
			tLLSubReportSchema.setAccCountyCode(tTempLLSubReportSchema.getAccCountyCode());

			String tLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);

			String tSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);

			tLLSubReportSchema.setSubRptNo(tSubRptNo);
			tLLSubReportSchema.setMngCom(mLLCaseSchema.getMngCom());

			// 案件事件关联表
			String tCaseRelaNo = PubFun1.CreateMaxNo("CASERELANO", tLimit);

			LLCaseRelaSchema tLLCaseRelaSchema = new LLCaseRelaSchema();
			tLLCaseRelaSchema.setCaseNo(mLLCaseSchema.getCaseNo());
			tLLCaseRelaSchema.setCaseRelaNo(tCaseRelaNo);
			tLLCaseRelaSchema.setSubRptNo(tSubRptNo);

			mLLSubReportSet.add(tLLSubReportSchema);
			mLLCaseRelaSet.add(tLLCaseRelaSchema);

		}

		return true;
	}

	/**
	 * 处理LLAppClaimReason
	 * 
	 * @return
	 * @throws Exception
	 */
	private boolean dealLLAppClaimReason() throws Exception {
		System.out.println("dealLLAppClaimReason......");

		if (mLLAppClaimReasonSet == null) {
			return true;
		}

		// 备份获取数据
		LLAppClaimReasonSet tLLAppClaimReasonSet = new LLAppClaimReasonSet();
		tLLAppClaimReasonSet.set(mLLAppClaimReasonSet);
		mLLAppClaimReasonSet = new LLAppClaimReasonSet();

		for (int i = 1; i <= tLLAppClaimReasonSet.size(); i++) {
			LLAppClaimReasonSchema tTempLLAppClaimReasonSchema = tLLAppClaimReasonSet
					.get(i);
			LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();

			// 申请原因
			String tReasonCode = tTempLLAppClaimReasonSchema.getReasonCode();
			if (tReasonCode == null || "".equals(tReasonCode)) {
				mErrors.addOneError("申请原因获取失败");
				return false;
			}

			LDCodeSchema tLDCodeSchema = mCachedLPInfo
					.findLLRgtReasonByCode(tReasonCode);
			if (tLDCodeSchema == null) {
				mErrors.addOneError("申请原因代码错误");
				return false;
			}

			tLLAppClaimReasonSchema.setReasonCode(tReasonCode);

			// 批次号
			tLLAppClaimReasonSchema.setRgtNo(mLLCaseSchema.getRgtNo());

			// 案件号
			tLLAppClaimReasonSchema.setCaseNo(mLLCaseSchema.getCaseNo());

			// 客户号
			tLLAppClaimReasonSchema
					.setCustomerNo(mLLCaseSchema.getCustomerNo());

			// 原因类型
			tLLAppClaimReasonSchema.setReasonType("0");

			// 操作人
			tLLAppClaimReasonSchema.setOperator(mGlobalInput.Operator);

			// 管理机构
			tLLAppClaimReasonSchema.setMngCom(mLLCaseSchema.getMngCom());

			// 生成日期
			tLLAppClaimReasonSchema.setMakeDate(mCurrentDate);

			// 生成时间
			tLLAppClaimReasonSchema.setMakeTime(mCurrentTime);

			// 修改日期
			tLLAppClaimReasonSchema.setModifyDate(mCurrentDate);

			// 修改时间
			tLLAppClaimReasonSchema.setModifyTime(mCurrentTime);
			mLLAppClaimReasonSet.add(tLLAppClaimReasonSchema);
		}

		return true;
	}

	/**
	 * 得到处理后的结果数据
	 * 
	 * @return VData
	 */
	public VData getResult() throws Exception {
		mResult.clear();
		mResult.add(mLLCaseSchema);
		mResult.add(mLLRegisterSchema);
		mResult.add(mLLCaseRelaSet);
		mResult.add(mLLSubReportSet);
		if (mLDPersonSchema != null) {
			mResult.add(mLDPersonSchema);
		}
		if (mLLAppClaimReasonSet != null && mLLAppClaimReasonSet.size() > 0) {
			mResult.add(mLLAppClaimReasonSet);
		}
		return mResult;
	}

	/**
	 * 获取提交数据库的MMap
	 */
	public MMap getMMap() throws Exception {
		mMMap.put(mLLCaseSchema, "INSERT");
		mMMap.put(mLLRegisterSchema, "INSERT");
		mMMap.put(mLLCaseRelaSet, "INSERT");
		mMMap.put(mLLSubReportSet, "INSERT");
		if (mLDPersonSchema != null) {
			mMMap.put(mLDPersonSchema, "UPDATE");
		}
		if (mLLAppClaimReasonSet != null && mLLAppClaimReasonSet.size() > 0) {
			mMMap.put(mLLAppClaimReasonSet, "INSERT");
		}
		return mMMap;
	}

	/**
	 * 处理LDPerson死亡日期
	 * 
	 * @return 成功标志
	 */
	private boolean dealLDPerson() throws Exception {
		LDPersonDB tLDPersonDB = new LDPersonDB();
		tLDPersonDB.setCustomerNo(mLLCaseSchema.getCustomerNo());

		if (!tLDPersonDB.getInfo()) {
			mErrors.addOneError("被保险人客户信息查询失败");
			return false;
		}

		// 客户信息表死亡日期为空时，进行维护
		if (tLDPersonDB.getDeathDate() == null) {
			mLDPersonSchema = tLDPersonDB.getSchema();
			mLDPersonSchema.setDeathDate(mLLCaseSchema.getDeathDate());
			mLDPersonSchema.setOperator(mGlobalInput.Operator);
			mLDPersonSchema.setModifyDate(mCurrentDate);
			mLDPersonSchema.setModifyTime(mCurrentTime);
		}

		return true;
	}

	/**
	 * 根据被保险人险种类型，出险日期、保单失效日期，判断是否需要录入账单
	 * RiskType1:１-医疗保险，2-疾病保险，3-收入保障保险，4-长期护理保险，5-意外保险，6-医疗责任保险，7-其他
	 * 
	 * @param aCustomerNo
	 *            被保险人客户号
	 * @param aAccidentDate
	 *            被保险人出险日期
	 * @return 账单录入标志 0-未录入,1-录入完毕，3-不需要录入
	 */
	private String dealReceiptFlag(String aCustomerNo, String aAccidentDate)
			throws Exception {
		String tReceiptFlag = "";
		if (aCustomerNo == null || "".equals(aCustomerNo)) {
			mErrors.addOneError("被保险人客户号为空");
			return tReceiptFlag;
		}

		if (aAccidentDate == null || "".equals(aAccidentDate)) {
			mErrors.addOneError("出险日期为空");
			return tReceiptFlag;
		}

		System.out.println("账单标志判断：被保险人客户号-" + aCustomerNo + ",出险日期-"
				+ aAccidentDate);

		String tReceiptSQL = "SELECT b.RiskType1 FROM LCPol a,LMRiskApp b "
				+ " WHERE a.AppFlag='1' AND b.RiskCode = a.RiskCode AND a.StateFlag<>'4' "
				+ " AND b.RiskType1 in ('2','5') "
				+ " AND a.insuredno='"
				+ aCustomerNo
				+ "' AND a.EndDate >='"
				+ aAccidentDate
				+ "'"
				+ " UNION ALL "
				+ " SELECT b.RiskType1 FROM LBPol a,LMRiskApp b "
				+ " WHERE a.AppFlag='1' AND b.RiskCode = a.RiskCode AND a.StateFlag<>'4' "
				+ " AND b.RiskType1 in ('2','5') " + " AND a.insuredno='"
				+ aCustomerNo + "' AND a.EndDate >='" + aAccidentDate + "'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tReceiptSSRS = new SSRS();
		System.out.println(tReceiptSQL);
		tReceiptSSRS = tExeSQL.execSQL(tReceiptSQL);
		if (tReceiptSSRS.getMaxRow() > 0) {
			tReceiptFlag = "3";
		} else {
			tReceiptFlag = "0";
		}
		return tReceiptFlag;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// LLCaseRegister tLLCaseRegister = new LLCaseRegister();
		// // 案件信息
		// LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		// tLLCaseSchema.setCustomerNo("000000031");
		// // 事件信息
		// LLSubReportSet tLLSubReportSet = new LLSubReportSet();
		//
		// LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
		// tLLSubReportSchema.setAccDate("2011-4-26");
		//
		// tLLSubReportSet.add(tLLSubReportSchema);
		//
		// VData tVData = new VData();
		// tVData.add(tLLCaseSchema);
		// tVData.add(tLLSubReportSet);
		// try {
		// tLLCaseRegister.submitData(tVData, null);
		// } catch (Exception ex) {
		// ex.printStackTrace();
		// }

		System.out.println("14:02:16".compareTo("12:26:42"));

	}
}
