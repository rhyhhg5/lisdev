package com.sinosoft.lis.llcase;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class CaseTransferAutoBL {

    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    ExeSQL tExeSQL = new ExeSQL();


    private GlobalInput mGlobalInput = new GlobalInput();
    private LLCaseSet mLLCaseSet = new LLCaseSet();

    public CaseTransferAutoBL() {
    }

    public static void main(String[] args) {
        LLCaseSet tLLCaseSet = new LLCaseSet();
        CaseTransferAutoBL tCaseTransferBL = new CaseTransferAutoBL();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setCaseNo("C1100060719000007");
        tLLCaseSet.add(tLLCaseSchema);

        String Operate = "cm0001";
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "cm0001";

        VData tVData = new VData();

        tVData.add(tG);
        tVData.add(tLLCaseSet);

        tCaseTransferBL.submitData(tVData, Operate);

    }

    public VData getResult() {
        return mResult;
    }


    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        //得到输入数据
        if (!getInputData()) {
            return false;
        }

        if (mLLCaseSet.size() == 0) {
            LLCaseDB tLLCaseDB = new LLCaseDB();
            String strSQL = "select a.*  from llcase a, ldcode b where "
                            + " b.codetype='llrgtstate' and b.code=a.rgtstate and a.rgtstate not in  ('07','09','12','14','11','13')  and a.handler='"
                            + cOperate + "' "
                            + " union select a.* from llcase a, ldcode b,llclaimuwmain c where b.codetype='llrgtstate' "
                            + " and b.code=a.rgtstate and a.rgtstate not in  ('07','09','12','14','11','13')  "
                            +
                            " and rgtstate in ('05','10') and c.caseno=a.caseno and c.appclmuwer='"
                            + cOperate + "' ";

            mLLCaseSet = tLLCaseDB.executeQuery(strSQL);
        }

        int count = mLLCaseSet.size();
        String SimpleCase = "00";
        String strMngCom = mGlobalInput.ManageCom;
        for (int j = 1; j <= count; j++) {
            MMap map = new MMap();
            LLCaseSet tLLCaseSet = new LLCaseSet();
            LLCaseDB tLLCaseDB = new LLCaseDB();
            LLCaseBackSet mLLCaseBackSet = new LLCaseBackSet();
            LLCaseSchema tLLCaseSchema = new LLCaseSchema();
            tLLCaseSchema = mLLCaseSet.get(j);
            LLCaseBackSchema mLLCaseBackSchema = new LLCaseBackSchema();
            LLClaimUWMainSchema tLLClaimUWMainSchema = new LLClaimUWMainSchema();
            LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new
                    LLClaimUnderwriteSchema();
            LLCaseCommon tLLCaseCommon = new LLCaseCommon();

            LLCaseOpTimeSet tLLCaseOpTimeSet = new LLCaseOpTimeSet();
            LLClaimUnderwriteSet tLLClaimUnderwriteSet = new
                    LLClaimUnderwriteSet();
            LLClaimUWMainSet tLLClaimUWMainSet = new LLClaimUWMainSet();

            LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
            LLClaimUserSchema tLLClaimUserSchema = new LLClaimUserSchema();
            tLLClaimUserDB.setUserCode(cOperate);
            tLLClaimUserDB.getInfo();
            tLLClaimUserSchema = tLLClaimUserDB.getSchema();
            String AppGrade = tLLClaimUserSchema.getClaimPopedom();

            String strHandler = tLLCaseCommon.ChooseUser(strMngCom,
                    tLLCaseSchema.getCustomerNo(),
                    SimpleCase, cOperate);
            System.out.println("-----------strHandler" + strHandler);
            tLLCaseDB.setCaseNo(tLLCaseSchema.getCaseNo());
            tLLCaseDB.getInfo();
            tLLCaseSchema = tLLCaseDB.getSchema();

            if (!LLCaseCommon.checkHospCaseState(tLLCaseSchema.getCaseNo())) {
                CError.buildErr(this,
                                tLLCaseSchema.getCaseNo() + "医保通案件待确认，不能进行处理");
                return false;
            }

            String tLimit = PubFun.getNoLimit(strMngCom);
            String CaseBackNo = PubFun1.CreateMaxNo("CaseBack", tLimit);
            mLLCaseBackSchema.setCaseBackNo(CaseBackNo);
            mLLCaseBackSchema.setCaseNo(tLLCaseSchema.getCaseNo());
            mLLCaseBackSchema.setRgtNo(tLLCaseSchema.getRgtNo());
            mLLCaseBackSchema.setBeforState(tLLCaseSchema.getRgtState());
            mLLCaseBackSchema.setAfterState(tLLCaseSchema.getRgtState());
            mLLCaseBackSchema.setOHandler(tLLCaseSchema.getHandler());
            mLLCaseBackSchema.setNHandler(strHandler);
            mLLCaseBackSchema.setMngCom(mGlobalInput.ManageCom);
            mLLCaseBackSchema.setOperator(mGlobalInput.Operator);
            mLLCaseBackSchema.setMakeDate(PubFun.getCurrentDate());
            mLLCaseBackSchema.setMakeTime(PubFun.getCurrentTime());
            mLLCaseBackSchema.setModifyDate(PubFun.getCurrentDate());
            mLLCaseBackSchema.setModifyTime(PubFun.getCurrentTime());
            mLLCaseBackSchema.setBackType("9");
            mLLCaseBackSet.add(mLLCaseBackSchema);

            tLLCaseSchema = tLLCaseDB.getSchema();
            if (!(tLLCaseSchema.getRgtState().equals("05") ||
                  tLLCaseSchema.getRgtState().equals("10"))) {
                tLLCaseSchema.setHandler(strHandler);
            }
            tLLCaseSchema.setDealer(strHandler);
            String sql = "select comcode from llclaimuser where usercode='" +
                         cOperate + "'";
            String comname = tExeSQL.getOneValue(sql);

            if (!tLLCaseSchema.getRgtState().equals("02") &&
                !tLLCaseSchema.getRgtState().equals("05") &&
                tLLCaseSchema.getRgtState().equals("06")) {
                LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
                tLLCaseOpTimeSchema.setCaseNo(tLLCaseSchema.getCaseNo());
                tLLCaseOpTimeSchema.setRgtState(tLLCaseSchema.getRgtState());
                tLLCaseOpTimeSchema.setOperator(cOperate);
                tLLCaseOpTimeSchema.setManageCom(comname);
                tLLCaseOpTimeSchema.setSequance(99);
                tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(
                        tLLCaseOpTimeSchema);
                tLLCaseOpTimeSet.add(tLLCaseOpTimeSchema);

            }
            if (comname.length() > 2) {
                tLLCaseSchema.setCaseProp("06");
            }
            if (tLLCaseSchema.getRgtState().equals("05")) {
                LLClaimUWMainDB mLLClaimUWMainDB = new LLClaimUWMainDB();
                LLClaimUWMainSet mLLClaimUWMainSet = new LLClaimUWMainSet();
                mLLClaimUWMainDB.setCaseNo(tLLCaseSchema.getCaseNo());
                mLLClaimUWMainSet = mLLClaimUWMainDB.query();
                tLLClaimUWMainSchema = mLLClaimUWMainSet.get(1);
                tLLClaimUWMainSchema.setAppClmUWer(cOperate);
                tLLClaimUWMainSchema.setAppGrade(AppGrade);

                LLClaimUnderwriteDB tLLClaimUnderwriteDB = new
                        LLClaimUnderwriteDB();
                LLClaimUnderwriteSet mLLClaimUnderwriteSet = new
                        LLClaimUnderwriteSet();
                tLLClaimUnderwriteDB.setCaseNo(tLLCaseSchema.getCaseNo());
                mLLClaimUnderwriteSet = tLLClaimUnderwriteDB.query();
                tLLClaimUnderwriteSchema = mLLClaimUnderwriteSet.get(1);
                tLLClaimUnderwriteSchema.setAppClmUWer(cOperate);

                tLLClaimUnderwriteSet.add(tLLClaimUnderwriteSchema);
                tLLClaimUWMainSet.add(tLLClaimUWMainSchema);
            }
            if (tLLCaseSchema.getRgtState().equals("10")) {
                LLClaimUWMainDB mLLClaimUWMainDB = new LLClaimUWMainDB();
                LLClaimUWMainSet mLLClaimUWMainSet = new LLClaimUWMainSet();
                mLLClaimUWMainDB.setCaseNo(tLLCaseSchema.getCaseNo());
                mLLClaimUWMainSet = mLLClaimUWMainDB.query();
                tLLClaimUWMainSchema = mLLClaimUWMainSet.get(1);
                tLLClaimUWMainSchema.setAppClmUWer(cOperate);
                tLLClaimUWMainSchema.setAppGrade(AppGrade);
                tLLClaimUWMainSet.add(tLLClaimUWMainSchema);
            }

            tLLCaseSet.add(tLLCaseSchema);

            map.put(tLLCaseSet, "UPDATE");
            map.put(mLLCaseBackSet, "INSERT");
            if (tLLCaseOpTimeSet != null &&
                tLLCaseOpTimeSet.size() != 0) {
                map.put(tLLCaseOpTimeSet, "INSERT");
            }
            if (tLLClaimUWMainSet != null &&
                tLLClaimUWMainSet.size() != 0) {
                map.put(tLLClaimUWMainSet, "UPDATE");
            }
            if (tLLClaimUnderwriteSet != null &&
                tLLClaimUnderwriteSet.size() != 0) {
                map.put(tLLClaimUnderwriteSet, "UPDATE");
            }

            this.mResult.add(map);
            mInputData.add(map);

            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, cOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "CaseTransferBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mInputData.clear();
        }
        mInputData = null;
        return true;
    }

    private boolean getInputData() {
        System.out.println("getInputData()...");

        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLLCaseSet = (LLCaseSet) mInputData.getObjectByObjectName(
                "LLCaseSet", 0);

        // mLLCaseSet = (LLCaseSet) mInputData.getObjectByObjectName(  "LLCaseSet", 0);


        return true;
    }


}
