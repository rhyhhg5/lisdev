package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class QueryPayPolBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  /**  */
  private LLClaimSchema mLLClaimSchema = new LLClaimSchema();
  private LLClaimSet mLLClaimSet = new LLClaimSet();
  private LLClaimPolicySet mLLClaimPolicySet = new LLClaimPolicySet();
  private LLClaimDetailSet mLLClaimDetailSet = new LLClaimDetailSet();
  private LLClaimUnderwriteSchema mLLClaimUnderwriteSchema = new LLClaimUnderwriteSchema();
  private LLClaimUWMainSchema mLLClaimUWMainSchema = new LLClaimUWMainSchema();
  private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
  private LLClaimErrorSet mLLClaimErrorSet = new LLClaimErrorSet();


  public QueryPayPolBL() {}

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;

    //数据查询业务处理
    if (cOperate.equals("QUERY||MAIN"))
    {
      if(!queryData())
	return false;
      System.out.println("---queryData---");
    }
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    // 暂交费查询条件
    if (mOperate.equals("QUERY||MAIN"))
    {
      mLLClaimSchema.setSchema((LLClaimSchema)cInputData.getObjectByObjectName("LLClaimSchema",0));
    }

    return true;
  }

  /**
   * 查询符合条件的暂交费信息
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean queryData()
  {

    String a_HandlerName="";
    LLClaimDB tLLClaimDB = new LLClaimDB();
    tLLClaimDB.setSchema( mLLClaimSchema);
    mLLClaimSet=tLLClaimDB.query();
    if (tLLClaimDB.mErrors.needDealError() == true)
    {
      // @@错误处理
	  this.mErrors.copyAllErrors(tLLClaimDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "QueryPayPolBL";
      tError.functionName = "queryData";
      tError.errorMessage = "赔案查询失败!";
      this.mErrors.addOneError(tError);
      mLLClaimSet.clear();
      return false;
    }
    if (mLLClaimSet.size() == 0)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "QueryPayPolBL";
      tError.functionName = "queryData";
      tError.errorMessage = "未找到相关数据!";
      this.mErrors.addOneError(tError);
      mLLClaimSet.clear();
      return false;
     }
    mResult.clear();
    a_HandlerName=(CaseFunPub.show_People(mLLClaimSet.get(1).getClmUWer().trim()));
    mResult.addElement(a_HandlerName.trim());
    mResult.add(mLLClaimSet);

    LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
    tLLClaimPolicyDB.setRgtNo( mLLClaimSchema.getRgtNo());
    mLLClaimPolicySet.set(tLLClaimPolicyDB.query());
    if (tLLClaimPolicyDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLClaimPolicyDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "QueryPayPolBL";
      tError.functionName = "queryData";
      tError.errorMessage = "保单赔案信息查询失败!";
      this.mErrors.addOneError(tError);
      mLLClaimPolicySet.clear();
      return false;
    }

    mResult.add( mLLClaimPolicySet );

    LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
    System.out.println("begin");
    tLLClaimDetailDB.setRgtNo( mLLClaimSchema.getRgtNo());
    System.out.println("getdata");
    mLLClaimDetailSet.set(tLLClaimDetailDB.query());
    System.out.println("end");
    if (tLLClaimDetailDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLClaimDetailDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "QueryPayPolBL";
      tError.functionName = "queryData";
      tError.errorMessage = "保单赔案明细信息查询失败!";
      this.mErrors.addOneError(tError);
      mLLClaimDetailSet.clear();
      return false;
    }

    mResult.add( mLLClaimDetailSet );

    LLClaimUnderwriteDB tLLClaimUnderwriteDB = new LLClaimUnderwriteDB();
    LLClaimUnderwriteSet tLLClaimUnderwriteSet = new LLClaimUnderwriteSet();
    tLLClaimUnderwriteDB.setClmNo( mLLClaimSet.get(1).getClmNo());
    tLLClaimUnderwriteSet.set(tLLClaimUnderwriteDB.query());
    System.out.println("sdfsdff=============================");
    System.out.println(tLLClaimUnderwriteSet.size());
    mResult.add( tLLClaimUnderwriteSet );

    LLClaimUWMainDB tLLClaimUWMainDB = new LLClaimUWMainDB();
    LLClaimUWMainSet tLLClaimUWMainSet = new LLClaimUWMainSet();
    tLLClaimUWMainDB.setClmNo(mLLClaimSet.get(1).getClmNo());
    tLLClaimUWMainSet=tLLClaimUWMainDB.query();
    if (tLLClaimUWMainDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLClaimUWMainDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "QueryPayPolBL";
      tError.functionName = "queryData";
      tError.errorMessage = "核赔信息查询失败!";
      this.mErrors.addOneError(tError);
      mLLClaimDetailSet.clear();
      return false;
    }
    if (tLLClaimUWMainSet.size()>0)
    {
      mLLClaimUWMainSchema.setSchema(tLLClaimUWMainSet.get(1));

    }

     mResult.add( mLLClaimUWMainSchema );

     LLRegisterDB tLLRegisterDB = new LLRegisterDB();
     tLLRegisterDB.setRgtNo(mLLClaimSchema.getRgtNo());
     tLLRegisterDB.getInfo();
     if (tLLRegisterDB.mErrors.needDealError() == true)
     {
       // @@错误处理
       this.mErrors.copyAllErrors(tLLClaimUWMainDB.mErrors);
       CError tError = new CError();
       tError.moduleName = "QueryPayPolBL";
       tError.functionName = "queryData";
       tError.errorMessage = "立案信息查询失败!";
       this.mErrors.addOneError(tError);
       return false;
     }

     mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
     mResult.add(mLLRegisterSchema);

     LLClaimErrorDB tLLClaimErrorDB = new LLClaimErrorDB();
     tLLClaimErrorDB.setClmNo(mLLClaimSet.get(1).getClmNo());
     if (tLLClaimErrorDB.getCount()>0)
     {
       mLLClaimErrorSet.set(tLLClaimErrorDB.query());
       if (tLLClaimErrorDB.mErrors.needDealError() == true)
       {
         // @@错误处理
         this.mErrors.copyAllErrors(tLLClaimErrorDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "QueryPayPolBL";
         tError.functionName = "queryData";
         tError.errorMessage = "核赔错误信息查询失败!";
         this.mErrors.addOneError(tError);
         return false;
       }

     }
     mResult.add(mLLClaimErrorSet);



	return true;
  }

  /**
   * 校验传入的暂交费收据号是否合法
   * 输出：如果发生错误则返回false,否则返回true
   */


}