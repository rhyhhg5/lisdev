package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.text.DecimalFormat;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 理赔-团体给付确认逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author：zhangtao
 * @version：1.0
 * @CreateDate：2005-03-18
 */
public class GrpGiveEnsureBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 往后面传输的数据库操作 */
    private MMap map = new MMap();
    /** 执行删除的数据库操作，放在最后 */
    private MMap mapDel = new MMap();
    /**用户登陆信息 */
    private GlobalInput mGlobalInput = new GlobalInput();
    //团体立案
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    //团体立案号
    private String mRgtNo = "";
    private String mPrePaidFlag = "";
    
    private String mBackMsg = "";
    //分案集
    private LLCaseSet mLLCaseSet = new LLCaseSet();
    private LLClaimSet mLLClaimSet = new LLClaimSet();

    private LJSGetSet mLJSGetSet = new LJSGetSet();

    private LJSGetClaimSet mLJSGetClaimSet = new LJSGetClaimSet();
    
    private LLPrepaidGrpContSet mLLPrepaidGrpContSet = new LLPrepaidGrpContSet();
    private LLPrepaidGrpPolSet mLLPrepaidGrpPolSet = new LLPrepaidGrpPolSet();
    private LLPrepaidTraceSet mLLPrepaidTraceSet = new LLPrepaidTraceSet();


    public GrpGiveEnsureBL() {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * @param: cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        System.out.println("after getInputData");

        //根据赔案号、保单号查询保单、赔案、核赔、核赔错误、用户权限信息
        if (!getBaseData()) {
            return false;
        }

        System.out.println("after getBaseData");

        //数据操作业务处理
        if (!dealData()) {
            return false;
        }

        System.out.println("after dealData");

        //准备给后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        System.out.println("after prepareOutputData");

        //数据提交
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ICaseCureBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;

        return true;
    }

    private boolean dealData() {
        MMap tMap = new MMap();
        if (mOperate.equals("GIVE|ENSURE")) {

            //锁定批次
            MMap mMap = lockTable(mRgtNo, mGlobalInput);
            if(mMap != null){
                map.add(mMap);
            } else {
                return false;
            }


            Reflections rf = new Reflections();
            String sNoLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
            String ActuGetNo = PubFun1. //实付号码
                               CreateMaxNo("ACTUGETNO", sNoLimit);

            //查询团体案件下所有个人案件的赔付应付记录
            //修改其他号码与其他号码类型 [RgtNo]
            //生成相应的赔付实付记录
            LJAGetClaimSet tLJAGetClaimSet = new LJAGetClaimSet();
            LCInsureAccTraceSet tsLCInsureAccTraceSet = new LCInsureAccTraceSet();

            for (int i = 1; i <= mLJSGetClaimSet.size(); i++) {
                LJAGetClaimSchema tLJAGetClaimSchema = new LJAGetClaimSchema();
                LJSGetClaimSchema tLJSGetClaimSchema = mLJSGetClaimSet.get(i);
                rf.transFields(tLJAGetClaimSchema, tLJSGetClaimSchema);
                System.out.println("给付代码:" + tLJSGetClaimSchema.getPolNo());
                tLJAGetClaimSchema.setActuGetNo(ActuGetNo);
                tLJAGetClaimSchema.setOtherNo(tLJSGetClaimSchema.getOtherNo());
                tLJAGetClaimSchema.setOPConfirmCode(mGlobalInput.Operator);
                tLJAGetClaimSchema.setOPConfirmDate(PubFun.getCurrentDate());
                tLJAGetClaimSchema.setOPConfirmTime(PubFun.getCurrentTime());
                tLJAGetClaimSchema.setOperator(mGlobalInput.Operator);
//                tLJAGetClaimSchema.setManageCom(mLLRegisterSchema.getMngCom());
//                LLCaseDB tLLCaseDB = new LLCaseDB();
//                tLLCaseDB.setCaseNo(tLJSGetClaimSchema.getOtherNo());
//                if (tLLCaseDB.getInfo()) {
//                    tLJAGetClaimSchema.setManageCom(tLLCaseDB.getMngCom());
//                }
                tLJAGetClaimSchema.setMakeDate(PubFun.getCurrentDate());
                tLJAGetClaimSchema.setMakeTime(PubFun.getCurrentTime());
                tLJAGetClaimSchema.setModifyDate(PubFun.getCurrentDate());
                tLJAGetClaimSchema.setModifyTime(PubFun.getCurrentTime());

                tLJAGetClaimSet.add(tLJAGetClaimSchema);
//                MMap tmpMap = new MMap();
//                tmpMap = LLCaseCommon.addSumMoney(tLJSGetClaimSchema.getOtherNo(),
//                                                  mGlobalInput.Operator);
//                if (tmpMap == null) {
//                    CError tError = new CError();
//                    tError.moduleName = "LJAGetInsertBL";
//                    tError.functionName = "dealData";
//                    tError.errorMessage = "更新lcget中的领取现金失败！";
//                    mErrors.addOneError(tError);
//                    return false;
//                }
//                tMap.add(tmpMap);
            }

            //合成团体案件下所有的个案应付总表记录
            //修改其他号码与其他号码类型 [RgtNo]
            //生成一条团体案件的实付总表记录
            LJAGetSchema tLJAGetSchema = new LJAGetSchema();
            LJSGetSchema tLJSGetSchema = new LJSGetSchema();
            tLJSGetSchema = mLJSGetSet.get(1);
            rf.transFields(tLJAGetSchema, tLJSGetSchema);

            tLJAGetSchema.setActuGetNo(ActuGetNo);
            tLJAGetSchema.setOtherNo(mRgtNo);

            double sumMoney = 0;
            for (int i = 1; i <= mLJSGetSet.size(); i++) {
                tLJSGetSchema = mLJSGetSet.get(i);
                sumMoney += tLJSGetSchema.getSumGetMoney();
            }
            sumMoney = Arith.round(sumMoney, 2);

            tLJAGetSchema.setSumGetMoney(sumMoney);
            System.out.println("LJAGet里的钱：" + sumMoney);
            if ("".equals(StrTool.cTrim(mLLRegisterSchema.getCaseGetMode()))) {
                mLLRegisterSchema.setCaseGetMode("1"); //默认现金
            }
            if ("4".equals(mLLRegisterSchema.getCaseGetMode()) ||
                "11".equals(mLLRegisterSchema.getCaseGetMode())) {
                //保险金领取方式为银行转帐
                tLJAGetSchema.setBankAccNo(mLLRegisterSchema.getBankAccNo());
                tLJAGetSchema.setBankCode(mLLRegisterSchema.getBankCode());
                tLJAGetSchema.setAccName(mLLRegisterSchema.getAccName());
            }
            tLJAGetSchema.setPayMode(mLLRegisterSchema.getCaseGetMode());
            tLJAGetSchema.setDrawer(mLLRegisterSchema.getRgtantName());
            tLJAGetSchema.setDrawerID(mLLRegisterSchema.getIDNo());
            tLJAGetSchema.setOperator(mGlobalInput.Operator);
            tLJAGetSchema.setManageCom(mLLRegisterSchema.getMngCom());
            tLJAGetSchema.setShouldDate(PubFun.getCurrentDate());
            tLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
            tLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
            tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
            tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
            //更新团体案件状态为“04”－所有个人给付确认
            mLLRegisterSchema.setRgtState("05");
            mLLRegisterSchema.setPrePaidFlag(mPrePaidFlag);

            //更新该团体案件下所有案件状态为“给付状态”
            for (int k = 1; k <= mLLCaseSet.size(); k++) {
                String aCaseNo = mLLCaseSet.get(k).getCaseNo();
                mLLCaseSet.get(k).setRgtState("11");               
                //#3331start
                mLLCaseSet.get(k).setModifyDate(PubFun.getCurrentDate());
                mLLCaseSet.get(k).setModifyTime(PubFun.getCurrentTime());
                //#3331end
                LLClaimDB tLLClaimDB = new LLClaimDB();
                LLClaimSet tLLClaimSet = new LLClaimSet();
                tLLClaimDB.setCaseNo(aCaseNo);
                tLLClaimSet.set(tLLClaimDB.query());
                LLClaimSchema tLLClaimSchema = new LLClaimSchema();
                tLLClaimSchema = tLLClaimSet.get(1);
                tLLClaimSchema.setClmState("3");
                mLLClaimSet.add(tLLClaimSchema);

            }
            if (!dealInsureAcc()) {
                return false;
            }
            
            if (!dealPrapaid(tLJAGetSchema,tLJAGetClaimSet)) {
            	CError.buildErr(this, "预付赔款处理失败！");
                return false;
            }

            MMap tmpMap = new MMap();
            tmpMap = LLCaseCommon.addSumMoney(mRgtNo,mGlobalInput.Operator);
            if (tmpMap == null) {
                   CError tError = new CError();
                   tError.moduleName = "GrpGiveEnsureBL";
                   tError.functionName = "dealData";
                   tError.errorMessage = "更新lcget中的领取现金失败！";
                   mErrors.addOneError(tError);
                   return false;
            }
            tMap.add(tmpMap);


            map.put(mLLRegisterSchema, "UPDATE");
            map.put(mLLCaseSet, "UPDATE");
            map.put(mLLClaimSet, "UPDATE");

            map.put(tLJAGetClaimSet, "INSERT");
            map.put(tLJAGetSchema, "INSERT");
            if (tsLCInsureAccTraceSet.size() > 0) {
                map.put(tsLCInsureAccTraceSet, "DELETE&INSERT");
            }
            map.add(mapDel);
            map.add(tMap);
        }

        return true;
    }

    private boolean getInputData(VData cInputData) {
        mGlobalInput.setSchema(
                (GlobalInput)
                cInputData.getObjectByObjectName("GlobalInput", 0));

        mLLRegisterSchema.setSchema(
                (LLRegisterSchema)
                cInputData.getObjectByObjectName("LLRegisterSchema", 0));
        mRgtNo = mLLRegisterSchema.getRgtNo();
        mPrePaidFlag = mLLRegisterSchema.getPrePaidFlag();
        System.out.println("回销预付赔款标记=="+mPrePaidFlag);
        
		if (mRgtNo == null || "".equals(mRgtNo)) {
			mErrors.addOneError("获取批次信息失败");
			return false;
		}
        
		if (mGlobalInput == null) {
			mErrors.addOneError("登录信息获取失败，请重新登录操作");
			return false;
		}
        
        return true;
    }

    private boolean getBaseData() {
        //查询团体立案信息
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if (!tLLRegisterDB.getInfo()) {
            // @@错误处理
            mErrors.copyAllErrors(tLLRegisterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpGiveEnsureBL";
            tError.functionName = "getBaseData";
            tError.errorMessage = "团体立案信息查询失败!" + "团体批次号：" + mRgtNo;
            mErrors.addOneError(tError);
            return false;
        }
        mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
        if (mLLRegisterSchema.getMngCom().length() == 4) {
            mLLRegisterSchema.setMngCom(mLLRegisterSchema.getMngCom() + "0000");
        }

        if (mLLRegisterSchema.getMngCom().length() == 2) {
            mLLRegisterSchema.setMngCom(mLLRegisterSchema.getMngCom() +
                                        "000000");
        }
        
		if (mGlobalInput.Operator == null || "".equals(mGlobalInput.Operator)) {
			mErrors.addOneError("登录信息获取失败，请重新登录");
			return false;
		}

		if (mGlobalInput.ManageCom == null || "".equals(mGlobalInput.ManageCom)) {
			mErrors.addOneError("登录信息获取失败，请重新登录");
			return false;
		}
		
		if (mGlobalInput.Operator.trim().equals(mLLRegisterSchema.getHandler1().trim())) {
			mErrors.addOneError("案件受理人不能进行给付确认操作");
			return false;
		}
		
		//判断是否为社保的批次,若为社保批次，不允许社保人员操作
		if("1".equals(LLCaseCommon.checkSocialSecurity(mRgtNo, "rgtno"))){
			LLSocialClaimUserDB tLLSocialClaimUserDB = new LLSocialClaimUserDB();
			tLLSocialClaimUserDB.setUserCode(mGlobalInput.Operator.trim());
			tLLSocialClaimUserDB.setStateFlag("1");//社保用户有效
			tLLSocialClaimUserDB.setHandleFlag("1");//参与案件分配
			LLSocialClaimUserSet tLLSocialClaimUserSet = tLLSocialClaimUserDB.query();
			
			if (tLLSocialClaimUserSet.size() > 0) {
				mErrors.addOneError("参与案件分配的社保人员不能进行给付确认操作");
				return false;
			}
			LLSocialClaimUserDB ttLLSocialClaimUserDB = new LLSocialClaimUserDB();
			ttLLSocialClaimUserDB.setUserCode(mGlobalInput.Operator.trim());
			if(!ttLLSocialClaimUserDB.getInfo()){
				mErrors.addOneError("非社保配置用户不能进行给付确认操作");
				return false;
			}
			
		}


        if ("1".equals(mLLRegisterSchema.getTogetherFlag())) {
            CError tError = new CError();
            tError.moduleName = "GrpGiveEnsureBL";
            tError.functionName = "getBaseData";
            tError.errorMessage = "团体立案给付方式为个人给付!" +
                                  "团体批次号：" + mRgtNo;
            mErrors.addOneError(tError);
            return false;
        }

        if (!"03".equals(mLLRegisterSchema.getRgtState())
				&& ("3".equals(mLLRegisterSchema.getTogetherFlag()) || "4"
						.equals(mLLRegisterSchema.getTogetherFlag()))) {
			CError tError = new CError();
			tError.moduleName = "GrpGiveEnsureBL";
			tError.functionName = "getBaseData";
			tError.errorMessage = "该批次状态不准许此操作!" + "团体批次号：" + mRgtNo;
			mErrors.addOneError(tError);
			return false;
		}

        String tSql = "SELECT * FROM llcase WHERE rgtno='" + mRgtNo +
                      "' AND rgtstate NOT IN ('09','14','11','12')";
        System.out.println(tSql);
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSql);
        if (tSSRS.getMaxRow() > 0) {
            CError tError = new CError();
            tError.moduleName = "GrpGiveEnsureBL";
            tError.functionName = "getBaseData";
            tError.errorMessage = "团体立案下个人案件尚未全部结案!" + "团体批次号：" + mRgtNo;
            mErrors.addOneError(tError);
            return false;
        }


        //查询团体立案下所有个案信息
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setRgtNo(mRgtNo);
        tLLCaseDB.setRgtState("09");
        mLLCaseSet.set(tLLCaseDB.query());
        if (tLLCaseDB.mErrors.needDealError() == true) {
            // @@错误处理
            mErrors.copyAllErrors(tLLCaseDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpGiveEnsureBL";
            tError.functionName = "getBaseData";
            tError.errorMessage = "团体立案分案信息查询失败!" +
                                  "团体批次号：" + mRgtNo;
            mErrors.addOneError(tError);
            return false;
        }
        String delSql_2 = " delete from LJAGet " +
                          " where othernotype='5' and otherno='" +
                          mRgtNo + "' and (finstate<>'FC' or finstate is null)";//处理财务反冲
        map.put(delSql_2, "DELETE");

        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        for (int i = 1; i <= mLLCaseSet.size(); i++) {
            tLLCaseSchema = mLLCaseSet.get(i);
            LJSGetDB tLJSGetDB = new LJSGetDB();
            tLJSGetDB.setOtherNoType("5");
            tLJSGetDB.setOtherNo(tLLCaseSchema.getCaseNo());
            mLJSGetSet.add(tLJSGetDB.query());
            if (tLJSGetDB.mErrors.needDealError() == true) {
                // @@错误处理
                mErrors.copyAllErrors(tLJSGetDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "GrpGiveEnsureBL";
                tError.functionName = "getBaseData";
                tError.errorMessage = "分案应付总表信息查询失败!" +
                                      "理赔号：" + tLLCaseSchema.getCaseNo();
                mErrors.addOneError(tError);
                return false;
            }

            LJSGetClaimDB LJSGetClaimDB = new LJSGetClaimDB();
            LJSGetClaimDB.setOtherNoType("5");
            LJSGetClaimDB.setOtherNo(tLLCaseSchema.getCaseNo());
            mLJSGetClaimSet.add(LJSGetClaimDB.query());
            if (tLJSGetDB.mErrors.needDealError() == true) {
                // @@错误处理
                mErrors.copyAllErrors(LJSGetClaimDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "GrpGiveEnsureBL";
                tError.functionName = "getBaseData";
                tError.errorMessage = "分案赔付应付表信息查询失败!" +
                                      "理赔号：" + tLLCaseSchema.getCaseNo();
                mErrors.addOneError(tError);
                return false;
            }
            System.out.println("mLJSGetClaimSet 的size:" + mLJSGetClaimSet.size());

            //插入前清空实付表垃圾数据
            String delSql_1 = " delete from LJAGetclaim " +
                              " where othernotype='5' and otherno='" +
                              tLLCaseSchema.getCaseNo() + "' and feeoperationtype<>'FC'";//处理财务反冲
//          String delSql_2 = " delete from LJAGet " +
//                           " where othernotype='5' and otherno='" +
//                           tLLCaseSchema.getCaseNo() +"'";
            String delSql_5 = " delete from LCInsureAccTrace  " +
                              " where othertype='5' and otherno='" +
                              tLLCaseSchema.getCaseNo() + "' ";
            map.put(delSql_5, "DELETE");
            map.put(delSql_1, "DELETE");

            //删除应付表数据
            String delSql_3 = " delete from LJSGetclaim " +
                              " where othernotype='5' and otherno='" +
                              tLLCaseSchema.getCaseNo() + "'";
            String delSql_4 = " delete from LJSGet " +
                              " where othernotype='5' and otherno='" +
                              tLLCaseSchema.getCaseNo() + "'";
            mapDel.put(delSql_3, "DELETE");
            mapDel.put(delSql_4, "DELETE");

//          LCGrpContDB tLCGrpContDB = new LCGrpContDB();
//          String strGrpContNo = "";
//          tLCGrpContDB.setGrpContNo(strGrpContNo);
//          if (!tLCGrpContDB.getInfo())
//          {
//              // @@错误处理
//              mErrors.copyAllErrors(tLCGrpContDB.mErrors);
//              CError tError = new CError();
//              tError.moduleName = "GrpGiveEnsureBL";
//              tError.functionName = "getBaseData";
//              tError.errorMessage = "团体合同信息查询失败!" +
//                                    "团体合同号：" + strGrpContNo;
//              mErrors.addOneError(tError);
//              return false;
//          }
//          mLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());
        }
        if (mLJSGetClaimSet.size() < 1) {
            CError.buildErr(this, "没有生成应付数据！");
            return false;
        }
        String tMngCom = "";
        double tSumMoney = 0.0;
        for (int j = 1; j <= mLJSGetClaimSet.size(); j++) {
        	tSumMoney += mLJSGetClaimSet.get(j).getPay();
//            if (mLJSGetClaimSet.get(j).getPay() <= 0) {
//                continue;
//            }
            if (tMngCom.equals("")) {
                tMngCom = "" + mLJSGetClaimSet.get(j).getManageCom();
            }
            if (!tMngCom.equals("" + mLJSGetClaimSet.get(j).getManageCom())) {
                CError.buildErr(this, "该批次下有赔款归属于不同机构，不能做统一给付！");
                return false;
            }
            
			if ("00000000000000000000".equals(mLJSGetClaimSet.get(j)
					.getGrpContNo())) {
				CError.buildErr(this, "批次下案件"
						+ mLJSGetClaimSet.get(j).getOtherNo()
						+ "赔付了个人保单，请进行单独给付确认、回退或撤件。");
				return false;
			}
        }
        
        String tClaimDetailSQL = "select nvl(sum(b.realpay),0) from llcase a,llclaimdetail b "
        	                   + " where a.caseno=b.caseno and a.rgtstate='09' and a.rgtno='"+mRgtNo+"'";
        System.out.println(tClaimDetailSQL);
        double tSumRealPay = Double.parseDouble(tExeSQL.getOneValue(tClaimDetailSQL));   
        System.out.println(tSumRealPay);
        if (Arith.round(tSumMoney, 2)!=Arith.round(tSumRealPay, 2)) {
        	CError.buildErr(this, "财务应付数据与理算业务数据不符，请回退后重新计算");
            return false;
        }
        
        return true;
    }

    /**
     * 帐户处理
     * @return boolean
     */
    private boolean dealInsureAcc() {
//      String delsql = "delete FROM LCINSUREACCTRACE WHERE OTHERNO IN "
//                      +" (SELECT CASENO FROM LLCASE WHERE RGTSTATE= '09' "
//                      +" AND RGTNO = '"+mRgtNo+"')";
//      map.put(delsql,"DELETE");
    	
    	// 优化 #3284 特需险170106超赔问题  start
    	LLAccClaimTraceSet tLLAccClaimTraceSet = new LLAccClaimTraceSet();
    	// 优化 #3284 特需险170106超赔问题  end
    	
        LCInsureAccTraceSet sLCInsureAccTraceSet = new LCInsureAccTraceSet();
        LCInsureAccSet sLCInsureAccSet = new LCInsureAccSet();
        //查出该批次下发生赔付了的账户险种，如果团体批次给付，只计算该团体投保了的集体保单。
        String tsql = "select distinct a.grpcontno,a.grppolno,sum(a.realpay),a.riskcode from "   //3337 管B增加险种号
                      + " llclaimdetail a,llcase b,lmrisk c,lmdutyget d "
                      +
                      " where a.riskcode = c.riskcode and a.getdutycode=d.getdutycode and b.caseno = a.caseno "
                      + " and a.realpay>0 and c.insuaccflag='Y' and d.needacc='1' AND a.rgtno ='"
                      + mRgtNo + "' and b.rgtstate='09' group by a.grpcontno,a.grppolno,a.riskcode";
        ExeSQL texesql = new ExeSQL();
        SSRS tss = texesql.execSQL(tsql);
        if (tss == null || tss.getMaxRow() <= 0) {
            //没有帐户型险种产生的有效赔款
            return true;
        }
        
        String tClaimSQL = "select b.* from llcase a,llclaimdetail b,lmdutyget c "
        	             + " where a.caseno=b.caseno and a.rgtstate='09' and b.getdutycode=c.getdutycode "
        	             + " and c.needacc='1' and a.rgtno='" + mRgtNo + "'";
		LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
		LLClaimDetailSet tLLClaimDetailSet = tLLClaimDetailDB.executeQuery(tClaimSQL);

		if (tLLClaimDetailSet.size() <= 0) {
			CError.buildErr(this, "理算数据查询失败");
            return false;
		}
		
		for (int ll = 1; ll <= tLLClaimDetailSet.size(); ll++) {
			LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB();
			tLJSGetClaimDB.setOtherNo(tLLClaimDetailSet.get(ll).getCaseNo());
			tLJSGetClaimDB.setPolNo(tLLClaimDetailSet.get(ll).getPolNo());
			tLJSGetClaimDB.setFeeFinaType("ZH");
			if (tLJSGetClaimDB.query().size() <= 0) {
				CError.buildErr(this, "特需财务账户信息生成失败，请重新进行理算!");
				return false;
			}
		}
        
        for (int i = 1; i <= tss.getMaxRow(); i++) {
            LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
            double ibala = 0;
            double gbala = 0;
            double gpay = 0;
            double ipay = 0;
            double remnant = 0;
            //遍历该赔案发生赔付的所有帐户型险种保单
            String tGrpContNo = tss.GetText(i, 1);
            String tGrpPolNo = tss.GetText(i, 2);
            double sumPay = Double.parseDouble(tss.GetText(i, 3));
            String tRiskCode = tss.GetText(i, 4);   // # 3337 管B
            
            tsql = "select a.insuaccno,sum(a.money) from lcinsureacctrace a where a.grpcontno='"+tGrpContNo
                 + "' and a.grppolno='"+tGrpPolNo+"' and exists (select 1 from llcase where a.otherno=caseno and rgtno='"
                 + mRgtNo+"') and state='temp' group by a.insuaccno ";
            SSRS tSSRS = texesql.execSQL(tsql);
            if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
                CError.buildErr(this, "未生成帐户轨迹，请重新理算！");
                return false;
            }
            for (int j = 1; j <= tSSRS.getMaxRow(); j++) {
                remnant += Double.parseDouble(tSSRS.GetText(j,2));
                if (getAccType(tSSRS.GetText(j,1)).equals("002")){
                    ipay += Double.parseDouble(tSSRS.GetText(j,2));
                }else{
                    gpay += Double.parseDouble(tSSRS.GetText(j,2));
                }
            }
            if (Arith.round(sumPay, 2)+Arith.round(remnant, 2)!=0){
                CError.buildErr(this, "生成帐户轨迹错误，请重新理算！");
                return false;
            }
            if (gpay!=0){
                String gaccpol = "";
                tsql = "select distinct b.insuaccno from lcinsureacc a,lmriskinsuacc b "
                     + " where a.insuaccno=b.insuaccno and b.acctype='001' and grppolno='"
                     + tGrpPolNo + "'";
                String grpaccno = texesql.getOneValue(tsql);
                tsql = "select * from lcinsureacc where insuaccno='" + grpaccno +
                       "' and polno="
                     + "(select a.polno from lcpol a where a.poltypeflag='2' and grppolno='"
                     + tGrpPolNo + "')";
                LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
                LCInsureAccSet tInsureAccSet = tLCInsureAccDB.executeQuery(tsql);
                LCInsureAccSchema tInsuAccSchema = new LCInsureAccSchema();
                
                // 优化 #3284 特需险170106超赔问题  start 
                if ("170106".equals(tInsureAccSet.get(1).getRiskCode())) {
                	LLAccClaimTraceSchema tLLAccClaimTraceSchema = new LLAccClaimTraceSchema();
                	LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
                	tLCInsureAccTraceDB.setGrpPolNo(tGrpPolNo);
                	tLCInsureAccTraceDB.setInsuAccNo(grpaccno);
                	LCInsureAccTraceSet tLCInsureAccTraceSet_ = tLCInsureAccTraceDB.query();
                	LCInsureAccTraceSchema tLCInsureAccTraceSchema = tLCInsureAccTraceSet_.get(1);
                	// 通过轨迹表查询的账户余额是否足够，不够为1，够为0
                	String sql = "select case when "
                		+ "(select nvl(sum(money),0) from LCInsureAccTrace where GrpPolNo='"+tGrpPolNo+"' and InsuAccNo='"+grpaccno+"' and state='0')+ "
                		+ "(select nvl(sum(money),0) from LCInsureAccTrace where GrpPolNo='"+tGrpPolNo+"' and InsuAccNo='"+grpaccno+"' and state='temp'and moneytype='PK'and money<0) "
                		+ "<0 then 1 else 0 end from dual ";
	                String acc = texesql.getOneValue(sql);
	                // 查询账户余额时间
	                tLLAccClaimTraceSchema.setBeforeClaimTime(PubFun.getCurrentTime());
                	if ("1".equals(acc)) {
                		CError.buildErr(this, "团体账户余额不足，无法给付！请回退重新理算");
                        return false;
                	}
                	// 账户余额，不包括理赔的temp
                	sql = "select sum(money) from LCInsureAccTrace where GrpPolNo='"+tGrpPolNo+"' " +
            			"and InsuAccNo='"+grpaccno+"' and State = '0' ";
                	String realbala = String.valueOf(Double.parseDouble(texesql.getOneValue(sql)) + gpay);
                	tLLAccClaimTraceSchema.setGrpContNo(tLCInsureAccTraceSchema.getGrpContNo());
                	tLLAccClaimTraceSchema.setGrpPolNo(tLCInsureAccTraceSchema.getGrpPolNo());
                	tLLAccClaimTraceSchema.setContNo(tLCInsureAccTraceSchema.getContNo());
                	tLLAccClaimTraceSchema.setPolNo(tLCInsureAccTraceSchema.getPolNo());
                	tLLAccClaimTraceSchema.setCaseNo("OtherNo");
                	tLLAccClaimTraceSchema.setSerialNo(PubFun.getCurrentDate2()+PubFun1.CreateMaxNo("llaccclaimtrace",12));
                	tLLAccClaimTraceSchema.setPatchNo(realbala);// 暂时存入理赔后的正确余额
                	tLLAccClaimTraceSchema.setExeLocation("0");
                	tLLAccClaimTraceSchema.setInsuAccNo(tLCInsureAccTraceSchema.getInsuAccNo());
                	tLLAccClaimTraceSchema.setRiskCode(tLCInsureAccTraceSchema.getRiskCode());
                	tLLAccClaimTraceSchema.setOtherNo(mRgtNo);
                	tLLAccClaimTraceSchema.setOtherType("5");
                	tLLAccClaimTraceSchema.setMoneyType("PK");
                	tLLAccClaimTraceSchema.setBeforeClaimMoney(tInsureAccSet.get(1).getInsuAccBala());// 错误的理赔前余额
                	tLLAccClaimTraceSchema.setAfterClaimMoney(tInsureAccSet.get(1).getInsuAccBala() + gpay);// 错误的理赔后余额
                	tLLAccClaimTraceSchema.setOperator(mGlobalInput.Operator);
                	tLLAccClaimTraceSchema.setMakeDate(PubFun.getCurrentDate());
                	tLLAccClaimTraceSchema.setMakeTime(PubFun.getCurrentTime());
                	tLLAccClaimTraceSchema.setModifyDate(PubFun.getCurrentDate());
                	tLLAccClaimTraceSchema.setModifyTime(PubFun.getCurrentTime());
                	tLLAccClaimTraceSet.add(tLLAccClaimTraceSchema);
                }
                // 优化 #3284 特需险170106超赔问题  end
                
                if (tInsureAccSet.size() <= 0) {
                    CError.buildErr(this,
                                    "数据错误，团体保单" + tGrpContNo + "没有对应的团体账户！");
                    return false;
                } else {
                    tInsuAccSchema = tInsureAccSet.get(1);
                    gbala = tInsuAccSchema.getInsuAccBala();
                    if (Arith.round(gbala, 2)+Arith.round(gpay, 2)<0 && !"162501".equals(tRiskCode)){  // 改校验不包括管B
                        CError.buildErr(this,"团体帐户余额不足！");
                        return false;
                    }
                    
                    // # 3337 管理式补充医疗保险B款 保额校验**start**
                    if("162501".equals(tRiskCode)){
                    	//本次公共账户 赔款额+ 公共账户余额进行比较
                    	if(gpay+ LLCaseCommon.getPooledAccount(mRgtNo,tInsuAccSchema.getPolNo(), PubFun.getCurrentDate(), "JF")<0){
                    		 CError.buildErr(this, "赔付金额大于个人保险金额，无法给付！请回退重新理算");
                             return false;	
                    	}
                    }
                    
                    
                    tInsuAccSchema.setInsuAccBala(gbala+gpay);
                    tInsuAccSchema.setSumPaym(tInsuAccSchema.getSumPaym()-gpay);
                    tInsuAccSchema.setOperator(mGlobalInput.Operator);
                    tInsuAccSchema.setModifyDate(PubFun.getCurrentDate());
                    tInsuAccSchema.setModifyTime(PubFun.getCurrentTime());
                    sLCInsureAccSet.add(tInsuAccSchema);
                }
            }
            if (ipay!=0){
                double tPayMoney =0;
                tsql = "select a.polno,sum(a.money) from lcinsureacctrace a,llcase b "
                     + " where b.caseno=a.otherno and b.rgtstate = '09' and a.money<>0 and a.state='temp' and b.rgtno='"
                     + mRgtNo + "' and a.grppolno='" + tGrpPolNo + "' "
                     + " group by a.polno order by a.polno";
                System.out.println("tsql="+tsql);
                SSRS pss = texesql.execSQL(tsql);
                for (int j = 1; j <= pss.getMaxRow(); j++) {
                    String tPolNo = pss.GetText(j, 1);
                    tsql = "select a.polno,a.insuaccno from lcinsureacctrace a,lmriskinsuacc b "
                         + " where a.insuaccno=b.insuaccno and b.acctype='002' and a.polno = '"
                         + tPolNo+"' and a.grppolno='"+tGrpPolNo
                         + "' group by a.polno,a.insuaccno order by a.insuaccno desc";
                    System.out.println("tsql="+tsql);
                    SSRS tCSSRS = texesql.execSQL(tsql);
                    for(int m=1;m<=tCSSRS.getMaxRow();m++){
                        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
                        tLCInsureAccDB.setPolNo(tCSSRS.GetText(m,1));
                        tLCInsureAccDB.setInsuAccNo(tCSSRS.GetText(m,2));
                        if (!tLCInsureAccDB.getInfo()) {
                            CError.buildErr(this, "查询不到轨迹对应账户信息！");
                            return false;
                        }
                        double tInsuAccPay = 0;
                        tInsuAccPay=Double.parseDouble(pss.GetText(j,2));
                        //2713补充A**start**新增代码
                        LCPolBL tLCPolBL = new LCPolBL();
                        tLCPolBL.setPolNo(tPolNo);
                        if (!tLCPolBL.getInfo()) {
                            CError.buildErr(this, "险种保单信息查询错误!");
                        }
                        LCPolSchema tLCPolSchema = new LCPolSchema();
                        tLCPolSchema.setSchema(tLCPolBL.getSchema());
                        String riskcode = tLCPolSchema.getRiskCode();
                        // 3207 管理式补充A 把不看lcinsureacc 表账户余额的险种做成配置，方便以后的开发。
                        String codeSQL ="select 1 from ldcode where codetype='lprisk' and code ='"+riskcode+"' with ur";
                        String tCode = texesql.getOneValue(codeSQL);
                        //2713补充A**end**新增代码
                        
                        // 优化 #3284 特需险170106超赔问题  start 
                        if ("170106".equals(riskcode)) {
                        	LLAccClaimTraceSchema tLLAccClaimTraceSchema = new LLAccClaimTraceSchema();
                        	LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
                        	tLCInsureAccTraceDB.setPolNo(tCSSRS.GetText(m,1));
                        	tLCInsureAccTraceDB.setInsuAccNo(tCSSRS.GetText(m,2));
                        	LCInsureAccTraceSet tLCInsureAccTraceSet_ = tLCInsureAccTraceDB.query();
                        	LCInsureAccTraceSchema tLCInsureAccTraceSchema = tLCInsureAccTraceSet_.get(1);
                        	// 通过轨迹表查询的账户余额是否足够，不够为1，够为0
                        	String sql = "select case when "
                        		+ "(select nvl(sum(money),0) from LCInsureAccTrace where PolNo='"+tCSSRS.GetText(m,1)+"' and InsuAccNo='"+tCSSRS.GetText(m,2)+"' and state='0')+ "
                        		+ "(select nvl(sum(money),0) from LCInsureAccTrace where PolNo='"+tCSSRS.GetText(m,1)+"' and InsuAccNo='"+tCSSRS.GetText(m,2)+"' and state='temp'and moneytype='PK'and money<0) "
                        		+ "<0 then 1 else 0 end from dual ";
    		                String acc = texesql.getOneValue(sql);
    		                // 查询账户余额时间
    		                tLLAccClaimTraceSchema.setBeforeClaimTime(PubFun.getCurrentTime());
                        	if ("1".equals(acc)) {
                        		String tSql = "select distinct b.customername,a.otherno from lcinsureacctrace a,llcase b where a.polno = '"
                        			+tPolNo+"' and a.insuaccno = '"+tCSSRS.GetText(m,2)+"' and a.otherno=b.caseno and b.rgtno = '"+mRgtNo+"'";
                        		ExeSQL tExeSql = new ExeSQL();
                        		SSRS tMessage = tExeSql.execSQL(tSql);
                        		CError.buildErr(this,"被保险人姓名：" + tMessage.GetText(1, 1) + "，案件号" + tMessage.GetText(1, 2) + "所赔付个人账户余额不足，无法给付！请回退重新理算");
                                return false;
                        	}
                        	// 账户余额，不包括理赔的temp
                        	sql = "select sum(money) from LCInsureAccTrace where PolNo='"+tCSSRS.GetText(m,1)+"' " +
                    			"and InsuAccNo='"+tCSSRS.GetText(m,2)+"' and State = '0' ";
                        	String realbala = String.valueOf(Double.parseDouble(texesql.getOneValue(sql)) + tInsuAccPay);
                        	tLLAccClaimTraceSchema.setGrpContNo(tLCInsureAccTraceSchema.getGrpContNo());
                        	tLLAccClaimTraceSchema.setGrpPolNo(tLCInsureAccTraceSchema.getGrpPolNo());
                        	tLLAccClaimTraceSchema.setContNo(tLCInsureAccTraceSchema.getContNo());
                        	tLLAccClaimTraceSchema.setPolNo(tLCInsureAccTraceSchema.getPolNo());
                        	tLLAccClaimTraceSchema.setCaseNo("OtherNo");
                        	tLLAccClaimTraceSchema.setSerialNo(PubFun.getCurrentDate2()+PubFun1.CreateMaxNo("llaccclaimtrace",12));
                        	tLLAccClaimTraceSchema.setPatchNo(realbala);// 暂时存入理赔后的正确余额
                        	tLLAccClaimTraceSchema.setExeLocation("0");
                        	tLLAccClaimTraceSchema.setInsuAccNo(tLCInsureAccTraceSchema.getInsuAccNo());
                        	tLLAccClaimTraceSchema.setRiskCode(tLCInsureAccTraceSchema.getRiskCode());
                        	tLLAccClaimTraceSchema.setOtherNo(mRgtNo);
                        	tLLAccClaimTraceSchema.setOtherType("5");
                        	tLLAccClaimTraceSchema.setMoneyType(tLCInsureAccTraceSchema.getMoneyType());
                        	tLLAccClaimTraceSchema.setBeforeClaimMoney(tLCInsureAccDB.getSchema().getInsuAccBala());// 错误的理赔前余额
                        	tLLAccClaimTraceSchema.setAfterClaimMoney(tLCInsureAccDB.getSchema().getInsuAccBala() + tInsuAccPay);// 错误的理赔后余额
                        	tLLAccClaimTraceSchema.setOperator(mGlobalInput.Operator);
                        	tLLAccClaimTraceSchema.setMakeDate(PubFun.getCurrentDate());
                        	tLLAccClaimTraceSchema.setMakeTime(PubFun.getCurrentTime());
                        	tLLAccClaimTraceSchema.setModifyDate(PubFun.getCurrentDate());
                        	tLLAccClaimTraceSchema.setModifyTime(PubFun.getCurrentTime());
                        	tLLAccClaimTraceSet.add(tLLAccClaimTraceSchema);
                        }
//                         优化 #3284 特需险170106超赔问题  end
                        
                        if (tLCInsureAccDB.getSchema().getInsuAccBala() +
                                tInsuAccPay < 0&&!"1".equals(tCode)) {//2713补充A  修改  
                                CError.buildErr(this, "账户余额不足，无法给付！请回退重新理算");
                                return false;
                            }
                        //2713补充A**start**新增代码
                        if ("1".equals(tCode)){
	                       String tsql11 = "select caseno from llclaimdetail where rgtno='"+mRgtNo+"' and polno='"+tPolNo+"'";
	                       System.out.println("tsql11="+tsql11);
	                       SSRS ttCSSRS = texesql.execSQL(tsql11);
	                       for(int h=1;h<=ttCSSRS.getMaxRow();h++){
	                           	if(tInsuAccPay + LLCaseCommon.getAmntFromAcc(ttCSSRS.GetText(h,1), tPolNo, PubFun.getCurrentDate(), "JF") <0){
	                                   CError.buildErr(this, "赔付金额大于个人保险金额，无法给付！请回退重新理算");
	                                   return false;	
	                           	}
	                       }
                        }
                        //2713补充A**end**新增代码
                        if (tInsuAccPay!=0){
                            tPayMoney += tInsuAccPay;
                            System.out.println("tPayMoney"+tPayMoney);
                            System.out.println("tInsuAccPay"+tInsuAccPay);
                            LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccDB.getSchema();
                            tLCInsureAccSchema.setSumPaym(tLCInsureAccSchema.getSumPaym() - tInsuAccPay);
                            tLCInsureAccSchema.setInsuAccBala(tLCInsureAccSchema.getInsuAccBala()+tInsuAccPay);
                            tLCInsureAccSchema.setOperator(mGlobalInput.Operator);
                            tLCInsureAccSchema.setModifyDate(PubFun.getCurrentDate());
                            tLCInsureAccSchema.setModifyTime(PubFun.getCurrentTime());
                            sLCInsureAccSet.add(tLCInsureAccSchema);
                        }
                        
                    }   
                }
           
                if (Arith.round(tPayMoney,2)!=ipay){
                    CError.buildErr(this, "账户轨迹错误");
                    return false;
                }
            }

            LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
            tsql = "select * from lcinsureacctrace a where a.grpcontno='"+tGrpContNo
                 + "' and a.grppolno='"+tGrpPolNo+"' and exists (select 1 from llcase where a.otherno=caseno and rgtno='"
                 + mRgtNo+"') and state='temp' ";
            tLCInsureAccTraceSet = tLCInsureAccTraceDB.executeQuery(tsql);
            for (int n =1; n<=tLCInsureAccTraceSet.size(); n++){
                LCInsureAccTraceSchema tLCInsureAccTraceSchema = tLCInsureAccTraceSet.get(n);
                tLCInsureAccTraceSchema.setState("0");
                //2713补充A 赔医疗保险金PAYDATE为通知给付时间(mCurrentData),赔身故责任PAYDATE为身故日期**START修改
                String aRiskCode =tLCInsureAccTraceSchema.getRiskCode(); 
                // 3207 把需要设置paydate的险种 做成一个配置表，方便以后程序的修改
                String sql="select 1 from  ldcode where codetype='lprisk' and code ='"+aRiskCode+"' with ur";
                String aCode=texesql.getOneValue(sql);
                if("1".equals(aCode)){ //3207 
                	String tDeathSQL ="select deathdate from llcase where caseno ='"+tLCInsureAccTraceSchema.getOtherNo()+"' with ur";
                	String tDeathDate =texesql.getOneValue(tDeathSQL);
                	if (tDeathDate!=null && !"".equals(tDeathDate)){
                		tLCInsureAccTraceSchema.setPayDate(tDeathDate);
                	}else{
                	tLCInsureAccTraceSchema.setPayDate(PubFun.getCurrentDate());
                	}
                }
                //2713补充A 赔医疗保险金PAYDATE为通知给付时间(mCurrentData),赔身故责任PAYDATE为身故日期**END
                tLCInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
                tLCInsureAccTraceSchema.setModifyDate(PubFun.getCurrentDate());
                tLCInsureAccTraceSchema.setModifyTime(PubFun.getCurrentTime());
                sLCInsureAccTraceSet.add(tLCInsureAccTraceSchema);
            }


//            //每个GrpPolNo只对应一个团体账户
//            String gaccpol = "";
//            tsql =
//                    "select distinct b.insuaccno from lcinsureacc a,lmriskinsuacc b "
//                    +
//                    " where a.insuaccno=b.insuaccno and b.acctype='001' and grppolno='"
//                    + tGrpPolNo + "'";
//            String grpaccno = texesql.getOneValue(tsql);
//            tsql = "select * from lcinsureacc where insuaccno='" + grpaccno +
//                   "' and polno="
//                   +
//                   "(select a.polno from lcpol a where a.poltypeflag='2' and grppolno='"
//                   + tGrpPolNo + "')";
//            LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
//            LCInsureAccSet tInsureAccSet = tLCInsureAccDB.executeQuery(tsql);
//            LCInsureAccSchema tInsuAccSchema = new LCInsureAccSchema();
//            if (tInsureAccSet.size() <= 0) {
//                CError.buildErr(this, "数据错误，团体保单" + tGrpContNo + "没有对应的团体账户！");
////              return false;
//            } else {
//                tInsuAccSchema = tInsureAccSet.get(1);
//                gbala = tInsuAccSchema.getInsuAccBala();
//                gpay = tInsuAccSchema.getSumPaym();
//                gaccpol = tInsuAccSchema.getPolNo();
//            }
//            tsql = "select a.otherno,sum(a.pay) from ljsgetclaim a,llcase b "
//                   +
//                   " where b.caseno=a.otherno and b.rgtstate = '09' and a.pay>0  and b.rgtno='"
//                   + mRgtNo + "' and a.grppolno='" + tGrpPolNo + "' "
//                   + " group by a.otherno order by a.otherno";
//            SSRS pss = texesql.execSQL(tsql);
//            System.out.println(tsql);
//            boolean outpay = false;
//            double totalpay = 0;
//            for (int j = 1; j <= pss.getMaxRow(); j++) {
//                String tCaseNo = pss.GetText(j, 1);
//                double casepay = 0;
//                try {
//                    casepay = Double.parseDouble(pss.GetText(j, 2));
//                } catch (Exception ex) {
//                    System.out.println("案件" + tCaseNo + "的应付数据奇怪。。。");
//                    continue;
//                }
//                totalpay += casepay;
//                if (outpay) {
//                    continue;
//                }
//                double remnant = casepay;
//                tsql = "select * from lcinsureacctrace where otherno = '"
//                       + tCaseNo + "' and grppolno='" + tGrpPolNo
//                       + "' order by insuaccno desc ";
//                LCInsureAccTraceDB tAccTraceDB = new LCInsureAccTraceDB();
//                LCInsureAccTraceSet tAccTraceSet = new LCInsureAccTraceSet();
//                tAccTraceSet = tAccTraceDB.executeQuery(tsql);
//                if (tAccTraceSet.size() <= 0) {
//                    CError.buildErr(this, "案件" + tCaseNo
//                                    + "理算时发生错误，没有生成帐户轨迹，请回退重新理算！");
//                    return false;
//                }
//                String tinsuaccno = "";
//                boolean hasiacc = false;
//                for (int k = 1; k <= tAccTraceSet.size(); k++) {
//                    //理算时可能会产生多条轨迹（发生错误），但此处可以挽回，一个案件一个账户只需存一条轨迹即可。
//                    LCInsureAccTraceSchema tAccTraceSchema = new
//                            LCInsureAccTraceSchema();
//                    tAccTraceSchema = tAccTraceSet.get(k);
//                    if (tAccTraceSchema.getInsuAccNo().equals(tinsuaccno)) {
//                        continue;
//                    } else {
//                        tinsuaccno = tAccTraceSchema.getInsuAccNo();
//                    }
//                    double tracepay = 0;
//                    if (getAccType(tinsuaccno).equals("002")) {
//                        tLCInsureAccDB.setPolNo(tAccTraceSchema.getPolNo());
//                        tLCInsureAccDB.setInsuAccNo(tinsuaccno);
//                        if (!tLCInsureAccDB.getInfo()) {
//                            CError.buildErr(this, "查询不到轨迹对应账户信息！");
//                            return false;
//                        }
//                        double caccbala = tLCInsureAccDB.getInsuAccBala();
//                        ibala = caccbala;
//                        if (caccbala > remnant) {
//                            tracepay = ( -1) * remnant;
//                        } else {
//                            tracepay = ( -1) * caccbala;
//                        }
//                        caccbala += tracepay;
//                        remnant += tracepay;
//                        double sumpay = tLCInsureAccDB.getSumPaym() - tracepay;
//                        LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccDB.
//                                getSchema();
//                        tLCInsureAccSchema.setSumPaym(sumpay);
//                        tLCInsureAccSchema.setInsuAccBala(caccbala);
//                        tLCInsureAccSchema.setOperator(mGlobalInput.Operator);
//                        tLCInsureAccSchema.setModifyDate(PubFun.getCurrentDate());
//                        tLCInsureAccSchema.setModifyTime(PubFun.getCurrentTime());
//                        sLCInsureAccSet.add(tLCInsureAccSchema);
//                        hasiacc = true;
//                    } else {
//                        if (gbala > remnant) {
//                            tracepay = ( -1) * remnant;
//                        } else {
//                            tracepay = ( -1) * gbala;
//                        }
//                        gbala += tracepay;
//                        remnant += tracepay;
//                        gpay -= tracepay;
//                    }
//                    tAccTraceSchema.setMoney(tracepay);
//                    tAccTraceSchema.setState("0");
//                    tAccTraceSchema.setModifyDate(PubFun.getCurrentDate());
//                    tAccTraceSchema.setModifyTime(PubFun.getCurrentTime());
//                    sLCInsureAccTraceSet.add(tAccTraceSchema);
//                }
//                if (remnant > 0) {
//                    if (hasiacc) {
//                        CError.buildErr(this,
//                                        "案件" + tCaseNo + "账户余额不足，无法给付！目前个人账户余额"
//                                        + ibala + "元，团体账户余额" + gbala
//                                        + "元，本次需赔付" + casepay + "元。");
//                        return false;
//                    }
//                    outpay = true;
//                    continue;
//                }
//            }
//            if (outpay) {
//                CError.buildErr(this, "账户余额不足，无法给付！目前保单"
//                                + tGrpContNo + "团体账户余额"
//                                + tInsuAccSchema.getInsuAccBala()
//                                + "元，本次需赔付" + totalpay + "元。");
//                return false;
//            }
//            tInsuAccSchema.setSumPaym(gpay);
//            tInsuAccSchema.setInsuAccBala(gbala);
//            tInsuAccSchema.setOperator(mGlobalInput.Operator);
//            tInsuAccSchema.setModifyDate(PubFun.getCurrentDate());
//            tInsuAccSchema.setModifyTime(PubFun.getCurrentTime());
//            sLCInsureAccSet.add(tInsuAccSchema);
        }
        // 优化 #3284 特需险170106超赔问题  start
        map.put(tLLAccClaimTraceSet, "INSERT");
    	// 优化 #3284 特需险170106超赔问题  end
        map.put(sLCInsureAccTraceSet, "INSERT");
        map.put(sLCInsureAccSet, "UPDATE");
        return true;
    }

    private String getAccType(String tInsuAccNo) {
        String tType = "";
        LMRiskInsuAccDB triskaccdb = new LMRiskInsuAccDB();
        triskaccdb.setInsuAccNo(tInsuAccNo);
        if (triskaccdb.getInfo()) {
            tType = triskaccdb.getAccType();
        }
        return tType;
    }

    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ICaseCureBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错:" +
                                  ex.toString();
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    /**
     * 锁定案件给付
     * @param LockNoKey
     * @param tGlobalInput
     * @return
     */
    private MMap lockTable(String lockNoKey, GlobalInput globalInput)
    {
        MMap map = null;

        //案件给付类型：“LJ”
        String tLockNoType = "LJ";

        //锁定有效时间
        String tAIS = "60";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", lockNoKey);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(globalInput);
        tVData.add(tTransferData);

        LLLockTableActionBL tLockTableActionBL = new LLLockTableActionBL();
        map = tLockTableActionBL.getSubmitMap(tVData, null);
        if(map == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }

        return map;
    }
    
    /**
     * 扣除预付赔款
     * @return
     */
    private boolean dealPrapaid(LJAGetSchema aLJAGetSchema,
			LJAGetClaimSet aLJAGetClaimSet) {
		for (int i = 1; i <= aLJAGetClaimSet.size(); i++) {
			LJAGetClaimSchema tLJAGetClaimSchema = aLJAGetClaimSet.get(i);

			if ("00000000000000000000"
					.equals(tLJAGetClaimSchema.getGrpContNo())|| !mPrePaidFlag.equals("1")) {
				System.out.println("预付赔款个单不处理："
						+ tLJAGetClaimSchema.getContNo());
				continue;
			}

			LLPrepaidGrpContDB tLLPrepaidGrpContDB = new LLPrepaidGrpContDB();
			tLLPrepaidGrpContDB.setGrpContNo(tLJAGetClaimSchema.getGrpContNo());

			if (!tLLPrepaidGrpContDB.getInfo()) {
				continue;
			}

			boolean tLockFlag = true;

			for (int j = 1; j <= mLLPrepaidGrpContSet.size(); j++) {
				LLPrepaidGrpContSchema tLLPrepaidGrpContSchema = mLLPrepaidGrpContSet
						.get(j);
				if (tLLPrepaidGrpContSchema.getGrpContNo().equals(
						tLJAGetClaimSchema.getGrpContNo())) {
					tLockFlag = false;
					break;
				}
			}

			if (tLockFlag) {
				if (!LLCaseCommon.lockFY(tLJAGetClaimSchema.getGrpContNo(),
						mGlobalInput)) {
					CError.buildErr(this, "为防止并发操作，团体保单"
							+ tLJAGetClaimSchema.getGrpContNo() + "被锁定，请稍后操作");
					return false;
				}

				if (Arith.round(tLLPrepaidGrpContDB.getApplyAmount()
						+ tLLPrepaidGrpContDB.getRegainAmount()
						+ tLLPrepaidGrpContDB.getSumPay(), 2) != tLLPrepaidGrpContDB
						.getPrepaidBala()) {
					CError.buildErr(this, "保单"
							+ tLJAGetClaimSchema.getGrpContNo() + "预付赔款余额错误");
					return false;
				}

				mLLPrepaidGrpContSet.add(tLLPrepaidGrpContDB.getSchema());
			}

			boolean tTraceFlag = true;

			for (int j = 1; j <= mLLPrepaidTraceSet.size(); j++) {
				LLPrepaidTraceSchema tLLPrepaidTraceSchema = mLLPrepaidTraceSet
						.get(j);
				if (tLLPrepaidTraceSchema.getGrpContNo().equals(
						tLJAGetClaimSchema.getGrpContNo())
						&& tLLPrepaidTraceSchema.getGrpPolNo().equals(
								tLJAGetClaimSchema.getGrpPolNo())) {
					tLLPrepaidTraceSchema.setMoney(Arith.round(
							tLLPrepaidTraceSchema.getMoney()
									- tLJAGetClaimSchema.getPay(), 2));
					tTraceFlag = false;
					break;
				}
			}

			if (tTraceFlag) {
				LLPrepaidGrpPolDB tLLPrepaidGrpPolDB = new LLPrepaidGrpPolDB();
				tLLPrepaidGrpPolDB
						.setGrpPolNo(tLJAGetClaimSchema.getGrpPolNo());
				if (!tLLPrepaidGrpPolDB.getInfo()) {
					CError.buildErr(this, "保单"
							+ tLJAGetClaimSchema.getGrpContNo() + "下险种"
							+ tLJAGetClaimSchema.getRiskCode() + "预付信息查询失败");
					return false;
				}

				if (Arith.round(tLLPrepaidGrpPolDB.getApplyAmount()
						+ tLLPrepaidGrpPolDB.getRegainAmount()
						+ tLLPrepaidGrpPolDB.getSumPay(), 2) != tLLPrepaidGrpPolDB
						.getPrepaidBala()) {
					CError.buildErr(this, "保单"
							+ tLJAGetClaimSchema.getGrpContNo() + "下险种"
							+ tLJAGetClaimSchema.getRiskCode() + "预付赔款余额错误");
					return false;
				}

				mLLPrepaidGrpPolSet.add(tLLPrepaidGrpPolDB.getSchema());

				LLPrepaidTraceSchema tLLPrepaidTraceSchema = new LLPrepaidTraceSchema();

				String tSerialNo = PubFun1.CreateMaxNo("PREPAIDTRACENO",
						tLJAGetClaimSchema.getManageCom());

				Reflections tReflections = new Reflections();
				tReflections.transFields(tLLPrepaidTraceSchema,
						tLJAGetClaimSchema);
				tLLPrepaidTraceSchema.setSerialNo(tSerialNo);
				tLLPrepaidTraceSchema.setOtherNoType("C");
				tLLPrepaidTraceSchema.setBalaDate(PubFun.getCurrentDate());
				tLLPrepaidTraceSchema.setBalaTime(PubFun.getCurrentTime());
				tLLPrepaidTraceSchema.setMoneyType("PK");
				tLLPrepaidTraceSchema.setMoney(Arith.round(-tLJAGetClaimSchema
						.getPay(), 2));
				tLLPrepaidTraceSchema.setState("1");

				mLLPrepaidTraceSet.add(tLLPrepaidTraceSchema);
			}
		}

		if (mLLPrepaidTraceSet.size() != mLLPrepaidGrpPolSet.size()) {
			CError.buildErr(this, "预付险种轨迹查询错误");
			return false;
		}

		for (int j = 1; j <= mLLPrepaidTraceSet.size(); j++) {
			LLPrepaidTraceSchema tLLPrepaidTraceSchema = mLLPrepaidTraceSet
					.get(j);
			for (int m = 1; m <= mLLPrepaidGrpPolSet.size(); m++) {
				LLPrepaidGrpPolSchema tLLPrepaidGrpPolSchema = mLLPrepaidGrpPolSet
						.get(m);
				if (tLLPrepaidGrpPolSchema.getGrpPolNo().equals(
						tLLPrepaidTraceSchema.getGrpPolNo())) {
//					取消部分回销，部分走赔款的情况
					double tMoney = 0;
//					tMoney = tLLPrepaidGrpPolSchema.getPrepaidBala() > -tLLPrepaidTraceSchema
//							.getMoney() ? -tLLPrepaidTraceSchema.getMoney()
//							: tLLPrepaidGrpPolSchema.getPrepaidBala();
							
					if(tLLPrepaidGrpPolSchema.getPrepaidBala() < -tLLPrepaidTraceSchema.getMoney()){
						CError.buildErr(this, "团体保单下险种:"+tLLPrepaidGrpPolSchema.getGrpPolNo()+",预付赔款余额不足回销该案件/批次！");
						return false;
					}else{
						tMoney = -tLLPrepaidTraceSchema.getMoney();
					}
					tLLPrepaidTraceSchema.setLastBala(tLLPrepaidGrpPolSchema
							.getPrepaidBala());
					tLLPrepaidGrpPolSchema.setPrepaidBala(Arith
							.round(tLLPrepaidGrpPolSchema.getPrepaidBala()
									- tMoney, 2));
					tLLPrepaidTraceSchema.setMoney(Arith.round(-tMoney, 2));
					tLLPrepaidTraceSchema.setAfterBala(tLLPrepaidGrpPolSchema
							.getPrepaidBala());
					tLLPrepaidGrpPolSchema.setSumPay(Arith.round(
							tLLPrepaidGrpPolSchema.getSumPay()
									+ tLLPrepaidTraceSchema.getMoney(), 2));
					tLLPrepaidGrpPolSchema.setBalaDate(tLLPrepaidTraceSchema
							.getBalaDate());
					tLLPrepaidGrpPolSchema.setBalaTime(tLLPrepaidTraceSchema
							.getBalaTime());
					tLLPrepaidGrpPolSchema.setOperator(tLLPrepaidTraceSchema
							.getOperator());
					tLLPrepaidGrpPolSchema.setModifyDate(tLLPrepaidTraceSchema
							.getModifyDate());
					tLLPrepaidGrpPolSchema.setModifyTime(tLLPrepaidTraceSchema
							.getModifyTime());
					break;
				}
			}

			for (int m = 1; m <= mLLPrepaidGrpContSet.size(); m++) {
				LLPrepaidGrpContSchema tLLPrepaidGrpContSchema = mLLPrepaidGrpContSet
						.get(m);
				if (tLLPrepaidGrpContSchema.getGrpContNo().equals(
						tLLPrepaidTraceSchema.getGrpContNo())) {
					tLLPrepaidGrpContSchema.setPrepaidBala(Arith.round(
							tLLPrepaidGrpContSchema.getPrepaidBala()
									+ tLLPrepaidTraceSchema.getMoney(), 2));
					tLLPrepaidGrpContSchema.setSumPay(Arith.round(
							tLLPrepaidGrpContSchema.getSumPay()
									+ tLLPrepaidTraceSchema.getMoney(), 2));
					tLLPrepaidGrpContSchema.setBalaDate(tLLPrepaidTraceSchema
							.getBalaDate());
					tLLPrepaidGrpContSchema.setBalaTime(tLLPrepaidTraceSchema
							.getBalaTime());
					tLLPrepaidGrpContSchema.setOperator(tLLPrepaidTraceSchema
							.getOperator());
					tLLPrepaidGrpContSchema.setModifyDate(tLLPrepaidTraceSchema
							.getModifyDate());
					tLLPrepaidGrpContSchema.setModifyTime(tLLPrepaidTraceSchema
							.getModifyTime());
					break;
				}
			}
		}

		// 扣除的预付赔款金额
		double tSumMoney = 0;

		for (int l = 1; l <= mLLPrepaidTraceSet.size(); l++) {
			LLPrepaidTraceSchema tLLPrepaidTraceSchema = mLLPrepaidTraceSet
					.get(l);
			if (tLLPrepaidTraceSchema.getMoney() != 0) {
				mBackMsg += "<br>保单" + tLLPrepaidTraceSchema.getGrpContNo()
						+ "下险种" + tLLPrepaidTraceSchema.getRiskCode()
						+ ":预付赔款余额" + tLLPrepaidTraceSchema.getLastBala()
						+ ",本次回销预付赔款" + -tLLPrepaidTraceSchema.getMoney();

				LJAGetClaimSchema tLJAGetClaimSchema = new LJAGetClaimSchema();
				tLJAGetClaimSchema
						.setActuGetNo(aLJAGetSchema.getActuGetNo());
				tLJAGetClaimSchema.setFeeFinaType("YF");
				tLJAGetClaimSchema.setFeeOperationType("000");
				tLJAGetClaimSchema.setOtherNo(aLJAGetSchema.getOtherNo());
				tLJAGetClaimSchema.setOtherNoType("Y");
				tLJAGetClaimSchema.setGetDutyKind("000");
				tLJAGetClaimSchema.setGrpContNo(tLLPrepaidTraceSchema
						.getGrpContNo());
				tLJAGetClaimSchema.setContNo("00000" + l);
				tLJAGetClaimSchema.setGrpPolNo(tLLPrepaidTraceSchema
						.getGrpPolNo());
				tLJAGetClaimSchema.setPolNo("00000" + l);

				LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
				tLMRiskAppDB.setRiskCode(tLLPrepaidTraceSchema.getRiskCode());

				if (!tLMRiskAppDB.getInfo()) {
					CError.buildErr(this, "险种信息查询失败");
					return false;
				}

				tLJAGetClaimSchema.setRiskCode(tLMRiskAppDB.getRiskCode());
				tLJAGetClaimSchema.setRiskVersion(tLMRiskAppDB.getRiskVer());
				tLJAGetClaimSchema.setKindCode(tLMRiskAppDB.getKindCode());

				LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
				tLCGrpPolDB.setGrpPolNo(tLLPrepaidTraceSchema.getGrpPolNo());
				if (!tLCGrpPolDB.getInfo()) {
					LBGrpPolDB tLBGrpPolDB = new LBGrpPolDB();
					tLBGrpPolDB
							.setGrpPolNo(tLLPrepaidTraceSchema.getGrpPolNo());
					if (!tLBGrpPolDB.getInfo()) {
						CError.buildErr(this, tLLPrepaidTraceSchema
								.getGrpContNo()
								+ "保单查询失败");
						return false;
					}
					tLJAGetClaimSchema.setSaleChnl(tLBGrpPolDB.getSaleChnl());
					tLJAGetClaimSchema.setAgentCode(tLBGrpPolDB.getAgentCode());
					tLJAGetClaimSchema.setAgentGroup(tLBGrpPolDB
							.getAgentGroup());
					tLJAGetClaimSchema.setAgentCom(tLBGrpPolDB.getAgentCom());
					tLJAGetClaimSchema.setManageCom(tLBGrpPolDB.getManageCom());
					tLJAGetClaimSchema.setAgentType(tLBGrpPolDB.getAgentType());

				} else {
					tLJAGetClaimSchema.setSaleChnl(tLCGrpPolDB.getSaleChnl());
					tLJAGetClaimSchema.setAgentCode(tLCGrpPolDB.getAgentCode());
					tLJAGetClaimSchema.setAgentGroup(tLCGrpPolDB
							.getAgentGroup());
					tLJAGetClaimSchema.setAgentCom(tLCGrpPolDB.getAgentCom());
					tLJAGetClaimSchema.setManageCom(tLCGrpPolDB.getManageCom());
					tLJAGetClaimSchema.setAgentType(tLCGrpPolDB.getAgentType());
				}

				tLJAGetClaimSchema.setPay(tLLPrepaidTraceSchema.getMoney());
				tLJAGetClaimSchema.setOPConfirmCode(mGlobalInput.Operator);
				tLJAGetClaimSchema.setOPConfirmDate(aLJAGetSchema
						.getMakeDate());
				tLJAGetClaimSchema.setOPConfirmTime(aLJAGetSchema
						.getMakeTime());
				tLJAGetClaimSchema.setOperator(mGlobalInput.Operator);
				tLJAGetClaimSchema.setMakeDate(aLJAGetSchema.getMakeDate());
				tLJAGetClaimSchema.setMakeTime(aLJAGetSchema.getMakeTime());
				tLJAGetClaimSchema
						.setModifyDate(aLJAGetSchema.getMakeDate());
				tLJAGetClaimSchema
						.setModifyTime(aLJAGetSchema.getMakeTime());

				tSumMoney = Arith.round(
						tSumMoney + tLJAGetClaimSchema.getPay(), 2);

				aLJAGetClaimSet.add(tLJAGetClaimSchema);
			}
		}

		if (tSumMoney != 0) {
			aLJAGetSchema.setSumGetMoney(Arith.round(aLJAGetSchema
					.getSumGetMoney()
					+ tSumMoney, 2));
			mBackMsg += "<br>批次实际给付" + aLJAGetSchema.getSumGetMoney();
		}

		if (mLLPrepaidTraceSet.size() > 0) {
			map.put(mLLPrepaidTraceSet, "INSERT");
			map.put(mLLPrepaidGrpPolSet, "UPDATE");
			map.put(mLLPrepaidGrpContSet, "UPDATE");
		}

		return true;
	}
    
    public String getBackMsg() {
    	return mBackMsg;
    }

    public static void main(String[] args) {
        LLRegisterSchema tSchema = new LLRegisterSchema();
        tSchema.setRgtNo("P1100060728000003");

        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "8611";
        mGlobalInput.ComCode = "8611";
        mGlobalInput.Operator = "cm1102";
        VData aVData = new VData();
        aVData.addElement(mGlobalInput);
        aVData.add(tSchema);
        GrpGiveEnsureBL tBL = new GrpGiveEnsureBL();
        tBL.submitData(aVData, "GIVE|ENSURE");
    }


}
