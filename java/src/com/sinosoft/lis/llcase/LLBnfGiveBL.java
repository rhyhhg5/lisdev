/*
 * <p>ClassName: LLBnfGiveBL </p>
 * <p>Description: LLBnfGiveBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Author: Xx
 * @CreateDate：2006-07-06 15:27:43
 */
package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LLBnfGiveBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LLBnfSchema mLLBnfSchema = new LLBnfSchema();
    private LLBnfSet mLLBnfSet = new LLBnfSet();
    private String mCaseNo = "";
    public LLBnfGiveBL() {
    }

    public static void main(String[] args) {
        LLBnfGiveBL tLLBnfGiveBL= new LLBnfGiveBL();
        TransferData tTransferData = new TransferData();
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "8611";
        tG.Operator = "cm1101";
        VData tVData = new VData();
        tTransferData.setNameAndValue("CaseNo","C1100060710000006");
        // 准备传输数据 VData
        tVData.add(tTransferData);
        tVData.add(tG);
        System.out.println("begin");
        tLLBnfGiveBL.submitData(tVData, "DISPATCH||MAIN");
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * @cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLBnfGiveBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LLBnfGiveBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLBnfGiveBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("End LLBnfGiveBL Submit...");
        mInputData = null;
        return true;
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mCaseNo);
        if (!tLLCaseDB.getInfo()) {
            CError tError = new CError();
            tError.moduleName = "LLBnfGiveBL";
            tError.functionName = "DealData";
            tError.errorMessage = "案件信息查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!tLLCaseDB.getRgtState().equals("09")&&!tLLCaseDB.getRgtState().equals("04")
        &&!tLLCaseDB.getRgtState().equals("05")&&!tLLCaseDB.getRgtState().equals("06")
        &&!tLLCaseDB.getRgtState().equals("10")){
            CError.buildErr(this, "该状态无法为受益人分配赔款！");
            return false;
        }
        LJSGetSet saveLJSGetSet = new LJSGetSet();
        LJSGetSet delLJSGetSet = new LJSGetSet();
        LJSGetDB tLJSGetDB = new LJSGetDB();
        tLJSGetDB.setOtherNo(mCaseNo);
        tLJSGetDB.setOtherNoType("5");
        LJSGetSet tLJSGetSet = tLJSGetDB.query();
        tLJSGetDB.setOtherNoType("B");
        delLJSGetSet = tLJSGetDB.query();
        LJSGetSchema aLJSGetSchema = new LJSGetSchema();
        aLJSGetSchema = tLJSGetSet.get(1);
        LLBnfDB tLLBnfDB = new LLBnfDB();
        LLBnfSet tLLBnfSet = new LLBnfSet();
        tLLBnfDB.setCaseNo(mCaseNo);
        tLLBnfSet = tLLBnfDB.query();
        if (tLLBnfSet == null || tLLBnfSet.size() <= 0)
            return false;
        String ljsSql =
                "select sum(pay),polno from ljsgetclaim where otherno='"
                + mCaseNo + "' group by polno order by polno";
        SSRS tssrs = new SSRS();
        ExeSQL texesql = new ExeSQL();
        tssrs = texesql.execSQL(ljsSql);
        LJSGetClaimSet saveLJSGetClaimSet = new LJSGetClaimSet();
        LJSGetClaimSet updateLJSGetClaimSet = new LJSGetClaimSet();
        LJSGetClaimSet delLJSGetClaimSet = new LJSGetClaimSet();
        for (int i = 1; i <= tssrs.getMaxRow(); i++) {
            String apolno = tssrs.GetText(i, 2);
            LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB();
            LJSGetClaimSet tLJSGetClaimSet = new LJSGetClaimSet();
            tLJSGetClaimDB.setPolNo(apolno);
            tLJSGetClaimDB.setOtherNo(mCaseNo);
            tLJSGetClaimDB.setOtherNoType("5");
            tLJSGetClaimSet = tLJSGetClaimDB.query();
            tLLBnfDB = new LLBnfDB();
            tLLBnfDB.setCaseNo(mCaseNo);
            tLLBnfDB.setPolNo(apolno);
            tLLBnfDB.setBnfGrade("1");
            tLLBnfSet = new LLBnfSet();
            tLLBnfSet = tLLBnfDB.query();
            boolean IsAver = false;
            int bnfcount = tLLBnfSet.size();
            if (bnfcount <= 0) {
                //该条ljsgetclaim保留，钱不变，给付号不变
                continue;
            }else{
                if(tLLBnfSet.get(1).getBnfLot() < 0)
                    IsAver = true;
                //每个人生成一个给付号
                String[] tActugetNo = new String[bnfcount];
                double[] tSumGetMoney = new double[bnfcount];
                for (int k = 0; k < bnfcount; k++) {
                    String sNoLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
                    tActugetNo[k] = PubFun1.CreateMaxNo("GetNoticeNo", sNoLimit);
                    tLLBnfSet.get(k+1).setGetMoney(0);
                    tLLBnfSet.get(k+1).setCustomerNo(tActugetNo[k]); //用这个字段暂存给付号码
                }
                for(int j=1;j<=tLJSGetClaimSet.size();j++){
                    double tsumpay = tLJSGetClaimSet.get(j).getPay();
                    double tbnfpay = 0.0;
                    double tbnfpay_l = 0.0;
                    for (int k = 1; k < tLLBnfSet.size(); k++) {
                        if (IsAver) { //均分
                            tbnfpay = tsumpay / bnfcount;
                        } else { //比例分配
                            tbnfpay = tsumpay * tLLBnfSet.get(k).getBnfLot();
                        }
                        tbnfpay = Arith.round(tbnfpay, 2);
                        double nmoney = tLLBnfSet.get(k).getGetMoney()+tbnfpay;
                        tLLBnfSet.get(k).setGetMoney(nmoney);
                        LJSGetClaimSchema tLJSGetClaimSchema = new LJSGetClaimSchema();
                        tLJSGetClaimSchema.setSchema(tLJSGetClaimSet.get(j) );
                        tLJSGetClaimSchema.setGetNoticeNo(tActugetNo[k-1]);
                        tLJSGetClaimSchema.setPay(tbnfpay);
                        tLJSGetClaimSchema.setOtherNoType("B");
                        tLJSGetClaimSchema.setGetDutyCode("");
                        saveLJSGetClaimSet.add(tLJSGetClaimSchema);
                    }
                    //最后一个人得剩余赔款
                    tbnfpay_l = tsumpay - tbnfpay * (bnfcount - 1);
                    double nmoney_l = tLLBnfSet.get(bnfcount).getGetMoney()+tbnfpay_l;
                    tLLBnfSet.get(bnfcount).setGetMoney(nmoney_l);
                    LJSGetClaimSchema tLJSGetClaimSchema = new LJSGetClaimSchema();
                    tLJSGetClaimSchema.setSchema(tLJSGetClaimSet.get(j) );
                    tLJSGetClaimSchema.setGetNoticeNo(tActugetNo[bnfcount-1]);
                    tLJSGetClaimSchema.setPay(tbnfpay_l);
                    tLJSGetClaimSchema.setOtherNoType("B");
                    tLJSGetClaimSchema.setGetDutyCode("");
                    saveLJSGetClaimSet.add(tLJSGetClaimSchema);
                    mLLBnfSet.add(tLLBnfSet);
                    tLJSGetClaimSet.get(j).setGetDutyCode("BNF");
                    updateLJSGetClaimSet.add(tLJSGetClaimSet.get(j));
                }
            }
            for (int k = 1; k <= tLLBnfSet.size(); k++) {
                LLBnfSchema temLLBnfSchema = new LLBnfSchema();
                temLLBnfSchema.setSchema(tLLBnfSet.get(k));
                LJSGetSchema bnfLJSGetSchema = new LJSGetSchema();
                bnfLJSGetSchema.setSchema(aLJSGetSchema);
                bnfLJSGetSchema.setGetNoticeNo(temLLBnfSchema.getCustomerNo());
                bnfLJSGetSchema.setOtherNoType("B");
                bnfLJSGetSchema.setSumGetMoney(temLLBnfSchema.getGetMoney());
                bnfLJSGetSchema.setPayMode(temLLBnfSchema.getCaseGetMode());
                bnfLJSGetSchema.setDrawer(temLLBnfSchema.getName());
                bnfLJSGetSchema.setDrawerID(temLLBnfSchema.getIDNo());
                bnfLJSGetSchema.setAccName(temLLBnfSchema.getAccName());
                bnfLJSGetSchema.setBankCode(temLLBnfSchema.getBankCode());
                bnfLJSGetSchema.setBankAccNo(temLLBnfSchema.getBankAccNo());
                saveLJSGetSet.add(bnfLJSGetSchema);
            }
        }
        LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB();
        tLJSGetClaimDB.setOtherNo(mCaseNo);
        tLJSGetClaimDB.setOtherNoType("B");
        delLJSGetClaimSet = tLJSGetClaimDB.query();
        map.put(mLLBnfSet, "UPDATE");
        map.put(updateLJSGetClaimSet,"UPDATE");
        map.put(delLJSGetClaimSet,"DELETE");
        map.put(saveLJSGetClaimSet,"INSERT");
        map.put(delLJSGetSet,"DELETE");
        map.put(saveLJSGetSet,"INSERT");

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("begin getinputdata");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        TransferData aTransferData = (TransferData) cInputData.
                                     getObjectByObjectName("TransferData", 0);
        mCaseNo = (String) aTransferData.getValueByName("CaseNo");
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLLBnfSchema);
            this.mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLLBnfSchema);
        } catch (Exception ex) {

            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLBnfGiveBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
