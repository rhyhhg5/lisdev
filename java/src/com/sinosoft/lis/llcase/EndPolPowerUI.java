package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: 民生人寿业务系统</p>
 * <p>Description:立案阶段的保单效力终止程序
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @version 1.0
 */
public class EndPolPowerUI
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData = new VData();
  private VData mResult = new VData();
  private String mOperate;

  public EndPolPowerUI() {}
  public boolean submitData(VData cInputData,String cOperate)
  {
    this.mOperate = cOperate;
    EndPolPowerBL mEndPolPowerBL = new EndPolPowerBL();

    System.out.println("---UI BEGIN---");
    if (mEndPolPowerBL.submitData(cInputData,mOperate) == false)
    {
      this.mErrors.copyAllErrors(mEndPolPowerBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "EndPolPowerUI";
      tError.functionName = "submitData";
      tError.errorMessage = "保单效力终止失败！";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  public static void main(String[] args)
  {
  }
}