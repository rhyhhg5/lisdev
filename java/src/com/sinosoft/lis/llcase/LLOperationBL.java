package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HE
 * @version 1.0
 */

public class LLOperationBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData;
	private VData mResult = new VData();
	
	private String mOperate;
	private MMap tmpMap = new MMap();
	
	private GlobalInput mGlobalInput = new GlobalInput();
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
	//录入
    private LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
    private LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();
    //陪案类
    LLClaimSchema tLLClaimSchema;
    LLClaimSchema mLLClaimSchema = new LLClaimSchema();
    //保单明细类
    LLClaimDetailSchema tLLClaimDetailSchema;
    LLClaimDetailSchema mLLClaimDetailSchema = new LLClaimDetailSchema();
    //险种明细类
    LLClaimPolicySchema tLLClaimPolicySchema;
    LLClaimPolicySchema mLLClaimPolicySchema = new LLClaimPolicySchema();
      
    public LLOperationBL() {
    	
    }
    
    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData() {
    	if (mOperate.equals("UPDATE")){
		    System.out.println("---dealData---");
		    
		    //******赔付结论Update******//
		    if(tLLClaimSchema.getClmNo() != null) {
	    	    LLClaimDB tLLClaimDB = new LLClaimDB();
	    	    //设置Update条件
			    tLLClaimDB.setClmNo(tLLClaimSchema.getClmNo());
			    tLLClaimDB.setCaseNo(tLLClaimSchema.getCaseNo());
			    System.out.println("CaseNO=" +tLLClaimDB.getCaseNo());
			    LLClaimSet mLLClaimSet = tLLClaimDB.query();
			    
			    if(mLLClaimSet == null || mLLClaimSet.size() <= 0)
			    {
			    	this.mErrors.copyAllErrors(tLLClaimDB.mErrors);
	                CError tError = new CError();
	                tError.moduleName = "LLOperationBL";
	                tError.functionName = "dealData";
	                tError.errorMessage = "给付责任编码" +
	                tLLClaimSchema.getClmNo() +
	                                      "出错，需查询信息后修改！";
	                this.mErrors.addOneError(tError);
	                return false;
			    }
			    
			    for(int i = 1; i <= mLLClaimSet.size(); i++ ) {
				    mLLClaimSchema = mLLClaimSet.get(i);
				    //添加修改字段
				    mLLClaimSchema.setGiveType(tLLClaimSchema.getGiveType());
				    mLLClaimSchema.setGiveTypeDesc(tLLClaimSchema.getGiveTypeDesc());
				    mLLClaimSchema.setModifyDate(CurrentDate);
				    mLLClaimSchema.setModifyTime(CurrentTime);
				    tmpMap.put(mLLClaimSchema,"UPDATE");
			    }
		    }
		   
		    //*****保单明细Update*******//
			for(int i = 1; i <= tLLClaimDetailSet.size(); i++) {
				tLLClaimDetailSchema = tLLClaimDetailSet.get(i);
				LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
				//System.out.println(tLLClaimDetailSchema.getGetDutyCode());
				//设置Update条件
				tLLClaimDetailDB.setCaseNo(tLLClaimDetailSchema.getCaseNo());
				tLLClaimDetailDB.setGetDutyCode(tLLClaimDetailSchema.getGetDutyCode());
				LLClaimDetailSet mLLClaimDetailSet = tLLClaimDetailDB.query();
				
				if (mLLClaimDetailSet == null || mLLClaimDetailSet.size() <= 0) {
	                this.mErrors.copyAllErrors(tLLClaimDetailDB.mErrors);
	                CError tError = new CError();
	                tError.moduleName = "LLOperationBL";
	                tError.functionName = "dealData";
	                tError.errorMessage = "给付责任编码" +
	                	tLLClaimDetailSchema.getGetDutyCode() +
	                                      "出错，需查询信息后修改！";
	                this.mErrors.addOneError(tError);
	                return false;
	            }
				for(int j = 1; j <= mLLClaimDetailSet.size(); j++) {
					//添加修改字段
					mLLClaimDetailSchema = mLLClaimDetailSet.get(j);
					mLLClaimDetailSchema.setGiveTypeDesc(tLLClaimDetailSchema.getGiveTypeDesc());
					mLLClaimDetailSchema.setGiveType(tLLClaimDetailSchema.getGiveType());
					mLLClaimDetailSchema.setGiveReasonDesc(tLLClaimDetailSchema.getGiveReasonDesc());
					mLLClaimDetailSchema.setGiveReason(tLLClaimDetailSchema.getGiveReason());
					mLLClaimDetailSchema.setModifyDate(CurrentDate);
					mLLClaimDetailSchema.setModifyTime(CurrentTime);
					tmpMap.put( mLLClaimDetailSchema, "UPDATE");
				}				
		    }
			
			
			//***********险种明细Update*********//
			for(int i = 1; i <= tLLClaimPolicySet.size(); i++)
			{
				tLLClaimPolicySchema = tLLClaimPolicySet.get(i);			
			    LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
			    //设置Update条件
			    tLLClaimPolicyDB.setCaseNo(tLLClaimPolicySchema.getCaseNo());
			    tLLClaimPolicyDB.setPolNo(tLLClaimPolicySchema.getPolNo());
			    LLClaimPolicySet mLLClaimPolicySet = tLLClaimPolicyDB.query();
			    if (mLLClaimPolicySet == null || mLLClaimPolicySet.size() <= 0) {
	                this.mErrors.copyAllErrors(tLLClaimPolicyDB.mErrors);
	                CError tError = new CError();
	                tError.moduleName = "LLOperationBL";
	                tError.functionName = "dealData";
	                tError.errorMessage = "保单号" +
	                	tLLClaimPolicySchema.getPolNo() +
	                                      "出错，需查询信息后修改！";
	                this.mErrors.addOneError(tError);
	                return false;
	            }
			    for(int j = 1; j <= mLLClaimPolicySet.size(); j++) {
			    	mLLClaimPolicySchema = mLLClaimPolicySet.get(j);
				    //添加修改字段
				    mLLClaimPolicySchema.setGiveTypeDesc(tLLClaimPolicySchema.getGiveTypeDesc());
				    mLLClaimPolicySchema.setGiveType(tLLClaimPolicySchema.getGiveType());
				    mLLClaimPolicySchema.setGiveReasonDesc(tLLClaimPolicySchema.getGiveReasonDesc());
				    mLLClaimPolicySchema.setGiveReason(tLLClaimPolicySchema.getGiveReason());
				    mLLClaimPolicySchema.setModifyDate(CurrentDate);
				    mLLClaimPolicySchema.setModifyTime(CurrentTime);
					tmpMap.put(mLLClaimPolicySchema, "UPDATE");
			    }
		    }
			
			//判断是否有修改字段，有执行Update，无返回信息给予提示
		    if(tmpMap.size() > 0) {
		    	System.out.println("tmpMap" + tmpMap);
				this.mResult.add(tmpMap);
		    } else {
		    	CError tError = new CError();
	            tError.moduleName = "LLOperationBL";
	            tError.functionName = "dealData";
	            tError.errorMessage = "数据没有修改!";
	            this.mErrors.addOneError(tError);
	            return false;
		    }
    	}
    	return true;
    }
    
	 /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
	public boolean submitData(VData cInputData, String cOperate) {
        this.mOperate = cOperate;
        
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
   
      //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLOperationBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LLOperationBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
      //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        
	    PubSubmit tPubSubmit = new PubSubmit();
	    System.out.println("Start LLOperation Submit...");
	    if (!tPubSubmit.submitData(mInputData, mOperate)) {
	        // @@错误处理
	        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
	        CError tError = new CError();
	        tError.moduleName = "BlackListPersonBL";
	        tError.functionName = "submitData";
	        tError.errorMessage = "数据提交失败!";
	        this.mErrors.addOneError(tError);
	        return false;
	    }
	        return true;
	}
	
	/**
     * 从输入数据中得到所有对象
     */
	public boolean getInputData(VData cInputData){
		tLLClaimSchema = (LLClaimSchema) cInputData.getObjectByObjectName("LLClaimSchema", 0);
		tLLClaimPolicySet = (LLClaimPolicySet) cInputData.getObjectByObjectName("LLClaimPolicySet", 0);
		tLLClaimDetailSet = (LLClaimDetailSet) cInputData.getObjectByObjectName("LLClaimDetailSet", 0);
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		return true;
	}
	
    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        try {
            mInputData = new VData();
            this.mInputData.add(tmpMap);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLOperationBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    
    public VData getResult()
    {
        return mResult;
    }

}




















