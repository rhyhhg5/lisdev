/*
 * @(#)ICaseCureBL.java	2005-02-20
 *
 * Copyright 2005 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.llcase;

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;
import java.text.DecimalFormat;

/*
 * <p>Title: Web业务系统</p>
 * <p>ClassName:ICaseCureBL </p>
 * <p>Description: 理赔案件-账单录入 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft </p>
 * @author：zhangtao
 * @version：1.0
 * @CreateDate：2005-02-20
 */
public class ICaseSecuBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private MMap map = new MMap();
    private String mOperate = "";
    private String MngCom = "86110000";

    //全局数据
    private GlobalInput mGlobalInput = new GlobalInput();
    //账单信息
    private LLFeeMainSet mFeeMainSet = new LLFeeMainSet();
    private LLFeeMainSchema mFeeMainSchema = new LLFeeMainSchema();
    private LLFeeMainSchema sFeeMainSchema = new LLFeeMainSchema();
    //账单费用明细
    private LLCaseReceiptSet mReceiptSet = new LLCaseReceiptSet();
    private LLSecurityReceiptSchema mLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
    private LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
    private LLSecuDetailSet mLLSecuDetailSet = new LLSecuDetailSet();
    private String mCaseNo = "";

    public ICaseSecuBL()
    {
    }

    public static void main(String[] args)
    {

        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "cm0001";
        tGI.ManageCom = "86";
        TransferData CN = new TransferData();
        CN.setNameAndValue("CaseNo","C1100060206000015");
        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(CN);
        ICaseSecuBL tICaseSecuBL = new ICaseSecuBL();

         tICaseSecuBL.submitData(tVData, "INSERT");

        //用于调试
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData 输入数据
     * @param cOperate String 操作类型
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        //得到输入数据
        if (!getInputData())
        {
            return false;
        }
//          mOperate = cOperate;
        //检查数据合法性
        if (!checkInputData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData(cOperate))
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ICaseCureBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有数据对象
     * @return boolean
     */
    private boolean getInputData()
    {
        System.out.println("getInputData()..."+mOperate);

        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        TransferData tCN = (TransferData) mInputData.getObjectByObjectName("TransferData",0);
        mCaseNo = (String) tCN.getValueByName("CaseNo");
        return true;
    }

    /**
     * 对输入数据做必要的校验
     * @return boolean
     */
    private boolean checkInputData()
    {
        System.out.println("checkInputData()..."+mOperate);
        try
        {
            LLCaseDB tLLCaseDB = new LLCaseDB();
            tLLCaseDB.setCaseNo(mCaseNo);
            if(!tLLCaseDB.getInfo()){
                CError tError = new CError();
                tError.moduleName = "ICaseSecuBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "案件信息查询失败，理赔号："+mCaseNo;
                this.mErrors.addOneError(tError);
                return false;
            }
            String RgtState  =tLLCaseDB.getRgtState();
            if(!(RgtState.equals("01")||RgtState.equals("02")||RgtState.equals("08")))
            {
                CError tError = new CError();
                tError.moduleName = "ICaseCureBL";
                tError.functionName = "checkInputData";
                tError.errorMessage = "该案件状态下不能修改账单信息！";
                mErrors.addOneError(tError);
                return false;
            }
            String sHandler = ""+tLLCaseDB.getHandler();
//            if (!sHandler.equals(mGlobalInput.Operator)) {
//                //当前操作者不是指定处理人
//                CError tError = new CError();
//                tError.moduleName = "ICaseCureBL";
//                tError.functionName = "checkInputData";
//                tError.errorMessage = "对不起，您没有权限处理该案件!" +
//                                      "指定处理人：" + sHandler;
//                mErrors.addOneError(tError);
//                return false;
//            }
            LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
            String tsql = "select * from llfeemain where caseno='"+mCaseNo
                          +"' and feeatti='0' and feetype='1' order by mainfeeno";
            mFeeMainSet = tLLFeeMainDB.executeQuery(tsql);
            if (mFeeMainSet.size()<=0)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ICaseCureBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有得到足够的数据，请您确认有：门诊医院账单信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLLFeeMainDB = new LLFeeMainDB();
            tLLFeeMainDB.setCaseNo(mCaseNo);
            tLLFeeMainDB.setReceiptNo(mCaseNo);
            LLFeeMainSet tLLFeeMainSet = tLLFeeMainDB.query();
            if (tLLFeeMainSet.size()>0)
            {
              //数据更新
              sFeeMainSchema = tLLFeeMainSet.get(1);
              mOperate = "UPDATE";
              System.out.println("账单更新");
            }else{
              mOperate = "INSERT";
            }

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ICaseCureBL";
            tError.functionName = "checkInputData";
            tError.errorMessage = "在校验输入的数据时出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @param cOperate String 操作类型
     * @return boolean
     */
    private boolean dealData(String cOperate) {
        System.out.println("dealData()..." + mOperate);
        boolean tReturn = false;

        //保存录入
        //创建账单记录
        String tempFeeNo = mFeeMainSet.get(1).getMainFeeNo();
        String tHospitalCode = mFeeMainSet.get(1).getHospitalCode();
        mFeeMainSchema = mFeeMainSet.get(1);
        double tApplyAmnt = 0;
        double tSelfPay2 = 0;
        double tSelfAmnt = 0;
        double tFeeInSecu = 0;
        for (int i = 1; i <= mFeeMainSet.size(); i++) {
            LLFeeMainSchema temLLFeeMainSchema = new LLFeeMainSchema();
            temLLFeeMainSchema = mFeeMainSet.get(i);
            if (tempFeeNo.compareTo(temLLFeeMainSchema.getMainFeeNo()) < 0) {
                mFeeMainSchema = temLLFeeMainSchema;
                tempFeeNo = temLLFeeMainSchema.getMainFeeNo();
            }
            if (!tHospitalCode.equals(temLLFeeMainSchema.getHospitalCode())) {
                mFeeMainSchema.setHospitalCode("1100088");
                mFeeMainSchema.setHospitalName("汇总医院");
            }
            LLCaseReceiptSet tReceiptSet = new LLCaseReceiptSet();
            LLCaseReceiptDB tLLCaseReceiptDB = new LLCaseReceiptDB();
            tLLCaseReceiptDB.setMainFeeNo(tempFeeNo);
            tReceiptSet = tLLCaseReceiptDB.query();
            for (int k = 1; k <= tReceiptSet.size(); k++) {
                LLCaseReceiptSchema tReceiptSchema = new LLCaseReceiptSchema();
                tReceiptSchema = tReceiptSet.get(k);
                tApplyAmnt += tReceiptSchema.getFee();
                tSelfAmnt += tReceiptSchema.getSelfAmnt();
                tSelfPay2 += tReceiptSchema.getPreAmnt();
            }
        }
        tFeeInSecu = tApplyAmnt - tSelfAmnt - tSelfPay2;
        String tMainFeeNo = "";
        if (mOperate.equals("INSERT")) {
            String tLimit = PubFun.getNoLimit("86");
            tMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO", tLimit);
        } else {
            tMainFeeNo = sFeeMainSchema.getMainFeeNo();
        }
        mFeeMainSchema.setMainFeeNo(tMainFeeNo);
        mFeeMainSchema.setFeeAtti("2");
        mFeeMainSchema.setReceiptNo(mCaseNo);
        mFeeMainSchema.setMngCom(mGlobalInput.ManageCom);
        mFeeMainSchema.setOperator(mGlobalInput.Operator);
        mFeeMainSchema.setMakeDate(PubFun.getCurrentDate());
        mFeeMainSchema.setMakeTime(PubFun.getCurrentTime());
        mFeeMainSchema.setModifyDate(mFeeMainSchema.getMakeDate());
        mFeeMainSchema.setModifyTime(mFeeMainSchema.getMakeTime());

        double tsumFee = 0;
        if (mFeeMainSchema.getSumFee() < 0.01)
            mFeeMainSchema.setSumFee(tsumFee);

        mLLSecurityReceiptSchema.setFeeDetailNo(tMainFeeNo);
        mLLSecurityReceiptSchema.setMainFeeNo(tMainFeeNo);
        mLLSecurityReceiptSchema.setCaseNo(mCaseNo);
        mLLSecurityReceiptSchema.setRgtNo(mFeeMainSchema.getRgtNo());
        mLLSecurityReceiptSchema.setPayReceipt(mFeeMainSet.size());
        mLLSecurityReceiptSchema.setApplyAmnt(tApplyAmnt);
        mLLSecurityReceiptSchema.setFeeInSecu(tFeeInSecu);
        mLLSecurityReceiptSchema.setFeeOutSecu(tSelfAmnt + tSelfPay2);
        mLLSecurityReceiptSchema.setSelfAmnt(tSelfAmnt);
        mLLSecurityReceiptSchema.setSelfPay2(tSelfPay2);
        mLLSecurityReceiptSchema.setLowAmnt(0.0);
        mLLSecurityReceiptSchema.setMidAmnt(0.0);
        mLLSecurityReceiptSchema.setHighAmnt1(0.0);
        mLLSecurityReceiptSchema.setHighAmnt2(0.0);
        if(!calSecuDuty())
            return false;
        mLLSecurityReceiptSchema.setMngCom(mGlobalInput.ManageCom);
        mLLSecurityReceiptSchema.setOperator(mGlobalInput.Operator);
        mLLSecurityReceiptSchema.setMakeDate(PubFun.getCurrentDate());
        mLLSecurityReceiptSchema.setMakeTime(PubFun.getCurrentTime());
        mLLSecurityReceiptSchema.setModifyDate(mLLSecurityReceiptSchema.
                                               getMakeDate());
        mLLSecurityReceiptSchema.setModifyTime(mLLSecurityReceiptSchema.
                                               getMakeTime());
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mFeeMainSchema.getCaseNo());
        if (tLLCaseDB.getInfo() && !tLLCaseDB.getMngCom().equals("86"))
            MngCom = tLLCaseDB.getMngCom();
        map.put(mLLSecurityReceiptSchema, "DELETE&INSERT");
        map.put(mLLSecuDetailSet, "DELETE&INSERT");

        //创建账单费用明细记录
        mLLCaseOpTimeSchema.setCaseNo(mFeeMainSchema.getCaseNo());
        mLLCaseOpTimeSchema.setRgtState("03");
        mLLCaseOpTimeSchema.setOperator(mGlobalInput.Operator);
        mLLCaseOpTimeSchema.setManageCom(mGlobalInput.ManageCom);
        LLCaseCommon tLLCaseCommon = new LLCaseCommon();
        LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(
                mLLCaseOpTimeSchema);
        map.put(tLLCaseOpTimeSchema, "DELETE&INSERT");

        map.put(mFeeMainSchema, "DELETE&INSERT");

        mResult.add(mFeeMainSchema);
        tReturn = true;

        return tReturn;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        System.out.println("prepareOutputData()...");

        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ICaseCureBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 计算社保各段赔付
     * @return boolean
     */
private boolean calSecuDuty()
{
   //初始化变量
   double TA = 0.0; //本次住院医疗费用总额
   double SI = 0.0; //本次住院医保范围内金额
   double PM = 0.0; //本次住院自费金额
   double PSI = 0.0; //本次住院统筹支付金额
   double PL = 0.0; //本次住院大额互助支付金额
   double A = 0.0; //本次甲类费用总额，待用
   double SO = 0.0;//本次住院医保范围外金额
   double SP1 = 0.0;//本次住院自付一金额
   double SP2 = 0.0;//本次住院自付二金额

   String tLevelCode = "";
   TA += mLLSecurityReceiptSchema.getApplyAmnt();
   SI += mLLSecurityReceiptSchema.getFeeInSecu();
   SO += mLLSecurityReceiptSchema.getFeeOutSecu();
   PM += mLLSecurityReceiptSchema.getSelfAmnt();
   PSI += mLLSecurityReceiptSchema.getPlanFee();
   PL += mLLSecurityReceiptSchema.getSupInHosFee();
   SP1 += mLLSecurityReceiptSchema.getSelfPay1();
   SP2 += mLLSecurityReceiptSchema.getSelfPay2();
   mFeeMainSchema.setSumFee(mLLSecurityReceiptSchema.getApplyAmnt());

   tLevelCode = mFeeMainSchema.getHosGrade().substring(0,1); //医院等级只分一二三等，不细分到甲乙丙丁
   //取社保计算要素
   LLSecurityFactorSet tLLSecurityFactorSet = new LLSecurityFactorSet();
   //社保住院类

       //计算门诊费用
       double DFee = 0.0; //门诊总费用
       double DGL = 0.0; //门诊起付限
       double DPL = 0.0; //大额门诊费用
       double TYSDF = 0.0;  //本次理赔前年度大额门诊总费用
       double FIS = 0.0;//社保内费用
       double FOS = 0.0;//社保外费用
       double YSDF = 0.0;//年度大额门诊费用累计金额
       //给初始变量赋值
       TYSDF = mLLSecurityReceiptSchema.getTotalSupDoorFee();
       FIS = mLLSecurityReceiptSchema.getFeeInSecu();
       FOS = mLLSecurityReceiptSchema.getFeeOutSecu();
       YSDF = mLLSecurityReceiptSchema.getYearSupDoorFee();
       SP2 = mLLSecurityReceiptSchema.getSelfPay2();
       PM = mLLSecurityReceiptSchema.getSelfAmnt();

       String tisustat = mFeeMainSchema.getInsuredStat();
       if (mFeeMainSchema.getAge() >= 70 &&
           mFeeMainSchema.getInsuredStat().equals("2"))
           tisustat = "3";

       //查询门诊社保政策
       LLSecurityFactorDB tLLSecurityFactorDB = new LLSecurityFactorDB();
       tLLSecurityFactorDB.setSecurityType("2001");
       tLLSecurityFactorDB.setInsuredStat(tisustat);
       tLLSecurityFactorDB.setComCode(MngCom.substring(0,4));
       tLLSecurityFactorSet = tLLSecurityFactorDB.query( );
       if (tLLSecurityFactorSet != null &&
           tLLSecurityFactorSet.size() != 0) {
           DGL = tLLSecurityFactorSet.get(1).getGetLimit();
           double dpeakline = tLLSecurityFactorSet.get(1).getPeakLine();
           double dgetrate = tLLSecurityFactorSet.get(1).getGetRate();
           double sDoorPay = 0.0; //小额门急诊保险金
           double bDoorPay = 0.0; //大额门急诊保险金
           double cDoorPay = 0.0; //门急诊保险金

           if(FIS<DGL){
               mLLSecurityReceiptSchema.setSelfPay1(FIS);
               sDoorPay = FIS;
               bDoorPay = 0;
               cDoorPay = FIS;
           }
           else {
               CError tError = new CError();
               tError.moduleName = "ICaseCureBL";
               tError.functionName = "calSecuDuty";
               tError.errorMessage = "已超过社保起付限，请先到社保进行结算！";
               mErrors.addOneError(tError);
               return false;
//               double planfee = (FIS-DGL)*dgetrate;
//               if(planfee>dpeakline){
//                   planfee = dpeakline;
//               }
//               mLLSecurityReceiptSchema.setSelfPay1(FIS-planfee);
//               mLLSecurityReceiptSchema.setSupDoorFee(planfee);
//               sDoorPay = DGL;
//               bDoorPay = FIS-planfee-DGL;
//               cDoorPay = mLLSecurityReceiptSchema.getSelfPay1();
           }
           LLSecuDetailSchema tLLSecuDetailSchema = new LLSecuDetailSchema();
           tLLSecuDetailSchema.setDownLimit(0);
           tLLSecuDetailSchema.setUpLimit(tLLSecurityFactorSet.get(1).getGetLimit());
           tLLSecuDetailSchema.setPlanPayRate(0);
           tLLSecuDetailSchema.setSerialNo("1");
           fillSecuDetailBasic(tLLSecuDetailSchema);
           tLLSecuDetailSchema.setPlanFee(0);
           tLLSecuDetailSchema.setSelfPay(Arith.round(sDoorPay,2));
           mLLSecuDetailSet.add(tLLSecuDetailSchema);
           tLLSecuDetailSchema = new LLSecuDetailSchema();
           tLLSecuDetailSchema.setUpLimit(tLLSecurityFactorSet.get(1).getPeakLine());
           tLLSecuDetailSchema.setDownLimit(tLLSecurityFactorSet.get(1).getGetLimit());
           tLLSecuDetailSchema.setPlanPayRate(dgetrate);
           tLLSecuDetailSchema.setSerialNo("2");
           fillSecuDetailBasic(tLLSecuDetailSchema);
           tLLSecuDetailSchema.setPlanFee(Arith.round(DPL,2));
           tLLSecuDetailSchema.setSelfPay(Arith.round(bDoorPay,2));
           mLLSecuDetailSet.add(tLLSecuDetailSchema);
           mLLSecurityReceiptSchema.setSmallDoorPay(sDoorPay);
           mLLSecurityReceiptSchema.setEmergencyPay(cDoorPay);
           mLLSecurityReceiptSchema.setHighDoorAmnt(bDoorPay);
           mLLSecurityReceiptSchema.setGetLimit(DGL);
           System.out.println(mLLSecurityReceiptSchema.getHighDoorAmnt());
        }
   return true;
}
private LLSecuDetailSchema fillSecuDetailBasic(LLSecuDetailSchema tLLSecuDetailSchema)
    {
        tLLSecuDetailSchema.setCaseNo(mFeeMainSchema.getCaseNo());
        tLLSecuDetailSchema.setRgtNo(mFeeMainSchema.getRgtNo());
        tLLSecuDetailSchema.setMainFeeNo(mFeeMainSchema.getMainFeeNo());
        tLLSecuDetailSchema.setMngCom(mGlobalInput.ManageCom);
        tLLSecuDetailSchema.setOperator(mGlobalInput.Operator);
        tLLSecuDetailSchema.setMakeDate(PubFun.getCurrentDate());
        tLLSecuDetailSchema.setMakeTime(PubFun.getCurrentTime());
        tLLSecuDetailSchema.setModifyDate(tLLSecuDetailSchema.getMakeDate());
        tLLSecuDetailSchema.setModifyTime(tLLSecuDetailSchema.getMakeTime());
        return tLLSecuDetailSchema;
    }
    /**
     * 得到处理后的结果数据
     * @return boolean
     */
    public VData getResult()
    {
        return this.mResult;
    }

}
