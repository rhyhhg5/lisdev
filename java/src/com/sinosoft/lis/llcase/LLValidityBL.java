package com.sinosoft.lis.llcase;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LICaseTimeRemindSet;
import com.sinosoft.lis.vschema.LLBnfSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class LLValidityBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();


    /** 全局数据 */
    private GlobalInput mG = new GlobalInput();

    private String mOperate;

    private LICaseTimeRemindSchema mLICaseTimeRemindSchema = new LICaseTimeRemindSchema();
    private LICaseTimeRemindSet tLICaseTimeRemindSet = new LICaseTimeRemindSet();
    private LICaseTimeRemindSet aLICaseTimeRemindSet = new LICaseTimeRemindSet();

    public LLValidityBL() {
    }
    
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * @cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError.buildErr(this, "数据处理失败ClientRegisterBL-->dealData!");
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
    	
    	this.tLICaseTimeRemindSet.set((LICaseTimeRemindSet)cInputData.getObjectByObjectName("LICaseTimeRemindSet", 0));
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
    	//校验个人证件有效期提醒
    	int i;
    	for (i = 1; i <= this.tLICaseTimeRemindSet.size(); ++i) {
    		mLICaseTimeRemindSchema = this.tLICaseTimeRemindSet.get(i);
    		String endDate = mLICaseTimeRemindSchema.getIDEndDate();
            PubFun pf = new PubFun();
            String toDate = pf.getCurrentDate();
            int number = pf.calInterval(toDate,endDate, "D");
            if(number<0){
            	mLICaseTimeRemindSchema.setother1("已过期");
            }
            if(number>0&&number<90){
            	mLICaseTimeRemindSchema.setother1("即将过期");
            }
            aLICaseTimeRemindSet.add(mLICaseTimeRemindSchema);
    	}
    	
    	map.put(aLICaseTimeRemindSet, "INSERT");
    	this.mInputData.clear();
        mInputData.add(map);
    	PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
              // @@错误处理
              CError.buildErr(this, "数据提交失败!");
              return false;
        }
        return true;
    }

  

    public VData getResult() {
        return this.mResult;
    }

}
