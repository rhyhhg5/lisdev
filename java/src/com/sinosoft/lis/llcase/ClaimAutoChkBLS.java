package com.sinosoft.lis.llcase;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 理赔自动核赔业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 */

public class ClaimAutoChkBLS
{
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();
  private String mOperate;

  public ClaimAutoChkBLS() {}


  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;
    this.mOperate = cOperate;
    System.out.print("save claimsavebls data begin");
    if (!this.saveData())
      return false;
    System.out.println("End ClaimSave BLS Submit...");
    mInputData=null;
    return true;
  }



  private boolean saveData()
  {
  	LLClaimErrorSet tLLClaimErrorSet = new LLClaimErrorSet();
        LLClaimPolicySchema tLLClaimPolicySchema = new LLClaimPolicySchema();
        LLClaimSchema tLLClaimSchema = new LLClaimSchema();

 	tLLClaimErrorSet = (LLClaimErrorSet)mInputData.getObjectByObjectName("LLClaimErrorSet",0);
        tLLClaimPolicySchema = (LLClaimPolicySchema)mInputData.getObjectByObjectName("LLClaimPolicySchema",0);
        tLLClaimSchema = (LLClaimSchema)mInputData.getObjectByObjectName("LLClaimSchema",0);


    	Connection conn = null;
    	conn = DBConnPool.getConnection();
    	if (conn==null)
    	{
      		// @@错误处理
      		CError tError = new CError();
      		tError.moduleName = "ClaimAutoChkBLS";
      		tError.functionName = "saveData";
      		tError.errorMessage = "数据库连接失败!";
      		this.mErrors .addOneError(tError) ;
      		return false;
    	}

    	try
    	{
              conn.setAutoCommit(false);
              // 保存现在的关联
              //删除保单已有核赔错误信息
              if (this.mOperate.equals("AutoChkPol"))
              {

                LLClaimErrorDB tLLClaimErrorDB = new LLClaimErrorDB(conn);
//                tLLClaimErrorDB.setClmNo(tLLClaimPolicySchema.getClmNo());
                tLLClaimErrorDB.setPolNo(tLLClaimPolicySchema.getPolNo());
                if (tLLClaimErrorDB.deleteSQL() == false)
                {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLLClaimErrorDB.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "ClaimAutoChkBLS";
                  tError.functionName = "saveData";
                  tError.errorMessage = "删除数据失败!";
                  this.mErrors .addOneError(tError) ;
                  conn.rollback() ;
                  conn.close();
                  return false;
                }

                LLClaimErrorDBSet tLLClaimErrorDBSet = new LLClaimErrorDBSet( conn );
                tLLClaimErrorDBSet.set(tLLClaimErrorSet);
                System.out.println("insert or update LLClaimError begin...");
                System.out.println("size=="+tLLClaimErrorDBSet.size());
                if (tLLClaimErrorDBSet.insert() == false)
                {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLLClaimErrorDBSet.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "ClaimAutoChkBLS";
                  tError.functionName = "saveData";
                  tError.errorMessage = "保存数据失败!";
                  this.mErrors .addOneError(tError) ;
                  conn.rollback() ;
                  conn.close();
                  return false;
                }

                LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB(conn);
                tLLClaimPolicyDB.setSchema(tLLClaimPolicySchema);
                if (tLLClaimPolicyDB.update() == false)
                {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLLClaimPolicyDB.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "ClaimAutoChkBLS";
                  tError.functionName = "saveData";
                  tError.errorMessage = "保存数据失败!";
                  this.mErrors .addOneError(tError) ;
                  conn.rollback() ;
                  conn.close();
                  return false;
                }
              }
              else  //保存案件自动核赔信息
              {
                LLClaimErrorDB tLLClaimErrorDB = new LLClaimErrorDB(conn);
//                tLLClaimErrorDB.setClmNo(tLLClaimSchema.getClmNo());
                tLLClaimErrorDB.setPolNo("000000");
                if (tLLClaimErrorDB.deleteSQL() == false)
                {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLLClaimErrorDB.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "ClaimAutoChkBLS";
                  tError.functionName = "saveData";
                  tError.errorMessage = "删除数据失败!";
                  this.mErrors .addOneError(tError) ;
                  conn.rollback() ;
                  conn.close();
                  return false;
                }

                LLClaimErrorDBSet tLLClaimErrorDBSet = new LLClaimErrorDBSet( conn );
                tLLClaimErrorDBSet.set(tLLClaimErrorSet);
                System.out.println("insert or update LLClaimError begin...");
                System.out.println("size=="+tLLClaimErrorDBSet.size());
                if (tLLClaimErrorDBSet.insert() == false)
                {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLLClaimErrorDBSet.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "ClaimAutoChkBLS";
                  tError.functionName = "saveData";
                  tError.errorMessage = "保存数据失败!";
                  this.mErrors .addOneError(tError) ;
                  conn.rollback() ;
                  conn.close();
                  return false;
                }

              }



              conn.commit();
              conn.close();
            } // end of try
            catch (Exception ex)
            {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "ClaimAutoChkBLS";
              tError.functionName = "submitData";
              tError.errorMessage = ex.toString();
              this.mErrors .addOneError(tError);
              try{conn.rollback() ;} catch(Exception e){}

              return false;
	}
    return true;
  }

}
