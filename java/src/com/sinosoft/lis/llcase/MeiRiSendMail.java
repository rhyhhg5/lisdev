package com.sinosoft.lis.llcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.Transport;
import javax.mail.internet.MimeMultipart;

import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;

public class MeiRiSendMail {

	/** 收件人地址 */
	private String toAddress;
	/** 抄送人地址 */
	private String ccAddress;
	/** 暗抄人地址 */
	private String bccAddress;
	/** 传送对象 */
	private Transport mTransport;
	/** 邮件发送服务类 */
	private MailSender mMailSender;
	/** 邮件内容 */
	private String mBody;
	/** 邮件标题 */
	private String mSubject;
	/** 邮件体 */
	private BodyPart messageBodyPart;
	/** 发送邮件 */
	private Multipart multipart = new MimeMultipart();
	/** 添加附件 */
	private String mFile;

	public CErrors mErrors = new CErrors();

	/**
	 * 发送邮件构造器
	 * @return 
	 */
	public void SendMail() {
		try {
			//查询邮件发送者相关信息
			String tSQLMailSenderSql = "select code,codename from ldcode where codetype = 'lpsagentmailsender' ";
			SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSenderSql);
			System.out.println("邮件发送-邮件密码："+tSSRS.GetText(1, 2));
		    System.out.println("邮件发送-邮件账户："+tSSRS.GetText(1, 1));
			mMailSender = new MailSender(tSSRS.GetText(1, 1), tSSRS.GetText(1, 2),"picchealth");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * 发送邮件
	 * 
	 * @return boolean
	 */
	public boolean send(String body, String subject) {
		this.mBody = StrTool.unicodeToGBK(body);
		this.mSubject = subject;
		
		Date date=new Date();
		final SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd"); 
		String today=df.format(date);

		try {
			//标题，内容，附件(多附件可以用|分隔，末尾|可加可不加)
			mMailSender.setSendInf(mSubject+"_"+today+"_全部",mBody,getFile());
	
			//开始发送邮件
			mMailSender.setToAddress(getToAddress(),getCcAddress(),getBccAddress());
			
			if (!mMailSender.sendMail()) {
				System.out.println(mMailSender.getErrorMessage());
			}
		}catch(Exception ex){
			this.mErrors.addOneError(new CError("邮件发送类SagentMail出错",
					"SagentMail", "send"));
			ex.toString();
			ex.printStackTrace();
		}
		
		return true;
	}




	/**
	 * 获得收件人地址
	 * 
	 * @return 
	 */
	public String getToAddress() {
		return toAddress;
	}

	/**
	 * 获得抄送人地址
	 * 
	 * @return 
	 */
	public String getCcAddress() {
		return ccAddress;
	}
	
	/**
	 * 获得暗抄人地址
	 * 
	 * @return 
	 */
	public String getBccAddress() {
		return bccAddress;
	}

	/**
	 * 添加暗抄人地址
	 * 
	 * @param bccAddress
	 *            
	 */
	public void setBccAddress(String bccAddress) {
		this.bccAddress = bccAddress;
	}

	/**
	 * 添加收件人地址
	 * 
	 * @param toAddress
	 *            
	 */
	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	/**
	 * 添加抄送人地址
	 * 
	 * @param ccAddress
	 *           
	 */
	public void setCcAddress(String ccAddress) {
		this.ccAddress = ccAddress;
	}

	public void setFile(String mFile) {
		this.mFile = StrTool.unicodeToGBK(mFile);
	}



	public static void main(String[] args) {
//		SagentMail tSendMail = new SagentMail();
//		String address = "houyadong14320@sinosoft.com.cn;hydsxsy@126.com";
//		try {
//			tSendMail.SendMail();
//			tSendMail.setToAddress(address);
			//tSendMail.setToAddress(address);
			//tSendMail.setToAddress("jingbaobaoccc@hotmail.com");
//			tSendMail.setCcAddress("hydsxsy@126.com");
			//tSendMail.setBccAddress("zhangjialong@sinosoft.com.cn");
//		} catch (Exception ex) {
//			System.out.println("434::::"+ex.toString());
//		}
//		try {
//			tSendMail.setFile("");
//			tSendMail.send("<b>中文</b>", "测试");
//		} catch (Exception ex) {
//			System.out.println("440::::"+ex.toString());
//			ex.printStackTrace();
//		}
	}

	public String getFile() {
		return mFile;
	}
}
