package com.sinosoft.lis.llcase;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 案件－立案－分案保单明细保存的数据库处理类</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author 刘岩松
 * @version 1.0
 */

public class CasePolicyBLS
{
  private VData mInputData ;
  public  CErrors mErrors = new CErrors();

  public String mOperate;
  private String CaseNo = "";
  LLCasePolicySet mLLCasePolicySet = new  LLCasePolicySet();

  public CasePolicyBLS() {}

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;
    if(cOperate.equals("INSERT"))
    {
      mLLCasePolicySet=(LLCasePolicySet)mInputData.getObjectByObjectName("LLCasePolicySet",0);
    }
    if(cOperate.equals("DELETE"))
    {
      CaseNo = (String)mInputData.get(1);
    }

    if (cOperate.equals("INSERT"))
    {
      //在同一事务中处理两个表的操作要把下条语句添加上
      try
      {
        if (!this.saveData())
          return false;
      }
      catch(Exception ex)
      {
      }
      return true;
    }

    if(cOperate.equals("DELETE"))
    {
      deleteData();
    }
    mInputData=(VData)cInputData.clone();
    String mOperate;
    mOperate=cOperate;
    System.out.println("Start ReportDelete BLS Submit...");
    System.out.println("End ReportDelete BLS Submit...");
    mInputData=null;
    return true;
  }

private boolean deleteData()
{
  Connection conn = DBConnPool.getConnection();
  if (conn==null)
  {
    CError tError = new CError();
    tError.moduleName = "CasePolicyBLS";
    tError.functionName = "saveData";
    tError.errorMessage = "数据库连接失败!";
    this.mErrors .addOneError(tError) ;
    return false;
  }
  try
  {
    conn.setAutoCommit(false);
    LLCasePolicyDB tLLCasePolicyDB = new LLCasePolicyDB(conn);

    tLLCasePolicyDB.setCaseNo(CaseNo);
    tLLCasePolicyDB.deleteSQL();
    System.out.println("删除结束");
    conn.commit() ;
    conn.close();
  }
  catch (Exception ex)
  {
    CError tError = new CError();
    tError.moduleName = "CasePolicyBLS";
    tError.functionName = "submitData";
    tError.errorMessage = ex.toString();
    this.mErrors .addOneError(tError);
    try
    {
      conn.rollback() ;
      conn.close();
    }
    catch(Exception e)
    {
    }
    return false;
  }
  return true;
}
  private boolean saveData()
  {
    Connection conn = DBConnPool.getConnection();
    if (conn==null)
    {
      CError tError = new CError();
      tError.moduleName = "CasePolicyBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    try
    {
      conn.setAutoCommit(false);
      LLCasePolicyDB tLLCasePolicyDB = new LLCasePolicyDB(conn);
      LLCasePolicySchema tLLCasePolicySchema = mLLCasePolicySet.get(1);
      tLLCasePolicyDB.setCaseNo(tLLCasePolicySchema.getCaseNo());
      tLLCasePolicyDB.setPolType("1");
      if (!tLLCasePolicyDB.deleteSQL())
      {
        this.mErrors.copyAllErrors(tLLCasePolicyDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "LLCasePolicyBL";
        tError.functionName = "DeleteData";
        tError.errorMessage = "删除失败!";
        this.mErrors.addOneError(tError);
        conn.rollback() ;
        conn.close();
        return false;
      }

      LLCasePolicyDBSet mLLCasePolicyDBSet=new LLCasePolicyDBSet(conn);
      mLLCasePolicyDBSet.set(mLLCasePolicySet);
      if (mLLCasePolicyDBSet.insert() == false)
      {
        this.mErrors.copyAllErrors(mLLCasePolicyDBSet.mErrors);
        CError tError = new CError();
        tError.moduleName = "ReportBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "保存数据失败!";
        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
        return false;
      }
      conn.commit() ;
      conn.close();
    }
    catch (Exception ex)
    {
      CError tError = new CError();
      tError.moduleName = "CasePolicyBLS";
      tError.functionName = "submitData";
      tError.errorMessage = ex.toString();
      this.mErrors .addOneError(tError);
      try
      {
        conn.rollback() ;
        conn.close();
      }
      catch(Exception e)
      {
      }
      return false;
    }
    return true;
  }
}
