package com.sinosoft.lis.llcase;



import com.sinosoft.httpclient.dto.pty004.request.PTY004Request;
import com.sinosoft.httpclient.dto.pty004.request.RequestNode;
import com.sinosoft.httpclient.dto.pty004.response.PTY004Response;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.db.LBAppntDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title: 既往理赔信息查询
 * </p>
 * <p>
 * Description:根据保单号获取既往理赔信息查询
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author : xyp
 * @version 1.0
 */
public class SyZhBXQuery {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 执行sql语句查询的类 */
	private ExeSQL tExeSQL = new ExeSQL();

	/** 携带数据的类 */
	private TransferData tempTransferData = new TransferData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 用于存储传入的保单号 */
	private String mContNo;

	/** 用于存储既往理赔信息 */
	private String mResult;
	/** 年度累计赔付金额*/
	private double annualClaimSa;
	/** 终身累计赔付金额*/
	private double wholeLifeClaimSa; 

	/**
	 * 提供无参的构造方法
	 */
	public SyZhBXQuery() {
	}

	/**
	 * 获取税优识别码等的方法
	 */
	public boolean submitData(String tContNo,String operator) {
		// 将操作数据拷贝到本类中
		this.mContNo = tContNo;
		this.mOperate = operator;
		if (!dealData()) {
			return false;
		}

		return true;
	}

	/**
	 * 获取传入的方法
	 */
	public boolean getInputData() {
		/**
		 * 接收传入的数据
		 */
		tempTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		mContNo = (String) tempTransferData.getValueByName("ContNo");
		System.out.println(mContNo);
		if (mContNo == null && mContNo.equals("")) {
			CError tCError = new CError();
			tCError.moduleName = "SyZhBXQuery";
			tCError.functionName = "getMessage";
			tCError.errorMessage = "传入的投保单号为空！";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}

	/**
	 * 获取税优识别码发送报文的方法
	 */
	public boolean dealData() {
		/**
		 * 封装请求报文dto 上传保单号，从中保信获取税优识别码
		 */
		LCAppntDB mLCAppntDB = new LCAppntDB();
		LBAppntDB mLBAppntDB = null;
		mLCAppntDB.setContNo(mContNo);
		if (!mLCAppntDB.getInfo()) {
			//#3530 理赔模块失效保单被保人信息查询功能优化
			mLBAppntDB = new LBAppntDB();  // c表拿不到数据，就去b表，都拿不到返回false
			mLBAppntDB.setContNo(mContNo);
			if(!mLBAppntDB.getInfo()){
				CError tCError = new CError();
				tCError.moduleName = "SyZhBXQuery";
				tCError.functionName = "dealData";
				tCError.errorMessage = "投保人信息查询失败！";
				this.mErrors.addOneError(tCError);
				return false;
			}			
		}

		LCContSubDB mLCContSubDB = new LCContSubDB();
		mLCContSubDB.setPrtNo(mLCAppntDB.getPrtNo());
		if (!mLCContSubDB.getInfo()) {	
			mLCContSubDB.setPrtNo(mLBAppntDB.getPrtNo());
			if(!mLCContSubDB.getInfo()){
				CError tCError = new CError();
				tCError.moduleName = "SyZhBXQuery";
				tCError.functionName = "dealData";
				tCError.errorMessage = "税优信息查询失败！";
				this.mErrors.addOneError(tCError);
				return false;
			}		
		}

		// String birthday = tExeSQL.execSQL(
		// "select to_char('" + mLCAppntDB.getAppntBirthday()
		// + "','yyyy-mm-dd') from dual where 1=1 ").GetText(1, 1);

		PTY004Request tPTY004Request = new PTY004Request();
		RequestNode tRequestNode = new RequestNode();
		tRequestNode.setBusiNo("1");
		if(mLCAppntDB.getInfo()){
			tRequestNode.setName(mLCAppntDB.getAppntName());
			tRequestNode.setGender(mLCAppntDB.getAppntSex());
			tRequestNode.setCertiType(mLCAppntDB.getIDType());
			tRequestNode.setCertiNo(mLCAppntDB.getIDNo());
			tRequestNode.setBirthday(mLCAppntDB.getAppntBirthday());
			tRequestNode.setProposalNo(mLCAppntDB.getPrtNo());
		}else{		
			tRequestNode.setName(mLBAppntDB.getAppntName());
			tRequestNode.setGender(mLBAppntDB.getAppntSex());
			tRequestNode.setCertiType(mLBAppntDB.getIDType());
			tRequestNode.setCertiNo(mLBAppntDB.getIDNo());
			tRequestNode.setBirthday(mLBAppntDB.getAppntBirthday());
			tRequestNode.setProposalNo(mLBAppntDB.getPrtNo());
		}	
		//#3530 end
		tRequestNode.setPolicySource(mLCContSubDB.getTransFlag());		
		tPTY004Request.getRequestNodes().getRequestNode().add(tRequestNode);
		/** 生成xml的头部信息 */
		try {
			String responseXml = CommunicateServiceImpl.post(tPTY004Request,
					mOperate);
			/** 获取结果 */
			PTY004Response tPTY004Response = (PTY004Response) XmlParseUtil
					.xmlToDto(responseXml, PTY004Response.class);
			/**
			 * 获取返回结果，并对返回的结果进行判断 1：如果responseCode不等于"0000",表示处理失败,
			 */
			String responseCode = tPTY004Response.getHead().getResponseCode();
			String resultStatus = tPTY004Response.getResponseNodes()
					.getResponseNode().get(0).getResult().getResultStatus();
			/**
			 * 添加上处理失败的情况下的详细信息
			 */
			String resultInfoDesc = tPTY004Response.getResponseNodes()
					.getResponseNode().get(0).getResult().getResultInfoDesc();
			String ciitcResultMessage = tPTY004Response.getResponseNodes()
					.getResponseNode().get(0).getResult()
					.getCiitcResultMessage();

			if (responseCode != null && !responseCode.equals("")
					&& responseCode.equals("0000")) {
				if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("00")) {
					System.out.println("系统正常，业务成功处理！");

					annualClaimSa= java.lang.Double.parseDouble(tPTY004Response.getResponseNodes()
							.getResponseNode().get(0)
							.getBusinessObject().getAnnualClaimSa());
					
					wholeLifeClaimSa= java.lang.Double.parseDouble(tPTY004Response.getResponseNodes()
							.getResponseNode().get(0)
							.getBusinessObject().getWholeLifeClaimSa());
					System.out.println("年度累计赔付金额："+annualClaimSa);
					System.out.println("终身累计赔付金额："+wholeLifeClaimSa);

				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("01")) {
					System.out.println("系统正常，等待异步返回!");
					CError tCError = new CError();
					tCError.moduleName = "SyZhBXQuery";
					tCError.functionName = "SyZhBXQuery";
					tCError.errorMessage = "系统正常，等待异步返回!";
					this.mErrors.addOneError(tCError);
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("02")) {
					System.out
							.println("系统正常，数据校验不通过!原因为：" + ciitcResultMessage);
					CError tCError = new CError();
					tCError.moduleName = "SyZhBXQuery";
					tCError.functionName = "SyZhBXQuery";
					tCError.errorMessage = "系统正常，数据校验不通过!原因为："
							+ ciitcResultMessage;
					this.mErrors.addOneError(tCError);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("03")) {
					System.out.println("系统正常，业务处理失败!原因为：" + ciitcResultMessage);
					CError tCError = new CError();
					tCError.moduleName = "SyZhBXQuery";
					tCError.functionName = "SyZhBXQuery";
					tCError.errorMessage = "系统正常，业务处理失败!原因为："
							+ ciitcResultMessage;
					this.mErrors.addOneError(tCError);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("99")) {
					System.out.println("系统异常！");
					CError tCError = new CError();
					tCError.moduleName = "SyZhBXQuery";
					tCError.functionName = "SyZhBXQuery";
					tCError.errorMessage = "系统正常，业务处理失败!原因为："
							+ ciitcResultMessage;
					this.mErrors.addOneError(tCError);
					return false;
				}
				return true;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			CError tCError = new CError();
			tCError.moduleName = "SyZhBXQuery";
			tCError.functionName = "SyZhBXQuery";
			tCError.errorMessage = "处理异常!";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}

	
	
	/**获得年度累计赔付金额*/
	public double getAnnualClaimSa() {
		return this.annualClaimSa;
	}
	/**获得终身累计赔付金额*/
	public double getWholeLifeClaimSa() {
		return this.wholeLifeClaimSa;
	}

	public CErrors getCErrors(){
		return this.mErrors;
	}
	/**
	 * 测试用
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("ContNo", "014059968000002");
		tVData.add(tTransferData);
		SyZhBXQuery tSyZhBXQuery = new SyZhBXQuery();
		tSyZhBXQuery.submitData("014065507000001", "ZBXPT");
		System.out.println(tSyZhBXQuery.getAnnualClaimSa());
		System.out.println(tSyZhBXQuery.getWholeLifeClaimSa());


	}
}

