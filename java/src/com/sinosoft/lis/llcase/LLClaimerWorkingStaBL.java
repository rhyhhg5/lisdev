package com.sinosoft.lis.llcase;


import java.util.Date;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LLClaimerWorkingStaBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 全局变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mManageCom = "";

	private String mStartDate = "";

	private String mEndDate = "";

	private String mOperator = "";

	private String tCurrentDate = "";

	private VData mInputData = new VData();

	private String mOperate = "";

	private PubFun mPubFun = new PubFun();

	private ListTable mListTable = new ListTable();

	private TransferData mTransferData = new TransferData();

	private XmlExport mXmlExport = null;

	private String mManageComNam = "";
	
	private String mFileNameB = "";
	
    private String moperator = "";
    
    private String mMakeDate = "";
	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		Date d = new Date();
		mOperate = cOperate;
		mInputData = (VData) cInputData;
		if (mOperate.equals("")) {
			this.bulidErrorB("submitData", "数据不完整");
			return false;
		}

		if (!mOperate.equals("PRINT")) {
			this.bulidErrorB("submitData", "数据不完整");
			return false;
		}

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(mInputData)) {
			return false;
		}
		System.out.println("BBBXXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
				+ this.mManageCom);

		// 进行数据查询
		if (!queryData()) {
			return false;
		}else{
    		TransferData tTransferData= new TransferData();
    		tTransferData.setNameAndValue("tFileNameB",mFileNameB );
    		tTransferData.setNameAndValue("tMakeDate", mMakeDate);
    		tTransferData.setNameAndValue("tOperator", moperator);
    		LLPrintSave tLLPrintSave = new LLPrintSave();
    		VData tVData = new VData();
    		tVData.addElement(tTransferData);
    		if(!tLLPrintSave.submitData(tVData,"")){
    			return false;
    		     }
    	}
		Date d1 = new Date();

		System.out.println("dayinchenggong1232121212121"+d+"  "+d1);

		return true;
	}

	private void bulidErrorB(String cFunction, String cErrorMsg) {

		CError tCError = new CError();

		tCError.moduleName = "LLCLaimerWorkingStaBL";
		tCError.functionName = cFunction;
		tCError.errorMessage = cErrorMsg;

		this.mErrors.addOneError(tCError);

	}

	/**
	 * 取得传入的数据 如果没有传入管理机构和起止如期则查全部机构全年的信息
	 * 
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		// tCurrentDate = mPubFun.getCurrentDate();
		try {
			mGlobalInput.setSchema((GlobalInput) cInputData
					.getObjectByObjectName("GlobalInput", 0));
			mTransferData = (TransferData) cInputData.getObjectByObjectName(
					"TransferData", 0);
			// 页面传入的数据 三个
			mOperator = (String) mTransferData.getValueByName("tOperator");
			this.mManageCom = (String) mTransferData
					.getValueByName("tManageCom");

			System.out.println("XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXX"
					+ this.mManageCom);
			this.mStartDate = (String) mTransferData
					.getValueByName("tStartDate");
			this.mEndDate = (String) mTransferData.getValueByName("tEndDate");
			if (mManageCom == null || mManageCom.equals("")) {
				this.mManageCom = "86";
			}
			if (mStartDate == null || mStartDate.equals("")) {
				this.mStartDate = getYear(tCurrentDate) + "-01-01";
			}
			if (mEndDate == null || mEndDate.equals("")) {
				this.mEndDate = getYear(tCurrentDate) + "-12-31";
			}

			System.out
					.println("XXXXXXXXXXXXXXXXXXXcom name XXXXXXXXXXXXXXLLLLLLLLLLLLLLLLLLLL"
							+ mManageCom);
			mFileNameB = (String)mTransferData.getValueByName("tFileNameB");
			
	        moperator = mGlobalInput.Operator;
	        
	        mMakeDate=PubFun.getCurrentDate();
	        
		} catch (Exception ex) {
			this.mErrors.addOneError("");
			return false;
		}
		return true;
	}

	private boolean queryData() {
		TextTag tTextTag = new TextTag();
		mXmlExport = new XmlExport();
		// 设置模版名称
		mXmlExport.createDocument("LLClaimerWorkingSta.vts", "printer");

		String tMakeDate = "";
		tMakeDate = mPubFun.getCurrentDate();

		System.out.print("dayin252");
		String sql = "select name from ldcom where comcode='" + mManageCom
				+ "'";
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		String mManageComName = tExeSQL.getOneValue(sql);
		tTextTag.add("ManageComName", mManageComName);
		tTextTag.add("StartDate", mStartDate);
		tTextTag.add("EndDate", mEndDate);
		tTextTag.add("MakeDate", tMakeDate);
		tTextTag.add("operator", mOperator);
		System.out.println("1212121" + tMakeDate);
		if (tTextTag.size() < 1) {
			return false;
		}
		mXmlExport.addTextTag(tTextTag);
		String[] title = { "", "", "", "" };
		mListTable.setName("ENDOR");
		if (!getDataList()) {
			return false;
		}
		if (mListTable == null || mListTable.equals("")) {
			bulidErrorB("getDataList", "没有符合条件的信息!");
			return false;
		}

		System.out.println("111");
		mXmlExport.addListTable(mListTable, title);
		System.out.println("121");
		mXmlExport.outputDocumentToFile("c:\\", "new1");
		this.mResult.clear();
		mResult.addElement(mXmlExport);

		return true;
	}

	private String getYear(String pmDate) {
		String mYear = "";
		String tSQL = "";
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(tSQL);
		mYear = tSSRS.GetText(1, 1);
		return mYear;
	}

	private boolean getDataList() {

		SSRS tSSRS = new SSRS();
		String sql_claimer = "select distinct claimer from llcase l where mngcom like '"
				+ mManageCom
				+ "%' and claimer is not null and ReceiptFlag='1' and exists (select 1 from llfeemain f where caseno=l.caseno and makedate >= '"
				+ mStartDate + "' and modifydate<='"
				+ mEndDate
				+ "' and not exists (select caseno from llcase where caseno=f.caseno and caseprop='06' )) with ur";
		System.out.println("heoewjowehfdihieeehh~~~~~~~~~~~~~~~~~~~~~~~~~~"
				+ sql_claimer);
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(sql_claimer);
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			String ListInfo[] = new String[25];
			String claimer = tSSRS.GetText(i, 1);
			String FeeNum = getSumFeeN(claimer);
			String CaseNum = getSumCase(claimer);
			String AvgCaseEfficient = getAvgCaseEfficient(claimer);
			if ("0".equals(FeeNum) && "0".equals(CaseNum)
					&& AvgCaseEfficient == null) {
			} else {
				ListInfo[0] = claimer;
				ListInfo[1] = FeeNum;
				ListInfo[2] = CaseNum;
				ListInfo[3] = AvgCaseEfficient;

				System.out.println(claimer + ":" + FeeNum + "   " + CaseNum
						+ "   " + AvgCaseEfficient);
				mListTable.add(ListInfo);
			}
		}

		return true;
	}

	private String getSumFeeN(String claimer) {
		String tSQL = "select  count(mainfeeno) from llfeemain f where not exists (select caseno from llcase where caseno=f.caseno and caseprop='06' ) and makedate >= '"
				+ mStartDate
				+ "' and modifydate<='"
				+ mEndDate
				+ "' and exists (select 1 from llcase where caseno=f.caseno and claimer='"
				+ claimer + "') with ur";
		System.out.println(tSQL);
		ExeSQL tExeSQL = new ExeSQL();
		String sumCase = tExeSQL.getOneValue(tSQL);
		System.out.println(sumCase);
		return sumCase;
	}

	private String getSumCase(String claimer) {
		String sql = "select count(caseno) from (select caseno,min(makedate) a,max(modifydate) b from llfeemain f where not exists (select caseno from llcase where caseno=f.caseno and caseprop='06') and exists (select 1 from llcase where caseno=f.caseno and claimer='"
				+ claimer
				+ "') group by caseno ) as x where a >='"
				+ mStartDate + "' and b <='" + mEndDate + "' with ur";
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		String result = tExeSQL.getOneValue(sql);
		System.out.println(result);
		return result;
	}

	private String getAvgCaseEfficient(String claimer) {
		String sql = "select DECIMAL(DECIMAL(sum(a+b))/60,10,2) "
				+ "from (select caseno,mainfeeno,coalesce((days(modifydate)-days(makedate)),0)* 86400 a,coalesce((MIDNIGHT_SECONDS(modifytime)-MIDNIGHT_SECONDS(maketime)),0 ) b"
				+ " from llfeemain f where caseno in (select caseno from (select caseno,min(makedate) a,max(modifydate) b from llfeemain f where not exists (select caseno from llcase where caseno=f.caseno and caseprop='06' ) and exists (select 1 from llcase where caseno=f.caseno and claimer='"
				+ claimer + "')group by caseno ) as x where a >='" + mStartDate
				+ "' and b <='" + mEndDate + "' )) as x  with ur ";
		System.out.println(sql);
		ExeSQL tExeSQL = new ExeSQL();
		String result = tExeSQL.getOneValue(sql);
		System.out.println(result);
		return result;

	}

	public VData getResult() {
		return mResult;
	}

	public static void main(String[] args) {
		System.out.println("hello");
	}

}
