/*
 * <p>ClassName: OLLAnswerInfoBL </p>
 * <p>Description: OLLAnswerInfoBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-14 15:54:42
 */
package com.sinosoft.lis.llcase;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class OLLAnswerInfoBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
/** 业务处理相关变量 */
private LLAnswerInfoSchema mLLAnswerInfoSchema=new LLAnswerInfoSchema();
//private LLAnswerInfoSet mLLAnswerInfoSet=new LLAnswerInfoSet();
private TransferData mTransferData= null;
public OLLAnswerInfoBL() {
}
public static void main(String[] args)
{
  OLLAnswerInfoBL mOLLAnswerInfoBL = new OLLAnswerInfoBL();
  VData mVData = new VData();
  LLAnswerInfoSchema mLLAnswerInfoSchema = new LLAnswerInfoSchema();
  GlobalInput tG = new GlobalInput();
  mLLAnswerInfoSchema.setLogNo("200502240");
  mLLAnswerInfoSchema.setSerialNo("");
  mLLAnswerInfoSchema.setAnswer("qwerqewrqwerqwerqwerqwerqr");
  mLLAnswerInfoSchema.setConsultNo("123456789");
  mVData.add(mLLAnswerInfoSchema);
  tG.ManageCom="8600";
  tG.Operator="001";
  mVData.add(tG);
  mOLLAnswerInfoBL.submitData(mVData,"INSERT||MAIN");
}
/**
* 传输数据的公共方法
	* @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "OLLAnswerInfoBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败OLLAnswerInfoBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
      System.out.println("Start OLLAnswerInfoBL Submit...");
      String wfFlag = null;
            if (mTransferData != null)
            {
                wfFlag = (String)this.mTransferData.getValueByName(
                        "WFFLAG");

            }
            if (wfFlag == null || !"1".equals(wfFlag)) //工作流调用标记
            {
                PubSubmit pubSubmit = new PubSubmit();
                if (!pubSubmit.submitData(this.mResult, ""))
                {
                    CError.buildErr(this, "提交事务失败!");
                    return false;
                }
            }

    }
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
    String cDate = PubFun.getCurrentDate();
        String cTime = PubFun.getCurrentTime();
        String op = "UPDATE";
    if ( "INSERT||MAIN".equals( this.mOperate))
    {
         op = "DELETE&INSERT";
         //String evNo = PubFun1.CreateMaxNo("SerialNo", "5");
         //String sql ="select max(serialno) from llanswerinfo where consultno='"+ mLLAnswerInfoSchema.getConsultNo() +"'";
        int serialno = 1;
      /*   ExeSQL exesql = new ExeSQL();    //目前在回复表中， 一条咨询只会对应一条回复
         SSRS ssrs=exesql.execSQL(sql);
         if ( ssrs!=null && ssrs.getMaxRow()>1)
         {
             serialno = Integer.parseInt( ssrs.GetText(1,1));
             serialno++;
         }*/
        mLLAnswerInfoSchema.setSerialNo(serialno);
        mLLAnswerInfoSchema.setMakeDate(cDate);
        mLLAnswerInfoSchema.setMakeTime(cTime);
        mLLAnswerInfoSchema.setAskState("1");
        mLLAnswerInfoSchema.setSendFlag("0");

    }

    if ( "UPDATE||MAIN".equals( this.mOperate))
{

    mLLAnswerInfoSchema.setMakeDate(cDate);
    mLLAnswerInfoSchema.setMakeTime(cTime);
    mLLAnswerInfoSchema.setAskState("1");
    mLLAnswerInfoSchema.setSendFlag("0");


}
if ( "CONF||MAIN".equals( this.mOperate))
{
  op="UPDATE";
    mLLAnswerInfoSchema.setAskState("2");
    mLLAnswerInfoSchema.setSendFlag("1");


}
mLLAnswerInfoSchema.setAnswerOperator(this.mGlobalInput.Operator);
   mLLAnswerInfoSchema.setManageCom(this.mGlobalInput.ManageCom);
   mLLAnswerInfoSchema.setAnswerDate(cDate);
   mLLAnswerInfoSchema.setAnswerTime(cTime);

   mLLAnswerInfoSchema.setModifyDate(cDate);
   mLLAnswerInfoSchema.setModifyTime(cTime);

   MMap tmpMap = new MMap();
   String strSQL = "update LLConsult set ReplyState='2' where ConsultNo='" +
       mLLAnswerInfoSchema.getConsultNo() + "'";
   tmpMap.put(strSQL, "UPDATE");
   System.out.println(tmpMap);
   tmpMap.put(mLLAnswerInfoSchema, op);
   System.out.println(tmpMap);
   this.mResult.add( tmpMap );
   this.mResult.add(mLLAnswerInfoSchema);
return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
         this.mLLAnswerInfoSchema.setSchema((LLAnswerInfoSchema)cInputData.getObjectByObjectName("LLAnswerInfoSchema",0));
         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LLAnswerInfoDB tLLAnswerInfoDB=new LLAnswerInfoDB();
    tLLAnswerInfoDB.setSchema(this.mLLAnswerInfoSchema);
		//如果有需要处理的错误，则返回
		if (tLLAnswerInfoDB.mErrors.needDealError())
 		{
		  // @@错误处理
 			this.mErrors.copyAllErrors(tLLAnswerInfoDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "LLAnswerInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors .addOneError(tError) ;
 			return false;
    }
    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
   try
	{
//		this.mInputData.clear();
//		this.mInputData.add(this.mLLAnswerInfoSchema);
//		mResult.clear();
//    mResult.add(this.mLLAnswerInfoSchema);
	}
	catch(Exception ex)
	{
 		// @@错误处理
		CError tError =new CError();
 		tError.moduleName="LLAnswerInfoBL";
 		tError.functionName="prepareData";
 		tError.errorMessage="在准备往后层处理所需要的数据时出错。";
 		this.mErrors .addOneError(tError) ;
		return false;
	}
	return true;
	}
	public VData getResult()
	{
  	return this.mResult;
	}
}
