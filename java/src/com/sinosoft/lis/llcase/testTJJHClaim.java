package com.sinosoft.lis.llcase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.message.SOAPBodyElement;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class testTJJHClaim {
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /**文件路径*/
    private String FilePath="";
    /**文件名*/
    private String FileName="";
    
    private File mFileDir =null ;
    private String outFilePath="";
    /** 全局变量 */
    private GlobalInput mGlobalInput;
    
	private Logger cLogger = Logger.getLogger(getClass());

	private DocumentBuilder cDocbuilder = null;
	{
		DocumentBuilderFactory tDocFactory = DocumentBuilderFactory.newInstance();
		try {
			cDocbuilder = tDocFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	private TransformerFactory cTransformerFactory = TransformerFactory.newInstance();

	/**
	 * 测试WebService调用主程序
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		System.out.println("WebService调用测试开始…");

       	 String mServiceURL = "http://10.252.126.16:9080/ybtpre/services/YibaotongService";
//		 String mServiceURL = "http://localhost:8080/ybtpre/services/YibaotongService";
//		 http://10.252.4.4:9080/ybtpre/services/YibaotongService
		String mInFilePath = "E:/JH01.xml";  //E:/18201506083.xml
		String mOutFilePath = "E:/JH01返回.xml"; //E:/12313.xml

		InputStream mInputStream = new FileInputStream(mInFilePath);
		Reader mReader = new InputStreamReader(mInputStream, "GBK");

		testTJJHClaim mTestUI = new testTJJHClaim();
		try {
			Document mOutXmlDoc = mTestUI.callService(mReader, mServiceURL);

			mTestUI.outputDOM(mOutXmlDoc, new FileOutputStream(mOutFilePath));
		} catch (Exception ex) {
			ex.printStackTrace();
			System.err.println("WebService调用测试异常！");
			return;
		}

		System.out.println("WebService调用测试成功！");
		return;
	}
	/**
	 * 导入入口
	 * @throws Exception 
	 */
	public boolean submitData(VData mVData, String cOperate) throws Exception{
	
			if(!getInputData(mVData)){
				return false;
			}
			if(!checkData(outFilePath)){
				return false;
			}
			if(!doAdd(FilePath,FileName,outFilePath)){
				return false;
			}
	
		return true;
	}
	/**
	 * 从save页面获取数据
	 */
	private boolean getInputData(VData mInputData) {
        System.out.println("getInputData ......");
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        TransferData tTransferData = new TransferData();
        tTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData",0);
        FilePath = (String) tTransferData.getValueByName("FilePath");
        FileName = (String) tTransferData.getValueByName("FileName");
        outFilePath = (String) tTransferData.getValueByName("OutFilePath");
        return true;
    }
	/**
	 * checkData
	 * @param outFilePath
	 * @return
	 */
	public boolean checkData(String outFilePath){
	
	    mFileDir = new File(outFilePath);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录[" + outFilePath.toString() + "]失败！"
						+ mFileDir.getPath());
				return false;
			}
		}
		return true;
	}
	/**
	 * 
	 * @param ImportPath
	 * @param FileName
	 * @param outFilePath
	 * @return
	 * @throws Exception
	 */
	public boolean doAdd(String ImportPath,String FileName,String outFilePath) throws Exception{
		
		System.out.println("WebService调用测试开始…");
		
		if(!"".equals(ImportPath) && ImportPath != null){
      	 String mServiceURL = "http://10.252.126.16:9080/ybtpre/services/YibaotongService";
//		 String mServiceURL = "http://localhost:8080/ybtpre/services/YibaotongService";
//		 http://10.252.4.4:9080/ybtpre/services/YibaotongService
// 		 String mInFilePath = "E:/JH01.xml";  //E:/18201506083.xml
		
 //      String mOutFilePath = PubDocument.getSavePath(url);

      	String mOutFilePath =  outFilePath + FileName; //E:/12313.xml
		String newImportPath = ImportPath + FileName;
	
		InputStream mInputStream = new FileInputStream(newImportPath);
		Reader mReader = new InputStreamReader(mInputStream, "GBK");

		testTJJHClaim mTestUI = new testTJJHClaim();
		try {
			Document mOutXmlDoc = mTestUI.callService(mReader, mServiceURL);

			mTestUI.outputDOM(mOutXmlDoc, new FileOutputStream(mOutFilePath));
		} catch (Exception ex) {
			ex.printStackTrace();
			System.err.println("WebService调用测试异常！");
			return false;
			
		}
		}else{
			return buildErr("doAdd","WebService调用测试失败！");
		}
		
		System.out.println("WebService调用测试成功！");
		return true ;
	}
	
    // @@错误处理
    private boolean buildErr(String FucName,String ErrMsg){
        CError tError = new CError();
        tError.moduleName = "testTJJHClaim";
        tError.functionName = FucName;
        tError.errorMessage = ErrMsg;
        this.mErrors.addOneError(tError);
        return false;
    }
	/**
	 * 1、客户端准备数据;2、调用WebService服务;3、获取服务端处理结果数据
	 *
	 * @param pReader
	 * @param pServiceURL
	 * @return org.w3c.dom.Document
	 * @throws Exception
	 */
	public Document callService(Reader pReader, String pServiceURL)
			throws Exception {
		long mOldTimeMillis = System.currentTimeMillis();
		long mCurTimeMillis = mOldTimeMillis;

		SOAPBodyElement[] mSOAPBodyElements = new SOAPBodyElement[1];
		InputSource mInputSource = new InputSource(pReader);
		Document mInXmlDoc = cDocbuilder.parse(mInputSource);
		mSOAPBodyElements[0] = new SOAPBodyElement(mInXmlDoc.getDocumentElement());

		mCurTimeMillis = System.currentTimeMillis();
		cLogger.info("客户端准备数据完毕！耗时：" + (mCurTimeMillis - mOldTimeMillis) / 1000.0 + "s");

		cLogger.info("开始调用WebService服务！" + pServiceURL);

		Service mService = new Service();
		Call mCall = (Call) mService.createCall();
		mCall.setTargetEndpointAddress(pServiceURL);
		Vector mReElements = (Vector) mCall.invoke(mSOAPBodyElements);

		mCurTimeMillis = System.currentTimeMillis();
		cLogger.info("调用WebService服务成功！耗时：" + (mCurTimeMillis - mOldTimeMillis) / 1000.0 + "s");

		SOAPBodyElement mReSOAPBodyElement = (SOAPBodyElement) mReElements.get(0);
		return mReSOAPBodyElement.getAsDocument();
	}

	/**
	 * 将org.w3c.dom.Document转化为文件输出流
	 *
	 * @param pNode
	 * @param pOutputStream
	 * @throws Exception
	 */
	public void outputDOM(Node pNode, OutputStream pOutputStream)
			throws Exception {
		DOMSource mDOMSource = new DOMSource(pNode);

		StreamResult mStreamResult = new StreamResult(pOutputStream);

		Transformer mTransformer = cTransformerFactory.newTransformer();
		mTransformer.setOutputProperty(OutputKeys.INDENT, "yes");
		mTransformer.setOutputProperty(OutputKeys.ENCODING, "GB2312");

		mTransformer.transform(mDOMSource, mStreamResult);
	}
}
