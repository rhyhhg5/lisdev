package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: ClientRegisterBL </p>
 * <p>Description: ClientRegisterBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author:Xx
 * @CreateDate：2005-02-18 13:58:55
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Date;

public class SimpleClaimBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private String strHandler;
    private FDate fDate = new FDate();

    /** 业务处理相关变量 */
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private LLCaseSchema aLLCaseSchema = new LLCaseSchema();
    private LLSubReportSchema mLLSubReportSchema = new LLSubReportSchema();
    private LLCaseRelaSchema mLLCaseRelaSchema = new LLCaseRelaSchema();
    private LLFeeMainSchema mLLFeeMainSchema = new LLFeeMainSchema();
    private LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
    private LLSecurityReceiptSchema mLLSecurityReceiptSchema = new
            LLSecurityReceiptSchema();
    private LLCaseCureSet mLLCaseCureSet = new LLCaseCureSet();
    private LLAppClaimReasonSchema mLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
    private LLToClaimDutySet mLLToClaimDutySet = new LLToClaimDutySet();
    private LLCasePolicySet mLLCasePolicySet = new LLCasePolicySet();
    private String aCaseNo="";
    private String aRgtNo="";
    private String aSubRptNo = "";
    private String aCaseRelaNo = "";
    private String aCureNo = "";
    private String aMainFeeNo = "";
    private String aDate = "";
    private String aTime = "";
    private String oDate = "";
    private String oTime = "";

    public SimpleClaimBL() {
    }

    public static void main(String[] args) {
        String strOperate = "INSERT||MAIN";
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
        LLSecurityReceiptSchema tLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
        LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
        LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
        VData tVData = new VData();
        SimpleClaimBL tSimpleClaimBL = new SimpleClaimBL();

        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86530000";
        mGlobalInput.ComCode = "86530000";
        mGlobalInput.Operator = "cm5301";

        tLLCaseSchema.setRgtType("1");
        tLLRegisterSchema.setRgtNo("P5300060809000002");

        /**************************************************/
        tLLCaseSchema.setRgtState("01"); //案件状态
        tLLCaseSchema.setCaseNo("C0000060525000006");
        tLLCaseSchema.setCustomerName("刘欣");
        tLLCaseSchema.setCustomerNo("000310000");
        tLLCaseSchema.setPostalAddress("");
        tLLCaseSchema.setPhone("");
        tLLCaseSchema.setDeathDate("");
        tLLCaseSchema.setCustBirthday("1941-03-13");
        tLLCaseSchema.setCustomerSex("0");
        tLLCaseSchema.setIDType("0");
        tLLCaseSchema.setIDNo("110105194103131822");
        tLLCaseSchema.setCustState("04");
        tLLCaseSchema.setDeathDate("2005-5-1");
        tLLFeeMainSchema.setRgtNo("P0000060522000001");
        tLLFeeMainSchema.setCaseNo("");
        tLLFeeMainSchema.setCustomerNo("000310000");
        tLLFeeMainSchema.setCustomerSex("0");
        tLLFeeMainSchema.setHospitalCode("");
        tLLFeeMainSchema.setHospitalName("");
        tLLFeeMainSchema.setHosAtti("");
        tLLFeeMainSchema.setReceiptNo("53000");
        tLLFeeMainSchema.setFeeType("31");
        tLLFeeMainSchema.setFeeAtti("1");
        tLLFeeMainSchema.setFeeDate("2006-5-1");
        tLLFeeMainSchema.setHospStartDate("2006-5-1");
        tLLFeeMainSchema.setHospEndDate("2006-5-11");
        tLLFeeMainSchema.setRealHospDate("");
        tLLFeeMainSchema.setInHosNo("");
        tLLFeeMainSchema.setSecurityNo("");
        tLLFeeMainSchema.setInsuredStat("");
        tLLSecurityReceiptSchema.setApplyAmnt("300");
        tLLSecurityReceiptSchema.setSelfAmnt("2");
        tLLSecurityReceiptSchema.setSelfPay2("2");
        tLLSecurityReceiptSchema.setGetLimit("2");
        tLLSecurityReceiptSchema.setMidAmnt("2");
        tLLSecurityReceiptSchema.setPlanFee("2");
        tLLSecurityReceiptSchema.setHighAmnt2("22");
        tLLSecurityReceiptSchema.setSupInHosFee("2");
        tLLSecurityReceiptSchema.setHighAmnt1("222");
        tLLSecurityReceiptSchema.setOfficialSubsidy("");
        LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
        tLLAppClaimReasonSchema.setReasonCode("02"); //原因代码
        tLLAppClaimReasonSchema.setReason("住院"); //申请原因
        tLLAppClaimReasonSchema.setCustomerNo("000310000");
        tLLAppClaimReasonSchema.setReasonType("0");

        LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
        tLLCaseCureSchema.setDiseaseCode("333");
        tLLCaseCureSchema.setDiseaseName("对当地");
        tLLCaseCureSet.add(tLLCaseCureSchema);
        tLLCaseOpTimeSchema.setRgtState("01");
        tLLCaseOpTimeSchema.setStartDate("2005-9-15");
        tLLCaseOpTimeSchema.setStartTime("20:30:30");
        tVData.add(tLLCaseSchema);
        tVData.add(tLLRegisterSchema);
        tVData.add(tLLFeeMainSchema);
        tVData.add(tLLSecurityReceiptSchema);
        tVData.add(tLLAppClaimReasonSchema);
        tVData.add(tLLCaseCureSchema);
        tVData.add(tLLCaseOpTimeSchema);

        tVData.add(mGlobalInput);
        tSimpleClaimBL.submitData(tVData, strOperate);
        tVData.clear();
        tVData = tSimpleClaimBL.getResult();
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ClientRegisterBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ClientRegisterBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        System.out.println("Start ClientRegisterBL Submit...");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "SimpleClaimBL";
            tError.functionName = "dealdata";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        if (!calClaim()) {
            CError.buildErr(this, "理算失败");
            return false;
        }
        System.out.println("End SimpleClaimBL Submit...");
        mInputData = null;

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("--Star GetInputData--");
        this.mLLRegisterSchema.setSchema((LLRegisterSchema) cInputData.
                             getObjectByObjectName("LLRegisterSchema", 0));
        this.mLLCaseSchema.setSchema((LLCaseSchema) cInputData.
                                     getObjectByObjectName("LLCaseSchema", 0));
        this.mLLFeeMainSchema.setSchema((LLFeeMainSchema) cInputData.
                getObjectByObjectName("LLFeeMainSchema",0));
        this.mLLSecurityReceiptSchema.setSchema((LLSecurityReceiptSchema) cInputData.
                getObjectByObjectName("LLSecurityReceiptSchema",0));
        this.mLLCaseCureSet = (LLCaseCureSet) cInputData.
                                     getObjectByObjectName("LLCaseCureSet", 0);
        this.mLLAppClaimReasonSchema.setSchema((LLAppClaimReasonSchema) cInputData.
                                     getObjectByObjectName("LLAppClaimReasonSchema", 0));
        mLLCaseOpTimeSchema = (LLCaseOpTimeSchema) cInputData.
                              getObjectByObjectName("LLCaseOpTimeSchema", 0);
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        if (mGlobalInput.ManageCom.length() == 2)
            mGlobalInput.ManageCom = mGlobalInput.ManageCom + "000000";
        if (mGlobalInput.ManageCom.length() == 4)
            mGlobalInput.ManageCom = mGlobalInput.ManageCom + "0000";
        aDate = PubFun.getCurrentDate();
        aTime = PubFun.getCurrentTime();
        oDate = PubFun.getCurrentDate();
        oTime = PubFun.getCurrentTime();
        if (mLLCaseSchema.getCaseNo() != null) {
            LLCaseDB tLLCaseDB = new LLCaseDB();
            tLLCaseDB.setCaseNo(mLLCaseSchema.getCaseNo());
            if (tLLCaseDB.getInfo()) {
                if (!tLLCaseDB.getRgtState().equals("01")
                    && !tLLCaseDB.getRgtState().equals("02")
                    && !tLLCaseDB.getRgtState().equals("03")
                    && !tLLCaseDB.getRgtState().equals("04")
                    && !tLLCaseDB.getRgtState().equals("08")){
                    CError.buildErr(this, "该状态不允许修改案件信息！");
                    return false;
                }
                mOperate = "UPDATE||MAIN";
                aLLCaseSchema.setSchema(tLLCaseDB.getSchema());
                oDate = aLLCaseSchema.getMakeDate();
                oTime = aLLCaseSchema.getMakeTime();
            }
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("---Star dealData---mOperate---" + mOperate);
        if(!checkData())
            return false;
        if (this.mOperate.equals("INSERT||MAIN")) {
            if(!insertData())
                return false;
        }

        if (this.mOperate.equals("UPDATE||MAIN")) {
            if(!updateData())
                return false;
        }
        if(!fillCasePolicy()){
            CError.buildErr(this, "没有符合条件的保单！");
            return false;
        }
        if(!getClaimDuty()){
            CError.buildErr(this, "没有符合条件的责任！");
            return false;
        }
        if(!dealPolicyState())
            return false;

        if (this.mOperate.equals("DELETE||MAIN")) {
            map.put(mLLRegisterSchema, "DELETE");
        }

        return true;
    }

    private boolean insertData(){
        LLCaseSet tLLCaseSet = new LLCaseSet();
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        tLLCaseSet.set(tLLCaseDB.query());
        if (tLLCaseDB.mErrors.needDealError()) {
            // @@错误处理
            CError.buildErr(this, "个人案件查询失败");
            return false;
        }
        int realPeoples = tLLCaseSet.size();
        int appPeoples = mLLRegisterSchema.getAppPeoples();
        if (realPeoples >= appPeoples) {
            mLLRegisterSchema.setAppPeoples(realPeoples + 1);
            mLLRegisterSchema.setApplyerType("5");
            mLLRegisterSchema.setOperator(mGlobalInput.Operator);
            mLLRegisterSchema.setMngCom(mGlobalInput.ManageCom);
            mLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
            mLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLLRegisterSchema, "UPDATE");
        }

        /*生成理赔号*/
        System.out.println("---Star Insert case---");
        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        aCaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit);
        aSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);
        aCaseRelaNo = PubFun1.CreateMaxNo("CASERELANO", tLimit);
        aMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO",tLimit);
//        aCureNo = PubFun1.CreateMaxNo("CURENO", tLimit);
        aRgtNo = mLLRegisterSchema.getRgtNo();
        System.out.println("理赔号" + aCaseNo);
        System.out.println("申请号" + aRgtNo);

        if(!getCaseInfo())
            return false;

        mLLCaseOpTimeSchema.setCaseNo(aCaseNo);
        mLLCaseOpTimeSchema.setOperator(mGlobalInput.Operator);
        mLLCaseOpTimeSchema.setManageCom(mGlobalInput.ManageCom);
        LLCaseCommon tLLCaseCommon = new LLCaseCommon();
        LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(
                mLLCaseOpTimeSchema);
        map.put(tLLCaseOpTimeSchema, "INSERT");
        return true;
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        /*生成理赔号*/
        System.out.println("---Star Insert case---");
        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        aCaseNo = aLLCaseSchema.getCaseNo();
        aRgtNo = mLLRegisterSchema.getRgtNo();
        LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
        tLLCaseRelaDB.setCaseNo(aCaseNo);
        LLCaseRelaSet tLLCaseRelaSet = tLLCaseRelaDB.query();
        if (tLLCaseRelaSet.size()>0){
            aCaseRelaNo = tLLCaseRelaSet.get(1).getCaseRelaNo();
            aSubRptNo = tLLCaseRelaSet.get(1).getSubRptNo();
        }else{
            aSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);
            aCaseRelaNo = PubFun1.CreateMaxNo("CASERELANO", tLimit);
        }
        LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
        tLLFeeMainDB.setCaseNo(aCaseNo);
        LLFeeMainSet tLLFeeMainSet = tLLFeeMainDB.query();
        if(tLLFeeMainSet.size()>0)
            aMainFeeNo = tLLFeeMainSet.get(1).getMainFeeNo();
        else
            aMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO",tLimit);
        System.out.println("理赔号" + aCaseNo);
        System.out.println("申请号" + aRgtNo);

        if(!getCaseInfo())
            return false;

        return true;
    }

    private boolean getCaseInfo() {
        int CustAge = 0;
        try{
            CustAge = calAge();
        }catch(Exception ex){
            System.out.println("sheng ri ge shi cuo wu!");
        }
        mLLCaseSchema.setCustomerAge(CustAge);
        mLLCaseSchema.setRgtNo(aRgtNo);
        mLLCaseSchema.setCaseNo(aCaseNo);
        mLLCaseSchema.setRigister(mGlobalInput.Operator);
        mLLCaseSchema.setHandler(mGlobalInput.Operator);
        mLLCaseSchema.setDealer(mGlobalInput.Operator);
        mLLCaseSchema.setRgtDate(PubFun.getCurrentDate());
        mLLCaseSchema.setOperator(mGlobalInput.Operator);
        mLLCaseSchema.setMngCom(mGlobalInput.ManageCom);
        mLLCaseSchema.setMakeDate(aDate);
        mLLCaseSchema.setMakeTime(aTime);
        mLLCaseSchema.setModifyDate(oDate);
        mLLCaseSchema.setModifyTime(oTime);

        if(!genSubReport()){
            CError.buildErr(this, "生成事件失败！");
            return false;
        }

        mLLAppClaimReasonSchema.setRgtNo(aRgtNo);
        mLLAppClaimReasonSchema.setCaseNo(aCaseNo);
        mLLAppClaimReasonSchema.setOperator(mGlobalInput.Operator);
        mLLAppClaimReasonSchema.setMngCom(mGlobalInput.ManageCom);
        mLLAppClaimReasonSchema.setMakeDate(aDate);
        mLLAppClaimReasonSchema.setMakeTime(aTime);
        mLLAppClaimReasonSchema.setModifyDate(oDate);
        mLLAppClaimReasonSchema.setModifyTime(oTime);

        mLLFeeMainSchema.setAge(CustAge);
        mLLFeeMainSchema.setMainFeeNo(aMainFeeNo);
        mLLFeeMainSchema.setCustomerName(mLLCaseSchema.getCustomerName());
        mLLFeeMainSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
        mLLFeeMainSchema.setRgtNo(aRgtNo);
        mLLFeeMainSchema.setCaseNo(aCaseNo);
        mLLFeeMainSchema.setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());
        mLLFeeMainSchema.setOperator(mGlobalInput.Operator);
        mLLFeeMainSchema.setMngCom(mGlobalInput.ManageCom);
        mLLFeeMainSchema.setMakeDate(aDate);
        mLLFeeMainSchema.setMakeTime(aTime);
        mLLFeeMainSchema.setModifyDate(oDate);
        mLLFeeMainSchema.setModifyTime(oTime);

        if(!calSecu()){
            CError.buildErr(this, "生成社保帐单失败！");
            return false;
        }
        mLLSecurityReceiptSchema.setMainFeeNo(aMainFeeNo);
        mLLSecurityReceiptSchema.setFeeDetailNo(aMainFeeNo);
        mLLSecurityReceiptSchema.setRgtNo(aRgtNo);
        mLLSecurityReceiptSchema.setCaseNo(aCaseNo);
        mLLSecurityReceiptSchema.setOperator(mGlobalInput.Operator);
        mLLSecurityReceiptSchema.setMngCom(mGlobalInput.ManageCom);
        mLLSecurityReceiptSchema.setMakeDate(aDate);
        mLLSecurityReceiptSchema.setMakeTime(aTime);
        mLLSecurityReceiptSchema.setModifyDate(oDate);
        mLLSecurityReceiptSchema.setModifyTime(oTime);

        for(int i=1;i<=mLLCaseCureSet.size();i++){
            String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
            aCureNo = PubFun1.CreateMaxNo("CURENO", tLimit);
            mLLCaseCureSet.get(i).setSerialNo(aCureNo);
            mLLCaseCureSet.get(i).setCustomerNo(mLLCaseSchema.getCustomerNo());
            mLLCaseCureSet.get(i).setCustomerName(mLLCaseSchema.getCustomerName());
            mLLCaseCureSet.get(i).setDiagnose(mLLCaseCureSet.get(i).getDiseaseName());
            mLLCaseCureSet.get(i).setHospitalCode(mLLFeeMainSchema.getHospitalCode());
            mLLCaseCureSet.get(i).setHospitalName(mLLFeeMainSchema.getHospitalName());
            mLLCaseCureSet.get(i).setCaseNo(aCaseNo);
            mLLCaseCureSet.get(i).setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());
            mLLCaseCureSet.get(i).setOperator(mGlobalInput.Operator);
            mLLCaseCureSet.get(i).setMngCom(mGlobalInput.ManageCom);
            mLLCaseCureSet.get(i).setMakeDate(aDate);
            mLLCaseCureSet.get(i).setMakeTime(aTime);
            mLLCaseCureSet.get(i).setModifyDate(oDate);
            mLLCaseCureSet.get(i).setModifyTime(oTime);
        }
        String DelCaseCure = "delete from llcasecure where caseno ='"
                             + aCaseNo + "'";
        map.put(mLLCaseSchema, "DELETE&INSERT");
        map.put(mLLFeeMainSchema, "DELETE&INSERT");
        map.put(mLLSecurityReceiptSchema, "DELETE&INSERT");
        map.put(DelCaseCure,"DELETE");
        map.put(mLLCaseCureSet,"INSERT");
        map.put(mLLAppClaimReasonSchema,"DELETE&INSERT");

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 处理前检查数据是否合理
     * @return boolean
     */
    private boolean checkData(){
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        if (!tLLRegisterDB.getInfo()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SimpleClaimBL";
            tError.functionName = "dealData";
            tError.errorMessage = "团体立案信息查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
        String declineflag = "" + mLLRegisterSchema.getDeclineFlag();
        if (declineflag.equals("1")) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SimpleClaimBL";
            tError.functionName = "dealData";
            tError.errorMessage = "该团体申请已撤件，不能再录入个人客户!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if ("03".equals(mLLRegisterSchema.getRgtState())) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SimpleClaimBL";
            tError.functionName = "dealData";
            tError.errorMessage = "团体申请下所有个人案件已经结案完毕，" +
                                  "不能再录入个人客户!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if ("04".equals(mLLRegisterSchema.getRgtState())) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SimpleClaimBL";
            tError.functionName = "dealData";
            tError.errorMessage = "团体申请下所有个人案件已经给付确认，" +
                                  "不能再录入个人客户!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 生成事件及与案件的关联
     * @return boolean
     */
    private boolean genSubReport() {
        //立案分案,即事件
        mLLSubReportSchema.setSubRptNo(aSubRptNo);
        mLLSubReportSchema.setCustomerName(mLLCaseSchema.getCustomerName());
        mLLSubReportSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
        mLLSubReportSchema.setAccDate(PubFun.getCurrentDate());
        mLLSubReportSchema.setInHospitalDate(mLLFeeMainSchema.getHospStartDate());
        mLLSubReportSchema.setOutHospitalDate(mLLFeeMainSchema.getHospEndDate());
        if(mLLCaseCureSet.size()>0)
            mLLSubReportSchema.setAccDesc(mLLCaseCureSet.get(1).getDiseaseName());
        mLLSubReportSchema.setOperator(mGlobalInput.Operator);
        mLLSubReportSchema.setMngCom(mGlobalInput.ManageCom);
        mLLSubReportSchema.setMakeDate(PubFun.getCurrentDate());
        mLLSubReportSchema.setMakeTime(PubFun.getCurrentTime());
        mLLSubReportSchema.setModifyDate(PubFun.getCurrentDate());
        mLLSubReportSchema.setModifyTime(PubFun.getCurrentTime());

        mLLCaseRelaSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        mLLCaseRelaSchema.setSubRptNo(aSubRptNo);
        mLLCaseRelaSchema.setCaseRelaNo(aCaseRelaNo);
        map.put(mLLSubReportSchema,"DELETE&INSERT");
        map.put(mLLCaseRelaSchema,"DELETE&INSERT");
        return true;
    }

    /**
     * 校验计算社保信息
     * @return boolean
     */
    private boolean calSecu(){
        double aSelfPay1 = mLLSecurityReceiptSchema.getGetLimit()
                           +mLLSecurityReceiptSchema.getMidAmnt()
                           +mLLSecurityReceiptSchema.getHighAmnt1();
//                           +mLLSecurityReceiptSchema.getHighAmnt2();
        mLLSecurityReceiptSchema.setSelfPay1(aSelfPay1);
//        double aFeeIS = mLLSecurityReceiptSchema.getPlanFee()
//                        +mLLSecurityReceiptSchema.getSupInHosFee()
//                        +aSelfPay1;
//        double aFeeOS = mLLSecurityReceiptSchema.getSelfPay2()
//                        +mLLSecurityReceiptSchema.getSelfAmnt();
//        mLLSecurityReceiptSchema.setFeeInSecu(aFeeIS);
//        mLLSecurityReceiptSchema.setFeeOutSecu(aFeeOS);
        return true;
    }

    /**
     * 过滤要赔付的责任
     * @return boolean
     */
    private boolean getClaimDuty(){
        String aCaseNo = mLLCaseSchema.getCaseNo();
        String aCaseRelaNo = mLLCaseRelaSchema.getCaseRelaNo();
        String aSubRptNo = mLLSubReportSchema.getSubRptNo();
        LCGetDB tLCGetDB = new LCGetDB();
        LCGetSet tLCGetSet = new LCGetSet();
        LBGetDB tLBGetDB = new LBGetDB();
        LBGetSet tLBGetSet = new LBGetSet();
        tLCGetDB.setInsuredNo(mLLCaseSchema.getCustomerNo());
        tLCGetDB.setGrpContNo(mLLRegisterSchema.getRgtObjNo());
        tLBGetDB.setInsuredNo(mLLCaseSchema.getCustomerNo());
        tLBGetDB.setGrpContNo(mLLRegisterSchema.getRgtObjNo());
        tLCGetSet = tLCGetDB.query();
        tLBGetSet = tLBGetDB.query();
        for(int i=1;i<=tLCGetSet.size();i++){
            LLToClaimDutySchema row = new LLToClaimDutySchema();
            LCGetSchema lcGet = tLCGetSet.get(i);
            LCPolSchema lcPol = getLCPol(lcGet.getPolNo());
            if(lcPol==null){
                CError tError = new CError();
                tError.moduleName = "SimpleClaimBL";
                tError.functionName = "dealCase";
                tError.errorMessage = "险种保单信息查询失败,请契约运维核实保单数据是否有误!";
                this.mErrors.addOneError(tError);
                return false;
            }
            row.setCaseNo(aCaseNo);
            row.setCaseRelaNo(aCaseRelaNo);
            row.setSubRptNo(aSubRptNo);
            row.setPolNo(lcGet.getPolNo());        /*保单号*/
            row.setGetDutyCode(lcGet.getGetDutyCode()); /* 给付责任编码 */
            row.setDutyCode(lcGet.getDutyCode());
            row.setGetDutyKind("100"); /* 给付责任类型 */
            row.setGrpContNo(lcGet.getGrpContNo()); /* 集体合同号 */
            row.setGrpPolNo(lcPol.getGrpPolNo()); /* 集体保单号 */
            row.setContNo(lcGet.getContNo()); /* 个单合同号 */
            row.setKindCode(lcPol.getKindCode()); /* 险类代码 */
            row.setRiskCode(lcPol.getRiskCode()); /* 险种代码 */
            row.setRiskVer(lcPol.getRiskVersion()); /* 险种版本号 */
            row.setPolMngCom(lcPol.getManageCom()); /* 保单管理机构 */
            row.setClaimCount(0); /* 理算次数 */
            mLLToClaimDutySet.add(row);
        }
        for(int i=1;i<=tLBGetSet.size();i++){
            LLToClaimDutySchema row = new LLToClaimDutySchema();
            LBGetSchema lbGet = tLBGetSet.get(i);
            LBPolSchema lbPol = getLBPol(lbGet.getPolNo());
            row.setCaseNo(aCaseNo);
            row.setCaseRelaNo(aCaseRelaNo);
            row.setSubRptNo(aSubRptNo);
            row.setPolNo(lbGet.getPolNo());        /*保单号*/
            row.setGetDutyCode(lbGet.getGetDutyCode()); /* 给付责任编码 */
            row.setGetDutyKind("100"); /* 给付责任类型 */
            row.setGrpContNo(lbGet.getGrpContNo()); /* 集体合同号 */
            row.setGrpPolNo(lbPol.getGrpPolNo()); /* 集体保单号 */
            row.setContNo(lbGet.getContNo()); /* 个单合同号 */
            row.setKindCode(lbPol.getKindCode()); /* 险类代码 */
            row.setRiskCode(lbPol.getRiskCode()); /* 险种代码 */
            row.setRiskVer(lbPol.getRiskVersion()); /* 险种版本号 */
            row.setPolMngCom(lbPol.getManageCom()); /* 保单管理机构 */
            row.setClaimCount(0); /* 理算次数 */
            mLLToClaimDutySet.add(row);
        }
        map.put(mLLToClaimDutySet,"DELETE&INSERT");
        return true;
    }

    /**
     * 查询保单信息包括C表和B表的信息
     * 以后要考虑保单状态表里的信息
     * @param string String
     * @return LCPolSchema
     */
    private LCPolSchema getLCPol(String polNo)
    {
      LCPolDB db = new LCPolDB();
      db.setPolNo(polNo);
      if(!db.getInfo())
      {
        mErrors.addOneError("险种保单LCPol查询失败,请契约运维核实保单数据是否有误!");
        return null;
      }
      else
        return db.getSchema();
    }
    private LBPolSchema getLBPol(String polNo)
    {
      LBPolDB db = new LBPolDB();
      db.setPolNo(polNo);
      if(!db.getInfo())
      {
        mErrors.addOneError("查询LCPol出错");
        return null;
      }
      else
        return db.getSchema();
    }

    /**
     * 案件保单信息
     * @return boolean
     */
    private boolean fillCasePolicy() {
        String aCaseNo = mLLCaseSchema.getCaseNo();
        String aCaseRelaNo = mLLCaseRelaSchema.getCaseRelaNo();
        String aRgtNo = mLLCaseSchema.getRgtNo();
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setInsuredNo(mLLCaseSchema.getCustomerNo());
        tLCPolBL.setGrpContNo(mLLRegisterSchema.getRgtObjNo());
        LCPolSet tLCPolSet = new LCPolSet();
        tLCPolSet = tLCPolBL.query();
        int n = tLCPolSet.size();
        String tdate = PubFun.getCurrentDate();
        String ttime = PubFun.getCurrentTime();
        String strAccidentDate=mLLFeeMainSchema.getHospStartDate();
        if (!(strAccidentDate == null || strAccidentDate.equals(""))) {
            strAccidentDate = mLLFeeMainSchema.getFeeDate();
        }
        try {
            for (int i = 1; i <= n; i++) {
                LLCasePolicySchema tLLCasePolicySchema = new LLCasePolicySchema();
                LCPolSchema tLCPolSchema = new LCPolSchema();
                tLCPolSchema = tLCPolSet.get(i);
                System.out.println("出险日期是" + strAccidentDate);
                System.out.println("生效日期是" + tLCPolSchema.getCValiDate());

                if (!(strAccidentDate == null || strAccidentDate.equals(""))) {
                    if (!(CaseFunPub.checkDate(tLCPolSchema.getCValiDate(),
                                               strAccidentDate))) {
                        this.mErrors.copyAllErrors(tLCPolBL.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "SimpleClaimBL";
                        tError.functionName = "fillCasePolicy";
                        tError.errorMessage = "保单号码是" + tLCPolBL.getPolNo() +
                                              "的出险日期在保单生效日期之前，该操作被取消！！！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
                tLLCasePolicySchema.setCaseNo(aCaseNo);
                tLLCasePolicySchema.setRgtNo(aRgtNo);
                tLLCasePolicySchema.setCaseRelaNo(aCaseRelaNo);
                tLLCasePolicySchema.setOperator(this.mGlobalInput.Operator);
                tLLCasePolicySchema.setMngCom(this.mGlobalInput.ManageCom);
                tLLCasePolicySchema.setMakeDate(tdate);
                tLLCasePolicySchema.setModifyDate(tdate);
                tLLCasePolicySchema.setMakeTime(ttime);
                tLLCasePolicySchema.setModifyTime(ttime);

                if ((tLCPolSchema.getInsuredNo().equals(mLLCaseSchema.getCustomerNo()))) {
                    tLLCasePolicySchema.setCasePolType("0");
                } else
                if (tLCPolSchema.getAppntNo().equals(mLLCaseSchema.getCustomerNo())) {
                    tLLCasePolicySchema.setCasePolType("1");
                }

                tLLCasePolicySchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                tLLCasePolicySchema.setContNo(tLCPolSchema.getContNo());
                tLLCasePolicySchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                tLLCasePolicySchema.setPolNo(tLCPolSchema.getPolNo());
                tLLCasePolicySchema.setKindCode(tLCPolSchema.getKindCode());
                tLLCasePolicySchema.setRiskCode(tLCPolSchema.getRiskCode());
                tLLCasePolicySchema.setRiskVer(tLCPolSchema.getRiskVersion());
                tLLCasePolicySchema.setPolMngCom(tLCPolSchema.getManageCom());
                tLLCasePolicySchema.setSaleChnl(tLCPolSchema.getSaleChnl());
                tLLCasePolicySchema.setAgentCode(tLCPolSchema.getAgentCode());
                tLLCasePolicySchema.setAgentGroup(tLCPolSchema.getAgentGroup());
                tLLCasePolicySchema.setInsuredNo(tLCPolSchema.getInsuredNo());
                tLLCasePolicySchema.setInsuredName(tLCPolSchema.getInsuredName());
                tLLCasePolicySchema.setInsuredSex(tLCPolSchema.getInsuredSex());
                tLLCasePolicySchema.setInsuredBirthday(tLCPolSchema.
                        getInsuredBirthday());
                tLLCasePolicySchema.setAppntNo(tLCPolSchema.getAppntNo());
                tLLCasePolicySchema.setAppntName(tLCPolSchema.getAppntName());
                tLLCasePolicySchema.setCValiDate(tLCPolSchema.getCValiDate());

                tLLCasePolicySchema.setPolType("1"); //正式保单
                mLLCasePolicySet.add(tLLCasePolicySchema);
            }
            String delSql = "delete from llcasepolicy where caseno='" +
                            aCaseNo + "'";
            map.put(delSql, "DELETE");
            map.put(this.mLLCasePolicySet, "INSERT");
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            CError.buildErr(this, "准备数据出错:" + ex.getMessage());
            return false;
        }

    }

    /**
     * 自动理算，生成赔案信息表。
     * @return boolean
     */
    private boolean calClaim(){
        ClaimCalBL aClaimCalBL = new ClaimCalBL();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        VData aVData = new VData();
        aVData.addElement(mGlobalInput);
        aVData.addElement(tLLCaseSchema);
        if(!aClaimCalBL.submitData(aVData, "autoCal")){
            CError.buildErr(this, "理算失败");
             return false;
        }
        return true;
    }

    /**
     * 计算年龄的函数
     * @return int
     */
    private int calAge() {
        Date Birthday = fDate.getDate(mLLCaseSchema.getCustBirthday());
        String strNow = PubFun.getCurrentDate();
        Date aNow = fDate.getDate(strNow);
        int CustomerAge = PubFun.calInterval(Birthday, aNow, "Y");
        System.out.println("年龄计算函数。。。"+CustomerAge);
        return CustomerAge;
    }

    /**
     * 处理保单状态，针对普通理赔将保单状态置为0400
     * 死亡保单给LDPerson置死亡日期，保单状态置0410
     * @return boolean
     */
    private boolean dealPolicyState() {
            //死亡
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setInsuredNo(mLLCaseSchema.getCustomerNo());
            LCPolSet tLCPolSet = tLCPolDB.query();
            int y = tLCPolSet.size();
            System.out.println("该客户以投保人或被保险人存在的共有" + y + "张保单;");

            LDPersonDB tLDPersonDB = new LDPersonDB();
            tLDPersonDB.setCustomerNo(mLLCaseSchema.getCustomerNo());
            if(!tLDPersonDB.getInfo()){
                CError.buildErr(this, "查询客户信息失败");
                return false;
            }
            if (mLLCaseSchema.getDeathDate() != null||tLDPersonDB.getDeathDate()!=null) {
            for (int t = 1; t <= y; t++) {
                //修改该保单状态为终止(死亡报案终止).
                String opolstate = "" + tLCPolSet.get(t).getPolState();
                String npolstate = "";
                if (opolstate.equals("null") || opolstate.equals("")) {
                    npolstate = "0401";
                } else {
                    if(opolstate.substring(0,2).equals("04")){
                       npolstate = "0401"+opolstate.substring(4);
                   }else{
                       if (opolstate.length() < 4) {
                           npolstate = "0401" + opolstate;
                       } else {
                           npolstate = "0401" + opolstate.substring(0, 4);
                       }
                   }
                }
                tLCPolSet.get(t).setPolState(npolstate);
            }
            LDPersonSchema tLDPersonSchema = tLDPersonDB.getSchema();
            tLDPersonSchema.setDeathDate(mLLCaseSchema.getDeathDate());
            //保单挂起保存
//            map.put(tLCPolSet, "UPDATE");
            map.put(tLDPersonSchema, "UPDATE");
        }else{
            for (int t = 1; t <= y; t++) {
                //理赔立案.
                String opolstate = "" + tLCPolSet.get(t).getPolState();
               String npolstate = "";
               if (opolstate.equals("null") || opolstate.equals("")) {
                   npolstate = "0400";
               } else {
                   if(opolstate.substring(0,2).equals("04")){
                       npolstate = "0400"+opolstate.substring(4);
                   }else{
                       if (opolstate.length() < 4) {
                           npolstate = "0400" + opolstate;
                       } else {
                           npolstate = "0400" + opolstate.substring(0, 4);
                       }
                   }
               }
               tLCPolSet.get(t).setPolState(npolstate);
            }
//            map.put(tLCPolSet, "UPDATE");
        }
        return true;
    }

    /**
     * 查询功能
     * @return boolean
     */
    private boolean submitquery() {
        return false;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLLRegisterSchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLLCaseSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDDrugBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
