package com.sinosoft.lis.llcase;

import com.sinosoft.lis.vschema.LLCaseTreatmentInfoSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LLCaseTreatmentInfoUI {

	private VData mInputData;
    private VData mResult = new VData();
    public CErrors mErrors = new CErrors();
    private LLCaseTreatmentInfoSet mLLCaseTreatmentInfoSet;
    public LLCaseTreatmentInfoUI()
    {
    }

    public static void main(String[] args)
    {
    }
    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        LLCaseTreatmentInfoBL tLLCaseTreatmentInfoBL = new LLCaseTreatmentInfoBL();
        tLLCaseTreatmentInfoBL.submitData(mInputData, cOperate);
        //如果有需要处理的错误，则返回
        if (tLLCaseTreatmentInfoBL.mErrors.needDealError())
        this.mErrors.copyAllErrors(tLLCaseTreatmentInfoBL.mErrors);
        //取得结果返回前台
        this.mResult.clear();
        this.mResult = tLLCaseTreatmentInfoBL.getResult();
        mInputData = null;
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

}
