package com.sinosoft.lis.llcase;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.rmi.RemoteException;
import java.util.List;

import javax.xml.rpc.ServiceException;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.DOMBuilder;
import org.jdom.input.SAXBuilder;

import com.sinosoft.brms.databus.client.RuleService;
import com.sinosoft.brms.databus.client.RuleServiceImplServiceLocator;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.ExeSQL;

public class LPRuleTransfer {

	public LPRuleTransfer() {
		super();
		// TODO Auto-generated constructor stub
	}

	private static final String XML_ISAPPROVED = "isApproved";
	private static final String XML_VERIFYRESULTLIST = "verifyResultList";
	private static final String XML_VERIFYRESULT = "verifyResult";
	private static final String XML_RETURNINFO = "returnInfo";
	private static final String XML_RULENAME = "ruleName";
	private String passFlag;
	
	//调用规则引擎,返回校验信息
    public String getXmlStr(String system,String subSystem,String businessModule,String caseno,boolean isTest){

    	//获取规则引擎服务地址
    	ExeSQL exeSql = new ExeSQL();
    	String sql = "select codealias from ldcode where 1=1 and codetype = 'LPRule'";
    	String ruleAddress = exeSql.getOneValue(sql);
    	System.out.println(ruleAddress);
    	//ruleAddress = "http://10.10.164.4:8028/DataBus/RuleService";

    	String xmlStr = "";
		RuleService service = null;
		RuleServiceImplServiceLocator locator = new RuleServiceImplServiceLocator();
		System.out.println("生成RuleServiceImplServiceLocator完毕");
//		"http://10.10.164.4:8028/DataBus/RuleService"
		locator.setRuleServiceImplPortEndpointAddress(ruleAddress);
		System.out.println("设置地址完毕");
		try {
			service = locator.getRuleServiceImplPort();
		} catch (ServiceException e1) {
			e1.printStackTrace();
		}
		System.out.println("生成RuleService完毕");
		try {
			//获得规则引擎返回的校验信息
			xmlStr = service.fireRule(system, subSystem, businessModule, caseno, isTest);
			System.out.println("调用fireRule完毕");
			System.out.println(xmlStr);
		} catch (RemoteException e) {
			System.out.println("调用fireRule异常");
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return xmlStr;
    }
    
    //将字符串转为Document对象
    public Document stringToDoc(String xmlStr){

    	Document document = null;
    	SAXBuilder saxBuilder = null;
    	Reader in = null;
		try {
			in = new StringReader(xmlStr);
			saxBuilder = new SAXBuilder();
			document = saxBuilder.build(in);
			System.out.println("打印传出报文============");
			JdomUtil.print(document);	
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
    	return document;
    }
    
    //解析校验结论
    public String getApproved(Document doc){

    	Element root = (Element) doc.getRootElement();
   	    Element isApproved = root.getChild(XML_ISAPPROVED);
   	    passFlag = isApproved.getTextTrim();

   	    return passFlag;
    }
    
    //解析被校验的返回的信息
    public String getInForMation(Document doc) {
    	
    	Element root = (Element) doc.getRootElement();
   	    Element verifyResultList = root.getChild(XML_VERIFYRESULTLIST);
   	    String ruleName = "";
   	    String returnInfo = "";
   	    if(verifyResultList!=null){
   	    	List rsList = verifyResultList.getChildren(XML_VERIFYRESULT);
   	    	for(int i=0;i<rsList.size();i++){
   	    		ruleName = "";
   	    		returnInfo = "";
   	    		Element verifyResult = (Element) rsList.get(i);
   	    		ruleName = verifyResult.getChildTextTrim(XML_RULENAME);
   	    		returnInfo = verifyResult.getChildTextTrim(XML_RETURNINFO);
   	    		if(!"".equals(ruleName)&&!"null".equals(ruleName)&&ruleName!=null
   	    				&&!"".equals(returnInfo)&&!"null".equals(returnInfo)&&returnInfo!=null) {
   	    			break;
   	    		}
   	    	}
   	    }
    	return ruleName + "," + returnInfo;
    }
}
