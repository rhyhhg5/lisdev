package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.schema.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:个人保全人工核保</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */
public class PEdorManuUWPolUI
{
    private PEdorManuUWPolBL mPEdorManuUWPolBL = null;

    /**
     * 调用业务逻辑
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        mPEdorManuUWPolBL = new PEdorManuUWPolBL();
        if (mPEdorManuUWPolBL.submitData(data) == false)
        {
            return false;
        }
        return true;
    }

    /**
     * 返回错误
     * @return String
     */
    public String getError()
    {
        return mPEdorManuUWPolBL.mErrors.getFirstError();
    }

    public static void main(String[] args)
    {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86";
        tGI.Operator = "endor";

        LPUWMasterSchema tLPUWMasterSchema = new LPUWMasterSchema();
        tLPUWMasterSchema.setEdorNo("20060720000006");
        tLPUWMasterSchema.setEdorType("BF");
        tLPUWMasterSchema.setContNo("00001083801");
        tLPUWMasterSchema.setPolNo("21000065896");
        tLPUWMasterSchema.setInsuredNo("000010838");
        tLPUWMasterSchema.setAppntNo("000032555");
        tLPUWMasterSchema.setPassFlag("4");
        tLPUWMasterSchema.setUWIdea("fff");
        tLPUWMasterSchema.setAddPremFlag("1");
        tLPUWMasterSchema.setDisagreeDeal("1");

        //设置加费信息
        LPPremSchema tLPPremSchema = new LPPremSchema();
        tLPPremSchema.setEdorNo("20060720000006");
        tLPPremSchema.setEdorType("BF");
        tLPPremSchema.setContNo("00003255501");
        tLPPremSchema.setPolNo("21000065896");
        tLPPremSchema.setAppntNo("000032555");
        tLPPremSchema.setPrem("200");
        tLPPremSchema.setRate(0.0);
        tLPPremSchema.setPayStartDate("2005-05-01");
        tLPPremSchema.setPayEndDate("2005-05-01");
        tLPPremSchema.setPaytoDate("2005-10-20");

        EdorItemSpecialData tEdorItemSpecialData =
            new EdorItemSpecialData(tLPPremSchema.getEdorNo(),
                                    tLPPremSchema.getEdorType());
        tEdorItemSpecialData.setPolNo(tLPPremSchema.getPolNo());
        tEdorItemSpecialData.add("EdorValiDate", "2006-10-26");

        VData data = new VData();
        data.add(tGI);
        data.add(tLPUWMasterSchema);
        data.add(tLPPremSchema);
        data.add(tEdorItemSpecialData);

        PEdorManuUWPolUI tPEdorManuUWPolUI = new PEdorManuUWPolUI();
        if (!tPEdorManuUWPolUI.submitData(data))
        {
        }

    }
}
