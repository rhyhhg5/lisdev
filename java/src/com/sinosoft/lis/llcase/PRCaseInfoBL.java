package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: ClientRegisterBL </p>
 * <p>Description: ClientRegisterBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author:Xx
 * @CreateDate：2005-02-18 13:58:55
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Date;

public class PRCaseInfoBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private FDate fDate = new FDate();

    /** 业务处理相关变量 */
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private LLCaseExtSchema mLLCaseExtSchema = new LLCaseExtSchema();
    private LLCaseSchema aLLCaseSchema = new LLCaseSchema();
    private LLSubReportSchema mLLSubReportSchema = new LLSubReportSchema();
    private LLCaseRelaSchema mLLCaseRelaSchema = new LLCaseRelaSchema();
    private LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
    private LLAppClaimReasonSchema mLLAppClaimReasonSchema = new
            LLAppClaimReasonSchema();

    private String aCaseNo = "";
    private String aRgtNo = "";
    private String aSubRptNo = "";
    private String aCaseRelaNo = "";
    private String aDate = "";
    private String aTime = "";
    private String oDate = "";
    private String oTime = "";
    private String mManageCom = "";
    
    /**社保校验标记*/
    private boolean mSocialFlag = false;

    public PRCaseInfoBL() {
    }

    public static void main(String[] args) {
        String strOperate = "INSERT||MAIN";
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
        LLSecurityReceiptSchema tLLSecurityReceiptSchema = new
                LLSecurityReceiptSchema();
        LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
        LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
        VData tVData = new VData();
        PRCaseInfoBL tgenCaseInfoBL = new PRCaseInfoBL();

        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86110000";
        mGlobalInput.ComCode = "86110000";
        mGlobalInput.Operator = "bcm005";

        tLLCaseSchema.setRgtType("1");
        tLLRegisterSchema.setRgtNo("P0000060522000001");

        /**************************************************/
        tLLCaseSchema.setRgtState("01"); //案件状态
        tLLCaseSchema.setCaseNo("C0000060525000006");
        tLLCaseSchema.setCustomerName("刘欣");
        tLLCaseSchema.setCustomerNo("000310000");
        tLLCaseSchema.setPostalAddress("");
        tLLCaseSchema.setPhone("");
        tLLCaseSchema.setDeathDate("");
        tLLCaseSchema.setCustBirthday("1941-03-13");
        tLLCaseSchema.setCustomerSex("0");
        tLLCaseSchema.setIDType("0");
        tLLCaseSchema.setIDNo("110105194103131822");
        tLLCaseSchema.setCustState("04");
        tLLCaseSchema.setDeathDate("2005-5-1");
        tLLFeeMainSchema.setRgtNo("P0000060522000001");
        tLLFeeMainSchema.setCaseNo("");
        tLLFeeMainSchema.setCustomerNo("000310000");
        tLLFeeMainSchema.setCustomerSex("0");
        tLLFeeMainSchema.setHospitalCode("");
        tLLFeeMainSchema.setHospitalName("");
        tLLFeeMainSchema.setHosAtti("");
        tLLFeeMainSchema.setReceiptNo("53000");
        tLLFeeMainSchema.setFeeType("31");
        tLLFeeMainSchema.setFeeAtti("1");
        tLLFeeMainSchema.setFeeDate("2006-5-1");
        tLLFeeMainSchema.setHospStartDate("2006-5-1");
        tLLFeeMainSchema.setHospEndDate("2006-5-11");
        tLLFeeMainSchema.setRealHospDate("");
        tLLFeeMainSchema.setInHosNo("");
        tLLFeeMainSchema.setSecurityNo("");
        tLLFeeMainSchema.setInsuredStat("");
        tLLSecurityReceiptSchema.setApplyAmnt("300");
        tLLSecurityReceiptSchema.setSelfAmnt("2");
        tLLSecurityReceiptSchema.setSelfPay2("2");
        tLLSecurityReceiptSchema.setGetLimit("2");
        tLLSecurityReceiptSchema.setMidAmnt("2");
        tLLSecurityReceiptSchema.setPlanFee("2");
        tLLSecurityReceiptSchema.setHighAmnt2("22");
        tLLSecurityReceiptSchema.setSupInHosFee("2");
        tLLSecurityReceiptSchema.setHighAmnt1("222");
        tLLSecurityReceiptSchema.setOfficialSubsidy("");
        LLAppClaimReasonSchema tLLAppClaimReasonSchema = new
                LLAppClaimReasonSchema();
        tLLAppClaimReasonSchema.setReasonCode("02"); //原因代码
        tLLAppClaimReasonSchema.setReason("住院"); //申请原因
        tLLAppClaimReasonSchema.setCustomerNo("000310000");
        tLLAppClaimReasonSchema.setReasonType("0");

        tLLCaseCureSchema.setDiseaseCode("333");
        tLLCaseCureSchema.setDiseaseName("对当地");
        tLLCaseOpTimeSchema.setRgtState("01");
        tLLCaseOpTimeSchema.setStartDate("2005-9-15");
        tLLCaseOpTimeSchema.setStartTime("20:30:30");
        tVData.add(tLLCaseSchema);
        tVData.add(tLLRegisterSchema);
        tVData.add(tLLFeeMainSchema);
        tVData.add(tLLSecurityReceiptSchema);
        tVData.add(tLLAppClaimReasonSchema);
        tVData.add(tLLCaseCureSchema);
        tVData.add(tLLCaseOpTimeSchema);

        tVData.add(mGlobalInput);
        tgenCaseInfoBL.submitData(tVData, strOperate);
        tVData.clear();
        tVData = tgenCaseInfoBL.getResult();
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "genCaseInfoBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败genCaseInfoBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("Start ClientRegisterBL Submit...");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDDrugBL";
            tError.functionName = "ClientRegisterBL";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("End SimpleClaimBL Submit...");
        mInputData = null;

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("--Star GetInputData--");
        this.mLLRegisterSchema.setSchema((LLRegisterSchema) cInputData.
                                         getObjectByObjectName(
                                                 "LLRegisterSchema", 0));
        this.mLLCaseSchema.setSchema((LLCaseSchema) cInputData.
                                     getObjectByObjectName("LLCaseSchema", 0));
          if((LLCaseExtSchema) cInputData.getObjectByObjectName("LLCaseExtSchema", 0)!=null){
        	  this.mLLCaseExtSchema.setSchema((LLCaseExtSchema) cInputData.getObjectByObjectName("LLCaseExtSchema", 0));
          }
        
        
        this.mLLSubReportSchema.setSchema((LLSubReportSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LLSubReportSchema",
                                                  0));

        this.mLLAppClaimReasonSchema.setSchema((LLAppClaimReasonSchema)
                                               cInputData.
                                               getObjectByObjectName(
                "LLAppClaimReasonSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        mManageCom = mGlobalInput.ManageCom;
        if (mGlobalInput.ManageCom.trim().length() == 2) {
            mGlobalInput.ManageCom = mGlobalInput.ManageCom.trim() + "000000";
        }
        if (mGlobalInput.ManageCom.trim().length() == 4) {
            mGlobalInput.ManageCom = mGlobalInput.ManageCom.trim() + "0000";
        }
        aDate = PubFun.getCurrentDate();
        aTime = PubFun.getCurrentTime();
        oDate = PubFun.getCurrentDate();
        oTime = PubFun.getCurrentTime();
        
        //受理申请（团体）通过批次号，判断该批次导入的案件类型，支持社保案件分配必须
        String tRgtNo = mLLRegisterSchema.getRgtNo();
        String tSSFlag = LLCaseCommon.checkSocialSecurity(tRgtNo, "rgtno");
        if("1".equals(tSSFlag)){//社保批次
        	mSocialFlag = true;
        }
        
        if (mLLCaseSchema.getCaseNo() != null) {
            LLCaseDB tLLCaseDB = new LLCaseDB();
            tLLCaseDB.setCaseNo(mLLCaseSchema.getCaseNo());
            if (tLLCaseDB.getInfo()) {
                if (!tLLCaseDB.getRgtState().equals("01")
                    && !tLLCaseDB.getRgtState().equals("02")
                    && !tLLCaseDB.getRgtState().equals("03")
                    && !tLLCaseDB.getRgtState().equals("04")
                    && !tLLCaseDB.getRgtState().equals("08")) {
                    CError.buildErr(this, "该状态不允许修改案件信息！");
                    return false;
                }
                mOperate = "UPDATE||MAIN";
                aLLCaseSchema.setSchema(tLLCaseDB.getSchema());
                oDate = aLLCaseSchema.getMakeDate();
                oTime = aLLCaseSchema.getMakeTime();
            }
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("---Star dealData---mOperate---" + mOperate);
        if (this.mOperate.equals("INSERT||MAIN")) {
            if (!insertData()) {
                return false;
            }
        }

        if (this.mOperate.equals("UPDATE||MAIN")) {
            if (!updateData()) {
                return false;
            }
        }

        return true;
    }

    private boolean insertData() {

        /*生成理赔号*/
        System.out.println("---Star Insert case---");
        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        VData tno = new VData();
        tno.add(0, "1");
        tno.add(1, aRgtNo);
        tno.add(2, "STORE");
        aCaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit, tno);
        if (aCaseNo.length() < 17) {
            aCaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit);
        }
        aSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);
        aCaseRelaNo = PubFun1.CreateMaxNo("CASERELANO", tLimit);
        aRgtNo = mLLRegisterSchema.getRgtNo();
        System.out.println("理赔号" + aCaseNo);
        System.out.println("申请号" + aRgtNo);

        if (!getCaseInfo()) {
            return false;
        }

        if (!dealDeathDate()){
            return false;
        }

        mLLCaseOpTimeSchema.setCaseNo(aCaseNo);
        mLLCaseOpTimeSchema.setRgtState("01");
        mLLCaseOpTimeSchema.setStartDate(aDate);
        mLLCaseOpTimeSchema.setStartTime(aTime);
        mLLCaseOpTimeSchema.setOperator(mGlobalInput.Operator);
        mLLCaseOpTimeSchema.setManageCom(mGlobalInput.ManageCom);
        LLCaseCommon tLLCaseCommon = new LLCaseCommon();
        LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(
                mLLCaseOpTimeSchema);
        map.put(tLLCaseOpTimeSchema, "INSERT");
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        /*生成理赔号*/
        System.out.println("---Star Insert case---");
        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        aCaseNo = aLLCaseSchema.getCaseNo();
        aRgtNo = mLLRegisterSchema.getRgtNo();
        LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
        tLLCaseRelaDB.setCaseNo(aCaseNo);
        LLCaseRelaSet tLLCaseRelaSet = tLLCaseRelaDB.query();
        if (tLLCaseRelaSet.size() > 0) {
            aCaseRelaNo = tLLCaseRelaSet.get(1).getCaseRelaNo();
            aSubRptNo = tLLCaseRelaSet.get(1).getSubRptNo();
        } else {
            aSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);
            aCaseRelaNo = PubFun1.CreateMaxNo("CASERELANO", tLimit);
        }

        LLCaseCureDB tLLCaseCureDB = new LLCaseCureDB();
        tLLCaseCureDB.setCaseNo(aCaseNo);
        LLCaseCureSet tLLCaseCureSet = tLLCaseCureDB.query();
        map.put(tLLCaseCureSet, "DELETE");
        System.out.println("理赔号" + aCaseNo);
        System.out.println("申请号" + aRgtNo);

        if (!getCaseInfo()) {
            return false;
        }

        return true;
    }

    private boolean getCaseInfo() {
        LLCaseCommon tLLCaseCommon = new LLCaseCommon();
        int CustAge = 0;
        try {
            CustAge = calAge();
        } catch (Exception ex) {
            System.out.println("计算年龄错误");
        }
        mLLCaseSchema.setCustomerAge(CustAge);
        mLLCaseSchema.setRgtNo(aRgtNo);
        mLLCaseSchema.setCaseNo(aCaseNo);
        String tCaseProp = "";
        String tHandler ="";
        String tInputer = "";
        if(mSocialFlag){//走社保人员分配，设置机构处理，分配handler、dealer、claimer
        	mLLCaseSchema.setCaseProp("06");
        	tHandler = tLLCaseCommon.SocialSecurityChooseUser(mGlobalInput.ManageCom);
        	tInputer = mGlobalInput.Operator;
        		
        	if (tHandler == null || tHandler.equals("")) {
                   CError tError = new CError();
                   tError.moduleName = "ClientRegisterBL";
                   tError.functionName = "dealHandler";
                   tError.errorMessage = "机构"+mGlobalInput.ManageCom+"及机构"+mManageCom.substring(0,4)+"下，均没有符合条件的社保处理人!";
                   this.mErrors.addOneError(tError);
                   return false;
            }
        }else{
        	if ("1".equals(mLLRegisterSchema.getRptFlag())) {
                tCaseProp = "01";
                mLLCaseSchema.setCaseProp("06");
                tHandler = tLLCaseCommon.ChooseAssessor(mManageCom,
                        mLLCaseSchema.getCustomerNo(), tCaseProp);
            }else if("2".equals(mLLRegisterSchema.getRptFlag())){
            	tCaseProp = "03";
                mLLCaseSchema.setCaseProp("09");
                //cbs00055863简易案件也纳入了案件分配校验，需要修改
                tHandler=mLLRegisterSchema.getHandler1();
                if(tHandler == null || tHandler.equals("")){
                	tHandler=mGlobalInput.Operator;
                }
            }else if("3".equals(mLLRegisterSchema.getRptFlag())){
            	tCaseProp = "04";
                mLLCaseSchema.setCaseProp("11");
                tHandler = tLLCaseCommon.ChooseAssessor(mManageCom,
                        mLLCaseSchema.getCustomerNo(), tCaseProp);
            }else {
            	tCaseProp = "02";
                mLLCaseSchema.setCaseProp("08");
                tHandler = tLLCaseCommon.ChooseAssessor(mManageCom,
                        mLLCaseSchema.getCustomerNo(), tCaseProp);
            }   
        	
        	tInputer = tLLCaseCommon.chooseInputer(mManageCom,
                    tCaseProp);
        	
        	if (tHandler == null || tHandler.equals("")) {
                CError tError = new CError();
                tError.moduleName = "ClientRegisterBL";
                tError.functionName = "dealHandler";
                tError.errorMessage = "机构" + mManageCom + "没有符合条件的理赔人!";
                this.mErrors.addOneError(tError);
                return false;
            }
        	
        }
              
        mLLCaseSchema.setHandler(tHandler);
        mLLCaseSchema.setDealer(tHandler);
        mLLCaseSchema.setClaimer(tInputer);
        mLLCaseSchema.setRigister(mGlobalInput.Operator);
        mLLCaseSchema.setRgtDate(PubFun.getCurrentDate());
        mLLCaseSchema.setOperator(mGlobalInput.Operator);
        mLLCaseSchema.setMngCom(mGlobalInput.ManageCom);
        mLLCaseSchema.setMakeDate(aDate);
        mLLCaseSchema.setMakeTime(aTime);
        mLLCaseSchema.setModifyDate(oDate);
        mLLCaseSchema.setModifyTime(oTime);
if(mLLCaseExtSchema!=null){
	 mLLCaseExtSchema.setCaseNo(aCaseNo);
     mLLCaseExtSchema.setOperator(mGlobalInput.Operator);
     mLLCaseExtSchema.setMngCom(mGlobalInput.ManageCom);
     mLLCaseExtSchema.setMakeDate(aDate);
     mLLCaseExtSchema.setMakeTime(aTime);
     mLLCaseExtSchema.setModifyDate(oDate);
     mLLCaseExtSchema.setModifyTime(oTime);
}
       
        
        if (!genSubReport()) {
            CError.buildErr(this, "生成事件失败！");
            return false;
        }

        mLLAppClaimReasonSchema.setRgtNo(aRgtNo);
        mLLAppClaimReasonSchema.setCaseNo(aCaseNo);
        mLLAppClaimReasonSchema.setOperator(mGlobalInput.Operator);
        mLLAppClaimReasonSchema.setMngCom(mGlobalInput.ManageCom);
        mLLAppClaimReasonSchema.setMakeDate(aDate);
        mLLAppClaimReasonSchema.setMakeTime(aTime);
        mLLAppClaimReasonSchema.setModifyDate(oDate);
        mLLAppClaimReasonSchema.setModifyTime(oTime);

        if(mLLCaseExtSchema!=null){
        	map.put(mLLCaseExtSchema, "DELETE&INSERT");
        }
        map.put(mLLCaseSchema, "DELETE&INSERT");
        map.put(mLLAppClaimReasonSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 生成事件及与案件的关联
     * @return boolean
     */
    private boolean genSubReport() {
        //立案分案,即事件
        mLLSubReportSchema.setSubRptNo(aSubRptNo);
        mLLSubReportSchema.setOperator(mGlobalInput.Operator);
        mLLSubReportSchema.setMngCom(mGlobalInput.ManageCom);
        mLLSubReportSchema.setMakeDate(aDate);
        mLLSubReportSchema.setMakeTime(aTime);
        mLLSubReportSchema.setModifyDate(oDate);
        mLLSubReportSchema.setModifyTime(oTime);

        mLLCaseRelaSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        mLLCaseRelaSchema.setSubRptNo(aSubRptNo);
        mLLCaseRelaSchema.setCaseRelaNo(aCaseRelaNo);
        map.put(mLLSubReportSchema, "DELETE&INSERT");
        map.put(mLLCaseRelaSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 计算年龄的函数
     * @return int
     */
    private int calAge() {
        Date Birthday = fDate.getDate(mLLCaseSchema.getCustBirthday());
        String strNow = PubFun.getCurrentDate();
        Date aNow = fDate.getDate(strNow);
        int CustomerAge = PubFun.calInterval(Birthday, aNow, "Y");
        System.out.println("年龄计算函数。。。" + CustomerAge);
        return CustomerAge;
    }

    /**
     * 处理死亡的人
     * @return boolean
     */
    private boolean dealDeathDate() {
        LDPersonDB tLDPersonDB = new LDPersonDB();
        tLDPersonDB.setCustomerNo(mLLCaseSchema.getCustomerNo());
        if (!tLDPersonDB.getInfo()) {
            CError.buildErr(this, "查询客户信息失败");
            return false;
        }

        if (!"".equals(mLLCaseSchema.getDeathDate()) &&
            mLLCaseSchema.getDeathDate() != null) {
            LDPersonSchema tLDPersonSchema = tLDPersonDB.getSchema();
            tLDPersonSchema.setDeathDate(mLLCaseSchema.getDeathDate());
            map.put(tLDPersonSchema, "UPDATE");
        }
        return true;
    }


    /**
     * 查询功能
     * @return boolean
     */
    private boolean submitquery() {
        return false;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLLRegisterSchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLLCaseSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "genCaseInfoBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
