package com.sinosoft.lis.llcase;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 保全人工核保</p>
 * <p>Description: 体检通知书录入</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorUWManuHealthQBL
{
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPPENoticeResultSet mLPPENoticeResultSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();
    
    private String mEdorNo = null;

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!getInputData(data))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到传入参数
     * @param data VData
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                    "GlobalInput", 0);
            mLPPENoticeResultSet = (LPPENoticeResultSet) data.
                    getObjectByObjectName("LPPENoticeResultSet", 0);
            mEdorNo = mLPPENoticeResultSet.get(1).getEdorNo();
        }
        catch (Exception ex)
        {
            mErrors.addOneError("传入参数错误！");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 处理数据
     * @return boolean
     */
    private boolean dealData()
    {
        setPENoticeResult();
        changeEdorType();
        return true;
    }

    /**
     * 设置体检通知书主表
     */
    private void setPENoticeResult()
    {
        for (int i = 1; i <= mLPPENoticeResultSet.size(); i++)
        {
            LPPENoticeResultSchema tLPPENoticeResultSchema =
                    mLPPENoticeResultSet.get(i);
            tLPPENoticeResultSchema.setOperator(mGlobalInput.Operator);
            tLPPENoticeResultSchema.setMakeDate(mCurrentDate);
            tLPPENoticeResultSchema.setMakeTime(mCurrentTime);
            tLPPENoticeResultSchema.setModifyDate(mCurrentDate);
            tLPPENoticeResultSchema.setModifyTime(mCurrentTime);
            mMap.put(mLPPENoticeResultSet, "DELETE&INSERT");
        }
    }
    
    /**
     * 改变理赔工单的状态：待回复-》待处理
     *
     */
    private void changeEdorType(){
    	System.out.println("PEdorUWManuHealthBL-->changeEdorType");
    	//需要通过理赔续保二核的工单号，查询此次对应的任务流号，从而状态改变
    	String tSql = "select MissionId 工作流任务号, SubMissionId 工作流子任务号 " 
    		+ " from LWMission where ProcessId = '0000000003' and ActivityId = '0000001181' " 
    		+ "and Missionprop12 ='"+mEdorNo+"' "
    		+ "order by Missionprop1 desc ";
    	SSRS tSSRS = new SSRS();
    	ExeSQL tExeSQL = new ExeSQL();
    	tSSRS = tExeSQL.execSQL(tSql);
    	String tMissionId = "";
    	String tSubMissionId = "";
    	String tActivityId = "0000001181";
    	String tActivityStatus = "1";
    	if(tSSRS != null && tSSRS.getMaxRow() > 0){
    		for(int i=1;i<=tSSRS.getMaxRow();i++){		
    			tMissionId = tSSRS.GetText(i, 1);
    			tSubMissionId = tSSRS.GetText(i, 2);
    		}
    		
    	}else{
    		mErrors.addOneError("PEdorUWManuHealthBL中查询工单的对应工作流号失败");
    	}
    	
    	LWMissionSchema tLWMissionSchema   = new LWMissionSchema();
        UWIndStateUI tUWIndStateUI = new UWIndStateUI();
        tLWMissionSchema.setMissionID(tMissionId);
        tLWMissionSchema.setSubMissionID(tSubMissionId);
        tLWMissionSchema.setActivityID(tActivityId);
        tLWMissionSchema.setActivityStatus(tActivityStatus);
        String tOperate = "UPDATE||MAIN";
        VData tVData = new VData();
		tVData.add(tLWMissionSchema);
		tVData.add(mGlobalInput);
		tUWIndStateUI.submitData(tVData,tOperate);
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
