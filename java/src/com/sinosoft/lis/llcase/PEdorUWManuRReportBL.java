package com.sinosoft.lis.llcase;

import java.util.Date;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 理赔续保待核</p>
 * <p>Description: 契调通知书录入</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author Houyd
 * @version 1.0
 */

public class PEdorUWManuRReportBL
{
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPRReportSchema mLPRReportSchema = null;

    private LPRReportItemSet mLPRReportItemSet = null;

    private String mEdorNo = null;

    private String mContNo = null;

    private String mAppntNo = null;

    private String mAppntName = null;

    private String mInsuredNo = null;

    private String mInsuredName = null;

    private String mPrtNo = null;
    
    private String mContPrtNo = null;

    private String mManageCom = null;

    private String mAgentCode = null;

    private String mAgentName = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();
    
    /** 打印管理表 */
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!getInputData(data))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回契调通知书号
     * @return String
     */
    public String getPrtNo()
    {
        return mPrtNo;
    }
    
    /**
     * 返回保单印刷号
     * @return String
     */
    public String getContPrtNo()
    {
        return mContPrtNo;
    }

    /**
     * 得到传入参数
     * @param data VData
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                    "GlobalInput", 0);
            mLPRReportSchema = (LPRReportSchema) data.
                    getObjectByObjectName("LPRReportSchema", 0);
            mLPRReportItemSet = (LPRReportItemSet) data.
                    getObjectByObjectName("LPRReportItemSet", 0);
            mEdorNo = mLPRReportSchema.getEdorNo();
            mContNo = mLPRReportSchema.getContNo();
            LCContSchema tLCContSchema = CommonBL.getLCCont(mContNo);
            mAppntNo = tLCContSchema.getAppntNo();
            mManageCom = tLCContSchema.getManageCom();
            mAgentCode = tLCContSchema.getAgentCode();
            mContPrtNo = tLCContSchema.getPrtNo();
            mAppntName = CommonBL.getAppntName(mAppntNo);
            mInsuredNo = mLPRReportSchema.getCustomerNo();
            mInsuredName = CommonBL.getInsuredName(mInsuredNo);
        }
        catch (Exception ex)
        {
            mErrors.addOneError("传入参数错误！");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 处理数据
     * @return boolean
     */
    private boolean dealData()
    {
        mPrtNo = createPrtNo();
        setLPRReport();
        setLPRReportItem();
        preparePrint();
        changeEdorType();
        return true;
    }

    /**
     * 打印信息表
     * @return
     */
    private boolean preparePrint()
    {
      LDSysVarDB tLDSysVarDB = new LDSysVarDB();
      tLDSysVarDB.setSysVar("URGEInterval");

      if (tLDSysVarDB.getInfo() == false) {
        CError tError = new CError();
        tError.moduleName = "UWSendPrintBL";
        tError.functionName = "prepareURGE";
        tError.errorMessage = "没有描述催发间隔!";
        this.mErrors.addOneError(tError);
        return false;
      }
      FDate tFDate = new FDate();
      int tInterval = Integer.parseInt(tLDSysVarDB.getSysVarValue());
      System.out.println(tInterval);

      Date tDate = PubFun.calDate(tFDate.getDate(PubFun.getCurrentDate()),
                                  tInterval, "D", null);
      System.out.println(tDate);//取预计催办日期
        //准备打印管理表数据
        mLOPRTManagerSchema.setPrtSeq(mPrtNo);
        mLOPRTManagerSchema.setOtherNo(mContNo);
        mLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_CONT); //保单号
        mLOPRTManagerSchema.setCode("04"); //保全生调
        mLOPRTManagerSchema.setManageCom(mManageCom);
        mLOPRTManagerSchema.setAgentCode(mAgentCode);
        mLOPRTManagerSchema.setReqCom(mGlobalInput.ComCode);
        mLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
        //mLOPRTManagerSchema.setExeCom();
        //mLOPRTManagerSchema.setExeOperator();
        mLOPRTManagerSchema.setPrtType(PrintManagerBL.PT_FRONT); //前台打印
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setPatchFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        //mLOPRTManagerSchema.setDoneDate() ;
        //mLOPRTManagerSchema.setDoneTime();
        mLOPRTManagerSchema.setOldPrtSeq(mPrtNo);
        mLOPRTManagerSchema.setForMakeDate(tDate);
        mMap.put(mLOPRTManagerSchema, "DELETE&INSERT");
        return true;
    }
    
    /**
     * 产生打印号
     * @return String
     */
    private String createPrtNo()
    {
        LPRReportDB tLPRReportDB = new LPRReportDB();
        tLPRReportDB.setEdorNo(mEdorNo);
        tLPRReportDB.setCustomerNo(mInsuredNo);
        LPRReportSet tLPRReportSet = tLPRReportDB.query();
        if (tLPRReportSet.size() != 0)
        {
            String prtNo = tLPRReportSet.get(1).getPrtSeq();
            if (prtNo != null)
            {
                return prtNo;
            }
        }
        return PubFun1.CreateMaxNo("PRTSEQNO","SN");
    }

    /**
     * 设置体检通知书主表
     */
    private void setLPRReport()
    {
        String sql = "delete from LPRReport " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and CustomerNo = '" + mInsuredNo + "' ";
        mMap.put(sql, "DELETE");
        mLPRReportSchema.setEdorNo(mEdorNo);
        mLPRReportSchema.setGrpContNo(BQ.GRPFILLDATA);
        mLPRReportSchema.setProposalContNo(mContNo);
        mLPRReportSchema.setCustomerNo(mInsuredNo);
        mLPRReportSchema.setAppntNo(mAppntNo);
        mLPRReportSchema.setAppntName(mAppntName);
        mLPRReportSchema.setName(mInsuredName);
        mLPRReportSchema.setPrtSeq(mPrtNo);
        mLPRReportSchema.setReplyFlag("0");
        mLPRReportSchema.setManageCom(mManageCom);
        mLPRReportSchema.setOperator(mGlobalInput.Operator);
        mLPRReportSchema.setMakeDate(mCurrentDate);
        mLPRReportSchema.setMakeTime(mCurrentTime);
        mLPRReportSchema.setModifyDate(mCurrentDate);
        mLPRReportSchema.setModifyTime(mCurrentTime);
        mMap.put(mLPRReportSchema, "DELETE&INSERT");
    }

    /**
     * 设置体检项目表
     */
    private void setLPRReportItem()
    {
        String sql = "delete from LPRReportItem " +
                "where EdorNo = '" + mEdorNo + "' " +
                "and PrtSeq = '" + mPrtNo + "' ";
        mMap.put(sql, "DELETE");
        for (int i = 1; i <= mLPRReportItemSet.size(); i++)
        {
            LPRReportItemSchema tLPRReportItemSchema =
                    mLPRReportItemSet.get(i);
            tLPRReportItemSchema.setEdorNo(mEdorNo);
            tLPRReportItemSchema.setGrpContNo(BQ.GRPFILLDATA);
            tLPRReportItemSchema.setContNo(mContNo);
            tLPRReportItemSchema.setProposalContNo(mContNo);
            tLPRReportItemSchema.setPrtSeq(mPrtNo);
            tLPRReportItemSchema.setModifyDate(mCurrentDate);
            tLPRReportItemSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPRReportItemSchema, "DELETE&INSERT");
        }
    }
    
    /**
     * 改变理赔工单的状态：待处理-》待回复
     *
     */
    private void changeEdorType(){
    	System.out.println("PEdorUWManuHealthBL-->changeEdorType");
    	//需要通过理赔续保二核的工单号，查询此次对应的任务流号，从而状态改变
    	String tSql = "select MissionId 工作流任务号, SubMissionId 工作流子任务号 " 
    		+ " from LWMission where ProcessId = '0000000003' and ActivityId = '0000001181' " 
    		+ "and Missionprop12 ='"+mEdorNo+"' "
    		+ "order by Missionprop1 desc ";
    	SSRS tSSRS = new SSRS();
    	ExeSQL tExeSQL = new ExeSQL();
    	tSSRS = tExeSQL.execSQL(tSql);
    	String tMissionId = "";
    	String tSubMissionId = "";
    	String tActivityId = "0000001181";
    	String tActivityStatus = "2";
    	if(tSSRS != null && tSSRS.getMaxRow() > 0){
    		for(int i=1;i<=tSSRS.getMaxRow();i++){		
    			tMissionId = tSSRS.GetText(i, 1);
    			tSubMissionId = tSSRS.GetText(i, 2);
    		}
    		
    	}else{
    		mErrors.addOneError("PEdorUWManuHealthBL中查询工单的对应工作流号失败");
    	}
    	
    	LWMissionSchema tLWMissionSchema   = new LWMissionSchema();
        UWIndStateUI tUWIndStateUI = new UWIndStateUI();
        tLWMissionSchema.setMissionID(tMissionId);
        tLWMissionSchema.setSubMissionID(tSubMissionId);
        tLWMissionSchema.setActivityID(tActivityId);
        tLWMissionSchema.setActivityStatus(tActivityStatus);
        String tOperate = "UPDATE||MAIN";
        VData tVData = new VData();
		tVData.add(tLWMissionSchema);
		tVData.add(mGlobalInput);
		tUWIndStateUI.submitData(tVData,tOperate);
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
