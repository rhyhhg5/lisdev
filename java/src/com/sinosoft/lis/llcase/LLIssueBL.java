package com.sinosoft.lis.llcase;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author cc
 * @version 1.0
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.*;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.ExeSQL;

public class LLIssueBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    private String IssueNo = "";
    private String Operator = "";
    private String CaseNo = "";
    private String ORgtState = "";
    private String Handler = "";
    private String SQL = "";

    private String mOperate = "";

    //业务处理相关变量
    /** 全局数据 */

    MMap map = new MMap();
    ExeSQL tExeSQL = new ExeSQL();
    SSRS tSSRS = new SSRS();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LLIssuePolSchema mLLIssuePolSchema = new LLIssuePolSchema();
    private LLCaseBackSchema tLLCaseBackSchema = new LLCaseBackSchema();
    private LLCaseDB tLLCaseDB = new LLCaseDB();
    private TransferData mTransferData = new TransferData();

    private String strUrl = "";

    public LLIssueBL() {
    }

    /**
         传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mOperate = cOperate;
        if (!mOperate.equals("INSERT||NEW") &&
            !mOperate.equals("INSERT||ADD")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 进行业务处理
        if (!dealData()) {
            return false;
        }
        // 准备传往后台的数据
        VData vData = new VData();
        {

            if (!prepareOutputData(vData)) {
                return false;
            }
        }

        //　数据提交、保存

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start PubSubmit Submit...");

        if (!tPubSubmit.submitData(vData, "")) {
            // @@错误处理ssss
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "LLIssueBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("---commitData---");
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData) {
        try {
            String receiptflag = "";
            tLLCaseDB.setCaseNo(CaseNo);
            if (!tLLCaseDB.getInfo()) {
                CError tError = new CError();
                tError.moduleName = "LLIssueBL";
                tError.functionName = "prepareOutputData";
                tError.errorMessage = "案件信息查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            if (!LLCaseCommon.checkHospCaseState(CaseNo)) {
                CError.buildErr(this, "医保通案件待确认，不能进行处理");
                return false;
            }

            if ("11".equals(tLLCaseDB.getRgtState()) ||
                "12".equals(tLLCaseDB.getRgtState()) ||
                "14".equals(tLLCaseDB.getRgtState())) {
                CError tError = new CError();
                tError.moduleName = "LLIssueBL";
                tError.functionName = "prepareOutputData";
                tError.errorMessage = "案件已处理完毕";
                this.mErrors.addOneError(tError);
                return false;
            }

            ORgtState = tLLCaseDB.getRgtState();
            Handler = tLLCaseDB.getHandler();
            Operator = tLLCaseDB.getOperator();
            String RgtNo = tLLCaseDB.getRgtNo();
            if (RgtNo.substring(0, 1).equals("P")) {
                LLRegisterDB tLLRegisterDB = new LLRegisterDB();
                tLLRegisterDB.setRgtNo(RgtNo);
                if (!tLLRegisterDB.getInfo()) {
                    CError tError = new CError();
                    tError.moduleName = "LLCaseReturnBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "团体批次信息查询失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (tLLRegisterDB.getRgtState().equals("03")) {
                    LLRegisterSchema aLLRegisterSchema = new LLRegisterSchema();
                    aLLRegisterSchema = tLLRegisterDB.getSchema();
                    aLLRegisterSchema.setRgtState("02");
                    map.put(aLLRegisterSchema, "UPDATE");
                }
            }

            if (mOperate.equals("INSERT||NEW")) { //操作员发问
                int MAX = 0; // 新插入问题的问题号
                String SQL3 =
                        "select max(int(IssueNo)) from llissuepol where caseno = '" +
                        CaseNo + "'";
                String MaxIssNo = tExeSQL.getOneValue(SQL3);
                if (!StrTool.cTrim(MaxIssNo).equals("")) {
                    MAX = Integer.parseInt(MaxIssNo);
                }
                MAX = MAX + 1;
                mLLIssuePolSchema.setIssueNo(String.valueOf(MAX));
                mLLIssuePolSchema.setMessageNo("1"); //确定问题号和流水号。
                mLLIssuePolSchema.setRgtState(ORgtState); //保留提起时案件状态。
                if (mLLIssuePolSchema.getReplyer().equals("")) { //界面未指定处理人，则默认为处理人。
                    System.out.println("默认返回受理人员队列........");
                    mLLIssuePolSchema.setReplyer(Handler); //
                }
                //界面不能指定返回状态，默认为延迟状态。
                if (mLLIssuePolSchema.getBackRgtState().equals("")) {
                    mLLIssuePolSchema.setBackRgtState("13");
                } else if (mLLIssuePolSchema.getBackRgtState().equals("02")) {
                    receiptflag = " ,receiptflag= '2' ";
                }
                //界面指定返回状态，则视为回退。增加回退表记录和团体批次状态的控制
                tLLCaseBackSchema = CaseBack("13", "问题件提问回退");

                String SQL4 = "UPDATE LLCASE SET RGTSTATE = '13' " +
                              receiptflag + "  WHERE CASENO = '" + CaseNo + "'";
                map.put(SQL4, "UPDATE");
            }

            if (mOperate.equals("INSERT||ADD")) { //操作员回复
                String RequrieMan = "";
                String BackRgtState = "";
                int MAX2 = 0; //对当前回复问题的新流水号
                String SQL5 =
                        "select max(messageNo) from llissuepol where caseno = '" +
                        CaseNo + "' and issueno = '" + IssueNo +
                        "' and MessageType = '2'";
                String MessNo = tExeSQL.getOneValue(SQL5);
                if (!StrTool.cTrim(MessNo).equals("")) {
                    MAX2 = Integer.parseInt(MessNo);
                }
                MAX2 = MAX2 + 1;
                mLLIssuePolSchema.setIssueNo(IssueNo);
                mLLIssuePolSchema.setMessageNo(String.valueOf(MAX2)); //确定问题号和流水号。
                mLLIssuePolSchema.setRgtState(ORgtState); //保留提起时案件状态。

                String SQL6 =
                        "select RequrieMan,BackRgtState from llissuepol where caseno = '" +
                        CaseNo + "' and IssueNo = '" + IssueNo +
                        "' and MessageType = '1'";
                tSSRS = tExeSQL.execSQL(SQL6);
                if (tSSRS.getMaxRow() > 0) {
                    BackRgtState = tSSRS.GetText(1, 2);
                    RequrieMan = tSSRS.GetText(1, 1);
                }
                if (mLLIssuePolSchema.getReplyer().equals("")) {
                    mLLIssuePolSchema.setReplyer(RequrieMan);
                }

                //界面未指定处理人，则默认为提问人。
                if (BackRgtState.equals("02")) {
                    if (mLLIssuePolSchema.getBackRgtState().equals("")
                        || mLLIssuePolSchema.getBackRgtState().equals("02")) {
                        receiptflag = " ,receiptflag= '0' ";
                    } else { //界面未指定返回状态，则默认为受理状态。
                        receiptflag = " ,receiptflag= '1' ";
                    }
                }
                if (mLLIssuePolSchema.getBackRgtState().equals("")
                    || mLLIssuePolSchema.getBackRgtState().equals("02")) {
                    mLLIssuePolSchema.setBackRgtState("01");
                }

                tLLCaseBackSchema = CaseBack(mLLIssuePolSchema.getBackRgtState(),
                                             "问题件回复回退");
                mLLIssuePolSchema.setState("1");
                String SQL7 =
                        "update llissuepol set state = '1' where caseno = '" +
                        CaseNo + "' and issueno = '" + IssueNo +
                        "' and MessageNo = '1' and MessageType = '1'";
                String SQL8 = "UPDATE LLCASE SET RGTSTATE = '" +
                              mLLIssuePolSchema.getBackRgtState() + "'" +
                              receiptflag +
                              " WHERE CASENO = '" + CaseNo + "'";
                map.put(SQL7, "UPDATE");
                map.put(SQL8, "UPDATE");
            }
            if (mLLIssuePolSchema.getReplyer().equals("")) {
                tLLCaseBackSchema.setNHandler(mGlobalInput.Operator);
            } else {
                tLLCaseBackSchema.setNHandler(mLLIssuePolSchema.getReplyer());
            }
            mLLIssuePolSchema.setMakeDate(PubFun.getCurrentDate());
            mLLIssuePolSchema.setMakeTime(PubFun.getCurrentTime());
            mLLIssuePolSchema.setModifyDate(PubFun.getCurrentDate());
            mLLIssuePolSchema.setModifyTime(PubFun.getCurrentTime());
            if (!ORgtState.equals(mLLIssuePolSchema.getBackRgtState())) {
                map.put(tLLCaseBackSchema, "INSERT");
            }
            map.put(mLLIssuePolSchema, "INSERT");
            vData.add(map);
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    private LLCaseBackSchema CaseBack(String Rgtstate, String Remark) {

        LLCaseBackSchema mLLCaseBackSchema = new LLCaseBackSchema();
        String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
        String CaseBackNo = PubFun1.CreateMaxNo("CaseBack", tLimit);
        mLLCaseBackSchema.setCaseBackNo(CaseBackNo);
        mLLCaseBackSchema.setRgtNo(mLLIssuePolSchema.getRgtNo());
        mLLCaseBackSchema.setCaseNo(mLLIssuePolSchema.getCaseNo());
        mLLCaseBackSchema.setBeforState(ORgtState);
        mLLCaseBackSchema.setAfterState(Rgtstate);
        mLLCaseBackSchema.setOHandler(Handler);
        mLLCaseBackSchema.setNHandler(Handler);
        mLLCaseBackSchema.setReason("6");
        mLLCaseBackSchema.setRemark(Remark);
        mLLCaseBackSchema.setMngCom(mGlobalInput.ManageCom);
        mLLCaseBackSchema.setOperator(mGlobalInput.Operator);
        mLLCaseBackSchema.setMakeDate(PubFun.getCurrentDate());
        mLLCaseBackSchema.setMakeTime(PubFun.getCurrentTime());
        mLLCaseBackSchema.setModifyDate(PubFun.getCurrentDate());
        mLLCaseBackSchema.setModifyTime(PubFun.getCurrentTime());
        return mLLCaseBackSchema;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mLLIssuePolSchema = (LLIssuePolSchema) cInputData.getObjectByObjectName(
                "LLIssuePolSchema", 0);
        IssueNo = (String) mTransferData.getValueByName("IssueNo");

        if (mLLIssuePolSchema == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        CaseNo = mLLIssuePolSchema.getCaseNo();
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "DerferAppF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "cm0001";

        VData tVData = new VData();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("IssueNo", "4");
        LLIssuePolSchema tLLIssuePolSchema = new LLIssuePolSchema();
        tLLIssuePolSchema.setCaseNo("C3500070403000003");
        tLLIssuePolSchema.setRgtNo("C3500070403000003");
        tLLIssuePolSchema.setMessageType("1");
        tLLIssuePolSchema.setRequrieMan(tG.Operator);
        tLLIssuePolSchema.setSubject("DSFASDFSDFSDF");
        tLLIssuePolSchema.setMessageCont("DSFASDFSDFSDFSSSSSS798798789798798");
        tLLIssuePolSchema.setReplyer("");
        tLLIssuePolSchema.setBackRgtState("");
        tLLIssuePolSchema.setState("0");
        tLLIssuePolSchema.setOperator(tG.Operator);
        tLLIssuePolSchema.setManageCom(tG.ManageCom);
        tVData.add(tTransferData);
        tVData.add(tLLIssuePolSchema);
        tVData.add(tG);

        LLIssueBL tLLIssueBL = new LLIssueBL();
        tLLIssueBL.submitData(tVData, "INSERT||ADD");
    }


}
