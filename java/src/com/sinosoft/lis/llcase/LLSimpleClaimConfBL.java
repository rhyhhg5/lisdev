package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: SimpleClaimAuditBL </p>
 * <p>Description: 团体批量审定 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft </p>
 * @author:Xx
 * @CreateDate：2005-02-18 13:58:55
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LLSimpleClaimConfBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mG = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCaseSet mLLCaseSet = new LLCaseSet();
    private LLCaseSet mbackCaseSet = new LLCaseSet();
    private LLClaimUserSchema mLLClaimUserSchema = new LLClaimUserSchema();
    private String mReturnMessage = "";
    private String aDate = "";
    private String aTime = "";

    private String mRgtNo = "";
    
    /** 社保保单标志 0：非社保；1：社保*/
    private String mSocialSecurity = "0";


    public LLSimpleClaimConfBL() {
    }

    public static void main(String[] args) {
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLCaseSet tLLCaseSet = new LLCaseSet();
        VData tVData = new VData();
        SimpleClaimAuditBL tSimpleClaimAuditBL = new SimpleClaimAuditBL();

        GlobalInput mG = new GlobalInput(); 
        mG.ManageCom = "86";
        mG.ComCode = "86";
        mG.Operator = "cm0012";
        tLLRegisterSchema.setRgtNo("P9400070214000001");
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setCaseNo("C9400070215000128");
        tLLCaseSchema.setRgtState("03");
        tLLCaseSchema.setCancleReason("1");
        tLLCaseSchema.setCancleRemark("郁闷郁闷郁闷");
        tLLCaseSet.add(tLLCaseSchema);
        tVData.add(tLLCaseSet);
        tVData.add(tLLRegisterSchema);
        tVData.add(mG);
        tSimpleClaimAuditBL.submitData(tVData, "UW||MAIN");
        tVData.clear();
        tVData = tSimpleClaimAuditBL.getResult();
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行数据基本校验
        if (!checkData())
            return false;
        //进行业务处理
        if (!dealData()) {
            mReturnMessage += "数据处理失败SimpleClaimAuditBL-->dealData!";
            CError.buildErr(this,mReturnMessage);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, null)) {
            CError.buildErr(this,"数据提交失败!");
            return false;
        }
        System.out.println("End SimpleClaimAuditBL Submit...");

        mInputData.clear();
        map = new MMap();
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        String tRgtNo = mLLRegisterSchema.getRgtNo();
        tLLRegisterDB.setRgtNo(tRgtNo);
        if (tLLRegisterDB.getInfo()) {
            LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
            tLLRegisterSchema = tLLRegisterDB.getSchema();
            LLCaseDB tLLCaseDB = new LLCaseDB();
            LLCaseSet tLLCaseSet = new LLCaseSet();
            String tsql =
                    "select * from llcase where rgtstate not in('09','14') and rgtno='"
                    + tRgtNo + "'";
            System.out.println(tsql);
            tLLCaseSet = tLLCaseDB.executeQuery(tsql);
            System.out.println(tLLRegisterSchema.getRgtState());
            if (tLLCaseSet != null && tLLCaseSet.size() > 0) {
                if (tLLRegisterSchema.getRgtState().equals("03"))
                    tLLRegisterSchema.setRgtState("02");
            } else {
                if (tLLRegisterSchema.getRgtState().equals("02"))
                    tLLRegisterSchema.setRgtState("03");
            }
            map.put(tLLRegisterSchema, "UPDATE");
            mInputData.add(map);
            mResult.add(mReturnMessage);
            tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, null)) {
                CError.buildErr(this,"数据提交失败!");
                return false;
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("--Star GetInputData--");
        mLLCaseSet = ((LLCaseSet) cInputData.getObjectByObjectName("LLCaseSet", 0));
        mRgtNo = mLLCaseSet.get(1).getRgtNo();
        mG = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        aDate = PubFun.getCurrentDate();
        aTime = PubFun.getCurrentTime();
        return true;
    }

    /**
     * 处理前检查数据是否合理
     * @return boolean
     */
    private boolean checkData(){
        System.out.println("---start checkData---");
        
        //受理申请（团体）审定、审批时，在数据校验阶段，需要添加社保用户校验流程。目的是向核赔类传入操作人
        String tSSFlag = LLCaseCommon.checkSocialSecurity(mRgtNo, "rgtno");
        if("1".equals(tSSFlag)){
        	LLSocialClaimUserDB tLLSocialClaimUserDB = new LLSocialClaimUserDB();
        	tLLSocialClaimUserDB.setUserCode(mG.Operator);
        	tLLSocialClaimUserDB.setStateFlag("1");//社保用户有效
			tLLSocialClaimUserDB.setHandleFlag("1");//参与案件分配
			LLSocialClaimUserSet tSocialClaimUserSet = tLLSocialClaimUserDB.query();
        	if(tSocialClaimUserSet.size() <= 0){
        		CError.buildErr(this,"您不是社保参与案件分配的审核人或已失效，无权作审定操作");
                return false;
        	}
        }else{
        	LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
            tLLClaimUserDB.setUserCode(mG.Operator);
            if(!tLLClaimUserDB.getInfo()){
                CError.buildErr(this,"您不是理赔人，无权作审定操作");
                return false;
            }
            if(tLLClaimUserDB.getClaimPopedom()==null){
                CError.buildErr(this,"您没有理赔权限，无权作审定操作");
                return false;
            }
            mLLClaimUserSchema = tLLClaimUserDB.getSchema();
            tLLClaimUserDB.setUserCode(mLLClaimUserSchema.getUpUserCode());
            if(tLLClaimUserDB.getInfo())
                mLLClaimUserSchema.setComCode(tLLClaimUserDB.getClaimPopedom());
        }
        
        LLCaseSet tLLCaseSet = mLLCaseSet;
        mLLCaseSet = new LLCaseSet();
        for(int i= 1;i<=tLLCaseSet.size();i++){
            LLCaseDB tLLCaseDB = new LLCaseDB();
            String tCaseNo = tLLCaseSet.get(i).getCaseNo();
            tLLCaseDB.setCaseNo(tCaseNo);
            String treason = tLLCaseSet.get(i).getCancleReason();
            String tremark = tLLCaseSet.get(i).getCancleRemark();
            if (!tLLCaseDB.getInfo()) {
                mReturnMessage += "<br>个人案件查询失败,案件号："+tCaseNo;
                continue;
            }
            if("11".equals(tLLCaseDB.getRgtState())||
               "12".equals(tLLCaseDB.getRgtState())){
                mReturnMessage += "<br>案件"+tCaseNo+"已经确认给付，不能再做审定操作。";
                continue;
            }
            tLLCaseDB.setCancleReason(treason);
            tLLCaseDB.setCancleRemark(tremark);
            mLLCaseSet.add(tLLCaseDB.getSchema());
        }
        if(mLLCaseSet.size()<=0){
            mReturnMessage +="<br>没有符合审定条件的案件。";
            CError.buildErr(this,mReturnMessage);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("---Star dealData---mOperate: " + mOperate);

            System.out.println("---Star PASS---" + mLLCaseSet.size());
            LLCaseSet tLLCaseSet = new LLCaseSet();
            tLLCaseSet.set(mLLCaseSet);

            for(int i=1;i<=tLLCaseSet.size();i++){
                LLCaseSchema tcase = tLLCaseSet.get(i);

                if(!UWCase(tcase))
                    continue;
            }

        CaseBack(mbackCaseSet);
        return true;
    }

    private boolean UWCase(LLCaseSchema aCaseSchema){
        VData tvdata = new VData();
        //此处为循环调用核赔类的入口，在此处判断此批次下案件是否为社保案件，若为社保案件，传入社保批次标志                
        mSocialSecurity = LLCaseCommon.checkSocialSecurity(aCaseSchema.getCaseNo(), null);
        ClaimUnderwriteBL tClaimUnderwriteBL = new ClaimUnderwriteBL();
        tvdata.clear();
        tvdata.add(aCaseSchema);
        tvdata.add(mLLClaimUserSchema);
        tvdata.add(mG);
        
        //将社保标记传入ClaimUnderwriteBL
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("mSocialSecurity", mSocialSecurity);
        tvdata.add(tTransferData);
        System.out.println("社保标记为："+mSocialSecurity);
        
        if(!tClaimUnderwriteBL.submitData(tvdata,"BATCH||UW")){
            CErrors tError = tClaimUnderwriteBL.mErrors;
            String ErrMessage = tError.getFirstError();
            mReturnMessage += "<br>案件" + aCaseSchema.getCaseNo()
                    + "审批失败,原因是:"+ErrMessage;
            return false;
        }
        VData tResultData = tClaimUnderwriteBL.getResult();
        String strResult = (String) tResultData.getObjectByObjectName("String", 0);
        mReturnMessage+="<br>"+aCaseSchema.getCaseNo()+strResult;
        return true;
    }
    /**
     * 案件回退
     * 1。审定人发现案件错误，做案件回退
     * 2。系统审定操作不能通过，做自动回退
     * @return boolean
     */
    private boolean CaseBack(LLCaseSet aLLCaseSet) {
        System.out.println("回退。。。"+aLLCaseSet.size());
        LLCaseBackSet tLLCaseBackSet = new LLCaseBackSet();
        LLCaseBackSet oLLCaseBackSet = new LLCaseBackSet();
        boolean tflag= false;
        for(int i=1;i<=aLLCaseSet.size();i++){
            LLCaseSchema tLLCaseSchema = aLLCaseSet.get(i);
            LLCaseBackSchema tLLCaseBackSchema = new LLCaseBackSchema();
            String oRgtState = ""+tLLCaseSchema.getRgtState();
            String oRgtStateName = "";
            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCode(oRgtState);
            tLDCodeDB.setCodeType("llrgtstate");
            if(tLDCodeDB.getInfo())
                oRgtStateName = tLDCodeDB.getCodeName();
            if(!oRgtState.equals("03")&&!oRgtState.equals("04")&&
               !oRgtState.equals("05")&&!oRgtState.equals("06")&&
               !oRgtState.equals("09")&&!oRgtState.equals("10")){
                //写报错信息返回
                mReturnMessage+="案件"+tLLCaseSchema.getCaseNo()+"当前为"
                        +oRgtStateName+",不能做回退操作<br>";
                continue;
            }
            String tLimit = PubFun.getNoLimit(this.mG.ManageCom);
            String CaseBackNo = PubFun1.CreateMaxNo("CaseBack", tLimit);

            tLLCaseBackSchema.setBeforState(oRgtState);
            tLLCaseBackSchema.setCaseBackNo(CaseBackNo);
            tLLCaseBackSchema.setAviFlag("Y");
            tLLCaseBackSchema.setRgtNo(tLLCaseSchema.getRgtNo());
            tLLCaseBackSchema.setCaseNo(tLLCaseSchema.getCaseNo());
            tLLCaseBackSchema.setOHandler(tLLCaseSchema.getHandler());
            tLLCaseBackSchema.setNHandler(tLLCaseSchema.getHandler());
            tLLCaseBackSchema.setAfterState("01");
            tLLCaseBackSchema.setReason(tLLCaseSchema.getCancleReason());
            tLLCaseBackSchema.setRemark(tLLCaseSchema.getCancleRemark());
            tLLCaseBackSchema.setMngCom(mG.ManageCom);
            tLLCaseBackSchema.setOperator(mG.Operator);
            tLLCaseBackSchema.setMakeDate(aDate);
            tLLCaseBackSchema.setMakeTime(aTime);
            tLLCaseBackSchema.setModifyDate(aDate);
            tLLCaseBackSchema.setModifyTime(aTime);

            tLLCaseBackSet.add(tLLCaseBackSchema);
            //保存后当前回退记录生效，需要把过去的回退记录设为过期
            LLCaseBackDB tLLCaseBackDB = new LLCaseBackDB();
            tLLCaseBackDB.setCaseNo(tLLCaseSchema.getCaseNo());
            tLLCaseBackDB.setAviFlag("Y");
            LLCaseBackSet tempLLCaseBackSet = new LLCaseBackSet();
            tempLLCaseBackSet = tLLCaseBackDB.query();
            for(int x=1;x<=tempLLCaseBackSet.size();x++){
                tempLLCaseBackSet.get(x).setAviFlag("");
                tempLLCaseBackSet.get(x).setModifyDate(aDate);
                tempLLCaseBackSet.get(x).setModifyTime(aTime);
            }
            oLLCaseBackSet.add(tempLLCaseBackSet);
            aLLCaseSet.get(i).setRgtState("01");
            aLLCaseSet.get(i).setCancleReason("");
            aLLCaseSet.get(i).setCancleRemark("");
            aLLCaseSet.get(i).setUWer(mG.Operator);
            aLLCaseSet.get(i).setDealer(mG.Operator);
            aLLCaseSet.get(i).setModifyDate(aDate);
            aLLCaseSet.get(i).setModifyTime(aTime);
            tflag = true;
        }
        if(tflag){
            map.put(aLLCaseSet, "UPDATE");
            map.put(tLLCaseBackSet, "INSERT");
            map.put(oLLCaseBackSet, "UPDATE");
        }
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLLRegisterSchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(mReturnMessage);
            mResult.add(this.mLLCaseSet);
        } catch (Exception ex) {
            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
