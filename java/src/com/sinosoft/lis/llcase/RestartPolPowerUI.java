package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: 民生人寿业务系统</p>
 * <p>Description:立案阶段的保单效力恢复程序
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @version 1.0
 */
public class RestartPolPowerUI
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData = new VData();
  private VData mResult = new VData();
  private String mOperate;

  public RestartPolPowerUI() {}
  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;
    RestartPolPowerBL mRestartPolPowerBL = new RestartPolPowerBL();

    System.out.println("---UI BEGIN---");
    if (mRestartPolPowerBL.submitData(cInputData,mOperate) == false)
    {
      this.mErrors.copyAllErrors(mRestartPolPowerBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "RestartPolPowerUI";
      tError.functionName = "submitData";
      tError.errorMessage = "保单效力恢复失败！";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  public static void main(String[] args)
  {
    VData tVData=new VData();
    GlobalInput tG = new GlobalInput();
    LDSysTraceSet aLDSysTraceSet = new LDSysTraceSet();
    for(int y=1;y<=3;y++)
    {
      LDSysTraceSchema aLDSysTraceSchema = new LDSysTraceSchema();

      String strLimit=PubFun.getNoLimit("001");
      aLDSysTraceSchema.setPolNo(PubFun1.CreateMaxNo("CASENO",strLimit));
      System.out.println(aLDSysTraceSchema.getPolNo());
      aLDSysTraceSchema.setOperator("001");
      aLDSysTraceSchema.setMakeDate("2003-01-20");
      aLDSysTraceSchema.setMakeTime("12:12:12");
      aLDSysTraceSchema.setOperator2("001");
      aLDSysTraceSchema.setModifyDate("2003-01-20");
      aLDSysTraceSchema.setModifyTime("12:12:12");
      aLDSysTraceSet.add(aLDSysTraceSchema);
    }
    tVData.addElement(aLDSysTraceSet);
    tVData.addElement(tG);

    RestartPolPowerUI tRestartPolPowerUI = new RestartPolPowerUI();
    tRestartPolPowerUI.submitData(tVData,"INSERT");
  }
}