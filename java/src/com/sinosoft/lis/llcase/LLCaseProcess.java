/**
 * 
 */
package com.sinosoft.lis.llcase;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.VData;

/**
 * 理赔案件流程父类，每一个案件步骤子类继承该类
 * 
 * @author maning
 * @version 1.0
 */
public abstract class LLCaseProcess {

	/**
	 * 
	 */
	public LLCaseProcess() {
	}

	/**
	 * 获取数据VData，经过业务处理，返回包含业务数据的VData
	 * 
	 * @param aInputData
	 *            包含获取的Schema、Set
	 * @param aOperate
	 * @return boolean 成功处理标志
	 * @throws Exception
	 *             所有的异常都有外层调用类处理
	 */
	public abstract boolean submitData(VData aInputData, String aOperate)
			throws Exception;

	/**
	 * 得到处理后的结果数据
	 * 
	 * @return VData
	 */
	public abstract VData getResult() throws Exception;

	/**
	 * 获取提交数据库的MMap
	 */
	public abstract MMap getMMap() throws Exception;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
