package com.sinosoft.lis.llcase;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.lis.schema.LLOutcoucingAverageSchema;
import com.sinosoft.lis.schema.LLOutsourcingTotalSchema;
import com.sinosoft.lis.vschema.LLOutcoucingAverageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
public class OutsourcingCostExportBL {
	
	/**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSqlTitle = null;
    private String mSql = null;
    private String mTitle = null;
    private String mOutXmlPath = null;
    //private String mtype = null;
    SSRS tSSRS=new SSRS();
    SSRS tSSRS1=new SSRS();
    SSRS tSSRS2=new SSRS();
	
	
	/**
    传输数据的公共方法
    */
   public boolean submitData(VData cInputData, String cOperate) {
       //得到外部传入的数据,将数据备份到本类中
       if (!getInputData(cInputData)) {
           return false;
       }
       //进行业务处理
       if (!dealData()) {
           return false;
       }
       
       return true;
   }
	
   
   /**
    * 从输入数据中得到所有对象
    *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    */
   private boolean getInputData(VData cInputData) {
	   TransferData tf = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
	   if(tf == null)
       {
           CError tError = new CError();
           tError.moduleName = "OutsourcingCostExportBL";
           tError.functionName = "getInputData";
           tError.errorMessage = "传入的信息不完整";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return false;
       }
	   
	   mSql = (String) tf.getValueByName("querySql");
       mSqlTitle = (String) tf.getValueByName("querySqlTitle");
       mTitle = (String) tf.getValueByName("Title");
       mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
       
       if(mSql == null || mOutXmlPath == null)
       {
           CError tError = new CError();
           tError.moduleName = "OutsourcingCostExportBL";
           tError.functionName = "getInputData";
           tError.errorMessage = "传入的信息不完整2";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return false;
       }
	   return true;
   }

   /**
    * 业务处理主函数
    * @return boolean
    */
   public boolean dealData() {
//	 报表表头 和 列名
       ExeSQL tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(mSqlTitle);

       if(tExeSQL.mErrors.needDealError())
       {
           System.out.println(tExeSQL.mErrors.getErrContent());

           CError tError = new CError();
           tError.moduleName = "OutsourcingCostExportBL";
           tError.functionName = "dealData";
           tError.errorMessage = "没有查询到需要下载的数据";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return false;
       }
       
       //数据
       tSSRS1 = tExeSQL.execSQL(mSql);       

       if(tExeSQL.mErrors.needDealError())
       {
           System.out.println(tExeSQL.mErrors.getErrContent());

           CError tError = new CError();
           tError.moduleName = "OutsourcingCostExportBL";
           tError.functionName = "dealData";
           tError.errorMessage = "没有查询到需要下载的数据";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return false;
       }
       
//     数据
       tSSRS2 = tExeSQL.execSQL(mTitle);       

       if(tExeSQL.mErrors.needDealError())
       {
           System.out.println(tExeSQL.mErrors.getErrContent());

           CError tError = new CError();
           tError.moduleName = "OutsourcingCostExportBL";
           tError.functionName = "dealData";
           tError.errorMessage = "没有查询到需要下载的数据";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return false;
       }
       
       
       String[][] mToExcel = new String[tSSRS1.getMaxRow() + 4][50];
       mToExcel[0][0] =  tSSRS2.GetText(1, 1);        

       for(int row = 1; row <= tSSRS.getMaxRow(); row++)
       {
           for(int col = 1; col <= tSSRS.getMaxCol(); col++)
           {
               mToExcel[row][col - 1] = tSSRS.GetText(row, col);
           }
       }
       
       for(int row = 1; row <= tSSRS1.getMaxRow(); row++)
       {
           for(int col = 1; col <= tSSRS1.getMaxCol(); col++)
           {
               mToExcel[row +1][col - 1] = tSSRS1.GetText(row, col);
           }
       }
      
       try
       {
           WriteToExcel t = new WriteToExcel("");
           t.createExcelFile();
           String[] sheetName ={PubFun.getCurrentDate()};
           t.addSheet(sheetName);
           t.setData(0, mToExcel);
           t.write(mOutXmlPath);
       }
       catch(Exception ex)
       {
           ex.toString();
           ex.printStackTrace();
       }

       return true;
	   
   }
	
  

}
