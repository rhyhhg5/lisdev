package com.sinosoft.lis.llcase;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class LLCaseStateDateBL {

	/**
	 * @param args
	 */
//	private static final Logger cLogger = Logger
	//		.getLogger(YibaotongServlet.class);
	
	public CErrors mCErrors = new CErrors();

	public LLCaseStateDateBL() {

	}

	public boolean dealData(String aStartDate, String aEndDate) {
		System.out.println("案件状态时效表历史数据导入开始。。。。。。");

		System.out.println("开始日期：" + aStartDate);
		if (aStartDate == null || "".equals(aStartDate)) {
			System.out.println("开始日期为空，不能进行处理");
			mCErrors.addOneError("开始日期为空，不能进行处理");
//			cLogger.error("开始日期为空，不能进行处理");
			return false;
		}

		if (!PubFun.checkDateForm(aStartDate)) {
			System.out.println("开始日期格式错误，不能进行处理");
			mCErrors.addOneError("开始日期格式错误，不能进行处理");
//			cLogger.error("开始日期格式错误，不能进行处理");
			return false;
		}

		System.out.println("结束日期：" + aEndDate);
//		cLogger.error("结束日期：" + aEndDate);
		if (aEndDate == null || "".equals(aEndDate)) {
			System.out.println("结束日期为空，不能进行处理");
			mCErrors.addOneError("结束日期为空，不能进行处理");
//			cLogger.error("结束日期为空，不能进行处理");
			return false;
		}

		if (!PubFun.checkDateForm(aEndDate)) {
			System.out.println("结束日期格式错误，不能进行处理");
			mCErrors.addOneError("结束日期格式错误，不能进行处理");
//			cLogger.error("结束日期格式错误，不能进行处理");
			return false;
		}

		// 处理日期
		String tDealDate = aStartDate;

		do {
			if (tDealDate == null || "".equals(tDealDate)) {
				System.out.println("处理日期为空");
				mCErrors.addOneError("处理日期为空");
//				cLogger.error("处理日期为空");
				return false;
			}

			System.out.println("本次处理日期：" + tDealDate);
			//			cLogger.error("本次处理日期：" + tDealDate);

			if (!catchData(tDealDate)) {
				return false;
			}

			tDealDate = addDay(tDealDate, 1);
			System.out.println("下次处理日期：" + tDealDate);
			//			cLogger.error("下次处理日期：" + tDealDate);
			System.out.println();
			System.out.println();
		} while (PubFun.calInterval(tDealDate, aEndDate, "D") >= 0);

		System.out.println("案件状态时效表历史数据导入结束。。。。。。");
		System.out.println("案件状态时效表历史数据导入结束日期：" + aEndDate);
		return true;
	}

	/**
	 * 进行数据导入
	 * 
	 * @param aDealDate
	 * @return
	 */
	private boolean catchData(String aDealDate) {
 /**	// 清空每日团体保单变化表
		String tLLCaseTraceSQL = "alter table LLCaseTrace activate not logged initially with empty table";

		System.out.println("开始清空" + aDealDate + "前一天每日结案案件变化表："
				+ tLLCaseTraceSQL);


		if (!pubSubmit(tLLCaseTraceSQL)) {
			System.out.println("执行失败：" + tLLCaseTraceSQL);
			mCErrors.addOneError("执行失败：" + tLLCaseTraceSQL);
		
			return false;
		}
		System.out.println("清空" + aDealDate + "前一天每日结案案件变化表成功");
		//		cLogger.error("清空" + aDealDate + "前一天每日团体保单变化表成功");

		// 导入每日结案案件变化表
		String tLLCaseTraceLoadSQL = "call db2inst1.loaddata('"
				+ "select caseno,''1'' from llcase where endcasedate>=''"
				//+ aDealDate
				+"2012-02-05'' and endcasedate<=''2012-02-10"
				+ "'' ','LLCaseTrace')";

		System.out.println("开始导入" + aDealDate + "每日结案案件变化表：" + tLLCaseTraceLoadSQL);
		//		cLogger.error("开始导入" + aDealDate + "每日团体保单变化表：" + tGrpContLoadSQL);

		if (!callProcedure(tLLCaseTraceLoadSQL)) {
			System.out.println("执行失败：" + tLLCaseTraceLoadSQL);
			mCErrors.addOneError("执行失败：" + tLLCaseTraceLoadSQL);
			//			cLogger.error("开始导入" + aDealDate + "每日团体保单变化表：" + tGrpContLoadSQL);
			return false;
		}
		System.out.println("导入" + aDealDate + "每日结案案件变化表成功");
		//		cLogger.error("导入" + aDealDate + "每日团体保单变化表成功");
*/
		  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String tdeleteSQL = "delete from LLCaseTrace a where othertype='1' ";

		System.out.println("开始删除中间表LLCaseTrace类型1数据："
				+ tdeleteSQL);
		//		cLogger.error("开始删除中间表" + aCaseNo + "数据：" + tGrpPremDataDeleteSQL);

		if (!pubSubmit(tdeleteSQL)) {
			System.out.println("执行失败：" + tdeleteSQL);
			mCErrors.addOneError("执行失败：" + tdeleteSQL);
			//			cLogger.error("执行失败：" + tGrpPremDataDeleteSQL);
			return false;
		}
		System.out.println("删除中间表LLCaseTrace类型1成功");
	
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// 导入中间表数据
		String tLLCaseTraceLoadSQL = "insert into LLCaseTrace select caseno,'1' from llcase where endcasedate>='"+aDealDate+"' and endcasedate<='"+aDealDate+"' ";
		System.out.println("开始导入" + aDealDate + "每日结案案件变化表：" + tLLCaseTraceLoadSQL);
		if (!pubSubmit(tLLCaseTraceLoadSQL)) {
			System.out.println("执行失败：" + tLLCaseTraceLoadSQL);
			mCErrors.addOneError("执行失败：" + tLLCaseTraceLoadSQL);
			return false;
		}
		System.out.println("导入" + aDealDate + "每日结案案件变化表成功");
	
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		 		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		String tSQL = "select otherno from LLCaseTrace where othertype='1' ";
		tSSRS = tExeSQL.execSQL(tSQL);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			System.out.println(aDealDate + "没有需要处理的案件");
			//			cLogger.error(aDealDate + "没有需要处理保单");
			return true;
		}
		for (int j = 1; j <= tSSRS.getMaxRow(); j++) {
			String tCaseNo = tSSRS.GetText(j, 1);
			if (!catchGrpContData(tCaseNo)) {
				return false;				
			}
		}

		return true;
	}
	
	public boolean catchGrpContData(String aCaseNo) {
		if (aCaseNo == null || "".equals(aCaseNo)) {
			System.out.println("为获取案件号失败");
			mCErrors.addOneError("为获取案件号失败");
			//			cLogger.error("为获取保单号");
			return false;
		}
		System.out.println("开始处理" + aCaseNo);
		//		cLogger.error("开始处理" + aCaseNo);
		
//		 删除中间表今天需要导入的数据
		String tGrpPremDataDeleteSQL = "delete from LLCaseStateDate a where caseno='"+aCaseNo+"' ";

		System.out.println("开始删除中间表" + aCaseNo + "数据："
				+ tGrpPremDataDeleteSQL);
		//		cLogger.error("开始删除中间表" + aCaseNo + "数据：" + tGrpPremDataDeleteSQL);

		if (!pubSubmit(tGrpPremDataDeleteSQL)) {
			System.out.println("执行失败：" + tGrpPremDataDeleteSQL);
			mCErrors.addOneError("执行失败：" + tGrpPremDataDeleteSQL);
			//			cLogger.error("执行失败：" + tGrpPremDataDeleteSQL);
			return false;
		}
		System.out.println("删除" + aCaseNo + "中间表成功");
		//		cLogger.error("删除" + aGrpContNo + "中间表成功");

		// 导入中间表数据
		String tGrpPremDataLoadSQL = "insert into LLCaseStateDate select c.caseno caseno,c.rgtdate rgtdate, "
			+"(select min(makedate)   from es_doc_main where busstype='LP' and doccode=c.caseno) EsdocDate, "
			+"(select min(makedate) from llfeemain where caseno=c.caseno ) FeemainDate, "
			+"( select min(enddate) from llcaseoptime where caseno=c.caseno  and rgtstate='03') RollCallDate, "
			+"( select min(enddate) from llcaseoptime where caseno=c.caseno  and rgtstate='05') APPROVALDATE1, "
			+"( select max(enddate) from llcaseoptime where caseno=c.caseno  and rgtstate='05' and exists(select 1  from llclaimuser where usercode=llcaseoptime.operator and comcode<>'86')) ApprovalDate2, "
			+"( select min(enddate) from llcaseoptime where caseno=c.caseno  and rgtstate='06') ValidationDate, "
			+"(select min(startdate) from llcaseoptime where caseno=c.caseno and rgtstate='07') StartSurveyDate, "
			+"(select max(enddate) from llcaseoptime where caseno=c.caseno and rgtstate='08') EndSurveyDate, "
			+"c.endcasedate EndCaseDate, "
			+"(select min(startdate) from llcaseoptime where caseno=c.caseno and rgtstate='10') SamplingDate, "
			+"(select min(makedate) from ljaget where otherno=c.caseno or otherno=c.rgtno) NoticeDate, "
			+"(select min(confdate) from ljaget where otherno=c.caseno or otherno=c.rgtno) PaymentDate, "
			+"(case when LF_Claimpopedom(c.caseno,'01')='01' then '1' else '2' end)  RightsFlag, "
			+"(case when LF_Claimpopedom(c.caseno,'06')='06' then '2' else '1' end) ComCaseFlag, "
			+"c.mngcom Managecom, "
			+"'' StandByString1, "
			+"current date StandByDate "
			+"from llcase c where 1=1  "
			+"and caseno='"
			+ aCaseNo
			+"' ";
		System.out.println("开始导入中间表" + aCaseNo + "数据："
				+ tGrpPremDataLoadSQL);
		if (!pubSubmit(tGrpPremDataLoadSQL)) {
			System.out.println("执行失败：" + tGrpPremDataLoadSQL);
			mCErrors.addOneError("执行失败：" + tGrpPremDataLoadSQL);
			return false;
		}
		System.out.println("导入" + aCaseNo + "中间表成功");
		//		cLogger.error("导入" + aGrpContNo + "中间表成功");
		return true;
	}

	/**
	 * 提交数据
	 * 
	 * @param tSQL
	 * @return
	 */
	private boolean pubSubmit(String aSQL) {
		ExeSQL tExeSQL = new ExeSQL();
		if (!tExeSQL.execUpdateSQL(aSQL)) {
			System.out.println("提交失败");
			//			cLogger.error("提交失败");
			return false;
		}
		return true;
	}

	private boolean callProcedure(String aSQL) {
		ExeSQL tExeSQL = new ExeSQL();
		if (!tExeSQL.callProcedure(aSQL)) {
			System.out.println("提交失败");
			//			cLogger.error("提交失败");
			return false;
		}
		return true;
	}

	private String addDay(String s, int n) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			Calendar cd = Calendar.getInstance();
			cd.setTime(sdf.parse(s));
			cd.add(Calendar.DATE, n);// 增加N天
			return sdf.format(cd.getTime());

		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String tStartDate = "2011-01-04";
		String tEndDate = "2011-01-04";
		LLCaseStateDateBL tLLCaseStateDateBL = new LLCaseStateDateBL();
		try {
			tLLCaseStateDateBL.dealData(tStartDate, tEndDate);
//			System.out.println();
		} catch (Exception ex) {
			System.out.println("每日结案案件历史数据导入错误");
		
			ex.printStackTrace();
		}
	}

}
