package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description: 理赔批量打印
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 *
 * @author Xx
 * @version 1.0
 */
public class NewLLBatchPrtExcelBL {
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private String mOperate;

	private GlobalInput mG = new GlobalInput();
    
    private String mCaseNo ;
    
    //文件存放路径
    private String mrealpath = "";
    //Excel打印工具类
    private Readhtml rh;
    //生成文件名
    private String mFileName;

	public NewLLBatchPrtExcelBL() {
	}

	private TransferData mPrintFactor = new TransferData();

	// add new parameters
	private int mCount = 0;


	/**
	 * 传输数据的公共方法
	 *
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		cInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;
		System.out.println("Operate:" + mOperate);
		if (!getInputData(cInputData)) {
			return false;
		}
		if (!dealData()) {
			return false;
		}
		return true;
	}

	public VData getResult() {
		return mResult;
	}

	private boolean getInputData(VData cInputData) {
		mPrintFactor = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
        mrealpath = (String) mPrintFactor.getValueByName("realpath");
		return true;
	}

	public int getCount() {
		return mCount;
	}

	/**
	 * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean dealData() {
		if (!prepareOutputData()) {
			return false;
		}
		return true;
	}

	private boolean prepareOutputData() {
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		//String tLimit = PubFun.getNoLimit(this.mG.ManageCom);
		String tBatchType = (String) mPrintFactor.getValueByName("BatchType");
		
        //理赔给付细目表（excel）
        String tDetailExcelPrt = (String) mPrintFactor.getValueByName("DetailExcelPrt");
        System.out.println("DetailExcelPrt:"+tDetailExcelPrt);
        System.out.println("BatchType:"+tBatchType);
		LLCaseSet tLLCaseSet = new LLCaseSet();
		if (tBatchType.equals("1")) {
			String tRgtNo = (String) mPrintFactor.getValueByName("RgtNo");
			if (tRgtNo == null || tRgtNo == "") {
				CError tError = new CError();
				tError.moduleName = "NewLLBatchPrtExcelBL";
				tError.functionName = "checkInputData";
				tError.errorMessage = "批次号为空出错";
				this.mErrors.addOneError(tError);
				return false;
			}	
				String tCountSQL = "select count(1) from llcase where rgtno='"
						+ tRgtNo + "' and rgtstate in ('11','12')";
				int tCount = Integer.parseInt((tExeSQL.getOneValue(tCountSQL)));
				if (tCount > 1000) {
					buildError("LLBatchPrtBL",
							"单次打印数量过大，可能造成打印异常，请减少打印数量到1000条以下。");
					return false;
				}

				String sqlstr = " select * from LLCase where RgtNo='" + tRgtNo
						+ "' and rgtstate in ('11','12') order by caseno";
				LLCaseDB tLLCaseDB = new LLCaseDB();
				tLLCaseSet = tLLCaseDB.executeQuery(sqlstr);
		}
		if (tBatchType.equals("2")) {
			String tScaseno = (String) mPrintFactor.getValueByName("SCaseNo");
			String tEcaseno = (String) mPrintFactor.getValueByName("ECaseNo");
			String tRgtDateS = (String) mPrintFactor.getValueByName("RgtDateS");
			String tRgtDateE = (String) mPrintFactor.getValueByName("RgtDateE");
			String tEndDateS = (String) mPrintFactor.getValueByName("EndDateS");
			String tEndDateE = (String) mPrintFactor.getValueByName("EndDateE");

			String tSQL = "";
			if (tScaseno != null && !"".equals(tScaseno)) {
				tSQL += " and substr(caseno,6)>=substr('" + tScaseno + "',6)";
			}
			
			if (tEcaseno != null && !"".equals(tEcaseno)) {
				tSQL += " and substr(caseno,6)<=substr('" + tEcaseno + "',6)";
			}
			
			if (tRgtDateS != null && !"".equals(tRgtDateS)) {
				tSQL += " and rgtdate >= '" + tRgtDateS + "'";
			}
			
			if (tRgtDateE != null && !"".equals(tRgtDateE)) {
				tSQL += " and rgtdate <= '" + tRgtDateE + "'";
			}
			
			if (tEndDateS != null && !"".equals(tEndDateS)&& tEndDateE != null && !"".equals(tEndDateE)) {
				tSQL+=" and exists(select 1 from ljagetclaim where otherno=llcase.caseno and othernotype='5'" 
							+" and feeoperationtype<>'FC' and opconfirmdate between '"+tEndDateS+"' and '"+tEndDateE+"')";	
			}
			
			if (!"".equals(tSQL)) {
				String tCountSQL = "select count(1) from llcase where rgtstate in ('11','12') and MngCom like '"
						+ mG.ManageCom + "%' " + tSQL;
				int tCount = Integer.parseInt((tExeSQL.getOneValue(tCountSQL)));
				if (tCount > 1000) {
					buildError("NewLLBatchPrtExcelBL",
							"单次打印数量过大，可能造成打印异常，请减少打印数量到1000条以下。");
					return false;
				}
								
				LLCaseDB tLLCaseDB = new LLCaseDB();
				String sql = "select * from llcase where rgtstate in ('11','12') and MngCom like '"
						+ mG.ManageCom + "%' " + tSQL + " order by CaseNo";
				System.out.println(sql);
				tLLCaseSet = tLLCaseDB.executeQuery(sql);

			}
		}
		if (tBatchType.equals("3")) {
			String tempstr = (String) mPrintFactor
					.getValueByName("CaseNoBatch");
			String tCaseNoBatch = tempstr.trim();
			while (tCaseNoBatch.length() >= 17) {
				LLCaseSchema tLLCaseSchema = new LLCaseSchema();
				//此处什么意义都没有，复制粘贴果然不靠谱
				int Cindex = tCaseNoBatch.indexOf("C")<0?(tCaseNoBatch.indexOf("R")<0?tCaseNoBatch.indexOf("S"):tCaseNoBatch.indexOf("R")):tCaseNoBatch.indexOf("C");			
				String tCaseNo = tCaseNoBatch.substring(Cindex, Cindex + 17);
				LLCaseDB tLLCaseDB = new LLCaseDB();
				tLLCaseDB.setCaseNo(tCaseNo);
				if (tLLCaseDB.getInfo()) {
					tLLCaseSchema.setSchema(tLLCaseDB.getSchema());
					if(!"11".equals(tLLCaseSchema.getRgtState()) && !"12".equals(tLLCaseSchema.getRgtState()) ){
						buildError("NewLLBatchPrtExcelBL",
						"案件不为通知或给付状态，不可打印给付细目表！");
						return false;
					}
					tLLCaseSet.add(tLLCaseSchema);
				}
				tCaseNoBatch = tCaseNoBatch.substring(Cindex + 17);
				
				if (tCaseNoBatch.length() >= 17 && tLLCaseSet.size() == 1000) {
					buildError("NewLLBatchPrtExcelBL",
							"单次打印数量过大，可能造成打印异常，请减少打印数量到1000条以下。");
					return false;
				}				
			}
		}
		// 新增类型4.用来支持从客户信箱中的打印 
		if (tBatchType.equals("4")) {
			String tCaseNo = (String) mPrintFactor
					.getValueByName("CaseNoBatch");
			LLCaseDB tLLCaseDB = new LLCaseDB();
			tLLCaseDB.setCaseNo(tCaseNo);
			if (tLLCaseDB.getInfo()) {
				if(!"11".equals(tLLCaseDB.getSchema().getRgtState()) && !"12".equals(tLLCaseDB.getSchema().getRgtState()) ){
					buildError("NewLLBatchPrtExcelBL",
					"案件不为通知或给付状态，不可打印给付细目表！");
					return false;
				}
				tLLCaseSet.add(tLLCaseDB.getSchema());
			}
		}
		
		// 获取了需要进行打印的所有LLCase信息后,下面进行处理
		int count = tLLCaseSet.size();
        if(count <= 0){
            buildError("NewLLBatchPrtExcelBL",
            "案件信息为空");
            return false;
        }
        //将批次下的生成文件名整合在String[]
        //#1869 （需求编号：2014-094）广东理赔给付细目表样式调整，对于广东的案件采用特殊的处理
        String[] tInputEntry=new String[count];       
		for (int i = 1; i <= count; i++) {
			String trgtstate = tLLCaseSet.get(i).getRgtState();
			if (!trgtstate.equals("11") && !trgtstate.equals("12")) {
				continue;
			} else {
                if ("on".equals(tDetailExcelPrt)) {
                	//广东需要将全部事件的信息记录到一个给付细目表中
                	String tFlag = tLLCaseSet.get(i).getCaseNo().substring(1, 3);//从案件号中截取机构
                	//广东机构：8644
                	if("44".equals(tFlag)){
                		ClaimDetailPrintExcelGDBL tClaimDetailPrintExcelGDBL = new ClaimDetailPrintExcelGDBL(); 
                        String outname="";
                		VData tVData = new VData();
                        TransferData PrintElement = new TransferData();
                        mCaseNo = tLLCaseSet.get(i).getCaseNo();
                        PrintElement.setNameAndValue("caseno", mCaseNo);
                        tVData.add(mG);
                        tVData.add(PrintElement);
                        
                        if (!tClaimDetailPrintExcelGDBL.submitData(tVData, null)) {
                            mErrors.addOneError(tClaimDetailPrintExcelGDBL.mErrors.getError(0));
                            if (!mErrors.getError(0).functionName.equals("saveData")) {
                                continue;
                            }else{
                                System.out.println("ClaimDetailPrintExcelGDBL报非中断错："
                                        + mErrors.getFirstError());
                            }
                        }
                        rh= new Readhtml();
                        outname = tClaimDetailPrintExcelGDBL.getMFilePath();
                        tInputEntry[i-1]=outname;
                        System.out.println("tZDInputEntry["+(i-1)+"]:"+tInputEntry[i-1]);
                     
                	}else{
                		//这里只允许打印理赔已经赔付的细目表,且打印方式为Excel create by houyd @ 2013-11-26                 
                        String SQL = "select distinct b.caserelano from llclaimdetail a ,llcaserela b where a.caseno ='"
                                + tLLCaseSet.get(i).getCaseNo()
                                + "' and a.Caseno = b.CaseNo  and a.CaseRelaNo = b.CaseRelaNo with ur";
                        System.out.println("SQL:"+SQL);
                        tSSRS = tExeSQL.execSQL(SQL);   
                        String caserelano = "";
                        String outname="";
                        String outpathname="";
                        int num = tSSRS.MaxRow; 
                        System.out.println("num:" + num);
                        //将案件下多个账单的生成文件名整合在String[]
                        String[] tZDInputEntry=new String[num];
                        
                        if (tSSRS != null && tSSRS.getMaxRow() > 0) {
                        	for (int k = 1; k <= num; k++) {
                            	//一个一个事件的打印账单
                                caserelano = tSSRS.GetText(k, 1);
                                mCaseNo = tLLCaseSet.get(i).getCaseNo();
                                System.out.println("CaseRelaNo:" + caserelano);
                                System.out.println("CaseNo:" + mCaseNo);
                                
                                VData tVData1 = new VData();
                                TransferData PrintElement = new TransferData();
                                PrintElement.setNameAndValue("caserelano", caserelano);
                                PrintElement.setNameAndValue("caseno", mCaseNo);
                                tVData1.add(mG);
                                tVData1.add(PrintElement);
                                
                                ClaimDetailPrintExcelBL tClaimDetailPrintExcelBL = new ClaimDetailPrintExcelBL();
                                if (!tClaimDetailPrintExcelBL.submitData(tVData1, null)) {
                                    mErrors.addOneError(tClaimDetailPrintExcelBL.mErrors.getError(0));
                                    if (!mErrors.getError(0).functionName.equals("saveData")) {
                                        continue;
                                    }else{
                                        System.out.println("ClaimDetailPrintExcelBL报非中断错："
                                                + mErrors.getFirstError());
                                    }
                                }
                                rh= new Readhtml();
                                rh.XmlParse(tClaimDetailPrintExcelBL.getInputStream());
                                String templatename = rh.getTempLateName();// 模板名字
                                System.out.println("templatename" + templatename);
                                String templatepathname = mrealpath + "f1print/picctemplate/"
                                        + templatename;// 模板名字和地址
                                System.out.println("templatepathname" + templatepathname);
                                outname = "LP" + caserelano+"_"+mCaseNo+ ".xls";
                                outpathname = mrealpath + "vtsfile/" 
                                        + outname;// 该文件目录必须存在,应该约定好,统一存放,便于定期做文件清理工作
                                rh.setReadFileAddress(templatepathname);
                                rh.setWriteFileAddress(outpathname);
                                rh.start("vtsmuch");
                                tZDInputEntry[k-1]=outpathname;
                                System.out.println("tZDInputEntry["+(k-1)+"]:"+tZDInputEntry[k-1]);
                            
                        	}                                             
	                    }else{
	                        buildError("NewLLBatchPrtExcelBL",
	        					"案件事件信息查询失败，不可打印给付细目表！");
	        				return false;
	                    }
                        System.out.println("案件下账单整合打印.zip");
                        mFileName=mCaseNo+"_"+PubFun.getCurrentDate()+".zip";
                        String tZipFile=mrealpath+ "vtsfile/"+mFileName;
                        rh.CreateZipFile(tZDInputEntry, tZipFile);
                        tInputEntry[i-1]=tZipFile;
                        System.out.println("tInputEntry["+(i-1)+"]:"+tInputEntry[i-1]);
	                }
                }
            } 
		}
        System.out.println("批次下案件整合打印.zip");
        mFileName="ZT_"+mCaseNo+"_"+PubFun.getCurrentDate()+".zip";
        String tZipFile=mrealpath+ "vtsfile/"+mFileName;
        rh.CreateZipFile(tInputEntry, tZipFile);
		return true;
	}

	public void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLBatchPrtBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {

		Readhtml rh = new Readhtml();
        String[] path = {"E:\\lisdev\\ui\\vtsfile\\LP86110000712673_.xls","E:\\lisdev\\ui\\vtsfile\\LP86110000712674_.xls"};
        String file = "E:\\lisdev\\ui\\vtsfile\\HYDTEST.zip";
        rh.CreateZipFile(path, file);
        System.out.println("OK");

	}
    public String getFileName() {
        return mFileName;
    }

}
