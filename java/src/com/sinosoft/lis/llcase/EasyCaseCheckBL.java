/************************************************
 * AppleWood 2005-04-27 在理算结束时增加自动责任匹配.
 *
 ************************************************/

package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author MN
 * @version 1.0
 */
public class EasyCaseCheckBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  private GlobalInput mGlobalInput = new GlobalInput();
  /**  */
  private LLCaseSchema  mLLCaseSchema = new LLCaseSchema();

  private LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();

  public EasyCaseCheckBL() {}

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;

   if (!checkData())
   {
       return false;
   }

    // 数据操作业务处理
    if (cOperate.equals("CHECKFINISH"))
    {
      if (!dealData())
        return false;

    }
   if ( cOperate.equals("FEERELASAVE"))
   {
       if (!dealFeeRelaSave())
       {
           return false;
       }
   }
    PubSubmit ps = new PubSubmit();
    if (!ps.submitData( this.mResult, null ))
    {
        CError.buildErr(this,"数据库保存失败");
        return false;
    }
    return true;
  }

  public VData getResult()
  {
        return mResult;
  }

  /**
   * 处理账单关联项目
   * @return boolean
   */
  private boolean dealFeeRelaSave()
{
    LLFeeMainSet tLLFeeMainSet = (LLFeeMainSet)this.mInputData.getObjectByObjectName("LLFeeMainSet",0);
    LLFeeMainSet saveFeeMainSet = new LLFeeMainSet();
    if ( tLLFeeMainSet == null || tLLFeeMainSet.size()<=0)
    {
        CError.buildErr(this,"传入数据集为空");
        return false;
    }
    LLFeeMainDB tdb = new LLFeeMainDB();
    for ( int i=1;i<= tLLFeeMainSet.size();i++)
    {
        LLFeeMainSchema inSchema = tLLFeeMainSet.get(i);
        tdb.setMainFeeNo( inSchema.getMainFeeNo());
        if (!tdb.getInfo())
        {
            CError.buildErr(this,"查询账单[" + inSchema.getMainFeeNo()+"]失败");
            return false;
        }
        LLFeeMainSchema dbSchema =  tdb.getSchema();
        dbSchema.setModifyDate( PubFun.getCurrentDate());
        dbSchema.setModifyTime( PubFun.getCurrentTime());
        dbSchema.setCaseRelaNo(inSchema.getCaseRelaNo());
      saveFeeMainSet.add(dbSchema);

    }
    this.mResult.clear();
    MMap tmpMap = new MMap();
    tmpMap.put( saveFeeMainSet, "UPDATE");
    this.mResult.add( tmpMap);
    return true;
}
  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealData()
  {
    LLCaseDB tLLCaseDB = new LLCaseDB();
    tLLCaseDB.setCaseNo( this.mLLCaseSchema.getCaseNo());
    if (!tLLCaseDB.getInfo() )
    {
        CError.buildErr(this,"理赔案件查询失败");
        return false;
    }
    String RgtState  =tLLCaseDB.getRgtState();
    String aOperator = mGlobalInput.Operator;
    if(!aOperator.equals(tLLCaseDB.getHandler())){
        CError tError = new CError();
        tError.moduleName = "CaseCheckBL";
        tError.functionName = "dealdata";
        tError.errorMessage = "您不是该案件指定的处理人，不能进行检录！";
        mErrors.addOneError(tError);
        return false;
    }
    if(!(RgtState.equals("01")||RgtState.equals("02")||RgtState.equals("08")))
    {
        CError tError = new CError();
        tError.moduleName = "CaseCheckBL";
        tError.functionName = "dealdata";
        tError.errorMessage = "该案件状态下不能检录！";
        mErrors.addOneError(tError);
        return false;
    }

    this.mLLCaseSchema.setSchema(tLLCaseDB.getSchema());
    //加校验--待做

    //更改状态
    this.mLLCaseSchema.setRgtState("03"); //审核完毕
    mLLCaseSchema.setHandleDate( PubFun.getCurrentDate() );
    mLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
    mLLCaseSchema.setModifyTime( PubFun.getCurrentTime());
    MMap tmpMap = new MMap();

    mLLCaseOpTimeSchema.setCaseNo(mLLCaseSchema.getCaseNo());
    mLLCaseOpTimeSchema.setRgtState("03");
    mLLCaseOpTimeSchema.setOperator(mGlobalInput.Operator);
    mLLCaseOpTimeSchema.setManageCom(mGlobalInput.ManageCom);
    LLCaseCommon tLLCaseCommon = new LLCaseCommon();
    try{
        LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(
                mLLCaseOpTimeSchema);
        tmpMap.put(tLLCaseOpTimeSchema, "DELETE&INSERT");
    }catch(Exception ex){
        System.out.println("没有时效记录");
    }
    tmpMap.put( this.mLLCaseSchema, "UPDATE");

    this.mResult.add( tmpMap );
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {


      mLLCaseSchema.setSchema((LLCaseSchema)cInputData.getObjectByObjectName("LLCaseSchema",0));
      mGlobalInput= (GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0);
    return true;

  }

  /**
   * 查询符合条件的暂交费信息
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */

  /**
   * 校验传入的暂交费收据号是否合法
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkData()
  {

      LLCaseDB tLLCaseDB = new LLCaseDB();
      tLLCaseDB.setCaseNo(mLLCaseSchema.getCaseNo());
      if (!tLLCaseDB.getInfo())
      {
          CError.buildErr(this, "没有查询到理赔案件");
          return false;
      }

        return true;

  }

  /**
   * 准备需要保存的数据
   */
  private void prepareOutputData()
  {



  }


  private LLToClaimDutySet getToClaimDutySet(String caseNo)
  {
    LLToClaimDutySet result = new LLToClaimDutySet();

    LLCaseRelaDB relaDB = new LLCaseRelaDB();
    relaDB.setCaseNo(caseNo);
    LLCaseRelaSet relaSet = relaDB.query();
    AutoClaimDutyMap autoClaimDutyMap = null;

    try
    {
      autoClaimDutyMap = (AutoClaimDutyMap) Class.forName("com.sinosoft.lis.llcase."+SysConst.AUTOCHOOSEDUTY).newInstance();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      mErrors.addOneError("加载AutoClaimDutyMap实现类时错误:Msg="+ex.getMessage());
      return null;
    }

    System.out.println(".........."+relaSet.size());

    for(int i=1;i<=relaSet.size();i++)
    {
      LLToClaimDutySet oneRela = autoClaimDutyMap.autoChooseGetDuty(caseNo,relaSet.get(i).getCaseRelaNo());
      result.add(oneRela);
    }

    System.out.println(".........."+result.size());

    return result;
  }

  public static void main(String[] args){
      GlobalInput mGlobalInput = new GlobalInput();
      mGlobalInput.ManageCom = "86";
      mGlobalInput.ComCode = "8611";
      mGlobalInput.Operator = "cm0004";
      LLCaseSchema tLLCaseSchema  = new LLCaseSchema();
      tLLCaseSchema.setCaseNo("C5300060329000031");
      VData tVData = new VData();
      tVData.addElement(mGlobalInput);
      tVData.addElement(tLLCaseSchema);
      EasyCaseCheckBL tCaseCheckBL   = new EasyCaseCheckBL();
      tCaseCheckBL.submitData(tVData,"CHECKFINISH");

  }

}
