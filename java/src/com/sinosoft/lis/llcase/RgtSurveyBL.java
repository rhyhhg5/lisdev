/*
 * <p>ClassName: RgtSurveyBL </p>
 * <p>Description: 提调、调查回复及调查复核 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author: Xx
 * @CreateDate：2005-02-23 11:53:36
 */
package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.lang.String;

public class RgtSurveyBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 提调机构调查主管 */
    private String mSurveyConfer;
    /** 业务处理相关变量 */
    private LLSurveySchema mLLSurveySchema = new LLSurveySchema();
    private LLInqApplySchema mLLInqApplySchema = new LLInqApplySchema();
    private LLInqCertificateSet mLLInqCertificateSet = new LLInqCertificateSet();
    private LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
    private int index;
    
    /**社保案件标记 0：非社保；1：社保*/
    private String mSocialFlag = "0";
//private LLSurveySet mLLSurveySet=new LLSurveySet();
    public RgtSurveyBL() {
    }

    public static void main(String[] args) {
        GlobalInput aGlobalInput = new GlobalInput();
        aGlobalInput.ComCode = "8611";
        aGlobalInput.Operator = "cm1101";
        aGlobalInput.ManageCom = "8611";
        LLSurveySchema tLLSurveySchema = new LLSurveySchema();
//        tLLSurveySchema.setSurveyNo("C11000604260000042");
        tLLSurveySchema.setOtherNo("C1100060405000002");
        tLLSurveySchema.setSubRptNo("0000");
        tLLSurveySchema.setStartPhase("0");
        tLLSurveySchema.setRgtState("01");
        tLLSurveySchema.setCustomerNo("000029100");
        tLLSurveySchema.setCustomerName("于春梅");
        tLLSurveySchema.setSurveyType("1");
        tLLSurveySchema.setContent("diaocha");
        tLLSurveySchema.setresult("result");
        tLLSurveySchema.setOHresult("OHresult");
//        tLLSurveySchema.setSurveyFlag("0");
        tLLSurveySchema.setSurveyClass("1");

        //    aLLSurveySchema.setSurveyNo("C31000604070000012");
        VData aVData = new VData();
        aVData.add(tLLSurveySchema);
        aVData.add(aGlobalInput);
        RgtSurveyBL tRgtSurveyBL = new RgtSurveyBL();
        tRgtSurveyBL.submitData(aVData, "INSERT||MAIN");
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mOperate = cOperate;
        System.out.println("mOperate：" + mOperate);

        if (!getInputData(cInputData)) {
            return false;
        }

        if (!dealData()) {
            CError tError = new CError();
            tError.moduleName = "RgtSurveyBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败RgtSurveyBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "RgtSurveyBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("End RgtSurveyBL Submit...");
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("BL dealData");
        String tDate = PubFun.getCurrentDate();
        String tTime = PubFun.getCurrentTime();
        String op = mGlobalInput.Operator;
        
        //调查申请
        if (mOperate.equals("INSERT||MAIN")) {
            if (!dealSurveyer()) {
                return false;
            }
//    String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
//    String SurveyNo = PubFun1.CreateMaxNo("SurveyNo", tLimit);
            LLSurveyDB tLLSurveyDB = new LLSurveyDB();
            LLSurveySet ttLLSurveySet = new LLSurveySet();
            String OtherNo = "";
            String SurveyFlag = "";
            String strSQL = "";
            SurveyFlag = mLLSurveySchema.getSurveyFlag();
            System.out.println("SurveyFlag:"+SurveyFlag);
            OtherNo = mLLSurveySchema.getOtherNo();
            String tOtherNoType = getNoType(OtherNo);
            tLLSurveyDB.setOtherNo(OtherNo);
            ttLLSurveySet.set(tLLSurveyDB.query());
            index = ttLLSurveySet.size() + 1;
            String SurveyNo = OtherNo + index;//调查号规则：案件号+i
            //调查号
            mLLSurveySchema.setSurveyNo(SurveyNo);
            mLLSurveySchema.setOtherNoType(tOtherNoType);
            if (!dealReport()) {
                System.out.println("事件生成失败了！");
            }
            //调查开始时间
            mLLSurveySchema.setSurveyStartDate(PubFun.getCurrentDate());
            mLLSurveySchema.setOperator(op);
            mLLSurveySchema.setMngCom(mGlobalInput.ComCode);
            mLLSurveySchema.setConfer(mSurveyConfer);
            mLLSurveySchema.setStartMan(mGlobalInput.Operator);
            mLLSurveySchema.setMakeDate(tDate);
            mLLSurveySchema.setMakeTime(tTime);
            mLLSurveySchema.setModifyDate(tDate);
            mLLSurveySchema.setModifyTime(tTime);

            String InqNo = SurveyNo;

            mLLInqApplySchema.setOtherNo(OtherNo);
            mLLInqApplySchema.setOtherNoType(tOtherNoType);
            mLLInqApplySchema.setSurveyNo(SurveyNo);
            mLLInqApplySchema.setInqNo(InqNo);
            mLLInqApplySchema.setInqDesc(mLLSurveySchema.getContent());
            mLLInqApplySchema.setInqState("0");
            mLLInqApplySchema.setDipatcher(mSurveyConfer);/** 转发人 */
            mLLInqApplySchema.setOperator(op);
            mLLInqApplySchema.setMakeDate(tDate);
            mLLInqApplySchema.setMakeTime(tTime);
            mLLInqApplySchema.setModifyDate(tDate);
            mLLInqApplySchema.setModifyTime(tTime);
            
            //在案件提调时，均不修改案件的handler，改为在查讫时统一修改，此处只更新案件状态和调查标记
            //add by Houyd #1738 社保调查
//            if (mLLSurveySchema.getRgtState().equals("0")) {
//                strSQL =
//                        "update llcase set RgtState='07',surveyflag='1' where caseno='" +
//                        OtherNo + "'";
//            } else {
//                strSQL =
//                        "update llcase set RgtState='07',surveyflag='1',handler='" +
//                         mGlobalInput.Operator + "' where caseno='" +
//                         OtherNo + "'";
//            }

            strSQL =
                    "update llcase set RgtState='07',surveyflag='1' where caseno='" +
                     OtherNo + "'";

            mLLCaseOpTimeSchema.setCaseNo(OtherNo);
            mLLCaseOpTimeSchema.setSequance(index);
            mLLCaseOpTimeSchema.setRgtState("07");

            LLCaseCommon tLLCaseCommon = new LLCaseCommon();
            LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(
                    mLLCaseOpTimeSchema);
            map.put(tLLCaseOpTimeSchema, "DELETE&INSERT");

            map.put(strSQL, "UPDATE");
            map.put(mLLSurveySchema, "INSERT");
            map.put(mLLInqApplySchema, "INSERT");
        }
        //保存调查
        if (mOperate.equals("INSERT||REPLY")) {
            System.out.println(mOperate);
            LLSurveyDB tLLSurveyDB = new LLSurveyDB();

            String tresult = "" + mLLSurveySchema.getresult();
            String tOHresult = "" + mLLSurveySchema.getOHresult();
            if (tresult.trim().equals("") && tOHresult.trim().equals("")) {
                CError tError = new CError();
                tError.moduleName = "RgtSurveyBL";
                tError.functionName = "dealData";
                tError.errorMessage = "没有填写任何调查信息！";
                this.mErrors.addOneError(tError);
                return false;
            } else {
                if (tresult.trim().equals("")) {
                    mLLSurveySchema.setSurveyType("1"); //院外调查
                } else if (tOHresult.trim().equals("")) {
                    mLLSurveySchema.setSurveyType("0"); //院内调查
                } else {
                    mLLSurveySchema.setSurveyType("2"); //院内外调查
                }
            }
            tLLSurveyDB.setSurveyNo(mLLSurveySchema.getSurveyNo());
            LLSurveySet tLLSurveySet = new LLSurveySet();
            tLLSurveySet = tLLSurveyDB.query();
            if (tLLSurveySet.size() <= 0) {
                CError tError = new CError();
                tError.moduleName = "RgtSurveyBL";
                tError.functionName = "dealData";
                tError.errorMessage = "提调信息查询失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
            LLSurveySchema tLLSurveySchema = tLLSurveySet.get(1);
            if (tLLSurveySchema.getSurveyFlag().equals("3")) {
                CError tError = new CError();
                tError.moduleName = "RgtSurveyBL";
                tError.functionName = "dealData";
                tError.errorMessage = "该案件已复核完毕！";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            //添加校验，不是案件的调查员，无法做“调查回复” 判断逻辑llinqapply.inqper
            LLInqApplyDB tLLInqApplyDB = new LLInqApplyDB();
            tLLInqApplyDB.setSurveyNo(mLLSurveySchema.getSurveyNo());
            tLLInqApplyDB.setInqNo(mLLSurveySchema.getSurveyNo());
            LLInqApplySet tLLInqApplySet = new LLInqApplySet();
            tLLInqApplySet = tLLInqApplyDB.query();
            if (tLLInqApplySet.size() <= 0) {
                CError tError = new CError();
                tError.moduleName = "RgtSurveyBL";
                tError.functionName = "dealData";
                tError.errorMessage = "调查分配信息查询失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
            LLInqApplySchema tLLInqApplySchema = tLLInqApplySet.get(1);
            if (!this.mGlobalInput.Operator.equals(tLLInqApplySchema.getInqPer())) {
                CError tError = new CError();
                tError.moduleName = "RgtSurveyBL";
                tError.functionName = "dealData";
                tError.errorMessage = "调查回复人与指定的案件调查人不符，该案件调查员为："+tLLInqApplySchema.getInqPer();
                this.mErrors.addOneError(tError);
                return false;
            }
            
            
            if(mLLSurveySchema.getContent()!=null && !"null".equals(mLLSurveySchema.getContent())){
            tLLSurveySchema.setContent(mLLSurveySchema.getContent());
            }else{
            tLLSurveySchema.setContent("");
            }
            if(mLLSurveySchema.getresult()!=null && !"null".equals(mLLSurveySchema.getresult())){
            tLLSurveySchema.setresult(mLLSurveySchema.getresult());
            }else{
            tLLSurveySchema.setresult("");
            }
            if(mLLSurveySchema.getOHresult()!=null && !"null".equals(mLLSurveySchema.getOHresult())){
            tLLSurveySchema.setOHresult(mLLSurveySchema.getOHresult());
            }else{
            tLLSurveySchema.setOHresult("");
            }
            tLLSurveySchema.setSurveyOperator(op);
            tLLSurveySchema.setSurveyFlag("2");
            tLLSurveySchema.setModifyDate(tDate);
            tLLSurveySchema.setModifyTime(tTime);
            tLLSurveySchema.setOperator(op);
            map.put(tLLSurveySchema, "UPDATE");
            LLInqCertificateSet tLLInqCertificateSet = new LLInqCertificateSet();
            LLInqCertificateSet xLLInqCertificateSet = new LLInqCertificateSet();
            LLInqCertificateDB tLLInqCertificateDB = new LLInqCertificateDB();
            tLLInqCertificateDB.setSurveyNo(mLLSurveySchema.getSurveyNo());
            xLLInqCertificateSet = tLLInqCertificateDB.query();
            map.put(xLLInqCertificateSet, "DELETE");
            if (mLLInqCertificateSet != null) {

                int CerSize = mLLInqCertificateSet.size();
                for (int j = 1; j <= CerSize; j++) {
                    LLInqCertificateSchema tLLInqCertificateSchema = new
                            LLInqCertificateSchema();
                    tLLInqCertificateSchema.setSchema(mLLInqCertificateSet.get(
                            j));
                    if (tLLInqCertificateSchema.getCerCount() <= 0) {
                        tLLInqCertificateSchema.setCerCount(1);
                    }
                    tLLInqCertificateSchema.setOperator(op);
                    tLLInqCertificateSchema.setMakeDate(tDate);
                    tLLInqCertificateSchema.setMakeTime(tTime);
                    tLLInqCertificateSchema.setModifyDate(tDate);
                    tLLInqCertificateSchema.setModifyTime(tTime);
                    tLLInqCertificateSet.add(tLLInqCertificateSchema);
                }
            }
            map.put(tLLInqCertificateSet, "INSERT");
            String SurveyNo = mLLSurveySchema.getSurveyNo();
            mLLCaseOpTimeSchema.setSequance(Integer.parseInt(SurveyNo.substring(
                    17)));
            mLLCaseOpTimeSchema.setCaseNo(mLLSurveySchema.getOtherNo());
            mLLCaseOpTimeSchema.setRgtState("07");
            mLLCaseOpTimeSchema.setManageCom(mGlobalInput.ManageCom);
            mLLCaseOpTimeSchema.setOperator(mGlobalInput.Operator);
            LLCaseCommon tLLCaseCommon = new LLCaseCommon();
            LLCaseOpTimeSchema xLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
            xLLCaseOpTimeSchema.setSchema(tLLCaseCommon.CalTimeSpan(
                    mLLCaseOpTimeSchema));
            map.put(xLLCaseOpTimeSchema, "DELETE&INSERT");
        }
        //保存审核
        if (mOperate.equals("INSERT||CONF")) {
            System.out.println(mOperate);
            String flag = "";
            LLSurveyDB tLLSurveyDB = new LLSurveyDB();
            LLSurveySet tLLSurveySet = new LLSurveySet();
            tLLSurveyDB.setSurveyNo(mLLSurveySchema.getSurveyNo());
            tLLSurveySet = tLLSurveyDB.query();
            if (tLLSurveySet.size() <= 0) {
                CError.buildErr(this, "调查信息查询失败");
                return false;
            }
            LLSurveySchema saveLLSurveySchema = new LLSurveySchema();
            saveLLSurveySchema = tLLSurveySet.get(1);
            String OtherNo = "" + mLLSurveySchema.getOtherNo();
            String SurveyFlag = "" + mLLSurveySchema.getSurveyFlag();
            System.out.println("xxxxxx" + SurveyFlag);
            if (SurveyFlag.equals("0")) {
                flag = "3"; //同意调查结论
            } else {
                flag = "1"; //不同意调查结论
            }
            if (saveLLSurveySchema.getSurveyFlag().equals("3")) {
                CError tError = new CError();
                tError.moduleName = "RgtSurveyBL";
                tError.functionName = "dealData";
                tError.errorMessage = "该案件已复核完毕！";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            //添加校验，只有该调查案件的审核人（llsurvey.confer）可以做审核操作	#1738 社保调查
            System.out.println("修改调查案件审核人校验");
            LLInqApplyDB tLLInqApplyDB = new LLInqApplyDB();
            LLInqApplySet tLLInqApplySet = new LLInqApplySet();
            tLLInqApplyDB.setSurveyNo(mLLSurveySchema.getSurveyNo());
            tLLInqApplyDB.setInqNo(mLLSurveySchema.getSurveyNo());
            tLLInqApplySet = tLLInqApplyDB.query();
            if (tLLInqApplySet.size() <= 0) {
                CError.buildErr(this, "调查回复信息查询失败");
                return false;
            }
            
            if (!mGlobalInput.Operator.equals(tLLInqApplySet.get(1).getDipatcher())) {
                CError tError = new CError();
                tError.moduleName = "RgtSurveyBL";
                tError.functionName = "dealData";
                tError.errorMessage = "无权审核该案件，该案件指定的审核人为："+tLLInqApplySet.get(1).getDipatcher();
                this.mErrors.addOneError(tError);
                return false;
            }
            
            if(mLLSurveySchema.getConfNote()!=null && !"null".equals(mLLSurveySchema.getConfNote())){
            	saveLLSurveySchema.setConfNote(mLLSurveySchema.getConfNote());
            }else{
            	saveLLSurveySchema.setConfNote("");
            }
            saveLLSurveySchema.setModifyDate(tDate);
            saveLLSurveySchema.setModifyTime(tTime);
            saveLLSurveySchema.setOperator(op);
            saveLLSurveySchema.setSurveyFlag(flag);
            saveLLSurveySchema.setSurveyEndDate(tDate);
            map.put(saveLLSurveySchema, "UPDATE");
            String strSQL1 = "";
//                String strSQL2 = "";
            
            //校验除了本次审核的调查案件外，是否还存在其他未完成的此理赔案件的调查
            //对于社保部的案件，在“受理”提调，不修改handler，原因是：“受理”的提调人为“运营部成员”
            //判断案件是否为社保案件
            String tCaseNo = "";
            if("R".equals(OtherNo.substring(0,1)) || "S".equals(OtherNo.substring(0,1)))
        	{
        		System.out.println("申诉纠错案件换为正常的C案件");
                String sql = "select caseno from LLAppeal where appealno ='"+OtherNo+"'";
                ExeSQL exeSQL = new ExeSQL();
                tCaseNo = exeSQL.getOneValue(sql);
        	}else{
        		tCaseNo = OtherNo;
        	}
            String mSocialSecurity = LLCaseCommon.checkSocialSecurity(tCaseNo, null);
            String mRgtState = saveLLSurveySchema.getRgtState();

            //本次修改重点来了，将一开始修改的handler转移到审核完毕后修改。llsurvey.startMan 对应 llcase.handler
            String tSQL = "select * from llsurvey where otherno='" +
                          OtherNo +
                          "' and surveyno <> '" + mLLSurveySchema.getSurveyNo() +
                          "'  and ( surveyflag <> '3' or surveyflag is null) ";
            SSRS tSSRS = new SSRS();
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(tSQL);
            if (SurveyFlag.equals("null") || SurveyFlag.equals("") ||
                SurveyFlag.equals("1") || tSSRS.getMaxRow() > 0) {
                strSQL1 = "update llcase set RgtState='07' where caseno='" +
                          OtherNo + "'";
            } else {
            	if("01".equals(mRgtState)&&"1".equals(mSocialSecurity)){
            		//案件提调状态为“受理”、且为社保案件
            		strSQL1 = "update llcase set RgtState='08' where caseno='" +
                    OtherNo + "'";
            	}else{
                    strSQL1 = "update llcase set RgtState='08',handler='"+saveLLSurveySchema.getStartMan()+"' where caseno='" +
                    OtherNo + "'";
            	}
            }
            String SurveyNo = mLLSurveySchema.getSurveyNo();

            LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
            if (SurveyFlag.equals("0")) {
                tLLCaseOpTimeSchema.setCaseNo(OtherNo);
                tLLCaseOpTimeSchema.setSequance(Integer.parseInt(SurveyNo.
                        substring(17)));
                tLLCaseOpTimeSchema.setManageCom(mGlobalInput.ManageCom);
                tLLCaseOpTimeSchema.setOperator(mGlobalInput.Operator);
                tLLCaseOpTimeSchema.setOpTime("0:0:0");
                tLLCaseOpTimeSchema.setStartDate(PubFun.getCurrentDate());
                tLLCaseOpTimeSchema.setStartTime(PubFun.getCurrentTime());
                tLLCaseOpTimeSchema.setEndDate(PubFun.getCurrentDate());
                tLLCaseOpTimeSchema.setEndTime(PubFun.getCurrentTime());
                tLLCaseOpTimeSchema.setRgtState("08");
            } else {

                mLLCaseOpTimeSchema.setCaseNo(OtherNo);
                mLLCaseOpTimeSchema.setSequance(Integer.parseInt(SurveyNo.
                        substring(17)));
                mLLCaseOpTimeSchema.setRgtState("07");
                LLCaseCommon tLLCaseCommon = new LLCaseCommon();
                tLLCaseOpTimeSchema = tLLCaseCommon.
                                      CalTimeSpan(mLLCaseOpTimeSchema);
            }
            map.put(tLLCaseOpTimeSchema, "DELETE&INSERT");
            map.put(strSQL1, "UPDATE");
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
//    private boolean updateData() {
//        return true;
//    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
//    private boolean deleteData() {
//        return true;
//    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("BL Star GetInput");
        this.mLLSurveySchema.setSchema((LLSurveySchema) cInputData.
                                       getObjectByObjectName("LLSurveySchema",
                0));
        this.mLLInqCertificateSet.set((LLInqCertificateSet) cInputData.
                                      getObjectByObjectName(
                                              "LLInqCertificateSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        if (!mLLSurveySchema.getOtherNo().substring(0, 1).equals("T") && !mLLSurveySchema.getOtherNo().substring(0, 1).equals("Z")) {
            //非咨询、通知类的调查
        	LLCaseDB tLLCaseDB = new LLCaseDB();
            tLLCaseDB.setCaseNo(mLLSurveySchema.getOtherNo());
            if (!tLLCaseDB.getInfo()) {
                CError tError = new CError();
                tError.moduleName = "RgtSurveyBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "案件信息查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            if (!LLCaseCommon.checkHospCaseState(tLLCaseDB.getCaseNo())) {
                CError.buildErr(this, "医保通案件待确认，不能进行处理");
                return false;
            }

            String tRgtState = "" + tLLCaseDB.getRgtState();
            if (mOperate.equals("INSERT||MAIN")) {
            	if (tRgtState.equals("07") || tRgtState.equals("09") || tRgtState.equals("11") ||
                        tRgtState.equals("12") || tRgtState.equals("14") ||
                        tRgtState.equals("13") || tRgtState.equals("16")) {
                        CError tError = new CError();
                        tError.moduleName = "RgtSurveyBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "该案件状态下，不允许做提调或调查回复操作!";
                        this.mErrors.addOneError(tError);
                        return false;
                }
            } else {
            	if (!tRgtState.equals("07")) {
            		CError tError = new CError();
                	tError.moduleName = "RgtSurveyBL";
                	tError.functionName = "getInputData";
                	tError.errorMessage = "该案件状态下，不允许做调查回复或审核操作!";
                	this.mErrors.addOneError(tError);
                	return false;
            	}
            }
            
            if (this.mGlobalInput.ComCode.length() <= 4) {
                this.mGlobalInput.ComCode = tLLCaseDB.getMngCom();
                this.mGlobalInput.ManageCom = tLLCaseDB.getMngCom();
            }
            
            String tCaseNo = "";
         	if("R".equals(tLLCaseDB.getCaseNo().substring(0,1)) || "S".equals(tLLCaseDB.getCaseNo().substring(0,1)))
         	{
         		System.out.println("申诉纠错案件换为正常的C案件");
         		String sql = "select caseno from LLAppeal where appealno ='"+tLLCaseDB.getCaseNo()+"'";
         		ExeSQL exeSQL = new ExeSQL();
         		tCaseNo = exeSQL.getOneValue(sql);
         	}else{
         		tCaseNo = tLLCaseDB.getCaseNo();
         	}
            mSocialFlag = LLCaseCommon.checkSocialSecurity(tCaseNo, null);
            
            //社保案件只能由社保用户提起和回复
            if("1".equals(mSocialFlag)){
            	String checkSql = "Select '1' From LLSocialClaimUser where UserCode = '"+this.mGlobalInput.Operator+"' and StateFlag = '1'";
            	ExeSQL tSSExeSQL = new ExeSQL();
            	String tOper_Result = tSSExeSQL.getOneValue(checkSql);
    	  		if(tOper_Result!=null&&"1".equals(tOper_Result))
    	  		{
    		  
    	  		}else{
    			  
    			  CError tError = new CError();
    		      tError.moduleName = "GrpRegisterBL";
    		      tError.functionName = "checkData";
    		      tError.errorMessage = "您不具有社保案件调查操作权限！";
    		      this.mErrors.addOneError(tError);
    			  return false;  
    		  }		  
            }
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LLSurveyDB tLLSurveyDB = new LLSurveyDB();
        tLLSurveyDB.setSchema(this.mLLSurveySchema);
        //如果有需要处理的错误，则返回
        if (tLLSurveyDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLLSurveyDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLSurveyBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        System.out.println("BL prepareOutputData");
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLLSurveySchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLLSurveySchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLSurveyBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    
    /**
     * 调查案件分配到调查主管下
     * @return
     */
    private boolean dealSurveyer() {
        System.out.println("get survey administrator...");
        String strMngCom = mGlobalInput.ManageCom;
        String tMngCom = "";
        if(strMngCom.length()<=2){
        	strMngCom = "86000000";
        }else{
        	strMngCom = mGlobalInput.ManageCom;
        }
               
        //对于社保案件支持案件调查，提起调查后案件分给社保部调查主管，支持申诉、纠错案件
        if("1".equals(mSocialFlag) || 
        		(("sc".equals(mGlobalInput.Operator.substring(0, 2))
        				||"ss".equals(mGlobalInput.Operator.substring(0, 2))
        				||"si".equals(mGlobalInput.Operator.substring(0, 2)))
        		  &&("T".equals(mLLSurveySchema.getOtherNo().substring(0, 1))
        		  		|| "Z".equals(mLLSurveySchema.getOtherNo().substring(0, 1)))
        		)
        	){
        	LLSocialClaimUserDB tSocialClaimUserDB = new LLSocialClaimUserDB();
        	LLSocialClaimUserSet tSocialClaimUserSet = new LLSocialClaimUserSet();
        	//分公司或支公司，调查主管、状态有效的社保部人员
        	String tSSQL = "select * from llsocialclaimuser where surveyflag='1' and" +
        			" stateflag='1' and handleflag='1' and (comcode='"+strMngCom+
        			"' or comcode='"+strMngCom.substring(0, 4)+"')";
        	System.out.println("tSSQL:"+tSSQL);
        	tSocialClaimUserSet = tSocialClaimUserDB.executeQuery(tSSQL);
        	if(tSocialClaimUserSet.size() <= 0){
        		 // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LLSurveyBL";
                tError.functionName = "dealSurveyer";
                tError.errorMessage = "社保部分公司无调查主管，不能提调";
                this.mErrors.addOneError(tError);
                return false;
        	}else if(tSocialClaimUserSet.size() < 2){
        		mSurveyConfer = tSocialClaimUserSet.get(1).getUserCode();
                tMngCom = tSocialClaimUserSet.get(1).getComCode();
                System.out.println("社保调查主管所属机构："+tMngCom);
        	}else{
        		//存在多个调查主管，首先任意分配一人
        		mSurveyConfer = tSocialClaimUserSet.get(1).getUserCode();
        		//按照案件受理的机构，优先选择该机构下的调查主管
        		for (int i = 1; i <= tSocialClaimUserSet.size(); i++) {
                    if (tSocialClaimUserSet.get(i).getComCode().equals(strMngCom)) {
                        mSurveyConfer = tSocialClaimUserSet.get(i).getUserCode();
                        tMngCom = strMngCom;
                        System.out.println("社保调查主管所属机构："+tMngCom);
                        break;
                    }
                }
        	}
        }else{
        	LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
            LLClaimUserSet tLLClaimUserSet = new LLClaimUserSet();
            String strSQL = "select * from llclaimuser where SurveyFlag='1' and "
                            + " stateflag='1' and (Comcode = '" + strMngCom +
                            "' or comcode = '" + strMngCom.substring(0, 4) + "')";
        	System.out.println("strSQL:"+strSQL);
            tLLClaimUserSet = tLLClaimUserDB.executeQuery(strSQL);
            if (tLLClaimUserSet.size() <= 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LLSurveyBL";
                tError.functionName = "dealSurveyer";
                tError.errorMessage = "分公司无调查主管，不能提调";
                this.mErrors.addOneError(tError);
                return false;
            } else if (tLLClaimUserSet.size() < 2) {
                mSurveyConfer = tLLClaimUserSet.get(1).getUserCode();
                tMngCom = tLLClaimUserSet.get(1).getComCode();
                System.out.println("调查主管所属机构："+tMngCom);
            } else {
                mSurveyConfer = tLLClaimUserSet.get(1).getUserCode();
                for (int i = 1; i <= tLLClaimUserSet.size(); i++) {
                    if (tLLClaimUserSet.get(i).getComCode().equals(strMngCom)) {
                        mSurveyConfer = tLLClaimUserSet.get(i).getUserCode();
                        tMngCom = strMngCom;
                        System.out.println("调查主管所属机构："+tMngCom);
                        break;
                    }
                }
            }
        }
        
        /**String sqlpart = "";
                 if (tMngCom.equals(strMngCom)) {
            sqlpart = "')";
                 } else {
            sqlpart = "' or comcode='" + tMngCom + "')";
                 }
                 strSQL = "select * from llclaimuser where SurveyFlag='2' and "
                 + " stateflag='1' and (Comcode = '" + strMngCom + sqlpart;
                 System.out.println(strSQL);
                 tLLClaimUserSet = new LLClaimUserSet();
                 tLLClaimUserSet = tLLClaimUserDB.executeQuery(strSQL);
                 if (tLLClaimUserSet.size() <= 0) {
            mLLInqApplySchema.setInqDept(strMngCom);
            mLLInqApplySchema.setInqPer(mSurveyConfer);
            mLLInqApplySchema.setLocFlag("0");
            mLLInqApplySchema.setInqStartDate(PubFun.getCurrentDate());
            mLLInqApplySchema.setConPer(mSurveyConfer);
                 }
         */
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
    
    /**
     * 通过案件号，判断是0-咨询通知类调查；1-普通案件调查；2-申诉纠错案件调查
     * 将咨询通知类案件的rgtstate置为03-检录	add by Houyd
     * @param OtherNo
     * @return
     */
    private String getNoType(String OtherNo) {
        if ("Z".equals(OtherNo.substring(0, 1)) ||
            "T".equals(OtherNo.substring(0, 1))) {
        	mLLSurveySchema.setRgtState("03");
            return "0";
        } else if ("C".equals(OtherNo.substring(0, 1))) {
            return "1";
        } else {
            return "2";
        }
    }

    /**
     * 将客户出险的事件信息整理到调查的”事故描述“
     * @return
     */
    private boolean dealReport() {
        LLCaseRelaSet tLLCaseRelaSet = new LLCaseRelaSet();
        LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
        LLSubReportDB tLLSubReportDB = new LLSubReportDB();
        String AccDate = "";
        String AccPlace = "";
        String InHosDate = "";
        String OutHosDate = "";
        String AccDesc = "";
        String AccidentDesc = "";
        tLLCaseRelaDB.setCaseNo(mLLSurveySchema.getOtherNo());
        tLLCaseRelaSet = tLLCaseRelaDB.query();
        if (tLLCaseRelaSet == null || tLLCaseRelaSet.size() <= 0) {
            mLLSurveySchema.setAccidentDesc("无该客户的出险事件信息");
        } else {
            for (int i = 1; i <= tLLCaseRelaSet.size(); i++) {
                tLLSubReportDB.setSubRptNo(tLLCaseRelaSet.get(i).getSubRptNo());
                if (tLLSubReportDB.getInfo()) {
                    AccDate = tLLSubReportDB.getAccDate();
                    AccPlace = tLLSubReportDB.getAccPlace();
                    InHosDate = tLLSubReportDB.getInHospitalDate();
                    OutHosDate = tLLSubReportDB.getOutHospitalDate();
                    AccDesc = tLLSubReportDB.getAccDesc();
                    String CName = tLLSubReportDB.getCustomerName();
                    System.out.println("姓名：" + CName);
                    AccidentDesc = AccidentDesc + "事件" + i + "： 被保险人" ;
                    if(!"null".equals(CName)&&CName!=null){
                    	AccidentDesc=AccidentDesc+CName;
                    }
                    System.out.println("姓名：" + AccidentDesc);
                    if(!"null".equals(AccDate) && AccDate!=null){
                    	AccidentDesc=AccidentDesc+AccDate;
                    	System.out.println(AccidentDesc);
                    }
                    if(!"null".equals(AccDesc) && AccDesc!=null){
                    	AccidentDesc=AccidentDesc+AccDesc;
                    	System.out.println(AccidentDesc);
                    }
                    if(!"null".equals(InHosDate)&& InHosDate!=null){
                    	System.out.println(InHosDate);
                    	AccidentDesc=AccidentDesc+",于"+InHosDate;
                    	System.out.println(AccidentDesc);
                    }
                    if(!"".equals(InHosDate)&&!"null".equals(InHosDate)&&!"null".equals(OutHosDate)&& InHosDate!=null && OutHosDate!=null){
                    	AccidentDesc=AccidentDesc+"至"+OutHosDate;
                    	System.out.println(AccidentDesc);
                    }
                    AccidentDesc = AccidentDesc+ "就诊";
                    if(!"null".equals(AccPlace) && AccPlace!=null){
                    	AccidentDesc=AccidentDesc+AccPlace+".\n";
                    }else{
                    	AccidentDesc=AccidentDesc+".\n";
                    }
                	System.out.println(AccidentDesc);
                    mLLSurveySchema.setAccidentDesc(AccidentDesc);
                }
            }
        }
        return true;
    }
}
