package com.sinosoft.lis.llcase;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
public class CaseCureQueryUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  public CaseCureQueryUI() {}
  public boolean submitData(VData cInputData,String cOperate)
  {
  	try {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    CaseCureQueryBL tCaseCureQueryBL = new CaseCureQueryBL();
    if (tCaseCureQueryBL.submitData(cInputData,mOperate) == false)
    {
  		// @@错误处理
      this.mErrors.copyAllErrors(tCaseCureQueryBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "CaseCureQueryUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
        else
			mResult = tCaseCureQueryBL.getResult();
			System.out.println("CaseCureQueryUI.java--53");
    return true;
}
catch(Exception ex){
	ex.printStackTrace();
	System.out.println("asdasdasdf");
	return false;
  }

}
  public VData getResult()
  {
  	return mResult;
  }
}