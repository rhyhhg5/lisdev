package com.sinosoft.lis.llcase;


import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:承保暂交费功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class ClaimUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public ClaimUI() {}

  /**
  传输数据的公共方法
  */
  public static void main(String[] args)
    {
   LLClaimSchema tLLClaimSchema = new LLClaimSchema();
    VData tVData = new VData();
    tVData.addElement(tLLClaimSchema);

     // 数据传输
     ClaimUI tClaimUI   = new ClaimUI();
           tClaimUI.submitData(tVData,"QUERY||MAIN");
           tVData.clear();
           tVData = tClaimUI.getResult();

                // 显示
            LLClaimSet mLLClaimSet = new LLClaimSet();
            mLLClaimSet.set((LLClaimSet)tVData.getObjectByObjectName("LLClaimSet",0));
            int n = mLLClaimSet.size() ;


     }


  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    ClaimBL tClaimBL = new ClaimBL();

    System.out.println("---UI BEGIN---");
    if (tClaimBL.submitData(cInputData,mOperate) == false)
    {
  		// @@错误处理
      System.out.println("error");

      this.mErrors.copyAllErrors(tClaimBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
	{		mResult = tClaimBL.getResult();
                        System.out.println("mResult=="+mResult);
	}
      return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

 /* public static void main(String[] args)
  {
    ClaimUI tClaimUI=new ClaimUI();
    VData tVData=new VData();
    LLClaimSchema tLLClaimSchema=new LLClaimSchema();
    LLClaimSet tLLClaimSet=new LLClaimSet();
    LLClaimDetailSchema tLLClaimDetailSchema=new LLClaimDetailSchema();
    LLClaimDetailSet tLLClaimDetailSet=new LLClaimDetailSet();
    tLLClaimSchema.setCaseNo("1");
    tLLClaimSchema.setCasePolNo("1");
    tLLClaimSet.add(tLLClaimSchema);
    tLLClaimDetailSchema.setGetDutyCode("1");
    tLLClaimDetailSchema.setGetDutyKind("1");
    tLLClaimDetailSet.add(tLLClaimDetailSchema);
    tVData.add(tLLClaimSet);
    tVData.add(tLLClaimDetailSet);
    tClaimUI.submitData(tVData,"INSERT");
  }
*/
}