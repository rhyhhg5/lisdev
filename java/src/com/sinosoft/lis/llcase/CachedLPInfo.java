package com.sinosoft.lis.llcase;

import java.util.Hashtable;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;

/**
 * @author maning
 * @version 1.0
 */
public class CachedLPInfo {

	public CErrors mErrors = new CErrors();

	// 性别
	private Hashtable m_hashSex = new Hashtable();

	// 证件类型
	private Hashtable m_hashIDType = new Hashtable();

	// 申请人与被保险人关系
	private Hashtable m_hashLLRelation = new Hashtable();

	// 受理方式
	private Hashtable m_hashLLAskMode = new Hashtable();

	// 领取方式
	private Hashtable m_hashLLPayMode = new Hashtable();

	// 被保险人现状
	private Hashtable m_hashLLCustState = new Hashtable();

	// 申请原因
	private Hashtable m_hashLLRgtReason = new Hashtable();

	// 事件类型
	private Hashtable m_hashLLAccType = new Hashtable();

	// 申请材料信息
	private Hashtable m_hashLLMAffix = new Hashtable();

	// 申请材料与出险原因关系
	private Hashtable m_hashLLMAppReasonAffix = new Hashtable();

	// 账单种类
	private Hashtable m_hashLLFeeType = new Hashtable();

	// 账单属性
	private Hashtable m_hashLLFeeAtti = new Hashtable();

	// 账单类型
	private Hashtable m_hashFeeAffixType = new Hashtable();

	// 人员类别
	private Hashtable m_hashInsuStat = new Hashtable();

	// 费用明细
	private Hashtable m_hashLLFeeItemType = new Hashtable();

	// 社保费用类型
	private Hashtable m_hashLLSecuFeeItem = new Hashtable();

	// 意外险平台账单分类明细代码
	private Hashtable m_HashLLAFeeItemType = new Hashtable();

	// 医保通调用类
	private Hashtable m_HashYBTClassType = new Hashtable();

	// 静态变量 未找到
	private static final String NOFOUND = "NOFOUND";

	// 实例对象
	private static CachedLPInfo m_cri = null;

	/**
	 * 构造函数
	 */
	public CachedLPInfo() {
	}

	/**
	 * 生成申请材料缓存实例
	 * 
	 * @return CachedAffix 缓存实例
	 */
	public static CachedLPInfo getInstance() {
		if (m_cri == null) {
			m_cri = new CachedLPInfo();
		}

		m_cri.mErrors.clearErrors();

		return m_cri;
	}

	/**
	 * 刷新缓存
	 */
	public void refresh() {
		m_hashSex.clear();
		m_hashIDType.clear();
		m_hashLLRelation.clear();
		m_hashLLAskMode.clear();
		m_hashLLPayMode.clear();
		m_hashLLCustState.clear();
		m_hashLLRgtReason.clear();
		m_hashLLAccType.clear();
		m_hashLLMAffix.clear();
		m_hashLLMAppReasonAffix.clear();
		m_hashLLFeeType.clear();
		m_hashLLFeeAtti.clear();
		m_hashFeeAffixType.clear();
		m_hashInsuStat.clear();
		m_hashLLFeeItemType.clear();
		m_hashLLSecuFeeItem.clear();
		m_HashLLAFeeItemType.clear();
		m_HashYBTClassType.clear();
	}

	/**
	 * 根据性别代码查找性别
	 * 
	 * @param aCode
	 *            性别代码
	 * @return LDCodeSchema 性别
	 */
	public LDCodeSchema findSexByCode(String aCode) {
		if (aCode == null || "".equals(aCode)) {
			return null;
		}

		Hashtable hash = m_hashSex;

		Object obj = hash.get(aCode);

		if (obj != null) {
			if (obj instanceof String) {
				return null;
			} else {
				return (LDCodeSchema) obj;
			}
		} else {
			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("sex");
			tLDCodeDB.setCode(aCode);
			if (!tLDCodeDB.getInfo()) {
				mErrors.addOneError("没有对应的性别");
				hash.put(aCode, NOFOUND);
				return null;
			}

			hash.put(aCode, tLDCodeDB.getSchema());
			return tLDCodeDB.getSchema();
		}
	}

	/**
	 * 根据证件类型获取证件类型
	 * 
	 * @param aIDType
	 *            证件类型编码
	 * @return LDCodeSchema 证件名称
	 */
	public LDCodeSchema findIDTypeByCode(String aCode) {
		if (aCode == null || "".equals(aCode)) {
			return null;
		}

		Hashtable hash = m_hashIDType;

		Object obj = hash.get(aCode);

		if (obj != null) {
			if (obj instanceof String) {
				return null;
			} else {
				return (LDCodeSchema) obj;
			}
		} else {
			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("idtype");
			tLDCodeDB.setCode(aCode);
			if (!tLDCodeDB.getInfo()) {
				mErrors.addOneError("没有对应的证件类型");
				hash.put(aCode, NOFOUND);
				return null;
			}

			hash.put(aCode, tLDCodeDB.getSchema());
			return tLDCodeDB.getSchema();
		}
	}

	/**
	 * 根据申请人与被保险人关系查询关系
	 * 
	 * @param aCode
	 *            申请人关系代码
	 * @return LDCodeSchema 申请人与被保险人关系
	 */
	public LDCodeSchema findLLRelationByCode(String aCode) {
		if (aCode == null || "".equals(aCode)) {
			return null;
		}

		Hashtable hash = m_hashLLRelation;

		Object obj = hash.get(aCode);

		if (obj != null) {
			if (obj instanceof String) {
				return null;
			} else {
				return (LDCodeSchema) obj;
			}
		} else {
			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("llrelation");
			tLDCodeDB.setCode(aCode);
			if (!tLDCodeDB.getInfo()) {
				mErrors.addOneError("没有对应的申请人关系");
				hash.put(aCode, NOFOUND);
				return null;
			}

			hash.put(aCode, tLDCodeDB.getSchema());
			return tLDCodeDB.getSchema();
		}
	}

	/**
	 * 根据受理方式代码查询受理方式
	 * 
	 * @param aCode
	 *            受理方式代码
	 * @return LDCodeSchema 受理方式
	 */
	public LDCodeSchema findLLAskModeByCode(String aCode) {
		if (aCode == null || "".equals(aCode)) {
			return null;
		}

		Hashtable hash = m_hashLLAskMode;

		Object obj = hash.get(aCode);

		if (obj != null) {
			if (obj instanceof String) {
				return null;
			} else {
				return (LDCodeSchema) obj;
			}
		} else {
			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("llaskmode");
			tLDCodeDB.setCode(aCode);
			if (!tLDCodeDB.getInfo()) {
				mErrors.addOneError("没有对应的受理方式");
				hash.put(aCode, NOFOUND);
				return null;
			}

			hash.put(aCode, tLDCodeDB.getSchema());
			return tLDCodeDB.getSchema();
		}
	}

	/**
	 * 根据领取方式编码查询领取方式
	 * 
	 * @param aCode
	 *            领取方式
	 * @return LDCodeSchema 领取方式
	 */
	public LDCodeSchema findLLGetModeByCode(String aCode) {
		if (aCode == null || "".equals(aCode)) {
			return null;
		}

		Hashtable hash = m_hashLLPayMode;

		Object obj = hash.get(aCode);

		if (obj != null) {
			if (obj instanceof String) {
				return null;
			} else {
				return (LDCodeSchema) obj;
			}
		} else {
			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("llgetmode");
			tLDCodeDB.setCode(aCode);
			if (!tLDCodeDB.getInfo()) {
				mErrors.addOneError("没有对应的领取方式");
				hash.put(aCode, NOFOUND);
				return null;
			}

			hash.put(aCode, tLDCodeDB.getSchema());
			return tLDCodeDB.getSchema();
		}
	}

	/**
	 * 根据被保险人现状代码获取信息
	 * 
	 * @param aCode
	 *            被保险人现状代码
	 * @return LDCodeSchema 被保险人现状
	 */
	public LDCodeSchema findLLCustStateByCode(String aCode) {
		if (aCode == null || "".equals(aCode)) {
			return null;
		}

		Hashtable hash = m_hashLLCustState;

		Object obj = hash.get(aCode);

		if (obj != null) {
			if (obj instanceof String) {
				return null;
			} else {
				return (LDCodeSchema) obj;
			}
		} else {
			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("llcuststatus");
			tLDCodeDB.setCode(aCode);
			if (!tLDCodeDB.getInfo()) {
				mErrors.addOneError("没有对应的被保险人现状");
				hash.put(aCode, NOFOUND);
				return null;
			}

			hash.put(aCode, tLDCodeDB.getSchema());
			return tLDCodeDB.getSchema();
		}
	}

	/**
	 * 根据申请原因代码查询申请原因
	 * 
	 * @param aCode
	 *            申请原因代码
	 * @return LDCodeSchema 申请原因
	 */
	public LDCodeSchema findLLRgtReasonByCode(String aCode) {
		if (aCode == null || "".equals(aCode)) {
			return null;
		}

		Hashtable hash = m_hashLLRgtReason;

		Object obj = hash.get(aCode);

		if (obj != null) {
			if (obj instanceof String) {
				return null;
			} else {
				return (LDCodeSchema) obj;
			}
		} else {
			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("llrgtreason");
			tLDCodeDB.setCode(aCode);
			if (!tLDCodeDB.getInfo()) {
				mErrors.addOneError("没有对应的申请原因");
				hash.put(aCode, NOFOUND);
				return null;
			}

			hash.put(aCode, tLDCodeDB.getSchema());
			return tLDCodeDB.getSchema();
		}
	}

	/**
	 * 根据事件类型代码查询事件类型
	 * 
	 * @param aCode
	 *            事件类型代码
	 * @return LDCodeSchema 事件类型
	 */
	public LDCodeSchema findLLAccTypeByCode(String aCode) {
		if (aCode == null || "".equals(aCode)) {
			return null;
		}

		Hashtable hash = m_hashLLAccType;

		Object obj = hash.get(aCode);

		if (obj != null) {
			if (obj instanceof String) {
				return null;
			} else {
				return (LDCodeSchema) obj;
			}
		} else {
			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("llacctype");
			tLDCodeDB.setCode(aCode);
			if (!tLDCodeDB.getInfo()) {
				mErrors.addOneError("没有对应的申请原因");
				hash.put(aCode, NOFOUND);
				return null;
			}

			hash.put(aCode, tLDCodeDB.getSchema());
			return tLDCodeDB.getSchema();
		}
	}

	/**
	 * 根据申请材料代码查找申请材料原因
	 * 
	 * @param aAffixCode
	 *            申请材料代码
	 * @return LLMAppReasonAffixSet 一个申请材料可能有多个申请原因
	 */
	public LLMAppReasonAffixSet findLLMAppReasonAffixByAffixCode(
			String aAffixCode) {

		if (aAffixCode == null || "".equals(aAffixCode)) {
			return null;
		}

		Hashtable hash = m_hashLLMAppReasonAffix;

		Object obj = hash.get(aAffixCode);

		if (obj != null) {
			if (obj instanceof String) {
				return null;
			} else {
				return (LLMAppReasonAffixSet) obj;
			}
		} else {
			LLMAppReasonAffixDB tLLMAppReasonAffixDB = new LLMAppReasonAffixDB();

			tLLMAppReasonAffixDB.setAffixCode(aAffixCode);

			LLMAppReasonAffixSet tLLMAppReasonAffixSet = tLLMAppReasonAffixDB
					.query();

			if (tLLMAppReasonAffixSet.size() <= 0) {
				mErrors.addOneError("没有对应的申请材料");
				hash.put(aAffixCode, NOFOUND);
				return null;
			}

			hash.put(aAffixCode, tLLMAppReasonAffixSet);
			return tLLMAppReasonAffixSet;
		}
	}

	/**
	 * 根据申请材料代码查询申请材料信息
	 * 
	 * @param aAffixCode
	 *            申请材料代码
	 * @return LLMAffixSet 申请材料信息，可能有多个
	 */
	public LLMAffixSet findLLMAffixByAffixCode(String aAffixCode) {

		if (aAffixCode == null || "".equals(aAffixCode)) {
			return null;
		}

		Hashtable hash = m_hashLLMAffix;

		Object obj = hash.get(aAffixCode);

		if (obj != null) {
			if (obj instanceof String) {
				return null;
			} else {
				return (LLMAffixSet) obj;
			}
		} else {
			LLMAffixDB tLLMAffixDB = new LLMAffixDB();

			tLLMAffixDB.setAffixCode(aAffixCode);

			LLMAffixSet tLLMAffixSet = tLLMAffixDB.query();

			if (tLLMAffixSet.size() <= 0) {
				mErrors.addOneError("没有对应的申请材料");
				hash.put(aAffixCode, NOFOUND);
				return null;
			}

			hash.put(aAffixCode, tLLMAffixSet);
			return tLLMAffixSet;
		}
	}

	/**
	 * 根据账单种类代码查找账单类型
	 * 
	 * @param aCode
	 *            账单种类代码
	 * @return LDCodeSchema 账单种类
	 */
	public LDCodeSchema findLLFeeTypeByCode(String aCode) {
		if (aCode == null || "".equals(aCode)) {
			return null;
		}

		Hashtable hash = m_hashLLFeeType;

		Object obj = hash.get(aCode);

		if (obj != null) {
			if (obj instanceof String) {
				return null;
			} else {
				return (LDCodeSchema) obj;
			}
		} else {
			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("llfeetype");
			tLDCodeDB.setCode(aCode);
			if (!tLDCodeDB.getInfo()) {
				mErrors.addOneError("没有对应的账单种类");
				hash.put(aCode, NOFOUND);
				return null;
			}

			hash.put(aCode, tLDCodeDB.getSchema());
			return tLDCodeDB.getSchema();
		}
	}

	/**
	 * 根据账单属性代码查找账单属性
	 * 
	 * @param aCode
	 *            账单属性代码
	 * @return LDCodeSchema 账单属性
	 */
	public LDCodeSchema findLLFeeAttiByCode(String aCode) {
		if (aCode == null || "".equals(aCode)) {
			return null;
		}

		Hashtable hash = m_hashLLFeeAtti;

		Object obj = hash.get(aCode);

		if (obj != null) {
			if (obj instanceof String) {
				return null;
			} else {
				return (LDCodeSchema) obj;
			}
		} else {
			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("llfeeatti");
			tLDCodeDB.setCode(aCode);
			if (!tLDCodeDB.getInfo()) {
				mErrors.addOneError("没有对应的账单属性");
				hash.put(aCode, NOFOUND);
				return null;
			}

			hash.put(aCode, tLDCodeDB.getSchema());
			return tLDCodeDB.getSchema();
		}
	}

	/**
	 * 根据账单类型代码查找账单类型
	 * 
	 * @param aCode
	 *            账单类型代码
	 * @return LDCodeSchema 账单类型
	 */
	public LDCodeSchema findFeeAffixTypeByCode(String aCode) {
		if (aCode == null || "".equals(aCode)) {
			return null;
		}

		Hashtable hash = m_hashFeeAffixType;

		Object obj = hash.get(aCode);

		if (obj != null) {
			if (obj instanceof String) {
				return null;
			} else {
				return (LDCodeSchema) obj;
			}
		} else {
			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("feeaffixtype");
			tLDCodeDB.setCode(aCode);
			if (!tLDCodeDB.getInfo()) {
				mErrors.addOneError("没有对应的账单类型");
				hash.put(aCode, NOFOUND);
				return null;
			}

			hash.put(aCode, tLDCodeDB.getSchema());
			return tLDCodeDB.getSchema();
		}
	}

	/**
	 * 根据人员类别代码查找人员类别
	 * 
	 * @param aCode
	 *            人员类别代码
	 * @return LDCodeSchema 人员类别
	 */
	public LDCodeSchema findInsuStatByCode(String aCode) {
		if (aCode == null || "".equals(aCode)) {
			return null;
		}

		Hashtable hash = m_hashInsuStat;

		Object obj = hash.get(aCode);

		if (obj != null) {
			if (obj instanceof String) {
				return null;
			} else {
				return (LDCodeSchema) obj;
			}
		} else {
			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("insustat");
			tLDCodeDB.setCode(aCode);
			if (!tLDCodeDB.getInfo()) {
				mErrors.addOneError("没有对应的人员类别");
				hash.put(aCode, NOFOUND);
				return null;
			}

			hash.put(aCode, tLDCodeDB.getSchema());
			return tLDCodeDB.getSchema();
		}
	}

	/**
	 * 根据费用明细代码查找费用明细
	 * 
	 * @param aCode
	 *            费用明细代码
	 * @return LDCodeSchema 费用明细
	 */
	public LDCodeSchema findLLFeeItemTypeByCode(String aCode) {
		if (aCode == null || "".equals(aCode)) {
			return null;
		}

		Hashtable hash = m_hashLLFeeItemType;

		Object obj = hash.get(aCode);

		if (obj != null) {
			if (obj instanceof String) {
				return null;
			} else {
				return (LDCodeSchema) obj;
			}
		} else {
			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("llfeeitemtype");
			tLDCodeDB.setCode(aCode);
			if (!tLDCodeDB.getInfo()) {
				mErrors.addOneError("没有对应的费用明细");
				hash.put(aCode, NOFOUND);
				return null;
			}

			hash.put(aCode, tLDCodeDB.getSchema());
			return tLDCodeDB.getSchema();
		}
	}

	/**
	 * 根据社保费用明细代码查找社保费用明细
	 * 
	 * @param aCode
	 *            社保费用代码
	 * @return LDCodeSchema 社保费用明细
	 */
	public LDCodeSchema findLLSecuFeeItemByCode(String aCode) {
		if (aCode == null || "".equals(aCode)) {
			return null;
		}

		Hashtable hash = m_hashLLSecuFeeItem;

		Object obj = hash.get(aCode);

		if (obj != null) {
			if (obj instanceof String) {
				return null;
			} else {
				return (LDCodeSchema) obj;
			}
		} else {
			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("llsecufeeitem");
			tLDCodeDB.setCode(aCode);
			if (!tLDCodeDB.getInfo()) {
				mErrors.addOneError("没有对应的社保费用明细");
				hash.put(aCode, NOFOUND);
				return null;
			}

			hash.put(aCode, tLDCodeDB.getSchema());
			return tLDCodeDB.getSchema();
		}
	}

	/**
	 * 根据账单类型、费用代码查找核心代码
	 * 
	 * @param aFeeType
	 *            账单类型
	 * @param aCode
	 *            平台代码
	 * @return LDCode1Schema 核心费用代码
	 */
	public LDCode1Schema findLLAFeeItemTypeByCode(String aFeeType, String aCode) {
		if (aFeeType == null || "".equals(aFeeType) || aCode == null
				|| "".equals(aCode)) {
			return null;
		}

		Hashtable hash = m_HashLLAFeeItemType;

		Object obj = hash.get(aFeeType + aCode);

		if (obj != null) {
			if (obj instanceof String) {
				return null;
			} else {
				return (LDCode1Schema) obj;
			}
		} else {
			LDCode1DB tLDCode1DB = new LDCode1DB();
			tLDCode1DB.setCodeType("llfeeitemtype");
			tLDCode1DB.setCode(aFeeType);
			tLDCode1DB.setCode1(aCode);
			if (!tLDCode1DB.getInfo()) {
				mErrors.addOneError("没有对应的意外平台费用明细");
				hash.put(aFeeType + aCode, NOFOUND);
				return null;
			}

			hash.put(aFeeType + aCode, tLDCode1DB.getSchema());
			return tLDCode1DB.getSchema();
		}
	}

	/**
	 * 根据调用类型和编码查询医保通调用类
	 * @param aCode 调用类型
	 * @param aCode1 附加编码
	 * @return
	 */
	public LDCode1Schema findYBTClassTypeByCode(String aCode, String aCode1) {
		if (aCode == null || "".equals(aCode) || aCode1 == null
				|| "".equals(aCode1)) {
			return null;
		}

		Hashtable hash = m_HashYBTClassType;

		Object obj = hash.get(aCode + aCode1);

		if (obj != null) {
			if (obj instanceof String) {
				return null;
			} else {
				return (LDCode1Schema) obj;
			}
		} else {
			LDCode1DB tLDCode1DB = new LDCode1DB();
			tLDCode1DB.setCodeType("ybt_service");
			tLDCode1DB.setCode(aCode);
			tLDCode1DB.setCode1(aCode1);
			if (!tLDCode1DB.getInfo()) {
				mErrors.addOneError("没有对应的医保通调用类");
				hash.put(aCode + aCode1, NOFOUND);
				return null;
			}

			hash.put(aCode + aCode, tLDCode1DB.getSchema());
			return tLDCode1DB.getSchema();
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
