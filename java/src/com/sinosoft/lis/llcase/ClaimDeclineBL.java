/************************************************
 * Xx 2005-12-18 拒赔案件类.
 ************************************************/

package com.sinosoft.lis.llcase;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 拒赔案件类</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author Xx
 * @version 1.0
 */
public class ClaimDeclineBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  private GlobalInput mGlobalInput = new GlobalInput();
  /**  */
  private LLCaseSchema  mLLCaseSchema = new LLCaseSchema();
  private LLClaimDeclineSchema mLLClaimDeclineSchema = new LLClaimDeclineSchema();

  public ClaimDeclineBL() {}

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;

   if (!checkData())
   {
       return false;
   }

    // 数据操作业务处理
    if (mOperate.equals("INSERT"))
    {
      if (!dealData())
	return false;

    }
    PubSubmit ps = new PubSubmit();
    if (!ps.submitData( this.mResult, null ))
    {
        CError.buildErr(this,"数据库保存失败");
        return false;
    }
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealData()
  {
    LLClaimDeclineDB tLLClaimDeclineDB = new LLClaimDeclineDB();
    tLLClaimDeclineDB.setCaseNo( this.mLLClaimDeclineSchema.getCaseNo());
    if (!tLLClaimDeclineDB.getInfo()) {
        mLLClaimDeclineSchema.setDeclineNo(mLLClaimDeclineSchema.getClmNo());
        mLLClaimDeclineSchema.setMakeDate(PubFun.getCurrentDate());
        mLLClaimDeclineSchema.setMakeTime(PubFun.getCurrentTime());
        mLLClaimDeclineSchema.setDeclineDate(PubFun.getCurrentDate());
    }
    else{
        mLLClaimDeclineSchema.setDeclineNo(tLLClaimDeclineDB.getClmNo());
        mLLClaimDeclineSchema.setMakeDate(tLLClaimDeclineDB.getMakeDate());
        mLLClaimDeclineSchema.setMakeTime(tLLClaimDeclineDB.getMakeTime());
        mLLClaimDeclineSchema.setDeclineDate(tLLClaimDeclineDB.getDeclineDate());
    }
    mLLClaimDeclineSchema.setOperator(this.mGlobalInput.Operator);
    mLLClaimDeclineSchema.setMngCom(this.mGlobalInput.ComCode);
    mLLClaimDeclineSchema.setModifyDate(PubFun.getCurrentDate());
    mLLClaimDeclineSchema.setModifyTime(PubFun.getCurrentTime());
    MMap tmpMap = new MMap();

    tmpMap.put( this.mLLClaimDeclineSchema, "DELETE&INSERT");

    this.mResult.add( tmpMap );
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
      mLLClaimDeclineSchema.setSchema((LLClaimDeclineSchema)cInputData.getObjectByObjectName("LLClaimDeclineSchema",0));
      mGlobalInput= (GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0);
    return true;

  }

  /**
   * 校验传入的赔案信息是否正确
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkData()
  {
      LLCaseDB tLLCaseDB = new LLCaseDB();
      tLLCaseDB.setCaseNo(mLLClaimDeclineSchema.getCaseNo());
      if (!tLLCaseDB.getInfo()) {
          CError.buildErr(this, "没有查询到理赔案件");
          return false;
      }
      if(!tLLCaseDB.getHandler().equals(mGlobalInput.Operator)){
          CError.buildErr(this, "您不是指定处理人！");
          return false;
      }
      if (!"03".equals(tLLCaseDB.getRgtState())//add liuyc
          &&!"04".equals(tLLCaseDB.getRgtState())
          && !"05".equals(tLLCaseDB.getRgtState())
          && !"06".equals(tLLCaseDB.getRgtState()))
      {
          CError.buildErr(this, "该案件状态不能录入或修改拒赔原因");
          return false;
      }
      LLClaimDB tLLClaimDB = new LLClaimDB();
      LLClaimSet tLLClaimSet = new LLClaimSet();
      tLLClaimDB.setCaseNo(mLLClaimDeclineSchema.getCaseNo());
      tLLClaimSet  = tLLClaimDB.query();
      if (tLLClaimSet.size()<=0) {
          CError.buildErr(this, "尚未作理算确认，没有生成完整的赔案信息！");
          return false;
      }
    //by liuyc
//      if (!"03".equals(tLLClaimSet.get(1).getGiveType())){
//          CError.buildErr(this,"该赔案不是拒赔案件！");
//          return false;
//      }
      mLLClaimDeclineSchema.setClmNo(tLLClaimSet.get(1).getClmNo());
      mLLClaimDeclineSchema.setRgtNo(tLLCaseDB.getRgtNo());
	return true;

  }

  /**
   * 准备需要保存的数据
   */
  private void prepareOutputData()
  {

  }

  public static void main(String[] args){
      GlobalInput mGlobalInput = new GlobalInput();
      mGlobalInput.ManageCom = "86";
      mGlobalInput.ComCode = "8611";
      mGlobalInput.Operator = "ocm005";
      LLCaseSchema tLLCaseSchema  = new LLCaseSchema();
      tLLCaseSchema.setCaseNo("C0000050528000002");
      VData tVData = new VData();
      tVData.addElement(mGlobalInput);
      tVData.addElement(tLLCaseSchema);
      ClaimDeclineBL tClaimDeclineBL   = new ClaimDeclineBL();
      tClaimDeclineBL.submitData(tVData,"CHECKFINISH");

  }

}
