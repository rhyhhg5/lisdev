package com.sinosoft.lis.llcase;

import java.io.InputStream;
import java.text.DecimalFormat;

import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.db.LLAppealDB;
import com.sinosoft.lis.db.LLCaseCureDB;
import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.db.LLCaseDrugDB;
import com.sinosoft.lis.db.LLCaseReceiptDB;
import com.sinosoft.lis.db.LLCaseRelaDB;
import com.sinosoft.lis.db.LLClaimDB;
import com.sinosoft.lis.db.LLClaimDetailDB;
import com.sinosoft.lis.db.LLFeeMainDB;
import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.lis.db.LLSecurityReceiptDB;
import com.sinosoft.lis.db.LLSubReportDB;
import com.sinosoft.lis.db.LMDutyGetDB;
import com.sinosoft.lis.f1print.CSVFileWrite;
import com.sinosoft.lis.f1print.LLClaimCollectionCSVBL;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LLCaseDrugSchema;
import com.sinosoft.lis.schema.LLCaseReceiptSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimDetailSchema;
import com.sinosoft.lis.schema.LLClaimSchema;
import com.sinosoft.lis.schema.LLFeeMainSchema;
import com.sinosoft.lis.schema.LLSecurityReceiptSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.schema.LOBatchPRTManagerSchema;
import com.sinosoft.lis.vschema.LLAppealSet;
import com.sinosoft.lis.vschema.LLCaseCureSet;
import com.sinosoft.lis.vschema.LLCaseDrugSet;
import com.sinosoft.lis.vschema.LLCaseReceiptSet;
import com.sinosoft.lis.vschema.LLCaseRelaSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LLClaimDetailSet;
import com.sinosoft.lis.vschema.LLClaimSet;
import com.sinosoft.lis.vschema.LLFeeMainSet;
import com.sinosoft.lis.vschema.LLSecurityReceiptSet;
import com.sinosoft.lis.vschema.LLSubReportSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description: 理赔批量打印
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Xx
 * @version 1.0
 */
public class ClaimDetailPrintCVSBL {

	public ClaimDetailPrintCVSBL() {
	}

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 全局变量 */
	private TransferData mTransferData = new TransferData();

	private String mRgtNo = "";

	private String mGrpcontno = "";

	private double lowamnt = 0;

	private double midamnt = 0;

	private double highamnt1 = 0;

	private double highamnt2 = 0;

	private double supinhosamnt = 0;

	private double supdooramnt = 0;

	private double emergdooramnt = 0;

	private double smalldooramnt = 0;

	private String mremark = "";

	private int[] mremarktag = { 0, 0, 0, 0, 0 };

	String mGrpName = "";

	String mRgtantAddress = "";

	/** 批次导入标记 */
	private String applyertype = "";

	/** 赔案记录 */
	private LLClaimSchema mLLClaimSchema = new LLClaimSchema();

	/** 赔案号 */
	private String mClmNo;

	/** 案件类型 */
	private String mRgtClass;

	/** 被保人客户号 */
	private String mCustomerNo;

	/** 赔付明细信息 */
	private LLClaimDetailSet mLLClaimDetailSet = new LLClaimDetailSet();

	/** 帐单信息 */
	private LLFeeMainSet mLLFeeMainSet = new LLFeeMainSet();

	private LLFeeMainDB mLLFeeMainDB = new LLFeeMainDB();

	/** 帐单费用明细信息 */
	private LLCaseReceiptSet mLLCaseReceiptSet = new LLCaseReceiptSet();

	private LLSecurityReceiptSet mLLSecurityReceiptSet = new LLSecurityReceiptSet();

	ExeSQL tExeSQL = new ExeSQL();

	private XmlExport xml = new XmlExport();

	TextTag textTag = new TextTag();

	ListTable listTable = new ListTable();
	TextTag textTag1 = new TextTag();

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			 this.bulidError("getInputData", "数据不完整");
			return false;
		}

		
		// 获取打印数据
		if (!getPrintData()) {
			return false;
		}
		System.out.println("-----end-----");
		return true;

	}

	

	private boolean getPrintData() {

		if (!getDataList(mRgtNo, mGrpcontno, mCustomerNo)) {
			return false;
		}
		xml.createDocument("llclaimdetailcvs.htm", "printer");

		xml.addTextTag(textTag);

		String[] title = { "理赔号码", "险种名称", "责任项目", "就诊医院", "账单日期","账单金额", "社保外",
				"先期给付", "不合理费用", "理赔金额", "免赔", "给付比例", "实赔金额", "赔付结论", "赔付结论依据" };
		xml.addListTable(listTable, title);
		
		xml.addTextTag(textTag1);
        mResult.clear();
        mResult.add(xml);
		return true;

	}

	private boolean getDataList(String rgtNo, String grpcontno,
			String customerno) {

		listTable.setName("BB");
		String sqlstr = "";
		sqlstr = "select * from lcgrpcont where grpcontno='" + grpcontno
				+ "' with ur";
		LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContSchema = tLCGrpContDB.executeQuery(sqlstr).get(1);
		String grpcont = grpcontno;// 保单号
		String grpname = tLCGrpContSchema.getGrpName();// 投保单位
		sqlstr = "select name,"
				+ "case sex when '0' then '男' when '1' then '女' else '不详' end ,"
				+ "idno,RelationToMainInsured"
				+ " from lcinsured where insuredno='" + customerno
				+ "' and grpcontno='" + grpcont + "'";
		SSRS insuredinfo = tExeSQL.execSQL(sqlstr);
		String name1 = insuredinfo.GetText(1, 1);
		String idno1 = insuredinfo.GetText(1, 3);
		String sex1 = insuredinfo.GetText(1, 2);
		String relation = insuredinfo.GetText(1, 4);
		textTag.add("grpcont", grpcont);
		textTag.add("grpname", grpname);
		textTag.add("insuredno1", customerno);
		textTag.add("name1", name1);
		textTag.add("idno1", idno1);
		textTag.add("sex1", sex1);
		if (relation!=null){
			sqlstr = "select insuredno,name,idno from lcinsured where relationtomaininsured='00' "
					+ "and contno=(select contno from lcinsured where insuredno='"
					+ customerno + "' and grpcontno='" + grpcont + "') with ur";
			SSRS tRelationC = tExeSQL.execSQL(sqlstr);
			if (tRelationC != null) {
				String name2 = tRelationC.GetText(1, 2);
				String idno2 = tRelationC.GetText(1, 3);
				String insuredno = tRelationC.GetText(1, 1);
				sqlstr = "select codename from ldcode where codetype='relation' and code='"
						+ relation + "' with ur";
				String rela = tExeSQL.execSQL(sqlstr).GetText(1, 1);
				
				if(rela.equals("本人")){
					textTag.add("insuredno2", " ");
					textTag.add("name2", " ");
					textTag.add("idno2"," ");
					textTag.add("relation"," ");
				}else{
					textTag.add("insuredno2", insuredno);
					textTag.add("name2", name2);
					textTag.add("idno2", idno2);
					textTag.add("relation", rela);
				}
				

			}

		}else{
			textTag.add("insuredno2", " ");
			textTag.add("name2", " ");
			textTag.add("idno2", " ");
			textTag.add("relation", " ");
		}
	
		sqlstr = "select casegetmode,(select codename from ldcode where codetype='paymode' and code=a.casegetmode),"
				+ "(select bankname from ldbank where bankcode=a.bankcode),"
				+ "bankaccno,accname "
				+ "from llcase a where rgtno='"
				+ rgtNo
				+ "' and customerno='"
				+ customerno
				+ "' and rgtstate in ('11','12') with ur";
		SSRS tPayWay = tExeSQL.execSQL(sqlstr);
		if (tPayWay != null) {
			for (int nn = 1; nn <= tPayWay.getMaxRow(); nn++) {
				String paycode = tPayWay.GetText(nn, 1);
				if (paycode.equals("11") || paycode.equals("4")) {
					String payway = tPayWay.GetText(nn, 2);
					String bankname = tPayWay.GetText(nn, 3);
					String bankno = tPayWay.GetText(nn, 4);
					String accname = tPayWay.GetText(nn, 5);
					textTag.add("payway", payway);
					textTag.add("bankname", bankname);
					textTag.add("bankno", bankno);
					textTag.add("accname", accname);

					break;
				}
				if (nn == tPayWay.getMaxRow()) {
					String payway = tPayWay.GetText(nn, 2);
					textTag.add("payway", payway);
					textTag.add("bankname", " ");
					textTag.add("bankno", " ");
					textTag.add("accname", " ");

				}
			}

		}

		textTag.add("rgtno", rgtNo);


		sqlstr = "select distinct a.caseno from llclaimdetail a,llcase b where a.RgtNo='"
				+ rgtNo
				+ "' and b.customerno='"
				+ customerno
				+ "' and a.caseno=b.caseno "
				+ "  and b.rgtstate in ('11','12') with ur";
		SSRS tCaseSet = tExeSQL.execSQL(sqlstr);
		double AllRealPay = 0;
		for (int k = 1; k <= tCaseSet.getMaxRow(); k++) {

			String tCaseNo = tCaseSet.GetText(k, 1);
			String SQL = "select distinct b.caserelano from llclaimdetail a ,llcaserela b where a.caseno ='"
					+ tCaseNo
					+ "' and a.Caseno = b.CaseNo  and a.CaseRelaNo = b.CaseRelaNo with ur";
			SSRS tCaseRelaSSRS = new SSRS();
			tCaseRelaSSRS = tExeSQL.execSQL(SQL);
			if (tCaseRelaSSRS != null && tCaseRelaSSRS.getMaxRow() > 0) {
				System.out.println(tCaseRelaSSRS.getMaxRow());
				for (int l = 1; l <= tCaseRelaSSRS.MaxRow; l++) {

					String tCaseRelaNo = tCaseRelaSSRS.GetText(l, 1);// 案件事件关联号
					String FeeDate = "";
					String SubDate = "";
					String hospitalname = "";
					double planfee = 0;
					double supInHosFee = 0;
					String riskname = "";

					if (!getBaseData(tCaseNo, tCaseRelaNo)) {
						return false;
					}

					LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
					LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
					LLCaseReceiptSchema tLLCaseReceiptSchema = new LLCaseReceiptSchema();
					LLCaseCureDB tLLCaseCureDB = new LLCaseCureDB();
					LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
					tLLCaseCureDB.setCaseNo(tCaseNo);
					tLLCaseCureDB.setCaseRelaNo(tCaseRelaNo);
					tLLCaseCureSet.set(tLLCaseCureDB.query());

					// 申诉、纠错处理
					boolean tAppealFlag = false;
					LLAppealDB tLLAppealDB = new LLAppealDB();
					LLAppealSet tLLAppealSet = new LLAppealSet();
					String tOrigCaseNo = "";

					tLLAppealDB.setCaseNo(tCaseNo);
					tLLAppealSet = tLLAppealDB.query();
					if (tLLAppealSet.size() > 0) {
						tOrigCaseNo = tCaseNo;
						tCaseNo = tLLAppealSet.get(1).getAppealNo();
						LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
						tLLCaseRelaDB.setCaseNo(tCaseNo);
						tCaseRelaNo = tLLCaseRelaDB.query().get(1)
								.getCaseRelaNo();
						tAppealFlag = true;
					}

					if (tCaseNo.substring(0, 1).equals("R")
							|| tCaseNo.substring(0, 1).equals("S")) {
						tLLAppealDB = new LLAppealDB();
						tLLAppealDB.setAppealNo(tCaseNo);
						tLLAppealSet = tLLAppealDB.query();
						tOrigCaseNo = tLLAppealSet.get(1).getCaseNo();
						tAppealFlag = true;
					}
					
					if (mLLSecurityReceiptSet != null && mLLSecurityReceiptSet.size() > 0 &&
				            !tAppealFlag) {
				            if (mLLSecurityReceiptSet.size() > 0) {
				                int SRcount = mLLSecurityReceiptSet.size();
				                for (int i = 1; i <= SRcount; i++) {
				                    LLSecurityReceiptSchema tLLSecurityReceiptSchema = new
				                            LLSecurityReceiptSchema();
				                    tLLSecurityReceiptSchema = mLLSecurityReceiptSet.get(i);
				                    planfee += tLLSecurityReceiptSchema.getPlanFee();
				                    supInHosFee += tLLSecurityReceiptSchema.getSupInHosFee();
				                }
				            }
				    }					

					int count_1 = mLLClaimDetailSet.size();
					int count_2 = mLLFeeMainSet.size();

					if (count_2 > 0) {
						FeeDate = mLLFeeMainSet.get(1).getFeeDate();
					}
					LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
					LLCaseRelaSet tLLCaseRelaSet = new LLCaseRelaSet();
					tLLCaseRelaDB.setCaseNo(tCaseNo);
					tLLCaseRelaDB.setCaseRelaNo(tCaseRelaNo);
					tLLCaseRelaSet.set(tLLCaseRelaDB.query());

					String sql2 = "select accdate from llsubreport where subrptno='"
							+ tLLCaseRelaSet.get(1).getSubRptNo() + "'";
					SubDate = tExeSQL.getOneValue(sql2);
					if (count_2 > 0) {
						hospitalname = mLLFeeMainSet.get(1).getHospitalName();

					} else {
						if (tLLCaseCureSet.size() > 0) {
							if (tLLCaseCureSet.get(1).getHospitalName() != null) {
								hospitalname = tLLCaseCureSet.get(1)
										.getHospitalName();
							}

						}

					}
					String strLine[] = null;
					double AllFee = 0;
					double AllClaimMoney = 0;

					int count = count_1;

					for (int n = 1; n <= count; n++) {
						strLine = new String[19];

						tLLClaimDetailSchema = mLLClaimDetailSet.get(n);
						String strGetDutyName = getGetDutyName(tLLClaimDetailSchema
								.getGetDutyCode());
						String riskcode = tLLClaimDetailSchema.getRiskCode();
						String risksql = "select riskname from lmrisk where riskcode='"
								+ riskcode + "' with ur";
						SSRS risk = tExeSQL.execSQL(risksql);
						riskname = risk.GetText(1, 1);

						String strGetDutyKind = "";
						if (tLLClaimDetailSchema.getGetDutyKind().substring(0,
								1).equals("1")) {
							strGetDutyKind = " and feetype='2' ";
						}
						if (tLLClaimDetailSchema.getGetDutyKind().substring(0,
								1).equals("2")) {
							strGetDutyKind = "and feetype='1'";
						}

						if (strGetDutyName == null) {
							return false;
						}

						int indexb = -1;
						indexb = String.valueOf(strGetDutyName).indexOf("保");
						if (indexb < 0) {
							strLine[0] = String.valueOf(strGetDutyName);
						} else {
							strLine[0] = String.valueOf(strGetDutyName)
									.substring(0, indexb); // 责任项目
						}
						LCPolDB tLCPolDB = new LCPolDB();
						tLCPolDB.setPolNo(tLLClaimDetailSchema.getPolNo());
						if (!tLCPolDB.getInfo()) {
							LBPolDB tLBPolDB = new LBPolDB();
							tLBPolDB.setPolNo(tLLClaimDetailSchema.getPolNo());
							if (!tLBPolDB.getInfo()) {
								CError.buildErr(this, "保单信息查询失败");
								return false;
							} else {
								tLCPolDB.setRiskSeqNo(tLBPolDB.getRiskSeqNo());
							}
						}
						strLine[7] = ""; // 险种序号
						strLine[1] = "";
						strLine[2] = " ";
						strLine[3] = " ";
						strLine[4] = " ";
						strLine[5] = " ";
						strLine[13] = " ";
						strLine[14] = " ";
						strLine[15] = " ";
						strLine[16] = " ";
						strLine[17] = " ";// 赔付结论
						strLine[18] = " ";// 赔付依据

						LLFeeMainSet tLLFeeMainSet = new LLFeeMainSet();
						mLLFeeMainDB.setCaseRelaNo(tLLClaimDetailSchema
								.getCaseRelaNo());
						tLLFeeMainSet.set(mLLFeeMainDB.query());
						if (tLLFeeMainSet.size() > 0) {
							tLLFeeMainSchema = tLLFeeMainSet.get(1);
						}
						// 帐单还有记录备注信息
						if (!tLLFeeMainSchema.equals("")) {
							if (tLLFeeMainSchema.getFeeDate() != null) {
								strLine[1] = tLLFeeMainSchema.getFeeDate(); // 帐单日期
							}
						}

						double acountMoney = 0;
						double FIS = 0;
						double FOS = 0;
						double RefuseFee = 0;
						double PayedFee = 0;

						String sql10 = "select coalesce(sum(fee),0),coalesce(sum(PreAmnt),0),coalesce(sum(SelfAmnt),0),coalesce(sum(RefuseAmnt),0) from  LLCaseReceipt where caseno='"
								+ tLLClaimDetailSchema.getCaseNo()
								+ "' "
								+ " and  mainfeeno in ( select mainfeeno from llfeemain where caserelano ='"
								+ tLLClaimDetailSchema.getCaseRelaNo()
								+ "' "
								+ strGetDutyKind + " )";

						tExeSQL = new ExeSQL();
						SSRS tSSRS = new SSRS();
						tSSRS = tExeSQL.execSQL(sql10);
						acountMoney = Double.parseDouble(tSSRS.GetText(1, 1));
						FIS = Double.parseDouble(tSSRS.GetText(1, 2));
						FOS = Double.parseDouble(tSSRS.GetText(1, 3));
						RefuseFee = Double.parseDouble(tSSRS.GetText(1, 4));
						if (acountMoney == 0) {
							String sql11 = "select coalesce(sum(fee),0),coalesce(sum(PreAmnt),0),coalesce(sum(SelfAmnt),0),coalesce(sum(RefuseAmnt),0) from  LLCaseReceipt where caseno='"
									+ tLLClaimDetailSchema.getCaseNo()
									+ "' and  mainfeeno in ( "
									+ " select mainfeeno from llfeemain where caserelano ='"
									+ tLLClaimDetailSchema.getCaseRelaNo()
									+ "' " + strGetDutyKind + " )";

							SSRS fSSRS = new SSRS();
							fSSRS = tExeSQL.execSQL(sql11);
							acountMoney = Double.parseDouble(fSSRS
									.GetText(1, 1));
							FIS = Double.parseDouble(fSSRS.GetText(1, 2));
							FOS = Double.parseDouble(fSSRS.GetText(1, 3));
							RefuseFee = Double.parseDouble(fSSRS.GetText(1, 4));

						}
						if (acountMoney == 0) {

							String sql12 = "select coalesce(sum(sumfee),0) from llfeemain where caseno='"
									+ tCaseNo
									+ "' and  mainfeeno in ( "
									+ " select mainfeeno from llfeemain where caserelano ='"
									+ tLLClaimDetailSchema.getCaseRelaNo()
									+ "' " + strGetDutyKind + " )";

							SSRS fSSRS = new SSRS();
							fSSRS = tExeSQL.execSQL(sql12);
							acountMoney = Double.parseDouble(fSSRS
									.GetText(1, 1));

						}

						if (acountMoney == 0) {

							if (tLLClaimDetailSchema.getTabFeeMoney() > 0) {
								acountMoney = tLLClaimDetailSchema
										.getTabFeeMoney();
							}

						}
						if (tLLClaimDetailSchema.getGetDutyCode()
								.equals("1605")) {
							if (!tLLFeeMainSchema.equals("")) {
								PayedFee += tLLFeeMainSchema.getSumFee()
										- tLLFeeMainSchema.getRemnant();
							}
						}

						strLine[2] = String.valueOf(acountMoney); // 帐单金额
						strLine[14] = String.valueOf(Arith
								.round(acountMoney, 2));// 上海社保账单
						if ("8631".equals(mLLClaimSchema.getMngCom().substring(
								0, 4))
								&& ("604208".equals(tLLClaimDetailSchema
										.getGetDutyCode())
										|| "615220".equals(tLLClaimDetailSchema
												.getGetDutyCode())
										|| "615208".equals(tLLClaimDetailSchema
												.getGetDutyCode())
										|| "629220".equals(tLLClaimDetailSchema
												.getGetDutyCode())
										|| "629208".equals(tLLClaimDetailSchema
												.getGetDutyCode())
										|| "647208".equals(tLLClaimDetailSchema
												.getGetDutyCode()) || "603201"
										.equals(tLLClaimDetailSchema
												.getGetDutyCode()))) {
							if ("5".equals(applyertype)
									|| "2".equals(applyertype)) {
								strLine[3] = String.valueOf(Arith.round(planfee
										+ supInHosFee, 2)); // 已报销费用(批次导入的，取导入的“统筹支付”和“大额救助支付”字段合计金额)
							} else {
								strLine[3] = String
										.valueOf(Arith.round(FIS, 2)); // 已报销费用(个案处理的，取账单录入页面的“先期给付”字段金额)
							}
						} else {
							strLine[3] = String.valueOf(Arith.round(FIS, 2)); // 社保內
						}

						if ("8631".equals(mLLClaimSchema.getMngCom().substring(
								0, 4))
								&& ("604208".equals(tLLClaimDetailSchema
										.getGetDutyCode())
										|| "615220".equals(tLLClaimDetailSchema
												.getGetDutyCode())
										|| "615208".equals(tLLClaimDetailSchema
												.getGetDutyCode())
										|| "629220".equals(tLLClaimDetailSchema
												.getGetDutyCode())
										|| "629208".equals(tLLClaimDetailSchema
												.getGetDutyCode())
										|| "647208".equals(tLLClaimDetailSchema
												.getGetDutyCode()) || "603201"
										.equals(tLLClaimDetailSchema
												.getGetDutyCode()))) {
							if ("5".equals(applyertype)
									|| "2".equals(applyertype)) {
								strLine[4] = String.valueOf(Arith.round(planfee
										+ supInHosFee, 2)); // 已报销费用(批次导入的，取导入的“统筹支付”和“大额救助支付”字段合计金额)
							} else {
								strLine[4] = String
										.valueOf(Arith.round(FIS, 2)); // 已报销费用(个案处理的，取账单录入页面的“先期给付”字段金额)
							}
						} else {
							strLine[4] = String.valueOf(Arith
									.round(PayedFee, 2)); // 已报销费用
						}
						strLine[5] = String.valueOf(Arith.round(RefuseFee, 2)); // 不合理费用
						strLine[13] = String.valueOf(Arith.round(FOS, 2)); // 社保外
						double fos2 = Arith.round(
								acountMoney - FOS - RefuseFee, 2);
						strLine[15] = String.valueOf(fos2); // 上海社保账单
						AllFee += acountMoney;

						if (count_2 <= 0) {
							strLine[1] = "　　　";
							strLine[2] = "0.00";
							strLine[3] = "0.00";
							strLine[4] = "0.00";
							strLine[5] = "0.00";
							strLine[13] = "0.00";
						}

						strLine[6] = "　　　";

						if (tLLClaimDetailSchema.getGetDutyCode().trim()
								.equals("207201")) {
							strLine[8] = "3天"; // 免赔天数
							strLine[3] = "0.00";
							strLine[5] = "0.00";
							strLine[13] = "0.00";
							strLine[15] = "0.00";

						} else {
							strLine[8] = String.valueOf(tLLClaimDetailSchema
									.getOutDutyAmnt()); // 免赔
						}

						strLine[9] = String.valueOf(tLLClaimDetailSchema
								.getClaimMoney()); // 理算金额
						strLine[10] = String.valueOf(tLLClaimDetailSchema
								.getRealPay()); // 实赔金额
						strLine[11] = trunGiveType(tLLClaimDetailSchema
								.getGiveType()); // 备注
						strLine[12] = String.valueOf(tLLClaimDetailSchema
								.getOutDutyRate()); // 给付比例
						AllClaimMoney += tLLClaimDetailSchema.getClaimMoney();
						AllRealPay += tLLClaimDetailSchema.getRealPay();
						
						strLine[17] = tLLClaimDetailSchema.getGiveTypeDesc();
				    strLine[18] = tLLClaimDetailSchema.getGiveReasonDesc();

						String info[] = new String[] { tCaseNo, riskname,
								strLine[0], hospitalname, strLine[1],
								strLine[2], strLine[13], strLine[4],
								strLine[5], strLine[9], strLine[8],
								strLine[12], strLine[10], strLine[17],
								strLine[18] };

						listTable.add(info);

					}

				}
			}
		}
		String allCountMoney = String.valueOf(PubFun.getChnMoney(AllRealPay));

		textTag1.add("sum", String.valueOf(AllRealPay));
		textTag1.add("money", "本次理赔申请实赔保险金总计金额人民币" + allCountMoney + "(￥"
				+ String.valueOf(AllRealPay) + ")");

		String tAgentSQL = "SELECT DISTINCT b.name,getUniteCode(a.agentcode) "
				+ " FROM llclaimdetail a,laagent b "
				+ " WHERE a.agentcode=b.agentcode " + " AND a.rgtno='" + rgtNo
				+ "' and a.grpcontno='" + grpcont + "' WITH UR";
		SSRS tAgentSSRS = tExeSQL.execSQL(tAgentSQL);
		textTag1.add("grpname1", grpname);
		if (tAgentSSRS.getMaxRow() <= 0) {

			textTag1.add("agentname", " ");
			textTag1.add("agentno", " ");
		} else {

			textTag1.add("agentname", tAgentSSRS.GetText(1, 1));
			textTag1.add("agentno", tAgentSSRS.GetText(1, 2));
		}

		return true;
	}

	public String getGetDutyName(String strGetDutyCode) {
		LMDutyGetDB tLMDutyGetDB = new LMDutyGetDB();
		tLMDutyGetDB.setGetDutyCode(strGetDutyCode);
		if (!tLMDutyGetDB.getInfo()) {
			// @@错误处理
			this.mErrors.copyAllErrors(tLMDutyGetDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "ClaimDetailPrintBL";
			tError.functionName = "getBaseData";
			tError.errorMessage = "给付责任名称查询失败!";
			this.mErrors.addOneError(tError);

			return null;
		}
		return tLMDutyGetDB.getGetDutyName();
	}

	public String trunGiveType(String GiveType) {
		String reGiveType = "";
		if ("1".equals(GiveType)) {
			reGiveType = "A";
			if (mremarktag[0] < 1) {
				mremark += "A、正常给付   ";
			}
			mremarktag[0] = 1;
		}
		if ("2".equals(GiveType)) {
			reGiveType = "B";
			if (mremarktag[1] < 1) {
				mremark += "B、部分给付   ";
			}
			mremarktag[1] = 1;
		}
		if ("3".equals(GiveType)) {
			reGiveType = "C";
			if (mremarktag[2] < 1) {
				mremark += "C、全额拒付   ";
			}
			mremarktag[2] = 1;
		}
		if ("5".equals(GiveType)) {
			reGiveType = "D";
			if (mremarktag[3] < 1) {
				mremark += "D、通融给付   ";
			}
			mremarktag[3] = 1;
		}
		if ("4".equals(GiveType)) {
			reGiveType = "E";
			if (mremarktag[4] < 1) {
				mremark += "E、协议给付   ";
			}
			mremarktag[4] = 1;
		}

		return reGiveType;
	}

	public double getselfamnt(String getdutyname) {
		System.out.println("&&&&&" + getdutyname);
		double amnt = 0.0;
		if (getdutyname.equals("低段保险金")) {
			amnt = lowamnt;
		}
		if (getdutyname.equals("中段保险金")) {
			amnt = midamnt;
		}
		if (getdutyname.equals("高段保险金1")) {
			amnt = highamnt1;
		}
		if (getdutyname.equals("高段保险金2")) {
			amnt = highamnt2;
		}
		if (getdutyname.equals("超高段保险金")) {
			amnt = supinhosamnt;
		}
		if (getdutyname.equals("大额门急诊保险金")) {
			amnt = supdooramnt;
		}
		if (getdutyname.equals("小额门急诊保险金")) {
			amnt = smalldooramnt;
		}
		if (getdutyname.equals("门急诊保险金")) {
			amnt = emergdooramnt;
		}
		System.out.println("******" + amnt);
		amnt = Double.parseDouble(new DecimalFormat("0.00").format(amnt));
		return amnt;
	}

	private boolean getBaseData(String cCaseNo, String cCaseRelaNo) {

		LLClaimSet tLLClaimSet = new LLClaimSet();
		LLClaimDB tLLClaimDB = new LLClaimDB();
		tLLClaimDB.setCaseNo(cCaseNo);
		tLLClaimSet.set(tLLClaimDB.query());
		if (tLLClaimDB.mErrors.needDealError() == true || tLLClaimSet == null
				|| tLLClaimSet.size() == 0) {
			CError.buildErr(this, "赔案信息查询失败");
			return false;
		}

		mLLClaimSchema.setSchema(tLLClaimSet.get(1));
		mClmNo = mLLClaimSchema.getClmNo();

		// 查询立案信息
		LLRegisterDB tLLRegisterDB = new LLRegisterDB();
		tLLRegisterDB.setRgtNo(mRgtNo);
		if (!tLLRegisterDB.getInfo()) {
			CError.buildErr(this, "立案信息查询失败");
			return false;
		}
		mRgtClass = tLLRegisterDB.getRgtClass();
		mGrpName += tLLRegisterDB.getGrpName();
		mRgtantAddress += tLLRegisterDB.getRgtantAddress();
		applyertype = tLLRegisterDB.getApplyerType();
		// 查询分案信息
		LLCaseDB tLLCaseDB = new LLCaseDB();
		tLLCaseDB.setCaseNo(cCaseNo);
		if (!tLLCaseDB.getInfo()) {
			CError.buildErr(this, "案件信息查询失败");
			return false;
		}
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		tLLCaseSchema.setSchema(tLLCaseDB.getSchema());
		
		// 查询赔付明细信息
		LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
		tLLClaimDetailDB.setClmNo(mClmNo);
		tLLClaimDetailDB.setCaseRelaNo(cCaseRelaNo);
		mLLClaimDetailSet.set(tLLClaimDetailDB.query());
		if (tLLClaimDetailDB.mErrors.needDealError() == true) {
			CError.buildErr(this, "赔付明细信息查询失败");
			return false;
		}

		// 查询帐单信息
		LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
		tLLFeeMainDB.setCaseNo(cCaseNo);
		tLLFeeMainDB.setCaseRelaNo(cCaseRelaNo);
		mLLFeeMainSet.set(tLLFeeMainDB.query());
		if (tLLFeeMainDB.mErrors.needDealError() == true) {
			CError.buildErr(this, "帐单信息查询失败");
			return false;
		}
		LLSecurityReceiptDB tLLSecurityReceiptDB = new LLSecurityReceiptDB();
		tLLSecurityReceiptDB.setCaseNo(cCaseNo);
		String sql = "";
		if (cCaseRelaNo != null && !cCaseRelaNo.equals("")
				&& !cCaseRelaNo.equals("null")) {
			sql = "select * from LLSecurityReceipt where mainfeeno in "
					+ "(select mainfeeno from llfeemain where caseno='"
					+ cCaseNo + "' and caserelano='" + cCaseRelaNo + "')";
		} else {
			sql = "select * from LLSecurityReceipt where mainfeeno in "
					+ "(select mainfeeno from llfeemain where caseno='"
					+ cCaseNo + "')";
		}
		mLLSecurityReceiptSet.set(tLLSecurityReceiptDB.executeQuery(sql));

		// 查询帐单费用明细信息
		LLCaseReceiptDB tLLCaseReceiptDB = new LLCaseReceiptDB();
		tLLCaseReceiptDB.setCaseNo(cCaseNo);
		mLLCaseReceiptSet.set(tLLCaseReceiptDB.query());
		if (tLLCaseReceiptDB.mErrors.needDealError() == true) {
			CError.buildErr(this, "帐单费用明细信息查询失败");
			return false;
		}

		return true;
	}

	private boolean getInputData(VData mInputData) {
		mTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		
		mRgtNo = (String)mTransferData.getValueByName("rgtNo");

		mGrpcontno = (String) mTransferData.getValueByName("grpcontno");

		mCustomerNo = (String) mTransferData.getValueByName("customerno");

		System.out.println("getInputData:" + mRgtNo + "--------"+mGrpcontno+"---"+mCustomerNo );

		return true;
	}

	/**
     * 写入错误消息
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg)
    {
        CError error = new CError();
        error.moduleName = "ClaimDetailPrintCVSBL";
        error.functionName = cFunction;
        error.errorMessage = cErrorMsg;
        this.mErrors.addOneError(error);
    }

    /**
     * 获得结果
     * @return VData
     */
    public VData getResult()
    {
        return this.mResult;
    }
    
    /**
     * 得到xml的输入流
     * @return InputStream
     */
    public InputStream getInputStream()
    {
        return xml.getInputStream();
    }


	public static void main(String[] args) {

		

	}

}
