/**
 * 
 */
package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.CachedRiskInfo;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * 案件审批
 * 
 * @author maning
 * @version 1.0
 */
public class LLCaseUnderWrite extends LLCaseProcess {

	/**
	 * 
	 */
	public LLCaseUnderWrite() {
		// TODO Auto-generated constructor stub
	}

	// 业务逻辑错误
	public CErrors mErrors = new CErrors();

	// 获取的业务数据
	private VData mInputData = new VData();

	// 返回处理后数据
	private VData mResult = new VData();

	// 返回的提交数据库MMap
	private MMap mMMap = new MMap();

	// 用户登录信息
	private GlobalInput mGlobalInput = new GlobalInput();

	// 操作类型
	private String mOperate = "";

	// 案件信息
	private LLCaseSchema mLLCaseSchema = null;

	// 理算汇总
	private LLClaimSchema mLLClaimSchema = null;

	// 理算信息
	private LLClaimPolicySet mLLClaimPolicySet = null;

	// 理赔明细
	private LLClaimDetailSet mLLClaimDetailSet = null;

	// 核赔信息
	private LLClaimUWMainSchema mLLClaimUWMainSchema = null;

	// 核赔明细
	private LLClaimUnderwriteSet mLLClaimUnderwriteSet = null;

	// 核赔赔付明细
	private LLClaimUWDetailSet mLLClaimUWDetailSet = null;

	// 核赔轨迹
	private LLClaimUWMDetailSchema mLLClaimUWMDetailSchema = null;

	// 理赔人上级
	private String mUpUserCode = "";

	// 理赔人上级权限
	private String mUpClaimPopedom = "";

	// 理赔人权限
	private String mClaimPopedom = "";

	// 理赔人类别
	private String mClaimType = "";

	// 当前日期
	private String mCurrentDate = PubFun.getCurrentDate();

	// 当前事件
	private String mCurrentTime = PubFun.getCurrentTime();

	// 险种缓存
	private CachedRiskInfo mCachedRiskInfo = CachedRiskInfo.getInstance();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sinosoft.lis.llcase.LLCaseProcess#getMMap()
	 */
	public MMap getMMap() throws Exception {
		mMMap.put(mLLCaseSchema, "DELETE&INSERT");
		mMMap.put(mLLClaimUWMainSchema, "DELETE&INSERT");
		if (mLLClaimUnderwriteSet != null && mLLClaimUnderwriteSet.size() > 0) {
			mMMap.put(mLLClaimUnderwriteSet.get(1), "INSERT");
		}
		if (mLLClaimUWDetailSet != null && mLLClaimUWDetailSet.size() > 0) {
			//只取一条到险种层级
			mMMap.put(mLLClaimUWDetailSet.get(1), "INSERT");
		}
		mMMap.put(mLLClaimUWMDetailSchema, "INSERT");

		return mMMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sinosoft.lis.llcase.LLCaseProcess#getResult()
	 */
	public VData getResult() throws Exception {
		mResult.add(mLLCaseSchema);
		mResult.add(mLLClaimUWMainSchema);
		if (mLLClaimUnderwriteSet != null && mLLClaimUnderwriteSet.size() > 0) {
			mResult.add(mLLClaimUnderwriteSet.get(1));
		}
		if (mLLClaimUWDetailSet != null && mLLClaimUWDetailSet.size() > 0) {
			mResult.add(mLLClaimUWDetailSet.get(1));
		}
		mResult.add(mLLClaimUWMDetailSchema);
		return mResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sinosoft.lis.llcase.LLCaseProcess#submitData(com.sinosoft.utility.VData,
	 *      java.lang.String)
	 */
	public boolean submitData(VData aInputData, String aOperate)
			throws Exception {
		System.out.println("开始案件审批处理");
		mInputData = aInputData;
		mOperate = aOperate;

		if (!getInputData()) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		 //提交数据库
        PubSubmit tPubSubmit = new PubSubmit();
        mInputData.clear();       
        mInputData.add(getMMap());
        if (!tPubSubmit.submitData(mInputData, ""))
        {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LLCaseUnderWrite";
                tError.functionName = "dealData";
                tError.errorMessage = "理赔审批审定数据保存失败!";
                this.mErrors.addOneError(tError);
                return false;
        }
		System.out.println("案件审批处理结束");
		return true;
	}

	/**
	 * 解析获取的对象
	 * 
	 * @return 获取数据成功标志
	 */
	private boolean getInputData() throws Exception {
		mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0);

		if (mGlobalInput == null) {
			mErrors.addOneError("用户登录信息获取失败");
			return false;
		}

		mLLCaseSchema = (LLCaseSchema) mInputData.getObjectByObjectName(
				"LLCaseSchema", 0);

		if (mLLCaseSchema == null) {
			mErrors.addOneError("案件信息获取失败");
			return false;
		}

		mLLClaimSchema = (LLClaimSchema) mInputData.getObjectByObjectName(
				"LLClaimSchema", 0);
		if (mLLClaimSchema == null) {
			mErrors.addOneError("案件理算汇总获取失败");
			return false;
		}

		mLLClaimPolicySet = (LLClaimPolicySet) mInputData
				.getObjectByObjectName("LLClaimPolicySet", 0);

		if (mLLClaimPolicySet == null) {
			mErrors.addOneError("案件理算信息获取失败");
			return false;
		}

		mLLClaimUWMainSchema = (LLClaimUWMainSchema) mInputData
				.getObjectByObjectName("LLClaimUWMainSchema", 0);
		if (mLLClaimUWMainSchema == null) {
			mErrors.addOneError("案件核赔信息获取失败");
			return false;
		}

		return true;
	}

	/**
	 * 基本校验
	 * 
	 * @return 成功标志
	 * @throws Exception
	 */
	private boolean checkData() throws Exception {

		if (mGlobalInput.Operator == null || "".equals(mGlobalInput.Operator)) {
			mErrors.addOneError("操作用户登录信息获取失败");
			return false;
		}

		if (mGlobalInput.ManageCom == null || "".equals(mGlobalInput.ManageCom)) {
			mErrors.addOneError("操作用户登录信息获取失败");
			return false;
		}

		if (!"ENDCASE".equals(mOperate) && !"UWCASE".equals(mOperate)) {
			mErrors.addOneError("获取的操作类型错误");
			return false;
		}

		if (mLLCaseSchema.getCaseNo() == null
				|| "".equals(mLLCaseSchema.getCaseNo())) {
			mErrors.addOneError("理赔案件号获取失败");
			return false;
		}

		if (mLLCaseSchema.getRgtNo() == null
				|| "".equals(mLLCaseSchema.getRgtNo())) {
			mErrors.addOneError("理赔批次号获取失败");
			return false;
		}

		if (!"04".equals(mLLCaseSchema.getRgtState())
				&& !"05".equals(mLLCaseSchema.getRgtState())
				&& !"06".equals(mLLCaseSchema.getRgtState())
				&& !"10".equals(mLLCaseSchema.getRgtState())) {
			mErrors.addOneError("案件状态错误，不能进行处理");
			return false;
		}

		if (mLLCaseSchema.getHandler() == null
				|| "".equals(mLLCaseSchema.getHandler())) {
			mErrors.addOneError("案件没有分配处理人，不能进行操作");
			return false;
		}

		// 查询用户核赔权限及上级信息
		/*String tClaimUserSQL = "SELECT a.usercode,a.claimpopedom,a.upusercode,"
				+ "(SELECT b.claimpopedom FROM LLClaimUser b WHERE b.usercode=a.upusercode),a.uwflag "
				+ " FROM LLClaimUser a WHERE a.usercode= '"
				+ mGlobalInput.Operator + "'";*/
		
		String tClaimUserSQL = "SELECT a.SurveyFlag "
				+ " FROM LLSocialClaimUser a WHERE a.usercode= '"
				+ mGlobalInput.Operator + "'";
		
		System.out.println(tClaimUserSQL);
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(tClaimUserSQL);
		if (tSSRS.getMaxRow() <= 0) {
			mErrors.addOneError("用户核赔权限查询失败!用户代码：" + mGlobalInput.Operator);
			return false;
		}
		//mUpUserCode = tSSRS.GetText(1, 3);
		//mClaimType = tSSRS.GetText(1, 5);
		
		mClaimPopedom = tSSRS.GetText(1, 1);
		if (mClaimPopedom.equals("")) {
			mErrors.addOneError("用户无核赔权限!");
			return false;
		}
//		if (mClaimType.equals("")) {
//			mErrors.addOneError("用户无人员类别!");
//			return false;
//		}
		//mUpClaimPopedom = tSSRS.GetText(1, 4);

		if (mLLClaimSchema.getClmNo() == null
				|| "".equals(mLLClaimSchema.getClmNo())) {
			mErrors.addOneError("获取案件赔付信息失败");
			return false;
		}

		if (mLLClaimSchema.getGiveType() == null
				|| "".equals(mLLClaimSchema.getGiveType())) {
			mErrors.addOneError("获取案件赔付结论失败");
			return false;
		}

		if (mLLClaimPolicySet.size() <= 0) {
			mErrors.addOneError("获取理算数据失败");
			return false;
		}

		if (mLLClaimUWMainSchema.getRemark1() == null
				|| "".equals(mLLClaimUWMainSchema.getRemark1())) {
			mErrors.addOneError("获取审批意见失败");
			return false;
		}

		return true;
	}

	/**
	 * 业务处理方法
	 * 
	 * @return 处理成功标志
	 * @throws Exception
	 */
	private boolean dealData() throws Exception {
		if ("ENDCASE".equals(mOperate)) {
			if (!ENDCase()) {
				return false;
			}
		}

		if ("UWCASE".equals(mOperate)) {
			if (!UWCase()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 直接结案
	 * 
	 * @return
	 * @throws Exception
	 */
	private boolean ENDCase() throws Exception {
		if (!mLLCaseSchema.getHandler().equals(mGlobalInput.Operator)) {
			mErrors.addOneError("当前操作人不是案件处理人，不能自动结案");
			return false;
		}

		mLLClaimUWDetailSet = new LLClaimUWDetailSet();
		mLLClaimUnderwriteSet = new LLClaimUnderwriteSet();

		for (int i = 1; i <= mLLClaimPolicySet.size(); i++) {
			LLClaimPolicySchema tLLClaimPolicySchema = mLLClaimPolicySet.get(i);
			if (tLLClaimPolicySchema.getGiveType() == null
					|| "".equals(tLLClaimPolicySchema.getGiveType())) {
				mErrors.addOneError("保单无赔付结论,请先做理算确认！");
				return false;
			}

			if (!mLLClaimSchema.getClmNo().equals(
					tLLClaimPolicySchema.getClmNo())) {
				mErrors.addOneError("案件险种赔付信息与案件汇总不符");
				return false;
			}

			/**
			 * 处理核赔明细
			 */
			LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new LLClaimUnderwriteSchema();

			// 理算核赔号
			tLLClaimUnderwriteSchema.setClmNo(tLLClaimPolicySchema.getClmNo());

			// 事件关联号
			tLLClaimUnderwriteSchema.setCaseRelaNo(tLLClaimPolicySchema
					.getCaseRelaNo());

			// 批次号
			tLLClaimUnderwriteSchema.setRgtNo(tLLClaimPolicySchema.getRgtNo());

			// 案件号
			tLLClaimUnderwriteSchema
					.setCaseNo(tLLClaimPolicySchema.getCaseNo());

			// 团体保单号
			tLLClaimUnderwriteSchema.setGrpContNo(tLLClaimPolicySchema
					.getGrpContNo());

			// 团体险种号
			tLLClaimUnderwriteSchema.setGrpPolNo(tLLClaimPolicySchema
					.getGrpContNo());

			// 个人保单号
			tLLClaimUnderwriteSchema
					.setContNo(tLLClaimPolicySchema.getContNo());

			// 个人险种号
			tLLClaimUnderwriteSchema.setPolNo(tLLClaimPolicySchema.getPolNo());

			// 险类代码
			tLLClaimUnderwriteSchema.setKindCode(tLLClaimPolicySchema
					.getKindCode());

			// 险种代码
			tLLClaimUnderwriteSchema.setRiskCode(tLLClaimPolicySchema
					.getRiskCode());

			// 险种版本号
			tLLClaimUnderwriteSchema.setRiskVer(tLLClaimPolicySchema
					.getRiskVer());

			// 保单管理机构
			tLLClaimUnderwriteSchema.setPolMngCom(tLLClaimPolicySchema
					.getPolMngCom());

			// 销售渠道
			tLLClaimUnderwriteSchema.setSaleChnl(tLLClaimPolicySchema
					.getSaleChnl());

			// 代理人代码
			tLLClaimUnderwriteSchema.setAgentCode(tLLClaimPolicySchema
					.getAgentCode());

			// 代理人组别
			tLLClaimUnderwriteSchema.setAgentGroup(tLLClaimPolicySchema
					.getAgentGroup());

			// 被保人客户号
			tLLClaimUnderwriteSchema.setInsuredNo(tLLClaimPolicySchema
					.getInsuredNo());

			// 被保人名称
			tLLClaimUnderwriteSchema.setInsuredName(tLLClaimPolicySchema
					.getInsuredName());

			// 投保人客户号
			tLLClaimUnderwriteSchema.setAppntNo(tLLClaimPolicySchema
					.getAppntNo());

			// 投保人名称
			tLLClaimUnderwriteSchema.setAppntName(tLLClaimPolicySchema
					.getAppntName());

			// 保单生效日期
			tLLClaimUnderwriteSchema.setCValiDate(tLLClaimPolicySchema
					.getCValiDate());

			// 保单状态
			tLLClaimUnderwriteSchema.setPolState(tLLClaimPolicySchema
					.getPolState());

			// 核算赔付金额
			tLLClaimUnderwriteSchema.setStandPay(tLLClaimPolicySchema
					.getStandPay());

			// 核赔赔付金额
			tLLClaimUnderwriteSchema.setRealPay(tLLClaimPolicySchema
					.getRealPay());

			// 核赔结论
			tLLClaimUnderwriteSchema.setClmDecision(tLLClaimPolicySchema
					.getGiveType());

			// 核赔依据
			tLLClaimUnderwriteSchema.setClmDepend(tLLClaimPolicySchema
					.getGiveReason());

			// 核赔员
			tLLClaimUnderwriteSchema.setClmUWer(mGlobalInput.Operator);

			// 核赔级别
			tLLClaimUnderwriteSchema.setClmUWGrade(mClaimPopedom);

			// 设定本次保单核赔的审核类型为[初次审核]
			tLLClaimUnderwriteSchema.setCheckType("0"); // 审批审定类型

			// 操作员
			tLLClaimUnderwriteSchema.setOperator(mGlobalInput.Operator);

			// 管理机构
			tLLClaimUnderwriteSchema.setMngCom(mGlobalInput.ManageCom);

			// 入机日期
			tLLClaimUnderwriteSchema.setMakeDate(mCurrentDate);

			// 入机时间
			tLLClaimUnderwriteSchema.setMakeTime(mCurrentTime);

			// 修改日期
			tLLClaimUnderwriteSchema.setModifyDate(mCurrentDate);

			// 修改时间
			tLLClaimUnderwriteSchema.setModifyTime(mCurrentTime);
			mLLClaimUnderwriteSet.add(tLLClaimUnderwriteSchema);

			/**
			 * 准备核赔履历记录数据LLClaimUWDetail
			 */
			LLClaimUWDetailSchema tLLClaimUWDetailSchema = new LLClaimUWDetailSchema();
			Reflections tReflections = new Reflections();
			// 根据LLClaimUnderwriteSchema获取信息
			tReflections.transFields(tLLClaimUWDetailSchema,
					tLLClaimUnderwriteSchema);

			// 给付责任类型
			tLLClaimUWDetailSchema.setGetDutyKind(tLLClaimPolicySchema
					.getGetDutyKind());

			// 给付责任编码
			tLLClaimUWDetailSchema.setGetDutyCode(tLLClaimPolicySchema
					.getGetDutyKind());
			// 核赔次数
			tLLClaimUWDetailSchema.setClmUWNo(String.valueOf(i));

			mLLClaimUWDetailSet.add(tLLClaimUWDetailSchema);
		}

		/**
		 * 准备核赔履历记录数据LLClaimUWMain
		 */
		LLClaimUWMainSchema tLLClaimUWMainSchema = new LLClaimUWMainSchema();
		tLLClaimUWMainSchema.setSchema(mLLClaimUWMainSchema);
		mLLClaimUWMainSchema = new LLClaimUWMainSchema();

		// 入机日期
		mLLClaimUWMainSchema.setMakeDate(mCurrentDate);

		// 入机时间
		mLLClaimUWMainSchema.setMakeTime(mCurrentTime);

		// 理算核赔号
		mLLClaimUWMainSchema.setClmNo(mLLClaimSchema.getClmNo());

		// 案件号
		mLLClaimUWMainSchema.setRgtNo(mLLCaseSchema.getCaseNo());

		// 批次号
		mLLClaimUWMainSchema.setCaseNo(mLLCaseSchema.getRgtNo());

		// 操作人
		mLLClaimUWMainSchema.setOperator(mGlobalInput.Operator);

		// 管理机构
		mLLClaimUWMainSchema.setMngCom(mGlobalInput.ManageCom);

		// 修改日期
		mLLClaimUWMainSchema.setModifyDate(mCurrentDate);

		// 修改时间
		mLLClaimUWMainSchema.setModifyTime(mCurrentTime);

		// 申请阶段
		mLLClaimUWMainSchema.setAppPhase("0"); // 审核

		// 核赔员
		mLLClaimUWMainSchema.setClmUWer(mGlobalInput.Operator);

		// 核赔级别
		mLLClaimUWMainSchema.setClmUWGrade(mClaimPopedom);

		// 核赔结论
		mLLClaimUWMainSchema.setClmDecision(mLLClaimSchema.getGiveType());

		// 审批结论
		mLLClaimUWMainSchema.setcheckDecision1("1");

		// 审批意见
		mLLClaimUWMainSchema.setRemark1(tLLClaimUWMainSchema.getRemark1());

		/**
		 * 准备案件核赔履历信息LLClaimUWMDetail
		 */
		LLClaimUWMDetailDB tLLClaimUWMDetailDB = new LLClaimUWMDetailDB();
		tLLClaimUWMDetailDB.setClmNo(mLLClaimSchema.getClmNo());
		int tCount = tLLClaimUWMDetailDB.getCount();
		tCount++;
		Reflections ref = new Reflections();
		mLLClaimUWMDetailSchema = new LLClaimUWMDetailSchema();
		// 根据LLClaimUWMainSchema获取信息
		ref.transFields(mLLClaimUWMDetailSchema, mLLClaimUWMainSchema);

		// 核赔次数
		mLLClaimUWMDetailSchema.setClmUWNo(String.valueOf(tCount));

		// 备注
		mLLClaimUWMDetailSchema.setRemark(mLLClaimUWMainSchema.getRemark1());

		// 操作员
		mLLClaimUWMDetailSchema.setOperator(mGlobalInput.Operator);

		// 管理机构
		mLLClaimUWMDetailSchema.setMngCom(mGlobalInput.ManageCom);

		// 入机日期
		mLLClaimUWMDetailSchema.setMakeDate(mCurrentDate);

		// 入机时间
		mLLClaimUWMDetailSchema.setMakeTime(mCurrentTime);

		// 修改日期
		mLLClaimUWMDetailSchema.setModifyDate(mCurrentDate);

		// 修改时间
		mLLClaimUWMDetailSchema.setModifyTime(mCurrentTime);

		/**
		 * 处理案件信息表LLCase
		 */

		// 案件状态
		mLLCaseSchema.setRgtState("09");

		// 审批人
		mLLCaseSchema.setUWer(mGlobalInput.Operator);

		// 审批日期
		mLLCaseSchema.setUWDate(mCurrentDate);

		// 签批人
		mLLCaseSchema.setSigner(mGlobalInput.Operator);

		// 签批日期
		mLLCaseSchema.setSignerDate(mCurrentDate);
		
		// 结案日期
		mLLCaseSchema.setEndCaseDate(mCurrentDate);

		// 修改日期
		mLLCaseSchema.setModifyDate(mCurrentDate);

		// 修改时间
		mLLCaseSchema.setModifyTime(mCurrentTime);

		return true;
	}

	/**
	 * 通过审批流程处理案件
	 * 
	 * @return
	 * @throws Exception
	 */
	private boolean UWCase() throws Exception {
		if ("04".equals(mLLCaseSchema.getRgtState())) {
			if (!dealLS()) {
				return true;
			}
		}

		// if ("05".equals(mLLCaseSchema.getRgtState())) {
		// if (!dealSP()) {
		// return true;
		// }
		// }
		//
		// if ("06".equals(mLLCaseSchema.getRgtState())) {
		// if (!dealSD()) {
		// return true;
		// }
		// }
		//
		// if ("10".equals(mLLCaseSchema.getRgtState())) {
		// if (!dealCJ()) {
		// return true;
		// }
		// }
		//
		// if ("09".equals(mLLCaseSchema.getRgtState())) {
		// if (!dealEndCase()) {
		// return true;
		// }
		// }
		return true;
	}

	/**
	 * 理算状态审批
	 * 
	 * @return
	 */
	private boolean dealLS() {
		boolean tIsPower = true;

		String tHandler = mLLCaseSchema.getHandler();
		if (tHandler == null || "".equals(tHandler)) {
			CError.buildErr(this, "案件受理时未指定审批人！");
			return false;
		}
		if (!tHandler.equals(mGlobalInput.Operator)) {
			CError.buildErr(this, "审批人员不符，案件受理时指定审批人为：" + tHandler);
			return false;
		}

		LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
		tLLClaimUserDB.setUserCode(mGlobalInput.Operator);

		if (!tLLClaimUserDB.getInfo()) {
			CError.buildErr(this, "操作人非理赔用户");
			return false;
		}
		LLClaimUnderwriteSet tLLClaimUnderwriteSet = new LLClaimUnderwriteSet();
		for (int i = 1; i <= mLLClaimPolicySet.size(); i++) {
			LLClaimPolicySchema tLLClaimPolicySchema = mLLClaimPolicySet.get(i);
			if (tLLClaimPolicySchema.getGiveType() == null
					|| "".equals(tLLClaimPolicySchema.getGiveType())) {
				CError.buildErr(this, "保单无赔付结论,请先做理算确认！");
				return false;
			}

			LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new LLClaimUnderwriteSchema();
			tLLClaimUnderwriteSchema.setClmNo(tLLClaimPolicySchema.getClmNo());
			tLLClaimUnderwriteSchema.setCaseRelaNo(tLLClaimPolicySchema
					.getCaseRelaNo());
			tLLClaimUnderwriteSchema.setRgtNo(tLLClaimPolicySchema.getRgtNo());
			tLLClaimUnderwriteSchema
					.setCaseNo(tLLClaimPolicySchema.getCaseNo());
			tLLClaimUnderwriteSchema.setGrpContNo(tLLClaimPolicySchema
					.getGrpContNo());
			tLLClaimUnderwriteSchema.setGrpPolNo(tLLClaimPolicySchema
					.getGrpContNo());
			tLLClaimUnderwriteSchema.setPolNo(tLLClaimPolicySchema.getPolNo());
			tLLClaimUnderwriteSchema.setKindCode(tLLClaimPolicySchema
					.getKindCode());
			tLLClaimUnderwriteSchema.setRiskCode(tLLClaimPolicySchema
					.getRiskCode());
			tLLClaimUnderwriteSchema.setRiskVer(tLLClaimPolicySchema
					.getRiskVer());
			tLLClaimUnderwriteSchema.setPolMngCom(tLLClaimPolicySchema
					.getPolMngCom());
			tLLClaimUnderwriteSchema.setSaleChnl(tLLClaimPolicySchema
					.getSaleChnl());
			tLLClaimUnderwriteSchema.setAgentCode(tLLClaimPolicySchema
					.getAgentCode());
			tLLClaimUnderwriteSchema.setAgentGroup(tLLClaimPolicySchema
					.getAgentGroup());
			tLLClaimUnderwriteSchema.setInsuredNo(tLLClaimPolicySchema
					.getInsuredNo());
			tLLClaimUnderwriteSchema.setInsuredName(tLLClaimPolicySchema
					.getInsuredName());
			tLLClaimUnderwriteSchema.setAppntNo(tLLClaimPolicySchema
					.getAppntNo());
			tLLClaimUnderwriteSchema.setAppntName(tLLClaimPolicySchema
					.getAppntName());
			tLLClaimUnderwriteSchema.setCValiDate(tLLClaimPolicySchema
					.getCValiDate());
			tLLClaimUnderwriteSchema.setPolState(tLLClaimPolicySchema
					.getPolState());
			tLLClaimUnderwriteSchema.setStandPay(tLLClaimPolicySchema
					.getStandPay());
			tLLClaimUnderwriteSchema.setRealPay(tLLClaimPolicySchema
					.getRealPay());

			tLLClaimUnderwriteSchema.setClmDecision(tLLClaimPolicySchema
					.getGiveType());
			tLLClaimUnderwriteSchema.setClmDepend(tLLClaimPolicySchema
					.getGiveReason());

			tLLClaimUnderwriteSchema.setClmUWer(tLLClaimPolicySchema
					.getClmUWer());
			tLLClaimUnderwriteSchema.setClmUWGrade(mClaimPopedom);

			if (!checkUserRight(tLLClaimPolicySchema)) {
				tIsPower = false;
				tLLClaimUnderwriteSchema.setAppGrade(mUpClaimPopedom);
				tLLClaimUnderwriteSchema.setAppClmUWer(mUpUserCode);
				tLLClaimUnderwriteSchema.setAppActionType("3"); // 申请动作 ** 待定 **
			}
            //准备核赔记录数据LLClaimUnderwrite
            tLLClaimUnderwriteSchema.setClmDecision(tLLClaimPolicySchema.getGiveType()); //核赔结论
            tLLClaimUnderwriteSchema.setClmDepend(tLLClaimPolicySchema.getGiveReason()); //核赔依据
            tLLClaimUnderwriteSchema.setClmUWer(mGlobalInput.Operator); //核赔员
            tLLClaimUnderwriteSchema.setClmUWGrade(mClaimPopedom);
			// 设定本次保单核赔的审核类型为[初次审核]
			tLLClaimUnderwriteSchema.setCheckType("0"); // 审批审定类型
			tLLClaimUnderwriteSchema.setOperator(mGlobalInput.Operator);
			tLLClaimUnderwriteSchema.setMngCom(mGlobalInput.ManageCom);
			tLLClaimUnderwriteSchema.setMakeDate(mCurrentDate);
			tLLClaimUnderwriteSchema.setMakeTime(mCurrentTime);
			tLLClaimUnderwriteSchema.setModifyDate(mCurrentDate);
			tLLClaimUnderwriteSchema.setModifyTime(mCurrentTime);
			System.out.println("1111111111111");
			tLLClaimUnderwriteSet.add(tLLClaimUnderwriteSchema);
			System.out.println("23222222222222");
		}
		mLLClaimUnderwriteSet = tLLClaimUnderwriteSet;
		
		/**
		 * 准备核赔履历记录数据LLClaimUWMain
		 */
		LLClaimUWMainSchema tLLClaimUWMainSchema = new LLClaimUWMainSchema();
		tLLClaimUWMainSchema.setSchema(mLLClaimUWMainSchema);
		mLLClaimUWMainSchema = new LLClaimUWMainSchema();

		// 入机日期
		mLLClaimUWMainSchema.setMakeDate(mCurrentDate);

		// 入机时间
		mLLClaimUWMainSchema.setMakeTime(mCurrentTime);

		// 理算核赔号
		mLLClaimUWMainSchema.setClmNo(mLLClaimSchema.getClmNo());

		// 案件号
		mLLClaimUWMainSchema.setRgtNo(mLLCaseSchema.getCaseNo());

		// 批次号
		mLLClaimUWMainSchema.setCaseNo(mLLCaseSchema.getRgtNo());

		// 操作人
		mLLClaimUWMainSchema.setOperator(mGlobalInput.Operator);

		// 管理机构
		mLLClaimUWMainSchema.setMngCom(mGlobalInput.ManageCom);

		// 修改日期
		mLLClaimUWMainSchema.setModifyDate(mCurrentDate);

		// 修改时间
		mLLClaimUWMainSchema.setModifyTime(mCurrentTime);

		// 申请阶段
		mLLClaimUWMainSchema.setAppPhase("0"); // 审核

		// 核赔员
		mLLClaimUWMainSchema.setClmUWer(mGlobalInput.Operator);

		// 核赔级别
		mLLClaimUWMainSchema.setClmUWGrade(mClaimPopedom);

		// 核赔结论
		mLLClaimUWMainSchema.setClmDecision(mLLClaimSchema.getGiveType());

		// 审批结论
		mLLClaimUWMainSchema.setcheckDecision1("1");

		// 审批意见
		mLLClaimUWMainSchema.setRemark1(tLLClaimUWMainSchema.getRemark1());
		
		// 审定人
		mLLClaimUWMainSchema.setAppClmUWer(mUpUserCode);

		/**
		 * 准备案件核赔履历信息LLClaimUWMDetail
		 */
		LLClaimUWMDetailDB tLLClaimUWMDetailDB = new LLClaimUWMDetailDB();
		tLLClaimUWMDetailDB.setClmNo(mLLClaimSchema.getClmNo());
		int tCount = tLLClaimUWMDetailDB.getCount();
		tCount++;
		Reflections ref = new Reflections();
		mLLClaimUWMDetailSchema = new LLClaimUWMDetailSchema();
		// 根据LLClaimUWMainSchema获取信息
		ref.transFields(mLLClaimUWMDetailSchema, mLLClaimUWMainSchema);

		// 核赔次数
		mLLClaimUWMDetailSchema.setClmUWNo(String.valueOf(tCount));

		// 备注
		mLLClaimUWMDetailSchema.setRemark(mLLClaimUWMainSchema.getRemark1());

		// 操作员
		mLLClaimUWMDetailSchema.setOperator(mGlobalInput.Operator);

		// 管理机构
		mLLClaimUWMDetailSchema.setMngCom(mGlobalInput.ManageCom);

		// 入机日期
		mLLClaimUWMDetailSchema.setMakeDate(mCurrentDate);

		// 入机时间
		mLLClaimUWMDetailSchema.setMakeTime(mCurrentTime);

		// 修改日期
		mLLClaimUWMDetailSchema.setModifyDate(mCurrentDate);

		// 修改时间
		mLLClaimUWMDetailSchema.setModifyTime(mCurrentTime);

		//如果有结案权限则直接结案
        if (tIsPower) {
            //设置案件状态为[结案状态]
            mLLCaseSchema.setRgtState("09");
            mLLCaseSchema.setUWer(mGlobalInput.Operator);
            mLLCaseSchema.setUWDate(mCurrentDate);
            mLLCaseSchema.setSigner(mGlobalInput.Operator);
            mLLCaseSchema.setSignerDate(mCurrentDate);
        }
        else{
		// 案件状态
		mLLCaseSchema.setRgtState("05");

		// 审批人
		mLLCaseSchema.setUWer(mGlobalInput.Operator);

		// 审批日期
		mLLCaseSchema.setUWDate(mCurrentDate);
		
		//当前处理人
		mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());

		// 修改日期
		mLLCaseSchema.setModifyDate(mCurrentDate);

		// 修改时间
		mLLCaseSchema.setModifyTime(mCurrentTime);
        }
		return true;
	}

	// 逐单校验核赔人核赔权限
	private boolean checkUserRight(LLClaimPolicySchema aLLClaimPolicySchema) {
		
		String strRiskcode = aLLClaimPolicySchema.getRiskCode(); //险种代码
		String tGiveKind = getGiveKind(strRiskcode); //给付类别
		
		double tRealpay = 0.0;
		if ("3".equals(aLLClaimPolicySchema.getGiveType())
				&& aLLClaimPolicySchema.getRealPay() == 0) {
			tRealpay = 0.0;
			String sql_rp = "select sum(DeclineAmnt) from llclaimdetail where clmno= '" +
		                    aLLClaimPolicySchema.getClmNo() + "' and polno='" +
		                    aLLClaimPolicySchema.getPolNo() + "' with ur";
		    ExeSQL tExeSQL = new ExeSQL();
		    String strRealpay = tExeSQL.getOneValue(sql_rp); //全额拒付金额
		    
		    if (!"".equals(strRealpay)) {
		    	tRealpay = Double.parseDouble(strRealpay);
		    }       
		} else {
			tRealpay = aLLClaimPolicySchema.getRealPay();
		}
		String tLimitMoney = Double
				.toString(Math.abs(Arith.round(tRealpay, 2)));

		// 目前系统中只有一条核赔规则，就懒得查了，-_-||(表情很好)
		String tCalCode = "QX0001";
		Calculator mCalculator = new Calculator();
		mCalculator.setCalCode(tCalCode);
		// 增加基本要素{核赔级别,给付类别,给付结论,给付金额}
		mCalculator.addBasicFactor("ClaimPopedom", mClaimPopedom);
		mCalculator.addBasicFactor("GetDutyKind", tGiveKind);
		mCalculator.addBasicFactor("GetDutyType", aLLClaimPolicySchema
				.getGiveType());
		mCalculator.addBasicFactor("LimitMoney", tLimitMoney);

		String tStr = mCalculator.calculate();
		if (tStr.trim().equals("") || tStr.trim().equals("0")) {
			return false;
		}
		return true;
	}
	
    //根据险种代码得到给付类别
    private String getGiveKind(String riskcode) {
        ExeSQL tExeSQL = new ExeSQL();
        
        String sql3 = "select risktype1 from lmriskapp where riskcode='"
                      + riskcode + "'";
        String risktype = tExeSQL.getOneValue(sql3);
        if (risktype.equals("2") || risktype.equals("5")) {
            return ("2");
        } else {
            return "1";
        }
    }

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
