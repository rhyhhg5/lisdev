/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: 青岛社保理赔磁盘导入的解析类，从一个XML数据流中解析出批次下分案信息</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author Xx
 * @version 6.0
*/

package com.sinosoft.lis.llcase;

import org.apache.xpath.XPathAPI;
import org.apache.xpath.objects.XObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LLAppClaimReasonSchema;
import com.sinosoft.lis.schema.LLCaseCureSchema;
import com.sinosoft.lis.schema.LLCaseOpTimeSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLFeeMainSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLSecurityReceiptSchema;
import com.sinosoft.lis.vschema.LLCaseCureSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class QDSCParser implements SIParser{

    //案件
    private static String PATH_INSURED = "ROW";
    private static String PATH_SecurityNo = "SecurityNo";
    private static String PATH_CustomerName = "CustomerName";
    private static String PATH_Sex = "Sex";
    private static String PATH_Birthday = "Birthday";
    private static String PATH_IDNo = "IDNo";
    private static String PATH_GrpNo = "GrpNo";
    private static String PATH_HosSecNo = "HosSecNo";
    private static String PATH_HospitalName = "HospitalName";
    private static String PATH_inpatientNo = "inpatientNo";
    private static String PATH_HospStartDate = "HospStartDate";
    private static String PATH_HospEndDate = "HospEndDate";
    private static String PATH_MICDCode = "InICDCode";
    private static String PATH_SICDCode = "OutICDCode";
    private static String PATH_FeeType = "FeeType";
    private static String PATH_BudgetType = "BudgetType";
    private static String PATH_FeeDate = "FeeDate";
    private static String PATH_ApplyDate = "ApplyDate";
    private static String PATH_ApplyAmnt = "ApplyAmnt";
    private static String PATH_FeeInSecu = "FeeInSecu";
    private static String PATH_PlanFee = "PlanFee";
    private static String PATH_SupInHosFee = "SupInHosFee";
    private static String PATH_SecuRefuseFee = "SecuRefuseFee";
    private static String PATH_HosFlag = "HosFlag"; //医疗标记

    // 下面是一些常量定义
    private String mRgtNo = "";
    private String mGrpContNo = "";
    private String mPath = "";
    private String[] Title = null;
    private String vtsName = "ClaimErrList.vts";
    private ListTable  FalseListTable = new ListTable();
    private GlobalInput mG = new GlobalInput();
    public CErrors mErrors = new CErrors();

    public QDSCParser() {
    }

    public boolean setGlobalInput(GlobalInput aG) {
        this.mG = aG;
        FalseListTable.setName("FAULT");
        return true;
    }

    public void setFormTitle(String[] cTitle,String cvtsName){
        Title = cTitle;
        vtsName = cvtsName;
    }

    /**
     * 解析案件结点(公用函数)
     * @param node Node
     * @return VData
     */
    public VData parseOneInsuredNode(Node node) {
        VData tReturn = new VData();
        // 得到被保人信息
        NodeList nodeList = null;
        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_INSURED);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
            return null;
        }
        int nLength = nodeList == null ? 0 : nodeList.getLength();
        int FailureCount = 0;
        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeCase = nodeList.item(nIndex);
            if(!genCase(nodeCase)){
                System.out.println("导入这个人失败");
                FailureCount++;
                //写导出错误xml的函数
            }
        }
        
        String[] tErrorTitle = {"职工电脑编号", "姓名", "性别", "出生日期",
                         "公民身份号码", "单位编号", "医院编码", "医院名称",
                         "住院流水号", "入院日期", "出院日期",
                         "入院诊断疾病编码", "出院疾病编码", "医疗形式", "结算方式",
                         "医院结算时间", "医保结算时间", "医疗总额", "纳入统筹额",
                         "统筹支付额", "大额支付额", "现金支付额","医疗标记", "错误原因"};
        String[] tErrorType = {"String", "String", "String", "DateTime",
                "String", "String", "String", "String",
                "String", "DateTime", "DateTime",
                "String", "String", "String", "String",
                "DateTime", "DateTime", "Number", "Number",
                "Number", "Number", "Number","String", "String"};
        
        LLExportErrorList xmlexport = new LLExportErrorList(); //新建一个XmlExport的实例
        xmlexport.createDocument(mRgtNo);
        xmlexport.addListTable(FalseListTable, tErrorTitle, tErrorType);
        xmlexport.outputDocumentToFile(mPath, mRgtNo); //输出xml文档到文件
        String FailureNum = FailureCount+"";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("FailureNum",FailureNum);
        tReturn.clear();
//        tReturn.addElement(xmlexport);
        tReturn.add(tTransferData);
        return tReturn;
    }

    public boolean setParam(String aRgtNo,String aGrpContNo,String aPath){
        mRgtNo = aRgtNo;
        mGrpContNo = aGrpContNo;
        mPath = aPath;
        return true;
    }

    /**
     * 利用XPathAPI取得某个节点的节点值
     * @param node Node
     * @param strPath String
     * @return String
     */
    private static String parseNode(Node node, String strPath) {
        String strValue = "";
        try {
            XObject xobj = XPathAPI.eval(node, strPath);
            strValue = xobj == null ? "" : xobj.toString();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return strValue;
    }

    private boolean genCase(Node nodeCase){
        String aCustomerName = parseNode(nodeCase,QDSCParser.PATH_CustomerName);
        String aSecurityNo = parseNode(nodeCase,QDSCParser.PATH_SecurityNo);
        if("".equals("aCustomerName")||null==aCustomerName||
           "".equals("aSecurityNo")||null==aSecurityNo)
        {
        	String ErrMessage = "导入模板中的客户姓名或职工电脑编号不能为空!";
        	getFalseList(nodeCase,ErrMessage);
            return false;
        }
        String strSQL0 ="select distinct a.insuredno,a.sex,a.birthday,a.idno,a.insuredstat,a.idtype ";
        String sqlpart1="from lcinsured a where ";
        String sqlpart2="from lbinsured a where ";
        String wherePart = " a.othidno='"+aSecurityNo+"' and a.GrpContNo = '"+mGrpContNo
                         + "' and a.name='"+aCustomerName+"' ";
        String strSQL=strSQL0+sqlpart1+ wherePart+" union "+strSQL0+sqlpart2+wherePart;
        ExeSQL texesql = new ExeSQL();
        SSRS tssrs = new SSRS();
        tssrs = texesql.execSQL(strSQL);
        System.out.println(strSQL);
        if(tssrs==null||tssrs.getMaxRow()<=0){
            String ErrMessage = "团单"+mGrpContNo+"下没有社保号为"+aSecurityNo+"的被保险人,请检查证件号码和姓名是否正确。";
            getFalseList(nodeCase,ErrMessage);
            return false;
        }
        String result = LLCaseCommon.checkPerson(tssrs.GetText(1, 1));
        if(!"".equals(result)){
            String ErrMessage = "团单"+mGrpContNo+"下"+ "该客户正在进行保全操作！";
            getFalseList(nodeCase,ErrMessage);
            return false;
        }
        String strCustomerSQL="select * from lpedorapp a,lpgrpedoritem b where a.edoracceptno "
        		+" in (select max(edorno) from lpdiskimport where  insuredno='"+tssrs.GetText(1, 1)+"' "
        		+" and grpcontno ='"+mGrpContNo+"') and a.edorstate !='0' and a.edoracceptno = b.edoracceptno "
        		+" and b.edortype ='ZT'";
        SSRS customertssrs = new SSRS();
        customertssrs = texesql.execSQL(strCustomerSQL);
        if(customertssrs.getMaxRow()>0){
        	String ErrMessage ="团单"+mGrpContNo+"下"+ "该客户正在进行保全减人操作！";
            getFalseList(nodeCase, ErrMessage);
            return false;

        }
        if(parseNode(nodeCase,QDSCParser.PATH_ApplyDate)==null||
           parseNode(nodeCase,QDSCParser.PATH_ApplyDate).equals("")){
            String ErrMessage = "社保结算日期为空。";
            getFalseList(nodeCase,ErrMessage);
            return false;
        }
        if(parseNode(nodeCase,QDSCParser.PATH_SupInHosFee)==null||
           parseNode(nodeCase,QDSCParser.PATH_SupInHosFee).equals("")){
            String ErrMessage = "大额支付额为空，无法进行理算。";
            getFalseList(nodeCase,ErrMessage);
            return false;
        }

        //校验客户基本信息姓名、性别、出生日期、身份证号是否与系统一致
        String aCustomerNo = tssrs.GetText(1,1);
        String aSex = parseNode(nodeCase,QDSCParser.PATH_Sex);//tssrs.GetText(1,2)
        String aIDNo = parseNode(nodeCase,QDSCParser.PATH_IDNo);
        if(parseNode(nodeCase,QDSCParser.PATH_Sex)!=null &&!parseNode(nodeCase,QDSCParser.PATH_Sex).equals(""))           
        {
        	 aSex = parseNode(nodeCase,QDSCParser.PATH_Sex);
        	if(!aSex.equals(tssrs.GetText(1,2)))
        	{
        		String ErrMessage ="出险人"+aCustomerNo+"的性别与系统中的性别不一致!";
	            getFalseList(nodeCase,ErrMessage);
	        	return false;
        	}
        }else aSex = tssrs.GetText(1,2);  
        
        String aBirthday = "";
        if(parseNode(nodeCase,QDSCParser.PATH_Birthday)!=null &&!parseNode(nodeCase,QDSCParser.PATH_Birthday).equals(""))
        {
        	 aBirthday = FormatDate(parseNode(nodeCase,QDSCParser.PATH_Birthday));
 	        if(!aBirthday.equals(tssrs.GetText(1,3)))
 	    	{
 	    		String ErrMessage ="出险人"+aCustomerNo+"的出生日期与系统中的出生日期不一致!";
 	            getFalseList(nodeCase,ErrMessage);
 	        	return false;
 	    	}
        }           
        else aBirthday = tssrs.GetText(1,3);
        
        String tIdtype=tssrs.GetText(1, 6).equals("null") ? "" : tssrs.GetText(1, 6);;//获得证件类型
        String tId = "";
        if(parseNode(nodeCase,QDSCParser.PATH_IDNo)!=null &&!parseNode(nodeCase,QDSCParser.PATH_IDNo).equals(""))
        {
        	tId = parseNode(nodeCase,QDSCParser.PATH_IDNo);
	        if(!tId.equals(tssrs.GetText(1,4)))
	    	{
	    		String ErrMessage ="出险人"+aCustomerNo+"的证件号码与系统中的证件号码不一致!";
	            getFalseList(nodeCase,ErrMessage);
	        	return false;
	    	}
        }        
        else tId = tssrs.GetText(1,4);
	                
        /* if(tIdtype.equals("0"))//是身份证时，对身份证号进行校验
        {	       
	        if(!PubFun.checkIDNo(tId))//校验数据库里存储的身份证号，by zzh 20100925
	        {	
	        	String ErrMessage ="社保号为"+aSecurityNo+"的客户身份证号错误，请先做客户资料变更！";
	            getFalseList(nodeCase,ErrMessage);
	        	return false;
	        }
        }*/
        
        
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
        LLSecurityReceiptSchema tLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
        LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
        LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
        VData tVData = new VData();

        genCaseInfoBL tgenCaseInfoBL  = new genCaseInfoBL();

        tLLCaseSchema.setRgtType("1");
        tLLRegisterSchema.setRgtNo(mRgtNo);
        tLLRegisterSchema.setRgtObjNo(mGrpContNo);

        LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
        tLLAppClaimReasonSchema.setReasonCode("02");        //原因代码
        tLLAppClaimReasonSchema.setReason("住院");                //申请原因
        tLLAppClaimReasonSchema.setCustomerNo(aCustomerNo);
        tLLAppClaimReasonSchema.setReasonType("0");

        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setRgtNo(mRgtNo);
        tLLCaseDB.setCustomerNo(aCustomerNo);
        LLCaseSet temLLCaseSet = tLLCaseDB.query();
        if(temLLCaseSet.size()>0){
            tLLCaseSchema.setUWState("7");
        }
        
//      modify by zhangyige 2012-07-09
        String aPrePaidFlag = "";
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if(tLLRegisterDB.getInfo())
		{
			aPrePaidFlag = tLLRegisterDB.getSchema().getPrePaidFlag();
		}
        tLLCaseSchema.setPrePaidFlag(aPrePaidFlag);
        
        tLLCaseSchema.setRgtState("03");//案件状态
        tLLCaseSchema.setCustomerName(aCustomerName);
        tLLCaseSchema.setCustomerNo(aCustomerNo);
//        tLLCaseSchema.setDeathDate(request.getParameter("DeathDate"));
//        tLLCaseSchema.setCustState(request.getParameter("CustStatus"));
//待补入数据
        tLLCaseSchema.setCaseNo("");

        tLLCaseSchema.setRgtNo(mRgtNo);
        tLLCaseSchema.setSurveyFlag("0");
        tLLCaseSchema.setAccdentDesc("磁盘导入");
        tLLCaseSchema.setRgtState("03");//案件状态
        tLLCaseSchema.setCustBirthday(aBirthday);
        tLLCaseSchema.setCustomerSex(aSex);
        tLLCaseSchema.setIDType("0");
        if(parseNode(nodeCase,QDSCParser.PATH_IDNo)!=null
           &&!parseNode(nodeCase,QDSCParser.PATH_IDNo).equals(""))
            tLLCaseSchema.setIDNo(parseNode(nodeCase,QDSCParser.PATH_IDNo));
        else
            tLLCaseSchema.setIDNo(tssrs.GetText(1,4));
        tLLCaseSchema.setOtherIDType("5");
        tLLCaseSchema.setOtherIDNo(aSecurityNo);
        tLLCaseSchema.setGrpNo(parseNode(nodeCase,QDSCParser.PATH_GrpNo));
        String aAccDate = FormatDate(parseNode(nodeCase,QDSCParser.PATH_ApplyDate));
        if(aAccDate==null || "".equals(aAccDate) ||"null".equals(aAccDate)){
            String ErrMessage = "团单"+mGrpContNo+"下" + "该客户出险日期不能为空，或出险日期格式不正确应为'YYYY-MM-DD'！";
             getFalseList(nodeCase, ErrMessage);
             return false;
         }
        tLLCaseSchema.setAccidentDate(aAccDate);
        if(tLLCaseSchema.getAccidentDate()==null)//by zzh 0930
        {
        	String ErrMessage = "团单"+mGrpContNo+"下" + "该客户出险日期格式不正确,应为'YYYY-MM-DD'！";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }
        System.out.println(tLLCaseSchema.getAccidentDate());
        tLLCaseSchema.setSurveyFlag("0");
        String tPayModeSQL = "SELECT casegetmode FROM llregister WHERE rgtno='"+mRgtNo+"'";
        ExeSQL tPMExSQL = new ExeSQL();
        SSRS tPMSSRS = tPMExSQL.execSQL(tPayModeSQL);
        if (tPMSSRS.getMaxRow() > 0) {
           try {
               tLLCaseSchema.setCaseGetMode(tPMSSRS.GetText(1, 1));
           } catch (Exception ex) {
               System.out.println("插入给付方式失败！");
           }
        }

        String sql =
                "select bankcode,bankaccno,accname from lcinsured where insuredno='" +
                aCustomerNo + "' and grpcontno='" + mGrpContNo + "'" +
                " union " +
                "select bankcode,bankaccno,accname from lbinsured where insuredno='" +
                aCustomerNo + "' and grpcontno='" + mGrpContNo + "'";

        ExeSQL texesql1 = new ExeSQL();
        SSRS trr = texesql1.execSQL(sql);
        if (trr.getMaxRow() > 0) {
            try {
                tLLCaseSchema.setBankCode(trr.GetText(1, 1));
                tLLCaseSchema.setBankAccNo(trr.GetText(1, 2));
                tLLCaseSchema.setAccName(trr.GetText(1, 3));
            } catch (Exception ex) {
                System.out.println("插入用户帐户信息失败！");
            }
        }
        
        String tAccSQL = "select 1 from lcgrppol a,lmriskapp b where a.riskcode=b.riskcode"
            + " and b.risktype3='7' and b.risktype <>'M'"
            + " and a.grpcontno='"+ mGrpContNo +"'";
        SSRS tAccSSRS = texesql1.execSQL(tAccSQL);
        boolean tDiseaseFlag = true;
        if (tAccSSRS.getMaxRow()>0) {
        	tDiseaseFlag = false;
        }
        
        String tHospitalCode = ""+parseNode(nodeCase,QDSCParser.PATH_HosSecNo);
        String tHospitalName = ""+parseNode(nodeCase,QDSCParser.PATH_HospitalName);
        
        if (("".equals(tHospitalCode)||"null".equals(tHospitalCode))&&tDiseaseFlag) {
            CError.buildErr(this, "医院编码不能为空！");
            getFalseList(nodeCase, "医院编码不能为空！");
            return false;
        }
        
        if (("".equals(tHospitalName)||"null".equals(tHospitalName))&&tDiseaseFlag) {
            CError.buildErr(this, "医院名称不能为空！");
            getFalseList(nodeCase, "医院名称不能为空！");
            return false;
        }
        
        tLLFeeMainSchema.setRgtNo(mRgtNo);
        tLLFeeMainSchema.setCaseNo("");
        tLLFeeMainSchema.setCustomerNo(aCustomerNo);
        tLLFeeMainSchema.setCustomerName(aCustomerName);
        tLLFeeMainSchema.setCustomerSex(aSex);
        tLLFeeMainSchema.setInsuredStat(tssrs.GetText(1, 5));
//        tLLFeeMainSchema.setHospitalCode(parseNode(nodeCase,QDSCParser.PATH_HospitalCode));
//       tLLFeeMainSchema.setRealHospDate(request.getParameter("RealHospDate"));
//        tLLFeeMainSchema.setInsuredStat(request.getParameter("InsuredStat"));
        tLLFeeMainSchema.setHospitalName(tHospitalName);
        tLLFeeMainSchema.setHospitalCode(tHospitalCode);
        tLLFeeMainSchema.setHosAtti(tHospitalCode);
        tLLFeeMainSchema.setReceiptNo("53000");
        tLLFeeMainSchema.setFeeAtti("4");
        tLLFeeMainSchema.setSecurityNo(aSecurityNo);
        tLLFeeMainSchema.setReceiptNo("94000");
        tLLFeeMainSchema.setFeeType(parseNode(nodeCase,QDSCParser.PATH_FeeType));
        tLLFeeMainSchema.setFeeAtti("4");
        tLLFeeMainSchema.setFeeDate(FormatDate(parseNode(nodeCase,QDSCParser.PATH_FeeDate)));
        tLLFeeMainSchema.setHospStartDate(FormatDate(parseNode(nodeCase,QDSCParser.PATH_HospStartDate)));
        tLLFeeMainSchema.setHospEndDate(FormatDate(parseNode(nodeCase,QDSCParser.PATH_HospEndDate)));
        tLLFeeMainSchema.setInHosNo(parseNode(nodeCase,QDSCParser.PATH_inpatientNo));
        tLLFeeMainSchema.setMedicalType(parseNode(nodeCase,QDSCParser.PATH_FeeType));
        tLLFeeMainSchema.setSecuPayType(parseNode(nodeCase,QDSCParser.PATH_BudgetType));
        tLLFeeMainSchema.setSecuPayDate(parseNode(nodeCase,QDSCParser.PATH_ApplyDate));
        tLLFeeMainSchema.setOriginFlag(parseNode(nodeCase,QDSCParser.PATH_HosFlag));

        tLLSecurityReceiptSchema.setApplyAmnt(parseNode(nodeCase,QDSCParser.PATH_SupInHosFee));
        tLLSecurityReceiptSchema.setSupInHosFee(parseNode(nodeCase,QDSCParser.PATH_SupInHosFee));
        tLLSecurityReceiptSchema.setHighAmnt2(parseNode(nodeCase,QDSCParser.PATH_SupInHosFee));
        tLLSecurityReceiptSchema.setFeeInSecu(parseNode(nodeCase,QDSCParser.PATH_FeeInSecu));
        tLLSecurityReceiptSchema.setPlanFee(parseNode(nodeCase,QDSCParser.PATH_PlanFee));
        tLLSecurityReceiptSchema.setSecuRefuseFee(parseNode(nodeCase,QDSCParser.PATH_SecuRefuseFee));

        String tDiseaseCode = ""+parseNode(nodeCase,QDSCParser.PATH_MICDCode);
//        String tDiseaseName = ""+parseNode(nodeCase,QDSCParser.PATH_MICDCode);
        if (("".equals(tDiseaseCode)||"null".equals(tDiseaseCode))&&tDiseaseFlag) {
            CError.buildErr(this, "疾病编码不能为空！");
            getFalseList(nodeCase, "疾病编码不能为空！");
            return false;
        }

        if (!"".equals(tDiseaseCode)) {
            LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
            tLLCaseCureSchema.setDiseaseCode(tDiseaseCode);
        //        tLLCaseCureSchema.setDiseaseName(parseNode(nodeCase,QDSCParser.PATH_MICDCode));
            tLLCaseCureSchema.setMainDiseaseFlag("1");
            tLLCaseCureSet.add(tLLCaseCureSchema);
        }
        
        
        String tDiseaseCode2 = ""
				+ parseNode(nodeCase, QDSCParser.PATH_SICDCode);
		if (!tDiseaseCode.equals(tDiseaseCode2)) {
			if (("".equals(tDiseaseCode2) || "null".equals(tDiseaseCode2))
					&& tDiseaseFlag) {
				CError.buildErr(this, "疾病编码不能为空！");
				getFalseList(nodeCase, "疾病编码不能为空！");
				return false;
			}

			if (!"".equals(tDiseaseCode2)) {
				LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
				tLLCaseCureSchema.setDiseaseCode(tDiseaseCode2);
				// tLLCaseCureSchema.setDiseaseName(parseNode(nodeCase,QDSCParser.PATH_SICDCode));
				tLLCaseCureSchema.setMainDiseaseFlag("0");
				tLLCaseCureSet.add(tLLCaseCureSchema);
			}
		}
        // 保存案件时效
        tLLCaseOpTimeSchema.setRgtState("01");
        tLLCaseOpTimeSchema.setStartDate(PubFun.getCurrentDate());
        tLLCaseOpTimeSchema.setStartTime(PubFun.getCurrentTime());

        System.out.println("<--Star Submit VData-->");
        tVData.add(tLLAppClaimReasonSchema);
        tVData.add(tLLCaseSchema);
        tVData.add(tLLRegisterSchema);
        tVData.add(tLLFeeMainSchema);
        tVData.add(tLLSecurityReceiptSchema);
        tVData.add(tLLCaseCureSet);
        tVData.add(tLLCaseOpTimeSchema);

        tVData.add(mG);
        System.out.println("<--Into tSimpleClaimUI-->");
        if (!tgenCaseInfoBL.submitData(tVData, "INSERT||MAIN")){
            CErrors tError = tgenCaseInfoBL.mErrors;
            String ErrMessage = tError.getFirstError();
            getFalseList(nodeCase,ErrMessage);
            return false;
        }
        return true;
    }

    private boolean getFalseList(Node nodeCase,String ErrMessage){
        String[] strCol = new String[24];
        strCol[0] = parseNode(nodeCase,QDSCParser.PATH_SecurityNo);
        strCol[1] = parseNode(nodeCase,QDSCParser.PATH_CustomerName);
        strCol[2] = parseNode(nodeCase,QDSCParser.PATH_Sex);
        strCol[3] = FormatDate(parseNode(nodeCase,QDSCParser.PATH_Birthday));
        strCol[4] = parseNode(nodeCase,QDSCParser.PATH_IDNo);
        strCol[5] = parseNode(nodeCase,QDSCParser.PATH_GrpNo);
        strCol[6] = parseNode(nodeCase,QDSCParser.PATH_HosSecNo);
        strCol[7] = parseNode(nodeCase,QDSCParser.PATH_HospitalName);
        strCol[8] = parseNode(nodeCase,QDSCParser.PATH_inpatientNo);
        strCol[9] = FormatDate(parseNode(nodeCase,QDSCParser.PATH_HospStartDate));
        strCol[10] = FormatDate(parseNode(nodeCase,QDSCParser.PATH_HospEndDate));
        strCol[11] = parseNode(nodeCase,QDSCParser.PATH_MICDCode);
        strCol[12] = parseNode(nodeCase,QDSCParser.PATH_SICDCode);
        strCol[13] = parseNode(nodeCase,QDSCParser.PATH_FeeType);
        strCol[14] = parseNode(nodeCase,QDSCParser.PATH_BudgetType);
        strCol[15] = FormatDate(parseNode(nodeCase,QDSCParser.PATH_FeeDate));
        strCol[16] = FormatDate(parseNode(nodeCase,QDSCParser.PATH_ApplyDate));
        strCol[17] = parseNode(nodeCase,QDSCParser.PATH_ApplyAmnt);
        strCol[18] = parseNode(nodeCase,QDSCParser.PATH_FeeInSecu);
        strCol[19] = parseNode(nodeCase,QDSCParser.PATH_PlanFee);
        strCol[20] = parseNode(nodeCase,QDSCParser.PATH_SupInHosFee);
        strCol[21] = parseNode(nodeCase,QDSCParser.PATH_SecuRefuseFee);
        strCol[22] = parseNode(nodeCase,QDSCParser.PATH_HosFlag);
        strCol[23] = ErrMessage;
        System.out.println("ErrMessage:"+ErrMessage);
        FalseListTable.add(strCol);
        return true;
    }

    private String FormatDate(String aDate){
        String rDate = "";
        rDate = aDate;
        if(aDate.length()<8){
            int days = 0;
            try{
                days = Integer.parseInt(aDate)-2;
            }catch(Exception ex){
                return "";
            }
            rDate = PubFun.calDate("1900-1-1",days,"D",null);
        }
        return rDate;
    }
}
