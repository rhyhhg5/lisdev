package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LPUWMasterDB;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LBMissionSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LPUWMasterSchema;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.vschema.LPUWMasterSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
//import com.sinosoft.lis.bq.*;

/**
 * <p>Title: 保全人工核保</p>
 * <p>Description:个人保全人工核保核保完毕确认</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class LLSecondLPUWManuBL
{
    /** 错误处理类*/
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private TransferData mTransferData = null;

    private String mEdorNo = null;
    
    private String mEdorType = null;
    
    private String mContNo = null;
    
    private String mPolNo = null;
    
    private String mInsuredNo = null;
    
    private String mfmtransact = null;

    private String mMissionId = null;

    private String mSubMissionId = null;

    private String mActivityId = null;

    private String mPassFlag = null;
    
    private String mUWIdea = null;

    private GlobalInput mGlobalInput = null;

    private LPUWMasterSet mLPUWMasterSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();
    
    private LPEdorItemSet mLPEdorItemSet = null;

    public LLSecondLPUWManuBL(GlobalInput gi, TransferData td)
    {
        this.mGlobalInput = gi;
        this.mTransferData = td;
    }

    /**mTransferData
     * 数据提交的公共方法
     * @param gi GlobalInput
     * @param edorAcceptNo String
     * @return boolean
     */
    public boolean submitData()
    {
        if (!getInputData())
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        
        if(!submit()){
        	return false;
        }
        return true;
    }

    private boolean getInputData()
    {
        this.mEdorNo = (String) mTransferData.getValueByName("EdorNo");
        this.mMissionId = (String) mTransferData.getValueByName("MissionId");
        this.mSubMissionId = (String) mTransferData.getValueByName("SubMissionId");
        this.mActivityId = (String) mTransferData.getValueByName("ActivityId");
        this.mEdorType = (String) mTransferData.getValueByName("EdorType");
        this.mContNo = (String) mTransferData.getValueByName("ContNo");
        this.mPolNo = (String) mTransferData.getValueByName("PolNo");
        this.mPassFlag = (String) mTransferData.getValueByName("PassFlag");
        this.mUWIdea = (String) mTransferData.getValueByName("UWIdea");
        this.mInsuredNo = (String) mTransferData.getValueByName("InsuredNo");
        this.mfmtransact = (String) mTransferData.getValueByName("fmtransact");
        LPUWMasterDB tLPUWMasterDB = new LPUWMasterDB();
        tLPUWMasterDB.setEdorNo(mEdorNo);
        this.mLPUWMasterSet = tLPUWMasterDB.query();
        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {

    	//保全结论校验
    	if(mfmtransact!=null&&mfmtransact.toUpperCase().equals("SAVEUW")){
    		LCPolDB tLCPolDB = new LCPolDB();
    		tLCPolDB.setContNo(mContNo);
    		tLCPolDB.setPolNo(mPolNo);
    		if(tLCPolDB.query().size()<=0){
                mErrors.addOneError( "查询险种信息失败！");
                return false;
    		}
    		if(mPassFlag==null||"".equals(mPassFlag)){
                mErrors.addOneError( "核保结论不能为空");
                return false;    			
    		}
    		if(!"1".equals(mPassFlag)){
    			if(mUWIdea==null||"".equals(mUWIdea)){
                    mErrors.addOneError( "核保意见不能为空");
                    return false;      				
    			}
    		}
    	}else if(mfmtransact!=null&&mfmtransact.toUpperCase().equals("FINISHUW")){
    		if(mInsuredNo==null||"".equals(mInsuredNo)){
                mErrors.addOneError( "没有指定核保被保险人");
                return false;      			
    		}
    		//校验被保险人下的所有保单的所有险种均核保完毕
    		String checkSQL = "select polno from lcpol where insuredno='"+mInsuredNo+"' " +
    				" and not exists (select 1 from LPUWMaster where grpcontno='00000000000000000000' and polno=lcpol.polno and edorno='"+mEdorNo+"' ) " +
    				" and lcpol.conttype='1' and appflag='1' fetch first 1 rows only ";
    		ExeSQL mExeSQL = new ExeSQL();
    		if(mExeSQL.getOneValue(checkSQL)!=null&&!"".equals(mExeSQL.getOneValue(checkSQL))){
                mErrors.addOneError( "被保险人"+mInsuredNo+"有未核保的险种，无法二核完毕");
                return false;    			
    		}
    	}else{
    		 mErrors.addOneError( "操作错误");
    		 return false;
    	}
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
    	//保存核保结论
    	if(mfmtransact!=null&&mfmtransact.toUpperCase().equals("SAVEUW")){
    		//查询保单信息
    		LCContDB tLCContDB = new LCContDB();
    		LCContSchema tLCContSchema = new LCContSchema();
    		tLCContDB.setContNo(mContNo);
    		 if(!tLCContDB.getInfo()){
                 mErrors.addOneError( "查询保单号"+mContNo+"的保单信息失败");
                 return false;        			 
    		 }
    		 tLCContSchema =tLCContDB.getSchema();
    		 //查询险种信息
    		 LCPolDB tLCPolDB = new LCPolDB();
    		 LCPolSchema tLCPolSchema = new LCPolSchema();
    		 tLCPolDB.setContNo(mContNo);
    		 tLCPolDB.setPolNo(mPolNo);
    		 if(!tLCPolDB.getInfo()){
    			 mErrors.addOneError( "查询险种号"+mPolNo+"的险种信息失败");
                 return false;     
    		 }
    		 tLCPolSchema = tLCPolDB.getSchema();
    		 
    		LPUWMasterSchema tLPUWMasterSchema = new LPUWMasterSchema();
    		tLPUWMasterSchema.setEdorNo(mEdorNo);
    		tLPUWMasterSchema.setEdorType("SP");
    		tLPUWMasterSchema.setGrpContNo(tLCContSchema.getGrpContNo());
    		tLPUWMasterSchema.setContNo(tLCContSchema.getContNo());
    		tLPUWMasterSchema.setProposalContNo(tLCContSchema.getProposalContNo());
    		tLPUWMasterSchema.setPolNo(tLCPolSchema.getPolNo());
    		tLPUWMasterSchema.setProposalNo(tLCPolSchema.getProposalNo());
    		tLPUWMasterSchema.setUWNo(createUWNo());
    		tLPUWMasterSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
    		tLPUWMasterSchema.setInsuredName(tLCPolSchema.getInsuredName());
    		tLPUWMasterSchema.setAppntNo(tLCContSchema.getAppntNo());
    		tLPUWMasterSchema.setAppntName(tLCContSchema.getAppntName());
    		tLPUWMasterSchema.setPassFlag(mPassFlag);
    		tLPUWMasterSchema.setAutoUWFlag("2"); //人工核保
    		tLPUWMasterSchema.setUWIdea(mUWIdea);
    		String aSugUWIdea = "";
    		if("1".equals(mPassFlag)){
    			aSugUWIdea="标准承保,";
    		}else if("2".equals(mPassFlag)){
    			aSugUWIdea="变更承保,";
    		}else{
    			aSugUWIdea="拒保,";
    		}
    		aSugUWIdea = aSugUWIdea+mUWIdea;
    		tLPUWMasterSchema.setSugUWIdea(aSugUWIdea);
    		tLPUWMasterSchema.setOperator(mGlobalInput.Operator);
    		tLPUWMasterSchema.setMakeDate(mCurrentDate);
    		tLPUWMasterSchema.setMakeTime(mCurrentTime);
    		tLPUWMasterSchema.setModifyDate(mCurrentDate);
    		tLPUWMasterSchema.setModifyTime(mCurrentTime);
    		mMap.put(tLPUWMasterSchema, SysConst.DELETE_AND_INSERT);
    	}else if(mfmtransact!=null&&mfmtransact.toUpperCase().equals("FINISHUW")){
    		bakWorkflow();

    		
    	}
    	return true;
    }

    /**
     * 生成核保轨迹号
     * @return String
     */
    private String createUWNo()
    {
        String sql = "select UWNo + 1 from LPUWMaster " +
                "where EdorNo = '" + mEdorNo  + "' " +
                "and EdorType = '" + mEdorType + "' " +
                "and PolNo = '" + mPolNo + "'";
        String uwNo = (new ExeSQL()).getOneValue(sql);
        if (uwNo.equals(""))
        {
            return "1";
        }
        return uwNo;
    }

    /**
     * 把工作流表里的数据放入B表
     * @return boolean
     */
    private boolean bakWorkflow()
    {
        LWMissionDB tLWMissionDB = new LWMissionDB();
        tLWMissionDB.setMissionID(mMissionId);
        tLWMissionDB.setSubMissionID(mSubMissionId);
        tLWMissionDB.setActivityID(mActivityId);
        if (!tLWMissionDB.getInfo())
        {
            mErrors.addOneError("未找到人工核保工作流信息！");
            return false;
        }
        LBMissionSchema tLBMissionSchema = new LBMissionSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLBMissionSchema, tLWMissionDB.getSchema());
        String serielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
        tLBMissionSchema.setSerialNo(serielNo);
        tLBMissionSchema.setModifyDate(mCurrentDate);
        tLBMissionSchema.setModifyTime(mCurrentTime);
        
        //调用理赔接口处理数据
		LLUnderWritBL tLLUnderWritBL = new LLUnderWritBL();
		tLLUnderWritBL.changeCaseState(tLBMissionSchema.getMissionProp1());
        
        mMap.put(tLBMissionSchema, "DELETE&INSERT");
        mMap.put(tLWMissionDB.getSchema(), "DELETE");
        
        
        return true;
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
