package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;

import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 理赔-审批审定业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author：Xx
 * @CreateDate：2005-02-25
 */
public class ClaimUnderwriteBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 往后面传输的数据库操作 */
    private MMap map = new MMap();

    /** 执行成功时，页面反馈信息 */
    private String mReturnMessage = "";
    private String mClmNo = ""; /** 赔案号 */
    private String mRgtNo = ""; /** 立案号 */
    private String mCaseNo = ""; /** 立案分案号 */
    private String mRemarkSD = ""; /** 审定意见 */
    private String mClaimGiveType = ""; /** 赔付结论 */
    private String mUpUserCode = ""; /** 上级代码 */
    private String mClaimPopedom = ""; /** 理赔权限 */
    private String mUpClaimPopedom = ""; /** 上级理赔权限 */
    private String mUpStateFlag = ""; /** 上级理赔状态 */
    private String aDate = "";
    private String aTime = "";
    private String tSumOperator="";/**案件理赔员人数*/
    private String tHandler="";
    private String boolblack="false"; /** 是否黑名单 */
    private String boolupuser="";/**  用户是否最高权限 */
    
    /** 社保保单标志 0：非社保；1：社保*/
    private String mSocialSecurity = "0";
    /** 社保案件实赔（用于抽检）*/
    private double mSocialSecurityRealPay = 0.0;
    /** 社保案件抽检金额（由于抽检）*/
    private double mSocialSecuritySpotLimit = 0.0;
    /** 商保案件实赔（用于抽检）*/
    private double mBusinessRealPay = 0.0;
    
    /**用户登陆信息 */
    private GlobalInput mG = new GlobalInput();
    /**立案分案信息 */
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    /**案件核赔信息 */
    private LLClaimUWMainSchema mLLClaimUWMainSchema = new LLClaimUWMainSchema();
    /**案件时效表*/
    private LLCaseOpTimeSet mLLCaseOpTimeSet = new LLCaseOpTimeSet();

    public ClaimUnderwriteBL() {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * @param: cOperate 数据操作:  APPROVE|SP"-审批  APPROVE|SD"-审定
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mOperate = cOperate;
        if (cOperate.equals("BATCH||UW")) {
            if (!getBatchUWData(cInputData)) {
                return false;
            }
        } else {
            //得到外部传入的数据,将数据备份到本类中
            if (!getInputData(cInputData)) {
                return false;
            }
            //根据赔案号、保单号查询保单、赔案、核赔、核赔错误、用户权限信息
            if (!getBaseData()) {
                return false;
            }
        }
        
        if (!checkData()) {
        	return false;
        }
        
        //数据操作业务处理
        if (!dealData()) {
            return false;
        }
        //准备给后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        //数据提交
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            CError.buildErr(this, "数据提交失败！");
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean getInputData(VData cInputData) {
        mG = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mLLClaimUWMainSchema = (LLClaimUWMainSchema) cInputData.
                               getObjectByObjectName("LLClaimUWMainSchema", 0);

        mRgtNo = mLLClaimUWMainSchema.getRgtNo(); //立案号
        mCaseNo = mLLClaimUWMainSchema.getCaseNo(); //分案号
        mClmNo = mLLClaimUWMainSchema.getClmNo(); //赔案号
        mRemarkSD = mLLClaimUWMainSchema.getRemark2(); //审定意见
        System.out.println("after getInputData");
        //查询赔案信息LLClaim
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setClmNo(mClmNo);
        LLClaimSet tLLClaimSet = tLLClaimDB.query();
        if (tLLClaimSet.size() <= 0) {
            CError.buildErr(this, "赔案信息查询失败，该案件尚未理算！");
            return false;
        }
        mBusinessRealPay = tLLClaimSet.get(1).getRealPay();
        return true;
    }

    private boolean getBatchUWData(VData cInputData) {
        mG = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        //获得上层传入的社保保单标记
        TransferData tTransferData = new TransferData();
        tTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mSocialSecurity = (String)tTransferData.getValueByName("mSocialSecurity");
        if("1".equals(mSocialSecurity)){//在数据准备阶段，判断走社保用户，还是理赔用户
        	LLSocialClaimUserSchema tLLSocialClaimUserSchema = getLLSocialClaimUserSchema(mG.Operator);
        	mClaimPopedom = tLLSocialClaimUserSchema.getClaimPopedom();
        	mUpUserCode = tLLSocialClaimUserSchema.getUpUserCode();
        	mUpClaimPopedom = tLLSocialClaimUserSchema.getClaimPopedom(); //社保用户对该字段不存储       	
        	mSocialSecuritySpotLimit = tLLSocialClaimUserSchema.getPrepaidLimit();
        	
        	//查询社保用户上级信息
        	LLSocialClaimUserDB tLLSocialClaimUserDB = new LLSocialClaimUserDB();
        	tLLSocialClaimUserDB.setUserCode(mUpUserCode);
            if (tLLSocialClaimUserDB.getInfo()) {
            	mUpStateFlag = tLLSocialClaimUserDB.getStateFlag();
            }
        }else{
            LLClaimUserSchema clmuser = (LLClaimUserSchema) cInputData.
            	getObjectByObjectName("LLClaimuserSchema",0);
            mClaimPopedom = clmuser.getClaimPopedom();
            mUpUserCode = clmuser.getUpUserCode();
            mUpClaimPopedom = clmuser.getComCode(); //暂存在这个字段中
            
            LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
            tLLClaimUserDB.setUserCode(mUpUserCode);
            if (tLLClaimUserDB.getInfo()) {
            	mUpStateFlag = tLLClaimUserDB.getStateFlag();
            }
        }

        mLLCaseSchema = (LLCaseSchema) cInputData.getObjectByObjectName(
                "LLCaseSchema", 0);
        mRgtNo = mLLCaseSchema.getRgtNo(); //立案号
        mCaseNo = mLLCaseSchema.getCaseNo(); //分案号 
        
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mCaseNo);
        if (!tLLCaseDB.getInfo()) {
        	CError.buildErr(this, "案件信息查询失败");
            return false;
        }
        
        String tRgtState = tLLCaseDB.getRgtState();
		if (!"04".equals(tRgtState) && !"03".equals(tRgtState)
				&& !"05".equals(tRgtState) && !"06".equals(tRgtState)
				&& !"10".equals(tRgtState) && !"08".equals(tRgtState)) {
			CError.buildErr(this, "案件当前状态不能进行审批审定");
			return false;
		}
//		如果案件状态为‘08’，需要先判断原始调查案件的提调状态,若提调前案件状态为‘03’、‘04’、‘05’、‘06’、‘10’，
        //然后将案件状态置为提调前状态,需要排除多次在”查讫状态“提调的问题
        if("08".equals(tRgtState)){
    		String tSql = "select * from llsurvey a where a.otherno='"+mCaseNo
    		+"' and a.surveyflag='3' and a.rgtstate <> '08' order by surveyno desc fetch first 1 rows only with ur";
    	
	    	LLSurveyDB tLLSurveyDB = new LLSurveyDB();
	        LLSurveySet tLLSurveySet = new LLSurveySet();
	        tLLSurveySet = tLLSurveyDB.executeQuery(tSql);
	        System.out.println("tSql:"+tSql);
	        if (tLLSurveySet.size() <= 0) {
	        	
	        	CError.buildErr(this, "调查信息查询失败");
	            return false;
	        }
	        LLSurveySchema tLLSurveySchema = new LLSurveySchema();
	        tLLSurveySchema = tLLSurveySet.get(1);
	        tRgtState = "" + tLLSurveySchema.getRgtState();
	        String tRgtName = "";
	        String tBakErr = "";
	        LDCodeDB tLDCodeDB = new LDCodeDB();
	        tLDCodeDB.setCodeType("llrgtstate");
	        tLDCodeDB.setCode(tRgtState);
	        if (tLDCodeDB.getInfo()) {
	        	tRgtName = tLDCodeDB.getCodeName();
	        }
	        System.out.println("tRgtState，tRgtName:" + tRgtState+tRgtName);
	        
	        //案件提调前状态对应返回的错误信息
	        if("01".equals(tRgtState) || "0".equals(tRgtState)//历史调查，0为受理状态    	 
	        		|| "".equals(tRgtState)){//核心目前只有3条数据，此分支只是为了体现数据情况，以后系统不会出现该类数据
	        	tRgtState = "01";
	        	tBakErr = "案件提调状态为‘受理状态’，请您去‘账单录入’或‘检录’页面进行操作";	        	
	        }else if("02".equals(tRgtState)){
	        	tBakErr = "案件提调状态为‘扫描状态’，请您去‘账单录入’或‘检录’页面进行操作";
	        }else if("03".equals(tRgtState) || "1".equals(tRgtState)){//历史调查，1为检录状态，咨询、通知类案件均为检录状态
	        	tRgtState = "04";
	        	tBakErr = "";
	        }

	        if("04".equals(tRgtState) 
					|| "05".equals(tRgtState) || "06".equals(tRgtState)
					|| "10".equals(tRgtState)){
	        	mLLCaseSchema.setRgtState(tRgtState);
	        }else if("03".equals(tRgtState)){
	        	CError.buildErr(this, tBakErr);
	            return false;	        	
	        }else{
	        	CError.buildErr(this, tBakErr);
	            return false;
	        }
	       
        }

        
        
        //查询赔案信息LLClaim
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setCaseNo(mCaseNo);
        LLClaimSet tLLClaimSet = tLLClaimDB.query();
        if (tLLClaimSet.size() <= 0) {
            CError.buildErr(this, "赔案信息查询失败，该案件尚未理算！");
            return false;
        }
        mClaimGiveType = tLLClaimSet.get(1).getGiveType();
        if (mClaimGiveType == null) {
            CError.buildErr(this, "案件无赔付结论,请先做理算确认!");
            return false;
        }
        mClmNo = tLLClaimSet.get(1).getClmNo(); //赔案号
        mBusinessRealPay = tLLClaimSet.get(1).getRealPay();
        aDate = PubFun.getCurrentDate();
        aTime = PubFun.getCurrentTime();
        LLClaimUWMainDB mLLClaimUWMainDB = new LLClaimUWMainDB();
        mLLClaimUWMainDB.setClmNo(mClmNo);
        if (mLLClaimUWMainDB.getInfo()) {
            mLLClaimUWMainDB.setcheckDecision1("1");
            mLLClaimUWMainDB.setRemark1("批量审批通过");
            mLLClaimUWMainDB.setcheckDecision2("1");
            mLLClaimUWMainDB.setRemark2("批量审定通过");
            mLLClaimUWMainSchema.setSchema(mLLClaimUWMainDB.getSchema());
            mLLClaimUWMainSchema.setCaseNo(mCaseNo);
            mLLClaimUWMainSchema.setRgtNo(mRgtNo);

        } else {
            mLLClaimUWMainSchema.setcheckDecision1("1");
            mLLClaimUWMainSchema.setRemark1("批量审批通过");
            mLLClaimUWMainSchema.setcheckDecision2("1");
            mLLClaimUWMainSchema.setRemark2("批量审定通过");
            mLLClaimUWMainSchema.setMakeDate(aDate);
            mLLClaimUWMainSchema.setMakeTime(aTime);
            mLLClaimUWMainSchema.setClmNo(mClmNo);
            mLLClaimUWMainSchema.setCaseNo(mCaseNo);
            mLLClaimUWMainSchema.setRgtNo(mRgtNo);
        }
        mLLClaimUWMainSchema.setOperator(mG.Operator);
        mLLClaimUWMainSchema.setMngCom(mG.ManageCom);
        mLLClaimUWMainSchema.setModifyDate(aDate);
        mLLClaimUWMainSchema.setModifyTime(aTime);

        return true;
    }

    private boolean getBaseData() {
        aDate = PubFun.getCurrentDate();
        aTime = PubFun.getCurrentTime();
        //查询立案分案信息LLCase
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mCaseNo);
        if (!tLLCaseDB.getInfo()) {
            CError.buildErr(this, "分案信息查询失败,分案号：" + mCaseNo);
            return false;
        }
        mLLCaseSchema.setSchema(tLLCaseDB.getSchema());
        //如果案件状态为‘08’，需要先判断原始调查案件的提调状态,若提调前案件状态为‘04’、‘05’、‘06’、‘10’，
        //然后将案件状态置为提调前状态,需要排除多次在”查讫状态“提调的问题
        if("08".equals(mLLCaseSchema.getRgtState())){
    		String tSql = "select * from llsurvey a where a.otherno='"+mCaseNo
    		+"' and a.surveyflag='3' and a.rgtstate <> '08' order by surveyno desc fetch first 1 rows only with ur";
    	
	    	LLSurveyDB tLLSurveyDB = new LLSurveyDB();
	        LLSurveySet tLLSurveySet = new LLSurveySet();
	        tLLSurveySet = tLLSurveyDB.executeQuery(tSql);
	        System.out.println("tSql:"+tSql);
	        if (tLLSurveySet.size() <= 0) {
	        	
	        	CError.buildErr(this, "调查信息查询失败");
	            return false;
	        }
	        LLSurveySchema tLLSurveySchema = new LLSurveySchema();
	        tLLSurveySchema = tLLSurveySet.get(1);
	        String tRgtState = "" + tLLSurveySchema.getRgtState();
	        String tRgtName = "";
	        String tBakErr = "";
	        LDCodeDB tLDCodeDB = new LDCodeDB();
	        tLDCodeDB.setCodeType("llrgtstate");
	        tLDCodeDB.setCode(tRgtState);
	        if (tLDCodeDB.getInfo()) {
	        	tRgtName = tLDCodeDB.getCodeName();
	        }

	        System.out.println("tRgtState，tRgtName:" + tRgtState+tRgtName);
	        
	        //案件提调前状态对应返回的错误信息
	        if("01".equals(tRgtState) || "0".equals(tRgtState)//历史调查，0为受理状态    	 
	        		|| "".equals(tRgtState)){//核心目前只有3条数据，此分支只是为了体现数据情况，以后系统不会出现该类数据
	        	tRgtState = "01";
	        	tBakErr = "案件提调状态为‘受理状态’，请您去‘账单录入’或‘检录’页面进行操作";	        	
	        }else if("02".equals(tRgtState)){
	        	tBakErr = "案件提调状态为‘扫描状态’，请您去‘账单录入’或‘检录’页面进行操作";
	        }else if("03".equals(tRgtState) || "1".equals(tRgtState)){//历史调查，1为检录状态，咨询、通知类案件均为检录状态
	        	tRgtState = "03";
	        	tBakErr = "案件提调状态为‘检录状态’，请您去‘理算’或‘团体审定’页面进行操作";
	        }

	        if("04".equals(tRgtState) 
					|| "05".equals(tRgtState) || "06".equals(tRgtState)
					|| "10".equals(tRgtState)){
	        	 mLLCaseSchema.setRgtState(tRgtState);
	        }else{
	        	CError.buildErr(this, tBakErr);
	            return false;
	        }
	       
        }

        if (!LLCaseCommon.checkHospCaseState(mCaseNo)) {
            CError.buildErr(this, "医保通案件待确认，不能进行处理");
            return false;
        }

        //查询赔案信息LLClaim
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setClmNo(mClmNo);
        if (!tLLClaimDB.getInfo()) {
            CError.buildErr(this, "赔案信息查询失败，该案件尚未理算！");
            return false;
        }
        mClaimGiveType = tLLClaimDB.getGiveType();
        if (mClaimGiveType == null) {
            CError.buildErr(this, "案件无赔付结论,请先做理算确认!");
            return false;
        }

        //查询该案件既往核赔信息（案件有回退、上报等操作）
        LLClaimUWMainDB mLLClaimUWMainDB = new LLClaimUWMainDB();
        mLLClaimUWMainDB.setClmNo(mClmNo);
        if (mLLClaimUWMainDB.getInfo()) {
            mLLClaimUWMainDB.setcheckDecision1(mLLClaimUWMainSchema.
                                               getcheckDecision1());
            mLLClaimUWMainDB.setRemark1(mLLClaimUWMainSchema.getRemark1());
            mLLClaimUWMainDB.setcheckDecision2(mLLClaimUWMainSchema.
                                               getcheckDecision2());
            mLLClaimUWMainDB.setRemark2(mLLClaimUWMainSchema.getRemark2());
            mLLClaimUWMainSchema.setSchema(mLLClaimUWMainDB.getSchema());
        } else {
            mLLClaimUWMainSchema.setMakeDate(aDate);
            mLLClaimUWMainSchema.setMakeTime(aTime);
        }
        mLLClaimUWMainSchema.setRgtNo(mLLCaseSchema.getRgtNo());
        mLLClaimUWMainSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        mLLClaimUWMainSchema.setOperator(mG.Operator);
        mLLClaimUWMainSchema.setMngCom(mG.ManageCom);
        mLLClaimUWMainSchema.setModifyDate(aDate);
        mLLClaimUWMainSchema.setModifyTime(aTime);

        //add by Houyd 社保案件可以支持个案的审批，此处为个案传入社保标记,同时需要考虑申诉类案件
        String tCaseNo = "";
        if("R".equals(mCaseNo.substring(0,1)) || "S".equals(mCaseNo.substring(0,1)))
    	{
    		System.out.println("申诉纠错案件换为正常的C案件");
            String sql = "select caseno from LLAppeal where appealno ='"+mCaseNo+"'";
            ExeSQL exeSQL = new ExeSQL();
            tCaseNo = exeSQL.getOneValue(sql);
    	}else{
    		tCaseNo = mCaseNo;
    	}
        mSocialSecurity = LLCaseCommon.checkSocialSecurity(tCaseNo, null);
        if("1".equals(mSocialSecurity)){
        	LLSocialClaimUserDB aLLSocialClaimUserDB = new LLSocialClaimUserDB();
        	aLLSocialClaimUserDB.setUserCode(mG.Operator);
        	aLLSocialClaimUserDB.setStateFlag("1");//社保用户有效
        	aLLSocialClaimUserDB.setHandleFlag("1");//参与案件分配
			LLSocialClaimUserSet tSocialClaimUserSet = aLLSocialClaimUserDB.query();
        	if(tSocialClaimUserSet.size() <= 0){
        		CError.buildErr(this,"您不是社保参与案件分配的审核人或已失效，无权作审定操作");
                return false;
        	}
        	
        	LLSocialClaimUserSchema tLLSocialClaimUserSchema = getLLSocialClaimUserSchema(mG.Operator);
        	mClaimPopedom = tLLSocialClaimUserSchema.getClaimPopedom();
        	mUpUserCode = tLLSocialClaimUserSchema.getUpUserCode();
        	mUpClaimPopedom = tLLSocialClaimUserSchema.getClaimPopedom(); //社保用户对该字段不存储       	
        	mSocialSecuritySpotLimit = tLLSocialClaimUserSchema.getPrepaidLimit();
        	
        	//查询社保用户上级信息
        	LLSocialClaimUserDB tLLSocialClaimUserDB = new LLSocialClaimUserDB();
        	tLLSocialClaimUserDB.setUserCode(mUpUserCode);
            if (tLLSocialClaimUserDB.getInfo()) {
            	mUpStateFlag = tLLSocialClaimUserDB.getStateFlag();
            }
        }else{
        	 //查询用户核赔权限及上级信息
    		String tsql = "select a.usercode,a.claimpopedom,a.upusercode,"
    				+ "(select b.claimpopedom from llclaimuser b where b.usercode=a.upusercode),"
    				+ "(select stateflag from llclaimuser where usercode=a.upusercode)"
    				+ " from llclaimuser a where a.usercode= '" + mG.Operator + "'";
            ExeSQL texesql = new ExeSQL();
            SSRS ssrs = texesql.execSQL(tsql);
            if (ssrs.getMaxRow() <= 0) {
                CError.buildErr(this, "用户核赔权限查询失败!用户代码：" + mG.Operator);
                return false;
            }
            mUpUserCode = ssrs.GetText(1, 3).equals("null") ? "" :
                          ssrs.GetText(1, 3);
            mClaimPopedom = ssrs.GetText(1, 2).equals("null") ? "" :
                            ssrs.GetText(1, 2);
    		
            if (mClaimPopedom.equals("")) {
                CError.buildErr(this, "用户无核赔权限!");
                return false;
            }

            mUpClaimPopedom = ssrs.GetText(1, 4).equals("null") ? "" :
                              ssrs.GetText(1, 4);
            mUpStateFlag = ssrs.GetText(1, 5).equals("null") ? "" : ssrs.GetText(1,
    				5);
        }     
        System.out.println("after getBaseData");
        return true;
    }

    private boolean dealData() {
    	if(!"1".equals(mSocialSecurity)){ //非社保
      	boolblack=checkBlack(); //
      	boolupuser=checkUpuser();
    	}
        String tRgtState = mLLCaseSchema.getRgtState();
        /*案件处于[理算状态]，案件处理人为案件作审批结论，
         处理人的权限足够则可结案，不够则案件转上级签批*/
        if (tRgtState.equals("04")) {
            if (!SPUW()) {
                return false;
            }
        }
        /*案件处于[审批状态]，等待上级做审定操作（签批），如上级权限不够则上报*/
        if (tRgtState.equals("05")) {
            if (!SDUW()) {
                return false;
            }
        }
        /*案件处于[审定状态]，即上级已签过该案件，等待原处理人结案·
         * #2369 cbs00072047-C3300141231000034案件轨迹问题，没有结案权
         * */
        if (tRgtState.equals("06")) {
            if (!backUW()) {
                return false;
            }
        }
        /**抽检/稽查后上级审定*/
        if (tRgtState.equals("10") || tRgtState.equals("15")) {
            if (!SpotUW(tRgtState)) {
                return false;
            }
        }

        if (mLLCaseSchema.getRgtState().equals("09")) {
        	
        	String tCjSQL = "select comcode from llclaimuser where usercode='" + mG.Operator + "'" +
        			" union all select comcode from LLSocialClaimUser where usercode='" + mG.Operator + "' "
     		+" fetch first 1 rows only";
     		
             ExeSQL tCjExeSQL = new ExeSQL();
             String tCJ = tCjExeSQL.getOneValue(tCjSQL); 
             if("".equals(tCJ)||tCJ == null){
            	 CError.buildErr(this, "用户机构查询失败");
                 return false;
             }
		     String tTJSQL = "SELECT 1 FROM LLHospCase WHERE caseno='" +mCaseNo + 
		     		"' AND hospitcode ='TJ05' and casetype='02' fetch first 10 rows only";
		     ExeSQL tTJExeSQL = new ExeSQL();
				System.out.println("tTJSQL=====" + tTJSQL);
				String tTJHOSP = tTJExeSQL.getOneValue(tTJSQL);
				
//             #2575
			//#3663
			// #3663 由一人操作，必须抽检上报上级(不属于外部接口案件) Star
			String tSumOperatorSql = "select count(distinct operator) from llcaseoptime where caseno='"+mCaseNo+"' and rgtstate!='02' and not exists(select 1 from llhospcase where caseno=llcaseoptime.caseno ) with ur";
			tSumOperator = tCjExeSQL.getOneValue(tSumOperatorSql);
			tHandler = mLLCaseSchema.getRigister();
			// #3663 由一人操作，必须抽检上报上级(不属于外部接口案件)  End
			//#3663	
             if(mBusinessRealPay <= 1000 && !"86".equals(tCJ) && !"1".equals(mSocialSecurity)&&(!"1".equals(tSumOperator)||!mG.Operator.equals(tHandler))){

            //案件结案后进行抽检，如果没有抽中则做结案处理        
                if (!endCase()) {
                    return false;
                }
             }
//           #2459
             else if("C12".equals((mCaseNo.substring(0, 3))) && "1".equals(tTJHOSP) && !"86".equals(tCJ) && !"1".equals(mSocialSecurity) ){
            //天津数采系统不抽检       
                if (!endCase()) {
                    return false;
                }
             }
             else{
                 //案件结案后进行抽检，如果没有抽中则做结案处理
                 if (!SpotClaim()) {
                 	if (mErrors.needDealError()) {
                 		return false;
                 	}
                     if (!endCase()) {
                         return false;
                     }
                 }
                  }
        }

        //更新立案分案表中案件状态
        mLLCaseSchema.setModifyDate(aDate);
        mLLCaseSchema.setModifyTime(aTime);
        map.put(mLLCaseSchema, "UPDATE");
        map.put(mLLClaimUWMainSchema, "DELETE&INSERT");
        if (mLLCaseOpTimeSet.size() > 0) {
            map.put(mLLCaseOpTimeSet, "DELETE&INSERT");
        }
        mResult.add(mReturnMessage);
        System.out.println("after dealData");
        return true;
    }

    private String checkUpuser() {
		// TODO Auto-generated method stub
    	if(mUpUserCode==null || "".equals(mUpUserCode)){
    		return "true";
    	}
		return "false";
	}

	/**
     * 循环处理保单核赔-审批->审定->结案
     * @return
     */
    private boolean SPUW() {
    	//是否有权限的标志，true-有权限
        boolean IsPower = true;
        if (!checkUserSP()) {
            return false;
        }
        //销售渠道是不是电商的，true代表是
        boolean DSFlag = true;

        LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
        tLLClaimPolicyDB.setClmNo(mClmNo);
        LLClaimPolicySet tLLClaimPolicySet = tLLClaimPolicyDB.query();
        if (tLLClaimPolicySet.size() <= 0) {
            CError.buildErr(this, "赔案明细表查询失败，没有理算结果！");
            return false;
        }

        //********************
         // 循环处理保单核赔
         //********************
        LLClaimUnderwriteSet tLLClaimUnderwriteSet = new LLClaimUnderwriteSet();
        LLClaimUWDetailSet tLLClaimUWDetailSet = new LLClaimUWDetailSet(); 
        //TODO: 首先循环判断赔案明细中是否只有特需险种：1）只有特需且有特需权限，走特需险种的权限；2）含有其他险种，走原有权限
    	String tSpecialNeedSql = "select 1 from llclaimpolicy where clmno='"+mClmNo+"' " +
    			"and riskcode in (select riskcode from lmriskapp where risktype3='7') ";
    	ExeSQL tSpecialNeedExe = new ExeSQL();
    	SSRS tSpecialNeedSSRS = new SSRS();
    	tSpecialNeedSSRS = tSpecialNeedExe.execSQL(tSpecialNeedSql);
    	LLClaimUserSchema tLLClaimUserSchema = getLLClaimUserSchema(mG.Operator);
    	
    	//  # 2739 <补充A> **start**
    	String aSpecialSQL ="select distinct 1 from llclaimdetail  where clmno='"+mClmNo+"' and riskcode ='170501' "+
    			"and not exists(select 1 from llclaimdetail where caseno='"+mCaseNo+"'and getdutykind='500'and getdutycode = '701204') with ur";
    	ExeSQL aSpecialExe = new ExeSQL();                                 
    	SSRS aSpecialSSRS =aSpecialExe.execSQL(aSpecialSQL);
    	// #2739 <补充A> **end**
    	
    	// # 3770 贴心管家险种  **start**
    	String TXGJSQL = "select distinct 1 from llclaimpolicy where 1=1 and caseno = '" +mCaseNo +  
    					"' and riskcode not in (select riskcode from lmriskapp where risktype3='7') and riskcode not in ('690101')";
    	ExeSQL TXGJExeSQL = new ExeSQL();
    	SSRS tss = TXGJExeSQL.execSQL(TXGJSQL);
    	int Number = tss.getMaxRow();
    	//  # 3770 贴心管家险种  **end**
    	
    	//3997 电商业务系统配置功能判断 start
    	int number1 = 0;
    	int number2 = 0;
    	for (int i = 1; i <= tLLClaimPolicySet.size(); i++) {
    		LLClaimPolicySchema sLLClaimPolicySchema = tLLClaimPolicySet.get(i);
    		String SocialSQL = "select lc.salechnl from llclaimpolicy ll ,lccont lc where 1=1 "
    				+ " and ll.contno = lc.contno and lc.salechnl is not null and ll.contno = '" 
    				+sLLClaimPolicySchema.getContNo() + "' and ll.caseno = '" + mCaseNo + "' with ur";
    		ExeSQL SocialExeSQL = new ExeSQL();
    		SSRS SocialSSRS = SocialExeSQL.execSQL(SocialSQL);
    		int length = SocialSSRS.getMaxRow();
    		if(length > 0) {    								
    				String Salechnl = SocialSSRS.GetText(1, 1);
    				if("21".equals(Salechnl) || "22".equals(Salechnl)) {
    					number1++;
    				}else {
    					number2++;
    				}
    		}
    	}
    	if(number1 > 0 && number2 == 0) {
    		//销售渠道是全为电商走电商逻辑
    		DSFlag = true;
    	}else if(number1>0 && number2>0){
    		//即含有电商业务又含有非电商业务需要判断电商和普通的权限大小
    		//电商业务审批权限
            String tmmDSNeedFlag = tLLClaimUserSchema.getDSNeedFlag();
            if("1".equals(tmmDSNeedFlag)) {          	
            	try{
            		//根据审批权限进行判断用户级别的大小
            		String DSClaimPopedom = tLLClaimUserSchema.getDSNeedLimitPopedom();
            		//目前只针对单个字母的权限，若以后需要扩展权限在此修改
            		if(DSClaimPopedom.length()!=1 && mClaimPopedom.length()!=1) {
            			mReturnMessage = "权限级别异常";
                		return false;
            		}
            		char ch1 = DSClaimPopedom.charAt(0);
            		int num1 = (Integer)(ch1-64);
            		char ch2 = mClaimPopedom.charAt(0);
            		int num2 = (Integer)(ch2-64);
            		if(num1>=num2) {
            			//电商权限大走普通逻辑
            			DSFlag = false;
            		}else {
            			//电商权限小走普通逻辑
            			DSFlag = true;
            		}
            	}catch(Exception e) {
            		e.printStackTrace();
            		mReturnMessage = "权限级别转换异常";
            		return false;
            	}	
            }else {
            	DSFlag = false;
            }
    	}else if(number1 == 0 && number2 > 0) {
    		//销售渠道没有电商走普通逻辑
    		DSFlag = false;
    	}else {
    		//没有销售渠道走普通逻辑
    		DSFlag = false;
    	}
    	//3997 电商业务系统配置功能判断end
    	
    	if(tSpecialNeedSSRS != null && tSpecialNeedSSRS.getMaxRow() > 0 
    			&& tSpecialNeedSSRS.getMaxRow() == tLLClaimPolicySet.size()
    			&& "1".equals(tLLClaimUserSchema.getSpecialNeedFlag())){//有特需险，且全部为特需险   		
    		checkSpecialNeedSP(tLLClaimUserSchema);	
    		
    	}
    	else if(mG.ManageCom.length()>=4 && ("8632").equals(mG.ManageCom.substring(0, 4)) 
    			&& Number!=1) {
    		checkSpecialNeedSP(tLLClaimUserSchema);	//江苏以及江苏分公司的贴心管家理赔案件处理，#3770走特需
    	}
    	else if(aSpecialSSRS != null && aSpecialSSRS.getMaxRow()>0        	// #2739 <补充A> 走特需
    			&& aSpecialSSRS.getMaxRow() == tLLClaimPolicySet.size()){
    		checkSpecialNeedSP(tLLClaimUserSchema);	
    	}
    	else if("1".equals(mSocialSecurity)){//TODO:添加社保权限的判断，只校验金额
    		LLSocialClaimUserSchema tLLSocialClaimUserSchema = getLLSocialClaimUserSchema(mG.Operator);
    		if(!checkSocialSecuritySP(tLLSocialClaimUserSchema)){//不符合社保校验，结束审批
    			return false;
    		}
    	}
    	else if(DSFlag){//添加电商审批权限校验
    		if(!checkDSSecuritySP(tLLClaimUserSchema)) {
    			return false;
    		}
    	}else{//无特需险或部分为特需，走正常
    		for (int i = 1; i <= tLLClaimPolicySet.size(); i++) {

                LLClaimPolicySchema tCPolSchema = tLLClaimPolicySet.get(i);
                if (tCPolSchema.getGiveType() == null) {
                    CError.buildErr(this, "保单无赔付结论,请先做理算确认！");
                    return false;
                }
                Reflections trf = new Reflections();
                LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new
                        LLClaimUnderwriteSchema();
                trf.transFields(tLLClaimUnderwriteSchema, tCPolSchema);
                if (!checkUserRight(tCPolSchema)) {
                    IsPower = false;
                    tLLClaimUnderwriteSchema.setAppGrade(mUpClaimPopedom);
                    tLLClaimUnderwriteSchema.setAppClmUWer(mUpUserCode);
                    tLLClaimUnderwriteSchema.setAppActionType("3"); //申请动作 ** 待定 **
                }
                //准备核赔记录数据LLClaimUnderwrite
                tLLClaimUnderwriteSchema.setClmDecision(tCPolSchema.getGiveType()); //核赔结论
                tLLClaimUnderwriteSchema.setClmDepend(tCPolSchema.getGiveReason()); //核赔依据
                tLLClaimUnderwriteSchema.setClmUWer(mG.Operator); //核赔员
                tLLClaimUnderwriteSchema.setClmUWGrade(mClaimPopedom);
                //设定本次保单核赔的审核类型为[初次审核]
                tLLClaimUnderwriteSchema.setCheckType("0"); //审批审定类型
                tLLClaimUnderwriteSchema.setOperator(mG.Operator);
                tLLClaimUnderwriteSchema.setMngCom(mG.ManageCom);
                tLLClaimUnderwriteSchema.setMakeDate(aDate);
                tLLClaimUnderwriteSchema.setMakeTime(aTime);
                tLLClaimUnderwriteSchema.setModifyDate(aDate);
                tLLClaimUnderwriteSchema.setModifyTime(aTime);
                tLLClaimUnderwriteSet.add(tLLClaimUnderwriteSchema);

                //准备核赔履历记录数据LLClaimUWDetail
                //获取履历最大序号
                LLClaimUWDetailSchema tLLClaimUWDetailSchema = new
                        LLClaimUWDetailSchema();
                LLClaimUWDetailDB tmLLClaimUWDetailDB = new LLClaimUWDetailDB();
                tmLLClaimUWDetailDB.setClmNo(mClmNo);
                tmLLClaimUWDetailDB.setPolNo(tCPolSchema.getPolNo());
                int tCount;
                tCount = tmLLClaimUWDetailDB.getCount();
                tCount++;
                trf.transFields(tLLClaimUWDetailSchema, tLLClaimUnderwriteSchema);
                tLLClaimUWDetailSchema.setGetDutyKind(tCPolSchema.getGetDutyKind());
                tLLClaimUWDetailSchema.setGetDutyCode(tCPolSchema.getGetDutyKind());
                tLLClaimUWDetailSchema.setClmUWNo(String.valueOf(tCount));
                tLLClaimUWDetailSchema.setGetDutyCode(tCPolSchema.getGetDutyKind());
                tLLClaimUWDetailSet.add(tLLClaimUWDetailSchema);
            
    		}
			map.put(tLLClaimUnderwriteSet.get(1), "DELETE&INSERT");
			map.put(tLLClaimUWDetailSet.get(1), "INSERT");
			mLLClaimUWMainSchema.setAppPhase("0"); //审核
			mLLClaimUWMainSchema.setClmUWer(mG.Operator);
			mLLClaimUWMainSchema.setClmUWGrade(mClaimPopedom);
			
			//校验用户结案权限
	        if (((IsPower)&& ("false".equals(boolblack))) || (!"false".equals(boolblack) && "true".equals(boolupuser) ) ) {
	            //用户具有结案权限
	        	//校验用户具有结案权限不是同一人
	        	if(!TestEndCaseUser()) {
	        		if (mUpUserCode == null||"".equals(mUpUserCode)) {
		                CError.buildErr(this, "该用户无权审定该案件，且无上级，案件上报失败！");
		                return false;
		            }
		        	
					if (!"1".equals(mUpStateFlag)) {
						CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");
						return false;
					}
					 mLLClaimUWMainSchema.setAppGrade(mUpClaimPopedom); //申请级别
			         mLLClaimUWMainSchema.setAppClmUWer(mUpUserCode); //申请审定人员
			         mLLClaimUWMainSchema.setAppActionType("3"); //申请动作 ** 待定 **
			         //设置案件状态为[审批状态]
			         mLLCaseSchema.setRgtState("05");
			         mLLCaseSchema.setUWer(mG.Operator);
			         mLLCaseSchema.setUWDate(aDate);
			         mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
			         mReturnMessage = "账单录入到理赔结案不能是同一个理赔人员，请等待上级进行审定，审定人:" +
			                             mLLClaimUWMainSchema.getAppClmUWer();
	        	}else {
	        		//设置核赔结论为赔付结论
		            mLLClaimUWMainSchema.setClmDecision(mClaimGiveType);
		            //设置案件状态为[结案状态]
		            mLLCaseSchema.setRgtState("09");
		            mLLCaseSchema.setUWer(mG.Operator);
		            mLLCaseSchema.setUWDate(aDate);
		            mLLCaseSchema.setSigner(mG.Operator);
		            mLLCaseSchema.setSignerDate(aDate);
		            mReturnMessage = "审批完毕,已经结案";
	        	}
	            
	        } else {
	            //用户没有结案权限，上报处理
	        	
	        	if (mUpUserCode == null||"".equals(mUpUserCode)) {
	                CError.buildErr(this, "该用户无权审定该案件，且无上级，案件上报失败！");
	                return false;
	            }
	        	
				if (!"1".equals(mUpStateFlag)) {
					CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");
					return false;
				}
	        	
	            mLLClaimUWMainSchema.setAppGrade(mUpClaimPopedom); //申请级别
	            mLLClaimUWMainSchema.setAppClmUWer(mUpUserCode); //申请审定人员
	            mLLClaimUWMainSchema.setAppActionType("3"); //申请动作 ** 待定 **
	            //设置案件状态为[审批状态]
	            mLLCaseSchema.setRgtState("05");
	            mLLCaseSchema.setUWer(mG.Operator);
	            mLLCaseSchema.setUWDate(aDate);
	            mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
	            mReturnMessage = "审批完毕,您权限不够,请等待上级审定,审定人:" +
	                             mLLClaimUWMainSchema.getAppClmUWer();
	        }
    	}
    	//黑名单，如果为最高权限，则不做处理
	        if(!"false".equals(boolblack) && !"1".equals(mSocialSecurity)){ //黑名单并且非社保
	        	 if( "true".equals(boolupuser)){//如果上级为空，说明当前用户为最高权限人,并且是黑名单
	        		 
	        	 }else{
	        		 	if("true_lcinsured".equals(boolblack)){
			            	mReturnMessage = "该案件被保险人为外国政要、国际组织高级管理人员及其特定关系人，报上级确认该被保险人身份信息";
			    	        
			            }
			            if("true_llbnf30".equals(boolblack)){
			            	mReturnMessage = "该案件身故受益人为外国政要、国际组织高级管理人员及其特定关系人，报上级确认该被保险人身份信息";
			    	        
			            }
			            if("true_llbnf31".equals(boolblack)){
			            	mReturnMessage = "该身故受益人为外国政要、国际组织高级管理人员及其特定关系人，报上级确认该被保险人身份信息";
			    	        
			            }
	        	 }
	        }
    	//黑名单，如果不是最高权限，则需要处理
    	
        //准备案件核赔履历信息
        //获取履历最大序号
        LLClaimUWMDetailDB tLLClaimUWMDetailDB = new LLClaimUWMDetailDB();
        tLLClaimUWMDetailDB.setClmNo(mClmNo);
        int tCount = tLLClaimUWMDetailDB.getCount();
        tCount++;
        Reflections ref = new Reflections();
        LLClaimUWMDetailSchema tClmUWMDlSchema = new LLClaimUWMDetailSchema();
        ref.transFields(tClmUWMDlSchema, mLLClaimUWMainSchema);
        tClmUWMDlSchema.setClmUWNo(String.valueOf(tCount));
        tClmUWMDlSchema.setRemark(mLLClaimUWMainSchema.getRemark2());
        tClmUWMDlSchema.setOperator(mG.Operator);
        tClmUWMDlSchema.setMngCom(mG.ManageCom);
        tClmUWMDlSchema.setMakeDate(aDate);
        tClmUWMDlSchema.setMakeTime(aTime);
        tClmUWMDlSchema.setModifyDate(aDate);
        tClmUWMDlSchema.setModifyTime(aTime);
        map.put(tClmUWMDlSchema, "DELETE&INSERT");
        fillOpTime("05", mG.Operator);
        return true;
    }
    /**
     * 校验是被保险人还是受益人黑名单 ，3668外国政要、国际组织高级管理人员及其特定关系人”理赔系统校验功能需求
     */
    private  String checkBlack() {
    	//3668外国政要、国际组织高级管理人员及其特定关系人”理赔系统校验功能需求start
		//28
    	ExeSQL checkBlackExe = new ExeSQL();
		String checkBlack="select name,idno from LDForeignSpecialList where 1=1 and trim(name)in('"+mLLCaseSchema.getCustomerName()+"')  and trim(idno) is not null and idno <>''  ";
		SSRS tDetailBlack = checkBlackExe.execSQL(checkBlack);
		if(tDetailBlack.getMaxRow() >= 1){ //黑名单列表有姓名并且有证件号
			String name=tDetailBlack.GetText(1, 1);
			String idno=tDetailBlack.GetText(1, 2);
			 //28
			//证件号一致
			if(idno.equals(mLLCaseSchema.getIDNo())){
				//黑名单
				return "true_lcinsured";
			}
			
		}
		 
		//29 ，有姓名 沒正件 
		String checkBlackIdno29="select * from LDForeignSpecialList where 1=1  and trim(name)in('"+mLLCaseSchema.getCustomerName()+"')  and (trim(idno) is   null or idno ='' ) ";
		SSRS tDetailBlackIdno29 = checkBlackExe.execSQL(checkBlackIdno29);
		if(tDetailBlackIdno29.getMaxRow() >= 1){ // 
			//黑名单
			return "true_lcinsured";
		}
		//30 身故
		String bnf="select name,idno from llbnf where 1=1 and caseno='"+mLLCaseSchema.getCaseNo()+"' and exists (select 1 from llcase where 1=1 and  caseno='"+mLLCaseSchema.getCaseNo()+"'  and DEATHDATE is not null   )";
		SSRS tDetailbnf = checkBlackExe.execSQL(bnf);
		if(tDetailbnf.getMaxRow() >= 1){ //身故案件
			for (int i = 1; i <= tDetailbnf.getMaxRow(); i++) {
				
			 
			String name=tDetailbnf.GetText(i, 1);
			String idno=tDetailbnf.GetText(i, 2);
			 
			String checkBlackIdno="select name,idno from LDForeignSpecialList where 1=1 and trim(name)in('"+name+"') and trim(idno) is not null and idno <>''  ";
			SSRS tDetailBlackIdno = checkBlackExe.execSQL(checkBlackIdno);
			if(tDetailBlackIdno.getMaxRow() >= 1){ //有姓名，you证件号
				//黑名单的姓名證件號
				for (int j = 1; j <= tDetailBlackIdno.getMaxRow(); j++) {
					String nameb=tDetailBlackIdno.GetText(j, 1);
					String idnob=tDetailBlackIdno.GetText(j, 2);
					
							//判斷身故受益人和黑名单的證件號是否一致
							if(idno.equals(idnob)){
								//黑名单
								return "true_llbnf30";
							}
					
				}
				
			}
			
			String checkBlackIdno2="select name,idno from LDForeignSpecialList where 1=1 and trim(name)in('"+name+"') and (trim(idno) is  null or trim(idno) =''  )  ";
			SSRS tDetailBlackIdno2 = checkBlackExe.execSQL(checkBlackIdno2);
			if(tDetailBlackIdno2.getMaxRow() >= 1){ //有姓名，沒有证件号
				 
							//黑名单
							return "true_llbnf31";
				 
			}
		}
    }
//3668外国政要、国际组织高级管理人员及其特定关系人”理赔系统校验功能需求end
		return "false";
	}
    /**
     * 传入用户编号，获得LLClaimUserSchema
     */
    private LLClaimUserSchema getLLClaimUserSchema(String aUserCode){
    	System.out.println("<-Go Into getLLClaimUserSchema()->");
    	LLClaimUserSchema tLLClaimUserSchema = new LLClaimUserSchema();
    	if(!"".equals(aUserCode) && aUserCode != null){
	    	LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
	    	LLClaimUserSet tllClaimUserSet =new LLClaimUserSet();
	        tLLClaimUserDB.setUserCode(aUserCode);
	        tllClaimUserSet = tLLClaimUserDB.query();
	        if(tllClaimUserSet.size()==0){
	        	return tLLClaimUserSchema;
	        }else{
	        	tLLClaimUserSchema = tLLClaimUserDB.query().get(1);	   
	        }  
    	}
    	return tLLClaimUserSchema;
    }
    
    /**
     * 传入用户编号，获得LLSocialClaimUserSchema
     */
    private LLSocialClaimUserSchema getLLSocialClaimUserSchema(String aUserCode){
    	System.out.println("<-Go Into getLLSocialClaimUserSchema()->");
    	LLSocialClaimUserSchema tLLSocialClaimUserSchema = new LLSocialClaimUserSchema();
    	if(!"".equals(aUserCode) && aUserCode != null){
    		LLSocialClaimUserDB tLLSocialClaimUserDB = new LLSocialClaimUserDB();
    		LLSocialClaimUserSet tLLSocialClaimUserSet = new LLSocialClaimUserSet();
    		tLLSocialClaimUserDB.setUserCode(aUserCode);
    		System.out.println("++++");
    		tLLSocialClaimUserSet = tLLSocialClaimUserDB.query();
    		if(tLLSocialClaimUserSet.size()==0){
    			return tLLSocialClaimUserSchema;
    		}else{
    			tLLSocialClaimUserSchema = tLLSocialClaimUserSet.get(1);
    		}
    	}
    	return tLLSocialClaimUserSchema;
    }
    /**
     * 审批时进行权限判断
     */
    private boolean checkDSSecuritySD(LLClaimUserSchema aLLClaimUserSchema) {
    	System.out.println("<-Go Into checkDSSecuritySP()->");
    	//是否有权限的标志，true-有权限
        boolean IsPower = true;
        //电商是否有权限标志
        boolean IsDSFlag = true;
        //电商业务审批权限
        String tmDSNeedFlag = aLLClaimUserSchema.getDSNeedFlag();
        //电商上级用户
    	String DSUpUserCode = aLLClaimUserSchema.getDSNeedUpUserCode();
    	
    	LLClaimUserSchema amLLClaimUserSchema = getLLClaimUserSchema(DSUpUserCode);
    	//电商上级用户的状态
    	String mDSUpStateFlag = amLLClaimUserSchema.getStateFlag();
    	//电商上级核赔权限
    	String tmClaimPopedom = amLLClaimUserSchema.getClaimPopedom();
       
    	
    	LLClaimPolicyDB amLLClaimPolicyDB = new LLClaimPolicyDB();
        amLLClaimPolicyDB.setClmNo(mClmNo);
        LLClaimPolicySet amLLClaimPolicySet = amLLClaimPolicyDB.query();
        
        LLClaimDB amLLClaimDB = new LLClaimDB();
        amLLClaimDB.setClmNo(mClmNo);
        LLClaimSet amLLClaimSet = amLLClaimDB.query();        
        //实赔金额
        double tRealPay = 0.0;
        for (int i = 1; i <= amLLClaimSet.size(); i++) {
            LLClaimSchema tCPolSchema = amLLClaimSet.get(i);    
            //实赔金额，需要取到案件级别
            tRealPay = tCPolSchema.getRealPay();
            System.out.println("特需案件实赔金额："+tRealPay);
        }
        
        for (int i = 1; i <= amLLClaimPolicySet.size(); i++) {

            LLClaimPolicySchema tCPolSchema = amLLClaimPolicySet.get(i);
            if (tCPolSchema.getGiveType() == null) {
                CError.buildErr(this, "保单无赔付结论,请先做理算确认！");
                return false;
            }
            
            if("1".equals(tmDSNeedFlag)) { 
            	//电商的核赔权限
            	mClaimPopedom = aLLClaimUserSchema.getDSNeedLimitPopedom();
            	if (!checkUserRight(tCPolSchema) && !"".equals(DSUpUserCode) && DSUpUserCode!=null) {
            		 //校验电商上级用户是否为有效状态
            		if (!"1".equals(mDSUpStateFlag)) {
    					CError.buildErr(this, "上级用户" + mDSUpStateFlag + "已失效，请与总公司联系");
    					return false;
    				}
            		//上报特需上级
                    //用户没有结案权限，上报处理       	
                    mLLClaimUWMainSchema.setAppGrade(tmClaimPopedom); //申请级别
                    mLLClaimUWMainSchema.setAppClmUWer(DSUpUserCode); //申请审定人员
                    mLLClaimUWMainSchema.setAppActionType(""); //申请动作 ** 待定 **
                    mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
                    mReturnMessage = "审定完毕,您权限不够,已送交上级审定,审定人:" +
                                     mLLClaimUWMainSchema.getAppClmUWer();
                    return true;
            	}else if(!checkUserRight(tCPolSchema) && ("".equals(DSUpUserCode) || DSUpUserCode==null)) {
            		//走普通上报上级
                	IsPower = false;
                    break;
            	}
            }else {
            	if (!checkUserRight(tCPolSchema)) {
            		//走普通上报上级
                	IsPower = false;
                    break;
            	}
            }
        }   
		
        if ((IsPower && ("false".equals(boolblack)))  || (!"false".equals(boolblack) && "true".equals(boolupuser) )) {
            //签批通过，设置案件状态为[审定状态]
            if (mOperate.equals("BATCH||UW") || mRemarkSD == null ||
                mRemarkSD.equals("")) {
                mLLCaseSchema.setRgtState("09");
                mLLCaseSchema.setEndCaseDate(aDate);
            } else {
                mLLCaseSchema.setRgtState("06");
            }
            mLLCaseSchema.setSigner(mLLClaimUWMainSchema.getAppClmUWer());
            mLLCaseSchema.setSignerDate(aDate);
            mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getClmUWer());
            mReturnMessage = "审定完毕,已送交原审批人处理";
            if (mOperate.equals("BATCH||UW") || mRemarkSD == null ||
                mRemarkSD.equals("")) {
                mReturnMessage = "审定完毕";
            }
        } else {
            //无权限,继续上报
            if (mUpUserCode == null||"".equals(mUpUserCode)) {
                CError.buildErr(this, "该用户无权审定该案件，且无上级，案件上报失败！");
                return false;
            }
            
            if (!"1".equals(mUpStateFlag)) {
				CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");
				return false;
			}
            
            mLLClaimUWMainSchema.setAppGrade(mUpClaimPopedom); //申请级别
            mLLClaimUWMainSchema.setAppClmUWer(mUpUserCode); //申请审定人员
            mLLClaimUWMainSchema.setAppActionType(""); //申请动作 ** 待定 **
            mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
            mReturnMessage = "审定完毕,您权限不够,已送交上级审定,审定人:" +
                             mLLClaimUWMainSchema.getAppClmUWer();
        }
        
        
    	return true;
    }
    /**
     * 理算时进行权限判断
     */
    private boolean checkDSSecuritySP(LLClaimUserSchema aLLClaimUserSchema) {
    	System.out.println("<-Go Into checkDSSecuritySP()->");
    	//是否有权限的标志，true-有权限
        boolean IsPower = true;
        //电商是否有权限标志
        boolean IsDSFlag = true;
        //电商业务审批权限
        String tmDSNeedFlag = aLLClaimUserSchema.getDSNeedFlag();
        //电商上级用户
    	String DSUpUserCode = aLLClaimUserSchema.getDSNeedUpUserCode();
    	
    	LLClaimUserSchema amLLClaimUserSchema = getLLClaimUserSchema(DSUpUserCode);
    	//电商上级用户的状态
    	String mDSUpStateFlag = amLLClaimUserSchema.getStateFlag();
    	//电商上级核赔权限
    	String tmClaimPopedom = amLLClaimUserSchema.getClaimPopedom();
        
        LLClaimUnderwriteSet tmLLClaimUnderwriteSet = new LLClaimUnderwriteSet();
        LLClaimUWDetailSet tmLLClaimUWDetailSet = new LLClaimUWDetailSet();
    	
    	LLClaimPolicyDB amLLClaimPolicyDB = new LLClaimPolicyDB();
        amLLClaimPolicyDB.setClmNo(mClmNo);
        LLClaimPolicySet amLLClaimPolicySet = amLLClaimPolicyDB.query();
        
        LLClaimDB amLLClaimDB = new LLClaimDB();
        amLLClaimDB.setClmNo(mClmNo);
        LLClaimSet amLLClaimSet = amLLClaimDB.query();        
        //实赔金额
        double tRealPay = 0.0;
        for (int i = 1; i <= amLLClaimSet.size(); i++) {
            LLClaimSchema tCPolSchema = amLLClaimSet.get(i);    
            //实赔金额，需要取到案件级别
            tRealPay = tCPolSchema.getRealPay();
            System.out.println("特需案件实赔金额："+tRealPay);
        }
        
        for (int i = 1; i <= amLLClaimPolicySet.size(); i++) {

            LLClaimPolicySchema tCPolSchema = amLLClaimPolicySet.get(i);
            if (tCPolSchema.getGiveType() == null) {
                CError.buildErr(this, "保单无赔付结论,请先做理算确认！");
                return false;
            }
            Reflections trf = new Reflections();
            LLClaimUnderwriteSchema tmLLClaimUnderwriteSchema = new
                    LLClaimUnderwriteSchema();
            trf.transFields(tmLLClaimUnderwriteSchema, tCPolSchema);
            
            if("1".equals(tmDSNeedFlag)) {
            	//电商的核赔权限
            	mClaimPopedom = aLLClaimUserSchema.getDSNeedLimitPopedom();
            	if (!checkUserRight(tCPolSchema) && !"".equals(DSUpUserCode) && DSUpUserCode!=null) {
            		  //校验电商上级用户是否为有效状态
            		if (!"1".equals(mDSUpStateFlag)) {
    					CError.buildErr(this, "上级用户" + mDSUpStateFlag + "已失效，请与总公司联系");
    					return false;
    				}
            		//用户没有结案权限，有电商上级用户，上报处理       	
                    mLLClaimUWMainSchema.setAppGrade(tmClaimPopedom); //申请级别
                    mLLClaimUWMainSchema.setAppClmUWer(DSUpUserCode); //申请审定人员
                    mLLClaimUWMainSchema.setAppActionType("3"); //申请动作 ** 待定 **
                    //设置案件状态为[审批状态]
                    mLLCaseSchema.setRgtState("05");
                    mLLCaseSchema.setUWer(mG.Operator);
                    mLLCaseSchema.setUWDate(aDate);
                    mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
                    mReturnMessage = "审批完毕,您权限不够,请等待上级审定,审定人:" +
                                     mLLClaimUWMainSchema.getAppClmUWer();
                    IsDSFlag = false;
            	}else if(!checkUserRight(tCPolSchema) && ("".equals(DSUpUserCode) || DSUpUserCode==null)) {
            		//用户没有结案权限，无电商上级用户，上报处理     
            		IsPower = false;
                	tmLLClaimUnderwriteSchema.setAppGrade(mUpClaimPopedom);
                    tmLLClaimUnderwriteSchema.setAppClmUWer(mUpUserCode);
                    tmLLClaimUnderwriteSchema.setAppActionType("3"); //申请动作 ** 待定 **
                	}
            	}else {
            		if (!checkUserRight(tCPolSchema)) {
               		 //没有电商审批权限
               		 IsPower = false;
               		 tmLLClaimUnderwriteSchema.setAppGrade(mUpClaimPopedom);
                     tmLLClaimUnderwriteSchema.setAppClmUWer(mUpUserCode);
                     tmLLClaimUnderwriteSchema.setAppActionType("3"); //申请动作 ** 待定 **
               	 	}
            	}
            //准备核赔记录数据LLClaimUnderwrite
            tmLLClaimUnderwriteSchema.setClmDecision(tCPolSchema.getGiveType()); //核赔结论
            tmLLClaimUnderwriteSchema.setClmDepend(tCPolSchema.getGiveReason()); //核赔依据
            tmLLClaimUnderwriteSchema.setClmUWer(mG.Operator); //核赔员
            tmLLClaimUnderwriteSchema.setClmUWGrade(mClaimPopedom);
            //设定本次保单核赔的审核类型为[初次审核]
            tmLLClaimUnderwriteSchema.setCheckType("0"); //审批审定类型
            tmLLClaimUnderwriteSchema.setOperator(mG.Operator);
            tmLLClaimUnderwriteSchema.setMngCom(mG.ManageCom);
            tmLLClaimUnderwriteSchema.setMakeDate(aDate);
            tmLLClaimUnderwriteSchema.setMakeTime(aTime);
            tmLLClaimUnderwriteSchema.setModifyDate(aDate);
            tmLLClaimUnderwriteSchema.setModifyTime(aTime);
            tmLLClaimUnderwriteSet.add(tmLLClaimUnderwriteSchema);
            
          //准备核赔履历记录数据LLClaimUWDetail
            //获取履历最大序号
            LLClaimUWDetailSchema tLLClaimUWDetailSchema = new
                    LLClaimUWDetailSchema();
            LLClaimUWDetailDB tmLLClaimUWDetailDB = new LLClaimUWDetailDB();
            tmLLClaimUWDetailDB.setClmNo(mClmNo);
            tmLLClaimUWDetailDB.setPolNo(tCPolSchema.getPolNo());
            int tCount;
            tCount = tmLLClaimUWDetailDB.getCount();
            tCount++;
            trf.transFields(tLLClaimUWDetailSchema, tmLLClaimUnderwriteSchema);
            tLLClaimUWDetailSchema.setGetDutyKind(tCPolSchema.getGetDutyKind());
            tLLClaimUWDetailSchema.setGetDutyCode(tCPolSchema.getGetDutyKind());
            tLLClaimUWDetailSchema.setClmUWNo(String.valueOf(tCount));
            tLLClaimUWDetailSchema.setGetDutyCode(tCPolSchema.getGetDutyKind());
            tmLLClaimUWDetailSet.add(tLLClaimUWDetailSchema);
            }
        map.put(tmLLClaimUnderwriteSet.get(1), "DELETE&INSERT");
		map.put(tmLLClaimUWDetailSet.get(1), "INSERT");
		mLLClaimUWMainSchema.setAppPhase("0"); //审核
		mLLClaimUWMainSchema.setClmUWer(mG.Operator);
		mLLClaimUWMainSchema.setClmUWGrade(mClaimPopedom);
		
		
		 if ((IsPower && IsDSFlag && ("false".equals(boolblack)))  || (!"false".equals(boolblack) && "true".equals(boolupuser) && IsDSFlag &&  IsPower) ) {
	            //用户具有结案权限
			 //校验用户具有结案权限不是同一人
	        	if(!TestEndCaseUser()) {
	        		if("".equals(DSUpUserCode) || "null".equals(DSUpUserCode) || DSUpUserCode == null) {
	        			//用户没有结案权限，上报普通案件上级处理    	
	    	        	if (mUpUserCode == null ||"".equals(mUpUserCode)) {
	    	                CError.buildErr(this, "该用户无权审定该案件，且无上级，案件上报失败！");
	    	                return false;
	    	            }
	    	        	
	    				if (!"1".equals(mUpStateFlag)) {
	    					CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");
	    					return false;
	    				}
	    				mLLClaimUWMainSchema.setAppGrade(mUpClaimPopedom); //申请级别
	    	            mLLClaimUWMainSchema.setAppClmUWer(mUpUserCode); //申请审定人员
	    	            mLLClaimUWMainSchema.setAppActionType("3"); //申请动作 ** 待定 **
	    	            //设置案件状态为[审批状态]
	    	            mLLCaseSchema.setRgtState("05");
	    	            mLLCaseSchema.setUWer(mG.Operator);
	    	            mLLCaseSchema.setUWDate(aDate);
	    	            mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
	    	            mReturnMessage = "账单录入到理赔结案不能是同一个理赔人员，请等待上级进行审定，审定人:" +
	    	                             mLLClaimUWMainSchema.getAppClmUWer();
	        		}else {
	        			 //校验电商上级用户是否为有效状态
	            		if (!"1".equals(mDSUpStateFlag)) {
	    					CError.buildErr(this, "上级用户" + mDSUpStateFlag + "已失效，请与总公司联系");
	    					return false;
	    				}
	            		//用户没有结案权限，有电商上级用户，上报处理       	
	                    mLLClaimUWMainSchema.setAppGrade(tmClaimPopedom); //申请级别
	                    mLLClaimUWMainSchema.setAppClmUWer(DSUpUserCode); //申请审定人员
	                    mLLClaimUWMainSchema.setAppActionType("3"); //申请动作 ** 待定 **
	                    //设置案件状态为[审批状态]
	                    mLLCaseSchema.setRgtState("05");
	                    mLLCaseSchema.setUWer(mG.Operator);
	                    mLLCaseSchema.setUWDate(aDate);
	                    mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
	                    mReturnMessage = "账单录入到理赔结案不能是同一个理赔人员，请等待上级进行审定，审定人:" +
	                                     mLLClaimUWMainSchema.getAppClmUWer();
	        		}
	        	}else {
	        		//设置核赔结论为赔付结论
		            mLLClaimUWMainSchema.setClmDecision(mClaimGiveType);
		            //设置案件状态为[结案状态]
		            mLLCaseSchema.setRgtState("09");
		            mLLCaseSchema.setUWer(mG.Operator);
		            mLLCaseSchema.setUWDate(aDate);
		            mLLCaseSchema.setSigner(mG.Operator);
		            mLLCaseSchema.setSignerDate(aDate);
		            mReturnMessage = "审批完毕,已经结案";
	        	}
	            
	            
	        } else if((IsDSFlag && !IsPower) || (!"false".equals(boolblack) && !"true".equals(boolupuser) && IsDSFlag &&  IsPower  ) ){
	            //用户没有结案权限，上报普通案件上级处理    	
	        	if (mUpUserCode == null ||"".equals(mUpUserCode)) {
	                CError.buildErr(this, "该用户无权审定该案件，且无上级，案件上报失败！");
	                return false;
	            }
	        	
				if (!"1".equals(mUpStateFlag)) {
					CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");
					return false;
				}
	        	
	            mLLClaimUWMainSchema.setAppGrade(mUpClaimPopedom); //申请级别
	            mLLClaimUWMainSchema.setAppClmUWer(mUpUserCode); //申请审定人员
	            mLLClaimUWMainSchema.setAppActionType("3"); //申请动作 ** 待定 **
	            //设置案件状态为[审批状态]
	            mLLCaseSchema.setRgtState("05");
	            mLLCaseSchema.setUWer(mG.Operator);
	            mLLCaseSchema.setUWDate(aDate);
	            mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
	            mReturnMessage = "审批完毕,您权限不够,请等待上级审定,审定人:" +
	                             mLLClaimUWMainSchema.getAppClmUWer();
	        }
        
        
    	return true;
    }
    
    /**
     * 校验理赔人员操作是同一人
     * @return
     */
private boolean TestEndCaseUser() {
    	
    	System.out.println("开始校验案件处理人是否为同一人");
    	String LLCaseno = mLLCaseSchema.getCaseNo();
    	ExeSQL tttExeSQL = new ExeSQL();
    	String tt1SQL = "select operator from llfeemain where 1=1 and caseno = '" +
    			LLCaseno + "' order by modifydate desc,modifytime desc with ur";
    	SSRS MM1 = tttExeSQL.execSQL(tt1SQL);
    	String ttHandler2 = "";
    	if(MM1.getMaxRow() > 0) {
    		ttHandler2 = MM1.GetText(1, 1);
    	}
    	
    	String tt2SQL = "select operator from llclaimpolicy where caseno = '" + LLCaseno + "' with ur";
    	SSRS MM2 = tttExeSQL.execSQL(tt2SQL);
    	if(MM2.getMaxRow()<1) {
    		return true;
    	}
    	String ttHandler1 = MM2.GetText(1, 1);
    	String tt3SQL = "select handler from llcase where caseno = '" + LLCaseno + "' with ur ";
    	SSRS MM3 = tttExeSQL.execSQL(tt3SQL);
    	if(MM3.getMaxRow()<1) {
    		return true;
    	}
    	String ttHandler3 = MM3.GetText(1, 1);
    	
    	if(("".equals(ttHandler1)||"null".equals(ttHandler1)||ttHandler1==null) ||  
    			("".equals(ttHandler3)||"null".equals(ttHandler3)||ttHandler2==null)) {
    		return true;
    	}else {
    		if("".equals(ttHandler2)||"null".equals(ttHandler2)||ttHandler2==null) {
    			if(ttHandler1.equals(ttHandler3)) {
    				return false;
    			}
    		}else {
    			if(ttHandler1.equals(ttHandler2) && ttHandler1.equals(ttHandler3)) {
        			return false;
        		}
    		}
    		
    	}   		
         return true;
    }
    
    /**
     * 
     * @param aLLClaimUserSchema
     * @return
     */
    private boolean checkSocialSecuritySP(LLSocialClaimUserSchema aLLSocialClaimUserSchema){
    	System.out.println("<-Go Into checkSocialSecuritySP()->");

    	//是否有权限的标志，true-有权限
        boolean IsPower = true;
        //社保案件特殊标志 
        boolean IsSocialSecurity = true;
    	//社保业务结案金额
        double tSocialMoney = aLLSocialClaimUserSchema.getSocialMoney();
        //社保上级
        String tUpUserCode = aLLSocialClaimUserSchema.getUpUserCode();
        LLSocialClaimUserSchema tLLSocialClaimUserSchema = getLLSocialClaimUserSchema(tUpUserCode);
        //社保上级权限
        String tUpClaimPopedom = tLLSocialClaimUserSchema.getClaimPopedom();
        String tUpStateFlag = tLLSocialClaimUserSchema.getStateFlag();

    	LLClaimUnderwriteSet tLLClaimUnderwriteSet = new LLClaimUnderwriteSet();
        LLClaimUWDetailSet tLLClaimUWDetailSet = new LLClaimUWDetailSet();
    	
    	LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
        tLLClaimPolicyDB.setClmNo(mClmNo);
        LLClaimPolicySet tLLClaimPolicySet = tLLClaimPolicyDB.query();
        
        //社保案件层次实赔金额
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setClmNo(mClmNo);
        LLClaimSet tLLClaimSet = tLLClaimDB.query();
        //实赔金额，需要取到案件级别
        double tRealPay = 0.0;        
        for (int i = 1; i <= tLLClaimSet.size(); i++) {
            LLClaimSchema tCPolSchema = tLLClaimSet.get(i);    
            //实赔金额，需要取到案件级别
            tRealPay = tCPolSchema.getRealPay();
            mSocialSecurityRealPay = tRealPay;
            System.out.println("社保案件实赔金额："+tRealPay);
        }
        
        for (int i = 1; i <= tLLClaimPolicySet.size(); i++) {

            LLClaimPolicySchema tCPolSchema = tLLClaimPolicySet.get(i);
            if (tCPolSchema.getGiveType() == null) {
                CError.buildErr(this, "保单无赔付结论,请先做理算确认！");
                return false;
            }
            Reflections trf = new Reflections();
            LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new
                    LLClaimUnderwriteSchema();
            trf.transFields(tLLClaimUnderwriteSchema, tCPolSchema);
            
            System.out.println("社保案件险种"+tCPolSchema.getRiskCode()+"的实赔金额："+tRealPay);
            if (tSocialMoney < tRealPay && !"".equals(tUpUserCode) && tUpUserCode != null) {//社保超权限且有社保上级
                tLLClaimUnderwriteSchema.setAppGrade(tUpClaimPopedom);
                tLLClaimUnderwriteSchema.setAppClmUWer(tUpUserCode);
                tLLClaimUnderwriteSchema.setAppActionType("3"); //申请动作 ** 待定 **
                
                //上报社保上级
                //用户没有结案权限，上报处理     
	        	
				if (!"1".equals(tUpStateFlag)) {
					CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");
					return false;
				}
                
                mLLClaimUWMainSchema.setAppGrade(tUpClaimPopedom); //申请级别
                mLLClaimUWMainSchema.setAppClmUWer(tUpUserCode); //申请审定人员
                mLLClaimUWMainSchema.setAppActionType("3"); //申请动作 ** 待定 **
                //设置案件状态为[审批状态]
                mLLCaseSchema.setRgtState("05");
                mLLCaseSchema.setUWer(mG.Operator);
                mLLCaseSchema.setUWDate(aDate);
                mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
                mReturnMessage = "审批完毕,您权限不够,请等待上级审定,审定人:" +
                                 mLLClaimUWMainSchema.getAppClmUWer();
                IsSocialSecurity = false;
            }else if(tSocialMoney < tRealPay && ("".equals(tUpUserCode) || tUpUserCode == null)){//社保超权且无社保上级
            	IsPower = false;
            	CError.buildErr(this, "该用户无权审定该社保案件，且无上级，案件上报失败！");
                return false;
            }
            //准备核赔记录数据LLClaimUnderwrite
            tLLClaimUnderwriteSchema.setClmDecision(tCPolSchema.getGiveType()); //核赔结论
            tLLClaimUnderwriteSchema.setClmDepend(tCPolSchema.getGiveReason()); //核赔依据
            tLLClaimUnderwriteSchema.setClmUWer(mG.Operator); //核赔员
            tLLClaimUnderwriteSchema.setClmUWGrade(mClaimPopedom);
            //设定本次保单核赔的审核类型为[初次审核]
            tLLClaimUnderwriteSchema.setCheckType("0"); //审批审定类型
            tLLClaimUnderwriteSchema.setOperator(mG.Operator);
            tLLClaimUnderwriteSchema.setMngCom(mG.ManageCom);
            tLLClaimUnderwriteSchema.setMakeDate(aDate);
            tLLClaimUnderwriteSchema.setMakeTime(aTime);
            tLLClaimUnderwriteSchema.setModifyDate(aDate);
            tLLClaimUnderwriteSchema.setModifyTime(aTime);
            tLLClaimUnderwriteSet.add(tLLClaimUnderwriteSchema);

            //准备核赔履历记录数据LLClaimUWDetail
            //获取履历最大序号
            LLClaimUWDetailSchema tLLClaimUWDetailSchema = new
                    LLClaimUWDetailSchema();
            LLClaimUWDetailDB tmLLClaimUWDetailDB = new LLClaimUWDetailDB();
            tmLLClaimUWDetailDB.setClmNo(mClmNo);
            tmLLClaimUWDetailDB.setPolNo(tCPolSchema.getPolNo());
            int tCount;
            tCount = tmLLClaimUWDetailDB.getCount();
            tCount++;
            trf.transFields(tLLClaimUWDetailSchema, tLLClaimUnderwriteSchema);
            tLLClaimUWDetailSchema.setGetDutyKind(tCPolSchema.getGetDutyKind());
            tLLClaimUWDetailSchema.setGetDutyCode(tCPolSchema.getGetDutyKind());
            tLLClaimUWDetailSchema.setClmUWNo(String.valueOf(tCount));
            tLLClaimUWDetailSchema.setGetDutyCode(tCPolSchema.getGetDutyKind());
            tLLClaimUWDetailSet.add(tLLClaimUWDetailSchema);
        }
        map.put(tLLClaimUnderwriteSet.get(1), "DELETE&INSERT");
		map.put(tLLClaimUWDetailSet.get(1), "INSERT");
		mLLClaimUWMainSchema.setAppPhase("0"); //审核
		mLLClaimUWMainSchema.setClmUWer(mG.Operator);
		mLLClaimUWMainSchema.setClmUWGrade(mClaimPopedom);
		
		 
		
//		校验用户结案权限
        if (IsPower && IsSocialSecurity) {
            //用户具有结案权限
        	
        	//校验用户具有结案权限不是同一人
        	if(!TestEndCaseUser()) {
        		//上报社保上级
                //用户没有结案权限，上报处理     	        	
				if (!"1".equals(tUpStateFlag)) {
					CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");
					return false;
				}
                
                mLLClaimUWMainSchema.setAppGrade(tUpClaimPopedom); //申请级别
                mLLClaimUWMainSchema.setAppClmUWer(tUpUserCode); //申请审定人员
                mLLClaimUWMainSchema.setAppActionType("3"); //申请动作 ** 待定 **
                //设置案件状态为[审批状态]
                mLLCaseSchema.setRgtState("05");
                mLLCaseSchema.setUWer(mG.Operator);
                mLLCaseSchema.setUWDate(aDate);
                mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
                mReturnMessage = "账单录入到理赔结案不能是同一个理赔人员，请等待上级进行审定，审定人:" +
                                 mLLClaimUWMainSchema.getAppClmUWer();
        	}else {
        		//设置核赔结论为赔付结论
                mLLClaimUWMainSchema.setClmDecision(mClaimGiveType);
                //设置案件状态为[结案状态]
                mLLCaseSchema.setRgtState("09");
                mLLCaseSchema.setUWer(mG.Operator);
                mLLCaseSchema.setUWDate(aDate);
                mLLCaseSchema.setSigner(mG.Operator);
                mLLCaseSchema.setSignerDate(aDate);
                mReturnMessage = "审批完毕,已经结案";
        	}          
        } 
		return true;
    }
    
    /**
     * 审批时用户的特需数据存储与权限判断-false:返回普通案件上级
     * @return
     */
    private boolean checkSpecialNeedSP(LLClaimUserSchema aLLClaimUserSchema) {
    	System.out.println("<-Go Into checkSpecialNeedSP()->");

    	//是否有权限的标志，true-有权限
        boolean IsPower = true;
        //特需险特殊标志 
        boolean IsSpecialNeed = true;
    	//特需权限金额
        double tSpecialNeedLimit = aLLClaimUserSchema.getSpecialNeedLimit();
        //特需上级
        String tSpecialNeedUpUserCode = aLLClaimUserSchema.getSpecialNeedUpUserCode();
    	LLClaimUserSchema tLLClaimUserSchema = getLLClaimUserSchema(tSpecialNeedUpUserCode);
        //特需上级权限
        String tUpClaimPopedom = tLLClaimUserSchema.getClaimPopedom();
        //特需上级是否为有效的
        String tSpecialUpClaimState = tLLClaimUserSchema.getStateFlag();

    	LLClaimUnderwriteSet tLLClaimUnderwriteSet = new LLClaimUnderwriteSet();
        LLClaimUWDetailSet tLLClaimUWDetailSet = new LLClaimUWDetailSet();
    	
    	LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
        tLLClaimPolicyDB.setClmNo(mClmNo);
        LLClaimPolicySet tLLClaimPolicySet = tLLClaimPolicyDB.query();
        
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setClmNo(mClmNo);
        LLClaimSet tLLClaimSet = tLLClaimDB.query();        
        //实赔金额
        double tRealPay = 0.0;
        for (int i = 1; i <= tLLClaimSet.size(); i++) {
            LLClaimSchema tCPolSchema = tLLClaimSet.get(i);    
            //实赔金额，需要取到案件级别
            tRealPay = tCPolSchema.getRealPay();
            System.out.println("特需案件实赔金额："+tRealPay);
        }
        
        for (int i = 1; i <= tLLClaimPolicySet.size(); i++) {

            LLClaimPolicySchema tCPolSchema = tLLClaimPolicySet.get(i);
            if (tCPolSchema.getGiveType() == null) {
                CError.buildErr(this, "保单无赔付结论,请先做理算确认！");
                return false;
            }
            Reflections trf = new Reflections();
            LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new
                    LLClaimUnderwriteSchema();
            trf.transFields(tLLClaimUnderwriteSchema, tCPolSchema);
            
            System.out.println("案件特需险种"+tCPolSchema.getRiskCode()+"的实赔金额："+tRealPay);
            if (tSpecialNeedLimit < tRealPay && !"".equals(tSpecialNeedUpUserCode) && tSpecialNeedUpUserCode != null) {//特需超权限且有特需上级
                tLLClaimUnderwriteSchema.setAppGrade(tUpClaimPopedom);
                tLLClaimUnderwriteSchema.setAppClmUWer(tSpecialNeedUpUserCode);
                tLLClaimUnderwriteSchema.setAppActionType("3"); //申请动作 ** 待定 **
                
                //上报特需上级
                //用户没有结案权限，上报处理       	
                mLLClaimUWMainSchema.setAppGrade(tUpClaimPopedom); //申请级别
                mLLClaimUWMainSchema.setAppClmUWer(tSpecialNeedUpUserCode); //申请审定人员
                mLLClaimUWMainSchema.setAppActionType("3"); //申请动作 ** 待定 **
                //设置案件状态为[审批状态]
                mLLCaseSchema.setRgtState("05");
                mLLCaseSchema.setUWer(mG.Operator);
                mLLCaseSchema.setUWDate(aDate);
                mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
                mReturnMessage = "审批完毕,您权限不够,请等待上级审定,审定人:" +
                                 mLLClaimUWMainSchema.getAppClmUWer();
                IsSpecialNeed = false;
            }else if(tSpecialNeedLimit < tRealPay && ("".equals(tSpecialNeedUpUserCode) || tSpecialNeedUpUserCode == null)){//特需超权且无特需上级
            	IsPower = false;
            	tLLClaimUnderwriteSchema.setAppGrade(mUpClaimPopedom);
                tLLClaimUnderwriteSchema.setAppClmUWer(mUpUserCode);
                tLLClaimUnderwriteSchema.setAppActionType("3"); //申请动作 ** 待定 **
            }
            //准备核赔记录数据LLClaimUnderwrite
            tLLClaimUnderwriteSchema.setClmDecision(tCPolSchema.getGiveType()); //核赔结论
            tLLClaimUnderwriteSchema.setClmDepend(tCPolSchema.getGiveReason()); //核赔依据
            tLLClaimUnderwriteSchema.setClmUWer(mG.Operator); //核赔员
            tLLClaimUnderwriteSchema.setClmUWGrade(mClaimPopedom);
            //设定本次保单核赔的审核类型为[初次审核]
            tLLClaimUnderwriteSchema.setCheckType("0"); //审批审定类型
            tLLClaimUnderwriteSchema.setOperator(mG.Operator);
            tLLClaimUnderwriteSchema.setMngCom(mG.ManageCom);
            tLLClaimUnderwriteSchema.setMakeDate(aDate);
            tLLClaimUnderwriteSchema.setMakeTime(aTime);
            tLLClaimUnderwriteSchema.setModifyDate(aDate);
            tLLClaimUnderwriteSchema.setModifyTime(aTime);
            tLLClaimUnderwriteSet.add(tLLClaimUnderwriteSchema);

            //准备核赔履历记录数据LLClaimUWDetail
            //获取履历最大序号
            LLClaimUWDetailSchema tLLClaimUWDetailSchema = new
                    LLClaimUWDetailSchema();
            LLClaimUWDetailDB tmLLClaimUWDetailDB = new LLClaimUWDetailDB();
            tmLLClaimUWDetailDB.setClmNo(mClmNo);
            tmLLClaimUWDetailDB.setPolNo(tCPolSchema.getPolNo());
            int tCount;
            tCount = tmLLClaimUWDetailDB.getCount();
            tCount++;
            trf.transFields(tLLClaimUWDetailSchema, tLLClaimUnderwriteSchema);
            tLLClaimUWDetailSchema.setGetDutyKind(tCPolSchema.getGetDutyKind());
            tLLClaimUWDetailSchema.setGetDutyCode(tCPolSchema.getGetDutyKind());
            tLLClaimUWDetailSchema.setClmUWNo(String.valueOf(tCount));
            tLLClaimUWDetailSchema.setGetDutyCode(tCPolSchema.getGetDutyKind());
            tLLClaimUWDetailSet.add(tLLClaimUWDetailSchema);
        }
        map.put(tLLClaimUnderwriteSet.get(1), "DELETE&INSERT");
		map.put(tLLClaimUWDetailSet.get(1), "INSERT");
		mLLClaimUWMainSchema.setAppPhase("0"); //审核
		mLLClaimUWMainSchema.setClmUWer(mG.Operator);
		mLLClaimUWMainSchema.setClmUWGrade(mClaimPopedom);
		 
		 //具有结案权限，并且是特需并且不是黑名单，走正常结案
		 //黑名单并且是最高权限，并且是特需险，走正常结案
//		校验用户结案权限
        if ((IsPower && IsSpecialNeed && ("false".equals(boolblack)))  || (!"false".equals(boolblack) && "true".equals(boolupuser) && IsSpecialNeed &&  IsPower) ) {
            
        	//校验用户具有结案权限不是同一人
        	if(!TestEndCaseUser()) {
        		if("".equals(tSpecialNeedUpUserCode) || tSpecialNeedUpUserCode == null || "null".equals(tSpecialNeedUpUserCode)) {
        			 	
        			if (!"1".equals(mUpStateFlag)) {
        				CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");
        				return false;
        			}
        				
        				mLLClaimUWMainSchema.setAppGrade(mUpClaimPopedom); //申请级别
        	            mLLClaimUWMainSchema.setAppClmUWer(mUpUserCode); //申请审定人员
        	            mLLClaimUWMainSchema.setAppActionType("3"); //申请动作 ** 待定 **
        	            //设置案件状态为[审批状态]
        	            mLLCaseSchema.setRgtState("05");
        	            mLLCaseSchema.setUWer(mG.Operator);
        	            mLLCaseSchema.setUWDate(aDate);
        	            mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
        	            mReturnMessage = "账单录入到理赔结案不能是同一个理赔人员，请等待上级进行审定，审定人:" +
        	                             mLLClaimUWMainSchema.getAppClmUWer();
        		}else {
        			
        			if (!"1".equals(tSpecialUpClaimState)) {
        				CError.buildErr(this, "特需上级用户" + mUpUserCode + "已失效，请与总公司联系");
        				return false;
        			}
        			//上报特需上级
                    //用户没有结案权限，上报处理       	
                    mLLClaimUWMainSchema.setAppGrade(tUpClaimPopedom); //申请级别
                    mLLClaimUWMainSchema.setAppClmUWer(tSpecialNeedUpUserCode); //申请审定人员
                    mLLClaimUWMainSchema.setAppActionType("3"); //申请动作 ** 待定 **
                    //设置案件状态为[审批状态]
                    mLLCaseSchema.setRgtState("05");
                    mLLCaseSchema.setUWer(mG.Operator);
                    mLLCaseSchema.setUWDate(aDate);
                    mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
                    mReturnMessage = "账单录入到理赔结案不能是同一个理赔人员，请等待上级进行审定，审定人:" +
                                     mLLClaimUWMainSchema.getAppClmUWer();
        		}
        	}else {
        		//用户具有结案权限
                //设置核赔结论为赔付结论
                mLLClaimUWMainSchema.setClmDecision(mClaimGiveType);
                //设置案件状态为[结案状态]
                mLLCaseSchema.setRgtState("09");
                mLLCaseSchema.setUWer(mG.Operator);
                mLLCaseSchema.setUWDate(aDate);
                mLLCaseSchema.setSigner(mG.Operator);
                mLLCaseSchema.setSignerDate(aDate);
                mReturnMessage = "审批完毕,已经结案";
        	}       	
            
            //没有结案权限，并且是特需，不管是否为黑名单都走上报，只是提示语不同，如果为黑名单后续代码为黑名单提示语，如果不是则走正常上报提示语
            //特需险并且有结案权限，并且为黑名单,并且不是最高级走上报
        } else if((IsSpecialNeed && !IsPower) || (!"false".equals(boolblack) && !"true".equals(boolupuser) && IsSpecialNeed &&  IsPower  ) ){
            //用户没有结案权限，上报普通案件上级处理    	
        	if (mUpUserCode == null ||"".equals(mUpUserCode)) {
                CError.buildErr(this, "该用户无权审定该案件，且无上级，案件上报失败！");
                return false;
            }
        	
			if (!"1".equals(mUpStateFlag)) {
				CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");
				return false;
			}
        	
            mLLClaimUWMainSchema.setAppGrade(mUpClaimPopedom); //申请级别
            mLLClaimUWMainSchema.setAppClmUWer(mUpUserCode); //申请审定人员
            mLLClaimUWMainSchema.setAppActionType("3"); //申请动作 ** 待定 **
            //设置案件状态为[审批状态]
            mLLCaseSchema.setRgtState("05");
            mLLCaseSchema.setUWer(mG.Operator);
            mLLCaseSchema.setUWDate(aDate);
            mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
            mReturnMessage = "审批完毕,您权限不够,请等待上级审定,审定人:" +
                             mLLClaimUWMainSchema.getAppClmUWer();
        }
		return true;
	}

    /**
     * 用户的特需权限校验
     * 
     */
//	private boolean checkSNUserRight(double aRealPay) {
//		
//		System.out.println("<-Go Into checkSNUserRight()->");
//		
//		
//                
//        return true;
//    
//	}

	/**
     * 循环处理保单核赔-审定->结案
     * @return
     */
    private boolean SDUW() {
        //判断用户是否是审批时指定的上级审定人
        if (!checkUserSD()) {
            return false;
        }
        boolean IsPower = true;
        boolean DSFlag = true;
        LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
        tLLClaimPolicyDB.setClmNo(mClmNo);
        LLClaimPolicySet tLLClaimPolicySet = tLLClaimPolicyDB.query();
        if (tLLClaimPolicySet.size() <= 0) {
            CError.buildErr(this, "赔案明细表查询失败，没有理算结果！");
            return false;
        }
        
        //TODO: 首先循环判断赔案明细中是否只有特需险种：1）只有特需且有特需权限，走特需险种的权限；2）含有其他险种，走原有权限
    	String tSpecialNeedSql = "select 1 from llclaimpolicy where clmno='"+mClmNo+"' " +
    			"and riskcode in (select riskcode from lmriskapp where risktype3='7') ";
    	ExeSQL tSpecialNeedExe = new ExeSQL();
    	SSRS tSpecialNeedSSRS = new SSRS();
    	tSpecialNeedSSRS = tSpecialNeedExe.execSQL(tSpecialNeedSql);
    	
    	LLClaimUserSchema tLLClaimUserSchema = getLLClaimUserSchema(mG.Operator);
    	
        //  # 2739 <补充A> **start**
    	String aSpecialSQL ="select distinct 1 from llclaimdetail  where clmno='"+mClmNo+"' and riskcode ='170501' "+
    			"and not exists(select 1 from llclaimdetail where caseno='"+mCaseNo+"'and getdutykind='500' and getdutycode = '701204') with ur";
    	ExeSQL aSpecialExe = new ExeSQL();
    	SSRS aSpecialSSRS =aSpecialExe.execSQL(aSpecialSQL);
    	// #2739 <补充A> **end**
    	
    	// # 3770 贴心管家险种  **start**
    	String TXGJSQL = "select distinct 1 from llclaimpolicy where 1=1 and caseno = '" +mCaseNo +  
    					"' and riskcode not in (select riskcode from lmriskapp where risktype3='7') and riskcode not in ('690101')";
    	ExeSQL TXGJExeSQL = new ExeSQL();
    	SSRS tss = TXGJExeSQL.execSQL(TXGJSQL);
    	int Number = tss.getMaxRow();
    	//  # 3770 贴心管家险种  **end**
    	
    	//3997 电商业务系统配置功能判断 start
    	int number1 = 0;
    	int number2 = 0;
    	for (int i = 1; i <= tLLClaimPolicySet.size(); i++) {
    		LLClaimPolicySchema sLLClaimPolicySchema = tLLClaimPolicySet.get(i);
    		String SocialSQL = "select lc.salechnl from llclaimpolicy ll ,lccont lc where 1=1 "
    				+ " and ll.contno = lc.contno and lc.salechnl is not null and ll.contno = '" 
    				+sLLClaimPolicySchema.getContNo() + "' and ll.caseno = '" + mCaseNo + "' with ur";
    		ExeSQL SocialExeSQL = new ExeSQL();
    		SSRS SocialSSRS = SocialExeSQL.execSQL(SocialSQL);
    		int length = SocialSSRS.getMaxRow();
    		if(length > 0) {
    			String Salechnl = SocialSSRS.GetText(1, 1);
    			if("21".equals(Salechnl) || "22".equals(Salechnl)) {
    				number1++;
    			}else {
    				number2++;
    			}
    		}
    	}
    	if(number1 > 0 && number2 == 0) {
    		//销售渠道是全为电商走电商逻辑
    		DSFlag = true;
    	}else if(number1>0 && number2>0){
    		//即含有电商业务又含有非电商业务需要判断电商和普通的权限大小
    		String tmmDSNeedFlag = tLLClaimUserSchema.getDSNeedFlag();
            if("1".equals(tmmDSNeedFlag)) {            	
            	try{
            		//根据审批权限进行判断用户级别的大小
            		String DSClaimPopedom = tLLClaimUserSchema.getDSNeedLimitPopedom();
            		//目前只针对单个字母的权限，若以后需要扩展权限在此修改
            		if(DSClaimPopedom.length()!=1 && mClaimPopedom.length()!=1) {
            			mReturnMessage = "权限级别异常";
                		return false;
            		}
            		char ch1 = DSClaimPopedom.charAt(0);
            		int num1 = (Integer)(ch1-64);
            		char ch2 = mClaimPopedom.charAt(0);
            		int num2 = (Integer)(ch2-64);
            		if(num1>=num2) {
            			//电商权限大走普通逻辑
            			DSFlag = false;
            		}else {
            			//电商权限小走普通逻辑
            			DSFlag = true;
            		}
            	}catch(Exception e) {
            		e.printStackTrace();
            		mReturnMessage = "权限级别转换异常";
            		return false;
            	}	
            }else {
            	DSFlag = false;
            }
    	}else if(number1 == 0 && number2 > 0) {
    		//销售渠道没有电商走普通逻辑
    		DSFlag = false;
    	}else {
    		//没有销售渠道走普通逻辑
    		DSFlag = false;
    	}
    	//3997 电商业务系统配置功能判断end
    	
    	if(tSpecialNeedSSRS != null && tSpecialNeedSSRS.getMaxRow() > 0 
    			&& tSpecialNeedSSRS.getMaxRow() == tLLClaimPolicySet.size()
    			&& "1".equals(tLLClaimUserSchema.getSpecialNeedFlag())){//有特需险，且全部为特需险
    		checkSpecialNeedSD(tLLClaimUserSchema);

    	}
    	else if(mG.ManageCom.length()>=4 && ("8632").equals(mG.ManageCom.substring(0, 4)) 
    			&& Number!=1) {
    		checkSpecialNeedSD(tLLClaimUserSchema);	//江苏以及江苏分公司的贴心管家理赔案件处理，#3770走特需
    	}
    	else if(aSpecialSSRS != null && aSpecialSSRS.getMaxRow() >0     	//  # 2739 <补充A>走特需
    			&& aSpecialSSRS.getMaxRow() == tLLClaimPolicySet.size()){
    		checkSpecialNeedSD(tLLClaimUserSchema);
    	}
    	else if("1".equals(mSocialSecurity)){//TODO:添加社保权限的判断，只校验金额
    		LLSocialClaimUserSchema tLLSocialClaimUserSchema = getLLSocialClaimUserSchema(mG.Operator);
    		if(!checkSocialSecuritySD(tLLSocialClaimUserSchema)){//不符合社保校验，结束审批
    			return false;
    		}
    	}
    	else if(DSFlag){//添加电商审批权限校验
    		if(!checkDSSecuritySD(tLLClaimUserSchema)) {
    			return false;
    		}
    	}else{ 
	        for (int i = 1; i <= tLLClaimPolicySet.size(); i++) {
	            String strGiveType = tLLClaimPolicySet.get(i).getGiveType(); //给付结论
	            if (strGiveType == null) {
	                CError.buildErr(this, "保单无赔付结论,请先做理算确认！");
	                return false;
	            }
	            if (!checkUserRight(tLLClaimPolicySet.get(i))) {
	                IsPower = false;
	                break;
	            }
	        }
	        if (((IsPower)&& ("false".equals(boolblack))) || (!"false".equals(boolblack) && "true".equals(boolupuser) ) ) {
	            //签批通过，设置案件状态为[审定状态]
	            if (mOperate.equals("BATCH||UW") || mRemarkSD == null ||
	                mRemarkSD.equals("")) {
	                mLLCaseSchema.setRgtState("09");
	                mLLCaseSchema.setEndCaseDate(aDate);
	            } else {
	                mLLCaseSchema.setRgtState("06");
	            }
	            mLLCaseSchema.setSigner(mLLClaimUWMainSchema.getAppClmUWer());
	            mLLCaseSchema.setSignerDate(aDate);
	            mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getClmUWer());
	            mReturnMessage = "审定完毕,已送交原审批人处理";
	            if (mOperate.equals("BATCH||UW") || mRemarkSD == null ||
	                mRemarkSD.equals("")) {
	                mReturnMessage = "审定完毕";
	            }
	        } else {
	            //无权限,继续上报
	            if (mUpUserCode == null||"".equals(mUpUserCode)) {
	                CError.buildErr(this, "该用户无权审定该案件，且无上级，案件上报失败！");
	                return false;
	            }
	            
	            if (!"1".equals(mUpStateFlag)) {
					CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");
					return false;
				}
	            
	            mLLClaimUWMainSchema.setAppGrade(mUpClaimPopedom); //申请级别
	            mLLClaimUWMainSchema.setAppClmUWer(mUpUserCode); //申请审定人员
	            mLLClaimUWMainSchema.setAppActionType(""); //申请动作 ** 待定 **
	            mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
	            mReturnMessage = "审定完毕,您权限不够,已送交上级审定,审定人:" +
	                             mLLClaimUWMainSchema.getAppClmUWer();
	        }
    	}
	        if(!"false".equals(boolblack) && !"1".equals(mSocialSecurity)){ //黑名单并且非社保
	        	 if( "true".equals(boolupuser)){//如果上级为空，并且有效，说明当前用户为最高权限人,并且是黑名单
	        		 
	        	 }else{
	        		 	if("true_lcinsured".equals(boolblack)){
			            	mReturnMessage = "该案件被保险人为外国政要、国际组织高级管理人员及其特定关系人，报上级确认该被保险人身份信息";
			    	        
			            }
	        		 	if("true_llbnf30".equals(boolblack)){
				            	mReturnMessage = "该案件身故受益人为外国政要、国际组织高级管理人员及其特定关系人，报上级确认该被保险人身份信息";
				    	        
				        }
				        if("true_llbnf31".equals(boolblack)){
				            	mReturnMessage = "该身故受益人为外国政要、国际组织高级管理人员及其特定关系人，报上级确认该被保险人身份信息";
				    	        
				        }
	        		 
	        	 }
	        }
   	//黑名单，如果不是最高权限，则需要处理
        fillOpTime("06", mG.Operator);

        //添加案件核赔履历记录...
        //获取履历最大序号
        LLClaimUWMDetailDB tLLClaimUWMDetailDB = new LLClaimUWMDetailDB();
        tLLClaimUWMDetailDB.setClmNo(mClmNo);
        int tCount = tLLClaimUWMDetailDB.getCount();
        tCount++;

        LLClaimUWMDetailSchema aClmUWMDetailSchema = new LLClaimUWMDetailSchema();
        Reflections ref = new Reflections();
        ref.transFields(aClmUWMDetailSchema, mLLClaimUWMainSchema);
        aClmUWMDetailSchema.setRemark(mLLClaimUWMainSchema.getRemark2());
        aClmUWMDetailSchema.setClmUWNo(String.valueOf(tCount));
        aClmUWMDetailSchema.setOperator(mG.Operator);
        aClmUWMDetailSchema.setMngCom(mG.ManageCom);
        aClmUWMDetailSchema.setMakeDate(aDate);
        aClmUWMDetailSchema.setMakeTime(aTime);
        aClmUWMDetailSchema.setModifyDate(aDate);
        aClmUWMDetailSchema.setModifyTime(aTime);

        map.put(aClmUWMDetailSchema, "INSERT");
        return true;
    }

    /**
     * 审定时，社保案件的数据存储与判断
     * @param aLLSocialClaimUserSchema
     * @return
     */
    private boolean checkSocialSecuritySD(LLSocialClaimUserSchema aLLSocialClaimUserSchema) {

    	System.out.println("<-Go Into checkSocialSecuritySD()->");
    	
    	//是否有权限的标志，true-有权限
        boolean IsPower = true;
        //社保案件特殊标志 
        boolean IsSocialSecurity = true;
    	//社保业务结案金额
        double tSocialMoney = aLLSocialClaimUserSchema.getSocialMoney();
        //社保上级
        String tUpUserCode = aLLSocialClaimUserSchema.getUpUserCode();
        LLSocialClaimUserSchema tLLSocialClaimUserSchema = getLLSocialClaimUserSchema(tUpUserCode);
        //社保上级权限
        String tUpClaimPopedom = tLLSocialClaimUserSchema.getClaimPopedom();
        //社保上级状态
        String tUpStateFlag = tLLSocialClaimUserSchema.getStateFlag();
    	
    	LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
        tLLClaimPolicyDB.setClmNo(mClmNo);
        LLClaimPolicySet tLLClaimPolicySet = tLLClaimPolicyDB.query();
        
        //社保案件层次实赔金额
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setClmNo(mClmNo);
        LLClaimSet tLLClaimSet = tLLClaimDB.query();
        //实赔金额，需要取到案件级别
        double tRealPay = 0.0;        
        for (int i = 1; i <= tLLClaimSet.size(); i++) {
            LLClaimSchema tCPolSchema = tLLClaimSet.get(i);    
            //实赔金额，需要取到案件级别
            tRealPay = tCPolSchema.getRealPay();
            mSocialSecurityRealPay = tRealPay;
            System.out.println("社保案件实赔金额："+tRealPay);
        }
        
        for (int i = 1; i <= tLLClaimPolicySet.size(); i++) {

            LLClaimPolicySchema tCPolSchema = tLLClaimPolicySet.get(i);
            if (tCPolSchema.getGiveType() == null) {
                CError.buildErr(this, "保单无赔付结论,请先做理算确认！");
                return false;
            }
            Reflections trf = new Reflections();
            LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new
                    LLClaimUnderwriteSchema();
            trf.transFields(tLLClaimUnderwriteSchema, tCPolSchema);
            
            System.out.println("社保案件险种"+tCPolSchema.getRiskCode()+"的实赔金额："+tRealPay);
            if (tSocialMoney < tRealPay && !"".equals(tUpUserCode) && tUpUserCode != null) {//社保超权限且有社保上级
            	//上报社保上级
                //用户没有结案权限，上报处理    
            	
				if (!"1".equals(tUpStateFlag)) {
					CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");
					return false;
				}
            	
                mLLClaimUWMainSchema.setAppGrade(tUpClaimPopedom); //申请级别
                mLLClaimUWMainSchema.setAppClmUWer(tUpUserCode); //申请审定人员
                mLLClaimUWMainSchema.setAppActionType(""); //申请动作 ** 待定 **
                mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
                mReturnMessage = "审定完毕,您权限不够,已送交上级审定,审定人:" +
                                 mLLClaimUWMainSchema.getAppClmUWer();
                return true;

            }else if(tSocialMoney < tRealPay && ("".equals(tUpUserCode) || tUpUserCode == null)){
            	//上报失败
            	IsPower = false;
            	IsSocialSecurity = false;
            	CError.buildErr(this, "该用户无权审定该社保案件，且无上级，案件上报失败！");
                break;
            }
        }
       
        if ((IsPower && ("false".equals(boolblack)))  || (!"false".equals(boolblack) && "true".equals(boolupuser))) {
            //签批通过，设置案件状态为[审定状态]
            if (mOperate.equals("BATCH||UW") || mRemarkSD == null ||
                mRemarkSD.equals("")) {
                mLLCaseSchema.setRgtState("09");
                mLLCaseSchema.setEndCaseDate(aDate);
            } else {
                mLLCaseSchema.setRgtState("06");
            }
            mLLCaseSchema.setSigner(mLLClaimUWMainSchema.getAppClmUWer());
            mLLCaseSchema.setSignerDate(aDate);
            mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getClmUWer());
            mReturnMessage = "审定完毕,已送交原审批人处理";
            if (mOperate.equals("BATCH||UW") || mRemarkSD == null ||
                mRemarkSD.equals("")) {
                mReturnMessage = "审定完毕";
            }
        } else {
            //无权限,继续上报，依然上报社保上级
            if (!IsSocialSecurity) {
            	
            	CError.buildErr(this, "该用户无权审定该社保案件，且无上级，案件上报失败！");
                return false;
            }
            
            if (!"1".equals(mUpStateFlag)) {
				CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");
				return false;
			}
            
            mLLClaimUWMainSchema.setAppGrade(mUpClaimPopedom); //申请级别
            mLLClaimUWMainSchema.setAppClmUWer(mUpUserCode); //申请审定人员
            mLLClaimUWMainSchema.setAppActionType(""); //申请动作 ** 待定 **
            mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
            mReturnMessage = "审定完毕,您权限不够,已送交上级审定,审定人:" +
                             mLLClaimUWMainSchema.getAppClmUWer();
        }
        
		return true;
    }
    /**
     * 审定时用户的特需数据存储与权限判断-false:
     * @param 
     * @return
     */
    private boolean checkSpecialNeedSD(LLClaimUserSchema aLLClaimUserSchema) {
    	System.out.println("<-Go Into checkSpecialNeedSD()->");
    	
    	//是否有权限的标志，true-有权限
        boolean IsPower = true;        
    	//特需权限金额
        double tSpecialNeedLimit = aLLClaimUserSchema.getSpecialNeedLimit();
        //特需上级
        String tSpecialNeedUpUserCode = aLLClaimUserSchema.getSpecialNeedUpUserCode();
    	LLClaimUserSchema tLLClaimUserSchema = getLLClaimUserSchema(tSpecialNeedUpUserCode);
        //特需上级权限
        String tUpClaimPopedom = tLLClaimUserSchema.getClaimPopedom();
    	
    	LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
        tLLClaimPolicyDB.setClmNo(mClmNo);
        LLClaimPolicySet tLLClaimPolicySet = tLLClaimPolicyDB.query();
        
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setClmNo(mClmNo);
        LLClaimSet tLLClaimSet = tLLClaimDB.query();        
        //实赔金额
        double tRealPay = 0.0;
        for (int i = 1; i <= tLLClaimSet.size(); i++) {
            LLClaimSchema tCPolSchema = tLLClaimSet.get(i);    
            //实赔金额，需要取到案件级别
            tRealPay = tCPolSchema.getRealPay();
            System.out.println("特需案件实赔金额："+tRealPay);
        }
        
        for (int i = 1; i <= tLLClaimPolicySet.size(); i++) {

            LLClaimPolicySchema tCPolSchema = tLLClaimPolicySet.get(i);
            if (tCPolSchema.getGiveType() == null) {
                CError.buildErr(this, "保单无赔付结论,请先做理算确认！");
                return false;
            }
            Reflections trf = new Reflections();
            LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new
                    LLClaimUnderwriteSchema();
            trf.transFields(tLLClaimUnderwriteSchema, tCPolSchema);
            
            System.out.println("案件特需险种"+tCPolSchema.getRiskCode()+"的实赔金额："+tRealPay);
            if (tSpecialNeedLimit < tRealPay && !"".equals(tSpecialNeedUpUserCode) && tSpecialNeedUpUserCode != null) {//特需超权限且有特需上级
            	//上报特需上级
                //用户没有结案权限，上报处理       	
                mLLClaimUWMainSchema.setAppGrade(tUpClaimPopedom); //申请级别
                mLLClaimUWMainSchema.setAppClmUWer(tSpecialNeedUpUserCode); //申请审定人员
                mLLClaimUWMainSchema.setAppActionType(""); //申请动作 ** 待定 **
                mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
                mReturnMessage = "审定完毕,您权限不够,已送交上级审定,审定人:" +
                                 mLLClaimUWMainSchema.getAppClmUWer();
                return true;

            }else if(tSpecialNeedLimit < tRealPay && ("".equals(tSpecialNeedUpUserCode) || tSpecialNeedUpUserCode == null)){
            	//走普通上报上级
            	IsPower = false;
                break;
            }
        }
        if ((IsPower && ("false".equals(boolblack)))  || (!"false".equals(boolblack) && "true".equals(boolupuser) )) {
            //签批通过，设置案件状态为[审定状态]
            if (mOperate.equals("BATCH||UW") || mRemarkSD == null ||
                mRemarkSD.equals("")) {
                mLLCaseSchema.setRgtState("09");
                mLLCaseSchema.setEndCaseDate(aDate);
            } else {
                mLLCaseSchema.setRgtState("06");
            }
            mLLCaseSchema.setSigner(mLLClaimUWMainSchema.getAppClmUWer());
            mLLCaseSchema.setSignerDate(aDate);
            mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getClmUWer());
            mReturnMessage = "审定完毕,已送交原审批人处理";
            if (mOperate.equals("BATCH||UW") || mRemarkSD == null ||
                mRemarkSD.equals("")) {
                mReturnMessage = "审定完毕";
            }
        } else {
            //无权限,继续上报
            if (mUpUserCode == null||"".equals(mUpUserCode)) {
                CError.buildErr(this, "该用户无权审定该案件，且无上级，案件上报失败！");
                return false;
            }
            
            if (!"1".equals(mUpStateFlag)) {
				CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");
				return false;
			}
            
            mLLClaimUWMainSchema.setAppGrade(mUpClaimPopedom); //申请级别
            mLLClaimUWMainSchema.setAppClmUWer(mUpUserCode); //申请审定人员
            mLLClaimUWMainSchema.setAppActionType(""); //申请动作 ** 待定 **
            mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
            mReturnMessage = "审定完毕,您权限不够,已送交上级审定,审定人:" +
                             mLLClaimUWMainSchema.getAppClmUWer();
        }
        
		return true;
	}

	/**
     * 审定完毕后，返回原处理人结案，或告知其案件需要回退
     * @return boolean
     */
    private boolean backUW() {
        //判断当前用户是否为立案时指定的审批人
        if (!checkUserSP()) {
            return false;
        }
        
        //由于社保存在抽检金额，在返回原处理人签批时，需要再次取下本次案件的实赔金额
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setClmNo(mClmNo);
        LLClaimSet tLLClaimSet = tLLClaimDB.query();
        
        for (int i = 1; i <= tLLClaimSet.size(); i++) {
            LLClaimSchema tCPolSchema = tLLClaimSet.get(i);    
            //实赔金额，需要取到案件级别
            double tRealPay = tCPolSchema.getRealPay();
            mSocialSecurityRealPay = tRealPay;
            System.out.println("社保案件实赔金额："+tRealPay);
        }
        
        //读取页面上审定结论
        String sDecisionSD = mLLClaimUWMainSchema.getcheckDecision2();
        if (sDecisionSD == null || sDecisionSD.equals("")) {
            CError.buildErr(this, "没有审定结论！");
            return false;
        }
        //读取上级审定结论
        //查询该案件既往核赔信息（案件有回退、上报等操作）
        LLClaimUWMainDB mLLClaimUWMainDB = new LLClaimUWMainDB();
        mLLClaimUWMainDB.setClmNo(mClmNo);
        LLClaimUWMainSchema tLLClaimUWMainSchema = new LLClaimUWMainSchema();
        if (mLLClaimUWMainDB.getInfo()) {
            tLLClaimUWMainSchema.setSchema(mLLClaimUWMainDB.getSchema());
        } else {
        	CError.buildErr(this, "没有上级审定结论！");
            return false;
        }
        String ssDecisionSD = tLLClaimUWMainSchema.getcheckDecision2();

        //如果审定结论为[同意],设置案件状态为[结案状态],进行结案处理
        //由于此处结论为页面处展示结论，本次优先判断上级给出的结论
        if(sDecisionSD.equals(ssDecisionSD)){
            if (sDecisionSD.equals("1")) {
                //更新核赔结论为赔付结论
                mLLClaimUWMainSchema.setClmDecision(mClaimGiveType);

                //设置案件状态为[结案状态]
                mLLCaseSchema.setRgtState("09");
                mReturnMessage = "审批完毕,上级审定人同意您上次的审批结论,已经结案！";
            } else {
                CError.buildErr(this, "审批完毕，上级审定人不同意您上次的审批结论，"
                                + "应交由上级把案件退回重新处理！");
                return false;
            }
        }else{
        	CError.buildErr(this, "审批完毕，上级审定人不同意您上次的审批结论，"
                    + "应交由上级把案件退回重新处理！");
        	return false;
        }

        return true;
    }

    /**
     * 结案抽检
     * @return boolean
     */
    private boolean SpotClaim() {
        //如果无上级则不能被抽检
        if (mUpUserCode == "" || mUpUserCode == null) {
            return false;
        }

        LDSpotTrackSet tLDSpotTrackSet = new LDSpotTrackSet();
        LDSpotTrackSet tMLDSpotTrackSet = new LDSpotTrackSet();

        SpotPrepare tSpotPrepare = new SpotPrepare();
        //由于添加了社保抽检，此处需要判断核赔类型,社保为20000
        String tUWType = "";
        if("1".equals(mSocialSecurity)){
        	tUWType = "20000";
        }else{
        	tUWType = "30000";
        }
        
        // 3067 抽检规则调整 **start**
        String tCustomerName=""  ;//客户名字
        String tAccName="" ; //银行账户名
        String tDeathDate="";//死亡日期
        String tCaseGetMode="";//领取方式
        ExeSQL tExeSQL = new ExeSQL();
        String tSamplingRule = "select togetherFlag from llregister where 1=1 and rgtno='"+ mRgtNo +"' with ur ";
        String tRule= tExeSQL.getOneValue(tSamplingRule);
        String tName="select c.customername,c.accname ,c.deathdate,c.casegetmode from llcase c where 1=1  " +
            		"and c.caseno='"+ mCaseNo +"' with ur";
        SSRS tSSRS = tExeSQL.execSQL(tName);
        if(tSSRS.getMaxRow()>0){
        	tCustomerName=tSSRS.GetText(1, 1);
            tAccName =tSSRS.GetText(1, 2);
            tDeathDate=tSSRS.GetText(1,3);
            tCaseGetMode=tSSRS.GetText(1, 4);
         }
        String tResault ="select distinct 1 from llclaimdetail where caseno='"+ mCaseNo +"' and givetype='1' and givereason='03' with ur" ;
        String tGive=tExeSQL.getOneValue(tResault);
        
        // #3279 案件赔款领取人姓名校验特定险种抽检比例调整需求 start 
		String tRiskCodeSql = "select distinct riskcode from llclaimdetail where caseno = '"+mCaseNo+"'";
		String tRiskCode = tExeSQL.getOneValue(tRiskCodeSql);
		SpotPrepareClaim tSpotPrepareClaim = new SpotPrepareClaim();
		boolean tIsCheck = false; // 抽检选中标志
		String tSign; // 抽检比例标志，1为10%，2为20%
		// #3279 案件赔款领取人姓名校验特定险种抽检比例调整需求 end
        
 
        //操作员为yun003不抽检
        if(mG.Operator.equals("yun003")){
        	
        	return false;
        	
        }
        //#3663 仅有一人处理案件
    	else if("1".equals(tSumOperator)&&mG.Operator.equals(tHandler)){
    		if (mUpUserCode == null||"".equals(mUpUserCode)) {
                CError.buildErr(this, "该用户无权审定该案件，且无上级，案件上报失败！");
                return false;
            }
			
			if (!"1".equals(mUpStateFlag)) {
				CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");

				return false;
			}
			// 保存轨迹
			//随机抽取轨迹表
	        LDSpotTrackSchema tLDSpotTrackSchema = new LDSpotTrackSchema();

	        //获取抽取批次号
	        String tSpotID = PubFun1.CreateMaxNo("SpotCheck", 20);

	        tLDSpotTrackSchema.setSpotID(tSpotID);
	        tLDSpotTrackSchema.setOtherNo(mCaseNo);
	        tLDSpotTrackSchema.setOtherType("CaseNo");
	        tLDSpotTrackSchema.setSpotType("");
	        tLDSpotTrackSchema.setMakeDate(PubFun.getCurrentDate());
	        tLDSpotTrackSchema.setMakeTime(PubFun.getCurrentTime());
	        tLDSpotTrackSchema.setSpotFlag("1");
			map.put(tLDSpotTrackSchema, "DELETE&INSERT");
    	}

        //对于社保的案件抽检，若实赔金额超过社保的抽检金额，直接被抽检
        else if("1".equals(mSocialSecurity) && mSocialSecurityRealPay > mSocialSecuritySpotLimit){
        	if (mUpUserCode == null||"".equals(mUpUserCode)) {
                CError.buildErr(this, "该用户无权审定该社保案件，且无上级，案件上报失败！");
                return false;
            }
			
			if (!"1".equals(mUpStateFlag)) {
				CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");
				return false;
			}
			// 保存轨迹
			//随机抽取轨迹表
	        LDSpotTrackSchema tLDSpotTrackSchema = new LDSpotTrackSchema();

	        //获取抽取批次号
	        String tSpotID = PubFun1.CreateMaxNo("SpotCheck", 20);

	        tLDSpotTrackSchema.setSpotID(tSpotID);
	        tLDSpotTrackSchema.setOtherNo(mCaseNo);
	        tLDSpotTrackSchema.setOtherType("CaseNo");
	        tLDSpotTrackSchema.setSpotType("");
	        tLDSpotTrackSchema.setMakeDate(PubFun.getCurrentDate());
	        tLDSpotTrackSchema.setMakeTime(PubFun.getCurrentTime());
	        tLDSpotTrackSchema.setSpotFlag("1");
			
			map.put(tLDSpotTrackSchema, "DELETE&INSERT");
        }
        // #3279 案件赔款领取人姓名校验特定险种抽检比例调整需求 start 
        // 针对特定险种在受理界面、审批审定界面校验案件赔款领取人姓名与被保险人不一致时，抽检比例调整为10%；其他险种抽检比例调整为20%
		else if (("1".equals(tRule) && !tCustomerName.equals(tAccName) && !"1".equals(mSocialSecurity) && (!tCaseGetMode.equals("1") && !tCaseGetMode.equals("2"))) 
					|| ("".equals(tRule) && !tCustomerName.equals(tAccName) && !"1".equals(mSocialSecurity) && (!tCaseGetMode.equals("1") && !tCaseGetMode.equals("2")))) {
			if ("520701".equals(tRiskCode) || "220401".equals(tRiskCode) || "121901".equals(tRiskCode)
					|| "121801".equals(tRiskCode) || "121701".equals(tRiskCode) || "290101".equals(tRiskCode)) {
				tSign = "1";
			} else {
				tSign = "2";
			}
			tIsCheck = tSpotPrepareClaim.PrepareData(mCaseNo, "CaseNo", false, tSign);
			// 抽中则校验上级并插入轨迹，没抽中直接结案
			if (tIsCheck) {
				if (mUpUserCode == null||"".equals(mUpUserCode)) {
					CError.buildErr(this, "该用户无权审定该案件，且无上级，案件上报失败！");
					return false;
				}
				if (!"1".equals(mUpStateFlag)) {
					CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");
					return false;
				}
				map.put(tSpotPrepareClaim.getLDSpotTrackSet(), "DELETE&INSERT");
			} else {
				return false;
			}
		}
		// #3279 案件赔款领取人姓名校验特定险种抽检比例调整需求 end
        //对于客户姓名和银行账户名不一致 或理算结论为协议约定通融给付直接抽检
        else if(("1".equals(tRule) && !tCustomerName.equals(tAccName) && tDeathDate.equals("")&& !"1".equals(mSocialSecurity)&&(!tCaseGetMode.equals("1")&&!tCaseGetMode.equals("2"))) ||
        		 (tRule.equals("")&& !tCustomerName.equals(tAccName)&& tDeathDate.equals("")&& !"1".equals(mSocialSecurity) &&(!tCaseGetMode.equals("1")&&!tCaseGetMode.equals("2")))
        		||(tCustomerName.equals(tAccName) &&("1".equals(tGive)) && !"1".equals(mSocialSecurity))){
			//如果不重复抽取，则执行轨迹表查询
			boolean cFlag=false;
	        if (!cFlag)
	        {
	            LDSpotTrackDB tLDSpotTrackDB = new LDSpotTrackDB();
	            tLDSpotTrackDB.setOtherNo(mCaseNo);
	            tLDSpotTrackDB.setOtherType("CaseNo");
	            //如果轨迹表中存在，则返回false
	            if (tLDSpotTrackDB.query().size() > 0)
	            {
	                return false;
	            }
	        }
			
			//随机抽取的轨迹表
			LDSpotTrackSchema mLDSpotTrackSchema = new LDSpotTrackSchema();
			//获取抽取批次号
			String mSpotID = PubFun1.CreateMaxNo("SpotCheck", 20);
			mLDSpotTrackSchema.setSpotID(mSpotID);
			mLDSpotTrackSchema.setOtherNo(mCaseNo);
			mLDSpotTrackSchema.setOtherType("CaseNo");
	        mLDSpotTrackSchema.setSpotType("");
	        mLDSpotTrackSchema.setMakeDate(PubFun.getCurrentDate());
	        mLDSpotTrackSchema.setMakeTime(PubFun.getCurrentTime());
	        mLDSpotTrackSchema.setSpotFlag("1");
	        
	        map.put(mLDSpotTrackSchema, "DELETE&INSERT");
	
		//#3067 抽检规则调整**end**
		
        
    }	
        
        else if (!tSpotPrepare.PrepareData(mCaseNo, mG.Operator, mClaimPopedom,
        		tUWType, "CaseNo", false)) {
            //没有被抽中
            String strSql =
                    "select distinct a.caseno from llclaimdetail a where a.caseno='" +
                    mCaseNo + "'"
                    + " and a.GiveType='1' and a.GiveReason in('04','05','06','07') ";
            LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
            LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
            tLLClaimDetailSet = tLLClaimDetailDB.executeQuery(strSql);

            if (tLLClaimDetailSet.size() > 0) {
                tLDSpotTrackSet = tSpotPrepare.getLDSpotTrackSet();
                //已经抽检过的案件，不会有返回值
                if (tLDSpotTrackSet.size() <= 0) {
                    return false;
                }
                
                if (mUpUserCode == null||"".equals(mUpUserCode)) {
                    CError.buildErr(this, "该用户无权审定该案件，且无上级，案件上报失败！");
                    return false;
                }
    			
    			if (!"1".equals(mUpStateFlag)) {
					String tMaxClaimPopedomSQL = "select 1 from llclaimuser where usercode='"
							+ mUpUserCode
							+ "' and claimpopedom = (select max(claimpopedom) from llclaimpopedom)";
					ExeSQL tExeSQl = new ExeSQL();
					if ("1".equals(tExeSQl.getOneValue(tMaxClaimPopedomSQL))) {
						return false;
					}
    				CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");
    				return false;
    			} 
                
                for (int i = 1; i <= tLDSpotTrackSet.size(); i++) {
                    LDSpotTrackSchema tLDSpotTrackSchema = tLDSpotTrackSet.get(
                            i);
                    tLDSpotTrackSchema.setSpotFlag("1");
                    tMLDSpotTrackSet.add(tLDSpotTrackSchema);
                    //保存轨迹
                    map.put(tMLDSpotTrackSet, "DELETE&INSERT");

                }
            } 
            else 
            {
                return false;
            }
		} else {
			if (mUpUserCode == null||"".equals(mUpUserCode)) {
                CError.buildErr(this, "该用户无权审定该案件，且无上级，案件上报失败！");
                return false;
            }
			
			if (!"1".equals(mUpStateFlag)) {
				CError.buildErr(this, "上级用户" + mUpUserCode + "已失效，请与总公司联系");
				return false;
			}
			// 保存轨迹
			map.put(tSpotPrepare.getLDSpotTrackSet(), "DELETE&INSERT");
		}
        mLLClaimUWMainSchema.setAppClmUWer(mUpUserCode);
        mLLClaimUWMainSchema.setAppGrade(mUpClaimPopedom);
        mReturnMessage = "该案件被抽检，请等待上级审定，审定人：" +
                         mLLClaimUWMainSchema.getAppClmUWer();

        mLLCaseSchema.setRgtState("10");
        mLLCaseSchema.setEndCaseDate("");
        mLLCaseSchema.setUWFlag("6"); //给抽检案件做标记
        mLLCaseSchema.setCaseNoDate(aDate);
        mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
        fillOpTime("10", mUpUserCode);

        return true;
    }

    /**
     * 抽检/稽查审定，以抽检人下的核赔结论为准，无须再判断审定人是否有权限签批
     * @return boolean
     */
    private boolean SpotUW(String aRgtState) {
        //判断用户是否是抽检时指定的上级审定人
        if (!checkUserSD()) {
            return false;
        }

        //读取审定结论
        String sDecisionSD = mLLClaimUWMainSchema.getcheckDecision2();
        if (sDecisionSD == null || sDecisionSD.equals("")) {
            CError.buildErr(this, "请先给出审定结论，再做确认。！");
            return false;
        }
        //如果审定结论为[同意],设置案件状态为[结案状态],进行结案处理
        if (sDecisionSD.equals("1")) {
            //更新核赔结论为赔付结论
            mLLClaimUWMainSchema.setClmDecision(mClaimGiveType);
            if (aRgtState.equals("10")) {
                mLLCaseSchema.setRgtState("09");
                mReturnMessage = "抽检完毕,本案正式结案！";
            } else {
                mLLCaseSchema.setRgtState("12");
                mReturnMessage = "稽查完毕,本案正式结案";
            }
        } else {
            if (aRgtState.equals("10")) {
                mReturnMessage = "抽检完毕,该案件存在问题,请把案件退回重新处理!";
            } else {
                mReturnMessage = "稽查完毕,该案件存在问题,应把案件做纠错处理！";
            }
        }
        fillOpTime(aRgtState, mG.Operator);
        return true;
    }

    /**
     * 结案的后续操作：1 解除保单锁定 2 置结案时间  3 如果是团体最后一个结案，则团体结案
     * @return boolean
     */
    private boolean endCase() {
        mLLCaseSchema.setEndCaseDate(aDate); //结案日期
        fillOpTime("09", mG.Operator);
        //chgPolState();
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if (!tLLRegisterDB.getInfo()) {
            CError.buildErr(this, "团体受理信息查询失败！");
            return false;
        }
        if (("02".equals(tLLRegisterDB.getRgtState()) ||
             "01".equals(tLLRegisterDB.getRgtState())) &&
            !"1".equals(tLLRegisterDB.getApplyerType())) {
            //尚未受理完毕，不能批次结案  简易理赔的（applyertype为1）手工批次结案
            String tsql = "select count(CaseNo) from llcase where rgtno='"
                          + mRgtNo + "'";
            ExeSQL texesql = new ExeSQL();
            String count = texesql.getOneValue(tsql);
            int totalcount = Integer.parseInt(count);
            tsql = "select count(CaseNo) from llcase where rgtno='"
                   + mRgtNo + "' and rgtstate in ('09','11','12','14')";
            count = texesql.getOneValue(tsql);
            int endcount = Integer.parseInt(count);
            if (totalcount <= endcount + 1) {
                //该团体批次下所有案件都已经结案，则团体结案
                LLRegisterSchema tLLRegisterSchema = tLLRegisterDB.getSchema();
                tLLRegisterSchema.setRgtState("03");
                tLLRegisterSchema.setEndCaseDate(aDate);
//                tLLRegisterSchema.setHandler1(mG.Operator); //团体结案人
                tLLRegisterSchema.setModifyDate(aDate);
                tLLRegisterSchema.setModifyTime(aTime);
                map.put(tLLRegisterSchema, "UPDATE");
            }
        }
        return true;
    }


    private boolean prepareOutputData() {
        try {
            mInputData.add(map);
        } catch (Exception ex) {
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错:" + ex.toString());
            return false;
        }
        System.out.println("after prepareOutputData");
        return true;
    }

    /**
     * 判断当前用户是否为立案指定的审批人
     */
    private boolean checkUserSP() {
    	//modify by Houyd	20140425 社保不判断审批人
    	if("1".equals(mSocialSecurity)){
    		return true;
    	}
    	
        String sHandler = "" + mLLCaseSchema.getHandler();
        if (sHandler.equals("")) {
            CError.buildErr(this, "受理时未指定审批人！");
            return false;
        }
        if (!sHandler.trim().equals(mG.Operator.trim())) {
            CError.buildErr(this, "审批人员不符，指定审批人为：" + sHandler);
            return false;
        }
        return true;
    }

    //校验用户审定权限
    private boolean checkUserSD() {
        if (mLLClaimUWMainSchema.getAppClmUWer() == null ||
            mLLClaimUWMainSchema.getAppClmUWer().equals("")) {
            CError.buildErr(this, "审批时未指定审定人！");
            return false;
        }
        if (!mLLClaimUWMainSchema.getAppClmUWer().equals(mG.Operator.trim())) {
            CError.buildErr(this, "审定人不符！审批时指定的审定人为："
                            + mLLClaimUWMainSchema.getAppClmUWer());
            return false;
        }
        return true;
    }

    /**
     * 逐单校验核赔人核赔权限
     * true-有权限
     * 
     */
    private boolean checkUserRight(LLClaimPolicySchema aClmPolSchema) {

        String strRiskcode = aClmPolSchema.getRiskCode(); //险种代码
        String strGiveKind = getGiveKind(strRiskcode); //给付类别
        String strRealpay = String.valueOf(aClmPolSchema.getRealPay());
        String strContNo = aClmPolSchema.getContNo();//保单号
        if ("3".equals(aClmPolSchema.getGiveType()) &&
            "全额拒付".equals(aClmPolSchema.getGiveTypeDesc())) 
        {
        	String tLLFeeMain ="select count(1) from LLFeeMain where caseno = '" +mCaseNo + "' with ur";
	        ExeSQL sExeSQL = new ExeSQL();
	        String sLLFeeMain = sExeSQL.getOneValue(tLLFeeMain);//是否有账单
	        if(sLLFeeMain.equals("0"))
	        {
	        	String tRickcode ="select count(1) from llclaimdetail a,lmriskapp b where a.riskcode = b.riskcode and b.RiskType1 in ('2','5') and a.caseno = '" +mCaseNo +"' with ur";
		        ExeSQL aExeSQL = new ExeSQL();
		        String aRickcode = aExeSQL.getOneValue(tRickcode); //判断险种是否是疾病和意外险种
		        if(!aRickcode.equals("0"))
		        {
		        	String sql_DC =
		                "select DutyCode from llclaimdetail where clmno= '" +
		                aClmPolSchema.getClmNo() + "' and polno='" +
		                aClmPolSchema.getPolNo() + "' with ur";
			    	System.out.println("责任编码SQL:" + sql_DC);
			        ExeSQL tExeSQL = new ExeSQL();
			        String strDutyCode = tExeSQL.getOneValue(sql_DC); //责任编码
			        System.out.println("责任编码:" + strDutyCode);  
			     
			            String sql_rp =
			                    "select Amnt from LCDuty where DutyCode= '" +
			                    strDutyCode + "' and polno='" +
			                    aClmPolSchema.getPolNo() + "' " +
			                    "union " +
			                    "select Amnt from LBDuty where DutyCode= '" +
			                    strDutyCode + "' and polno='" +
			                    aClmPolSchema.getPolNo() + "' " +
			                    "with ur";
			            System.out.println("全额拒付的责任保额SQL:" + sql_rp);
			            ExeSQL qExeSQL = new ExeSQL();
			            strRealpay = qExeSQL.getOneValue(sql_rp); //全额拒付,责任保额
			            System.out.println("全额拒付的责任保额:" + strRealpay);
		        }
		        else
		        {
		            String sql_rp =
	                    "select DeclineAmnt from llclaimdetail where clmno= '" +
	                    aClmPolSchema.getClmNo() + "' and polno='" +
	                    aClmPolSchema.getPolNo() + "' with ur";
	            ExeSQL tExeSQL = new ExeSQL();
	            strRealpay = tExeSQL.getOneValue(sql_rp); //全额拒付金额
		        }
	        }
	        else
	        {
	            String sql_rp =
                    "select DeclineAmnt from llclaimdetail where clmno= '" +
                    aClmPolSchema.getClmNo() + "' and polno='" +
                    aClmPolSchema.getPolNo() + "' with ur";
            ExeSQL tExeSQL = new ExeSQL();
            strRealpay = tExeSQL.getOneValue(sql_rp); //全额拒付金额
	        }
        }
        if (!"".equals(strRealpay)) {
            strRealpay = Double.toString(Math.abs(Double.parseDouble(strRealpay)));
        }

		//豁免期数 freepried,给付类型 GetDutyType
        ExeSQL tExeSQL = new ExeSQL();
        String tGetDutyType = aClmPolSchema.getGiveType();
        //豁免险判断，如果是申诉纠错案件换回C的案件进行判断
        String tCaseNo = mCaseNo;
        if("R".equals(mCaseNo.substring(0,1)) || "S".equals(mCaseNo.substring(0,1)))
    		{
    				System.out.println("申诉纠错案件换为正常的C案件");
            String sql = "select caseno from LLAppeal where appealno ='"+mCaseNo+"'";
            ExeSQL exeSQL = new ExeSQL();
            tCaseNo = exeSQL.getOneValue(sql);
    		}
        String sqlHuoMian = "select freepried from LLExemption where riskcode='"+strRiskcode+"' and caseno='"+tCaseNo+"' and not exists (select 1 from lcpol where contno = '"+strContNo+"' and riskcode in('231701','333801') and payintv='0' union select 1 from lbpol where contno = '"+strContNo+"' and riskcode in('231701','333801') and payintv='0') with ur";
        String tHuoMian = tExeSQL.getOneValue(sqlHuoMian);
        if(!"null".equals(tHuoMian) && !"".equals(tHuoMian) && tHuoMian != null)
        {
        	strRealpay = tHuoMian;
        	tGetDutyType = "0";
        }
        //#3372
        //如果赔付结论是不准予豁免，则在此处计算险种可豁免的期数。star
        if("7".equals(aClmPolSchema.getGiveType())){
        	String tAccDate = "";//事件日期
        	String tFreePriedDate = "";//豁免开始日期
        	String tFreeEndDate = "";//豁免结束日期
        	String tFreePried = "";//豁免期数
        	ExeSQL tFreeExeSQL = new ExeSQL(); 
        	String SQL = "select accdate from LLSubReport where subrptno in( "+
                    "select subrptno from llcaserela where caseno='"+mCaseNo+"')";
        	tAccDate = tFreeExeSQL.getOneValue(SQL);
        	String tFreePriedSql = "select curpaytodate from ljapayperson a where "+
        			 "exists (select 1 from ljapay b where a.contno = b.incomeno) "+
        			 "and contno ='"+strContNo+"' and riskcode ='"+strRiskcode+"' "+
        			 "and '"+tAccDate+"' between lastpaytodate and curpaytodate "+
        			 "order by contno ";
        	
        	tFreePriedDate = tFreeExeSQL.getOneValue(tFreePriedSql);
        	tFreePriedSql = "select enddate from lcpol where contno ='"+strContNo+"' and riskcode ='"+strRiskcode+"' with ur ";
        	tFreeEndDate = tFreeExeSQL.getOneValue(tFreePriedSql);
        	int num = 12;
            
        	if((PubFun.calInterval(tFreePriedDate, tFreeEndDate, "M"))%num == 0){
        	    tFreePried = (PubFun.calInterval(tFreePriedDate, tFreeEndDate, "M"))/num+"";
        	}else{
        	    tFreePried = ((PubFun.calInterval(tFreePriedDate, tFreeEndDate, "M"))/num)+1+"";
        	}
        	System.out.println("tFreePried:"+tFreePried);
        	strGiveKind ="3";
        	strRealpay = tFreePried;
        	tGetDutyType = "0";
        	//如果赔付结论是不准予豁免，则在此处计算险种可豁免的期数。end 	 
        }

       

        //查询算法编码
//        LMUWDB tLMUWDB = new LMUWDB();
//        tLMUWDB.setRelaPolType("G"); //个单
//        tLMUWDB.setUWType("5"); //核赔权限
//        tLMUWDB.setRiskCode("0");
//        LMUWSet tLMUWSet = tLMUWDB.query();
//        if(tLMUWSet.size()<=0){
//            CError.buildErr(this, "系统中没有核赔规则的描述，请联系运维处理！");
//            return false;
//        }
//        //取计算编码
//        LMUWSchema tLMUWSchema = tLMUWSet.get(1);
//        String tCalCode = tLMUWSchema.getCalCode();

        //目前系统中只有一条核赔规则，就懒得查了，-_-||
        String tCalCode = "QX0001";
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(tCalCode);
        //增加基本要素{核赔级别,给付类别,给付结论,给付金额}
        mCalculator.addBasicFactor("ClaimPopedom", mClaimPopedom);
        mCalculator.addBasicFactor("GetDutyKind", strGiveKind);
        mCalculator.addBasicFactor("GetDutyType", tGetDutyType);
        mCalculator.addBasicFactor("LimitMoney", strRealpay);

        String tStr = mCalculator.calculate();
        if (tStr.trim().equals("") || tStr.trim().equals("0")) {
            //无权限
            return false;
        }
        return true;
    }

    /**
     * 根据险种代码得到给付类别
     */
    private String getGiveKind(String riskcode) {
        ExeSQL tExeSQL = new ExeSQL();
        //豁免险判断，如果是申诉纠错案件换回C的案件进行判断
        String tCaseNo = mCaseNo;
        if("R".equals(mCaseNo.substring(0,1)) || "S".equals(mCaseNo.substring(0,1)))
    	{
    		System.out.println("申诉纠错案件换为正常的C案件");
            String sql = "select caseno from LLAppeal where appealno ='"+mCaseNo+"'";
            ExeSQL exeSQL = new ExeSQL();
            tCaseNo = exeSQL.getOneValue(sql);
    	}
        
        String sqlHuoMian = "select 1 from LLExemption where riskcode='"+riskcode+"' and caseno='"+tCaseNo+"'  ";
        String tHuoMian = tExeSQL.getOneValue(sqlHuoMian);
        if("1".equals(tHuoMian))
        {
        	return "3";
        }
        
        String sql3 = "select risktype1 from lmriskapp where riskcode='"
                      + riskcode + "'";
        String risktype = tExeSQL.getOneValue(sql3);
        if (risktype.equals("2") || risktype.equals("5")) {
            return ("2");
        } else {
            return "1";
        }
    }

//    private boolean chgPolState() {
//        LCPolDB tLCPolDB = new LCPolDB();
//        tLCPolDB.setInsuredNo(mLLCaseSchema.getCustomerNo());
//        LCPolSet tLCPolSet = tLCPolDB.query();
//        int y = tLCPolSet.size();
//        for (int t = 1; t <= y; t++) {
//            String opolstate = "" + tLCPolSet.get(t).getPolState();
//            String npolstate = "";
//            if (opolstate.length() <= 4) {
//                npolstate = "0001";
//            } else {
//                npolstate = opolstate.substring(4) + opolstate.substring(0, 4);
//            }
//            tLCPolSet.get(t).setPolState(npolstate);
//        }
//        map.put(tLCPolSet, "UPDATE");
//        return true;
//    }

    private boolean fillOpTime(String aRgtState, String aOperator) {
        try {
            LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
            tLLCaseOpTimeSchema.setCaseNo(mCaseNo);
            tLLCaseOpTimeSchema.setRgtState(aRgtState);
            tLLCaseOpTimeSchema.setOperator(aOperator);
            tLLCaseOpTimeSchema.setManageCom(mG.ManageCom);
            LLCaseCommon tLLCaseCommon = new LLCaseCommon();
            String tSQL =
                    "SELECT MAX(sequance) FROM llcaseoptime WHERE caseno='" +
                    mCaseNo + "' AND rgtstate='" + aRgtState + "'";
            ExeSQL tExeSQL = new ExeSQL();
            String aSequance = "";
            if (tExeSQL.getOneValue(tSQL).equals("null") ||
                tExeSQL.getOneValue(tSQL).equals("")) {
                aSequance = "";
            } else {
                aSequance = Integer.toString(Integer.parseInt(tExeSQL.
                        getOneValue(tSQL)));
            }
            tLLCaseOpTimeSchema.setSequance(aSequance);
            tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(tLLCaseOpTimeSchema);
            mLLCaseOpTimeSet.add(tLLCaseOpTimeSchema);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }
    
	/**
	 * 校验理算数据
	 * @return 校验成功
	 */
	private boolean checkData() {
		try {
			if (!mCaseNo.substring(0, 1).equals("S")
					&& !mCaseNo.substring(0, 1).equals("R")) {
				String tDetailSQL = "select 1 from"
						+ " (select sum(realpay) real,caseno caseno from llclaimdetail where caseno ='"
						+ mCaseNo
						+ "' group by caseno) a, "
						+ " (select sum(pay) pay,otherno otherno from ljsgetclaim where otherno ='"
						+ mCaseNo + "' and othernotype<>'H' group by otherno) b "
						+ " where a.caseno=b.otherno and a.caseno='" + mCaseNo
						+ "' and a.real<>b.pay" + " with ur";
				ExeSQL tDetailExeSQL = new ExeSQL();
				System.out.println("tDetailSQL=====" + tDetailSQL);
				SSRS tDetailSSRS = tDetailExeSQL.execSQL(tDetailSQL);
				if (tDetailSSRS.getMaxRow() >= 1) {
					CError.buildErr(this, "理赔业务明细信息和财务明细信息合计金额不等！");
					return false;
				}
			}
			String tClaimSQL = "select 1 from"
					+ " (select sum(realpay) real,caseno caseno from llclaimdetail where caseno ='"
					+ mCaseNo
					+ "' group by caseno) a, "
					+ " (select sum(realpay) real,caseno caseno from llclaim where caseno ='"
					+ mCaseNo + "' group by caseno) b "
					+ " where a.caseno=b.caseno and a.caseno='" + mCaseNo
					+ "' and a.real<>b.real" + " with ur";
			ExeSQL tClaimExeSQL = new ExeSQL();
			System.out.println("tClaimSQL=====" + tClaimSQL);
			SSRS tClaimSSRS = tClaimExeSQL.execSQL(tClaimSQL);
			if (tClaimSSRS.getMaxRow() >= 1) {
				CError.buildErr(this, "理赔业务明细信息和业务汇总合计金额不等！");
				return false;
			}

			String tljsclaimSQL = "select 1 from"
					+ " (select sum(sumgetmoney) real,otherno otherno from ljsget where otherno ='"
					+ mCaseNo
					+ "' group by otherno) a, "
					+ " (select sum(pay) real,otherno otherno from ljsgetclaim where otherno ='"
					+ mCaseNo + "' group by otherno) b "
					+ " where a.otherno=b.otherno and a.otherno='" + mCaseNo
					+ "' and a.real<>b.real" + " with ur";
			ExeSQL tljsclaimExeSQL = new ExeSQL();
			System.out.println("tljsclaimSQL=====" + tljsclaimSQL);
			SSRS tljsclaimSSRS = tljsclaimExeSQL.execSQL(tljsclaimSQL);
			if (tljsclaimSSRS.getMaxRow() >= 1) {
				CError.buildErr(this, "财务明细信息和财务汇总合计金额不等！");
				return false;
			}

			if ("1".equals(mLLCaseSchema.getRgtType())) {
				String tContNo = "";
				String tRiskCode = "";
				LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();

				// 判断是否有万能需要处理账户的险种赔付且赔付大于0
				// 这里认为所有万能附加险都会赔付账户价值
				String tWNSQL = "select * from llclaimdetail where caseno='"
						+ mCaseNo + "'";
				LLClaimDetailSet tLLClaimDetailSet = tLLClaimDetailDB
						.executeQuery(tWNSQL);
				double tPayMoney = 0;
				for (int i = 1; i <= tLLClaimDetailSet.size(); i++) {
					LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
					tLLClaimDetailSchema = tLLClaimDetailSet.get(i);
					tPayMoney += tLLClaimDetailSchema.getRealPay();
					tRiskCode = tLLClaimDetailSchema.getRiskCode();
					tContNo = tLLClaimDetailSchema.getContNo();
					String tPolNo = tLLClaimDetailSchema.getPolNo();
					String tGetDutyCode = tLLClaimDetailSchema.getGetDutyCode();
//					福泽180天之外重疾不赔付账户，跳过账户处理
	                String tFZAccRDSQL = "select 1 from LLElementDetail where caseno='" + mCaseNo + "' "
	        		+" and polno='"+tPolNo+"' and getdutycode='"+tGetDutyCode+"'"
	        		+" and elementcode='RgtDays' and bigint(ElementValue)>180";
	                
	                ExeSQL tFZAccRDExeSQL = new ExeSQL();
	                String tFZAccRD = tFZAccRDExeSQL.getOneValue(tFZAccRDSQL); 
	                
//          		 #2668
	           		 String SQL = "select 1 from lmriskapp where taxoptimal='Y' and  riskcode ='"+tRiskCode+"'";
	           	        ExeSQL mExeSQL = new ExeSQL();
	           	        String riskcode = mExeSQL.getOneValue(SQL);
	                
	                if(tFZAccRD.equals("1") && tRiskCode.equals("231201"))
	                {
	                	System.out.println("福泽180天之外重疾不赔付账户，跳过账户处理");
	                }
	                else
	                {
					if (LLCaseCommon.chenkWN(tRiskCode) && tPayMoney > 0 && !riskcode.equals("1")) {
						LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB();
						tLJSGetClaimDB.setOtherNo(mCaseNo);
						tLJSGetClaimDB.setOtherNoType("5");
						tLJSGetClaimDB.setFeeFinaType("ZH");
						if (tLJSGetClaimDB.query().size() <= 0) {
							CError.buildErr(this, "万能财务账户信息生成失败，请重新进行理算!");
							return false;
						}

						// 查询万能账户
						LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
						String tAccSQL = "select * from lcinsureacc where contno='"
								+ tContNo + "'";
						LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB
								.executeQuery(tAccSQL);

						double tInsuAccBala = 0; // 初始账户余额
						double tAccMoney = 0; // 本次赔款
						if (tLCInsureAccSet.size() > 0) {
							// 针对保单多万能险修改
							for (int tTr = 1; tTr <= tLCInsureAccSet.size(); tTr++) {
								// 万能只有一个账户
								LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccSet
										.get(tTr);
								String tTrPolNo = tLCInsureAccSchema.getPolNo();
								String tTrInsuAccNo = tLCInsureAccSchema
										.getInsuAccNo();

								String TraceAccSql = "select sum(money) from lcinsureacctrace where polno='"
										+ tTrPolNo
										+ "'"
										+ " and insuaccno='"
										+ tTrInsuAccNo + "'";
								ExeSQL tAccExeSQL = new ExeSQL();
								String TraceAccMoney = tAccExeSQL
										.getOneValue(TraceAccSql);
								System.out.println("账户轨迹合计金额TraceAccMoney=="
										+ TraceAccMoney);
								if (!TraceAccMoney.equals("0.00")) {
									CError.buildErr(this, "账户轨迹合计金额不等于0！");
									return false;
								}

								tInsuAccBala = Arith.round(
										tLCInsureAccSchema.getInsuAccBala(), 2);
								String tTraceSQL = "select coalesce(sum(money),0) from lcinsureacctrace "
										+ " where polno='"
										+ tTrPolNo
										+ "' and insuaccno='"
										+ tTrInsuAccNo
										+ "' and otherno='"
										+ mCaseNo
										+ "' and state='temp' and moneytype='PK'";
								ExeSQL tExeSQL = new ExeSQL();
								tAccMoney = Arith.round(Double
										.parseDouble(tExeSQL
												.getOneValue(tTraceSQL)), 2);
								System.out.println("账户余额tInsuAccBala=="
										+ tInsuAccBala);
								System.out
										.println("账户轨迹本次需要赔付金额==" + tAccMoney);
								if (tInsuAccBala < tAccMoney) {
									CError.buildErr(this, "账户余额无法支付本次赔款！");
									return false;
								}

								LCInsureAccFeeDB aLCInsureAccFeeDB = new LCInsureAccFeeDB();
								aLCInsureAccFeeDB.setPolNo(tTrPolNo);
								if (aLCInsureAccFeeDB.query().size() <= 0) {
									CError.buildErr(this, "账户管理费查询失败！");
									return false;
								}
							}
						} else {
							CError.buildErr(this, "保单" + tContNo + "账户信息查询失败！");
							return false;
						}
					} else {
						continue;
					}
	                }
				}
			}
			
			// # 3091 理赔人员直系亲属回避**start**
			String aHandler =mLLCaseSchema.getDealer();
			LLRelationCaseSchema tLLRelationCaseSchema= new LLRelationCaseSchema();
			LLRelationCaseSchema oLLRelationCaseSchema = new LLRelationCaseSchema();
			ExeSQL checkExe = new ExeSQL();
			//校验案件处理人与被保人是否属于直系亲属
			String checkRelaSQL="select relation  from llrelationclaim where usercode='"+aHandler+"' and relationname in ('"+mLLCaseSchema.getCustomerName()+"') "
					+ " and relationidno in('"+mLLCaseSchema.getIDNo()+"') with ur";
			SSRS checkRel = checkExe.execSQL(checkRelaSQL);
			if(checkRel.getMaxRow()>0){
				System.out.println("=========start begin==============");
				tLLRelationCaseSchema.setRelation(checkRel.GetText(1, 1));
				tLLRelationCaseSchema.setCaseNo(mLLCaseSchema.getCaseNo());
				tLLRelationCaseSchema.setDealer(aHandler);
				tLLRelationCaseSchema.setRelationName(mLLCaseSchema.getCustomerName());
				tLLRelationCaseSchema.setRgtState(mLLCaseSchema.getRgtState());
				tLLRelationCaseSchema.setSerialNo(PubFun1.CreateMaxNo("RELA_CASE", 20));
				tLLRelationCaseSchema.setMakeDate(PubFun.getCurrentDate());
				tLLRelationCaseSchema.setMakeTime(PubFun.getCurrentTime());
				tLLRelationCaseSchema.setMngCom(mG.ManageCom);
				tLLRelationCaseSchema.setOperator(mG.Operator);
				tLLRelationCaseSchema.setRgtNo(mRgtNo);
				tLLRelationCaseSchema.setModifyDate(PubFun.getCurrentDate());
				tLLRelationCaseSchema.setModifyTime(PubFun.getCurrentTime());
				
				map.put(tLLRelationCaseSchema, "INSERT");
			
			}
			//校验案件处理人姓名与赔款领取人是否属于直系亲属
			String checkAccSQL="select relation from llrelationclaim where usercode='"+aHandler+"' and relationname in ('"+mLLCaseSchema.getAccName()+"') with ur";
			ExeSQL AccExe = new ExeSQL();
			SSRS checkAcc= AccExe.execSQL(checkAccSQL);
			if(checkAcc.getMaxRow()>0){
				oLLRelationCaseSchema.setCaseNo(mLLCaseSchema.getCaseNo());
				oLLRelationCaseSchema.setRelation(checkAcc.GetText(1, 1));
				oLLRelationCaseSchema.setRelationName(mLLCaseSchema.getAccName());
				oLLRelationCaseSchema.setSerialNo(PubFun1.CreateMaxNo("RELA_CASE", 20));
				oLLRelationCaseSchema.setDealer(aHandler);
				oLLRelationCaseSchema.setRgtNo(mRgtNo);
				oLLRelationCaseSchema.setRgtState(mLLCaseSchema.getRgtState());
				oLLRelationCaseSchema.setMngCom(mG.ManageCom);
				oLLRelationCaseSchema.setOperator(mG.Operator);
				oLLRelationCaseSchema.setMakeDate(PubFun.getCurrentDate());
				oLLRelationCaseSchema.setMakeTime(PubFun.getCurrentTime());
				oLLRelationCaseSchema.setModifyDate(PubFun.getCurrentDate());
				oLLRelationCaseSchema.setModifyTime(PubFun.getCurrentTime());
				map.put(oLLRelationCaseSchema, "INSERT");
			}
			if((!"".equals(oLLRelationCaseSchema.getSerialNo()) && oLLRelationCaseSchema.getSerialNo()!=null) 
					|| (!"".equals(tLLRelationCaseSchema.getSerialNo())  && tLLRelationCaseSchema.getSerialNo() !=null)){
				mInputData.clear();
				mInputData.add(map);
				PubSubmit ps = new PubSubmit();
		        if (!ps.submitData(mInputData, null)) {
		            CError.buildErr(this, "理赔人员直系亲属数据库保存失败");
		            return false;
		        }
		        //错误处理
		        CError.buildErr(this, "保存失败，请与总公司理赔人员联系");
		        return false;
			}
			// # 3091 理赔人员直系亲属回避**end**
		} catch (Exception ex) {
			// @@错误处理
			CError.buildErr(this, "校验理赔金额出错！");
			return false;
		}
		return true;
	}

    public static void main(String[] args) {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "cm1201";
        tGlobalInput.ManageCom = "8612";

        LLClaimUWMainSchema tLLClaimUWMainSchema = new LLClaimUWMainSchema();
        tLLClaimUWMainSchema.setRgtNo("P1200070212000001");
        tLLClaimUWMainSchema.setCaseNo("C1200070212000001");
        tLLClaimUWMainSchema.setClmNo("52000017714");
        tLLClaimUWMainSchema.setcheckDecision1("1");
        tLLClaimUWMainSchema.setRemark1("shen pi remark for test");
//        tLLClaimUWMainSchema.setcheckDecision2("1");
//        tLLClaimUWMainSchema.setRemark2("shen ding remark for test");

//        LLCaseSchema tcase = new LLCaseSchema();
//        LLCaseDB tllcasedb = new LLCaseDB();
//        tcase.setRgtNo("P3100070208000003");
//        tllcasedb.setCaseNo("C3100070213000175");
//        tllcasedb.getInfo();
//        tcase=tllcasedb.getSchema();
//        tcase.setRgtState("04");
//        LLClaimUserSchema tcuser = new LLClaimUserSchema();
//        tcuser.setUserCode("cm0009");
//        tcuser.setUpUserCode("cm0002");
//        tcuser.setClaimPopedom("H");

        VData aVData = new VData();
        aVData.add(tGlobalInput);
        aVData.add(tLLClaimUWMainSchema);
//        aVData.add(tcase);
//        aVData.add(tcuser);
        ClaimUnderwriteBL tClaimUnderwriteBL = new ClaimUnderwriteBL();
        tClaimUnderwriteBL.submitData(aVData, "APPROVE|SP");
    }
}
