package com.sinosoft.lis.llcase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import com.sinosoft.lis.bl.LCGrpPolBL;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.bq.EdorCalZTTestBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单理赔给付计算业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HE
 * @version 1.0
 */
public class ClaimCalBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    private MMap tmpMap = new MMap();
    private GlobalInput mGlobalInput = new GlobalInput();

//描述信息
//    private LMDutyGetClmSet mLMDutyGetClmSet = new LMDutyGetClmSet();

//保单信息
    private LCContSchema mLCContSchema = new LCContSchema();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    private LCDutySchema mLCDutySchema = new LCDutySchema();
    private LDWrapSchema mLDWrapSchema = new LDWrapSchema();
    private LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet();
//    private LCInsureAccTraceSchema mLCInsureAccTraceSchema = new
//            LCInsureAccTraceSchema();

//案件信息
    private LLSecurityReceiptSchema mLLSecurityReceiptSchema = new
            LLSecurityReceiptSchema();

//赔案信息
    private LLClaimDetailSet mSaveLLClaimDetailSet = new LLClaimDetailSet();
    private LLClaimPolicySchema mLLClaimPolicySchema = null;
    private LLClaimSchema mLLClaimSchema = null;
    private LLClaimPolicySet mLLClaimPolicySet = new LLClaimPolicySet();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();

    private String mClmNo = "";
    private String mCaseNo = "";
    private String mRgtNo = "";
    private String mCaseRelaNo = "";
    private String mMngCom = "";
    private String mAccTraceDate = "";
    private String mPolNo = "";
    private String mContNo = "";
    private String mGetDutyKind="";
    private String mGetDutyCode="";
    private String mDutyCode = "";
    private boolean havetab = false;
    private boolean polvalidate = true;
    private boolean dealacc = false;
    private boolean mMJFlag = true;
    private double t_standmoney = 0.0;
    private double t_sum_gf_je = 0;
    private double t_real_gf_je = 0;
    private String backMsg = "";
    private String SpendAmnt="0";
    private String mAccDate = null;
    private boolean checkAmnt = true;
    private double mGetLimit = 0;
    private boolean mCalGetLimitFlag = false;
    
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    //#2781 xuyunpeng add 税优产品专用标志
    private boolean syPoint = false;
    //#2781 xuyunpeng add 税优产品专用标志
    public ClaimCalBL() {
    }

    public String getBackMsg(){
        return backMsg;
    }
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData,cOperate)) {
            return false;
        }
        
        //查询分案信息
        if (!this.getLLCaseInfo()) {
            return false;
        }

        System.out.println("更新该分案赔案信息开始："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
        String tsq1 = "INSERT INTO ldtimetest VALUES ('rgtType1','批理算','"+mRgtNo+"_"+mCaseNo+"','更新分案赔案信息开始','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq1);
        //查询赔案信息
        if (!getLLClaimInfo()) {
            return false;
        }
        System.out.println("更新该分案赔案信息结束："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
        String tsq2 = "INSERT INTO ldtimetest VALUES ('rgtType1','批理算','"+mRgtNo+"_"+mCaseNo+"','更新分案赔案信息结束','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq2);
        System.out.println("计算该分案理赔金额开始："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
        String tsq3 = "INSERT INTO ldtimetest VALUES ('rgtType1','批理算','"+mRgtNo+"_"+mCaseNo+"','计算分案理赔金额开始','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq3);
        //理赔计算
        if(cOperate.equals("Cal")){
            if (!calpay()) {
                CError.buildErr(this, "理赔计算错误!");
                return false;
            }
        }else{
            if (!autocalpay()) {
                CError.buildErr(this, "理赔计算错误!");
                return false;
            }
        }
        System.out.println("计算该分案理赔金额结束："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
        String tsq4 = "INSERT INTO ldtimetest VALUES ('rgtType1','批理算','"+mRgtNo+"_"+mCaseNo+"','计算分案理赔金额结束','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq4);
        System.out.println("账户险处理开始："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
        String tsq5 = "INSERT INTO ldtimetest VALUES ('rgtType1','批理算','"+mRgtNo+"_"+mCaseNo+"','账户险处理开始','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq5);
        //帐户险处理
        if(dealacc)
            getInsuAcc();

        System.out.println("账户险处理结束："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
        String tsq6 = "INSERT INTO ldtimetest VALUES ('rgtType1','批理算','"+mRgtNo+"_"+mCaseNo+"','账户险处理结束','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq6);
        tmpMap.put(this.mSaveLLClaimDetailSet, "INSERT");

        mResult.clear();
        mResult.add(tmpMap);
        System.out.println("税优产品校验开始："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
        String tsq7 = "INSERT INTO ldtimetest VALUES ('rgtType1','批理算','"+mRgtNo+"_"+mCaseNo+"','税优产品校验开始','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq7);
        //#2781 xuyunpeng add 税优产品专用弹窗 start
        if(checkSyPoint()){
        	if(syPoint){
        		backMsg+="实际赔付的金额低于合同约定范围内被保险人医疗费用的百分之九十，向被保险人自动补齐相关差额!";
        	}
        }
        //#2781 xuyunpeng add 税优产品专用弹窗 end
        System.out.println("税优产品校验结束："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
        String tsq8 = "INSERT INTO ldtimetest VALUES ('rgtType1','批理算','"+mRgtNo+"_"+mCaseNo+"','税优产品校验结束','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq8);
        System.out.println("分案数据submit开始："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
        String tsq9 = "INSERT INTO ldtimetest VALUES ('rgtType1','批理算','"+mRgtNo+"_"+mCaseNo+"','分案数据submit开始','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq9);
        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(mResult, null)) {
            CError.buildErr(this, "数据库保存失败");
            return false;
        }
        System.out.println("分案数据submit结束："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
        String tsq10 = "INSERT INTO ldtimetest VALUES ('rgtType1','批理算','"+mRgtNo+"_"+mCaseNo+"','分案数据submit结束','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq10);
        return true;
    }

    /**
     * 取参数信息
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData,String cOperate) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LLCaseSchema aLLCaseSchema = (LLCaseSchema) cInputData.
                                     getObjectByObjectName("LLCaseSchema", 0);
        mCaseNo = aLLCaseSchema.getCaseNo();
        if (mCaseNo == null||mCaseNo.equals("")) {
            CError.buildErr(this, "案件号为空,无法理算!");
            return false;
        }
        if(cOperate.equals("Cal")){
            mLLClaimPolicySet = (LLClaimPolicySet) cInputData.getObjectByObjectName(
                    "LLClaimPolicySet", 0);

            //去掉重复的
            if (mLLClaimPolicySet == null || mLLClaimPolicySet.size() < 1) {
                CError.buildErr(this, "请选出需要理算的保单");
                return false;
            }

            String tMngCom = "";
            for (int j = 1; j <= mLLClaimPolicySet.size(); j++) {
                LCPolBL tLCPolBL = new LCPolBL();
                tLCPolBL.setPolNo(mLLClaimPolicySet.get(j).getPolNo());
                if (!tLCPolBL.getInfo()) {
                    CError.buildErr(this, "险种保单信息查询错误!");
                    return false;
                }
                String tManageCom = "" + tLCPolBL.getManageCom();

                if (tMngCom.equals("")) {
                    tMngCom = "" + tManageCom;
                }
                if (!tMngCom.equals("" + tManageCom)) {
                    CError.buildErr(this, "一个案件下不能进行多机构保单赔付，请分案件处理！");
                    return false;
                }
            }

            for (int i = 2; i <= mLLClaimPolicySet.size(); i++) {
                String tPolNo = mLLClaimPolicySet.get(i).getPolNo();
                String tGetDutyKind = mLLClaimPolicySet.get(i).getGetDutyKind();
                for (int j = 1; j < i; j++) {
                    if (tPolNo.equals(mLLClaimPolicySet.get(j).getPolNo())
                        && tGetDutyKind.equals(
                                mLLClaimPolicySet.get(j).getGetDutyKind())) {
                        mLLClaimPolicySet.removeRange(i, i);
                        i = i - 1;
                        break;
                    }
                }
            }
        }
        return true;
    }

    //从分案保单明细表中取得赔案信息
    private boolean getLLCaseInfo() {
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mCaseNo);
        if (!tLLCaseDB.getInfo()) {
            CError.buildErr(this, "案件信息查询失败");
            return false;
        }

        mRgtNo = tLLCaseDB.getRgtNo();
        mMngCom = tLLCaseDB.getMngCom();
        mLLCaseSchema.setSchema(tLLCaseDB.getSchema());
        if (!"03".equals(mLLCaseSchema.getRgtState())
            && !"04".equals(mLLCaseSchema.getRgtState())
            && !"02".equals(mLLCaseSchema.getRgtState())
            && !"08".equals(mLLCaseSchema.getRgtState())) {
            CError.buildErr(this, "该案件状态不能理算");
            return false;
        }
        
        //#1738 社保调查 理赔各处理流程支持
        String tReturnMsg = LLCaseCommon.checkSurveyRgtState(mCaseNo,mLLCaseSchema.getRgtState(),"03");
        if(!"".equals(tReturnMsg)){
        	CError.buildErr(this, tReturnMsg);
            return false;
        }
        
        if (!LLCaseCommon.checkHospCaseState(mCaseNo)) {
            CError.buildErr(this, "医保通案件待确认，不能进行处理");
            return false;
        }
        
        //更新案件信息
        mLLCaseSchema.setHandleDate(mCurrentDate);
//        aLLCaseSchema.setRgtState("03");
        mLLCaseSchema.setCalFlag("1");
        mLLCaseSchema.setModifyDate(mCurrentDate);
        mLLCaseSchema.setModifyTime(mCurrentTime);
        tmpMap.put(mLLCaseSchema, "UPDATE");
        //加多事件理算
        LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
        tLLCaseRelaDB.setCaseNo(this.mCaseNo);
        LLCaseRelaSet tLLCaseRelaSet = new LLCaseRelaSet();
        tLLCaseRelaSet = tLLCaseRelaDB.query();
        if (tLLCaseRelaSet == null) {
            CError.buildErr(this, "该案件没有关联出险事件，不能理算");
            return false;
        }
        return true;
    }

    /**
     * 查询赔案信息
     * @return boolean
     */
    private boolean getLLClaimInfo() {
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setCaseNo(this.mCaseNo);
        LLClaimSet tLLClaimSet = tLLClaimDB.query();
        if (tLLClaimDB.mErrors.needDealError()) {
            mErrors.copyAllErrors(tLLClaimDB.mErrors);
            return false;
        }
        if (tLLClaimSet != null && tLLClaimSet.size() > 0) {
            mLLClaimSchema = tLLClaimSet.get(1);
            mLLClaimSchema.setGiveType("");
            mLLClaimSchema.setGiveTypeDesc("");
            mClmNo = mLLClaimSchema.getClmNo();
        } else {
            String limit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
            this.mClmNo = PubFun1.CreateMaxNo("CLMNO", limit);
            //创建赔案信息
            createLLClaim();
        }
        //先删除已经计算过的
        String strSQL1 = "delete from LLClaimPolicy where caseno='" + mCaseNo + "'";
        String strSQL2 = "delete from LLClaimDetail where caseno='" + mCaseNo + "'";
        String strSQL3 = "delete from LCInsureAccTrace where Otherno='" + mCaseNo +"' and othertype in ('5','6')";
        String strSQL4 = "delete from LLElementDetail where caseno='"+mCaseNo+"'" ;        
        String strSQL5 = "delete from lcinsureaccfeetrace where otherno='"+mCaseNo+"' and othertype in ('5','6')";
        
        tmpMap.put(strSQL1, "DELETE");
        tmpMap.put(strSQL2, "DELETE");
        tmpMap.put(strSQL3, "DELETE");
        tmpMap.put(strSQL4, "DELETE");
        tmpMap.put(strSQL5, "DELETE");
                
        return true;
    }

    /**
     * 理赔计算
     * @return boolean
     */
    private boolean autocalpay(){
        LLToClaimDutyDB toClaimDutyDB = new LLToClaimDutyDB();
        String sql = "select * from lltoclaimduty where caseno = '"
                     +mCaseNo+"' order by polno, caserelano,getdutykind,getdutycode";
        LLToClaimDutySet toClaimDutySet = toClaimDutyDB.executeQuery(sql);
      
        int getSize = toClaimDutySet.size();
        if(getSize<=0){
            CError.buildErr(this, "没有过滤出符合理赔条件的责任");
            return false;
        }else{
        	 //# 2781  税优险种取消自动理算功能**start**
            String sYRiskCode=toClaimDutySet.get(1).getRiskCode();
            String sYSQL="select taxoptimal from lmriskapp where riskcode='"+sYRiskCode+"'";
            ExeSQL sYExeSQL= new ExeSQL();
            String tTaxptimal = sYExeSQL.getOneValue(sYSQL);
            if (tTaxptimal.equals("Y")){
            	CError.buildErr(this, "没有过滤出符合理赔条件的责任");
            	return false;
            }  	
        }
        // #2781 税优险种取消自动理算功能**end**
        LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();
        
        String tMngCom = "";
        for (int j = 1; j <= toClaimDutySet.size(); j++) {
            if (tMngCom.equals("")) {
                tMngCom = "" + toClaimDutySet.get(j).getPolMngCom();
            }
            if (!tMngCom.equals("" + toClaimDutySet.get(j).getPolMngCom())) {
                CError.buildErr(this, "一个案件下不能进行多机构保单赔付，请分案件处理！");
                return false;
            }
        }
        
        double tSumRealPay = 0.0;
        
        outer:
        for(int i=1;i<=getSize;i++){
            LLToClaimDutySchema toclaimdutyschema = new LLToClaimDutySchema();
            toclaimdutyschema = toClaimDutySet.get(i);
            String tPolNo = toclaimdutyschema.getPolNo();
            String tGetDutyKind  = toclaimdutyschema.getGetDutyKind();
            String tCaseRelaNo = toclaimdutyschema.getCaseRelaNo();
            String tContNo = toclaimdutyschema.getContNo();
            if (i == 1 ||
                    !(tCaseRelaNo.equals(mCaseRelaNo) &&
                      tGetDutyKind.equals(mGetDutyKind) && tPolNo.equals(mPolNo))){
//                if(i!=1){
//                    tmpMap.put(mLLClaimPolicySchema, "INSERT");
//                }
                mCaseRelaNo = tCaseRelaNo;
                mGetDutyKind = tGetDutyKind;
                mPolNo = tPolNo;
                mContNo = tContNo;
                if(!getSubReportSchema())
                    continue;
                mLLClaimPolicySchema = new LLClaimPolicySchema();
                if(!getLCPolInfo()&&mLCPolSchema==null){
                    return false;
                }
                t_sum_gf_je = 0;
                t_real_gf_je = 0;
                if(!getLLClaimPolicyInfo(toclaimdutyschema))
                    return false;
            }
            String aDutyCode = toclaimdutyschema.getDutyCode();
            mDutyCode = toclaimdutyschema.getDutyCode();
            mGetDutyCode = toclaimdutyschema.getGetDutyCode();
            if(!calDutyPay(aDutyCode,mPolNo))
                continue;
            t_sum_gf_je = Arith.round(t_sum_gf_je, 2);
            t_real_gf_je = Arith.round(t_real_gf_je, 2);
            mLLClaimPolicySchema.setStandPay(t_sum_gf_je);
            mLLClaimPolicySchema.setRealPay(t_real_gf_je);
            
            for (int m = 1;m<=tLLClaimPolicySet.size();m++) {
            	LLClaimPolicySchema tLLClaimPolicySchema = tLLClaimPolicySet.get(m);
            	if (tLLClaimPolicySchema.getCaseRelaNo().equals(mLLClaimPolicySchema.getCaseRelaNo())
            		&&tLLClaimPolicySchema.getPolNo().equals(mLLClaimPolicySchema.getPolNo())
            		&&tLLClaimPolicySchema.getGetDutyKind().equals(mLLClaimPolicySchema.getGetDutyKind())
            		&&tLLClaimPolicySchema.getClmNo().equals(mLLClaimPolicySchema.getClmNo())) {
            		continue outer;
            	}
            }
            
            tLLClaimPolicySet.add(mLLClaimPolicySchema);
            tSumRealPay += mLLClaimPolicySchema.getRealPay();
        }

        tmpMap.put(tLLClaimPolicySet, "INSERT");
        
        mLLClaimSchema.setRealPay(Arith.round(tSumRealPay, 2));
        String tSQL="DELETE FROM llclaim WHERE caseno='"+mCaseNo+"'";
        tmpMap.put(tSQL,"DELETE");
        tmpMap.put(mLLClaimSchema, "INSERT");
        
        return true;
    }

    /**
     * 按照保单,受理事故,getDutyKind 计算赔付,及明细 AppleWood
     * //按责任给付计算赔付明细
     *
     */
    private boolean calpay() {
        //加多事件理算
        LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
        tLLCaseRelaDB.setCaseNo(this.mCaseNo);
        LLCaseRelaSet tLLCaseRelaSet = tLLCaseRelaDB.query();
        if (tLLCaseRelaSet == null) {
            CError.buildErr(this, "没有查找到相关的事件，不能理算");
            return false;
        }
        
        LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();
        
        double tSumRealPay = 0.0;

        //如果有多个事件，循环计算
        for (int t = 1; t <= tLLCaseRelaSet.size(); t++) {
            LLCaseRelaSchema tLLCaseRelaSchema = tLLCaseRelaSet.get(t);
            mCaseRelaNo = tLLCaseRelaSchema.getCaseRelaNo();
            if(!getSubReportSchema())
                continue;

            for (int i = 1; i <= mLLClaimPolicySet.size(); i++) {
                //初始化
                t_sum_gf_je = 0;
                t_real_gf_je = 0;
                mLLClaimPolicySchema = new LLClaimPolicySchema();
                mLLClaimPolicySchema.setSchema(mLLClaimPolicySet.get(i));
                mPolNo = mLLClaimPolicySchema.getPolNo();
                mContNo = mLLClaimPolicySchema.getContNo();
                mGetDutyKind = mLLClaimPolicySchema.getGetDutyKind();

                //取得保单信息
                if (!getLCPolInfo()) {
//                    return false;
                }

                //查询或创建赔案明细信息
                if (!getLLClaimPolicyInfo(null)) {
                    return false;
                }

                //取得该保单下所有责任给付赔付
                SynLCLBGetBL aSynLCLBGetBL = new SynLCLBGetBL();
                aSynLCLBGetBL.setPolNo(mPolNo);
                LCGetSet tgetset = aSynLCLBGetBL.query();
                if(tgetset.size()<=0){
                    CError.buildErr(this,"LCGet信息查询失败，保单号"
                            +mPolNo+"没有对应的给付责任！");
                    return false;
                }

                tgetset = LLCaseCommon.orderLCGetSet(tgetset);

                for (int k=1;k<=tgetset.size();k++){
                    mGetDutyCode = tgetset.get(k).getGetDutyCode();
                    String aDutyCode = tgetset.get(k).getDutyCode();
                    mDutyCode = tgetset.get(k).getDutyCode();
                    calDutyPay(aDutyCode, mPolNo);
                }

                //创建赔案明细记录
                t_sum_gf_je = Arith.round(t_sum_gf_je, 2);
                t_real_gf_je = Arith.round(t_real_gf_je, 2);
                mLLClaimPolicySchema.setStandPay(t_sum_gf_je);
                mLLClaimPolicySchema.setRealPay(t_real_gf_je);

                tLLClaimPolicySet.add(mLLClaimPolicySchema);
                tSumRealPay += mLLClaimPolicySchema.getRealPay();
            }
        }
        tmpMap.put(tLLClaimPolicySet, "INSERT");
        
        mLLClaimSchema.setRealPay(Arith.round(tSumRealPay, 2));
        String tSQL="DELETE FROM llclaim WHERE caseno='"+mCaseNo+"'";
        tmpMap.put(tSQL,"DELETE");
        tmpMap.put(mLLClaimSchema, "INSERT");
        
        return true;
    }

    /**
     * 计算每条LCGet的赔付金额，对应存的理赔表为LLClaimDetail
     * @param aDutyCode String
     * @param aPolNo String
     * @return boolean
     */
    private boolean calDutyPay(String aDutyCode,String aPolNo){
        double mSum_gf_je = 0;
        double mReal_gf_je = 0;
        double t_gf_je_base = 0;
        double outAmnt = 0;
        t_standmoney = 0;
        SpendAmnt = "0";

        double t_daylimitfee = 0.0;

        //创建赔付明细记录
        LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();

        //取出责任给付赔付计算代码
        LMDutyGetClmSchema dutyGetClm = getDutyGetClm(this.mGetDutyCode,
                this.mGetDutyKind);
        if(dutyGetClm==null)
            return false;
        String t_CalCode = dutyGetClm.getCalCode();

        //添加续保程序
        SynLCLBDutyBL tSynLCLBDutyBL = new SynLCLBDutyBL();
        tSynLCLBDutyBL.setDutyCode(aDutyCode);
        tSynLCLBDutyBL.setPolNo(aPolNo);
        if (!tSynLCLBDutyBL.getInfo()) {
            CError.buildErr(this, "LCDuty表查询失败");
            return false;
        }
        mLCDutySchema.setSchema(tSynLCLBDutyBL.getSchema());

        //算出起付线
        mGetLimit = getGetLimit(dutyGetClm);

        if (mGetDutyCode.equals("000001")) {
            //退保费
            //查询保单的保单责任表
            t_gf_je_base = mLCPolSchema.getSumPrem();
            mSum_gf_je = t_gf_je_base;
            if (!t_CalCode.equals("")) {
                mReal_gf_je = executepay(t_gf_je_base, t_CalCode);
            }
        } else if (mGetDutyCode.equals("000002")) {
            //加入退保金计算公式
        } else {
            //正常计算方式
            if (dutyGetClm.getInpFlag() == null) {
                dutyGetClm.setInpFlag("0");
            }
                //如果保单有效,查找保额限额
                LCGetSet tLCGetSet = new LCGetSet();
                String tv_where = " where PolNo='" + aPolNo
                                  + "' and GetDutyCode='" + mGetDutyCode
                                  + "' and DutyCode='" + aDutyCode + "'";
                String tv_sql = "select * from LCGet " + tv_where.trim();
                String tv_sqlb = "select * from LBGet " + tv_where.trim();
                System.out.println(tv_sql);
                System.out.println(tv_sqlb);
                SynLCLBGetBL aSynLCLBGetBL = new SynLCLBGetBL();
                tLCGetSet = aSynLCLBGetBL.executeQuery(tv_sql, tv_sqlb);
                if (tLCGetSet.size() > 0) {
                    t_standmoney += tLCGetSet.get(1).getStandMoney();
                }
              //xuyunpengg add 添加对121101险种的保额的特殊处理 #2995 给付责任
				if("121101".equals(mLLClaimPolicySchema.getRiskCode())&&"0".equals(getSecurityFlag())){
					t_standmoney = 1.05*t_standmoney;
				}
				//xuyunpengg add 添加对121101险种的保额的特殊处理 #2995 给付责任
                LMDutyGetAddFactorDB tLMDutyGetAddFactorDB = new
                        LMDutyGetAddFactorDB();
                tLMDutyGetAddFactorDB.setLimitType("01");
                tLMDutyGetAddFactorDB.setGetDutyCode(this.mGetDutyCode);
                int multd = (int) mLCDutySchema.getMult();
                String mult = "" + multd;
                tLMDutyGetAddFactorDB.setMult(mult);
                if (tLMDutyGetAddFactorDB.getInfo()) {
                    t_daylimitfee += tLMDutyGetAddFactorDB.
                            getLimitValue();
                }
                String tempRealHosDays = "0";
                if (!getRealHospitalDays().equals(""))
                    tempRealHosDays = getRealHospitalDays();
                int realhosdays = Integer.parseInt(tempRealHosDays);
                double t_standmoney2 = t_daylimitfee * realhosdays;
                if (t_standmoney2 < t_standmoney && t_standmoney2 > 0.1)
                    t_standmoney = t_standmoney2;
                System.out.println("=== 花费限额：===" + t_standmoney);
            if (dutyGetClm.getInpFlag().equals("2")||"1202".equals(mLLClaimPolicySchema.getRiskCode())) {
                //给付金由出险时发生的金额确定(帐单费用总和)，如医疗类给付
                //   t_gf_je_base = getInpVal();

                //new add 账单金额
                //账单金额处理
                double[] receiptfee ;
                if ("1202".equals(mLLClaimPolicySchema.getRiskCode())){
                    receiptfee = getAvaliFee();
                } else {
                    receiptfee = getAvaliFeeC();
                }

                tLLClaimDetailSchema.setTabFeeMoney(receiptfee[0]);
                tLLClaimDetailSchema.setSelfGiveAmnt(receiptfee[1]);
                tLLClaimDetailSchema.setPreGiveAmnt(receiptfee[2]);
                tLLClaimDetailSchema.setRefuseAmnt(receiptfee[3]);
                System.out.println(tLLClaimDetailSchema.getTabFeeMoney());
                //拒付金额=自费+不合理
                if (((String) mLLClaimPolicySchema.getRiskCode()).equals(
                        "1203")) {
                    tLLClaimDetailSchema.setDeclineAmnt(
                            PubFun.setPrecision(tLLClaimDetailSchema.
                            getRefuseAmnt(), "0.00"));
                } else {
                    tLLClaimDetailSchema.setDeclineAmnt(
                            PubFun.setPrecision(tLLClaimDetailSchema.
                            getSelfGiveAmnt()
                            + tLLClaimDetailSchema.getRefuseAmnt() +
                            getDayOverFee(), "0.00"));
                    outAmnt = getDayOverFee();
                }
                //理算金额=账单金额-拒付金额- 先期给付-自付
                //#1500
                //对于“特定疾病医疗费用保险金-重疾（被保险人未享有社会医疗保险或公费医疗）”，添加对险种231701的特殊处理
                /* 对于“特定疾病医疗费用保险金”分情况特殊开发
                 * 合计金额-不合理费用-先期给付 */
                if(((String) mLLClaimPolicySchema.getRiskCode()).equals(
                "231701") && (mGetDutyCode.equals("350202"))){
                	t_gf_je_base = PubFun.setPrecision(
                            tLLClaimDetailSchema.getTabFeeMoney()
                            - tLLClaimDetailSchema.getPreGiveAmnt()
                            - tLLClaimDetailSchema.getRefuseAmnt(), "0.00");
                	
                }else{
                	t_gf_je_base = PubFun.setPrecision(
                            tLLClaimDetailSchema.getTabFeeMoney()
                            - tLLClaimDetailSchema.getSelfGiveAmnt()
                            - tLLClaimDetailSchema.getPreGiveAmnt()
                            - tLLClaimDetailSchema.getRefuseAmnt(), "0.00");
                }
                
                tLLClaimDetailSchema.setClaimMoney(t_gf_je_base);
                System.out.println(tLLClaimDetailSchema.getTabFeeMoney());
                System.out.println(tLLClaimDetailSchema.getSelfGiveAmnt());
                System.out.println(tLLClaimDetailSchema.getPreGiveAmnt());
                System.out.println(tLLClaimDetailSchema.getRefuseAmnt());

/*取消理算金额与每日限额之和的比较，直接传到后台再进行比较*/
//                if(t_gf_je_base>t_standmoney){
//                    t_gf_je_base = t_standmoney;
//                }
/*取消理算金额与每日限额之和的比较，直接传到后台再进行比较*/

            } else { //承保时确定给付金金额
                //添加续保的程序
                t_gf_je_base += t_standmoney;
                if (!getSecuReceipt())
                    System.out.println("社保账单填充失败");
            }

            System.out.println("t_gf_je_base===" + t_gf_je_base);
            String extrAmntFlag = "" + dutyGetClm.getExtraAmntFlag();
            //额外保险金
            if (extrAmntFlag.equals("Y"))
                t_gf_je_base = getAdditionalPay(aPolNo);
            if (dutyGetClm.getNeedReCompute() != null &&
                dutyGetClm.getNeedReCompute().equals("N")) {
                mSum_gf_je = t_gf_je_base;
            } else {
                if (t_CalCode != null && !t_CalCode.equals("")) {
                    mSum_gf_je = executepay(t_gf_je_base, t_CalCode);
                    mReal_gf_je = mSum_gf_je;
                }
            }
        }
        mSum_gf_je = (mSum_gf_je<0)?0:mSum_gf_je;
        mReal_gf_je = (mReal_gf_je<0)?0:mReal_gf_je;

        //对保单生效期De判断
        if (!polvalidate||!mMJFlag) {
            mReal_gf_je = 0;
            mGetLimit = 0.0;
        }

        System.out.println("=== 比例前的给付金：===" + mReal_gf_je);
        //确定该保险金的赔付比例
        double tPayRate = 0;
        tPayRate = getCaseRate(dutyGetClm);
        mReal_gf_je = mReal_gf_je * tPayRate;
        System.out.println("=== 比例后的给付金：===" + mReal_gf_je);
        //若是批次导入中录入了实赔金额，则按实赔金额计算
        String sqlRealPay = "select affixno from LLFeeMain where  caseno='" +
             mCaseNo + "' and caserelano='" +
             this.mCaseRelaNo + "' fetch first 1 rows only";

        ExeSQL exesqlRealPay = new ExeSQL();
        String resRealPay = exesqlRealPay.getOneValue(sqlRealPay);
        double rtesRealPay = 0;
        if (resRealPay != null && resRealPay.length() > 0) {
            rtesRealPay = Double.parseDouble(resRealPay);
        }
        // #3339 管A接口对接 如果批次里记录了健管金额，则取健管金额进行计算
        if("162401".equals(mLLClaimPolicySchema.getRiskCode())){
        	String mAmountsql="select realamount,getdutycode from llcaseamount where typecode='MA01'and "
        			+ "caseno='"+mCaseNo+"' and getdutycode='"+mGetDutyCode+"' with ur ";
        	 ExeSQL exesqlRealAmouont = new ExeSQL();
             SSRS  tAmountSSRS = exesqlRealAmouont.execSQL(mAmountsql);
             if(tAmountSSRS.getMaxRow()>0){
            	 for(int k=1;k<=tAmountSSRS.getMaxRow();k++){
            		 String tGetDutyCode =tAmountSSRS.GetText(k, 2);
            		 if("741201".equals(tGetDutyCode)){
            			 rtesRealPay = Double.parseDouble(tAmountSSRS.GetText(k, 1));
            			 mReal_gf_je=rtesRealPay;
            	         mSum_gf_je=rtesRealPay;
            		 }
            	 }
             }
        }
  
        //#1922 批次导入 城乡居民大病团体医疗保险（A型）理算逻辑调整
        //若险种城乡居民大病团体医疗保险（A型）且录入了“大额医疗支付”（大额救助支付），此时实赔金额应该为空，就将此金额置为实赔金额
        String sqlHighAmnt2 = "select HighAmnt2 from LLSecurityReceipt where  caseno='" +
        mCaseNo + "' fetch first 1 rows only";

		ExeSQL exesqlHighAmnt2 = new ExeSQL();
		String aHighAmnt2 = exesqlHighAmnt2.getOneValue(sqlHighAmnt2);
//		2774
        if(mLCPolSchema.getRiskCode().equals("162201") && aHighAmnt2 != null && !aHighAmnt2.equals("0.00")&&!("8694".equals(mLCPolSchema.getManageCom().substring(0, 4))&&"1".equals(LLCaseCommon.checkSocialSecurity(mLCPolSchema.getGrpContNo(),"grpno"))))
        {
        	rtesRealPay = Double.parseDouble(aHighAmnt2);
        }
         
        if ((rtesRealPay >=0 && resRealPay!=null && !resRealPay.equals(""))||(rtesRealPay >=0 && aHighAmnt2!=null && !aHighAmnt2.equals("0.00")&& !aHighAmnt2.equals("") && mLCPolSchema.getRiskCode().equals("162201"))) {
        	if(rtesRealPay==0)
        	{
        	mLLClaimSchema.setGiveType("3");
        	mLLClaimSchema.setGiveTypeDesc("全额拒付");
        	}
            mReal_gf_je=rtesRealPay;
            mSum_gf_je=rtesRealPay;
        }
        // # 2864 天津静海结算退费情况**start**
        String casetype= "";
        String tJSQL="select casetype from llhospcase where caseno='"+mLLCaseSchema.getCaseNo() +"' with ur ";
        ExeSQL tJExeSQL = new ExeSQL();
        SSRS tJSSRS =tJExeSQL.execSQL(tJSQL);
        if(tJSSRS.getMaxRow()>0){
        	 casetype=tJSSRS.GetText(1, 1);
        }
        if(rtesRealPay <0 && casetype.equals("03") ){
        	  mReal_gf_je=rtesRealPay;
              mSum_gf_je=rtesRealPay;
        }
        // 2864 **end **
        //# 3853 天津大病退费存负数 start
        if(rtesRealPay <0 && casetype.equals("13") ){
      	  	mReal_gf_je=rtesRealPay;
            mSum_gf_je=rtesRealPay;
        }
        //#3853 天津大病退费存负数 end
      //添加青岛3454
//        boolean bool=false;
//        int index=pageName.indexOf("case/LLQDGrpRegisterInput.jsp");
        String qdSQL=" select count(1) from LLFeeMain where 1=1 and ReceiptNo='94000' and FeeAtti='4' and caseno='"+mLLCaseSchema.getCaseNo() +"'  with ur ";
        String mange=mGlobalInput.ManageCom.length()>=4?mGlobalInput.ManageCom.substring(0, 4):mGlobalInput.ManageCom;//当前机构是否为8694
        String index=new ExeSQL().getOneValue(qdSQL);
		if(!"0".equals(index) && rtesRealPay <0 && "8694".equals(mange)){
	      	  mReal_gf_je=rtesRealPay;
	          mSum_gf_je=rtesRealPay;
		 }
        
        //把原来通过险种判断是否赔账户，修改成判断给付责任是否赔账户
        LMDutyGetDB tLMDutyGetDB = new LMDutyGetDB();
        String insuaccflag = "";
        //万能附加险根据主险判断是否赔账户
        LDCode1DB tLDCode1DB = new LDCode1DB();
        tLDCode1DB.setCode(mLCPolSchema.getRiskCode());
        tLDCode1DB.setCodeType("checkappendrisk");
        if(tLDCode1DB.query().size()>=1)
        {
        	String sqlFR="select c.needacc from lmriskduty a,lmdutygetrela b ,lmdutyget c"
        			+" where a.dutycode=b.dutycode and b.getdutycode=c.getdutycode"
        			+" and a.riskcode='"+tLDCode1DB.query().get(1).getCode1()+"'";
        	ExeSQL WNFRexeSQL = new ExeSQL();
			SSRS WNFRssrs = WNFRexeSQL.execSQL(sqlFR);        	
        	if(WNFRssrs.getMaxRow()>=1)
        	{
        		insuaccflag = WNFRssrs.GetText(1, 1);
        	}
        }
        else{
        	 tLMDutyGetDB.setGetDutyCode(mGetDutyCode);
        	 if(tLMDutyGetDB.getInfo()) 
        	 insuaccflag = tLMDutyGetDB.getSchema().getNeedAcc();
        }                     
        String flag = null;
        if(LLCaseCommon.chenkWN(mLCPolSchema.getRiskCode()))
        {
        	flag = "4";
        }
        String tpolno = mLCPolSchema.getPolNo();              
        if (insuaccflag.equals("1")&&!"4".equals(flag) ) {  	// 赔账户，非万能
        	dealacc = true;				//账户标记           	
    		 //帐户险保额控制               



            havetab = false;
            tLLClaimDetailSchema.setTabFeeMoney(Arith.round(getTabFee(), 2));
            tLLClaimDetailSchema.setClaimMoney(Arith.round(getClaimMoney(t_CalCode), 2));
            double caledpay = getAccPay(tpolno,mGetDutyCode);
            
            GetAccAmnt tgetaccamnt = new GetAccAmnt();            
            mReal_gf_je = tgetaccamnt.getAccPay(tpolno,mCaseNo,caledpay+mReal_gf_je)-caledpay;
            
            //大于0的时候会返回
            if (mReal_gf_je<=0) {
                backMsg += tgetaccamnt.getAccMsg();
            }
            double declinemoney = mSum_gf_je - mReal_gf_je;
            tLLClaimDetailSchema.setDeclineAmnt(declinemoney);
         	             
        } else {
        	/* 1-非账单；2-取普通医院账单；3-取社保账单 */
            if ("3".equals(dutyGetClm.getInpFlag())
                    || "2".equals(dutyGetClm.getInpFlag())) {
                //需要保额控制的给付责任
//对保额的控制精确到责任明细级别
                double aAmnt=0;
//                if(dutyGetClm.getInpFlag().equals("4")){            //当InpFlag为4时表示责任下明细保额可能超过险种的保额
//                    aAmnt = getAmnt(mLCPolSchema.getPolNo(),
//                                    mLCDutySchema.getDutyCode(), mGetDutyCode);
//                    if(mReal_gf_je>aAmnt){
//                        mReal_gf_je=aAmnt;
//                    }
//
//                }else{
                    aAmnt = mLCDutySchema.getAmnt();
                                        
                    // 取特殊的保额值
    				aAmnt = getSpecialAmnt(mLCDutySchema.getDutyCode(),
    				mLCDutySchema.getAmnt());
    				//xuyunpengg add 添加对121101险种的保额的特殊处理 #2995 责任层
    				if("121101".equals(mLLClaimPolicySchema.getRiskCode())&&"0".equals(getSecurityFlag())){
    					aAmnt = 1.05*aAmnt;
    				}
    				//xuyunpengg add 添加对121101险种的保额的特殊处理 #2995 责任层
    				
    				mLCDutySchema.setAmnt(aAmnt);
                    
    				if (aAmnt > 0.01 && mReal_gf_je > aAmnt) {
                        outAmnt += Arith.round(mReal_gf_je - aAmnt, 2);
                        mReal_gf_je = aAmnt;
                    }

                    //每种责任的给付金不能超过保单的保额
                    double temGet = getacupay(mGetDutyCode, mCaseRelaNo);
                    if (mReal_gf_je > temGet && checkAmnt) {
//                    outAmnt = Arith.round(mReal_gf_je - temGet, 2);
                        mReal_gf_je = temGet;
                    }

                    double declinemoney = mSum_gf_je - mReal_gf_je;
                    tLLClaimDetailSchema.setDeclineAmnt(declinemoney);
                    if (tLLClaimDetailSchema.getTabFeeMoney() < 0.01) {
                        tLLClaimDetailSchema.setTabFeeMoney(Arith.round(
                                getTabFee(), 2));
                    }
                    double aclmmoney = Arith.round(getClaimMoney(t_CalCode), 2);
                    if (aclmmoney > 0) {
                        tLLClaimDetailSchema.setClaimMoney(aclmmoney);
                    }
                    //对溢额的显示
                    if (mLCDutySchema.getAmnt() > 0.01) {
                        outAmnt += tLLClaimDetailSchema.getClaimMoney() -
                                mLCDutySchema.getAmnt();
                        outAmnt = (outAmnt < 0) ? 0 : Arith.round(outAmnt, 2);
                    }
//                }
            } else {
                havetab = false;
                tLLClaimDetailSchema.setTabFeeMoney(Arith.round(getTabFee(), 2));
                tLLClaimDetailSchema.setClaimMoney(Arith.round(
                        getClaimMoney(t_CalCode), 2));
                //inpflag 0和1的都是赔保额保费 不用控制了 以后逐步取消0
//                if (mLCDutySchema.getAmnt() > 0.01)
//                    outAmnt = tLLClaimDetailSchema.getClaimMoney() -
//                              mLCDutySchema.getAmnt();
//                if (outAmnt < 0)
//                    outAmnt = 0;
//                double declinemoney = mSum_gf_je - mReal_gf_je;
//                declinemoney = Arith.round(declinemoney, 2);
//                tLLClaimDetailSchema.setDeclineAmnt(declinemoney);
            }
        }
        //取分
        mSum_gf_je = Arith.round(mSum_gf_je, 2);
        mReal_gf_je = Arith.round(mReal_gf_je, 2);
        t_sum_gf_je = t_sum_gf_je + mSum_gf_je;
        t_real_gf_je = t_real_gf_je + mReal_gf_je;
        System.out.println("实赔金额：" + mReal_gf_je);
      System.out.println("总实赔金额：" + t_real_gf_je);



        //险种总给付
//        if (!dutyGetClm.getInpFlag().equals("3")&&!dutyGetClm.getInpFlag().equals("4") && !insuaccflag.equals("Y")) {
//            if (mLCPolSchema.getAmnt()>0&&t_real_gf_je > mLCPolSchema.getAmnt()) {
//                t_real_gf_je = mLCPolSchema.getAmnt();
//            }
//        }
        //免赔额

        String yeargetlimitflag = "" + dutyGetClm.getYearGetLimitFlag();
        if (mReal_gf_je <= 0
				&& (yeargetlimitflag.equals("1") || "2"
						.equals(yeargetlimitflag)) && !mCalGetLimitFlag
				&& mLCDutySchema.getGetRate() > 0) {
			mGetLimit = tLLClaimDetailSchema.getClaimMoney();
		}
        System.out.println("+++++免赔额++++++" + mGetLimit);
        //理算金额
        if (tLLClaimDetailSchema.getClaimMoney() < 0.001)
            tLLClaimDetailSchema.setClaimMoney(mSum_gf_je);

        tLLClaimDetailSchema.setGetDutyCode(mGetDutyCode);
        tLLClaimDetailSchema.setClmNo(this.mClmNo);
        tLLClaimDetailSchema.setRgtNo(mRgtNo);
        tLLClaimDetailSchema.setGetDutyKind(mGetDutyKind);
        tLLClaimDetailSchema.setCaseNo(mCaseNo);
        tLLClaimDetailSchema.setStatType(dutyGetClm.getStatType());
        tLLClaimDetailSchema.setContNo(mLCPolSchema.getContNo());
        tLLClaimDetailSchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
        tLLClaimDetailSchema.setGrpContNo(mLCPolSchema.getGrpContNo());
        tLLClaimDetailSchema.setPolNo(mLCPolSchema.getPolNo());
        tLLClaimDetailSchema.setKindCode(mLCPolSchema.getKindCode());
        tLLClaimDetailSchema.setRiskCode(mLCPolSchema.getRiskCode());
        tLLClaimDetailSchema.setRiskVer(mLCPolSchema.getRiskVersion());
        tLLClaimDetailSchema.setPolMngCom(mLCPolSchema.getManageCom());
        tLLClaimDetailSchema.setSaleChnl(mLCPolSchema.getSaleChnl());
        tLLClaimDetailSchema.setOutDutyAmnt(mGetLimit);
        tLLClaimDetailSchema.setOutDutyRate(mLCDutySchema.getGetRate());
        tLLClaimDetailSchema.setAgentCode(mLCPolSchema.getAgentCode());
        tLLClaimDetailSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
        tLLClaimDetailSchema.setStandPay(Arith.round(mSum_gf_je, 2));
        tLLClaimDetailSchema.setOverAmnt(outAmnt);
        tLLClaimDetailSchema.setRealPay(mReal_gf_je);
        tLLClaimDetailSchema.setDutyCode(mLCDutySchema.getDutyCode());
        tLLClaimDetailSchema.setMakeDate(mCurrentDate);
        tLLClaimDetailSchema.setModifyDate(mCurrentDate);
        tLLClaimDetailSchema.setMakeTime(mCurrentTime);
        tLLClaimDetailSchema.setModifyTime(mCurrentTime);
        tLLClaimDetailSchema.setOperator(this.mGlobalInput.Operator);
        tLLClaimDetailSchema.setMngCom(this.mGlobalInput.ManageCom);
        tLLClaimDetailSchema.setCaseRelaNo(mCaseRelaNo);
   
        //账单金额
        double tabfee = this.getTabFee();
        System.out.println(tabfee);
        if (tLLClaimDetailSchema.getTabFeeMoney() < 0.001 && !havetab) {
            tLLClaimDetailSchema.setTabFeeMoney(tabfee);
            System.out.println(tabfee);
        }

        double[] receiptfee ;
        if ("1202".equals(mLCPolSchema.getRiskCode())) {
            receiptfee = getAvaliFee();
            tLLClaimDetailSchema.setTabFeeMoney(receiptfee[0]);
            t_gf_je_base = PubFun.setPrecision(
                    tLLClaimDetailSchema.getTabFeeMoney()
                    - tLLClaimDetailSchema.getSelfGiveAmnt()
                    - tLLClaimDetailSchema.getPreGiveAmnt()
                    - tLLClaimDetailSchema.getRefuseAmnt(), "0.00");
            tLLClaimDetailSchema.setClaimMoney(t_gf_je_base);
            tLLClaimDetailSchema.setDeclineAmnt(Arith.round(mSum_gf_je - mReal_gf_je, 2));//拒付
        } else {
            receiptfee = getAvaliFeeC();
        }
        //tLLClaimDetailSchema.setClaimMoney(Math.round(receiptfee[0]-receiptfee[1]-receiptfee[2]-receiptfee[3]));
        
        if (tLLClaimDetailSchema.getClaimMoney() <0.001 && !havetab) {
            if (tLLClaimDetailSchema.getOutDutyRate() > 0.0001) {
                double tcalmny = tLLClaimDetailSchema.getRealPay() /
                                 tLLClaimDetailSchema.getOutDutyRate() +
                                 tLLClaimDetailSchema.getOutDutyAmnt();
                tcalmny = Arith.round(Arith.round(tcalmny, 4), 2);
                tLLClaimDetailSchema.setClaimMoney(tcalmny);
            } else {
                tLLClaimDetailSchema.setClaimMoney(tLLClaimDetailSchema.getTabFeeMoney());
            }
        }
        System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa="+tLLClaimDetailSchema.getClaimMoney());
        tLLClaimDetailSchema.setSelfGiveAmnt(receiptfee[1]);
        tLLClaimDetailSchema.setPreGiveAmnt(receiptfee[2]);
        tLLClaimDetailSchema.setRefuseAmnt(receiptfee[3]);
        //#1922 批次导入 城乡居民大病团体医疗保险（A型）理算逻辑调整
        //如果 模板中导入了“可报销范围内金额”，那么理算金额就取此金额，不会影响其他金额的计算        
		String srSQL = "select sum(Reimbursement) from LLFeeMain where caseno='" + mCaseNo + "'";
        ExeSQL texesql = new ExeSQL();
        String mReimbursement = texesql.getOneValue(srSQL);
        if(!mReimbursement.equals(null)&&!mReimbursement.equals("null")&&!mReimbursement.equals("")&&!mReimbursement.equals("0.00"))
        {
        	tLLClaimDetailSchema.setClaimMoney(mReimbursement);
        }

        //tLLClaimDetailSet.add(tLLClaimDetailSchema);
        mSaveLLClaimDetailSet.add(tLLClaimDetailSchema);
        return true;
    }

    /**
     * 查询保单信息
     * @return boolean
     */
    private boolean getLCPolInfo() {
    	LLCaseCommon tLLCaseCommon = new LLCaseCommon();
        polvalidate=true;
        mMJFlag = true;
        
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mContNo);
        
        if (!tLCContDB.getInfo()) {
			LBContDB tLBContDB = new LBContDB();
			tLBContDB.setContNo(mContNo);
			if (!tLBContDB.getInfo()) {
				CError.buildErr(this, "保单信息查询错误!");
				return false;
			}
			Reflections tR=new Reflections();
			tR.transFields(mLCContSchema,tLBContDB.getSchema());
		} else {
			mLCContSchema = tLCContDB.getSchema();
		}
        
        //续保新增代码,LCPolBL同时查C表和B表
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setPolNo(mPolNo);
        if (!tLCPolBL.getInfo()) {
            CError.buildErr(this, "险种保单信息查询错误!");
            return false;
        }

        //保全提供的保单失效日期
//        mEndDate = CommonBL.getPolInvalidate(mPolNo);
//        if (mEndDate.equals(null)||mEndDate.equals("")||mEndDate.equals("null")){
//            CError.buildErr(this, mPolNo+"险种保单失效日查询失败!");
//            return false;
//        }
//
//        int validays = PubFun.calInterval(tLCPolBL.getCValiDate(),
//                                          mAccDate, "D");
//        int remdays = PubFun.calInterval(mAccDate,
//                                         mEndDate, "D");
//        String SQL0 = "select riskcode from lmriskapp where risktype3='7' and risktype <>'M' and riskcode ='"+tLCPolBL.getRiskCode()+"'";
        ExeSQL tExeSQL = new ExeSQL();
//        String riskcode = tExeSQL.getOneValue(SQL0);
//        //cbs00006365 取消特需医疗险种关于事件时间的校验
//        if ((validays < 0 || remdays <= 0)&&StrTool.cTrim(riskcode).equals("")) {
//            String errmsg = "保单" + tLCPolBL.getPolNo() + "的生效日期："
//                            + tLCPolBL.getCValiDate() + ",失效日期："
//                            + mEndDate + "；保单在出险日期："
//                            + mAccDate + " 无效";
//
//            polvalidate = false;
//            CError.buildErr(this, errmsg);
////            mErrors.addOneError(errmsg);
////            System.out.println(errmsg);
////            return false;
//        }
        polvalidate = tLLCaseCommon.checkPolValid(mPolNo,mAccDate);
        mLCPolSchema.setSchema(tLCPolBL.getSchema());
        mLLClaimPolicySchema.setRiskCode(mLCPolSchema.getRiskCode());

//        //做过满期的不能再赔（目前只针对少儿险，还要改）
//        String tSQL = "select * From  ljsgetdraw a where contno ='"+mLCPolSchema.getContNo()
//                    + "' and not exists(select 1 from ljaget where a.getnoticeno = actugetno and paymode ='5') ";
//        SSRS tSSRS = tExeSQL.execSQL(tSQL);
//        if (tSSRS.getMaxRow()>0&&(mLCPolSchema.getRiskCode().equals("320106")||mLCPolSchema.getRiskCode().equals("120706"))){
//            mMJFlag = false;
//            backMsg += "险种"+mLCPolSchema.getRiskCode()+"已满期结算";
//        }
        
        //处理所有满期给付责任
        //查询主险及附加险
        String tRiskSQL = "select code1 from ldcode1 where codetype='checkappendrisk' "
                         + " and code='"
                         + mLCPolSchema.getRiskCode() + "' union "
                         + " select code from ldcode1 where codetype='checkappendrisk' "
                         + " and code1='" + mLCPolSchema.getRiskCode() + "' union "
                         + " select '"+mLCPolSchema.getRiskCode()+"' from dual";
       SSRS tRiskSSRS = tExeSQL.execSQL(tRiskSQL);
       if (tRiskSSRS.getMaxRow()<=0) {
           //无关联险种直接判断
           mMJFlag = checkMJ(mLCPolSchema.getRiskCode());
       } else {
          for (int i=1; i<=tRiskSSRS.getMaxRow(); i++) {
              if (!checkMJ(tRiskSSRS.GetText(i,1))){
                  mMJFlag = false;
                  CError.buildErr(this, "险种下有满期责任已给付");
                  break;
              }
          }
       }
       
       // 查询套餐信息
       getRiskWrapCode();

       return true;
    }

    /**
     * 查询或创建赔案明细
     * @param tLLClaimPolicySchema LLClaimPolicySchema
     * @return boolean
     */
    private boolean getLLClaimPolicyInfo(LLToClaimDutySchema toclaimduty) {
        LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
        tLLClaimPolicyDB.setClmNo(mClmNo);
        tLLClaimPolicyDB.setPolNo(mPolNo);
        tLLClaimPolicyDB.setGetDutyKind(mGetDutyKind);
        tLLClaimPolicyDB.setCaseRelaNo(mCaseRelaNo);
//        if (tLLClaimPolicyDB.getInfo()) {
//            //赔案明细存在，说明以前理算过
//            mLLClaimPolicySchema = tLLClaimPolicyDB.getSchema();
//        } else {
            //目前没有了CasePolicy
            //从分案保单明细表中取得赔案信息
            if(toclaimduty!=null){
                Reflections rf = new Reflections();
                LCPolBL tlcpolbl = new LCPolBL();
                tlcpolbl.setPolNo(toclaimduty.getPolNo());
                if(!tlcpolbl.getInfo()){
                    CError.buildErr(this,"保单信息查询失败");
                    return false;
                }
                LCPolSchema tlcpolschema = tlcpolbl.getSchema();
                rf.transFields(mLLClaimPolicySchema, tlcpolschema);
                mLLClaimPolicySchema.setPolMngCom(tlcpolschema.getManageCom());
                mLLClaimPolicySchema.setRiskVer(tlcpolschema.getRiskVersion());
                mLLClaimPolicySchema.setGetDutyKind(toclaimduty.getGetDutyKind());
            }else{
                if (!getPolicyInfo()) {
                    return false;
                }
            }
            mLLClaimPolicySchema.setMakeDate(mCurrentDate);
            mLLClaimPolicySchema.setMakeTime(mCurrentTime);
            mLLClaimPolicySchema.setOperator(this.mGlobalInput.Operator);
            mLLClaimPolicySchema.setMngCom(this.mGlobalInput.ManageCom);
//        }
        mLLClaimPolicySchema.setClmNo(this.mClmNo);
        mLLClaimPolicySchema.setRgtNo(mRgtNo);
        mLLClaimPolicySchema.setCaseNo(mCaseNo);
        mLLClaimPolicySchema.setCaseRelaNo(mCaseRelaNo);
        mLLClaimPolicySchema.setGetDutyKind(mGetDutyKind);
        mLLClaimPolicySchema.setPolNo(mPolNo);
        mLLClaimPolicySchema.setGrpContNo(mLCPolSchema.getGrpContNo());
        mLLClaimPolicySchema.setGiveType("");
        mLLClaimPolicySchema.setGiveTypeDesc("");
        mLLClaimPolicySchema.setModifyDate(mCurrentDate);
        mLLClaimPolicySchema.setModifyTime(mCurrentTime);
        return true;
    }

    private boolean getPolicyInfo() {
        LLCasePolicyDB tLLCasePolicyDB = new LLCasePolicyDB();
        tLLCasePolicyDB.setCaseNo(mCaseNo);
        tLLCasePolicyDB.setPolNo(mPolNo);
        if (!tLLCasePolicyDB.getInfo()) {
            CError.buildErr(this, "案件关联保单查询失败");
            return false;
        }
        Reflections rf = new Reflections();
        rf.transFields(mLLClaimPolicySchema, tLLCasePolicyDB.getSchema());
        return true;
    }

    /**
     * getSubReportSchema
     *
     * @param caseRelaNo String
     * @return LLSubReportSchema
     */
    private boolean getSubReportSchema() {
    	String srSQL = "select a.accdate from llsubreport a,llcaserela b "
				+ " where a.subrptno=b.subrptno and b.caserelano='"
				+ mCaseRelaNo + "' and b.caseno='" + mCaseNo + "'";
        ExeSQL texesql = new ExeSQL();
        mAccDate = texesql.getOneValue(srSQL);
        if(mAccDate.equals("")||mAccDate.equals("null")){
            CError.buildErr(this,"事件信息查询失败!");
            System.out.println("CaseRelaNo"+mCaseRelaNo+"对应的事件查询失败!");
            return false;
        }
        return true;
    }

    /**
     * 查询LMDutyGlm
     * @param getDutyCode String
     * @param getDutyKind String
     * @return String
     */
    private LMDutyGetClmSchema getDutyGetClm(String getDutyCode,
                                             String getDutyKind) {
        LMDutyGetClmDB db = new LMDutyGetClmDB();
        db.setGetDutyCode(getDutyCode);
        db.setGetDutyKind(getDutyKind);
        if (!db.getInfo()) {
            mErrors.addOneError("查询LMDutyGetClm错误");
            System.out.println("查询LMDutyGetClm错误");
            return null;
        }

        return db.getSchema();
    }

    /**
     * 创建赔案记录
     * @return boolean
     */
    private boolean createLLClaim() {
        //判断是否产生赔案信息
        if (this.mLLClaimSchema == null) {
            this.mLLClaimSchema = new LLClaimSchema();
            this.mLLClaimSchema.setClmNo(this.mClmNo);

            String tGetDutyKind = "000000";
            mLLClaimSchema.setGetDutyKind(tGetDutyKind);
            mLLClaimSchema.setCaseNo(mCaseNo);
            mLLClaimSchema.setClmState("1"); //结算
            mLLClaimSchema.setRgtNo(mRgtNo);
            mLLClaimSchema.setClmUWer(mGlobalInput.Operator);
            mLLClaimSchema.setCheckType("0");
            mLLClaimSchema.setMngCom(mMngCom);
            mLLClaimSchema.setOperator(mGlobalInput.Operator);
            mLLClaimSchema.setMakeDate(mCurrentDate);
            mLLClaimSchema.setMakeTime(mCurrentTime);
            mLLClaimSchema.setModifyDate(mCurrentDate);
            mLLClaimSchema.setModifyTime(mCurrentTime);

        }
        return true;
    }


    /**
     * 理赔计算
     * @param t_gf_je_base double
     * @param t_CalCode String
     * @return double
     */
    private double executepay(double t_gf_je_base, String t_CalCode) {
        double rValue;
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(t_CalCode);

        //增加基本要素,计算给付金
        TransferData tTransferData = new TransferData();
        //理算金额
        tTransferData.setNameAndValue("Je_gf", String.valueOf(t_gf_je_base));
        //账单总金额
        tTransferData.setNameAndValue("SumFee", String.valueOf(getTabFee()));
        double aOwnFee = getOwnFee();
        double aOwnFeeC = getOwnFeeC();
        //理算金额-自费金额
        tTransferData.setNameAndValue(
                "PubFee", String.valueOf(t_gf_je_base - aOwnFee));
        //理算金额-自费部分(不受费用项目控制)
        tTransferData.setNameAndValue(
        		"PubFeeC", String.valueOf(Arith.round(t_gf_je_base - aOwnFeeC,2)));

        //社保外费用
        tTransferData.setNameAndValue("OwnFee", String.valueOf(aOwnFee));
        //社保外费用（不受费用项控制）
        tTransferData.setNameAndValue("OwnFeeC", String.valueOf(aOwnFeeC));

        //险种机构
		tTransferData.setNameAndValue("PolManageCom",
				mLCPolSchema.getManageCom());
        //险种保单号
        tTransferData.setNameAndValue(
                "PolNo", String.valueOf(mLCPolSchema.getPolNo()));
        //险种合同号
        tTransferData.setNameAndValue(
                "ContNo", String.valueOf(mLCPolSchema.getContNo()));
	    //理赔号
        tTransferData.setNameAndValue(
                "CaseNo", String.valueOf(mCaseNo));
        System.out.println("理赔号:"+mCaseNo);
                
        //套餐份数
        tTransferData.setNameAndValue("Copys", getWrapCopys());
        //档次
        tTransferData.setNameAndValue(
                "Mult", String.valueOf(mLCDutySchema.getMult()));
        //总保费
        tTransferData.setNameAndValue(
                "Prem", String.valueOf(mLCPolSchema.getPrem()));
        //总保额
        tTransferData.setNameAndValue(
                "Amnt", String.valueOf(mLCPolSchema.getAmnt()));
         //有等待期的总保额
        tTransferData.setNameAndValue("SubAmnt", getSubAmnt());
        System.out.println("有等待期的总保额:"+getSubAmnt());
        //限额
        tTransferData.setNameAndValue("StandMoney",String.valueOf(t_standmoney));
        //交费间隔
        tTransferData.setNameAndValue(
                "PayIntv", String.valueOf(mLCPolSchema.getPayIntv()));
        //被保人投保年龄
        tTransferData.setNameAndValue(
                "GetIntv", String.valueOf(mLCPolSchema.getInsuredAppAge()));
        //被保险人当前年龄
        tTransferData.setNameAndValue(
                "Age", String.valueOf(getCurrentAge()));
        //已交费年期
        tTransferData.setNameAndValue(
                "AppAge", String.valueOf(PubFun.calInterval(
                        mLCPolSchema.getCValiDate(), mLCPolSchema.getPaytoDate(),
                        "Y")));
        //交费年期
        tTransferData.setNameAndValue(
                "PayYears", String.valueOf(mLCDutySchema.getPayYears()));
        //出险时已保年期
        // tTransferData.setNameAndValue("RgtYears",String.valueOf(PubFun.calInterval(mLCPolSchema.getCValiDate(),mLLRegisterSchema.getAccidentDate(),"Y")) );
        tTransferData.setNameAndValue("RgtYears", String.valueOf(
                PubFun.calInterval(mLCPolSchema.getCValiDate(),mAccDate, "Y")));
        //出险时已保天数
        tTransferData.setNameAndValue("RgtDays", String.valueOf(
                PubFun.calInterval(mLCPolSchema.getCValiDate(),mAccDate, "D")));
        //被保人性别
        tTransferData.setNameAndValue(
                "Sex", String.valueOf(mLCPolSchema.getInsuredSex()));
        //住院天数
        tTransferData.setNameAndValue("DaysInHos", getRealHospitalDays());
        //重症监护天数 wujs
        tTransferData.setNameAndValue("SeriousWard", getSeriousWard());
        //保险年期
        tTransferData.setNameAndValue(
                "Years", String.valueOf(mLCDutySchema.getYears()));
        //保单生效日期
        tTransferData.setNameAndValue(
                "ValiDate", String.valueOf(mLCPolSchema.getCValiDate()));
        //出险日期
        tTransferData.setNameAndValue("AccidentDate",this.mAccDate);
        //累计保费
        tTransferData.setNameAndValue(
                "SumPrem", String.valueOf(mLCPolSchema.getSumPrem()));
        //观察期天数
        tTransferData.setNameAndValue(
                "TermDay", String.valueOf(getTermDay()));
        //住院费用
        tTransferData.setNameAndValue(
            "InHospFee", String.valueOf(getInHospFee()));
        //意外医疗住院费用
        tTransferData.setNameAndValue(
                "AccInHospFee", String.valueOf(getAccInHospFee()));

        //门诊费用
        tTransferData.setNameAndValue(
                "DoorPostFee", String.valueOf(getDoorPostFee()));
        //意外门诊费用
        tTransferData.setNameAndValue(
                "AccDoorPostFee", String.valueOf(getAccDoorPostFee()));
        //补充A门诊类型账单金额(2713补充A)
        tTransferData.setNameAndValue("DoorSumFee", String.valueOf(getDoorSumFee()));
        //补充A住院医疗保险金(2713补充A)
        tTransferData.setNameAndValue("InHospSumFee", String.valueOf(getInHospSumFee()));
        //补充A身故前90日内个人基本保险金额的增减 --M的值  (2713补充A)
        tTransferData.setNameAndValue("BasicAmntM", String.valueOf(getBasicAmntM()));
        //该被保险人身故当日零时的个人保险金额(2713补充A)
        tTransferData.setNameAndValue("AccAmnt", String.valueOf(getAccAmnt()));
        //该被保险人身故当日理赔的医疗保险金(2713补充A)
        tTransferData.setNameAndValue("CurrentClaimPay", String.valueOf(getCurrentClaimPay()));
        //管理式补充A门（急）诊医疗保险金(3207管A)
        tTransferData.setNameAndValue("ManageSumFee", String.valueOf(getManageDoorSumFee()));
        //管理式补充A住院医疗保险金(3207管A)
        tTransferData.setNameAndValue("ManageHospSumFee", String.valueOf(getManageHospSumFee()));
        //管理式补充A--M值(3207管A)
        tTransferData.setNameAndValue("BasicMAmntM", String.valueOf(getBasicMAmntM()));
        //管理式补充A身故当日理赔健康医疗保险金(3207管A)
        tTransferData.setNameAndValue("CurrentHealthPay",String.valueOf(getCurrentHealthPay()));
        //管理式补充A身故当日零时的保险金额(3207管A)
        tTransferData.setNameAndValue("ManageAmnt", String.valueOf(getManageAmnt()));
        //管理式补充B--M值(3337 管B)
        tTransferData.setNameAndValue("BasicMBAmntM", String.valueOf(getBasicMBAmntM()));
        //管理式补充B身故当日零时的保险金额 (3337 管B)
        tTransferData.setNameAndValue("ManageBAmnt", String.valueOf(getManageBAmnt()));
        //管理式补充B身故当日理赔的健康医疗保险金 (3337 管B)
        tTransferData.setNameAndValue("CurrentHealthPayB", String.valueOf(getCurrentHealthPayB()));
        //管理式补充B事件类型(3337 管B)
        tTransferData.setNameAndValue("AccidentType", String.valueOf(getAccidentType()));
        //体检费
        tTransferData.setNameAndValue("PEFee", String.valueOf(getPEFee()));
        //救护车费
        tTransferData.setNameAndValue("AmbuFee", String.valueOf(getAmbuFee()));
        //床位费
        tTransferData.setNameAndValue("BedFee", String.valueOf(getBedFee()));
        //续保次数
        tTransferData.setNameAndValue(
                "RenewCount", String.valueOf(mLCPolSchema.getRenewCount()));
        //被保人0岁保单生效对应日
        tTransferData.setNameAndValue("InsuredvalidBirth",
                                      getInsuredvalideBirth());
        //保险人个人帐户的帐户金额
        tTransferData.setNameAndValue(
                "InsuAccBala", String.valueOf(getInsuAccBala(t_gf_je_base)));
        //指定医院 new
        tTransferData.setNameAndValue(
                "hospital", getFixHospital());
//        手术重要程度OprateGrade
        tTransferData.setNameAndValue("OprateGrade",
                                      String.valueOf(getOprateGrade()));
//        重疾责任成立标志SeriousDisease
        tTransferData.setNameAndValue("SeriousDisease", getSeriousDiseaseFlag());
//        慢性或急性病
        tTransferData.setNameAndValue("Disease1Class1", getDisease1Class());
//        意外信息
        tTransferData.setNameAndValue("Incident", getIncidentInfo());
        //身故标记
        tTransferData.setNameAndValue("getDeathFlag", getDeathFlag());
//        获得全残标志信息
        tTransferData.setNameAndValue("SerDiformit", getSerDiformit());

//        赔付比例
        tTransferData.setNameAndValue("GetRate",
                                      String.valueOf(mLCDutySchema.getGetRate()));
//        备用属性1
        tTransferData.setNameAndValue("StandbyFlag1",
                                      String.valueOf(mLCPolSchema.
                getStandbyFlag1()));
//        备用属性2
        tTransferData.setNameAndValue("StandbyFlag2",
                                      String.valueOf(mLCPolSchema.
                getStandbyFlag2()));
//        备用属性3
        tTransferData.setNameAndValue("StandbyFlag3",
                                      String.valueOf(mLCPolSchema.
                getStandbyFlag3()));
//       花费限额
        tTransferData.setNameAndValue("SpendLimit",String.valueOf(getMaxConsume()));
        //身故日期
        tTransferData.setNameAndValue("DeathDate", getDeathDate());
//        获得全残标志信息

        //       限额标记
        tTransferData.setNameAndValue("SpendAmnt",SpendAmnt);
//        大额门诊
        tTransferData.setNameAndValue("SupDoorFee",
                                      String.valueOf(mLLSecurityReceiptSchema.
                getHighDoorAmnt()));
//        小额门诊
        tTransferData.setNameAndValue("SmallDoorPay",
                                      String.valueOf(mLLSecurityReceiptSchema.
                getSmallDoorPay()));
//        门急诊
        tTransferData.setNameAndValue("EmergencyPay",
                                      String.valueOf(mLLSecurityReceiptSchema.
                getEmergencyPay()));
//        自付一
        tTransferData.setNameAndValue(
                "SelfPay1", String.valueOf(mLLSecurityReceiptSchema.getSelfPay1()));
//        自付二
        tTransferData.setNameAndValue(
                "SelfPay2", String.valueOf(mLLSecurityReceiptSchema.getSelfPay2()));
//        自费
        tTransferData.setNameAndValue(
                "SelfAmnt", String.valueOf(mLLSecurityReceiptSchema.getSelfAmnt()));
//        社保起付限
        tTransferData.setNameAndValue(
                "SGetLimit",
                String.valueOf(mLLSecurityReceiptSchema.getGetLimit()));
//        低段社保未赔金额
        tTransferData.setNameAndValue(
                "LowAmnt", String.valueOf(mLLSecurityReceiptSchema.getLowAmnt()));
//        中段社保未赔金额
        tTransferData.setNameAndValue(
                "MidAmnt", String.valueOf(mLLSecurityReceiptSchema.getMidAmnt()));
        //高段社保未赔金额1
        tTransferData.setNameAndValue("HighAmnt1",
                                      String.valueOf(mLLSecurityReceiptSchema.
                getHighAmnt1()));
        //高段社保未赔金额2
        tTransferData.setNameAndValue("HighAmnt2",
                                      String.valueOf(mLLSecurityReceiptSchema.
                getHighAmnt2()));
        //大单重特病医疗支付段,暂存在StandbyAmnt中
        tTransferData.setNameAndValue("SPlanPart",
                                      String.valueOf(mLLSecurityReceiptSchema.
                getStandbyAmnt()));
        //超高段社保未赔金额
        tTransferData.setNameAndValue("SuperAmnt",
                                      String.valueOf(mLLSecurityReceiptSchema.
                getSuperAmnt()));
        //大病补充支付保险金
        tTransferData.setNameAndValue("SupInHosFee",String.valueOf(mLLSecurityReceiptSchema.
                getSupInHosFee()));
        
        //特需医疗门诊待赔金额
        tTransferData.setNameAndValue("RemDoorFee",
                                      String.valueOf(getRemainDFee()));
        //特需医疗住院待赔金额
        tTransferData.setNameAndValue("RemPatientFee",
                                      String.valueOf(getRemainPFee()));
        
        //是否在基本医疗保险期间
        tTransferData.setNameAndValue("HaltPeriod",String.valueOf(isHaltPeriod()));

        if (!mLCPolSchema.getGrpPolNo().equals("00000000000000000000")) {
            LCGrpPolBL tLCGrpPolBL = new LCGrpPolBL();
            tLCGrpPolBL.setGrpPolNo(mLCPolSchema.getGrpPolNo());
            if (tLCGrpPolBL.getInfo()) {
                LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
                tLCGrpPolSchema.setSchema(tLCGrpPolBL.getSchema());
                //赔付比例
//                tTransferData.setNameAndValue("GetRate",
//                                              String.valueOf(tLCGrpPolSchema.
//                        getGetRate()));
//                //免赔额
//                tTransferData.setNameAndValue("GetLimit",
//                                              String.valueOf(t_GetLimit));
                //备用属性1
                tTransferData.setNameAndValue("StandbyFlag1",
                                              String.valueOf(tLCGrpPolSchema.
                        getStandbyFlag1()));
                //备用属性2
                tTransferData.setNameAndValue("StandbyFlag2",
                                              String.valueOf(tLCGrpPolSchema.
                        getStandbyFlag2()));
                //备用属性3
                tTransferData.setNameAndValue("StandbyFlag3",
                                              String.valueOf(tLCGrpPolSchema.
                        getStandbyFlag3()));
                //住院津贴 HospAllowance
                tTransferData.setNameAndValue("HospAllowance", String.valueOf(getHospAllowance()));
                //保障计划下免赔额
                tTransferData.setNameAndValue("GrpGetLimit", String.valueOf(getGrpGetLimit()));

            }
            //历次保费本息和
            tTransferData.setNameAndValue("PremIntSum",
                                          String.valueOf(getPremIntSum()));
            //开始领取但未领完的年金和
//            tTransferData.setNameAndValue("RemainAnn",
//                                          String.valueOf(getRemainAnn()));
        }
        //保单的备用属性1
        tTransferData.setNameAndValue("PolStandbyFlag1",
                                      String.valueOf(mLCPolSchema.
                getStandbyFlag1()));
        //该责任已经赔付金额累计
        double tSumPay = getSumDutyPay(mLCPolSchema.getPolNo(),
                                       mLCDutySchema.getDutyCode());
        tTransferData.setNameAndValue("SumPay", String.valueOf(tSumPay));
        //套餐编码
        tTransferData.setNameAndValue("RiskWrapCode", mLDWrapSchema.getRiskWrapCode());
        //住院自费金额
        tTransferData.setNameAndValue("HospOwnFee", String.valueOf(getHospOwnFee()));
        //住院费用（不受费用类型控制）
        tTransferData.setNameAndValue("HospFee", String.valueOf(getHospFee()));

        //意外医疗住院费用(不受费用类型控制)
        tTransferData.setNameAndValue("AccHospFee",String.valueOf(getAccHospFee()));
        
        //癌症责任给付标志
        tTransferData.setNameAndValue("isCancerPaidFlag", String.valueOf(getCancerPaidFlag()));
        
        //保单经过年度（复效）
        tTransferData.setNameAndValue("PolYears", String.valueOf(getPolYears()));
        
        //已保天数（复效）
        tTransferData.setNameAndValue("InsuDays", String.valueOf(getInsuDays()));
        //重疾次数
        tTransferData.setNameAndValue("SDCount", String.valueOf(getSDCount()));
        //二次重疾赔付标志
        tTransferData.setNameAndValue("SecSDFlag", getSecSDFlag());
        //首次重疾给付标志
        tTransferData.setNameAndValue("FirSDGive", getFirSDGive());
        
        //被保险人失能对应保单周年纪念日的年龄
        tTransferData.setNameAndValue("DisableAnniAge", String.valueOf(getDisableAnniAge()));
        //被保险人出险对应保单周年纪念日的年龄
        tTransferData.setNameAndValue("PolAge", String.valueOf(getPolAge()));
        //是否失能
        tTransferData.setNameAndValue("DisabilityFlag", String.valueOf(getDisabilityFlag()));
        //观察期
        tTransferData.setNameAndValue("ObserPeriod", String.valueOf(getObserPeriod()));
        //观察期结束时被保险人年龄
        tTransferData.setNameAndValue("ObserveEndAge", String.valueOf(getObserveEndAge()));
        //观察期结束日后相对于保单周年纪念日的年度
        tTransferData.setNameAndValue("ObserveEndAnniAge", String.valueOf(getObserveEndAnniAge()));
        //部分领取总金额
        tTransferData.setNameAndValue("PartGetMoney", String.valueOf(getPartGetMoney()));
        //老年关爱保险金总额
        tTransferData.setNameAndValue("GetMoney", String.valueOf(getGetMoney()));
        //万能账户价值
        tTransferData.setNameAndValue("AccValue",String.valueOf(getAccValue()));
        //全部保费
        tTransferData.setNameAndValue("AllPrem", String.valueOf(getAllPrem()));
        //万能账户日
        tTransferData.setNameAndValue("AccTraceDate", mAccTraceDate);
        
        //重疾存活天数
        tTransferData.setNameAndValue("SeriousDays", String.valueOf(getSeriousDays()));
        //给付责任历史赔付
        tTransferData.setNameAndValue("GetDutySumPay", String.valueOf(getGetDutySumPay()));
        
        // 是否享有社会医疗保险或公费医疗
        tTransferData.setNameAndValue("SecurityFlag", getSecurityFlag());
        // 是否从其他途径获得给付或补偿(0-是，1-否)
        tTransferData.setNameAndValue("OtherWayPay", getOtherWayPay());
        
        // 免赔额
        tTransferData.setNameAndValue("GetLimit", String.valueOf(calGetLimit(tTransferData)));
        //290101险种 Q90唐氏综合征、Q90.9唐氏综合征NOS、Q90.902+唐氏综合征[蒙古种型-先天愚型综合征]、Q90.901 21-三体综合征才能给付该责任
        //add by GY 2013-1-7
        //modify By Houyd 20140310 将231601白血病描入重疾赔付
        //合同约定重大疾病标记
        tTransferData.setNameAndValue("DutyFlag", getDutyFlag());
        //合同约定特定疾病（重大疾病内）标记
        tTransferData.setNameAndValue("SpecialDisease", getSpecialDisease());
              
        //险种历史赔付
        tTransferData.setNameAndValue("RiskMoney", String.valueOf(getRiskMoney()));
        
        //现金价值 
        //好日子险种 专用！！！
        tTransferData.setNameAndValue("CashValue", String.valueOf(getCashValue()));
        
        //获取伤残或残疾信息中的”鉴定日期“与”事件日期“之间间隔的天数
        tTransferData.setNameAndValue("DeforOrInvaDays", String.valueOf(getDeforOrInvaDays()));

        //是否未按期缴纳保费
        tTransferData.setNameAndValue("PayDateFlag", getPayDateFlag());
        System.out.println("是否未按期缴纳保费:"+getPayDateFlag());
        
        //#2216 生产机现金价值错误
        //修改的计算现金价值的参数，参考保全的退保试算
        tTransferData.setNameAndValue("CashValueNew", String.valueOf(getCashValueNew()));
        
        //#1500
        //社保账单增加“不合理费用”//#1500 不合理费用 暂存储在该字段，之前（昆明统筹基金封顶线下保险金赔付 使用了该字段，目前已没有相关业务）
        tTransferData.setNameAndValue("RefuseAmnt", String.valueOf(mLLSecurityReceiptSchema.
                getStandbyAmnt()));
        
//       #1500 合计金额
        tTransferData.setNameAndValue("ApplyAmnt", String.valueOf(mLLSecurityReceiptSchema.
                getApplyAmnt()));

//       #1500 统筹基金支付
        tTransferData.setNameAndValue("PlanFee", String.valueOf(mLLSecurityReceiptSchema.
                getPlanFee()));

//       #1500 公务员医疗补助
        tTransferData.setNameAndValue("OfficialSubsidy", String.valueOf(mLLSecurityReceiptSchema.
                getOfficialSubsidy()));

        //特定疾病医疗费用保险金//#1500
        tTransferData.setNameAndValue("SpecialDiseaseSumFee",
                                      String.valueOf(getSpecialDiseaseSumFee()));
        
        //重疾类别//#1500
        tTransferData.setNameAndValue("SeriousDiseaseCate",
                                      String.valueOf(getSeriousDiseaseCate()));
        //xuyunpeng add #2995 121101险种 start
        tTransferData.setNameAndValue("AccMoneyFee",
        								String.valueOf(getAccMoneyFee()));
        //xuyunpeng add #2995 121101险种 end
        //#2781 xuyunpeng add 税优产品险种 start
        if(checkSyPoint()){        	
        	
        	tTransferData.setNameAndValue("SYMoneyMZ",
        			String.valueOf(getSYMoneyMZ()));
        	
        	//健康咨询费
	        tTransferData.setNameAndValue("HealthConsul",
	        								String.valueOf(getHealthConsul()));
	        //健康评估费
	        tTransferData.setNameAndValue("HealthAass",
	        								String.valueOf(getHealthAass()));
	        //健康档案费
	        tTransferData.setNameAndValue("HealthFile",
	        								String.valueOf(getHealthFile()));
	        //服务费	
	        tTransferData.setNameAndValue("Service",
        								String.valueOf(getService()));
        }
        //#2781 xuyunpeng add 税优产品险种 end
        
        // 2768 福利双全B款 **start**
        tTransferData.setNameAndValue("InUnitDeath",String.valueOf(
                PubFun.calInterval(mAccDate,mLLCaseSchema.getDeathDate(),"D")) );
        // 2768 福利双全B款 **end**
        
        // 2978 健康守望补充医疗保险（B款）**start**
        //申请原因
        tTransferData.setNameAndValue("LLAppClaimReason1", String.valueOf(getLLAppClaimReason()));      
        //事件信息
        tTransferData.setNameAndValue("Llsubreport1", String.valueOf(getLlsubreport()));
        //连续投保标志
        tTransferData.setNameAndValue("ContinuousInsurance", String.valueOf(getContinuousInsurance()));
        //等待期
        tTransferData.setNameAndValue("WaitDate", String.valueOf(getWaitDate()));
        //重疾类别
        tTransferData.setNameAndValue("SeriousDiseaseB",
                                      String.valueOf(getSeriousDisease()));  
        // 2978 健康守望补充医疗保险（B款）**end**
        // #3653 一带一路境外员工团体意外医疗保险及其附加险理赔系统功能开发需求 **start**  
        // 出险日期至账单日期的经过时间
        tTransferData.setNameAndValue("FeeElapsedDate", String.valueOf(getFeeElapsedDate())); 

        // 录入伤残
        tTransferData.setNameAndValue("Disability", String.valueOf(getDisability()));
        // 死亡日期距发生日期的经过天数
        tTransferData.setNameAndValue("AcctoDeath", String.valueOf(getAcctoDeath()));
        // #3653 一带一路境外员工团体意外医疗保险及其附加险理赔系统功能开发需求 **end**
        
        Vector tv = tTransferData.getValueNames();
        LLElementDetailSet tLLElementDetailSet = new LLElementDetailSet();
        
        LDCode1DB tLDCode1DB = new LDCode1DB();
        tLDCode1DB.setCodeType("ClaimCalParam");
        tLDCode1DB.setCode(t_CalCode);
        LDCode1Set tLDCode1Set =  tLDCode1DB.query();
        
        for (int i = 0; i < tv.size(); i++) {
        	String tName = (String) tv.get(i);
            String tValue = (String) tTransferData.getValueByName(tName);
            System.out.println("tName:" + tName + "  tValue:" + tValue);
            
            for (int j = 1 ; j <= tLDCode1Set.size(); j++) {
            	LDCode1Schema tLDCode1Schema = tLDCode1Set.get(j);
            	if (tName.equals(tLDCode1Schema.getCode1())) {
            		LLElementDetailSchema tLLElementDetailSchema = new LLElementDetailSchema();
                    System.out.println("Insert LLElementDetail:tName:" + tName + "  tValue:" + tValue);
                    tLLElementDetailSchema.setCalCode(t_CalCode);
                    tLLElementDetailSchema.setCaseNo(mCaseNo);
                    tLLElementDetailSchema.setCaseRelaNo(mCaseRelaNo);
                    tLLElementDetailSchema.setElementCode(tName);
                    tLLElementDetailSchema.setElementName(tLDCode1Schema.getCodeName());
                    tLLElementDetailSchema.setElementValue(tValue);
                    tLLElementDetailSchema.setMakeDate(mCurrentDate);
                    tLLElementDetailSchema.setModifyDate(mCurrentDate);
                    tLLElementDetailSchema.setMakeTime(mCurrentTime);
                    tLLElementDetailSchema.setModifyTime(mCurrentTime);
                    tLLElementDetailSchema.setPolNo(mPolNo);
                    tLLElementDetailSchema.setContNo(mLCPolSchema.getContNo());
                    tLLElementDetailSchema.setGrpContNo(mLCPolSchema.getGrpContNo());
                    tLLElementDetailSchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
                    tLLElementDetailSchema.setRiskCode(mLCPolSchema.getRiskCode());
                    tLLElementDetailSchema.setDutyCode(mLCDutySchema.getDutyCode());
                    tLLElementDetailSchema.setGetDutyCode(mGetDutyCode);
                    tLLElementDetailSchema.setGetDutyKind(mGetDutyKind);
                    tLLElementDetailSchema.setOperator(mGlobalInput.Operator);
                    tLLElementDetailSchema.setRgtNo(mRgtNo);
                    tLLElementDetailSchema.setMngCom(mGlobalInput.ManageCom);

                    tLLElementDetailSet.add(tLLElementDetailSchema);
            	}
            }
            mCalculator.addBasicFactor(tName, tValue);
        }
        
        tmpMap.put(tLLElementDetailSet, "INSERT");

        String tStr = "";
        System.out.println("CalSQL=" + mCalculator.getCalSQL());
        tStr = mCalculator.calculate();
        if (tStr.trim().equals("")) {
            rValue = 0;
        } else {
            rValue = Double.parseDouble(tStr);
        }
        return rValue;
    }

    /**
     * 计算实际住院天数
     * @return int
     */
    private String getRealHospitalDays() {
        String days = "0";
        String sql = "select sum(RealHospDate) from llfeemain where caseno='" +
                     mCaseNo + "' and caserelano='"
                     + this.mCaseRelaNo + "' and feetype='2'";
        ExeSQL exeSQL = new ExeSQL();
        days = exeSQL.getOneValue(sql);
        try {
        	Integer.parseInt(days);
        } catch (NumberFormatException ex) {
        	days = "0";
        }
        return days;
    }
    
    /**
     * 出险人对应保单周年纪念日的相对年龄
     * @return
     */
    private int getDisableAnniAge(){
    	String validate = mLCPolSchema.getCValiDate();
    	String brithday = mLCPolSchema.getInsuredBirthday();
    	String sql = "select DisabilityDate from LLDisability where caseno='"+mCaseNo+"' and caserelano='"+mCaseRelaNo+"'";
    	ExeSQL exeSQL = new ExeSQL();
        String disabilityDay = exeSQL.getOneValue(sql);
        String maxDay = compareMD(brithday,validate);
        if(disabilityDay!=null && !"".equals(disabilityDay)){
            if(disabilityDay.equals(compareMD(disabilityDay,maxDay))){
        	    return getYear(disabilityDay)-getYear(brithday);
            }else{
        	    return getYear(disabilityDay)-getYear(brithday)-1;
            }
        }else{
        	return 0;
        }
    }
    /**
     * 出险人出险对应保单周年纪念日的相对年龄
     * @return
     */
    private int getPolAge(){
    	String validate = mLCPolSchema.getCValiDate();
    	String brithday = mLCPolSchema.getInsuredBirthday();    	
        String maxDay = compareMD(brithday,validate);
        if(this.mAccDate!=null && !"".equals(this.mAccDate)){
            if(this.mAccDate.equals(compareMD(this.mAccDate,maxDay))){
        	    return getYear(this.mAccDate)-getYear(brithday);
            }else{
        	    return getYear(this.mAccDate)-getYear(brithday)-1;
            }
        }else{
        	return 0;
        }
    }
    /**
     *
     * @return观察期结束后相对于保单生效日的周年数
     */
    private int getObserveEndAnniAge(){
    	String validate = mLCPolSchema.getCValiDate();
    	String brithday = mLCPolSchema.getInsuredBirthday();
    	String sql = "select ObservationDate from lldisability where caseno='"+mCaseNo+"' and caserelano='"+mCaseRelaNo+"'";
    	ExeSQL exeSQL = new ExeSQL();
        String observationDate = exeSQL.getOneValue(sql);
        String maxDay = compareMD(brithday,validate);
        if(observationDate!=null && !"".equals(observationDate)){
            if(observationDate.equals(compareMD(observationDate,maxDay))){
        	    return getYear(observationDate)-getYear(brithday);
            }else{
        	    return getYear(observationDate)-getYear(brithday)-1;
            }
        }else{
        	return 0;
        }
    }
    /**
     *
     * @return　观察期结束时被保险人年龄
     */
    private int getObserveEndAge(){
    	String brithday = mLCPolSchema.getInsuredBirthday();
    	String sql = "select ObservationDate from lldisability where caseno='"+mCaseNo+"' and caserelano='"+mCaseRelaNo+"' ";
    	ExeSQL exeSQL = new ExeSQL();
        String observationDate = exeSQL.getOneValue(sql);
        if(observationDate!=null && !"".equals(observationDate)){
        	String maxDay = compareMD(observationDate,brithday);
            if(observationDate.equals(maxDay)){
        	    return getYear(observationDate)-getYear(brithday);
            }else{
        	    return getYear(observationDate)-getYear(brithday)-1;
            }
        }else{
        	return 0;
        }
   }
    /**
     * 计算a岁对应的保单周年纪念日
     */
    private String getSevenValidate( int a){
    	String cValidate = mLCPolSchema.getCValiDate();
    	String brithday = mLCPolSchema.getInsuredBirthday();
    	int Year = 0;
    	int month = 1;
    	int day = 1;
    	String temp = compareMD(cValidate,brithday);
		month = Integer.parseInt(cValidate.substring(cValidate.indexOf("-")+1,cValidate.lastIndexOf("-") ));
		day = Integer.parseInt(cValidate.substring(cValidate.lastIndexOf("-")+1 ));
    	if(cValidate.equals(temp)){
    		Year = getYear(brithday)+a;
    	}else{
    		Year = getYear(brithday)+a+1;
    	}
    	return getConCatYear(Year,month,day);

    }
    private String getConCatYear(int year,int month,int day){
    	Calendar c= Calendar.getInstance();
    	c.set(Calendar.YEAR,year);
    	c.set(Calendar.MONTH, month-1);
    	c.set(Calendar.DATE, day);
    	Date d = c.getTime();
    	SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
    	String result = s.format(d);
    	return result;

    }
    /**
     * 得到日期的年份
     */
    private int getYear(String a){
    	return Integer.parseInt(a.substring(0,a.indexOf("-")));
    }
    /**
     * 得到两个年份中日期最大的（年份不做比较）
     */
    private String compareMD(String a,String b){
    	int a1 = Integer.parseInt(a.substring(a.indexOf("-")+1, a.lastIndexOf("-")));
    	System.out.println("a1="+a1);
    	int b1 = Integer.parseInt(b.substring(b.indexOf("-")+1,b.lastIndexOf("-")));
    	System.out.println("b1="+b1);
    	int a2 = Integer.parseInt(a.substring(a.lastIndexOf("-")+1));
    	System.out.println("a2="+a2);
    	int b2 = Integer.parseInt(b.substring(b.lastIndexOf("-")+1));
    	System.out.println("b2="+b2);
    	if(a1>b1){
    		return a;
    	}else if(a1<b1){
    		return b;
    	}else{
    		if(a2>=b2){
    			return a;
    		}else{
    			return b;
    		}
    	}
    }
    /**
     *
     * @return 返回是否失能
     */
    private String getDisabilityFlag(){
    	String sql = "select DisabilityFlag from lldisability where caseno='"+mCaseNo+"' and caserelano='"+mCaseRelaNo+"'";
    	 ExeSQL exeSQL = new ExeSQL();
         String result = exeSQL.getOneValue(sql);
    	return result;
    }
    /**
     * #1498 《福利双全个人护理保险》及其附加险 理赔功能开发-需与2216同步
     * 失能实际观察天数
     * @return
     */
    private int getObserPeriod(){
    	String sql = "select DisabilityDate,ObservationDate from lldisability where caseno='"+mCaseNo+"' and caserelano='"+mCaseRelaNo+"' ";
    	ExeSQL exeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = exeSQL.execSQL(sql);
        int days = 0;
        String tDisabilityDate = "";//失能日期
        String tObservationDate = "";//观察期结束日期
        if(tSSRS!=null && tSSRS.getMaxRow()>0){
        	for(int i=1;i<=tSSRS.getMaxRow();i++){
        		tDisabilityDate = tSSRS.GetText(i, 1);
        		tObservationDate = tSSRS.GetText(i, 2);
        		days = PubFun.calInterval(tDisabilityDate,tObservationDate, "D");       		
        	}
        	return days;	    
        }else{
        	return 0;
        }
    }
    /**
     * 计算日期后的部分领取的总金额
     * @return
     */
//    private double getPartGetMoney(String aDate){
//        String tSQL = "select nvl(sum(SumGetMoney),0) from LJAGet a,LPEdorItem b,LPEdorApp c where a.OtherNo = b.EdorAcceptNo and a.OtherNo = c.EdorAcceptNo and c.EdorState = '0' and b.EdorType = 'LQ' and b.ContNo = '"+mLCPolSchema.getContNo()+"' and edorvalidate>='"+aDate+"'";
//    	ExeSQL exeSQL = new ExeSQL();
//        String result = exeSQL.getOneValue(tSQL);
//        if(result!=null && !"".equals(result)){
//            return Double.parseDouble(result);
//        }else{
//            return 0.00;
//        }
//    }
    /**
     * 计算部分领取的总金额
     * @return
     */
    private double getPartGetMoney(){
    	String sql = "select -(sum(getmoney)) from ljagetendorse where feeoperationtype = 'LQ' and grpcontno = '00000000000000000000'" +
    			" and Contno='" +mContNo+
    			"'";
    	ExeSQL exeSQL = new ExeSQL();
        String result = exeSQL.getOneValue(sql);
        if(result!=null && !"".equals(result)){
    	    return Double.parseDouble(result);
        }else{
        	return 0.00;
        }
    }
    /**
     * 计算领取的老年关爱保险金总额
     * @return
     */
    private double getGetMoney(){
    	String sql = " select coalesce(sum(getmoney),0) "
    		+" From ljsgetdraw a,ljaget b "
    		+" where a.getnoticeno = b.actugetno and a.contno ='"+mLCPolSchema.getContNo()+"' "
    		+" and a.getdutykind='0'";
    	ExeSQL exeSQL = new ExeSQL();
        String result = exeSQL.getOneValue(sql);
        if(result!=null && !"".equals(result)){
    	    return Double.parseDouble(result);
        }else{
        	return 0.00;
        }
    }
    /**
     * #1498 《福利双全个人护理保险》及其附加险 理赔功能开发-需与2216同步
     * 计算全部保费
     * @return
     */
    private double getAllPrem(){
    	//正常首期及续期保费
    	String sql1 = "select coalesce(sum(SumActuPayMoney),0) From LJAPay where IncomeNo = '"+mLCPolSchema.getContNo()+"'";
    	ExeSQL exeSQL = new ExeSQL();
        String prem = exeSQL.getOneValue(sql1);
        //保全追加的保费
        String sql2 = "select coalesce(sum(SumActuPayMoney),0) from LJAPay a,LPEdorItem b,LPEdorApp c where a.Incomeno = b.EdorAcceptNo and a.Incomeno = c.EdorAcceptNo and c.EdorState = '0' and b.EdorType in ('ZB') and b.ContNo = '"+mLCPolSchema.getContNo()+"'";
        String Lprem = exeSQL.getOneValue(sql2);
        //保全复效的保费
        String sql3 = "select coalesce(sum(a.GetMoney),0) from ljagetendorse a,LPEdorItem b,LPEdorApp c where a.contno = b.contno and a.endorsementno = c.EdorAcceptNo and c.EdorState = '0' and a.feefinatype='BF' and b.EdorType ='FX' and b.ContNo = '"+mLCPolSchema.getContNo()+"'";
        String FXPrem = exeSQL.getOneValue(sql3);
        return Double.parseDouble(prem)+Double.parseDouble(Lprem)+Double.parseDouble(FXPrem);
    }

     /**
     * 计算万能账户价值
     * @return double
     */
    private double getAccValue() {
        ExeSQL tExeSQL = new ExeSQL();
        //账户价值
        double tAccValue = 0.0;
        //计算日期
        String tCalDate = "";
        //判断是否万能险种，包括附加险
        String tWNRiskCodeSQL = "select riskcode From lmriskapp where riskcode='"
                           + mLCPolSchema.getRiskCode()
                           + "' and risktype4='4' and riskcode not in(select riskcode from lmriskapp where taxoptimal='Y' )" //#2668 税优险种无需计算账户
                           + " union all select b.code1 from lmriskapp a,ldcode1 b "
                           + " where a.riskcode=b.code1 and b.code='"
                           + mLCPolSchema.getRiskCode()
                           + "' and b.codetype='checkappendrisk' and a.risktype4='4' ";
        String tRiskCode = tExeSQL.getOneValue(tWNRiskCodeSQL);
        if (tRiskCode == null || "".equals(tRiskCode) || "null".equals(tRiskCode)) {
            return 0.0;
        }

        //获得账户polno
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mLCPolSchema.getContNo());
        tLCPolDB.setInsuredNo(mLCPolSchema.getInsuredNo());
        tLCPolDB.setRiskCode(tRiskCode);
        LCPolSet tLCPolSet = tLCPolDB.query();
        if (tLCPolSet.size() < 1) {
            return 0.0;
        }
        LCPolSchema tLCPolSchema = tLCPolSet.get(1);

        //获得险种账户类型
        String tWNAccNoSQL = "select b.insuaccno from lmrisktoacc a,LMRiskInsuAcc b "
                           + " where a.insuaccno = b.insuaccno and a.riskcode='"
                           + tRiskCode + "' and b.acctype='002' ";
        String tInsuAccNo = tExeSQL.getOneValue(tWNAccNoSQL);

        if (tInsuAccNo == null || "".equals(tInsuAccNo) || "null".equals(tInsuAccNo)) {
            return 0.0;
        }

        //查询账户
        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
        tLCInsureAccDB.setPolNo(tLCPolSchema.getPolNo());
        tLCInsureAccDB.setInsuAccNo(tInsuAccNo);
        if (!tLCInsureAccDB.getInfo()) {
            return 0.0;
        }
        LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccDB.getSchema();

        //判断处理类型 死亡or失能
        LMDutyGetClmDB tLMDutyGetClmDB = new LMDutyGetClmDB();
        tLMDutyGetClmDB.setGetDutyCode(mGetDutyCode);
        tLMDutyGetClmDB.setGetDutyKind(mGetDutyKind);
        if (!tLMDutyGetClmDB.getInfo()) {
            return 0.0;
        }

        //获取计算日期
        if ("SW".equals(tLMDutyGetClmDB.getStatType())) {
        	if (mLLCaseSchema.getDeathDate()==null) {
                return 0.0;
            }
            tCalDate = mLLCaseSchema.getDeathDate();
        } else {
            String tObservationDateSQL = "select ObservationDate+1 days from lldisability "
                                       + " where caseno='" + mCaseNo
                                       + "' and caserelano='" + mCaseRelaNo + "'";
            SSRS tDateSSRS = tExeSQL.execSQL(tObservationDateSQL);
            if (tDateSSRS.getMaxRow()<1) {
                return 0.0;
            }
            tCalDate = tDateSSRS.GetText(1,1);
            if (tCalDate == null || "".equals(tCalDate) || "null".equals(tCalDate)) {
                return 0.0;
            }
            //比较观察期结束次日与周岁保单周年日
            //zhangjl add
            String tAnniversarySQL = "select 1 from ldcode1 where codetype = 'anniversary' and code =  '" + tRiskCode + "'";
            ExeSQL Anniversarysql = new ExeSQL();
		    		String lRiskCode = Anniversarysql.getOneValue(tAnniversarySQL);	
		    		int Anniversary = 70;
		    		if(lRiskCode!=null && "1".equals(lRiskCode)){
		    		Anniversary = 75;
            }
            String tSeventyDate = getSevenValidate(Anniversary);
            int tDays = PubFun.calInterval(tCalDate,tSeventyDate,"D");
            if (tDays < 0) {
                tCalDate = tSeventyDate;
            }
        }
        mAccTraceDate = tCalDate;
        //将计算日期赋值到账户表中
        tLCInsureAccSchema.setBalaDate(tCalDate);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tLCInsureAccSchema);
        System.out.println("计算账户:"+tLCInsureAccSchema.getPolNo()
                           + "," + tLCInsureAccSchema.getBalaDate() + "账户价值");
        LLAccValueCal tLLAccValueCal = new LLAccValueCal();
         tLLAccValueCal.setMOtherNO(this.mCaseNo);
        try
        {
            if(!tLLAccValueCal.submitData(tVData, ""))
            {
                CError.buildErr(this, tLLAccValueCal.mErrors.getErrContent());
                return 0.0;
            }

         }catch(Exception ex)
         {
             ex.printStackTrace();
             CError.buildErr(this, "结算出现未知异常");
             return 0.0;
         }
         System.out.println("计算出的账户价值AccValue=="+tLLAccValueCal.getAccValue());
         tAccValue = Arith.round(tLLAccValueCal.getAccValue(),2);
         int RgtDays = PubFun.calInterval(mLCPolSchema.getCValiDate(),mAccDate, "D");
         String Incident = getIncidentInfo();
         if(RgtDays > 180 || Incident.equals("1"))
         {
        	 MMap aMMap = tLLAccValueCal.getAccTraceValue();        

             LCInsureAccTraceSet aLCInsureAccTraceSet = new LCInsureAccTraceSet (); 
             LCInsureAccFeeTraceSet aLCInsureAccFeeTraceSet = new LCInsureAccFeeTraceSet (); 
             System.out.println("getAllObjectByObjectName.size===="+aMMap.getAllObjectByObjectName("LCInsureAccTraceSchema", 0).length);         
             for(int i=0; i<aMMap.getAllObjectByObjectName("LCInsureAccTraceSchema", 0).length;i++)
             {
            	 LCInsureAccTraceSchema aLCInsureAccTraceSchema =(LCInsureAccTraceSchema)aMMap.getAllObjectByObjectName("LCInsureAccTraceSchema", 0)[i] ;
            	 String tSerialNo = PubFun1.CreateMaxNo("SERIALNO",mMngCom);
            	 aLCInsureAccTraceSchema.setSerialNo(tSerialNo);
            	 aLCInsureAccTraceSchema.setOtherNo(mCaseNo);
            	 aLCInsureAccTraceSchema.setOtherType("6");  
            	 aLCInsureAccTraceSchema.setState("temp");                     	 
            	 aLCInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
            	 aLCInsureAccTraceSchema.setManageCom(tLCInsureAccSchema.getManageCom());
            	 aLCInsureAccTraceSchema.setMakeDate(mCurrentDate);
            	 aLCInsureAccTraceSchema.setMakeTime(mCurrentTime);
            	 aLCInsureAccTraceSchema.setModifyDate(mCurrentDate);
            	 aLCInsureAccTraceSchema.setModifyTime(mCurrentTime);
            	 aLCInsureAccTraceSet.add(aLCInsureAccTraceSchema); 
            	 
                 //管理费轨迹
                 Reflections rf = new Reflections();
                 LCInsureAccFeeTraceSchema aLCInsureAccFeeTraceSchema= new LCInsureAccFeeTraceSchema();
                 if(!aLCInsureAccTraceSchema.getMoneyType().equals("LX")&&!aLCInsureAccTraceSchema.getMoneyType().equals("VD"))
                 {
                	 LDCodeDB tLDCodeDB =new LDCodeDB();
                	 tLDCodeDB.setCodeType("accmanagefee");
                	 tLDCodeDB.setCodeName(aLCInsureAccTraceSchema.getMoneyType());
                	 if(tLDCodeDB.query().size() >=0 )
                	 {
                		 aLCInsureAccFeeTraceSchema.setFeeCode(tLDCodeDB.query().get(1).getCode());
                	 }
                	 String aFeetraceSerialNo = PubFun1.CreateMaxNo("SERIALNO",mMngCom);
                     rf.transFields(aLCInsureAccFeeTraceSchema, aLCInsureAccTraceSchema); 
                	 aLCInsureAccFeeTraceSchema.setSerialNo(aFeetraceSerialNo);  
                     aLCInsureAccFeeTraceSchema.setFee(-(aLCInsureAccTraceSchema.getMoney())); 
                     aLCInsureAccFeeTraceSchema.setOtherType("6");
                     aLCInsureAccFeeTraceSchema.setState("temp");
                     aLCInsureAccFeeTraceSchema.setOtherNo(mCaseNo);                 
                     aLCInsureAccFeeTraceSchema.setOperator(mGlobalInput.Operator);     
                     aLCInsureAccFeeTraceSchema.setManageCom(tLCInsureAccSchema.getManageCom());
                     aLCInsureAccFeeTraceSchema.setMakeDate(mCurrentDate);
                     aLCInsureAccFeeTraceSchema.setMakeTime(mCurrentTime);
                     aLCInsureAccFeeTraceSchema.setModifyDate(mCurrentDate);
                     aLCInsureAccFeeTraceSchema.setModifyTime(mCurrentTime);
                     //#3209营改增价税分离理赔万能账户相关逻辑调整**start**
                     aLCInsureAccFeeTraceSchema.setMoneyNoTax(null);
                     aLCInsureAccFeeTraceSchema.setMoneyTax(null);
                     aLCInsureAccFeeTraceSchema.setBusiType(null);
                     aLCInsureAccFeeTraceSchema.setTaxRate(null);   
                     //#3209营改增价税分离理赔万能账户相关逻辑调整**end**
                     aLCInsureAccFeeTraceSet.add(aLCInsureAccFeeTraceSchema);
                 }            
             }
             
       
             tmpMap.put(aLCInsureAccTraceSet, "INSERT");  
             tmpMap.put(aLCInsureAccFeeTraceSet, "INSERT"); 
           
         }        
         String strSQL = "delete from LCInsureAccBalance where polno='" + tLCInsureAccDB.getPolNo() +"' and InsuAccNo='000000'";
         tmpMap.put(strSQL, "DELETE");
         tmpMap.add(tLLAccValueCal.getBalanceValue()); 
         return tAccValue; 
    }

    /**
     * 获取推荐医院标志(根据推荐医院生效日期及失效日期来判断暂缺) 1-推荐医院 2-非推荐
     * @return String
     */
    private String getFixHospital() {
		String tAssociateClass = "2";
//		String tBJMAY = "";

//		if (PubFun.calInterval("2011-05-01",mLCContSchema.getInputDate(),"D") >= 0
//				&& "8611".equals(mLCContSchema.getManageCom().substring(0, 4))
//				&& ("1201".equals(mLCPolSchema.getRiskCode()) || "120106"
//						.equals(mLCPolSchema.getRiskCode()))) {
//			tBJMAY = " and hospitcode not in ('1109004','1112002','1115001','1114004','1111004','1111001','1129001','1117001','1113003','1128001','1113001','1113006','1113005','1115002','1116030')";
//		}
		String sql = "select max(associateclass) from ldhospital a where a.HospitCode "
				+ " in (select HospitalCode from LLFeeMain where caseno='"
				+ mCaseNo + "' and caserelano='" + this.mCaseRelaNo + "')" 
				+ " and not exists (select 1 from lddinghospital c where "
				+ " a.Hospitcode=c.Hospitcode and c.managecom=substr('"+mLCContSchema.getManageCom()+"',1,length(c.managecom)) " //支持特定管理机构
				+ " and '"+mLCContSchema.getInputDate()
				+ "' between c.validate and c.cinValidate "//保证保单的InputDate ，C表的validate 和cinValidate 可在2中路径下修改
				+ " and riskcode='"+mLCPolSchema.getRiskCode()+"')";
		ExeSQL exesql = new ExeSQL();
		String res = exesql.getOneValue(sql);
		if (res != null && res.length() > 0 && res.equals("1")) {
			tAssociateClass = res;
		}

		return tAssociateClass;
	}
    
    /**
	 * 套餐份数默认为0
	 * @return
	 */
    private String getWrapCopys() {
		String tCopys = "0";
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setPolNo(mPolNo);
		if (tLCPolDB.getInfo()) {
			try {
				tCopys = String
						.valueOf(Arith.round(tLCPolDB.getCopys(), 0));
			} catch (Exception ex) {
				System.out.println("套餐Copys转换失败");
			}
		}
		return tCopys;
	}
    
    //生存观察期的天数
    private int getTermDay() {
        int tDays=0;
        String tDeathDate="";                                 //死亡日期
        String tRgtDate="";                                   //立案日期
        tDeathDate = mLLCaseSchema.getDeathDate();
        tRgtDate=mLLCaseSchema.getRgtDate();
        if(tDeathDate!=null&&tDeathDate!=""){
          tDays=PubFun.calInterval(mAccDate,tDeathDate, "D");
        }
        else{
            tDays=PubFun.calInterval(mAccDate,tRgtDate, "D");
        }
        return tDays;
    }
    /**
     * 查询重症监护天数
     * @return int
     */
    private String getSeriousWard() {
        String days = "0";
        LLOtherFactorSet tLLOtherFactorSet = new LLOtherFactorSet();
        LLOtherFactorDB tLLOtherFactorDB = new LLOtherFactorDB();
        tLLOtherFactorDB.setCaseNo(mCaseNo);
        tLLOtherFactorDB.setCaseRelaNo(this.mCaseRelaNo);
        tLLOtherFactorSet = tLLOtherFactorDB.query();
        String Serious = "";
        int Count = 0;
        for (int i = 1; i <= tLLOtherFactorSet.size(); i++) {
            LLOtherFactorSchema tSchema = tLLOtherFactorSet.get(i);
            if (tSchema.getFactorCode().equals("1")) { //重症监护
                Serious = "" + tSchema.getValue();
                System.out.println("重症疾病天数" + Serious);
                try {
                    Integer tInteger = new Integer(Serious);
                    Count += tInteger.intValue();
                } catch (Exception ex) {
                    CError.buildErr(this, "重症监护天数输入的不是数字");
                    continue;
                }
            }
        }
        System.out.println("重症疾病天数总和" + Count);
        days = Count + "";
        return days;
    }
    
     /**
     * 有等待期的保额
     * @return int
     */
    private String getSubAmnt() {
        String SubAmnt = String.valueOf(mLCPolSchema.getAmnt());
        //是否做过保全的变更项
        String sql = "select 1 from lpedoritem where Contno = '" +mContNo+ "' and edortype = 'BA'";
        ExeSQL exesql = new ExeSQL();
        String tlpedormain = exesql.getOneValue(sql);
        if (!"".equals(tlpedormain) && !"null".equals(tlpedormain) && tlpedormain != null){
	        //保全日期生效日期+等待期
	        String sql1 = "select EdorValiDate + 180 day from lpedormain where Contno = '" +mContNo+ "'";
	        ExeSQL exesql1 = new ExeSQL();
	        String tEdorValiDate = exesql1.getOneValue(sql1);
	        //理赔申请日期
	        String sql3 = "select rgtdate from llcase where caseno = '" +mCaseNo+ "'";
	        ExeSQL exesql3 = new ExeSQL();
	        String trgtdate = exesql3.getOneValue(sql3);
	        
	        int res = tEdorValiDate.compareTo(trgtdate); 
	        
	        if( res > 0)
	        {
	        	String sql4 = "select Amnt from LPPol where PolNo = '" +mLCPolSchema.getPolNo()+ "'";
	            ExeSQL exesql4 = new ExeSQL();
	            String tSubAmnt = exesql4.getOneValue(sql4);
	            if(!"".equals(tSubAmnt) && !"null".equals(tSubAmnt) && tSubAmnt != null)
	            {
	            	SubAmnt = tSubAmnt;
	            }
	        }
        }
        return SubAmnt;
    }
    
    /**
     * 是否在基本医疗保险期间
     * @return String
     */
    private String isHaltPeriod(){
    String result = "0";
    	String sql = "select 1 from LLOtherFactor where caseno='"+mCaseNo+"' and caserelano='"+mCaseRelaNo+"' and FactorType='9' and FactorCode='9920' ";
    	ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if(res!=null && "1".equals(res)){
        	result="1";
        }
    	return result;
    }

    /**
     * 查询手术级别
     * @return String
     */
    private String getOprateGrade() {
        String sql = "select min( oplevel )  from lloperation where caseno='" +
                     mCaseNo + "' and caserelano='" + this.mCaseRelaNo + "'";
        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0)
            return res;
        return "0";
    }

    /**
     * 查询慢性病，急性病
     * @return String
     */
    private String getDisease1Class() {
        String sql = "select case when ChronicMark='1' then '2' else '1' end "
                     + " from LDDisease where ICDCode in "
                     + "(select DiseaseCode from LLCaseCure where caseno='"
                     + mCaseNo + "' and caserelano='"
                     + this.mCaseRelaNo + "')";
        ExeSQL exesql = new ExeSQL();
        System.out.println("慢性病标志查询语句：" + sql);
        SSRS ssrs = exesql.execSQL(sql);
        if (ssrs != null && ssrs.getMaxRow() > 0) {
            return ssrs.GetText(1, 1);
        }
        return "1";
    }

    /**
     * 是否录入意外信息
     * @return String
     */
    private String getIncidentInfo() {
        String Incident = "0";
        LLAccidentDB llAccidentDB = new LLAccidentDB();
        llAccidentDB.setCaseNo(mCaseNo);
        llAccidentDB.setCaseRelaNo(this.mCaseRelaNo);
        LLAccidentSet set = llAccidentDB.query();
        int accidentCount = set.size();
        if (accidentCount > 0)
            Incident = "1";
        return Incident;
    }

    /**
     * 身故标志 0-未身故 1-疾病身故 2-意外身故
     * @return 返回身故标志
     */
    private String getDeathFlag() {
		String tDeathFlag = "0"; // 未身故
		if (mLLCaseSchema.getDeathDate() != null
				&& !"".equals(mLLCaseSchema.getDeathDate())) {
			tDeathFlag = "1";
			
			LLAccidentDB tLLAccidentDB = new LLAccidentDB();
			tLLAccidentDB.setCaseNo(mCaseNo);
			tLLAccidentDB.setCaseRelaNo(mCaseRelaNo);
			
			if (tLLAccidentDB.query().size() > 0) {
				tDeathFlag = "2";				
			}
		}
		System.out.println("身故标志："+tDeathFlag);
		return tDeathFlag;

	}

    /**
     * 通过案件号查询残疾信息表，若残疾级别为‘1’，即为全残标记，支持残疾及新伤残
     * @return
     */
    private String getSerDiformit() {
        String SerDiformit = "0";
        LLCaseInfoDB tLLCaseInfoDB = new LLCaseInfoDB();
        tLLCaseInfoDB.setCaseNo(mCaseNo);
        tLLCaseInfoDB.setDeformityGrade("1");
        tLLCaseInfoDB.setCaseRelaNo(this.mCaseRelaNo);
        LLCaseInfoSet set = tLLCaseInfoDB.query();
        int accidentCount = set.size();
        if (accidentCount > 0)
            SerDiformit = "1";
        return SerDiformit;
    }


    /**
     * 获取重疾责任标志
     * @return String
     */
    private String getSeriousDiseaseFlag() {
        String SDDutyFlag = "0";
        LLCaseCureDB tLLCaseCureDB = new LLCaseCureDB();
        tLLCaseCureDB.setCaseNo(mCaseNo);
        tLLCaseCureDB.setCaseRelaNo(this.mCaseRelaNo);
        tLLCaseCureDB.setSeriousFlag("1");
        LLCaseCureSet tLLCaseCureSet = tLLCaseCureDB.query();
        int SDCount = tLLCaseCureSet.size();
        for (int i = 1; i <= SDCount; i++) {
            String SDFlag = "" + tLLCaseCureSet.get(i).getSeriousFlag();
            if (SDFlag.equals("1")) {
                SDDutyFlag = "1";
                break;
            }
        }
        return SDDutyFlag;
    }
    
    /**
     * 获取死亡日期
     * @return String
     */
    private String getDeathDate() {
    	String tDeathDate = mLLCaseSchema.getDeathDate();
        return tDeathDate;
    }


    /**
     * 计算伤残级别给付比例
     * @param tLMDutyGetClmSchema LMDutyGetClmSchema
     * @return double
     */
    private double getCaseRate(LMDutyGetClmSchema tLMDutyGetClmSchema) {
        double tCaseRate = 0;

        if (tLMDutyGetClmSchema.getDeformityGrade() == null ||
            tLMDutyGetClmSchema.getDeformityGrade().equals("0")) {
            //身故
            if (tLMDutyGetClmSchema.getDeadValiFlag().equals("Y")) {
                LLAppClaimReasonSet tLLAppClaimReasonSet = new
                        LLAppClaimReasonSet();
                LLAppClaimReasonDB tLLAppClaimReasonDB = new LLAppClaimReasonDB();
                tLLAppClaimReasonDB.setCaseNo(mCaseNo);
                tLLAppClaimReasonSet = tLLAppClaimReasonDB.query();
                for (int i = 1; i <= tLLAppClaimReasonSet.size(); i++) {
                    String aReason = tLLAppClaimReasonSet.get(i).getReasonCode();
                    System.out.println(aReason);
                    if ("05".equals(aReason)) //身故
                        return 1;
                }
            } else
                //给付金无比例要素
                return 1;
        }
        LLCaseInfoSet tLLCaseInfoSet = new LLCaseInfoSet();
        LLCaseInfoDB tLLCaseInfoDB = new LLCaseInfoDB();
        tLLCaseInfoDB.setCaseNo(mCaseNo);
        tLLCaseInfoDB.setCaseRelaNo(this.mCaseRelaNo);
        String CDeformityGrade = tLMDutyGetClmSchema.getDeformityGrade();
        tLLCaseInfoSet = tLLCaseInfoDB.query();
        for (int i = 1; i <= tLLCaseInfoSet.size(); i++) {
            String aDeformityType = tLLCaseInfoSet.get(i).getType();
        	//支持#1777 新伤残评定标准及代码系统定义 新伤残
            if("1".equals(CDeformityGrade)){
                if (aDeformityType.equals("1") || "3".equals(aDeformityType)){
                    tCaseRate += tLLCaseInfoSet.get(i).getDeformityRate();
                }
            }
            if("2".equals(CDeformityGrade)){//烧伤
                if (aDeformityType.equals("2") &&
                        tLLCaseInfoSet.get(i).getDeformityRate() > tCaseRate) {
                	tCaseRate = tLLCaseInfoSet.get(i).getDeformityRate();

                }
            }

        }
        if (tCaseRate > 1) {
            tCaseRate = 1;
        }
        return tCaseRate;
    }

    /**
     * 从分案收据明细中查询事故者发生床位费用总金额
     * @return double
     */
    private double getBedFee() {
        double tInpval = 0;
        String sql = "select sum(fee-preamnt-refuseamnt-selfamnt) "
                + " from LLCaseReceipt where MainFeeNo in "
                + " (select MainFeeNo from LLFeeMain where  caseno='"
                + mCaseNo + "' and caserelano='" + this.mCaseRelaNo
                + "') and FeeItemCode ='201' ";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0)
            return Double.parseDouble(res);

        return tInpval;
    }

    /**
     * 查询各有效性费用项目金额
     * @return double
     */
    private double[] getAvaliFee() {
        String sql =
                "select sum(fee),sum(selfamnt),sum(preamnt),sum(refuseamnt)"
                + " from LLCaseReceipt where MainFeeNo in "
                + " (select MainFeeNo from LLFeeMain where caseno='"
                + mCaseNo + "' and caserelano='" +mCaseRelaNo
                + "') and FeeItemCode in "
                + " (select Feecode from LMDutyGetFeeRela where getdutykind='"
                + mGetDutyKind + "' and getdutycode='" + mGetDutyCode + "')";
System.out.println(sql);
        ExeSQL exesql = new ExeSQL();
        SSRS tssrs = exesql.execSQL(sql);
        double[] afee = {0,0,0,0};
        if(tssrs!=null){
            for (int i = 0; i < tssrs.getMaxCol();i++) {
                try{
                    afee[i] = Double.parseDouble(tssrs.GetText(1,i+1));
                 }catch(Exception ex){
                 }
            }
        }
        return afee;
    }
    /**
         * 意外医疗住院费用
         * @return double
         */
        private double getAccInHospFee() {
            double AcchospFee = 0;
            String AheadDays="180";
            FDate tD = new FDate();
            Date AfterDate = PubFun.calDate(tD.getDate(mAccDate),
                                            Integer.parseInt(AheadDays), "D", null);
            FDate fdate = new FDate();
            String afterdate = fdate.getString(AfterDate);

            String sql = "select sum(fee-preamnt-selfamnt-refuseamnt) "
                     + "from LLCaseReceipt where MainFeeNo in "
                     + "(select MainFeeNo from LLFeeMain where  caseno='"
                     + mCaseNo + "' and feedate<='"+afterdate+"' and caserelano='" + this.mCaseRelaNo
                     + "' and FeeType='2' and FeeItemCode in "
                     //加描述表中的信息
                     + "(select Feecode from LMDutyGetFeeRela where getdutykind='"
                     + this.mGetDutyKind + "' and getdutycode='" +
                     this.mGetDutyCode + "'))";


            ExeSQL exesql = new ExeSQL();
            String res = exesql.getOneValue(sql);
            if (res != null && res.length() > 0)
                return Double.parseDouble(res);
            return AcchospFee;
        }

    /**
    /**
     * 住院费用
     * @return double
     */
    private double getInHospFee() {
        double hospFee = 0;
        String sql = "select sum(fee-preamnt-selfamnt-refuseamnt) "
                     + "from LLCaseReceipt where MainFeeNo in "
                     + "(select MainFeeNo from LLFeeMain where  caseno='"
                     + mCaseNo + "' and caserelano='" + this.mCaseRelaNo
                     + "' and FeeType='2' and FeeItemCode in "
                     //加描述表中的信息
                     + "(select Feecode from LMDutyGetFeeRela where getdutykind='"
                     + this.mGetDutyKind + "' and getdutycode='" +
                     this.mGetDutyCode + "'))";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0)
            return Double.parseDouble(res);
        return hospFee;
    }

    /**
     * 帐单金额
     * @return double
     */
    private double getTabFee() {
        double hospFee = 0;
        String sql = "select sum(sumfee) from LLFeeMain where  caseno='" +
                     mCaseNo + "' and caserelano='" +
                     this.mCaseRelaNo + "'";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0) {
            return Double.parseDouble(res);
        }
        return hospFee;
    }

    /**
     * 门诊费用
     * @return double
     */
    private double getDoorPostFee() {
        double hospFee = 0;
        String sql = "select sum(fee-preamnt-socialPlanAmnt-selfamnt-refuseamnt) "
                + " from LLCaseReceipt where MainFeeNo in (select MainFeeNo "
                + "from LLFeeMain where  caseno='" + mCaseNo
                + "' and caserelano='" + mCaseRelaNo
                + "' and FeeType='1' and FeeItemCode<>'120')";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0) {
            return Double.parseDouble(res);
        }
        return hospFee;
    }
    /**
     * 门急诊门诊费用
     * @return double
     */
    private double getAccDoorPostFee() {
        double AccDoorFee = 0;

        String AheadDays="180";
            FDate tD = new FDate();
            Date AfterDate = PubFun.calDate(tD.getDate(mAccDate),
                                            Integer.parseInt(AheadDays), "D", null);
            FDate fdate = new FDate();
            String afterdate = fdate.getString(AfterDate);

        String sql = "select sum(fee-preamnt-selfamnt-refuseamnt) "
                + " from LLCaseReceipt where MainFeeNo in (select MainFeeNo "
                + "from LLFeeMain where  caseno='" + mCaseNo
                + "' and feedate<='" + afterdate
                + "' and caserelano='" + mCaseRelaNo
                + "' and FeeType='1' and FeeItemCode<>'120')";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0) {
            return Double.parseDouble(res);
        }
        return AccDoorFee;
    }
    /**
     * 补充团体医疗A门诊账单费用(2713)
     * @return double
     */
    private double getDoorSumFee() {
    	double DoorSumFee = 0;

    	String sql = "select nvl(sum(fee-refuseamnt-preamnt),0) "
            + " from LLCaseReceipt where MainFeeNo in (select MainFeeNo "
            + "from LLFeeMain where  caseno='" + mCaseNo
            + "' and caserelano='" + mCaseRelaNo
            + "' and FeeType='1' and FeeAtti ='0')";
    	
    	ExeSQL exesql = new ExeSQL();
    	String res = exesql.getOneValue(sql);
    	if (res != null && res.length() > 0) {
    		return Double.parseDouble(res);
    	}
    	return DoorSumFee;
    }
    /**
     * 补充团体医疗A住院账单合计(2713)
     * @return double
     */
    private double getInHospSumFee() {
    	double DoorSumFee = 0;
    	
    	String sql = "select nvl(sum(fee-refuseamnt-preamnt),0) "
    		+ " from LLCaseReceipt where MainFeeNo in (select MainFeeNo "
    		+ "from LLFeeMain where  caseno='" + mCaseNo
    		+ "' and caserelano='" + mCaseRelaNo
    		+ "' and FeeType='2' and FeeAtti ='0')";
    	
    	ExeSQL exesql = new ExeSQL();
    	String res = exesql.getOneValue(sql);
    	if (res != null && res.length() > 0) {
    		return Double.parseDouble(res);
    	}
    	return DoorSumFee;
    }
    /**
     * 补充A身故前90日内个人基本保险金额的增减 --M的值  (2713)
     * M=该被保险人身故前90日内累计新增的个人基本保险金额-该被保险人身故前90日内累计理赔的医疗保险金。
     * @return double
     */
    private double getBasicAmntM() {
    	double BasicAmntM = 0;
    	String tDeathDate = mLLCaseSchema.getDeathDate();
    	if(tDeathDate==null || "".equals(tDeathDate)||!"170501".equals(mLCPolSchema.getRiskCode())){
    		return 0.0;
    	}
    	//该被保险人身故前90日内累计新增的个人基本保险金额
    	BasicAmntM=LLCaseCommon.getBasicAppendAmnt(mCaseNo, mPolNo, PubFun.calDate(tDeathDate, -90, "D", null), tDeathDate, "LS");
    	//该被保险人身故前90日内累计理赔的医疗保险金
    	String sql = "select nvl(sum(a.pay),0) from ljagetclaim a ,llcase b where a.otherno=b.caseno " +
    			"and b.rgtstate in ('11','12')  and   a.othernotype= '5' " +
    			"and a.getdutykind='000' and a.polno ='"+mPolNo+"'  " +
    			"and a.makedate between '"+PubFun.calDate(tDeathDate, -90, "D", null)+"' and '"+tDeathDate+"' " ;
    	ExeSQL exesql = new ExeSQL();
    	String res = exesql.getOneValue(sql);
    	double realpay90 =Double.parseDouble(res);
    	BasicAmntM-=realpay90;
    	return BasicAmntM;
    }
    /**
     * 补充A该被保险人身故当日零时的个人保险金额  (2713)
     * @return double
     */
    private double getAccAmnt() {
    	double AccAmnt = 0;
    	String tDeathDate = mLLCaseSchema.getDeathDate();
    	if(tDeathDate==null || "".equals(tDeathDate)||!"170501".equals(mLCPolSchema.getRiskCode())){
    		return 0.0;
    	}
    	AccAmnt=LLCaseCommon.getAmntFromAcc(mCaseNo, mPolNo, tDeathDate, "LS");
    	return AccAmnt;
    }
    /**
     * 补充A该被保险人身故当日理赔的医疗保险金(2713)
     * @return double
     */
    private double getCurrentClaimPay() {
    	double tCurrentClaimPay = 0;
    	String tDeathDate = mLLCaseSchema.getDeathDate();
    	if(tDeathDate==null || "".equals(tDeathDate)||!"170501".equals(mLCPolSchema.getRiskCode())){
    		return 0.0;
    	}
    	String sql = "select nvl(sum(a.pay),0) from ljagetclaim a ,llcase b where a.otherno=b.caseno " +
		"and b.rgtstate in ('11','12')  and   a.othernotype= '5' " +
		"and a.getdutykind='000' and a.polno ='"+mPolNo+"'  " +
		"and a.makedate ='"+tDeathDate+"' " ;

    	ExeSQL exesql = new ExeSQL();
    	String res = exesql.getOneValue(sql);
    	tCurrentClaimPay =Double.parseDouble(res);
    	return tCurrentClaimPay;
    }
    /*2713  以上5个方法是任务2713补充A新加的*/


    /**
     * 救护车费用
     * @return double
     */
    private double getAmbuFee() {
        double AmbuFee = 0;
        String sql =
                "select sum(fee-preamnt-selfamnt-refuseamnt) from "
                + " LLCaseReceipt where MainFeeNo in (select MainFeeNo from "
                + "LLFeeMain where  caseno='" + mCaseNo + "' and caserelano='"
                + mCaseRelaNo + "') and FeeItemCode='120'";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0) {
            return Double.parseDouble(res);
        }
        return AmbuFee;
    }

    /**
     * 从分案收据明细中查询事故者发生床位费用总金额
     * @return double
     */
    private double getPEFee() {
        double tInpval = 0;
        String sql = "select sum(fee-preamnt-refuseamnt-selfamnt) from "
                + " LLCaseReceipt where MainFeeNo in (select MainFeeNo from "
                + " LLFeeMain where  caseno='" + mCaseNo
                + "' and caserelano='" + this.mCaseRelaNo
                + "') and FeeItemCode ='227' ";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0)
            return Double.parseDouble(res);

        return tInpval;
    }

    /**
     * 费用险的责任每分到各个费用细项，但是某个费用有日均限额，查询相关的拒付金额
     * @return double
     */
    private double getDayOverFee() {
        double OverFlow = 0;

        double hospFee = 0;
        hospFee = Arith.round(hospFee, 2);
        String strSql = "select * from LMDutyGetFeeRela where getdutykind='"
                        + this.mGetDutyKind + "' and getdutycode='" +
                        this.mGetDutyCode + "' and Dayfeemaxctrl is not null and Dayfeemaxctrl <>'' ";
        LMDutyGetFeeRelaDB tLMDutyGetFeeRelaDB = new LMDutyGetFeeRelaDB();
        LMDutyGetFeeRelaSet tLMDutyGetFeeRelaSet = new LMDutyGetFeeRelaSet();
        tLMDutyGetFeeRelaSet = tLMDutyGetFeeRelaDB.executeQuery(strSql);
        System.out.println("查询日限额" + strSql);
        if (tLMDutyGetFeeRelaSet == null || tLMDutyGetFeeRelaSet.size() <= 0) {
            return 0;
        } else {
            for (int i = 1; i <= tLMDutyGetFeeRelaSet.size(); i++) {
                LMDutyGetFeeRelaSchema tLMDutyGetFeeRelaSchema = new
                        LMDutyGetFeeRelaSchema();
                tLMDutyGetFeeRelaSchema = tLMDutyGetFeeRelaSet.get(i);
                double Daylimit = 0.0;
                if (tLMDutyGetFeeRelaSchema.getDayFeeMAXCtrl().equals("01")) {
                    Daylimit = tLMDutyGetFeeRelaSchema.getDayFeeMaxValue();
                }
                System.out.println(Daylimit);
                String sql =
                        "select sum(fee-selfamnt-preamnt-refuseamnt) from LLCaseReceipt "
                        +
                        " where MainFeeNo in (select MainFeeNo from LLFeeMain where caseno='"
                        + mCaseNo + "' and caserelano='" +
                        this.mCaseRelaNo + "')"
                        + " and FeeItemCode ='" +
                        tLMDutyGetFeeRelaSchema.getFeecode() + "'";
                ExeSQL exesql = new ExeSQL();
                String spefee = exesql.getOneValue(sql);
                if (!spefee.equals("null") && !spefee.equals(""))
                    hospFee = Double.parseDouble(spefee);
                String D = getRealHospitalDays();
                double hosdays = 0;
                if(D!=null && !"".equals(D)){
                	hosdays=Integer.parseInt(D);
                }
                double tDlimit = Daylimit * hosdays;
                if (hospFee > tDlimit) {
                    OverFlow += (hospFee - tDlimit);
                }
            }
            return OverFlow;
        }
    }

    private double getMaxConsume(){
        double perLimit = 0.0;
        if(mLCDutySchema.getStandbyFlag2()!=null)
            perLimit = Double.parseDouble(mLCDutySchema.getStandbyFlag2());
        double totalLimit = 0.0;
        double maxconsume = 0.0;
        LCContPlanDutyParamDB tContPlanParamDB = new LCContPlanDutyParamDB();
        tContPlanParamDB.setGrpContNo(mLCPolSchema.getGrpContNo());
        tContPlanParamDB.setRiskCode(mLCPolSchema.getRiskCode());
        tContPlanParamDB.setContPlanCode(mLCPolSchema.getContPlanCode());
        tContPlanParamDB.setDutyCode(mLCDutySchema.getDutyCode());
        tContPlanParamDB.setCalFactor("Amnt");
//        tContPlanParamDB.setCalFactorType("0");
        LCContPlanDutyParamSet tContPlanParamSet = tContPlanParamDB.query();
        if(tContPlanParamSet.size()>0){
            String stLimit = tContPlanParamSet.get(1).getCalFactorValue();
            if(stLimit!=null&&!stLimit.equals("")){
                totalLimit = Double.parseDouble(stLimit);
                SpendAmnt = stLimit;
            }
        }
        
        // 修改为通过实赔金额反推限额
        String tsql = "select NVL(sum(b.realpay),0),NVL(sum(b.OutDutyAmnt),0) from llcase a,llclaimdetail b "
				+ " where a.caseno=b.caseno and a.rgtstate in ('09','11','12') and b.polno='"
				+ mLCPolSchema.getPolNo() + "' and b.dutycode='"
				+ mLCDutySchema.getDutyCode() + "' and b.caseno<>'" + mCaseNo
				+ "'";
       // ExeSQL texesql = new ExeSQL();
        //String exconsume = texesql.getOneValue(tsql);
        String exconsume =null;
        String tOutDutyAmnt =null;
        ExeSQL texesql = new ExeSQL();
        SSRS trr=texesql.execSQL(tsql);
        if(trr.getMaxRow()>0){
            try{
            	exconsume = trr.GetText(1,1);
            }catch(Exception ex){
                System.out.println("历史实赔金额获取错误！");
            }
            try{
            	tOutDutyAmnt = trr.GetText(1,2);
            }catch(Exception ex){
                System.out.println("历史免赔额获取错误！");
            }
        }
        double texconsume = 0.0;
        if(!exconsume.equals("null")&&!exconsume.equals("")){
            texconsume = Double.parseDouble(exconsume);
        }
        
        // 反推下
        System.out.println(mLCDutySchema.getGetRate());
        System.out.println(Double.parseDouble(tOutDutyAmnt));
        System.out.println(texconsume);
        if (mLCDutySchema.getGetRate() > 0) {
			texconsume = Arith.round((texconsume / mLCDutySchema.getGetRate())
					+ Double.parseDouble(tOutDutyAmnt), 2);
		}
        
        totalLimit -=texconsume;
        if (perLimit > 0 && totalLimit > 0) {
            maxconsume = totalLimit > perLimit ? perLimit : totalLimit;
        } else if (perLimit <= 0) {
            maxconsume = totalLimit;
        } else if (totalLimit <= 0) {
            maxconsume = perLimit;
        }

        return maxconsume;
    }

    /**
     * 计算免赔额
     * 考虑到年度共用免赔额的问题（还有事件共用免赔额的问题没解决）
     * 2011-4-14 之前的年度免陪其实是保单期间免陪，新增真正的保单年度免陪  YearGetLimitFlag 2
     * @param YearGetLimitFlag String
     * @return double
     */
    private double getGetLimit(LMDutyGetClmSchema aLMDutyGetClmSchema) {
        String YearGetLimitFlag = aLMDutyGetClmSchema.getYearGetLimitFlag();
        double getlimit = mLCDutySchema.getGetLimit();
        ExeSQL tExeSQL = new ExeSQL();
        
        //针对个险条款中规定免赔额的情况，如果没有从契约取到，从产品取
        if (getlimit <= 0) {
        	getlimit = aLMDutyGetClmSchema.getDeductible();

			if (mLDWrapSchema.getRiskWrapCode() != null
					&& !"".equals(mLDWrapSchema.getRiskWrapCode())) {
				
				LDRiskDutyWrapDB tLDRiskDutyWrapDB = new LDRiskDutyWrapDB();
				tLDRiskDutyWrapDB.setRiskWrapCode(mLDWrapSchema
						.getRiskWrapCode());
				tLDRiskDutyWrapDB.setRiskCode(mLCPolSchema.getRiskCode());
				tLDRiskDutyWrapDB.setDutyCode(mLCDutySchema.getDutyCode());
				tLDRiskDutyWrapDB.setCalFactor("GetLimit");

				LDRiskDutyWrapSet tLDRiskDutyWrapSet = tLDRiskDutyWrapDB
						.query();
				if (tLDRiskDutyWrapSet.size() > 0) {
					if (tLDRiskDutyWrapSet.get(1).getCalFactorValue() != null
							&& "".equals(tLDRiskDutyWrapSet.get(1)
									.getCalFactorValue())) {
						try {
							getlimit = Double.parseDouble(tLDRiskDutyWrapSet
									.get(1).getCalFactorValue());
						} catch (Exception ex) {
							System.out.println("免赔额值不是数字！");
						}
					}
				} else {
					getlimit = 0.0;
				}

			}
		}

        
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLCPolSchema.getGrpContNo());
        String adutyYearFlag = "";
        //暂时使用契约GetFlag字段，非标准业务实施后从新取值
        if (tLCGrpContDB.getInfo())
            adutyYearFlag += tLCGrpContDB.getGetFlag();
        if (getlimit < 0.001) {
            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            tLCInsuredDB.setInsuredNo(mLCPolSchema.getInsuredNo());
            tLCInsuredDB.setContNo(mLCPolSchema.getContNo());
            String workstate = "";
            if (tLCInsuredDB.getInfo())
                workstate = tLCInsuredDB.getInsuredStat();
            String sql = "select distinct FactorValue from LDRiskParamPrint "
                         +" where factorname='GetLimit' and riskcode='"
                         +mLCPolSchema.getRiskCode()+"' and ParamValue2='"
                         +mGetDutyCode+"' and (ParamValue3 is "
                         +" null or ParamValue3='"+workstate+"')";
            ExeSQL texesql = new ExeSQL();
            SSRS trr=texesql.execSQL(sql);
            if(trr.getMaxRow()>0){
                try{
                    getlimit = Double.parseDouble(trr.GetText(1,1));
                }catch(Exception ex){
                    System.out.println("免赔额值不是数字！");
                }
            }
        }
        System.out.println("adutyYearFlag:" + adutyYearFlag);
        if (adutyYearFlag.equals("1")) {
            String adutycode = mLCDutySchema.getDutyCode();
            for (int k = 1; k <= mSaveLLClaimDetailSet.size(); k++) {
                if (mSaveLLClaimDetailSet.get(k).getDutyCode().equals(
                        adutycode) && mSaveLLClaimDetailSet.get(k).getPolNo().equals(mPolNo)) {
                    if (!mSaveLLClaimDetailSet.get(k).getCaseRelaNo().equals(
                            mCaseRelaNo))
                        getlimit -= mSaveLLClaimDetailSet.get(k).getOutDutyAmnt();
                    else {
                        if (!mSaveLLClaimDetailSet.get(k).getGetDutyCode().
                            equals(mGetDutyCode) ||
                            !mSaveLLClaimDetailSet.get(k).getGetDutyKind().
                            equals(mGetDutyKind))
                            getlimit -= mSaveLLClaimDetailSet.get(k).
                                    getOutDutyAmnt();
                    }
                }
            }
            LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
            tLLClaimDetailDB.setPolNo(mLCPolSchema.getPolNo());
            tLLClaimDetailDB.setDutyCode(adutycode);
            LLClaimDetailSet tLLClaimDetailSet = tLLClaimDetailDB.query();
            for (int k = 1; k <= tLLClaimDetailSet.size(); k++) {
                LLCaseDB tLLCaseDB = new LLCaseDB();
                tLLCaseDB.setCaseNo(tLLClaimDetailSet.get(k).getCaseNo());
                if (!tLLCaseDB.getInfo())
                    System.out.println("查询不到案件信息");
                if (tLLCaseDB.getRgtState().equals("09") ||
                    tLLCaseDB.getRgtState().equals("11")
                    || tLLCaseDB.getRgtState().equals("12"))
                    getlimit -= tLLClaimDetailSet.get(k).getOutDutyAmnt();
            }
        } else {
            if (YearGetLimitFlag == null)
                YearGetLimitFlag = "";
            if (YearGetLimitFlag.equals("1")) {
            	System.out.println( " mSaveLLClaimDetailSet.size()"+mSaveLLClaimDetailSet.size());
            	for (int k = 1; k <= mSaveLLClaimDetailSet.size(); k++) {
					if (mSaveLLClaimDetailSet.get(k).getGetDutyCode().equals(
							mGetDutyCode)
							&& !mSaveLLClaimDetailSet.get(k).getCaseRelaNo()
									.equals(mCaseRelaNo) && mSaveLLClaimDetailSet.get(k).getPolNo().equals(mPolNo))
						getlimit -= mSaveLLClaimDetailSet.get(k)
								.getOutDutyAmnt();
				}

				String tOldLimitSQL = "select nvl(sum(b.outdutyamnt),0) "
						+ " from llcase a,llclaimdetail b "
						+ " where a.caseno=b.caseno "
						+ " and a.rgtstate in ('09','11','12') "
						+ " and b.polno='"
						+ mPolNo
						+ "' and b.getdutycode='"
						+ mGetDutyCode
						+ "' and not exists (select 1 from llappeal where caseno=a.caseno and appealno='"
						+ mCaseNo
						+ "') and not exists ( select 1 from llappeal m,llappeal n where m.caseno=n.caseno "
						+ " and a.caseno=n.appealno and m.appealno='" + mCaseNo
						+ "')";
				
				String tOldLimitValue = tExeSQL.getOneValue(tOldLimitSQL);
				double tOldLimit = 0;
				if (tOldLimitValue != null && !"".equals(tOldLimitValue)) {
					tOldLimit = Double.parseDouble(tOldLimitValue);
				}

				getlimit -= tOldLimit;
			} else if ("2".equals(YearGetLimitFlag)) {
				for (int k = 1; k <= mSaveLLClaimDetailSet.size(); k++) {
					if (mSaveLLClaimDetailSet.get(k).getGetDutyCode().equals(
							mGetDutyCode)
							&& !mSaveLLClaimDetailSet.get(k).getCaseRelaNo()
									.equals(mCaseRelaNo) && mSaveLLClaimDetailSet.get(k).getPolNo().equals(mPolNo)) {
						getlimit -= mSaveLLClaimDetailSet.get(k)
								.getOutDutyAmnt();
					}
				}
				String tEndDate = CommonBL.getPolInvalidate(mPolNo);
				String tCValiDate = mLCPolSchema.getCValiDate();

				String tPolDateSQL = "select date('" + tCValiDate
						+ "') + year(date('"+mAccDate+"')-date('" + tCValiDate
						+ "')) year, " + " date(minvalue(days(date('"
						+ tCValiDate + "') + (year(date('"+mAccDate+"')-date('"
						+ tCValiDate + "'))+1) year) " + " ,days('" + tEndDate
						+ "'))) from dual where 1=1 with ur";

				System.out.println(tPolDateSQL);

				SSRS tPolDateSSRS = tExeSQL.execSQL(tPolDateSQL);
				String tSDate = "";
				String tEDate = "";
				if (tPolDateSSRS.getMaxRow() > 0) {
					tSDate = tPolDateSSRS.GetText(1, 1);
					tEDate = tPolDateSSRS.GetText(1, 2);
				}

				String tOldLimitSQL = "select nvl(sum(b.outdutyamnt),0) "
						+ " from llcase a,llclaimdetail b,llcaserela c,llsubreport d "
						+ " where a.caseno=b.caseno "
						// 申诉纠错的没有对应的事件，关联原事件，caserelano需要截取
						+ " and substr(b.caserelano,1,14)=c.caserelano and c.subrptno=d.subrptno "
						+ " and a.rgtstate in ('09','11','12') "
						+ " and b.polno='"
						+ mPolNo
						+ "' and b.getdutycode='"
						+ mGetDutyCode
						+ "' and d.accdate >= '"
						+ tSDate
						+ "' and d.accdate < '"
						+ tEDate
						+ "' and not exists (select 1 from llappeal where caseno=a.caseno and appealno='"
						+ mCaseNo
						+ "') and not exists ( select 1 from llappeal m,llappeal n where m.caseno=n.caseno "
						+ " and a.caseno=n.appealno and m.appealno='" + mCaseNo
						+ "')";

				String tOldLimitValue = tExeSQL.getOneValue(tOldLimitSQL);
				double tOldLimit = 0;
				if (tOldLimitValue != null && !"".equals(tOldLimitValue)) {
					tOldLimit = Double.parseDouble(tOldLimitValue);
				}

				getlimit -= tOldLimit;

			}
        }
        if (getlimit < 0.01)
            getlimit = 0;
        System.out.println("************免赔额*************：" +
                           Arith.round(getlimit, 2));
        return Arith.round(getlimit, 2);
    }

    /**
     * 查询帐单中自费部分
     * @return double
     */
    private double getOwnFee() {
        double OwnFee = 0.0;
        String sql =
                "select sum(selfamnt) from LLCaseReceipt where MainFeeNo in "
                + " (select MainFeeNo from LLFeeMain where  caseno='" +
                mCaseNo + "' and caserelano='" +
                this.mCaseRelaNo + "'"
                //  +"' and FeeType='0'"
                //加描述表中的信息
                +
                " and FeeItemCode in (select Feecode from LMDutyGetFeeRela where getdutykind='"
                + this.mGetDutyKind + "' and getdutycode='" +
                this.mGetDutyCode + "'))";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0) {
            OwnFee = Double.parseDouble(res);
        }
        return OwnFee;
    }

    /**
     * 取社保帐单中各分段值（理算前）
     * @return boolean
     */
    private boolean getSecuReceipt() {
        mLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
        String sql = "select * from LLSecurityReceipt where MainFeeNo in " +
                     " (select MainFeeNo from LLFeeMain where  caseno='" +
                     mCaseNo + "' and caserelano='" +
                     this.mCaseRelaNo + "')";
        System.out.println(sql);
        LLSecurityReceiptDB tLLSecurityReceiptDB = new LLSecurityReceiptDB();
        LLSecurityReceiptSet tLLSecurityReceiptSet = new LLSecurityReceiptSet();
        tLLSecurityReceiptSet = tLLSecurityReceiptDB.executeQuery(sql);
        if (tLLSecurityReceiptSet != null && tLLSecurityReceiptSet.size() > 0) {
            int srcount = tLLSecurityReceiptSet.size();
            mLLSecurityReceiptSchema = tLLSecurityReceiptSet.get(1);
            double lowamnt = 0.0;
            double midamnt = 0.0;
            double highamnt1 = 0.0;
            double highamnt2 = 0.0;
            double supamnt = 0.0;
            double sDoorpay = 0.0;
            double cDoorpay = 0.0;
            double bDoorpay = 0.0;
            double appamnt = 0.0;
            double overamnt = 0.0;
            double selfpay2 = 0.0;
            double selfamnt = 0.0;
            double standbyamnt = 0.0;//#1500 不合理费用 暂存储在该字段，之前（昆明统筹基金封顶线下保险金赔付 使用了该字段，目前已没有相关业务）
            //#1500
            double planfee = 0.0;//社保账单增加“本次统筹基金医疗费用”
            double supinhosfee = 0.0;//社保账单增加“本次住院大额医疗费用”
            double officialsubsidy = 0.0;//社保账单增加“本次公务员医疗补助费用”
            double applyamnt = 0.0;//社保账单增加“本次合计金额费用”

            for (int i = 1; i <= srcount; i++) {
                lowamnt += tLLSecurityReceiptSet.get(i).getLowAmnt();
                midamnt += tLLSecurityReceiptSet.get(i).getMidAmnt();
                highamnt1 += tLLSecurityReceiptSet.get(i).getHighAmnt1();
                highamnt2 += tLLSecurityReceiptSet.get(i).getHighAmnt2();
                supamnt += tLLSecurityReceiptSet.get(i).getSuperAmnt();
                sDoorpay += tLLSecurityReceiptSet.get(i).getSmallDoorPay();
                cDoorpay += tLLSecurityReceiptSet.get(i).getEmergencyPay();
                bDoorpay += tLLSecurityReceiptSet.get(i).getHighDoorAmnt();
                appamnt += tLLSecurityReceiptSet.get(i).getFeeInSecu();
                selfpay2 += tLLSecurityReceiptSet.get(i).getSelfPay2();
                selfamnt += tLLSecurityReceiptSet.get(i).getSelfAmnt();
                standbyamnt += tLLSecurityReceiptSet.get(i).getStandbyAmnt();
                planfee += tLLSecurityReceiptSet.get(i).getPlanFee();
                supinhosfee += tLLSecurityReceiptSet.get(i).getSupInHosFee();
                officialsubsidy += tLLSecurityReceiptSet.get(i).getOfficialSubsidy();
                applyamnt += tLLSecurityReceiptSet.get(i).getApplyAmnt();

            }
//            if (appamnt>mLCDutySchema.getAmnt())
//            {
//                if(mLCDutySchema.getAmnt()>0.001)
//                    overamnt = appamnt - mLCDutySchema.getAmnt();
//                supamnt = supamnt - overamnt;
//            }
            mLLSecurityReceiptSchema.setLowAmnt(lowamnt);
            mLLSecurityReceiptSchema.setMidAmnt(midamnt);
            mLLSecurityReceiptSchema.setHighAmnt1(highamnt1);
            mLLSecurityReceiptSchema.setHighAmnt2(highamnt2);
            mLLSecurityReceiptSchema.setSuperAmnt(supamnt);
            mLLSecurityReceiptSchema.setSmallDoorPay(sDoorpay);
            mLLSecurityReceiptSchema.setEmergencyPay(cDoorpay);
            mLLSecurityReceiptSchema.setHighDoorAmnt(bDoorpay);
            mLLSecurityReceiptSchema.setSecuRefuseFee(overamnt);
            mLLSecurityReceiptSchema.setSelfPay2(selfpay2);
            mLLSecurityReceiptSchema.setSelfAmnt(selfamnt);
            mLLSecurityReceiptSchema.setStandbyAmnt(standbyamnt);
            mLLSecurityReceiptSchema.setPlanFee(planfee);
            mLLSecurityReceiptSchema.setSupInHosFee(supinhosfee);
            mLLSecurityReceiptSchema.setOfficialSubsidy(officialsubsidy);
            mLLSecurityReceiptSchema.setApplyAmnt(applyamnt);
           
        }
        return true;
    }

    /**
     * 取社保帐单分段金额（理算后）
     * @return double
     */
    private double getClaimMoney(String CalCode) {
        double claimmoney = 0.0;
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("llcalclaimamnt");
        tLDCodeDB.setCode(CalCode+mLCPolSchema.getManageCom());
        
        String ClaimName = "";
        if (!tLDCodeDB.getInfo()) {
        	tLDCodeDB = new LDCodeDB();
        	tLDCodeDB.setCodeType("llcalclaimamnt");
        	tLDCodeDB.setCode(CalCode);
            if (tLDCodeDB.getInfo()) {
            	ClaimName = "" + tLDCodeDB.getCodeName();            	
            } else {
            	havetab = false;
            	return 0;
            }
        } else {
            havetab = true;
            ClaimName = "" + tLDCodeDB.getCodeName();
        }
        if (ClaimName.equals("DoorPostFee")||ClaimName.equals("EmergencyPay")) {
            claimmoney = getDoorPostFee();
        } else if("DoorPostFee+HospFee".equals(ClaimName)){
        	 claimmoney = getDoorPostFee()+getHospFee();
        } else if (ClaimName.equals("RemDoorFee")) {
            claimmoney = getRemainDFee();
        } else if (ClaimName.equals("RemPatientFee")) {
            claimmoney = getRemainPFee();
        } else if ("RemPatientFee+RemDoorFee".equals(ClaimName)) {
            claimmoney = getRemainPFee()+getRemainDFee();
        } else if ("OwnFeeC".equals(ClaimName)){
            claimmoney = getOwnFeeC();
        } else {
            String sql = "select " + ClaimName +
                         " from LLSecurityReceipt where MainFeeNo " +
                         " in (select MainFeeNo from LLFeeMain where  caseno='" +
                         mCaseNo + "' and caserelano='" +
                         this.mCaseRelaNo + "')";
            System.out.println(sql);
            ExeSQL execsql = new ExeSQL();
            SSRS ssrs = execsql.execSQL(sql);
            if (ssrs != null) {
                for (int j = 1; j <= ssrs.getMaxRow(); j++) {
                    claimmoney += Double.parseDouble(ssrs.GetText(j, 1));
                }
            }
        }
        return Arith.round(claimmoney, 2);
    }

    //计算额外保险金
    private double getAdditionalPay(String polno) {
        double additionalpay = 0.0;
        for (int k = 1; k <= mSaveLLClaimDetailSet.size(); k++) {
            LLClaimDetailSchema DetailSchema = mSaveLLClaimDetailSet.get(k);
            if (!DetailSchema.getGetDutyCode().equals(mGetDutyCode) &&
                DetailSchema.getCaseRelaNo().equals(mCaseRelaNo) &&
                DetailSchema.getPolNo().equals(polno))
                additionalpay += DetailSchema.getRealPay();
        }
        return additionalpay;
    }

    /**
     * 计算特需待赔的门诊费用
     * @return boolean
     */
    private double getRemainDFee() {
        double remaindoorfee = 0.0;
        LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
        LLFeeMainSet tLLFeeMainSet = new LLFeeMainSet();
        tLLFeeMainDB.setCaseNo(mCaseNo);
        tLLFeeMainDB.setCaseRelaNo(mCaseRelaNo);
        tLLFeeMainDB.setFeeType("1");
        tLLFeeMainSet = tLLFeeMainDB.query();
        for (int i = 1; i <= tLLFeeMainSet.size(); i++) {
            if (tLLFeeMainSet.get(i).getFeeAtti().equals("3"))
                remaindoorfee += tLLFeeMainSet.get(i).getRemnant();
            if (tLLFeeMainSet.get(i).getFeeAtti().equals("0"))
                remaindoorfee += tLLFeeMainSet.get(i).getSumFee();
        }
        return remaindoorfee;
    }

    /**
     * 计算特需待赔的住院费用
     * @return boolean
     */
    private double getRemainPFee() {
        double remainpatientfee = 0.0;
        LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
        LLFeeMainSet tLLFeeMainSet = new LLFeeMainSet();
        tLLFeeMainDB.setCaseNo(mCaseNo);
        tLLFeeMainDB.setCaseRelaNo(mCaseRelaNo);
        tLLFeeMainDB.setFeeType("2");
        tLLFeeMainSet = tLLFeeMainDB.query();
        for (int i = 1; i <= tLLFeeMainSet.size(); i++) {
            if (tLLFeeMainSet.get(i).getFeeAtti().equals("3"))
                remainpatientfee += tLLFeeMainSet.get(i).getRemnant();
            if (tLLFeeMainSet.get(i).getFeeAtti().equals("0"))
                remainpatientfee += tLLFeeMainSet.get(i).getSumFee();
        }
        return remainpatientfee;
    }

    /**
     * 计算保单在责任下的累计赔付金额
     * @param tPolNo String
     * @param tDutyCode String
     * @return double
     */
    private double getSumDutyPay(String tPolNo, String tDutyCode) {
        LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
        LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
        String tSql = "select * from llclaimdetail where polno='" +
                      tPolNo + "' and dutycode ='" + tDutyCode + "' ";
        tSql = tSql +
               " and clmno in(select clmno from llclaim where ClmState='2' or ClmState='3')";
        System.out.println(tSql);
        tLLClaimDetailSet = tLLClaimDetailDB.executeQuery(tSql);
        System.out.println("LLClaimDetailSet.size=" + tLLClaimDetailSet.size());
        double tSumDuty = 0;
        for (int k = 1; k <= tLLClaimDetailSet.size(); k++) {
            LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
            tLLClaimDetailSchema = tLLClaimDetailSet.get(k);
            tSumDuty = tSumDuty + tLLClaimDetailSchema.getRealPay();
        }
        return tSumDuty;
    }

    /**
     * 检查该张保单下所有责任赔付是否超过该责任的保额
     * 查询保单所有责任记录
     * 添加续保的方法
     */
    private double getacupay(String aGetDutyCode, String aCaseRelaNo) {
        double acuLimit = 0;
        //查询保单所有的给付记录（包含本次赔付）
        //#2781  xuyunpeng add  续保支持 start
        //首先会判断险种是否是税优险，其他险种不会走此逻辑
        double tSumDuty = 0;
        String sql = "select 1 from lcpol a,lbpol b "
 		   +" where a.prtno=b.prtno and a.grpcontno='00000000000000000000' "
    		   +" and a.polno='"+mLCPolSchema.getPolNo()+"'";
        ExeSQL tExeSQL=new ExeSQL();
        SSRS tXbaoS=tExeSQL.execSQL(sql); 
        if(checkSyPoint()&&tXbaoS.getMaxRow()>0){
        	tSumDuty = checkSyXB();
        }else{
        	
        	LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
        	LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
        	String tSql = "select * from llclaimdetail where polno='" +
        	mLCPolSchema.getPolNo() + "' ";
        	tSql = tSql + " and caseno in (select caseno from llcase where "
        	+ " customerno ='" + mLCPolSchema.getInsuredNo()
        	+ "' and rgtstate in ('09','11','12')) ";
        	System.out.println(tSql); //不包括本次理赔
        	tLLClaimDetailSet = tLLClaimDetailDB.executeQuery(tSql);
        	System.out.println("LLClaimDetailSet.size=" + tLLClaimDetailSet.size());
        	for (int k = 1; k <= tLLClaimDetailSet.size(); k++) {
        		LLClaimDetailSchema tLLClaimDetailSchema = new
        		LLClaimDetailSchema();
        		tLLClaimDetailSchema = tLLClaimDetailSet.get(k);
        		if (tLLClaimDetailSchema.getDutyCode().equals(mLCDutySchema.
        				getDutyCode())) {
        			tSumDuty = tSumDuty + tLLClaimDetailSchema.getRealPay();
        		}
        	}
        }
        //#2781 xuyunpeng add 续保支持 end
        double tThisSumDuty = 0; //本次理赔赔款
        for (int k = 1; k <= this.mSaveLLClaimDetailSet.size(); k++) {
            LLClaimDetailSchema tLLClaimDetailSchema = new
                    LLClaimDetailSchema();
            tLLClaimDetailSchema = this.mSaveLLClaimDetailSet.get(k);
            if (tLLClaimDetailSchema.getDutyCode().equals(mLCDutySchema.
                    getDutyCode())
                && !tLLClaimDetailSchema.getGetDutyCode().equals(aGetDutyCode)
                && !tLLClaimDetailSchema.getCaseRelaNo().equals(aCaseRelaNo)) {
                tThisSumDuty = tThisSumDuty + tLLClaimDetailSchema.getRealPay();
            }
        }
        System.out.println(mLCDutySchema.getDutyCode() + "已赔付=" + tSumDuty +
                           tThisSumDuty);
        System.out.println(mLCDutySchema.getDutyCode() + "保额=" +
                           mLCDutySchema.getAmnt());
        if (mLCDutySchema.getAmnt() > 0) {
            acuLimit = mLCDutySchema.getAmnt() - tSumDuty - tThisSumDuty;
            checkAmnt = true;
        } else {
            checkAmnt = false;
        }
        //检查保单是否超过保单保额
        //添加续保的方法
        SynLCLBGetBL tSynLCLBGetBL = new SynLCLBGetBL();
        tSynLCLBGetBL.setPolNo(mLCPolSchema.getPolNo());
        LCGetSet tLCGetSet = new LCGetSet();
        tLCGetSet.set(tSynLCLBGetBL.query());
        if (tSynLCLBGetBL.mErrors.needDealError()) {
            CError.buildErr(this, "险种保单给付责任查询失败");
            return 0;
        }
        //添加续保的处理方法
//        LCPolBL tLCPolBL = new LCPolBL();
//        tLCPolBL.setPolNo(mLCPolSchema.getPolNo());
//        if (!tLCPolBL.getInfo()) {
//            CError.buildErr(this, "险种保单查询失败");
//            return 0;
//        }
//        LCPolSchema tLCPolSchema = new LCPolSchema();
//        tLCPolSchema.setSchema(tLCPolBL.getSchema());

        double tSumPay = 0;
        for (int j = 1; j <= tLCGetSet.size(); j++) {
            LCGetSchema tLCGetSchema = new LCGetSchema();
            tLCGetSchema.setSchema(tLCGetSet.get(j));
            if ("1".equals(tLCGetSchema.getLiveGetType())) {
                tSumPay = tSumPay + tLCGetSchema.getSumMoney();
            }
        }
        System.out.println("tSumPay=" + tSumPay);
        
        // 取险种特殊保额
        double tRiskAmnt = mLCPolSchema.getAmnt();
        tRiskAmnt = getSpecialAmnt(mLCPolSchema.getRiskCode(), mLCPolSchema.getAmnt());
      //#2995 xuyunpengg add 添加对121101险种的保额的特殊处理 #2995 险种层
		if("121101".equals(mLLClaimPolicySchema.getRiskCode())&&"0".equals(getSecurityFlag())){
			tRiskAmnt = 1.05*tRiskAmnt;
		}
		//#2995 xuyunpengg add 添加对121101险种的保额的特殊处理 #2995 险种层
        mLCPolSchema.setAmnt(tRiskAmnt);
        
        System.out.println("Amnt=" + mLCPolSchema.getAmnt());

        double tempGet = mLCPolSchema.getAmnt() - tSumPay;
        if (tempGet < acuLimit && mLCPolSchema.getAmnt() > 0 && checkAmnt) {
            acuLimit = tempGet;
        } else if (mLCPolSchema.getAmnt() > 0 && !checkAmnt) {
            acuLimit = tempGet;
        }

        if (mLCPolSchema.getAmnt() > 0 || mLCDutySchema.getAmnt() > 0) {
            checkAmnt = true;
        } else {
            checkAmnt = false;
        }

        if(acuLimit <0.01)
        {
            return 0;
        }
        return acuLimit;
    }

    /**
     * 计算契约协议中定的特殊理赔算法
     * @return boolean
     */
    //取得保险金额的方法
    public  double getAmnt(String PolNo,String DutyCode,String GetDutyCode){
        double tFinalAmnt=0;
        System.out.println(PolNo);
        System.out.println(DutyCode);
        System.out.println(GetDutyCode);
        //取得lcget表中的责任明细保额
        SynLCLBGetBL tSynLCLBGetBL = new SynLCLBGetBL();
        tSynLCLBGetBL.setPolNo(PolNo);
        tSynLCLBGetBL.setDutyCode(DutyCode);
        tSynLCLBGetBL.setGetDutyCode(GetDutyCode);
        if (!tSynLCLBGetBL.getInfo()) {
            CError.buildErr(this, "LCGet表查询失败");
            return 0;
        }

        LCGetSchema tLCGetSchema = new LCGetSchema();
        tLCGetSchema.setSchema(tSynLCLBGetBL.getSchema());

       if (tSynLCLBGetBL.mErrors.needDealError()) {
           CError.buildErr("", "险种保单给付责任查询失败");
           return 0;
       }
       double tAmnt1=mLCPolSchema.getAmnt();
       double tAmnt2=mLCDutySchema.getAmnt();
       double tAmnt3=tLCGetSchema.getStandMoney();
       System.out.println(tAmnt1);
       System.out.println(tAmnt2);
       System.out.println(tAmnt3);
       if(tAmnt1>tAmnt2){
           tFinalAmnt=tAmnt1;
       }else{
           tFinalAmnt=tAmnt2;
       }
       if(tFinalAmnt<tAmnt3){
           tFinalAmnt=tAmnt3;
       }
System.out.println(tFinalAmnt);
        return tFinalAmnt;
    }
//    private boolean calSpecial(TransferData tTransferData) {
//        //查找计算要素
//        LCFactoryDB tLCFactoryDB = new LCFactoryDB();
//        PubCalculator tPubCalculator = new PubCalculator();
//        LCFactorySet tResultLCFactorySet = new LCFactorySet();
//
//        String tIndivSql = "select * from LCFactory where GrpPolNo='" +
//                           this.mLCPolSchema.getGrpPolNo() + "' and polno='" +
//                           this.mLCPolSchema.getPolNo() + "' ";
//        String tCollSql = "select * from LCFactory where GrpPolNo='" +
//                          this.mLCPolSchema.getGrpPolNo() +
//                          "' and polno='00000000000000000000'";
//        String tGetDutySql = " and factorytype='000002' and otherno in(select GetDutyCode from LMDutyGetRela where DutyCode='" +
//                             this.mLCDutySchema.getDutyCode() + "')";
//        String tDutySql = " and factorytype='000001' and otherno='" +
//                          this.mLCDutySchema.getDutyCode() + "'";
//        String tPolSql = " and factorytype='000000' and otherno='" +
//                         this.mLCPolSchema.getPolNo() + "'";
//        String tGrpPolSql = " and factorytype='000000' and otherno='" +
//                            this.mLCPolSchema.getGrpPolNo() + "'";
//        //处理个单协议中的给付信息
//        String tsql = tIndivSql + tGetDutySql;
//        System.out.println(tsql);
//        LCFactorySet t0LCFactorySet = tLCFactoryDB.executeQuery(tsql);
//        tResultLCFactorySet.add(t0LCFactorySet);
//
//        //处理个单协议中的责任信息
//        tsql = tIndivSql + tDutySql;
//        System.out.println(tsql);
//        LCFactorySet t1LCFactorySet = tLCFactoryDB.executeQuery(tsql);
//        tResultLCFactorySet.add(t1LCFactorySet);
//
//        //处理个单协议中的保单信息
//        tsql = tIndivSql + tPolSql;
//        System.out.println(tsql);
//        LCFactorySet t2LCFactorySet = tLCFactoryDB.executeQuery(tsql);
//        tResultLCFactorySet.add(t2LCFactorySet);
//
//        //处理团单协议中的给付信息
//        tsql = tCollSql + tGetDutySql;
//        System.out.println(tsql);
//        LCFactorySet t3LCFactorySet = tLCFactoryDB.executeQuery(tsql);
//        tResultLCFactorySet.add(t3LCFactorySet);
//
//        //处理团单协议中的责任信息
//        tsql = tCollSql + tDutySql;
//        System.out.println(tsql);
//        LCFactorySet tLCFactorySet = tLCFactoryDB.executeQuery(tsql);
//        tResultLCFactorySet.add(tLCFactorySet);
//
//        //处理团单协议中的保单信息
//        tsql = tCollSql + tGrpPolSql;
//        System.out.println(tsql);
//        LCFactorySet t5LCFactorySet = tLCFactoryDB.executeQuery(tsql);
//        tResultLCFactorySet.add(t5LCFactorySet);
//
//        Vector tv = tTransferData.getValueNames();
//        for (int i = 0; i < tv.size(); i++) {
//            String tName = (String) tv.get(i);
//            String tValue = (String) tTransferData.getValueByName(tName);
//            System.out.println("tName:" + tName + "  tValue:" + tValue);
//            tPubCalculator.addBasicFactor(tName, tValue);
//        }
//        System.out.println("load data to PubCalculator end");
//
//        for (int i = 1; i <= tResultLCFactorySet.size(); i++) {
//            LCFactorySchema tLCFactorySchema = new LCFactorySchema();
//            tLCFactorySchema = tResultLCFactorySet.get(i);
//            tPubCalculator.setCalSql(tLCFactorySchema.getCalSql());
//            String tResult = tPubCalculator.calculate();
//            if (tResult != null && !tResult.trim().equals("")) {
//                if (tTransferData.findIndexByName(tLCFactorySchema.
//                                                  getFactoryName()) ==
//                    -1) {
//                    tTransferData.setNameAndValue(tLCFactorySchema.
//                                                  getFactoryName(),
//                                                  tResult);
//                }
//            }
//        }
//        return true;
//        //如果没有协议要素，取default值
//    }

    //被保人0岁保单生效对应日
    private String getInsuredvalideBirth() {
        FDate tInsuredBirthday = new FDate();
        FDate tCValidate = new FDate();
        tCValidate.getDate(mLCPolSchema.getCValiDate());
        Date tInsuredvalideBirth = null;
        tInsuredvalideBirth = PubFun.calDate(tInsuredBirthday.getDate(
                mLCPolSchema.getInsuredBirthday()), 0, "Y",
                                             tCValidate.getDate(mLCPolSchema.
                getCValiDate()));
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        return df.format(tInsuredvalideBirth);
    }

    /**
     * 开始领取但尚未领完的年金
     * @return double
     */
//    private double getRemainAnn() {
//        SynLCLBGetBL aSynLCLBGetBL = new SynLCLBGetBL();
//        aSynLCLBGetBL.setPolNo(this.mLCPolSchema.getPolNo());
//
//        LCGetSet tLCGetSet = new LCGetSet();
//        tLCGetSet = aSynLCLBGetBL.query();
//        double tReturnMoney = 0;
//        for (int i = 1; i <= tLCGetSet.size(); i++) {
//            LCGetSchema tLCGetSchema = new LCGetSchema();
//            tLCGetSchema = tLCGetSet.get(i);
//            if (tLCGetSchema.getLiveGetType().equals("1"))
//                continue;
//            FDate tFDate = new FDate();
//            if (tFDate.getDate(tLCGetSchema.getGetStartDate()).
//                after(tFDate.getDate(mAccDate)))
//                continue;
//
//            if (tLCGetSchema.getGetIntv() == 0)
//                continue;
//
//            int NoneGetNum = PubFun.calInterval(tLCGetSchema.getGettoDate(),
//                                                tLCGetSchema.getGetEndDate(),
//                                                "M") / tLCGetSchema.getGetIntv();
//            tReturnMoney += NoneGetNum * tLCGetSchema.getStandMoney();
//        }
//        return tReturnMoney;
//    }

    private double getAccPay(String aPolNo,String aGetDutyCode){
        double accpay = 0;
        for(int i=1;i<=mSaveLLClaimDetailSet.size();i++){
            LLClaimDetailSchema tclmdetail = mSaveLLClaimDetailSet.get(i);
            if(tclmdetail.getPolNo().equals(aPolNo) && tclmdetail.getGetDutyCode().equals(aGetDutyCode))
                accpay += tclmdetail.getRealPay();
        }
        return accpay;
    }
    /**
     * 保险人个人帐户的帐户金额
     * @return double
     * 需要补充扣除管理费和利息，但是相应表结构未设计且产品为做相应要求，待做
     */
    private double getInsuAccBala(double exp_getmoney) {
        if (exp_getmoney < 0.01)
            return 0.0;
        double actmoney = 0.0;
        double tem_money = 0.0;
        LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
        tLCInsureAccTraceDB.setPolNo(mLCPolSchema.getPolNo());
        LCInsureAccTraceSet tLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
        String toSerialNo = "";
        if (tLCInsureAccTraceSet.size() > 0) {
            for (int i = 1; i <= tLCInsureAccTraceSet.size(); i++) {
                if (tLCInsureAccTraceSet.get(i).getOtherNo().equals(mCaseNo)) {
                    //如果该案件在轨迹表里已经有记录,则serialno不变
                    toSerialNo = tLCInsureAccTraceSet.get(i).getSerialNo();
                    continue;
                }
                tem_money += tLCInsureAccTraceSet.get(i).getMoney();
            }
        } else
            return 0.0;
        //本次案件的每条责任的给付记录
        for (int i = 1; i <= mLCInsureAccTraceSet.size(); i++) {
            tem_money += mLCInsureAccTraceSet.get(i).getMoney();
        }
        if (tem_money < 0.01)
            return 0.0;
        if (tem_money > exp_getmoney)
            actmoney = exp_getmoney;
        else
            actmoney = tem_money;
        double decrease = 0 - actmoney;
        String tSerialNo = mRgtNo.substring(1);
        if (toSerialNo.equals(""))
            tSerialNo = PubFun1.CreateMaxNo("LLACCTRACENO", "86"+mCaseNo.substring(1,5));
         else
            tSerialNo = toSerialNo;
        LCInsureAccTraceSchema tLCInsureAccTraceSchema = tLCInsureAccTraceSet.get(1);
        tLCInsureAccTraceSchema.setSerialNo(tSerialNo);
        tLCInsureAccTraceSchema.setState("temp");
        tLCInsureAccTraceSchema.setMoney(decrease);
        tLCInsureAccTraceSchema.setMoneyType("PK");
        tLCInsureAccTraceSchema.setOtherNo(mCaseNo);
        tLCInsureAccTraceSchema.setOtherType("5");
        mLCInsureAccTraceSet.add(tLCInsureAccTraceSchema);

        return actmoney;
    }

    //帐户存取轨迹保存
    private boolean getInsuAcc() {
    	String tsql = "select a.polno from lcpol a,lmrisk b where a.riskcode=b.riskcode "
	             +" and b.insuaccflag = 'Y' and a.insuredno = '"
	             +mLCPolSchema.getInsuredNo()+"'" 
	             +" union "
		           +"select a.polno from lbpol a,lmrisk b where a.riskcode=b.riskcode "
		           +" and b.insuaccflag = 'Y' and a.insuredno = '"
		           +mLCPolSchema.getInsuredNo()+"'";
	             
	   ExeSQL tesql = new ExeSQL();
	   SSRS tss = tesql.execSQL(tsql);
	   
	   LCInsureAccTraceSet sinsuacctraceset = new LCInsureAccTraceSet();
	   for(int i=1;i<=tss.getMaxRow();i++){
	       String tPolNo = tss.GetText(i, 1);
	       double totalclaim = 0;
	       for(int j=1;j<=mSaveLLClaimDetailSet.size();j++){
	           LLClaimDetailSchema tclmdetail = mSaveLLClaimDetailSet.get(j);
	           LMDutyGetDB tLMDutyGetDB = new LMDutyGetDB();
	           tLMDutyGetDB.setGetDutyCode(tclmdetail.getGetDutyCode());
	           tLMDutyGetDB.setNeedAcc("1");
	           if(tLMDutyGetDB.query().size()>=1)
	           {
	           	 if(tclmdetail.getPolNo().equals(tPolNo))
	                    totalclaim += tclmdetail.getRealPay();
	           }
	          
	       }
	       totalclaim = Arith.round(totalclaim,2);
	       if (totalclaim > 0) {
	       	System.out.println("#################进入%%%%%%%%%%%%"+sinsuacctraceset.size());
	       	GetAccAmnt tgetaccamnt = new GetAccAmnt();
	           tgetaccamnt.getAccPay(tPolNo, mCaseNo, totalclaim);
	           sinsuacctraceset.add(tgetaccamnt.getTrace());
	           for(int j=1;j<=sinsuacctraceset.size();j++)
	           {
	           	sinsuacctraceset.get(j).setPayDate(mAccTraceDate);                	                	
	           }
	           System.out.println("****************"+sinsuacctraceset.size());
	           backMsg += tgetaccamnt.getAccMsg();
	       }
	   }
	   tmpMap.put(sinsuacctraceset, "DELETE&INSERT");
	   return true;
    }

    //计算历次交费的本息和
    private double getPremIntSum() {
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if (!tLLRegisterDB.getInfo()) {
            CError.buildErr(this, "立案信息查询失败");
            return 0.0;
        }
        double tReturnGet = 0.0;
        LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
        LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
        tLJAPayPersonDB.setPolNo(mLCPolSchema.getPolNo());
        tLJAPayPersonDB.setPayType("ZC");
        tLJAPayPersonSet = tLJAPayPersonDB.query();
//    LMLoanDB tLMLoanDB = new LMLoanDB();
//    tLMLoanDB.setRiskCode(mLCPolSchema.getRiskCode());
//    tLMLoanDB.getInfo();
        double rate = 0.025;
        AccountManage tAccountManage = new AccountManage();
        for (int i = 1; i <= tLJAPayPersonSet.size(); i++) {
            double interest = 0.0;
            LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
            tLJAPayPersonSchema = tLJAPayPersonSet.get(i);
            interest = tAccountManage.getInterest(
                    tLJAPayPersonSchema.getSumActuPayMoney(),
                    tLJAPayPersonSchema.getConfDate(),
                    tLLRegisterDB.getAccidentDate(), rate, "1", "2", "D");
            tReturnGet += tLJAPayPersonSchema.getSumActuPayMoney() + interest;
        }
        return tReturnGet;
    }

    /**
     * 套餐编码
     * @return String
     */
    private String getRiskWrapCode(){
        String tRiskWrapCode = "";
    	String tSQL = "";
    	//个单和团单不一样
    	if ("1".equals(mLCPolSchema.getContType())) {
    		tSQL = "SELECT DISTINCT riskwrapcode FROM LCRiskDutyWrap WHERE prtno='"+mLCPolSchema.getPrtNo()
    		            + "' AND riskcode='"+mLCPolSchema.getRiskCode()+"' ";
    	} else {
    		tSQL = "select DISTINCT riskwrapcode from lccontplanrisk where riskwrapflag='Y' and grpcontno='"
    			 + mLCPolSchema.getGrpContNo()+"' and contplancode='"+mLCPolSchema.getContPlanCode()
    			 + "' and riskcode = '"+mLCPolSchema.getRiskCode()+"' ";
    	}
    	
    	ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(tSQL);
		if (tSSRS.getMaxRow()>0){
			tRiskWrapCode = tSSRS.GetText(1,1);
			mLDWrapSchema.setRiskWrapCode(tRiskWrapCode);
	    }
    	
        return tRiskWrapCode;
    }

    /**
     * 住院自费费用
     * @return double
     */
    private double getHospOwnFee(){
    double OwnFee = 0.0;
        String sql ="select sum(selfamnt) from LLCaseReceipt where MainFeeNo in "
                   + " (select MainFeeNo from LLFeeMain where  caseno='"
                   + mCaseNo + "' and caserelano='" +
                   this.mCaseRelaNo
                   + "' and FeeType='2') " ;
        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0) {
            OwnFee = Double.parseDouble(res);
        }
        return OwnFee;
    }
    
    /**
     * 住院费用（不受费用类型控制）
     * @return double
     */
    private double getHospFee(){
        double HospFee = 0.0;
        String sql ="select sum(fee-selfamnt-preamnt-refuseamnt) from LLCaseReceipt where MainFeeNo in "
                   + " (select MainFeeNo from LLFeeMain where  caseno='"
                   + mCaseNo + "' and caserelano='" +
                     this.mCaseRelaNo
                   + "' and FeeType='2') " ;
        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0) {
            HospFee = Double.parseDouble(res);
        }
        return HospFee;
    }
    
    /**
     * 校验满结责任
     * @param aRiskCode String
     * @return boolean
     */
    private boolean checkMJ(String aRiskCode){
        ExeSQL tExeSQL = new ExeSQL();
        String tMJSQL = "select distinct d.getdutycode from lmrisk a,lmriskduty b,lmdutygetrela c,lmdutygetalive d "
                          + " where a.riskcode=b.riskcode and b.dutycode=c.dutycode "
                          + " and c.getdutycode=d.getdutycode and a.getflag='Y' "
                          + " and ((d.getdutykind='12' and d.afterget='003') "
                          + " or d.getdutykind='0' ) and a.riskcode='" + aRiskCode +
                          "' and exists (select 1 from lmriskapp where riskcode=a.riskcode and risktype4<>'4')";
        SSRS tMJSSRS = tExeSQL.execSQL(tMJSQL);
        for (int i=1; i<=tMJSSRS.getMaxRow(); i++) {
            if (!LLCaseCommon.checkMJ(mPolNo,tMJSSRS.GetText(i,1))){
                return false;
            }
        }
        return true;
    }
    
    /**
     * 癌症责任是否赔付
     * @return String
     */
    private String getCancerPaidFlag() {
        ExeSQL tExeSQL = new ExeSQL();
        String tSQL = "select coalesce(sum(pay),0) from ljagetclaim where othernotype='5' and polno='"
                      + mPolNo +"' and getdutykind='404'";
        String tSumPay = tExeSQL.getOneValue(tSQL);
        double tPay = Double.parseDouble(tSumPay);
        if (tPay>0) {
            return "1";
        } else {
            return "0";
        }
    }

    /**
     * 保单经过年度
     * @return String
     */
    private String getPolYears() {
        ExeSQL tExeSQL = new ExeSQL();
        int tYears = 0;
        String tSQL = "select max(b.edorvalidate) From lpedorapp a,lpedoritem b where a.edoracceptno = b.edoracceptno "
                      + " and edortype in('FX','TF') and a.edorstate ='0' "
                      + " and b.contno ='" + mContNo + "'";
        String tValiDate = tExeSQL.getOneValue(tSQL);
        if (tValiDate.equals("")||tValiDate == null) {
            tYears = PubFun.calInterval(mLCPolSchema.getCValiDate(),mAccDate, "Y");
        } else {
            tYears = PubFun.calInterval(tValiDate,mAccDate, "Y");
        }
        return String.valueOf(tYears);
    }

    /**
     * 已保天数（复效）
     * @return String
     */
    private String getInsuDays() {
        ExeSQL tExeSQL = new ExeSQL();
        int tDays = 0;
        String tSQL = "select max(b.edorvalidate) From lpedorapp a,lpedoritem b where a.edoracceptno = b.edoracceptno "
                      + " and edortype in('FX','TF') and a.edorstate ='0' "
                      + " and b.contno ='" + mContNo + "'";
        String tValiDate = tExeSQL.getOneValue(tSQL);
        if (tValiDate.equals("")||tValiDate == null) {
            tDays = PubFun.calInterval(mLCPolSchema.getCValiDate(),mAccDate, "D");
        } else {
            tDays = PubFun.calInterval(tValiDate,mAccDate, "D");
        }
        return String.valueOf(tDays);
    }
    
    /**
     * 保单生效后重疾次数
     * @return String
     */
    private String getSDCount() {
        ExeSQL tExeSQL = new ExeSQL();
        String tSQL = "select count(1) from llcasecure a,llcase c where a.seriousflag='1' "
                      + " and a.caseno=c.caseno and c.rgttype='1' and c.rgtstate<>'14' "
                      + " and c.customerno='"+mLCPolSchema.getInsuredNo()
                      + "' and a.diagnosedate > '"+mLCPolSchema.getCValiDate()+"'";
        String tCount = tExeSQL.getOneValue(tSQL);
        return tCount;
    }

    /**
     * 二次重疾赔付标志
     * @return String
     */
    private String getSecSDFlag() {
        String tSecSDFlag = "0";
        ExeSQL tExeSQL = new ExeSQL();
        String tFDSQL = "select b.diagnosedate "
                        + " from ljagetclaim a,llcasecure b,llcase c,llcaserela e "
                        + " where a.otherno=c.caseno and b.caseno=c.caseno and b.caserelano=e.caserelano "
                        + " and e.caseno=c.caseno and a.othernotype='5' and b.seriousflag='1' "
                        + " and a.polno='"+mLCPolSchema.getPolNo()+"' and a.getdutykind in ('400','401','402') ";
        SSRS tFDSSRS = tExeSQL.execSQL(tFDSQL);
        if (tFDSSRS.getMaxRow()>0) {
            String tSDSQL = "select '1' from llcasecure a,llcase c "
                            + " where c.caseno=a.caseno "
                            + " and c.caseno='"+mCaseNo+"' and a.caserelano='"+mCaseRelaNo
                            + "' and a.seriousflag='1' "
                            + " and days(a.diagnosedate)-days(date('"+tFDSSRS.GetText(1,1)+"'))>365 "
                            + " and days(coalesce(c.deathdate,current date))-days(a.diagnosedate)>28 ";
            String tFlag = tExeSQL.getOneValue(tSDSQL);
            if ("1".equals(tFlag)) {
                tSecSDFlag = "1";
            }
        }
        return tSecSDFlag;
    }

    /**
     * 首次重疾给付标志
     * @return String
     */
    private String getFirSDGive() {
        ExeSQL tExeSQL = new ExeSQL();
        String tSQL = "select coalesce(sum(pay),0) from ljagetclaim where othernotype='5' and polno='"
                      + mPolNo +"' and getdutykind in ('400','401','402')";
        String tSumPay = tExeSQL.getOneValue(tSQL);
        double tPay = Double.parseDouble(tSumPay);
        if (tPay>0) {
            return "0";
        } else {
            return "1";
        }
    }

    /**
     * 意外住院费用（不受费用项控制）
     * @return double
     */
    private double getAccHospFee() {
        double AcchospFee = 0;
        String AheadDays="180";
        FDate tD = new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(mAccDate),
                                            Integer.parseInt(AheadDays), "D", null);
        FDate fdate = new FDate();
        String afterdate = fdate.getString(AfterDate);

        String sql = "select sum(fee-preamnt-selfamnt-refuseamnt) "
                   + "from LLCaseReceipt where MainFeeNo in "
                   + "(select MainFeeNo from LLFeeMain where  caseno='"
                   + mCaseNo + "' and feedate<='"+afterdate+"' and caserelano='" + this.mCaseRelaNo
                   + "' and FeeType='2' )";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0)
            return Double.parseDouble(res);
        return AcchospFee;
    }

    /**
     * 查询帐单中自费部分(不受费用项目控制)
     * @return double
     */
    private double getOwnFeeC() {
        double OwnFee = 0.0;
        String sql =
        	    "select sum(preamnt+selfamnt+refuseamnt) from LLCaseReceipt where MainFeeNo in "
                + " (select MainFeeNo from LLFeeMain where  caseno='" +
                mCaseNo + "' and caserelano='" +
                this.mCaseRelaNo + "'"
                //  +"' and FeeType='0'"
                //加描述表中的信息
                +
                " )";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0) {
            OwnFee = Double.parseDouble(res);
        }
        return Arith.round(OwnFee,2);
    }

    /**
     * 查询项目金额，账单金额，自费金额，先期给付，不合理费用
     * @return double
     */
    private double[] getAvaliFeeC() {
        String sql =
                "select sum(fee),sum(selfamnt),sum(preamnt),sum(refuseamnt)"
                + " from LLCaseReceipt where MainFeeNo in "
                + " (select MainFeeNo from LLFeeMain where caseno='"
                + mCaseNo + "' and caserelano='" +mCaseRelaNo
                + "') ";
        System.out.println(sql);
        ExeSQL exesql = new ExeSQL();
        SSRS tssrs = exesql.execSQL(sql);
        double[] afee = {0,0,0,0};
        if(tssrs!=null){
            for (int i = 0; i < tssrs.getMaxCol();i++) {
                try{
                    afee[i] = Double.parseDouble(tssrs.GetText(1,i+1));
                 }catch(Exception ex){
                 }
            }
        }
        return afee;
    }
    
    /**
     * 查询被保险人当前年龄
     * @return String
     */
    private int getCurrentAge() {
    	FDate fDate = new FDate();
    	Date Birthday = fDate.getDate(mLCPolSchema.getInsuredBirthday());
        String strNow = mCurrentDate;
        Date aNow = fDate.getDate(strNow);
        int CustomerAge = PubFun.calInterval(Birthday, aNow, "Y");
        System.out.println("年龄计算函数。。。"+CustomerAge);
        return CustomerAge;
    }
    
    /**
     * 重疾观察天数
     * 已经死亡的取死亡至重疾确诊日期，否则取受理日期
     * @return
     */
    private int getSeriousDays() {
    	int tSerouseDays = 0;
    	ExeSQL tExeSQL = new ExeSQL();
        String tSQL = "select min(diagnosedate),min(c.deathdate),min(c.rgtdate) "
        	          + " from llcasecure a,llcase c where a.seriousflag='1' "
                      + " and a.caseno=c.caseno "
                      + " and c.caseno='" + mCaseNo + "' and a.caserelano='" +mCaseRelaNo
                      + "' ";
        SSRS tSeriousSSRS = tExeSQL.execSQL(tSQL);
        
        if (tSeriousSSRS.getMaxRow() > 0) {
        	String tSerouseDate = tSeriousSSRS.GetText(1, 1);
        	String tDeathDate = tSeriousSSRS.GetText(1, 2);
        	String tRgtDate = tSeriousSSRS.GetText(1, 3);
        	
        	if(tSerouseDate!=null && tSerouseDate!=""){
        		if (tDeathDate != null && tDeathDate != "") {
        			tSerouseDays = PubFun.calInterval(tSerouseDate,tDeathDate, "D");
        		} else {
            	    tSerouseDays = PubFun.calInterval(tSerouseDate,tRgtDate, "D");
              }
        	}
        }
    	return tSerouseDays;
    }
    
    /**
     * 给付责任历史赔付
     * @return String
     */
    private double getGetDutySumPay() {
        ExeSQL tExeSQL = new ExeSQL();
        String tSQL = "select coalesce(sum(b.realpay),0) from llcase a,llclaimdetail b "
        	        + " where a.caseno=b.caseno and a.rgtstate in ('09','11','12') and polno='"
                      + mPolNo +"' and dutycode='"+mDutyCode
                      + "' and getdutycode = '"+mGetDutyCode
                      + "' and getdutykind = '"+mGetDutyKind+"' ";
        String tSumPay = tExeSQL.getOneValue(tSQL);
        double tPay = Double.parseDouble(tSumPay);
        return tPay;
    }
    
    /**
     * 是否享有社会医疗保险或公费医疗 0-是 1-否
     * @return
     */
    private String getSecurityFlag() {
		String tSecurityFlag = "1";
		LLOtherFactorDB tLLOtherFactorDB = new LLOtherFactorDB();
		tLLOtherFactorDB.setCaseNo(mCaseNo);
		tLLOtherFactorDB.setCaseRelaNo(mCaseRelaNo);
		tLLOtherFactorDB.setFactorType("9");
		tLLOtherFactorDB.setFactorCode("9921");
		if (tLLOtherFactorDB.getInfo()) {
			if ("0".equals(tLLOtherFactorDB.getValue())) {
				tSecurityFlag = "0";
			}
		}
		return tSecurityFlag;
	}
	
    /**
     * 是否从其他途径获得给付或补偿(0-是，1-否)
     * @return
     */
    private String getOtherWayPay() {
		String tSecurityFlag = "1";
		LLOtherFactorDB tLLOtherFactorDB = new LLOtherFactorDB();
		tLLOtherFactorDB.setCaseNo(mCaseNo);
		tLLOtherFactorDB.setCaseRelaNo(mCaseRelaNo);
		tLLOtherFactorDB.setFactorType("9");
		tLLOtherFactorDB.setFactorCode("9922");
		if (tLLOtherFactorDB.getInfo()) {
			if ("0".equals(tLLOtherFactorDB.getValue())) {
				tSecurityFlag = "0";
			}
		}
		return tSecurityFlag;
	}
    
	 /**
     * 是否未按期缴纳保费 0-是 1-否
     * @return
     */
    private String getPayDateFlag() {
		String tPayDateFlag = "1";
		ExeSQL tExeSQL = new ExeSQL();
        String tSQL = "select accdate from LLSubReport where subrptno in(select subrptno from llcaserela where caseno='" + mCaseNo + "') order by accdate fetch first 1 row only";
        String tAccdate = tExeSQL.getOneValue(tSQL);
        
        ExeSQL aExeSQL = new ExeSQL();
        String aSQL = "select Paytodate from lcpol where polno = '"+mPolNo+"' union select Paytodate from lbpol where polno = '"+mPolNo+"'";
        String tPaytodate = aExeSQL.getOneValue(aSQL);
        
        ExeSQL bExeSQL = new ExeSQL();
        String bSQL = "select Payenddate from lcpol where polno = '"+mPolNo+"' union select Payenddate from lbpol where polno = '"+mPolNo+"'";
        String tPayenddate = bExeSQL.getOneValue(bSQL);
        
        int res = tAccdate.compareTo(tPaytodate); 
        
        if(!tPaytodate.equals(tPayenddate) && res > 0)
        {
        	tPayDateFlag = "0";
        }
        return tPayDateFlag;
	}
	
	 /**
     * Q90、Q90.9、Q90.902、Q90.901四种责任是否可以给付 0-否 1-是
     * 
     * Modify By Houyd 2014-3-10
     * 在ldcode1配置表中添加othersign标记，存放险种代码（RiskCode），通过疾病代码（code）和险种代码（othersign）、险种的给付责任（code1）
     * 判断是否需要赔付此特种疾病
     * @return
     */
    private String getDutyFlag() {
		String tDutyFlag = "0";
		ExeSQL tExeSQL = new ExeSQL();
		String DutyFlagSQL="select code from ldcode1 where codetype ='lpdutyflag' " +
		//code1：险种的给付责任（GetDutyCode） othersign:险种号（RiskCode）
				"and code1 = '"+mGetDutyCode+"' and othersign='"+mLCPolSchema.getRiskCode()+"' and code in " 
				+"(select DiseaseCode from LLCaseCure where caseno='"+mCaseNo+"' and caserelano='"+this.mCaseRelaNo+"')";
		SSRS DutyFlagSSRS=tExeSQL.execSQL(DutyFlagSQL);
		if (DutyFlagSSRS!=null && DutyFlagSSRS.getMaxRow()>0) {
			tDutyFlag="1";
		}
		return tDutyFlag;
	}
    
    /**
     * # 1500 配置合同约定特定疾病（重大疾病内）
     * @return
     */
    private String getSpecialDisease() {
		String tSpecialDisease = "0";
		ExeSQL tExeSQL = new ExeSQL();
		String SpecialDiseaseSQL="select code from ldcode1 where codetype ='SpecialDisease' " +
		// othersign:险种号（RiskCode）
				"and  othersign='"+mLCPolSchema.getRiskCode()+"' and code in " 
				+"(select DiseaseCode from LLCaseCure where caseno='"+mCaseNo+"' and caserelano='"+this.mCaseRelaNo+"')";
		SSRS SpecialDiseaseSSRS=tExeSQL.execSQL(SpecialDiseaseSQL);
		if (SpecialDiseaseSSRS!=null && SpecialDiseaseSSRS.getMaxRow()>0) {
			tSpecialDisease="1";
		}
		return tSpecialDisease;
	}
    
    /**
     * #1500（重大疾病类别） 
     * @return
     */
    private String getSeriousDiseaseCate() {
		String tSpecialDisease = "0";
		ExeSQL tExeSQL = new ExeSQL();
		String SpecialDiseaseSQL=" select dieasetype from  llmserialsdiease where  code in " 
				+"(select DiseaseCode from LLCaseCure where caseno='"+mCaseNo+"' and SeriousFlag = '1' and caserelano='"+this.mCaseRelaNo+"')";
		SSRS SpecialDiseaseSSRS=tExeSQL.execSQL(SpecialDiseaseSQL);
		if (SpecialDiseaseSSRS!=null && SpecialDiseaseSSRS.getMaxRow()>0) {
			tSpecialDisease=SpecialDiseaseSSRS.GetText(1, 1);
		}
		return tSpecialDisease;
	}
    
    /**
     * 特定疾病医疗费用保险金//#1500
     * @return
     */
    private double getSpecialDiseaseSumFee() {
    	
    	double SpecialDiseaseSumFee = 0.0;
        String sql =
        	    "select sum(Fee-preamnt-refuseamnt) from LLCaseReceipt where MainFeeNo in "
                + " (select MainFeeNo from LLFeeMain where  caseno='" +
                mCaseNo + "' and caserelano='" +
                this.mCaseRelaNo + "'"
                +
                " )";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0) {
        	SpecialDiseaseSumFee = Double.parseDouble(res);
        }
        return Arith.round(SpecialDiseaseSumFee,2);
	}
    
	 /**
     * 险种历史赔付
     * @return
     */
    
    private double getRiskMoney () {
        ExeSQL tExeSQL = new ExeSQL();
        String tSQL = "select coalesce(sum(b.realpay),0) from llcase a,llclaimdetail b "
        	        + " where a.caseno=b.caseno and a.rgtstate in ('09','11','12') and polno='"
                      + mPolNo +"'";
        String tSumPay = tExeSQL.getOneValue(tSQL);
        double tRiskMoney = Double.parseDouble(tSumPay);
        return tRiskMoney;
    }
    
    /**
     * 现金价值 好日子专用！！
     * @param bTransferData 
     * @return
     */
    private double getCashValue () {
    	double tCashValue = 0.0;
    	//险种
    	String tRiskCode = mLCPolSchema.getRiskCode();
    	//被保人性别
        String tSex = mLCPolSchema.getInsuredSex();
        //被保人年龄
        int tAge = mLCPolSchema.getInsuredAppAge();
        //保单年度
        int tCurryear = PubFun.calInterval(mLCPolSchema.getCValiDate(),mAccDate, "Y") + 1;
        //险种保额
        double tAmnt = mLCPolSchema.getAmnt();
        //险种保费
        double aPrem = 0;
        if (tRiskCode.equals("231502")){
        	aPrem = mLCPolSchema.getPrem();
        }
        else if (tRiskCode.equals("333102")){
        	ExeSQL tExeSQL = new ExeSQL();
        	String tSQL = "select Prem from lcpol where riskcode = '231502' and contno = '"+ mContNo +"' " 
        				  +" union" 
        				  +" select Prem from lbpol where riskcode = '231502' and contno = '"+ mContNo +"'";
        	String tHao= tExeSQL.getOneValue(tSQL);
        	aPrem = Double.parseDouble(tHao);
        }
        
        double bPrem = 0;
        if (tRiskCode.equals("333102")){
        	bPrem = mLCPolSchema.getPrem();
        }
        else if (tRiskCode.equals("231502")){
        	ExeSQL tExeSQL = new ExeSQL();
        	String tSQL = "select Prem from lcpol where riskcode = '333102' and contno = '"+ mContNo +"' " 
        				  +" union" 
        				  +" select Prem from lbpol where riskcode = '333102' and contno = '"+ mContNo +"'";
        	String tHao= tExeSQL.getOneValue(tSQL);
        	bPrem = Double.parseDouble(tHao);
        }
        
        int tPayIntv = mLCPolSchema.getPayIntv();
        int tPayEndYear = mLCPolSchema.getPayEndYear();
        String tPayEndYearFlag = mLCPolSchema.getPayEndYearFlag();
        //开始查询：231501,333101,231502,333102险种现金价值
    	if (tRiskCode.equals("231501") || tRiskCode.equals("333101")) {
    		ExeSQL tExeSQL = new ExeSQL();    
    		String tSQL ="select sum(X.z) from " 
    			+"(select "+ tAmnt +"*cashvalue/1000 z "
	        	+"from cashvalue231501A  where  Sex = "+ tSex +" and Age = "+ tAge +" and Curryear = "+ tCurryear +" "
	        	+"and '"+ tPayIntv +"' = '0' "  
	        	+"union "
	        	+"select "+ tAmnt +"*cashvalue/1000 z "
	        	+"from cashvalue231501B  where PayEndYear = "+ tPayEndYear +" And PayEndYearFlag = '"+ tPayEndYearFlag +"' " 
	        	+"and Sex = "+ tSex +" and Age = "+ tAge +" and Curryear = "+ tCurryear +" and '"+ tPayIntv +"' <> '0' "
	        	+"union "
	        	+"select "+ tAmnt +"*cashvalue/1000 z "
	    		+"from cashvalue333101A  where  Sex = "+ tSex +" and Age = "+ tAge +" and Curryear = "+ tCurryear +" " 
	            +"and '"+ tPayIntv +"' = '0' "  
	            +"union  "
	            +"select "+ tAmnt +"*cashvalue/1000 z "
	            +"from cashvalue333101B  where PayEndYear = "+ tPayEndYear +" And PayEndYearFlag = '"+ tPayEndYearFlag +"' " 
	            +"and Sex = "+ tSex +" and Age = "+ tAge +" and Curryear = "+ tCurryear +" and '"+ tPayIntv +"' <> '0') as X ";
    		String tHao= tExeSQL.getOneValue(tSQL);
    		tCashValue = Double.parseDouble(tHao);
    	}
    	else if (tRiskCode.equals("231502") || tRiskCode.equals("333102")){
    		ExeSQL mExeSQL = new ExeSQL(); 
        	String mSQL ="select sum(X.z) from " 
        		+"(select "+ aPrem +"*a.cashvalue/b.prem z "
        		+"from cashvalue231502A a,premyz231502A b where  a.Sex = "+ tSex +" and a.Age = "+ tAge +" and Curryear = "+ tCurryear +" " 
        		+"and b.Sex="+ tSex +" and b.Age="+ tAge +" and '"+ tPayIntv +"'='0' " 
        		+"union "
        		+"select "+ aPrem +"*a.cashvalue/b.prem z "
        		+"from cashvalue231502B a,premyz231502B b where  a.PayEndYear = "+ tPayEndYear +" and a.Sex = "+ tSex +" and Curryear = "+ tCurryear +" "
        		+"and a.Age = "+ tAge +" And b.PayEndYear="+ tPayEndYear +" and b.Sex="+ tSex +" and b.Age="+ tAge +" "
        		+"and '"+ tPayIntv +"'<>'0' " 
        		+"union "
        		+"select "+ bPrem +"*a.cashvalue/b.prem z "
    			+"from cashvalue333102A a,premyz333102A b where  a.Sex = "+ tSex +" and a.Age = "+ tAge +" and Curryear = "+ tCurryear +" "  
    			+"and '"+ tPayIntv +"'='0' and b.Sex="+ tSex +" and b.Age="+ tAge +" " 
    			+"union " 
    			+"select "+ bPrem +"*a.cashvalue/b.prem z "
    			+"from cashvalue333102B a,premyz333102B b where  a.PayEndYear = "+ tPayEndYear +" and Curryear = "+ tCurryear +" " 
    			+"and a.Sex = "+ tSex +" and a.Age = "+ tAge +" And b.PayEndYear="+ tPayEndYear +" and b.Sex="+ tSex +" and b.Age="+ tAge +" " 
    			+"and '"+ tPayIntv +"'<>'0') as X "; 
        	String mHao= mExeSQL.getOneValue(mSQL);
        	tCashValue = Double.parseDouble(mHao);
    	}
    	return tCashValue;
    }
    
    /**
     * 获取伤残或残疾信息中的”鉴定日期“与”事件日期“之间间隔的天数
     * #1233 《百万安行个人护理保险》等三款产品定义
     * @return
     */
    private int getDeforOrInvaDays(){
    	String sql = "select judgedate from llcaseinfo where caseno='"+mCaseNo+"' and caserelano='"+mCaseRelaNo+"' with ur";
    	ExeSQL exeSQL = new ExeSQL();
        String result = exeSQL.getOneValue(sql);
        if(result!=null && !"".equals(result)){
    	    return PubFun.calInterval(mAccDate,result, "D");
        }else{
        	return 0;
        }
    }
    
    /**
     * 计算保单下险种的现金价值CashValueNew
     * 保单号：从本类中获取
     * 险种编码：需要单独配置
     * #1498 《福利双全个人护理保险》及其附加险 理赔功能开发-需与2216同步
     * #1233 《百万安行个人护理保险》等三款产品定义
     * @return
     */
    private double getCashValueNew(){
    	String tDeathDate = getDeathDate();
    	String tRiskCode = mLCPolSchema.getRiskCode();
    	String tContNo = mLCPolSchema.getContNo();
//    	tDeathDate = "2014-11-19";
    	if(tDeathDate == null || "".equals(tDeathDate)){
    		System.out.println("无死亡日期，跳过现金价值计算");
    		return 0;
    	}
    	ExeSQL exeSQL = new ExeSQL();
    	String tLongPolDateSQL = "select max(payToDate) from lcpol where contno = '"+tContNo+"' with ur";
    	String payToDateLongPol = exeSQL.getOneValue(tLongPolDateSQL);
    	if(payToDateLongPol == null || "".equals(payToDateLongPol)){
    		System.out.println("无长期险预计缴至日期，跳过现金价值计算");
    		return 0;
    	}
    	EdorCalZTTestBL tEdorCalZTTestBL = new EdorCalZTTestBL();
    	tEdorCalZTTestBL.setEdorValiDate(tDeathDate);
    	tEdorCalZTTestBL.setCurPayToDateLongPol(payToDateLongPol);
    	tEdorCalZTTestBL.setNeedBugetResultFlag(false);//不需要存储结果
    	tEdorCalZTTestBL.setOperator(this.mGlobalInput.Operator);
    	//配置语句，配置险种现金价值的计算险种
    	String tRiskSql = "select codename from ldcode where codetype='cashvalue' and code ='"+tRiskCode+"' with ur";
    	
        String tResult = exeSQL.getOneValue(tRiskSql);
        if(tResult!=null && !"".equals(tResult)){
        	double getSumMoney = 0;  //退保总退费
        	String[] aRiskCode = tResult.split(",");
        	for(int i=0;i<aRiskCode.length;i++){
        		System.out.println("需要处理的险种："+aRiskCode[i]);
        		LCPolDB tLCPolDB = new LCPolDB();
        		LCPolSet tLCPolSet = new LCPolSet();
        		LCPolSchema tLCPolSchema = new LCPolSchema();
        		String tQuerySql = "select * from lcpol where contno='"+tContNo+"' and riskcode ='"+aRiskCode[i]+"' with ur";
        		tLCPolSet = tLCPolDB.executeQuery(tQuerySql);
        		if(tLCPolSet.size()>0){
        			tLCPolSchema = tLCPolSet.get(1);
        			double getMoney = 0;  //退保退费
        			LJSGetEndorseSchema tLJSGetEndorseSchema 
        	        	= tEdorCalZTTestBL.budgetOnePol(tLCPolSchema.getPolNo());
        			System.out.println("调用保全退保试算-错误显示栏");
        	        System.out.println("保全退保试算错误："+tEdorCalZTTestBL.mErrors.needDealError());
        	        System.out.println("调用保全退保试算-错误显示栏");
        	        if(tEdorCalZTTestBL.mErrors.needDealError())
        	        {
        	        	break;
        	        }
        	        
        	        if(tLJSGetEndorseSchema != null)
        	        {
        	        	getMoney += tLJSGetEndorseSchema.getGetMoney();
        	        	getMoney = PubFun.setPrecision(getMoney, "0.00");
        	        	if(getMoney != 0)
        	        	{
        	        		getMoney = -getMoney;
        	        		getSumMoney += getMoney;
        	        	}        	        
        	        }
        	      
        		}
        		
        	}
    	    return PubFun.setPrecision(getSumMoney, "0.00");
        }else{
        	return 0;
        }
    }
    
    /**
     * 根据险种编码或责任编码计算特殊保额，如果没有特殊保额，取原先的。
     * @param aRiskCode
     * @param aAmnt
     * @return
     */
    private double getSpecialAmnt(String aRiskCode,double aAmnt) {
		double tAmnt = 0.0;
		LMCalModeDB tLMCalModeDB = new LMCalModeDB();
		tLMCalModeDB.setRiskCode(aRiskCode);
		tLMCalModeDB.setType("B");
		LMCalModeSet tLMCalModeSet = tLMCalModeDB.query();
		if (tLMCalModeSet.size()!=1) {
			System.out.println("未配置或配置有误");
			return aAmnt;
		}
		
		String t_CalCode = tLMCalModeSet.get(1).getCalCode();

		Calculator tCalculator = new Calculator();
		tCalculator.setCalCode(t_CalCode);



		tCalculator.addBasicFactor("Amnt", String.valueOf(aAmnt));
		String tStr = "";
		System.out.println("CalSQL=" + tCalculator.getCalSQL());
		tStr = tCalculator.calculate();
		if (tStr.trim().equals("")) {
			tAmnt = aAmnt;
		} else {
			tAmnt = Double.parseDouble(tStr);
			System.out.println("特殊的保额计算结果："+tAmnt);
		}
		return tAmnt;
	}
    
    /**xuyunpeng add #2995
     * 121101险种专用理赔计算
     * @return double
     */
    private double getAccMoneyFee() {
        double AccDoorFee = 0;

        String AheadDays="180";
            FDate tD = new FDate();
            Date AfterDate = PubFun.calDate(tD.getDate(mAccDate),
                                            Integer.parseInt(AheadDays), "D", null);
            FDate fdate = new FDate();
            String afterdate = fdate.getString(AfterDate);

        String sql = "select sum(fee-preamnt-selfamnt-refuseamnt) "
                + " from LLCaseReceipt where MainFeeNo in (select MainFeeNo "
                + "from LLFeeMain where  caseno='" + mCaseNo
                //治疗时间在时间发生时间180天内
                + "' and HospStartDate<='" + afterdate
                //账单属性必为医院
                + "' and caserelano='" + mCaseRelaNo+"' and FeeAtti='0')";

        ExeSQL exesql = new ExeSQL();
        String res = exesql.getOneValue(sql);
        if (res != null && res.length() > 0) {
            return Double.parseDouble(res);
        }
        return AccDoorFee;
    }
    /**
     * #2781
     * xuyunpeng add 
     * 税优先计算赔付金额
     * @return
     */    
    private double getSYMoneyMZ(){
    	
    	LLSecurityReceiptSchema SYLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
    	
    	SYLLSecurityReceiptSchema = getSecuReceiptSY();
    	
    	double SYMoneyMZ=0.0;
    	    	
    	//自付一+(自付二+自费-不合理费用)*0.8
    	double moneyOne = SYLLSecurityReceiptSchema.getSelfPay1()
    						+(SYLLSecurityReceiptSchema.getSelfPay2()
    						+SYLLSecurityReceiptSchema.getSelfAmnt()
    						-SYLLSecurityReceiptSchema.getStandbyAmnt())*0.8;
    	//（合计金额-统筹基金支付-大额医疗支付-公务员医疗补助-退休补充医疗-不合理费用）*90%
    	double moneyTwo = (SYLLSecurityReceiptSchema.getApplyAmnt()-
    						SYLLSecurityReceiptSchema.getPlanFee()-
    						SYLLSecurityReceiptSchema.getSupInHosFee()-
    						SYLLSecurityReceiptSchema.getOfficialSubsidy()-
    						SYLLSecurityReceiptSchema.getRetireAddFee()-
    						SYLLSecurityReceiptSchema.getStandbyAmnt())*0.9;
    	if(moneyOne>0||moneyTwo>0){
    		
    		if(moneyOne>moneyTwo){
    			return moneyOne;
    		}else{
    			syPoint = true;
    			return moneyTwo;
    		}
    	}
    	
    	return SYMoneyMZ;
    }
    
    
    /**xuyunpeng add #2781
     * 取社保帐单中值（税优产品专用）
     * @return boolean
     */
    private LLSecurityReceiptSchema getSecuReceiptSY() {
    	
    	LLSecurityReceiptSchema SYLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
        String sql = "select * from LLSecurityReceipt where MainFeeNo in " +
                     " (select MainFeeNo from LLFeeMain where  caseno='" +
                     mCaseNo + "' and caserelano='" +
                     this.mCaseRelaNo + "' and FeeAtti='4' and (ReceiptType <>'10' or ReceiptType is null) )";
        System.out.println(sql);
        LLSecurityReceiptDB tLLSecurityReceiptDB = new LLSecurityReceiptDB();
        LLSecurityReceiptSet tLLSecurityReceiptSet = new LLSecurityReceiptSet();
        tLLSecurityReceiptSet = tLLSecurityReceiptDB.executeQuery(sql);
        if (tLLSecurityReceiptSet != null && tLLSecurityReceiptSet.size() > 0) {
            int srcount = tLLSecurityReceiptSet.size();
            SYLLSecurityReceiptSchema = tLLSecurityReceiptSet.get(1);
            double SelfPay1 = 0.0;
            double lowamnt = 0.0;
            double midamnt = 0.0;
            double highamnt1 = 0.0;
            double highamnt2 = 0.0;
            double supamnt = 0.0;
            double sDoorpay = 0.0;
            double cDoorpay = 0.0;
            double bDoorpay = 0.0;
            double appamnt = 0.0;
            double overamnt = 0.0;
            double selfpay2 = 0.0;
            double selfamnt = 0.0;
            double standbyamnt = 0.0;//#1500 不合理费用 暂存储在该字段，之前（昆明统筹基金封顶线下保险金赔付 使用了该字段，目前已没有相关业务）
       
            double planfee = 0.0;//社保账单增加“本次统筹基金医疗费用”
            double supinhosfee = 0.0;//社保账单增加“本次住院大额医疗费用”
            double officialsubsidy = 0.0;//社保账单增加“本次公务员医疗补助费用”
            double applyamnt = 0.0;//社保账单增加“本次合计金额费用”
            
            double retireAddFee = 0.0;//退休补充医疗
            double supInHosFee = 0.0;//大额医疗支付
            
            for (int i = 1; i <= srcount; i++) {
                lowamnt += tLLSecurityReceiptSet.get(i).getLowAmnt();
                midamnt += tLLSecurityReceiptSet.get(i).getMidAmnt();
                highamnt1 += tLLSecurityReceiptSet.get(i).getHighAmnt1();
                highamnt2 += tLLSecurityReceiptSet.get(i).getHighAmnt2();
                supamnt += tLLSecurityReceiptSet.get(i).getSuperAmnt();
                sDoorpay += tLLSecurityReceiptSet.get(i).getSmallDoorPay();
                cDoorpay += tLLSecurityReceiptSet.get(i).getEmergencyPay();
                bDoorpay += tLLSecurityReceiptSet.get(i).getHighDoorAmnt();
                appamnt += tLLSecurityReceiptSet.get(i).getFeeInSecu();
                selfpay2 += tLLSecurityReceiptSet.get(i).getSelfPay2();
                selfamnt += tLLSecurityReceiptSet.get(i).getSelfAmnt();
                standbyamnt += tLLSecurityReceiptSet.get(i).getStandbyAmnt();
                planfee += tLLSecurityReceiptSet.get(i).getPlanFee();
                supinhosfee += tLLSecurityReceiptSet.get(i).getSupInHosFee();
                officialsubsidy += tLLSecurityReceiptSet.get(i).getOfficialSubsidy();
                applyamnt += tLLSecurityReceiptSet.get(i).getApplyAmnt();
             
                retireAddFee +=tLLSecurityReceiptSet.get(i).getRetireAddFee();
                supInHosFee +=tLLSecurityReceiptSet.get(i).getSupInHosFee();
                SelfPay1 +=tLLSecurityReceiptSet.get(i).getSelfPay1();
            }
            
            SYLLSecurityReceiptSchema.setLowAmnt(lowamnt);
            SYLLSecurityReceiptSchema.setMidAmnt(midamnt);
            SYLLSecurityReceiptSchema.setHighAmnt1(highamnt1);
            SYLLSecurityReceiptSchema.setHighAmnt2(highamnt2);
            SYLLSecurityReceiptSchema.setSuperAmnt(supamnt);
            SYLLSecurityReceiptSchema.setSmallDoorPay(sDoorpay);
            SYLLSecurityReceiptSchema.setEmergencyPay(cDoorpay);
            SYLLSecurityReceiptSchema.setHighDoorAmnt(bDoorpay);
            SYLLSecurityReceiptSchema.setSecuRefuseFee(overamnt);
            SYLLSecurityReceiptSchema.setSelfPay2(selfpay2);
            SYLLSecurityReceiptSchema.setSelfAmnt(selfamnt);
            SYLLSecurityReceiptSchema.setStandbyAmnt(standbyamnt);
            SYLLSecurityReceiptSchema.setPlanFee(planfee);
            SYLLSecurityReceiptSchema.setSupInHosFee(supinhosfee);
            SYLLSecurityReceiptSchema.setOfficialSubsidy(officialsubsidy);
            SYLLSecurityReceiptSchema.setApplyAmnt(applyamnt);
            
            SYLLSecurityReceiptSchema.setRetireAddFee(retireAddFee);
            SYLLSecurityReceiptSchema.setSupInHosFee(supInHosFee);
            SYLLSecurityReceiptSchema.setSelfPay1(SelfPay1);

        }
        return SYLLSecurityReceiptSchema;
    }
    
    /**
     * xuyunpeng add #2781
     * 健康评估
     * @param aTransferData
     * @return
     */
    private double getHealthAass(){
    	double healthAass=0.0;
    	  String sql = "select sum(fee-preamnt-selfamnt-refuseamnt) "
              + " from LLCaseReceipt where FeeItemCode ='164' and MainFeeNo in (select MainFeeNo "
              + "from LLFeeMain where  caseno='" + mCaseNo
              //账单属性为医院，账单种类为门诊
              + "' and caserelano='" + mCaseRelaNo+"' and FeeAtti='0' and feetype='1')";

      ExeSQL exesql = new ExeSQL();
      String res = exesql.getOneValue(sql);
      if (res != null && res.length() > 0) {
          return Double.parseDouble(res);
      } 
      return healthAass;
    }
    
    
    /**
     * xuyunpeng add #2781
     * 健康咨询
     * @param aTransferData
     * @return
     */
    
    private double getHealthConsul(){
    	double healthConsul=0.0;
    	  String sql = "select sum(fee-preamnt-selfamnt-refuseamnt) "
              + " from LLCaseReceipt where FeeItemCode ='163' and MainFeeNo in (select MainFeeNo "
              + "from LLFeeMain where  caseno='" + mCaseNo
              //账单属性为医院，账单种类为门诊
              + "' and caserelano='" + mCaseRelaNo+"' and FeeAtti='0' and feetype='1')";

      ExeSQL exesql = new ExeSQL();
      String res = exesql.getOneValue(sql);
      if (res != null && res.length() > 0) {
          return Double.parseDouble(res);
      } 
      return healthConsul;
    }
    /**
     * 健康档案
     * xuyunpeng add #2781
     * @param aTransferData
     * @return
     */
    
    private double getHealthFile(){
    	double healthFile=0.0;
    	  String sql = "select sum(fee-preamnt-selfamnt-refuseamnt) "
              + " from LLCaseReceipt where FeeItemCode ='162' and MainFeeNo in (select MainFeeNo "
              + "from LLFeeMain where  caseno='" + mCaseNo
              //账单属性为医院，账单种类为门诊
              + "' and caserelano='" + mCaseRelaNo+"' and FeeAtti='0' and feetype='1')";

      ExeSQL exesql = new ExeSQL();
      String res = exesql.getOneValue(sql);
      if (res != null && res.length() > 0) {
          return Double.parseDouble(res);
      } 
      return healthFile;
    }
    
    /**
     * 服务费
     * xuyunpeng add #2781
     * @param aTransferData
     * @return
     */
    
    private double getService(){
    	double Service=0.0;
    	  String sql = "select sum(fee-preamnt-selfamnt-refuseamnt) "
              + " from LLCaseReceipt where FeeItemCode ='126' and MainFeeNo in (select MainFeeNo "
              + "from LLFeeMain where  caseno='" + mCaseNo
              //账单属性为医院，账单种类为门诊
              + "' and caserelano='" + mCaseRelaNo+"' and FeeAtti='0' and feetype='1')";

      ExeSQL exesql = new ExeSQL();
      String res = exesql.getOneValue(sql);
      if (res != null && res.length() > 0) {
          return Double.parseDouble(res);
      } 
      return Service;
    }
    
    
    /**
     * 计算免陪额
     * @param aTransferData
     * @return
     */
    private double calGetLimit(TransferData aTransferData) {
    	double tGetLimit = 0.0;
    	tGetLimit = mGetLimit;
    	LMCalModeDB tLMCalModeDB = new LMCalModeDB();
		tLMCalModeDB.setRiskCode(mGetDutyCode+mGetDutyKind);
		tLMCalModeDB.setType("M");
		LMCalModeSet tLMCalModeSet = tLMCalModeDB.query();
		if (tLMCalModeSet.size()!=1) {
			System.out.println("未配置或配置有误");
			return tGetLimit;
		}
		
		String t_CalCode = tLMCalModeSet.get(1).getCalCode();











		Calculator tCalculator = new Calculator();
		tCalculator.setCalCode(t_CalCode);
		
		Vector tVector = aTransferData.getValueNames();
		
		for (int i = 0; i < tVector.size(); i++) {
        	String tName = (String) tVector.get(i);
            String tValue = (String) aTransferData.getValueByName(tName);
            tCalculator.addBasicFactor(tName, tValue);
		}
		tCalculator.addBasicFactor("GetLimit", String.valueOf(tGetLimit));
		
		String tStr = "";
		System.out.println("CalSQL=" + tCalculator.getCalSQL());
		tStr = tCalculator.calculate();
		if (tStr.trim().equals("")) {
//			tGetLimit = 0;
		} else {
			tGetLimit = Double.parseDouble(tStr);
			System.out.println("免赔额计算结果："+tGetLimit);
			mCalGetLimitFlag = true;
		}
		mGetLimit = tGetLimit;
    	
    	return tGetLimit;
    }
  //#2781 xuyunpeng add 检查是否是税优续保保单 start
    private boolean checkSyPoint(){
    	String triskCode = mLLClaimPolicySchema.getRiskCode();
    	String sql = "select taxoptimal from lmriskapp where riskcode='"+triskCode+"'";

    	ExeSQL exesql = new ExeSQL();
    	String res = exesql.getOneValue(sql); 
    	if("Y".equals(res)){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    //#2781 xuyunpeng add 检查是否是税优续保保单
    private double checkSyXB(){
    	double tSumM=0.0;
    	//String triskCode = mLLClaimPolicySchema.getRiskCode();
    	
    	ExeSQL tExeSQL=new ExeSQL();
    	
    
	    		String tDate=" select lp.Cvalidate ,lp.enddate "
			           +" from lcpol lp "
			           +" where lp.polno='"+mLCPolSchema.getPolNo()+"' "   
			        
			           +" union "
			           +" select lp.Cvalidate ,lp.enddate "
			           +" from lbpol lp "
			           +" where lp.polno='"+mLCPolSchema.getPolNo()+"' "
			           +" with ur ";
		   SSRS tDateS=tExeSQL.execSQL(tDate);
		   System.out.println(tDateS.GetText(1,1)+"***有效期间***"+tDateS.GetText(1,2));
		   
		  
	   	   //历史赔付金额
	       String tMoney=" select nvl(sum(a.realpay),0)  "
	                    +" from llclaimdetail a,llcase b "
	                    +" where a.caseno=b.caseno "
	                    +" and b.rgtstate in ('09','10','11','12') "
	                    +" and a.polno='"+mLCPolSchema.getPolNo()+"' "
	                    +" and b.customerno ='" + mLCPolSchema.getInsuredNo()+"' "
	                    +" and a.dutycode='"+mLCDutySchema.getDutyCode()+"' "
	                    +" and a.caseno in "
	                    +" ( "
	                    +" select c.caseno " 
	                    +" from llcaserela c,llsubreport d "
	                    +" where c.caseno=a.caseno "
	                    +" and d.subrptno=c.subrptno "
	                    +" and d.accdate between '"+tDateS.GetText(1,1)+"' and '"+tDateS.GetText(1,2)+"'"
	                    +" ) with ur ";
	       SSRS tMoneyS=tExeSQL.execSQL(tMoney);
	       tSumM=Double.parseDouble(tMoneyS.GetText(1, 1)); 
   
    	return tSumM;
    }

  //#2781 xuyunpeng add 检查是否是税优续保保单 end 
   
  //#3207 团体管理式补充A款 **start**
    /**
     * 团体管理式补充医疗门诊费用（A款）
     * @return
     */
   private double getManageDoorSumFee(){
		double ManageDoorSumFee = 0;

    	String sql = "select nvl(sum(fee-refuseamnt-preamnt-selfamnt),0) "
            + " from LLCaseReceipt where MainFeeNo in (select MainFeeNo "
            + "from LLFeeMain where  caseno='" + mCaseNo
            + "' and caserelano='" + mCaseRelaNo
            + "' and FeeType='1' and FeeAtti ='0')";
    	
    	ExeSQL exesql = new ExeSQL();
    	String res = exesql.getOneValue(sql);
    	if (res != null && res.length() > 0) {
    		return Double.parseDouble(res);
    	}
	   return ManageDoorSumFee;
   }
   /**
    * 团体管理式补充医疗住院医疗费用（A款）
    * @return
    */
   private double getManageHospSumFee(){
		double ManageHospSumFee = 0;

    	String sql = "select nvl(sum(fee-refuseamnt-preamnt-selfamnt),0) "
            + " from LLCaseReceipt where MainFeeNo in (select MainFeeNo "
            + "from LLFeeMain where  caseno='" + mCaseNo
            + "' and caserelano='" + mCaseRelaNo
            + "' and FeeType='2' and FeeAtti ='0')";
    	
    	ExeSQL exesql = new ExeSQL();
    	String res = exesql.getOneValue(sql);
    	if (res != null && res.length() > 0) {
    		return Double.parseDouble(res);
    	}
	   return ManageHospSumFee;
	   
   }
   
   /**
    * 团体式管理式补充医疗A款--M值
    * @return
    */
   private double getBasicMAmntM(){
	double BasicMAmntM = 0;
   	String tDeathDate = mLLCaseSchema.getDeathDate();
   	if(tDeathDate==null || "".equals(tDeathDate)||!"162401".equals(mLCPolSchema.getRiskCode())){
   		return 0.0;
   	}
   	//该被保险人身故前90日内累计新增的保险金额
   	BasicMAmntM=LLCaseCommon.getBasicAppendAmnt(mCaseNo, mPolNo, PubFun.calDate(tDeathDate, -90, "D", null), tDeathDate, "LS");
   	//该被保险人身故前90日内累计理赔的健康医疗保险金
   	String sql = "select nvl(sum(a.pay),0) from ljagetclaim a ,llcase b where a.otherno=b.caseno " +
   			"and b.rgtstate in ('11','12')  and   a.othernotype= '5' " +
   			"and a.getdutykind in('900','200','100') and a.polno ='"+mPolNo+"' and a.riskcode='162401' " +
   			"and a.makedate between '"+PubFun.calDate(tDeathDate, -90, "D", null)+"' and '"+tDeathDate+"' " ;
   	ExeSQL exesql = new ExeSQL();
   	String res = exesql.getOneValue(sql);
   	double realpay90 =Double.parseDouble(res);
   	BasicMAmntM-=realpay90;
   	return BasicMAmntM;
   }
   
   /**
    * 团体管理式补充医疗身故当日理赔健康医疗保险金（A款）
    * @return
    */
   private double getCurrentHealthPay(){
	   double tCurrentHealthPay=0;
	   String tDeathDate=mLLCaseSchema.getDeathDate();
	   if(tDeathDate==null ||"".equals(tDeathDate)||!"162401".equals(mLCPolSchema.getRiskCode())){
		   return 0.0;
	   }
	   String sql="select nvl(sum(a.pay),0) from ljagetclaim a,llcase b where a.otherno=b.caseno " +
			   "and b.rgtstate in ('11','12') and a.othernotype='5' and a.riskcode='162401' "+
			   "and a.getdutykind in ('900','200','100') and a.polno='"+mPolNo+"' "+
			   "and a.makedate ='"+tDeathDate+"' with ur";
	   ExeSQL exesql = new ExeSQL();
	   String res =exesql.getOneValue(sql);
	   tCurrentHealthPay=Double.parseDouble(res);
	   return tCurrentHealthPay;
			   
   }
   
   /**
    * 团体管理式补充医疗当日身故0时保险金（A款）
    * @return
    */
   private double getManageAmnt(){
	   double ManageAmnt=0;
	   String tDeathDate=mLLCaseSchema.getDeathDate();
	   if(tDeathDate==null ||"".equals(tDeathDate)||!"162401".equals(mLCPolSchema.getRiskCode())){
		   return 0.0;
	   }
	   ManageAmnt=LLCaseCommon.getAmntFromAcc(mCaseNo, mPolNo, tDeathDate, "LS");
   	return ManageAmnt;
   }
   
  // #3207 团体管理式补充A款 **end**

  // #3337 团体管理式补充B款**start**
   /***
    * 团体管理式补充B款 M取值
    * @return
    */
   private double getBasicMBAmntM(){
	   double BasicMBAmntM=0;
	   String tDeathDate =mLLCaseSchema.getDeathDate();
	   if(tDeathDate ==null || "".equals(tDeathDate)||!"162501".equals(mLCPolSchema.getRiskCode())){
		  return 0.0; 
	   }
	   // 被保险人身故前90日累计新增的个人保险金额
	   BasicMBAmntM=LLCaseCommon.getBasicAppendAmnt(mCaseNo, mPolNo, PubFun.calDate(tDeathDate, -90, "D", null), tDeathDate, "LS");
	   // 该被保险人身故前90日内累计理赔的健康医疗保险金
	   String sql ="select nvl(sum(a.pay),0) from ljagetclaim a,llcase b where a.otherno=b.caseno "
	   		+ "and a.polno='"+mPolNo+"' and b.rgtstate in ('11','12') and a.riskcode='162501' and "
	   		+ "a.contno='"+mContNo+"' and a.getdutykind in ('900','200','100') and a.othernotype='5' "
	   		+ "and a.makedate between '"+PubFun.calDate(tDeathDate, -90, "D", null)+"' and '"+tDeathDate+"' with ur";
	   ExeSQL tExeSQL = new ExeSQL();
	   String tRelAmnt= tExeSQL.getOneValue(sql);
	   double tRel=Double.parseDouble(tRelAmnt);
	  
	   BasicMBAmntM-=tRel;
	   return BasicMBAmntM;
   }
   
   /***
    * 团体管理式补充医疗当日身故0时保险金(B款) 
    * @return
    */
    private double getManageBAmnt(){
    	double ManageBAmnt=0;
    	String tDeathDate =mLLCaseSchema.getDeathDate();
 	    if(tDeathDate ==null || "".equals(tDeathDate)||!"162501".equals(mLCPolSchema.getRiskCode())){
 		  return 0.0; 
 	    }
 	    ManageBAmnt=LLCaseCommon.getAmntFromAcc(mCaseNo, mPolNo, tDeathDate, "LS");
        return ManageBAmnt;
    }
   
    /***
     * 团体管理式补充医疗身故当日理赔的健康医疗保险金(B款)
     * @return
     */
    private double getCurrentHealthPayB(){
    	double CurrentHealthPayB=0;
    	String tDeathDate =mLLCaseSchema.getDeathDate();
 	    if(tDeathDate ==null || "".equals(tDeathDate)||!"162501".equals(mLCPolSchema.getRiskCode())){
 		  return 0.0; 
 	    }
    	String sql="select nvl((sum(a.pay)),0) from ljagetclaim a,llcase b where a.otherno=b.caseno "
    			+ "and b.rgtstate in ('11','12') and a.polno='"+mPolNo+"' and a.riskcode='162501' and "
    			+ "a.contno='"+mContNo +"' and a.othernotype='5' and a.getdutykind in ('900','100','200') "
    			+ "and a.makedate='"+tDeathDate+"' with ur";
    	ExeSQL tExeSQL = new ExeSQL();
    	String tRelAmntB= tExeSQL.getOneValue(sql);
    	CurrentHealthPayB=Double.parseDouble(tRelAmntB);
    	return CurrentHealthPayB;
    }
       
    /***
     * 事件类型(管理式补充医疗保险B款)
     * @return
     */
    private String getAccidentType(){
    	
    	String tAccType=null;
    	String tAccSQL="select accidenttype from llsubreport  a ,llcaserela b where a.subrptno =b.subrptno and b.caseno='"+mCaseNo+"' with ur";
    	ExeSQL tExe= new ExeSQL();    	
    	return tAccType=tExe.getOneValue(tAccSQL);
    }
    
    //#3337 团体管理式补充B款**end**
    
    // #2978 健康守望补充医疗保险（B款）**start**
    /**
     * 申请原因04-重大疾病
     */
    private String getLLAppClaimReason(){    	
    	String sql ="select ReasonCode from LLAppClaimReason where caseno='"+mCaseNo+"'";
    	ExeSQL exesql = new ExeSQL();
    	String mReasonCode = exesql.getOneValue(sql);
    	return mReasonCode;
    }
    
    /**
     * 事件信息1-疾病
     */
    private String getLlsubreport(){
    	String sql ="select 1 from llsubreport where subrptno in(select subrptno from llcaserela where caseno in('"+mCaseNo+"')) and accidenttype='1'";
    	ExeSQL exesql = new ExeSQL();
    	String mAccidentType = exesql.getOneValue(sql);
    	return mAccidentType;
    }
    
    /**
    /**
     * 连续投保标识 0-不连续  1-连续
     * 
     */
    private String getContinuousInsurance(){
    	String mContinuousIns = "";
    	String mInsuredno = "";
    	String sql ="select customerno from llcase where caseno='"+mCaseNo+"'";
    	ExeSQL exesql = new ExeSQL();
    	mInsuredno = exesql.getOneValue(sql);   	
    	String sql1 = "select 1 from lccont where insuredno='"+mInsuredno+"' and cinvalidate = date('"+mLCContSchema.getCValiDate()+"') - 1 days "
    				+ "union select 1 from lbcont where insuredno='"+mInsuredno+"' and cinvalidate = date('"+mLCContSchema.getCValiDate()+"') - 1 days";    	
    	mInsuredno = exesql.getOneValue(sql1);
    	if(mInsuredno.equals("1")){
    		mContinuousIns = "1";
    	}else{
    		mContinuousIns = "0";    		
    	}
    	return mContinuousIns;
    }
    
    /**
     * 等待期（保单生效日期至重大疾病确诊日期的经过时间）
     * @return
     */
    private int getWaitDate(){
    	int mWaitDate = 0;
    	String mCValiDate = mLCContSchema.getCValiDate();
    	String sql = "select min(DiagnoseDate) from LLCaseCure where DiseaseCode='201401' and caseno='"+mCaseNo+"' and SeriousFlag = '1' and caserelano='"+this.mCaseRelaNo+"'";
    	ExeSQL exesql = new ExeSQL();
    	String mDiagnoseDate = exesql.getOneValue(sql);
    	if(mDiagnoseDate!=null && mDiagnoseDate!=""){
    		mWaitDate = PubFun.calInterval(mCValiDate,mDiagnoseDate, "D");
    	}   	
    	return mWaitDate;
    }
    
    /**
     * #2978（重大疾病类别） 0-不是2978重疾 1-是2978重疾
     * @return
     */
    private String getSeriousDisease() {
		String tSpecialDisease = "0";
		String sql ="select min(1) from LLCaseCure where DiseaseCode='201401' and caseno='"+mCaseNo+"' and SeriousFlag = '1' and caserelano='"+this.mCaseRelaNo+"'";
		ExeSQL exesql = new ExeSQL();
		String SpecialDiseaseSQL = exesql.getOneValue(sql);
		if (SpecialDiseaseSQL!=null && !"".equals(SpecialDiseaseSQL)) {
			tSpecialDisease = "1"; 
		}
		System.out.println(tSpecialDisease);
		return tSpecialDisease;
	}
    // #2978 健康守望补充医疗保险（B款）**end**
    /**
     * #3653 出险日期至账单结算日期的经过天数
     * @return 
     */
    
    private int getFeeElapsedDate() {
    	int FeeElapsedDate = 0;
    	String FeeDate = "select min(feedate) from llfeemain where caseno='"+mCaseNo+"' with ur";
    	ExeSQL exesql = new ExeSQL();
		String tFeeDate = exesql.getOneValue(FeeDate);
		if(tFeeDate!=null&&!"".equals(tFeeDate)){
			FeeElapsedDate=PubFun.calInterval(mAccDate,tFeeDate, "D");
			if(FeeElapsedDate<0){
				FeeElapsedDate=0;
			}
		}
    	return FeeElapsedDate;
    }
    /**
     * #3653  住院日津贴
     * @return
     */
    
    private int getHospAllowance() {
    	int HospAllowance = 0;
    	String HospAllowanceSQL = "select cast(actuget/90 as decimal(20, 2)) from lcget where polno='"+mPolNo+"' and getdutycode='"+mGetDutyCode+"' union select cast(actuget/90 as decimal(20, 2)) from lcget where polno='"+mPolNo+"' and getdutycode='"+mGetDutyCode+"' with ur";
    	HospAllowanceSQL = "select calfactorvalue from lccontplandutyparam where grppolno='"+mLCPolSchema.getGrpPolNo()+"' and dutycode='"+mDutyCode+"' and calfactor='StandbyFlag1' union select calfactorvalue from lccontplandutyparam where grppolno='"+mLCPolSchema.getGrpPolNo()+"' and dutycode='"+mDutyCode+"' and calfactor='StandbyFlag1' with ur";
    	ExeSQL exesql = new ExeSQL();
		String tHospAllowance = exesql.getOneValue(HospAllowanceSQL);
		if(tHospAllowance!=null&&!"".equals(tHospAllowance)){
			HospAllowance=Integer.parseInt(tHospAllowance);
		}
    	return HospAllowance;
    }
    /**
     * #3653  伤残
     * 
     * @return
     */
    private String getDisability() {
    	String Disability = "0";
    	String DisabilitySQL = "select 1 from llcaseinfo where caseno='"+mCaseNo+"' with ur";
    	ExeSQL exesql = new ExeSQL();
		String tDisability = exesql.getOneValue(DisabilitySQL);
		if(tDisability!=null&&!"".equals(tDisability)){
			Disability = tDisability;
		}
    	return Disability;
    }
    /**
     * 发生日期距死亡日期的经过时间
     * @return
     */
    private int getAcctoDeath() {
    	int AcctoDeath = 0;
    	String DeathDate=mLLCaseSchema.getDeathDate();
    	
    	if(DeathDate!=null&&!"".equals(DeathDate)){
    		AcctoDeath=PubFun.calInterval(mAccDate,DeathDate, "D");
			if(AcctoDeath<0){
				AcctoDeath=0;
			}
		}
    	return AcctoDeath;
    }
    /**
     * 团险保障计划下的免赔额
     * @return
     */
    private double getGrpGetLimit() {
    	double GrpGetLimit = 0.00;
    	String GrpGetLimitSQL = "select cast(actuget/90 as decimal(20, 2)) from lcget where polno='"+mPolNo+"' and getdutycode='"+mGetDutyCode+"' union select cast(actuget/90 as decimal(20, 2)) from lcget where polno='"+mPolNo+"' and getdutycode='"+mGetDutyCode+"' with ur";
    	GrpGetLimitSQL = "select calfactorvalue from lccontplandutyparam where grppolno='"+mLCPolSchema.getGrpPolNo()+"' and dutycode='"+mDutyCode+"' and calfactor='GetLimit' union select calfactorvalue from lbcontplandutyparam where grppolno='"+mLCPolSchema.getGrpPolNo()+"' and dutycode='"+mDutyCode+"' and calfactor='GetLimit' with ur";
    	ExeSQL exesql = new ExeSQL();
		String tGrpGetLimit = exesql.getOneValue(GrpGetLimitSQL);
		if(tGrpGetLimit!=null&&!"".equals(tGrpGetLimit)){
			GrpGetLimit= Double.parseDouble(tGrpGetLimit);
		}
    	return GrpGetLimit;
    }
    
    public VData getResult()
    {
        return mResult;
    }

    
    public static void main(String[] args) {//TODO：测试
//    	ClaimCalBL c = new ClaimCalBL();
    	int a = PubFun.calInterval("2010-1-1","2011-2-1", "Y");
    	System.out.println(a);
//    	c.mLCPolSchema.setContNo("011752648000001");
//    	c.mLCPolSchema.setRiskCode("331801");
//    	c.mLCPolSchema.setPolNo("21034168902");
//    	c.mLCPolSchema.setInsuredNo("011752647");
//    	c.mGetDutyCode = "241202";
//    	c.mGetDutyKind = "501";
//    	c.mLLCaseSchema.setDeathDate("2013-12-12");//2011-5-26
//    	c.mCaseRelaNo = "86610000517073";
//    	c.mCaseNo = "C6105140227000001";
//    	c.getAccValue();
    	
//    	c.mLCPolSchema.setContNo("004239934000001");
//    	c.mLCPolSchema.setRiskCode("330701");
//    	c.mLCPolSchema.setPolNo("21009634368");--2008-12-9
//    	c.mLCPolSchema.setInsuredNo("004239934");
//    	c.mGetDutyCode = "266202";
//    	c.mGetDutyKind = "501";
//    	c.mLLCaseSchema.setDeathDate("2014-1-31");
//    	c.mCaseRelaNo = "86410000039148";
//    	c.mCaseNo = "C4117140305000002";
//    	c.getAccValue();
    	
//    	c.mLCPolSchema.setContNo("021143266000001");
//    	c.mLCPolSchema.setRiskCode("332601");
//    	c.mLCPolSchema.setPolNo("21071362485");
//    	c.mLCPolSchema.setInsuredNo("021143266");
//    	c.mLCPolSchema.setCValiDate("2013-11-1");
//    	c.mGetDutyCode = "315202";
//    	c.mGetDutyKind = "501";
//    	c.mLLCaseSchema.setDeathDate("2014-3-5");
//    	c.mAccDate = "2014-3-5";
//    	c.mCaseRelaNo = "86510000888921";
//    	c.mCaseNo = "C5114140401000001";
//    	c.getAccValue();
//    	c.mCaseNo="C4400140314000011";
//    	c.calpay();
    	
    	
//    	int a = 70;
//    	String cValidate="2008-9-1";
//    	String brithday = "1963-8-1";
//    	int Year = 0;
//    	int month = 1;
//    	int day = 1;
//    	String temp = c.compareMD(cValidate,brithday);
//		month = Integer.parseInt(cValidate.substring(cValidate.indexOf("-")+1,cValidate.lastIndexOf("-") ));
//		day = Integer.parseInt(cValidate.substring(cValidate.lastIndexOf("-")+1 ));
//    	if(cValidate.equals(temp)){
//    		Year = c.getYear(brithday)+a;
//    	}else{
//    		Year = c.getYear(brithday)+a+1;
//    	}
//    	System.out.println( c.getConCatYear(Year,month,day));
//    	c.mCaseNo = "C1100121115000003";
//    	c.mContNo = "001010791000002";
//    	c.mPolNo = "21000614334";
//    	c.mCaseRelaNo = "86110000710487";
//    	c.mLCPolSchema.setRiskCode("290101");
//    	c.getDutyFlag();
    	
    	
    	ClaimCalBL c = new ClaimCalBL();
    	c.mLCPolSchema.setContNo("013990269000001");
    	c.mLCPolSchema.setRiskCode("333701");
    	System.out.println(c.getCashValueNew());
//    	c.mLCPolSchema.setContNo("014035678000001");
//    	
//    	System.out.println("AllPrem:"+c.getAllPrem());
//    	LCPolDB  tLCPolDB = new LCPolDB();
//    	tLCPolDB.setPolNo("21000554014");
//    	LCPolSchema tLCPolSchema = tLCPolDB.query().get(1);
//    	c.setLCPolSchema(tLCPolSchema);
//    	c.setCaseNo("C1100081124000001");
//    	c.setCaseRelaNo("86000000019594");
//    	System.out.println(c.getConCatYear(2008, 1, 1));
//    	System.out.println(c.getNextBalanDate("2008-12-29"));
//    	System.out.println(c.getDays("2008-2-1"));
//    	String s = c.compareMD("2008-1-21","1973-12-22");
//    	System.out.println(s);
//    	System.out.println(c.getYear("2008-1-1"));
//        String caseno  = "C1100070324000002";
//        ClaimCalBL aClaimCalBL = new ClaimCalBL();
//        String aContNo = "2301002220";
//        System.out.println(aClaimCalBL.toString());
//
//        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
//        LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();
//        LLClaimPolicySchema aLLClaimPolicySchema = new LLClaimPolicySchema();
//        aLLClaimPolicySchema.setContNo(aContNo);
//        aLLClaimPolicySchema.setRiskCode("170106");
//        aLLClaimPolicySchema.setGetDutyKind("000");
//        aLLClaimPolicySchema.setRgtNo("P1100070324000001");
//        aLLClaimPolicySchema.setCaseNo(caseno);
//        aLLClaimPolicySchema.setPolNo("21001533779");
//        tLLClaimPolicySet.add(aLLClaimPolicySchema);

//        aLLClaimPolicySchema = new LLClaimPolicySchema();
//        aLLClaimPolicySchema.setContNo(aContNo);
//        aLLClaimPolicySchema.setRiskCode("1605");
//        aLLClaimPolicySchema.setGetDutyKind("100");
//        aLLClaimPolicySchema.setRgtNo("P1100060412000001");
//        aLLClaimPolicySchema.setCaseNo(caseno);
//        aLLClaimPolicySchema.setPolNo("21000027519");
//        tLLClaimPolicySet.add(aLLClaimPolicySchema);

//        tLLCaseSchema.setCaseNo(caseno);
//
//        GlobalInput mGlobalInput = new GlobalInput();
//        mGlobalInput.ManageCom = "86";
//        mGlobalInput.ComCode = "86";
//        mGlobalInput.Operator = "cm0008";
//        VData aVData = new VData();
//        aVData.addElement(mGlobalInput);
//        aVData.addElement(tLLCaseSchema);
//        aVData.addElement(tLLClaimPolicySet);
//
//        aClaimCalBL.submitData(aVData, "Cal");
//          aClaimCalBL.mAccDate="2007-12-12" ;
//          aClaimCalBL.getAccInHospFee();
//          aClaimCalBL.getAccDoorPostFee();



    }

}
