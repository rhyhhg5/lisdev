package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;


/*******************************************************************************
 * Name     :AllCaseInfoBL.java
 * Function :案件－立案－分案疾病伤残明细业务逻辑处理类
 * Author   :LiuYansong
 * Date     :2003-7-16
 */
public class AllCaseInfoBL {
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private String mOperate;

    private LLCaseInfoSet mLLCaseInfoSet = new LLCaseInfoSet();

    private LLCaseCureSet mLLCaseCureSet = new LLCaseCureSet();
    private LLOperationSet mLLOperationSet = new LLOperationSet();
    private LLOtherFactorSet mLLOtherFactorSet = new LLOtherFactorSet();
    private LLAccidentSet mLLAccidentSet = new LLAccidentSet();
    private LLSubReportSet mLLSubReportSet = new LLSubReportSet();
//    private LLToClaimDutySet mLLToClaimDutySet = new LLToClaimDutySet();
    private LLDisabilitySet mLLDisabilitySet = new LLDisabilitySet();


    /** 全局数据 */
    private String strDate = "";
    private String strTime = "";
    String DisplayFlag = ""; //判断是在立案中进入还是在理算中进入
    String Set_count = "";
    String AccidentType = "";
//    private GlobalInput mGlobalInput = new GlobalInput();
    private GlobalInput mG = new GlobalInput();
    private String mCaseRelaNo = "";
    private String mCaseNo = "";
    private String IsSingle="0";
    public AllCaseInfoBL()
    {}

    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        if (!getInputData(cInputData))
            return false;

        if (!dealDataInsert())
            return false;

        if (!dealData())
            return false;
        System.out.println("-------dealData--------");

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(this.mResult, null)) {
            CError.buildErr(this,"提交数据保存失败");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    private boolean dealDataInsert() {
        MMap tmpMap = new MMap();
        String noLimit = PubFun.getNoLimit(mG.ManageCom);
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mCaseNo);
        if (!tLLCaseDB.getInfo())
            return false;
        String trgtstate = tLLCaseDB.getRgtState();
        if (!trgtstate.equals("01") &&!trgtstate.equals("02")
            && !trgtstate.equals("08")) {
            CError tError = new CError();
            tError.moduleName = "AllCaseInfoBL";
            tError.functionName = "dealDataInsert";
            tError.errorMessage = "该状态下不允许更改检录信息。";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        //#1738 社保调查 理赔各处理流程支持
        String tReturnMsg = LLCaseCommon.checkSurveyRgtState(mCaseNo,trgtstate,"01");
        if(!"".equals(tReturnMsg)){
        	String tMsg = LLCaseCommon.checkSurveyRgtState(mCaseNo,trgtstate,"02");
        	if(!"".equals(tMsg)){
            	CError.buildErr(this, tMsg);
                return false;
        	}
        }
        
//====ADD====zhangtao=====2005-03-31===========BGN====================
        String sHandler = tLLCaseDB.getHandler();
        if (sHandler == null || "".equals(sHandler)) {
            //案件未指定处理人,指定为当前操作者
            LLCaseSchema tLLCaseSchema = new LLCaseSchema();
            tLLCaseSchema.setSchema(tLLCaseDB.getSchema());
            tLLCaseSchema.setHandler(mG.Operator);
            tmpMap.put(tLLCaseSchema, "UPDATE");
        }

//====ADD====zhangtao=====2005-03-31===========END====================

          //立案分案,即事件
         LLCaseRelaSet tLLCaseRelaSet = new LLCaseRelaSet();
         LLCaseRelaSchema tLLCaseRelaSchema = new LLCaseRelaSchema ();
         LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
         if (mLLSubReportSet != null) {
             if( mLLSubReportSet.size()!=0&&mLLSubReportSet!=null)
                 tLLSubReportSchema = new LLSubReportSchema();
             tLLSubReportSchema =    mLLSubReportSet.get(1);
             //如果事件没有，则创建事件关联
             String tsuprptno = tLLSubReportSchema.getSubRptNo()+"";
             if ("".equals(StrTool.cTrim(tsuprptno))) {
                 //事件增加
                 tsuprptno = PubFun1.CreateMaxNo("SUBRPTNO", noLimit);
                 tLLSubReportSchema.setSubRptNo(tsuprptno);
                 tLLSubReportSchema.setMngCom(mG.ManageCom);
                 tLLSubReportSchema.setOperator(mG.Operator);
                 tLLSubReportSchema.setMakeDate(strDate);
                 tLLSubReportSchema.setMakeTime(strTime);
                 tLLSubReportSchema.setModifyDate(strDate);
                 tLLSubReportSchema.setModifyTime(strTime);
                 tmpMap.put(tLLSubReportSchema, "INSERT");
             } else { //若事件存在，则先删除事件与案件的关联，然后在插入
                 tmpMap.put("delete from llcaserela where caseno='" + mCaseNo
                            +"' and subrptno='"+tsuprptno+"'" ,"DELETE");
             }

             String caserela = PubFun1.CreateMaxNo("CASERELANO",noLimit);
             this.mCaseRelaNo = caserela;
             tLLCaseRelaSchema.setCaseRelaNo(mCaseRelaNo);
             tLLCaseRelaSchema.setCaseNo(mCaseNo);
             tLLCaseRelaSchema.setSubRptNo(tsuprptno);

             tmpMap.put(tLLCaseRelaSchema,"INSERT");
         }
        /////////////

        for (int i = 1; i <= this.mLLCaseCureSet.size(); i++) {
            LLCaseCureSchema tLLCaseCureSchema = mLLCaseCureSet.get(i);
            tLLCaseCureSchema.setCustomerName(tLLCaseDB.getCustomerName());
            tLLCaseCureSchema.setCustomerNo(tLLCaseDB.getCustomerNo());
             String cureNo = PubFun1.CreateMaxNo("CURENO", noLimit);
            tLLCaseCureSchema.setSerialNo(cureNo);
            tLLCaseCureSchema.setCaseNo(mCaseNo);
            tLLCaseCureSchema.setCaseRelaNo(mCaseRelaNo);
            tLLCaseCureSchema.setMngCom(this.mG.ManageCom);
            tLLCaseCureSchema.setOperator(this.mG.Operator);
            tLLCaseCureSchema.setMakeDate(strDate);
            tLLCaseCureSchema.setMakeTime(strTime);
            tLLCaseCureSchema.setModifyDate(strDate);
            tLLCaseCureSchema.setModifyTime(strTime);
        }
        //#1777 新伤残评定标准及代码系统定义	伤残信息和残疾信息不可以同时录入
        boolean tFlag = false;
        for (int i = 1; i <= this.mLLCaseInfoSet.size(); i++) {
            LLCaseInfoSchema tLLCaseInfoSchema = mLLCaseInfoSet.get(i);
            String tType = tLLCaseInfoSchema.getType();

            if(("1".equals(tType)||"2".equals(tType))){
            	tFlag = true;
            }
            if(tFlag && "3".equals(tType)){
                CError.buildErr(this, "‘残疾信息’和‘伤残信息’只可录入一种，请重新录入！");
                return false;
            }
            tLLCaseInfoSchema.setCustomerName(tLLCaseDB.getCustomerName());
            tLLCaseInfoSchema.setCustomerNo(tLLCaseDB.getCustomerNo());
            String cureNo = PubFun1.CreateMaxNo("CASEINFO", noLimit);
            tLLCaseInfoSchema.setRgtNo(tLLCaseDB.getRgtNo());
            tLLCaseInfoSchema.setCaseNo(mCaseNo);
            tLLCaseInfoSchema.setCaseRelaNo(mCaseRelaNo);
            tLLCaseInfoSchema.setSerialNo(cureNo);
            tLLCaseInfoSchema.setMngCom(this.mG.ManageCom);
            tLLCaseInfoSchema.setOperator(this.mG.Operator);
            tLLCaseInfoSchema.setMakeDate(strDate);
            tLLCaseInfoSchema.setMakeTime(strTime);
            tLLCaseInfoSchema.setModifyDate(strDate);
            tLLCaseInfoSchema.setModifyTime(strTime);
        }
        for (int i = 1; i <= this.mLLOperationSet.size(); i++) {
            LLOperationSchema tLLOperationSchema = mLLOperationSet.get(i);

            String cureNo = PubFun1.CreateMaxNo("OPERATION", noLimit);
            tLLOperationSchema.setCaseNo(mCaseNo);
            tLLOperationSchema.setCaseRelaNo(mCaseRelaNo);
            tLLOperationSchema.setSerialNo(cureNo);
            tLLOperationSchema.setMngCom(mG.ManageCom);
            tLLOperationSchema.setOperator(mG.Operator);
            tLLOperationSchema.setMakeDate(strDate);
            tLLOperationSchema.setMakeTime(strTime);
            tLLOperationSchema.setModifyDate(strDate);
            tLLOperationSchema.setModifyTime(strTime);
        }
        if(mLLDisabilitySet!=null && mLLDisabilitySet.size()>0){
        for (int i = 1; i<=this.mLLDisabilitySet.size(); i++) {
            LLDisabilitySchema tLLDisabilitySchema = mLLDisabilitySet.get(i);
            String cureNo = PubFun1.CreateMaxNo("Disability", noLimit);
            tLLDisabilitySchema.setCaseNo(mCaseNo);
            tLLDisabilitySchema.setCaseRelaNo(mCaseRelaNo);
            tLLDisabilitySchema.setCustomerName(tLLCaseDB.getCustomerName());
            tLLDisabilitySchema.setCustomerNo(tLLCaseDB.getCustomerNo());
            tLLDisabilitySchema.setRgtNo(tLLCaseDB.getRgtNo());
            tLLDisabilitySchema.setSerialNo(cureNo);
            tLLDisabilitySchema.setManageCom(mG.ManageCom);
            tLLDisabilitySchema.setOperator(mG.Operator);
            tLLDisabilitySchema.setMakeDate(strDate);
            tLLDisabilitySchema.setMakeTime(strTime);
            tLLDisabilitySchema.setModifyDate(strDate);
            tLLDisabilitySchema.setModifyTime(strTime);
        }
        }
        for (int i = 1; i <= this.mLLOtherFactorSet.size(); i++) {
            LLOtherFactorSchema mLLOtherFactorSchema = mLLOtherFactorSet.get(i);
            mLLOtherFactorSchema.setCaseRelaNo(mCaseRelaNo);
            mLLOtherFactorSchema.setSubRptNo(tLLCaseRelaSchema.getSubRptNo());
            if (mLLOtherFactorSchema.getFactorCode().equals("9800") ||
                mLLOtherFactorSchema.getFactorCode().equals("9600")) { //重症监护
                IsSingle = "9";
            }
        }
        for (int i = 1; i <= this.mLLAccidentSet.size(); i++) {
            LLAccidentSchema mLLAccidentSchema = mLLAccidentSet.get(i);
            String AccidentNo = PubFun1.CreateMaxNo("ACCIDENTNO", noLimit);
            mLLAccidentSchema.setAccidentNo(AccidentNo);
            mLLAccidentSchema.setCaseNo(mCaseNo);
            mLLAccidentSchema.setCaseRelaNo(mCaseRelaNo);
        }
       //账单信息关联
       LLFeeMainSet tLLFeeMainSet = (LLFeeMainSet)this.mInputData.getObjectByObjectName("LLFeeMainSet",0);
       LLFeeMainSet saveFeeMainSet = new LLFeeMainSet();
       LLSecurityReceiptSet tLLSecurityReceiptSet = new LLSecurityReceiptSet();
//  if ( tLLFeeMainSet == null || tLLFeeMainSet.size()<=0)
//  {
//      CError.buildErr(this,"传入数据集为空");
//      return false;
//  }
       LLFeeMainDB tdb = new LLFeeMainDB();
       for (int i = 1; i <= tLLFeeMainSet.size(); i++) {
           LLFeeMainSchema inSchema = tLLFeeMainSet.get(i);
           tdb.setMainFeeNo(inSchema.getMainFeeNo());
           if (!tdb.getInfo()) {
               CError.buildErr(this, "查询账单[" + inSchema.getMainFeeNo() + "]失败");
               return false;
           }
           if(mLLSubReportSet.size()<=0){
               CError.buildErr(this, "查询事件信息失败");
               return false;
           }
           LLSubReportSchema tsLLSubReportSchema = mLLSubReportSet.get(1);
           LLFeeMainSchema dbSchema = tdb.getSchema();
           int days = PubFun.calInterval(tsLLSubReportSchema.getAccDate(),
                                         dbSchema.getHospStartDate(),
                                         "D");
           System.out.println("出事时间与住院时间相差的天数："+days);
//      if (days<0){
//          CError.buildErr(this, "帐单日期早于事件发生日期，不能进行关联！");
//          return false;
//      }
           dbSchema.setModifyDate(strDate);
           dbSchema.setModifyTime(strTime);
           dbSchema.setCaseRelaNo(mCaseRelaNo);
           saveFeeMainSet.add(dbSchema);
           if (IsSingle=="9"){
               LLSecurityReceiptDB tLLSecurityReceiptDB = new LLSecurityReceiptDB();
               tLLSecurityReceiptDB.setFeeDetailNo(dbSchema.getMainFeeNo());
               if(tLLSecurityReceiptDB.getInfo()){
                   if(dbSchema.getFeeType().equals("2")){
                       LLSecurityReceiptSchema tLLSecurityReceiptSchema =
                               tLLSecurityReceiptDB.getSchema();
                       tLLSecurityReceiptSchema.setLowAmnt(0);
                       LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
                       LLFeeMainSet sLLFeeMainSet = new LLFeeMainSet();
                       tLLFeeMainDB.setMainFeeNo(
                               tLLSecurityReceiptSchema.getMainFeeNo());
                       LLFeeMainSchema tLLFeeMainSchema = new
                               LLFeeMainSchema();
                       sLLFeeMainSet = tLLFeeMainDB.query();
                       tLLFeeMainSchema = sLLFeeMainSet.get(1);
                       LLCaseDB sLLCaseDB = new LLCaseDB();
                       LLCaseSchema tLLCaseSchema = new LLCaseSchema();
                       sLLCaseDB.setCaseNo(mCaseNo);
                       tLLCaseDB.query();
                       tLLCaseSchema = tLLCaseDB.getSchema();
                       String sql3 =
                               "select getrate from LLSecurityFactor where securitytype='4001' and comcode =substr('"
                               + tLLCaseSchema.getMngCom() +
                               "',1,4) and insuredstat='" +
                               tLLFeeMainSchema.getInsuredStat() +
                               "' and  startdate <='" +
                               tLLFeeMainSchema.getHospEndDate() + "'"; //查询退休人员补充政策
                       System.out.println(sql3);
                       ExeSQL tExeSQL = new ExeSQL();
                       String rate = tExeSQL.getOneValue(sql3);
                       double rate1 = 1;
                       if (!(rate.equals("") || rate == null ||
                             rate.equals("null"))) {
                           rate1 = Double.parseDouble(rate);
                       }
                       double tempmidAmnt = tLLSecurityReceiptSchema.
                                            getSelfPay1() -
                                            tLLSecurityReceiptSchema.
                                            getHighAmnt1() / rate1 -
                                            tLLSecurityReceiptSchema.
                                            getSuperAmnt();
                       if (tempmidAmnt < 0) {
                           tempmidAmnt = 0;
                       }
                       tempmidAmnt = Arith.round(tempmidAmnt, 2);
                       tLLSecurityReceiptSchema.setMidAmnt(Arith.round(tempmidAmnt*rate1,2));
//                  tLLSecurityReceiptSchema.setSuperAmnt(0);
                       tLLSecurityReceiptSet.add(tLLSecurityReceiptSchema);
                   }else{
                       CError.buildErr(this, "关联帐单为纯门诊帐单,未按特殊病或单病种规则理算");
                       return false;
                   }
               }
           }

       }
       if (IsSingle=="9"){
           tmpMap.put(tLLSecurityReceiptSet,"UPDATE");
       }
       tmpMap.put( saveFeeMainSet, "UPDATE");
       this.mResult.clear();

       ///////////
        String[] deltable = {"llcaseinfo","llcasecure" ,"lloperation","LLOtherFactor","LLAccident","LLDisability"};
        for (int i = 0; i < deltable.length; i++) {
         String delsql ="delete from " + deltable[i] +" where caseno ='" + mCaseNo +"'"
                        +" and caserelano='" + mCaseRelaNo +"'";
         tmpMap.put( delsql,"DELETE");
         String delsql2 ="delete from " + deltable[i] +" a where caseno ='" + mCaseNo +"'"
                        +" and not exists (select 1 from llcaserela where caserelano=a.caserelano)";
         tmpMap.put( delsql2,"DELETE");
     }

//     LLToClaimDutySet tLLToClaimDutySet = new LLToClaimDutySet();
//     LLToClaimDutyDB tLLToClaimDutyDB = new LLToClaimDutyDB();
//     tLLToClaimDutyDB.setCaseNo(mCaseNo);
//     tLLToClaimDutySet = tLLToClaimDutyDB.query();
//     for (int j=1 ; j <= tLLToClaimDutySet.size(); j++){
//         LLToClaimDutySchema tLLToClaimDutySchema = tLLToClaimDutySet.get(j);
//         tLLToClaimDutySchema.setCaseRelaNo(mCaseRelaNo);
//         mLLToClaimDutySet.add(tLLToClaimDutySchema);
//     }
//
//     String tDELETESQL = "delete from lltoclaimduty where caseno='"+mCaseNo+"'";
//
//     tmpMap.put(tDELETESQL, "DELETE");
//     tmpMap.put(this.mLLToClaimDutySet, "INSERT");
     tmpMap.put(this.mLLCaseCureSet, "INSERT");
     tmpMap.put(this.mLLCaseInfoSet, "INSERT");
     tmpMap.put(this.mLLOperationSet, "INSERT");
     tmpMap.put(this.mLLDisabilitySet,"INSERT");
     tmpMap.put(this.mLLOtherFactorSet, "INSERT");
     tmpMap.put(this.mLLAccidentSet, "INSERT");
     this.mResult.add( tmpMap );
     return true;
 }


    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    //先要调用prepareOutputData()函数
    private boolean dealData() {
        return true;
    }

    private boolean getInputData(VData cInputData) {
        mG = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        LLCaseSchema tLLCaseSchema = (LLCaseSchema) cInputData.
                                     getObjectByObjectName("LLCaseSchema", 0);
        mCaseNo = tLLCaseSchema.getCaseNo();
        mCaseRelaNo = (String) cInputData.getObjectByObjectName("String", 0);
        mLLCaseInfoSet = (LLCaseInfoSet) cInputData.
                         getObjectByObjectName("LLCaseInfoSet", 0);
        mLLCaseCureSet = (LLCaseCureSet) cInputData.
                         getObjectByObjectName("LLCaseCureSet", 0);
        mLLOperationSet = (LLOperationSet) cInputData.
                          getObjectByObjectName("LLOperationSet", 0);
        mLLOtherFactorSet = (LLOtherFactorSet) cInputData.
                            getObjectByObjectName("LLOtherFactorSet", 0);
        mLLAccidentSet = (LLAccidentSet) cInputData.
                         getObjectByObjectName("LLAccidentSet", 0);
        mLLSubReportSet = (LLSubReportSet)cInputData.
                          getObjectByObjectName("LLSubReportSet",0);
        mLLDisabilitySet = (LLDisabilitySet) cInputData.
                         getObjectByObjectName("LLDisabilitySet", 0);
        strDate = PubFun.getCurrentDate();
        strTime = PubFun.getCurrentTime();
        return true;
    }

    private void prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(DisplayFlag);
            mInputData.add(AccidentType);
            mInputData.add(mLLCaseInfoSet);
            mResult.clear();
            mResult.add(mLLCaseInfoSet);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        AllCaseInfoBL aCaseInfoBL = new AllCaseInfoBL();
        VData tVData = new VData();
        LLCaseSchema aLLCaseSchema = new LLCaseSchema();
        aLLCaseSchema.setCaseNo("00100020030550000090");
        GlobalInput tG = new GlobalInput();
        LLCaseInfoSchema aLLCaseInfoSchema = new LLCaseInfoSchema();
        LLCaseInfoSet aLLCaseInfoSet = new LLCaseInfoSet();
        aLLCaseInfoSchema.setCaseNo("86000020030550000002");
        aLLCaseInfoSchema.setType("YW");
        aLLCaseInfoSchema.setCode("001");
        aLLCaseInfoSchema.setName("1111111111");
        aLLCaseInfoSet.add(aLLCaseInfoSchema);
        tVData.add(aLLCaseInfoSet);
        aCaseInfoBL.submitData(tVData,"INSERT");
        tVData.add(tG);
        tVData.add(aLLCaseSchema);
    }
}
