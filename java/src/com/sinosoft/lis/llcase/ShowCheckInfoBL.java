package com.sinosoft.lis.llcase;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 案件－立案－分案保单明细保存功能业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @version 1.0
 */
public class ShowCheckInfoBL
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData ;
  private VData mResult = new VData();
  private String mOperate;
  //xLLClaimUWMDetailSet承载最终的返回结果集合
  private LLClaimUWMDetailSet xLLClaimUWMDetailSet = new LLClaimUWMDetailSet();
  private String tRgtNo="";
  public ShowCheckInfoBL() {}

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;

    if (!getInputData(cInputData))
      return false;
    if(cOperate.equals("INIT"))
    {
      if(!initData())
        return false;
    }
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    if(mOperate.equals("INIT"))
    {
      tRgtNo = (String)cInputData.get(0);
    }
    return true;
  }
  private boolean initData()
  {
    xLLClaimUWMDetailSet.set(query_date());
    mResult.clear();
    mResult.addElement(xLLClaimUWMDetailSet);
    return true;
  }
  public  LLClaimUWMDetailSet query_date()
  {
    LLClaimUWMDetailSet mLLClaimUWMDetailSet = new LLClaimUWMDetailSet();
    LLClaimUWMDetailSet endLLClaimUWMDetailSet = new LLClaimUWMDetailSet();
    LLClaimUWMDetailDB mLLClaimUWMDetailDB = new LLClaimUWMDetailDB();
    //查询对应于该立案号码共有多少条分公司的审核记录
    String m_sql = "select * from LLClaimUWMDetail where RgtNo = '"+tRgtNo
                 +"' and Appphase = '0' order by makedate,maketime ";
    System.out.println("查询审核的语句是"+m_sql);
    ExeSQL exesql = new ExeSQL();
    mLLClaimUWMDetailSet.set(mLLClaimUWMDetailDB.executeQuery(m_sql));
    if(mLLClaimUWMDetailSet.size()>0)
    {
      for (int i=1;i<=mLLClaimUWMDetailSet.size();i++)
      {
        LLClaimUWMDetailSchema tLLClaimUWMDetailSchema = new LLClaimUWMDetailSchema();
        LLClaimUWMDetailSchema endLLClaimUWMDetailSchema = new LLClaimUWMDetailSchema();//存放由分公司审核到分公司签批的案件
        LLClaimUWMDetailSchema end2LLClaimUWMDetailSchema = new LLClaimUWMDetailSchema();//存放由分公司审核到总公司签批的案件
        tLLClaimUWMDetailSchema.setSchema(mLLClaimUWMDetailSet.get(i));
        String t_AppClmUWer = tLLClaimUWMDetailSchema.getAppClmUWer();
        endLLClaimUWMDetailSchema.setClmUWer(tLLClaimUWMDetailSchema.getClmUWer());//分公司的最初的审核人员
        System.out.println("申请审核人员是"+t_AppClmUWer);
        endLLClaimUWMDetailSchema.setClmUWNo(tLLClaimUWMDetailSchema.getClmUWNo());
        endLLClaimUWMDetailSchema.setAppClmUWer(t_AppClmUWer);//符合人员
        System.out.println("符合人员是"+t_AppClmUWer);
        LLClaimUWMDetailSet tLLClaimUWMDetailSet = new LLClaimUWMDetailSet();
        //将char型的进行相加的函数
        String ClmUWNo = get_ClmUWNo(tLLClaimUWMDetailSchema.getClmUWNo());
        String t_sql = "select * from LLClaimUWMDetail where ClmUWer = '"
                     +t_AppClmUWer+"' and RgtNo = '"+tRgtNo
                     +"' and ClmUWNo='"+ClmUWNo
                     +"' and AppPhase = '1' order by makedate,maketime";
        System.out.println("查询符合签批的是"+t_sql);
        LLClaimUWMDetailDB tLLClaimUWMDetailDB = new LLClaimUWMDetailDB();
        tLLClaimUWMDetailSet.set(tLLClaimUWMDetailDB.executeQuery(t_sql));
        if(tLLClaimUWMDetailSet.size()>0)
        {
          LLClaimUWMDetailSchema aLLClaimUWMDetailSchema = new LLClaimUWMDetailSchema();
          aLLClaimUWMDetailSchema.setSchema(tLLClaimUWMDetailSet.get(1));
          String tAppActionType = aLLClaimUWMDetailSchema.getAppActionType();
          endLLClaimUWMDetailSchema.setClmUWer(tLLClaimUWMDetailSchema.getClmUWer());//分公司的审核人员
          System.out.println("理算人是"+endLLClaimUWMDetailSchema.getClmUWer());
          endLLClaimUWMDetailSchema.setClmUWNo(tLLClaimUWMDetailSchema.getClmUWNo());
          endLLClaimUWMDetailSchema.setAppClmUWer(aLLClaimUWMDetailSchema.getClmUWer());//分公司的签批人员
          System.out.println("符合人是"+endLLClaimUWMDetailSchema.getAppClmUWer());
          endLLClaimUWMDetailSchema.setRemark(get_info(aLLClaimUWMDetailSchema.getAppActionType()));
          System.out.println("审核意见是"+endLLClaimUWMDetailSchema.getAppActionType());
          endLLClaimUWMDetailSchema.setMakeDate(aLLClaimUWMDetailSchema.getMakeDate());
          System.out.println("实际是"+endLLClaimUWMDetailSchema.getMakeDate());
          endLLClaimUWMDetailSchema.setMakeTime(aLLClaimUWMDetailSchema.getMakeTime());
          endLLClaimUWMDetailSet.add(endLLClaimUWMDetailSchema);
          if(!(tAppActionType==null||tAppActionType.equals("")))
          {
            if(tAppActionType.equals("3"))
            {
              String b_ClmUWNo = get_ClmUWNo(aLLClaimUWMDetailSchema.getClmUWNo());
              String b_sql = "select * from LLClaimUWMDetail where ClmUWer = '"
                           +aLLClaimUWMDetailSchema.getAppClmUWer()+"' and RgtNo = '"+tRgtNo
                           +"'and ClmUWNo= '"+b_ClmUWNo+"' ";
              System.out.println("查询的语句是"+b_sql);
              LLClaimUWMDetailDB bLLClaimUWMDetailDB = new LLClaimUWMDetailDB();
              LLClaimUWMDetailSet bLLClaimUWMDetailSet = new LLClaimUWMDetailSet();
              bLLClaimUWMDetailSet.set(bLLClaimUWMDetailDB.executeQuery(b_sql));
              if(bLLClaimUWMDetailSet.size()>0)
              {
                LLClaimUWMDetailSchema bLLClaimUWMDetailSchema = new LLClaimUWMDetailSchema();
                bLLClaimUWMDetailSchema.setSchema(bLLClaimUWMDetailSet.get(1));
                end2LLClaimUWMDetailSchema.setClmUWer(tLLClaimUWMDetailSchema.getClmUWer());//分公司的审核人员
                System.out.println("分公司审核直接到总公司的签批的情况");
                System.out.println("理算人员是"+end2LLClaimUWMDetailSchema.getClmUWer());
                end2LLClaimUWMDetailSchema.setClmUWNo(tLLClaimUWMDetailSchema.getClmUWNo());
                end2LLClaimUWMDetailSchema.setAppClmUWer(bLLClaimUWMDetailSchema.getClmUWer());
                System.out.println("总公司的审核人员是"+end2LLClaimUWMDetailSchema.getAppClmUWer());
                end2LLClaimUWMDetailSchema.setRemark(get_info(bLLClaimUWMDetailSchema.getAppActionType()));
                System.out.println("总公司的审核意见是"+end2LLClaimUWMDetailSchema.getAppActionType());
                end2LLClaimUWMDetailSchema.setMakeDate(bLLClaimUWMDetailSchema.getMakeDate());
                end2LLClaimUWMDetailSchema.setMakeTime(bLLClaimUWMDetailSchema.getMakeTime());
                endLLClaimUWMDetailSet.add(end2LLClaimUWMDetailSchema);
              }
            }
          }
        }
      }
    }
    System.out.println("最终的测试记录");
    System.out.println("序号");
    System.out.println("理算申请人");
    System.out.println("审核人");
    System.out.println("意见");
    System.out.println("时间");


    for(int count=1;count<=endLLClaimUWMDetailSet.size();count++)
    {
      System.out.println(count);
      System.out.println(endLLClaimUWMDetailSet.get(count).getClmUWer());
      System.out.println(endLLClaimUWMDetailSet.get(count).getAppClmUWer());
      System.out.println(endLLClaimUWMDetailSet.get(count).getRemark());
      System.out.println(endLLClaimUWMDetailSet.get(count).getMakeDate());
    }
    return endLLClaimUWMDetailSet;
  }
  private String get_ClmUWNo(String t_ClmUWNo)
  {
    String m_ClmUWNo="";
    int i_ClmUWNo = Integer.parseInt(t_ClmUWNo);
    i_ClmUWNo = i_ClmUWNo+1;
    m_ClmUWNo = String.valueOf(i_ClmUWNo);
    System.out.println("转换后的号码是"+m_ClmUWNo);
    return m_ClmUWNo;
  }
  /****************************************************************************
   * Name      :get_info;
   * CreateDate:2003-12-19
   * Author    :LiuYansong
   * Function  :判断理赔状态：1---确认；2----退回;3----上报
   * parameter :AppActionType(申请动作)
   */
  private String get_info(String t_AppActionType)
  {
    String info = "没有录入";
    if(t_AppActionType.equals("1"))
    {
      info = "确认";
    }
    if(t_AppActionType.equals("2"))
    {
      info = "退回";
    }
    if(t_AppActionType.equals("3"))
    {
      info = "上报";
    }
    System.out.println("意见是"+info);
    return info;
  }

  public static void main(String[] args)
  {
    String tRgtNo  = "86000020030510000005";
    VData tVData = new VData();
    tVData.addElement(tRgtNo);
    ShowCheckInfoBL aShowCheckInfoBL = new ShowCheckInfoBL();
    aShowCheckInfoBL.submitData(tVData,"INIT");
  }
}