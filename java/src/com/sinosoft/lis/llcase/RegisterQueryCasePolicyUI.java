package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: 业务核心系统－立案模块中</p>
 * <p>Description:承保暂交费功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class RegisterQueryCasePolicyUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public RegisterQueryCasePolicyUI() {}


  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

   RegisterQueryCasePolicyBL tRegisterQueryCasePolicyBL = new RegisterQueryCasePolicyBL();
   System.out.println("出错在这了");
    System.out.println("---UI BEGIN---");
    System.out.println("执行到此");
    if (tRegisterQueryCasePolicyBL.submitData(cInputData,mOperate) == false)
    {
  		// @@错误处理
      System.out.println("出错了");
      this.mErrors.copyAllErrors(tRegisterQueryCasePolicyBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "RegisterQueryCasePolicyUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
			mResult = tRegisterQueryCasePolicyBL.getResult();
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

}