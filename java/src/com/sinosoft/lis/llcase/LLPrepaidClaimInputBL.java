/**
 * <p>
 * ClassName: LLPrepaidClaimInputBL.java
 * </p>
 * <p>
 * Description: 预付赔款录入
 * </p>
 * <p>
 * Copyright: Copyright (c) 2009
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author
 * @version 1.0
 * @CreateDate：2010-11-25
 */

// 包名
package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LLPrepaidClaimInputBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	/** 数据操作字符串 */
	private String mOperate;

	/** 业务处理相关变量 */
	private LLPrepaidClaimSchema mLLPrepaidClaimSchema = new LLPrepaidClaimSchema();

	private LLPrepaidClaimDetailSet mLLPrepaidClaimDetailSet = new LLPrepaidClaimDetailSet();

	private LLPrepaidTraceSet mLLPrepaidTraceSet = new LLPrepaidTraceSet();

	// 团体保单号
	private String mGrpContNo = "";

	private double mSumMoney = 0;

	private String mOperator = "";

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private MMap mMap = new MMap();
	
    /**社保案件标记 0：非社保；1：社保*/
    private String mSocialFlag = "0";
    
    private String mPrepaidUpUserCode = "";

	public LLPrepaidClaimInputBL() {

	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = cInputData;
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}

		if (!prepareOutputData()) {
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();

		System.out.println("Start LLPrepaidClaimInputBL Submit...");

		if (!tPubSubmit.submitData(mInputData, null)) {
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}

		mInputData = null;
		System.out.println("End LLPrepaidClaimInputBL Submit...");
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * 
	 */
	private boolean getInputData() {
		if ("".equals(mOperate)) {
			buildError("getInputData", "获取操作类型失败");
			return false;
		}

		if (mInputData == null) {
			buildError("getInputData", "获取页面传输数据失败");
			return false;
		}

		mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0);

		if (mGlobalInput == null) {
			buildError("getInputData", "获取操作人信息失败");
			return false;
		}

		mOperator = mGlobalInput.Operator;

		if ("".equals(mOperator)) {
			buildError("getInputData", "获取操作人信息失败");
			return false;
		}

		mLLPrepaidClaimSchema = (LLPrepaidClaimSchema) mInputData
				.getObjectByObjectName("LLPrepaidClaimSchema", 0);

		if (mLLPrepaidClaimSchema == null) {
			buildError("getInputData", "获取预付赔款数据失败");
			return false;
		}

		mLLPrepaidClaimDetailSet = (LLPrepaidClaimDetailSet) mInputData
				.getObjectByObjectName("LLPrepaidClaimDetailSet", 0);

		return true;
	}

	private boolean checkData() {
		if ("Prepaid||Apply".equals(mOperate)) {
			if (mLLPrepaidClaimDetailSet == null
					|| mLLPrepaidClaimDetailSet.size() <= 0) {
				buildError("checkData", "获取预付赔款明细数据失败");
				return false;
			}

			if (mLLPrepaidClaimSchema.getGrpContNo() == null
					|| "".equals(mLLPrepaidClaimSchema.getGrpContNo())) {
				buildError("checkData", "团体保单号不能为空");
				return false;
			}

			if (mLLPrepaidClaimSchema.getPayMode() == null
					|| "".equals(mLLPrepaidClaimSchema.getPayMode())) {
				buildError("checkData", "给付方式不能为空");
				return false;
			}

			if (!"1".equals(mLLPrepaidClaimSchema.getRgtType())
					&& !"2".equals(mLLPrepaidClaimSchema.getRgtType())) {
				buildError("checkData", "预付赔款类型获取错误");
				return false;
			}

			LLPrepaidGrpContDB tLLPrepaidGrpContDB = new LLPrepaidGrpContDB();
			tLLPrepaidGrpContDB.setGrpContNo(mLLPrepaidClaimSchema
					.getGrpContNo());

			mGrpContNo = mLLPrepaidClaimSchema.getGrpContNo();
			if (!tLLPrepaidGrpContDB.getInfo()) {
				buildError("checkData", "该保单未进行预付保单确认");
				return false;
			}

			if (!"1".equals(tLLPrepaidGrpContDB.getState())) {
				buildError("checkData", "该预付保单已经撤销或状态有误");
				return false;
			}

			if ("1".equals(mLLPrepaidClaimSchema.getRgtType())) {
				if (mLLPrepaidClaimSchema.getBankCode() == null
						|| "".equals(mLLPrepaidClaimSchema.getBankCode())) {
					buildError("checkData", "银行编码不能为空");
					return false;
				}

				if (mLLPrepaidClaimSchema.getBankAccNo() == null
						|| "".equals(mLLPrepaidClaimSchema.getBankAccNo())) {
					buildError("checkData", "银行账号不能为空");
					return false;
				}

				if (mLLPrepaidClaimSchema.getAccName() == null
						|| "".equals(mLLPrepaidClaimSchema.getAccName())) {
					buildError("checkData", "银行账户名不能为空");
					return false;
				}
			} else if ("2".equals(mLLPrepaidClaimSchema.getRgtType())) {
				if (!"1".equals(mLLPrepaidClaimSchema.getPayMode())
						&& !"2".equals(mLLPrepaidClaimSchema.getPayMode())) {
					if (mLLPrepaidClaimSchema.getBankCode() == null
							|| "".equals(mLLPrepaidClaimSchema.getBankCode())) {
						buildError("checkData", "银行编码不能为空");
						return false;
					}
				}

				if ("4".equals(mLLPrepaidClaimSchema.getPayMode())
						|| "11".equals(mLLPrepaidClaimSchema.getPayMode())) {
					if (mLLPrepaidClaimSchema.getBankAccNo() == null
							|| "".equals(mLLPrepaidClaimSchema.getBankAccNo())) {
						buildError("checkData", "银行账号不能为空");
						return false;
					}

					if (mLLPrepaidClaimSchema.getAccName() == null
							|| "".equals(mLLPrepaidClaimSchema.getAccName())) {
						buildError("checkData", "银行账户名不能为空");
						return false;
					}
				}

			}

		} else if ("Prepaid||Cancel".equals(mOperate)) {
			if (mLLPrepaidClaimSchema.getPrepaidNo() == null
					|| "".equals(mLLPrepaidClaimSchema.getPrepaidNo())) {
				buildError("checkData", "预付赔款号不能为空");
				return false;
			}

			LLPrepaidClaimDB tLLPrepaidClaimDB = new LLPrepaidClaimDB();
			tLLPrepaidClaimDB
					.setPrepaidNo(mLLPrepaidClaimSchema.getPrepaidNo());

			if (!tLLPrepaidClaimDB.getInfo()) {
				buildError("checkData", "预付赔款查询失败");
				return false;
			}

			mLLPrepaidClaimSchema = tLLPrepaidClaimDB.getSchema();

			if (!mOperator.equals(mLLPrepaidClaimSchema.getRegister())
					&& !LLCaseCommon.checkUPUpUser(mOperator,
							mLLPrepaidClaimSchema.getRegister())) {
				buildError("checkData", "操作人无撤销权限");
				return false;
			}

			if (!"01".equals(mLLPrepaidClaimSchema.getRgtState())
					&& !"02".equals(mLLPrepaidClaimSchema.getRgtState())) {
				buildError("checkData", "当前状态不允许进行撤销");
				return false;
			}

		}

		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		if ("Prepaid||Apply".equals(mOperate)) {
			if (!prepaidApply()) {
				return false;
			}
		} else if ("Prepaid||Cancel".equals(mOperate)) {
			if (!prepaidCancel()) {
				return false;
			}
		}
		return true;
	}

	private boolean prepareOutputData() {
		mInputData.clear();
		mInputData.add(mMap);
		mResult.add(mLLPrepaidClaimSchema);
		return true;
	}

	/**
	 * 预付赔款申请
	 * 
	 * @return
	 */
	private boolean prepaidApply() {
		if ("".equals(mLLPrepaidClaimSchema.getPrepaidNo())) {
			if (!prepaidInsert()) {
				return false;
			}
		} else {
			if (!prepaidUpdate()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 新申请预付赔款
	 * 
	 * @return
	 */
	private boolean prepaidInsert() {
		ExeSQL tExeSQL = new ExeSQL();
		
		//#1962 社保业务预付赔款功能需求分析
		//1.首先判断保单是否为社保保单
        mSocialFlag = LLCaseCommon.checkSocialSecurity(mGrpContNo, "grpno");
        //2.社保保单，只能由社保用户“确认预付保单”
        if("1".equals(mSocialFlag)){
        	LLSocialClaimUserDB tSocialClaimUserDB = new LLSocialClaimUserDB();
        	LLSocialClaimUserSet tSocialClaimUserSet = new LLSocialClaimUserSet();
        	//预付保单确认时操作人必须为在“社保业务理赔人员权限配置”中进行配置的用户，且所属机构为分公司
        	String tSSQL = "select * from llsocialclaimuser where 1=1 and" +
        			" stateflag='1' and usercode ='"+mOperator+"' with ur ";
        	System.out.println("tSSQL:"+tSSQL);
        	tSocialClaimUserSet = tSocialClaimUserDB.executeQuery(tSSQL);
        	if(tSocialClaimUserSet.size() <= 0){
        		// @@错误处理
    			buildError("prepaidInsert", 
    					"当前登录用户"+mOperator+"未在社保业务理赔人员权限配置，不能对社保保单进行‘申请’！");
    			return false;
        	}
        	if(tSocialClaimUserSet.get(1).getComCode().length() != 4){
        		//@@错误处理
    			buildError("prepaidInsert", 
    					"请使用分公司用户进行申请！");
    			return false;

        	}
        	if(tSocialClaimUserSet.get(1).getPrepaidUpUserCode() == null
    				|| "".equals(tSocialClaimUserSet.get(1).getPrepaidUpUserCode())){
        		//@@错误处理
    			buildError("prepaidInsert", 
    					"用户没有预付赔款审批上级，不能申请！");
    			return false;

        	}
        	mPrepaidUpUserCode = tSocialClaimUserSet.get(1).getPrepaidUpUserCode();
        }else{
        	LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
    		tLLClaimUserDB.setUserCode(mOperator);

    		if (!tLLClaimUserDB.getInfo()) {
    			buildError("prepaidInsert", "请使用理赔用户进行申请");
    			return false;
    		}

    		if (tLLClaimUserDB.getComCode().length() != 4) {
    			buildError("prepaidInsert", "请使用分公司用户申请");
    			return false;
    		}

    		if (tLLClaimUserDB.getPrepaidUpUserCode() == null
    				|| "".equals(tLLClaimUserDB.getPrepaidUpUserCode())) {
    			buildError("prepaidInsert", "用户没有预付赔款审批上级，不能申请");
    			return false;
    		}
    		mPrepaidUpUserCode = tLLClaimUserDB.getPrepaidUpUserCode();
        }
		

		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setGrpContNo(mLLPrepaidClaimSchema.getGrpContNo());

		if (!tLCGrpContDB.getInfo()) {
			buildError("prepaidInsert", "团体保单查询失败");
			return false;
		}

		if (!"1".equals(tLCGrpContDB.getAppFlag())
				|| (!"1".equals(tLCGrpContDB.getStateFlag())&&!"3".equals(tLCGrpContDB.getStateFlag())&&!"2".equals(tLCGrpContDB.getStateFlag()))) {
			buildError("prepaidInsert", "团体保单状态非有效");
			return false;
		}

		if ("1".equals(mLLPrepaidClaimSchema.getRgtType())) {

			String tPrepaidSQL = "SELECT count(1) FROM llprepaidclaim WHERE grpcontno='"
					+ mLLPrepaidClaimSchema.getGrpContNo()
					+ "' AND rgtstate not in ('05','06','07') AND rgttype='1' ";

			int tPrepaidCount = Integer.parseInt(tExeSQL
					.getOneValue(tPrepaidSQL));

			if (tPrepaidCount > 0) {
				buildError("prepaidInsert", "保单下有未通知的申请类预付赔款");
				return false;
			}
		} else if ("2".equals(mLLPrepaidClaimSchema.getRgtType())) {
			String tPrepaidSQL = "SELECT count(1) FROM llprepaidclaim WHERE grpcontno='"
					+ mLLPrepaidClaimSchema.getGrpContNo()
					+ "' AND rgtstate not in ('05','06','07') ";

			int tPrepaidCount = Integer.parseInt(tExeSQL
					.getOneValue(tPrepaidSQL));

			if (tPrepaidCount > 0) {
				buildError("prepaidInsert", "保单下有未通知的预付赔款");
				return false;
			}
		}

		mLLPrepaidClaimSchema.setRgtState("01");
		mLLPrepaidClaimSchema.setGrpName(tLCGrpContDB.getGrpName());
		mLLPrepaidClaimSchema.setRgtDate(mCurrentDate);
		mLLPrepaidClaimSchema.setRegister(mOperator);
		mLLPrepaidClaimSchema.setHandler(mPrepaidUpUserCode);
		mLLPrepaidClaimSchema.setDealer(mPrepaidUpUserCode);
		mLLPrepaidClaimSchema.setManageCom(tLCGrpContDB.getManageCom());
		mLLPrepaidClaimSchema.setOperator(mOperator);
		mLLPrepaidClaimSchema.setMakeDate(mCurrentDate);
		mLLPrepaidClaimSchema.setMakeTime(mCurrentTime);
		mLLPrepaidClaimSchema.setModifyDate(mCurrentDate);
		mLLPrepaidClaimSchema.setModifyTime(mCurrentTime);

		mSumMoney = 0.0;

		LLPrepaidClaimDetailSet tLLPrepaidClaimDetailSet = new LLPrepaidClaimDetailSet();

		if (!updatePrepaidDetail(tLLPrepaidClaimDetailSet)) {
			return false;
		}

		String tLimit = PubFun.getNoLimit(tLCGrpContDB.getManageCom());
		String tPrepaidNo = PubFun1.CreateMaxNo("PREPAIDNO", tLimit);
		System.out.println("预付赔款号：" + tPrepaidNo);

		mLLPrepaidClaimSchema.setPrepaidNo(tPrepaidNo);
		mLLPrepaidClaimSchema.setSumMoney(Arith.round(mSumMoney, 2));

		for (int i = 1; i <= tLLPrepaidClaimDetailSet.size(); i++) {
			tLLPrepaidClaimDetailSet.get(i).setPrepaidNo(tPrepaidNo);
		}

		for (int i = 1; i <= mLLPrepaidTraceSet.size(); i++) {
			mLLPrepaidTraceSet.get(i).setOtherNo(tPrepaidNo);
		}

		mMap.put(mLLPrepaidClaimSchema, "INSERT");
		mMap.put(tLLPrepaidClaimDetailSet, "INSERT");
		mMap.put(mLLPrepaidTraceSet, "INSERT");
		return true;
	}

	/**
	 * 预付赔款修改
	 * 
	 * @return
	 */
	private boolean prepaidUpdate() {
		LLPrepaidClaimDB tLLPrepaidClaimDB = new LLPrepaidClaimDB();
		tLLPrepaidClaimDB.setPrepaidNo(mLLPrepaidClaimSchema.getPrepaidNo());

		if (!tLLPrepaidClaimDB.getInfo()) {
			buildError("prepaidUpdate", "查询预付赔款信息失败");
			return false;
		}

		LLPrepaidClaimSchema tLLPrepaidClaimSchema = tLLPrepaidClaimDB
				.getSchema();

		if (!"01".equals(tLLPrepaidClaimSchema.getRgtState())) {
			buildError("prepaidUpdate", "该状态下不允许修改预付赔款信息");
			return false;
		}

		if (!mOperator.equals(tLLPrepaidClaimSchema.getRegister())) {
			buildError("prepaidUpdate", "操作人不是预付赔款的申请人");
			return false;
		}

		if (!tLLPrepaidClaimSchema.getGrpContNo().equals(
				mLLPrepaidClaimSchema.getGrpContNo())) {
			buildError("prepaidUpdate", "不能更换预付赔款保单");
			return false;
		}

		tLLPrepaidClaimSchema.setPayMode(mLLPrepaidClaimSchema.getPayMode());
		tLLPrepaidClaimSchema.setBankCode(mLLPrepaidClaimSchema.getBankCode());
		tLLPrepaidClaimSchema
				.setBankAccNo(mLLPrepaidClaimSchema.getBankAccNo());
		tLLPrepaidClaimSchema.setAccName(mLLPrepaidClaimSchema.getAccName());
		tLLPrepaidClaimSchema.setOperator(mOperator);
		tLLPrepaidClaimSchema.setModifyDate(mCurrentDate);
		tLLPrepaidClaimSchema.setModifyTime(mCurrentTime);

		mSumMoney = 0.0;

		LLPrepaidClaimDetailSet tLLPrepaidClaimDetailSet = new LLPrepaidClaimDetailSet();

		if (!updatePrepaidDetail(tLLPrepaidClaimDetailSet)) {
			return false;
		}

		tLLPrepaidClaimSchema.setSumMoney(Arith.round(mSumMoney, 2));

		String tPrepaidDeleteSQL = "DELETE FROM llprepaidclaimdetail where prepaidno='"
				+ mLLPrepaidClaimSchema.getPrepaidNo() + "'";

		String tPrepaidTraceSQL = "DELETE FROM llprepaidtrace where otherno='"
				+ mLLPrepaidClaimSchema.getPrepaidNo()
				+ "' AND othernotype='Y' AND state='0' ";

		mMap.put(tLLPrepaidClaimSchema, "UPDATE");
		mMap.put(tPrepaidDeleteSQL, "DELETE");
		mMap.put(tPrepaidTraceSQL, "DELETE");
		mMap.put(tLLPrepaidClaimDetailSet, "INSERT");
		mMap.put(mLLPrepaidTraceSet, "INSERT");

		return true;
	}

	private boolean updatePrepaidDetail(
			LLPrepaidClaimDetailSet aLLPrepaidClaimDetailSet) {
		ExeSQL tExeSQL = new ExeSQL();

		for (int i = 1; i <= mLLPrepaidClaimDetailSet.size(); i++) {
			LLPrepaidClaimDetailSchema tLLPrepaidClaimDetailSchema = mLLPrepaidClaimDetailSet
					.get(i);

			String tGrpPolNo = tLLPrepaidClaimDetailSchema.getGrpPolNo();

			if (tGrpPolNo == null || "".equals(tGrpPolNo)) {
				buildError("updatePrepaidDetail", "团体险种号获取失败");
				return false;
			}

			LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
			tLCGrpPolDB.setGrpPolNo(tGrpPolNo);

			if (!tLCGrpPolDB.getInfo()) {
				buildError("updatePrepaidDetail", "团体险种信息查询失败");
				return false;
			}

			if (!"1".equals(tLCGrpPolDB.getAppFlag())
					|| (!"1".equals(tLCGrpPolDB.getStateFlag())&&!"3".equals(tLCGrpPolDB.getStateFlag())&&!"2".equals(tLCGrpPolDB.getStateFlag()))) {
				buildError("updatePrepaidDetail", "团体险种状态非有效");
				return false;
			}

			if (tLLPrepaidClaimDetailSchema.getMoney() <= 0) {
				buildError("updatePrepaidDetail", "本次预付金额需大于0");
				return false;
			}

			String tActuPaySQL = " SELECT (NVL((SELECT SUM(sumactupaymoney) FROM ljapaygrp WHERE endorsementno IS NULL "
					+ " AND grppolno='"
					+ tGrpPolNo
					+ " '),0) + "
					+ " NVL((SELECT SUM(getmoney) FROM ljagetendorse WHERE GrpPolNo='"
					+ tGrpPolNo + "'),0)) from dual ";
			double tSumActuPay = Double.parseDouble(tExeSQL
					.getOneValue(tActuPaySQL));
			System.out.println(tGrpPolNo + "下实收保费" + tSumActuPay);

			String tRealPaySQL = "SELECT NVL(SUM(pay),0) FROM ljagetclaim WHERE othernotype='5' AND grppolno='"
					+ tGrpPolNo + "'";
			double tSumRealPay = Double.parseDouble(tExeSQL
					.getOneValue(tRealPaySQL));
			System.out.println(tGrpPolNo + "下通知赔款" + tSumRealPay);

			LLPrepaidGrpPolDB tLLPrepaidGrpPolDB = new LLPrepaidGrpPolDB();
			tLLPrepaidGrpPolDB.setGrpPolNo(tGrpPolNo);

			if (!tLLPrepaidGrpPolDB.getInfo()) {
				buildError("updatePrepaidDetail", "预付险种查询失败");
				return false;
			}

			double tPrepaidBala = Arith.round(tLLPrepaidGrpPolDB
					.getApplyAmount()
					+ tLLPrepaidGrpPolDB.getRegainAmount()
					+ tLLPrepaidGrpPolDB.getSumPay(), 2);
			System.out.println(tGrpPolNo + "下预付赔款余额" + tPrepaidBala);

			if (tLLPrepaidGrpPolDB.getPrepaidBala() != tPrepaidBala) {
				buildError("updatePrepaidDetail", "预付险种下余额错误");
				return false;
			}

			if ("1".equals(mLLPrepaidClaimSchema.getRgtType())) {
				// 取消对预付金额的控制 2011-12-30 mn #323
//				if (Arith.round(tSumActuPay - tSumRealPay - tPrepaidBala, 2) < tLLPrepaidClaimDetailSchema
//						.getMoney()) {
//					buildError("updatePrepaidDetail",
//							"本次预付金额不能超过实收保费扣除该险种下理赔结案通知赔款与预付赔款余额之和");
//					return false;
//				}
			} else if ("2".equals(mLLPrepaidClaimSchema.getRgtType())) {
				// if (tLLPrepaidClaimDetailSchema.getMoney() > tPrepaidBala) {
				// buildError("updatePrepaidDetail",
				// "本次回收金额不能大于预付赔款余额");
				// return false;
				// }
				tLLPrepaidClaimDetailSchema.setMoney(Arith.round(-1
						* tLLPrepaidClaimDetailSchema.getMoney(), 2));
			}

			tLLPrepaidClaimDetailSchema.setPrepaidNo(mLLPrepaidClaimSchema
					.getPrepaidNo());
			tLLPrepaidClaimDetailSchema
					.setGrpContNo(tLCGrpPolDB.getGrpContNo());
			tLLPrepaidClaimDetailSchema.setRiskCode(tLCGrpPolDB.getRiskCode());
			tLLPrepaidClaimDetailSchema
					.setManageCom(tLCGrpPolDB.getManageCom());
			tLLPrepaidClaimDetailSchema.setOperator(mOperator);
			tLLPrepaidClaimDetailSchema.setMakeDate(mCurrentDate);
			tLLPrepaidClaimDetailSchema.setMakeTime(mCurrentTime);
			tLLPrepaidClaimDetailSchema.setModifyDate(mCurrentDate);
			tLLPrepaidClaimDetailSchema.setModifyTime(mCurrentTime);

			aLLPrepaidClaimDetailSet.add(tLLPrepaidClaimDetailSchema);

			LLPrepaidTraceSchema tLLPrepaidTraceSchema = new LLPrepaidTraceSchema();

			String tSerialNo = PubFun1.CreateMaxNo("PREPAIDTRACENO",
					tLCGrpPolDB.getManageCom());
			tLLPrepaidTraceSchema.setSerialNo(tSerialNo);
			tLLPrepaidTraceSchema.setOtherNo(mLLPrepaidClaimSchema
					.getPrepaidNo());
			tLLPrepaidTraceSchema.setOtherNoType("Y");
			tLLPrepaidTraceSchema.setBalaDate(mCurrentDate);
			tLLPrepaidTraceSchema.setBalaTime(mCurrentTime);

			if ("1".equals(mLLPrepaidClaimSchema.getRgtType())) {
				tLLPrepaidTraceSchema.setMoneyType("SQ");
			} else if ("2".equals(mLLPrepaidClaimSchema.getRgtType())) {
				tLLPrepaidTraceSchema.setMoneyType("HS");
			}
			tLLPrepaidTraceSchema.setState("0");
			Reflections tReflections = new Reflections();
			tReflections.transFields(tLLPrepaidTraceSchema,
					tLLPrepaidClaimDetailSchema);

			mLLPrepaidTraceSet.add(tLLPrepaidTraceSchema);

			mSumMoney += tLLPrepaidClaimDetailSchema.getMoney();
		}
		return true;
	}

	/**
	 * 撤销预付赔款
	 * 
	 * @return
	 */
	private boolean prepaidCancel() {
		mLLPrepaidClaimSchema.setRgtState("07");
		mLLPrepaidClaimSchema.setCanceler(mOperator);
		mLLPrepaidClaimSchema.setCancelDate(mCurrentDate);
		mLLPrepaidClaimSchema.setOperator(mOperator);
		mLLPrepaidClaimSchema.setModifyDate(mCurrentDate);
		mLLPrepaidClaimSchema.setModifyTime(mCurrentTime);

		mMap.put(mLLPrepaidClaimSchema, "UPDATE");
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	/**
	 * 追加错误信息
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLPrepaidClaimInputBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {

	}
}
