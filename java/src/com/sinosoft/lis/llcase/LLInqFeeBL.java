/*
 * <p>Title: PICCH业务系统</p>
 * <p>ClassName:LLInqFeeBL </p>
 * <p>Description: 直接调查费录入 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author：Xx
 * @version：1.0
 * @CreateDate：2005-02-20
 */

package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LLInqFeeBL {
    /**错误处理类*/
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /**向页面返回的数据*/
    private VData mResult = new VData();
    /**向后台递交的数据*/
    private MMap map = new MMap();
    /**业务类型操作符*/
    private String mOperate = "";
    /**全局数据*/
    private GlobalInput mG = new GlobalInput();
    /**查勘费用明细*/
    private LLInqFeeSet mLLInqFeeSet = new LLInqFeeSet();
    private String mOtherNo = "";
    private String mSurveyNo = "";
    private String adate = "";
    private String atime = "";

    public LLInqFeeBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //用于调试
    public static void main(String[] args) {
        LLInqFeeBL tLLInqFeeBL = new LLInqFeeBL();
        LLInqFeeSchema tLLInqFeeSchema = new LLInqFeeSchema();
        TransferData aTransferData = new TransferData();
        aTransferData.setNameAndValue("SurveyNo", "C11000606010001091");
        LLInqFeeSet tLLInqFeeSet = new LLInqFeeSet();
        tLLInqFeeSchema.setFeeItem("01");
        tLLInqFeeSchema.setSurveyNo("C11000606010001091");
        tLLInqFeeSchema.setFeeSum("17.2");
        tLLInqFeeSchema.setContSN("1");
        tLLInqFeeSchema.setInqDept("8611");
        tLLInqFeeSet.add(tLLInqFeeSchema);
        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "cm1101";
        tGI.ManageCom = "8611";
        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tLLInqFeeSet);
        tVData.add(aTransferData);
        tLLInqFeeBL.submitData(tVData, "FI||CONFIRM");
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData 输入数据
     * @param cOperate String 操作类型
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mOperate = cOperate;

        //得到输入数据
        if (!getInputData(cInputData)) {
            return false;
        }

        //检查数据合法性
        if (mOperate.equals("INSERT")) {
            if (!checkInputData()) {
                return false;
            }
            //进行业务处理
            if (!dealData()) {
                return false;
            }
        } else if (mOperate.equals("FI||CONFIRM")) {
            if (!FiConf()) {
                return false;
            }
        } else {
            if (!InConf()) {
                return false;
            }
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate)) {
            CError.buildErr(this, "数据提交失败！");
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有数据对象
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("getInputData()..." + mOperate);

        mG = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        TransferData tTD = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mOtherNo = "" + (String) tTD.getValueByName("OtherNo");
        mSurveyNo = mOtherNo + (String) tTD.getValueByName("SurveyNo");
        mLLInqFeeSet = (LLInqFeeSet) cInputData.getObjectByObjectName(
                "LLInqFeeSet", 0);
        adate = PubFun.getCurrentDate();
        atime = PubFun.getCurrentTime();
        return true;
    }

    /**
     * 对输入数据做必要的校验
     * @return boolean
     */
    private boolean checkInputData() {
        System.out.println("checkInputData()..." + mOperate);
        if (mSurveyNo.equals("") || mSurveyNo.equals("null")) {
            CError.buildErr(this, "案件号不能为空！");
            return false;
        }
        String sql = "select distinct b.surveyno from llinqapply b "
        			+" where b.surveyno='" + mSurveyNo+"'"
                    +" and (b.inqper='" + mG.Operator + "' or b.conper='"+mG.Operator+"')";
        ExeSQL texesql = new ExeSQL();
        SSRS tsn = texesql.execSQL(sql);
        if (tsn.getMaxRow() <= 0) {
            CError.buildErr(this, "您没有做与本案件有关的调查工作，无权限修改直接调查费用！");
            return false;
        }

        String tSQL = "select * from llinqfee where uwstate in ('1','4') and surveyno='" +
                      mSurveyNo + "'";
        System.out.println(tSQL);
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tSSRS.getMaxRow() > 0) {
            CError.buildErr(this, "该调查费用业务审核完毕，不能修改！");
            return false;
        }

        LLInqFeeSet delLLInqFeeSet = new LLInqFeeSet();
        LLInqFeeStaSet delLLInqFeeStaSet = new LLInqFeeStaSet();
        LLInqFeeSet tempLLInqFeeSet = mLLInqFeeSet;
        mLLInqFeeSet = new LLInqFeeSet();
        for (int j = 1; j <= tsn.getMaxRow(); j++) {
            String tsurveyno = tsn.GetText(j, 1);
            LLInqFeeDB tLLInqFeeDB = new LLInqFeeDB();
            tLLInqFeeDB.setSurveyNo(tsurveyno);
            LLInqFeeSet tLLInqFeeSet = tLLInqFeeDB.query();
            delLLInqFeeSet.add(tLLInqFeeSet);
            LLInqFeeStaDB tLLInqFeeStaDB = new LLInqFeeStaDB();
            tLLInqFeeStaDB.setSurveyNo(tsurveyno);
            LLInqFeeStaSet tLLInqFeeStaSet = tLLInqFeeStaDB.query();
            delLLInqFeeStaSet.add(tLLInqFeeStaSet);
            for (int i = 1; i <= tempLLInqFeeSet.size(); i++) {
                LLInqFeeSchema tLLInqFeeSchema = tempLLInqFeeSet.get(i);
                String sSurveyNo = tLLInqFeeSchema.getSurveyNo();
                if (sSurveyNo.equals(tsurveyno)) {
                    mLLInqFeeSet.add(tLLInqFeeSchema);
                }
            }
        }
        map.put(delLLInqFeeSet, "DELETE");
        map.put(delLLInqFeeStaSet, "DELETE");
        if (delLLInqFeeSet.size() > 0) {
            adate = delLLInqFeeSet.get(1).getMakeDate();
            atime = delLLInqFeeSet.get(1).getMakeTime();
        }
        if (mLLInqFeeSet.size() <= 0 && delLLInqFeeSet.size() <= 0) {
            CError.buildErr(this, "请录入直接调查费信息！");
            return false;
        }
        return true;
    }

    /**
     * 直接调查费录入保存逻辑
     * @return boolean
     */
    private boolean dealData() {
        System.out.println("dealData()..." + mOperate);
        String ndate = PubFun.getCurrentDate();
        String ntime = PubFun.getCurrentTime();
        LLInqFeeSchema tLLInqFeeSchema = null;
        String oNoType = "1";
        if (mLLInqFeeSet.size() > 0) {
            oNoType = getNoType(mLLInqFeeSet.get(1).getOtherNo());
        }
        //LLInqFee直接调查费按项目存
        for (int i = 1; i <= mLLInqFeeSet.size(); i++) {
            tLLInqFeeSchema = mLLInqFeeSet.get(i);
            tLLInqFeeSchema.setOtherNoType(oNoType);
            tLLInqFeeSchema.setUWState("0");
            tLLInqFeeSchema.setInqDept(mG.ManageCom);
            tLLInqFeeSchema.setOperator(mG.Operator);
            tLLInqFeeSchema.setMakeDate(adate);
            tLLInqFeeSchema.setMakeTime(atime);
            tLLInqFeeSchema.setModifyDate(ndate);
            tLLInqFeeSchema.setModifyTime(ntime);
            map.put(tLLInqFeeSchema, "INSERT");
        }
        LLSurveyDB tLLSurveyDB = new LLSurveyDB();
        tLLSurveyDB.setOtherNo(mOtherNo);
        LLSurveySet tLLSurveySet = tLLSurveyDB.query();
        int SurveyCount = tLLSurveySet.size();
        LLInqFeeStaSet tLLInqFeeStaSet = new LLInqFeeStaSet();
        LLInqFeeStaSchema tLLInqFeeStaSchema = null;
        for (int j = 1; j <= SurveyCount; j++) {
            tLLInqFeeStaSchema = new LLInqFeeStaSchema();
            double FeeSum = 0;
            String tSurvNo = tLLSurveySet.get(j).getSurveyNo();
            for (int k = 1; k <= mLLInqFeeSet.size(); k++) {
                if (mLLInqFeeSet.get(k).getSurveyNo().equals(tSurvNo)) {
                    FeeSum += mLLInqFeeSet.get(k).getFeeSum();
                }
            }
            LLInqFeeStaDB tLLInqFeeStaDB = new LLInqFeeStaDB();
            tLLInqFeeStaDB.setSurveyNo(tSurvNo);
            tLLInqFeeStaDB.setInqDept(mG.ManageCom);
            tLLInqFeeStaDB.setContSN("1");
            if (!tLLInqFeeStaDB.getInfo()) {
                if (FeeSum == 0) {
                    continue;
                } else {
                    tLLInqFeeStaSchema.setSurveyNo(tSurvNo);
                    tLLInqFeeStaSchema.setOtherNo(mOtherNo);
                    tLLInqFeeStaSchema.setOtherNoType(oNoType);
                    tLLInqFeeStaSchema.setContSN("1");
                    tLLInqFeeStaSchema.setInqDept(mG.ManageCom);
                    tLLInqFeeStaSchema.setInputer(mG.Operator);
                    tLLInqFeeStaSchema.setConfer("");
                    tLLInqFeeStaSchema.setConfDate("");
                    tLLInqFeeStaSchema.setInputDate(adate);
                    tLLInqFeeStaSchema.setOperator(mG.Operator);
                    tLLInqFeeStaSchema.setMakeDate(adate);
                    tLLInqFeeStaSchema.setMakeTime(atime);
                    tLLInqFeeStaSchema.setModifyDate(adate);
                    tLLInqFeeStaSchema.setModifyTime(atime);
                }
            } else {
                if (FeeSum == 0 && tLLInqFeeStaDB.getIndirectFee() <= 0) {
                    continue;
                } else {
                    tLLInqFeeStaSchema = tLLInqFeeStaDB.getSchema();
                    tLLInqFeeStaSchema.setOperator(mG.Operator);
                    tLLInqFeeStaSchema.setConfer("");
                    tLLInqFeeStaSchema.setConfDate("");
                    tLLInqFeeStaSchema.setModifyDate(adate);
                    tLLInqFeeStaSchema.setModifyTime(atime);
                }
            }
            FeeSum = Arith.round(FeeSum, 2);
            tLLInqFeeStaSchema.setInqFee(FeeSum);
            double totalSurveyFee = 0;
            totalSurveyFee = tLLInqFeeStaDB.getIndirectFee() + FeeSum;
            totalSurveyFee = Arith.round(totalSurveyFee, 2);
            tLLInqFeeStaSchema.setSurveyFee(totalSurveyFee);

            tLLInqFeeStaSet.add(tLLInqFeeStaSchema);
        }
        map.put(tLLInqFeeStaSet, "INSERT");
        return true;
    }

    /**
     * 业务审核逻辑
     * @return boolean
     */
    private boolean InConf() {  
    	 String tSQL = "select * from llinqfee where uwstate ='1' and surveyno='" +
         mSurveyNo + "'";
    	 System.out.println(tSQL);
		 ExeSQL tExeSQL = new ExeSQL();
		 SSRS tSSRS = new SSRS();
		 tSSRS = tExeSQL.execSQL(tSQL);
		 if (tSSRS.getMaxRow() > 0) {
			 CError.buildErr(this, "该调查费用业务审核完毕，不能修改！");
			 return false;
		 }
        LLInqFeeDB tLLInqFeeDB = new LLInqFeeDB();
        tLLInqFeeDB.setSurveyNo(mSurveyNo);
        LLInqFeeSet tLLInqFeeSet = tLLInqFeeDB.query();
        if (tLLInqFeeSet.size() <= 0) {
            CError.buildErr(this, "请先合计录入的直接查勘费再进行审核！");
            return false;
        }
        LLInqFeeStaDB tLLInqFeeStaDB = new LLInqFeeStaDB();
        tLLInqFeeStaDB.setSurveyNo(mSurveyNo);
        LLInqFeeStaSet tLLInqFeeStaSet = tLLInqFeeStaDB.query();
        if (tLLInqFeeStaSet.size() <= 0) {
            CError.buildErr(this, "查勘费用汇总数据有误！");
            return false;
        }

        LLInqFeeStaSchema tLLInqFeeStaSchema = tLLInqFeeStaSet.get(1);
        String payed = "" + tLLInqFeeStaSchema.getPayed();
        if (payed.equals("1")) {
            CError.buildErr(this, "该笔查勘费用已经生成了财务数据，不能再做审定操作");
            return false;
        }

        if (!mOtherNo.substring(0, 1).equals("T") &&
            !mOtherNo.substring(0, 1).equals("Z")) {
            String tCaseSQL =
                    "SELECT * FROM llcase WHERE rgtstate IN ('11','12','14') "
                    + " AND caseno='" + mOtherNo + "'";
            ExeSQL tCaseExeSQL = new ExeSQL();
            SSRS tCaseSSRS = new SSRS();
            tCaseSSRS = tCaseExeSQL.execSQL(tCaseSQL);
            if (tCaseSSRS.getMaxRow() <= 0) {
                CError.buildErr(this, "该案件还未给付确认，不能审核！");
                return false;
            }
        }

        LLSurveyDB tLLSurveyDB = new LLSurveyDB();
        tLLSurveyDB.setSurveyNo(tLLInqFeeSet.get(1).getSurveyNo());
        LLSurveySet tLLSurveySet = tLLSurveyDB.query();
        if (tLLSurveySet.size() < 0) {
            CError.buildErr(this, "调查信息查询失败！");
            return false;
        }

        LLInqApplyDB tLLInqApplyDB = new LLInqApplyDB();
        LLInqApplySet tLLInqApplySet = new LLInqApplySet();
        tLLInqApplyDB.setSurveyNo(tLLInqFeeSet.get(1).getSurveyNo());
        tLLInqApplySet = tLLInqApplyDB.query();
        if (tLLInqApplySet.size() < 0) {
            CError.buildErr(this, "调查分配信息查询失败！");
            return false;
        }

        //审核主管
        String sConfer = "" + tLLSurveySet.get(1).getConfer();
        //调查主管
        String tDispatcher = tLLInqApplySet.get(1).getDipatcher();
        if (!sConfer.equals(mG.Operator) && !tDispatcher.equals(mG.Operator)) {
            CError.buildErr(this, "对不起，您没有权限审核直接调查费用!");
            return false;
        }
        //本地调查
        if (sConfer.equals(tDispatcher)) {
            for (int i = 1; i <= tLLInqFeeSet.size(); i++) {
                tLLInqFeeSet.get(i).setConfer(mG.Operator);
                tLLInqFeeSet.get(i).setUWState("1");
                tLLInqFeeSet.get(i).setModifyDate(PubFun.getCurrentDate());
                tLLInqFeeSet.get(i).setModifyTime(PubFun.getCurrentTime());
            }
            map.put(tLLInqFeeSet, "UPDATE");
            String updateSql = "update llinqfeesta set confer='"
                               + mG.Operator + "',confdate='"
                               + PubFun.getCurrentDate() +
                               "', modifydate = '"
                               + PubFun.getCurrentDate() +
                               "', modifytime = '" +
                               PubFun.getCurrentTime()
                               + "' where surveyno ='"
                               + mSurveyNo + "'";
            map.put(updateSql, "UPDATE");
        } else {
            //异地调查
            //审核主管通过审核
            if (sConfer.equals(mG.Operator)) {
                for (int i = 1; i <= tLLInqFeeSet.size(); i++) {
                    if (!tLLInqFeeSet.get(i).getUWState().equals("4") &&
                        !tLLInqFeeSet.get(i).getUWState().equals("1")) {
                        CError.buildErr(this, "对不起，调查主管还未审核！");
                        return false;
                    }
                }
                for (int i = 1; i <= tLLInqFeeSet.size(); i++) {
                    tLLInqFeeSet.get(i).setConfer(mG.Operator);
                    tLLInqFeeSet.get(i).setUWState("1");
                    tLLInqFeeSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    tLLInqFeeSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
                map.put(tLLInqFeeSet, "UPDATE");
                String updateSql = "update llinqfeesta set confer='"
                                   + mG.Operator + "',confdate='"
                                   + PubFun.getCurrentDate() +
                                   "', modifydate = '"
                                   + PubFun.getCurrentDate() +
                                   "', modifytime = '" +
                                   PubFun.getCurrentTime()
                                   + "' where surveyno ='"
                                   + mSurveyNo + "'";
                map.put(updateSql, "UPDATE");
            } else {
                //调查主管审核
                for (int i = 1; i <= tLLInqFeeSet.size(); i++) {
                    tLLInqFeeSet.get(i).setConfer(mG.Operator);
                    tLLInqFeeSet.get(i).setUWState("4");
                    tLLInqFeeSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    tLLInqFeeSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
                map.put(tLLInqFeeSet, "UPDATE");
                String updateSql = "update llinqfeesta set confer='"
                                   + mG.Operator + "', modifydate = '"
                                   + PubFun.getCurrentDate() +
                                   "', modifytime = '" +
                                   PubFun.getCurrentTime()
                                   + "' where surveyno ='"
                                   + mSurveyNo + "'";
                map.put(updateSql, "UPDATE");
            }
        }
        return true;
    }

    /**
     * 财务审核逻辑
     * @return boolean
     */
    private boolean FiConf() {
        String ndate = PubFun.getCurrentDate();
        String ntime = PubFun.getCurrentTime();
        LLInqFeeDB tLLInqFeeDB = new LLInqFeeDB();
        tLLInqFeeDB.setSurveyNo(mSurveyNo);
        LLInqFeeSet tLLInqFeeSet = tLLInqFeeDB.query();
        double feesum = 0;
        for (int i = 1; i <= tLLInqFeeSet.size(); i++) {
            LLInqFeeSchema tLLInqFeeSchema = tLLInqFeeSet.get(i);
            tLLInqFeeSchema.setFinInspector(mG.Operator);
            tLLInqFeeSchema.setModifyDate(ndate);
            tLLInqFeeSchema.setModifyTime(ntime);
            tLLInqFeeSchema.setUWState("3");
            for (int j = 1; j <= mLLInqFeeSet.size(); j++) {
                String titemcode = mLLInqFeeSet.get(j).getFeeItem();
                if (titemcode.equals(tLLInqFeeSchema.getFeeItem())) {
                    tLLInqFeeSchema.setUWState("2");
                    feesum += tLLInqFeeSchema.getFeeSum();
                    break;
                }
            }
        }
        map.put(tLLInqFeeSet, "UPDATE");
        LLInqFeeStaDB tLLInqFeeStaDB = new LLInqFeeStaDB();
        tLLInqFeeStaDB.setSurveyNo(mSurveyNo);
        LLInqFeeStaSet tLLInqFeeStaSet = tLLInqFeeStaDB.query();
        if (tLLInqFeeStaSet.size() <= 0) {
            CError.buildErr(this, "查勘费用汇总数据有误！");
            return false;
        }
        LLInqFeeStaSchema tLLInqFeeStaSchema = tLLInqFeeStaSet.get(1);
        String payed = "" + tLLInqFeeStaSchema.getPayed();
        if (payed.equals("1")) {
            CError.buildErr(this, "该笔查勘费用已经生成了财务数据，不能再做审定操作");
            return false;
        }
        tLLInqFeeStaSchema.setInqFee(feesum);
        double total = feesum + tLLInqFeeStaSchema.getIndirectFee();
        tLLInqFeeStaSchema.setSurveyFee(total);
        map.put(tLLInqFeeStaSchema, "UPDATE");
        return true;
    }

    /**
     * 根据案件号判断案件号类型
     * 咨询/通知类案件返回0
     * 申请类案件返回1
     * 申诉/纠错类案件返回2
     * @param OtherNo String
     * @return String
     */
    public String getNoType(String OtherNo) {
        if ("Z".equals(OtherNo.substring(0, 1)) ||
            "T".equals(OtherNo.substring(0, 1))) {
            return "0";
        } else if ("C".equals(OtherNo.substring(0, 1))) {
            return "1";
        } else {
            return "2";
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        System.out.println("prepareOutputData()...");
        try {
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错。");
            return false;
        }
        return true;
    }

    /**
     * 得到处理后的结果数据
     * @return boolean
     */
    public VData getResult() {
        return this.mResult;
    }

    private void jbInit() throws Exception {
    }

}
