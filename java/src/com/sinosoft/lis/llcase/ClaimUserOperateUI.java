package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:理赔岗位权限操作类
 * <p>Company: Sinosoft</p>
 * @author LiuYansong
 * @version 1.0
 * @date :2004-10-26
 */
public class ClaimUserOperateUI
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData = new VData();
  private VData mResult = new VData();
  private String mOperate;

  public ClaimUserOperateUI() {}
  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    ClaimUserOperateBL tClaimUserOperateBL = new ClaimUserOperateBL();
    if (tClaimUserOperateBL.submitData(cInputData,mOperate) == false)
    {
  		// @@错误处理
      this.mErrors.copyAllErrors(tClaimUserOperateBL.mErrors);
      mResult.clear();
      return false;
    }
    else
			mResult = tClaimUserOperateBL.getResult();
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }
}