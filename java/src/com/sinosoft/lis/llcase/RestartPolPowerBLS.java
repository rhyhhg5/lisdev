package com.sinosoft.lis.llcase;
import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: 核心业务系统基本的保单效力恢复</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author 刘岩松
 * @date:2003-01-18
 * @version 1.0
 */
public class RestartPolPowerBLS
{
  private VData mInputData ;
  private LDSysTraceSet mLDSysTraceSet = new LDSysTraceSet();
  int n;
  public  CErrors mErrors = new CErrors();
  public String mOperate;
  public RestartPolPowerBLS() {}
  public static void main(String[] args)
  {
  }

  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData=(VData)cInputData.clone();
    String mOperate;
    mOperate=cOperate;
    mLDSysTraceSet.set((LDSysTraceSet)mInputData.getObjectByObjectName("LDSysTraceSet",0));
    n=mLDSysTraceSet.size();
    System.out.println("BLS中的n为"+n);
    for(int h=1;h<=n;h++)
    {
      LDSysTraceSchema aLDSysTraceSchema = new LDSysTraceSchema();
      aLDSysTraceSchema.setSchema(mLDSysTraceSet.get(h));
      System.out.println("bls中接收的数据中的保单号码是"+aLDSysTraceSchema.getPolNo());
    }
    if(n==0)
    {
      if(!dealSave())
         return false;
    }
   else
   {
     if (!this.saveData())
       return false;
   }
    mInputData=null;
    return true;
  }

  private boolean saveData()
  {
    System.out.println("开始执行save操作");
    Connection conn = DBConnPool.getConnection();
    if (conn==null)
    {
      CError tError = new CError();
      tError.moduleName = "ReportUpdateBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }//end of ifc(conn==null)
    try
    {
      conn.setAutoCommit(false);
      LDSysTraceDB tLDSysTraceDB = new LDSysTraceDB(conn);
      System.out.println("将要执行删除操作！！！！");
      for(int i=1;i<=n;i++)
      {
        LDSysTraceSchema mLDSysTraceSchema = new LDSysTraceSchema();
        mLDSysTraceSchema = mLDSysTraceSet.get(i);
        tLDSysTraceDB.setPolNo(mLDSysTraceSchema.getPolNo());
        System.out.println("bls中要删除的保单号码是"+mLDSysTraceSchema.getPolNo());
        if(!tLDSysTraceDB.deleteSQL())
        {
          this.mErrors.copyAllErrors(tLDSysTraceDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "RestartPolPowerBLS";
          tError.functionName = "DeleteData";
          tError.errorMessage = "删除失败!";
          this.mErrors.addOneError(tError);
          conn.rollback() ;
          conn.close();
          return false;
        }
      }

      LDSysTraceDBSet tLDSysTraceDBSet = new LDSysTraceDBSet(conn);
      tLDSysTraceDBSet.set(mLDSysTraceSet);
      if (tLDSysTraceDBSet.insert() == false)
      {
        System.out.println("开始执行save操作6");
        this.mErrors.copyAllErrors(tLDSysTraceDBSet.mErrors);
        CError tError = new CError();
        tError.moduleName = "RestartPolPowerBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "保存数据失败!";
        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
        return false;
      }//end of if 261 ;

      conn.commit() ;
      conn.close();
      System.out.println("save7");
    } // end of try240
    catch (Exception ex)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "RestartPolPowerBLS";
      tError.functionName = "submitData";
      tError.errorMessage = ex.toString();
      this.mErrors .addOneError(tError);
      try
      {
        conn.rollback() ;
        conn.close();
      }//end of try;285
      catch(Exception e)
      {
      }//end of catch;

      return false;
    }//end of catch;277
    System.out.println("执行结束！！");
    return true;
  }

  private boolean dealSave()
  {
    System.out.println("开始执行dealSave操作！！");
    LDSysTraceDBSet tLDSysTraceDBSet = new LDSysTraceDBSet();
    tLDSysTraceDBSet.set(mLDSysTraceSet);
    System.out.println("共要插入"+tLDSysTraceDBSet.size()+"条记录！！！");
    if (!tLDSysTraceDBSet.insert())
    {
      System.out.println("开始执行save操作6");
      this.mErrors.copyAllErrors(tLDSysTraceDBSet.mErrors);
      CError tError = new CError();
      tError.moduleName = "RestartPolPowerBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "保存数据失败!";
      this.mErrors .addOneError(tError) ;
      //        conn.rollback() ;
      //        conn.close();
      return false;
    }//end of if 26
    return true;
  }
}