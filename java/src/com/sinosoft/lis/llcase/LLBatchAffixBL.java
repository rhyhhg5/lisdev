package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: LLBatchAffixBL </p>
 * <p>Description: LLBatchAffixBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author:Xx
 * @CreateDate：2005-02-18 13:58:55
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LLBatchAffixBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LLAffixSet mLLAffixSet = new LLAffixSet();
    private LLCaseSet mLLCaseSet = new LLCaseSet();
    private String mRgtNo = "";
    private String mLoadFlag = "";

    public LLBatchAffixBL() {
    }

    public static void main(String[] args) {
        VData tVData = new VData();
        LLBatchAffixBL tBatchClaimCalBL = new LLBatchAffixBL();
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        tLLRegisterSchema.setRgtNo("P9400061121000001");
        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86940000";
        mGlobalInput.ComCode = "86940000";
        mGlobalInput.Operator = "cm9402";

        tVData.add(mGlobalInput);
        tVData.add(tLLRegisterSchema);
        tBatchClaimCalBL.submitData(tVData, "cal");
        tVData.clear();
        tVData = tBatchClaimCalBL.getResult();
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLBatchAffixBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LLBatchAffixBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("--Star GetInputData--");
        this.mLLAffixSet.set((LLAffixSet) cInputData.
                             getObjectByObjectName("LLAffixSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mRgtNo = mLLAffixSet.get(1).getRgtNo();
        TransferData tTransferData = (TransferData) cInputData.
                                     getObjectByObjectName("TransferData",
                0);
        mLoadFlag = (String) tTransferData.getValueByName("LoadFlag");

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {

        System.out.println("---Star dealData---mOperate---" + mOperate);
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setRgtNo(mRgtNo);
        tLLCaseDB.setRgtState("13");
        mLLCaseSet = tLLCaseDB.query();

        if (mLLCaseSet.size() <= 0) {
            CError.buildErr(this, "批次下无可选择材料案件");
            return false;
        }

        for (int i = 1; i <= mLLCaseSet.size(); i++) {
            if (!Affix(mLLCaseSet.get(i).getCaseNo())) {
                continue;
            }
        }
        return true;
    }


    /**
     * 自动理算，生成赔案信息表。
     * @return boolean
     */
    private boolean Affix(String aCaseNo) {
        System.out.println("案件:" + aCaseNo);
        LLAffixBL tLLAffixBL = new LLAffixBL();
        LLAffixSet tLLAffixSet = new LLAffixSet();
        for (int i = 1; i <= mLLAffixSet.size(); i++) {
            LLAffixSchema tLLAffixSchema = new LLAffixSchema();
            tLLAffixSchema.setSchema(mLLAffixSet.get(i));
            tLLAffixSchema.setCaseNo(aCaseNo);
            tLLAffixSet.add(tLLAffixSchema);
        }

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LoadFlag", mLoadFlag);

        VData aVData = new VData();
        aVData.addElement(tLLAffixSet);
        aVData.addElement(mGlobalInput);
        aVData.addElement(tTransferData);
        if (!tLLAffixBL.submitData(aVData, "INSERT||MAIN")) {
            CError.buildErr(this, "申请材料选择失败");
            return false;
        }
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLLAffixSet);
            mInputData.add(map);
            mResult.clear();
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDDrugBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
