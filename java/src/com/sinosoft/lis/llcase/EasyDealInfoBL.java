package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: EasyCaseInfoBL </p>
 * <p>Description: EasyCaseInfoBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author:MN
 * @CreateDate：2008-06-03 13:58:55
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class EasyDealInfoBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private FDate fDate = new FDate();

    /** 业务处理相关变量 */
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private LLCaseRelaSchema mLLCaseRelaSchema = new LLCaseRelaSchema();
    private LLToClaimDutySet mLLToClaimDutySet = new LLToClaimDutySet();


    private LLCasePolicySet mLLCasePolicySet = new LLCasePolicySet();
    private TransferData mTransferData = new TransferData();
    private String mCaseNo = "";



    public static void main(String[] args) {
        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86110000";
        mGlobalInput.ComCode = "86110000";
        mGlobalInput.Operator = "cm1101";

        VData tVData = new VData();
        EasyCaseInfoBL tEasyCaseInfoBL = new EasyCaseInfoBL();
        tVData.add(mGlobalInput);
        tEasyCaseInfoBL.submitData(tVData, "");
        tVData.clear();
        tVData = tEasyCaseInfoBL.getResult();
    }
  
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "EasyCaseInfoBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败EasyCaseInfoBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("Start EasyCaseInfoBL Submit...");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDDrugBL";
            tError.functionName = "EasyCaseInfoBL";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("End EasyCaseInfoBL Submit...");
        mInputData = null;

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("--Star GetInputData--");
        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData",0);
        mCaseNo = (String) mTransferData.getValueByName("CaseNo");



        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mCaseNo);
        if (!tLLCaseDB.getInfo()){
            CError.buildErr(this, "没有查询到理赔案件");
            return false;
        } else {
            mLLCaseSchema=tLLCaseDB.getSchema();
        }

        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mLLCaseSchema.getRgtNo());
        if (!tLLRegisterDB.getInfo()){
            CError.buildErr(this, "没有查询到受理信息");
            return false;
        } else {
            mLLRegisterSchema=tLLRegisterDB.getSchema();
        }


        LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
        tLLCaseRelaDB.setCaseNo(mCaseNo);
        //只有一个事件
        mLLCaseRelaSchema=tLLCaseRelaDB.query().get(1);
        if (mLLCaseRelaSchema==null){
            CError.buildErr(this, "没有查询到出险事件");
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("---Star dealData---mOperate---" + mOperate);

        String tSQL = "SELECT DISTINCT code FROM ldcode1 WHERE codetype='EasyCaseCon' AND codealias='10' WITH UR";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tSQL);
        String changeRgtType = "update llregister set applyertype='1' where rgtno='"+mLLRegisterSchema.getRgtNo()+"'";
        for (int j=1; j<=tSSRS.getMaxRow();j++){
            if (!fillCasePolicy(tSSRS.GetText(j,1))) {
                CError.buildErr(this, "没有符合条件的保单！");
                return false;
            }
            if (mLLToClaimDutySet.size() <= 0) {
                CError.buildErr(this, "没有符合条件的责任！");
                return false;
            }
            map.put(changeRgtType, "UPDATE");
            map.put(mLLToClaimDutySet, "DELETE&INSERT");
        }

        return true;
    }


    /**
     * 过滤要赔付的责任
     * @return boolean
     */
    private boolean getClaimDuty(LCPolSchema tpolschema) {
        String aCaseNo = mLLCaseSchema.getCaseNo();
        String aCaseRelaNo = mLLCaseRelaSchema.getCaseRelaNo();
        String aSubRptNo = mLLCaseRelaSchema.getSubRptNo();
        SynLCLBGetBL aSynLCLBGetBL = new SynLCLBGetBL();
        aSynLCLBGetBL.setPolNo(tpolschema.getPolNo());

        LCGetSet tLCGetSet = aSynLCLBGetBL.query();

        LDCode1DB tLDCode1DB = new LDCode1DB();
        tLDCode1DB.setCodeType("EasyCaseCon");
        tLDCode1DB.setCode(tpolschema.getRiskCode());
        tLDCode1DB.setCodeAlias("10");
        LDCode1Set tLDCode1Set = tLDCode1DB.query();
        for (int j = 1; j<=tLDCode1Set.size(); j++){
            LDCode1Schema tLDCode1Schema = tLDCode1Set.get(j);
            for (int i = 1; i <= tLCGetSet.size(); i++) {
                LLToClaimDutySchema row = new LLToClaimDutySchema();
                LCGetSchema lcGet = tLCGetSet.get(i);
                if (tLDCode1Schema.getCode1().equals(lcGet.getDutyCode())) {
                    row.setCaseNo(aCaseNo);
                    row.setCaseRelaNo(aCaseRelaNo);
                    row.setSubRptNo(aSubRptNo);
                    row.setPolNo(lcGet.getPolNo()); /*保单号*/
                    row.setGetDutyCode(lcGet.getGetDutyCode()); /* 给付责任编码 */
                    row.setDutyCode(lcGet.getDutyCode());
                    row.setGetDutyKind("200");

                    /* 给付责任类型 */
                    row.setGrpContNo(lcGet.getGrpContNo()); /* 集体合同号 */
                    row.setGrpPolNo(tpolschema.getGrpContNo()); /* 集体保单号 */
                    row.setContNo(lcGet.getContNo()); /* 个单合同号 */
                    row.setKindCode(tpolschema.getKindCode()); /* 险类代码 */
                    row.setRiskCode(tpolschema.getRiskCode()); /* 险种代码 */
                    row.setRiskVer(tpolschema.getRiskVersion()); /* 险种版本号 */
                    row.setPolMngCom(tpolschema.getManageCom()); /* 保单管理机构 */
                    row.setClaimCount(0); /* 理算次数 */
                    mLLToClaimDutySet.add(row);
                }
            }
        }
        return true;
    }

    /**
     * 案件保单信息
     * @return boolean
     */
    private boolean fillCasePolicy(String aRiskCode) {
        String aCaseNo = mLLCaseSchema.getCaseNo();
        String aCaseRelaNo = mLLCaseRelaSchema.getCaseRelaNo();
        String aRgtNo = mLLCaseSchema.getRgtNo();
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setInsuredNo(mLLCaseSchema.getCustomerNo());
        if ("0".equals(mLLRegisterSchema.getRgtObj())){
            tLCPolBL.setGrpContNo(mLLRegisterSchema.getRgtObjNo());
        }
        tLCPolBL.setAppFlag("1");
            tLCPolBL.setRiskCode(aRiskCode);
            LCPolSet tLCPolSet = new LCPolSet();
            tLCPolSet = tLCPolBL.query();
            int n = tLCPolSet.size();
            String tdate = PubFun.getCurrentDate();
            String ttime = PubFun.getCurrentTime();
            String strAccidentDate = mLLCaseSchema.getAccidentDate();

            try {
                for (int i = 1; i <= n; i++) {
                    //处理保单状态
                    String opolstate = "" + tLCPolSet.get(i).getPolState();
                    String npolstate = "";
                    if (opolstate.equals("null") || opolstate.equals("")) {
                        npolstate = "0400";
                    } else {
                        if (opolstate.substring(0, 2).equals("04")) {
                            npolstate = "0400" + opolstate.substring(4);
                        } else {
                            if (opolstate.length() < 4) {
                                npolstate = "0400" + opolstate;
                            } else {
                                npolstate = "0400" + opolstate.substring(0, 4);
                            }
                        }
                    }
                    tLCPolSet.get(i).setPolState(npolstate);
                    LLCasePolicySchema tLLCasePolicySchema = new
                            LLCasePolicySchema();
                    LCPolSchema tLCPolSchema = tLCPolSet.get(i);
                    if (!(strAccidentDate == null || strAccidentDate.equals(""))) {
                        if (!(CaseFunPub.checkDate(tLCPolSchema.getCValiDate(),
                                strAccidentDate))) {
                            this.mErrors.copyAllErrors(tLCPolBL.mErrors);
                            CError tError = new CError();
                            tError.moduleName = "EasyCaseInfoBL";
                            tError.functionName = "fillCasePolicy";
                            tError.errorMessage = "保单号码是" +
                                                  tLCPolSchema.getPolNo() +
                                                  "的出险日期" + strAccidentDate
                                                  + "在保单生效日期" +
                                                  tLCPolSchema.getCValiDate()
                                                  + "之前，不能参加理赔！！！";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                    }
                    tLLCasePolicySchema.setCaseNo(aCaseNo);
                    tLLCasePolicySchema.setRgtNo(aRgtNo);
                    tLLCasePolicySchema.setCaseRelaNo(aCaseRelaNo);
                    tLLCasePolicySchema.setOperator(mLLCaseSchema.getOperator());
                    tLLCasePolicySchema.setMngCom(mLLCaseSchema.getMngCom());
                    tLLCasePolicySchema.setMakeDate(tdate);
                    tLLCasePolicySchema.setModifyDate(tdate);
                    tLLCasePolicySchema.setMakeTime(ttime);
                    tLLCasePolicySchema.setModifyTime(ttime);

                    if ((tLCPolSchema.getInsuredNo().equals(mLLCaseSchema.
                            getCustomerNo()))) {
                        tLLCasePolicySchema.setCasePolType("0");
                    } else {
                        if (tLCPolSchema.getAppntNo().equals(mLLCaseSchema.
                                getCustomerNo())) {
                            tLLCasePolicySchema.setCasePolType("1");
                        }
                    }
                    tLLCasePolicySchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                    tLLCasePolicySchema.setContNo(tLCPolSchema.getContNo());
                    tLLCasePolicySchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                    tLLCasePolicySchema.setPolNo(tLCPolSchema.getPolNo());
                    tLLCasePolicySchema.setKindCode(tLCPolSchema.getKindCode());
                    tLLCasePolicySchema.setRiskCode(tLCPolSchema.getRiskCode());
                    tLLCasePolicySchema.setRiskVer(tLCPolSchema.getRiskVersion());
                    tLLCasePolicySchema.setPolMngCom(tLCPolSchema.getManageCom());
                    tLLCasePolicySchema.setSaleChnl(tLCPolSchema.getSaleChnl());
                    tLLCasePolicySchema.setAgentCode(tLCPolSchema.getAgentCode());
                    tLLCasePolicySchema.setAgentGroup(tLCPolSchema.
                            getAgentGroup());
                    tLLCasePolicySchema.setInsuredNo(tLCPolSchema.getInsuredNo());
                    tLLCasePolicySchema.setInsuredName(tLCPolSchema.
                            getInsuredName());
                    tLLCasePolicySchema.setInsuredSex(tLCPolSchema.
                            getInsuredSex());
                    tLLCasePolicySchema.setInsuredBirthday(tLCPolSchema.
                            getInsuredBirthday());
                    tLLCasePolicySchema.setAppntNo(tLCPolSchema.getAppntNo());
                    tLLCasePolicySchema.setAppntName(tLCPolSchema.getAppntName());
                    tLLCasePolicySchema.setCValiDate(tLCPolSchema.getCValiDate());

                    tLLCasePolicySchema.setPolType("1"); //正式保单
                    mLLCasePolicySet.add(tLLCasePolicySchema);
                    getClaimDuty(tLCPolSchema);
                }

                String delSql = "delete from llcasepolicy where caseno='" +
                                aCaseNo + "'";
                String delSQL_LLToClaimDuty="delete from LLToClaimDuty where caseno='"+aCaseNo + "'";
                map.put(delSQL_LLToClaimDuty,"DELETE");
                map.put(delSql, "DELETE");
                map.put(this.mLLCasePolicySet, "INSERT");
                map.put(tLCPolSet, "UPDATE");
                return true;
            } catch (Exception ex) {
                ex.printStackTrace();
                CError.buildErr(this, "准备数据出错:" + ex.getMessage());
                return false;
            }
    }

    /**
     * 查询功能
     * @return boolean
     */
    private boolean submitquery() {
        return false;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLLRegisterSchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLLCaseSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "genCaseInfoBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
