/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: 社保导入文件通用解析类，从一个XML数据流中解析出批次下分案信息</p>
 * <p>Copyright: Copyright (c) 2007 </p>
 * <p>Company: Sinosoft </p>
 * @author Xx
 * @version 6.0
 */

package com.sinosoft.lis.llcase;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import org.apache.xpath.XPathAPI;
import org.apache.xpath.objects.XObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.ExportExcelXML;

class SCParser implements SIParser {

    // 下面是一些常量定义
    private static String PATH_INSURED = "ROW";

    //案件
    private String mRgtNo = "";
    private String mGrpContNo = "";
    private String mPath = "";
    private String[] Title = null;
    private String vtsName = "ClaimErrList.vts";
    private ListTable FalseListTable = new ListTable();
    private GlobalInput mG = new GlobalInput();
    public CErrors mErrors = new CErrors();
    
    private String[] mErrorTitle = null;
    private String[] mErrorType = null;

    SCParser() {
    }

    public boolean setGlobalInput(GlobalInput aG) {
        mG = aG;
        FalseListTable.setName("FAULT");
        return true;
    }
    
    /**
     * 计算时间差,打印到控制台
     * @param inVal1 起始时间
     * 		  inVal2 结束时间
     * @return 
     * 
     */
    public void fromDateStringToLong(String inVal1,String inVal2) { 
    	//此方法计算时间毫秒
    	Date date1 = null; 
    	Date date2 = null;
    	//定义时间类型 
    	SimpleDateFormat inputFormat = new SimpleDateFormat("hh:mm:ss"); 
    	try { 
    		date1 = inputFormat.parse(inVal1); 
    		date2 = inputFormat.parse(inVal2); 
    	//将字符型转换成日期型
    	} catch (Exception e) {
    		e.printStackTrace(); 
    	} 
    	long ss=(date2.getTime()-date1.getTime())/1000; //共计秒数
        int MM = (int)ss/60; //共计分钟数
        int hh=(int)ss/3600; //共计小时数
        int dd=(int)hh/24; //共计天数
        System.out.println("执行时间："+dd+"天"+(hh-dd*24)+"小时"+(MM-hh*60)+"分"+(ss-MM*60)+"秒"); 
    }

    /**
     * 解析案件结点(公用函数)
     * @param node Node
     * @return VData
     */
    public VData parseOneInsuredNode(Node node) {
        VData tReturn = new VData();
        // 得到被保人信息
        NodeList nodeList = null;
        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_INSURED);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
            return null;
        }
        int nLength = nodeList == null ? 0 : nodeList.getLength();
        int FailureCount = 0;
        String tStart = PubFun.getCurrentTime();
        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeCase = nodeList.item(nIndex);
            if (!genCase(getValue(nodeCase))) {
                System.out.println("导入这个人失败");
                FailureCount++;
                //写导出错误xml的函数
            }
        }
        String tEnd = PubFun.getCurrentTime();
        fromDateStringToLong(tStart,tEnd);
        
        String tOtherSign = mRgtNo.substring(1,3);
        String tTitleSQL = "select codename,othersign from ldcode1 where codetype='llimportfeeitem' and code='86"+tOtherSign+"'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tTitleSSRS = tExeSQL.execSQL(tTitleSQL);
        
        if (tTitleSSRS.getMaxRow() > 0) {
        	mErrorTitle = new String[tTitleSSRS.getMaxRow()+1];
        	mErrorType = new String[tTitleSSRS.getMaxRow()+1];
        	
        	for (int i = 1;i<=tTitleSSRS.getMaxRow();i++) {
        		mErrorTitle[i-1] = tTitleSSRS.GetText(i, 1);
        		mErrorType[i-1] = tTitleSSRS.GetText(i, 2);
        	}
        	
        	mErrorTitle[tTitleSSRS.getMaxRow()] = "失败原因";
        	mErrorType[tTitleSSRS.getMaxRow()] = "String";
        	
        	LLExportErrorList xmlexport = new LLExportErrorList(); //新建一个XmlExport的实例
            xmlexport.createDocument(mRgtNo);
            xmlexport.addListTable(FalseListTable, mErrorTitle, mErrorType);
            xmlexport.outputDocumentToFile(mPath, mRgtNo); //输出xml文档到文件	
        }
        
        String FailureNum = FailureCount + "";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("FailureNum", FailureNum);
        tReturn.clear();
        tReturn.add(tTransferData);
        return tReturn;
    }

    public boolean setParam(String aRgtNo, String aGrpContNo, String aPath) {
        mRgtNo = aRgtNo;
        mGrpContNo = aGrpContNo;
        mPath = aPath;
        return true;
    }

    public void setFormTitle(String[] cTitle, String cvtsName) {
        Title = cTitle;
        vtsName = cvtsName;
    }

    /**
     * 利用XPathAPI取得某个节点的节点值
     * @param node Node
     * @param strPath String
     * @return String
     */
    private static String parseNode(Node node, String strPath) {
        String strValue = "";
        try {
            XObject xobj = XPathAPI.eval(node, strPath);
            strValue = xobj == null ? "" : xobj.toString();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return strValue;
    }

    private TransferData getValue(Node nodeCase) {
        TransferData td = new TransferData();
        for (int i = 0; i < Title.length; i++) {
            System.out.println(Title[i]);
            String tvalue = "" + parseNode(nodeCase, Title[i]);
            td.setNameAndValue(Title[i], tvalue);
        }
        return td;
    }

    private boolean genCase(TransferData cTD) {
        //确认客户信息
        String aSecurityNo = "" + (String) cTD.getValueByName("SecurityNo");
        String aCustomerNo = "" + (String) cTD.getValueByName("CustomerNo");
        String aIDNo = "" + (String) cTD.getValueByName("IDNo");
        String aCustomerName = "" + (String) cTD.getValueByName("CustomerName");
        String aSex = "" + (String) cTD.getValueByName("Sex");
        String aBirthday = FormatDate((String) cTD.getValueByName("Birthday"));
        String tSupInHosFee = FormatDate((String) cTD.getValueByName("SupInHosFee"));
        String tInsuredStat = "" + (String) cTD.getValueByName("InsuredStat");
        String tGetDutyKind = "" + (String) cTD.getValueByName("GetDutyKind");
        System.out.println(tSupInHosFee);
        String strSQL0 =
                "select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype ";
        String sqlpart1 = "from lcinsured a where ";
        String sqlpart2 = "from lbinsured a where ";
        String wherePart = "a.GrpContNo = '" + mGrpContNo + "' ";
        String msgPart = "";
        String ErrMessage = "";
        
    	//数字有效性校验
    	String tOtherSign = mRgtNo.substring(1,3);
        String tTitleSQL = "select codealias,othersign,codename from ldcode1 where codetype='llimportfeeitem' and code='86"+tOtherSign+"'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tTitleSSRS = tExeSQL.execSQL(tTitleSQL);
        
        if (tTitleSSRS.getMaxRow() > 0) {
        	String [] mcodealias = new String[tTitleSSRS.getMaxRow()+1];
        	String [] mothersign = new String[tTitleSSRS.getMaxRow()+1];
        	String [] mcodename = new String[tTitleSSRS.getMaxRow()+1];
        	for (int i = 1;i<=tTitleSSRS.getMaxRow();i++) {
        		mcodealias[i-1] = tTitleSSRS.GetText(i, 1);
        		mothersign[i-1] = tTitleSSRS.GetText(i, 2);
        		mcodename[i-1] = tTitleSSRS.GetText(i, 3);
        		if("Number".equals((mothersign[i-1]).trim())&&cTD.getValueByName(mcodealias[i-1])!=null&&!"".equals(cTD.getValueByName(mcodealias[i-1]))){
        			try{
        				Double.parseDouble((String) cTD.getValueByName(mcodealias[i-1]));
        			}catch (Exception ex) {
        	               System.out.println("导入模板中'"+mcodename[i-1]+"'项'"+(String) cTD.getValueByName(mcodealias[i-1])+"不能转换成数字！");
        	               ErrMessage = "导入模板中'"+mcodename[i-1]+"'项'"+(String) cTD.getValueByName(mcodealias[i-1])+"'不能转换成数字！";
        	               getFalseList(cTD, ErrMessage);
        	               return false;
        	           }
        		
        		}
        		
        	}
        	

        }
        

        
        if (!"".equals(aCustomerName) && !"null".equals(aCustomerName)) {
        	wherePart += " and a.name='" + aCustomerName + "' ";
            msgPart = "客户姓名为" + aCustomerName ;
		}
        
        if (!aCustomerNo.equals("null") && !aCustomerNo.equals("")) {
            wherePart += " and a.insuredno='" + aCustomerNo + "' ";
            msgPart += ",客户号为" + aCustomerNo;
        } else if (!aSecurityNo.equals("null") && !aSecurityNo.equals("")) {
            wherePart += " and a.othidno='" + aSecurityNo + "'";
            msgPart += ",社保号为" + aSecurityNo;
        } else if (!aIDNo.equals("null") && !aIDNo.equals("")) {
            wherePart += " and a.idno='" + aIDNo + "'";
            msgPart += ",身份证号为" + aIDNo;
        } else {
            ErrMessage = "未提供客户号，社保号或身份证号等信息，无法确认客户身份！";
            getFalseList(cTD, ErrMessage);
            return false;
        }

        String strSQL = strSQL0 + sqlpart1 + wherePart + " union " + strSQL0 +
                        sqlpart2 + wherePart;
        ExeSQL texesql = new ExeSQL();
        SSRS tssrs = new SSRS();
        tssrs = texesql.execSQL(strSQL);
        System.out.println(strSQL);
        if (tssrs == null || tssrs.getMaxRow() <= 0) {
            ErrMessage = "团单" + mGrpContNo + "下没有" + msgPart + "的被保险人";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        aCustomerNo = tssrs.GetText(1, 1);
        aIDNo = tssrs.GetText(1, 5);
        aSecurityNo = tssrs.GetText(1, 6);
        if (aCustomerName.equals("") || aCustomerName.equals("null")) {
            aCustomerName = tssrs.GetText(1, 2);
        } else if (!tssrs.GetText(1, 2).equals(aCustomerName)) {
            ErrMessage = msgPart + "的客户投保时姓名为" + tssrs.GetText(1, 2)
                         + ",与导入姓名" + aCustomerName + "不符！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        String tSex = tssrs.GetText(1, 3).equals("null") ? "" :
                      tssrs.GetText(1, 3);
        if (aSex.equals("") || aSex.equals("null")) {
            aSex = tSex;
        } else if (!tSex.equals("") && !tSex.equals(aSex)) {
            ErrMessage = msgPart + "的客户投保时性别为" + tSex
                         + ",与本次导入性别" + aSex + "不符！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        String tBirthday = tssrs.GetText(1, 4).equals("null") ? "" :
                           tssrs.GetText(1, 4);
        if (aBirthday.equals("") || aBirthday.equals("null")) {
            aBirthday = tBirthday;
        } else if (!tBirthday.equals("") && !tBirthday.equals(aBirthday)) {
            ErrMessage = msgPart + "的客户投保时生日为" + tBirthday
                         + ",与本次导入生日" + aBirthday + "不符！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        /*   String tIdtype=tssrs.GetText(1, 8).equals("null") ? "" :
            tssrs.GetText(1, 8);//获得证件类型
       if(tIdtype.equals("0"))//是身份证时，对身份证号进行校验
        {
        	String tId=tssrs.GetText(1, 5).equals("null") ? "" :
                tssrs.GetText(1, 5);;//获得身份证号
            if(!PubFun.checkIDNo(tId))//校验数据库里存储的身份证号，by zzh 20100925
            {	
            	ErrMessage =msgPart+"的客户身份证号错误，请先做客户资料变更！";
            	getFalseList(cTD, ErrMessage);
            	return false;
            }
        }*/
          
        
        String insts = tssrs.GetText(1, 7).equals("null") ? "" :
                       tssrs.GetText(1, 7);
        if (tInsuredStat.equals("") || tInsuredStat.equals("null")) {
            tInsuredStat = insts;
        } else if (!insts.equals("") && !insts.equals(tInsuredStat)) {
            ErrMessage = msgPart + "的客户投保时为" + insts
                         + "状态,与本次导入" + tInsuredStat + "状态不符！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        String result = LLCaseCommon.checkPerson(aCustomerNo);
        if(!"".equals(result)){
            ErrMessage = msgPart + "的客户正在进行保全操作,工单号为"+result+"！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        String strCustomerSQL="select * from lpedorapp a,lpgrpedoritem b where a.edoracceptno in (select max(edorno) from lpdiskimport where  insuredno='"+aCustomerNo+"' and grpcontno ='"+mGrpContNo+"') and a.edorstate !='0' and a.edoracceptno = b.edoracceptno and b.edortype ='ZT'";
        ExeSQL customersql = new ExeSQL();
        SSRS customertssrs = new SSRS();
        customertssrs = texesql.execSQL(strCustomerSQL);
        if(customertssrs.getMaxRow()>0){
            ErrMessage = msgPart + "的客户正在进行保全减人操作！";
            getFalseList(cTD, ErrMessage);
            return false;
        }

        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
        LLSecurityReceiptSchema tLLSecurityReceiptSchema = new
                LLSecurityReceiptSchema();
        LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
        LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
        VData tVData = new VData();

        genCaseInfoBL tgenCaseInfoBL = new genCaseInfoBL();

        tLLCaseSchema.setRgtType("1");
        tLLRegisterSchema.setRgtNo(mRgtNo);
        tLLRegisterSchema.setRgtObjNo(mGrpContNo);
        

        String aFeeType = "" + cTD.getValueByName("FeeType");
        System.out.println("aFeeTypeaFeeTypeaFeeTypeaFeeTypeaFeeTypeaFeeTypeaFeeTypeaFeeTypeaFeeType="+aFeeType);
        LLAppClaimReasonSchema tLLAppClaimReasonSchema = new
                LLAppClaimReasonSchema();
        if (aFeeType.trim().equals("1")) {
            tLLAppClaimReasonSchema.setReasonCode("01");
            tLLAppClaimReasonSchema.setReason("门诊"); //申请原因
        } else if(aFeeType.trim().equals("2")){
            tLLAppClaimReasonSchema.setReasonCode("02"); //原因代码
            tLLAppClaimReasonSchema.setReason("住院"); //申请原因
        }else if(aFeeType.trim().equals("3")){
            tLLAppClaimReasonSchema.setReasonCode("03"); //原因代码
            tLLAppClaimReasonSchema.setReason("特种病"); //申请原因
        }else{
        	//#531_如果不是以上三种类型，则不存
            tLLAppClaimReasonSchema.setReasonCode("00"); //原因代码
            tLLAppClaimReasonSchema.setReason("无"); //申请原因
            /**ErrMessage = "帐单类型错误，请输入正确的帐单类型1，2或3";
            getFalseList(cTD, ErrMessage);
            return false;
        	*/

        }
        tLLAppClaimReasonSchema.setCustomerNo(aCustomerNo);
        tLLAppClaimReasonSchema.setReasonType("0");

        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setRgtNo(mRgtNo);
        tLLCaseDB.setCustomerNo(aCustomerNo);
        LLCaseSet temLLCaseSet = tLLCaseDB.query();
        if (temLLCaseSet.size() > 0) {
            tLLCaseSchema.setUWState("7");
        }
        
//      modify by zhangyige 2012-07-09
        String aPrePaidFlag = "";
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if(tLLRegisterDB.getInfo())
		{
			aPrePaidFlag = tLLRegisterDB.getSchema().getPrePaidFlag();
		}
        tLLCaseSchema.setPrePaidFlag(aPrePaidFlag);
        
        tLLCaseSchema.setCaseNo("");
        tLLCaseSchema.setRgtNo(mRgtNo);
        tLLCaseSchema.setCustomerName(aCustomerName);
        tLLCaseSchema.setCustomerNo(aCustomerNo);
        tLLCaseSchema.setIDType("0");
        tLLCaseSchema.setIDNo(aIDNo);
        tLLCaseSchema.setOtherIDType("5");
        tLLCaseSchema.setOtherIDNo(aSecurityNo);
        tLLCaseSchema.setPhone((String) cTD.getValueByName("GetDutyCode"));
System.out.println(tLLCaseSchema.getPhone());
        tLLCaseSchema.setCustBirthday(aBirthday);
        tLLCaseSchema.setCustomerSex(aSex);
        tLLCaseSchema.setGrpNo((String) cTD.getValueByName("GrpNo"));
        String aAccDate = FormatDate((String) cTD.getValueByName("AccDate"));
//        aAccDate = aAccDate.equals("") ? PubFun.getCurrentDate() : aAccDate;
        if(aAccDate==null || "".equals(aAccDate) ||"null".equals(aAccDate)){
            ErrMessage = msgPart + "的客户出险日期不能为空，或出险日期格式不正确应为'YYYY-MM-DD'或者'YYYYMMDD'！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        tLLCaseSchema.setAccidentDate(aAccDate);
        if(tLLCaseSchema.getAccidentDate()==null)//by zzh 0930
        {
        	ErrMessage = msgPart + "的客户出险日期格式不正确，应为'YYYY-MM-DD'或者'YYYYMMDD'！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        System.out.println(tLLCaseSchema.getAccidentDate());
        String tMobilePhone=""+(String) cTD.getValueByName("MobilePhone");
        tLLCaseSchema.setMobilePhone(tMobilePhone);
        tLLCaseSchema.setRgtState("03"); //案件状态
        tLLCaseSchema.setSurveyFlag("0");
        tLLCaseSchema.setAccdentDesc("磁盘导入");
        tLLCaseSchema.setSurveyFlag("0");

        String tPayModeSQL = "SELECT casegetmode FROM llregister WHERE rgtno='"+mRgtNo+"'";
        ExeSQL tPMExSQL = new ExeSQL();
        SSRS tPMSSRS = tPMExSQL.execSQL(tPayModeSQL);
        if (tPMSSRS.getMaxRow() > 0) {
           try {
               tLLCaseSchema.setCaseGetMode(tPMSSRS.GetText(1, 1));
           } catch (Exception ex) {
               System.out.println("插入给付方式失败！");
           }
        }
        String aBankCode=(String) cTD.getValueByName("BankCode");
        String aBankAccNo=(String) cTD.getValueByName("BankAccNo");
        String aAccName=(String) cTD.getValueByName("AccName");
        ExeSQL texesql1 = new ExeSQL();
        if(aBankCode==null || "".equals(aBankCode) ||"null".equals(aBankCode)||aBankAccNo==null || "".equals(aBankAccNo) ||"null".equals(aBankAccNo)||aAccName==null || "".equals(aAccName) ||"null".equals(aAccName)){
            String sql =
                "select bankcode,bankaccno,accname from lcinsured where insuredno='" +
                aCustomerNo + "' and grpcontno='" + mGrpContNo + "'" +
                " union " +
                "select bankcode,bankaccno,accname from lbinsured where insuredno='" +
                aCustomerNo + "' and grpcontno='" + mGrpContNo + "'";

            	
            	SSRS trr = texesql1.execSQL(sql);
            	if (trr.getMaxRow() > 0) {
            		try {
            			tLLCaseSchema.setBankCode(trr.GetText(1, 1));
            			tLLCaseSchema.setBankAccNo(trr.GetText(1, 2));
            			tLLCaseSchema.setAccName(trr.GetText(1, 3));
            		} catch (Exception ex) {
            			System.out.println("通过数据库查询后插入用户帐户信息失败！");
            		}
            	}
        
        }else{
        	try {
    			tLLCaseSchema.setBankCode(aBankCode);
    			tLLCaseSchema.setBankAccNo(aBankAccNo);
    			tLLCaseSchema.setAccName(aAccName);
    		} catch (Exception ex) {
    			System.out.println("通过导入模板插入用户帐户信息失败！");
    		}
        }

        String tAccSQL = "select 1 from lcgrppol a,lmriskapp b where a.riskcode=b.riskcode"
            + " and b.risktype3='7' and b.risktype <>'M'"
            + " and a.grpcontno='"+ mGrpContNo +"'";
        SSRS tAccSSRS = texesql1.execSQL(tAccSQL);
        boolean tDiseaseFlag = true;
        if (tAccSSRS.getMaxRow()>0) {
        	tDiseaseFlag = false;
        }
        
        String tHospitalCode = "" + (String) cTD.getValueByName("HospitalCode");
        String tHospitalName = "" + (String) cTD.getValueByName("HospitalName");
        
        if (("".equals(tHospitalCode)||"null".equals(tHospitalCode))&&tDiseaseFlag) {
            ErrMessage = "医院编码不能为空";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        if (("".equals(tHospitalName)||"null".equals(tHospitalName))&&tDiseaseFlag) {
            ErrMessage = "医院名称不能为空";
            getFalseList(cTD, ErrMessage);
            return false;
        }
      
    /**    String aAffixNo="" + (String) cTD.getValueByName("AffixNo");
        if(!(aAffixNo==null || "".equals(aAffixNo) ||"null".equals(aAffixNo))){
        	String ttGetDutyCode ="" + (String) cTD.getValueByName("GetDutyCode");
        	if(ttGetDutyCode==null || "".equals(ttGetDutyCode) ||"null".equals(ttGetDutyCode)){
                LCPolBL tLCPolBL = new LCPolBL();
                tLCPolBL.setInsuredNo(aCustomerNo);
                tLCPolBL.setGrpContNo(mGrpContNo);
                LCPolSet tLCPolSet = new LCPolSet();
                tLCPolSet = tLCPolBL.query();
                int n = tLCPolSet.size();
                for (int i = 1; i <= n; i++) {
                	LCPolSchema tLCPolSchema = tLCPolSet.get(i);
                    String rsql="select 1 from(select getdutykind a,count(getdutycode) b from lmdutygetclm where getdutycode in (select getdutycode from lcget where polno='"+tLCPolSchema.getPolNo()+"' and GrpContNo='"+mGrpContNo+"' union select getdutycode from lbget where polno='"+tLCPolSchema.getPolNo()+"' and GrpContNo='"+mGrpContNo+"') group by getdutykind )as aa where aa.b>1 with ur";
                    ExeSQL mExeSQL = new ExeSQL();
                    SSRS mTitleSSRS = mExeSQL.execSQL(rsql);
                    if (mTitleSSRS.getMaxRow() > 0) {
                        ErrMessage = msgPart + "的客户‘给付责任编码’或‘就诊类别’，‘医疗标记’错误导致金额重复！";
                        getFalseList(cTD, ErrMessage);
                        return false;
                    }
                }
        	}
        		
        }**/

        if(!(tGetDutyKind==null||"".equals(tGetDutyKind)||"null".equals(tGetDutyKind))){
        	tLLFeeMainSchema.setOldMainFeeNo(tGetDutyKind);
        }
        tLLFeeMainSchema.setRgtNo(mRgtNo);
        tLLFeeMainSchema.setCaseNo("");
        tLLFeeMainSchema.setCustomerNo(aCustomerNo);
        tLLFeeMainSchema.setCustomerName(aCustomerName);
        tLLFeeMainSchema.setCustomerSex(aSex);
        tLLFeeMainSchema.setInsuredStat(tInsuredStat);
        tLLFeeMainSchema.setHospitalCode(tHospitalCode);
        tLLFeeMainSchema.setHospitalName(tHospitalName);
        tLLFeeMainSchema.setFeeAtti("4");
        tLLFeeMainSchema.setSecurityNo(aSecurityNo);
        tLLFeeMainSchema = (LLFeeMainSchema) fillCommonField(tLLFeeMainSchema,
                cTD);
        if (tLLFeeMainSchema.getFeeDate()==null
            || "".equals(tLLFeeMainSchema.getFeeDate())
            ||"null".equals(tLLFeeMainSchema.getFeeDate())) {
            tLLFeeMainSchema.setFeeDate(aAccDate);
        }
        tLLFeeMainSchema.setHospStartDate(FormatDate((String) cTD.
                getValueByName("HospStartDate")));
        tLLFeeMainSchema.setHospEndDate(FormatDate((String) cTD.getValueByName(
                "HospEndDate")));
        tLLFeeMainSchema.setInHosNo(FormatDate((String) cTD.getValueByName(
        		        "InpatientNo")));
        //不合理费用
        String tRefuseAmnt=(String) cTD.getValueByName("RefuseAmnt");
        if(tRefuseAmnt!=null && !"".equals(tRefuseAmnt) && !"null".equals(tRefuseAmnt)){
        	tLLFeeMainSchema.setSumFee(Double.parseDouble(tRefuseAmnt));
        }
        
        //可报销范围内金额
        String tReimbursement=(String) cTD.getValueByName("Reimbursement");
        if(tReimbursement!=null && !"".equals(tReimbursement) && !"null".equals(tReimbursement)){
        	tLLFeeMainSchema.setReimbursement(Double.parseDouble(tReimbursement));
        }
        
        LCGrpContSchema kLCGrpContSchema = new LCGrpContSchema();
        LCGrpContDB kLCGrpContDB = new LCGrpContDB();
        kLCGrpContDB.setGrpContNo(mGrpContNo);
        kLCGrpContDB.getInfo();
        kLCGrpContSchema = kLCGrpContDB.getSchema();

        String tMngCom = kLCGrpContSchema.getManageCom();
        System.out.println(tMngCom.substring(0, 4));  
        String tApplyAmnt=(String) cTD.getValueByName("ApplyAmnt");
        if(tApplyAmnt==null || "".equals(tApplyAmnt) ||"null".equals(tApplyAmnt)){
            ErrMessage = msgPart + "的客户费用总额为空！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        tLLFeeMainSchema.setSumFee(Double.parseDouble((String) cTD.
                getValueByName("ApplyAmnt")));

//解析xml模版中定义社保项到LLSecurityReceipt，按照名称一一对应
        tLLSecurityReceiptSchema = (LLSecurityReceiptSchema)
                                   fillCommonField(tLLSecurityReceiptSchema,
                cTD);
        System.out.println(aFeeType);
        System.out.println(tLLSecurityReceiptSchema.getMidAmnt());

        System.out.println(tMngCom.substring(0, 4));
        if (aFeeType.trim().equals("1")) {
            tLLSecurityReceiptSchema.setHighDoorAmnt(tLLSecurityReceiptSchema.
                    getMidAmnt());
            tLLSecurityReceiptSchema.setMidAmnt(0);
            tLLSecurityReceiptSchema.setSmallDoorPay(tLLSecurityReceiptSchema.
                    getGetLimit());
            tLLSecurityReceiptSchema.setGetLimit(0);

        }

        tLLSecurityReceiptSchema.setLowAmnt(tLLSecurityReceiptSchema.
                                            getGetLimit());
        tLLSecurityReceiptSchema.setHighAmnt2(tLLSecurityReceiptSchema.
                                              getSupInHosFee());

        LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();

        System.out.println((String) cTD.getValueByName("DiseaseCode"));
        System.out.println((String) cTD.getValueByName("DiseaseName"));
        String tDiseaseCode = "" + (String) cTD.getValueByName("DiseaseCode");
        String tDiseaseName = "" + (String) cTD.getValueByName("DiseaseName");
        System.out.println(tDiseaseCode);
        System.out.println(tDiseaseName);
        if (("".equals(tDiseaseCode)||"null".equals(tDiseaseCode))&&tDiseaseFlag) {
            ErrMessage = "疾病编码不能为空";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        if (("".equals(tDiseaseName)||"null".equals(tDiseaseName))&&tDiseaseFlag) {
            ErrMessage = "疾病名称不能为空";
            getFalseList(cTD, ErrMessage);
            return false;
        }

        if (!"".equals(tDiseaseCode) && !"null".equals(tDiseaseCode)) {
            tLLCaseCureSchema.setDiseaseCode(tDiseaseCode);
            tLLCaseCureSchema.setDiseaseName(tDiseaseName);
            tLLCaseCureSchema.setMainDiseaseFlag("1");
            tLLCaseCureSet.add(tLLCaseCureSchema);
        }
        String tDiseaseCode2 = "" + (String) cTD.getValueByName("DiseaseCode2");
        String tDiseaseName2 = "" + (String) cTD.getValueByName("DiseaseName2");

        if (!tDiseaseCode2.equals("null") || !tDiseaseName2.equals("null")) {
            tLLCaseCureSchema = new LLCaseCureSchema();
            tLLCaseCureSchema.setDiseaseCode(tDiseaseCode2);
            tLLCaseCureSchema.setDiseaseName(tDiseaseName2);
            tLLCaseCureSet.add(tLLCaseSchema);
        }
        //保存案件时效
        tLLCaseOpTimeSchema.setRgtState("01");
        tLLCaseOpTimeSchema.setStartDate(PubFun.getCurrentDate());
        tLLCaseOpTimeSchema.setStartTime(PubFun.getCurrentTime());

        System.out.println("<--Star Submit VData-->");
        if(!"00".equals(tLLAppClaimReasonSchema.getReasonCode())){
        tVData.add(tLLAppClaimReasonSchema);	
        }
        tVData.add(tLLCaseSchema);
        tVData.add(tLLRegisterSchema);
        tVData.add(tLLFeeMainSchema);
        tVData.add(tLLSecurityReceiptSchema);
        tVData.add(tLLCaseCureSet);
        tVData.add(tLLCaseOpTimeSchema);
        tVData.add(mG);
        System.out.println("<--Into tSimpleClaimUI-->" + mG.ManageCom);
        if (!tgenCaseInfoBL.submitData(tVData, "INSERT||MAIN")) {
            CErrors tError = tgenCaseInfoBL.mErrors;
            ErrMessage = tError.getFirstError();
            getFalseList(cTD, ErrMessage);
            return false;
        }
        return true;
    }

    private Object fillCommonField(Object cSchema, TransferData cTD) {
        int n = cTD.getValueNames().size();
        for (int i = 0; i < n; i++) {
            String fieldName = (String) cTD.getNameByIndex(i);
            String fieldValue = (String) cTD.getValueByIndex(i);
            Class[] c = new Class[1];
            Method m = null;
            fieldName = "set" + fieldName;
            String[] tValue = {fieldValue};
            try {
                c[0] = Class.forName("java.lang.String");
            } catch (Exception ex) {
            }
            try {
                m = cSchema.getClass().getMethod(fieldName, c);
            } catch (Exception ex) {
            }
            try {
                m.invoke(cSchema, tValue);
            } catch (Exception ex) {
            }
        }
        return cSchema;
    }

    private boolean getFalseList(TransferData cTD, String ErrMessage) {
        int n = Title.length;
        String[] strCol = new String[n + 1];
        for (int i = 0; i < n; i++) {
            strCol[i] = (String) cTD.getValueByIndex(i);
        }
        strCol[n] = ErrMessage;
        System.out.println("ErrMessage:" + ErrMessage);
        FalseListTable.add(strCol);
        return true;
    }

    private String FormatDate(String aDate) {
        String rDate = "";
        rDate += aDate;
        if (rDate.equals("") || rDate.equals("null")) {
            return "";
        }
        if (aDate.length() < 8) {
            int days = 0;
            try {
                days = Integer.parseInt(aDate) - 2;
            } catch (Exception ex) {
                return "";
            }
            rDate = PubFun.calDate("1900-1-1", days, "D", null);
        }
        return rDate;
    }
}
