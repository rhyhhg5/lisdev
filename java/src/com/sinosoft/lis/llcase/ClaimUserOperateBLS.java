package com.sinosoft.lis.llcase;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统投保暂交费功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author YT
 * @version 1.0
 */

public class ClaimUserOperateBLS
{
  private   ClaimUserOperateBL mClaimUserOperateBL = new ClaimUserOperateBL();
  private LDCode1Set mLDCode1Set = new LDCode1Set();
  private VData mInputData ;
  private String mOperate;
  public  CErrors mErrors = new CErrors();
  public ClaimUserOperateBLS() {}
  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;
    this.mOperate=cOperate;
    if (!this.dealData())
      return false;
    return true;
  }
 private boolean dealData()
 {
   LLClaimUserSchema mLLClaimUserSchema = new LLClaimUserSchema();
   LDSpotUWRateSchema mLDSpotUWRateSchema = new LDSpotUWRateSchema();
   mLLClaimUserSchema.setSchema((LLClaimUserSchema)mInputData.getObjectByObjectName("LLClaimUserSchema",0));
   mLDSpotUWRateSchema.setSchema((LDSpotUWRateSchema)mInputData.getObjectByObjectName("LDSpotUWRateSchema",0));
   mLDCode1Set = (LDCode1Set)mInputData.getObjectByObjectName("LDCode1Set", 0);
   Connection conn = DBConnPool.getConnection();
   if (conn==null)
   {
     mClaimUserOperateBL.buildError("mClaimUserOperateBL","数据库连接失败！");
     return false;
   }
   try
   {
     conn.setAutoCommit(false);
     LLClaimUserDB mLLClaimUserDB = new LLClaimUserDB(conn);
     mLLClaimUserDB.setSchema(mLLClaimUserSchema);
     LDSpotUWRateDB mLDSpotUWRateDB = new LDSpotUWRateDB(conn);
     mLDSpotUWRateDB.setSchema(mLDSpotUWRateSchema);
     if(mOperate.equals("INSERT"))
     {
       if(!mLLClaimUserDB.insert())
       {
         this.mErrors.copyAllErrors(mLLClaimUserDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "ClaimUserOperateBLS";
         tError.functionName = "saveData";
         tError.errorMessage = "保存数据失败!";
         this.mErrors .addOneError(tError) ;
         conn.rollback() ;
         conn.close();
         return false;
       }
       if(!mLDSpotUWRateDB.insert())
       {
         this.mErrors.copyAllErrors(mLDSpotUWRateDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "ClaimUserOperateBLS";
         tError.functionName = "saveData";
         tError.errorMessage = "保存数据失败!";
         this.mErrors .addOneError(tError) ;
         conn.rollback() ;
         conn.close();
         return false;
       }
       for(int i = 1; i <=mLDCode1Set.size();i++){
    	   LDCode1DB tLDCode1DB = new LDCode1DB();
    	   LDCode1Schema tLDCode1Schema = new LDCode1Schema();
    	   tLDCode1Schema = mLDCode1Set.get(i);
    	   tLDCode1DB.setSchema(tLDCode1Schema);
    	   if(!tLDCode1DB.insert()){
    		    
    		     this.mErrors.copyAllErrors(tLDCode1DB.mErrors);
    	         CError tError = new CError();
    	         tError.moduleName = "ClaimUserOperateBLS";
    	         tError.functionName = "saveData";
    	         tError.errorMessage = "保存数据失败!";
    	         this.mErrors .addOneError(tError) ;
    	         conn.rollback() ;
    	         conn.close();
    	         return false; 
    	   
    	   }
    	   
       }
       conn.commit() ;
       conn.close();
     }
     if(mOperate.equals("UPDATE"))
     {
       if(!(mLLClaimUserDB.getStateFlag().equals("1")))
       {
       LLCaseDB tLLCaseDB = new  LLCaseDB();
       tLLCaseDB.setHandler(mLLClaimUserDB.getUserCode());
       LLCaseSet tLLCaseSet ;
       for(int i = 1;i<=8;i++)
       {
         tLLCaseSet = new LLCaseSet();
         if(i==5)
         {
           String strsql = "select count(*) from llcase a, LLClaimUWMain b where a.caseno=b.caseno "
               +" and a.rgtstate = '05' and b.AppClmUWer= '"+mLLClaimUserDB.getUserCode()+
               "'";

           ExeSQL exesql = new ExeSQL();
           SSRS ssrs = exesql.execSQL(strsql);
           if(!ssrs.GetText(1,1).equals("0"))
           {
             CError tError = new CError();
             tError.moduleName = "RegisterBLS";
             tError.functionName = "saveData";
             tError.errorMessage = "此用户有待审定的案件，不能更新用户信息为其他状态!";
             this.mErrors.addOneError(tError);
             System.out.println("此用户有待审定的案件，不能更新用户信息为其他状态!");
             return false;

           }
         }
         else
         {
           tLLCaseDB.setRgtState("0" + i);
           tLLCaseSet.set(tLLCaseDB.query());
           if (! (tLLCaseSet == null || tLLCaseSet.size() == 0))
           {
             CError tError = new CError();
             tError.moduleName = "RegisterBLS";
             tError.functionName = "saveData";
             tError.errorMessage = "此用户有未结案件，不能更新用户信息为其他状态!";
             this.mErrors.addOneError(tError);
             System.out.println("此用户有未结案件，不能更新用户信息为其他状态!");
             return false;
           }
         }
         //如果案件为抽检状态并且设定的抽检审定人为此用户的话不能离职
           String strsql1 = "select count(*) from llcase a, LLClaimUWMain b where a.caseno=b.caseno "
               +" and a.rgtstate = '10' and b.AppClmUWer= '"+mLLClaimUserDB.getUserCode()+
               "'";

           ExeSQL exesql1 = new ExeSQL();
           SSRS ssrs1 = exesql1.execSQL(strsql1);
           if(!ssrs1.GetText(1,1).equals("0"))
           {
             CError tError = new CError();
             tError.moduleName = "RegisterBLS";
             tError.functionName = "saveData";
             tError.errorMessage = "此用户有待抽检审定的案件，不能更新用户信息为其他状态!";
             this.mErrors.addOneError(tError);
             System.out.println("此用户有待抽检审定的案件，不能更新用户信息为其他状态!");
             return false;

           }

       }
       }
       if(!mLLClaimUserDB.update())
       {
         this.mErrors.copyAllErrors(mLLClaimUserDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "ClaimUserOperateBLS";
         tError.functionName = "UPDATE";
         tError.errorMessage = "修改数据失败!";
         this.mErrors .addOneError(tError) ;
         conn.rollback() ;
         conn.close();
         return false;
       }
       if(!mLDSpotUWRateDB.update())
       {
         this.mErrors.copyAllErrors(mLLClaimUserDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "ClaimUserOperateBLS";
         tError.functionName = "UPDATE";
         tError.errorMessage = "修改数据失败!";
         this.mErrors .addOneError(tError) ;
         conn.rollback() ;
         conn.close();
         return false;
       }
       String sql = "select code,code1,comcode,OtherSign,codename from ldcode1 where codetype='LLCaseAssign' and code='"+mLLClaimUserDB.getUserCode()+
       "' with ur";
       SSRS tSSRS = new SSRS();
       ExeSQL tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(sql);
       if(tSSRS.getMaxRow()>0){
       for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
    	   LDCode1DB tLDCode1DB = new LDCode1DB();
           LDCode1Schema tLDCode1Schema = new LDCode1Schema();
           tLDCode1Schema.setCodeType("LLCaseAssign");
           tLDCode1Schema.setCode(tSSRS.GetText(i, 1));
           tLDCode1Schema.setCode1(tSSRS.GetText(i, 2));
           tLDCode1Schema.setComCode(tSSRS.GetText(i, 3));
           tLDCode1Schema.setOtherSign(tSSRS.GetText(i, 4));
           tLDCode1Schema.setCodeName(tSSRS.GetText(i, 5));
           tLDCode1DB.setSchema(tLDCode1Schema);
           if(!tLDCode1DB.delete()){
   		    
  		     this.mErrors.copyAllErrors(tLDCode1DB.mErrors);
  	         CError tError = new CError();
  	         tError.moduleName = "ClaimUserOperateBLS";
  	         tError.functionName = "deleteData";
  	         tError.errorMessage = "删除数据失败!";
  	         this.mErrors .addOneError(tError) ;
  	         conn.rollback() ;
  	         conn.close();
  	         return false; 
  	        }
           
       }
       }
       if(mLDCode1Set!=null){
       for(int i = 1; i <=mLDCode1Set.size();i++){
    	   LDCode1DB tLDCode1DB = new LDCode1DB();
    	   LDCode1Schema tLDCode1Schema = new LDCode1Schema();
    	   tLDCode1Schema = mLDCode1Set.get(i);
    	   tLDCode1DB.setSchema(tLDCode1Schema);
    	   if(!tLDCode1DB.insert()){
    		    
    		     this.mErrors.copyAllErrors(tLDCode1DB.mErrors);
    	         CError tError = new CError();
    	         tError.moduleName = "ClaimUserOperateBLS";
    	         tError.functionName = "saveData";
    	         tError.errorMessage = "保存数据失败!";
    	         this.mErrors .addOneError(tError) ;
    	         conn.rollback() ;
    	         conn.close();
    	         return false; 
    	   }
    	   
       }
       }
       conn.commit() ;
       conn.close();
     }
     if(mOperate.equals("DELETE"))
     {
       if(!mLLClaimUserDB.delete())
       {
         this.mErrors.copyAllErrors(mLLClaimUserDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "RegisterBLS";
         tError.functionName = "saveData";
         tError.errorMessage = "保存数据失败!";
         this.mErrors .addOneError(tError) ;
         conn.rollback() ;
         conn.close();
         return false;
       }
       if(!mLDSpotUWRateDB.delete())
       {
         this.mErrors.copyAllErrors(mLLClaimUserDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "RegisterBLS";
         tError.functionName = "saveData";
         tError.errorMessage = "保存数据失败!";
         this.mErrors .addOneError(tError) ;
         conn.rollback() ;
         conn.close();
         return false;
       }
       if(mLDCode1Set!=null){
           for(int i = 1; i <=mLDCode1Set.size();i++){
        	   LDCode1DB tLDCode1DB = new LDCode1DB();
        	   LDCode1Schema tLDCode1Schema = new LDCode1Schema();
        	   tLDCode1Schema = mLDCode1Set.get(i);
        	   tLDCode1DB.setSchema(tLDCode1Schema);
        	   if(!tLDCode1DB.delete()){
        		    
        		     this.mErrors.copyAllErrors(tLDCode1DB.mErrors);
        	         CError tError = new CError();
        	         tError.moduleName = "ClaimUserOperateBLS";
        	         tError.functionName = "saveData";
        	         tError.errorMessage = "保存数据失败!";
        	         this.mErrors .addOneError(tError) ;
        	         conn.rollback() ;
        	         conn.close();
        	         return false; 
        	   }
        	   
           }
           }
       conn.commit() ;
       conn.close();
     }
   } // end of try
   catch (Exception ex)
   {
     // @@错误处理
     CError tError = new CError();
     tError.moduleName = "RegisterBLS";
     tError.functionName = "submitData";
     tError.errorMessage = ex.toString();
     this.mErrors .addOneError(tError);
     try
     {
       conn.rollback() ;
       conn.close();
     }
     catch(Exception e)
     {
     }

     return false;
   }
   return true;
 }
}
