/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: 社保导入文件通用解析类，以一个xml配置模板解析Excel</p>
 * <p>Copyright: Copyright (c) 2007 </p>
 * <p>Company: Sinosoft </p>
 * @author Houyd
 * @version 1.0
 */

package com.sinosoft.lis.llcase;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;

import org.w3c.dom.Node;

import com.sinosoft.lis.pubfun.diskimport.Sheet;
import com.sinosoft.lis.pubfun.diskimport.XlsImporter;
import com.sinosoft.lis.llcase.LLValidityBL;
class NewSCParser implements SIParser {

    // 下面是一些常量定义
    //案件
    private String mRgtNo = "";
    private String mGrpContNo = "";
    private String mPath = "";
    private String[] Title = null;
    private String vtsName = "ClaimErrList.vts";
    private ListTable FalseListTable = new ListTable();
    private GlobalInput mG = new GlobalInput();
    public CErrors mErrors = new CErrors();
    private LICaseTimeRemindSet tLICaseTimeRemindSet = new LICaseTimeRemindSet();
    LLValidityBL tLLValidityBL = new LLValidityBL();
    private PubFun pf =new PubFun();
    private PubFun1 pf1 = new PubFun1();
    
    private String[] mErrorTitle = null;
    private String[] mErrorType = null;
    
    /** xls文件名*/
    private String fileName = null;
    /** xml文件名*/
    private String configFileName = null;
    /** Sheet的名字 */
    private String[] mSheetNames = {"Sheet1"};
    /** 存放数据的容器 */
    private Vector mVector = new Vector();

  	
    NewSCParser() {
    }
    /**
     * 为了不改动其抽象类，在此加入其构造器，配置传入的文件名及解析xml名
     * @param aFileName
     * @param aXmlName
     */
    NewSCParser(String aFileName,String aXmlName) {
    	this.fileName = aFileName;
        this.configFileName = aXmlName;
    }

    public boolean setGlobalInput(GlobalInput aG) {
        mG = aG;
        FalseListTable.setName("FAULT");
        return true;
    }

    /**
     * 解析案件，不采用节点的方式，此处传入的节点在函数中并未使用 make by houyd 2014-01-20
     * @param node Node
     * @return VData
     */
    public VData parseOneInsuredNode(Node node) {
        VData tReturn = new VData();  
        System.out.println("<-Go Into NewSCParser.parseOneInsuredNode()->");
        System.out.println("fileName:"+fileName);
        System.out.println("configFileName:"+configFileName);
        String tStart = PubFun.getCurrentTime();
        if(!doImport()){
        	CError.buildErr(this,"磁盘导入失败，Excel文件解析错误！");
        }
        int tLength = mVector.size();
        int FailureCount = 0;
        String tsq5 = "INSERT INTO ldtimetest VALUES ('rgtType','批导','"+mRgtNo+"','循环导入被保人信息开始','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq5);
        //优化效率end 
        for(int nIndex = 0; nIndex < tLength; nIndex++){
        	TransferData tTransferData = (TransferData)mVector.get(nIndex);      	
        	if (!genCase(tTransferData)) {
              System.out.println("导入这个人失败");
              FailureCount++;
              //写导出错误xml的函数
        	}
        }
        String tsq6 = "INSERT INTO ldtimetest VALUES ('rgtType','批导','"+mRgtNo+"','循环导入被保人信息结束','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
  	    new ExeSQL().execUpdateSQL(tsq6);
        if(tLICaseTimeRemindSet.size()>0){
    		try{
    			VData tVData = new VData();
    			tVData.add(tLICaseTimeRemindSet);
    		  	tVData.add(mG);
    		  	tLLValidityBL.submitData(tVData,"INSERT||MAIN");
    		}catch(Exception ex) {
    			ex.printStackTrace();
    		}
        }
        String tEnd = PubFun.getCurrentTime();
        fromDateStringToLong(tStart,tEnd);
        String tOtherSign = mRgtNo.substring(1,3);
        String tTitleSQL = "select codename,othersign from ldcode1 where codetype='llimportfeeitem' and code='86"+tOtherSign+"'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tTitleSSRS = tExeSQL.execSQL(tTitleSQL);
        String tsq7 = "INSERT INTO ldtimetest VALUES ('rgtType','批导','"+mRgtNo+"','生成错误清单信息开始','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
  	    new ExeSQL().execUpdateSQL(tsq7);
        if (tTitleSSRS.getMaxRow() > 0) {
        	mErrorTitle = new String[tTitleSSRS.getMaxRow()+1];
        	mErrorType = new String[tTitleSSRS.getMaxRow()+1];
        	
        	for (int i = 1;i<=tTitleSSRS.getMaxRow();i++) {
        		mErrorTitle[i-1] = tTitleSSRS.GetText(i, 1);
        		mErrorType[i-1] = tTitleSSRS.GetText(i, 2);
        	}
        	
        	mErrorTitle[tTitleSSRS.getMaxRow()] = "失败原因";
        	mErrorType[tTitleSSRS.getMaxRow()] = "String";
        	
        	LLExportErrorList xmlexport = new LLExportErrorList(); //新建一个XmlExport的实例
        	System.out.println("错误清单容量：" + FalseListTable.size());
            xmlexport.createDocument(mRgtNo);
            xmlexport.addListTable(FalseListTable, mErrorTitle, mErrorType);
            xmlexport.outputDocumentToFile(mPath, mRgtNo); //输出xml文档到文件	
        }
        
        String FailureNum = FailureCount + "";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("FailureNum", FailureNum);
        // #3684 阻断提示
        String tSecurityNo = LLCaseCommon.checkSocialSecurity(mRgtNo, "rgtno");
        if("1".equals(tSecurityNo) && FailureCount>0){
        	tTransferData.setNameAndValue("ErrorTitle",FailureNum); //错误清单个数
        }
        String tsq8 = "INSERT INTO ldtimetest VALUES ('rgtType','批导','"+mRgtNo+"','生成错误清单信息结束','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	  new ExeSQL().execUpdateSQL(tsq8);
        tReturn.clear();
        tReturn.add(tTransferData);
        return tReturn;
    }

    public boolean setParam(String aRgtNo, String aGrpContNo, String aPath) {
        mRgtNo = aRgtNo;
        mGrpContNo = aGrpContNo;
        mPath = aPath;
        return true;
    }

    public void setFormTitle(String[] cTitle, String cvtsName) {
        Title = cTitle;
        vtsName = cvtsName;
        System.out.println("NewSCParser-vtsName:"+vtsName);
    }
    
    /**
     * 计算时间差,打印到控制台
     * @param inVal1 起始时间
     * 		  inVal2 结束时间
     * @return 
     * 
     */
    public void fromDateStringToLong(String inVal1,String inVal2) { 
    	//此方法计算时间毫秒
    	Date date1 = null; 
    	Date date2 = null;
    	//定义时间类型 
    	SimpleDateFormat inputFormat = new SimpleDateFormat("hh:mm:ss"); 
    	try { 
    		date1 = inputFormat.parse(inVal1); 
    		date2 = inputFormat.parse(inVal2); 
    	//将字符型转换成日期型
    	} catch (Exception e) {
    		e.printStackTrace(); 
    	} 
    	long ss=(date2.getTime()-date1.getTime())/1000; //共计秒数
        int MM = (int)ss/60; //共计分钟数
        int hh=(int)ss/3600; //共计小时数
        int dd=(int)hh/24; //共计天数
    	System.out.println("执行时间："+dd+"天"+(hh-dd*24)+"小时"+(MM-hh*60)+"分"+(ss-MM*60)+"秒"); 
    }
    
    /**
     * 执行磁盘导入
     * @return boolean
     */
    public boolean doImport() {
    	//fileName = "E:\\lisdev\\ui\\temp_lp\\8661.xls";
    	//configFileName = "E:\\lisdev\\ui\\temp_lp\\LL61CaseImport.xml";
    	System.out.println(" Into NewSCParser doImport");
    	
		XlsImporter importer = new XlsImporter(fileName, configFileName,
                mSheetNames);
		
		importer.doImport();
        Sheet[] sheets = importer.getSheets();
        if (!dealSheets(sheets)) {
            return false;
        }
        
//        deleteFile();
        return true;
    }
    
    /**
     * 处理所有导入的sheet
     * @param sheets Sheet[]
     * @return boolean
     */
    protected boolean dealSheets(Sheet[] sheets) {
        for (int i = 0; i < sheets.length; i++) {
            if (!dealOneSheet(sheets[i], i)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * 处理一个sheet中全部的数据，包装为一个存放数据的List，用于后续逻辑处理
     * @param sheet
     * @param numberOfSheet
     * @return
     */
    protected boolean dealOneSheet(Sheet sheet,int numberOfSheet) {
   
    	int size = sheet.getRowSize();//Excel中数据量的多少（无版本号），读到空行结束
    	
    	for (int i = 0; i < size; i++) { 
//    		读sheet中的一行记录，TransferData
    		 TransferData td = new TransferData();
    		 int col = sheet.getColSize();
             for (int j = 0; j < col; j++) {
                 String head = sheet.getHead(j);
                 System.out.println("head:" + head);
                 if ((head == null) || (head.equals(""))) {
                     continue;
                 }
                 String value = sheet.getText(i, j, 1);
                 Object[] args = new Object[1];
                 args[0] = StrTool.cTrim(value);
                 td.setNameAndValue(head, value);
             }  
             mVector.add(td);
         }
    	
    	return true;
    }
    
    /**
     * 处理一条记录，完成导入操作
     * @param cTD
     * @return
     */
    private boolean genCase(TransferData cTD) {

        //确认客户信息
        String aSecurityNo = "" + (String) cTD.getValueByName("SecurityNo");
        String aCustomerNo = "" + (String) cTD.getValueByName("CustomerNo");
        String aIDNo = "" + (String) cTD.getValueByName("IDNo");
        String aCustomerName = "" + (String) cTD.getValueByName("CustomerName");
        System.out.println("CustomerNo:"+aCustomerNo+",CustomerName:"+aCustomerName);
        String aSex = "" + (String) cTD.getValueByName("Sex");
        String aBirthday = FormatDate((String) cTD.getValueByName("Birthday"));
        String tSupInHosFee = FormatDate((String) cTD.getValueByName("SupInHosFee"));
        String tInsuredStat = "" + (String) cTD.getValueByName("InsuredStat");
        String tGetDutyKind = "" + (String) cTD.getValueByName("GetDutyKind");
        String tGetDutyCode = "" + (String) cTD.getValueByName("GetDutyCode");
        //#3545 增加字段理赔终止
        String tClaimEnd = "" + (String) cTD.getValueByName("ClaimEnd");
        String tPostalAddress = "" + (String) cTD.getValueByName("PostalAddress");
        // #3661 **start**
        String tRealPay = "" + (String) cTD.getValueByName("AffixNo");
        // #3661 **end**
        
        String strSQL0 =
        	    "select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype,idenddate,contno,idstartdate ";// 3207 管理式补充A
        String sqlpart1 = "from lcinsured a where ";
        String sqlpart2 = "from lbinsured a where ";
        String wherePart = "a.GrpContNo = '" + mGrpContNo + "' ";
        String msgPart = "";
        String ErrMessage = "";
        
    	//数字有效性校验
    	String tOtherSign = mRgtNo.substring(1,3);
        String tTitleSQL = "select codealias,othersign,codename from ldcode1 where codetype='llimportfeeitem' and othersign='Number' and code='86"+tOtherSign+"'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tTitleSSRS = tExeSQL.execSQL(tTitleSQL);
        
        if (tTitleSSRS.getMaxRow() > 0) {
        	String [] mcodealias = new String[tTitleSSRS.getMaxRow()+1];
        	String [] mothersign = new String[tTitleSSRS.getMaxRow()+1];
        	String [] mcodename = new String[tTitleSSRS.getMaxRow()+1];
        	for (int i = 1;i<=tTitleSSRS.getMaxRow();i++) {
        		mcodealias[i-1] = tTitleSSRS.GetText(i, 1);
        		mothersign[i-1] = tTitleSSRS.GetText(i, 2);
        		mcodename[i-1] = tTitleSSRS.GetText(i, 3);
        		String modes="" + (String) cTD.getValueByName(mcodealias[i-1]);
        		if(!"".equals(modes) && !"null".equals(modes) && modes!=null){
        			try{
        				Double.parseDouble(modes);
        			}catch (Exception ex) {
        	               System.out.println("导入模板中'"+mcodename[i-1]+"'项'"+modes+"不能转换成数字！");
        	               ErrMessage = "导入模板中'"+mcodename[i-1]+"'项'"+modes+"'不能转换成数字！";
        	               getFalseList(cTD, ErrMessage);
        	               return false;
        	        }       		
        		}        		
        	}
        }
             
        if (!"".equals(aCustomerName) && !"null".equals(aCustomerName)) {
        	wherePart += " and a.name='" + aCustomerName + "' ";
            msgPart = "客户姓名为" + aCustomerName ;
		}
        
        if (!aCustomerNo.equals("null") && !aCustomerNo.equals("")) {
            wherePart += " and a.insuredno='" + aCustomerNo + "' ";
            msgPart += ",客户号为" + aCustomerNo;
        } else if (!aSecurityNo.equals("null") && !aSecurityNo.equals("")) {
            wherePart += " and a.othidno='" + aSecurityNo + "'";
            msgPart += ",社保号为" + aSecurityNo;
        } else if (!aIDNo.equals("null") && !aIDNo.equals("")) {
            wherePart += " and a.idno='" + aIDNo + "'";
            msgPart += ",身份证号为" + aIDNo;
        } else {
            ErrMessage = "未提供客户号，社保号或身份证号等信息，无法确认客户身份！";
            getFalseList(cTD, ErrMessage);
            return false;
        }

        String strSQL = strSQL0 + sqlpart1 + wherePart + " union " + strSQL0 +
                        sqlpart2 + wherePart;
        
          
        SSRS tssrs = tExeSQL.execSQL(strSQL);
        System.out.println(strSQL);
        if (tssrs == null || tssrs.getMaxRow() <= 0) {
            ErrMessage = "团单" + mGrpContNo + "下没有" + msgPart + "的被保险人";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        aCustomerNo = tssrs.GetText(1, 1);
        aIDNo = tssrs.GetText(1, 5);
        aSecurityNo = tssrs.GetText(1, 6);
        String tIdType=tssrs.GetText(1, 8);
        String tContno=tssrs.GetText(1, 10); //3545 增加分单号
        String atIdStartDate=tssrs.GetText(1, 11);
        if("null".equals(tIdType) ||tIdType==null||tIdType==""){
        	tIdType="4";
        }
      //----------------------------------------------------------------------------	
        String  atIdEndDate="";
		atIdEndDate= tssrs.GetText(1, 9);
		LICaseTimeRemindSchema tLICaseTimeRemindSchema = null;
//----------------------------------------------------------------------------		
		//Date datea=new Date();
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
		//Date dateb;
			try {
				//dateb = sdf.parse(atIdEndDate);
				//if(dateb.before(datea)){
					String date1 = pf.getCurrentDate();
					int no = pf.calInterval(date1,atIdEndDate,"D");
					if(no<0){
					tLICaseTimeRemindSchema = new LICaseTimeRemindSchema();
					String serialno = "LS"+pf.getCurrentDate2()+""+ pf1.CreateMaxNo("LSH_TimeRemind", 8);
				    System.out.println("serialno:"+serialno);
					tLICaseTimeRemindSchema.setSerialNo(serialno);
					tLICaseTimeRemindSchema.setinsuredno(aCustomerNo);
					tLICaseTimeRemindSchema.setinsuredname(aCustomerName);
					tLICaseTimeRemindSchema.setsex(aSex);
					tLICaseTimeRemindSchema.setbirthday(aBirthday);
					tLICaseTimeRemindSchema.setidtype(tIdType);
					tLICaseTimeRemindSchema.setidno(aIDNo);
					tLICaseTimeRemindSchema.setIDStartDate(atIdStartDate);
					tLICaseTimeRemindSchema.setIDEndDate(atIdEndDate);
					tLICaseTimeRemindSchema.setReminddate(pf.getCurrentDate());
					tLICaseTimeRemindSchema.setRemindTime(pf.getCurrentTime());
					tLICaseTimeRemindSchema.setaddress(tPostalAddress);
					tLICaseTimeRemindSchema.setManageCom(mG.ManageCom);
					tLICaseTimeRemindSchema.setOperator(mG.Operator);
					tLICaseTimeRemindSchema.setMakeDate(pf.getCurrentDate());
					tLICaseTimeRemindSchema.setMakeTime(pf.getCurrentTime());
					tLICaseTimeRemindSchema.setmodifydate(pf.getCurrentDate());
					tLICaseTimeRemindSchema.setmodifytime(pf.getCurrentTime());
					
					tLICaseTimeRemindSet.add(tLICaseTimeRemindSchema);
					
					ErrMessage = "被保险人"+ aCustomerName +"，"+"客户号"+aCustomerNo+"，" +"证件已过期，请联系客户变更证件有效期。";
		            getFalseList(cTD, ErrMessage);
		            return false; 
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//----------------------------------------------------------------------------	
        if (aCustomerName.equals("") || aCustomerName.equals("null")) {
            aCustomerName = tssrs.GetText(1, 2);
        } else if (!tssrs.GetText(1, 2).equals(aCustomerName)) {
            ErrMessage = msgPart + "的客户投保时姓名为" + tssrs.GetText(1, 2)
                         + ",与导入姓名" + aCustomerName + "不符！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        String tSex = tssrs.GetText(1, 3).equals("null") ? "" :
                      tssrs.GetText(1, 3);
        if (aSex.equals("") || aSex.equals("null")) {
            aSex = tSex;
        } else if (!tSex.equals("") && !tSex.equals(aSex)) {
            ErrMessage = msgPart + "的客户投保时性别为" + tSex
                         + ",与本次导入性别" + aSex + "不符！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        String tBirthday = tssrs.GetText(1, 4).equals("null") ? "" :
                           tssrs.GetText(1, 4);
        if (aBirthday.equals("") || aBirthday.equals("null")) {
            aBirthday = tBirthday;
        } else if (!tBirthday.equals("") && !tBirthday.equals(aBirthday)) {
            ErrMessage = msgPart + "的客户投保时生日为" + tBirthday
                         + ",与本次导入生日" + aBirthday + "不符！";
            getFalseList(cTD, ErrMessage);
            return false;
        }        
        
        String insts = tssrs.GetText(1, 7).equals("null") ? "" :
                       tssrs.GetText(1, 7);
        if (tInsuredStat.equals("") || tInsuredStat.equals("null")) {
            tInsuredStat = insts;
        } else if (!insts.equals("") && !insts.equals(tInsuredStat)) {
            ErrMessage = msgPart + "的客户投保时为" + insts
                         + "状态,与本次导入" + tInsuredStat + "状态不符！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        String result = LLCaseCommon.checkPerson(aCustomerNo);
        if(!"".equals(result)){
            ErrMessage = msgPart + "的客户正在进行保全操作,工单号为"+result+"！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        String strCustomerSQL="select * from lpedorapp a,lpgrpedoritem b where a.edoracceptno in (select max(edorno) from lpdiskimport where  insuredno='"+aCustomerNo+"' and grpcontno ='"+mGrpContNo+"') and a.edorstate !='0' and a.edoracceptno = b.edoracceptno and b.edortype ='ZT'";
        SSRS customertssrs = new SSRS();
        customertssrs = tExeSQL.execSQL(strCustomerSQL);
        if(customertssrs.getMaxRow()>0){
            ErrMessage = msgPart + "的客户正在进行保全减人操作！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        
        //modify by Houyd 修改校验位置，将部分校验前置 20140331
        String aAccDate = FormatDate((String) cTD.getValueByName("AccDate"));
        //aAccDate = aAccDate.equals("") ? PubFun.getCurrentDate() : aAccDate;
        if(aAccDate==null || "".equals(aAccDate) ||"null".equals(aAccDate)){
            ErrMessage = msgPart + "的客户出险日期不能为空，或出险日期格式不正确应为'YYYY-MM-DD'或者'YYYYMMDD'！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        //#4205 账单日期和发生日期
        String tFeeDate = FormatDate((String) cTD.getValueByName("FeeDate"));
        if(tFeeDate==null || "".equals(tFeeDate) ||"null".equals(tFeeDate)){
            ErrMessage = "账单日期为空";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        if(PubFun.calInterval(PubFun.getCurrentDate(),tFeeDate,"D")>0 || PubFun.calInterval(tFeeDate,"2004-01-01","D")>0 || !"2".equals(tFeeDate.substring(0,1))){
    		ErrMessage ="账单日期/入院日期/出院日期有误，请核实并录入正确日期" ;
            getFalseList(cTD, ErrMessage);
            return false;
    	}   
        if(PubFun.calInterval(PubFun.getCurrentDate(),aAccDate,"D")>0 || PubFun.calInterval(aAccDate,"2004-01-01","D")>0 || !"2".equals(aAccDate.substring(0,1))){
    		ErrMessage ="请核实发生日期录入是否正确" ;
            getFalseList(cTD, ErrMessage);
            return false;
    	}       
        //#4205
        ExeSQL texesql1 = new ExeSQL();
        String tAccSQL = "select 1 from lcgrppol a,lmriskapp b where a.riskcode=b.riskcode"
            + " and b.risktype3='7' and b.risktype <>'M'"
            + " and a.grpcontno='"+ mGrpContNo +"'";
        SSRS tAccSSRS = texesql1.execSQL(tAccSQL);
        boolean tDiseaseFlag = true;
        if (tAccSSRS.getMaxRow()>0) {
        	tDiseaseFlag = false;
        }
        String tHospitalCode = "" + (String) cTD.getValueByName("HospitalCode");
        String tHospitalName = "" + (String) cTD.getValueByName("HospitalName");
        
        if (("".equals(tHospitalCode)||"null".equals(tHospitalCode))&&tDiseaseFlag) {
            ErrMessage = "医院编码不能为空";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        if (("".equals(tHospitalName)||"null".equals(tHospitalName))&&tDiseaseFlag) {
            ErrMessage = "医院名称不能为空";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        
        String tApplyAmnt=(String) cTD.getValueByName("ApplyAmnt");
        if(tApplyAmnt==null || "".equals(tApplyAmnt) ||"null".equals(tApplyAmnt)){
            ErrMessage = msgPart + "的客户费用总额为空！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        
        String tDiseaseCode = "" + (String) cTD.getValueByName("DiseaseCode");
        String tDiseaseName = "" + (String) cTD.getValueByName("DiseaseName");
        if (("".equals(tDiseaseCode)||"null".equals(tDiseaseCode))&&tDiseaseFlag) {
            ErrMessage = "疾病编码不能为空";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        if (("".equals(tDiseaseName)||"null".equals(tDiseaseName))&&tDiseaseFlag) {
            ErrMessage = "疾病名称不能为空";
            getFalseList(cTD, ErrMessage);
            return false;
        }

        
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
        LLSecurityReceiptSchema tLLSecurityReceiptSchema = new
                LLSecurityReceiptSchema();
        LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
        LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
        VData tVData = new VData();

        genCaseInfoBL tgenCaseInfoBL = new genCaseInfoBL();

        tLLCaseSchema.setRgtType("1");
        tLLRegisterSchema.setRgtNo(mRgtNo);
        tLLRegisterSchema.setRgtObjNo(mGrpContNo);
        

        String aFeeType = "" + cTD.getValueByName("FeeType");
        //#4205 就诊类别
        if(aFeeType==null || "".equals(aFeeType) || "null".equals(aFeeType) || (!"1".equals(aFeeType) && !"2".equals(aFeeType) && !"3".equals(aFeeType))){
            ErrMessage = "未录入正确的就诊类别";
            getFalseList(cTD, ErrMessage);
            return false;
        }      
        //#4205
        LLAppClaimReasonSchema tLLAppClaimReasonSchema = new
                LLAppClaimReasonSchema();
        if (aFeeType.trim().equals("1")) {
            tLLAppClaimReasonSchema.setReasonCode("01");
            tLLAppClaimReasonSchema.setReason("门诊"); //申请原因
        } else if(aFeeType.trim().equals("2")){
            tLLAppClaimReasonSchema.setReasonCode("02"); //原因代码
            tLLAppClaimReasonSchema.setReason("住院"); //申请原因
        }else if(aFeeType.trim().equals("3")){
            tLLAppClaimReasonSchema.setReasonCode("03"); //原因代码
            tLLAppClaimReasonSchema.setReason("特种病"); //申请原因
        }else{
        	//#531_如果不是以上三种类型，则不存
            tLLAppClaimReasonSchema.setReasonCode("00"); //原因代码
            tLLAppClaimReasonSchema.setReason("无"); //申请原因
            /**ErrMessage = "帐单类型错误，请输入正确的帐单类型1，2或3";
            getFalseList(cTD, ErrMessage);
            return false;
        	*/

        }
        tLLAppClaimReasonSchema.setCustomerNo(aCustomerNo);
        tLLAppClaimReasonSchema.setReasonType("0");

        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setRgtNo(mRgtNo);
        tLLCaseDB.setCustomerNo(aCustomerNo);
        LLCaseSet temLLCaseSet = tLLCaseDB.query();
        if (temLLCaseSet.size() > 0) {
            tLLCaseSchema.setUWState("7");
        }
        
//      modify by zhangyige 2012-07-09
        String aPrePaidFlag = "";
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if(tLLRegisterDB.getInfo())
		{
			aPrePaidFlag = tLLRegisterDB.getSchema().getPrePaidFlag();
		}
        tLLCaseSchema.setPrePaidFlag(aPrePaidFlag);
        
        tLLCaseSchema.setCaseNo("");
        tLLCaseSchema.setRgtNo(mRgtNo);
        tLLCaseSchema.setCustomerName(aCustomerName);
        tLLCaseSchema.setCustomerNo(aCustomerNo);
        tLLCaseSchema.setIDType(tIdType);
        tLLCaseSchema.setIDNo(aIDNo);
        tLLCaseSchema.setOtherIDType("5");
        tLLCaseSchema.setOtherIDNo(aSecurityNo);
        tLLCaseSchema.setPhone((String) cTD.getValueByName("GetDutyCode"));
        System.out.println(tLLCaseSchema.getPhone());
        tLLCaseSchema.setCustBirthday(aBirthday);
        tLLCaseSchema.setCustomerSex(aSex);
        tLLCaseSchema.setGrpNo((String) cTD.getValueByName("GrpNo"));
        
        // add new 2014-04-04 扣除明细
        tLLCaseSchema.setRemark((String) cTD.getValueByName("Remark"));
        // add end
        
        tLLCaseSchema.setAccidentDate(aAccDate);
//        if(tLLCaseSchema.getAccidentDate()==null)//by zzh 0930
//        {
//        	ErrMessage = msgPart + "的客户出险日期格式不正确，应为'YYYY-MM-DD'或者'YYYYMMDD'！";
//            getFalseList(cTD, ErrMessage);
//            return false;
//        }
        System.out.println(tLLCaseSchema.getAccidentDate());
        String tMobilePhone=""+(String) cTD.getValueByName("MobilePhone");
        tLLCaseSchema.setMobilePhone(tMobilePhone);
        tLLCaseSchema.setRgtState("03"); //案件状态
        tLLCaseSchema.setSurveyFlag("0");
        tLLCaseSchema.setAccdentDesc("磁盘导入");
        tLLCaseSchema.setSurveyFlag("0");
        String tPayModeSQL = "SELECT casegetmode FROM llregister WHERE rgtno='"+mRgtNo+"'";
        ExeSQL tPMExSQL = new ExeSQL();
        SSRS tPMSSRS = tPMExSQL.execSQL(tPayModeSQL);
        if (tPMSSRS.getMaxRow() > 0) {
           try {
               tLLCaseSchema.setCaseGetMode(tPMSSRS.GetText(1, 1));
           } catch (Exception ex) {
               System.out.println("插入给付方式失败！");
           }
        }
        String aBankCode=(String) cTD.getValueByName("BankCode");
        String aBankAccNo=(String) cTD.getValueByName("BankAccNo");
        String aAccName=(String) cTD.getValueByName("AccName");

        if(aBankCode==null || "".equals(aBankCode) ||"null".equals(aBankCode)||aBankAccNo==null || "".equals(aBankAccNo) ||"null".equals(aBankAccNo)||aAccName==null || "".equals(aAccName) ||"null".equals(aAccName)){
            String sql =
                "select bankcode,bankaccno,accname from lcinsured where insuredno='" +
                aCustomerNo + "' and grpcontno='" + mGrpContNo + "'" +
                " union " +
                "select bankcode,bankaccno,accname from lbinsured where insuredno='" +
                aCustomerNo + "' and grpcontno='" + mGrpContNo + "'";

            	
            	SSRS trr = tExeSQL.execSQL(sql);
            	if (trr.getMaxRow() > 0) {
            		try {
            			tLLCaseSchema.setBankCode(trr.GetText(1, 1));
            			tLLCaseSchema.setBankAccNo(trr.GetText(1, 2));
            			tLLCaseSchema.setAccName(trr.GetText(1, 3));
            		} catch (Exception ex) {
            			System.out.println("通过数据库查询后插入用户帐户信息失败！");
            		}
            	}
        
        }else{
        	try {
    			tLLCaseSchema.setBankCode(aBankCode);
    			tLLCaseSchema.setBankAccNo(aBankAccNo);
    			tLLCaseSchema.setAccName(aAccName);
    		} catch (Exception ex) {
    			System.out.println("通过导入模板插入用户帐户信息失败！");
    		}
        }

//      2807领款人和被保人关系校验
        String RelaDrawerInsured = "" + (String)cTD.getValueByName("RelaDrawerInsured");
        if(!"".equals(RelaDrawerInsured)&&RelaDrawerInsured!=null&&!"null".equals(RelaDrawerInsured)){
//	        String sql23 = "select 1 from dual where " + RelaDrawerInsured +" in(select code from LDcode where codetype='relation')";
        	String sql23 = "select 1 from LDcode where codetype='relation' and code='"+RelaDrawerInsured+"' ";
	        
	        SSRS trr2 = tExeSQL.execSQL(sql23);
	        if (trr2.getMaxRow() <= 0) {
	        	 ErrMessage = msgPart + "领款人和被保人关系校验编码不正确！";
	             getFalseList(cTD, ErrMessage);
	             return false;
	        }
        }
        LLCaseExtSchema tLLCaseExtSchema = new LLCaseExtSchema();
        tLLCaseExtSchema.setRelaDrawerInsured(RelaDrawerInsured);
        
        // #3365 河南分公司大病保险业务理赔批量导入功能调整 **start**
        String  AreaCode =""+ cTD.getValueByName("AreaCode");
        if(!"".equals(AreaCode)&& AreaCode!=null && !"null".equals(AreaCode)){
              Pattern tPattern = Pattern.compile("(\\d)+");
    		  Matcher isNum = tPattern.matcher(AreaCode);
            if(isNum.matches()){
	        	if(AreaCode.length()>50){
	        		ErrMessage =msgPart +",地区编码字符串过长";
	        		getFalseList(cTD,ErrMessage);
	        		return false;
	        	}else{
	        		tLLCaseExtSchema.setAreaCode(AreaCode);
	        	}
            }else{
            	if(AreaCode.length()>16){
	        		ErrMessage =msgPart +",地区编码字符串过长";
	        		getFalseList(cTD,ErrMessage);
	        		return false;
	        	}else{
	        		tLLCaseExtSchema.setAreaCode(AreaCode);
	        	}
            }
        }
        String tAuthorization = ""+cTD.getValueByName("Authorization");
        if("".equals(tAuthorization) || "1".equals(tAuthorization) || tAuthorization == null || "null".equals(tAuthorization)){
           String sqlAuth = "select authorization from ldperson where customerno ='"+aCustomerNo+"' with ur";
           String authValue = new ExeSQL().getOneValue(sqlAuth);
           if("".equals(tAuthorization) || tAuthorization == null || "null".equals(tAuthorization)){
            	tLLCaseExtSchema.setAuthorization(authValue);
           }else{
               if("1".equals(authValue)){
                	tLLCaseExtSchema.setAuthorization(authValue);
                }
               else{ 
        	       tLLCaseExtSchema.setAuthorization("1".equals(tAuthorization)?tAuthorization:"");
                }
           }
        }else{
        	ErrMessage ="模板授权字段信息不符合标准";
    		getFalseList(cTD,ErrMessage);
    		return false;
        }
        

        String CountyCode =""+ cTD.getValueByName("CountyCode");
        if(!"".equals(CountyCode) && CountyCode!=null && !"null".equals(CountyCode)){
           Pattern aPattern = Pattern.compile("(\\d)+");
   		   Matcher isNum = aPattern.matcher(CountyCode);
           if(isNum.matches()){
	        	if(CountyCode.length()>50){
	        		ErrMessage =msgPart +",县区编码字符串过长";
	        		getFalseList(cTD,ErrMessage);
	        		return false;
	        	}else{
	        		tLLCaseExtSchema.setCountyCode(CountyCode);
	        	}
           }else{
        	   if(CountyCode.length()>16){
	        		ErrMessage =msgPart +",县区编码字符串过长";
	        		getFalseList(cTD,ErrMessage);
	        		return false;
	        	}else{
	        		tLLCaseExtSchema.setAreaCode(CountyCode);
	        	}
           }
        }
        
        String RationalAmnt =""+ cTD.getValueByName("RationalAmnt");
        if(!"".equals(RationalAmnt) && RationalAmnt !=null && !"null".equals(RationalAmnt)){
        	tLLCaseExtSchema.setRationalAmnt(Double.parseDouble(RationalAmnt));
        }
       //#3365 河南分公司大病保险业务理赔批量导入功能调整 **end**
        //#3545 守望健康增加理赔终止标识star  698001 体检责任
//      String checkRiskSQL="select riskcode from lcgrppol where grpcontno='"+mGrpContNo+"' union select riskcode from lbgrppol where grpcontno='"+mGrpContNo+"' with ur";
       //是否有守望健康
      String checkRiskSQL="select 1 from lcgrppol where grpcontno='"+mGrpContNo+"' and riskcode='122101' union select 1 from lbgrppol where grpcontno='"+mGrpContNo+"' and riskcode='122101' with ur";
      ExeSQL checkRisk = new ExeSQL();
      SSRS SSRScheckRisk = checkRisk.execSQL(checkRiskSQL);
//      if(SSRScheckRisk.getMaxRow()==1&&"698001".equals(SSRScheckRisk.GetText(1, 1))){
      if(SSRScheckRisk.getMaxRow()>0&&"901".equals(tGetDutyKind)&&"698201".equals(tGetDutyCode)){   
//   		String checkDutySQL="select dutycode from lcduty where contno='"+tContno+"' union select dutycode from lbduty where contno='"+tContno+"' with ur";
   		String checkDutySQL="select dutycode from lcduty a,lcpol b where a.polno=b.polno and a.contno='"+tContno+"' and b.riskcode='122101' union select dutycode from lbduty a,lbpol b where a.polno=b.polno and a.contno='"+tContno+"' and b.riskcode='122101' with ur";

   	    SSRS SSRScheckDutyp = tExeSQL.execSQL(checkDutySQL);
   	    if(SSRScheckDutyp.getMaxRow()==1&&"698001".equals(SSRScheckDutyp.GetText(1, 1))){
	    	 //校验客户是否导入过理赔终止
	    	if(!"1".equals(tClaimEnd)&&!"2".equals(tClaimEnd)){
	    		 ErrMessage = "赔付险种为《健康守望补充医疗保险（A款）》，且仅有体检责任，请核实理赔终止字段是否录入";
	    		 getFalseList(cTD,ErrMessage);
	    		 return false;
	    	 	 }
   	    	 //批次导入未理算的情况
   	    	 String checkCustomerSQL = "select a.caseno from llcase a,llregister b where a.rgtno=b.rgtno and b.rgtobjno='"+mGrpContNo+"' and a.customerno='"+aCustomerNo+"' and a.rgtstate not in('11','12','14') "
   	    	 		+ "and exists(select 1 from llcaseext where caseno=a.caseno and claimend='1') fetch first 1 rows only with ur";

   	    	   
   	    	 String CaseState;
   	    	 String Caseno;
   	    	 String ClaimEnd;
   	    	 SSRS SSRScheckCustomer = tExeSQL.execSQL(checkCustomerSQL);
   	    	//批次导入理算的情况
   	    	 if(SSRScheckCustomer.getMaxRow()>0){
   	    		 Caseno = SSRScheckCustomer.GetText(1, 1); //案件号
   	    		 ErrMessage = "该客户有正在赔付的《健康守望补充医疗保险（A款）》险种的理赔案件，案件为:"+Caseno+"待其结案并通知给付后再处理本案件";
   	    		 getFalseList(cTD,ErrMessage);
	        		 return false;
   	    	 }
   	    	 checkCustomerSQL = "select a.caseno,a.claimend,b.rgtstate from llcaseext a,llcase b where a.caseno=b.caseno and a.claimend='1' and a.caseno in(select distinct caseno from llclaimdetail where grpcontno='"+mGrpContNo+"' and contno='"+tContno+"') with ur";
   	    	 SSRScheckCustomer = tExeSQL.execSQL(checkCustomerSQL);
   	    	 //如果导入过
   	    	
   	    	 if(SSRScheckCustomer.getMaxRow()>0){
   	    		 for (int i = 1; i <= SSRScheckCustomer.getMaxRow(); i++) {
   	    			 CaseState = SSRScheckCustomer.GetText(i, 3); //案件状态
   	    			 Caseno = SSRScheckCustomer.GetText(i, 1); //案件号
   	    			 ClaimEnd = SSRScheckCustomer.GetText(i, 2); //是否理赔终止
   	    			 if(!"11".equals(CaseState)&&!"12".equals(CaseState)){
   	    				 ErrMessage = "该客户有正在赔付的《健康守望补充医疗保险（A款）》险种的理赔案件，案件为:"+Caseno+"待其结案并通知给付后再处理本案件";
   	    	    		 getFalseList(cTD,ErrMessage);
   	 	        		 return false;
   	    			 }else if("1".equals(ClaimEnd)){
   	    				 ErrMessage = "被保险人"+aCustomerName+"《健康守望补充医疗保险（A款）》已失效，不能再次理赔";
   	    	    		 getFalseList(cTD,ErrMessage);
   	 	        		 return false;
   	    			 }
					}
   	    	 }
   	    		 //扩表，llcaseExt  字段ClaimEnd 
   	    		 tLLCaseExtSchema.setClaimEnd(tClaimEnd);
   	    		
   	    	 
   	   }
      }
      
      //#3545 守望健康增加理赔终止标识end
        
      // 3207 团体管理式补充医疗保险（A款）**start**
       String mCaseSQL ="select  distinct a.caseno ,b.riskcode from llcase a,llclaimdetail b where a.caseno=b.caseno "+
    		   		"and a.rgtstate not in ('11','12','14') and b.riskcode in('162401','162501') and b.contno='"+tssrs.GetText(1, 10)+"' " +
    		   		"and a.customerno ='"+aCustomerNo+"' and a.caseno<>'' with ur";
       			
       			SSRS tSSRSCount=tExeSQL.execSQL(mCaseSQL);
       			if(tSSRSCount.getMaxRow()>0){
       				for(int i=1;i<=tSSRSCount.getMaxRow();i++){
       					String tRiskSQL="select riskname from lmrisk where riskcode='"+tSSRSCount.GetText(i, 2)+"' with ur";
       					String tRiskName = tExeSQL.getOneValue(tRiskSQL);
       					ErrMessage= msgPart +"被保险人该保单有正在赔付"+tRiskName+"险种的理赔案件，案件号为"+tSSRSCount.GetText(i, 1)+"，待其结案并通知给付后再处理本案件。";
       					getFalseList(cTD, ErrMessage);
       					return false;
       			}
       		}	
       // 3207  团体管理式补充医疗保险（A款）**end**	
	 // 3661 《健康守望补充医疗保险（B款）》**start**	
		String tRiskcodeBSQL = "select code from ldcode where codetype ='jiankangB' and comcode='"+tGetDutyCode+"' and othersign='"+tGetDutyKind+"' with ur ";
        String tRiskcodeB = tExeSQL.getOneValue(tRiskcodeBSQL);
        String tRiskCodeSQL = "select 1 from lcgrppol where grpcontno='"+mGrpContNo+"' and riskcode ='"+tRiskcodeB+"' " +
       					      "union select 1 from lbgrppol where grpcontno='"+mGrpContNo+"' and riskcode ='"+tRiskcodeB+"' with ur";
        SSRS tRiskCode = tExeSQL.execSQL(tRiskCodeSQL);
        if(tRiskCode.getMaxRow()>0&&"900".equals(tGetDutyKind)){
			String tPolNoSQL = "select polno from lcget where contno='"+tContno+"' and getdutycode='"+tGetDutyCode+"' union select polno from lbget where contno='"+tContno+"' and getdutycode='"+tGetDutyCode+"' with ur ";
			String tPolNo = tExeSQL.getOneValue(tPolNoSQL);   	   
			String tPremSQL = "select prem from lcpol a where a.polno='"+tPolNo+"' and riskcode ='"+tRiskcodeB+"' " +
		   					  "union select prem from lbpol a where a.polno='"+tPolNo+"' and riskcode ='"+tRiskcodeB+"' with ur ";
			String tPrem = tExeSQL.getOneValue(tPremSQL);  // 险种保费						   
			if(!("".equals(tPrem)||tPrem==null)){
				String tSumPaySQL = "select sum(b.realpay) from llcase a,llclaimdetail b where a.caseno=b.caseno and a.rgtstate in ('09','11','12') and b.polno='"+tPolNo+"' with ur";
				String tSumPay = tExeSQL.getOneValue(tSumPaySQL);
				if("".equals(tSumPay)||tSumPay==null){
					tSumPay = "0";	
				}
				if(Double.parseDouble(tSumPay)+Double.parseDouble(tRealPay)>Double.parseDouble(tPrem)){
						String tGetdutyNameSQL = "select getdutyname from lmdutygetclm where getdutycode='"+tGetDutyCode+"'";
						String tGetdutyName = tExeSQL.getOneValue(tGetdutyNameSQL);
						ErrMessage= "被保险人："+aCustomerName+"，"+tGetdutyName+"支出已达限额";
						getFalseList(cTD, ErrMessage);
						return false;
				}
			}
       }
      // 3661 《健康守望补充医疗保险（B款）》**end**
	  // 3500 增加发生地点和医保类型**start**
   		LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();  // 事件信息表
   		String tProvinceAccCode=(String) cTD.getValueByName("ProvinceAccPlace");	//发生地点省
   		String tCityAccCode =(String) cTD.getValueByName("CityAccPlace");			//发生地点市     
   		String tCountyAccCode =(String) cTD.getValueByName("CountyAccPlace");       //发生地点县
   		//#4205 事件类型
        String aAccType = (String) cTD.getValueByName("AccType");
        if(aAccType==null || "".equals(aAccType) ||"null".equals(aAccType) || (!"1".equals(aAccType) && !"2".equals(aAccType))){       	
    		ErrMessage = "未录入正确的事件类型";
            getFalseList(cTD, ErrMessage);
            return false;         
        }
        //#4205 事件类型
        //#4172
        String tGetDutyKindJB = tExeSQL.getOneValue("select 1 from ldcode where codetype = 'JBGetDuty' and code='"+tGetDutyKind+"' with ur");
        String tGetDutyKindYW = tExeSQL.getOneValue("select 2 from ldcode where codetype = 'YWGetDuty' and code='"+tGetDutyKind+"' with ur");      
        if("1".equals(tGetDutyKindJB)){
        	if(!"1".equals(aAccType)){
        		ErrMessage = "当前事件类型不为疾病，请确认事件类型录入是否正确";
                getFalseList(cTD, ErrMessage);
                return false;  
        	}
        }
        if("2".equals(tGetDutyKindYW)){
        	if(!"2".equals(aAccType)){
        		ErrMessage = "当前事件类型不为意外，请确认事件类型是否录入正确";
                getFalseList(cTD, ErrMessage);
                return false;  
        	}
        }
        //#4172
   		// 校验录入的省、市、县编码信息
   		String tAccCode=LLCaseCommon.checkAccPlace(tProvinceAccCode,tCityAccCode,tCountyAccCode); 
       		if(!"".equals(tAccCode)){
       			ErrMessage = msgPart + "的客户,"+tAccCode+"！";
                getFalseList(cTD, ErrMessage);
                return false;
       		}else{
       			tLLSubReportSchema.setAccProvinceCode(tProvinceAccCode);
       			tLLSubReportSchema.setAccCityCode(tCityAccCode);
       			tLLSubReportSchema.setAccCountyCode(tCountyAccCode);
       			tLLSubReportSchema.setAccidentType(aAccType);
       		}
   		String tMedicareType =(String) cTD.getValueByName("MedicareType");			//医保类型
   		
   		//校验录入的医保类型信息
   		String tMedicareCode =LLCaseCommon.checkMedicareType(tMedicareType);
       		if(!"".equals(tMedicareCode)){
       			ErrMessage = msgPart + "的客户录入时,"+tMedicareCode+"！";
                getFalseList(cTD, ErrMessage);
                return false;
       		}else{
       			tLLFeeMainSchema.setMedicareType(tMedicareCode);
       		}
   // 3500 增加发生地点和医保类型**end**	
    		   
    /**    String aAffixNo="" + (String) cTD.getValueByName("AffixNo");
        if(!(aAffixNo==null || "".equals(aAffixNo) ||"null".equals(aAffixNo))){
        	String ttGetDutyCode ="" + (String) cTD.getValueByName("GetDutyCode");
        	if(ttGetDutyCode==null || "".equals(ttGetDutyCode) ||"null".equals(ttGetDutyCode)){
                LCPolBL tLCPolBL = new LCPolBL();
                tLCPolBL.setInsuredNo(aCustomerNo);
                tLCPolBL.setGrpContNo(mGrpContNo);
                LCPolSet tLCPolSet = new LCPolSet();
                tLCPolSet = tLCPolBL.query();
                int n = tLCPolSet.size();
                for (int i = 1; i <= n; i++) {
                	LCPolSchema tLCPolSchema = tLCPolSet.get(i);
                    String rsql="select 1 from(select getdutykind a,count(getdutycode) b from lmdutygetclm where getdutycode in (select getdutycode from lcget where polno='"+tLCPolSchema.getPolNo()+"' and GrpContNo='"+mGrpContNo+"' union select getdutycode from lbget where polno='"+tLCPolSchema.getPolNo()+"' and GrpContNo='"+mGrpContNo+"') group by getdutykind )as aa where aa.b>1 with ur";
                    ExeSQL mExeSQL = new ExeSQL();
                    SSRS mTitleSSRS = mExeSQL.execSQL(rsql);
                    if (mTitleSSRS.getMaxRow() > 0) {
                        ErrMessage = msgPart + "的客户‘给付责任编码’或‘就诊类别’，‘医疗标记’错误导致金额重复！";
                        getFalseList(cTD, ErrMessage);
                        return false;
                    }
                }
        	}
        		
        }**/

        if(!(tGetDutyKind==null||"".equals(tGetDutyKind)||"null".equals(tGetDutyKind))){
        	tLLFeeMainSchema.setOldMainFeeNo(tGetDutyKind);
        }
        if(!"".equals(tPostalAddress) && tPostalAddress !=null && !"null".equals(tPostalAddress)){
        	tLLCaseExtSchema.setPostalAddress(tPostalAddress);
        }
        tLLFeeMainSchema.setRgtNo(mRgtNo);
        tLLFeeMainSchema.setCaseNo("");
        tLLFeeMainSchema.setCustomerNo(aCustomerNo);
        tLLFeeMainSchema.setCustomerName(aCustomerName);
        tLLFeeMainSchema.setCustomerSex(aSex);
        tLLFeeMainSchema.setInsuredStat(tInsuredStat);
        tLLFeeMainSchema.setHospitalCode(tHospitalCode);
        tLLFeeMainSchema.setHospitalName(tHospitalName);
        tLLFeeMainSchema.setFeeAtti("4");
        tLLFeeMainSchema.setSecurityNo(aSecurityNo);
        tLLFeeMainSchema = (LLFeeMainSchema) fillCommonField(tLLFeeMainSchema,
                cTD);
        if (tLLFeeMainSchema.getFeeDate()==null
            || "".equals(tLLFeeMainSchema.getFeeDate())
            ||"null".equals(tLLFeeMainSchema.getFeeDate())) {
            tLLFeeMainSchema.setFeeDate(aAccDate);
        }
        // #3558 保单登记平台日期校验 star
        String tHospStartDate = FormatDate((String) cTD.getValueByName("HospStartDate"));
        String tHospEndDate = FormatDate((String) cTD.getValueByName("HospEndDate")) ;
        if(aFeeType=="2"||"2".equals(aFeeType)){
        	if(tHospStartDate==null||tHospEndDate==null||"".equals(tHospStartDate)||"".equals(tHospEndDate)){
        		ErrMessage =msgPart+"的客户入出院日期有误,请核实！" ;
                getFalseList(cTD, ErrMessage);
                return false;
        	}
        	//#4205  入出院日期
        	if(PubFun.calInterval(tHospStartDate,"2004-01-01","D")>0 || PubFun.calInterval(tHospEndDate,"2004-01-01","D")>0){
        		ErrMessage ="账单日期/入院日期/出院日期有误，请核实并录入正确日期" ;
                getFalseList(cTD, ErrMessage);
                return false;
        	}
        	//#4205
//        	String BeforeDate = PubFun.getBeforeDate(tHospStartDate, tHospEndDate);
        	//FormatDate 日期格式.
        	FDate fd = new FDate();
        	Date HospStartDate = fd.getDate(tHospStartDate);
        	Date HospEndDate = fd.getDate(tHospEndDate);
        	if(HospStartDate!=null&&HospEndDate!=null&&!"".equals(HospStartDate)&&!"".equals(HospEndDate)){
	        	if(HospStartDate.after(HospEndDate)){
	        		ErrMessage ="出院日期早于入院日期。" ;
	                getFalseList(cTD, ErrMessage);
	                return false;
	        	}
        	}else{
        		ErrMessage =msgPart+"的客户入出院日期有误,请核实！" ;
                getFalseList(cTD, ErrMessage);
                return false;
        	}
        }
        // #3558 保单登记平台日期校验  end
        tLLFeeMainSchema.setHospStartDate(tHospStartDate);
        tLLFeeMainSchema.setHospEndDate(tHospEndDate);
        tLLFeeMainSchema.setInHosNo(FormatDate((String) cTD.getValueByName(
        		        "InpatientNo")));
        //不合理费用
        String tRefuseAmnt=(String) cTD.getValueByName("RefuseAmnt");
        if(tRefuseAmnt!=null && !"".equals(tRefuseAmnt) && !"null".equals(tRefuseAmnt)){
        	tLLFeeMainSchema.setSumFee(Double.parseDouble(tRefuseAmnt));
        }
        
        //可报销范围内金额
        String tReimbursement=(String) cTD.getValueByName("Reimbursement");
        if(tReimbursement!=null && !"".equals(tReimbursement) && !"null".equals(tReimbursement)){
        	tLLFeeMainSchema.setReimbursement(Double.parseDouble(tReimbursement));
        }
        
        LCGrpContSchema kLCGrpContSchema = new LCGrpContSchema();
        LCGrpContDB kLCGrpContDB = new LCGrpContDB();
        kLCGrpContDB.setGrpContNo(mGrpContNo);
        kLCGrpContDB.getInfo();
        kLCGrpContSchema = kLCGrpContDB.getSchema();

        String tMngCom = kLCGrpContSchema.getManageCom();
        System.out.println(tMngCom.substring(0, 4));  
       
        tLLFeeMainSchema.setSumFee(Double.parseDouble((String) cTD.
                getValueByName("ApplyAmnt")));
        //# 3684 核心业务系统可能出现的社保业务案件重复受理    start
        
        String SNo = LLCaseCommon.checkSocialSecurity(mRgtNo, "rgtno");
        if("1".equals(SNo)) {
        	tHospStartDate = tLLFeeMainSchema.getHospStartDate();
            tHospEndDate = tLLFeeMainSchema.getHospEndDate();
        	if("".equals(tHospStartDate)||tHospStartDate==null||"null".equals(tHospStartDate)) {
        		ErrMessage ="录入入院日期不能为空" ;
                getFalseList(cTD, ErrMessage);
                return false;
        	}
        	if("".equals(tHospEndDate)||tHospEndDate==null||"null".equals(tHospEndDate)) {
        		ErrMessage ="录入出院日期不能为空" ;
                getFalseList(cTD, ErrMessage);
                return false;
        	}
        	String aReceiptStr = "";
        	String aReceiptNo = tLLFeeMainSchema.getReceiptNo();
        	if("".equals(aReceiptNo)||aReceiptNo==null||"null".equals(aReceiptNo)) {
        		aReceiptStr = " and ReceiptNo is null";
        	}else {
        		aReceiptStr = " and ReceiptNo = '" + aReceiptNo + "'";
        	}
        	String aAffixNo = tLLFeeMainSchema.getAffixNo();
        	String aAffixNoStr = "";
        	if("".equals(aAffixNo)||aAffixNo==null||"null".equals(aAffixNo)) {
        		aAffixNoStr = " and AffixNo is null";
        	}else {
        		aAffixNoStr = " and AffixNo = '" + aAffixNo + "'";
        	}
        	//校验本批次下重复的保单
        	String repeatSQL = "select * from llfeemain where 1=1 and rgtno = '" + mRgtNo 
        			+ "' and customerno = '" + aCustomerNo 
        			+ "' and HospStartDate = '" + tHospStartDate 
        			+ "' and HospEndDate = '" + tHospEndDate 
        			+ "' and SumFee = '" + tLLFeeMainSchema.getSumFee() 
        			+ "' " + aAffixNoStr 
        			+  aReceiptStr
        			+ " with ur";
        	ExeSQL RepeatExeSQL = new ExeSQL();
        	SSRS RepeatSSRS = RepeatExeSQL.execSQL(repeatSQL);
        	int repeatNo = RepeatSSRS.getMaxRow();
        	if(repeatNo>0) {
        		ErrMessage= "该批次被保险人" + aCustomerName + "已存在，不能重复上载！";
            	getFalseList(cTD, ErrMessage);
            	return false; 
        	}
        	
        	if("".equals(aReceiptNo)||aReceiptNo==null||"null".equals(aReceiptNo)) {
        		aReceiptStr = " and a.ReceiptNo is null";
        	}else {
        		aReceiptStr = " and a.ReceiptNo = '" + aReceiptNo + "'";
        	}
        	if("".equals(aAffixNo)||aAffixNo==null||"null".equals(aAffixNo)) {
        		aAffixNoStr = " and a.AffixNo is null";
        	}else {
        		aAffixNoStr = " and a.AffixNo = '" + aAffixNo + "'";
        	}
        	String SocialSQL = "select b.caseno from llfeemain a ,llclaimpolicy b where "
        			+ "1=1 and a.caseno=b.caseno and  a.customerno = '" +
        			aCustomerNo + "' and a.HospStartDate = '" + 
        			tHospStartDate + "' and a.HospEndDate = '" + 
        			tHospEndDate + "' and a.SumFee = '" + 
        			tLLFeeMainSchema.getSumFee() + "' " +
        			aAffixNoStr + aReceiptStr +
        			 " and b.grpcontno='"+
        			mGrpContNo+"' with ur";
        	
        	SSRS SocialSSRS = tExeSQL.execSQL(SocialSQL);
        	int SocialNumber = SocialSSRS.getMaxRow();
        	if(SocialNumber>0) {
        		String CaseStr = "";
        		if(SocialNumber==1) {
        			CaseStr = SocialSSRS.GetText(1, 1);
        		}
        		if(SocialNumber>1){
        			for(int i = 1 ; i<= SocialNumber ; i++) {
            			CaseStr += SocialSSRS.GetText(i, 1) + ",";
            		}
        		}
        		ErrMessage= "被保险人" + aCustomerName + "，已受理申请案件号：" + CaseStr  
        					+ "与本次申请账单信息中客户号，入院日期，出院日期，费用总额,账单号，赔款金额六者重复，请审核。";
        		getFalseList(cTD, ErrMessage);
	            return false; 
        	}
        }
        //# 3684 核心业务系统可能出现的社保业务案件重复受理    end

        
        //领款人信息
        
        
      //领款人和被保人关系
     	 
        String tostrSQL=" select llr.TogetherFlag,llr.GetMode,(select codename from ldcode where 1=1 and codetype='llgetmode' and code=llr.GetMode ) from LLRegister llr where  rgtno like 'P%' and  rgtno = '" + mRgtNo + "'";
         SSRS ssr = new ExeSQL().execSQL(tostrSQL);
         if (ssr.getMaxRow() > 0) {	
    	
        	 	String TogetherFlag=ssr.GetText(1, 1);
        	 	String GetMode=ssr.GetText(1, 2);
        	 	String GetModeName=ssr.GetText(1, 3);
    			if(  "1".equals(TogetherFlag) || "2".equals(TogetherFlag) ){//个人给付
    				 //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
//    				String RelaDrawerInsured = ""+cTD.getValueByName("RelaDrawerInsured");
    				String DrawerIDType = ""+cTD.getValueByName("DrawerIDType");
    				String DrawerID = ""+cTD.getValueByName("DrawerID");
    				
    				if("1".equals(GetMode) || "2".equals(GetMode)){    //为现金
    					tLLCaseExtSchema.setDrawerIDType("");
        				tLLCaseExtSchema.setDrawerID("");
    					
    				}else{
    				
    				//5.6.3.二.4
    				if("28".equals(RelaDrawerInsured)){ 
    					System.out.println("领款人和被保人关系为雇主，领款人证件号和证件类型可以为空");
    					
    				}else if("00".equals(RelaDrawerInsured)){
    					if(  "null".equals(DrawerIDType)||DrawerIDType==null || "".equals(DrawerIDType)|| "0".equals(DrawerIDType)){
    					}else{
    						ErrMessage ="领款人身份证信息与被保人证件信息不符，请核实";
        					getFalseList(cTD, ErrMessage);
        	    			return false;
    					}
    					if(  "null".equals(DrawerID)||DrawerID==null || "".equals(DrawerID)|| aIDNo.equals(DrawerID)){
    					}else{
    						ErrMessage ="领款人身份证信息与被保人证件信息不符，请核实";
        					getFalseList(cTD, ErrMessage);
        	    			return false;
    					}
    				}
    				else
    				{
    					if("null".equals(RelaDrawerInsured)||"".equals(RelaDrawerInsured)||RelaDrawerInsured==null){
    						ErrMessage ="赔款领取方式为("+GetMode+"-"+GetModeName+")时，领款人和被保人关系不能为空" ;
    				        getFalseList(cTD, ErrMessage);
    				        return false;
    					} 
    					if( "null".equals(DrawerIDType)||"".equals(DrawerIDType)||DrawerIDType==null){
    						ErrMessage ="赔款领取方式为("+GetMode+"-"+GetModeName+")时，领款人证件类型不能为空" ;
    				        getFalseList(cTD, ErrMessage);
    				        return false;		
    					}
    					if( "null".equals(DrawerID)||"".equals(DrawerID)||DrawerID==null){
    						ErrMessage ="赔款领取方式为("+GetMode+"-"+GetModeName+")时，领款人证件号码不能为空" ;
    				        getFalseList(cTD, ErrMessage);
    				        return false;
    					}
    					
    					
    				}
    				
    				
    				//5.6.3.二.5
    				
    				
    				//5.6.3.二.6
    				if(!"28".equals(RelaDrawerInsured)){
    					if( "5".equals(DrawerIDType)|| "0".equals(DrawerIDType)){
    						
    						if("".equals(PubFun.CheckIDNo( DrawerIDType, DrawerID, "", ""))){
    						}else{
    							ErrMessage ="领款人证件号码不符合规则，请核实";
            					getFalseList(cTD, ErrMessage);
            	    			return false;
    						}
    						 
    					}
    					 
    				}
    				tLLCaseExtSchema.setDrawerIDType("null".equals(DrawerIDType)|| null==DrawerIDType  ? "" : DrawerIDType);
    				tLLCaseExtSchema.setDrawerID("null".equals(DrawerID)|| null==DrawerID  ? "" : DrawerID);
      			    //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
    			
    			}
    				
    				
         }
        }
        
        
        
        
        
        //解析xml模版中定义社保项到LLSecurityReceipt，按照名称一一对应
        tLLSecurityReceiptSchema = (LLSecurityReceiptSchema)
                                   fillCommonField(tLLSecurityReceiptSchema,
                cTD);
        System.out.println(aFeeType);
        System.out.println(tLLSecurityReceiptSchema.getMidAmnt());

        System.out.println(tMngCom.substring(0, 4));
        if (aFeeType.trim().equals("1")) {
            tLLSecurityReceiptSchema.setHighDoorAmnt(tLLSecurityReceiptSchema.
                    getMidAmnt());
            tLLSecurityReceiptSchema.setMidAmnt(0);
            tLLSecurityReceiptSchema.setSmallDoorPay(tLLSecurityReceiptSchema.
                    getGetLimit());
            tLLSecurityReceiptSchema.setGetLimit(0);

        }

        tLLSecurityReceiptSchema.setLowAmnt(tLLSecurityReceiptSchema.
                                            getGetLimit());
        tLLSecurityReceiptSchema.setHighAmnt2(tLLSecurityReceiptSchema.
                                              getSupInHosFee());

        LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();

        if (!"".equals(tDiseaseCode) && !"null".equals(tDiseaseCode)) {
            tLLCaseCureSchema.setDiseaseCode(tDiseaseCode);
            tLLCaseCureSchema.setDiseaseName(tDiseaseName);
            tLLCaseCureSchema.setMainDiseaseFlag("1");
            tLLCaseCureSet.add(tLLCaseCureSchema);
        }
        String tDiseaseCode2 = "" + (String) cTD.getValueByName("DiseaseCode2");
        String tDiseaseName2 = "" + (String) cTD.getValueByName("DiseaseName2");

        if (!tDiseaseCode2.equals("null") || !tDiseaseName2.equals("null")) {
            tLLCaseCureSchema = new LLCaseCureSchema();
            tLLCaseCureSchema.setDiseaseCode(tDiseaseCode2);
            tLLCaseCureSchema.setDiseaseName(tDiseaseName2);
            tLLCaseCureSet.add(tLLCaseSchema);
        }
        //保存案件时效
        tLLCaseOpTimeSchema.setRgtState("01");
        tLLCaseOpTimeSchema.setStartDate(PubFun.getCurrentDate());
        tLLCaseOpTimeSchema.setStartTime(PubFun.getCurrentTime());

        System.out.println("<--Star NewSCParser Submit VData-->");
        if(!"00".equals(tLLAppClaimReasonSchema.getReasonCode())){
        tVData.add(tLLAppClaimReasonSchema);	
        }
        tVData.add(tLLCaseSchema);
        tVData.add(tLLCaseExtSchema);//2807领款人和被保人关
        tVData.add(tLLRegisterSchema);
        tVData.add(tLLFeeMainSchema);
        tVData.add(tLLSecurityReceiptSchema);
        tVData.add(tLLCaseCureSet);
        tVData.add(tLLCaseOpTimeSchema);
        tVData.add(tLLSubReportSchema);
        tVData.add(mG);
        if(tLLSubReportSchema !=null){  // #3500 增加省、市、县编码
        	tVData.add(tLLSubReportSchema);
        }
        System.out.println("<--Into NewSCParser tSimpleClaimUI-->" + mG.ManageCom);
        if (!tgenCaseInfoBL.submitData(tVData, "INSERT||MAIN")) {
            CErrors tError = tgenCaseInfoBL.mErrors;
            ErrMessage = tError.getFirstError();
            getFalseList(cTD, ErrMessage);
            return false;
        }
        return true;
    }

    private Object fillCommonField(Object cSchema, TransferData cTD) {
        int n = cTD.getValueNames().size();
        for (int i = 0; i < n; i++) {
            String fieldName = (String) cTD.getNameByIndex(i);
            String fieldValue = (String) cTD.getValueByIndex(i);
            Class[] c = new Class[1];
            Method m = null;
            fieldName = "set" + fieldName;
            String[] tValue = {fieldValue};
            try {
                c[0] = Class.forName("java.lang.String");
            } catch (Exception ex) {
            }
            try {
                m = cSchema.getClass().getMethod(fieldName, c);
            } catch (Exception ex) {
            }
            try {
                m.invoke(cSchema, tValue);
            } catch (Exception ex) {
            }
        }
        return cSchema;
    }

    private boolean getFalseList(TransferData cTD, String ErrMessage) {
        int n = Title.length;
        String[] strCol = new String[n + 1];
        for (int i = 0; i < n; i++) {
            strCol[i] = (String) cTD.getValueByIndex(i);
        }
        strCol[n] = ErrMessage;
        System.out.println("ErrMessage:" + ErrMessage);
        FalseListTable.add(strCol);
        return true;
    }

    private String FormatDate(String aDate) {
        String rDate = "";
        rDate += aDate;
        if (rDate.equals("") || rDate.equals("null")) {
            return "";
        }
        if (aDate.length() < 8) {
            int days = 0;
            try {
                days = Integer.parseInt(aDate) - 2;
            } catch (Exception ex) {
                return "";
            }
            rDate = PubFun.calDate("1900-1-1", days, "D", null);
        }
        return rDate;
    }
    
    public static void main(String[] args) {
    	String tStart = "15:30:00";
    	String tEnd = "15:30:30";
    	NewSCParser tNewSCParser = new NewSCParser();
    	tNewSCParser.fromDateStringToLong(tStart,tEnd);
    	String tCurruent = PubFun.getCurrentTime();
    	System.out.println("CurrentTime:"+tCurruent);
	}

}
