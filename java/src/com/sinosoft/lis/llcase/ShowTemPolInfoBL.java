package com.sinosoft.lis.llcase;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author 刘岩松
 * function :show temporary pol info Business Logic Layer
 * @version 1.0
 * @date 2003-04-09
 */

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;

public class ShowTemPolInfoBL {
  public CErrors mErrors=new CErrors();
  private VData mResult = new VData();
  /**
   * @define private variable
   */
  private String strRgtNo;
  private String strInsuredNo;
  private GlobalInput mG = new GlobalInput();
  private String strMngCom;
  private String t_MngCom;
  private String sub_MngCom;
  public ShowTemPolInfoBL()
  {
  }

  public boolean submitData(VData cInputData, String cOperate)
  {
    if( !getInputData(cInputData) )
    {
      return false;
    }

    if( !QueryData() )
    {
      return false;
    }
    return true;
  }
  public VData getResult()
  {
    return mResult;
  }
  private boolean getInputData(VData cInputData)
  {
    strRgtNo = (String)cInputData.get(0);
    strInsuredNo = (String)cInputData.get(1);
    mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    strMngCom = mG.ManageCom;
    System.out.println("在Business Logic Layer中得到的数据是");
    System.out.println("立案号码是"+strRgtNo);
    System.out.println("客户号码是"+strInsuredNo);
    System.out.println("管理机构是"+strMngCom);
    return true;
  }



  /********
   * name : buildError
   * function : Prompt error message
   * return :error message
   */

  private void buildError(String szFunc, String szErrMsg)
  {
    CError cError = new CError( );
    cError.moduleName = "PEfficiencyBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }

  public boolean QueryData()
  {
    System.out.println("开始执行查询系统表查询操作！！！");
    LDSysVarDB tLDSysVarDB = new LDSysVarDB();
    tLDSysVarDB.setSysVar("temppolvalidate");
    if(!tLDSysVarDB.getInfo())
    {
      CError tError = new CError();
      tError.moduleName = "PEfficiencyBL";
      tError.functionName = "getDutyGetClmInfo";
      tError.errorMessage = "系统表信息查询失败！！！" ;
      this.mErrors.addOneError(tError) ;
      return false;
    }
    System.out.println("系统表的描述信息是"+tLDSysVarDB.getSysVarType());
    LCPolSet tLCPolSet = new LCPolSet();
    String t_sql = "";

    if(tLDSysVarDB.getSysVarType().equals("0"))
    {
      System.out.println("这种情况下使用的是FirstPayDate作为开始日期！！！");
      t_sql = "select PolNo,AppntName,InsuredName,RiskCode,Amnt,FirstPayDate,CValiDate,SaleChnl from lcpol where AppFlag = '1' and insuredno = '"
          +strInsuredNo +"' and ManageCom like '"
          +strMngCom+ "%' and polno in (select polno from lcget where getdutycode"
          +" in (select getdutycode from LMDutyGetClm where getdutykind='102'))";
    }
    else
    {
      System.out.println("这种情况下使用的是PolApplyDate作为开始日期！！！");
      t_sql = "select PolNo,AppntName,InsuredName,RiskCode,Amnt,PolApplyDate,CValiDate,SaleChnl from lcpol where AppFlag = '1' and insuredno = '"
          +strInsuredNo +"' and ManageCom like '"
          +strMngCom+ "%' and polno in (select polno from lcget where getdutycode"
          +" in (select getdutycode from LMDutyGetClm where getdutykind='102'))";

    }
    System.out.println("您的查询语句是"+t_sql);
    ExeSQL exeSQL = new ExeSQL();
    SSRS ssrs = exeSQL.execSQL(t_sql);
    int pol_count = ssrs.getMaxRow();
    if(pol_count == 0)
    {
      CError tError = new CError();
      tError.moduleName = "PEfficiencyBL";
      tError.functionName = "getDutyGetClmInfo";
      tError.errorMessage = "没有符合条件的数据记录！！！" ;
      this.mErrors.addOneError(tError) ;
      return false;
    }
    mResult.clear();
    mResult.add(ssrs);
    return true;
  }
  /*
  * @function :in order to debug
  */
  public static void main(String[] args)
  {
    String tRgtNo = "2003-03-01";
    String tInsuredNo = "86110020030990000002";
    String tMngCom = "86112000";
    VData tVData = new VData();
    tVData.addElement(tRgtNo);
    tVData.addElement(tInsuredNo);
    tVData.addElement(tMngCom);
    ShowTemPolInfoUI tShowTemPolInfoUI = new ShowTemPolInfoUI();
    tShowTemPolInfoUI.submitData(tVData,"QUERY");
  }
}