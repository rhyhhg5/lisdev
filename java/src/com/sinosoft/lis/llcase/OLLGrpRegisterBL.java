/*
 * <p>ClassName: OLLRegisterBL </p>
 * <p>Description: OLLRegisterBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author:YangMing
 * @CreateDate：2005-02-16 11:49:38
 */
package com.sinosoft.lis.llcase;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class OLLGrpRegisterBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
/** 业务处理相关变量 */
private LLRegisterSchema mLLRegisterSchema=new LLRegisterSchema();
private LLRegisterSet mLLRegisterSet=new LLRegisterSet();
public OLLGrpRegisterBL() {
}
public static void main(String[] args) {
}
/**
* 传输数据的公共方法
	* @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "OLLRegisterBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败OLLRegisterBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
      System.out.println("Start OLLRegisterBL Submit...");
      /*OLLRegisterBLS tOLLRegisterBLS=new OLLRegisterBLS();
      tOLLRegisterBLS.submitData(mInputData,mOperate);
      System.out.println("End OLLRegisterBL Submit...");
      //如果有需要处理的错误，则返回
      if (tOLLRegisterBLS.mErrors.needDealError())
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tOLLRegisterBLS.mErrors);
        CError tError = new CError();
        tError.moduleName = "OLLRegisterBL";
        tError.functionName = "submitDat";
        tError.errorMessage ="数据提交失败!";
        this.mErrors .addOneError(tError) ;
        return false;
      }
*/    PubSubmit tPubSubmit = new PubSubmit();
         if (!tPubSubmit.submitData(mInputData, mOperate))
         {
           // @@错误处理
           this.mErrors.copyAllErrors(tPubSubmit.mErrors);
           CError tError = new CError();
           tError.moduleName = "OLLGrpRegisterBL";
           tError.functionName = "submitData";
           tError.errorMessage = "数据提交失败!";

           this.mErrors.addOneError(tError);
           return false;
         }
         System.out.println("End OLLGrpRegisterBL Submit...");

    }
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
  /*生成理赔号*/
  String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
  System.out.println("管理机构代码是 : "+tLimit);
  mLLRegisterSchema.setRgtNo("");
  mLLRegisterSchema.setRgtState("");
  mLLRegisterSchema.setRgtObj("");
  mLLRegisterSchema.setOperator(mGlobalInput.Operator);
  mLLRegisterSchema.setMakeDate(PubFun.getCurrentDate());
  mLLRegisterSchema.setMakeTime(PubFun.getCurrentTime());
  mLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
  mLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());


  return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
         this.mLLRegisterSet.set((LLRegisterSet)cInputData.getObjectByObjectName("LLRegisterSet",0));
         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LLRegisterDB tLLRegisterDB=new LLRegisterDB();
    tLLRegisterDB.setSchema(this.mLLRegisterSchema);
		//如果有需要处理的错误，则返回
		if (tLLRegisterDB.mErrors.needDealError())
 		{
		  // @@错误处理
 			this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "LLRegisterBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors .addOneError(tError) ;
 			return false;
    }
    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
   try
	{
		this.mInputData.clear();
		this.mInputData.add(this.mLLRegisterSchema);
		mResult.clear();
    mResult.add(this.mLLRegisterSchema);
	}
	catch(Exception ex)
	{
 		// @@错误处理
		CError tError =new CError();
 		tError.moduleName="LLRegisterBL";
 		tError.functionName="prepareData";
 		tError.errorMessage="在准备往后层处理所需要的数据时出错。";
 		this.mErrors .addOneError(tError) ;
		return false;
	}
	return true;
	}
	public VData getResult()
	{
  	return this.mResult;
	}
}
