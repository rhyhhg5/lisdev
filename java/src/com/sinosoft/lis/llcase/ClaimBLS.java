package com.sinosoft.lis.llcase;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统投保暂交费功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author YT
 * @version 1.0
 */

public class ClaimBLS
{
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();


  public ClaimBLS() {}

  public static void main(String[] args)
  {
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone();

    System.out.println("Start LLClaim BLS Submit...");
    if (!this.saveData())
      return false;
    System.out.println("End LLClaim BLS Submit...");
    mInputData=null;
    return true;
  }

  private boolean saveData()
  {
    LLClaimSet mLLClaimSet = (LLClaimSet)mInputData.getObjectByObjectName("LLClaimBLSet",0);
    LLClaimDetailSet mLLClaimDetailSet = (LLClaimDetailSet)mInputData.getObjectByObjectName("LLClaimDetailBLSet",0);

    Connection conn = null;
    conn = DBConnPool.getConnection();
    if (conn==null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ClaimBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    try
    {
      conn.setAutoCommit(false);
			// 保存现在的关联
			LLClaimDBSet mLLClaimDBSet = new LLClaimDBSet( conn );
			mLLClaimDBSet.set(mLLClaimSet);
			if (mLLClaimDBSet.insert() == false)
			{
				// @@错误处理
			  this.mErrors.copyAllErrors(mLLClaimDBSet.mErrors);
				CError tError = new CError();
				tError.moduleName = "ClaimBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "保存数据失败!";
				this.mErrors .addOneError(tError) ;
                                conn.rollback() ;
				return false;
			}

                        LLClaimDetailDBSet mLLClaimDetailDBSet = new LLClaimDetailDBSet( conn );
			mLLClaimDetailDBSet.set(mLLClaimDetailSet);
			if (mLLClaimDetailDBSet.insert() == false)
			{
				// @@错误处理
                          this.mErrors.copyAllErrors(mLLClaimDetailDBSet.mErrors);
				CError tError = new CError();
				tError.moduleName = "ClaimDetailBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "保存数据失败!";
				this.mErrors .addOneError(tError) ;
                                conn.rollback() ;
				return false;
			}
			conn.commit() ;
		} // end of try
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ClaimBLS";
			tError.functionName = "submitData";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);
			try{conn.rollback() ;} catch(Exception e){}
			return false;
		}
    return true;
  }

}