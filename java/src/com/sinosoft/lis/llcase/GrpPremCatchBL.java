package com.sinosoft.lis.llcase;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.yibaotong.YibaotongServlet;
import com.sinosoft.utility.*;

import java.util.Calendar;
import java.text.SimpleDateFormat;
import org.apache.log4j.Logger;

public class GrpPremCatchBL {

	//private static final Logger cLogger = Logger
	//		.getLogger(YibaotongServlet.class);
	
	public CErrors mCErrors = new CErrors();

	public GrpPremCatchBL() {

	}

	public boolean dealData(String aStartDate, String aEndDate) {
		System.out.println("团体保单经过保费历史数据导入开始。。。。。。");
	//	cLogger.error("团体保单经过保费历史数据导入开始。。。。。。");

		System.out.println("开始日期：" + aStartDate);
	//	cLogger.error("开始日期：" + aStartDate);
		if (aStartDate == null || "".equals(aStartDate)) {
			System.out.println("开始日期为空，不能进行处理");
			mCErrors.addOneError("开始日期为空，不能进行处理");
//			cLogger.error("开始日期为空，不能进行处理");
			return false;
		}

		if (!PubFun.checkDateForm(aStartDate)) {
			System.out.println("开始日期格式错误，不能进行处理");
			mCErrors.addOneError("开始日期格式错误，不能进行处理");
//			cLogger.error("开始日期格式错误，不能进行处理");
			return false;
		}

		System.out.println("结束日期：" + aEndDate);
//		cLogger.error("结束日期：" + aEndDate);
		if (aEndDate == null || "".equals(aEndDate)) {
			System.out.println("结束日期为空，不能进行处理");
			mCErrors.addOneError("结束日期为空，不能进行处理");
//			cLogger.error("结束日期为空，不能进行处理");
			return false;
		}

		if (!PubFun.checkDateForm(aEndDate)) {
			System.out.println("结束日期格式错误，不能进行处理");
			mCErrors.addOneError("结束日期格式错误，不能进行处理");
//			cLogger.error("结束日期格式错误，不能进行处理");
			return false;
		}

		// 处理日期
		String tDealDate = aStartDate;

		do {
			if (tDealDate == null || "".equals(tDealDate)) {
				System.out.println("处理日期为空");
				mCErrors.addOneError("处理日期为空");
//				cLogger.error("处理日期为空");
				return false;
			}

			System.out.println("本次处理日期：" + tDealDate);
			//			cLogger.error("本次处理日期：" + tDealDate);

			if (!catchData(tDealDate)) {
				return false;
			}

			tDealDate = addDay(tDealDate, 1);
			System.out.println("下次处理日期：" + tDealDate);
			//			cLogger.error("下次处理日期：" + tDealDate);
			System.out.println();
			System.out.println();
		} while (PubFun.calInterval(tDealDate, aEndDate, "D") >= 0);

		System.out.println("团体保单经过保费历史数据导入结束。。。。。。");
		//		cLogger.error("团体保单经过保费历史数据导入结束。。。。。。");
		System.out.println("团体保单经过保费历史数据导入结束日期：" + aEndDate);
		//		cLogger.error("团体保单经过保费历史数据导入结束日期：" + aEndDate);
		return true;
	}

	/**
	 * 进行数据导入
	 * 
	 * @param aDealDate
	 * @return
	 */
	private boolean catchData(String aDealDate) {
		// 清空每日团体保单变化表
		String tGrpContDeleteSQL = "alter table LQGrpContDayData activate not logged initially with empty table";

		System.out.println("开始清空" + aDealDate + "前一天每日团体保单变化表："
				+ tGrpContDeleteSQL);
		//		cLogger.error("开始清空" + aDealDate + "前一天每日团体保单变化表：" + tGrpContDeleteSQL);

		if (!pubSubmit(tGrpContDeleteSQL)) {
			System.out.println("执行失败：" + tGrpContDeleteSQL);
			mCErrors.addOneError("执行失败：" + tGrpContDeleteSQL);
			//			cLogger.error("执行失败：" + tGrpContDeleteSQL);
			return false;
		}
		System.out.println("清空" + aDealDate + "前一天每日团体保单变化表成功");
		//		cLogger.error("清空" + aDealDate + "前一天每日团体保单变化表成功");

		// 导入每日团体保单变化表
		String tGrpContLoadSQL = "insert into LQGrpContDayData "
				+ "select distinct b.grpcontno grpcontno from lpedorapp a,lpgrpedoritem b"
				+ " where a.edoracceptno=b.edoracceptno and a.othernotype='2'"
				+ " and a.confdate = '"
				+ aDealDate
				+ "' and b.edortype in ('NI','ZT','CT','XT','WZ','WJ','WT')"
				+ " union select grpcontno from lcgrpcont where signdate = '"
				+ aDealDate + "' and appflag='1' ";

		System.out.println("开始导入" + aDealDate + "每日团体保单变化表：" + tGrpContLoadSQL);
		//		cLogger.error("开始导入" + aDealDate + "每日团体保单变化表：" + tGrpContLoadSQL);

		if (!callProcedure(tGrpContLoadSQL)) {
			System.out.println("执行失败：" + tGrpContLoadSQL);
			mCErrors.addOneError("执行失败：" + tGrpContDeleteSQL);
			//			cLogger.error("开始导入" + aDealDate + "每日团体保单变化表：" + tGrpContLoadSQL);
			return false;
		}
		System.out.println("导入" + aDealDate + "每日团体保单变化表成功");
		//		cLogger.error("导入" + aDealDate + "每日团体保单变化表成功");

		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		String tSQL = "select grpcontno from LQGrpContDayData where 1=1 ";
		tSSRS = tExeSQL.execSQL(tSQL);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			System.out.println(aDealDate + "没有需要处理保单");
			//			cLogger.error(aDealDate + "没有需要处理保单");
			return true;
		}
		for (int j = 1; j <= tSSRS.getMaxRow(); j++) {
			String tGrpContNo = tSSRS.GetText(j, 1);
			if (!catchGrpContData(tGrpContNo)) {
				return false;				
			}
		}

		return true;
	}
	
	public boolean catchGrpContData(String aGrpContNo) {
		if (aGrpContNo == null || "".equals(aGrpContNo)) {
			System.out.println("为获取保单号");
			mCErrors.addOneError("为获取保单号");
			//			cLogger.error("为获取保单号");
			return false;
		}
		System.out.println("开始处理" + aGrpContNo);
		//		cLogger.error("开始处理" + aGrpContNo);
		
//		 删除中间表今天需要导入的数据
		String tGrpPremDataDeleteSQL = "delete from LQGrpPremData a where grpcontno='"+aGrpContNo+"'";

		System.out.println("开始删除中间表" + aGrpContNo + "数据："
				+ tGrpPremDataDeleteSQL);
		//		cLogger.error("开始删除中间表" + aGrpContNo + "数据：" + tGrpPremDataDeleteSQL);

		if (!pubSubmit(tGrpPremDataDeleteSQL)) {
			System.out.println("执行失败：" + tGrpPremDataDeleteSQL);
			mCErrors.addOneError("执行失败：" + tGrpPremDataDeleteSQL);
			//			cLogger.error("执行失败：" + tGrpPremDataDeleteSQL);
			return false;
		}
		System.out.println("删除" + aGrpContNo + "中间表成功");
		//		cLogger.error("删除" + aGrpContNo + "中间表成功");

		// 导入中间表数据
		String tGrpPremDataLoadSQL = "insert into LQGrpPremData "
			+ " select 'C' type, b.grpcontno grpcontno,b.grppolno grppolno,b.RiskCode riskcode,"
			+ " cast(sum(b.Prem) as decimal(12, 2)) prem,b.ManageCom managecom,b.Payintv payintv,b.salechnl salechnl,"
			+ " b.cvalidate cvalidate,b.enddate enddate,b.signdate signdate,"
			+ " (select a.cvalidate from lcgrppol a where a.grppolno=b.grppolno and a.grpcontno=b.grpcontno) grpcvalidate,"
			+ " b.enddate grpenddate,'NI' NI from lcpol b where b.appflag = '1' "
			+ " and b.grpcontno='"
			+ aGrpContNo
			+ "' and b.conttype = '2' "
			+ " and exists (select 1 from ljagetendorse c where b.polno = c.polno and b.grpcontno=c.grpcontno and b.grppolno=c.grppolno and feeoperationtype = 'NI') "
			+ " group by b.ManageCom,b.Payintv,b.salechnl,b.cvalidate,b.riskcode,b.grpcontno,b.grppolno,b.enddate,b.signdate"
			+ " union all"
			+ " select 'C' type , b.grpcontno grpcontno,b.grppolno grppolno,b.RiskCode riskcode,"
			+ " cast(sum(b.Prem) as decimal(12, 2)) prem,b.ManageCom managecom,b.Payintv payintv,b.salechnl salechnl,"
			+ " b.cvalidate cvalidate,b.enddate enddate,b.signdate signdate,b.cvalidate grpcvalidate,b.enddate grpenddate,'' NI"
      + " from lcpol b where b.appflag = '1' and b.grpcontno='"
      + aGrpContNo
      + "' "
      + " and b.conttype = '2' "
      + " and not exists (select 1 from ljagetendorse c where b.polno = c.polno and b.grpcontno=c.grpcontno and b.grppolno=c.grppolno and feeoperationtype = 'NI')"
      + " group by b.ManageCom,b.Payintv,b.salechnl,b.cvalidate,b.riskcode,b.grpcontno,b.grppolno,b.enddate,b.signdate"
      + " union all"
      + " select 'B' Type,X.grpcontno grpcontno,X.grppolno grppolno,X.riskcode riskcode,"
      + " cast(sum(X.Prem) as decimal(12, 2)) prem,X.managecom managecom,X.payintv payintv,X.salechnl salechnl,"
      + " X.cvalidate cvalidate,X.enddate enddate,X.signdate signdate,"
      + " (select cvalidate from lcgrppol where grppolno=X.grppolno and grpcontno=X.grpcontno"
      + " union all select cvalidate from lbgrppol where grppolno=X.grppolno and grpcontno=X.grpcontno fetch first 1 rows only) grpcvalidate,"
      + " X.grpenddate grpenddate,X.NI NI   from ("
      + " select a. grpcontno grpcontno,a.grppolno grppolno,a.RiskCode riskcode,a. Prem Prem,a.ManageCom managecom,"
      + " a.Payintv payintv,a. salechnl salechnl,a.cvalidate cvalidate,"
      + " (case b.edortype when 'WT' then a.cvalidate else b.edorvalidate end) enddate, a.signdate signdate,"
      + " a.enddate grpenddate,nvl((select 'NI' from ljagetendorse where a.polno = polno and grpcontno=a.grpcontno and grppolno=a.grppolno and feeoperationtype = 'NI' fetch first 1 rows only),'') NI"
      + " from lbpol a, lpedoritem b where a.polno not in ('11080609378','11080609380','11080609551','11080609552','11080609373','11080609374','11080609654','11080609655','11080609704','11080609705') and a.edorno = b.edorno and a.contno = b.contno"
      + " and a.grpcontno='"
      + aGrpContNo
      + "' and a.conttype = '2' and b.edortype in ('CT','ZT')"
      + " and exists (select 1 from lpedorapp where edoracceptno = b.edorno and edorstate = '0')) AS X"
      + " group by X.ManageCom,X.Payintv,X.salechnl,X.cvalidate,X.riskcode,X.grpcontno,X.grppolno,X.enddate,X.signdate,X.grpenddate,X.NI"
      + " union all"
      + " select 'B' Type,X.grpcontno grpcontno,X.grppolno grppolno,X.riskcode riskcode,"
      + " cast(sum(X.Prem) as decimal(12, 2)) prem,X.managecom managecom,X.payintv payintv,X.salechnl salechnl,"
      + " X.cvalidate cvalidate,X.enddate enddate,X.signdate signdate,X.grpcvalidate grpcvalidate,X.grpenddate grpenddate,X.NI NI "
      + " from (select a.grpcontno grpcontno,a.grppolno grppolno,a.RiskCode riskcode,a.Prem Prem,a.ManageCom managecom,"
      + " a.Payintv payintv,a.salechnl salechnl,a.cvalidate cvalidate,"
      + " (case b.edortype when 'WT' then a. cvalidate else b.edorvalidate end) enddate,a.signdate signdate,"
      + " c.cvalidate grpcvalidate,a.enddate grpenddate,nvl((select 'NI' from ljagetendorse where a.polno = polno and grpcontno=a.grpcontno and grppolno=a.grppolno and feeoperationtype = 'NI' fetch first 1 rows only),'') NI"
      + " from lbpol a, lpgrpedoritem b, lbgrppol c where a.polno not in ('11080609378','11080609380','11080609551','11080609552','11080609373','11080609374','11080609654','11080609655','11080609704','11080609705') and a.edorno = b.edorno and a.grppolno = c.grppolno"
      + " and a.grpcontno = b.grpcontno and a.grpcontno=c.grpcontno and b.grpcontno='"
      + aGrpContNo
      + "' "
      + " and a.conttype = '2' and b. edortype in ('XT', 'WT') and exists (select 1 from lpedorapp where edoracceptno = b.edorno and edorstate = '0' and othernotype = '2'))"
      + " AS X group by X.ManageCom,X.Payintv,X.salechnl,X.cvalidate,X.riskcode,X.grpcontno,X.grppolno,X.enddate,X.signdate,X.grpcvalidate,X.grpenddate,X.NI"
      + " union all"
      + " select 'B' Type,X.grpcontno grpcontno,X.grppolno grppolno,X.riskcode riskcode,"
      + " cast(sum(X.Prem) as decimal(12, 2)) prem,X.managecom managecom,X.payintv payintv,X.salechnl salechnl,"
      + " X.cvalidate cvalidate,X.enddate enddate,X.signdate signdate,X.grpcvalidate grpcvalidate,X.grpenddate grpenddate,X.NI NI"
      + " from (select a.grpcontno grpcontno,a.grppolno grppolno,a.RiskCode riskcode,a.Prem Prem,a.ManageCom managecom,"
      + " a.Payintv payintv,a.salechnl salechnl,a.cvalidate cvalidate,a.enddate enddate,a.signdate signdate,b.cvalidate grpcvalidate,"
      + " a.enddate grpenddate,nvl((select 'NI' from ljagetendorse where a.polno = polno and grpcontno=a.grpcontno and grppolno=a.grppolno and feeoperationtype = 'NI' fetch first 1 rows only),'') NI"
      + " from lbpol a, lbgrppol b where a.polno not in ('11080609378','11080609380','11080609551','11080609552','11080609373','11080609374','11080609654','11080609655','11080609704','11080609705') and a.grppolno = b.grppolno and a.grpcontno=b.grpcontno and a.appflag = '1'"
      + " and b.grpcontno='"
      + aGrpContNo
      + "' and a.conttype = '2'"
      + " and exists (select 1 from lcrnewstatelog where grpcontno = a.grpcontno and newpolno = a.polno)) AS X"
      + " group by X.ManageCom,X.Payintv,X.salechnl,X.cvalidate,X.riskcode,X.grpcontno,X.grppolno,X.enddate,X.signdate,X.grpcvalidate,X.grpenddate,X.NI ";
  
	System.out.println("开始导入中间表" + aGrpContNo + "数据："
				+ tGrpPremDataLoadSQL);
		//		cLogger.error("开始导入中间表" + aGrpContNo + "数据：" + tGrpPremDataLoadSQL);

		if (!callProcedure(tGrpPremDataLoadSQL)) {
			System.out.println("执行失败：" + tGrpPremDataLoadSQL);
			mCErrors.addOneError("执行失败：" + tGrpPremDataLoadSQL);
			//			cLogger.error("执行失败：" + tGrpPremDataLoadSQL);
			return false;
		}
		System.out.println("导入" + aGrpContNo + "中间表成功");
		//		cLogger.error("导入" + aGrpContNo + "中间表成功");
		return true;
	}

	/**
	 * 提交数据
	 * 
	 * @param tSQL
	 * @return
	 */
	private boolean pubSubmit(String aSQL) {
		ExeSQL tExeSQL = new ExeSQL();
		if (!tExeSQL.execUpdateSQL(aSQL)) {
			System.out.println("提交失败");
			//			cLogger.error("提交失败");
			return false;
		}
		return true;
	}

	private boolean callProcedure(String aSQL) {
		ExeSQL tExeSQL = new ExeSQL();
		if (!tExeSQL.callProcedure(aSQL)) {
			System.out.println("提交失败");
			//			cLogger.error("提交失败");
			return false;
		}
		return true;
	}

	private String addDay(String s, int n) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			Calendar cd = Calendar.getInstance();
			cd.setTime(sdf.parse(s));
			cd.add(Calendar.DATE, n);// 增加N天
			return sdf.format(cd.getTime());

		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String tStartDate = "2013-05-21";
		String tEndDate = "2013-05-21";
		GrpPremCatchBL tGrpPremCatchBL = new GrpPremCatchBL();
		try {
			tGrpPremCatchBL.dealData(tStartDate, tEndDate);
		} catch (Exception ex) {
			System.out.println("团体保单经过保费历史数据导入错误");
			//			cLogger.error("团体保单经过保费历史数据导入错误", ex);
			ex.printStackTrace();
		}
	}

}
