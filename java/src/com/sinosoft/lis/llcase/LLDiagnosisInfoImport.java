package com.sinosoft.lis.llcase;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.diskimport.DefaultDiskImporter;
import com.sinosoft.lis.schema.LLCaseDiagnosticInfoSchema;
import com.sinosoft.lis.vschema.LLCaseDiagnosticInfoSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 导入诊断数据信息类</p>
 * <p>Description: 把从磁盘导入的诊断数据信息添加到数据库 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author liuli
 * @version 1.0
 */

public class LLDiagnosisInfoImport {

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGlobalInput;
    private VData mResult = new VData();
    
    private int mSuccessNum = 0;

    public int getmSuccessNum() {
		return mSuccessNum;
	}


	public void setmSuccessNum(int mSuccessNum) {
		this.mSuccessNum = mSuccessNum;
	}

    private static final String schemaClassName =
        "com.sinosoft.lis.schema.LLCaseDiagnosticInfoSchema";
    private static final String schemaSetClassName =
        "com.sinosoft.lis.vschema.LLCaseDiagnosticInfoSet";

    /** 节点名 */
    private String sheetName = "Sheet1";
    /** 配置文件名 */
    private String configName = "DiagnosticInfo.xml";
    private LLCaseDiagnosticInfoSet mLLCaseDiagnosticInfoSet = new LLCaseDiagnosticInfoSet(); //诊断数据信息（含门诊特殊病）信息表
    /**
     * 构造函数，新契约入口
     * @param GrpContNo String
     * @param gi GlobalInputs
     */
    public LLDiagnosisInfoImport(GlobalInput gi) {
        this.mGlobalInput = gi;
    }


    /**
     * 添加被保人清单
     * @param path String
     * @param fileName String
     */
    public boolean doAdd(String path, String fileName) {

        //从磁盘导入数据
        DefaultDiskImporter importFile = new DefaultDiskImporter(path +
                fileName,
                path + configName,
                sheetName);
        importFile.setSchemaClassName(schemaClassName);
        importFile.setSchemaSetClassName(schemaSetClassName);
        if (!importFile.doImport()) {
            mErrors.copyAllErrors(importFile.mErrors);
            return false;
        }

        LLCaseDiagnosticInfoSet tLLCaseDiagnosticInfoSet = (LLCaseDiagnosticInfoSet) importFile.getSchemaSet();

        //校验导入的数据
        if (!checkData(tLLCaseDiagnosticInfoSet)) {
            return false;
        }

        //存放Insert Into语句的容器
        MMap map = new MMap();
        for (int i = 1; i <= tLLCaseDiagnosticInfoSet.size(); i++) {
        	addOneDiagnosticInfo(tLLCaseDiagnosticInfoSet.get(i));
        	mSuccessNum = mSuccessNum + 1;
        }
        map.put(mLLCaseDiagnosticInfoSet, "INSERT");
        this.mResult.add(mLLCaseDiagnosticInfoSet);
        //提交数据到数据库
        if (!submitData(map)) {
            return false;
        }
        return true;
    }

    private void deletePayList(MMap map) {

    }

    /**
     * 校验导入数据
     * @param cLLCaseDiagnosticInfoSet
     * @return boolean
     */
    private boolean checkData(LLCaseDiagnosticInfoSet cLLCaseDiagnosticInfoSet) {
        for (int i = 1; i <= cLLCaseDiagnosticInfoSet.size(); i++) {
        	LLCaseDiagnosticInfoSchema schema = cLLCaseDiagnosticInfoSet.get(i);
            String strDiseaseCode = schema.getDiseaseCode();
            if(strDiseaseCode==null||strDiseaseCode.equals("")){
                mErrors.addOneError("缺少地区疾病编码！");
                return false;
            }
            String strICD10 = schema.getICD10();
            if ((strICD10 == null) || (strICD10.equals(""))) {
                mErrors.addOneError("缺少ICD10编码！");
                return false;
            }

            String strDiseaseName = schema.getDiseaseName();
            if ((strDiseaseName == null) || (strDiseaseName.equals(""))) {
                mErrors.addOneError("缺少疾病名称！");
                return false;
            }
            
            String strMngCom = schema.getMngCom();
            if ((strMngCom == null) || (strMngCom.equals(""))) {
                mErrors.addOneError("缺少管理机构！");
                return false;
            }
            
            String strAreaCode = schema.getAreaCode();
            if ((strAreaCode == null) || (strAreaCode.equals(""))) {
                mErrors.addOneError("缺少地区编码！");
                return false;
            }
            
            String tMngCom = schema.getMngCom();
            String tMngComSQL = "select name from ldcom where sign = '1' and comcode = '" + tMngCom + "'";
            String MngCom = new ExeSQL().getOneValue(tMngComSQL);
            if ((MngCom == null) || (MngCom.equals(""))) {
                mErrors.addOneError("管理机构不存在！");
                return false;
            }
            
            String tAreaCode = schema.getAreaCode();
            String tAreaCodeSQL = "select  codename from LDCode where codetype = 'hmareacode' and code = '" + tAreaCode + "'";
            String AreaCode = new ExeSQL().getOneValue(tAreaCodeSQL);
            if ((AreaCode == null) || (AreaCode.equals(""))) {
                mErrors.addOneError("地区编码不存在！");
                return false;
            }
        }
        return true;
    }

    /**
     * 处理诊断数据信息数据
     * @param map MMap
     * @param LDHospitalSchema aLDHospitalSchema
     */
    private void addOneDiagnosticInfo(LLCaseDiagnosticInfoSchema cLLCaseDiagnosticInfo) {

    	LLCaseDiagnosticInfoSchema tLLCaseDiagnosticInfoSchema = cLLCaseDiagnosticInfo;
    	
    	tLLCaseDiagnosticInfoSchema.setSerialNo(PubFun1.CreateMaxNo("DIAGNOSTIC", 20));
    	tLLCaseDiagnosticInfoSchema.setOperator(mGlobalInput.Operator);
    	tLLCaseDiagnosticInfoSchema.setMakeDate(PubFun.getCurrentDate());
    	tLLCaseDiagnosticInfoSchema.setMakeTime(PubFun.getCurrentTime());
    	tLLCaseDiagnosticInfoSchema.setModifyDate(PubFun.getCurrentDate());
    	tLLCaseDiagnosticInfoSchema.setModifyTime(PubFun.getCurrentTime());
    	mLLCaseDiagnosticInfoSet.add(tLLCaseDiagnosticInfoSchema);
    }


    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submitData(MMap map) {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 返回执行结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

}
