package com.sinosoft.lis.llcase;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.vschema.LDGetDutyKindSet;

/**
 * <p>Title: 核心业务系统</p>
 * <p>Description: 责任过滤接口</p>
 * <p>Copyright: Sinosoft (c) 2005</p>
 * <p>Company: SinoSoft Co. Ltd,</p>
 * @author AppleWood
 * @version 6.0
 */
public interface GetDutyGet {
    /**
        接口 getPols
        给付保单初步筛选：初步筛选出客户所有的给付保单
        String customerNo 客户号
        String accdate   出险日期
        String appntNo 投保人客户号
        说明: 从C表或轨迹表中取得给付项记录
     */
    public VData getPols(String customerNo,String accdate,String appntNo);
    /**
        接口 getGetDutyGets
        给付责任初步筛选：初步筛选出客户所有的给付责任和给付责任给付
        String customerNo 客户号
        String accdate   出险日期
        说明: 从C表或轨迹表中取得给付项记录
     */
    public VData getGetDutyGets(String customerNo,String accdate,String appntNo);

    /**
     接口 getGetDutyGetsFilter
     给付责任过滤:过滤掉不属于该给付责任的
     VData dutygets 给付责任
     LDGetDutyKindSet getdutykindes　给付责任类型
     */
    public VData getGetDutyGetsFilter(VData dutygets,LDGetDutyKindSet getdutykindes);

    /**
     * 设置计算要素
     * @param transferData TransferData
     * @return VData
     */
    public void setCalFactor(TransferData transferData);

}
