package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 理赔-团体给付确认逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author：Xx
 * @version：1.0
 * @CreateDate：2005-03-18
 */
public class QDGrpGiveEnsureBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 往后面传输的数据库操作 */
    private MMap map = new MMap();
    /** 执行删除的数据库操作，放在最后 */
    private MMap mapDel = new MMap();
    /**用户登陆信息 */
    private GlobalInput mGlobalInput = new GlobalInput();
    //团体立案
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    //团体立案号
    private String mRgtNo = "";
    private String mPrePaidFlag = "";
    
    private String mBackMsg = "";
    //分案集
    private LLCaseSet mLLCaseSet = new LLCaseSet();
    private LLClaimSet mLLClaimSet = new LLClaimSet();

    private LJSGetSet mLJSGetSet = new LJSGetSet();

    private LJSGetClaimSet mLJSGetClaimSet = new LJSGetClaimSet();
    //预付赔款表
    private LLPrepaidGrpContSet mLLPrepaidGrpContSet = new LLPrepaidGrpContSet();
    private LLPrepaidGrpPolSet mLLPrepaidGrpPolSet = new LLPrepaidGrpPolSet();
    private LLPrepaidTraceSet mLLPrepaidTraceSet = new LLPrepaidTraceSet();

    public QDGrpGiveEnsureBL() {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * @param: cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("after after getInputData");
        //根据赔案号、保单号查询保单、赔案、核赔、核赔错误、用户权限信息
        if (!getBaseData()) {
            return false;
        }

        System.out.println("after getBaseData");

        //数据操作业务处理
        if (!dealData()) {
            return false;
        }

        System.out.println("after dealData");

        //准备给后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        System.out.println("after prepareOutputData");

        //数据提交
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ICaseCureBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;

        return true;
    }

    private boolean getInputData(VData cInputData) {
    	System.out.println("begin getInputData");
        mGlobalInput.setSchema((GlobalInput)
                               cInputData.getObjectByObjectName("GlobalInput",
                0));
        mLLRegisterSchema.setSchema((LLRegisterSchema)
                                    cInputData.getObjectByObjectName(
                "LLRegisterSchema", 0));
        System.out.println("yufu");
        mRgtNo = mLLRegisterSchema.getRgtNo();
        mPrePaidFlag = mLLRegisterSchema.getPrePaidFlag();
        System.out.println("回销预付赔款标记=="+mPrePaidFlag);
        
        if (mRgtNo == null || "".equals(mRgtNo)) {
			mErrors.addOneError("获取批次信息失败");
			return false;
		}
        
		if (mGlobalInput == null) {
			mErrors.addOneError("登录信息获取失败，请重新登录操作");
			return false;
		}
        
        System.out.println("after getInputData");
        return true;
    }
    

    private boolean getBaseData() {
        //查询团体立案信息
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if (!tLLRegisterDB.getInfo()) {
            CError.buildErr(this, "团体立案信息查询失败!" + "团体批次号：" + mRgtNo);
            return false;
        }
        mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());

        if (mLLRegisterSchema.getMngCom().length() == 4) {
            mLLRegisterSchema.setMngCom(mLLRegisterSchema.getMngCom() + "0000");
        }

        if (mLLRegisterSchema.getMngCom().length() == 2) {
            mLLRegisterSchema.setMngCom(mLLRegisterSchema.getMngCom() +
                                        "000000");
        }
        
        if (mGlobalInput.Operator == null || "".equals(mGlobalInput.Operator)) {
			mErrors.addOneError("登录信息获取失败，请重新登录");
			return false;
		}

		if (mGlobalInput.ManageCom == null || "".equals(mGlobalInput.ManageCom)) {
			mErrors.addOneError("登录信息获取失败，请重新登录");
			return false;
		}
		
		if (mGlobalInput.Operator.trim().equals(mLLRegisterSchema.getHandler1().trim())) {
			mErrors.addOneError("案件受理人不能进行给付确认操作");
			return false;
		}

        if (!"03".equals(mLLRegisterSchema.getRgtState())
				&& ("3".equals(mLLRegisterSchema.getTogetherFlag()) || "4"
						.equals(mLLRegisterSchema.getTogetherFlag()))) {
			CError.buildErr(this, "团体立案下个人案件尚未全部结案!" + "团体批次号：" + mRgtNo);
			return false;
		}
        String tSql = "SELECT * FROM llcase WHERE rgtno='" + mRgtNo +
        "' AND rgtstate NOT IN ('09','14')";
        System.out.println(tSql);
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSql);
        if (tSSRS.getMaxRow() > 0) {
             CError tError = new CError();
             tError.moduleName = "QDGrpGiveEnsureBL";
             tError.functionName = "getBaseData";
             tError.errorMessage = "团体立案下个人案件尚未全部结案!" + "团体批次号：" + mRgtNo;
             mErrors.addOneError(tError);
             return false;
         }

        if ("1".equals(mLLRegisterSchema.getTogetherFlag())) {
            CError.buildErr(this, "团体立案给付方式为个人给付!" + "团体批次号：" + mRgtNo);
            return false;
        }
        String real = "select 1 from ljsgetclaim where otherno='"+mRgtNo+"' with ur";
        tSSRS = tExeSQL.execSQL(real);
        if (tSSRS.getMaxRow() <= 0) {
            CError tError = new CError();
            tError.moduleName = "QDGrpGiveEnsureBL";
            tError.functionName = "getBaseData";
            tError.errorMessage = "请确认团体审批审定后是否进行了【团体结案】操作!" + "团体批次号：" + mRgtNo;
            mErrors.addOneError(tError);
            return false;
        }
        //查询团体立案下所有个案信息
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setRgtNo(mRgtNo);
        tLLCaseDB.setRgtState("09");
        mLLCaseSet.set(tLLCaseDB.query());
        if (tLLCaseDB.mErrors.needDealError() == true) {
            CError.buildErr(this, "团体批次下没有已结案待给付的分案!" + "团体批次号：" + mRgtNo);
            return false;
        }

        String delSql_2 = " delete from LJAGet where othernotype='5' " +
                          " and otherno='" + mRgtNo + "'";
        map.put(delSql_2, "DELETE");

        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        for (int i = 1; i <= mLLCaseSet.size(); i++) {
            tLLCaseSchema = mLLCaseSet.get(i);
            String tCaseNo = tLLCaseSchema.getCaseNo();
            LJSGetDB tLJSGetDB = new LJSGetDB();
            tLJSGetDB.setOtherNoType("5");
            tLJSGetDB.setOtherNo(tCaseNo);
            mLJSGetSet.add(tLJSGetDB.query());
            if (tLJSGetDB.mErrors.needDealError() == true) {
                CError.buildErr(this, "分案应付总表信息查询失败!" + "理赔号：" + tCaseNo);
                return false;
            }

            LJSGetClaimDB LJSGetClaimDB = new LJSGetClaimDB();
            LJSGetClaimDB.setOtherNoType("5");
            LJSGetClaimDB.setOtherNo(tCaseNo);
            mLJSGetClaimSet.add(LJSGetClaimDB.query());
            if (tLJSGetDB.mErrors.needDealError() == true) {
                CError.buildErr(this, "分案赔付应付表信息查询失败!" + "理赔号：" + tCaseNo);
                return false;
            }

            //插入前清空实付表垃圾数据
            String delSql_1 = " delete from LJAGetclaim where othernotype='5' "
                              + " and otherno='" + tCaseNo + "'";
            map.put(delSql_1, "DELETE");

            //删除应付表数据
        }
        map.put(mLJSGetClaimSet, "DELETE");
        map.put(mLJSGetSet, "DELETE");
        
        if (mLJSGetClaimSet.size() < 1) {
            CError.buildErr(this, "没有生成应付数据！");
            return false;
        }
        String tMngCom = "";

        for (int j = 1; j <= mLJSGetClaimSet.size(); j++) {
            if (tMngCom.equals("")) {
                tMngCom = "" + mLJSGetClaimSet.get(j).getManageCom();
            }
            if (!tMngCom.equals("" + mLJSGetClaimSet.get(j).getManageCom())) {
                CError.buildErr(this, "该批次下有赔款归属于不同机构，不能做统一给付！");
                return false;
            }
            
			if ("00000000000000000000".equals(mLJSGetClaimSet.get(j)
					.getGrpContNo())) {
				CError.buildErr(this, "批次下案件"
						+ mLJSGetClaimSet.get(j).getOtherNo()
						+ "赔付了个人保单，请进行单独给付确认、回退或撤件。");
				return false;
			}
        }
        
        return true;
    }

    private boolean dealData() {
        MMap tMap = new MMap();
        Reflections rf = new Reflections();
        MMap mMap = lockTable(mRgtNo, mGlobalInput);
        if(mMap != null){
            map.add(mMap);
        } else {
            return false;
        }

        String sNoLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
        String ActuGetNo = PubFun1.CreateMaxNo("ACTUGETNO", sNoLimit);

        //查询团体案件下所有个人案件的赔付应付记录
        //修改其他号码与其他号码类型 [RgtNo]
        //生成相应的赔付实付记录
        LJAGetClaimSet tLJAGetClaimSet = new LJAGetClaimSet();
        LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB();
        tLJSGetClaimDB.setOtherNo(mRgtNo);
        LJSGetClaimSet tLJSGetClaimSet = new LJSGetClaimSet();
        tLJSGetClaimSet = tLJSGetClaimDB.query();
        LJSGetClaimSchema aLJSGetClaimSchema = new LJSGetClaimSchema();
        aLJSGetClaimSchema = tLJSGetClaimSet.get(1);
        LJAGetClaimSchema tLJAGetClaimSchema = new LJAGetClaimSchema();
        rf.transFields(tLJAGetClaimSchema, aLJSGetClaimSchema);
        tLJAGetClaimSchema.setActuGetNo(ActuGetNo);
        tLJAGetClaimSchema.setOtherNo(mRgtNo);
        tLJAGetClaimSchema.setOPConfirmCode(mGlobalInput.Operator);
        tLJAGetClaimSchema.setOPConfirmDate(PubFun.getCurrentDate());
        tLJAGetClaimSchema.setOPConfirmTime(PubFun.getCurrentTime());
        tLJAGetClaimSchema.setOperator(mGlobalInput.Operator);
//        tLJAGetClaimSchema.setManageCom(mLLRegisterSchema.getMngCom());
        tLJAGetClaimSchema.setMakeDate(PubFun.getCurrentDate());
        tLJAGetClaimSchema.setMakeTime(PubFun.getCurrentTime());
        tLJAGetClaimSchema.setModifyDate(PubFun.getCurrentDate());
        tLJAGetClaimSchema.setModifyTime(PubFun.getCurrentTime());

        LJSGetDB tLJSGetDB = new LJSGetDB();
        tLJSGetDB.setOtherNo(mRgtNo);
        LJSGetSet tLJSGetSet = new LJSGetSet();
        tLJSGetSet = tLJSGetDB.query();
        LJSGetSchema aLJSGetSchema = new LJSGetSchema();
        aLJSGetSchema = tLJSGetSet.get(1);
        tLJAGetClaimSet.add(tLJAGetClaimSchema);
        //合成团体案件下所有的个案应付总表记录
        //修改其他号码与其他号码类型 [RgtNo]
        //生成一条团体案件的实付总表记录
        LJAGetSchema tLJAGetSchema = new LJAGetSchema();
        rf.transFields(tLJAGetSchema, aLJSGetSchema);

        tLJAGetSchema.setActuGetNo(ActuGetNo);
        tLJAGetSchema.setOtherNo(mRgtNo);

        LJSGetSchema tLJSGetSchema = new LJSGetSchema();
        double sumMoney = 0;
        for (int i = 1; i <= mLJSGetSet.size(); i++) {
            tLJSGetSchema = mLJSGetSet.get(i);
            sumMoney += tLJSGetSchema.getSumGetMoney();
        }
        System.out.println("实际应付的钱：" + sumMoney);
        double remnant = sumMoney - aLJSGetSchema.getSumGetMoney();
        remnant = Arith.round(remnant, 2);
        aLJSGetSchema.setSumGetMoney(remnant);
        aLJSGetClaimSchema.setPay(remnant);

        if ("".equals(StrTool.cTrim(mLLRegisterSchema.getCaseGetMode()))) {
            mLLRegisterSchema.setCaseGetMode("1"); //默认现金
        }
        if ("4".equals(mLLRegisterSchema.getCaseGetMode()) ||
            "11".equals(mLLRegisterSchema.getCaseGetMode())) {
            //保险金领取方式为银行转帐
            tLJAGetSchema.setBankAccNo(mLLRegisterSchema.getBankAccNo());
            tLJAGetSchema.setBankCode(mLLRegisterSchema.getBankCode());
            tLJAGetSchema.setAccName(mLLRegisterSchema.getAccName());
        }
        tLJAGetSchema.setPayMode(mLLRegisterSchema.getCaseGetMode());
        tLJAGetSchema.setDrawer(mLLRegisterSchema.getRgtantName());
        tLJAGetSchema.setDrawerID(mLLRegisterSchema.getIDNo());
        tLJAGetSchema.setOperator(mGlobalInput.Operator);
        tLJAGetSchema.setManageCom(mLLRegisterSchema.getMngCom());
        tLJAGetSchema.setShouldDate(PubFun.getCurrentDate());
        tLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
        tLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
        tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
        tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
        //更新团体案件状态为“04”－所有个人给付确认
        mLLRegisterSchema.setRgtState("05");
        mLLRegisterSchema.setPrePaidFlag(mPrePaidFlag);
        //更新该团体案件下所有案件状态为“给付状态”
        for (int k = 1; k <= mLLCaseSet.size(); k++) {
            mLLCaseSet.get(k).setRgtState("11");
            LLClaimDB tLLClaimDB = new LLClaimDB();
            LLClaimSet tLLClaimSet = new LLClaimSet();
            tLLClaimDB.setCaseNo(mLLCaseSet.get(k).getCaseNo());
            tLLClaimSet.set(tLLClaimDB.query());
            LLClaimSchema tLLClaimSchema = new LLClaimSchema();
            tLLClaimSchema = tLLClaimSet.get(1);
            tLLClaimSchema.setClmState("3");
            mLLClaimSet.add(tLLClaimSchema);
        }
        
        if ("1".equals(mLLRegisterSchema.getPrePaidFlag())) {
	        if (!dealPrapaid(tLJAGetSchema,tLJAGetClaimSet)) {
	        	CError.buildErr(this, "预付赔款处理失败！");
	            return false;
	        }
        }
        
        map.put(mLLRegisterSchema, "UPDATE");
        map.put(mLLCaseSet, "UPDATE");
        map.put(mLLClaimSet, "UPDATE");

        map.put(tLJAGetClaimSet, "INSERT");
        map.put(tLJAGetSchema, "INSERT");
        map.put(aLJSGetClaimSchema, "UPDATE");
        map.put(aLJSGetSchema, "UPDATE");
        map.add(tMap);

        return true;
    }

    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "QDGrpGiveEnsureBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错:" +
                                  ex.toString();
            mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }
    
    /**
     * 锁定案件给付
     * @param LockNoKey
     * @param tGlobalInput
     * @return
     */
    private MMap lockTable(String lockNoKey, GlobalInput globalInput)
    {
        MMap map = null;

        //案件给付类型：“LJ”
        String tLockNoType = "LJ";

        //锁定有效时间
        String tAIS = "60";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", lockNoKey);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(globalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        map = tLockTableActionBL.getSubmitMap(tVData, null);
        if(map == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }

        return map;
    } 
    
    /**
     * 扣除预付赔款
     * @return
     */
    private boolean dealPrapaid(LJAGetSchema aLJAGetSchema,
			LJAGetClaimSet aLJAGetClaimSet) {
    	System.out.println(aLJAGetClaimSet.size());
		for (int i = 1; i <= aLJAGetClaimSet.size(); i++) {
			LJAGetClaimSchema tLJAGetClaimSchema = aLJAGetClaimSet.get(i);

			if ("00000000000000000000"
					.equals(tLJAGetClaimSchema.getGrpContNo())|| !mPrePaidFlag.equals("1")) {
				System.out.println("预付赔款个单不处理："
						+ tLJAGetClaimSchema.getContNo());
				continue;
			}

			LLPrepaidGrpContDB tLLPrepaidGrpContDB = new LLPrepaidGrpContDB();
			tLLPrepaidGrpContDB.setGrpContNo(tLJAGetClaimSchema.getGrpContNo());

			if (!tLLPrepaidGrpContDB.getInfo()) {
				continue;
			}

			boolean tLockFlag = true;

			for (int j = 1; j <= mLLPrepaidGrpContSet.size(); j++) {
				LLPrepaidGrpContSchema tLLPrepaidGrpContSchema = mLLPrepaidGrpContSet
						.get(j);
				if (tLLPrepaidGrpContSchema.getGrpContNo().equals(
						tLJAGetClaimSchema.getGrpContNo())) {
					tLockFlag = false;
					break;
				}
			}

			if (tLockFlag) {
				if (!LLCaseCommon.lockFY(tLJAGetClaimSchema.getGrpContNo(),
						mGlobalInput)) {
					CError.buildErr(this, "为防止并发操作，团体保单"
							+ tLJAGetClaimSchema.getGrpContNo() + "被锁定，请稍后操作");
					return false;
				}

				if (Arith.round(tLLPrepaidGrpContDB.getApplyAmount()
						+ tLLPrepaidGrpContDB.getRegainAmount()
						+ tLLPrepaidGrpContDB.getSumPay(), 2) != tLLPrepaidGrpContDB
						.getPrepaidBala()) {
					CError.buildErr(this, "保单"
							+ tLJAGetClaimSchema.getGrpContNo() + "预付赔款余额错误");
					return false;
				}

				mLLPrepaidGrpContSet.add(tLLPrepaidGrpContDB.getSchema());
			}

			boolean tTraceFlag = true;

			for (int j = 1; j <= mLLPrepaidTraceSet.size(); j++) {
				LLPrepaidTraceSchema tLLPrepaidTraceSchema = mLLPrepaidTraceSet
						.get(j);
				if (tLLPrepaidTraceSchema.getGrpContNo().equals(
						tLJAGetClaimSchema.getGrpContNo())
						&& tLLPrepaidTraceSchema.getGrpPolNo().equals(
								tLJAGetClaimSchema.getGrpPolNo())) {
					tLLPrepaidTraceSchema.setMoney(Arith.round(
							tLLPrepaidTraceSchema.getMoney()
									- tLJAGetClaimSchema.getPay(), 2));
					tTraceFlag = false;
					break;
				}
			}

			if (tTraceFlag) {
				LLPrepaidGrpPolDB tLLPrepaidGrpPolDB = new LLPrepaidGrpPolDB();
				tLLPrepaidGrpPolDB
						.setGrpPolNo(tLJAGetClaimSchema.getGrpPolNo());
				if (!tLLPrepaidGrpPolDB.getInfo()) {
					CError.buildErr(this, "保单"
							+ tLJAGetClaimSchema.getGrpContNo() + "下险种"
							+ tLJAGetClaimSchema.getRiskCode() + "预付信息查询失败");
					return false;
				}

				if (Arith.round(tLLPrepaidGrpPolDB.getApplyAmount()
						+ tLLPrepaidGrpPolDB.getRegainAmount()
						+ tLLPrepaidGrpPolDB.getSumPay(), 2) != tLLPrepaidGrpPolDB
						.getPrepaidBala()) {
					CError.buildErr(this, "保单"
							+ tLJAGetClaimSchema.getGrpContNo() + "下险种"
							+ tLJAGetClaimSchema.getRiskCode() + "预付赔款余额错误");
					return false;
				}

				mLLPrepaidGrpPolSet.add(tLLPrepaidGrpPolDB.getSchema());

				LLPrepaidTraceSchema tLLPrepaidTraceSchema = new LLPrepaidTraceSchema();

				String tSerialNo = PubFun1.CreateMaxNo("PREPAIDTRACENO",
						tLJAGetClaimSchema.getManageCom());

				Reflections tReflections = new Reflections();
				tReflections.transFields(tLLPrepaidTraceSchema,
						tLJAGetClaimSchema);
				tLLPrepaidTraceSchema.setSerialNo(tSerialNo);
				tLLPrepaidTraceSchema.setOtherNoType("C");
				tLLPrepaidTraceSchema.setBalaDate(PubFun.getCurrentDate());
				tLLPrepaidTraceSchema.setBalaTime(PubFun.getCurrentTime());
				tLLPrepaidTraceSchema.setMoneyType("PK");
				tLLPrepaidTraceSchema.setMoney(Arith.round(-tLJAGetClaimSchema
						.getPay(), 2));
				tLLPrepaidTraceSchema.setState("1");

				mLLPrepaidTraceSet.add(tLLPrepaidTraceSchema);
			}
		}

		if (mLLPrepaidTraceSet.size() != mLLPrepaidGrpPolSet.size()) {
			CError.buildErr(this, "预付险种轨迹查询错误");
			return false;
		}

		for (int j = 1; j <= mLLPrepaidTraceSet.size(); j++) {
			LLPrepaidTraceSchema tLLPrepaidTraceSchema = mLLPrepaidTraceSet
					.get(j);
			for (int m = 1; m <= mLLPrepaidGrpPolSet.size(); m++) {
				LLPrepaidGrpPolSchema tLLPrepaidGrpPolSchema = mLLPrepaidGrpPolSet
						.get(m);
				if (tLLPrepaidGrpPolSchema.getGrpPolNo().equals(
						tLLPrepaidTraceSchema.getGrpPolNo())) {
//					取消部分回销，部分走赔款的情况
					double tMoney = 0;
//					tMoney = tLLPrepaidGrpPolSchema.getPrepaidBala() > -tLLPrepaidTraceSchema
//							.getMoney() ? -tLLPrepaidTraceSchema.getMoney()
//							: tLLPrepaidGrpPolSchema.getPrepaidBala();
							
					if(tLLPrepaidGrpPolSchema.getPrepaidBala() < -tLLPrepaidTraceSchema.getMoney()){
						CError.buildErr(this, "团体保单下险种:"+tLLPrepaidGrpPolSchema.getGrpPolNo()+",预付赔款余额不足回销该案件/批次！");
						return false;
					}else{
						tMoney = -tLLPrepaidTraceSchema.getMoney();
					}
					tLLPrepaidTraceSchema.setLastBala(tLLPrepaidGrpPolSchema
							.getPrepaidBala());
					tLLPrepaidGrpPolSchema.setPrepaidBala(Arith
							.round(tLLPrepaidGrpPolSchema.getPrepaidBala()
									- tMoney, 2));
					tLLPrepaidTraceSchema.setMoney(Arith.round(-tMoney, 2));
					tLLPrepaidTraceSchema.setAfterBala(tLLPrepaidGrpPolSchema
							.getPrepaidBala());
					tLLPrepaidGrpPolSchema.setSumPay(Arith.round(
							tLLPrepaidGrpPolSchema.getSumPay()
									+ tLLPrepaidTraceSchema.getMoney(), 2));
					tLLPrepaidGrpPolSchema.setBalaDate(tLLPrepaidTraceSchema
							.getBalaDate());
					tLLPrepaidGrpPolSchema.setBalaTime(tLLPrepaidTraceSchema
							.getBalaTime());
					tLLPrepaidGrpPolSchema.setOperator(tLLPrepaidTraceSchema
							.getOperator());
					tLLPrepaidGrpPolSchema.setModifyDate(tLLPrepaidTraceSchema
							.getModifyDate());
					tLLPrepaidGrpPolSchema.setModifyTime(tLLPrepaidTraceSchema
							.getModifyTime());
					break;
				}
			}

			for (int m = 1; m <= mLLPrepaidGrpContSet.size(); m++) {
				LLPrepaidGrpContSchema tLLPrepaidGrpContSchema = mLLPrepaidGrpContSet
						.get(m);
				if (tLLPrepaidGrpContSchema.getGrpContNo().equals(
						tLLPrepaidTraceSchema.getGrpContNo())) {
					tLLPrepaidGrpContSchema.setPrepaidBala(Arith.round(
							tLLPrepaidGrpContSchema.getPrepaidBala()
									+ tLLPrepaidTraceSchema.getMoney(), 2));
					tLLPrepaidGrpContSchema.setSumPay(Arith.round(
							tLLPrepaidGrpContSchema.getSumPay()
									+ tLLPrepaidTraceSchema.getMoney(), 2));
					tLLPrepaidGrpContSchema.setBalaDate(tLLPrepaidTraceSchema
							.getBalaDate());
					tLLPrepaidGrpContSchema.setBalaTime(tLLPrepaidTraceSchema
							.getBalaTime());
					tLLPrepaidGrpContSchema.setOperator(tLLPrepaidTraceSchema
							.getOperator());
					tLLPrepaidGrpContSchema.setModifyDate(tLLPrepaidTraceSchema
							.getModifyDate());
					tLLPrepaidGrpContSchema.setModifyTime(tLLPrepaidTraceSchema
							.getModifyTime());
					break;
				}
			}
		}

		// 扣除的预付赔款金额
		double tSumMoney = 0;

		for (int l = 1; l <= mLLPrepaidTraceSet.size(); l++) {
			LLPrepaidTraceSchema tLLPrepaidTraceSchema = mLLPrepaidTraceSet
					.get(l);
			if (tLLPrepaidTraceSchema.getMoney() != 0) {
				mBackMsg += "<br>保单" + tLLPrepaidTraceSchema.getGrpContNo()
						+ "下险种" + tLLPrepaidTraceSchema.getRiskCode()
						+ ":预付赔款余额" + tLLPrepaidTraceSchema.getLastBala()
						+ ",本次回销预付赔款" + -tLLPrepaidTraceSchema.getMoney();

				LJAGetClaimSchema tLJAGetClaimSchema = new LJAGetClaimSchema();
				tLJAGetClaimSchema
						.setActuGetNo(aLJAGetSchema.getActuGetNo());
				tLJAGetClaimSchema.setFeeFinaType("YF");
				tLJAGetClaimSchema.setFeeOperationType("000");
				tLJAGetClaimSchema.setOtherNo(aLJAGetSchema.getOtherNo());
				tLJAGetClaimSchema.setOtherNoType("Y");
				tLJAGetClaimSchema.setGetDutyKind("000");
				tLJAGetClaimSchema.setGrpContNo(tLLPrepaidTraceSchema
						.getGrpContNo());
				tLJAGetClaimSchema.setContNo("00000" + l);
				tLJAGetClaimSchema.setGrpPolNo(tLLPrepaidTraceSchema
						.getGrpPolNo());
				tLJAGetClaimSchema.setPolNo("00000" + l);

				LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
				tLMRiskAppDB.setRiskCode(tLLPrepaidTraceSchema.getRiskCode());

				if (!tLMRiskAppDB.getInfo()) {
					CError.buildErr(this, "险种信息查询失败");
					return false;
				}

				tLJAGetClaimSchema.setRiskCode(tLMRiskAppDB.getRiskCode());
				tLJAGetClaimSchema.setRiskVersion(tLMRiskAppDB.getRiskVer());
				tLJAGetClaimSchema.setKindCode(tLMRiskAppDB.getKindCode());

				LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
				tLCGrpPolDB.setGrpPolNo(tLLPrepaidTraceSchema.getGrpPolNo());
				if (!tLCGrpPolDB.getInfo()) {
					LBGrpPolDB tLBGrpPolDB = new LBGrpPolDB();
					tLBGrpPolDB
							.setGrpPolNo(tLLPrepaidTraceSchema.getGrpPolNo());
					if (!tLBGrpPolDB.getInfo()) {
						CError.buildErr(this, tLLPrepaidTraceSchema
								.getGrpContNo()
								+ "保单查询失败");
						return false;
					}
					tLJAGetClaimSchema.setSaleChnl(tLBGrpPolDB.getSaleChnl());
					tLJAGetClaimSchema.setAgentCode(tLBGrpPolDB.getAgentCode());
					tLJAGetClaimSchema.setAgentGroup(tLBGrpPolDB
							.getAgentGroup());
					tLJAGetClaimSchema.setAgentCom(tLBGrpPolDB.getAgentCom());
					tLJAGetClaimSchema.setManageCom(tLBGrpPolDB.getManageCom());
					tLJAGetClaimSchema.setAgentType(tLBGrpPolDB.getAgentType());

				} else {
					tLJAGetClaimSchema.setSaleChnl(tLCGrpPolDB.getSaleChnl());
					tLJAGetClaimSchema.setAgentCode(tLCGrpPolDB.getAgentCode());
					tLJAGetClaimSchema.setAgentGroup(tLCGrpPolDB
							.getAgentGroup());
					tLJAGetClaimSchema.setAgentCom(tLCGrpPolDB.getAgentCom());
					tLJAGetClaimSchema.setManageCom(tLCGrpPolDB.getManageCom());
					tLJAGetClaimSchema.setAgentType(tLCGrpPolDB.getAgentType());
				}

				tLJAGetClaimSchema.setPay(tLLPrepaidTraceSchema.getMoney());
				tLJAGetClaimSchema.setOPConfirmCode(mGlobalInput.Operator);
				tLJAGetClaimSchema.setOPConfirmDate(aLJAGetSchema
						.getMakeDate());
				tLJAGetClaimSchema.setOPConfirmTime(aLJAGetSchema
						.getMakeTime());
				tLJAGetClaimSchema.setOperator(mGlobalInput.Operator);
				tLJAGetClaimSchema.setMakeDate(aLJAGetSchema.getMakeDate());
				tLJAGetClaimSchema.setMakeTime(aLJAGetSchema.getMakeTime());
				tLJAGetClaimSchema
						.setModifyDate(aLJAGetSchema.getMakeDate());
				tLJAGetClaimSchema
						.setModifyTime(aLJAGetSchema.getMakeTime());

				tSumMoney = Arith.round(
						tSumMoney + tLJAGetClaimSchema.getPay(), 2);

				aLJAGetClaimSet.add(tLJAGetClaimSchema);
			}
		}

		if (tSumMoney != 0) {
			aLJAGetSchema.setSumGetMoney(Arith.round(aLJAGetSchema
					.getSumGetMoney()
					+ tSumMoney, 2));
			mBackMsg += "<br>批次实际给付" + aLJAGetSchema.getSumGetMoney();
		}

		if (mLLPrepaidTraceSet.size() > 0) {
			map.put(mLLPrepaidTraceSet, "INSERT");
			map.put(mLLPrepaidGrpPolSet, "UPDATE");
			map.put(mLLPrepaidGrpContSet, "UPDATE");
		}

		return true;
	}

    public static void main(String[] args) {
        LLRegisterSchema tSchema = new LLRegisterSchema();
        tSchema.setRgtNo("P9400060904000001");

        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86";
        mGlobalInput.ComCode = "8611";
        mGlobalInput.Operator = "claim";
        VData aVData = new VData();
        aVData.addElement(mGlobalInput);
        aVData.add(tSchema);
        QDGrpGiveEnsureBL tBL = new QDGrpGiveEnsureBL();
        tBL.submitData(aVData, "QDGIVE|ENSURE");
    }
}
