package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class GrpContRiskAmntModifyBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 往后面传输的数据库操作 */
    private MMap map = new MMap();
    /** 执行删除的数据库操作，放在最后 */
    private MMap mapDel = new MMap();
    /**用户登陆信息 */
    private GlobalInput mGlobalInput = new GlobalInput();
    //团体保单险种
    private LCPolSchema mLCPolSchema = new LCPolSchema();

    private LCPolSet mLCPolSet = new LCPolSet();

    //团体保单号
    private String mGrpContNo = "";
    //险种代码
    private String mRiskCode = "";
    //保障计划
    private String mContPlanCode = "";
    //保额
    private double mRiskAmnt = 0.0;

    public GrpContRiskAmntModifyBL() {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * @param: cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        System.out.println("after getInputData");

        //数据操作业务处理
        if (!dealData()) {
            return false;
        }

        System.out.println("after dealData");

        //准备给后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        System.out.println("after prepareOutputData");

        //数据提交
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpContRiskAmntModifyBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;

        return true;
    }

    private boolean dealData() {
        if ("UPDATE".equals(mOperate)) {
            LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
            tLCGrpPolDB.setGrpContNo(mGrpContNo);
            tLCGrpPolDB.setRiskCode(mRiskCode);
            if (tLCGrpPolDB.query().size() <= 0) {
                CError tError = new CError();
                tError.moduleName = "GrpContRiskAmntModifyBL";
                tError.functionName = "getBaseData";
                tError.errorMessage = "团体保单" + mGrpContNo + "查询失败";
                mErrors.addOneError(tError);
                return false;
            }

            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setGrpContNo(mGrpContNo);
            tLCPolDB.setRiskCode(mRiskCode);
            tLCPolDB.setContPlanCode(mContPlanCode);
            mLCPolSet.add(tLCPolDB.query());
            if (mLCPolSet.size() <= 0) {
                CError tError = new CError();
                tError.moduleName = "GrpContRiskAmntModifyBL";
                tError.functionName = "getBaseData";
                tError.errorMessage = "团单" + mGrpContNo + "下个人保单查询失败";
                mErrors.addOneError(tError);
                return false;
            }

            System.out.println("保单" + mGrpContNo + "下险种" + mRiskCode + "险种保额置为"+mRiskAmnt);
            System.out.println("Operator:" + mGlobalInput.Operator);

            for (int i = 1; i <= mLCPolSet.size(); i++) {
                mLCPolSet.get(i).setRiskAmnt(mRiskAmnt);
                mLCPolSet.get(i).setModifyDate(PubFun.getCurrentDate());
                mLCPolSet.get(i).setModifyTime(PubFun.getCurrentTime());
                mLCPolSet.get(i).setOperator(mGlobalInput.Operator);
            }

            map.put(mLCPolSet, "UPDATE");
        }
        return true;
    }


    private boolean getInputData(VData cInputData) {
        mGlobalInput.setSchema(
                (GlobalInput)
                cInputData.getObjectByObjectName("GlobalInput", 0));

        mLCPolSchema.setSchema(
                (LCPolSchema)
                cInputData.getObjectByObjectName("LCPolSchema", 0));
        mGrpContNo = mLCPolSchema.getGrpContNo();

        if (mGrpContNo == null || mGrpContNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "GrpContRiskAmntModifyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "保单号空";
            mErrors.addOneError(tError);
            return false;
        }

        mRiskCode = mLCPolSchema.getRiskCode();
        if (mRiskCode == null || mRiskCode.equals("")) {
            CError tError = new CError();
            tError.moduleName = "GrpContRiskAmntModifyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "险种号空";
            mErrors.addOneError(tError);
            return false;
        }

        mContPlanCode = mLCPolSchema.getContPlanCode();
        if (mContPlanCode == null || mContPlanCode.equals("")) {
            CError tError = new CError();
            tError.moduleName = "GrpContRiskAmntModifyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "保障计划空";
            mErrors.addOneError(tError);
            return false;
        }

        try {
            mRiskAmnt = mLCPolSchema.getRiskAmnt();
        } catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "GrpContRiskAmntModifyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "保额录入错误";
            mErrors.addOneError(tError);
            return false;
        }
        if (mRiskAmnt < 0) {
            CError tError = new CError();
            tError.moduleName = "GrpContRiskAmntModifyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "保额不能为负值";
            mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpContRiskAmntModifyBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错:" +
                                  ex.toString();
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {

    }


}
