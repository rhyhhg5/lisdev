/*
 * @(#)ICaseCureBL.java	2005-02-20
 *
 * Copyright 2005 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.llcase;

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;

/*
 * <p>Title: Web业务系统</p>
 * <p>ClassName:ICaseCureBL </p>
 * <p>Description: 理赔案件-账单录入 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft </p>
 * @author：zhangtao
 * @version：1.0
 * @CreateDate：2005-02-20
 */
public class LLClaimPopedomBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private MMap map = new MMap();
    private String mOperate = "";

    //全局数据
    private GlobalInput mGlobalInput = new GlobalInput();
    //账单费用明细
    private LLClaimPopedomSet mLLClaimPopedomSet = new LLClaimPopedomSet();

    public LLClaimPopedomBL()
    {
    }

    public static void main(String[] args)
    {

        LLClaimPopedomBL tLLClaimPopedomBL   = new LLClaimPopedomBL();
        LLInqFeeSchema tLLInqFeeSchema = new LLInqFeeSchema();
        LLInqFeeSet tLLInqFeeSet = new LLInqFeeSet();
        tLLInqFeeSchema.setFeeItem("01");
        tLLInqFeeSchema.setSurveyNo("86110000000001");
        tLLInqFeeSchema.setFeeSum("213");
        tLLInqFeeSchema.setOtherNo("C1100050601000004");
        tLLInqFeeSchema.setOtherNoType("1");
        tLLInqFeeSchema.setContSN("1");
        tLLInqFeeSchema.setInqDept("86");
        tLLInqFeeSet.add(tLLInqFeeSchema);
        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "claim";
        tGI.ManageCom = "86";
        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tLLInqFeeSet);
    tLLClaimPopedomBL.submitData(tVData,"INSERT");
        //用于调试
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData 输入数据
     * @param cOperate String 操作类型
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        mOperate=cOperate;

        //得到输入数据
        if (!getInputData())
        {
            return false;
        }

        //检查数据合法性
        if (!checkInputData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData(cOperate))
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLInqFeeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有数据对象
     * @return boolean
     */
    private boolean getInputData()
    {
        System.out.println("getInputData()..."+mOperate);

        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLLClaimPopedomSet = (LLClaimPopedomSet) mInputData.getObjectByObjectName(
                "LLClaimPopedomSet", 0);
        System.out.println(mLLClaimPopedomSet.size());
        if (mLLClaimPopedomSet == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLInqFeeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认有：直接费用信息!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 对输入数据做必要的校验
     * @return boolean
     */
    private boolean checkInputData()
    {
        System.out.println("checkInputData()..."+mOperate);
        String tClaimPopedom = "";
       tClaimPopedom+= mLLClaimPopedomSet.get(1).getClaimPopedom();
        if (tClaimPopedom.trim().equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "LLClaimPopedomBL";
            tError.functionName = "checkInputData";
            tError.errorMessage = "理赔师权限级别不能为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else{
            LLClaimPopedomDB tLLClaimPopedomDB = new LLClaimPopedomDB();
            tLLClaimPopedomDB.setClaimPopedom(tClaimPopedom);
            tLLClaimPopedomDB.setGetDutyKind("1");
            tLLClaimPopedomDB.setGetDutyType("1");
            if (tLLClaimPopedomDB.getInfo()) {
                if (mOperate.equals("INSERT")) {
                    CError tError = new CError();
                    tError.moduleName = "LLClaimPopedomBL";
                    tError.functionName = "checkInputData";
                    tError.errorMessage = "系统中已有该级别的权限设置，您可以选择修改!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            } else {
                if (mOperate.equals("UPDATE")) {
                    CError tError = new CError();
                    tError.moduleName = "LLClaimPopedomBL";
                    tError.functionName = "checkInputData";
                    tError.errorMessage = "系统中没有该级别的权限设置，无法进行修改操作，请选择新增!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (mOperate.equals("DELETE")) {
                    CError tError = new CError();
                    tError.moduleName = "LLClaimPopedomBL";
                    tError.functionName = "checkInputData";
                    tError.errorMessage = "系统中没有该级别的权限设置!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @param cOperate String 操作类型
     * @return boolean
     */
    private boolean dealData(String cOperate)
    {
        System.out.println("dealData()..."+mOperate);

        boolean tReturn = false;

        //保存录入
        if (mOperate.equals("INSERT"))
        {
            map.put(mLLClaimPopedomSet, "INSERT");
            mResult.add(mLLClaimPopedomSet);
            tReturn = true;
        }

        //修改录入
        if (mOperate.equals("UPDATE"))
        {
            //查询记录，保留创建时间

            String sbSql = "";
            sbSql =
            " DELETE FROM LLClaimPopedom  WHERE ClaimPopedom = '"
            + mLLClaimPopedomSet.get(1).getClaimPopedom() +"'";
            map.put(sbSql, "DELETE");
            map.put(mLLClaimPopedomSet, "INSERT");
            mResult.add(mLLClaimPopedomSet);
            tReturn = true;

        }

        //删除录入
        if (cOperate.equals("DELETE"))
        {
            String sbSql = "";
            sbSql =
            " DELETE FROM LLClaimPopedom  WHERE ClaimPopedom = '"
            + mLLClaimPopedomSet.get(1).getClaimPopedom() +"'";

            map.put(sbSql, "DELETE");
            tReturn = true;
        }

        return tReturn;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        System.out.println("prepareOutputData()...");

        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLClaimPopedomBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 得到处理后的结果数据
     * @return boolean
     */
    public VData getResult()
    {
        return this.mResult;
    }

}
