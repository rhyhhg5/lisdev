package com.sinosoft.lis.llcase;

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 案件－立案－分案保单明细保存功能业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @version 1.0
 */
public class CasePolicyBL
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData ;
  private VData mResult = new VData();
  private String mOperate;
  private String CustomerNo = "";
  private LLCasePolicySet mLLCasePolicySet = new LLCasePolicySet();
  private LCPolSet mLCPolSet = new LCPolSet();
  private LBPolSet mLBPolSet = new LBPolSet();
  private LCPolSchema mLCPolSchema = new LCPolSchema();
  private LBPolSchema mLBPolSchema = new LBPolSchema();
  private String RgtNo="";
  private String CaseNo = "";
  private String tRptNo = "";


  private LLCasePolicySchema mLLCasePolicySchema = new LLCasePolicySchema();
  private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
  String strAccidentDate="";
  private GlobalInput mG = new GlobalInput();
  public CasePolicyBL() {}

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;

    if (!getInputData(cInputData))
      return false;

    if (cOperate.equals("INSERT"))
    {
      //通过查询分案号码来查询出立案号码，在以立案号码为索引条件在LLRegister表中查询出案件状态
      //来进行判断。
      LLCasePolicySchema aLLCasePolicySchema = new LLCasePolicySchema();
//      if(mLLCasePolicySet.size()==0)
//      {
//        System.out.println("个数是0则直接将该分案的所有的保单信息进行删除!!!!");
//        CasePolicyBLS tCasePolicyBLS = new CasePolicyBLS();
//        if (!tCasePolicyBLS.submitData(mInputData,"DELETE"))
//        {
//          // @@错误处理
//          this.mErrors.copyAllErrors(tCasePolicyBLS.mErrors);
//          CError tError = new CError();
//          tError.moduleName = "CasePolicyBL";
//          tError.functionName = "submitData";
//          tError.errorMessage = "数据删除失败！！！";
//          this.mErrors .addOneError(tError) ;
//          return false;
//        }
//        mInputData.clear();
//        mInputData.addElement(CustomerNo);
//        mInputData.addElement(CaseNo);
//      }
      if(mLLCasePolicySet.size()>0)
      {
//        aLLCasePolicySchema.setSchema(mLLCasePolicySet.get(1));
//        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
//        tLLRegisterDB.setRgtNo(aLLCasePolicySchema.getRgtNo());
//        if(!tLLRegisterDB.getInfo())
//          return false;
        //对正处于调查中的案件进行控制
        //记录该客户的出险日期
//        this.mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
//        strAccidentDate = tLLRegisterDB.getAccidentDate();
//        tRptNo = tLLRegisterDB.getRptNo();
//
//        if(!(tLLRegisterDB.getClmState()==null||tLLRegisterDB.getClmState().equals("")))
//        {
//          System.out.println("案件状态是"+tLLRegisterDB.getClmState());
//          if(tLLRegisterDB.getClmState().equals("5"))
//          {
//            this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "RegisterUpdateBL";
//            tError.functionName = "submitData";
//            tError.errorMessage = "该案件正处于调查之中，您无法进行保存操作！！";
//            this.mErrors .addOneError(tError) ;
//            return false;
//          }
//          //对结案的案件进行控制
//          if(tLLRegisterDB.getEndCaseFlag().equals("2"))
//          {
//            this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "RegisterUpdateBL";
//            tError.functionName = "submitData";
//            tError.errorMessage = "该案件已经结案，您无法进行保存操作！！";
//            this.mErrors .addOneError(tError) ;
//            return false;
//          }
//        }
        if (!dealData())
          return false;
      }
    }

    if(cOperate.equals("INIT"))
    {
      if(!initData())
        return false;
    }
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }
  //先要调用prepareOutputData()函数
  private boolean dealData()
  {
    if (! dealInsert() ) return false;
    PubSubmit ps = new PubSubmit();
    if (! ps.submitData(this.mResult,null ))
    {
        CError.buildErr(this, "保存数据失败");
        return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    if(mOperate.equals("INSERT"))
    {
      mLLCasePolicySet.set((LLCasePolicySet)cInputData.getObjectByObjectName("LLCasePolicySet",0));
      mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
      CustomerNo=(String)cInputData.get(0);
      CaseNo    =(String)cInputData.get(1);
     // System.out.println("2003-8-7分案号码是"+CaseNo);
    }
//    if(mOperate.equals("INIT"))
//    {
//      RgtNo  = (String)cInputData.get(0);
//      CaseNo = (String)cInputData.get(1);
//    }
    return true;
  }

  private boolean dealInsert()
  {
    int n=mLLCasePolicySet.size();
    LLCasePolicySet yLLCasePolicySet= new LLCasePolicySet();
    LLCaseDB tLLCaseDB = new LLCaseDB();
    tLLCaseDB.setCaseNo(CaseNo);
    if (!tLLCaseDB.getInfo() )
    {
        CError.buildErr(this,"没有查询到理赔案件信息");
        return false;
    }
    String tdate = PubFun.getCurrentDate();
          String ttime = PubFun.getCurrentTime() ;
    try
    {
      for(int i=1;i<=n;i++)
      {
        //对出险日期和保单生效日期进行判断
        String polno = mLLCasePolicySet.get(i).getPolNo();
        LLCasePolicySchema  tLLCasePolicySchema = mLLCasePolicySet.get(i);
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setPolNo(polno);
        if(!tLCPolBL.getInfo())
        {
          this.mErrors.copyAllErrors(tLCPolBL.mErrors);
          CError tError = new CError();
          tError.moduleName = "CasePolicyBL";
          tError.functionName = "submitData";
          tError.errorMessage = "保单表及备份表中没有该条保单记录，系统数据出错！！！";
          this.mErrors .addOneError(tError) ;
          return false;
        }
        System.out.println("出险日期是"+strAccidentDate);
        System.out.println("生效日期是"+tLCPolBL.getCValiDate());

        if(!(strAccidentDate==null||strAccidentDate.equals("")))
        {
          if(!(CaseFunPub.checkDate(tLCPolBL.getCValiDate(),strAccidentDate)))
          {
            this.mErrors.copyAllErrors(tLCPolBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "CasePolicyBL";
            tError.functionName = "submitData";
            tError.errorMessage = "保单号码是"+tLCPolBL.getPolNo()+"的出险日期在保单生效日期之前，该操作被取消！！！";
            this.mErrors .addOneError(tError) ;
            return false;
          }
        }


          tLLCasePolicySchema.setCaseNo(tLLCaseDB.getCaseNo());
        tLLCasePolicySchema.setRgtNo(tLLCaseDB.getRgtNo());
          tLLCasePolicySchema.setOperator( this.mG.Operator );
          tLLCasePolicySchema.setMngCom( this.mG.ManageCom );
          tLLCasePolicySchema.setMakeDate(tdate);
          tLLCasePolicySchema.setModifyDate( tdate );
          tLLCasePolicySchema.setMakeTime(ttime);
          tLLCasePolicySchema.setModifyTime( ttime  );


        //CasePolType的附值根据LLReport表中的CustomerType来进行附值
//        LLReportDB tLLReportDB = new LLReportDB();
//        tLLReportDB.setRptNo(this.mLLRegisterSchema.getRptNo());
//        tLLReportDB.getInfo();
        System.out.println("now being ...");
//        if((tLCPolBL.getInsuredNo().equals(tLCPolBL.getAppntNo()))||
//           tLCPolBL.getInsuredNo().equals(CustomerNo))
//        {
//          System.out.println("begin the judgement");
//          if (tLLReportDB.getCustomerType().equals("2")||tLLReportDB.getCustomerType().equals("3")) //连带被保险人
//            tLLCasePolicySchema.setCasePolType("2");
//          else
//            tLLCasePolicySchema.setCasePolType("0");
//        }
        if((tLCPolBL.getInsuredNo().equals(CustomerNo)) )
      {
        tLLCasePolicySchema.setCasePolType("0");
      }else
        if(tLCPolBL.getAppntNo().equals(CustomerNo))
        {
          tLLCasePolicySchema.setCasePolType("1");
        }


        tLLCasePolicySchema.setGrpContNo(tLCPolBL.getGrpContNo());
        tLLCasePolicySchema.setContNo(tLCPolBL.getContNo());
        tLLCasePolicySchema.setGrpPolNo(tLCPolBL.getGrpPolNo());
        tLLCasePolicySchema.setPolNo(mLLCasePolicySet.get(i).getPolNo());
        tLLCasePolicySchema.setKindCode(tLCPolBL.getKindCode());
        tLLCasePolicySchema.setRiskCode(tLCPolBL.getRiskCode());
        tLLCasePolicySchema.setRiskVer(tLCPolBL.getRiskVersion());
        tLLCasePolicySchema.setPolMngCom(tLCPolBL.getManageCom());
        tLLCasePolicySchema.setSaleChnl(tLCPolBL.getSaleChnl());
        tLLCasePolicySchema.setAgentCode(tLCPolBL.getAgentCode());
        tLLCasePolicySchema.setAgentGroup(tLCPolBL.getAgentGroup());
        tLLCasePolicySchema.setInsuredNo(tLCPolBL.getInsuredNo());
        tLLCasePolicySchema.setInsuredName(tLCPolBL.getInsuredName());
        tLLCasePolicySchema.setInsuredSex(tLCPolBL.getInsuredSex());
        tLLCasePolicySchema.setInsuredBirthday(tLCPolBL.getInsuredBirthday());
        tLLCasePolicySchema.setAppntNo(tLCPolBL.getAppntNo());
        tLLCasePolicySchema.setAppntName(tLCPolBL.getAppntName());
        tLLCasePolicySchema.setCValiDate(tLCPolBL.getCValiDate());
     //   tLLCasePolicySchema.setPolState(CaseFunPub.getCaseStateByRgtNo(tLLCaseDB.getRgtNo()));

        tLLCasePolicySchema.setPolType("1");//正式保单
       // yLLCasePolicySet.add(tLLCasePolicySchema);
      }
      MMap tmpMap = new MMap();
      String delSql ="delete from llcasepolicy where caseno='"+ this.CaseNo +"'";
      tmpMap.put( delSql , "DELETE");
      tmpMap.put( this.mLLCasePolicySet ,"INSERT");
      this.mResult.add( tmpMap );
//      mInputData.clear();
//      mInputData.add(yLLCasePolicySet);
     return true;
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      CError.buildErr(this,"准备数据出错:"+ ex.getMessage());
      return false;
    }

  }

  /*****************************************************************************
   * Name     :queryData()
   * Function :初始化分案保单明细信息
   * Author   :LiuYansong
   * Date     :2003-8-6
   * 所使用到的字段的说明
   * RiskVer------主/附加险信息
   * ContNo-------MainPolNo
   * GrpPolNo-----被保人/投保人/即是被保人又是投保人
   * AgentCode----销售渠道01团险！01个险
   * AppntNo------投保人姓名
   * AppntName----险种名称
   * ClmNo--------保单效力开始日期
   * DeclineNo----保单效力终止日期
   * MngCom-------出单机构
   * AgentGroup---金额
   */

  private boolean initData()
  {
    String sql = "Select * from LLCasePolicy where RgtNo = '"+RgtNo+"' and CaseNo = '"+CaseNo+"' and PolType='1' ";
    System.out.println("执行的sql语句是"+sql);
    LLCasePolicySet tLLCasePolicySet = new LLCasePolicySet();
    LLCasePolicySet rLLCasePolicySet = new LLCasePolicySet();
    LLCasePolicyDB tLLCasePolicyDB = new LLCasePolicyDB();
    tLLCasePolicySet.set(tLLCasePolicyDB.executeQuery(sql));
    if(tLLCasePolicySet.size()>0)
    {
      for(int i=1;i<=tLLCasePolicySet.size();i++)
      {
        LLCasePolicySchema tLLCasePolicySchema = new LLCasePolicySchema();
        LLCasePolicySchema rLLCasePolicySchema = new LLCasePolicySchema();
        tLLCasePolicySchema.setSchema(tLLCasePolicySet.get(i));
        rLLCasePolicySchema.setSchema(tLLCasePolicySet.get(i));
        String sql_mainpolno = "select Mainpolno,Amnt,SignCom from lcpol where polno = '"+tLLCasePolicySchema.getPolNo()+"'";
        ExeSQL exesql = new ExeSQL();
        SSRS ssrs = exesql.execSQL(sql_mainpolno);
        if(ssrs.getMaxRow()==0)
        {
          String sql_b =  "select Mainpolno,Amnt,SignCom from lbpol where polno = '"+tLLCasePolicySchema.getPolNo()+"'";
          SSRS ssrs_b = exesql.execSQL(sql_b);
          if(ssrs_b.getMaxRow()>0)
          {
            rLLCasePolicySchema.setContNo(ssrs_b.GetText(1,1));//获取MainPolNo
            rLLCasePolicySchema.setAgentGroup(ssrs_b.GetText(1,2));
            rLLCasePolicySchema.setMngCom(ssrs_b.GetText(1,3));
            if(ssrs_b.GetText(1,1).equals(tLLCasePolicySchema.getPolNo()))
            {
              rLLCasePolicySchema.setRiskVer("主险");
            }
            else
              rLLCasePolicySchema.setRiskVer("附加险");
          }
        }
        if(ssrs.getMaxRow()>0)
        {
          rLLCasePolicySchema.setContNo(ssrs.GetText(1,1));//获取MainPolNo
          rLLCasePolicySchema.setAgentGroup(ssrs.GetText(1,2));
          rLLCasePolicySchema.setMngCom(ssrs.GetText(1,3));
          if(ssrs.GetText(1,1).equals(tLLCasePolicySchema.getPolNo()))
          {
            rLLCasePolicySchema.setRiskVer("主险");
          }
          else
            rLLCasePolicySchema.setRiskVer("附加险");
        }

        if(tLLCasePolicySchema.getCasePolType().equals("1"))
        {
          rLLCasePolicySchema.setGrpPolNo("投保人");
        }
        if(tLLCasePolicySchema.getCasePolType().equals("0"))
        {
          rLLCasePolicySchema.setGrpPolNo("被保人");
        }
        if(tLLCasePolicySchema.getSaleChnl().equals("01"))
        {
          rLLCasePolicySchema.setAgentCode("团险");
        }
        if(!tLLCasePolicySchema.getSaleChnl().equals("01"))
        {
          rLLCasePolicySchema.setAgentCode("个险");
        }
        System.out.println("销售渠道是"+rLLCasePolicySchema.getAgentCode());
        rLLCasePolicySchema.setAppntNo(tLLCasePolicySchema.getAppntName());
        String sql_riskname = "select RiskName from LMRisk where RiskCode = '"+tLLCasePolicySchema.getRiskCode()+"'";
        SSRS ssrs_2 = exesql.execSQL(sql_riskname);
        if(ssrs_2.getMaxRow()>0)
        {
          rLLCasePolicySchema.setAppntName(ssrs_2.GetText(1,1));
        }
        String tState[] = new String[4];
        tState = CaseFunPub.getPolState(tLLCasePolicySchema.getPolNo());
        rLLCasePolicySchema.setClmNo(tState[2]);
//        rLLCasePolicySchema.setDeclineNo(tState[3]);
        System.out.println("开始日期是"+rLLCasePolicySchema.getClmNo());
//        System.out.println("结束日期是"+rLLCasePolicySchema.getDeclineNo());
        rLLCasePolicySet.add(rLLCasePolicySchema);

      }
    }
    mResult.clear();
    mResult.addElement(rLLCasePolicySet);
    return true;
  }
  public static void main(String[] args)
  {
    LLCasePolicySet aLLCasePolicySet = new LLCasePolicySet();
    LLCasePolicySchema aLLCasePolicySchema = new LLCasePolicySchema();
    String tRgtNo  = "P0000050609000003";
    String tCaseNo = "C0000050609000001";
    String tPolNo = "21000000538";
    VData tVData = new VData();
//    tVData.addElement(tRgtNo);
//    tVData.addElement(tCaseNo);
//    tVData.addElement(tPolno);
    aLLCasePolicySchema.setCaseNo(tCaseNo);
    aLLCasePolicySchema.setRgtNo(tRgtNo);
    aLLCasePolicySchema.setPolNo(tPolNo);
    aLLCasePolicySchema.setCaseRelaNo("86000000000071");
    aLLCasePolicySet.add(aLLCasePolicySchema);
    CasePolicyBL aCasePolicyBL = new CasePolicyBL();
    String aCustomerNo = "000003312";
    GlobalInput mGlobalInput = new GlobalInput();
    mGlobalInput.ManageCom = "86";
    mGlobalInput.ComCode = "86";
    mGlobalInput.Operator = "ocm001";
    tVData.add(aCustomerNo);
    tVData.addElement(tCaseNo);
    tVData.add(aLLCasePolicySet);
    tVData.addElement(mGlobalInput);
    aCasePolicyBL.submitData(tVData,"INSERT");
  }
}
