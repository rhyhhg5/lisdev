package com.sinosoft.lis.llcase;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;

import com.sinosoft.lis.pubfun.PubFun1;
public class OutsourcingCostUpload {
	
	public List checkGrpcontnoList(InputStream inputStream){
		
		List grpConnoList = new ArrayList();
		
		try{
				//			处理输入流
			Workbook workbook = Workbook.getWorkbook(inputStream);
			Sheet sheet = workbook.getSheet(0);// 获取第一个sheet
		     int rows = sheet.getRows();   //获取总行号
		     for (int i = 1; i < rows; i++) {// 遍历行获得每行信息
			     String grpContno = sheet.getCell(1, i).getContents();// 获得第i行第2列信息
			     grpContno = OutsourcingCostUpload.full2HalfChange(grpContno);
			     
			     //去掉空格和全角后，如果不为空则加入到grpConnoList
			     
			     if("".equals(grpContno)||grpContno==null){
			    	 
			     }else{
			    	 System.out.println("============"+grpContno+"========");
			    	 grpConnoList.add(grpContno);
			     }
		     }
		}catch(Exception e){
			e.printStackTrace();
		}
		  return grpConnoList;
	
	}
	
	
	
	/** 
     *  半角全角转换及替换半角全角空白 
     */  
	public static final String full2HalfChange(String QJstr) throws UnsupportedEncodingException  
    {  
	StringBuffer outStrBuf = new StringBuffer("");  
	String Tstr = "";  
	byte[] b = null;  
	for (int i = 0; i < QJstr.length(); i++) {  
	    Tstr = QJstr.substring(i, i + 1);  
	    // 全角空格转换成半角空格  
	    if (Tstr.equals("　")) {  
	        outStrBuf.append(" ");  
	        continue;  
	    }  
	    b = Tstr.getBytes("unicode");  
	    // 得到 unicode 字节数据  
	    if (b[2] == -1) {  
	        b[3] = (byte) (b[3] + 32);  
	        b[2] = 0;  
	        outStrBuf.append(new String(b, "unicode"));  
	    } else {  
	        outStrBuf.append(Tstr);  
	    }  
		} // end for.  
		Tstr = outStrBuf.toString().replaceAll("(^[ |　]*|[ |　]*$)", "");  
		Tstr = Tstr.replaceAll("　", ""); 
		return Tstr.toString();  
	
	}  
	


}


/*  这是另一种方式-----备用
 * 
/*
private static final String PARSE_PATH = "/DATASET/CASETABLE/ROW";


public List polnoList(String xmlFileName){
	
	
//  解析保单信息
	List polnoList = new ArrayList();
	
    XMLPathTool xmlPT = new XMLPathTool(xmlFileName);
    
    NodeList nodeList = xmlPT.parseN(PARSE_PATH);
    
    
    
    System.out.println("长度 ： " + nodeList.getLength());
    for (int i = 0; i < nodeList.getLength(); i++) {
        Node node = nodeList.item(i);
        
        NodeList contNodeList = node.getChildNodes();
        
        
        System.out.println("长度 ： " + contNodeList.getLength());
        
        
        if (contNodeList.getLength() <= 0)
            continue;
        for (int j = 0; j < contNodeList.getLength(); j++) {

            Node contNode = contNodeList.item(j);
            String nodeName = contNode.getNodeName();
            
          
            if (nodeName.equals("#text")) {
            	
            }
            //解析保单号
            else if (("polno").equals(nodeName)) {
            	System.out.println("444"+contNode.getTextContent());
            	
            }
        }
    }
    
    //解析完成删除xml临时文件
    File xmlFile = new File(xmlFileName);
    xmlFile.delete();
    return polnoList;3
    
 
String name = item.getName();

System.out.println("name : " + name);
long size = item.getSize();

if((name==null||name.equals("")) && size==0)
  continue;

ImportPath= path+ImportPath ;

FileName = name.substring(name.lastIndexOf("\\") + 1);
System.out.println("-----------importpath."+ImportPath + FileName);
File file = new File(ImportPath + FileName);
if (file.exists()) {
    break;
}



SIXmlParser rgtvp = new SIXmlParser();
rgtvp.setFileName(ImportPath+File.separator+FileName);
rgtvp.setConfigFileName(ImportPath+File.separator+"polno.xml");
System.out.println("1"+ImportPath+File.separator+FileName+"2"+ImportPath+File.separator+"polno.xml");
//转换excel到xml
rgtvp.transform();

String[] xmlFiles = rgtvp.getDataFiles();

System.out.println("xmlFiles[0]"+xmlFiles[0]);

new OutsourcingCostUpload().polnoList(xmlFiles[0]);

*/
