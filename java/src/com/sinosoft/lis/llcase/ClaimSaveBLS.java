package com.sinosoft.lis.llcase;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统投保暂交费功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author YT
 * @version 1.0
 */

public class ClaimSaveBLS
{
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();
  private String mOperate;

  public ClaimSaveBLS() {}


  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;
    this.mOperate = cOperate;
    System.out.print("save claimsavebls data begin");
    if (mOperate.equals("DELETE"))
    {
      if (!this.deleteData())
      return false;
    }
    else
    {
      if (!this.saveData())
      return false;
    }
    System.out.println("End ClaimSave BLS Submit...");
    mInputData=null;
    return true;
  }

  private boolean deleteData()
  {
    	LLClaimSchema tLLClaimSchema = new LLClaimSchema();
        tLLClaimSchema = (LLClaimSchema)mInputData.getObjectByObjectName("LLClaimSchema",0);
        Connection conn = null;
    	conn = DBConnPool.getConnection();
    	if (conn==null)
    	{
      		// @@错误处理
      		CError tError = new CError();
      		tError.moduleName = "ClaimSaveBLS";
      		tError.functionName = "saveData";
      		tError.errorMessage = "数据库连接失败!";
      		this.mErrors .addOneError(tError) ;
      		return false;
    	}
        try
    	{
      		conn.setAutoCommit(false);
			// 保存现在的关联
		LLClaimDB tLLClaimDB = new LLClaimDB( conn );
		tLLClaimDB.setSchema(tLLClaimSchema);
	        if (tLLClaimDB.delete() == false)
		{
				// @@错误处理
		  	this.mErrors.copyAllErrors(tLLClaimDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "ClaimSaveBLS";
			tError.functionName = "saveData";
			tError.errorMessage = "删除数据失败!";
			this.mErrors .addOneError(tError) ;
	       		conn.rollback() ;
			return false;
		}
                LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB( conn );
		tLLClaimPolicyDB.setClmNo(tLLClaimSchema.getClmNo());
		if (tLLClaimPolicyDB.deleteSQL() == false)
		{
				// @@错误处理
			this.mErrors.copyAllErrors(tLLClaimPolicyDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "ClaimSaveBLS";
			tError.functionName = "saveData";
			tError.errorMessage = "删除数据失败!";
			this.mErrors .addOneError(tError) ;
	       		conn.rollback() ;
			return false;
                }

                LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB( conn );
		tLLClaimDetailDB.setClmNo(tLLClaimSchema.getClmNo());
		if (tLLClaimDetailDB.deleteSQL() == false)
		{
				// @@错误处理
			this.mErrors.copyAllErrors(tLLClaimDetailDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "ClaimSaveBLS";
			tError.functionName = "saveData";
			tError.errorMessage = "删除数据失败!";
			this.mErrors .addOneError(tError) ;
	       		conn.rollback() ;
			return false;
                }

                LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB( conn );
                tLJSGetClaimDB.setOtherNo(tLLClaimSchema.getClmNo());
                tLJSGetClaimDB.setOtherNoType("5");
                if (tLJSGetClaimDB.deleteSQL() == false)
                {
                                // @@错误处理
                        this.mErrors.copyAllErrors(tLJSGetClaimDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "ClaimSaveBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "删除数据失败!";
                        this.mErrors .addOneError(tError) ;
                               conn.rollback() ;
                        return false;
                }

                LJSGetDB tLJSGetDB = new LJSGetDB( conn );
                tLJSGetDB.setOtherNo(tLLClaimSchema.getClmNo());
                tLJSGetDB.setOtherNoType("5");
                if (tLJSGetDB.deleteSQL() == false)
                {
                                // @@错误处理
                        this.mErrors.copyAllErrors(tLJSGetDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "ClaimSaveBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "删除数据失败!";
                        this.mErrors .addOneError(tError) ;
                               conn.rollback() ;
                        return false;
                }

		conn.commit() ;
      }
      catch (Exception ex)
	{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ClaimSaveBLS";
			tError.functionName = "submitData";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);
			try{conn.rollback() ;} catch(Exception e){}
			return false;
	}
    return true;

  }



  private boolean saveData()
  {
  	LLClaimSchema tLLClaimSchema = new LLClaimSchema();
  	LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();
  	LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
        LJSGetClaimSet tLJSGetClaimSet = new LJSGetClaimSet();
        LJSGetSchema tLJSGetSchema = new LJSGetSchema();
        LLRegisterSchema tLLRegisterSchema= new LLRegisterSchema();

 	tLLClaimSchema = (LLClaimSchema)mInputData.getObjectByObjectName("LLClaimSchema",0);
    	tLLClaimPolicySet = (LLClaimPolicySet)mInputData.getObjectByObjectName("LLClaimPolicySet",0);
    	tLLClaimDetailSet = (LLClaimDetailSet)mInputData.getObjectByObjectName("LLClaimDetailSet",0);
        tLJSGetClaimSet = (LJSGetClaimSet)mInputData.getObjectByObjectName("LJSGetClaimSet",0);
        tLJSGetSchema = (LJSGetSchema)mInputData.getObjectByObjectName("LJSGetSchema",0);
        tLLRegisterSchema = (LLRegisterSchema)mInputData.getObjectByObjectName("LLRegisterSchema",0);


    	Connection conn = null;
    	conn = DBConnPool.getConnection();
    	if (conn==null)
    	{
      		// @@错误处理
      		CError tError = new CError();
      		tError.moduleName = "ClaimSaveBLS";
      		tError.functionName = "saveData";
      		tError.errorMessage = "数据库连接失败!";
      		this.mErrors .addOneError(tError) ;
      		return false;
    	}

    	try
    	{
      		conn.setAutoCommit(false);
			// 保存现在的关联
		LLClaimDB tLLClaimDB = new LLClaimDB( conn );
		tLLClaimDB.setSchema(tLLClaimSchema);
		System.out.println("insert or update llclaim begin...");
		if (mOperate == "INSERT")
		{
			if (tLLClaimDB.insert() == false)
			{
				// @@错误处理
			  	this.mErrors.copyAllErrors(tLLClaimDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "ClaimSaveBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "保存数据失败!";
				this.mErrors .addOneError(tError) ;
	       			conn.rollback() ;
	       			conn.close();
				return false;
			}
		}
		else
		{
			if (tLLClaimDB.update() == false)
			{
				// @@错误处理
			  	this.mErrors.copyAllErrors(tLLClaimDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "ClaimSaveBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "保存数据失败!";
				this.mErrors .addOneError(tError) ;
	       			conn.rollback() ;
	       			conn.close();
				return false;
			}

		}

      		System.out.println("insert or update llclaimpolicy begin....");
      		if (mOperate == "UPDATE")
		{
			LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB(conn);
                        tLLClaimPolicyDB.setClmNo(tLLClaimSchema.getClmNo());
                        if (tLLClaimPolicyDB.deleteSQL() == false)
			{
				// @@错误处理
				this.mErrors.copyAllErrors(tLLClaimPolicyDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "ClaimSaveBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "删除数据失败!";
				this.mErrors .addOneError(tError) ;
	       			conn.rollback() ;
	       			conn.close();
				return false;
			}
		}
                LLClaimPolicyDBSet tLLClaimPolicyDBSet = new LLClaimPolicyDBSet( conn );
                tLLClaimPolicyDBSet.set(tLLClaimPolicySet);

                if (tLLClaimPolicyDBSet.insert() == false)
		{
				// @@错误处理
				this.mErrors.copyAllErrors(tLLClaimPolicyDBSet.mErrors);
				CError tError = new CError();
				tError.moduleName = "ClaimSaveBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "保存数据失败!";
				this.mErrors .addOneError(tError) ;
	       			conn.rollback() ;
	       			conn.close();
				return false;
		}

		System.out.println("insert or update llclaimdetail begin....");
      		if (mOperate == "UPDATE")
		{
			LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB(conn);
                        tLLClaimDetailDB.setClmNo(tLLClaimSchema.getClmNo());
                        if (tLLClaimDetailDB.deleteSQL() == false)
			{
				// @@错误处理
				this.mErrors.copyAllErrors(tLLClaimDetailDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "ClaimSaveBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "删除数据失败!";
				this.mErrors .addOneError(tError) ;
	       			conn.rollback() ;
	       			conn.close();
				return false;
			}
		}
                LLClaimDetailDBSet tLLClaimDetailDBSet = new LLClaimDetailDBSet( conn );
                tLLClaimDetailDBSet.set(tLLClaimDetailSet);

		if (tLLClaimDetailDBSet.insert() == false)
		{
				// @@错误处理
				this.mErrors.copyAllErrors(tLLClaimDetailDBSet.mErrors);
				CError tError = new CError();
				tError.moduleName = "ClaimSaveBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "保存数据失败!";
				this.mErrors .addOneError(tError) ;
	       			conn.rollback() ;
	       			conn.close();
				return false;
		}


                System.out.println("insert or update LJSGetClaim begin....");
                if (mOperate == "UPDATE")
                {
                        LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB(conn);
                        tLJSGetClaimDB.setOtherNo(tLLClaimSchema.getClmNo());
                        if (tLJSGetClaimDB.deleteSQL() == false)
                        {
                                // @@错误处理
                                this.mErrors.copyAllErrors(tLJSGetClaimDB.mErrors);
                                CError tError = new CError();
                                tError.moduleName = "ClaimSaveBLS";
                                tError.functionName = "saveData";
                                tError.errorMessage = "删除数据失败!";
                                this.mErrors .addOneError(tError) ;
                                conn.rollback() ;
                                conn.close();
                                return false;
                        }
                }

                LJSGetClaimDBSet tLJSGetClaimDBSet = new LJSGetClaimDBSet( conn );
                tLJSGetClaimDBSet.set(tLJSGetClaimSet);

                if (tLJSGetClaimDBSet.insert() == false)
                {
                                // @@错误处理
                                this.mErrors.copyAllErrors(tLJSGetClaimDBSet.mErrors);
                                CError tError = new CError();
                                tError.moduleName = "ClaimSaveBLS";
                                tError.functionName = "saveData";
                                tError.errorMessage = "保存数据失败!";
                                this.mErrors .addOneError(tError) ;
                                 conn.rollback() ;
                                 conn.close();
                                return false;
                }


                System.out.println("insert or update LJSGet begin....");
                //System.out.println("GetNoticeNo=="+tLJSGetSchema.getGetNoticeNo());
                LJSGetDB tLJSGetDB = new LJSGetDB( conn );
                if (tLJSGetSchema!=null)
                {
                  tLJSGetDB.setSchema(tLJSGetSchema);
                if (mOperate == "UPDATE")
                {

                        if (tLJSGetDB.delete() == false)
                        {
                                // @@错误处理
                                this.mErrors.copyAllErrors(tLJSGetDB.mErrors);
                                CError tError = new CError();
                                tError.moduleName = "ClaimSaveBLS";
                                tError.functionName = "saveData";
                                tError.errorMessage = "删除数据失败!";
                                this.mErrors .addOneError(tError) ;
                                conn.rollback() ;
                                conn.close();
                                return false;
                        }
                }


                if (tLJSGetDB.insert() == false)
                {
                                // @@错误处理
                                this.mErrors.copyAllErrors(tLJSGetDB.mErrors);
                                CError tError = new CError();
                                tError.moduleName = "ClaimSaveBLS";
                                tError.functionName = "saveData";
                                tError.errorMessage = "保存数据失败!";
                                this.mErrors .addOneError(tError) ;
                                 conn.rollback() ;
                                 conn.close();
                                return false;
                }
                }
                LLRegisterDB tLLRegisterDB = new LLRegisterDB();
                tLLRegisterDB.setSchema(tLLRegisterSchema);
                if (tLLRegisterDB.update() == false)
                {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLJSGetDB.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "ClaimSaveBLS";
                  tError.functionName = "saveData";
                  tError.errorMessage = "保存数据失败!";
                  this.mErrors .addOneError(tError) ;
                  conn.rollback() ;
                  conn.close();
                  return false;

                }

		conn.commit();
		conn.close();
		} // end of try
	catch (Exception ex)
	{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ClaimSaveBLS";
			tError.functionName = "submitData";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);
			try{conn.rollback() ;} catch(Exception e){}

			return false;
	}
    return true;
  }

}