package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LLPrepaidGrpContInputBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	/** 数据操作字符串 */
	private String mOperate;

	/** 业务处理相关变量 */
	private LLPrepaidGrpContSchema mLLPrepaidGrpContSchema = new LLPrepaidGrpContSchema();

	private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

	// 团体保单号
	private String mGrpContNo = "";

	private String mOperator = "";

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();
	
    /**社保案件标记 0：非社保；1：社保*/
    private String mSocialFlag = "0";

	private MMap mMap = new MMap();

	public LLPrepaidGrpContInputBL() {

	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = cInputData;
		this.mOperate = cOperate;
		System.out.println(mOperate);

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}

		if (!prepareOutputData()) {
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start LLPrepaidGrpContInputBL Submit...");

		if (!tPubSubmit.submitData(mInputData, null)) {
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}

		mInputData = null;
		System.out.println("End LLPrepaidGrpContInputBL Submit...");

		return true;

	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * 
	 */
	private boolean getInputData() {
		if ("".equals(mOperate)) {
			buildError("getInputData", "获取操作类型失败");
			return false;
		}

		if (mInputData == null) {
			buildError("getInputData", "获取页面传输数据失败");
			return false;
		}

		mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0);

		if (mGlobalInput == null) {
			buildError("getInputData", "获取操作人信息失败");
			return false;
		}

		mOperator = mGlobalInput.Operator;

		if ("".equals(mOperator)) {
			buildError("getInputData", "获取操作人信息失败");
			return false;
		}

		LLPrepaidGrpContSchema tLLPrepaidGrpContSchema = (LLPrepaidGrpContSchema) mInputData
				.getObjectByObjectName("LLPrepaidGrpContSchema", 0);

		if (tLLPrepaidGrpContSchema == null) {
			buildError("getInputData", "获取保单数据失败");
			return false;
		}

		mGrpContNo = tLLPrepaidGrpContSchema.getGrpContNo();

		if ("".equals(mGrpContNo)) {
			buildError("getInputData", "获取保单数据失败");
			return false;
		}

		return true;
	}

	private boolean checkData() {
		//#1962 社保业务预付赔款功能需求分析
		//1.首先判断保单是否为社保保单
        mSocialFlag = LLCaseCommon.checkSocialSecurity(mGrpContNo, "grpno");
        //2.社保保单，只能由社保用户“确认预付保单”
        if("1".equals(mSocialFlag)){
        	LLSocialClaimUserDB tSocialClaimUserDB = new LLSocialClaimUserDB();
        	LLSocialClaimUserSet tSocialClaimUserSet = new LLSocialClaimUserSet();
        	//预付保单确认时操作人必须为在“社保业务理赔人员权限配置”中进行配置的用户，且所属机构为分公司
        	String tSSQL = "select * from llsocialclaimuser where 1=1 and" +
        			" stateflag='1' and usercode ='"+mOperator+"' with ur ";
        	System.out.println("tSSQL:"+tSSQL);
        	tSocialClaimUserSet = tSocialClaimUserDB.executeQuery(tSSQL);
        	if(tSocialClaimUserSet.size() <= 0){
        		// @@错误处理
    			buildError("checkData", 
    					"当前登录用户"+mOperator+"未在社保业务理赔人员权限配置，不能对社保保单进行‘预付保单确认’或‘预付保单撤销’！");
    			return false;
        	}
        	if(tSocialClaimUserSet.get(1).getComCode().length() != 4){
        		//@@错误处理
    			buildError("checkData", 
    					"预付保单确认只能由分公司用户操作！");
    			return false;

        	}
        }else{
        	LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
    		tLLClaimUserDB.setUserCode(mOperator);
    		if (!tLLClaimUserDB.getInfo()) {
    			buildError("checkData", "请使用理赔用户操作");
    			return false;
    		}

    		if (tLLClaimUserDB.getComCode().length() != 4) {
    			buildError("checkData", "预付保单确认只能由分公司用户操作");
    			return false;
    		}

        }
		
		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setGrpContNo(mGrpContNo);
		if (!tLCGrpContDB.getInfo()) {
			buildError("checkData", "查询保单信息失败");
			return false;
		}

		mLCGrpContSchema = tLCGrpContDB.getSchema();

		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {

		if ("GrpCont||Confirm".equals(mOperate)) {
			if (!confirm()) {
				return false;
			}
		} else if ("GrpCont||Cancel".equals(mOperate)) {
			if (!cancel()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 保单预付确认
	 * 
	 * @return
	 */
	private boolean confirm() {
		if (!"1".equals(mLCGrpContSchema.getAppFlag())) {
			buildError("dealData", "保单必须承保有效");
			return false;
		}

		if (!"1".equals(mLCGrpContSchema.getStateFlag())&& !"3".equals(mLCGrpContSchema.getStateFlag())
				&& !"2".equals(mLCGrpContSchema.getStateFlag())) {
			buildError("dealData", "保单非有效状态");
			return false;
		}

		LLPrepaidGrpContDB tLLPrepaidGrpContDB = new LLPrepaidGrpContDB();
		tLLPrepaidGrpContDB.setGrpContNo(mGrpContNo);
		if (tLLPrepaidGrpContDB.getInfo()) {
			mLLPrepaidGrpContSchema = tLLPrepaidGrpContDB.getSchema();

			String tState = mLLPrepaidGrpContSchema.getState();

			if ("1".equals(tState)) {
				buildError("dealData", "保单已经预付确认");
				return false;
			}

			mLLPrepaidGrpContSchema.setRegister(mOperator);
			mLLPrepaidGrpContSchema.setState("1");
			mLLPrepaidGrpContSchema.setOperator(mOperator);
			mLLPrepaidGrpContSchema.setModifyDate(mCurrentDate);
			mLLPrepaidGrpContSchema.setModifyTime(mCurrentTime);

			LLPrepaidGrpPolSet tLLPrepaidGrpPolSet = new LLPrepaidGrpPolSet();
			LLPrepaidGrpPolDB tLLPrepaidGrpPolDB = new LLPrepaidGrpPolDB();
			tLLPrepaidGrpPolDB.setGrpContNo(mGrpContNo);

			tLLPrepaidGrpPolSet = tLLPrepaidGrpPolDB.query();

			if (tLLPrepaidGrpPolSet.size() <= 0) {
				buildError("dealData", "预付保单下险种查询失败");
				return false;
			}

			LLPrepaidGrpPolSet tULLPrepaidGrpPolSet = new LLPrepaidGrpPolSet();

			for (int i = 1; i <= tLLPrepaidGrpPolSet.size(); i++) {
				LLPrepaidGrpPolSchema tLLPrepaidGrpPolSchema = tLLPrepaidGrpPolSet
						.get(i);
				tLLPrepaidGrpPolSchema.setState("1");
				tLLPrepaidGrpPolSchema.setOperator(mOperator);
				tLLPrepaidGrpPolSchema.setModifyDate(mCurrentDate);
				tLLPrepaidGrpPolSchema.setModifyTime(mCurrentTime);

				tULLPrepaidGrpPolSet.add(tLLPrepaidGrpPolSchema);
			}

			mMap.put(mLLPrepaidGrpContSchema, "UPDATE");
			mMap.put(tULLPrepaidGrpPolSet, "UPDATE");

		} else {
			mLLPrepaidGrpContSchema.setGrpContNo(mGrpContNo);
			mLLPrepaidGrpContSchema.setFoundDate(mCurrentDate);
			mLLPrepaidGrpContSchema.setFoundTime(mCurrentTime);
			mLLPrepaidGrpContSchema.setPrepaidBala(0);
			mLLPrepaidGrpContSchema.setApplyAmount(0);
			mLLPrepaidGrpContSchema.setRegainAmount(0);
			mLLPrepaidGrpContSchema.setSumPay(0);
			mLLPrepaidGrpContSchema.setRegister(mOperator);
			mLLPrepaidGrpContSchema.setState("1");
			mLLPrepaidGrpContSchema.setManageCom(mGlobalInput.ManageCom);
			mLLPrepaidGrpContSchema.setOperator(mOperator);
			mLLPrepaidGrpContSchema.setMakeDate(mCurrentDate);
			mLLPrepaidGrpContSchema.setMakeTime(mCurrentTime);
			mLLPrepaidGrpContSchema.setModifyDate(mCurrentDate);
			mLLPrepaidGrpContSchema.setModifyTime(mCurrentTime);

			LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
			tLCGrpPolDB.setGrpContNo(mGrpContNo);

			LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();

			if (tLCGrpPolSet.size() <= 0) {
				buildError("dealData", "保单下险种查询失败");
				return false;
			}

			LLPrepaidGrpPolSet tLLPrepaidGrpPolSet = new LLPrepaidGrpPolSet();

			for (int i = 1; i <= tLCGrpPolSet.size(); i++) {

				Reflections tReflections = new Reflections();

				LLPrepaidGrpPolSchema tLLPrepaidGrpPolSchema = new LLPrepaidGrpPolSchema();
				LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolSet.get(i);

				tReflections.transFields(tLLPrepaidGrpPolSchema,
						mLLPrepaidGrpContSchema);
				tLLPrepaidGrpPolSchema.setGrpPolNo(tLCGrpPolSchema
						.getGrpPolNo());
				tLLPrepaidGrpPolSchema.setRiskCode(tLCGrpPolSchema
						.getRiskCode());

				tLLPrepaidGrpPolSet.add(tLLPrepaidGrpPolSchema);
			}

			mMap.put(mLLPrepaidGrpContSchema, "INSERT");
			mMap.put(tLLPrepaidGrpPolSet, "INSERT");
		}
		return true;
	}

	/**
	 * 预付保单撤销
	 * 
	 * @return
	 */
	private boolean cancel() {
		LLPrepaidGrpContDB tLLPrepaidGrpContDB = new LLPrepaidGrpContDB();
		tLLPrepaidGrpContDB.setGrpContNo(mGrpContNo);
		if (!tLLPrepaidGrpContDB.getInfo()) {
			buildError("dealData", "预付保单查询失败");
			return false;
		}
		mLLPrepaidGrpContSchema = tLLPrepaidGrpContDB.getSchema();

		if (!mOperator.equals(mLLPrepaidGrpContSchema.getRegister())
				&& !LLCaseCommon.checkUPUpUser(mOperator,
						mLLPrepaidGrpContSchema.getRegister())) {
			buildError("dealData", "操作人无撤销权限");
			return false;
		}

		String tState = mLLPrepaidGrpContSchema.getState();

		if ("0".equals(tState)) {
			buildError("dealData", "保单已经预付撤销");
			return false;
		}

		if (mLLPrepaidGrpContSchema.getPrepaidBala() > 0
				|| mLLPrepaidGrpContSchema.getApplyAmount() > 0
				|| mLLPrepaidGrpContSchema.getRegainAmount() > 0
				|| mLLPrepaidGrpContSchema.getSumPay() > 0) {
			buildError("dealData", "保单已经使用预付赔款，不能撤销");
			return false;
		}

		String tPrepaidSQL = "SELECT COUNT(1) FROM llprepaidclaim where rgtstate<>'07' and grpcontno='"
				+ mGrpContNo + "'";
		ExeSQL tExeSQl = new ExeSQL();
		System.out.println(tPrepaidSQL);
		String tNum = tExeSQl.getOneValue(tPrepaidSQL);

		if (Integer.parseInt(tNum) > 0) {
			buildError("dealData", "保单已经使用预付赔款，不能撤销");
			return false;
		}

		// 修改预付保单状态
		mLLPrepaidGrpContSchema.setState("0");
		mLLPrepaidGrpContSchema.setCanceler(mOperator);
		mLLPrepaidGrpContSchema.setCancelDate(mCurrentDate);
		mLLPrepaidGrpContSchema.setCancelTime(mCurrentTime);
		mLLPrepaidGrpContSchema.setOperator(mOperator);
		mLLPrepaidGrpContSchema.setModifyDate(mCurrentDate);
		mLLPrepaidGrpContSchema.setModifyTime(mCurrentTime);

		// 修改预付保单险种状态
		LLPrepaidGrpPolSet tLLPrepaidGrpPolSet = new LLPrepaidGrpPolSet();
		LLPrepaidGrpPolDB tLLPrepaidGrpPolDB = new LLPrepaidGrpPolDB();
		tLLPrepaidGrpPolDB.setGrpContNo(mGrpContNo);

		tLLPrepaidGrpPolSet = tLLPrepaidGrpPolDB.query();

		if (tLLPrepaidGrpPolSet.size() <= 0) {
			buildError("dealData", "预付保单下险种查询失败");
			return false;
		}

		LLPrepaidGrpPolSet tULLPrepaidGrpPolSet = new LLPrepaidGrpPolSet();

		for (int i = 1; i <= tLLPrepaidGrpPolSet.size(); i++) {
			LLPrepaidGrpPolSchema tLLPrepaidGrpPolSchema = tLLPrepaidGrpPolSet
					.get(i);
			tLLPrepaidGrpPolSchema.setState("0");
			tLLPrepaidGrpPolSchema.setOperator(mOperator);
			tLLPrepaidGrpPolSchema.setModifyDate(mCurrentDate);
			tLLPrepaidGrpPolSchema.setModifyTime(mCurrentTime);

			tULLPrepaidGrpPolSet.add(tLLPrepaidGrpPolSchema);
		}

		mMap.put(mLLPrepaidGrpContSchema, "UPDATE");
		mMap.put(tULLPrepaidGrpPolSet, "UPDATE");
		return true;
	}

	private boolean prepareOutputData() {
		mInputData.clear();
		mInputData.add(mMap);
		mResult = mInputData;
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	/**
	 * 追加错误信息
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLPrepaidGrpContBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {

	}
}
