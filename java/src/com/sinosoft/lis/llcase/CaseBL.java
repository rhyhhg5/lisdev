package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class CaseBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  /**  */
  private LLCaseBL  mLLCaseBL = new LLCaseBL();
  private LLCaseBLSet mLLCaseBLSet = new LLCaseBLSet();

  public CaseBL() {}

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;

    //数据查询业务处理
    if (cOperate.equals("QUERY||MAIN"))
    {
      if(!queryData())
	return false;
      System.out.println("---queryData---");
    }
    // 数据操作业务处理
    if (cOperate.equals("INSERT"))
    {
      if (!dealData())
	return false;
      System.out.println("---dealData---");
    }
    if(cOperate.equals("UPDATE"))
    {
    	if(!updateData())
    		return false;
    	System.out.println("---updateData---");
    }
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealData()
  {
		//准备给后台的数据
    prepareOutputData();
    //数据提交
    CaseBLS tCaseBLS = new CaseBLS();
    System.out.println("Start LLCase BL Submit...");
    if (!tCaseBLS.submitData(mInputData,mOperate))
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tCaseBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "CaseBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {

    // 暂交费查询条件
    if (mOperate.equals("QUERY||MAIN"))
    {
      mLLCaseBL.setSchema((LLCaseSchema)cInputData.getObjectByObjectName("LLCaseSchema",0));
    }
    if (mOperate.equals("INSERT")||mOperate.equals("UPDATE"))
    {
      mLLCaseBLSet.set((LLCaseSet)cInputData.getObjectByObjectName("LLCaseSet",0));
    }

    return true;

  }

  /**
   * 查询符合条件的暂交费信息
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean queryData()
  {

    LLCaseDB tLLCaseDB = new LLCaseDB();
    tLLCaseDB.setRgtNo( mLLCaseBL.getRgtNo());

    mLLCaseBLSet.set(tLLCaseDB.query());
    if (tLLCaseDB.mErrors.needDealError() == true)
    {
      // @@错误处理
	  this.mErrors.copyAllErrors(tLLCaseDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "LLCaseBL";
      tError.functionName = "queryData";
      tError.errorMessage = "保单查询失败!";
      this.mErrors.addOneError(tError);
      mLLCaseBLSet.clear();
      return false;
    }
    if (mLLCaseBLSet.size() == 0)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LLCaseBL";
      tError.functionName = "queryData";
      tError.errorMessage = "未找到相关数据!";
      this.mErrors.addOneError(tError);
      mLLCaseBLSet.clear();
      return false;
	}

	mResult.clear();
	mResult.add( mLLCaseBLSet );

	return true;

  }

  private boolean updateData()
  {

    LLCaseDB tLLCaseDB = new LLCaseDB();
    LLCaseSchema tLLCaseSchema=new LLCaseSchema();
    tLLCaseSchema=mLLCaseBLSet.get(1);
    mLLCaseBL.setSchema(tLLCaseSchema);
    mLLCaseBL.setDefaultValue();
    tLLCaseSchema=mLLCaseBL.getSchema();
		tLLCaseDB.setSchema(tLLCaseSchema);
    tLLCaseDB.update();
    if (tLLCaseDB.mErrors.needDealError() == true)
    {
      // @@错误处理
	  this.mErrors.copyAllErrors(tLLCaseDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "LLCaseBL";
      tError.functionName = "updateData";
      tError.errorMessage = "更新失败!";
      this.mErrors.addOneError(tError);
      mLLCaseBLSet.clear();
      return false;
    }
    if (mLLCaseBLSet.size() == 0)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LLCaseBL";
      tError.functionName = "queryData";
      tError.errorMessage = "未找到相关数据!";
      this.mErrors.addOneError(tError);
      mLLCaseBLSet.clear();
      return false;
	}

	mResult.clear();
	mResult.add( mLLCaseBLSet );

	return true;

  }

  /**
   * 校验传入的暂交费收据号是否合法
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkData()
  {

	boolean flag = true;
	return flag;

  }

  /**
   * 准备需要保存的数据
   */
  private void prepareOutputData()
  {
    int n = mLLCaseBLSet.size();
    for (int i = 1; i <= n; i++)
    {
      LLCaseSchema tLLCaseSchema = mLLCaseBLSet.get(i);
      tLLCaseSchema.setCustomerNo("customer"+i);
      tLLCaseSchema.setMngCom("ddd");
      tLLCaseSchema.setOperator("ddd");
      tLLCaseSchema.setMakeDate(PubFun.getCurrentDate());
      tLLCaseSchema.setMakeTime(PubFun.getCurrentTime());
      tLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
      tLLCaseSchema.setModifyTime(PubFun.getCurrentTime());
      mLLCaseBLSet.set(i,tLLCaseSchema);
    }

    mInputData.clear();
    mInputData.add(mLLCaseBLSet);
    mResult.clear();
    mResult.add(mLLCaseBLSet);

  }

}