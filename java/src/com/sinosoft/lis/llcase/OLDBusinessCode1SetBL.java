/*
 * <p>ClassName: OLDBusinessCode1SetBL </p>
 * <p>Description: OLDBusinessCode1SetBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-26 13:18:17
 */
package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLDBusinessCode1SetBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LDCode1Set mLDCode1Set = new LDCode1Set();
    private String mCodeType = "";
    private String zdywlx = "";//重点业务类型
    private String grpcontno = "";//团体保单号
    private MMap map = new MMap();

    public OLDBusinessCode1SetBL() {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLDCode1SetBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLDCode1SetBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("Start OLDBusinessCode1SetBL Submit...");

        PubSubmit tPubSubmit = new PubSubmit();

        if (!tPubSubmit.submitData(mInputData, cOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "LDPersonBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("End OLDCode1SetBL Submit...");

        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        boolean tReturn = false;
        System.out.println("After getInputData");
        System.out.println(mOperate);
        //处理个人信息数据
        //添加纪录
        if (mOperate.equals("INSERT")) {

            map.put(mLDCode1Set, "INSERT");
            tReturn = true;
        }

        if (mOperate.equals("UPDATE")) {
        	if(zdywlx==null||zdywlx.equals(""))
        	{
        		CError tError = new CError();
                tError.moduleName = "OLDCode1SetBL";
                tError.functionName = "dealData";
                tError.errorMessage = "重点业务类型为空!";
                this.mErrors.addOneError(tError);
                return false;
        	}
        	if(grpcontno==null||grpcontno.equals(""))
        	{
        		CError tError = new CError();
                tError.moduleName = "OLDCode1SetBL";
                tError.functionName = "dealData";
                tError.errorMessage = "团体保单号为空!";
                this.mErrors.addOneError(tError);
                return false;
        	}
        	LDCode1DB tLDCode1DB = new LDCode1DB();
        	tLDCode1DB.setCodeType(mLDCode1Set.get(1).getCodeType());
            tLDCode1DB.setCode1(mLDCode1Set.get(1).getCode1());
            tLDCode1DB.setCode(mLDCode1Set.get(1).getCode());//add by zzh 101111 这样就可以新增同一保单不同重点业务的记录
            if(tLDCode1DB.getInfo())
            {
            	CError tError = new CError();
                tError.moduleName = "OLDCode1SetBL";
                tError.functionName = "dealData";
                tError.errorMessage = "该保单已经存在此重点业务类型!";
                this.mErrors.addOneError(tError);
                return false;
            }
            map.put("update LDCode1 set Code='"+mLDCode1Set.get(1).getCode()
            		+"',Code1='"+mLDCode1Set.get(1).getCode1()
            		+"',CodeName='"+mLDCode1Set.get(1).getCodeName()
            		+"' where code='"+zdywlx+"' and code1='"+grpcontno+"' and codetype='llgrptype'", "UPDATE");
            tReturn = true;
        }

        if (mOperate.equals("DELETE")) {
            map.put(mLDCode1Set, "DELETE");
            tReturn = true;
        }

        if (mOperate.equals("DELETE&INSERT")) {
            LDCode1Set tLDCode1Set = new LDCode1Set();
            for (int i=1;i<=mLDCode1Set.size();i++) {
                LDCode1DB tLDCode1DB = new LDCode1DB();
                tLDCode1DB.setCodeType(mLDCode1Set.get(i).getCodeType());
                tLDCode1DB.setCode1(mLDCode1Set.get(i).getCode1());
                tLDCode1DB.setCode(mLDCode1Set.get(i).getCode());//add by zzh 101111 这样就可以新增同一保单不同重点业务的记录
                LDCode1Set aLDCode1Set = tLDCode1DB.query();
                for (int j=1;j<=aLDCode1Set.size();j++) {
                    LDCode1Schema tLDCode1Schema = aLDCode1Set.get(j);
                    tLDCode1Set.add(tLDCode1Schema);
                }
            }
            map.put(tLDCode1Set, "DELETE");
            map.put(mLDCode1Set, "INSERT");
            tReturn = true;
        }

        return tReturn;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLDCode1Set.set((LDCode1Set) cInputData.
                             getObjectByObjectName("LDCode1Set", 0));
        //this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        this.zdywlx=(String)((TransferData) cInputData.getObjectByObjectName("TransferData", 0)).getValueByName("zdywlx");
        this.grpcontno=(String)((TransferData) cInputData.getObjectByObjectName("TransferData", 0)).getValueByName("grpcontno");
        if (mLDCode1Set == null) {
            buildError("getInputData", "获取分类业务信息失败");
            return false;
        }

        if (mLDCode1Set.size() <= 0) {
            buildError("getInputData", "获取分类业务信息失败");
            return false;
        }

        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLDCode1Set);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDCode1SetBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "OLDCode1SetBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

}
