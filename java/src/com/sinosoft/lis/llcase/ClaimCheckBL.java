package com.sinosoft.lis.llcase;

import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 理赔赔案保单核赔业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author：zhangtao
 * @version：1.0
 * @CreateDate：2005-02-20
 */
public class ClaimCheckBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();

  /** 核赔员代码 */
  private String mOperator;
  /** 赔案名细 */
  private LLClaimPolicySchema mLLClaimPolicySchema = new LLClaimPolicySchema();
  /** 赔案主表 */
  private LLClaimSchema mLLClaimSchema = new LLClaimSchema();
  /**用户表信息 */
  private LLClaimUserSchema mLLClaimUserSchema = new LLClaimUserSchema();
  /**用户登陆信息 */
  private GlobalInput mGlobalInput = new GlobalInput();

  private TransferData mTransferData = new TransferData();
  private LLCaseSet mLLCaseSet = new LLCaseSet();
  private LDSpotTrackSet mLDSpotTrackSet = new LDSpotTrackSet();
  private MMap map = new MMap();

  public ClaimCheckBL() {}

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   * @param: cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone();

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
    {
        return false;
    }

    System.out.println("after getInputData");

    //根据赔案号、保单号查询保单、赔案、核赔、核赔错误、用户权限信息
    if (!getBaseData())
    {
        return  false;
    }
    System.out.println("after getBaseData");

    //数据操作业务处理
    if (!dealData())
    {
        return false;
    }
    System.out.println("after dealData");

    //准备给后台的数据
    if (!prepareOutputData())
    {
       return false;
    }

System.out.println("after prepareOutputData");

    //数据提交
    //数据提交
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, "")) {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        CError tError = new CError();
        tError.moduleName = "ClaimCheckBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";
        this.mErrors.addOneError(tError);
        return false;
    }
    mInputData = null;
    System.out.println("BL ending.....");

    return true;
  }

  private boolean dealData()
  {
    String tClaimPopedom = (String) mTransferData.getValueByName("ClaimPopedom");
    String MngCom = ""+(String) mTransferData.getValueByName("MngCom");
    String Claimer = ""+(String) mTransferData.getValueByName("Claimer");
    String AgentCode = ""+(String) mTransferData.getValueByName("AgentCode");
    String DealWith = ""+(String) mTransferData.getValueByName("DealWith");
    String GiveType = ""+(String) mTransferData.getValueByName("GiveType");
    String Rate = ""+(String) mTransferData.getValueByName("Rate");
    String RgtDateS = ""+(String) mTransferData.getValueByName("RgtDateS");
    String RgtDateE = ""+(String) mTransferData.getValueByName("RgtDateE");
    String OpTime = ""+(String) mTransferData.getValueByName("OpTime");
    String Query = ""+(String) mTransferData.getValueByName("Query");
    String AccTimeS = ""+(String) mTransferData.getValueByName("AccTimeS");
    String AccDateE = ""+(String) mTransferData.getValueByName("AccDateE");
    String strSQL = "select distinct a.caseno from llcase a,llclaimdetail b,llclaim c "
            + " where rgtstate in ('11','12') and b.caseno=a.caseno and c.caseno = a.caseno ";
    if(!MngCom.equals(""))
        strSQL += " and a.MngCom = '"+MngCom+"' ";
    if(!Claimer.equals(""))
        strSQL += " and a.handler = '"+Claimer+"' ";
    if(!AgentCode.equals(""))
        strSQL += " and b.agentcode = '"+AgentCode+"' ";
    if(!DealWith.equals(""))
        strSQL += " and a.rgttype = '"+DealWith+"' ";
    if(!GiveType.equals(""))
        strSQL += " and c.givetype = '"+GiveType+"' ";
    if(!Query.equals(""))
        strSQL += " and a.surveyflag = '"+Query+"' ";
    if(!RgtDateS.equals(""))
        strSQL += " and a.rgtdate >= '"+RgtDateS+"' ";
    if(!RgtDateE.equals(""))
        strSQL += " and a.rgtdate <= '"+RgtDateE+"' ";
    ExeSQL exesql = new ExeSQL();
    SSRS tss = exesql.execSQL(strSQL);
    int trate = 100;
    if (!Rate.equals("")){
        trate = Integer.parseInt(Rate);
    }
    if (tss != null && tss.getMaxRow() > 0)
    {
        for(int k = 1;k<=tss.getMaxRow();k++){
            String[] rowdata = tss.getRowData(k);
            String mCaseNo = rowdata[0];
            SpotCheck tspotcheck = new SpotCheck();
            //由于改用返回数据集的方式，因此在抽取的时候不会出现错误
            boolean tFlag = tspotcheck.RandomRate(mCaseNo, "30000", trate);
            //无论抽中与否，都需要取得返回的数据集
            if(tFlag){
                LDSpotTrackSchema tLDSpotTrackSchema = tspotcheck.getLDSpotTrackSet().get(1);
                tLDSpotTrackSchema.setSpotType("2");//2表示为稽查
                mLDSpotTrackSet.add(tLDSpotTrackSchema);
                LLCaseDB tLLCaseDB = new LLCaseDB();
                tLLCaseDB.setCaseNo(mCaseNo);
                if(tLLCaseDB.getInfo()){
                    LLCaseSchema tLLCaseSchema = tLLCaseDB.getSchema();
                    tLLCaseSchema.setRgtState("15");
                    mLLCaseSet.add(tLLCaseSchema);
                }
            }
        }
    }

    map.put(mLLCaseSet,"UPDATE");
    map.put(mLDSpotTrackSet,"INSERT");
    return true;
  }

  private boolean getInputData(VData cInputData)
  {
      mGlobalInput.setSchema(
              (GlobalInput)
              cInputData.getObjectByObjectName("GlobalInput", 0));

      mOperator = mGlobalInput.Operator;

      mTransferData =
            (TransferData)
            cInputData.getObjectByObjectName("TransferData", 0);

      return true;
  }

  private boolean getBaseData()
  {
      return true;
  }

  /**
   * 校验保单是否已经完成核赔
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkApprove( )
  {
    return true;
  }

  private boolean prepareOutputData()
  {
      try {
          mInputData.clear();
          mInputData.add(map);
      } catch (Exception ex) {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "ClaimCheck";
          tError.functionName = "prepareOutputData";
          tError.errorMessage = "在准备往后层处理所需要的数据时出错:" + ex.toString();
          mErrors.addOneError(tError);
          return false;
      }
    mResult.clear();
    mResult.add(mLLCaseSet);
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  //取得上一级的核赔级别
  private String getUperGrade(String tString)
  {
    if (tString.equals("A")) return "B";
    if (tString.equals("B")) return "C";
    if (tString.equals("C")) return "D";
    if (tString.equals("D")) return "E";
    if (tString.equals("E")) return "F";
    if (tString.equals("F")) return "G";
    if (tString.equals("G")) return "H";
    if (tString.equals("H")) return "I";
    return tString;
  }

  private boolean getAppGrade()
  {
      return true;
  }

  //检查核赔级别是否为空；
  //检查核赔级别是否合法
  private boolean checkClaimPopedom(String tClaimPopedom)
  {
    if (tClaimPopedom == null)
    {
      CError tError = new CError();
      tError.moduleName = "ClaimManChkBL";
      tError.functionName = "submitData";
      tError.errorMessage = "权限级别为空!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    if (!tClaimPopedom.equals("A")&
        !tClaimPopedom.equals("B")&
        !tClaimPopedom.equals("C")&
        !tClaimPopedom.equals("D")&
        !tClaimPopedom.equals("E")&
        !tClaimPopedom.equals("F")&
        !tClaimPopedom.equals("G")&
        !tClaimPopedom.equals("H")&
        !tClaimPopedom.equals("I")) //代码待定，表示核赔人员的多种权限
    {
        CError tError = new CError();
        tError.moduleName = "ClaimManChkBL";
        tError.functionName = "submitData";
        tError.errorMessage = "没有以下权限级别：" + tClaimPopedom.trim();
        this.mErrors.addOneError(tError);
        return false;
    }

    return true;
  }

  public static void main (String[] args)
  {
      TransferData tTransferData = new TransferData();

      tTransferData.setNameAndValue("MngCom","86110000");
      tTransferData.setNameAndValue("Claimer","");
      tTransferData.setNameAndValue("AgentCode","");
      tTransferData.setNameAndValue("DealWith","");
      tTransferData.setNameAndValue("GiveType","1");
      tTransferData.setNameAndValue("Rate","80");
      tTransferData.setNameAndValue("RgtDateS","2005-1-5");
      tTransferData.setNameAndValue("RgtDateE","2005-12-31");
      tTransferData.setNameAndValue("ClaimPopedom","");
      tTransferData.setNameAndValue("OpTime","");
      tTransferData.setNameAndValue("AccTimes","");
      tTransferData.setNameAndValue("AccDateE","");
      tTransferData.setNameAndValue("Query","");

      GlobalInput tG = new GlobalInput();
      tG.Operator = "hcm001";
      tG.ManageCom = "86";

      VData aVData = new VData();
      aVData.addElement(tTransferData);
      aVData.addElement(tG);
      ClaimCheckBL aClaimCheckBL = new ClaimCheckBL();
      aClaimCheckBL.submitData(aVData, "INSERT");
  }
}
