/************************************************
 * AppleWood 2005-04-27 在受理结束时增加自动责任匹配.
 *
 ************************************************/

package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author wujs
 * @version 1.0
 */
public class LLFirstDutyFilterBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  private GlobalInput mGlobalInput = new GlobalInput();
  /**  */
  private LLCaseSchema  mLLCaseSchema = new LLCaseSchema();
  private TransferData mTransferData = new TransferData();
  private String CaseNo;

  public LLFirstDutyFilterBL() {}

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;

   if (!checkData())
   {
       return false;
   }

    // 数据操作业务处理
    if (cOperate.equals("INSERT"))
    {
      if (!dealData())
	return false;

    }
    PubSubmit ps = new PubSubmit();
    if (!ps.submitData( this.mResult, null ))
    {
        CError.buildErr(this,"数据库保存失败");
        return false;
    }
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 处理账单关联项目
   * @return boolean
   */
  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealData()
  {
    LLCaseDB tLLCaseDB = new LLCaseDB();
    tLLCaseDB.setCaseNo(CaseNo);
    if (!tLLCaseDB.getInfo() )
    {
        CError.buildErr(this,"理赔案件查询失败");
        return false;
    }

    MMap tmpMap = new MMap();

    this.mResult.add( tmpMap );



    /***************************************************/
    System.out.println("go into........... ");
    LLToClaimDutySet llToClaimDutySet = getToClaimDutySet(CaseNo);

    System.out.println("llToClaimDutySet.size()=="+llToClaimDutySet.size());

    tmpMap.put("delete from LLToClaimDuty where caseNo='"+mLLCaseSchema.getCaseNo()+"'","DELETE");
    tmpMap.put(llToClaimDutySet,"DELETE&INSERT");

    /////////////////////////////////////////////////////

    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
      mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData",0);
      mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",0);
      return true;
  }

  /**
   * 查询符合条件的暂交费信息
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */

  /**
   * 校验传入的暂交费收据号是否合法
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkData()
  {

      LLCaseDB tLLCaseDB = new LLCaseDB();
      CaseNo = (String) mTransferData.getValueByName("CaseNo");
      tLLCaseDB.setCaseNo(CaseNo);
      if (!tLLCaseDB.getInfo())
      {
          CError.buildErr(this, "没有查询到理赔案件");
          return false;
      }

	return true;

  }

  /**
   * 准备需要保存的数据
   */
  private void prepareOutputData()
  {
  }


  private LLToClaimDutySet getToClaimDutySet(String caseNo)
  {
    LLToClaimDutySet result = new LLToClaimDutySet();

    LLCaseRelaDB relaDB = new LLCaseRelaDB();
    relaDB.setCaseNo(caseNo);
    LLCaseRelaSet relaSet = relaDB.query();
    AutoClaimDutyMap autoClaimDutyMap = null;

    try
    {
      autoClaimDutyMap = (AutoClaimDutyMap) Class.forName("com.sinosoft.lis.llcase."+SysConst.AUTOCHOOSEDUTY).newInstance();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      mErrors.addOneError("加载AutoClaimDutyMap实现类时错误:Msg="+ex.getMessage());
      return null;
    }

    System.out.println(".........."+relaSet.size());

    for(int i=1;i<=relaSet.size();i++)
    {
      LLToClaimDutySet oneRela = autoClaimDutyMap.autoChooseGetDuty(caseNo,relaSet.get(i).getCaseRelaNo());
      result.add(oneRela);
    }

    System.out.println(".........."+result.size());

    return result;
  }

}
