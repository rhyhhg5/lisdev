package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * 账单录入
 * 
 * @author maning
 * @version 1.0
 */
public class LLCaseReceiptSave extends LLCaseProcess {

	/**
	 * 
	 */
	public LLCaseReceiptSave() {
		// TODO Auto-generated constructor stub
	}

	// 业务逻辑错误
	public CErrors mErrors = new CErrors();

	// 获取的业务数据
	private VData mInputData = new VData();

	// 返回处理后数据
	private VData mResult = new VData();

	// 返回的提交数据库MMap
	private MMap mMMap = new MMap();

	// 用户登录信息
	private GlobalInput mGlobalInput = new GlobalInput();

	// 操作类型
	private String mOperate = "";

	// 案件信息
	private LLCaseSchema mLLCaseSchema = null;

	// 账单信息
	private LLFeeMainSchema mLLFeeMainSchema = null;

	// 费用明细
	private LLCaseReceiptSet mLLCaseReceiptSet = null;

	// 社保账单
	private LLSecurityReceiptSchema mLLSecurityReceiptSchema = null;
	
	// 药品明细
	private LLReceiptSet mLLReceiptSet = null;

	// 社保明细
	private LLFeeOtherItemSet mLLFeeOtherItemSet = null;

	// 医院信息
	private LDHospitalSchema mLDHospitalSchema = null;

	// 当前日期
	private String mCurrentDate = PubFun.getCurrentDate();

	// 当前时间
	private String mCurrentTime = PubFun.getCurrentTime();

	// 理赔缓存
	private CachedLPInfo mCachedLPInfo = CachedLPInfo.getInstance();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sinosoft.lis.llcase.LLCaseProcess#getMMap()
	 */
	public MMap getMMap() throws Exception {
		mMMap.put(mLLFeeMainSchema, "INSERT");
		if (mLLCaseReceiptSet != null) {
			mMMap.put(mLLCaseReceiptSet, "INSERT");
		}
		if (mLLSecurityReceiptSchema != null) {
			mMMap.put(mLLSecurityReceiptSchema, "INSERT");
		}
		if (mLLFeeOtherItemSet != null) {
			mMMap.put(mLLFeeOtherItemSet, "INSERT");
		}
		if (mLLReceiptSet != null) {
			mMMap.put(mLLReceiptSet, "INSERT");
		}
		return mMMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sinosoft.lis.llcase.LLCaseProcess#getResult()
	 */
	public VData getResult() throws Exception {
		mResult.add(mLLFeeMainSchema);
		if (mLLCaseReceiptSet != null) {
			mResult.add(mLLCaseReceiptSet);
		}
		if (mLLSecurityReceiptSchema != null) {
			mResult.add(mLLSecurityReceiptSchema);
		}
		if (mLLFeeOtherItemSet != null) {
			mResult.add(mLLFeeOtherItemSet);
		}
		if (mLLReceiptSet != null) {
			mResult.add(mLLReceiptSet);
		}
		return mResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sinosoft.lis.llcase.LLCaseProcess#submitData(com.sinosoft.utility.VData,
	 *      java.lang.String)
	 */
	public boolean submitData(VData aInputData, String aOperate)
			throws Exception {
		System.out.println("开始账单保存");
		mInputData = aInputData;
		mOperate = aOperate;

		if (!getInputData()) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		 //提交数据库
        PubSubmit tPubSubmit = new PubSubmit();
        mInputData.clear();       
        mInputData.add(getMMap());
        if (!tPubSubmit.submitData(mInputData, ""))
        {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LLCaseReceiptSave";
                tError.functionName = "dealData";
                tError.errorMessage = "理赔账单录入数据保存失败!";
                this.mErrors.addOneError(tError);
                return false;
        }
		System.out.println("账单保存结束");

		return true;
	}

	/**
	 * 解析获取的对象
	 * 
	 * @return 处理成功标志
	 * @throws Excepiton
	 */
	private boolean getInputData() throws Exception {
		mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0);

		if (mGlobalInput == null) {
			mErrors.addOneError("用户登录信息获取失败");
			return false;
		}

		mLLCaseSchema = (LLCaseSchema) mInputData.getObjectByObjectName(
				"LLCaseSchema", 0);

		if (mLLCaseSchema == null) {
			mErrors.addOneError("案件信息获取失败");
			return false;
		}

		mLLFeeMainSchema = (LLFeeMainSchema) mInputData.getObjectByObjectName(
				"LLFeeMainSchema", 0);

		if (mLLFeeMainSchema == null) {
			mErrors.addOneError("账单信息获取失败");
			return false;
		}

		mLLCaseReceiptSet = (LLCaseReceiptSet) mInputData
				.getObjectByObjectName("LLCaseReceiptSet", 0);

		mLLSecurityReceiptSchema = (LLSecurityReceiptSchema) mInputData
				.getObjectByObjectName("LLSecurityReceiptSchema", 0);
		
		mLLReceiptSet = (LLReceiptSet) mInputData
		.getObjectByObjectName("LLReceiptSet", 0);

		return true;
	}

	/**
	 * 基本数据校验
	 * 
	 * @return 校验成功标志
	 * @throws Exception
	 */
	private boolean checkData() throws Exception {
		if (mGlobalInput.Operator == null || "".equals(mGlobalInput.Operator)) {
			mErrors.addOneError("操作用户登录信息获取失败");
			return false;
		}

		if (mGlobalInput.ManageCom == null || "".equals(mGlobalInput.ManageCom)) {
			mErrors.addOneError("操作用户登录信息获取失败");
			return false;
		}

		if (mLLCaseSchema.getCaseNo() == null
				|| "".equals(mLLCaseSchema.getCaseNo())) {
			mErrors.addOneError("案件号获取失败");
			return false;
		}

		if (mLLCaseSchema.getRgtNo() == null
				|| "".equals(mLLCaseSchema.getRgtNo())) {
			mErrors.addOneError("批次号获取失败");
			return false;
		}

		if (!"01".equals(mLLCaseSchema.getRgtState())
				&& !"02".equals(mLLCaseSchema.getRgtState())
				&& !"08".equals(mLLCaseSchema.getRgtState())) {
			mErrors.addOneError("该案件状态下不能进行账单录入");
			return false;
		}

		if ((mLLCaseSchema.getHandler() == null || "".equals(mLLCaseSchema
				.getHandler()))
				&& (mLLCaseSchema.getClaimer() == null || ""
						.equals(mLLCaseSchema.getClaimer()))) {
			mErrors.addOneError("案件未分配账单录入人");
			return false;
		}

		if (!mGlobalInput.Operator.equals(mLLCaseSchema.getHandler())
				&& !mGlobalInput.Operator.equals(mLLCaseSchema.getClaimer())) {
			mErrors.addOneError("操作人没有权限进行录入，请使用案件处理人"
					+ mLLCaseSchema.getHandler() + "或案件录入人"
					+ mLLCaseSchema.getClaimer());
			return false;
		}

		if (mLLFeeMainSchema.getFeeDate() == null
				|| "".equals(mLLFeeMainSchema.getFeeDate())) {
			mErrors.addOneError("账单结算日期不能为空");
			return false;
		}

		if (!PubFun.checkDateForm(mLLFeeMainSchema.getFeeDate())) {
			mErrors.addOneError("结算日期格式错误");
			return false;
		}

		if (mLLFeeMainSchema.getFeeType() == null
				|| "".equals(mLLFeeMainSchema.getFeeType())) {
			mErrors.addOneError("账单种类不能为空");
			return false;
		}

		LDCodeSchema tLDCodeSchema = mCachedLPInfo
				.findLLFeeTypeByCode(mLLFeeMainSchema.getFeeType());
		if (tLDCodeSchema == null) {
			mErrors.addOneError("账单种类代码错误");
			return false;
		}

		if (mLLFeeMainSchema.getFeeAtti() == null
				|| "".equals(mLLFeeMainSchema.getFeeAtti())) {
			mErrors.addOneError("账单属性不能为空");
			return false;
		}

		tLDCodeSchema = mCachedLPInfo.findLLFeeAttiByCode(mLLFeeMainSchema
				.getFeeAtti());
		if (tLDCodeSchema == null) {
			mErrors.addOneError("账单属性代码错误");
			return false;
		}

		if (mLLFeeMainSchema.getReceiptNo() == null
				|| "".equals(mLLFeeMainSchema.getReceiptNo())) {
			mErrors.addOneError("账单号码不能为空");
			return false;
		}

		if (mLLFeeMainSchema.getPreAmnt() < 0) {
			mErrors.addOneError("先期给付金额小于零");
			return false;
		}

		if (mLLFeeMainSchema.getSelfAmnt() < 0) {
			mErrors.addOneError("自费金额小于零");
			return false;
		}

		if (mLLFeeMainSchema.getRefuseAmnt() < 0) {
			mErrors.addOneError("不合理金额小于零");
			return false;
		}

		if (mLLFeeMainSchema.getSocialPlanAmnt() < 0) {
			mErrors.addOneError("社会统筹金额小于零");
			return false;
		}

		if (mLLFeeMainSchema.getAppendAmnt() < 0) {
			mErrors.addOneError("附加支付金额小于零");
			return false;
		}

		if (mLLFeeMainSchema.getOtherOrganAmnt() < 0) {
			mErrors.addOneError("其他机构报销金额小于零");
			return false;
		}

		if (mLLFeeMainSchema.getOtherAmnt() < 0) {
			mErrors.addOneError("其他金额小于零");
			return false;
		}

		if (mLLFeeMainSchema.getPersonGiveAmnt() < 0) {
			mErrors.addOneError("个人支付金额小于零");
			return false;
		}

		if (mLLFeeMainSchema.getSumFee() < 0) {
			mErrors.addOneError("费用合计小于零");
			return false;
		}

		if (mLLFeeMainSchema.getRemnant() < 0) {
			mErrors.addOneError("赔付剩余金额小于零");
			return false;
		}

		return true;
	}

	/**
	 * 业务处理
	 * 
	 * @return 处理成功标志
	 * @throws Exception
	 */
	private boolean dealData() throws Exception {
		// 处理LLFeeMain表
		if (!dealLLFeeMain()) {
			return false;
		}

		// 处理LLCaseReceipt表
		if (!dealLLCaseReceipt()) {
			return false;
		}

		// 处理LLSecurityRecepit表
		if (!dealLLSecurityReceipt()) {
			return false;
		}

		// 处理LLFeeOtherItem表
		if (!dealLLFeeOtherItem()) {
			return false;
		}

		return true;
	}

	/**
	 * 处理账单主表
	 * 
	 * @return
	 * @throws Exception
	 */
	private boolean dealLLFeeMain() throws Exception {
		System.out.println("dealLLFeeMain......");

		LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
		tLLFeeMainSchema.setSchema(mLLFeeMainSchema);
		mLLFeeMainSchema = new LLFeeMainSchema();

		/**
		 * 案件信息部分
		 */

		// 理赔案件号
		mLLFeeMainSchema.setCaseNo(mLLCaseSchema.getCaseNo());
		// 理赔批次号
		mLLFeeMainSchema.setRgtNo(mLLCaseSchema.getRgtNo());

		/**
		 * 客户信息部分
		 */

		// 被保险人客户号
		mLLFeeMainSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
		// 被保险人姓名
		mLLFeeMainSchema.setCustomerName(mLLCaseSchema.getCustomerName());
		// 被保险人性别
		mLLFeeMainSchema.setCustomerSex(mLLCaseSchema.getCustomerSex());
		// 被保险人年龄
		mLLFeeMainSchema.setAge((int)(mLLCaseSchema.getCustomerAge()));

		// 人员类别
		if ("1".equals(tLLFeeMainSchema.getFeeAtti())
				|| "2".equals(tLLFeeMainSchema.getFeeAtti())
				|| "4".equals(tLLFeeMainSchema.getFeeAtti())) {
			if (tLLFeeMainSchema.getInsuredStat() == null
					|| "".equals(tLLFeeMainSchema.getInsuredStat())) {
				mErrors.addOneError("社保账单，人员类别不能为空");
				return false;
			}
		}

		if (tLLFeeMainSchema.getInsuredStat() != null
				&& !"".equals(tLLFeeMainSchema.getInsuredStat())) {
			LDCodeSchema tLDCodeSchema = mCachedLPInfo
					.findInsuStatByCode(tLLFeeMainSchema.getInsuredStat());
			if (tLDCodeSchema == null) {
				mErrors.addOneError("人员类别代码错误");
				return false;
			}
		}

		mLLFeeMainSchema.setInsuredStat(tLLFeeMainSchema.getInsuredStat());

		// 客户社保号
		mLLFeeMainSchema.setSecurityNo(tLLFeeMainSchema.getSecurityNo());

		// 单位社保等级号
		mLLFeeMainSchema.setCompSecuNo(tLLFeeMainSchema.getCompSecuNo());

		/**
		 * 账单信息
		 */
		// 账单号码
		mLLFeeMainSchema.setReceiptNo(tLLFeeMainSchema.getReceiptNo());
		// 账单结算日期
		mLLFeeMainSchema.setFeeDate(tLLFeeMainSchema.getFeeDate());
		// 账单属性
		mLLFeeMainSchema.setFeeType(tLLFeeMainSchema.getFeeType());
		// 账单种类
		mLLFeeMainSchema.setFeeAtti(tLLFeeMainSchema.getFeeAtti());
    
		// 账单类型

		if (tLLFeeMainSchema.getFeeAffixType() != null
				&& !"".equals(tLLFeeMainSchema.getFeeAffixType())) {
			LDCodeSchema tLDCodeSchema = mCachedLPInfo
					.findFeeAffixTypeByCode(tLLFeeMainSchema.getFeeAffixType());
			if (tLDCodeSchema == null) {
				mErrors.addOneError("账单类型代码错误");
				return false;
			}
		}
		mLLFeeMainSchema.setFeeAffixType(tLLFeeMainSchema.getFeeAffixType());

		if (tLLFeeMainSchema.getHospitalCode() != null
				&& !"".equals(tLLFeeMainSchema.getHospitalCode())) {
			LDHospitalDB tLDHospitalDB = new LDHospitalDB();
			tLDHospitalDB.setHospitCode(tLLFeeMainSchema.getHospitalCode());
			if (!tLDHospitalDB.getInfo()) {
				mErrors.addOneError("医院查询失败");
				return false;
			}
			mLDHospitalSchema = tLDHospitalDB.getSchema();
		}

		// 医院代码
		mLLFeeMainSchema.setHospitalCode(tLLFeeMainSchema.getHospitalCode());

		// 医院名称
		if (tLLFeeMainSchema.getHospitalName() != null
				&& !"".equals(tLLFeeMainSchema.getHospitalName())) {
			mLLFeeMainSchema
					.setHospitalName(tLLFeeMainSchema.getHospitalName());
		} else {
			mLLFeeMainSchema.setHospitalName(mLDHospitalSchema.getHospitName());
			//医院级别
			mLLFeeMainSchema.setHosGrade(mLDHospitalSchema.getLevelCode());

			// 医院城区属性
			mLLFeeMainSchema.setHosDistrict(mLDHospitalSchema.getUrbanOption());
		}
		

		// 医院地址
		mLLFeeMainSchema.setHosAddress(tLLFeeMainSchema.getHosAddress());

		// 医院电话
		mLLFeeMainSchema.setHosPhone(tLLFeeMainSchema.getHosPhone());

		// 医院属性
		mLLFeeMainSchema.setHosAtti(tLLFeeMainSchema.getHosAtti());

		// 出入院日期
		if ("2".equals(tLLFeeMainSchema.getFeeType())) {
			if (tLLFeeMainSchema.getHospStartDate() == null
					|| "".equals(tLLFeeMainSchema.getHospStartDate())) {
				mErrors.addOneError("住院账单，入院日期不能为空");
				return false;
			}

			if (!PubFun.checkDateForm(tLLFeeMainSchema.getHospStartDate())) {
				mErrors.addOneError("入院日期格式错误");
				return false;
			}

			mLLFeeMainSchema.setHospStartDate(tLLFeeMainSchema
					.getHospStartDate());

			if (tLLFeeMainSchema.getHospEndDate() == null
					|| "".equals(tLLFeeMainSchema.getHospEndDate())) {
				mErrors.addOneError("住院账单，出院日期不能为空");
				return false;
			}

			if (!PubFun.checkDateForm(tLLFeeMainSchema.getHospEndDate())) {
				mErrors.addOneError("出院日期格式错误");
				return false;
			}

			mLLFeeMainSchema.setHospEndDate(tLLFeeMainSchema.getHospEndDate());
		} else {
			// 门诊出入院日期如果空取账单日期
			if (tLLFeeMainSchema.getHospStartDate() == null
					|| "".equals(tLLFeeMainSchema.getHospStartDate())) {
				mLLFeeMainSchema
						.setHospStartDate(tLLFeeMainSchema.getFeeDate());
			}

			if (tLLFeeMainSchema.getHospEndDate() == null
					|| "".equals(tLLFeeMainSchema.getHospEndDate())) {
				mLLFeeMainSchema.setHospEndDate(tLLFeeMainSchema.getFeeDate());
			}
		}

		// 住院天数
		int tRealHospDay = PubFun.calInterval(mLLFeeMainSchema
				.getHospStartDate(), mLLFeeMainSchema.getHospEndDate(), "D") + 1;

		if (tRealHospDay != tLLFeeMainSchema.getRealHospDate()) {
			System.out.println("账单住院天数与计算结果不符，以计算结果为准");
		}

		mLLFeeMainSchema.setRealHospDate(tRealHospDay);

		// 住院号
		mLLFeeMainSchema.setInHosNo(tLLFeeMainSchema.getInHosNo());

		// 先期给付
		mLLFeeMainSchema.setPreAmnt(tLLFeeMainSchema.getPreAmnt());

		// 自费金额
		mLLFeeMainSchema.setSelfAmnt(tLLFeeMainSchema.getSelfAmnt());

		// 不合理金额
		mLLFeeMainSchema.setRefuseAmnt(tLLFeeMainSchema.getRefuseAmnt());

		// 社会统筹
		mLLFeeMainSchema
				.setSocialPlanAmnt(tLLFeeMainSchema.getSocialPlanAmnt());

		// 附加支付
		mLLFeeMainSchema.setAppendAmnt(tLLFeeMainSchema.getAppendAmnt());

		// 其他机构报销
		mLLFeeMainSchema
				.setOtherOrganAmnt(tLLFeeMainSchema.getOtherOrganAmnt());

		// 其它
		mLLFeeMainSchema.setOtherAmnt(tLLFeeMainSchema.getOtherAmnt());

		// 个人支付
		mLLFeeMainSchema
				.setPersonGiveAmnt(tLLFeeMainSchema.getPersonGiveAmnt());

		// 费用总额
		mLLFeeMainSchema.setSumFee(tLLFeeMainSchema.getSumFee());

		// 第一次住院标志
		mLLFeeMainSchema.setFirstInHos(tLLFeeMainSchema.getFirstInHos());

		// 原始账单标记 -- 赔付实赔金额
		mLLFeeMainSchema.setOriginFlag(tLLFeeMainSchema.getOriginFlag());

		// 再次赔付标记
		mLLFeeMainSchema.setRepayFlag(tLLFeeMainSchema.getRepayFlag());

		// 赔付次数
		mLLFeeMainSchema.setPayTimes(tLLFeeMainSchema.getPayTimes());

		// 既往账单号
		mLLFeeMainSchema.setOldMainFeeNo(tLLFeeMainSchema.getOldMainFeeNo());

		// 既往理赔号
		mLLFeeMainSchema.setOldCaseNo(tLLFeeMainSchema.getOldCaseNo());

		// 医保结算时间
		if (tLLFeeMainSchema.getSecuPayDate() != null
				&& !"".equals(tLLFeeMainSchema.getSecuPayDate())
				&& !PubFun.checkDateForm(tLLFeeMainSchema.getSecuPayDate())) {
			mErrors.addOneError("医保结算日期格式错误");
			return false;
		}
		mLLFeeMainSchema.setSecuPayDate(tLLFeeMainSchema.getSecuPayDate());

		// 医疗形式
		mLLFeeMainSchema.setMedicalType(tLLFeeMainSchema.getMedicalType());

		// 医保结算方式
		mLLFeeMainSchema.setSecuPayType(tLLFeeMainSchema.getSecuPayType());

		/**
		 * 账单号码
		 */
		String tLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
		String tMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO", tLimit);
		if (tMainFeeNo == null || "".equals(tMainFeeNo)) {
			mErrors.addOneError("账单号生成失败");
			return false;
		}
		mLLFeeMainSchema.setMainFeeNo(tMainFeeNo);

		// 操作人
		mLLFeeMainSchema.setOperator(mGlobalInput.Operator);

		// 管理机构
		mLLFeeMainSchema.setMngCom(mGlobalInput.ManageCom);

		// 生成日期
		mLLFeeMainSchema.setMakeDate(mCurrentDate);

		// 生成时间
		mLLFeeMainSchema.setMakeTime(mCurrentTime);

		// 修改日期
		mLLFeeMainSchema.setModifyDate(mCurrentDate);

		// 修改时间
		mLLFeeMainSchema.setModifyTime(mCurrentTime);
		
		// #3544 医保类型
		mLLFeeMainSchema.setMedicareType(tLLFeeMainSchema.getMedicareType());

		return true;
	}

	/**
	 * 处理费用明细表
	 * 
	 * @return
	 * @throws Exception
	 */
	private boolean dealLLCaseReceipt() throws Exception {
		if (mLLCaseReceiptSet == null || mLLCaseReceiptSet.size() <= 0) {
			return true;
		}

		LLCaseReceiptSet tLLCaseReceiptSet = new LLCaseReceiptSet();
		tLLCaseReceiptSet.set(mLLCaseReceiptSet);
		mLLCaseReceiptSet = new LLCaseReceiptSet();

		// 汇总费用
		double tFee = 0.0;

		// 汇总先期给付
		double tPreAmnt = 0.0;

		// 汇总自费
		double tSelfAmnt = 0.0;

		// 汇总不合理费用
		double tRefuseAmnt = 0.0;

		// 汇总社会统筹
		double tSocialPlanAmnt = 0.0;

		// 汇总附加支付
		double tAppendAmnt = 0.0;

		// 汇总其他机构报销
		double tOtherOrganAmnt = 0.0;

		// 汇总其他金额
		double tOtherAmnt = 0.0;

		// 汇总全额统筹
		double tWholePlanFee = 0.0;

		// 汇总部分统筹
		double tPartPlanFee = 0.0;

		// 汇总自付2
		double tSelfPay2 = 0.0;

		// 汇总自费
		double tSelfFee = 0.0;

		for (int i = 1; i <= tLLCaseReceiptSet.size(); i++) {
			LLCaseReceiptSchema tTempLLCaseReceiptSchema = tLLCaseReceiptSet
					.get(i);
			LLCaseReceiptSchema tLLCaseReceiptSchema = new LLCaseReceiptSchema();

			// 案件号
			tLLCaseReceiptSchema.setCaseNo(mLLFeeMainSchema.getCaseNo());

			// 批次号
			tLLCaseReceiptSchema.setRgtNo(mLLFeeMainSchema.getRgtNo());

			// 账单号
			tLLCaseReceiptSchema.setMainFeeNo(mLLFeeMainSchema.getMainFeeNo());

			// 费用项目代码
			String tFeeItemCode = tTempLLCaseReceiptSchema.getFeeItemCode();
			if (tFeeItemCode == null || "".equals(tFeeItemCode)) {
				mErrors.addOneError("获取费用明细代码失败");
				return false;
			}
			LDCodeSchema tLDCodeSchema = mCachedLPInfo
					.findLLFeeItemTypeByCode(tFeeItemCode);

			if (tLDCodeSchema == null) {
				mErrors.addOneError("费用明细代码错误");
				return false;
			}
			tLLCaseReceiptSchema.setFeeItemCode(tLDCodeSchema.getCode());

			// 费用项目名称
			tLLCaseReceiptSchema.setFeeItemName(tLDCodeSchema.getCodeName());

			// 费用金额
			if (tTempLLCaseReceiptSchema.getFee() < 0) {
				mErrors.addOneError("费用明细下费用金额小于0");
				return false;
			}
			tLLCaseReceiptSchema.setFee(tTempLLCaseReceiptSchema.getFee());
			tFee += tTempLLCaseReceiptSchema.getFee();

			// 有效标记
			tLLCaseReceiptSchema.setAvaliFlag("0");

			// 先期给付
			if (tTempLLCaseReceiptSchema.getPreAmnt() < 0) {
				mErrors.addOneError("费用明细先期给付金额小于0");
				return false;
			}
			tLLCaseReceiptSchema.setPreAmnt(tTempLLCaseReceiptSchema
					.getPreAmnt());
			tPreAmnt += tTempLLCaseReceiptSchema.getPreAmnt();

			// 自费金额
			if (tTempLLCaseReceiptSchema.getSelfAmnt() < 0) {
				mErrors.addOneError("费用明细自费金额小于0");
				return false;
			}
			tLLCaseReceiptSchema.setSelfAmnt(tTempLLCaseReceiptSchema
					.getSelfAmnt());
			tSelfAmnt += tTempLLCaseReceiptSchema.getSelfAmnt();

			// 不合理费用
			if (tTempLLCaseReceiptSchema.getRefuseAmnt() < 0) {
				mErrors.addOneError("费用明细不合理费用金额小于0");
				return false;
			}
			tLLCaseReceiptSchema.setRefuseAmnt(tTempLLCaseReceiptSchema
					.getRefuseAmnt());
			tRefuseAmnt += tTempLLCaseReceiptSchema.getRefuseAmnt();

			// 社会统筹
			if (tTempLLCaseReceiptSchema.getSocialPlanAmnt() < 0) {
				mErrors.addOneError("费用明细社会统筹金额小于0");
				return false;
			}
			tLLCaseReceiptSchema.setSocialPlanAmnt(tTempLLCaseReceiptSchema
					.getSocialPlanAmnt());
			tSocialPlanAmnt += tTempLLCaseReceiptSchema.getSocialPlanAmnt();

			// 附加支付
			if (tTempLLCaseReceiptSchema.getAppendAmnt() < 0) {
				mErrors.addOneError("费用明细附加支付金额小于0");
				return false;
			}
			tLLCaseReceiptSchema.setAppendAmnt(tTempLLCaseReceiptSchema
					.getAppendAmnt());
			tAppendAmnt += tTempLLCaseReceiptSchema.getAppendAmnt();

			// 其他机构报销
			if (tTempLLCaseReceiptSchema.getOtherOrganAmnt() < 0) {
				mErrors.addOneError("费用明细其他机构报销金额小于0");
				return false;
			}
			tLLCaseReceiptSchema.setOtherOrganAmnt(tTempLLCaseReceiptSchema
					.getOtherOrganAmnt());
			tOtherOrganAmnt += tTempLLCaseReceiptSchema.getOtherOrganAmnt();

			// 其它
			if (tTempLLCaseReceiptSchema.getOtherAmnt() < 0) {
				mErrors.addOneError("费用明细其它金额小于0");
				return false;
			}
			tLLCaseReceiptSchema.setOtherAmnt(tTempLLCaseReceiptSchema
					.getOtherAmnt());
			tOtherAmnt += tTempLLCaseReceiptSchema.getOtherAmnt();

			// 自费项目描述
			tLLCaseReceiptSchema.setSelfStatement(tTempLLCaseReceiptSchema
					.getSelfStatement());

			// 不合理费用描述
			tLLCaseReceiptSchema.setRefuseStatement(tTempLLCaseReceiptSchema
					.getRefuseStatement());

			// 先期给付描述
			tLLCaseReceiptSchema.setPreStatement(tTempLLCaseReceiptSchema
					.getPreStatement());

			// 全额统筹
			if (tTempLLCaseReceiptSchema.getWholePlanFee() < 0) {
				mErrors.addOneError("费用明细全额统筹金额小于0");
				return false;
			}
			tLLCaseReceiptSchema.setWholePlanFee(tTempLLCaseReceiptSchema
					.getWholePlanFee());
			tWholePlanFee += tTempLLCaseReceiptSchema.getWholePlanFee();

			// 部分统筹
			if (tTempLLCaseReceiptSchema.getPartPlanFee() < 0) {
				mErrors.addOneError("费用明细部分统筹金额小于0");
				return false;
			}
			tLLCaseReceiptSchema.setPartPlanFee(tTempLLCaseReceiptSchema
					.getPartPlanFee());
			tPartPlanFee += tTempLLCaseReceiptSchema.getPartPlanFee();

			// 自负2
			if (tTempLLCaseReceiptSchema.getSelfPay2() < 0) {
				mErrors.addOneError("费用明细自负2金额小于0");
				return false;
			}
			tLLCaseReceiptSchema.setSelfPay2(tTempLLCaseReceiptSchema
					.getSelfPay2());
			tSelfPay2 += tTempLLCaseReceiptSchema.getSelfPay2();

			// 自费
			if (tTempLLCaseReceiptSchema.getSelfFee() < 0) {
				mErrors.addOneError("费用明细自费金额小于0");
				return false;
			}
			tLLCaseReceiptSchema.setSelfFee(tTempLLCaseReceiptSchema
					.getSelfFee());
			tSelfFee += tTempLLCaseReceiptSchema.getSelfFee();

			// 操作人
			tLLCaseReceiptSchema.setOperator(mLLFeeMainSchema.getOperator());

			// 管理机构
			tLLCaseReceiptSchema.setMngCom(mLLFeeMainSchema.getMngCom());

			// 生成日期
			tLLCaseReceiptSchema.setMakeDate(mCurrentDate);

			// 生成时间
			tLLCaseReceiptSchema.setMakeTime(mCurrentTime);

			// 修改日期
			tLLCaseReceiptSchema.setModifyDate(mCurrentDate);

			// 修改时间
			tLLCaseReceiptSchema.setModifyTime(mCurrentTime);

			// 费用明细号码
			String tLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
			String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", tLimit);
			if (tFeeDetailNo == null || "".equals(tFeeDetailNo)) {
				mErrors.addOneError("费用明细号生成失败");
				return false;
			}
			tLLCaseReceiptSchema.setFeeDetailNo(tFeeDetailNo);
			mLLCaseReceiptSet.add(tLLCaseReceiptSchema);
		}

		if (mLLFeeMainSchema.getSumFee() <= 0) {
			mLLFeeMainSchema.setSumFee(tFee);
		}

		if (mLLFeeMainSchema.getPreAmnt() <= 0) {
			mLLFeeMainSchema.setPreAmnt(tPreAmnt);
		}

		if (mLLFeeMainSchema.getSelfAmnt() <= 0) {
			mLLFeeMainSchema.setSelfAmnt(tSelfAmnt);
		}

		if (mLLFeeMainSchema.getRefuseAmnt() <= 0) {
			mLLFeeMainSchema.setRefuseAmnt(tRefuseAmnt);
		}

		if (mLLFeeMainSchema.getSocialPlanAmnt() <= 0) {
			mLLFeeMainSchema.setSocialPlanAmnt(tSocialPlanAmnt);
		}

		if (mLLFeeMainSchema.getAppendAmnt() <= 0) {
			mLLFeeMainSchema.setAppendAmnt(tAppendAmnt);
		}

		if (mLLFeeMainSchema.getOtherOrganAmnt() <= 0) {
			mLLFeeMainSchema.setOtherOrganAmnt(tOtherOrganAmnt);
		}

		if (mLLFeeMainSchema.getOtherAmnt() <= 0) {
			mLLFeeMainSchema.setOtherAmnt(tOtherAmnt);
		}

		return true;
	}

	/**
	 * 处理社保账单表
	 * 
	 * @return
	 * @throws Exception
	 */
	private boolean dealLLSecurityReceipt() throws Exception {
		if ("1".equals(mLLFeeMainSchema.getFeeAtti())
				|| "2".equals(mLLFeeMainSchema.getFeeAtti())
				|| "4".equals(mLLFeeMainSchema.getFeeAtti())) {
			if (mLLSecurityReceiptSchema == null) {
				mErrors.addOneError("社保账单信息获取失败");
				return false;
			}
		}

		/**
		 * 根据传入的数据处理社保账单信息
		 */
		if (mLLSecurityReceiptSchema != null) {
			LLSecurityReceiptSchema tLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
			tLLSecurityReceiptSchema.setSchema(mLLSecurityReceiptSchema);
			mLLSecurityReceiptSchema = new LLSecurityReceiptSchema();

			// 案件号
			mLLSecurityReceiptSchema.setCaseNo(mLLFeeMainSchema.getCaseNo());

			// 批次号
			mLLSecurityReceiptSchema.setRgtNo(mLLFeeMainSchema.getRgtNo());

			// 账单号
			mLLSecurityReceiptSchema.setMainFeeNo(mLLFeeMainSchema
					.getMainFeeNo());

			// 社保账单号
			mLLSecurityReceiptSchema.setFeeDetailNo(mLLFeeMainSchema
					.getMainFeeNo());

			// 申报金额
			if (tLLSecurityReceiptSchema.getApplyAmnt() < 0) {
				mErrors.addOneError("申报金额小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setApplyAmnt(tLLSecurityReceiptSchema
					.getApplyAmnt());

			// 医保目录内金额
			if (tLLSecurityReceiptSchema.getFeeInSecu() < 0) {
				mErrors.addOneError("医保目录内金额小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setFeeInSecu(tLLSecurityReceiptSchema
					.getFeeInSecu());

			// 医保目录外金额
			if (tLLSecurityReceiptSchema.getFeeOutSecu() < 0) {
				mErrors.addOneError("医保目录外金额小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setFeeOutSecu(tLLSecurityReceiptSchema
					.getFeeOutSecu());

			// 门诊大额年度累计支付金额
			if (tLLSecurityReceiptSchema.getTotalSupDoorFee() < 0) {
				mErrors.addOneError("门诊大额年度累计支付金额小于0");
				return false;
			}
			mLLSecurityReceiptSchema
					.setTotalSupDoorFee(tLLSecurityReceiptSchema
							.getTotalSupDoorFee());

			// 报销单据张数
			if (tLLSecurityReceiptSchema.getPayReceipt() < 0) {
				mErrors.addOneError("报销单据张数小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setPayReceipt(tLLSecurityReceiptSchema
					.getPayReceipt());

			// 拒付单据张数
			if (tLLSecurityReceiptSchema.getRefuseReceipt() < 0) {
				mErrors.addOneError("拒付单据张数小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setRefuseReceipt(tLLSecurityReceiptSchema
					.getRefuseReceipt());

			// 本次门诊大额医疗费用
			if (tLLSecurityReceiptSchema.getSupDoorFee() < 0) {
				mErrors.addOneError("本次门诊大额医疗费用小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setSupDoorFee(tLLSecurityReceiptSchema
					.getSupDoorFee());

			// 年度累计门诊大额医疗费用
			if (tLLSecurityReceiptSchema.getYearSupDoorFee() < 0) {
				mErrors.addOneError("年度累计门诊大额医疗费用小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setYearSupDoorFee(tLLSecurityReceiptSchema
					.getYearSupDoorFee());

			// 本次统筹基金医疗费用
			if (tLLSecurityReceiptSchema.getPlanFee() < 0) {
				mErrors.addOneError("本次统筹基金医疗费用小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setPlanFee(tLLSecurityReceiptSchema
					.getPlanFee());

			// 年度累计统筹基金医疗费用
			if (tLLSecurityReceiptSchema.getYearPlayFee() < 0) {
				mErrors.addOneError("年度累计统筹基金医疗费用小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setYearPlayFee(tLLSecurityReceiptSchema
					.getYearPlayFee());

			// 本次住院大额医疗费用
			if (tLLSecurityReceiptSchema.getSupInHosFee() < 0) {
				mErrors.addOneError("本次住院大额医疗费用小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setSupInHosFee(tLLSecurityReceiptSchema
					.getSupInHosFee());

			// 年度累计住院大额医疗费用
			if (tLLSecurityReceiptSchema.getYearSupInHosFee() < 0) {
				mErrors.addOneError("年度累计住院大额医疗费用小于0");
				return false;
			}
			mLLSecurityReceiptSchema
					.setYearSupInHosFee(tLLSecurityReceiptSchema
							.getYearSupInHosFee());

			// 医保支付合计
			if (tLLSecurityReceiptSchema.getSecurityFee() < 0) {
				mErrors.addOneError("医保支付合计小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setSecurityFee(tLLSecurityReceiptSchema
					.getSecurityFee());

			// 小额门诊费用
			if (tLLSecurityReceiptSchema.getSmallDoorPay() < 0) {
				mErrors.addOneError("小额门诊费用小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setSmallDoorPay(tLLSecurityReceiptSchema
					.getSmallDoorPay());

			// 门急诊费用
			if (tLLSecurityReceiptSchema.getEmergencyPay() < 0) {
				mErrors.addOneError("小额门诊费用小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setEmergencyPay(tLLSecurityReceiptSchema
					.getEmergencyPay());

			// 自付一金额
			if (tLLSecurityReceiptSchema.getSelfPay1() < 0) {
				mErrors.addOneError("自付一金额小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setSelfPay1(tLLSecurityReceiptSchema
					.getSelfPay1());

			// 自付二金额
			if (tLLSecurityReceiptSchema.getSelfPay2() < 0) {
				mErrors.addOneError("自付二金额小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setSelfPay2(tLLSecurityReceiptSchema
					.getSelfPay2());

			// 医保不予支付金额合计
			if (tLLSecurityReceiptSchema.getSecuRefuseFee() < 0) {
				mErrors.addOneError("医保不予支付金额合计小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setSecuRefuseFee(tLLSecurityReceiptSchema
					.getSecuRefuseFee());

			// 自费金额
			if (tLLSecurityReceiptSchema.getSelfAmnt() < 0) {
				mErrors.addOneError("自费金额小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setSelfAmnt(tLLSecurityReceiptSchema
					.getSelfAmnt());

			// 起付线
			if (tLLSecurityReceiptSchema.getGetLimit() < 0) {
				mErrors.addOneError("起付线小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setGetLimit(tLLSecurityReceiptSchema
					.getGetLimit());

			// 低段责任金额
			if (tLLSecurityReceiptSchema.getLowAmnt() < 0) {
				mErrors.addOneError("低段责任金额小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setLowAmnt(tLLSecurityReceiptSchema
					.getLowAmnt());

			// 中段责任金额
			if (tLLSecurityReceiptSchema.getMidAmnt() < 0) {
				mErrors.addOneError("中段责任金额小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setMidAmnt(tLLSecurityReceiptSchema
					.getMidAmnt());

			// 高段一责任金额
			if (tLLSecurityReceiptSchema.getHighAmnt1() < 0) {
				mErrors.addOneError("高段一责任金额小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setHighAmnt1(tLLSecurityReceiptSchema
					.getHighAmnt1());

			// 高段二责任金额
			if (tLLSecurityReceiptSchema.getHighAmnt2() < 0) {
				mErrors.addOneError("高段二责任金额小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setHighAmnt2(tLLSecurityReceiptSchema
					.getHighAmnt2());

			// 超高段责任金额
			if (tLLSecurityReceiptSchema.getSuperAmnt() < 0) {
				mErrors.addOneError("超高段责任金额小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setSuperAmnt(tLLSecurityReceiptSchema
					.getSuperAmnt());

			// 帐户支付金额
			if (tLLSecurityReceiptSchema.getAccountPay() < 0) {
				mErrors.addOneError("超高段责任金额小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setAccountPay(tLLSecurityReceiptSchema
					.getAccountPay());

			// 公务员医疗补助支付金额
			if (tLLSecurityReceiptSchema.getOfficialSubsidy() < 0) {
				mErrors.addOneError("公务员医疗补助支付金额小于0");
				return false;
			}
			mLLSecurityReceiptSchema
					.setOfficialSubsidy(tLLSecurityReceiptSchema
							.getOfficialSubsidy());

			// 大额门诊责任金额
			if (tLLSecurityReceiptSchema.getHighDoorAmnt() < 0) {
				mErrors.addOneError("大额门诊责任金额小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setHighDoorAmnt(tLLSecurityReceiptSchema
					.getHighDoorAmnt());

			// 退休人员补充医疗费用
			if (tLLSecurityReceiptSchema.getRetireAddFee() < 0) {
				mErrors.addOneError("退休人员补充医疗费用小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setRetireAddFee(tLLSecurityReceiptSchema
					.getRetireAddFee());

			// 年度累计退休人员补充医疗费用
			if (tLLSecurityReceiptSchema.getYearRetireAddFee() < 0) {
				mErrors.addOneError("年度累计退休人员补充医疗费用小于0");
				return false;
			}
			mLLSecurityReceiptSchema
					.setYearRetireAddFee(tLLSecurityReceiptSchema
							.getYearRetireAddFee());

			// 备用金额字段
			if (tLLSecurityReceiptSchema.getStandbyAmnt() < 0) {
				mErrors.addOneError("备用金额字段小于0");
				return false;
			}
			mLLSecurityReceiptSchema.setStandbyAmnt(tLLSecurityReceiptSchema
					.getStandbyAmnt());

			// 操作人
			mLLSecurityReceiptSchema
					.setOperator(mLLFeeMainSchema.getOperator());

			// 管理机构
			mLLSecurityReceiptSchema.setMngCom(mLLFeeMainSchema.getMngCom());

			// 生成日期
			mLLSecurityReceiptSchema.setMakeDate(mCurrentDate);

			// 生成时间
			mLLSecurityReceiptSchema.setMakeTime(mCurrentTime);

			// 修改日期
			mLLSecurityReceiptSchema.setModifyDate(mCurrentDate);

			// 修改时间
			mLLSecurityReceiptSchema.setModifyTime(mCurrentTime);

			if (mLLSecurityReceiptSchema.getFeeOutSecu() <= 0) {
				mLLSecurityReceiptSchema.setFeeOutSecu(Arith.round(
						mLLSecurityReceiptSchema.getSelfAmnt()
								+ mLLSecurityReceiptSchema.getSelfPay2(), 2));
			}

			if (mLLSecurityReceiptSchema.getFeeInSecu() <= 0) {
				mLLSecurityReceiptSchema.setFeeInSecu(Arith.round(
						mLLSecurityReceiptSchema.getApplyAmnt()
								- mLLSecurityReceiptSchema.getFeeOutSecu(), 2));

			}

			if (mLLSecurityReceiptSchema.getPlanFee() <= 0) {
				mLLSecurityReceiptSchema
						.setPlanFee(Arith.round(mLLSecurityReceiptSchema
								.getFeeInSecu()
								- mLLSecurityReceiptSchema.getSelfPay1()
								- mLLSecurityReceiptSchema.getSupInHosFee(), 2));
			}

			if (mLLSecurityReceiptSchema.getSelfPay1() <= 0) {
				mLLSecurityReceiptSchema
						.setSelfPay1(Arith.round(mLLSecurityReceiptSchema
								.getFeeInSecu()
								- mLLSecurityReceiptSchema.getPlanFee()
								- mLLSecurityReceiptSchema.getSupInHosFee(), 2));
			}

			if (mLLSecurityReceiptSchema.getSelfPay1() <= 0) {
				mLLSecurityReceiptSchema.setSelfPay1(Arith.round(
						mLLSecurityReceiptSchema.getFeeInSecu()
								- mLLSecurityReceiptSchema.getSupDoorFee(), 2));
			}

			if (mLLSecurityReceiptSchema.getSelfPay1() <= 0) {
				mLLSecurityReceiptSchema.setSelfPay1(Arith.round(
						mLLSecurityReceiptSchema.getLowAmnt()
								+ mLLSecurityReceiptSchema.getMidAmnt()
								+ mLLSecurityReceiptSchema.getHighAmnt1()
								+ mLLSecurityReceiptSchema.getSuperAmnt(), 2));
			}

			if (mLLSecurityReceiptSchema.getSelfPay1() <= 0) {
				mLLSecurityReceiptSchema
						.setSelfPay1(Arith
								.round(mLLSecurityReceiptSchema
										.getSmallDoorPay()
										+ mLLSecurityReceiptSchema
												.getHighDoorAmnt(), 2));
			}

			if (mLLFeeMainSchema.getSumFee() <= 0) {
				mLLFeeMainSchema.setSumFee(mLLSecurityReceiptSchema
						.getApplyAmnt());
			}
		}

		return true;
	}

	/**
	 * 处理LLFeeOtherItem表
	 * 
	 * @return
	 * @throws Exception
	 */
	private boolean dealLLFeeOtherItem() throws Exception {
		if (mLLSecurityReceiptSchema != null) {
			mLLFeeOtherItemSet = new LLFeeOtherItemSet();
			// 合计金额
			if (mLLSecurityReceiptSchema.getApplyAmnt() > 0) {
				LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
				String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO",
						mLLSecurityReceiptSchema.getMngCom());
				if (tFeeDetailNo == null || "".equals(tFeeDetailNo)) {
					mErrors.addOneError("社保明细序号生成失败");
					return false;
				}
				Reflections tReflection = new Reflections();
				tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
				tReflection.transFields(tLLFeeOtherItemSchema,
						mLLSecurityReceiptSchema);
				tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
						.getApplyAmnt());
				tLLFeeOtherItemSchema.setItemClass("S");
				tLLFeeOtherItemSchema.setAvliFlag("0");
				tLLFeeOtherItemSchema.setItemCode("301");

				LDCodeSchema tLDCodeSchema = mCachedLPInfo
						.findLLSecuFeeItemByCode("301");

				if (tLDCodeSchema != null) {
					tLLFeeOtherItemSchema.setDrugName(tLDCodeSchema
							.getCodeName());
					tLLFeeOtherItemSchema.setAvliReason(tLDCodeSchema
							.getCodeAlias());
					mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
				}

			}

			// 统筹基金支付
			if (mLLSecurityReceiptSchema.getPlanFee() > 0) {
				LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
				String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO",
						mLLSecurityReceiptSchema.getMngCom());
				if (tFeeDetailNo == null || "".equals(tFeeDetailNo)) {
					mErrors.addOneError("社保明细序号生成失败");
					return false;
				}
				Reflections tReflection = new Reflections();
				tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
				tReflection.transFields(tLLFeeOtherItemSchema,
						mLLSecurityReceiptSchema);
				tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
						.getPlanFee());
				tLLFeeOtherItemSchema.setItemClass("S");
				tLLFeeOtherItemSchema.setAvliFlag("0");
				tLLFeeOtherItemSchema.setItemCode("302");

				LDCodeSchema tLDCodeSchema = mCachedLPInfo
						.findLLSecuFeeItemByCode("302");

				if (tLDCodeSchema != null) {
					tLLFeeOtherItemSchema.setDrugName(tLDCodeSchema
							.getCodeName());
					tLLFeeOtherItemSchema.setAvliReason(tLDCodeSchema
							.getCodeAlias());
					mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
				}
			}
			
			//大额医疗支付
			if (mLLSecurityReceiptSchema.getSupInHosFee() > 0) {
				LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
				String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO",
						mLLSecurityReceiptSchema.getMngCom());
				if (tFeeDetailNo == null || "".equals(tFeeDetailNo)) {
					mErrors.addOneError("社保明细序号生成失败");
					return false;
				}
				Reflections tReflection = new Reflections();
				tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
				tReflection.transFields(tLLFeeOtherItemSchema,
						mLLSecurityReceiptSchema);
				tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
						.getSupInHosFee());
				tLLFeeOtherItemSchema.setItemClass("S");
				tLLFeeOtherItemSchema.setAvliFlag("0");
				tLLFeeOtherItemSchema.setItemCode("303");

				LDCodeSchema tLDCodeSchema = mCachedLPInfo
						.findLLSecuFeeItemByCode("303");

				if (tLDCodeSchema != null) {
					tLLFeeOtherItemSchema.setDrugName(tLDCodeSchema
							.getCodeName());
					tLLFeeOtherItemSchema.setAvliReason(tLDCodeSchema
							.getCodeAlias());
					mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
				}
			}

			//公务员医疗补助
			if (mLLSecurityReceiptSchema.getOfficialSubsidy() > 0) {
				LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
				String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO",
						mLLSecurityReceiptSchema.getMngCom());
				if (tFeeDetailNo == null || "".equals(tFeeDetailNo)) {
					mErrors.addOneError("社保明细序号生成失败");
					return false;
				}
				Reflections tReflection = new Reflections();
				tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
				tReflection.transFields(tLLFeeOtherItemSchema,
						mLLSecurityReceiptSchema);
				tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
						.getOfficialSubsidy());
				tLLFeeOtherItemSchema.setItemClass("S");
				tLLFeeOtherItemSchema.setAvliFlag("0");
				tLLFeeOtherItemSchema.setItemCode("304");

				LDCodeSchema tLDCodeSchema = mCachedLPInfo
						.findLLSecuFeeItemByCode("304");

				if (tLDCodeSchema != null) {
					tLLFeeOtherItemSchema.setDrugName(tLDCodeSchema
							.getCodeName());
					tLLFeeOtherItemSchema.setAvliReason(tLDCodeSchema
							.getCodeAlias());
					mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
				}
			}

			// 全自费
			if (mLLSecurityReceiptSchema.getSelfAmnt() > 0) {
				LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
				String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO",
						mLLSecurityReceiptSchema.getMngCom());
				if (tFeeDetailNo == null || "".equals(tFeeDetailNo)) {
					mErrors.addOneError("社保明细序号生成失败");
					return false;
				}
				Reflections tReflection = new Reflections();
				tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
				tReflection.transFields(tLLFeeOtherItemSchema,
						mLLSecurityReceiptSchema);
				tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
						.getSelfAmnt());
				tLLFeeOtherItemSchema.setItemClass("S");
				tLLFeeOtherItemSchema.setAvliFlag("0");
				tLLFeeOtherItemSchema.setItemCode("305");

				LDCodeSchema tLDCodeSchema = mCachedLPInfo
						.findLLSecuFeeItemByCode("305");

				if (tLDCodeSchema != null) {
					tLLFeeOtherItemSchema.setDrugName(tLDCodeSchema
							.getCodeName());
					tLLFeeOtherItemSchema.setAvliReason(tLDCodeSchema
							.getCodeAlias());
					mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
				}
			}

			// 自付二
			if (mLLSecurityReceiptSchema.getSelfPay2() > 0) {
				LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
				String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO",
						mLLSecurityReceiptSchema.getMngCom());
				if (tFeeDetailNo == null || "".equals(tFeeDetailNo)) {
					mErrors.addOneError("社保明细序号生成失败");
					return false;
				}
				Reflections tReflection = new Reflections();
				tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
				tReflection.transFields(tLLFeeOtherItemSchema,
						mLLSecurityReceiptSchema);
				tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
						.getSelfPay2());
				tLLFeeOtherItemSchema.setItemClass("S");
				tLLFeeOtherItemSchema.setAvliFlag("0");
				tLLFeeOtherItemSchema.setItemCode("306");

				LDCodeSchema tLDCodeSchema = mCachedLPInfo
						.findLLSecuFeeItemByCode("306");

				if (tLDCodeSchema != null) {
					tLLFeeOtherItemSchema.setDrugName(tLDCodeSchema
							.getCodeName());
					tLLFeeOtherItemSchema.setAvliReason(tLDCodeSchema
							.getCodeAlias());
					mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
				}
			}

			// 自付一
			if (mLLSecurityReceiptSchema.getSelfPay1() > 0) {
				LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
				String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO",
						mLLSecurityReceiptSchema.getMngCom());
				if (tFeeDetailNo == null || "".equals(tFeeDetailNo)) {
					mErrors.addOneError("社保明细序号生成失败");
					return false;
				}
				Reflections tReflection = new Reflections();
				tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
				tReflection.transFields(tLLFeeOtherItemSchema,
						mLLSecurityReceiptSchema);
				tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
						.getSelfPay1());
				tLLFeeOtherItemSchema.setItemClass("S");
				tLLFeeOtherItemSchema.setAvliFlag("0");
				tLLFeeOtherItemSchema.setItemCode("307");

				LDCodeSchema tLDCodeSchema = mCachedLPInfo
						.findLLSecuFeeItemByCode("307");

				if (tLDCodeSchema != null) {
					tLLFeeOtherItemSchema.setDrugName(tLDCodeSchema
							.getCodeName());
					tLLFeeOtherItemSchema.setAvliReason(tLDCodeSchema
							.getCodeAlias());
					mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
				}
			}

			// 起付线
			if (mLLSecurityReceiptSchema.getGetLimit() > 0) {
				LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
				String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO",
						mLLSecurityReceiptSchema.getMngCom());
				if (tFeeDetailNo == null || "".equals(tFeeDetailNo)) {
					mErrors.addOneError("社保明细序号生成失败");
					return false;
				}
				Reflections tReflection = new Reflections();
				tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
				tReflection.transFields(tLLFeeOtherItemSchema,
						mLLSecurityReceiptSchema);
				tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
						.getGetLimit());
				tLLFeeOtherItemSchema.setItemClass("S");
				tLLFeeOtherItemSchema.setAvliFlag("0");
				tLLFeeOtherItemSchema.setItemCode("308");

				LDCodeSchema tLDCodeSchema = mCachedLPInfo
						.findLLSecuFeeItemByCode("308");

				if (tLDCodeSchema != null) {
					tLLFeeOtherItemSchema.setDrugName(tLDCodeSchema
							.getCodeName());
					tLLFeeOtherItemSchema.setAvliReason(tLDCodeSchema
							.getCodeAlias());
					mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
				}
			}

			// 中段
			if (mLLSecurityReceiptSchema.getMidAmnt() > 0) {
				LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
				String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO",
						mLLSecurityReceiptSchema.getMngCom());
				if (tFeeDetailNo == null || "".equals(tFeeDetailNo)) {
					mErrors.addOneError("社保明细序号生成失败");
					return false;
				}
				Reflections tReflection = new Reflections();
				tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
				tReflection.transFields(tLLFeeOtherItemSchema,
						mLLSecurityReceiptSchema);
				tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
						.getMidAmnt());
				tLLFeeOtherItemSchema.setItemClass("S");
				tLLFeeOtherItemSchema.setAvliFlag("0");
				tLLFeeOtherItemSchema.setItemCode("309");

				LDCodeSchema tLDCodeSchema = mCachedLPInfo
						.findLLSecuFeeItemByCode("309");

				if (tLDCodeSchema != null) {
					tLLFeeOtherItemSchema.setDrugName(tLDCodeSchema
							.getCodeName());
					tLLFeeOtherItemSchema.setAvliReason(tLDCodeSchema
							.getCodeAlias());
					mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
				}
			}

			// 高段1
			if (mLLSecurityReceiptSchema.getHighAmnt1() > 0) {
				LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
				String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO",
						mLLSecurityReceiptSchema.getMngCom());
				if (tFeeDetailNo == null || "".equals(tFeeDetailNo)) {
					mErrors.addOneError("社保明细序号生成失败");
					return false;
				}
				Reflections tReflection = new Reflections();
				tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
				tReflection.transFields(tLLFeeOtherItemSchema,
						mLLSecurityReceiptSchema);
				tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
						.getHighAmnt1());
				tLLFeeOtherItemSchema.setItemClass("S");
				tLLFeeOtherItemSchema.setAvliFlag("0");
				tLLFeeOtherItemSchema.setItemCode("310");

				LDCodeSchema tLDCodeSchema = mCachedLPInfo
						.findLLSecuFeeItemByCode("310");

				if (tLDCodeSchema != null) {
					tLLFeeOtherItemSchema.setDrugName(tLDCodeSchema
							.getCodeName());
					tLLFeeOtherItemSchema.setAvliReason(tLDCodeSchema
							.getCodeAlias());
					mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
				}
			}
			
			// 高段2
			if (mLLSecurityReceiptSchema.getHighAmnt2() > 0) {
				LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
				String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO",
						mLLSecurityReceiptSchema.getMngCom());
				if (tFeeDetailNo == null || "".equals(tFeeDetailNo)) {
					mErrors.addOneError("社保明细序号生成失败");
					return false;
				}
				Reflections tReflection = new Reflections();
				tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
				tReflection.transFields(tLLFeeOtherItemSchema,
						mLLSecurityReceiptSchema);
				tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
						.getHighAmnt2());
				tLLFeeOtherItemSchema.setItemClass("S");
				tLLFeeOtherItemSchema.setAvliFlag("0");
				tLLFeeOtherItemSchema.setItemCode("311");

				LDCodeSchema tLDCodeSchema = mCachedLPInfo
						.findLLSecuFeeItemByCode("311");

				if (tLDCodeSchema != null) {
					tLLFeeOtherItemSchema.setDrugName(tLDCodeSchema
							.getCodeName());
					tLLFeeOtherItemSchema.setAvliReason(tLDCodeSchema
							.getCodeAlias());
					mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
				}
			}
			
			//超高段
			if (mLLSecurityReceiptSchema.getSuperAmnt() > 0) {
				LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
				String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO",
						mLLSecurityReceiptSchema.getMngCom());
				if (tFeeDetailNo == null || "".equals(tFeeDetailNo)) {
					mErrors.addOneError("社保明细序号生成失败");
					return false;
				}
				Reflections tReflection = new Reflections();
				tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
				tReflection.transFields(tLLFeeOtherItemSchema,
						mLLSecurityReceiptSchema);
				tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
						.getSuperAmnt());
				tLLFeeOtherItemSchema.setItemClass("S");
				tLLFeeOtherItemSchema.setAvliFlag("0");
				tLLFeeOtherItemSchema.setItemCode("312");

				LDCodeSchema tLDCodeSchema = mCachedLPInfo
						.findLLSecuFeeItemByCode("312");

				if (tLDCodeSchema != null) {
					tLLFeeOtherItemSchema.setDrugName(tLDCodeSchema
							.getCodeName());
					tLLFeeOtherItemSchema.setAvliReason(tLDCodeSchema
							.getCodeAlias());
					mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
				}
			}
			
			//退休补充医疗
			if (mLLSecurityReceiptSchema.getRetireAddFee() > 0) {
				LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
				String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO",
						mLLSecurityReceiptSchema.getMngCom());
				if (tFeeDetailNo == null || "".equals(tFeeDetailNo)) {
					mErrors.addOneError("社保明细序号生成失败");
					return false;
				}
				Reflections tReflection = new Reflections();
				tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
				tReflection.transFields(tLLFeeOtherItemSchema,
						mLLSecurityReceiptSchema);
				tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
						.getRetireAddFee());
				tLLFeeOtherItemSchema.setItemClass("S");
				tLLFeeOtherItemSchema.setAvliFlag("0");
				tLLFeeOtherItemSchema.setItemCode("313");

				LDCodeSchema tLDCodeSchema = mCachedLPInfo
						.findLLSecuFeeItemByCode("313");

				if (tLDCodeSchema != null) {
					tLLFeeOtherItemSchema.setDrugName(tLDCodeSchema
							.getCodeName());
					tLLFeeOtherItemSchema.setAvliReason(tLDCodeSchema
							.getCodeAlias());
					mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
				}
			}
			
			//小额门急诊
			if (mLLSecurityReceiptSchema.getSmallDoorPay() > 0) {
				LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
				String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO",
						mLLSecurityReceiptSchema.getMngCom());
				if (tFeeDetailNo == null || "".equals(tFeeDetailNo)) {
					mErrors.addOneError("社保明细序号生成失败");
					return false;
				}
				Reflections tReflection = new Reflections();
				tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
				tReflection.transFields(tLLFeeOtherItemSchema,
						mLLSecurityReceiptSchema);
				tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
						.getSmallDoorPay());
				tLLFeeOtherItemSchema.setItemClass("S");
				tLLFeeOtherItemSchema.setAvliFlag("0");
				tLLFeeOtherItemSchema.setItemCode("314");

				LDCodeSchema tLDCodeSchema = mCachedLPInfo
						.findLLSecuFeeItemByCode("314");

				if (tLDCodeSchema != null) {
					tLLFeeOtherItemSchema.setDrugName(tLDCodeSchema
							.getCodeName());
					tLLFeeOtherItemSchema.setAvliReason(tLDCodeSchema
							.getCodeAlias());
					mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
				}
			}
			
			//大额门急诊
			if (mLLSecurityReceiptSchema.getHighDoorAmnt() > 0) {
				LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
				String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO",
						mLLSecurityReceiptSchema.getMngCom());
				if (tFeeDetailNo == null || "".equals(tFeeDetailNo)) {
					mErrors.addOneError("社保明细序号生成失败");
					return false;
				}
				Reflections tReflection = new Reflections();
				tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
				tReflection.transFields(tLLFeeOtherItemSchema,
						mLLSecurityReceiptSchema);
				tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
						.getHighDoorAmnt());
				tLLFeeOtherItemSchema.setItemClass("S");
				tLLFeeOtherItemSchema.setAvliFlag("0");
				tLLFeeOtherItemSchema.setItemCode("315");

				LDCodeSchema tLDCodeSchema = mCachedLPInfo
						.findLLSecuFeeItemByCode("315");

				if (tLDCodeSchema != null) {
					tLLFeeOtherItemSchema.setDrugName(tLDCodeSchema
							.getCodeName());
					tLLFeeOtherItemSchema.setAvliReason(tLDCodeSchema
							.getCodeAlias());
					mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
				}
			}
			
			//医疗保险范围内金额 
			if (mLLSecurityReceiptSchema.getFeeInSecu() > 0) {
				LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
				String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO",
						mLLSecurityReceiptSchema.getMngCom());
				if (tFeeDetailNo == null || "".equals(tFeeDetailNo)) {
					mErrors.addOneError("社保明细序号生成失败");
					return false;
				}
				Reflections tReflection = new Reflections();
				tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
				tReflection.transFields(tLLFeeOtherItemSchema,
						mLLSecurityReceiptSchema);
				tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
						.getFeeInSecu());
				tLLFeeOtherItemSchema.setItemClass("S");
				tLLFeeOtherItemSchema.setAvliFlag("0");
				tLLFeeOtherItemSchema.setItemCode("316");

				LDCodeSchema tLDCodeSchema = mCachedLPInfo
						.findLLSecuFeeItemByCode("316");

				if (tLDCodeSchema != null) {
					tLLFeeOtherItemSchema.setDrugName(tLDCodeSchema
							.getCodeName());
					tLLFeeOtherItemSchema.setAvliReason(tLDCodeSchema
							.getCodeAlias());
					mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
				}
			}
			
			//年度门诊大额医疗互助基金累计支付   YEARSUPINHOSFEE
			if (mLLSecurityReceiptSchema.getYearSupInHosFee() > 0) {
				LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
				String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO",
						mLLSecurityReceiptSchema.getMngCom());
				if (tFeeDetailNo == null || "".equals(tFeeDetailNo)) {
					mErrors.addOneError("社保明细序号生成失败");
					return false;
				}
				Reflections tReflection = new Reflections();
				tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
				tReflection.transFields(tLLFeeOtherItemSchema,
						mLLSecurityReceiptSchema);
				tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
						.getYearSupInHosFee());
				tLLFeeOtherItemSchema.setItemClass("S");
				tLLFeeOtherItemSchema.setAvliFlag("0");
				tLLFeeOtherItemSchema.setItemCode("318");

				LDCodeSchema tLDCodeSchema = mCachedLPInfo
						.findLLSecuFeeItemByCode("318");

				if (tLDCodeSchema != null) {
					tLLFeeOtherItemSchema.setDrugName(tLDCodeSchema
							.getCodeName());
					tLLFeeOtherItemSchema.setAvliReason(tLDCodeSchema
							.getCodeAlias());
					mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
				}
			}
		}
		return true;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
