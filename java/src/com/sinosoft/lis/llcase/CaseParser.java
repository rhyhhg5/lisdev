/*
 * @(#)GrpPolParser.java	2005-11-12
 * Xx
 * Copyright 2005 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.llcase;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import org.apache.xpath.XPathAPI;
import org.apache.xpath.objects.XObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: 磁盘投保的解析类，从一个XML数据流中解析出批次下分案信息</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author Xx
 * @version 6.0
*/
public class CaseParser {

    // private static String PATH_GROUPCONTNO = "GRPCONTNO";
//private static String PATH_INSURED = "SUBINSUREDTABLE/ROW";
    private String PATH_ROW = "ROW";
    //被保险人
    private static String PATH_INSURED = "ROW";
    private static String PATH_CustomerNo = "CustomerNo";
    private static String PATH_ContNo = "ContNo";
    private static String PATH_CustomerName = "CustomerName";
    private static String PATH_Passport = "Passport";
    private static String PATH_IDNo = "IDNo";
    private static String PATH_SalvationNo = "SalvationNo";
    private static String PATH_SRegisterDate = "SRegisterDate";
    private static String PATH_SRegisterTime = "SRegisterTime";
    private static String PATH_ReportPhone = "ReportPhone";
    private static String PATH_Hospital = "Hospital";
    private static String PATH_DrName = "DrName";
    private static String PATH_DrLicense = "DrLicense";
    private static String PATH_AccDate = "AccDate";
    private static String PATH_AccBJDate = "AccBJDate";
    private static String PATH_AccCountry = "AccCountry";
    private static String PATH_AccDistrict = "AccDistrict";
    private static String PATH_CustState = "CustState";
    private static String PATH_DeathDate = "DeathDate";
    private static String PATH_AccDesc = "AccDesc";
    private static String PATH_AccType = "AccType";
    private static String PATH_ICDCode1 = "ICDCode1";
    private static String PATH_ICDCode2 = "ICDCode2";
    private static String PATH_ICDCode3 = "ICDCode3";
    private static String PATH_EmergTransferFee = "EmergTransferFee";
    private static String PATH_OtherTransferFee = "OtherTransferFee";
    private static String PATH_EscortFee = "EscortFee";
    private static String PATH_CompanyIHFee = "CompanyIHFee";
    private static String PATH_RepatriationFee = "RepatriationFee";
    private static String PATH_ChildRepatrFee = "ChildRepatrFee";
    private static String PATH_RelativeViaticumFee = "RelativeViaticumFee";
    private static String PATH_RelativeAccomodateFee = "RelativeAccomodateFee";
    private static String PATH_CoffinFee = "CoffinFee";
    private static String PATH_CoffinTransferFee = "CoffinTransferFee";
    private static String PATH_CremationFee = "CremationFee";
    private static String PATH_UrnFee = "UrnFee";
    private static String PATH_BuryFee = "BuryFee";
    private static String PATH_InpatientFee = "InpatientFee";
    private static String PATH_EmergencyFee = "EmergencyFee";
    private static String PATH_DentistryFee = "DentistryFee";
    private static String PATH_LawHelp = "LawHelp";
    private static String PATH_TripConsult = "TripConsult";
    private static String PATH_Translate = "Translate";
    private static String PATH_LostFound = "LostFound";
    private static String PATH_ServiceFee = "ServiceFee";
    private static String PATH_FeeSum = "FeeSum";


    // 下面是一些常量定义
    private static String PATH_ID = "ID";
//  private static String PATH_GrpContNo = "GrpContNo";
    private static String PATH_GrpPolNo = "GrpPolNo";
    private static String PATH_POL_ContNo = "PolContNo";
    private static String PATH_RiskCode = "RiskCode";
    private static String PATH_InSuredNo = "InsuredNo";
    private static String PATH_POL_Other_Insured = "OtherInsured";
    private static String PATH_MainPolNo = "MainPolNo";
    private static String PATH_OutPayFlag = "OutPayFlag";
    private static String PATH_PayLocation = "PayLocation";

    private static String PATH_LiveGetMode = "LiveGetMode";
    private static String PATH_GetDutyKind = "GetDutyKind";
    private static String PATH_GetIntv = "GetIntv";

    private static String PATH_Mult = "Mult";
    private static String PATH_Prem = "Prem";
    private static String PATH_Amnt = "Amnt";
    private static String PATH_RnewFlag = "RnewFlag";
    private static String PATH_SpecifyValiDate = "SpecifyValiDate";
    private static String PATH_FloatRate = "FloatRate";
    private static String PATH_Remark = "Remark";
    private static String PATH_PremToAmnt = "PremToAmnt";
    private static String PATH_CalRule = "CalRule";
    private static String PATH_GetLimit = "GetLimit";
    private static String PATH_GetRate = "GetRate";
    private static String PATH_StandbyFlag1 = "StandbyFlag1";
    private static String PATH_StandbyFlag2 = "StandbyFlag2";
    private static String PATH_StandbyFlag3 = "StandbyFlag3";
    private static String PATH_SavePolType = "SavePolType";
    private static String PATH_CValiDate = "CValiDate";

    private static String PATH_PREM = "SUBPREMTABLE/ROW";
    private static String PATH_DutyCode = "DutyCode";
    private static String PATH_PayPlanCode = "PayPlanCode";
    private static String PATH_PayPlanRate = "PayPlanRate";
    private static String PATH_PayPlanPrem = "PayPlanPrem";
    private static String PATH_InsuAccNo = "InsuAccNo";
    private static String PATH_InsuAccRate = "InsuAccRate";
    private static String PATH_ManageFeeRate = "ManageFeeRate";

    private static String PATH_DUTY = "SUBDUTYTABLE/ROW";
//    private static String PATH_DutyCode1 = "DutyCode1";
    private static String PATH_PayIntv = "PayIntv";
    private static String PATH_InsuYear = "InsuYear";
    private static String PATH_InsuYearFlag = "InsuYearFlag";
    private static String PATH_PayEndYear = "PayEndYear";
    private static String PATH_PayEndYearFlag = "PayEndYearFlag";
    private static String PATH_GetYear = "GetYear";
    private static String PATH_GetYearFlag = "GetYearFlag";
    private static String PATH_GetStartType = "GetStartType";

    private static String PATH_BNF = "ROW";
    private static String PATH_BNF_ContId = "ContIdBNF";
    private static String PATH_INSUREDID = "InsuredId";
    private static String PATH_BNF_RiskCode = "RiskCode";
    private static String PATH_BnfType = "BnfType";
    private static String PATH_BnfName = "BnfName";
    private static String PATH_BnfSex = "BnfSex";
    private static String PATH_BnfIDType = "BnfIDType";
    private static String PATH_BnfIDNo = "BnfIDNo";
    private static String PATH_BnfBirthday = "BnfBirthday";
    private static String PATH_RelationToInsured = "RelationToInsured";
    private static String PATH_BnfLot = "BnfLot";
    private static String PATH_BnfGrade = "BnfGrade";


    private static String PATH_INS_RELA = "ROW";
    private static String PATH_INS_RELA_CONTID = "ContIdRELA";
    private static String PATH_INS_RELA_INSUREDID = "InsuredId";
    private static String PATH_INS_RELA_RISKCODE = "RiskCode";
    private static String PATH_INS_RELA_ID = "RelaId";

    private String mBatchNo = "";
    public CErrors mErrors = new CErrors();

    public CaseParser() {
    }


    /**
     * 解析被保险人结点
     * @param node Node
     * @return VData
     */
    public VData parseOneInsuredNode(Node node) {
        VData tReturn = new VData();
        // 得到被保人信息
//        LLCaseImportListSet tLLCaseImportListSet = getCaseImportSet(node);
//        if (tLLCaseImportListSet == null || tLLCaseImportListSet.size() <= 0) {
//            return null;
//        }

//        tReturn.add(tLLCaseImportListSet);
        return tReturn;
    }

    public boolean setBatchNo(String aBatchNo){
        mBatchNo = aBatchNo;
        return true;
    }

    public VData parseLCPolNode(Node node) {
        NodeList nodeList = null;

        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_ROW);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
            return null;
        }
        VData reData = new VData();
        int nLength = nodeList == null ? 0 : nodeList.getLength();
        //解析<LCPOLTABLE>下每个<ROW>
        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            VData tData = new VData();
            if (parseOneNode(nodeList.item(nIndex), tData)) {
                reData.add(tData);
            }
        }
        return reData;
    }

    /**
     * 解析一个DOM树的节点，在这个节点中，包含了一个团体下个人保单的所有信息。
     * @param node Node
     * @param vReturn VData
     * @return boolean
     */
    public boolean parseOneNode(Node node, VData vReturn) {
        try {
            if (vReturn == null) {
                buildError("parseOneNode", "存放返回数据的VData是非法的");
                return false;
            }

            // 一些特殊的信息
            TransferData tTransferData = getTransferData(node);

            // 个人险种信息
            LCPolSchema tLCPolSchema = getLCPolSchema(node);
            String id = (String) tTransferData.getValueByName("ID");

            if ("".equals(StrTool.cTrim(tLCPolSchema.getInsuredNo()))) {
                CError.buildErr(this, "保单[" + id + "]没有填写被保险人ID");
                return false;
            }
            if ("".equals(StrTool.cTrim(tLCPolSchema.getContNo()))) {
                CError.buildErr(this, "保单[" + id + "]没有填写合同ID");
                return false;
            }
            if ("".equals(StrTool.cTrim(tLCPolSchema.getRiskCode()))) {
                CError.buildErr(this, "保单[" + id + "]没有填写险种代码");
                return false;
            }
            String PolKey = tLCPolSchema.getContNo() + "-"
                            + tLCPolSchema.getInsuredNo() + "-"
                            + tLCPolSchema.getRiskCode();
            tTransferData.setNameAndValue("PolKey", PolKey);
            tTransferData.setNameAndValue("ContId", tLCPolSchema.getContNo());

            // 得到保费项金额及关联帐户比例
            VData tVData = getPremRelated(node);
            LCPremSet tLCPremSet =
                    (LCPremSet) tVData.getObjectByObjectName("LCPremSet", 0);
            LCPremToAccSet tLCPremToAccSet =
                    (LCPremToAccSet) tVData.getObjectByObjectName(
                            "LCPremToAccSet", 0);

            // 得到保险责任计算信息
            LCDutySet tLCDutySet = getLCDutySet(node);

            if (tLCDutySet != null && tLCDutySet.size() == 1) {
                LCDutySchema tDutySchema = tLCDutySet.get(1);
                tDutySchema.setAmnt(tLCPolSchema.getAmnt());
                tDutySchema.setPrem(tLCPolSchema.getPrem());
                tDutySchema.setMult(tLCPolSchema.getMult());

            }

            // 得到被保人信息
            // LCInsuredSet tLCInsuredSet = getLCInsuredSet(node);


            // 得到受益人信息
            //LCBnfSet tLCBnfSet = getLCBnfSet(node);

            // 返回数据
            vReturn.add(tTransferData);
            vReturn.add(tLCPolSchema);
            vReturn.add(tLCPremSet);
            vReturn.add(tLCPremToAccSet);
            vReturn.add(tLCDutySet);
            // vReturn.add(tLCInsuredSet);
            // vReturn.add(tLCBnfSet);

        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

        return true;
    }


    /**
     * 利用XPathAPI取得某个节点的节点值
     * @param node Node
     * @param strPath String
     * @return String
     */
    private static String parseNode(Node node, String strPath) {
        String strValue = "";

        try {
            XObject xobj = XPathAPI.eval(node, strPath);
            strValue = xobj == null ? "" : xobj.toString();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return strValue;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "GrpPolParser";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 得到一些不好用Schema表示的信息
     * @param node Node
     * @return TransferData
     */
    private static TransferData getTransferData(Node node) {
        TransferData tTransferData = new TransferData();
        //  tTransferData.setNameAndValue("GrpContNo",parseNode(node,PATH_GROUPCONTNO));;
        tTransferData.setNameAndValue("GetDutyKind",
                                      parseNode(node, PATH_GetDutyKind));
        tTransferData.setNameAndValue("GetIntv", parseNode(node, PATH_GetIntv));
        tTransferData.setNameAndValue("ID", parseNode(node, PATH_ID));
        tTransferData.setNameAndValue("SavePolType",
                                      parseNode(node, PATH_SavePolType));

        return tTransferData;
    }


    /**
     * 得到个人险种保单的信息
     * @param node Node
     * @return LCPolSchema
     */
    private static LCPolSchema getLCPolSchema(Node node) {
        LCPolSchema tLCPolSchema = new LCPolSchema();

        tLCPolSchema.setGrpPolNo(parseNode(node, PATH_GrpPolNo));

        tLCPolSchema.setContNo(parseNode(node, CaseParser.PATH_POL_ContNo));
        tLCPolSchema.setRiskCode(parseNode(node, PATH_RiskCode));
        tLCPolSchema.setInsuredNo(parseNode(node, PATH_InSuredNo));

        tLCPolSchema.setMainPolNo(parseNode(node, PATH_MainPolNo));
        //借用AppFlag保存连身被保险人
        tLCPolSchema.setAppFlag(parseNode(node,
                                          CaseParser.PATH_POL_Other_Insured));

        tLCPolSchema.setPayLocation(parseNode(node, PATH_PayLocation));

        tLCPolSchema.setLiveGetMode(parseNode(node, PATH_LiveGetMode));

        tLCPolSchema.setPayIntv(parseNode(node, PATH_PayIntv));
        tLCPolSchema.setInsuYear(parseNode(node, PATH_InsuYear));
        tLCPolSchema.setInsuYearFlag(parseNode(node, PATH_InsuYearFlag));
        tLCPolSchema.setPayEndYear(parseNode(node, PATH_PayEndYear));
        tLCPolSchema.setPayEndYearFlag(parseNode(node, PATH_PayEndYearFlag));
        tLCPolSchema.setGetYear(parseNode(node, PATH_GetYear));
        tLCPolSchema.setGetYearFlag(parseNode(node, PATH_GetYearFlag));
        tLCPolSchema.setGetStartType(parseNode(node, PATH_GetStartType));

        tLCPolSchema.setMult(parseNode(node, PATH_Mult));
        tLCPolSchema.setPrem(parseNode(node, PATH_Prem));
        tLCPolSchema.setAmnt(parseNode(node, PATH_Amnt));
        tLCPolSchema.setRnewFlag(parseNode(node, PATH_RnewFlag));
        tLCPolSchema.setSpecifyValiDate(parseNode(node, PATH_SpecifyValiDate));
        //指定生效日期标志 为"1"才设置 生效日期
        if (tLCPolSchema.getSpecifyValiDate().equals("1")) {
            tLCPolSchema.setCValiDate(parseNode(node, PATH_CValiDate));
        }
        tLCPolSchema.setOccupationType(parseNode(node,
                                                 CaseParser.
                                                 PATH_POL_Other_Insured));
        tLCPolSchema.setFloatRate(parseNode(node, PATH_FloatRate));
        tLCPolSchema.setRemark(parseNode(node, PATH_Remark));
        //计算方向
        tLCPolSchema.setPremToAmnt(parseNode(node, PATH_PremToAmnt));
        //借用 最终核保人编码UWCode 缓存 计算规则
        tLCPolSchema.setUWCode(parseNode(node, PATH_CalRule));
//        tLCPolSchema.setPolTypeFlag(parseNode(node, PATH_PolTypeFlag));

        tLCPolSchema.setStandbyFlag1(parseNode(node, PATH_StandbyFlag1));
        tLCPolSchema.setStandbyFlag2(parseNode(node, PATH_StandbyFlag2));
        tLCPolSchema.setStandbyFlag3(parseNode(node, PATH_StandbyFlag3));
//        tLCPolSchema.setInsuredPeoples(parseNode(node, PATH_InsuredPeoples));

        //借用[复核状态]缓存 免赔额
        tLCPolSchema.setApproveFlag(parseNode(node, PATH_GetLimit));
        //借用[核保状态]缓存 赔付比例
        tLCPolSchema.setUWFlag(parseNode(node, PATH_GetRate));

        return tLCPolSchema;
    }


    /**
     * 连身被保险人信息
     * @param node Node
     * @return SchemaSet
     */
    public SchemaSet getLCInsuredRelatedSet(Node node) {
        LCInsuredRelatedSet tSet = new LCInsuredRelatedSet();
        NodeList nodeList = null;

        try {
            nodeList = XPathAPI.selectNodeList(node, CaseParser.PATH_INS_RELA);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
        }

        int nLength = nodeList == null ? 0 : nodeList.getLength();
        //  LCBnfSet tLCBnfSet = new LCBnfSet();

        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeBnf = nodeList.item(nIndex);
            LCInsuredRelatedSchema tLCInsuredRelatedSchema = new
                    LCInsuredRelatedSchema();

            tLCInsuredRelatedSchema.setMainCustomerNo(parseNode(nodeBnf,
                    CaseParser.PATH_INS_RELA_INSUREDID));
            //被保险人序号
            tLCInsuredRelatedSchema.setCustomerNo(parseNode(nodeBnf,
                    CaseParser.PATH_INS_RELA_ID));
            tLCInsuredRelatedSchema.setOperator(parseNode(nodeBnf,
                    CaseParser.PATH_INS_RELA_CONTID)); //借用，保存ContId
            tLCInsuredRelatedSchema.setPolNo(parseNode(nodeBnf,
                    PATH_INS_RELA_RISKCODE)); //Riskcode

            tSet.add(tLCInsuredRelatedSchema);
        }

        return tSet;

    }


    /**
     * 得到跟保费相关的信息:保费项金额以及关联帐户比例
     * @param node Node
     * @return VData
     */
    private static VData getPremRelated(Node node) {
        NodeList nodeList = null;

        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_PREM);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
        }

        int nLength = nodeList == null ? 0 : nodeList.getLength();

        LCPremToAccSet tLCPremToAccSet = new LCPremToAccSet();
        LCPremSet tLCPremSet = new LCPremSet();

        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodePrem = nodeList.item(nIndex);
            //保费项表和客户帐户表的关联表
            LCPremToAccSchema tLCPremToAccSchema = new LCPremToAccSchema();

            tLCPremToAccSchema.setDutyCode(parseNode(nodePrem, PATH_DutyCode));
            tLCPremToAccSchema.setPayPlanCode(parseNode(nodePrem,
                    PATH_PayPlanCode));
            tLCPremToAccSchema.setInsuAccNo(parseNode(nodePrem, PATH_InsuAccNo));
            tLCPremToAccSchema.setRate(parseNode(nodePrem, PATH_InsuAccRate));

            tLCPremToAccSet.add(tLCPremToAccSchema);

            //保费项表(目前仅对众悦年金有用<保费保额不用计算的>,在承保后台会判断处理)
            LCPremSchema tLCPremSchema = new LCPremSchema();

            tLCPremSchema.setDutyCode(tLCPremToAccSchema.getDutyCode()); //责任编码
            tLCPremSchema.setPayPlanCode(tLCPremToAccSchema.getPayPlanCode()); //缴费计划编码
            tLCPremSchema.setStandPrem(parseNode(nodePrem, PATH_PayPlanPrem));
            tLCPremSchema.setRate(parseNode(nodePrem, PATH_PayPlanRate));
            /*Lis5.3 upgrade set
             tLCPremSchema.setManageFeeRate( parseNode(nodePrem, PATH_ManageFeeRate) );
             */
            tLCPremSchema.setStandPrem(tLCPremSchema.getStandPrem() *
                                       tLCPremSchema.getRate()); //标准保费
            tLCPremSchema.setPrem(tLCPremSchema.getStandPrem()); //实际保费
            tLCPremSet.add(tLCPremSchema);
        }

        VData tVData = new VData();

        tVData.add(tLCPremToAccSet);
        tVData.add(tLCPremSet);

        return tVData;
    }


    /**
     * 得到保单责任计算信息
     * @param node Node
     * @return LCDutySet
     */
    private static LCDutySet getLCDutySet(Node node) {
        NodeList nodeList = null;

        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_DUTY);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
        }

        int nLength = nodeList == null ? 0 : nodeList.getLength();
        LCDutySet tLCDutySet = new LCDutySet();

        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeDuty = nodeList.item(nIndex);

            LCDutySchema tLCDutySchema = new LCDutySchema();

            tLCDutySchema.setDutyCode(parseNode(nodeDuty, PATH_DutyCode));
            tLCDutySchema.setPayIntv(parseNode(nodeDuty, PATH_PayIntv));
            tLCDutySchema.setInsuYear(parseNode(nodeDuty, PATH_InsuYear));
            tLCDutySchema.setInsuYearFlag(parseNode(nodeDuty, PATH_InsuYearFlag));
            tLCDutySchema.setPayEndYear(parseNode(nodeDuty, PATH_PayEndYear));
            tLCDutySchema.setPayEndYearFlag(parseNode(nodeDuty,
                    PATH_PayEndYearFlag));
            tLCDutySchema.setGetYear(parseNode(nodeDuty, PATH_GetYear));
            tLCDutySchema.setGetYearFlag(parseNode(nodeDuty, PATH_GetYearFlag));
            tLCDutySchema.setGetStartType(parseNode(nodeDuty, PATH_GetStartType));

            tLCDutySchema.setMult(parseNode(nodeDuty, PATH_Mult));
            tLCDutySchema.setPrem(parseNode(nodeDuty, PATH_Prem));
            tLCDutySchema.setAmnt(parseNode(nodeDuty, PATH_Amnt));
            tLCDutySchema.setFloatRate(parseNode(nodeDuty, PATH_FloatRate));
            //计算方向
            tLCDutySchema.setPremToAmnt(parseNode(nodeDuty, PATH_PremToAmnt));
            //计算规则
            tLCDutySchema.setCalRule(parseNode(nodeDuty, PATH_CalRule));
            tLCDutySchema.setGetLimit(parseNode(nodeDuty, PATH_GetLimit));
            tLCDutySchema.setGetRate(parseNode(nodeDuty, PATH_GetRate));
            tLCDutySchema.setStandbyFlag1(parseNode(nodeDuty, PATH_StandbyFlag1));
            tLCDutySchema.setStandbyFlag2(parseNode(nodeDuty, PATH_StandbyFlag2));
            tLCDutySchema.setStandbyFlag3(parseNode(nodeDuty, PATH_StandbyFlag3));

            tLCDutySet.add(tLCDutySchema);
        }

        return tLCDutySet;
    }


    /**
     * 得到导入案件信息，明细到事件
     * @param node Node
     * @return LCInsuredSet
    public LLCaseImportListSet getCaseImportSet(Node node) {
        NodeList nodeList = null;

        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_INSURED);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
            return null;
        }

        int nLength = nodeList == null ? 0 : nodeList.getLength();
        LLCaseImportListSet tLLCaseImportListSet = new LLCaseImportListSet();
        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeInsured = nodeList.item(nIndex);
            LLCaseImportListSchema tLLCaseImportListSchema = new LLCaseImportListSchema();
            tLLCaseImportListSchema.setSerialNo(nIndex+"");
            tLLCaseImportListSchema.setBatchNo(mBatchNo);
            tLLCaseImportListSchema.setContNo(parseNode(nodeInsured,
                                                 CaseParser.PATH_ContNo));
            tLLCaseImportListSchema.setCustomerNo(parseNode(nodeInsured,CaseParser.PATH_CustomerNo));
            tLLCaseImportListSchema.setCustomerName(parseNode(nodeInsured,
                                                 CaseParser.PATH_CustomerName));
            tLLCaseImportListSchema.setPassport(parseNode(nodeInsured,
                                                 CaseParser.PATH_Passport));
            tLLCaseImportListSchema.setIDNo(parseNode(nodeInsured,
                                                 CaseParser.PATH_IDNo));
            tLLCaseImportListSchema.setSalvationNo(parseNode(nodeInsured,
                                                 CaseParser.PATH_SalvationNo));
            tLLCaseImportListSchema.setSRegisterDate(parseNode(nodeInsured,
                                                 CaseParser.PATH_SRegisterDate));
            tLLCaseImportListSchema.setSRegisterTime(parseNode(nodeInsured,
                                                 CaseParser.PATH_SRegisterTime));
            tLLCaseImportListSchema.setReportPhone(parseNode(nodeInsured,
                                                 CaseParser.PATH_ReportPhone));
            tLLCaseImportListSchema.setHospital(parseNode(nodeInsured,
                                                 CaseParser.PATH_Hospital));
            tLLCaseImportListSchema.setDrName(parseNode(nodeInsured,
                                                 CaseParser.PATH_DrName));
            tLLCaseImportListSchema.setDrLicense(parseNode(nodeInsured,
                                                 CaseParser.PATH_DrLicense));
            tLLCaseImportListSchema.setAccDate(parseNode(nodeInsured,
                                                 CaseParser.PATH_AccDate));
            tLLCaseImportListSchema.setAccBJDate(parseNode(nodeInsured,
                                                 CaseParser.PATH_AccBJDate));
            tLLCaseImportListSchema.setAccCountry(parseNode(nodeInsured,
                                                 CaseParser.PATH_AccCountry));
            tLLCaseImportListSchema.setAccDistrict(parseNode(nodeInsured,
                                                 CaseParser.PATH_AccDistrict));
            tLLCaseImportListSchema.setCustState(parseNode(nodeInsured,
                                                 CaseParser.PATH_CustState));
            tLLCaseImportListSchema.setDeathDate(parseNode(nodeInsured,
                                                 CaseParser.PATH_DeathDate));
            tLLCaseImportListSchema.setAccDesc(parseNode(nodeInsured,
                                                 CaseParser.PATH_AccDesc));
            tLLCaseImportListSchema.setAccType(parseNode(nodeInsured,
                                                 CaseParser.PATH_AccType));

            tLLCaseImportListSchema.setICDCode1(parseNode(nodeInsured,
                                                 CaseParser.PATH_ICDCode1));
            tLLCaseImportListSchema.setICDCode2(parseNode(nodeInsured,
                                                 CaseParser.PATH_ICDCode2));
            tLLCaseImportListSchema.setAccType(parseNode(nodeInsured,
                                                 CaseParser.PATH_ICDCode3));

            tLLCaseImportListSet.add(tLLCaseImportListSchema);
        }

        return tLLCaseImportListSet;
    }
     */

    /**
     * 得到账单细目表
     * @param node Node
     * @return LCInsuredSet
     */
    public LLCaseReceiptSet getLLCaseReceipt(Node node) {
        NodeList nodeList = null;
        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_INSURED);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
            return null;
        }

        int nLength = nodeList == null ? 0 : nodeList.getLength();
        LLCaseReceiptSet tLLCaseReceiptSet = new LLCaseReceiptSet();
        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeInsured = nodeList.item(nIndex);
            LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
            LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
            LLCaseReceiptSchema tLLCaseReceiptSchema = null;
            tLLFeeMainSchema.setCustomerNo(parseNode(nodeInsured,
                    PATH_CustomerNo));
            tLLFeeMainSchema.setCustomerName(parseNode(nodeInsured,
                    PATH_CustomerName));

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY001");
            tLLCaseReceiptSchema.setFeeItemName("转移就近医院费");
            tLLCaseReceiptSchema.setFee(parseNode(nodeInsured,
                    CaseParser.PATH_EmergTransferFee));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY002");
            tLLCaseReceiptSchema.setFeeItemName("转移其他医院费");
            tLLCaseReceiptSchema.setFee(parseNode(nodeInsured,
                    CaseParser.PATH_OtherTransferFee));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY003");
            tLLCaseReceiptSchema.setFeeItemName("医护人员护送费");
            tLLCaseReceiptSchema.setFee(parseNode(nodeInsured,
                    CaseParser.PATH_EscortFee));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY004");
            tLLCaseReceiptSchema.setFeeItemName("陪同住院费");
            tLLCaseReceiptSchema.setFee(parseNode(nodeInsured,
                    CaseParser.PATH_CompanyIHFee));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY005");
            tLLCaseReceiptSchema.setFeeItemName("转送回国费");
            tLLCaseReceiptSchema.setFee(parseNode(nodeInsured,
                    CaseParser.PATH_RepatriationFee));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY006");
            tLLCaseReceiptSchema.setFeeItemName("安排子女回国");
            tLLCaseReceiptSchema.setAvaliFlag(parseNode(nodeInsured,
                    CaseParser.PATH_ChildRepatrFee));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY007");
            tLLCaseReceiptSchema.setFeeItemName("亲属往返交通费");
            tLLCaseReceiptSchema.setFee(parseNode(nodeInsured,
                    CaseParser.PATH_RelativeViaticumFee));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY008");
            tLLCaseReceiptSchema.setFeeItemName("亲属住宿费");
            tLLCaseReceiptSchema.setFee(parseNode(nodeInsured,
                    CaseParser.PATH_RelativeAccomodateFee));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY009");
            tLLCaseReceiptSchema.setFeeItemName("灵柩费");
            tLLCaseReceiptSchema.setFee(parseNode(nodeInsured,
                    CaseParser.PATH_CoffinFee));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY010");
            tLLCaseReceiptSchema.setFeeItemName("灵柩运送费");
            tLLCaseReceiptSchema.setFee(parseNode(nodeInsured,
                    CaseParser.PATH_CoffinTransferFee));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY011");
            tLLCaseReceiptSchema.setFeeItemName("火葬费");
            tLLCaseReceiptSchema.setFee(parseNode(nodeInsured,
                    CaseParser.PATH_CremationFee));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY012");
            tLLCaseReceiptSchema.setFeeItemName("骨灰盒费");
            tLLCaseReceiptSchema.setFee(parseNode(nodeInsured,
                    CaseParser.PATH_UrnFee));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY013");
            tLLCaseReceiptSchema.setFeeItemName("就地安葬费");
            tLLCaseReceiptSchema.setFee(parseNode(nodeInsured,
                    CaseParser.PATH_BuryFee));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY014");
            tLLCaseReceiptSchema.setFeeItemName("住院医疗费");
            tLLCaseReceiptSchema.setFee(parseNode(nodeInsured,
                    CaseParser.PATH_InpatientFee));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY015");
            tLLCaseReceiptSchema.setFeeItemName("紧急门诊费");
            tLLCaseReceiptSchema.setFee(parseNode(nodeInsured,
                    CaseParser.PATH_EmergencyFee));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY016");
            tLLCaseReceiptSchema.setFeeItemName("法律援助");
            tLLCaseReceiptSchema.setAvaliFlag(parseNode(nodeInsured,
                    CaseParser.PATH_LawHelp));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY017");
            tLLCaseReceiptSchema.setFeeItemName("旅行前咨询");
            tLLCaseReceiptSchema.setAvaliFlag(parseNode(nodeInsured,
                    CaseParser.PATH_TripConsult));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY018");
            tLLCaseReceiptSchema.setFeeItemName("翻译服务");
            tLLCaseReceiptSchema.setAvaliFlag(parseNode(nodeInsured,
                    CaseParser.PATH_Translate));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeItemCode("JY019");
            tLLCaseReceiptSchema.setFeeItemName("证件行李丢失援助");
            tLLCaseReceiptSchema.setAvaliFlag(parseNode(nodeInsured,
                    CaseParser.PATH_LostFound));
            tLLCaseReceiptSet.add(tLLCaseReceiptSchema);

        }

        return tLLCaseReceiptSet;
    }

    /**
     * 得到疾病信息表
     * @param node Node
     * @return LCInsuredSet
     */
    public LLCaseCureSet getLLCaseCure(Node node) {
        NodeList nodeList = null;
        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_INSURED);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
            return null;
        }

        int nLength = nodeList == null ? 0 : nodeList.getLength();
        LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeInsured = nodeList.item(nIndex);
            LLCaseCureSchema tLLCaseCureSchema = null;
            String tCustomerNo  = parseNode(nodeInsured,PATH_CustomerNo);
            String tCustomerName  = parseNode(nodeInsured,PATH_CustomerName);
            String tDoctorNo = parseNode(nodeInsured,PATH_DrLicense);
            String tDoctorName = parseNode(nodeInsured,PATH_DrName);

            tLLCaseCureSchema = new LLCaseCureSchema();
            tLLCaseCureSchema.setCustomerNo(tCustomerNo);
            tLLCaseCureSchema.setCustomerName(tCustomerName);
            tLLCaseCureSchema.setDoctorNo(tDoctorNo);
            tLLCaseCureSchema.setDoctorName(tDoctorName);
            tLLCaseCureSchema.setDiseaseCode(parseNode(nodeInsured,
                    CaseParser.PATH_ICDCode1));
            tLLCaseCureSet.add(tLLCaseCureSchema);

            tLLCaseCureSchema = new LLCaseCureSchema();
            tLLCaseCureSchema.setCustomerNo(tCustomerNo);
            tLLCaseCureSchema.setCustomerName(tCustomerName);
            tLLCaseCureSchema.setDiseaseCode(parseNode(nodeInsured,
                    CaseParser.PATH_ICDCode2));
            tLLCaseCureSet.add(tLLCaseCureSchema);

            tLLCaseCureSchema = new LLCaseCureSchema();
            tLLCaseCureSchema.setCustomerNo(tCustomerNo);
            tLLCaseCureSchema.setCustomerName(tCustomerName);
            tLLCaseCureSchema.setDiseaseCode(parseNode(nodeInsured,
                    CaseParser.PATH_ICDCode3));
            tLLCaseCureSet.add(tLLCaseCureSchema);

        }

        return tLLCaseCureSet;
    }
}

