package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:案件－分案－分案诊疗明细数据接收类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @version 1.0
 */
public class QueryCodeUI
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData = new VData();
  private VData mResult = new VData();
  private String mOperate;

  public QueryCodeUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    System.out.println("2003-10-16QueryCodeUI－－1");
    this.mOperate = cOperate;
    QueryCodeBL tQueryCodeBL = new QueryCodeBL();
    if (tQueryCodeBL.submitData(cInputData,mOperate) == false)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tQueryCodeBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "QueryCodeUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
    {
      System.out.println("2003-10-16QueryCodeUI－－2");
      mResult = tQueryCodeBL.getResult();
    }
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }
/*
  public static void main(String[] args)
  {
    CaseCureUI tCaseCureUI=new CaseCureUI();

    VData tVData=new VData();

    LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();

    tLLCaseCureSchema.setSerialNo("1");
    tLLCaseCureSchema.setCaseNo("1");
    tLLCaseCureSchema.setMngCom("ddd");
    tLLCaseCureSchema.setOperator("ddd");
    tLLCaseCureSchema.setMakeDate("2002-10-10");
    tLLCaseCureSchema.setMakeTime("12:12:12");
    tLLCaseCureSchema.setModifyDate("2002-10-10");
    tLLCaseCureSchema.setModifyTime("12:12:12");


    LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
    tLLCaseCureSet.add(tLLCaseCureSchema);

    tVData.addElement(tLLCaseCureSet);

    tCaseCureUI.submitData(tVData,"INSERT||MAIN");
  }
  */
}