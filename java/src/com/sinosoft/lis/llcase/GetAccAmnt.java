package com.sinosoft.lis.llcase;

import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LCInsureAccDB;
import com.sinosoft.lis.db.LMRiskInsuAccDB;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.vschema.LCInsureAccTraceSet;
import com.sinosoft.lis.schema.LCInsureAccTraceSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.db.LCInsureAccTraceDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.db.LLAppealDB;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class GetAccAmnt {
    public TransferData tresult = new TransferData();
    private double[] accinfo= {0,0,0,0,0,0,0};
    private String AccMsg = "";
    private LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet();
    public double[] getAccInfo(){
        return accinfo;
    }

    public String getAccMsg(){
        return AccMsg;
    }

    public LCInsureAccTraceSet getTrace(){
        return mLCInsureAccTraceSet;
    }

    /**
     * 取当前帐户号对应的帐户类型（唉！极烂的数据结构设计导致多余的转换消耗）
     * 001：团体特需医疗帐户，004：团体特需医疗固定帐户，002：个人特需医疗理赔帐户
     * @param tInsuAccNo String
     * @return String
     */
    private String getAccType(String tInsuAccNo) {
        String tType = "";
        LMRiskInsuAccDB triskaccdb = new LMRiskInsuAccDB();
        triskaccdb.setInsuAccNo(tInsuAccNo);
        if (triskaccdb.getInfo()) {
            tType = triskaccdb.getAccType();
        }
        return tType;
    }

    /**
     * 根据险种号和帐户类型查InsuAccNo
     * @param aRiskCode String
     * @param aAccType String
     * @return String
     */
    private String getInsuAccNo(String aRiskCode,String aAccType){
        String tsql = "select b.insuaccno from lmrisktoacc a,LMRiskInsuAcc b "
                      +" where a.insuaccno = b.insuaccno and a.riskcode='"
                      +aRiskCode+"' and b.acctype='"+aAccType+"'";
        ExeSQL texesql = new ExeSQL();
        String aInsuAccNo = texesql.getOneValue(tsql);
        aInsuAccNo = (aInsuAccNo==null||aInsuAccNo.equals("null"))?"":aInsuAccNo;
        return aInsuAccNo;
    }

    /**
     * 查询个人账户的PolNo和InsuAccNo
     * @param aPolNo String 个人保单号
     * @return double
     */
    private String[] getIAccPolInfo(String aPolNo){
        String[] gpolinfo = {aPolNo,""};
        String tsql = "select riskcode from lcpol where polno = '"+aPolNo+"' union select riskcode from lbpol where polno = '"+aPolNo+"'";
        ExeSQL texesql = new ExeSQL();
        String ariskcode = texesql.getOneValue(tsql);
        gpolinfo[1] = getInsuAccNo(ariskcode,"002");
        return gpolinfo;
    }

    /**
     * 查询团体账户的PolNo和InsuAccNo
     * @param aPolNo String 个人保单号
     * @return double
     */
    private String[] getGAccPolInfo(String aPolNo){
        String[] gpolinfo = {"",""};
        String tGRPSQL = "select grppolno from lcpol "
                      +" where polno='"+aPolNo+"' union select grppolno from lbpol where polno='"+aPolNo+"'";
                      
        ExeSQL texesql = new ExeSQL();
        String tGrpPolNo = texesql.getOneValue(tGRPSQL);

        String tsql = "select polno,riskcode from lcpol "
                      + "where grppolno='"+tGrpPolNo+"' and poltypeflag = '2' and acctype='1' ";
        SSRS tss = texesql.execSQL(tsql);
        if(tss.getMaxRow()>0){
            gpolinfo[0] = tss.GetText(1,1);
            gpolinfo[1] = getInsuAccNo(tss.GetText(1,2),"001");
        }
        return gpolinfo;
    }

    private String[] getAccPolInfo(String aPolNo,String aAccType){
        if(aAccType.equals("I"))
            return getIAccPolInfo(aPolNo);
        else
            return getGAccPolInfo(aPolNo);
    }

    //个人账户余额
    private double getAccBala(String[] aPolInfo,String aCaseNo){
        double accbala = 0;
        //2713补充a是否为团体补充A的险种**start**修改
        String rsql = "select riskcode from lcpol where polno='"+aPolInfo[0]+"' ";
        ExeSQL texesql = new ExeSQL();
        String tRiskcode = texesql.getOneValue(rsql);
        String sql ="select 1 from ldcode where code='"+tRiskcode+"' and codetype='lprisk' "; // 3207 新增
        String tcode=texesql.getOneValue(sql);
        if("1".equals(tcode)){
        	accbala=getAccBalaLX(aPolInfo,aCaseNo);//#2713补充团体医疗保险（A款）
        }else{
        	LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
	        tLCInsureAccDB.setPolNo(aPolInfo[0]);
	        tLCInsureAccDB.setInsuAccNo(aPolInfo[1]);
	        if(tLCInsureAccDB.getInfo()){
	            accbala = tLCInsureAccDB.getInsuAccBala();
	        }
	        //2713补充a是否为团体补充A的险种**end**修改
        }
        String tAppealSQL = "SELECT caseno FROM llappeal where appealno='"+aCaseNo
                          +"' UNION "
                          + "SELECT appealno FROM llappeal "
                          + "WHERE caseno = (select caseno from llappeal where appealno='"
                          + aCaseNo +"') AND appealno <> '"+aCaseNo+"'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tAppealSSRS = tExeSQL.execSQL(tAppealSQL);
        for (int i = 1; i<=tAppealSSRS.getMaxRow();i++){
            double tAppealMoney = 0;
            String tOldCaseNo = tAppealSSRS.GetText(i,1);
            LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
            tLCInsureAccTraceDB.setOtherNo(tOldCaseNo);
            tLCInsureAccTraceDB.setPolNo(aPolInfo[0]);
            tLCInsureAccTraceDB.setInsuAccNo(aPolInfo[1]);
            tLCInsureAccTraceDB.setState("0");
            LCInsureAccTraceSet tLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
            for (int m = 1; m<=tLCInsureAccTraceSet.size(); m++){
                tAppealMoney +=tLCInsureAccTraceSet.get(m).getMoney();
            }
            accbala -=tAppealMoney;
        }

        return accbala;
    }
    //2713补充团体医疗A的账户余额+利息 作为个人  保险金额**start**新增
    private double getAccBalaLX(String[] aPolInfo,String aCaseNo){
        double accbala = 0;
        //根据polno查险种号
        String tpolno=aPolInfo[0];
        //LCPolBL同时查C表和B表
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setPolNo(tpolno);
        if (!tLCPolBL.getInfo()) {
            CError.buildErr(this, "险种保单信息查询错误!");
        }
        // # 3337 管B claimnum =3 仅从公共账户
        if("202001".equals(aPolInfo[1])){
        	accbala =LLCaseCommon.getPooledAccount(aCaseNo, tpolno, PubFun.getCurrentDate(), "AC");
        }
        else{	
        	// #3337 管理是补充医疗保险B款 死亡和非死亡账户余额
        	String tDeathSQL="select deathdate from llcase where caseno='"+aCaseNo+"' with ur";
        	ExeSQL tExeSQL = new ExeSQL();
        	String tDeathDate= tExeSQL.getOneValue(tDeathSQL);
        	if(!"".equals(tDeathDate) && tDeathDate!=null){
        		accbala = LLCaseCommon.getAmntFromAcc(aCaseNo, tpolno, tDeathDate, "AC");
        	}else{
        		accbala = LLCaseCommon.getAmntFromAcc(aCaseNo, tpolno, PubFun.getCurrentDate(), "AC");
        	}
        }
        return accbala;
    }
    //2713补充团体医疗A的账户余额+利息 作为个人  保险金额**end**新增

    //个人帐户余额
    private double getAccBala(String[] aPolInfo){
        double accbala = 0;
        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
        tLCInsureAccDB.setPolNo(aPolInfo[0]);
        tLCInsureAccDB.setInsuAccNo(aPolInfo[1]);
        if(tLCInsureAccDB.getInfo()){
            accbala = tLCInsureAccDB.getInsuAccBala();
        }
        return accbala;
    }


    //个人账户余额
    private double getIAccBala(String aPolNo){
        double iaccbala = 0;
        String[] IPolInfo = getIAccPolInfo(aPolNo);
        iaccbala = getAccBala(IPolInfo);
        return iaccbala;
    }

    //团体账户余额
    private double getGAccBala(String aPolNo){
        double gaccbala = 0;
        String[] GPolInfo = getGAccPolInfo(aPolNo);
        gaccbala = getAccBala(GPolInfo);
        return gaccbala;
    }

    //获取账户待付金额,不分团个险。
    private double getTempPay(String[] aPolInfo,String aOtherNo){
        double temppay=0;
        String tsql = "select sum(money) from lcinsureacctrace where polno ='"+aPolInfo[0]
                      +"' and moneytype='PK' and state = 'temp' and insuaccno = '"
                      +aPolInfo[1]+"' and otherno<>'"+aOtherNo+"'";
        ExeSQL texesql = new ExeSQL();
        String tpay = texesql.getOneValue(tsql);
        try{
            temppay = Double.parseDouble(tpay);
        }catch(Exception ex){

        }
        return temppay;
    }

    public double getAccPay(String aPolNo,String aCaseNo,double exp_pay,String AccType){
        double remnant = exp_pay;
        double actupay = remnant;
        String polinfo[] = getAccPolInfo(aPolNo,AccType);
        double accbala = getAccBala(polinfo,aCaseNo);
        double exp_sum = getTempPay(polinfo,aCaseNo);
        double rem = accbala+exp_sum;
        actupay = rem<exp_pay?rem:exp_pay;
        actupay = actupay<0?0:actupay;
        
        //2713补充团体医疗保险（A款） 是补充A险种且赔身故的,赔付金额不受账户余额限制**start**
        String risksql = "select riskcode from lcpol where polno='"+aPolNo+"' union select riskcode from lbpol where polno='"+aPolNo+"'";
    	ExeSQL texesql = new ExeSQL();
        String tRiskcode = texesql.getOneValue(risksql);
        String sql="select 1 from ldcode where code='"+tRiskcode+"' and codetype='lprisk' with ur"; //3207 
        String tCode=texesql.getOneValue(sql);
        if("1".equals(tCode)){//险种为补充A
        	String dsql = "select DeathDate from llcase where caseno='"+aCaseNo+"'";
            String deathdate = texesql.getOneValue(dsql);
            if(deathdate!=null&&!"".equals(deathdate) ){//赔身故   
                actupay=exp_pay;
            }
        }
      //2713补充团体医疗保险（A款）**end**
        remnant -= actupay;
        String tMsg = "账户余额" + Arith.round(accbala,2) + "元。";
        if(accbala>0){
            tMsg += "其他已理算未给付案件预期支付"
                    + Arith.round((-1)*exp_sum,2) + "元，本次案件期望支付" + Arith.round(exp_pay,2)
                    + "元,实际能够支付" + Arith.round(actupay,2) + "元。";
        }
        if(AccType.equals("I")){
            accinfo[1] = accbala; //个人账户余额
            accinfo[2] = exp_sum; //个人账户待付金额（除本案件）
            accinfo[3] = actupay; //个人账户可支出金额
            AccMsg = "<br>个人"+tMsg+"<br>";
        }else{
            accinfo[4] = accbala; //团体账户余额
            accinfo[5] = exp_sum; //团体账户待付金额（除本案件）
            accinfo[6] = actupay; //团体账户可支出金额
            AccMsg += "团体"+tMsg;
        }
        accinfo[0] = remnant;
        if(actupay>0)
            buildTrace(polinfo,aCaseNo,(-1)*actupay);
        
        return actupay;
        
        }
  
    public double getAccPay(String aPolNo,String aCaseNo,double exp_pay){
    	// #3337 管B 新增赔付顺序 仅从公共。增此方法...**start**
    	double remnant = exp_pay;
    	String tClaimSQL="select a.claimnum from lcgrpfee a, lcpol b where a.grpcontno=b.grpcontno and b.polno='"+aPolNo+"' union "
    			+ "select a.claimnum from lbgrpfee a,lbpol b where a.grpcontno=b.grpcontno and b.polno='"+aPolNo+"' with ur";
    	ExeSQL tClaimExe = new ExeSQL();
    	String tClaimNo= tClaimExe.getOneValue(tClaimSQL);
    	String tDeathSQL="select deathdate from llcase where caseno='"+aCaseNo+"'";
    	String tDeathDate =tClaimExe.getOneValue(tDeathSQL);
    	if("3".equals(tClaimNo) && "".equals(tDeathDate)){
    		remnant -=getAccPay(aPolNo,aCaseNo,exp_pay,"G");
    	}else{
	        remnant -= getAccPay(aPolNo,aCaseNo,exp_pay,"I");
	        //#2221 理赔团体账户个人账户理赔规则调整
	        boolean tPassFlag = false;//仅赔个人账户为true
	        //首先查看契约签单处配置，查询语句校验是否仅赔个人
	        String tTBSql = "select 1 from lcpol a where a.polno='"+aPolNo+"' " +
	        		"and exists (select 1 from lcgrpfee b where b.claimnum='1' and b.grpcontno=a.grpcontno union select 1 from lbgrpfee b where b.claimnum='1' and b.grpcontno=a.grpcontno fetch first 1 rows only ) " +
	        		"union select 1 from lbpol a where a.polno='"+aPolNo+"' " +
	        		"and exists (select 1 from lcgrpfee b where b.claimnum='1' and b.grpcontno=a.grpcontno union select 1 from lbgrpfee b where b.claimnum='1' and b.grpcontno=a.grpcontno fetch first 1 rows only ) " ;
	        ExeSQL tTBExeSQL = new ExeSQL();
	        String tTBMark = tTBExeSQL.getOneValue(tTBSql);
	        if("1".equals(tTBMark)){
	        	tPassFlag = true;
	        }else{
	        	//契约处无数据，校验理赔历史配置
	        	String txSql = "select 1 from lcpol where "+
	    		"  polno = '"+aPolNo+"' and grpcontno in " +
	    				"(select code from ldcode where codetype='usegrpacc')" +
	    				" union all " +
	    				" select 1 from lbpol where " +
	    				" polno = '"+aPolNo+"' and grpcontno in " +
	    				" (select code from ldcode where codetype='usegrpacc') with ur ";
	        	ExeSQL txSqlbz = new ExeSQL();
	        	String txBz = txSqlbz.getOneValue(txSql);
	        	System.out.println("lyc_fighting------: "+ txSql);
	        	if("1".equals(txBz)){
	        		tPassFlag = true;
	        	}
	        	//2713补充A不赔付团体账户的校验**start**新增
	        	String buchongsql = "select riskcode from lcpol where polno='"+aPolNo+"' ";
	        	ExeSQL buchongEx = new ExeSQL();
	        	String riskcode = buchongEx.getOneValue(buchongsql);
//	        	String  sql="select 1 from ldcode where code='"+riskcode+"' and codetype='lprisk' with ur"; //3207
//	        	String tCode=buchongEx.getOneValue(sql);
	        	if("162401".equals(riskcode) || "170501".equals(riskcode)){
	        		tPassFlag = true;
	        	}
	        	//2713补充A不赔付团体账户的校验**end**新增
	        }
	        //若均无配置，赔完个人账户赔付团体账户       			
	        if(remnant>0 && !tPassFlag)
	            remnant -= getAccPay(aPolNo,aCaseNo,remnant,"G");
    	}
        return exp_pay-remnant;
    }

    private boolean buildTrace(String[] polInfo,String aCaseNo,double money) {
        LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
        tLCInsureAccTraceDB.setPolNo(polInfo[0]);
        tLCInsureAccTraceDB.setInsuAccNo(polInfo[1]);
        tLCInsureAccTraceDB.setOtherNo(aCaseNo);
        LCInsureAccTraceSet tLCInsureAccTraceSet = tLCInsureAccTraceDB.query();
        String toSerialNo = "";
        if (tLCInsureAccTraceSet.size() > 0) {
            toSerialNo = tLCInsureAccTraceSet.get(1).getSerialNo();
        }else{
            String tsql = "select * from lcinsureacctrace where polno='"
                          +polInfo[0]+"' and insuaccno='"+polInfo[1]
                          +"' and moneytype<>'PK'";
            tLCInsureAccTraceSet = tLCInsureAccTraceDB.executeQuery(tsql);
            if(tLCInsureAccTraceSet.size()<=0) {
                return false;
            }
        }
        String tSerialNo = "";
        if (toSerialNo.equals(""))
            tSerialNo = PubFun1.CreateMaxNo("LLACCTRACENO", "86"+aCaseNo.substring(1,5));
         else
            tSerialNo = toSerialNo;
        LCInsureAccTraceSchema tLCInsureAccTraceSchema = tLCInsureAccTraceSet.get(1);
        tLCInsureAccTraceSchema.setSerialNo(tSerialNo);
        tLCInsureAccTraceSchema.setState("temp");
        tLCInsureAccTraceSchema.setMoney(Arith.round(money,2));
        tLCInsureAccTraceSchema.setMoneyType("PK");
        tLCInsureAccTraceSchema.setOtherNo(aCaseNo);
        tLCInsureAccTraceSchema.setOtherType("5");
        mLCInsureAccTraceSet.add(tLCInsureAccTraceSchema);
        return true;
    }
    
    
    public static void main(String[] args) {
        GetAccAmnt tgaa = new GetAccAmnt();
        tgaa.getAccPay("21042696574","C1100150302000010",200000);
    }
}
