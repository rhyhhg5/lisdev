/*
 * <p>ClassName: OLLNoticeBL </p>
 * <p>Description: OLLNoticeBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-12 16:10:52
 */
package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLLNoticeBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LLNoticeSchema mLLNoticeSchema = new LLNoticeSchema();

//private LLNoticeSet mLLNoticeSet=new LLNoticeSet();
    public OLLNoticeBL()
    {
    }

    public static void main(String[] args)
    {
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLLNoticeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLLNoticeBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            PubSubmit pubSubmit = new PubSubmit();
            if (!pubSubmit.submitData(this.mResult, ""))
            {
                CError.buildErr(this, "提交事务失败!");
                return false;
            }

        }
        mInputData = null;
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {

        //加入结果集
        MMap tmpMap = new MMap();
        String op = "UPDATE";
        String cDate = PubFun.getCurrentDate();
        String cTime = PubFun.getCurrentTime();
        if ("INSERT||MAIN".equals(this.mOperate))
        {
            String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
            String evNo = PubFun1.CreateMaxNo("NOTICENO", tLimit);

            this.mLLNoticeSchema.setNoticeNo(evNo);
           // this.mLLNoticeSchema.setMngCom(this.mGlobalInput.ManageCom);
            this.mLLNoticeSchema.setMakeDate(cDate);
            this.mLLNoticeSchema.setMakeTime(cTime);
            System.out.println(cDate);
            this.mLLNoticeSchema.setNoticeDate(cDate);
            this.mLLNoticeSchema.setNoticeTime(cTime);

            this.mLLNoticeSchema.setOperator(this.mGlobalInput.Operator);

            op = "DELETE&INSERT";
        }

        String delSql = null;
        if ("DELETE||MAIN".equals(this.mOperate))
        {
            op = "DELETE";
                //关联表数据
                delSql = "delete  from LLNoticeRela where NoticeNo ='" + mLLNoticeSchema.getNoticeNo() +"'";
                String delevent = "delete from LLSubReport where SubRptNo in "
                    +"(select SubRptNo from LLNoticeRela where NoticeNo='" +
                    mLLNoticeSchema.getNoticeNo() + "')";
                String delrale = "delete from LLNoticeRela where NoticeNo='"
                    + mLLNoticeSchema.getNoticeNo() +"'";
                tmpMap.put(delSql, "DELETE");
                tmpMap.put(delevent,"DELETE");
                tmpMap.put(delrale,"DELETE");
         }


     //   }
        if ("UPDATE||MAIN".equals(this.mOperate))
        {
            op = "UPDATE";
            LLNoticeDB tLLNoticeDB = new LLNoticeDB();
            tLLNoticeDB.setNoticeNo(this.mLLNoticeSchema.getNoticeNo());
            if (!tLLNoticeDB.getInfo())
            {
                CError.buildErr(this, "查询咨询登记信息有误!");
                return false;
            }
            LLNoticeSchema tLLNoticeSchema = tLLNoticeDB.getSchema();
            this.mLLNoticeSchema.setMakeDate(tLLNoticeSchema.getMakeDate());
            this.mLLNoticeSchema.setMakeTime(tLLNoticeSchema.getMakeTime());
            this.mLLNoticeSchema.setOperator(tLLNoticeSchema.getOperator());
            //this.mLLNoticeSchema.setMngCom(tLLNoticeSchema.getMngCom());

        }

        //更新时间
        this.mLLNoticeSchema.setModifyDate(cDate);
        this.mLLNoticeSchema.setModifyTime(cTime);

        tmpMap.put(this.mLLNoticeSchema, op);
        this.mResult.add(tmpMap);
        this.mResult.add(mLLNoticeSchema);
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData()
    {
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLLNoticeSchema.setSchema((LLNoticeSchema) cInputData.
                                        getObjectByObjectName("LLNoticeSchema",
                0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        LLNoticeDB tLLNoticeDB = new LLNoticeDB();
        tLLNoticeDB.setSchema(this.mLLNoticeSchema);
        //如果有需要处理的错误，则返回
        if (tLLNoticeDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLLNoticeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLNoticeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
//            String op = "INSERT";
//            if ("INSERT||MAIN".equals(this.mOperate))
//            {
//                op = "DELETE&INSERT";
//            }
//            else
//            if ("DELETE||MAIN".equals(this.mOperate))
//            {
//                op = "DELETE";
//            }
//            if ("UPDATE||MAIN".equals(this.mOperate))
//            {
//                op = "UPDATE";
//            }
//
//加入结果集
//            MMap tmpMap = new MMap();
//            tmpMap.put(this.mLLNoticeSchema, op);
//            this.mResult.add(tmpMap);
//            this.mResult.add( this.mLLNoticeSchema );
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLNoticeBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
