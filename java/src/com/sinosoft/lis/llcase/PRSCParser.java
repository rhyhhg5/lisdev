/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: 社保导入文件通用解析类，从一个XML数据流中解析出批次下分案信息</p>
 * <p>Copyright: Copyright (c) 2007 </p>
 * <p>Company: Sinosoft </p>
 * @author Xx
 * @version 6.0
 */

package com.sinosoft.lis.llcase;

import java.lang.reflect.Method;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import org.apache.xpath.XPathAPI;
import org.apache.xpath.objects.XObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.sinosoft.lis.pubfun.*;

class PRSCParser implements SIParser {

    // 下面是一些常量定义
    private static String PATH_INSURED = "ROW";

    //案件
    private String mRgtNo = "";
    private String mGrpContNo = "";
    private String mPath = "";
    private String[] Title = null;
    private String vtsName = "ClaimErrList.vts";
    private ListTable FalseListTable = new ListTable();
    private GlobalInput mG = new GlobalInput();
    public CErrors mErrors = new CErrors();

    PRSCParser() {
    }

    public boolean setGlobalInput(GlobalInput aG) {
        mG = aG;
        FalseListTable.setName("FAULT");
        return true;
    }

    /**
     * 解析案件结点(公用函数)
     * @param node Node
     * @return VData
     */
    public VData parseOneInsuredNode(Node node) {
        VData tReturn = new VData();
        // 得到被保人信息
        NodeList nodeList = null;
        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_INSURED);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
            return null;
        }
        int nLength = nodeList == null ? 0 : nodeList.getLength();
        int FailureCount = 0;
        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeCase = nodeList.item(nIndex);
            if (!genCase(getValue(nodeCase))) {
                System.out.println("导入这个人失败");
                FailureCount++;
                //写导出错误xml的函数
            }
        }

        /**
         * 根据xml生成title 不写死在程序里，另外需要生成动态数组
         */
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument(vtsName, "print");
        xmlexport.addListTable(FalseListTable, Title);
        xmlexport.outputDocumentToFile(mPath, mRgtNo); //输出xml文档到文件
        String FailureNum = FailureCount + "";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("FailureNum", FailureNum);
        tReturn.clear();
        tReturn.addElement(xmlexport);
        tReturn.add(tTransferData);
        return tReturn;
    }

    public boolean setParam(String aRgtNo, String aGrpContNo, String aPath) {
        mRgtNo = aRgtNo;
        mGrpContNo = aGrpContNo;
        mPath = aPath;
        return true;
    }

    public void setFormTitle(String[] cTitle, String cvtsName) {
        Title = cTitle;
        vtsName = cvtsName;
    }

    /**
     * 利用XPathAPI取得某个节点的节点值
     * @param node Node
     * @param strPath String
     * @return String
     */
    private static String parseNode(Node node, String strPath) {
        String strValue = "";
        try {
            XObject xobj = XPathAPI.eval(node, strPath);
            strValue = xobj == null ? "" : xobj.toString();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return strValue;
    }

    private TransferData getValue(Node nodeCase) {
        TransferData td = new TransferData();
        for (int i = 0; i < Title.length; i++) {
            System.out.println(Title[i]);
            String tvalue = "" + parseNode(nodeCase, Title[i]);
            td.setNameAndValue(Title[i], tvalue);
        }
        return td;
    }

    private boolean genCase(TransferData cTD) {
        //确认客户信息
        String aSecurityNo = "" + (String) cTD.getValueByName("SecurityNo");
        String aIDNo = "" + (String) cTD.getValueByName("IDNo");
        String aCustomerName = "" + (String) cTD.getValueByName("CustomerName");
        String aSex = "" + (String) cTD.getValueByName("Sex");
        String aCustomerNo = "";

        String strSQL0 =
                "select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype ";
        String sqlpart1 = "from lcinsured a where ";
        String sqlpart2 = "from lbinsured a where ";
        String wherePart = "a.GrpContNo = '" + mGrpContNo + "' ";
        String msgPart = "";
        String ErrMessage = "";
        if (!"null".equals(aSecurityNo) && !"".equals(aSecurityNo)) {
            wherePart += " and a.othidno='" + aSecurityNo + "'";
            msgPart = "社保号为" + aSecurityNo;
        } else if (!"null".equals(aIDNo) && !"".equals(aIDNo)) {
            wherePart += " and a.idno='" + aIDNo + "'";
            msgPart = "身份证号为" + aIDNo;
        } else {
            ErrMessage = "未提供客户号，社保号或身份证号等信息，无法确认客户身份！";
            getFalseList(cTD, ErrMessage);
            return false;
        }

        String strSQL = strSQL0 + sqlpart1 + wherePart + " union " + strSQL0 +
                        sqlpart2 + wherePart;
        ExeSQL texesql = new ExeSQL();
        SSRS tssrs = new SSRS();
        tssrs = texesql.execSQL(strSQL);
        System.out.println(strSQL);
        if (tssrs == null || tssrs.getMaxRow() <= 0) {
            ErrMessage = "团单" + mGrpContNo + "下没有" + msgPart + "的被保险人";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        aCustomerNo = tssrs.GetText(1, 1);
        aIDNo = tssrs.GetText(1, 5);
        aSecurityNo = tssrs.GetText(1, 6);

        if ("".equals(aCustomerName) || "null".equals(aCustomerName)) {
            ErrMessage = "未提供客户姓名";
            getFalseList(cTD, ErrMessage);
            return false;
        } else if (!aCustomerName.equals(tssrs.GetText(1, 2))) {
            ErrMessage = msgPart + "的客户投保时姓名为" + tssrs.GetText(1, 2)
                         + ",与导入姓名" + aCustomerName + "不符！";
            getFalseList(cTD, ErrMessage);
            return false;
        }

        String tSex = tssrs.GetText(1, 3).equals("null") ? "" :
                      tssrs.GetText(1, 3);
        if ("".equals(aSex) || "null".equals(aSex)) {
            ErrMessage = "未提供客户性别";
            getFalseList(cTD, ErrMessage);
            return false;
        } else if (!tSex.equals("") && !aSex.equals(tSex)) {
            ErrMessage = msgPart + "的客户投保时性别为" + tSex
                         + ",与本次导入性别" + aSex + "不符！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        
        String tIdtype=tssrs.GetText(1, 8).equals("null") ? "" :
            tssrs.GetText(1, 8);;//获得证件类型
            /*   if(tIdtype.equals("0"))//是身份证时，对身份证号进行校验
        {
	        String tId=tssrs.GetText(1, 5).equals("null") ? "" :
	            tssrs.GetText(1, 5);;//获得身份证号
	        if(!PubFun.checkIDNo(tId))//校验数据库里存储的身份证号，by zzh 20100925
	        {	
	        	ErrMessage =msgPart+"的客户身份证号错误，请先做客户资料变更！";
	        	getFalseList(cTD, ErrMessage);
	        	return false;
	        }
        }*/
            

        String tBirthday = tssrs.GetText(1, 4).equals("null") ? "" :
                           tssrs.GetText(1, 4);

        String result = LLCaseCommon.checkPerson(aCustomerNo);
        if (!"".equals(result)) {
            ErrMessage = msgPart + "的客户正在进行保全操作,工单号为" + result + "！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        String strCustomerSQL = "select * from lpedorapp a,lpgrpedoritem b where a.edoracceptno in (select max(edorno) from lpdiskimport where  insuredno='" +
                                aCustomerNo + "' and grpcontno ='" + mGrpContNo + "') and a.edorstate !='0' and a.edoracceptno = b.edoracceptno and b.edortype ='ZT'";
        SSRS customertssrs = new SSRS();
        customertssrs = texesql.execSQL(strCustomerSQL);
        if (customertssrs.getMaxRow() > 0) {
            ErrMessage = msgPart + "的客户正在进行保全减人操作！";
            getFalseList(cTD, ErrMessage);
            return false;
        }

        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if (!tLLRegisterDB.getInfo()) {
            ErrMessage = "批次查询失败";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        tLLRegisterSchema = tLLRegisterDB.getSchema();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        VData tVData = new VData();

        PRCaseInfoBL tgenCaseInfoBL = new PRCaseInfoBL();

        tLLCaseSchema.setRgtType("1");

        String aReasonCode = "" + (String) cTD.getValueByName("ReasonCode");
        LLAppClaimReasonSchema tLLAppClaimReasonSchema = new
                LLAppClaimReasonSchema();
        if (!"".equals(aReasonCode) && !"null".equals(aReasonCode)) {
            tLLAppClaimReasonSchema.setRgtNo(mRgtNo);
            tLLAppClaimReasonSchema.setCaseNo("");
            tLLAppClaimReasonSchema.setReasonCode(aReasonCode);
            tLLAppClaimReasonSchema.setCustomerNo(aCustomerNo);
            tLLAppClaimReasonSchema.setReasonType("0");
        }

        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setRgtNo(mRgtNo);
        tLLCaseDB.setCustomerNo(aCustomerNo);
        LLCaseSet temLLCaseSet = tLLCaseDB.query();
        if (temLLCaseSet.size() > 0) {
            tLLCaseSchema.setUWState("7");
        }
        
//      modify by zhangyige 2012-07-09
        tLLCaseSchema.setPrePaidFlag(tLLRegisterSchema.getPrePaidFlag());
        
        tLLCaseSchema.setCaseNo("");
        tLLCaseSchema.setRgtNo(mRgtNo);
        tLLCaseSchema.setCustomerName(aCustomerName);
        tLLCaseSchema.setCustomerNo(aCustomerNo);
        tLLCaseSchema.setIDType("0");
        tLLCaseSchema.setIDNo(aIDNo);
        tLLCaseSchema.setOtherIDType("5");
        tLLCaseSchema.setOtherIDNo(aSecurityNo);
        tLLCaseSchema.setCustBirthday(tBirthday);
        tLLCaseSchema.setCustomerSex(aSex);

        String aAccDate = "" + FormatDate((String) cTD.getValueByName("AccDate"));
        if (aAccDate == null || "".equals(aAccDate) || "null".equals(aAccDate)) {
            ErrMessage = msgPart + "的客户出险日期不能为空，或出险日期格式不正确应为'YYYY-MM-DD'！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        String aDeathDate = "" +
                            FormatDate((String) cTD.getValueByName("DeathDate"));
        tLLCaseSchema.setAccidentDate(aAccDate);
        if(tLLCaseSchema.getAccidentDate()==null)//by zzh 0930
        {
        	ErrMessage = msgPart + "的客户出险日期格式不正确,应为'YYYY-MM-DD'！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        tLLCaseSchema.setDeathDate(aDeathDate);
        System.out.println(tLLCaseSchema.getAccidentDate());
        tLLCaseSchema.setRgtState("13"); //案件状态
        tLLCaseSchema.setSurveyFlag("0");
        tLLCaseSchema.setAccdentDesc("批量受理");
        tLLCaseSchema.setSurveyFlag("0");
        tLLCaseSchema.setReceiptFlag("0");
        tLLCaseSchema.setCaseGetMode(tLLRegisterSchema.getCaseGetMode());

        String sql =
                "select bankcode,bankaccno,accname from lcinsured where insuredno='" +
                aCustomerNo + "' and grpcontno='" + mGrpContNo + "'" +
                " union " +
                "select bankcode,bankaccno,accname from lbinsured where insuredno='" +
                aCustomerNo + "' and grpcontno='" + mGrpContNo + "' with ur";

        ExeSQL texesql1 = new ExeSQL();
        SSRS trr = texesql1.execSQL(sql);
        if (trr.getMaxRow() > 0) {
            try {
                tLLCaseSchema.setBankCode(trr.GetText(1, 1));
                tLLCaseSchema.setBankAccNo(trr.GetText(1, 2));
                tLLCaseSchema.setAccName(trr.GetText(1, 3));
            } catch (Exception ex) {
                System.out.println("插入用户帐户信息失败！");
            }
        }


        LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
        tLLSubReportSchema.setCustomerNo(aCustomerNo);
        tLLSubReportSchema.setAccDate(aAccDate);
        tLLSubReportSchema.setCustomerName(aCustomerName);
        String aAccidentType = "" + (String) cTD.getValueByName("AccType");
        tLLSubReportSchema.setAccidentType(aAccidentType);
        String aAccDesc = "" + (String) cTD.getValueByName("DiseaseName");
        tLLSubReportSchema.setAccDesc(aAccDesc);
        String aAccPlace = "" + (String) cTD.getValueByName("HospitalName");
        tLLSubReportSchema.setAccPlace(aAccPlace);
        String aHospStartDate = "" + FormatDate((String) cTD.
                                                getValueByName("HospStartDate"));
        tLLSubReportSchema.setInHospitalDate(aHospStartDate);
        String aHospEndDate = "" + FormatDate((String) cTD.
                                              getValueByName("HospEndDate"));
        tLLSubReportSchema.setOutHospitalDate(aHospEndDate);

        System.out.println("<--Star Submit VData-->");
        tVData.add(tLLAppClaimReasonSchema);
        tVData.add(tLLCaseSchema);
        tVData.add(tLLRegisterSchema);
        tVData.add(tLLSubReportSchema);
        tVData.add(mG);
        System.out.println("<--Into tSimpleClaimUI-->" + mG.ManageCom);
        if (!tgenCaseInfoBL.submitData(tVData, "INSERT||MAIN")) {
            CErrors tError = tgenCaseInfoBL.mErrors;
            ErrMessage = tError.getFirstError();
            getFalseList(cTD, ErrMessage);
            return false;
        }
        return true;
    }

    private Object fillCommonField(Object cSchema, TransferData cTD) {
        int n = cTD.getValueNames().size();
        for (int i = 0; i < n; i++) {
            String fieldName = (String) cTD.getNameByIndex(i);
            String fieldValue = (String) cTD.getValueByIndex(i);
            Class[] c = new Class[1];
            Method m = null;
            fieldName = "set" + fieldName;
            String[] tValue = {fieldValue};
            try {
                c[0] = Class.forName("java.lang.String");
            } catch (Exception ex) {
            }
            try {
                m = cSchema.getClass().getMethod(fieldName, c);
            } catch (Exception ex) {
            }
            try {
                m.invoke(cSchema, tValue);
            } catch (Exception ex) {
            }
        }
        return cSchema;
    }

    private boolean getFalseList(TransferData cTD, String ErrMessage) {
        int n = Title.length;
        String[] strCol = new String[n + 1];
        for (int i = 0; i < n; i++) {
            strCol[i] = (String) cTD.getValueByIndex(i);
        }
        strCol[n] = ErrMessage;
        System.out.println("ErrMessage:" + ErrMessage);
        FalseListTable.add(strCol);
        return true;
    }

    private String FormatDate(String aDate) {
        String rDate = "";
        rDate += aDate;
        if (rDate.equals("") || rDate.equals("null")) {
            return "";
        }
        if (aDate.length() < 8) {
            int days = 0;
            try {
                days = Integer.parseInt(aDate) - 2;
            } catch (Exception ex) {
                return "";
            }
            rDate = PubFun.calDate("1900-1-1", days, "D", null);
        }
        return rDate;
    }
}
