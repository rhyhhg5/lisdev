package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 给付确认的返回类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @version 1.0
 */
public class PayAffirmReturnBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  /**  */
  private LLClaimSchema mLLClaimSchema = new LLClaimSchema();
  private LLClaimSet mLLClaimSet = new LLClaimSet();
  private LJSGetSet mLJSGetSet = new LJSGetSet();
  private LLClaimPolicySet mLLClaimPolicySet = new LLClaimPolicySet();

  public PayAffirmReturnBL() {}

  /**
   * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;

    //数据查询业务处理
    if (cOperate.equals("QUERY||MAIN"))
    {
      if(!queryData())
        return false;
      System.out.println("---queryData---");
    }
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  private boolean getInputData(VData cInputData)
  {
    // 暂交费查询条件
    if (mOperate.equals("QUERY||MAIN"))
    {
      mLLClaimSchema.setSchema((LLClaimSchema)cInputData.getObjectByObjectName("LLClaimSchema",0));
    }

    return true;
  }

  private boolean queryData()
  {
    mResult.clear();
    LLRegisterDB tLLRegisterDB = new LLRegisterDB();
    tLLRegisterDB.setRgtNo(mLLClaimSchema.getRgtNo());
    tLLRegisterDB.getInfo();
    if (tLLRegisterDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "PayAffirmReturnBL";
      tError.functionName = "queryData";
      tError.errorMessage = "立案查询失败!";
      this.mErrors.addOneError(tError);
      mLLClaimSet.clear();
      return false;
    }
    LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
    tLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
    mResult.add(tLLRegisterSchema);


    LLClaimDB tLLClaimDB = new LLClaimDB();
    tLLClaimDB.setRgtNo(mLLClaimSchema.getRgtNo());
    mLLClaimSet=tLLClaimDB.query();
    if (tLLClaimDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLClaimDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "PayAffirmReturnBL";
      tError.functionName = "queryData";
      tError.errorMessage = "赔案查询失败!";
      this.mErrors.addOneError(tError);
      mLLClaimSet.clear();
      return false;
    }
    if (mLLClaimSet.size() == 0)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "QueryPayPolBL";
      tError.functionName = "queryData";
      tError.errorMessage = "未找到相关数据!";
      this.mErrors.addOneError(tError);
      mLLClaimSet.clear();
      return false;
    }

    mResult.add(mLLClaimSet);

    LJSGetDB tLJSGetDB = new LJSGetDB();
    tLJSGetDB.setOtherNo(mLLClaimSchema.getClmNo());
    mLJSGetSet=tLJSGetDB.query();

    mResult.add(mLJSGetSet);


    LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
    tLLClaimPolicyDB.setRgtNo( mLLClaimSchema.getRgtNo());
    mLLClaimPolicySet.set(tLLClaimPolicyDB.query());
    if (tLLClaimPolicyDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLClaimPolicyDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "QueryPayPolBL";
      tError.functionName = "queryData";
      tError.errorMessage = "保单赔案信息查询失败!";
      this.mErrors.addOneError(tError);
      mLLClaimPolicySet.clear();
      return false;
    }

    mResult.add( mLLClaimPolicySet );
    return true;
  }
}
