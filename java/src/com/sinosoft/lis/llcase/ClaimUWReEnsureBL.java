package com.sinosoft.lis.llcase;

import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 理赔复核签批确认业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * HE
 */
public class ClaimUWReEnsureBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */

  private String mOperate;
  private GlobalInput mGlobalInput = new GlobalInput();

  /** 业务处理相关变量 */
  /** 赔案名细 */
  //案件赔案信息
  private LLClaimSchema mLLClaimSchema = new LLClaimSchema();
  //案件立案信息表
  private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
  //保单赔案
  private LLClaimPolicySet mLLClaimPolicySet = new LLClaimPolicySet();
  //保单赔案明细
  private LLClaimDetailSet mLLClaimDetailSet = new LLClaimDetailSet();
  //应付总表
  private LJSGetSchema mLJSGetSchema = new LJSGetSchema();
  //赔案应付表
  private LJSGetClaimSet mLJSGetClaimSet = new LJSGetClaimSet();
  //核赔主表
  private LLClaimUnderwriteSet mLLClaimUnderwriteSet = new LLClaimUnderwriteSet();
  //案件核赔主表
  private LLClaimUWMainSchema mLLClaimUWMainSchema = new LLClaimUWMainSchema();
  //上次案件核赔表
  private LLClaimUWMainSchema mLLClaimLastUWMainSchema = new LLClaimUWMainSchema();
  //案件核赔履历表
  private LLClaimUWMDetailSchema mLLClaimUWMDetailSchema = new LLClaimUWMDetailSchema();
  /**用户表信息 */
  private LLClaimUserSchema mLLClaimUserSchema = new LLClaimUserSchema();
  //是否有拒赔的保单标志
  private String mDeclineFlag = "N";
  public ClaimUWReEnsureBL() {}

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData,String cOperate)
  {

    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone();
    System.out.println("---1---");
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;

    //根据赔案信息查询相关信息
    if (!getBaseData())
      return false;

    // 校验数据
    if (!CanICheck())
      return false;

    //对有拒赔保单并且可以签批的案件对赔付数据进行处理
    if (mLLClaimUWMainSchema.getAppActionType().equals("1"))
    {
      if (!UpdPay())
        return false;
    }
    //检查历次赔付的赔付金额是否超过保单的保额
    if (!checkApprove())
    {
      return false;
    }

    //准备给后台的数据
    if (!prepareOutputData())
      return false;

    //数据提交
    ClaimUWReEnsureBLS tClaimUWReEnsureBLS = new ClaimUWReEnsureBLS();
    System.out.println("init end");
    if (!tClaimUWReEnsureBLS.submitData(mInputData,mOperate))
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tClaimUWReEnsureBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimUWReEnsureBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    mGlobalInput.setSchema(
            (GlobalInput)
            cInputData.getObjectByObjectName("GlobalInput",0));
    mOperate = "INSERT";
    mLLClaimSchema =
            (LLClaimSchema)
            cInputData.getObjectByObjectName("LLClaimSchema",0);
    mLLClaimUWMainSchema =
            (LLClaimUWMainSchema)
            cInputData.getObjectByObjectName("LLClaimUWMainSchema",0);

    return true;
  }

  private boolean getBaseData()
  {

    //查询案件赔案表
    LLClaimDB tLLClaimDB = new LLClaimDB();
    tLLClaimDB.setClmNo(mLLClaimSchema.getClmNo());
    if (!tLLClaimDB.getInfo())
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLClaimDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimUWEnsureBL";
      tError.functionName = "getBaseData";
      tError.errorMessage = "赔案表LLClaim查询失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mLLClaimSchema.setSchema(tLLClaimDB.getSchema());
    if (!mLLClaimSchema.getClmState().equals("1"))
    {
      this.mErrors.copyAllErrors(tLLClaimDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimUWEnsureBL";
      tError.functionName = "getBaseData";
      tError.errorMessage = "案件不允许作复核签批!";
      this.mErrors.addOneError(tError);
      return false;
    }

    //查询案件立案信息表
    LLRegisterDB  tLLRegisterDB = new LLRegisterDB();
    tLLRegisterDB.setRgtNo(mLLClaimSchema.getRgtNo());
    if (!tLLRegisterDB.getInfo())
    {
      this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimUWEnsureBL";
      tError.functionName = "getBaseData";
      tError.errorMessage = "立案表LLRegister查询失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());


    //查询上次案件核赔表
    LLClaimUWMainDB tLLClaimUWMainDB = new LLClaimUWMainDB();
    tLLClaimUWMainDB.setClmNo(mLLClaimSchema.getClmNo());
    tLLClaimUWMainDB.getInfo();
    if (tLLClaimUWMainDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLClaimUWMainDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimReUWEnsureBL";
      tError.functionName = "getBaseData";
      tError.errorMessage = "案件核赔表LLClaimUWMain查询失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mLLClaimLastUWMainSchema.setSchema(tLLClaimUWMainDB.getSchema());

    //查询保单赔付表
    LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
    tLLClaimPolicyDB.setClmNo(mLLClaimSchema.getClmNo());
    mLLClaimPolicySet.set(tLLClaimPolicyDB.query());
    if (tLLClaimPolicyDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLClaimPolicyDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimUWEnsureBL";
      tError.functionName = "getBaseData";
      tError.errorMessage = "保单赔案表LLClaimPolicy查询失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    //查询保单核赔表
    LLClaimUnderwriteDB tLLClaimUnderwriteDB = new LLClaimUnderwriteDB();
    tLLClaimUnderwriteDB.setClmNo(mLLClaimSchema.getClmNo());
    mLLClaimUnderwriteSet.set(tLLClaimUnderwriteDB.query());
    if (tLLClaimUnderwriteDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLClaimUnderwriteDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimUWEnsureBL";
      tError.functionName = "getBaseData";
      tError.errorMessage = "核赔表LLClaimUnderwrite查询失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    //查询保单赔付明细表
    LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
    tLLClaimDetailDB.setClmNo(mLLClaimSchema.getClmNo());
    mLLClaimDetailSet.set(tLLClaimDetailDB.query());
    if (tLLClaimDetailDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLClaimDetailDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimUWEnsureBL";
      tError.functionName = "getBaseData";
      tError.errorMessage = "保单赔案明细表LLClaimDetail查询失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    //查询财务应付赔案明细表
    LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB();
    tLJSGetClaimDB.setOtherNo(mLLClaimSchema.getClmNo());
    tLJSGetClaimDB.setOtherNoType("5");
    mLJSGetClaimSet.set(tLJSGetClaimDB.query());
    if (tLJSGetClaimDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJSGetClaimDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimUWEnsureBL";
      tError.functionName = "getBaseData";
      tError.errorMessage = "赔付应付表LJSGetClaim查询失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    //准备应付总表LJSGet
    LJSGetDB tLJSGetDB = new LJSGetDB();
    tLJSGetDB.setGetNoticeNo(mLJSGetClaimSet.get(1).getGetNoticeNo());
    if (!tLJSGetDB.getInfo())
    {
      this.mErrors.copyAllErrors(tLJSGetDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimManChkBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "应付总表LJSGet查询失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mLJSGetSchema.setSchema(tLJSGetDB.getSchema());

    //查询用户信息
    LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
    tLLClaimUserDB.setUserCode(mGlobalInput.Operator);
    tLLClaimUserDB.getInfo();
    if (tLLClaimUserDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLClaimUserDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimManChkBL";
      tError.functionName = "getUser";
      tError.errorMessage = "用户核赔权限信息查询失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mLLClaimUserSchema.setSchema(tLLClaimUserDB.getSchema());
    if (mLLClaimUserSchema.getClaimPopedom()==null)
    {
      CError tError = new CError();
      tError.moduleName = "ClaimManChkBL";
      tError.functionName = "getUser";
      tError.errorMessage = "用户无核赔权限!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  private boolean CanICheck()
  {
    //检查是否已经作过初审
    if (mLLClaimLastUWMainSchema.getAppClmUWer() == null)
    {
      CError tError = new CError();
      tError.moduleName = "ClaimUWReEnsureBL";
      tError.functionName = "CanICheck";
      tError.errorMessage = "请先作案件审核!" ;
      this.mErrors.addOneError(tError) ;
      return false ;
    }

    //检查上次核赔的申请人与当前用户是否一致
    if (mLLClaimLastUWMainSchema.getAppClmUWer() != null &&
        mLLClaimLastUWMainSchema.getAppClmUWer().length() != 0 )
    {
      if (!mLLClaimLastUWMainSchema.getAppClmUWer().
          equals(this.mGlobalInput.Operator))
      {
        CError tError = new CError();
        tError.moduleName = "ClaimUWReEnsureBL";
        tError.functionName = "CanICheck";
        tError.errorMessage = "当前案件应由" +
                              mLLClaimLastUWMainSchema.getAppClmUWer() +
                              "签批" ;
        this.mErrors.addOneError(tError) ;
        return false ;
      }
    }

    //检查该案件是否已经提起调查报告
    if (mLLRegisterSchema.getClmState().equals("5"))
    {
      CError tError = new CError();
      tError.moduleName = "ClaimUWReEnsureBL";
      tError.functionName = "CanICheck";
      tError.errorMessage = "案件已经提起调查，不能作核赔确认!";
      this.mErrors.addOneError(tError);
      return false;
    }

    //检查案件状态
    if (mLLClaimSchema.getClmState().equals("2") )
      //核赔成功
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ClaimUWEnsureBL";
      tError.functionName = "CanICheck";
      tError.errorMessage = "已经核赔成功!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    if (mLLClaimSchema.getClmState().equals("3") )
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ClaimUWReEnsureBL";
      tError.functionName = "CanICheck";
      tError.errorMessage = "已经完成给付!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    if (mLLClaimSchema.getClmState().equals("4") )
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ClaimUWReEnsureBL";
      tError.functionName = "CanICheck";
      tError.errorMessage = "已经拒赔!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    //检查是否所有的保单都已经核赔确认
    String tChkEnsureFlag = "Y";
    for (int i=1;i<=mLLClaimPolicySet.size();i++)
    {
      LLClaimPolicySchema tLLClaimPolicySchema = new LLClaimPolicySchema();
      tLLClaimPolicySchema.setSchema(mLLClaimPolicySet.get(i));
      String tFlag = "N";
      for (int j=1;j<=mLLClaimUnderwriteSet.size();j++)
      {
        LLClaimUnderwriteSchema tLLClaimUnderwriteSchema= new LLClaimUnderwriteSchema();
        tLLClaimUnderwriteSchema.setSchema(mLLClaimUnderwriteSet.get(j));
        if (tLLClaimUnderwriteSchema.getPolNo().equals(tLLClaimPolicySchema.getPolNo()))
        {
          tFlag = "Y";
          break ;
        }
      }
      if (tFlag.equals("N"))
      {
        tChkEnsureFlag="N";
        break;
      }
    }
    if (tChkEnsureFlag == "N")
    {
      CError tError = new CError();
      tError.moduleName = "ClaimUWReEnsureBL";
      tError.functionName = "CanICheck";
      tError.errorMessage = "保单并未全部核赔确认!" ;
      this.mErrors.addOneError(tError) ;
      return false ;
    }

    //检查是否录入案件处理意见
    System.out.println("AppActionType=="+mLLClaimUWMainSchema.getAppActionType());
    if (mLLClaimUWMainSchema.getAppActionType() == null ||
        mLLClaimUWMainSchema.getAppActionType().equals(""))
    {
      CError tError = new CError();
      tError.moduleName = "ClaimUWReEnsureBL";
      tError.functionName = "CanICheck";
      tError.errorMessage = "请录入处理意见" ;
      this.mErrors.addOneError(tError) ;
      return false ;
    }

    //如果上次案件核赔的申请动作为退回，应该先作审核
    if (mLLClaimLastUWMainSchema.getAppActionType().equals("2"))
    {
      CError tError = new CError();
      tError.moduleName = "ClaimUWReEnsureBL";
      tError.functionName = "CanICheck";
      tError.errorMessage = "请先作案件审核!" ;
      this.mErrors.addOneError(tError) ;
      return false ;
    }

    if (mLLClaimUWMainSchema.getAppActionType().equals("1")) //确认
    {
        System.out.println("作确认处理");
        if (this.mLLClaimLastUWMainSchema.getAppGrade() != null &&
            mLLClaimLastUWMainSchema.getAppGrade().length() != 0)
        {
            System.out.println("上次申请级别不为空:" +
                               mLLClaimLastUWMainSchema.getAppActionType().
                               toUpperCase());
            System.out.println("本次级别为：" +
                               mLLClaimUserSchema.getClaimPopedom().
                               toUpperCase());
            if (mLLClaimLastUWMainSchema.getAppGrade().toUpperCase().
                compareTo(
                    mLLClaimUserSchema.getClaimPopedom().toUpperCase()) > 0)
            {
                //如果上次确定的申请级别高于本次审核人员的申请级别
                CError tError = new CError();
                tError.moduleName = "ClaimUWReEnsureBL";
                tError.functionName = "CanICheck";
                tError.errorMessage = "您不具有签批确认的权力，请选择退回重审或上报审核";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
    }

    return true;
  }


  //对于核赔拒赔的保单，更新LLClaim、LLClaimPolicy、
  //LLClaimDetail、LJSGet、tLJSGetClaim表的赔付记录
  private boolean UpdPay()
  {
    //检查保单核赔表确定拒赔的保单
    for (int i = 1; i <= mLLClaimUnderwriteSet.size(); i++)
    {
        String tDeclineFlag = "N"; //拒赔标志
        LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new
                LLClaimUnderwriteSchema();
        tLLClaimUnderwriteSchema.setSchema(mLLClaimUnderwriteSet.get(i));
        if (tLLClaimUnderwriteSchema.getClmDecision().equals("0"))
        {
            //修改拒赔保单在LLClaimPolicy表中实付金额为0
            for (int j = 1; j <= mLLClaimPolicySet.size(); j++)
            {
                LLClaimPolicySchema tLLClaimPolicySchema = new
                        LLClaimPolicySchema();
                tLLClaimPolicySchema.setSchema(mLLClaimPolicySet.get(j));
                if (tLLClaimPolicySchema.getPolNo().trim().
                    equals(tLLClaimUnderwriteSchema.getPolNo().trim()))
                {
                    tLLClaimPolicySchema.setRealPay(0);
                }
                mLLClaimPolicySet.set(j, tLLClaimPolicySchema);
            }

            //修改拒赔保单在LLClaimDetail表中实付金额为0
            for (int j = 1; j <= mLLClaimDetailSet.size(); j++)
            {
                LLClaimDetailSchema tLLClaimDetailSchema = new
                        LLClaimDetailSchema();
                tLLClaimDetailSchema.setSchema(mLLClaimDetailSet.get(j));
                if (tLLClaimDetailSchema.getPolNo().trim().
                    equals(tLLClaimUnderwriteSchema.getPolNo().trim()))
                {
                    tLLClaimDetailSchema.setRealPay(0);
                }
                mLLClaimDetailSet.set(j, tLLClaimDetailSchema);
            }

            //修改拒赔保单在LJSGetClaim表中实付金额为0
            for (int j = 1; j <= mLJSGetClaimSet.size(); j++)
            {
                LJSGetClaimSchema tLJSGetClaimSchema = new
                        LJSGetClaimSchema();
                tLJSGetClaimSchema.setSchema(mLJSGetClaimSet.get(j));
                if (tLJSGetClaimSchema.getPolNo().trim().
                    equals(tLLClaimUnderwriteSchema.getPolNo().trim()))
                {
                    tLJSGetClaimSchema.setPay(0);
                }
                mLJSGetClaimSet.set(j, tLJSGetClaimSchema);
            }

        } // end if
    }

    double tSumPay = 0;
    for (int i = 1; i <= mLLClaimPolicySet.size(); i++)
    {
        LLClaimPolicySchema tLLClaimPolicySchema = new LLClaimPolicySchema();
        tLLClaimPolicySchema.setSchema(mLLClaimPolicySet.get(i));
        tLLClaimPolicySchema.setModifyDate(PubFun.getCurrentDate());
        tLLClaimPolicySchema.setModifyTime(PubFun.getCurrentTime());
        tLLClaimPolicySchema.setOperator(mGlobalInput.Operator);
        tLLClaimPolicySchema.setMngCom(mGlobalInput.ManageCom);
        //统计实付金额
        tSumPay = tSumPay + tLLClaimPolicySchema.getRealPay();
        mLLClaimPolicySet.set(i, tLLClaimPolicySchema);
    }
    for (int i = 1;i <= mLLClaimDetailSet.size(); i++)
    {
        LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
        tLLClaimDetailSchema.setSchema(mLLClaimDetailSet.get(i));
        tLLClaimDetailSchema.setModifyDate(PubFun.getCurrentDate());
        tLLClaimDetailSchema.setModifyTime(PubFun.getCurrentTime());
        tLLClaimDetailSchema.setOperator(mGlobalInput.Operator);
        tLLClaimDetailSchema.setMngCom(mGlobalInput.ManageCom);
        mLLClaimDetailSet.set(i, tLLClaimDetailSchema);
    }
    for (int i = 1; i <= mLJSGetClaimSet.size(); i++)
    {
        LJSGetClaimSchema tLJSGetClaimSchema = new LJSGetClaimSchema();
        tLJSGetClaimSchema.setSchema(mLJSGetClaimSet.get(i));
        tLJSGetClaimSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetClaimSchema.setModifyTime(PubFun.getCurrentTime());
        tLJSGetClaimSchema.setOperator(mGlobalInput.Operator);
        mLJSGetClaimSet.set(i, tLJSGetClaimSchema);
    }

    //重置LLClaim表的实付金额
    mLLClaimSchema.setRealPay(tSumPay);
    mLLClaimSchema.setOperator(mGlobalInput.Operator);
    //mLLClaimSchema.setMngCom(mGlobalInput.ManageCom);
    mLLClaimSchema.setModifyDate(PubFun.getCurrentDate());
    mLLClaimSchema.setModifyTime(PubFun.getCurrentTime());
    mLJSGetSchema.setSumGetMoney(tSumPay);
    mLJSGetSchema.setOperator(mGlobalInput.Operator);
    mLJSGetSchema.setModifyDate(PubFun.getCurrentDate());
    mLJSGetSchema.setModifyTime(PubFun.getCurrentTime());

    return true;
  }

  /**
   * 校验保单是否已经完成核赔
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkApprove( )
  {
      //检查历次赔付是否已经超过保单保额
      for (int i = 1; i <= mLLClaimPolicySet.size(); i++)
      {
          LLClaimPolicySchema tLLClaimPolicySchema = new
                                                     LLClaimPolicySchema();
          LCPolSchema tLCPolSchema = new LCPolSchema();
          tLLClaimPolicySchema.setSchema(mLLClaimPolicySet.get(i));
          LCPolBL tLCPolBL = new LCPolBL();
          tLCPolBL.setPolNo(tLLClaimPolicySchema.getPolNo());
          tLCPolBL.getInfo();
          if (tLCPolBL.mErrors.needDealError())
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tLCPolBL.mErrors);
              CError tError = new CError();
              tError.moduleName = "ClaimUWEnsureBL";
              tError.functionName = "checkApprove";
              tError.errorMessage = "LCPol表查询失败!";
              this.mErrors.addOneError(tError);
              return false;
          }
          tLCPolSchema.setSchema(tLCPolBL.getSchema());

          LCGetSet tLCGetSet = new LCGetSet();
          SynLCLBGetBL tSynLCLBGetBL = new SynLCLBGetBL();
          tSynLCLBGetBL.setPolNo(tLLClaimPolicySchema.getPolNo());
          tLCGetSet.set(tSynLCLBGetBL.query());
          if (tSynLCLBGetBL.mErrors.needDealError())
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tSynLCLBGetBL.mErrors);
              CError tError = new CError();
              tError.moduleName = "ClaimUWEnsureBL";
              tError.functionName = "prepareOutputData";
              tError.errorMessage = "LCGet表查询失败!";
              this.mErrors.addOneError(tError);
              return false;
          }
          double tSumPay = 0;
          for (int j = 1; j <= tLCGetSet.size(); j++)
          {
              LCGetSchema tLCGetSchema = new LCGetSchema();
              tLCGetSchema.setSchema(tLCGetSet.get(j));
              if (tLCGetSchema.getLiveGetType().equals("1"))
              {
                  tSumPay = tSumPay + tLCGetSchema.getSumMoney();
              }
          }
          tSumPay = tSumPay + tLLClaimPolicySchema.getRealPay();
          if (tSumPay > tLCPolSchema.getRiskAmnt())
          {
              CError tError = new CError();
              tError.moduleName = "ClaimUWEnsureBL";
              tError.functionName = "prepareOutputData";
              tError.errorMessage = "历次赔付超过保单保额，保单：" +
                                    tLLClaimPolicySchema.getPolNo();
              this.mErrors.addOneError(tError);
              return false;
          }
      }
      return true;
  }

  /**
   * 核保险种信息校验,准备核保算法
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData()
  {
      if (mLLClaimUWMainSchema.getAppActionType().equals("1"))
      {
          //案件签批通过，置赔案状态为[已核赔]
          mLLClaimSchema.setClmState("2");
          //[结案日期]
          mLLClaimSchema.setEndCaseDate(PubFun.getCurrentDate());
          for (int i = 1; i <= mLLClaimPolicySet.size(); i++)
          {
              LLClaimPolicySchema tLLClaimPolicySchema = new
                      LLClaimPolicySchema();
              tLLClaimPolicySchema.setSchema(mLLClaimPolicySet.get(i));
              tLLClaimPolicySchema.setClmState("2");
              tLLClaimPolicySchema.setEndCaseDate(PubFun.getCurrentDate());
              mLLClaimPolicySet.set(i, tLLClaimPolicySchema);
          }

      }
      //准备案件核赔表
      mLLClaimUWMainSchema.setRgtNo(mLLRegisterSchema.getRgtNo());
      mLLClaimUWMainSchema.setAppPhase("1"); //复核签批
      mLLClaimUWMainSchema.setClmUWer(mGlobalInput.Operator);
      mLLClaimUWMainSchema.setClmUWGrade(mLLClaimUserSchema.getClaimPopedom());

      mLLClaimUWMainSchema.setMngCom(mGlobalInput.ManageCom);
      mLLClaimUWMainSchema.setMakeDate(PubFun.getCurrentDate());
      mLLClaimUWMainSchema.setMakeTime(PubFun.getCurrentTime());
      mLLClaimUWMainSchema.setModifyDate(PubFun.getCurrentDate());
      mLLClaimUWMainSchema.setModifyTime(PubFun.getCurrentTime());
      //申请级别初审时确定的核赔级别
      mLLClaimUWMainSchema.setAppGrade(mLLClaimLastUWMainSchema.getAppGrade());

      if (!mLLClaimUWMainSchema.getAppActionType().equals("2"))
      //上报或确定申请审核人员根据权限树与申请级别确定
      {
          String tAppClmUWer = CaseFunPub.
                               getAppClmUWer(mLLClaimUWMainSchema.getAppGrade(),
                                             mGlobalInput.Operator);
          mLLClaimUWMainSchema.setAppClmUWer(tAppClmUWer);
      }
      else //退回的申请人为初审人员
      {
          mLLClaimUWMainSchema.setAppClmUWer(mLLRegisterSchema.getHandler());
      }

      mLLClaimUWMainSchema.setCheckType(mLLClaimLastUWMainSchema.getCheckType());
      //审核类型由上次审核确定
      //备注由前台传入；

      //准备案件核赔履历表
      LLClaimUWMDetailDB tLLClaimUWMDetailDB = new LLClaimUWMDetailDB();
      tLLClaimUWMDetailDB.setClmNo(mLLClaimUWMainSchema.getClmNo());
      int tCount = tLLClaimUWMDetailDB.getCount();
      tCount++;
      LLClaimUWMDetailSchema tLLClaimUWMDetailSchema = new
              LLClaimUWMDetailSchema();
      tLLClaimUWMDetailSchema.setClmNo(mLLClaimUWMainSchema.getClmNo());
      tLLClaimUWMDetailSchema.setClmUWNo(String.valueOf(tCount));
      tLLClaimUWMDetailSchema.setClmUWer(mLLClaimUWMainSchema.getClmUWer());
      tLLClaimUWMDetailSchema.setMngCom(mLLClaimUWMainSchema.getMngCom());
      tLLClaimUWMDetailSchema.setMakeDate(PubFun.getCurrentDate());
      tLLClaimUWMDetailSchema.setMakeTime(PubFun.getCurrentTime());
      tLLClaimUWMDetailSchema.setModifyDate(PubFun.getCurrentDate());
      tLLClaimUWMDetailSchema.setModifyTime(PubFun.getCurrentTime());
      tLLClaimUWMDetailSchema.setClmUWGrade(mLLClaimUWMainSchema.
                                            getClmUWGrade());
      tLLClaimUWMDetailSchema.setAppGrade(mLLClaimUWMainSchema.getAppGrade());
      tLLClaimUWMDetailSchema.setCheckType(mLLClaimUWMainSchema.getCheckType());
      if (mLLClaimUWMainSchema.getAppClmUWer() != null)
      {
          tLLClaimUWMDetailSchema.setAppClmUWer(mLLClaimUWMainSchema.
                                                getAppClmUWer());
      }
      tLLClaimUWMDetailSchema.setAppActionType(mLLClaimUWMainSchema.
                                               getAppActionType());
      tLLClaimUWMDetailSchema.setRemark(mLLClaimUWMainSchema.getRemark());
      tLLClaimUWMDetailSchema.setAppPhase("1"); //复核签批
      tLLClaimUWMDetailSchema.setRgtNo(mLLRegisterSchema.getRgtNo());

      mInputData.clear();
      mInputData.add(mLLClaimSchema);
      mInputData.add(mLLClaimPolicySet);
      mInputData.add(mLLClaimDetailSet);
      mInputData.add(mLJSGetClaimSet);
      mInputData.add(mLJSGetSchema);
      mInputData.add(mLLClaimUWMainSchema);
      mInputData.add(tLLClaimUWMDetailSchema);
      mResult.clear();
      mResult.add(mLLClaimSchema);
      mResult.add(mLLClaimUWMainSchema);

      return true;
  }

  public VData getResult()
  {
    return mResult;
  }
  //取得上一级的核赔级别
  private String getUperGrade(String tString)
  {
    if (tString.equals("A")) return "B";
    if (tString.equals("B")) return "C";
    if (tString.equals("C")) return "D";
    if (tString.equals("D")) return "E";
    if (tString.equals("E")) return "F";
    if (tString.equals("F")) return "G";
    if (tString.equals("G")) return "H";
    if (tString.equals("H")) return "I";
    return tString;
  }
  private String getEnsGrade()
  {
    String tUser="";
    String tGrade="A";
    if (mLLClaimUWMainSchema.getAppClmUWer()!=null && mLLClaimUWMainSchema.getAppClmUWer().length()!=0)
      //如果用户指定要退回的审核人员
      tUser = mLLClaimUWMainSchema.getAppClmUWer();
    else
      //否则为初审人员的级别
      tUser = this.mLLRegisterSchema.getHandler();

    LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
    tLLClaimUserDB.setUserCode(tUser);
    tLLClaimUserDB.getInfo();
    if (tLLClaimUserDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLClaimUserDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimUWReEnsureBL";
      tError.functionName = "getEnsGrade";
      tError.errorMessage = "用户核赔权限信息查询失败!";
      this.mErrors.addOneError(tError);
      return tGrade;
    }
    return tLLClaimUserDB.getClaimPopedom();
  }

  public static void main(String[] args)
  {
    ClaimUWReEnsureBL aClaimUWReEnsureBL = new ClaimUWReEnsureBL();
    GlobalInput tG = new GlobalInput();
    tG.Operator="110016";
    tG.ManageCom="8611";

    String aRgtNo = "86110020040510001303";
    String aClmNo = "86110020040520001487";
    LLClaimSchema aLLClaimSchema = new LLClaimSchema();
    aLLClaimSchema.setClmNo(aClmNo);

    LLClaimUWMainSchema aLLClaimUWMainSchema = new LLClaimUWMainSchema();
    aLLClaimUWMainSchema.setClmNo(aClmNo);
    aLLClaimUWMainSchema.setRgtNo(aRgtNo);
    aLLClaimUWMainSchema.setAppActionType("1");
    VData aVData = new VData();
    aVData.add(tG);
    aVData.add(aLLClaimSchema);
    aVData.add(aLLClaimUWMainSchema);
    aClaimUWReEnsureBL.submitData(aVData,"INSERT");


  }

}
