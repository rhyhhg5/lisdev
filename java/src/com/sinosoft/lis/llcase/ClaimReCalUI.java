package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:承保暂交费功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class ClaimReCalUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  private String backMsg = "";
  /** 数据操作字符串 */
  private String mOperate;

  public ClaimReCalUI() {}

  public String getBackMsg(){
      return backMsg;
  }
  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    ClaimReCalBL tClaimReCalBL = new ClaimReCalBL();

    System.out.println("---UI BEGIN---");
    if (tClaimReCalBL.submitData(cInputData,mOperate) == false)
    {
  		// @@错误处理
      this.mErrors.copyAllErrors(tClaimReCalBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "tClaimSaveUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else{
        mResult = tClaimReCalBL.getResult();
        backMsg = tClaimReCalBL.getBackMsg();
    }
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  public static void main(String[] args)
  {
    ClaimReCalUI tClaimReCalUI = new ClaimReCalUI();
    VData tVData=new VData();
    LLClaimSchema tLLClaimSchema=new LLClaimSchema();
    LLClaimPolicySet tLLClaimPolicySet=new LLClaimPolicySet();
    tLLClaimSchema.setRgtNo("1000011");
    //tLLClaimSchema.setClmNo("11111111");
    GlobalInput tG = new GlobalInput();
    tG.Operator="ddd";
    tG.ManageCom="001";


    tVData.addElement(tLLClaimSchema);
    tVData.addElement(tLLClaimPolicySet);
    tVData.addElement(tG);
    tClaimReCalUI.submitData(tVData,"INSERT");
  }

}
