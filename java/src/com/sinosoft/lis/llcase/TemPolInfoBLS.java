package com.sinosoft.lis.llcase;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Save temporary pol info </p>
 * <p>Description: Save temporary pol info Business Logic Server</p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author 刘岩松
 * @version 1.0
 * @date :2003-04-10
 */

public class TemPolInfoBLS
{
  private VData mInputData ;
  public  CErrors mErrors = new CErrors();
  public String mOperate;
  private LLCasePolicySet mLLCasePolicySet = new  LLCasePolicySet();

  public TemPolInfoBLS() {}
  public static void main(String[] args)
  {
  }

  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;
    if (cOperate.equals("INSERT"))
    {
      try
      {
        if (!this.saveData())
          return false;
      }
      catch(Exception ex)
      {
      }
      return true;
    }

    mInputData=(VData)cInputData.clone();
    String mOperate;
    mOperate=cOperate;
    System.out.println("Start ReportDelete BLS Submit...");
    System.out.println("End ReportDelete BLS Submit...");
    mInputData=null;
    return true;
  }


  private boolean saveData()
  {
    mLLCasePolicySet.set((LLCasePolicySet)mInputData.getObjectByObjectName("LLCasePolicySet",0));
    System.out.println("要保存的记录共有"+mLLCasePolicySet.size()+"条记录");
    System.out.println("开始执行save操作");
    Connection conn = DBConnPool.getConnection();
    if (conn==null)
    {
      CError tError = new CError();
      tError.moduleName = "CasePolicyBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    System.out.println("开始执行save操作2");
    try
    {
      conn.setAutoCommit(false);
      LLCasePolicyDB tLLCasePolicyDB = new LLCasePolicyDB(conn);
      System.out.println("将要执行该操作！！！");
      LLCasePolicySchema tLLCasePolicySchema = new LLCasePolicySchema();
      int int_count = mLLCasePolicySet.size();
      if(int_count==0)
      {
        System.out.println("没有要操作的数据");
        return false;
      }
      tLLCasePolicySchema.setSchema(mLLCasePolicySet.get(1));
      tLLCasePolicyDB.setCaseNo(tLLCasePolicySchema.getCaseNo());
      tLLCasePolicyDB.setPolNo(tLLCasePolicySchema.getPolNo());
      tLLCasePolicyDB.setPolType("0");
      if (!tLLCasePolicyDB.deleteSQL())
      {
        this.mErrors.copyAllErrors(tLLCasePolicyDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "TemPolInfoBLS";
        tError.functionName = "DeleteData";
        tError.errorMessage = "删除失败!";
        this.mErrors.addOneError(tError);
        conn.rollback() ;
        conn.close();
        return false;
      }

      LLCasePolicyDBSet mLLCasePolicyDBSet=new LLCasePolicyDBSet(conn);
      mLLCasePolicyDBSet.set(mLLCasePolicySet);
      if (mLLCasePolicyDBSet.insert() == false)
      {
        System.out.println("开始执行save操作6");
        this.mErrors.copyAllErrors(mLLCasePolicyDBSet.mErrors);
        CError tError = new CError();
        tError.moduleName = "ReportBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "保存数据失败!";
        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
        return false;
      }
      conn.commit() ;
      conn.close();
      System.out.println("save7");
    }
    catch (Exception ex)
    {
      CError tError = new CError();
      tError.moduleName = "CasePolicyBLS";
      tError.functionName = "submitData";
      tError.errorMessage = ex.toString();
      this.mErrors .addOneError(tError);
      try
      {
        conn.rollback() ;
        conn.close();
      }
      catch(Exception e)
      {
      }

      return false;
    }
    System.out.println("执行结束！！");
    return true;
  }
}
