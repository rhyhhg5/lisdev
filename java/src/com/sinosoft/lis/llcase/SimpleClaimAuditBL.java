package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: SimpleClaimAuditBL </p>
 * <p>Description: 团体批量审定 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft </p>
 * @author:Xx
 * @CreateDate：2005-02-18 13:58:55
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class SimpleClaimAuditBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mG = new GlobalInput();
    private PubFun1 pf1 = new PubFun1();
    private PubFun pf =new PubFun();
    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCaseSet mLLCaseSet = new LLCaseSet();
    private LLCaseSet mbackCaseSet = new LLCaseSet();
    private LLClaimUserSchema mLLClaimUserSchema = new LLClaimUserSchema();
    private String mReturnMessage = "";
    private String aDate = "";
    private String aTime = "";
    /** 社保保单标志 0：非社保；1：社保*/
    private String mSocialSecurity = "0";

//    private String mRgtNo = "";

    public SimpleClaimAuditBL() {
    }

    public static void main(String[] args) {
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLCaseSet tLLCaseSet = new LLCaseSet();
        VData tVData = new VData();
        SimpleClaimAuditBL tSimpleClaimAuditBL = new SimpleClaimAuditBL();

        GlobalInput mG = new GlobalInput();
        mG.ManageCom = "86";
        mG.ComCode = "86";
        mG.Operator = "cm0012";
        tLLRegisterSchema.setRgtNo("P9400070214000001");
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setCaseNo("C9400070215000128");
        tLLCaseSchema.setRgtState("03");
        tLLCaseSchema.setCancleReason("1");
        tLLCaseSchema.setCancleRemark("郁闷郁闷郁闷");
        tLLCaseSet.add(tLLCaseSchema);
        tVData.add(tLLCaseSet);
        tVData.add(tLLRegisterSchema);
        tVData.add(mG);
        tSimpleClaimAuditBL.submitData(tVData, "UW||MAIN");
        tVData.clear();
        tVData = tSimpleClaimAuditBL.getResult();
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行数据基本校验
        if (!checkData())
            return false;
        //社保保单结案锁定和解锁
        if (!mOperate.equals("BACK||MAIN")) {        	
        	if(!chkGrpcont()) {
        		return false;
        	}
        }
        //进行业务处理
        if (!dealData()) {
            mReturnMessage += "数据处理失败SimpleClaimAuditBL-->dealData!";
            CError.buildErr(this,mReturnMessage);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, null)) {
            CError.buildErr(this,"数据提交失败!");
            return false;
        }
        System.out.println("End SimpleClaimAuditBL Submit...");

        mInputData.clear();
        map = new MMap();
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        String tRgtNo = mLLRegisterSchema.getRgtNo();
        tLLRegisterDB.setRgtNo(tRgtNo);
        if (tLLRegisterDB.getInfo()) {
            LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
            tLLRegisterSchema = tLLRegisterDB.getSchema();
            LLCaseDB tLLCaseDB = new LLCaseDB();
            LLCaseSet tLLCaseSet = new LLCaseSet();
            String tsql =
                    "select * from llcase where rgtstate not in('09','14') and rgtno='"
                    + tRgtNo + "'";
            System.out.println(tsql);
            tLLCaseSet = tLLCaseDB.executeQuery(tsql);
            System.out.println(tLLRegisterSchema.getRgtState());
            if (tLLCaseSet != null && tLLCaseSet.size() > 0) {
                if (tLLRegisterSchema.getRgtState().equals("03"))
                    tLLRegisterSchema.setRgtState("02");
            } else {
                if (tLLRegisterSchema.getRgtState().equals("02"))
                    tLLRegisterSchema.setRgtState("03");
            }
            map.put(tLLRegisterSchema, "UPDATE");
            mInputData.add(map);
            mResult.add(mReturnMessage);
            tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, null)) {
                CError.buildErr(this,"数据提交失败!");
                return false;
            }
        }
        return true;
    }
    //社保保单结案锁定和解锁 start
    private boolean chkGrpcont() {
    	System.out.println("chkGrpcont------start");
    	LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        if (!tLLRegisterDB.getInfo()) {
            CError.buildErr(this, "团体立案信息查询失败!");
            return false;
        }
        mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
        String LLRgtno = mLLRegisterSchema.getRgtNo();//批次号
        String tLLFlag = LLCaseCommon.checkSocialSecurity(LLRgtno, "rgtno");
        String LLGrpContno = mLLRegisterSchema.getRgtObjNo();//保单号
        String tGrpContNoFlag = LLCaseCommon.checkSocialSecurity(LLGrpContno, "grpno");
        double sumPayMoney = 0 ;//历史赔付总额
        double sumGetMoney = 0 ;//实收总保费
        double onePayMoney = 0 ;//本次赔款
        String projectNo = "";
        String projectName = "";
        String cvalidate = "";
        String Cvalidate = "";
        String Cvalidate1 = "";
        String Cvalidate2 = "";
        if("1".equals(tGrpContNoFlag)) {
        	String pSQL = "select state from lllock where 1=1 and grpcontno = '" + LLGrpContno + "'" + 
        				  " and rgtno = '" + LLRgtno + "' with ur";
        	ExeSQL pExeSQL = new ExeSQL();
        	SSRS pSSRS = pExeSQL.execSQL(pSQL);
        	if(pSSRS.getMaxRow()>0) {
        		String state = pSSRS.GetText(1, 1);
        		if("2".equals(state)) {
        			return true;
        		}
        	}

			String sSql = "select a.projectno,a.projectname,b.cvalidate from lcgrpcontsub a,lcgrpcont b "
  			      + "where 1=1 and a.prtno=b.prtno and b.grpcontno = '" + LLGrpContno + 
  			      "' and a.projectno is not null " +
  			      "union " + 
  			      "select a.projectno,a.projectname,b.cvalidate from lcgrpcontsub a,lbgrpcont b "
			      + "where 1=1 and a.prtno=b.prtno and b.grpcontno = '" + LLGrpContno + 
			      "' and a.projectno is not null with ur";
		  	ExeSQL sExeSQL = new ExeSQL();
		  	SSRS sSSRS = sExeSQL.execSQL(sSql);
		  	if(sSSRS.getMaxRow()>0) {        		
		  		projectNo = sSSRS.GetText(1, 1);
		  		projectName = sSSRS.GetText(1, 2);
		  		cvalidate = sSSRS.GetText(1, 3);
		  		Cvalidate = cvalidate.substring(0, 4);
		  		Cvalidate1 = Cvalidate + "-1-1";
		  		Cvalidate2 = Cvalidate + "-12-31";
		  		//lifeiyang#3782new+start
		  	}else {
		  		return true;
		  	}
		  		//lifeiyang#3782new+end
		  	//年度实收保费
		  	String LLSql = "select a.grpcontno ,b.projectname ,b.projectno ," + 
		  				   "COALESCE((select sum(sumactupaymoney) from ljapay where incomeno = a.grpcontno and duefeetype = 0),0) ," +
		  				   "COALESCE((select sum(sumactupaymoney) from ljapay where incomeno = a.grpcontno and duefeetype = 1),0) ," + 
		  				   "COALESCE((select sum(lja.sumactupaymoney) from lpgrpedoritem lpg,lpedorapp lpe,ljapay lja where " + 
		  				   "lpg.edorno=lpe.edoracceptno and  lpg.edorno=lja.incomeno and lpe.edorstate='0' and lpg.grpcontno=a.grpcontno),0) ," + 
		  				   "COALESCE((select sum(sumgetmoney) from lpgrpedoritem lpg,lpedorapp lpe,ljaget lja where " + 
		  				   "lpg.edorno=lpe.edoracceptno and lpg.edorno=lja.otherno and lpe.edorstate='0' and lpg.grpcontno=a.grpcontno),0) " + 
		  				   "from lcgrpcont a,lcgrpcontsub b where 1=1 and a.prtno = b.prtno and a.appflag = '1' and a.stateflag in('1','2','3') " + 
		  				   "and b.projectno ='" + projectNo + "' " + 
		  				   "and b.projectno is not null " + 
		  				   "and a.cvalidate between '" + Cvalidate1 + "' and '" + Cvalidate2 + "' " +
		  				   "union " +
		  				   "select a.grpcontno ,b.projectname ,b.projectno ," + 
		  				   "COALESCE((select sum(sumactupaymoney) from ljapay where incomeno = a.grpcontno and duefeetype = 0),0) ," +
		  				   "COALESCE((select sum(sumactupaymoney) from ljapay where incomeno = a.grpcontno and duefeetype = 1),0) ," + 
		  				   "COALESCE((select sum(lja.sumactupaymoney) from lpgrpedoritem lpg,lpedorapp lpe,ljapay lja where " + 
		  				   "lpg.edorno=lpe.edoracceptno and  lpg.edorno=lja.incomeno and lpe.edorstate='0' and lpg.grpcontno=a.grpcontno),0) ," + 
		  				   "COALESCE((select sum(sumgetmoney) from lpgrpedoritem lpg,lpedorapp lpe,ljaget lja where " + 
		  				   "lpg.edorno=lpe.edoracceptno and lpg.edorno=lja.otherno and lpe.edorstate='0' and lpg.grpcontno=a.grpcontno),0) " + 
		  				   "from lbgrpcont a,lcgrpcontsub b where 1=1 and a.prtno = b.prtno and a.appflag = '1' and a.stateflag in('1','2','3') " + 
		  				   "and b.projectno ='" + projectNo + "' " + 
		  				   "and b.projectno is not null " + 
		  				   "and a.cvalidate between '" + Cvalidate1 + "' and '" + Cvalidate2 + "' with ur" ;
		  	ExeSQL lExeSQL = new ExeSQL();
		  	SSRS lSSRS = lExeSQL.execSQL(LLSql);
		  	int LLNo = lSSRS.getMaxRow();
		  	if(LLNo>0) {     
		  		for(int i=1;i<=LLNo;i++) {
		  			String firstPayFee = lSSRS.GetText(i, 4);
		  			String renewalPayFee = lSSRS.GetText(i, 5);
		  			String prePayFee = lSSRS.GetText(i, 6);
		  			String outPayFee = lSSRS.GetText(i, 7);
		  			double allPayFee = Double.parseDouble(firstPayFee) + Double.parseDouble(renewalPayFee) + Double.parseDouble(prePayFee) - Double.parseDouble(outPayFee);
		  			sumGetMoney += allPayFee;
		  		}
		  	}
		  	//历史年度赔款
		  	String LLSQL = "select  sum(lja.pay) " + 
	  				   "from ljagetclaim lja,lcgrpcont a,lcgrpcontsub b " + 
	  				   "where 1=1 and a.prtno = b.prtno and a.grpcontno = lja.grpcontno " +  
	  				   "and b.projectno ='" + projectNo + "' " +  
	  				   "and b.projectno is not null " + 
	  				   "and a.cvalidate between '" + Cvalidate1 + "' and '" + Cvalidate2 + "' " + 
	  				   "union " + 
	  				   "select  sum(lja.pay) " + 
	  				   "from ljagetclaim lja,lbgrpcont a,lcgrpcontsub b " + 
	  				   "where 1=1 and a.prtno = b.prtno and a.grpcontno = lja.grpcontno " +  
	  				   "and b.projectno ='" + projectNo + "' " +  
	  				   "and b.projectno is not null " + 
	  				   "and a.cvalidate between '" + Cvalidate1 + "' and '" + Cvalidate2 + "' with ur";    
		  	ExeSQL llExeSQL = new ExeSQL();
		  	SSRS LSSRS = llExeSQL.execSQL(LLSQL);
		  	int tLFlag = LSSRS.getMaxRow();
		  	if(tLFlag>0) {
		  		String getMoney = LSSRS.GetText(1, 1);
		  		if(!"null".equals(getMoney) && getMoney!=null && !"".equals(getMoney)){        			
		  			sumPayMoney = Double.parseDouble(getMoney);
		  		}
		  	}
		  	//本次赔款
		  	String mSql = "select sum(realpay) from llclaim where 1=1 and rgtno = '" + LLRgtno + "' and realpay is not null with ur";
		  	ExeSQL mExeSQL = new ExeSQL();
		  	SSRS mSSRS = mExeSQL.execSQL(mSql);
		  	int tNumber = mSSRS.getMaxRow();
		  	if(tNumber>0) {
		  		String onePay = mSSRS.GetText(1, 1);
		  		onePayMoney = Double.parseDouble(onePay);
		  	}
		  	double diffValue = sumGetMoney * 0.95 - (sumPayMoney + onePayMoney) ;
		  	// add 2018-11-23 
		  	double surplus = sumGetMoney - (sumPayMoney + onePayMoney); 
		  	
		  	String fTimeNo = pf.getCurrentDate2();
	  		String serialno = "LS"+fTimeNo+""+ pf1.CreateMaxNo(fTimeNo+"JS", 8);
	  		System.out.println("serialno= " + serialno);
	  		// add 2018-11-23
	  		if(diffValue <= 0 && surplus >0){
	  			CError.buildErr(this, projectName+"项目，项目编码"+projectNo+"，“百分之九十五实收保费-赔款支出” ≤0，请及时进行保费增补");
	  		}
		  	
	  		//modify 2018-11-23
		  	//if(diffValue < 0) {
	  		//lifeiyang_#3782new_set_start
	  		if(surplus < 0) {
	  		//lifeiyang_#3782new_set_end
		  		MMap tmap = new MMap();
		  		String fSQL = "select state from lllock where 1=1 and grpcontno = '" + LLGrpContno + "'" + 
      				  " and rgtno = '" + LLRgtno + "' with ur";
		  		ExeSQL fExeSQL = new ExeSQL();
	        	SSRS fSSRS = fExeSQL.execSQL(fSQL);
	        	int fNumber = fSSRS.getMaxRow();
	        	if(fNumber > 0) {
	        		String fDel = "delete from lllock where 1=1 and grpcontno = '" + LLGrpContno + "'" + 
	        				      " and rgtno = '" + LLRgtno + "' with ur";
	        		tmap.put(fDel, "DELETE");	
	        	}
		  		//锁定表
		  		LLLockSchema tLLLockSchema = new LLLockSchema();
		  		LLLockSet mLLLockSet = new LLLockSet();
		  		tLLLockSchema.setGrpContNo(LLGrpContno);
		  		tLLLockSchema.setRgtno(LLRgtno);
		  		tLLLockSchema.setProjectNo(projectNo);
		  		tLLLockSchema.setProjectName(projectName);
		  		tLLLockSchema.setState("1");
		  		tLLLockSchema.setLockDate(aDate);
		  		tLLLockSchema.setLockTime(aTime);
		  		tLLLockSchema.setCvalidate(cvalidate);
		  		tLLLockSchema.setSumGetFee(sumGetMoney);//实收总保费
		  		tLLLockSchema.setSumPayFee(sumPayMoney);//历史赔款
		  		tLLLockSchema.setOnePayFee(onePayMoney);//本次赔款
		  		tLLLockSchema.setOperator(mG.Operator);
		  		tLLLockSchema.setManagecom(mG.ManageCom);
		  		tLLLockSchema.setMakeDate(aDate);
		  		tLLLockSchema.setMakeTime(aTime);
		  		tLLLockSchema.setModifyDate(aDate);
		  		tLLLockSchema.setModifyTime(aTime);
		  		mLLLockSet.add(tLLLockSchema);
		  		//锁定轨迹表
		  		LLLockRecordSchema tLLLockRecordSchema = new LLLockRecordSchema();
		  		LLLockRecordSet tLLLockRecordSet = new LLLockRecordSet();		  		
		  		//流水号
		  		tLLLockRecordSchema.setLLFlowNumber(serialno);
		  		//历史实收保费
		  		tLLLockRecordSchema.setSumGetFee(sumGetMoney);
		  		tLLLockRecordSchema.setGrpContNo(LLGrpContno);
		  		tLLLockRecordSchema.setRgtno(LLRgtno);
		  		tLLLockRecordSchema.setState("1");
		  		tLLLockRecordSchema.setLockDate(aDate);
		  		tLLLockRecordSchema.setLockTime(aTime);
		  		tLLLockRecordSchema.setOperator(mG.Operator);
		  		tLLLockRecordSchema.setManagecom(mG.ManageCom);
		  		tLLLockRecordSchema.setMakeDate(aDate);
		  		tLLLockRecordSchema.setMakeTime(aTime);
		  		tLLLockRecordSchema.setModifyDate(aDate);
		  		tLLLockRecordSchema.setModifyTime(aTime);
		  		tLLLockRecordSet.add(tLLLockRecordSchema);
		  		tmap.put(mLLLockSet, "INSERT");
		  		tmap.put(tLLLockRecordSet, "INSERT");
		  		this.mInputData.clear();
		  		mInputData.add(tmap);
		  		PubSubmit tPubSubmit = new PubSubmit();
		          if (!tPubSubmit.submitData(mInputData, null)) {
		                // @@错误处理
		                CError.buildErr(this, "数据提交失败!");
		                return false;
		          }
		          //modify 2018-11-23
		        CError.buildErr(this, projectName+"项目，项目编码"+projectNo+"，赔款支出已达到实收保费，不能审定!");
		        //CError.buildErr(this, "项目下年度的累计赔付已超项目年度百分之九十五实收总保费，不能审定!");
		  		return false;
		  	}else {
		  		String wSQL = "select state from lllock where 1=1 and grpcontno = '" + LLGrpContno + "'" + 
      				  " and rgtno = '" + LLRgtno + "' with ur";
		  		ExeSQL wExeSQL = new ExeSQL();
		  		SSRS wSSRS = wExeSQL.execSQL(wSQL);
		  		if(wSSRS.getMaxRow()>0) {
		  			LLLockDB wLLLockDB = new LLLockDB();
		  			wLLLockDB.setGrpContNo(LLGrpContno);
		  			wLLLockDB.setRgtno(LLRgtno);
		  			if(!wLLLockDB.getInfo()) {
		  				CError.buildErr(this, "锁定信息查询失败!");
		  	            return false;
		  			}
		  			LLLockSchema wLLLockSchema = new LLLockSchema();
		  			LLLockSet wLLLockSet = new LLLockSet();
		  			wLLLockSchema = wLLLockDB.getSchema();
		  			wLLLockSchema.setOperator(mG.Operator);
		  			wLLLockSchema.setManagecom(mG.ManageCom);
		  			wLLLockSchema.setSumGetFee(sumGetMoney);
		  			wLLLockSchema.setSumPayFee(sumPayMoney);
		  			wLLLockSchema.setOnePayFee(onePayMoney);
		  			wLLLockSchema.setState("2");
		  			wLLLockSchema.setModifyDate(aDate);
		  			wLLLockSchema.setModifyTime(aTime);
		  			wLLLockSet.add(wLLLockSchema);
		  			
		  			LLLockRecordSchema wLLLockRecordSchema = new LLLockRecordSchema();
		  			LLLockRecordSet wLLLockRecordSet = new LLLockRecordSet();
		  			wLLLockRecordSchema.setLLFlowNumber(serialno);
		  			wLLLockRecordSchema.setSumGetFee(sumGetMoney);
		  			wLLLockRecordSchema.setGrpContNo(LLGrpContno);
		  			wLLLockRecordSchema.setRgtno(LLRgtno);
		  			wLLLockRecordSchema.setState("2");
		  			wLLLockRecordSchema.setOperator(mG.Operator);
		  			wLLLockRecordSchema.setManagecom(mG.ManageCom);
		  			wLLLockRecordSchema.setLockDate(aDate);
		  			wLLLockRecordSchema.setLockTime(aTime);
		  			wLLLockRecordSchema.setMakeDate(aDate);
		  			wLLLockRecordSchema.setMakeTime(aTime);
		  			wLLLockRecordSchema.setModifyDate(aDate);
		  			wLLLockRecordSchema.setModifyTime(aTime);
		  			wLLLockRecordSchema.setLockAttri("automatic");//自动解锁
		  			wLLLockRecordSet.add(wLLLockRecordSchema);
		  			
		  			MMap tmap = new MMap();
		  			tmap.put(wLLLockSet, "UPDATE");
		  			tmap.put(wLLLockRecordSet, "INSERT");
			  		this.mInputData.clear();
			  		mInputData.add(tmap);
		  			PubSubmit tPubSubmit = new PubSubmit();
			          if (!tPubSubmit.submitData(mInputData, null)) {
			                // @@错误处理
			                CError.buildErr(this, "数据提交失败!");
			                return false;
			          }
		  		}
		  	}
        }
    	return true;
    }
    //社保保单结案锁定和解锁 end

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("--Star GetInputData--");
        mLLRegisterSchema = (LLRegisterSchema) cInputData.
                            getObjectByObjectName("LLRegisterSchema", 0);
        mLLCaseSet = ((LLCaseSet) cInputData.getObjectByObjectName("LLCaseSet", 0));
        mG = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        aDate = PubFun.getCurrentDate();
        aTime = PubFun.getCurrentTime();
        return true;
    }

    /**
     * 处理前检查数据是否合理
     * @return boolean
     */
    private boolean checkData(){
        System.out.println("---start checkData---");
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        if (!tLLRegisterDB.getInfo()) {
            CError.buildErr(this, "团体立案信息查询失败!");
            return false;
        }
        mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
        String declineflag = "" + mLLRegisterSchema.getDeclineFlag();
        if (declineflag.equals("1")) {
            CError.buildErr(this, "该团体申请已撤件，不能再做审定通过或回退操作!");
            return false;
        }
        
  	  	//#2169 批次导入前后台数据同步问题方案
  	  	if(!LLCaseCommon.checkGrpRgtState(mLLRegisterSchema.getRgtNo())){
  	  		//@@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "dealData";
            tError.errorMessage = "批次号:"+mLLRegisterSchema.getRgtNo()+"还在处理中，请稍候再处理该批次的案件!";
            this.mErrors.addOneError(tError);
            return false;
  	  	}
        
        if ("04".equals(mLLRegisterSchema.getRgtState())
            ||"05".equals(mLLRegisterSchema.getRgtState())) {
            CError.buildErr(this,"该团体批次已经给付确认，不能再做审定通过或回退操作!");
            return false;
        }
        //团体审定、审批时，在数据校验阶段，需要添加社保用户校验流程。目的是向核赔类传入操作人
        String tSSFlag = LLCaseCommon.checkSocialSecurity(mLLRegisterSchema.getRgtNo(), "rgtno");
        if("1".equals(tSSFlag)){
        	LLSocialClaimUserDB tLLSocialClaimUserDB = new LLSocialClaimUserDB();
        	tLLSocialClaimUserDB.setUserCode(mG.Operator);
        	tLLSocialClaimUserDB.setStateFlag("1");//社保用户有效
			tLLSocialClaimUserDB.setHandleFlag("1");//参与案件分配
			LLSocialClaimUserSet tSocialClaimUserSet = tLLSocialClaimUserDB.query();
        	if(tSocialClaimUserSet.size() <= 0){
        		CError.buildErr(this,"您不是社保参与案件分配的审核人或已失效，无权作审定操作");
                return false;
        	}
        }else{
        	LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
            tLLClaimUserDB.setUserCode(mG.Operator);
            if(!tLLClaimUserDB.getInfo()){
                CError.buildErr(this,"您不是理赔人，无权作审定操作");
                return false;
            }
            if(tLLClaimUserDB.getClaimPopedom()==null){
                CError.buildErr(this,"您没有理赔权限，无权作审定操作");
                return false;
            }
            mLLClaimUserSchema = tLLClaimUserDB.getSchema();
            tLLClaimUserDB.setUserCode(mLLClaimUserSchema.getUpUserCode());
            if(tLLClaimUserDB.getInfo())
                mLLClaimUserSchema.setComCode(tLLClaimUserDB.getClaimPopedom());
        }
        
        LLCaseSet tLLCaseSet = mLLCaseSet;
        mLLCaseSet = new LLCaseSet();
        for(int i= 1;i<=tLLCaseSet.size();i++){
            LLCaseDB tLLCaseDB = new LLCaseDB();
            String tCaseNo = tLLCaseSet.get(i).getCaseNo();
            tLLCaseDB.setCaseNo(tCaseNo);
            String treason = tLLCaseSet.get(i).getCancleReason();
            String tremark = tLLCaseSet.get(i).getCancleRemark();
            if (!tLLCaseDB.getInfo()) {
                mReturnMessage += "<br>个人案件查询失败,案件号："+tCaseNo;
                continue;
            }
            if("11".equals(tLLCaseDB.getRgtState())||
               "12".equals(tLLCaseDB.getRgtState())){
                mReturnMessage += "<br>案件"+tCaseNo+"已经确认给付，不能再做审定操作。";
                continue;
            }
            tLLCaseDB.setCancleReason(treason);
            tLLCaseDB.setCancleRemark(tremark);
            mLLCaseSet.add(tLLCaseDB.getSchema());
        }
        if(mLLCaseSet.size()<=0){
            mReturnMessage +="<br>没有符合审定条件的案件。";
            CError.buildErr(this,mReturnMessage);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("---Star dealData---mOperate: " + mOperate);
        if (!mOperate.equals("BACK||MAIN")) {
            System.out.println("---Star PASS---" + mLLCaseSet.size());
            
            LLCaseSet tLLCaseSet = new LLCaseSet();
            tLLCaseSet.set(mLLCaseSet);
            mLLCaseSet = new LLCaseSet();
            for(int i=1;i<=tLLCaseSet.size();i++){
                LLCaseSchema tcase = tLLCaseSet.get(i);               
                if (mOperate.equals("UW||MAIN")) {
                    if (!dealClaim(tcase)) {
                        continue;
                    }
                    tcase.setRgtState("04");
                    tcase.setHandler(mG.Operator);
                }
                if(!UWCase(tcase))
                    continue;
            }
        }

        if (this.mOperate.equals("BACK||MAIN")) {
            mbackCaseSet.add(mLLCaseSet);
        }
        CaseBack(mbackCaseSet);
        return true;
    }

    private boolean dealClaim(LLCaseSchema aCaseSchema) {
        VData tvdata = new VData();
        UnitClaimSaveBL tUnitClaimSaveBL = new UnitClaimSaveBL();
        tvdata.add(aCaseSchema);
        tvdata.add(mG);
        if(!(tUnitClaimSaveBL.submitData(tvdata,"BATCH"))){
            CErrors tError = tUnitClaimSaveBL.mErrors;
            String ErrMessage = tError.getFirstError();
            aCaseSchema.setCancleRemark(ErrMessage);
            aCaseSchema.setCancleReason("4");
            mbackCaseSet.add(aCaseSchema);
            mReturnMessage += "<br>案件"+aCaseSchema.getCaseNo()
                    +"信息不全，被自动回退到批改信箱。";
            return false;
        }
        return true;
    }

    private boolean UWCase(LLCaseSchema aCaseSchema){
        VData tvdata = new VData();
        //此处为循环调用核赔类的入口，在此处判断此批次下案件是否为社保案件，若为社保案件，传入社保批次标志                
        mSocialSecurity = LLCaseCommon.checkSocialSecurity(aCaseSchema.getCaseNo(), null);
        ClaimUnderwriteBL tClaimUnderwriteBL = new ClaimUnderwriteBL();
        tvdata.clear();
        tvdata.add(aCaseSchema);
        tvdata.add(mLLClaimUserSchema);
        tvdata.add(mG);
        //将社保标记传入ClaimUnderwriteBL
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("mSocialSecurity", mSocialSecurity);
        tvdata.add(tTransferData);
        System.out.println("社保标记为："+mSocialSecurity);
        if(!tClaimUnderwriteBL.submitData(tvdata,"BATCH||UW")){
            CErrors tError = tClaimUnderwriteBL.mErrors;
            String ErrMessage = tError.getFirstError();
            mReturnMessage += "<br>案件" + aCaseSchema.getCaseNo()
                    + "审批失败,原因是:"+ErrMessage;
            return false;
        }
        VData tResultData = tClaimUnderwriteBL.getResult();
        String strResult = (String) tResultData.getObjectByObjectName("String", 0);
        mReturnMessage+="<br>"+aCaseSchema.getCaseNo()+strResult;
        return true;
    }
    /**
     * 案件回退
     * 1。审定人发现案件错误，做案件回退
     * 2。系统审定操作不能通过，做自动回退
     * @return boolean
     */
    private boolean CaseBack(LLCaseSet aLLCaseSet) {
        System.out.println("回退。。。"+aLLCaseSet.size());
        LLCaseBackSet tLLCaseBackSet = new LLCaseBackSet();
        LLCaseBackSet oLLCaseBackSet = new LLCaseBackSet();
        LLCaseOpTimeSet tLLCaseOpTimeSet = new LLCaseOpTimeSet();
        boolean tflag= false;
        for(int i=1;i<=aLLCaseSet.size();i++){
            LLCaseSchema tLLCaseSchema = aLLCaseSet.get(i);
            LLCaseBackSchema tLLCaseBackSchema = new LLCaseBackSchema();
            String oRgtState = ""+tLLCaseSchema.getRgtState();
            String oRgtStateName = "";
            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCode(oRgtState);
            tLDCodeDB.setCodeType("llrgtstate");
            if(tLDCodeDB.getInfo())
                oRgtStateName = tLDCodeDB.getCodeName();
            if(!oRgtState.equals("03")&&!oRgtState.equals("04")&&
               !oRgtState.equals("05")&&!oRgtState.equals("06")&&
               !oRgtState.equals("09")&&!oRgtState.equals("10")){
                //写报错信息返回
                mReturnMessage+="案件"+tLLCaseSchema.getCaseNo()+"当前为"
                        +oRgtStateName+",不能做回退操作<br>";
                continue;
            }
            String tLimit = PubFun.getNoLimit(this.mG.ManageCom);
            String CaseBackNo = PubFun1.CreateMaxNo("CaseBack", tLimit);

            tLLCaseBackSchema.setBeforState(oRgtState);
            tLLCaseBackSchema.setCaseBackNo(CaseBackNo);
            tLLCaseBackSchema.setAviFlag("Y");
            tLLCaseBackSchema.setRgtNo(tLLCaseSchema.getRgtNo());
            tLLCaseBackSchema.setCaseNo(tLLCaseSchema.getCaseNo());
            tLLCaseBackSchema.setOHandler(tLLCaseSchema.getHandler());
            tLLCaseBackSchema.setNHandler(tLLCaseSchema.getHandler());
            tLLCaseBackSchema.setAfterState("01");
            tLLCaseBackSchema.setReason(tLLCaseSchema.getCancleReason());
            tLLCaseBackSchema.setRemark(tLLCaseSchema.getCancleRemark());
            tLLCaseBackSchema.setMngCom(mG.ManageCom);
            tLLCaseBackSchema.setOperator(mG.Operator);
            tLLCaseBackSchema.setMakeDate(aDate);
            tLLCaseBackSchema.setMakeTime(aTime);
            tLLCaseBackSchema.setModifyDate(aDate);
            tLLCaseBackSchema.setModifyTime(aTime);

            tLLCaseBackSet.add(tLLCaseBackSchema);
            
            //#740 批次导入后、案件回退时增加轨迹表数据
            //add by GY 2013-1-14
            LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
            tLLCaseOpTimeSchema.setCaseNo(tLLCaseSchema.getCaseNo());
            tLLCaseOpTimeSchema.setRgtState("01");
            tLLCaseOpTimeSchema.setSequance("1");
            tLLCaseOpTimeSchema.setManageCom(mG.ManageCom);
            tLLCaseOpTimeSchema.setOperator(mG.Operator);
            tLLCaseOpTimeSchema.setStartDate(aDate);
            tLLCaseOpTimeSchema.setStartTime(aTime);
            tLLCaseOpTimeSchema.setOpTime("0:00:00");
            
            tLLCaseOpTimeSet.add(tLLCaseOpTimeSchema);
            
            //保存后当前回退记录生效，需要把过去的回退记录设为过期
            LLCaseBackDB tLLCaseBackDB = new LLCaseBackDB();
            tLLCaseBackDB.setCaseNo(tLLCaseSchema.getCaseNo());
            tLLCaseBackDB.setAviFlag("Y");
            LLCaseBackSet tempLLCaseBackSet = new LLCaseBackSet();
            tempLLCaseBackSet = tLLCaseBackDB.query();
            for(int x=1;x<=tempLLCaseBackSet.size();x++){
                tempLLCaseBackSet.get(x).setAviFlag("");
                tempLLCaseBackSet.get(x).setModifyDate(aDate);
                tempLLCaseBackSet.get(x).setModifyTime(aTime);
            }
            oLLCaseBackSet.add(tempLLCaseBackSet);
            aLLCaseSet.get(i).setRgtState("01");
            aLLCaseSet.get(i).setCancleReason("");
            aLLCaseSet.get(i).setCancleRemark("");
            aLLCaseSet.get(i).setUWer(mG.Operator);
            aLLCaseSet.get(i).setDealer(mG.Operator);
            aLLCaseSet.get(i).setModifyDate(aDate);
            aLLCaseSet.get(i).setModifyTime(aTime);
            tflag = true;
        }
        if(tflag){
        	String LockSql = "select * from lllock where 1=1 and rgtno = '" + aLLCaseSet.get(1).getRgtNo() + "' with ur";
        	ExeSQL LExeSQL = new ExeSQL();
        	SSRS LSSRS = LExeSQL.execSQL(LockSql);
        	int number = LSSRS.getMaxRow();
        	if(number>0) {
        		String delSQL = "delete from lllock where rgtno = '" + aLLCaseSet.get(1).getRgtNo() + "' with ur";
        		map.put(delSQL, "DELETE");
        	}
            map.put(aLLCaseSet, "UPDATE");
            map.put(tLLCaseBackSet, "INSERT");
            map.put(oLLCaseBackSet, "UPDATE");
            map.put(tLLCaseOpTimeSet, "INSERT");
        }
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLLRegisterSchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(mReturnMessage);
            mResult.add(this.mLLCaseSet);
        } catch (Exception ex) {
            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
