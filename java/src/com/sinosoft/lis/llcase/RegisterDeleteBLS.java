/**
 * <p>Title: "立案"模块中基本功能的删除和修改功能；</p>
 * <p>Description: </p>
 * <p>Copyright: 2002-11-08
 * <p>Company: Sinosoft< /p>
 * @author 刘岩松
 * @version 1.0
 */
package com.sinosoft.lis.llcase;
import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

public class RegisterDeleteBLS
{
  //传输数据类
  private VData mInputData = new VData();
  private String operate_flag;
  private String t_caseno;
  LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
  LLCaseSchema tLLCaseSchema = new LLCaseSchema();
  LLRgtAffixSchema tLLRgtAffixSchema = new LLRgtAffixSchema();
  LLCasePolicySchema tLLCasePolicySchema = new LLCasePolicySchema(); //分案保单
  LLCaseInfoSchema tLLCaseInfoSchema = new LLCaseInfoSchema();       //疾病
  LLCaseReceiptSchema tLLCaseReceiptSchema = new LLCaseReceiptSchema();//收据
  LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();//分案诊疗明细

  public  CErrors mErrors = new CErrors();

  public String mOperate;

  public RegisterDeleteBLS() {}

  public static void main(String[] args)
  {
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData=(VData)cInputData.clone();
    System.out.println("开始执行到BLS！！！");
    //首先将数据在本类中做一个备份
    if(1==1)
    {

      operate_flag = (String)mInputData.get(0);
      System.out.println("操作字符是"+operate_flag);
      t_caseno = (String)mInputData.get(1);
      System.out.println("分案号码是"+t_caseno);
      Connection conn = DBConnPool.getConnection();
      tLLRegisterSchema=(LLRegisterSchema)mInputData.getObjectByObjectName("LLRegisterSchema",0);
      LLRegisterDB tLLRegisterDB = new LLRegisterDB(conn);
      LLCaseDB tLLCaseDB = new LLCaseDB(conn);
      LLRgtAffixDB tLLRgtAffixDB = new LLRgtAffixDB(conn);
      LLCasePolicyDB tLLCasePolicyDB = new LLCasePolicyDB(conn);
      LLCaseInfoDB tLLCaseInfoDB = new LLCaseInfoDB(conn);
      LLCaseCureDB tLLCaseCureDB = new LLCaseCureDB(conn);
      LLCaseReceiptDB tLLCaseReceiptDB = new LLCaseReceiptDB(conn);
      //在同一事务中处理七个表的操作要把下条语句添加上
      try
      {
        tLLRegisterDB= new LLRegisterDB(conn);
        tLLRegisterDB.setRgtNo(tLLRegisterSchema.getRgtNo());
        System.out.println("------rgtno:"+tLLRegisterSchema.getRgtNo());
        if (!tLLRegisterDB.deleteSQL())
        {
          this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "RegisterDeleteBL";
          tError.functionName = "DeltetData";
          tError.errorMessage = "删除失败!";
          this.mErrors .addOneError(tError) ;
          System.out.println(tError);
          conn.rollback();
          conn.close();
          return false;
        }//end of LLRegisterDB.deleteSQL;

        tLLCaseDB = new LLCaseDB(conn);
        tLLCaseDB.setRgtNo(tLLRegisterSchema.getRgtNo());
        if (!tLLCaseDB.deleteSQL())
        {
          // @@错误处理
          this.mErrors.copyAllErrors(tLLCaseDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "RegisterDeleteBL";
          tError.functionName = "DeltetData";
          tError.errorMessage = "删除失败!";
          this.mErrors .addOneError(tError) ;
          System.out.println(tError);
          conn.rollback();
          conn.close();
          return false;
        }//end of LLCaseDB.deleteSQL;

        tLLRgtAffixDB = new LLRgtAffixDB(conn);
        tLLRgtAffixDB.setRgtNo(tLLRegisterSchema.getRgtNo());
        if (!tLLRgtAffixDB.deleteSQL())
        {
          // @@错误处理
          this.mErrors.copyAllErrors(tLLRgtAffixDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "RegisterDeleteBL";
          tError.functionName = "DeltetData";
          tError.errorMessage = "删除失败!";
          this.mErrors .addOneError(tError) ;
          System.out.println(tError);
          conn.rollback();
          conn.close();
          return false;
        }//end of LLRgtAffixDB.deleteSQL;
        if(operate_flag == "Y")
        {
          System.out.println("开始执行删除分案保单明细表的程序");
          tLLCasePolicyDB = new LLCasePolicyDB(conn);
          tLLCasePolicyDB.setCaseNo(t_caseno);
          if (!tLLCasePolicyDB.deleteSQL())
          {
            // @@错误处理
            this.mErrors.copyAllErrors(tLLRgtAffixDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLRegisterDeleteBL";
            tError.functionName = "DeltetData";
            tError.errorMessage = "分案保单明细表删除失败！！！";
            this.mErrors .addOneError(tError) ;
            System.out.println(tError);
            conn.rollback();
            conn.close();
            return false;
          }

          System.out.println("开始执行删除分案诊疗明细表的程序");
          tLLCaseCureDB = new LLCaseCureDB(conn);
          tLLCaseCureDB.setCaseNo(t_caseno);

          if (!tLLCaseCureDB.deleteSQL())
          {
            // @@错误处理
            this.mErrors.copyAllErrors(tLLRgtAffixDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLRegisterDeleteBL";
            tError.functionName = "DeltetData";
            tError.errorMessage = "删除分案诊疗明细信息时出险失败！！！";
            this.mErrors .addOneError(tError) ;
            System.out.println(tError);
            conn.rollback();
            conn.close();
            return false;
          }//end of LLRgtAffixDB.deleteSQL;

          System.out.println("开始执行删除分案疾病明细表的程序");
          tLLCaseInfoDB = new LLCaseInfoDB(conn);
          tLLCaseInfoDB.setCaseNo(t_caseno);

          if (!tLLCaseInfoDB.deleteSQL())
          {
            // @@错误处理
            this.mErrors.copyAllErrors(tLLRgtAffixDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLRegisterDeleteBL";
            tError.functionName = "DeltetData";
            tError.errorMessage = "删除分案疾病明细信息时出险失败！！！";
            this.mErrors .addOneError(tError) ;
            System.out.println(tError);
            conn.rollback();
            conn.close();
            return false;
          }

          System.out.println("开始执行删除分案收据明细表的程序");
          tLLCaseReceiptDB = new LLCaseReceiptDB(conn);
          tLLCaseReceiptDB.setCaseNo(t_caseno);

          if (!tLLCaseReceiptDB.deleteSQL())
          {
            // @@错误处理
            this.mErrors.copyAllErrors(tLLRgtAffixDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLRegisterDeleteBL";
            tError.functionName = "DeltetData";
            tError.errorMessage = "删除分案收据明细信息时出现失败！！！";
            this.mErrors .addOneError(tError) ;
            System.out.println(tError);
            conn.rollback();
            conn.close();
            return false;
          }
        }

        conn.commit();
        conn.close();
      }//end of try deletesql;
      catch (Exception ex)
      {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "RegisterDeleteBLS";
        tError.functionName = "DeleteData";
        tError.errorMessage = ex.toString();
        this.mErrors .addOneError(tError);
        try
        {
          conn.rollback() ;
          conn.close();
        }//end of try
        catch(Exception e)
        {
        }//end of catch;
        return false;
      }
      return true;
    }

    System.out.println("Start RegisterDelete BLS Submit...");
    System.out.println("End RegisterDelete BLS Submit...");
    mInputData=null;
    return true;
  }//end of submit;
}//end of RegisterDeleteBLS