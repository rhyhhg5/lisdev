package com.sinosoft.lis.llcase;


import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class ClaimUWQueryBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  /**  */
  private LLClaimSchema  mLLClaimSchema = new LLClaimSchema();
  private LLClaimSet mLLClaimSet = new LLClaimSet();
  private LLClaimBLSet mLLClaimBLSet = new LLClaimBLSet();
  private LLClaimPolicySet mLLClaimPolicySet = new LLClaimPolicySet();
  private LLClaimDetailBL mLLClaimDetailBL=new LLClaimDetailBL();
  private LLClaimDetailBLSet mLLClaimDetailBLSet=new LLClaimDetailBLSet();
  private GlobalInput mG = new GlobalInput();
  public ClaimUWQueryBL() {}

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;

    //数据查询业务处理
    if (cOperate.equals("QUERY||MAIN"))
    {
      if(!queryData())
	return false;
      System.out.println("---queryData---");
    }
    // 数据操作业务处理
    if (cOperate.equals("INSERT"))
    {
      if (!dealData())
	return false;
      System.out.println("---dealData---");
    }
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealData()
  {
		//准备给后台的数据
    prepareOutputData();
    //数据提交
    ClaimBLS tClaimBLS = new ClaimBLS();
    System.out.println("Start LLClaim BL Submit...");
    if (!tClaimBLS.submitData(mInputData,mOperate))
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tClaimBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {

    // 暂交费查询条件
    if (mOperate.equals("QUERY||MAIN"))
    {
      mLLClaimSchema.setSchema((LLClaimSchema)cInputData.getObjectByObjectName("LLClaimSchema",0));
      mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    }
    if (mOperate.equals("INSERT")||mOperate.equals("UPDATE"))
    {
      mLLClaimBLSet.set((LLClaimSet)cInputData.getObjectByObjectName("LLClaimSet",0));
      mLLClaimDetailBLSet.set((LLClaimDetailSet)cInputData.getObjectByObjectName("LLClaimDetailSet",0));
      mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    }

    return true;

  }

  /**
   * 查询符合条件的暂交费信息
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean queryData()
  {
    LLClaimDB tLLClaimDB = new LLClaimDB();
    SQLString tSQLString = new SQLString("LLClaim");
    tSQLString.setWherePart(mLLClaimSchema);
    String tWherepart = tSQLString.getWherePart();
    String tSQLAdd = "";
    if (tWherepart.length()==0)
    {
      tSQLAdd = " where MngCom like '"+mG.ManageCom.trim()+"%'";
    }
    else
    {
      tSQLAdd = tWherepart+"and MngCom like '"+mG.ManageCom.trim()+"%'";
    }
    String tSQL = "select * from LLClaim "+ tSQLAdd.trim();
    System.out.println(tSQL);
    mLLClaimSet.set(tLLClaimDB.executeQuery(tSQL));
    if (tLLClaimDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLClaimDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "LLClaimBL";
      tError.functionName = "queryData";
      tError.errorMessage = "赔案查询失败!";
      this.mErrors.addOneError(tError);
      mLLClaimSet.clear();
      return false;
    }
    if (mLLClaimSet.size() == 0)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LLClaimBL";
      tError.functionName = "queryData";
      tError.errorMessage = "未找到相关数据!";
      this.mErrors.addOneError(tError);
      mLLClaimBLSet.clear();
      return false;
    }

    mResult.clear();
    mResult.add( mLLClaimSet );
    System.out.println("xxx==="+mLLClaimSet.get(1).getRgtNo() );

    LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
    LLClaimPolicySchema tLLClaimPolicySchema = new LLClaimPolicySchema();
    tLLClaimPolicySchema.setClmNo(mLLClaimSchema.getClmNo());
    tLLClaimPolicySchema.setRgtNo(mLLClaimSchema.getRgtNo());
    String ttSQL = "select * from LLClaimPolicy "+ tSQLAdd.trim();
    System.out.println(ttSQL);
    mLLClaimPolicySet.set(tLLClaimPolicyDB.executeQuery(ttSQL));
    if (tLLClaimPolicyDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLClaimPolicyDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimBL";
      tError.functionName = "queryData";
      tError.errorMessage = "赔案保单明细查询失败!";
      this.mErrors.addOneError(tError);
      mLLClaimPolicySet.clear();

      return false;
    }


    System.out.println("policy.size=="+mLLClaimPolicySet.size() );
    mResult.add( mLLClaimPolicySet );


	return true;

  }

  /**
   * 校验传入的暂交费收据号是否合法
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkData()
  {

	boolean flag = true;
	return flag;

  }

  /**
   * 准备需要保存的数据
   */
  private void prepareOutputData()
  {
    LLClaimSchema tLLClaimSchema = mLLClaimBLSet.get(1);
    String strLimit=PubFun.getNoLimit("001");
    tLLClaimSchema.setClmNo(PubFun1.CreateMaxNo("CLMNO",strLimit));
    tLLClaimSchema.setMngCom(mG.ManageCom);
    tLLClaimSchema.setOperator(mG.Operator);
    tLLClaimSchema.setMakeDate(PubFun.getCurrentDate());
    tLLClaimSchema.setMakeTime(PubFun.getCurrentTime());
    tLLClaimSchema.setModifyDate(PubFun.getCurrentDate());
    tLLClaimSchema.setModifyTime(PubFun.getCurrentTime());
    mLLClaimBLSet.set(1,tLLClaimSchema);

    int n = mLLClaimDetailBLSet.size();
    for (int i = 1; i <= n; i++)
    {
      LLClaimDetailSchema tLLClaimDetailSchema = mLLClaimDetailBLSet.get(i);
      tLLClaimDetailSchema.setClmNo(tLLClaimSchema.getClmNo());
      tLLClaimDetailSchema.setGetDutyCode("code"+i);
      tLLClaimDetailSchema.setGetDutyKind("kind");
      tLLClaimDetailSchema.setMngCom(mG.ManageCom);
      tLLClaimDetailSchema.setOperator(mG.Operator);
      tLLClaimDetailSchema.setMakeDate(PubFun.getCurrentDate());
      tLLClaimDetailSchema.setMakeTime(PubFun.getCurrentTime());
      tLLClaimDetailSchema.setModifyDate(PubFun.getCurrentDate());
      tLLClaimDetailSchema.setModifyTime(PubFun.getCurrentTime());
      mLLClaimDetailBLSet.set(i,tLLClaimDetailSchema);
    }


    mInputData.clear();
    mInputData.add(mLLClaimBLSet);
    mInputData.add(mLLClaimDetailBLSet);
    mResult.clear();
    mResult.add(mLLClaimBLSet);
    mResult.add(mLLClaimDetailBLSet);

  }

}