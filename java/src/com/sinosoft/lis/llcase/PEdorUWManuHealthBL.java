package com.sinosoft.lis.llcase;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 保全人工核保</p>
 * <p>Description: 体检通知书录入</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */
public class PEdorUWManuHealthBL
{
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private LPPENoticeSchema mLPPENoticeSchema = null;

    private LPPENoticeItemSet mLPPENoticeItemSet = null;

    private String mEdorNo = null;

    private String mContNo = null;

    private String mProposalContNo = null;

    private String mAppntNo = null;

    private String mAppntName = null;

    private String mInsuredNo = null;

    private String mInsuredName = null;

    private String mPrtNo = null;

    private String mPrtSeq = null;

    private String mManageCom = null;

    private String mAgentCode = null;

    private String mAgentName = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!getInputData(data))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回体检通知书号
     * @return String
     */
    public String getPrtSeq()
    {
        return mPrtSeq;
    }

    /**
     * 得到传入参数
     * @param data VData
     */
    private boolean getInputData(VData data)
    {
        try
        {
            mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                    "GlobalInput", 0);
            mLPPENoticeSchema = (LPPENoticeSchema) data.
                    getObjectByObjectName("LPPENoticeSchema", 0);
            mLPPENoticeItemSet = (LPPENoticeItemSet) data.
                    getObjectByObjectName("LPPENoticeItemSet", 0);
            mEdorNo = mLPPENoticeSchema.getEdorNo();
            mContNo = mLPPENoticeSchema.getContNo();
            LCContSchema tLCContSchema = CommonBL.getLCCont(mContNo);
            mPrtNo = tLCContSchema.getPrtNo();
            mProposalContNo = tLCContSchema.getProposalContNo();
            mAppntNo = tLCContSchema.getAppntNo();
            mManageCom = tLCContSchema.getManageCom();
            mAgentCode = tLCContSchema.getAgentCode();
            mAppntName = CommonBL.getAppntName(mAppntNo);
            mInsuredNo = mLPPENoticeSchema.getCustomerNo();
            mInsuredName = CommonBL.getInsuredName(mInsuredNo);
        }
        catch (Exception ex)
        {
            mErrors.addOneError("传入参数错误！");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 处理数据
     * @return boolean
     */
    private boolean dealData()
    {
        mPrtSeq = createPrtSeq();
        setLPPNotice();
        setLPPENoticeItem();
        setPrtManager();
        changeEdorType();
        return true;
    }

    /**
     * 产生打印号
     * @return String
     */
    private String createPrtSeq()
    {
//        LPPENoticeDB tLPPENoticeDB = new LPPENoticeDB();
//        tLPPENoticeDB.setEdorNo(mEdorNo);
//        tLPPENoticeDB.setCustomerNo(mInsuredNo);
//        LPPENoticeSet tLPPENoticeSet = tLPPENoticeDB.query();
//        if (tLPPENoticeSet.size() != 0)
//        {
//            String prtSeq = tLPPENoticeSet.get(1).getPrtSeq();
//            if (prtSeq != null)
//            {
//                return prtSeq;
//            }
//        }
//        String sql = "select int(substr(max(PrtSeq), 13)) + 1 from loprtmanager " +
//                     "where PrtSeq like '" + mPrtNo + "2%'";
//        String maxNo = (new ExeSQL()).getOneValue(sql);
//        if ((maxNo == null) || (maxNo.equals("")))
//        {
//            maxNo = "01";
//        }
//        if (maxNo.length() == 1)
//        {
//            maxNo = "0" + maxNo;
//        }
//        System.out.println("PrtSeq:" + mPrtNo + "2" + maxNo);
        return PubFun1.CreateMaxNo("EDORPPAYNOTICENO", mPrtNo);
    }

    /**
     * 设置体检通知书主表
     */
    private void setLPPNotice()
    {
//        String sql = "delete from LPPENotice " +
//                "where EdorNo = '" + mEdorNo + "' " +
//                "and CustomerNo = '" + mInsuredNo + "' ";
//        mMap.put(sql, "DELETE");
        mLPPENoticeSchema.setEdorAcceptNo(mEdorNo);
        mLPPENoticeSchema.setEdorType("PP");
        mLPPENoticeSchema.setGrpContNo(BQ.GRPFILLDATA);
        mLPPENoticeSchema.setProposalContNo(mContNo);
        mLPPENoticeSchema.setAppName(mAppntName);
        mLPPENoticeSchema.setName(mInsuredName);
        mLPPENoticeSchema.setPrtSeq(mPrtSeq);
        mLPPENoticeSchema.setPEState(BQ.PESTATE_INPUT);
        mLPPENoticeSchema.setManageCom(mManageCom);
        mLPPENoticeSchema.setAgentCode(mAgentCode);
        mLPPENoticeSchema.setPrintFlag(BQ.PRINTFLAG_N); //未打印
        mLPPENoticeSchema.setOperator(mGlobalInput.Operator);
        mLPPENoticeSchema.setMakeDate(mCurrentDate);
        mLPPENoticeSchema.setMakeTime(mCurrentTime);
        mLPPENoticeSchema.setModifyDate(mCurrentDate);
        mLPPENoticeSchema.setModifyTime(mCurrentTime);
        mMap.put(mLPPENoticeSchema, "INSERT");
    }

    /**
     * 设置体检项目表
     */
    private void setLPPENoticeItem()
    {
//        String sql = "delete from LPPENoticeItem " +
//                "where EdorNo = '" + mEdorNo + "' " +
//                "and PrtSeq = '" + mPrtSeq + "' ";
//        mMap.put(sql, "DELETE");
        for (int i = 1; i <= mLPPENoticeItemSet.size(); i++)
        {
            LPPENoticeItemSchema tLPPENoticeItemSchema =
                    mLPPENoticeItemSet.get(i);
            tLPPENoticeItemSchema.setEdorAcceptNo(mEdorNo);
            tLPPENoticeItemSchema.setEdorNo(mEdorNo);
            tLPPENoticeItemSchema.setEdorType("PP");
            tLPPENoticeItemSchema.setGrpContNo(BQ.GRPFILLDATA);
            tLPPENoticeItemSchema.setContNo(mContNo);
            tLPPENoticeItemSchema.setProposalContNo(mContNo);
            tLPPENoticeItemSchema.setPrtSeq(mPrtSeq);
            tLPPENoticeItemSchema.setModifyDate(mCurrentDate);
            tLPPENoticeItemSchema.setModifyTime(mCurrentTime);
            mMap.put(tLPPENoticeItemSchema, "DELETE&INSERT");
        }
    }

    /**
     * 设置打印任务
     */
    private void setPrtManager()
    {
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setPrtSeq(mPrtSeq);
        tLOPRTManagerSchema.setOtherNo(mEdorNo);
        tLOPRTManagerSchema.setOtherNoType("03");
        tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_PEdorPE);
        tLOPRTManagerSchema.setManageCom(mManageCom);
        tLOPRTManagerSchema.setAgentCode(mAgentCode);
        tLOPRTManagerSchema.setReqCom(mGlobalInput.ManageCom);
        tLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
        tLOPRTManagerSchema.setPrtType("0");
        tLOPRTManagerSchema.setStateFlag(BQ.PRINTFLAG_N); //未打印
        tLOPRTManagerSchema.setMakeDate(mCurrentDate);
        tLOPRTManagerSchema.setMakeTime(mCurrentTime);
        tLOPRTManagerSchema.setPrintTimes(0);
        mMap.put(tLOPRTManagerSchema, "DELETE&INSERT");
    }

    /**
     * 改变理赔工单的状态:待处理-》待回复
     *
     */
    private void changeEdorType(){
    	System.out.println("PEdorUWManuHealthBL-->changeEdorType");
    	//需要通过理赔续保二核的工单号，查询此次对应的任务流号，从而状态改变
    	String tSql = "select MissionId 工作流任务号, SubMissionId 工作流子任务号 " 
    		+ " from LWMission where ProcessId = '0000000003' and ActivityId = '0000001181' " 
    		+ "and Missionprop12 ='"+mEdorNo+"' "
    		+ "order by Missionprop1 desc ";
    	SSRS tSSRS = new SSRS();
    	ExeSQL tExeSQL = new ExeSQL();
    	tSSRS = tExeSQL.execSQL(tSql);
    	String tMissionId = "";
    	String tSubMissionId = "";
    	String tActivityId = "0000001181";
    	String tActivityStatus = "2";
    	if(tSSRS != null && tSSRS.getMaxRow() > 0){
    		for(int i=1;i<=tSSRS.getMaxRow();i++){		
    			tMissionId = tSSRS.GetText(i, 1);
    			tSubMissionId = tSSRS.GetText(i, 2);
    		}
    		
    	}else{
    		mErrors.addOneError("PEdorUWManuHealthBL中查询工单的对应工作流号失败");
    	}
    	
    	LWMissionSchema tLWMissionSchema   = new LWMissionSchema();
        UWIndStateUI tUWIndStateUI = new UWIndStateUI();
        tLWMissionSchema.setMissionID(tMissionId);
        tLWMissionSchema.setSubMissionID(tSubMissionId);
        tLWMissionSchema.setActivityID(tActivityId);
        tLWMissionSchema.setActivityStatus(tActivityStatus);
        String tOperate = "UPDATE||MAIN";
        VData tVData = new VData();
		tVData.add(tLWMissionSchema);
		tVData.add(mGlobalInput);
		tUWIndStateUI.submitData(tVData,tOperate);
    }
    
    
    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
