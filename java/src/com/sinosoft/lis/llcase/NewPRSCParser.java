/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: 社保导入文件通用解析类，从一个XML数据流中解析出批次下分案信息</p>
 * <p>Copyright: Copyright (c) 2007 </p>
 * <p>Company: Sinosoft </p>
 * @author Xx
 * @version 6.0
 */

package com.sinosoft.lis.llcase;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import com.f1j.ss.BookModelImpl;
import com.f1j.util.F1Exception;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;

import org.apache.xpath.XPathAPI;
import org.apache.xpath.objects.XObject;
import org.w3c.dom.Node;

import com.sinosoft.lis.pubfun.diskimport.Sheet;
import com.sinosoft.lis.pubfun.diskimport.XlsImporter;

class NewPRSCParser implements SIParser {


    //案件
    private String mRgtNo = "";
    private String mGrpContNo = "";
    private String mPath = "";
    private String[] Title = null;
    private String vtsName = "ClaimErrList.vts";
    private ListTable FalseListTable = new ListTable();
    private GlobalInput mG = new GlobalInput();
    public CErrors mErrors = new CErrors();
    private LICaseTimeRemindSet tLICaseTimeRemindSet = new LICaseTimeRemindSet();
    LLValidityBL tLLValidityBL = new LLValidityBL();
    private PubFun pf =new PubFun();
    private PubFun1 pf1 = new PubFun1();
    

    
    /** xls文件名*/
    private String fileName = null;
    /** xml文件名*/
    private String configFileName = null;
    /** Sheet的名字 */
    private String[] mSheetNames = {"Sheet1"};
    /** 存放数据的容器 */
    private Vector mVector = new Vector();

    NewPRSCParser() {
    }
    
    /**
     * 为了不改动其抽象类，在此加入其构造器，配置传入的文件名及解析xml名
     * @param aFileName
     * @param aXmlName
     */
    NewPRSCParser(String aFileName,String aXmlName) {
    	this.fileName = aFileName;
        this.configFileName = aXmlName;
    }

    public boolean setGlobalInput(GlobalInput aG) {
        mG = aG;
        FalseListTable.setName("FAULT");
        return true;
    }

    /**
     * 解析案件结点(公用函数)
     * @param node Node
     * @return VData
     */
    public VData parseOneInsuredNode(Node node) {
        VData tReturn = new VData();
        System.out.println("<-Go Into NewPRSCParser.parseOneInsuredNode()->");
        System.out.println("fileName:"+fileName);
        System.out.println("configFileName:"+configFileName);
        String tStart = PubFun.getCurrentTime();
        if(!doImport()){
        	CError.buildErr(this,"磁盘导入失败，Excel文件解析错误！");
        }
        int tLength = mVector.size();
        int FailureCount = 0;
        
        for(int nIndex = 0; nIndex < tLength; nIndex++){
        	TransferData tTransferData = (TransferData)mVector.get(nIndex);      	
        	if (!genCase(tTransferData)) {
              System.out.println("导入这个人失败");
              FailureCount++;
              //写导出错误xml的函数
        	}
        }
        if(tLICaseTimeRemindSet.size()>0){
    		try{
    			VData tVData = new VData();
    			tVData.add(tLICaseTimeRemindSet);
    		  	tVData.add(mG);
    		  	tLLValidityBL.submitData(tVData,"INSERT||MAIN");
    		}catch(Exception ex) {
    			ex.printStackTrace();
    		}
            }
        String tEnd = PubFun.getCurrentTime();
        fromDateStringToLong(tStart,tEnd);

        /**
         * 根据xml生成title 不写死在程序里，另外需要生成动态数组
         */
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument(vtsName, "print");
        xmlexport.addListTable(FalseListTable, Title);
        xmlexport.outputDocumentToFile(mPath, mRgtNo); //输出xml文档到文件
        String FailureNum = FailureCount + "";
        TransferData tTransferData = new TransferData();
        System.out.println("错误清单容量：" + FalseListTable.size());
        tTransferData.setNameAndValue("FailureNum", FailureNum);
        tReturn.clear();
        tReturn.addElement(xmlexport);
        tReturn.add(tTransferData);
        return tReturn;
    }
    
    /**
     * 计算时间差,打印到控制台
     * @param inVal1 起始时间
     * 		  inVal2 结束时间
     * @return 
     * 
     */
    public void fromDateStringToLong(String inVal1,String inVal2) { 
    	//此方法计算时间毫秒
    	Date date1 = null; 
    	Date date2 = null;
    	//定义时间类型 
    	SimpleDateFormat inputFormat = new SimpleDateFormat("hh:mm:ss"); 
    	try { 
    		date1 = inputFormat.parse(inVal1); 
    		date2 = inputFormat.parse(inVal2); 
    	//将字符型转换成日期型
    	} catch (Exception e) {
    		e.printStackTrace(); 
    	} 
    	long ss=(date2.getTime()-date1.getTime())/1000; //共计秒数
        int MM = (int)ss/60; //共计分钟数
        int hh=(int)ss/3600; //共计小时数
        int dd=(int)hh/24; //共计天数
    	System.out.println("执行时间："+dd+"天"+(hh-dd*24)+"小时"+(MM-hh*60)+"分"+(ss-MM*60)+"秒"); 
    }
    
    /**
     * 执行磁盘导入
     * @return boolean
     */
    public boolean doImport() {
//    	fileName = "F:\\lisdev\\ui\\temp_lp\\8661.xls";
//    	configFileName = "F:\\lisdev\\ui\\temp_lp\\LL61CaseImport.xml";
    	System.out.println(" Into NewPRSCParser doImport");
        
		XlsImporter importer = new XlsImporter(fileName, configFileName,
                mSheetNames);
		
		importer.doImport();
        Sheet[] sheets = importer.getSheets();
        if (!dealSheets(sheets)) {
            return false;
        }
        
//        deleteFile();
        return true;
    }
    
    /**
     * 处理所有导入的sheet
     * @param sheets Sheet[]
     * @return boolean
     */
    protected boolean dealSheets(Sheet[] sheets) {
        for (int i = 0; i < sheets.length; i++) {
            if (!dealOneSheet(sheets[i], i)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * 处理一个sheet中全部的数据，包装为一个存放数据的List，用于后续逻辑处理
     * @param sheet
     * @param numberOfSheet
     * @return
     */
    protected boolean dealOneSheet(Sheet sheet,int numberOfSheet) {
   
    	int size = sheet.getRowSize();//Excel中数据量的多少（无版本号），读到空行结束
    	
    	for (int i = 0; i < size; i++) { 
//    		读sheet中的一行记录，TransferData
    		 TransferData td = new TransferData();
    		 int col = sheet.getColSize();
             for (int j = 0; j < col; j++) {
                 String head = sheet.getHead(j);
                 System.out.println("head:" + head);
                 if ((head == null) || (head.equals(""))) {
                     continue;
                 }
                 String value = sheet.getText(i, j, 1);
                 Object[] args = new Object[1];
                 args[0] = StrTool.cTrim(value);
                 td.setNameAndValue(head, value);
             }  
             mVector.add(td);
         }
    	
    	return true;
    }

    public boolean setParam(String aRgtNo, String aGrpContNo, String aPath) {
        mRgtNo = aRgtNo;
        mGrpContNo = aGrpContNo;
        mPath = aPath;
        return true;
    }

    public void setFormTitle(String[] cTitle, String cvtsName) {
        Title = cTitle;
        vtsName = cvtsName;
    }

    /**
     * 利用XPathAPI取得某个节点的节点值
     * @param node Node
     * @param strPath String
     * @return String
     */
    private static String parseNode(Node node, String strPath) {
        String strValue = "";
        try {
            XObject xobj = XPathAPI.eval(node, strPath);
            strValue = xobj == null ? "" : xobj.toString();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return strValue;
    }

    private boolean genCase(TransferData cTD) {
        //确认客户信息
        String aSecurityNo = "" + (String) cTD.getValueByName("SecurityNo");
        String aIDNo = "" + (String) cTD.getValueByName("IDNo");
        String aCustomerName = "" + (String) cTD.getValueByName("CustomerName");
        String aSex = "" + (String) cTD.getValueByName("Sex");
        String aCustomerNo = "";

        String strSQL0 =
                "select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype,idenddate,idstartdate ";
        String sqlpart1 = "from lcinsured a where ";
        String sqlpart2 = "from lbinsured a where ";
        String wherePart = "a.GrpContNo = '" + mGrpContNo + "' ";
        String msgPart = "";
        String ErrMessage = "";
        if (!"null".equals(aSecurityNo) && !"".equals(aSecurityNo)) {
            wherePart += " and a.othidno='" + aSecurityNo + "'";
            msgPart = "社保号为" + aSecurityNo;
        } else if (!"null".equals(aIDNo) && !"".equals(aIDNo)) {
            wherePart += " and a.idno='" + aIDNo + "'";
            msgPart = "身份证号为" + aIDNo;
        } else {
            ErrMessage = "未提供客户号，社保号或身份证号等信息，无法确认客户身份！";
            getFalseList(cTD, ErrMessage);
            return false;
        }

        String strSQL = strSQL0 + sqlpart1 + wherePart + " union " + strSQL0 +
                        sqlpart2 + wherePart;
        ExeSQL texesql = new ExeSQL();
        SSRS tssrs = new SSRS();
        tssrs = texesql.execSQL(strSQL);
        System.out.println(strSQL);
        if (tssrs == null || tssrs.getMaxRow() <= 0) {
            ErrMessage = "团单" + mGrpContNo + "下没有" + msgPart + "的被保险人";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        aCustomerNo = tssrs.GetText(1, 1);
        aIDNo = tssrs.GetText(1, 5);
        aSecurityNo = tssrs.GetText(1, 6);

        if ("".equals(aCustomerName) || "null".equals(aCustomerName)) {
            ErrMessage = "未提供客户姓名";
            getFalseList(cTD, ErrMessage);
            return false;
        } else if (!aCustomerName.equals(tssrs.GetText(1, 2))) {
            ErrMessage = msgPart + "的客户投保时姓名为" + tssrs.GetText(1, 2)
                         + ",与导入姓名" + aCustomerName + "不符！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        //----------------------------------------------------------------------------	
        String  atIdEndDate="";
		atIdEndDate= tssrs.GetText(1, 9);
		System.out.println(atIdEndDate+"-----");
		LICaseTimeRemindSchema tLICaseTimeRemindSchema = null;
//----------------------------------------------------------------------------		
		//Date datea=new Date();
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
		//Date dateb;
			try {
				//dateb = sdf.parse(atIdEndDate);
				//if(dateb.before(datea)){
					String date1 = pf.getCurrentDate();
					int no = pf.calInterval(date1,atIdEndDate,"D");
					if(no<0){
					tLICaseTimeRemindSchema = new LICaseTimeRemindSchema();
					String serialno = "LS"+pf.getCurrentDate2()+""+ pf1.CreateMaxNo("LSH_TimeRemind", 8);
				    System.out.println("serialno:"+serialno);
					tLICaseTimeRemindSchema.setSerialNo(serialno);
					tLICaseTimeRemindSchema.setinsuredno(aCustomerNo);
					tLICaseTimeRemindSchema.setinsuredname(aCustomerName);
					tLICaseTimeRemindSchema.setsex(aSex);
					tLICaseTimeRemindSchema.setbirthday(tssrs.GetText(1, 4));
					tLICaseTimeRemindSchema.setidtype(tssrs.GetText(1, 8));
					tLICaseTimeRemindSchema.setidno(aIDNo);
					tLICaseTimeRemindSchema.setIDStartDate(tssrs.GetText(1, 10));
					tLICaseTimeRemindSchema.setIDEndDate(atIdEndDate);
					tLICaseTimeRemindSchema.setReminddate(pf.getCurrentDate());
					tLICaseTimeRemindSchema.setRemindTime(pf.getCurrentTime());
					tLICaseTimeRemindSchema.setManageCom(mG.ManageCom);
					tLICaseTimeRemindSchema.setOperator(mG.Operator);
					tLICaseTimeRemindSchema.setMakeDate(pf.getCurrentDate());
					tLICaseTimeRemindSchema.setMakeTime(pf.getCurrentTime());
					tLICaseTimeRemindSchema.setmodifydate(pf.getCurrentDate());
					tLICaseTimeRemindSchema.setmodifytime(pf.getCurrentTime());
					
					tLICaseTimeRemindSet.add(tLICaseTimeRemindSchema);
					
					ErrMessage = "被保险人"+ aCustomerName +"，"+"客户号"+aCustomerNo+"，" +"证件已过期，请联系客户变更证件有效期。";
		            getFalseList(cTD, ErrMessage);
		            return false; 
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//----------------------------------------------------------------------------	

        String tSex = tssrs.GetText(1, 3).equals("null") ? "" :
                      tssrs.GetText(1, 3);
        if ("".equals(aSex) || "null".equals(aSex)) {
            ErrMessage = "未提供客户性别";
            getFalseList(cTD, ErrMessage);
            return false;
        } else if (!tSex.equals("") && !aSex.equals(tSex)) {
            ErrMessage = msgPart + "的客户投保时性别为" + tSex
                         + ",与本次导入性别" + aSex + "不符！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        
        
    	
        //3501 保单登记平台，证件类型为空时，默认修改为 4-其他 star
        String tIdType=tssrs.GetText(1, 8);
        if("null".equals(tIdType) ||tIdType==null||tIdType==""){
        	tIdType="4";
        }
//        String tIdtype=tssrs.GetText(1, 8).equals("null") ? "" :
//            tssrs.GetText(1, 8);;//获得证件类型
            /*   if(tIdtype.equals("0"))//是身份证时，对身份证号进行校验
        {
	        String tId=tssrs.GetText(1, 5).equals("null") ? "" :
	            tssrs.GetText(1, 5);;//获得身份证号
	        if(!PubFun.checkIDNo(tId))//校验数据库里存储的身份证号，by zzh 20100925
	        {	
	        	ErrMessage =msgPart+"的客户身份证号错误，请先做客户资料变更！";
	        	getFalseList(cTD, ErrMessage);
	        	return false;
	        }
        }*/
            

        String tBirthday = tssrs.GetText(1, 4).equals("null") ? "" :
                           tssrs.GetText(1, 4);

        String result = LLCaseCommon.checkPerson(aCustomerNo);
        if (!"".equals(result)) {
            ErrMessage = msgPart + "的客户正在进行保全操作,工单号为" + result + "！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        String strCustomerSQL = "select * from lpedorapp a,lpgrpedoritem b where a.edoracceptno in (select max(edorno) from lpdiskimport where  insuredno='" +
                                aCustomerNo + "' and grpcontno ='" + mGrpContNo + "') and a.edorstate !='0' and a.edoracceptno = b.edoracceptno and b.edortype ='ZT'";
        SSRS customertssrs = new SSRS();
        customertssrs = texesql.execSQL(strCustomerSQL);
        if (customertssrs.getMaxRow() > 0) {
            ErrMessage = msgPart + "的客户正在进行保全减人操作！";
            getFalseList(cTD, ErrMessage);
            return false;
        }

        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if (!tLLRegisterDB.getInfo()) {
            ErrMessage = "批次查询失败";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        tLLRegisterSchema = tLLRegisterDB.getSchema();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        VData tVData = new VData();
        LLCaseExtSchema tLLCaseExtSchema = new LLCaseExtSchema();
        PRCaseInfoBL tgenCaseInfoBL = new PRCaseInfoBL();

        tLLCaseSchema.setRgtType("1");

        String aReasonCode = "" + (String) cTD.getValueByName("ReasonCode");
        LLAppClaimReasonSchema tLLAppClaimReasonSchema = new
                LLAppClaimReasonSchema();
        if (!"".equals(aReasonCode) && !"null".equals(aReasonCode)) {
            tLLAppClaimReasonSchema.setRgtNo(mRgtNo);
            tLLAppClaimReasonSchema.setCaseNo("");
            tLLAppClaimReasonSchema.setReasonCode(aReasonCode);
            tLLAppClaimReasonSchema.setCustomerNo(aCustomerNo);
            tLLAppClaimReasonSchema.setReasonType("0");
        }

        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setRgtNo(mRgtNo);
        tLLCaseDB.setCustomerNo(aCustomerNo);
        LLCaseSet temLLCaseSet = tLLCaseDB.query();
        if (temLLCaseSet.size() > 0) {
            tLLCaseSchema.setUWState("7");
        }
        
//      modify by zhangyige 2012-07-09
        tLLCaseSchema.setPrePaidFlag(tLLRegisterSchema.getPrePaidFlag());
        
        tLLCaseSchema.setCaseNo("");
        tLLCaseSchema.setRgtNo(mRgtNo);
        tLLCaseSchema.setCustomerName(aCustomerName);
        tLLCaseSchema.setCustomerNo(aCustomerNo);
        tLLCaseSchema.setIDType(tIdType);
        tLLCaseSchema.setIDNo(aIDNo);
        tLLCaseSchema.setOtherIDType("5");
        tLLCaseSchema.setOtherIDNo(aSecurityNo);
        tLLCaseSchema.setCustBirthday(tBirthday);
        tLLCaseSchema.setCustomerSex(aSex);
        //3501 保单登记平台，证件类型为空时，默认修改为 4-其他 end

        
        String aAccDate = "" + FormatDate((String) cTD.getValueByName("AccDate"));
        if (aAccDate == null || "".equals(aAccDate) || "null".equals(aAccDate)) {
            ErrMessage = msgPart + "的客户出险日期不能为空，或出险日期格式不正确应为'YYYY-MM-DD'！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        String aDeathDate = "" +
                            FormatDate((String) cTD.getValueByName("DeathDate"));
        tLLCaseSchema.setAccidentDate(aAccDate);
        if(tLLCaseSchema.getAccidentDate()==null)//by zzh 0930
        {
        	ErrMessage = msgPart + "的客户出险日期格式不正确,应为'YYYY-MM-DD'！";
            getFalseList(cTD, ErrMessage);
            return false;
        }
        if(!LLCaseCommon.llcheckdate(tLLCaseSchema.getAccidentDate())){
        	ErrMessage = "请核实发生日期录入是否正确";
        	getFalseList(cTD, ErrMessage);
            return false;
        }
        tLLCaseSchema.setDeathDate(aDeathDate);
        System.out.println(tLLCaseSchema.getAccidentDate());
        tLLCaseSchema.setRgtState("13"); //案件状态
        tLLCaseSchema.setSurveyFlag("0");
        tLLCaseSchema.setAccdentDesc("批量受理");
        tLLCaseSchema.setSurveyFlag("0");
        tLLCaseSchema.setReceiptFlag("0");
        tLLCaseSchema.setCaseGetMode(tLLRegisterSchema.getCaseGetMode());
        if(tLLCaseSchema.getDeathDate()!=null&&tLLCaseSchema.getDeathDate()!=""){
        	if(!LLCaseCommon.llcheckdate(tLLCaseSchema.getDeathDate())){
            	ErrMessage = "请核实死亡日期录入是否正确";
            	getFalseList(cTD, ErrMessage);
                return false;
            }
        }
        String sql =
                "select bankcode,bankaccno,accname from lcinsured where insuredno='" +
                aCustomerNo + "' and grpcontno='" + mGrpContNo + "'" +
                " union " +
                "select bankcode,bankaccno,accname from lbinsured where insuredno='" +
                aCustomerNo + "' and grpcontno='" + mGrpContNo + "' with ur";

        ExeSQL texesql1 = new ExeSQL();
        SSRS trr = texesql1.execSQL(sql);
        if (trr.getMaxRow() > 0) {
            try {
                tLLCaseSchema.setBankCode(trr.GetText(1, 1));
                tLLCaseSchema.setBankAccNo(trr.GetText(1, 2));
                tLLCaseSchema.setAccName(trr.GetText(1, 3));
            } catch (Exception ex) {
                System.out.println("插入用户帐户信息失败！");
            }
        }


        LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
        tLLSubReportSchema.setCustomerNo(aCustomerNo);
        tLLSubReportSchema.setAccDate(aAccDate);
        tLLSubReportSchema.setCustomerName(aCustomerName);
        String aAccidentType = "" + (String) cTD.getValueByName("AccType");
        //#4205 start
        if(aAccidentType==null || "".equals(aAccidentType) ||"null".equals(aAccidentType) || (!"1".equals(aAccidentType) && !"2".equals(aAccidentType))){       	
    		ErrMessage = "未录入正确的事件类型";
            getFalseList(cTD, ErrMessage);
            return false;         
        }   
        //#4205 end
        tLLSubReportSchema.setAccidentType(aAccidentType);
        String aAccDesc = "" + (String) cTD.getValueByName("DiseaseName");
        tLLSubReportSchema.setAccDesc(aAccDesc);
        String aAccPlace = "" + (String) cTD.getValueByName("HospitalName");
        tLLSubReportSchema.setAccPlace(aAccPlace);
//        String aHospStartDate = "" + FormatDate((String) cTD.
//                                                getValueByName("HospStartDate"));
        String aHospStartDate = "" + (String) cTD.
                getValueByName("HospStartDate");
        tLLSubReportSchema.setInHospitalDate(FormatDate(aHospStartDate));
//        String aHospEndDate = "" + FormatDate((String) cTD.
//                                              getValueByName("HospEndDate"));
        String aHospEndDate = "" + (String) cTD.
                getValueByName("HospEndDate");
        
        tLLSubReportSchema.setOutHospitalDate(FormatDate(aHospEndDate));
        // #3558 保单登记平台日期校验 star
//      String tHospStartDate = (String) cTD.getValueByName("HospStartDate");
//      String tHospEndDate = (String) cTD.getValueByName("HospEndDate") ;

//      	String BeforeDate = PubFun.getBeforeDate(tHospStartDate, tHospEndDate);
        aHospStartDate = tLLSubReportSchema.getInHospitalDate();
        aHospEndDate = tLLSubReportSchema.getOutHospitalDate();
      if(aHospStartDate!=null&&aHospEndDate!=null&&!"".equals(aHospStartDate)&&!"".equals(aHospEndDate)){	
      	FDate fd = new FDate();
      	Date HospStartDate = fd.getDate(FormatDate(aHospStartDate));
      	Date HospEndDate = fd.getDate(FormatDate(aHospEndDate));
      	if(HospStartDate!=null&&HospEndDate!=null&&!"".equals(HospStartDate)&&!"".equals(HospEndDate)){
	        	if(HospStartDate.after(HospEndDate)){
	        		ErrMessage =msgPart+"的客户入出院日期有误,请核实！" ;
	                getFalseList(cTD, ErrMessage);
	                return false;
	        	}
      	}else{
      		ErrMessage =msgPart+"的客户入出院日期有误,请核实！" ;
              getFalseList(cTD, ErrMessage);
              return false;
      	}
      
      }
  	if(aHospStartDate!=null&&aHospStartDate!=""){
    	if(!LLCaseCommon.llcheckdate(aHospStartDate)){
    		ErrMessage ="请核实入院日期录入是否正确" ;
            getFalseList(cTD, ErrMessage);
            return false;
    	}
  	}
  	if(aHospEndDate!=null&&aHospEndDate!=""){
    	if(!LLCaseCommon.llcheckdate(aHospEndDate)){
    		ErrMessage ="请核实出院日期录入是否正确" ;
            getFalseList(cTD, ErrMessage);
            return false;
    	}
  	}
  	
      // #3558 保单登记平台日期校验  end
  	
  	//#4049授权
  	String tAuthorization = ""+cTD.getValueByName("Authorization");
    String sqlAuth = "select authorization from ldperson where customerno ='"+aCustomerNo+"'";
    String authValue = new ExeSQL().getOneValue(sqlAuth);
        if("1".equals(authValue)){
        	tLLCaseExtSchema.setAuthorization(authValue);
         }
        else{ 
        	if(tAuthorization==null || "".equals(tAuthorization) || "1".equals(tAuthorization) ||"null".equals(tAuthorization)){
        		 tLLCaseExtSchema.setAuthorization("1".equals(tAuthorization)?tAuthorization:"");
        	}else {
        		ErrMessage ="模板授权字段信息不符合标准" ;
                getFalseList(cTD, ErrMessage);
                return false;
        	}
    	   
         }

      //领款人和被保人关系
    	 
        String tostrSQL=" select llr.TogetherFlag,llr.GetMode,(select codename from ldcode where 1=1 and codetype='llgetmode' and code=llr.GetMode ) from LLRegister llr where  rgtno like 'P%' and  rgtno = '" + mRgtNo + "'";
         SSRS ssr = new ExeSQL().execSQL(tostrSQL);
         if (ssr.getMaxRow() > 0) {	
    	
        	 	String TogetherFlag=ssr.GetText(1, 1);
        	 	String GetMode=ssr.GetText(1, 2);
        	 	String GetModeName=ssr.GetText(1, 3);
    			if(  "1".equals(TogetherFlag) || "2".equals(TogetherFlag) ){//个人给付
    				 //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
    				String RelaDrawerInsured = ""+cTD.getValueByName("RelaDrawerInsured");
    				String DrawerIDType = ""+cTD.getValueByName("DrawerIDType");
    				String DrawerID = ""+cTD.getValueByName("DrawerID");
    				
    				if("1".equals(GetMode) ||  "2".equals(GetMode)){// 为现金
    					
    					tLLCaseExtSchema.setDrawerIDType("");
        				tLLCaseExtSchema.setDrawerID("");
        				
        				
    				
    				}else{
        				
        				//5.6.3.二.4
        				if("28".equals(RelaDrawerInsured)){ 
        					System.out.println("领款人和被保人关系为雇主，领款人证件号和证件类型可以为空");
        					
        				}else if("00".equals(RelaDrawerInsured)){
        					if(  "null".equals(DrawerIDType)||DrawerIDType==null || "".equals(DrawerIDType)|| "0".equals(DrawerIDType)){
        					}else{
        						ErrMessage ="领款人身份证信息与被保人证件信息不符，请核实";
            					getFalseList(cTD, ErrMessage);
            	    			return false;
        					}
        					if(  "null".equals(DrawerID)||DrawerID==null || "".equals(DrawerID)|| aIDNo.equals(DrawerID)){
        					}else{
        						ErrMessage ="领款人身份证信息与被保人证件信息不符，请核实";
            					getFalseList(cTD, ErrMessage);
            	    			return false;
        					}
        				}
        				else
        				{
        					if("null".equals(RelaDrawerInsured)||"".equals(RelaDrawerInsured)||RelaDrawerInsured==null){
        						ErrMessage ="赔款领取方式为("+GetMode+"-"+GetModeName+")时，领款人和被保人关系不能为空" ;
        				        getFalseList(cTD, ErrMessage);
        				        return false;
        					} 
        					if( "null".equals(DrawerIDType)||"".equals(DrawerIDType)||DrawerIDType==null){
        						ErrMessage ="赔款领取方式为("+GetMode+"-"+GetModeName+")时，领款人证件类型不能为空" ;
        				        getFalseList(cTD, ErrMessage);
        				        return false;		
        					}
        					if( "null".equals(DrawerID)||"".equals(DrawerID)||DrawerID==null){
        						ErrMessage ="赔款领取方式为("+GetMode+"-"+GetModeName+")时，领款人证件号码不能为空" ;
        				        getFalseList(cTD, ErrMessage);
        				        return false;
        					}
        					
        					
        				}
        				
        				
        				//5.6.3.二.5
        				
        				
        				//5.6.3.二.6
        				if(!"28".equals(RelaDrawerInsured)){
        					if( "5".equals(DrawerIDType)|| "0".equals(DrawerIDType)){
        						
        						if("".equals(PubFun.CheckIDNo( DrawerIDType, DrawerID, "", ""))){
        						}else{
        							ErrMessage ="领款人证件号码不符合规则，请核实";
                					getFalseList(cTD, ErrMessage);
                	    			return false;
        						}
        						 
        					}
        					 
        				}
        				tLLCaseExtSchema.setDrawerIDType("null".equals(DrawerIDType)|| null==DrawerIDType  ? "" : DrawerIDType);
        				tLLCaseExtSchema.setDrawerID("null".equals(DrawerID)|| null==DrawerID  ? "" : DrawerID);
          			    //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
        			
        			}
    				
    				tLLCaseExtSchema.setRelaDrawerInsured("null".equals(RelaDrawerInsured)|| null==RelaDrawerInsured  ? "" : RelaDrawerInsured);
         }
        }
        
        
        
         

        System.out.println("<--NewPRSCParser Star Submit VData-->");
        tVData.add(tLLAppClaimReasonSchema);
        tVData.add(tLLCaseSchema);
        if(tLLCaseExtSchema!=null){
        	tVData.add(tLLCaseExtSchema);
        }
        
        tVData.add(tLLRegisterSchema);
        tVData.add(tLLSubReportSchema);
        tVData.add(mG);
        System.out.println("<--NewPRSCParser Into tSimpleClaimUI-->" + mG.ManageCom);
        if (!tgenCaseInfoBL.submitData(tVData, "INSERT||MAIN")) {
            CErrors tError = tgenCaseInfoBL.mErrors;
            ErrMessage = tError.getFirstError();
            getFalseList(cTD, ErrMessage);
            return false;
        }
        return true;
    }

    private boolean getFalseList(TransferData cTD, String ErrMessage) {
        int n = Title.length;
        String[] strCol = new String[n + 1];
        for (int i = 0; i < n; i++) {
            strCol[i] = (String) cTD.getValueByIndex(i);
        }
        strCol[n] = ErrMessage;
        System.out.println("ErrMessage:" + ErrMessage);
        FalseListTable.add(strCol);
        return true;
    }

    private String FormatDate(String aDate) {
        String rDate = "";
        rDate += aDate;
        if (rDate.equals("") || rDate.equals("null")) {
            return "";
        }
        if (aDate.length() < 8) {
            int days = 0;
            try {
                days = Integer.parseInt(aDate) - 2;
            } catch (Exception ex) {
                return "";
            }
            rDate = PubFun.calDate("1900-1-1", days, "D", null);
        }
        return rDate;
    }
     
}
