/*
 * <p>ClassName: LJAGetInsertBL </p>
 * <p>Description: LJAGetInsertBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2005-02-26 15:27:43
 */
package com.sinosoft.lis.llcase;

import java.text.DecimalFormat;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LLHospGetDealPayBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mResult = new VData();

	private VData mInputData = new VData();

	private MMap map = new MMap();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	/** 数据操作字符串 */
	private String mBackMsg = "";

	private String mPrePaidFlag = "";

	private String tHCNo = "";

	/** 业务处理相关变量 */
	private LLHospGetSchema mLLHospGetSchema = new LLHospGetSchema();

	private LJAGetSchema mLJAGetSchema = new LJAGetSchema();

	private LJAGetSchema saveLJAGetSchema = new LJAGetSchema();

	private LJAGetSet saveLJAGetSet = new LJAGetSet();

	private LJAGetClaimSet mLJAGetClaimSet = new LJAGetClaimSet();

	private LLCaseSchema mLLCaseSchema = new LLCaseSchema();

	private LJSGetSchema mLJSGetSchema = new LJSGetSchema();

	private LLPrepaidGrpPolSet mLLPrepaidGrpPolSet = new LLPrepaidGrpPolSet();

	private LLPrepaidGrpContSet mLLPrepaidGrpContSet = new LLPrepaidGrpContSet();

	private LLPrepaidTraceSet mLLPrepaidTraceSet = new LLPrepaidTraceSet();

	private LLHospCaseSet tLLHospCaseSet = new LLHospCaseSet();

	private String mCurrentData = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();
	
	private double zPayMoney = 0;

	public LLHospGetDealPayBL() {
	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据
	 * @cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 取出操作符
		System.out.println("begin submit");

		// 取出页面传入数据
		if (!getInputData(cInputData,cOperate))
			return false;
		if (!checkInputData())
			return false;

		// 进行业务处理
		if (!dealData()) {
			return false;
		}

		System.out.println("after dealdata");
		// 数据提交
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, "")) {
			return false;
		}

		System.out.println("after tPubSubmit");
		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData, String cOperate) {
		System.out.println("getinputData......");
		LLHospGetSchema tLLHospGetSchema = new LLHospGetSchema();
		tLLHospGetSchema = (LLHospGetSchema) cInputData
				.getObjectByObjectName("LLHospGetSchema", 0);
		this.mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		mPrePaidFlag = cOperate;  //回销预付赔款
		tHCNo = tLLHospGetSchema.getHCNo();
		LLHospCaseDB tLLHospCaseDB = new LLHospCaseDB();
		tLLHospCaseDB.setHCNo(tLLHospGetSchema.getHCNo());
		tLLHospCaseSet = tLLHospCaseDB.query();
		if (tLLHospCaseSet.size() < 1) {
			CError.buildErr("dealData", "批次下没有可结算案件");
			return false;
		}
		LLHospGetDB tLLHospGetDB = new LLHospGetDB();
		tLLHospGetDB.setHCNo(tHCNo);
		tLLHospGetDB.getInfo();
		this.mLLHospGetSchema = tLLHospGetDB.getSchema();
		
		this.mLLHospGetSchema.setPayMode(tLLHospGetSchema.getPayMode());
		this.mLLHospGetSchema.setBankCode(tLLHospGetSchema.getBankCode());
		this.mLLHospGetSchema.setBankAccNo(tLLHospGetSchema.getBankAccNo());
		this.mLLHospGetSchema.setAccName(tLLHospGetSchema.getAccName());
		this.mLLHospGetSchema.setDrawer(tLLHospGetSchema.getDrawer());
		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		System.out.println("dealData......");
		MMap tmpMap = new MMap();
		/** 取得不包括受益人的应付信息 */
		if (!getPay()) {
			return false;
		}
		if (!dealCaseInfo()) {
			return false;
		}

		// 生成实付号码
		String sNoLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
		String ActuGetNo = PubFun1.CreateMaxNo("ACTUGETNO", sNoLimit);
		// 生成一条ljaget数据
		String tsql = "select sum(sumgetmoney) from LJsGet where otherno in (select caseno from LLHospCase where HCNo = '"
				+ tHCNo + "')";
		ExeSQL texesql = new ExeSQL();
		System.out.println(tsql);
		String tsumgetmoney = texesql.getOneValue(tsql);

		String tCaseNo = tLLHospCaseSet.get(1).getCaseNo();
		LJSGetDB tLJSGetDB = new LJSGetDB();
		tLJSGetDB.setOtherNo(tCaseNo);
		mLJSGetSchema = tLJSGetDB.query().get(1).getSchema();

		Reflections rf = new Reflections();
		rf.transFields(saveLJAGetSchema, mLJSGetSchema);
		saveLJAGetSchema.setActuGetNo(ActuGetNo);
		saveLJAGetSchema.setOtherNo(tHCNo);
		saveLJAGetSchema.setSumGetMoney(tsumgetmoney);
		saveLJAGetSchema.setDrawer(mLLHospGetSchema.getDrawer());
		saveLJAGetSchema.setPayMode(mLLHospGetSchema.getPayMode());
		saveLJAGetSchema.setAccName(mLLHospGetSchema.getAccName());
		saveLJAGetSchema.setBankAccNo(mLLHospGetSchema.getBankAccNo());
		saveLJAGetSchema.setBankCode(mLLHospGetSchema.getBankCode());
		saveLJAGetSchema.setOperator(mGlobalInput.Operator);
		saveLJAGetSchema.setManageCom(mLLHospGetSchema.getManageCom());
		saveLJAGetSchema.setMakeDate(mCurrentData);
		saveLJAGetSchema.setMakeTime(mCurrentTime);
		saveLJAGetSchema.setModifyDate(saveLJAGetSchema.getMakeDate());
		saveLJAGetSchema.setShouldDate(mCurrentData);
		saveLJAGetSchema.setModifyTime(saveLJAGetSchema.getMakeTime());

		for (int i = 1; i <= tLLHospCaseSet.size(); i++) {
			String mCaseNo = tLLHospCaseSet.get(i).getCaseNo();

			LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB();
			tLJSGetClaimDB.setOtherNo(mCaseNo);
			LJSGetClaimSchema mLJSGetClaimSchema = new LJSGetClaimSchema();
			mLJSGetClaimSchema = tLJSGetClaimDB.query().get(1).getSchema();

			LJAGetClaimSchema tLJAGetClaimSchema = new LJAGetClaimSchema();
			rf.transFields(tLJAGetClaimSchema, mLJSGetClaimSchema);
			tLJAGetClaimSchema.setActuGetNo(ActuGetNo);
			tLJAGetClaimSchema.setOPConfirmDate(mCurrentData);
			tLJAGetClaimSchema.setOPConfirmCode(mGlobalInput.Operator);
			tLJAGetClaimSchema.setOPConfirmTime(mCurrentTime);
			tLJAGetClaimSchema.setOperator(mGlobalInput.Operator);
			// tLJAGetClaimSchema.setManageCom(mLLCaseSchema.getMngCom());
			tLJAGetClaimSchema.setMakeDate(mCurrentData);
			tLJAGetClaimSchema.setMakeTime(mCurrentTime);
			tLJAGetClaimSchema.setModifyDate(tLJAGetClaimSchema.getMakeDate());
			tLJAGetClaimSchema.setModifyTime(tLJAGetClaimSchema.getMakeTime());

			mLJAGetClaimSet.add(tLJAGetClaimSchema);

			// 更新lcget表中的领取现金（此分配不受受益人影响）
			tmpMap = LLCaseCommon.addSumMoney(mCaseNo, mGlobalInput.Operator);

			if (tmpMap == null || tmpMap.size() == 0) {
				for (int m = 1; m <= mLJAGetClaimSet.size(); m++) {
					LBRnewStateLogDB tLBRnewStateLogDB = new LBRnewStateLogDB();
					LBRnewStateLogSet tLBRnewStateLogSet = new LBRnewStateLogSet();
					if (mLJAGetClaimSet.get(m).getContNo() != null) {
						tLBRnewStateLogDB.setNewContNo(mLJAGetClaimSet.get(m)
								.getContNo());
						tLBRnewStateLogSet = tLBRnewStateLogDB.query();
						if (tLBRnewStateLogSet.size() > 0) {
							CError.buildErr(this,
									"保单在理赔过程中做了续保回退,请将案件回退到检录，重新选择保单进行理算！");
							return false;
						}
					}
				}
				CError.buildErr(this, "给付责任查询失败！");
				return false;

			}
			// 处理账户金额，需要加条件判断（此分支不受受益人分配影响）
			String delSql5 = "delete from LCInsureAccTrace where othertype ='5' "
					+ " and otherno='" + mCaseNo + "'";
			map.put(delSql5, "DELETE");
			
			if (!dealInsureAcc(mCaseNo)) {
				CError.buildErr(this, "账户金额处理失败！");
				return false;
			}		
			map.add(tmpMap);
		}
		if (!dealPrapaid()) {
			CError.buildErr(this, "预付赔款处理失败！");
			return false;
		}

		saveLJAGetSet.add(saveLJAGetSchema);

		String delSql1 = "delete from LJAGetclaim where othernotype='5' and otherno in (select caseno from LLHospCase where HCNo = '"
				+ tHCNo + "') and feeoperationtype<>'FC'";// 处理财务反冲
		String delSql2 = "delete from LJAGet where othernotype='5' and otherno in (select caseno from LLHospCase where HCNo = '"
				+ tHCNo + "') and (finstate<>'FC' or finstate is null)";// 处理财务反冲
		map.put(delSql1, "DELETE");
		map.put(delSql2, "DELETE");

		// 把应付表插入实付表
		map.put(saveLJAGetSet, "INSERT");
		map.put(mLJAGetClaimSet, "INSERT");
		// 删除应付表
		String delSql3 = "delete from LJSGetclaim where othernotype in ('5','B') "
				+ " and otherno in (select caseno from LLHospCase where HCNo = '"
				+ tHCNo + "')";
		String delSql4 = "delete from LJSGet where othernotype in ('5','B') "
				+ " and otherno in (select caseno from LLHospCase where HCNo = '"
				+ tHCNo + "')";
		map.put(delSql3, "DELETE");
		map.put(delSql4, "DELETE");
		map.add(tmpMap);

		try {
			mInputData.clear();
			mInputData.add(map);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ICaseCureBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错:" + ex.toString();
			mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	// 查询保单赔付相关数据
	public boolean getPay() {
		System.out.println("Get pay from LJSGetClaim......");
		// 查询赔付应付表
		for (int i = 1; i <= tLLHospCaseSet.size(); i++) {
			String tCaseNo = tLLHospCaseSet.get(i).getCaseNo();

			LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB();
			tLJSGetClaimDB.setOtherNo(tCaseNo);
			tLJSGetClaimDB.setOtherNoType("5"); // 理赔号
			LJSGetClaimSet mLJSGetClaimSet = new LJSGetClaimSet();
			mLJSGetClaimSet.clear();
			mLJSGetClaimSet.set(tLJSGetClaimDB.query());
			if (tLJSGetClaimDB.mErrors.needDealError() == true) {
				// @@错误处理
				CError.buildErr(this, "赔付应付表LJSGetClaim查询失败!");
				return false;
			}
			if (mLJSGetClaimSet == null || mLJSGetClaimSet.size() == 0) {
				CError.buildErr(this, "赔付应付表LJSGetClaim无数据!");
				return false;
			}

			String tMngCom = "";
			for (int j = 1; j <= mLJSGetClaimSet.size(); j++) {
				if (mLJSGetClaimSet.get(j).getPay() == 0) {
					continue;
				}
				if (tMngCom.equals("")) {
					tMngCom = "" + mLJSGetClaimSet.get(j).getManageCom();
				}
				if (!tMngCom.equals("" + mLJSGetClaimSet.get(j).getManageCom())) {
					CError.buildErr(this, "一个案件下不能进行多机构保单赔付，请分案件处理！");
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 处理案件状态及案件的给付信息
	 * 
	 * @return boolean
	 */
	private boolean dealCaseInfo() {
		LLCaseSet tLLCaseSet = new LLCaseSet();
		LLRegisterSet tLLRegisterSet = new LLRegisterSet();
		for (int i = 1; i <= tLLHospCaseSet.size(); i++) {
			String tCaseNo = tLLHospCaseSet.get(i).getCaseNo();
			LLCaseDB tLLCaseDB = new LLCaseDB();
			tLLCaseDB.setCaseNo(tCaseNo);
			if (!tLLCaseDB.getInfo()) {
				CError.buildErr(this, "理赔案件信息查询失败！");
				return false;
			}

			mLLCaseSchema = tLLCaseDB.getSchema();
			// 更新是否回销预付赔款标记
			mLLCaseSchema.setPrePaidFlag(mPrePaidFlag);
			// 更新登记表中的银行账户字段
			mLLCaseSchema.setCaseGetMode(mLJAGetSchema.getPayMode());
			mLLCaseSchema.setAccName(mLJAGetSchema.getAccName());
			mLLCaseSchema.setBankAccNo(mLJAGetSchema.getBankAccNo());
			mLLCaseSchema.setBankCode(mLJAGetSchema.getBankCode());
			// 更新案件的的状态为“通知状态”
			mLLCaseSchema.setRgtState("11");
			mLLCaseSchema.setModifyDate(mCurrentData);
			mLLCaseSchema.setModifyTime(mCurrentTime);
			tLLCaseSet.add(mLLCaseSchema);
			// 立案信息处理
			LLRegisterDB tLLRegisterDB = new LLRegisterDB();
			tLLRegisterDB.setRgtNo(mLLCaseSchema.getRgtNo());
			if (!tLLRegisterDB.getInfo()) {
				CError.buildErr(this, "立案信息查询失败！");
				return false;
			}
			LLRegisterSchema mLLRegisterSchema = tLLRegisterDB.getSchema();

			// 个人申请则将给付方式及银行账户信息与付费表同步
			mLLRegisterSchema.setCaseGetMode(mLLHospGetSchema.getPayMode());
			mLLRegisterSchema.setAccName(mLLHospGetSchema.getAccName());
			mLLRegisterSchema.setBankAccNo(mLLHospGetSchema.getBankAccNo());
			mLLRegisterSchema.setBankCode(mLLHospGetSchema.getBankCode());
			mLLRegisterSchema.setModifyDate(mCurrentData);
			mLLRegisterSchema.setModifyTime(mCurrentTime);
			tLLRegisterSet.add(mLLRegisterSchema);

		}
		String tsql = "update LLClaim set ClmState='3' where caseno in (select caseno from LLHospCase where HCNo = '"
				+ tHCNo + "')";
		map.put(tsql, "UPDATE");
		map.put(tLLRegisterSet, "UPDATE");
		map.put(tLLCaseSet, "UPDATE");
		return true;
	}

	/**
	 * 帐户处理
	 * 
	 * @return boolean
	 */
	private boolean dealInsureAcc(String mCaseNo) {
		String tsql = "select distinct A.grppolno,A.polno,A.grpcontno,a.contno,a.riskcode "
				+ " from llclaimdetail a,lMrisk b,lmdutyget c "
				+ " where a.riskcode = b.riskcode and a.getdutycode=c.getdutycode and a.realpay != 0 "
				+ " and b.insuaccflag='Y' and c.needacc='1' AND CASENO ='"
				+ mCaseNo + "'";
		ExeSQL texesql = new ExeSQL();
		System.out.println(tsql);
		SSRS tss = texesql.execSQL(tsql);
		if (tss == null || tss.getMaxRow() <= 0) {
			// 没有帐户型险种产生的有效赔款
			return true;
		}
		for (int i = 1; i <= tss.getMaxRow(); i++) {
			// 判断是否为万能
			if (LLCaseCommon.chenkWN(tss.GetText(i, 5))) {
				continue;
			}
			// 遍历该赔案发生赔付的所有帐户型险种保单
			String tGrpPolNo = tss.GetText(i, 1);
			String tPolNo = tss.GetText(i, 2);
			double polpay = 0.0;

			LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
			tLLClaimDetailDB.setCaseNo(mCaseNo);
			LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
			tLLClaimDetailSet = tLLClaimDetailDB.query();
			if (tLLClaimDetailSet.size() <= 0) {
				CError.buildErr(this, "理算时发生错误，没有生成理赔明细，请回退重新理算！");
				return false;
			}

			for (int k = 1; k <= tLLClaimDetailSet.size(); k++) {
				if (tLLClaimDetailSet.get(k).getPolNo().equals(tPolNo)) {
					LMDutyGetDB tLMDutyGetDB = new LMDutyGetDB();
					tLMDutyGetDB.setGetDutyCode(tLLClaimDetailSet.get(k)
							.getGetDutyCode());
					if (!tLMDutyGetDB.getInfo()) {
						CError.buildErr(this, "给付责任查询失败！");
						return false;
					}
					if ("0".equals(tLMDutyGetDB.getNeedAcc())) {
						continue;
					}
					if (!tLLClaimDetailSet.get(k).getGiveTypeDesc().equals(
							"申诉拒付")) {
						LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB();
						tLJSGetClaimDB.setOtherNo(mCaseNo);
						tLJSGetClaimDB.setPolNo(tPolNo);
						tLJSGetClaimDB.setFeeFinaType("ZH");
						if (tLJSGetClaimDB.query().size() <= 0) {
							CError.buildErr(this, "特需财务账户信息生成失败，请重新进行理算!");
							return false;
						}
					}
					polpay += tLLClaimDetailSet.get(k).getRealPay();
				}
			}
			double remnant = polpay;
			double tPayMoney = 0;
			tsql = "select polno,insuaccno from lcinsureacctrace where otherno = '"
					+ mCaseNo
					+ "' and grppolno='"
					+ tGrpPolNo
					+ "' group by polno,insuaccno order by insuaccno desc";
			SSRS tSSRS = texesql.execSQL(tsql);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				CError.buildErr(this, "理算时发生错误，没有生成帐户轨迹，请回退重新理算！");
				return false;
			}
			for (int j = 1; j <= tSSRS.getMaxRow(); j++) {
				LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
				tLCInsureAccDB.setPolNo(tSSRS.GetText(j, 1));
				tLCInsureAccDB.setInsuAccNo(tSSRS.GetText(j, 2));
				if (!tLCInsureAccDB.getInfo()) {
					CError.buildErr(this, "查询不到轨迹对应账户信息！");
					return false;
				}
				double tInsuAccPay = 0;
				LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
				tLCInsureAccTraceDB.setOtherNo(mCaseNo);
				tLCInsureAccTraceDB.setPolNo(tSSRS.GetText(j, 1));
				tLCInsureAccTraceDB.setInsuAccNo(tSSRS.GetText(j, 2));
				LCInsureAccTraceSet tLCInsureAccTraceSet = tLCInsureAccTraceDB
						.query();
				if (tLCInsureAccTraceSet != null
						&& tLCInsureAccTraceSet.size() > 0) {
					for (int index = 1; index <= tLCInsureAccTraceSet.size(); index++) {
						tInsuAccPay += tLCInsureAccTraceSet.get(index)
								.getMoney();
					}
					if (tLCInsureAccDB.getSchema().getInsuAccBala()
							+ tInsuAccPay < 0) {
						CError.buildErr(this, "账户余额不足，无法给付！请回退重新理算");
						return false;
					}
					if (tInsuAccPay != 0) {
						tPayMoney += tInsuAccPay;
						zPayMoney += tInsuAccPay;
						LCInsureAccTraceSchema tLCInsureAccTraceSchema = tLCInsureAccTraceSet
								.get(1);
						tLCInsureAccTraceSchema.setMoney(tInsuAccPay);
						tLCInsureAccTraceSchema.setState("0");
						tLCInsureAccTraceSchema.setModifyDate(PubFun
								.getCurrentDate());
						tLCInsureAccTraceSchema.setModifyTime(PubFun
								.getCurrentTime());
						tLCInsureAccTraceSchema
								.setOperator(mGlobalInput.Operator);
						map.put(tLCInsureAccTraceSchema, "INSERT");
						LCInsureAccSchema tLCInsureAccSchema = tLCInsureAccDB
								.getSchema();
						tLCInsureAccSchema.setSumPaym(tLCInsureAccSchema
								.getSumPaym()
								- zPayMoney);
						tLCInsureAccSchema.setInsuAccBala(tLCInsureAccSchema
								.getInsuAccBala()
								+ zPayMoney);
						tLCInsureAccSchema.setOperator(mGlobalInput.Operator);
						tLCInsureAccSchema.setModifyDate(mCurrentData);
						tLCInsureAccSchema.setModifyTime(mCurrentTime);
						map.put(tLCInsureAccSchema, "UPDATE");
					}
				}
			}
			if (Arith.round(tPayMoney, 2) + Arith.round(remnant, 2) != 0) {
				CError.buildErr(this, "帐户轨迹与理算结果不符，请回退重新理算");
				return false;
			}
		}
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private boolean checkInputData() {
		return true;
	}

	public static void main(String[] args) {

		LJAGetInsertBL tLJAGetInsertBL = new LJAGetInsertBL();
		LLCaseSchema tLLCaseSchema = new LLCaseSchema();
		tLLCaseSchema.setCaseNo("C1100070114000110");
		VData tVData = new VData();
		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.ManageCom = "86";
		tGlobalInput.Operator = "001";
		tVData.addElement(tGlobalInput);
		LJAGetSchema tLJAGetSchema = new LJAGetSchema();
		tLJAGetSchema.setOtherNo("C1100070114000110");
		tLJAGetSchema.setOtherNoType("5");
		tLJAGetSchema.setAccName("");
		tLJAGetSchema.setBankAccNo("");
		tLJAGetSchema.setPayMode("1");
		tLJAGetSchema.setBankCode("");
		tLJAGetSchema.setDrawer("");
		tLJAGetSchema.setDrawerID("");
		tVData.addElement(tLJAGetSchema);
		tVData.addElement(tLLCaseSchema);
		tLJAGetInsertBL.submitData(tVData, "INSERT");
		VData vdata = tLJAGetInsertBL.getResult();

	}

	/**
	 * 扣除预付赔款
	 * 
	 * @return
	 */
	private boolean dealPrapaid() {
		System.out.print(mLJAGetClaimSet.size());
			LJAGetClaimSchema tLJAGetClaimSchema = mLJAGetClaimSet.get(1);
			System.out.println("是否回销预付赔款=====" + mPrePaidFlag);
			if (!"00000000000000000000"
					.equals(tLJAGetClaimSchema.getGrpContNo())
					&& "1".equals(mPrePaidFlag)) {

			LLPrepaidGrpContDB tLLPrepaidGrpContDB = new LLPrepaidGrpContDB();
			tLLPrepaidGrpContDB.setGrpContNo(tLJAGetClaimSchema.getGrpContNo());

			if (!tLLPrepaidGrpContDB.getInfo()) {
			}

			boolean tLockFlag = true;

			for (int j = 1; j <= mLLPrepaidGrpContSet.size(); j++) {
				LLPrepaidGrpContSchema tLLPrepaidGrpContSchema = mLLPrepaidGrpContSet
						.get(j);
				if (tLLPrepaidGrpContSchema.getGrpContNo().equals(
						tLJAGetClaimSchema.getGrpContNo())) {
					tLockFlag = false;
					break;
				}
			}

			if (tLockFlag) {
				if (Arith.round(tLLPrepaidGrpContDB.getApplyAmount()
						+ tLLPrepaidGrpContDB.getRegainAmount()
						+ tLLPrepaidGrpContDB.getSumPay(), 2) != tLLPrepaidGrpContDB
						.getPrepaidBala()) {
					CError.buildErr(this, "保单"
							+ tLJAGetClaimSchema.getGrpContNo() + "预付赔款余额错误");
					return false;
				}

				mLLPrepaidGrpContSet.add(tLLPrepaidGrpContDB.getSchema());
			}

			boolean tTraceFlag = true;

			for (int j = 1; j <= mLLPrepaidTraceSet.size(); j++) {
				LLPrepaidTraceSchema tLLPrepaidTraceSchema = mLLPrepaidTraceSet
						.get(j);
				if (tLLPrepaidTraceSchema.getGrpContNo().equals(
						tLJAGetClaimSchema.getGrpContNo())
						&& tLLPrepaidTraceSchema.getGrpPolNo().equals(
								tLJAGetClaimSchema.getGrpPolNo())) {
					tLLPrepaidTraceSchema.setMoney(Arith.round(
							tLLPrepaidTraceSchema.getMoney()
									- tLJAGetClaimSchema.getPay(), 2));
					tTraceFlag = false;
					break;
				}
			}

			if (tTraceFlag) {
				LLPrepaidGrpPolDB tLLPrepaidGrpPolDB = new LLPrepaidGrpPolDB();
				tLLPrepaidGrpPolDB
						.setGrpPolNo(tLJAGetClaimSchema.getGrpPolNo());
				if (!tLLPrepaidGrpPolDB.getInfo()) {
					CError.buildErr(this, "保单"
							+ tLJAGetClaimSchema.getGrpContNo() + "下险种"
							+ tLJAGetClaimSchema.getRiskCode() + "预付信息查询失败");
					return false;
				}

				if (Arith.round(tLLPrepaidGrpPolDB.getApplyAmount()
						+ tLLPrepaidGrpPolDB.getRegainAmount()
						+ tLLPrepaidGrpPolDB.getSumPay(), 2) != tLLPrepaidGrpPolDB
						.getPrepaidBala()) {
					CError.buildErr(this, "保单"
							+ tLJAGetClaimSchema.getGrpContNo() + "下险种"
							+ tLJAGetClaimSchema.getRiskCode() + "预付赔款余额错误");
					return false;
				}

				mLLPrepaidGrpPolSet.add(tLLPrepaidGrpPolDB.getSchema());
			for (int i = 1; i <= mLJAGetClaimSet.size(); i++) {
				LJAGetClaimSchema zLJAGetClaimSchema = mLJAGetClaimSet.get(i);
				
				LLPrepaidTraceSchema tLLPrepaidTraceSchema = new LLPrepaidTraceSchema();

				String tSerialNo = PubFun1.CreateMaxNo("PREPAIDTRACENO",
						zLJAGetClaimSchema.getManageCom());

				Reflections tReflections = new Reflections();
				tReflections.transFields(tLLPrepaidTraceSchema,
						zLJAGetClaimSchema);
				tLLPrepaidTraceSchema.setSerialNo(tSerialNo);
				tLLPrepaidTraceSchema.setOtherNoType("C");
				tLLPrepaidTraceSchema.setBalaDate(mCurrentData);
				tLLPrepaidTraceSchema.setBalaTime(mCurrentTime);
				tLLPrepaidTraceSchema.setMoneyType("PK");
				tLLPrepaidTraceSchema.setMoney(Arith.round(-zLJAGetClaimSchema
						.getPay(), 2));
				tLLPrepaidTraceSchema.setState("1");

				mLLPrepaidTraceSet.add(tLLPrepaidTraceSchema);
			}
		}

		for (int j = 1; j <= mLLPrepaidTraceSet.size(); j++) {
			LLPrepaidTraceSchema tLLPrepaidTraceSchema = mLLPrepaidTraceSet
					.get(j);
			for (int m = 1; m <= mLLPrepaidGrpPolSet.size(); m++) {
				LLPrepaidGrpPolSchema tLLPrepaidGrpPolSchema = mLLPrepaidGrpPolSet
						.get(m);
				if (tLLPrepaidGrpPolSchema.getGrpPolNo().equals(
						tLLPrepaidTraceSchema.getGrpPolNo())) {
					double tMoney = tLLPrepaidGrpPolSchema.getPrepaidBala() > -tLLPrepaidTraceSchema
							.getMoney() ? -tLLPrepaidTraceSchema.getMoney()
							: tLLPrepaidGrpPolSchema.getPrepaidBala();
					tLLPrepaidTraceSchema.setLastBala(tLLPrepaidGrpPolSchema
							.getPrepaidBala());
					tLLPrepaidGrpPolSchema.setPrepaidBala(Arith
							.round(tLLPrepaidGrpPolSchema.getPrepaidBala()
									- tMoney, 2));
					tLLPrepaidTraceSchema.setMoney(Arith.round(-tMoney, 2));
					tLLPrepaidTraceSchema.setAfterBala(tLLPrepaidGrpPolSchema
							.getPrepaidBala());
					tLLPrepaidGrpPolSchema.setSumPay(Arith.round(
							tLLPrepaidGrpPolSchema.getSumPay()
									+ tLLPrepaidTraceSchema.getMoney(), 2));
					tLLPrepaidGrpPolSchema.setBalaDate(tLLPrepaidTraceSchema
							.getBalaDate());
					tLLPrepaidGrpPolSchema.setBalaTime(tLLPrepaidTraceSchema
							.getBalaTime());
					tLLPrepaidGrpPolSchema.setOperator(tLLPrepaidTraceSchema
							.getOperator());
					tLLPrepaidGrpPolSchema.setModifyDate(tLLPrepaidTraceSchema
							.getModifyDate());
					tLLPrepaidGrpPolSchema.setModifyTime(tLLPrepaidTraceSchema
							.getModifyTime());
					break;
				}
			}

			for (int m = 1; m <= mLLPrepaidGrpContSet.size(); m++) {
				LLPrepaidGrpContSchema tLLPrepaidGrpContSchema = mLLPrepaidGrpContSet
						.get(m);
				if (tLLPrepaidGrpContSchema.getGrpContNo().equals(
						tLLPrepaidTraceSchema.getGrpContNo())) {
					tLLPrepaidGrpContSchema.setPrepaidBala(Arith.round(
							tLLPrepaidGrpContSchema.getPrepaidBala()
									+ tLLPrepaidTraceSchema.getMoney(), 2));
					tLLPrepaidGrpContSchema.setSumPay(Arith.round(
							tLLPrepaidGrpContSchema.getSumPay()
									+ tLLPrepaidTraceSchema.getMoney(), 2));
					tLLPrepaidGrpContSchema.setBalaDate(tLLPrepaidTraceSchema
							.getBalaDate());
					tLLPrepaidGrpContSchema.setBalaTime(tLLPrepaidTraceSchema
							.getBalaTime());
					tLLPrepaidGrpContSchema.setOperator(tLLPrepaidTraceSchema
							.getOperator());
					tLLPrepaidGrpContSchema.setModifyDate(tLLPrepaidTraceSchema
							.getModifyDate());
					tLLPrepaidGrpContSchema.setModifyTime(tLLPrepaidTraceSchema
							.getModifyTime());
					break;
				}
			}
		}

		// 扣除的预付赔款金额
		double tSumMoney = 0;

		for (int l = 1; l <= mLLPrepaidTraceSet.size(); l++) {
			LLPrepaidTraceSchema tLLPrepaidTraceSchema = mLLPrepaidTraceSet
					.get(l);
			if (tLLPrepaidTraceSchema.getMoney() != 0) {
				mBackMsg += "<br>保单"
						+ tLLPrepaidTraceSchema.getGrpContNo()
						+ "下险种"
						+ tLLPrepaidTraceSchema.getRiskCode()
						+ ":预付赔款余额"
						+ new DecimalFormat("0.00")
								.format(tLLPrepaidTraceSchema.getLastBala())
						+ ",本次回销预付赔款"
						+ new DecimalFormat("0.00")
								.format(-tLLPrepaidTraceSchema.getMoney());

				LJAGetClaimSchema aLJAGetClaimSchema = new LJAGetClaimSchema();
				aLJAGetClaimSchema
						.setActuGetNo(saveLJAGetSchema.getActuGetNo());
				aLJAGetClaimSchema.setFeeFinaType("YF");
				aLJAGetClaimSchema.setFeeOperationType("000");
				aLJAGetClaimSchema.setOtherNo(tLLPrepaidTraceSchema
						.getOtherNo());
				aLJAGetClaimSchema.setOtherNoType("Y");
				aLJAGetClaimSchema.setGetDutyKind("000");
				aLJAGetClaimSchema.setGrpContNo(tLLPrepaidTraceSchema
						.getGrpContNo());
				aLJAGetClaimSchema.setContNo("00000" + l);
				aLJAGetClaimSchema.setGrpPolNo(tLLPrepaidTraceSchema
						.getGrpPolNo());
				aLJAGetClaimSchema.setPolNo("00000" + l);

				LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
				tLMRiskAppDB.setRiskCode(tLLPrepaidTraceSchema.getRiskCode());

				if (!tLMRiskAppDB.getInfo()) {
					CError.buildErr(this, "险种信息查询失败");
					return false;
				}

				aLJAGetClaimSchema.setRiskCode(tLMRiskAppDB.getRiskCode());
				aLJAGetClaimSchema.setRiskVersion(tLMRiskAppDB.getRiskVer());
				aLJAGetClaimSchema.setKindCode(tLMRiskAppDB.getKindCode());

				LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
				tLCGrpPolDB.setGrpPolNo(tLLPrepaidTraceSchema.getGrpPolNo());
				if (!tLCGrpPolDB.getInfo()) {
					LBGrpPolDB tLBGrpPolDB = new LBGrpPolDB();
					tLBGrpPolDB
							.setGrpPolNo(tLLPrepaidTraceSchema.getGrpPolNo());
					if (!tLBGrpPolDB.getInfo()) {
						CError.buildErr(this, tLLPrepaidTraceSchema
								.getGrpContNo()
								+ "保单查询失败");
						return false;
					}
					aLJAGetClaimSchema.setSaleChnl(tLBGrpPolDB.getSaleChnl());
					aLJAGetClaimSchema.setAgentCode(tLBGrpPolDB.getAgentCode());
					aLJAGetClaimSchema.setAgentGroup(tLBGrpPolDB
							.getAgentGroup());
					aLJAGetClaimSchema.setAgentCom(tLBGrpPolDB.getAgentCom());
					aLJAGetClaimSchema.setManageCom(tLBGrpPolDB.getManageCom());
					aLJAGetClaimSchema.setAgentType(tLBGrpPolDB.getAgentType());

				} else {
					aLJAGetClaimSchema.setSaleChnl(tLCGrpPolDB.getSaleChnl());
					aLJAGetClaimSchema.setAgentCode(tLCGrpPolDB.getAgentCode());
					aLJAGetClaimSchema.setAgentGroup(tLCGrpPolDB
							.getAgentGroup());
					aLJAGetClaimSchema.setAgentCom(tLCGrpPolDB.getAgentCom());
					aLJAGetClaimSchema.setManageCom(tLCGrpPolDB.getManageCom());
					aLJAGetClaimSchema.setAgentType(tLCGrpPolDB.getAgentType());
				}

				aLJAGetClaimSchema.setPay(tLLPrepaidTraceSchema.getMoney());
				aLJAGetClaimSchema.setOPConfirmCode(mGlobalInput.Operator);
				aLJAGetClaimSchema.setOPConfirmDate(saveLJAGetSchema
						.getMakeDate());
				aLJAGetClaimSchema.setOPConfirmTime(saveLJAGetSchema
						.getMakeTime());
				aLJAGetClaimSchema.setOperator(mGlobalInput.Operator);
				aLJAGetClaimSchema.setMakeDate(saveLJAGetSchema.getMakeDate());
				aLJAGetClaimSchema.setMakeTime(saveLJAGetSchema.getMakeTime());
				aLJAGetClaimSchema
						.setModifyDate(saveLJAGetSchema.getMakeDate());
				aLJAGetClaimSchema
						.setModifyTime(saveLJAGetSchema.getMakeTime());

				tSumMoney = Arith.round(
						tSumMoney + aLJAGetClaimSchema.getPay(), 2);

				mLJAGetClaimSet.add(aLJAGetClaimSchema);
			}
		}

		if (tSumMoney != 0) {
			saveLJAGetSchema.setSumGetMoney(Arith.round(saveLJAGetSchema
					.getSumGetMoney()
					+ tSumMoney, 2));
			mBackMsg += "<br>案件实际给付"
					+ new DecimalFormat("0.00").format(saveLJAGetSchema
							.getSumGetMoney());
		}

		if (mLLPrepaidTraceSet.size() > 0) {
			map.put(mLLPrepaidTraceSet, "INSERT");
			map.put(mLLPrepaidGrpPolSet, "UPDATE");
			map.put(mLLPrepaidGrpContSet, "UPDATE");
		}

	
			}
			return true;
	}
	public String getBackMsg() {
		return mBackMsg;
	}
}
