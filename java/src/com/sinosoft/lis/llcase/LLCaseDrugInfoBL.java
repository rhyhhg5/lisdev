package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.LLCaseDrugInfoDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLCaseDrugInfoSchema;
import com.sinosoft.lis.vschema.LLCaseDrugInfoSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LLCaseDrugInfoBL {
	//错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private LLCaseDrugInfoSet mLLCaseDrugInfoSet;
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private MMap map = new MMap();
    
    private LLCaseDrugInfoSchema mLLCaseDrugInfoSchema = new LLCaseDrugInfoSchema();
    
    public LLCaseDrugInfoBL() {
    }

    public static void main(String[] args) {
    }
    
    public boolean submitData(VData cInputData, String cOperate){
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        //得到输入数据
        if (!getInputData()) {
            return false;
        }

        //进行业务处理
        if (!dealData(cOperate)) {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLCaseDrugInfoBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }
    
    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData() 
    {
    	mLLCaseDrugInfoSchema = (LLCaseDrugInfoSchema) mInputData.getObjectByObjectName(
                "LLCaseDrugInfoSchema", 0);
    	if (mLLCaseDrugInfoSchema == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "mLLCaseDrugInfoBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "在药品信息接受时没有得到足够的数据，请您确认有：药品完整信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    
    /**
     * 根据前面的输入数据，进行UI逻辑处理
      * 如果在处理过程中出错，则返回false,否则返回true
      */
     private boolean dealData(String cOperate)
     {
          boolean tReturn =false;
          
          if("UPDATE".equals(cOperate))
          {
        	  map.put(mLLCaseDrugInfoSchema, "DELETE&INSERT");
              tReturn=true;
              return tReturn ;
//        	  LLCaseDrugInfoDB tLLCaseDrugInfoDB = new LLCaseDrugInfoDB();
//              tLLCaseDrugInfoDB.setSerialNo(mLLCaseDrugInfoSchema.getSerialNo());
//              if(!tLLCaseDrugInfoDB.getInfo())
//              {
//            	  CError tError = new CError();
//                  tError.moduleName = "mLLCaseDrugInfoBL";
//                  tError.functionName = "dealData";
//                  tError.errorMessage = "在药品信息接受时没有得到足够的数据，请您确认有：药品完整信息!";
//                  this.mErrors.addOneError(tError);
//                  return false;
//              }
//              LLCaseDrugInfoSchema tLLCaseDrugInfoSchema = tLLCaseDrugInfoDB.getSchema();
//              tLLCaseDrugInfoSchema.setDrugCode(mLLCaseDrugInfoSchema.getDrugCode());
//          	  tLLCaseDrugInfoSchema.setDrugCategory(mLLCaseDrugInfoSchema.getDrugCategory());
//          	  tLLCaseDrugInfoSchema.setDrugGenericName(mLLCaseDrugInfoSchema.getDrugGenericName());
//          	  tLLCaseDrugInfoSchema.setDrugTradeName(mLLCaseDrugInfoSchema.getDrugTradeName());
//          	  tLLCaseDrugInfoSchema.setDrugEnglishName(mLLCaseDrugInfoSchema.getDrugEnglishName());
//          	  tLLCaseDrugInfoSchema.setDrugClassification(mLLCaseDrugInfoSchema.getDrugClassification());
//          	  tLLCaseDrugInfoSchema.setSecondCategory(mLLCaseDrugInfoSchema.getSecondCategory());
//          	  tLLCaseDrugInfoSchema.setFormulations(mLLCaseDrugInfoSchema.getFormulations());
//          	  tLLCaseDrugInfoSchema.setSpecifications(mLLCaseDrugInfoSchema.getSpecifications());
//          	  tLLCaseDrugInfoSchema.setPhonetic(mLLCaseDrugInfoSchema.getPhonetic());
//          	  tLLCaseDrugInfoSchema.setManufacturer(mLLCaseDrugInfoSchema.getManufacturer());
//          	  tLLCaseDrugInfoSchema.setPackageMessage(mLLCaseDrugInfoSchema.getPackageMessage());
//          	  tLLCaseDrugInfoSchema.setAdministration(mLLCaseDrugInfoSchema.getAdministration());
//          	  tLLCaseDrugInfoSchema.setHealthNo(mLLCaseDrugInfoSchema.getHealthNo());
//          	  tLLCaseDrugInfoSchema.setCostLevel(mLLCaseDrugInfoSchema.getCostLevel());
//          	  tLLCaseDrugInfoSchema.setPaymentScope(mLLCaseDrugInfoSchema.getPaymentScope());
//          	  tLLCaseDrugInfoSchema.setOutpatientLimit(mLLCaseDrugInfoSchema.getOutpatientLimit());
//          	  tLLCaseDrugInfoSchema.setHospitalLimit(mLLCaseDrugInfoSchema.getHospitalLimit());
//          	  tLLCaseDrugInfoSchema.setInjuriesMark(mLLCaseDrugInfoSchema.getInjuriesMark());
//          	  tLLCaseDrugInfoSchema.setFertilityMark(mLLCaseDrugInfoSchema.getFertilityMark());
//          	  tLLCaseDrugInfoSchema.setPriceCeiling(mLLCaseDrugInfoSchema.getPriceCeiling());
//          	  tLLCaseDrugInfoSchema.setRemark(mLLCaseDrugInfoSchema.getRemark());
//          	  tLLCaseDrugInfoSchema.setMngCom(mLLCaseDrugInfoSchema.getMngCom());
//          	  tLLCaseDrugInfoSchema.setAreaCode(mLLCaseDrugInfoSchema.getAreaCode());
//          	  tLLCaseDrugInfoSchema.setOperator(mLLCaseDrugInfoSchema.getOperator());
//          	  tLLCaseDrugInfoSchema.setModifyDate(PubFun.getCurrentDate());
//          	  tLLCaseDrugInfoSchema.setModifyTime(PubFun.getCurrentTime());
              
          }
          else
          {
        	  map.put(mLLCaseDrugInfoSchema, cOperate);
              tReturn=true;
              return tReturn ;
          }
    }
     
     /**
      * 准备往后层输出所需要的数据
      * 输出：如果准备数据时发生错误则返回false,否则返回true
      */
     private boolean prepareOutputData()
     {
        try
        {
          mInputData.clear();
          mInputData.add(map);
          mResult.add(mLLCaseDrugInfoSchema);
        }
        catch(Exception ex)
        {
          // @@错误处理
          CError tError =new CError();
          tError.moduleName="LLCaseDrugInfoUI";
          tError.functionName="prepareData";
          tError.errorMessage="在准备往后层处理所需要的数据时出错。";
          this.mErrors .addOneError(tError) ;
          return false;
        }
        return true;
    }
     
   //得到结果
     public VData getResult() {
         return this.mResult;
     }
}
