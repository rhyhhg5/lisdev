package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: ClientRegisterBL </p>
 * <p>Description: ClientRegisterBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author:Xx
 * @CreateDate：2005-02-18 13:58:55
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class BatchSecurityCalBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCaseSet mLLCaseSet = new LLCaseSet();

    public BatchSecurityCalBL() {
    }

    public static void main(String[] args) {
        VData tVData = new VData();
        BatchSecurityCalBL tBatchClaimCalBL = new BatchSecurityCalBL();
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        tLLRegisterSchema.setRgtNo("P9400061121000001");
        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86940000";
        mGlobalInput.ComCode = "86940000";
        mGlobalInput.Operator = "cm9402";

        tVData.add(mGlobalInput);
        tVData.add(tLLRegisterSchema);
        tBatchClaimCalBL.submitData(tVData, "cal");
        tVData.clear();
        tVData = tBatchClaimCalBL.getResult();
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BatchSecurityCalBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ClientRegisterBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("--Star GetInputData--");
        this.mLLRegisterSchema.setSchema((LLRegisterSchema) cInputData.
                                         getObjectByObjectName(
                "LLRegisterSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        MMap tmap = new MMap();
        VData tdata = new VData();

        System.out.println("---Star dealData---mOperate---" + mOperate);
        if (!checkData()) {
            return false;
        }
        for (int i = 1; i <= mLLCaseSet.size(); i++) {
            if (!calClaim(mLLCaseSet.get(i).getCaseNo())) {
                continue;
            }
        }

        tdata.add(tmap);
        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(tdata, null)) {
            CError.buildErr(this, "数据库保存失败");
            return false;
        }
        return true;
    }

    /**
     * 处理前检查数据是否合理
     * @return boolean
     */
    private boolean checkData() {
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        if (!tLLRegisterDB.getInfo()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SimpleClaimBL";
            tError.functionName = "checkData";
            tError.errorMessage = "团体立案信息查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
        String declineflag = "" + mLLRegisterSchema.getDeclineFlag();
        if (declineflag.equals("1")) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SimpleClaimBL";
            tError.functionName = "checkData";
            tError.errorMessage = "该团体申请已撤件，不能再录入个人客户!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if ("03".equals(mLLRegisterSchema.getRgtState())) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SimpleClaimBL";
            tError.functionName = "checkData";
            tError.errorMessage = "团体申请下所有个人案件已经结案完毕，" +
                                  "不能再录入个人客户!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if ("04".equals(mLLRegisterSchema.getRgtState())) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SimpleClaimBL";
            tError.functionName = "checkData";
            tError.errorMessage = "团体申请下所有个人案件已经给付确认，" +
                                  "不能再录入个人客户!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        tLLCaseDB.setRgtState("03");
        mLLCaseSet = tLLCaseDB.query();
        if (mLLCaseSet.size() <= 0) {
            CError tError = new CError();
            tError.moduleName = "SimpleClaimBL";
            tError.functionName = "checkData";
            tError.errorMessage = "没有待推算的案件!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 自动理算，生成赔案信息表。
     * @return boolean
     */
    private boolean calClaim(String aCaseNo) {
        LLSecurityCalBL aClaimCalAutoBL = new LLSecurityCalBL();
        LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
        tLLFeeMainDB.setCaseNo(aCaseNo);
        LLFeeMainSet tLLFeeMainSet = tLLFeeMainDB.query();
        for (int i = 1; i <= tLLFeeMainSet.size(); i++) {
            VData aVData = new VData();
            aVData.addElement(mGlobalInput);
            aVData.addElement(tLLFeeMainSet.get(i));
            if (!aClaimCalAutoBL.submitData(aVData, "batchCal")) {
                CError.buildErr(this, "案件"+aCaseNo+":"+aClaimCalAutoBL.mErrors.getFirstError());
                return false;
            }
        }
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLLRegisterSchema);
            mInputData.add(map);
            mResult.clear();
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDDrugBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
