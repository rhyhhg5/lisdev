package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class CaseCommonRightCheckBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData ;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /**用户登陆信息 */
//    private GlobalInput mGlobalInput = new GlobalInput();
//    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
//    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
//    private LLClaimUserSchema mLLClaimUserSchema = new LLClaimUserSchema();
//    private LLClaimOprerateRightSchema mLLClaimOprerateRightSchema = new LLClaimOprerateRightSchema();

//    private String mJspFileName;
//    private String mButtonName;
    private String mCaseNo;
    private String mRgtState;
    private String mOperator;
    private String mHandler;

    public CaseCommonRightCheckBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * @param: cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
      //将操作数据拷贝到本类中
      mInputData = (VData) cInputData.clone();
      mOperate = cOperate;

      //得到外部传入的数据,将数据备份到本类中
      if (!getInputData(cInputData))
      {
          return false;
      }

  System.out.println("after getInputData");

      //数据操作业务处理
      if (!dealData())
      {
          return false;
      }

  System.out.println("after dealData");

      return true;
    }

    private boolean getInputData(VData cInputData)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();

        tGlobalInput.setSchema(
              (GlobalInput)
              cInputData.getObjectByObjectName("GlobalInput",0));

        tLLCaseSchema.setSchema(
              (LLCaseSchema)
              cInputData.getObjectByObjectName("LLCaseSchema", 0));

        mCaseNo = tLLCaseSchema.getCaseNo();
        mOperator = tGlobalInput.Operator;

        return true;
    }

    private boolean getBaseData()
    {
        //查询案件状态
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mCaseNo);
        if (tLLCaseDB.getInfo() == false)
        {

            // @@错误处理
            mErrors.copyAllErrors(tLLCaseDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "CaseCommonRightCheckBL";
            tError.functionName = "getBaseData";
            tError.errorMessage = "案件状态查询失败!" +
                                  "理赔号：" + mCaseNo;
            mErrors.addOneError(tError);
            return false;
        }
        mHandler = tLLCaseDB.getHandler();
        mRgtState = tLLCaseDB.getRgtState();
        if ("05".equals(mRgtState)) //审批状态
        {
            LLClaimUWMainSet tLLClaimUWMainSet = new LLClaimUWMainSet();
            LLClaimUWMainDB tLLClaimUWMainDB = new LLClaimUWMainDB();
            tLLClaimUWMainDB.setCaseNo(mCaseNo);
            tLLClaimUWMainSet.set(tLLClaimUWMainDB.query());
            if (tLLClaimUWMainDB.mErrors.needDealError())
            {
                // @@错误处理
                mErrors.copyAllErrors(tLLClaimUWMainDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "CaseCommonRightCheckBL";
                tError.functionName = "getBaseData";
                tError.errorMessage = "案件审定者查询失败!" +
                                      "理赔号：" + mCaseNo;
                mErrors.addOneError(tError);
                return false;
            }
            mHandler = tLLClaimUWMainSet.get(1).getAppClmUWer();
        }
//        //查询用户核赔权限信息LLClaimUser
//        LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
//        tLLClaimUserDB.setUserCode(mOperator);
//        if (tLLClaimUserDB.getInfo() == false)
//        {
//            // @@错误处理
//            mErrors.copyAllErrors(tLLClaimUserDB.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "ClaimUnderwriteBL";
//            tError.functionName = "getBaseData";
//            tError.errorMessage = "用户核赔权限查询失败!" +
//                                  "用户代码：" + mOperator;
//            mErrors.addOneError(tError);
//            return false;
//        }
//        mLLClaimUserSchema.setSchema(tLLClaimUserDB.getSchema());
//
//        //查询按钮操作权限信息LLClaimOprerateRight
//        LLClaimOprerateRightDB tLLClaimOprerateRightDB = new LLClaimOprerateRightDB();
//        tLLClaimOprerateRightDB.setUserCode(mJspFileName);
//        tLLClaimOprerateRightDB.setUserCode(mButtonName);
//        if (tLLClaimUserDB.getInfo() == false)
//        {
//            // @@错误处理
//            mErrors.copyAllErrors(tLLClaimOprerateRightDB.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "ClaimUnderwriteBL";
//            tError.functionName = "getBaseData";
//            tError.errorMessage = "按钮操作权限查询失败!" +
//                                  "页面名称：" + mJspFileName +
//                                  "按钮名称：" + mButtonName;
//            mErrors.addOneError(tError);
//            return false;
//        }
//        mLLClaimOprerateRightSchema.setSchema(tLLClaimOprerateRightDB.getSchema());

        return true;
    }

    private boolean dealData()
    {
        if (mOperate.equals("CHECK|RIGHT"))
        {
            getBaseData();
            System.out.println("== mCaseNo ====" + mCaseNo);
            System.out.println("== mRgtState ==" + mRgtState);
            System.out.println("== mOperator ==" + mOperator);
            System.out.println("== mHandler ===" + mHandler);

            if (mHandler == null || mHandler.equals(""))
            {
                System.out.println("=========尚未指定处理人========");
                System.out.println("=========  mHandler =========" + mHandler);
                return true;
            }
            if (!mOperator.equals(mHandler))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "CaseCommonRightCheckBL";
                tError.functionName = "dealData";
                tError.errorMessage = "对不起，您没有权限处理该案件!" +
                                      "指定处理人：" + mHandler;
                mErrors.addOneError(tError);
                return false;
            }

        }

        return true;
    }

}
