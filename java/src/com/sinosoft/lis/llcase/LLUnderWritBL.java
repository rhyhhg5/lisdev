package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.task.CommonBL;


/**
 * <p>Title: LLUnderWritBL</p>
 * <p>Description: 理赔二核数据存储-工作流</p>
 * <p>Copyright: Copyright (c) 2013</p>
 * <p>Company: Sinosoft</p>
 * @author Houyd
 * @version 1.0
 */
public class LLUnderWritBL {
    public CErrors mErrors = new CErrors();
    
    /**数据提交的容器*/
    private VData mResult = new VData();
    
    /** 输入数据的容器及操作符 */
    private VData mInputData = new VData();
    private String mOperate;

    /**接收前台的数据*/
    private String mCaseNo = "";
    private String mInsuredNo = "";
    private String mInsuredName = "";
    private String mRemark = "";
    
    /**当前系统时间*/
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    
    /**其他数据参数*/
    private String mOperator = "";
    private String mManageCom = "";
    
    /**全局变量及数据存放容器*/
    private GlobalInput mG = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    private MMap tmpMap = new MMap();
    
    /**记录案件操作轨迹*/
    private LLCaseOpTimeSet mLLCaseOpTimeSet = new LLCaseOpTimeSet();
    
    
    public LLUnderWritBL() {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        System.out.println("LLUnderWritBL begin......");

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        
        //数据效验
        if (!checkData()) {
            return false;
        }

        //对数据进行处理并封装
        if (!dealData()) {
            return false;
        }

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(this.mResult, null)) {
            CError.buildErr(this, "数据保存失败");
            return false;
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("getInputData......");
       
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData",0);
        
        mOperator = mG.Operator;
        mManageCom = mG.ManageCom;

        if (mManageCom.length() == 2) {
            mManageCom = mManageCom + "000000";
        } else if (mManageCom.length() == 4) {
            mManageCom = mManageCom + "0000";
        }
        
        mCaseNo = (String)mTransferData.getValueByName("CaseNo");
        mInsuredNo = (String)mTransferData.getValueByName("InsuredNo");
        mInsuredName = (String)mTransferData.getValueByName("InsuredName");
        mRemark = (String)mTransferData.getValueByName("Remark");
              
        return true;
    }
    
    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData() {
        if (mOperate == null || "".equals(mOperate)) {
            CError.buildErr(this, "Operate为空");
            return false;
        }

        if (mCaseNo == null || "".equals(mCaseNo)) {
            CError.buildErr(this, "CaseNo为空");
            return false;
        }
        
        if (mInsuredNo == null || "".equals(mInsuredNo)) {
            CError.buildErr(this, "InsuredNo为空");
            return false;
        }
        
        if (mInsuredName == null || "".equals(mInsuredName)) {
            CError.buildErr(this, "InsuredName为空");
            return false;
        }
        
        LLHospCaseDB tLLHospCaseDB = new LLHospCaseDB();
        tLLHospCaseDB.setCaseNo(mCaseNo);
        if (tLLHospCaseDB.getInfo()) {
            CError.buildErr(this, "此案件为医保通案件，不能进行处理");
            return false;
        }
        
        return true;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("---dealData---");
        //查询案件信息
        String tCaseInfoSql = "select rgtstate from llcase where caseno='"+mCaseNo+"'";
        ExeSQL tCaseInfoExeSQL = new ExeSQL();
        String tRgtState = tCaseInfoExeSQL.getOneValue(tCaseInfoSql);
        
        //将信息存入LWMission中
        String tMissionID = PubFun1.CreateMaxNo("MissionId", 20);
        String tEdorNo = CommonBL.createWorkNo();
        LWMissionSchema tLWMissionSchema = new LWMissionSchema();
        tLWMissionSchema.setMissionID(tMissionID);//工作流任务号
        tLWMissionSchema.setSubMissionID("1");//工作流子任务号 
        tLWMissionSchema.setProcessID("0000000003");//核保过程id  
        tLWMissionSchema.setActivityID("0000001182");//核保活动id
        tLWMissionSchema.setActivityStatus("1");
        tLWMissionSchema.setMissionProp1(mCaseNo);//案件号
        tLWMissionSchema.setMissionProp2(mInsuredNo);//被保人客户号
        tLWMissionSchema.setMissionProp3(mInsuredName);//被保人
        tLWMissionSchema.setMissionProp4("");//受理人
        tLWMissionSchema.setMissionProp5("");//受理日期
        tLWMissionSchema.setMissionProp6(mCurrentDate);//送核日期 
        tLWMissionSchema.setMissionProp7(mCurrentTime);//送核时间 
        tLWMissionSchema.setMissionProp9("");//经办人
        tLWMissionSchema.setMissionProp10(tEdorNo);//二核工单号
        tLWMissionSchema.setMissionProp11("SP");//二核工单类型 理赔二核固定为：‘SP’
        tLWMissionSchema.setMissionProp12(mRemark);//理赔送核备注信息
        tLWMissionSchema.setMissionProp13(mOperator);//理赔送核提起人
        tLWMissionSchema.setMissionProp14(tRgtState);//理赔案件送核之前的状态
        
        tLWMissionSchema.setCreateOperator(mOperator);//工作流创建人
        tLWMissionSchema.setMakeDate(mCurrentDate);
        tLWMissionSchema.setMakeTime(mCurrentTime);
        tLWMissionSchema.setModifyDate(mCurrentDate);
        tLWMissionSchema.setModifyTime(mCurrentTime);
        
        
        tmpMap.put(tLWMissionSchema, "DELETE&INSERT");
        
        //数据流数据存储完毕后，修改llcase表状态等
        LLCaseDB tLLCaseDB = new LLCaseDB();
        LLCaseSet tLLCaseSet = new LLCaseSet();
        tLLCaseDB.setCaseNo(mCaseNo);
        tLLCaseSet = tLLCaseDB.query();
        for(int i=1;i<=tLLCaseSet.size();i++){
            tLLCaseSet.get(i).setRgtState("16");
            tLLCaseSet.get(i).setOperator(mOperator);
            tLLCaseSet.get(i).setModifyDate(mCurrentDate);
            tLLCaseSet.get(i).setModifyTime(mCurrentTime);      
        }
        tmpMap.put(tLLCaseSet, "UPDATE");
        
        //添加案件状态改变轨迹
        createLCOT("16", mG.Operator);
        
        if (mLLCaseOpTimeSet.size() > 0) {
            tmpMap.put(mLLCaseOpTimeSet, "DELETE&INSERT");
        }
        
        this.mResult.add(tmpMap);
        return true;
    }
    /**
     * 案件状态改变轨迹LLCaseOpTime
     * @param aRgtState
     * @param aOperator
     * @return
     */
    private boolean createLCOT(String aRgtState, String aOperator) {
        try {
            LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
            tLLCaseOpTimeSchema.setCaseNo(mCaseNo);
            tLLCaseOpTimeSchema.setRgtState(aRgtState);
            tLLCaseOpTimeSchema.setOperator(aOperator);
            tLLCaseOpTimeSchema.setManageCom(mG.ManageCom);
            LLCaseCommon tLLCaseCommon = new LLCaseCommon();
            String tSQL =
                    "SELECT MAX(sequance) FROM llcaseoptime WHERE caseno='" +
                    mCaseNo + "' AND rgtstate='" + aRgtState + "'";
            ExeSQL tExeSQL = new ExeSQL();
            String aSequance = "";
            if (tExeSQL.getOneValue(tSQL).equals("null") ||
                tExeSQL.getOneValue(tSQL).equals("")) {
                aSequance = "";
            } else {
                aSequance = Integer.toString(Integer.parseInt(tExeSQL.
                        getOneValue(tSQL)));
            }
            tLLCaseOpTimeSchema.setSequance(aSequance);
            tLLCaseOpTimeSchema.setPageNo("0");
            
            tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(tLLCaseOpTimeSchema);
            mLLCaseOpTimeSet.add(tLLCaseOpTimeSchema);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }
    
    /**
     * 二核结束后，改变LLCase表状态,记录二核持续时间
     * @param tCaseNo
     * @return
     */
    public void changeCaseState(String tCaseNo){
        MMap tMap = new MMap();
        VData tVData = new VData();
        PubSubmit tPS = new PubSubmit();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
            
        //工作流信息
        LWMissionDB tLWMissionDB = new LWMissionDB();
        LWMissionSet tLWMissionSet = new LWMissionSet();
        tLWMissionDB.setActivityID("0000001182");
        tLWMissionDB.setMissionProp1(tCaseNo);
        tLWMissionSet = tLWMissionDB.query();
        
        String tRgtState = tLWMissionSet.get(1).getMissionProp14();//案件二核前信息
        
        //案件信息
        LLCaseSet tLLCaseSet = new LLCaseSet();
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(tCaseNo);
        tLLCaseSet = tLLCaseDB.query();
        
        tLLCaseSchema = tLLCaseSet.get(1).getSchema();
        tLLCaseSchema.setRgtState(tRgtState);//将案件状态还原
        tLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
        tLLCaseSchema.setModifyTime(PubFun.getCurrentTime());   
        tMap.put(tLLCaseSchema, "UPDATE");
        
        //状态持续时间
        LLCaseOpTimeSet tLLCaseOpTimeSet = new LLCaseOpTimeSet();
        LLCaseOpTimeDB tLLCaseOpTimeDB = new LLCaseOpTimeDB();
        tLLCaseOpTimeDB.setCaseNo(tCaseNo);
        tLLCaseOpTimeDB.setRgtState("16");
        tLLCaseOpTimeSet = tLLCaseOpTimeDB.query();
        LLCaseOpTimeSchema mLLCaseOpTimeSchema = tLLCaseOpTimeSet.get(1).getSchema();
        LLCaseCommon tLLCaseCommon = new LLCaseCommon();
        
        //改变操作次数
        String tSQL =
            "SELECT MAX(sequance) FROM llcaseoptime WHERE caseno='" +
            tCaseNo + "' AND rgtstate='16'";
        ExeSQL tExeSQL = new ExeSQL();
        String aSequance = "";
        if (tExeSQL.getOneValue(tSQL).equals("null") ||
            tExeSQL.getOneValue(tSQL).equals("")) {
            aSequance = "";
        } else {
            aSequance = Integer.toString(Integer.parseInt(tExeSQL.
                    getOneValue(tSQL))-1);
        }
        mLLCaseOpTimeSchema.setSequance(aSequance);
        
        mLLCaseOpTimeSchema.setEndDate(PubFun.getCurrentDate());
        mLLCaseOpTimeSchema.setEndTime(PubFun.getCurrentTime());
        LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(mLLCaseOpTimeSchema);
        
        tMap.put(tLLCaseOpTimeSchema, "DELETE&INSERT");
        tVData.add(tMap);
        
        if (!tPS.submitData(tVData, null)) {
            CError.buildErr(this, "数据保存失败");
            return;
        }
        
    }
    
    public static void main(String[] args) {
    	 LLUnderWritBL tLLUnderWritBL = new LLUnderWritBL();
         tLLUnderWritBL.changeCaseState("C1100131206000001");
    }
}
