package com.sinosoft.lis.llcase;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p>Title: Web业务系统案件－报案保存功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author 刘岩松
 * @version 1.0
 */

public class ReportBLS
{
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();
  public ReportBLS() {}

  public static void main(String[] args)
  {
  }
  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
  	boolean tReturn = false;
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;
    System.out.println("Start Report BLS Submit...");

    if (!this.saveData())
      return false;
    System.out.println("End Report BLS Submit...");
    mInputData=null;
    return true;
  }

  private boolean saveData()
  {
    //接收数据
    LLReportSet mLLReportSet = (LLReportSet)mInputData.getObjectByObjectName("LLReportBLSet",0);
    LLSubReportSet mLLSubReportSet=(LLSubReportSet)mInputData.getObjectByObjectName("LLSubReportBLSet",0);
    LDSysTraceSet mLDSysTraceSet = new LDSysTraceSet();
    mLDSysTraceSet = (LDSysTraceSet)mInputData.getObjectByObjectName("LDSysTraceSet",0);

    Connection conn = DBConnPool.getConnection();

    if (conn==null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ReportBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    try
    {
      conn.setAutoCommit(false);
			LLReportDBSet mLLReportDBSet = new LLReportDBSet( conn );
			mLLReportDBSet.set(mLLReportSet);
			if (mLLReportDBSet.insert() == false)
			{
				// @@错误处理
			  	this.mErrors.copyAllErrors(mLLReportDBSet.mErrors);
				CError tError = new CError();
				tError.moduleName = "ReportBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "保存数据失败!";
				this.mErrors .addOneError(tError) ;
	       			conn.rollback() ;
	       			conn.close();
				return false;
			}

      LDSysTraceDBSet mLDSysTraceDBSet = new LDSysTraceDBSet(conn);
      mLDSysTraceDBSet.set(mLDSysTraceSet);
      int h;
      h = mLDSysTraceSet.size();
      for(int l=1;l<=h;l++)
      {
        LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
        tLDSysTraceSchema.setSchema(mLDSysTraceSet.get(l));
        System.out.println("先要删除的保单号码分别是"+tLDSysTraceSchema.getPolNo());
        LDSysTraceDB tLDSysTraceDB = new LDSysTraceDB(conn);
        tLDSysTraceDB.setPolNo(tLDSysTraceSchema.getPolNo());
        tLDSysTraceDB.deleteSQL();
      }
      if (mLDSysTraceDBSet.insert() == false)
      {
        // @@错误处理
        this.mErrors.copyAllErrors(mLDSysTraceDBSet.mErrors);
        CError tError = new CError();
        tError.moduleName = "LDTraceDBSet";
        tError.functionName = "saveData";
        tError.errorMessage = "保存数据失败!";
        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
        return false;
      }

      LLSubReportDBSet mLLSubReportDBSet=new LLSubReportDBSet(conn);
      mLLSubReportDBSet.set(mLLSubReportSet);
      if (mLLSubReportDBSet.insert() == false)
			{
        // @@错误处理
        this.mErrors.copyAllErrors(mLLSubReportDBSet.mErrors);
        CError tError = new CError();
        tError.moduleName = "ReportBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "保存数据失败!";
        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
        return false;
			}

			conn.commit() ;
			conn.close();
		} // end of try
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ReportBLS";
			tError.functionName = "submitData";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);
			try{conn.rollback() ;
			conn.close();
			} catch(Exception e){}
			return false;
		}
    		return true;
  }
  }
