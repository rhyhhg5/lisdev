package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.LLAppClaimReasonDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLAppClaimReasonSchema;
import com.sinosoft.lis.vschema.LLAppClaimReasonSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class LLGrpSocialConfigBL {
	
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private String mOperate;
    private LLAppClaimReasonSchema mLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
    private LLAppClaimReasonSet mLLAppClaimReasonSet = new LLAppClaimReasonSet();
    private LLAppClaimReasonSet mdelLLAppClaimReasonSet = new LLAppClaimReasonSet();
    private GlobalInput mG = new GlobalInput();
    private MMap map = new MMap();

    public LLGrpSocialConfigBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        if (!getInputData(cInputData, cOperate)) {
            return false;
        }

        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit pubSubmit = new PubSubmit();
        if (!pubSubmit.submitData(this.mInputData, "")) {
            this.mErrors.copyAllErrors(pubSubmit.mErrors);
            return false;
        }
        return true;

    }

    private boolean getInputData(VData cInputData, String cOperate) {
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        mLLAppClaimReasonSchema = (LLAppClaimReasonSchema) cInputData.getObjectByObjectName(
                "LLAppClaimReasonSchema", 0);
        mLLAppClaimReasonSet = (LLAppClaimReasonSet) cInputData.getObjectByObjectName(
                "LLAppClaimReasonSet", 0);
        
        if(mOperate!=null&&"insert".equals(mOperate))
        {
            if (mLLAppClaimReasonSchema == null || mInputData == null || mG == null) {
                CError tError = new CError();
                tError.moduleName = "LLGrpSocialConfigBL";
                tError.functionName = "submitData_getInputData";
                tError.errorMessage = "读取数据失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (mLLAppClaimReasonSchema.getRgtNo() == null || "".equals(mLLAppClaimReasonSchema.getRgtNo())) 
            {
                CError tError = new CError();
                tError.moduleName = "LLGrpSocialConfigBL";
                tError.functionName = "submitData_getInputData";
                tError.errorMessage = "请选择保单！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;

    }

    private boolean dealData() {
        if ("insert".equals(mOperate)) 
        {
        	
            mLLAppClaimReasonSchema.setMakeDate(PubFun.getCurrentDate());
            mLLAppClaimReasonSchema.setMakeTime(PubFun.getCurrentTime());
            mLLAppClaimReasonSchema.setModifyDate(PubFun.getCurrentDate());
            mLLAppClaimReasonSchema.setModifyTime(PubFun.getCurrentTime());
            mLLAppClaimReasonSchema.setOperator(mG.Operator);
            
            String tSQL = "select managecom from lcgrpcont where grpcontno='" 
                    + mLLAppClaimReasonSchema.getRgtNo() 
                    + "' union select managecom from lbgrpcont where grpcontno='" 
                    + mLLAppClaimReasonSchema.getRgtNo() + "'";
            
            ExeSQL tExeSQL = new ExeSQL();
            String comName = tExeSQL.getOneValue(tSQL);
            mLLAppClaimReasonSchema.setMngCom(comName);
            
            map.put(mLLAppClaimReasonSchema, "INSERT");
            return true;
        } else if ("del".equals(mOperate))
        {
           for(int i =1;i<=mLLAppClaimReasonSet.size();i++)
           {
        	   LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
        	   tLLAppClaimReasonSchema = mLLAppClaimReasonSet.get(i);
        	   
        	   LLAppClaimReasonDB tLLAppClaimReasonDB = new LLAppClaimReasonDB();
        	   tLLAppClaimReasonDB.setCaseNo(tLLAppClaimReasonSchema.getCaseNo());
        	   tLLAppClaimReasonDB.setRgtNo(tLLAppClaimReasonSchema.getRgtNo());
        	   tLLAppClaimReasonDB.setReasonCode(tLLAppClaimReasonSchema.getReasonCode());
        	   
        	   if (!tLLAppClaimReasonDB.getInfo())
        	   {
                   CError tError = new CError();
                   tError.moduleName = "LLGrpSocialConfigBL";
                   tError.functionName = "submitData_dealData";
                   tError.errorMessage = "团体保单号为"+tLLAppClaimReasonSchema.getRgtNo()+"社保业务配置信息查询失败！";
                   this.mErrors.addOneError(tError);
               }
        	   
               LLAppClaimReasonSchema tempLLAppClaimReasonSchema = tLLAppClaimReasonDB.getSchema();
               mdelLLAppClaimReasonSet.add(tempLLAppClaimReasonSchema);              
           }
           map.put(mdelLLAppClaimReasonSet, "DELETE");
           return true;
        } else {
            CError tError = new CError();
            tError.moduleName = "LLGrpSocialConfigBL";
            tError.functionName = "submitData_dealData";
            tError.errorMessage = "处理数据失败！";
            this.mErrors.addOneError(tError);
            return false;

        }
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLGrpSocialConfigBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

}
