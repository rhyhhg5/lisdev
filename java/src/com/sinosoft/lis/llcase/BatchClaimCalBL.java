package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: ClientRegisterBL </p>
 * <p>Description: ClientRegisterBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author:Xx
 * @CreateDate：2005-02-18 13:58:55
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class BatchClaimCalBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCaseSet mLLCaseSet = new LLCaseSet();

    public BatchClaimCalBL() {
    }

    public static void main(String[] args) {
        VData tVData = new VData();
        BatchClaimCalBL tBatchClaimCalBL = new BatchClaimCalBL();
        LLRegisterSchema tLLRegisterSchema =new LLRegisterSchema();
        tLLRegisterSchema.setRgtNo("P9400061121000001");
        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86940000";
        mGlobalInput.ComCode = "86940000";
        mGlobalInput.Operator = "cm9402";

        tVData.add(mGlobalInput);
        tVData.add(tLLRegisterSchema);
        tBatchClaimCalBL.submitData(tVData, "cal");
        tVData.clear();
        tVData = tBatchClaimCalBL.getResult();
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ClientRegisterBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ClientRegisterBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("--Star GetInputData--");
        this.mLLRegisterSchema.setSchema((LLRegisterSchema) cInputData.
                             getObjectByObjectName("LLRegisterSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
    	System.out.println("团体理赔处理批次理算开始："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
    	String tsq1 = "INSERT INTO ldtimetest VALUES ('rgtType1','批理算','"+mLLRegisterSchema.getRgtNo()+"','批次理算开始','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq1);
        String tsql = "update llcase set calflag='' where rgtno='"
                      +mLLRegisterSchema.getRgtNo()+"'";
        MMap tmap = new MMap();
        tmap.put(tsql,"UPDATE");
        VData tdata = new VData();
        System.out.println("获取批次号并校验批次信息开始："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
        String tsq2 = "INSERT INTO ldtimetest VALUES ('rgtType1','批理算','"+mLLRegisterSchema.getRgtNo()+"','校验批次信息开始','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq2);
        System.out.println("---Star dealData---mOperate---" + mOperate);
        if(!checkData())
            return false;
        
        System.out.println("获取批次号并校验批次信息结束："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
        String tsq3 = "INSERT INTO ldtimetest VALUES ('rgtType1','批理算','"+mLLRegisterSchema.getRgtNo()+"','校验批次信息结束','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq3);
        System.out.println("循环处理该批次下每个分案信息开始："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
        String tsq4 = "INSERT INTO ldtimetest VALUES ('rgtType1','批理算','"+mLLRegisterSchema.getRgtNo()+"','循环处理分案信息开始','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq4);
        for(int i=1;i<=mLLCaseSet.size();i++){
            if(!calClaim(mLLCaseSet.get(i).getCaseNo())){
                continue;
            }else{
                LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
                mLLCaseOpTimeSchema.setCaseNo(mLLCaseSet.get(i).getCaseNo());
                mLLCaseOpTimeSchema.setRgtState("04");
                mLLCaseOpTimeSchema.setOperator(mGlobalInput.Operator);
                mLLCaseOpTimeSchema.setManageCom(mGlobalInput.ManageCom);
                LLCaseCommon tLLCaseCommon = new LLCaseCommon();
                try {
                    LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.
                            CalTimeSpan(mLLCaseOpTimeSchema);
                    tmap.put(tLLCaseOpTimeSchema, "DELETE&INSERT");
                } catch (Exception ex) {
                    System.out.println("没有时效记录");
                }
            }
        }
        System.out.println("循环处理该批次下每个分案信息结束："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
        String tsq5 = "INSERT INTO ldtimetest VALUES ('rgtType1','批理算','"+mLLRegisterSchema.getRgtNo()+"','循环处理分案信息结束','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq5);
        if (this.mOperate.equals("DELETE||MAIN")) {
            map.put(mLLRegisterSchema, "DELETE");
        }
        System.out.println("理赔核赔开始："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
        String tsq6 = "INSERT INTO ldtimetest VALUES ('rgtType1','批理算','"+mLLRegisterSchema.getRgtNo()+"','理赔核赔开始','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq6);
        //#2283 团体理赔案件，理算增加保额校验，在理算完成后进行
        if(!this.mOperate.equals("DELETE||MAIN"))
        {
	        if (!uwCheck()) {
	            return false;
	        }
        }
        System.out.println("理赔核赔结束："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
        String tsq7 = "INSERT INTO ldtimetest VALUES ('rgtType1','批理算','"+mLLRegisterSchema.getRgtNo()+"','理赔核赔结束','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq7);
        tdata.add(tmap);
        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(tdata, null)) {
            CError.buildErr(this, "数据库保存失败");
            return false;
        }
        System.out.println("团体理赔处理批次理算结束："+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
        String tsq8 = "INSERT INTO ldtimetest VALUES ('rgtType1','批理算','"+mLLRegisterSchema.getRgtNo()+"','批次理算结束','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"') ";
    	new ExeSQL().execUpdateSQL(tsq8);
        return true;
    }
    
    /**
     * 调用核赔规则
     * @return boolean
     */
    private boolean uwCheck() {

        String tsql="";
        int tErrNO=0;
        String tErr=null;
        ExeSQL exesql = new ExeSQL();
        SSRS ssrs = new SSRS();
        System.out.println(this.mLLRegisterSchema.getRgtNo());
        if(this.mLLRegisterSchema.getRgtNo()!=null&&!("".equals(this.mLLRegisterSchema.getRgtNo()))){
	        tsql="select distinct customerno from llcase where rgtno='"+this.mLLRegisterSchema.getRgtNo()+"'  with ur";
	        System.out.println("BatchClaimCalBL-中通过批量号获取客户号："+tsql);
	        ssrs = exesql.execSQL(tsql);
        }
               
        System.out.println(ssrs.getMaxRow());
        for(int i=1;i<=ssrs.getMaxRow();i++){
      	  VData tVData = new VData();
      	  LLUWCheckBL tLLUWCheckBL = new LLUWCheckBL();
      	  LLCaseSchema tLLCaseSchema=new LLCaseSchema();
      	  LLCaseSet tLLCaseSet=new LLCaseSet();
      	  LLCaseRelaSet tLLCaseRelaSet=new LLCaseRelaSet();
      	  LLSubReportSet tLLSubReportSet=new LLSubReportSet();
      	  LLClaimDetailSet tLLClaimDetailSet=new LLClaimDetailSet();
      	  LLCaseDB tLLCaseDB=new LLCaseDB();
      	  tLLCaseDB.setCustomerNo(ssrs.GetText(i, 1));
      	  tLLCaseDB.setRgtNo(this.mLLRegisterSchema.getRgtNo());
      	  tLLCaseSet=tLLCaseDB.query();
      	  String casenos="";
      	  if(tLLCaseSet!=null&&tLLCaseSet.size()>0)
      	  {
      		  for(int a=1;a<=tLLCaseSet.size();a++){
	      		  System.out.println("BatchClaimCalBL-中通过客户号获取理赔业务表信息Start！");
	      		  casenos=casenos+","+tLLCaseSet.get(a).getCaseNo();
	      		  LLClaimDetailDB tLLClaimDetailDB=new LLClaimDetailDB();
	      		  tLLClaimDetailDB.setCaseNo(tLLCaseSet.get(a).getCaseNo());
	      		  tLLClaimDetailSet.add(tLLClaimDetailDB.query());
	      		  LLCaseRelaDB tLLCaseRelaDB=new LLCaseRelaDB();
	      		  tLLCaseRelaDB.setCaseNo(tLLCaseSet.get(a).getCaseNo());
	      		  tLLCaseRelaSet.add(tLLCaseRelaDB.query());
	      		  System.out.println("BatchClaimCalBL-中通过客户号获取理赔业务表信息END！");
      		  }
      		  if(tLLCaseRelaSet!=null&&tLLCaseRelaSet.size()>0){
	      		  for(int b=1;b<=tLLCaseRelaSet.size();b++){
	      			  LLSubReportDB tLLSubReportDB=new LLSubReportDB();
	      			  tLLSubReportDB.setSubRptNo(tLLCaseRelaSet.get(b).getSubRptNo());
	      			  tLLSubReportSet.add(tLLSubReportDB.query());
	      		  }
	      		  System.out.println("BatchClaimCalBL-中获取理赔LLSubReport表信息END！");
      		  }
      	  }
        
      	  tVData.addElement(tLLCaseSchema);
      	  tVData.addElement(tLLCaseRelaSet);
      	  tVData.addElement(tLLSubReportSet);
      	  tVData.addElement(tLLClaimDetailSet);
      	  tVData.addElement(mGlobalInput);
          if (!tLLUWCheckBL.submitData(tVData,"")) {
          	  tErrNO=tErrNO+1;
          	  if(tErr==null){
          		  tErr=String.valueOf(tErrNO)+"、客户号为"+ssrs.GetText(i, 1)+"的客户，"+tLLUWCheckBL.getErrors().getFirstError()+"(本次批量导入该客户所属案件号"+casenos+");";  
          	  }else{ 
          		  tErr=tErr+String.valueOf(tErrNO)+"、客户号为"+ssrs.GetText(i, 1)+"的客户，"+tLLUWCheckBL.getErrors().getFirstError()+"(本次批量导入该客户所属案件号"+casenos+");";
               // CError.buildErr(this,"客户号为"+ssrs.GetText(i, 1)+"的客户，"+tLLUWCheckBL.getErrors().getFirstError()+"(本次批量导入该客户所属案件号"+casenos+")");
          	  }
            }          
         }
         if(tErr!=null){
        	 CError.buildErr(this,tErr);
        	 return false;
         }
         return true;
    }

    /**
     * 处理前检查数据是否合理
     * @return boolean
     */
    private boolean checkData(){
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        if (!tLLRegisterDB.getInfo()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SimpleClaimBL";
            tError.functionName = "checkData";
            tError.errorMessage = "团体立案信息查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
        String declineflag = "" + mLLRegisterSchema.getDeclineFlag();
        if (declineflag.equals("1")) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SimpleClaimBL";
            tError.functionName = "checkData";
            tError.errorMessage = "该团体申请已撤件，不能再录入个人客户!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if ("03".equals(mLLRegisterSchema.getRgtState())) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SimpleClaimBL";
            tError.functionName = "checkData";
            tError.errorMessage = "团体申请下所有个人案件已经结案完毕，" +
                                  "不能再录入个人客户!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if ("04".equals(mLLRegisterSchema.getRgtState())) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SimpleClaimBL";
            tError.functionName = "checkData";
            tError.errorMessage = "团体申请下所有个人案件已经给付确认，" +
                                  "不能再录入个人客户!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        tLLCaseDB.setRgtState("03");
        mLLCaseSet = tLLCaseDB.query();
        if(mLLCaseSet.size()<=0){
            CError tError = new CError();
            tError.moduleName = "SimpleClaimBL";
            tError.functionName = "checkData";
            tError.errorMessage = "没有待理算的案件!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 自动理算，生成赔案信息表。
     * @return boolean
     */
    private boolean calClaim(String aCaseNo){
        ClaimCalBL aClaimCalAutoBL = new ClaimCalBL();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setCaseNo(aCaseNo);
        VData aVData = new VData();
        aVData.addElement(mGlobalInput);
        aVData.addElement(tLLCaseSchema);
        if(!aClaimCalAutoBL.submitData(aVData, "autoCal")){
            CError.buildErr(this, "理算失败");
             return false;
        }
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLLRegisterSchema);
            mInputData.add(map);
            mResult.clear();
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDDrugBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
