/*
 * <p>ClassName: LLAffixBL </p>
 * <p>Description: LLAffixBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author:Xx:
 * @CreateDate：2005-01-19 13:58:55
 */
package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import java.util.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;


public class LLAffixBL {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();
  private VData mResult = new VData();

  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  private MMap map = new MMap();

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();
  private String appDate="";
  /** 数据操作字符串 */
  private String mOperate;
  private String mCaseNo = "";
  /** 业务处理相关变量 */
  private LLAffixSet mLLAffixSet = new LLAffixSet();
  private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
  String LoadFlag = "";
  PubSubmit tPubSubmit = new PubSubmit();
//private LDDrugSet mLDDrugSet=new LDDrugSet();
  public LLAffixBL() {
  }

  public static void main(String[] args)
  {
    LLAffixSet mLLAffixSet = new LLAffixSet();
    LLAffixSchema mLLAffixSchema = new LLAffixSchema();
    LLAffixSchema mLLAffixSchema1 = new LLAffixSchema();
    VData tVDate=new VData();
  LLAffixBL tLLAffixBL=new LLAffixBL();

    mLLAffixSchema.setAffixNo(" 86000000000304");
    mLLAffixSchema.setSerialNo("1606");
//    mLLAffixSet.get(1).setAffixType("2");
//    mLLAffixSet.get(1).setCaseNo("11222455488");
//    mLLAffixSet.get(1).setRgtNo("324234234");
//    mLLAffixSet.get(1).setReasonCode("ewe");
//    mLLAffixSet.get(1).setSupplyDate("2005-1-5");
//    mLLAffixSet.get(1).setShortCount("0");
    GlobalInput gInput = new GlobalInput();
    gInput.Operator="xuxin";
    gInput.ManageCom="86";
    mLLAffixSet.add(mLLAffixSchema);
    tVDate.add(mLLAffixSet);
    tVDate.add( gInput );
    tLLAffixBL.submitData(tVDate,"BACK||MAIN");

//    tLLAffixBL.setSupplyDate("2005-8-5","550000000000094","1");

  }

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {

    this.mOperate = cOperate;

    if (!getInputData(cInputData))
    {
        return false;
    }
    //检查数据合法性
    if (!checkInputData())
    {
        return false;
        }


    //进行业务处理
    if (!dealData())
    {
        return false;
    }

    //准备往后台的数据
    if (!prepareOutputData())
    {
        return false;
    }

    if (!mOperate.equals("QUERY||MAIN"))
    {
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLAffixBL";
            tError.functionName = "LLAffixBL";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }

    }

    mInputData = null;
    return true;
  }


  /**
 * 对输入数据做必要的校验
 * @return boolean
 */
private boolean checkInputData()
{
    LLCaseDB mLLCaseDB = new LLCaseDB();
//    LLCaseSet mLLCaseSet = new LLCaseSet();
    mLLCaseDB.setCaseNo(mCaseNo);
    if(!mLLCaseDB.getInfo()){
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        CError tError = new CError();
        tError.moduleName = "LLAffixBL";
        tError.functionName = "LLAffixBL";
        tError.errorMessage = "查询llcase表数据失败!";
        this.mErrors.addOneError(tError);
    }
//    mLLCaseSet.set(mLLCaseDB.query());
//    mLLCaseSchema=mLLCaseSet.get(1);
    mLLCaseSchema=mLLCaseDB.getSchema();

    if (!mLLCaseSchema.getRgtState().equals("01")
    && !mLLCaseSchema.getRgtState().equals("02")
    && !mLLCaseSchema.getRgtState().equals("13")) {

    CError tError = new CError();
    tError.moduleName = "LLAffixBL";
    tError.functionName = "prepareData";
    tError.errorMessage = "该案件状态下不允许修改该案件信息。";
    this.mErrors.addOneError(tError);
    return false;

}

    return true;
    }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData()
  {
      int noShortFlag = 1;
      FDate fd = new FDate();
      Date lastDate = new Date();
      Date theDate = new Date();
      String sDate = "";
      if (mOperate.equals("INSERT||MAIN") ||
          mOperate.equals("UPDATE||MAIN"))
      {
          String strSQL = " delete from LLAffix " +
                          " where CaseNo='" + mCaseNo + "'";

          map.put(strSQL, "DELETE");

          String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
          System.out.println("管理机构代码是 : " + tLimit);
          String rgtaffixno = PubFun1.CreateMaxNo("rgtaffixno", tLimit);
          if (mLLAffixSet != null && mLLAffixSet.size() > 0)
          {
              lastDate = fd.getDate(mLLAffixSet.get(1).getSupplyDate());
              sDate = mLLAffixSet.get(1).getSupplyDate();
              for (int i = 1; i <= mLLAffixSet.size(); i++)
              {
                  mLLAffixSet.get(i).setAffixNo(rgtaffixno);
                  mLLAffixSet.get(i).setSerialNo(i);
                  mLLAffixSet.get(i).setMakeDate(PubFun.getCurrentDate());
                  mLLAffixSet.get(i).setMakeTime(PubFun.getCurrentTime());
                  mLLAffixSet.get(i).setModifyDate(PubFun.getCurrentDate());
                  mLLAffixSet.get(i).setModifyTime(PubFun.getCurrentTime());
                  mLLAffixSet.get(i).setMngCom(tLimit);
                  mLLAffixSet.get(i).setOperator(mGlobalInput.Operator);
                  if (noShortFlag == 1)
                  {
                      if (mLLAffixSet.get(i).getShortCount() > 0)
                      {
                          noShortFlag = 0;
                          sDate = "";
                      }
                      else
                      {
                              theDate = fd.getDate(mLLAffixSet.get(i).getSupplyDate());
                              if (theDate.after(lastDate))
                              {
                                  lastDate = theDate;
                                  sDate = mLLAffixSet.get(i).getSupplyDate();
                              }
                      }
                  }

              }

              map.put(mLLAffixSet, "INSERT");
          }
          //材料齐备，并且日期不能为空3235。
          if(!"".equals(sDate) && sDate!=null && !"".equals(appDate) && appDate!=null && noShortFlag==1){
        	  System.out.println(sDate);
        	  System.out.println(appDate);
        	  System.out.println(noShortFlag);
        	    	  
        	  Date sd=fd.getDate(sDate);
        	  Date ad=fd.getDate(appDate);
        	  if(ad.after(sd)){
        	  CError tError = new CError();
              tError.moduleName = "LLAffixBL";
              tError.functionName = "LLAffixBL";
              tError.errorMessage = "申请日期与材料齐备日期先后顺序不符合逻辑，请检查录入是否正确";
              this.mErrors.addOneError(tError);
        	  return false;
        	  }
          } //3235
          setSupplyDate(sDate, mCaseNo, noShortFlag);

      }

// 退回提交材料处理

      if( mOperate.equals("BACK||MAIN"))

      {
//
//     String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
//     System.out.println("管理机构代码是 : " + tLimit);
//     String rgtaffixno = PubFun1.CreateMaxNo("rgtaffixno", tLimit);

          LLAffixSet saveLLAffixSet= new LLAffixSet();
     if (mLLAffixSet != null && mLLAffixSet.size() > 0)
     {

//         lastDate = fd.getDate(mLLAffixSet.get(1).getSupplyDate());
//         sDate = mLLAffixSet.get(1).getSupplyDate();
         for (int i = 1; i <= mLLAffixSet.size(); i++)
         {
          //   LLAffixDB tDB = new LLAffixDB();
             System.out.println("here *****************************");
              LLAffixSchema tLLAffixSchema =  mLLAffixSet.get(i);
//              tDB.setAffixNo(tLLAffixSchema.getAffixNo() );
//              tDB.setSerialNo( tLLAffixSchema.getSerialNo());
              //tDB.setSchema(tLLAffixSchema);
//              if ( !tDB.getInfo() )
//              {
//                  CError.buildErr(this,"没有查找到传入的附件");
//                  return false;
//              }
//              tLLAffixSchema= tDB.getSchema();
              if ( tLLAffixSchema.getFileEndDate()==null ){
//             mLLAffixSet.get(i).setAffixNo(rgtaffixno);
//             mLLAffixSet.get(i).
//                     setSerialNo(PubFun1.CreateMaxNo("AffixSerNo", 20));
            tLLAffixSchema.setMakeDate(PubFun.getCurrentDate());
             tLLAffixSchema.setMakeTime(PubFun.getCurrentTime());
             tLLAffixSchema.setMngCom(mGlobalInput.ManageCom);
                  tLLAffixSchema.setModifyDate(PubFun.getCurrentDate());
                  tLLAffixSchema.setModifyTime(PubFun.getCurrentTime());
                   tLLAffixSchema.setOperator(mGlobalInput.Operator);
                  tLLAffixSchema.setFileEndDate(PubFun.getCurrentDate());
                  saveLLAffixSet.add(tLLAffixSchema);
              }
//             mLLAffixSet.get(i).setMngCom(tLimit);
//             mLLAffixSet.get(i).setOperator(mGlobalInput.Operator);
//             if (noShortFlag == 1)
//             {
//                 if (mLLAffixSet.get(i).getShortCount() > 0)
//                 {
//                     noShortFlag = 0;
//                     sDate = "";
//                 }
//                 else
//                 {
//                         theDate = fd.getDate(mLLAffixSet.get(i).getSupplyDate());
//                         if (theDate.after(lastDate))
//                         {
//                             lastDate = theDate;
//                             sDate = mLLAffixSet.get(i).getSupplyDate();
//                         }
//                 }
//             }

         }

         map.put(saveLLAffixSet, "UPDATE");
     }
 }

// 退回提交材料处理结束







      return true;
  }
  public boolean setSupplyDate(String mDate, String mCaseNo, int noShortFlag)
  {
      String RgtState = "13";

      if (noShortFlag == 1)
      {
          System.out.println("材料齐备！");
          System.out.println("齐备日期：" + mDate);
          RgtState = "01";
          if (!"".equals(mDate))
          {
              mDate = "'" + mDate + "'";
          }
          else
          {
               mDate = null;
          }
          if(LoadFlag.equals("1")){
              LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
              tLLCaseOpTimeSchema.setCaseNo(mCaseNo);
              tLLCaseOpTimeSchema.setRgtState("01");
              LLCaseCommon tLLCaseCommon = new LLCaseCommon();
              LLCaseOpTimeSchema saveLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(tLLCaseOpTimeSchema);
              map.put(saveLLCaseOpTimeSchema,"DELETE&INSERT");
          }
      }
      else
      {
          System.out.println("材料未齐备！");
          RgtState = "13";
          mDate = null;
      }

      String strSQL = " update llcase set " +
                      " affixgetdate = " + mDate + "," +
                      " RgtState = '" + RgtState + "' " +
                      " where CaseNo='" + mCaseNo + "'";

      map.put(strSQL, "UPDATE");

      return true;
  }

  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
      try{
          mLLAffixSet.set(
                  (LLAffixSet)
                  cInputData.getObjectByObjectName("LLAffixSet", 0));
          mCaseNo = mLLAffixSet.get(1).getCaseNo();

          if (!mOperate.equals("BACK||MAIN")) {
              TransferData tTransferData = (TransferData) cInputData.
                                           getObjectByObjectName("TransferData",
                      0);
              LoadFlag = (String) tTransferData.getValueByName("LoadFlag");
              appDate=(String) tTransferData.getValueByName("appDate");
          }
          mGlobalInput.setSchema(
                  (GlobalInput)
                  cInputData.getObjectByObjectName("GlobalInput", 0));
      }catch(Exception ex)
          {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "LLAffixBL";
              tError.functionName = "getInputData";
              tError.errorMessage = "输入数据出错";
              this.mErrors.addOneError(tError);
              return false;

          }

      return true;
  }

  private boolean prepareOutputData()
  {
      try
      {
          mInputData.clear();
          mInputData.add(mLLAffixSet);
          mInputData.add(map);
          mResult.clear();
          mResult.add(mLLAffixSet);
      }
      catch (Exception ex)
      {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LLAffixBL";
          tError.functionName = "prepareData";
          tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
          this.mErrors.addOneError(tError);
          return false;
      }
      return true;
  }

  public VData getResult()
  {
      return this.mResult;
  }

}
