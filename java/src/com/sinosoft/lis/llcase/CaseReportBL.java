package com.sinosoft.lis.llcase;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class CaseReportBL
{
  public  CErrors mErrors = new CErrors();
  VData mInputData = new VData();
  private VData mResult = new VData();

  String mOperate = "";
  String StartDate = "";
  String EndDate = "";
  String ManageCom = "";
  double totalMoney = 0.00;
  public CaseReportBL()
  {
  }
  public VData getResult()
  {
    return mResult;
  }
  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;
    if (!getInputData(cInputData))
      return false;
    if(!QueryData())
      return false;
    return true;
  }
  private boolean getInputData(VData cInputData)
  {
    StartDate = (String)cInputData.get(0);
    StartDate = StartDate.trim();
    System.out.println("开始日期是"+StartDate);
    EndDate = (String)cInputData.get(1);
    EndDate = EndDate.trim();
    System.out.println("结束日期是"+EndDate);
    ManageCom = (String)cInputData.get(2);
    ManageCom = ManageCom.trim();
    System.out.println("管理机构是"+ManageCom.trim());
    return true;
  }
  public static void main(String args[])
  {
    CaseReportBL tCaseReportBL = new CaseReportBL();
    String aStartDate = "2004-6-26";
    String aEndDate = "2004-7-25";
    String aManageCom = "86";
    VData aVData = new VData();
    aVData.addElement(aStartDate);
    aVData.addElement(aEndDate);
    aVData.addElement(aManageCom);
    tCaseReportBL.submitData(aVData,"QUERY");
  }
  public boolean QueryData()
  {
    ListTable tlistTable = new ListTable();
    tlistTable.setName("MODE");

    TextTag texttag=new TextTag();//新建一个TextTag的实例
    XmlExport xmlexport=new XmlExport();//新建一个XmlExport的实例
    xmlexport.createDocument("CaseReport.vts","printer");

    ListTable alistTable = new ListTable();
    alistTable.setName("INFO");

    LDComDB tLDComDB = new LDComDB();
    tLDComDB.setComCode(ManageCom);
    tLDComDB.getInfo();

    //查询出签批日期再本月的所有案件的险种信息
    String risk_sql = "select RiskCode,PolNo,RgtNo,RealPay,GrpPolNo from LLClaimPolicy  where "
                    +"rgtno in ( "+"select rgtno  from LLClaimUWMain where AppActionType='1' "
                    +"and makedate >='"+StartDate+"' and makedate <='"+EndDate+"' )"
                    +" and MngCom like '"+ManageCom+"%' "
                    +" order by RiskCode ";
    System.out.println("主查询语句是"+risk_sql);
    ExeSQL risk_exesql = new ExeSQL();
    SSRS risk_ssrs = new SSRS();
    risk_ssrs =risk_exesql.execSQL(risk_sql);
    if(risk_ssrs.getMaxRow()>0)
    {
      //可能查询出一个案件对应于多个保单号的现象
      String main_array[][] = new String [risk_ssrs.getMaxRow()][15];
      for(int i=1;i<=risk_ssrs.getMaxRow();i++)
      {
        //查询出立案号码的语句
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(risk_ssrs.GetText(i,3));
        tLLRegisterDB.getInfo();
        //查询出签批同意日期的语句
        LLClaimUWMainDB tLLClaimUWMainDB = new LLClaimUWMainDB();
        LLClaimUWMainSet tLLClaimUWMainSet = new LLClaimUWMainSet();
        tLLClaimUWMainDB.setRgtNo(risk_ssrs.GetText(i,3));
        tLLClaimUWMainDB.setAppActionType("1");
        tLLClaimUWMainSet.set( tLLClaimUWMainDB.query());
        if(tLLClaimUWMainSet.size()>0)
        {
          //查询出保单的管理机构的语句
          String manage_sql = "select ManageCom ,CValiDate ,PrtNo from LCPol where polno = '"+risk_ssrs.GetText(i,2).trim()+"' and ManageCom like '"+ManageCom.trim()+"%'"
                            +"union select ManageCom ,CValiDate,PrtNo from LBPol where PolNo = '"+risk_ssrs.GetText(i,2).trim()+"'and ManageCom like '"+ManageCom.trim()+"%' ";
          System.out.println("管理机构的查询语句是"+manage_sql);
          ExeSQL manage_exesql = new ExeSQL();
          SSRS manage_ssrs = manage_exesql.execSQL(manage_sql);
          if(manage_ssrs.getMaxRow()>0)
          {
            main_array[i-1][0] = "";//险种名称
            main_array[i-1][1] = String.valueOf(i);//序号
            main_array[i-1][2] = risk_ssrs.GetText(i,2).trim();//保单号码
            main_array[i-1][3] = risk_ssrs.GetText(i,3).trim();//立案号码
            main_array[i-1][4] = "无记录";//事故者姓名
            main_array[i-1][5] = tLLRegisterDB.getMakeDate().trim();//立案日期
            main_array[i-1][6] = tLLClaimUWMainSet.get(1).getMakeDate();//签批同意日期
            main_array[i-1][7] = risk_ssrs.GetText(i,4);//给付金额
            main_array[i-1][8] = manage_ssrs.GetText(1,1);//出单机构
            main_array[i-1][9] = "";//险种合计给付金额
            main_array[i-1][10]=risk_ssrs.GetText(i,1);
            main_array[i-1][11]=manage_ssrs.GetText(1,2);//保单生效日期
            main_array[i-1][12]=tLLRegisterDB.getSchema().getAccidentDate();//出险日期
            main_array[i-1][13]=risk_ssrs.GetText(i,5);
            main_array[i-1][14]=manage_ssrs.GetText(1,3);
            //查询出事故者姓名的语句
            String Name_sql = "select CustomerName from LLSubReport where RptNo in "
                            +"( select RptNo from LLRegister where RgtNo = '"
                            +risk_ssrs.GetText(i,3).trim()+"' ) ";
            System.out.println("事故者姓名的查询语句是"+Name_sql);
            ExeSQL exesql_1 = new ExeSQL();
            SSRS Name_ssrs = new SSRS();
            Name_ssrs = exesql_1.execSQL(Name_sql);
            if(Name_ssrs.getMaxRow()>0)
            {
              main_array[i-1][4]= Name_ssrs.GetText(1,1).trim();
            }
          }
        }
      }
      //对险种名称和合计金额进行赋值
      //查询出所有的险种和合计值
      String sum_sql = "select RiskCode,sum(RealPay),count(RiskCode) from LLClaimPolicy where "
                     +"rgtno in ( "+"select rgtno  from LLClaimUWMain where AppActionType='1' "
                     +"and makedate >='"+StartDate+"' and makedate <='"+EndDate+"')"
                     +"and MngCom like '"+ManageCom+"%'"
                     +" group by RiskCode order by RiskCode ";
      System.out.println("合计的查询语句是"+sum_sql);
      ExeSQL exesql_2 = new ExeSQL();
      SSRS sum_ssrs = new SSRS();
      sum_ssrs = exesql_2.execSQL(sum_sql);
      if(sum_ssrs.getMaxRow()>0)
      {
        int count = 0;
        for(int j=1;j<=sum_ssrs.getMaxRow();j++)
        {
          //对于险种名称的查询语句
          LMRiskDB tLMRiskDB = new LMRiskDB();
          tLMRiskDB.setRiskCode(sum_ssrs.GetText(j,1));
          if(!tLMRiskDB.getInfo())
          {
            this.mErrors.copyAllErrors(tLMRiskDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "RegisterUpdateBL";
            tError.functionName = "submitData";
            tError.errorMessage = "险种信息查询失败!!";
            this.mErrors .addOneError(tError) ;
            return false;
          }
          count=count+Integer.parseInt(sum_ssrs.GetText(j,3));
          System.out.println("当j的值是"+j+"时，count的值时"+count);
          main_array[count-1][0]=tLMRiskDB.getRiskName();
          main_array[count-1][9]=sum_ssrs.GetText(j,2);
        }

        for(int k=1;k<=risk_ssrs.getMaxRow();k++)
        {
          String[] cols = new String[15];
          cols[0] = main_array[k-1][10];//险种代码;
          cols[1] = main_array[k-1][0];//险种名称
          cols[2] = main_array[k-1][1];//序号
          cols[3] =main_array[k-1][2];//保单号码
          cols[4] =main_array[k-1][3];//立案号码
          cols[5] =main_array[k-1][4];//事故者姓名
          cols[6] =main_array[k-1][5];//立案日期
          cols[7] =main_array[k-1][6];//签批确认日期
          cols[8] =main_array[k-1][7];//给付金额
          cols[9] =main_array[k-1][9];//给付金额合计
          cols[10]=main_array[k-1][11];//保单生效日期
          cols[11]=main_array[k-1][12];//出险日期
          cols[12]=main_array[k-1][8];//保单管理机构
          cols[13]=main_array[k-1][13];//团单号码
          cols[14]=main_array[k-1][14];//印刷号码
          totalMoney = totalMoney + Double.parseDouble(cols[8]);
          alistTable.add(cols);
        }
        String[] m_col = new String[15];
        xmlexport.addDisplayControl("displayinfo");
        xmlexport.addListTable(alistTable, m_col);
        texttag.add("ManageName",tLDComDB.getName());
        texttag.add("StartDate",StartDate);
        texttag.add("EndDate",EndDate);
        texttag.add("Date1",PubFun.getCurrentDate());
        texttag.add("SumMoney",totalMoney);
        if (texttag.size()>0)
          xmlexport.addTextTag(texttag);
       // xmlexport.outputDocumentToFile("e:\\","CaseReportBL");//输出xml文档到文件

        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
      }
    }
    //查询出险种名称和合计给付金额
    return true;
  }
}
