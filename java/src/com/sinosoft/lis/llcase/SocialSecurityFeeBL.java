package com.sinosoft.lis.llcase;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author cc
 * @version 1.0
 */

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.SocialSecurityFeeClaimDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.SocialSecurityFeeClaimSchema;
import com.sinosoft.lis.vschema.SocialSecurityFeeClaimSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun;

public class SocialSecurityFeeBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据结果存储 */
    private VData mResult = new VData();
    /** 前台传来的操作符 */
    private String mOperate = "";
    /** 查勘费结算业务号 */
    private String mSettlementNo;
    /** 查勘费调查案件号 */
    private String mSurveyCaseNo;
    /** 查勘费成本中心 */
    private String mCostCenter;

    /** 数据提交容器 */
    private VData mInputData= new VData();

    /** 全局数据 */
    MMap map = new MMap();
    private GlobalInput mGlobalInput = new GlobalInput();
    private SocialSecurityFeeClaimSchema mSocialSecurityFeeClaimSchema = new SocialSecurityFeeClaimSchema();
    private SocialSecurityFeeClaimSet mSocialSecurityFeeClaimSet = new SocialSecurityFeeClaimSet();
    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
    private TransferData mTransferData = new TransferData();

    private String mDate = PubFun.getCurrentDate();
    private String mTime = PubFun.getCurrentTime();


    public SocialSecurityFeeBL() {
    }

    /**
         传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!"INSERT||MAIN".equals(cOperate) && !"QUERY||MAIN".equals(cOperate)) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        mOperate = cOperate;
        System.out.println("本次业务操作类型："+mOperate);
        if (!getInputData(cInputData,cOperate)) {
            return false;
        }
        
        if("QUERY||MAIN".equals(cOperate)){
    		if(!getQueryResult(mTransferData)){
    			buildError("submitData", "查询数据集失败");
    		}
        }

        // 得到外部传入的数据，将数据备份到本类中
        if("INSERT||MAIN".equals(cOperate)){       	
        	//进行业务处理
            if (!dealData()) {
                return false;
            }
        }
        
        //准备传往后台的数据
        VData vData = new VData();
        MMap mMap = new MMap();
        if (mMap != null) {
            map.add(mMap);
            vData.add(map);
        } else {
            return false;
        }

        //数据提交、保存
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start PubSubmit Submit...");

        if (!tPubSubmit.submitData(vData, "")) {
        	if("QUERY||MAIN".equals(cOperate)){

        	}else{
        		CError tError = new CError();
                tError.moduleName = "SocialSecurityFeeBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!请每次结算操作后等待60秒后再继续操作";
                this.mErrors.addOneError(tError);
                return false;
        	}           
        }
        System.out.println("---commitData---");
        return true;
    }

    /**
     * 查询结算信息，需要判断是否已经生成结算记录，此处需要从其他数据源获取数据（视图）
     * @return
     */
    private boolean getQueryResult(TransferData aTransferData) {
    	DBConnDB tDBConnDB = new DBConnDB();
    	JdbcUrl tJdbcUrl = new JdbcUrl();

    	//社保通提供的对查勘费结算的连接配置
    	tJdbcUrl.setDBType("DB2");
    	tJdbcUrl.setIP("10.252.56.3");
    	tJdbcUrl.setPort("50000");
    	tJdbcUrl.setDBName("devnew");
    	tJdbcUrl.setUser("db2finance");
    	tJdbcUrl.setPassWord("db2finance");
          	
    	tDBConnDB.getConnection(tJdbcUrl);
    	
    	String RgtDateS = (String)aTransferData.getValueByName("RgtDateS");
		String RgtDateE = (String)aTransferData.getValueByName("RgtDateE");
		String cbdUnitIdT = (String)aTransferData.getValueByName("cbdUnitIdT");
		String ydUnitIdT = (String)aTransferData.getValueByName("ydUnitIdT");
		String nameT = (String)aTransferData.getValueByName("nameT");
		String medicareCodeT = (String)aTransferData.getValueByName("medicareCodeT");
		String idNoT = (String)aTransferData.getValueByName("idNoT");
    	//根据SQL语句获取结果集
    	String sql = "select surveyCaseNum,settlementNum,paymentVoucher,cbdUnitName,ydUnitName," +
    			"totalAmount,surveyStartDate,surveyEndDate,createDate,name,medicareCode," +
    			"idNo,cbdUnitId,ydUnitId " +
    			"from db2inst1.SIP_YIDI_FINANCESETTLEMENT a " +
    			"where 1=1 and surveyEndDate >= '"+RgtDateS+"' and surveyEndDate <= '"+RgtDateE+"' " +
    			"and cbdUnitId like '"+cbdUnitIdT+"%' and ydUnitId like '"+ydUnitIdT+"%' ";
    	String tPartSql = "";
    	if(!"".equals(nameT) && nameT!=null){
    		tPartSql += " and name like '"+nameT+"%'"; 
    	}
    	if(!"".equals(medicareCodeT) && medicareCodeT!=null){
    		tPartSql += " and medicareCode like '"+medicareCodeT+"%'"; 
    	}
    	if(!"".equals(idNoT) && idNoT!=null){
    		tPartSql += " and idNo like '"+idNoT+"%'"; 
    	}
    	sql = sql+tPartSql;
    	System.out.println("最终查询语句为："+sql);
    	ResultSet tResultSet = tDBConnDB.getResultSet(sql);
    	if(tResultSet==null){
    		System.out.println("获取结果集过程，连接中断或连接为空");
    		buildError("getQueryResult", "结果集为空！请修改查询条件重新操作");
    		return false;
    		//添加错误处理,进行阻断
    	}
    	try {
			while(tResultSet.next()){
				SocialSecurityFeeClaimSchema tSocialSecurityFeeClaimSchema = new SocialSecurityFeeClaimSchema(); 
		    	//对mSocialSecurityFeeClaimSchema进行存储
				tSocialSecurityFeeClaimSchema.setActuGetNo("00");//结算前临时存储
				tSocialSecurityFeeClaimSchema.setFeeFinaType("SSF");
				tSocialSecurityFeeClaimSchema.setFeeOperationType("CS");
				tSocialSecurityFeeClaimSchema.setOperator(mGlobalInput.Operator);
				tSocialSecurityFeeClaimSchema.setMakeDate(mDate);
				tSocialSecurityFeeClaimSchema.setMakeTime(mTime);
				tSocialSecurityFeeClaimSchema.setSettlementState("00");
				tSocialSecurityFeeClaimSchema.setModifyDate(mDate);
				tSocialSecurityFeeClaimSchema.setModifyTime(mTime);
				
				String tSurveyCaseNo = tResultSet.getString("surveyCaseNum");
				String tSettlementNo = tResultSet.getString("settlementNum");

				if(!queryExistData(tSurveyCaseNo,tSettlementNo)){
					continue;
				}
				tSocialSecurityFeeClaimSchema.setSurveyCaseNo(tSurveyCaseNo);
				tSocialSecurityFeeClaimSchema.setSettlementNo(tSettlementNo);
				tSocialSecurityFeeClaimSchema.setPaymentVoucherNo(tResultSet.getString("paymentVoucher"));
				tSocialSecurityFeeClaimSchema.setInsuranceComName(tResultSet.getString("cbdUnitName"));
				tSocialSecurityFeeClaimSchema.setRemoteSurveyComName(tResultSet.getString("ydUnitName"));
				tSocialSecurityFeeClaimSchema.setTotalAmount(tResultSet.getString("totalAmount"));
				tSocialSecurityFeeClaimSchema.setSurveyStartDate(tResultSet.getString("surveyStartDate"));
				tSocialSecurityFeeClaimSchema.setSurveyEndDate(tResultSet.getString("surveyEndDate"));
				tSocialSecurityFeeClaimSchema.setCreateDate(tResultSet.getString("createDate"));
				tSocialSecurityFeeClaimSchema.setName(tResultSet.getString("name"));
				tSocialSecurityFeeClaimSchema.setMedicareCode(tResultSet.getString("medicareCode"));
				tSocialSecurityFeeClaimSchema.setIdNo(tResultSet.getString("idNo"));
				tSocialSecurityFeeClaimSchema.setInsuranceComCode(tResultSet.getString("cbdUnitId"));
				tSocialSecurityFeeClaimSchema.setRemoteSurveyComCode(tResultSet.getString("ydUnitId"));
				mSocialSecurityFeeClaimSet.add(tSocialSecurityFeeClaimSchema);				
			}
			map.put(mSocialSecurityFeeClaimSet, "INSERT");
		} catch (SQLException e) {
			buildError("getQueryResult", "结果集查询失败，请重新操作！");
			e.printStackTrace();
			return false;
		}finally{
			//各种操作
	    	tDBConnDB.closeConnection();
		}  	
		return true;
	}

    /**
     * 通过查勘费结算业务号、查勘费调查案件号判断是否继续向数据库中存储数据
     * @param surveyCaseNo
     * @param settlementNo
     * @return	没有记录，返回true
     */
	private boolean queryExistData(String surveyCaseNo, String settlementNo) {
		SocialSecurityFeeClaimDB tSocialSecurityFeeClaimDB = new SocialSecurityFeeClaimDB();
        tSocialSecurityFeeClaimDB.setSettlementNo(settlementNo);
        tSocialSecurityFeeClaimDB.setSurveyCaseNo(surveyCaseNo);
        tSocialSecurityFeeClaimDB.setFeeFinaType("SSF");
        tSocialSecurityFeeClaimDB.setFeeOperationType("CS");
        if(!tSocialSecurityFeeClaimDB.getInfo()){
        	return true;
        }
		return false;
	}

	/**
	 * 根据前面的输入数据，进行UI逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
    private boolean dealData() {
    	//生成实付号码
        String sNoLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
        String ActuGetNo = PubFun1.CreateMaxNo("ACTUGETNO", sNoLimit);
    	
        SocialSecurityFeeClaimSchema tSocialSecurityFeeClaimSchema = new SocialSecurityFeeClaimSchema();
        SocialSecurityFeeClaimDB tSocialSecurityFeeClaimDB = new SocialSecurityFeeClaimDB();
        tSocialSecurityFeeClaimDB.setSettlementNo(mSettlementNo);
        tSocialSecurityFeeClaimDB.setSurveyCaseNo(mSurveyCaseNo);
        tSocialSecurityFeeClaimDB.setFeeFinaType("SSF");
        tSocialSecurityFeeClaimDB.setFeeOperationType("CS");
        tSocialSecurityFeeClaimSchema = tSocialSecurityFeeClaimDB.query().get(1).getSchema();
        
        tSocialSecurityFeeClaimSchema.setActuGetNo(ActuGetNo);
        tSocialSecurityFeeClaimSchema.setOperator(mGlobalInput.Operator);
        tSocialSecurityFeeClaimSchema.setSettlementState("01");//该结算信息由00变为01；临时状态变为未财务付费
        tSocialSecurityFeeClaimSchema.setModifyDate(mDate);
        tSocialSecurityFeeClaimSchema.setModifyTime(mTime);
        tSocialSecurityFeeClaimSchema.setCostCenter(mCostCenter);	//存储成本中心
        
        //#2208 社保通数据读取报错-成本中心与机构一致校验
        String tInsuranceComCode = mSocialSecurityFeeClaimSchema.getInsuranceComCode();
        String tInsuranceComName = "";
        String tSQL = "select CodeName from ldcode where 1 = 1 and code='"+tInsuranceComCode+"' and codetype = 'ssfmng'  with ur ";
        ExeSQL tExeSQL = new ExeSQL();
        tInsuranceComName = tExeSQL.getOneValue(tSQL);
        if("".equals(tInsuranceComName) || tInsuranceComName==null || "null".equals(tInsuranceComName)){
    		buildError("dealData", "录入的参保地机构不存在，请重新录入！");
    		return false;
        }
        tSocialSecurityFeeClaimSchema.setInsuranceComCode(tInsuranceComCode);
        tSocialSecurityFeeClaimSchema.setInsuranceComName(tInsuranceComName);
    	
    	//同步生成ljaget表中的数据
    	mLJAGetSchema.setActuGetNo(ActuGetNo);
    	mLJAGetSchema.setOtherNo(mSettlementNo);
    	mLJAGetSchema.setOtherNoType("23");
    	mLJAGetSchema.setPayMode("1");
    	mLJAGetSchema.setSumGetMoney(mSocialSecurityFeeClaimSchema.getTotalAmount());
    	mLJAGetSchema.setOperator(mGlobalInput.Operator);
    	mLJAGetSchema.setManageCom(mGlobalInput.ManageCom);
    	mLJAGetSchema.setMakeDate(mDate);
    	mLJAGetSchema.setMakeTime(mTime);
    	mLJAGetSchema.setModifyDate(mDate);
    	mLJAGetSchema.setModifyTime(mTime);
         
        //实付记录插入
        map.put(tSocialSecurityFeeClaimSchema, "UPDATE");
        map.put(mLJAGetSchema, "INSERT");
            
        try {
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SocialSecurityFeeBL";
            tError.functionName = "dealdate";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错:" + ex.toString();
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData,String cOperate) {

    	mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        // 得到外部传入的数据，将数据备份到本类中
        if("INSERT||MAIN".equals(cOperate)){       	
            //全局变量            
            mSocialSecurityFeeClaimSchema = (SocialSecurityFeeClaimSchema) cInputData.getObjectByObjectName(
            		"SocialSecurityFeeClaimSchema", 0);
        
            mSettlementNo = mSocialSecurityFeeClaimSchema.getSettlementNo();
            if("".equals(mSettlementNo) || mSettlementNo == null){
            	buildError("submitData", "结算号不存在！请重新选择结算信息");
            	return false;
            }
            mSurveyCaseNo = mSocialSecurityFeeClaimSchema.getSurveyCaseNo();
            if("".equals(mSettlementNo) || mSettlementNo == null){
            	buildError("submitData", "调查案件号不存在！请重新选择结算信息");
            	return false;
            }

            mCostCenter = mSocialSecurityFeeClaimSchema.getCostCenter();
            if("".equals(mCostCenter) || mCostCenter == null){
            	buildError("submitData", "成本中心不存在！请重新选择结算信息");
            	return false;
            }
            
            SocialSecurityFeeClaimDB tSocialSecurityFeeClaimDB = new SocialSecurityFeeClaimDB();
            tSocialSecurityFeeClaimDB.setFeeFinaType("SSF");
            tSocialSecurityFeeClaimDB.setFeeOperationType("CS");
            tSocialSecurityFeeClaimDB.setSurveyCaseNo(mSurveyCaseNo);
            tSocialSecurityFeeClaimDB.setSettlementNo(mSettlementNo);
            if(!tSocialSecurityFeeClaimDB.getInfo()){
            	buildError("submitData", "实付信息不存在！请重新选择结算信息");
            	return false;
            }
            
        }else if("QUERY||MAIN".equals(cOperate)){
        	mTransferData = (TransferData)cInputData.getObjectByObjectName(
            		"TransferData", 0);
        }        
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "SocialSecurityFeeBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
}
