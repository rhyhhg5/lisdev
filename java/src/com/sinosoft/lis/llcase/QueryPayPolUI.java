package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:承保暂交费功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class QueryPayPolUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public QueryPayPolUI() {}


 public static void main(String[] args)
  {
    LLClaimSchema tLLClaimSchema   = new LLClaimSchema();
    tLLClaimSchema.setRgtNo("00100020030510000019");
    VData tVData = new VData();
    tVData.addElement(tLLClaimSchema);
  // 数据传输
  QueryPayPolUI tQueryPayPolUI   = new QueryPayPolUI();
  tQueryPayPolUI.submitData(tVData,"QUERY||MAIN");

  // 准备传输数据 VData
    }





  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    QueryPayPolBL tQueryPayPolUIBL = new QueryPayPolBL();

    System.out.println("---UI BEGIN---");
    if (tQueryPayPolUIBL.submitData(cInputData,mOperate) == false)
    {
  		// @@错误处理
      this.mErrors.copyAllErrors(tQueryPayPolUIBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "tQueryPayPolUIBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
			mResult = tQueryPayPolUIBL.getResult();
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

/*  public static void main(String[] args)
  {
    RegisterUI tRegisterUI=new RegisterUI();
    VData tVData=new VData();
    LLRegisterSchema tLLRegisterSchema=new LLRegisterSchema();
    LLRegisterSet tLLRegisterSet=new LLRegisterSet();
    LLCaseSchema tLLCaseSchema=new LLCaseSchema();
    LLCaseSet tLLCaseSet=new LLCaseSet();
    LLRgtAffixSchema tLLRgtAffixSchema=new LLRgtAffixSchema();
    LLRgtAffixSet tLLRgtAffixSet=new LLRgtAffixSet();
    tLLRegisterSchema.setContNo("1");
    tLLRegisterSet.add(tLLRegisterSchema);
    tLLCaseSchema.setCustomerNo("1");
    tLLCaseSet.add(tLLCaseSchema);
    tLLRgtAffixSchema.setAffixSerialNo("1");
    tLLRgtAffixSchema.setAffixCode("1");
    tLLRgtAffixSet.add(tLLRgtAffixSchema);
    tVData.add(tLLRegisterSet);
    tVData.add(tLLCaseSet);
    tVData.add(tLLRgtAffixSet);
    tRegisterUI.submitData(tVData,"INSERT");
  }
*/
}