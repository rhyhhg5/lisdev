package com.sinosoft.lis.llcase;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.PEdorUWAppConfirmBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全人工核保</p>
 * <p>Description:保全降档或免责</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class LSubMultAndAmnt
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private LPEdorItemSet mLPEdorItemSet = null;

    private String mEdorNo = null;

    private MMap mMap = new MMap();

    /**
     * 构造
     * @param gi GlobalInput
     * @param edorNo String
     */
    public LSubMultAndAmnt(GlobalInput gi, String edorNo)
    {
        this.mGlobalInput = gi;
        this.mEdorNo = edorNo;
        this.mLPEdorItemSet = getLPEdorItemSet();
    }

    /**
     * 数据提交的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData()
    {
        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    private boolean checkData()
    {
        if (mLPEdorItemSet == null)
        {
            return false;
        }
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!reCalPrem())
        {
            return false;
        }
        return true;
    }

    /**
     * 返回Map
     * @return MMap
     */
    public MMap getMMap()
    {
        return mMap;
    }

    /**
     * 得到需要重算保费的项目
     * 这里由于设计问题LPEdorItem 表没有项目序号，无法从LPUWMaster表关联Item表
     * 所以只有用比较笨的方法来做。
     * 目前Item表的设计可以满足一般的保全变更需求，但如果要实现一个客户多个项目就有问题
     * 这里没有项目序号,而只有项目类型,而实际上一个受理中相同类型的项目可能有多个，
     * Item表中的DisplayType表明该项目是针对合同的，被保人的还是险种的
     * 但实际上现在没有针对险种的项目，针对被保人的有CM和BC，其他都是针对合同的
     * @return LPEdorItemSet
     */
    private LPEdorItemSet getLPEdorItemSet()
    {
    	LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
    	ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
       String sqlExData = "select edortype,insuredno from LPUWMaster where edorno='"+mEdorNo+"' group by insuredno,edortype";
       tSSRS = tExeSQL.execSQL(sqlExData);
       System.out.println("row=" + tSSRS.getMaxRow());
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
        	/**此处是数据校验Error1  和  Error7 查询出来的问题 由于此处按照 LPUWMaster的实际数量来进行循环
        	 * 因此造成了多次提交，对于万能险，由于存在lcinsureacctrace的生产，因此会使其产生多条的情况发生
        	 * 为了支持只选一次的情况因此特地添加了此程序  防止程序多次更新 2012-08-16 【OoO?】ytz
        	 */
            String tEdorTypeEx = tSSRS.GetText(i, 1);
            String tInsuredNoEx = tSSRS.GetText(i, 2);
            System.out.println("tEdorTypeEx=" + tEdorTypeEx);
            System.out.println("tInsuredNoEx=" + tInsuredNoEx);

            if ((tEdorTypeEx.equals(BQ.EDORTYPE_CM)) ||
                    (tEdorTypeEx.equals(BQ.EDORTYPE_BF)) ||
                    (tEdorTypeEx.equals(BQ.EDORTYPE_XB))||
                    (tEdorTypeEx.equals(BQ.EDORTYPE_NS))||
            	    (tEdorTypeEx.equals(BQ.EDORTYPE_TB)))
            {
                //这里如果LPEdorItem表查出多条记录才比较InsuredNo，就可以准确定位一个Item 因为此处Item可以有多条
                LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
                tLPEdorItemDB.setEdorNo(mEdorNo);
                tLPEdorItemDB.setEdorType(tEdorTypeEx);
                LPEdorItemSet edorItemSet = tLPEdorItemDB.query();
                LPEdorItemSchema tLPEdorItemSchema = null;
                if (edorItemSet.size() == 1) //当只有一条edoritem时，直接将值付给Set 2012-08-16 【OoO?】ytz
                {
                    tLPEdorItemSchema = edorItemSet.get(1);
                }
                else
                {
                    for (int j = 1; j <= edorItemSet.size(); j++)
                    {
                        if ((tInsuredNoEx != null) && (tInsuredNoEx.
                                equals(edorItemSet.get(j).getInsuredNo())))
                        {
                            tLPEdorItemSchema = edorItemSet.get(j);
                        }
                    }
                }
                if (tLPEdorItemSchema == null)
                {
                    mErrors.addOneError("未找到被保人" + tInsuredNoEx +
                            "申请的" + tEdorTypeEx + "项目！");
                    return null;
                }
                tLPEdorItemSet.add(tLPEdorItemSchema);
            }
        }
        return tLPEdorItemSet;
    }

    /**
     * 重算保费，从LPUWMaster表取出加费的项目
     * @return boolean
     */
    private boolean reCalPrem()
    {
        for (int i = 1; i <= mLPEdorItemSet.size(); i++)
        {
            LPEdorItemSchema tLPEdorItemSchema = mLPEdorItemSet.get(i);
            String edorType = tLPEdorItemSchema.getEdorType();
            if (edorType == null || (edorType.equals("")))
            {
                mErrors.addOneError("缺少项目类型代码！");
                return false;
            }
            String sql = "delete from LJSGetendorse " +
                    "where EndorseMentNo = '" +  tLPEdorItemSchema.getEdorNo() + "' " +
                    "and FeeOperationType = '" + tLPEdorItemSchema.getEdorType() + "' " +
                    "and ContNo = '" + tLPEdorItemSchema.getContNo() + "' ";
            mMap.put(sql, "DELETE");
            System.out.println(sql);
            //如果是像CM一样会重算保费的项目则调理算类，否则要另外加算法
            if(edorType.equals(BQ.EDORTYPE_CM)
                    || edorType.equals(BQ.EDORTYPE_BF)
                    || edorType.equals(BQ.EDORTYPE_XB)
                    || edorType.equals(BQ.EDORTYPE_NS))
            {
                if (!reCalOneItem(tLPEdorItemSchema))
                {
                    return false;
                }
            }
            else
            {
                if (!reCalPrem(tLPEdorItemSchema))
                {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 计算一个项目
     * @param aLPEdorItemSchema LPEdorItemSchema
     * @return boolean
     */
    private boolean reCalOneItem(LPEdorItemSchema aLPEdorItemSchema)
    {
        String edorType = aLPEdorItemSchema.getEdorType();
        try
        {
        	
            //按项目编码调用不同的理算类
        	LPEdorXBAppConfirmBL tLPEdorXBAppConfirmBL = new LPEdorXBAppConfirmBL();

            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue(BQ.EDORTYPE_UW, BQ.EDORTYPE_UW);  //可以在项目理算类中知道是否核保调用

            VData data = new VData();
            data.add(mGlobalInput);
            data.add(aLPEdorItemSchema);
            data.add(tTransferData);
            if (!tLPEdorXBAppConfirmBL.submitData(data, "APPCONFIRM||" + edorType))
            {
                mErrors.copyAllErrors(tLPEdorXBAppConfirmBL.mErrors);
                return false;
            }
            VData ret = tLPEdorXBAppConfirmBL.getResult();
            MMap map = (MMap) ret.getObjectByObjectName("MMap", 0);
            if (map == null)
            {
                mErrors.addOneError(edorType + "项目理算失败!");
                return false;
            }
            mMap.add(map);
        }
        catch (Exception ex)
        {
            System.out.println(aLPEdorItemSchema.getEdorType() +
                    "项目没有保全理算类!");
        }
        
        return true;
    }

    /**
     * 重算保费
     * @return boolean
     */
    private boolean reCalPrem(LPEdorItemSchema aLPEdorItemSchema)
    {
    	PEdorUWAppConfirmBL tPEdorUWAppConfirmBL = new PEdorUWAppConfirmBL();
        VData data = new VData();
        data.add(mGlobalInput);
        data.add(aLPEdorItemSchema);
        if (!tPEdorUWAppConfirmBL.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPEdorUWAppConfirmBL.mErrors);
            return false;
        }
        VData ret = tPEdorUWAppConfirmBL.getResult();
        MMap map = (MMap) ret.getObjectByObjectName("MMap", 0);
        if (map == null)
        {
            mErrors.addOneError(tPEdorUWAppConfirmBL + "项目理算失败!");
            return false;
        }
        mMap.add(map);
        return true;
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
