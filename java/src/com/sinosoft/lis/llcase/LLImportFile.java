package com.sinosoft.lis.llcase;

import java.io.*;
import org.w3c.dom.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.agent.LARecomInterface;
import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Excel文件解析类</p>
 * <p>Description: 把数据从磁盘中的XML文件导入，存到相应的类中 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author Xx
 * @version 1.0
 */

public class LLImportFile {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 保单信息在xml中的结点 */
    private static final String PARSE_PATH = "/DATASET";
    /** 配置文件名 */
    private String Config_File_Name = "LLSCCaseImport.xml";
    /** 文件路径 */
    private String path;
    /** Excel文件名 */
    private String fileName;
    private String mRgtNo;
    private GlobalInput mG = new GlobalInput();
    private String[] mTitle = null;
    private SIParser tCaseParser ;
    private String DistrictSign = "";

    /**
     * 构造函数
     * @param path String
     * @param fileName String
     */
    public LLImportFile(String path, String fileName,String aRgtNo,
    		GlobalInput aGlobalInput,String aImportFlag) {
        this.path = path;
        this.fileName = fileName;
        this.mRgtNo = aRgtNo;
        this.mG = aGlobalInput;
        String tOtherSign = mRgtNo.substring(1,3);

        //导入标志 目前为批量受理导入
        if (aImportFlag!=null&&!"".equals(aImportFlag)) {
            tOtherSign = aImportFlag;
        }

        String SQL="select code,codealias from ldcode1 where codetype='llimport' "
        	+" and code1='SI' and othersign='"+tOtherSign+"'";
        ExeSQL texesql = new ExeSQL();
        System.out.println(SQL);
        SSRS tss = texesql.execSQL(SQL);
        if(tss.getMaxRow()>0){
        	DistrictSign = tss.GetText(1, 1);
        	Config_File_Name = tss.GetText(1, 2);
        }
    }

    /**
     * 从Excel文件导入数据，保存存到List中<br>
     * 先把数据保存到多个xml文件中，然后从xml读出存到VData中
     * @return VData 保存数据的容器
     */
    public VData doImport() {
        String[] xmlFiles = parseVts(); //解析Excel到xml
        if (xmlFiles == null) {
            return null;
        }

        VData data = new VData();
//        for (int i = 0; i < xmlFiles.length; i++) {
            data=parseXml(xmlFiles[0]); //解析xml到VData
//        }
        return data;
    }

    /**
     * 得到Excel文件路径
     * @return String
     */
    private String getFilePath() {
        return path + File.separator + fileName;
    }

    /**
     * 得到配置文件路径
     * @return String
     */
    private String getConfigFilePath() {
        return path + File.separator + Config_File_Name;
    }

    /**
     * 解析excel并转换成多个xml文件
     * @return String[] 存放xml文件名的数组
     */
    private String[] parseVts() {
        SIXmlParser rgtvp = new SIXmlParser();
        rgtvp.setFileName(getFilePath());
        rgtvp.setConfigFileName(getConfigFilePath());
        //转换excel到xml
        if (!rgtvp.transform()) {
            mErrors.copyAllErrors(rgtvp.mErrors);
            return null;
        }
        mTitle = rgtvp.getTitle();
        return rgtvp.getDataFiles();
    }

    private VData parseXml(String xmlFileName) {
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if (!tLLRegisterDB.getInfo()) {
            CError.buildErr(this,"磁盘导入失败，未查到团体批次信息！");
        }

        try {
            System.out.println("System.out.printlnSystem.out.printlnSystem.out.println="+DistrictSign);
            Class tParserClass = Class.forName(
                    "com.sinosoft.lis.llcase." + DistrictSign + "SCParser");
                    System.out.println(tParserClass);
            tCaseParser = (SIParser) tParserClass.newInstance();
        } catch (ClassNotFoundException ex) {
            System.out.println(
                    ".........Not Found " + DistrictSign + "SCParser，错误");
            System.out.println("aaaaaaaaaaaaaaaa");
            tCaseParser = new SCParser();
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError.buildErr(this, "找不到合适的解析类！请联系开发人员。");
            return null;
        }
        tCaseParser.setParam(mRgtNo,tLLRegisterDB.getRgtObjNo(),path);
        tCaseParser.setGlobalInput(mG);
        tCaseParser.setFormTitle(mTitle,DistrictSign+"ClaimErrList.vts");
        VData tVData = new VData();
        try {
            //解析保单信息
            XMLPathTool xmlPT = new XMLPathTool(xmlFileName);
            NodeList nodeList = xmlPT.parseN(PARSE_PATH);
            System.out.println("长度 ： " + nodeList.getLength());
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                NodeList contNodeList = node.getChildNodes();
                System.out.println("长度 ： " + contNodeList.getLength());
                if (contNodeList.getLength() <= 0)
                    continue;
                for (int j = 0; j < contNodeList.getLength(); j++) {

                    Node contNode = contNodeList.item(j);
                    String nodeName = contNode.getNodeName();
                    if (nodeName.equals("#text")) {
                    }
                    //解析被保险人
                    else if (nodeName.equals("CASETABLE")) {
                        tVData = tCaseParser.parseOneInsuredNode(contNode);
                    }
                }
            }

            //解析完成删除xml临时文件
            File xmlFile = new File(xmlFileName);
            xmlFile.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tVData;
    }


    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        tG.ComCode = "8694";
        tG.Operator = "cm9402";
        tG.ManageCom = "8694";
        LLImportFile tLLimportFile = new LLImportFile(
                "D:\\DEVELOP\\ui\\temp_lp", "data.xls","P9400060925000001",tG,"01");
        tLLimportFile.doImport();
    }
}
