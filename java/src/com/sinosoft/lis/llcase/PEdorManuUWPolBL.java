package com.sinosoft.lis.llcase;


import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 保全人工核保</p>
 * <p>Description:保存个人保全人工核保险种结论,针对每个项目下的险种下结论</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author QiuYang
 * @version 1.0
 */

public class PEdorManuUWPolBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mEdorNo = null;

    private String mEdorType = null;

    private String mPolNo = null;

    private LPUWMasterSchema mLPUWMasterSchema = null;
    private LGLetterSchema mLGLetterSchema = new LGLetterSchema();

    private LPPremSchema mLPPremSchema = null;

    private LPSpecSet mLPSpecSet = null;

    private EdorItemSpecialData mSpecialData = null;

    private String mEdorValiDate = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /**
     * 数据提交的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData data)
    {
        if (!getInputData(data))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }
        return true;
    }

    /**
     * 得到外部传入的数据
     * @return boolean
     */
    private boolean getInputData(VData data)
    {
        mGlobalInput = (GlobalInput) data.getObjectByObjectName(
                "GlobalInput", 0);
        mLPUWMasterSchema = (LPUWMasterSchema) data.getObjectByObjectName(
                "LPUWMasterSchema", 0);
        mLPPremSchema = (LPPremSchema) data.getObjectByObjectName(
                "LPPremSchema", 0);
        mLPSpecSet = (LPSpecSet) data.getObjectByObjectName(
                "LPSpecSet", 0);
        mSpecialData = (EdorItemSpecialData) data.getObjectByObjectName(
                "EdorItemSpecialData", 0);
        try
        {
            this.mEdorNo = mLPUWMasterSchema.getEdorNo();
            this.mEdorType = mLPUWMasterSchema.getEdorType();
            this.mPolNo = mLPUWMasterSchema.getPolNo();
            this.mEdorValiDate = mSpecialData.getEdorValue("EdorValiDate");

        }
        catch (Exception ex)
        {
            mErrors.addOneError("传入参数错误！");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    private boolean checkData()
    {
        String passFlag = mLPUWMasterSchema.getPassFlag();
        String edorType = mLPUWMasterSchema.getEdorType();
        if ((passFlag == null) || (passFlag.equals("")))
        {
            mErrors.addOneError("核保结论录入日错误！");
            return false;
        }
        if ((edorType == null) || (edorType.equals("")))
        {
            mErrors.addOneError("保全项目类型错误！");
            return false;
        }
        if (passFlag.equals(BQ.PASSFLAG_STOP))
        {
            if (edorType.equals("TB"))
            {
                mErrors.addOneError("投保事项变更项目不能终止申请！");
                return false;
            }
        }
        if (passFlag.equals(BQ.PASSFLAG_CONDITION))
        {
            String disagreeDeal = mLPUWMasterSchema.getDisagreeDeal();
            if ((disagreeDeal != null) && (disagreeDeal.equals("1")))
            {
                if (edorType.equals("TB"))
                {
                    mErrors.addOneError("投保事项变更项目不能终止申请！");
                    return false;
                }
            }
        }
        if(!checkByItem())
        {
            return false;
        }

        return true;
    }

    /**
     * 按项目校验录入数据的合法性
     * @return boolean:通过true,否则false
     */
    private boolean checkByItem()
    {
        String passFlag = mLPUWMasterSchema.getPassFlag();
        String edorType = mLPUWMasterSchema.getEdorType();

        if(edorType.equals(BQ.EDORTYPE_NS))
        {
            if(passFlag.equals(BQ.PASSFLAG_CANCEL))
            {
                mErrors.addOneError(edorType + "核保结论不能为险种解约");
                return false;
            }

            if(passFlag.equals(BQ.PASSFLAG_CONDITION))
            {
                String disagreeDeal = mLPUWMasterSchema.getDisagreeDeal();
                if((disagreeDeal != null) && !disagreeDeal.equals("1"))
                {
                    mErrors.addOneError(edorType + "项目客户不同意处理只能终止申请！");
                    return false;
                }
            }
        }
        
        if(edorType.equals(BQ.EDORTYPE_NS))
        {
            if(passFlag.equals(BQ.PASSFLAG_CANCEL))
            {
                mErrors.addOneError(edorType + "核保结论不能为险种解约");
                return false;
            }

            if(passFlag.equals(BQ.PASSFLAG_CONDITION))
            {
                String disagreeDeal = mLPUWMasterSchema.getDisagreeDeal();
                if((disagreeDeal != null) && !disagreeDeal.equals("1"))
                {
                    mErrors.addOneError(edorType + "项目客户不同意处理只能终止申请！");
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData()
    {
        if (!setAddition())
        {
            return false;
        }
        setLPUWMaster();
        setLPUWSub();
        setLGLetter();
        setEdorValiDate();
        return true;
    }

    /**
     * 生成核保轨迹号
     * @return String
     */
    private String createUWNo()
    {
        String sql = "select UWNo + 1 from LPUWMaster " +
                "where EdorNo = '" + mEdorNo  + "' " +
                "and EdorType = '" + mEdorType + "' " +
                "and PolNo = '" + mPolNo + "'";
        String uwNo = (new ExeSQL()).getOneValue(sql);
        if (uwNo.equals(""))
        {
            return "1";
        }
        return uwNo;
    }
    
    
    private void setLGLetter()
    {
       
    	mLGLetterSchema.setEdorAcceptNo(mEdorNo);
    	mLGLetterSchema.setSerialNumber("1");
    	mLGLetterSchema.setLetterType("0");
    	mLGLetterSchema.setState("0");
    	mLGLetterSchema.setOperator(mGlobalInput.Operator);
    	mLGLetterSchema.setMakeDate(mCurrentDate);
    	mLGLetterSchema.setMakeTime(mCurrentTime);
    	mLGLetterSchema.setModifyDate(mCurrentDate);
    	mLGLetterSchema.setModifyTime(mCurrentTime);
        mMap.put(mLGLetterSchema, "DELETE&INSERT");
    }

    /**
     * 保存核保结论
     * @return boolean
     */
    private void setLPUWMaster()
    {
        String passFlag = mLPUWMasterSchema.getPassFlag();
        if (!passFlag.equals(BQ.PASSFLAG_CONDITION))
        {
            mLPUWMasterSchema.setAddPremFlag(null);
            mLPUWMasterSchema.setSpecFlag(null);
            mLPUWMasterSchema.setSubAmntFlag(null);
            mLPUWMasterSchema.setSubMultFlag(null);
            mLPUWMasterSchema.setDisagreeDeal(null);
            String payPlanCode = mLPUWMasterSchema.getPayPlanCode();
            //删除加费信息
            String sql = "delete from LPPrem " +
                    "where EdorNo = '" + mEdorNo + "' " +
                    "and EdorType = '" + mEdorType + "' " +
                    "and PolNo = '" + mPolNo + "' " +
                    "and PayPlanCode = '" + payPlanCode + "' ";
            mMap.put(sql, "DELETE");
            //删除免责信息
            sql = "delete from LPSpec " +
                    "where EdorNo = '" + mEdorNo + "' " +
                    "and EdorType = '" + mEdorType + "' " +
                    "and PolNo = '" + mPolNo + "' ";
             mMap.put(sql, "DELETE");
        }
        System.out.println("setLPUWMaster");
        System.out.println("InsuredNo" + mLPUWMasterSchema.getInsuredNo());
        String appntName = CommonBL.getAppntName(mLPUWMasterSchema.getAppntNo());
        String insuredName = CommonBL.getInsuredName(mLPUWMasterSchema.getInsuredNo());
        mLPUWMasterSchema.setAppntName(appntName);
        mLPUWMasterSchema.setInsuredName(insuredName);
        mLPUWMasterSchema.setGrpContNo(BQ.GRPFILLDATA);
        mLPUWMasterSchema.setProposalContNo(mLPUWMasterSchema.getContNo());
        mLPUWMasterSchema.setProposalNo(mLPUWMasterSchema.getPolNo());
        mLPUWMasterSchema.setUWNo(createUWNo());
        mLPUWMasterSchema.setAutoUWFlag("2"); //人工核保
        mLPUWMasterSchema.setOperator(mGlobalInput.Operator);
        mLPUWMasterSchema.setMakeDate(mCurrentDate);
        mLPUWMasterSchema.setMakeTime(mCurrentTime);
        mLPUWMasterSchema.setModifyDate(mCurrentDate);
        mLPUWMasterSchema.setModifyTime(mCurrentTime);
        mMap.put(mLPUWMasterSchema, "DELETE&INSERT");
    }

    /**
     * 设置核保轨迹
     * @return boolean
     */
    private void setLPUWSub()
    {
        LPUWSubSchema tLPUWSubSchema = new LPUWSubSchema();
        Reflections ref = new Reflections();
        ref.transFields(tLPUWSubSchema, mLPUWMasterSchema);
        mMap.put(tLPUWSubSchema, "DELETE&INSERT");
    }

    /**
     * 保存附加条件
     * @return boolean
     */
    private boolean setAddition()
    {
        //有附加条件
        String passFlag = mLPUWMasterSchema.getPassFlag();
        if ((passFlag != null) && (passFlag.equals(BQ.PASSFLAG_CONDITION)))
        {
            if (!addPrem())
            {
                return false;
            }
            System.out.println("addPrem");
            if (!addSpec())
            {
                return false;
            }
            System.out.println("addSpec");
            if (!subMultAndAmnt())
            {
                return false;
            }
            System.out.println("subMultAndAmnt");
        }
        return true;
    }

    /**
     * 判断是否有相应附加条件标志
     * @param flag String
     * @return boolean
     */
    private boolean hasFlag(String flag)
    {
        if ((flag != null) && (flag.equals("1")))
        {
            return true;
        }
        return false;
    }

    /**
     * 加费
     * @return boolean
     */
    private boolean addPrem()
    {
        String addPremFlag = mLPUWMasterSchema.getAddPremFlag();
        if (hasFlag(addPremFlag))
        {
            VData data = new VData();
            data.add(mGlobalInput);
            data.add(mLPPremSchema);
            AddPrem addPrem = new AddPrem();
            if (!addPrem.submitData(data))
            {
                mErrors.copyAllErrors(addPrem.mErrors);
                return false;
            }
            mMap.add(addPrem.getMMap());
            mLPUWMasterSchema.setPayPlanCode(addPrem.getPayPlanCode());
        }
        return true;
    }

    /**
     * 免责
     * @return boolean
     */
    private boolean addSpec()
    {
        String specFlag = mLPUWMasterSchema.getSpecFlag();
        if (hasFlag(specFlag))
        {
            VData data = new VData();
            data.add(mGlobalInput);
            data.add(mLPSpecSet);
            AddSpec tAddSpec = new AddSpec();
            MMap map = tAddSpec.getSubmitData(data);
            if (map == null)
            {
                mErrors.copyAllErrors(tAddSpec.mErrors);
                return false;
            }
            mMap.add(map);
        }
        return true;
    }

    /**
     * 降档或减额，两个互斥，只算一个
     * @return boolean
     */
    private boolean subMultAndAmnt()
    {
        String subMultFlag = mLPUWMasterSchema.getSubMultFlag();
        String subAmntFlag = mLPUWMasterSchema.getSubAmntFlag();
        double mult = mLPUWMasterSchema.getMult();
        double amnt = mLPUWMasterSchema.getAmnt();

        DetailDataQuery query = new DetailDataQuery(mEdorNo, mEdorType);
        LPPolSchema tLPPolSchema = (LPPolSchema)
                query.getDetailData("LCPol", mPolNo);
        if (hasFlag(subMultFlag))
        {
           tLPPolSchema.setMult(mult);
        }
        if (hasFlag(subAmntFlag))
        {
            tLPPolSchema.setAmnt(amnt);
        }
        tLPPolSchema.setOperator(mGlobalInput.Operator);
        tLPPolSchema.setModifyDate(mCurrentDate);
        tLPPolSchema.setModifyTime(mCurrentTime);
        mMap.put(tLPPolSchema, "DELETE&INSERT");
        return true;
    }

    private void setEdorValiDate()
    {
        mMap.put(mSpecialData.getSpecialDataSet(), "DELETE&INSERT");
    }

    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
