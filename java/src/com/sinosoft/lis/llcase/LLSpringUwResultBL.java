package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: LLSpringUwResultBL </p>
 * <p>Description: LLSpringUwResultBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author:Xx
 * @CreateDate：2005-02-18 13:58:55
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Date;
import com.sinosoft.lis.bl.LCPolBL;

public class LLSpringUwResultBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();


    /** 全局数据 */
    private GlobalInput mG = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private String mCaseNo;
    private String mRgtNo;
    private String mClmNo;
     private String mGrpContNo;

    /** 业务处理相关变量 */
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
    private LLSpringGiveResultSchema mLLSpringGiveResultSchema = new LLSpringGiveResultSchema();
    private LLClaimDetailSchema mLLClaimDetailSchema = new LLClaimDetailSchema();
    private LLClaimSchema mLLClaimSchema = new LLClaimSchema();
    private LJAGetClaimSchema mLJAGetClaimSchema = new LJAGetClaimSchema();
    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
    LLClaimUWMainSchema mLLClaimUWMainSchema = new LLClaimUWMainSchema();


    public LLSpringUwResultBL() {
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * @cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        if (!dealHandler())
            return false;
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError.buildErr(this, "数据处理失败LLSpringUwResultBL-->dealData!");
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;

            System.out.println("Start LLSpringUwResultBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                CError.buildErr(this, "数据提交失败!");
                return false;
            }
            System.out.println("End LLSpringUwResultBL Submit...");

        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
     private boolean getInputData(VData cInputData) {
        System.out.println("--Star GetInputData--");
        mLLClaimUWMainSchema = (LLClaimUWMainSchema) cInputData.
                              getObjectByObjectName("LLClaimUWMainSchema", 0);
        this.mG.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        if (mG.ManageCom.length() == 2)
            mG.ManageCom = mG.ManageCom + "000000";
        if (mG.ManageCom.length() == 4)
            mG.ManageCom = mG.ManageCom + "0000";
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("---Star dealData---mOperate---" + mOperate);
        if(mLLClaimUWMainSchema.getRgtNo()==null||mLLClaimUWMainSchema.getRgtNo().trim()
                                                                            .equals(""))
        {
            CError.buildErr(this, "没有传入相应的批次号!");
            return false;
        }
        mRgtNo=mLLClaimUWMainSchema.getRgtNo();
        LLCaseDB tLLCaseDB = new LLCaseDB();
        LLCaseSet tLLCaseSet = new LLCaseSet();
        tLLCaseDB.setRgtNo(mRgtNo);
        tLLCaseSet=tLLCaseDB.query();
        mLLCaseSchema=tLLCaseSet.get(1);
        mCaseNo=mLLCaseSchema.getCaseNo();

        mLLCaseSchema.setUWer(mG.Operator);//设置案件的理算人员为当前操作人
        mLLCaseSchema.setRgtState("11");      //设置案件状态为理算状态
        mLLCaseSchema.setUWDate(PubFun.getCurrentDate());
        mLLCaseSchema.setEndCaseDate(PubFun.getCurrentDate());
        mLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
        mLLCaseSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLLCaseSchema, "UPDATE");

        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        tLLRegisterDB.getInfo();
        mLLRegisterSchema=tLLRegisterDB.getSchema();
        mGrpContNo=mLLRegisterSchema.getRgtObjNo();
        mLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
        mLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());
        mLLRegisterSchema.setRgtState("05");
        map.put(mLLRegisterSchema, "UPDATE");


        //插入案件轨迹
        mLLCaseOpTimeSchema.setCaseNo(mCaseNo);
        mLLCaseOpTimeSchema.setRgtState("09");
        mLLCaseOpTimeSchema.setSequance(1);
        mLLCaseOpTimeSchema.setStartDate(PubFun.getCurrentDate());
        mLLCaseOpTimeSchema.setStartTime(PubFun.getCurrentTime());
        mLLCaseOpTimeSchema.setOpTime("1");
        mLLCaseOpTimeSchema.setEndDate(PubFun.getCurrentDate());
        mLLCaseOpTimeSchema.setEndTime(PubFun.getCurrentTime());
        mLLCaseOpTimeSchema.setOperator(mG.Operator);
        mLLCaseOpTimeSchema.setManageCom(mG.ManageCom);
        map.put(mLLCaseOpTimeSchema, "INSERT");

//取对应记录的llclaim表
        LLClaimDB tLLClaimDB = new LLClaimDB();
        LLClaimSet tLLClaimSet = new LLClaimSet();
        tLLClaimDB.setRgtNo(mRgtNo);
        tLLClaimSet=tLLClaimDB.query();
        mLLClaimSchema=tLLClaimSet.get(1);
        mClmNo=mLLClaimSchema.getClmNo();
//取对应记录的llclaimdetail表
        LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
        LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
        tLLClaimDetailDB.setRgtNo(mRgtNo);
        tLLClaimDetailSet = tLLClaimDetailDB.query();
        mLLClaimDetailSchema = tLLClaimDetailSet.get(1);




        String sNoLimit = PubFun.getNoLimit(mG.ManageCom);
        String ActuGetNo = PubFun1.CreateMaxNo("ACTUGETNO", sNoLimit);
        String strLimit = PubFun.getNoLimit(mG.ManageCom);
        String tGetNoticeNo = PubFun1.CreateMaxNo("GetNoticeNo", strLimit);
        String countSql = "select distinct agentcode,agentgroup from lcgrppol where grpcontno='"
                        +mGrpContNo+"'";
        ExeSQL exeSQL = new ExeSQL();
        SSRS ssrs = exeSQL.execSQL(countSql);


        mLJAGetSchema.setActuGetNo(ActuGetNo);
        mLJAGetSchema.setOtherNo(mRgtNo);
        mLJAGetSchema.setOtherNoType("5");
        mLJAGetSchema.setAgentCode(ssrs.GetText(1, 1));
        mLJAGetSchema.setAgentGroup(ssrs.GetText(1, 2));
        mLJAGetSchema.setGetNoticeNo(tGetNoticeNo);
        mLJAGetSchema.setSumGetMoney(mLLClaimSchema.getRealPay());
        mLJAGetSchema.setDrawer(mLLRegisterSchema.getRgtantName());
        mLJAGetSchema.setDrawerID(mLLRegisterSchema.getIDNo());
        mLJAGetSchema.setPayMode(mLLRegisterSchema.getGetMode());
        mLJAGetSchema.setAccName(mLLRegisterSchema.getAccName());
        mLJAGetSchema.setBankAccNo(mLLRegisterSchema.getBankAccNo());
        mLJAGetSchema.setBankCode(mLLRegisterSchema.getBankCode());
        mLJAGetSchema.setOperator(mG.Operator);
        mLJAGetSchema.setManageCom(mLLCaseSchema.getMngCom());
        mLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
        mLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
        mLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
        mLJAGetSchema.setShouldDate(PubFun.getCurrentDate());
        mLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLJAGetSchema, "INSERT");


        mLJAGetClaimSchema.setActuGetNo(ActuGetNo);
        mLJAGetClaimSchema.setFeeFinaType("YL");
        mLJAGetClaimSchema.setFeeOperationType("100");
        mLJAGetClaimSchema.setOtherNo(mRgtNo);
        mLJAGetClaimSchema.setOtherNoType("5");
        mLJAGetClaimSchema.setGetDutyCode(mLLClaimDetailSchema.getGetDutyCode());
        mLJAGetClaimSchema.setGetDutyKind(mLLClaimDetailSchema.getGetDutyKind());
        mLJAGetClaimSchema.setGrpContNo(mLLClaimDetailSchema.getGrpContNo());
        mLJAGetClaimSchema.setContNo(mLLClaimDetailSchema.getContNo());
        mLJAGetClaimSchema.setGrpPolNo(mLLClaimDetailSchema.getGrpPolNo());
        mLJAGetClaimSchema.setPolNo(mLLClaimDetailSchema.getPolNo());
        mLJAGetClaimSchema.setKindCode(mLLClaimDetailSchema.getKindCode());
        mLJAGetClaimSchema.setRiskCode(mLLClaimDetailSchema.getRiskCode());
        mLJAGetClaimSchema.setRiskVersion(mLLClaimDetailSchema.getRiskVer());
        mLJAGetClaimSchema.setAgentCode(ssrs.GetText(1, 1));
        mLJAGetClaimSchema.setAgentGroup(ssrs.GetText(1, 2));
        mLJAGetClaimSchema.setPay(mLLClaimDetailSchema.getRealPay());
        mLJAGetClaimSchema.setManageCom(mLLCaseSchema.getMngCom());
        mLJAGetClaimSchema.setGetNoticeNo(tGetNoticeNo);
        mLJAGetClaimSchema.setOperator(mG.Operator);
        mLJAGetClaimSchema.setManageCom(mLLCaseSchema.getMngCom());
        mLJAGetClaimSchema.setMakeDate(PubFun.getCurrentDate());
        mLJAGetClaimSchema.setMakeTime(PubFun.getCurrentTime());
        mLJAGetClaimSchema.setModifyDate(PubFun.getCurrentDate());
        mLJAGetClaimSchema.setModifyTime(PubFun.getCurrentTime());
        mLJAGetClaimSchema.setOPConfirmCode(mG.Operator);
        mLJAGetClaimSchema.setOPConfirmDate(PubFun.getCurrentDate());
        mLJAGetClaimSchema.setOPConfirmTime(PubFun.getCurrentTime());
        map.put(mLJAGetClaimSchema, "INSERT");

        //插入弹性拨付表
        LLSpringGiveResultDB tLLSpringGiveResultDB = new LLSpringGiveResultDB();
        tLLSpringGiveResultDB.setCaseNo(mLLCaseSchema.getCaseNo());
        tLLSpringGiveResultDB.setRgtNo(mLLCaseSchema.getRgtNo());
        tLLSpringGiveResultDB.getInfo();
        mLLSpringGiveResultSchema=tLLSpringGiveResultDB.getSchema();

        mLLSpringGiveResultSchema.setMngCom(mG.ManageCom);
        mLLSpringGiveResultSchema.setUWDate(PubFun.getCurrentDate());
        mLLSpringGiveResultSchema.setUWTime(PubFun.getCurrentTime());
        map.put(mLLSpringGiveResultSchema, "UPDATE");

        //插入审批表
        LLClaimUserSchema tLLClaimUserSchema= new LLClaimUserSchema();
        LLClaimUserDB tLLClaimUserDB= new LLClaimUserDB();
        tLLClaimUserDB.setUserCode(mG.Operator);
        tLLClaimUserDB.getInfo();
        tLLClaimUserSchema=tLLClaimUserDB.getSchema();
        String mClaimPopedom = tLLClaimUserSchema.getClaimPopedom();
        String mUpUserCode = tLLClaimUserSchema.getUpUserCode();

        mLLClaimUWMainSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        mLLClaimUWMainSchema.setClmUWer(mG.Operator);
        mLLClaimUWMainSchema.setClmUWGrade(mClaimPopedom);
        mLLClaimUWMainSchema.setAppClmUWer(mUpUserCode);
        mLLClaimUWMainSchema.setClmNo(mClmNo);
        mLLClaimUWMainSchema.setcheckDecision1(mLLClaimUWMainSchema.getcheckDecision2());
        mLLClaimUWMainSchema.setRemark1(mLLClaimUWMainSchema.getRemark2());
        mLLClaimUWMainSchema.setOperator(mG.Operator);
        mLLClaimUWMainSchema.setMngCom(mG.ManageCom);
        mLLClaimUWMainSchema.setMakeDate(PubFun.getCurrentDate());
        mLLClaimUWMainSchema.setMakeTime(PubFun.getCurrentTime());
        mLLClaimUWMainSchema.setModifyDate(PubFun.getCurrentDate());
        mLLClaimUWMainSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLLClaimUWMainSchema, "DELETE&INSERT");
        //插入审核明细表
        String countSql2 = "select count(*) from LLClaimUWMDetail where caseno ='"
                        +mCaseNo+"'";
                ExeSQL texesql2 = new ExeSQL();
                String casecount = texesql2.getOneValue(countSql2);
                int count = Integer.parseInt(casecount);

        LLClaimUWMDetailSchema aClmUWMDetailSchema = new LLClaimUWMDetailSchema();
        aClmUWMDetailSchema.setRemark(mLLClaimUWMainSchema.getRemark2());
        aClmUWMDetailSchema.setClmUWNo(String.valueOf(count));
        aClmUWMDetailSchema.setRgtNo(mRgtNo);
        aClmUWMDetailSchema.setCaseNo(mCaseNo);
        aClmUWMDetailSchema.setClmNo(mClmNo);
        aClmUWMDetailSchema.setClmDecision("1");
        aClmUWMDetailSchema.setAppClmUWer(mUpUserCode);
        aClmUWMDetailSchema.setClmUWer(mG.Operator);
        aClmUWMDetailSchema.setClmUWGrade(mClaimPopedom);
        aClmUWMDetailSchema.setOperator(mG.Operator);
        aClmUWMDetailSchema.setMngCom(mG.ManageCom);
        aClmUWMDetailSchema.setMakeDate(PubFun.getCurrentDate());
        aClmUWMDetailSchema.setMakeTime(PubFun.getCurrentTime());
        aClmUWMDetailSchema.setModifyDate(PubFun.getCurrentDate());
        aClmUWMDetailSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(aClmUWMDetailSchema, "INSERT");


        return true;
}



    /**
     * 案件分配
     * @return boolean
     */
    private boolean dealHandler() {
//        LLCaseCommon tLLCaseCommon = new LLCaseCommon();
//        String strMngCom;
//        SimpleCase="";
//
//        String tcaseprop = mLLCaseSchema.getCaseProp()+"";
//        System.out.println("案件属性"+tcaseprop);
//        if(tcaseprop.equals("06"))
//            SimpleCase = "01";
//        else
//            SimpleCase = "02";
//
//        strMngCom = mG.ManageCom;
//        strHandler = tLLCaseCommon.ChooseAssessor(strMngCom, SimpleCase);
//        if (strHandler == null || strHandler.equals("")) {
//            CError tError = new CError();
//            tError.moduleName = "LLSpringUwResultBL";
//            tError.functionName = "dealHandler";
//            tError.errorMessage = "机构" + strMngCom + "没有符合条件的理赔人!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
        return true;
    }


    /**
     * 准备提交数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLLRegisterSchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLLCaseSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDDrugBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */


    public VData getResult() {
        return this.mResult;
    }
}
