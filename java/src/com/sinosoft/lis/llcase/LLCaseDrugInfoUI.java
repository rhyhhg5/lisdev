package com.sinosoft.lis.llcase;

import com.sinosoft.lis.vschema.LDPersonSet;
import com.sinosoft.lis.vschema.LLCaseDrugInfoSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LLCaseDrugInfoUI {
	private VData mInputData;
    private VData mResult = new VData();
    public CErrors mErrors = new CErrors();
    private LLCaseDrugInfoSet mLLCaseDrugInfoSet;
    public LLCaseDrugInfoUI()
    {
    }

    public static void main(String[] args)
    {
    }
    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        LLCaseDrugInfoBL tLLCaseDrugInfoBL = new LLCaseDrugInfoBL();
        tLLCaseDrugInfoBL.submitData(mInputData, cOperate);
        //如果有需要处理的错误，则返回
        if (tLLCaseDrugInfoBL.mErrors.needDealError())
        this.mErrors.copyAllErrors(tLLCaseDrugInfoBL.mErrors);
        //取得结果返回前台
        this.mResult.clear();
        this.mResult = tLLCaseDrugInfoBL.getResult();
        mInputData = null;
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
