/**
 * <p>Title: 业务系统--"立案"模块中实现修改和删除功能</p>
 * <p>Copyright: 2002-11-08
 * <p>Company: Sinosoft</p>
 * @author刘岩松
 * @version 1.0
 */

package com.sinosoft.lis.llcase;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;

public class RegisterDeleteBL
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData = new VData();
  private VData mResult = new VData();
  private String mOperate;
  private String t_caseno;


  private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
  public RegisterDeleteBL() {}

  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;
    System.out.println("----deleteData11---");

    mLLRegisterSchema = (LLRegisterSchema)mInputData.getObjectByObjectName("LLRegisterSchema",0);
    System.out.println("在RegisterDeleteBL中的立案号码是"+mLLRegisterSchema.getRgtNo());
    String t_sql = "select caseno from llcase where rgtno = '"
                 +mLLRegisterSchema.getRgtNo().trim()+"'";
    ExeSQL exeSQL = new ExeSQL();
    SSRS ssrs = exeSQL.execSQL(t_sql);
    int case_count = ssrs.getMaxRow();
    String operate_flag = "";
    if(case_count==0)
    {
      operate_flag = "N";
      t_caseno = "0000";
    }
    else
    {
      operate_flag = "Y";
      t_caseno = ssrs.GetText(1,1);
      System.out.println("分案号码是"+ssrs.GetText(1,1));
    }
    mInputData.clear();
    mInputData.addElement(operate_flag);
    mInputData.addElement(t_caseno);
    mInputData.add(mLLRegisterSchema);

    LLRegisterDB tLLRegisterDB = new LLRegisterDB();
    tLLRegisterDB.setRgtNo(mLLRegisterSchema.getRgtNo());
    if(!tLLRegisterDB.getInfo())
      return false;
    if(tLLRegisterDB.getClmState().equals("5"))
    {
      this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "RegisterUpdateBL";
      tError.functionName = "submitData";
      tError.errorMessage = "该案件正处于调查之中，您无法进行修改操作！！";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    System.out.println("案件状态为"+tLLRegisterDB.getClmState());
    System.out.println("该案件的结案标志是："+tLLRegisterDB.getEndCaseFlag());
    if(tLLRegisterDB.getEndCaseFlag().equals("2"))
    {
      this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "RegisterUpdateBL";
        tError.functionName = "submitData";
        tError.errorMessage = "该案件已经结案，您无法进行删除操作！！";
        this.mErrors .addOneError(tError) ;
        return false;
    }

    if (!deleteData())
      return false;
    System.out.println("----deleteData---");
    return true;
  }//end of submit;

  public VData getResult()
  {
    return mResult;
  }

  private boolean deleteData()
  {
    RegisterDeleteBLS tRegisterDeleteBLS = new RegisterDeleteBLS();
    if (!tRegisterDeleteBLS.submitData(mInputData,mOperate))
      return false;
    return true;
  }//end of deletedata;

}//end of RegisterDeleteBL