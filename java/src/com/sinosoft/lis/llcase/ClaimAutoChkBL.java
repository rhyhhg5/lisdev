package com.sinosoft.lis.llcase;

import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 理赔自动核赔业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 */
public class ClaimAutoChkBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  /** 往后面传输数据的容器 */
  private VData mInputData;

  /** 往界面传输数据的容器 */
  private VData mResult = new VData();

  /** 数据操作字符串 */
  private String mOperate;
  private String mpassflag; //通过标记
  private String mCalCode; //计算编码
  private double mValue;
  private int mCount;

  private String mClmNo = "";
  private GlobalInput mGlobalInput = new GlobalInput();
  private LCPolSchema mLCPolSchema = new LCPolSchema();
  private LLClaimSchema mLLClaimSchema = new LLClaimSchema();
  private LLClaimPolicySchema mLLClaimPolicySchema = new LLClaimPolicySchema();
  private LLClaimUnderwriteSchema mLLClaimUnderwriteSchema= new LLClaimUnderwriteSchema();
  private LLClaimErrorSet mLLClaimErrorSet = new LLClaimErrorSet();
  private LLClaimUnderwriteSet tLLClaimUnderwriteSet = new LLClaimUnderwriteSet();
  private LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();
  private LMUWSet mLMUWSet = new LMUWSet();
  private LMUWSchema mLMUWSchema = new LMUWSchema();
  private CalBase mCalBase = new CalBase();
  public ClaimAutoChkBL() {}

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    System.out.println("在ClaimSaveBL程序执行完毕后，要对保单进行自动核赔，具体调用的程序是ClaimAutoChkBL");
    this.mOperate=cOperate;
    mInputData = (VData)cInputData.clone();
    if (!getInputData(cInputData))
    {
      return false;
    }


    if (mOperate.equals("AutoChkPol"))
    {
      //保单自动核赔
      if (!dealData_Pol())
      {
        return false;
      }
    }
    else
    {
      //案件自动核赔
      if (!dealData_Case())
      {
        return false;
      }
    }
    mResult.clear();
    prepareOutputData();

    //数据提交
//    ClaimAutoChkBLS tClaimAutoChkBLS = new ClaimAutoChkBLS();
//    if (!tClaimAutoChkBLS.submitData(mInputData, mOperate))
//    {
//      // @@错误处理
//      this.mErrors.copyAllErrors(tClaimAutoChkBLS.mErrors);
//      CError tError = new CError();
//      tError.moduleName = "ClaimAutoChkBL";
//      tError.functionName = "submitData";
//      tError.errorMessage = "数据提交失败!";
//      this.mErrors .addOneError(tError) ;
//      return false;
//    }
    return true;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealData_Pol()
  {
   for ( int t=1;t<= tLLClaimUnderwriteSet.size();t++)
   {
       this.mLLClaimUnderwriteSchema = tLLClaimUnderwriteSet.get(t);
       //准备算法
       if (!CheckKinds())
       {
           return false;
       }

       int n = mLMUWSet.size();
       mCount = 0;
       if (n == 0)
       {
           mLLClaimUnderwriteSchema.setAutoClmDecision("1");
       }
       else
       {
           mLLClaimUnderwriteSchema.setAutoClmDecision("1");
           for (int i = 1; i <= n; i++)
           {
               //取计算编码
               mLMUWSchema = mLMUWSet.get(i);
               mCalCode = mLMUWSchema.getCalCode();
               CheckPol();
           }
       }
   }
    return true;
  }

  private boolean dealData_Case()
  {
    //准备算法
    if (!CheckCase())
    {
      return  false;
    }
    //案件核赔
    int n = mLMUWSet.size();
    for (int i = 1; i <= n; i++)
    {
      //取计算编码
      mLMUWSchema = mLMUWSet.get(i);
      mCalCode = mLMUWSchema.getCalCode();
      calCase();
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    mGlobalInput.setSchema(
      (GlobalInput)
      cInputData.getObjectByObjectName("GlobalInput",0));

    if (mOperate.equals("AutoChkPol"))
    {
      tLLClaimUnderwriteSet =
          (LLClaimUnderwriteSet)
          cInputData.getObjectByObjectName("LLClaimUnderwriteSet",0);
      //添加续保的处理方法保单表

   if ( tLLClaimUnderwriteSet== null )
   {
       CError.buildErr(this,"没有传入的核赔数据");
       return false;
   }
   for ( int t=1;t<= tLLClaimUnderwriteSet.size();t++)
   {
       mLLClaimUnderwriteSchema = tLLClaimUnderwriteSet.get(t);
       LCPolBL mLCPolBL = new LCPolBL();
       mLCPolBL.setPolNo(mLLClaimUnderwriteSchema.getPolNo());
       if (mLCPolBL.getInfo() == false)
       {
           // @@错误处理
           this.mErrors.copyAllErrors(mLCPolBL.mErrors);
           CError tError = new CError();
           tError.moduleName = "UWAutoChkBL";
           tError.functionName = "getInputData";
           tError.errorMessage = "LCPol和LBPol表查询失败!";
           this.mErrors.addOneError(tError);
           return false;
       }
       if (mLCPolBL.getPolNo() == null ||
           mLCPolBL.getPolNo().equals(""))
       {
           this.mErrors.copyAllErrors(mLCPolBL.mErrors);
           CError tError = new CError();
           tError.moduleName = "UWAutoChkBL";
           tError.functionName = "getInputData";
           tError.errorMessage = "LCPol和LBPol表查询失败!" +
                                 "保单号码是：" +
                                 mLLClaimUnderwriteSchema.getPolNo();
           this.mErrors.addOneError(tError);
           return false;
       }
       mLCPolSchema.setSchema(mLCPolBL.getSchema());

       mLLClaimUnderwriteSchema.setContNo(mLCPolSchema.getContNo());
       mLLClaimUnderwriteSchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
       mLLClaimUnderwriteSchema.setKindCode(mLCPolSchema.getKindCode());
       mLLClaimUnderwriteSchema.setRiskCode(mLCPolSchema.getRiskCode());
       mLLClaimUnderwriteSchema.setRiskVer(mLCPolSchema.getRiskVersion());
       mLLClaimUnderwriteSchema.setPolMngCom(mLCPolSchema.getManageCom());
       mLLClaimUnderwriteSchema.setSaleChnl(mLCPolSchema.getSaleChnl());
       mLLClaimUnderwriteSchema.setAgentCode(mLCPolSchema.getAgentCode());
       mLLClaimUnderwriteSchema.setAgentGroup(mLCPolSchema.
               getAgentGroup());
       mLLClaimUnderwriteSchema.setInsuredNo(mLCPolSchema.getInsuredNo());
       mLLClaimUnderwriteSchema.setInsuredName(mLCPolSchema.
               getInsuredName());
       mLLClaimUnderwriteSchema.setAppntNo(mLCPolSchema.getAppntNo());
       mLLClaimUnderwriteSchema.setAppntName(mLCPolSchema.getAppntName());
       mLLClaimUnderwriteSchema.setCValiDate(mLCPolSchema.getCValiDate());
       mLLClaimUnderwriteSchema.setPolState(mLCPolSchema.getPolState());


       //保单赔案表
       mClmNo = mLLClaimUnderwriteSchema.getClmNo();
       LLClaimPolicyDB mLLClaimPolicyDB = new LLClaimPolicyDB();
       mLLClaimPolicyDB.setClmNo(mClmNo);
       mLLClaimPolicyDB.setPolNo(mLLClaimUnderwriteSchema.getPolNo());
       mLLClaimPolicyDB.setCaseRelaNo( mLLClaimUnderwriteSchema.getCaseRelaNo() );
    //   mLLClaimPolicyDB.getSetDutyKind( mLLClaimUnderwriteSchema.getGe)
       if (mLLClaimPolicyDB.getInfo() == false)
       {
           // @@错误处理
           this.mErrors.copyAllErrors(mLLClaimPolicyDB.mErrors);
           CError tError = new CError();
           tError.moduleName = "UWAutoChkBL";
           tError.functionName = "getInputData";
           tError.errorMessage = "赔案表查询失败!";
           this.mErrors.addOneError(tError);
           return false;
       }
       mLLClaimPolicySchema = new LLClaimPolicySchema();
       mLLClaimPolicySchema.setSchema(mLLClaimPolicyDB.getSchema());
       mLLClaimUnderwriteSchema.setStandPay(mLLClaimPolicySchema.
               getStandPay());
       mLLClaimUnderwriteSchema.setRealPay(mLLClaimPolicySchema.
               getRealPay());
       mLLClaimUnderwriteSchema.setClmUWer(mGlobalInput.Operator);
       mLLClaimUnderwriteSchema.setMngCom(mGlobalInput.ManageCom);
       //
       tLLClaimUnderwriteSet.add(mLLClaimUnderwriteSchema) ;
       tLLClaimPolicySet.add(mLLClaimPolicySchema);

   }
    }
    else
    {
      mLLClaimSchema =
          (LLClaimSchema)
          cInputData.getObjectByObjectName("LLClaimSchema", 0);
    }
    return true;
  }

  /**
   * 校验投保单是否复核
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkApprove()
  {
    if (mLLClaimPolicySchema.getClmState().equals("2"))
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ClaimAutoChkBL";
      tError.functionName = "checkApprove";
      tError.errorMessage = "已经完成核赔!保单号：" +
          mLLClaimPolicySchema.getPolNo().trim();
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  /**
   * 核保险种信息校验,准备核保算法
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean CheckKinds()
  {
    LMUWSchema tLMUWSchema = new LMUWSchema();
    //查询算法编码
    tLMUWSchema.setRiskCode(
      mLLClaimUnderwriteSchema.getRiskCode().trim());
    tLMUWSchema.setRelaPolType("G"); //个单
    tLMUWSchema.setUWType("4");      //核赔
    LMUWDB tLMUWDB = new LMUWDB();
    tLMUWDB.setSchema(tLMUWSchema);
    mLMUWSet.set(tLMUWDB.query());
    if (tLMUWDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLMUWDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimAutoChkBL";
      tError.functionName = "checkpol";
      tError.errorMessage = "险种信息查询失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  private boolean CheckCase()
  {
    System.out.println("checkCase begin");
    LMUWDB tLMUWDB = new LMUWDB();
    //查询算法编码
    tLMUWDB.setRiskCode("000000");
    tLMUWDB.setUWType("4");      //核赔
    mLMUWSet.set(tLMUWDB.query());
//    System.out.println("nnn==="+mLMUWSet.size());
    if (tLMUWDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLMUWDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ClaimAutoChkBL";
      tError.functionName = "checkpol";
      tError.errorMessage = "险种信息查询失败!";
      this.mErrors.addOneError(tError);
      //mLMUWDBSet.clear();
      return false;
    }
    return true;
  }

  private void calCase()
  {
    // 计算
    Calculator mCalculator = new Calculator();
    mCalculator.setCalCode( mCalCode );
    //增加基本要素
    mCalculator.addBasicFactor("RealPay",String.valueOf(mLLClaimSchema.getRealPay()));
    mCalculator.addBasicFactor("CasePayType",mLLClaimSchema.getCasePayType());

    String tStr = mCalculator.calculate() ;
    if (tStr.trim().equals("") ||
        tStr.trim().equals("0"))
    {
      //通过
      mValue = 1;
    }
    else
    {
      mValue = Double.parseDouble( tStr );
      mCount = mCount + 1;

      LLClaimErrorSchema tLLClaimErrorSchema = new LLClaimErrorSchema();
      tLLClaimErrorSchema.setSerialNo(String.valueOf(mCount));
//      tLLClaimErrorSchema.setClmNo(mLLClaimSchema.getClmNo());
//      tLLClaimErrorSchema.setPolNo("000000");
//      tLLClaimErrorSchema.setManageCom(mGlobalInput.ManageCom);
//      tLLClaimErrorSchema.setUWRuleCode(mCalCode);
//      tLLClaimErrorSchema.setUWError(mLMUWSchema.getRemark());
//      tLLClaimErrorSchema.setCurrValue(tStr);
//      tLLClaimErrorSchema.setUWGrade(mLMUWSchema.getUWGrade());
      tLLClaimErrorSchema.setModifyDate(PubFun.getCurrentDate());
      tLLClaimErrorSchema.setModifyTime(PubFun.getCurrentTime());

      mLLClaimErrorSet.add(tLLClaimErrorSchema);
    }
  }

  /**
   * 个人单核保
   * 输出：如果发生错误则返回false,否则返回true
   */
  private void CheckPol()
  {
    // 计算
    int dayInHosp = 0;
    LLCaseCureDB tLLCaseCureDB = new LLCaseCureDB();
    tLLCaseCureDB.setCaseNo(mLLClaimPolicySchema.getCaseNo());
    LLCaseCureSet tLLCaseCureSet = tLLCaseCureDB.query();
    for ( int i = 1; i <= tLLCaseCureSet.size(); i++)
    {
      dayInHosp += tLLCaseCureSet.get(i).getInHospitalDays();
    }

    Calculator mCalculator = new Calculator();
    mCalculator.setCalCode( mCalCode );
    //增加基本要素
    mCalculator.addBasicFactor("PolNo",mLLClaimPolicySchema.getPolNo()  );
    mCalculator.addBasicFactor("CaseNo", mLLClaimPolicySchema.getCaseNo() );
    mCalculator.addBasicFactor("InsuredNo", mLLClaimPolicySchema.getInsuredNo() );
    mCalculator.addBasicFactor("ClmUWer", mCalBase.getAppAge() );
    mCalculator.addBasicFactor("GetDutyKind", mLLClaimPolicySchema.getGetDutyKind() );
    mCalculator.addBasicFactor("DayInHosp", String.valueOf(dayInHosp) );
    mCalculator.addBasicFactor("Name", mLLClaimPolicySchema.getInsuredName() );
    mCalculator.addBasicFactor("StandPay",String.valueOf(mLLClaimPolicySchema.getStandPay()));
    mCalculator.addBasicFactor("RealPay",String.valueOf(mLLClaimPolicySchema.getRealPay()));

    String tStr = mCalculator.calculate() ;
    if (tStr.trim().equals("") ||
        tStr.trim().equals("0"))
    {
      //通过
      mValue = 1;
    }
    else
    {
      mValue = Double.parseDouble( tStr );
      mCount = mCount++;
      LLClaimErrorSchema
          tLLClaimErrorSchema = new LLClaimErrorSchema();
      tLLClaimErrorSchema.setSerialNo(String.valueOf(mCount));
//      tLLClaimErrorSchema.setClmNo(mClmNo);
      tLLClaimErrorSchema.setPolNo(mLLClaimPolicySchema.getPolNo());
      tLLClaimErrorSchema.setContNo(mLCPolSchema.getContNo());
      tLLClaimErrorSchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
      tLLClaimErrorSchema.setInsuredNo(mLCPolSchema.getInsuredNo());
      tLLClaimErrorSchema.setInsuredName(mLCPolSchema.getInsuredName());
      tLLClaimErrorSchema.setAppntNo(mLCPolSchema.getAppntNo());
      tLLClaimErrorSchema.setAppntName(mLCPolSchema.getAppntName());
      tLLClaimErrorSchema.setManageCom(mGlobalInput.ManageCom);
//      tLLClaimErrorSchema.setUWRuleCode(mCalCode);
      tLLClaimErrorSchema.setUWError(mLMUWSchema.getRemark());
//      tLLClaimErrorSchema.setCurrValue(tStr);
//      tLLClaimErrorSchema.setUWGrade(mLMUWSchema.getUWGrade());
      tLLClaimErrorSchema.setModifyDate(PubFun.getCurrentDate());
      tLLClaimErrorSchema.setModifyTime(PubFun.getCurrentTime());
      mLLClaimErrorSet.add(tLLClaimErrorSchema);
      mLLClaimUnderwriteSchema.setAutoClmDecision("0");
    }
  }

  public double getValue()
  {
    return this.mValue;
  }
  public VData getResult()
  {
    return mResult;
  }

  private void prepareOutputData()
  {
    mInputData.clear();
    mInputData.add(mLLClaimErrorSet);
    MMap tmpMap = new MMap();
    if (mOperate.equals("AutoChkPol"))
    {
      //mLLClaimPolicySchema.setClmState("5");  //人工核赔
     // mInputData.add(mLLClaimPolicySchema);
     tmpMap.put(this.tLLClaimUnderwriteSet,"DELETE&INSERT");
   //  tmpMap.put(this.tLLClaimPolicySet,"DELETE&INSERT");
     tmpMap.put(this.mLLClaimErrorSet,"INSERT");
     mInputData.add(tmpMap);
    }
    else
    {
      mInputData.add(mLLClaimSchema);
    }
  }

  public static void main (String[] args)
  {
    LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new LLClaimUnderwriteSchema();
    tLLClaimUnderwriteSchema.setClmNo("86110020040520000034"); //赔案号
    tLLClaimUnderwriteSchema.setRgtNo("86110020040510000028"); //立案号
    tLLClaimUnderwriteSchema.setPolNo("86110020040210055727");

    GlobalInput tG = new GlobalInput();
    tG.Operator="001";
    tG.ManageCom="86";

    VData aVData = new VData();
    aVData.addElement(tLLClaimUnderwriteSchema);
    aVData.addElement(tG);
    ClaimAutoChkBL aClaimAutoChkBL = new ClaimAutoChkBL();
    aClaimAutoChkBL.submitData(aVData,"AutoChkPol");
  }
}
