/*
 * <p>ClassName: OLDPersonnelBL </p>
 * <p>Description: OLDPersonnelBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-04-13 23:19:03
 */
package com.sinosoft.lis.llcase;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class OLDPersonnelBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
private MMap map = new MMap();
/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
/** 业务处理相关变量 */
private LDPersonnelSchema mLDPersonnelSchema=new LDPersonnelSchema();
//private LDPersonnelSet mLDPersonnelSet=new LDPersonnelSet();
private LLRelationClaimSet mLLRelationClaimSet = new LLRelationClaimSet();
//private LLRelationClaimSchema mLLRelationClaimSchema=new LLRelationClaimSchema();
public OLDPersonnelBL() {
}
public static void main(String[] args) {
    LDPersonnelSchema tLDPersonnelSchema   = new LDPersonnelSchema();
    OLDPersonnelBL tOLDPersonnelBL   = new OLDPersonnelBL();
    GlobalInput mGlobalInput = new GlobalInput();
    mGlobalInput.ManageCom = "86";
    mGlobalInput.ComCode = "86";
    mGlobalInput.Operator = "xuxin";
    tLDPersonnelSchema.setUserCode("hcm000");
    tLDPersonnelSchema.setCustomerNo("wys111");
    tLDPersonnelSchema.setSex("0");
    tLDPersonnelSchema.setName("吴耀生");
    tLDPersonnelSchema.setBirthday("1935-6-6");
    tLDPersonnelSchema.setIDType("0");
    tLDPersonnelSchema.setIDNo("2342423");
    tLDPersonnelSchema.setManageCom("86");
    tLDPersonnelSchema.setDegree("3");
    tLDPersonnelSchema.setOccupationGrade("2");
    tLDPersonnelSchema.setWorkType("3424235423");
    tLDPersonnelSchema.setPluralityType("234234");
    VData aVData = new VData();
    aVData.addElement(mGlobalInput);
    aVData.addElement(tLDPersonnelSchema);
    tOLDPersonnelBL.submitData(aVData,"UPDATE||MAIN");
}
/**
* 传输数据的公共方法
	* @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "OLDPersonnelBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败OLDPersonnelBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHCustomInHospitalBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("End OLHCustomInHospitalBL Submit...");

    }
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
    if (this.mOperate.equals("INSERT||MAIN"))
    {
        System.out.println("insert.....");
        LLRelationClaimSet tLLRelationClaimSet = new LLRelationClaimSet();
        LDPersonnelDB tLDPersonnelDB = new LDPersonnelDB();
        tLDPersonnelDB.setCustomerNo(mLDPersonnelSchema.getCustomerNo());
        if(tLDPersonnelDB.getInfo()){
            CError tError = new CError();
            tError.moduleName = "tLDPersonnelDB";
            tError.functionName = "getInfo";
            tError.errorMessage = "系统中已有使用该员工号码的职员，请确认数据是否正确!";
            this.mErrors .addOneError(tError) ;
            return false;
        }
        mLDPersonnelSchema.setOperator(mGlobalInput.Operator);
        mLDPersonnelSchema.setMakeDate(PubFun.getCurrentDate());
        mLDPersonnelSchema.setMakeTime(PubFun.getCurrentTime());
        mLDPersonnelSchema.setModifyDate(PubFun.getCurrentDate());
        mLDPersonnelSchema.setModifyTime(PubFun.getCurrentTime());
        
        // #3340 人员信息档案管理模块**start**
        if(mLLRelationClaimSet!=null && mLLRelationClaimSet.size()>0){
	        for(int i=1;i<=mLLRelationClaimSet.size();i++){
	        	System.out.println("---Star Insert Operate ---");  
	        	LLRelationClaimSchema tLLRelationClaimSchema  =mLLRelationClaimSet.get(i); 
	        	String tSerialNo = tLLRelationClaimSchema.getSerialNo();
	        	if(mOperate.equals("INSERT||MAIN")&& ("".equals(tSerialNo)) || tSerialNo==null){
	        		tLLRelationClaimSchema.setSerialNo(PubFun1.CreateMaxNo("RELA_USER", 20));
	        	}
	        	tLLRelationClaimSchema.setMakeDate(PubFun.getCurrentDate());
	        	tLLRelationClaimSchema.setMakeTime(PubFun.getCurrentTime());
	        	tLLRelationClaimSchema.setModifyDate(PubFun.getCurrentDate());
	        	tLLRelationClaimSchema.setModifyTime(PubFun.getCurrentTime());
	        	tLLRelationClaimSchema.setOperator(mGlobalInput.Operator);
	        	tLLRelationClaimSchema.setMngCom(mGlobalInput.ManageCom);
	        	tLLRelationClaimSchema.setUserCode(mLDPersonnelSchema.getUserCode());
	        	tLLRelationClaimSchema.setUserName(mLDPersonnelSchema.getName());
	        	tLLRelationClaimSet.add(tLLRelationClaimSchema);
	        	
	          }
        
        map.put(tLLRelationClaimSet, "INSERT");
        }
        //  #3340 人员信息档案管理模块**end**
        map.put(mLDPersonnelSchema, "INSERT");
    }
    if (this.mOperate.equals("UPDATE||MAIN")) {
        System.out.println("update.....");
        LLRelationClaimSet tLLRelationClaimSet = new LLRelationClaimSet();
        LDPersonnelDB tLDPersonnelDB = new LDPersonnelDB();
        tLDPersonnelDB.setCustomerNo(mLDPersonnelSchema.getCustomerNo());
        if(!tLDPersonnelDB.getInfo()){
            CError tError = new CError();
            tError.moduleName = "tLDPersonnelDB";
            tError.functionName = "getInfo";
            tError.errorMessage = "系统中尚无该员工的信息，不能进行修改操作!";
            this.mErrors .addOneError(tError) ;
            return false;
        }
      
        mLDPersonnelSchema.setMakeDate(tLDPersonnelDB.getMakeDate());
        mLDPersonnelSchema.setMakeTime(tLDPersonnelDB.getMakeTime());
        mLDPersonnelSchema.setOperator(tLDPersonnelDB.getOperator());
        mLDPersonnelSchema.setModifyDate(PubFun.getCurrentDate());
        mLDPersonnelSchema.setModifyTime(PubFun.getCurrentTime());
        
        // # 3340 人员信息档案管理模块**start**
        //查询是否有历史数据，有数据则删除在添加。。
        if(mLLRelationClaimSet!=null && mLLRelationClaimSet.size()>0){
      
	        for(int j=1;j<=mLLRelationClaimSet.size();j++){
	        	  
	       	 LLRelationClaimSchema tLLRelationClaimSchema = mLLRelationClaimSet.get(j);
	       	 getRelaUserInfo(tLLRelationClaimSchema);
	       	 tLLRelationClaimSchema.setSerialNo(PubFun1.CreateMaxNo("RELA_USER", 20));
	   		 tLLRelationClaimSchema.setMakeDate(PubFun.getCurrentDate());
	       	 tLLRelationClaimSchema.setMakeTime(PubFun.getCurrentTime()); 
	       	 tLLRelationClaimSchema.setModifyDate(PubFun.getCurrentDate());
	       	 tLLRelationClaimSchema.setModifyTime(PubFun.getCurrentTime());
	       	 tLLRelationClaimSchema.setOperator(mGlobalInput.Operator);
	       	 tLLRelationClaimSchema.setMngCom(mGlobalInput.ManageCom);
	       	 tLLRelationClaimSchema.setUserCode(mLDPersonnelSchema.getUserCode());
	       	 tLLRelationClaimSchema.setUserName(mLDPersonnelSchema.getName());
	       	 tLLRelationClaimSet.add(tLLRelationClaimSchema);
	        }
	        map.put(tLLRelationClaimSet, "INSERT");
	     }
        // # 3340 人员信息档案管理模块**end**
        map.put(mLDPersonnelSchema, "UPDATE");
    }
    if (this.mOperate.equals("DELETE||MAIN")) {
        System.out.println("delete.....");
        LDPersonnelDB tLDPersonnelDB = new LDPersonnelDB();
        tLDPersonnelDB.setCustomerNo(mLDPersonnelSchema.getCustomerNo());
        if(!tLDPersonnelDB.getInfo()){
            CError tError = new CError();
            tError.moduleName = "tLDPersonnelDB";
            tError.functionName = "getInfo";
            tError.errorMessage = "系统中尚无该员工的信息，不能进行删除操作!";
            this.mErrors .addOneError(tError) ;
            return false;
        }
        //***3091 功能迁移****
        //进行信息的判断删除
        if(!getRelaUserInfo(mLDPersonnelSchema)){
    		 return false;
    	 }
        
        map.put(mLDPersonnelSchema, "DELETE");
       
    }

    return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
         this.mLDPersonnelSchema.setSchema((LDPersonnelSchema)cInputData.getObjectByObjectName("LDPersonnelSchema",0));
         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         LLRelationClaimSet tLLRelationClaimSet = new LLRelationClaimSet();
         tLLRelationClaimSet=(LLRelationClaimSet) cInputData.getObjectByObjectName("LLRelationClaimSet", 0); // #3340  人员信息档案管理模块
         if(tLLRelationClaimSet!=null){
        	 this.mLLRelationClaimSet.set(tLLRelationClaimSet);
         }
       
         return true;
}

public boolean getRelaUserInfo(LLRelationClaimSchema aLLRelationClaimSchema){

	  LLRelationClaimDB oldLLRelationClaimDB= new LLRelationClaimDB();
	  String relaSQL="select * from llrelationclaim where serialno='"+aLLRelationClaimSchema.getSerialNo()+"' with ur";
	  LLRelationClaimSet oldLLRelationClaimSet =oldLLRelationClaimDB.executeQuery(relaSQL);
	  
	  if(oldLLRelationClaimSet==null ||oldLLRelationClaimSet.size()<=0){
		  return true;
	  }else{
		  String deleSQL ="delete from llrelationclaim where serialno='"+aLLRelationClaimSchema.getSerialNo()+"'" ;
		  MMap tmpMap= new MMap();
		  tmpMap.put(deleSQL, "DELETE");
		  VData aResult = new VData();
		  aResult.clear();
		  aResult.add(tmpMap);
		  PubSubmit ps = new PubSubmit();
      if (!ps.submitData(aResult, null)) {
          CError.buildErr(this, "数据库保存失败");
          return false;
      }
	   }
	 
	  return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LDPersonnelDB tLDPersonnelDB=new LDPersonnelDB();
    tLDPersonnelDB.setSchema(this.mLDPersonnelSchema);
		//如果有需要处理的错误，则返回
		if (tLDPersonnelDB.mErrors.needDealError())
 		{
		  // @@错误处理
 			this.mErrors.copyAllErrors(tLDPersonnelDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "LDPersonnelBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors .addOneError(tError) ;
 			return false;
    }
    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
   try
	{
		this.mInputData.clear();
		this.mInputData.add(this.mLDPersonnelSchema);
                this.mInputData.add(this.map);
		mResult.clear();
    mResult.add(this.mLDPersonnelSchema);
	}
	catch(Exception ex)
	{
 		// @@错误处理
		CError tError =new CError();
 		tError.moduleName="LDPersonnelBL";
 		tError.functionName="prepareData";
 		tError.errorMessage="在准备往后层处理所需要的数据时出错。";
 		this.mErrors .addOneError(tError) ;
		return false;
	}
	return true;
	}
	public VData getResult()
	{
  	return this.mResult;
	}
	  // # 3340  人员信息档案管理模块**start**
	  public boolean getRelaUserInfo(LDPersonnelSchema aLDPersonnelSchema){

		  LLRelationClaimDB oldLLRelationClaimDB= new LLRelationClaimDB();
		  String relaSQL="select * from llrelationclaim where usercode='"+aLDPersonnelSchema.getUserCode()+"' ";
		  LLRelationClaimSet oldLLRelationClaimSet =oldLLRelationClaimDB.executeQuery(relaSQL);
		  
		  if(oldLLRelationClaimSet==null ||oldLLRelationClaimSet.size()<=0){
			  return true;
		  }else{
			  String deleSQL ="delete from llrelationclaim where usercode='"+aLDPersonnelSchema.getUserCode()+"'" ;
			  map.put(deleSQL, "DELETE");
		   }
		 
		  return true;
	  }
	//# 3340 人员信息档案管理模块**end** 
	
}
