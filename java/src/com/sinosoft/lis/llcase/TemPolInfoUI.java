package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:Save temporary pol info User Interface Layer
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @date:2003-04-10
 * @author 刘岩松
 * @version 1.0
 */
public class TemPolInfoUI
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData = new VData();
  private VData mResult = new VData();
  private String mOperate;

  public TemPolInfoUI() {}

  public boolean submitData(VData cInputData,String cOperate)
  {
    this.mOperate = cOperate;
    TemPolInfoBL tTemPolInfoBL = new TemPolInfoBL();
    System.out.println("---TemPolInfoUI BEGIN---");
    if (tTemPolInfoBL.submitData(cInputData,mOperate) == false)
    {
      this.mErrors.copyAllErrors(tTemPolInfoBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "TemPolInfoUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据保存失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
      mResult = tTemPolInfoBL.getResult();
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public static void main(String[] args)
  {
    TemPolInfoUI tTemPolInfoUI=new TemPolInfoUI();
    VData tVData=new VData();
    LLCasePolicySchema tLLCasePolicySchema = new LLCasePolicySchema();
    tLLCasePolicySchema.setCasePolType("1");
    tLLCasePolicySchema.setAppntName("中国机电协会");
    tLLCasePolicySchema.setCaseNo("00100020030550000033");
    tLLCasePolicySchema.setPolNo("86110020030210000007");
    tLLCasePolicySchema.setRgtNo("00100020030510000033");
    tLLCasePolicySchema.setMngCom("ddd");
    tLLCasePolicySchema.setOperator("ddd");
    tLLCasePolicySchema.setMakeDate("2003-04-01");
    tLLCasePolicySchema.setMakeTime("12:12:12");
    tLLCasePolicySchema.setModifyDate("2002-10-10");
    tLLCasePolicySchema.setModifyTime("12:12:12");
    LLCasePolicySet tLLCasePolicySet = new LLCasePolicySet();
    tLLCasePolicySet.add(tLLCasePolicySchema);
    tVData.addElement(tLLCasePolicySet);
    tTemPolInfoUI.submitData(tVData,"INSERT||MAIN");
  }
}