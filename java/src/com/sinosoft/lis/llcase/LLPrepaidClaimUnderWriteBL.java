package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.task.Task;

/**
 * <p>Title: 工单管理系统</p>
 * <p>Description: 工单录入BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @version 1.0
 * @date 2005-01-17
 */

public class LLPrepaidClaimUnderWriteBL {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 输入数据的容器 */
    private VData mInputData = new VData();

    /** 输出数据的容器 */
    private VData mResult = new VData();
    private MMap map = new MMap();

    /** 数据操作字符串 */
    private LLPrepaidUWMainSchema mLLPrepaidUWMainSchema = new LLPrepaidUWMainSchema();
    private LJSGetClaimSet tLJSGetClaimSet = new LJSGetClaimSet();    
    private String mPrepaidNo = "";    
    private String mDealer = "";		//审定人
    private double mPreMoney = 0.0;		//审批金额
    private String mOperate;
    private String mReturnMessage = "";

    /** 全局参数 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    /**社保案件标记 0：非社保；1：社保*/
    private String mSocialFlag = "0";
	/** 团体保单号 */
	private String mGrpContNo = "";
    
    public LLPrepaidClaimUnderWriteBL() {
    }

    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     * @param: cOperate   数据操作字符串
     * @return: boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
    	
    	//将传入的数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //将外部传入的数据分解到本类的属性中，准备处理
        if (!getInputData()) {
            return false;
        }
        
        if (!checkData()) {
            return false;
        }

        //根据业务逻辑对数据进行处理
        if (!dealData()) {
            return false;
        }
        prepareOutputData();
        
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
        	CError.buildErr(this, "数据保存失败");
            return false;
          }
        return true;
    }
    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData() {
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mLLPrepaidUWMainSchema.setSchema((LLPrepaidUWMainSchema)mInputData.getObjectByObjectName("LLPrepaidUWMainSchema",0));

        mPrepaidNo = mLLPrepaidUWMainSchema.getPrepaidNo();
        if(mPrepaidNo.equals("")||mPrepaidNo==null)
        {
        	CError.buildErr(this, "预付赔款号为空!");
            return false;
        }
        return true;
    }

    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData() {
    	
        String tsql = "select a.dealer,b.prepaidflag from LLPrepaidClaim a,llclaimuser b "
        			+" where a.dealer=b.usercode and a.prepaidno='"+ mPrepaidNo +"' union " +
        			"select a.dealer ,b.prepaidflag from llprepaidclaim a,llsocialclaimuser b " +
        			"where a.dealer=b.usercode and a.prepaidno='"+mPrepaidNo+"' with ur";
        ExeSQL texesql = new ExeSQL();
        SSRS ssrs = texesql.execSQL(tsql);
        if (ssrs.getMaxRow()<=0 ) {
            CError.buildErr(this, "没有查询到案件"+mPrepaidNo+"的处理人!");
            return false;
        }else{
        	if(!ssrs.GetText(1, 1).equals(mGlobalInput.Operator)||!ssrs.GetText(1, 2).equals("1"))
        	{
        		CError.buildErr(this, "当前操作员"+mGlobalInput.Operator+"没有预付案件审批权限!");
                return false;
        	}
        }  
		//#1962 社保业务预付赔款功能需求分析
        LLPrepaidClaimDetailDB tLLPrepaidClaimDetailDB = new LLPrepaidClaimDetailDB();
    	LLPrepaidClaimDetailSet tLLPrepaidClaimDetailSet = new LLPrepaidClaimDetailSet();
    	LLPrepaidClaimDetailSchema tLLPrepaidClaimDetailSchema = new LLPrepaidClaimDetailSchema();
    	
    	tLLPrepaidClaimDetailDB.setPrepaidNo(mPrepaidNo);
    	tLLPrepaidClaimDetailSet = tLLPrepaidClaimDetailDB.query();
    	if(tLLPrepaidClaimDetailSet.size()<=0)
    	{
    		CError.buildErr(this, "预付赔款明细信息查询失败!");
            return false;
    	}else{ 	         
    		for (int i = 1; i <= tLLPrepaidClaimDetailSet.size(); i++) 
    		{
    			tLLPrepaidClaimDetailSchema = tLLPrepaidClaimDetailSet.get(i);	    	
		    	LCGrpPolBL tLCGrpPolBL = new LCGrpPolBL();
		    	tLCGrpPolBL.setGrpPolNo(tLLPrepaidClaimDetailSchema.getGrpPolNo());
		        if (!tLCGrpPolBL.getInfo()) {
		            CError.buildErr(this, "保单查询" +tLLPrepaidClaimDetailSchema.getGrpPolNo()+"查询失败");
		            return false;
		        }
		        mGrpContNo = tLLPrepaidClaimDetailSchema.getGrpContNo();
    		}
    		
    	}

		mSocialFlag = LLCaseCommon.checkSocialSecurity(mGrpContNo, "grpno");
        
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: boolean
     */
    private boolean dealData() {
    	boolean IsPower = true;
    	LLPrepaidUWMainSchema tLLPrepaidUWMainSchema = new LLPrepaidUWMainSchema();
    	LLPrepaidUWTraceSchema tLLPrepaidUWTraceSchema = new LLPrepaidUWTraceSchema();
    	LLPrepaidClaimDB tLLPrepaidClaimDB = new LLPrepaidClaimDB();
    	LLPrepaidClaimSchema tLLPrepaidClaimSchema = new LLPrepaidClaimSchema();
    	
    	tLLPrepaidClaimDB.setPrepaidNo(mPrepaidNo);
    	if(!tLLPrepaidClaimDB.getInfo())
    	{
    		CError.buildErr(this, "预付赔款信息查询失败!");
            return false;
    	}else{
    		tLLPrepaidClaimSchema = tLLPrepaidClaimDB.getSchema();
    	}
    	    	
    	mPreMoney = tLLPrepaidClaimSchema.getSumMoney();
    	if(mOperate.equals("SPSDUW"))
    	{
    		if(!tLLPrepaidClaimSchema.getRgtState().equals("01")
    	    		&&!tLLPrepaidClaimSchema.getRgtState().equals("02")
    	    		&&!tLLPrepaidClaimSchema.getRgtState().equals("03"))
	    	{
	    		CError.buildErr(this, "预付案件只有在申请扫描审批状态才能进行审批操作");
	            return false;
	    	}
    		
    		if(!checkUserRight(tLLPrepaidClaimSchema))
    		{
    			 IsPower = false;
    		}
    		
    		//社保预付校验
    		double tLowPrepaidLimit = 0;
    		double tHighPrepaidLimit = 0;
    		if("1".equals(mSocialFlag)){
    			LLSocialClaimUserDB tSocialClaimUserDB = new LLSocialClaimUserDB();
            	LLSocialClaimUserSet tSocialClaimUserSet = new LLSocialClaimUserSet();
            	//预付保单确认时操作人必须为在“社保业务理赔人员权限配置”中进行配置的用户，且所属机构为分公司
            	String tSSQL = "select * from llsocialclaimuser where 1=1 and" +
            			" stateflag='1' and usercode ='"+tLLPrepaidClaimSchema.getHandler()+"' with ur ";
            	System.out.println("tSSQL:"+tSSQL);
            	tSocialClaimUserSet = tSocialClaimUserDB.executeQuery(tSSQL);
            	if(tSocialClaimUserSet.size() <= 0){
            		// @@错误处理
            		CError.buildErr(this, 
        					"当前登录用户"+tLLPrepaidClaimSchema.getHandler()+"未在社保业务理赔人员权限配置！");
        			return false;
            	}else{
            		tLowPrepaidLimit = tSocialClaimUserSet.get(1).getPrepaidMoney();
            	}
            	
            	LLSocialClaimUserDB tUpSocialClaimUserDB = new LLSocialClaimUserDB();
            	LLSocialClaimUserSet tUpSocialClaimUserSet = new LLSocialClaimUserSet();
            	//预付保单确认时操作人必须为在“社保业务理赔人员权限配置”中进行配置的用户，且所属机构为分公司
            	String tUpSSQL = "select * from llsocialclaimuser where 1=1 and" +
            			" stateflag='1' and usercode ='"+mDealer+"' with ur ";
            	System.out.println("tUpSSQL:"+tUpSSQL);
            	tUpSocialClaimUserSet = tUpSocialClaimUserDB.executeQuery(tUpSSQL);
            	if(tUpSocialClaimUserSet.size() <= 0){
            		// @@错误处理
            		CError.buildErr(this, 
        					"用户"+mDealer+"未在社保业务理赔人员权限配置！");
        			return false;
            	}else{
            		tHighPrepaidLimit = tUpSocialClaimUserSet.get(1).getPrepaidMoney();
            	}

    		}else{
    			LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
        		LLClaimUserSchema tLLClaimUserSchema = new LLClaimUserSchema();
        		tLLClaimUserDB.setUserCode(tLLPrepaidClaimSchema.getHandler());
        		if(!tLLClaimUserDB.getInfo())
        		{
        			CError.buildErr(this, "理赔用户"+tLLPrepaidClaimSchema.getHandler()+"查询失败!");
                    return false;
        		}else{
        			tLLClaimUserSchema = tLLClaimUserDB.getSchema();
        			tLowPrepaidLimit = tLLClaimUserSchema.getPrepaidLimit();
        		}
        		
	   	    	LLClaimUserDB upLLClaimUserDB = new LLClaimUserDB(); 
	    		LLClaimUserSchema upLLClaimUserSchema = new LLClaimUserSchema();
	    		upLLClaimUserDB.setUserCode(mDealer);
	    		if(!upLLClaimUserDB.getInfo())
	    		{
	    			CError.buildErr(this, "理赔用户"+mDealer+"查询失败!");
	                return false;
	    		} else{
	    			upLLClaimUserSchema = upLLClaimUserDB.getSchema();
	    			tHighPrepaidLimit = upLLClaimUserSchema.getPrepaidLimit();
	    		}
    		}
    		
    		
    		tLLPrepaidUWMainSchema.setPrepaidNo(mPrepaidNo);	 
	       	tLLPrepaidUWMainSchema.setHandler(tLLPrepaidClaimSchema.getHandler());
	       	tLLPrepaidUWMainSchema.setHandlLimit(tLowPrepaidLimit);
	    	tLLPrepaidUWMainSchema.setRemark("");	    	
	    	tLLPrepaidUWMainSchema.setManageCom(mGlobalInput.ComCode);
	    	tLLPrepaidUWMainSchema.setOperator(mGlobalInput.Operator);
	    	tLLPrepaidUWMainSchema.setMakeDate(mCurrentDate);
	    	tLLPrepaidUWMainSchema.setMakeTime(mCurrentTime);
	    	tLLPrepaidUWMainSchema.setModifyDate(mCurrentDate);
	    	tLLPrepaidUWMainSchema.setModifyTime(mCurrentTime);
	    	
	    	String tSerialNo = PubFun1.CreateMaxNo("SERIALNO",mGlobalInput.ComCode);
	    	tLLPrepaidUWTraceSchema.setSerialNo(tSerialNo);
	    	tLLPrepaidUWTraceSchema.setPrepaidNo(mPrepaidNo);
	    	tLLPrepaidUWTraceSchema.setHandler(tLLPrepaidClaimSchema.getHandler());
	    	tLLPrepaidUWTraceSchema.setHandlLimit(tLowPrepaidLimit);	
	    	tLLPrepaidUWTraceSchema.setRemark("");	    	
	    	tLLPrepaidUWTraceSchema.setManageCom(mGlobalInput.ComCode);
	    	tLLPrepaidUWTraceSchema.setOperator(mGlobalInput.Operator);
	    	tLLPrepaidUWTraceSchema.setMakeDate(mCurrentDate);
	    	tLLPrepaidUWTraceSchema.setMakeTime(mCurrentTime);
	    	tLLPrepaidUWTraceSchema.setModifyDate(mCurrentDate);
	    	tLLPrepaidUWTraceSchema.setModifyTime(mCurrentTime);
	    		    
		    if(IsPower)
		    {
		    	 tLLPrepaidUWMainSchema.setCheckDecision("1");
		    	 tLLPrepaidUWMainSchema.setDealer(mDealer);
		    	 tLLPrepaidUWMainSchema.setDealLimit(tHighPrepaidLimit);
		    	 
			     tLLPrepaidUWTraceSchema.setCheckDecision("1");		
			     tLLPrepaidUWTraceSchema.setAppDealer(mDealer);
			     tLLPrepaidUWTraceSchema.setAppDealLimit(tHighPrepaidLimit);
			     if(tLLPrepaidClaimSchema.getRgtState().equals("01")
			    		 ||tLLPrepaidClaimSchema.getRgtState().equals("02"))
			     {
			    	 tLLPrepaidClaimSchema.setHandleDate(mCurrentDate);
			     }
		    	 tLLPrepaidClaimSchema.setDealer(mDealer);
		    	 tLLPrepaidClaimSchema.setEndDate(mCurrentDate);
		    	 tLLPrepaidClaimSchema.setRgtState("04");
		    	 tLLPrepaidClaimSchema.setModifyDate(mCurrentDate);
		    	 tLLPrepaidClaimSchema.setModifyTime(mCurrentTime);
		    	 if(tLLPrepaidClaimSchema.getRgtType().equals("1"))
		    	 {
		    		 if (!getFinanceLJSGet(tLLPrepaidClaimSchema)) {
			              CError.buildErr(this, "生成业务应付数据失败");
			              return false;
			          }
		    	 }
		    	 mReturnMessage = "审批完毕";	    
		     }else{		    
		    	 tLLPrepaidUWMainSchema.setCheckDecision("2");
		    	 tLLPrepaidUWMainSchema.setDealer(mDealer);
		    	 tLLPrepaidUWMainSchema.setDealLimit(tHighPrepaidLimit);
		    	 
			     tLLPrepaidUWTraceSchema.setCheckDecision("2");		
			     tLLPrepaidUWTraceSchema.setAppDealer(mDealer);
			     tLLPrepaidUWTraceSchema.setAppDealLimit(tHighPrepaidLimit);
			     if(tLLPrepaidClaimSchema.getRgtState().equals("01")
			    		 ||tLLPrepaidClaimSchema.getRgtState().equals("02"))
			     {
			    	 tLLPrepaidClaimSchema.setHandleDate(mCurrentDate);
			     }		    	 
			     tLLPrepaidClaimSchema.setDealer(mDealer);
		    	 tLLPrepaidClaimSchema.setRgtState("03");
		    	 tLLPrepaidClaimSchema.setModifyDate(mCurrentDate);
		    	 tLLPrepaidClaimSchema.setModifyTime(mCurrentTime);
		    	 mReturnMessage = "审批完毕,您权限不够,已送交上级审批人:"+mDealer;	 
		     }	    		    	
		    map.put(tLLPrepaidUWMainSchema,"DELETE&INSERT");
	    	map.put(tLLPrepaidUWTraceSchema,"INSERT");
	    	map.put(tLLPrepaidClaimSchema,"DELETE&INSERT");  
	    	
    	}else if(mOperate.equals("BACK")){
    		
    		if(!tLLPrepaidClaimSchema.getRgtState().equals("03")
    	    		&&!tLLPrepaidClaimSchema.getRgtState().equals("04"))
	    	{
	    		CError.buildErr(this, "预付案件只有在审批审定状态才能进行审批回退");
	            return false;
	    	}
    		
    		LLPrepaidBackSchema tLLPrepaidBackSchema = new LLPrepaidBackSchema();
    		
    		 String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
             String CaseBackNo = PubFun1.CreateMaxNo("CaseBack", tLimit);

    		tLLPrepaidBackSchema.setPrepaidBackNo(CaseBackNo);
    		tLLPrepaidBackSchema.setPrepaidNo(mPrepaidNo);
    		tLLPrepaidBackSchema.setBackType("1");
    		tLLPrepaidBackSchema.setBeforState(tLLPrepaidClaimSchema.getRgtState());
    		tLLPrepaidBackSchema.setAfterState("01");
    		tLLPrepaidBackSchema.setHandler(mGlobalInput.Operator);
    		tLLPrepaidBackSchema.setNewHanler(tLLPrepaidClaimSchema.getRegister());
    		tLLPrepaidBackSchema.setBackDate("01");
    		tLLPrepaidBackSchema.setRemark("");	    	
    		tLLPrepaidBackSchema.setManageCom(mGlobalInput.ComCode);
    		tLLPrepaidBackSchema.setOperator(mGlobalInput.Operator);
    		tLLPrepaidBackSchema.setMakeDate(mCurrentDate);
    		tLLPrepaidBackSchema.setMakeTime(mCurrentTime);
    		tLLPrepaidBackSchema.setModifyDate(mCurrentDate);
    		tLLPrepaidBackSchema.setModifyTime(mCurrentTime);
    		
    		tLLPrepaidClaimSchema.setRgtState("01");   
    		tLLPrepaidClaimSchema.setDealer(tLLPrepaidClaimSchema.getHandler());
    		tLLPrepaidClaimSchema.setModifyDate(mCurrentDate);
    		tLLPrepaidClaimSchema.setModifyTime(mCurrentTime);
    		mReturnMessage = "回退完毕,已回退至原申请人:"+tLLPrepaidClaimSchema.getRegister();
    		map.put(tLLPrepaidBackSchema,"INSERT");
    		map.put(tLLPrepaidClaimSchema,"DELETE&INSERT");
    	}
    	mResult.add(mReturnMessage);
        return true;
    }
    /**
     * 判断是否需要上报
     * @param: 无
     * @return: void
     */
    private boolean checkUserRight(LLPrepaidClaimSchema aLLPrepaidClaimSchema) {
    	mDealer = aLLPrepaidClaimSchema.getDealer();
    	String UserSql="select UserCode,PrepaidUpUserCode,PrepaidLimit"
	 		+" from llclaimuser where 1=1 and PrepaidFlag='1' and StateFlag='1'"
	 		+" and usercode='"+ mGlobalInput.Operator +"' " +
	 		"union select usercode,prepaidupusercode,prepaidmoney " +
	 		"from llsocialclaimuser where 1=1 and PrepaidFlag='1' and StateFlag='1' " +
	 		" and usercode='"+ mGlobalInput.Operator +"' with ur";
		ExeSQL Userexesql = new ExeSQL();
	    SSRS Userssrs = Userexesql.execSQL(UserSql);		   
	    double LimitAmnt = Double.parseDouble(Userssrs.GetText(1, 3));
	     if(Userssrs.getMaxRow()<=0||LimitAmnt < Math.abs(mPreMoney))
	     {		
	    	 mDealer = Userssrs.GetText(1, 2);	    	 
	    	 return false;
	     }	   
	    	
	    System.out.println("mDealer====="+mDealer);
        return true;
    }
    /**
     * 预付赔款时生成财务ljsget信息
     * @param: 无
     * @return: void
     */
    private boolean getFinanceLJSGet(LLPrepaidClaimSchema aLLPrepaidClaimSchema)
    {
    	String delsql1 = "delete from ljsget where otherno='"+ mPrepaidNo +"' and othernotype='Y'";
    	String delsql2 = "delete from ljsgetclaim where otherno='"+ mPrepaidNo +"' and othernotype='Y'";
    	map.put(delsql1, "DELETE");
    	map.put(delsql2, "DELETE");
 	     
    	LLPrepaidClaimDetailDB tLLPrepaidClaimDetailDB = new LLPrepaidClaimDetailDB();
    	LLPrepaidClaimDetailSet tLLPrepaidClaimDetailSet = new LLPrepaidClaimDetailSet();
    	LLPrepaidClaimDetailSchema tLLPrepaidClaimDetailSchema = new LLPrepaidClaimDetailSchema();
    	
    	tLLPrepaidClaimDetailDB.setPrepaidNo(mPrepaidNo);
    	tLLPrepaidClaimDetailSet = tLLPrepaidClaimDetailDB.query();
    	if(tLLPrepaidClaimDetailSet.size()<=0)
    	{
    		CError.buildErr(this, "预付赔款明细信息查询失败!");
            return false;
    	}else{
 	        String strLimit = PubFun.getNoLimit(mGlobalInput.ComCode);
   	        String tGetNoticeNo = PubFun1.CreateMaxNo("GetNoticeNo", strLimit);   	         
    		double tSumRealPay = 0.0;
    		for (int i = 1; i <= tLLPrepaidClaimDetailSet.size(); i++) 
    		{
    			tLLPrepaidClaimDetailSchema = tLLPrepaidClaimDetailSet.get(i);
	    		LJSGetClaimSchema tLJSGetClaimSchema = new LJSGetClaimSchema();
	    	
	   	    	 LCGrpPolBL tLCGrpPolBL = new LCGrpPolBL();
	   	    	 tLCGrpPolBL.setGrpPolNo(tLLPrepaidClaimDetailSchema.getGrpPolNo());
	   	         if (!tLCGrpPolBL.getInfo()) {
	   	             CError.buildErr(this, "保单查询" +tLLPrepaidClaimDetailSchema.getGrpPolNo()+"查询失败");
	   	             return false;
	   	         }

	   	         tLJSGetClaimSchema.setGetNoticeNo(tGetNoticeNo);
	   	         tLJSGetClaimSchema.setContNo("00000"+i);
	   	         tLJSGetClaimSchema.setPolNo("00000"+i);
	   	         tLJSGetClaimSchema.setFeeFinaType("YF");
	   	         tLJSGetClaimSchema.setFeeOperationType("YF");
	   	         tLJSGetClaimSchema.setRiskCode(tLCGrpPolBL.getRiskCode());
	   	         tLJSGetClaimSchema.setGrpContNo(tLCGrpPolBL.getGrpContNo());
	   	         tLJSGetClaimSchema.setSaleChnl(tLCGrpPolBL.getSaleChnl());
	   	         tLJSGetClaimSchema.setGrpPolNo(tLCGrpPolBL.getGrpPolNo());
	   	         tLJSGetClaimSchema.setAgentCode(tLCGrpPolBL.getAgentCode());
	   	         tLJSGetClaimSchema.setAgentCom(tLCGrpPolBL.getAgentCom());
	   	         tLJSGetClaimSchema.setAgentGroup(tLCGrpPolBL.getAgentGroup());
	   	         tLJSGetClaimSchema.setAgentType(tLCGrpPolBL.getAgentType());
	   	         tLJSGetClaimSchema.setGetDate(mCurrentDate);
	   	         tLJSGetClaimSchema.setOperator(mGlobalInput.Operator);
	   	         tLJSGetClaimSchema.setManageCom(tLCGrpPolBL.getManageCom());
	   	         tLJSGetClaimSchema.setMakeDate(mCurrentDate);
	   	         tLJSGetClaimSchema.setMakeTime(mCurrentTime);
	   	         tLJSGetClaimSchema.setModifyDate(mCurrentDate); 
	   	         tLJSGetClaimSchema.setModifyTime(mCurrentTime);
	   	         tLJSGetClaimSchema.setOtherNo(mPrepaidNo);
	   	         tLJSGetClaimSchema.setOtherNoType("Y");
	   	         tLJSGetClaimSchema.setPay(tLLPrepaidClaimDetailSchema.getMoney());
	   	         tSumRealPay +=  tLJSGetClaimSchema.getPay();
	   	        	 
	   	         tLJSGetClaimSet.add(tLJSGetClaimSchema);
    		}
    		LJSGetSchema tLJSGetSchema = new LJSGetSchema();
    		Reflections tref = new Reflections();
    	    tref.transFields(tLJSGetSchema, tLJSGetClaimSet.get(1));
    	    tLJSGetSchema.setSumGetMoney(Arith.round(tSumRealPay, 2));
    	    tLJSGetSchema.setPayMode(aLLPrepaidClaimSchema.getPayMode());
    	    tLJSGetSchema.setAccName(aLLPrepaidClaimSchema.getAccName());
    	    tLJSGetSchema.setBankAccNo(aLLPrepaidClaimSchema.getBankAccNo());
    	    tLJSGetSchema.setBankCode(aLLPrepaidClaimSchema.getBankCode());
    	    
    		map.put(tLJSGetClaimSet, "INSERT");
    		map.put(tLJSGetSchema, "INSERT");
    	}
    	  return true;
    }
   
    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData() {
        mInputData.clear();
        mInputData.add(map);
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */
    public VData getResult() {
        return mResult;
    }
   
    public static void main(String arg[]) {
//        GlobalInput tGI = new GlobalInput();
//        tGI.ComCode = "862100";
//        tGI.Operator = "cm1102";
//
//        //输入参数
//        LLClaimUserSchema tLLClaimUserSchema = new LLClaimUserSchema();
//        tLLClaimUserSchema.setUserCode("cm1102");        
//        tLLClaimUserSchema.setUpUserCode("cm0003");
//        tLLClaimUserSchema.setPrepaidFlag("1");
//        tLLClaimUserSchema.setPrepaidLimit("20000");
//        tLLClaimUserSchema.setPrepaidUpUserCode("cm0001");
//        
//        LLPrepaidUWMainSchema mLLPrepaidUWMainSchema = new LLPrepaidUWMainSchema();
//        mLLPrepaidUWMainSchema.setPrepaidNo("Y1100110130000001");
//        
//        VData tVData = new VData();
//        tVData.add(tLLClaimUserSchema);
//        tVData.add(mLLPrepaidUWMainSchema);
//        tVData.add(tGI);
//      
//       
//        LLPrepaidClaimUnderWriteBL tLLPrepaidClaimUnderWriteBL = new LLPrepaidClaimUnderWriteBL();
//        if (!tLLPrepaidClaimUnderWriteBL.submitData(tVData, "BACK")) {
//        	 CError.buildErr(tLLClaimUserSchema, "当前操作员没有预付审批权限!");
//	         System.out.println();
//        }
    	int count1=0;
    	int count2=0;
    	for(int i=1;i<5;i++){
    		count1 = count1 ++;
    		count2 = count2++;
    		System.out.println("tCount1:"+count1);
    		System.out.println("tCount2:"+count2);
    	}
    }
}
