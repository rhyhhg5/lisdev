package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 理赔-事件保存关联逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author：zhangtao
 * @version：1.0
 * @CreateDate：2005-03-18
 */
public class CaseEventSaveBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  /** 往后面传输的数据库操作 */
  private MMap map = new MMap();
  /**用户登陆信息 */
  private GlobalInput mGlobalInput = new GlobalInput();
  /** 事件信息 */
  private LLSubReportSchema mLLSubReportSchema = new LLSubReportSchema();
  /** 案件事件关联 */
  LLCaseRelaSchema  mLLCaseRelaSchema = new LLCaseRelaSchema();


  public CaseEventSaveBL()
  {
  }

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   * @param: cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate)
  {
    //将操作数据拷贝到本类中
    mInputData = (VData) cInputData.clone();
    mOperate = cOperate;

System.out.println("start submit...");

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
    {
        return false;
    }

System.out.println("after getInputData");

    //根据赔案号、保单号查询保单、赔案、核赔、核赔错误、用户权限信息
    if (!getBaseData())
    {
        return  false;
    }

System.out.println("after getBaseData");

    //数据操作业务处理
    if (!dealData())
    {
        return false;
    }

System.out.println("after dealData");

    //准备给后台的数据
    if (!prepareOutputData())
    {
       return false;
    }

System.out.println("after prepareOutputData");

    //数据提交
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, ""))
    {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        CError tError = new CError();
        tError.moduleName = "ICaseCureBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";
        this.mErrors.addOneError(tError);
        return false;
    }
    mInputData = null;

    return true;
  }

  private boolean dealData()
  {
      if (mOperate.equals("EVENTSAVE|CASE"))
      {
          String tLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
          String eventNo = PubFun1.CreateMaxNo("EVENTNO", tLimit);

          mLLSubReportSchema.setSubRptNo(eventNo);
          mLLSubReportSchema.setMakeDate(PubFun.getCurrentDate());
          mLLSubReportSchema.setMakeTime(PubFun.getCurrentTime());
          mLLSubReportSchema.setOperator(mGlobalInput.Operator);
          mLLSubReportSchema.setMngCom(mGlobalInput.ManageCom);
          mLLSubReportSchema.setModifyDate(PubFun.getCurrentDate());
          mLLSubReportSchema.setModifyTime(PubFun.getCurrentTime());


          String CaseRelaNo = PubFun1.CreateMaxNo("CASERELANO", tLimit);
          mLLCaseRelaSchema.setCaseRelaNo( CaseRelaNo );
          mLLCaseRelaSchema.setSubRptNo(eventNo);

          map.put(mLLSubReportSchema, "INSERT");
          map.put(mLLCaseRelaSchema, "INSERT");
      }

    return true;
  }

  private boolean getInputData(VData cInputData)
  {
      mGlobalInput.setSchema(
            (GlobalInput)
            cInputData.getObjectByObjectName("GlobalInput",0));

      mLLSubReportSchema =
            (LLSubReportSchema)
            cInputData.getObjectByObjectName("LLSubReportSchema", 0);

      mLLCaseRelaSchema =
                (LLCaseRelaSchema)
                cInputData.getObjectByObjectName("LLCaseRelaSchema", 0);

     if (mLLCaseRelaSchema.getCaseNo() == null ||
         "".equals(mLLCaseRelaSchema.getCaseNo()))
     {
         // @@错误处理
         CError tError = new CError();
         tError.moduleName = "CaseEventSaveBL";
         tError.functionName = "getInputData";
         tError.errorMessage = "理赔号不能为空!";
         mErrors.addOneError(tError);

         return false;
     }


      return true;
  }

  private boolean getBaseData()
  {

      return true;
  }

  private boolean prepareOutputData()
  {
      try
      {
          mInputData.clear();
          mInputData.add(map);
      }
      catch (Exception ex)
      {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "ICaseCureBL";
          tError.functionName = "prepareOutputData";
          tError.errorMessage = "在准备往后层处理所需要的数据时出错:" +
                                ex.toString();
          mErrors.addOneError(tError);
          return false;
      }

      return true;
  }

  public VData getResult()
  {
      return mResult;
  }

}
