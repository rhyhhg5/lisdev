package com.sinosoft.lis.llcase;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.bl.LLCaseBL;
import com.sinosoft.lis.bl.LLRegisterBL;
import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.db.LLCasePolicyDB;
import com.sinosoft.lis.db.LLClaimDB;
import com.sinosoft.lis.db.LLExemptionDB;
import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.db.LLHospCaseDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLAppClaimReasonSchema;
import com.sinosoft.lis.schema.LLCasePolicySchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLRgtAffixSchema;
import com.sinosoft.lis.vbl.LLCaseBLSet;
import com.sinosoft.lis.vbl.LLCasePolicyBLSet;
import com.sinosoft.lis.vbl.LLRegisterBLSet;
import com.sinosoft.lis.vschema.LLAppClaimReasonSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LLClaimSet;
import com.sinosoft.lis.vschema.LLExemptionSet;
import com.sinosoft.lis.vschema.LLRegisterSet;
import com.sinosoft.lis.vschema.LLRgtAffixSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.wasforwardxml.project.pad.util.FTPReplyCodeName;
import com.sinosoft.wasforwardxml.project.pad.util.FTPTool;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 立案业务逻辑保存处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Xx
 * @version 1.0
 */
public class RegisterBL
{
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private String mOperate;
    private FTPTool tFTPTool;
    private String mFTPPath;
    private PubFun1 pf1 = new PubFun1();
    private PubFun pf =new PubFun();
    private SSRS insuredSSRS = new SSRS();


    private LLRegisterBL mLLRegisterBL = new LLRegisterBL();
    private LLRegisterBLSet mLLRegisterBLSet = new LLRegisterBLSet();

    private LLCasePolicyBLSet mLLCasePolicyBLSet = new LLCasePolicyBLSet();
    private LLCaseBL mLLCaseBL = new LLCaseBL();
    private LLCaseBLSet mLLCaseBLSet = new LLCaseBLSet();

    private LLRgtAffixSet mLLRgtAffixSet = new LLRgtAffixSet();

    LLAppClaimReasonSchema mLLAppClaimReasonSchema = null;
    LLAppClaimReasonSet mLLAppClaimReasonSet = null;

    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCasePolicySchema mLLCasePolicySchema = new LLCasePolicySchema();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private GlobalInput mGlobalInput = new GlobalInput();
//    private LLSubReportSet mLLSubReportSet = new LLSubReportSet();
//    private LLReportSchema mLLReportSchema = new LLReportSchema(); //向BLS中传递数据

    private GlobalInput mG = new GlobalInput();
    String strHandler = "";
    String Can_RgtNo = "";
    String mCancleReason = "";
    String RgtReason = "";
    String Can_CaseNo = "";
    String mCancleRemark = "";
    private String mTransactionNum;
    // String RgtObj="";
    // String RgtObjNo = "";

    public RegisterBL()
    {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("RegisterBL begin......");
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        if (!getInputData(cInputData))
            return false;
        //理算中的立案信息查询
//    if (cOperate.equals("QUERY||MAIN")||cOperate.equals("QUERYALL"))
//    {
//      if(!queryData())
//        return false;
//    }
        //立案中的立案信息查询
//    if(cOperate.equals("QueryRgtInfo"))
//    {
//      if (!getInputData(cInputData))
//        return false;
//      if(!QueryRgtInfo())
//        return false;
//    }
        if (cOperate.equals("INSERT") || cOperate.equals("UPDATE"))
        {
            System.out.println("abcdefg");
            if (!dealData())
                return false;
            System.out.println("---dealData---");

            PubSubmit ps = new PubSubmit();
            ps.submitData(this.mResult, "");
            if (!ps.submitData(this.mResult, ""))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(ps.mErrors);
                return false;
            }

        }
        if (cOperate.equals("UPDATE"))
        {
            if (!updateData())
                return false;
        }
        if (cOperate.equals("ReturnData"))
        {
            if (!ReturnData())
                return false;
        }
        if (cOperate.equals("RGTCANCEL"))
        {
            if (!CancelRgt())
                return false;
        }
        if (cOperate.equals("CASECANCEL"))
        {
        	if (!CancelCase())
                return false;
        	
        	//深圳外包个险对接案件同步撤件报文
        	 String SZDSSQL = "select 1 from llcase llc,llhospcase llh where llc.caseno = llh.caseno and llc.mngcom like '8695%' and llh.casetype = '15' and llh.hcno = 'SZWB09' and llh.caseno='"+Can_CaseNo+"' with ur";
     		ExeSQL RgtTypeExeSQL = new ExeSQL();
     		SSRS RgtTypeSSRS = RgtTypeExeSQL.execSQL(SZDSSQL);
     		if(RgtTypeSSRS.getMaxRow()>0) { 
     			if(!uploadXMLToFTP()) {
    				return false;
    			}
     		}
            
        }
        if (cOperate.equals("RECEIPT||UPDATE"))
        {
            if (!CompleteReceipt())
                return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
    
    /**
     * 上传问题件到FTP
     * @return
     */
    private boolean uploadXMLToFTP() {
    	System.out.println("--Star uploadFTP--");
    	
    	if(!getConn()) {
    		return false;
    	}
    	
    	if(!uploadXML()) {
    		return false;
    	}
    	
    	return true;
    }
    
    /***
	 * 错误信息
	 * @param args
	 */
	private void buildError(String szFunc,String szErrMsg){
		CError tCError = new CError();
		tCError.moduleName="RegisterBL";
		tCError.functionName=szFunc;
		tCError.errorMessage=szErrMsg;
		System.out.println(szFunc+"--"+szErrMsg);
		this.mErrors.addOneError(tCError);
	}
    
    /**
     * 连接FTP
     * @return
     */
    private boolean getConn() {
    	System.out.println("--Star getConn--");
    	String getIPPort = "select codename ,codealias from ldcode where codetype='LLTYScan' and code='IP/Port' ";
		String getUserPs = "select codename ,codealias from ldcode where codetype='LLTYScan' and code='User/Pass' ";
		SSRS tIPSSRS = new ExeSQL().execSQL(getIPPort);
		SSRS tUPSSRS = new ExeSQL().execSQL(getUserPs);
		if (tIPSSRS.getMaxRow() < 1 || tUPSSRS.getMaxRow() < 1) { 
			buildError("getConn()", "ftp配置有误");
			return false;
		}
		tFTPTool = new FTPTool(tIPSSRS.GetText(1, 1), tUPSSRS.GetText(1, 1),
				tUPSSRS.GetText(1, 2), Integer.parseInt(tIPSSRS.GetText(1, 2)));
		try {
			if (!tFTPTool.loginFTP()) {
				buildError("getConn()", tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE));
				return false;
			}
		} catch (SocketException e) {
			buildError("getConn()", "FTP连接异常");
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			buildError("getConn()", "FTP连接异常");
			e.printStackTrace();
			return false;
		}
		return true;
    }
    
    /**
     * 上载XML到Ftp
     * @return
     */
    private boolean uploadXML() {
    	System.out.println("--Star uploadXML--");
    	    	
    	String  getPath = "select codename ,codealias from ldcode where codetype='LLSZUpload' and code='FTPPath/LocalPath' ";//若有约定好目录保存FTPPath		
		SSRS tPathSSRS = new ExeSQL().execSQL(getPath);
		if (tPathSSRS.getMaxRow() < 1) {
			buildError("uploadXML()", "ftp服务器路径未配置");
			return false;
		}
		mFTPPath = tPathSSRS.GetText(1, 2) + pf.getCurrentDate2() + "/";
		//获取核心根目录
		String tUIRoot= CommonBL.getUIRoot();
		if(tUIRoot==null){
			buildError("uploadXML()", "没有查到应用目录");
			return false;
		}
		System.out.println(tUIRoot);
		//备份数据核心系统存储路径
		String tFileCorePath = tUIRoot + mFTPPath;	
//		tFileCorePath = "D:\\";
		System.out.println("核心路径：" + tFileCorePath);
		String insuredSQL = "select customerno,customername from llcase where caseno = '" + Can_CaseNo + "' with ur";
    	ExeSQL insuredExeSQL = new ExeSQL();
    	insuredSSRS = insuredExeSQL.execSQL(insuredSQL);
    	if(insuredSSRS.getMaxRow()<0) {
    		buildError("createXML()", "案件信息查询异常");
			return false;
    	}	
		Document WXDocument = createXML();
		String pName=Can_CaseNo + ".xml";
		StringBuffer mFilePath = new StringBuffer();
		mFilePath.append(tFileCorePath);
		File mFileDir = new File(mFilePath.toString());
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录失败！" + mFileDir.getPath());
				buildError("uploadXML()", "创建目录失败！" + mFileDir.getPath());
				return false;
			}
		}
		File tempFile=new File(mFilePath.toString() + pName);
		if(!tempFile.exists()){
			try{
				if(!tempFile.createNewFile()){
					System.err.println("创建文件失败！" + tempFile.getPath());
					buildError("uploadXML()", "创建文件失败！" + tempFile.getPath());
					return false;
				}
			}catch(Exception ex){
				System.err.println("创建文件时出现异常！" + tempFile.getPath());
				ex.printStackTrace();
				buildError("uploadXML()", "创建文件时出现异常！" + tempFile.getPath());
				return false;
			}
				
		}
		try {
			FileOutputStream tFos = new FileOutputStream(mFilePath.toString()+ pName);
			JdomUtil.output(WXDocument, tFos);
			tFos.close();
			//FTP上创建文件夹
			if(!tFTPTool.makeDirectory("/" + mFTPPath)){
            	System.out.println("新建目录已存在");
            }
			tFTPTool.upload("/" + mFTPPath, mFilePath.toString() + pName);
			
		} catch (IOException ex) {
			ex.printStackTrace();
			buildError("uploadXML()", "上传文件出现异常");
			return false;			
		}
		//断开FTP连接
		tFTPTool.logoutFTP();
		
		return true;
    }
    
    /**
     * 创建问题件XML
     * @return
     */
    private Document createXML() {
    	System.out.println("--Star createXML--");
    	
    	Element RootData = new Element("PACKET");
    	RootData.addAttribute("type", "REQUEST");
    	RootData.addAttribute("version", "1.0");
    	
    	//创建头部
    	Element HeadData = new Element("HEAD");
    	Element RequestType = new Element("REQUEST_TYPE");
    	RequestType.setText("SZDSCancle");
    	HeadData.addContent(RequestType);
    	Element TransactionNum = new Element("TRANSACTION_NUM");
    	String strOC = "SZDSCancle00"+pf.getCurrentDate2();
		mTransactionNum = strOC + pf1.CreateMaxNo("SZDS" + pf.getCurrentDate2(), 10);
		System.out.println("mTransactionNum:"+mTransactionNum);
    	TransactionNum.setText(mTransactionNum);
    	HeadData.addContent(TransactionNum);
    	RootData.addContent(HeadData);
    	//创建BODY
    	Element BodyData = new Element("BODY");
    	Element tCaseData = new Element("CASE_DATA");
    	Element tCaseNo = new Element("CASENO");
    	tCaseNo.setText(Can_CaseNo);
    	tCaseData.addContent(tCaseNo);
    	
    	Element tCustomerNo = new Element("CUSTOMERNO");
    	tCustomerNo.setText(insuredSSRS.GetText(1, 1));
    	tCaseData.addContent(tCustomerNo);
    	
    	Element tCustomerNname = new Element("CUATOMERNAME");
    	tCustomerNname.setText(insuredSSRS.GetText(1, 2));
    	tCaseData.addContent(tCustomerNname);
    	
    	String cRNameSQL="select codename from ldcode where 1 = 1 and codetype = 'llcasecancel' and code = '"+mCancleReason+" ' with ur";
    	ExeSQL CancleName = new ExeSQL();
    	SSRS CanCleSSRS = CancleName.execSQL(cRNameSQL);
    	Element tCancleReason = new Element("CANCLEREASON");
    	if(CanCleSSRS.getMaxRow()>0) {    		
    		tCancleReason.setText(CanCleSSRS.GetText(1, 1));
    	}
    	tCaseData.addContent(tCancleReason);
    	
    	Element tCancleDate = new Element("CANCLEDATE");
    	tCancleDate.setText(pf.getCurrentDate());
    	tCaseData.addContent(tCancleDate);
    	
    	Element tCancleRemark = new Element("CANCLEREMARK");
    	String cRemarkSQL="select cancleremark from llcase where caseno = '" +Can_CaseNo+ "' with ur";
    	ExeSQL cRemarkName = new ExeSQL();
    	SSRS cRemarkSSRS = cRemarkName.execSQL(cRemarkSQL);
    	mCancleRemark = cRemarkSSRS.GetText(1, 1);
    	tCancleRemark.setText(mCancleRemark);
    	tCaseData.addContent(tCancleRemark);
    	
    	BodyData.addContent(tCaseData);
    	RootData.addContent(BodyData);
    	Document tDocument = new Document(RootData);
    	
    	return tDocument;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData()
    {
        //选择
//    if (!dealHandler())
//    {
//      CError tError = new CError();
//      tError.moduleName = "RegisterBL";
//      tError.functionName = "submitData";
//      tError.errorMessage = "确定审核人失败!";
//      this.mErrors .addOneError(tError) ;
//      return false;
//    }
        prepareOutputData();

//    RegisterBLS tRegisterBLS = new RegisterBLS();
//    System.out.println("Start RegisterBL Submit...");
//    if (!tRegisterBLS.submitData(mInputData,mOperate))

//    {
//      // @@错误处理
//      this.mErrors.copyAllErrors(tRegisterBLS.mErrors);
//      CError tError = new CError();
//      tError.moduleName = "RegisterBL";
//      tError.functionName = "submitData";
//      tError.errorMessage = "数据提交失败!";
//      this.mErrors .addOneError(tError) ;
//      return false;
//    }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        // 暂交费查询条件
//    if (mOperate.equals("QUERY||MAIN"))
//    {
//      mLLRegisterBL.setSchema((LLRegisterSchema)cInputData.getObjectByObjectName("LLRegisterSchema",0));
//      mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
//    }
//    if (mOperate.equals("QUERYALL"))
//    {
//      mLLRegisterBL.setSchema((LLRegisterSchema)cInputData.getObjectByObjectName("LLRegisterSchema",0));
//      mLLCaseBL.setSchema((LLCaseSchema)cInputData.getObjectByObjectName("LLCaseSchema",0));
//     mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
//    }
        if (mOperate.equals("RECEIPT||UPDATE"))
        {
            mLLCaseSchema.setSchema((LLCaseSchema) cInputData.
                                    getObjectByObjectName("LLCaseSchema", 0));
            mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0));
        }
        if (mOperate.equals("INSERT") || mOperate.equals("UPDATE"))
        {
            //mLLRegisterBLSet.set((LLRegisterSet)cInputData.getObjectByObjectName("LLRegisterSet",0));
            //mLLCaseBLSet.set((LLCaseSet)cInputData.getObjectByObjectName("LLCaseSet",0));
            mLLRegisterSchema.setSchema((LLRegisterSchema) cInputData.
                                        getObjectByObjectName(
                    "LLRegisterSchema", 0));
            mLLCaseSchema.setSchema((LLCaseSchema) cInputData.
                                    getObjectByObjectName("LLCaseSchema", 0));
            mLLRgtAffixSet.set((LLRgtAffixSet) cInputData.getObjectByObjectName(
                    "LLRgtAffixSet", 0));
            mLLAppClaimReasonSet = (LLAppClaimReasonSet) cInputData.
                                   getObjectByObjectName("LLAppClaimReasonSet",
                    0);
            mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0));
        }
        //立案中查询立案信息的函数
//    if(mOperate.equals("QueryRgtInfo"))
//    {
//      mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
//      System.out.println("接受的管理机构是"+mG.ManageCom);
//      mLLRegisterSchema.setSchema((LLRegisterSchema)cInputData.getObjectByObjectName("LLRegisterSchema",0));
//      mLLCasePolicySchema.setSchema((LLCasePolicySchema)cInputData.getObjectByObjectName("LLCasePolicySchema",0));
//      mLLCaseSchema.setSchema((LLCaseSchema)cInputData.getObjectByObjectName("LLCaseSchema",0));
//    }
//    if(mOperate.equals("ReturnData"))
//    {
//      mLLRegisterSchema.setSchema((LLRegisterSchema)cInputData.getObjectByObjectName("LLRegisterSchema",0));
//    }
        if (mOperate.equals("CASECANCEL")||mOperate.equals("RGTCANCEL"))
        {
            LLCaseSchema tLLCaseSchema = new LLCaseSchema();
            tLLCaseSchema = (LLCaseSchema) cInputData.getObjectByObjectName("LLCaseSchema",0);
            Can_CaseNo = tLLCaseSchema.getCaseNo();//撤件的时的理赔号
            Can_RgtNo = tLLCaseSchema.getRgtNo();//团体批次号
            System.out.println("------------"+Can_CaseNo+"-----------------");
            System.out.println("------------"+Can_RgtNo+"-----------------");
            mCancleReason = tLLCaseSchema.getCancleReason();
            mCancleRemark = tLLCaseSchema.getCancleRemark();
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",0);
        }
        return true;
    }

    /**
     * 查询符合条件的暂交费信息
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean queryData()
    {
        SQLString sqlObj = new SQLString("LLRegister");
        LLRegisterSchema aSchema = mLLRegisterBL.getSchema();
        sqlObj.setSQL(5, aSchema);
        String sql = sqlObj.getSQL();
        if (sql.toLowerCase().indexOf("where") == -1)
        {
            sql = sql + " where 1=1 and MngCom like '" + mG.ManageCom + "%'";
        }
        else
        {
            sql = sql + " and MngCom like '" + mG.ManageCom + "%' ";
        }
        System.out.println("查询的结果是+sql" + sql);
        if (mLLCaseBL.getCustomerName() != null)
        {
            if (mLLCaseBL.getCustomerName().length() > 0)
            {
                sql = sql +
                        " and rgtno in(select rgtno from llcase where CustomerName='" +
                      mLLCaseBL.getCustomerName() + "')";
            }
        }
        System.out.println("添加了管理机构后的查询语句是" + sql);
        mResult.clear();

        if (mOperate.equals("QUERYALL"))
        {
            //处于调查中的状态不能进行查询到
            sql = sql + " and Handler = '" + mG.Operator +
                    "' and rgtno not in (select rgtno from llsurvey where surveyflag = '1') "; //审核人为当前用户

            System.out.println("执行的语句是" + sql);
            SSRS tSSRS1 = new SSRS();
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS1 = tExeSQL.execSQL(sql);
            SSRS tSSRS3 = null;

            for (int i = 1; i <= tSSRS1.getMaxRow(); i++)
            {
                String tRgtno = tSSRS1.GetText(i, 1);

                String tLLCaseSql = "select * from llcase where rgtno='" +
                                    tRgtno + "' and rownum<2";
                if (i == 1)
                {
                    tSSRS3 = tExeSQL.execSQL(tLLCaseSql);
                }
                else
                {
                    SSRS tSSRS2 = tExeSQL.execSQL(tLLCaseSql);
                    tSSRS3.addRow(tSSRS2);
                }
            }
            if (tSSRS3 != null)
                tSSRS1.addCol(tSSRS3);
            String returnStr = tSSRS1.encode();
            System.out.println("returnStr===" + returnStr);
            mResult.add(returnStr);
            if (returnStr.length() == 0)
            {
                CError tError = new CError();
                tError.moduleName = "LLRegisterBL";
                tError.functionName = "queryData";
                tError.errorMessage = "立案记录查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        else
        {
            LLRegisterDB tLLRegisterDB = new LLRegisterDB();
            mLLRegisterBLSet.set(tLLRegisterDB.executeQuery(sql)); //承载了第一次查询的结果集
            if (tLLRegisterDB.mErrors.needDealError() == true)
            {
                this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LLRegisterBL";
                tError.functionName = "queryData";
                tError.errorMessage = "立案记录查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (mLLRegisterBLSet.size() == 0)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LLRegisterBL";
                tError.functionName = "queryData";
                tError.errorMessage = "未找到相关数据!";
                this.mErrors.addOneError(tError);
                return false;
            }
            String a_HandlerName = CaseFunPub.show_People(mG.Operator);
            mResult.add(a_HandlerName);
            mResult.add(mLLRegisterBLSet);
            LLCasePolicyDB tLLCasePolicyDB = new LLCasePolicyDB();
            tLLCasePolicyDB.setRgtNo(mLLRegisterBL.getRgtNo());
            mLLCasePolicyBLSet.set(tLLCasePolicyDB.query());
            if (tLLCasePolicyDB.mErrors.needDealError() == true)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLLCasePolicyDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LLRegisterBL";
                tError.functionName = "queryData";
                tError.errorMessage = "立案记录查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mResult.add(mLLCasePolicyBLSet);
        }
        return true;
    }

    private boolean QueryRgtInfo()
    {
        System.out.println("管理机构是" + mG.ManageCom);
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        LLRegisterSet tLLRegisterSet = new LLRegisterSet();
        tLLRegisterDB.setSchema(mLLRegisterSchema);
        tLLRegisterSet.set(tLLRegisterDB.query());
        //显示出查询语句的函数
        SQLString sqlObj = new SQLString("LLRegister");
        LLRegisterSchema aLLRegisterSchema = mLLRegisterSchema;
        sqlObj.setSQL(5, aLLRegisterSchema);
        String sql = sqlObj.getSQL();

        if (sql.toLowerCase().indexOf("where") == -1)
        {
            sql = sql + " where 1=1 and MngCom like  '" + mG.ManageCom.trim() +
                  "%' ";
        }
        else
        {
            sql = sql + " and MngCom like '" + mG.ManageCom.trim() + "%' ";
        }
        //判断当保单号是空和非空的情况
        if (mLLCasePolicySchema.getPolNo() == null ||
            mLLCasePolicySchema.getPolNo().equals(""))
        {
            if (mLLCaseSchema.getCustomerName() == null ||
                mLLCaseSchema.getCustomerName().equals(""))
            {
//          sql = sql ;
            }
            else
            {
                sql = sql +
                        " and RgtNo in ( select RgtNo from LLCase where CustomerName = '" +
                      mLLCaseSchema.getCustomerName()
                      + "' ) and MngCom like '" + mG.ManageCom +
                      "%' order by RgtNo";
            }
        }
        if (!(mLLCasePolicySchema.getPolNo() == null ||
              mLLCasePolicySchema.getPolNo().equals("")))
        {
            if (mLLCaseSchema.getCustomerName() == null ||
                mLLCaseSchema.getCustomerName().equals(""))
                sql = sql + " and rgtno in( select LLCase.rgtno from LLCase,LLCasePolicy where LLCasePolicy.PolNo='" +
                      mLLCasePolicySchema.getPolNo()
                      +
                      "' and LLCase.RgtNo=LLCasePolicy.RgtNo ) order by rgtno ";
            if (mLLCaseSchema.getCustomerName().length() > 0)
            {
                sql = sql + " and rgtno in( select LLCase.rgtno from LLCase,LLCasePolicy where LLCasePolicy.PolNo='" +
                      mLLCasePolicySchema.getPolNo()
                      +
                        "' and LLCase.RgtNo=LLCasePolicy.RgtNo and LLCase.CustomerName = '"
                      + mLLCaseSchema.getCustomerName() + "' ) order by rgtno ";
            }
        }

        ExeSQL exesql = new ExeSQL();
        SSRS ssrs = new SSRS();
        ssrs = exesql.execSQL(sql);
        LLRegisterSet yLLRegisterSet = new LLRegisterSet();
        for (int count = 1; count <= ssrs.getMaxRow(); count++)
        {
            System.out.println("立案记录的条数是" + count);
            LLRegisterSchema yLLRegisterSchema = new LLRegisterSchema();
            yLLRegisterSchema.setRgtNo(ssrs.GetText(count, 1));
            //  yLLRegisterSchema.setRptNo(ssrs.GetText(count,5));
            yLLRegisterSchema.setRgtDate(ssrs.GetText(count, 14));
            yLLRegisterSchema.setCaseGetMode(ssrs.GetText(count, 43));
            System.out.println("当count＝" + count + "时，立案号码是" +
                               ssrs.GetText(count, 1));

            SSRS ssrs2 = new SSRS();
            String sql2 =
                    "select CustomerNo,CustomerName from LLCase where RgtNo = '" +
                    ssrs.GetText(count, 1) + "'";
            ssrs2 = exesql.execSQL(sql2);
            if (ssrs2.getMaxRow() > 0)
            {
                yLLRegisterSchema.setRgtantAddress(ssrs2.GetText(1, 1));
                yLLRegisterSchema.setRgtantName(ssrs2.GetText(1, 2)); //用立案人姓名来记录事故者姓名
                System.out.println("姓名是" + ssrs2.GetText(1, 1));
            }
            else
            {
                yLLRegisterSchema.setRgtantName("无记录");
                yLLRegisterSchema.setRgtantAddress("无记录");
            }
            yLLRegisterSet.add(yLLRegisterSchema);
            System.out.println("yLLRegisterSet的条数是" + yLLRegisterSet.size());
        }

        if (tLLRegisterDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLReportBL";
            tError.functionName = "queryData";
            tError.errorMessage = "查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (ssrs.getMaxRow() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLReportBL";
            tError.functionName = "queryData";
            tError.errorMessage = "未找到相关数据!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mResult.clear();
        mResult.add(yLLRegisterSet);
        return true;
    }

    private boolean updateData()
    {
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        tLLRegisterSchema = mLLRegisterBLSet.get(1);
        mLLRegisterBL.setSchema(tLLRegisterSchema);
        mLLRegisterBL.setDefaultValue();
        tLLRegisterSchema = mLLRegisterBL.getSchema();
        tLLRegisterDB.setSchema(tLLRegisterSchema);
        tLLRegisterDB.update();
        if (tLLRegisterDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLRegisterBL";
            tError.functionName = "updateData";
            tError.errorMessage = "保单查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
/**
 * 给团体保单做撤件
 * @return
 */
    private boolean CancelRgt()
    {
    	
        LLRegisterSet tLLRegisterSet = new LLRegisterSet();
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        System.out.println("立案号码是" + Can_RgtNo);
        tLLRegisterDB.setRgtNo(Can_RgtNo);
        if(tLLRegisterDB.getInfo()){
            if (!(tLLRegisterDB.getDeclineFlag() == null ||
                  tLLRegisterDB.getDeclineFlag().equals(""))){
                if (tLLRegisterDB.getDeclineFlag().equals("1")){
                    this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LLRegisterBL";
                    tError.functionName = "CancelRgt";
                    tError.errorMessage = "该批次已经进行了撤销，不能对此进行撤销！";
                    this.mErrors.addOneError(tError);
                    return false;
                }

            }
            boolean flag = false;
            String scase = "select 1 from llcase where rgtno='"+tLLRegisterDB.getRgtNo()+"' and rgtstate<>'14' fetch first rows only with ur";
            ExeSQL tExe = new ExeSQL();
            String result  =  tExe.getOneValue(scase);
            if("1".equals(result)){
            	flag = true;
            }
            System.out.println("tLLRegisterDB.getRgtState()  && !flag" + !(tLLRegisterDB.getRgtState().equals("03") && !flag));
             
             
            if (!tLLRegisterDB.getRgtState().equals("01") && 
            		!tLLRegisterDB.getRgtState().equals("02") && 
            		!(tLLRegisterDB.getRgtState().equals("03") && !flag) && 
            		!"07".equals(tLLRegisterDB.getRgtState())){
            	    this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LLRegisterBL";
                    tError.functionName = "CancelRgt";
                    tError.errorMessage = "该批次下所有个人结案完毕，不能对此进行撤销！";
                    this.mErrors.addOneError(tError);
                    return false;
            }
            
            if (!tLLRegisterDB.getHandler1().equals(mGlobalInput.Operator)
                && !LLCaseCommon.checkUPUpUser(mGlobalInput.Operator,tLLRegisterDB.getHandler1())){
                this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LLRegisterBL";
                tError.functionName = "CancelRgt";
                tError.errorMessage = "您不是该批次处理人，不能撤销该批次！";
                this.mErrors.addOneError(tError);
                return false;
            }
            
        }else{
            this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLRegisterBL";
            tError.functionName = "CancelRgt";
            tError.errorMessage = "批次查询失败！";
            this.mErrors.addOneError(tError);
            return false;
        }

 
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        String tCaseSQL = "SELECT * FROM llcase WHERE rgtno='"+Can_RgtNo+"' AND rgtstate IN ('05','06','09','10','11','12')";
        tSSRS = tExeSQL.execSQL(tCaseSQL);
        if (tSSRS.getMaxRow() > 0)
        {
            CError tError = new CError();
            tError.moduleName = "LLRegisterBL";
            tError.functionName = "updateData";
            tError.errorMessage = "该批次下案件状态不允许批次撤件！";
            System.out.println("不能进行撤销数据");
            this.mErrors.addOneError(tError);
            return false;
        }
        
        
        
        //删除已产生的赔案信息
        String sql0 = "delete from LLClaim where rgtno='"+Can_RgtNo+"'";
        String sql_p = "delete from LLClaimPolicy where rgtno='"+Can_RgtNo+"'";
        String sql_d = "delete from LLClaimDetail where rgtno='"+Can_RgtNo+"'";
        String sql_acc = "delete from LCInsureAccTrace where OtherNo in (select caseno from llcase where rgtno='"+Can_RgtNo+"')";
        //删除赔案信息增加ljsget,ljsgetclaim的数据	#2368 cbs00071160关于撤件案件造成的宽限期过后保单状态的问题
        String sql_ljsget_p = "delete from ljsget where otherno='"+Can_RgtNo+"'";
        String sql_ljsget_c = "delete from ljsget where otherno in (select caseno from llcase where rgtno='"+Can_RgtNo+"') ";
        String sql_ljsgetclaim_p = "delete from ljsgetclaim where otherno='"+Can_RgtNo+"'";
        String sql_ljsgetclaim_c = "delete from ljsgetclaim where otherno in (select caseno from llcase where rgtno='"+Can_RgtNo+"') ";

        
        //删除审批相关的信息
        String sql_uwm = "delete from LLClaimUWmain where rgtno='"+Can_RgtNo+"'";
        String sql_ud = "delete from LLClaimUWDetail where caseno in (select caseno from llcase where rgtno='"+Can_RgtNo+"')";
        String sql_uw = "delete from LLClaimUnderwrite where rgtno='"+Can_RgtNo+"'";
        
        tLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
        tLLRegisterSchema.setDeclineFlag("1"); //撤销标准
        tLLRegisterSchema.setRgtReason(mCancleRemark);
        tLLRegisterSchema.setCanceler(mGlobalInput.Operator);
        tLLRegisterSchema.setCancelReason(mCancleReason);
        tLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
        tLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());

        
        MMap map = new MMap();
        /*
         * 判断是否是天津社保案件，TRUE 更改状态  false 不做处理
         * */
        ExeSQL tExe = new ExeSQL();
        String querySQL = "select RgtType from llregister where rgtno='"+Can_RgtNo+"'";
        
        String rgtType_result  =  tExe.getOneValue(querySQL);
        if("8".equals(rgtType_result)){
        	String squerySQL = "select caseno from llcase where rgtno='"+Can_RgtNo+"'";
        	SSRS rgtType_resultt  =  tExe.execSQL(squerySQL);//下标从1开始
        	if(rgtType_resultt!=null&&rgtType_resultt.MaxRow>0){
        		for(int i=1;i<=rgtType_resultt.MaxRow;i++){
        			String ConfirmState_update = "update LLHospCase set ConfirmState = '2' where caseno = '"+rgtType_resultt.GetText(i, 1)+"'";
        			map.put(ConfirmState_update,"UPDATE");
        		}
        	}
        }
        
        map.put(tLLRegisterSchema, "UPDATE");
        map.put(sql0, "DELETE");
        map.put(sql_p, "DELETE");
        map.put(sql_d, "DELETE");
        map.put(sql_acc, "DELETE");
        map.put(sql_uwm, "DELETE");
        map.put(sql_ud, "DELETE");
        map.put(sql_uw, "DELETE");
        map.put(sql_ljsget_p, "DELETE");
        map.put(sql_ljsget_c, "DELETE");
        map.put(sql_ljsgetclaim_p, "DELETE");
        map.put(sql_ljsgetclaim_c, "DELETE");

        LLCaseDB tLLCaseDB = new LLCaseDB();
        LLCaseSet tLLCaseSet = new LLCaseSet();
        LLCaseSet saveLLCaseSet = new LLCaseSet();
        tLLCaseDB.setRgtNo(Can_RgtNo);
        tLLCaseSet = tLLCaseDB.query();
        if(tLLCaseSet.size()>0){
            for (int i=1 ; i<=tLLCaseSet.size();i++){
                LLCaseSchema tLLCaseSchema = new LLCaseSchema();
                tLLCaseSchema = tLLCaseSet.get(i);
                tLLCaseSchema.setCancleReason(mCancleReason);
                tLLCaseSchema.setCancleRemark("团体批次被撤销。");
                tLLCaseSchema.setRgtState("14");
                tLLCaseSchema.setHandler(mGlobalInput.Operator);
                tLLCaseSchema.setCancleDate(PubFun.getCurrentDate());
                tLLCaseSchema.setCancler(mGlobalInput.Operator);
                tLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
                tLLCaseSchema.setModifyTime(PubFun.getCurrentTime());
                saveLLCaseSet.add(tLLCaseSchema);
            }
            map.put(saveLLCaseSet,"UPDATE");
       }


            this.mResult.clear();
            this.mResult.add(map);
        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(this.mResult, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }

//    mInputData.clear();
//    mInputData.addElement(Can_RgtNo);
//    mInputData.addElement(RgtReason);
//    RegisterBLS tRegisterBLS = new RegisterBLS();
//    if (!tRegisterBLS.submitData(mInputData,"CANCEL"))
//   {
//      this.mErrors.copyAllErrors(tRegisterBLS.mErrors);
//      CError tError = new CError();
//      tError.moduleName = "RegisterBL";
//      tError.functionName = "submitData";
//      tError.errorMessage = "案件撤销失败！！！！";
//      this.mErrors .addOneError(tError) ;
//      return false;
//    }
        return true;
    }

    /**
     * 撤销分案
     * 给个单做撤件
     * @return boolean
     */
    private boolean CancelCase()
    {	
    	
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(Can_CaseNo);
        if(!tLLCaseDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLLCaseDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "RegisterBL";
            tError.functionName = "CancelCase";
            tError.errorMessage = "案件信息查询失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        
         
        if(!tLLCaseDB.getHandler().equals(mGlobalInput.Operator) && !tLLCaseDB.getRigister().equals(mGlobalInput.Operator))
        {
            this.mErrors.copyAllErrors(tLLCaseDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "RegisterBL";
            tError.functionName = "CancelCase";
            tError.errorMessage = "您没有权限撤销该案件！";
            this.mErrors.addOneError(tError);
            return false;
        }
        

        LLHospCaseDB tLLHospCaseDB = new LLHospCaseDB();
        tLLHospCaseDB.setCaseNo(Can_CaseNo);
        if (tLLHospCaseDB.getInfo()) {
            if ("1".equals(tLLHospCaseDB.getDealType())) {
                CError.buildErr(this, "非核心系统完成案件不允许撤件，请做申诉纠错");
                return false;
            }
        }

        /**
         * 案件状态校验
         */
        if(tLLCaseDB.getRgtState().equals("05")||tLLCaseDB.getRgtState().equals("06")||
           tLLCaseDB.getRgtState().equals("09")||tLLCaseDB.getRgtState().equals("10")||
           tLLCaseDB.getRgtState().equals("11")||tLLCaseDB.getRgtState().equals("12"))
        {
            this.mErrors.copyAllErrors(tLLCaseDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "RegisterBL";
            tError.functionName = "CancelCase";
            tError.errorMessage = "该案件状态下不允许撤件操作！";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        if(tLLCaseDB.getRgtState().equals("14"))
        {
            this.mErrors.copyAllErrors(tLLCaseDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "RegisterBL";
            tError.functionName = "CancelCase";
            tError.errorMessage = "该案件已被撤销！";
            this.mErrors.addOneError(tError);
            return false;
        }
        //删除已产生的赔案信息
        String sql0 = "delete from LLClaim where CaseNo='"+Can_CaseNo+"'";
        String sql_p = "delete from LLClaimPolicy where CaseNo='"+Can_CaseNo+"'";
        String sql_d = "delete from LLClaimDetail where CaseNo='"+Can_CaseNo+"'";
        String sql_acc = "delete from LCInsureAccTrace where OtherNo='"+Can_CaseNo+"'";
        String sql_accfee = "delete from LCInsureAccFeeTrace where OtherNo='"+Can_CaseNo+"'";
        //删除赔案信息增加ljsget,ljsgetclaim的数据	#2368 cbs00071160关于撤件案件造成的宽限期过后保单状态的问题
        String sql_ljsget_c = "delete from ljsget where otherno='"+Can_CaseNo+"'";
        String sql_ljsgetclaim_c = "delete from ljsgetclaim where otherno='"+Can_CaseNo+"'";
        
        //删除审批相关的信息
        String sql_uwm = "delete from LLClaimUWmain where CaseNo='"+Can_CaseNo+"'";
        String sql_ud = "delete from LLClaimUWDetail where CaseNo='"+Can_CaseNo+"'";
        String sql_uw = "delete from LLClaimUnderwrite where CaseNo='"+Can_CaseNo+"'";
        
//      TODO:如果做过保费豁免，且没有做完，在撤件时将状态置为2
        System.out.println("撤件-豁免情况");
        LLExemptionDB tLLExemptionDB = new LLExemptionDB();
        tLLExemptionDB.setCaseNo(Can_CaseNo);
        LLExemptionSet tLLExemptionSet = tLLExemptionDB.query();
        if(tLLExemptionSet != null && tLLExemptionSet.size() >0){
            for(int i = 1 ;i<= tLLExemptionSet.size();i++){
                tLLExemptionSet.get(i).setState("2");
                tLLExemptionSet.get(i).setOperator(mGlobalInput.Operator);
                tLLExemptionSet.get(i).setModifyDate(PubFun.getCurrentDate());
                tLLExemptionSet.get(i).setModifyTime(PubFun.getCurrentTime());
            } 
        }
        
        
        MMap tmpMap = new MMap();
        /*
         * 判断是否是天津社保案件，TRUE 更改状态  false 不做处理
         * */
        ExeSQL tExe = new ExeSQL();
        String querySQL = "select hospitcode from llhospcase where caseno='"+Can_CaseNo+"'";
        String rgtType_result  =  tExe.getOneValue(querySQL);
        
        if("TJ05".equals(rgtType_result)){
        	String ConfirmState_update = "update LLHospCase set ConfirmState = '2' where caseno = '"+Can_CaseNo+"'";
        	tmpMap.put(ConfirmState_update,"UPDATE");
        	System.out.println("ConfirmState_update状态的更改------------");
        }
       
        tmpMap.put(sql0, "DELETE");
        tmpMap.put(sql_p, "DELETE");
        tmpMap.put(sql_d, "DELETE");
        tmpMap.put(sql_acc, "DELETE");
        tmpMap.put(sql_accfee, "DELETE");
        tmpMap.put(sql_uwm, "DELETE");
        tmpMap.put(sql_ud, "DELETE");
        tmpMap.put(sql_uw, "DELETE");
        tmpMap.put(sql_ljsget_c, "DELETE");
        tmpMap.put(sql_ljsgetclaim_c, "DELETE");
        tmpMap.put(tLLExemptionSet, "UPDATE");
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setSchema(tLLCaseDB.getSchema());
        tLLCaseSchema.setCancleReason(mCancleReason);
        tLLCaseSchema.setCancleRemark(mCancleRemark);
        tLLCaseSchema.setRgtState("14");
        tLLCaseSchema.setCancleDate(PubFun.getCurrentDate());
        tLLCaseSchema.setCancler(mGlobalInput.Operator);
        tLLCaseSchema.setHandler(mGlobalInput.Operator);
        tLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
        tLLCaseSchema.setModifyTime(PubFun.getCurrentTime());
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        System.out.println(tLLCaseSchema.getRgtNo());
        if(tLLCaseSchema.getRgtNo().substring(0,1).equals("P")){
            LLCaseDB aLLCaseDB = new LLCaseDB();
            String tSql = "select * from llcase where rgtno = '"
                          +tLLCaseSchema.getRgtNo()
                          +"' and rgtstate in ('09','14','11','12')";
            System.out.println(tSql);
            LLCaseSet tLLCaseSet = new LLCaseSet();
            tLLCaseSet = tLLCaseDB.executeQuery(tSql);
            tLLRegisterDB.setRgtNo(tLLCaseSchema.getRgtNo());
            if(!tLLRegisterDB.getInfo()){
                this.mErrors.copyAllErrors(tLLCaseDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "RegisterBL";
                tError.functionName = "CancelCase";
                tError.errorMessage = "团体批次信息查询失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
            String allSql="select count(1) from llcase where rgtno='"+tLLCaseSchema.getRgtNo()+"'";
            ExeSQL tExeSQL =new ExeSQL();
            String NumCase = tExeSQL.getOneValue(allSql);
            
            if(tLLCaseSet.size()>=(Integer.parseInt(NumCase)-1)){
                tLLRegisterDB.setRgtState("03");
            }
            tSql = "select * from llcase where rgtno = '"
                          +tLLCaseSchema.getRgtNo()
                          +"' and rgtstate in ('14','12','11')";
            tLLCaseSet = new LLCaseSet();
            tLLCaseSet = tLLCaseDB.executeQuery(tSql);
            if(tLLCaseSet.size()>=(Integer.parseInt(NumCase)-1)){
                tLLRegisterDB.setRgtState("04");
            }
            tmpMap.put(tLLRegisterDB.getSchema(),"UPDATE");
        }
        tmpMap.put(tLLCaseSchema,"UPDATE");
        /* LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setInsuredNo(tLLCaseSchema.getCustomerNo());
        LCPolSet tLCPolSet = tLCPolDB.query();
        int y = tLCPolSet.size();
        for (int t = 1; t <= y; t++) {
            //修改该保单状态为终止(死亡报案终止).
            String opolstate = "" + tLCPolSet.get(t).getPolState();
            tLCPolSet.get(t).setPolState(opolstate.substring(4));
        }
        tmpMap.put(tLCPolSet,"UPDATE");*/
        this.mResult.clear();
        this.mResult.add(tmpMap);

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(this.mResult, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }

    private boolean CompleteReceipt(){
    	System.out.println("账单录入-录入完毕");
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mLLCaseSchema.getCaseNo());
        if(!tLLCaseDB.getInfo()){
            this.mErrors.copyAllErrors(tLLCaseDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "RegisterBL";
            tError.functionName = "CancelCase";
            tError.errorMessage = "案件信息查询失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        String Inputer = ""+tLLCaseDB.getClaimer();
        String Handler = ""+tLLCaseDB.getHandler();

        String tRgtState="";
        String tReceiptFlag="";
        tRgtState=tLLCaseDB.getRgtState();
        tReceiptFlag=tLLCaseDB.getReceiptFlag();
        System.out.println("tRgtState="+tRgtState);
        if (!(tRgtState.equals("01") || tRgtState.equals("02") ||
              tRgtState.equals("08"))) {
            CError.buildErr(this, "该案件状态下不能账单录入!");
            return false;
        }
        //#1738 社保调查 理赔各处理流程支持
        String tReturnMsg = LLCaseCommon.checkSurveyRgtState(mLLCaseSchema.getCaseNo(),tRgtState,"01");
        if(!"".equals(tReturnMsg)){
        	String tMsg = LLCaseCommon.checkSurveyRgtState(mLLCaseSchema.getCaseNo(),tRgtState,"02");
        	if(!"".equals(tMsg)){
            	CError.buildErr(this, tMsg);
                return false;
        	}
        }
        
        if(!Inputer.equals(mG.Operator.trim())
           &&!Inputer.equals(mG.Operator.trim())){
            this.mErrors.copyAllErrors(tLLCaseDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "RegisterBL";
            tError.functionName = "CancelCase";
            tError.errorMessage = "您不是该案件指定的录入人员，无权执行该操作！";
            this.mErrors.addOneError(tError);
            return false;
        }
        LLCaseSchema tLLCaseSchema = tLLCaseDB.getSchema();
        tLLCaseSchema.setReceiptFlag("1");
        MMap tmpMap = new MMap();
        tmpMap.put(tLLCaseSchema,"UPDATE");
        this.mResult.clear();
        this.mResult.add(tmpMap);
        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(this.mResult, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }
    /**
     * 准备需要保存的数据
     */
    private void prepareOutputData()
    {
        // LLRegisterSchema tLLRegisterSchema = mLLRegisterBLSet.get(1);
        LLRegisterSchema tLLRegisterSchema = mLLRegisterSchema;
        String strLimit = PubFun.getNoLimit(mG.ManageCom);
        String RgtNo = PubFun1.CreateMaxNo("RGTNO", strLimit);
        tLLRegisterSchema.setRgtNo(RgtNo);

        tLLRegisterSchema.setMngCom(mG.ManageCom);
        tLLRegisterSchema.setOperator(mG.Operator);
        tLLRegisterSchema.setMakeDate(PubFun.getCurrentDate());
        tLLRegisterSchema.setDeclineFlag("0"); //撤单标志，0未撤单
        tLLRegisterSchema.setMakeTime(PubFun.getCurrentTime());
        tLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
        tLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());
        tLLRegisterSchema.setRgtDate(PubFun.getCurrentDate());

        tLLRegisterSchema.setHandler(strHandler);

        tLLRegisterSchema.setRgtClass("0");
        tLLRegisterSchema.setClmState("0");
        tLLRegisterSchema.setGetMode("0");
        tLLRegisterSchema.setGetIntv("0");
        tLLRegisterSchema.setCalFlag("0");
        tLLRegisterSchema.setEndCaseFlag("0");
        // tLLRegisterSchema.setRgtObj(RgtObj);
        // tLLRegisterSchema.setRgtObjNo(RgtObjNo);

//对号码类型和号码的维护
        //  mLLRegisterBLSet.set(1,tLLRegisterSchema);
        String a_Name1 = CaseFunPub.show_People(mG.Operator.trim());
        String a_Name2 = CaseFunPub.show_People(strHandler.trim());

//    int n = mLLCaseBLSet.size();
//    for (int i = 1; i <= n; i++)
//    {
        LLCaseSchema tLLCaseSchema = mLLCaseSchema;
        strLimit = PubFun.getNoLimit(mG.ManageCom);
        //tLLCaseSchema.setCaseNo(PubFun1.CreateMaxNo("CASENO",strLimit));
        tLLCaseSchema.setCaseNo(RgtNo);
        tLLCaseSchema.setRgtNo(tLLRegisterSchema.getRgtNo());
        tLLCaseSchema.setRgtState("0");
        tLLCaseSchema.setMngCom(mG.ManageCom);
        tLLCaseSchema.setOperator(mG.Operator);
        tLLCaseSchema.setMakeDate(PubFun.getCurrentDate());
        tLLCaseSchema.setMakeTime(PubFun.getCurrentTime());
        tLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
        tLLCaseSchema.setModifyTime(PubFun.getCurrentTime());

        //疯狂的状态
        tLLCaseSchema.setRgtType("0");
        tLLCaseSchema.setRgtState("0");
        tLLCaseSchema.setCustomerNo("0");
        tLLCaseSchema.setCustomerName("0");
        tLLCaseSchema.setAccidentType("0");
        tLLCaseSchema.setReceiptFlag("0");
        tLLCaseSchema.setHospitalFlag("0");
        tLLCaseSchema.setSurveyFlag("0");
        tLLCaseSchema.setFeeInputFlag("0");
        tLLCaseSchema.setCalFlag("0");
        tLLCaseSchema.setUWFlag("0");
        tLLCaseSchema.setDeclineFlag("0");
        tLLCaseSchema.setEndCaseFlag("0");
        tLLCaseSchema.setInvaliHosDays("0");
        tLLCaseSchema.setInHospitalDays("0");
        tLLCaseSchema.setUWState("0");
        //   tLLCaseSchema.setUWNower("0");
        tLLCaseSchema.setAppealFlag("0");
        tLLCaseSchema.setGetMode("0");
        tLLCaseSchema.setGetIntv("0");

        // mLLCaseBLSet.set(i,tLLCaseSchema);
        //根据SubRptNo查询LLSubReport表，并将对CaseNo进行附值
//      String tSubRptNo = tLLCaseSchema.getSubRptNo();
//      System.out.println("报案分案号码是"+tSubRptNo);
//      LLSubReportSet tLLSubReportSet = new LLSubReportSet();
//      LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
//      LLSubReportDB tLLSubReportDB = new LLSubReportDB();
//      tLLSubReportDB.setSubRptNo(tSubRptNo);
//      tLLSubReportSet.set(tLLSubReportDB.query());
//      if(tLLSubReportSet.size()==0)
//      {
//        //分案信息查询失败
//      }
//      else
//      {
//        tLLSubReportSchema.setSchema(tLLSubReportSet.get(1));//主键查询
//        tLLSubReportSchema.setCaseNo(tLLCaseSchema.getCaseNo());
//        System.out.println("分案号码是"+tLLSubReportSchema.getCaseNo());
//        mLLSubReportSet.add(tLLSubReportSchema);
//        System.out.println("在LLSubReport表中加入第"+i+"条记录！");
//      }
//    }
        //判断是否完全立案

//    //查询LLSubReport的个数
//    LLSubReportDB aLLSubReportDB = new LLSubReportDB();
//    aLLSubReportDB.setRptNo(mLLReportSchema.getRptNo());
//    int total_count = aLLSubReportDB.getCount();
//    System.out.println("立案号码是"+mLLReportSchema.getRptNo()+"的报案下的报案分案的个数是"+total_count);
//    //查询出已经立案的分案的个数
//    String x_sql = " select Count(*) from LLSubReport where RptNo = '"
//                 +mLLReportSchema.getRptNo()+"' and CaseNo <> '00000000000000000000' ";
//    ExeSQL exesql = new ExeSQL();
//    SSRS x_ssrs = exesql.execSQL(x_sql);
//    int x_count=0;
//    System.out.println("已经立案的个数是"+x_count);
//    System.out.println("本次立案的个数是"+mLLSubReportSet.size());
//    if(x_ssrs.getMaxRow()>0)
//    {
//      x_count = Integer.parseInt(x_ssrs.GetText(1,1));
//    }
//    if(total_count==(x_count+mLLSubReportSet.size()))
//    {
//      mLLReportSchema.setRgtFlag("1");
//      System.out.println("完全立案");
//    }
//    if(total_count>(x_count+mLLSubReportSet.size()))
//    {
//      mLLReportSchema.setRgtFlag("2");
//      System.out.println("部分立案");
//    }
        int n = mLLRgtAffixSet.size();
        for (int i = 1; i <= n; i++)
        {
            LLRgtAffixSchema tLLRgtAffixSchema = mLLRgtAffixSet.get(i);
            tLLRgtAffixSchema.setRgtNo(tLLRegisterSchema.getRgtNo());
            tLLRgtAffixSchema.setSerialNo(i);
            tLLRgtAffixSchema.setMngCom(mG.ManageCom);
            tLLRgtAffixSchema.setOperator(mG.Operator);
            tLLRgtAffixSchema.setMakeDate(PubFun.getCurrentDate());
            tLLRgtAffixSchema.setMakeTime(PubFun.getCurrentTime());
            tLLRgtAffixSchema.setModifyDate(PubFun.getCurrentDate());
            tLLRgtAffixSchema.setModifyTime(PubFun.getCurrentTime());
            mLLRgtAffixSet.set(i, tLLRgtAffixSchema);
        }

        int m = mLLAppClaimReasonSet.size();
        for (int i = 1; i <= m; i++)
        {
            LLAppClaimReasonSchema tLLAppClaimReasonSchema =
                    mLLAppClaimReasonSet.get(i);
            tLLAppClaimReasonSchema.setRgtNo(tLLRegisterSchema.getRgtNo());
            tLLAppClaimReasonSchema.setCaseNo(tLLCaseSchema.getCaseNo());
            tLLAppClaimReasonSchema.setCustomerNo(tLLCaseSchema.getCustomerNo());
            tLLAppClaimReasonSchema.setMngCom(mG.ManageCom);
            tLLAppClaimReasonSchema.setOperator(mG.Operator);
            tLLAppClaimReasonSchema.setMakeDate(PubFun.getCurrentDate());
            tLLAppClaimReasonSchema.setMakeTime(PubFun.getCurrentTime());
            tLLAppClaimReasonSchema.setModifyDate(PubFun.getCurrentDate());
            tLLAppClaimReasonSchema.setModifyTime(PubFun.getCurrentTime());
            //mLLRgtAffixBLSet.set(i,tLLAppClaimReasonSchema);
        }

//    mInputData.clear();
//    mInputData.add(mLLRegisterBLSet);
//    mInputData.add(mLLCaseBLSet);
//    mInputData.add(mLLRgtAffixBLSet);
//    mInputData.add(mLLSubReportSet);
//    mInputData.add(mLLReportSchema);
        MMap map = new MMap();
        map.put(mLLRegisterSchema, "DELETE&INSERT");
        map.put(mLLCaseSchema, "DELETE&INSERT");
        map.put(mLLRgtAffixSet, "DELETE&INSERT");
        // map.put(mLLSubReportSet,"UPDATE");
        // map.put(mLLReportSchema,"UPDATE");
        map.put(mLLAppClaimReasonSet, "DELETE&INSERT");

        mResult.clear();
        mResult.add(map);
        mResult.add(mLLRegisterSchema);
        // mResult.add(a_Name1);
        //mResult.add(a_Name2);

        //  mResult.add(mLLRegisterBLSet);
        //  mResult.add(mLLCaseBLSet);
        //  mResult.add(mLLRgtAffixBLSet);

    }

    //选取审核人的函数
    private boolean ReturnData()
    {
        String a_RgtName = "";
        String a_HandlerName = "";
        LLRegisterDB aLLRegisterDB = new LLRegisterDB();
        aLLRegisterDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        LLRegisterSet aLLRegisterSet = new LLRegisterSet();
        aLLRegisterSet.set(aLLRegisterDB.query());
        String CaseState = CaseFunPub.getCaseStateByRgtNo(mLLRegisterSchema.
                getRgtNo());
        System.out.println(CaseState);
        if (aLLRegisterSet.size() == 0)
        {
            this.mErrors.copyAllErrors(aLLRegisterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLRegisterBL";
            tError.functionName = "updateData";
            tError.errorMessage = "数据返回失败！！！";
            this.mErrors.addOneError(tError);
            return false;
        }
        mResult.clear();
        a_RgtName = CaseFunPub.show_People(aLLRegisterSet.get(1).getOperator());
        a_HandlerName = CaseFunPub.show_People(aLLRegisterSet.get(1).getHandler());
        mResult.addElement(CaseState);
        mResult.addElement(a_RgtName);
        mResult.addElement(a_HandlerName);
        mResult.add(aLLRegisterSet);
        return true;
    }

    private boolean dealHandler()
    {
        LLCaseCommon tLLCaseCommon = new LLCaseCommon();
        //String strMngCom = "8611";
        String strMngCom;

        strMngCom = mG.ManageCom;
        System.out.println("当前的登陆机构是＝＝＝＝" + strMngCom);
        strHandler = tLLCaseCommon.ChooseAssessor(strMngCom,mLLCaseSchema.getCustomerNo(),"01");
        if (strHandler == null || strHandler.equals(""))
        {
            return false;
        }
        System.out.println("本次操作的审核人是＝＝＝＝" + strHandler);
        return true;
    }

    /*******************************************************************************
     * Name     :isRegister
     * Function :判断该报案信息是否已经立案，若已经立案则不能对此进行重复立案操作
     * Author   :LiuYansong
     * Date     :2003-8-6
     */

    private boolean isRegister()
    {
//    LLReportDB tLLReportDB = new LLReportDB();
//    tLLReportDB.setRptNo(mLLRegisterBLSet.get(1).getRptNo());
//    if(!tLLReportDB.getInfo())
//      return false;
//    System.out.println(" 开始执行查询号码类型和号码！！！");
//    RgtObj = tLLReportDB.getRptObj();
//    RgtObjNo = tLLReportDB.getRptObjNo();
//    System.out.println("号码是"+tLLReportDB.getRptObj());
//    System.out.println("号码是"+tLLReportDB.getRptObjNo());
//    if(!(tLLReportDB.getRgtFlag()==null||tLLReportDB.getRgtFlag().equals("")))
//    {
//      if(tLLReportDB.getRgtFlag().equals("1"))
//      {
//        this.mErrors.copyAllErrors(tLLReportDB.mErrors);
//        CError tError = new CError();
//        tError.moduleName = "LLRegisterBL";
//        tError.functionName = "updateData";
//        tError.errorMessage = "该报案信息已经立案，不能对此进行重复立案操作！！！";
//        this.mErrors.addOneError(tError);
//        return false;
//      }
//    }
        return true;
    }

    /*****************************************************************************
     * Name     :checkDate()
     * Function :对立案中所有的日期进行判断
     * Author   :LiuYansong
     * Date     :2003-8-6
     */
    private boolean checkDate()
    {
        System.out.println("checkDate111");
        String AccidentDate = mLLRegisterBLSet.get(1).getAccidentDate();
        System.out.println("出险日期是" + mLLRegisterBLSet.get(1).getAccidentDate());
        for (int i = 1; i <= mLLCaseBLSet.size(); i++)
        {
            System.out.println("i的值是" + i);
            LLCaseSchema tLLCaseSchema = new LLCaseSchema();
            tLLCaseSchema.setSchema(mLLCaseBLSet.get(i));
            System.out.println("分案日期是" + mLLCaseBLSet.get(i).getInHospitalDate());
            System.out.println("出院日期是" + tLLCaseSchema.getOutHospitalDate());
            //入院日期要早于出院日期
            if ((!(tLLCaseSchema.getInHospitalDate() == null ||
                   tLLCaseSchema.getInHospitalDate().equals(""))) &&
                (!(tLLCaseSchema.getOutHospitalDate() == null ||
                   tLLCaseSchema.getOutHospitalDate().equals(""))))
            {
                System.out.println("判断入院日期和出院日期");
                if (!CaseFunPub.checkDate(tLLCaseSchema.getInHospitalDate(),
                                          tLLCaseSchema.getOutHospitalDate()))
                {
                    this.mErrors.copyAllErrors(tLLCaseSchema.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LLCaseSchema";
                    tError.functionName = "INSERT";
                    tError.errorMessage = "分案的入院日期比出院日期晚，请您核实！！！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            //入院日期与出险日期
            if (!(tLLCaseSchema.getInHospitalDate() == null ||
                  tLLCaseSchema.getInHospitalDate().equals("")))
            {
                System.out.println("判断出险日期和入院日期");
                if (!CaseFunPub.checkDate(AccidentDate,
                                          tLLCaseSchema.getInHospitalDate()))
                {
                    this.mErrors.copyAllErrors(tLLCaseSchema.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LLCaseSchema";
                    tError.functionName = "INSERT";
                    tError.errorMessage = "入院日期比出险日期早，请您核实！！！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            //出院日期与当前日期
            if (!(tLLCaseSchema.getOutHospitalDate() == null ||
                  tLLCaseSchema.getOutHospitalDate().equals("")))
            {
                System.out.println("判断出院日期和当前日期");
                if (!CaseFunPub.checkDate(tLLCaseSchema.getOutHospitalDate(),
                                          PubFun.getCurrentDate()))
                {
                    this.mErrors.copyAllErrors(tLLCaseSchema.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LLCaseSchema";
                    tError.functionName = "INSERT";
                    tError.errorMessage = "出院日期比当前日期晚，请您核实！！！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
        }
        System.out.println("结束了");
        return true;
    }

    public static void main(String[] args)
    {
        RegisterBL tRegisterBL   = new RegisterBL();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        VData tVData = new VData();
        //输出参数
        String strOperate = "CASECANCEL";
        GlobalInput tG = new GlobalInput();
        tG.ComCode = "8653";
        tG.Operator = "cm5302";
        tLLCaseSchema.setCaseNo("C5300060403000016");
        tLLCaseSchema.setRgtNo("C5300060403000016");
        tLLCaseSchema.setCancleReason("1");
        tLLCaseSchema.setCancleRemark("");
        tVData.addElement(tLLCaseSchema);
        tVData.addElement(tG);
        tRegisterBL.submitData(tVData,strOperate);
    }
}
