package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title:ShowPolInfoUI</p>
 * <p>Description:客户保单查询类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author ：LiuYansong
 * @version 1.0
 */
public class ShowPolInfoBL
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData ;
  private VData mResult = new VData();
  private String mOperate;
  private String InsuredNo="";//接收客户号码;
  private LCPolSet mLCPolSet = new LCPolSet();
  private LCPolSchema mLCPolSchema = new LCPolSchema();
  public ShowPolInfoBL() {}

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return: true or false
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;
    if (!getInputData(cInputData))
      return false;
    if(!queryData())
      return false;
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }
  private boolean getInputData(VData cInputData)
  {
    InsuredNo=(String)cInputData.get(0);
    System.out.println("BL中接受的客户号码是"+InsuredNo);
    return true;
  }
  /*****************************************************************************
   * 根据客户号码进行保单明显查询的函数；
   * 1.GrpPolNo用来判断险种性质（主险还是附加险）；
   * 2.ContNo用来记录险种的状态；
   * 3.Remark用来记录导致状态的原因；
   * 4.lcpol中的1用来表示是在lcpol中查询出的记录；
   * 5.lbpol中的2用来表示是在lbpol中查询的记录；
   * 6.tLCPolSet中装载的是原始的查询数据；
   * 7.yLCPolSet中装载的是经过处理后最终要进行显示的数据；
   * 8.BankAccNo：表示险种名称
   * 9.ManageCom：表示销售渠道
   * 10.t_sql1……查询出即是被保险人又是投保人的记录
   * 11.t_sql2……查询出被保险人的信息；
   * 12.t_sql3……查询出是投保人的信息；
   * 13.b_sql1……查询出即是被保险人又是投保人的记录（B）
   * 14.b_sql2……查询出被保险人的信息；
   * 15.b_sql3……查询出是投保人的信息；
   */
  private boolean queryData()
  {
    LCPolDB tLCPolDB = new LCPolDB();
    LCPolSet bLCPolSet = new LCPolSet();
    LCPolSet tLCPolSet = new LCPolSet();
    LCPolSet yLCPolSet = new LCPolSet();

    String t_sql1="select * from  lcpol where  AppFlag = '1' and AppntNo =  InsuredNo and  InsuredNo = '"+InsuredNo + "' order by MainPolNo,PolNo";
    String t_sql2="select * from  lcpol where  AppFlag = '1' and AppntNo != InsuredNo and  InsuredNo = '"+InsuredNo + "' order by MainPolNo,PolNo";
    String t_sql3="select * from  lcpol where  AppFlag = '1' and AppntNo != InsuredNo and  AppntNo   = '"+InsuredNo + "' order by MainPolNo,PolNo";

    String b_sql1="select * from  lbpol where  AppFlag = '1' and AppntNo =  InsuredNo and  InsuredNo = '"+InsuredNo + "' order by MainPolNo,PolNo";
    String b_sql2="select * from  lbpol where  AppFlag = '1' and AppntNo != InsuredNo and  InsuredNo = '"+InsuredNo + "' order by MainPolNo,PolNo";
    String b_sql3="select * from  lbpol where  AppFlag = '1' and AppntNo != InsuredNo and  AppntNo   = '"+InsuredNo + "' order by MainPolNo,PolNo";

    tLCPolSet.add(tLCPolDB.executeQuery(t_sql1));
    System.out.println("查询的语句是"+t_sql1);
    tLCPolSet.add(tLCPolDB.executeQuery(t_sql2));
    System.out.println("查询的语句是"+t_sql2);
    tLCPolSet.add(tLCPolDB.executeQuery(t_sql3));
    System.out.println("查询的语句是"+t_sql3);
    tLCPolSet.add(tLCPolDB.executeQuery(b_sql1));
    System.out.println("查询的语句是"+b_sql1);
    tLCPolSet.add(tLCPolDB.executeQuery(b_sql2));
    System.out.println("查询的语句是"+b_sql2);
    tLCPolSet.add(tLCPolDB.executeQuery(b_sql3));
    System.out.println("查询的语句是"+b_sql3);
    if(tLCPolSet.size()==0)
    {
      this.mErrors.copyAllErrors(tLCPolDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "ShowPolInfoBL";
      tError.functionName = "getDutyGetClmInfo";
      tError.errorMessage = "该客户号没有保单信息，请您确认录入的客户号码是否正确！！！" ;
      this.mErrors.addOneError(tError) ;
      return false;
    }
    //对tLCPolSet中的数据进行处理
    for(int count=1;count<=tLCPolSet.size();count++)
    {
      LCPolSchema tLCPolSchema = new LCPolSchema();
      tLCPolSchema.setSchema(tLCPolSet.get(count));
      LCPolSchema yLCPolSchema = new LCPolSchema();
      yLCPolSchema.setSchema(tLCPolSchema);
      //确定保单性质
      if(!(tLCPolSchema.getMainPolNo()==null||tLCPolSchema.getMainPolNo().equals("")))
      {
        String PolNo = tLCPolSchema.getPolNo().trim();
        System.out.println("该条记录的保单号码是"+PolNo);
        String MainPolNo = tLCPolSchema.getMainPolNo().trim();
        if(PolNo.equals(MainPolNo))
        {
          yLCPolSchema.setGrpPolNo("主险");
        }
        else
        {
          yLCPolSchema.setGrpPolNo("附加险");
        }
      }
      //确定类别（销售渠道）
      if(!(tLCPolSchema.getSaleChnl()==null||tLCPolSchema.getSaleChnl().equals("")))
      {
        if(tLCPolSchema.getSaleChnl().equals("01"))
        {
          yLCPolSchema.setManageCom("团险");
        }
        else
        {
          yLCPolSchema.setManageCom("个险");
        }
      }
      //确定险种名称
      if(!(tLCPolSchema.getRiskCode()==null||tLCPolSchema.getRiskCode().equals("")))
      {
        LMRiskDB tLMRiskDB = new LMRiskDB();
        LMRiskSet tLMRiskSet = new LMRiskSet();
        tLMRiskDB.setRiskCode(tLCPolSchema.getRiskCode());
        tLMRiskSet.set(tLMRiskDB.query());
      /*Lis5.3 upgrade set
        if(tLMRiskSet.size()>0)
        {
          yLCPolSchema.setBankAccNo(tLMRiskSet.get(1).getRiskName());
        }
        else
        {
          yLCPolSchema.setBankAccNo("");
        }
        */
      }
      //确定险种状态的方法
      String tState[] = new String[4];
      tState = CaseFunPub.getPolState(tLCPolSchema.getPolNo());

      yLCPolSchema.setContNo(tState[0]);
      yLCPolSchema.setRemark(tState[1]);
      //确定出险人是被保人还是投保人
      if(!(tLCPolSchema.getInsuredNo()==null||tLCPolSchema.getInsuredNo().equals(""))&&
         !(tLCPolSchema.getAppntNo()==null||tLCPolSchema.getAppntNo().equals("")))
      {
        System.out.println("被保人的客户号码是"+tLCPolSchema.getInsuredNo());
        System.out.println("投保人的客户号码是"+tLCPolSchema.getAppntNo());
        if(tLCPolSchema.getInsuredNo().equals(tLCPolSchema.getAppntNo()))

        {
          yLCPolSchema.setAgentCom("即是被保人又是投保人");

        }
        if((!tLCPolSchema.getInsuredNo().equals(tLCPolSchema.getAppntNo()))
           &&(tLCPolSchema.getInsuredNo().equals(InsuredNo)))
        {
          yLCPolSchema.setAgentCom("被保人");
        }
        if((!tLCPolSchema.getInsuredNo().equals(tLCPolSchema.getAppntNo()))
           &&tLCPolSchema.getAppntNo().equals(InsuredNo))
        {
          yLCPolSchema.setAgentCom("投保人");
        }
        System.out.println("性质是"+yLCPolSchema.getAgentCom());
      }
      yLCPolSet.add(yLCPolSchema);
    }
     mResult.add(yLCPolSet);
    return true;
  }
  public static void main(String[] args)
  {
    ShowPolInfoBL tShowPolInfoBL = new ShowPolInfoBL();
    String tInsuredNo = "0000001850";
    VData tVData = new VData();
    tVData.addElement(tInsuredNo);
    tShowPolInfoBL.submitData(tVData,"Query");
  }
}
