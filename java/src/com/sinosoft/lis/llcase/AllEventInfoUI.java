package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:案件－立案－分案疾病伤残明细信息的数据接收处理类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @version 1.0
 */
public class AllEventInfoUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public AllEventInfoUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    AllEventInfoBL tAllEventInfoBL = new AllEventInfoBL();
    System.out.println("---UI BEGIN---");
    if (tAllEventInfoBL.submitData(cInputData,mOperate) == false)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tAllEventInfoBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "CaseInfoUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据保存失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
      mResult = tAllEventInfoBL.getResult();
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public static void main(String[] args)
  {
    AllCaseInfoUI tCaseInfoUI=new AllCaseInfoUI();

    VData tVData=new VData();

    LLCaseInfoSchema tLLCaseInfoSchema = new LLCaseInfoSchema();

    //tLLCaseInfoSchema.setAffixSerialNo("1");
    tLLCaseInfoSchema.setCaseNo("1");
    tLLCaseInfoSchema.setMngCom("ddd");
    tLLCaseInfoSchema.setOperator("ddd");
    tLLCaseInfoSchema.setMakeDate("2002-10-10");
    tLLCaseInfoSchema.setMakeTime("12:12:12");
    tLLCaseInfoSchema.setModifyDate("2002-10-10");
    tLLCaseInfoSchema.setModifyTime("12:12:12");


    LLCaseInfoSet tLLCaseInfoSet = new LLCaseInfoSet();
    tLLCaseInfoSet.add(tLLCaseInfoSchema);

    tVData.addElement(tLLCaseInfoSet);

    tCaseInfoUI.submitData(tVData,"INSERT||MAIN");
  }
}
