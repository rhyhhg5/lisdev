package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: ClientRegisterBackBL </p>
 * <p>Description: ClientRegisterBackBL类文件 </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: testcompany </p>
 * @author: Xx
 * @CreateDate：2005-04-05
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.llcase.LLCaseCommon;
import java.util.Date;

public class ClientRegisterBackBL {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();
  private VData mResult = new VData();

  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  private MMap map = new MMap();

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();

  /** 数据操作字符串 */
  private String mOperate;
  private FDate fDate = new FDate();

  /** 业务处理相关变量 */
  private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
  private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
  private LLCaseExtSchema mLLCaseExtSchema = new LLCaseExtSchema();
  private LLAppealSchema mLLAppealSchema= new LLAppealSchema();
  private LLSubReportSet tLLSubReportSet = new LLSubReportSet();
  private LLAppClaimReasonSet mLLAppClaimReasonSet = new LLAppClaimReasonSet();
  private LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
  
  /**社保校验标记*/
  private boolean mSocialFlag = false;

  public ClientRegisterBackBL()
  {
  }

  public static void main(String[] args)
  {
      String strOperate = "APPEALINSERT||MAIN";
      LLCaseSchema tLLCaseSchema = new LLCaseSchema();
      LLAppealSchema tLLAppealSchema = new LLAppealSchema();
      LLRegisterSchema tLLRegisterSchema=new LLRegisterSchema();
      LLSubReportSet tLLSubReportSet = new LLSubReportSet();
      VData tVData = new VData();
      ClientRegisterBackBL tClientRegisterBackBL   = new ClientRegisterBackBL();
      GlobalInput tG = new GlobalInput();
      tLLRegisterSchema.setRgtState("01");//案件状态
      tLLRegisterSchema.setRgtObj("2"); //个人客户
      tLLRegisterSchema.setRgtState("01");//案件状态
      tLLRegisterSchema.setRgtObj("2");						//号码类型 0总单 1分单 2个单 3客户
      tLLRegisterSchema.setRgtObjNo("000013282");
      tLLRegisterSchema.setRgtClass("0");
      tLLRegisterSchema.setCustomerNo("000013282");
      tLLRegisterSchema.setRgtType("1");         		//受理方式
      tLLRegisterSchema.setRgtantName("李娟");     	//申请人姓名
      tLLRegisterSchema.setRelation("05");        		//与被保险人关系
      tLLRegisterSchema.setRgtantAddress("新化路23号");	//申请人地址
      tLLRegisterSchema.setRgtantPhone("53232514");     	//申请人电话
      tLLRegisterSchema.setPostCode("100021");        		//邮编
      tLLRegisterSchema.setAppAmnt(15456);       		//预估申请金额
      tLLRegisterSchema.setGetMode("1");
      tLLRegisterSchema.setBankCode("");
      tLLRegisterSchema.setBankAccNo("");
      tLLRegisterSchema.setAccName("");
      tLLRegisterSchema.setReturnMode("1");    		//回执发送方式
      tLLRegisterSchema.setIDType("0");        			//申请人证件类型
      tLLRegisterSchema.setIDNo("110106194605214229");          //申请人证件号码
      tLLRegisterSchema.setRgtObj("1"); //个人客户
      tLLCaseSchema.setRgtType("4");
      tLLCaseSchema.setCustomerName("李娟");
      tLLCaseSchema.setCustomerNo("000013282");
      tLLCaseSchema.setPostalAddress("新化路23号");
      tLLCaseSchema.setPhone("53232514");
      tLLCaseSchema.setDeathDate("");
      tLLCaseSchema.setCustBirthday("1981-10-14");
      tLLCaseSchema.setCustomerSex("0");
      tLLCaseSchema.setIDType("0");
      tLLCaseSchema.setIDNo("110106194605214229");
      tLLCaseSchema.setCustState("01");
      tLLCaseSchema.setCaseNo("C5300060207000005");
      tLLAppealSchema.setCaseNo("C5300060207000005");
      tLLAppealSchema.setAppeanRCode("1");
      tLLAppealSchema.setAppealType("1");
      tLLAppealSchema.setAppealReason("业务员原因");
      tLLAppealSchema.setAppealRDesc("ff");

      LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
      tLLSubReportSchema.setSubRptNo("A5300060207000005");
      tLLSubReportSchema.setCustomerNo("000013282");
      tLLSubReportSchema.setCustomerName("王小梅");
      tLLSubReportSchema.setAccDate("2005-05-01");
      tLLSubReportSchema.setAccDesc("zhuyuan");
      tLLSubReportSchema.setAccPlace("yiyuan");
      tLLSubReportSchema.setInHospitalDate("2005-05-01");
      tLLSubReportSchema.setOutHospitalDate("2005-05-12");
      tLLSubReportSchema.setAccidentType("1");

      tLLSubReportSet.add(tLLSubReportSchema);
      tG.ManageCom = "86";
      tG.ComCode = "86";
      tG.Operator = "xuxin";

            tVData.add(tLLCaseSchema);
            tVData.add(tLLAppealSchema);
            tVData.add(tLLRegisterSchema);
            tVData.add(tLLSubReportSet);
            tVData.add(tG);
        tClientRegisterBackBL.submitData(tVData,strOperate);
  }

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
    if (!checkInputData())
        return false;
    //进行业务处理
    if (!dealData()) {
        // @@错误处理
        CError.buildErr(this, "数据处理失败ClientRegisterBL-->dealData!");
        return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
        return false;

    System.out.println("Start ClientRegisterBL Submit...");
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
        // @@错误处理
        CError.buildErr(this, "数据提交失败!");
        return false;
    }
    System.out.println("End ClientRegisterBL Submit...");

    mInputData = null;
    return true;
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData()
  {
      ///申诉处理
      if (this.mOperate.equals("APPEALINSERT||MAIN"))
      {
          System.out.println("<---Start Appeal Insert Operate --->");

          LLCaseDB tLLCaseDB = new LLCaseDB();
          tLLCaseDB.setCaseNo(mLLCaseSchema.getCaseNo());
          System.out.println(mLLCaseSchema.getCaseNo());
          String currentDate = PubFun.getCurrentDate();
          String currentTime = PubFun.getCurrentTime();
          if (!tLLCaseDB.getInfo()) {
              // @@错误处理
              CError.buildErr(this, "理赔案件查询失败!");
              return false;
          }

          if (!"1".equals(tLLCaseDB.getRgtType())){
              CError.buildErr(this, "非申请类案件，不能进行纠错、申诉处理！");
              return false;
          }

          String tAppealSQL = "SELECT 1 FROM llappeal a,llcase b WHERE a.appealno=b.caseno "
                            + "AND b.rgtstate not in ('11','12','14') AND a.caseno='"
                            + mLLCaseSchema.getCaseNo()+"'";
          ExeSQL tExeSQL = new ExeSQL();
          SSRS tAppealSSRS = tExeSQL.execSQL(tAppealSQL);
          if (tAppealSSRS.getMaxRow()>0){
              CError.buildErr(this, "该案件有未处理完的纠错、申诉案件！");
              return false;
          }

          /*查询理赔案件信息*/
          System.out.println("<---Start to Query Case Info--->");

//       mLLCaseSchema = tLLCaseDB.getSchema();
          mLLCaseSchema.setHandler(tLLCaseDB.getHandler());
          mLLCaseSchema.setRgtNo(tLLCaseDB.getRgtNo());
          mLLCaseSchema.setCaseNo(tLLCaseDB.getCaseNo());
          mLLCaseSchema.setRgtState(tLLCaseDB.getRgtState());
          mLLCaseSchema.setRgtDate(tLLCaseDB.getRgtDate());
          mLLCaseSchema.setAffixGetDate(tLLCaseDB.getAffixGetDate());
          mLLCaseSchema.setMngCom(tLLCaseDB.getMngCom());
          int CustAge = calAge();
          mLLCaseSchema.setCustomerAge(CustAge);
          tLLCaseDB = null;

          if ((!mLLCaseSchema.getRgtState().equals("11")) &&
              (!mLLCaseSchema.getRgtState().equals("12"))) {
              String RGTSTATE = mLLCaseSchema.getRgtState();
              System.out.println("案件状态为：" + RGTSTATE);

              CError tError = new CError();
              tError.moduleName = "";
              tError.functionName = "dealData";
              tError.errorMessage = "理赔案件状态不符!";
              this.mErrors.addOneError(tError);
              return false;
          }

//检查申诉原因

          System.out.println("<---Start to Check AppeanRCode--->");
          System.out.println("AppeanRCode"+ mLLAppealSchema.getAppeanRCode());
       if ("".equals( StrTool.cTrim(mLLAppealSchema.getAppeanRCode())))
              {
                  CError tError = new CError();
                  tError.moduleName = "";
                  tError.functionName = "dealData";
                  tError.errorMessage = "请输入申诉原因!";
                  this.mErrors.addOneError(tError);
                  return false;
              }

       /*生成申诉号*/
       String MngCom = mLLCaseSchema.getMngCom();
       System.out.println("<---Start to Create AppealNo--->");
       String tLimit = PubFun.getNoLimit(MngCom);
       String AppealNo = "";
       if("0".equals(mLLAppealSchema.getAppealType()))
           AppealNo = PubFun1.CreateMaxNo("AppealNo", tLimit);
       else
           AppealNo = PubFun1.CreateMaxNo("LLERRORNO", tLimit);
       System.out.println("申诉号" + AppealNo);
       //管理机构

       String Operator = mGlobalInput.Operator;
       //操作员
       System.out.println("<---Start to Insert AppealSchema--->");

       mLLAppealSchema.setAppealNo(AppealNo);
       mLLAppealSchema.setCaseNo(mLLCaseSchema.getCaseNo());
       mLLAppealSchema.setAppealState("0");
       mLLAppealSchema.setMakeDate(currentDate);
       mLLAppealSchema.setMakeTime(currentTime);
       mLLAppealSchema.setModifyDate(currentDate);
       mLLAppealSchema.setModifyTime(currentTime);
       //加入到队列
       map.put(mLLAppealSchema, "INSERT");
     //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
       mLLCaseExtSchema.setCaseNo(AppealNo);
       mLLCaseExtSchema.setOperator(mGlobalInput.Operator);
       mLLCaseExtSchema.setMngCom(mGlobalInput.ManageCom);
       mLLCaseExtSchema.setMakeDate(PubFun.getCurrentDate());
       mLLCaseExtSchema.setMakeTime(PubFun.getCurrentTime());
       mLLCaseExtSchema.setModifyTime(PubFun.getCurrentTime());
       mLLCaseExtSchema.setModifyDate(PubFun.getCurrentDate());
       map.put(mLLCaseExtSchema, "INSERT");
     //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
     LLCaseSchema newLLCaseSchema = new LLCaseSchema();
     //确定申诉处理人
     //社保案件申诉纠错的处理人为原案件处理人 add by Houyd
     String AppealHandler="";
     String tSSFlag = LLCaseCommon.checkSocialSecurity(mLLCaseSchema.getCaseNo(), null);
     if("1".equals(tSSFlag)){
    	 AppealHandler=LLCaseCommon.getSSAppealHandler(mLLCaseSchema.getHandler());
     }else{
    	 AppealHandler=LLCaseCommon.getAppealHandler(mLLCaseSchema.getHandler());
         
     }
     	if ("".equals(AppealHandler) || AppealHandler == null)
     	{
     		//所有理赔员当前状态均不合法
     		CError tError = new CError();
     		tError.moduleName = "";
     		tError.functionName = "dealData";
     		tError.errorMessage = "当前无合法状态的理赔员";
     		this.mErrors.addOneError(tError);
     		return false;
     	}
     
       newLLCaseSchema.setSchema(mLLCaseSchema);
       newLLCaseSchema.setHandler(AppealHandler);
       newLLCaseSchema.setCaseNo(AppealNo);
       newLLCaseSchema.setRgtNo(AppealNo);
       newLLCaseSchema.setRgtDate(currentDate);
       newLLCaseSchema.setRgtState("01");

       //更改结案日期
       newLLCaseSchema.setEndCaseDate("");

       newLLCaseSchema.setMakeDate(currentDate);
       newLLCaseSchema.setMakeTime(currentTime);
       newLLCaseSchema.setModifyDate(currentDate);
       newLLCaseSchema.setModifyTime(currentTime);
       newLLCaseSchema.setRgtDate(currentDate);
       newLLCaseSchema.setHandleDate("");
       newLLCaseSchema.setClaimCalDate("");
       newLLCaseSchema.setEndCaseFlag("0");
       newLLCaseSchema.setMngCom(MngCom);
       newLLCaseSchema.setOperator(Operator);
       newLLCaseSchema.setRigister(Operator);
       //加入到队列
       map.put(newLLCaseSchema, "INSERT");
       mLLCaseOpTimeSchema.setCaseNo(AppealNo);
       mLLCaseOpTimeSchema.setOperator(mGlobalInput.Operator);
       mLLCaseOpTimeSchema.setManageCom(mGlobalInput.ManageCom);
       LLCaseCommon tLLCaseCommon = new LLCaseCommon();
       LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(mLLCaseOpTimeSchema);
       tLLCaseOpTimeSchema.setEndDate(PubFun.getCurrentDate());
       tLLCaseOpTimeSchema.setEndTime(PubFun.getCurrentTime());
       map.put(tLLCaseOpTimeSchema,"INSERT");



       LLRegisterSchema newLLRegisterSchema = new LLRegisterSchema();
       newLLRegisterSchema.setSchema(mLLRegisterSchema);
       newLLRegisterSchema.setRgtNo(AppealNo);
       newLLRegisterSchema.setMakeDate(currentDate);
       newLLRegisterSchema.setMakeTime(currentTime);
       newLLRegisterSchema.setModifyDate(currentDate);
       newLLRegisterSchema.setModifyTime(currentTime);
       newLLRegisterSchema.setRgtDate(currentDate);
       newLLRegisterSchema.setEndCaseFlag("0");
       newLLRegisterSchema.setOperator(Operator);
       //加入到队列
       map.put(newLLRegisterSchema, "INSERT");
       if (mLLAppClaimReasonSet != null && mLLAppClaimReasonSet.size() > 0)
       {
         for (int i = 1; i <= mLLAppClaimReasonSet.size(); i++)
         {
           mLLAppClaimReasonSet.get(i).setCaseNo(AppealNo);
           mLLAppClaimReasonSet.get(i).setRgtNo(AppealNo);
           mLLAppClaimReasonSet.get(i).setMakeDate(PubFun.getCurrentDate());
           mLLAppClaimReasonSet.get(i).setMakeTime(PubFun.getCurrentTime());
           mLLAppClaimReasonSet.get(i).setModifyDate(PubFun.getCurrentDate());
           mLLAppClaimReasonSet.get(i).setModifyTime(PubFun.getCurrentTime());
           mLLAppClaimReasonSet.get(i).setOperator(mGlobalInput.Operator);
           mLLAppClaimReasonSet.get(i).setMngCom(MngCom);
         }
         map.put(mLLAppClaimReasonSet,"INSERT");
       }

//出险事件
       //立案分案,即事件
   LLCaseRelaSet tLLCaseRelaSet = new LLCaseRelaSet();
   LLSubReportSet saveLLSubReportSet = new LLSubReportSet();
   LLSubReportSet modifyLLSubReportSet = new LLSubReportSet();
   if ( tLLSubReportSet!=null )
   {
       System.out.println("yyyyyyyyyyy:");
       for (int i = 1; i <= tLLSubReportSet.size(); i++) {
           LLSubReportSchema tLLSubReportSchema = tLLSubReportSet.get(i);
           //如果事件没有，则创建事件关联
           if ("".equals(StrTool.cTrim(tLLSubReportSchema.getSubRptNo()))) {

               String SubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);
               tLLSubReportSchema.setSubRptNo(SubRptNo);
               tLLSubReportSchema.setMakeDate(PubFun.getCurrentDate());
               tLLSubReportSchema.setMakeTime(PubFun.getCurrentTime());
               tLLSubReportSchema.setOperator(this.mGlobalInput.Operator);
               tLLSubReportSchema.setMngCom(MngCom);
               tLLSubReportSchema.setModifyDate(tLLSubReportSchema.getMakeDate());
               tLLSubReportSchema.setModifyTime(tLLSubReportSchema.getMakeTime());
               saveLLSubReportSet.add(tLLSubReportSchema);
           } else {
               LLSubReportDB tsLLSubReportDB = new LLSubReportDB();
               tsLLSubReportDB.setSubRptNo(tLLSubReportSchema.getSubRptNo());
               if (!tsLLSubReportDB.getInfo()){
                   continue;
               }
               else{
                   tLLSubReportSchema.setMakeDate(tsLLSubReportDB.getMakeDate());
                   tLLSubReportSchema.setMakeTime(tsLLSubReportDB.getMakeTime());
                   tLLSubReportSchema.setOperator(tsLLSubReportDB.getOperator());
                   tLLSubReportSchema.setMngCom(tsLLSubReportDB.getMngCom());
                   tLLSubReportSchema.setModifyDate(PubFun.getCurrentDate());
                   tLLSubReportSchema.setModifyTime(PubFun.getCurrentTime());
                   modifyLLSubReportSet.add(tLLSubReportSchema);
               }
           }
           LLCaseRelaSchema tLLCaseRelaSchema = new LLCaseRelaSchema();
           String caserela = PubFun1.CreateMaxNo("CASERELANO", tLimit);
           System.out.println("caserela:" + caserela);
           tLLCaseRelaSchema.setCaseRelaNo(caserela);
           tLLCaseRelaSchema.setCaseNo(this.mLLCaseSchema.getCaseNo());
           tLLCaseRelaSchema.setSubRptNo(tLLSubReportSchema.getSubRptNo());
           tLLCaseRelaSet.add(tLLCaseRelaSchema);
       }
       //删除关联
//       map.put("delete from llcaserela where caseno='" +
//               this.mLLCaseSchema.getCaseNo() + "'", "DELETE");
//       map.put(tLLCaseRelaSet, "INSERT");
//事件增加
       if (saveLLSubReportSet.size() > 0) {
           map.put(saveLLSubReportSet, "INSERT");
       }
       if (modifyLLSubReportSet.size() > 0) {
           map.put(modifyLLSubReportSet, "DELETE&INSERT");
       }
   }


       //申请原因
       LLAppClaimReasonSet tLLAppClaimReasonSet = null;
       LLAppClaimReasonDB tLLAppClaimReasonDB = new LLAppClaimReasonDB();
       tLLAppClaimReasonDB.setCaseNo(mLLCaseSchema.getCaseNo());
       tLLAppClaimReasonSet = tLLAppClaimReasonDB.query();
       if (tLLAppClaimReasonSet != null && tLLAppClaimReasonSet.size() > 0)
       {
           for (int i = 1; i <= tLLAppClaimReasonSet.size(); i++)
           {
               LLAppClaimReasonSchema tLLAppClaimReasonSchema =
                       tLLAppClaimReasonSet.get(i);

               tLLAppClaimReasonSchema.setCaseNo(AppealNo);
               tLLAppClaimReasonSchema.setMakeDate(currentDate);
               tLLAppClaimReasonSchema.setMakeTime(currentTime);
               tLLAppClaimReasonSchema.setModifyDate(currentDate);
               tLLAppClaimReasonSchema.setModifyTime(currentTime);
               tLLAppClaimReasonSchema.setMngCom(MngCom);
               tLLAppClaimReasonSchema.setOperator(Operator);

           }
          map.put(tLLAppClaimReasonSet, "INSERT");
       }

//关联赔付保单
       LLCasePolicySet tLLCasePolicySet = null;
       LLCasePolicyDB tLLCasePolicyDB = new LLCasePolicyDB();
       tLLCasePolicyDB.setCaseNo(mLLCaseSchema.getCaseNo());
       tLLCasePolicySet = tLLCasePolicyDB.query();
       if (tLLCasePolicySet != null && tLLCasePolicySet.size() > 0)
       {
           for (int i = 1; i <= tLLCasePolicySet.size(); i++)
           {
               LLCasePolicySchema tLLCasePolicySchema =
                       tLLCasePolicySet.get(i);

               tLLCasePolicySchema.setCaseNo(AppealNo);
               tLLCasePolicySchema.setMakeDate(currentDate);
               tLLCasePolicySchema.setMakeTime(currentTime);
               tLLCasePolicySchema.setModifyDate(currentDate);
               tLLCasePolicySchema.setModifyTime(currentTime);
               tLLCasePolicySchema.setMngCom(MngCom);
               tLLCasePolicySchema.setOperator(Operator);
           }
         map.put(tLLCasePolicySet, "INSERT");
       }

//疾病信息
       LLCaseCureSet tLLCaseCureSet = null;
       LLCaseCureDB tLLCaseCureDB = new LLCaseCureDB();
       tLLCaseCureDB.setCaseNo(mLLCaseSchema.getCaseNo());
       tLLCaseCureSet = tLLCaseCureDB.query();
       if (tLLCaseCureSet != null && tLLCaseCureSet.size() > 0)
         {
             for (int i = 1; i <= tLLCaseCureSet.size(); i++)
             {

                 //生成序列号
                    System.out.println("<---Start to Create SerialNo--->");
                // String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
                    System.out.println("管理机构代码是 : " + tLimit);
                    String SerialNo1= PubFun1.CreateMaxNo("SerialNo", tLimit);
                    System.out.println("序列号" + SerialNo1);

                 LLCaseCureSchema tLLCaseCureSchema =
                         tLLCaseCureSet.get(i);

                 tLLCaseCureSchema.setSerialNo(SerialNo1);
                 tLLCaseCureSchema.setCaseNo(AppealNo);
                 tLLCaseCureSchema.setMakeDate(currentDate);
                 tLLCaseCureSchema.setMakeTime(currentTime);
                 tLLCaseCureSchema.setModifyDate(currentDate);
                 tLLCaseCureSchema.setModifyTime(currentTime);
                 tLLCaseCureSchema.setMngCom(MngCom);
                 tLLCaseCureSchema.setOperator(Operator);
             }
           map.put(tLLCaseCureSet, "INSERT");
         }

//残疾信息
      LLCaseInfoSet tLLCaseInfoSet = null;
      LLCaseInfoDB tLLCaseInfoDB = new LLCaseInfoDB();
      tLLCaseInfoDB.setCaseNo(mLLCaseSchema.getCaseNo());
      tLLCaseInfoSet = tLLCaseInfoDB.query();
      if (tLLCaseInfoSet != null && tLLCaseInfoSet.size() > 0)
         {
             for (int i = 1; i <= tLLCaseInfoSet.size(); i++)
             {
                 LLCaseInfoSchema tLLCaseInfoSchema =
                         tLLCaseInfoSet.get(i);

                 tLLCaseInfoSchema.setCaseNo(AppealNo);
                 tLLCaseInfoSchema.setMakeDate(currentDate);
                 tLLCaseInfoSchema.setMakeTime(currentTime);
                 tLLCaseInfoSchema.setModifyDate(currentDate);
                 tLLCaseInfoSchema.setModifyTime(currentTime);
                 tLLCaseInfoSchema.setMngCom(MngCom);
                 tLLCaseInfoSchema.setOperator(Operator);
             }
             map.put(tLLCaseInfoSet, "INSERT");
         }

//手术
         LLOperationSet tLLOperationSet = null;
         LLOperationDB tLLOperationDB = new LLOperationDB();
         tLLOperationDB.setCaseNo(mLLCaseSchema.getCaseNo());
         tLLOperationSet = tLLOperationDB.query();
         if (tLLOperationSet != null && tLLOperationSet.size() > 0)
            {
                for (int i = 1; i <= tLLOperationSet.size(); i++)
                {
                    //生成序列号
                    System.out.println("<---Start to Create SerialNo--->");
                       // String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
                        System.out.println("管理机构代码是 : " + tLimit);
                        String SerialNo = PubFun1.CreateMaxNo("SerialNo", tLimit);
                        System.out.println("序列号" + SerialNo);


                    LLOperationSchema tLLOperationSchema =
                            tLLOperationSet.get(i);

                    tLLOperationSchema.setSerialNo(SerialNo);
                    tLLOperationSchema.setCaseNo(AppealNo);
                    tLLOperationSchema.setMakeDate(currentDate);
                    tLLOperationSchema.setMakeTime(currentTime);
                    tLLOperationSchema.setModifyDate(currentDate);
                    tLLOperationSchema.setModifyTime(currentTime);
                    tLLOperationSchema.setMngCom(MngCom);
                    tLLOperationSchema.setOperator(Operator);
                }
                map.put(tLLOperationSet, "INSERT");
            }

//意外
          LLAccidentSet tLLAccidentSet = null;
          LLAccidentDB tLLAccidentDB = new LLAccidentDB();
          tLLAccidentDB.setCaseNo(mLLCaseSchema.getCaseNo());
          tLLAccidentSet = tLLAccidentDB.query();
          if (tLLAccidentSet != null && tLLAccidentSet.size() > 0)
           {

               //生成序列号
                   System.out.println("<---Start to Create AccidentNo--->");
               // String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
                   System.out.println("管理机构代码是 : " + tLimit);
                   String AccidentNo;

              for (int i = 1; i <= tLLAccidentSet.size(); i++)
              {
                  AccidentNo= PubFun1.CreateMaxNo("AccidentNo", tLimit);
                   System.out.println("序列号" + AccidentNo);

                LLAccidentSchema tLLAccidentSchema =
                   tLLAccidentSet.get(i);

                tLLAccidentSchema.setAccidentNo( AccidentNo);
                tLLAccidentSchema.setCaseNo(AppealNo);

              }
            map.put(tLLAccidentSet, "INSERT");
          }
        //失能
        LLDisabilitySet tLLDisabilitySet = null;
        LLDisabilityDB tLLDisabilityDB = new LLDisabilityDB();
        tLLDisabilityDB.setCaseNo(mLLCaseSchema.getCaseNo());
        tLLDisabilitySet = tLLDisabilityDB.query();
        if (tLLDisabilitySet != null && tLLDisabilitySet.size() > 0) {
            for (int i = 1; i <= tLLDisabilitySet.size(); i++) {
                LLDisabilitySchema tLLDisabilitySchema = tLLDisabilitySet.get(i);
                tLLDisabilitySchema.setCaseNo(AppealNo);
            }
            map.put(tLLDisabilitySet,"INSERT");
        }
//其他信息
        LLOtherFactorSet tLLOtherFactorSet = null;
        LLOtherFactorDB tLLOtherFactorDB = new LLOtherFactorDB();
        tLLOtherFactorDB.setCaseNo(mLLCaseSchema.getCaseNo());
        tLLOtherFactorSet = tLLOtherFactorDB.query();
        if (tLLOtherFactorSet != null && tLLOtherFactorSet.size() > 0)
            {
              for (int i = 1; i <= tLLOtherFactorSet.size(); i++)
               {
                  LLOtherFactorSchema tLLOtherFactorSchema =
                          tLLOtherFactorSet.get(i);

                   tLLOtherFactorSchema.setCaseNo(AppealNo);

                }
                   map.put(tLLOtherFactorSet, "INSERT");
              }

          //账单
              LLFeeMainSet tLLFeeMainSet = null;
              LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
              tLLFeeMainDB.setCaseNo(mLLCaseSchema.getCaseNo());
              tLLFeeMainSet = tLLFeeMainDB.query();

              if (tLLFeeMainSet != null && tLLFeeMainSet.size() > 0) {
                  for (int i = 1; i <= tLLFeeMainSet.size(); i++) {
                      LLFeeMainSchema tLLFeeMainSchema =
                              tLLFeeMainSet.get(i);

                      /*生成MainFeeNo*/
                      System.out.println("<---Start to Create MainFeeNo--->");
                      // String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
                      System.out.println("管理机构代码是 : " + tLimit);
                      String MainFeeNo = PubFun1.CreateMaxNo("MainFeeNo", tLimit);
                      System.out.println("帐单号" + MainFeeNo);

                      //查找和LLFainMinSchema相关的LLCaseRecipt
                      // LLCaseReceiptSet tLLCaseReceiptSet = null;
                      LLCaseReceiptDB tLLCaseReceiptDB = new LLCaseReceiptDB();
                      tLLCaseReceiptDB.setMainFeeNo(tLLFeeMainSchema.getMainFeeNo());
                      LLCaseReceiptSet tLLCaseReceiptSet = tLLCaseReceiptDB.query();
                      if (tLLCaseReceiptSet != null && tLLCaseReceiptSet.size() > 0) {
                          for (int j = 1; j <= tLLCaseReceiptSet.size(); j++) {

                              /*生成FeeDatailNo*/
                              System.out.println("<---Start to Create FeeDetailNo--->");

                              System.out.println("管理机构代码是 : " + tLimit);
                              String FeeDetailNo = PubFun1.CreateMaxNo("FeeDetailNo",
                                      tLimit);
                              System.out.println("帐单号" + FeeDetailNo);

                              LLCaseReceiptSchema tLLCaseReceiptSchema =
                                      tLLCaseReceiptSet.get(j);

                              tLLCaseReceiptSchema.setFeeDetailNo(FeeDetailNo);
                              tLLCaseReceiptSchema.setMainFeeNo(MainFeeNo);
                              tLLCaseReceiptSchema.setRgtNo(AppealNo);
                              tLLCaseReceiptSchema.setCaseNo(AppealNo);

                              System.out.println("====================");
                              System.out.println("AppealNo" + AppealNo);
                              System.out.println("CaseNo" +
                                                 tLLCaseReceiptSchema.getCaseNo());
                              System.out.println("RgtNo" + tLLCaseReceiptSchema.getRgtNo());
                              System.out.println("====================");

                              tLLCaseReceiptSchema.setMakeDate(currentDate);
                              tLLCaseReceiptSchema.setMakeTime(currentTime);
                              tLLCaseReceiptSchema.setModifyDate(currentDate);
                              tLLCaseReceiptSchema.setModifyTime(currentTime);
                              tLLCaseReceiptSchema.setMngCom(MngCom);
                              tLLCaseReceiptSchema.setOperator(Operator);
                          }
                          map.put(tLLCaseReceiptSet, "INSERT");
                      }
                      //查找和LLFainMinSchema相关的LLCaseDrug
                      LLCaseDrugDB tLLCaseDrugDB = new LLCaseDrugDB();
                      tLLCaseDrugDB.setMainFeeNo(tLLFeeMainSchema.getMainFeeNo());
                      LLCaseDrugSet tLLCaseDrugSet = tLLCaseDrugDB.query();
                      System.out.println(tLLCaseDrugSet.size());
                      if (tLLCaseDrugSet != null && tLLCaseDrugSet.size() > 0) {
                          for (int j = 1; j <= tLLCaseDrugSet.size(); j++) {
                              /*生成FeeDatailNo*/
                              System.out.println("<---Start to Create CaseDrugNo--->");

                              System.out.println("管理机构代码是 : " + tLimit);
                              String drugDetailNo = PubFun1.CreateMaxNo("DRUGDETAILNO",
                                      tLimit);

                              LLCaseDrugSchema tLLCaseDrugSchema = tLLCaseDrugSet.get(j);

                              tLLCaseDrugSchema.setDrugDetailNo(drugDetailNo);
                              tLLCaseDrugSchema.setMainFeeNo(MainFeeNo);
                              tLLCaseDrugSchema.setRgtNo(AppealNo);
                              tLLCaseDrugSchema.setCaseNo(AppealNo);

                              tLLCaseDrugSchema.setMakeDate(currentDate);
                              tLLCaseDrugSchema.setMakeTime(currentTime);
                              tLLCaseDrugSchema.setModifyDate(currentDate);
                              tLLCaseDrugSchema.setModifyTime(currentTime);
                              tLLCaseDrugSchema.setMngCom(MngCom);
                              tLLCaseDrugSchema.setOperator(Operator);
                          }
                          map.put(tLLCaseDrugSet, "INSERT");
                      }
                      //查找和LLFainMinSchema相关的LLSecurityReceipt
                      LLSecurityReceiptDB tLLSecurityReceiptDB = new
                              LLSecurityReceiptDB();
                      tLLSecurityReceiptDB.setFeeDetailNo(tLLFeeMainSchema.
                                                          getMainFeeNo());
                      if (tLLSecurityReceiptDB.getInfo()) {
                          LLSecurityReceiptSchema tLLSecurityReceiptSchema =
                                  tLLSecurityReceiptDB.getSchema();

                          tLLSecurityReceiptSchema.setFeeDetailNo(MainFeeNo);
                          tLLSecurityReceiptSchema.setMainFeeNo(MainFeeNo);
                          tLLSecurityReceiptSchema.setRgtNo(AppealNo);
                          tLLSecurityReceiptSchema.setCaseNo(AppealNo);

                          tLLSecurityReceiptSchema.setMakeDate(currentDate);
                          tLLSecurityReceiptSchema.setMakeTime(currentTime);
                          tLLSecurityReceiptSchema.setModifyDate(currentDate);
                          tLLSecurityReceiptSchema.setModifyTime(currentTime);
                          tLLSecurityReceiptSchema.setMngCom(MngCom);
                          tLLSecurityReceiptSchema.setOperator(Operator);
                          map.put(tLLSecurityReceiptSchema, "INSERT");
                      }
                      //查找和LLFainMinSchema相关的LLSecuDetail
                      LLSecuDetailDB tLLSecuDetailDB = new LLSecuDetailDB();
                      tLLSecuDetailDB.setMainFeeNo(tLLFeeMainSchema.getMainFeeNo());
                      LLSecuDetailSet tLLSecuDetailSet = tLLSecuDetailDB.query();
                      if (tLLSecuDetailSet != null && tLLSecuDetailSet.size() > 0) {
                          for (int j = 1; j <= tLLSecuDetailSet.size(); j++) {

                              LLSecuDetailSchema tLLSecuDetailSchema =
                                      tLLSecuDetailSet.get(j);

                              tLLSecuDetailSchema.setMainFeeNo(MainFeeNo);
                              tLLSecuDetailSchema.setRgtNo(AppealNo);
                              tLLSecuDetailSchema.setCaseNo(AppealNo);

                              tLLSecuDetailSchema.setMakeDate(currentDate);
                              tLLSecuDetailSchema.setMakeTime(currentTime);
                              tLLSecuDetailSchema.setModifyDate(currentDate);
                              tLLSecuDetailSchema.setModifyTime(currentTime);
                              tLLSecuDetailSchema.setMngCom(MngCom);
                              tLLSecuDetailSchema.setOperator(Operator);
                          }
                          map.put(tLLSecuDetailSet, "INSERT");
                      }
                      tLLFeeMainSchema.setMainFeeNo(MainFeeNo);
                      tLLFeeMainSchema.setCaseNo(AppealNo);
                      tLLFeeMainSchema.setMakeDate(currentDate);
                      tLLFeeMainSchema.setMakeTime(currentTime);
                      tLLFeeMainSchema.setModifyDate(currentDate);
                      tLLFeeMainSchema.setModifyTime(currentTime);
                      tLLFeeMainSchema.setMngCom(MngCom);
                      tLLFeeMainSchema.setOperator(Operator);
                  }
                  map.put(tLLFeeMainSet, "INSERT");
              }

     LLBnfSet tLLBnfSet = null;
     LLBnfDB tLLBnfDB = new LLBnfDB();
     tLLBnfDB.setCaseNo(mLLCaseSchema.getCaseNo());
     tLLBnfSet = tLLBnfDB.query();
     if (tLLBnfSet != null && tLLBnfSet.size() > 0)
       {
        for (int i = 1; i <= tLLBnfSet.size(); i++)
         {
           LLBnfSchema tLLBnfSchema =
                  tLLBnfSet.get(i);

           tLLBnfSchema.setCaseNo(AppealNo);
           tLLBnfSchema.setMakeDate(currentDate);
           tLLBnfSchema.setMakeTime(currentTime);
           tLLBnfSchema.setModifyDate(currentDate);
           tLLBnfSchema.setModifyTime(currentTime);
           tLLBnfSchema.setOperator(Operator);
          }
         map.put(tLLBnfSet, "INSERT");
       }


     LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
     tLLCaseRelaDB.setCaseNo(mLLCaseSchema.getCaseNo());
     tLLCaseRelaSet = tLLCaseRelaDB.query();
     if (tLLCaseRelaSet != null && tLLCaseRelaSet.size() > 0)
       {
        for (int i = 1; i <= tLLCaseRelaSet.size(); i++)
         {
           LLCaseRelaSchema tLLCaseRelaSchema =
                  tLLCaseRelaSet.get(i);

           tLLCaseRelaSchema.setCaseNo(AppealNo);

          }
          map.put(tLLCaseRelaSet, "INSERT");
       }

//准备返回LLCasewSchema的CaseNo和RgtNo
       mLLCaseSchema.setCaseNo(AppealNo);
       mLLCaseSchema.setRgtNo(AppealNo);

   }

    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
      System.out.println(" <--Start GetInputData--> ");

      this.mLLCaseSchema.setSchema((LLCaseSchema) cInputData.
                                   getObjectByObjectName("LLCaseSchema", 0));
      this.mLLAppealSchema.setSchema((LLAppealSchema) cInputData.
                                     getObjectByObjectName("LLAppealSchema",
              0));
      this.mLLRegisterSchema.setSchema((LLRegisterSchema) cInputData.
                                       getObjectByObjectName(
              "LLRegisterSchema", 0));
      mLLAppClaimReasonSet.set((LLAppClaimReasonSet) cInputData.
                               getObjectByObjectName("LLAppClaimReasonSet", 0));
      this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                  getObjectByObjectName(
                                          "GlobalInput", 0));
      tLLSubReportSet = (LLSubReportSet) cInputData.getObjectByObjectName(
              "LLSubReportSet", 0);
      mLLCaseOpTimeSchema = (LLCaseOpTimeSchema) cInputData.getObjectByObjectName(
                            "LLCaseOpTimeSchema",0);
      
      LLCaseExtSchema tLLCaseExtSchema = new LLCaseExtSchema();//2807领款人和被保人关
      tLLCaseExtSchema = (LLCaseExtSchema)cInputData.getObjectByObjectName("LLCaseExtSchema", 0);
      if(tLLCaseExtSchema!=null){
      	this.mLLCaseExtSchema.setSchema(tLLCaseExtSchema); 
      }
      
      return true;
  }

  private boolean prepareOutputData() {
    try {
      this.mInputData.clear();
      this.mInputData.add(this.mLLRegisterSchema);
      mInputData.add(map);
      mResult.clear();
      mResult.add(this.mLLCaseSchema);

      System.out.println("================");
      System.out.println("CaseNo"+this.mLLCaseSchema.getCaseNo());
      System.out.println("RgtNo"+this.mLLCaseSchema.getRgtNo());
      System.out.println("================");

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LDDrugBL";
      tError.functionName = "prepareData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  private int calAge()
 {
     Date Birthday = fDate.getDate(mLLCaseSchema.getCustBirthday());
     String strNow = PubFun.getCurrentDate();
     Date aNow = fDate.getDate(strNow);
     int CustomerAge = PubFun.calInterval(Birthday,aNow,"Y");
     System.out.println("年龄计算函数。。。"+CustomerAge);
     return CustomerAge;
 }

 private boolean checkInputData() {
	 
     	//add by Houyd 社保申诉纠错，校验只允许社保人员操作
     	System.out.println("申诉原案件号为："+mLLCaseSchema.getCaseNo());
	  	ExeSQL tSSExeSQL = new ExeSQL();
	  	if("1".equals(LLCaseCommon.checkSocialSecurity(mLLCaseSchema.getCaseNo(), null)))
	  	{	
	  		mSocialFlag = true;
	  		String checkSql = "Select '1' From LLSocialClaimUser where UserCode = '"+this.mGlobalInput.Operator+"' and StateFlag = '1' and HandleFlag ='0'";
	  		String tOper_Result = tSSExeSQL.getOneValue(checkSql);
	  		if(tOper_Result!=null&&"1".equals(tOper_Result))
	  		{
		  
	  		}else{
			  
			  CError tError = new CError();
		      tError.moduleName = "GrpRegisterBL";
		      tError.functionName = "checkData";
		      tError.errorMessage = "您不具有社保案件理赔操作权限！";
		      this.mErrors.addOneError(tError);
			  return false;  
		  }		  
	  }else{//普通类案件
		  String checkSql = "Select '1' From LLSocialClaimUser where UserCode = '"+this.mGlobalInput.Operator+"' ";
	  		String tOper_Result = tSSExeSQL.getOneValue(checkSql);
	  		if(tOper_Result!=null&&"1".equals(tOper_Result))
	  		{
	  			CError tError = new CError();
			    tError.moduleName = "GrpRegisterBL";
			    tError.functionName = "checkData";
			    tError.errorMessage = "社保用户无法对普通类案件提起申诉纠错！";
			    this.mErrors.addOneError(tError);
				return false;  
	  		}else{
			  
			  
		  }		  
	  }
	 
     String result = LLCaseCommon.checkPerson(mLLCaseSchema.getCustomerNo());
     if (!"".equals(result)){
         CError.buildErr(this, "工单号为"+result+"的客户现正进行保全操作或续保，请稍后受理");
         return false;
     }
     String wnsql = "select distinct riskcode from llclaimdetail where caseno='"+mLLCaseSchema.getCaseNo()+"'";
     ExeSQL wnExeSQL = new ExeSQL();
     SSRS wnSSRS = wnExeSQL.execSQL(wnsql);
     
   //#4276 XQGLD-2019-00104申请放开理赔的申诉纠错对税优险种的校验，允许受理成功
     String wdxsql="select lm.riskcode from lmriskapp lm where 1=1  and lm.taxoptimal='Y' ";
     SSRS wdxSSRS = wnExeSQL.execSQL(wdxsql);
     String rsks="";//税优
     if(wdxSSRS.getMaxRow()>=1)
     {
    	 for (int j = 1; j <= wdxSSRS.getMaxRow(); j++) {
    		 String rsk=wdxSSRS.GetText(j, 1);
    		 if(j!=wdxSSRS.getMaxRow()){
    			 rsks=rsks+rsk+",";
    		 }else{
    			 rsks=rsks+rsk;
    		 }
		}
     }
     System.out.println("税优险种："+rsks);
     if(wnSSRS.getMaxRow()>=1)
     {
    	 for (int i=1; i<=wnSSRS.getMaxRow();i++)
    	 {
    		 if(LLCaseCommon.chenkWN(wnSSRS.GetText(i, 1)))
             {//说明是万能险。
    			 if(rsks.equals("") || !rsks.equals("") && rsks.indexOf(wnSSRS.GetText(i, 1))==-1){
    				 //说明不是税优
    				 CError.buildErr(this, "万能险不能进行申诉纠错!");
                     return false;
    			 }
             }   
    	 }
     }
     String sql = "select name,idno from lcinsured where insuredno='"+mLLCaseSchema.getCustomerNo()+"' group by name,idno union select name,idno from lbinsured where insuredno='"+mLLCaseSchema.getCustomerNo()+"' group by name,idno with ur";
     ExeSQL tExeSQL = new ExeSQL();
     SSRS tSSRS = tExeSQL.execSQL(sql);
     int count = tSSRS.getMaxRow();
     if(tSSRS.getMaxRow()<=0){
     	 CError.buildErr(this, "系统中不存在该用户！");
          return false;
     }else{
     	System.out.println(tSSRS.GetText(1, 1)+":"+StrTool.GBKToUnicode(mLLCaseSchema.getCustomerName())+":"+StrTool.GBKToUnicode(tSSRS.GetText(1, 2)));
     	for(int j=1;j<=count;j++){
     		if(tSSRS.GetText(j, 1).equals(StrTool.GBKToUnicode(mLLCaseSchema.getCustomerName()))){
     			return true;
     		}else{
     			if(j==count){
     				CError.buildErr(this, "请核实被保险人姓名是否正确！");
                     return false;
     			}else{
     				continue;
     			}
     		}
     	}
       for(int i=1;i<=count;i++){
     		if(tSSRS.GetText(i, 2).equals(mLLCaseSchema.getIDNo())){
     			return true;
     		}else{
     			if(i==count){
     				CError.buildErr(this, "请核实被保险人身份证号是否正确！");
                     return false;
     			}else{
     				continue;
     			}
     		}

     	}
     }
        return true;
}


  public VData getResult() {
    return this.mResult;
  }
}
