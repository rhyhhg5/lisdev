package com.sinosoft.lis.llcase;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLCaseTransferBL {
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private MMap map = new MMap();
    private String mRemark = "";

    private GlobalInput mGlobalInput = new GlobalInput();
    private LLCaseSet mLLCaseSet = new LLCaseSet();

    ExeSQL tExeSQL = new ExeSQL();

    public LLCaseTransferBL() {
    }

    public static void main(String[] args) {

    }

    public VData getResult() {
        return mResult;
    }


    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        //得到输入数据
        if (!getInputData()) {
            return false;
        }
//          mOperate = cOperate;
        //检查数据合法性
        if (!checkInputData()) {
            return false;
        }

        //进行业务处理
        if (!dealData(cOperate)) {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLCaseTransferBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;

        return true;
    }

    private boolean getInputData() {
        System.out.println("getInputData()...");

        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);

        mLLCaseSet = (LLCaseSet) mInputData.getObjectByObjectName(
                "LLCaseSet", 0);
        mRemark = (String) mInputData.lastElement();

        return true;
    }


    private boolean prepareOutputData() {
        System.out.println("prepareOutputData()...");

        try {
            mInputData.clear();
            mInputData.add(map);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ICaseCureBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    private boolean checkInputData() {

        return true;
    }

    private boolean dealData(String cOperate) {

        LLCaseBackSet mLLCaseBackSet = new LLCaseBackSet();
        LLCaseDB tLLCaseDB = new LLCaseDB();
        LLCaseSet tLLCaseSet = new LLCaseSet();
        int count = mLLCaseSet.size();

        for (int j = 1; j <= count; j++) {
            LLCaseSchema tLLCaseSchema = new LLCaseSchema();
            tLLCaseSchema.setCaseNo(mLLCaseSet.get(j).getCaseNo());
            tLLCaseDB.setCaseNo(tLLCaseSchema.getCaseNo());
            tLLCaseDB.getInfo();
            tLLCaseSchema = tLLCaseDB.getSchema();

            if (!LLCaseCommon.checkHospCaseState(tLLCaseSchema.getCaseNo())) {
                CError.buildErr(this,
                                tLLCaseSchema.getCaseNo() + "医保通案件待确认，不能进行处理");
                return false;
            }

            String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
            String CaseBackNo = PubFun1.CreateMaxNo("CaseBack", tLimit);
            LLCaseBackSchema mLLCaseBackSchema = new LLCaseBackSchema();
            mLLCaseBackSchema.setCaseBackNo(CaseBackNo);
            mLLCaseBackSchema.setRemark(mRemark);
            mLLCaseBackSchema.setCaseNo(tLLCaseSchema.getCaseNo());
            mLLCaseBackSchema.setRgtNo(tLLCaseSchema.getRgtNo());
            mLLCaseBackSchema.setBeforState(tLLCaseSchema.getRgtState());
            mLLCaseBackSchema.setAfterState(tLLCaseSchema.getRgtState());
            mLLCaseBackSchema.setOHandler(tLLCaseSchema.getHandler());
            mLLCaseBackSchema.setNHandler(cOperate);
            mLLCaseBackSchema.setMngCom(mGlobalInput.ManageCom);
            mLLCaseBackSchema.setOperator(mGlobalInput.Operator);
            mLLCaseBackSchema.setMakeDate(PubFun.getCurrentDate());
            mLLCaseBackSchema.setMakeTime(PubFun.getCurrentTime());
            mLLCaseBackSchema.setModifyDate(PubFun.getCurrentDate());
            mLLCaseBackSchema.setModifyTime(PubFun.getCurrentTime());
            System.out.print("mLLCaseBackSchema" +
                             mLLCaseBackSchema.getModifyDate());
            mLLCaseBackSchema.setBackType("9");
            mLLCaseBackSet.add(mLLCaseBackSchema);
            tLLCaseSchema.setClaimer(cOperate);
            tLLCaseSet.add(tLLCaseSchema);

        }

        map.put(tLLCaseSet, "UPDATE");
        map.put(mLLCaseBackSet, "INSERT");

        this.mResult.add(map);
        return true;
    }


}
