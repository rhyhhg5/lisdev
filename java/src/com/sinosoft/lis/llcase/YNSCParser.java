/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: 云南社保理赔磁盘导入的解析类，从一个XML数据流中解析出批次下分案信息</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author Xx
 * @version 6.0
*/

package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.PubFun;
import org.apache.xpath.XPathAPI;
import org.apache.xpath.objects.XObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.lis.f1print.ExportExcelXML;

public class YNSCParser implements SIParser{

    //案件
    private static String PATH_INSURED = "ROW";
    private static String PATH_SecurityNo = "SecurityNo";
    private static String PATH_CustomerNo = "CustomerNo";
    private static String PATH_CustomerName = "CustomerName";
    private static String PATH_Sex = "Sex";
    private static String PATH_IDNo = "IDNo";
    private static String PATH_HosSecNo = "HosSecNo";
    private static String PATH_HospitalCode = "HospitalCode";
    private static String PATH_HospitalName = "HospitalName";
    private static String PATH_inpatientNo = "inpatientNo";
    private static String PATH_RealHospDate = "RealHospDate";
    private static String PATH_FeeType = "FeeType";
    private static String PATH_FeeDate = "FeeDate";
    private static String PATH_ApplyAmnt = "ApplyAmnt";
    private static String PATH_SelfAmnt = "SelfAmnt";
    private static String PATH_SelfPay2 = "SelfPay2";
    private static String PATH_GetLimit = "GetLimit";
    private static String PATH_MidAmnt = "MidAmnt";
    private static String PATH_PlanFee = "PlanFee";
    private static String PATH_HighAmnt2 = "HighAmnt2";
    private static String PATH_SupInHosFee = "SupInHosFee";
    private static String PATH_HighAmnt1 = "HighAmnt1";
    private static String PATH_OfficialSubsidy = "OfficialSubsidy";
    private static String PATH_DiseaseName = "DiseaseName";
    private static String PATH_DiseaseCode = "DiseaseCode";
    private static String PATH_HospStartDate = "HospStartDate";
    private static String PATH_HospEndDate = "HospEndDate";
    private static String PATH_AffixNo = "AffixNo";
    private static String PATH_OriginFlag = "OriginFlag";

    // 下面是一些常量定义
    private String mRgtNo = "";
    private String mGrpContNo = "";
    private String mPath = "";
    private String[] Title = null;
    private String vtsName = "ClaimErrList.vts";
    private ListTable  FalseListTable = new ListTable();
    private GlobalInput mG = new GlobalInput();
    public CErrors mErrors = new CErrors();
    
    public YNSCParser(){

    }

    public boolean setGlobalInput(GlobalInput aG) {
        this.mG = aG;
        FalseListTable.setName("FAULT");
        return true;
    }

    public void setFormTitle(String[] cTitle,String cvtsName){
        Title = cTitle;
        vtsName = cvtsName;
    }


    /**
     * 解析案件结点(公用函数)
     * @param node Node
     * @return VData
     */
    public VData parseOneInsuredNode(Node node) {
        VData tReturn = new VData();
        // 得到被保人信息
        NodeList nodeList = null;
        try {
            nodeList = XPathAPI.selectNodeList(node, PATH_INSURED);
        } catch (Exception ex) {
            ex.printStackTrace();
            nodeList = null;
            return null;
        }
        int nLength = nodeList == null ? 0 : nodeList.getLength();
        int FailureCount = 0;
        for (int nIndex = 0; nIndex < nLength; nIndex++) {
            Node nodeCase = nodeList.item(nIndex);
            if(!genCase(nodeCase)){
                System.out.println("导入这个人失败");
                FailureCount++;
                //写导出错误xml的函数
            }
        }
        String[] tErrorTitle = {"医保号","客户号", "客户姓名", "性别",
                         "身份证号码","医保编码","系统医院编码","医院名称",
                         "住院号","住院天数","支付类别","经办日期","费用总额",
                         "全自费部分","先自付部分","起付线","统筹自付部分","统筹支付部分",
                         "35000-49000","大病支付部分","大病自付部分","超限自付部分",
                         "出院诊断","ICD编码","入院日期", "出院日期","实赔金额","医疗标志","错误原因"};
        String[] tErrorType = {"String","String","String","String","String","String","String",
        		               "String","String","Number","String","DateTime","Number",
        		               "Number","Number","Number","Number","Number",
        		               "Number","Number","Number","Number",
        		               "String","String","DateTime","DateTime","Number","String","String"};
        LLExportErrorList xmlexport = new LLExportErrorList(); //新建一个XmlExport的实例
        xmlexport.createDocument(mRgtNo);
        xmlexport.addListTable(FalseListTable, tErrorTitle, tErrorType);
        xmlexport.outputDocumentToFile(mPath, mRgtNo); //输出xml文档到文件
        String FailureNum = FailureCount+"";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("FailureNum",FailureNum);
        tReturn.clear();
        tReturn.add(tTransferData);
        return tReturn;
    }

    public boolean setParam(String aRgtNo,String aGrpContNo,String aPath){
        mRgtNo = aRgtNo;
        mGrpContNo = aGrpContNo;
        mPath = aPath;
        return true;
    }

    /**
     * 解析一个DOM树的节点，在这个节点中，包含了一个团体下个人保单的所有信息。
     * @param node Node
     * @param vReturn VData
     * @return boolean
     */
    private static String parseNode(Node node, String strPath) {
        String strValue = "";
        try {
            XObject xobj = XPathAPI.eval(node, strPath);
            strValue = xobj == null ? "" : xobj.toString();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return strValue;
    }

    private boolean genCase(Node nodeCase){
        String aCustomerNo = parseNode(nodeCase,YNSCParser.PATH_CustomerNo);
        String aCustomerName = parseNode(nodeCase,YNSCParser.PATH_CustomerName);
        String aSecurityNo = parseNode(nodeCase,YNSCParser.PATH_SecurityNo);
        String aSex = parseNode(nodeCase,YNSCParser.PATH_Sex);
        String aIDNo = parseNode(nodeCase,YNSCParser.PATH_IDNo);
        String InHosDate = FormatDate(parseNode(nodeCase,YNSCParser.PATH_HospStartDate));
        String strSQL0 ="select distinct a.insuredno,a.name,a.sex,a.birthday,a.idno,a.othidno,a.insuredstat,b.grpname,a.idtype ";
        String sqlpart1="from lcinsured a,ldperson b where b.CustomerNo=a.insuredNo ";
        String sqlpart2="from lbinsured a,ldperson b where b.CustomerNo=a.insuredNo ";
        String wherePart = "";
        String msgPart = "";
        String ErrMessageAL = "";
        
        if (!"".equals(aCustomerName) && !"null".equals(aCustomerName)) {
        	wherePart += " and a.name='" + aCustomerName + "' ";
            msgPart = "客户姓名为" + aCustomerName ;
		}
        
        if (!aIDNo.equals("null") && !"".equals(aIDNo)) {
			wherePart += " and a.idno='" + aIDNo + "' and a.GrpContNo = '"+mGrpContNo+"'";
			msgPart += "，身份证号为" + aIDNo;
		} else {
			ErrMessageAL = "未提供身份证号，无法确认客户身份！";
			getFalseList(nodeCase, ErrMessageAL);
			return false;
		}
        if("".equals(InHosDate) || "null".equals(InHosDate)){
        	String ErrMessage = "入院日期不能为空！";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }
        //String wherePart = " and a.othidno='"+aSecurityNo+"' and a.GrpContNo = '"+mGrpContNo+"'";
        String strSQL=strSQL0+sqlpart1+ wherePart+" union "+strSQL0+sqlpart2+wherePart;
        ExeSQL texesql = new ExeSQL();
        SSRS tssrs = new SSRS();
        tssrs = texesql.execSQL(strSQL);
        System.out.println(strSQL);
        if(tssrs==null || tssrs.getMaxRow()<=0){
            CError.buildErr(this,"查询客户信息失败");
            ErrMessageAL = "团单" + mGrpContNo + "下没有" + msgPart + "的被保险人";
			getFalseList(nodeCase, ErrMessageAL);
            return false;
        }
        String tCustomerNo=tssrs.GetText(1,1);
        String tCustomerName=tssrs.GetText(1,2);
        if (!aCustomerNo.equals(tCustomerNo)) {
            String ErrMessage = "客户号码与系统中该保单下对应社保号的客户不对。";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }
        if (!aCustomerName.equals(tCustomerName)) {
            String ErrMessage = "客户姓名与系统中该保单下对应社保号的客户不对。";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }

      String tIdtype=tssrs.GetText(1, 9).equals("null") ? "" :
            tssrs.GetText(1, 9);;//获得证件类型
            /*    if(tIdtype.equals("0"))//是身份证时，对身份证号进行校验
        {

        	String tId=tssrs.GetText(1, 5).equals("null") ? "" :
        		tssrs.GetText(1, 5);;//获得身份证号
        	if(!PubFun.checkIDNo(tId))//校验数据库里存储的身份证号，by zzh 20100925
        	{	
        		String ErrMessage =aIDNo+"的客户身份证号错误，请先做客户资料变更！";
        		getFalseList(nodeCase,ErrMessage);
        		return false;
        	}
        }*/
           

        if(parseNode(nodeCase,YNSCParser.PATH_FeeDate)==null||
        parseNode(nodeCase,YNSCParser.PATH_FeeDate).equals("")){
         String ErrMessage = "社保结算日期为空。";
         getFalseList(nodeCase,ErrMessage);
         return false;

     }

        String strCustomerSQL="select * from lpedorapp a,lpgrpedoritem b where a.edoracceptno in (select max(edorno) from lpdiskimport where  insuredno='"+aCustomerNo+"' and grpcontno ='"+mGrpContNo+"') and a.edorstate !='0' and a.edoracceptno = b.edoracceptno and b.edortype ='ZT'";
        ExeSQL customersql = new ExeSQL();
        SSRS customertssrs = new SSRS();
        customertssrs = texesql.execSQL(strCustomerSQL);
        String result = LLCaseCommon.checkPerson(aCustomerNo);
        if(!"".equals(result)){
            String ErrMessage = msgPart + "的客户正在进行保全操作！";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }
        if(customertssrs.getMaxRow()>0){
        	String ErrMessage = "客户正在进行保全减人操作！";
            getFalseList(nodeCase, ErrMessage);
            return false;

        }
//     if(parseNode(nodeCase,YNSCParser.PATH_SupInHosFee)==null||
//        parseNode(nodeCase,YNSCParser.PATH_SupInHosFee).equals("")){
//         String ErrMessage = "大额支付额为空，无法进行理算。";
//         getFalseList(nodeCase,ErrMessage);
//         return false;
//     }


        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
        LLSecurityReceiptSchema tLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
        LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
        LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
        LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
        VData tVData = new VData();

        genCaseInfoBL tgenCaseInfoBL  = new genCaseInfoBL();

        tLLCaseSchema.setRgtType("1");
        tLLRegisterSchema.setRgtNo(mRgtNo);
        tLLRegisterSchema.setRgtObjNo(mGrpContNo);

        LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
        tLLAppClaimReasonSchema.setReasonCode("02");        //原因代码
        tLLAppClaimReasonSchema.setReason("住院");                //申请原因
        tLLAppClaimReasonSchema.setCustomerNo(aCustomerNo);
        tLLAppClaimReasonSchema.setReasonType("0");

        tLLCaseSchema.setRgtState("03");//案件状态
        tLLCaseSchema.setCustomerName(aCustomerName);
        tLLCaseSchema.setCustomerNo(aCustomerNo);
//        tLLCaseSchema.setDeathDate(request.getParameter("DeathDate"));
        tLLCaseSchema.setCustBirthday(tssrs.GetText(1,4));
        tLLCaseSchema.setGrpName(tssrs.GetText(1,8));
        tLLCaseSchema.setCaseNo("");

//      modify by zhangyige 2012-07-09
        String aPrePaidFlag = "";
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mRgtNo);
        if(tLLRegisterDB.getInfo())
		{
			aPrePaidFlag = tLLRegisterDB.getSchema().getPrePaidFlag();
		}
        tLLCaseSchema.setPrePaidFlag(aPrePaidFlag);
        
        tLLCaseSchema.setCustomerSex(aSex);
        tLLCaseSchema.setIDType("0");
        tLLCaseSchema.setRgtNo(mRgtNo);
        tLLCaseSchema.setIDNo(parseNode(nodeCase,YNSCParser.PATH_IDNo));
        tLLCaseSchema.setOtherIDType("5");
        tLLCaseSchema.setOtherIDNo(aSecurityNo);
        tLLCaseSchema.setCustState(tssrs.GetText(1,7));
        tLLCaseSchema.setSurveyFlag("0");
        tLLCaseSchema.setAccdentDesc("磁盘导入");
        String aAccDate = FormatDate(parseNode(nodeCase,YNSCParser.PATH_HospStartDate));
        if(aAccDate==null || "".equals(aAccDate) ||"null".equals(aAccDate)){
           String ErrMessage = msgPart + "的客户出险日期不能为空，或出险日期格式不正确应为'YYYY-MM-DD'！";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }
        tLLCaseSchema.setAccidentDate(FormatDate(parseNode(nodeCase,YNSCParser.PATH_HospStartDate)));
        if(tLLCaseSchema.getAccidentDate()==null)//by zzh 0930
        {
        	String ErrMessage = msgPart + "出险日期格式不正确,应为'YYYY-MM-DD'！";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }
        String tPayModeSQL = "SELECT casegetmode FROM llregister WHERE rgtno='"+mRgtNo+"'";
        ExeSQL tPMExSQL = new ExeSQL();
        SSRS tPMSSRS = tPMExSQL.execSQL(tPayModeSQL);
        if (tPMSSRS.getMaxRow() > 0) {
           try {
               tLLCaseSchema.setCaseGetMode(tPMSSRS.GetText(1, 1));
           } catch (Exception ex) {
               System.out.println("插入给付方式失败！");
           }
        }

        String sql =
                "select bankcode,bankaccno,accname from lcinsured where insuredno='" +
                aCustomerNo + "' and grpcontno='" + mGrpContNo + "'" +
                " union " +
                "select bankcode,bankaccno,accname from lbinsured where insuredno='" +
                aCustomerNo + "' and grpcontno='" + mGrpContNo + "'";

        ExeSQL texesql1 = new ExeSQL();
        SSRS trr = texesql1.execSQL(sql);
        if (trr.getMaxRow() > 0) {
            try {
                tLLCaseSchema.setBankCode(trr.GetText(1, 1));
                tLLCaseSchema.setBankAccNo(trr.GetText(1, 2));
                tLLCaseSchema.setAccName(trr.GetText(1, 3));
            } catch (Exception ex) {
                System.out.println("插入用户帐户信息失败！");
            }
        }
        
        String tAccSQL = "select 1 from lcgrppol a,lmriskapp b where a.riskcode=b.riskcode"
            + " and b.risktype3='7' and b.risktype <>'M'"
            + " and a.grpcontno='"+ mGrpContNo +"'";
        SSRS tAccSSRS = texesql1.execSQL(tAccSQL);
        boolean tDiseaseFlag = true;
        if (tAccSSRS.getMaxRow()>0) {
        	tDiseaseFlag = false;
        }
        
        String tHospitalCode = parseNode(nodeCase,YNSCParser.PATH_HospitalCode);
        String tHospitalName = parseNode(nodeCase,YNSCParser.PATH_HospitalName);
        
        if (("".equals(tHospitalCode)||"null".equals(tHospitalCode))&&tDiseaseFlag) {
        	String ErrMessage = "医院编码不能为空";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }
        if (("".equals(tHospitalName)||"null".equals(tHospitalName))&&tDiseaseFlag) {
        	String ErrMessage = "医院名称不能为空";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }
        
        tLLFeeMainSchema.setRgtNo(mRgtNo);
        tLLFeeMainSchema.setCaseNo("");
        tLLFeeMainSchema.setCustomerNo(aCustomerNo);
        tLLFeeMainSchema.setCustomerSex(aSex);
        tLLFeeMainSchema.setHospitalCode(tHospitalCode);
        tLLFeeMainSchema.setHospitalName(tHospitalName);
        tLLFeeMainSchema.setHosAtti(parseNode(nodeCase,YNSCParser.PATH_HosSecNo));
        tLLFeeMainSchema.setReceiptNo("53000");
        tLLFeeMainSchema.setFeeType(parseNode(nodeCase,YNSCParser.PATH_FeeType));
        tLLFeeMainSchema.setFeeAtti("4");
        tLLFeeMainSchema.setAffixNo(parseNode(nodeCase, YNSCParser.PATH_AffixNo));
        tLLFeeMainSchema.setOriginFlag(parseNode(nodeCase, YNSCParser.PATH_OriginFlag));
        System.out.println(tLLFeeMainSchema.getAffixNo());
        System.out.println(tLLFeeMainSchema.getOriginFlag());
        String tApplyAmnt=parseNode(nodeCase, YNSCParser.PATH_ApplyAmnt);
        if(tApplyAmnt==null || "".equals(tApplyAmnt) ||"null".equals(tApplyAmnt)){
        	String ErrMessage = msgPart + "的客户费用总额为空！";
            getFalseList(nodeCase, ErrMessage);
            return false;
        }
        tLLFeeMainSchema.setSumFee(parseNode(nodeCase, YNSCParser.PATH_ApplyAmnt));
        tLLFeeMainSchema.setFeeDate(FormatDate(parseNode(nodeCase,YNSCParser.PATH_FeeDate)));
        tLLFeeMainSchema.setHospStartDate(FormatDate(parseNode(nodeCase,YNSCParser.PATH_HospStartDate)));
        tLLFeeMainSchema.setHospEndDate(FormatDate(parseNode(nodeCase,YNSCParser.PATH_HospEndDate)));
        tLLFeeMainSchema.setRealHospDate(parseNode(nodeCase,YNSCParser.PATH_RealHospDate));
        tLLFeeMainSchema.setInHosNo(parseNode(nodeCase,YNSCParser.PATH_inpatientNo));
        tLLFeeMainSchema.setSecurityNo(aSecurityNo);
        tLLFeeMainSchema.setInsuredStat(tssrs.GetText(1,7));

        tLLSecurityReceiptSchema.setApplyAmnt(parseNode(nodeCase, YNSCParser.PATH_ApplyAmnt));
        tLLSecurityReceiptSchema.setSelfAmnt(parseNode(nodeCase, YNSCParser.PATH_SelfAmnt));
        tLLSecurityReceiptSchema.setSelfPay2(parseNode(nodeCase, YNSCParser.PATH_SelfPay2));
        tLLSecurityReceiptSchema.setGetLimit(parseNode(nodeCase, YNSCParser.PATH_GetLimit));
        tLLSecurityReceiptSchema.setMidAmnt(parseNode(nodeCase, YNSCParser.PATH_MidAmnt));
        tLLSecurityReceiptSchema.setPlanFee(parseNode(nodeCase,  YNSCParser.PATH_PlanFee));
        tLLSecurityReceiptSchema.setHighAmnt1(parseNode(nodeCase, YNSCParser.PATH_HighAmnt1));
        tLLSecurityReceiptSchema.setStandbyAmnt(parseNode(nodeCase,YNSCParser.PATH_HighAmnt2));
        tLLSecurityReceiptSchema.setSupInHosFee(parseNode(nodeCase,YNSCParser.PATH_SupInHosFee));
        tLLSecurityReceiptSchema.setHighAmnt2(parseNode(nodeCase,  YNSCParser.PATH_SupInHosFee));
        tLLSecurityReceiptSchema.setOfficialSubsidy(parseNode(nodeCase,YNSCParser.PATH_OfficialSubsidy));
        System.out.println(tLLFeeMainSchema.getAffixNo());
        if(tLLFeeMainSchema.getAffixNo()==null||tLLFeeMainSchema.getAffixNo().trim().equals("")){
            double tsumMoney = tLLSecurityReceiptSchema.getSelfAmnt() +
                               tLLSecurityReceiptSchema.getSelfPay2()
                               + tLLSecurityReceiptSchema.getGetLimit() +
                               tLLSecurityReceiptSchema.getMidAmnt()
                               + tLLSecurityReceiptSchema.getPlanFee() +
                               tLLSecurityReceiptSchema.getStandbyAmnt()
                               + tLLSecurityReceiptSchema.getSupInHosFee() +
                               tLLSecurityReceiptSchema.getHighAmnt1()
                               + tLLSecurityReceiptSchema.getOfficialSubsidy();
            System.out.println(tLLSecurityReceiptSchema.getApplyAmnt());
            System.out.println(tsumMoney);
            System.out.println(Math.abs(tLLSecurityReceiptSchema.getApplyAmnt() -
                                        tsumMoney));
            if (Math.abs(tLLSecurityReceiptSchema.getApplyAmnt() - tsumMoney) >
                0.01) {
                CError.buildErr(this, "各费用分项之和不等于费用总额！");
                getFalseList(nodeCase, "各费用分项之和不等于费用总额！");
                return false;
            }
        }

        String tDiseaseCode = ""+parseNode(nodeCase,YNSCParser.PATH_DiseaseCode);
        String tDiseaseName = ""+parseNode(nodeCase,YNSCParser.PATH_DiseaseName);
        if (("".equals(tDiseaseCode)||"null".equals(tDiseaseCode))&&tDiseaseFlag) {
            CError.buildErr(this, "疾病编码不能为空！");
            getFalseList(nodeCase, "疾病编码不能为空！");
            return false;
        }
        if (("".equals(tDiseaseName)||"null".equals(tDiseaseName))&&tDiseaseFlag) {
            CError.buildErr(this, "出院诊断不能为空！");
            getFalseList(nodeCase, "出院诊断不能为空！");
            return false;
        }

        if (!"".equals(tDiseaseCode)&&!"null".equals(tDiseaseCode)) {
            tLLCaseCureSchema.setDiseaseCode(tDiseaseCode);
            tLLCaseCureSchema.setDiseaseName(tDiseaseName);
            tLLCaseCureSet.add(tLLCaseCureSchema);
        }

        //保存案件时效
        tLLCaseOpTimeSchema.setRgtState("01");
        tLLCaseOpTimeSchema.setStartDate(PubFun.getCurrentDate());
        tLLCaseOpTimeSchema.setStartTime(PubFun.getCurrentTime());

        System.out.println("<--Star Submit VData-->");
        tVData.add(tLLAppClaimReasonSchema);
        tVData.add(tLLCaseSchema);
        tVData.add(tLLRegisterSchema);
        tVData.add(tLLFeeMainSchema);
        tVData.add(tLLSecurityReceiptSchema);
        tVData.add(tLLCaseCureSet);
        tVData.add(tLLCaseOpTimeSchema);

        tVData.add(mG);
        System.out.println("<--Into tSimpleClaimUI-->");
        if (!tgenCaseInfoBL.submitData(tVData, "INSERT||MAIN")){
            CErrors tError = tgenCaseInfoBL.mErrors;
            String ErrMessage = tError.getFirstError();
            getFalseList(nodeCase, ErrMessage);
            return false;

        }
        return true;
    }

    private boolean getFalseList(Node nodeCase,String ErrMessage){
        String[] strCol = new String[28];
        strCol[0] = parseNode(nodeCase,YNSCParser.PATH_SecurityNo);
        strCol[1] = parseNode(nodeCase,YNSCParser.PATH_CustomerNo);
        strCol[2] = parseNode(nodeCase,YNSCParser.PATH_CustomerName);
        strCol[3] = parseNode(nodeCase,YNSCParser.PATH_Sex);
        strCol[4] = parseNode(nodeCase,YNSCParser.PATH_IDNo);
        strCol[5] = parseNode(nodeCase,YNSCParser.PATH_HosSecNo);
        strCol[6] = parseNode(nodeCase,YNSCParser.PATH_HospitalCode);
        strCol[7] = parseNode(nodeCase,YNSCParser.PATH_HospitalName);
        strCol[8] = parseNode(nodeCase,YNSCParser.PATH_inpatientNo);
        strCol[9] = parseNode(nodeCase,YNSCParser.PATH_RealHospDate);
        strCol[10] = parseNode(nodeCase,YNSCParser.PATH_FeeType);
        strCol[11] = FormatDate(parseNode(nodeCase,YNSCParser.PATH_FeeDate));
        strCol[12] = parseNode(nodeCase,YNSCParser.PATH_ApplyAmnt);
        strCol[13] = parseNode(nodeCase,YNSCParser.PATH_SelfAmnt);
        strCol[14] = parseNode(nodeCase,YNSCParser.PATH_SelfPay2);
        strCol[15] = parseNode(nodeCase,YNSCParser.PATH_GetLimit);
        strCol[16] = parseNode(nodeCase,YNSCParser.PATH_MidAmnt);
        strCol[17] = parseNode(nodeCase,YNSCParser.PATH_PlanFee);
        strCol[18] = parseNode(nodeCase,YNSCParser.PATH_HighAmnt2);
        strCol[19] = parseNode(nodeCase,YNSCParser.PATH_SupInHosFee);
        strCol[20] = parseNode(nodeCase,YNSCParser.PATH_HighAmnt1);
        strCol[21] = parseNode(nodeCase,YNSCParser.PATH_OfficialSubsidy);
        strCol[22] = parseNode(nodeCase,YNSCParser.PATH_DiseaseName);
        strCol[23] = parseNode(nodeCase,YNSCParser.PATH_DiseaseCode);
        strCol[24] = FormatDate(parseNode(nodeCase,YNSCParser.PATH_HospStartDate));
        strCol[25] = FormatDate(parseNode(nodeCase,YNSCParser.PATH_HospEndDate));
        strCol[26] = parseNode(nodeCase,YNSCParser.PATH_OriginFlag);
        strCol[27] = ErrMessage;
        System.out.println("ErrMessage:"+ErrMessage);
        FalseListTable.add(strCol);
        return true;
    }

    private String FormatDate(String aDate){
        String rDate = "";
        rDate = aDate;
        if(aDate.length()<8){
            int days = 0;
            try{
                days = Integer.parseInt(aDate)-2;
            }catch(Exception ex){
                return "";
            }
            rDate = PubFun.calDate("1900-1-1",days,"D",null);
        }
        return rDate;
    }
}
