package com.sinosoft.lis.llcase;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;

/**
 * <p>
 * Title: LIS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Yangming
 * @version 6.0
 */
public class SagentMail {

	/** 收件人地址 */
	private String toAddress;
	/** 抄送人地址 */
	private String ccAddress;
	/** 暗抄人地址 */
	private String bccAddress;
	/** 传送对象 */
	private Transport mTransport;
	/** 邮件发送服务类 */
	private MailSender mMailSender;
	/** 邮件内容 */
	private String mBody;
	/** 邮件标题 */
	private String mSubject;
	/** 邮件体 */
	private BodyPart messageBodyPart;
	/** 发送邮件 */
	private Multipart multipart = new MimeMultipart();
	/** 添加附件 */
	private String mFile;

	public CErrors mErrors = new CErrors();

	/**
	 * 发送邮件构造器
	 * @return 
	 */
	public void SendMail() {
		try {
			//查询邮件发送者相关信息
			String tSQLMailSenderSql = "select code,codename from ldcode where codetype = 'lpsagentmailsender' ";
			SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSenderSql);
			System.out.println("邮件发送-邮件密码："+tSSRS.GetText(1, 2));
		    System.out.println("邮件发送-邮件账户："+tSSRS.GetText(1, 1));
			mMailSender = new MailSender(tSSRS.GetText(1, 1), tSSRS.GetText(1, 2),"picchealth");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * 发送邮件
	 * 
	 * @return boolean
	 */
	public boolean send(String body, String subject) {
		this.mBody = StrTool.unicodeToGBK(body);
		this.mSubject = subject;
		
		Calendar calendar = Calendar.getInstance();//可以对每个时间域单独修改
		int tYear = calendar.get(Calendar.YEAR);
		int tMonth = calendar.get(Calendar.MONTH);
		
		try {
			//标题，内容，附件(多附件可以用|分隔，末尾|可加可不加)
			mMailSender.setSendInf(tYear+"年"+tMonth+"月份"+mSubject,mBody,getFile());
	
			//开始发送邮件
			mMailSender.setToAddress(getToAddress(),getCcAddress(),getBccAddress());
			
			if (!mMailSender.sendMail()) {
				System.out.println(mMailSender.getErrorMessage());
			}
		}catch(Exception ex){
			this.mErrors.addOneError(new CError("邮件发送类SagentMail出错",
					"SagentMail", "send"));
			ex.toString();
			ex.printStackTrace();
		}
		
		return true;
	}

	/**
	 * addFile
	 * 
	 * @return boolean
	 */
	private boolean addFile() {
		if (this.mFile != null) {

			String[] multFile = mFile.split(";");
			if (multFile != null && multFile.length > 0) {
				for (int i = 0; i < multFile.length; i++) {
					messageBodyPart = new MimeBodyPart();

					DataSource source = new FileDataSource(multFile[i]);
					try {
						messageBodyPart.setDataHandler(new DataHandler(source));
						
						System.out.println(multFile[i].lastIndexOf("\\"));
						
						int len=multFile[i].lastIndexOf("\\");
						if(len==-1)
						{
							messageBodyPart.setFileName(multFile[i].substring(multFile[i]
							                           								.lastIndexOf("/") + 1));
						}
						else
						{
							messageBodyPart.setFileName(multFile[i].substring(multFile[i]
							                           								.lastIndexOf("\\") + 1));
						}
						

						multipart.addBodyPart(messageBodyPart);
					} catch (MessagingException ex) {
						ex.printStackTrace();
						buildError("addFile", "添加附件错误！" + ex.getMessage());
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * addMailBody
	 * 
	 * @return boolean
	 */
	private boolean addMailBody() {
		messageBodyPart = new MimeBodyPart();
		try {
			messageBodyPart.setContent(this.mBody, "text/html;charset=gbk");
			multipart.addBodyPart(messageBodyPart);
		} catch (MessagingException ex2) {
			ex2.printStackTrace();
			buildError("creatMessage", "添加邮件内容错误！" + ex2.getMessage());
			return false;
		}
		return true;
	}



	/**
	 * 获得收件人地址
	 * 
	 * @return 
	 */
	public String getToAddress() {
		return toAddress;
	}

	/**
	 * 获得抄送人地址
	 * 
	 * @return 
	 */
	public String getCcAddress() {
		return ccAddress;
	}
	
	/**
	 * 获得暗抄人地址
	 * 
	 * @return 
	 */
	public String getBccAddress() {
		return bccAddress;
	}

	/**
	 * 添加暗抄人地址
	 * 
	 * @param bccAddress
	 *            
	 */
	public void setBccAddress(String bccAddress) {
		this.bccAddress = bccAddress;
	}

	/**
	 * 添加收件人地址
	 * 
	 * @param toAddress
	 *            
	 */
	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	/**
	 * 添加抄送人地址
	 * 
	 * @param ccAddress
	 *           
	 */
	public void setCcAddress(String ccAddress) {
		this.ccAddress = ccAddress;
	}

	public void setFile(String mFile) {
		this.mFile = StrTool.unicodeToGBK(mFile);
	}


	/**
	 * 出错处理
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "SendMail";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
		System.out.println("程序报错：" + cError.errorMessage);
	}

	public static void main(String[] args) {
		SagentMail tSendMail = new SagentMail();
		String address = "houyadong14320@sinosoft.com.cn;hydsxsy@126.com";
		try {
			tSendMail.SendMail();
			tSendMail.setToAddress(address);
			//tSendMail.setToAddress(address);
			//tSendMail.setToAddress("jingbaobaoccc@hotmail.com");
			tSendMail.setCcAddress("hydsxsy@126.com");
			//tSendMail.setBccAddress("zhangjialong@sinosoft.com.cn");
		} catch (Exception ex) {
			System.out.println("434::::"+ex.toString());
		}
		try {
			tSendMail.setFile("");
			tSendMail.send("<b>中文</b>", "测试");
		} catch (Exception ex) {
			System.out.println("440::::"+ex.toString());
			ex.printStackTrace();
		}
	}

	public String getFile() {
		return mFile;
	}
}

