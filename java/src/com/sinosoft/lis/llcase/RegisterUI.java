package com.sinosoft.lis.llcase;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:承保暂交费功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class RegisterUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    public RegisterUI()
    {}


    public static void main(String[] args)
    {
//    LLRegisterSchema tLLRegisterSchema   = new LLRegisterSchema();
//    tLLRegisterSchema.setRgtNo("00100020030510000015");
//    LLCaseSchema tLLCaseSchema = new LLCaseSchema();
//    tLLCaseSchema.setCustomerName("刘欣");
//    GlobalInput mG = new GlobalInput();
//    mG.ManageCom="86";
//    mG.Operator="001";
//  // 准备传输数据 VData
//    VData tVData = new VData();
//    tVData.addElement(tLLRegisterSchema);
//    tVData.addElement(tLLCaseSchema);
//    tVData.addElement(mG);
//  // 数据传输
//  RegisterUI tRegisterUI   = new RegisterUI();
//  tRegisterUI.submitData(tVData,"QUERY||MAIN");
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo("00100020030510000108");
        tLLRegisterDB.getInfo();
        tLLRegisterSchema = tLLRegisterDB.getSchema();
        String tt = tLLRegisterSchema.encode();

    }


    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        RegisterBL tRegisterBL = new RegisterBL();

        System.out.println("---UI 1111111111BEGIN---");
        System.out.println("RgiserUI.java");
        if (tRegisterBL.submitData(cInputData, mOperate) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tRegisterBL.mErrors);
            mResult.clear();
            return false;
        }
        else
            mResult = tRegisterBL.getResult();
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }


    /*  public static void main(String[] args)
      {
        RegisterUI tRegisterUI=new RegisterUI();
        VData tVData=new VData();
        LLRegisterSchema tLLRegisterSchema=new LLRegisterSchema();
        LLRegisterSet tLLRegisterSet=new LLRegisterSet();
        LLCaseSchema tLLCaseSchema=new LLCaseSchema();
        LLCaseSet tLLCaseSet=new LLCaseSet();
        LLRgtAffixSchema tLLRgtAffixSchema=new LLRgtAffixSchema();
        LLRgtAffixSet tLLRgtAffixSet=new LLRgtAffixSet();
        tLLRegisterSchema.setContNo("1");
        tLLRegisterSet.add(tLLRegisterSchema);
        tLLCaseSchema.setCustomerNo("1");
        tLLCaseSet.add(tLLCaseSchema);
        tLLRgtAffixSchema.setAffixSerialNo("1");
        tLLRgtAffixSchema.setAffixCode("1");
        tLLRgtAffixSet.add(tLLRgtAffixSchema);
        tVData.add(tLLRegisterSet);
        tVData.add(tLLCaseSet);
        tVData.add(tLLRgtAffixSet);
        tRegisterUI.submitData(tVData,"INSERT");
      }
     */
}
