package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: ClientRegisterBL </p>
 * <p>Description: ClientRegisterBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author:Xx
 * @CreateDate：2005-02-18 13:58:55
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class BatchClaimConfBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCaseSet mLLCaseSet = new LLCaseSet();
    private double ApplyAmnt = 0.0;
    private String mBatchCaseNo = "";

    public BatchClaimConfBL() {
    }

    public static void main(String[] args) {
        VData tVData = new VData();
        BatchClaimConfBL tBatchClaimConfBL = new BatchClaimConfBL();

        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86940000";
        mGlobalInput.ComCode = "86940000";
        mGlobalInput.Operator = "xuxin";
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        tLLRegisterSchema.setRgtNo("P9400060904000001");
        tLLRegisterSchema.setAppAmnt(180000);

        tVData.add(mGlobalInput);
        tVData.add(tLLRegisterSchema);
        tBatchClaimConfBL.submitData(tVData, "CONF|MAIN");
        tVData.clear();
        tVData = tBatchClaimConfBL.getResult();
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ClientRegisterBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ClientRegisterBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        System.out.println("Start BatchClaimConf Submit...");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "BatchClaimConfBL";
            tError.functionName = "dealdata";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("--Star GetInputData--");
        this.mLLRegisterSchema.setSchema((LLRegisterSchema) cInputData.
                             getObjectByObjectName("LLRegisterSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mLLCaseSet.set((LLCaseSet)cInputData.
                             getObjectByObjectName("LLCaseSet", 0));
//        mBatchCaseNo = mLLRegisterSchema.getRemark();
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("---Star dealData---mOperate---" + mOperate);
        if(mOperate.equals("CONF||MAIN")){
            ApplyAmnt = mLLRegisterSchema.getAppAmnt();
            if (!checkData())
                return false;
            String ljsgetSql = "select * from ljsget where otherno in " +
                               "(select caseno from llcase where rgtno='" +
                               mLLRegisterSchema.getRgtNo() + "')";
            LJSGetDB tLJSGetDB = new LJSGetDB();
            LJSGetSet tLJSGetSet = new LJSGetSet();
            tLJSGetSet = tLJSGetDB.executeQuery(ljsgetSql);
            String ljsgetclaimSql =
                    "select * from ljsgetclaim where otherno in " +
                    "(select caseno from llcase where rgtno='" +
                    mLLRegisterSchema.getRgtNo() + "')";
            String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
            String tGetNoticeNo = PubFun1.CreateMaxNo("GetNoticeNo", tLimit);
            LJSGetClaimDB tLJSGetClaimDB = new LJSGetClaimDB();
            LJSGetClaimSet tLJSGetClaimSet = new LJSGetClaimSet();
            tLJSGetClaimSet = tLJSGetClaimDB.executeQuery(ljsgetclaimSql);
            LJSGetSchema tLJSGetSchema = new LJSGetSchema();
            LJSGetClaimSchema tLJSGetClaimSchema = new LJSGetClaimSchema();
            tLJSGetSchema = tLJSGetSet.get(1);
            tLJSGetSchema.setSumGetMoney(ApplyAmnt);
            tLJSGetSchema.setOtherNo(mLLRegisterSchema.getRgtNo());
            tLJSGetSchema.setGetNoticeNo(tGetNoticeNo);
            tLJSGetClaimSchema = tLJSGetClaimSet.get(1);
            tLJSGetClaimSchema.setPay(ApplyAmnt);
            tLJSGetClaimSchema.setOtherNo(mLLRegisterSchema.getRgtNo());
            tLJSGetClaimSchema.setGetNoticeNo(tGetNoticeNo);
//            tLJSGetClaimSchema.setContNo("");
            tLJSGetClaimSchema.setPolNo(tLJSGetClaimSchema.getGrpPolNo());
            String delgetsql = "delete from ljsget where otherno='"
                               + mLLRegisterSchema.getRgtNo() + "'";
            String delgetclaimsql = "delete from ljsgetclaim where otherno='"
                                    + mLLRegisterSchema.getRgtNo() + "'";
            mLLRegisterSchema.setAppAmnt(ApplyAmnt);
            mLLRegisterSchema.setRgtState("03");
            map.put(delgetsql, "DELETE");
            map.put(delgetclaimsql, "DELETE");
            map.put(tLJSGetClaimSchema, "INSERT");
            map.put(tLJSGetSchema, "INSERT");
            map.put(mLLRegisterSchema, "UPDATE");
        }
        if (this.mOperate.equals("DELETE||MAIN")) {
        	if (mLLRegisterSchema == null) {
				CError tError = new CError();
				tError.moduleName = "BatchClaimConfBL";
				tError.functionName = "dealData";
				tError.errorMessage = "团体立案信息获取失败!";
				this.mErrors.addOneError(tError);
				return false;
			}

			if (mLLRegisterSchema.getRgtNo() == null
					|| "".equals(mLLRegisterSchema.getRgtNo())) {
				CError tError = new CError();
				tError.moduleName = "BatchClaimConfBL";
				tError.functionName = "dealData";
				tError.errorMessage = "团体立案信息获取失败!";
				this.mErrors.addOneError(tError);
				return false;
			}

			LLRegisterDB tLLRegisterDB = new LLRegisterDB();
			tLLRegisterDB.setRgtNo(mLLRegisterSchema.getRgtNo());

			if (!tLLRegisterDB.getInfo()) {
				CError tError = new CError();
				tError.moduleName = "BatchClaimConfBL";
				tError.functionName = "dealData";
				tError.errorMessage = "团体立案信息查询失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
			
			if (!"01".equals(tLLRegisterDB.getRgtState()) && !"07".equals(tLLRegisterDB.getRgtState())) {
				CError tError = new CError();
				tError.moduleName = "BatchClaimConfBL";
				tError.functionName = "dealData";
				tError.errorMessage = "批次已经受理完毕，不能修改申请信息，请进行撤件处理。";
				this.mErrors.addOneError(tError);
				return false;
			}
        	        	
            for (int i = 1; i <= mLLCaseSet.size(); i++) {
            	if (mLLCaseSet.get(i).getCaseNo()==null || "".equals(mLLCaseSet.get(i).getCaseNo())) {
            		CError tError = new CError();
    				tError.moduleName = "BatchClaimConfBL";
    				tError.functionName = "dealData";
    				tError.errorMessage = "案件信息获取失败";
    				this.mErrors.addOneError(tError);
    				return false;
            	}
            	
            	String tCaseNo = mLLCaseSet.get(i).getCaseNo();
            	
            	LLCaseDB tLLCaseDB = new LLCaseDB();
            	tLLCaseDB.setCaseNo(tCaseNo);
            	if (!tLLCaseDB.getInfo()) {
            		CError tError = new CError();
    				tError.moduleName = "BatchClaimConfBL";
    				tError.functionName = "dealData";
    				tError.errorMessage = "案件信息查询失败";
    				this.mErrors.addOneError(tError);
    				return false;
            	}
            	
            	if (!"01".equals(tLLCaseDB.getRgtState())
						&& !"02".equals(tLLCaseDB.getRgtState())
						&& !"03".equals(tLLCaseDB.getRgtState())
						&& !"13".equals(tLLCaseDB.getRgtState())) {
					CError tError = new CError();
					tError.moduleName = "BatchClaimConfBL";
					tError.functionName = "dealData";
					tError.errorMessage = "案件受理信息不可进行修改。";
					this.mErrors.addOneError(tError);
					return false;
				}
            	
            	String DelCaseCure = "delete from llcasecure where caseno ='"
                                     + tCaseNo + "'";
                String DelFeeMain = "delete from llfeemain where caseno ='"
                                    + tCaseNo + "'";
                String DelSecReceipt =
                        "delete from llsecurityreceipt where caseno ='"
                        + tCaseNo + "'";
                String Delsubreport =
                        "delete from llsubreport where subrptno in ("
                        +
                        "select subrptno from llcaserela where caseno ='"
                        + tCaseNo + "')";
                String Delcasereceipt = "delete from llcasereceipt where caseno ='"
                	                  + tCaseNo + "'";
                String DelCaseRela = "delete from llcaserela where caseno ='"
                                     + tCaseNo + "'";
                String DelClaimReason =
                        "delete from llappclaimreason where caseno ='"
                        + tCaseNo + "'";
                String DeltoClaimDuty =
                        "delete from lltoclaimduty where caseno ='"
                        + tCaseNo + "'";
                String DelCasePolicy =
                        "delete from llcasepolicy where caseno ='"
                        + tCaseNo + "'";
                String DelCaseOpTime =
                        "delete from llcaseoptime where caseno ='"
                        + tCaseNo + "'";
                String DelClaimPolicy =
                        "delete from llclaimpolicy where caseno ='"
                        + tCaseNo + "'";
                String DelClaim = "delete from llclaim where caseno ='"
                                  + tCaseNo + "'";
                String DelClaimDetail =
                        "delete from llclaimdetail where caseno ='"
                        + tCaseNo + "'";
                String DelInsureAccTrace =
                    "delete from lcinsureacctrace where otherno ='"
                    + tCaseNo + "'";
                String DelInsureAccFeeTrace =
                    "delete from lcinsureaccfeetrace where otherno ='"
                    + tCaseNo + "'";
                String DelJSGet = "delete from ljsget where otherno ='"
                                  + tCaseNo + "'";
                String DelJSGetClaim =
                        "delete from ljsgetclaim where otherno ='"
                        + tCaseNo + "'";
                String DelCase = "delete from llcase where caseno ='"
                                 + tCaseNo + "'";

                map.put(DelCaseCure, "DELETE");
                map.put(DelFeeMain, "DELETE");
                map.put(DelSecReceipt, "DELETE");
                map.put(Delcasereceipt, "DELETE");
                map.put(Delsubreport, "DELETE");
                map.put(DelCaseRela, "DELETE");
                map.put(DelClaimReason, "DELETE");
                map.put(DeltoClaimDuty, "DELETE");
                map.put(DelCasePolicy, "DELETE");
//                map.put(DelCaseOpTime, "DELETE");
                map.put(DelClaimPolicy, "DELETE");
                map.put(DelClaimDetail, "DELETE");
                map.put(DelClaim, "DELETE");
                map.put(DelInsureAccTrace, "DELETE");
                map.put(DelInsureAccFeeTrace, "DELETE");
                map.put(DelJSGet, "DELETE");
                map.put(DelJSGetClaim, "DELETE");
                map.put(DelCase, "DELETE");
            }
        }
        return true;
    }

    /**
     * 处理前检查数据是否合理
     * @return boolean
     */
    private boolean checkData(){
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        if (!tLLRegisterDB.getInfo()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SimpleClaimBL";
            tError.functionName = "checkData";
            tError.errorMessage = "团体立案信息查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
        String declineflag = "" + mLLRegisterSchema.getDeclineFlag();
        if (declineflag.equals("1")) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SimpleClaimBL";
            tError.functionName = "checkData";
            tError.errorMessage = "该团体申请已撤件，不能再录入个人客户!";
            this.mErrors.addOneError(tError);
            return false;
        }
//        if ("03".equals(mLLRegisterSchema.getRgtState())) {
//            // @@错误处理
//            CError tError = new CError();
//            tError.moduleName = "SimpleClaimBL";
//            tError.functionName = "checkData";
//            tError.errorMessage = "团体申请下所有个人案件已经结案完毕，" +
//                                  "不能再录入个人客户!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
        if ("04".equals(mLLRegisterSchema.getRgtState())) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SimpleClaimBL";
            tError.functionName = "checkData";
            tError.errorMessage = "团体申请下所有个人案件已经给付确认，" +
                                  "不能再录入个人客户!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String sql = "select * from llcase where rgtno='"+mLLRegisterSchema.getRgtNo()+"' and rgtstate not in ('09','14')";
        System.out.println(sql);
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0) {
            CError tError = new CError();
            tError.moduleName = "BatchClaimConfBL";
            tError.functionName = "getBaseData";
            tError.errorMessage = "团体立案下个人案件尚未全部结案!" + "团体批次号：" + mLLRegisterSchema.getRgtNo();
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 自动理算，生成赔案信息表。
     * @return boolean
     */
    private boolean calClaim(String aCaseNo){
        ClaimCalAutoBL aClaimCalAutoBL = new ClaimCalAutoBL();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setCaseNo(aCaseNo);
        VData aVData = new VData();
        aVData.addElement(mGlobalInput);
        aVData.addElement(tLLCaseSchema);
        if(!aClaimCalAutoBL.submitData(aVData, "Cal")){
            CError.buildErr(this, "理算失败");
             return false;
        }
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            mInputData.add(map);
            mResult.clear();
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDDrugBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
