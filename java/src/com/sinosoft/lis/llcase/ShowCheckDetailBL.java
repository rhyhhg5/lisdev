package com.sinosoft.lis.llcase;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.io.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class ShowCheckDetailBL
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData ;
  private VData mResult = new VData();
  private String mOperate;
  public ShowCheckDetailBL(){}
  private String tRgtNo = "";
  private String tClmNo = "";//赔案号码
  private String tClmUWNo = "";//改善程序，使其包括序号
  private String tUpClmUWer = "";//记录签批人员
  private String tMakeDate = "";
  private String tMakeTime = "";
  //private String tClmUWer = "";

  private LLClaimUWDetailSet pol_LLClaimUWDetailSet = new LLClaimUWDetailSet();
  private LLReportSchema re_LLReportSchema = new LLReportSchema();

  LLReportSchema lastLLReportSchema = new LLReportSchema();
  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;
    if(!getInputData(cInputData))
      return false;
    pol_LLClaimUWDetailSet.set(query_PolInfo());//pol_LLClaimUWDetailSet是最后的保单的审核结果
    re_LLReportSchema.setSchema(query_info(tUpClmUWer));
    mResult.clear();
    mResult.add(pol_LLClaimUWDetailSet);
    mResult.add(re_LLReportSchema);
    return true;
  }
  private boolean getInputData(VData cInputData)
  {
    if(mOperate.equals("QUERY"))
    {
      tRgtNo = (String)cInputData.get(0);
      //System.out.println("立案号码是"+tRgtNo);
      tClmUWNo = (String)cInputData.get(1);
//      System.out.println("序号是"+tClmUWNo);
      tUpClmUWer = (String)cInputData.get(2);
    }
    return true;
  }
  //查询保单核赔的详细信息
  private LLClaimUWDetailSet query_PolInfo()
  {
    LLClaimUWDetailSet endLLClaimUWDetailSet = new LLClaimUWDetailSet();//保单核赔履历
    LLClaimUWDetailSet mLLClaimUWDetailSet = new LLClaimUWDetailSet();//保单核赔履历
    //根据立案号码查询赔案号码
    LLClaimSet tLLClaimSet = new LLClaimSet();
    LLClaimSchema tLLClaimSchema = new LLClaimSchema();
    LLClaimDB tLLClaimDB = new LLClaimDB();
    String t_sql = "select ClmNo from llclaim where rgtno = '"+tRgtNo+"'";
    //System.out.println("查询的语句是"+t_sql);
    tLLClaimSet.set(tLLClaimDB.executeQuery(t_sql));
    if(tLLClaimSet.size()>0)
    {
      //System.out.println("共有"+tLLClaimSet.size());
      tLLClaimSchema.setSchema(tLLClaimSet.get(1));
      tClmNo = tLLClaimSchema.getClmNo();
      //System.out.println("案件号码是"+tClmNo);
      if(!(tClmNo==null||tClmNo.equals("")))
      {
        LLClaimUWMDetailSet tLLClaimUWMDetailSet = new LLClaimUWMDetailSet();//案件核赔履历
        LLClaimUWMDetailDB tLLClaimUWMDetailDB = new LLClaimUWMDetailDB();//案件核赔履历
        tLLClaimUWMDetailDB.setClmNo(tClmNo);//案件核赔履历
        tLLClaimUWMDetailDB.setRgtNo(tRgtNo);//案件核赔履历
        tLLClaimUWMDetailDB.setClmUWNo(tClmUWNo);//案件核赔履历
        tLLClaimUWMDetailSet.set(tLLClaimUWMDetailDB.query());//案件核赔履历
        if(tLLClaimUWMDetailSet.size()>0)//案件核赔履历
        {
          LLClaimUnderwriteSet tLLClaimUnderwriteSet = new LLClaimUnderwriteSet();

          LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();

          LLClaimUWMDetailSchema tLLClaimUWMDetailSchema = new LLClaimUWMDetailSchema();//案件核赔履历
          tLLClaimUWMDetailSchema.setSchema(tLLClaimUWMDetailSet.get(1));
          tMakeDate = tLLClaimUWMDetailSchema.getMakeDate();
          tMakeTime = tLLClaimUWMDetailSchema.getMakeTime();

          //查询出唯一的保单号码
          String a_sql = "select distinct(PolNo) from LLClaimUWDetail where ClmNo = '"+tClmNo
                       +"' and MakeDate <='"+tMakeDate+"' and ClmUWer ='"+tLLClaimUWMDetailSchema.getClmUWer()
                       +"' and MakeTime <='"+tMakeTime+"' order by PolNo";
//          System.out.println("保单履历表的查询语句是"+a_sql);
          ExeSQL exesql = new ExeSQL();
          SSRS ssrs = new SSRS();
          ssrs = exesql.execSQL(a_sql);
          if(ssrs.getMaxRow()>0)
          {
            for(int j = 1; j<= ssrs.getMaxRow();j++)
            {
              String tPolNo = ssrs.GetText(j,1);
//              System.out.println("当ｊ＝　"+j+"是的保单号码是"+tPolNo);
              //取相同的保单号码的最后一条记录
              String sec_sql = "select * from LLClaimUWDetail where ClmNo = '"+tClmNo
                             +"' and Makedate <='"+tMakeDate+"' and Maketime <='"+tMakeTime
                             +"' and ClmUWer = '"+tLLClaimUWMDetailSchema.getClmUWer()
                             +"' and PolNo = '"+tPolNo+"'"
                             +" order by makedate,maketime desc ";
              LLClaimUWDetailSet sec_LLClaimUWDetailSet = new LLClaimUWDetailSet();
              LLClaimUWDetailSchema sec_LLClaimUWDetailSchema = new LLClaimUWDetailSchema();
//              System.out.println("执行的ｓｅｃ＿ｓｑｌ是"+sec_sql);
               LLClaimUWDetailDB tLLClaimUWDetailDB = new LLClaimUWDetailDB();
              sec_LLClaimUWDetailSet.set(tLLClaimUWDetailDB.executeQuery(sec_sql));
              if(sec_LLClaimUWDetailSet.size()>0)
              {
                sec_LLClaimUWDetailSchema.setSchema(sec_LLClaimUWDetailSet.get(1));
                //查询其他信息
                //查询审核结论的语句，用ContNo承载核赔结论；用MngCom承载ClmUWGrade(核赔级别)
                String Decision_sql = "select * from LLClaimUnderwrite where polno = '"
                                    +sec_LLClaimUWDetailSchema.getPolNo()+"' and clmno = '"
                                    +sec_LLClaimUWDetailSchema.getClmNo()+"'";
//                System.out.println("查询的语句是"+Decision_sql);
                tLLClaimUnderwriteSet.clear();//清除原来的结论
                 LLClaimUnderwriteDB tLLClaimUnderwriteDB = new LLClaimUnderwriteDB();
                tLLClaimUnderwriteSet.set(tLLClaimUnderwriteDB.executeQuery(Decision_sql));
                if(tLLClaimUnderwriteSet.size()>0)
                {
                  sec_LLClaimUWDetailSchema.setContNo(tLLClaimUnderwriteSet.get(1).getClmDecision());
                  sec_LLClaimUWDetailSchema.setMngCom(tLLClaimUnderwriteSet.get(1).getClmUWGrade());
                }
                String Insure_sql = "select * from LLClaimPolicy where polno = '"
                                  +sec_LLClaimUWDetailSchema.getPolNo()+"' and clmno = '"
                                  +sec_LLClaimUWDetailSchema.getClmNo()+"' ";
                tLLClaimPolicySet.clear();
                LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
                tLLClaimPolicySet.set(tLLClaimPolicyDB.executeQuery(Insure_sql));
                if(tLLClaimPolicySet.size()>0)
                {
                  sec_LLClaimUWDetailSchema.setRemark(tLLClaimPolicySet.get(1).getInsuredName());
                  sec_LLClaimUWDetailSchema.setGetDutyKind(tLLClaimPolicySet.get(1).getGetDutyKind());
                }
//                System.out.println("保单号码是"+sec_LLClaimUWDetailSchema.getPolNo());
//                System.out.println("被保人姓名"+sec_LLClaimUWDetailSchema.getRemark());
//                System.out.println("给付责任类型:"+sec_LLClaimUWDetailSchema.getGetDutyKind());
//                System.out.println("金额是"+sec_LLClaimUWDetailSchema.getRealPay());
//                System.out.println("核赔意见是"+sec_LLClaimUWDetailSchema.getContNo());
//                System.out.println("核赔级别是"+sec_LLClaimUWDetailSchema.getMngCom());
                endLLClaimUWDetailSet.add(sec_LLClaimUWDetailSchema);
              }
            }
          }
        }
      }
    }
    return endLLClaimUWDetailSet;//保单核赔履历
  }


  private String get_ClmUWNo(String t_ClmUWNo)
  {
    String m_ClmUWNo="";
    int i_ClmUWNo = Integer.parseInt(t_ClmUWNo);
    i_ClmUWNo = i_ClmUWNo+1;
    m_ClmUWNo = String.valueOf(i_ClmUWNo);
    //System.out.println("转换后的号码是"+m_ClmUWNo);
    return m_ClmUWNo;
  }
  private String get_info(String t_AppActionType)
  {
    String info = "没有录入";
    if(t_AppActionType.equals("1"))
    {
      info = "确认";
    }
    if(t_AppActionType.equals("2"))
    {
      info = "退回";
    }
    if(t_AppActionType.equals("3"))
    {
      info = "上报";
    }
    //System.out.println("意见是"+info);
    return info;
  }

  /*******************************************************************************
   * Name       :query_info()
   * Author     :LiuYansong
   * CreateDate :2003-12-22
   * Function   :返回理赔中的审核的明细信息到主界面上
   * Remark     :其中用LLReport来承载返回的结果
   *            :AccidentCourse--------审核意见
   *            ：RptNo----------------ClmUWer(审核人员)
   *            :RptObjNo--------------ClmUWNo
   *            :RptorName-------------AppClmUWer（签批人员）
   *            :AccidentReason--------签批意见
   *            :MakeDate--------------MakeDate
   *            :MakeTime--------------MakeTime
   *            :Remark----------------AppActionType
   */
  //不能够是循环。
  //得到案件核赔结论的函数
  //返回审核意见和签批意见
  public  LLReportSchema query_info(String UpClmUWer)
  {
    LLReportSchema endLLReportSchema = new LLReportSchema();//存放最后的结果
    LLClaimUWMDetailSet mLLClaimUWMDetailSet = new LLClaimUWMDetailSet();
    LLClaimUWMDetailDB mLLClaimUWMDetailDB = new LLClaimUWMDetailDB();
    //查询对应于该立案号码共有多少条分公司的审核记录
    String m_sql = "select * from LLClaimUWMDetail where RgtNo = '"+tRgtNo
                 +"' and ClmNo='"+tClmNo+"' and ClmUWNo = '"+tClmUWNo
                 +"' order by makedate,maketime ";
//    System.out.println("查询审核的语句是"+m_sql);
    mLLClaimUWMDetailSet.set(mLLClaimUWMDetailDB.executeQuery(m_sql));
    //主键查询
    if(mLLClaimUWMDetailSet.size()>0)
    {
      LLClaimUWMDetailSet tLLClaimUWMDetailSet = new LLClaimUWMDetailSet();//装载签批的信息
//      System.out.println("案件履历表中共有"+mLLClaimUWMDetailSet.size());
      LLClaimUWMDetailSchema tLLClaimUWMDetailSchema = new LLClaimUWMDetailSchema();
      tLLClaimUWMDetailSchema.setSchema(mLLClaimUWMDetailSet.get(1));
      String t_AppClmUWer = tLLClaimUWMDetailSchema.getAppClmUWer();
      endLLReportSchema.setRptNo(tLLClaimUWMDetailSchema.getClmUWer());
      endLLReportSchema.setAccidentCourse(tLLClaimUWMDetailSchema.getRemark());
//      System.out.println("审核人员是"+endLLReportSchema.getRptNo());
//      System.out.println("审核人员的意见是"+endLLReportSchema.getAccidentCourse());
      if(!(tLLClaimUWMDetailSchema.getAppClmUWer()==null
           ||tLLClaimUWMDetailSchema.getAppClmUWer().equals("")))
      {
        String ClmUWNo = get_ClmUWNo(tLLClaimUWMDetailSchema.getClmUWNo());//得到下个序号
        String t_sql = "select * from LLClaimUWMDetail where ClmUWer = '"
                     +t_AppClmUWer+"' and RgtNo = '"+tRgtNo
                     +"' and ClmUWNo='"+ClmUWNo+"' and ClmNo = '"+tClmNo
                     +"' and AppPhase = '1' order by makedate,maketime";
//        System.out.println("查询符合签批的是"+t_sql);
        //唯一查询
        LLClaimUWMDetailDB tLLClaimUWMDetailDB = new LLClaimUWMDetailDB();
        tLLClaimUWMDetailSet.set(tLLClaimUWMDetailDB.executeQuery(t_sql));
        if(tLLClaimUWMDetailSet.size()>0)
        {
          LLClaimUWMDetailSchema aLLClaimUWMDetailSchema = new LLClaimUWMDetailSchema();
          aLLClaimUWMDetailSchema.setSchema(tLLClaimUWMDetailSet.get(1));
          String tAppActionType = aLLClaimUWMDetailSchema.getAppActionType();
          //查询的就是此条记录
          if(aLLClaimUWMDetailSchema.getClmUWer().equals(UpClmUWer))
          {
            endLLReportSchema.setRptorName(aLLClaimUWMDetailSchema.getClmUWer());
//            System.out.println("签批人是"+endLLReportSchema.getRptorName());
            endLLReportSchema.setAccidentReason(aLLClaimUWMDetailSchema.getRemark());
//            System.out.println("签批人的意见是"+endLLReportSchema.getAccidentReason());
          }
          //查询的不是这条记录
          else
          {
            if(!(tAppActionType==null||tAppActionType.equals("")))
            {
              if(tAppActionType.equals("3"))
              {
                String b_ClmUWNo = get_ClmUWNo(aLLClaimUWMDetailSchema.getClmUWNo());
                String b_sql = "select * from LLClaimUWMDetail where ClmUWer = '"
                             +aLLClaimUWMDetailSchema.getAppClmUWer()+"' and RgtNo = '"+tRgtNo
                             +"'and ClmUWNo= '"+b_ClmUWNo+"' and ClmNo = '"+tClmNo+"' " ;
//                System.out.println("查询的语句是"+b_sql);
                LLClaimUWMDetailDB bLLClaimUWMDetailDB = new LLClaimUWMDetailDB();
                LLClaimUWMDetailSet bLLClaimUWMDetailSet = new LLClaimUWMDetailSet();
                bLLClaimUWMDetailSet.set(bLLClaimUWMDetailDB.executeQuery(b_sql));
//                System.out.println("查询的条数是"+bLLClaimUWMDetailSet.size());
                if(bLLClaimUWMDetailSet.size()>0)
                {
                  LLClaimUWMDetailSchema bLLClaimUWMDetailSchema = new LLClaimUWMDetailSchema();
                  bLLClaimUWMDetailSchema.setSchema(bLLClaimUWMDetailSet.get(1));
                  endLLReportSchema.setRptorName(bLLClaimUWMDetailSchema.getClmUWer());
//                  System.out.println("签批人是"+endLLReportSchema.getRptorName());
                  endLLReportSchema.setAccidentReason(bLLClaimUWMDetailSchema.getRemark());
//                  System.out.println("签批意见是"+endLLReportSchema.getAccidentReason());
                }
              }
            }
          }
        }
      }
    }
    return endLLReportSchema;
  }
  public VData getResult()
  {
    return mResult;
  }

  public static void main(String[] args)
  {
    String aRgtNo  = "86000020030510000011";
    String aClmUWNo = "7";
    String aUpClmUWer = "000012";
    VData tVData = new VData();
    tVData.addElement(aRgtNo);
    tVData.addElement(aClmUWNo);
    tVData.addElement(aUpClmUWer);
    ShowCheckDetailBL aShowCheckDetailBL = new ShowCheckDetailBL();
    //aShowCheckDetailBL.query_info(aUpClmUWer);
    aShowCheckDetailBL.submitData(tVData,"QUERY");
  }
}