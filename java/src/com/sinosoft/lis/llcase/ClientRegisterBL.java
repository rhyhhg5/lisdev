package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: ClientRegisterBL </p>
 * <p>Description: ClientRegisterBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author:Xx
 * @CreateDate：2005-02-18 13:58:55
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Date;
import com.sinosoft.lis.llcase.LLAffixBL;

public class ClientRegisterBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();


    /** 全局数据 */
    private GlobalInput mG = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private String strHandler;
    private String SimpleCase="";
    private boolean mEasyFlag=false;
    private String mManageCom = "";
    private FDate fDate = new FDate();
    /**社保校验标记*/
    private boolean mSocialFlag = false;

    /** 业务处理相关变量 */
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLAppClaimReasonSet mLLAppClaimReasonSet = new LLAppClaimReasonSet();
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private LLCaseExtSchema mLLCaseExtSchema = new LLCaseExtSchema();//2807领款人和被保人关
    private LLCaseSchema aLLCaseSchema = new LLCaseSchema();
    private LLSubReportSet tLLSubReportSet = new LLSubReportSet();
    private LLFeeMainSet mLLFeeMainSet = new LLFeeMainSet();
    private LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
    private LLAffixSet mLLAffixSet = new LLAffixSet();

    public ClientRegisterBL() {
    }

    public static void main(String[] args) {
        String tLimit = PubFun.getNoLimit("86110000");
        VData tno = new VData();
       tno.add(0,"1");
       tno.add(1,"P1100070329000001");
       tno.add(2,"STORE");
       String CaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit,tno);
       if(CaseNo.length()<17){
           System.out.println("预留号超限，不能在该批次下继续增加出险人！");
       }
        String strOperate = "INSERT||MAIN";
        LLAppClaimReasonSet tLLAppClaimReasonSet = new LLAppClaimReasonSet();
        LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
        LLAppealSchema tLLAppealSchema = new LLAppealSchema();
        LLSubReportSet tLLSubReporSet = new LLSubReportSet();
        VData tVData = new VData();
        ClientRegisterBL tClientRegisterBL = new ClientRegisterBL();

        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86110000";
        mGlobalInput.ComCode = "86110000";
        mGlobalInput.Operator = "bcm005";

        tLLCaseSchema.setRgtType("1");
        tLLRegisterSchema.setRgtNo("P1100051010000001");
        tLLRegisterSchema.setRgtState("01"); //案件状态
        tLLRegisterSchema.setRgtObj("2"); //号码类型 0总单 1分单 2个单 3客户
        tLLRegisterSchema.setRgtObjNo("000000132");
        tLLRegisterSchema.setRgtClass("0");
        tLLRegisterSchema.setCustomerNo("000011010");
        tLLRegisterSchema.setRgtType("1"); //受理方式
        tLLRegisterSchema.setRgtantName("任旭东"); //申请人姓名
        tLLRegisterSchema.setRelation("00"); //与被保险人关系
        tLLRegisterSchema.setRgtantAddress(""); //申请人地址
        tLLRegisterSchema.setRgtantPhone("RgtantPhone"); //申请人电话
        tLLRegisterSchema.setPostCode("100010"); //邮编
        tLLRegisterSchema.setAppAmnt(""); //预估申请金额
        tLLRegisterSchema.setGetMode("1");
        tLLRegisterSchema.setBankCode("");
        tLLRegisterSchema.setBankAccNo("");
        tLLRegisterSchema.setAccName("");
        tLLRegisterSchema.setReturnMode("1"); //回执发送方式
        tLLRegisterSchema.setIDType("0"); //申请人证件类型
        tLLRegisterSchema.setIDNo("110103741225091"); //申请人证件号码
        tLLRegisterSchema.setRgtObj("1"); //个人客户

        LLAppClaimReasonSchema tLLAppClaimReasonSchema = new
                LLAppClaimReasonSchema();
        tLLAppClaimReasonSchema.setReasonCode("01"); //原因代码
        tLLAppClaimReasonSchema.setCustomerNo("000007322");
        tLLAppClaimReasonSchema.setReasonType("0");
        tLLAppClaimReasonSet.add(tLLAppClaimReasonSchema);

        LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
        tLLSubReportSchema.setSubRptNo("A1100050707000011");
        tLLSubReportSchema.setCustomerNo("000011010");
        tLLSubReportSchema.setCustomerName("费碧君");
        tLLSubReportSchema.setAccDate("2005-05-01");
        tLLSubReportSchema.setAccDesc("干燥综合征、桥本甲状腺炎");
        tLLSubReportSchema.setAccPlace("北京同仁医院");
        tLLSubReportSchema.setInHospitalDate("2005-05-01");
        tLLSubReportSchema.setOutHospitalDate("2005-05-12");
        tLLSubReportSchema.setAccidentType("1");
        tLLSubReporSet.add(tLLSubReportSchema);
        tLLSubReportSchema = new LLSubReportSchema();
        tLLSubReportSchema.setSubRptNo("A1100050722000001");
        tLLSubReportSchema.setCustomerNo("000011010");
        tLLSubReportSchema.setCustomerName("费碧君");
        tLLSubReportSchema.setAccDate("2005-05-01");
        tLLSubReportSchema.setAccDesc("门诊");
        tLLSubReportSchema.setAccPlace("北京同仁医院");
        tLLSubReportSchema.setInHospitalDate("");
        tLLSubReportSchema.setOutHospitalDate("");
        tLLSubReportSchema.setAccidentType("1");
        tLLSubReporSet.add(tLLSubReportSchema);

        /**************************************************/
        tLLCaseSchema.setRgtState("13"); //案件状态
        tLLCaseSchema.setCustomerName("费碧君");
        tLLCaseSchema.setCustomerNo("000011010");
        tLLCaseSchema.setPostalAddress("");
        tLLCaseSchema.setPhone("");
        tLLCaseSchema.setDeathDate("");
        tLLCaseSchema.setCustBirthday("1941-03-13");
        tLLCaseSchema.setCustomerSex("1");
        tLLCaseSchema.setIDType("0");
        tLLCaseSchema.setIDNo("110105194103131822");
        tLLCaseSchema.setCustState("04");
        tLLCaseSchema.setDeathDate("2005-5-1");
        tLLCaseOpTimeSchema.setRgtState("01");
        tLLCaseOpTimeSchema.setStartDate("2005-9-15");
        tLLCaseOpTimeSchema.setStartTime("20:30:30");
        TransferData SC = new TransferData();
        SC.setNameAndValue("SimpleCase", "02");
        SC.setNameAndValue("RiskCode", "1605");
        tVData.add(tLLAppClaimReasonSet);
        tVData.add(tLLCaseSchema);
        tVData.add(tLLRegisterSchema);
        tVData.add(tLLAppealSchema);
        tVData.add(tLLSubReporSet);
        tVData.add(tLLCaseOpTimeSchema);
        tVData.add(SC);

        tVData.add(mGlobalInput);
        tClientRegisterBL.submitData(tVData, strOperate);
        tVData.clear();
        tVData = tClientRegisterBL.getResult();
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * @cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        if (!checkInputData())
            return false;
        if (!dealHandler())
            return false;
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError.buildErr(this, "数据处理失败ClientRegisterBL-->dealData!");
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start ClientRegisterBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                CError.buildErr(this, "数据提交失败!");
                return false;
            }
            System.out.println("End ClientRegisterBL Submit...");

            //门诊大额自动选择申请材料
            if (!mOperate.equals("CASE||UPDATE")&&mEasyFlag) {
                if (!dealAffix()){
                    return false;
                }
            }
        }
        
        //作废续期催收
        if (this.mOperate.equals("INSERT||MAIN")) {
        	if (!indiJSCancel()) {
        		return false;
        	}
        }
        
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("--Star GetInputData--");
        mLLRegisterSchema.setSchema((LLRegisterSchema) cInputData.
                                    getObjectByObjectName("LLRegisterSchema", 0));
        mLLCaseSchema.setSchema((LLCaseSchema) cInputData.
                                getObjectByObjectName("LLCaseSchema", 0));
        LLCaseExtSchema tLLCaseExtSchema = new LLCaseExtSchema();//2807领款人和被保人关
        tLLCaseExtSchema = (LLCaseExtSchema)cInputData.getObjectByObjectName("LLCaseExtSchema", 0);
        if(tLLCaseExtSchema!=null){
        	this.mLLCaseExtSchema.setSchema(tLLCaseExtSchema);//2807领款人和被保人关
        }
        mLLAppClaimReasonSet = (LLAppClaimReasonSet) cInputData.
                               getObjectByObjectName("LLAppClaimReasonSet", 0);
        tLLSubReportSet = (LLSubReportSet) cInputData.getObjectByObjectName(
                "LLSubReportSet", 0);
        mLLCaseOpTimeSchema = (LLCaseOpTimeSchema) cInputData.
                              getObjectByObjectName("LLCaseOpTimeSchema", 0);
        this.mG.setSchema((GlobalInput) cInputData.
                          getObjectByObjectName("GlobalInput", 0));
        mManageCom = mG.ManageCom;
        if (mG.ManageCom.trim().length() == 2)
            mG.ManageCom = mG.ManageCom.trim() + "000000";
        if (mG.ManageCom.trim().length() == 4)
            mG.ManageCom = mG.ManageCom.trim() + "0000";
        System.out.println("mG.ManageCom======="+mG.ManageCom);
        System.out.println(mLLCaseSchema.getCaseNo());
        if (mLLCaseSchema.getCaseNo() != null &&
            !mLLCaseSchema.getCaseNo().trim().equals("")) {
            System.out.println(mLLCaseSchema.getCaseNo());
            LLCaseDB tLLCaseDB = new LLCaseDB();
            tLLCaseDB.setCaseNo(mLLCaseSchema.getCaseNo());
            if (tLLCaseDB.getInfo()) {
                mOperate = "CASE||UPDATE";
                aLLCaseSchema.setSchema(tLLCaseDB.getSchema());
            }
        }
        
        //add by Houyd 社保受理申请（团体）社保人员操作支持
        System.out.println("团体批次号为："+this.mLLRegisterSchema.getRgtNo());
  	  	String sql = "";
  	  	if("UPDATE||MAIN".equals(this.mOperate))
  	  	{
  		  sql = "select CHECKGRPCONT((select RgtObjNo from LLRegister where 1=1 and rgtno ='"+mLLRegisterSchema.getRgtNo()+"' )) from dual where 1=1  "; 		  
  	  	}
  	  
  	  	ExeSQL tExeSQL = new ExeSQL();
  	  	String tResult = tExeSQL.getOneValue(sql);
  	  	if(tResult!=null&&tResult.equals("Y"))
  	  	{	
  	  		mSocialFlag = true;
  	  		String checkSql = "Select '1' From LLSocialClaimUser where UserCode = '"+this.mG.Operator+"' and StateFlag = '1' and HandleFlag ='0'";
  	  		String tOper_Result = tExeSQL.getOneValue(checkSql);
  	  		if(tOper_Result!=null&&"1".equals(tOper_Result))
  	  		{
			  
  	  		}else{
  			  
  			  CError tError = new CError();
  		      tError.moduleName = "GrpRegisterBL";
  		      tError.functionName = "checkData";
  		      tError.errorMessage = "您不具有社保案件理赔操作权限！";
  		      this.mErrors.addOneError(tError);
  			  return false;  
  		  }		  
  	  }else{
  		  LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
  		  tLLClaimUserDB.setUserCode(mG.Operator);
  		  if (!tLLClaimUserDB.getInfo()) {
  			  CError tError = new CError();
  			  tError.moduleName = "ClientRegisterBL";
  			  tError.functionName = "getInputData";
  			  tError.errorMessage = "操作用户非理赔用户，请重新登录。";
  			  this.mErrors.addOneError(tError);
  			  return false;
  		  }
  	  }
        
        
        //申请原因：门诊大额
        for(int i=1; i<=mLLAppClaimReasonSet.size(); i++){
            if (mLLAppClaimReasonSet.get(i).getReasonCode().equals("10")){
                mEasyFlag=true;
            }
        }
        if (mEasyFlag){
            String tSQL = "SELECT b.affixtypecode,b.AffixCode,b.AffixName "
                        + "FROM LLMAppReasonAffix a,LLMAffix b WHERE "
                        + "a.affixcode=b.affixcode AND reasoncode='10' ORDER BY b.affixtypecode,b.AffixCode WITH UR";
            ExeSQL tExeSQl = new ExeSQL();
            SSRS tSSRS = new SSRS();
            tSSRS = tExeSQl.execSQL(tSQL);
            if (tSSRS.getMaxRow()>0){
                for (int i=1; i<=tSSRS.getMaxRow(); i++){
                    LLAffixSchema mLLAffixSchema = new LLAffixSchema();
                    mLLAffixSchema.setReasonCode("10");
                    mLLAffixSchema.setAffixType(tSSRS.GetText(i, 1));
                    mLLAffixSchema.setAffixCode(tSSRS.GetText(i, 2));
                    mLLAffixSchema.setAffixName(tSSRS.GetText(i, 3));
                    mLLAffixSchema.setCount(1);
                    mLLAffixSchema.setShortCount(0);
                    mLLAffixSchema.setSupplyDate(PubFun.getCurrentDate());
                    mLLAffixSet.add(mLLAffixSchema);
                }
            }else{
                CError.buildErr(this, "未配置门诊大额申请材料");
                return false;
            }
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("---Star dealData---mOperate---" + mOperate);
        String CaseNo = "";
        String RgtNo = "";
        int CustAge = calAge();
        String ReceiptFlag = getReceiptFlag();
        LLCaseCommon tLLCaseCommon = new LLCaseCommon();
        String tInputer = "";
        
        if(mSocialFlag){//社保受理到检录，均为一个人
        	tInputer = mG.Operator;
        }else{	
            tInputer = tLLCaseCommon.chooseInputer(mManageCom,
                             mLLCaseSchema.getCaseProp());
            if(tInputer==null||tInputer.equals(""))
                tInputer = strHandler;
            if((tInputer==null||tInputer.equals(""))&&mLLCaseSchema.getCaseProp().equals("06")){
                CError.buildErr(this, "录入人查询失败！");
                return false;
            }            
        }
        
        if (this.mOperate.equals("INSERT||MAIN")) {//个案申请
            System.out.println("---Star Insert Operate ---");
            /*生成理赔号*/
            String tLimit = PubFun.getNoLimit(this.mG.ManageCom);
            CaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit);
            //团体申请才创建团体申请号,否则援用案件号码
            if (mLLRegisterSchema.getRgtClass().equals("0")) {
                RgtNo = CaseNo;
            } else {
                RgtNo = PubFun1.CreateMaxNo("RgtNo", tLimit);
            }
            System.out.println("理赔号" + CaseNo);
            System.out.println("申请号" + RgtNo);

            //立案申请
            mLLRegisterSchema.setRgtNo(RgtNo);
            mLLRegisterSchema.setRgtDate(PubFun.getCurrentDate());
            mLLRegisterSchema.setOperator(mG.Operator);
            mLLRegisterSchema.setHandler(strHandler);
            mLLRegisterSchema.setMngCom(mG.ManageCom);
            mLLRegisterSchema.setMakeDate(PubFun.getCurrentDate());
            mLLRegisterSchema.setMakeTime(PubFun.getCurrentTime());
            mLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
            mLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLLRegisterSchema, "INSERT");
            //立案分案
            System.out.println(mLLCaseSchema.getCaseProp());
            System.out.println(tInputer);

            if(SimpleCase.equals("01")){
                mLLCaseSchema.setClaimer("");
            }else if (SimpleCase.equals("03") ){
                mLLCaseSchema.setClaimer(mG.Operator);
            }else{
                mLLCaseSchema.setClaimer(tInputer);
            }
            System.out.println(mLLCaseSchema.getClaimer());
            mLLCaseSchema.setCustomerAge(CustAge);
            mLLCaseSchema.setReceiptFlag(ReceiptFlag);
            mLLCaseSchema.setRgtNo(RgtNo);
            mLLCaseSchema.setCaseNo(CaseNo);
            mLLCaseSchema.setHandler(strHandler);
            mLLCaseSchema.setDealer(strHandler);

            mLLCaseSchema.setRigister(mG.Operator);
            mLLCaseSchema.setOperator(mG.Operator);
            mLLCaseSchema.setMngCom(mG.ManageCom);
            mLLCaseSchema.setRgtDate(PubFun.getCurrentDate());
            mLLCaseSchema.setMakeDate(PubFun.getCurrentDate());
            mLLCaseSchema.setMakeTime(PubFun.getCurrentTime());
            mLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
            mLLCaseSchema.setModifyTime(PubFun.getCurrentTime());
            //2807领款人和被保人关**start**
            mLLCaseExtSchema.setCaseNo(CaseNo);
            mLLCaseExtSchema.setOperator(mG.Operator);
            mLLCaseExtSchema.setMngCom(mG.ManageCom);
            mLLCaseExtSchema.setMakeDate(PubFun.getCurrentDate());
            mLLCaseExtSchema.setMakeTime(PubFun.getCurrentTime());
            mLLCaseExtSchema.setModifyTime(PubFun.getCurrentTime());
            mLLCaseExtSchema.setModifyDate(PubFun.getCurrentDate());
            map.put(mLLCaseExtSchema, "INSERT");
            //2807领款人和被保人关**end**
            map.put(mLLCaseSchema, "INSERT");

            if (CaseNo!=null&&!"".equals(CaseNo)&&!"null".equals(CaseNo)) {
                mLLCaseOpTimeSchema.setCaseNo(CaseNo);
                mLLCaseOpTimeSchema.setOperator(mG.Operator);
                mLLCaseOpTimeSchema.setManageCom(mG.ManageCom);
                tLLCaseCommon = new LLCaseCommon();
                LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(
                        mLLCaseOpTimeSchema);
                map.put(tLLCaseOpTimeSchema, "INSERT");
            }
        }
        if (this.mOperate.equals("UPDATE||MAIN")) {//受理申请（团体）申请
            LLRegisterDB tLLRegisterDB = new LLRegisterDB();
            tLLRegisterDB.setRgtNo(mLLRegisterSchema.getRgtNo());
            if (!tLLRegisterDB.getInfo()) {
                // @@错误处理
                CError.buildErr(this, "团体立案信息查询失败！");
                return false;
            }
            mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
            String declineflag = "" + mLLRegisterSchema.getDeclineFlag();
            if (declineflag.equals("1")) {
                // @@错误处理
                CError.buildErr(this, "该团体申请已撤件，不能再录入个人客户！");
                return false;
            }
            if ("02".equals(mLLRegisterSchema.getRgtState())) {
                // @@错误处理
                CError.buildErr(this, "该团体申请已确认录入完毕所有出险人，不能再添加个人客户！");
                return false;
            }
            if ("03".equals(mLLRegisterSchema.getRgtState())) {
                // @@错误处理
                CError.buildErr(this, "团体申请下所有个人案件已经结案完毕，不能再录入个人客户！");
                return false;
            }
            if ("04".equals(mLLRegisterSchema.getRgtState())||
                "05".equals(mLLRegisterSchema.getRgtState())) {
                // @@错误处理
                CError.buildErr(this, "团体申请下所有个人案件已经给付确认，不能再录入个人客户！");
                return false;
            }

            LLCaseSet tLLCaseSet = new LLCaseSet();
            LLCaseDB tLLCaseDB = new LLCaseDB();
            tLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
            tLLCaseSet.set(tLLCaseDB.query());
            if (tLLCaseDB.mErrors.needDealError()) {
                CError.buildErr(this, "库表有误，批次下个人案件查询失败！");
                return false;
            }
            int realPeoples = tLLCaseSet.size();
            int appPeoples = mLLRegisterSchema.getAppPeoples();
            if (realPeoples >= appPeoples) {
                mLLRegisterSchema.setAppPeoples(realPeoples + 1);
                mLLRegisterSchema.setOperator(mG.Operator);
                mLLRegisterSchema.setMngCom(mG.ManageCom);
                mLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
                mLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());
                map.put(mLLRegisterSchema, "UPDATE");
            }

            //增加个人客户已经入库校验
//            cbs00011491取消该校验，允许一个批次一个人受理多次
//            tLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
//            tLLCaseDB.setCustomerNo(mLLCaseSchema.getCustomerNo());
//            tLLCaseSet.set(tLLCaseDB.query());
//            if (tLLCaseDB.mErrors.needDealError()) {
//                // @@错误处理
//                CError.buildErr(this, "库表有误，批次下个人案件查询失败！");
//                return false;
//            }
//            if (tLLCaseSet != null && tLLCaseSet.size() > 0) {
//                // @@错误处理
//                this.mErrors.copyAllErrors(tLLCaseDB.mErrors);
//                CError tError = new CError();
//                tError.moduleName = "GrpRegisterBL";
//                tError.functionName = "dealData";
//                String rgtstate = tLLCaseSet.get(1).getRgtState();
//                if (rgtstate.equals("01") || rgtstate.equals("02") ||
//                    rgtstate.equals("13"))
//                    tError.errorMessage = "团体申请下该客户已经录入!" +
//                                          "案件号：" + tLLCaseSet.get(1).getCaseNo() +
//                                          "当前处理人：" +
//                                          tLLCaseSet.get(1).getHandler() +
//                                          "请与之合并处理";
//                else
//                    tError.errorMessage = "团体申请下该客户已经录入!" +
//                                          "案件号：" + tLLCaseSet.get(1).getCaseNo() +
//                                          "案件状态：" +
//                                          tLLCaseSet.get(1).getRgtState() +
//                                          "请将该案件放入另一批次进行处理";
//
//                this.mErrors.addOneError(tError);
//                return false;
//            }

            /*生成理赔号*/
            System.out.println("---Star update data---");
            RgtNo = mLLRegisterSchema.getRgtNo();
            String tLimit = PubFun.getNoLimit(this.mG.ManageCom);
            System.out.println("管理机构代码是 : " + tLimit);
            VData tno = new VData();
           tno.add(0,"1");
           tno.add(1,RgtNo);
           tno.add(2,"STORE");
           CaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit,tno);
           if(CaseNo.length()<17){
//               CError.buildErr(this,"预留号超限，不能在该批次下继续增加出险人！");
               CaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit);
           }
            System.out.println("理赔号" + CaseNo);
            System.out.println("申请号" + RgtNo);
            
            if(mSocialFlag){//claimer为账单录入确认人员
            	mLLCaseSchema.setClaimer(tInputer);
            }else{
            	if(SimpleCase.equals("01")){
                    mLLCaseSchema.setClaimer("");
                }else{
                    mLLCaseSchema.setClaimer(tInputer);
                }
            }

            mLLCaseSchema.setCustomerAge(CustAge);
            mLLCaseSchema.setReceiptFlag(ReceiptFlag);
            mLLCaseSchema.setRgtNo(RgtNo);
            mLLCaseSchema.setCaseNo(CaseNo);
            mLLCaseSchema.setHandler(strHandler);
            mLLCaseSchema.setDealer(strHandler);
            mLLCaseSchema.setRigister(mG.Operator);
            mLLCaseSchema.setOperator(mG.Operator);
            mLLCaseSchema.setMngCom(mG.ManageCom);
            mLLCaseSchema.setRgtDate(PubFun.getCurrentDate());
            mLLCaseSchema.setMakeDate(PubFun.getCurrentDate());
            mLLCaseSchema.setMakeTime(PubFun.getCurrentTime());
            mLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
            mLLCaseSchema.setModifyTime(PubFun.getCurrentTime());
          //2807领款人和被保人关**start**
            mLLCaseExtSchema.setCaseNo(CaseNo);
            mLLCaseExtSchema.setOperator(mG.Operator);
            mLLCaseExtSchema.setMngCom(mG.ManageCom);
            mLLCaseExtSchema.setMakeDate(PubFun.getCurrentDate());
            mLLCaseExtSchema.setMakeTime(PubFun.getCurrentTime());
            mLLCaseExtSchema.setModifyTime(PubFun.getCurrentTime());
            mLLCaseExtSchema.setModifyDate(PubFun.getCurrentDate());
            map.put(mLLCaseExtSchema, "INSERT");
            System.out.println(mLLCaseExtSchema.getCaseNo());
            //2807领款人和被保人关**end**
            map.put(mLLCaseSchema, "INSERT");

            if (CaseNo!=null&&!"".equals(CaseNo)&&!"null".equals(CaseNo)) {
                mLLCaseOpTimeSchema.setCaseNo(CaseNo);
                mLLCaseOpTimeSchema.setOperator(mG.Operator);
                mLLCaseOpTimeSchema.setManageCom(mG.ManageCom);
                tLLCaseCommon = new LLCaseCommon();
                LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(
                        mLLCaseOpTimeSchema);
                map.put(tLLCaseOpTimeSchema, "INSERT");
            }
            String RiskCode = mLLCaseSchema.getRiskCode()+"";
            if (RiskCode.equals("1605")) {
                if (!dealTEXU())
                    return false;
            }
        }
        if (this.mOperate.equals("CASE||UPDATE")) {
            CaseNo = aLLCaseSchema.getCaseNo();
            RgtNo = aLLCaseSchema.getRgtNo();
            if (!aLLCaseSchema.getCustomerNo().equals(mLLCaseSchema.
                    getCustomerNo())) {
                CError.buildErr(this, "不允许更改案件下的客户信息");
                return false;
            }
            if (!aLLCaseSchema.getRigister().equals(mG.Operator)) {
                CError.buildErr(this, "您不是该案件的原受理人，不允许修改该案件信息");
                return false;
            }
            if (!aLLCaseSchema.getRgtState().equals("01")
                && !aLLCaseSchema.getRgtState().equals("02")
                && !aLLCaseSchema.getRgtState().equals("13")) {
                CError.buildErr(this, "该案件状态下不允许修改该案件信息");
                return false;
            }
            aLLCaseSchema.setCustomerAge(CustAge);
            mLLCaseSchema.setReceiptFlag(ReceiptFlag);
            aLLCaseSchema.setCustState(mLLCaseSchema.getCustState());
            aLLCaseSchema.setPostalAddress(mLLCaseSchema.getPostalAddress());
            aLLCaseSchema.setPhone(mLLCaseSchema.getPhone());
            aLLCaseSchema.setMobilePhone(mLLCaseSchema.getMobilePhone());
            aLLCaseSchema.setDeathDate(mLLCaseSchema.getDeathDate());
            aLLCaseSchema.setCustBirthday(mLLCaseSchema.getCustBirthday());
            aLLCaseSchema.setCustomerSex(mLLCaseSchema.getCustomerSex());
            aLLCaseSchema.setIDType(mLLCaseSchema.getIDType());
            aLLCaseSchema.setIDNo(mLLCaseSchema.getIDNo());
            aLLCaseSchema.setCaseGetMode(mLLCaseSchema.getCaseGetMode());
            aLLCaseSchema.setBankCode(mLLCaseSchema.getBankCode());
            aLLCaseSchema.setBankAccNo(mLLCaseSchema.getBankAccNo());
            aLLCaseSchema.setAccName(mLLCaseSchema.getAccName());
            aLLCaseSchema.setPrePaidFlag(mLLCaseSchema.getPrePaidFlag());
            aLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
            aLLCaseSchema.setModifyTime(PubFun.getCurrentTime());
          //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
            mLLCaseExtSchema.setCaseNo(CaseNo);
            mLLCaseExtSchema.setOperator(mG.Operator);
            mLLCaseExtSchema.setMngCom(mG.ManageCom);
            mLLCaseExtSchema.setMakeDate(PubFun.getCurrentDate());
            mLLCaseExtSchema.setMakeTime(PubFun.getCurrentTime());
            mLLCaseExtSchema.setModifyTime(PubFun.getCurrentTime());
            mLLCaseExtSchema.setModifyDate(PubFun.getCurrentDate());
//            map.put(mLLCaseExtSchema, "UPDATE");
            LLCaseExtDB tLLCaseExtDB=new LLCaseExtDB();
            tLLCaseExtDB.setCaseNo(CaseNo);
            if(!tLLCaseExtDB.getInfo()){
            	//不存在则添加
            	map.put(mLLCaseExtSchema, "INSERT");
            	
            }else{//存在则更新
            	map.put(mLLCaseExtSchema, "UPDATE");
            }
            System.out.println(mLLCaseExtSchema.getCaseNo());
          //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
            map.put(aLLCaseSchema, "UPDATE");
            if (aLLCaseSchema.getRgtNo().equals(aLLCaseSchema.getCaseNo())) {
                mLLRegisterSchema.setRgtNo(aLLCaseSchema.getCaseNo());
                mLLRegisterSchema.setRgtDate(aLLCaseSchema.getRgtDate());
                mLLRegisterSchema.setOperator(aLLCaseSchema.getOperator());
                mLLRegisterSchema.setHandler(aLLCaseSchema.getHandler());
                mLLRegisterSchema.setMngCom(aLLCaseSchema.getMngCom());
                mLLRegisterSchema.setMakeDate(aLLCaseSchema.getMakeDate());
                mLLRegisterSchema.setMakeTime(aLLCaseSchema.getMakeTime());
                mLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
                mLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());
                map.put(mLLRegisterSchema, "UPDATE");
            }
            String delsql = "delete from llappclaimreason where caseno='"
                            + CaseNo + "'";
            map.put(delsql, "DELETE");
            //将申请原因填充
        }

/*************************公用********************************/
        //将申请原因填充
        if (mLLAppClaimReasonSet != null && mLLAppClaimReasonSet.size() > 0) {
            for (int i = 1; i <= mLLAppClaimReasonSet.size(); i++) {
                mLLAppClaimReasonSet.get(i).setCaseNo(CaseNo);
                mLLAppClaimReasonSet.get(i).setRgtNo(RgtNo);
                mLLAppClaimReasonSet.get(i).setMakeDate(PubFun.getCurrentDate());
                mLLAppClaimReasonSet.get(i).setMakeTime(PubFun.getCurrentTime());
                mLLAppClaimReasonSet.get(i).setModifyDate(PubFun.getCurrentDate());
                mLLAppClaimReasonSet.get(i).setModifyTime(PubFun.getCurrentTime());
                mLLAppClaimReasonSet.get(i).setOperator(mG.Operator);
                mLLAppClaimReasonSet.get(i).setMngCom(mG.ManageCom);
            }
            map.put(mLLAppClaimReasonSet, "INSERT");
        }

        //该方法公用
        String tLimit = PubFun.getNoLimit(this.mG.ManageCom);
        //事件处理
        LLCaseRelaSet tLLCaseRelaSet = new LLCaseRelaSet();
        LLSubReportSet saveLLSubReportSet = new LLSubReportSet();
        if (tLLSubReportSet != null) {
            System.out.println("事件处理:");
            if(tLLSubReportSet.size()==0)//说明没有关联事件 by zzh 20101008
            {
            	CError.buildErr(this, "请关联至少一个事件！");
                return false;
            }
            for (int i = 1; i <= tLLSubReportSet.size(); i++) {
                LLSubReportSchema tLLSubReportSchema = tLLSubReportSet.get(i);
                if(tLLSubReportSchema.getAccDate()==null||tLLSubReportSchema.getAccDate().equals(""))
                {
                	CError.buildErr(this, "事件发生日期为空！");
                    return false;
                }	
                //如果事件没有，则创建事件关联
                if ("".equals(StrTool.cTrim(tLLSubReportSchema.getSubRptNo()))) {
                    String SubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);
                    tLLSubReportSchema.setSubRptNo(SubRptNo);
                    tLLSubReportSchema.setMakeDate(PubFun.getCurrentDate());
                    tLLSubReportSchema.setMakeTime(PubFun.getCurrentTime());
                    tLLSubReportSchema.setOperator(this.mG.Operator);
                    tLLSubReportSchema.setMngCom(this.mG.ManageCom);
                    tLLSubReportSchema.setModifyDate(PubFun.getCurrentDate());
                    tLLSubReportSchema.setModifyTime(PubFun.getCurrentTime());
                    saveLLSubReportSet.add(tLLSubReportSchema);
                } else {
                    String tsql = "select * from llcaserela where subrptno='"
                                  + tLLSubReportSchema.getSubRptNo() +
                                  "' and caseno <>'"
                                  + this.mLLCaseSchema.getCaseNo() + "'";
                    System.out.println(tsql);
                    LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
                    LLCaseRelaSet aLLCaseRelaSet = new LLCaseRelaSet();
                    aLLCaseRelaSet = tLLCaseRelaDB.executeQuery(tsql);
                    if (aLLCaseRelaSet.size() > 0) {
                        System.out.println("事件" +
                                           tLLSubReportSchema.getSubRptNo()
                                           + "关联到了别的案件上，不允许修改其信息");
                    } else {
                        LLSubReportDB tLLSubReportDB = new LLSubReportDB();
                        tLLSubReportDB.setSubRptNo(tLLSubReportSchema.
                                getSubRptNo());
                        if (tLLSubReportDB.getInfo()) {
                            tLLSubReportSchema.setMakeDate(tLLSubReportDB.
                                    getMakeDate());
                            tLLSubReportSchema.setMakeTime(tLLSubReportDB.
                                    getMakeTime());
                            tLLSubReportSchema.setOperator(tLLSubReportDB.
                                    getOperator());
                            tLLSubReportSchema.setMngCom(tLLSubReportDB.
                                    getMngCom());
                        } else {
                            String SubRptNo = PubFun1.CreateMaxNo("SUBRPTNO",
                                    tLimit);
                            tLLSubReportSchema.setSubRptNo(SubRptNo);
                            tLLSubReportSchema.setMakeDate(PubFun.getCurrentDate());
                            tLLSubReportSchema.setMakeTime(PubFun.getCurrentTime());
                            tLLSubReportSchema.setOperator(this.mG.Operator);
                            tLLSubReportSchema.setMngCom(this.mG.ManageCom);
                        }
                        tLLSubReportSchema.setModifyDate(PubFun.getCurrentDate());
                        tLLSubReportSchema.setModifyTime(PubFun.getCurrentTime());
                        saveLLSubReportSet.add(tLLSubReportSchema);
                    }
                }
                LLCaseRelaSchema tLLCaseRelaSchema = new LLCaseRelaSchema();
                String caserela = PubFun1.CreateMaxNo("CASERELANO", tLimit);
                System.out.println("caserela:" + caserela);
                tLLCaseRelaSchema.setCaseRelaNo(caserela);
                tLLCaseRelaSchema.setCaseNo(this.mLLCaseSchema.getCaseNo());
                tLLCaseRelaSchema.setSubRptNo(tLLSubReportSchema.getSubRptNo());
                tLLCaseRelaSet.add(tLLCaseRelaSchema);
            }
            //删除关联
            map.put("delete from llcaserela where caseno='" +
                    this.mLLCaseSchema.getCaseNo() + "'", "DELETE");
            map.put(tLLCaseRelaSet, "INSERT");
            //事件增加
            System.out.println("保存事件数：" + saveLLSubReportSet.size());
            if (saveLLSubReportSet.size() > 0) {
                map.put(saveLLSubReportSet, "DELETE&INSERT");
            }
        }
        //处理保单状态
        dealPolicyState();
        if (this.mOperate.equals("DELETE||MAIN")) {
            map.put(mLLRegisterSchema, "DELETE");
        }
        return true;
    }

    /**
     * 案件分配
     * add by Houyd 社保受理申请（团体）进入时，采用社保的案件分配规则
     * @return boolean
     */
    private boolean dealHandler() {
        LLCaseCommon tLLCaseCommon = new LLCaseCommon();
        String strMngCom = mManageCom;//此时的机构，为当前登录机构
        SimpleCase="";
        
        if(mSocialFlag){//社保案件的话，选择案件处理方式无效
        	strHandler = tLLCaseCommon.SocialSecurityChooseUser(mG.ManageCom);
        	if (strHandler == null || strHandler.equals("")) {
                CError tError = new CError();
                tError.moduleName = "ClientRegisterBL";
                tError.functionName = "dealHandler";
                tError.errorMessage = "机构"+mG.ManageCom+"及机构"+mG.ManageCom.substring(0, 4)+"下，均没有符合条件的社保审批人!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }else{
        	String tcaseprop = mLLCaseSchema.getCaseProp()+"";
            System.out.println("案件属性"+tcaseprop);
            if(tcaseprop.equals("06")){
                SimpleCase = "01";
            } else if("07".equals(tcaseprop) || "09".equals(tcaseprop)){
                SimpleCase = "03";
            } else if("11".equals(tcaseprop) ){
                SimpleCase = "04";
            }else {
                SimpleCase = "02";
            }
            
            System.out.println("insuredNo:"+mLLCaseSchema.getCustomerNo());

            if ("03".equals(SimpleCase)){
                strHandler = mG.Operator;
            }else{
                strHandler = tLLCaseCommon.ChooseAssessor(strMngCom,mLLCaseSchema.getCustomerNo(), SimpleCase);
            	//strHandler = tLLCaseCommon.ChooseUserNew(strMngCom,mLLCaseSchema.getCustomerNo(), SimpleCase,"",mLLCaseSchema.getCaseFlag());
            }
            if (strHandler == null || strHandler.equals("")) {
                CError tError = new CError();
                tError.moduleName = "ClientRegisterBL";
                tError.functionName = "dealHandler";
                tError.errorMessage = "机构" + strMngCom + "没有符合条件的理赔人!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        
        return true;
    }

    /**
     * 计算年龄
     * @return int
     */
    private int calAge() {
        Date Birthday = fDate.getDate(mLLCaseSchema.getCustBirthday());
        String strNow = PubFun.getCurrentDate();
        Date aNow = fDate.getDate(strNow);
        int CustomerAge = PubFun.calInterval(Birthday, aNow, "Y");
        System.out.println("年龄计算函数。。。"+CustomerAge);
        return CustomerAge;
    }

    /**
     * 处理保单状态
     * @return boolean
     */
    private boolean dealPolicyState() {
            //死亡
            LCPolDB tLCPolDB = new LCPolDB();
            String tInsuredNo = ""+mLLCaseSchema.getCustomerNo();
            if(tInsuredNo.equals("")||tInsuredNo.equals("null")){
                CError.buildErr(this, "查询客户信息失败");
                return false;
            }
            tLCPolDB.setInsuredNo(tInsuredNo);
            LCPolSet tLCPolSet = tLCPolDB.query();
            int y = tLCPolSet.size();
            System.out.println("该客户以投保人或被保险人存在的共有" + y + "张保单;");

            LDPersonDB tLDPersonDB = new LDPersonDB();
            tLDPersonDB.setCustomerNo(mLLCaseSchema.getCustomerNo());
            if(!tLDPersonDB.getInfo()){
                CError.buildErr(this, "查询客户信息失败");
                return false;
            }
            if (mLLCaseSchema.getDeathDate() != null||tLDPersonDB.getDeathDate()!=null) {
            for (int t = 1; t <= y; t++) {
                //修改该保单状态为终止(死亡报案终止).
                String opolstate = "" + tLCPolSet.get(t).getPolState();
                String npolstate = "";
                if (opolstate.equals("null") || opolstate.equals("")) {
                    npolstate = "0401";
                } else {
                    if(opolstate.substring(0,2).equals("04")){
                       npolstate = "0401"+opolstate.substring(4);
                   }else{
                       if (opolstate.length() < 4) {
                           npolstate = "0401" + opolstate;
                       } else {
                           npolstate = "0401" + opolstate.substring(0, 4);
                       }
                   }
                }
                tLCPolSet.get(t).setPolState(npolstate);
            }
            LDPersonSchema tLDPersonSchema = tLDPersonDB.getSchema();
            tLDPersonSchema.setDeathDate(mLLCaseSchema.getDeathDate());
            //保单挂起保存
            //map.put(tLCPolSet, "UPDATE");
            map.put(tLDPersonSchema, "UPDATE");
        }else{
            for (int t = 1; t <= y; t++) {
                //理赔立案.
                String opolstate = "" + tLCPolSet.get(t).getPolState();
               String npolstate = "";
               if (opolstate.equals("null") || opolstate.equals("")) {
                   npolstate = "0400";
               } else {
                   if(opolstate.substring(0,2).equals("04")){
                       npolstate = "0400"+opolstate.substring(4);
                   }else{
                       if (opolstate.length() < 4) {
                           npolstate = "0400" + opolstate;
                       } else {
                           npolstate = "0400" + opolstate.substring(0, 4);
                       }
                   }
               }
               tLCPolSet.get(t).setPolState(npolstate);
            }
            //map.put(tLCPolSet, "UPDATE");
        }
        return true;
    }

    /**
     * 处理特需医疗受理
     * 必要的话，录入申报金额，进行提前提示
     * @return boolean
     */
    private boolean dealTEXU(){
        LLFeeMainDB tLLFeeMainDB = new LLFeeMainDB();
        tLLFeeMainDB.setCustomerNo(mLLCaseSchema.getCustomerNo());
        LLFeeMainSet tLLFeeMainSet = new LLFeeMainSet();
        LLFeeMainSet updateFeeMainSet = new LLFeeMainSet();
        tLLFeeMainSet = tLLFeeMainDB.query();
        if (tLLFeeMainSet == null || tLLFeeMainSet.size() <= 0) {
            TransferData tipword = new TransferData();
            tipword.setNameAndValue("tipword",
                                    "该客户在本公司无既往理赔，请给出本次理赔申报金额。");
            System.out.println("该客户在本公司无既往理赔");
            this.mResult.add(tipword);
        } else {
            for (int k = 1; k <= tLLFeeMainSet.size(); k++) {
                LLFeeMainSchema tLLFeeMainSchema = tLLFeeMainSet.get(k);
                LLFeeMainSchema sLLFeeMainSchema = tLLFeeMainSet.get(k);
                String repayflag = "" + tLLFeeMainSchema.getRepayFlag();
                if (tLLFeeMainSchema.getRepayFlag() == null)
                    repayflag = "Y";
                if (!repayflag.equals("N")) {
                    LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
                    LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();
                    String cpolsql = "select * from llclaimpolicy where caserelano='"
                                     +tLLFeeMainSchema.getCaseRelaNo()+"'";
                    System.out.println(cpolsql);
                        tLLClaimPolicySet = tLLClaimPolicyDB.executeQuery(cpolsql);
                    double paymoney = 0.0;
                    if (tLLClaimPolicySet != null && tLLClaimPolicySet.size() > 0) {
                        for (int c = 1; c <= tLLClaimPolicySet.size();c++) {
                            paymoney += tLLClaimPolicySet.get(c).getRealPay();
                        }
                    }
                    tLLFeeMainSchema.setOtherOrganAmnt(paymoney);
                    double tabmoney = 0.0;
                    if (tLLFeeMainSchema.getFeeAtti().equals("1") ||
                        tLLFeeMainSchema.getFeeAtti().equals("2")) {
                        LLSecurityReceiptDB tLLSecurityReceiptDB = new
                                LLSecurityReceiptDB();
                        tLLSecurityReceiptDB.setFeeDetailNo(
                                tLLFeeMainSchema.getMainFeeNo());
                        if (tLLSecurityReceiptDB.getInfo()) {
                            double SP1 = tLLSecurityReceiptDB.getSelfPay1();
                            double SP2 = tLLSecurityReceiptDB.getSelfPay2();
                            double SAMNT = tLLSecurityReceiptDB.getSelfAmnt();
                            double Total = tLLSecurityReceiptDB.getApplyAmnt();
                            tabmoney += SP1 + SP2 + SAMNT;
                            double planmoney = Total - tabmoney;
                            tLLFeeMainSchema.setSumFee(Total);
                            tLLFeeMainSchema.setSocialPlanAmnt(planmoney);
                        }
                    } else {
                        LLCaseReceiptDB tLLCaseReceiptDB = new LLCaseReceiptDB();
                        LLCaseReceiptSet tLLCaseReceiptSet = new LLCaseReceiptSet();
                        tLLCaseReceiptDB.setMainFeeNo(tLLFeeMainSchema.getMainFeeNo());
                        tLLCaseReceiptSet = tLLCaseReceiptDB.query();
                        double splanmoney = 0.0;
                        System.out.println(tLLCaseReceiptSet.size());
                        for (int d = 1; d <= tLLCaseReceiptSet.size();d++) {
                            tabmoney += tLLCaseReceiptSet.get(d).getFee() -
                                    tLLCaseReceiptSet.get(d).getPreAmnt();
                            splanmoney += tLLCaseReceiptSet.get(d).getPreAmnt();
                        }
                        tLLFeeMainSchema.setSumFee(tabmoney);
                        tLLFeeMainSchema.setSocialPlanAmnt(splanmoney);
                    }
                    double remnant = tabmoney - paymoney;
                    tLLFeeMainSchema.setRemnant(remnant);
                    tLLFeeMainSchema.setOldCaseNo(tLLFeeMainSchema.getCaseNo());
                    tLLFeeMainSchema.setOldMainFeeNo(tLLFeeMainSchema.getMainFeeNo());
                    String tLimit1 = PubFun.getNoLimit("86");
                    String tMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO",tLimit1);
                    tLLFeeMainSchema.setMainFeeNo(tMainFeeNo);
                    tLLFeeMainSchema.setFeeAtti("3");
                    tLLFeeMainSchema.setCaseNo(mLLCaseSchema.getCaseNo());
                    tLLFeeMainSchema.setRgtNo(mLLRegisterSchema.getRgtNo());
                    tLLFeeMainSchema.setCaseRelaNo("");
                    if(tLLFeeMainSchema.getRemnant()>0){
                        mLLFeeMainSet.add(tLLFeeMainSchema);
                        sLLFeeMainSchema.setRepayFlag("N");
                        updateFeeMainSet.add(sLLFeeMainSchema);
                    }
                }
            }
            map.put(mLLFeeMainSet, "INSERT");
            map.put(updateFeeMainSet, "UPDATE");
        }
        return true;
    }

    /**
     * 取得是否需要录入账单标志，如果客户参保重疾或意外则可不录入账单
     * @return String
     */
    private String getReceiptFlag(){
        String ReceiptFlag = "0";
        String tsql = "select b.risktype1 from lcpol a,lmriskapp b "
                      +" where b.riskcode = a.riskcode and a.insuredno='"
                      +mLLCaseSchema.getCustomerNo()+"'";
        ExeSQL texesql = new ExeSQL();
        SSRS tss = new SSRS();
        tss = texesql.execSQL(tsql);
        for(int i=1;i<=tss.getMaxRow();i++){
            if(tss.GetText(i,1).equals("2")||tss.GetText(i,1).equals("5")){
                ReceiptFlag = "3";
                break;
            }
        }
        return ReceiptFlag;
    }

    private boolean dealAffix(){
        LLAffixSet tLLAffixSet = new LLAffixSet();
        for (int i=1; i<=mLLAffixSet.size();i++){
            LLAffixSchema tLLAffixSchema = new LLAffixSchema();
            tLLAffixSchema=mLLAffixSet.get(i);
            tLLAffixSchema.setCaseNo(mLLCaseSchema.getCaseNo());
            tLLAffixSchema.setRgtNo(mLLCaseSchema.getRgtNo());
            tLLAffixSet.add(tLLAffixSchema);
        }
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LoadFlag","1");
        VData tVData = new VData();
        tVData.add(tLLAffixSet);
        tVData.add(tTransferData);
        tVData.add(mG);
        LLAffixBL tLLAffixBL = new LLAffixBL();
        if (!tLLAffixBL.submitData(tVData,"INSERT||MAIN")){
            CError.buildErr(this, "申请材料选择失败，请手动选择！");
            return false;
        }
        return true;
    }
    /**
     * 准备提交数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLLRegisterSchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLLCaseSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDDrugBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setSchema(this.mLLRegisterSchema);
        //如果有需要处理的错误，则返回
        if (tLLRegisterDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ClientRegisterBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean checkInputData() {
    	String result = LLCaseCommon.checkPerson(mLLCaseSchema.getCustomerNo());
        if (!"".equals(result)){
            CError.buildErr(this, "工单号为"+result+"的客户现正进行保全操作或续保，请稍后受理");
            return false;
        }
        String sql = "select name,idno from lcinsured where insuredno='"+mLLCaseSchema.getCustomerNo()+"' group by name,idno union select name,idno from lbinsured where insuredno='"+mLLCaseSchema.getCustomerNo()+"' group by name,idno with ur";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);
        int count = tSSRS.getMaxRow();
        if(tSSRS.getMaxRow()<=0){
        	 CError.buildErr(this, "系统中不存在该用户！");
             return false;
        }else{
        	System.out.println(tSSRS.GetText(1, 1)+":"+StrTool.GBKToUnicode(mLLCaseSchema.getCustomerName())+":"+StrTool.GBKToUnicode(tSSRS.GetText(1, 2)));
        	for(int j=1;j<=count;j++){
        		if(tSSRS.GetText(j, 1).equals(StrTool.GBKToUnicode(mLLCaseSchema.getCustomerName()))){
        			return true;
        		}else{
        			if(j==count){
        				CError.buildErr(this, "请核实被保险人姓名是否正确！");
                        return false;
        			}else{
        				continue;
        			}
        		}
        	}
          for(int i=1;i<=count;i++){
        		if(tSSRS.GetText(i, 2).equals(mLLCaseSchema.getIDNo())){
        			return true;
        		}else{
        			if(i==count){
        				CError.buildErr(this, "请核实被保险人身份证号是否正确！");
                        return false;
        			}else{
        				continue;
        			}
        		}

        	}
        }
        return true;

    }
    
    private boolean indiJSCancel() {
    	String mInsuredNo =mLLCaseSchema.getCustomerNo();
    	TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("InsuredNo", mInsuredNo);
		tTransferData.setNameAndValue("ContNo", "");
		VData tVData = new VData();
		tVData.addElement(mG);
		tVData.addElement(tTransferData);
		
		LLIndiLJSCancelBL tLLIndiLJSCancelBL = new LLIndiLJSCancelBL();
		if (!tLLIndiLJSCancelBL.submitData(tVData, "")) {
			CError.buildErr(this, "撤销续期催收异常");
            return false;
		}
		
		String tIndiInfo = "";
		
		LJSPaySet tLJSPaySet = ((LJSPaySet) tLLIndiLJSCancelBL.getResult().getObjectByObjectName(
                "LJSPaySet", 0));
		for (int i=1;i<=tLJSPaySet.size();i++) {
			LJSPaySchema tLJSPaySchema = tLJSPaySet.get(i);
			tIndiInfo = "被保险人" + mLLCaseSchema.getCustomerNo()
				      + "，续期应收"+tLJSPaySchema.getGetNoticeNo()
			          + "："+tLJSPaySchema.getAccName()
			          + "。";
		}
		
		if (!"".equals(tIndiInfo)) {
			TransferData tTrans = new TransferData();
			tTrans.setNameAndValue("IndiInfo", tIndiInfo);
			mResult.addElement(tTrans);
		}
		
    	return true;    	
    }

    public VData getResult() {
        return this.mResult;
    }
}
