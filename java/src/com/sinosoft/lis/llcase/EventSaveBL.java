package com.sinosoft.lis.llcase;

/*
 * <p>ClassName: ClientRegisterBL </p>
 * <p>Description: ClientRegisterBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @author:YangMing
 * @CreateDate：2005-02-18 13:58:55
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Date;

public class EventSaveBL {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();
  private VData mResult = new VData();

  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  private MMap map = new MMap();

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();

  /** 数据操作字符串 */
  private String mOperate;
  private String strHandler;
  private FDate fDate = new FDate();

  /** 业务处理相关变量 */
  private LLSubReportSet tLLSubReportSet = new LLSubReportSet();
  private TransferData mTransferData = new TransferData();
  private String CaseNo;

  public EventSaveBL()
  {
  }

  public static void main(String[] args)
  {

    String strOperate = "INSERT||MAIN";
    System.out.println("==== strOperate == " + strOperate);

          LLSubReportSet tLLSubReporSet = new LLSubReportSet();
          VData tVData = new VData();

          EventSaveBL tEventSaveBL   = new EventSaveBL();
          CErrors tError = null;
          String tRela  = "";
          String FlagStr = "";
          String Content = "";
          String EventFlag="";	//事件申请标记 为1 有客户存在事件，为0不存在事件
          GlobalInput mGlobalInput = new GlobalInput();
          mGlobalInput.ManageCom = "86110000";
          mGlobalInput.ComCode = "86110000";
          mGlobalInput.Operator = "bcm005";

          System.out.println("<--submit LLRegisterSchema-->");


      LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
      tLLSubReportSchema.setSubRptNo("");
      tLLSubReportSchema.setCustomerNo("000000132");
      tLLSubReportSchema.setCustomerName("任旭东");
      tLLSubReportSchema.setAccDate("2005-05-23");
      tLLSubReportSchema.setAccDesc("事件");
      tLLSubReportSchema.setAccPlace("协和");
      tLLSubReportSchema.setInHospitalDate("2005-05-23");
      tLLSubReportSchema.setOutHospitalDate("2005-05-29");
      tLLSubReportSchema.setAccidentType("2");

      tLLSubReporSet.add(tLLSubReportSchema);
      TransferData CN = new TransferData();
                      CN.setNameAndValue("CaseNo","C1100051202000008");

          /**************************************************/
      tVData.add( tLLSubReporSet );
      tVData.add(CN);

      tVData.add(mGlobalInput);
      tEventSaveBL.submitData(tVData,strOperate);
      tVData.clear();
  }

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
    if(!checkData())
        return false;
    if (!dealData())
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "EventSaveBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据处理失败EventSaveBL-->dealData!";
      this.mErrors.addOneError(tError);
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
      System.out.println("Start EventSaveBL Submit...");
      PubSubmit tPubSubmit = new PubSubmit();
      if (!tPubSubmit.submitData(mInputData, mOperate))
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        CError tError = new CError();
        tError.moduleName = "EventSaveBL";
        tError.functionName = "EventSaveBL";
        tError.errorMessage = "数据提交失败!";

        this.mErrors.addOneError(tError);
        return false;
      }
      System.out.println("End EventSaveBL Submit...");

    mInputData = null;
    return true;
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
      //该方法工用
      String tLimit = PubFun.getNoLimit(this.mGlobalInput.ManageCom);
      //立案分案,即事件
      LLCaseRelaSet tLLCaseRelaSet = new LLCaseRelaSet();
      LLSubReportSet saveLLSubReportSet = new LLSubReportSet();
      if (tLLSubReportSet != null) {
          System.out.println("yyyyyyyyyyy:");
          for (int i = 1; i <= tLLSubReportSet.size(); i++) {
              LLSubReportSchema tLLSubReportSchema = tLLSubReportSet.get(i);
              //如果事件没有，则创建事件关联
              if ("".equals(StrTool.cTrim(tLLSubReportSchema.getSubRptNo()))) {

                  String SubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);
                  tLLSubReportSchema.setSubRptNo(SubRptNo);
                  tLLSubReportSchema.setMakeDate(PubFun.getCurrentDate());
                  tLLSubReportSchema.setMakeTime(PubFun.getCurrentTime());
                  tLLSubReportSchema.setOperator(this.mGlobalInput.Operator);
                  tLLSubReportSchema.setMngCom(this.mGlobalInput.ManageCom);
                  tLLSubReportSchema.setModifyDate(tLLSubReportSchema.
                          getMakeDate());
                  tLLSubReportSchema.setModifyTime(tLLSubReportSchema.
                          getMakeTime());
                  saveLLSubReportSet.add(tLLSubReportSchema);
              }
              LLCaseRelaSchema tLLCaseRelaSchema = new LLCaseRelaSchema();
              String caserela = PubFun1.CreateMaxNo("CASERELANO", tLimit);
              System.out.println("caserela:" + caserela);
              tLLCaseRelaSchema.setCaseRelaNo(caserela);
              tLLCaseRelaSchema.setCaseNo(CaseNo);
              tLLCaseRelaSchema.setSubRptNo(tLLSubReportSchema.getSubRptNo());
              tLLCaseRelaSet.add(tLLCaseRelaSchema);
          }
          //删除关联
          map.put("delete from llcaserela where caseno='" + CaseNo + "'",
                  "DELETE");
          map.put(tLLCaseRelaSet, "INSERT");
//事件增加
          if (saveLLSubReportSet.size() > 0) {
              map.put(saveLLSubReportSet, "INSERT");
          }

      }
      return true;
  }
  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean updateData() {
    return true;
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean deleteData() {
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    System.out.println("--Star GetInputData--");
  tLLSubReportSet = (LLSubReportSet)cInputData.getObjectByObjectName("LLSubReportSet",0);
  mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData",0);
  CaseNo = (String) mTransferData.getValueByName("CaseNo");
   this.mGlobalInput.setSchema( (GlobalInput) cInputData.getObjectByObjectName(
       "GlobalInput", 0));
   return true;
  }

  private boolean prepareOutputData() {
    try {
      this.mInputData.clear();
      mInputData.add(map);
      mResult.clear();
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LDDrugBL";
      tError.functionName = "prepareData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  private boolean checkData() {
      LLCaseDB tLLCaseDB = new LLCaseDB();
      tLLCaseDB.setCaseNo(CaseNo);
      if(!tLLCaseDB.getInfo()){
          CError tError = new CError();
          tError.moduleName = "EventSaveBL";
          tError.functionName = "checkData";
          tError.errorMessage = "查询不到案件信息！";
          this.mErrors.addOneError(tError);
          return false;
      }
      String sHandler = ""+tLLCaseDB.getHandler();
      String sOperator = "" + tLLCaseDB.getOperator();
      if (!sHandler.equals(mGlobalInput.Operator)&&!sOperator.equals(mGlobalInput.Operator))
      {
          CError tError = new CError();
          tError.moduleName = "EventSaveBL";
          tError.functionName = "checkData";
          tError.errorMessage = "您不是该案件指定的处理人，没有操作权限。";
          this.mErrors.addOneError(tError);
          return false;
      }
      String sRgtState = ""+tLLCaseDB.getRgtState();
      if(!sRgtState.equals("01")&&!sRgtState.equals("02")&&!sRgtState.equals("08"))
      {
          CError tError = new CError();
          tError.moduleName = "EventSaveBL";
          tError.functionName = "checkData";
          tError.errorMessage = "该状态下不允许更改事件关联信息。";
          this.mErrors.addOneError(tError);
          return false;
      }
      //#1738 社保调查 理赔各处理流程支持
      String tReturnMsg = LLCaseCommon.checkSurveyRgtState(CaseNo,sRgtState,"01");
      if(!"".equals(tReturnMsg)){
    	  String tMsg = LLCaseCommon.checkSurveyRgtState(CaseNo,sRgtState,"02");
    	  if(!"".equals(tMsg)){
    		  CError.buildErr(this, tMsg);
              return false;
    	  }
      }
      
      return true;
  }
  public VData getResult() {
    return this.mResult;
  }
}
