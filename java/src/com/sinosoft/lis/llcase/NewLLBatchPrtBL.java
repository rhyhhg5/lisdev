package com.sinosoft.lis.llcase;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description: 理赔批量打印
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 *
 * @author Xx
 * @version 1.0
 */
public class NewLLBatchPrtBL {
	public CErrors mErrors = new CErrors();

	private MMap tmpMap = new MMap();

	private VData mResult = new VData();

	private String mOperate;

	private GlobalInput mG = new GlobalInput();

	public NewLLBatchPrtBL() {
	}

	private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();

	private TransferData mPrintFactor = new TransferData();

	// add new parameters
	private int mCount = 0;

	private String ActuGetNo;


	/**
	 * 传输数据的公共方法
	 *
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		cInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;
		System.out.println(mOperate + "LL");
		if (!getInputData(cInputData)) {
			return false;
		}
		if (!dealData()) {
			return false;
		}
		return true;
	}

	public VData getResult() {
		return mResult;
	}

	private boolean getInputData(VData cInputData) {
		mPrintFactor = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		return true;
	}

	public int getCount() {
		return mCount;
	}

	/**
	 * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean dealData() {
		if (!prepareOutputData()) {
			return false;
		}
		mCount = mLOPRTManagerSet.size();
		VData tVData = new VData();
		tVData.add(mG);
		tVData.add(mLOPRTManagerSet);
		System.out.print(mLOPRTManagerSet.get(1).getAgentCode());
		PDFPrintBatchManagerBL tPDFPrintBatchManagerBL = new PDFPrintBatchManagerBL();
		if (!tPDFPrintBatchManagerBL.submitData(tVData, mOperate)) {
			mErrors.addOneError(tPDFPrintBatchManagerBL.mErrors.getError(1));
			return false;
		}
		return true;
	}

	private boolean prepareOutputData() {
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		String tLimit = PubFun.getNoLimit(this.mG.ManageCom);
		String tBatchType = (String) mPrintFactor.getValueByName("BatchType");
		String tGrpDetailPrt = (String) mPrintFactor
				.getValueByName("GrpDetailPrt");
		System.out.println(tGrpDetailPrt);
		String tDetailPrt = (String) mPrintFactor.getValueByName("DetailPrt");
		String tNoticePrt = (String) mPrintFactor.getValueByName("NoticePrt");
		String tGetDetailPrt = (String) mPrintFactor
				.getValueByName("GetDetailPrt");
		LLCaseSet tLLCaseSet = new LLCaseSet();
		if (tBatchType.equals("1")) {
			String tRgtNo = (String) mPrintFactor.getValueByName("RgtNo");
			if (tRgtNo == null || tRgtNo == "") {
				CError tError = new CError();
				tError.moduleName = "LLBatchPrtBL";
				tError.functionName = "checkInputData";
				tError.errorMessage = "批次号为空出错";
				this.mErrors.addOneError(tError);
				return false;
			}
			
//			LLCaseDB tLLCaseDB = new LLCaseDB();
			// tLLCaseDB.setRgtNo(tRgtNo);
			// tLLCaseSet = tLLCaseDB.query();
			// liuli修改于2008/06/26 解决默认的排序比较各乱的问题
//			String sqlstr = " select * from LLCase where RgtNo='" + tRgtNo
//					+ "' order by caseno";
			// System.out.println("--liuli--: "+sqlstr);
//			tLLCaseSet = tLLCaseDB.executeQuery(sqlstr);
			// 打印团体汇总细目表，前台判断案件状态，此处不做判断，如状态不正确也能出当前已有数据的细目表
			if (tGrpDetailPrt.equals("on")) {
				String tCountSQL = "select count(1) from llcase where rgtno='"
						+ tRgtNo + "' and rgtstate in ('11','12')";
				int tCount = Integer.parseInt((tExeSQL.getOneValue(tCountSQL)));
				if (tCount > 0) {
					LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
					String prtSeqNo1 = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
					tLOPRTManagerSchema.setPrtSeq(prtSeqNo1);
					tLOPRTManagerSchema.setOtherNo(tRgtNo);
					tLOPRTManagerSchema.setOtherNoType("7");
					tLOPRTManagerSchema.setPrtType("1");
					tLOPRTManagerSchema.setStateFlag("0");
					tLOPRTManagerSchema.setManageCom(mG.ManageCom);
					System.out.println("~~~~" + tRgtNo.substring(1, 3));
					if (tRgtNo.substring(1, 3).equals("12")) {
						tLOPRTManagerSchema.setCode("lp011");
					} else if (tRgtNo.substring(1, 3).equals("11")) {
						tLOPRTManagerSchema.setCode("lp003bj");
					} else {
						tLOPRTManagerSchema.setCode("lp003");
					}
					tLOPRTManagerSchema.setReqCom(mG.ManageCom);
					tLOPRTManagerSchema.setReqOperator(mG.Operator);
					tLOPRTManagerSchema.setExeOperator(mG.Operator);
					tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
					tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
					mLOPRTManagerSet.add(tLOPRTManagerSchema);
				}

			} else {
				String tCountSQL = "select count(1) from llcase where rgtno='"
						+ tRgtNo + "' and rgtstate in ('11','12')";
				int tCount = Integer.parseInt((tExeSQL.getOneValue(tCountSQL)));
				if (tCount > 1000) {
					buildError("LLBatchPrtBL",
							"单次打印数量过大，可能造成打印异常，请减少打印数量到1000条以下。");
					return false;
				}

				String sqlstr = " select * from LLCase where RgtNo='" + tRgtNo
						+ "' and rgtstate in ('11','12') order by caseno";
				LLCaseDB tLLCaseDB = new LLCaseDB();
				tLLCaseSet = tLLCaseDB.executeQuery(sqlstr);
			}

		}
		if (tBatchType.equals("2")) {
			String tScaseno = (String) mPrintFactor.getValueByName("SCaseNo");
			String tEcaseno = (String) mPrintFactor.getValueByName("ECaseNo");
			String tRgtDateS = (String) mPrintFactor.getValueByName("RgtDateS");
			String tRgtDateE = (String) mPrintFactor.getValueByName("RgtDateE");
			String tEndDateS = (String) mPrintFactor.getValueByName("EndDateS");
			String tEndDateE = (String) mPrintFactor.getValueByName("EndDateE");

			String tSQL = "";
			if (tScaseno != null && !"".equals(tScaseno)) {
				tSQL += " and substr(caseno,6)>=substr('" + tScaseno + "',6)";
			}
			
			if (tEcaseno != null && !"".equals(tEcaseno)) {
				tSQL += " and substr(caseno,6)<=substr('" + tEcaseno + "',6)";
			}
			
			if (tRgtDateS != null && !"".equals(tRgtDateS)) {
				tSQL += " and rgtdate >= '" + tRgtDateS + "'";
			}
			
			if (tRgtDateE != null && !"".equals(tRgtDateE)) {
				tSQL += " and rgtdate <= '" + tRgtDateE + "'";
			}
			
			if (tEndDateS != null && !"".equals(tEndDateS)&& tEndDateE != null && !"".equals(tEndDateE)) {
				tSQL+=" and exists(select 1 from ljagetclaim where otherno=llcase.caseno and othernotype='5'" 
							+" and feeoperationtype<>'FC' and opconfirmdate between '"+tEndDateS+"' and '"+tEndDateE+"')";	
			}
			
			if (!"".equals(tSQL)) {
				String tCountSQL = "select count(1) from llcase where rgtstate in ('11','12') and MngCom like '"
						+ mG.ManageCom + "%' " + tSQL;
				int tCount = Integer.parseInt((tExeSQL.getOneValue(tCountSQL)));
				if (tCount > 1000) {
					buildError("LLBatchPrtBL",
							"单次打印数量过大，可能造成打印异常，请减少打印数量到1000条以下。");
					return false;
				}
								
				LLCaseDB tLLCaseDB = new LLCaseDB();
				String sql = "select * from llcase where rgtstate in ('11','12') and MngCom like '"
						+ mG.ManageCom + "%' " + tSQL + " order by CaseNo";
				System.out.println(sql);
				tLLCaseSet = tLLCaseDB.executeQuery(sql);

			}
		}
		if (tBatchType.equals("3")) {
			String tempstr = (String) mPrintFactor
					.getValueByName("CaseNoBatch");
			String tCaseNoBatch = tempstr.trim();
			while (tCaseNoBatch.length() >= 17) {
				LLCaseSchema tLLCaseSchema = new LLCaseSchema();
				int Cindex = tCaseNoBatch.indexOf("C")<0?(tCaseNoBatch.indexOf("R")<0?tCaseNoBatch.indexOf("S"):tCaseNoBatch.indexOf("R")):tCaseNoBatch.indexOf("C");
				String tCaseNo = tCaseNoBatch.substring(Cindex, Cindex + 17);
				LLCaseDB tLLCaseDB = new LLCaseDB();
				tLLCaseDB.setCaseNo(tCaseNo);
				if (tLLCaseDB.getInfo()) {
					tLLCaseSchema.setSchema(tLLCaseDB.getSchema());
					tLLCaseSet.add(tLLCaseSchema);
				}
				tCaseNoBatch = tCaseNoBatch.substring(Cindex + 17);
				
				if (tCaseNoBatch.length() >= 17 && tLLCaseSet.size() == 1000) {
					buildError("LLBatchPrtBL",
							"单次打印数量过大，可能造成打印异常，请减少打印数量到1000条以下。");
					return false;
				}
				
			}
		}
		// 原先VTS的打印目前走PDF格式,所以这里新增类型4.用来支持从客户信箱中的打印 added by huxl at 20070810
		if (tBatchType.equals("4")) {
			String tCaseNo = (String) mPrintFactor
					.getValueByName("CaseNoBatch");
			LLCaseDB tLLCaseDB = new LLCaseDB();
			tLLCaseDB.setCaseNo(tCaseNo);
			if (tLLCaseDB.getInfo()) {
				tLLCaseSet.add(tLLCaseDB.getSchema());
			}
		}
		// 获取了需要进行打印的所有LLCase信息后,下面进行处理
		int count = tLLCaseSet.size();
		for (int i = 1; i <= count; i++) {
			String trgtstate = tLLCaseSet.get(i).getRgtState();
			if (!trgtstate.equals("11") && !trgtstate.equals("12")) {
				continue;
			} else {
				if (tNoticePrt.equals("on")) {
					LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();

					String prtSeq = PubFun1.CreateMaxNo("prtSeq", tLimit);
					tLOPRTManagerSchema.setPrtSeq(prtSeq);
					tLOPRTManagerSchema.setOtherNo(tLLCaseSet.get(i)
							.getCaseNo());
					tLOPRTManagerSchema.setOtherNoType("5");
					tLOPRTManagerSchema.setPrtType("1");
					tLOPRTManagerSchema.setStateFlag("0");
					tLOPRTManagerSchema.setManageCom(mG.ManageCom);
					tLOPRTManagerSchema.setCode("lp000");
					tLOPRTManagerSchema.setReqCom(mG.ManageCom);
					tLOPRTManagerSchema.setReqOperator(mG.Operator);
					tLOPRTManagerSchema.setExeOperator(mG.Operator);
					tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
					tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
					mLOPRTManagerSet.add(tLOPRTManagerSchema);
					
				}
				// 给付凭证的批次打印
				if (tGetDetailPrt.equals("on")) {

                    LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();                   		
					String prtSeq = PubFun1.CreateMaxNo("prtSeq", tLimit);
					tLOPRTManagerSchema.setPrtSeq(prtSeq);
					tLOPRTManagerSchema.setOtherNo(tLLCaseSet.get(i)
							.getCaseNo());
					tLOPRTManagerSchema.setOtherNoType("5");
					tLOPRTManagerSchema.setPrtType("1");
					tLOPRTManagerSchema.setStateFlag("0");
					tLOPRTManagerSchema.setManageCom(mG.ManageCom);
					tLOPRTManagerSchema.setCode("lp008");
					tLOPRTManagerSchema.setReqCom(mG.ManageCom);
					tLOPRTManagerSchema.setReqOperator(mG.Operator);
					tLOPRTManagerSchema.setExeOperator(mG.Operator);
					tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
					tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
    					
					String strSQL = "select ActuGetNo from LJAGet where OtherNo='"
							+ tLLCaseSet.get(i).getCaseNo()
							+ "'"
							+ "and OtherNoType='5' and (finstate<>'FC' or finstate is null)";
					// 由于理赔可能出现团体统一给付,部分给付的情况,这里对于这种情况不处理,只打印个人给付.
					String tActuGetNo = new ExeSQL().getOneValue(strSQL);
					if (tActuGetNo != "") {
						ActuGetNo = tActuGetNo; // 实付号码
					} else {
						// 对于团体理赔
						continue;
					}
					tLOPRTManagerSchema.setStandbyFlag3("0");
					tLOPRTManagerSchema.setStandbyFlag2(ActuGetNo);
					mLOPRTManagerSet.add(tLOPRTManagerSchema);
				}

				if (tDetailPrt.equals("on")) {
					// 这里只允许打印理赔已经赔付的细目表 modified by huxl @ 2008-8-22
					String SQL = "select distinct b.caserelano from llclaimdetail a ,llcaserela b where a.caseno ='"
							+ tLLCaseSet.get(i).getCaseNo()
							+ "' and a.Caseno = b.CaseNo  and a.CaseRelaNo = b.CaseRelaNo with ur";
					tSSRS = tExeSQL.execSQL(SQL);
					if (tSSRS != null && tSSRS.getMaxRow() > 0) {
						for (int k = 1; k <= tSSRS.getMaxRow(); k++) {
							LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
							String prtSeq = PubFun1.CreateMaxNo("prtSeq",
									tLimit);
							tLOPRTManagerSchema.setPrtSeq(prtSeq);
							tLOPRTManagerSchema.setOtherNo(tLLCaseSet.get(
									i).getCaseNo());
							tLOPRTManagerSchema.setStandbyFlag1(tSSRS
									.GetText(k, 1));
							tLOPRTManagerSchema.setOtherNoType("5");
							tLOPRTManagerSchema.setPrtType("1");
							tLOPRTManagerSchema.setStateFlag("0");
							tLOPRTManagerSchema.setManageCom(mG.ManageCom);
							tLOPRTManagerSchema.setCode("lp001");
							tLOPRTManagerSchema.setReqCom(mG.ManageCom);
							tLOPRTManagerSchema
									.setReqOperator(mG.Operator);
							tLOPRTManagerSchema
									.setExeOperator(mG.Operator);
							tLOPRTManagerSchema.setMakeDate(PubFun
									.getCurrentDate());
							tLOPRTManagerSchema.setMakeTime(PubFun
									.getCurrentTime());
							mLOPRTManagerSet.add(tLOPRTManagerSchema);

						}
					}
				}
			}
		}

		if (mLOPRTManagerSet == null || mLOPRTManagerSet.size() <= 0) {
			if (count == 0) {
				buildError("LLBatchPrtBL", "没有符合打印条件的数据，请确认是否有结案案件！");
			} else if (tGetDetailPrt.equals("on")) {
				buildError("LLBatchPrtBL", "没有符合打印条件的数据，请确认是否为团体统一给付！");
			}
			return false;
		} else {
			tmpMap.put(mLOPRTManagerSet, "INSERT");
		}
		System.out.print(mLOPRTManagerSet.get(1).getAgentCode());
		mResult.clear();
		mResult.add(tmpMap);

		return true;
	}

	public void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLBatchPrtBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {

		TransferData PrintElement = new TransferData();
		PrintElement.setNameAndValue("RgtNo", "P1100070225000019");
		// PrintElement.setNameAndValue("SCaseNo","C0000051107000002");
		// PrintElement.setNameAndValue("ECaseNo","C0000051107000002");
		// PrintElement.setNameAndValue("CaseNoBatch","C1100061116000095");
		PrintElement.setNameAndValue("BatchType", "1");
		PrintElement.setNameAndValue("DetailPrt", "on");
		PrintElement.setNameAndValue("GrpDetailPrt", "on");
		PrintElement.setNameAndValue("NoticePrt", "off");

		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.ComCode = "86";
		tGlobalInput.ManageCom = "86";
		tGlobalInput.Operator = "cm0001";
		LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();
		// mLDSysVarSchema.setSysVar("xmlrealpath");
		mLDSysVarSchema.setSysVarValue("D://62//ui");

		VData aVData = new VData();
		aVData.add(tGlobalInput);
		aVData.add(PrintElement);
		aVData.add(mLDSysVarSchema);
		/*
		 * String aUserCode = "000011"; VData aVData = new VData();
		 * aVData.addElement(aUserCode);
		 */
		LLBatchPrtBL tLLBatchPrtBL = new LLBatchPrtBL();
		tLLBatchPrtBL.submitData(aVData, "PRINT");
		int tCount = tLLBatchPrtBL.getCount();
		System.out.println("~~~~~~~~~~~~~~~~~~~" + tCount);
		String tFileName[] = new String[tCount];
		VData tResult = new VData();
		tResult = tLLBatchPrtBL.getResult();
		tFileName = (String[]) tResult.getObject(0);
		System.out.println("~~~~~~~~~~~~~~~~~~~2" + tFileName);

	}


}
