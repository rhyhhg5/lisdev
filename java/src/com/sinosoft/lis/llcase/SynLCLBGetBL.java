/*
 * <p>ClassName: LCGetBL </p>
 * <p>Description: LCGetSchemaBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-04-01
 */
package com.sinosoft.lis.llcase;
import java.sql.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class SynLCLBGetBL extends LCGetSchema
{
	// @Constructor
	public SynLCLBGetBL()
  {
  }
  /**
   * 设置默认的字段属性
   */
  public void setDefaultFields()
  {
    this.setModifyDate(PubFun.getCurrentDate());
    this.setModifyTime(PubFun.getCurrentTime()) ;
  }
  /**
   *从领取项表和领取项备份表读取信息
   * 返回true或false
   */
  public boolean getInfo()
  {
    Reflections tR=new Reflections();
    LCGetDB tDB=new LCGetDB();
    tDB.setSchema(this);
    if (!tDB.getInfo())//如果查询失败，查询B表
    {
      LBGetDB tDBB=new LBGetDB();
      LBGetSchema tLBGetSchema=new LBGetSchema();
      tR.transFields(tLBGetSchema,this.getSchema());
      tDBB.setSchema(tLBGetSchema);
      if (! tDBB.getInfo())
      {
        // @@错误处理
        CError tError =new CError();
        tError.moduleName="LCGetBL";
        tError.functionName="getInfo";
        tError.errorMessage="没有查询到领取项表";
        this.mErrors .addOneError(tError) ;
        return false;
      }
      else
      {
        LCGetSchema tS=new LCGetSchema();
        tR.transFields(tS,tDBB.getSchema());
        this.setSchema(tS);
      }
    }
    else
    {
      this.setSchema(tDB.getSchema());
    }
    return true;
  }

  /**
   *从领取项表和领取项备份表读取信息
   * 返回LCGetSet
   *
   */
  public LCGetSet query()
  {
    Reflections tR=new Reflections();
    LCGetSet tLCGetSet =new LCGetSet();
    LCGetDB tLCGetDB=new LCGetDB();
    tLCGetDB.setSchema(this.getSchema());
    tLCGetSet=tLCGetDB.query();
    if (tLCGetSet.size()==0)
    {
      LBGetSet tLBGetSet =new LBGetSet();
      LBGetDB tLBGetDB1=new LBGetDB();
      LBGetSchema tLBGetSchema = new LBGetSchema();
      tR.transFields(tLBGetSchema,this.getSchema());
      tLBGetDB1.setSchema(tLBGetSchema);
      tLBGetSet=tLBGetDB1.query();
      if (tLBGetSet.size()==0)
      {
        // @@错误处理
        CError tError =new CError();
        tError.moduleName="LCGetBL";
        tError.functionName="query";
        tError.errorMessage="没有查询到领取项表";
        this.mErrors .addOneError(tError) ;
        return tLCGetSet;
      }
      else
      {

        tLCGetSet.add(this.getSchema());
        tR.transFields(tLCGetSet,tLBGetSet);
      }
    }
    return tLCGetSet;
  }

  /**
   *从领取项表和领取项备份表读取信息
   * 返回LCGetSet
   *
   */
  public LCGetSet executeQuery(String sql_c,String sql_b)
  {
    Reflections tR=new Reflections();
    LCGetSet tLCGetSet =new LCGetSet();
    LCGetDB tLCGetDB=new LCGetDB();
    tLCGetSet=tLCGetDB.executeQuery(sql_c);
    if (tLCGetSet.size()==0)
    {
      LBGetSet tLBGetSet =new LBGetSet();
      LBGetDB tLBGetDB1=new LBGetDB();
      LBGetSchema tLBGetSchema = new LBGetSchema();
      tLBGetSet=tLBGetDB1.executeQuery(sql_b);
      if (tLBGetSet.size()==0)
      {
        // @@错误处理
        CError tError =new CError();
        tError.moduleName="LCGetBL";
        tError.functionName="query";
        tError.errorMessage="没有查询到领取项表";
        this.mErrors .addOneError(tError) ;
        return tLCGetSet;
      }
      else
      {
        tLCGetSet.add(this.getSchema());
        tR.transFields(tLCGetSet,tLBGetSet);
      }
    }
    return tLCGetSet;
  }

  public static void main(String[] args)
  {
//    String a = "";
//      //添加测试代码
//    SynLCLBGetBL aSynLCLBGetBL = new SynLCLBGetBL();
//    aSynLCLBGetBL.setPolNo("86110020040210055727");
//    aSynLCLBGetBL.setGetDutyCode("207201");
//    LCGetSet bLCGetSet = new LCGetSet();
//    bLCGetSet.set(aSynLCLBGetBL.query());
//    System.out.println(bLCGetSet.size());
    LCPolDB tLCPolDB = new LCPolDB();
    tLCPolDB.setPolNo("86110020040210055727");
    boolean t = tLCPolDB.getInfo();
    System.out.println("t的值是"+t);

    LBPolDB tLBPolDB = new LBPolDB();
    tLBPolDB.setPolNo("86110020040210055727");
    boolean tb = tLBPolDB.getInfo();
    System.out.println("t的值是"+tb);

  }


}
