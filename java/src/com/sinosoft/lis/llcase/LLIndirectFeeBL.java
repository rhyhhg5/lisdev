/*
 * <p>Title: Web业务系统</p>
 * <p>ClassName:LLIndirectFeeBL </p>
 * <p>Description: 理赔案件-间接调查费录入 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft </p>
 * @author：Xx
 * @version：1.0
 * @CreateDate：2005-02-20
 */

package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LLIndirectFeeBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private MMap map = new MMap();
    private String mOperate = "";
    //全局数据
    private GlobalInput mG = new GlobalInput();
    //账单费用明细
    private LLIndirectFeeSet mLLIndirectFeeSet = new LLIndirectFeeSet();
    private LLCaseIndFeeSet mLLCaseIndFeeSet = new LLCaseIndFeeSet();
    private String afeedate = "";
    private String odate = "";
    private String otime = "";
    public LLIndirectFeeBL() {
    }

    public static void main(String[] args) {

        LLIndirectFeeSet tLLIndirectFeeSet = new LLIndirectFeeSet();
        LLCaseIndFeeSet tLLCaseIndFeeSet = new LLCaseIndFeeSet();
        LLIndirectFeeBL tLLIndirectFeeBL = new LLIndirectFeeBL();
        LLIndirectFeeSchema tLLIndirectFeeSchema = new LLIndirectFeeSchema();
        tLLIndirectFeeSchema.setFeeItem("01");
        tLLIndirectFeeSchema.setFeeSum("105");
        tLLIndirectFeeSchema.setFeeDate("2006-08-31");
        tLLIndirectFeeSet.add(tLLIndirectFeeSchema);
//        tLLIndirectFeeSchema = new LLIndirectFeeSchema();
//        tLLIndirectFeeSchema.setFeeItem("03");
//        tLLIndirectFeeSchema.setFeeSum("50");
//        tLLIndirectFeeSchema.setFeeDate("2006-04-300");
//        tLLIndirectFeeSet.add(tLLIndirectFeeSchema);
        LLCaseIndFeeSchema tLLCaseIndFeeSchema = new LLCaseIndFeeSchema();
        tLLCaseIndFeeSchema.setOtherNo("C2100060808000001");
        tLLCaseIndFeeSchema.setIndirectSN("0000000000");
        tLLCaseIndFeeSchema.setFeeDate("2006-08-31");
        tLLCaseIndFeeSet.add(tLLCaseIndFeeSchema);
        TransferData te = new TransferData();
        te.setNameAndValue("FeeDate","2006-8-31");
        tLLCaseIndFeeSchema = new LLCaseIndFeeSchema();
        tLLCaseIndFeeSchema.setOtherNo("C2100060815000010");
        tLLCaseIndFeeSchema.setIndirectSN("0000000000");
        tLLCaseIndFeeSchema.setFeeDate("2006-08-31");
         tLLCaseIndFeeSet.add(tLLCaseIndFeeSchema);
//        tLLCaseIndFeeSchema = new LLCaseIndFeeSchema();
//        tLLCaseIndFeeSchema.setOtherNo("C3100060415000002");
//        tLLCaseIndFeeSchema.setFeeDate("2006-04-30");
//         tLLCaseIndFeeSet.add(tLLCaseIndFeeSchema);

        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "cm1102";
        tGI.ManageCom = "8611";
        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tLLIndirectFeeSet);
        tVData.add(tLLCaseIndFeeSet);
        tVData.add(te);
        tLLIndirectFeeBL.submitData(tVData, "INSERT");
        //用于调试
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData 输入数据
     * @param cOperate String 操作类型
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到输入数据
        if (!getInputData())
            return false;
        if(mOperate.equals("INSERT"))
        {
	        //检查数据合法性
	        if (!checkInputData())
	            return false;
	
	        if(!getBaseData())
	            return false;
	
	        //进行业务处理
	        if (!dealData()) {
	            return false;
	        }
	    }
        else if(mOperate.equals("CONFIRM"))
        {        	
        	 //检查数据合法性
	        if (!checkconf())
	            return false;
	        
	        //进行业务处理
	        if (!dealconf()) {
	            return false;
	        }
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLInqFeeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有数据对象
     * @return boolean
     */
    private boolean getInputData() {
        System.out.println("getInputData()..." + mOperate);

        mG = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
        mLLIndirectFeeSet = (LLIndirectFeeSet) mInputData.getObjectByObjectName(
                "LLIndirectFeeSet", 0);
        mLLCaseIndFeeSet = (LLCaseIndFeeSet) mInputData.getObjectByObjectName(
                "LLCaseIndFeeSet", 0);
        TransferData tTr = (TransferData) mInputData.getObjectByObjectName("TransferData",0);
        afeedate = (String) tTr.getValueByName("FeeDate");
        System.out.println(mLLIndirectFeeSet.size());

        odate = PubFun.getCurrentDate();
        otime = PubFun.getCurrentTime();
        
        for(int index=1;index<=mLLCaseIndFeeSet.size();index++)
        {
        	String mOtherNo = mLLCaseIndFeeSet.get(index).getOtherNo();
	        if (!mOtherNo.substring(0, 1).equals("T") && !mOtherNo.substring(0, 1).equals("Z")) 
	        {
		            String tCaseSQL ="SELECT * FROM llcase WHERE rgtstate IN ('11','12','14') "
		                    		+ " AND caseno='" + mOtherNo + "'";
		            ExeSQL tCaseExeSQL = new ExeSQL();
		            SSRS tCaseSSRS = new SSRS();
		            tCaseSSRS = tCaseExeSQL.execSQL(tCaseSQL);
		            if (tCaseSSRS.getMaxRow() <= 0) 
		            {
		                CError.buildErr(this, "该案件还未给付确认或撤件，不能审核！");
		                return false;
		            }
		     }
        }        
        return true;
    }

    /**
     * 对输入数据做必要的校验
     * @return boolean
     */
    private boolean checkInputData() {
    	for(int index=1;index<=mLLCaseIndFeeSet.size();index++)
    	{    		             	     	       
    		  String tSQL = "select a.otherno from LLCaseIndFee a,LLIndirectFee b where b.uwstate in ('1','4') "
    			  	+" and a.feedate=b.feedate and a.otherno='"+mLLCaseIndFeeSet.get(index).getOtherNo()+"'";
   	       	System.out.println(tSQL); 
   	   		ExeSQL tExeSQL = new ExeSQL();
   	   		SSRS tSSRS = new SSRS();
   	   		tSSRS = tExeSQL.execSQL(tSQL);
   	   		if (tSSRS.getMaxRow() > 0) 
   	   		{
   	   			 CError.buildErr(this, "该间接调查费用业务已经审核完毕，不能修改！");
   	   			 return false;
   	   		}     	    	
    	}    
        System.out.println("checkInputData()..." + mOperate);
        String sHandler = mG.Operator;
        String sql = "select surveyflag from llclaimuser where usercode='" +
                     sHandler + "' union select surveyflag from llsocialclaimuser where usercode='"+sHandler+"'";
        ExeSQL es = new ExeSQL();
        String SurveyFlag = es.getOneValue(sql);
        if (!"1".equals(SurveyFlag)) {
            CError.buildErr(this,"对不起，您不是调查主管，没有权限进行这项操作!");
            return false;
        }
        if (mLLIndirectFeeSet.size()>0&&mLLCaseIndFeeSet.size() <= 0) {
            CError.buildErr(this,"没有参加均摊的调查案件，不能录入费用!");
            return false;
        }
        
        String comlimit = "";
        String year = afeedate.substring(0,4);
        if(mG.ManageCom.length()==2)
            comlimit="000000";
        if(mG.ManageCom.length()==4)
            comlimit=mG.ManageCom.substring(2)+"0000";
        if(mG.ManageCom.length()==8)
            comlimit=mG.ManageCom.substring(2);
        String month = afeedate.substring(5,afeedate.lastIndexOf("-"));
        month = (month.length()==1)?"0"+month:month;
        String BatchNo = comlimit+year+month;
        LJAGetClaimDB tljagetclaimdb = new LJAGetClaimDB();
        sql = "select * from ljagetclaim where feefinatype='JJ' and getnoticeno in ('C"
              +BatchNo+"','F"+BatchNo+"')";
        System.out.println(sql);
        LJAGetClaimSet tljagetclaimset = tljagetclaimdb.executeQuery(sql);
        if(tljagetclaimset.size()>0){
            CError.buildErr(this,month+"月的间接调查费已经结算完毕，不能再做修改！");
            return false;
        }
        //增加校验,阻止调查费给付后再修改
        return true;
    }

    /**
     * 处理原来保存过的旧数据
     * @return boolean
     */
    private boolean getBaseData(){
        String fSql = "delete from LLIndirectFee where FeeDate = '" + afeedate
                      + "' and MngCom='"+mG.ManageCom+"'";
        map.put(fSql, "DELETE");
        String cSql = " select * from LLCaseIndFee where FeeDate = '"+ afeedate
                      + "' and MngCom='"+mG.ManageCom+"'";
        LLCaseIndFeeDB tCaseIndFeeDB = new LLCaseIndFeeDB();
        LLCaseIndFeeSet oCaseIndFeeSet = tCaseIndFeeDB.executeQuery(cSql);
        if(oCaseIndFeeSet.size()>0){
            //将案件表的调查状态置为未参加间接调查费用分配
            odate = oCaseIndFeeSet.get(1).getMakeDate();
            otime = oCaseIndFeeSet.get(1).getMakeTime();
            String updatecasesql = "update llcase set surveyflag='1' where caseno in ";
            String updateconsql = "update llconsult set surveyflag='1' where consultno in ";
            String updatefeestasql = "update llinqfeesta set surveyfee=surveyfee-indirectfee,"
                                     +"indirectfee=0 where otherno in ";
            String sqlpart = "(";
            for(int i=1;i<=oCaseIndFeeSet.size();i++){
                sqlpart+="'"+oCaseIndFeeSet.get(i).getOtherNo()+"',";
            }
            sqlpart+="'')";
            updatecasesql += sqlpart;
            updatefeestasql += sqlpart;
            updateconsql += sqlpart;
            map.put(updatecasesql,"UPDATE");
            map.put(updateconsql,"UPDATE");
            map.put(updatefeestasql,"UPDATE");
            map.put(oCaseIndFeeSet, "DELETE");
        }else{
            if(mLLCaseIndFeeSet.size()<=0){
                CError.buildErr(this,"没有可执行的操作，请确认有要保存的信息！");
                return false;
            }
        }
        return true;
    }
    /**
     * 根据前面的输入数据，进行逻辑处理
     * @param cOperate String 操作类型
     * @return boolean
     */
    private boolean dealData() {
        System.out.println("dealData()..." + mOperate);
        //创建账单记录
        LLIndirectFeeSchema tLLIndirectFeeSchema = null;
        double tempFee = 0.00;
        String tLimit = PubFun.getNoLimit("86");
        String IndirectSN = PubFun1.CreateMaxNo("FEEDETAILNO", tLimit);
        String adate = PubFun.getCurrentDate();
        String atime = PubFun.getCurrentTime();
        for (int i = 1; i <= mLLIndirectFeeSet.size(); i++) {
            tLLIndirectFeeSchema = mLLIndirectFeeSet.get(i);
            tLLIndirectFeeSchema.setIndirectSN(IndirectSN);
            tLLIndirectFeeSchema.setOperator(mG.Operator);
            tLLIndirectFeeSchema.setMngCom(mG.ManageCom);
            tLLIndirectFeeSchema.setMakeDate(odate);
            tLLIndirectFeeSchema.setMakeTime(otime);
            tLLIndirectFeeSchema.setModifyDate(adate);
            tLLIndirectFeeSchema.setModifyTime(atime);
            if(mOperate.equals("CONFIRM")&&!tLLIndirectFeeSchema.getUWState().equals("2"))
                continue;
            tempFee += tLLIndirectFeeSchema.getFeeSum();
        }
        map.put(mLLIndirectFeeSet, "INSERT");

        LLInqFeeStaSet iLLInqFeeStaSet = new LLInqFeeStaSet();
        LLInqFeeStaSet uLLInqFeeStaSet = new LLInqFeeStaSet();
        LLCaseIndFeeSchema tLLCaseIndFeeSchema = null;
        int CaseCount = mLLCaseIndFeeSet.size();

        double AverageFee = Math.floor(tempFee * 100 / CaseCount) / 100;
        double FeeinFen = (tempFee - AverageFee * CaseCount) / 0.01;
        String ucasesql = "update llcase set surveyflag='3' where caseno in (";
        String uconssql = "update llconsult set surveyflag='3' where consultno in (";
        for (int i = 1; i <= CaseCount; i++) {
            tLLCaseIndFeeSchema = mLLCaseIndFeeSet.get(i);
            String aOtherNo = tLLCaseIndFeeSchema.getOtherNo();
            ucasesql+="'"+aOtherNo+"',";
            uconssql+="'"+aOtherNo+"',";
            double tfee = AverageFee;
            if (i <= FeeinFen) {
                tfee += 0.01;
            }
            if (i == CaseCount) {
                tfee = tempFee;
            }
            tLLCaseIndFeeSchema.setFeeSum(tfee);
            tempFee -= tfee;
            String tNoType = getNoType(aOtherNo);
            if(mOperate.equals("INSERT")){
                tLLCaseIndFeeSchema.setPayed("0");
            }else{
                tLLCaseIndFeeSchema.setPayed("2");
            }
            tLLCaseIndFeeSchema.setOtherNoType(tNoType);
            tLLCaseIndFeeSchema.setOperator(mG.Operator);
            tLLCaseIndFeeSchema.setMngCom(mG.ManageCom);
            tLLCaseIndFeeSchema.setMakeDate(odate);
            tLLCaseIndFeeSchema.setMakeTime(otime);
            tLLCaseIndFeeSchema.setModifyDate(adate);
            tLLCaseIndFeeSchema.setModifyTime(atime);
            //新增加插入 LLInqFeeSta 表
            LLInqFeeStaDB tLLInqFeeStaDB = new LLInqFeeStaDB();
            LLInqFeeStaSet tLLInqFeeStaSet = new LLInqFeeStaSet();
            LLInqFeeStaSchema tLLInqFeeStaSchema = new LLInqFeeStaSchema();

            tLLInqFeeStaDB.setInqDept(mG.ManageCom);
//                tLLInqFeeStaDB.setContSN("1");
            tLLInqFeeStaDB.setOtherNo(aOtherNo);
            tLLInqFeeStaSet = tLLInqFeeStaDB.query();
            if (tLLInqFeeStaSet.size() > 0) {
                tLLInqFeeStaSchema = tLLInqFeeStaSet.get(1);
                tLLInqFeeStaSchema.setIndirectFee(tfee);
                tLLInqFeeStaSchema.setSurveyFee(Arith.round(tfee +
                        tLLInqFeeStaSchema.getInqFee(), 2));
                tLLInqFeeStaSchema.setOperator(mG.Operator);
//                tLLInqFeeStaSchema.setConfer("");
//                tLLInqFeeStaSchema.setConfDate("");
                tLLInqFeeStaSchema.setModifyDate(adate);
                tLLInqFeeStaSchema.setModifyTime(atime);
                uLLInqFeeStaSet.add(tLLInqFeeStaSchema);
            } else {
                tLLInqFeeStaSchema.setSurveyNo(aOtherNo +"1");
                tLLInqFeeStaSchema.setOtherNo(aOtherNo);
                tLLInqFeeStaSchema.setOtherNoType(tNoType);
                tLLInqFeeStaSchema.setContSN("1");
                tLLInqFeeStaSchema.setInqDept(mG.ManageCom);
                tLLInqFeeStaSchema.setInputer(mG.Operator);
                tLLInqFeeStaSchema.setIndirectFee(tfee);
                tLLInqFeeStaSchema.setSurveyFee(tfee);
//                tLLInqFeeStaSchema.setConfer("");
//                tLLInqFeeStaSchema.setConfDate("");
                tLLInqFeeStaSchema.setInputDate(adate);
                tLLInqFeeStaSchema.setOperator(mG.Operator);
                tLLInqFeeStaSchema.setMakeDate(adate);
                tLLInqFeeStaSchema.setMakeTime(atime);
                tLLInqFeeStaSchema.setModifyDate(adate);
                tLLInqFeeStaSchema.setModifyTime(atime);
                iLLInqFeeStaSet.add(tLLInqFeeStaSchema);
            }
        }
        ucasesql +="'')";
        uconssql +="'')";
        map.put(ucasesql,"UPDATE");
        map.put(uconssql,"UPDATE");
        map.put(mLLCaseIndFeeSet, "INSERT");
        if (iLLInqFeeStaSet.size() > 0)
            map.put(iLLInqFeeStaSet, "INSERT");
        if (uLLInqFeeStaSet.size() > 0)
            map.put(uLLInqFeeStaSet, "UPDATE");
        return true;
    }

    /**
     * 间接调查费校验
     * @return boolean
     */
    private boolean checkconf()
    {
    	for(int index=1;index<=mLLIndirectFeeSet.size();index++)
    	{
    		 String mIndirectSN = mLLIndirectFeeSet.get(index).getIndirectSN();   
        	 if(mIndirectSN.equals("")||mIndirectSN.equals("null"))
        	 {
        		 CError.buildErr(this, "间接调查费用批次流水号为空！");
                 return false;
        	 }                	     	       
 	        String tSQL = "select * from LLIndirectFee where uwstate='1' and IndirectSN='"+mIndirectSN + "'";
 	       	System.out.println(tSQL);
 	   		ExeSQL tExeSQL = new ExeSQL();
 	   		SSRS tSSRS = new SSRS();
 	   		tSSRS = tExeSQL.execSQL(tSQL);
 	   		if (tSSRS.getMaxRow() > 0) 
 	   		{
 	   			 CError.buildErr(this, "该间接调查费用业务已经审核完毕，不能修改！");
 	   			 return false;
 	   		}     	   
    		LLIndirectFeeDB tLLIndirectFeeDB = new LLIndirectFeeDB();	
    		tLLIndirectFeeDB.setIndirectSN(mIndirectSN);
    		LLIndirectFeeSet tLLIndirectFeeSet = tLLIndirectFeeDB.query();
            if (tLLIndirectFeeSet.size() <= 0) {
            	
                CError.buildErr(this, "请先录入间接查勘费再进行审核！");
                return false;
            }
    	}    	    
    	for(int index=1;index<=mLLCaseIndFeeSet.size();index++)
    	{
    		LLInqFeeStaDB tLLInqFeeStaDB = new LLInqFeeStaDB();
    		String mOtherNo = mLLCaseIndFeeSet.get(index).getOtherNo();
            tLLInqFeeStaDB.setOtherNo(mOtherNo);
            LLInqFeeStaSet tLLInqFeeStaSet = tLLInqFeeStaDB.query();
            if (tLLInqFeeStaSet.size() <= 0) {
                CError.buildErr(this, "查勘费用汇总数据有误！");
                return false;
            }
           
            String payed = "" + mLLCaseIndFeeSet.get(index).getPayed();
            if (payed.equals("1")) {
                CError.buildErr(this, "该笔查勘费用已经生成了财务数据，不能再做审定操作");
                return false;
            }
          
    	}    	
        
    	return true;
    }
    /**
     * 间接调查费审核
     * @return boolean
     */
    private boolean dealconf()
    {
    	for(int index=1;index<=mLLCaseIndFeeSet.size();index++)
    	{
    		String mOtherNo = mLLCaseIndFeeSet.get(index).getOtherNo();    
	    	LLSurveyDB tLLSurveyDB = new LLSurveyDB();
	        System.out.println("调查号=========="+mOtherNo+mLLCaseIndFeeSet.get(index).getIndirectSN());
	        tLLSurveyDB.setSurveyNo(mOtherNo+mLLCaseIndFeeSet.get(index).getIndirectSN());
	        LLSurveySet tLLSurveySet = tLLSurveyDB.query();
	        if (tLLSurveySet.size() < 0) {
	            CError.buildErr(this, "调查信息查询失败！");
	            return false;
	        }
	
	        LLInqApplyDB tLLInqApplyDB = new LLInqApplyDB();
	        LLInqApplySet tLLInqApplySet = new LLInqApplySet();
	        tLLInqApplyDB.setSurveyNo(mOtherNo+mLLCaseIndFeeSet.get(index).getIndirectSN());
	        tLLInqApplySet = tLLInqApplyDB.query();
	        if (tLLInqApplySet.size() < 0) {
	            CError.buildErr(this, "调查分配信息查询失败！");
	            return false;
	        }
	        //审核主管
	        String sConfer = "" + tLLSurveySet.get(1).getConfer();

	        if (!sConfer.equals(mG.Operator)) {
	            CError.buildErr(this, "对不起，您没有权限审核间接调查费用!");
	            return false;
	        }
	        LLIndirectFeeSet tLLIndirectFeeSet = new LLIndirectFeeSet();
	        if (sConfer.equals(mG.Operator)) 
	        {	        	
	            for (int i = 1; i <= mLLIndirectFeeSet.size(); i++) 
	            {
	            	LLIndirectFeeDB tLLIndirectFeeDB =  new LLIndirectFeeDB();
	            	tLLIndirectFeeDB.setIndirectSN(mLLIndirectFeeSet.get(i).getIndirectSN());
	            	tLLIndirectFeeDB.setFeeItem(mLLIndirectFeeSet.get(i).getFeeItem());	            	
	            	if(tLLIndirectFeeDB.query().size()>0)
	            	{
	            		LLIndirectFeeSchema tLLIndirectFeeSchema = tLLIndirectFeeDB.query().get(1);
	            		tLLIndirectFeeSchema.setFinInspector(mG.Operator);
	            		tLLIndirectFeeSchema.setUWState("1");
	            		tLLIndirectFeeSchema.setModifyDate(PubFun.getCurrentDate());
	            		tLLIndirectFeeSchema.setModifyTime(PubFun.getCurrentTime());
	            		tLLIndirectFeeSet.add(tLLIndirectFeeSchema);
		            	map.put(tLLIndirectFeeSet, "UPDATE");
	            	}	            	
	            }
	            
	        } 
    	}
    	return true;
    }
    /**
     * 准备往后层输出所需要的数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        System.out.println("prepareOutputData()...");
        try {
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
            return false;
        }
        return true;
    }

    /**
     * 得到处理后的结果数据
     * @return boolean
     */
    public VData getResult() {
        return this.mResult;
    }

    public String getNoType(String OtherNo) {
        if ("Z".equals(OtherNo.substring(0, 1)) ||
            "T".equals(OtherNo.substring(0, 1))) {
            return "0";
        } else if ("C".equals(OtherNo.substring(0, 1))) {
            return "1";
        } else
            return "2";
    }
}
