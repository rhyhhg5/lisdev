package com.sinosoft.lis.llcase;

import com.sinosoft.lis.vschema.LLCaseDiagnosticInfoSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LLCaseDiagnosticInfoUI {

	private VData mInputData;
    private VData mResult = new VData();
    public CErrors mErrors = new CErrors();
    private LLCaseDiagnosticInfoSet mLLCaseDiagnosticInfoSet;
    public LLCaseDiagnosticInfoUI()
    {
    }

    public static void main(String[] args)
    {
    }
    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        LLCaseDiagnosticInfoBL tLLCaseDiagnosticInfoBL = new LLCaseDiagnosticInfoBL();
        tLLCaseDiagnosticInfoBL.submitData(mInputData, cOperate);
        //如果有需要处理的错误，则返回
        if (tLLCaseDiagnosticInfoBL.mErrors.needDealError())
        this.mErrors.copyAllErrors(tLLCaseDiagnosticInfoBL.mErrors);
        //取得结果返回前台
        this.mResult.clear();
        this.mResult = tLLCaseDiagnosticInfoBL.getResult();
        mInputData = null;
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

}
