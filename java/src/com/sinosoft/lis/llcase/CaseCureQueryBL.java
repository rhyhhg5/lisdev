package com.sinosoft.lis.llcase;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class CaseCureQueryBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  private LLCaseCureBL mLLCaseCureBL = new LLCaseCureBL();
  private LLCaseCureBLSet mLLCaseCureBLSet = new LLCaseCureBLSet();
  public CaseCureQueryBL() {}

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
    //数据查询业务处理
    if (cOperate.equals("QUERY||MAIN"))
    {
      if(!queryData())
        return false;
    }
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  private boolean getInputData(VData cInputData)
  {

    if (mOperate.equals("QUERY||MAIN"))
    {
      mLLCaseCureBL.setSchema((LLCaseCureSchema)cInputData.getObjectByObjectName("LLCaseCureSchema",0));
    }
    return true;
  }
  private boolean queryData()
  {
    LLCaseCureDB tLLCaseCureDB = new LLCaseCureDB();
    tLLCaseCureDB.setCaseNo(mLLCaseCureBL.getCaseNo());
    mLLCaseCureBLSet.set(tLLCaseCureDB.query());
    if (tLLCaseCureDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLCaseCureDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "LLCaseCureBL";
      tError.functionName = "queryData";
      tError.errorMessage = "保单查询失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

	mResult.clear();
	      mResult.add(mLLCaseCureBLSet);
	return true;
  }
  }