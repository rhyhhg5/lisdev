package com.sinosoft.lis.llcase;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.PEdorAppItemBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LRDutyDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.task.CheckData;
import com.sinosoft.task.Task;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.schema.LPUWErrorSchema;
import com.sinosoft.lis.schema.LRDutySchema;
import com.sinosoft.lis.schema.LRPolDutyResultSchema;
import com.sinosoft.lis.schema.LWMissionSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.vschema.LRDutySet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubSubmit;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LPSecondUWBL {

    /** 错误处理 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = new GlobalInput();

    private MMap mMap = new MMap();

    /** 数据操作字符串 */
	private String mOperate;
	
    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();
    
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    
    private String mEdorNo = null;  //若需要核保，则生成保全项目信息
    
    private LCContSchema mLCContSchema = new LCContSchema();

    public LPSecondUWBL() {
    }

    public static void main(String[] args) {
    	LPSecondUWBL tLPSecondUWBL = new LPSecondUWBL();
    	VData tVData = new VData();
    	LJAGetSchema tLJAGetSchema = new LJAGetSchema();
    	LLCaseSchema tLLCaseSchema = new LLCaseSchema();
  		tLLCaseSchema.setCaseNo("C4400150123000005");
  		tLLCaseSchema.setRgtNo("C4400150123000005");
		tLJAGetSchema.setOtherNo("C4400150123000005");
		tLJAGetSchema.setOtherNoType("5");
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "pa0001";
        
		tVData.add(tLLCaseSchema);
		tVData.add(tLJAGetSchema);  
		tVData.add(tGlobalInput);
    	tLPSecondUWBL.submitData(tVData, "");
	}
    /**
     * 提交数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
    	this.mOperate = cOperate;
        if (!getInputData(cInputData)) {
            return false;
        }

        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
    	this.mLLCaseSchema.setSchema((LLCaseSchema) cInputData
				.getObjectByObjectName("LLCaseSchema", 0));
    	this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 处理业务数据
     * @return boolean
     */
    private boolean dealData() {
//    	String tSql = "select sum(sumgetmoney) from ljaget where otherno='"+mLLCaseSchema.getCaseNo()+"' with ur";
//    	String tMoney  = new ExeSQL().getOneValue(tSql);
//        //#2304 （需求编号：2014-215）关于“幸福一家”家庭综合保障计划理赔模块增加保单终止缴费标记的需求
//        LLSpecialWrapDealBL tLLSpecialWrapDealBL = new LLSpecialWrapDealBL(mLLCaseSchema.getCaseNo(),mGlobalInput.Operator,Double.parseDouble(tMoney));
//        if(!tLLSpecialWrapDealBL.submitData("DEAL")){
//        	return false;
//        }
//    	
    	 String tSQL = "select distinct contno from llclaimdetail where caseno = '"+mLLCaseSchema.getCaseNo()+"'";
         
         SSRS tSSRS = new SSRS();
         tSSRS = new ExeSQL().execSQL(tSQL);
         if(tSSRS.getMaxRow()>0)
         {
        	 for(int i = 1; i <= tSSRS.getMaxRow(); i++){
        		 
        		 String tSQLCont = "select * from lccont where contno = '"+tSSRS.GetText(1, 1)+"'";
                 LCContSet tLCContSet = new LCContDB().executeQuery(tSQLCont);
                 if (tLCContSet.size() > 1) { //查询结果>1则报错。
                	 return false;
                 }else if(tLCContSet.size() < 1){
                    
                     return false;
                 }
                 
                mLCContSchema = tLCContSet.get(1);
            	if(!createTask() || !createEdorItem() || !createXBDetail())
                {
                   
                    return false;
                }
            	
                if (!createWorkflow()) {
                    return false;
                }
                
               
        		 
        	 }
        	 
        	 
         }
        
         if (!submit())
         {
             return false;
         }

        return true;
    }

    /**
     * 创建工作流，向工作流表中插数据，送人工核保
     */
    private boolean createWorkflow( ) {
        
        String missionId = PubFun1.CreateMaxNo("MissionId", 20);
        LWMissionSchema tLWMissionSchema = new LWMissionSchema();
        
       
        tLWMissionSchema.setMissionID(missionId);
        tLWMissionSchema.setSubMissionID("1");
        tLWMissionSchema.setProcessID("0000000003");
        tLWMissionSchema.setActivityID("0000001181");
        tLWMissionSchema.setActivityStatus("1");
        tLWMissionSchema.setMissionProp1("");
        tLWMissionSchema.setMissionProp2(mLCContSchema.getContNo());
        tLWMissionSchema.setMissionProp3(mLCContSchema.getAppntNo());
        tLWMissionSchema.setMissionProp4(mLCContSchema.getAppntName());
        tLWMissionSchema.setMissionProp5(mLLCaseSchema.getCaseNo());
        tLWMissionSchema.setMissionProp6(mCurrentDate);
        tLWMissionSchema.setMissionProp7(mGlobalInput.Operator);
        tLWMissionSchema.setMissionProp8(mCurrentDate);
        tLWMissionSchema.setMissionProp9(mCurrentTime);
        tLWMissionSchema.setMissionProp10(mLCContSchema.getManageCom());
        tLWMissionSchema.setMissionProp11(mLLCaseSchema.getRgtType());
        tLWMissionSchema.setMissionProp12(mEdorNo);
        tLWMissionSchema.setCreateOperator(mGlobalInput.Operator);
        tLWMissionSchema.setLastOperator(mGlobalInput.Operator);
        tLWMissionSchema.setMakeDate(PubFun.getCurrentDate());
        tLWMissionSchema.setMakeTime(PubFun.getCurrentTime());
        tLWMissionSchema.setModifyDate(PubFun.getCurrentDate());
        tLWMissionSchema.setModifyTime(PubFun.getCurrentTime());
        mMap.put(tLWMissionSchema, "DELETE&INSERT");
        return true;
    }

    
    /**
     * 若需要人工核保，则生成工单信息
     * @return boolean
     */
    private boolean createTask()
    {
        System.out.println("\n\n生成工单");
//        if(mEdorNo == null)
//        {
//            mEdorNo = com.sinosoft.task.CommonBL.createWorkNo();
//        }

//        String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
//        mGetNoticeNo = PubFun1.CreateMaxNo("PAYNOTICENO", tLimit);

        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setWorkNo(getEdorNo());
        tLGWorkSchema.setCustomerNo(mLCContSchema.getAppntNo());
        tLGWorkSchema.setContNo(mLCContSchema.getContNo());
        tLGWorkSchema.setAcceptWayNo(Task.ACCEPTWAY_BATCH_AUTO);
        tLGWorkSchema.setStatusNo(Task.WORKSTATUS_DOING);
        tLGWorkSchema.setApplyName("自动");
        tLGWorkSchema.setTypeNo(Task.WORK_TYPE_XQXBP);
        tLGWorkSchema.setRemark("理赔触发续保核保流程");
        tLGWorkSchema.setInnerSource(Task.WORK_TYPE_LPXB);

        VData tVData = new VData();
        tVData.add(tLGWorkSchema);
        tVData.add(mGlobalInput);
        tVData.add(getWorkBoxNo());

        TaskInputBL tTaskInputBL = new TaskInputBL();
        MMap tMMap = tTaskInputBL.getSubmitData(tVData, "");
        if(tMMap == null)
        {
            mErrors.addOneError("需要人工核保，但是生成工单信息失败:\n"
                + tTaskInputBL.mErrors.getFirstError());
            return false;
        }
        mMap.add(tMMap);

        this.mEdorNo = tTaskInputBL.getWorkNo();

        return true;
    }
    
    private boolean createXBDetail()
    {
        Reflections ref = new Reflections();
        String tSQLCPol = "select * from lcpol a where contno = '"+mLCContSchema.getContNo()+"' and " +
        				" exists (select 1 from llclaimdetail where contno = a.contno and polno=a.polno " +
        					" and caseno = '"+mLLCaseSchema.getCaseNo()+"') and a.riskcode in (select code from ldcode where codetype='lxb' ) ";
        LCPolSet tLCPolSet = new LCPolDB().executeQuery(tSQLCPol);
        if (tLCPolSet.size() <= 0 ) { 
       	 
        	return false;
        }
        
        for(int i = 1; i <= tLCPolSet.size(); i++)
        {
            LPPolSchema tLPPolSchema = new LPPolSchema();
            ref.transFields(tLPPolSchema, tLCPolSet.get(i));
            tLPPolSchema.setEdorNo(this.getEdorNo());
            tLPPolSchema.setEdorType(BQ.EDORTYPE_XB);
//            tLPPolSchema.setContNo(mLCContSchema.getContNo());
//            tLPPolSchema.setProposalContNo(newProposalContNo);
            mMap.put(tLPPolSchema, "INSERT");
            setUWError(i, "险种" + tLPPolSchema.getRiskCode() + "进行过理赔，续保前需要进行人工核保", getEdorNo(), tLPPolSchema);
        }

        return true;
    }
    
    /**
     * 为做人工核保生成XB项目
     * @return boolean
     */
    private boolean createEdorItem()
    {
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setEdorAcceptNo(getEdorNo());
        tLPEdorItemSchema.setGrpContNo(mLCContSchema.getGrpContNo());
        //tLPEdorItemSchema.setDisplayType("1");
        tLPEdorItemSchema.setEdorType(BQ.EDORTYPE_XB);
        tLPEdorItemSchema.setContNo(mLCContSchema.getContNo());
        tLPEdorItemSchema.setInsuredNo(BQ.FILLDATA);
        tLPEdorItemSchema.setPolNo(BQ.FILLDATA);
        tLPEdorItemSchema.setEdorValiDate(mCurrentDate);
        tLPEdorItemSchema.setEdorAppDate(mCurrentDate);

        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
        tLPEdorItemSet.add(tLPEdorItemSchema);

        // 准备传输数据 VData
         VData tVData = new VData();
         tVData.add(tLPEdorItemSet);
         tVData.add(mGlobalInput);

         PEdorAppItemBL tPEdorAppItemBL = new PEdorAppItemBL();
         MMap tMMap = tPEdorAppItemBL.getSubmitData(tVData,"INSERT||EDORITEM");
        if(tMMap == null)
        {
            mErrors.addOneError("需要人工核保，但是生成续保项目信息失败:\n"
                + tPEdorAppItemBL.mErrors.getFirstError());
            return false;
        }
        mMap.add(tMMap);

        setUWState();

        return true;
    }
    
    /**
     * 记录自核错误信息
     * @param error String
     * @param aLPPolSchema LPPolSchema
     */
    private void setUWError(int uwNo, String error, String edorNo, LPPolSchema schema)
    {
        LPUWErrorSchema tLPUWErrorSchema = new LPUWErrorSchema();
        tLPUWErrorSchema.setEdorNo(edorNo);
        tLPUWErrorSchema.setEdorType(BQ.EDORTYPE_XB);
        tLPUWErrorSchema.setGrpContNo(schema.getGrpContNo());
        tLPUWErrorSchema.setContNo(schema.getContNo());
        tLPUWErrorSchema.setProposalContNo(schema.getProposalNo());
        tLPUWErrorSchema.setPolNo(schema.getPolNo());
        tLPUWErrorSchema.setProposalNo(schema.getPolNo());
        tLPUWErrorSchema.setUWNo(uwNo);
        tLPUWErrorSchema.setSerialNo("1"); //对自核来说SerialNo没有意义
        tLPUWErrorSchema.setInsuredNo(schema.getInsuredNo());
        tLPUWErrorSchema.setInsuredName(schema.getInsuredName());
        tLPUWErrorSchema.setAppntNo(schema.getAppntNo());
        tLPUWErrorSchema.setAppntName(schema.getAppntName());
        tLPUWErrorSchema.setManageCom(mGlobalInput.ManageCom);
        tLPUWErrorSchema.setUWError(error);
        tLPUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
        tLPUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
        tLPUWErrorSchema.setUWPassFlag(BQ.UWFLAG_FAIL); //核保未通过
        mMap.put(tLPUWErrorSchema, "DELETE&INSERT");

        mErrors.addOneError(error);
    }
    
    /**
     * 得到信箱号
     * @return String
     */
    private String getWorkBoxNo( )
    {
        String manageCom8 = mLCContSchema.getManageCom();
        String manageCom4 = manageCom8.substring(0, 4);
        String manageCom2 = manageCom8.substring(0, 2);
        ExeSQL tExeSQL = new ExeSQL();

//        StringBuffer sql = new StringBuffer();
//        sql.append()
//            .append()
//            .append()
//            .append().append("' ")
//            .append(")
//            .append().append("' ");
        String sql = "select target "
                     + "from LGAutoDeliverRule "
                     + "where sourceComCode = '" + manageCom8 + "' "
                     + "   and goalType = '" + CheckData.XBP + "' "
                     + "   and defaultFlag = '1' ";
        String target = tExeSQL.getOneValue(sql);
        if(!target.equals("") && !target.equals("null"))
        {
            return target;
        }

        sql = "select target "
                     + "from LGAutoDeliverRule "
                     + "where sourceComCode = '" + manageCom4 + "' "
                     + "   and goalType = '" + CheckData.XBP + "' "
                     + "   and defaultFlag = '1' ";
        target = tExeSQL.getOneValue(sql);
        if(!target.equals("") && !target.equals("null"))
        {
            return target;
        }

        sql = "select target "
                     + "from LGAutoDeliverRule "
                     + "where sourceComCode = '" + manageCom2 + "' "
                     + "   and goalType = '" + CheckData.XBP + "' "
                     + "   and defaultFlag = '1' ";
        target = tExeSQL.getOneValue(sql);
        if(!target.equals("") && !target.equals("null"))
        {
            return target;
        }

        return null;
    }
    
    /**
     * 为核保，取得保全受理号
     * 单一模式既可减少代码，又可避免错误。
     * 如果受理号不存在就生成
     * qulq add 2007-2-3
     */
    private String getEdorNo()
    {
        if(mEdorNo == null)
        {
            mEdorNo = com.sinosoft.task.CommonBL.createWorkNo();
        }
        return mEdorNo;
    }
    
    /**
     * 设置核保状态
     */
    private void setUWState()
    {
        //更新App表
        String sql = "update LPEdorApp " +
                     "set EdorState = '" + BQ.EDORSTATE_SENDUW + "' " +
                     "where EdorAcceptNo = '" + this.getEdorNo() + "'";
        mMap.put(sql, "UPDATE");
        //更新Main表
        sql = "update LPEdorMain " +
              "set EdorState = '" + BQ.EDORSTATE_SENDUW + "' " +
              "where EdorNo = '" + this.getEdorNo() + "'";
        mMap.put(sql, "UPDATE");
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submit() {
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
}
