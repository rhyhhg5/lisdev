package com.sinosoft.lis.llcase;

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: 民生人寿业务系统</p>
 * <p>Description: 立案阶段的保单效力终止业务处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @version 1.0
 */
public class EndPolPowerBL
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData ;
  private VData mResult = new VData();
  private String mOperate;
  private LDSysTraceSet tLDSysTraceSet = new LDSysTraceSet();//向bls传递数据
  private LDSysTraceSet mLDSysTraceSet = new LDSysTraceSet();//接收ui数据
  private LDSysTraceSchema mLDSysTraceSchema = new LDSysTraceSchema();//接收ui数据

  private LLCaseSchema mLLCaseSchema = new LLCaseSchema();//接收界面上的事故类型和分案号码
  private LLCasePolicySet mLLCasePolicySet = new LLCasePolicySet();
  private LLCasePolicySchema mLLCasePolicySchema = new LLCasePolicySchema();
  private GlobalInput mG = new GlobalInput();
  int n;  //界面上所要处理的保单的个数；
  //int i;

  public EndPolPowerBL() {}

  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;
    mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    mLLCasePolicySet = (LLCasePolicySet)cInputData.getObjectByObjectName("LLCasePolicySet",0);
    //mLLCasePolicySet.set((LLCasePolicySet)cInputData.getObjectByObjectName("LLCasePolicySet",0));
    mLLCaseSchema = (LLCaseSchema)cInputData.getObjectByObjectName("LLCaseSchema",0);
    n = mLLCasePolicySet.size();
    System.out.println("共有"+n+"张保单要进行效力终止处理！");
    System.out.println("事故类型是"+mLLCaseSchema.getAccidentType());
    System.out.println("该案件的分案号码是"+mLLCaseSchema.getCaseNo());
    //添加函数来判断该案件是否处于提取调查报告或者是结案状态。该函数的名称是CaseStation（）
    if(!CaseStation())
      return false;
    if(!dealData())
      return false;
    return true;
  }

  private boolean dealData()
  {
    prepareOutputData();
    EndPolPowerBLS tEndPolPowerBLS = new EndPolPowerBLS();
    System.out.println("Start EndPolPower BL Submit...");

    if (!tEndPolPowerBLS.submitData(mInputData,mOperate))
    {
      this.mErrors.copyAllErrors(tEndPolPowerBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "EndPolPowerBL";
      tError.functionName = "submitData";
      tError.errorMessage = "保单效力终止失败！";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

 private boolean CaseStation()
 {
   LLCaseDB tLLCaseDB = new LLCaseDB();
   tLLCaseDB.setCaseNo(mLLCaseSchema.getCaseNo());
   if(!tLLCaseDB.getInfo())
     return false;
   else
   {
     LLRegisterDB tLLRegisterDB = new LLRegisterDB();
     tLLRegisterDB.setRgtNo(tLLCaseDB.getRgtNo());
     if(!tLLRegisterDB.getInfo())
       return false;
     if(tLLRegisterDB.getClmState().equals("5"))
     {
       this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
       CError tError = new CError();
       tError.moduleName = "RegisterUpdateBL";
       tError.functionName = "submitData";
       tError.errorMessage = "该案件正处于调查之中，您无法进保单效力终止操作！！";
       this.mErrors .addOneError(tError) ;
       return false;
     }
     if(tLLRegisterDB.getClmState().equals("2"))
     {
       this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
       CError tError = new CError();
       tError.moduleName = "RegisterUpdateBL";
       tError.functionName = "submitData";
       tError.errorMessage = "该案件已经结案，您无法进行保单效力终止操作！！";
       this.mErrors .addOneError(tError) ;
       return false;
     }
   }
   return true;
 }

 private void prepareOutputData()
 {
   for(int i=1;i<=n;i++)
    {

      System.out.println("i的数值是"+i);
      LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();//向bls传递数据
      mLLCasePolicySchema=mLLCasePolicySet.get(i);
      System.out.println("要进行保单效力终止操作的保单号码是"+mLLCasePolicySchema.getPolNo());
      LDSysTraceDB tLDSysTraceDB = new LDSysTraceDB();
      tLDSysTraceDB.setPolNo(mLLCasePolicySchema.getPolNo());
      System.out.println("保单号是"+mLLCasePolicySchema.getPolNo());
      mLDSysTraceSet=tLDSysTraceDB.query();
      int m = mLDSysTraceSet.size();
      System.out.println("bl中m的值是"+m);
      if(m==0)
      {
        System.out.println("m==0时的准备数据的工程");
        tLDSysTraceSchema.setPolNo(mLLCasePolicySchema.getPolNo());
        tLDSysTraceSchema.setCreatePos("立案录入！");
        tLDSysTraceSchema.setCreatePos2("立案录入！");
        tLDSysTraceSchema.setMakeDate(PubFun.getCurrentDate());
        tLDSysTraceSchema.setMakeTime(PubFun.getCurrentTime());
        tLDSysTraceSchema.setManageCom(mG.ManageCom);
        tLDSysTraceSchema.setManageCom2(mG.ManageCom);
        tLDSysTraceSchema.setModifyDate(PubFun.getCurrentDate());
        tLDSysTraceSchema.setModifyTime(PubFun.getCurrentTime());
        tLDSysTraceSchema.setOperator(mG.Operator);
        tLDSysTraceSchema.setOperator2(mG.Operator);
        tLDSysTraceSchema.setStartDate(PubFun.getCurrentDate());
        if(mLLCaseSchema.getAccidentType().equals("0"))
          tLDSysTraceSchema.setPolState("4001");
        if(mLLCaseSchema.getAccidentType().equals("1"))
           tLDSysTraceSchema.setPolState("4002");
        tLDSysTraceSchema.setPolState(mLLCasePolicySchema.getPolState());
        tLDSysTraceSchema.setRemark("保单效力终止！");
        tLDSysTraceSchema.setValiFlag("1");
        System.out.println("bl中的数据StartData"+tLDSysTraceSchema.getStartDate());
        tLDSysTraceSet.add(tLDSysTraceSchema);
        tLDSysTraceSet.set(i,tLDSysTraceSchema);
        System.out.println("bl中set的个数是"+tLDSysTraceSet.size());

      }
      else
      {
        System.out.println("准备要保存的数据");
        tLDSysTraceSchema.setPolNo(mLLCasePolicySchema.getPolNo());
        tLDSysTraceSchema.setOperator(mG.Operator);
        tLDSysTraceSchema.setMakeDate(PubFun.getCurrentDate());
        tLDSysTraceSchema.setMakeTime(PubFun.getCurrentTime());
        tLDSysTraceSchema.setManageCom(mG.ManageCom);
        tLDSysTraceSchema.setCreatePos("立案录入！");
        if(mLLCaseSchema.getAccidentType().equals("0"))
          tLDSysTraceSchema.setPolState("4001");
        if(mLLCaseSchema.getAccidentType().equals("1"))
          tLDSysTraceSchema.setPolState("4002");
        tLDSysTraceSchema.setOperator2(mG.Operator);
        tLDSysTraceSchema.setManageCom2(mG.ManageCom);
        tLDSysTraceSchema.setCreatePos2("立案录入！");
        tLDSysTraceSchema.setEndDate(PubFun.getCurrentDate());
        tLDSysTraceSchema.setModifyDate(PubFun.getCurrentDate());
        tLDSysTraceSchema.setModifyTime(PubFun.getCurrentTime());
        tLDSysTraceSchema.setValiFlag("1");
        tLDSysTraceSchema.setRemark("保单效力终止！");
        tLDSysTraceSet.add(tLDSysTraceSchema);
        tLDSysTraceSet.set(i,tLDSysTraceSchema);

      }
    }
    mInputData.clear();
    mInputData.add(tLDSysTraceSet);

  }
}