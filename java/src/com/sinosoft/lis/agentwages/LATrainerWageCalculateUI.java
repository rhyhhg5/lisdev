package com.sinosoft.lis.agentwages;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author pengcheng
 * @version 1.0
 */
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LATrainerWageCalculateUI {

	public CErrors mErrors = new CErrors();
	private VData mInputData = new VData();

	public LATrainerWageCalculateUI() {
	}

	public boolean submitData(VData cInputData) {
		mInputData = (VData) cInputData.clone();
		LATrainerWageCalculateBL tLATrainerWageCalculateBL = new LATrainerWageCalculateBL();
		if (!tLATrainerWageCalculateBL.submitData(mInputData)) {
			this.mErrors.copyAllErrors(tLATrainerWageCalculateBL.mErrors);
			CError tError = new CError();
			tError.moduleName = "LACalWageUI";
			tError.functionName = "submitData";
			tError.errorMessage = tLATrainerWageCalculateBL.mErrors
					.getFirstError();
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
}
