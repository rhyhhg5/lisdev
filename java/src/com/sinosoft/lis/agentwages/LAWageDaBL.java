/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agentwages;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LAWageHistorySet;
import com.sinosoft.lis.vschema.LBContSet;
import com.sinosoft.lis.vschema.LBGrpContSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LAAccountsSchema;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.schema.LAWageHistorySchema;
import com.sinosoft.lis.schema.LAWageSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.utility.SSRS;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author pengcheng
 * @version 1.0
 */
public class LAWageDaBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap mMap = new MMap();
    private MMap mmMap = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /*时间变量*/
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAWageSchema mLAWageSchema = new LAWageSchema();
    private LAWageSet mLAWageSet = new LAWageSet();
    private LAWageHistorySchema mLAWageHistorySchema = new LAWageHistorySchema();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private LAAgentSchema mLAAgentSchema=new LAAgentSchema();
    private LJAGetSet mLJAGetSet=new LJAGetSet();
    private String mManageCom = "";
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String mIndexCalNo = "";
    private String mflag = "";
    private String mLAccNameType="ACCOUNTNAME";
    private String mLAccBankNoType ="ACCBankNO";
    public LAWageDaBL()
    {
    }

     public static void main(String[] args)
    {
    }


    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        this.mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        // 进行业务处理
        if (!dealData())
        {
             // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageDaBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAWageDaBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        // 进行业务处理
        if (!dealDataDa())
        {
             // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageDaBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAWageDaBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LAWageDaBL Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAWageDaBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
       
        
        // 进行个险筹备标记2013版 modify by 20130618
        if (!dealDataPrepare())
        {
             // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageDaBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAWageDaBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        if (!prepareOutputDataDa())
        {
            return false;
        }
        PubSubmit tPubSubmitDa = new PubSubmit();
        System.out.println("Start LAWageDaBL Submit...");
        if (!tPubSubmitDa.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAWageDaBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        mInputData=null;

        return true;
    }
    private boolean checkBankNoAccount(){
    	String tWageSQL ="select a.agentcode from  lawage a   where a.state ='0'  and (a.lastmoney + a.CurrMoney) > 0" +
    			" and not exists (select 'X' from labranchgroup where branchattr = a.branchattr " +
    			" and a.branchtype = '1' and state = '1' and branchlevel = '01') and a.indexcalno = '"+mLAWageSchema.getIndexCalNo()+"'" +
    			" and a.managecom like '"+mLAWageSchema.getManageCom()+"%' and a.BranchType = '"+mLAWageSchema.getBranchType()+"'and a.BranchType2 = '"+mLAWageSchema.getBranchType2()+"' ";
    	SSRS tSSRS = new SSRS();
    	ExeSQL tExeSQL = new ExeSQL();
    	tSSRS = tExeSQL.execSQL(tWageSQL);
    	for(int i = 1; i <= tSSRS.getMaxRow(); i++){
    		LAAgentSchema tLAAgentSchema = new LAAgentSchema();
    		LAAgentDB tLAAgentDB = new LAAgentDB();
    		tLAAgentDB.setAgentCode(tSSRS.GetText(i, 1));
    		tLAAgentDB.getInfo();
    		tLAAgentSchema = tLAAgentDB.getSchema();
    		String tAgentCode = tLAAgentSchema.getAgentCode();
    		String tAccountsSql ="select * from laaccounts where agentcode ='"+tAgentCode+"' and state ='0' with ur ";
    		LAAccountsSet tLAAccountsSet = new LAAccountsSet();
    		LAAccountsDB tLAAccountsDB = new LAAccountsDB();
    		tLAAccountsSet = tLAAccountsDB.executeQuery(tAccountsSql);
    		if(tLAAccountsSet.size() == 0){
    			this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
	            CError tError = new CError();
	            tError.moduleName = "LAWageDaBL";
	            tError.functionName = "check";
	            tError.errorMessage = "代理人" + tLAAgentSchema.getGroupAgentCode() + "的有效银行帐户信息为空,请录入该代理人有效的银行帐户信息!";
	            this.mErrors.addOneError(tError);
	            return false;
    		}
    		LAAccountsSchema tLAAccountsSchema = new LAAccountsSchema();
    		tLAAccountsSchema = tLAAccountsSet.get(1);
    		String tBankCode = tLAAccountsSchema.getBankCode();
    		String tAccName = tLAAccountsSchema.getAccountName();
    		if("".equals(tBankCode)||tBankCode==null){
    			this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
	            CError tError = new CError();
	            tError.moduleName = "LAWageDaBL";
	            tError.functionName = "check";
	            tError.errorMessage = "代理人" + tLAAgentSchema.getGroupAgentCode() + "的开户行号为空,请录入该代理人的开户行号!";
	            this.mErrors.addOneError(tError);
	            return false;
    		}
    		if("".equals(tAccName)||tAccName==null){
    			this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
	            CError tError = new CError();
	            tError.moduleName = "LAWageDaBL";
	            tError.functionName = "check";
	            tError.errorMessage = "代理人" + tLAAgentSchema.getGroupAgentCode() + "的帐号名称为空,请录入该代理人的帐号名称!";
	            this.mErrors.addOneError(tError);
	            return false;
    		}
    	}
    	
    	return true;
    }
    private boolean checkData(){
    	String tLAWageSQL = " select a.agentcode,sum(a.F01+a.F04+a.F05+a.F16+a.k07+a.k08+a.k09) as fyc from  lawage a   where a.state ='0'  and (a.lastmoney + a.CurrMoney) > 0 " +
    			" and not exists (select 'X' from labranchgroup where branchattr = a.branchattr  " +
    			" and a.branchtype = '1' and state = '1' and branchlevel = '01') and a.indexcalno = '"+mLAWageSchema.getIndexCalNo()+"'" +
    			" and a.managecom like '"+mLAWageSchema.getManageCom()+"%' and (a.F01 + a.F04 + a.F05 + a.F16+a.k07+a.k08+a.k09) <>0 and a.BranchType = '"+mLAWageSchema.getBranchType()+"'and a.BranchType2 = '"+mLAWageSchema.getBranchType2()+"'"+ 
    			"   group by a.agentcode ";
    	String tLACommisionSQL = "select a.agentcode,sum(a.directwage) as fyc from LACommision a "
    							+" where  1=1 and a.branchtype = '1' and a.Fyc != 0 and a.commdire = '1' "
								+" and (a.branchtype2 = '01' or (a.branchtype2 = '03' and a.f3 is not null and trim(a.f3) <> '')) "
								+" and a.agentgroup not in ('000000000003', '000000000002') "
								+" and a.wageno = '"+mLAWageSchema.getIndexCalNo()+"' and a.managecom like '"+mLAWageSchema.getManageCom()+"%' "
								+" and exists (select 1 from lawage where 1 = 1 AND managecom = a.managecom AND indexcalno = a.WageNo and state ='0'  and (lastmoney + CurrMoney) > 0 "
								+" and BranchType = '"+mLAWageSchema.getBranchType()+"'and BranchType2 = '"+mLAWageSchema.getBranchType2()+"'" 
								+" AND agentcode = a.agentcode AND agentgroup not in ('000000000003', '000000000002')) group by a.agentcode";
   
    	String tJudgeSQL = "select distinct 1 from ("+tLAWageSQL+") as lawage,("+tLACommisionSQL+") as lacommision where lawage.agentcode= lacommision.agentcode and lawage.fyc<>lacommision.fyc with ur";
    	String tResult = new ExeSQL().getOneValue(tJudgeSQL);
    	if("1".equals(tResult))
    	{
    		return false;
    	}
    	return true;
    }

    private boolean dealData()
    {
        boolean tReturn = true;
        if(mflag=="0")
        {//根据前台数据全部处理，但是SumMoney不小于0

            System.out.println("----:" + mLAWageSchema.getAgentGroup());

            String sql = "update lawage set state='1' "
            +" where state='0' and (lastmoney+CurrMoney)>=0   "
            +" and not exists (select 'X' from labranchgroup where branchattr=lawage.branchattr and branchtype='1' and state='1' and branchlevel='01' )"
            +" and indexcalno='" +mLAWageSchema.getIndexCalNo()
            +"' and managecom like '" +mLAWageSchema.getManageCom()
            + "%' and managecom like '" +mGlobalInput.ManageCom
            + "%' and BranchType='" +mLAWageSchema.getBranchType()
            + "' and BranchType2='" + mLAWageSchema.getBranchType2() +"'";
//            if (mLAWageSchema.getAgentCode() != null &&!mLAWageSchema.getAgentCode().trim().equals("")) {
//                sql = sql + " and agentcode='" + mLAWageSchema.getAgentCode() +"'";
//            }
//            if (mLAAgentSchema.getName() != null &&!mLAAgentSchema.getName().trim().equals("")) {
//                if (!checkname()) {
//                    return false;
//                }
//                sql = sql + " and exists ( select a.agentcode from laagent a  where a.agentcode=lawage.agentcode and a.name='" +
//                      mLAAgentSchema.getName().trim() + "') ";
//            }

//            if (mLAWageSchema.getAgentGroup() != null &&!mLAWageSchema.getAgentGroup().equals("")) {
//                if (!checkagentgroup()) {
//                    return false;
//                }
//                sql = sql + " and branchattr like '" +
//                      mLAWageSchema.getAgentGroup() +
//                      "%' ";
//            }

            System.out.println("update LAWage:---" +mManageCom);
            mMap.put(sql, "UPDATE");
//            ExeSQL tExeSQL = new ExeSQL();
//            if (!tExeSQL.execUpdateSQL(sql)) {
//                this.mErrors.copyAllErrors(tExeSQL.mErrors);
//                CError tError = new CError();
//                tError.moduleName = "LAWageDaBL";
//                tError.functionName = "dealData";
//                tError.errorMessage = "薪筹审核失败!";
//                this.mErrors.addOneError(tError);
//                return false;
//
//            }
        }
        else
        {//做个案处理SumMoney可以小于0 2005-1107 modify:xiangchun
            System.out.println( "update LAWage"+mLAWageSet.size());
            LAWageSet tLAWageSet=new LAWageSet();
            tLAWageSet=mLAWageSet;
            System.out.println("update LAWage"+tLAWageSet.size());
            mLAWageSet=new LAWageSet();
            for(int i=1;i<=tLAWageSet.size();i++)
            {
                System.out.println("update LAWage");
                mLAWageSchema=new LAWageSchema();
                LAWageDB tLAWageDB=new LAWageDB();
                LAWageSchema tLAWageSchema=new LAWageSchema();
                mLAWageSchema=tLAWageSet.get(i);
                tLAWageDB.setManageCom(mLAWageSchema.getManageCom());
                tLAWageDB.setAgentCode(mLAWageSchema.getAgentCode());
                tLAWageDB.setIndexCalNo(mLAWageSchema.getIndexCalNo());
                tLAWageDB.setBranchType(mLAWageSchema.getBranchType());
                tLAWageDB.setBranchType2(mLAWageSchema.getBranchType2());
                System.out.println("update LAWage");
                tLAWageDB.getInfo();
                System.out.println("update LAWage");
                tLAWageSchema=tLAWageDB.getSchema();
                if(tLAWageSchema.getState().trim().equals("1"))
                {
                    CError tError = new CError();
                     tError.moduleName = "ALAWageBL";
                     tError.functionName = "dealdata";
                     tError.errorMessage = "销售员代码为："+tLAWageSchema.getAgentCode()+",月份为："+tLAWageSchema.getIndexCalNo()+"的薪资已发放,不需要再审核";
                     this.mErrors.addOneError(tError);
                     return false;
                }
                System.out.println("update LAWage"+tLAWageSchema);
                tLAWageSchema.setState("1");
                mLAWageSet.add(tLAWageSchema);
            }
           mMap.put(mLAWageSet, "UPDATE");

        }
        tReturn = true;
        return tReturn;

    }

    private boolean dealDataDa()
    {
//       String tSQL="select distinct state  from  lawage  where managecom like '"+mManageCom+"%' and indexcalno='"
//                    +mIndexCalNo+"' and (lastmoney+CurrMoney)>=0 and branchtype='"+mBranchType+"' and branchtype2='"+mBranchType2+"' order by state";
//       SSRS tSSRS = new SSRS();
//       ExeSQL tExeSQL = new ExeSQL();
//       System.out.println("查询语句："+tSQL);
//       tSSRS = tExeSQL.execSQL(tSQL);
//
//	       if(!tSSRS.GetText(1, 1).equals("0"))
//	       {
	           String tSql="select * from LAWageHistory where managecom like '"+mManageCom+"%' and wageno='"
	                        +mIndexCalNo+"' and branchtype='"+mBranchType+"' and branchtype2='"+mBranchType2
	                        +"' and aclass='03' ";
	          LAWageHistoryDB tLAWageHistoryDB= new LAWageHistoryDB();
	          LAWageHistorySet tLAWageHistorySet= new LAWageHistorySet();
	          tLAWageHistorySet=tLAWageHistoryDB.executeQuery(tSql);
	          if(tLAWageHistorySet.size() <= 0)
	          {
	            buildError("dealData","查询佣金计算历史表出错");
	            return false;
	          }
	           mLAWageHistorySchema=tLAWageHistorySet.get(1);
	           if(mLAWageHistorySchema.getState().equals("14"))
	           {
	              buildError("dealData","该月佣金已确认，不能再确认!");
	              return false;
	           }
	           if(mLAWageHistorySchema.getState().equals("12"))
	           {
	               buildError("dealData","该月佣金试算未完成，不能再确认!");
	              return false;
	           }
	            mLAWageHistorySchema.setState("14");
	            mLAWageHistorySchema.setModifyDate(currentDate);
	            mLAWageHistorySchema.setModifyTime(currentTime);
	            mmMap.put(this.mLAWageHistorySchema, "UPDATE");
	            //对查询完成后的数据审核完成后，生成应付
	            String tSQL1="select '1' from ldcode where codetype = 'XS_WAGE101' and code = '"+mManageCom.substring(0, 4)+"'";
	            SSRS tSSRS1 = new SSRS();
	            ExeSQL tExeSQL1 = new ExeSQL();
	            System.out.println("查询语句："+tSQL1);
	            tSSRS1 = tExeSQL1.execSQL(tSQL1);
	            if(tSSRS1.getMaxRow()>0&&"1".equals(tSSRS1.GetText(1, 1))){
	                if(!checkBankNoAccount())
	                {
	                   return false; 	
	                }
	                if (!checkData())
	                {
	                	CError tError = new CError();
	                    tError.moduleName = "LAWageDaBL";
	                    tError.functionName = "submitData";
	                    tError.errorMessage = "直接佣金计算结果与明细核对不上，请重新计算薪资!";
	                    this.mErrors.addOneError(tError);
	                    return false;
	                } 

		            if(!createPayableData()){
		            	buildError("dealData","生成应付数据失败！");
		            	return false;
		            }
	            }
	            //由于财务的要求，应付功能先不上线，此处注释掉
	//            if(mManageCom.substring(0,4).equals("8637")){
	//            	if(!createGetData()){
	//                	buildError("dealData","生成财务应付数据失败!");
	//                    return false;
	//                }
	//            }
	            
//	       }
       
        return true;
    }

    private boolean dealDataPrepare()
    {
       String tSQL="select distinct state  from  lawage "
    	          +" where managecom like '"+mManageCom+"%' and indexcalno='"+mIndexCalNo+"'"
                  +" and (lastmoney+CurrMoney)>=0 and branchtype='"+mBranchType+"' "
                  +" and branchtype2='"+mBranchType2+"' order by state";
       SSRS tSSRS = new SSRS();
       ExeSQL tExeSQL = new ExeSQL();
       System.out.println("查询语句："+tSQL);
       tSSRS = tExeSQL.execSQL(tSQL);
       if(!tSSRS.GetText(1, 1).equals("0"))
       {
           String tSql=" select * from Laagent a"
        	         +" where a.managecom like '"+mManageCom+"%' "
        	         +" and a.branchtype='1' and a.branchtype2='01'  "
        	         +" and a.wageversion='2013A' "
        	         +" and exists (select '1' from lawage where branchtype='1' and branchtype2='01' and agentcode=a.agentcode "
        	         +"    and indexcalno='"+mIndexCalNo+"' and managecom like '"+mManageCom+"%' and agentgrade<'B01')"
        	         +" and ( "
        	         +"( day(a.traindate)<16 and  DATE_FORMAT((date(a.traindate)+ 2 month),'yyyymm')='"+mIndexCalNo+"')"
        	         +" or "
        	         +"( day(a.traindate)>=16 and  DATE_FORMAT((date(a.traindate)+ 3 month),'yyyymm')='"+mIndexCalNo+"')"
        	         +" )"
        	         +" union "
        	         +" select * from Laagent a"
        	         +" where a.managecom like '"+mManageCom+"%' "
        	         +" and a.branchtype='1' and a.branchtype2='01'  "
        	         +" and a.wageversion='2013A' "
        	         +" and exists (select '1' from lawage where branchtype='1' and branchtype2='01' and agentcode=a.agentcode  "
        	         +"    and indexcalno='"+mIndexCalNo+"' and managecom like '"+mManageCom+"%' and agentgrade>'B01')"
        	         +" and DATE_FORMAT((date(a.traindate)+ 2 month),'yyyymm')='"+mIndexCalNo+"' "
        	         +" union "
        	         +" select * from Laagent a"
        	         +" where a.managecom like '"+mManageCom+"%' "
        	         +" and a.branchtype='1' and a.branchtype2='01'  "
        	         +" and a.wageversion='2013A' "
        	         +" and exists (select '1' from lawage where branchtype='1' and branchtype2='01' and agentcode=a.agentcode  "
        	         +"    and indexcalno='"+mIndexCalNo+"' and managecom like '"+mManageCom+"%' and agentgrade>'B01')"
        	         +" and DATE_FORMAT((date(a.traindate)+ 2 month),'yyyymm')>'"+mIndexCalNo+"' "
        	         //+" and exists (SELECT '1' FROM DUAL WHERE (select sum(f23) from LAWAGE where agentcode=a.agentcode and indexcalno>=DATE_FORMAT(date(a.traindate),'yyyymm')"
        	         //+"      and indexcalno<='"+mIndexCalNo+"')=5000 )"
       	             +" and exists (SELECT '1' FROM DUAL WHERE (select value(sum(t23),0) from LAINDEXINFO where agentcode=a.agentcode and indextype='01' and indexcalno>=DATE_FORMAT(date(a.traindate),'yyyymm')"
        	         +"      and indexcalno<='"+mIndexCalNo+"')=5000 )"
        	         ;
          System.out.println(tSql);
          LAAgentDB tLAAgentDB= new LAAgentDB();
          LAAgentSet tLAAgentSet= new LAAgentSet();
          tLAAgentSet=tLAAgentDB.executeQuery(tSql);
          if(tLAAgentSet.size()<=0)
          {
        	  return true;
          }
          
          for (int i = 1; i <= tLAAgentSet.size(); i++)
        	{
        	 String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        	 LAAgentSchema cLAAgentSchema = new LAAgentSchema();
             cLAAgentSchema = tLAAgentSet.get(i);

             LAAgentDB ttLAAgentDB = new LAAgentDB();
             ttLAAgentDB.setAgentCode(cLAAgentSchema.getAgentCode());
             if (!ttLAAgentDB.getInfo())
             {
                 this.mErrors.copyAllErrors(ttLAAgentDB.mErrors);
                 CError tError = new CError();
                 tError.moduleName = "LAWageDaBL";
                 tError.functionName = "dealData";
                 tError.errorMessage = "查询业务员"+cLAAgentSchema.getAgentCode()  +"的基础信息失败!";
                 this.mErrors.addOneError(tError);
                 return false;
              }
              LAAgentSchema  tLAAgentSchema = new LAAgentSchema();
              tLAAgentSchema = ttLAAgentDB.getSchema();
              LAAgentBSchema tLAAgentBSchema=new LAAgentBSchema();
              Reflections tReflections=new Reflections();
              tReflections=new Reflections();
              tReflections.transFields(tLAAgentBSchema,tLAAgentSchema) ;
              tLAAgentBSchema.setMakeDate(currentDate) ;
              tLAAgentBSchema.setMakeTime(currentTime) ;
              tLAAgentBSchema.setOperator(mGlobalInput.Operator ) ;
              tLAAgentBSchema.setEdorNo(tEdorNo) ;
              tLAAgentBSchema.setEdorType("13") ;
              //  当前月的上月月底，考虑薪资问题，暂时把这个当成筹备即结束日期
              String sql1 = "select  enddate - 1 months ,enddate+1  days,enddate+12 months   from lastatsegment where stattype='1' and  yearmonth=int('"+mIndexCalNo+"')";
              SSRS tSSRS1 = new SSRS();
              ExeSQL tExeSQL1 = new ExeSQL();
              tSSRS1 = tExeSQL1.execSQL(sql1);
              if (tExeSQL1.mErrors.needDealError()) {
                 this.mErrors.copyAllErrors(tExeSQL1.mErrors);
                 CError tError = new CError();
                 tError.moduleName = "LAWageDaBL";
                 tError.functionName = "dealData";
                 tError.errorMessage = "查询新代理人信息出错！";
                 this.mErrors.addOneError(tError);
                 return false;
               }
              String tenddate = tSSRS1.GetText(1, 1);
              tLAAgentBSchema.setPrepareEndDate(tenddate);//后面补充
              this.mLAAgentBSet.add(tLAAgentBSchema)  ;
              tLAAgentSchema.setModifyDate(currentDate) ;
              tLAAgentSchema.setModifyTime(currentTime);
              tLAAgentSchema.setNoWorkFlag("N");
              tLAAgentSchema.setTrainDate("");
              tLAAgentSchema.setPreparaGrade("");
              
              String sql11 = "select GBuildFlag  from labranchgroup where agentgroup='"+tLAAgentSchema.getAgentGroup()+"'";
              SSRS tSSRS11 = new SSRS();
              ExeSQL tExeSQL11 = new ExeSQL();
              tSSRS11 = tExeSQL11.execSQL(sql11);
              if (tExeSQL11.mErrors.needDealError()) {
                  this.mErrors.copyAllErrors(tExeSQL1.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "LAWageDaBL";
                  tError.functionName = "dealData";
                  tError.errorMessage = "查询新代理人信息出错！";
                  this.mErrors.addOneError(tError);
                  return false;
                }
              if(tSSRS11!=null&&tSSRS11.GetText(1, 1).equals("A")||tSSRS11.GetText(1, 1).equals("B"))
              {
            	  tLAAgentSchema.setWageVersion("2013B");
            	  tLAAgentSchema.setGBuildFlag(tSSRS11.GetText(1, 1));
            	  tLAAgentSchema.setGBuildStartDate(tSSRS1.GetText(1, 2));
            	  tLAAgentSchema.setGBuildEndDate(tSSRS1.GetText(1, 3));
              }
              else
              {
            	  tLAAgentSchema.setWageVersion("2010");
            	  tLAAgentSchema.setGBuildFlag("");
            	  tLAAgentSchema.setGBuildStartDate("");
            	  tLAAgentSchema.setGBuildEndDate("");
              }
              
              
              tLAAgentSchema.setOperator(mGlobalInput.Operator ) ;
              this.mLAAgentSet.add(tLAAgentSchema) ;

              LATreeDB tLATreeDB = new LATreeDB();
              tLATreeDB.setAgentCode(cLAAgentSchema.getAgentCode()); //jiangcx add for BK 主键
              if (!tLATreeDB.getInfo())
              {
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAPreparationBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询业务员"+cLAAgentSchema.getAgentCode()  +"的基础信息失败!";
                this.mErrors.addOneError(tError);
                return false;
              }
              LATreeSchema tLATreeSchema = new LATreeSchema();
              tLATreeSchema = tLATreeDB.getSchema();
              LATreeBSchema tLATreeBSchema=new LATreeBSchema();
              tReflections=new Reflections();
              tReflections.transFields(tLATreeBSchema,tLATreeSchema);
              tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator()) ;
              tLATreeBSchema.setOperator(mGlobalInput.Operator);
              tLATreeBSchema.setEdorNO(tEdorNo) ;
              tLATreeBSchema.setRemoveType("13") ;
              tLATreeBSchema.setMakeDate2(tLATreeBSchema.getMakeDate() );
              tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
              tLATreeBSchema.setMakeDate(currentDate) ;
              tLATreeBSchema.setMakeTime(currentTime) ;
              this.mLATreeBSet .add(tLATreeBSchema) ;
              //设置
              tLATreeSchema.setWageVersion(tLAAgentSchema.getWageVersion()) ;
              tLATreeSchema.setOperator(this.mGlobalInput.Operator);
              tLATreeSchema.setModifyDate(currentDate);
              tLATreeSchema.setModifyTime(currentTime);
              this.mLATreeSet.add(tLATreeSchema);
        	}

            mmMap.put(this.mLAAgentBSet ,"INSERT") ;
            mmMap.put(this.mLATreeBSet,"INSERT") ;
            mmMap.put(this.mLATreeSet,"UPDATE") ;
            mmMap.put(this.mLAAgentSet,"UPDATE") ;          
       }

        return true;
    }
    
    
    private boolean createGetData(){
    	SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	String tSQL="select a.managecom,a.grpcontno,a.contno,a.receiptno," +
    			"a.agentcode,a.agentgroup,sum(fyc) from lacommision a,lawage b " +
    			"where a.wageno=b.indexcalno and a.agentcode=b.agentcode " +
    			"and a.branchtype=b.branchtype and a.branchtype2=b.branchtype2 " + 
    			"and a.branchtype='1' and a.branchtype2='01' " +
    			"and a.agentgroup not in ('000000000003','000000000002') " +
    			"and a.commdire='1' and a.fyc!=0 " +
    			"and a.managecom like '"+mManageCom+
    			"%' and a.wageno='"+mIndexCalNo+ 
    			"' group by a.managecom,a.agentcode,a.agentgroup,a.grpcontno," +
    		    "a.contno,a.receiptno order by a.managecom,a.agentcode," +
    		    "a.grpcontno,a.contno with ur" ;
    	tSSRS=tExeSQL.execSQL(tSQL);
    	LCContSet tLCContSet=null;
        LCContDB tLCContDB=new LCContDB();
        LCGrpContSet tLCGrpContSet=null;
        LCGrpContDB tLCGrpContDB=new LCGrpContDB();
        LBContSet tLBContSet=null;
        LBContDB tLBContDB=new LBContDB();
        LBGrpContSet tLBGrpContSet=null;
        LBGrpContDB tLBGrpContDB=new LBGrpContDB();
    	String tLimit = null;
    	LJAGetSchema tLJAGetSchema=null;
    	for(int i=1;i<=tSSRS.getMaxRow();i++){
    		tLJAGetSchema = new LJAGetSchema();
            tLimit = PubFun.getNoLimit(tSSRS.GetText(i,1));
            tLJAGetSchema.setActuGetNo(PubFun1.CreateMaxNo("SCGETNO", tLimit));
            tLJAGetSchema.setOtherNo(tSSRS.GetText(i,4));
            tLJAGetSchema.setOtherNoType("SC");
    		if(tSSRS.GetText(i,2)==null
    				||tSSRS.GetText(i,2).equals("00000000000000000000")){
    			tSQL="select * from lccont where contno='"
          		  +tSSRS.GetText(i,3)+"'";
          	  tLCContSet=tLCContDB.executeQuery(tSQL);
          	  if(tLCContSet.size()>0){
          		  tLJAGetSchema.setOtherNo(tSSRS.GetText(i,4));
              	  tLJAGetSchema.setPayMode(tLCContSet.get(1).getPayMode());
                    tLJAGetSchema.setManageCom(tSSRS.GetText(i,1));
                    tLJAGetSchema.setAgentCom("");
                    tLJAGetSchema.setAgentType(tLCContSet.get(1).getAgentType());
                    tLJAGetSchema.setAgentCode(tSSRS.GetText(i,5));
                    tLJAGetSchema.setAgentGroup(tSSRS.GetText(i,6));
                    tLJAGetSchema.setAppntNo(tLCContSet.get(1).getAppntNo());
                    tLJAGetSchema.setAccName(tLCContSet.get(1).getAccName());
                    tLJAGetSchema.setSumGetMoney(tSSRS.GetText(i,7));
                    tLJAGetSchema.setBankAccNo(tLCContSet.get(1).getBankAccNo());
                    tLJAGetSchema.setBankCode(tLCContSet.get(1).getBankCode());
                    tLJAGetSchema.setDrawer(tLCContSet.get(1).getAppntName());
                    tLJAGetSchema.setDrawerID(tLCContSet.get(1).getAppntIDNo());
          	  }else{
          		  tSQL="select * from lbcont where contno='"
              		  +tSSRS.GetText(i,3)+"'";
              	  tLBContSet=tLBContDB.executeQuery(tSQL);
              	  tLJAGetSchema.setOtherNo(tSSRS.GetText(i,4));
              	  tLJAGetSchema.setPayMode(tLBContSet.get(1).getPayMode());
                    tLJAGetSchema.setManageCom(tSSRS.GetText(i,1));
                    tLJAGetSchema.setAgentCom("");
                    tLJAGetSchema.setAgentType(tLBContSet.get(1).getAgentType());
                    tLJAGetSchema.setAgentCode(tSSRS.GetText(i,5));
                    tLJAGetSchema.setAgentGroup(tSSRS.GetText(i,6));
                    tLJAGetSchema.setAppntNo(tLBContSet.get(1).getAppntNo());
                    tLJAGetSchema.setAccName(tLBContSet.get(1).getAccName());
                    tLJAGetSchema.setSumGetMoney(tSSRS.GetText(i,7));
                    tLJAGetSchema.setBankAccNo(tLBContSet.get(1).getBankAccNo());
                    tLJAGetSchema.setBankCode(tLBContSet.get(1).getBankCode());
                    tLJAGetSchema.setDrawer(tLBContSet.get(1).getAppntName());
                    tLJAGetSchema.setDrawerID(tLBContSet.get(1).getAppntIDNo());
          	  }
    		}else{
    			tSQL="select * from lcgrpcont where grpcontno='"
          		  +tSSRS.GetText(i,2)+"'";
          	  tLCGrpContSet=tLCGrpContDB.executeQuery(tSQL);
          	  if(tLCGrpContSet.size()>0){
          		  tLJAGetSchema.setOtherNo(tSSRS.GetText(i,4));
              	  tLJAGetSchema.setPayMode(tLCGrpContSet.get(1).getPayMode());
                    tLJAGetSchema.setManageCom(tSSRS.GetText(i,1));
                    tLJAGetSchema.setAgentCom("");
                    tLJAGetSchema.setAgentType(tLCGrpContSet.get(1).getAgentType());
                    tLJAGetSchema.setAgentCode(tSSRS.GetText(i,5));
                    tLJAGetSchema.setAgentGroup(tSSRS.GetText(i,6));
                    tLJAGetSchema.setAppntNo(tLCGrpContSet.get(1).getAppntNo());
                    tLJAGetSchema.setAccName(tLCGrpContSet.get(1).getAccName());
                    tLJAGetSchema.setSumGetMoney(tSSRS.GetText(i,7));
                    tLJAGetSchema.setBankAccNo(tLCGrpContSet.get(1).getBankAccNo());
                    tLJAGetSchema.setBankCode(tLCGrpContSet.get(1).getBankCode());
                    tLJAGetSchema.setDrawer(tLCGrpContSet.get(1).getGrpName());
          	  }else{
          		  tSQL="select * from lbgrpcont where grpcontno='"
              		  +tSSRS.GetText(i,2)+"'";
              	  tLBGrpContSet=tLBGrpContDB.executeQuery(tSQL);
              	  tLJAGetSchema.setOtherNo(tSSRS.GetText(i,4));
              	  tLJAGetSchema.setPayMode(tLBGrpContSet.get(1).getPayMode());
                    tLJAGetSchema.setManageCom(tSSRS.GetText(i,1));
                    tLJAGetSchema.setAgentCom("");
                    tLJAGetSchema.setAgentType(tLBGrpContSet.get(1).getAgentType());
                    tLJAGetSchema.setAgentCode(tSSRS.GetText(i,5));
                    tLJAGetSchema.setAgentGroup(tSSRS.GetText(i,6));
                    tLJAGetSchema.setAppntNo(tLBGrpContSet.get(1).getAppntNo());
                    tLJAGetSchema.setAccName(tLBGrpContSet.get(1).getAccName());
                    tLJAGetSchema.setSumGetMoney(tSSRS.GetText(i,7));
                    tLJAGetSchema.setBankAccNo(tLBGrpContSet.get(1).getBankAccNo());
                    tLJAGetSchema.setBankCode(tLBGrpContSet.get(1).getBankCode());
                    tLJAGetSchema.setDrawer(tLBGrpContSet.get(1).getGrpName());
          	  }
    		}
    		tLJAGetSchema.setOperator(this.mGlobalInput.Operator);
            tLJAGetSchema.setMakeDate(currentDate);
            tLJAGetSchema.setMakeTime(currentTime);
            tLJAGetSchema.setModifyDate(currentDate);
            tLJAGetSchema.setModifyTime(currentTime);
            this.mLJAGetSet.add(tLJAGetSchema);
    	}
    	mmMap.put(this.mLJAGetSet, "INSERT");
    	return true;
    }
    private boolean createPayableData(){
    	String sql = " select * from  lawage  where state ='0'  and (lastmoney + CurrMoney) > 0 " +
    			" and not exists (select 'X' from labranchgroup where branchattr = lawage.branchattr " +
    			" and branchtype = '1' and state = '1' and branchlevel = '01') and indexcalno = '"+mLAWageSchema.getIndexCalNo()+"'" +
    			" and managecom like '"+mLAWageSchema.getManageCom()+"%' and managecom like '"+mGlobalInput.ManageCom+"%' and " +
    			" BranchType = '"+mLAWageSchema.getBranchType()+"'and BranchType2 = '"+mLAWageSchema.getBranchType2()+"' ";
//      if (mLAWageSchema.getAgentCode() != null &&!mLAWageSchema.getAgentCode().trim().equals("")) {
//      sql = sql + " and agentcode='" + mLAWageSchema.getAgentCode() +"'";
//  }
//  if (mLAAgentSchema.getName() != null &&!mLAAgentSchema.getName().trim().equals("")) {
//      if (!checkname()) {
//          return false;
//      }
//      sql = sql + " and exists ( select a.agentcode from laagent a  where a.agentcode=lawage.agentcode and a.name='" +
//            mLAAgentSchema.getName().trim() + "') ";
//  }
//
//  if (mLAWageSchema.getAgentGroup() != null &&!mLAWageSchema.getAgentGroup().equals("")) {
//      if (!checkagentgroup()) {
//          return false;
//      }
//      sql = sql + " and branchattr like '" +
//            mLAWageSchema.getAgentGroup() +
//            "%' ";
//  }
    	
    	LAWageDB tLAWageDB = new LAWageDB();
    	LAAgentDB tLAAgentDB = new LAAgentDB();
    	LAWageSet tLAWageSet = new LAWageSet();
    	LAAgentSet tLAAgentSet = new LAAgentSet();
    	SSRS tSSRS = new SSRS();
    	System.out.println("查询语句："+sql);
    	tLAWageSet =(LAWageSet)tLAWageDB.executeQuery(sql);
    	if(tLAWageSet.size()>0){
    		for (int i = 1; i <= tLAWageSet.size(); i++) {
				LJAGetSchema tLJAGetSchema = new LJAGetSchema();
				LAWageSchema tLAWageSchema = new LAWageSchema();
				LAAgentSchema tLAAgentSchema = new LAAgentSchema();
				tLAWageSchema = tLAWageSet.get(i);
				String tLimit = PubFun.getNoLimit(tLAWageSchema.getManageCom());
	            tLJAGetSchema.setActuGetNo(PubFun1.CreateMaxNo("BCGETNO", tLimit));
	            tLJAGetSchema.setOtherNo(tLAWageSchema.getAgentCode().trim()+tLAWageSchema.getIndexCalNo().trim());
	            tLJAGetSchema.setGetNoticeNo(tLAWageSchema.getAgentCode());
	            tLJAGetSchema.setPayMode("4");
	            tLJAGetSchema.setShouldDate(currentDate);
	            tLJAGetSchema.setAgentGroup(tLAWageSchema.getAgentGroup());
	            tLJAGetSchema.setOtherNoType("24");
	            tLJAGetSchema.setManageCom(tLAWageSchema.getManageCom());
	            tLJAGetSchema.setAgentCode(tLAWageSchema.getAgentCode());
	            tLJAGetSchema.setSumGetMoney(tLAWageSchema.getSumMoney());
	            tLAAgentDB.setAgentCode(tLAWageSchema.getAgentCode());
	            tLAAgentSchema = tLAAgentDB.getSchema();
	            String tSql ="select * from laaccounts where agentcode ='"+tLAAgentSchema.getAgentCode()+"' and state ='0' with ur ";
	            LAAccountsSchema tLAAccountsSchema = new LAAccountsSchema();
	            LAAccountsDB tLAAccountsDB = new LAAccountsDB();
	            tLAAccountsSchema = tLAAccountsDB.executeQuery(tSql).get(1);
	            tLJAGetSchema.setBankAccNo(tLAAccountsSchema.getAccount());
	            //添加对accountNo的判断
//	            tSSRS =  getLAaccountResult(tLAAgentSchema);
	            tLJAGetSchema.setAccName(tLAAccountsSchema.getAccountName());
	            tLJAGetSchema.setBankCode(tLAAccountsSchema.getBankCode());
	            //添加领取人相关信息
	            tLJAGetSchema.setDrawer(tLAAgentSchema.getName());
//	            tLJAGetSchema.setDrawerID(tLAAgentSchema.getIDNo());
	            tLJAGetSchema.setOperator(this.mGlobalInput.Operator);
	            tLJAGetSchema.setMakeDate(currentDate);
	            tLJAGetSchema.setMakeTime(currentTime);
	            tLJAGetSchema.setModifyDate(currentDate);
	            tLJAGetSchema.setModifyTime(currentTime);
	            this.mLJAGetSet.add(tLJAGetSchema); 
    		}
    	}
    	mmMap.put(this.mLJAGetSet, "INSERT");
    	return true;
    } 
    private SSRS getLAaccountResult(LAAgentSchema tLAAgentSchema){
    	 SSRS tSSRS = new SSRS();
         ExeSQL tExeSQL = new ExeSQL();
  	  String sql=" select AccountName,BankCode from laaccounts where agentcode='"+tLAAgentSchema.getAgentCode()+"' and account = '"+tLAAgentSchema.getBankAccNo()+"' with ur ";
  	 tSSRS = tExeSQL.execSQL(sql);
  	if (tExeSQL.mErrors.needDealError() == true) {
        // @@错误处理
        this.mErrors.copyAllErrors(tExeSQL.mErrors);
        CError tError = new CError();
        tError.moduleName = "LAWageDaBL";
        tError.functionName = "getLAaccountResult";
        tError.errorMessage = "个人帐户表查询失败!";
        this.mErrors.addOneError(tError);
        return null;
    }
    return tSSRS;
    }
    
   private boolean checkname()
    {

        if (mLAWageSchema.getAgentCode() != null &&
            !mLAWageSchema.getAgentCode().trim().equals(""))
        {
            LAAgentDB tLAAgentDB=new LAAgentDB();
            tLAAgentDB.setAgentCode(mLAWageSchema.getAgentCode());
            tLAAgentDB.setName(mLAAgentSchema.getName());
            tLAAgentDB.query();
            LAAgentSet tLAAgentSet=new LAAgentSet();
            tLAAgentSet=tLAAgentDB.query();
            if (tLAAgentDB.mErrors.needDealError())
             {
                 // @@错误处理
                 this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                 CError tError = new CError();
                 tError.moduleName = "ALAWageBL";
                 tError.functionName = "checkname";
                 tError.errorMessage = "没有查到业务员代码为"+mLAWageSchema.getAgentCode()+"且业务员为姓名为"+mLAAgentSchema.getName()+"的信息！！！";
                 this.mErrors.addOneError(tError);
                 return false;
          }
            System.out.println("123223"+tLAAgentSet);
            if(tLAAgentSet.size()<=0)

            {
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAWageBL";
                tError.functionName = "checkname";
                tError.errorMessage = "没有查到业务员代码为"+mLAWageSchema.getAgentCode()+"且业务员为姓名为"+mLAAgentSchema.getName()+"的信息！！！";
                this.mErrors.addOneError(tError);
                return false;
            }


        }
        return true ;
    }
    private boolean checkagentgroup()
    {
        if (mLAAgentSchema.getName() != null &&
           !mLAAgentSchema.getName().trim().equals(""))
       {
           String sql = "select 'X' from laagent  where name='" +
                        mLAAgentSchema.getName() + "' and branchtype='" +
                        mLAWageSchema.getBranchType() + "' and  branchtype2='" +
                        mLAWageSchema.getBranchType2() + "' and exists(select 'X' from labranchgroup a  where laagent.agentgroup=a.agentgroup and a.branchattr='"+mLAWageSchema.getAgentGroup()+"')";
           ExeSQL tExeSQL = new ExeSQL();
           SSRS tSSRS = new SSRS();
           tSSRS = tExeSQL.execSQL(sql);
           if (tExeSQL.mErrors.needDealError() || tSSRS.getMaxRow()<=0)
           {
               this.mErrors.copyAllErrors(tExeSQL.mErrors);
               CError tError = new CError();
               tError.moduleName = "AgentWageCalDoBL";
               tError.functionName = "checkname";
               tError.errorMessage = "没有查到业务员为姓名为" + mLAAgentSchema.getName() +
                                     "并且销售单位为"+mLAWageSchema.getAgentGroup()+"的信息！";
               this.mErrors.addOneError(tError);
               return false;
           }
       }
        if (mLAWageSchema.getAgentCode() != null &&
            !mLAWageSchema.getAgentCode().trim().equals(""))
        {
            String sql = "select 'X' from laagent  where agentcode='" +
                         mLAWageSchema.getAgentCode() + "' and branchtype='" +
                         mLAWageSchema.getBranchType() + "' and  branchtype2='" +
                         mLAWageSchema.getBranchType2() + "' and exists(select 'X' from labranchgroup a  where laagent.agentgroup=a.agentgroup and a.branchattr='"+mLAWageSchema.getAgentGroup()+"')";

           ExeSQL tExeSQL = new ExeSQL();
           SSRS tSSRS = new SSRS();
           tSSRS = tExeSQL.execSQL(sql);
           if (tExeSQL.mErrors.needDealError() || tSSRS.getMaxRow()<=0) {
               this.mErrors.copyAllErrors(tExeSQL.mErrors);
               CError tError = new CError();
               tError.moduleName = "AgentWageCalDoBL";
               tError.functionName = "checkname";
               tError.errorMessage = "没有查到业务员为代码为" + mLAWageSchema.getAgentCode() +
                                     "并且销售单位为"+mLAWageSchema.getAgentGroup()+"的信息！";
               this.mErrors.addOneError(tError);
               return false;
           }
        }
        return true;
    }

            /**
             * 从输入数据中得到所有对象
             *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
             */
            private boolean getInputData(VData cInputData)
            {
                this.mLAWageSchema.setSchema((LAWageSchema) cInputData.
                                             getObjectByObjectName("LAWageSchema", 0));
                this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                             getObjectByObjectName("LAAgentSchema", 0));
                this.mLAWageSet.set((LAWageSet) cInputData.
                                             getObjectByObjectName("LAWageSet", 0));
                this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                           getObjectByObjectName("GlobalInput", 0));
                //mflag=cInputData.get(4).toString();
                mflag="0";//此处设定为0，是为了使程序改动最小，为了对该管理机构下所有符合条件的人员一次性做审核发放
                mManageCom=mLAWageSchema.getManageCom();
                mBranchType=mLAWageSchema.getBranchType();
                mBranchType2=mLAWageSchema.getBranchType2();
                mIndexCalNo=mLAWageSchema.getIndexCalNo();
                return true;
            }



    private boolean prepareOutputData()
    {
        mInputData.clear();
        mInputData.add(mMap);
        return true;

    }
    private boolean prepareOutputDataDa()
    {
        mInputData.clear();
        mInputData.add(mmMap);
        return true;

    }


    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAWageDaBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

}
