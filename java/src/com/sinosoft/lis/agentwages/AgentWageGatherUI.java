package com.sinosoft.lis.agentwages;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author pengcheng
 * @version 1.0
 */
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class AgentWageGatherUI
{

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();


    public AgentWageGatherUI()
    {
    }

    public boolean submitData(VData cInputData)
    {
        mInputData = (VData) cInputData.clone();
        AgentWageGatherBL tAgentWageGatherBL = new AgentWageGatherBL();
        if (!tAgentWageGatherBL.submitData(mInputData))
        {
            this.mErrors.copyAllErrors(tAgentWageGatherBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LACalWageUI";
            tError.functionName = "submitData";
            tError.errorMessage = tAgentWageGatherBL.mErrors.getFirstError();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
