package com.sinosoft.lis.agentwages;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class AgentActiveWage1XLSUI
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    public AgentActiveWage1XLSUI()
    {
        System.out.println("AgentWage1XLSUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
       AgentActiveWage1XLSBL bl = new AgentActiveWage1XLSBL();
        if(!bl.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        AgentActiveWage1XLSUI tAgentWage1XLSUI = new   AgentActiveWage1XLSUI();
    }
}
