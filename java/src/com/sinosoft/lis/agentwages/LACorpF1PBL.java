package com.sinosoft.lis.agentwages;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LACorpF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

//取得的时间
    private String PolNo = null;
    private String WageNo = null;
    private String BeginDate = null;
    private String EndDate = null;
    private String BranchType = null;
    private String AgentCode = null;
    private String BranchAttr = null;
//输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
//业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public LACorpF1PBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        PolNo = ((String) cInputData.get(0));
        WageNo = ((String) cInputData.get(1));
        BeginDate = ((String) cInputData.get(2));
        EndDate = ((String) cInputData.get(3));
        BranchType = ((String) cInputData.get(4));
        AgentCode = ((String) cInputData.get(5));
        BranchAttr = ((String) cInputData.get(6));
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolBillF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {

        SSRS tSSRS = new SSRS();
        msql = "select a.GrpPolNo,d.RiskName,a.p11,a.SignDate,a.CValiDate,a.GetPolDate,a.TransMoney,a.AgentCode,c.Name,a.FYC "
               + "from LACommision a,LAAgent c,LmRiskApp d where a.AgentCode=c.AgentCode and a.RiskCode=d.RiskCode and  a.ManageCom like '" +
               mGlobalInput.ManageCom + "%'";
        if (BranchType.equals(""))
        {
            msql = msql + " and (a.BranchType='2' or a.BranchType='3') ";
        }
        else
        {
            msql = msql + " and a.BranchType='" + BranchType + "'";
        }
        if (!BranchAttr.equals(""))
        {
            msql = msql + " and BranchAttr like '" + BranchAttr + "%' ";
        }
        if (!WageNo.equals(""))
        {
            msql = msql + " and a.WageNo='" + WageNo + "'";
        }
        if (!PolNo.equals(""))
        {
            msql = msql + " and a.PolNo='" + PolNo + "'";
        }
        if (!AgentCode.equals(""))
        {
            msql = msql + " and a.AgentCode='" + AgentCode + "'";
        }
        if (!BeginDate.equals(""))
        {
            msql = msql + " and a.CalDate>='" + BeginDate + "'";
        }
        if (!EndDate.equals(""))
        {
            msql = msql + " and a.CalDate<='" + EndDate + "'";
        }
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);

        ListTable tlistTable = new ListTable();
        String strArr[] = null;
        tlistTable.setName("BANK");
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            strArr = new String[10];
            for (int j = 1; j <= tSSRS.getMaxCol(); j++)
            {
                strArr[j - 1] = tSSRS.GetText(i, j);
            }
            tlistTable.add(strArr);
        }
        strArr = new String[10];
        strArr[0] = "保单号";
        strArr[1] = "险种名称";
        strArr[2] = "投保人";
        strArr[3] = "签单日期";
        strArr[4] = "生效日期";
        strArr[5] = "回单日期";
        strArr[6] = "保费";
        strArr[7] = "代理人编码";
        strArr[8] = "姓名";
        strArr[9] = "提奖金额";
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("CorpDetail.vts", "printer"); //最好紧接着就初始化xml文档
        xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
        mResult.addElement(xmlexport);
        return true;
    }

    public static void main(String[] args)
    {
        LACorpF1PBL LACorpF1PBL1 = new LACorpF1PBL();
    }
}
