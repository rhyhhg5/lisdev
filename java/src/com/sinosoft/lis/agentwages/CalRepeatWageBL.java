package com.sinosoft.lis.agentwages;

import com.sinosoft.lis.agentcalculate.AgCalBase;
import com.sinosoft.lis.agentcalculate.CalIndex;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAssessIndexDB;
import com.sinosoft.lis.db.LAIndexInfoDB;
import com.sinosoft.lis.db.LAIndexVsCommDB;
import com.sinosoft.lis.db.LAStatSegmentDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LAWageDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAIndexInfoSchema;
import com.sinosoft.lis.schema.LAIndexVsCommSchema;
import com.sinosoft.lis.schema.LAStatSegmentSchema;
import com.sinosoft.lis.schema.LAWageSchema;
import com.sinosoft.lis.vschema.LAIndexInfoSet;
import com.sinosoft.lis.vschema.LAIndexVsCommSet;
import com.sinosoft.lis.vschema.LAStatSegmentSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */

public class CalRepeatWageBL
{
    // @Field
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mOperate = "";

//必须初始化的值
    private String ManageCom; //管理机构
    private String Year; //佣金计算年份
    private String Month; //佣金计算月份
    private String BranchType; //展业类型

    private String IndexCalNo; //佣金计算编码
    private CalIndex tCalIndex = new CalIndex(); //指标计算类

    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    private LAWageSet mLAWageSet = null;
    private LAWageSet aLAWageSet = new LAWageSet();
    private LAWageSchema mLAWageSchema = null;

    private LAIndexInfoSet aLAIndexInfoSet = new LAIndexInfoSet();
    private LAIndexInfoSchema mLAIndexInfoSchema = null;

    private String AgentCode; //代理人代码

//管理机构下的代理人存放集
    private LATreeSet mLATreeSet = new LATreeSet();
//存指标信息
    private LAAssessIndexDB mLAAssessIndexDB;
//职级对应的佣金存放处
    private String[][] GradeIndex;

    private double F25 = 0;

    public CalRepeatWageBL()
    {
    }

// @Method
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        Year = (String) cInputData.getObject(1);
        Month = (String) cInputData.getObject(2);
        ManageCom = (String) cInputData.getObject(3);
        BranchType = (String) cInputData.getObject(4);
        mLAWageSet = (LAWageSet) cInputData.getObjectByObjectName("LAWageSet",
                0);
        return true;
    }

    private boolean dealData()
    {
        LAWageSchema tLAWageSchema;
        LAWageSchema aLAWageSchema;
        LAWageDB tLAWageDB;
        LAIndexInfoSchema aLAIndexInfoSchema;
        LAIndexInfoDB tLAIndexInfoDB;
        for (int i = 1; i <= mLAWageSet.size(); i++)
        {
            tLAWageSchema = mLAWageSet.get(i);
            tLAWageDB = new LAWageDB();
            tLAWageDB.setAgentCode(tLAWageSchema.getAgentCode());
            tLAWageDB.setIndexCalNo(tLAWageSchema.getIndexCalNo());
            if (!tLAWageDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "CalRepeatWageBL";
                tError.functionName = "dealData";
                tError.errorMessage = "数据处理失败LAWageDB-->getinfo!";
                this.mErrors.addOneError(tError);
                return false;
            }
            F25 = (tLAWageDB.getF23() + tLAWageDB.getF24() +
                   tLAWageDB.getLastMoney() + tLAWageDB.getF25()) -
                  (tLAWageSchema.getF23() + tLAWageSchema.getF24() +
                   tLAWageSchema.getLastMoney());
            tLAWageDB.setF23(tLAWageSchema.getF23());
            if (BranchType.equals("3"))
            {
                tLAWageDB.setF24(tLAWageSchema.getF24());
            }
            tLAWageDB.setLastMoney(tLAWageSchema.getLastMoney());
            tLAWageDB.setF25(F25);
            aLAWageSchema = new LAWageSchema();
            aLAWageSchema.setSchema(tLAWageDB);
            aLAWageSet.add(aLAWageSchema);

            tLAIndexInfoDB = new LAIndexInfoDB();
            tLAIndexInfoDB.setIndexCalNo(tLAWageSchema.getIndexCalNo());
            tLAIndexInfoDB.setIndexType("01");
            tLAIndexInfoDB.setAgentCode(tLAWageSchema.getAgentCode());
            System.out.println("indexcalnoo---" + tLAIndexInfoDB.getIndexCalNo());
            System.out.println("indextype---" + tLAIndexInfoDB.getIndexType());
            System.out.println("agentcode---" + tLAIndexInfoDB.getAgentCode());
            if (!tLAIndexInfoDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "CalRepeatWageBL";
                tError.functionName = "dealData";
                tError.errorMessage = "数据处理失败LAIndexInfoDB-->getinfo!";
                this.mErrors.addOneError(tError);
                return false;
            }
//      LAIndexInfoSet tLAIndexInfoSet = tLAIndexInfoDB.query();
//      tLAIndexInfoDB.setSchema(tLAIndexInfoSet.get(1));
            tLAIndexInfoDB.setT11(tLAWageSchema.getF23());
            if (BranchType.equals("3"))
            {
                tLAIndexInfoDB.setT17(tLAWageSchema.getF24());
            }
            tLAIndexInfoDB.setT14(tLAWageSchema.getLastMoney());
            tLAIndexInfoDB.setT58(F25);
            aLAIndexInfoSchema = new LAIndexInfoSchema();
            aLAIndexInfoSchema.setSchema(tLAIndexInfoDB);
            aLAIndexInfoSet.add(aLAIndexInfoSchema);
        }
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(this.aLAWageSet);
            this.mInputData.add(this.aLAIndexInfoSet);
            this.mResult.clear();
            this.mResult.add(this.aLAWageSet);
            this.mResult.add(this.aLAIndexInfoSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalRepeatWageBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public boolean submitData(VData cInputData)
    {
        try
        {
            if (!getInputData(cInputData))
            {
                return false;
            }
            if (!dealData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "CalRepeatWageBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败CalRepeatWageBL-->dealData!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (!prepareOutputData())
            {
                return false;
            }
            System.out.println("Start LAWageBL Submit...");
            LAWageBLS tLAWageBLS = new LAWageBLS();
            tLAWageBLS.submitData(mInputData, "UPDATE");
            System.out.println("End LAWageBL Submit...");
            //如果有需要处理的错误，则返回
            if (tLAWageBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAWageBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalRepeatWageBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            System.out.println("开始计算ManageCom:" + ManageCom);
            if (!Calculate())
            {
                return false;
            }
            return true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", ex.toString());
            return false;
        }
    }


    /** 计算佣金 */
    private boolean Calculate()
    {
        String tSQL = "", tReturn = "", tIndexType = "";
        int tCount = 0, iMax = 0, iPos = 0;
        String tGrade = "";
        boolean tExist = false, tNew = false;

        //检验管理机构代码有效否
        if (!checkCalculate())
        {
            return false;
        }
        IndexCalNo = Year + Month; //生成佣金计算编码
        System.out.println("生成佣金计算编码1：" + IndexCalNo);
        //计算日期的起止期(没用上)
        if (!setBeginEnd())
        {
            return false;
        }
        //查询指标类型
        tSQL =
                "select distinct CalPrpty from LAAssessIndex order by CalPrpty asc";
        ExeSQL tExeSQL = new ExeSQL();
        tReturn = tExeSQL.getEncodedResult(tSQL, 0);
        if (tReturn.indexOf("|") != 1)
        {
            System.out.println("指标定义表中的指标类型字段无值！");
            CError tError = new CError();
            tError.moduleName = "CalBankWageBL";
            tError.functionName = "Calculate";
            tError.errorMessage = "指标定义表中的指标类型字段无值!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("查询结果：" + tReturn);
        tCount = Integer.parseInt(tReturn.substring(tReturn.indexOf('|') + 1,
                tReturn.indexOf('|') + 2));
        tReturn = tReturn.substring(tReturn.indexOf(SysConst.RECORDSPLITER) + 1);

        //查询管理机构下的代理人
//    LATreeDB tLATreeDB = new LATreeDB();
//    tSQL ="select * from LATree where ManageCom='"+ManageCom.trim()+"' and (AgentGrade like 'D%' or AgentGrade like 'E%') order by AgentGrade,AgentCode asc";
//    System.out.println("查询管理机构下的代理人sql: " + tSQL);
//    mLATreeSet = tLATreeDB.executeQuery(tSQL);
//    LATreeSchema tLATreeSchema = new LATreeSchema();
//    iMax = mLATreeSet.size();
        LAWageSchema tLAWageSchema = new LAWageSchema();
        LATreeDB tLATreeDB = null;
        iMax = aLAWageSet.size();
        System.out.println("------------开始按指标类型循环" + tReturn);
        for (int i = 0; i < tCount; i++)
        {
            tIndexType = PubFun.getStr(tReturn, i + 1, SysConst.RECORDSPLITER);
            System.out.println(String.valueOf(i) + "--指标类型：" + tIndexType);
            if (tIndexType.equals("") || tIndexType == null)
            {
                continue;
            }
            //计算管理机构下所有代理人该指标类型的指标值
            System.out.println("---------代理人循环计算该指标类型的指标值");
            for (int j = 1; j <= iMax; j++)
            {
                tExist = false;
                //tLATreeSchema = mLATreeSet.get(j);
                tLAWageSchema = aLAWageSet.get(j);
                //AgentCode = tLATreeSchema.getAgentCode();
                AgentCode = tLAWageSchema.getAgentCode();
                System.out.println(String.valueOf(j) + "------代理人编码：" +
                                   AgentCode);
                //tGrade = tLATreeSchema.getAgentGrade();
                tLATreeDB = new LATreeDB();
                tLATreeDB.setAgentCode(tLAWageSchema.getAgentCode());
                if (!tLATreeDB.getInfo())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "CalRepeatWageBL";
                    tError.functionName = "Calculate";
                    tError.errorMessage = "数据处理失败LATreeDB-->getinfo!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                tGrade = tLATreeDB.getAgentGrade();
                System.out.println(" ------代理人职级：" + tGrade);
                //tCalIndex.tAgCalBase.setAgentGroup(tLATreeSchema.getAgentGroup());
                tCalIndex.tAgCalBase.setAgentGroup(tLAWageSchema.getAgentGroup());
                //数组中存在该职级否
                if (!tNew)
                {
                    System.out.println("       存储第一个职级佣金编码");
                    GradeIndex = new String[1][];
                    System.out.println("length:" +
                                       String.valueOf(GradeIndex.length));
                    keepGrade(tGrade, 0);
                    tNew = true;
                    iPos = 0;
                }
                else
                {
                    for (int k = 0; k < GradeIndex.length; k++)
                    {
                        if (GradeIndex[k][0].equals(tGrade.trim()))
                        {
                            System.out.println("    职级数组中已存在该职级");
                            tExist = true;
                            iPos = k;
                            break;
                        }
                    }
                    //职级对应的佣金代码集加入数组中
                    if (!tExist)
                    {
                        String[][] temp = GradeIndex;
                        GradeIndex = new String[GradeIndex.length + 1][];
                        for (int jj = 0; jj < temp.length; jj++)
                        {
                            GradeIndex[jj] = temp[jj];
                        }
                        System.out.println("存入职级数组中职级个数:" +
                                           String.valueOf(GradeIndex.length));
                        keepGrade(tGrade, GradeIndex.length - 1);
                        iPos = GradeIndex.length - 1;
                    }
                }
                System.out.println("在职级数组中的位置iPos:" + String.valueOf(iPos));
                //循环计算所有佣金指标值
                System.out.println();
                System.out.println("/-----------------开始循环计算该职级对应的佣金");

                if (!WageCal(iPos, AgentCode, tIndexType))
                {
                    System.out.println("Error:" + this.mErrors.getFirstError());
                    return false;
                }
                System.out.println();
                System.out.println(
                        "************************************************************");
                System.out.println();
            }
        } //end of 指标类型for
        return true;
    }

    /**
     *循环计算一职级对应的所有佣金
     * @return boolean 计算出错返回false
     **/
    private boolean WageCal(int cPos, String cAgentCode, String cIndexType)
    {
        String tReturn = "";
        System.out.println("佣金总数－－GradeIndex.length =" +
                           GradeIndex[cPos].length);
        for (int i = 1; i < GradeIndex[cPos].length; i++)
        {
            System.out.println(String.valueOf(i) + "--佣金编码：" +
                               GradeIndex[cPos][i]);
            //查询判断佣金对应的指标是否已存在于表中
            System.out.println("查询判断佣金对应的指标是否已存在于表中");
            if (indexExist(cAgentCode, GradeIndex[cPos][i], GradeIndex[cPos][0]))
            {
                System.out.println("--该佣金对应的指标值已存在与表中");
//        continue;
            }
            System.out.println("mLAAssessIndexDB.getCalPrpty = " +
                               mLAAssessIndexDB.getCalPrpty());
            if (!mLAAssessIndexDB.getCalPrpty().trim().equals(cIndexType))
            {
                System.out.println("指标类型不符，跳过");
                continue;
            }
            //指标计算
            System.out.println("--该佣金对应的指标值表中没有，开始计算。。。");
            //tCalIndex = new CalIndex();
            tCalIndex.setAssessIndex(mLAAssessIndexDB);
            tCalIndex.setAgentCode(cAgentCode);
            tCalIndex.setIndexCalNo(IndexCalNo);
            tCalIndex.setOperator(mGlobalInput.Operator);
            /* 设置基本参数 */
            //tCalIndex.tAgCalBase.setAgentCode(cAgentCode);
            //tCalIndex.tAgCalBase.setWageNo(IndexCalNo);
            //tCalIndex.tAgCalBase.setWageCode(GradeIndex[cPos][i]);
            //tCalIndex.tAgCalBase.setAgentGrade(GradeIndex[cPos][0]);
            if (!setBaseValue(tCalIndex.tAgCalBase, cAgentCode,
                              GradeIndex[cPos][i], GradeIndex[cPos][0]))
            {
                return false;
            }
            tReturn = tCalIndex.Calculate();
            //tjj chg 0411
            if (StrTool.cTrim(tReturn).equals(""))
            {
                tReturn = "0";
            }
            if (tCalIndex.mErrors.needDealError())
            {
                System.out.println("Error:" + this.mErrors.getFirstError());
                // @@错误处理
                this.mErrors.copyAllErrors(tCalIndex.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalWage";
                tError.functionName = "WageCal";
                tError.errorMessage = "佣金计算出错。";
                this.mErrors.addOneError(tError);
                return false;
            }
            else
            {
                if (!insertWage(IndexCalNo, AgentCode, GradeIndex[cPos][0],
                                GradeIndex[cPos][i], tReturn))
                {
                    System.out.println("把插入佣金表出错！！");
                    return false;
                }
            }
            System.out.println("结果：" + tReturn);
            System.out.println();
            System.out.println(
                    "---------------------------------------------------------");
            System.out.println();
        }
        return true;
    }

    /**
     *判断指标在表中是否已存在
     * 参数：代理人编码，佣金编码，代理人职级
     */
    private boolean indexExist(String cAgentCode, String cWageCode,
                               String cAgentGrade)
    {
        System.out.println("----在佣金指标对应表中，取出指标值存放的表名和字段名");
        LAIndexVsCommDB tLAIndexVsCommDB = new LAIndexVsCommDB();
        tLAIndexVsCommDB.setAgentGrade(cAgentGrade);
        tLAIndexVsCommDB.setWageCode(cWageCode);
        if (!tLAIndexVsCommDB.getInfo())
        {
            //@错误处理
            this.mErrors.copyAllErrors(tLAIndexVsCommDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "CalBankWageBL";
            tError.functionName = "indexExist";
            tError.errorMessage = "查询佣金指标对应的表名字段名出错!";
            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("--表中不存在该指标值，从指标表中查询指标信息用于后续的指标计算");
        mLAAssessIndexDB = new LAAssessIndexDB();
        mLAAssessIndexDB.setIndexCode(tLAIndexVsCommDB.getIndexCode());
        if (!mLAAssessIndexDB.getInfo())
        {
            //@错误处理
            this.mErrors.copyAllErrors(mLAAssessIndexDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "CalBankWageBL";
            tError.functionName = "indexExist";
            tError.errorMessage = "查询指标信息出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("   ----算法编码：" + mLAAssessIndexDB.getCalCode());
        return false;

    }


    private boolean insertWage(String cIndexCalNo, String cAgentCode,
                               String cGrade, String cWageCode,
                               String cIndexValue)
    {
        String tSQL = "", tCount = "";
        String TableName = "", IColName = "";
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        boolean tReturn = false;
        //查询佣金对应的表名和字段名
        LAIndexVsCommDB tLAIndexVsCommDB = new LAIndexVsCommDB();
        tLAIndexVsCommDB.setAgentGrade(cGrade);
        tLAIndexVsCommDB.setWageCode(cWageCode);
        tLAIndexVsCommDB.getInfo();
        TableName = tLAIndexVsCommDB.getCTableName();
        IColName = tLAIndexVsCommDB.getCColName();
        ExeSQL tExeSQL = new ExeSQL();
        //先判断记录是否已存在
        tSQL = "select AgentCode from " + TableName + " where IndexCalNo = '" +
               cIndexCalNo + "' and AgentCode = '" + cAgentCode + "'";
        System.out.println("插入最终佣金表的SQL:  " + tSQL);
        tCount = tExeSQL.getOneValue(tSQL);
        System.out.println("该代理人的最终佣金记录是否已存在：" + tCount + "aa");
        if (tCount.trim().equals(""))
        {
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(cAgentCode);
            if (!tLAAgentDB.getInfo())
            {
                //@错误处理
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalWage";
                tError.functionName = "insertIndex";
                tError.errorMessage = "代理人代码非法!" + cAgentCode.trim();
                this.mErrors.addOneError(tError);
                return false;
            }
            tSQL = "insert into " + TableName +
                   "( IndexCalNo,AgentCode,AgentGroup,ManageCom,BranchType," +
                   IColName +
                   ",Operator,Operator2,makeDate,makeTime,modifyDate,modifyTime)"
                   + " values('" + cIndexCalNo + "','" + cAgentCode + "','" +
                   tLAAgentDB.getAgentGroup() + "','"
                   + tLAAgentDB.getManageCom() + "','" + BranchType + "','" +
                   cIndexValue + "','" + mGlobalInput.Operator + "','" +
                   mGlobalInput.Operator + "','" + currentDate + "','" +
                   currentTime + "','" + currentDate + "','" + currentTime +
                   "')";
            System.out.println("tjjinsert插入记录的SQL: " + tSQL);
            tExeSQL = new ExeSQL();
            tReturn = tExeSQL.execUpdateSQL(tSQL);
            if (tExeSQL.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalBankWageBL";
                tError.functionName = "insertIndex";
                tError.errorMessage = "执行SQL语句：判断记录是否存在失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            return tReturn;
        }
        else
        {
            tSQL = "update " + TableName + " set " + IColName + "='" +
                   cIndexValue + "',modifyDate = '" + currentDate +
                   "',modifyTime = '" + currentTime + "'"
                   + " where IndexCalNo = '" + cIndexCalNo +
                   "' and AgentCode = '" + cAgentCode + "'";
            System.out.println("tjjupdate更新记录的SQL: " + tSQL);
            tExeSQL = new ExeSQL();
            tReturn = tExeSQL.execUpdateSQL(tSQL);
            if (tExeSQL.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalBankWageBL";
                tError.functionName = "insertIndex";
                tError.errorMessage = "执行SQL语句：向指标表中插入记录失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            return tReturn;
        }
    }

//给基数类赋值
    private boolean setBaseValue(AgCalBase cAgCalBase, String cAgentCode,
                                 String cWageCode, String cAgentGrade)
    {
        cAgCalBase.setAgentCode(cAgentCode);
        cAgCalBase.setWageNo(IndexCalNo);
        cAgCalBase.setWageCode(cWageCode);
        cAgCalBase.setAgentGrade(cAgentGrade);

        return true;
    }

    private boolean setBeginEnd()
    {
        //从数据库中查出时间区间
        LAStatSegmentSchema tLAStatSegmentSchema;
        LAStatSegmentSet tLAStatSegmentSet;
        LAStatSegmentDB tLAStatSegmentDB = new LAStatSegmentDB();
        //tLAStatSegmentDB.setYearMonth(Integer.parseInt(this.IndexCalNo));
        String tSQL = "select * from LAStatSegment where YearMonth = " +
                      this.IndexCalNo
                      + " order by stattype";
        tLAStatSegmentSet = tLAStatSegmentDB.executeQuery(tSQL);
        if (tLAStatSegmentDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError tCError = new CError();
            tCError.moduleName = "CalBankWageBL";
            tCError.functionName = "setBeginEnd()";
            tCError.errorMessage = "查询时间区间出错！";
            this.mErrors.addOneError(tCError);
            return false;
        }
        int tCount = tLAStatSegmentSet.size();
        System.out.println("tCount:" + tCount + "," + this.IndexCalNo);
        if (tCount < 1)
        {
            CError tCError = new CError();
            tCError.moduleName = "CalBankWage";
            tCError.functionName = "setBeginEnd()";
            tCError.errorMessage = "所输年月有问题，无法查到时间区间";
            this.mErrors.addOneError(tCError);
            return false;
        }
        String tStatType = "";
        for (int i = 1; i <= tCount; i++)
        {
            tLAStatSegmentSchema = tLAStatSegmentSet.get(i);
            tStatType = tLAStatSegmentSchema.getStatType().trim();
            System.out.println("-----------------stattype;" + tStatType);
            System.out.println(
                    "-----------------tCalIndex.tAgCalBase.getHalfYearMark();" +
                    tCalIndex.tAgCalBase.getHalfYearMark());
            System.out.println(
                    "-----------------tCalIndex.tAgCalBase.getYearMark();" +
                    tCalIndex.tAgCalBase.getYearMark());
            System.out.println(
                    "-----------------tCalIndex.tAgCalBase.getQuauterMark();" +
                    tCalIndex.tAgCalBase.getQuauterMark());
            if (tStatType.equals("1"))
            {
                tCalIndex.tAgCalBase.setMonthMark("1");
                tCalIndex.tAgCalBase.setMonthBegin(tLAStatSegmentSchema.
                        getStartDate());
                tCalIndex.tAgCalBase.setMonthEnd(tLAStatSegmentSchema.
                                                 getEndDate());
                tCalIndex.tAgCalBase.setQuauterBegin(tLAStatSegmentSchema.
                        getStartDate());
                tCalIndex.tAgCalBase.setQuauterEnd(tLAStatSegmentSchema.
                        getEndDate());
                tCalIndex.tAgCalBase.setHalfYearBegin(tLAStatSegmentSchema.
                        getStartDate());
                tCalIndex.tAgCalBase.setHalfYearEnd(tLAStatSegmentSchema.
                        getEndDate());
                tCalIndex.tAgCalBase.setYearBegin(tLAStatSegmentSchema.
                                                  getStartDate());
                tCalIndex.tAgCalBase.setYearEnd(tLAStatSegmentSchema.getEndDate());
                continue;
            }
            if (tStatType.equals("2"))
            {
                tCalIndex.tAgCalBase.setQuauterMark("1");
                tCalIndex.tAgCalBase.setQuauterBegin(tLAStatSegmentSchema.
                        getStartDate());
                tCalIndex.tAgCalBase.setQuauterEnd(tLAStatSegmentSchema.
                        getEndDate());
                tCalIndex.tAgCalBase.setHalfYearBegin(tLAStatSegmentSchema.
                        getStartDate());
                tCalIndex.tAgCalBase.setHalfYearEnd(tLAStatSegmentSchema.
                        getEndDate());
                tCalIndex.tAgCalBase.setYearBegin(tLAStatSegmentSchema.
                                                  getStartDate());
                tCalIndex.tAgCalBase.setYearEnd(tLAStatSegmentSchema.getEndDate());
                continue;
            }
            if (tStatType.equals("3"))
            {
                tCalIndex.tAgCalBase.setHalfYearMark("1");
                tCalIndex.tAgCalBase.setHalfYearBegin(tLAStatSegmentSchema.
                        getStartDate());
                tCalIndex.tAgCalBase.setHalfYearEnd(tLAStatSegmentSchema.
                        getEndDate());
                tCalIndex.tAgCalBase.setYearBegin(tLAStatSegmentSchema.
                                                  getStartDate());
                tCalIndex.tAgCalBase.setYearEnd(tLAStatSegmentSchema.getEndDate());
                continue;
            }
            if (tStatType.equals("4"))
            {
                tCalIndex.tAgCalBase.setYearMark("1");
                tCalIndex.tAgCalBase.setYearBegin(tLAStatSegmentSchema.
                                                  getStartDate());
                tCalIndex.tAgCalBase.setYearEnd(tLAStatSegmentSchema.getEndDate());
                continue;
            }
        }
        tCalIndex.tAgCalBase.setTempBegin(tCalIndex.tAgCalBase.getMonthBegin());
        tCalIndex.tAgCalBase.setTempEnd(tCalIndex.tAgCalBase.getMonthEnd());
        return true;
    }

    /**
     * 校验管理机构的值是否存在
     * @return boolean 如果不正确返回false
     */
    private boolean checkCalculate()
    {
        if (ManageCom == null || ManageCom.equals(""))
        {
            // @@错误处理
            buildError("checkCalculate", "计算时必须有管理机构编码！");
            return false;
        }
        return true;
    }


    /**
     *职级对应的佣金代码存入数组中
     */
    private void keepGrade(String cGrade, int i)
    {
        int iSize = 0;
        LAIndexVsCommSet tLAIndexVsCommSet = new LAIndexVsCommSet();
        LAIndexVsCommDB tLAIndexVsCommDB = new LAIndexVsCommDB();
        tLAIndexVsCommDB.setAgentGrade(cGrade);
        tLAIndexVsCommDB.setPrintFlag("1");
        tLAIndexVsCommSet = tLAIndexVsCommDB.query(); //职级对应的佣金代码集
        iSize = tLAIndexVsCommSet.size();
        GradeIndex[i] = new String[iSize + 1];
        GradeIndex[i][0] = cGrade;
        System.out.println("个数：" + String.valueOf(iSize) + " G:" +
                           GradeIndex[i][0]);
        LAIndexVsCommSchema tLAIndexVsCommSchema = new LAIndexVsCommSchema();
        for (int ii = 1; ii <= iSize; ii++)
        {
            tLAIndexVsCommSchema = tLAIndexVsCommSet.get(ii);
            GradeIndex[i][ii] = tLAIndexVsCommSchema.getWageCode();
            System.out.println("GradeIndex[" + i + "][" + ii + "] = " +
                               GradeIndex[i][ii]); ;
        }
    }


    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CalBankWageBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        LAWageSet tLAWageSet = new LAWageSet();
        LAWageSchema tLAWageSchema = new LAWageSchema();
        tLAWageSchema.setIndexCalNo("200403");
        tLAWageSchema.setAgentCode("8611000506"); //代理人代码
        tLAWageSchema.setF23(3000);
        tLAWageSchema.setF24(100);
        tLAWageSchema.setLastMoney(200);
        tLAWageSet.add(tLAWageSchema);

        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.ManageCom = "86";

        VData tVData = new VData();
        tVData.addElement(tGI);
        tVData.addElement("2004");
        tVData.addElement("03");
        tVData.add("8611");
        tVData.addElement("3");
        tVData.addElement(tLAWageSet);

        CalRepeatWageBL mCalRepeatWageBL = new CalRepeatWageBL();
        mCalRepeatWageBL.submitData(tVData);
    }
}
