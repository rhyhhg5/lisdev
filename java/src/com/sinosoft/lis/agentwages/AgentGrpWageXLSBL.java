package com.sinosoft.lis.agentwages;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.*;

public class AgentGrpWageXLSBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    //private String mtype = null;
    private String mAgentType = null;
    private String mBranchType2 = null;
    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    public AgentGrpWageXLSBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        SSRS tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "AgentWage1XLSBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 2][40];
      if(mBranchType2.equals("02")){ 
	       mToExcel[0][0] = "团险薪资计算结果";
	       mToExcel[0][10] = "团险薪资计算结果";
	       mToExcel[1][0] = "业务员代码";
	       mToExcel[1][1] = "业务员姓名";
	       mToExcel[1][2] = "业务员销售单位";
	       mToExcel[1][3] = "管理机构";
	       if(mAgentType.equals("1")){
	       	mToExcel[1][4] = "基本收入";
	       	mToExcel[1][5] = "业务提奖";
	        mToExcel[1][7] = "提奖加款";
	        mToExcel[1][8] = "提奖扣款";
	       }else{
	       	mToExcel[1][4] = "固定工资";
	       	mToExcel[1][5] = "绩效工资";
	        mToExcel[1][7] = "绩效加款";
	        mToExcel[1][8] = "绩效扣款";
	       }
	       mToExcel[1][6] = "管理津贴";
	       mToExcel[1][9] = "其它加款";
	       mToExcel[1][10] = "其他扣款";
	       mToExcel[1][11] = "交叉销售佣金";
	       mToExcel[1][12] = "上次薪资余额";
	       mToExcel[1][13] = "本期应发薪资";
	       mToExcel[1][14] = "本期实发薪资";
      }else{
	       mToExcel[0][0] = "团险薪资计算结果";
	       mToExcel[0][10] = "团险薪资计算结果";
	       mToExcel[1][0] = "业务员代码";
	       mToExcel[1][1] = "业务员姓名";
	       mToExcel[1][2] = "业务员销售单位";
	       mToExcel[1][3] = "管理机构";
	       if(mAgentType.equals("1")){
	       	mToExcel[1][4] = "基本收入";
	       	mToExcel[1][5] = "业务提奖";
	        mToExcel[1][7] = "提奖加款";
	        mToExcel[1][8] = "提奖扣款";
	        mToExcel[1][11] = "提奖预扣";
	        mToExcel[1][12] = "提奖预扣补发";
	       }else{
	       	mToExcel[1][4] = "固定工资";
	       	mToExcel[1][5] = "绩效工资";
	        mToExcel[1][7] = "绩效加款";
	        mToExcel[1][8] = "绩效扣款";
	        mToExcel[1][11] = "绩效预扣";
	        mToExcel[1][12] = "绩效预扣补发";
	       }
	       mToExcel[1][6] = "管理津贴";
	       mToExcel[1][9] = "其它加款";
	       mToExcel[1][10] = "其他扣款";
	       mToExcel[1][13] = "交叉销售佣金";
	       mToExcel[1][14] = "上次薪资余额";
	       mToExcel[1][15] = "本期应发薪资";
	       mToExcel[1][16] = "本期实发薪资";
      }

        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row+1][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "AgentWage1XLSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        mAgentType = (String)tf.getValueByName("agentType");
        mBranchType2 = (String)tf.getValueByName("branchtype2");
       //mtype = (String) tf.getValueByName("Type");
        if(mSql == null || mOutXmlPath == null||mAgentType==null||mBranchType2==null)
        {
            CError tError = new CError();
            tError.moduleName = "AgentWage1XLSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {
        AgentWage1XLSBL AgentWage1XLSBL = new
            AgentWage1XLSBL();
    }
}
