package com.sinosoft.lis.agentwages;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author pengcheng
 * @version 1.0
 */
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class LAHealthWageDaInsuredUI
{

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();


    public LAHealthWageDaInsuredUI()
    {
    }

    public boolean submitData(VData cInputData)
    {
        mInputData = (VData) cInputData.clone();
        LAHealthWageDaInsuredBL tLAHealthWageDaInsuredBL = new LAHealthWageDaInsuredBL();
        if (!tLAHealthWageDaInsuredBL.submitData(mInputData))
        {
            this.mErrors.copyAllErrors(tLAHealthWageDaInsuredBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "tLAWageDaInsuredUI";
            tError.functionName = "submitData";
            tError.errorMessage = tLAHealthWageDaInsuredBL.mErrors.getFirstError();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
