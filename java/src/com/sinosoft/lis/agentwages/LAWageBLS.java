/*
 * <p>ClassName: LAWageBLS </p>
 * <p>Description: LAWageBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.agentwages;

import java.sql.Connection;

import com.sinosoft.lis.db.LAWageDB;
import com.sinosoft.lis.schema.LAWageSchema;
import com.sinosoft.lis.vdb.LAWageDBSet;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class LAWageBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    private LAWageSet mLAWageSet = new LAWageSet();
    public LAWageBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start LAWageBLS Submit...");
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            tReturn = saveLAWage(cInputData);
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            tReturn = deleteLAWage(cInputData);
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            tReturn = updateLAWage(cInputData);
        }
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End LAWageBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLAWage(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAWageDBSet tLAWageDBSet = new LAWageDBSet(conn);
            tLAWageDBSet.set((LAWageSet) mInputData.getObjectByObjectName(
                    "LAWageSet", 0));
            if (!tLAWageDBSet.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAWageDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAWageBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                return false;
            }
            conn.commit();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
        }
        try
        {
            conn.close();
        }
        catch (Exception e)
        {}
        ;
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean deleteLAWage(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAWageDB tLAWageDB = new LAWageDB(conn);
            tLAWageDB.setSchema((LAWageSchema) mInputData.getObjectByObjectName(
                    "LAWageSchema", 0));
            if (!tLAWageDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAWageDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAWageBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                return false;
            }
            conn.commit();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
        }
        try
        {
            conn.close();
        }
        catch (Exception e)
        {}
        ;
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean updateLAWage(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Update...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAWageBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 更新...");
            LAWageDBSet tLAWageDBSet = new LAWageDBSet(conn);
            tLAWageDBSet.set((LAWageSet) mInputData.getObjectByObjectName(
                    "LAWageSet", 0));
            if (!tLAWageDBSet.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAWageDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAWageBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                return false;
            }
            conn.commit();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
        }
        try
        {
            conn.close();
        }
        catch (Exception e)
        {}
        ;
        return tReturn;
    }
}
