/*
 * <p>ClassName: CalWageBaseBL </p>
 * <p>Description: CalWageBaseBL类文件,
 * <p>用于计算单个代理人的一系列的佣金项 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-11
 */

package com.sinosoft.lis.agentwages;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;

import com.sinosoft.lis.agentcalculate.AgCalBase;
import com.sinosoft.lis.agentcalculate.CalIndex;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAssessIndexDB;
import com.sinosoft.lis.db.LAIndexVsCommDB;
import com.sinosoft.lis.db.LAStatSegmentDB;
import com.sinosoft.lis.db.LAWageDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAAssessIndexSchema;
import com.sinosoft.lis.schema.LAIndexVsCommSchema;
import com.sinosoft.lis.schema.LAStatSegmentSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.schema.LAWageSchema;
import com.sinosoft.lis.vschema.LAAssessIndexSet;
import com.sinosoft.lis.vschema.LAIndexVsCommSet;
import com.sinosoft.lis.vschema.LAStatSegmentSet;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAWageActivityLogSchema;
import com.sinosoft.lis.vschema.LAWageActivityLogSet;
import com.sinosoft.lis.db.LAWageTempDB;
import com.sinosoft.lis.vschema.LAWageTempSet;
import com.sinosoft.utility.Reflections;

public class CalWageBaseBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 计算结果存储在mResult中*/
    private VData mResult = new VData();

    private VData mInputData = new VData();
    private VData mOutputData = new VData();
    private LAWageActivityLogSet mLAWageActivityLogSet = new LAWageActivityLogSet();
    private GlobalInput mGlobalInput = new GlobalInput();

    private LAIndexVsCommSet mLAIndexVsCommSet = new LAIndexVsCommSet();

    private LAIndexVsCommSchema mLAIndexVsCommSchema = new LAIndexVsCommSchema();

    private LAAssessIndexSchema mLAAssessIndexSchema = new LAAssessIndexSchema();

    private LAAssessIndexSet mLAAssessIndexSet = new LAAssessIndexSet();

    private CtrlCommDraw mCtrlCommDraw = new CtrlCommDraw();
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String mAgentCode = new String(); //代理人编码
    private String mAgentGrade = new String(); //职级，用于取得计算指标集
    private String mAgentGroup = new String();
    private String ManageCom = new String(); //管理机构
    private String mBranchAttr = new String(); //外部编码
    private String mBranchType = new String(); //展业类型
    private String mBranchType2 =new String();//渠道类型
    private String mBranchSeries=new String();//展业机构系列号
    private String WageNo = new String(); //佣金代码
    private String Operator = new String();
    private String mAreaType = new String(); //地区类型
    private String mWageVersion = new String(); //地区类型
    private String IndexCalNo = new String(); //计算编码
    private String WageCode = new String(); //佣金编码
    private String mBranchCode = new String(); //团队编码
    // add new 团队建设
    private String mGbuildFlag = "";
    private String mGbuildStartDate = "";

    private boolean hasQueriedWage = false;

    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LAWageSchema mLAWageSchema;
    //=new LAWageSchema();

    private LAWageSet mInLAWageSet = new LAWageSet(); //如果该代理人的当月佣金项已经存在于佣金表中，该Set用于保存需要更新的记录
    private LAWageSet mUpLAWageSet = new LAWageSet(); //如果该代理人的当月佣金项不存在于佣金表中，该Set用于保存需要添加的记录

    private CalIndex mCalIndex = new CalIndex(); //指标计算类

    private boolean exist = true; //boolean类型，用于记录该代理人的当月佣金项是否已经存在于LAWage表中

    public CalWageBaseBL()
    {
    }

    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!check())
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        if (!prepareOutputData())
        {
            return true;
        }
        CalWageBaseBLS tCalWageBaseBLS = new CalWageBaseBLS();
        if (!tCalWageBaseBLS.submitData(mOutputData))
        {
            this.mErrors.copyAllErrors(tCalWageBaseBLS.mErrors);
            buildError("submitData", tCalWageBaseBLS.mErrors.getFirstError());
            return false;
        }
        return true;

    }

    private boolean dealData()
    {
        if (!Calculate())
        {
            return false;
        }
        return true;
    }

    private boolean check()
    {
        return true;
    }

    /**
     * 得到从外界传入的参数
     * @param cInputData
     * @return
     */
    private boolean getInputData(VData cInputData)
    {
        try
        {
            TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData",0);
            mLAIndexVsCommSet = (LAIndexVsCommSet) tTransferData.getValueByName("LAIndexVsCommSet");
            //全局变量
            mGlobalInput = (GlobalInput) tTransferData.getValueByName("GlobalInput");
            if (mGlobalInput == null)
            {
                buildError("getInputData", "没有得到足够的信息！");
                return false;
            }
            IndexCalNo = (String) cInputData.getObject(0);
            ManageCom = (String) cInputData.getObject(1);
            mAreaType = (String) cInputData.getObject(2);
            mBranchType = (String) cInputData.getObject(3);
            mBranchType2= (String) cInputData.getObject(4);
            mLATreeSchema = (LATreeSchema) tTransferData.getValueByName("LATreeSchema");
            mAgentCode = mLATreeSchema.getAgentCode();
            // 个险比较特殊 ---
            mBranchAttr = AgentPubFun.getAgentBranchAttr(mAgentCode);//注意branchattr 取值字段 用的是branchcode对应labranchgroup agentgroup 字段
            //得到人员的 agentgroup
            mAgentGroup = mLATreeSchema.getAgentGroup();
            mWageVersion = mLATreeSchema.getWageVersion();
            mBranchCode = mLATreeSchema.getBranchCode();

            if(mBranchType.equals("2")&& mBranchType2.equals("01"))
            {
                mAgentGroup=AgentPubFun.getMonthAgentGroup(mAgentCode,IndexCalNo);
                mBranchAttr=AgentPubFun.getMonthBranchAttr(mAgentGroup);
            }
            //modify by Abigale  2008-03-05  194-199
            if(mBranchType.equals("3")&& mBranchType2.equals("01"))
            {
               mAgentGroup=AgentPubFun.getMonthAgentGroup(mAgentCode,IndexCalNo);
               mBranchAttr=AgentPubFun.getMonthBranchAttr(mAgentGroup);
            }

            Operator = mGlobalInput.Operator;
            mLAAssessIndexSet = (LAAssessIndexSet) tTransferData.getValueByName("LAAssessIndexSet");
            LABranchGroupDB tLABranchGroupDB=new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(mLATreeSchema.getBranchCode() ) ;
            tLABranchGroupDB.getInfo() ;
            mBranchSeries=tLABranchGroupDB.getBranchSeries();
            if(mBranchType.equals("2")&& mBranchType2.equals("01"))
            {
                mBranchSeries=AgentPubFun.getMonthBranchSeries(mAgentGroup);
            }
            //modify by Abigale  2008-03-05  214-218
            if(mBranchType.equals("3")&& mBranchType2.equals("01"))
            {
               mBranchSeries=AgentPubFun.getMonthBranchSeries(mAgentGroup);
            }
            return true;
        }
        catch (Exception e)
        {
            buildError("getInputData", "获取外界传入的参数时出错");
            return false;
        }
    }

    /**
     * 构造错误信息
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CalWageBaseBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 往计算基数类中设置基本信息
     * @param cAgCalBase
     * @param cAgentCode
     * @param cWageCode
     * @param cAgentGrade
     * @return
     */
    private boolean setBaseValue(AgCalBase cAgCalBase, String cAgentCode,
                                 String cWageCode, String cAgentGrade,
                                 String cAgentGroup)
    {
        mCalIndex.tAgCalBase.setAgentCode(cAgentCode);
        mCalIndex.tAgCalBase.setWageNo(IndexCalNo);
        mCalIndex.tAgCalBase.setWageCode(cWageCode);
        mCalIndex.tAgCalBase.setAgentGrade(cAgentGrade);
        mCalIndex.tAgCalBase.setManageCom(this.ManageCom);
        mCalIndex.tAgCalBase.setAgentGroup(cAgentGroup);
        mCalIndex.tAgCalBase.setAssessType("00");
        mCalIndex.tAgCalBase.setZSGroupBranchAttr(mBranchAttr);
        mCalIndex.tAgCalBase.setAreaType(mAreaType);
        mCalIndex.tAgCalBase.setWageVersion(mWageVersion);
        mCalIndex.tAgCalBase.setZSGroupBranchSeries(mBranchSeries);
        mCalIndex.tAgCalBase .setBranchType(mBranchType) ;
        mCalIndex.tAgCalBase .setBranchType2(mBranchType2);
        return true;
    }

    /**
     * 往计算基数类中设置基本信息
     * @param cAgCalBase
     * @param cAgentCode
     * @param cWageCode
     * @param cAgentGrade
     * @return
     */
    private boolean setBaseValue(AgCalBase cAgCalBase, String cAgentCode,
                                 String cWageCode, String cAgentGrade,
                                 String cAgentGroup,String cBranchCode,String cGbuildFlag,String cGbuildStartDate)
    {
        mCalIndex.tAgCalBase.setAgentCode(cAgentCode);
        mCalIndex.tAgCalBase.setWageNo(IndexCalNo);
        mCalIndex.tAgCalBase.setWageCode(cWageCode);
        mCalIndex.tAgCalBase.setAgentGrade(cAgentGrade);
        mCalIndex.tAgCalBase.setManageCom(this.ManageCom);
        mCalIndex.tAgCalBase.setAgentGroup(cAgentGroup);
        mCalIndex.tAgCalBase.setBranchCode(cBranchCode);
        mCalIndex.tAgCalBase.setAssessType("00");
        mCalIndex.tAgCalBase.setZSGroupBranchAttr(mBranchAttr);
        mCalIndex.tAgCalBase.setAreaType(mAreaType);
        mCalIndex.tAgCalBase.setWageVersion(mWageVersion);
        mCalIndex.tAgCalBase.setZSGroupBranchSeries(mBranchSeries);
        mCalIndex.tAgCalBase.setBranchType(mBranchType) ;
        mCalIndex.tAgCalBase.setBranchType2(mBranchType2);
        // add new 团队建设
        mCalIndex.tAgCalBase.setGbuildFlag(cGbuildFlag);
        mCalIndex.tAgCalBase.setGbuildStartDate(cGbuildStartDate);
        
        
        return true;
    }
    
    //开始计算佣金项
    private boolean Calculate()
    {
        String tIndexCode = new String();
        String tTableName = new String(); //保存该项指标应该存储的数据表名
        String tColName = new String(); //保存该项指标应该存储的数据字段名
        String tReturn = new String();

        //设置起止日期
        if (!setBeginEnd())
        {
            return false;
        }
        //查到该代理人的基本信息（为了查找该代理人的branchType）
        if (!setLAAgentSchema())
        {
            return false;
        }

        //xiangchun 2006-9-25
        mAgentGrade=AgentPubFun.getMonthAgentGrade(mAgentCode,IndexCalNo);
        //如果展业类型为个人的话，需要判断该人是否离职，是否需要计算他的佣金
        if (mLAAgentSchema.getAgentState().compareTo("05") > 0 &&
                !IsCalWage(mAgentCode))
        {
           return true;
        }
        //如果需要计算，则开始计算
        if (!setAgentGroupAttr(mCalIndex.tAgCalBase))
        {
           return false;
        }
//      VData tVData = new VData();
//      tVData.clear();
//      tVData.add(mGlobalInput);
//      tVData.add(mAgentCode);
//      tVData.add(IndexCalNo) ;
//      mCtrlCommDraw = new CtrlCommDraw();
//      if (!mCtrlCommDraw.judge(tVData))
//      {
//        CError tError = new CError();
//        tError.moduleName = "CalBankWageBL";
//        tError.functionName = "Calculate";
//        tError.errorMessage = "判断抽佣链是否断裂出错!";
//        this.mErrors .addOneError(tError) ;
//        return false;
//      }
//        }
        try
        {
            //开始逐项指标的计算
            for (int i = 1; i <= mLAIndexVsCommSet.size(); i++)
            {
                mCalIndex.mErrors.clearErrors();
                mLAIndexVsCommSchema = mLAIndexVsCommSet.get(i);
                tIndexCode = mLAIndexVsCommSchema.getIndexCode();
                System.out.println("----------------------->>>"+tIndexCode);
                WageCode = mLAIndexVsCommSchema.getWageCode();
                tTableName = mLAIndexVsCommSchema.getCTableName();
                tColName = mLAIndexVsCommSchema.getCColName();
                if (!getLAAssessIndexSchema(i))
                {
                    return false;
                }
                //指标计算
                mCalIndex.setAssessIndex(mLAAssessIndexSchema);
                mCalIndex.setAgentCode(mAgentCode);
                mCalIndex.setIndexCalNo(IndexCalNo);
                mCalIndex.setOperator(Operator);
                mCalIndex.clearIndexSet();
//                if (!setBaseValue(mCalIndex.tAgCalBase, mAgentCode, WageCode,
//                                  mAgentGrade, mAgentGroup))
                if (!setBaseValue(mCalIndex.tAgCalBase, mAgentCode, WageCode,
                      mAgentGrade, mAgentGroup,mBranchCode,mGbuildFlag,mGbuildStartDate))
                {
                    return false;
                }
                //将参数设置好后，开始调用指标计算类开始计算
                tReturn = mCalIndex.Calculate();

                if (StrTool.cTrim(tReturn).equals(""))
                {
                    tReturn = "0";
                }
                if(  "2".equals(mBranchType))
                {
                   if( mLAIndexVsCommSchema.getWageCode().equals("WP0001"))
                   {
                       double tValue=Double.parseDouble(tReturn);
                       if (tValue<0)
                       {
                           LAWageActivityLogSchema tLAWageActivityLogSchema = new LAWageActivityLogSchema();
                           tLAWageActivityLogSchema.setAgentCode(mAgentCode);
                           tLAWageActivityLogSchema.setAgentGroup(mAgentGroup);
                           tLAWageActivityLogSchema.setContNo("000000");
                           if(tValue==-1)
                           {
                               tLAWageActivityLogSchema.setDescribe("计算薪资项" +mLAIndexVsCommSchema.getWageName()+"查询为空！");
                           }
                           else if(tValue==-2)
                           {
                               tLAWageActivityLogSchema.setDescribe("计算薪资项"+mLAIndexVsCommSchema.getWageName()+"时，工作日计算错误！");
                           }
                           else if(tValue==-2)
                           {
                               tLAWageActivityLogSchema.setDescribe("计算薪资项" +mLAIndexVsCommSchema.getWageName()+"时返回结果错误！");
                           }
                           tLAWageActivityLogSchema.setGrpContNo("000000");
                           tLAWageActivityLogSchema.setGrpPolNo("000000");
                           tLAWageActivityLogSchema.setMakeDate(CurrentDate);
                           tLAWageActivityLogSchema.setMakeTime(CurrentTime);
                           tLAWageActivityLogSchema.setManageCom(ManageCom);
                           tLAWageActivityLogSchema.setOperator(mGlobalInput.Operator);
                           tLAWageActivityLogSchema.setOtherNo("");
                           tLAWageActivityLogSchema.setOtherNoType("");
                           tLAWageActivityLogSchema.setPolNo("000000");
                           tLAWageActivityLogSchema.setRiskCode("000000");
                           String tSerialNo = PubFun1.CreateMaxNo("WAGEACTIVITYLOG", 12);
                           tLAWageActivityLogSchema.setSerialNo(tSerialNo);
                           tLAWageActivityLogSchema.setWageLogType("02");
                           tLAWageActivityLogSchema.setWageNo(IndexCalNo);
                           mLAWageActivityLogSet.add(tLAWageActivityLogSchema);
                           tReturn = "0";
                       }
                   }

                }
                //modify by Abigale  411-460
                if(  "3".equals(mBranchType) && "01".equals(mBranchType2))
                {
                   if( mLAIndexVsCommSchema.getWageCode().equals("WY0001"))
                   {
                       double tValue=Double.parseDouble(tReturn);
                       if (tValue<0)
                       {
                           LAWageActivityLogSchema tLAWageActivityLogSchema = new LAWageActivityLogSchema();
                           tLAWageActivityLogSchema.setAgentCode(mAgentCode);
                           tLAWageActivityLogSchema.setAgentGroup(mAgentGroup);
                           tLAWageActivityLogSchema.setContNo("000000");
                           if(tValue==-1)
                           {
                               tLAWageActivityLogSchema.setDescribe("计算薪资项"
                                   +mLAIndexVsCommSchema.getWageName()+"查询为空！");
                           }
                           else if(tValue==-2)
                           {
                               tLAWageActivityLogSchema.setDescribe("计算薪资项"
                                   +mLAIndexVsCommSchema.getWageName()+"时，工作日计算错误！");
                           }
                           else if(tValue==-2)
                           {
                               tLAWageActivityLogSchema.setDescribe("计算薪资项"
                                   +mLAIndexVsCommSchema.getWageName()+"时返回结果错误！");
                           }
                           tLAWageActivityLogSchema.setGrpContNo("000000");
                           tLAWageActivityLogSchema.setGrpPolNo("000000");
                           tLAWageActivityLogSchema.setMakeDate(CurrentDate);
                           tLAWageActivityLogSchema.setMakeTime(CurrentTime);
                           tLAWageActivityLogSchema.setManageCom(ManageCom);
                           tLAWageActivityLogSchema.setOperator(mGlobalInput.Operator);
                           tLAWageActivityLogSchema.setOtherNo("");
                           tLAWageActivityLogSchema.setOtherNoType("");
                           tLAWageActivityLogSchema.setPolNo("000000");
                           tLAWageActivityLogSchema.setRiskCode("000000");
                           String tSerialNo = PubFun1.CreateMaxNo("WAGEACTIVITYLOG", 12);
                           tLAWageActivityLogSchema.setSerialNo(tSerialNo);
                           tLAWageActivityLogSchema.setWageLogType("02");
                           tLAWageActivityLogSchema.setWageNo(IndexCalNo);
                           mLAWageActivityLogSet.add(tLAWageActivityLogSchema);
                           tReturn = "0";
                       }
                   }

                }


                if (mCalIndex.mErrors.needDealError())
                {
                    LAWageActivityLogSchema tLAWageActivityLogSchema = new LAWageActivityLogSchema();
                    tLAWageActivityLogSchema.setAgentCode(mAgentCode);
                    tLAWageActivityLogSchema.setAgentGroup(mAgentGroup);
                    tLAWageActivityLogSchema.setContNo("000000");
                    tLAWageActivityLogSchema.setDescribe("计算薪资项"
                            +mLAIndexVsCommSchema.getWageName()+"出错!");
                    if("1".equals(mBranchType) && "01".equals(mBranchType2))
                    {
                        tLAWageActivityLogSchema.setGrpContNo("00000000000000000000");
                        tLAWageActivityLogSchema.setGrpPolNo("00000000000000000000");
                    }
                    else if("2".equals(mBranchType)  )
                    {
                        tLAWageActivityLogSchema.setGrpContNo("000000");
                        tLAWageActivityLogSchema.setGrpPolNo("000000");
                    }
                    //modify by Abigale 487-491
                    else if("3".equals(mBranchType) && "01".equals(mBranchType2) )
                   {
                       tLAWageActivityLogSchema.setGrpContNo("000000");
                       tLAWageActivityLogSchema.setGrpPolNo("000000");
                   }
                    tLAWageActivityLogSchema.setMakeDate(CurrentDate);
                    tLAWageActivityLogSchema.setMakeTime(CurrentTime);
                    tLAWageActivityLogSchema.setManageCom(ManageCom);
                    tLAWageActivityLogSchema.setOperator(mGlobalInput.Operator);
                    tLAWageActivityLogSchema.setOtherNo("");
                    tLAWageActivityLogSchema.setOtherNoType("");
                    tLAWageActivityLogSchema.setPolNo("000000");
                    tLAWageActivityLogSchema.setRiskCode("000000");
                    String tSerialNo = PubFun1.CreateMaxNo("WAGEACTIVITYLOG", 12);
                    tLAWageActivityLogSchema.setSerialNo(tSerialNo);
                    tLAWageActivityLogSchema.setWageLogType("02");
                    tLAWageActivityLogSchema.setWageNo(IndexCalNo);
                    mLAWageActivityLogSet.add(tLAWageActivityLogSchema);
                }
                else if (!insertWage(IndexCalNo, mAgentCode, mAgentGrade, WageNo,
                                tReturn,
                                tTableName, tColName))
                {
                    System.out.println("插入佣金项的值时出错！！");
                    return false;
                }
            }
            //如果LAWage表存在改代理人当月工资的记录，只需要刷新该记录即可
            if (exist)
            {
                mUpLAWageSet.add(mLAWageSchema);
            }
            //否则，需要插入新记录
            else
            {
                mInLAWageSet.add(mLAWageSchema);
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("Caculate", e.toString());
            return false;
        }
    }

    private String getAreaType(String agentCode, String wageCode)
    {
        String sql = "select calAgentAreaType('" + agentCode + "','" + wageCode +
                     "','" + ManageCom +
                     "') from ldsysvar where sysvar = 'onerow' ";
        ExeSQL tExeSQL = new ExeSQL();
        String tAreaType = tExeSQL.getOneValue(sql);

        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            buildError("getAreaType",
                       "获取" + agentCode + "的佣金项" + wageCode + "的地区类型时出错！");
            return null;
        }
        if (tAreaType.equals("S"))
        {
            return mAreaType;
        }
        else
        {
            return tAreaType;
        }
    }

    private boolean prepareOutputData()
    {
        mOutputData.addElement(mUpLAWageSet);
        mOutputData.addElement(mInLAWageSet);
        mOutputData.addElement(mLAWageActivityLogSet);
        return true;
    }

    /**
     * 往mLAWageSchema中，根据该项指标对应的字段，将结果保存到该字段当中
     * @param cIndexCalNo
     * @param cAgentCode
     * @param cGrade
     * @param cWageCode
     * @param cIndexValue
     * @return
     */
    private boolean insertWage(String cIndexCalNo, String cAgentCode,
                               String cGrade, String cWageCode,
                               String cIndexValue, String tTableName,
                               String tColName)
    {
        if((mBranchType.equals("1") && mBranchType2.equals("01"))||(mBranchType.equals("2"))
        		//添加对于互动渠道的判断,添加健康管理渠道
        		//2014-12-8 by yangyang
        		||(mBranchType.equals("5") && mBranchType2.equals("01"))||(mBranchType.equals("7") && mBranchType2.equals("01")))
        {
            String tSQL = "", tCount = "";
            String currentDate = PubFun.getCurrentDate();
            String currentTime = PubFun.getCurrentTime();

            LAWageTempSet tLAWageTempSet = new LAWageTempSet();
            //exist的初始值是true，所以第一次的时候，要去LAWage表中查找该代理人的当月佣金是否已经算过一部分，
            //如果算过，则exist依然为true
            if (exist) {
                if (!hasQueriedWage) {
                    //先判断记录是否已存在
                    tSQL = "select * from " + tTableName +
                           " where IndexCalNo = '" +
                           cIndexCalNo + "' and AgentCode = '" + cAgentCode +
                           "'";
                    System.out.println(tSQL);
                    LAWageTempDB tLAWageTempDB = new LAWageTempDB();
                    tLAWageTempSet = tLAWageTempDB.executeQuery(tSQL);
                    if (tLAWageTempDB.mErrors.needDealError()) {
                        this.mErrors.copyAllErrors(tLAWageTempDB.mErrors);
                        buildError("insetWage",
                                   "查询代理人" + cAgentCode + "的" + cIndexCalNo +
                                   "佣金项是否已经计算时DB类失败");
                        return false;
                    }
                    hasQueriedWage = true;
                    if (tLAWageTempSet.size() == 0) {
                        exist = false;
                    }
                }
            }
            System.out.println("tColName:    " + tColName);
            System.out.println("cIndexValue: " + cIndexValue);
            //如果不存在该记录，那么需要准备一条记录插入LAWage表
            if (!exist) {
                if (mLAWageSchema == null) {
                    System.out.println(" is null");
                    mLAWageSchema = new LAWageSchema();
                    System.out.println(" has new");
                }
                mLAWageSchema.setAgentCode(cAgentCode);
                mLAWageSchema.setAgentGrade(cGrade);
                mLAWageSchema.setAgentGroup(mAgentGroup);
                mLAWageSchema.setBranchType(mBranchType);
                mLAWageSchema.setBranchType2(mBranchType2);
                //添加销售人员类型，确保薪资计算时的人员类型与流转到财务接口时的一致  2017-5-3 9:50
                mLAWageSchema.setAgentType(mLAAgentSchema.getAgentType());
                mLAWageSchema.setBranchAttr(this.mCalIndex.tAgCalBase.
                                            getZSGroupBranchAttr());
                mLAWageSchema.setManageCom(mLATreeSchema.getManageCom());
                mLAWageSchema.setIndexCalNo(cIndexCalNo);
                mLAWageSchema.setOperator(Operator);
                mLAWageSchema.setOperator2(Operator);
                mLAWageSchema.setState("0");
                mLAWageSchema.setGetDate(currentDate);
                mLAWageSchema.setMakeDate(currentDate);
                mLAWageSchema.setMakeTime(currentTime);
                mLAWageSchema.setModifyDate(currentDate);
                mLAWageSchema.setModifyTime(currentTime);
                mLAWageSchema.setV(tColName, cIndexValue);
            }
            else { //否则，只需要更新该记录即可
                if (mLAWageSchema == null) {
                    LAWageSchema tLAWageSchema = new LAWageSchema();
                    Reflections tReflections = new Reflections();
                    tReflections.transFields(tLAWageSchema,tLAWageTempSet.get(1));
                    mLAWageSchema = new LAWageSchema();
                    mLAWageSchema.setSchema(tLAWageSchema);
                }
                mLAWageSchema.setBranchType(mBranchType);
                mLAWageSchema.setBranchType2(mBranchType2);
                mLAWageSchema.setState("0");
                mLAWageSchema.setOperator(Operator);
                mLAWageSchema.setModifyDate(currentDate);
                mLAWageSchema.setModifyTime(currentTime);
                mLAWageSchema.setV(tColName, cIndexValue);

            }
        }
        else
        {
            String tSQL = "", tCount = "";
            String currentDate = PubFun.getCurrentDate();
            String currentTime = PubFun.getCurrentTime();

            LAWageSet tLAWageSet = new LAWageSet();
            //exist的初始值是true，所以第一次的时候，要去LAWage表中查找该代理人的当月佣金是否已经算过一部分，
            //如果算过，则exist依然为true
            if (exist) {
                if (!hasQueriedWage) {
                    //先判断记录是否已存在
                    tSQL = "select * from " + tTableName + " where IndexCalNo = '" +
                           cIndexCalNo + "' and AgentCode = '" + cAgentCode +"'";
                    System.out.println(tSQL);
                    LAWageDB tLAWageDB = new LAWageDB();
                    tLAWageSet = tLAWageDB.executeQuery(tSQL);
                    if (tLAWageDB.mErrors.needDealError()) {
                        this.mErrors.copyAllErrors(tLAWageDB.mErrors);
                        buildError("insetWage",
                                   "查询代理人" + cAgentCode + "的" + cIndexCalNo +
                                   "佣金项是否已经计算时DB类失败");
                        return false;
                    }
                    hasQueriedWage = true;
                    if (tLAWageSet.size() == 0) {
                        exist = false;
                    }
                }
            }
            System.out.println("tColName:    " + tColName);
            System.out.println("cIndexValue: " + cIndexValue);
            //如果不存在该记录，那么需要准备一条记录插入LAWage表
            if (!exist) {
                if (mLAWageSchema == null) {
                    System.out.println(" is null");
                    mLAWageSchema = new LAWageSchema();
                    System.out.println(" has new");
                }
                mLAWageSchema.setAgentCode(cAgentCode);
                mLAWageSchema.setAgentGrade(cGrade);
                mLAWageSchema.setAgentGroup(mAgentGroup);
                mLAWageSchema.setBranchType(mBranchType);
                mLAWageSchema.setBranchType2(mBranchType2);
                //添加销售人员类型，确保薪资计算时的人员类型与流转到财务接口时的一致  2017-5-3 9:50
                mLAWageSchema.setAgentType(mLAAgentSchema.getAgentType());
                mLAWageSchema.setBranchAttr(this.mCalIndex.tAgCalBase.
                                            getZSGroupBranchAttr());
                mLAWageSchema.setManageCom(mLATreeSchema.getManageCom());
                mLAWageSchema.setIndexCalNo(cIndexCalNo);
                mLAWageSchema.setOperator(Operator);
                mLAWageSchema.setOperator2(Operator);
                mLAWageSchema.setState("0");
                mLAWageSchema.setGetDate(currentDate);
                mLAWageSchema.setMakeDate(currentDate);
                mLAWageSchema.setMakeTime(currentTime);
                mLAWageSchema.setModifyDate(currentDate);
                mLAWageSchema.setModifyTime(currentTime);
                mLAWageSchema.setV(tColName, cIndexValue);
            } else { //否则，只需要更新该记录即可

                if (mLAWageSchema == null) {
                    LAWageSchema tLAWageSchema = new LAWageSchema();
                    Reflections tReflections = new Reflections();
                    tReflections.transFields(tLAWageSchema,
                                             tLAWageSet.get(1));
                    mLAWageSchema = new LAWageSchema();
                    mLAWageSchema.setSchema(tLAWageSchema);
                }
                mLAWageSchema.setBranchType(mBranchType);
                mLAWageSchema.setBranchType2(mBranchType2);
                mLAWageSchema.setState("0");
                mLAWageSchema.setOperator(Operator);
                mLAWageSchema.setModifyDate(currentDate);
                mLAWageSchema.setModifyTime(currentTime);
                mLAWageSchema.setV(tColName, cIndexValue);
            }
        }
        return true;
    }

    /**
     * 根据佣金计算指标记录中的指标编码从LAIndexCode取出该编码对应的指标记录
     * @return
     */
    private boolean getLAAssessIndexSchema(int i)
    {
        if (mLAAssessIndexSet != null && mLAAssessIndexSet.size() > 0)
        {
            mLAAssessIndexSchema = mLAAssessIndexSet.get(i);
        }
        else
        {
            String tIndexCode = mLAIndexVsCommSchema.getIndexCode();
            LAAssessIndexDB tLAAssessIndexDB = new LAAssessIndexDB();
            tLAAssessIndexDB.setIndexCode(tIndexCode);
            System.out.println("指标值indexcode"+tIndexCode);
            if (!tLAAssessIndexDB.getInfo())
            {
                this.mErrors.copyAllErrors(tLAAssessIndexDB.mErrors);
                buildError("getLAAssessINdexSchema",
                           "未查到指标编码为" + tIndexCode + "的指标信息");
                return false;
            }
            mLAAssessIndexSchema = tLAAssessIndexDB.getSchema();
        }
        return true;
    }

    /**
     * 往计算基数类cAgCalBase中根据AgentGroup置入BranchAttr
     * @param cAgCalBase
     * @return
     */
    private boolean setAgentGroupAttr(AgCalBase cAgCalBase)
    {
        String tSQL = "", tReturn = "";
        ExeSQL tExeSQL = new ExeSQL();
        tSQL ="select trim(BranchAttr) from LABranchGroup where AgentGroup = '"
                + mAgentGroup + "'";
        tReturn = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "CalWage";
            tError.functionName = "setAgentGroupAttr";
            tError.errorMessage = "查询所在机构的外部编码失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        cAgCalBase.setBranchAttr(tReturn);
        return true;
    }

    /**
     * 校验代理人是否离职，若为本月离职则计算佣金 否则不计算
     * @Return true  :计算
     *         false :不计算
     */
    private boolean IsCalWage(String cAgentCode)
    {
        ExeSQL tExeSQL = new ExeSQL();
        String tDepartDate = mLAAgentSchema.getOutWorkDate();
        FDate tFDate = new FDate();
        System.out.println("tDepartDate " + tDepartDate);
        //如果该代理人没有离职，那么需要计算该代理人的佣金指标
        if (tDepartDate == null || tDepartDate.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "CalWageBaseBL";
            tError.functionName = "IsCalWage";
            tError.errorMessage = "代理人" + mLAAgentSchema.getAgentCode() +
                                  "已离职，但离职日期为空！";
            this.mErrors.addOneError(tError);
            return false;
        }

        //否则，根据离职时间确定离职时处于哪一个承保月度，如果这个承保月度小于IndexCalno，则不用计算（因为在当前的计算月肯定没有业绩）
        else
        {
            //查询承保月度统计间隔
            String tSQL1="";
            if(mBranchType.equals("1")&&mBranchType2.equals("01"))//个险
            {
                tSQL1 =
                        "select yearmonth from lastatsegment where stattype='5' and '" +
                        tDepartDate + "'>=startDate  and '" + tDepartDate +
                        "'<=endDate";
            }
            if(mBranchType.equals("2"))//团险
            {
                tSQL1 =
                        "select yearmonth from lastatsegment where stattype='91' and '" +
                        tDepartDate + "'>=startDate  and '" + tDepartDate +
                        "'<=endDate";
            }
            if(mBranchType.equals("3")&&mBranchType2.equals("01"))//银代   modify by Abigale
           {
               tSQL1 =
                       "select yearmonth from lastatsegment where stattype='91' and '" +
                       tDepartDate + "'>=startDate  and '" + tDepartDate +
                       "'<=endDate";
           }
            if(mBranchType.equals("5")&&mBranchType2.equals("01"))//互动渠道
            {
                tSQL1 =
                        "select yearmonth from lastatsegment where stattype='5' and '" +
                        tDepartDate + "'>=startDate  and '" + tDepartDate +
                        "'<=endDate";
            }
            if(mBranchType.equals("7")&&mBranchType2.equals("01"))//互动渠道
            {
                tSQL1 =
                        "select yearmonth from lastatsegment where stattype='5' and '" +
                        tDepartDate + "'>=startDate  and '" + tDepartDate +
                        "'<=endDate";
            }
            System.out.println(tSQL1);
            tExeSQL = new ExeSQL();
            System.out.println("calDate " + tSQL1);
            String calDate = tExeSQL.getOneValue(tSQL1);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalBankWageBL";
                tError.functionName = "IsCalWage";
                tError.errorMessage = "查询" + tDepartDate + "的统计日期出错！";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("calDate " + calDate);
            if (calDate.compareTo(this.IndexCalNo) < 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
    }

    /**
     * 设置起止时间标志（仅仅设置标志，而不是设置具体时间）
     * @return
     */
    private boolean setBeginEnd()
    {
        //从数据库中查出时间区间
        LAStatSegmentSchema tLAStatSegmentSchema;
        LAStatSegmentSet tLAStatSegmentSet;
        LAStatSegmentDB tLAStatSegmentDB = new LAStatSegmentDB();
        String tSQL = "select * from LAStatSegment where YearMonth = " +
                      this.IndexCalNo
                      + " order by stattype";
        tLAStatSegmentSet = tLAStatSegmentDB.executeQuery(tSQL);
        if (tLAStatSegmentDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError tCError = new CError();
            tCError.moduleName = "CalBankWageBL";
            tCError.functionName = "setBeginEnd()";
            tCError.errorMessage = "查询时间区间出错！";
            this.mErrors.addOneError(tCError);
            return false;
        }
        int tCount = tLAStatSegmentSet.size();
        System.out.println("tCount:" + tCount + "," + this.IndexCalNo);
        if (tCount < 1)
        {
            CError tCError = new CError();
            tCError.moduleName = "CalBankWage";
            tCError.functionName = "setBeginEnd()";
            tCError.errorMessage = "所输年月有问题，无法查到时间区间";
            this.mErrors.addOneError(tCError);
            return false;
        }
        String tStatType = "";
        for (int i = 1; i <= tCount; i++)
        {
            tLAStatSegmentSchema = tLAStatSegmentSet.get(i);
            tStatType = tLAStatSegmentSchema.getStatType().trim();
            System.out.println("-----------------stattype;" + tStatType);
            System.out.println(
                    "-----------------mCalIndex.tAgCalBase.getHalfYearMark();" +
                    mCalIndex.tAgCalBase.getHalfYearMark());
            System.out.println(
                    "-----------------mCalIndex.tAgCalBase.getYearMark();" +
                    mCalIndex.tAgCalBase.getYearMark());
            System.out.println(
                    "-----------------mCalIndex.tAgCalBase.getQuauterMark();" +
                    mCalIndex.tAgCalBase.getQuauterMark());
            if (tStatType.equals("1"))
            {
                mCalIndex.tAgCalBase.setMonthMark("1");
                mCalIndex.tAgCalBase.setMonthBegin(tLAStatSegmentSchema.
                        getStartDate());
                mCalIndex.tAgCalBase.setMonthEnd(tLAStatSegmentSchema.
                                                 getEndDate());
                mCalIndex.tAgCalBase.setQuauterBegin(tLAStatSegmentSchema.
                        getStartDate());
                mCalIndex.tAgCalBase.setQuauterEnd(tLAStatSegmentSchema.
                        getEndDate());
                mCalIndex.tAgCalBase.setHalfYearBegin(tLAStatSegmentSchema.
                        getStartDate());
                mCalIndex.tAgCalBase.setHalfYearEnd(tLAStatSegmentSchema.
                        getEndDate());
                mCalIndex.tAgCalBase.setYearBegin(tLAStatSegmentSchema.
                                                  getStartDate());
                mCalIndex.tAgCalBase.setYearEnd(tLAStatSegmentSchema.getEndDate());
                continue;
            }
            if (tStatType.equals("2"))
            {
                mCalIndex.tAgCalBase.setQuauterMark("1");
                mCalIndex.tAgCalBase.setQuauterBegin(tLAStatSegmentSchema.
                        getStartDate());
                mCalIndex.tAgCalBase.setQuauterEnd(tLAStatSegmentSchema.
                        getEndDate());
                mCalIndex.tAgCalBase.setHalfYearBegin(tLAStatSegmentSchema.
                        getStartDate());
                mCalIndex.tAgCalBase.setHalfYearEnd(tLAStatSegmentSchema.
                        getEndDate());
                mCalIndex.tAgCalBase.setYearBegin(tLAStatSegmentSchema.
                                                  getStartDate());
                mCalIndex.tAgCalBase.setYearEnd(tLAStatSegmentSchema.getEndDate());
                continue;
            }
            if (tStatType.equals("3"))
            {
                mCalIndex.tAgCalBase.setHalfYearMark("1");
                mCalIndex.tAgCalBase.setHalfYearBegin(tLAStatSegmentSchema.
                        getStartDate());
                mCalIndex.tAgCalBase.setHalfYearEnd(tLAStatSegmentSchema.
                        getEndDate());
                mCalIndex.tAgCalBase.setYearBegin(tLAStatSegmentSchema.
                                                  getStartDate());
                mCalIndex.tAgCalBase.setYearEnd(tLAStatSegmentSchema.getEndDate());
                continue;
            }
            if (tStatType.equals("4"))
            {
                mCalIndex.tAgCalBase.setYearMark("1");
                mCalIndex.tAgCalBase.setYearBegin(tLAStatSegmentSchema.
                                                  getStartDate());
                mCalIndex.tAgCalBase.setYearEnd(tLAStatSegmentSchema.getEndDate());
                continue;
            }
        }
        mCalIndex.tAgCalBase.setTempBegin(mCalIndex.tAgCalBase.getMonthBegin());
        mCalIndex.tAgCalBase.setTempEnd(mCalIndex.tAgCalBase.getMonthEnd());
        return true;
    }

    /**
     *
     * @return
     */
    private boolean setLAAgentSchema()
    {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            buildError("setLAAgentSchema", "查询代理人" + mAgentCode + "的基本信息失败");
            return false;
        }
        mLAAgentSchema.setSchema(tLAAgentDB.getSchema());
        mBranchType = mLAAgentSchema.getBranchType();
        mBranchType2 = mLAAgentSchema.getBranchType2();
        mGbuildFlag= mLAAgentSchema.getGBuildFlag();
        mGbuildStartDate = mLAAgentSchema.getGBuildStartDate();
        return true;
    }

    public static void main(String[] args)
    {
        try
        {
            PrintStream out =
                    new PrintStream(
                            new BufferedOutputStream(
                                    new FileOutputStream("CalWageBaseBL.out")));
            System.setOut(out);
            System.out.println("---------------");
            GlobalInput tGlobalInput = new GlobalInput();
            tGlobalInput.ManageCom = "86110000";
            tGlobalInput.Operator = "aa";

            String tAgentCode = "8611000002";
            String tAgentGrade = "A08";

            LAIndexVsCommSet tLAIndexVsCommSet = new LAIndexVsCommSet();
            LAIndexVsCommDB tLAIndexVsCommDB = new LAIndexVsCommDB();
            String tSQL = "select * from laindexvscomm where agentgrade='" +
                          tAgentGrade + "' and wagecode='W00011'";
            System.out.println("===tSQL===" + tSQL);
            tLAIndexVsCommSet = tLAIndexVsCommDB.executeQuery(tSQL);

            VData tVData = new VData();
            tVData.add(tGlobalInput);
            tVData.addElement(tLAIndexVsCommSet);
            tVData.addElement("200401");
            tVData.add("86110000");
            tVData.addElement(tAgentCode);

            CalWageBaseBL calWageBaseBL1 = new CalWageBaseBL();
            if (calWageBaseBL1.mErrors.needDealError())
            {
                System.out.println(calWageBaseBL1.mErrors.getFirstError());
            }
            System.out.println("---------------");
            System.out.println("over");
            out.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
