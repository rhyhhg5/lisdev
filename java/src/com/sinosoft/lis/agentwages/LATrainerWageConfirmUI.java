package com.sinosoft.lis.agentwages;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: </p>
 * @author wangQingMin
 * @version 1.0
 */
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class LATrainerWageConfirmUI
{

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();


    public LATrainerWageConfirmUI()
    {
    }

    public boolean submitData(VData cInputData)
    {
        mInputData = (VData) cInputData.clone();
        LATrainerWageConfirmBL tLATrainerWageConfirmBL = new LATrainerWageConfirmBL();
        if (!tLATrainerWageConfirmBL.submitData(mInputData))
        {
            this.mErrors.copyAllErrors(tLATrainerWageConfirmBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LATrainerWageConfirm";
            tError.functionName = "submitData";
            tError.errorMessage = tLATrainerWageConfirmBL.mErrors.getFirstError();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
