package com.sinosoft.lis.agentwages;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.vdb.LACommisionDBSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class AgentWageCalSetWageNoReBLS
{
    public CErrors mErrors = new CErrors();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    public AgentWageCalSetWageNoReBLS()
    {
    }

    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!saveData())
        {
            return false;
        }
        return true;

    }

    private boolean getInputData(VData cInputData)
    {
        try
        {
            mLACommisionSet = (LACommisionSet) cInputData.getObject(0);
            return true;
        }
        catch (Exception e)
        {
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSetWageNoReBLS";
            tError.functionName = "getInputData";
            tError.errorMessage = "获取BL传入数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    private boolean saveData()
    {
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSetWageNoReBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LACommisionDBSet tLACommisionDBSet = new LACommisionDBSet(conn);
            tLACommisionDBSet.add(mLACommisionSet);
            if (!tLACommisionDBSet.update())
            {
                this.mErrors.copyAllErrors(tLACommisionDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentWageCalSetWageNoReBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "刷新记录失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSetWageNoReBLS";
            tError.functionName = "saveData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {
        AgentWageCalSetWageNoReBLS agentWageCalSetWageNoReBLS1 = new
                AgentWageCalSetWageNoReBLS();
    }

}
