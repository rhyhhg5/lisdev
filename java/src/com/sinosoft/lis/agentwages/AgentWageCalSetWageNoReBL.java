package com.sinosoft.lis.agentwages;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LAStatSegmentDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LAStatSegmentSchema;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAStatSegmentSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class AgentWageCalSetWageNoReBL
{
    public CErrors mErrors = new CErrors();
    private String mOperate = new String();
    private GlobalInput mGI = new GlobalInput();
    private String mManageCom = new String();
    private String mIndexCalNo = new String();
    private String mBranchType = new String();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private VData mOutputData = new VData();
    public AgentWageCalSetWageNoReBL()
    {
    }

    public static void main(String[] args)
    {
        AgentWageCalSetWageNoReBL agentWageCalSetWageNoReBL1 = new
                AgentWageCalSetWageNoReBL();
        VData tInputData = new VData();
        String tManageCom = "8613";
        String tIndexCalNo = "200408";
        String tBranchTyp = "1";
        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "aa";
        tGI.ManageCom = "86";
        tInputData.add(tManageCom);
        tInputData.add(tIndexCalNo);
        tInputData.add(tBranchTyp);
        tInputData.add(tGI);
        agentWageCalSetWageNoReBL1.submitData(tInputData, "");
    }

    private boolean getInputData(VData cInputData)
    {
        try
        {
            mManageCom = (String) cInputData.get(0);
            mIndexCalNo = (String) cInputData.get(1);
            mBranchType = (String) cInputData.get(2);
            mGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",
                    0);
            return true;
        }
        catch (Exception e)
        {
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSetWageNoReBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        if (!parpareOutputData())
        {
            return false;
        }
        AgentWageCalSetWageNoReBLS BLS = new AgentWageCalSetWageNoReBLS();
        if (!BLS.submitData(mOutputData))
        {
            this.mErrors.copyAllErrors(BLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSetWageNoReBL";
            tError.functionName = "submitData";
            tError.errorMessage = "BLS保存数据失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean parpareOutputData()
    {
        mOutputData.add(mLACommisionSet);
        return true;
    }

    private boolean dealData()
    {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        String tSQL = "select comcode from ldcom where comcode like '" +
                      mManageCom +
                      "%'"
                      //+" and length(trim(comcode)) = 8" //xjh Modify ,2005/02/24
                      ;
        tSSRS = tExeSQL.execSQL(tSQL);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String tManageCom = tSSRS.GetText(i, 1);
            String sql = "select "
                         //+" trim(nvl(max(indexcalno),'0'))"
                         + " max(indexcalno) " //xjh Modify ,2005/02/24
                         + " from lawage where branchtype='" +
                         mBranchType + "' and managecom = '" +
                         tManageCom + "'";

            String startIndexCalNo = tExeSQL.getOneValue(sql);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentWageCalSetWageNoReBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询lawage表失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
            //if (startIndexCalNo.equals("0")) {
            if (startIndexCalNo == null || startIndexCalNo.equals(""))
            {
                sql = "select "
                      //+ "trim(nvl(min(signdate),'3000-12-31')) "
                      +
                        "(case when min(signdate)is null then '3000-12-31' else min(signdate) end)"
                      + " from lacommision where branchtype='"
                      + mBranchType + "' and commdire='1' and managecom = '" +
                      tManageCom +
                      "'";
                startIndexCalNo = tExeSQL.getOneValue(sql);
                if (tExeSQL.mErrors.needDealError())
                {
                    this.mErrors.copyAllErrors(tExeSQL.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AgentWageCalSetWageNoReBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "查询LACommision表失败！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (startIndexCalNo != null &&
                    startIndexCalNo.equals("3000-12-31"))
                {
                    continue;
                }
                startIndexCalNo = AgentPubFun.formatDate(startIndexCalNo,
                        "yyyyMM");
            }
            else
            {
                startIndexCalNo = getNextIndexCalNo(startIndexCalNo, 1);
            }
            String tIndexCalNo = startIndexCalNo;
            while (tIndexCalNo.compareTo(mIndexCalNo) <= 0)
            {
                String lastDate = getLastDate(tIndexCalNo);
                LAStatSegmentDB tLAStatSegmentDB = new LAStatSegmentDB();
                tLAStatSegmentDB.setYearMonth(Integer.parseInt(tIndexCalNo));
                tLAStatSegmentDB.setStatType("7");
                if (!tLAStatSegmentDB.getInfo())
                {
                    this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AgentWageCalSetWageNoReBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "查询" + tIndexCalNo +
                                          "LAStatSegment表的扫描日期失败！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                String tScanDate = tLAStatSegmentDB.getStartDate();
                String tMakeDate = tLAStatSegmentDB.getEndDate();
                String tGetPolDate = tLAStatSegmentDB.getExtDate();
                sql = "  select  c.* "
                      + " from lacommision c "
                      + " where c.scandate<='" + tScanDate + "' "
                      + " and c.branchtype='1' "
                      + " and c.TMakeDate<='" + tMakeDate + "' "
                      + " and c.payyear<1 "
                      + " and c.getpoldate<='" + tGetPolDate + "' "
                      + " and c.wageno>'" + tIndexCalNo + "' "
                      + " and c.managecom = '" + tManageCom + "' ";
                System.out.println("SQL:" + sql);
                LACommisionSet tLACommisionSet = new LACommisionSet();
                LACommisionDB tLACommisionDB = new LACommisionDB();
                tLACommisionSet = tLACommisionDB.executeQuery(sql);
//        if (tLACommisionDB.mErrors.needDealError()) {
//          this.mErrors.copyAllErrors(tLACommisionDB.mErrors);
//          CError tError = new CError();
//          tError.moduleName = "AgentWageCalSetWageNoReBL";
//          tError.functionName = "dealData";
//          tError.errorMessage = "查询LACommision表的特殊单子失败！";
//          this.mErrors.addOneError(tError);
//          return false;
//        }
                for (int j = 1; j <= tLACommisionSet.size(); j++)
                {
                    LACommisionSchema tLACommisionSchema = new
                            LACommisionSchema();
                    tLACommisionSchema = tLACommisionSet.get(j);
                    tLACommisionSchema.setWageNo(tIndexCalNo);
                    tLACommisionSchema.setCalDate(lastDate);
                    tLACommisionSchema.setTransState("89");
//          tLACommisionSchema.setModifyDate(currentDate) ;
//          tLACommisionSchema.setModifyTime(currentTime);
                    this.mLACommisionSet.add(tLACommisionSchema);
                }
                tIndexCalNo = getNextIndexCalNo(tIndexCalNo, 1);
            }
        }
        return true;
    }

    private String getLastDate(String cIndexCalNo)
    {
        String tSQL =
                "select * from lastatsegment where stattype='1' and yearmonth=" +
                cIndexCalNo + "";
        LAStatSegmentDB tLAStatSegmentDB = new LAStatSegmentDB();
        System.out.println(tSQL);
        LAStatSegmentSchema tLAStatSegmentSchema = new LAStatSegmentSchema();
        LAStatSegmentSet tLAStatSegmentSet = new LAStatSegmentSet();
        tLAStatSegmentSet = tLAStatSegmentDB.executeQuery(tSQL);
        if (tLAStatSegmentSet == null || tLAStatSegmentSet.size() == 0)
        {
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveBL";
            tError.functionName = "getLastDate";
            tError.errorMessage = "查询" + cIndexCalNo + "的当月的最后一天时出错！";
            this.mErrors.addOneError(tError);
            return null;
        }
        tLAStatSegmentSchema = tLAStatSegmentSet.get(1);
        return tLAStatSegmentSchema.getEndDate();
    }

    private String getNextIndexCalNo(String cIndexCalNo, int unit)
    {
        String tIndexCalNo = cIndexCalNo.substring(0, 4) + "-" +
                             cIndexCalNo.substring(4, 6) + "-01";
        tIndexCalNo = PubFun.calDate(tIndexCalNo, unit, "M", null);
        tIndexCalNo = AgentPubFun.formatDate(tIndexCalNo, "yyyyMM");
        return tIndexCalNo;
    }


}
