package com.sinosoft.lis.agentwages;

/*
 * <p>ClassName: LAAssessInputBLS </p>
 * <p>Description: LAAssessBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-06-21
 */

import java.sql.Connection;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.vdb.LACommisionBDBSet;
import com.sinosoft.lis.vschema.LACommisionBSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class AdjustBankPolBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public AdjustBankPolBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start AdjustBankPolBLS Submit...");
        tReturn = saveLAAssess(cInputData);

        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End AdjustBankPolBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLAAssess(VData mInputData)
    {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustBankPolBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("update 插入lacommision...");
            LACommisionSet tLACommisionSet = new LACommisionSet();
            tLACommisionSet.set((LACommisionSet) mInputData.
                                getObjectByObjectName("LACommisionSet", 0));
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            tLACommisionSchema.setSchema((LACommisionSchema) mInputData.
                                         getObjectByObjectName(
                                                 "LACommisionSchema", 0));
            LACommisionBSet tLACommisionBSet = new LACommisionBSet();
            tLACommisionBSet.set((LACommisionBSet) mInputData.
                                 getObjectByObjectName("LACommisionBSet", 0));
            //先备份
            System.out.println("LACommisionBSet size" + tLACommisionBSet.size());
            if (tLACommisionBSet.size() > 0)
            {
                System.out.println("come in 备份");
                LACommisionBDBSet tLACommisionBDBSet = new LACommisionBDBSet(
                        conn);
                tLACommisionBDBSet.set(tLACommisionBSet);
                if (!tLACommisionBDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLACommisionBDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAAssessInputBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            //再做处理
            if (tLACommisionSet.size() > 0)
            {
                for (int i = 1; i <= tLACommisionSet.size(); i++)
                {
                    //查询变更人的组信息
                    String mSQL =
                            "select a.agentgroup,a.branchcode,b.branchattr from laagent a,labranchgroup b " +
                            "where a.agentgroup=b.agentgroup and a.agentcode = '" +
                            tLACommisionSet.get(i).getAgentCode() + "' ";
                    SSRS tSSRS = new SSRS();
                    ExeSQL aExeSQL = new ExeSQL();
                    tSSRS = aExeSQL.execSQL(mSQL);
                    String tSQL = "update LACommision set agentcode='" +
                                  tLACommisionSet.get(i).getAgentCode() +
                                  "',ModifyDate='" + currentDate +
                                  "',ModifyTime='" + currentTime
                                  + "',agentgroup='" + tSSRS.GetText(1, 1) +
                                  "',branchcode='" + tSSRS.GetText(1, 2) +
                                  "',branchattr='" + tSSRS.GetText(1, 3) + "' "
                                  + " where agentcode='" +
                                  tLACommisionSchema.getAgentCode() +
                                  "' and agentcom='" +
                                  tLACommisionSet.get(i).getAgentCom()
                                  + "' and polno ='" +
                                  tLACommisionSet.get(i).getPolNo() +
                                  "' and p11='" + tLACommisionSet.get(i).getP11()
                                  + "' and p13='" +
                                  tLACommisionSet.get(i).getP13() +
                                  "' and signdate='" +
                                  tLACommisionSet.get(i).getSignDate()
                                  + "' and getpoldate='" +
                                  tLACommisionSet.get(i).getGetPolDate() +
                                  "' and transmoney='" +
                                  tLACommisionSet.get(i).getTransMoney() + "'";
                    ExeSQL tExeSQL = new ExeSQL();
                    System.out.println("插入记录的SQL: " + tSQL);
                    tReturn = tExeSQL.execUpdateSQL(tSQL);
                    if (tExeSQL.mErrors.needDealError())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tExeSQL.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "CalIndex";
                        tError.functionName = "insertAssess";
                        tError.errorMessage = "执行SQL语句：判断记录是否存在失败!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustBankPolBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }
}
