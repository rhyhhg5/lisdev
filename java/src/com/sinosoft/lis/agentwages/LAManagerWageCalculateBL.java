/**
 * Copyright (c) 2018 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agentwages;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.LATrainerWageHistorySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author pengcheng
 * @version 1.0
 */
public class LAManagerWageCalculateBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();

	// 业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	private String mManageCom = "";
	private String mBranchType = "";
	private String mBranchType2 = "";
	private String mWageNo = "";

	public LAManagerWageCalculateBL() {
	}

	public static void main(String[] args) {
		try {
		
			GlobalInput tGlobalInput = new GlobalInput();
			tGlobalInput.ManageCom = "86";
			tGlobalInput.Operator = "aa";

			VData tVData = new VData();
			LAManagerWageCalculateBL LATrainerWageCalculateBL1 = new LAManagerWageCalculateBL();
			tVData.add("86110000");
			tVData.add("1");
			tVData.add("01");
			tVData.add("201803");
			tVData.add(tGlobalInput);
			LATrainerWageCalculateBL1.submitData(tVData);
			if (LATrainerWageCalculateBL1.mErrors.needDealError()) {
				for (int i = 0; i < LATrainerWageCalculateBL1.mErrors
						.getErrorCount(); i++) {
					System.out.println(LATrainerWageCalculateBL1.mErrors
							.getError(i).moduleName);
					System.out.println(LATrainerWageCalculateBL1.mErrors
							.getError(i).functionName);
					System.out.println(LATrainerWageCalculateBL1.mErrors
							.getError(i).errorMessage);
				}
			}
			System.out.println("---------------");
			System.out.println("over");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData) {

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		if (!check()) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			if ((mBranchType.equals("1") && mBranchType2.equals("01"))) {

				if (!returnState()) {
					return false;
				}
			}
			return false;
		}

		return true;
	}

	private boolean check() {
		System.out.println("mBranchType-----" + mBranchType);
		if (mBranchType.equals("1"))// 个险按支公司进行计算
		{
			if (this.mManageCom.length() != 8) {
				buildError("check", "机构代码长度必须为8位");
				return false;

			}
		}

		if ((mBranchType.equals("1") && mBranchType2.equals("01"))) {

			// 是否正在计算
			if (!checkState()) {
				return false;
			}
			// 插入状态，其他人在计算完成前不能计算
			if (!insertState()) {
				return false;
			}

		}
		return true;
	}

	private boolean dealData() {

		VData tInputData = new VData();
		TransferData tTransferData = new TransferData();
		// 计算前删除原来的计算结果
		if (!deleteWage()) {
			return false;
		}
		// 佣金计算前的基础指标
		tInputData.clear();
		tTransferData = new TransferData();
		tTransferData.setNameAndValue("ManageCom", mManageCom);
		tTransferData.setNameAndValue("WageNo", mWageNo);
		tTransferData.setNameAndValue("GlobalInput", mGlobalInput);
		tTransferData.setNameAndValue("BranchType", mBranchType);
		tTransferData.setNameAndValue("BranchType2", mBranchType2);
		tInputData.add(tTransferData);
		LATrainerAgentMonthCal tLATrainerAgentMonthCal = new LATrainerAgentMonthCal();
		// 计算八位管理机构下所有营业部的薪资项
		if (!tLATrainerAgentMonthCal.submitData(tInputData, "")) {
			this.mErrors.copyAllErrors(tLATrainerAgentMonthCal.mErrors);
			return false;
		}

		// 计算完成，回复原来的计算
		if (mBranchType.equals("1") && mBranchType2.equals("01")) {
			if (!returnState()) {
				return false;
			}
		}

		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		// 全局变量
		mManageCom = (String) cInputData.get(0);
		mBranchType = (String) cInputData.get(1);
		mBranchType2 = (String) cInputData.get(2);
		mWageNo = (String) cInputData.get(3);
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 4));
		System.out.println("mManageCom" + mManageCom);
		return true;

	}

	// 个险组训人员佣金开始试算，插入state = '11'，表示正在计算，其他人不能再算
	private boolean insertState() {
		String tSQL1 = "update LATrainerWageHistory set state = '11',modifydate=current date ,modifytime=current time  where "
				+ " wageno = '"
				+ mWageNo
				+ "' and ManageCom like '"
				+ mManageCom
				+ "%' and BranchType = '"
				+ mBranchType
				+ "' and BranchType2 ='" + mBranchType2 + "' and AClass = '03'";
		System.out.println("最后更新LATrainerWageHistory表中state记录的SQL: " + tSQL1);
		ExeSQL tExeSQL = new ExeSQL();
		if (!tExeSQL.execUpdateSQL(tSQL1)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tExeSQL.mErrors);
			CError tError = new CError();
			tError.moduleName = "LATrainerWageCalculateBL";
			tError.functionName = "insertState";
			tError.errorMessage = "执行SQL语句,更新LATrainerWageHistory表记录失败!";
			this.mErrors.addOneError(tError);
			System.out.println("执行SQL语句,更新LATrainerWageHistory表表中state记录失败!");
			return false;
		}
		return true;
	}

	// 个险佣金试算结束，回到原来的状态，插入state = '12'，表示可以计算
	private boolean returnState() {
		String tSQL1 = "update LATrainerWageHistory set state = '12',modifydate=current date ,modifytime=current time where "
				+ " wageno = '"
				+ mWageNo
				+ "' and ManageCom like '"
				+ mManageCom
				+ "%' and BranchType = '"
				+ mBranchType
				+ "' and BranchType2 ='" + mBranchType2 + "' and AClass = '03'";
		System.out.println("最后更新LATrainerWageHistory表中state记录的SQL: " + tSQL1);
		ExeSQL tExeSQL = new ExeSQL();
		if (!tExeSQL.execUpdateSQL(tSQL1)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tExeSQL.mErrors);
			CError tError = new CError();
			tError.moduleName = "LATrainerWageCalculateBL";
			tError.functionName = "returnState";
			tError.errorMessage = "执行SQL语句,更新LATrainerWageHistory表记录失败!";
			this.mErrors.addOneError(tError);
			System.out.println("执行SQL语句,更新LATrainerWageHistory表表中state记录失败!");
			return false;
		}
		return true;
	}

	// 检查是否正在计算，不能同时多用户计算
	private boolean checkState() {
		String tSQL = "select * from laTrainerWagehistory where managecom like '"
				+ mManageCom
				+ "%' and wageno='"
				+ mWageNo
				+ "' and branchtype='"
				+ mBranchType
				+ "' and BranchType2='"
				+ mBranchType2 + "' and aclass='03' ";
		LATrainerWageHistorySet tLATrainerWageHistorySet = new LATrainerWageHistorySet();
		LATrainerWageHistoryDB tLATrainerWageHistoryDB = new LATrainerWageHistoryDB();
		tLATrainerWageHistorySet = tLATrainerWageHistoryDB.executeQuery(tSQL);

		if (tLATrainerWageHistoryDB.mErrors.needDealError()) {
			this.mErrors.copyAllErrors(tLATrainerWageHistoryDB.mErrors);
			return false;
		} else if (tLATrainerWageHistorySet.size() == 0) {
			buildError("checkState", "本月没有进行提数计算");
			return false;
		} else if (tLATrainerWageHistorySet.size() > 0
				&& tLATrainerWageHistorySet.get(1).getState().equals("13")) {
			buildError("checkState", "本月佣金已经确认，不能再计算");
			return false;
		} else if (tLATrainerWageHistorySet.size() > 0
				&& tLATrainerWageHistorySet.get(1).getState().equals("11")) {
				buildError("checkState", "本月佣金正在进行计算，不能计算");
				return false;
		}
		return true;
	}

	// 删除原来的佣金计算的数据
	private boolean deleteWage() {
		String tSql = "delete  from  latrainerWageIndex  where  managecom = '"
				+ mManageCom
				+ "' and WageNo>='"
				+ mWageNo
				+ "' and branchtype='"
				+ mBranchType
				+ "' and BranchType2='"
				+ mBranchType2 + "' ";
		ExeSQL tExeSQL = new ExeSQL();
		tExeSQL.execUpdateSQL(tSql);
		if (tExeSQL.mErrors.needDealError()) {
			// @@错误处理
			this.mErrors.copyAllErrors(tExeSQL.mErrors);
			CError tError = new CError();
			tError.moduleName = "LATrainerWageCalculateBL";
			tError.functionName = "returnState";
			tError.errorMessage = "删除原来的佣金数据失败!";
			this.mErrors.addOneError(tError);
			System.out.println("删除原来的佣金数据失败latrainerWageIndex!");
			return false;
		}
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "LATrainerWageCalculateBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public CErrors getErrors() {
		return mErrors;
	}

}
