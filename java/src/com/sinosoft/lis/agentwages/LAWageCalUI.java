/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agentwages;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAWageCalUI
{

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();

    public LAWageCalUI()
    {
    }

    public boolean submitData(VData cInputData)
    {
        mInputData = (VData) cInputData.clone();
        LAWageCalBL tLAWageCalBL = new LAWageCalBL();
        if (!tLAWageCalBL.submitData(mInputData))
        {
            this.mErrors.copyAllErrors(tLAWageCalBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LACalWageUI";
            tError.functionName = "submitData";
            tError.errorMessage = tLAWageCalBL.mErrors.getFirstError();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {
        LACalWageUI LACalWageUI1 = new LACalWageUI();
        LACalWageUI1.submitData(new VData());
    }
}
