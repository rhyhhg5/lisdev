package com.sinosoft.lis.agentwages;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class AgentWageCalFYCReUI
{
    public CErrors mErrors = new CErrors();
    private VData mOutputData = new VData();
    public AgentWageCalFYCReUI()
    {
    }

    public boolean submitData(VData cInputData)
    {
        mOutputData = (VData) cInputData.clone();
        AgentWageCalFYCReBL tAgentWageCalFYCReBL = new AgentWageCalFYCReBL();
        if (!tAgentWageCalFYCReBL.submitData(mOutputData))
        {
            this.mErrors.copyAllErrors(tAgentWageCalFYCReBL.mErrors);
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {
        AgentWageCalFYCReUI agentWageCalFYCReUI1 = new AgentWageCalFYCReUI();
    }
}
