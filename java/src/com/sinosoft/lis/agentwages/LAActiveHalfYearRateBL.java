package com.sinosoft.lis.agentwages;


import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentwages.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.db.*;

/**
 * <p>Title: LAActiveHalfYearRateBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author xin
 * @version 1.0
 */
public class LAActiveHalfYearRateBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";

  private MMap map = new MMap();
  public  GlobalInput mGlobalInput = new GlobalInput();

  private LAWageRadix5Set mLAWageRadix5Set  = new LAWageRadix5Set();

  public LAActiveHalfYearRateBL() {

  }

  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LAActiveHalfYearRateBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start LAActiveHalfYearRateBL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAActiveHalfYearRateBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LAActiveHalfYearRateBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     System.out.println("GlobalInput get");
     this.mLAWageRadix5Set.set((LAWageRadix5Set) cInputData.get(1));
     System.out.println("LAWageRadix5Set get"+mLAWageRadix5Set.size());
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAActiveHalfYearRateBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LAActiveHalfYearRateBL.dealData........."+mOperate);
    try {
          LAWageRadix5Set tLAWageRadix5SetNew = new LAWageRadix5Set();
    	  for (int i = 1; i <= mLAWageRadix5Set.size(); i++) {
    		  LAWageRadix5Schema tLAWageRadix5Schemanew = new  LAWageRadix5Schema();
    		  LAWageRadix5Schema tLAWageRadix5Schemaold = new  LAWageRadix5Schema();
    		  LAWageRadix5DB tLAWageRadix5DB = new LAWageRadix5DB();
    		  tLAWageRadix5Schemanew = mLAWageRadix5Set.get(i);
    		  int idx = tLAWageRadix5Schemanew.getIdx();
    		  tLAWageRadix5DB.setIdx(idx);
    		  if(tLAWageRadix5DB.getInfo())
    		  {
    			  tLAWageRadix5Schemaold = tLAWageRadix5DB.getSchema();
    			  if(mOperate.equals("INSERT"))
    			  {
    				  CError tError = new CError();
    				  tError.moduleName = "LAActiveHalfYearRateBL";
    				  tError.functionName = "dealData";
    				  tError.errorMessage = tLAWageRadix5Schemanew.getManageCom()+"下的职级"+tLAWageRadix5Schemanew.getAgentGrade()+"已经录入过底薪！";
    				  this.mErrors.addOneError(tError);
    				  break;
    			  }
    			  if(mOperate.equals("UPDATE"))
    			  {
    				  tLAWageRadix5Schemaold.setRewardRate(tLAWageRadix5Schemanew.getRewardRate());
    				  tLAWageRadix5Schemaold.setModifyDate(PubFun.getCurrentDate());
    				  tLAWageRadix5Schemaold.setModifyTime(PubFun.getCurrentTime());
    				  tLAWageRadix5SetNew.add(tLAWageRadix5Schemaold);
    			  }
    		  }
    		  else
    		  {
    			  tLAWageRadix5Schemanew.setOperator(this.mGlobalInput.Operator);
    			  tLAWageRadix5Schemanew.setMakeDate(PubFun.getCurrentDate());
    			  tLAWageRadix5Schemanew.setMakeTime(PubFun.getCurrentTime());
    			  tLAWageRadix5Schemanew.setModifyDate(PubFun.getCurrentDate());
    			  tLAWageRadix5Schemanew.setModifyTime(PubFun.getCurrentTime());
    			  tLAWageRadix5SetNew.add(tLAWageRadix5Schemanew);
    		  }
              
          }
            map.put(tLAWageRadix5SetNew, mOperate);
      }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAActiveHalfYearRateBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      System.out.println(
          "Begin LAActiveHalfYearRateBL.prepareOutputData.........");
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAActiveHalfYearRateBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
