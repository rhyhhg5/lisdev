package com.sinosoft.lis.agentwages;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class AgentHealthWage1XLSUI
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    public AgentHealthWage1XLSUI()
    {
        System.out.println("AgentHealthWage1XLSUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
       AgentHealthWage1XLSBL bl = new AgentHealthWage1XLSBL();
        if(!bl.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        AgentHealthWage1XLSUI tAgentHealthWage1XLSUI = new   AgentHealthWage1XLSUI();
    }
}
