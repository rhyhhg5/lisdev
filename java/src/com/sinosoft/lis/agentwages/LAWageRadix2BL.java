package com.sinosoft.lis.agentwages;


import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentwages.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.db.*;

/**
 * <p>Title: LAWageRadix2BL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author xin
 * @version 1.0
 */
public class LAWageRadix2BL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private String mRadixType = "";

  private MMap map = new MMap();
  public  GlobalInput mGlobalInput = new GlobalInput();

  private LAWageRadix2Set mLAWageRadix2Set  = new LAWageRadix2Set();
  private LAWageRadix2Set mmLAWageRadix2Set = new LAWageRadix2Set();
  private Reflections ref = new Reflections();

  public LAWageRadix2BL() {

  }


  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LAWageRadix2BL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start LAWageRadix2BL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAWageRadix2BL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LAWageRadix2BL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     System.out.println("GlobalInput get");
     this.mRadixType = (String) cInputData.get(1);
     this.mLAWageRadix2Set.set((LAWageRadix2Set) cInputData.get(2));
     if(mRadixType.equals("08")||mRadixType.equals("09"))
     {
     this.mmLAWageRadix2Set.set((LAWageRadix2Set) cInputData.get(3));
     }
     System.out.println("LAWageRadix2Set get"+mLAWageRadix2Set.size());

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAWageRadix2BL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LAWageRadix2BL.dealData........."+mOperate);
    try {
        if (mOperate.equals("UPDATE") ) {

          System.out.println("Begin LAWageRadix2BL.dealData.........1"+mLAWageRadix2Set.size());
          LAWageRadix2Set tLAWageRadix2Set = new LAWageRadix2Set();
          for (int i = 1; i <= mLAWageRadix2Set.size(); i++) {
            System.out.println("Begin LAWageRadix2BL.dealData.........2");
            LAWageRadix2Schema tLAWageRadix2Schemanew = new  LAWageRadix2Schema();
            LAWageRadix2Schema tLAWageRadix2Schemaold = new  LAWageRadix2Schema();
            LAWageRadix2DB tLAWageRadix2DB = new LAWageRadix2DB();

            tLAWageRadix2Schemanew = mLAWageRadix2Set.get(i);
            tLAWageRadix2DB.setIdx(tLAWageRadix2Schemanew.getIdx());
            tLAWageRadix2Schemaold = tLAWageRadix2DB.query().get(1);
            //岗位津贴
            if(mRadixType.equals("WP1017"))
            {
            	if(tLAWageRadix2Schemanew.getAgentGrade().equals(tLAWageRadix2Schemaold.getAgentGrade())
               		 &&tLAWageRadix2Schemanew.getWageCode().equals(tLAWageRadix2Schemaold.getWageCode())
               		 &&tLAWageRadix2Schemanew.getWageName().equals(tLAWageRadix2Schemaold.getWageName())
               		 &&tLAWageRadix2Schemanew.getAreaType().equals(tLAWageRadix2Schemaold.getAreaType())
               		 &&tLAWageRadix2Schemanew.getChannelType().equals(tLAWageRadix2Schemaold.getChannelType())
               		 &&tLAWageRadix2Schemanew.getBranchType().equals(tLAWageRadix2Schemaold.getBranchType())
               		 &&tLAWageRadix2Schemanew.getLimitPeriod()==tLAWageRadix2Schemaold.getLimitPeriod()
               		 &&tLAWageRadix2Schemanew.getRewardMoney()==tLAWageRadix2Schemaold.getRewardMoney()
               		 &&tLAWageRadix2Schemanew.getDrawStart()==tLAWageRadix2Schemaold.getDrawStart()
               		 &&tLAWageRadix2Schemanew.getDrawEnd()==tLAWageRadix2Schemaold.getDrawEnd()
               		 &&tLAWageRadix2Schemanew.getDrawRateOth()==tLAWageRadix2Schemaold.getDrawRateOth()
               		 &&tLAWageRadix2Schemanew.getBranchType2().equals(tLAWageRadix2Schemaold.getBranchType2())
                   ){
                       	if( !(tLAWageRadix2Schemanew.getCond1()>=tLAWageRadix2Schemaold.getCond2()
                          		&&tLAWageRadix2Schemanew.getCond2()<=tLAWageRadix2Schemaold.getCond1()))
                       	{
           	            	CError tError = new CError();
           				    tError.moduleName = "LAWageRadix2BL";
           				    tError.functionName = "dealData";
           				    tError.errorMessage = tLAWageRadix2Schemanew.getAgentGrade()+"职级的晋升奖FYC区间有重复值,";
           				    this.mErrors.addOneError(tError);
           				    break;
                       	}
                   }
            	tLAWageRadix2Set.add(tLAWageRadix2Schemanew);
            }
            map.put(tLAWageRadix2Set, mOperate);
          }
      }
     else if (mOperate.equals("DELETE")) {
             System.out.println("Begin LAWageRadix2BL.dealData.........1"+mLAWageRadix2Set.size());
             LAWageRadix2Set tLAWageRadix2Set = new LAWageRadix2Set();
             for (int i = 1; i <= mLAWageRadix2Set.size(); i++) {
                 System.out.println("Begin LAWageRadix2BL.dealData.........2");
                 LAWageRadix2Schema tLAWageRadix2Schemanew = new  LAWageRadix2Schema();
                 tLAWageRadix2Schemanew = mLAWageRadix2Set.get(i);
                 tLAWageRadix2Set.add(tLAWageRadix2Schemanew);
             }
             if(mRadixType.equals("08")||mRadixType.equals("09"))
            {
              for (int i = 1; i <= mmLAWageRadix2Set.size(); i++) {
              System.out.println("Begin LAWageRadix2BL.dealData.........2");
              LAWageRadix2Schema ttLAWageRadix2Schemanew = new  LAWageRadix2Schema();
              ttLAWageRadix2Schemanew = mmLAWageRadix2Set.get(i);
              tLAWageRadix2Set.add(ttLAWageRadix2Schemanew);
            }
            }
               map.put(tLAWageRadix2Set, mOperate);
      }
      else {
	      LAWageRadix2Set tLAWageRadix2Set = new   LAWageRadix2Set();
	      for (int i = 1; i <= mLAWageRadix2Set.size(); i++) {
	       LAWageRadix2Schema tLAWageRadix2Schemanew = new    LAWageRadix2Schema();
	       tLAWageRadix2Schemanew = mLAWageRadix2Set.get(i);
	       LAWageRadix2Schema tLAWageRadix2Schemaold = new  LAWageRadix2Schema();
	       LAWageRadix2DB tLAWageRadix2DB = new LAWageRadix2DB();
	
	     //岗位津贴
	       if(mRadixType.equals("WP1017"))
	       {
	    	   tLAWageRadix2DB.setAgentGrade(tLAWageRadix2Schemanew.getAgentGrade())  ;
		       tLAWageRadix2DB.setWageCode(tLAWageRadix2Schemanew.getWageCode())    ;
		       tLAWageRadix2DB.setWageName(tLAWageRadix2Schemanew.getWageName())    ;
		       tLAWageRadix2DB.setAreaType(tLAWageRadix2Schemanew.getAreaType())    ;
		       tLAWageRadix2DB.setChannelType(tLAWageRadix2Schemanew.getChannelType())    ;
		       tLAWageRadix2DB.setBranchType(tLAWageRadix2Schemanew.getBranchType())  ;
		       tLAWageRadix2DB.setLimitPeriod(tLAWageRadix2Schemanew.getLimitPeriod())  ;
		       tLAWageRadix2DB.setRewardMoney(tLAWageRadix2Schemanew.getRewardMoney())  ;
		       tLAWageRadix2DB.setDrawStart(tLAWageRadix2Schemanew.getDrawStart())    ;
		       tLAWageRadix2DB.setDrawEnd(tLAWageRadix2Schemanew.getDrawEnd())     ;
		       tLAWageRadix2DB.setDrawRateOth(tLAWageRadix2Schemanew.getDrawRateOth())  ;
		       tLAWageRadix2DB.setBranchType2(tLAWageRadix2Schemanew.getBranchType2()) ;
		       tLAWageRadix2Schemaold = tLAWageRadix2DB.query().get(1);
		       if(null!=tLAWageRadix2Schemaold)
		       {
		    	   if( !(tLAWageRadix2Schemanew.getCond1()>=tLAWageRadix2Schemaold.getCond2()
		    			   ||tLAWageRadix2Schemanew.getCond2()<=tLAWageRadix2Schemaold.getCond1()))
		    	   {
		    		   CError tError = new CError();
		    		   tError.moduleName = "LAWageRadix2BL";
		    		   tError.functionName = "dealData";
		    		   tError.errorMessage = tLAWageRadix2Schemanew.getAgentGrade()+"职级的岗位津贴录入的FYC区间有重复值,";
		    		   this.mErrors.addOneError(tError);
		    		   break;
		    	   }
		       }
	       }
	       tLAWageRadix2Set.add(tLAWageRadix2Schemanew);
        }
        map.put(tLAWageRadix2Set, mOperate);
       }
      }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAWageRadix2BL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      System.out.println(
          "Begin LAWageRadix2BL.prepareOutputData.........");
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAWageRadix2BL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
