package com.sinosoft.lis.agentwages;

import java.sql.Connection;

import com.sinosoft.lis.vdb.LACommisionDBSet;
import com.sinosoft.lis.vdb.LAWageHistoryDBSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAWageHistorySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class AgentWageCalFYCReBLS
{
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private LAWageHistorySet mLAWageHistorySet = new LAWageHistorySet();
    public AgentWageCalFYCReBLS()
    {
    }

    public boolean submitData(VData cInputData)
    {
        mInputData = (VData) cInputData.clone();
        if (!saveToLACommision())
        {
            return false;
        }
        return true;
    }

    private boolean saveToLACommision()
    {
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentWageCalFYCReBLS";
            tError.functionName = "saveLACommision";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            mLACommisionSet = (LACommisionSet) mInputData.getObjectByObjectName(
                    "LACommisionSet", 0);
            LACommisionDBSet tLACommisionDBSet = new LACommisionDBSet(conn);
            tLACommisionDBSet.add(mLACommisionSet);
            if (!tLACommisionDBSet.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLACommisionDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentWageCalFYCReBLS";
                tError.functionName = "saveLACommision";
                tError.errorMessage = "更新机构为" +
                                      mLACommisionSet.get(1).getManageCom() +
                                      "的数据失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            mLAWageHistorySet = (LAWageHistorySet) mInputData.
                                getObjectByObjectName("LAWageHistorySet", 0);
            LAWageHistoryDBSet tLAWageHistoryDBSet = new LAWageHistoryDBSet(
                    conn);
            tLAWageHistoryDBSet.add(mLAWageHistorySet);
            if (!tLAWageHistoryDBSet.update())
            {
                this.mErrors.copyAllErrors(tLAWageHistoryDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentWageCalFYCReBLS";
                tError.functionName = "saveLACommision";
                tError.errorMessage = "插入佣金历史记录时失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
            return true;
        }
        catch (Exception ex)
        {
            CError tError = new CError();
            tError.moduleName = "AgentWageCalFYCReBLS";
            tError.functionName = "saveToLACommision";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
    }

    public static void main(String[] args)
    {
        AgentWageCalFYCReBLS agentWageCalFYCReBLS1 = new AgentWageCalFYCReBLS();
    }
}
