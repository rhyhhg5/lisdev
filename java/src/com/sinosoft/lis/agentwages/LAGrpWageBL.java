/*
 * <p>ClassName: LAWageBL </p>
 * <p>Description: LAWageBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.agentwages;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAWageDB;
import com.sinosoft.lis.db.LAWageHistoryDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.Reflections;

public class LAGrpWageBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /*时间变量*/
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAWageSchema mLAWageSchema = new LAWageSchema();
    private LAWageSet mLAWageSet = new LAWageSet();
    private LAAgentSchema mLAAgentSchema=new LAAgentSchema();
    private String mAgentCode = "";
    private String mBranchCode = "";
    private String mflag = "";
    private String[] mSpecialGroups = null;
    private LAWageHistorySchema mLAWageHistorySchema = new LAWageHistorySchema();
    private LAWageHistorySet mLAWageHistorySet = new LAWageHistorySet();
    private MMap mmMap = new MMap();
    public LAGrpWageBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGrpWageBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALAWageBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start LAGrpWageBL Submit...");
            LAWageBLS tLAWageBLS = new LAWageBLS();
            tLAWageBLS.submitData(mInputData, cOperate);
            System.out.println("End LAGrpWageBL Submit...");
            //如果有需要处理的错误，则返回
            if (tLAWageBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAWageBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAGrpWageBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
     // 进行业务处理
        if (!dealDataDa())
        {
             // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageDaBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAWageDaBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!prepareOutputDataDa())
        {
            return false;
        }
        PubSubmit tPubSubmitDa = new PubSubmit();
        System.out.println("Start LAWageDaBL Submit...");
        if (!tPubSubmitDa.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmitDa.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAWageDaBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        double lastmoney = 0;
        double currmoney = 0;
        String agentCode = "";
        if(mflag=="0")
        {//根据前台数据全部处理，但是SumMoney不小于0
            LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();

            System.out.println("----:" + mLAWageSchema.getAgentGroup());

            String sql = "update lawage set state='1' where state='0'   and indexcalno='" +
                         mLAWageSchema.getIndexCalNo() +
                         "' and managecom like '" +
                         mLAWageSchema.getManageCom() +
                         "%' and managecom like '" +
                         mGlobalInput.ManageCom + "%' and BranchType='" +
                         mLAWageSchema.getBranchType()
                         + "' and BranchType2='" + mLAWageSchema.getBranchType2() +
                         "'";
            if (mLAWageSchema.getAgentCode() != null &&
                !mLAWageSchema.getAgentCode().trim().equals("")) {
                sql = sql + " and agentcode='" + mLAWageSchema.getAgentCode() +
                      "'";
            }
            if (mLAAgentSchema.getName() != null &&
                !mLAAgentSchema.getName().trim().equals("")) {
                if (!checkname()) {
                    return false;
                }
                sql = sql + " and exists ( select a.agentcode from laagent a  where a.agentcode=lawage.agentcode and a.name='" +
                      mLAAgentSchema.getName().trim() + "') ";
            }

            if (mLAWageSchema.getAgentGroup() != null &&
                !mLAWageSchema.getAgentGroup().equals("")) {
                if (!checkagentgroup()) {
                    return false;
                }
                sql = sql + " and branchattr like '" +
                      mLAWageSchema.getAgentGroup() +
                      "%' ";
            }
            ExeSQL tExeSQL = new ExeSQL();
            if (!tExeSQL.execUpdateSQL(sql)) {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentWageCalDoBL";
                tError.functionName = "dealData";
                tError.errorMessage = "薪筹审核失败!";
                this.mErrors.addOneError(tError);
                return false;

            }
        }
        else
        {//做个案处理SumMoney可以小于0 2005-1107 modify:xiangchun
            Reflections tReflections = new Reflections();
            LAWageSet tLAWageSet=new LAWageSet();
            tLAWageSet=mLAWageSet;
            mLAWageSet=new LAWageSet();
            for(int i=1;i<=tLAWageSet.size();i++)
            {
                mLAWageSchema=new LAWageSchema();
                LAWageDB tLAWageDB=new LAWageDB();
                LAWageSchema tLAWageSchema=new LAWageSchema();
                mLAWageSchema=tLAWageSet.get(i);
                tLAWageDB.setManageCom(mLAWageSchema.getManageCom());
                tLAWageDB.setAgentCode(mLAWageSchema.getAgentCode());
                tLAWageDB.setIndexCalNo(mLAWageSchema.getIndexCalNo());
                tLAWageDB.setBranchType(mLAWageSchema.getBranchType());
                tLAWageDB.setBranchType2(mLAWageSchema.getBranchType2());
                tLAWageSchema=tLAWageDB.getSchema();
                if(tLAWageSchema.getState().trim().equals("1"))
                {
                    CError tError = new CError();
                     tError.moduleName = "ALAWageBL";
                     tError.functionName = "dealdata";
                     tError.errorMessage = "销售员代码为："+tLAWageSchema.getAgentCode()+",月份为："+tLAWageSchema.getIndexCalNo()+"的薪资已发放,不需要再审核";
                     this.mErrors.addOneError(tError);
                     return false;
                }
                tLAWageSchema.setState("1");
                mLAWageSet.add(tLAWageSchema);
            }

        }
        tReturn = true;
        return tReturn;
    }

    /**
     * 用一个数组来存储所有特殊组的编码  //cg added in 1月13日
     * @return
     */
    private String[] getSpecialAgentGroup()
    {
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String tSql =
                "Select trim(AgentGroup) from labranchgroup where state = '1' "
                + "and BranchLevel = '01'  And BranchType = '1'";
        tSSRS = tExeSQL.execSQL(tSql);
        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoBL";
            tError.functionName = "checkData";
            tError.errorMessage = "查询客服组出错!";
            this.mErrors.addOneError(tError);
            return null;
        }
        String[] tSpecialAgentGroups = new String[tSSRS.getMaxRow()];
        for (int i = 0; i < tSpecialAgentGroups.length; i++)
        {
            tSpecialAgentGroups[i] = tSSRS.GetText(i + 1, 1);
        }
        return tSpecialAgentGroups;
    }

    /**
     * 判断该人是否是特殊组成员  //cg added in 1月13日
     * @param tAgentGroup
     * @return
     */
    private boolean isInSpecialGroup(String tAgentCode)
    {
        String tBranchCode = "";
        if (this.mSpecialGroups == null)
        {
            mSpecialGroups = getSpecialAgentGroup();
        }
        if (mSpecialGroups == null)
        {
            return false;
        }
        if (!this.mAgentCode.equals(tAgentCode))
        {
            mAgentCode = tAgentCode;
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tAgentCode);
            if (!tLAAgentDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAWageBL";
                tError.functionName = "submitData";
                tError.errorMessage = "查询代理人信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tBranchCode = tLAAgentDB.getBranchCode();
            mBranchCode = tBranchCode;
        }
        else
        {
            tBranchCode = mBranchCode;
        }
        for (int i = 0; i < mSpecialGroups.length; i++)
        {
            if (tBranchCode.equals(mSpecialGroups[i]))
            {
                System.out.println("特殊业务");
                return true;
            }
        }
        return false;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAWageSchema.setSchema((LAWageSchema) cInputData.
                                     getObjectByObjectName("LAWageSchema", 0));
        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                     getObjectByObjectName("LAAgentSchema", 0));
        this.mLAWageSet.set((LAWageSet) cInputData.
                                     getObjectByObjectName("LAWageSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
        mflag=cInputData.get(4).toString();
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start ALAWageBLQuery Submit...");
        LAWageDB tLAWageDB = new LAWageDB();
        tLAWageDB.setSchema(this.mLAWageSchema);
        this.mLAWageSet = tLAWageDB.query();
        this.mResult.add(this.mLAWageSet);
        System.out.println("End ALAWageBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLAWageDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAWageDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAWageBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAWageSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAWageBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    private boolean checkname()
    {
        if (mLAWageSchema.getAgentCode() != null &&
            !mLAWageSchema.getAgentCode().trim().equals(""))
        {
            LAAgentDB tLAAgentDB=new LAAgentDB();
            tLAAgentDB.setAgentCode(mLAWageSchema.getAgentCode());
            tLAAgentDB.setName(mLAAgentSchema.getName());
            tLAAgentDB.query();
            LAAgentSet tLAAgentSet=new LAAgentSet();
            tLAAgentSet=tLAAgentDB.query();
            if (tLAAgentDB.mErrors.needDealError())
             {
                 // @@错误处理
                 this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                 CError tError = new CError();
                 tError.moduleName = "ALAWageBL";
                 tError.functionName = "checkname";
                 tError.errorMessage = "没有查到业务员代码为"+mLAWageSchema.getAgentCode()+"且业务员为姓名为"+mLAAgentSchema.getName()+"的信息！！！";
                 this.mErrors.addOneError(tError);
                 return false;
          }
            System.out.println("123223"+tLAAgentSet);
            if(tLAAgentSet.size()<=0)

            {
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAWageBL";
                tError.functionName = "checkname";
                tError.errorMessage = "没有查到业务员代码为"+mLAWageSchema.getAgentCode()+"且业务员为姓名为"+mLAAgentSchema.getName()+"的信息！！！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true ;
    }
    private boolean checkagentgroup()
    {
        if (mLAAgentSchema.getName() != null &&
           !mLAAgentSchema.getName().trim().equals(""))
       {
           String sql = "select 'X' from laagent  where name='" +
                        mLAAgentSchema.getName() + "' and branchtype='" +
                        mLAWageSchema.getBranchType() + "' and  branchtype2='" +
                        mLAWageSchema.getBranchType2() + "' and exists(select 'X' from labranchgroup a  where laagent.agentgroup=a.agentgroup and a.branchattr='"+mLAWageSchema.getAgentGroup()+"')";
           ExeSQL tExeSQL = new ExeSQL();
           SSRS tSSRS = new SSRS();
           tSSRS = tExeSQL.execSQL(sql);
           if (tExeSQL.mErrors.needDealError() || tSSRS.getMaxRow()<=0)
           {
               this.mErrors.copyAllErrors(tExeSQL.mErrors);
               CError tError = new CError();
               tError.moduleName = "AgentWageCalDoBL";
               tError.functionName = "checkname";
               tError.errorMessage = "没有查到业务员为姓名为" + mLAAgentSchema.getName() +
                                     "并且销售单位为"+mLAWageSchema.getAgentGroup()+"的信息！";
               this.mErrors.addOneError(tError);
               return false;
           }
       }
        if (mLAWageSchema.getAgentCode() != null &&
            !mLAWageSchema.getAgentCode().trim().equals(""))
        {
            String sql = "select 'X' from laagent  where agentcode='" +
                         mLAWageSchema.getAgentCode() + "' and branchtype='" +
                         mLAWageSchema.getBranchType() + "' and  branchtype2='" +
                         mLAWageSchema.getBranchType2() + "' and exists(select 'X' from labranchgroup a  where laagent.agentgroup=a.agentgroup and a.branchattr='"+mLAWageSchema.getAgentGroup()+"')";

           ExeSQL tExeSQL = new ExeSQL();
           SSRS tSSRS = new SSRS();
           tSSRS = tExeSQL.execSQL(sql);
           if (tExeSQL.mErrors.needDealError() || tSSRS.getMaxRow()<=0) {
               this.mErrors.copyAllErrors(tExeSQL.mErrors);
               CError tError = new CError();
               tError.moduleName = "AgentWageCalDoBL";
               tError.functionName = "checkname";
               tError.errorMessage = "没有查到业务员为代码为" + mLAWageSchema.getAgentCode() +
                                     "并且销售单位为"+mLAWageSchema.getAgentGroup()+"的信息！";
               this.mErrors.addOneError(tError);
               return false;
           }
        }
        return true;
    }
    public VData getResult()
    {
        return this.mResult;
    }
    /***************************************************************
     *  @ check date
     * @return
     */
    private boolean dealDataDa()
    {    	   
          String tSql="select * from LAWageHistory where managecom like '"+mLAWageSchema.getManageCom()+"%' and wageno='"
                        +mLAWageSchema.getIndexCalNo()+"' and branchtype='"+mLAWageSchema.getBranchType()+"' and branchtype2='"+mLAWageSchema.getBranchType2()
                        +"' and aclass='03' and state = '13' ";
          System.out.println("页面传过来的管理机构"+mLAWageSchema.getManageCom());
          LAWageHistoryDB tLAWageHistoryDB= new LAWageHistoryDB();
          LAWageHistorySet tLAWageHistorySet= new LAWageHistorySet();
          tLAWageHistorySet=tLAWageHistoryDB.executeQuery(tSql);
          if(tLAWageHistorySet.size() <= 0)
          {
            buildError("dealData","查询佣金计算历史表出错");
            return false;
          }
          for(int i = 1;i<=tLAWageHistorySet.size();i++)
          {
          	LAWageHistorySchema tLAWageHistorySchema= new LAWageHistorySchema();
          	tLAWageHistorySchema = tLAWageHistorySet.get(i).getSchema();
          	// 校验是否每个人都已薪资审核发放
            String tSQL="select distinct state  from  lawage  where managecom like '"+tLAWageHistorySchema.getManageCom()+"%' and indexcalno='"
            +tLAWageHistorySchema.getWageNo()+"' and (lastmoney+CurrMoney)>=0 and branchtype='"+tLAWageHistorySchema.getBranchType()+"' and branchtype2='"+tLAWageHistorySchema.getBranchType2()+"' order by state";
            ExeSQL tExeSQL = new ExeSQL();
            String tResult = tExeSQL.getOneValue(tSQL);
            boolean flag = false;
            if(tResult==null||"".equals(tResult))
             {
            	flag = true;
             }else if(("0").equals(tResult))
             {
            	 flag = false;
             }else{
            	 flag = true;
             }
            if(flag)
            {
          	if(tLAWageHistorySchema.getState().equals("14"))
              {
                  buildError("dealData","该月佣金已审核发放，不能再确认!");
                  return false;
              }
              if(tLAWageHistorySchema.getState().equals("12"))
              {
                  buildError("dealData","该月佣金计算未确认未完成，请先进行薪资确认操作!");
                  return false;
              }
              if(tLAWageHistorySchema.getState().equals("11"))
              {
                  buildError("dealData","该月佣金计算未确认未完成，请先进行薪资计算操作!");
                  return false;
              }
              tLAWageHistorySchema.setState("14");
              tLAWageHistorySchema.setModifyDate(currentDate);
              tLAWageHistorySchema.setModifyTime(currentTime);
              mLAWageHistorySet.add(tLAWageHistorySchema);
          }
            
          }
            //由于财务的要求，应付功能先不上线，此处注释掉    
          mmMap.put(this.mLAWageHistorySet, "UPDATE");
//       }

        return true;
    }
    /***************************
     * @ 
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAWageDaBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    private boolean prepareOutputDataDa()
    {
        mInputData.clear();
        mInputData.add(mmMap);
        return true;
    }
    
    // 薪资审核发放相关校验
    private boolean checkData()
    {
    	// 校验是否进行提数计算
    	ExeSQL tExeSQL = new ExeSQL();
    	String SQL = "select '1' from lawagehistory where branchtype ='"+mLAWageSchema.getBranchType()+"'" +
    			"and branchtype2 = '"+mLAWageSchema.getBranchType2()+"' and managecom like '"+mLAWageSchema.getManageCom()+"%'" +
    					"and wageno ='"+mLAWageSchema.getIndexCalNo()+"' ";
    	
    	String tResult = tExeSQL.getOneValue(SQL);
    	if(tResult==null||"".equals(tResult))
    	{
    		buildError("dealData","分公司"+this.mLAWageSchema.getManageCom()+"，"+mLAWageSchema.getIndexCalNo()+"尚未进行提数计算操作，无法进行审核发放操作！");
            return false; 
    		
    	}
    	
    	// 校验该月薪资是否已经进行确认
    	String check_sql = "select '1' from lawagehistory where branchtype ='"+mLAWageSchema.getBranchType()+"' " 
		+" and branchtype2 ='"+mLAWageSchema.getBranchType2()+"' and managecom like '"+mLAWageSchema.getManageCom()+"%'" 
		+" and wageno ='"+mLAWageSchema.getIndexCalNo()+"' and state ='13'";
    	String tMax_ch =tExeSQL.getOneValue(check_sql);
   	   if(tMax_ch==null||tMax_ch.equals(""))
	    {
		  buildError("dealData","分公司"+this.mLAWageSchema.getManageCom()+","+mLAWageSchema.getIndexCalNo()+"薪资状态不是确认状态,请先进行薪资确认操作！");
          return false; 
	    }
    	 return true;
    }
}
