package com.sinosoft.lis.agentwages;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author pengcheng
 * @version 1.0
 */
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class LAWageDaInsuredUI
{

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();


    public LAWageDaInsuredUI()
    {
    }

    public boolean submitData(VData cInputData)
    {
        mInputData = (VData) cInputData.clone();
        LAWageDaInsuredBL tLAWageDaInsuredBL = new LAWageDaInsuredBL();
        if (!tLAWageDaInsuredBL.submitData(mInputData))
        {
            this.mErrors.copyAllErrors(tLAWageDaInsuredBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "tLAWageDaInsuredUI";
            tError.functionName = "submitData";
            tError.errorMessage = tLAWageDaInsuredBL.mErrors.getFirstError();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
