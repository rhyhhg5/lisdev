package com.sinosoft.lis.agentwages;

/*
 * <p>ClassName: RefreshTableBL </p>
 * <p>Description: LAAssessBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2016-11-24
 */

import javax.servlet.jsp.tagext.TryCatchFinally;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class RefreshTableBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String mWageNo;

    public RefreshTableBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(String cWageNo, String cOperate)
    {
    	mOperate = cOperate;
    	mWageNo = cWageNo;
    	mInputData.clear();
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "RefreshTableBL";
            tError.functionName = "DealData";
            tError.errorMessage = "数据处理失败RefreshTableBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
        	PubSubmit tPubSubmit = new PubSubmit();
        	tPubSubmit.submitData(this.mInputData, "UPDATE");
			
		} catch (Exception e) {
			CError tError = new CError();
            tError.moduleName = "RefreshTableBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败RefreshTableBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
		}
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	MMap tMMap = new MMap();
    	if("INSERT".equals(mOperate))
    	{
    		String tUpSQL0 ="insert into lawagehistory values('"+mWageNo+"','00000000','03','1','','12','WageNo',current date,current time,current date,current time,'01')";
    		String tUpSQL1 ="insert into lawagehistory values('"+mWageNo+"','11111111','03','1','','12','WageNo',current date,current time,current date,current time,'01')";
    		String tUpSQL2 ="insert into lawagehistory values('"+mWageNo+"','22222222','03','1','','12','WageNo',current date,current time,current date,current time,'01')";
    		String tUpSQL3 ="insert into lawagehistory values('"+mWageNo+"','33333333','03','1','','12','WageNo',current date,current time,current date,current time,'01')";
    		String tUpSQL4 ="insert into lawagehistory values('"+mWageNo+"','44444444','03','1','','12','WageNo',current date,current time,current date,current time,'01')";
    		String tUpSQL5 ="insert into lawagehistory values('"+mWageNo+"','55555555','03','1','','12','WageNo',current date,current time,current date,current time,'01')";
    		String tUpSQL6 ="insert into lawagehistory values('"+mWageNo+"','66666666','03','1','','12','WageNo',current date,current time,current date,current time,'01')";
    		String tUpSQL7 ="insert into lawagehistory values('"+mWageNo+"','77777777','03','1','','12','WageNo',current date,current time,current date,current time,'01')";
    		String tUpSQL8 ="insert into lawagehistory values('"+mWageNo+"','88888888','03','1','','12','WageNo',current date,current time,current date,current time,'01')";
    		String tUpSQL9 ="insert into lawagehistory values('"+mWageNo+"','99999999','03','1','','12','WageNo',current date,current time,current date,current time,'01')";
    		tMMap.put(tUpSQL0, "INSERT");
    		tMMap.put(tUpSQL1, "INSERT");
    		tMMap.put(tUpSQL2, "INSERT");
    		tMMap.put(tUpSQL3, "INSERT");
    		tMMap.put(tUpSQL4, "INSERT");
    		tMMap.put(tUpSQL5, "INSERT");
    		tMMap.put(tUpSQL6, "INSERT");
    		tMMap.put(tUpSQL7, "INSERT");
    		tMMap.put(tUpSQL8, "INSERT");
    		tMMap.put(tUpSQL9, "INSERT");
    		this.mInputData.add(tMMap);
    	}
    	else if("DELETE".equals(mOperate))
    	{
    		String tUpSQL ="delete from lawagehistory where branchtype='1' and branchtype2='01' and wageno ='"+mWageNo+"' and managecom in ('00000000','11111111','22222222','33333333','44444444','55555555','66666666','77777777','88888888','99999999')";
    		tMMap.put(tUpSQL, "DELETE");
    		this.mInputData.add(tMMap);
    	}
    	else if("REFRESH".equals(mOperate))
    	{
    		String tUpSQL ="delete from lawagehistory where branchtype='1' and branchtype2='01' and wageno ='"+mWageNo+"' and managecom in ('00000000','11111111','22222222','33333333','44444444','55555555','66666666','77777777','88888888','99999999')";
    		ExeSQL tExeSQL = new ExeSQL();
    		String tSql ="select count from lawagehistory where branchtype='1' and branchtype2='01' and state ='12' and managecom not in  ('00000000','11111111','22222222','33333333','44444444','55555555','66666666','77777777','88888888','99999999') with ur";
    		String tCalCount = tExeSQL.getOneValue(tSql);
    		if((Integer.parseInt(tCalCount))>0)
    		{
    			CError tError = new CError();
                tError.moduleName = "RefreshTableBL";
                tError.functionName = "submitData";
                tError.errorMessage = "目前有机构正在计算薪资，不能进行刷新，请稍后再试";
                this.mErrors.addOneError(tError);
    			return false;
    		}
    		try {
    			tExeSQL.execUpdateSQL("refresh table LATEMPMISION");
			} catch (Exception e) {
				// TODO: handle exception
				tExeSQL.execUpdateSQL(tUpSQL);
				CError tError = new CError();
                tError.moduleName = "RefreshTableBL";
                tError.functionName = "submitData";
                tError.errorMessage = "刷新表latempmision失败！";
                this.mErrors.addOneError(tError);
    			return false;
			}
    		finally
    		{
    			tExeSQL.execUpdateSQL(tUpSQL);
    		}
    	}
        return true;
    }

}
