package com.sinosoft.lis.agentwages;


/**
 * 根据外部传入的四位ManageCom，查出所有与之
 * 对应的八位ManageCom，循环调用AgentWageCalFYCReUI,
 * 对该八位的ManageCom进行处理。
 */
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class AgentWageLoopCalFYC
{
    public CErrors mErrors = new CErrors();
    private String mManageCom = "";
    private String mIndexCalNo = "";
    private String mBranchType = "";
    private String mBranchType2 = "";
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    private VData mOutputData = new VData();
    public AgentWageLoopCalFYC()
    {
    }

    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            mBranchType = (String) mTransferData.getValueByName("BranchType");
            mBranchType2=(String)mTransferData.getValueByName("BranchType2") ;
            mManageCom = (String) mTransferData.getValueByName("ManageCom");
            mIndexCalNo = (String) mTransferData.getValueByName("IndexCalNo");
            return true;
        }
        catch (Exception e)
        {
            CError tError = new CError();
            tError.moduleName = "AgentWageLoopCalFYC";
            tError.functionName = "getInputData";
            tError.errorMessage = "getInputData" + e.toString() + "!";
            return false;
        }

    }

    private boolean dealData()
    {
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        String tSQL = "select comcode from ldcom where comcode like '" +
                      mManageCom + "%'"
                      //xjh Delete 2005/02/26,扩展
                      +"and length(trim(comcode)) = 8"
                      ;
        tSSRS = tExeSQL.execSQL(tSQL);
        for (int i = 0; i < tSSRS.getMaxRow(); i++)
        {
            String tManageCom = tSSRS.GetText(i + 1, 1);
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("BranchType", mBranchType);
            tTransferData.setNameAndValue("BranchType2",mBranchType2) ;
            tTransferData.setNameAndValue("ManageCom", tManageCom);
            tTransferData.setNameAndValue("IndexCalNo", mIndexCalNo);
            VData tVData = new VData();
            tVData.add(tTransferData);
            tVData.add(mGlobalInput);
            AgentWageCalFYCReUI tAgentWageCalFYCReUI = new AgentWageCalFYCReUI();
            if (!tAgentWageCalFYCReUI.submitData(tVData))
            {
                this.mErrors.copyAllErrors(tAgentWageCalFYCReUI.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentWageLoopCalFYC";
                tError.functionName = "dealData";
                tError.errorMessage = "重算" + tManageCom + "的FYC时出错!";
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args)
    {
        AgentWageLoopCalFYC agentWageLoopCalFYC1 = new AgentWageLoopCalFYC();
        try
        {
            PrintStream out =
                    new PrintStream(
                            new BufferedOutputStream(
                                    new FileOutputStream(
                    "AgentWageLoopCalFYCReBL.out")));
            System.setOut(out);

            GlobalInput tGlobalInput = new GlobalInput();
            tGlobalInput.ManageCom = "86";
            tGlobalInput.Operator = "hahahaha";
            VData tInputData = new VData();
            tInputData.add(tGlobalInput);
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("ManageCom", "8611");
            tTransferData.setNameAndValue("BranchType", "1");
            tTransferData.setNameAndValue("IndexCalNo", "200403");
            tInputData.add(tTransferData);
            if (!agentWageLoopCalFYC1.submitData(tInputData))
            {
                System.out.println(agentWageLoopCalFYC1.mErrors.getFirstError());
            }
            out.close();
        }
        catch (Exception e)
        {
        }

    }
}
