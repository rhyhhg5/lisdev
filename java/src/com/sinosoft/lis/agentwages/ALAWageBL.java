/*
 * <p>ClassName: LAWageBL </p>
 * <p>Description: LAWageBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.agentwages;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAWageDB;
import com.sinosoft.lis.db.LAWageHistoryDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAWageSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LAWageHistorySet;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class ALAWageBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /*时间变量*/
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAWageSchema mLAWageSchema = new LAWageSchema();
    private LAWageSet mLAWageSet = new LAWageSet();
    private LAAgentSchema mLAAgentSchema=new LAAgentSchema();
    private String mAgentCode = "";
    private String mBranchCode = "";
    private String mflag = "";
    private String[] mSpecialGroups = null;
    private String mBranchType ="";
    private String mBranchType2 ="";
    private String mManageCom ="";
    private String mIndexCalNo ="";
    private MMap mmMap = new MMap();
    
    public ALAWageBL()
    {
    }

    public static void main(String[] args)
    {
        LAWageSchema tLAWageSchema = new LAWageSchema();
        LAAgentSchema tLAAgentSchema=new LAAgentSchema();
        tLAWageSchema.setAgentCode("1101000002");
        tLAWageSchema.setManageCom("86110000");
        tLAWageSchema.setIndexCalNo("200505");
        tLAWageSchema.setBranchType("1");
        tLAWageSchema.setBranchType2("01");
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "8611";
        LAWageSet tLAWageSet = new LAWageSet();
        tLAWageSet.add(tLAWageSchema);
        VData tVData = new VData();
        tVData.addElement(tG);
        tVData.addElement(tLAWageSet);
        tVData.addElement(tLAWageSchema);
        tVData.addElement(tLAAgentSchema);
        tVData.addElement("1");


        tVData.addElement(tLAWageSet);
        ALAWageBL tALAWageBL=new ALAWageBL();
        try
                {
                    tALAWageBL.submitData(tVData, "UPDATE||AGENTWAGE");
                }catch(Exception ex)
                {
                    ex.printStackTrace();
        }
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //检查数据是否有业务员工资为负的
        if(!checkDate()){
        	
        	return false;
        }
        
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAWageBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALAWageBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
//        if (!prepareOutputData())
//        {
//            return false;
//        }
//        if (this.mOperate.equals("QUERY||MAIN"))
//        {
//            this.submitquery();
//        }
//        else
//        {
//            System.out.println("Start ALAWageBL Submit...");
//            LAWageBLS tLAWageBLS = new LAWageBLS();
//            tLAWageBLS.submitData(mInputData, cOperate);
//            System.out.println("End ALAWageBL Submit...");
//            //如果有需要处理的错误，则返回
//            if (tLAWageBLS.mErrors.needDealError())
//            {
//                // @@错误处理
//                this.mErrors.copyAllErrors(tLAWageBLS.mErrors);
//                CError tError = new CError();
//                tError.moduleName = "ALAWageBL";
//                tError.functionName = "submitDat";
//                tError.errorMessage = "数据提交失败!";
//                this.mErrors.addOneError(tError);
//                return false;
//            }
//        }
        mInputData = null;
        return true;
    }
    /**
     * 根据前面的输入数据，进行校验处理
     * 如果出现某个业务员工资为负数，则返回false,否则返回true
     */
    private boolean checkDate(){
    	System.out.println("***詹士德007***");
    	String name = "";
    	String groupagentcode="";
   	 	String errorMessage="销售员:：";
    	SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
         String sql="select getunitecode(agentcode),(select a.name from laagent a where a.agentcode=lawage.agentcode) from lawage where (lastmoney+CurrMoney)<0 and indexcalno='" +this.mIndexCalNo + "' " +
    				" and managecom like '" +this.mManageCom+ "%' " +
					" and BranchType='" + this.mBranchType+ "' " +
					" and BranchType2='" + this.mBranchType2 + "'";
         tSSRS=tExeSQL.execSQL(sql);
         
         if(tSSRS!=null && !tSSRS.equals("")&& tSSRS.getMaxRow()>0)
         {   System.out.println("蜘蛛侠：");  
	         for(int i=1;i<=tSSRS.getMaxRow();i++)
	         {
	             System.out.println("哆啦A梦");
	             groupagentcode=tSSRS.GetText(i,1);
	             System.out.println(groupagentcode);
	             name=tSSRS.GetText(i,2);
	             System.out.println(name);
	             System.out.println("蜘蛛侠：");
	             System.out.println( "海绵宝宝");
	             errorMessage+=groupagentcode+","+name+";";
	         }
	        
	        	  System.out.println("胡巴");
	              CError tError = new CError();
	              tError.moduleName = "ALAWageBL";
	              tError.functionName = "dealdata";
	              tError.errorMessage = errorMessage.substring(0,errorMessage.length()-1)+",月份为："+this.mIndexCalNo+"的薪资为负，不能进行薪资确认！";
	              this.mErrors.addOneError(tError);
	              return false;
         }
         
         String sql1="select distinct getunitecode(agentcode),(select a.name from laagent a where a.agentcode=larewardpunish.agentcode)" 
         		+" from larewardpunish where sendgrp<>'02' and wageno='" +this.mIndexCalNo 
         		+ "'and managecom like '" +this.mManageCom+ "%' " 
				+" and BranchType='" + this.mBranchType+ "' " 
				+" and BranchType2='" + this.mBranchType2 + "'";
         tSSRS=tExeSQL.execSQL(sql1);
         if(tSSRS!=null && !tSSRS.equals("")&& tSSRS.getMaxRow()>0){
	         for(int i=1;i<=tSSRS.getMaxRow();i++)
	         {
	             groupagentcode=tSSRS.GetText(i,1);
	             name=tSSRS.GetText(i,2);       
	             errorMessage+=groupagentcode+","+name+";";
	         }
             CError tError = new CError();
             tError.moduleName = "ALAWageBL";
             tError.functionName = "checkDate";
             tError.errorMessage = errorMessage.substring(0,errorMessage.length()-1)+",月份为："+this.mIndexCalNo+"的加扣款未审核通过，不能进行薪资确认！";
             this.mErrors.addOneError(tError);
             return false;
         }
	return true;
}
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        double lastmoney = 0;
        double currmoney = 0;
        String agentCode = "";
//        if(mflag=="0")
//        {//根据前台数据全部处理，但是SumMoney不小于0
            LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();

            System.out.println("----:" + mLAWageSchema.getAgentGroup());

            String sql = "update lawage set state='1',modifydate = current date,modifytime = current time where state='0' and (lastmoney+CurrMoney)>=0   " +
            		" and indexcalno='" +this.mIndexCalNo + "' " +
    				" and managecom like '" +this.mManageCom+ "%' " +
					" and BranchType='" + this.mBranchType+ "' " +
					" and BranchType2='" + this.mBranchType2 + "'";
            
            System.out.println(
                    "update LAWage1111111111111111111111111111111111:---" +
                    mLAAgentSchema.getName() + mLAWageSchema.getAgentGroup());
            ExeSQL tExeSQL = new ExeSQL();
            if (!tExeSQL.execUpdateSQL(sql)) {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentWageCalDoBL";
                tError.functionName = "dealData";
                tError.errorMessage = "薪筹确认失败!";
                this.mErrors.addOneError(tError);
                return false;

            }
            String tSql=" update LAWageHistory set state='14' ,modifydate = current date,modifytime = current time where managecom like '"+mManageCom+"%' and wageno='"
            +mIndexCalNo+"' and branchtype='"+mBranchType+"' and branchtype2='"+mBranchType2
            +"' and aclass='03' "
            +" and state = '11' "
            +" and managecom in (select managecom from lawage where state = '1' " 
            +" and indexcalno = '"+this.mIndexCalNo+"'" 
			+" and managecom like '" +this.mManageCom+ "%' " 
			+" and BranchType='" + this.mBranchType+ "' " 
			+" and BranchType2='" + this.mBranchType2 + "'"	
            +")"
            ;
            if (!tExeSQL.execUpdateSQL(tSql)) {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentWageCalDoBL";
                tError.functionName = "dealData";
                tError.errorMessage = "薪筹确认失败!";
                this.mErrors.addOneError(tError);
                return false;

            }
//        }
//        else
//        {//做个案处理SumMoney可以小于0 2005-1107 modify:xiangchun
//            System.out.println(
//                    "update LAWage11111111111111111112222222222w"+mLAWageSet.size());
//            Reflections tReflections = new Reflections();
//            LAWageSet tLAWageSet=new LAWageSet();
//            tLAWageSet=mLAWageSet;
//            System.out.println(
//                    "update LAWage111111111111111111122222222222"+tLAWageSet.size());
//            mLAWageSet=new LAWageSet();
//            for(int i=1;i<=tLAWageSet.size();i++)
//            {
//                System.out.println(
//                    "update LAWage1111111111111111111223555");
//                mLAWageSchema=new LAWageSchema();
//                LAWageDB tLAWageDB=new LAWageDB();
//                LAWageSchema tLAWageSchema=new LAWageSchema();
//                mLAWageSchema=tLAWageSet.get(i);
//                tLAWageDB.setManageCom(mLAWageSchema.getManageCom());
//                tLAWageDB.setAgentCode(mLAWageSchema.getAgentCode());
//                tLAWageDB.setIndexCalNo(mLAWageSchema.getIndexCalNo());
//                tLAWageDB.setBranchType(mLAWageSchema.getBranchType());
//                tLAWageDB.setBranchType2(mLAWageSchema.getBranchType2());
//                System.out.println(
//                    "update LAWage1111111111111111111244");
//                tLAWageDB.getInfo();
//                System.out.println(
//                    "update LAWage1111111111111111111223333");
//                tLAWageSchema=tLAWageDB.getSchema();
//                if(tLAWageSchema.getState().trim().equals("1"))
//                {
//                    CError tError = new CError();
//                     tError.moduleName = "ALAWageBL";
//                     tError.functionName = "dealdata";
//                     tError.errorMessage = "销售员代码为："+tLAWageSchema.getAgentCode()+",月份为："+tLAWageSchema.getIndexCalNo()+"的薪资已发放,不需要再审核";
//                     this.mErrors.addOneError(tError);
//                     return false;
//                }
//                System.out.println(
//                    "update LAWage111111111111111111122222222222"+tLAWageSchema);
//                tLAWageSchema.setState("1");
//                mLAWageSet.add(tLAWageSchema);
//            }
//
//        }
        tReturn = true;
        return tReturn;
    }

    /**
     * 用一个数组来存储所有特殊组的编码  //cg added in 1月13日
     * @return
     */
    private String[] getSpecialAgentGroup()
    {
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String tSql =
                "Select trim(AgentGroup) from labranchgroup where state = '1' "
                + "and BranchLevel = '01'  And BranchType = '1'";
        tSSRS = tExeSQL.execSQL(tSql);
        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoBL";
            tError.functionName = "checkData";
            tError.errorMessage = "查询客服组出错!";
            this.mErrors.addOneError(tError);
            return null;
        }
        String[] tSpecialAgentGroups = new String[tSSRS.getMaxRow()];
        for (int i = 0; i < tSpecialAgentGroups.length; i++)
        {
            tSpecialAgentGroups[i] = tSSRS.GetText(i + 1, 1);
        }
        return tSpecialAgentGroups;
    }

    /**
     * 判断该人是否是特殊组成员  //cg added in 1月13日
     * @param tAgentGroup
     * @return
     */
    private boolean isInSpecialGroup(String tAgentCode)
    {
        String tBranchCode = "";
        if (this.mSpecialGroups == null)
        {
            mSpecialGroups = getSpecialAgentGroup();
        }
        if (mSpecialGroups == null)
        {
            return false;
        }
        if (!this.mAgentCode.equals(tAgentCode))
        {
            mAgentCode = tAgentCode;
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tAgentCode);
            if (!tLAAgentDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAWageBL";
                tError.functionName = "submitData";
                tError.errorMessage = "查询代理人信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tBranchCode = tLAAgentDB.getBranchCode();
            mBranchCode = tBranchCode;
        }
        else
        {
            tBranchCode = mBranchCode;
        }
        for (int i = 0; i < mSpecialGroups.length; i++)
        {
            if (tBranchCode.equals(mSpecialGroups[i]))
            {
                System.out.println("特殊业务");
                return true;
            }
        }
        return false;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
  private boolean getInputData(VData cInputData)
  {
      mManageCom = (String) cInputData.get(0);
      mBranchType = (String) cInputData.get(1);
      mBranchType2 =(String)cInputData.get(2) ;
      mIndexCalNo = (String) cInputData.get(3);
      mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
              "GlobalInput", 4));
      
      System.out.println(mBranchType);
      System.out.println(mBranchType2);
      System.out.println(mManageCom);
      System.out.println(mIndexCalNo);
      return true;
  }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start ALAWageBLQuery Submit...");
        LAWageDB tLAWageDB = new LAWageDB();
        tLAWageDB.setSchema(this.mLAWageSchema);
        this.mLAWageSet = tLAWageDB.query();
        this.mResult.add(this.mLAWageSet);
        System.out.println("End ALAWageBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLAWageDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAWageDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAWageBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAWageSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAWageBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    private boolean checkname()
    {
//        String sql="select 'X' from laagent  where name='"+mLAAgentSchema.getName()+"' and branchtype='"+mLAWageSchema.getBranchType()+"' and  branchtype2='"+mLAWageSchema.getBranchType2()+"' ";
//        ExeSQL tExeSQL=new ExeSQL();
//        SSRS tSSRS = new SSRS();
//        tSSRS = tExeSQL.execSQL(sql);
//        if (tExeSQL.mErrors.needDealError() || tSSRS.getMaxRow()<=0)
//        {
//            this.mErrors.copyAllErrors(tExeSQL.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "AgentWageCalDoBL";
//            tError.functionName = "checkname";
//            tError.errorMessage = "没有查到业务员为姓名为"+mLAAgentSchema.getName()+"的信息！";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
        if (mLAWageSchema.getAgentCode() != null &&
            !mLAWageSchema.getAgentCode().trim().equals(""))
        {
            LAAgentDB tLAAgentDB=new LAAgentDB();
            tLAAgentDB.setAgentCode(mLAWageSchema.getAgentCode());
            tLAAgentDB.setName(mLAAgentSchema.getName());
            tLAAgentDB.query();
            LAAgentSet tLAAgentSet=new LAAgentSet();
            tLAAgentSet=tLAAgentDB.query();
            if (tLAAgentDB.mErrors.needDealError())
             {
                 // @@错误处理
                 this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                 CError tError = new CError();
                 tError.moduleName = "ALAWageBL";
                 tError.functionName = "checkname";
                 tError.errorMessage = "没有查到业务员代码为"+mLAWageSchema.getAgentCode()+"且业务员为姓名为"+mLAAgentSchema.getName()+"的信息！！！";
                 this.mErrors.addOneError(tError);
                 return false;
          }
            System.out.println("123223"+tLAAgentSet);
            if(tLAAgentSet.size()<=0)

            {
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAWageBL";
                tError.functionName = "checkname";
                tError.errorMessage = "没有查到业务员代码为"+mLAWageSchema.getAgentCode()+"且业务员为姓名为"+mLAAgentSchema.getName()+"的信息！！！";
                this.mErrors.addOneError(tError);
                return false;
            }
//            System.out.println(mLAAgentSchema.getName()+tLAAgentDB.getName());
//            LAAgentSchema tLAAgentSchema=new LAAgentSchema();
//            tLAAgentSchema=tLAAgentDB.getSchema();
//            if(!(tLAAgentSchema.getName().trim()).equals(mLAAgentSchema.getName().trim()))
//            {
//                CError tError = new CError();
//                tError.moduleName = "ALAWageBL";
//                tError.functionName = "prepareData";
//                tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
//                this.mErrors.addOneError(tError);
//                return false;
//            }

        }
        return true ;
    }
    private boolean checkagentgroup()
    {
        if (mLAAgentSchema.getName() != null &&
           !mLAAgentSchema.getName().trim().equals(""))
       {
           String sql = "select 'X' from laagent  where name='" +
                        mLAAgentSchema.getName() + "' and branchtype='" +
                        mLAWageSchema.getBranchType() + "' and  branchtype2='" +
                        mLAWageSchema.getBranchType2() + "' and exists(select 'X' from labranchgroup a  where laagent.agentgroup=a.agentgroup and a.branchattr='"+mLAWageSchema.getAgentGroup()+"')";
           ExeSQL tExeSQL = new ExeSQL();
           SSRS tSSRS = new SSRS();
           tSSRS = tExeSQL.execSQL(sql);
           if (tExeSQL.mErrors.needDealError() || tSSRS.getMaxRow()<=0)
           {
               this.mErrors.copyAllErrors(tExeSQL.mErrors);
               CError tError = new CError();
               tError.moduleName = "AgentWageCalDoBL";
               tError.functionName = "checkname";
               tError.errorMessage = "没有查到业务员为姓名为" + mLAAgentSchema.getName() +
                                     "并且销售单位为"+mLAWageSchema.getAgentGroup()+"的信息！";
               this.mErrors.addOneError(tError);
               return false;
           }
       }
        if (mLAWageSchema.getAgentCode() != null &&
            !mLAWageSchema.getAgentCode().trim().equals(""))
        {
            String sql = "select 'X' from laagent  where agentcode='" +
                         mLAWageSchema.getAgentCode() + "' and branchtype='" +
                         mLAWageSchema.getBranchType() + "' and  branchtype2='" +
                         mLAWageSchema.getBranchType2() + "' and exists(select 'X' from labranchgroup a  where laagent.agentgroup=a.agentgroup and a.branchattr='"+mLAWageSchema.getAgentGroup()+"')";

           ExeSQL tExeSQL = new ExeSQL();
           SSRS tSSRS = new SSRS();
           tSSRS = tExeSQL.execSQL(sql);
           if (tExeSQL.mErrors.needDealError() || tSSRS.getMaxRow()<=0) {
               this.mErrors.copyAllErrors(tExeSQL.mErrors);
               CError tError = new CError();
               tError.moduleName = "AgentWageCalDoBL";
               tError.functionName = "checkname";
               tError.errorMessage = "没有查到业务员为代码为" + mLAWageSchema.getAgentCode() +
                                     "并且销售单位为"+mLAWageSchema.getAgentGroup()+"的信息！";
               this.mErrors.addOneError(tError);
               return false;
           }
        }
        return true;
    }
    public VData getResult()
    {
        return this.mResult;
    }
   }
