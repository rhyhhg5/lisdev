package com.sinosoft.lis.agentwages;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author xin
 * @version 1.0
 */
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class BankAgentWageCalUI
{

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();


    public BankAgentWageCalUI()
    {
    }

    public boolean submitData(VData cInputData)
    {
        mInputData = (VData) cInputData.clone();
        BankAgentWageCalBL tBankAgentWageCalBL = new BankAgentWageCalBL();
        System.out.println("开始计算银代薪资：");
        if (!tBankAgentWageCalBL.submitData(mInputData))
        {
            this.mErrors.copyAllErrors(tBankAgentWageCalBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "BankAgentWageCalUI";
            tError.functionName = "submitData";
            tError.errorMessage = tBankAgentWageCalBL.mErrors.getFirstError();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
