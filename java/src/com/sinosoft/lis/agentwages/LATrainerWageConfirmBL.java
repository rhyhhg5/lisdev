/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agentwages;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author wangQingMin
 * @version 1.0
 */
public class LATrainerWageConfirmBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	// 业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	private String mManageCom = "";
	private String mBranchType = "";
	private String mBranchType2 = "";
	private String mWageNo = "";

	public LATrainerWageConfirmBL() {
	}

	public static void main(String[] args) {}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData) {

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		if (!check()) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		return true;
	}

	private boolean check() {
		return true;
	}

	private boolean dealData() {
		String tSQL1 = "update LATrainerWageHistory set state = '13',modifydate=current date ,modifytime=current time where "
				+ " wageno = '"
				+ mWageNo
				+ "' and ManageCom = '"
				+ mManageCom
				+ "' and BranchType = '"
				+ mBranchType
				+ "' and BranchType2 ='" + mBranchType2 + "' and AClass = '03'";
		System.out.println("更新LATrainerWageHistory表中state记录的SQL: " + tSQL1);
		ExeSQL tExeSQL = new ExeSQL();
		if (!tExeSQL.execUpdateSQL(tSQL1)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tExeSQL.mErrors);
			CError tError = new CError();
			tError.moduleName = "LATrainerWageConfirmBL";
			tError.functionName = "dealData";
			tError.errorMessage = "执行SQL语句,更新LATrainerWageHistory表记录失败!";
			this.mErrors.addOneError(tError);
			System.out.println("执行SQL语句,更新LATrainerWageHistory表表中state记录失败!");
			return false;
		}
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		// 全局变量
		mManageCom = (String) cInputData.get(0);
		mBranchType = (String) cInputData.get(1);
		mBranchType2 = (String) cInputData.get(2);
		mWageNo = (String) cInputData.get(3);
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 4));
		return true;

	}
}
