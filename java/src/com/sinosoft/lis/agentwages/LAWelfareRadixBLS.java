package com.sinosoft.lis.agentwages;

import java.sql.Connection;

import com.sinosoft.lis.db.LAWelfareRadixBDB;
import com.sinosoft.lis.db.LAWelfareRadixDB;
import com.sinosoft.lis.schema.LAWelfareRadixBSchema;
import com.sinosoft.lis.schema.LAWelfareRadixSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */

public class LAWelfareRadixBLS
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
//传输数据类
    private VData mInputData;
    /** 数据操作字符串 */
    private String mOperate;
    public LAWelfareRadixBLS()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        mInputData = (VData) cInputData.clone();
        System.out.println("Start LAWelfareRadixBLS Submit...");
        if (this.mOperate.equals("INSERT"))
        {
            if (!saveLAWelfareRadix())
            {
                System.out.println("Insert failed");
            }
            System.out.println("End LAWelfareRadixBLS Submit...");
            return false;
        }
        if (this.mOperate.equals("DELETE"))
        {
            if (!deleteLAWelfareRadix())
            {
                System.out.println("delete failed");
            }
            System.out.println("End LAWelfareRadixBLS Submit...");
            return false;
        }
        if (this.mOperate.equals("UPDATE"))
        {
            if (!updateLAWelfareRadix())
            {
                System.out.println("update failed");
            }
            System.out.println("End LAWelfareRadixBLS Submit...");
            return false;
        }
        System.out.println(" sucessful");

        return true;
    }

    /**
     * 保存函数
     */
    private boolean saveLAWelfareRadix()
    {
        LAWelfareRadixSchema tLAWelfareRadixSchema = new LAWelfareRadixSchema();
        tLAWelfareRadixSchema = (LAWelfareRadixSchema) mInputData.
                                getObjectByObjectName("LAWelfareRadixSchema", 0);
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWelfareRadixBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAWelfareRadixDB tLAWelfareRadixDB = new LAWelfareRadixDB(conn);
            tLAWelfareRadixDB.setSchema(tLAWelfareRadixSchema);
            if (!tLAWelfareRadixDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAWelfareRadixDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAWelfareRadixBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWelfareRadixBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean deleteLAWelfareRadix()
    {
        LAWelfareRadixSchema tLAWelfareRadixSchema = new LAWelfareRadixSchema();
        tLAWelfareRadixSchema = (LAWelfareRadixSchema) mInputData.
                                getObjectByObjectName("LAWelfareRadixSchema", 0);
        LAWelfareRadixBSchema tLAWelfareRadixBSchema = new
                LAWelfareRadixBSchema();
        tLAWelfareRadixBSchema = (LAWelfareRadixBSchema) mInputData.
                                 getObjectByObjectName("LAWelfareRadixBSchema",
                0);
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWelfareRadixBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAWelfareRadixDB tLAWelfareRadixDB = new LAWelfareRadixDB(conn);
            tLAWelfareRadixDB.setSchema(tLAWelfareRadixSchema);
            if (!tLAWelfareRadixDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAWelfareRadixDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAWelfareRadixBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LAWelfareRadixBDB tLAWelfareRadixBDB = new LAWelfareRadixBDB(conn);
            tLAWelfareRadixBDB.setSchema(tLAWelfareRadixBSchema);
            System.out.println("-----------------------");
            if (!tLAWelfareRadixBDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAWelfareRadixBDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAWelfareRadixBLS";
                tError.functionName = "updateData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWelfareRadixBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean updateLAWelfareRadix()
    {
        LAWelfareRadixSchema tLAWelfareRadixSchema = new LAWelfareRadixSchema();
        tLAWelfareRadixSchema = (LAWelfareRadixSchema) mInputData.
                                getObjectByObjectName("LAWelfareRadixSchema", 0);
        LAWelfareRadixBSchema tLAWelfareRadixBSchema = new
                LAWelfareRadixBSchema();
        tLAWelfareRadixBSchema = (LAWelfareRadixBSchema) mInputData.
                                 getObjectByObjectName("LAWelfareRadixBSchema",
                0);
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAWelfareRadixBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAWelfareRadixDB tLAWelfareRadixDB = new LAWelfareRadixDB(conn);
            tLAWelfareRadixDB.setSchema(tLAWelfareRadixSchema);
            if (!tLAWelfareRadixDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAWelfareRadixDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAWelfareRadixBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LAWelfareRadixBDB tLAWelfareRadixBDB = new LAWelfareRadixBDB(conn);
            tLAWelfareRadixBDB.setSchema(tLAWelfareRadixBSchema);
            System.out.println("-----------------------");
            if (!tLAWelfareRadixBDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAWelfareRadixBDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAWelfareRadixBLS";
                tError.functionName = "updateData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWelfareRadixBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {
        LAWelfareRadixBLS LAWelfareRadixBLS1 = new LAWelfareRadixBLS();
    }
}
