package com.sinosoft.lis.agentwages;


import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.agentdaily.AgentInfoDiskImporter;
import java.io.File;

/**
 * <p>Title: </p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author luomin
 * @version 1.1
 */
public class DiskImportAgencyCommBL {
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private MMap mmap = new MMap();

    private int importPersons = 0; //记录导入成功的记录数


    private String diskimporttype = "";
    private String path = "";
    private String fileName = "";
    private String configName = "LACrossSelWage.xml";
    private int merrorNum =0;
    private StringBuffer merrorInfo = new StringBuffer();
    private int unImportRecords = 0;
    
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LACrossWageSet mFinalLACrossWageSet = new LACrossWageSet();
    private LAAccountsSet mLAAccountsSet = null;
    private LACrossWageSet mLACrossWageSet = null;
    private LCGrpImportLogSet mLCGrpImportLogSet = new LCGrpImportLogSet();


    public DiskImportAgencyCommBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        if (!getInputData(cInputData)) {
            return false;
        }
        //文件批次校验
        if(!checkBatchno()){
        	return false;
        }
        mOperate = cOperate;
        String path = (String) mTransferData.getValueByName("path");
        String fileName = path + File.separator +
                          (String) mTransferData.getValueByName("fileName");

        String configFileName = path + File.separator + configName;

        //从磁盘导入数据
        AgentInfoDiskImporter importer = new AgentInfoDiskImporter(fileName,configFileName,diskimporttype);
        importer.setTableName(diskimporttype);
        if (!importer.doImport()) {
            mErrors.copyAllErrors(importer.mErrrors);
            return false;
        }
        if (diskimporttype.equals("LACrossWage")) {
        	mLACrossWageSet = (LACrossWageSet) importer
                                  .getSchemaSet();

        }
        
        //若被保人在保单中没有记录，则剔出
        if (!checkData()) {
            return false;
        }
        //数据准备操作
        if (mOperate.equals("INSERT")) {
            if (!prepareData()) {
                return false;
            }
            System.out.println("---End prepareData---");
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            System.out.println("DiskImportAgencyCommBL数据提交失败");
            this.mErrors.addOneError("数据提交失败!");
            return false;
        }
        System.out.println("--- ->submitData--- 数据提交成功。");

        return true;
    }

    private boolean getInputData(VData cInputData) {
        mTransferData = (TransferData) cInputData.
                        getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.
                       getObjectByObjectName("GlobalInput", 0);
        this.fileName=(String)mTransferData.getValueByIndex(4);
        if (mTransferData == null || mGlobalInput == null) {
            mErrors.addOneError("传入的的数据不完整: mTransferData == null "
                                + "|| mGlobalInput == null");
            return false;
        }

        diskimporttype = (String) mTransferData.getValueByName("diskimporttype");
        return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkBatchno(){
    	String tSQL="select 1 from lcgrpimportlog where batchno='"+this.fileName+"' with ur";
    	ExeSQL tExeSQL=new ExeSQL();
    	if(tExeSQL.getOneValue(tSQL).equals("1")){
        	mErrors.addOneError("该文件批次已经被使用过，请使用新的文件批次！");
    		return false;
    	}
    	return true;
    }
    
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkData() {

        //交叉销售佣金校验
        if(diskimporttype.equals("LACrossWage")){
        	 if (mLACrossWageSet.size() == 0) {
                 mErrors.addOneError("导入交叉销售佣金信息数据不存在失败或业务员代码不存在！");
                 return false;
             }else{
            	 LCGrpImportLogSchema tLCGrpImportLogSchema;
            	 LDComDB tLDComDB = new LDComDB();
            	 LDComSet tLDComSet = new LDComSet();
            	 LAAgentDB tLAAgentDB = new LAAgentDB();
            	 LAAgentSet tLAAgentSet = new LAAgentSet();
            	 ExeSQL tExeSQL = new ExeSQL();
            	 LACrossWageSchema mTempLACrossWageSchema = new LACrossWageSchema();
            	 LACrossWageDB mTempLACrossWageDB = new LACrossWageDB();
            	 LACrossWageSchema mTemp2LACrossWageSchema;
            	 String tLDComSql =" select * from ldcom where length(trim(comcode))=8 and sign ='1' and comcode <>'86000000' with ur";
            	 tLDComSet = tLDComDB.executeQuery(tLDComSql);
            	 System.out.println("tLDComSet.size:"+tLDComSet.size()+"");
            	 for (int j = 1; j <= mLACrossWageSet.size(); j++) {
            		 mTempLACrossWageSchema = mLACrossWageSet.get(j);
            		 this.merrorNum=0;
                 	this.merrorInfo.delete(0,merrorInfo.length());
                 	System.out.println("..............checkdata here merrorInfo"
                 			+this.merrorInfo);
                 	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
            		 if(mTempLACrossWageSchema.getAgentCode()==null || 
            				 "".equals(mTempLACrossWageSchema.getAgentCode())){
            			   this.merrorNum++;
                           this.merrorInfo.append("业务员不能为空/");
            		 }
            		 if(mTempLACrossWageSchema.getManageCom()==null ||
            				  "".equals(mTempLACrossWageSchema.getManageCom())){
            			 this.merrorNum++;
                         this.merrorInfo.append("管理机构不能为空/");
            		 }
            		 if(mTempLACrossWageSchema.getWageNo()==null || 
            				 "".equals(mTempLACrossWageSchema.getWageNo())){
            			 this.merrorNum++;
            			 this.merrorInfo.append("所属年月不能为空/");
            		 }
            		 if(mTempLACrossWageSchema.getPropertyFYC()<0){
                         this.merrorNum++;
                         this.merrorInfo.append("健代产佣金金额不能小于0/");
            		 }
            		 if(mTempLACrossWageSchema.getLifeFyc()<0){
                         this.merrorNum++;
                         this.merrorInfo.append("健代寿佣金金额不能小于0/");
            		 }
            		 if(mTempLACrossWageSchema.getWageNo()!=null && 
            				 !"".equals(mTempLACrossWageSchema.getWageNo())){
            			 String tWageNo = mTempLACrossWageSchema.getWageNo();
            			 if(tWageNo.replaceAll(" ", "").length()!=6){
            				 this.merrorNum++;
                             this.merrorInfo.append("所属年月的格式不符合/");
            			 };
            		 }
            		  if(mTempLACrossWageSchema.getManageCom()!=null && 
            				!"".equals( mTempLACrossWageSchema.getManageCom())){
            		     int tempFlag =0;
            		     for(int i=1;i<=tLDComSet.size();i++){
            		    	 if(mTempLACrossWageSchema.getManageCom()
            		    			 .equals(tLDComSet.get(i).getComCode())){
            		    		 tempFlag =1;
            		    		 break;
            		    	 }
            		     }
            		     if(tempFlag==0){
            		    	 this.merrorNum++;
            		    	 this.merrorInfo.append("录入的管理机构代码不存在/");
            		     }
            		 }
            		  if(mTempLACrossWageSchema.getAgentCode()!= null && 
            				  !"".equals(mTempLACrossWageSchema.getAgentCode())){
            			  String tAgentSql =" select managecom from laagent where groupagentcode ='"+mTempLACrossWageSchema.getAgentCode()+"' " +
            			  		" and branchtype='2' and branchtype2='02' ";
            			 String tResult=tExeSQL.getOneValue(tAgentSql);
        				  if(!tResult.equals(mTempLACrossWageSchema.getManageCom())){
        					  this.merrorNum++;
        					  this.merrorInfo.append("录入的管理机构与业务员不匹配/录入业务员的渠道错误！");
        				  }
        				
            		  }
            		  //重复性校验
					if(this.merrorNum==0){
						for(int i =j+1;i<=mLACrossWageSet.size();i++){
							mTemp2LACrossWageSchema = mLACrossWageSet.get(i);
							if(mTempLACrossWageSchema.getManageCom().equals
									(mTemp2LACrossWageSchema.getManageCom())&& mTempLACrossWageSchema.getWageNo().equals(
									mTemp2LACrossWageSchema.getWageNo())&& mTempLACrossWageSchema.getAgentCode().equals(
											mTemp2LACrossWageSchema.getAgentCode())){
								this.merrorNum++;
								this.merrorInfo.append("第"+j+"行与第"+i+"行数据重复/");
							}
						}
					}
					//add 关于业务员校验
  				  if(this.merrorNum==0){
  				  String tSQL1 ="select * from Laagent where groupagentcode ='"+mTempLACrossWageSchema.getAgentCode()+"'";
                    tLAAgentSet = tLAAgentDB.executeQuery(tSQL1);
                    if(tLAAgentSet.size()>0){
                    	LAAgentSchema tLAAgentSchema = new LAAgentSchema();
                    	tLAAgentSchema = tLAAgentSet.get(1);
  				 String tAgentCode = tLAAgentSchema.getAgentCode();
  				//离职时间与所在薪资月的校验
  				  String tAgentState = tLAAgentSchema.getAgentState();
  				  if(tAgentState!=null && tAgentState.compareTo("06")>=0){
  					  String tOutWorkDate = tLAAgentSchema.getOutWorkDate();
  					  String tSectionSql =" select startdate,enddate from LAStatSegment where stattype ='5' and " +
  					  		"yearmonth='"+mTempLACrossWageSchema.getWageNo()+"' ";
  					  SSRS tSSRS = new SSRS();
  					  tSSRS = tExeSQL.execSQL(tSectionSql);
  					  String tStarDate = tSSRS.GetText(1, 1);
  					  String tEndDate = tSSRS.GetText(1, 2);
  					  if(tOutWorkDate.compareTo(tStarDate)<0){
  			                this.merrorNum++;
  			                this.merrorInfo.append( "代理人" + mTempLACrossWageSchema.getAgentCode() + "在" + tOutWorkDate +
	                                      "已经离职，不能录入" + mTempLACrossWageSchema.getWageNo() + "的交叉销售佣金!/");
  					  }
  				  }
  				  //校验薪资是否确认与审核发放
  				  String tWageSql ="select * from lawage where branchtype ='2' and branchtype2='02' and indexcalno ='"+mTempLACrossWageSchema.getWageNo()+"'" +
  				  		" and managecom ='"+mTempLACrossWageSchema.getManageCom()+"' fetch first 5 rows only with ur ";
  				  LAWageDB tLAWageDB = new LAWageDB();
  				  LAWageSet tLAWageSet = new LAWageSet();
  				  tLAWageSet = tLAWageDB.executeQuery(tWageSql);
  				  if(tLAWageSet.size()>0){
  					  this.merrorNum++;
  					  this.merrorInfo.append("机构:"+mTempLACrossWageSchema.getManageCom()+"月份:"+mTempLACrossWageSchema.getWageNo()+"已经计算过佣金,不能进行下一步操作!/");
  				  }
  				}
      		  } 
            		 if(this.merrorNum==0){
            			 checkExists(mTempLACrossWageSchema,mTempLACrossWageDB);
            		 }
            		 if(this.merrorNum>0){
            			 tLCGrpImportLogSchema = new LCGrpImportLogSchema();
            			 tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                     	tLCGrpImportLogSchema.setErrorInfo("导入错误！具体错误有："
                     			+this.merrorInfo.toString());
                     	tLCGrpImportLogSchema.setErrorState("1");
                     	tLCGrpImportLogSchema.setErrorType("1");
                     	tLCGrpImportLogSchema.setContID(j+"");
                     	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                     	this.unImportRecords++;
            		 }else{
            			 tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                     	tLCGrpImportLogSchema.setErrorInfo("导入成功！");
                     	tLCGrpImportLogSchema.setErrorState("0");
                     	tLCGrpImportLogSchema.setErrorType("0");
                     	tLCGrpImportLogSchema.setContID(j+"");
                     	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
            		 }
				}
             }
        	
        }

        return true;
    }
    //判断数据库中是否存在
    private boolean checkExists(LACrossWageSchema tLACrossWageSchema,LACrossWageDB tLACrossWageDB){
     	LACrossWageSet mtempLACrossWageSet = new LACrossWageSet();
     	String tCheckSql =" select * from lacrosswage where managecom ='"+tLACrossWageSchema.getManageCom()+"'" +
     			          " and getUniteCode(agentcode) = '"+tLACrossWageSchema.getAgentCode()+"' and wageno ='"+tLACrossWageSchema.
     			          getWageNo()+"' and branchtype ='"+mTransferData.getValueByName("branchtype")+"'" +
     					  " and branchtype2='"+mTransferData.getValueByName("branchtype2")+"' ";
     	mtempLACrossWageSet = tLACrossWageDB.executeQuery(tCheckSql);
     	if(mtempLACrossWageSet.size()>0){
     		System.out.println("checkExists LACrossWage");
     		this.merrorNum++;
     		this.merrorInfo.append("业务员"+tLACrossWageSchema.getAgentCode()+"在"+tLACrossWageSchema.getWageNo()+"在数据库中存在/");
     	}else{
     		LACrossWageSchema mteLACrossWageSchema = new LACrossWageSchema();
     		mteLACrossWageSchema.setManageCom(tLACrossWageSchema.getManageCom());
     		mteLACrossWageSchema.setPropertyFYC(tLACrossWageSchema.getPropertyFYC());
     		mteLACrossWageSchema.setLifeFyc(tLACrossWageSchema.getLifeFyc());
     		mteLACrossWageSchema.setWageNo(tLACrossWageSchema.getWageNo());
     		mteLACrossWageSchema.setAgentCode(tLACrossWageSchema.getAgentCode());
     		mFinalLACrossWageSet.add(mteLACrossWageSchema);
     	}
    	return true;
    }


    private boolean prepareData() {


        if (diskimporttype.equals("LACrossWage")) {
        	System.out.print("prepare DOING");
        	importPersons=mFinalLACrossWageSet.size();
          if(mFinalLACrossWageSet != null && mFinalLACrossWageSet.size()>0){
        	for(int i =1;i<=mFinalLACrossWageSet.size();i++){
        	String branchtype = (String) mTransferData.getValueByName("branchtype");	
        	String branchtype2= (String) mTransferData.getValueByName("branchtype2");
        	System.out.println("mFinalLACrossWageSet.size():"+mFinalLACrossWageSet.size());
        	ExeSQL tEXESQL = new ExeSQL();
        	 String sql ="select agentcode from laagent where  groupagentcode ='"+mFinalLACrossWageSet.get(i).getAgentCode()+"'";
        	mFinalLACrossWageSet.get(i).setAgentCode(tEXESQL.getOneValue(sql));
        	mFinalLACrossWageSet.get(i).setOperator(mGlobalInput.Operator);
        	mFinalLACrossWageSet.get(i).setBranchType(branchtype);
        	mFinalLACrossWageSet.get(i).setBranchType2(branchtype2);
        	mFinalLACrossWageSet.get(i).setMakeDate(PubFun.getCurrentDate());
        	mFinalLACrossWageSet.get(i).setMakeTime(PubFun.getCurrentTime());
        	mFinalLACrossWageSet.get(i).setModifyDate(PubFun.getCurrentDate());
        	mFinalLACrossWageSet.get(i).setModifyTime(PubFun.getCurrentTime());
        	mmap.put(mFinalLACrossWageSet.getObj(i), "INSERT");
        	}
          }
          if(this.mLCGrpImportLogSet != null && this.mLCGrpImportLogSet.size()>0){
        	 for (int i = 1; i <= mLCGrpImportLogSet.size(); i++) {
				mLCGrpImportLogSet.get(i).setBatchNo(this.fileName);
				mLCGrpImportLogSet.get(i).setOperator(this.mGlobalInput.ManageCom);
				mLCGrpImportLogSet.get(i).setMakeDate(PubFun.getCurrentDate());
				mLCGrpImportLogSet.get(i).setMakeTime(PubFun.getCurrentTime());
				mLCGrpImportLogSet.get(i).setGrpContNo("00000000000000000000");
				mmap.put(mLCGrpImportLogSet.get(i), "INSERT");
			} 
          }
          System.out.println("prepareData:adding map");
          this.mResult.add(mmap);
          //this.mResult.add(mmapCompare);
          System.out.println("prepareData:did");
        }
        return true;
    }

    /**
     *成功导入的记录数
     * @return int
     */
    public int getImportPersons() {
        return importPersons;
    }
    public int getUnImportRecords() {
        return unImportRecords;
    }

    public static void main(String[] args) {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        TransferData t = new TransferData();

        t.setNameAndValue("diskimporttype", "LACertification");
        t.setNameAndValue("path", "D:\\picch\\ui\\temp");
        t.setNameAndValue("fileName", "展业证清单.xls");

        VData v = new VData();
        v.add(g);
        v.add(t);

        DiskImportAgencyCommBL d = new DiskImportAgencyCommBL();
        d.submitData(v, "INSERT");

        if (d.mErrors.needDealError()) {
            System.out.println(d.mErrors.getErrContent());
        }
    }
    // 去除批量录入信息中全角空格
   private String getSwitchData(String temp)
   {
	   if(temp == null)
	   {
		   return null;
	   }
	   String Result = "";
	   String regStartSpace = "^[　 ]*";  
       String regEndSpace = "[　 ]*$";  
         
       // 连续两个 replaceAll   
       Result = temp.replaceAll(regStartSpace, "").replaceAll(regEndSpace, "").trim();
	   return Result;
   }
}
