package com.sinosoft.lis.agentwages;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author luomin
 * @version 1.1
 */
public class DiskImportCrossCommUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private DiskImportCrossCommBL mDiskImportCrossCommBL = null;

    public DiskImportCrossCommUI()
    {
    	mDiskImportCrossCommBL = new DiskImportCrossCommBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if(!mDiskImportCrossCommBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mDiskImportCrossCommBL.mErrors);
            return false;
        }
        //
        if(mDiskImportCrossCommBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mDiskImportCrossCommBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mDiskImportCrossCommBL.getImportPersons();
    }

    public int getUnImportRecords()
    {
        return mDiskImportCrossCommBL.getUnImportRecords();
    }

    public static void main(String[] args)
    {
    	DiskImportCrossCommUI zDiskImportCrossCommUI = new DiskImportCrossCommUI();
    }
}
