package com.sinosoft.lis.agentwages;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LACalWageUI
{

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();

    public LACalWageUI()
    {
    }

    public boolean submitData(VData cInputData)
    {
        System.out.println("start ui");
        mInputData = (VData) cInputData.clone();
        LACalWageBL tLACalWageBL = new LACalWageBL();
        if (!tLACalWageBL.submitData(mInputData))
        {
            this.mErrors.copyAllErrors(tLACalWageBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LACalWageUI";
            tError.functionName = "submitData";
            tError.errorMessage = tLACalWageBL.mErrors.getFirstError();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {
        LACalWageUI LACalWageUI1 = new LACalWageUI();
        LACalWageUI1.submitData(new VData());
    }
}
