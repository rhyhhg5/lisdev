/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agentwages;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author zy
 * @version 1.0
 */
public class LAWageCalBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mManageCom = "";
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String YearMonth = "";
    private TransferData tTransferData = new TransferData();

    public LAWageCalBL()
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData)
    {

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!check())
        {
            return false;
        }

        // 进行业务处理
        if (!dealData())
        {
            return false;
        }

        // 准备传往后台的数据
        VData vData = new VData();
        return true;
    }

    private boolean check()
    {
        if(mBranchType.equals("1")||mBranchType.equals("3")||mBranchType.equals("5")||mBranchType.equals("7"))
        {
            if (mManageCom.length() != 8)
            {
                buildError("ManageCom", "管理机构代码应该为八位");
                return false;
            }
        }
        else
        {
            if (mManageCom.length() != 4)
            {
                buildError("ManageCom", "管理机构代码应该为四位");
                return false;
            }
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {

        LATreeSet tLATreeSet = new LATreeSet();

        tTransferData.setNameAndValue("BranchType", mBranchType);
        tTransferData.setNameAndValue("BranchType2",mBranchType2) ;
        tTransferData.setNameAndValue("IndexCalNo", YearMonth);
        tTransferData.setNameAndValue("GlobalInput", mGlobalInput);
        tTransferData.setNameAndValue("LATreeSet", tLATreeSet);
        VData vData = new VData();
        //xjh Modify 2005/02/24
       // String SQL = "select ComCode from ldcom where comcode like '" +mManageCom + "%' and length(trim(ComCode))=8  and sign='1' order by comcode";
        String SQL = "select managecom from lawagehistory where 1=1 and managecom like '"+mManageCom+"%' and wageno ='"+YearMonth+"'  " +
      "and branchtype='" +mBranchType + "' and BranchType2='"+mBranchType2+"' and exists(select '1' from ldcom where comcode = lawagehistory.managecom and sign = '1') " ;
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(SQL);
        for (int a = 1; a <= tSSRS.getMaxRow(); a++)
        {
            String tSQL = "select state From lawagehistory where wageno='" +YearMonth + "' and managecom ='"
                        + tSSRS.GetText(a, 1) + "' and branchtype='" +mBranchType + "' and BranchType2='"+mBranchType2+"' ";
            ExeSQL aExeSQL = new ExeSQL();
            String state = aExeSQL.getOneValue(tSQL);
            if (state != null && state.equals("14"))
            {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LAWageCalBL";
                    tError.functionName = "check()";
                    tError.errorMessage = "管理机构" + tSSRS.GetText(a, 1) +"佣金已经计算过！";
                    this.mErrors.addOneError(tError);
                    return false;
            }
            if((mBranchType.equals("1") && mBranchType2.equals("01"))||(mBranchType.equals("2")))
            {//个险为佣金试算，为 12
                 if (state != null && !state.equals("12"))
                 {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "LAWageCalBL";
                        tError.functionName = "check()";
                        tError.errorMessage = "管理机构" + tSSRS.GetText(a, 1) +
                                              "提数计算未完成！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
            }
          //增加对于互动渠道的判断	
            else if((mBranchType.equals("5") && mBranchType2.equals("01"))||(mBranchType.equals("7") && mBranchType2.equals("01")))
			{
    			 if (state != null && !state.equals("12"))
                 {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "LAWageCalBL";
                        tError.functionName = "check()";
                        tError.errorMessage = "管理机构" + tSSRS.GetText(a, 1) +
                                              "提数计算未完成！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
			}
            else
            {   //新机构开业
            	
            	
        		/**
            	 *  注: 这个地方还需要改动
            	 */
            	System.out.println("state为_________________"+state);
                     if(state==null)
                     {
                          continue;
                     }else if (!state.equals("")&& !state.equals("11"))
                       {
                     // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "LAWageCalBL";
                        tError.functionName = "check()";
                        tError.errorMessage = "管理机构" + tSSRS.GetText(a, 1) +
                                              "提数计算未完成！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
             }

                tTransferData = new TransferData();
                tTransferData.setNameAndValue("ManageCom", tSSRS.GetText(a, 1));
                tTransferData.setNameAndValue("BranchType", mBranchType);
                tTransferData.setNameAndValue("BranchType2",mBranchType2) ;
                tTransferData.setNameAndValue("IndexCalNo", YearMonth);
                tTransferData.setNameAndValue("GlobalInput", mGlobalInput);
                tTransferData.setNameAndValue("LATreeSet", tLATreeSet);
                vData.clear();
                vData.add(tTransferData);
                LACalWageBL tLACalWageBL = new LACalWageBL();
                System.out.println("------------Start LACalWageBL Submit ...");

                if (!tLACalWageBL.submitData(vData))
                {
                    if (tLACalWageBL.mErrors.needDealError())
                    {
                        mErrors.copyAllErrors(tLACalWageBL.mErrors);
                        return false;
                    }
                    else
                    {
                        buildError("submitData",
                                   "LACalWageBL发生错误，但是没有提供详细的出错信息");
                        return false;
                    }
                }
                
                /**
                 *  注：团险新需求 添加团险薪资预算功能
                 *  add fanting  2012-8-6
                 */
                //修改：2004-06-08
                //每个机构佣金计算成功后，把lawagehistory表中state设置为14
                if((mBranchType.equals("1") && mBranchType2.equals("01"))||(mBranchType.equals("7") && mBranchType2.equals("01")))
                {//个险为佣金试算，不需要改为 14
//                    return true;
                }else if(mBranchType.equals("3") && mBranchType2.equals("01"))
                {
                	System.out.println("come in?");
                }
                else
                {
                    String tSQL1 = "update LAWageHistory set state = '14',modifydate = current date,modifytime = current time  where "
                                   + " wageno = '" + YearMonth +
                                   "' and ManageCom = '" + tSSRS.GetText(a, 1)
                                   + "' and BranchType = '" + mBranchType
                                   + "' and BranchType2 ='" + mBranchType2
                                   + "' and AClass = '03'";
                    System.out.println("最后更新LAWageHistory表中state记录的SQL: " + tSQL1);
                    boolean tValue = tExeSQL.execUpdateSQL(tSQL1);
                    if (tExeSQL.mErrors.needDealError()) {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tExeSQL.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "LAWageCalBL";
                        tError.functionName = "dealdate";
                        tError.errorMessage = "执行SQL语句,更新LAWageHistory表记录失败!";
                        this.mErrors.addOneError(tError);
                        System.out.println("执行SQL语句,更新LAWageHistory表表中state记录失败!");
                        return false;
                    }
                }
            }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mManageCom = (String) cInputData.get(0);
        mBranchType = (String) cInputData.get(1);
        mBranchType2 = (String) cInputData.get(2) ;
        YearMonth = (String) cInputData.get(3);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 3));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LACalWageBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    public static void main(String[] args)
    {
        String mManageCom = "8633";
        String mBranchtype = "1";
        String YearMonth = "200402";
        VData tVData = new VData();
        tVData.add(mManageCom);
        tVData.add(mBranchtype);
        tVData.add(YearMonth);
//    GlobalInput mGlobalInput = new GlobalInput() ;
//    GlobalInput mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",3));
        LAWageCalBL tLAWageCalBL = new LAWageCalBL();
        tLAWageCalBL.submitData(tVData);


    }
}
