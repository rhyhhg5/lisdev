package com.sinosoft.lis.agentwages;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class AgentWage1XLSUI
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    public AgentWage1XLSUI()
    {
        System.out.println("AgentWage1XLSUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
       AgentWage1XLSBL bl = new AgentWage1XLSBL();
        if(!bl.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        AgentWage1XLSUI tAgentWage1XLSUI = new   AgentWage1XLSUI();
    }
}
