package com.sinosoft.lis.agentwages;


/**
 * 根据外部传入的四位ManageCom，查出所有与之
 * 对应的八位ManageCom，循环调用AgentWageCalFYCReUI,
 * 对该八位的ManageCom进行处理。
 */
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BankRepeatFYC
{
    public CErrors mErrors = new CErrors();
    private String mManageCom = "";
    private String mIndexCalNo = "";
    private String mBranchType = "";
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    private VData mOutputData = new VData();
    public BankRepeatFYC()
    {
    }

    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            mBranchType = (String) mTransferData.getValueByName("BranchType");
            mManageCom = (String) mTransferData.getValueByName("ManageCom");
            mIndexCalNo = (String) mTransferData.getValueByName("IndexCalNo");
            return true;
        }
        catch (Exception e)
        {
            CError tError = new CError();
            tError.moduleName = "BankRepeatFYC";
            tError.functionName = "getInputData";
            tError.errorMessage = "getInputData" + e.toString() + "!";
            return false;
        }
    }

    private boolean dealData()
    {
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        ExeSQL aExeSQL = new ExeSQL();
        ExeSQL cExeSQL = new ExeSQL();
        String aSQL = "update lacommision a set fyc=directwage/2 where branchtype='3' and managecom like '" +
                      mManageCom + "%' and "
                      + "exists (select 'X' from latree where agentcode=a.agentcode and agentgrade='B01') "
                      + "and wageno='" + mIndexCalNo + "'";
        System.out.println("重算B01" + aSQL);
        tExeSQL.execUpdateSQL(aSQL);
        String bSQL = "update lacommision a set fyc=directwage where branchtype='3' and managecom like '" +
                      mManageCom + "%' and "
                      + "exists (select 'X' from latree where agentcode=a.agentcode and agentgrade<>'B01') "
                      + "and wageno='" + mIndexCalNo + "'";
        System.out.println("重算其他" + bSQL);
        aExeSQL.execUpdateSQL(bSQL);
        String cSQL = "update lawagehistory set state='13' where wageno='" +
                      mIndexCalNo + "' and managecom like '" + mManageCom +
                      "%'"
                      + "' and branchtype='3' ";
        System.out.println("update lawagehistory－－－－－：" + bSQL);
        cExeSQL.execUpdateSQL(cSQL);

        return true;
    }

    public static void main(String[] args)
    {
        AgentWageLoopCalFYC agentWageLoopCalFYC1 = new AgentWageLoopCalFYC();
        try
        {
            PrintStream out =
                    new PrintStream(
                            new BufferedOutputStream(
                                    new FileOutputStream(
                                            "AgentWageLoopCalFYCReBL.out")));
            System.setOut(out);

            GlobalInput tGlobalInput = new GlobalInput();
            tGlobalInput.ManageCom = "86";
            tGlobalInput.Operator = "hahahaha";
            VData tInputData = new VData();
            tInputData.add(tGlobalInput);
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("ManageCom", "8611");
            tTransferData.setNameAndValue("BranchType", "1");
            tTransferData.setNameAndValue("IndexCalNo", "200402");
            tInputData.add(tTransferData);
            if (!agentWageLoopCalFYC1.submitData(tInputData))
            {
                System.out.println(agentWageLoopCalFYC1.mErrors.getFirstError());
            }
            out.close();
        }
        catch (Exception e)
        {
        }
    }
}
