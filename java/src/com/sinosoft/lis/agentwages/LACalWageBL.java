/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agentwages;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Hashtable;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAssessIndexDB;
import com.sinosoft.lis.db.LAIndexVsCommDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAAssessIndexSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LAAssessIndexSet;
import com.sinosoft.lis.vschema.LAIndexVsCommSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vdb.LABranchGroupDBSet;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.schema.LABranchGroupSchema;


/*
 * <p>ClassName: LACalWageBL </p>
 * <p>Description: LACalWageBL类文件,
 * <p>封装CalWageBaseBL类，用于计算N个人的某类指标 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-13
 */
public class LACalWageBL
{

    private LATreeSet mLATreeSet = new LATreeSet();
    private String mIndexCalNo = new String();
    private String mWageCode = new String();
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mManageCom = new String();
    private String mAreaType = new String();
    private String mWageVersion = new String();
    private String mAgentCode = new String();
    private String mBranchType = new String();
    private String mBranchType2 = new String();
    private String mOperator = new String();
    public CErrors mErrors = new CErrors();
    private String mWageType = new String();
    private String mAgentGrade = new String();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

    private LAWageSet mUpLAWageSet = new LAWageSet(); //需要更新的LAWage记录集合
    private LAWageSet mInLAWageSet = new LAWageSet(); //需要插入的LAWage记录集合

    private Hashtable mAgentGradeHash = new Hashtable(); //用于保存某个职级对应的LAIndexVsCommSet的哈希表
    private Hashtable mIndexHash = new Hashtable(); //用于保存某个指标编码对应的LAAssessIndexSchema的哈希表
    private Hashtable mAgentGradeIndexHash = new Hashtable(); //用于保存某个指标编码对应的LAAssessIndexSchema的哈希表

    private VData mResult = new VData();
    private String OutWorkBeginDate = "";

    private boolean isReCalFlag = false; //记录是否是佣金重算得标记

    public LACalWageBL()
    {
    }

    public boolean test()
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "aa";

        VData tVData = new VData();

        LATreeSet tLATreeSet = new LATreeSet();
        LATreeDB tLATreeDB = new LATreeDB();
        String sql = "select * from latree where agentcode in (select agentcode from lawage where f01>0 and managecom like '8611%' and indexcalno='200407' ) order by agentgrade ";
        tLATreeSet = tLATreeDB.executeQuery(sql);
        tVData.add(tGlobalInput);
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("ManageCom", "86110000");
        tTransferData.setNameAndValue("BranchType", "1");
        tTransferData.setNameAndValue("IndexCalNo", "200409");
        tTransferData.setNameAndValue("GlobalInput", tGlobalInput);
        tTransferData.setNameAndValue("LATreeSet", tLATreeSet);

        LACalWageBL LACalWageBL1 = new LACalWageBL();
        tVData.add(tTransferData);
        LACalWageBL1.submitData(tVData);
        if (LACalWageBL1.mErrors.needDealError())
        {
            for (int i = 0; i < LACalWageBL1.mErrors.getErrorCount(); i++)
            {
                System.out.println(LACalWageBL1.mErrors.getError(i).moduleName);
                System.out.println(LACalWageBL1.mErrors.getError(i).
                                   functionName);
                System.out.println(LACalWageBL1.mErrors.getError(i).
                                   errorMessage);
            }
        }
        System.out.println("---------------");
        System.out.println("over");
        return true;

    }

    public static void main(String[] args)
    {
        try
        {
            PrintStream out =
                    new PrintStream(
                            new BufferedOutputStream(
                                    new FileOutputStream("LACalWageBL.out")));
            System.setOut(out);
            System.out.println("---------------");
            GlobalInput tGlobalInput = new GlobalInput();
            tGlobalInput.ManageCom = "86";
            tGlobalInput.Operator = "aa";

            VData tVData = new VData();

            LATreeSet tLATreeSet = new LATreeSet();
            LATreeDB tLATreeDB = new LATreeDB();
            String sql = "select * from latree where agentcode in (select agentcode from laagent where agentstate='03' and managecom like '8695%'  ) order by agentgrade,agentcode ";
            tLATreeSet = tLATreeDB.executeQuery(sql);
            tVData.add(tGlobalInput);
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("ManageCom", "86950000");
            tTransferData.setNameAndValue("BranchType", "2");
            tTransferData.setNameAndValue("BranchType2", "01");
            tTransferData.setNameAndValue("IndexCalNo", "200409");
            tTransferData.setNameAndValue("GlobalInput", tGlobalInput);
            tTransferData.setNameAndValue("LATreeSet", tLATreeSet);

            LACalWageBL LACalWageBL1 = new LACalWageBL();
            tVData.add(tTransferData);
            LACalWageBL1.submitData(tVData);
            if (LACalWageBL1.mErrors.needDealError())
            {
                for (int i = 0; i < LACalWageBL1.mErrors.getErrorCount(); i++)
                {
                    System.out.println(LACalWageBL1.mErrors.getError(i).
                                       moduleName);
                    System.out.println(LACalWageBL1.mErrors.getError(i).
                                       functionName);
                    System.out.println(LACalWageBL1.mErrors.getError(i).
                                       errorMessage);
                }
            }
            System.out.println("---------------");
            System.out.println("over");
            out.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!check())
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }

        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        System.out.println("getInputData");
        try
        {

            TransferData tTransferData = (TransferData) cInputData.
                                         getObjectByObjectName("TransferData",
                    0);
            mBranchType = (String) tTransferData.getValueByName("BranchType");
            mBranchType2 = (String) tTransferData.getValueByName("BranchType2") ;
            mManageCom = (String) tTransferData.getValueByName("ManageCom");
            String cType="";
            if(mBranchType.equals("1") && mBranchType2.equals("01"))
            {
                cType="01";//统计渠道类型
            }
            else  if(mBranchType.equals("2") && mBranchType2.equals("01"))
            {
                cType="02";
            }
            else if(mBranchType.equals("2") && mBranchType2.equals("02"))
             {
                 cType="03";
             }
             //modify by Abigale   207-221  用于查询地区类型
             else if(mBranchType.equals("3") && mBranchType2.equals("01"))
             {
                 cType="03";
             }
             else if(mBranchType.equals("7") && mBranchType2.equals("01"))
             {
                 cType="01";
             }
            if(mBranchType.equals("1") && mBranchType2.equals("01"))
            mAreaType = AgentPubFun.getAreaType(mManageCom.substring(0, 4),cType);
            else
            mAreaType = AgentPubFun.getAreaType(mManageCom.substring(0, 4),cType);
            //WageType用于定义佣金类型
            mWageType = (String) tTransferData.getValueByName("WageType");
            //WageType用于定义佣金类型 ;
            mWageCode = (String) tTransferData.getValueByName("WageCode");
            mIndexCalNo = (String) tTransferData.getValueByName("IndexCalNo");
            System.out.println("IndexCalNo:" + mIndexCalNo);
            mAgentGrade = (String) tTransferData.getValueByName("AgentGrade");
            mGlobalInput = (GlobalInput) tTransferData.getValueByName(
                    "GlobalInput");
            mOperator = mGlobalInput.Operator;
            mLATreeSet = (LATreeSet) tTransferData.getValueByName("LATreeSet");
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    private boolean check()
    {
        return true;
    }

    private boolean dealData()
    {
        String tSql ="";
        //===得到佣金计算时间 起期和止期
        if (mBranchType.equals("1"))
        {
           tSql="select startdate from lastatsegment where yearmonth=" +
                    mIndexCalNo + " and stattype='5' ";
        }
        else if(mBranchType.equals("2"))
        {
            tSql="select startdate from lastatsegment where yearmonth=" +
                    mIndexCalNo + " and stattype='91' ";
        }
        //modify by Abigale  264-269  2008/03/07
        else if(mBranchType.equals("3"))
        {
            tSql="select startdate from lastatsegment where yearmonth=" +
                    mIndexCalNo + " and stattype='91' ";
        }
        //增加对于互动渠道的判断
        //2014-12-8  by yangyang
        else if (mBranchType.equals("5"))
        {
           tSql="select startdate from lastatsegment where yearmonth=" +
                    mIndexCalNo + " and stattype='5' ";
        }
        else if (mBranchType.equals("7"))
        {
           tSql="select startdate from lastatsegment where yearmonth=" +
                    mIndexCalNo + " and stattype='5' ";
        }
        ExeSQL tExeSQL = new ExeSQL();
        OutWorkBeginDate = tExeSQL.getOneValue(tSql);
        OutWorkBeginDate = AgentPubFun.formatDate(OutWorkBeginDate,"yyyy-MM-dd");

        LATreeSchema tLATreeSchema = new LATreeSchema();
        //如果外界传入的LATreeSet中没有Schema，则说明是不是进行个案计算
        //因此需要查找所有符合条件的人进行计算 这次查的是latree表
        if (mLATreeSet == null || mLATreeSet.size() == 0)
        {
            String sql = "";
            LATreeDB tLATreeDB = new LATreeDB();
            StringBuffer sqlBF = new StringBuffer();
            sqlBF.append("select * from LATree where ManageCom " + "= '" + mManageCom.trim() + "'");
            if (mBranchType.equals("1"))//个险不给营业部经理算薪资
            {
                if (mAgentGrade != null && !mAgentGrade.equals(""))
                {
                    sqlBF.append(" and agentgrade='" + mAgentGrade + "'");
                }
                sql =" and (speciFlag is null or speciFlag<>'01' ) "
                    +" and exists ("
                    +" select 'X' from laagent where BranchType = '"+mBranchType+"' and BranchType2='"+mBranchType2+"' "
                    +" and EmployDate <= (select enddate from lastatsegment where stattype='5' and yearmonth= " +this.mIndexCalNo +" ) "
                    +" and (outworkdate is null or outworkdate>='" +OutWorkBeginDate + "') "
                    +" and agentcode=latree.agentcode"
                    +" ) "
                    +" and not exists ("
                    +" select 'X' from lawage where managecom ='" + mManageCom + "' and indexcalno='" + mIndexCalNo
                    +"' and agentcode=latree.agentcode ) "
                    +" order by AgentGrade,AgentCode asc with ur";
                sqlBF.append(sql);
            }
            else if (mBranchType.equals("3"))
            {
                if (mAgentGrade != null && !mAgentGrade.equals(""))
                {
                    sqlBF.append(" and agentgrade='" + mAgentGrade + "' ");
                }
                //xjh Modify 2005/02/25，职级关系确定
                sql =" and BranchType='"+mBranchType+"' and BranchType2='"+mBranchType2+"' and (speciFlag is null or speciFlag<>'01' ) "
                    +" and exists (select 'X' from laagent where BranchType = '"+mBranchType+"' and BranchType2='"+mBranchType2+"' "
                    +" and EmployDate <= (select enddate from lastatsegment where stattype='5' and yearmonth= "
                    +this.mIndexCalNo +" ) and (outworkdate is null or outworkdate>='"
                    +OutWorkBeginDate + "') and agentcode=latree.agentcode) "
                    +" order by AgentGrade,AgentCode asc";
                sqlBF.append(sql);
            }
            else if (mBranchType.equals("2"))
            {
                if (mAgentGrade != null && !mAgentGrade.equals(""))
                {
                    sqlBF.append(" and agentgrade='" + mAgentGrade + "' ");
                }
                sql = " and BranchType='"+mBranchType+"' and BranchType2='"+mBranchType2
                      +"' and (speciFlag is null or speciFlag<>'01' ) and exists (select 'X' from laagent where BranchType = '"+mBranchType
                      +"' and BranchType2='"+mBranchType2+"' "
                      + " and EmployDate <= (select enddate from lastatsegment where stattype='91' and yearmonth= "
                      +this.mIndexCalNo +" ) and (outworkdate is null or outworkdate>='"
                      +OutWorkBeginDate + "') and agentcode=latree.agentcode)";
                sqlBF.append(sql);
            }
            //增加对于互动渠道 的判断
            //2014-12-8 by yangyang
            else if (mBranchType.equals("5"))
            {
                if (mAgentGrade != null && !mAgentGrade.equals(""))
                {
                    sqlBF.append(" and agentgrade='" + mAgentGrade + "'");
                }
                sql =" and (speciFlag is null or speciFlag<>'01' ) "
                    +" and exists ("
                    +" select 'X' from laagent where BranchType = '"+mBranchType+"' and BranchType2='"+mBranchType2+"' "
                    +" and EmployDate <= (select enddate from lastatsegment where stattype='5' and yearmonth= " +this.mIndexCalNo +" ) "
                    +" and (outworkdate is null or outworkdate>='" +OutWorkBeginDate + "') "
                    +" and agentcode=latree.agentcode"
                    +" ) "
                    +" and not exists ("
                    +" select 'X' from lawage where managecom ='" + mManageCom + "' and indexcalno='" + mIndexCalNo
                    +"' and agentcode=latree.agentcode ) "
                    +" order by AgentGrade,AgentCode asc ";
                sqlBF.append(sql);
            }
          //新增健康管理渠道2016-11-07
            else if(mBranchType.equals("7"))
            {
                if (mAgentGrade != null && !mAgentGrade.equals(""))
                {
                    sqlBF.append(" and agentgrade='" + mAgentGrade + "'");
                }
                sql =" and (speciFlag is null or speciFlag<>'01' ) "
                    +" and exists ("
                    +" select 'X' from laagent where BranchType = '"+mBranchType+"' and BranchType2='"+mBranchType2+"' "
                    +" and EmployDate <= (select enddate from lastatsegment where stattype='5' and yearmonth= " +this.mIndexCalNo +" ) "
                    +" and (outworkdate is null or outworkdate>='" +OutWorkBeginDate + "') "
                    +" and agentcode=latree.agentcode"
                    +" ) "
                    +" and not exists ("
                    +" select 'X' from lawage where managecom ='" + mManageCom + "' and indexcalno='" + mIndexCalNo
                    +"' and agentcode=latree.agentcode ) "
                    +" order by AgentGrade,AgentCode asc ";
                sqlBF.append(sql);
            }
            System.out.println(sqlBF.toString());
            if (sqlBF != null && !sqlBF.equals(""))
            {
                mLATreeSet = tLATreeDB.executeQuery(sqlBF.toString());//得到行政信息
                if (tLATreeDB.mErrors.needDealError())
                {
                    this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                    buildError("deal", "查询机构人员时，DB类出错");
                    return false;
                }
            }
        }
        //个例
        else
        {
            tLATreeSchema = mLATreeSet.get(1);
            this.isReCalFlag = true;//
            mManageCom = tLATreeSchema.getManageCom();
            String cType="";
            if(mBranchType.equals("1") && mBranchType2.equals("01"))
            {
                cType="01";
            }
            else  if(mBranchType.equals("2") && mBranchType2.equals("01"))
            {
                cType="02";
            }
            else if(mBranchType.equals("2") && mBranchType2.equals("02"))
             {
                 cType="03";
             }
             //modify by Abigale  367-371
             else if(mBranchType.equals("3") && mBranchType2.equals("01"))
             {
                 cType="03";
             }
            mAreaType = AgentPubFun.getAreaType(mManageCom.substring(0, 4),cType);
           //因为是个案，所以BranchType设置为""，表示不限制，但是在佣金计算基础类中会查询出BranchType
        }
        //个例才计算这个
        if (isReCalFlag)
        {
            TransferData tTransferData = new TransferData();
            VData tOutputData = new VData();
            for (int i = 1; i <= mLATreeSet.size(); i++)
            {
                if (this.mBranchType.equals("1"))
                {
                    LAAgentSet tLAAgentSet = new LAAgentSet();
                    LAAgentDB tLAAgentDB = new LAAgentDB();
                    tLAAgentDB.setAgentCode(mLATreeSet.get(i).getAgentCode());
                    tLAAgentDB.getInfo();
                    tLAAgentSet.add(tLAAgentDB);
                    tTransferData = new TransferData();
                    tTransferData.setNameAndValue("ManageCom", mManageCom);
                    tTransferData.setNameAndValue("IndexCalNo", mIndexCalNo);
                    tTransferData.setNameAndValue("GlobalInput", mGlobalInput);
                    tTransferData.setNameAndValue("BranchType", mBranchType);
                    tTransferData.setNameAndValue("BranchType2",mBranchType2) ;
                    tTransferData.setNameAndValue("LAAgentSet", tLAAgentSet);//传进来有人了

                    tOutputData = new VData();
                    tOutputData.add(tTransferData);
                    AgentMonthCal tAgentMonthCal = new AgentMonthCal();
                    if (!tAgentMonthCal.submitData(tOutputData, ""))
                    {
                        this.mErrors.copyAllErrors(tAgentMonthCal.mErrors);
                        buildError("dealData",
                                   "计算" + tLATreeSchema.getAgentCode() +
                                   "的基础指标失败!");
                        return false;
                    }
                }
            }
        }
        //
        String tAgentGrade = new String();
        LAIndexVsCommSet tLAIndexVsCommSet = new LAIndexVsCommSet();
        LAAssessIndexSet tLAAssessIndexSet = new LAAssessIndexSet();
        for (int i = 1; i <= mLATreeSet.size(); i++)
        {
            StringBuffer sqlSB = new StringBuffer();

            tLATreeSchema = mLATreeSet.get(i);
            mAgentCode=tLATreeSchema.getAgentCode();
            //获取人员的agentgroup 如果此人的机构的state=1  则不算薪资与考核   xiangchun
            String tAgentGroup=tLATreeSchema.getBranchCode();
            mWageVersion=tLATreeSchema.getWageVersion();//直接取的是latree表当时薪资版本？？？
            if (mBranchType.equals("2"))
            {
                tAgentGroup=AgentPubFun.getMonthAgentGroup(mAgentCode,mIndexCalNo);
                mAreaType = AgentPubFun.getAreaType(tAgentGroup);
            }
            if (mBranchType.equals("3") && mBranchType2.equals("01"))
            {
              tAgentGroup=AgentPubFun.getMonthAgentGroup(mAgentCode,mIndexCalNo);
              mAreaType = getAreaType(mManageCom,mBranchType,mBranchType2);
            }
            LABranchGroupDB tLABranchGroupDB=new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tAgentGroup);
            tLABranchGroupDB.getInfo();
            LABranchGroupSchema tLABranchGroupSchema=new LABranchGroupSchema();
            tLABranchGroupSchema=tLABranchGroupDB.getSchema();
            String tState=tLABranchGroupSchema.getState();
            //state=1  不算薪资与考核   xiangchun
            if("1".equals(tState))
            {
                continue;
            }
            //得到当月的职级
            tAgentGrade = tLATreeSchema.getAgentGrade();
            System.out.println("搞不懂了"+tAgentGrade);
            if (mBranchType.equals("2"))
            {
                tAgentGrade = AgentPubFun.getMonthAgentGrade(mAgentCode,mIndexCalNo);
            }
            if (mBranchType.equals("3") && mBranchType2.equals("01"))
            {
                tAgentGrade = AgentPubFun.getMonthAgentGradeBK(mAgentCode,mIndexCalNo);
            }
            
            
            //因为添加了版本 已经不能单一判断了，暂时不用哈希函数判断了
            if(mBranchType.equals("1")){
            	String tAgentGradeVersion=tAgentGrade+"||"+mWageVersion;
            	if (!mAgentGradeHash.containsKey(tAgentGradeVersion))
              {
                //如果哈希表中还没有当前AgentGrade对应的记录，则查找该AgentGrade对应的指标项集合
                LDSysVarDB tLDSysVarDB = new LDSysVarDB();
                tLDSysVarDB.setSysVar("AgentLatitude");
                if (!tLDSysVarDB.getInfo())
                {
                    //如果没有查询到相关信息，则表示缺少纬度配置
                    buildError("dealData",
                               "查询代理人佣金纬度配置失败");
                    return false;
                }
                //xiangchun 2006-9-25 delete
                LAIndexVsCommDB tLAIndexVsCommDB = new LAIndexVsCommDB();
                //根据配置纬度信息，按照职级、或职务循环佣金项目
                //LDSysVar现在的结果AGENTGRADE
                if (tLDSysVarDB.getSysVarValue().trim().compareToIgnoreCase("AgentKind") == 0)
                {
                    sqlSB.append(
                            "select * from laindexvscomm where agentgrade='" +
                            tLATreeSchema.getAgentKind() + "' ");
                }
                //运行此处
                else
                {
                    sqlSB.append(
                            "select * from laindexvscomm where agentgrade='" +
                            tAgentGrade + "' ");
                }

                //2010-03-18 Abigale 添加版本号
                if (mBranchType.equals("1"))
                {
                    sqlSB.append(" and  wageversion='" +tLATreeSchema.getWageVersion() + "' ");
                }
                //2010-03-18 Abigale


                if (mWageType != null && !mWageType.equals(""))
                {
                    sqlSB.append(" and wageType in ('" + mWageType + "')");
                }
                if (mWageCode != null && !mWageCode.equals(""))
                {
                    sqlSB.append(" and wagecode in (" + mWageCode + ")");
                }

                sqlSB.append(" order by wageorder ");
                System.out.println(sqlSB.toString());
                tLAIndexVsCommSet = tLAIndexVsCommDB.executeQuery(sqlSB.toString());
                System.out.println(tLAIndexVsCommSet.size());
                //**  为了报错信息的显示，这段没有注释
                if(tLAIndexVsCommSet.size()<=0)
                {
                     this.mErrors.copyAllErrors(tLAIndexVsCommDB.mErrors);
                     buildError("dealData",
                                "查询AgentGrade为" + tAgentGrade +
                                "空的佣金项指标失败");
                     return false;
                }

                tLAAssessIndexSet = new LAAssessIndexSet();
                for (int j = 1; j <= tLAIndexVsCommSet.size(); j++)
                {  
                	//====得到他的indextype
                    String tIndexCode = tLAIndexVsCommSet.get(j).getIndexCode();
                    
                    LAAssessIndexSchema tLAAssessIndexSchema = new   LAAssessIndexSchema();
                    
                    if (!mIndexHash.containsKey(tAgentGrade))
                    {
                        LAAssessIndexDB tLAAssessIndexDB = new LAAssessIndexDB();
                        tLAAssessIndexDB.setIndexCode(tIndexCode);
                        System.out.println("指标---indexcode"+tIndexCode);
                        if (!tLAAssessIndexDB.getInfo())
                        {
                            this.mErrors.copyAllErrors(tLAAssessIndexDB.mErrors);
                            buildError("dealData","未查到指标编码为" + tIndexCode + "的指标信息");
                            return false;
                        }
                        tLAAssessIndexSchema = tLAAssessIndexDB.getSchema();
                        mIndexHash.put(tIndexCode, tLAAssessIndexSchema);
                    }
                    else
                    {
                        tLAAssessIndexSchema = (LAAssessIndexSchema) mIndexHash.get(tIndexCode);
                    }
                    tLAAssessIndexSet.add(tLAAssessIndexSchema);
                }

                mAgentGradeHash.put(tAgentGradeVersion, tLAIndexVsCommSet);
                mAgentGradeIndexHash.put(tAgentGradeVersion, tLAAssessIndexSet);
              }
              else
              { //否则，取出该职级对应的指标项集合
                  tLAIndexVsCommSet = (LAIndexVsCommSet) mAgentGradeHash.get(tAgentGradeVersion);
                  tLAAssessIndexSet = (LAAssessIndexSet) mAgentGradeIndexHash.get(tAgentGradeVersion);
              }
            }
            else 
            {            
               if (!mAgentGradeHash.containsKey(tAgentGrade))
               {
                   //如果哈希表中还没有当前AgentGrade对应的记录，则查找该AgentGrade对应的指标项集合
                   LDSysVarDB tLDSysVarDB = new LDSysVarDB();
                   tLDSysVarDB.setSysVar("AgentLatitude");
                   if (!tLDSysVarDB.getInfo())
                   {
                       //如果没有查询到相关信息，则表示缺少纬度配置
                       buildError("dealData",
                                  "查询代理人佣金纬度配置失败");
                       return false;
                   }
                   //xiangchun 2006-9-25 delete
                   LAIndexVsCommDB tLAIndexVsCommDB = new LAIndexVsCommDB();
                   //根据配置纬度信息，按照职级、或职务循环佣金项目
                   //LDSysVar现在的结果AGENTGRADE
                   System.out.println("除了嘛情况"+tAgentGrade);
                   if (tLDSysVarDB.getSysVarValue().trim().compareToIgnoreCase("AgentKind") ==
                       0)
                   {
                       sqlSB.append(
                               "select * from laindexvscomm where agentgrade='" +
                               tLATreeSchema.getAgentKind() + "' ");
                   }
                   //运行此处
                   else
                   {
                       sqlSB.append("select * from laindexvscomm where agentgrade='"+ tAgentGrade + "' ");
                   }
               
                   //2010-03-18 Abigale 添加版本号
                   if (mBranchType.equals("1"))
                   {
                       sqlSB.append(" and  wageversion='" +tLATreeSchema.getWageVersion() + "' ");
                   }
                   //2010-03-18 Abigale
               
               
                   if (mWageType != null && !mWageType.equals(""))
                   {
                       sqlSB.append(" and wageType in ('" + mWageType + "')");
                   }
                   if (mWageCode != null && !mWageCode.equals(""))
                   {
                       sqlSB.append(" and wagecode in (" + mWageCode + ")");
                   }
               
                   sqlSB.append(" order by wageorder ");
                   System.out.println(sqlSB.toString());
                   tLAIndexVsCommSet = tLAIndexVsCommDB.executeQuery(sqlSB.toString());
                   System.out.println(tLAIndexVsCommSet.size());
                  //**  为了报错信息的显示，这段没有注释
                  if(tLAIndexVsCommSet.size()<=0)
                  {
                       this.mErrors.copyAllErrors(tLAIndexVsCommDB.mErrors);
                       buildError("dealData",
                                  "查询AgentGrade为" + tAgentGrade +
                                  "空的佣金项指标失败");
                       return false;
                  }
               
                   tLAAssessIndexSet = new LAAssessIndexSet();
                   for (int j = 1; j <= tLAIndexVsCommSet.size(); j++)
                   {
                       String tIndexCode = tLAIndexVsCommSet.get(j).getIndexCode();
                       LAAssessIndexSchema tLAAssessIndexSchema = new   LAAssessIndexSchema();
                       if (!mIndexHash.containsKey(tAgentGrade))
                       {
                           LAAssessIndexDB tLAAssessIndexDB = new LAAssessIndexDB();
                           tLAAssessIndexDB.setIndexCode(tIndexCode);
                           if (!tLAAssessIndexDB.getInfo())
                           {
                               this.mErrors.copyAllErrors(tLAAssessIndexDB.mErrors);
                               buildError("dealData","未查到指标编码为" + tIndexCode + "的指标信息");
                               return false;
                           }
                           tLAAssessIndexSchema = tLAAssessIndexDB.getSchema();
                           mIndexHash.put(tIndexCode, tLAAssessIndexSchema);
                       }
                       else
                       {
                           tLAAssessIndexSchema = (LAAssessIndexSchema) mIndexHash.get(tIndexCode);
                       }
                       tLAAssessIndexSet.add(tLAAssessIndexSchema);
                   }
               
                   mAgentGradeHash.put(tAgentGrade, tLAIndexVsCommSet);
                   mAgentGradeIndexHash.put(tAgentGrade, tLAAssessIndexSet);
               }
               else
               { //否则，取出该职级对应的指标项集合
                   tLAIndexVsCommSet = (LAIndexVsCommSet) mAgentGradeHash.get(tAgentGrade);
                   tLAAssessIndexSet = (LAAssessIndexSet) mAgentGradeIndexHash.get(tAgentGrade);
               }
            }
            if (tLAIndexVsCommSet == null)
            {
                buildError("dealData","查询AgentGrade为" + tAgentGrade +"的佣金项指标为空");
                return false;
            }
            if (tLAIndexVsCommSet.size() == 0)
            {
                return true;
            }

            //计算一个人的佣金之前，先计算这个人的基础指标
            TransferData tTransferData = new TransferData();
            VData tOutputData = new VData();
            tOutputData = new VData();
            tOutputData.addElement(mIndexCalNo);
            tOutputData.addElement(mManageCom);
            tOutputData.addElement(mAreaType);
            tOutputData.addElement(mBranchType);
            tOutputData.addElement(mBranchType2);
            tTransferData = new TransferData();
            tTransferData.setNameAndValue("GlobalInput", mGlobalInput);
            tTransferData.setNameAndValue("LAIndexVsCommSet", tLAIndexVsCommSet);
            tTransferData.setNameAndValue("LAAssessIndexSet", tLAAssessIndexSet);
            tTransferData.setNameAndValue("LATreeSchema", tLATreeSchema);
            tOutputData.addElement(tTransferData);
            System.out.println("--------开始计算代理人佣金指标------------------------------");
            CalWageBaseUI tCalWageBaseUI = new CalWageBaseUI();
            if (!tCalWageBaseUI.submitData(tOutputData))
            {
                this.mErrors.copyAllErrors(tCalWageBaseUI.mErrors);
                buildError("dealData",
                           "计算代理人" + tLATreeSchema.getAgentCode() + "的佣金`指标时出错");
                return false;
            }
            System.out.println(
                    "-----------------------------------------------------");
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LACalWageBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    //新增银代获取地域类型
    public static String getAreaType(String cManageCom,String cBranchType,String cBranchType2)
    {
           //AreaType

           
           String tSql = "Select trim(WageFlag) From LABankIndexRadix Where managecom = trim(substr('"+cManageCom+"',1,4)) and branchtype = '" +
                        cBranchType + "' and branchtype2='"+cBranchType2+"' ";
           ExeSQL tExeSQL = new ExeSQL();
           String tAreaType = tExeSQL.getOneValue(tSql);
           if (tExeSQL.mErrors.needDealError())
               return null;

           if (tAreaType == null || tAreaType.equals(""))
               return null;
           return tAreaType;
   }
}
