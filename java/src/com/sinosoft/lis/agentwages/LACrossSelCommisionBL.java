package com.sinosoft.lis.agentwages;

import com.sinosoft.lis.vschema.LACrossWageBSet;
import com.sinosoft.lis.vschema.LACrossWageSet;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.lis.schema.LACrossWageBSchema;
import com.sinosoft.lis.schema.LACrossWageSchema;
import com.sinosoft.lis.db.LACrossWageDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAWageDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: LACommisionAwardRateBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author miaoxz
 * @version 1.0----2009-1-13
 */
public class LACrossSelCommisionBL {
  //错误处理类
  public static CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LACrossWageSet mLACrossWageSet = new LACrossWageSet();
  private Reflections ref = new Reflections();
  //private LARateCommisionBSet mLARateCommisionBSet = new LARateCommisionBSet();

  public LACrossSelCommisionBL() {

  }

//	public static void main(String[] args)
//	{
//		LaratecommisionSetBL LaratecommisionSetbl = new LaratecommisionSetBL();
//	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LACrossSelCommisionBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check()){
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start LACrossSelCommisionBL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LACrossSelCommisionBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LACrossSelCommisionBL.getInputData.........");
      this.mErrors.clearErrors();
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     System.out.println("GlobalInput get");
     // this.mLARateCommisionSet.set( (LARateCommisionSet) cInputData.get(1));
     this.mLACrossWageSet.set( (LACrossWageSet) cInputData.getObjectByObjectName("LACrossWageSet",0));
      System.out.println("LACrossWageSet get"+mLACrossWageSet.size());

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LACrossSelCommisionBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }
  /**
   * 新增，修改传入的数据校验
   */
  private boolean check(){
	  
	  //进行重复数据校验   
	  System.out.println("Begin LACrossSelCommisionBL.check.........1"+mLACrossWageSet.size());	 
	  	  LACrossWageSchema tempLACrossWageSchema1; 
	      LACrossWageSchema tempLACrossWageSchema2;
//	      LACrossWageSchema tLACrossWageSchemaNew = new LACrossWageSchema();
		  ExeSQL tExeSQL=new ExeSQL();
		  StringBuffer tSB;
		  String tResult;
//		  LACrossWageDB tLACrossWageDB = new LACrossWageDB();
		  for(int i=1;i<=this.mLACrossWageSet.size();i++){
			  tempLACrossWageSchema1=this.mLACrossWageSet.get(i);
			  //检查业务员与管理机构是否匹配
			  String tMngCom = tempLACrossWageSchema1.getManageCom();
			  String tCheckSQL = " select managecom from laagent where agentcode ='"+tempLACrossWageSchema1.getAgentCode()+"' and branchtype ='1' and branchtype2='01' ";
			  String tMnaCom1 = tExeSQL.getOneValue(tCheckSQL);
			  if(!tMnaCom1.equals(tMngCom)){
		            CError tError = new CError();
		            tError.moduleName = "LACrossSelCommisionBL";
		            tError.functionName = "check";
		            tError.errorMessage = "录入的管理机构与业务员不匹配/录入的业务员的销售渠道不正确！";
		            this.mErrors.addOneError(tError);
		            return false;
			  }
			  //进行业务员校验（包含离职时间与薪资计算）
			  String tAgentCode = mLACrossWageSet.get(i).getAgentCode();
			  LAAgentDB tLAAgentDB = new LAAgentDB();
			  tLAAgentDB.setAgentCode(tAgentCode);
			  //查询代理人是否存在
			  if(!tLAAgentDB.getInfo()){
		            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
		            CError tError = new CError();
		            tError.moduleName = "LACrossSelCommisionBL";
		            tError.functionName = "check";
		            tError.errorMessage = "查询不到代理人" + tAgentCode + "的信息!";
		            this.mErrors.addOneError(tError);
		            return false;
			  }
			  //离职时间与所在薪资月的校验
			  String tAgentState = tLAAgentDB.getAgentState();
			  if(tAgentState!=null && tAgentState.compareTo("06")>=0){
				  String tOutWorkDate = tLAAgentDB.getOutWorkDate();
				  String tSectionSql =" select startdate,enddate from LAStatSegment where stattype ='5' and " +
				  		"yearmonth='"+mLACrossWageSet.get(i).getWageNo()+"' ";
				  SSRS tSSRS = new SSRS();
				  tSSRS = tExeSQL.execSQL(tSectionSql);
				  String tStarDate = tSSRS.GetText(1, 1);
				  String tEndDate = tSSRS.GetText(1, 2);
				  if(tOutWorkDate.compareTo(tStarDate)<0){
		                CError tError = new CError();
		                tError.moduleName = "LACrossSelCommisionBL";
		                tError.functionName = "check";
		                tError.errorMessage = "代理人" + tAgentCode + "在" + tOutWorkDate +
		                                      "已经离职，不能录入" + mLACrossWageSet.get(i).getWageNo() + "的交叉销售佣金!";
		                this.mErrors.addOneError(tError);
		                return false;
				  }
			  }
			  //校验薪资是否确认与审核发放
			  String tWageSql ="select * from lawage where branchtype ='1' and branchtype2='01' and indexcalno ='"+mLACrossWageSet.get(i).getWageNo()+"'" +
			  		" and managecom ='"+mLACrossWageSet.get(i).getManageCom()+"' fetch first 5 rows only with ur ";
			  LAWageDB tLAWageDB = new LAWageDB();
			  LAWageSet tLAWageSet = new LAWageSet();
			  tLAWageSet = tLAWageDB.executeQuery(tWageSql);
			  if(tLAWageSet.size()>0){
				  CError tError = new CError();
	                tError.moduleName = "LACrossSelCommisionBL";
	                tError.functionName = "check";
	                tError.errorMessage = "机构:"+mLACrossWageSet.get(i).getManageCom()+"月份:"+mLACrossWageSet.get(i).getWageNo()+"已经计算过佣金,不能进行下一步操作!";
	                this.mErrors.addOneError(tError);
	                return false;
			  }
			  if(this.mOperate.equals("INSERT")||this.mOperate.equals("UPDATE")){
				  for(int j =i+1; j<=this.mLACrossWageSet.size();j++){
					  tempLACrossWageSchema2 = this.mLACrossWageSet.get(j);
					  if(tempLACrossWageSchema1.getAgentCode().equals(tempLACrossWageSchema2.getAgentCode())
						&&tempLACrossWageSchema1.getWageNo().equals(tempLACrossWageSchema2.getWageNo())
					  ){
							 BuildError("dealData","第"+i+","+j+"行业务员代码,薪资月相同请检查！");
		                     return false; 
					  }
				  }
			  }
			  if(this.mOperate.equals("INSERT")){
				  tSB=new StringBuffer();
				  tSB=tSB.append("select distinct 1 from LACrossWage where  managecom='")
				  .append(tempLACrossWageSchema1.getManageCom())
				  .append("' and agentcode = '")
				  .append(tempLACrossWageSchema1.getAgentCode())
				  .append("' and wageno = '")
				  .append(tempLACrossWageSchema1.getWageNo())
				  .append("' and branchtype='")
				  .append(tempLACrossWageSchema1.getBranchType())
				  .append("' and branchtype2='")
				  .append(tempLACrossWageSchema1.getBranchType2())
				  .append("'")
				  ;
				  tResult=tExeSQL.getOneValue(tSB.toString());
				  if(tResult!=null&&tResult.equals("1")){
					  CError tError = new CError();
				      tError.moduleName = "LACrossSelCommisionBL";
				      tError.functionName = "check()";				
				      tError.errorMessage = "第"+i+"行业务员当前薪资月的交叉销售佣金已录入";				      
				      this.mErrors.clearErrors();
				      this.mErrors.addOneError(tError);
				      return false;
			  }
		  }
//			 if(mOperate.equals("UPDATE")){
////				 tLACrossWageDB.setIdx(tempLACrossWageSchema1.getIdx());
//				 tLACrossWageSchemaNew = tLACrossWageDB.query().get(1);
//				 if(!tempLACrossWageSchema1.getManageCom().equals(tLAWageRadixAllSchemaNew.getManageCom())||
//					!tempLACrossWageSchema1.getAgentGrade().equals(tLAWageRadixAllSchemaNew.getAgentGrade())
//				 ){
//					 BuildError("dealData","第"+i+"行管理机构,职级代码,不能修改！");
//                     return false;
//				 }
//			 }
		  
	  }
	  return true;
  
  }
  /**
   * 业务处理主函数
   */
  private boolean dealData() {
	    System.out.println("Begin LACrossSelCommisionBL.dealData........."+mOperate);
	    try {
	    	LACrossWageSchema  tLACrossWageSchemaNew = new  LACrossWageSchema();
	    	LACrossWageSchema  tLACrossWageSchemaOld = new  LACrossWageSchema();
	    	LACrossWageDB tLACrossWageDB =new  LACrossWageDB();
	        if(mOperate.equals("INSERT")) {
		        System.out.println("Begin LACrossSelCommisionBL.dealData.........INSERT"+mLACrossWageSet.size());
		        LACrossWageSet tLACrossWageSet = new LACrossWageSet();
		        LACrossWageBSet tLACrossWageBSet = new LACrossWageBSet();
		        for (int i = 1; i <= mLACrossWageSet.size(); i++) {
		        	tLACrossWageSchemaNew = mLACrossWageSet.get(i);	
//			          ExeSQL tExe = new ExeSQL();
//			          String tSql =
//			              "select max(int(idx)) from LAWageRadixAll order by 1 desc";
//			          String strIdx = "";
//			          int tMaxIdx = 0;
//			          strIdx = tExe.getOneValue(tSql);
//			          if (strIdx == null || strIdx.trim().equals("")) {
//			            tMaxIdx = 0;
//			          }
//			          else {
//			            tMaxIdx = Integer.parseInt(strIdx);
//			          }
//			      tMaxIdx += i;
//			      String sMaxIdx =tMaxIdx+"";
//			      tLAWageRadixAllSchemaNew.setIdx(sMaxIdx);
			      tLACrossWageSchemaNew.setMakeDate(CurrentDate);
			      tLACrossWageSchemaNew.setMakeTime(CurrentTime);
			      tLACrossWageSchemaNew.setModifyDate(CurrentDate);
			      tLACrossWageSchemaNew.setModifyTime(CurrentTime);
			      tLACrossWageSchemaNew.setOperator(mGlobalInput.Operator); 
			      tLACrossWageSet.add(tLACrossWageSchemaNew);
	      }
	        	map.put(tLACrossWageSet, mOperate);
//	        	map.put(tLACrossWageBSet, "INSERT");
	      }
	      if(mOperate.equals("UPDATE")) {
	    	  System.out.println("111111---"+mLACrossWageSet.size());
	    	  LACrossWageSet tLACrossWageSet = new LACrossWageSet();
	    	  LACrossWageBSet tLACrossWageBSet = new LACrossWageBSet();
	    	 for(int i = 1;i<=mLACrossWageSet.size();i++){
	    		
//	    		 tLAWageRadixAllDB.setIdx(mLAWageRadixAllSet.get(i).getIdx());
	    		 tLACrossWageDB.setManageCom(mLACrossWageSet.get(i).getManageCom());
	    		 tLACrossWageDB.setAgentCode(mLACrossWageSet.get(i).getAgentCode());
	    		 tLACrossWageDB.setWageNo(mLACrossWageSet.get(i).getWageNo());
	    		 tLACrossWageDB.setBranchType(mLACrossWageSet.get(i).getBranchType());
	    		 tLACrossWageDB.setBranchType2(mLACrossWageSet.get(i).getBranchType2());
	    		 tLACrossWageSchemaOld = tLACrossWageDB.query().get(1); 
	    		 tLACrossWageSchemaNew = mLACrossWageSet.get(i);
	    		 tLACrossWageSchemaNew.setMakeDate(tLACrossWageSchemaOld.getMakeDate());
	    		 tLACrossWageSchemaNew.setMakeTime(tLACrossWageSchemaOld.getMakeTime());
	    		 tLACrossWageSchemaNew.setModifyDate(CurrentDate);
	    		 tLACrossWageSchemaNew.setModifyTime(CurrentTime);
	    		 tLACrossWageSchemaNew.setOperator(mGlobalInput.Operator); 
	    		 tLACrossWageSet.add(tLACrossWageSchemaNew);
			      //插入备份轨迹表
	    		 LACrossWageBSchema  tLACrossWageBSchema = new  LACrossWageBSchema();
	    		 System.out.println("2222");
	              ref.transFields(tLACrossWageBSchema, tLACrossWageSchemaOld);
	              System.out.println("333");
			      String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
			      tLACrossWageBSchema.setEdorNo(tEdorNo);
			      tLACrossWageBSchema.setPropertyFYC(tLACrossWageSchemaOld.getPropertyFYC());
			      tLACrossWageBSchema.setLifeFYC(tLACrossWageSchemaOld.getLifeFyc());
			      tLACrossWageBSchema.setMakeDate(PubFun.getCurrentDate());
			      tLACrossWageBSchema.setMakeTime(PubFun.getCurrentTime());
			      tLACrossWageBSchema.setModifyDate(PubFun.getCurrentDate());
			      tLACrossWageBSchema.setModifyTime(PubFun.getCurrentTime());
//			      tLAWageRadixAllTraceSchema.setEdorType("01");//”修改“标识
//			      tLAWageRadixAllTraceSchema.setIdx(tLAWageRadixAllSchemaOld.getIdx());
//			      tLAWageRadixAllTraceSchema.setAgentGrade(tLAWageRadixAllSchemaOld.getAgentGrade());
//			      tLAWageRadixAllTraceSchema.setManageCom(tLAWageRadixAllSchemaOld.getManageCom());
//			      tLAWageRadixAllTraceSchema.setAgentType(tLAWageRadixAllSchemaOld.getAgentType());
//			      tLAWageRadixAllTraceSchema.setWageCode(tLAWageRadixAllSchemaOld.getWageCode());
//			      tLAWageRadixAllTraceSchema.setWageType(tLAWageRadixAllSchemaOld.getWageType());
//			      tLAWageRadixAllTraceSchema.setWageName(tLAWageRadixAllSchemaOld.getWageName());
//			      tLAWageRadixAllTraceSchema.setRewardMoney(tLAWageRadixAllSchemaOld.getRewardMoney());
//			      tLAWageRadixAllTraceSchema.setBranchType(tLAWageRadixAllSchemaOld.getBranchType());
//			      tLAWageRadixAllTraceSchema.setBranchType2(tLAWageRadixAllSchemaOld.getBranchType2());
//			      tLAWageRadixAllTraceSchema.setMakeDate(tLAWageRadixAllSchemaOld.getMakeDate());
//			      tLAWageRadixAllTraceSchema.setMakeTime(tLAWageRadixAllSchemaOld.getMakeTime());
//			      tLAWageRadixAllTraceSchema.setModifyDate(tLAWageRadixAllSchemaOld.getModifyDate());
//			      tLAWageRadixAllTraceSchema.setModifyTime(tLAWageRadixAllSchemaOld.getModifyTime());
//			      tLAWageRadixAllTraceSchema.setOperator(tLAWageRadixAllSchemaOld.getOperator());
	              tLACrossWageBSet.add(tLACrossWageBSchema);
	              System.out.println(tLACrossWageBSchema.getAgentCode()+"121212");
	    	 }
	    	 map.put(tLACrossWageSet, mOperate);
	    	 map.put(tLACrossWageBSet, "INSERT");
	      }
	      if(mOperate.equals("DELETE")){
	    	  LACrossWageSet tLACrossWageSet = new LACrossWageSet();
	    	  LACrossWageBSet tLACrossWageBSet = new LACrossWageBSet();
	    	 for(int i = 1;i<=mLACrossWageSet.size();i++){	
	    		 tLACrossWageDB.setManageCom(mLACrossWageSet.get(i).getManageCom());
	    		 tLACrossWageDB.setAgentCode(mLACrossWageSet.get(i).getAgentCode());
	    		 tLACrossWageDB.setWageNo(mLACrossWageSet.get(i).getWageNo());
	    		 tLACrossWageDB.setBranchType(mLACrossWageSet.get(i).getBranchType());
	    		 tLACrossWageDB.setBranchType2(mLACrossWageSet.get(i).getBranchType2());
	    		 tLACrossWageSchemaOld = tLACrossWageDB.query().get(1); 
	    		 tLACrossWageSchemaNew = mLACrossWageSet.get(i);
	    		 tLACrossWageSchemaNew.setMakeDate(tLACrossWageSchemaOld.getMakeDate());
	    		 tLACrossWageSchemaNew.setMakeTime(tLACrossWageSchemaOld.getMakeTime());
	    		 tLACrossWageSchemaNew.setModifyDate(CurrentDate);
	    		 tLACrossWageSchemaNew.setModifyTime(CurrentTime);
	    		 tLACrossWageSchemaNew.setOperator(mGlobalInput.Operator); 
	    		 tLACrossWageSet.add(tLACrossWageSchemaNew);
			      //插入备份轨迹表
	    		 LACrossWageBSchema  tLACrossWageBSchema = new  LACrossWageBSchema();
	             ref.transFields(tLACrossWageBSchema, tLACrossWageSchemaOld);
			     String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
			     tLACrossWageBSchema.setEdorNo(tEdorNo);
			     tLACrossWageBSchema.setPropertyFYC(tLACrossWageSchemaOld.getPropertyFYC());
			      tLACrossWageBSchema.setLifeFYC(tLACrossWageSchemaOld.getLifeFyc());
			      tLACrossWageBSchema.setMakeDate(PubFun.getCurrentDate());
			      tLACrossWageBSchema.setMakeTime(PubFun.getCurrentTime());
			      tLACrossWageBSchema.setModifyDate(PubFun.getCurrentDate());
			      tLACrossWageBSchema.setModifyTime(PubFun.getCurrentTime());
//			      tLAWageRadixAllTraceSchema.setEdorType("02");//“删除“标识
//			      tLAWageRadixAllTraceSchema.setIdx(tLAWageRadixAllSchemaOld.getIdx());
//			      tLAWageRadixAllTraceSchema.setAgentGrade(tLAWageRadixAllSchemaOld.getAgentGrade());
//			      tLAWageRadixAllTraceSchema.setManageCom(tLAWageRadixAllSchemaOld.getManageCom());
//			      tLAWageRadixAllTraceSchema.setAgentType(tLAWageRadixAllSchemaOld.getAgentType());
//			      tLAWageRadixAllTraceSchema.setWageCode(tLAWageRadixAllSchemaOld.getWageCode());
//			      tLAWageRadixAllTraceSchema.setWageType(tLAWageRadixAllSchemaOld.getWageType());
//			      tLAWageRadixAllTraceSchema.setWageName(tLAWageRadixAllSchemaOld.getWageName());
//			      tLAWageRadixAllTraceSchema.setRewardMoney(tLAWageRadixAllSchemaOld.getRewardMoney());
//			      tLAWageRadixAllTraceSchema.setBranchType(tLAWageRadixAllSchemaOld.getBranchType());
//			      tLAWageRadixAllTraceSchema.setBranchType2(tLAWageRadixAllSchemaOld.getBranchType2());
//			      tLAWageRadixAllTraceSchema.setMakeDate(tLAWageRadixAllSchemaOld.getMakeDate());
//			      tLAWageRadixAllTraceSchema.setMakeTime(tLAWageRadixAllSchemaOld.getMakeTime());
//			      tLAWageRadixAllTraceSchema.setModifyDate(tLAWageRadixAllSchemaOld.getModifyDate());
//			      tLAWageRadixAllTraceSchema.setModifyTime(tLAWageRadixAllSchemaOld.getModifyTime());
//			      tLAWageRadixAllTraceSchema.setOperator(tLAWageRadixAllSchemaOld.getOperator());
	             tLACrossWageBSet.add(tLACrossWageBSchema);
	             System.out.println(tLACrossWageBSchema.getAgentCode()+"121212");
	    	 }
	    	 map.put(tLACrossWageSet, mOperate);
	    	 map.put(tLACrossWageBSet, "INSERT");
	      }
	    }
	    catch (Exception ex) {
	      // @@错误处理
	      CError tError = new CError();
	      tError.moduleName = "LACrossSelCommisionBL";
	      tError.functionName = "dealData";
	      tError.errorMessage = "在处理所数据时出错。";
	      this.mErrors.addOneError(tError);
	      return false;
	    }
	    return true;
	  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      System.out.println(
          "Begin LACommisionAwardRateBL.prepareOutputData.........");
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LACommisionAwardRateBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  private void BuildError(String tFun,String tMess)
  {
      CError tError = new CError();
      tError.moduleName = "LAAssessMixFYCBL";
      tError.functionName = tFun;
      tError.errorMessage = tMess;
      this.mErrors.addOneError(tError);

} 
}
