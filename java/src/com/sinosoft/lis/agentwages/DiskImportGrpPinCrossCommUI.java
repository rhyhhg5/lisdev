package com.sinosoft.lis.agentwages;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author luomin
 * @version 1.1
 */
public class DiskImportGrpPinCrossCommUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private DiskImportGrpPinCrossCommBL mDiskImportGrpPinCrossCommBL = null;

    public DiskImportGrpPinCrossCommUI()
    {
    	mDiskImportGrpPinCrossCommBL = new DiskImportGrpPinCrossCommBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if(!mDiskImportGrpPinCrossCommBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mDiskImportGrpPinCrossCommBL.mErrors);
            return false;
        }
        //
        if(mDiskImportGrpPinCrossCommBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mDiskImportGrpPinCrossCommBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mDiskImportGrpPinCrossCommBL.getImportPersons();
    }

    public int getUnImportRecords()
    {
        return mDiskImportGrpPinCrossCommBL.getUnImportRecords();
    }

    public static void main(String[] args)
    {
    	DiskImportCrossCommUI zDiskImportCrossCommUI = new DiskImportCrossCommUI();
    }
}
