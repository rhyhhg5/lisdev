package com.sinosoft.lis.agentwages;

/**
 * 重算一个机构某一个IndexCalNo的FYC，对该机构的扎帐表的FYC字段
 * 重新打折
 */

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;

import com.sinosoft.lis.agentcalculate.CalFormDrawRate;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LAWageHistoryDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LAWageHistorySchema;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAWageHistorySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


public class AgentWageCalFYCReBL
{

    public CErrors mErrors = new CErrors();
    private TransferData mTransferData = new TransferData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private VData mOutputData = new VData();
    private String mManageCom = new String();
    private String mIndexCalNo = new String();
    private String mBranchType = new String();
    private String mBranchType2 = new String();
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    private LAWageHistorySet mLAWageHistorySet = new LAWageHistorySet();
    public AgentWageCalFYCReBL()
    {
    }

    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!check())
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        if (!prepareOutputData())
        {
            return false;
        }
        AgentWageCalFYCReBLS tAgentWageCalFYCReBLS = new AgentWageCalFYCReBLS();
        if (!tAgentWageCalFYCReBLS.submitData(mOutputData))
        {
            this.mErrors.copyAllErrors(tAgentWageCalFYCReBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据库保存出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;

    }

    private boolean getInputData(VData cInputData)
    {
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mManageCom = (String) mTransferData.getValueByName("ManageCom");
        mIndexCalNo = (String) mTransferData.getValueByName("IndexCalNo");
        mBranchType = (String) mTransferData.getValueByName("BranchType");
        mBranchType2 = (String) mTransferData.getValueByName("BranchType2") ;
        return true;
    }

    private boolean dealData()
    {
        if (!check(mManageCom, mIndexCalNo))
        {
            return false;
        }
        LACommisionSet tLACommisionSet = this.queryLACommision(mManageCom);
        CalFormDrawRate tCalFormDrawRate = new CalFormDrawRate();
        VData temp = new VData();
        temp.clear();
        temp.add(tLACommisionSet);
        temp.add(""); //传入的是一个AgentGrade，提数计算的时候传空，归属的时候传入的是新的职级
        if (!tCalFormDrawRate.submitData(temp, CalFormDrawRate.RECALFYCFLAG))
        {
            this.mErrors.copyAllErrors(tCalFormDrawRate.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoBL";
            tError.functionName = "CalDirectWage";
            tError.errorMessage = "打折处理的时候出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        tLACommisionSet.clear();
        tLACommisionSet.set((LACommisionSet) tCalFormDrawRate.getResult().
                            getObjectByObjectName("LACommisionSet", 0));
        System.out.println("------zhangsj end....");
        mOutputData.add(tLACommisionSet);
        LAWageHistorySchema tLAWageHistorySchema = new LAWageHistorySchema();
        LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();
        tLAWageHistoryDB.setManageCom(mManageCom);
        tLAWageHistoryDB.setWageNo(mIndexCalNo);
        tLAWageHistoryDB.setAClass("03");
        tLAWageHistoryDB.setBranchType(mBranchType);
        if (!tLAWageHistoryDB.getInfo())
        {
            return true;
        }
        tLAWageHistorySchema = tLAWageHistoryDB.getSchema();
        tLAWageHistorySchema.setState("13");
        tLAWageHistorySchema.setOperator(mGlobalInput.Operator);
        tLAWageHistorySchema.setModifyDate(mCurrentDate);
        tLAWageHistorySchema.setModifyTime(mCurrentTime);
        mLAWageHistorySet.add(tLAWageHistorySchema);
        mOutputData.add(mLAWageHistorySet);
        return true;
    }

    private boolean check()
    {
        return true;
    }

    /**
     * 校验，从LAWageHistory表中查询tManageCom、tIndexCalNo的扎帐表信息是否重算过
     * @param tManageCom
     * @param tIndexCalNo
     * @return
     */
    private boolean check(String tManageCom, String tIndexCalNo)
    {
        LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();
        LAWageHistorySet tLAWageHistorySet = new LAWageHistorySet();
        tLAWageHistoryDB.setManageCom(tManageCom);
        tLAWageHistoryDB.setWageNo(tIndexCalNo);
        //xjh Modify 2005/02/26，管理机构有可能传入4位，4位底下有可能有8位管理机构
        //xjh Modify 2005/02/26，此处管理机构只能是一个
        String tSQL = "select * from lawagehistory where managecom='" +
                      tManageCom + "' and wageno='" + tIndexCalNo +
                      "' and BranchType='"+mBranchType+
                      "' and branchtype2='" + mBranchType2 +
                      "'  and aclass='03' ";
        //String tSQL="select * from lawagehistory where managecom like'"+tManageCom+"%' and wageno='"+tIndexCalNo+"' and branchtype='"+mBranchType+"'  and aclass='03' ";
        System.out.println("tSQL:" + tSQL);
        tLAWageHistorySet = tLAWageHistoryDB.executeQuery(tSQL);

        if (tLAWageHistoryDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLAWageHistoryDB.mErrors);
            return false;
        }
        else if (tLAWageHistorySet.size() == 0)
        {
//      CError tError = new CError();
//      tError.moduleName = "AgentWageCalFYCReBL";
//      tError.functionName = "check";
//      tError.errorMessage = "查询"+mManageCom+"的"+mIndexCalNo+"的提数记录失败";
//      this.mErrors.addOneError(tError);
//      return false;
            return true;
        }
        else
        {
            String tState = tLAWageHistorySet.get(1).getState();
            if (tState.equals("13"))
            {
                CError tError = new CError();
                tError.moduleName = "AgentWageCalFYCReBL";
                tError.functionName = "check";
                tError.errorMessage = "" + mManageCom + "的" + mIndexCalNo +
                                      "的FYC已经重算过";
                this.mErrors.addOneError(tError);
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    private boolean prepareOutputData()
    {
        return true;
    }

    private LACommisionSet queryLACommision(String tManageCom)
    {
        //xjh Modify 2005/02/26
        String sqlStr = "select a.* from LACommision a ,latree b where "
                        + "WageNo = '" + mIndexCalNo
                        + "' and a.managecom =  '" + tManageCom +
                        "' and a.commdire='1' " + " And a.BranchType = '" +
                        mBranchType + "' and a.BranchType2='"+mBranchType2
                        +"' a.agentcode=b.agentcode  ";
        sqlStr += " order by a.AgentCode,caldate,polno,ReceiptNo,RiskCode";
        System.out.println("直接佣金明细表 Sql:" + sqlStr);
        LACommisionSet tLACommisionSet = new LACommisionSet();
        LACommisionSchema tLACommisionSchema = new LACommisionSchema();
        LACommisionDB tLACommisionDB = tLACommisionSchema.getDB();
        tLACommisionSet = tLACommisionDB.executeQuery(sqlStr);
        if (tLACommisionDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            System.out.println("Error:" + tLACommisionDB.mErrors.getFirstError());
            this.mErrors.copyAllErrors(tLACommisionDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalFYCReBL";
            tError.functionName = "queryLACommision";
            tError.errorMessage = "直接佣金明细表查询失败!";
            this.mErrors.addOneError(tError);
            tLACommisionSet.clear();
            return null;
        }

        return tLACommisionSet;
    }

    public static void main(String[] args)
    {
        AgentWageCalFYCReBL agentWageCalFYCReBL1 = new AgentWageCalFYCReBL();
        try
        {
            PrintStream out =
                    new PrintStream(
                            new BufferedOutputStream(
                                    new FileOutputStream(
                    "AgentWageCalFYCReBL.out")));
            System.setOut(out);

            GlobalInput tGlobalInput = new GlobalInput();
            tGlobalInput.ManageCom = "86";
            tGlobalInput.Operator = "hahahaha";
            VData tInputData = new VData();
            tInputData.add(tGlobalInput);
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("ManageCom", "86110000");
            tTransferData.setNameAndValue("BranchType", "1");
            tTransferData.setNameAndValue("BranchType2","01") ;
            tTransferData.setNameAndValue("IndexCalNo", "200402");
            tInputData.add(tTransferData);
            if (!agentWageCalFYCReBL1.submitData(tInputData))
            {
                System.out.println(agentWageCalFYCReBL1.mErrors.getFirstError());
            }
            out.close();
        }
        catch (Exception e)
        {
        }

    }
}
