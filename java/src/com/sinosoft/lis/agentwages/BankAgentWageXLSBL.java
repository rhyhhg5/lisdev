package com.sinosoft.lis.agentwages;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Alse
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.FileWriter;
import java.io.*;

public class BankAgentWageXLSBL {
	/** 错误信息容器 */
	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = null;
	private String mSql = null;
	private String mOutXmlPath = null;
	private String mAgentType = null;
	// private String mtype = null;

	private String mCurDate = PubFun.getCurrentDate();
	private String mCurTime = PubFun.getCurrentTime();

	public BankAgentWageXLSBL() {
	}

	public boolean submitData(VData cInputData, String operate) {

		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		return true;
	}

	/**
	 * 校验操作是否合法
	 * 
	 * @return boolean
	 */
	private boolean checkData() {

		return true;
	}

	/**
	 * dealData 处理业务数据
	 * 
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean dealData() {
		ExeSQL tExeSQL = new ExeSQL();
		System.out.println("BL->dealDate()");
		System.out.println(mSql);
		System.out.println(mOutXmlPath);
		SSRS tSSRS = tExeSQL.execSQL(mSql);

		if (tExeSQL.mErrors.needDealError()) {
			System.out.println(tExeSQL.mErrors.getErrContent());

			CError tError = new CError();
			tError.moduleName = "BankAgentWageXLSBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		String[][] mToExcel = new String[tSSRS.getMaxRow() + 2][30];
		if (mAgentType.equals("1")) {
			mToExcel[0][0] = "银代薪资计算结果";
			mToExcel[0][10] = "银代薪资计算结果（营销人员）";
			mToExcel[1][0] = "业务员代码";
			mToExcel[1][1] = "业务员姓名";
			mToExcel[1][2] = "管理机构代码";
			mToExcel[1][3] = "销售机构编码";
			mToExcel[1][4] = "销售机构名称";
			mToExcel[1][5] = "职级";
			mToExcel[1][6] = "职级名称";
			mToExcel[1][7] = "在职状态";
			mToExcel[1][8] = "离职日期";
			mToExcel[1][9] = "基本报酬标准";
			mToExcel[1][10] = "月度业绩达成率";			
			mToExcel[1][11] = "基本报酬";
			mToExcel[1][12] = "基本报酬补发";
			mToExcel[1][13] = "首年绩效提奖";
			mToExcel[1][14] = "续期提奖";
			mToExcel[1][15] = "主管间接佣金";
			mToExcel[1][16] = "基本报酬加款";
			mToExcel[1][17] = "基本报酬扣款";
			mToExcel[1][18] = "绩效提奖加款";
			mToExcel[1][19] = "绩效提奖扣款";
			mToExcel[1][20] = "奖励方案加款";
			mToExcel[1][21] = "奖励方案扣款";
			mToExcel[1][22] = "其它加款";
			mToExcel[1][23] = "其它扣款";
			mToExcel[1][24] = "交叉销售佣金";
		    mToExcel[1][25] = "实发金额";
		    mToExcel[1][26] = "薪资计算操作人员";
	}
		else {
				mToExcel[0][0] = "银代薪资计算结果";
				mToExcel[0][10] = "银代薪资计算结果（直销人员）";
				mToExcel[1][0] = "业务员代码";
				mToExcel[1][1] = "业务员姓名";
				mToExcel[1][2] = "管理机构代码";
				mToExcel[1][3] = "销售机构编码";
				mToExcel[1][4] = "销售机构名称";
				mToExcel[1][5] = "职级";
				mToExcel[1][6] = "职级名称";
				mToExcel[1][7] = "在职状态";
				mToExcel[1][8] = "离职日期";
				mToExcel[1][9] = "基本工资标准";
				mToExcel[1][10] = "月度业绩达成率";					
				mToExcel[1][11] = "基本工资";
				mToExcel[1][12] = "基本工资补发";
				mToExcel[1][13] = "绩效工资";
				mToExcel[1][14] = "绩效工资补发";
				mToExcel[1][15] = "基本工资加款";
				mToExcel[1][16] = "基本工资扣款";
				mToExcel[1][17] = "绩效工资加款";
				mToExcel[1][18] = "绩效工资扣款";
				mToExcel[1][19] = "奖励方案加款";
				mToExcel[1][20] = "奖励方案扣款";
				mToExcel[1][21] = "其它加款";
				mToExcel[1][22] = "其它扣款";
				mToExcel[1][23] = "交叉销售佣金";
				mToExcel[1][24] = "实发金额";
				mToExcel[1][25] = "薪资计算操作人员";
		}

		for (int row = 1; row <= tSSRS.getMaxRow(); row++) {
			for (int col = 1; col <= tSSRS.getMaxCol(); col++) {
				mToExcel[row + 1][col - 1] = tSSRS.GetText(row, col);
			}
		}

		try {
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate() };
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(mOutXmlPath);
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}

		return true;
	}

	/**
	 * getInputData 将外部传入的数据分解到本类的属性中
	 * 
	 * @param cInputData
	 *            VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean getInputData(VData data) {
		mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		TransferData tf = (TransferData) data.getObjectByObjectName(
				"TransferData", 0);

		if (mGI == null || tf == null) {
			CError tError = new CError();
			tError.moduleName = "BankAgentWageXLSBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}

		mSql = (String) tf.getValueByName("querySql");
		mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
		mAgentType = (String) tf.getValueByName("AgentType");
		// mtype = (String) tf.getValueByName("Type");
		if (mSql == null || mOutXmlPath == null || mAgentType == null) {
			CError tError = new CError();
			tError.moduleName = "BankAgentWageXLSBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整2";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}

		return true;
	}

	public static void main(String[] args) {
		BankAgentWageXLSBL BankAgentWageXLSBL = new BankAgentWageXLSBL();
	}
}
