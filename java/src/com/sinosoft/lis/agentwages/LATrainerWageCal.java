/**
 * Copyright (c) 2018 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agentwages;

import java.sql.Connection;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATrainerDB;
import com.sinosoft.lis.db.LAtrainerIndexVsCommDB;
import com.sinosoft.lis.db.LATrainerWageDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LATrainerWageSchema;
import com.sinosoft.lis.vschema.LATrainerSet;
import com.sinosoft.lis.vschema.LAtrainerIndexVsCommSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import java.text.DecimalFormat;

/*
 * <p>Title: 组训营业部指标计算 </p>
 * <p>Description: 指标计算的流程控制类 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft</p>
 * @author 王清民
 * @version 1.0
 * @date 2018-05-22
 */
public class LATrainerWageCal {
	// 全局变量
	public CErrors mErrors = new CErrors(); // 错误处理类
	public GlobalInput mGlobalInput = new GlobalInput(); // 保存传入参数

	// 必须初始化的行政信息
	private String mManageCom; // 管理机构
	private String mYearMonth;
	private String mBranchtype;
	private String mBranchType2;
	// 操作人员
	private String Operator;
	private String FRateFORMATMODOL = "0.00"; // 浮动费率计算出来后的精确位数
	private DecimalFormat mFRDecimalFormat = new DecimalFormat(FRateFORMATMODOL); // 数字转换对象

	// 计算需要的计算信息
	private String WageNo; // 指标年月
	private LAtrainerIndexVsCommSet tLATrainerIndexVsCommSet = new LAtrainerIndexVsCommSet();
	private LAtrainerIndexVsCommDB tLATrainerIndexVsCommDB = new LAtrainerIndexVsCommDB();
	private LATrainerWageSchema mLATrainerWageSchema = new LATrainerWageSchema();
	private String mIColName = "";
	private Connection conn;

	public LATrainerWageCal() {
	}

	// 提交函数,参数入口
	public boolean submitData(VData cInputData, String cOperate) {
		this.getInputData(cInputData); // 初始化指标参数

		if (!Examine()) { // 主函数 
			if (mErrors.needDealError()) {
				System.out.println("计算异常结束原因：" + mErrors.getFirstError());
			}
			return false;
		}

		return true;
	}

	private void getInputData(VData cInputData) {
		// 信息收集
		try {
			TransferData tTransferData = (TransferData) cInputData
					.getObjectByObjectName("TransferData", 0);

			this.mManageCom = (String) tTransferData
					.getValueByName("ManageCom");
			this.mYearMonth = (String) tTransferData.getValueByName("WageNo");
			this.mGlobalInput.setSchema((GlobalInput) tTransferData
					.getValueByName("GlobalInput"));
			this.mBranchtype = (String) tTransferData
					.getValueByName("BranchType");
			this.mBranchType2 = (String) tTransferData
					.getValueByName("BranchType2");
			this.Operator = mGlobalInput.Operator;
			this.WageNo = mYearMonth;
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}

	// 指标计算主函数
	private boolean Examine() {
		// 1.计算组训员工查询

		String tSql = "select distinct(trainerCode) from laTrainer where cManageCom = '"
				+ mManageCom
				+ "' and trainerflag='0' and trainerState='0' "
				+ "  and TakeOfficeDate<=(select enddate from lastatsegment where stattype='5' and yearmonth="
				+ this.mYearMonth
				+ ") "
				+ "  and BranchType = '"
				+ mBranchtype
				+ "' " + "  and BranchType2 ='" + mBranchType2 + "' ";
		// 个险算薪资时 入司日期小于薪资月末日 离职日期为空或者离职日期大于薪资月首日 特殊人群不算薪资
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(tSql);
		System.out.println("共有：" + tSSRS.getMaxRow() + "条数据");

		// 获取要计算的基础指标LATrainerIndexVsComm 中 indextype=‘00’ 薪资计算提取营业部指标
		System.out.println("开始计算组训人员薪资指标了 -------------------------000000^-^");
		tLATrainerIndexVsCommDB.setBranchType(mBranchtype);
		tLATrainerIndexVsCommDB.setBranchType2(mBranchType2);
		tLATrainerIndexVsCommDB.setTrainerGrade("0");
		tLATrainerIndexVsCommSet = tLATrainerIndexVsCommDB.query();
		// 2.对每一个组训人员做计算
		// 循环对每一个组训人员进行薪资计算操作！！！
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			// 通过人员工号查出一个人对应几个营业部
			LATrainerDB tlaTrainerDB = new LATrainerDB();
			LATrainerSet tLaTrainerSet = new LATrainerSet();
			String trainerCode = tSSRS.GetText(i, 1);
			tlaTrainerDB.setTrainerCode(trainerCode);
			tLaTrainerSet = tlaTrainerDB.query();
			String agentGroup = " ";
			String branchAttr = " ";
			LABranchGroupDB tLaBranchGroupDB = new LABranchGroupDB();
			if (tLaTrainerSet.size() == 1) {
				agentGroup = tLaTrainerSet.get(1).getAgentGroup();
				tLaBranchGroupDB.setAgentGroup(agentGroup);
				tLaBranchGroupDB.getInfo();
				branchAttr = tLaBranchGroupDB.getBranchAttr();
			}
			String trainerGrade = tLaTrainerSet.get(1).getTrainerGrade();
			String currentDate = PubFun.getCurrentDate();
			String currentTime = PubFun.getCurrentTime();
			mLATrainerWageSchema = new LATrainerWageSchema();
			mLATrainerWageSchema.setManageCom(mManageCom);
			mLATrainerWageSchema.setWageNo(WageNo);
			mLATrainerWageSchema.setOperator(Operator);
			mLATrainerWageSchema.setTrainerCode(trainerCode);
			mLATrainerWageSchema.setTrainerGrade(trainerGrade);
			mLATrainerWageSchema.setBranchAttr(branchAttr);
			mLATrainerWageSchema.setAgentGroup(agentGroup);
			mLATrainerWageSchema.setMakeDate(currentDate);
			mLATrainerWageSchema.setModifyDate(currentDate);
			mLATrainerWageSchema.setMakeTime(currentTime);
			mLATrainerWageSchema.setModifyTime(currentTime);
			mLATrainerWageSchema.setBranchType(mBranchtype);
			mLATrainerWageSchema.setBranchType2(mBranchType2);
			System.out.println("agentgroup------------------:"+agentGroup);
			// 9.具体计算
			if (!WageCal(trainerCode)) {
				System.out.println("Error:" + this.mErrors.getFirstError());
				return false;
			}
			conn = DBConnPool.getConnection();
			if (this.conn == null) {
				CError tError = new CError();
				tError.moduleName = "LATrainerWageCal";
				tError.functionName = "Examine";
				tError.errorMessage = "获取数据库连结失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
			try {
				conn.setAutoCommit(false);
				// ====将计算出来的结果 存储到LATrainerWage表里
				LATrainerWageDB tQuaLATrainerWageDB = new LATrainerWageDB(conn);
				// ===存储到LATrainerWage表中去 其中indextype= ‘00’
				tQuaLATrainerWageDB.setManageCom(mManageCom);
				tQuaLATrainerWageDB.setWageNo(mLATrainerWageSchema.getWageNo());
				tQuaLATrainerWageDB.setTrainerCode(trainerCode);
				LATrainerWageDB tLATrainerWageDB = new LATrainerWageDB(conn);
				tLATrainerWageDB.setSchema(mLATrainerWageSchema);
				// 如果存在，update，否则insert 这里可以知道回退薪资的时候 不需要删除LATrainerWage 表
				if (!tQuaLATrainerWageDB.getInfo()) {
					if (!tLATrainerWageDB.insert()) {
						this.mErrors.copyAllErrors(tLATrainerWageDB.mErrors);
						CError tError = new CError();
						tError.moduleName = "LATrainerWageCal";
						tError.functionName = "Examine";
						tError.errorMessage = "更新结果记录失败";
						this.mErrors.addOneError(tError);
						conn.rollback();
						conn.close();
						return false;
					}
				} else {
					if (!tLATrainerWageDB.update()) {
						this.mErrors.copyAllErrors(tLATrainerWageDB.mErrors);
						CError tError = new CError();
						tError.moduleName = "LATrainerWageCal";
						tError.functionName = "Examine";
						tError.errorMessage = "更新结果记录失败";
						this.mErrors.addOneError(tError);
						conn.rollback();
						conn.close();
						return false;
					}
				}

				conn.commit();
				conn.close();
			}

			catch (Exception e) {
				e.printStackTrace();
				try {
					this.conn.rollback();
					this.conn.close();
				} catch (Exception ep) {
					ep.printStackTrace();
					CError tError = new CError();
					tError.moduleName = "LATrainerWageCal";
					tError.functionName = "Examine";
					tError.errorMessage = "插入LATrainerWage表的记录失败!";
					this.mErrors.addOneError(tError);
					return false;
				}

			}
			System.out.println("***************组训人员trainerCode:"+trainerCode+"薪资计算完成******************");
		}
		System.out.println("***************组训人员薪资计算完成******************");

		return true;

	}

	// 10.给基数类赋值

	private boolean insertAssess(String cIndexValue) {
		// ====将数值变成double型的
		double tValue = Double.parseDouble(cIndexValue);
		// 大于1千万，java会自动转化为科学技术法，需要作特殊处理
		if (tValue >= 10000000) {

			String tResult = mFRDecimalFormat.format(tValue);
			System.out.println("|||||||||||||||||||||||||||||||||||||||"
					+ tResult);
			mLATrainerWageSchema.setV(mIColName, tResult);
		} else {// 小于1千万，有可能是 int型，不能转化成浮点型数据（其实大于1000万也可能是int型，应该处理）
			mLATrainerWageSchema.setV(mIColName, cIndexValue);
		}
		return true;
	}

	private boolean WageCal(String TranerCode) {
		String tReturn = "";
		Calculator tCal = new Calculator();
		tCal.addBasicFactor("managecom", mManageCom);
		tCal.addBasicFactor("wageno", WageNo);
		tCal.addBasicFactor("trainercode", TranerCode);
		tCal.addBasicFactor("branchtype", mBranchtype);
		tCal.addBasicFactor("branchtype2", mBranchType2);
		for (int i = 1; i <= tLATrainerIndexVsCommSet.size(); i++) {
			// ====核心计算类 （这个需要好好看看）
			tCal.setCalCode(tLATrainerIndexVsCommSet.get(i).getCalCode());
			// ====循环开始计算各项指标信息
			tReturn = tCal.calculate();
			System.out.println("计算结果：tReturn:"+tReturn);
			
			// ===得到薪资项存储字段名
			mIColName = tLATrainerIndexVsCommSet.get(i).getCColName();
			// ====得到薪资项存储项名
			if ((tReturn == null) || (tReturn.trim().equals(""))) {
				tReturn = "0";
			}
			if (!insertAssess(tReturn)) {
				System.out.println("插入组训薪资指标表出错！！");
				return false;
			}
		}
		return true;
	}

	public CErrors getErrors() {

		return mErrors;
	}

	public static void main(String[] args) {
		System.out.println("---------------开始指标计算！-----------------");
		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.ManageCom = "86310000";
		tGlobalInput.Operator = "zy";
		tGlobalInput.ComCode = "86";
		String ManageCom = "86310000";
		String WageYear = "200703";
		VData tVData = new VData();
		tVData.clear();
		String FRateFORMATMODOL = "0.00"; // 浮动费率计算出来后的精确位数
		DecimalFormat mFRDecimalFormat = new DecimalFormat(FRateFORMATMODOL); // 数字转换对象
		double b = 100000000;
		System.out.println(mFRDecimalFormat.format(b));
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("ManageCom", ManageCom);
		tTransferData.setNameAndValue("WageNo", WageYear);
		tTransferData.setNameAndValue("GlobalInput", tGlobalInput);
		tTransferData.setNameAndValue("BranchType", "2");
		tTransferData.setNameAndValue("BranchType2", "01");
		tTransferData.setNameAndValue("LATrainerSet", new LATrainerSet());
		tVData.add(tTransferData);

		LATrainerWageCal tAgentMonthCal = new LATrainerWageCal();
		tAgentMonthCal.submitData(tVData, "");
		if (tAgentMonthCal.mErrors.needDealError()) {
			System.out.println("程序异常结束原因："
					+ tAgentMonthCal.mErrors.getFirstError());
		}
		System.out.println("--------------结束指标计算！---------------");
	}
}
