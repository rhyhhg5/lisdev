package com.sinosoft.lis.agentwages;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author luomin
 * @version 1.1
 */
public class DiskImportBAAgentCommUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private DiskImportBAAgentCommBL mDiskImportBAAgentCommBL = null;

    public DiskImportBAAgentCommUI()
    {
    	mDiskImportBAAgentCommBL = new DiskImportBAAgentCommBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if(!mDiskImportBAAgentCommBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mDiskImportBAAgentCommBL.mErrors);
            return false;
        }
        //
        if(mDiskImportBAAgentCommBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mDiskImportBAAgentCommBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mDiskImportBAAgentCommBL.getImportPersons();
    }

    public int getUnImportRecords()
    {
        return mDiskImportBAAgentCommBL.getUnImportRecords();
    }

    public static void main(String[] args)
    {
    	DiskImportBAAgentCommUI zDiskImportBAAgentCommUI = new DiskImportBAAgentCommUI();
    }
}
