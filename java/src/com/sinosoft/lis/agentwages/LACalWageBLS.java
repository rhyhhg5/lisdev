package com.sinosoft.lis.agentwages;

import java.sql.Connection;

import com.sinosoft.lis.vdb.LAWageDBSet;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class LACalWageBLS
{
    public CErrors mErrors = new CErrors();
    private LAWageSet mUpLAWageSet = new LAWageSet();
    private LAWageSet mInLAWageSet = new LAWageSet();


    public LACalWageBLS()
    {
    }

    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!check())
        {
            return false;
        }
        if (!doSaveLAWage())
        {
            return false;
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        try
        {
            mUpLAWageSet = (LAWageSet) cInputData.getObject(0);
            mInLAWageSet = (LAWageSet) cInputData.getObject(1);
            return true;
        }
        catch (Exception e)
        {
            buildError("getInputData", "获取BL传入的数据时出错");
            return false;
        }

    }

    private boolean check()
    {
        return true;
    }

    private boolean doSaveLAWage()
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LACalWageBLS";
            tError.functionName = "doSaveLAWage";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            LAWageDBSet tUpLAWageDBSet = new LAWageDBSet();
            tUpLAWageDBSet.add(mUpLAWageSet);
            if (!tUpLAWageDBSet.update())
            {
                this.mErrors.copyAllErrors(tUpLAWageDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LACalWageBLS";
                tError.functionName = "doSaveLAWage";
                tError.errorMessage = "数据更新失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            LAWageDBSet tInLAWageDBSet = new LAWageDBSet();
            tInLAWageDBSet.add(mInLAWageSet);
            if (!tInLAWageDBSet.insert())
            {
                this.mErrors.copyAllErrors(tUpLAWageDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LACalWageBLS";
                tError.functionName = "doSaveLAWage";
                tError.errorMessage = "数据插入失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LACalWageBLS";
            tError.functionName = "doSaveLAWage";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return true;

    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LACalWageBLS";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        LACalWageBLS LACalWageBLS1 = new LACalWageBLS();
    }
}
