/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agentwages;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LAWageHistorySet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LAWageLogSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.vschema.LAWageTempSet;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.lis.schema.LAWageTempSchema;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LAWageHistorySchema;
import com.sinosoft.lis.schema.LAWageSchema;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author pengcheng
 * @version 1.0
 */
public class LAActiveWageDaInsuredBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();


    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mManageCom = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String mIndexCalNo = "";
    private LAWageTempSet mLAWageTempSet = new LAWageTempSet();
    private LAWageSet mLAWageSet = new LAWageSet();
    private LAWageHistorySchema mLAWageHistorySchema= new LAWageHistorySchema();
    private MMap mMap = new MMap();
    private VData mInputData= new VData();
    private String mOperate="";
    public LAActiveWageDaInsuredBL()
    {
    }

    public static void main(String[] args)
    {

    }


    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData)
    {

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!check())
        {
            return false;
        }
        // 进行业务处理
        if (!dealData())
        {
            return false;
        }
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LAWageDaInsuredBL Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAWageDaInsuredBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData=null;

        return true;
    }

    private boolean check()
    {
        if (this.mManageCom.length() != 8)
        {
            buildError("check", "机构代码长度必须为8位");
            return false;

        }
        else
        {
            String sql="select EndDate+5 days from LAStatSegment where CHAR(yearmonth)='" + mIndexCalNo +
            "' and stattype='5' ";
            ExeSQL tExeSQL = new ExeSQL();
            String tEndDate = tExeSQL.getOneValue(sql);
            System.out.println("not error2");
            String tSQL1 = "select * from lawagelog where managecom like '" +
                          mManageCom + "%' and (char(int(wageno)-1)='" + mIndexCalNo +
                          "' or (char(int(wageno)-89)='" + mIndexCalNo +
                          "'  and  char(int(wageyear)-1)='" +mIndexCalNo.substring(0,4)+
                          "'  and substr(wageno,5,6)='01'))  and branchtype='" +
                      mBranchType + "' and BranchType2='"+mBranchType2+
                      "'    ";
             System.out.println("not error1");
            LAWageLogSet tLAWageLogSet = new LAWageLogSet();
            LAWageLogDB tLAWageLogDB = new LAWageLogDB();
            tLAWageLogSet = tLAWageLogDB.executeQuery(tSQL1);
            System.out.println("not error3"+tSQL1);

            if (tLAWageLogDB.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tLAWageLogDB.mErrors);
                return false;
             }
             else if(tLAWageLogSet.size()<=0)
             {
                 buildError("dealData", "本月没有进行提数计算未完成"+tEndDate+"还未提数，请提数后进行佣金试算，再确认佣金！");
                  return false;
             }
             else
             {
                 System.out.println(tSQL1+tLAWageLogSet.get(1).getEndDate());
                 String mEndDate= tLAWageLogSet.get(1).getEndDate();
                 if (mEndDate.compareTo(tEndDate)<0)
                 {
                     buildError("dealData", "本月没有进行提数计算未完成"+tEndDate+"还未提数，请提数后进行佣金试算，再确认佣金！");
                     return false;
                 }

            }
            return true;
        }
    }

    private boolean dealData()
    {
        String tSQL="select *  from  lawagetemp  where managecom like '"+mManageCom+"%' and indexcalno='"
                    +mIndexCalNo+"' and branchtype='"+mBranchType+"' and branchtype2='"+mBranchType2+"'";
        LAWageTempDB tLAWageTempDB= new LAWageTempDB();
        mLAWageTempSet=tLAWageTempDB.executeQuery(tSQL);

        if(mLAWageTempSet.size()>=1)
        {
            String tGetDate = mLAWageTempSet.get(1).getGetDate();
//            if(!currentDate.equals(tGetDate) )
//            {
//                buildError("dealData","佣金试算日期与佣金确认日期必需在同一天!");
//                return false;
//            }
            for(int i=1;i<=mLAWageTempSet.size();i++)
            {
                Reflections tReflections = new Reflections();
                LAWageSchema tLAWageSchema = new LAWageSchema();
                tReflections.transFields(tLAWageSchema, mLAWageTempSet.get(i));
                tLAWageSchema.setMakeDate(currentDate);
                tLAWageSchema.setMakeTime(currentTime);
                tLAWageSchema.setModifyDate(currentDate);
                tLAWageSchema.setModifyTime(currentTime);
                mLAWageSet.add(tLAWageSchema);
            }
            String tSql="select * from LAWageHistory where managecom like '"+mManageCom+"%' and wageno='"
                    +mIndexCalNo+"' and branchtype='"+mBranchType+"' and branchtype2='"+mBranchType2
                    +"' and aclass='03' ";
            LAWageHistoryDB tLAWageHistoryDB= new LAWageHistoryDB();
            LAWageHistorySet tLAWageHistorySet= new LAWageHistorySet();
            tLAWageHistorySet=tLAWageHistoryDB.executeQuery(tSql);
            if(tLAWageHistorySet.size() <= 0)
            {
                buildError("dealData","查询佣金计算历史表出错");
                return false;
            }
            mLAWageHistorySchema=tLAWageHistorySet.get(1);
            if(mLAWageHistorySchema.getState().equals("13"))
            {
                buildError("dealData","该月佣金已确认，不能再确认!");
                return false;
            }
            if(mLAWageHistorySchema.getState().equals("12"))
            {
                buildError("dealData","该月佣金试算未完成，不能再确认!");
                return false;
            }
            mLAWageHistorySchema.setState("13");
            mLAWageHistorySchema.setModifyDate(currentDate);
            mLAWageHistorySchema.setModifyTime(currentTime);
        }
        else
        {
            buildError("dealData","请先进行佣金试算!");
            return false;
        }


        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mManageCom = (String) cInputData.get(0);
        mBranchType = (String) cInputData.get(1);
        mBranchType2 =(String)cInputData.get(2) ;
        mIndexCalNo = (String) cInputData.get(3);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 4));
        return true;
    }
    // 个险佣金开始试算，插入state = '12'，表示正在试算，其他人不能再算
    private boolean prepareOutputData()
    {

        mMap.put(this.mLAWageSet, "INSERT");
        mMap.put(this.mLAWageHistorySchema, "UPDATE");
        mInputData.add(mMap);
        return true;

    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAWageDaInsuredBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

}
