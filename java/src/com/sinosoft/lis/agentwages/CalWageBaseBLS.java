package com.sinosoft.lis.agentwages;

import java.sql.Connection;

import com.sinosoft.lis.vdb.LAWageDBSet;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LAWageActivityLogSet;
import com.sinosoft.lis.vdb.LAWageActivityLogDBSet;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.vdb.LAWageTempDBSet;
import com.sinosoft.lis.vschema.LAWageTempSet;
import com.sinosoft.lis.schema.LAWageTempSchema;

public class CalWageBaseBLS
{
    public CErrors mErrors = new CErrors();
    private String mBranchType="";
    private String mBranchType2="";
    private LAWageSet mUpLAWageSet = new LAWageSet();
    private LAWageSet mInLAWageSet = new LAWageSet();
    private LAWageTempSet mUpLAWageTempSet = new LAWageTempSet();
    private LAWageTempSet mInLAWageTempSet = new LAWageTempSet();
    private LAWageActivityLogSet mLAWageActivityLogSet = new LAWageActivityLogSet();

    public CalWageBaseBLS()
    {
    }

    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!check())
        {
            return false;
        }
        if(("1".equals(mBranchType) && "01".equals(mBranchType2))||("2".equals(mBranchType))||
        		("7".equals(mBranchType) && "01".equals(mBranchType2))||("5".equals(mBranchType) && "01".equals(mBranchType2)))
        {
            if (!doSaveLAWageTemp()) {
                return false;
            }
        }
        else
        {
            if (!doSaveLAWage()) {
                return false;
            }
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        try
        {
            mUpLAWageSet = (LAWageSet) cInputData.getObject(0);
            mInLAWageSet = (LAWageSet) cInputData.getObject(1);

            if(mUpLAWageSet.size()>=1)
            {
                for(int i=1;i<=mUpLAWageSet.size();i++)
                {
                    Reflections tReflections = new Reflections();
                    LAWageTempSchema tLAWageTempSchema = new LAWageTempSchema();
                    tReflections.transFields(tLAWageTempSchema, mUpLAWageSet.get(i));
                    mUpLAWageTempSet.add(tLAWageTempSchema);

                }
                mBranchType=mUpLAWageSet.get(1).getBranchType();
                mBranchType2=mUpLAWageSet.get(1).getBranchType2();
            }
            if(mInLAWageSet.size()>=1)
            {
                for(int i=1;i<=mInLAWageSet.size();i++)
                {
                    Reflections tReflections = new Reflections();
                    LAWageTempSchema tLAWageTempSchema = new LAWageTempSchema();
                    tReflections.transFields(tLAWageTempSchema, mInLAWageSet.get(i));
                    mInLAWageTempSet.add(tLAWageTempSchema);
                }
                mBranchType=mInLAWageSet.get(1).getBranchType();
                mBranchType2=mInLAWageSet.get(1).getBranchType2();
            }
            mLAWageActivityLogSet=(LAWageActivityLogSet) cInputData.getObject(2);

            return true;
        }
        catch (Exception e)
        {
            buildError("getInputData", "获取BL传入的数据时出错");
            return false;
        }

    }

    private boolean check()
    {
        return true;
    }

    private boolean doSaveLAWageTemp()
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalWageBaseBLS";
            tError.functionName = "doSaveLAWage";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            LAWageTempDBSet tLAWageTempDBSet = new LAWageTempDBSet(conn);
            tLAWageTempDBSet.add(mUpLAWageTempSet);

            if (mUpLAWageTempSet.size() > 0 && !tLAWageTempDBSet.update())
            {
                this.mErrors.copyAllErrors(tLAWageTempDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalWageBaseBLS";
                tError.functionName = "doSaveLAWage";
                tError.errorMessage = "数据更新失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            LAWageTempDBSet tInLAWageTempDBSet = new LAWageTempDBSet(conn);
            tInLAWageTempDBSet.add(mInLAWageTempSet);
            if (mInLAWageTempSet.size() > 0 && !tInLAWageTempDBSet.insert())
            {
                this.mErrors.copyAllErrors(tInLAWageTempDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalWageBaseBLS";
                tError.functionName = "doSaveLAWage";
                tError.errorMessage = "数据插入失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LAWageActivityLogDBSet tLAWageActivityLogDBSet = new LAWageActivityLogDBSet(conn);
            tLAWageActivityLogDBSet.add(mLAWageActivityLogSet);
            if (mLAWageActivityLogSet.size() > 0 && !tLAWageActivityLogDBSet.insert())
            {
                this.mErrors.copyAllErrors(tLAWageActivityLogDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalWageBaseBLS";
                tError.functionName = "doSaveLAWage";
                tError.errorMessage = "数据插入失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalWageBaseBLS";
            tError.functionName = "doSaveLAWage";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return true;

    }
    private boolean doSaveLAWage()
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalWageBaseBLS";
            tError.functionName = "doSaveLAWage";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            LAWageDBSet tLAWageDBSet = new LAWageDBSet(conn);
            tLAWageDBSet.add(mUpLAWageSet);

            if (mUpLAWageSet.size() > 0 && !tLAWageDBSet.update())
            {
                this.mErrors.copyAllErrors(tLAWageDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalWageBaseBLS";
                tError.functionName = "doSaveLAWage";
                tError.errorMessage = "数据更新失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            LAWageDBSet tInLAWageDBSet = new LAWageDBSet(conn);
            tInLAWageDBSet.add(mInLAWageSet);
            if (mInLAWageSet.size() > 0 && !tInLAWageDBSet.insert())
            {
                this.mErrors.copyAllErrors(tInLAWageDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalWageBaseBLS";
                tError.functionName = "doSaveLAWage";
                tError.errorMessage = "数据插入失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LAWageActivityLogDBSet tLAWageActivityLogDBSet = new LAWageActivityLogDBSet(conn);
            tLAWageActivityLogDBSet.add(mLAWageActivityLogSet);
            if (mLAWageActivityLogSet.size() > 0 && !tLAWageActivityLogDBSet.insert())
            {
                this.mErrors.copyAllErrors(tLAWageActivityLogDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalWageBaseBLS";
                tError.functionName = "doSaveLAWage";
                tError.errorMessage = "数据插入失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CalWageBaseBLS";
            tError.functionName = "doSaveLAWage";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return true;

    }
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CalWageBaseBLS";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        CalWageBaseBLS CalWageBaseBLS1 = new CalWageBaseBLS();
    }
}
