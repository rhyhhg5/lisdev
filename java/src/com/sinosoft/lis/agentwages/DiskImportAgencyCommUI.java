package com.sinosoft.lis.agentwages;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author luomin
 * @version 1.1
 */
public class DiskImportAgencyCommUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private DiskImportAgencyCommBL mDiskImportAgencyCommBL = null;

    public DiskImportAgencyCommUI()
    {
    	mDiskImportAgencyCommBL = new DiskImportAgencyCommBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if(!mDiskImportAgencyCommBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mDiskImportAgencyCommBL.mErrors);
            return false;
        }
        //
        if(mDiskImportAgencyCommBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mDiskImportAgencyCommBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mDiskImportAgencyCommBL.getImportPersons();
    }

    public int getUnImportRecords()
    {
        return mDiskImportAgencyCommBL.getUnImportRecords();
    }

    public static void main(String[] args)
    {
    	DiskImportAgencyCommUI zDiskImportAgencyCommUI = new DiskImportAgencyCommUI();
    }
}
