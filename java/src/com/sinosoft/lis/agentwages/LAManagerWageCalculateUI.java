package com.sinosoft.lis.agentwages;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author pengcheng
 * @version 1.0
 */
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAManagerWageCalculateUI {

	public CErrors mErrors = new CErrors();
	private VData mInputData = new VData();

	public LAManagerWageCalculateUI() {
	}

	public boolean submitData(VData cInputData) {
		mInputData = (VData) cInputData.clone();
		LAManagerWageCalculateBL tLAManagerWageCalculateBL = new LAManagerWageCalculateBL();
		if (!tLAManagerWageCalculateBL.submitData(mInputData)) {
			this.mErrors.copyAllErrors(tLAManagerWageCalculateBL.mErrors);
			CError tError = new CError();
			tError.moduleName = "LAManagerWageCalculateUI";
			tError.functionName = "submitData";
			tError.errorMessage = tLAManagerWageCalculateBL.mErrors
					.getFirstError();
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
}
