/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agentwages;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LAWageHistorySet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LAWageLogSet;
import com.sinosoft.utility.ExeSQL;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: </p>
 * @author xin
 * @version 1.0
 */
public class BankAgentWageCalBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();


    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mManageCom = "";
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String mIndexCalNo = "";

    public BankAgentWageCalBL()
    {
    }

    public static void main(String[] args)
    {
        try
        {

            System.out.println("---------------");
            GlobalInput tGlobalInput = new GlobalInput();
            tGlobalInput.ManageCom = "86";
            tGlobalInput.Operator = "aa";

            VData tVData = new VData();
            BankAgentWageCalBL BankAgentWageCalBL1 = new BankAgentWageCalBL();
            tVData.add("8611");
            tVData.add("2");
            tVData.add("01") ;
            tVData.add("200605");
            tVData.add(tGlobalInput);
            BankAgentWageCalBL1.submitData(tVData);
            if (BankAgentWageCalBL1.mErrors.needDealError())
            {
                for (int i = 0; i < BankAgentWageCalBL1.mErrors.getErrorCount();
                             i++)
                {
                    System.out.println(BankAgentWageCalBL1.mErrors.getError(i).
                                       moduleName);
                    System.out.println(BankAgentWageCalBL1.mErrors.getError(i).
                                       functionName);
                    System.out.println(BankAgentWageCalBL1.mErrors.getError(i).
                                       errorMessage);
                }
            }
            System.out.println("---------------");
            System.out.println("over");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData)
    {

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!check())
        {
            return false;
        }
        // 进行业务处理
        if (!dealData())
        {
            if (mBranchType.equals("1") && mBranchType2.equals("01"))
            {

                if (!returnState())
                {
                    return false;
                }
            }
            else if (mBranchType.equals("3") && mBranchType2.equals("01"))
            {

                if (!returnState())
                {
                    return false;
                }
            }
            return false;
        }

        // 准备传往后台的数据
        VData vData = new VData();
        return true;
    }

    //判断录入的代码长度
    private boolean check()
    {System.out.println("mBranchType-----"+mBranchType);
        if(mBranchType.equals("1")||mBranchType.equals("3"))//个险按支公司进行计算
        {
        	if (this.mManageCom.length() != 8) {
                buildError("check", "机构代码长度必须为8位");
                return false;

            }
        }
        else //其他的按照分公司进行
        {
    	if (this.mManageCom.length() != 4) {
            buildError("check", "机构代码长度必须为4位");
            return false;

        }
    	}
        if (mBranchType.equals("1") && mBranchType2.equals("01")) {

            //是否正在计算
            if (!checkState()) {
                return false;
            }
            //插入状态，其他人在计算完成前不能计算
            if (!insertState()) {
                return false;
            }

        }
        return true;
    }






    private boolean dealData()
    {
        //设置参数名和值
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("ManageCom", mManageCom);
        tTransferData.setNameAndValue("BranchType", mBranchType);
        tTransferData.setNameAndValue("BranchType2",mBranchType2) ;
        tTransferData.setNameAndValue("IndexCalNo", mIndexCalNo);
        //暂时没用的
        AgentWageLoopCalFYC tAgentWageLoopCalFYC = new AgentWageLoopCalFYC();
        BankRepeatFYC tBankRepeatFYC = new BankRepeatFYC();
        //
        VData tInputData = new VData();
        tInputData.add(mGlobalInput);
        tInputData.add(tTransferData);
        if (mBranchType.equals("1"))
        {

            String tSQL = "select * from lawagehistory where managecom like '" +
                          mManageCom + "%' and wageno='" + mIndexCalNo +
                          "' and branchtype='" +
                          mBranchType + "' and BranchType2='"+mBranchType2+"' and aclass='03' ";
            System.out.println("tSQL:" + tSQL);
            LAWageHistorySet tLAWageHistorySet = new LAWageHistorySet();
            LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();
            tLAWageHistorySet = tLAWageHistoryDB.executeQuery(tSQL);

            if (tLAWageHistoryDB.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tLAWageHistoryDB.mErrors);
                return false;
            }
            else if (tLAWageHistorySet.size() == 0)
            {
                buildError("dealData", "本月没有进行提数计算");
                return false;
            }
            else if (tLAWageHistorySet.size() > 0 &&
                     tLAWageHistorySet.get(1).getState().compareTo("13") < 0)
            {
//            else if (tLAWageHistorySet.size() >= 0 &&
//                     tLAWageHistorySet.get(1).getState().compareTo("13") < 0){
                //caigang 2004-07-12 处理25日以前扫描,28日以前承保,下月5日回单问题
//                AgentWageCalSetWageNoReBL tAgentWageCalSetWageNoReBL = new
//                        AgentWageCalSetWageNoReBL();
//                tInputData.clear();
//                tInputData.add(mManageCom);
//                tInputData.add(mIndexCalNo);
//                tInputData.add(mBranchType);
//                tInputData.add(mGlobalInput);
//                if (!tAgentWageCalSetWageNoReBL.submitData(tInputData, ""))
//                {
//                    System.out.println("error has occur");
//                    this.mErrors.copyAllErrors(tAgentWageCalSetWageNoReBL.
//                                               mErrors);
//                    return false;
//                }
                //判断本月提数是否完成，下月也要提取，可以去掉犹豫期 10天的保单  20051208  XIANGCHUN
//                String sql="select EndDate+10 days from LAStatSegment where CHAR(yearmonth)='" + mIndexCalNo +
//                "' and stattype='5' ";
//                ExeSQL tExeSQL = new ExeSQL();
//                String tEndDate = tExeSQL.getOneValue(sql);
//                System.out.println("not error2");
//                String tSQL1 = "select * from lawagelog where managecom like '" +
//                              mManageCom + "%' and (char(int(wageno)-1)='" + mIndexCalNo +
//                              "' or (char(int(wageno)-89)='" + mIndexCalNo +
//                              "'  and  char(int(wageyear)-1)='" +mIndexCalNo.substring(0,4)+
//                              "'  and substr(wageno,5,6)='01'))  and branchtype='" +
//                          mBranchType + "' and BranchType2='"+mBranchType2+
//                          "'    ";
//                 System.out.println("not error1");
//                LAWageLogSet tLAWageLogSet = new LAWageLogSet();
//                LAWageLogDB tLAWageLogDB = new LAWageLogDB();
//                tLAWageLogSet = tLAWageLogDB.executeQuery(tSQL1);
//                System.out.println("not error3"+tSQL1);
//
//                if (tLAWageLogDB.mErrors.needDealError())
//                {
//                    this.mErrors.copyAllErrors(tLAWageHistoryDB.mErrors);
//                    return false;
//                 }
//                 else if(tLAWageLogSet.size()<=0)
//                 {
//                     buildError("dealData", "本月没有进行提数计算未完成"+tEndDate+"还未提数！");
//                      return false;
//                 }
//                 else
//                 {
//                     System.out.println(tSQL1+tLAWageLogSet.get(1).getEndDate());
//                     String mEndDate= tLAWageLogSet.get(1).getEndDate();
//                     if (mEndDate.compareTo(tEndDate)<0)
//                     {
//                         buildError("dealData", "本月没有进行提数计算未完成"+tEndDate+"还未提数！");
//                         return false;
//                     }
//                 }

                System.out.println("not error");
                String tState = tLAWageHistorySet.get(1).getState();
                System.out.println(tState);

                //修改：2004-06-08 LL
                //增加了 tState != 14 状态的判断
                if (!tState.equals("13") && !tState.equals("14"))
                {
                    tTransferData = new TransferData();
                    tTransferData.setNameAndValue("ManageCom", mManageCom);
                    tTransferData.setNameAndValue("BranchType", mBranchType);
                    tTransferData.setNameAndValue("BranchType2",mBranchType2) ;
                    tTransferData.setNameAndValue("IndexCalNo", mIndexCalNo);
                    tInputData.clear();
                    tInputData.add(tTransferData);
                    tInputData.add(mGlobalInput);
                    //计算前删除原来的计算结果
                    if(!deleteWage())
                    {
                        return false;
                     }
//                    if (!tAgentWageLoopCalFYC.submitData(tInputData))
//                    {
//                        this.mErrors.copyAllErrors(tAgentWageLoopCalFYC.mErrors);
//                        return false;
//                    }
                }

            }
        }
       else if (mBranchType.equals("2") && mBranchType2.equals("01") )
        {
            String tSQL = "select * from lawagehistory where managecom like '" +
                          mManageCom + "%' and wageno='" + mIndexCalNo +
                          "' and branchtype='" +
                          mBranchType + "' and BranchType2='"+mBranchType2+"' and aclass='03' ";
            System.out.println("tSQL:" + tSQL);
            LAWageHistorySet tLAWageHistorySet = new LAWageHistorySet();
            LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();
            tLAWageHistorySet = tLAWageHistoryDB.executeQuery(tSQL);

            if (tLAWageHistoryDB.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tLAWageHistoryDB.mErrors);
                return false;
            }
            else if (tLAWageHistorySet.size() == 0)
            {
                buildError("dealData", "本月没有进行提数计算");
                return false;
            }
            else if (tLAWageHistorySet.size() > 0 &&
                     tLAWageHistorySet.get(1).getState().compareTo("13") < 0)
            {
                //判断本月提数是否完成，下月也要提取，可以去掉犹豫期 10天的保单  20051208  XIANGCHUN
                String sql="select EndDate+15 days from LAStatSegment where CHAR(yearmonth)='" + mIndexCalNo +
                "' and stattype='91' ";
                ExeSQL tExeSQL = new ExeSQL();
                String tEndDate = tExeSQL.getOneValue(sql);
                System.out.println("not error2");
                String tSQL1 = "select * from lawagelog where managecom like '" +
                              mManageCom + "%' and (char(int(wageno)-1)='" + mIndexCalNo +
                              "' or (char(int(wageno)-89)='" + mIndexCalNo +
                              "'  and  char(int(wageyear)-1)='" +mIndexCalNo.substring(0,4)+
                              "'  and substr(wageno,5,6)='01'))  and branchtype='" +
                          mBranchType + "' and BranchType2='"+mBranchType2+
                          "'    ";
                 System.out.println("not error1");
                LAWageLogSet tLAWageLogSet = new LAWageLogSet();
                LAWageLogDB tLAWageLogDB = new LAWageLogDB();
                tLAWageLogSet = tLAWageLogDB.executeQuery(tSQL1);
                System.out.println("not error3"+tSQL1);

                if (tLAWageLogDB.mErrors.needDealError())
                {
                    this.mErrors.copyAllErrors(tLAWageHistoryDB.mErrors);
                    return false;
                 }
//把每月的十五日教验去掉，是为了测试，上线后要改回来
//                 else if(tLAWageLogSet.size()<=0)
//                 {
//                     buildError("dealData", "本月提数计算未完成,"+tEndDate+"还未提数！");
//                      return false;
//                 }
////  为了测试，注释掉，以后需要补上
//                 else
//                 {
//                     System.out.println(tSQL1+tLAWageLogSet.get(1).getEndDate());
//                     String mEndDate= tLAWageLogSet.get(1).getEndDate();
//                     if (mEndDate.compareTo(tEndDate)<0)
//                     {
//                         buildError("dealData", "本月提数计算未完成,"+tEndDate+"还未提数！");
//                         return false;
//                     }
//                 }
                System.out.println("not error");
                String tState = tLAWageHistorySet.get(1).getState();
                System.out.println(tState);

                //修改：2004-06-08 LL
                //增加了 tState != 14 状态的判断
                if (!tState.equals("13") && !tState.equals("14"))
                {
                    tTransferData = new TransferData();
                    tTransferData.setNameAndValue("ManageCom", mManageCom);
                    tTransferData.setNameAndValue("BranchType", mBranchType);
                    tTransferData.setNameAndValue("BranchType2",mBranchType2) ;
                    tTransferData.setNameAndValue("IndexCalNo", mIndexCalNo);
                    tInputData.clear();
                    tInputData.add(tTransferData);
                    tInputData.add(mGlobalInput);

//                    if (!tAgentWageLoopCalFYC.submitData(tInputData))
//                    {
//                        this.mErrors.copyAllErrors(tAgentWageLoopCalFYC.mErrors);
//                        return false;
//                    }
                }
            }
        }
        //新加入  by Abigale
        else if (mBranchType.equals("3")&& mBranchType2.equals("01") )
        {
            System.out.println("银代计算：");
//            if (!tBankRepeatFYC.submitData(tInputData))
//            {
//                this.mErrors.copyAllErrors(tAgentWageLoopCalFYC.mErrors);
//                return false;
//            }

           String tSQL = "select * from lawagehistory where managecom like '" +
                          mManageCom + "%' and wageno='" + mIndexCalNo +
                          "' and branchtype='" +
                          mBranchType + "' and BranchType2='"+mBranchType2+"' and aclass='03' ";
            System.out.println("查询提数记录的语句:" + tSQL);
            LAWageHistorySet tLAWageHistorySet = new LAWageHistorySet();
            LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();
            tLAWageHistorySet = tLAWageHistoryDB.executeQuery(tSQL);

            if (tLAWageHistoryDB.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tLAWageHistoryDB.mErrors);
                return false;
            }
            else if (tLAWageHistorySet.size() == 0)
            {
                buildError("dealData", "本月没有进行提数计算");
                return false;
            }
            else if (tLAWageHistorySet.size() > 0 &&
                     tLAWageHistorySet.get(1).getState().compareTo("13") < 0)
            {
                //判断本月提数是否完成，下月也要提取，回执回销 20天的保单
                //20080303 by Abigale
                String sql="select EndDate+15 days from LAStatSegment where CHAR(yearmonth)='" + mIndexCalNo +
                "' and stattype='91' ";
                ExeSQL tExeSQL = new ExeSQL();
                String tEndDate = tExeSQL.getOneValue(sql);
                System.out.println("not error1");
                String tSQL1 = "select * from lawagelog where managecom like '" +
                              mManageCom + "%' and (char(int(wageno)-1)='" + mIndexCalNo +
                              "' or (char(int(wageno)-89)='" + mIndexCalNo +
                              "'  and  char(int(wageyear)-1)='" +mIndexCalNo.substring(0,4)+
                              "'  and substr(wageno,5,6)='01'))  and branchtype='" +
                          mBranchType + "' and BranchType2='"+mBranchType2+
                          "'    ";
                System.out.println("判断本月提数是否完成的SQL："+tSQL1);
                LAWageLogSet tLAWageLogSet = new LAWageLogSet();
                LAWageLogDB tLAWageLogDB = new LAWageLogDB();
                tLAWageLogSet = tLAWageLogDB.executeQuery(tSQL1);
                System.out.println("not error3");

//                if (tLAWageLogDB.mErrors.needDealError())
//                {
//                    this.mErrors.copyAllErrors(tLAWageHistoryDB.mErrors);
//                    return false;
//                 }
//                //把每月的十五日教验去掉，是为了测试，上线后要改回来
//                 else if(tLAWageLogSet.size()<=0)
//                 {
//                     buildError("dealData", "本月提数计算未完成,"+tEndDate+"还未提数！");
//                      return false;
//                 }
//                 //  为了测试，注释掉，以后需要补上
//                 else
//                 {
//                     System.out.println(tSQL1+tLAWageLogSet.get(1).getEndDate());
//                     String mEndDate= tLAWageLogSet.get(1).getEndDate();
//                     if (mEndDate.compareTo(tEndDate)<0)
//                     {
//                         buildError("dealData", "本月提数计算未完成,"+tEndDate+"还未提数！");
//                         return false;
//                     }
//                 }
                System.out.println("not error");
                String tState = tLAWageHistorySet.get(1).getState();
                System.out.println(tState);

                //修改：2004-06-08 LL
                //增加了 tState != 14 状态的判断
                if (!tState.equals("13") && !tState.equals("14"))
                {
                    tTransferData = new TransferData();
                    tTransferData.setNameAndValue("ManageCom", mManageCom);
                    tTransferData.setNameAndValue("BranchType", mBranchType);
                    tTransferData.setNameAndValue("BranchType2",mBranchType2) ;
                    tTransferData.setNameAndValue("IndexCalNo", mIndexCalNo);
                    tInputData.clear();
                    tInputData.add(tTransferData);
                    tInputData.add(mGlobalInput);
                }
            }
        }

        if (mBranchType.equals("1"))
        {
            //2004-08-25 蔡刚添加用于佣金计算前校验
            PubCheckField checkField1 = new PubCheckField();
            VData cInputData = new VData();
//设置计算时要用到的参数值
            tTransferData = new TransferData();
            tTransferData.setNameAndValue("IndexCalNo", mIndexCalNo);
            tTransferData.setNameAndValue("BranchType", mBranchType);
            tTransferData.setNameAndValue("BranchType2",mBranchType2) ;
            tTransferData.setNameAndValue("ManageCom", mManageCom);
            LMCheckFieldDB tLMCheckFieldDB = new LMCheckFieldDB();
            LMCheckFieldSet tLMCheckFieldSet = new LMCheckFieldSet();
            String tSql =
                    "select * from lmcheckfield where riskcode = '000000'"
                    +
                    "  and fieldname = 'BankAgentWageCalBL' order by serialno asc";
            tLMCheckFieldSet = tLMCheckFieldDB.executeQuery(tSql);
            cInputData.add(tTransferData);
            System.out.println(tSql);
            cInputData.add(tLMCheckFieldSet);

            if (!checkField1.submitData(cInputData, "CKBYSET"))
            {
                System.out.println("Enter Error Field!");
                //此判断是用来区分是程序处理过程中报的错误，还是校验时报的错误
                if (checkField1.mErrors.needDealError())
                {

                    this.mErrors.copyAllErrors(checkField1.mErrors);
                    return false;
                }
                else
                {
                    VData t = checkField1.getResultMess();
                    buildError("dealData", t.get(0).toString());
                    return false;
                }
            }
            else
            {
                System.out.println("Congratulation!");
            }
            System.out.println("佣金计算前校验完毕!");
        }

        tInputData.clear();

        tTransferData = new TransferData();
        tTransferData.setNameAndValue("ManageCom", mManageCom);
        tTransferData.setNameAndValue("IndexCalNo", mIndexCalNo);
        tTransferData.setNameAndValue("GlobalInput", mGlobalInput);
        tTransferData.setNameAndValue("BranchType", mBranchType);
        tTransferData.setNameAndValue("BranchType2",mBranchType2) ;
        tTransferData.setNameAndValue("LAAgentSet", new LAAgentSet());
        tInputData.add(tTransferData);
        //调用计算基础指标的函数
        AgentMonthCal tAgentMonthCal = new AgentMonthCal();
//    String countSQL = "select count(*) from laindexinfo where managecom like '" +
//        mManageCom + "%' and indexcalno='" + mIndexCalNo +
//        "' and indextype='00' and agentcode in (select agentcode from laagent where managecom like '" +
//        mManageCom + "%'  and branchtype='" + mBranchType + "' )";
//    System.out.println(countSQL);
//    ExeSQL tExeSQL = new ExeSQL();
//    int count = Integer.parseInt(tExeSQL.getOneValue(countSQL));
//    if (count == 0)

        if (!tAgentMonthCal.submitData(tInputData, ""))//佣金计算前的基础指标
        {
            this.mErrors.copyAllErrors(tAgentMonthCal.mErrors);
            return false;
        }
        tInputData.clear();
        tInputData.add(mManageCom);
        tInputData.add(mBranchType);
        tInputData.add(mBranchType2) ;
        tInputData.add(mIndexCalNo);
        tInputData.add(mGlobalInput);
        LAWageCalUI tLAWageCalUI = new LAWageCalUI();
        if (!tLAWageCalUI.submitData(tInputData))
        {
            this.mErrors.copyAllErrors(tLAWageCalUI.mErrors);
            return false;
        }
        //计算完成，回复原来的计算
        if (mBranchType.equals("1") && mBranchType2.equals("01"))
        {
            if(!returnState())
            {
                return false;
            }
        }
        else if (mBranchType.equals("3") && mBranchType2.equals("01"))
        {
            if(!returnState())
            {
                return false;
            }
        }
        return true;
    }


   /**
    * 从输入数据中得到所有对象
    * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    */
   private boolean getInputData(VData cInputData)
   {
       //全局变量
       mManageCom = (String) cInputData.get(0);
       mBranchType = (String) cInputData.get(1);
       mBranchType2 =(String)cInputData.get(2) ;
       mIndexCalNo = (String) cInputData.get(3);
       mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
               "GlobalInput", 4));
       System.out.println("mManageCom:"+mManageCom+" mIndexCalNo:"+mIndexCalNo);
       ExeSQL tExeSQL = new ExeSQL();
     // tExeSQL.execUpdateSQL("refresh table LABANKMISION");
      System.out.println("mManageCom");

       return true;

   }



    // 个险佣金开始试算，插入state = '12'，表示正在试算，其他人不能再算
    private boolean insertState()
    {
        String tSQL1 = "update LAWageHistory set state = '12' where "
                       + " wageno = '" + mIndexCalNo +
                       "' and ManageCom like '" + mManageCom
                       + "%' and BranchType = '" + mBranchType
                       + "' and BranchType2 ='" + mBranchType2
                       + "' and AClass = '03'";
        System.out.println("最后更新LAWageHistory表中state记录的SQL: " + tSQL1);
        ExeSQL tExeSQL = new ExeSQL();
        boolean tValue = tExeSQL.execUpdateSQL(tSQL1);
        if (tExeSQL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "BankAgentWageCalBL";
            tError.functionName = "insertState";
            tError.errorMessage = "执行SQL语句,更新LAWageHistory表记录失败!";
            this.mErrors.addOneError(tError);
            System.out.println("执行SQL语句,更新LAWageHistory表表中state记录失败!");
            return false;
        }
        return true;
    }
    // 个险佣金试算结束，回到原来的状态，插入state = '11'，表示可以计算
    private boolean returnState()
    {
        String tSQL1 = "update LAWageHistory set state = '11' where "
                       + " wageno = '" + mIndexCalNo +
                       "' and ManageCom like '" + mManageCom
                       + "%' and BranchType = '" + mBranchType
                       + "' and BranchType2 ='" + mBranchType2
                       + "' and AClass = '03'";
        System.out.println("最后更新LAWageHistory表中state记录的SQL: " + tSQL1);
        ExeSQL tExeSQL = new ExeSQL();
        boolean tValue = tExeSQL.execUpdateSQL(tSQL1);
        if (tExeSQL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "BankAgentWageCalBL";
            tError.functionName = "returnState";
            tError.errorMessage = "执行SQL语句,更新LAWageHistory表记录失败!";
            this.mErrors.addOneError(tError);
            System.out.println("执行SQL语句,更新LAWageHistory表表中state记录失败!");
            return false;
        }
        return true;
    }
    //检查是否正在计算，不能同时多用户计算
    private boolean checkState()
    {
        String tSQL = "select * from lawagehistory where managecom like '" +
                      mManageCom + "%' and wageno='" + mIndexCalNo +
                      "' and branchtype='" +
                      mBranchType + "' and BranchType2='" + mBranchType2 +
                      "' and aclass='03' ";
        LAWageHistorySet tLAWageHistorySet = new LAWageHistorySet();
        LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();
        tLAWageHistorySet = tLAWageHistoryDB.executeQuery(tSQL);

        if (tLAWageHistoryDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLAWageHistoryDB.mErrors);
            return false;
        }
        else if (tLAWageHistorySet.size() == 0)
        {
            buildError("checkState", "本月没有进行提数计算");
            return false;
        }
        else if (tLAWageHistorySet.size() > 0 &&
                 tLAWageHistorySet.get(1).getState().compareTo("14") == 0)
        {
            buildError("checkState", "本月佣金已经确认，不能再计算");
            return false;
        }
        else if (tLAWageHistorySet.size() > 0 &&
                 tLAWageHistorySet.get(1).getState().compareTo("13") < 0)
        {
            String tState=tLAWageHistorySet.get(1).getState();
            if(tState.equals("12"))
            {
                buildError("checkState", "本月佣金正在进行计算，不能计算");
                return false;
            }
        }
      return true;
    }
    //删除原来的佣金计算的数据
    private boolean deleteWage()
    {
        String tSql="delete  from  laindexinfo  where  managecom like '" +
                       mManageCom + "%' and indexcalno='" + mIndexCalNo +
                       "' and branchtype='" +
                       mBranchType + "' and BranchType2='" + mBranchType2 +
                       "' and indextype<='01' ";
         ExeSQL tExeSQL = new ExeSQL();
         boolean tValue = tExeSQL.execUpdateSQL(tSql);
         if (tExeSQL.mErrors.needDealError()) {
              // @@错误处理
              this.mErrors.copyAllErrors(tExeSQL.mErrors);
              CError tError = new CError();
              tError.moduleName = "BankAgentWageCalBL";
              tError.functionName = "returnState";
              tError.errorMessage = "删除原来的佣金数据失败!";
              this.mErrors.addOneError(tError);
              System.out.println("删除原来的佣金数据失败laindexinfo!");
              return false;
          }
          tSql="delete  from  lawagetemp  where  managecom like '" +
                         mManageCom + "%' and indexcalno='" + mIndexCalNo +
                         "' and branchtype='" +
                         mBranchType + "' and BranchType2='" + mBranchType2 + "' ";
           tExeSQL = new ExeSQL();
           tValue = tExeSQL.execUpdateSQL(tSql);
           if (tExeSQL.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "BankAgentWageCalBL";
                tError.functionName = "deleteWage";
                tError.errorMessage = "删除原来的佣金数据失败!";
                this.mErrors.addOneError(tError);
                System.out.println("删除原来的佣金lawage数据失败!");
                return false;
          }
       return true;
    }
    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "BankAgentWageCalBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

}
