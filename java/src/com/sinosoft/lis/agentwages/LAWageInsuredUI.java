package com.sinosoft.lis.agentwages;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author pengcheng
 * @version 1.0
 */
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class LAWageInsuredUI
{

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();


    public LAWageInsuredUI()
    {
    }

    public boolean submitData(VData cInputData)
    {
        mInputData = (VData) cInputData.clone();
        LAWageInsuredBL tLAWageInsuredBL = new LAWageInsuredBL();
        if (!tLAWageInsuredBL.submitData(mInputData))
        {
            this.mErrors.copyAllErrors(tLAWageInsuredBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "tLAWageInsuredUI";
            tError.functionName = "submitData";
            tError.errorMessage = tLAWageInsuredBL.mErrors.getFirstError();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
