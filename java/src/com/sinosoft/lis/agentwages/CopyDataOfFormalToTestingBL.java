package com.sinosoft.lis.agentwages;


import java.lang.reflect.Constructor;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.FormalRSWrapper;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: CopyDataOfFormalToTestingBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author xin
 * @version 1.0
 */
public class CopyDataOfFormalToTestingBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";

  /**提交数据时封装set*/
  public MMap mMMap = new MMap();
	
  /**提交数据时，封装MMAP*/
  public VData mVData = new VData();
  
  /**导的数据量*/
  public int mCount = 0;
  
  private TransferData mTransferData = new TransferData();
  
  //要导的数据where 条件
  private String mWhereSql = "";
  
  //要导的表
  private String mTableName  = "";

  //查询sql
  private String mQuerySql = "";
  
  public CopyDataOfFormalToTestingBL() {

  }

  public static void main(String[] args) {
	  CopyDataOfFormalToTestingBL tCopyDataOfFormalToTestingBL = new CopyDataOfFormalToTestingBL();
	  TransferData tTransferData = new TransferData();
	  tTransferData.setNameAndValue("WhereSql", "commisionsn ='0167234870' ");
	  tTransferData.setNameAndValue("TableName", "LACommision");
	  // 准备传输数据 VData
	  VData tVData = new VData();
	  tVData.add(tTransferData);
	  tCopyDataOfFormalToTestingBL.submitData(tVData, "");
}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin CopyDataOfFormalToTestingBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
    	mInputData = cInputData;
    	mTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
    	mWhereSql = (String)mTransferData.getValueByName("WhereSql");
    	mTableName = (String)mTransferData.getValueByName("TableName");
    	mQuerySql = "select * from "+ mTableName +" where 1=1 and "+ mWhereSql;
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "CopyDataOfFormalToTestingBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin CopyDataOfFormalToTestingBL.dealData........."+mOperate);
    try {
    	String tClassSetName = "com.sinosoft.lis.vschema."+mTableName+"Set";
    	Class tClassSet = Class.forName(tClassSetName);
    	SchemaSet tObjectSet = (SchemaSet) tClassSet.newInstance();
//    	SchemaSet tInsertObjectSet = (SchemaSet) tSetConstructor.newInstance(null);
    	FormalRSWrapper tFormalRSWrapper = new FormalRSWrapper();
    	tFormalRSWrapper.prepareData(tObjectSet, mQuerySql);
    	do
    	{
    		tFormalRSWrapper.getData();
//    		String tClassSchemaName = "com.sinosoft.lis.schema."+mTableName+"schema";
//        	Class tClassSchema = Class.forName(tClassSchemaName);
//        	Constructor tConstructorSchema = tClassSchema.getConstructor(null );  
//        	for(int i=1;i<=tObjectSet.size();i++)
//        	{
//        		Schema tObjectSchema = (Schema) tConstructorSchema.newInstance(null);
//        		tObjectSchema = (Schema) tObjectSet.getObj(i);
//        		tInsertObjectSet.add(tObjectSchema);
//        	}
    		System.out.println(tObjectSet.size());
    		mMMap.put(tObjectSet,"INSERT");
			mVData.add(mMMap);
			PubSubmit tPubSubmit = new PubSubmit();
	        try{
	        if (!tPubSubmit.submitData(mVData, "")) {
	        	// @@错误处理
	            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
	            CError tError = new CError();
	            tError.moduleName = "AgentWageOfLJAPayPersonData";
	            tError.functionName = "PubSubmit";
	            tError.errorMessage = "数据插入到lacommision中失败败!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
	        }catch(Exception ex)
	        {
	        	ex.printStackTrace();
	        }
	        mCount+=tObjectSet.size();
	        mMMap = new MMap();
	        mVData.clear();
    	}
    	while(tObjectSet.size()>0);
      }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "CopyDataOfFormalToTestingBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
//  select A.TABNAME, B.COLNAME 
//  from syscat.tabconst A   ,SYSCAT.KEYCOLUSE B 
//  WHERE A.CONSTNAME = B.CONSTNAME AND   A.TYPE='P'
//  and A.tabname like 'TW_CORP_PRDCT_MEMBER_200803%'   with ur;
}
