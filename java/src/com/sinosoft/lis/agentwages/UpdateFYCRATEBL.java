package com.sinosoft.lis.agentwages;

/*
 * <p>ClassName: UpdateWageNoBL </p>
 * <p>Description: LAAssessBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2016-11-24
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class UpdateFYCRATEBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    /** 数据操作字符串 */
    private String mOperate;

    public UpdateFYCRATEBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UpdateFYCRATEBL";
            tError.functionName = "DealData";
            tError.errorMessage = "数据处理失败UpdateFYCRATEBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
        	PubSubmit tPubSubmit = new PubSubmit();
        	tPubSubmit.submitData(this.mInputData, "UPDATE");
			
		} catch (Exception e) {
			CError tError = new CError();
            tError.moduleName = "UpdateFYCRATEBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败UpdateFYCRATEBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
		}
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	MMap tMMap= new MMap();
        //置基础数据
        for (int i = 1; i <= this.mLACommisionSet.size(); i++)
        {
        	String tCommisionSN = mLACommisionSet.get(i).getCommisionSN();
        	String triskcode = mLACommisionSet.get(i).getRiskCode();
        	String tbranchtype=mLACommisionSet.get(i).getBranchType();
        	String tbranchtype2=mLACommisionSet.get(i).getBranchType2();
        	System.out.println("222:"+triskcode);
        	System.out.println("33:"+tbranchtype);
        	System.out.println("44:"+tbranchtype2);
        	String sql="select calcode from lawagecalelement where branchtype='"+tbranchtype+"' and branchtype2='"
        	+tbranchtype2+"' and riskcode='"+triskcode+"' and caltype='00'";
        	ExeSQL tExeSQL = new ExeSQL();
        	SSRS mSSRS = new SSRS();
            System.out.println("1111111111111111111");
            mSSRS = tExeSQL.execSQL(sql);    	 
         	 if (tExeSQL.mErrors.needDealError())
        	 {
         	     CError tError = new CError();
       	     tError.moduleName = "UpdateFYCRATEBL";
        	     tError.functionName = "dealData";
      	         tError.errorMessage = "查询提奖代码的信息出错";
       	     this.mErrors.addOneError(tError);
        	     return false;
         	 } 
         	 String mCalcode = mSSRS.GetText(1, 1);
        	String tUpSQL ="";
            if(tbranchtype.equals("1") && tbranchtype2.equals("01")&&mCalcode.equals("AP0052")){
            	
        		tUpSQL = "update lacommision set fycrate=db2inst1.COMMISIONRATE(char(lacommision.riskcode),char(lacommision.GrpContNo),char(lacommision.managecom),char(lacommision.agentcode),"
                        +"char(lacommision.payyears),char(lacommision.payyear),char(lacommision.BRANCHTYPE),char(lacommision.BRANCHTYPE2),char(lacommision.SIGNDATE),"
                        +"char(lacommision.PAYINTV),char(lacommision.RENEWCOUNT),char(lacommision.SCANDATE),char(lacommision.TMAKEDATE),char(lacommision.CONTNO),"
                        +"char(lacommision.FLAG),char(lacommision.PAYCOUNT),char(lacommision.TRANSSTATE),char(lacommision.F3)),"
        				+ "operator ='fycrate',modifydate = current date,modifytime = current time "
        				+ " where commisionsn ='"+tCommisionSN+"'";
        		tMMap.put(tUpSQL, "UPDATE");
        		tUpSQL="update lacommision set standfycrate=fycrate,fyc=ROUND(transmoney * fycrate,2),directwage=ROUND(transmoney * fycrate,2),"
              		+"modifydate=current date,modifytime=current time,operator ='fycrate' "
              		+" where commisionsn='"+tCommisionSN+"'";
        		tMMap.put(tUpSQL, "UPDATE");
        	}
        	else if(tbranchtype.equals("5") && tbranchtype2.equals("01")&&mCalcode.equals("AA0052"))
        	{
        		tUpSQL = "update lacommision set fycrate=db2inst1.COMMISIONACTIVERATE(char(lacommision.riskcode),char(lacommision.managecom),char(lacommision.GrpContNo),char(lacommision.agentcode),"
                        +"char(lacommision.CONTNO),char(lacommision.payyears),char(lacommision.payyear),char(lacommision.BRANCHTYPE),char(lacommision.BRANCHTYPE2),char(lacommision.PAYINTV),char(lacommision.RENEWCOUNT),"
                        +"char(lacommision.PAYCOUNT),char(lacommision.TRANSSTATE),char(lacommision.F3),char(lacommision.BRANCHTYPE3)),"
    				    + "operator ='fycrate',modifydate = current date,modifytime = current time "
    				    + " where commisionsn ='"+tCommisionSN+"'";
        		tMMap.put(tUpSQL, "UPDATE");
        		tUpSQL="update lacommision set standfycrate=fycrate,fyc=ROUND(transmoney * fycrate,2),directwage=ROUND(transmoney * fycrate,2),"
              		+"modifydate=current date,modifytime=current time,operator ='fycrate' "
              		+" where commisionsn='"+tCommisionSN+"'";
        		tMMap.put(tUpSQL, "UPDATE");
        	}
        	else if(tbranchtype.equals("2") && tbranchtype2.equals("01")&&mCalcode.equals("AP0052"))
        	{
        		tUpSQL = "update lacommision set fycrate=db2inst1.COMMISIONRATE(char(lacommision.riskcode),char(lacommision.GrpContNo),char(lacommision.managecom),char(lacommision.agentcode),"
                    +"char(lacommision.payyears),char(lacommision.payyear),char(lacommision.BRANCHTYPE),char(lacommision.BRANCHTYPE2),char(lacommision.SIGNDATE),"
                    +"char(lacommision.PAYINTV),char(lacommision.RENEWCOUNT),char(lacommision.SCANDATE),char(lacommision.TMAKEDATE),char(lacommision.CONTNO),"
                    +"char(lacommision.FLAG),char(lacommision.PAYCOUNT),char(lacommision.TRANSSTATE),char(lacommision.F3)),"
    				+ "operator ='fycrate',modifydate = current date,modifytime = current time "
    				+ " where commisionsn ='"+tCommisionSN+"'";
    		tMMap.put(tUpSQL, "UPDATE");
    		tUpSQL="update lacommision set standfycrate=fycrate,fyc=ROUND(transmoney * fycrate,2),directwage=ROUND(transmoney * fycrate,2),"
          		+"modifydate=current date,modifytime=current time,operator ='fycrate' "
          		+" where commisionsn='"+tCommisionSN+"'";
    		tMMap.put(tUpSQL, "UPDATE");	
          }
          else if(tbranchtype.equals("3") && tbranchtype2.equals("01")&&mCalcode.equals("AP3001"))
          {
        	  tUpSQL = "update lacommision set fycrate=db2inst1.GetBankCommiRate(char(lacommision.BRANCHTYPE),char(lacommision.BRANCHTYPE2),"
        			 +"char(lacommision.riskcode),char(lacommision.AgentCom),char(lacommision.contno),char(lacommision.grpcontno),char(lacommision.managecom),char(lacommision.payyears),"
        			 +"char(lacommision.payyear),'11',char(lacommision.Renewcount),char(lacommision.TRANSSTATE),char(lacommision.PAYINTV),"
        			 +"char(lacommision.AGENTCODE)),"
        			 + "operator ='fycrate',modifydate = current date,modifytime = current time "
     				 + " where commisionsn ='"+tCommisionSN+"'";
     		tMMap.put(tUpSQL, "UPDATE");
     		tUpSQL="update lacommision set standfycrate=fycrate,fyc=ROUND(transmoney * fycrate,2),directwage=ROUND(transmoney * fycrate,2),"
           		+"modifydate=current date,modifytime=current time,operator ='fycrate' "
           		+" where commisionsn='"+tCommisionSN+"'";
     		tMMap.put(tUpSQL, "UPDATE");	 
        	} 
         else if(tbranchtype.equals("3") && tbranchtype2.equals("01")&&mCalcode.equals("AP3011"))
         {
        	 tUpSQL = "update lacommision set fycrate=db2inst1.GetBankCommiRatePY(char(lacommision.BRANCHTYPE),char(lacommision.BRANCHTYPE2),"
        			+"char(lacommision.riskcode),char(lacommision.AgentCom),char(lacommision.contno),char(lacommision.grpcontno),char(lacommision.managecom),"
        			+"char(lacommision.payyears),char(lacommision.payyear),'11',char(lacommision.Renewcount),char(lacommision.TRANSSTATE),"
        			+"char(lacommision.PAYINTV),char(lacommision.AGENTCODE)),"
        			+ "operator ='fycrate',modifydate = current date,modifytime = current time "
    				+ " where commisionsn ='"+tCommisionSN+"'";
    		tMMap.put(tUpSQL, "UPDATE");
    		tUpSQL="update lacommision set standfycrate=fycrate,fyc=ROUND(transmoney * fycrate,2),directwage=ROUND(transmoney * fycrate,2),"
          		+"modifydate=current date,modifytime=current time,operator ='fycrate' "
          		+" where commisionsn='"+tCommisionSN+"'";
    		tMMap.put(tUpSQL, "UPDATE");	 
         }
         else if(tbranchtype.equals("6") && tbranchtype2.equals("01")&&mCalcode.equals("ASR052"))
         {
        	 tUpSQL = "update lacommision set fycrate=db2inst1.SOCIALRATE(char(lacommision.riskcode),char(lacommision.GrpContNo),"
        			 +"char(lacommision.managecom),char(lacommision.agentcode),char(lacommision.payyears),char(lacommision.payyear),"
        			 +"char(lacommision.BRANCHTYPE),char(lacommision.BRANCHTYPE2),char(lacommision.SIGNDATE),char(lacommision.PAYINTV),"
        			 +"char(lacommision.RENEWCOUNT),char(lacommision.SCANDATE),char(lacommision.TMAKEDATE),char(lacommision.CONTNO),"
        			 +"char(lacommision.FLAG),char(lacommision.PAYCOUNT),char(lacommision.TRANSSTATE),char(lacommision.F3)),"
        			 + "operator ='fycrate',modifydate = current date,modifytime = current time "
     				 + " where commisionsn ='"+tCommisionSN+"'";
     		tMMap.put(tUpSQL, "UPDATE");
     		tUpSQL="update lacommision set standfycrate=fycrate,fyc=ROUND(transmoney * fycrate,2),directwage=ROUND(transmoney * fycrate,2),"
           		+"modifydate=current date,modifytime=current time,operator ='fycrate' "
           		+" where commisionsn='"+tCommisionSN+"'";
     		tMMap.put(tUpSQL, "UPDATE");	 
         }
         else if(tbranchtype.equals("2") && tbranchtype2.equals("01")&&mCalcode.equals("AP0056"))
         {
        	 tUpSQL = "update lacommision set fycrate=db2inst1.GetGroupCommiRate(char(lacommision.BRANCHTYPE),char(lacommision.BRANCHTYPE2),char(lacommision.riskcode),char(lacommision.managecom),"
                 +"char(lacommision.COMMISIONSN)),operator ='fycrate',modifydate = current date,modifytime = current time "
 				 + " where commisionsn ='"+tCommisionSN+"'";
 		     tMMap.put(tUpSQL, "UPDATE");
 		     tUpSQL="update lacommision set standfycrate=fycrate,fyc=ROUND(transmoney * fycrate,2),directwage=ROUND(transmoney * fycrate,2),"
       		     +"modifydate=current date,modifytime=current time,operator ='fycrate' "
       		     +" where commisionsn='"+tCommisionSN+"'";
 		      tMMap.put(tUpSQL, "UPDATE");	 
         }
         else if(tbranchtype.equals("2") && tbranchtype2.equals("01")&&mCalcode.equals("AP0160"))
         {
        	 tUpSQL = "update lacommision set fycrate=db2inst1.FYCRATE(char(lacommision.riskcode),char(lacommision.managecom),char(lacommision.PAYINTV),"
                 +"char(lacommision.payyears),char(lacommision.payyear),char(lacommision.BRANCHTYPE),char(lacommision.BRANCHTYPE2),"
                 +" '31',char(lacommision.RENEWCOUNT),char(lacommision.CONTNO),char(lacommision.GRPCONTNO),char(lacommision.F3),"
                 +"char(lacommision.AGENTCODE)) ,operator ='fycrate',modifydate = current date,modifytime = current time "
 				 + " where commisionsn ='"+tCommisionSN+"'";
 		     tMMap.put(tUpSQL, "UPDATE");
 		     tUpSQL="update lacommision set standfycrate=fycrate,fyc=ROUND(transmoney * fycrate,2),directwage=ROUND(transmoney * fycrate,2),"
       		     +"modifydate=current date,modifytime=current time,operator ='fycrate' "
       		     +" where commisionsn='"+tCommisionSN+"'";
 		      tMMap.put(tUpSQL, "UPDATE");	 
         }
         else if(tbranchtype.equals("2") && tbranchtype2.equals("01")&&mCalcode.equals("AP3011"))
         {
        	 tUpSQL = "update lacommision set fycrate=db2inst1.GetBankCommiRatePY(char(lacommision.BRANCHTYPE),char(lacommision.BRANCHTYPE2),"
     			+"char(lacommision.riskcode),char(lacommision.AgentCom),char(lacommision.contno),char(lacommision.grpcontno),char(lacommision.managecom),"
     			+"char(lacommision.payyears),char(lacommision.payyear),'11',char(lacommision.Renewcount),char(lacommision.TRANSSTATE),"
     			+"char(lacommision.PAYINTV),char(lacommision.AGENTCODE)),"
     			+ "operator ='fycrate',modifydate = current date,modifytime = current time "
 				+ " where commisionsn ='"+tCommisionSN+"'";
 		      tMMap.put(tUpSQL, "UPDATE");
 		     tUpSQL="update lacommision set standfycrate=fycrate,fyc=ROUND(transmoney * fycrate,2),directwage=ROUND(transmoney * fycrate,2),"
       		    +"modifydate=current date,modifytime=current time,operator ='fycrate' "
       		    +" where commisionsn='"+tCommisionSN+"'";
 		       tMMap.put(tUpSQL, "UPDATE");	 
          }
         else if(tbranchtype.equals("3") && tbranchtype2.equals("01")&&mCalcode.equals("AP0165"))
         {
        	 tUpSQL = "update lacommision set fycrate=db2inst1.GRPTAXRATE(lacommision.riskcode,lacommision.GrpContNo,lacommision.managecom,lacommision.agentcode,"
                    +"lacommision.payyears,lacommision.payyear,lacommision.BRANCHTYPE,lacommision.BRANCHTYPE2,lacommision.SIGNDATE,"
                    +"lacommision.PAYINTV,lacommision.RENEWCOUNT,lacommision.SCANDATE,lacommision.TMAKEDATE,lacommision.CONTNO,"
                    +"lacommision.FLAG,lacommision.PAYCOUNT,lacommision.TRANSSTATE,lacommision.F3),"
        			+ "operator ='fycrate',modifydate = current date,modifytime = current time "
    				+ " where commisionsn ='"+tCommisionSN+"'";
    		tMMap.put(tUpSQL, "UPDATE");
    		tUpSQL="update lacommision set standfycrate=fycrate,fyc=ROUND(transmoney * fycrate,2),directwage=ROUND(transmoney * fycrate,2),"
          		+"modifydate=current date,modifytime=current time,operator ='fycrate' "
          		+" where commisionsn='"+tCommisionSN+"'";
    		tMMap.put(tUpSQL, "UPDATE");	 
         }
         else if(tbranchtype.equals("2") && tbranchtype2.equals("01")&&mCalcode.equals("AP0165"))
         {
        	 tUpSQL = "update lacommision set fycrate=db2inst1.GRPTAXRATE(lacommision.riskcode,lacommision.GrpContNo,lacommision.managecom,lacommision.agentcode,"
                    +"lacommision.payyears,lacommision.payyear,lacommision.BRANCHTYPE,lacommision.BRANCHTYPE2,lacommision.SIGNDATE,"
                    +"lacommision.PAYINTV,lacommision.RENEWCOUNT,lacommision.SCANDATE,lacommision.TMAKEDATE,lacommision.CONTNO,"
                    +"lacommision.FLAG,lacommision.PAYCOUNT,lacommision.TRANSSTATE,lacommision.F3),"
        			+ "operator ='fycrate',modifydate = current date,modifytime = current time "
    				+ " where commisionsn ='"+tCommisionSN+"'";
    		tMMap.put(tUpSQL, "UPDATE");
    		tUpSQL="update lacommision set standfycrate=fycrate,fyc=ROUND(transmoney * fycrate,2),directwage=ROUND(transmoney * fycrate,2),"
          		+"modifydate=current date,modifytime=current time,operator ='fycrate' "
          		+" where commisionsn='"+tCommisionSN+"'";
    		tMMap.put(tUpSQL, "UPDATE");	 
         }
         else if(tbranchtype.equals("7")&& tbranchtype2.equals("01")&&mCalcode.equals("AH0052"))
         {
        	 tUpSQL = "update lacommision set fycrate=db2inst1.HEALTHRATE(lacommision.riskcode,lacommision.managecom,lacommision.payyears,lacommision.payyear,"
                    +"lacommision.BRANCHTYPE,lacommision.BRANCHTYPE2,lacommision.PAYINTV,lacommision.RENEWCOUNT,lacommision.F3),"
                    + "operator ='fycrate',modifydate = current date,modifytime = current time "
    				+ " where commisionsn ='"+tCommisionSN+"'";
    		tMMap.put(tUpSQL, "UPDATE");
    		tUpSQL="update lacommision set standfycrate=fycrate,fyc=ROUND(transmoney * fycrate,2),directwage=ROUND(transmoney * fycrate,2),"
          		+"modifydate=current date,modifytime=current time,operator ='fycrate' "
          		+" where commisionsn='"+tCommisionSN+"'";
    		tMMap.put(tUpSQL, "UPDATE");	 
         }
         else if(tbranchtype.equals("1")&&tbranchtype2.equals("01")&&mCalcode.equals("AP0222")){
        	 tUpSQL = "update lacommision set fycrate=db2inst1.COMMISIONKLRSRATE(lacommision.riskcode,lacommision.GrpContNo,lacommision.managecom,lacommision.agentcode,lacommision.payyears,"
        		 +"lacommision.payyear,lacommision.BRANCHTYPE,lacommision.BRANCHTYPE2,lacommision.SIGNDATE,lacommision.PAYINTV,lacommision.RENEWCOUNT,"
        		 +"lacommision.SCANDATE,lacommision.TMAKEDATE,lacommision.CONTNO,lacommision.FLAG,lacommision.PAYCOUNT,lacommision.TRANSSTATE,"
                 + "lacommision.F3),operator ='fycrate',modifydate = current date,modifytime = current time "
 				+ " where commisionsn ='"+tCommisionSN+"'";
 		tMMap.put(tUpSQL, "UPDATE");
 		tUpSQL="update lacommision set standfycrate=fycrate,fyc=ROUND(transmoney * fycrate,2),directwage=ROUND(transmoney * fycrate,2),"
       		+"modifydate=current date,modifytime=current time,operator ='fycrate' "
       		+" where commisionsn='"+tCommisionSN+"'";
 		tMMap.put(tUpSQL, "UPDATE");	 
         }
      }
        this.mInputData.add(tMMap);
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mLACommisionSet.set((LACommisionSet) cInputData.
                                 getObjectByObjectName("LACommisionSet", 0));
        System.out.println("apple:"+this.mLACommisionSet.size());
        return true;
    }
}
