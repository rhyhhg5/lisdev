/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agentwages;

import java.sql.Connection;

import com.sinosoft.lis.agentcalculate.AgCalBase;
import com.sinosoft.lis.agentcalculate.CalIndex;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAssessIndexDB;
import com.sinosoft.lis.db.LAIndexInfoDB;
import com.sinosoft.lis.db.LAIndexInfoTempDB;
import com.sinosoft.lis.db.LAStatSegmentDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAIndexInfoSchema;
import com.sinosoft.lis.schema.LAIndexInfoTempSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LAAssessIndexSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAWageActivityLogSchema;
import com.sinosoft.lis.vschema.LAWageActivityLogSet;
import com.sinosoft.lis.vdb.LAWageActivityLogDBSet;
import java.text.DecimalFormat;
import java.math.BigDecimal;
import java.math.BigInteger;

/*
 * <p>Title: 个人每月指标计算流程类 </p>
 * <p>Description: 指标计算的流程控制类 </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 * @date 2004-03-09
 */
public class AgentMonthCal
{
    //全局变量
    public CErrors mErrors = new CErrors(); //错误处理类
    public GlobalInput mGlobalInput = new GlobalInput(); //保存传入参数


    //必须初始化的行政信息
    private String mManageCom; //管理机构
    private String mYearMonth;
    private String mBranchtype;
    private String mBranchType2;
    private String mAgentGrade;
    private LAWageActivityLogSet mLAWageActivityLogSet = new LAWageActivityLogSet();
    //操作人员
    private String Operator;
    private String mAgentCode;
    private String mBranchCode;
    private String FRateFORMATMODOL = "0.00"; //浮动费率计算出来后的精确位数
    private DecimalFormat mFRDecimalFormat = new DecimalFormat(FRateFORMATMODOL); //数字转换对象


    //计算需要的计算信息
    private String IndexCalNo; //指标年月
    private LAAssessIndexDB mLAAssessIndexDB; //存指标信息
    private LAAssessIndexSet tLAAssessIndexSet = new LAAssessIndexSet();
    private LAAssessIndexDB tLAAssessIndexDB = new LAAssessIndexDB();
    private LAIndexInfoSchema mLAIndexInfoSchema = new LAIndexInfoSchema();
    private String mTableName = "";
    private String mIColName = "";
    private boolean hasJudgeIfExist = false;
    private boolean Exist = false;
    private CalIndex tCalIndex = new CalIndex(); //指标计算类
    private String MonthBegin = "";
    private String MonthEnd = "";
    private String mAreaType = "";
    private String mWageVersion = "";
    private String OutWorkBeginDate = "";
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private boolean reCalFlag = false;
    private Connection conn;
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String mAssessFlag = "";
    private String mType = "";
    private String mGbuildFlag = "";
    private String mGbuildStartDate = "";
    public AgentMonthCal()
    {}


    //提交函数,参数入口
    public boolean submitData(VData cInputData, String cOperate)
    {
        this.getInputData(cInputData); //初始化指标参数

        if (!Examine())
        { //主函数
            if (mErrors.needDealError())
            {
                System.out.println("计算异常结束原因：" + mErrors.getFirstError());
            }
            return false;
        }

        return true;
    }

    private void getInputData(VData cInputData)
    {
        //信息收集
        try
        {
            TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData",0);

            this.mManageCom = (String) tTransferData.getValueByName("ManageCom");
            this.mYearMonth = (String) tTransferData.getValueByName("IndexCalNo");
            this.mGlobalInput.setSchema((GlobalInput) tTransferData.getValueByName("GlobalInput"));
            this.mBranchtype = (String) tTransferData.getValueByName("BranchType");
            this.mBranchType2 = (String) tTransferData.getValueByName("BranchType2") ;
            this.mLAAgentSet = (LAAgentSet) tTransferData.getValueByName("LAAgentSet");
            this.Operator = mGlobalInput.Operator;
            this.IndexCalNo = mYearMonth;
            if(mBranchtype.equals("1") && mBranchType2.equals("01"))
            {
                mType="01";//====个险
            }
            else  if(mBranchtype.equals("2") && mBranchType2.equals("01"))
            {
                mType="02";//=====团险直销
            }
            else if(mBranchtype.equals("2") && mBranchType2.equals("02"))
             {
                 mType="03";//============团险中介
             }
            //modify by Abigale  150-153   还要修改AgentPubFun中的getAreaType函数
             else if(mBranchtype.equals("3") && mBranchType2.equals("01"))
             {//mType="04";
                 mType="03";//==========银代
             }
             else if(mBranchtype.equals("7") && mBranchType2.equals("01"))
             {//mType="04";
                 mType="01";//==========健康管理渠道
             }
            mAreaType = AgentPubFun.getAreaType(mManageCom.substring(0, 4),mType);
            System.out.println("--------    getInputData    --------");
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
    }


    //指标计算主函数
    private boolean Examine()
    {
        String tSql="";
        if( mBranchtype.equals("1"))
        {
            tSql = "select startdate from lastatsegment where yearmonth=" +
                   mYearMonth + " and stattype='5' ";
        }
        else if( mBranchtype.equals("2"))
        {
            tSql = "select startdate from lastatsegment where yearmonth=" +
                   mYearMonth + " and stattype='91' ";
        }
        //modify by Abigale   2008-03-04   173-179
        else if( mBranchtype.equals("3"))
        {
            tSql = "select startdate from lastatsegment where yearmonth=" +
                   mYearMonth + " and stattype='91' ";
        }
        //modify by yangyang   2014-12-04   
        else if( mBranchtype.equals("5"))
        {
            tSql = "select startdate from lastatsegment where yearmonth=" +
                   mYearMonth + " and stattype='5' ";
        }else if( mBranchtype.equals("7"))
        {
            tSql = "select startdate from lastatsegment where yearmonth=" +
                   mYearMonth + " and stattype='5' ";
        }
        ExeSQL tExeSQL = new ExeSQL();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        OutWorkBeginDate = tExeSQL.getOneValue(tSql);
        OutWorkBeginDate = AgentPubFun.formatDate(OutWorkBeginDate,
                                                  "yyyy-MM-dd");
        if (this.mLAAgentSet == null || this.mLAAgentSet.size() == 0)
        {
//1.计算代理人查询
            if( mBranchtype.equals("1"))
            {

                    tSql = "select * from LAAgent where ManageCom like '" +
                           mManageCom + "%' "
                           + " and BranchType = '" + mBranchtype + "' "
                           + " and BranchType2 ='" + mBranchType2 + "' "
                           + " and EmployDate <= (select enddate from lastatsegment where stattype='5' and yearmonth= " +
                           IndexCalNo + ")"
                           + " and (outworkdate is null or outworkdate>='" +
                           OutWorkBeginDate + "') " +
                           " and exists (select 'X' from latree where agentcode=laagent.agentcode and (SpeciFlag is null or SpeciFlag<>'01'))" +
                           " and not exists (select 'X' from LAIndexInfo where managecom like '" +
                           mManageCom + "%'"
                           + " and indexcalno='" + IndexCalNo +
                           "' and indextype='00' and agentcode=LAAgent.agentcode ) "
                           + " order by AgentCode asc";
                   // 个险算薪资时 入司日期小于薪资月末日 离职日期为空或者离职日期大于薪资月首日 特殊人群不算薪资
              }
              else if( mBranchtype.equals("2"))
              {
                  tSql = "select * from LAAgent where ManageCom like '" +
                         mManageCom + "%' "
                         + " and BranchType = '" + mBranchtype + "' "
                         + " and BranchType2 ='" + mBranchType2 + "' "
                         + " and EmployDate <= (select enddate from lastatsegment where stattype='91' and yearmonth= " +
                         IndexCalNo + ")"
                         + " and (outworkdate is null or outworkdate>='" +
                         OutWorkBeginDate + "') " +
                         " and exists (select 'X' from latree where agentcode=laagent.agentcode and (SpeciFlag is null or SpeciFlag<>'01'))" +
                         " and not exists (select 'X' from LAIndexInfo where managecom like '" +
                         mManageCom + "%'"
                         + " and indexcalno='" + IndexCalNo +
                         "' and indextype='00' and agentcode=LAAgent.agentcode ) "
                       + " order by AgentCode asc";
              }
               //modify by Abigale   2008-03-04   225-242
              else if( mBranchtype.equals("3"))
              {
                  tSql = "select * from LAAgent where ManageCom like '" +
                         mManageCom + "%' "
                         + " and BranchType = '" + mBranchtype + "' "
                         + " and BranchType2 ='" + mBranchType2 + "' "
                         + " and EmployDate <= (select enddate from lastatsegment where stattype='5' and yearmonth= " +
                         IndexCalNo + ")"
                         + " and (outworkdate is null or outworkdate>='" +
                         OutWorkBeginDate + "') "
                         +" and exists (select 'X' from latree where agentcode=laagent.agentcode and (SpeciFlag is null or SpeciFlag<>'01')  "
                        +" )"
                        +" and not exists (select 'X' from LAIndexInfo where managecom like '" +mManageCom + "%'"
                        + " and indexcalno='" + IndexCalNo
                        + "' and indextype='00' and agentcode=LAAgent.agentcode ) "
                        + " order by AgentCode asc";
              }
            //modify by yangyang   2014-12-04   
            //对于互动渠道业务员查询
              else if( mBranchtype.equals("5"))
              {
                  tSql = "select * from LAAgent where ManageCom like '" +
                         mManageCom + "%' "
                         + " and BranchType = '" + mBranchtype + "' "
                         + " and BranchType2 ='" + mBranchType2 + "' "
                         + " and EmployDate <= (select enddate from lastatsegment where stattype='5' and yearmonth= " +
                         IndexCalNo + ")"
                         + " and (outworkdate is null or outworkdate>='" +
                         OutWorkBeginDate + "') "
                         +" and exists (select 'X' from latree where agentcode=laagent.agentcode and (SpeciFlag is null or SpeciFlag<>'01')  "
                        +" )"
                        +" and not exists (select 'X' from LAIndexInfo where managecom like '" +mManageCom + "%'"
                        + " and indexcalno='" + IndexCalNo
                        + "' and indextype='00' and agentcode=LAAgent.agentcode ) "
                        + " order by AgentCode asc";
              }
          //对于健康管理渠道的业务员查询
              else if(mBranchtype.equals("7")){


                  tSql = "select * from LAAgent where ManageCom like '" +
                         mManageCom + "%' "
                         + " and BranchType = '" + mBranchtype + "' "
                         + " and BranchType2 ='" + mBranchType2 + "' "
                         + " and EmployDate <= (select enddate from lastatsegment where stattype='5' and yearmonth= " +
                         IndexCalNo + ")"
                         + " and (outworkdate is null or outworkdate>='" +
                         OutWorkBeginDate + "') " +
                         " and exists (select 'X' from latree where agentcode=laagent.agentcode and (SpeciFlag is null or SpeciFlag<>'01'))" +
                         " and not exists (select 'X' from LAIndexInfo where managecom like '" +
                         mManageCom + "%'"
                         + " and indexcalno='" + IndexCalNo +
                         "' and indextype='00' and agentcode=LAAgent.agentcode ) "
                         + " order by AgentCode asc";
                
            
              } 

            mLAAgentSet = tLAAgentDB.executeQuery(tSql);
            //===得到需要计算薪资的人员基本信息
            System.out.println("查询人力" + tSql);
        }
        else
        {
        	//这个标注暂且没看出什么意思
            reCalFlag = true;//AgentMonthCal这个类调用了两次,一次有人 一次不结算
        }
        //获取要计算的基础指标laassessindex 中 indextype=‘00’ 薪资计算提取考核指标是干什么
        System.out.println("开始计算薪资指标了 -------------------------000000^-^");
        tLAAssessIndexDB.setBranchType(mBranchtype);
        tLAAssessIndexDB.setBranchType2(mBranchType2);
        //增加对团险中介的处理 :基础指标直接取团险直销不再单独配置--wrx
        if(mBranchType2.equals("02")){
            tLAAssessIndexDB.setBranchType2("01");
        }
        tLAAssessIndexDB.setIndexType("00");
        tLAAssessIndexSet = tLAAssessIndexDB.query();
        //======薪资指标计算 indextype = '00'  考核相关指标啊

        //获取计算的月初月末。为什么要描那么多的月份对应表
        LAStatSegmentDB tLAStatSegmentDB = new LAStatSegmentDB();
        tLAStatSegmentDB.setYearMonth(IndexCalNo);
        tLAStatSegmentDB.setStatType("1");
        if (!tLAStatSegmentDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentMonthCal";
            tError.functionName = "Examine";
            tError.errorMessage = "查询年月失败!";
            this.mErrors.addOneError(tError);
            return false;

        }
        MonthBegin = tLAStatSegmentDB.getStartDate();//薪资月计算起期
        MonthEnd = tLAStatSegmentDB.getEndDate();//薪资月计算止期
        //2.对每一个代理人作计算
         //循环对每一个人进行薪资计算操作！！！
        for (int i = 1; i <= mLAAgentSet.size(); i++)
        {
        	//对每一个代理人计算
            LAAgentSchema tLAAgentSchema = mLAAgentSet.get(i);
            mAgentCode = tLAAgentSchema.getAgentCode();
            mBranchCode=tLAAgentSchema.getBranchCode();
            mWageVersion=tLAAgentSchema.getWageVersion();
            // add new  团队建设
            this.mGbuildFlag=tLAAgentSchema.getGBuildFlag();
            this.mGbuildStartDate =tLAAgentSchema.getGBuildStartDate();
            
            mLAWageActivityLogSet= new LAWageActivityLogSet();
            tCalIndex.tAgCalBase.setAgentCode(mAgentCode);

            tExeSQL = new ExeSQL();
            //====为什么查询团队信息的时候 为什么要用branchcode字段.（区经理怎么办）  
            String bSQL =" select a.branchattr,b.agentgrade,a.branchseries,a.State "
                        +" from labranchgroup a,LATree b where a.agentgroup='"+ tLAAgentSchema.getBranchCode()
                        +"' and b.agentcode='" + mAgentCode + "'";
            SSRS tSSRS = new SSRS();
            tSSRS = tExeSQL.execSQL(bSQL);
            //如果 机构state =1  则不算佣金和考核     2006-03-30  xiangchun
            String tState=tSSRS.GetText(1, 4);
            if ("1".equals(tState))
            {
                continue;
            }
            String tAgentGroup = "";
            if(mBranchtype.equals("2"))
            {
                tAgentGroup=AgentPubFun.getMonthAgentGroup(mAgentCode,IndexCalNo);//为什么团险和银代加上了这个 个险没有
                mAreaType = AgentPubFun.getAreaType(tAgentGroup);
            }
            //modify  by Abigale   305-310
            else if(mBranchtype.equals("3")&& mBranchType2.equals("01"))
            {
                tAgentGroup=AgentPubFun.getMonthAgentGroup(mAgentCode,IndexCalNo);
                mAreaType = getAreaType(mManageCom,mBranchtype,mBranchType2);
            }

            else
            {
                tAgentGroup=tLAAgentSchema.getAgentGroup();//个险就是比较给力啊！！！
            }	

            //得到当月的职级
             String AgentGrade="";
            if(mBranchtype.equals("3")&& mBranchType2.equals("01"))
            {
                AgentGrade = AgentPubFun.getMonthAgentGradeBK(mAgentCode,IndexCalNo);
            }
            else
            {
                AgentGrade = AgentPubFun.getMonthAgentGrade(mAgentCode,IndexCalNo);
            }
            mAgentGrade = AgentGrade;
            String currentDate = PubFun.getCurrentDate();
            String currentTime = PubFun.getCurrentTime();
            mLAIndexInfoSchema = new LAIndexInfoSchema();
            mLAIndexInfoSchema.setAgentCode(mAgentCode);
            mLAIndexInfoSchema.setManageCom(tLAAgentSchema.getManageCom());
            mLAIndexInfoSchema.setIndexCalNo(IndexCalNo);
            mLAIndexInfoSchema.setIndexType("00");
            mLAIndexInfoSchema.setStartDate(MonthBegin);
            mLAIndexInfoSchema.setStartEnd(MonthEnd);
            mLAIndexInfoSchema.setOperator(Operator);
       //     mLAIndexInfoSchema.setAgentGroup(tLAAgentSchema.getAgentGroup());
            mLAIndexInfoSchema.setAgentGroup(tAgentGroup);
            mLAIndexInfoSchema.setMakeDate(currentDate);
            mLAIndexInfoSchema.setModifyDate(currentDate);
            mLAIndexInfoSchema.setMakeTime(currentTime);
            mLAIndexInfoSchema.setModifyTime(currentTime);

            mLAIndexInfoSchema.setBranchType(mBranchtype );
            mLAIndexInfoSchema.setBranchType2(mBranchType2);
            if (tSSRS.getMaxRow() == 0)
            {
//                if (AgentGrade == null || AgentGrade.equals(""))
//                {//职级没有发生变化
//                    String sql =
//                            "select agentgrade from LATree where agentcode='" +
//                            mAgentCode + "'";
//                    ExeSQL aExeSQL = new ExeSQL();
//                    AgentGrade = aExeSQL.getOneValue(sql);
//                }
                mLAIndexInfoSchema.setBranchAttr("");
                mLAIndexInfoSchema.setAgentGrade(AgentGrade);
                mLAIndexInfoSchema.setBranchSeries("");
            }
            else
            {
                String BranchAttr = tSSRS.GetText(1, 1);
//                if (AgentGrade==null || AgentGrade.equals(""))
//                {//当月职级没有变化
//                    AgentGrade = tSSRS.GetText(1, 2);
                if(mBranchtype.equals("2")&& mBranchType2.equals("01"))
                {
                	//团险和银代 根据内部编码 得到外部编码
                    BranchAttr = AgentPubFun.getMonthBranchAttr(tAgentGroup);
                    
                }
                //modify  by Abigale   362-367
                 if(mBranchtype.equals("3")&& mBranchType2.equals("01"))
                {
                    BranchAttr = AgentPubFun.getMonthBranchAttr(tAgentGroup);
                }

                mLAIndexInfoSchema.setBranchAttr(BranchAttr);
                mLAIndexInfoSchema.setAgentGrade(AgentGrade);
                mLAIndexInfoSchema.setBranchSeries(tSSRS.GetText(1,3) );
            }
             //9.具体计算
            if (!WageCal())
            {
                System.out.println("Error:" + this.mErrors.getFirstError());
                return false;
            }
            conn = DBConnPool.getConnection();
            if (this.conn == null)
            {
                CError tError = new CError();
                tError.moduleName = "AgentMonthCal";
                tError.functionName = "Examine";
                tError.errorMessage = "获取数据库连结失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            try
            {
                conn.setAutoCommit(false);
                //====将计算出来的结果 存储到laindexinfo表里
                LAIndexInfoDB tQuaLAIndexInfoDB = new LAIndexInfoDB(conn);
                //===存储到laindexinfo表中去 其中indextype= ‘00’
                tQuaLAIndexInfoDB.setAgentCode(mLAIndexInfoSchema.getAgentCode());
                tQuaLAIndexInfoDB.setIndexCalNo(mLAIndexInfoSchema.getIndexCalNo());
                tQuaLAIndexInfoDB.setIndexType(mLAIndexInfoSchema.getIndexType());
                LAIndexInfoDB tLAIndexInfoDB = new LAIndexInfoDB(conn);
                tLAIndexInfoDB.setSchema(mLAIndexInfoSchema);
                //如果存在，update，否则insert 这里可以知道回退薪资的时候 不需要删除laindexinfo 表
                if(!tQuaLAIndexInfoDB.getInfo())
                {
                    if (!tLAIndexInfoDB.insert()) {
                        this.mErrors.copyAllErrors(tLAIndexInfoDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "AgentMonthCal";
                        tError.functionName = "Examine";
                        tError.errorMessage = "更新结果记录失败";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                }
                else
                {
                    if (!tLAIndexInfoDB.update())
                    {
                        this.mErrors.copyAllErrors(tLAIndexInfoDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "AgentMonthCal";
                        tError.functionName = "Examine";
                        tError.errorMessage = "更新结果记录失败";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                }
                LAWageActivityLogDBSet tLAWageActivityLogDBSet = new LAWageActivityLogDBSet(conn);
                tLAWageActivityLogDBSet.set(mLAWageActivityLogSet);
                if (mLAWageActivityLogSet.size() >0 &&!tLAWageActivityLogDBSet.insert())
                {
                               // @@错误处理
                     this.mErrors.copyAllErrors(tLAWageActivityLogDBSet.mErrors);
                     CError tError = new CError();
                     tError.moduleName = "AgentMonthCal";
                     tError.functionName = "saveData";
                     tError.errorMessage = "更新结果记录失败!";
                     this.mErrors.addOneError(tError);
                     conn.rollback();
                     conn.close();
                     return false;
              }
                if (reCalFlag &&
                    !transResultFromLAIndexInfoToLAIndexInfoTemp(mAgentCode,
                        IndexCalNo,
                        conn
                    ))
                {
                    return false;
                }
                conn.commit();
                conn.close();
            }

            catch (Exception e)
            {
                e.printStackTrace();
                try
                {
                    this.conn.rollback();
                    this.conn.close();
                }
                catch (Exception ep)
                {
                    ep.printStackTrace();
                    CError tError = new CError();
                    tError.moduleName = "AgentMonthCal";
                    tError.functionName =
                            "transResultFromLAIndexInfoToLAIndexInfoTemp";
                    tError.errorMessage = "插入LAIndexInfoTemp表的记录失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }

            }
            System.out.println("***************一个人指标计算完成******************");
            ;
        }
        if (!reCalFlag)
        {
            conn = DBConnPool.getConnection();
            if (this.conn == null)
            {
                CError tError = new CError();
                tError.moduleName = "AgentMonthCal";
                tError.functionName = "Examine";
                tError.errorMessage = "获取数据库连结失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            try
            {
                conn.setAutoCommit(false);

                tExeSQL = new ExeSQL(conn);
                String sql =
                        "delete from laindexinfotemp where  ManageCom like '" +
                        mManageCom +
                        "%' and indextype='00' and IndexCalNo='" + mYearMonth +
                        "' ";
                if (!tExeSQL.execUpdateSQL(sql))
                {
                    this.mErrors.copyAllErrors(tExeSQL.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AgentMonthCal";
                    tError.functionName =
                            "transResultFromLAIndexInfoToLAIndexInfoTemp";
                    tError.errorMessage = "删除LAIndexInfoTemp表的记录失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;

                }
                else
                {
                    sql =
                            "insert into laindexinfotemp select * from laindexinfo where managecom like '" +
                            mManageCom + "%' and indexcalno='" + mYearMonth +
                            "' and indextype='00' ";
                    if (!tExeSQL.execUpdateSQL(sql))
                    {
                        this.mErrors.copyAllErrors(tExeSQL.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "AgentMonthCal";
                        tError.functionName =
                                "transResultFromLAIndexInfoToLAIndexInfoTemp";
                        tError.errorMessage = "删除LAIndexInfoTemp表的记录失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;

                    }
                    conn.commit();
                    conn.close();
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                try
                {
                    this.conn.rollback();
                    this.conn.close();
                }
                catch (Exception ep)
                {
                    ep.printStackTrace();
                    CError tError = new CError();
                    tError.moduleName = "AgentMonthCal";
                    tError.functionName =
                            "transResultFromLAIndexInfoToLAIndexInfoTemp";
                    tError.errorMessage = "插入LAIndexInfoTemp表的记录失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }

            }

        }

        return true;

    }
  //=====将数据插入到备份表中去
    private boolean transResultFromLAIndexInfoToLAIndexInfoTemp(String
            tAgentCode,
            String cIndexCalNo, Connection conn)
    {
        ExeSQL tExeSQL = new ExeSQL(conn);
        String sql = "delete from laindexinfotemp where  AgentCode='" +
                     tAgentCode +
                     "' and IndexCalNo='" + cIndexCalNo + "' ";
        if (!tExeSQL.execUpdateSQL(sql))
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentMonthCal";
            tError.functionName = "transResultFromLAIndexInfoToLAIndexInfoTemp";
            tError.errorMessage = "删除LAIndexInfoTemp表的记录失败!";
            this.mErrors.addOneError(tError);
            return false;

        }
        else
        {
            LAIndexInfoTempSchema tLAIndexInfoTempSchema = new
                    LAIndexInfoTempSchema();
            for (int j = 0; j < mLAIndexInfoSchema.FIELDNUM; j++)
            {
                String field = mLAIndexInfoSchema.getFieldName(j);
                String value = mLAIndexInfoSchema.getV(j);
                tLAIndexInfoTempSchema.setV(field, value);
            }
            LAIndexInfoTempDB tLAIndexInfoTempDB = new LAIndexInfoTempDB(conn);
            tLAIndexInfoTempDB.setSchema(tLAIndexInfoTempSchema);
            if (!tLAIndexInfoTempDB.insert())
            {
                this.mErrors.copyAllErrors(tLAIndexInfoTempDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentMonthCal";
                tError.functionName =
                        "transResultFromLAIndexInfoToLAIndexInfoTemp";
                tError.errorMessage = "插入LAIndexInfoTemp表的记录失败!";
                this.mErrors.addOneError(tError);
                return false;

            }

            return true;
        }
    }


//如果该代理人没有离职，那么需要计算该代理人的佣金指标
//否则，根据离职时间确定离职时处于哪一个承保月度，如果这个承保月度小于IndexCalno，
//则不用计算（因为在当前的计算月肯定没有业绩）

    private boolean getoutworkdate(LAAgentSet tLAAgentSet, int num)
    { //, String cAgentCode,String cGrade)
        if (tLAAgentSet.get(num).getOutWorkDate() == null ||
            tLAAgentSet.get(num).getOutWorkDate().equals(""))
        {
            return true;
        }
        //否则，根据离职时间确定离职时处于哪一个承保月度，如果这个承保月度小于IndexCalno，则不用计算（因为在当前的计算月肯定没有业绩）
        else
        {
            //查询承保月度统计间隔
             String tSQL1 = "";
            if (tLAAgentSet.get(num).getBranchType().equals("1") )
            {
                 tSQL1 =
                        "select yearmonth from lastatsegment where stattype='5' and '" +
                        tLAAgentSet.get(num).getOutWorkDate()
                        + "'>=startDate  and '" +
                        tLAAgentSet.get(num).getOutWorkDate() +
                        "'<=endDate";
            }
            if (tLAAgentSet.get(num).getBranchType().equals("2") )
            {
                 tSQL1 =
                        "select yearmonth from lastatsegment where stattype='91' and '" +
                        tLAAgentSet.get(num).getOutWorkDate()
                        + "'>=startDate  and '" +
                        tLAAgentSet.get(num).getOutWorkDate() +
                        "'<=endDate";
            }
            //modify by Abigale 656-665
            if (tLAAgentSet.get(num).getBranchType().equals("3") )
            {
                 tSQL1 =
                        "select yearmonth from lastatsegment where stattype='91' and '" +
                        tLAAgentSet.get(num).getOutWorkDate()
                        + "'>=startDate  and '" +
                        tLAAgentSet.get(num).getOutWorkDate() +
                        "'<=endDate";
            }
            System.out.println(tSQL1);
            ExeSQL tExeSQL = new ExeSQL();
            String calDate = tExeSQL.getOneValue(tSQL1);
            System.out.println("calDate " + calDate);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalBankWageBL";
                tError.functionName = "IsCalWage";
                tError.errorMessage = "查询" + tLAAgentSet.get(num).getAgentCode() +
                                      "的统计日期出错！";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (calDate.compareTo(this.mYearMonth) < 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }


//10.给基数类赋值
    private boolean setBaseValue(AgCalBase cAgCalBase)
    { //, String cAgentCode,String cGrade)
        tCalIndex.tAgCalBase.setAreaType(mAreaType);
        tCalIndex.tAgCalBase.setWageVersion(mWageVersion);
        System.out.println("111111111111111111111111111111111111111111111111111"+mAreaType);
        tCalIndex.tAgCalBase.setManageCom(mManageCom);
        tCalIndex.tAgCalBase.setAgentCode(mAgentCode);
        tCalIndex.tAgCalBase.setWageNo(IndexCalNo);
        tCalIndex.tAgCalBase.setBranchCode(mBranchCode);
        tCalIndex.tAgCalBase.setWageVersion(mWageVersion);
        tCalIndex.tAgCalBase.setTempBegin(MonthBegin);
        tCalIndex.tAgCalBase.setTempEnd(MonthEnd);
        tCalIndex.tAgCalBase.setBranchType(mBranchtype);
        tCalIndex.tAgCalBase.setBranchType2(mBranchType2);
        tCalIndex.tAgCalBase.setAgentGrade(mAgentGrade);
        tCalIndex.tAgCalBase.setBranchAttr(mLAIndexInfoSchema.getBranchAttr());
        tCalIndex.tAgCalBase.setAgentGroup(mLAIndexInfoSchema.getAgentGroup());
        
        // add new 团队建设
        tCalIndex.tAgCalBase.setGbuildFlag(mGbuildFlag);
        tCalIndex.tAgCalBase.setGbuildStartDate(mGbuildStartDate);

        return true;
    }

    private boolean insertAssess(String cIndexCalNo, String cAgentCode,
                                 String cManageCom, String calcode,
                                 String cIndexValue)
    {
        String tSQL = "", tCount = "";
        String tModifyFlag = "";
        boolean tReturn = false;
        //====将数值变成double型的
        double tValue=Double.parseDouble(cIndexValue);
        //大于1千万，java会自动转化为科学技术法，需要作特殊处理
        if(tValue>=10000000)
        {

            String tResult = mFRDecimalFormat.format(tValue);
        //    double tResult = mFRDecimalFormat.format(tValue);
            System.out.println("|||||||||||||||||||||||||||||||||||||||"+tResult);
            mLAIndexInfoSchema.setV(mIColName, tResult);
        }
        else
        {//小于1千万，有可能是 int型，不能转化成浮点型数据（其实大于1000万也可能是int型，应该处理）
            mLAIndexInfoSchema.setV(mIColName, cIndexValue);
        }
        return true;
    }

//设置计算函数中的参数
    private void addAgCalBase(Calculator cCal)
    {
        cCal.addBasicFactor("AreaType", tCalIndex.tAgCalBase.getAreaType());
        cCal.addBasicFactor("WageVersion", tCalIndex.tAgCalBase.getWageVersion());
        cCal.addBasicFactor("AgentCode", tCalIndex.tAgCalBase.getAgentCode());
        cCal.addBasicFactor("ManageCom", tCalIndex.tAgCalBase.getManageCom());
        cCal.addBasicFactor("BranchCode", tCalIndex.tAgCalBase.getBranchCode());
        cCal.addBasicFactor("WageNo", tCalIndex.tAgCalBase.getWageNo());
        cCal.addBasicFactor("IndexCalNo", tCalIndex.tAgCalBase.getWageNo());
        cCal.addBasicFactor("WageCode", tCalIndex.tAgCalBase.getWageCode());
        cCal.addBasicFactor("AgentGrade", tCalIndex.tAgCalBase.getAgentGrade());
        cCal.addBasicFactor("AgentGroup", tCalIndex.tAgCalBase.getAgentGroup());
        cCal.addBasicFactor("MonthBegin", tCalIndex.tAgCalBase.getMonthBegin());
        cCal.addBasicFactor("MonthEnd", tCalIndex.tAgCalBase.getMonthEnd());
        cCal.addBasicFactor("QuauterBegin",
                            tCalIndex.tAgCalBase.getQuauterBegin());
        cCal.addBasicFactor("QuauterEnd", tCalIndex.tAgCalBase.getQuauterEnd());
        cCal.addBasicFactor("HalfYearBegin",
                            tCalIndex.tAgCalBase.getHalfYearBegin());
        cCal.addBasicFactor("HalfYearEnd", tCalIndex.tAgCalBase.getHalfYearEnd());
        cCal.addBasicFactor("YearBegin", tCalIndex.tAgCalBase.getYearBegin());
        cCal.addBasicFactor("YearEnd", tCalIndex.tAgCalBase.getYearEnd());
        cCal.addBasicFactor("MonthMark", tCalIndex.tAgCalBase.getMonthMark());
        cCal.addBasicFactor("QuauterMark", tCalIndex.tAgCalBase.getQuauterMark());
        cCal.addBasicFactor("HalfYearMark",
                            tCalIndex.tAgCalBase.getHalfYearMark());
        cCal.addBasicFactor("YearMark", tCalIndex.tAgCalBase.getYearMark());
        cCal.addBasicFactor("TempBegin", tCalIndex.tAgCalBase.getTempBegin());
        cCal.addBasicFactor("TempEnd", tCalIndex.tAgCalBase.getTempEnd());
        cCal.addBasicFactor("Rate", String.valueOf(tCalIndex.tAgCalBase.getRate()));
        cCal.addBasicFactor("A1State",
                            String.valueOf(tCalIndex.tAgCalBase.getA1State()));
        cCal.addBasicFactor("AssessType", tCalIndex.tAgCalBase.getAssessType());
        cCal.addBasicFactor("LimitPeriod", tCalIndex.tAgCalBase.getLimitPeriod());
        cCal.addBasicFactor("DestAgentGrade",
                            tCalIndex.tAgCalBase.getDestAgentGrade());
        cCal.addBasicFactor("BranchAttr", tCalIndex.tAgCalBase.getBranchAttr());
        cCal.addBasicFactor("ChannelType", tCalIndex.tAgCalBase.getChannelType());;
        cCal.addBasicFactor("BranchType", tCalIndex.tAgCalBase.getBranchType());
        cCal.addBasicFactor("BranchType2", tCalIndex.tAgCalBase.getBranchType2());
        
        // add new 团队建设
        cCal.addBasicFactor("GbuildFlag", tCalIndex.tAgCalBase.getGbuildFlag());
        cCal.addBasicFactor("GbuildStartDate", tCalIndex.tAgCalBase.getGbuildStartDate());
        
        System.out.println("指标开始前奏：" + tCalIndex.tAgCalBase.getAgentCode());
        
    }

    private boolean WageCal()
    {
        String tReturn = "";
        /* 设置基本参数 */
        //判断该业务员是否本月指标
        if (!setBaseValue(this.tCalIndex.tAgCalBase))
        {
            return false;
        }

        for (int i = 1; i <= tLAAssessIndexSet.size(); i++)
        {
        	//====核心计算类 （这个需要好好看看）
            Calculator tCal = new Calculator();
            tCal.setCalCode(tLAAssessIndexSet.get(i).getCalCode());
            //====循环开始计算各项指标信息
            addAgCalBase(tCal);
            tReturn = tCal.calculate();
            
            if(tCal.mErrors.needDealError())
            {
                LAWageActivityLogSchema tLAWageActivityLogSchema = new LAWageActivityLogSchema();
                tLAWageActivityLogSchema.setAgentCode(tCalIndex.tAgCalBase.getAgentCode());
                tLAWageActivityLogSchema.setAgentGroup(tCalIndex.tAgCalBase.getAgentGroup());
                tLAWageActivityLogSchema.setContNo("000000");
                tLAWageActivityLogSchema.setDescribe("计算薪资基础指标"
                        +tLAAssessIndexSet.get(i).getIndexName()+"出错!");
                if("1".equals(mBranchtype) && "01".equals(mBranchType2))
                {
                    tLAWageActivityLogSchema.setGrpContNo("00000000000000000000");
                    tLAWageActivityLogSchema.setGrpPolNo("00000000000000000000");
                }
                else if("2".equals(mBranchtype)  )
                {
                    tLAWageActivityLogSchema.setGrpContNo("000000");
                    tLAWageActivityLogSchema.setGrpPolNo("000000");
                }
                //modify  by Abigale  796-801
                if("3".equals(mBranchtype) && "01".equals(mBranchType2))
                {
                    tLAWageActivityLogSchema.setGrpContNo("00000000000000000000");
                    tLAWageActivityLogSchema.setGrpPolNo("00000000000000000000");
                }if("7".equals(mBranchtype)&&"01".equals(mBranchType2)){
                	tLAWageActivityLogSchema.setGrpContNo("000000000000000000");
                    tLAWageActivityLogSchema.setGrpPolNo("000000000000000000");
                }

                tLAWageActivityLogSchema.setMakeDate(CurrentDate);
                tLAWageActivityLogSchema.setMakeTime(CurrentTime);
                tLAWageActivityLogSchema.setManageCom(mManageCom);
                tLAWageActivityLogSchema.setOperator(mGlobalInput.Operator);
                tLAWageActivityLogSchema.setOtherNo("");
                tLAWageActivityLogSchema.setOtherNoType("");
                tLAWageActivityLogSchema.setPolNo("000000");
                tLAWageActivityLogSchema.setRiskCode("000000");

                String tSerialNo = PubFun1.CreateMaxNo("WAGEACTIVITYLOG", 12);
                tLAWageActivityLogSchema.setSerialNo(tSerialNo);
                tLAWageActivityLogSchema.setWageLogType("02");
                tLAWageActivityLogSchema.setWageNo(IndexCalNo);
                mLAWageActivityLogSet.add(tLAWageActivityLogSchema);
            }

            mTableName = tLAAssessIndexSet.get(i).getITableName();
            //===得到薪资项存储表名
            mIColName = tLAAssessIndexSet.get(i).getIColName();
            //====得到薪资项存储项名
            if ((tReturn == null) || (tReturn.trim().equals("")))
            {
                tReturn = "0";
            }
            if (!insertAssess(IndexCalNo, mAgentCode, mManageCom,
                              tLAAssessIndexSet.get(i).getCalCode(), tReturn))
            {
                System.out.println("插入指标表出错！！");
                return false;
            }
        }
        System.out.println(
                "---------------------------------------------------------");
        return true;
    }

    public CErrors getErrors()
    {

        return mErrors;
    }
    
    public static String getAreaType(String cManageCom,String cBranchType,String cBranchType2)
    {
           //AreaType

           
           String tSql = "Select trim(WageFlag) From LABankIndexRadix Where managecom = trim(substr('"+cManageCom+"',1,4)) and branchtype = '" +
                        cBranchType + "' and branchtype2='"+cBranchType2+"' ";
           ExeSQL tExeSQL = new ExeSQL();
           String tAreaType = tExeSQL.getOneValue(tSql);
           if (tExeSQL.mErrors.needDealError())
               return null;

           if (tAreaType == null || tAreaType.equals(""))
               return null;
           return tAreaType;
   }
    
    public static void main(String[] args)
    {
        System.out.println("---------------开始指标计算！-----------------");
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86310000";
        tGlobalInput.Operator = "zy";
        tGlobalInput.ComCode = "86";
        String ManageCom = "86310000";
        String WageYear = "200703";
        VData tVData = new VData();
        tVData.clear();
         String FRateFORMATMODOL = "0.00"; //浮动费率计算出来后的精确位数
         DecimalFormat mFRDecimalFormat = new DecimalFormat(FRateFORMATMODOL); //数字转换对象
        double b=100000000;
        System.out.println(mFRDecimalFormat.format(b));
        BigDecimal c;
        String aa=mFRDecimalFormat.format(b);
        BigDecimal tOneBigInteger = new BigDecimal(aa);

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("ManageCom", ManageCom);
        tTransferData.setNameAndValue("IndexCalNo", WageYear);
        tTransferData.setNameAndValue("GlobalInput", tGlobalInput);
        tTransferData.setNameAndValue("BranchType", "2");
        tTransferData.setNameAndValue("BranchType2", "01");
        tTransferData.setNameAndValue("LAAgentSet", new LAAgentSet());
        tVData.add(tTransferData);

        AgentMonthCal tAgentMonthCal = new AgentMonthCal();
        tAgentMonthCal.submitData(tVData, "");
        if (tAgentMonthCal.mErrors.needDealError())
        {
            System.out.println("程序异常结束原因：" +
                               tAgentMonthCal.mErrors.getFirstError());
        }
        System.out.println("--------------结束指标计算！---------------");
    }
}
