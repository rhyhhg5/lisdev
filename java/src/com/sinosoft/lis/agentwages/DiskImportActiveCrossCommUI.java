package com.sinosoft.lis.agentwages;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author luomin
 * @version 1.1
 */
public class DiskImportActiveCrossCommUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private DiskImportActiveCrossCommBL mDiskImportActiveCrossCommBL = null;

    public DiskImportActiveCrossCommUI()
    {
    	mDiskImportActiveCrossCommBL = new DiskImportActiveCrossCommBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if(!mDiskImportActiveCrossCommBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mDiskImportActiveCrossCommBL.mErrors);
            return false;
        }
        //
        if(mDiskImportActiveCrossCommBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mDiskImportActiveCrossCommBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mDiskImportActiveCrossCommBL.getImportPersons();
    }

    public int getUnImportRecords()
    {
        return mDiskImportActiveCrossCommBL.getUnImportRecords();
    }

    public static void main(String[] args)
    {
    	DiskImportCrossCommUI zDiskImportCrossCommUI = new DiskImportCrossCommUI();
    }
}
