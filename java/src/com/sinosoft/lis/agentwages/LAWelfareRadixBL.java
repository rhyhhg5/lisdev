package com.sinosoft.lis.agentwages;

import com.sinosoft.lis.db.LAWelfareRadixDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAWelfareRadixBSchema;
import com.sinosoft.lis.schema.LAWelfareRadixSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */

public class LAWelfareRadixBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    //private GlobalInput mGlobalInput =new GlobalInput() ;
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAWelfareRadixSchema mLAWelfareRadixSchema = new
            LAWelfareRadixSchema();
    private LAWelfareRadixBSchema mLAWelfareRadixBSchema = new
            LAWelfareRadixBSchema();
    //private LAWelfareRadixSet mLAWelfareRadixSet=new LAWelfareRadixSet();
    public LAWelfareRadixBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        //进行插入数据
        if (mOperate.equals("INSERT"))
        {
            if (!dealData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAWelfareRadixBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败LAWelfareRadixBL-->dealData!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        //对数据进行修改操作
        if (mOperate.equals("UPDATE"))
        {
            if (!updateData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAWelfareRadixBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败LAWelfareRadixBL-->updateData!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("---updateData---");
        }
        //对数据进行删除操作
        if (mOperate.equals("DELETE"))
        {
            if (!deleteData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAWelfareRadixBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败LAWelfareRadixBL-->deleteData!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("----deleteData---");
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start LAWelfareRadixBL Submit...");
            LAWelfareRadixBLS tLAWelfareRadixBLS = new LAWelfareRadixBLS();
            tLAWelfareRadixBLS.submitData(mInputData, mOperate);
            System.out.println("End LAWelfareRadixBL Submit...");
            //如果有需要处理的错误，则返回
            if (tLAWelfareRadixBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAWelfareRadixBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAWelfareRadixBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        this.mLAWelfareRadixSchema.setMakeDate(PubFun.getCurrentDate());
        this.mLAWelfareRadixSchema.setMakeTime(PubFun.getCurrentTime());
        this.mLAWelfareRadixSchema.setModifyDate(PubFun.getCurrentDate());
        this.mLAWelfareRadixSchema.setModifyTime(PubFun.getCurrentTime());
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData()
    {
        LAWelfareRadixDB tLAWelfareRadixDB = new LAWelfareRadixDB();
        tLAWelfareRadixDB.setManageCom(mLAWelfareRadixSchema.getManageCom());
        tLAWelfareRadixDB.setAgentGrade(mLAWelfareRadixSchema.getAgentGrade());
        tLAWelfareRadixDB.setAClass(mLAWelfareRadixSchema.getAClass());
        if (!tLAWelfareRadixDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWelfareRadixBL";
            tError.functionName = "updateData";
            tError.errorMessage = "数据处理失败LAWelfareRadixBL-->updateData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.mLAWelfareRadixBSchema.setEdorNo(PubFun1.CreateMaxNo("SERIALNO",
                "WelfareB"));
        this.mLAWelfareRadixBSchema.setEdorType("02");
        this.mLAWelfareRadixBSchema.setManageCom(tLAWelfareRadixDB.getManageCom());
        this.mLAWelfareRadixBSchema.setAgentGrade(tLAWelfareRadixDB.
                                                  getAgentGrade());
        this.mLAWelfareRadixBSchema.setAClass(tLAWelfareRadixDB.getAClass());
        this.mLAWelfareRadixBSchema.setStartDate(tLAWelfareRadixDB.
                                                 getModifyDate());
        this.mLAWelfareRadixBSchema.setEndDate(PubFun.getCurrentDate());
        this.mLAWelfareRadixBSchema.setBranchType(tLAWelfareRadixDB.
                                                  getBranchType());
        this.mLAWelfareRadixBSchema.setStartPayYear(tLAWelfareRadixDB.
                getStartPayYear());
        this.mLAWelfareRadixBSchema.setServeYear(tLAWelfareRadixDB.getServeYear());
        this.mLAWelfareRadixBSchema.setDrawBase(tLAWelfareRadixDB.getDrawBase());
        this.mLAWelfareRadixBSchema.setDrawRate(tLAWelfareRadixDB.getDrawRate());
        this.mLAWelfareRadixBSchema.setDrawMoney(tLAWelfareRadixDB.getDrawMoney());
        this.mLAWelfareRadixBSchema.setOperator(tLAWelfareRadixDB.getOperator());
        this.mLAWelfareRadixBSchema.setMakeDate(PubFun.getCurrentDate());
        this.mLAWelfareRadixBSchema.setMakeTime(PubFun.getCurrentTime());
        this.mLAWelfareRadixBSchema.setModifyDate(PubFun.getCurrentDate());
        this.mLAWelfareRadixBSchema.setModifyTime(PubFun.getCurrentTime());

        this.mLAWelfareRadixSchema.setMakeDate(tLAWelfareRadixDB.getMakeDate());
        this.mLAWelfareRadixSchema.setMakeTime(tLAWelfareRadixDB.getMakeTime());
        this.mLAWelfareRadixSchema.setModifyDate(PubFun.getCurrentDate());
        this.mLAWelfareRadixSchema.setModifyTime(PubFun.getCurrentTime());
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        LAWelfareRadixDB tLAWelfareRadixDB = new LAWelfareRadixDB();
        tLAWelfareRadixDB.setManageCom(mLAWelfareRadixSchema.getManageCom());
        tLAWelfareRadixDB.setAgentGrade(mLAWelfareRadixSchema.getAgentGrade());
        tLAWelfareRadixDB.setAClass(mLAWelfareRadixSchema.getAClass());
        if (!tLAWelfareRadixDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWelfareRadixBL";
            tError.functionName = "updateData";
            tError.errorMessage = "数据处理失败LAWelfareRadixBL-->updateData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.mLAWelfareRadixBSchema.setEdorNo(PubFun1.CreateMaxNo("SERIALNO",
                "WelfareB"));
        this.mLAWelfareRadixBSchema.setEdorType("01");
        this.mLAWelfareRadixBSchema.setManageCom(tLAWelfareRadixDB.getManageCom());
        this.mLAWelfareRadixBSchema.setAgentGrade(tLAWelfareRadixDB.
                                                  getAgentGrade());
        this.mLAWelfareRadixBSchema.setAClass(tLAWelfareRadixDB.getAClass());
        this.mLAWelfareRadixBSchema.setStartDate(tLAWelfareRadixDB.
                                                 getModifyDate());
        this.mLAWelfareRadixBSchema.setEndDate(PubFun.getCurrentDate());
        this.mLAWelfareRadixBSchema.setBranchType(tLAWelfareRadixDB.
                                                  getBranchType());
        this.mLAWelfareRadixBSchema.setStartPayYear(tLAWelfareRadixDB.
                getStartPayYear());
        this.mLAWelfareRadixBSchema.setServeYear(tLAWelfareRadixDB.getServeYear());
        this.mLAWelfareRadixBSchema.setDrawBase(tLAWelfareRadixDB.getDrawBase());
        this.mLAWelfareRadixBSchema.setDrawRate(tLAWelfareRadixDB.getDrawRate());
        this.mLAWelfareRadixBSchema.setDrawMoney(tLAWelfareRadixDB.getDrawMoney());
        this.mLAWelfareRadixBSchema.setOperator(tLAWelfareRadixDB.getOperator());
        this.mLAWelfareRadixBSchema.setMakeDate(PubFun.getCurrentDate());
        this.mLAWelfareRadixBSchema.setMakeTime(PubFun.getCurrentTime());
        this.mLAWelfareRadixBSchema.setModifyDate(PubFun.getCurrentDate());
        this.mLAWelfareRadixBSchema.setModifyTime(PubFun.getCurrentTime());
        LAWelfareRadixSchema tLAWelfareRadixSchema = new LAWelfareRadixSchema();
        tLAWelfareRadixSchema.setManageCom(mLAWelfareRadixSchema.getManageCom());
        tLAWelfareRadixSchema.setAgentGrade(mLAWelfareRadixSchema.getAgentGrade());
        tLAWelfareRadixSchema.setAClass(mLAWelfareRadixSchema.getAClass());
        mLAWelfareRadixSchema.setSchema(tLAWelfareRadixSchema);
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAWelfareRadixSchema.setSchema((LAWelfareRadixSchema) cInputData.
                                             getObjectByObjectName(
                "LAWelfareRadixSchema", 0));
        //this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start LAWelfareRadixBLQuery Submit...");
        LAWelfareRadixDB tLAWelfareRadixDB = new LAWelfareRadixDB();
        tLAWelfareRadixDB.setSchema(this.mLAWelfareRadixSchema);
        //this.mLAWelfareRadixSet=tLAWelfareRadixDB.query();
        //this.mResult.add(this.mLAWelfareRadixSet);
        System.out.println("End LAWelfareRadixBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLAWelfareRadixDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAWelfareRadixDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAWelfareRadixBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            //this.mInputData=new VData();
            //this.mInputData.add(this.mGlobalInput);
            this.mInputData.clear();
            this.mInputData.add(this.mLAWelfareRadixSchema);
            mResult.clear();
            mResult.add(this.mLAWelfareRadixSchema);
            if (mOperate.equals("UPDATE") || mOperate.equals("DELETE"))
            {
                mInputData.add(this.mLAWelfareRadixBSchema);
                mResult.add(this.mLAWelfareRadixBSchema);
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWelfareRadixBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public static void main(String[] args)
    {
        LAWelfareRadixBL LAWelfareRadixBL1 = new LAWelfareRadixBL();
    }
}
