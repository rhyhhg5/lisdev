package com.sinosoft.lis.agentwages;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.LJAGetDB;

/**
 * <p>Title: 付费管理处理程序</p>
 *
 * <p>Description: 解锁付费，更改交</p>
 *
 * <p>Copyright: Copyright (c)2018</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author : zyy
 * @version 1.0
 */
public class UnLockBL {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public CErrors mErrors = new CErrors();

/** 往后面传输数据的容器 */
private VData mInputData= new VData();;

/** 往界面传输数据的容器 */
private VData mResult = new VData();
private TransferData mTransferData = new TransferData();
private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
private LJAGetDB tLJAGetDB = new LJAGetDB();
private LJAGetSchema tLJAGetSchema = new LJAGetSchema();
private LJAGetSet tLJAGetSet=new LJAGetSet();
private LJAGetSet mLJAGetSet=new LJAGetSet();
private GlobalInput mGlobalInput = new GlobalInput();
private MMap map = new MMap();
private String mOperate;
private String mActuGetNo="";
private String mManagecom="";
private String mIndexcalno="";
private String mAgentcode="";
private String mCanSendBank="";
private String CurrentDate = PubFun.getCurrentDate();
private String CurrentTime = PubFun.getCurrentTime();
public UnLockBL() {}
    /**
    * 传输数据的公共方法
    * @param: cInputData 输入的数据
    *         cOperate 数据操作
    * @return:
    */
   public boolean submitData(VData cInputData, String cOperate) {

     System.out.println("---UnLockBL BEGIN---");
     mOperate = cOperate;
     if (!getInputData(cInputData)){
         System.out.println("getinputdata failed");
         return false;
      }
     System.out.println("dealData BEGIN");
    if (!dealData())
       {  
    	  System.out.println("dealData failed");
              // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UnLockBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败UnLockBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
          }
       if (!prepareOutputData())
       {
          return false;
       }
    	PubSubmit tPubSubmit = new PubSubmit();
         if (!tPubSubmit.submitData(mInputData, "UPDATE||MAIN"))
         {
           // @@错误处理
           mErrors.copyAllErrors(tPubSubmit.mErrors);
           CError tError = new CError();
           tError.moduleName = "UnLockBL";
           tError.functionName = "submitData";
           tError.errorMessage = "数据提交失败!";
           mErrors.addOneError(tError);
           return false;
          }
         System.out.println("---UnLockBL END---");
         return true;
   }


   /**
    * 数据操作类业务处理
    * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    */
   private boolean dealData() {
	   System.out.println("dealdate mOperate:"+mOperate);
	   if (mOperate.equals("UNLOCK")) 
	   { MMap tMMap= new MMap();
	   for(int i=1;i<=this.mLJAGetSet.size();i++){
		  System.out.println("777777:");
 		  String ActuGetNo = this.mLJAGetSet.get(i).getActuGetNo();
           System.out.println("888888:"+ActuGetNo);
           String SQL="update ljaget set CanSendBank='0',modifydate=current date,modifytime=current time where actugetno='"+ActuGetNo+"' and othernotype='24' and paymode='4' and CanSendBank = '1'  ";
           tMMap.put(SQL, "UPDATE");
	     }
	     map.add(tMMap);
	  }else if(mOperate.equals("UNLOCKALL")){
		  String sql="select * from ljaget a,lawage b where a.managecom like'"+this.mManagecom+"%' and b.indexcalno='"+this.mIndexcalno+"' and a.managecom=b.managecom and a.agentcode=b.agentcode and b.indexcalno=substr(a.otherno,length(a.otherno)-5,6) ";
	        sql+=" and a.othernotype='24' and b.branchtype='1' and b.branchtype2='01' and a.confdate is null and a.paymode='4' and a.CanSendBank = '1'";
	      if(!this.mAgentcode.equals("")&& this.mAgentcode!=null ){
	        	sql =sql +" and a.agentcode='"+this.mAgentcode+"'" ;
	       }
	        sql+=" with ur";
	    	  System.out.println(sql);
	    	  tLJAGetSet=tLJAGetDB.executeQuery(sql);
	  for(int j=1;j<=tLJAGetSet.size();j++){  	
		  LJAGetSchema t_LJAGetSchema=new LJAGetSchema();
		  t_LJAGetSchema=tLJAGetSet.get(j).getSchema();
		  t_LJAGetSchema.setCanSendBank("0");
		  t_LJAGetSchema.setModifyDate(CurrentDate);
		  t_LJAGetSchema.setModifyTime(CurrentTime);
		  this.mLJAGetSet.add(t_LJAGetSchema); 
	  }
	  map.put(mLJAGetSet, "UPDATE");
	  }
     return true;
 }
   /**
    * 从输入数据中得到所有对象
    *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    */
   private boolean getInputData(VData cInputData) 
   {
	   System.out.println("getinputdata");
       mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
       System.out.println("9999933333");
       if(mOperate.equals("UNLOCK"))
       {
    	 this.mLJAGetSet.set( (LJAGetSet) cInputData.getObjectByObjectName("LJAGetSet",0));
    	  System.out.println("LJAGetSet get:"+this.mLJAGetSet.size());
        }
       else
       {
    	System.out.println("99999444444:");
       this.mManagecom=(String)cInputData.get(0);
       System.out.println("99999:"+this.mManagecom);
       this.mIndexcalno=(String)cInputData.get(2);
       System.out.println("11111:"+this.mIndexcalno);
    	this.mAgentcode=(String)cInputData.get(3);
           System.out.println("22222:"+this.mAgentcode);
       System.out.println("------------");
       }
       System.out.println("/////////:");
       return true;
   }

   /**
    *准备需要保存的数据
    **/
   private boolean prepareOutputData() {
     mInputData.clear();
     mInputData.add(map);
     return true;
   }
}
