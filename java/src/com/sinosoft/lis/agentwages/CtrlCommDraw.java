package com.sinosoft.lis.agentwages;

import java.sql.Connection;

import com.sinosoft.lis.db.LATreeAccessoryDB;
import com.sinosoft.lis.db.LATreeBDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LATreeAccessorySchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 判断是否继续抽佣</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author   : zsj
 * @version 1.0
 */

public class CtrlCommDraw
{
    //错误处理类
    public static CErrors mErrors = new CErrors();
    //业务处理相关变量
    /** 全局数据 */
    private VData mInputData = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private String mAgentCode = "";
    private String mManageCom = "";
    private String mBranchType = "";
    private String mIndexCalNo = "";
    private String[] MonDate = new String[2];

    public CtrlCommDraw()
    {
    }

    public static void main(String[] args)
    {
        CtrlCommDraw ctrlCommDraw1 = new CtrlCommDraw();
        VData cInputData = new VData();
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "8611";
        tGI.ComCode = "8611";
        tGI.Operator = "aa";
        String tAgentCode = "8611000893";
        String tIndexCalNo = "200602";

        cInputData.clear();
        cInputData.add(tGI);
        cInputData.add(0, tGI.ManageCom);
        cInputData.add(1, "1");
        cInputData.add(2, tIndexCalNo);

        if (!ctrlCommDraw1.judge(cInputData))
        {
            System.out.println("---failed!-----");
        }
        else
        {
            System.out.println("----Succ!---");
        }
    }

    public boolean judge(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }
        String tSql =
                "select ComCode from ldcom where length(trim(ComCode))=8 "
                + "and comcode like '" + this.mManageCom.trim()
                + "%' order by comcode";
        ExeSQL tExe = new ExeSQL();
        SSRS ssrs = new SSRS();
        ssrs = tExe.execSQL(tSql);
        int comCount = ssrs.getMaxRow();

        for (int j = 1; j <= comCount; j++)
        {
            String tManageCom = ssrs.GetText(j, 1);

            String sql = "select a.* from latree a,laagent b where "
//          + " a.agentcode='8611000165'and "
                         + " a.agentcode = b.agentcode and a.managecom = '"
                         + tManageCom.trim() + "' and b.branchtype = '"
                         + this.mBranchType.trim() + "' and exists ("
                         +
                         "select 'X' from latreeaccessory where rearagentcode = a.agentcode"
                         + " and managecom = '"
                         + tManageCom.trim() +
                         "' and ( commbreakflag is null or commbreakflag<>'1'))";
            System.out.println("---sql = " + sql);
            LATreeSet tSet = new LATreeSet();
            LATreeDB tDB = new LATreeDB();
            tSet = tDB.executeQuery(sql);
            int tCount = 0;
            tCount = tSet.size();
            if (tCount == 0)
            {
                return true;
            }

            LATreeSchema tSch = new LATreeSchema();
            for (int i = 1; i <= tCount; i++)
            {
                tSch.setSchema(tSet.get(i));

                this.mAgentCode = tSch.getAgentCode().trim();
                System.out.println("********************");
                System.out.println(this.mAgentCode);
                System.out.println(tSch.getAgentSeries());
                System.out.println("********************");
                LATreeDB tLATreeDB = new LATreeDB();
                tLATreeDB.setAgentCode(this.mAgentCode);
                if (!tLATreeDB.getInfo())
                {
                    // @@错误处理
                    dealError("getInputData", "查询代理人行政信息表失败！");
                    return false;
                }
                this.mLATreeSchema = tLATreeDB.getSchema();

                if (!preJudge())
                {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean preJudge()
    {
        System.out.println("-----------preJudge-------------");

        //取间接育成人
        String ISql = "select a.* from LATreeAccessory a "
                      + "where a.CommBreakFlag ='0' and a.RearAgentCode in "
                      + "(select distinct AgentCode from latreeaccessory "
                      + "where RearAgentCode = '"
                      + this.mAgentCode.trim()
                      + "') order by agentcode,agentgrade";

        //取直接育成人
        String DSql = "select * from latreeaccessory "
                      + "where commbreakflag = '0' and rearagentcode = '"
                      + this.mAgentCode.trim()
//        + "" + "8611000165" + ""
//        + "' and agentcode='8611000238 "
                      + "' order by agentcode,agentgrade";

        LATreeAccessoryDB tDB = new LATreeAccessoryDB();
        LATreeAccessorySet DSet = new LATreeAccessorySet();
        LATreeAccessorySet ISet = new LATreeAccessorySet();
        DSet = tDB.executeQuery(DSql);
        int DCount = DSet.size();

        ISet = tDB.executeQuery(ISql);
        int ICount = ISet.size();

        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            dealError("dealEach", "数据库连接失败！");
            return false;
        }

        try
        {
            conn.setAutoCommit(false);
            if (DCount != 0)
            {
                System.out.println("-----------一代育成count = " + DCount);
                if (!doJudge(DSet, "D"))
                {
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            if (ICount != 0)
            {
                System.out.println("-----------二代育成count = " + ICount);
                if (!doJudge(ISet, "I"))
                {
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception ep)
            {
                ep.printStackTrace();
                dealError("prejudge", ep.toString());
                return false;
            }
            dealError("prejudge", ex.toString());
            return false;
        }
        return true;
    }

    //tFlag='D'--直接育成、tFlag='I'--间接育成
    private boolean doJudge(LATreeAccessorySet tSet, String tFlag)
    {
        String tGetSeries = "";
        String tSeries = this.mLATreeSchema.getAgentSeries();
        String updFlag = ""; // Y--大于12个月、N--小于

        int tCount = tSet.size();
        System.out.println("-----------doJudge-----count = " + tCount);

        for (int i = 1; i <= tCount; i++)
        {
            LATreeAccessorySchema tSch = new LATreeAccessorySchema();
            tSch.setSchema(tSet.get(i));

            LATreeSchema tS = new LATreeSchema();
            LATreeDB tD = new LATreeDB();
            tD.setAgentCode(tSch.getAgentCode());
            if (!tD.getInfo())
            {
                dealError("judge", "取被育成代理人行政信息失败!");
                return false;
            }
            tS.setSchema(tD.getSchema());

            tGetSeries = tS.getAgentSeries();
            System.out.println("---tGetSeries = " + tGetSeries);

            //若育成人的当前职级大于被育成人的当前职级则无需判断是否断裂
            if (tGetSeries.trim().compareTo(tSeries.trim()) <= 0)
            {
                continue;
            }

            // 判断组育成津贴的抽佣关系
            if (tSch.getAgentGrade().trim().equalsIgnoreCase("A04") ||
                tSch.getAgentGrade().trim().equalsIgnoreCase("A05"))
            {
                // A、B均为高级经理以上职级抽取组育成津贴不受限制
                if ((tGetSeries.trim().compareToIgnoreCase("C") >= 0
                     && tSeries.trim().compareToIgnoreCase("C") >= 0)
                    || tGetSeries.compareToIgnoreCase(tSeries) <= 0)
                {
                    continue;
                }
                else
                {
                    updFlag = checkMonth(tS, tSch.getAgentGrade());
                    if (updFlag == "Y")
                    {
                        if (!updAccessory(tSch, tFlag))
                        {
                            return false;
                        }
                        return true;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            // 判断增部津贴的抽佣关系
            if (tSch.getAgentGrade().trim().equalsIgnoreCase("A06") ||
                tSch.getAgentGrade().trim().equalsIgnoreCase("A07"))
            {
                // 督导长以上职级不受限制
                if ((tGetSeries.equalsIgnoreCase("D") &&
                     tSeries.equalsIgnoreCase("D"))
                    || tGetSeries.compareToIgnoreCase(tSeries) <= 0)
                {
                    continue;
                }
                else
                {
                    updFlag = checkMonth(tS, tSch.getAgentGrade());
                    if (updFlag == "Y")
                    {
                        if (!updAccessory(tSch, tFlag))
                        {
                            return false;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            System.out.println("****************************");
        }
        return true;
    }

    private String checkMonth(LATreeSchema tSch, String rearedGrade)
    {
        String updFlag = "";
        updFlag = this.getFirstDiffDate(tSch);
        System.out.println("updFlag:" + updFlag);
        return updFlag;
    }

    /**
     * 查询育成人mLATreeSchema和被育成人tSch之间，最后一次育成人的系列比被育成人的系列
     * 低的日期 cg add  2004-05-14
     * @param tSch
     * @return
     */
    private String getFirstDiffDate(LATreeSchema tSch)
    {

        char tSeries = mLATreeSchema.getAgentSeries().charAt(0);
        char tRearedSeries = (char) ((int) tSeries + 1);
        String agentGradeSQL = "";
        if (tRearedSeries == 'C')
        {
            agentGradeSQL = " and (agentgrade='A06' or agentgrade='A07' )";
        }
        if (tRearedSeries == 'D')
        {
            agentGradeSQL = " and (agentgrade='A08' or agentgrade='A09' )";
        }
        LATreeAccessoryDB tLATreeAccessoryDB = new LATreeAccessoryDB();
        String tSQL = "select * from latreeaccessory where agentcode='" +
                      tSch.getAgentCode() + "'"
                      + agentGradeSQL;
        System.out.println(tSQL);
        LATreeAccessorySet tLATreeAccessorySet = new LATreeAccessorySet();
        tLATreeAccessorySet = tLATreeAccessoryDB.executeQuery(tSQL);
        LATreeAccessorySchema tLATreeAccessorySchema = new
                LATreeAccessorySchema();
        tLATreeAccessorySchema = tLATreeAccessorySet.get(1);
        String rearedDate = tLATreeAccessorySchema.getstartdate();
        System.out.println("rearedDate:" + rearedDate);

        String queryEdorSQL =
                "select nvl(max(edorno),'0000') from latreeb where makedate>='"
                + rearedDate + "'"
                + "and agentcode='" + mLATreeSchema.getAgentCode() +
                "' and agentseries>='" + tRearedSeries +
                "' and (removetype='01' or removetype='06' )";
        ExeSQL tExeSQL = new ExeSQL();
        String edorNo = tExeSQL.getOneValue(queryEdorSQL);
        System.out.println(edorNo);
        String queryLATreeBSQL = "";
        if (!edorNo.equals("0000"))
        {
            queryLATreeBSQL = "select * from latreeB where startdate>='"
                              + rearedDate + "'"
                              + "and agentcode='" + mLATreeSchema.getAgentCode() +
                              "' and agentseries<'" + tRearedSeries +
                              "' and (removetype='01' or removetype='06' ) ";
            queryLATreeBSQL = queryLATreeBSQL + " and edorno>'" + edorNo +
                              "' order by edorno";

            LATreeBDB tLATreeBDB = new LATreeBDB();
            LATreeBSet tLATreeBSet = new LATreeBSet();
            LATreeBSchema tLATreeBSchema = new LATreeBSchema();
            System.out.println(queryLATreeBSQL);
            tLATreeBSet = tLATreeBDB.executeQuery(queryLATreeBSQL);
            String diffDate = "";
            int tInterval = -1;

            tLATreeBSchema = tLATreeBSet.get(1);
            diffDate = tLATreeBSchema.getStartDate();
            System.out.println("diffdate1:" + diffDate);
            tInterval = PubFun.calInterval(diffDate, this.MonDate[0], "M");
            System.out.println("---tInterval = " + tInterval);
            if (tInterval > 12)
            {
                return "Y";
            }
            else
            {
                return "N";
            }
        }
        //如果从被育成人的系列高于育成人的系列之后，育成人的行政信息没有发生改变，
        //则从被育成人的系列高于育成人的系列开始计算12个月
        else
        {
            String diffDate = rearedDate;
            System.out.println("diffdate2:" + diffDate);
            int tInterval = PubFun.calInterval(diffDate, this.MonDate[0], "M");
            System.out.println("---tInterval = " + tInterval);
            if (tInterval > 12)
            {
                return "Y";
            }
            else
            {
                return "N";
            }
        }
    }

    //tFlag='D'--直接育成、tFlag='I'--间接育成
    private boolean updAccessory(LATreeAccessorySchema tSch, String tFlag)
    {
        PubFun tPF = new PubFun();
        LATreeAccessoryDB tDB = new LATreeAccessoryDB();

        if (tFlag.trim().equals("D"))
        {
            tSch.setCommBreakFlag("1");
        }
        else
        {
            tSch.setIntroBreakFlag("1");
        }

        tSch.setenddate(tPF.getCurrentDate());
        tSch.setModifyDate(tPF.getCurrentDate());
        tSch.setModifyTime(tPF.getCurrentTime());
        tSch.setOperator(this.mGlobalInput.Operator);

        tDB.setSchema(tSch);
        if (!tDB.update())
        {
            dealError("updAccessory", "刷新LATreeAccessory失败！");
            return false;
        }
        System.out.println("-----updAccessory---" + tFlag + "---" +
                           tSch.getAgentCode());
        return true;
    }

    public boolean getInputData(VData cInputData)
    {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        this.mManageCom = (String) cInputData.getObjectByObjectName("String", 0);
        this.mBranchType = (String) cInputData.getObjectByObjectName("String",
                1);
        this.mIndexCalNo = (String) cInputData.getObjectByObjectName("String",
                2);

        if (this.mGlobalInput == null || this.mManageCom == null
            || this.mBranchType == null || this.mIndexCalNo == null)
        {
            // @@错误处理
            dealError("getInputData", "没有得到足够的信息！");
            return false;
        }

        this.MonDate = genDate();
        return true;
    }

    private String[] genDate()
    {
        String[] tMonDate = new String[2];
        PubFun tPF = new PubFun();
        String tYear = this.mIndexCalNo.trim().substring(0, 4);
        String tMon = this.mIndexCalNo.trim().substring(4, 6);

        //计算旧职级的止聘日期
        String tOldDate = tYear + "-" + tMon + "-01";
        tMonDate = tPF.calFLDate(tOldDate);
        String tOldDay = tMonDate[1];
        System.out.println("---tOldDay---" + tOldDay);
        //计算新职级的起聘日期或转正日期
        if (tMon.equals("12"))
        {
            tYear = Integer.toString(Integer.parseInt(tYear) + 1);
            tMon = "01";
        }
        else
        {
            tMon = Integer.toString(Integer.parseInt(tMon) + 1);
        }
        int tLength = tMon.length();
        if (tLength == 1)
        {
            tMon = "0" + tMon;
        }
        String tDate = tYear.trim() + "-" + tMon.trim() + "-" + "01";
        System.out.println("---tNewDay---" + tDate);

        tMonDate[0] = tDate;
        tMonDate[1] = tOldDay;
        return tMonDate;
    }

    private static void dealError(String FuncName, String ErrMsg)
    {
        CError tError = new CError();

        tError.moduleName = "CtrlCommDraw";
        tError.functionName = FuncName;
        tError.errorMessage = ErrMsg;

        mErrors.addOneError(tError);
    }

}
