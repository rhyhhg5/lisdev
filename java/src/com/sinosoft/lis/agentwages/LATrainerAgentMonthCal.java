/**
 * Copyright (c) 2018 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agentwages;

import java.sql.Connection;

import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATrainerAssessIndexDB;
import com.sinosoft.lis.db.LATrainerWageIndexDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LATrainerWageIndexSchema;
import com.sinosoft.lis.vschema.LATrainerAssessIndexSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

import java.text.DecimalFormat;

/*
 * <p>Title: 组训营业部指标计算 </p>
 * <p>Description: 指标计算的流程控制类 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft</p>
 * @author 王清民
 * @version 1.0
 * @date 2018-05-22
 */
public class LATrainerAgentMonthCal {
	// 全局变量
	public CErrors mErrors = new CErrors(); // 错误处理类
	public GlobalInput mGlobalInput = new GlobalInput(); // 保存传入参数

	// 必须初始化的行政信息
	private String mManageCom; // 管理机构
	private String mYearMonth;
	private String mBranchtype;
	private String mBranchType2;
	// 操作人员
	private String Operator;
	private String FRateFORMATMODOL = "0.00"; // 浮动费率计算出来后的精确位数
	private DecimalFormat mFRDecimalFormat = new DecimalFormat(FRateFORMATMODOL); // 数字转换对象

	// 计算需要的计算信息
	private String WageNo; // 指标年月
	private LATrainerAssessIndexSet tLATrainerAssessIndexSet = new LATrainerAssessIndexSet();
	private LATrainerAssessIndexDB tLATrainerAssessIndexDB = new LATrainerAssessIndexDB();
	private LATrainerWageIndexSchema mLATrainerWageIndexSchema = new LATrainerWageIndexSchema();
	private String mIColName = "";
	private Connection conn;

	public LATrainerAgentMonthCal() {
	}

	// 提交函数,参数入口
	public boolean submitData(VData cInputData, String cOperate) {
		this.getInputData(cInputData); // 初始化指标参数

		if (!Examine()) { // 主函数
			if (mErrors.needDealError()) {
				System.out.println("计算异常结束原因：" + mErrors.getFirstError());
			}
			return false;
		}

		return true;
	}

	private void getInputData(VData cInputData) {
		// 信息收集
		try {
			TransferData tTransferData = (TransferData) cInputData
					.getObjectByObjectName("TransferData", 0);

			this.mManageCom = (String) tTransferData
					.getValueByName("ManageCom");
			this.mYearMonth = (String) tTransferData.getValueByName("WageNo");
			this.mGlobalInput.setSchema((GlobalInput) tTransferData
					.getValueByName("GlobalInput"));
			this.mBranchtype = (String) tTransferData
					.getValueByName("BranchType");
			this.mBranchType2 = (String) tTransferData
					.getValueByName("BranchType2");
			this.Operator = mGlobalInput.Operator;
			this.WageNo = mYearMonth;
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}

	// 指标计算主函数
	private boolean Examine() {
		// 1.计算营业部查询

		String tSql = "select agentgroup from labranchgroup where ManageCom = '"
				+ mManageCom
				+ "' and BranchLevel='03' and endflag='N' "
				+ " and BranchType = '"
				+ mBranchtype
				+ "' "
				+ " and BranchType2 ='" + mBranchType2 + "' ";
		// 个险算薪资时 入司日期小于薪资月末日 离职日期为空或者离职日期大于薪资月首日 特殊人群不算薪资
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(tSql);
		System.out.println("共有：" + tSSRS.getMaxRow() + "条数据");

		// 获取要计算的基础指标LATrainerAssessIndex 中 indextype=‘00’ 薪资计算提取营业部指标
		System.out.println("开始计算组训营业部薪资指标了 -------------------------000000^-^");
		tLATrainerAssessIndexDB.setBranchType(mBranchtype);
		tLATrainerAssessIndexDB.setBranchType2(mBranchType2);
		tLATrainerAssessIndexDB.setIndexType("00");
		tLATrainerAssessIndexSet = tLATrainerAssessIndexDB.query();
		// 2.对每一个营业部做计算
		// 循环对每一个营业部进行薪资计算操作！！！
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			LABranchGroupDB tLaBranchGroupDB = new LABranchGroupDB();
			String agentGroup = tSSRS.GetText(i, 1);
			tLaBranchGroupDB.setAgentGroup(agentGroup);
			tLaBranchGroupDB.getInfo();
			String currentDate = PubFun.getCurrentDate();
			String currentTime = PubFun.getCurrentTime();
			mLATrainerWageIndexSchema = new LATrainerWageIndexSchema();
			mLATrainerWageIndexSchema.setManageCom(mManageCom);
			mLATrainerWageIndexSchema.setWageNo(WageNo);
			mLATrainerWageIndexSchema.setIndexType("00");
			mLATrainerWageIndexSchema.setOperator(Operator);
			mLATrainerWageIndexSchema.setAgentGroup(agentGroup);
			mLATrainerWageIndexSchema.setBranchAttr(tLaBranchGroupDB
					.getBranchAttr());

			mLATrainerWageIndexSchema.setMakeDate(currentDate);
			mLATrainerWageIndexSchema.setModifyDate(currentDate);
			mLATrainerWageIndexSchema.setMakeTime(currentTime);
			mLATrainerWageIndexSchema.setModifyTime(currentTime);

			mLATrainerWageIndexSchema.setBranchType(mBranchtype);
			mLATrainerWageIndexSchema.setBranchType2(mBranchType2);
			// 9.具体计算
			if (!WageCal(agentGroup)) {
				System.out.println("Error:" + this.mErrors.getFirstError());
				return false;
			}
			conn = DBConnPool.getConnection();
			if (this.conn == null) {
				CError tError = new CError();
				tError.moduleName = "LATrainerAgentMonthCal";
				tError.functionName = "Examine";
				tError.errorMessage = "获取数据库连结失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
			try {
				conn.setAutoCommit(false);
				// ====将计算出来的结果 存储到LATrainerWageIndex表里

				LATrainerWageIndexDB tLATrainerWageIndexDB = new LATrainerWageIndexDB(
						conn);
				tLATrainerWageIndexDB.setSchema(mLATrainerWageIndexSchema);
				ExeSQL tExe1 = new ExeSQL();

				String tSql2 = "select idx from LATrainerWageIndex where managecom='"
						+ mManageCom
						+ "' and agentgroup='"
						+ agentGroup
						+ "' and wageno='" + WageNo + "' with ur";
				String strIdx1 = "";
				strIdx1 = tExe1.getOneValue(tSql2);
				// 如果存在，update，否则insert 这里可以知道回退薪资的时候 不需要删除LATrainerWageIndex 表
				if (strIdx1 != null && !strIdx1.equals("")) {
					tLATrainerWageIndexDB.setIdx(strIdx1);
					if (!tLATrainerWageIndexDB.update()) {
						this.mErrors
								.copyAllErrors(tLATrainerWageIndexDB.mErrors);
						CError tError = new CError();
						tError.moduleName = "LATrainerAgentMonthCal";
						tError.functionName = "Examine";
						tError.errorMessage = "更新结果记录失败";
						this.mErrors.addOneError(tError);
						conn.rollback();
						conn.close();
						return false;
					}

				} else {
					ExeSQL tExe = new ExeSQL();
					String tSql1 = "select max(int(idx)) from LATrainerWageIndex order by 1 desc";
					String strIdx = "";
					int tMaxIdx = 0;
					strIdx = tExe.getOneValue(tSql1);
					if (strIdx == null || strIdx.trim().equals("")) {
						tMaxIdx = 0;
					} else {
						tMaxIdx = Integer.parseInt(strIdx);
					}
					tMaxIdx += 1;
					String sMaxIdx = tMaxIdx + "";
					tLATrainerWageIndexDB.setIdx(sMaxIdx);
					if (!tLATrainerWageIndexDB.insert()) {
						this.mErrors
								.copyAllErrors(tLATrainerWageIndexDB.mErrors);
						CError tError = new CError();
						tError.moduleName = "LATrainerAgentMonthCal";
						tError.functionName = "Examine";
						tError.errorMessage = "更新结果记录失败";
						this.mErrors.addOneError(tError);
						conn.rollback();
						conn.close();
						return false;
					}
				}
				conn.commit();
				conn.close();
			}

			catch (Exception e) {
				e.printStackTrace();
				try {
					this.conn.rollback();
					this.conn.close();
				} catch (Exception ep) {
					ep.printStackTrace();
					CError tError = new CError();
					tError.moduleName = "LATrainerAgentMonthCal";
					tError.functionName = "Examine";
					tError.errorMessage = "插入LATrainerWageIndex表的记录失败!";
					this.mErrors.addOneError(tError);
					return false;
				}

			}
			System.out.println("***************组训营业部agentGroup:"+agentGroup+"基础指标计算完成******************");

		}
		    System.out.println("***************组训所有营业部基础指标都已经计算完成******************");
		return true;

	}

	// 10.给基数类赋值
	// , String cTrainerCode,String cGrade)

	private boolean insertAssess(String cIndexValue) {
		// ====将数值变成double型的
		double tValue = Double.parseDouble(cIndexValue);
		// 大于1千万，java会自动转化为科学技术法，需要作特殊处理
		if (tValue >= 10000000) {

			String tResult = mFRDecimalFormat.format(tValue);
			System.out.println("|||||||||||||||||||||||||||||||||||||||"
					+ tResult);
			mLATrainerWageIndexSchema.setV(mIColName, tResult);
		} else {// 小于1千万，有可能是 int型，不能转化成浮点型数据（其实大于1000万也可能是int型，应该处理）
			mLATrainerWageIndexSchema.setV(mIColName, cIndexValue);
		}
		return true;
	}

	private boolean WageCal(String agentGroup) {
		String tReturn = "";
		Calculator tCal = new Calculator();
		tCal.addBasicFactor("managecom", mManageCom);
		tCal.addBasicFactor("wageno", WageNo);
		tCal.addBasicFactor("agentgroup", agentGroup);
		tCal.addBasicFactor("branchtype", mBranchtype);
		tCal.addBasicFactor("branchtype2", mBranchType2);
		for (int i = 1; i <= tLATrainerAssessIndexSet.size(); i++) {
			// ====核心计算类 （这个需要好好看看）
			tCal.setCalCode(tLATrainerAssessIndexSet.get(i).getCalCode());
			// ====循环开始计算各项指标信息
			tReturn = tCal.calculate();
			// ===得到薪资项存储表名
			mIColName = tLATrainerAssessIndexSet.get(i).getIColName();
			// ====得到薪资项存储项名
			if ((tReturn == null) || (tReturn.trim().equals(""))) {
				tReturn = "0";
			}
			if (!insertAssess(tReturn)) {
				System.out.println("插入组训营业部薪资指标表出错！！");
				return false;
			}
		}
		System.out
				.println("---------------------------------------------------------");
		return true;
	}

	public CErrors getErrors() {

		return mErrors;
	}

	public static void main(String[] args) {
	}
}
