package com.sinosoft.lis.agentwages;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.FileWriter;
import java.io.*;

public class AgentWageXLSBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    //private String mtype = null;

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    public AgentWageXLSBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        SSRS tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "AgentWageXLSBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 2][45];
        mToExcel[0][0] = "薪资试算结果，非正式结果";
        mToExcel[0][10] = "薪资试算结果，非正式结果";
        mToExcel[0][20] = "薪资试算结果，非正式结果";
        mToExcel[0][30] = "薪资试算结果，非正式结果";
        mToExcel[1][0] = "营销员代码";
        mToExcel[1][1] = "姓名";
        mToExcel[1][2] = "销售单位";
        mToExcel[1][3] = "管理机构";
        mToExcel[1][4] = "综合拓展佣金";
        mToExcel[1][5] = "首年度佣金";   
        mToExcel[1][6] = "续期佣金";    
        mToExcel[1][7] = "续保佣奖";    
        mToExcel[1][8] = "续期服务奖";   
        mToExcel[1][9] = "孤儿单服务奖金"; 
        mToExcel[1][10] = "绩优达标奖"; 
        mToExcel[1][11] = "健代产佣金";
        mToExcel[1][12] = "健代寿佣金";
        mToExcel[1][13] = "岗位津贴";                                    
        mToExcel[1][14] = "季度奖";                                     
        mToExcel[1][15] = "增员奖";                                     
        mToExcel[1][16] = "晋升奖";                                     
        mToExcel[1][17] = "职务津贴";                                    
        mToExcel[1][18] = "管理津贴";                                    
        mToExcel[1][19] = "直接养成津贴";                                  
        mToExcel[1][20] = "间接养成津贴";                                  
        mToExcel[1][21] = "培训津贴（09筹）";                               
        mToExcel[1][22] = "财务支持费用（09筹）";                             
        mToExcel[1][23] = "新人财补";                                    
        mToExcel[1][24] = "主管财补";                                    
        mToExcel[1][25] = "加款(含财补加款)";                               
        mToExcel[1][26] = "财补加款";                                    
        mToExcel[1][27] = "差勤实扣";                                    
        mToExcel[1][28] = "季度团建补发津贴";                                
        mToExcel[1][29] = "年度团建补发津贴";                                
        mToExcel[1][30] = "推荐津贴";                                    
        mToExcel[1][31] = "税前薪资";                                    
        mToExcel[1][32] = "养老金计提";                                   
        mToExcel[1][33] = "个人所得税";                                   
        mToExcel[1][34] = "个人增值税";                                   
        mToExcel[1][35] = "个人城建税";                                   
        mToExcel[1][36] = "教育费附加税";                                  
        mToExcel[1][37] = "地方教育费附加税";                                
        mToExcel[1][38] = "其它附加税";                                   
        mToExcel[1][39] = "扣款";                                      
        mToExcel[1][40] = "上次佣金余额";                                  
        mToExcel[1][41] = "本期应发佣金指标";                                
        mToExcel[1][42] = "本期实发佣金指标";                                
        mToExcel[1][43] = "薪资版本";                                    
                                                 



        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row+1][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "AgentWageXLSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
       //mtype = (String) tf.getValueByName("Type");
        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "AgentWageXLSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {
        AgentWageXLSBL AgentWageXLSBL = new
            AgentWageXLSBL();
    }
}
