package com.sinosoft.lis.agentwages;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;

import com.sinosoft.lis.db.LAIndexVsCommDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.LAIndexVsCommSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class CalWageBaseUI
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private VData mInputData = new VData();

    public CalWageBaseUI()
    {
    }

    public boolean submitData(VData cInputData)
    {
        mInputData = (VData) cInputData.clone();
        CalWageBaseBL tCalWageBaseBL = new CalWageBaseBL();
        if (!tCalWageBaseBL.submitData(mInputData))
        {
            this.mErrors.copyAllErrors(tCalWageBaseBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "CalWageBaseUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {

        try
        {
            PrintStream out =
                    new PrintStream(
                            new BufferedOutputStream(
                                    new FileOutputStream("CalWageBaseBL.out")));
            System.setOut(out);
            System.out.println("---------------");
            GlobalInput tGlobalInput = new GlobalInput();
            tGlobalInput.ManageCom = "86530000";
            tGlobalInput.Operator = "aa";

            String tAgentCode = "5302000002";
            String tAgentGrade = "D01";

            LAIndexVsCommSet tLAIndexVsCommSet = new LAIndexVsCommSet();
            LAIndexVsCommDB tLAIndexVsCommDB = new LAIndexVsCommDB();
            String tSQL = "select * from laindexvscomm where agentgrade='" +
                          tAgentGrade + "' and wagecode='WP0001'";
            System.out.println("===tSQL===" + tSQL);
            tLAIndexVsCommSet = tLAIndexVsCommDB.executeQuery(tSQL);

            VData tVData = new VData();
            tVData.add(tGlobalInput);
            tVData.addElement(tLAIndexVsCommSet);
            tVData.addElement("200601");
            tVData.add("86530000");
            tVData.addElement(tAgentCode);

            CalWageBaseUI calWageBaseUI1 = new CalWageBaseUI();
            calWageBaseUI1.submitData(tVData);
            if (calWageBaseUI1.mErrors.needDealError())
            {
                System.out.println(calWageBaseUI1.mErrors.getFirstError());
            }
            System.out.println("---------------");
            System.out.println("over");
            out.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
