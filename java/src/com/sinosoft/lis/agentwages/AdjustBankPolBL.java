package com.sinosoft.lis.agentwages;

/*
 * <p>ClassName: LAAssessInputBL </p>
 * <p>Description: LAAssessBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-06-21
 */

import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LACommisionBSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.vschema.LACommisionBSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

public class AdjustBankPolBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LACommisionSchema mLACommisionSchema = new LACommisionSchema();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    //备份数据

    private LACommisionBSet mLACommisionBSet = new LACommisionBSet();
    //private LACommisionBSchema mLACommisionBSchema = new LACommisionBSchema();

    public AdjustBankPolBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustBankPolBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败AdjustBankPolBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start AdjustBankPolBL Submit...");
        AdjustBankPolBLS tAdjustBankPolBLS = new AdjustBankPolBLS();
        tAdjustBankPolBLS.submitData(mInputData, cOperate);
        System.out.println("End AdjustBankPolBL Submit...");
        //如果有需要处理的错误，则返回
        if (tAdjustBankPolBLS.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tAdjustBankPolBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "AdjustBankPolBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        LACommisionDB tLACommisionDB = new LACommisionDB();
        tLACommisionDB.setSchema(this.mLACommisionSchema);
        System.out.println("tLACommisionDB agentcode" +
                           tLACommisionDB.getAgentCode());
        LACommisionSet tLACommisionSet = new LACommisionSet();
        tLACommisionSet = tLACommisionDB.query();
        //置基础数据
        for (int i = 1; i <= tLACommisionSet.size(); i++)
        {
            String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            LACommisionBSchema aLACommisionBSchema = new LACommisionBSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(aLACommisionBSchema,
                                     tLACommisionSet.get(i).getSchema());
            aLACommisionBSchema.setEdorNo(tEdorNo);
            aLACommisionBSchema.setEdorType("00"); //待定
            aLACommisionBSchema.setMakeDate(currentDate);
            aLACommisionBSchema.setMakeTime(currentTime);
            aLACommisionBSchema.setModifyDate(currentDate);
            aLACommisionBSchema.setModifyTime(currentTime);
            aLACommisionBSchema.setOperator(this.mGlobalInput.Operator);
            this.mLACommisionBSet.add(aLACommisionBSchema);
        }

//    System.out.println("tLACommisionSet size"+tLACommisionSet.size());
//    Reflections tReflections = new Reflections();
//    tReflections.transFields(tLACommisionSet,this.mLACommisionBSet);
//    System.out.println("mLACommisionBSet size"+mLACommisionBSet.size());
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mLACommisionSet.set((LACommisionSet) cInputData.
                                 getObjectByObjectName("LACommisionSet", 0));
        //备份信息
        this.mLACommisionSchema.setSchema((LACommisionSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LACommisionSchema", 0));

        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLACommisionSet);
            this.mInputData.add(this.mLACommisionBSet);
            this.mInputData.add(this.mLACommisionSchema);
            System.out.println("size:" + this.mLACommisionSet.size());
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessInputBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
