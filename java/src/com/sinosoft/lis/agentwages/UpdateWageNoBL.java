package com.sinosoft.lis.agentwages;

/*
 * <p>ClassName: UpdateWageNoBL </p>
 * <p>Description: LAAssessBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2016-11-24
 */

import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LATEMPMISIONDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LATEMPMISIONSchema;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LATEMPMISIONSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class UpdateWageNoBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private LATEMPMISIONDB mLATEMPMISIONDB=new LATEMPMISIONDB();
    private LATEMPMISIONSet mLATEMPMISIONSet=new LATEMPMISIONSet();
    /** 数据操作字符串 */
    private String mOperate;

    public UpdateWageNoBL()
    {
    }

    public static void main(String[] args)
    {
//    	String tbranchseries="000000041300:000000046876:000000041312";
//    	String tdepartment=tbranchseries.substring(0, 12);
//		String tsection=tbranchseries.substring(13, 25);
//		String tbranch=tbranchseries.substring(26, 38);
//		System.out.println("qqqqq:"+tdepartment);
//		System.out.println("wwwww:"+tsection);
//		System.out.println("rrrrr:"+tbranch);
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UpdateWageNoBL";
            tError.functionName = "DealData";
            tError.errorMessage = "数据处理失败UpdateWageNoBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
        	PubSubmit tPubSubmit = new PubSubmit();
        	tPubSubmit.submitData(this.mInputData, "UPDATE");
			
		} catch (Exception e) {
			CError tError = new CError();
            tError.moduleName = "UpdateWageNoBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败UpdateWageNoBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
		}
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	MMap tMMap= new MMap();
        //置基础数据
        for (int i = 1; i <= this.mLACommisionSet.size(); i++)
        {
        	String tCommisionSN = mLACommisionSet.get(i).getCommisionSN();
        	String tNewWageNo = mLACommisionSet.get(i).getWageNo();
        	String tUpSQL ="";
        	if("".equals(tNewWageNo)||"null".equals(tNewWageNo))
        	{
        		tUpSQL = "update lacommision set wageno ='',caldate =null,"
        				+ "operator ='WageNo',modifydate = current date,modifytime = current time "
        				+ " where commisionsn ='"+tCommisionSN+"'";
        		String UpSQL1="delete from  latempmision where commisionsn='"+tCommisionSN+"'";
    			tMMap.put(UpSQL1, "UPDATE");
        	}
        	else
        	{
        		String tCalDate = tNewWageNo.substring(0, 4)+"-"+tNewWageNo.substring(4, 6)+"-01";
        		tUpSQL = "update lacommision set wageno ='"+tNewWageNo+"',caldate =  last_day(date('"+tCalDate+"')),"
    				+ "operator ='WageNo',modifydate = current date,modifytime = current time "
    				+ " where commisionsn ='"+tCommisionSN+"'";
        		String sql="select * from latempmision where commisionsn='"+tCommisionSN+"'";
        		mLATEMPMISIONSet=mLATEMPMISIONDB.executeQuery(sql);
        		String SQL="select tmakedate,branchseries from lacommision where commisionsn='"+tCommisionSN+"'";
        		SSRS tSSRS = new SSRS();
        	    ExeSQL tExeSQL = new ExeSQL();
        	    tSSRS = tExeSQL.execSQL(SQL);
        		String tmakedate="";
        		String tbranchseries="";
        		for(int j=1;j<=tSSRS.getMaxRow();j++){
        			tmakedate=tSSRS.GetText(1, 1);
        			tbranchseries=tSSRS.GetText(1, 2);
        		}
        		String tdepartment=tbranchseries.substring(0, 12);
        		String tsection=tbranchseries.substring(13, 25);
        		String tbranch=tbranchseries.substring(26, 38);
        		System.out.println("qqqqq:"+tdepartment);
        		System.out.println("wwwww:"+tsection);
        		System.out.println("rrrrr:"+tbranch);
        		System.out.println("eeeee:"+tCommisionSN);
        		if(mLATEMPMISIONSet.size()<=0){
        			LATEMPMISIONSchema tLATEMPMISIONSchema=new LATEMPMISIONSchema();
        			tLATEMPMISIONSchema.setCommisionSN(tCommisionSN);
        			tLATEMPMISIONSchema.setBranch(tbranch);
        			tLATEMPMISIONSchema.setDepartment(tdepartment);
        			tLATEMPMISIONSchema.setSection(tsection);
        			tLATEMPMISIONSchema.setTMakeDate(tmakedate);
        			tLATEMPMISIONSchema.setWageNo(tNewWageNo);
        			tMMap.put(tLATEMPMISIONSchema,"INSERT");
        		}else{
        			String UpSQL="update latempmision set wageno='"+tNewWageNo+"' where commisionsn='"+tCommisionSN+"'";
        			tMMap.put(UpSQL, "UPDATE");
        		}
        	}
        	tMMap.put(tUpSQL, "UPDATE");
        }
        this.mInputData.add(tMMap);
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mLACommisionSet.set((LACommisionSet) cInputData.
                                 getObjectByObjectName("LACommisionSet", 0));
        return true;
    }
}
