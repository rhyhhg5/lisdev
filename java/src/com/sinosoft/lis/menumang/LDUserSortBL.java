package com.sinosoft.lis.menumang;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author St.GN
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Vector;
import java.util.*;
import java.text.DecimalFormat;
import com.sinosoft.lis.f1print.*;
import java.lang.*;

public class LDUserSortBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    private TransferData rTransferData = new TransferData();

    public LDUserSortBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("come to BL............."+cOperate);
        if (cOperate.equals("SORT"))
        {
            if (!getInputData(cInputData)) {
                return false;
            }
            mResult.clear();
            //
            if (!getSortData()) {
                return false;
            }
            if(!getAdminSortData()){
                return false;
            }
        }
        else if (cOperate.equals("ALLUNDER"))
        {
            if (!getInputData(cInputData)) {
                return false;
            }
            mResult.clear();
            if(!getAllUnderLing())
            {
                return false;
            }
            if(!getAdminUnderLing())
            {
                return false;
            }

        }
        else
        {
            buildError("submitData", "不支持的操作字符串");
            // 得到外部传入的数据，将数据备份到本类中
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        if (mTransferData == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LDUserSortBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    ArrayList nameList=new ArrayList();
    HashMap relaMap=new HashMap();
    ArrayList sortList=new ArrayList();
    HashMap sortMap=new HashMap();

    /**
     * 得到排序的用户名List和用户层级关系map
     */
    private boolean getSortData() {

        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        nameList = (ArrayList) mTransferData.getValueByName("UserName"); //得到所有用户list
        relaMap = (HashMap) mTransferData.getValueByName("UserRela");    //得到用户与上级关系
        String operator = (String) mTransferData.getValueByName("Operator");//得到父结点

        sortMap.put(operator,1+"");//先添加父结点
        sortList.add(operator);

        sortUser(operator); //调用sortUser()
        rTransferData.setNameAndValue("SORTMAP",sortMap);
        rTransferData.setNameAndValue("SORTLIST",sortList);
        mResult.addElement(rTransferData);
        return true;
    }

    int uLevel = 1; //记录用户层级
    String underling =""; //用户名
    String leader = "";   //上级名

    /**
     * 得到排序的用户名List和用户层级关系map
     */
    private void sortUser(String ope)
    {
        Iterator nameIt = nameList.iterator(); //每次递归会从头迭代用户名list
        uLevel++; //每调用一次sortUser()层级加1
        while (nameIt.hasNext())
        {
            underling = (String) nameIt.next(); //得到用户名
            leader = (String) relaMap.get(underling); //得到该用户的上级
            if (leader.equals(ope)) //如果该用户等于参数
            {
                sortList.add(underling); //添加进排序list
                sortMap.put(underling, uLevel + ""); //记录该用户得层级

                sortUser(underling); //对该用户递归操作
            }
        }
        uLevel--; //保证每次递归结束层级减1
    }

    /**
    * 得到用户所有下级用户
    */
    private boolean getAllUnderLing()
    {
        String operator = (String)mTransferData.getValueByName("OpeCode");

        ArrayList allList  = new ArrayList();
        ArrayList tempList = new ArrayList();

        HashMap userMap = new HashMap(); //用于存放用户上下级关系

        HashMap codeNameMap = new HashMap(); //用于存放用户代码姓名关系
        int userLevel=1;
        String strSQL= " select usercode,operator,username from lduser where  usercode = '"+ operator +"'"; //得到操作者本身代码，上级，用户名
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(strSQL);
        int count = tSSRS.getMaxRow();
        String temp1[][] = tSSRS.getAllData();
        for(int i=0;i<temp1.length;i++)
        {
           codeNameMap.put(temp1[i][0],temp1[i][2]); //用户代码名称关系map
        }

        strSQL= " select usercode,operator,username from lduser where  operator = '"+ operator +"'"; //得到下级,用户号,用户名
        tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(strSQL);
        count = tSSRS.getMaxRow();
        String temp[][] = tSSRS.getAllData();

        for(int i=0;i<temp.length;i++)
        {
                allList.add(temp[i][0]); //添加进所有用户list
                tempList.add(temp[i][0]);//添加进临时用户list
                userMap.put(temp[i][0],temp[i][1]); //用户上级关系map
                codeNameMap.put(temp[i][0],temp[i][2]); //用户代码名称关系map
        }
        System.out.println(".....................");
        String opeStr="''";

        while(tempList.size()!=0) //循环得到下级的下级
        {
                userLevel++;
                Iterator it = tempList.iterator();
                while(it.hasNext())
                {
                        opeStr = opeStr +",'" + it.next()+"'";
                }
                tempList.clear();

                strSQL =  " select usercode,operator,username from lduser "
                +" where  operator in ("+ opeStr +") and usercode not in (" + opeStr + ") and usercode<>'" + operator + "'"; //得到全部下级

                tSSRS = tExeSQL.execSQL(strSQL);
                count = tSSRS.getMaxRow();

                String temp2[][] = tSSRS.getAllData();

                for(int i=0;i<temp2.length;i++)
                {
                        allList.add(temp2[i][0]);
                        tempList.add(temp2[i][0]);
                        userMap.put(temp2[i][0],temp2[i][1]);
                        codeNameMap.put(temp2[i][0],temp2[i][2]);
                }
        }
        String userStr = "'"+operator+"'";
        Iterator it = allList.iterator();
        while(it.hasNext())
        {
                userStr = userStr +",'" + it.next()+"'";
        }
        System.out.println("In java :.... "+userStr);

        rTransferData.setNameAndValue("UNDERSTR",userStr);
        rTransferData.setNameAndValue("USERLEVEL",userLevel+"");
        rTransferData.setNameAndValue("ALLLIST",allList);
        rTransferData.setNameAndValue("CODENAMEMAP",codeNameMap);
        rTransferData.setNameAndValue("USERMAP",userMap);
        mResult.addElement(rTransferData);

        return true;
    }

    /**
    * 根据参数得到所有下级用户,可从外部调用
    */
   public String getAllUnderLing(String userRoot)
    {
        ArrayList allList  = new ArrayList();
        ArrayList tempList = new ArrayList();

        //HashMap userMap = new HashMap(); //用于存放用户上下级关系

        //HashMap codeNameMap = new HashMap(); //用于存放用户代码姓名关系

        int userLevel=1;
        String strSQL= " select usercode,operator,username from lduser where  operator = '"+ userRoot +"'"; //得到下级,用户号,用户名
System.out.println("in usersort...........1............");
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(strSQL);
System.out.println("in usersort...........11............"+tSSRS);
        int count = tSSRS.getMaxRow();
        String temp[][] = tSSRS.getAllData();
System.out.println("in usersort...........11............"+count);
        for(int i=0;i<temp.length;i++)
        {
                allList.add(temp[i][0]); //添加进所有用户list
                tempList.add(temp[i][0]);//添加进临时用户list
                //userMap.put(temp[i][0],temp[i][1]); //用户上级关系map
                //codeNameMap.put(temp[i][0],temp[i][2]); //用户代码名称关系map
System.out.println("1级用户：underling: "+temp[i][0] +"  leader: "+temp[i][0]);
        }
System.out.println("in usersort...........2............");
        String opeStr="''";

        while(tempList.size()!=0) //循环得到下级的下级
        {
System.out.println("in usersort while.......................");
                userLevel++;
                Iterator it = tempList.iterator();
                while(it.hasNext())
                {
                        opeStr = opeStr +",'" + it.next()+"'";
                }
                tempList.clear();
System.out.println("allList.clear()后的size:  " + tempList.size());

                strSQL =  " select usercode,operator,username from lduser "
                +" where  operator in ("+ opeStr +") and usercode not in (" + opeStr + ") and usercode<>'" + userRoot + "'";

                tSSRS = tExeSQL.execSQL(strSQL);
                count = tSSRS.getMaxRow();

                String temp1[][] = tSSRS.getAllData();

                for(int i=0;i<temp1.length;i++)
                {
                        allList.add(temp1[i][0]);
                        tempList.add(temp1[i][0]);
                        //userMap.put(temp1[i][0],temp1[i][1]);
                        //codeNameMap.put(temp1[i][0],temp1[i][2]);
                }
                System.out.println("**opeStr: " + opeStr);
        }
System.out.println("in usersort...........3............");
        String userStr = "''";
        Iterator it = allList.iterator();
        while(it.hasNext())
        {
                userStr = userStr +",'" + it.next()+"'";
        }
System.out.println("In java :.... "+userStr);

        return userStr;
    }

    /**
    * 得到所有行政下级用户
    */
   private boolean getAdminUnderLing()
    {
        ArrayList allList  = new ArrayList();
        ArrayList tempList = new ArrayList();

        HashMap userMap = new HashMap(); //用于存放用户上下级关系
        HashMap codeNameMap = new HashMap(); //用于存放用户代码姓名关系
        int userLevel=1;




        String strSQL= " select usercode,operator,username from lduser where  usercode = 'yunyin'"; //得到操作者本身代码，上级，用户名
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(strSQL);
        int count = tSSRS.getMaxRow();
        String temp0[][] = tSSRS.getAllData();
        for(int i=0;i<temp0.length;i++)
        {
           allList.add(temp0[i][0]); //添加进所有用户list
           codeNameMap.put(temp0[i][0],temp0[i][2]); //添加进代码名称关系map
           userMap.put(temp0[i][0],"001"); //用户上级关系map
        }

//得到llclaimuser表中理赔下级用户
        String operator = "yun001";
        strSQL= " select usercode,operator,username from llclaimuser where  usercode = '"+operator+"'"; //得到操作者本身代码，上级，用户名
        tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(strSQL);
        count = tSSRS.getMaxRow();
        String temp1[][] = tSSRS.getAllData();
        for(int i=0;i<temp1.length;i++)
        {
           allList.add(temp1[i][0]); //添加进所有用户list  //
           codeNameMap.put(temp1[i][0],temp1[i][2]); //添加进代码名称关系map
           userMap.put(temp1[i][0],"yunyin"); //用户上级关系map
        }

        strSQL= " select usercode,upusercode,username from llclaimuser where  upusercode = '"+ operator +"'"; //得到下级,用户号,用户名
       tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(strSQL);
       count = tSSRS.getMaxRow();
       String temp[][] = tSSRS.getAllData();

       for(int i=0;i<temp.length;i++)
       {
               allList.add(temp[i][0]); //添加进所有用户list //
               tempList.add(temp[i][0]);//添加进临时用户list
               userMap.put(temp[i][0],temp[i][1]); //用户上级关系map
               codeNameMap.put(temp[i][0],temp[i][2]); //用户代码名称关系map

       }
       String opeStr="''";


        while(tempList.size()!=0) //循环得到下级的下级
        {
                userLevel++;
                Iterator it = tempList.iterator();
                while(it.hasNext())
                {
                        opeStr = opeStr +",'" + it.next()+"'";
                }
                tempList.clear();

                strSQL =  " select usercode,upusercode,username from llclaimuser  "
                +" where  operator in ("+ opeStr +") and usercode not in (" + opeStr + ") and usercode<>'" + operator + "'"; //得到全部下级

                tSSRS = tExeSQL.execSQL(strSQL);
                count = tSSRS.getMaxRow();

                String temp2[][] = tSSRS.getAllData();

                for(int i=0;i<temp2.length;i++)
                {
                        allList.add(temp2[i][0]);
                        tempList.add(temp2[i][0]);
                        userMap.put(temp2[i][0],temp2[i][1]);
                        codeNameMap.put(temp2[i][0],temp2[i][2]);
                }
        }

//得到lduwuser表中得到核保下级用户
        String operator1="UW0001";
        strSQL= " select usercode,operator,username from  lduser where  usercode = '"+operator1+"'"; //得到操作者本身代码，上级，用户名
        tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(strSQL);
        count = tSSRS.getMaxRow();
        String temp2[][] = tSSRS.getAllData();
        for(int i=0;i<temp2.length;i++)
        {
           allList.add(temp2[i][0]); //添加进所有用户list
           codeNameMap.put(temp2[i][0],temp2[i][2]); //添加进代码名称关系map
           userMap.put(temp2[i][0],"yunyin"); //用户上级关系map

        }

        strSQL= " select a.usercode, a.upusercode, b.username from lduwuser a ,lduser b "
                +" where  a.usercode=b.usercode and  a.upusercode = '"+operator1+"' "; //得到下级,用户号,用户名
       tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(strSQL);
       count = tSSRS.getMaxRow();
        String utemp[][] = tSSRS.getAllData();

       for(int i=0;i<utemp.length;i++)
       {
               allList.add(utemp[i][0]); //添加进所有用户list
               tempList.add(utemp[i][0]);//添加进临时用户list
               userMap.put(utemp[i][0],utemp[i][1]); //用户上级关系map
               codeNameMap.put(utemp[i][0],utemp[i][2]); //用户代码名称关系map
       }
       String opeStr1="''";  //拼lduwuser的下级用户的字符串


        while(tempList.size()!=0) //循环得到下级的下级
        {
                userLevel++;
                Iterator it = tempList.iterator();
                while(it.hasNext())
                {
                        opeStr1 = opeStr1 +",'" + it.next()+"'";
                }
                tempList.clear();

                strSQL=" select a.usercode, a.upusercode, b.username from lduwuser a ,lduser b "
                       + " where  a.usercode=b.usercode "
                       + " and a.upusercode in ("+opeStr1+") and a.usercode not in (" + opeStr1 + ") and a.usercode not in("+opeStr+") "
                       + " and a.usercode <>'"+operator1+"'"
                       ;

                tSSRS = tExeSQL.execSQL(strSQL);
                count = tSSRS.getMaxRow();

                String temp3[][] = tSSRS.getAllData();

                for(int i=0;i<temp3.length;i++)
                {
                        allList.add(temp3[i][0]);
                        tempList.add(temp3[i][0]);
                        userMap.put(temp3[i][0],temp3[i][1]);
                        codeNameMap.put(temp3[i][0],temp3[i][2]);
                }
        }

//得到lggroupmember表中得到保全下级用户
        String operator2="pa0001";
        strSQL= " select a.memberno,a.supermanager,b.username from lggroupmember a ,lduser b "
        + " where a.memberno=b.usercode "
        //+ " and (a.memberno like 'pa%' or a.memberno like 'su%' ) "
        + " and a.memberno='"+operator2+"'";
        tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(strSQL);
        count = tSSRS.getMaxRow();
        String temp3[][] = tSSRS.getAllData();
        for(int i=0;i<temp3.length;i++)
        {
           allList.add(temp3[i][0]); //添加进所有用户list
           codeNameMap.put(temp3[i][0],temp3[i][2]); //添加进代码名称关系map
           userMap.put(temp3[i][0],"UW0002"); //用户上级关系map

        }

        strSQL= "select a.memberno, a.supermanager, b.username from lggroupmember a ,lduser b "
        + " where a.memberno=b.usercode "
        //+"and (a.memberno like 'pa%' or a.memberno like 'su%' ) "
        + " and a.supermanager ='"+ operator2 +"' "; //得到下级,用户号,用户名
       tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(strSQL);
       count = tSSRS.getMaxRow();
       String gtemp[][] = tSSRS.getAllData();

       for(int i=0;i<gtemp.length;i++)
       {
               allList.add(gtemp[i][0]); //添加进所有用户list
               tempList.add(gtemp[i][0]);//添加进临时用户list
               userMap.put(gtemp[i][0],gtemp[i][1]); //用户上级关系map
               codeNameMap.put(gtemp[i][0],gtemp[i][2]); //用户代码名称关系map
       }
       String opeStr2="''";  //拼lduwuser的下级用户的字符串

        while(tempList.size()!=0) //循环得到下级的下级
        {
                userLevel++;
                Iterator it = tempList.iterator();
                while(it.hasNext())
                {
                        opeStr2 = opeStr2 +",'" + it.next()+"'";
                }
                tempList.clear();

                strSQL=" select a.memberno, a.supermanager, b.username from lggroupmember a ,lduser b "
                + " where a.memberno=b.usercode "
                //+ " and (a.memberno like 'pa%' or a.memberno like 'su%' ) "
                + " and a.supermanager in ("+opeStr2+") and a.memberno not in (" + opeStr1 + ") and a.memberno not in("+opeStr+") and a.memberno not in ("+opeStr2+")"
                + " and a.memberno <>'"+operator2+"'";

                tSSRS = tExeSQL.execSQL(strSQL);
                count = tSSRS.getMaxRow();

                String temp4[][] = tSSRS.getAllData();

                for(int i=0;i<temp4.length;i++)
                {
                        allList.add(temp4[i][0]);
                        tempList.add(temp4[i][0]);
                        userMap.put(temp4[i][0],temp4[i][1]);
                        codeNameMap.put(temp4[i][0],temp4[i][2]);
                }
        }
//得到业务管理下级用户
        strSQL= " select usercode,operator,username from  lduser where  usercode like 'op%'"; //得到操作者本身代码，上级，用户名
        tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(strSQL);
        count = tSSRS.getMaxRow();
        String temp5[][] = tSSRS.getAllData();
        for(int i=0;i<temp5.length;i++)
        {
             allList.add(temp5[i][0]); //添加进所有用户list
             codeNameMap.put(temp5[i][0],temp5[i][2]); //添加进代码名称关系map
             userMap.put(temp5[i][0],"yunyin"); //用户上级关系map
        }
/*************************************************************/
        String userStr = "'"+operator+"'";
        Iterator it = allList.iterator();
        while(it.hasNext())
        {
                userStr = userStr +",'" + it.next()+"'";
        }
        System.out.println("In java :.... "+userStr);

        rTransferData.setNameAndValue("ADMINUNDERSTR",userStr);
        rTransferData.setNameAndValue("ADMINUSERLEVEL",userLevel+"");
        rTransferData.setNameAndValue("ADMINALLLIST",allList);
        rTransferData.setNameAndValue("ADMINCODENAMEMAP",codeNameMap);
        rTransferData.setNameAndValue("ADMINUSERMAP",userMap);
        mResult.addElement(rTransferData);

        return true;
    }

    ArrayList adminNameList=new ArrayList();
    HashMap adminRelaMap=new HashMap();
    ArrayList adminSortList=new ArrayList();
    HashMap adminSortMap=new HashMap();

    /**
     * 得到排序的行政用户名List和用户层级关系map
     */
    private boolean getAdminSortData() {
        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        adminNameList = (ArrayList) mTransferData.getValueByName("AdminUserName"); //得到所有用户list
        adminRelaMap = (HashMap) mTransferData.getValueByName("AdminUserRela");    //得到用户与上级关系
        //String operator = (String) mTransferData.getValueByName("Operator");//得到父结点
        adminSortMap.put("yunyin",1+"");//先添加父结点
        adminSortList.add("yunyin");
        adminSortUser("yunyin"); //调用sortUser()
        rTransferData.setNameAndValue("ADMINSORTMAP",adminSortMap);
        rTransferData.setNameAndValue("ADMINSORTLIST",adminSortList);
        mResult.addElement(rTransferData);

        return true;
    }

    int adminULevel = 1; //记录用户层级
    String adminUnderling =""; //用户名
    String adminLeader = "";   //上级名

    /**
     * 得到排序的用户名List和用户层级关系map
     */
    private void adminSortUser(String ope)
    {
        Iterator nameIt = adminNameList.iterator(); //每次递归会从头迭代用户名list
        adminULevel++; //每调用一次sortUser()层级加1
        while (nameIt.hasNext())
        {
            adminUnderling = (String) nameIt.next(); //得到用户名
            adminLeader = (String) adminRelaMap.get(adminUnderling); //得到该用户的上级
            if (adminLeader.equals(ope)) //如果该用户等于参数
            {
                adminSortList.add(adminUnderling); //添加进排序list
                adminSortMap.put(adminUnderling, adminULevel + ""); //记录该用户得层级

                adminSortUser(adminUnderling); //对该用户递归操作
            }
        }
        adminULevel--; //保证每次递归结束层级减1
    }







    public static void main(String[] args) {
        LDUserSortBL bL = new LDUserSortBL();
        String underLing=bL.getAllUnderLing("claim");
        System.out.println(underLing);

    }

    private void jbInit() throws Exception {
    }

}
