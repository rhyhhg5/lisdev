/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.menumang;

//import java.sql.Connection;

import com.sinosoft.lis.schema.LDMenuGrpSchema;
import com.sinosoft.lis.schema.LDMenuGrpToMenuSchema;
import com.sinosoft.lis.vschema.LDMenuGrpToMenuSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
//import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 菜单组业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author DingZhong
 * @version 1.0
 */
public class LDMenuGrpBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mResult = new VData();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    private LDMenuGrpToMenuSet mLDMenuGrpToMenuSet = new LDMenuGrpToMenuSet();
    private LDMenuGrpSchema mLDMenuGrpSchema = new LDMenuGrpSchema();
    private String mLDMenuGrpToMenuSetStr = "";
    private String mRemoveSetStr = "";
    private LDMenuGrpToMenuSet mRemoveSet = new LDMenuGrpToMenuSet();
    //业务处理相关变量

    public LDMenuGrpBL()
    {
    }

    public static void main(String[] args)
    {
//        LDMenuGrpToMenuSet removeMenuSet = new LDMenuGrpToMenuSet();
//        LDMenuGrpToMenuSchema removeSchema = new LDMenuGrpToMenuSchema();
//        removeSchema.setMenuGrpCode("G1");
//        removeSchema.setNodeCode("1012");
//        LDMenuGrpToMenuSchema schema2 = new LDMenuGrpToMenuSchema();
//        schema2.setMenuGrpCode("G1");
//        schema2.setNodeCode("1104");
//        removeMenuSet.add(removeSchema);
//        removeMenuSet.add(schema2);
//        LDMenuGrpBL tLDMenuGrpBL = new LDMenuGrpBL();
//
//        Connection conn = DBConnPool.getConnection();
//        if (conn != null)
//        {
//            try
//            {
//                conn.close();
//            }
//            catch (Exception e)
//            {}
//            ;
//        }
        /*  	String str = "12363|1001|^12363|1002|^12363|1003|^12363|1004|^12363|1005|^";
           VData tData = new VData();
           tData.add(str);
           LDMenuGrpSchema tLDMenuGrpSchema = new LDMenuGrpSchema();
           tLDMenuGrpSchema.setMenuGrpCode("12363");
           tData.add(tLDMenuGrpSchema);
           LDMenuGrpBL tmenuGrpBL = new LDMenuGrpBL();
           tmenuGrpBL.submitData(tData,"insert");
           LDMenuGrpToMenuSet tSet = new LDMenuGrpToMenuSet();
         */
        // 	tmenuGrpBL.stringToSet(str,tSet);
        /*   LDMenuGrpBL tLDMenuGrpBL1 = new LDMenuGrpBL();
           VData tv=new VData();
           LDMenuGrpSchema temp = new LDMenuGrpSchema();
           temp.setMenuGrpCode("2008");
           LDMenuGrpToMenuSchema temp2 = new LDMenuGrpToMenuSchema();
           temp2.setMenuGrpCode("2008");
           temp2.setNodeCode("1111");
           LDMenuGrpToMenuSet set = new LDMenuGrpToMenuSet();
           set.add(temp2);
           tv.add(temp);
           tv.add(set);
           tLDMenuGrpBL1.submitData(tv,"update");
           if (tLDMenuGrpBL1.mErrors.getErrorCount() != 0) {
               CError tError = tLDMenuGrpBL1.mErrors.getError(0);
               System.out.println(tError.errorMessage);
           }
         */
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("After getinputdata");

        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        System.out.println("After dealData！");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        System.out.println("After prepareOutputData");

        System.out.println("Start LDMenuGrp BL Submit...");

        LDMenuGrpBLS tLDMenuGrpBLS = new LDMenuGrpBLS();
        boolean tag = tLDMenuGrpBLS.submitData(mInputData, cOperate);
        System.out.println("tag : " + tag);
        if (!tag)
        {
            return false;
        }

        System.out.println("End LDMenuGrp BL Submit...");

        //如果有需要处理的错误，则返回
        if (tLDMenuGrpBLS.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLDMenuGrpBLS.mErrors);
            return false;
        }

        mInputData = null;
        return true;
    }

    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData()
    {
        return true;

    }

    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData)
    {
        System.out.println("start BL get inputdata...");

        mLDMenuGrpSchema = (LDMenuGrpSchema) mInputData.getObjectByObjectName(
                "LDMenuGrpSchema", 0);
        mLDMenuGrpToMenuSet.set((LDMenuGrpToMenuSet) mInputData.
                                getObjectByObjectName("LDMenuGrpToMenuSet", 0));

        if ((mOperate.compareTo("delete") != 0) &&
            (mLDMenuGrpToMenuSet == null || mLDMenuGrpToMenuSet.size() == 0))
        {

            mLDMenuGrpToMenuSetStr = (String) mInputData.getObjectByObjectName(
                    "String", 0);
            mRemoveSetStr = (String) mInputData.getObjectByObjectName("String",
                    1);
            System.out.println("**mRemoveSetStr :" + mRemoveSetStr);
            if (mLDMenuGrpToMenuSetStr == null || "".equals(mLDMenuGrpToMenuSetStr))
            {
            	mErrors.addOneError("请选择菜单组。");
                return false;
            }

            stringToSet(mLDMenuGrpToMenuSetStr, mLDMenuGrpToMenuSet);

            if (mRemoveSetStr != null && mRemoveSetStr != "")
            {
                stringToSet(mRemoveSetStr, mRemoveSet);
                System.out.println("mRemoveSet size is " + mRemoveSet.size());
            }
        }

        System.out.println(mLDMenuGrpToMenuSet.size());

        if (mLDMenuGrpSchema == null ||
            (mOperate != "delete" && mLDMenuGrpToMenuSet == null))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDMenuGrpBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData()
    {
        mInputData = new VData();
        try
        {
            mInputData.add(mLDMenuGrpSchema);
            mInputData.add(mLDMenuGrpToMenuSet);
            mInputData.add(mRemoveSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDMenuGrpBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public void stringToSet(String schemaString, LDMenuGrpToMenuSet stringSet)
    {
        stringSet.clear();

        int serialNo = 1;
        String schemaStr = StrTool.decodeStr(schemaString, "^", serialNo);
        while (schemaStr != "" && serialNo < schemaString.length())
        {
            LDMenuGrpToMenuSchema tSchema = new LDMenuGrpToMenuSchema();

            String menuGrpCode = StrTool.decodeStr(schemaStr, "|", 1);
            String menuCode = StrTool.decodeStr(schemaStr, "|", 2);

            tSchema.setMenuGrpCode(menuGrpCode);
            tSchema.setNodeCode(menuCode);
            stringSet.add(tSchema);

            serialNo++;
            schemaStr = StrTool.decodeStr(schemaString, "^", serialNo);
        }
    }

    /*
      public boolean userToMenuGrp(LDMenuGrpToMenuSet removeMenuSet,String usercode, Connection conn)
      {
          System.out.println("start userToMenuGrp procedure");
          if (conn == null) {
           System.out.println("conn is null");
           return false;
          }

          if (removeMenuSet == null || removeMenuSet.size() == 0)
              return true;

          //得到真正的需增加和删去的菜单节点集合
          //LDMenuSet allMenu =  所有的菜单集合
          //对于每一个addMenuSet中的元素，如果在allMenu中，则删除
          //得到了hisAddMenuSet,hisRemoveMenuSet;
          try {
          LDMenuGrpToMenuSet allMenuSet = new LDMenuGrpToMenuSet();
          LDMenuGrpToMenuDB tMenuGrpToMenuDB = new LDMenuGrpToMenuDB(conn);
     String sqlStr = "select * from ldmenugrptomenu where menugrpcode in " +
     "(select menugrpcode from ldusertomenugrp where usercode = '" +
                          usercode + "')";

          System.out.println(sqlStr);

          allMenuSet = tMenuGrpToMenuDB.executeQuery(sqlStr);

          System.out.println("allMenuSet's size is :" + allMenuSet.size());

          if (allMenuSet == null || allMenuSet.size() == 0) {
//      	conn.close();
           return true;
          }

          LDMenuGrpToMenuSet realRemoveSet = new LDMenuGrpToMenuSet();

          for (int i = 1; i <= removeMenuSet.size(); i++) {
             LDMenuGrpToMenuSchema chooseSchema = removeMenuSet.get(i);
             LDMenuGrpToMenuSchema addSchema = null;
             String userCode1 = chooseSchema.getNodeCode();
              int j = 1;
              for (; j <= allMenuSet.size(); j++) {
               addSchema = allMenuSet.get(j);
               String userCode2 = addSchema.getNodeCode();
               if (userCode1.compareTo(userCode2) != 0)
                   continue;
                   break;
              }
              if ( j > allMenuSet.size()) {
               realRemoveSet.add(chooseSchema);
              }

          }

          if (realRemoveSet.size() == 0) {
             System.out.println("realRemoveSet is empty");
          //  conn.close();
              return true;
          }
     System.out.println("realRemoveSet's size is " + realRemoveSet.size());


          // 得到所有编码为usercode的用户创建的菜单组编码集合
          LDMenuGrpSet tAllCreateGrpSet = new LDMenuGrpSet();
     sqlStr = "select * from LDMenugrp where Operator = '" + usercode + "'";
          LDMenuGrpDB tLDMenuGrpDB = new LDMenuGrpDB(conn);
          tAllCreateGrpSet = tLDMenuGrpDB.executeQuery(sqlStr);
     System.out.println("tAllCreateGrpSet size is " + tAllCreateGrpSet.size());
          if (tAllCreateGrpSet.size() == 0) {
          // conn.close();
           return true;
          }

          //对于每一个菜单组，更新它的菜单节点集合
          for (int ii = 1; ii <= tAllCreateGrpSet.size(); ii++) {
               System.out.println("*********************");
               LDMenuGrpSchema tMenuGrpSchema = tAllCreateGrpSet.get(ii);
               String tMenuGrpCode = tMenuGrpSchema.getMenuGrpCode();
               sqlStr = "select * from ldmenugrptomenu where menugrpcode = '" + tMenuGrpCode + "'";
               LDMenuGrpToMenuDB tMenuDB = new LDMenuGrpToMenuDB(conn);
               if (tMenuDB == null) {
                System.out.println("tMenuDB is null");
//      	conn.close();
                return false;
               }

               LDMenuGrpToMenuSet menuSet = tMenuDB.executeQuery(sqlStr);
               System.out.println("menuSet size is " + menuSet.size());
               if (menuSet.size() == 0)
                continue;
               LDMenuGrpToMenuSet nextRemoveSet = new LDMenuGrpToMenuSet();


               //如果此菜单组节点集合中有的在realRemoveMenuSet中，则删除掉，并
               //将其加入到nextRemoveMenuSet中
               for(int i = 1; i <= realRemoveSet.size(); i++) {
                LDMenuGrpToMenuSchema chooseMenuSchema = realRemoveSet.get(i);
                LDMenuGrpToMenuSchema delMenuSchema = null;
                String nodecode1 = chooseMenuSchema.getNodeCode();
                int j = 1;
                for (; j <= menuSet.size(); j++) {
                 delMenuSchema = menuSet.get(j);
                 String nodecode2 = delMenuSchema.getNodeCode();
     System.out.println("nodecode 2 :" + nodecode2 + " nodecode1 : " + nodecode1);
                 if (nodecode1.compareTo(nodecode2) != 0)
                     continue;
       break;
      }
      if (j <= menuSet.size()) {
       nextRemoveSet.add(chooseMenuSchema);
       menuSet.remove(delMenuSchema); //可能有问题，指针问题
       System.out.println("menuSet.size is :" + menuSet.size());
      }
                }
     System.out.println("nextRemoveSet size is" + nextRemoveSet.size());
                if (nextRemoveSet.size() == 0) {
                    continue;
                }
               System.out.println("here");
        //保存menuSet,即更新此菜单组
        LDMenuGrpToMenuDB tSaveDB = new LDMenuGrpToMenuDB(conn);
        LDMenuGrpToMenuSchema tDelSchema = new LDMenuGrpToMenuSchema();
        tDelSchema.setMenuGrpCode(tMenuGrpCode);
               tSaveDB.setSchema(tDelSchema);
               if (!tSaveDB.deleteSQL()){
//           	 conn.close();
                 return false;
               }
     LDMenuGrpToMenuDBSet tLDMenuGrpToMenuDBSet =new LDMenuGrpToMenuDBSet(conn);
        tLDMenuGrpToMenuDBSet.set(menuSet);
        if (!tLDMenuGrpToMenuDBSet.insert()) {
//	       conn.close();
             return false;
        }

        //对于所有引用此菜单组的用户，进行递归调用进行更新
        //首先得到所有的用户组
        sqlStr = "select * from ldusertoMenugrp where menugrpcode = '" + tMenuGrpCode + "'";

        System.out.println(sqlStr);

        LDUserTOMenuGrpDB tLDUserDB = new LDUserTOMenuGrpDB(conn);
        LDUserTOMenuGrpSet tUserSet = tLDUserDB.executeQuery(sqlStr);
        System.out.println("使用此菜单组的用户数为" + tUserSet.size());
        if (tUserSet.size() == 0)
            continue;

        for (int i = 1; i <= tUserSet.size(); i++) {
         LDUserTOMenuGrpSchema nextUser = tUserSet.get(i);
         String nextUserCode = nextUser.getUserCode();
         userToMenuGrp(nextRemoveSet,nextUserCode,conn);
        }
            }

//     conn.close();
         }  catch (Exception ex) {
     try { System.out.println("excrption"); conn.close(); } catch (Exception e) {return false ; };
     return false;
         }
         return true;
     }

     */
}
