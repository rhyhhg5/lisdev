/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.menumang;

import java.text.SimpleDateFormat;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LDUserLogDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDUserLogSchema;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 用户退出或session超时释放时处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author liuchao	2010-07-30
 * @version 1.0
 */
public class LDUserLogOutBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
	private String tCurrentDate = PubFun.getCurrentDate();
	private String tCurrentTime = PubFun.getCurrentTime();
    /** 数据操作字符串 */
    private String mOperate;
    private String mLogNo;
    private MMap mMap = new MMap();

    /** 业务处理相关变量 */
    /** 菜单组、菜单组到菜单的相关信息*/
    LDUserLogSchema mLDUserLogSchema = new LDUserLogSchema();


    public LDUserLogOutBL()
    {
        // just for debug

    }

    public static void main(String[] args)
    {

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        if (!getInputData(cInputData))
        {
            return false;
        }

        LDUserLogDB mLDUserLogDB = new LDUserLogDB();
        mLDUserLogDB.setLogNo(mLogNo);
        if (!mLDUserLogDB.getInfo()) {
        	mErrors.addOneError("查找销毁session信息时出错!");
        	return false;
		}
        
        mLDUserLogSchema = mLDUserLogDB.getSchema();
        mLDUserLogSchema.setQuitDate(tCurrentDate);
        mLDUserLogSchema.setQuitTime(tCurrentTime);
        
        String logonTime = mLDUserLogSchema.getMakeDate()+" "+mLDUserLogSchema.getMakeTime();
        String logoutTime = tCurrentDate+" "+tCurrentTime;        
        mLDUserLogSchema.setOnlineTime(getBetweenDayNumber(logonTime,logoutTime,"M"));
        
        mMap.put(mLDUserLogSchema, "UPDATE");
        
        if (!prepareData())
		{
			return false;
		}
        
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, "")) {
           if (tPubSubmit.mErrors.needDealError()) {
             mErrors.addOneError("保存销毁session信息时出错!");
           }
        }
        return false;

    }    

    public VData getResult()
    {
        return mResult;
    }
    
    /**
     *	计算两个时间之差
     * 	参数unit "D"-天 "M"-分钟 "S"-秒
     * 	20100804 liuchao
     */
	private int getBetweenDayNumber(String dateA, String dateB, String unit) {
		
		long m = 0;
		long dayNumber = 0;
//		1s=1000ms
		long seds = 1000L;
//		1小时=60分钟=3600秒=3600000ms
		long mins = 60L * 1000L;
//		计算天数之差
		long day= 24L * 60L * 60L * 1000L;
		
		if (unit.equals("D")) {
			m = day ;
		}else if (unit.equals("M")) {
			m = mins ;
		}else if (unit.equals("S")) {
			m = seds ;
		}
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
		   java.util.Date d1 = df.parse(dateA);
		   java.util.Date d2 = df.parse(dateB);
		   dayNumber = (d2.getTime() - d1.getTime()) / m ;
		} catch (Exception e) {
		   e.printStackTrace();
		}
		return (int) dayNumber;
	}
	
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        // 检验查询条件
    	mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
    	mLogNo = (String)cInputData.getObjectByObjectName("String", 0);
        
    	if (mLogNo == null)
        {
            return false;
        }
    	
        return true;
    }
    private boolean prepareData()
	{
    	mResult.clear();
    	mResult.add(mMap);
		return true;
	}
}
