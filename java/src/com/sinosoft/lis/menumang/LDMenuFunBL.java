package com.sinosoft.lis.menumang;

import com.sinosoft.lis.schema.LDMenuSchema;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDMenuGrpToMenuSchema;
import com.sinosoft.utility.SSRS;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 菜单功能逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Fanym , Modify by Minim at 20050916
 * @version 1.0
 */
public class LDMenuFunBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mResult = new VData();
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String mOperate = null;
    private LDMenuSchema mLDMenuSchema = new LDMenuSchema();
    private String NodeCode = "";

    private MMap map = new MMap();

    public LDMenuFunBL() {
    }

    public static void main(String[] args) {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("After getinputdata");

        //进行业务处理
        if (!dealData()) {
            return false;
        }
        System.out.println("After dealData！");
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("After prepareOutputData");
        System.out.println("Start LDMenuGrp BL Submit...");

//        LDMenuFunBLS tLDMenuFunBLS = new LDMenuFunBLS();
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate)) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            mResult.clear();
            return false;
        }

        mInputData = null;
        return true;
    }

    private boolean getInputData(VData mInputData) {
        mLDMenuSchema = (LDMenuSchema) mInputData.getObjectByObjectName(
                "LDMenuSchema", 0);
        if ((mLDMenuSchema == null)) {
            CError tError = new CError();
            tError.moduleName = "LDMenuFunBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean prepareOutputData() {
        mInputData = new VData();
        try {
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDMenuFunBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean dealData() {
        try {
            //获取参考结点信息
            String selNodeCode = mLDMenuSchema.getNodeCode();
            LDMenuDB tLDMenuDB = new LDMenuDB();
            tLDMenuDB.setNodeCode(selNodeCode);
            if (!tLDMenuDB.getInfo()) {
                CError.buildErr(this, "获取参考结点失败");
                return false;
            }

            if (mOperate.equals("upinsert") || mOperate.equals("downinsert")) {
                //产生新菜单结点数据
            	
            	String nodeCode = getMaxNodeCode();
                System.out.println("=========nodeCode:" + nodeCode);
                
                LDMenuSchema tLDMenuSchema = tLDMenuDB.getSchema();
                tLDMenuSchema.setNodeCode(nodeCode);
                tLDMenuSchema.setNodeName(mLDMenuSchema.getNodeName());
                tLDMenuSchema.setRunScript(mLDMenuSchema.getRunScript());
                tLDMenuSchema.setChildFlag("0");

                //如果参考结点有父结点，更新父结点的孩子数
                if (!tLDMenuDB.getParentNodeCode().equals("0")) {
                    map.put(updateParentNodeChildNum(tLDMenuDB.getParentNodeCode(), 1), "UPDATE");
                }

                if (mOperate.equals("upinsert")) {
                    //更新参考结点以后所有结点的NodeOrder
                    String sql = "update LDMenu set NodeOrder = NodeOrder+1 Where NodeOrder >= "
                                 + tLDMenuDB.getNodeOrder();

                    map.put(sql, "UPDATE");
                    map.put(tLDMenuSchema, "INSERT");
                }
                else if (mOperate.equals("downinsert")) {
                    //如果参考结点有子结点，需要获取最后子结点的NodeOrder
                    int nodeOrder = tLDMenuDB.getNodeOrder();
                    if (!tLDMenuDB.getChildFlag().equals("0")) {
                        String sql = "select max(nodeorder) from ldmenu where parentnodecode='" + selNodeCode + "'";
                        ExeSQL e = new ExeSQL();
                        SSRS s = e.execSQL(sql);
                        nodeOrder = Integer.parseInt(s.GetText(1, 1));
                    }

                    //更新参考结点以后所有结点的NodeOrder
                    String sql = "update LDMenu set NodeOrder = NodeOrder+1 Where NodeOrder > "
                                 + nodeOrder;
                    map.put(sql, "UPDATE");

                    tLDMenuSchema.setNodeOrder(nodeOrder + 1);
                    map.put(tLDMenuSchema, "INSERT");
                }

                map.put(getLDMenuGrpToMenu(tLDMenuSchema.getNodeCode()), "DELETE&INSERT");
            }
            //做为参考结点的下级第一个结点插入
            else if (mOperate.equals("innerinsert")) {
                //产生新菜单结点数据
                LDMenuSchema tLDMenuSchema = tLDMenuDB.getSchema();
                tLDMenuSchema.setNodeCode(getMaxNodeCode());
                tLDMenuSchema.setParentNodeCode(tLDMenuDB.getNodeCode());
                tLDMenuSchema.setNodeName(mLDMenuSchema.getNodeName());
                tLDMenuSchema.setRunScript(mLDMenuSchema.getRunScript());
                tLDMenuSchema.setChildFlag("0");

                //更新参考结点的孩子数
                map.put(updateParentNodeChildNum(tLDMenuDB.getNodeCode(), 1), "UPDATE");

                //更新参考结点以后所有结点的NodeOrder
                String sql = "update LDMenu set NodeOrder = NodeOrder+1 Where NodeOrder > "
                             + tLDMenuDB.getNodeOrder();
                map.put(sql, "UPDATE");

                tLDMenuSchema.setNodeOrder(tLDMenuDB.getNodeOrder() + 1);
                map.put(tLDMenuSchema, "INSERT");
                map.put(getLDMenuGrpToMenu(tLDMenuSchema.getNodeCode()), "DELETE&INSERT");
            }
            //删除菜单结点，有子结点的不能直接删除，懒的写递归删除了，误操作风险也高
            else if (mOperate.equals("delete")) {
                if (!tLDMenuDB.getChildFlag().equals("0")) {
                    CError.buildErr(this, "该菜单存在子菜单，请先删除子菜单");
                    return false;
                }

                //如果参考结点有父结点，更新父结点的孩子数
                if (!tLDMenuDB.getParentNodeCode().equals("0")) {
                    map.put(updateParentNodeChildNum(tLDMenuDB.getParentNodeCode(), -1), "UPDATE");
                }

                //更新参考结点以后所有结点的NodeOrder
                String sql = "update LDMenu set NodeOrder = NodeOrder-1 Where NodeOrder >= "
                             + tLDMenuDB.getNodeOrder();
                map.put(sql, "UPDATE");

                //必须放在更新NodeOrder之后，否则结点插入顺序会有问题
                map.put(tLDMenuDB.getSchema(), "DELETE");
                
                //不同的菜单组下相同的节点全部删除
                LDMenuGrpToMenuSchema tLDMenuGrpToMenuSchema = new LDMenuGrpToMenuSchema();
                tLDMenuGrpToMenuSchema.setNodeCode(tLDMenuDB.getNodeCode());
                map.put(tLDMenuGrpToMenuSchema, "DELETE");
            }
            //update 只是针对本菜单的名称与映射路径
            else if(mOperate.equals("update"))
            {
            	String SQL = "update LDMenu set NodeName ='"+mLDMenuSchema.getNodeName()+"',RunScript = '"+mLDMenuSchema.getRunScript()+"' " +
            			" where NodeCode = '"+selNodeCode+"'";
            	map.put(SQL, "UPDATE");
            }
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    /**
     * 获取菜单表中的最大结点号
     * @return String
     */
    private String getMaxNodeCode() {
        ExeSQL tExeSQL = new ExeSQL();
        String sql = "select max(to_number(NodeCode))+1 from LDMenu";
        String MaxCode = tExeSQL.getOneValue(sql);
        
        int index = MaxCode.lastIndexOf(".");
        if(index > 0){
        	MaxCode = MaxCode.substring(0, index);
        }
        System.out.println("=========nodeCode:" + MaxCode);
        
        return MaxCode;
    }

    /**
     * 获取需要更新的父结点数据
     * @param parentNodeCode String
     * @return LDMenuSchema
     */
    private LDMenuSchema updateParentNodeChildNum(String parentNodeCode, int i) throws
            Exception {
        LDMenuDB tLDMenuDB = new LDMenuDB();
        tLDMenuDB.setNodeCode(parentNodeCode);
        if (!tLDMenuDB.getInfo()) {
            CError.buildErr(this, "获取参考结点的父结点失败，导致无法更新其父结点的孩子数");
            throw new Exception("");
        }

        tLDMenuDB.setChildFlag(String.valueOf(Integer.parseInt(tLDMenuDB.
                                                               getChildFlag()) + i));
        return tLDMenuDB.getSchema();
    }

    /**
     * 插入菜单分组关联表：新菜单默认仅仅分配给8080菜单组
     * @param nodeCode String
     * @return LDMenuGrpToMenuSchema
     */
    private LDMenuGrpToMenuSchema getLDMenuGrpToMenu(String nodeCode) {
        LDMenuGrpToMenuSchema tLDMenuGrpToMenuSchema = new
                                                       LDMenuGrpToMenuSchema();
        tLDMenuGrpToMenuSchema.setMenuGrpCode("8080");
        tLDMenuGrpToMenuSchema.setNodeCode(nodeCode);
        return tLDMenuGrpToMenuSchema;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult() {
        return mResult;
    }

    public void operation1(java.lang.String parentNodeCode, int i) {
    }
}
