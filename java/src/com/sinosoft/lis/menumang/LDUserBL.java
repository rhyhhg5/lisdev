/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.menumang;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.schema.LDUserSchema;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LDUserSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SysConst;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDUserLogSchema;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 用户业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author DingZhong    liuli修改于090706,增加用户锁定功能
 * @version 1.0
 */
public class LDUserBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;
    private MMap mMap = new MMap();

    /** 业务处理相关变量 */
    /** 菜单组、菜单组到菜单的相关信息*/
    LDUserSchema mLDUserSchema = new LDUserSchema();
    LDUserSet mLDUserSet = new LDUserSet();
    LDUserLogSchema mLDUserLogSchema = new LDUserLogSchema();

    String mResultStr = "";
    int mResultNum = 0;

    public LDUserBL()
    {
        // just for debug

    }

    public static void main(String[] args)
    {
        LDUserBL tLDUserBL1 = new LDUserBL();
        LDUserSchema tSchema = new LDUserSchema();
        tSchema.setUserCode("pa9601");
        tSchema.setPassword("F242D81CB4F1CF61");
        tSchema.setComCode("8696");
        VData tVData = new VData();
        tVData.add(tSchema);
        if (!tLDUserBL1.submitData(tVData, "query"))
        {
            System.out.println(tLDUserBL1.mErrors.getErrContent());
        }
        System.out.println("kkkk");
        for (int i = 0; i < tLDUserBL1.mErrors.getErrorCount(); i++)
        {
            CError tError = tLDUserBL1.mErrors.getError(i);
            System.out.println(tError.errorMessage);
            System.out.println(tError.moduleName);
            System.out.println(tError.functionName);
            System.out.println("----------------");
        }
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        // 判断操作是不是查询
        if (cOperate != "query")
        {
            return false;
        }

        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中

        System.out.println("start get inputdata...");
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("---getInputData---");

        //进行业务处理
        if (!queryUser())
        {
            if(mLDUserSet != null && mLDUserSet.size()!=0){ //如果是密码或者机构出错，将修改失败登陆的次数和置锁定标记。
                int count = mLDUserSet.get(1).getLoginFailCount() + 1;
                 mLDUserSet.get(1).setLoginFailCount(count);
                if (count >= SysConst.LoginFailCount) {
                    mLDUserSet.get(1).setUserState("L");
                }
                  String maxno = String.valueOf((new java.util.Date()).getTime());
                  mLDUserLogSchema.setLogNo(maxno);
                  mLDUserLogSchema.setManageCom(mLDUserSchema.getComCode());
                  mLDUserLogSchema.setOperator(mLDUserSchema.getUserCode());
                  mLDUserLogSchema.setLogType("F"); //记录登陆失败
                  mLDUserLogSchema.setCientIP(mLDUserSchema.getUserDescription());
                  mLDUserLogSchema.setMakeDate(PubFun.getCurrentDate());
                  mLDUserLogSchema.setMakeTime(PubFun.getCurrentTime());

            }
            prepareOutputData();
            PubSubmit   tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(this.mResult, "")) {
               if (tPubSubmit.mErrors.needDealError()) {
                 mErrors.addOneError("保存登陆信息时出错!");
               }
            }
            return false;
        }else{//登陆成功
             mLDUserSet.get(1).setLoginFailCount(0);
             mLDUserSet.get(1).setUserState("0"); //置为有效
             String maxno = String.valueOf((new java.util.Date()).getTime());
             mLDUserLogSchema.setLogNo(maxno);
             mLDUserLogSchema.setManageCom(mLDUserSchema.getComCode());
             mLDUserLogSchema.setOperator(mLDUserSchema.getUserCode());
             mLDUserLogSchema.setLogType("S"); //记录登陆成功
             mLDUserLogSchema.setCientIP(mLDUserSchema.getUserDescription());
             mLDUserLogSchema.setMakeDate(PubFun.getCurrentDate());
             mLDUserLogSchema.setMakeTime(PubFun.getCurrentTime());

             prepareOutputData();
             PubSubmit   tPubSubmit = new PubSubmit();
             if (!tPubSubmit.submitData(this.mResult, "")) {
                 if (tPubSubmit.mErrors.needDealError()) {
                     // @@错误处理
                     mErrors.addOneError("保存登陆信息时出错!");
                 }
             }
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public int getResultNum()
    {
        return mResultNum;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        // 检验查询条件
        mLDUserSchema = (LDUserSchema) cInputData.getObjectByObjectName(
                "LDUserSchema", 0);

        if (mLDUserSchema == null)
        {
            return false;
        }

        return true;
    }

    /**
     * 查询符合条件的信息
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean queryUser()
    {
//
//  	  String decryptPwd = tIdea.decryptString(encryptPwd);
//  	  decryptPwd = decryptPwd.trim();
//  	  System.out.println("now encryptPwd");
//  	  System.out.println("decrypt pwd:" + decryptPwd);
        String sqlStr = "select * from lduser where usercode = '" +
                        mLDUserSchema.getUserCode() + "' ";  //查询该用户所有信息
//        sqlStr += "and password = '" + encryptPwd + "'";

//    System.out.println(sqlStr);
        LDUserDB tLDUserDB = mLDUserSchema.getDB();

        mLDUserSet = tLDUserDB.executeQuery(sqlStr);
//    System.out.println("here here here");
        //*********************过渡代码*******************
         /*
           LisIDEA tIdea = new LisIDEA();
           if (mLDUserSet.size() == 0) {
               String decryptPwd = tIdea.decryptString(encryptPwd);
          sqlStr = "select * from lduser where usercode = '" + mLDUserSchema.getUserCode() + "' ";
          sqlStr += "and password = '" + decryptPwd + "'";
               System.out.println(sqlStr);
               mLDUserSet = tLDUserDB.executeQuery(sqlStr);
           }
          */
         //******************************************************/
         if (mLDUserSet.size() == 0) //用户不存在
         {
             // @@错误处理
             this.mErrors.copyAllErrors(tLDUserDB.mErrors);
             CError tError = new CError();
             tError.moduleName = "LDUserBL";
             tError.functionName = "queryUser";
             tError.errorMessage = "用户查询失败!";
             this.mErrors.addOneError(tError);
             mLDUserSet.clear();
             return false;
         }
        String encryptPwd = mLDUserSchema.getPassword(); //获取操作员输入的密码
        if (!mLDUserSet.get(1).getPassword().equals(encryptPwd))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDUserBL";
            tError.functionName = "queryUser";
            tError.errorMessage = "未找到相关数据!";
            this.mErrors.addOneError(tError);

//            mLDUserSet.clear();
            return false;
        }
        System.out.println("here gose:");

        String ComCode = mLDUserSchema.getComCode();
        //judge if comcode is valid station

        String strSql =
                "select * from LDCom where comCode = '" + ComCode + "'";
//    System.out.println(strSql);
        LDCodeSet tCodeSet = new LDCodeSet();
        LDCodeDB tCodeDB = new LDCodeDB();
        tCodeSet = tCodeDB.executeQuery(strSql);   //录入的机构是否有效
        if (tCodeSet == null || tCodeSet.size() == 0)
        {
            return false;
        }

        System.out.println("ComCode:" + ComCode);
        int matchCount = 0;
        for (int i = 1; i <= mLDUserSet.size(); i++)
        {
            LDUserSchema tUserSchema = mLDUserSet.get(i);
            String getComCode = tUserSchema.getComCode();
//      System.out.println("getComCode:" + getComCode);
//      System.out.println("*************************");
            if (getComCode.length() > ComCode.length())
            {
                continue;
            }
            int j = 0;
            for (; j < getComCode.length(); j++)
            {
                if (ComCode.charAt(j) != getComCode.charAt(j))
                {
                    break;
                }
            }

            if (j == getComCode.length())
            {
                matchCount++;
            }
        }
        System.out.println("matchCount:" + matchCount);
        if (matchCount == 0)
        {
            return false;
        }
        mResult.add(mLDUserSet);
        mResultNum = mLDUserSet.size();

        System.out.println(mResult);
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        try
        {
            mMap.put(mLDUserSet,"UPDATE");
            mMap.put(mLDUserLogSchema,"INSERT");
            mResult.add(mMap);
            mResult.add(mLDUserLogSchema.getLogNo());
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDUserBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
