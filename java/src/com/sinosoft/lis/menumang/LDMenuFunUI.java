package com.sinosoft.lis.menumang;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LDMenuSchema;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: * 从错误对象处理类继承，用来保存错误对象,在每个类中都存在 * </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft</p>
 * @author Fanym , Modify by Minim at 20050916
 * @version 1.0
 */
public class LDMenuFunUI
{

    //业务处理相关变量
    private VData mInputData;
    public CErrors mErrors = new CErrors();
    /** 传出数据的容器 */
    private VData mResult = new VData();

    public LDMenuFunUI()
    {
    }

    public static void main(String[] args)
    {
        LDMenuFunUI tLDMenuFunUI = new LDMenuFunUI();
        LDMenuSchema tLDMenuSchema = new LDMenuSchema();
        tLDMenuSchema.setNodeName("t5");
        tLDMenuSchema.setRunScript("bbbbbb");
        tLDMenuSchema.setNodeCode("95952");
        VData tVData = new VData();
        tVData.add(tLDMenuSchema);
//        tLDMenuFunUI.submitData(tVData, "upinsert");
//        tLDMenuFunUI.submitData(tVData, "delete");
        tLDMenuFunUI.submitData(tVData, "downinsert");
//        tLDMenuFunUI.submitData(tVData, "innerinsert");
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        LDMenuFunBL tLDMenuFunBL = new LDMenuFunBL();
        if (!tLDMenuFunBL.submitData(mInputData, cOperate))
        {
            this.mErrors.copyAllErrors(tLDMenuFunBL.mErrors);
            mResult.clear();
            mResult.add(mErrors.getFirstError());
            return false;
        }

        mInputData = null;
        return true;
    }

/**
 * 数据输出方法，供外界获取数据处理结果
 * @return 包含有数据查询结果字符串的VData对象
 */
public VData getResult() {
  return mResult;
}

}
