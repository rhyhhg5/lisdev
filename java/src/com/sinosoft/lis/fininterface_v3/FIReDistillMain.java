package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2008</p>
 *
 * <p>Company: </p>
 *
 * @author Dongjian
 * @version 1.0
 */
public class FIReDistillMain
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;
    private MMap mmap = new MMap();
    // 保存操作员和管理机构的类
    private GlobalInput mGI = new GlobalInput();
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String tAppNo = "";
    private String tBusinessNo = "";
    private String tDetailIndexID = "";
    public FIOperationLog tLogInfoDeal ;
    public static void main(String[] args)
    {

    }

    // 传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealData()) 
        {
            return false;
        }
        if (!PubSubmit())
		{
			return false;
		}
        
        return true;
    }


    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData)
    {
        mGI = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
        tDetailIndexID = (String) mInputData.get(1);
        tBusinessNo = (String) mInputData.get(2);
        tAppNo = (String) mInputData.get(3);
        if (mGI == null)
        {
            buildError("FIReDistillMain", "getInputData", "传入登陆信息参数为空");
            return false;
        }
        if (tAppNo == null || "".equals(tAppNo))
        {
            buildError("FIReDistillMain", "getInputData", "传入红冲信息参数为空");
            return false;
        }
        if (tBusinessNo == null || "".equals(tBusinessNo))
        {
            buildError("FIReDistillMain", "getInputData", "传入红冲信息参数为空");
            return false;
        }
        if (tDetailIndexID == null || "".equals(tDetailIndexID))
        {
            buildError("FIReDistillMain", "getInputData", "传入红冲信息参数为空");
            return false;
        }
        return true;
    }
    
    /**
     * 根据前面的输入数据，进行逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
     * @return
     */
    private boolean dealData()
    {
        try
        {
        	
        	FIAbStandardDataSet oFIAbStandardDataSet = new FIAbStandardDataSet();
            FIVoucherDataDetailSet oFIVoucherDataDetailSet = new FIVoucherDataDetailSet();
            
            FIAbStandardDataDB tFIAbStandardDataDB = new FIAbStandardDataDB();
        	tFIAbStandardDataDB.setIndexCode(tDetailIndexID);
        	tFIAbStandardDataDB.setIndexNo(tBusinessNo);
        	
        	FIAbStandardDataSet tFIAbStandardDataSet = tFIAbStandardDataDB.query();
        	if(tFIAbStandardDataSet==null || tFIAbStandardDataSet.size()<1)
        	{
        		buildError("FIReDistillMain", "dealData", "tFIAbStandardDataSet为空!");
                return false;
        	}
        	
        	String tSerialno[] = FinCreateSerialNo.getSerialNoByFINDATA(tFIAbStandardDataSet.size());
        	String ttSerialno[] = FinCreateSerialNo.getSerialNoByFINDATA(tFIAbStandardDataSet.size());
        	for(int i=1; i<=tFIAbStandardDataSet.size(); i++)
        	{
        		FIAbStandardDataSchema tFIAbStandardDataSchema = tFIAbStandardDataSet.get(i);
 
        		//生成FIAbStandardData表红冲数据
            	FIAbStandardDataSchema ttFIAbStandardDataSchema = (FIAbStandardDataSchema)tFIAbStandardDataSchema.clone();
            	ttFIAbStandardDataSchema.setSerialNo(ttSerialno[i-1]);
            	ttFIAbStandardDataSchema.setState("01");
            	//oFIAbStandardDataSet.add(ttFIAbStandardDataSchema);
            	
        		//生成FIAbStandardData表红冲数据
//            	tFIAbStandardDataSchema.setSerialNo(tSerialno[i-1]);
            	ttFIAbStandardDataSchema.setASerialNo(FinCreateSerialNo.getASerialNo());
            	
            	double tSumMoney = ttFIAbStandardDataSchema.getSumActuMoney();
            	ttFIAbStandardDataSchema.setSumActuMoney(-tSumMoney);
            	
            	ttFIAbStandardDataSchema.setStringInfo01(tAppNo);
            	ttFIAbStandardDataSchema.setMakeDate(CurrentDate);
            	ttFIAbStandardDataSchema.setMakeTime(CurrentTime);
            	ttFIAbStandardDataSchema.setCheckFlag("02");//红冲
            	oFIAbStandardDataSet.add(ttFIAbStandardDataSchema);
            	
                //(1)生成FIVoucheManage表数据
            	FIVoucheManageSchema tFIVoucheManageSchema = new FIVoucheManageSchema();
            	
            	tFIVoucheManageSchema.setBatchNo(FinCreateSerialNo.getBatchNo());
            	tFIVoucheManageSchema.setStartDate(tFIAbStandardDataSchema.getAccountDate());
            	tFIVoucheManageSchema.setEndDate(tFIAbStandardDataSchema.getAccountDate());
            	tFIVoucheManageSchema.setEventNo(tFIAbStandardDataSchema.getEventNo());
            	tFIVoucheManageSchema.setMakeDate(PubFun.getCurrentDate());
            	tFIVoucheManageSchema.setMakeTime(PubFun.getCurrentTime());
            	tFIVoucheManageSchema.setState("00");
            	tFIVoucheManageSchema.setModifyDate(PubFun.getCurrentDate());
            	tFIVoucheManageSchema.setModifyTime(PubFun.getCurrentTime());
            	tFIVoucheManageSchema.setOperator(mGI.Operator);
            	
            	mmap.put(tFIVoucheManageSchema, "INSERT");
            	
            	//生成FIVoucherDataDetail表红冲数据
            	FIVoucherDataDetailDB tFIVoucherDataDetailDB = new FIVoucherDataDetailDB();
            	
            	tFIVoucherDataDetailDB.setSerialNo(tFIAbStandardDataSchema.getSerialNo());
            	
                FIVoucherDataDetailSet tFIVoucherDataDetailSet = tFIVoucherDataDetailDB.query();
                
                if(tFIVoucherDataDetailSet.size()==0)
                {
                	buildError("FIReDistillMain", "dealData", "tFIVoucherDataDetailSet为空!");
                    return false;
                }
                for(int j=1; j<=tFIVoucherDataDetailSet.size(); j++)
                {
                	FIVoucherDataDetailSchema tFIVoucherDataDetailSchema = tFIVoucherDataDetailSet.get(j);
                	
                	tFIVoucherDataDetailSchema.setSerialNo(ttFIAbStandardDataSchema.getSerialNo());
                	tFIVoucherDataDetailSchema.setFSerialNo(FinCreateSerialNo.getFSerialNo());
                	
                	double ttSumMoney = tFIVoucherDataDetailSchema.getSumMoney();
                	
                	tFIVoucherDataDetailSchema.setSumMoney(-ttSumMoney);
                	tFIVoucherDataDetailSchema.setBatchNo(tFIVoucheManageSchema.getBatchNo());
                	tFIVoucherDataDetailSchema.setCheckFlag("02"); //红冲
                	
                    oFIVoucherDataDetailSet.add(tFIVoucherDataDetailSchema);
                }

        	}
        	            
            mmap.put(oFIAbStandardDataSet, "INSERT");
            mmap.put(oFIVoucherDataDetailSet, "INSERT");
            mmap.put("update FIDataFeeBackApp set AppState = '02' where AppNo = '" + tAppNo + "'", "UPDATE"); //红冲处理

            return true;
        }
        catch (Exception ex)
        {
            buildError("FIReDistillMain","dealData","FIReDistillMain保存后台数据出现异常，信息为：" + ex.getMessage());
            return false;
        }

    }

    private boolean PubSubmit()
	{
		try
		{
			mInputData = new VData();
			mInputData.add(mmap);

            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, ""))
            {
               this.mErrors.copyAllErrors(tPubSubmit.mErrors);
               return false;
            }
		}
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIReDistillMain";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}


    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }

}
