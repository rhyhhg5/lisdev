package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.db.FIPeriodManagementDB;
import com.sinosoft.lis.db.FIRulesVersionDB;
import com.sinosoft.lis.db.FIVoucheManageDB;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIVoucherDataGatherSchema;
import com.sinosoft.lis.vschema.FIPeriodManagementSet;
import com.sinosoft.lis.vschema.FIRulesVersionSet;
import com.sinosoft.lis.vschema.FIVoucheManageSet;
import com.sinosoft.lis.vschema.FIVoucherDataGatherSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class FICertificateGatherBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    public GlobalInput mGI = new GlobalInput();
    
    private String mBatchNo;
    
    private final String enter = "\r\n"; // 换行符
	/** 日志处理*/
	private FIOperationLog tLogDeal = null;
	private FIRulesVersionSet mFIRulesVersionSet;

    public boolean dealGather(String sBatchNo, GlobalInput tGI)
    {
        mBatchNo = sBatchNo;
        mGI = tGI ;
        
    	//第一步判断是否已经提取过数据
    	if(!Check())
        {
             return false;
    	}
        if(!init(mGI.Operator))
        {
             return false;
        }
    	if(!moveDataToGather())
        {
             return false;
    	}
    	tLogDeal.Complete(true);
    	return true;
    }
    public boolean Check()
    {

       try
       {
           String rSQL = "select * from FIVoucheManage a where BatchNo = '" + mBatchNo + "' and state='30' ";
           
           System.out.println(rSQL);
           FIVoucheManageSet tFIVoucheManageSet = new FIVoucheManageDB().executeQuery(rSQL);
           
           if(tFIVoucheManageSet==null||tFIVoucheManageSet.size()<1)
           {
               buildError("FICertificateGatherBL", "Check","该批次数据已经汇总");
               return false;
           }



           ExeSQL tExeSQL = new ExeSQL();
           SSRS tSSRS = new SSRS();
           tSSRS = tExeSQL.execSQL( "select min(AccountDate),max(AccountDate) from FIVoucherDataDetail where batchno='" +mBatchNo + "'");
           if(tExeSQL.mErrors.needDealError())
           {
               buildError("FICertificateGatherBL","Check","查询该批次数据对应时间区间出错");
               return false;
           }

           String StartDate = tSSRS.GetText(1, 1);
           String EndDate = tSSRS.GetText(1, 2);

           if(StartDate == null || EndDate == null || StartDate.equals("") || EndDate.equals(""))
           {
               buildError("FICertificateGatherBL","Check","未查询到该批次数据对应时间区间，该批次数据可能为空");
               return false;
           }

           //校验该批数据账务日期是否对应开启的会计区间
           if(!getPeriodManagement(StartDate, EndDate))
           {
               return false;
           }

           //校验该批数据是否对应多个账务规则版本，并获得规则版本号码
           if (!getRuleVersion(StartDate, EndDate)) 
           {
               return false;
           }

       }
       catch (Exception ex)
       {
           buildError("FIMoveDataFromGRPToPRO", "infoCheck","执行条件检测出现异常，信息为" + ex.getMessage());
           return false;
       }

        return true;
    }

    /**
     * 校检会计期间
     * @param sDate
     * @param eDate
     * @return
     */
    private boolean getPeriodManagement(String sDate, String eDate)
    {
        String sql = "  select * from FIPeriodManagement where StartDate<='" + eDate + "' " +
                     " and EndDate>='" + sDate + "' and state='1' order by StartDate asc";
        System.out.println("校检会计期间"+sql);
        FIPeriodManagementSet tFIPeriodManagementSet = new FIPeriodManagementSet();
        FIPeriodManagementDB tFIPeriodManagementDB = new FIPeriodManagementDB();
        tFIPeriodManagementSet = tFIPeriodManagementDB.executeQuery(sql);
        if (tFIPeriodManagementSet.size() ==0)
        {
            buildError("FICertificateGatherBL","getPeriodManagement", "您输入的时间区间所对应的会计区间尚未开启，请重新输入起始日期！");
            return false;
        }
        else if(tFIPeriodManagementSet.size() !=1)
        {
            String tInfo = "您输入的时间区间内查询到"+tFIPeriodManagementSet.size()+"个有效会计区间！分别是";
            for(int i =0;i<tFIPeriodManagementSet.size();i++)
            {
                if(i==0)
                {
                    tInfo += "[" + tFIPeriodManagementSet.get(i + 1).getStartDate() + "][" +  tFIPeriodManagementSet.get(i + 1).getEndDate() + "]";
                }
                else
                {
                    tInfo += "," + "[" + tFIPeriodManagementSet.get(i + 1).getStartDate() + "][" +  tFIPeriodManagementSet.get(i + 1).getEndDate() + "]";
                }
            }
            buildError("FICertificateGatherBL","getPeriodManagement", tInfo);
            return false;
        }
        return true;
    }
    
    /**
     * 校检账务规则版本
     * @param sDate
     * @param eDate
     * @return
     */
    private boolean getRuleVersion(String sDate, String eDate)
    {

        try
        {
            String sql = " select * from FIRulesVersion where  versionstate ='01' and StartDate<='" + eDate + "' "
                         + " and (EndDate>'" + sDate + "' or EndDate is null)" + " order by startdate asc ";
            
            System.out.println("账务规则版本"+sql);
            FIRulesVersionDB tFIRulesVersionDB = new FIRulesVersionDB();
            mFIRulesVersionSet = tFIRulesVersionDB.executeQuery(sql);
            if (mFIRulesVersionSet.size() == 0)
            {
                buildError("FICertificateGatherBL","getRuleVersion", "您输入的时间区间内提数规则无版本，请重新输入起始日期！");
                return false;
            }
            else if (mFIRulesVersionSet.size() != 1)
            {
                String tInfo = "您输入的时间区间内查询到" + mFIRulesVersionSet.size() + "个提数有效版本！版本分别是：";
                for (int i = 0; i < mFIRulesVersionSet.size(); i++) {
                    if (i == 0) {
                        tInfo += "[" + mFIRulesVersionSet.get(i + 1).getStartDate() + "][" +  mFIRulesVersionSet.get(i + 1).getEndDate()+ "]";
                    } else {
                        tInfo += "," +  "[" + mFIRulesVersionSet.get(i + 1).getStartDate() + "][" +  mFIRulesVersionSet.get(i + 1).getEndDate()+ "]";
                    }
                }
            }
            
            return true;
        }
        catch (Exception e)
        {
            buildError("FICertificateGatherBL","getRuleVersion", "提数规则版本核对出现异常，异常信息:" + e.getMessage());
            return false;
        }
    }
    
    /**
     * 初始化信息
     * @param cOperator
     * @return
     */
    public boolean init(String cOperator)
    {
        try
        {

        	tLogDeal = new FIOperationLog(cOperator,"40");

            StringBuffer oStringBuffer = new StringBuffer(1024);
            oStringBuffer.append("成功获得明细凭证导出相关的参数，参数列表如下：" + enter);
            oStringBuffer.append("操作员 = " + cOperator + ",数据批次号 = " + mBatchNo + enter);
            tLogDeal.WriteLogTxt(oStringBuffer.toString());

            //开始校验更新凭证类型数据
            String tSQL = "select distinct CertificateID from FIVoucherDataDetail where BatchNo = '" + mBatchNo + "' " ;
            ExeSQL tExeSQL = new ExeSQL();
            SSRS oSSRS = new SSRS();
            oSSRS = tExeSQL.execSQL(tSQL);
            if(tExeSQL.mErrors.needDealError())
            {
                buildError("FICertificateGatherBL", "Check","明细凭证与大类影射定义校验SQL执行出错" + tExeSQL.mErrors.getFirstError());
                return false;
            }
            if(oSSRS.MaxRow<1)
            {
                String tErrInfo = "明细凭证类型：";
                 for(int i=1;i<=oSSRS.MaxRow;i++)
                 {
                      tErrInfo += "[" + oSSRS.GetText(i, 1) + "]";
                 }
                 tErrInfo += "不存在大类转换定义";
                 buildError("FICertificateGatherBL", "Check",tErrInfo);
                 return false;
            }
            //开始进行编码转换

            String tSql =  "update FIVoucherDataDetail a set a.StandByString1 = (select b.vouchertype from FIVoucherDef b where b.VoucherID = a.CertificateID) where a.batchno = '" + mBatchNo + "'";
            tExeSQL = new ExeSQL();
            if(!tExeSQL.execUpdateSQL(tSql))
            {
                buildError("FICertificateGatherBL","init","执行设置凭证大类数据SQL出错，信息为：" + tExeSQL.mErrors.getFirstError());
                tLogDeal.WriteLogTxt("执行设置凭证大类数据出错，信息为：" + tExeSQL.mErrors.getFirstError());
                return false;
            }
            if(tExeSQL.mErrors.needDealError())
            {
                buildError("FICertificateGatherBL","init","执行设置凭证大类数据SQL出错，信息为：" + tExeSQL.mErrors.getFirstError());
                tLogDeal.WriteLogTxt("执行设置凭证大类数据出错，信息为：" + tExeSQL.mErrors.getFirstError());
                return false;
            }

            return true;
        }
        catch (Exception ex)
        {
            buildError("FICertificateGatherBL","init","执行初始化异常，信息为：" + ex.getMessage());
            tLogDeal.WriteLogTxt("执行初始化异常，信息为：" + ex.getMessage());
            return false;
        }
    }
    
 

	/**
	 * 通过批次号查询财务接口表FIVoucherDataDetail,把相应数据放入业务系统中总帐接口表FIVoucherDataGather
	 * @param sBatchNoNo 批次号
	 * @throws SQLException
	 */
	public boolean moveDataToGather()
    {
        try
        {

            
            String oSQL = "";
            oSQL = "select StandByString1,AccountCode,FinItemType,sum(SumMoney),AccountDate,SaleChnl,ManageCom,ExecuteCom,RiskCode,CostCenter,BClient,MarketType,Pcont,budget,Currency,Years,PremiumType,FirstYear,PolYear,VersionNo from FIVoucherDataDetail where Batchno = '"
                   + mBatchNo + "' group by StandByString1,AccountCode,FinItemType,AccountDate,SaleChnl,ManageCom,ExecuteCom,RiskCode,CostCenter,BClient,MarketType,Pcont,budget,Currency,Years,PremiumType,FirstYear,PolYear,VersionNo";

            SSRS aSSRS = new SSRS();
            ExeSQL aExeSQL = new ExeSQL();
            aSSRS = aExeSQL.execSQL(oSQL);
            if (aExeSQL.mErrors.needDealError())
            {
                buildError("FICertificateGatherBL", "moveDataToGather", "执行凭证汇总SQL出错，信息为：" + aExeSQL.mErrors.getFirstError());
                tLogDeal.WriteLogTxt("执行凭证汇总出错，信息为：" + aExeSQL.mErrors.getFirstError());
                return false;
            }

            FIVoucherDataGatherSet tFIVoucherDataGatherSet = new FIVoucherDataGatherSet();

            for (int i = 1; i <= aSSRS.getMaxRow(); i++)
            {
            	FIVoucherDataGatherSchema tFIVoucherDataGatherSchema = new FIVoucherDataGatherSchema();
            	tFIVoucherDataGatherSchema.setBatchNo(mBatchNo);
            	tFIVoucherDataGatherSchema.setImportType("");
            	tFIVoucherDataGatherSchema.setVSerialNo(FinCreateSerialNo.getFGSerialNo());
            	tFIVoucherDataGatherSchema.setVersionNo(aSSRS.GetText(i, 20));

            	tFIVoucherDataGatherSchema.setVoucherType(aSSRS.GetText(i, 1));//凭证类型
            	tFIVoucherDataGatherSchema.setAccountCode(aSSRS.GetText(i, 2)); //科目代码
            	tFIVoucherDataGatherSchema.setDCFlag(aSSRS.GetText(i, 3));	
            	tFIVoucherDataGatherSchema.setSumMoney(aSSRS.GetText(i, 4));
            	tFIVoucherDataGatherSchema.setChargeDate(aSSRS.GetText(i, 5));
            	tFIVoucherDataGatherSchema.setChargeTime("");
            	tFIVoucherDataGatherSchema.setChinal(aSSRS.GetText(i, 6));
            	tFIVoucherDataGatherSchema.setDepCode(aSSRS.GetText(i, 7));//机构代码
            	tFIVoucherDataGatherSchema.setFinMangerCount(aSSRS.GetText(i, 8));//对方机构
            	tFIVoucherDataGatherSchema.setRiskCode(aSSRS.GetText(i, 9));
            	tFIVoucherDataGatherSchema.setCostCenter(aSSRS.GetText(i, 10));
            	
            	tFIVoucherDataGatherSchema.setBClient(aSSRS.GetText(i, 11));  //客户          	
            	tFIVoucherDataGatherSchema.setMarketType(aSSRS.GetText(i, 12));//市场类型
            	tFIVoucherDataGatherSchema.setPCont(aSSRS.GetText(i, 13));//期数
            	
            	tFIVoucherDataGatherSchema.setCashFlowNo(aSSRS.GetText(i, 14));
            	tFIVoucherDataGatherSchema.setCurrency(aSSRS.GetText(i, 15));
            	tFIVoucherDataGatherSchema.setYears(aSSRS.GetText(i, 16));
            	tFIVoucherDataGatherSchema.setPremiumType(aSSRS.GetText(i, 17));
            	tFIVoucherDataGatherSchema.setFirstYear(aSSRS.GetText(i, 18));
            	tFIVoucherDataGatherSchema.setPolYear(aSSRS.GetText(i, 19));
            	tFIVoucherDataGatherSchema.setReadState("0");//未凭证导出
            	
            	tFIVoucherDataGatherSchema.setMakeDate(PubFun.getCurrentDate());
            	tFIVoucherDataGatherSchema.setMakeTime(PubFun.getCurrentTime());
            	tFIVoucherDataGatherSchema.setModifyDate(PubFun.getCurrentDate());
            	tFIVoucherDataGatherSchema.setModifyTime(PubFun.getCurrentTime());
            	tFIVoucherDataGatherSchema.setOperator(mGI.Operator);
            	            	
                if(tFIVoucherDataGatherSchema.getSumMoney()==0)
                {
                	tFIVoucherDataGatherSchema.setReadState("-1");
                }

                
                tFIVoucherDataGatherSet.add(tFIVoucherDataGatherSchema);
            }

        	//提交后台处理
            MMap tmap = new MMap();
            VData tInputData = new VData();
            tmap.put(tFIVoucherDataGatherSet, "INSERT");
            tmap.put("update FIVoucheManage set State = '40' where BatchNo = '"+mBatchNo+"'", "INSERT");
            
            String update = "update FIVoucherDataDetail a set a.VSerialNo = (" +
            "select b.VSerialNo from FIVoucherDataGather b where b.batchno = a.batchno and a.StandByString1 = b.VoucherType and b.AccountCode =a.AccountCode and  b.DCFlag=a.FinItemType and  b.ChargeDate=a.AccountDate and  b.Chinal=a.SaleChnl and  b.DepCode=a.ManageCom and  b.FinMangerCount=a.ExecuteCom and  b.RiskCode=a.RiskCode and  b.CostCenter=a.CostCenter and  b.BClient=a.BClient and  b.MarketType=a.MarketType and  b.PCont=a.Pcont and  b.CashFlowNo=a.budget and  b.Currency=a.Currency and  b.Years=a.Years and  b.PremiumType=a.PremiumType and  b.FirstYear=a.FirstYear and  b.PolYear=a.PolYear "+		
            " ) where a.BatchNo = '"+mBatchNo+"' ";
            update = update+" " ;
            
            tmap.put(update, "INSERT");
            tInputData.add(tmap);
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(tInputData, ""))
            {
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                buildError("FICertificateGatherBL", "moveDataToGather", "批次号码为" + mBatchNo + "的汇总凭证数据保存在接口表时出错，提示信息为：" + tPubSubmit.mErrors.getFirstError());
                tLogDeal.WriteLogTxt("批次号码为" + mBatchNo + "的汇总凭证数据保存在接口表时出错，提示信息为：" + tPubSubmit.mErrors.getFirstError() + enter);
                return false;
            }
            return true;
        }
        catch (Exception ex)
        {
            buildError("FICertificateGatherBL", "moveDataToGather",  "批次号码为" + mBatchNo + "的汇总凭证数据处理过程出现异常,信息为：" + ex.getMessage());
            tLogDeal.WriteLogTxt("批次号码为" + mBatchNo + "的汇总凭证数据处理过程出现异常,信息为：" + ex.getMessage());
            return false;
        }

    }

    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    /**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FICertificateGatherBL tFICertificateGatherBL = new FICertificateGatherBL();
		GlobalInput mGI = new GlobalInput();
		mGI.Operator="001";
		tFICertificateGatherBL.dealGather("00000000000000000005",mGI);
	}

}
