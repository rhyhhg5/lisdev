package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>
 * ClassName: FIAssociatedItemDefBL
 * </p>
 * <p>
 * Description: 财务接口-账务规则管理-凭证模板定义
 * </p>
 * <p>
 * Copyright: Copyright (c) 2011
 * </p>
 * <p>
 * Company: sinosoft
 * </p>
 * @Database: 财务接口
 * @author：曹淑国
 * @version：3.0
 * @CreateDate：2011-08-11
 */
public class FIVoucherTempDefBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;
    private String mOperater;
    private String mManageCom;;

	private String mPageFlag;
	
	private MMap mMMap = new MMap();
	
	private TransferData mTransferData = new TransferData();
	
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	
	private FIVoucherDefSchema mFIVoucherDefSchema = new FIVoucherDefSchema();

	/**
	 * 传输数据的公共方法
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */

	public boolean submitData(VData cInputData, String cOperate)
	{
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData, cOperate))
		{
			return false;
		}
		if (!checkData())
		{
			return false;
		}
		// 进行业务处理
		if (!dealData())
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIVoucherTempDefBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败FIVoucherTempDefBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		// 准备往后台的数据
		if (!prepareOutputData())
		{
			return false;
		}

		if (!pubSubmit())
		{
			return false;
		}

		return true;
	}
	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData, String cOperate)
	{
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		mTransferData =  (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mPageFlag =  (String)mTransferData.getValueByName("PageFlag");
		mFIVoucherDefSchema =  (FIVoucherDefSchema) cInputData.getObjectByObjectName("FIVoucherDefSchema", 0);
		if (mGlobalInput == null) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "FIVoucherTempDefBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

		if (mTransferData == null) {
            // @@URL地址参数
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "FIVoucherTempDefBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
		
		if (mPageFlag == null) {
            // @@URL地址参数
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "FIVoucherTempDefBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        //获得操作员编码
		mOperater = mGlobalInput.Operator;
        if ((mOperater == null) || mOperater.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "FIVoucherTempDefBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operater失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
        if ((mManageCom == null) || mManageCom.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "FIVoucherTempDefBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        //添加、更新、删除操作参数
        mOperate = cOperate;
        if ((mOperate == null) || mOperate.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "FIVoucherTempDefBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate任务节点编码失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
		return true;
	}
	
	
	private boolean checkData()
	{

		if(mFIVoucherDefSchema==null)
		{
			
			return false;
		}
		if("".equals(mOperate)||mOperate==null)
		{
			return false;
		}
		return true;
	}
	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 * @return
	 */
	private boolean dealData()
	{		
		if ("insert".equals(mOperate))
		{
			System.out.println("进入新增模块！");
			mMMap.put(mFIVoucherDefSchema, "INSERT");
		}

		else if ("update".equals(mOperate))
		{

			System.out.println("进入修改模块！");
			CheckFMTable();
			mMMap.put(mFIVoucherDefSchema, "UPDATE");

		}

		else if ("delete".equals(mOperate))
		{

			System.out.println("进入删除模块！");
			ExeSQL es = new ExeSQL();
			String count = es.getOneValue("select count(1) from FIVoucherBn where voucherid='"+mFIVoucherDefSchema.getVoucherID()+"'");

			if(Integer.parseInt(count)>=1)
			{
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "FIVoucherTempDefBL";
				tError.functionName = "prepareData";
				tError.errorMessage = "该凭证仍有关联业务交易数据，请先进行除删！";
				this.mErrors.addOneError(tError);
				return false;
			}
			CheckFMTable();
			mMMap.put(mFIVoucherDefSchema, "DELETE");

		}

		return true;
	}	
	
	/**
	 * 判断是否需要往FM表中插入数据 
	 * */
	private void CheckFMTable()
	{
		FIRulesVersionTraceDB tFIRulesVersionTraceDB = new FIRulesVersionTraceDB();
		FIRulesVersionTraceSet tFIRulesVersionTraceSet = new FIRulesVersionTraceSet();
		
		FMVoucherDefSchema tFMVoucherTypeDefSchema = new FMVoucherDefSchema();
		FMVoucherDefDB tFMVoucherTypeDefDB = new FMVoucherDefDB();
		FMVoucherDefSet tFMVoucherTypeDefSet = new FMVoucherDefSet();

		FIVoucherDefSchema tFIVoucherTypeDefSchema = new FIVoucherDefSchema();
		FIVoucherDefDB tFIVoucherTypeDefDB = new FIVoucherDefDB();
		FIVoucherDefSet tFIVoucherTypeDefSet = new FIVoucherDefSet();
		
		String VersionNo = mFIVoucherDefSchema.getVersionNo();
		String VoucherID = mFIVoucherDefSchema.getVoucherID();
		
		//=====获取当前FI表中的数据======
		tFIVoucherTypeDefDB.setVersionNo(VersionNo);
		tFIVoucherTypeDefDB.setVoucherID(VoucherID);
		tFIVoucherTypeDefSet=tFIVoucherTypeDefDB.query();
		if(tFIVoucherTypeDefSet==null)
		{
			System.out.println("无法查询到凭证"+VoucherID+"！");
		}
		tFIVoucherTypeDefSchema = tFIVoucherTypeDefSet.get(1);
		//============================
		//=========查询维护编码=========
		String sql = "select * from FIRulesVersionTrace where versionno = '"+VersionNo+"' and MaintenanceState = '02'";
		
		tFIRulesVersionTraceSet = tFIRulesVersionTraceDB.executeQuery(sql);
		
		if(tFIRulesVersionTraceSet==null)
		{
			System.out.println("版本号"+VersionNo+"没有正在修改状态的维护申请！");
		}
		String maintno = tFIRulesVersionTraceSet.get(1).getMaintenanceno();
		
		System.out.println("版本号"+VersionNo+"的维护申请号为："+maintno);
		//============================
		
		//查询FM表中是否有数据
		tFMVoucherTypeDefDB.setMaintNo(maintno);
		tFMVoucherTypeDefDB.setVersionNo(VersionNo);
		tFMVoucherTypeDefDB.setVoucherID(VoucherID);
		tFMVoucherTypeDefSet = tFMVoucherTypeDefDB.query();
		
		//如果FM表中没有数据，则新插入
		if(tFMVoucherTypeDefSet.size()==0)
		{
			tFMVoucherTypeDefSchema.setMaintNo(maintno);
			tFMVoucherTypeDefSchema.setVersionNo(VersionNo);
			tFMVoucherTypeDefSchema.setVoucherID(VoucherID);
			tFMVoucherTypeDefSchema.setVoucherName(tFIVoucherTypeDefSchema.getVoucherName());
			tFMVoucherTypeDefSchema.setVoucherType(tFIVoucherTypeDefSchema.getVoucherType());
			tFMVoucherTypeDefSchema.setRemark(tFIVoucherTypeDefSchema.getRemark());
			tFMVoucherTypeDefSchema.setState(tFIVoucherTypeDefSchema.getState());
			
			
			mMMap.put(tFMVoucherTypeDefSchema, "INSERT");
		}
		
	}
	/**
	 * 准备需要保存的数据
	 * @return boolean
	 */
	private boolean prepareOutputData()
	{
		try
		{
			mInputData.clear();
			mInputData.add(mMMap);
		}
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIVoucherTempDefBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	/**
	 * 提交数据
	 * @return
	 */
	private boolean pubSubmit()
	{
		// 进行数据提交
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, ""))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "FIVoucherTempDefBL";
			tError.functionName = "PubSubmit.submitData";
			tError.errorMessage = "数据提交失败FIVoucherTempDefBL-->pubSubmit!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}

	public VData getResult()
	{
		return this.mResult;
	}

	public static void main(String[] args)
	{

	}	
}
