package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.fininterface_v3.core.check.DataCheckService;
import com.sinosoft.lis.fininterface_v3.core.secondary.FISecondaryDealService;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.FIDataExtractDefSchema;
import com.sinosoft.lis.vschema.FIDataExtractDefSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class FIDataExtPlanExeJTUI {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    public GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData ;
    
    private final String enter = "\r\n"; // 换行符    
    
    private String mStartDate;
    private String mEndDate;
    /** 数据操作字符串 */
    private String mOperate;
    
    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	// 将操作数据拷贝到本类中
    	this.mOperate = cOperate;
       
		if(!getInputData(cInputData))
		{
			return false;
		}
		
        //二次处理
		secondaryDeal(cInputData);
		
       return true;
    }   

    /**
	  * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 *
	 */
	private boolean getInputData(VData cInputData)
	{
		mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0);
		mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData",0);
			
		if (mGlobalInput == null) 
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIDataExtPlanExeJTUI";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据失败!";
			this.mErrors.addOneError(tError);

			return false;
		}
		if (mTransferData == null) 
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIDataExtPlanExeJTUI";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输数据失败!";
			this.mErrors.addOneError(tError);

			return false;
		}
		mStartDate = (String)mTransferData.getValueByName("StartDate");
		mEndDate = (String)mTransferData.getValueByName("EndDate");
				
		return true;
	}
    
    /**
     * 二次处理
     * @param cInputData
     * @return
     */
    private boolean secondaryDeal(VData cInputData)
    {
    	try 
    	{
	    	FIOperationLog tLogDeal = new FIOperationLog(mGlobalInput.Operator,"20");
	  	   
	    	FISecondaryDealService tFISecondaryDealService = new FISecondaryDealService(tLogDeal);
	    	System.out.println("---FISecondaryDealService BEGIN---");
	    	
	      	//二次处理
	    	tLogDeal.WriteLogTxt("=========开始执行--长险应收计提--二次处理=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
	    	
	  		   if (!tFISecondaryDealService.dealServiceJT(cInputData)) 
	  		   {
					// @@错误处理
					this.mErrors.copyAllErrors(tFISecondaryDealService.mErrors);
					CError tError = new CError();
					tError.moduleName = "FIDataExtPlanExeJTUI";
					tError.functionName = "secondaryDeal";
					tError.errorMessage = "二次处理失败!";
					this.mErrors.addOneError(tError);
					tLogDeal.Complete(false);
					return false;
		      	}
	    	
  		   	tLogDeal.WriteLogTxt("=========长险应收计提--二次处理结束=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
  	  		 
  		   	tLogDeal.WriteLogTxt("=========执行数据校检=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
	      	DataCheckService mDataCheckService = new DataCheckService(tLogDeal);
			//对二次处理后的数据进行校检
			mDataCheckService.qualityDataCheck("20",cInputData);
			
			tLogDeal.WriteLogTxt("=========数据校检结束=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
			
			tLogDeal.Complete(true);
	    	
    	}
	    catch (Exception e) 
    	{
	      	// TODO Auto-generated catch block
	      	e.printStackTrace();
      	} 
    	
    	return true;
    }
    
    
    
    
    
    public VData getResult()
    {
        return this.mResult;
    }
    
    public static void main(String[] args)
    {
    	FIDataExtPlanExeJTUI tFIDataExtPlanExeJTUI = new FIDataExtPlanExeJTUI();
    	
    	VData tVData = new VData();
    	GlobalInput tG = new GlobalInput();
    	tG.Operator= "001";
    	tG.ManageCom="86";
    	TransferData tTransferData = new TransferData();
    	
    	tTransferData.setNameAndValue("StartDate","2011-10-29");
    	tTransferData.setNameAndValue("EndDate","2011-10-29");
    	tTransferData.setNameAndValue("FIExtType","00");
    	
    	FIDataExtractDefSet tFIDataExtractDefSet = new FIDataExtractDefSet();
    	FIDataExtractDefSchema tFIDataExtractDefSchema = new FIDataExtractDefSchema();
    	tFIDataExtractDefSchema.setVersionNo("00000000000000000001");
    	tFIDataExtractDefSchema.setRuleDefID("0000000001");
    	tFIDataExtractDefSet.add(tFIDataExtractDefSchema);
    	
    	tVData.add(tG);
    	tVData.add(tTransferData);
    	tVData.add(tFIDataExtractDefSet);
    	
    	tFIDataExtPlanExeJTUI.submitData(tVData,"00");

    	
    }
}
