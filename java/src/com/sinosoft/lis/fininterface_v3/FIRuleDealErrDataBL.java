package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.db.FIRuleDealErrLogDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIRuleDealErrLogSchema;
import com.sinosoft.lis.vschema.FIRuleDealErrLogSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

/*******************************************************************************
 * 忽略错误处理
 * 
 * @author lijs
 * @createTime 2008-08-26
 * 
 */
public class FIRuleDealErrDataBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/***/
	private FIRuleDealErrLogSchema mFIRuleDealErrLogSchema = new FIRuleDealErrLogSchema();

	private Reflections mReflections = new Reflections();


	/***************************************************************************
	 * 提供一条错误日志信息记录 根据对应的该笔信息记录进行删除对应的数据和备份数据 并同时修改错误数据处理状态
	 * 
	 * @param oVData
	 * @param strOperator
	 * @return
	 */
	public boolean submitData(VData oVData, String strOperator) 
	{

		/***********************************************************************
		 * 得到对应的错误数据源
		 */
		if (!getInputData(oVData)) 
		{
			return false;
		}

		if (!DealData()) {

			return false;
		}

		return true;
	}


	private boolean getInputData(VData oData) 
	{

		mFIRuleDealErrLogSchema.setSchema((FIRuleDealErrLogSchema) oData.getObjectByObjectName("FIRuleDealErrLogSchema", 0));
		if (mFIRuleDealErrLogSchema == null) 
		{
			buildError("getInputData", "前台传入的待删除数据为空!");
			return false;
		}

		return true;
	}

	
	/***************************************************************************
	 * 根据对应的明细批次信息以及对应的业务号码\凭证类型得到对应的错误数据
	 * 
	 * @return
	 */
	private boolean DealData() 
	{
		if (mFIRuleDealErrLogSchema != null) 
		{
			try 
			{
				String tSQL = "select * from FIRuleDealErrLog where ErrSerialNo='"+mFIRuleDealErrLogSchema.getErrSerialNo()+"'";
				FIRuleDealErrLogSet tFIRuleDealErrLogSet = new FIRuleDealErrLogDB().executeQuery(tSQL);
				
				if(tFIRuleDealErrLogSet!=null&&tFIRuleDealErrLogSet.size()>0)
				{
					mFIRuleDealErrLogSchema = tFIRuleDealErrLogSet.get(1);
					
					PubSubmit oPubSubmit = new PubSubmit();
					MMap oMap = new MMap();
					
					oMap.put("update FIRuleDealErrLog set DealState = '3' where ErrSerialNo = '"+mFIRuleDealErrLogSchema.getErrSerialNo()+"'", "UPDATE");
					oMap.put("update FIAboriginalMain set CheckFlag = '00' where IndexCode = '"+mFIRuleDealErrLogSchema.getIndexCode()+"' and IndexNo = '"+mFIRuleDealErrLogSchema.getBusinessNo()+"'", "UPDATE");
					oMap.put("update FIAboriginalGenDetail set CheckFlag = '00' where IndexCode = '"+mFIRuleDealErrLogSchema.getIndexCode()+"' and IndexNo = '"+mFIRuleDealErrLogSchema.getBusinessNo()+"'", "UPDATE");
					oMap.put("update FIAbStandardData set CheckFlag = '00' where IndexCode = '"+mFIRuleDealErrLogSchema.getIndexCode()+"' and IndexNo = '"+mFIRuleDealErrLogSchema.getBusinessNo()+"'", "UPDATE");
					oMap.put("update FIVoucherDataDetail a set a.CheckFlag = '00' where exists(select 1 from FIAbStandardData b where a.SerialNo = b.SerialNo and IndexCode = '"+mFIRuleDealErrLogSchema.getIndexCode()+"' and IndexNo = '"+mFIRuleDealErrLogSchema.getBusinessNo()+"')", "UPDATE");
					
					VData oData = new VData();
					oData.add(oMap);
					if (!oPubSubmit.submitData(oData, "DELETE"))
					{
						buildError("PubSubmit", "提交数据更新失败! ");
						return false;
					}
				}
				else
				{
					buildError("DealData", "未查询到错误日志数据！");
					return false;
				}
			} catch (Exception e) {
				buildError("DealData", "处理数据失败: " + e.getMessage());
				return false;
			}
		}

		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "FIRuleDealErrDataBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
		System.out.print(szErrMsg);
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {

		FIRuleDealErrDataBL oDataBL = new FIRuleDealErrDataBL();
		VData oData = new VData();

		FIRuleDealErrLogSchema oDealErrLogSchema = new FIRuleDealErrLogSchema();
		oDealErrLogSchema.setErrSerialNo("111111");

		System.out.println(oDealErrLogSchema.encode());
		oData.addElement(oDealErrLogSchema);
		oData.addElement("11");

		oDataBL.submitData(oData, "Test");
	}
}
