/**
 * <p>ClassName: FIFeeTypeDefBL.java </p>
 * <p>Description: 元数据定义 </p>
 * <p>Copyright: Copyright (c) 2009 </p>
 * <p>Company: </p>
 * @author
 * @version 1.0
 * @CreateDate：2011/10/14
 */
 
//包名
package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;


public class FIFeeTypeDefBL  {
 /** 错误处理类，每个需要错误处理的类中都放置该类 */
 public  CErrors mErrors=new CErrors();
 private VData mResult = new VData();
 /** 往后面传输数据的容器 */
 private VData mInputData= new VData();
 /** 全局数据 */
 private GlobalInput mGlobalInput =new GlobalInput() ;
 /** 数据操作字符串 */
 private String mOperate;
 
 /** 业务处理相关变量 */
 //private ××××Schema t****Schema=new ****Schema();
 
 private MMap map=new MMap();
 private FIVoucherBnFeeSet tFIFeeTypeDefSet = new FIVoucherBnFeeSet();
 private FIVoucherBnFeeSchema tFIFeeTypeDefSchema = new FIVoucherBnFeeSchema(); 
 public FIFeeTypeDefBL(){

 }

 /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
 public boolean submitData(VData cInputData,String cOperate){
  //将操作数据拷贝到本类中
  this.mInputData = cInputData;
  this.mOperate =cOperate;
  
  //得到外部传入的数据,将数据备份到本类中
  if (!getInputData()){
      return false;
  }
  if (!checkData()){
      return false;
  }

  //进行业务处理
  if (!dealData()){
      return false;
  }

  if (!prepareOutputData()){
      return false;
  }

  PubSubmit tPubSubmit = new PubSubmit();
  
  System.out.println("Start FIFeeTypeDefBL Submit...");
  
  if (!tPubSubmit.submitData(mInputData, null)){
    this.mErrors.copyAllErrors(tPubSubmit.mErrors);
    return false;
  }

  mInputData=null;
  System.out.println("End FIFeeTypeDefBL Submit...");
  return true;
  
 }


 /**
  * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 *
 */
 private boolean getInputData(){
	 mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
	 tFIFeeTypeDefSet.set((FIVoucherBnFeeSet) mInputData
             .getObjectByObjectName("FIVoucherBnFeeSet", 1));
  return true;
 }

 private boolean checkData(){
  return true;
 }

 /**
  * 根据前面的输入数据，进行BL逻辑处理
  * 如果在处理过程中出错，则返回false,否则返回true
  */
 private boolean dealData(){
	 

         if (tFIFeeTypeDefSet != null)
         {
             map.put(tFIFeeTypeDefSet, "DELETE&INSERT");
         }
     

  return true;
 }

 private boolean prepareOutputData(){
	 try
		{
			mInputData.clear();
			mInputData.add(map);
		}
		catch(Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIFeeTypeDefBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
 }

 public static void main(String[] args) {
 }
}

