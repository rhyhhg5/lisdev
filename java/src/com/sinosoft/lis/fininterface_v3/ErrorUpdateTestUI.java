package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.fininterface_v3.ErrorUpdateTestBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.FIDataExtractDefSchema;
import com.sinosoft.lis.vschema.FIDataExtractDefSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ErrorUpdateTestUI {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    public GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData ; 
    
    private final String enter = "\r\n"; // 换行符    
    
    /** 数据操作字符串 */
    private String mOperate;
    
    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	// 将操作数据拷贝到本类中
    	this.mOperate = cOperate;
       
		if(!getInputData(cInputData))
		{
			return false;
		}
		ErrorUpdateTestBL tErrorUpdateTestBL = new ErrorUpdateTestBL();
		tErrorUpdateTestBL.submitData(cInputData, cOperate);
		
       return true;
    }   

    /**
	  * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 *
	 */
	private boolean getInputData(VData cInputData)
	{
		mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0);
		//mFIDataExtractDefSet = (FIDataExtractDefSet)cInputData.getObjectByObjectName("FIDataExtractDefSet",0);
		mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData",0);
//		String indexno = (String)mTransferData.getValueByName("IndexidNo");
//		System.out.println("---索引值为：---"+indexno);
			
		if (mGlobalInput == null) 
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ErrorUpdateTestUI";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据失败!";
			this.mErrors.addOneError(tError);

			return false;
		}
		if (mTransferData == null) 
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ErrorUpdateTestUI";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输数据失败!";
			this.mErrors.addOneError(tError);

			return false;
		}
		
		return true;
	}
    
    
    public VData getResult()
    {
        return this.mResult;
    }
    
    public static void main(String[] args)
    {
    	ErrorUpdateTestUI tFIDataExtPlanExeUI = new ErrorUpdateTestUI();
    	
    	VData tVData = new VData();
    	GlobalInput tG = new GlobalInput();
    	tG.Operator= "001";
    	tG.ManageCom="86";
    	TransferData tTransferData = new TransferData();
    	
    	tTransferData.setNameAndValue("StartDate","2011-10-29");
    	tTransferData.setNameAndValue("EndDate","2011-10-29");
    	tTransferData.setNameAndValue("FIExtType","00");
    	
    	FIDataExtractDefSet tFIDataExtractDefSet = new FIDataExtractDefSet();
    	FIDataExtractDefSchema tFIDataExtractDefSchema = new FIDataExtractDefSchema();
    	tFIDataExtractDefSchema.setVersionNo("00000000000000000001");
    	tFIDataExtractDefSchema.setRuleDefID("0000000001");
    	tFIDataExtractDefSet.add(tFIDataExtractDefSchema);
    	
    	tVData.add(tG);
    	tVData.add(tTransferData);
    	tVData.add(tFIDataExtractDefSet);
    	
    	tFIDataExtPlanExeUI.submitData(tVData,"00");

    	
    }
}
