package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class FIRuleDealErrAppBL 
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 往界面传输数据的容器 */
	private VData mResult = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	private MMap mMMap = new MMap();
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	/** 业务处理相关变量 */
	private FIRuleDealErrLogSchema mFIRuleDealErrLogSchema = new FIRuleDealErrLogSchema();
	private FIRuleDealLogSchema mFIRuleDealLogSchema = new FIRuleDealLogSchema();

	public FIRuleDealErrAppBL()
	{}

	/**
	 * 传输数据的公共方法
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */

	public boolean submitData(VData cInputData, String cOperate)
	{
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData))
		{
			return false;
		}
		if (!checkdata())
		{
			return false;
		}
		// 进行业务处理
		if (!dealData())
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIRuleDealErrAppBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败FIRuleDealErrAppBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}
		// 准备往后台的数据
		if (!prepareOutputData())
		{
			return false;
		}

		if (!pubSubmit())
		{
			return false;
		}

		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData)
	{
		this.mFIRuleDealErrLogSchema.setSchema((FIRuleDealErrLogSchema) cInputData.getObjectByObjectName("FIRuleDealErrLogSchema", 0));
		this.mFIRuleDealLogSchema.setSchema((FIRuleDealLogSchema) cInputData.getObjectByObjectName("FIRuleDealLogSchema", 0));
		
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		if (this.mGlobalInput == null)
		{
			CError tError = new CError();
			tError.moduleName = "FIRuleDealErrAppBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "您输入的管理机构或者操作员代码为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		if (this.mFIRuleDealErrLogSchema == null)
		{
			CError tError = new CError();
			tError.moduleName = "FIRuleDealErrAppBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "mFIRuleDealErrLogSchema！";
			this.mErrors.addOneError(tError);
			return false;
		}
		if (this.mFIRuleDealLogSchema == null)
		{
			CError tError = new CError();
			tError.moduleName = "FIRuleDealErrAppBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "mFIRuleDealLogSchema！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean checkdata()
	{

		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 * @return
	 */
	private boolean dealData()
	{
		if (this.mOperate.equals("INSERT"))
		{
			System.out.println("进入新增模块！");
			mMMap.put(mFIRuleDealErrLogSchema, "INSERT");
			mMMap.put(mFIRuleDealLogSchema, "INSERT");
			
		}
		return true;
	}
	
	/**
	 * 准备需要保存的数据
	 * @return boolean
	 */
	private boolean prepareOutputData()
	{
		try
		{
			mInputData.clear();
			mInputData.add(mMMap);
		}
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIRuleDealErrAppBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 提交数据
	 * @return
	 */
	private boolean pubSubmit()
	{
		// 进行数据提交
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, ""))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "FIRuleDealErrAppBL";
			tError.functionName = "PubSubmit.submitData";
			tError.errorMessage = "数据提交失败FIRuleDealErrAppBL-->pubSubmit!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}

	public VData getResult()
	{
		return this.mResult;
	}

	public static void main(String[] args)
	{

	}



}
