package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LDBankSchema;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class FIAddBankFinItemCodeBL 
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
     public CErrors mErrors = new CErrors();
//     private VData mResult = new VData();
     private String mBankCode ;
     private String mManageCom;
     private String mChildBankName;
     private String mHeadBankName;
     private String mGetBankAccNo;
     private String mPayBankAccNo;
     private String mChildBankCode;
     private TransferData mAddElement = new TransferData(); //获取时间
     private GlobalInput mGlobalInput = new GlobalInput(); //全局变量
     private LDBankSchema mLDBankSchema = new LDBankSchema();
     private LDCodeSchema mLDCodeSchema = new LDCodeSchema();
     private String mResult;
public FIAddBankFinItemCodeBL() {
}

/**
 传输数据的公共方法
 */
public boolean submitData(VData cInputData, String cOperate) {
    try{
    if (!cOperate.equals("CHILD")&& !cOperate.equals("HEAD") &&!cOperate.equals("REPAIR") && !cOperate.equals("DELETE")) {
        buildError("submitData", "不支持的操作字符串");
        return false;
    }
    if (!getInputData(cInputData)) {
        return false;
    }
    mResult="";
   
    if (cOperate.equals("CHILD")) { //打印提数
        if (!setChildBank()) {
            return false;
        }
    }
    
    
    else if (cOperate.equals("HEAD"))
    {
        if (!setHeadBank()) {
              return false;
        }

    }
    else if (cOperate.equals("REPAIR"))
    {
        if (!updateChildBank()) {
              return false;
        }

    }
    else if (cOperate.equals("DELETE"))
    {
        if (!delChildBank()) {
              return false;
        }

    }
    }catch(Exception e) {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "FinBankAddBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据处理错误! " + e.getMessage();
        this.mErrors .addOneError(tError);
        return false;
      }
    return true;
}

/**
 * 从输入数据中得到所有对象
 * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData) { //打印付费
    //全局变量
    mAddElement = (TransferData) cInputData.getObjectByObjectName("TransferData",0);
    mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
            "GlobalInput", 0));
    if (mGlobalInput == null) {
        buildError("getInputData", "没有得到足够的信息！");
        return false;
    }
    mBankCode = (String) mAddElement.getValueByName("BankCode");
    mManageCom = (String) mAddElement.getValueByName("ManageCom");
    mChildBankName = (String) mAddElement.getValueByName("ChildBankName");
    mHeadBankName = (String) mAddElement.getValueByName("HeadBankName");
    mGetBankAccNo = (String) mAddElement.getValueByName("GetBankAccNo");
    mPayBankAccNo = (String) mAddElement.getValueByName("PayBankAccNo");
    mChildBankCode= (String) mAddElement.getValueByName("ChildBankCode");
    System.out.println("mBankCode:"+mBankCode+" mManageCom:"+mManageCom+" mChildBankName:"+mChildBankName+" mHeadBankName:"+mHeadBankName+" mGetBankAccNo:"+mGetBankAccNo+" mPayBankAccNo:"+mPayBankAccNo+" mChildBankCode"+mChildBankCode);
    return true;
}

public String getResult() {
    return mResult;
}

public LDBankSchema getLDBankSchema(){
    return mLDBankSchema;
}
private void buildError(String szFunc, String szErrMsg) {
    CError cError = new CError();
    cError.moduleName = "FinBankAddBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
}
//添加子银行
private boolean setChildBank() {
    String mBigBankCode;
    String mChildBankCode;
    ExeSQL tExeSQL = new ExeSQL();
    SSRS tSSRS = new SSRS();
    String sql = "select count(*) from ldbank where bankname = '" +
                 mChildBankName + "'";
    tSSRS = tExeSQL.execSQL(sql);
    if (mManageCom.substring(2, 4).equals("11"))
    {
        mBigBankCode = mBankCode + "01";
    }
    else if (mManageCom.substring(2, 4).equals("94"))
    {
        mBigBankCode = mBankCode + "02";
    }
    else
    {
        mBigBankCode = mBankCode + mManageCom.substring(2, 4);
    }
    if (tSSRS.GetText(1,1).equals("0"))
    {
        StringBuffer childsql = new StringBuffer(255);
        childsql.append("select case when a is null then '01' when a<10 then '0'||to_char(a) else to_char(a) end from "
                   +"(select max(integer(substr(bankcode,length('"+mBigBankCode+"')+1)))+1 a "
                   +"from ldbank where 1=1 "
                   +"and bankcode like '"+mBigBankCode+"%' "
                   +"and Length(rtrim(bankcode))>length('"+mBigBankCode+"')) u");
        String MaxNum = (new ExeSQL()).getOneValue(childsql.toString());
        if (MaxNum.equals(""))
        {
            buildError("FinBankAddBL->ChildBank","取子银行最大值失败");
            return false;
        }
        if (MaxNum.equals("10000"))
        {
            buildError("FinBankAddBL->ChildBank","已超过子银行最大数目");
            return false;
        }

        mChildBankCode = mBigBankCode+MaxNum;
        //添插入参数
        mLDBankSchema.setBankCode(mChildBankCode);
        mLDBankSchema.setBankName(mChildBankName);
        mLDBankSchema.setComCode(mManageCom);
        mLDBankSchema.setAccNo(mGetBankAccNo);
        mLDBankSchema.setPayAccNo(mPayBankAccNo);

        LDBankDB mLDBankDB = new LDBankDB();
        mLDBankDB.setSchema(mLDBankSchema);
        if (!mLDBankDB.insert())
        {
            buildError("FinBankAddBL->ChildBank","插入子银行失败");
            return false;
        }
        mResult=mChildBankCode;


    }else{
        buildError("FinBankAddBL->ChildBank","已经存在该银行名称");
        return false;
    }
    return true;
}
//添加总行
private boolean setHeadBank()
{
   String mHeadBankCode;
   ExeSQL tExeSQL = new ExeSQL();
   SSRS tSSRS = new SSRS();
   String sql = "select count(*) from LDCODE where codetype = 'banknum' and codename='" +
                mHeadBankName + "'";
   tSSRS = tExeSQL.execSQL(sql);
   if (tSSRS.GetText(1,1).equals("0"))
   {
       StringBuffer headsql = new StringBuffer(255);
       headsql.append("select case when a<10 then '0'||to_char(a) else to_char(a) end from "
                 +"(select cast(coalesce(max(code),'00') as integer )+1 a from ldcode where 1=1 "
                 +"and codetype = 'banknum') u");
       String MaxNum = (new ExeSQL()).getOneValue(headsql.toString());
       if (MaxNum.equals(""))
       {
           buildError("FinBankAddBL->HeadBank","取总行代码最大数失败");
           return false;
       }
       //添插入参数
       mLDCodeSchema.setCodeType("banknum");
       mLDCodeSchema.setCode(MaxNum);
       mLDCodeSchema.setCodeName(mHeadBankName);

       LDCodeDB mLDCodeDB = new LDCodeDB();
       mLDCodeDB.setSchema(mLDCodeSchema);
       if (!mLDCodeDB.insert())
       {
           buildError("FinBankAddBL->HeadBank","插入总银行失败");
           return false;
       }
       mResult = MaxNum;
    }
   return true;
}
private boolean updateChildBank()
{
    mLDBankSchema.setBankCode(mChildBankCode);
    mLDBankSchema.setBankName(mChildBankName);
    mLDBankSchema.setComCode(mManageCom);
    mLDBankSchema.setAccNo(mGetBankAccNo);
    mLDBankSchema.setPayAccNo(mPayBankAccNo);

    LDBankDB mLDBankDB = new LDBankDB();
    mLDBankDB.setSchema(mLDBankSchema);
    if (!mLDBankDB.update())
    {
        buildError("FinBankAddBL->ChildBank","更新子银行失败");
        return false;
    }
    return true;
}
private boolean delChildBank()
{
    mLDBankSchema.setBankCode(mChildBankCode);
    mLDBankSchema.setBankName(mChildBankName);
    mLDBankSchema.setComCode(mManageCom);
    mLDBankSchema.setAccNo(mGetBankAccNo);
    mLDBankSchema.setPayAccNo(mPayBankAccNo);

    LDBankDB mLDBankDB = new LDBankDB();
    mLDBankDB.setSchema(mLDBankSchema);
    if (!mLDBankDB.delete())
    {
        buildError("FinBankAddBL->ChildBank","删除子银行失败");
        return false;
        }
    return true;
}
//生成XML函数
private boolean productXml(String msql)
{
    return true;
}

}
