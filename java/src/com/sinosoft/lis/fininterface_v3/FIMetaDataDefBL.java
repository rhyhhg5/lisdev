package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

public class FIMetaDataDefBL 
{

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	private MMap mMMap = new MMap();

	// private String CurrentDate = PubFun.getCurrentDate();

	// private String CurrentTime = PubFun.getCurrentTime();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	// private TransferData mTransferData = new TransferData();

	// private Reflections mReflections = new Reflections();

	// private JdbcUrl mJdbcUrl = new JdbcUrl();

	// private ExeSQL mExeSQL = new ExeSQL();

	/** 业务处理相关变量 */
	private FIMetadataDefSchema mFIMetadataDefSchema = new FIMetadataDefSchema();

	// private FIAssociatedDirectItemDefSet mFIAssociatedDirectItemDefSet = new
	// FIAssociatedDirectItemDefSet();

	private String mVersionNo = "";

	private String mColumnID = "";

	public FIMetaDataDefBL()
	{}

	public String getVersionNo()
	{
		return mVersionNo;
	}

	public String getColumnID()
	{
		return mColumnID;
	}

	/**
	 * 传输数据的公共方法
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */

	public boolean submitData(VData cInputData, String cOperate)
	{
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData))
		{
			return false;
		}
		if (!checkdata())
		{
			return false;
		}
		// 进行业务处理
		if (!dealData())
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIMetaDataDeBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败FIMetaDataDeBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}
		// 准备往后台的数据
		if (!prepareOutputData())
		{
			return false;
		}
		// if (this.mOperate.equals("QUERY||MAIN"))
		// {
		// this.submitquery();
		// }
		if (!pubSubmit())
		{
			return false;
		}

		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData)
	{
		this.mFIMetadataDefSchema.setSchema((FIMetadataDefSchema) cInputData.getObjectByObjectName("FIMetadataDefSchema", 0));
		
//		FITableColumnDefSet tFITableColumnDefSet = new FITableColumnDefSet();
//		FITableColumnDefDB tFITableColumnDefDB = new FITableColumnDefDB();
//		tFITableColumnDefDB.setTableID("FIAboriginalData");
//		tFITableColumnDefDB.setColumnMark(mFIAssociatedDirectItemDefSchema.getSourceColumnID());
//		tFITableColumnDefSet = tFITableColumnDefDB.query();
//		if (tFITableColumnDefSet == null || tFITableColumnDefSet.size() > 1)
//		{
//			System.out.println("上游数据来源字段定义错误，请重新定义");
//		}
//		mFIAssociatedDirectItemDefSchema.setSourceTableID(tFITableColumnDefSet.get(1).getColumnID());
		
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		if (this.mGlobalInput == null)
		{
			CError tError = new CError();
			tError.moduleName = "FIMetaDataDeBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "您输入的管理机构或者操作员代码为空！";
			this.mErrors.addOneError(tError);
			return false;
		}
		if (this.mFIMetadataDefSchema == null)
		{
			CError tError = new CError();
			tError.moduleName = "FIMetaDataDeBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "mFIMetadataDefSchema！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean checkdata()
	{

		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 * @return
	 */
	private boolean dealData()
	{
		if (this.mOperate.equals("INSERT||MAIN"))
		{
			System.out.println("进入新增模块！");
			mMMap.put(mFIMetadataDefSchema, "INSERT");
		}

		else if (this.mOperate.equals("UPDATE||MAIN"))
		{

			System.out.println("进入修改模块！");
			mMMap.put(mFIMetadataDefSchema, "UPDATE");

		}

		else if (this.mOperate.equals("DELETE||MAIN"))
		{

			System.out.println("进入删除模块！");
			mMMap.put(mFIMetadataDefSchema, "DELETE");

		}

		return true;
	}

	/**
	 * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
	 * @return
	 */
	// private boolean submitquery()
	// {
	// this.mResult.clear();
	// System.out.println("Start FIAssociatedDirectItemDefBLQuery Submit...");
	// FIAssociatedDirectItemDefDB tFIAssociatedDirectItemDefDB = new
	// FIAssociatedDirectItemDefDB();
	// tFIAssociatedDirectItemDefDB.setSchema(this.mFIAssociatedDirectItemDefSchema);
	// this.mFIAssociatedDirectItemDefSet = tFIAssociatedDirectItemDefDB.query();
	// this.mResult.add(this.mFIAssociatedDirectItemDefSet);
	// System.out.println("End FIAssociatedDirectItemDefBLQuery Submit...");
	// // 如果有需要处理的错误，则返回
	// if (tFIAssociatedDirectItemDefDB.mErrors.needDealError())
	// {
	// // @@错误处理
	// this.mErrors.copyAllErrors(tFIAssociatedDirectItemDefDB.mErrors);
	// CError tError = new CError();
	// tError.moduleName = "FIAssociatedDirectItemDefBL";
	// tError.functionName = "submitData";
	// tError.errorMessage = "数据提交失败!";
	// this.mErrors.addOneError(tError);
	// return false;
	// }
	// mInputData = null;
	// return true;
	// }
	/**
	 * 准备需要保存的数据
	 * @return boolean
	 */
	private boolean prepareOutputData()
	{
		try
		{
			// this.mInputData = new VData();
			// this.mInputData.add(this.mGlobalInput);
			// this.mInputData.add(this.mFIAssociatedDirectItemDefSchema);
			mInputData.add(mMMap);
		}
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIMetaDataDeBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 提交数据
	 * @return
	 */
	private boolean pubSubmit()
	{
		// 进行数据提交
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, ""))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "FIMetaDataDeBL";
			tError.functionName = "PubSubmit.submitData";
			tError.errorMessage = "数据提交失败FIMetaDataDeBL-->pubSubmit!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}

	public VData getResult()
	{
		return this.mResult;
	}

	public static void main(String[] args)
	{

	}


}
