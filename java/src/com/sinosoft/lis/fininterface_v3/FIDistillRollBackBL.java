/**
 * <p>ClassName: FIBusTypeDefBL.java </p>
 * <p>Description: 业务交易定义 </p>
 * <p>Copyright: Copyright (c) 2009 </p>
 * <p>Company: </p>
 * @author 董健
 * @version 1.0
 * @CreateDate：2011/8/25
 */
 
//包名
package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.FIAbStandardDataBSet;
import com.sinosoft.lis.vschema.FIAboriginalMainBSet;
import com.sinosoft.lis.vschema.FIDataBaseLinkSet;
import com.sinosoft.lis.vschema.FIErrHandingItemSet;
import com.sinosoft.lis.vschema.FIVoucherDataDetailBSet;
import com.sinosoft.lis.db.FIAbStandardDataBDB;
import com.sinosoft.lis.db.FIAboriginalMainBDB;
import com.sinosoft.lis.db.FIDataBaseLinkDB;
import com.sinosoft.lis.db.FIErrHandingItemDB;
import com.sinosoft.lis.db.FIVoucherDataDetailBDB;
import com.sinosoft.lis.fininterface_v3.tools.dblink.PubSubmitForInterface;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


public class FIDistillRollBackBL  
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors=new CErrors();
	private VData mResult = new VData();
	/** 往后面传输数据的容器 */
	private VData mInputData= new VData();
	/** 往dblink后面传输数据的容器 */
	private VData dblink_InputData= new VData();
	/** 全局数据 */
	private GlobalInput mGlobalInput =new GlobalInput() ;
	private TransferData mTransferData = new TransferData();
	/** 数据操作字符串 */
	private String mOperate;
	//流水号
	private String mErrAppNo="";
	//回滚节点
	private String mProcessNode="";
	//当前节点
	private String cProcessNode="";	
	//业务类型
	private String mIndexCode="";	
	//业务号码
	private String mIndexNo="";	
	//批次号
	private String mBatchNo="";	
	private String mErrSerialNo;
	
	private MMap map=new MMap();
	private MMap dblink_map=new MMap();
	private FIErrHandingItemSchema mFIErrHandingItemSchema;
	
	public FIDistillRollBackBL()
	{}

	/**
	* 传输数据的公共方法
	* @param: cInputData 输入的数据
	*         cOperate 数据操作
	* @return:
	*/
	public boolean submitData(VData cInputData,String cOperate)
	{
		//将操作数据拷贝到本类中
		this.mInputData = cInputData;
		this.mOperate =cOperate;
		
		System.out.println("---FIDistillRollBackBL getInputData---");
		if (!getInputData(cInputData, cOperate)) 
		{
			return false;
		}
	     
		if (!checkData())
		{
			return false;
		}
		//进行业务处理
		if (!dealData())
		{
			return false;
		}		
		if(!pubSubmit())
		{
			return false;
		}

		System.out.println("End FIDistillRollBackBL Submit...");
		mInputData=null;
		return true;
	}


	/**
	  * 从输入数据中得到所有对象
	  *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	  *
	  */
	private boolean getInputData(VData cInputData, String cOperate)
	{
		mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
		mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);

		if (mTransferData == null) 
		{
            buildError("FIDistillRollBackBL","getInputData", "前台传输TransferData数据失败!");
            return false;
        }
		if (mTransferData == null) 
		{
            buildError("FIDistillRollBackBL","getInputData", "前台传输TransferData数据失败!");
            return false;
        }
		
		mErrAppNo = (String) mTransferData.getValueByName("ErrAppNo");
		mProcessNode = (String) mTransferData.getValueByName("ProcessNode");
		
		return true;
	}
	
	/**
	 * 数据校检
	 * @return
	 */
	private boolean checkData()
	{
		
		if (mGlobalInput == null) 
		{
            buildError("FIDistillRollBackBL","checkData", "前台传输全局公共数据失败!");
            return false;
        }
		if("".equals(mErrAppNo)||mErrAppNo==null)
		{
            buildError("FIDistillRollBackBL","checkData", "前台传输处理申请号失败!");
            return false;
		}
		if("".equals(mProcessNode)||mProcessNode==null)
		{
            buildError("FIDistillRollBackBL","checkData", "前台传输回滚节点失败!");
            return false;
		}
		
		FIErrHandingItemSet tFIErrHandingItemSet = new FIErrHandingItemDB().executeQuery("select * from  FIErrHandingItem where ErrAppNo='"+mErrAppNo+"' and DealType = '02'");
		
		if(tFIErrHandingItemSet==null||tFIErrHandingItemSet.size()<1)
		{
            buildError("FIDistillRollBackBL","checkData", "获取处理任务失败!");
            return false;
		}
		else
		{
			mFIErrHandingItemSchema = tFIErrHandingItemSet.get(1);
			mErrSerialNo = mFIErrHandingItemSchema.getErrSerialNo();
			cProcessNode = mFIErrHandingItemSchema.getCallPointID();
			mIndexCode = mFIErrHandingItemSchema.getIndexCode();
			mIndexNo = mFIErrHandingItemSchema.getIndexNo();
			
		}
		
		mBatchNo = new ExeSQL().getOneValue("select BatchNo from FIVoucherDataDetail a where exists(select 1 from FIAbStandardData b where a.SerialNo = b.SerialNo and b.indexcode = '"+mIndexCode+"' and b.IndexNo = '"+mIndexNo+"')");;
		
		return true;
	}

	 /**
	  * 根据前面的输入数据，进行BL逻辑处理
	  * 如果在处理过程中出错，则返回false,否则返回true
	  */
	private boolean dealData()
	{	
		//回滚到凭证转换
		if("30".equals(mProcessNode))
		{
			if(mBatchNo==null||"".equals(mBatchNo))
			{
				buildError("FIDistillRollBackBL","rollBack40", "获取汇总凭证数据批次失败!");
				return false;
			}
			rollBack40(mBatchNo);//回滚凭证汇总数据
		}
		//回滚到数据归档
		if("10".equals(mProcessNode))
		{
			rollBack40(mBatchNo); //回滚凭证汇总数据
			String tSQL = "select '"+mErrAppNo+"' as TaskNo ,a.* from FIVoucherDataDetail a where exists(select 1 from FIAbStandardData b where a.SerialNo = b.SerialNo and b.indexcode = '"+mIndexCode+"' and b.IndexNo = '"+mIndexNo+"') ";
			FIVoucherDataDetailBSet tFIVoucherDataDetailBSet = new FIVoucherDataDetailBDB().executeQuery(tSQL);
			if(tFIVoucherDataDetailBSet==null||tFIVoucherDataDetailBSet.size()<1)
			{
				buildError("FIDistillRollBackBL","rollBack30", "未查询到要回滚的明细凭证数据!");
				return false;
			}
			rollBack30(mIndexCode,mIndexNo); //回滚转换凭证数据
		}

		//回滚到数据提取
		if("00".equals(mProcessNode))
		{
			rollBack40(mBatchNo);//回滚凭证汇总数据
			rollBack30(mIndexCode,mIndexNo); //回滚转换凭证数据
			String tSQL = "select '"+mErrAppNo+"' as TaskNo ,a.* from FIAbStandardData a where a.indexcode = '"+mIndexCode+"' and a.IndexNo = '"+mIndexNo+"' ";
			FIAbStandardDataBSet tFIAbStandardDataBSet = new FIAbStandardDataBDB().executeQuery(tSQL);
			if(tFIAbStandardDataBSet==null||tFIAbStandardDataBSet.size()<1)
			{
				buildError("FIDistillRollBackBL","rollBack10", "未查询到已归档的数据!");
				return false;
			}
			rollBack10(mIndexCode,mIndexNo); //回滚归档数据
			
		}
		
		//全部回滚
		if("99".equals(mProcessNode))
		{
			rollBack40(mBatchNo);//回滚凭证汇总数据
			rollBack30(mIndexCode,mIndexNo); //回滚转换凭证数据
			rollBack10(mIndexCode,mIndexNo); //回滚归档数据
			String tSQL = "select '"+mErrAppNo+"' as TaskNo ,a.* from FIAboriginalMain a where a.indexcode = '"+mIndexCode+"' and a.IndexNo = '"+mIndexNo+"' ";
			FIAboriginalMainBSet tFIAboriginalMainBSet = new FIAboriginalMainBDB().executeQuery(tSQL);
			if(tFIAboriginalMainBSet==null||tFIAboriginalMainBSet.size()<1)
			{
				buildError("FIDistillRollBackBL","rollBack00", "未查询到已经提取的原始业务数据!");
				return false;
			}
			rollBack00(mIndexCode,mIndexNo); //回滚提取数据
		}
		
		map.put(" update FIErrHandingItem set AppState='99' where ErrAppNo = '"+mErrAppNo+"' and DealType = '02'", "UPDATE"); //处理完毕
		map.put(" update FIRuleDealErrLog set DealState='1' where ErrSerialNo = '"+mErrSerialNo+"' ", "UPDATE"); //处理完毕
		
		return true;
	}
 
	/**
	 * 提交数据
	 * @return
	 */
	private boolean pubSubmit()
	{
		// 进行数据提交
		mInputData.clear();
		mInputData.add(map);
		
		if(dblink_InputData!=null)
		{
			FIDataBaseLinkDB tFIDataBaseLinkDB = new FIDataBaseLinkDB();
			String strKeyDefSQL = "select * from FIDataBaseLink  ";
	        FIDataBaseLinkSet tFIDataBaseLinkSet = tFIDataBaseLinkDB.executeQuery(strKeyDefSQL);
	        if(tFIDataBaseLinkSet == null || tFIDataBaseLinkSet.size()==0)
	        {
	            System.out.println("未配置异地数据源，请检查FIDataBaseLink异地数据源配置表！");
	            buildError("FIDistillRollBackBL","submitData", "数据提交失败,未配置异地数据源，请检查FIDataBaseLink异地数据源配置表！FIDistillRollBackBL-->tPubSubmitForInterface!");
	            return false;
	        }	
	        
	        for(int i=1;i<=tFIDataBaseLinkSet.size();i++)
	        {
	        	 PubSubmitForInterface tPubSubmitForInterface = new PubSubmitForInterface();
			        if (!tPubSubmitForInterface.submitData(dblink_InputData, tFIDataBaseLinkSet.get(i)))
			        {
			        	buildError("FIDistillRollBackBL","submitData", "数据提交失败FIDistillRollBackBL-->tPubSubmitForInterface!");
						return false;
			        }
	        }
	        
		}
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, ""))
		{
			buildError("FIDistillRollBackBL","submitData", "数据提交失败FIDistillRollBackBL-->pubSubmit!");
			return false;
		}
		return true;
	}
	
	/**
	 * 回滚已经提取的数据
	 * @param indexCode
	 * @param indexNo
	 * @return
	 */
	private boolean rollBack00(String indexCode,String indexNo)
	{
		String tSQL = "select '"+mErrAppNo+"' as TaskNo ,a.* from FIAboriginalMain a where a.indexcode = '"+indexCode+"' and a.IndexNo = '"+indexNo+"' ";
		FIAboriginalMainBSet tFIAboriginalMainBSet = new FIAboriginalMainBDB().executeQuery(tSQL);
		if(tFIAboriginalMainBSet==null||tFIAboriginalMainBSet.size()<1)
		{
			//buildError("FIDistillRollBackBL","rollBack00", "未查询到已经提取的原始业务数据!");
			System.out.println("未查询到已经提取的原始业务数据!");
			return true;
		}
		map.put(tFIAboriginalMainBSet, "INSERT"); //数据备份
		
		String tSQL1  = "delete from FIAboriginaInfo where indexcode = '"+indexCode+"' and IndexNo = '"+indexNo+"' ";
		String tSQL2  = "delete from FIAboriginalMain where indexcode = '"+indexCode+"' and IndexNo = '"+indexNo+"' ";
		String tSQL3  = "delete from FIAboriginalGenDetail where indexcode = '"+indexCode+"' and IndexNo = '"+indexNo+"' ";
		
		map.put(tSQL1, "DELETE");
		map.put(tSQL2, "DELETE");
		map.put(tSQL3, "DELETE");
		
		dblink_map.put(tSQL1, "DELETE");
		dblink_map.put(tSQL2, "DELETE");
		dblink_map.put(tSQL3, "DELETE");
		
		dblink_InputData.clear();
		dblink_InputData.add(dblink_map);
		
		return true;
	}
	/**
	 * 回滚归档的数据
	 * @param indexCode
	 * @param indexNo
	 * @return
	 */
	private boolean rollBack10(String indexCode,String indexNo)
	{
		String tSQL = "select '"+mErrAppNo+"' as TaskNo ,a.* from FIAbStandardData a where a.indexcode = '"+indexCode+"' and a.IndexNo = '"+indexNo+"' ";
		FIAbStandardDataBSet tFIAbStandardDataBSet = new FIAbStandardDataBDB().executeQuery(tSQL);
		if(tFIAbStandardDataBSet==null||tFIAbStandardDataBSet.size()<1)
		{
			//buildError("FIDistillRollBackBL","rollBack10", "未查询到已归档的数据!");
			System.out.println("未查询到已归档的数据!");
			return true;
		}
		map.put(tFIAbStandardDataBSet, "INSERT"); //数据备份		
		map.put("delete from FIAbStandardData where indexcode = '"+indexCode+"' and IndexNo = '"+indexNo+"'", "DELETE");
		map.put("delete from FIAboriginalGenAtt where indexcode = '"+indexCode+"' and IndexNo = '"+indexNo+"'", "DELETE");
		map.put("delete from FIAboriginalMainAtt where indexcode = '"+indexCode+"' and IndexNo = '"+indexNo+"'", "DELETE");
		map.put("update FIAboriginalGenDetail set state = '00',CheckFlag='00' where indexcode = '"+indexCode+"' and IndexNo = '"+indexNo+"'", "UPDATE");
		map.put("update FIAboriginalMain set state = '02',CheckFlag='00' where indexcode = '"+indexCode+"' and IndexNo = '"+indexNo+"'", "UPDATE");
		
		return true;
	}

	/**
	 * 回滚凭证转换数据
	 * @param indexCode
	 * @param indexNo
	 * @return
	 */
	private boolean rollBack30(String indexCode,String indexNo)
	{
		String tSQL = "select '"+mErrAppNo+"' as TaskNo ,a.* from FIVoucherDataDetail a where exists(select 1 from FIAbStandardData b where a.SerialNo = b.SerialNo and b.indexcode = '"+indexCode+"' and b.IndexNo = '"+indexNo+"') ";
		FIVoucherDataDetailBSet tFIVoucherDataDetailBSet = new FIVoucherDataDetailBDB().executeQuery(tSQL);
		if(tFIVoucherDataDetailBSet==null||tFIVoucherDataDetailBSet.size()<1)
		{
			//buildError("FIDistillRollBackBL","rollBack30", "未查询到要回滚的明细凭证数据!");
			System.out.println("未查询到要回滚的明细凭证数据!");
			return true;
		}
		map.put(tFIVoucherDataDetailBSet, "INSERT"); //数据备份		
		map.put("delete from FIVoucherDataDetail where indexcode = '"+indexCode+"' and IndexNo = '"+indexNo+"' ", "DELETE");
		//更新状态为未凭证转换
		map.put("update FIAbStandardData set state = '00' ,CheckFlag='00' where indexcode = '"+indexCode+"' and IndexNo = '"+indexNo+"' ", "UPDATE");
		
		return true;
	}
	
	/**
	 * 回滚凭证汇总数据
	 * @param tBatchNo
	 * @return
	 */
	private boolean rollBack40(String tBatchNo)
	{
		if(tBatchNo==null||"".equals(tBatchNo))
		{
			//buildError("FIDistillRollBackBL","rollBack40", "获取汇总凭证数据批次失败!");
			System.out.println("该数据还未生成汇总凭证!");
			return true;
		}
		map.put("delete from FIVoucherDataGather where batchno = '"+tBatchNo+"' ", "DELETE"); 
		map.put("delete from FIVoucheCheckList where batchno = '"+tBatchNo+"' ", "DELETE");
		
		map.put("update FIVoucherDataDetail a set a.CheckFlag='00' where exists(select 1 from FIAbStandardData b where a.SerialNo = b.SerialNo and b.indexcode = '"+mIndexCode+"' and b.IndexNo = '"+mIndexNo+"') ", "UPDATE"); 
		map.put("update FIVoucheManage set State='30' where batchno = '"+tBatchNo+"' ", "UPDATE"); 
		
		return true;
	}
	
	public VData getResult()
	{
		return null;
	}

	/**
	 * 错误信息
	 * @param szModuleName
	 * @param szFunc
	 * @param szErrMsg
	 */
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    
	public static void main(String[] args) 
	{
		String tSQL = "select '11111111' as TaskNo ,a.* from FIAboriginalMain a where a.indexcode = '03' and a.IndexNo = '32010299405' ";
		FIAboriginalMainBSet tFIAboriginalMainBSet = new FIAboriginalMainBDB().executeQuery(tSQL);
		
		System.out.println(tFIAboriginalMainBSet.get(1).getPhysicalTable());
	}
}

