package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.EasyEdorTbBL;
import com.sinosoft.lis.bq.EasyEdorTbUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LICodeTransSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * @date 2013-05-06 
 * @author 卢翰林
 *
 */

public class BFDepartmentCodenewInputUI{
	
	
	 private BFDepartmentCodenewInputBL mBFDepartmentCodenewInputBL = null;

	    /**
	     * 构造函数
	     * @param gi GlobalInput
	     * @param edorNo String
	     */
	    public BFDepartmentCodenewInputUI()
	    {
	    	mBFDepartmentCodenewInputBL = new BFDepartmentCodenewInputBL();
	    }

	    /**
	     * 调用业务逻辑类
	     * @param operator String
	     * @return boolean
	     */
	    public boolean submitData(VData data, String operator)
	    {
	        if (!mBFDepartmentCodenewInputBL.submitData(data, operator))
	        {
	            return false;
	        }
	        return true;
	    }

	    /**
	     * 得到错误
	     * @return String
	     */
	    public CErrors getErrors()
	    {
	        return mBFDepartmentCodenewInputBL.mErrors;
	    }
}