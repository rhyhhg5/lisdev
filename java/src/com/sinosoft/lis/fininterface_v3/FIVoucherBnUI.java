package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.*;

public class FIVoucherBnUI
{


	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;
	
	private String mPageFlag;
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	
	private TransferData mTransferData = new TransferData();
	
	private FIVoucherBnBL mFIVoucherFeeDefBL = new FIVoucherBnBL();
	
	private FIVoucherBnSchema mFIVoucherBnSchema = new FIVoucherBnSchema();

	/**
	 * 传输数据的公共方法
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate)
	{

		 this.mOperate = cOperate;
 
	       System.out.println("---FIVoucherTypeBusBL UI BEGIN---");
	       if (mFIVoucherFeeDefBL.submitData(cInputData, mOperate) == false) {
	           // @@错误处理
	           this.mErrors.copyAllErrors(mFIVoucherFeeDefBL.mErrors);
	           CError tError = new CError();
	           tError.moduleName = "FIVoucherTypeBusBL";
	           tError.functionName = "submitData";
	           tError.errorMessage = "数据提交失败!";
	           this.mErrors.addOneError(tError);
	           mResult.clear();
	           return false;
	       }
	       
	       if (mOperate.equals("INSERT||MAIN")){
	           this.mResult.clear();
	           this.mResult = mFIVoucherFeeDefBL.getResult();
	       }
	       return true;
	}
	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData)
	{
		// 全局变量
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		mTransferData =  (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mPageFlag =  (String)mTransferData.getValueByName("PageFlag");
			
		if (mGlobalInput == null)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIVoucherTempDefUI";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的信息！";
			this.mErrors.addOneError(tError);
			return false;
		}
		if (mPageFlag == null||"".equals(mPageFlag))
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIVoucherTempDefUI";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的信息！";
			this.mErrors.addOneError(tError);
			return false;
		}		
		return true;
	}	
	/**
	 * 根据前面的输入数据，进行UI逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 * @return
	 */
	private boolean dealData(VData cInputData)
	{
		if("X".equals(mPageFlag))
		{
			
		}
		else if("C".equals(mPageFlag))
		{
			mFIVoucherFeeDefBL.submitData(cInputData, mOperate);

			System.out.println("End FIVoucherTempDefUI Submit...");
			// 如果有需要处理的错误，则返回
			if (mFIVoucherFeeDefBL.mErrors.needDealError())
			{
				// @@错误处理
				this.mErrors.copyAllErrors(mFIVoucherFeeDefBL.mErrors);
				CError tError = new CError();
				tError.moduleName = "FIVoucherTempDefUI";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		else
		{
			return false ;
		}

		
		
		return true;
	}

	public VData getResult()
	{
		return this.mResult;

	}	
	public static void main(String[] args)
	{
		
	}

}
