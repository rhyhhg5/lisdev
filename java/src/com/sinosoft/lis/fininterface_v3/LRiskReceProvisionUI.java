package com.sinosoft.lis.fininterface_v3;                        
                                                                 
/**                                                              
 * <p>Title: AgentSystem</p>                                     
 *                                                               
 * <p>Description: </p>                                          
 *                                                               
 * <p>Copyright: Copyright (c) 2005</p>                          
 *                                                               
 * <p>Company: Sinosoft</p>                                      
 *                                                               
 * @author                                                       
 * @version 1.0                                                  
 */                                                              
                                                                 
import com.sinosoft.utility.CErrors;                             
import com.sinosoft.utility.VData;                               
                                                                 
public class LRiskReceProvisionUI                                
{                                                                
    /**������Ϣ����*/                                            
    public CErrors mErrors = new CErrors();                      
                                                                 
    public LRiskReceProvisionUI()                                
    {                                                            
        System.out.println("LRiskReceProvisionUI");              
    }                                                            
                                                                 
    public boolean submitData(VData cInputData, String cOperator)
    {                                                            
    	LRiskReceProvisionBL bl = new LRiskReceProvisionBL();      
        if(!bl.submitData(cInputData, cOperator))                
        {                                                        
            mErrors.copyAllErrors(bl.mErrors);                   
            return false;                                        
        }                                                        
                                                                 
        return true;                                             
    }                                                            
                                                                 
    public static void main(String[] args)                       
    {                                                            
                                                                 
    }                                                            
}                                                                
                                                                 
                                                                 