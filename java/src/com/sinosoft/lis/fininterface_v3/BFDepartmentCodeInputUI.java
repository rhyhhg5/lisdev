package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.task.BuildError;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * 
 * @date 2013-05-06 
 * @author 卢翰林
 *
 */

public class BFDepartmentCodeInputUI{
	/**错误处理类*/
	public CErrors mErrors = new CErrors();
	
	/**结果集*/
	private VData mResult = new VData();
	
	/**全局输入数据*/
	private GlobalInput mGlobalInput = new GlobalInput();
	
	public BFDepartmentCodeInputUI(){}
	
	/**数据传输*/
	public boolean submitData(VData mInputData, String mOperate)
	{
		try
		{
			if(!mOperate.equals("INSERT"))
			{
				buildError("submitData", "不支持此操作字符串");
				return false;
			}
			//得到外部传入的数据，将数据备份到本类中
			if(!getInputData(mInputData))
			{
				return false;
			}
			//业务处理
			if(!dealData())
			{
				return false;
			}
			//准备往后台传输的数据
            if (!prepareOutputData(mInputData))
            {
                return false;
            }
            
            BFDepartmentCodeInputBL  mBFDepartmentCodeInputBL =  new BFDepartmentCodeInputBL();
            System.out.println("Start InputUI Submit ...");
            if (!mBFDepartmentCodeInputBL.submitData(mInputData, mOperate))
            {
                if (mBFDepartmentCodeInputBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(mBFDepartmentCodeInputBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData","mInputUI，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
            	this.mResult.clear();
            	this.mResult = mBFDepartmentCodeInputBL.getResult();
                return true;
            }
		}catch(Exception e)
		{
			e.printStackTrace();
	        buildError("submitData",e.toString());
	        return false;
		}
	}

	private VData getResult() 
	{
		return this.mResult;
	}

	/**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
	private boolean prepareOutputData(VData mInputData) 
	{
		return true;
	}

	/**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
	private boolean dealData() 
	{
		return true;
	}

	/**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
	private boolean getInputData(VData mInputData) 
	{
		return true;
	}

	private void buildError(String func, String msg) 
	{
		CError error = new CError();
		error.functionName = func;
		error.moduleName = "InputUI";
		error.errorMessage = msg;
		mErrors.addOneError(error);
	}
}