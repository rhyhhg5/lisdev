package com.sinosoft.lis.fininterface_v3;



import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FICodeTransSchema;
import com.sinosoft.lis.schema.LICodeTransSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @date 2013-05-06 
 * @author 卢翰林
 *
 */

public class BFDepartmentCodenewInputBL 
{
	/**错误处理类*/
	public CErrors mErrors = new CErrors();
	
	/** 往后面传输数据的容器 */
	private VData mInputData= new VData();
	/**全局数据*/
	private GlobalInput mGlobalInput = new GlobalInput();
	private MMap tmap = new MMap();
    private String BJHManageCom = "";
    private String operateCom = "";
    private String caiwuname = "";
    private String caiwucode = "";
    private String Codea = "";
    
    SSRS tSSRS = new SSRS();
    SSRS ttSSRS = new SSRS();
    SSRS dtSSRS = new SSRS();
    SSRS rtSSRS = new SSRS();
    SSRS sSSRS = new SSRS();
	/**操作字符串*/
	private String mOperate;
	/**业务处理相关变量 */
	private LICodeTransSchema mLICodeTransSchema = new LICodeTransSchema();
	private FICodeTransSchema mFICodeTransSchema = new FICodeTransSchema();
	
	public BFDepartmentCodenewInputBL(){}
	
	/**错误处理*/
	public void buildError(String func, String msg)
	{
		CError error = new CError();
		error.functionName = func;
		error.moduleName = "InputBL";
		error.errorMessage = msg;
		mErrors.addOneError(error);
	}
	/**
	 * 传输数据的公共方法
	 * @param: mInputData 输入的数据
	 *         mOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData mInputData, String mOperate)
	{
	this.mInputData = mInputData;
	this.mOperate = mOperate;

		//获取数据
		if(!getInputData(mInputData))
		{
			buildError("getInputData", "业务处理错误!");
			return false;
		}
		//检查数据
		if(!checkData())
		{
			buildError("checkData", "业务处理错误!");
			return false;
		}
		//业务处理
		if(!dealData())
		{
			buildError("dealData", "业务处理错误!");
			return false;
		}
		//验证银代佣金机构是否存在
		if (this.mOperate.equals("INSERT")){
		if(!checkmanage())
		{
			buildError("dealData", "业务处理错误!");
			return false;
		}
		}
		
		//准备后台数据
//	if(!perpareOutputData())
//		{
//			buildError("prepareOutputData","业务处理错误！");
//			return false;
//		}
//	//提交数据
//		PubSubmit ps = new PubSubmit();
//		System.out.println("SubmitData===============wait");
//		if(!ps.submitData(mInputData, mOperate))
//		{
//		if (ps.mErrors.needDealError())
//        {
//            this.mErrors.copyAllErrors(ps.mErrors);
//            return false;
//        }
//        else
//        {
//            buildError("submitData","PubSubmit，但是没有提供详细的出错信息!");
//            return false;
//        }
//	}
//	System.out.println("SubmitData===============success");
	return true;
	}
	
	private boolean checkmanage(){
		  MMap pmap = new MMap();
	      VData pInputData = new VData();
		ExeSQL pExeSQL = new ExeSQL();
		PubSubmit pPubSubmit = new PubSubmit();  
    	String ySQL = "select * from ficodetrans a where a.codetype ='ManageCom' and a.code='"+operateCom+"'and substr("+operateCom+",1,4) in (select distinct substr(code,1,4) from ficodetrans  where 1=1 and codetype ='JXTJManageCom' )  " +
    			"and length(trim(a.code)) >='6' and not exists(select 1 from ficodetrans b where 1=1 and b.codetype ='JXTJManageCom' and substr(b.code,1,4) in (select distinct substr(code,1,4) from ficodetrans  where 1=1 and codetype ='JXTJManageCom' )  " +
    			"and a.code = b.code and length(trim(b.code)) >='6')";
        System.out.println(ySQL);
        sSSRS = pExeSQL.execSQL(ySQL);         
       if(sSSRS.getMaxRow()>0)
      {
    	   pmap.put("INSERT INTO ficodetrans VALUES ( 'JXTJManageCom', '"+operateCom+"', '"+caiwuname+"', '"+caiwucode+"', 'ManageCom', 'ManageCom')","INSERT");
      }
       pInputData.add(pmap);
       if (!pPubSubmit.submitData(pInputData, ""))
       {
           this.mErrors.copyAllErrors(pPubSubmit.mErrors);
           buildError( "gatherData", "数据出错，提示信息为：" + pPubSubmit.mErrors.getFirstError());         
           pmap=null;
           pInputData=null;
           return false;
       }
       
       pmap=null;
   	pInputData=null;
		return true;
	}
	
	
	private boolean perpareOutputData() 
	{
		
		try
		{  	
			mInputData.clear();
			mInputData.add(tmap);
			//System.out.println("(iiiiiiiiiiiii2222222222:"+mLICodeTransSchema.getCode());
		}
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BFDepartmentCodeInputBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() 
	{//System.out.println("3333333:");
		boolean tReturn = true;
		String ic = "IC"+ caiwucode;
        MMap tmap = new MMap();
        VData tInputData = new VData();
        
        if (this.mOperate.equals("INSERT"))
		{
        	String inserta = "INSERT INTO licodetrans VALUES ('BJHManageCom','"+operateCom+"','"+caiwuname+"','"+caiwucode+"',NULL)";
        	String insertb = "INSERT INTO licodetrans VALUES ('ManageCom','"+operateCom+"','"+caiwuname+"','"+caiwucode+"',NULL)";
        	String insertc = "INSERT INTO ficodetrans  VALUES ('ManageCom','"+operateCom+"','"+caiwuname+"','"+caiwucode+"','ManageCom','ManageCom')";
        	
      	    tmap.put(inserta, "INSERT");
      	    tmap.put(insertb, "INSERT");
    	    tmap.put(insertc, "INSERT");
        	
    	    
        	ExeSQL tExeSQL = new ExeSQL();
        	String tSQL = "select * from ficodetrans where codetype = 'FXManageCom' and code = '"+caiwucode+"'";
            System.out.println(tSQL);
          tSSRS = tExeSQL.execSQL(tSQL);         
          if(tSSRS.getMaxRow()<= 0)
          {
        	  String srt = caiwucode.substring(0,2);
        	  String insertd = "INSERT INTO ficodetrans  select bb.codetype as codetype,'"+caiwucode+"' as code , bb.codename as codename ,bb.codealias as codealias, ''as othersign ,'' as versinno from ficodetrans bb where codetype = 'FXManageCom' and code like '%"+srt+"%' fetch first row only ";
        	  String inserte = "INSERT INTO ficodetrans  VALUES ('ICManageCom','"+caiwucode+"','"+caiwuname+"','"+ic+"','ICManageCom','ICManageCom')";
        	  
        	  tmap.put(insertd, "INSERT");
        	  tmap.put(inserte, "INSERT");
          }
			
			tReturn = true;
		}
		if (this.mOperate.equals("UPDATE"))
		{
			String srt = caiwucode.substring(0,2);
			String srtt = Codea.substring(0,2);
			System.out.print(srt);
			System.out.print(srtt);
			tmap.put("update licodetrans set codealias = '"+caiwucode+"',codename = '"+caiwuname+"'  where codetype in('BJHManageCom','ManageCom') and code = '"+operateCom+"'","UPDATE");
			tmap.put("update ficodetrans set codealias = '"+caiwucode+"',codename = '"+caiwuname+"'  where codetype in ('ManageCom','JXTJManageCom') and code = '"+operateCom+"'","UPDATE");			
			
        	ExeSQL ttExeSQL = new ExeSQL();
        	String ttSQL = "select 1 from ficodetrans where codetype = 'ManageCom' and codealias = '"+Codea+"' and code not in ('"+operateCom+"')";
            System.out.println(ttSQL);
            ttSSRS = ttExeSQL.execSQL(ttSQL);  
            
        	ExeSQL dtExeSQL = new ExeSQL();
        	String dtSQL = "select 1 from ficodetrans where codetype = 'FXManageCom' and code = '"+caiwucode+"'";
            System.out.println(dtSQL);
            dtSSRS = dtExeSQL.execSQL(dtSQL);
            
        	ExeSQL rtExeSQL = new ExeSQL();
        	String rtSQL = "select 1 from ficodetrans where codetype = 'FXManageCom' and code = '"+Codea+"'";
            System.out.println(rtSQL);
            rtSSRS = rtExeSQL.execSQL(rtSQL);
            
          if((ttSSRS.getMaxRow()> 0) & (dtSSRS.getMaxRow()<=0))
          {
        	  String insertd = "INSERT INTO ficodetrans  select bb.codetype as codetype,'"+caiwucode+"' as code , bb.codename as codename ,bb.codealias as codealias, ''as othersign ,'' as versinno from ficodetrans bb where codetype = 'FXManageCom' and code like '%"+srt+"%' fetch first row only ";
        	  String inserte = "INSERT INTO ficodetrans  VALUES ('ICManageCom','"+caiwucode+"','"+caiwuname+"','"+ic+"','ICManageCom','ICManageCom')";
        	  
        	  tmap.put(insertd, "INSERT");
        	  tmap.put(inserte, "INSERT");
          }
          
          else if (((ttSSRS.getMaxRow()<=0) & (rtSSRS.getMaxRow()>0) )& (dtSSRS.getMaxRow()>0)){
        	  
        	  String deletea = "delete from ficodetrans where codetype = 'FXManageCom' and code = '"+Codea+"'";
        	  String deleteb = "delete from ficodetrans where codetype = 'ICManageCom' and code = '"+Codea+"'";
        	  tmap.put(deletea, "DELETE");
        	  tmap.put(deleteb, "DELETE");
          }          
          else if (((ttSSRS.getMaxRow()<=0) & (rtSSRS.getMaxRow()>0) )& (dtSSRS.getMaxRow()<=0)){
        	  tmap.put("update ficodetrans set code = '"+caiwucode+"',codename = '"+caiwuname+"',codealias = '"+ic+"'  where codetype = 'ICManageCom' and code = '"+Codea+"'","UPDATE");
        	  tmap.put("update ficodetrans set code = '"+caiwucode+"' ,codename = (select codename from ficodetrans where codetype = 'FXManageCom' and code like '%"+srt+"%' fetch first 1 rows only) where codetype = 'FXManageCom' and code = '"+Codea+"'","UPDATE");
          }
          
			tReturn = true;
		}
		
		tInputData.add(tmap);
        System.out.println(tmap);       
        PubSubmit tPubSubmit = new PubSubmit();     
        if (!tPubSubmit.submitData(tInputData, ""))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buildError( "gatherData", "数据出错，提示信息为：" + tPubSubmit.mErrors.getFirstError());         
            tmap=null;
            tInputData=null;
            return false;
        }
        
    	tmap=null;
        tInputData=null;
		return tReturn;
	}


	private boolean checkData() 
	{
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * 如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData mInputData) 
	{//System.out.println("5555555");
		try 
		{
			mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
	        TransferData tf = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
	        
	        if (mGlobalInput == null || tf == null)
	        {
	        	buildError("getInputData","数据维护出错");
	            return false;
	        }
	        BJHManageCom = (String) tf.getValueByName("BJHManageCom"); 
	        operateCom = (String) tf.getValueByName("operateCom");
	        caiwuname = (String) tf.getValueByName("caiwuname");
	        caiwucode = (String) tf.getValueByName("caiwucode");
	        Codea = (String) tf.getValueByName("Codea");
	        
		} catch (Exception e) 
		{
		    e.printStackTrace();
		    buildError("prepareData","报错原因：获取业务数据失败！");
		    System.out.println("getInputData==========false");
		    return false;
		}
		return true;
	}
}
