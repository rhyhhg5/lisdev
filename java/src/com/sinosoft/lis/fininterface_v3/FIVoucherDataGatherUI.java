package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.fininterface_v3.core.check.DataCheckService;
import com.sinosoft.lis.fininterface_v3.core.gather.VoucherGatherService;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class FIVoucherDataGatherUI {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    private VData mResult = new VData();
	/** 往后面传输数据的容器 */
	private VData mInputData= new VData();
	/**  传输数据的容器*/
	private TransferData mTransferData;
    /** 业务数据 */
    GlobalInput mGlobalInput = new GlobalInput();

    private String mBatchno = "";
    
    /** 数据操作字符串 */
    private String mOperate;
    
    private final String enter = "\r\n"; // 换行符    
    
    
    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	// 将操作数据拷贝到本类中
    	this.mOperate = cOperate;
    	this.mInputData = cInputData;
    	
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData())
		{
			return false;
		}
		
		if (!checkData())
		{
			return false;
		}
		if(!dealData())
		{
			return false;
		}
		return true;
    }
    
	/**
	 * 获取前台数据
	 * @return
	 */
	private boolean getInputData()
	{
    	
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
        if(mTransferData==null){
        	buildError("FIVoucherDataGatherUI", "getInputData", "mTransferData获取参数出错");
        	return false;
        }
        mBatchno = (String) mTransferData.getValueByName("BatchNo");
		
		return true;
	}
	
	/**
	 * 数据校检
	 * @return
	 */
	private boolean checkData()
	{
		if(mGlobalInput==null)
		{
    		buildError("FIVoucherDataGatherUI","checkData","全局对象获取失败！");
    		return false;
		}
		if(mBatchno==null||"".equals(mBatchno))
		{
    		buildError("FIVoucherDataGatherUI","checkData","处理批次获取失败！");
    		return false;
		}
		return true;
	}
	
	/**
	 * 业务处理
	 * @return
	 */
    private boolean dealData()
    {
		try 
		{			
			FIOperationLog tLogDeal = new FIOperationLog(mGlobalInput.Operator,"40");
			VoucherGatherService tVoucherGatherService = new VoucherGatherService(tLogDeal);
			
			tLogDeal.WriteLogTxt("=========开始凭证汇总=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
			tLogDeal.WriteLogTxt("当前凭证汇总批次为："+ mBatchno +enter);
			
			tVoucherGatherService.dealService(mInputData);
			
			tLogDeal.WriteLogTxt("=========凭证汇总结束=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
    }
    
    public VData getResult()
    {
        return this.mResult;
	}
    
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FIVoucherDataGatherUI t = new FIVoucherDataGatherUI();
		
		GlobalInput tG = new GlobalInput();
		tG.Operator="001";
		VData tVData  = new VData();  
		tVData.add(tG);
		tVData.add("1");
		t.submitData(tVData, "");
	}

}
