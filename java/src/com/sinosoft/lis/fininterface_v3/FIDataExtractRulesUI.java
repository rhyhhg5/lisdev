package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class FIDataExtractRulesUI 
{

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	/** 业务处理相关变量 */
	private FIDataExtractRulesSchema mFIDataExtractRulesSchema = new FIDataExtractRulesSchema();

	// private FIAssociatedDirectItemDefSet mFIAssociatedDirectItemDefSet = new
	// FIAssociatedDirectItemDefSet();

	private FIDataExtractRulesBL mFIDataExtractRulesBL = new FIDataExtractRulesBL();

	private String mVersionNo = "";

	private String mColumnID = "";

	public FIDataExtractRulesUI()
	{}

	/**
	 * 传输数据的公共方法
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate)
	{
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		System.out.println("getInputData");
		if (!getInputData(cInputData))
		{
			return false;
		}

		System.out.println("dealData");
		if (!dealData())
		{
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData())
		{
			return false;
		}
		System.out.println("Start FIDataExtractRulesUI Submit...");

		mFIDataExtractRulesBL.submitData(mInputData, mOperate);

		System.out.println("End FIDataExtractRulesUI Submit...");
		// 如果有需要处理的错误，则返回
		if (mFIDataExtractRulesBL.mErrors.needDealError())
		{
			// @@错误处理
			this.mErrors.copyAllErrors(mFIDataExtractRulesBL.mErrors);
			CError tError = new CError();
			tError.moduleName = "FIMetadataDefUI";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		if (mOperate.equals("QUERY||MAIN"))
		{
			this.mResult.clear();
			this.mResult = mFIDataExtractRulesBL.getResult();
		}
		mColumnID = mFIDataExtractRulesBL.getColumnID();
		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData)
	{
		// 全局变量
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0)); // (GlobalInput)强制类型转换
		this.mFIDataExtractRulesSchema.setSchema((FIDataExtractRulesSchema) cInputData.getObjectByObjectName("FIDataExtractRulesSchema", 0));
		if (mGlobalInput == null)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIDataExtractRulesUI";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的信息！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 根据前面的输入数据，进行UI逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 * @return
	 */
	private boolean dealData()
	{
		boolean tReturn = true;
		
		return tReturn;
	}

	/**
	 * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
	 * @return
	 */
	private boolean prepareOutputData()
	{
		try
		{
			mInputData.clear();
			mInputData.add(this.mGlobalInput);
			mInputData.add(this.mFIDataExtractRulesSchema);
		}
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIDataExtractRulesUI";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public VData getResult()
	{
		return this.mResult;

	}

	public static void main(String[] args)
	{}


}
