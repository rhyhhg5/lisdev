package com.sinosoft.lis.fininterface_v3;

/**
 * <p>Title: 数据归档服务类</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
import com.sinosoft.lis.db.FIDataBaseLinkDB;
import com.sinosoft.lis.db.FIDataExtractDefDB;
import com.sinosoft.lis.db.FIDataExtractRulesDB;
import com.sinosoft.lis.fininterface_v3.core.archive.DataTransRuleEngineLR;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.fininterface_v3.tools.dblink.PubSubmitForInterface;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class FIDataTransServiceLR {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    public GlobalInput mGlobalInput = new GlobalInput();
    private String StartDate = "";
    private String EndDate = "";
    private String EventNo = "";
    private DataTransRuleEngineLR mDataTransRuleEnginelr;
    private final String enter = "\r\n"; // 换行符
    private FIOperationLog tLogDeal = null;
    
    public FIDataTransServiceLR(FIOperationLog logDeal)
    {
        tLogDeal = logDeal;
        EventNo = tLogDeal.getEventNo();
        mDataTransRuleEnginelr = new DataTransRuleEngineLR(tLogDeal);
    }

    /**
     * 归档所有业务数据
     * @param param
     * @return
     */
	public boolean submitData(VData param)
	{
		System.out.println("准备归档数据=========================");
		//准备归档数据
		if(!prepareAttData())
		{
			return false;
		}
		System.out.println("基本信息归档=========================");
		//基本信息归档
		if(!baseTypearchive(param))
		{
			return false;
		}
		System.out.println("归档业务交易及费用============ =============");
		//归档业务交易及费用
		mDataTransRuleEnginelr.dealFilingBusiness();
			
		return true;
	}
	
	/**
	 * 基本信息归档
	 * @return
	 */
	private boolean baseTypearchive(VData param)
	{
		FIDataExtractDefSet extFIDataExtractDefSet = (FIDataExtractDefSet)param.getObjectByObjectName("FIDataExtractDefSet",0);
		TransferData mTransferData = (TransferData)param.getObjectByObjectName("TransferData",0);
		String mFIExtType = (String) mTransferData.getValueByName("FIExtType");
		
		if(extFIDataExtractDefSet==null || "01".equals(mFIExtType))
		{
			//单笔数据抽取,执行所有归档规则
			//查询归档规则
			String tSQL = "select * from FIDataExtractDef where RuleState = '1' and RuleType = '01' and indexcode<>'00' and ruledefid in ('0000000111','0000000110')  order by RuleDefID desc,DealOrder with ur";
			
			FIDataExtractDefSet tFIDataExtractDefSet = new FIDataExtractDefDB().executeQuery(tSQL);

			String tRuleDefID = "";
			
			if(tFIDataExtractDefSet!=null &&tFIDataExtractDefSet.size()>0)
			{
				for (int i=1 ;i<=tFIDataExtractDefSet.size();i++)
				{					
					tRuleDefID = tFIDataExtractDefSet.get(i).getRuleDefID();
					//数据归档
					dealTrans( tRuleDefID,param);	
				}
			}
		}
		else
		{
			for(int j=1;j<=extFIDataExtractDefSet.size();j++)
			{
				//查询归档规则
				String tSQL = "select * from FIDataExtractDef where RuleState = '1' and RuleType = '01' and indexcode='"
					+extFIDataExtractDefSet.get(j).getIndexCode()+"' and ruledefid in ('0000000111','0000000110') order by RuleDefID desc,DealOrder with ur ";
				
				FIDataExtractDefSet tFIDataExtractDefSet = new FIDataExtractDefDB().executeQuery(tSQL);

				String tRuleDefID = "";
				
				if(tFIDataExtractDefSet!=null &&tFIDataExtractDefSet.size()>0)
				{
					for (int i=1 ;i<=tFIDataExtractDefSet.size();i++)
					{					
						tRuleDefID = tFIDataExtractDefSet.get(i).getRuleDefID();
						//数据归档
						dealTrans( tRuleDefID,param);	
					}
				}
			}
		}
		//执行公共归档规则
		String tSQL = "select * from FIDataExtractDef where RuleState = '1' and RuleType = '01' and indexcode='00' order by RuleDefID desc,DealOrder with ur ";
		
		FIDataExtractDefSet tFIDataExtractDefSet = new FIDataExtractDefDB().executeQuery(tSQL);

		String tRuleDefID = "";
		
		if(tFIDataExtractDefSet!=null &&tFIDataExtractDefSet.size()>0)
		{
			for (int i=1 ;i<=tFIDataExtractDefSet.size();i++)
			{					
				tRuleDefID = tFIDataExtractDefSet.get(i).getRuleDefID();
				//数据归档
				dealTrans( tRuleDefID,param);	
			}
		}
		//更新归档状态
		updateState();
		
		return true;
	}
	/**
	 * 主数据归档
	 * @param tRuleDefID
	 * @param param
	 * @return
	 */
	private boolean dealTrans(String tRuleDefID,VData param)
	{
		
		FIDataExtractRulesSet tFIDataExtractRulesSet = null;
		
		if(tRuleDefID==null)
			tFIDataExtractRulesSet = getDataAddRules();
		else
			tFIDataExtractRulesSet = getDataAddRules(tRuleDefID);
		
		if(tFIDataExtractRulesSet==null||tFIDataExtractRulesSet.size()<1)
		{
			return false;
		}
		mDataTransRuleEnginelr.ruleService(tFIDataExtractRulesSet, param);
		
		return true;
	}
	
	/**
	 * 归档规则
	 * @return
	 */
	private FIDataExtractRulesSet getDataAddRules()
	{
		String tSQL = "select * from FIDataExtractRules where state = '1' and ArithmeticType = '01' and ArithmeticID in ('G-LRT-000001','G-LRT-000002','G-LRF-000001','G-LRF-000002') order by RuleDefID,DealOrder with ur ";
		
		FIDataExtractRulesDB tFIDataExtractRulesDB = new FIDataExtractRulesDB();
		FIDataExtractRulesSet tFIDataExtractRulesSet  = tFIDataExtractRulesDB.executeQuery(tSQL);
		
		return tFIDataExtractRulesSet;
	}

	/**
	 * 归档规则
	 * @return
	 */
	private FIDataExtractRulesSet getDataAddRules(String RuleDefID)
	{
		String tSQL = "select * from FIDataExtractRules where state = '1' and ArithmeticType = '01' and RuleDefID ='"+RuleDefID+"' and state = '1' order by DealOrder with ur ";
		
		FIDataExtractRulesDB tFIDataExtractRulesDB = new FIDataExtractRulesDB();
		FIDataExtractRulesSet tFIDataExtractRulesSet  = tFIDataExtractRulesDB.executeQuery(tSQL);
		
		return tFIDataExtractRulesSet;
	}
	/**
	 * 归档前属性数据准备
	 * @return
	 */
	private boolean prepareAttData()
	{
		 
		//(1)主表属性数据准备	
		RSWrapper rsWrapper1 = new RSWrapper();
		FIAboriginalMainSet tFIAboriginalMainSet = new FIAboriginalMainSet();
		String tSqlMain = "select * from FIAboriginalMain a where not exists(select 1 from FIAboriginalMainAtt b where a.MSerialNo = b.MSerialNo " +
						  " and b.PhysicalTable in ('LRAccount_M_BF','LRAccount_M_PK','LRAccount_M_SX') and b.indexcode in ('11','10') and b.busdetail = 'LR' and b.feetype in ('BF','PK','SX'))" +
						  " and a.indexcode in ('11','10') and a.othernotype in ('BF','PK','SX') and a.PhysicalTable in ('LRAccount_M_BF','LRAccount_M_PK','LRAccount_M_SX')" +
						  " and a.State = '02' and a.checkflag = '00' with ur" ;
		
        if (!rsWrapper1.prepareData(tFIAboriginalMainSet, tSqlMain))
        {
            mErrors.copyAllErrors(rsWrapper1.mErrors);
            System.out.println(rsWrapper1.mErrors.getFirstError());
            buildError("FIDataTransService","prepareAttData", "批量准备主归档数据时出错,提示信息为：" +rsWrapper1.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("批量准备主归档数据时出错,提示信息为：" +rsWrapper1.mErrors.getFirstError() + enter);
            tLogDeal.buildErrorLog("批量准备主归档数据时出错，执行出现错误,需要 重新归档数据！");
            return false;
        }

        do
        {
        	rsWrapper1.getData();
            if (tFIAboriginalMainSet != null && tFIAboriginalMainSet.size() > 0)
            { 	
            	dealAttData(tFIAboriginalMainSet);
            }                 
        }while(tFIAboriginalMainSet != null && tFIAboriginalMainSet.size() > 0);
        
        tFIAboriginalMainSet=null;
        //rsWrapper1.close();
        
		//(2)明细属性数据准备	
		String tSQL = "select * from FIAboriginalGenDetail a where not exists(select 1 from FIAboriginalGenAtt b where a.ASerialNo = b.ASerialNo" +
				 " and b.SubPhysicalTable in ('LRAccount_BF','LRAccount_PK','LRAccount_SX') and b.indexcode in ('11','10') and b.feetype in ('BF','PK','SX') and b.busdetail = 'LR') " +
		         " and a.SubPhysicalTable in ('LRAccount_BF','LRAccount_PK','LRAccount_SX') and a.indexcode in ('11','10') and a.feetype in ('BF','PK','SX') "+
				 " and a.state = '00' and a.checkflag='00' with ur";
		
		RSWrapper rsWrapper2 = new RSWrapper();
		FIAboriginalGenDetailSet tFIAboriginalGenDetailSet  = new FIAboriginalGenDetailSet();
        if (!rsWrapper2.prepareData(tFIAboriginalGenDetailSet, tSQL))
        {
            mErrors.copyAllErrors(rsWrapper2.mErrors);
            System.out.println(rsWrapper2.mErrors.getFirstError());
            buildError("FIDataTransService","prepareAttData", "批量准备明细归档数据时出错,提示信息为：" +rsWrapper2.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("批量准备明细归档数据时出错,提示信息为：" +rsWrapper2.mErrors.getFirstError() + enter);
            tLogDeal.buildErrorLog("批量准备明细归档数据时出错，执行出现错误,需要 重新归档数据！");
            return false;
        }
		
        do
        {
        	rsWrapper2.getData();
            if (tFIAboriginalGenDetailSet != null && tFIAboriginalGenDetailSet.size() > 0)
            { 	
            	dealAttData(tFIAboriginalGenDetailSet);
            }                 
        }while(tFIAboriginalGenDetailSet != null && tFIAboriginalGenDetailSet.size() > 0);
        
        tFIAboriginalGenDetailSet=null;
        
        rsWrapper1.close();
        rsWrapper2.close();	
        
		return true;
	}
	/**
	 * 准备主属性数据
	 * @param tFIAboriginalMainSet
	 * @return
	 */
	private boolean dealAttData(FIAboriginalMainSet tFIAboriginalMainSet)
	{

		if(tFIAboriginalMainSet!=null && tFIAboriginalMainSet.size()>0)
		{
			MMap tMap= new MMap();
			VData tInputData = new VData();
			
			FIAboriginalMainAttSet tFIAboriginalMainAttSet = new FIAboriginalMainAttSet();
			FIAboriginalMainSchema tFIAboriginalMainSchema ;
			for(int i=1;i<=tFIAboriginalMainSet.size();i++)
			{
				tFIAboriginalMainSchema = tFIAboriginalMainSet.get(i);
				
				FIAboriginalMainAttSchema tFIAboriginalMainAttSchema = new FIAboriginalMainAttSchema();
				tFIAboriginalMainAttSchema.setMSerialNo(tFIAboriginalMainSchema.getMSerialNo());
				tFIAboriginalMainAttSchema.setPhysicalTable(tFIAboriginalMainSchema.getPhysicalTable());
				tFIAboriginalMainAttSchema.setTrandFlag("1"); //转换
				tFIAboriginalMainAttSchema.setIndexCode(tFIAboriginalMainSchema.getIndexCode());
				tFIAboriginalMainAttSchema.setIndexNo(tFIAboriginalMainSchema.getIndexNo());
				tFIAboriginalMainAttSchema.setListFlag(tFIAboriginalMainSchema.getListFlag());
				tFIAboriginalMainAttSchema.setGDCardFlag("0");//非广东卡单标记
				tFIAboriginalMainAttSchema.setYCFlag("0");//非
				tFIAboriginalMainAttSchema.setZengSongFlag("0");//非
				tFIAboriginalMainAttSchema.setGBType("00");//共保类型
				tFIAboriginalMainAttSchema.setDifComFlag("0");
				tFIAboriginalMainAttSchema.setRiskType("0");//默认为传统
				tFIAboriginalMainAttSchema.setBusDetail("ZC");
				tFIAboriginalMainAttSchema.setFeeType("ZC");
				tFIAboriginalMainAttSchema.setFeeDetail("ZC");
				tFIAboriginalMainAttSchema.setMarketType(tFIAboriginalMainSchema.getMarketType());
				tFIAboriginalMainAttSchema.setExecuteCom(tFIAboriginalMainSchema.getManageCom());				
				tFIAboriginalMainAttSchema.setYears(tFIAboriginalMainSchema.getYears());
				tFIAboriginalMainAttSchema.setPayMode(tFIAboriginalMainSchema.getPayMode());
				tFIAboriginalMainAttSchema.setBClient(tFIAboriginalMainSchema.getCustomerNo());//大客户
				if("1".equals(tFIAboriginalMainAttSchema.getListFlag()))
					tFIAboriginalMainAttSchema.setContNo(tFIAboriginalMainSchema.getContNo());
				else
					tFIAboriginalMainAttSchema.setContNo(tFIAboriginalMainSchema.getGrpContNo());
				
				tFIAboriginalMainAttSchema.setFIDate(tFIAboriginalMainSchema.getAccountDate());
				tFIAboriginalMainAttSchema.setCurrency(tFIAboriginalMainSchema.getCurrency());
				tFIAboriginalMainAttSchema.setSumActuMoney(tFIAboriginalMainSchema.getSumActuMoney());
				
				tFIAboriginalMainAttSet.add(tFIAboriginalMainAttSchema);
			}
			tMap.put(tFIAboriginalMainAttSet, "INSERT");
			
			tInputData.add(tMap);
		    PubSubmit tPubSubmit = new PubSubmit();
	        if (!tPubSubmit.submitData(tInputData, ""))
	        {
	            tMap = null;
	            tInputData = null;
	        	this.mErrors.copyAllErrors(tPubSubmit.mErrors);
	            buildError("FIDataTransService","dealAttData", "准备主归档数据时出错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
	            tLogDeal.WriteLogTxt("准备主归档数据时出错，提示信息为：" +tPubSubmit.mErrors.getFirstError() + enter);
	            return false;
	        }
	        tMap = null;
	        tInputData = null;
	        tFIAboriginalMainAttSet=null;
		}
		
		return true;
	}
	/**
	 * 准备明细属性数据
	 * @param tFIAboriginalGenDetailSet
	 * @return
	 */
	private boolean dealAttData(FIAboriginalGenDetailSet tFIAboriginalGenDetailSet)
	{
		if(tFIAboriginalGenDetailSet!=null && tFIAboriginalGenDetailSet.size()>0)
		{
			MMap tMap= new MMap();
			VData tInputData = new VData();
			
			FIAboriginalGenAttSet tFIAboriginalGenAttSet = new FIAboriginalGenAttSet();
			FIAboriginalGenDetailSchema tFIAboriginalGenDetailSchema ;
			for(int i=1;i<=tFIAboriginalGenDetailSet.size();i++)
			{
				tFIAboriginalGenDetailSchema = tFIAboriginalGenDetailSet.get(i);
				
				FIAboriginalGenAttSchema tFIAboriginalGenAttSchema = new FIAboriginalGenAttSchema();
				tFIAboriginalGenAttSchema.setASerialNo(tFIAboriginalGenDetailSchema.getASerialNo());
				tFIAboriginalGenAttSchema.setSubPhysicalTable(tFIAboriginalGenDetailSchema.getSubPhysicalTable());
				tFIAboriginalGenAttSchema.setTrandFlag("1"); //转换
				tFIAboriginalGenAttSchema.setIndexCode(tFIAboriginalGenDetailSchema.getIndexCode());
				tFIAboriginalGenAttSchema.setIndexNo(tFIAboriginalGenDetailSchema.getIndexNo());
				tFIAboriginalGenAttSchema.setListFlag(tFIAboriginalGenDetailSchema.getPolType());
				
				tFIAboriginalGenAttSchema.setGDCardFlag("0");//非卡单
				tFIAboriginalGenAttSchema.setYCFlag("0");//非邮储
				tFIAboriginalGenAttSchema.setTempFeeFlag("0");//正常
				tFIAboriginalGenAttSchema.setZengSongFlag("0");//非赠送保费
				tFIAboriginalGenAttSchema.setGBType("0");//共保类型
				tFIAboriginalGenAttSchema.setDifComFlag("0");
				tFIAboriginalGenAttSchema.setBusDetail("ZC");
				tFIAboriginalGenAttSchema.setRiskType(tFIAboriginalGenDetailSchema.getRiskType());
				tFIAboriginalGenAttSchema.setRiskPeriod(tFIAboriginalGenDetailSchema.getRiskPeriod());
				tFIAboriginalGenAttSchema.setExecuteCom(tFIAboriginalGenDetailSchema.getManageCom());
				tFIAboriginalGenAttSchema.setPayMode(tFIAboriginalGenDetailSchema.getPayMode());
				tFIAboriginalGenAttSchema.setMarketType(tFIAboriginalGenDetailSchema.getMarketType());
				tFIAboriginalGenAttSchema.setPolYear(tFIAboriginalGenDetailSchema.getPolYear());				
				tFIAboriginalGenAttSchema.setFirstYear(String.valueOf(tFIAboriginalGenDetailSchema.getAccYears()));
				tFIAboriginalGenAttSchema.setCustomerID(tFIAboriginalGenDetailSchema.getCustomerNo());
				tFIAboriginalGenAttSchema.setYears(tFIAboriginalGenDetailSchema.getYears());
				tFIAboriginalGenAttSchema.setFeeType(tFIAboriginalGenDetailSchema.getFeeType());
				tFIAboriginalGenAttSchema.setFeeDetail(tFIAboriginalGenDetailSchema.getFeeFinaType());
				
				if("1".equals(tFIAboriginalGenAttSchema.getListFlag()))
					tFIAboriginalGenAttSchema.setContNo(tFIAboriginalGenDetailSchema.getContNo());
				else
					tFIAboriginalGenAttSchema.setContNo(tFIAboriginalGenDetailSchema.getGrpContNo());
				
				tFIAboriginalGenAttSchema.setFIDate(tFIAboriginalGenDetailSchema.getAccountDate());
				tFIAboriginalGenAttSchema.setCurrency(tFIAboriginalGenDetailSchema.getCurrency());
				tFIAboriginalGenAttSchema.setSumActuMoney(tFIAboriginalGenDetailSchema.getSumActuMoney());
				
				tFIAboriginalGenAttSet.add(tFIAboriginalGenAttSchema);
			}
			tMap.put(tFIAboriginalGenAttSet, "INSERT");
			
			tInputData.add(tMap);
			PubSubmit tPubSubmit = new PubSubmit();
	        if (!tPubSubmit.submitData(tInputData, ""))
	        {
	            tMap = null;
	            tInputData = null;
	        	this.mErrors.copyAllErrors(tPubSubmit.mErrors);
	            buildError("FIDataTransService","dealAttData", "准备明细归档数据时出错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
	            tLogDeal.WriteLogTxt("准备明细归档数据时出错，提示信息为：" +tPubSubmit.mErrors.getFirstError() + enter);
	            return false;
	        }
	        tMap = null;
	        tInputData = null;
	        tFIAboriginalGenAttSet=null;
		}
		
		return true;
	}
	
	/**
	 * 将原始数据表中数据状态置为已归档  state = '10'
	 * @return
	 */
	private boolean updateState()
	{

		FIDataBaseLinkSchema tFIDataBaseLinkSchema = getDBSource();
		if(tFIDataBaseLinkSchema!=null)
		{
			VData interfInputData = new VData();	
			MMap interfMap= new MMap();

			interfMap.put("update FIAboriginalGenDetail a  set a.state = '10' where  a.state = '00' and a.SubPhysicalTable in ('LRAccount_BF','LRAccount_PK','LRAccount_SX') and a.indexcode in ('11','10') and a.feetype in ('BF','PK','SX')", "UPDATE");
			interfMap.put("update FIAboriginalMain a set a.state = '10' where  a.state = '02' and a.indexcode in ('11','10') and a.othernotype in ('BF','PK','SX') and a.PhysicalTable in ('LRAccount_M_BF','LRAccount_M_PK','LRAccount_M_SX') ", "UPDATE");

			interfInputData.add(interfMap);
			
			PubSubmitForInterface tPubSubmitForInterface = new PubSubmitForInterface();
	        if (!tPubSubmitForInterface.submitData(interfInputData, tFIDataBaseLinkSchema))
	        {		    		
	            buildError("FIDataTransService","updateState", "数据提取后更新数据状态时出错2，提示信息为：" +tPubSubmitForInterface.mErrors.getFirstError());
	            tLogDeal.WriteLogTxt("数据提取后更新数据状态时出错2!提示信息为：" +tPubSubmitForInterface.mErrors.getFirstError() + enter);		        		
		        interfInputData=null;
		        interfMap = null;
	            return false ;
	        }
	        interfInputData=null;
	        interfMap = null;
		}
		
		
		VData tInputData = new VData();	
		MMap tMap= new MMap();
		PubSubmit tPubSubmit = new PubSubmit();
		
		tMap.put("update FIAboriginalGenDetail a set a.state = '10' where  a.state = '00' and exists(select 1 from FIAboriginalGenAtt b where a.ASerialNo = b.ASerialNo" +
				" and b.SubPhysicalTable in ('LRAccount_BF','LRAccount_PK','LRAccount_SX') and b.indexcode in ('11','10') and b.feetype in ('BF','PK','SX') and b.busdetail = 'LR'"+
				") and a.SubPhysicalTable in ('LRAccount_BF','LRAccount_PK','LRAccount_SX') and a.indexcode in ('11','10') and a.feetype in ('BF','PK','SX')", "UPDATE");
		
		tMap.put("update FIAboriginalMain a set a.state = '10' where  a.state = '02' and exists(select 1 from FIAboriginalMainAtt b where a.MSerialNo = b.MSerialNo" +
				" and b.PhysicalTable in ('LRAccount_M_BF','LRAccount_M_PK','LRAccount_M_SX') and b.indexcode in ('11','10') and b.busdetail = 'LR' and b.feetype in ('BF','PK','SX')"+
				") and a.indexcode in ('11','10') and a.othernotype in ('BF','PK','SX') and a.PhysicalTable in ('LRAccount_M_BF','LRAccount_M_PK','LRAccount_M_SX')", "UPDATE");

		tInputData.add(tMap);
		
        if (!tPubSubmit.submitData(tInputData, ""))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buildError("FIDataTransService","updateState", "更新归档数据状态时出错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("更新归档数据状态时出错，提示信息为：，提示信息为：" +tPubSubmit.mErrors.getFirstError() + enter);
            return false;
        }
		
		return true;
	}

	/**
	 * 获取规则执行数据源
	 * @return
	 */
	private FIDataBaseLinkSchema getDBSource()
	{
		String sql = "select * from FIDataExtractRules where ArithmeticType='00' and subtype='00' and state='1' and DataSource is not null and ArithmeticID in ('E-LR-000001','E-LR-000002','E-LR-000003') with ur ";
		FIDataExtractRulesSet tFIDataExtractRulesSet= new FIDataExtractRulesDB().executeQuery(sql);
		
		if(tFIDataExtractRulesSet==null||tFIDataExtractRulesSet.size()<1)
		{
			return null;
		}
		FIDataExtractRulesSchema tFIDataExtractRulesSchema = tFIDataExtractRulesSet.get(1);
		
		FIDataBaseLinkDB tFIDataBaseLinkDB = new FIDataBaseLinkDB();
        String strKeyDefSQL = "select * from FIDataBaseLink where InterfaceCode ='"+tFIDataExtractRulesSchema.getDataSource()+"' with ur ";
        FIDataBaseLinkSet tFIDataBaseLinkSet = tFIDataBaseLinkDB.executeQuery(strKeyDefSQL);
        if(tFIDataBaseLinkSet == null || tFIDataBaseLinkSet.size()==0)
        {
            return null;
        }		
		return tFIDataBaseLinkSet.get(1);
	}
	
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
