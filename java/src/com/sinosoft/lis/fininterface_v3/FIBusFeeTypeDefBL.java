/**
 * <p>ClassName: FIBusTypeDefBL.java </p>
 * <p>Description: 业务交易定义 </p>
 * <p>Copyright: Copyright (c) 2009 </p>
 * <p>Company: </p>
 * @author  董健
 * @version 1.0
 * @CreateDate：2011/8/25
 */
package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class FIBusFeeTypeDefBL 
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors=new CErrors();
	private VData mResult = new VData();
	/** 往后面传输数据的容器 */
	private VData mInputData= new VData();
	/** 全局数据 */
	private GlobalInput mGlobalInput =new GlobalInput() ;
	/** 数据操作字符串 */
	private String mOperate;
	private String tMaintNo;
 
	/** 业务处理相关变量 */
	private FIBnFeeTypeDefSchema mFIBusFeeTypeDefSchema = new FIBnFeeTypeDefSchema();
	private MMap map=new MMap();
	
	 /**
	  * 传输数据的公共方法
	  * @param: cInputData 输入的数据
	  *         cOperate 数据操作
	  * @return:
	  */
		public boolean submitData(VData cInputData,String cOperate)
		{
			this.mOperate =cOperate;
	  
			//得到外部传入的数据,将数据备份到本类中
			if (!getInputData(cInputData))
			{
				return false;
			}

			if (!checkData())
			{
				return false;
			}

			//进行业务处理
			if (!dealData())
			{
				return false;
			}

			if (!prepareOutputData())
			{
				return false;
			}

			if(!pubSubmit())
			{
				return false;
			}

			System.out.println("End FIBusFeeTypeDefBL Submit...");
			mInputData=null;
			return true;
		}


	 /**
	  * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 *
	 */
		private boolean getInputData( VData cInputData)
		{
			mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0);
			mFIBusFeeTypeDefSchema = (FIBnFeeTypeDefSchema)cInputData.getObjectByObjectName("FIBnFeeTypeDefSchema",1);
		 
			return true;
		}

		private boolean checkData()
		{
		 
			return true;
		}

	 /**
	  * 根据前面的输入数据，进行BL逻辑处理
	  * 如果在处理过程中出错，则返回false,否则返回true
	  */
		private boolean dealData()
		{
			try
			{
				FMBnFeeTypeDefSchema tFMBusFeeTypeDefSchema = new FMBnFeeTypeDefSchema();
				FIBnFeeTypeDefSchema oFIBusFeeTypeDefSchema = new FIBnFeeTypeDefSchema();
				FIBnFeeTypeDefDB mFIBusFeeTypeDefDB = new FIBnFeeTypeDefDB();
				if("insert".equals(mOperate))
				{
					System.out.println("FIBusFeeTypeDefBL----新增业务类型!");
					map.put(mFIBusFeeTypeDefSchema, "INSERT");
				}
				else if("update".equals(mOperate)||"delete".equals(mOperate))
				{
					oFIBusFeeTypeDefSchema = mFIBusFeeTypeDefDB.executeQuery("select * from FIBnFeeTypeDef "
							+" where CostID='"+mFIBusFeeTypeDefSchema.getCostID()+"' and BusinessID = '"+mFIBusFeeTypeDefSchema.getBusinessID()+"'").get(1);
					tMaintNo = PubFun1.CreateMaxNo("MaintNo_FeeType",20);
				  	System.out.println("流水号作业：修改记录编号" + tMaintNo);
				  	tFMBusFeeTypeDefSchema.setMaintNo(tMaintNo);
				  	tFMBusFeeTypeDefSchema.setVersionNo(oFIBusFeeTypeDefSchema.getVersionNo());
				  	tFMBusFeeTypeDefSchema.setBusinessID(oFIBusFeeTypeDefSchema.getBusinessID());
				  	tFMBusFeeTypeDefSchema.setCostID(oFIBusFeeTypeDefSchema.getCostID());
				  	tFMBusFeeTypeDefSchema.setCostName(oFIBusFeeTypeDefSchema.getCostName());
				  	tFMBusFeeTypeDefSchema.setCostType(oFIBusFeeTypeDefSchema.getCostType());
				  	tFMBusFeeTypeDefSchema.setCostDetail(oFIBusFeeTypeDefSchema.getCostDetail());
					tFMBusFeeTypeDefSchema.setReport(PubFun.getCurrentDate()+" "+ PubFun.getCurrentTime());
					tFMBusFeeTypeDefSchema.setRemark(oFIBusFeeTypeDefSchema.getRemark());

					map.put(tFMBusFeeTypeDefSchema, "INSERT");
					
					if("update".equals(mOperate))
					{
						System.out.println("FIBusFeeTypeDefBL----修改业务类型!");
						map.put(mFIBusFeeTypeDefSchema, "UPDATE");
					}
					else if("delete".equals(mOperate))
					{
						System.out.println("FIBusFeeTypeDefBL----删除业务类型!");
						map.put(mFIBusFeeTypeDefSchema, "DELETE");
					}
				}
				else if("GDUpdate".equals(mOperate))
				{
					//归档类型定义
					String update = "update FIBnFeeTypeDef set CostType = '"+mFIBusFeeTypeDefSchema.getCostType()
						+"' where BusinessID='"+mFIBusFeeTypeDefSchema.getBusinessID()+"' and CostID = '"+mFIBusFeeTypeDefSchema.getCostID()+"' " ;
					map.put(update, "UPDATE");					
				}
				
			}
			catch(Exception ex)
			{
				System.out.println("FIBusFeeTypeDefBL--->dealData处理出错。");
				return false;
			}
			return true;
		}

		private boolean prepareOutputData()
		{
			try
			{
				mInputData.clear();
				mInputData.add(map);
			}
			catch(Exception ex)
			{
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "FIBusFeeTypeDefBL";
				tError.functionName = "prepareData";
				tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
				this.mErrors.addOneError(tError);
				return false;
			}
			return true;
		}
	 
		/**
		 * 提交数据
		 * @return
		 */
		private boolean pubSubmit()
		{
			// 进行数据提交
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(mInputData, ""))
			{
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "FIBusFeeTypeDefBL";
				tError.functionName = "PubSubmit.submitData";
				tError.errorMessage = "数据提交失败FIBusFeeTypeDefBL-->pubSubmit!";
				this.mErrors.addOneError(tError);
				return false;
			}
			return true;

		}
	 
	 public VData getResult()
	 {
		 return null;
	 }

	 public static void main(String[] args) {
	 }



}
