package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.fininterface_v3.core.check.DataCheckService;
import com.sinosoft.lis.fininterface_v3.core.trans.VoucherDataTransService;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class FIDistillCertificateUI {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    private VData mResult = new VData();
	/** 往后面传输数据的容器 */
	private VData mInputData= new VData();
    /** 业务数据 */
    GlobalInput mGlobalInput = new GlobalInput();

    private String DataType = "";
    
    /** 数据操作字符串 */
    private String mOperate;
    
    //批次号
    private String mBatchno ;
    
    private final String enter = "\r\n"; // 换行符    
    
    
    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	// 将操作数据拷贝到本类中
    	this.mOperate = cOperate;
    	this.mInputData = cInputData;
    	
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData())
		{
			return false;
		}
		
		if (!checkData())
		{
			return false;
		}
		if(!dealData())
		{
			return false;
		}
		return true;
    }
    
	/**
	 * 获取前台数据
	 * @return
	 */
	private boolean getInputData()
	{
    	
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        DataType = (String) mInputData.get(1);
		
		return true;
	}
	
	/**
	 * 数据校检
	 * @return
	 */
	private boolean checkData()
	{
		if(mGlobalInput==null)
		{
    		buildError("FIDistillCertificateUI","checkData","全局对象获取失败！");
    		return false;
		}
		if(DataType==null||"".equals(DataType))
		{
    		buildError("FIDistillCertificateUI","checkData","数据类型获取失败！");
    		return false;
		}
		return true;
	}
	
	/**
	 * 业务处理
	 * @return
	 */
    private boolean dealData()
    {
		try 
		{			
			FIOperationLog tLogDeal = new FIOperationLog(mGlobalInput.Operator,"30");
			VoucherDataTransService tVoucherDataTransService = new VoucherDataTransService(tLogDeal);
			
			tLogDeal.WriteLogTxt("=========开始凭证转换=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
			
			tVoucherDataTransService.dealService(mInputData);
			
			mBatchno = tVoucherDataTransService.getBatchno();
//			mResult.add(mBatchno);
			
			tLogDeal.WriteLogTxt("=========凭证转换结束=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
			
	      	tLogDeal.WriteLogTxt("=========执行数据校检=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
	      	
	      	DataCheckService mDataCheckService = new DataCheckService(tLogDeal);
			
	      	//对抽取后的数据进行校检
			mDataCheckService.qualityDataCheck("30",mInputData);
			
			tLogDeal.WriteLogTxt("=========数据校检结束=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
			tLogDeal.Complete(true);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
    }
    
    public VData getResult()
    {
        return this.mResult;
	}
    
    public String getBatchno()
    {
    	return this.mBatchno;
    }
    
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FIDistillCertificateUI t = new FIDistillCertificateUI();
		
		GlobalInput tG = new GlobalInput();
		tG.Operator="001";
		VData tVData  = new VData();  
		tVData.add(tG);
		tVData.add("1");
		t.submitData(tVData, "");
	}

}
