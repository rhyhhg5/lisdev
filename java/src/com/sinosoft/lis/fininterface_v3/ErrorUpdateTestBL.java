/**
 * <p>ClassName: FIBusTypeDefBL.java </p>
 * <p>Description: 业务交易定义 </p>
 * <p>Copyright: Copyright (c) 2009 </p>
 * <p>Company: </p>
 * @author  董健
 * @version 1.0
 * @CreateDate：2011/8/25
 */
package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.db.FIAbStandardDataDB;
import com.sinosoft.lis.db.FIBnFeeInfoDB;
import com.sinosoft.lis.db.FIRulesVersionTraceDB;
import com.sinosoft.lis.db.FIVoucherDataDetailDB;
import com.sinosoft.lis.db.FMBnFeeInfoDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.FIAbStandardDataDBSet;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.lis.vschema.FIVoucherDataDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ErrorUpdateTestBL 
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors=new CErrors();
	private VData mResult = new VData();
	/** 往后面传输数据的容器 */
	private VData mInputData= new VData();
	/** 全局数据 */
	private GlobalInput mGlobalInput =new GlobalInput() ;
	/** 数据操作字符串 */
	private String mOperate;
	private String tMaintNo;
	/** 业务处理相关变量 */
	private TransferData tTransferData;
	private FIVoucherDataDetailSchema mFIVoucherDataDetailSchema = new FIVoucherDataDetailSchema();
	private MMap map=new MMap();
	
	 /**
	  * 传输数据的公共方法
	  * @param: cInputData 输入的数据
	  *         cOperate 数据操作
	  * @return:
	  */
		public boolean submitData(VData cInputData,String cOperate)
		{
			//将操作数据拷贝到本类中
			this.mInputData = cInputData;
			this.mOperate =cOperate;
	  
			//得到外部传入的数据,将数据备份到本类中
			if (!getInputData())
			{
				return false;
			}

			if (!checkData())
			{
				return false;
			}

			//进行业务处理
			if (!dealData())
			{
				return false;
			}

			if (!prepareOutputData())
			{
				return false;
			}

			PubSubmit tPubSubmit = new PubSubmit();
	  
			if(!pubSubmit())
			{
				return false;
			}

			System.out.println("End FMBusInfoDefBL Submit...");
			mInputData=null;
			return true;
		}


	 /**
	  * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 *
	 */
		private boolean getInputData()
		{
			mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
			
			tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData",0);
//			tTransferData = (TransferData)mInputData.getObjectByObjectName("tTransferData",0);
//			String indexno = (String)tTransferData.getValueByName("IndexidNo");
//			System.out.println("---BL里的----索引值为：---"+indexno);
		 
			return true;
		}

		private boolean checkData()
		{
		 
			return true;
		}

	 /**
	  * 根据前面的输入数据，进行BL逻辑处理
	  * 如果在处理过程中出错，则返回false,否则返回true
	  */
		private boolean dealData()
		{
			try
			{
				
				FIVoucherDataDetailSchema fFIVoucherDataDetailSchema = new FIVoucherDataDetailSchema();
				FIAbStandardDataSchema fFIAbStandardDataSchema = new FIAbStandardDataSchema();
				FIVoucherDataDetailSet tFIVoucherDataDetailSet = new FIVoucherDataDetailSet();
				
				FIVoucherDataDetailDB fFIVoucherDataDetailDB = new FIVoucherDataDetailDB();
				FIVoucherDataDetailSet fFIVoucherDataDetailSet = new FIVoucherDataDetailSet();
				
				System.out.println("---------------------------------------");
				
				String indexidno = (String)tTransferData.getValueByName("IndexidNo");
				String checkflag = (String)tTransferData.getValueByName("IndexType");
				
				System.out.println("索引号----"+ indexidno);
				System.out.println("校检标记----"+ checkflag);
				
				fFIAbStandardDataSchema.setIndexNo(indexidno);
				
				String sql = "select serialno from FIAbStandardData where indexno = '" + tTransferData.getValueByName("IndexidNo").toString() + "'";

//				tFIAbStandardDataSet = fFIAbStandardDataDB.executeQuery(sql);
				ExeSQL execsql = new ExeSQL();
	            SSRS ssrs = execsql.execSQL(sql);
	            
	            System.out.println("查询个数-------"+ssrs.MaxRow);
	            for(int m = 1;m <= ssrs.MaxRow;m++){
	            	
	            	String serialNo = ssrs.GetText(m, 1);
	            	System.out.println("编号为----"+m +"--:" + serialNo);
	            	String sqlA = "select * from FIVoucherDataDetail where serialno = '" + serialNo + "'";
	            	fFIVoucherDataDetailSet = fFIVoucherDataDetailDB.executeQuery(sqlA);
	            	fFIVoucherDataDetailSchema = fFIVoucherDataDetailSet.get(1);
	            	fFIVoucherDataDetailSchema.setCheckFlag(checkflag);
	            	
	            	tFIVoucherDataDetailSet.add(fFIVoucherDataDetailSchema);
	            	
	            }
	            
//				for (int m = 0;m < tFIAbStandardDataSet.size();m++){
//					String serialno = tFIAbStandardDataSet.get(1).getSerialNo();
//					fFIVoucherDetailSchema.setSerialNo(serialno);
//					tFIVoucherDataDetailSet.add(fFIVoucherDetailSchema);
//				}
				
//				if("UPDATE".equals(mOperate))
//				{
				  	
					System.out.println("FIVoucherDataDetail----修改校检标记!");
					map.put(tFIVoucherDataDetailSet, "UPDATE");

//				}
				
				
			}
			catch(Exception ex)
			{
				System.out.println("FMBusTypeDefBL--->dealData处理出错。");
				return false;
			}
			return true;
		}

		private boolean prepareOutputData()
		{
			try
			{
				mInputData.clear();
				mInputData.add(map);
			}
			catch(Exception ex)
			{
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "FMBusInfoDefBL";
				tError.functionName = "prepareData";
				tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
				this.mErrors.addOneError(tError);
				return false;
			}
			return true;
		}
	 
		/**
		 * 提交数据
		 * @return
		 */
		private boolean pubSubmit()
		{
			// 进行数据提交
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(mInputData, ""))
			{
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "ErrorUpdateTestBL";
				tError.functionName = "PubSubmit.submitData";
				tError.errorMessage = "数据提交失败ErrorUpdateTestBL-->pubSubmit!";
				this.mErrors.addOneError(tError);
				return false;
			}
			return true;

		}
	 
	 public VData getResult()
	 {
		 return null;
	 }

	 public static void main(String[] args) {
	 }



}
