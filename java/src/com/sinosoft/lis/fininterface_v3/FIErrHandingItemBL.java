/**
 * <p>ClassName: FIErrHandingItemBL.java </p>
 * <p>Description: 错误处理 </p>
 * <p>Copyright: Copyright (c) 2009 </p>
 * <p>Company: </p>
 * @author 董健
 * @version 1.0
 * @CreateDate：2011/8/25
 */
package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.lis.vschema.FIRuleDealErrLogSet;
import com.sinosoft.lis.vschema.FIVoucherDataDetailSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


public class FIErrHandingItemBL  
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors=new CErrors();
	private VData mResult = new VData();
	/** 往后面传输数据的容器 */
	private VData mInputData= new VData();
	/** 全局数据 */
	private GlobalInput mGlobalInput =new GlobalInput() ;
    private TransferData mTransferData = new TransferData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 业务处理相关变量 */
	//错误流水号
	private String tErrSerialNo="";
	//处理类型
	private String tDealType="";
	//处理描述
	private String tDetailReMark="";
	//CQNo
	private String tCQNo="";
	private String tReasonType;
	
	private FIRuleDealErrLogSchema mFIRuleDealErrLogSchema ;
	private FIAbStandardDataSet mFIAbStandardDataSet ;
	
	private MMap map=new MMap();
 
	public FIErrHandingItemBL()
	{}

	/**
	* 传输数据的公共方法
	* @param: cInputData 输入的数据
	*         cOperate 数据操作
	* @return:
	*/
	public boolean submitData(VData cInputData,String cOperate)
	{
		//将操作数据拷贝到本类中
		this.mInputData = cInputData;
		this.mOperate =cOperate;
		
		System.out.println("---FIErrHandingItemBL getInputData---");
		if (!getInputData(cInputData, cOperate)) 
		{
			return false;
		}
		
		if(!checkData())
		{
			return false;
		}
		//进行业务处理
		if (!dealData())
		{
			return false;
		}
		if (!prepareOutputData())
		{
			return false;
		}
		
		if(!pubSubmit())
		{
			return false;
		}

		System.out.println("End FIErrHandingItemBL Submit...");
		mInputData=null;
		return true;
	}


	 /**
	  * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 *
	 */
	private boolean getInputData(VData cInputData, String cOperate)
	{
		mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
		mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		if (mGlobalInput == null) 
		{
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "FIErrHandingItemBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

		
		
		tErrSerialNo = (String) mTransferData.getValueByName("ErrSerialNo");
		tDealType = (String) mTransferData.getValueByName("DealType");
		tDetailReMark = (String) mTransferData.getValueByName("DetailReMark");
		tCQNo = (String) mTransferData.getValueByName("CQNo");
		tReasonType = (String) mTransferData.getValueByName("ReasonType");
		tReasonType = "请选择";
		if("".equals(tDealType)||tDealType==null)
		{
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "FIErrHandingItemBL";
            tError.functionName = "checkData";
            tError.errorMessage = "前台传输处理类型失败!";
            this.mErrors.addOneError(tError);
            return false;			
		}
		
		return true;
	}
    private boolean checkData()
    {
		FIRuleDealErrLogDB tFIRuleDealErrLogDB = new FIRuleDealErrLogDB();
		tFIRuleDealErrLogDB.setErrSerialNo(tErrSerialNo);
		
		FIRuleDealErrLogSet tFIRuleDealErrLogSet = new FIRuleDealErrLogSet();
		tFIRuleDealErrLogSet = tFIRuleDealErrLogDB.query();
		
		if(tFIRuleDealErrLogSet!=null&&tFIRuleDealErrLogSet.size()>0)
		{
			mFIRuleDealErrLogSchema = tFIRuleDealErrLogSet.get(1).getSchema();			
		}
		else
		{
			buildError("FIErrHandingItemBL", "checkData", "获取日志记录失败！");
			return false;
		}
    	
    	if("04".equals(tDealType))
		{
			//红冲处理
	        String tSql = "select * from FIAbStandardData a where IndexCode ='"+mFIRuleDealErrLogSchema.getIndexCode()+"' and IndexNo = '"+mFIRuleDealErrLogSchema.getBusinessNo()
	        	+"' and exists (select 1 from FIVoucherDataDetail f,FIVoucheManage e where f.SerialNo = a.SerialNo and f.batchno = e.batchno and  e.state = '50' and f.CheckFlag='00') ";
	        System.out.println(tSql);
	        
	        mFIAbStandardDataSet = new FIAbStandardDataDB().executeQuery(tSql);
	        
	        if(mFIAbStandardDataSet==null||mFIAbStandardDataSet.size()<1)
	        {
	            buildError("FIErrHandingItemBL", "checkData", "该红冲登记业务信息无对应已生成凭证数据");
	            return false;
	        }
		}
        return true;
    }
	
	/**
	  * 根据前面的输入数据，进行BL逻辑处理
	  * 如果在处理过程中出错，则返回false,否则返回true
	  */
	private boolean dealData()
	{
		//日志信息处理
		if(!prepareLog())
		{
			return false;
		}
		if("03".equals(tDealType))
		{
			//忽略错误
			map.put("update FIRuleDealErrLog set DealState = '3' where ErrSerialNo = '"+mFIRuleDealErrLogSchema.getErrSerialNo()+"'", "UPDATE");
			map.put("update FIAboriginalMain set CheckFlag = '00' where IndexCode = '"+mFIRuleDealErrLogSchema.getIndexCode()+"' and IndexNo = '"+mFIRuleDealErrLogSchema.getBusinessNo()+"'", "UPDATE");
			map.put("update FIAboriginalGenDetail set CheckFlag = '00' where IndexCode = '"+mFIRuleDealErrLogSchema.getIndexCode()+"' and IndexNo = '"+mFIRuleDealErrLogSchema.getBusinessNo()+"'", "UPDATE");
			map.put("update FIAbStandardData set CheckFlag = '00' where IndexCode = '"+mFIRuleDealErrLogSchema.getIndexCode()+"' and IndexNo = '"+mFIRuleDealErrLogSchema.getBusinessNo()+"'", "UPDATE");
			map.put("update FIVoucherDataDetail a set a.CheckFlag = '00' where exists(select 1 from FIAbStandardData b where a.SerialNo = b.SerialNo and IndexCode = '"+mFIRuleDealErrLogSchema.getIndexCode()+"' and IndexNo = '"+mFIRuleDealErrLogSchema.getBusinessNo()+"')", "UPDATE");
			
		}
		if("04".equals(tDealType))
		{
			//红冲处理
			 String tAskPrtNo = FinCreateSerialNo.getRBAppNo();
			 FIDataFeeBackAppSchema tFIDataFeeBackAppSchema = new FIDataFeeBackAppSchema();
			 tFIDataFeeBackAppSchema.setErrAppNo(tErrSerialNo);
			 tFIDataFeeBackAppSchema.setAppNo(tAskPrtNo);
			 tFIDataFeeBackAppSchema.setAppDate(PubFun.getCurrentDate());
			 tFIDataFeeBackAppSchema.setApplicant(mGlobalInput.Operator);
			 tFIDataFeeBackAppSchema.setAppState("01"); //红冲申请
			 tFIDataFeeBackAppSchema.setBusinessCode(mFIAbStandardDataSet.get(1).getBusinessType());
			 tFIDataFeeBackAppSchema.setFeeCode(mFIAbStandardDataSet.get(1).getFeeType());
			 
			 tFIDataFeeBackAppSchema.setDetailIndexID(mFIRuleDealErrLogSchema.getIndexCode());
			 tFIDataFeeBackAppSchema.setBusinessNo(mFIRuleDealErrLogSchema.getBusinessNo());

			 //tFIDataFeeBackAppSchema.setDetailIndexName(DetailIndexName);
			 tFIDataFeeBackAppSchema.setDetailReMark(tDetailReMark);
			 tFIDataFeeBackAppSchema.setReasonType(tReasonType);
			  
			 tFIDataFeeBackAppSchema.setMakeDate(PubFun.getCurrentDate());
			 tFIDataFeeBackAppSchema.setMakeTime(PubFun.getCurrentTime());
			 
			 map.put(tFIDataFeeBackAppSchema, "INSERT");
		}

		return true;
	}

	private boolean prepareLog()
	{
		mFIRuleDealErrLogSchema.setCQNo(tCQNo);
		mFIRuleDealErrLogSchema.setDealState("2"); //处理中
		map.put(mFIRuleDealErrLogSchema, "UPDATE");			
		
		FIErrHandingItemSchema tFIErrHandingItemSchema = new FIErrHandingItemSchema();
		
		tFIErrHandingItemSchema.setErrAppNo(tErrSerialNo);
		tFIErrHandingItemSchema.setBatchNo(mFIRuleDealErrLogSchema.getCheckBatchNo());
		tFIErrHandingItemSchema.setBusTypeID(mFIRuleDealErrLogSchema.getIndexCode());
		tFIErrHandingItemSchema.setCostID(mFIRuleDealErrLogSchema.getIndexCode());
		tFIErrHandingItemSchema.setIndexCode(mFIRuleDealErrLogSchema.getIndexCode());
		tFIErrHandingItemSchema.setIndexNo(mFIRuleDealErrLogSchema.getBusinessNo());
		tFIErrHandingItemSchema.setDataSource(mFIRuleDealErrLogSchema.getDealState());
		tFIErrHandingItemSchema.setDealType(tDealType);
		tFIErrHandingItemSchema.setCallPointID(mFIRuleDealErrLogSchema.getCallPointID());
		tFIErrHandingItemSchema.setErrSerialNo(tErrSerialNo);
		tFIErrHandingItemSchema.setErrType(tDealType);
		tFIErrHandingItemSchema.setSubErrType(tDealType);
		tFIErrHandingItemSchema.setErrReMark(tDetailReMark);
		tFIErrHandingItemSchema.setAppDate(PubFun.getCurrentDate());
		tFIErrHandingItemSchema.setApplicant(mGlobalInput.Operator);
		tFIErrHandingItemSchema.setAppState("01"); //01-申请完毕
		tFIErrHandingItemSchema.setMakeDate(PubFun.getCurrentDate());
		tFIErrHandingItemSchema.setMakeTime(PubFun.getCurrentTime());
		tFIErrHandingItemSchema.setCQNo(mFIRuleDealErrLogSchema.getCQNo());
		
		map.put(tFIErrHandingItemSchema, "INSERT");
		
		return true;
	}
	
	private boolean prepareOutputData()
	{
		try
		{
			mInputData.clear();
			mInputData.add(map);
		}
		catch(Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIErrHandingItemBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
 
	/**
	 * 提交数据
	 * @return
	 */
	private boolean pubSubmit()
	{
		// 进行数据提交
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, ""))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "FIErrHandingItemBL";
			tError.functionName = "PubSubmit.submitData";
			tError.errorMessage = "数据提交失败FIErrHandingItemBL-->pubSubmit!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
	public VData getResult()
	{
		return null;
	}

	public static void main(String[] args) 
	{
	}
}

