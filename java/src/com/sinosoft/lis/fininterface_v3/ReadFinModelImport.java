package com.sinosoft.lis.fininterface_v3;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


import jxl.DateCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import jxl.*;
import jxl.read.biff.*;

public class ReadFinModelImport
{
	public  CErrors mErrors=new CErrors();
	private VData mInputData= new VData();
	private TransferData tTransferData = new TransferData();
	/** 全局数据 */
	private GlobalInput mGlobalInput =new GlobalInput() ;
	/** 数据操作字符串 */

    private String mOperater;
    private String mManageCom;
    private String FileName;
    private String FItemplateType ;
    private String VersionNo ;
    private FIVoucherDefSet mFIVoucherDefSet = new FIVoucherDefSet();
    private FIVoucherBnSet mFIVoucherBnSet = new FIVoucherBnSet();
    private FIVoucherBnFeeSet mFIVoucherBnFeeSet = new FIVoucherBnFeeSet();
    private FIBnTypeDefSet mFIBnTypeDefSet = new FIBnTypeDefSet();
    private FIBnFeeTypeDefSet mFIBnFeeTypeDefSet = new FIBnFeeTypeDefSet();
    private FIRulesVersionSet mFIRulesVersionSet = new FIRulesVersionSet();
    private FIFinItemDefSet mFIFinItemDefSet = new FIFinItemDefSet();
    private  FIInfoFinItemAssociatedSet mFIInfoFinItemAssociatedSet = new FIInfoFinItemAssociatedSet();
    private MMap map=new MMap();
    
	public ReadFinModelImport()
	{

	}
    
    
    public boolean submitData(VData cInputData,String cOperate) throws RowsExceededException, WriteException
	{
		//将操作数据拷贝到本类中
		this.mInputData = cInputData;
		System.out.println("---ReadFinModelImport getInputData---");
		
		if (!getInputData(cInputData, cOperate)) {
	            return false;
	        }
	      

		if (!checkData())
		{
			return false;
		}

		//进行业务处理
		if (!readXls()) 
		{
			return false;
		}

		if (!prepareOutputData())
		{
			return false;
		}
		
		if(!pubSubmit())
		{
			return false;
		}

		System.out.println("End ReadFinModelImport Submit...");
		mInputData=null;
		return true;
	}
    
    
    /**
     * 创建Excel
     * @throws WriteException 
     * @throws RowsExceededException 
     */
    public boolean readXls() throws RowsExceededException, WriteException
    {
    	
        	Workbook books;
			try {
				//读取上传的Excel
				books = Workbook.getWorkbook
				(new File(FileName));
				//建新的Excel
				WritableWorkbook book= 
			        	Workbook.createWorkbook(new File(FileName.substring(0, FileName.lastIndexOf("/")+1)+"Excelnew.xls"));
				//得到上传的Excel中所有工作薄  
				WritableSheet sheet;
				Sheet[] sheets = books.getSheets();
				int rows =0;
				int Columns=0;
				 //设置数组与行数相同
		        String[] cee;
		        Cell[] cells;
				for(int n=1;n<sheets.length;n++)
				{
				//为新建Excel工作薄命名
				sheet=book.createSheet(sheets[n].getName(),n-1);
	        	System.out.println(sheets[n].getName());
	        	//得到行
	        	rows = sheets[n].getRows();
	           	cee=new String[rows+10];
		        //得到列
		         Columns = sheets[n].getColumns();
		        //遍历列
		        for(int a=0;a<Columns;a++)
		        {
			         	cells = sheets[n].getColumn(a);
			        //遍历列中所有单元格
				        for(int i=0;i<cells.length;i++)
				        {	
				        	cee[i] = cells[i].getContents();
				        	//第二个工作薄中对空的单元格设置为与上一个单元格一致（针对合并单元格）并且费用码不空的行
				        	if(n==1&&i!=0)
				        	{
				        		if(!"".equals(sheets[1].getCell(6,i).getContents())&&"".equals(cells[i].getContents()))
				        			cee[i]=cee[i-1];
				        		else if("".equals(sheets[1].getCell(6,i).getContents())&&"".equals(cells[i].getContents()))
				        			cee[i]=cells[i].getContents();
				        	}
				        	//第三个工作薄中对空的单元格设置处理 格式为 日期格式的
				        	if(n==2&&i!=0)
				        	{
				        		if(sheets[2].getCell(a,i).getType()==CellType.DATE&&!"".equals(sheets[2].getCell(a,i).getContents()))
				        		{
				        			 DateCell dc = (DateCell)sheets[2].getCell(a,i);
					        		 Date date = dc.getDate();
					        		 SimpleDateFormat ds = new SimpleDateFormat("yyyy-MM-dd");
					        		 cee[i] = ds.format(date);
					        		 System.out.println(cee[i]);
				        		} else 
				        			cee[i] = cee [i];
				        		 
				        	}
				        	//第五个工作薄中对空的单元格为与上一个单元格一致（针对合并单元格）并且业务交易码不空的行
				        	if(n==4&&i!=0)
				        	{
				        		if(!"".equals(sheets[4].getCell(1,i).getContents())&&"".equals(cells[i].getContents()))
				        			cee[i]=cee[i-1];
				        		else if("".equals(sheets[4].getCell(1,i).getContents())&&"".equals(cells[i].getContents()))
				        			cee[i]=cells[i].getContents();
				        	}
				        	//第六个工作薄中对空的单元格为与上一个单元格一致（针对合并单元格）并且费用码不空的行
				        	if(n==5&&i!=0)
				        	{
				        		if(!"".equals(sheets[5].getCell(4,i).getContents())&&"".equals(cells[i].getContents()))
				        		{
				        			if(!"".equals(sheets[5].getCell(6,i).getContents())||!"".equals(sheets[5].getCell(8,i).getContents()))
				        			   {
				        				if(a==6||a==7||a==8||a==9)
				        				   cee[i]=cells[i].getContents();
				        				else 
				        					cee[i]=cee[i-1];
				        			   }
				        			else
				        				cee[i]=cee[i-1];
				        		}
				        		else if("".equals(sheets[5].getCell(4,i).getContents())&&"".equals(cells[i].getContents()))
				        			cee[i]=cells[i].getContents();
				        	}
				        	//以及单元格内容为cee[i] 
						    Label label=new Label(a,i,cee[i]); 
						    //将定义好的单元格添加到工作表中 
						    sheet.addCell(label);
				        }     
		        	}
				}		       

		        book.write(); 
				books.close();
				book.close();
				
				
				

				Workbook	bookt = Workbook.getWorkbook
				(new File(FileName.substring(0, FileName.lastIndexOf("/")+1)+"Excelnew.xls"));
				
				//读取科目并写入Schema
				if(!getSheet6(bookt))
					return false;
				//读取科目核算项并写入Schema
				if(!getSheet7(bookt))
					return false;
				//先将科目及科目核算项写入 数据库，为方便导出凭证所流转科目核对使用
				if (!prepareOutputData())
				{
					return false;
				}
				
				if(!pubSubmit())
				{
					return false;
				}
				
//				if(!"0".equals(FileName))
//				{
//					return false;
//				}
				//重置map值
				map = new MMap();
				//读取业务类型及业务类型所包含费用信息并写入Schema
				if(!getSheet1(bookt))
					return false;
				//读取版本信息并写入Schema
				if(!getSheet2(bookt))
					return false;
				//读取凭证信息并写入Schema
				if(!getSheet3(bookt))
					return false;
				//读取凭证与业务对应关系并写入Schema
				if(!getSheet4(bookt))
					return false;
				//读取凭证、业务、费用信息及所流转科目并写入Schema
				if(!getSheet5(bookt))
					return false;

			} catch (BiffException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
			return true;
            
    }
    
  //读取业务类型及业务类型所包含费用信息并写入Schema
    private boolean getSheet1(Workbook bookt)
    {
    	Sheet sheett=bookt.getSheet(0);
    	//得到工作薄名称
		System.out.println(sheett.getName());
		//得到行数
		int trows = sheett.getRows();
		//得到列数
		int tColumns = sheett.getColumns();
		
		System.out.println(tColumns);
		String[] tFIBnTypeDefnum = new String[trows-1];
		String[] tFIBnFeeTypeDefnum = new String[trows-1];
		String bntype = "";
		String detailbntype = "";
		String objid = "";
		String index ="";
		ExeSQL es = new ExeSQL();
		for(int m=1;m<trows;m++)
		{
			if(!"".equals(sheett.getCell(6,m).getContents().trim()))
			{
				//校验业务大类是否为空
				if("".equals(sheett.getCell(0,m).getContents().trim()))
				{
					CError tError = new CError();
		            tError.moduleName = "ReadFinModelImport";
		            tError.functionName = "getInputData";
		            tError.errorMessage = sheett.getName()+" 工作薄中 "+sheett.getCell(2,m).getContents().trim()+"业务交易编码的业务大类信息为空，请确认该业务大类！";
		            this.mErrors.addOneError(tError);
		            return false;
					
				}
				//校验业务类别是否为空
				if("".equals(sheett.getCell(1,m).getContents().trim()))
				{
					CError tError = new CError();
		            tError.moduleName = "ReadFinModelImport";
		            tError.functionName = "getInputData";
		            tError.errorMessage = sheett.getName()+" 工作薄中 "+sheett.getCell(2,m).getContents().trim()+"业务交易编码的业务类别信息为空，请确认该业务类别！";
		            this.mErrors.addOneError(tError);
		            return false;
				}
				//校验业务对象是否为空
				if("".equals(sheett.getCell(4,m).getContents().trim()))
				{
					CError tError = new CError();
		            tError.moduleName = "ReadFinModelImport";
		            tError.functionName = "getInputData";
		            tError.errorMessage = sheett.getName()+" 工作薄中 "+sheett.getCell(2,m).getContents().trim()+"业务交易编码的业务对象信息为空，请确认该业务对象！";
		            this.mErrors.addOneError(tError);
		            return false;
				}
				//校验业务索引标识是否为空
				if("".equals(sheett.getCell(5,m).getContents().trim()))
				{
					CError tError = new CError();
		            tError.moduleName = "ReadFinModelImport";
		            tError.functionName = "getInputData";
		            tError.errorMessage = sheett.getName()+" 工作薄中 "+sheett.getCell(2,m).getContents().trim()+" 业务交易编码的业务索引标识信息为空，请确认该业务索引标识！";
		            this.mErrors.addOneError(tError);
		            return false;
				}
				//校验业务大类是否有效
				if(!"".equals(sheett.getCell(0,m).getContents().trim()))
				{
					bntype = es.getOneValue("select code from ldcode a where codetype = 'fibustype' and codename='"+sheett.getCell(0,m).getContents().trim()+"' fetch first 1 rows only");
					 System.out.println(sheett.getCell(0,m).getContents().trim());
					 if("".equals(bntype)||bntype==null)
					 {
						 	CError tError = new CError();
				            tError.moduleName = "ReadFinModelImport";
				            tError.functionName = "getInputData";
				            tError.errorMessage = sheett.getName()+" 工作薄中查询不到 "+sheett.getCell(0,m).getContents().trim()+" 业务大类信息，请确认该业务大类是否正确！";
				            this.mErrors.addOneError(tError);
				            return false;
					 }	 
				}
				//校验业务类别是否有效
				if(!"".equals(sheett.getCell(1,m).getContents().trim()))
				{
					detailbntype = es.getOneValue("select code from ldcode a where codetype = 'fidetailbustype' and codename='"+sheett.getCell(1,m).getContents().trim()+"' fetch first 1 rows only");
					 System.out.println(sheett.getCell(1,m).getContents().trim());
					 if("".equals(detailbntype)||detailbntype==null)
					 {
						
						 	CError tError = new CError();
				            tError.moduleName = "ReadFinModelImport";
				            tError.functionName = "getInputData";
				            tError.errorMessage = sheett.getName()+" 工作薄中查询不到 "+sheett.getCell(1,m).getContents().trim()+" 业务类别信息，请确认该业务类别是否正确！";
				            this.mErrors.addOneError(tError);
				            return false;
					 }	 
				}
				//校验业务对象是否有效
				if(!"".equals(sheett.getCell(4,m).getContents().trim()))
				{
					objid = es.getOneValue("select code from ldcode a where codetype = 'fibusobj' and codename='"+sheett.getCell(4,m).getContents().trim()+"' fetch first 1 rows only");
					 System.out.println(sheett.getCell(4,m).getContents().trim());
					 if("".equals(objid)||objid==null)
					 {
						  	CError tError = new CError();
				            tError.moduleName = "ReadFinModelImport";
				            tError.functionName = "getInputData";
				            tError.errorMessage = sheett.getName()+" 工作薄中查询不到 "+sheett.getCell(4,m).getContents().trim()+" 业务对象信息，请确认该业务对象是否正确！";
				            this.mErrors.addOneError(tError);
				            return false;
					 }	 
				}
				//校验业务索引标识是否有效
				if(!"".equals(sheett.getCell(5,m).getContents().trim()))
				{
					index = es.getOneValue("select code from ldcode a where codetype = 'fiindexid' and codename='"+sheett.getCell(5,m).getContents().trim()+"' fetch first 1 rows only");
					 System.out.println(sheett.getCell(5,m).getContents().trim());
					 if("".equals(index)||index==null)
					 {
						 	CError tError = new CError();
				            tError.moduleName = "ReadFinModelImport";
				            tError.functionName = "getInputData";
				            tError.errorMessage = sheett.getName()+" 工作薄中查询不到 "+sheett.getCell(5,m).getContents().trim()+" 业务索引标识信息，请确认该业务索引标识是否正确！";
				            this.mErrors.addOneError(tError);
				            return false;
					 }	 
				}
				//写入字符串数组   业务类别无版本，所以默认版本号写为  N 个 0
				tFIBnTypeDefnum[m-1] =  "00000000000000000000;"+
										sheett.getCell(2,m).getContents().trim()+";"+
										sheett.getCell(3,m).getContents().trim()+";"+
										bntype+";"+
										detailbntype+";"+
										sheett.getCell(4,m).getContents().trim()+";"+
										objid+";"+
										index+";"+
										sheett.getCell(5,m).getContents().trim();	
				//写入字符串数组  业务类别无版本，所以默认版本号写为  N 个 0
				tFIBnFeeTypeDefnum[m-1] = "00000000000000000000;"+
										sheett.getCell(2,m).getContents().trim()+";"+
										sheett.getCell(6,m).getContents().trim()+";"+
										sheett.getCell(7,m).getContents().trim();
			}
		}
		//去重复记录
		tFIBnTypeDefnum = array_unique(tFIBnTypeDefnum);
		tFIBnFeeTypeDefnum = array_unique(tFIBnFeeTypeDefnum);
		String bnstype = "";
		String ye = "";
		for(int i=0;i<tFIBnTypeDefnum.length;i++)
		{
			System.out.println(tFIBnFeeTypeDefnum[i]);
			if(tFIBnTypeDefnum[i]!=null)
			{
				String[] mFIBnTypeDefnum = tFIBnTypeDefnum[i].split(";");
				bnstype = mFIBnTypeDefnum[1].trim();
				ye = es.getOneValue("select count(1) from FIBnTypeDef where businessid='"+bnstype+"'");
				//校验业务类型是否已在数据库中存在。
				if(!"0".equals(ye))
				{
					    CError tError = new CError();
			            tError.moduleName = "ReadFinModelImport";
			            tError.functionName = "getInputData";
			            tError.errorMessage = sheett.getName()+" 工作薄中 "+bnstype+" 该业务类型已经存在，请重新输入业务类型编号!";
			            this.mErrors.addOneError(tError);
			            return false;
				}
				//写入Schema
				FIBnTypeDefSchema mFIBnTypeDefSchema = new FIBnTypeDefSchema(); 			   
				mFIBnTypeDefSchema.setVersionNo(mFIBnTypeDefnum[0].trim());
				mFIBnTypeDefSchema.setBusinessID(mFIBnTypeDefnum[1].trim());
				mFIBnTypeDefSchema.setBusinessName(mFIBnTypeDefnum[2].trim());
				mFIBnTypeDefSchema.setBusType(mFIBnTypeDefnum[3].trim());
				mFIBnTypeDefSchema.setDetailType(mFIBnTypeDefnum[4].trim());
				mFIBnTypeDefSchema.setObject(mFIBnTypeDefnum[5].trim());
				mFIBnTypeDefSchema.setObjectID(mFIBnTypeDefnum[6].trim());
				mFIBnTypeDefSchema.setIndexCode(mFIBnTypeDefnum[7].trim());
				mFIBnTypeDefSchema.setIndexName(mFIBnTypeDefnum[8].trim());
				mFIBnTypeDefSchema.setState("1");
				mFIBnTypeDefSet.add(mFIBnTypeDefSchema);
				
				String[] mFIBnFeeTypeDefnum = tFIBnFeeTypeDefnum[i].split(";");
				//写入Schema
				FIBnFeeTypeDefSchema mFIBnFeeTypeDefSchema = new FIBnFeeTypeDefSchema(); 			   
				mFIBnFeeTypeDefSchema.setVersionNo(mFIBnFeeTypeDefnum[0].trim());
				mFIBnFeeTypeDefSchema.setBusinessID(mFIBnFeeTypeDefnum[1].trim());
				mFIBnFeeTypeDefSchema.setCostID(mFIBnFeeTypeDefnum[2].trim());
				mFIBnFeeTypeDefSchema.setCostName(mFIBnFeeTypeDefnum[3].trim());
				mFIBnFeeTypeDefSchema.setCostType("D");
				mFIBnFeeTypeDefSet.add(mFIBnFeeTypeDefSchema);
			}
		}
		map.put(mFIBnTypeDefSet,"INSERT");
		map.put(mFIBnFeeTypeDefSet,"INSERT");
    	return true;
    }
  //读取版本信息并写入Schema 
    private boolean getSheet2(Workbook bookt)
    {
    	Sheet sheett=bookt.getSheet(1);
		System.out.println(sheett.getName());
		int trows = sheett.getRows();
		int tColumns = sheett.getColumns();
		System.out.println(tColumns);
		String[] tFIRulesVersionnum = new String[trows-1];
		ExeSQL es = new ExeSQL();
		String vstate="";
		DateCell cell;
		for(int m=1;m<trows;m++)
		{
			if(!"".equals(sheett.getCell(0,m).getContents().trim()))
			{
				if(!"".equals(sheett.getCell(2,m).getContents().trim()))
				{
					//校验版本状态是否有效。
					vstate = es.getOneValue("select code from ldcode a where codetype = 'versionstate' and codename='"+sheett.getCell(2,m).getContents().trim()+"'");
					 if("".equals(vstate)||vstate==null)
					 {
						 	CError tError = new CError();
				            tError.moduleName = "ReadFinModelImport";
				            tError.functionName = "getInputData";
				            tError.errorMessage = sheett.getName()+" 工作薄中 "+sheett.getCell(0,m).getContents().trim()+" 该版本状态为无效状态，请重新填写！";
				            this.mErrors.addOneError(tError);
				            return false;
					 }
				}
				//写入字符串数组
				if("".equals(sheett.getCell(4,m).getContents().trim()))
					tFIRulesVersionnum[m-1] =   sheett.getCell(0,m).getContents().trim()+";"+
												sheett.getCell(3,m).getContents().trim()+";"+
												sheett.getCell(1,m).getContents().trim()+";"+
												vstate+";"+mOperater+";a";
				else if(!"".equals(sheett.getCell(4,m).getContents().trim()))
					tFIRulesVersionnum[m-1] =   sheett.getCell(0,m).getContents().trim()+";"+
												sheett.getCell(3,m).getContents().trim()+";"+
												sheett.getCell(4,m).getContents().trim()+";"+
												sheett.getCell(1,m).getContents().trim()+";"+
												vstate+";"+mOperater+";b";
			}
		}
		//去重复记录
		tFIRulesVersionnum = array_unique(tFIRulesVersionnum);
		String versionno = "";
		String ye = "";
		for(int i=0;i<tFIRulesVersionnum.length;i++)
		{
			System.out.println(tFIRulesVersionnum[i]);
			if(tFIRulesVersionnum[i]!=null)
			{
				String[] mFIRulesVersionnum = tFIRulesVersionnum[i].split(";");
				versionno = mFIRulesVersionnum[0].trim();
				ye = es.getOneValue("select count(1) from FIRulesVersion where Versionno='"+versionno+"'");
				//校验版本是否已存在。
				if(!"0".equals(ye))
				{
					    CError tError = new CError();
			            tError.moduleName = "ReadFinModelImport";
			            tError.functionName = "getInputData";
			            tError.errorMessage = sheett.getName()+" 工作薄中 "+versionno+" 该版本编号已经存在，请重新输入新的版本编号!";
			            this.mErrors.addOneError(tError);
			            return false;
				}
			//	System.out.println(mFIRulesVersionnum[1].trim());
				//写入Schema
				FIRulesVersionSchema mFIRulesVersionSchema = new FIRulesVersionSchema(); 	
				if("a".equals(mFIRulesVersionnum[5].trim()))
				{
					mFIRulesVersionSchema.setVersionNo(mFIRulesVersionnum[0].trim());
					mFIRulesVersionSchema.setStartDate(mFIRulesVersionnum[1].trim());
					mFIRulesVersionSchema.setVersionReMark(mFIRulesVersionnum[2].trim());
					mFIRulesVersionSchema.setVersionState(mFIRulesVersionnum[3].trim());
					mFIRulesVersionSchema.setOperator(mFIRulesVersionnum[4].trim());
					mFIRulesVersionSchema.setMakeDate(PubFun.getCurrentDate());
					mFIRulesVersionSchema.setMakeTime(PubFun.getCurrentTime());
					mFIRulesVersionSchema.setAppDate(PubFun.getCurrentDate());
					System.out.println(mFIRulesVersionSchema.getStartDate());
				}
				else if("b".equals(mFIRulesVersionnum[6].trim()))
				{
					mFIRulesVersionSchema.setVersionNo(mFIRulesVersionnum[0].trim());
					mFIRulesVersionSchema.setStartDate(mFIRulesVersionnum[1].trim());
					mFIRulesVersionSchema.setEndDate(mFIRulesVersionnum[2].trim());
					mFIRulesVersionSchema.setVersionReMark(mFIRulesVersionnum[3].trim());
					mFIRulesVersionSchema.setVersionState(mFIRulesVersionnum[4].trim());
					mFIRulesVersionSchema.setOperator(mFIRulesVersionnum[5].trim());
					mFIRulesVersionSchema.setMakeDate(PubFun.getCurrentDate());
					mFIRulesVersionSchema.setMakeTime(PubFun.getCurrentTime());
					mFIRulesVersionSchema.setAppDate(PubFun.getCurrentDate());
					System.out.println(mFIRulesVersionSchema.getStartDate());
				}
				
				mFIRulesVersionSet.add(mFIRulesVersionSchema);
			}
		}
		map.put(mFIRulesVersionSet,"INSERT");
    	
    	return true;
    }
  //读取凭证信息并写入Schema
    private boolean getSheet3(Workbook bookt)
    {
    
    	Sheet sheett=bookt.getSheet(2);
		System.out.println(sheett.getName());
		int trows = sheett.getRows();
		int tColumns = sheett.getColumns();
		System.out.println(tColumns);
		String[] tFIVoucherDefnum = new String[trows-1];
		ExeSQL es = new ExeSQL();

		for(int m=1;m<trows;m++)
		{
			if(!"".equals(sheett.getCell(4,m).getContents().trim()))
			//写入字符串数组
				tFIVoucherDefnum[m-1] = sheett.getCell(2,m).getContents().trim()+";"+
										sheett.getCell(0,m).getContents().trim()+";"+
										sheett.getCell(1,m).getContents().trim()+";"+
										sheett.getCell(4,m).getContents().trim()+";"+
										sheett.getCell(5,m).getContents().trim();	
		}
		tFIVoucherDefnum = array_unique(tFIVoucherDefnum);
		String VouID = "";
		String ye = "";
		for(int i=0;i<tFIVoucherDefnum.length;i++)
		{
			System.out.println(tFIVoucherDefnum[i]);
			if(tFIVoucherDefnum[i]!=null)
			{
				String[] mFIVoucherTypeDefnum = tFIVoucherDefnum[i].split(";");
				VouID = mFIVoucherTypeDefnum[0].trim();
				ye = es.getOneValue("select count(1) from FIVoucherDef where VoucherID='"+VouID+"'");
				//校验凭证编号是否已存在。
				if(!"0".equals(ye))
				{
					    CError tError = new CError();
			            tError.moduleName = "ReadFinModelImport";
			            tError.functionName = "getInputData";
			            tError.errorMessage = sheett.getName()+" 工作薄中 "+VouID+" 该凭证编号已经存在，请重新输入新的凭证编号!";
			            this.mErrors.addOneError(tError);
			            return false;
				}
				
				FIVoucherDefSchema mFIVoucherDefSchema = new FIVoucherDefSchema(); 			   
				mFIVoucherDefSchema.setVersionNo(mFIVoucherTypeDefnum[0].trim());
				mFIVoucherDefSchema.setVoucherID(mFIVoucherTypeDefnum[1].trim());
				mFIVoucherDefSchema.setVoucherName(mFIVoucherTypeDefnum[2].trim());
				mFIVoucherDefSchema.setVoucherType(mFIVoucherTypeDefnum[3].trim());
				mFIVoucherDefSchema.setVoucherTypeName(mFIVoucherTypeDefnum[4].trim());
				mFIVoucherDefSchema.setState("01");
				mFIVoucherDefSet.add(mFIVoucherDefSchema);
			}
		}
		map.put(mFIVoucherDefSet,"INSERT");
    	
    	return true;
    }
  //读取凭证与业务对应关系并写入Schema
    private boolean getSheet4(Workbook bookt)
    {
    	
    	Sheet sheett=bookt.getSheet(3);
		System.out.println(sheett.getName());
		int trows = sheett.getRows();
		int tColumns = sheett.getColumns();
		System.out.println(tColumns);
		String[] tFIVoucherBnnum = new String[trows-1];
		
		for(int m=1;m<trows;m++)
		{
			if(!"".equals(sheett.getCell(2,m).getContents().trim()))
			tFIVoucherBnnum[m-1] = sheett.getCell(4,m).getContents().trim()+";"+
										sheett.getCell(2,m).getContents().trim()+";"+
										sheett.getCell(3,m).getContents().trim()+";"+
										sheett.getCell(0,m).getContents().trim()+";"+
										sheett.getCell(1,m).getContents().trim();
		}
		tFIVoucherBnnum = array_unique(tFIVoucherBnnum);
		
		for(int i=0;i<tFIVoucherBnnum.length;i++)
		{
			System.out.println(tFIVoucherBnnum[i]);
			if(tFIVoucherBnnum[i]!=null)
			{
				String[] mFIVoucherTypeBusnum = tFIVoucherBnnum[i].split(";");
				FIVoucherBnSchema tFIVoucherBnSchema = new FIVoucherBnSchema();
				tFIVoucherBnSchema.setVersionNo(mFIVoucherTypeBusnum[0].trim());
				tFIVoucherBnSchema.setVoucherID(mFIVoucherTypeBusnum[1].trim());
				tFIVoucherBnSchema.setVoucherName(mFIVoucherTypeBusnum[2].trim());
				tFIVoucherBnSchema.setBusinessID(mFIVoucherTypeBusnum[3].trim());
				tFIVoucherBnSchema.setBusinessName(mFIVoucherTypeBusnum[4].trim());
				
				mFIVoucherBnSet.add(tFIVoucherBnSchema);
			}
		}
		map.put(mFIVoucherBnSet,"INSERT");
		
		
    	return true;
    }
  //读取凭证、业务、费用信息及所流转科目并写入Schema
    private boolean getSheet5(Workbook bookt)
    {
    	Sheet sheett=bookt.getSheet(4);
		System.out.println(sheett.getName());
		int trows = sheett.getRows();
		int tColumns = sheett.getColumns();
		System.out.println(tColumns);
		String[] tFIVoucherBnFeenum = new String[trows-1];
		ExeSQL es = new ExeSQL();
		String finitemid1 = "";
		String finitemid2 = "";
		
		for(int m=1;m<trows;m++)
		{	
			if(!"".equals(sheett.getCell(4,m).getContents().trim()))
			{
				if(!"".equals(sheett.getCell(6,m).getContents().trim()))
				{
					 finitemid1 = es.getOneValue("select FinItemID from FIFinItemDef where ItemMainCode='"+sheett.getCell(6,m).getContents().trim()+"' fetch first 1 rows only");
					 System.out.println(sheett.getCell(6,m).getContents().trim());
					//校验借方科目是否为有效科目
					 if("".equals(finitemid1)||finitemid1==null)
					 {
						 	CError tError = new CError();
				            tError.moduleName = "ReadFinModelImport";
				            tError.functionName = "getInputData";
				            tError.errorMessage = sheett.getName()+" 工作薄查询不到 "+sheett.getCell(6,m).getContents().trim()+" 科目信息，请确认该科目是否正确！";
				            this.mErrors.addOneError(tError);
				            return false;
					 }
					 
					 
				}
				if(!"".equals(sheett.getCell(8,m).getContents().trim()))
				{
					 finitemid2 = es.getOneValue("select FinItemID from FIFinItemDef where ItemMainCode='"+sheett.getCell(8,m).getContents().trim()+"' fetch first 1 rows only");
					 System.out.println(sheett.getCell(8,m).getContents().trim());
					//校验贷方科目是否为有效科目
					 if("".equals(finitemid2)||finitemid2==null)
					 {
						  	CError tError = new CError();
				            tError.moduleName = "ReadFinModelImport";
				            tError.functionName = "getInputData";
				            tError.errorMessage = sheett.getName()+" 工作薄查询不到 "+sheett.getCell(8,m).getContents().trim()+" 科目信息，请确认该科目是否正确！";
				            this.mErrors.addOneError(tError);
				            return false;
					 }
					 
				}	
				//根据借贷是否为空进行区分，均不为空 n,借为空 a,贷为空 b
				if(!"".equals(sheett.getCell(6,m).getContents().trim())&&!"".equals(sheett.getCell(8,m).getContents().trim()))
					tFIVoucherBnFeenum[m-1] = "n;"+
											sheett.getCell(11,m).getContents().trim()+";"+
											sheett.getCell(14,m).getContents().trim()+";"+
											sheett.getCell(2,m).getContents().trim()+";"+
											sheett.getCell(5,m).getContents().trim()+";"+
											sheett.getCell(4,m).getContents().trim()+";"+
											finitemid1.trim()+";"+
//											sheett.getCell(6,m).getContents().trim()+";"+
											sheett.getCell(7,m).getContents().trim()+";"+
//											sheett.getCell(8,m).getContents().trim()+";"+
											finitemid2.trim()+";"+
											sheett.getCell(9,m).getContents().trim();
				else if("".equals(sheett.getCell(6,m).getContents().trim()))
					tFIVoucherBnFeenum[m-1] =   "a;"+
												sheett.getCell(11,m).getContents().trim()+";"+
												sheett.getCell(14,m).getContents().trim()+";"+
												sheett.getCell(2,m).getContents().trim()+";"+
												sheett.getCell(5,m).getContents().trim()+";"+
												sheett.getCell(4,m).getContents().trim()+";"+
//												sheett.getCell(8,m).getContents().trim()+";"+
												finitemid2.trim()+";"+
												sheett.getCell(9,m).getContents().trim();
				else if("".equals(sheett.getCell(8,m).getContents().trim()))
					tFIVoucherBnFeenum[m-1] = 	"b;"+
												sheett.getCell(11,m).getContents().trim()+";"+
												sheett.getCell(14,m).getContents().trim()+";"+
												sheett.getCell(2,m).getContents().trim()+";"+
												sheett.getCell(5,m).getContents().trim()+";"+
												sheett.getCell(4,m).getContents().trim()+";"+
												finitemid1.trim()+";"+
//												sheett.getCell(6,m).getContents().trim()+";"+
												sheett.getCell(7,m).getContents().trim();
			}
		}
		//去重复记录
		tFIVoucherBnFeenum = array_unique(tFIVoucherBnFeenum);
		
		for(int i=0;i<tFIVoucherBnFeenum.length;i++)					
		{
			System.out.println(tFIVoucherBnFeenum[i]);
			if(tFIVoucherBnFeenum[i]!=null)
			{
				String[] mFIFeeTypeDefnum = tFIVoucherBnFeenum[i].split(";");
	
				FIVoucherBnFeeSchema mFIVoucherBnFeeSchema = new FIVoucherBnFeeSchema();
				//System.out.println(mFIFeeTypeDefnum[0].trim()+"===============");
				//根据借贷是否为空进行区分，均不为空 n,借为空 a,贷为空 b
				if("n".equals(mFIFeeTypeDefnum[0].trim()))
				{	
					//System.out.println(mFIFeeTypeDefnum[0].trim()+"===============");
					mFIVoucherBnFeeSchema.setVersionNo(mFIFeeTypeDefnum[1].trim());
					mFIVoucherBnFeeSchema.setVoucherID(mFIFeeTypeDefnum[2].trim());
					mFIVoucherBnFeeSchema.setBusinessID(mFIFeeTypeDefnum[3].trim());
					mFIVoucherBnFeeSchema.setFeeName(mFIFeeTypeDefnum[4].trim());
					mFIVoucherBnFeeSchema.setFeeID(mFIFeeTypeDefnum[5].trim());
					mFIVoucherBnFeeSchema.setD_FinItemID(mFIFeeTypeDefnum[6].trim());
					mFIVoucherBnFeeSchema.setD_FinItemName(mFIFeeTypeDefnum[7].trim());
					mFIVoucherBnFeeSchema.setC_FinItemID(mFIFeeTypeDefnum[8].trim());
					mFIVoucherBnFeeSchema.setC_FinItemName(mFIFeeTypeDefnum[9].trim());
					mFIVoucherBnFeeSchema.setState("01");
					
				}
				if("a".equals(mFIFeeTypeDefnum[0].trim()))
				{
					mFIVoucherBnFeeSchema.setVersionNo(mFIFeeTypeDefnum[1].trim());
					mFIVoucherBnFeeSchema.setVoucherID(mFIFeeTypeDefnum[2].trim());
					mFIVoucherBnFeeSchema.setBusinessID(mFIFeeTypeDefnum[3].trim());
					mFIVoucherBnFeeSchema.setFeeName(mFIFeeTypeDefnum[4].trim());
					mFIVoucherBnFeeSchema.setFeeID(mFIFeeTypeDefnum[5].trim());
					mFIVoucherBnFeeSchema.setC_FinItemID(mFIFeeTypeDefnum[6].trim());
					mFIVoucherBnFeeSchema.setC_FinItemName(mFIFeeTypeDefnum[7].trim());
					mFIVoucherBnFeeSchema.setState("01");
				}
				if("b".equals(mFIFeeTypeDefnum[0].trim()))
				{
					mFIVoucherBnFeeSchema.setVersionNo(mFIFeeTypeDefnum[1].trim());
					mFIVoucherBnFeeSchema.setVoucherID(mFIFeeTypeDefnum[2].trim());
					mFIVoucherBnFeeSchema.setBusinessID(mFIFeeTypeDefnum[3].trim());
					mFIVoucherBnFeeSchema.setFeeName(mFIFeeTypeDefnum[4].trim());
					mFIVoucherBnFeeSchema.setFeeID(mFIFeeTypeDefnum[5].trim());
					mFIVoucherBnFeeSchema.setD_FinItemID(mFIFeeTypeDefnum[6].trim());
					mFIVoucherBnFeeSchema.setD_FinItemName(mFIFeeTypeDefnum[7].trim());
					mFIVoucherBnFeeSchema.setState("01");
				}
				mFIVoucherBnFeeSet.add(mFIVoucherBnFeeSchema);
			}
		}
		map.put(mFIVoucherBnFeeSet,"INSERT");
    	
    	return true;
    }
  //读取科目并写入Schema
    private boolean getSheet6(Workbook bookt)
    {
    	Sheet sheett=bookt.getSheet(5);
		System.out.println(sheett.getName());
		int trows = sheett.getRows();
		int tColumns = sheett.getColumns();
		System.out.println(tColumns);
		String[] tFIFinItemnum = new String[trows-1];
		String finitemtype = "";
		for(int m=1;m<trows;m++)
		{
			if(!"".equals(sheett.getCell(5,m).getContents().trim()))
			{
				if(!"".equals(sheett.getCell(1,m).getContents().trim()))
				{
					//判断科目类别
					if(("资产类科目").equals(sheett.getCell(1,m).getContents().trim()))
						finitemtype = "1";
					else if(("负债类科目").equals(sheett.getCell(1,m).getContents().trim()))
						finitemtype = "2";
					else if(("过渡科目").equals(sheett.getCell(1,m).getContents().trim()))
						finitemtype = "3";
					else if(("表外科目").equals(sheett.getCell(1,m).getContents().trim()))
						finitemtype = "4";
					else if(("损益类科目").equals(sheett.getCell(1,m).getContents().trim()))
						finitemtype = "6";
					else 
					{
						 	CError tError = new CError();
				            tError.moduleName = "ReadFinModelImport";
				            tError.functionName = "getInputData";
				            tError.errorMessage = sheett.getName()+"  工作薄中  "+sheett.getCell(5,m).getContents().trim()+"  科目  科目类别  填写错误，请重新填写!";
				            this.mErrors.addOneError(tError);

				            return false;
					}
				}
				tFIFinItemnum[m-1] = 	sheett.getCell(0,m).getContents().trim()+";"+
										sheett.getCell(2,m).getContents().trim()+";"+
										sheett.getCell(3,m).getContents().trim()+";"+
										sheett.getCell(5,m).getContents().trim()+";"+
										finitemtype+";"+
										sheett.getCell(4,m).getContents().trim();
			}
		}
		tFIFinItemnum = array_unique(tFIFinItemnum);
		
		for(int i=0;i<tFIFinItemnum.length;i++)
		{
			System.out.println(tFIFinItemnum[i]);
			if(tFIFinItemnum[i]!=null)
			{
				String[] mFIFinItemnum = tFIFinItemnum[i].split(";");
				FIFinItemDefSchema tFIFinItemDefSchema = new FIFinItemDefSchema();
				tFIFinItemDefSchema.setVersionNo(mFIFinItemnum[0].trim());
				tFIFinItemDefSchema.setFinItemID(mFIFinItemnum[1].trim());
				tFIFinItemDefSchema.setFinItemName(mFIFinItemnum[2].trim());
				tFIFinItemDefSchema.setItemMainCode(mFIFinItemnum[3].trim());
				tFIFinItemDefSchema.setFinItemType(mFIFinItemnum[4].trim());
				tFIFinItemDefSchema.setDealMode(mFIFinItemnum[5].trim());
				
				mFIFinItemDefSet.add(tFIFinItemDefSchema);
			}
		}
		map.put(mFIFinItemDefSet,"DELETE&INSERT");
		
    	
    	return true;
    }
    ////读取科目核算项并写入Schema
    private boolean getSheet7(Workbook bookt)
    {
    	Sheet sheett=bookt.getSheet(6);
		System.out.println(sheett.getName());
		int trows = sheett.getRows();
		int tColumns = sheett.getColumns();
		System.out.println(tColumns);
		String[] hsh = new String[tColumns-5];
		for(int i=5;i<tColumns;i++)
		{
			hsh[i-5]=sheett.getCell(i, 0).getContents().trim();
		}
//		System.out.println(hsh.length);
//		System.out.println("===================================");
		for(int i=0;i<hsh.length;i++)
		{
			
			System.out.println(hsh[i]);
		}
		//因需要将行变列导入数据库，取 行列 合并最大值为 数组长度
		int smn = Integer.parseInt(String.valueOf(trows)+String.valueOf(tColumns));
		System.out.println(smn);
		String[] tFIInfoFinItemAssociatednum = 	new String[smn];
		String mn="";
		String sn="";
		for(int m=1;m<trows;m++)
		{
			if(!"".equals(sheett.getCell(0,m).getContents().trim()))
				for(int n=0;n<hsh.length;n++)
				{
					if(!"".equals(sheett.getCell(n+5,m).getContents().trim()))
					{
						if(n<10)
							sn="0"+String.valueOf(n);
						else 
							sn = String.valueOf(n);
					mn = String.valueOf(m)+sn;
					////因需要将行变列导入数据库，取 行列 为 数组序号   防重复序号
					tFIInfoFinItemAssociatednum[Integer.parseInt(mn)] = 	sheett.getCell(0,m).getContents().trim()+";"+
																		sheett.getCell(1,m).getContents().trim()+";"+
																		mn+";"+
																		hsh[n];
					}
				}
		}
		tFIInfoFinItemAssociatednum = array_unique(tFIInfoFinItemAssociatednum);
		
		for(int i=0;i<tFIInfoFinItemAssociatednum.length;i++)
		{
			System.out.println(tFIInfoFinItemAssociatednum[i]);
			if(tFIInfoFinItemAssociatednum[i]!=null)
			{
				String[] mFIInfoFinItemAssociatednum = tFIInfoFinItemAssociatednum[i].split(";");
				FIInfoFinItemAssociatedSchema tFIInfoFinItemAssociatedSchema = new FIInfoFinItemAssociatedSchema();
				tFIInfoFinItemAssociatedSchema.setVersionNo(mFIInfoFinItemAssociatednum[0].trim());
				tFIInfoFinItemAssociatedSchema.setFinItemID(mFIInfoFinItemAssociatednum[1].trim());
				tFIInfoFinItemAssociatedSchema.setAssociatedID(mFIInfoFinItemAssociatednum[2].trim());
				tFIInfoFinItemAssociatedSchema.setAssociatedName(mFIInfoFinItemAssociatednum[3].trim());

				
				mFIInfoFinItemAssociatedSet.add(tFIInfoFinItemAssociatedSchema);
			}
		}
		map.put(mFIInfoFinItemAssociatedSet,"DELETE&INSERT");
    	return true;
    }
    
  //去重方法 
    public static String[] array_unique(String[] a) 
	{ // array_unique
		List list = new LinkedList(); 
		for(int i = 0; i < a.length; i++)  
			{ 
				if(!list.contains(a[i])) 
					{ 
						list.add(a[i]); 
					} 
			} 
		return (String[])list.toArray(new String[list.size()]); 
	 }
    
    private boolean getInputData(VData cInputData, String cOperate)
	{
		mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
		FileName = cOperate;
		tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData",0);
		if (mGlobalInput == null) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ReadFinModelImport";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得操作员编码
		mOperater = mGlobalInput.Operator;
        if ((mOperater == null) || mOperater.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ReadFinModelImport";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
    
        if ((mManageCom == null) || mManageCom.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ReadFinModelImport";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        System.out.println(FileName);
        if ((FileName == null) || FileName.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ReadFinModelImport";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据FileName失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
		return true;
	}

    
	private boolean checkData()
	{
		return true;
	}
	
	private boolean prepareOutputData()
	{
		try
		{
			mInputData.clear();
			mInputData.add(map);
		}
		catch(Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ReadFinModelImport";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
 
	/**
	 * 提交数据
	 * @return
	 */
	private boolean pubSubmit()
	{
		// 进行数据提交
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, ""))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ReadFinModelImport";
			tError.functionName = "PubSubmit.submitData";
			tError.errorMessage = "数据提交失败ReadFinModelImport-->pubSubmit!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
    /**
     * 测试
     * @param args
     * @throws WriteException 
     * @throws RowsExceededException 
     */
    public static void main(String[] args)
    {
    	
        ReadFinModelImport read = new ReadFinModelImport();
        try {
			read.readXls();
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
//			System.out.(e.printStackTrace());
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
}
