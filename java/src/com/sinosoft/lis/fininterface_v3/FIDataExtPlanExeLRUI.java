package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.fininterface_v3.core.check.DataCheckService;
import com.sinosoft.lis.fininterface_v3.core.ext.FIDataExtractService;
import com.sinosoft.lis.fininterface_v3.core.secondary.FISecondaryDealService;
import com.sinosoft.lis.fininterface_v3.core.secondary.FISecondaryDealServiceLR;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.FIDataExtractDefSchema;
import com.sinosoft.lis.vschema.FIDataExtractDefSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class FIDataExtPlanExeLRUI {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    public GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData ;
    
    private final String enter = "\r\n"; // 换行符    
    
    private String mStartDate;
    private String mEndDate;
    /** 数据操作字符串 */
    private String mOperate;
	private String tOperator;
    
    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	// 将操作数据拷贝到本类中
    	this.mOperate = cOperate;
       
		if(!getInputData(cInputData))
		{
			return false;
		}
		
		//数据抽取
		dataExt(cInputData);
		//数据归档
		dataTrans(cInputData);
        //二次处理
		secondaryDeal(cInputData);
		
       return true;
    }   

    /**
	  * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 *
	 */
	private boolean getInputData(VData cInputData)
	{
		mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0);
		//mFIDataExtractDefSet = (FIDataExtractDefSet)cInputData.getObjectByObjectName("FIDataExtractDefSet",0);
		mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData",0);

		if (mGlobalInput == null) 
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIDataExtPlanExeLRUI";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据失败!";
			this.mErrors.addOneError(tError);

			return false;
		}
		if (mTransferData == null) 
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIDataExtPlanExeLRUI";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输数据失败!";
			this.mErrors.addOneError(tError);

			return false;
		}
		mStartDate = (String)mTransferData.getValueByName("StartDate");
		mEndDate = (String)mTransferData.getValueByName("EndDate");
		tOperator = (String)mTransferData.getValueByName("Operator");
        System.out.println(tOperator);		
		return true;
	}
    /**
     * 数据抽取
     * @param cInputData
     * @return
     */
    private boolean dataExt(VData cInputData)
    {
    	try 
    	{
      	   FIOperationLog tLogDeal = new FIOperationLog(mGlobalInput.Operator,"00");
      	   
      	   FIDataExtractService tFIDataExtractService = new FIDataExtractService(tLogDeal);
      	   System.out.println("---FIDataExtractService BEGIN---");
      	   tLogDeal.WriteLogTxt("=========开始执行数据提取=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
      	   tLogDeal.WriteLogTxt("=========提数区间："+mStartDate+" 至 "+mEndDate + enter);
      	   //批量业务数据提取
      	   if("00".equals(mOperate))
      	   {
      		   if (!tFIDataExtractService.extractByRuleDef(cInputData)) 
      		   {
					// @@错误处理
					this.mErrors.copyAllErrors(tFIDataExtractService.mErrors);
					CError tError = new CError();
					tError.moduleName = "FIDataExtPlanExeUI";
					tError.functionName = "submitData";
					tError.errorMessage = "数据提取失败!";
					this.mErrors.addOneError(tError);
					tLogDeal.Complete(false);
					return false;
		      	}
	      	}
	      	//单笔业务数据提取
	      	if("01".equals(mOperate))
	      	{
		      	if (!tFIDataExtractService.extractByIndexNo(cInputData)) 
		      	{
			      	// @@错误处理
			      	this.mErrors.copyAllErrors(tFIDataExtractService.mErrors);
			      	CError tError = new CError();
			      	tError.moduleName = "FIDataExtPlanExeUI";
			      	tError.functionName = "submitData";
			      	tError.errorMessage = "数据提取失败!";
			      	this.mErrors.addOneError(tError);
			      	
			      	tLogDeal.Complete(false);
			      	
			      	return false;
		      	}
	      	}
	      	tLogDeal.WriteLogTxt("=========数据提取结束=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
			
	      	tLogDeal.WriteLogTxt("=========执行数据校检=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
	      	DataCheckService mDataCheckService = new DataCheckService(tLogDeal);
			//对抽取后的数据进行校检
			mDataCheckService.qualityDataCheck("00",cInputData);
			tLogDeal.WriteLogTxt("=========数据校检结束=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
			tLogDeal.Complete(true);
			
      	} catch (Exception e) {
	      	// TODO Auto-generated catch block
	      	e.printStackTrace();
      	}
    	
    	return true;
    } 

    /**
     * 数据归档
     * @param cInputData
     * @return
     */
    private boolean dataTrans(VData cInputData)
    {
    	try 
    	{
      	   FIOperationLog tLogDeal = new FIOperationLog(mGlobalInput.Operator,"10");
      	   
      	   FIDataTransServiceLR tFIDataTransServiceLR = new FIDataTransServiceLR(tLogDeal);
      	   System.out.println("---FIDataTransServiceLR BEGIN---");
      	   //批量业务数据归档
      	   tLogDeal.WriteLogTxt("=========开始执行数据归档=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
      	        	   
      	    if (!tFIDataTransServiceLR.submitData(cInputData)) 
  		   {
				// @@错误处理
				this.mErrors.copyAllErrors(tFIDataTransServiceLR.mErrors);
				CError tError = new CError();
				tError.moduleName = "FIDataExtPlanExeUI";
				tError.functionName = "dataTrans";
				tError.errorMessage = "数据归档失败!";
				this.mErrors.addOneError(tError);
				tLogDeal.Complete(false);
				return false;
	      	}
  		   
  		   	tLogDeal.WriteLogTxt("=========执行数据归档结束=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
  		 
  		   	tLogDeal.WriteLogTxt("=========执行数据校检=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
	      	DataCheckService mDataCheckService = new DataCheckService(tLogDeal);
			//对归档后的数据进行校检
			mDataCheckService.qualityDataCheck("10",cInputData);
			
			tLogDeal.WriteLogTxt("=========数据校检结束=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
			
			tLogDeal.Complete(true);
      	} 
    	catch (Exception e) 
    	{
	      	// TODO Auto-generated catch block
	      	e.printStackTrace();
      	}    	  	
    	return true;
    }   

    /**
     * 二次处理
     * @param cInputData
     * @return
     */
    private boolean secondaryDeal(VData cInputData)
    {
    	try 
    	{
	    	FIOperationLog tLogDeal = new FIOperationLog(mGlobalInput.Operator,"20");
	  	   
	    	FISecondaryDealServiceLR tFISecondaryDealServicelr = new FISecondaryDealServiceLR(tLogDeal);
	    	System.out.println("---FISecondaryDealServiceLR BEGIN---");
	    	
	      	//二次处理
	    	tLogDeal.WriteLogTxt("=========开始执行二次处理=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
	    	
	  		   if (!tFISecondaryDealServicelr.dealService(cInputData)) 
	  		   {
					// @@错误处理
					this.mErrors.copyAllErrors(tFISecondaryDealServicelr.mErrors);
					CError tError = new CError();
					tError.moduleName = "FIDataExtPlanExeUILR";
					tError.functionName = "secondaryDeal";
					tError.errorMessage = "二次处理失败!";
					this.mErrors.addOneError(tError);
					tLogDeal.Complete(false);
					return false;
		      	}
	    	
  		   	tLogDeal.WriteLogTxt("=========二次处理结束=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
  	  		 
  		   	tLogDeal.WriteLogTxt("=========执行数据校检=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
	      	DataCheckService mDataCheckService = new DataCheckService(tLogDeal);
			//对二次处理后的数据进行校检
			mDataCheckService.qualityDataCheck("20",cInputData);
			
			tLogDeal.WriteLogTxt("=========数据校检结束=========="+PubFun.getCurrentDate()+"/"+PubFun.getCurrentTime() + enter);
			
			tLogDeal.Complete(true);
	    	
    	}
	    catch (Exception e) 
    	{
	      	// TODO Auto-generated catch block
	      	e.printStackTrace();
      	} 
    	
    	return true;
    }
    
    
    
    
    
    public VData getResult()
    {
        return this.mResult;
    }
    
    public static void main(String[] args)
    {
    	FIDataExtPlanExeUI tFIDataExtPlanExeUI = new FIDataExtPlanExeUI();
    	
    	VData tVData = new VData();
    	GlobalInput tG = new GlobalInput();
    	tG.Operator= "001";
    	tG.ManageCom="86";
    	TransferData tTransferData = new TransferData();
    	
    	tTransferData.setNameAndValue("StartDate","2011-10-29");
    	tTransferData.setNameAndValue("EndDate","2011-10-29");
    	tTransferData.setNameAndValue("FIExtType","00");
    	
    	FIDataExtractDefSet tFIDataExtractDefSet = new FIDataExtractDefSet();
    	FIDataExtractDefSchema tFIDataExtractDefSchema = new FIDataExtractDefSchema();
    	tFIDataExtractDefSchema.setVersionNo("00000000000000000001");
    	tFIDataExtractDefSchema.setRuleDefID("0000000001");
    	tFIDataExtractDefSet.add(tFIDataExtractDefSchema);
    	
    	tVData.add(tG);
    	tVData.add(tTransferData);
    	tVData.add(tFIDataExtractDefSet);
    	
    	tFIDataExtPlanExeUI.submitData(tVData,"00");

    	
    }
}
