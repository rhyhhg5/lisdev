package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FICodeTransSchema;
import com.sinosoft.lis.schema.LICodeTransSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @date 2013-05-06 
 * @author 卢翰林
 *
 */

public class BFDepartmentCodeInputBL 
{
	/**错误处理类*/
	public CErrors mErrors = new CErrors();
	
	/**数据结果*/
	private VData mResult = new VData();
	
	/**全局数据*/
	private GlobalInput mGlobalInput = new GlobalInput();
	
	/**操作字符串*/
	private String mOperate;
	
	/**业务处理相关变量 */
	private LICodeTransSchema mLICodeTransSchema = new LICodeTransSchema();
	private FICodeTransSchema mFICodeTransSchema = new FICodeTransSchema();
	
	public BFDepartmentCodeInputBL(){}
	
	/**错误处理*/
	public void buildError(String func, String msg)
	{
		CError error = new CError();
		error.functionName = func;
		error.moduleName = "InputBL";
		error.errorMessage = msg;
		mErrors.addOneError(error);
	}
	
	/**
	 * 传输数据的公共方法
	 * @param: mInputData 输入的数据
	 *         mOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData mInputData, String mOperate)
	{
		//
		this.mOperate = mOperate;
		if(!mOperate.equals("INSERT"))
		{
			buildError("submitData", "不支持的操作字符串!");
			return false;
		}
		//获取数据
		if(!getInputData(mInputData))
		{
			buildError("getInputData", "业务处理错误!");
			return false;
		}
		//检查数据
		if(!checkData())
		{
			buildError("checkData", "业务处理错误!");
			return false;
		}
		//业务处理
		if(!dealData())
		{
			buildError("dealData", "业务处理错误!");
			return false;
		}
		//准备后台数据
		if(!perpareOutputData(mInputData))
		{
			buildError("prepareOutputData","业务处理错误！");
			return false;
		}
		//提交数据
		PubSubmit ps = new PubSubmit();
		System.out.println("SubmitData===============wait");
		if(!ps.submitData(mInputData, mOperate))
		{
			if (ps.mErrors.needDealError())
	        {
	            this.mErrors.copyAllErrors(ps.mErrors);
	            return false;
	        }
	        else
	        {
	            buildError("submitData","PubSubmit，但是没有提供详细的出错信息!");
	            return false;
	        }
		}
		System.out.println("SubmitData===============success");
		return true;
	}


	private boolean perpareOutputData(VData mInputData) 
	{
		try
	    {
	    	if(mOperate.equals("INSERT"))
	    	{
	    		MMap map=new MMap();
	    		String code = mLICodeTransSchema.getCode();
	    		String codeName = mLICodeTransSchema.getCodeName();
	    		String codeAlias = mLICodeTransSchema.getCodeAlias();
	    		
	    		String LISql1 = "INSERT INTO licodetrans(codeType, code, codeName, codeAlias) " +
	    				"values('BJHManageCom', '" + code + "', '" + 
	    				 codeName+ "', '" + codeAlias + "'" + ")  ";
	    		String LISql2 = "INSERT INTO licodetrans(codeType, code, codeName, codeAlias) " +
	    				"values('ManageCom', '" + code + "', '" + 
	    				 codeName+ "', '" + codeAlias + "'" + ")  ";
	    		
	    		String FISql = "INSERT INTO ficodetrans(codeType, code, codeName, codeAlias, otherSign) " +
	    				"values('ManageCom', '" + code + "', '" + 
	    				 codeName+ "', '" + codeAlias + "'" + ", '')  ";
	    		
	        	map.put(LISql1,"INSERT");
	        	map.put(LISql2, "INSERT");
	        	map.put(FISql, "INSERT");
	        	mInputData.add(map);
	        	System.out.println("Code==========" + code);
	        	System.out.println("CodeName==========" + codeName);
	        	System.out.println("CodeAlias==========" + codeAlias);
	    	}else 
	    	{
	    		buildError("prepareData","不存在的操作类型！");
			}
	    }
	    catch (Exception ex)
	    {
	      // 错误处理
	      ex.printStackTrace();
	      buildError("prepareData","报错原因："+ex.toString());
	      System.out.println("prepareOutputData==========false");
	      return false;
	    }
		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() 
	{
		return true;
	}


	private boolean checkData() 
	{
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * 如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData mInputData) 
	{
		try 
		{
			this.mLICodeTransSchema=(LICodeTransSchema) mInputData.getObjectByObjectName("LICodeTransSchema",0);
			this.mFICodeTransSchema=(FICodeTransSchema) mInputData.getObjectByObjectName("FICodeTransSchema",0);
			this.mGlobalInput=(GlobalInput) mInputData.getObjectByObjectName("GlobalInput",0);	
			System.out.println("mLICodeTransSchema============"); 
		} catch (Exception e) 
		{
		    e.printStackTrace();
		    buildError("prepareData","报错原因：获取业务数据失败！");
		    System.out.println("getInputData==========false");
		    return false;
		}
		return true;
	}
	
	public VData getResult()
	{
		return this.mResult;
	}
}
