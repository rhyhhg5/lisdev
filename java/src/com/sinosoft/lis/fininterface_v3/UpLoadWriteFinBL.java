package com.sinosoft.lis.fininterface_v3;

import java.io.File;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import jxl.format.UnderlineStyle;

public class UpLoadWriteFinBL
{
	public  CErrors mErrors=new CErrors();
	private VData mInputData= new VData();
	private TransferData tTransferData = new TransferData();
	/** 全局数据 */
	private GlobalInput mGlobalInput =new GlobalInput() ;
	/** 数据操作字符串 */

    private String mOperater;
    private String mManageCom;
    private String ImportPath;
    private String VersionNo ;
    private MMap map=new MMap();
    
	public UpLoadWriteFinBL()
	{

	}
    
    
    public boolean submitData(VData cInputData,String cOperate) throws RowsExceededException, WriteException
	{
		//将操作数据拷贝到本类中
		this.mInputData = cInputData;
		System.out.println("---UpLoadWriteFinBL getInputData---");
		
		if (!getInputData(cInputData, cOperate)) {
	            return false;
	        }
	      

		if (!checkData())
		{
			return false;
		}

		//进行业务处理
		if (!WriteXls()) 
		{
			return false;
		}

		if (!prepareOutputData())
		{
			return false;
		}
		
		if(!pubSubmit())
		{
			return false;
		}

		System.out.println("End UpLoadWriteFinBL Submit...");
		mInputData=null;
		return true;
	}
    
    
    /**
     * 创建Excel
     * @throws WriteException 
     * @throws RowsExceededException 
     */
    public boolean WriteXls() throws RowsExceededException, WriteException
    {
    	
        	
        	
			try {
				
				//建新的Excel
				WritableWorkbook book= 
			        	Workbook.createWorkbook(new File(ImportPath+"Finnew.xls"));
//					Workbook.createWorkbook(new File("d:/Finnew.xls"));
				//定义标题格式
					WritableFont wf = new WritableFont(WritableFont.ARIAL, 11,
					WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,
					jxl.format.Colour.BLACK); // 定义格式 字体 下划线 斜体 粗体 颜色
					WritableCellFormat wcf = new WritableCellFormat(wf); // 单元格定义
					wcf.setBackground(jxl.format.Colour.BLUE_GREY); // 设置单元格的背景颜色
					wcf.setAlignment(jxl.format.Alignment.CENTRE); // 设置对齐方式
					wcf.setBorder(jxl.format.Border.ALL,jxl.format.BorderLineStyle.THIN); 
				//定义内容格式	
							WritableFont wfc = new WritableFont(WritableFont.ARIAL, 10,
							WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
							jxl.format.Colour.BLACK); // 定义格式 字体 下划线 斜体 粗体 颜色
							WritableCellFormat wcft = new WritableCellFormat(wfc); // 单元格定义
							wcft.setBackground(jxl.format.Colour.WHITE); // 设置单元格的背景颜色
							wcft.setAlignment(jxl.format.Alignment.LEFT); // 设置对齐方式   
							wcft.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
							wcft.setBorder(jxl.format.Border.ALL,jxl.format.BorderLineStyle.THIN); 



				//为新建Excel工作薄命名
				String[] str = new String[] {"业务交易定义","版本管理","凭证模板定义","业务交易流转凭证","账务规则需求模板","科目定义模板","科目专项定义"};
				WritableSheet sheet0=book.createSheet(str[0],4);
				WritableSheet sheet1=book.createSheet(str[1],1);
				WritableSheet sheet2=book.createSheet(str[2],2);
				WritableSheet sheet3=book.createSheet(str[3],3);
				WritableSheet sheet4=book.createSheet(str[4],0);
				WritableSheet sheet5=book.createSheet(str[5],5);
				WritableSheet sheet6=book.createSheet(str[6],6);
//定义单元格标题				
//				业务大类	业务类别	业务交易编码	业务交易名称	业务对象	业务索引标识	费用编码	费用名称
				String[] str0 = new String[] {"业务大类","业务类别","业务交易编码","业务交易名称","业务对象","业务索引标识","费用编码","费用名称"};
				
				for(int i=0;i<str0.length;i++)
			    {	
					sheet0.setColumnView(i, 20); // 设置列的宽度
			    	sheet0.addCell(new Label(i,0,str0[i],wcf));
			    }
						       
//			    版本编码	版本名称	版本状态	生效日期	失效日期
			    String[] str1 = new String[] {"版本编码","版本名称","版本状态","生效日期","失效日期"};
			    
			    for(int i=0;i<str1.length;i++)
			    {
			    	sheet1.setColumnView(i, 20); 
			    	sheet1.addCell(new Label(i,0,str1[i],wcf));
			    }
			    
//			    凭证编码	凭证名称	版本编码	版本名称	凭证类型编码	凭证类型名称
			    String[] str2 = new String[] {"凭证编码","凭证名称","版本编码","版本名称","凭证类型编码","凭证类型名称"};
			    
			    for(int i=0;i<str2.length;i++)
			    {
			    	sheet2.setColumnView(i, 20); 
			    	sheet2.addCell(new Label(i,0,str2[i],wcf));
			    }
			    
//			    业务交易编码	业务交易名称	凭证编码	凭证名称	版本编码
			    String[] str3 = new String[] {"业务交易编码","业务交易名称","凭证编码","凭证名称","版本编码"};
			    
			    for(int i=0;i<str3.length;i++)
			    {
			    	sheet3.setColumnView(i, 20); 
			    	sheet3.addCell(new Label(i,0,str3[i],wcf));
			    }
				
//			    业务大类	业务类别	业务交易编码	业务交易名称	费用编码	费用名称	借方一级科目代码	借方科目名称	贷方一级科目代码	贷方科目名称	版本名称	版本编码	凭证类型编码	凭证类型名称	凭证编码	凭证名称
			    String[] str4 = new String[] {"业务大类","业务类别","业务交易编码","业务交易名称","费用编码","费用名称","借方一级科目代码","借方科目名称","贷方一级科目代码","贷方科目名称","版本名称","版本编码","凭证类型编码","凭证类型名称","凭证编码","凭证名称"};
			    
			    for(int i=0;i<str4.length;i++)
			    {
			    	sheet4.setColumnView(i, 15); 
			    	sheet4.addCell(new Label(i,0,str4[i],wcf));
			    }
			    
//			    版本编码	科目大类	系统科目编码	科目名称	科目层级	"一级科目代码"
			    String[] str5 = new String[] {"版本编码","科目大类","系统科目编码","科目名称","科目层级","一级科目代码"};
			    
			    for(int i=0;i<str5.length;i++)
			    {
			    	sheet5.setColumnView(i, 20); 
			    	sheet5.addCell(new Label(i,0,str5[i],wcf));
			    }
			    ExeSQL es = new ExeSQL();
		    	SSRS mSSRS = new SSRS();
		    	mSSRS = es.execSQL("select distinct associatedname from FIInfoFinItemAssociated b where exists (select 1 from FIRulesVersion c where c.versionno=b.versionno and c.versionstate='01') ");
		    	 
//			    版本编码	系统科目编码	科目大类	科目名称	一级科目编码	管理机构	对方机构	成本中心	交费年期	缴费方式	缴费首年	保险年期	保费收入类型	险种	渠道	大客户	市场类型	现金流量
			    String[] str6 = new String[] {"版本编码","系统科目编码","科目大类","科目名称","一级科目编码"};
			    for(int i=0;i<str6.length;i++)
			    {
			    	sheet6.setColumnView(i, 20); 
			    	sheet6.addCell(new Label(i,0,str6[i],wcf));
			    }
			    String sql = "";
			    if(mSSRS!=null&&mSSRS.getMaxRow()!=0)
			    {
					    for(int i=1;i<=mSSRS.getMaxRow();i++)
					    {
					    	sheet6.addCell(new Label(i+str6.length-1,0,mSSRS.GetText(i, 1),wcf));
					    	sql+=", max(case associatedname when '"+mSSRS.GetText(i, 1)+"' then 'Y' else '' end) "+mSSRS.GetText(i, 1);
					    }
		//			    sql = sql.substring(0,sql.length()-1);
					   
			    }
			    //设置科目核算项表头长度
			    String[] str7 = str6;
			    if(mSSRS!=null&&mSSRS.getMaxRow()!=0)
			    	str7 = new String[str6.length+mSSRS.getMaxRow()];
			    System.out.println(sql);
//定义工作薄取数SQL			    
			    String SQL0 = "select (select codename from ldcode where codetype='fibustype' and code=a.bustype fetch first 1 rows only),"
			    		 	+" (select codename from ldcode where codetype='fidetailbustype' and code=a.detailtype fetch first 1 rows only),"
			    		 	+" a.businessid,a.businessname,a.object,a.indexname,b.costid,b.costname"
			    		 	+" from FIBnTypeDef a,FIBnFeeTypeDef b where a.businessid=b.businessid and a.state='1' ";
				String SQL1 = "select a.versionno,a.versionremark,"
							+" (select codename from ldcode where codetype='versionstate' and code=a.versionstate fetch first 1 rows only),"
							+" a.startdate,a.enddate from FIRulesVersion a where a.versionstate='01' ";
				String SQL2 = "select a.voucherid,a.vouchername,a.versionno,"
							+" (select  versionremark from FIRulesVersion where versionno=a.versionno),"
							+" a.vouchertype,a.vouchertypename from FIVoucherDef a where a.state='01' ";
				String SQL3 = "select businessid,businessname,voucherid,vouchername,versionno from FIVoucherBn a"
							+" where exists (select 1 from FIVoucherDef b where b.voucherid=a.voucherid and b.state='01') ";
				String SQL4 = "select (select (select codename from ldcode where codetype='fibustype' and code=b.bustype fetch first 1 rows only)"
							+" from FIBnTypeDef b where b.businessid=a.businessid fetch first 1 rows only), "
							+" (select (select codename from ldcode where codetype='fidetailbustype' and code=b.detailtype fetch first 1 rows only)"
							+" from FIBnTypeDef b where b.businessid=a.businessid fetch first 1 rows only),"
							+" a.businessid,(select businessname from FIBnTypeDef b where b.businessid=a.businessid fetch first 1 rows only),"
							+" a.feeid,a.feename,(select c.itemmaincode from FIFinItemDef c where a.d_finitemid=c.finitemid fetch first 1 rows only),"
							+" a.d_finitemname,(select c.itemmaincode from FIFinItemDef c where a.c_finitemid=c.finitemid fetch first 1 rows only),"
							+" a.c_finitemname,(select d.versionremark from FIRulesVersion d where d.versionno=a.versionno fetch first 1 rows only),"
							+" a.versionno,(select e.vouchertype from FIVoucherDef e where a.voucherid=e.voucherid fetch first 1 rows only),"
							+" (select e.vouchertypename from FIVoucherDef e where a.voucherid=e.voucherid fetch first 1 rows only),"
							+" a.voucherid,(select e.vouchername from FIVoucherDef e where a.voucherid=e.voucherid fetch first 1 rows only)"
							+" from FIVoucherBnFee a where state='01' order by vouchertype,businessid,feeid ";
				String SQL5 = "select a.versionno,(case when a.finitemtype='1' then '资产' when a.finitemtype='2' then '负债' when a.finitemtype='3' then '过渡'  when a.finitemtype='4' then '表外' when a.finitemtype='6' then '损益'  else '其它' end)||'类科目',"
							+" a.finitemid,a.finitemname,a.dealmode,a.itemmaincode from FIFinItemDef a where exists (select 1 from FIRulesVersion b where a.versionno=b.versionno and b.versionstate='01') ";
				String SQL6 = "select b.versionno,b.finitemid,(case when a.finitemtype='1' then '资产' when a.finitemtype='2' then '负债' when a.finitemtype='3' then '过渡'  when a.finitemtype='4' then '表外' when a.finitemtype='6' then '损益'  else '其它' end)||'类科目',"
							+" a.finitemname,a.itemmaincode "+sql
							+" from FIInfoFinItemAssociated b,FIFinItemDef a"
							+" where a.versionno=b.versionno and a.finitemid=b.finitemid and exists (select 1 from FIRulesVersion c where c.versionno=b.versionno and c.versionstate='01') "
							+" group by b.versionno,b.finitemid,a.finitemtype,a.finitemname,a.itemmaincode ";
				
//				System.out.println(SQL6);
					//写入业务交易信息
//				"业务交易定义","版本管理","凭证模板定义","业务交易流转凭证","账务规则需求模板","科目定义模板","科目专项定义"}
				//写入业务交易定义
					WriteSheet(sheet0,SQL0,str0,wcft);
					WriteSheet_mergeCells(sheet0,wcft);
					//写入版本管理
					WriteSheet(sheet1,SQL1,str1,wcft);
					//写入凭证模板定义
					WriteSheet(sheet2,SQL2,str2,wcft);
					//写入业务交易流转凭证
					WriteSheet(sheet3,SQL3,str3,wcft);
					WriteSheet_mergeCells(sheet3,wcft);
					//写入账务规则需求模板
					WriteSheet(sheet4,SQL4,str4,wcft);
					WriteSheet_mergeCells(sheet4,wcft);
					//写入科目定义模板
					WriteSheet(sheet5,SQL5,str5,wcft);
					//写入科目专项定义
					WriteSheet(sheet6,SQL6,str7,wcft);
					
				book.write();				
				book.close();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
        	
			return true;
            
    }
//数据写入Excel中    
    private boolean WriteSheet(WritableSheet sheet,String sql,String[] str,WritableCellFormat wf) throws RowsExceededException, WriteException
    {
    	ExeSQL es = new ExeSQL();
    	SSRS mSSRS = new SSRS();
    	mSSRS = es.execSQL(sql);
    	for(int a=0;a<str.length;a++)
    	{
	    	for(int i=1;i<=mSSRS.getMaxRow();i++)
	    	{
	    		sheet.addCell(new Label(a,i,mSSRS.GetText(i, a+1),wf));
	    	}
    	}
    	
    		
    	return true;
    }
 //合并单元格   
    private boolean WriteSheet_mergeCells(WritableSheet sheet,WritableCellFormat wf) throws RowsExceededException, WriteException
    {
    	System.out.println(sheet.getCell(1, 1).getContents());
    	int cols = sheet.getColumns();
    	int rows = sheet.getRows();
    	System.out.println(rows);
    	int a = 1;
    	int rs = 0;
    	for(int n=0;n<cols;n++)
    	{
    		if(n==0||n==1||n==2||n==3||n==14)
    		{
		    	do
		    	{
			    	for(int i=a;i<rows-1;i++)
			    	{
			    		String str1=sheet.getCell(n, i).getContents();
			    		String str2=sheet.getCell(n, i+1).getContents();
			    		System.out.println("--------n:"+n+"---i:"+i+"----str1"+str1);
			    		if(str1.equals(str2))
			    		{
			    			rs=i+1;
			    			if(rs==rows-1)
			    			{
			    				String str = sheet.getCell(n, a).getContents();
				    	    	System.out.println(str);
				    	    	for(int m=a;m<=rs;m++)
				    	    	{	System.out.println("------m:"+m);
				    	    		sheet.addCell(new Label(n,m,""));
				    	    	}
				    	    	System.out.println(a);
				    	    	System.out.println(rs);
				    	    	sheet.mergeCells(n, a, n, rs);
				    	    	sheet.addCell(new Label(n,a,str,wf));
			    			}
			    		} 
			    		else 
			    		{
			    			System.out.println("======================");
			    	    	String str = sheet.getCell(n, a).getContents();
			    	    	System.out.println(str);
			    	    	for(int m=a;m<=rs;m++)
			    	    	{	System.out.println("------m:"+m);
			    	    		sheet.addCell(new Label(n,m,""));
			    	    	}
			    	    	System.out.println(a);
			    	    	System.out.println(rs);
			    	    	sheet.mergeCells(n, a, n, rs);
			    	    	sheet.addCell(new Label(n,a,str,wf));
			    			a = i+1;
			    			break;
			    		}
			    	}
		    	}
		    	while(a<rows-1&&rs<rows-1);
		    	a = 1;
		    	rs = 0;
    		}
    	}
    		
    	return true;
    }

  
    private boolean getInputData(VData cInputData, String cOperate)
	{
		mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
		ImportPath = cOperate;
		tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData",0);
		if (mGlobalInput == null) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "UpLoadWriteFinBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得操作员编码
		mOperater = mGlobalInput.Operator;
        if ((mOperater == null) || mOperater.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "UpLoadWriteFinBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
    
        if ((mManageCom == null) || mManageCom.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "UpLoadWriteFinBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        System.out.println(ImportPath);
        if ((ImportPath == null) || ImportPath.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "UpLoadWriteFinBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据ImportPath失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
		return true;
	}

    
	private boolean checkData()
	{
		return true;
	}
	
	private boolean prepareOutputData()
	{
		try
		{
			mInputData.clear();
			mInputData.add(map);
		}
		catch(Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UpLoadWriteFinBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
 
	/**
	 * 提交数据
	 * @return
	 */
	private boolean pubSubmit()
	{
		// 进行数据提交
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, ""))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "UpLoadWriteFinBL";
			tError.functionName = "PubSubmit.submitData";
			tError.errorMessage = "数据提交失败UpLoadWriteFinBL-->pubSubmit!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
    /**
     * 测试
     * @param args
     * @throws WriteException 
     * @throws RowsExceededException 
     */
    public static void main(String[] args)
    {
        UpLoadWriteFinBL read = new UpLoadWriteFinBL();
        try {
			read.WriteXls();
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
//			System.out.(e.printStackTrace());
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
}
