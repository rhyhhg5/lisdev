package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.FIAbStandardDataDB;
import com.sinosoft.lis.db.InterfaceTableDB;
import com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl.SecondaryRule_J01;
import com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl.SecondaryRule_J02;
import com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl.StandSecondaryRule;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.schema.FIDataBaseLinkSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.lis.vschema.InterfaceTableSet;
import com.sinosoft.lis.vschema.LIAboriginalDataSet;

public class LRiskReceProvisionBL extends StandSecondaryRule
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private String StartDate = "";
    private String EndDate = "";
    private String ipdate = "";
    SSRS tSSRS = new SSRS();
	//处理批次
	private String tBatchno = "";
    private String mCurDate = PubFun.getCurrentDate();
    	
	private String[][] mToExcel = null;
	      

    public LRiskReceProvisionBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate) 
    {

    	System.out.println("Start LRiskReceProvisionBL Submit ...");
        try{
        
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }  
        
//        二次处理类提取应收计提的数据
          prepareRule();
//        应收计提反冲数据          
          ppRule();                
          
  		if (!moveDataToFIAbStandardData())
		{
			return false;
		}
  		  if (!mDataToFIAbStandardData())
		{
			return false;
		}
        } catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LRiskReceProvisionBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理错误! " + e.getMessage();
            this.mErrors .addOneError(tError);
            return false;
          }
        
        if (!pdpzData())
        {
            return false;
        }
        
        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    public String prepareRule()
    {
    	StringBuffer tSQL = new StringBuffer();
	    tSQL.append(" select '' as SerialNo,");
		tSQL.append(" '0' as NoType,");
		tSQL.append(" '' as ASerialNo,");
		tSQL.append(" '1' as SecondaryFlag,");
		tSQL.append(" 'J1' as SecondaryType,");
		tSQL.append(" 'O-XQ-000006' as BusTypeID,");
		tSQL.append(" 'F0000000024' as CostID,");
		tSQL.append(" '10' as BusinessType,");
		tSQL.append(" '104' as BusinessDetail,");
		tSQL.append(" '' as FeeType,");
		tSQL.append(" '' as FeeDetail,");		
		tSQL.append(" '05' as IndexCode,");
		tSQL.append(" a.contno as IndexNo,");			
		tSQL.append(" '1' as ListFlag,");
		tSQL.append(" '' as GrpContNo,");
		tSQL.append(" '' as GrpPolNo, ");
		tSQL.append(" a.ContNo as ContNo,");
		tSQL.append(" a.polno as PolNo,");
		tSQL.append(" '' as EndorsementNo,");
		tSQL.append(" '' as NotesNo,");
		tSQL.append(" 'X' as FirstYearFlag,");
		tSQL.append(" 'X' as FirstTermFlag,");
		tSQL.append(" '' as BonusType,");
		tSQL.append(" '0' as DifComFlag,");
		tSQL.append(" a.riskcode as RiskCode,");
		tSQL.append(" 'L' as RiskPeriod,");
		tSQL.append(" '0' as RiskType,");
		tSQL.append(" '' as RiskType1,");
		tSQL.append(" '' as RiskType2,");
		tSQL.append(" (select costcenter from labranchgroup where agentgroup = a.agentgroup fetch first 1 rows only) as CostCenter,");//成本中心
		tSQL.append(" (select actype from lacom where agentcom=a.agentcom fetch first 1 rows only) as ACType,"); //中介机构类别
		tSQL.append(" a.AgentCom as AgentCom,");
		tSQL.append(" a.AgentGroup as AgentGroup,");
		tSQL.append(" a.AgentCode as AgentCode,");
		tSQL.append(" '99999999' as CustomerID,");
		tSQL.append(" '' as SupplierNo,");//供应商号码
		tSQL.append(" a.SaleChnl as SaleChnl,");
		tSQL.append(" '' as SaleChnlDetail,");
		tSQL.append(" a.ManageCom as ManageCom,");
		tSQL.append(" '' as ExecuteCom,");
		tSQL.append(" a.CValiDate as CValiDate,");
		tSQL.append(" a.PayIntv as PayIntv,");
		tSQL.append(" cast(null as date) as LastPayToDate,");
		tSQL.append(" cast(null as date) as CurPayToDate,");
		tSQL.append(" LF_PolYear(a.PayIntv, a.PayYears) as PolYear,");
		tSQL.append(" LF_Years(a.InsuYearFlag, a.InsuYear, a.Years) as Years,");
		tSQL.append(" '99' as PremiumType,");
		tSQL.append(" 0 as PayCount,");
		tSQL.append(" '' as PayMode,");
		tSQL.append(" '' as BankCode,");
		tSQL.append(" '' as AccName,");
		tSQL.append(" '' as BankAccNo,");
		tSQL.append(" a.prem as SumActuMoney,");
		tSQL.append(" cast(null as date) as MothDate,");
		tSQL.append(" cast(null as date) as BusinessDate,");
		tSQL.append(" (date('"+ipdate+"') - 1 days) as AccountDate,");
		tSQL.append(" '' as CashFlowNo,");
		tSQL.append(" (select substr(char(confdate), 1, 4) from ljapay where incomeno = a.contno and duefeetype = '0' fetch first 1 rows only) as FirstYear,");//
		tSQL.append(" '1' as MarketType,");
		tSQL.append(" '' as OperationType,");
		tSQL.append(" '' as Budget,");
		tSQL.append(" 'CNY' as Currency ,");
		tSQL.append(" a.AppntNo as StringInfo01,"); //业务明细
		tSQL.append(" '' as StringInfo02,");
		tSQL.append(" '' as StringInfo03,");
		tSQL.append(" cast(null as date) as DateInfo01,");
		tSQL.append(" cast(null as date) as DateInfo02,");
		tSQL.append(" '' as RelatedNo,");
		tSQL.append(" '' as EventNo,");
		tSQL.append(" '00' as State,"); //待凭证转换
		tSQL.append(" '0' as ReadState,"); //未同步
		tSQL.append(" '00' as CheckFlag,");
		tSQL.append(" '"+mGI.Operator+"' as Operator,");
		tSQL.append(" '"+PubFun.getCurrentDate()+"' as MakeDate,");
		tSQL.append(" '"+PubFun.getCurrentTime()+"' as MakeTime ");
		tSQL.append(" from lcpol a where month(date('"+ipdate+"') - 1 days) <> month(date('"+ipdate+"'))");
		tSQL.append(" and a.PayToDate < (date('"+ipdate+"') - 1 days)  and a.payenddate >(date('"+ipdate+"') - 1 days) " +
				" and a.ContType = '1' and a.Payintv <> 0 and a.StateFlag = '1' and a.prem <> 0 " +
				"and exists (select 1 from lmriskapp where riskcode = a.riskcode and riskperiod='L' and kindcode <> 'U') " +
				"and not exists(select 1 from FIAbStandardData c where c.contno = a.contno and c.SecondaryType='J1' and c.accountdate=(date('"+ipdate+"') - 1 days))");
		tSQL.append(" union all ");
		tSQL.append(" select '' as SerialNo,");
		tSQL.append(" '0' as NoType,");
		tSQL.append(" '' as ASerialNo,");
		tSQL.append(" '1' as SecondaryFlag,");
		tSQL.append(" 'J2' as SecondaryType,");
		tSQL.append(" 'O-XQ-000006' as BusTypeID,");
		tSQL.append(" 'F0000000024' as CostID,");
		tSQL.append(" '10' as BusinessType,");
		tSQL.append(" '104' as BusinessDetail,");
		tSQL.append(" '' as FeeType,");
		tSQL.append(" '' as FeeDetail,");		
		tSQL.append(" '05' as IndexCode,");
		tSQL.append(" a.contno as IndexNo,");			
		tSQL.append(" '1' as ListFlag,");
		tSQL.append(" '' as GrpContNo,");
		tSQL.append(" '' as GrpPolNo, ");
		tSQL.append(" a.ContNo as ContNo,");
		tSQL.append(" a.polno as PolNo,");
		tSQL.append(" '' as EndorsementNo,");
		tSQL.append(" '' as NotesNo,");
		tSQL.append(" 'X' as FirstYearFlag,");
		tSQL.append(" 'X' as FirstTermFlag,");
		tSQL.append(" '' as BonusType,");
		tSQL.append(" '0' as DifComFlag,");
		tSQL.append(" a.riskcode as RiskCode,");
		tSQL.append(" (select riskperiod from lmriskapp where riskcode = a.riskcode fetch first 1 rows only) as RiskPeriod,");
		tSQL.append(" '0' as RiskType,");
		tSQL.append(" '' as RiskType1,");
		tSQL.append(" '' as RiskType2,");
		tSQL.append(" (select costcenter from labranchgroup where agentgroup = a.agentgroup fetch first 1 rows only) as CostCenter,");//成本中心
		tSQL.append(" (select actype from lacom where agentcom=a.agentcom fetch first 1 rows only) as ACType,"); //中介机构类别
		tSQL.append(" a.AgentCom as AgentCom,");
		tSQL.append(" a.AgentGroup as AgentGroup,");
		tSQL.append(" a.AgentCode as AgentCode,");
		tSQL.append(" '99999999' as CustomerID,");
		tSQL.append(" '' as SupplierNo,");//供应商号码
		tSQL.append(" a.SaleChnl as SaleChnl,");
		tSQL.append(" '' as SaleChnlDetail,");
		tSQL.append(" a.ManageCom as ManageCom,");
		tSQL.append(" '' as ExecuteCom,");
		tSQL.append(" a.CValiDate as CValiDate,");
		tSQL.append(" a.PayIntv as PayIntv,");
		tSQL.append(" cast(null as date) as LastPayToDate,");
		tSQL.append(" cast(null as date) as CurPayToDate,");
		tSQL.append(" LF_PolYear(a.PayIntv, a.PayYears) as PolYear,");
		tSQL.append(" LF_Years(a.InsuYearFlag, a.InsuYear, a.Years) as Years,");
		tSQL.append(" '99' as PremiumType,");
		tSQL.append(" 0 as PayCount,");
		tSQL.append(" '' as PayMode,");
		tSQL.append(" '' as BankCode,");
		tSQL.append(" '' as AccName,");
		tSQL.append(" '' as BankAccNo,");
		tSQL.append(" a.prem as SumActuMoney,");
		tSQL.append(" cast(null as date) as MothDate,");
		tSQL.append(" cast(null as date) as BusinessDate,");
		tSQL.append(" (date('"+ipdate+"')- 1 days) as AccountDate,");
		tSQL.append(" '' as CashFlowNo,");
		tSQL.append(" (select substr(char(confdate), 1, 4) from ljapay where incomeno = a.contno and duefeetype = '0' fetch first 1 rows only) as FirstYear,");//
		tSQL.append(" '1' as MarketType,");
		tSQL.append(" '' as OperationType,");
		tSQL.append(" '' as Budget,");
		tSQL.append(" 'CNY' as Currency ,");
		tSQL.append(" a.AppntNo as StringInfo01,"); //业务明细
		tSQL.append(" '' as StringInfo02,");
		tSQL.append(" '' as StringInfo03,");
		tSQL.append(" cast(null as date) as DateInfo01,");
		tSQL.append(" cast(null as date) as DateInfo02,");
		tSQL.append(" '' as RelatedNo,");
		tSQL.append(" '' as EventNo,");
		tSQL.append(" '00' as State,"); //待凭证转换
		tSQL.append(" '0' as ReadState,"); //未同步
		tSQL.append(" '00' as CheckFlag,");
		tSQL.append(" '"+mGI.Operator+"' as Operator,");
		tSQL.append(" '"+PubFun.getCurrentDate()+"' as MakeDate,");
		tSQL.append(" '"+PubFun.getCurrentTime()+"' as MakeTime ");
		tSQL.append(" from lcpol a where month(date('"+ipdate+"') - 1 days) <> month(date('"+ipdate+"'))");
		tSQL.append("  and a.PayToDate < (date('"+ipdate+"') - 1 days) and a.payenddate >(date('"+ipdate+"') - 1 days) " +
				" and a.ContType = '1' and a.Payintv <> 0 and a.StateFlag = '1' and a.prem <> 0 " +
				"and exists (select 1 from lmriskapp where riskcode = a.riskcode and riskperiod in ('S','M')) " +
				"and exists (select 1 from LJSPayPersonB  where PolNo = a.PolNo and dealstate is not null) " +
				"and not exists(select 1 from FIAbStandardData c where c.contno = a.contno and c.SecondaryType='J2' and c.accountdate=(date('"+ipdate+"') - 1 days))");
		tSQL.append(" union all ");
		tSQL.append(" select '' as SerialNo,");
		tSQL.append(" '0' as NoType,");
		tSQL.append(" '' as ASerialNo,");
		tSQL.append(" '1' as SecondaryFlag,");
		tSQL.append(" 'J3' as SecondaryType,");
		tSQL.append(" 'O-XQ-000006' as BusTypeID,");
		tSQL.append(" 'F0000000024' as CostID,");
		tSQL.append(" '10' as BusinessType,");
		tSQL.append(" '104' as BusinessDetail,");
		tSQL.append(" '' as FeeType,");
		tSQL.append(" '' as FeeDetail,");		
		tSQL.append(" '05' as IndexCode,");
		tSQL.append(" a.grpcontno as IndexNo,");			
		tSQL.append(" '2' as ListFlag,");
		tSQL.append(" a.grpcontno as GrpContNo,");
		tSQL.append(" a.grppolno as GrpPolNo, ");
		tSQL.append(" '' as ContNo,");
		tSQL.append(" '' as PolNo,");
		tSQL.append(" '' as EndorsementNo,");
		tSQL.append(" '' as NotesNo,");
		tSQL.append(" 'X' as FirstYearFlag,");
		tSQL.append(" 'X' as FirstTermFlag,");
		tSQL.append(" '' as BonusType,");
		tSQL.append(" '0' as DifComFlag,");
		tSQL.append(" a.riskcode as RiskCode,");
		tSQL.append(" (select riskperiod from lmriskapp where riskcode = a.riskcode fetch first 1 rows only) as RiskPeriod,");
		tSQL.append(" '0' as RiskType,");
		tSQL.append(" '' as RiskType1,");
		tSQL.append(" '' as RiskType2,");
		tSQL.append(" (select costcenter from labranchgroup where agentgroup = a.agentgroup fetch first 1 rows only) as CostCenter,");//成本中心
		tSQL.append(" (select actype from lacom where agentcom=a.agentcom fetch first 1 rows only) as ACType,"); //中介机构类别
		tSQL.append(" a.AgentCom as AgentCom,");
		tSQL.append(" a.AgentGroup as AgentGroup,");
		tSQL.append(" a.AgentCode as AgentCode,");
		tSQL.append(" LF_BClient('2', a.GrpContNo) as CustomerID,");
		tSQL.append(" '' as SupplierNo,");//供应商号码
		tSQL.append(" a.SaleChnl as SaleChnl,");
		tSQL.append(" '' as SaleChnlDetail,");
		tSQL.append(" a.ManageCom as ManageCom,");
		tSQL.append(" '' as ExecuteCom,");
		tSQL.append(" a.CValiDate as CValiDate,");
		tSQL.append(" a.PayIntv as PayIntv,");
		tSQL.append(" cast(null as date) as LastPayToDate,");
		tSQL.append(" cast(null as date) as CurPayToDate,");
		tSQL.append(" LF_GrpPolYear(a.GrpPolNo) as PolYear,");
		tSQL.append(" LF_GrpYears(a.GrpPolNo) as Years,");
		tSQL.append(" '99' as PremiumType,");
		tSQL.append(" 0 as PayCount,");
		tSQL.append(" '' as PayMode,");
		tSQL.append(" '' as BankCode,");
		tSQL.append(" '' as AccName,");
		tSQL.append(" '' as BankAccNo,");
		tSQL.append(" a.prem as SumActuMoney,");
		tSQL.append(" cast(null as date) as MothDate,");
		tSQL.append(" cast(null as date) as BusinessDate,");
		tSQL.append(" (date('"+ipdate+"')- 1 days) as AccountDate,");
		tSQL.append(" '' as CashFlowNo,");
		tSQL.append(" (select substr(char(confdate), 1, 4) from ljapay where incomeno = a.grpcontno and duefeetype = '0' fetch first 1 rows only) as FirstYear,");//
		tSQL.append(" LF_MarketType(a.GrpContNo) as MarketType,");
		tSQL.append(" '' as OperationType,");
		tSQL.append(" '' as Budget,");
		tSQL.append(" 'CNY' as Currency ,");
		tSQL.append(" a.CustomerNo as StringInfo01,"); //业务明细
		tSQL.append(" '' as StringInfo02,");
		tSQL.append(" '' as StringInfo03,");
		tSQL.append(" cast(null as date) as DateInfo01,");
		tSQL.append(" cast(null as date) as DateInfo02,");
		tSQL.append(" '' as RelatedNo,");
		tSQL.append(" '' as EventNo,");
		tSQL.append(" '00' as State,"); //待凭证转换
		tSQL.append(" '0' as ReadState,"); //未同步
		tSQL.append(" '00' as CheckFlag,");
		tSQL.append(" '"+mGI.Operator+"' as Operator,");
		tSQL.append(" '"+PubFun.getCurrentDate()+"' as MakeDate,");
		tSQL.append(" '"+PubFun.getCurrentTime()+"' as MakeTime ");
		tSQL.append(" from lcgrppol a where month(date('"+ipdate+"') - 1 days) <> month(date('"+ipdate+"'))");
		tSQL.append(" and a.PayToDate < (date('"+ipdate+"') - 1 days)  and a.payenddate >(date('"+ipdate+"') - 1 days) " +
				" and a.Payintv <> 0 and a.StateFlag = '1' and a.prem <> 0 " +
				"and exists (select 1 from lmriskapp where riskcode = a.riskcode and riskperiod ='L' and kindcode<>'U') " +
				"and not exists(select 1 from FIAbStandardData c where c.grpcontno = a.grpcontno and c.SecondaryType='J3' and c.accountdate=(date('"+ipdate+"') - 1 days))");
//		System.out.print("tSQL:"+tSQL);
			return tSQL.toString();
	}
    
    private boolean moveDataToFIAbStandardData()
	{
    	String ruleSQL = prepareRule();
		if(ruleSQL==null||"".equals(ruleSQL))
		{
			buiError("StandSecondaryRule", "获取规则失败失败!");
			return false;
		}
		RSWrapper  rsWrapper  = new RSWrapper();		
		FIAbStandardDataSet tFIAbStandardDataSet = new FIAbStandardDataSet();
		
        if (!rsWrapper.prepareData(tFIAbStandardDataSet, ruleSQL+" with ur "))
        {
            mErrors.copyAllErrors(rsWrapper.mErrors);
            System.out.println(rsWrapper.mErrors.getFirstError());
            buiError("StandSecondaryRule", "二次处理，规则执行出错,提示信息为：" +rsWrapper.mErrors.getFirstError());
            return false;
        }
        do
        {
           	rsWrapper.getData();
            if (tFIAbStandardDataSet != null && tFIAbStandardDataSet.size() > 0)
            {
            	
            	if(!dealFIAbStandardData(tFIAbStandardDataSet))
        		{
            		buiError("StandSecondaryRule", "二次处理，数据处理出现错误！");
        		}
            }    
        }while(tFIAbStandardDataSet != null && tFIAbStandardDataSet.size() > 0);

        rsWrapper.close();	
		
		return true;
	}
    
	private boolean dealFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		MMap tMap= new MMap();
		VData tInputData = new VData();
		PubSubmit tPubSubmit = new PubSubmit();
		
		//获取处理后的数据
		FIAbStandardDataSet outFIAbStandardDataSet = prepareFIAbStandardData(tFIAbStandardDataSet);
		
		tMap.put(outFIAbStandardDataSet, "INSERT");
		
		String tsql="update FIAbStandardData a set (a.FirstTermFlag, a.RiskPeriod) = (select (CASE when not exists (select 1 from LCRnewStateLog where (contno = c.contno or NewContNo = c.contno) AND state = '6' and renewcount = '1'"+
		"and c.accountdate >= paytodate) AND not exists (select 1 from LbRnewStateLog where (contno = c.contno OR NewContNo = c.contno) AND state = '6' and renewcount = '1' and c.accountdate >= paytodate) THEN 'S' ELSE 'X' end), 'L'"+
		"from FIAbStandardData c where c.serialno = a.serialno) where a.state = '00' and a.riskperiod <> 'L' and (exists (select 1 from lmrisk b where b.riskcode = a.riskcode and b.rnewflagb = 'Y'))";
		
		tMap.put(tsql, "UPDATE");
		
		tInputData.add(tMap);
		
		if (!tPubSubmit.submitData(tInputData, ""))
		{
			buiError("StandSecondaryRule", "数据存储失败!");
			return false;
		}
		
		return true;
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				//应收计提
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tSerialNo[i-1]);
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//		
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
  }

	public String[][] getMToExcel() {
		return mToExcel;
	}	
	
	 public String ppRule(){
		 
	StringBuffer tSQL = new StringBuffer();
	tSQL.append(" select * from FIAbStandardData a where a.BusTypeID in ('O-XQ-000006') and a.accountdate=(date('"+ipdate+"') - 1 days) and month(date('"+ipdate+"') - 1 days) <> month(date('"+ipdate+"')) ");
	tSQL.append(" and not exists (select 1 from FIAbStandardData b where (b.grpcontno=a.grpcontno or b.contno=a.contno) and (b.grppolno=a.grppolno or b.polno=a.polno) and b.SecondaryType='J4' and b.accountdate='"+ipdate+"')");
    return tSQL.toString();
	}
	 private boolean mDataToFIAbStandardData()
		{
	    	String ruleSQL = ppRule();
			if(ruleSQL==null||"".equals(ruleSQL))
			{
				buiError("StandSecondaryRule", "获取规则失败失败!");
				return false;
			}
			RSWrapper  rsWrapper  = new RSWrapper();		
			FIAbStandardDataSet mFIAbStandardDataSet = new FIAbStandardDataSet();
			
	        if (!rsWrapper.prepareData(mFIAbStandardDataSet, ruleSQL+" with ur "))
	        {
	            mErrors.copyAllErrors(rsWrapper.mErrors);
	            System.out.println(rsWrapper.mErrors.getFirstError());
	            buiError("StandSecondaryRule", "二次处理，规则执行出错,提示信息为：" +rsWrapper.mErrors.getFirstError());
	            return false;
	        }
	        do
	        {
	           	rsWrapper.getData();
	            if (mFIAbStandardDataSet != null && mFIAbStandardDataSet.size() > 0)
	            {
	            	
	            	if(!delFIAbStandardData(mFIAbStandardDataSet))
	        		{
	            		buiError("StandSecondaryRule", "二次处理，数据处理出现错误！");
	        		}
	            }    
	        }while(mFIAbStandardDataSet != null && mFIAbStandardDataSet.size() > 0);

	        rsWrapper.close();	
			
			return true;
		}
	    
		private boolean delFIAbStandardData(FIAbStandardDataSet mFIAbStandardDataSet)
		{
			MMap tMap= new MMap();
			VData tInputData = new VData();
			PubSubmit tPubSubmit = new PubSubmit();
			
			//获取处理后的数据
			FIAbStandardDataSet outtFIAbStandardDataSet = ppFIAbStandardData(mFIAbStandardDataSet);
			
			tMap.put(outtFIAbStandardDataSet, "INSERT");
			String tsql="update FIAbStandardData a set (a.FirstTermFlag, a.RiskPeriod) = (select (CASE when not exists (select 1 from LCRnewStateLog where (contno = c.contno or NewContNo = c.contno) AND state = '6' and renewcount = '1'"+
		"and c.accountdate >= paytodate) AND not exists (select 1 from LbRnewStateLog where (contno = c.contno OR NewContNo = c.contno) AND state = '6' and renewcount = '1' and c.accountdate >= paytodate) THEN 'S' ELSE 'X' end), 'L'"+
		"from FIAbStandardData c where c.serialno = a.serialno) where a.state = '00' and a.riskperiod <> 'L' and (exists (select 1 from lmrisk b where b.riskcode = a.riskcode and b.rnewflagb = 'Y'))";
		
		tMap.put(tsql, "UPDATE");
			tInputData.add(tMap);
			
			if (!tPubSubmit.submitData(tInputData, ""))
			{
				buiError("StandSecondaryRule", "数据存储失败!");
				return false;
			}
			
			return true;
		}
	 public FIAbStandardDataSet ppFIAbStandardData(FIAbStandardDataSet mFIAbStandardDataSet)
		{
			FIAbStandardDataSet ppFIAbStandardDataSet = mFIAbStandardDataSet;
			
			try 
			{
				String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(ppFIAbStandardDataSet.size());
				
				for(int i=1;i<=ppFIAbStandardDataSet.size();i++)			
				{
					//月初一号反冲应收计提
					FIAbStandardDataSchema mFIAbStandardDataSchema = ppFIAbStandardDataSet.get(i);
					mFIAbStandardDataSchema.setSumActuMoney(-mFIAbStandardDataSchema.getSumActuMoney());
					mFIAbStandardDataSchema.setAccountDate(ipdate);
					mFIAbStandardDataSchema.setBusinessDetail("105");
					mFIAbStandardDataSchema.setCostID("F0000000025");
					mFIAbStandardDataSchema.setSecondaryType("J4");
					mFIAbStandardDataSchema.setSecondaryFlag("1");
					mFIAbStandardDataSchema.setRelatedNo(mFIAbStandardDataSchema.getSerialNo());
					mFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//		
					mFIAbStandardDataSchema.setBusTypeID("O-XQ-000007");

				}	
			} 
			catch (Exception e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return ppFIAbStandardDataSet;
		}
	 
	 
	 private boolean pdpzData() {
	    	
			ExeSQL tExeSQL = new ExeSQL();
	    	String tSQL = "Select 1 from fiabstandarddata Where makedate = '"+mCurDate+"' and accountdate in('"+ipdate+"',(date('"+ipdate+"') - 1 days)) and secondarytype in ('J1','J4') and state = '00' ";
	        System.out.println(tSQL);
	        System.out.println(mCurDate);
	        tSSRS = tExeSQL.execSQL(tSQL);  
	        
	      if(tSSRS.getMaxRow()<= 0)
	      {
	    	  buiError("StandSecondaryRule", "没有待生成凭证的长险应收计提数据!");
	    	  return false;  
	    	     	  
	      }else {
	   		VoucherDataTrans();
	      }
	      
	       return true;
	    }
	 
	 public void VoucherDataTrans()
		{		
			VData tVData  = new VData();
			String DataType = "1";
			
			tVData.add(mGI);
			tVData.add(DataType);
			
			FIDistillCertificateUI tFIDistillCertificateUI = new FIDistillCertificateUI();
			try
			{
				if(!tFIDistillCertificateUI.submitData(tVData,""))
				{
					 System.out.println("凭证转换出错！");
				}
				tBatchno = (String)tFIDistillCertificateUI.getResult().get(0);
			}
			catch(Exception ex)
			{
				System.out.println("执行异常，原因是:" + ex.toString());
			}
			
			
		}
	 
	 
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     * @throws java.text.ParseException 
     */
    public boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
        System.out.println(mGI.ManageCom);

        if (mGI == null || tf == null)
        {
        	buiError("getInputData","数据维护出错");
            return false;
        }
        
        String[] StartDate = tf.getValueByName("StartDate").toString().split("-");
        String[] EndDate = tf.getValueByName("EndDate").toString().split("-");
        if(StartDate[1].equals(EndDate[1]))
        {
        	if(Integer.parseInt(StartDate[2])==1)
        	{
        		ipdate=tf.getValueByName("StartDate").toString();
        	}
        }else if(!StartDate[1].equals(EndDate[1]))
        {
        	
        		ipdate=EndDate[0]+"-"+EndDate[1]+"-01";
        	
        }


        if (EndDate == null || StartDate == null)
        {
            buiError("getInputData", "传入信息不完整！");
            return false;
        }
        return true;
    }
    
    private void buiError(String functionName,String errorMessage){
    	CError tError = new CError();
        tError.moduleName = "LRiskReceProvisionBL";
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        mErrors.addOneError(tError);
    }
    
    public static void main(String[] args)
    {   	
    	
}
    }

