/**
 * <p>ClassName: FIBusTypeDefUI.java </p>
 * <p>Description: 业务交易定义 </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: Sinosoft </p>
 * @Database:
 * @author 董健
 * @CreateDate：2011/8/25
 */

//包名
package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.utility.*;


public class FIBusTypeDefUI {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    
    public FIBusTypeDefUI() {

    }

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate){
       // 将操作数据拷贝到本类中
       this.mOperate = cOperate;
       FIBusTypeDefBL tFIBusTypeDefBL = new FIBusTypeDefBL();
      
       
       System.out.println("---tFIBusTypeDefBL UI BEGIN---");
       if (tFIBusTypeDefBL.submitData(cInputData, mOperate) == false) {
           // @@错误处理
           this.mErrors.copyAllErrors(tFIBusTypeDefBL.mErrors);
           CError tError = new CError();
           tError.moduleName = "FIBusTypeDefUI";
           tError.functionName = "submitData";
           tError.errorMessage = "数据提交失败!";
           this.mErrors.addOneError(tError);
           mResult.clear();
           return false;
       }
       
       if (mOperate.equals("INSERT||MAIN")){
           this.mResult.clear();
           this.mResult = tFIBusTypeDefBL.getResult();
       }
       return true;
    }

    public static void main(String[] args){
    }
    
    public VData getResult(){
       return this.mResult;
    }
}
