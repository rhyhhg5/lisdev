package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2008</p>
 *
 * <p>Company: </p>
 *
 * @author jw
 * @version 1.0
 */
public class FIDistillRollBack {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = new GlobalInput();
    private String BatchNo = "";
    private String AppNo = "";
    
    private FIAbStandardDataSet oFIAbStandardDataSet = new FIAbStandardDataSet();
    private FIVoucherDataDetailSet oFIVoucherDataDetailSet = new FIVoucherDataDetailSet();
    private FIVoucherDataGatherSet oFIVoucherDataGatherSet = new FIVoucherDataGatherSet();

    StringBuffer logString = new StringBuffer();
    public String Content = "";
    private final String enter = "\r\n"; // 换行符
    public FIOperationLog tLogInfoDeal ;

    public FIDistillRollBack()
    {
    }

    public boolean dealProcess(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }
        
        if (!DealWithData()) 
        {
            return false;
        }
//        tLogInfoDeal.Complete(true);
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        AppNo = (String) cInputData.get(1);
        if (mGlobalInput == null)
        {
            buildError("FIDistillRollBack", "getInputData", "传入登陆信息参数为空");
            return false;
        }
        if (AppNo == null || AppNo.equals(""))
        {
            buildError("FIDistillRollBack", "getInputData", "传入红冲申请号码为空");
            return false;
        }
        return true;
    }

    private boolean DealWithData()
    {
        try
        {
        	FIAbStandardDataDB tFIAbStandardDataDB = new FIAbStandardDataDB();
        	FIAbStandardDataSet tFIAbStandardDataSet = new FIAbStandardDataSet();
        	
        	FIVoucherDataDetailDB tFIVoucherDataDetailDB = new FIVoucherDataDetailDB();
        	FIVoucherDataDetailSet tFIVoucherDataDetailSet = new FIVoucherDataDetailSet();
        	
        	tFIAbStandardDataDB.setStringInfo01(AppNo);
        	tFIAbStandardDataSet = tFIAbStandardDataDB.query();
        	
        	String Serialno = "";
        	
        	if(tFIAbStandardDataSet.size()==0)
        	{
        		 buildError("FIDistillRollBack","DealWithData", "FIAbStandardData表没有关联此申请号的数据：" + AppNo);
        		 return false;
        	}
        	for(int i=1; i<=tFIAbStandardDataSet.size(); i++)
        	{
        		FIAbStandardDataSchema tFIAbStandardDataSchema = tFIAbStandardDataSet.get(i);
        		Serialno = tFIAbStandardDataSchema.getSerialNo();
        		
        		tFIVoucherDataDetailDB.setSerialNo(Serialno);
        		tFIVoucherDataDetailSet = tFIVoucherDataDetailDB.query();
        		if(tFIVoucherDataDetailSet.size()==0)
        		{
        			buildError("FIDistillRollBack","DealWithData", "FIVoucherDataDetail表没有流水号为"+Serialno+"的数据!" );
           		 	return false;
        		}
        		for(int j=1; j<=tFIVoucherDataDetailSet.size(); j++)
        		{
        			FIVoucherDataDetailSchema tFIVoucherDataDetailSchema = tFIVoucherDataDetailSet.get(j);
        			String VSerialno = tFIVoucherDataDetailSchema.getVSerialNo();
        			
//        			tFIVoucherDataGatherDB.setVersionNo(VSerialno);
//        			tFIVoucherDataGatherSet = tFIVoucherDataGatherDB.query();
//        			if(tFIVoucherDataGatherSet.size()==0)
//        			{
//        				buildError("FIDistillRollBack","DealWithData", "FIVoucherDataGather表没有流水号为"+VSerialno+"的数据!" );
//               		 	return false;
//        			}
//        			for(int z=1; z<=tFIVoucherDataGatherSet.size(); z++)
//        			{
//        				FIVoucherDataGatherSchema tFIVoucherDataGatherSchema = tFIVoucherDataGatherSet.get(z);
//        				String readstate = tFIVoucherDataGatherSchema.getReadState();
//        				
//        				if("1".equals(readstate)||"3".endsWith(readstate)||"2".endsWith(readstate))
//        				{
//        					buildError("FIDistillRollBack","DealWithData", "FIVoucherDataGather表中流水号"+VSerialno+"的数据已经被提取，不能回滚！" );
//                   		 	return false;
//        				}
//        				
//        				oFIVoucherDataGatherSet.add(tFIVoucherDataGatherSchema);
//        				
//        			}
        			oFIVoucherDataDetailSet.add(tFIVoucherDataDetailSchema);
        			
        		}
        		oFIAbStandardDataSet.add(tFIAbStandardDataSchema);
        	}
        	
        	
            MMap tmap = new MMap();
            VData tInputData = new VData();
            tmap.put(oFIAbStandardDataSet, "DELETE");
            tmap.put(oFIVoucherDataDetailSet, "DELETE");
            tmap.put(oFIVoucherDataGatherSet, "DELETE");
            tInputData.add(tmap);
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(tInputData, ""))
            {
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                buildError("FIDistillRollBack","DealWithData", "流水号为" + BatchNo + "从本地数据删除相关数据失败，错误信息为：" + tPubSubmit.mErrors.getFirstError());
                return false;
            }

            return true;
        }
        catch (Exception ex)
        {
            buildError("FIDistillRollBack","DealWithData", "批次为" + BatchNo + "从数据源删除相关数据失败，错误信息为：" + ex.getMessage());
            tLogInfoDeal.WriteLogTxt("批次为" + BatchNo + "从数据源删除相关数据失败，错误信息为：" + ex.getMessage() + enter);
            ex.printStackTrace();
            return false;
        }
    }


    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }

    public static void main(String[] args)
    {
        VData vData = new VData();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";
        String Batchno = "00000000000000000104";
        vData.addElement(tG);
        vData.addElement(Batchno);
        FIDistillRollBack tFIDistillRollBack = new FIDistillRollBack();
        tFIDistillRollBack.dealProcess(vData);
        tFIDistillRollBack.mErrors.getFirstError();
    }

}
