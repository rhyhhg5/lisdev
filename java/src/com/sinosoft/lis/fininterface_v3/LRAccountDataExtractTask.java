package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.db.FIDataExtractDefDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;

import java.util.*;
/**
 * <p>Title:再保账单数据 新财务接口V3 提数、归档、生成凭证、凭证汇总、凭证导出、旧接口数据同步 批处理服务类</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class LRAccountDataExtractTask 
{
	public CErrors mErrors = new CErrors();
	private GlobalInput globalInput = new GlobalInput();
	private TransferData mGetCessData = new TransferData();
	private VData mOutputData = new VData();
	
	//管理机构
	private String tMangecom = "";
	//处理批次
	private String tBatchno = "";
	//提数日期
	private Date tDate = null;
	//规则类型
	
	private FIDataExtractDefSet tFIDataExtractDefSet = new FIDataExtractDefSet();
	
	public LRAccountDataExtractTask()
	{}
	
	public void run()
    {       
		dealData();
    }
	
	public boolean dealData()
	{
		System.out.println("再保预估数据提取开始:"+PubFun.getCurrentTime());
		//若财务接口正常提数批处理尚未执行完毕，则不执行 再保账单数据提取
		if(!IsComplete()){
			return false;
		}
		
		if(!IsCompletet()){
			return false;
		}
		//先准备数据
		getInputData();
		
		//数据提取和归档
		dataExtract();
		
		//生成凭证
		VoucherDataTrans();
		
		//凭证汇总
		VoucherDataGather();
		
		//凭证导出
		MoveDataToInterfacetable();
		
		//旧接口数据同步
		MoveDataToOldTable();
		
		System.out.println("再保预估数据导出完毕::"+PubFun.getCurrentTime());
		
		return true;
	}
	
	/**
	 * 判断财务接口正常批处理是否完成，若完成 则进行再保账单的提取，若没有完成 则不进行提取 
	 */
	public boolean IsComplete(){		
			
			SSRS tSSRS = new ExeSQL().execSQL("select 1 from FIOperationLog where 1=1 and makedate = current date and PerformState = '1' and operator = 'cwjk' and eventtype in ('00','10','20','30')");
			if(tSSRS!=null && tSSRS.MaxRow>0){
				System.out.println("财务接口正常提数批处理未执行完毕，不能执行再保账单提取:"+PubFun.getCurrentTime());
				return false;
			}					
			return true;
	}

	
	public boolean IsCompletet(){			
		//获取操作时间
		String Time = null;
		Time = PubFun.getCurrentTime();
		String tTime = Time.substring(0, 2);
		int n = Integer.parseInt(tTime);
		System.out.println("n:"+n);
		if(n>=0&&n<=3){
			System.out.println("每天晚0点-3点间不能进行此操作，谢谢:"+PubFun.getCurrentTime());
			return false;
		}
		
		return true;
}
	
	public void getInputData()
	{
		//设置管理机构
		tMangecom = "86";
		
		globalInput.ManageCom = tMangecom;
		globalInput.Operator = "cwLR";
		globalInput.ComCode = "86";
		
		//设置提数日期
		FDate chgdate = new FDate();
		tDate = chgdate.getDate(PubFun.getCurrentDate());
		
		//设置数据抽取规则
		FIDataExtractDefDB tFIDataExtractDefDB = new FIDataExtractDefDB();
		tFIDataExtractDefSet = tFIDataExtractDefDB.executeQuery(" select * from FIDataExtractDef a where ruletype = '00' and indexcode <> '00' and rulestate = '1' and ruledefid in('0000000011','0000000010') with ur ");
		
		System.out.println("提数机构为："+tMangecom+",当前取数日期为："+tDate);
		
		mGetCessData.setNameAndValue("StartDate",chgdate.getString(tDate));
		mGetCessData.setNameAndValue("EndDate",chgdate.getString(tDate));
		mGetCessData.setNameAndValue("ManageCom",tMangecom);
		mGetCessData.setNameAndValue("CallPointID","00"); //校检节点
		mGetCessData.setNameAndValue("FIExtType","00");
		
		mOutputData.add(globalInput);
		mOutputData.add(mGetCessData);
		mOutputData.add(tFIDataExtractDefSet);
		
	}
	
	public void dataExtract()
	{
		FIDataExtPlanExeLRUI tFIDataExtPlanExeLRUI = new FIDataExtPlanExeLRUI();		
		try
		{
			if(!tFIDataExtPlanExeLRUI.submitData(mOutputData,"00"))
			{
				System.out.println("操作失败，原因是:" + tFIDataExtPlanExeLRUI.mErrors.getFirstError());
			}
		}
		catch(Exception ex)
		{
			System.out.println("失败，原因是:" + ex.toString());
		}
		System.out.println("数据提取成功！");
	}
	
	public void VoucherDataTrans()
	{
		VData tVData  = new VData();
		String DataType = "1";
		
		tVData.add(globalInput);
		tVData.add(DataType);
		
		FIDistillCertificateUI tFIDistillCertificateUI = new FIDistillCertificateUI();
		try
		{
			if(!tFIDistillCertificateUI.submitData(tVData,""))
			{
				 System.out.println("凭证转换出错！");
			}
//			tBatchno = (String)tFIDistillCertificateUI.getResult().get(0);
			tBatchno = tFIDistillCertificateUI.getBatchno();
			System.out.println("FinDataExtractTask类tBatchno--"+tBatchno);
		}
		catch(Exception ex)
		{
			System.out.println("执行异常，原因是:" + ex.toString());
		}
		
		
	}
	
	/**
	 * 凭证汇总，由FiVoucherDataDetail表汇总至FIVoucherDataGather表
	 *
	 */
	public void VoucherDataGather(){
		VData tVData = new VData();
		
	    TransferData tTransferData= new TransferData();
	    tTransferData.setNameAndValue("BatchNo",tBatchno);
		
		tVData.add(globalInput);
		tVData.add(tTransferData);
		
		FIVoucherDataGatherUI tFIVoucherDataGatherUI = new FIVoucherDataGatherUI();
		
		try{
			
			if(!tFIVoucherDataGatherUI.submitData(tVData, "")){
				System.out.println("凭证汇总出错！");
			}
			
		}catch (Exception ex) {
			System.out.println("执行异常，原因是:" + ex.toString());
		}
		System.out.println("凭证汇总成功");
	}
	
	/**
	 * 凭证导出，由FIVoucherDataGather表导出至Interfacetable表
	 *
	 */
	public void MoveDataToInterfacetable(){
		VData tVData = new VData();
		
		tVData.add(globalInput);
		tVData.add(tBatchno);
		
		FIMoveToInterfacetableUI tFIMoveToInterfacetableUI = new FIMoveToInterfacetableUI();
		
		try{
			
			if(!tFIMoveToInterfacetableUI.submitData(tVData, "")){
				System.out.println("凭证导出出错！");
			}
			
		}catch (Exception ex) {
			System.out.println("执行异常，原因是:" + ex.toString());
		}
		System.out.println("凭证导出成功");
	}
	
	/**
	 * 向旧接口中同步数据，由FIVoucherDataDetail至LIDataTransResult，由FIAbstandardData至LIAboriginalData
	 *
	 */
	public void MoveDataToOldTable(){
		VData tVData = new VData();
		
		tVData.add(globalInput);
		tVData.add(tBatchno);
		
		FIMoveToOldTableUI tFIMoveToOldTable = new FIMoveToOldTableUI();
		
		try{
			
			if(!tFIMoveToOldTable.submitData(tVData, "")){
				System.out.println("旧接口数据同步失败");
			}
			
		}catch (Exception ex) {
			System.out.println("执行异常，原因是："+ex.toString());
		}
		System.out.println("旧接口数据同步成功");
	}
	
	/**
	 * 根据字符型日期，格式如"2014-02-22",返回该日期所在月份的月初与月末日期
	 * @param tDate
	 * @return
	 */
	public String[] calFLDate(String tDate)
    {
        String MonDate[] = new String[2];
        FDate fDate = new FDate();
        Date CurDate = fDate.getDate(tDate);
        GregorianCalendar mCalendar = new GregorianCalendar();
        mCalendar.setTime(CurDate);
        int Years = mCalendar.get(Calendar.YEAR);
        int Months = mCalendar.get(Calendar.MONTH);
        int FirstDay = mCalendar.getActualMinimum(Calendar.DAY_OF_MONTH);
        int LastDay = mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        mCalendar.set(Years, Months, FirstDay);
        MonDate[0] = fDate.getString(mCalendar.getTime());
        mCalendar.set(Years, Months, LastDay);
        MonDate[1] = fDate.getString(mCalendar.getTime());
        return MonDate;
    }
	
	public static void main(String args[])
	{
		LRAccountDataExtractTask tFinDataExtract_V3Task = new LRAccountDataExtractTask();
		tFinDataExtract_V3Task.run();
	}

}
