package com.sinosoft.lis.fininterface_v3.tools.sequence;

/**
 * <p>Title: 处理财务接口生成数据的流水号</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
import java.sql.Connection;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;

public class FinCreateSerialNo {

    /**
     * 获取事件号码
     * @return
     */
    public static synchronized String getEventNo()
    {
    	return PubFun1.CreateMaxNo("FIEventNo", 20) ;
    }

    /**
     * 凭证批次号
     * @return
     */
    public static synchronized String getBatchNo()
    {
    	return PubFun1.CreateMaxNo("FinBatch", 20) ;
    }
    
    /**
     * 获取主业务数据流水号
     * @return
     */
    public static synchronized String getMSerialNo()
    {
    	return PubFun1.CreateMaxNo("FIMSerialNo", 25) ;
    }
    
    /**
     * 获取子业务数据流水号
     * @return
     */
    public static synchronized String getASerialNo()
    {
    	return PubFun1.CreateMaxNo("FIASerialNo", 25) ;
    }
 
    /**
     * 明细凭证数据流水号
     * @return
     */
    public static synchronized String getFSerialNo()
    {
    	return "n"+PubFun1.CreateMaxNo("FIFSerialNo", 24) ;
    }

    /**
     * 汇总凭证数据流水号
     * @return
     */
    public static synchronized String getFGSerialNo()
    {
    	return PubFun1.CreateMaxNo("FININTERFACE", 20) ;
    }
    
    /**
     * 校检日志流水号
     * @return
     */
    public static synchronized String getLogSerialNo()
    {
    	return PubFun1.CreateMaxNo("FILogSerialNo", 20) ;
    }

    /**
     * 校检错误信息流水号
     * @return
     */
    public static synchronized String getErrLogSerialNo()
    {
    	return PubFun1.CreateMaxNo("FIErrSerialNo", 25) ;
    }
  
    /**
     * 红冲申请号码
     * @return
     */
    public static synchronized String getRBAppNo()
    {
    	return PubFun1.CreateMaxNo("FIRBAppNo", 20) ;
    }

    /**
     * 获取准业务数据流水号
     * @param size 增加数量
     * @return
     * @throws Exception
     */
    public static synchronized String[] getSerialNoByFINDATA(int size) throws
            Exception {

        int num = 0; //标记初始的数字
        String tSBql = "select maxno from ldmaxno where notype = 'FINDATA' and nolimit = 'SN' for update";
        Connection conn = DBConnPool.getConnection();
        try {
            conn.setAutoCommit(false);
            ExeSQL exeSQL = new ExeSQL(conn);
            String rsData = null;
            rsData = exeSQL.getOneValue(tSBql.toString());
            /***********************************************************************/
            /** 创建新的序列号 **/
            //如果没有创建的话则直接创建并且添加size个序号
            if ((rsData == null) || rsData.equals("")) 
            {
                tSBql =
                        "insert into ldmaxno(notype, nolimit, maxno) values('FINDATA','SN'," +
                        size + ")";
                exeSQL = new ExeSQL(conn);
                if (!exeSQL.execUpdateSQL(tSBql.toString())) {
                    System.out.println("CreateMaxNo 插入失败，请重试!");
                    conn.rollback();
                    conn.close();
                    Exception e = new Exception("CreateMaxNo 插入失败");
                    throw e;
                } else {
                    num = 1;
                }
            } else {
                //如果已经有了,只要直接添加就可以
                num = Integer.parseInt(rsData);
                tSBql = "update ldmaxno set maxno = maxno + " + size +
                        " where notype = 'FINDATA' and nolimit = 'SN'";
                exeSQL = new ExeSQL(conn);
                if (!exeSQL.execUpdateSQL(tSBql.toString())) {
                    System.out.println("CreateMaxNo 更新失败，请重试!");
                    conn.rollback();
                    conn.close();
                    Exception e = new Exception("CreateMaxNo 更新失败");
                    throw e;
                } else {
                    num = Integer.parseInt(rsData) + 1; //表示已经存在 并更新成功 则自动从下一个序列号开始增加
                }
            }
            conn.commit();
            conn.close();
        } catch (Exception ex) {
            try {
                conn.rollback();
                conn.close();
            } catch (Exception ex1) {
                System.out.println("流水号的错误!" + ex1.getMessage());
                throw ex1;
            }
            throw ex;
        }
        /****************************************************/
        String[] strArr = new String[size]; //返回数组的长度
        for (int i = 0; i < size; i++) {
            strArr[i] = getStringValue(num + i, 25);
        }
        System.out.println("流水号生成完毕!");
        return strArr;
    }

    /**
     * 获取准业务数据流水号
     * @param size 增加数量
     * @return
     * @throws Exception
     */
    public static synchronized String[] getSerialNoByFIFSerialNo(int size) throws
            Exception {

        int num = 0; //标记初始的数字
        String tSBql = "select maxno from ldmaxno where notype = 'FIFSerialNo' and nolimit = 'SN' for update";
        Connection conn = DBConnPool.getConnection();
        try {
            conn.setAutoCommit(false);
            ExeSQL exeSQL = new ExeSQL(conn);
            String rsData = null;
            rsData = exeSQL.getOneValue(tSBql.toString());
            /***********************************************************************/
            /** 创建新的序列号 **/
            //如果没有创建的话则直接创建并且添加size个序号
            if ((rsData == null) || rsData.equals("")) 
            {
                tSBql =
                        "insert into ldmaxno(notype, nolimit, maxno) values('FIFSerialNo','SN'," +
                        size + ")";
                exeSQL = new ExeSQL(conn);
                if (!exeSQL.execUpdateSQL(tSBql.toString())) {
                    System.out.println("CreateMaxNo 插入失败，请重试!");
                    conn.rollback();
                    conn.close();
                    Exception e = new Exception("CreateMaxNo 插入失败");
                    throw e;
                } else {
                    num = 1;
                }
            } else {
                //如果已经有了,只要直接添加就可以
                num = Integer.parseInt(rsData);
                tSBql = "update ldmaxno set maxno = maxno + " + size +
                        " where notype = 'FIFSerialNo' and nolimit = 'SN'";
                exeSQL = new ExeSQL(conn);
                if (!exeSQL.execUpdateSQL(tSBql.toString())) {
                    System.out.println("CreateMaxNo 更新失败，请重试!");
                    conn.rollback();
                    conn.close();
                    Exception e = new Exception("CreateMaxNo 更新失败");
                    throw e;
                } else {
                    num = Integer.parseInt(rsData) + 1; //表示已经存在 并更新成功 则自动从下一个序列号开始增加
                }
            }
            conn.commit();
            conn.close();
        } catch (Exception ex) {
            try {
                conn.rollback();
                conn.close();
            } catch (Exception ex1) {
                System.out.println("流水号的错误!" + ex1.getMessage());
                throw ex1;
            }
            throw ex;
        }
        /****************************************************/
        String[] strArr = new String[size]; //返回数组的长度
        for (int i = 0; i < size; i++) {
            strArr[i] = getStringValue(num + i, 25);
        }
        System.out.println("流水号生成完毕!");
        return strArr;
    }
    
    /**
     * 获取准业务数据流水号
     * @param size 增加数量
     * @return
     * @throws Exception
     */
    public static synchronized String[] getSerialNoByFIASerialNo(int size) throws
            Exception {

        int num = 0; //标记初始的数字
        String tSBql = "select maxno from ldmaxno where notype = 'FIASerialNo' and nolimit = 'SN' for update";
        Connection conn = DBConnPool.getConnection();
        try {
            conn.setAutoCommit(false);
            ExeSQL exeSQL = new ExeSQL(conn);
            String rsData = null;
            rsData = exeSQL.getOneValue(tSBql.toString());
            /***********************************************************************/
            /** 创建新的序列号 **/
            //如果没有创建的话则直接创建并且添加size个序号
            if ((rsData == null) || rsData.equals("")) 
            {
                tSBql =
                        "insert into ldmaxno(notype, nolimit, maxno) values('FIASerialNo','SN'," +
                        size + ")";
                exeSQL = new ExeSQL(conn);
                if (!exeSQL.execUpdateSQL(tSBql.toString())) {
                    System.out.println("CreateMaxNo 插入失败，请重试!");
                    conn.rollback();
                    conn.close();
                    Exception e = new Exception("CreateMaxNo 插入失败");
                    throw e;
                } else {
                    num = 1;
                }
            } else {
                //如果已经有了,只要直接添加就可以
                num = Integer.parseInt(rsData);
                tSBql = "update ldmaxno set maxno = maxno + " + size +
                        " where notype = 'FIASerialNo' and nolimit = 'SN'";
                exeSQL = new ExeSQL(conn);
                if (!exeSQL.execUpdateSQL(tSBql.toString())) {
                    System.out.println("CreateMaxNo 更新失败，请重试!");
                    conn.rollback();
                    conn.close();
                    Exception e = new Exception("CreateMaxNo 更新失败");
                    throw e;
                } else {
                    num = Integer.parseInt(rsData) + 1; //表示已经存在 并更新成功 则自动从下一个序列号开始增加
                }
            }
            conn.commit();
            conn.close();
        } catch (Exception ex) {
            try {
                conn.rollback();
                conn.close();
            } catch (Exception ex1) {
                System.out.println("流水号的错误!" + ex1.getMessage());
                throw ex1;
            }
            throw ex;
        }
        /****************************************************/
        String[] strArr = new String[size]; //返回数组的长度
        for (int i = 0; i < size; i++) {
            strArr[i] = getStringValue(num + i, 25);
        }
        System.out.println("流水号生成完毕!");
        return strArr;
    }
    
    /**
     * 获取准业务数据流水号
     * @param size 增加数量
     * @return
     * @throws Exception
     */
    public static synchronized String[] getSerialNoByFIMSerialNo(int size) throws
            Exception {

        int num = 0; //标记初始的数字
        String tSBql = "select maxno from ldmaxno where notype = 'FIMSerialNo' and nolimit = 'SN' for update";
        Connection conn = DBConnPool.getConnection();
        try {
            conn.setAutoCommit(false);
            ExeSQL exeSQL = new ExeSQL(conn);
            String rsData = null;
            rsData = exeSQL.getOneValue(tSBql.toString());
            /***********************************************************************/
            /** 创建新的序列号 **/
            //如果没有创建的话则直接创建并且添加size个序号
            if ((rsData == null) || rsData.equals("")) 
            {
                tSBql =
                        "insert into ldmaxno(notype, nolimit, maxno) values('FIMSerialNo','SN'," +
                        size + ")";
                exeSQL = new ExeSQL(conn);
                if (!exeSQL.execUpdateSQL(tSBql.toString())) {
                    System.out.println("CreateMaxNo 插入失败，请重试!");
                    conn.rollback();
                    conn.close();
                    Exception e = new Exception("CreateMaxNo 插入失败");
                    throw e;
                } else {
                    num = 1;
                }
            } else {
                //如果已经有了,只要直接添加就可以
                num = Integer.parseInt(rsData);
                tSBql = "update ldmaxno set maxno = maxno + " + size +
                        " where notype = 'FIMSerialNo' and nolimit = 'SN'";
                exeSQL = new ExeSQL(conn);
                if (!exeSQL.execUpdateSQL(tSBql.toString())) {
                    System.out.println("CreateMaxNo 更新失败，请重试!");
                    conn.rollback();
                    conn.close();
                    Exception e = new Exception("CreateMaxNo 更新失败");
                    throw e;
                } else {
                    num = Integer.parseInt(rsData) + 1; //表示已经存在 并更新成功 则自动从下一个序列号开始增加
                }
            }
            conn.commit();
            conn.close();
        } catch (Exception ex) {
            try {
                conn.rollback();
                conn.close();
            } catch (Exception ex1) {
                System.out.println("流水号的错误!" + ex1.getMessage());
                throw ex1;
            }
            throw ex;
        }
        /****************************************************/
        String[] strArr = new String[size]; //返回数组的长度
        for (int i = 0; i < size; i++) {
            strArr[i] = getStringValue(num + i, 25);
        }
        System.out.println("流水号生成完毕!");
        return strArr;
    }
    
    /**
     * 把相关的数字转换成相应长度的流水号
     * @param n 需要转换的数字,size则是转换后的长度
     * @return String
     */
    public static String getStringValue(int n, int size) {

        String str = Integer.toString(n);
        int len = str.length();
        String temp = "0000000000000000000000000000";
        str = temp.substring(0, size - len) + str; //未满的用0补充

        return str;
    }   
    /**
     * @param args
     */
    public static void main(String[] args) {

    }


}
