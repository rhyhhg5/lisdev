package com.sinosoft.lis.fininterface_v3.tools.sequence;


import java.sql.Connection;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;


public class PExpirBenefitPayNo {
	   /**
     * 获取满期金批次号
     * @return
     */
    public static synchronized String getMBatchNo()
    {
    	return PubFun1.CreateMaxNo("MQNO", 10) ;
    }

}
