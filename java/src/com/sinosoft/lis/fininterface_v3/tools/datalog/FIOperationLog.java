package com.sinosoft.lis.fininterface_v3.tools.datalog;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.FIOperationLogSchema;
import com.sinosoft.lis.schema.FIRuleDealErrLogSchema;
import com.sinosoft.lis.schema.FIRuleDealLogSchema;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import java.io.File;
/**
 * <p>
 * ClassName:
 * </p>
 * <p>
 * Description: 
 * </p>
 * <p>
 * Copyright: Copyright (c) 2011
 * </p>
 * <p>
 * Company: sinosoft
 * </p>
 * @author: caosg
 * @version: 3.0
 */

public class FIOperationLog
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    
    private VData mInputData ;
    private String LogFileName = "";
    private String LogFilePath = "";

    private FIOperationLogSchema mFIOperationLogSchema = new FIOperationLogSchema();
    private FIRuleDealLogSchema mFIRuleDealLogSchema = new FIRuleDealLogSchema();
    
    public FIOperationLog( String tOperator, String tEventType)  throws Exception
    {
        if(! this.InitInfo(tOperator,tEventType))
        {
            Exception tErr = new Exception(this.mErrors.getFirstError());
            throw tErr;
        }

    }
    /**
     * 初始化日志操作记录
     * @param tOperator
     * @param tEventType 00-数据抽取 10-数据归档 20-二次处理 30-凭证转换 40-凭证汇总 50-凭证审核 60-凭证导出
     * @return
     */
    private boolean InitInfo(String tOperator, String tEventType)
    {

        try
        {
            if (tOperator == null || tOperator.equals(""))
            {
                buildError("FIOperationLog", "initInfo", "日志处理类初始化失败，传入操作员参数为空");
                return false;
            }
            if (tEventType == null || tEventType.equals(""))
            {
                buildError("FIOperationLog", "initInfo", "日志处理类初始化失败，传入事件类型参数为空");
                return false;
            }



            String sql = "select codealias from ficodetrans where codetype = 'LogPath' and code = 'event' ";
            ExeSQL tExeSQL = new ExeSQL();
            
            LogFilePath = tExeSQL.getOneValue(sql);
            
            if (LogFilePath == null || LogFilePath.equals(""))
            {
                buildError("FIOperationLog", "initInfo", "日志处理类初始化失败，查询日志文件路径参数为空");
                return false;
            }
            
            LogFilePath = LogFilePath.toString();
            File myfolderPath = new File(LogFilePath);
            try
            {
                if (!myfolderPath.isDirectory())
                {
                    myfolderPath.mkdir();
                }
            }
            catch (Exception e)
            {
                buildError("FIOperationLog", "initInfo", "日志处理类初始化失败，创建目录"+ myfolderPath + "失败" );
            }

            String EventNo = PubFun1.CreateMaxNo("EventNo", 20);
            LogFileName = EventNo + "[" + tEventType + "]" + ".txt";
            
            String currentDate = PubFun.getCurrentDate();         
            String currentTime = PubFun.getCurrentTime();
            
            this.mFIOperationLogSchema.setEventNo(EventNo);
            this.mFIOperationLogSchema.setEventType(tEventType);
            this.mFIOperationLogSchema.setLogFileName(LogFileName);
            this.mFIOperationLogSchema.setLogFilePath(LogFilePath);
            this.mFIOperationLogSchema.setPerformState("1");//开始执行
            this.mFIOperationLogSchema.setOperator(tOperator);
            this.mFIOperationLogSchema.setMakeDate(currentDate);
            this.mFIOperationLogSchema.setMakeTime(currentTime);
            this.mFIOperationLogSchema.setModifyDate(currentDate);
            this.mFIOperationLogSchema.setModifyTime(currentTime);
            
            //执行错误记录
        	mFIRuleDealLogSchema = new FIRuleDealLogSchema();
        	mFIRuleDealLogSchema.setVersionNo("0000000000");
        	mFIRuleDealLogSchema.setCheckBatchNo(FinCreateSerialNo.getLogSerialNo());
        	mFIRuleDealLogSchema.setEventNo(EventNo);
        	mFIRuleDealLogSchema.setCheckType("03"); //执行规则
        	mFIRuleDealLogSchema.setRuleID("");
        	mFIRuleDealLogSchema.setCallPointID(tEventType);
        	mFIRuleDealLogSchema.setOperator(tOperator);
        	mFIRuleDealLogSchema.setRulePlanID("");
        	mFIRuleDealLogSchema.setMakeDate(PubFun.getCurrentDate());
        	mFIRuleDealLogSchema.setMakeTime(PubFun.getCurrentTime());
        	mFIRuleDealLogSchema.setLogFilePath("");
        	mFIRuleDealLogSchema.setLogFileName("");
        	mFIRuleDealLogSchema.setDataSource("1");        	
        	mFIRuleDealLogSchema.setRuleResult("Succ");
            
            MMap map = new MMap();
            map.put(mFIOperationLogSchema, "INSERT");
            map.put(mFIRuleDealLogSchema, "INSERT");
            
            
            this.mInputData = new VData();
            this.mInputData.add(map);

            String cOperate = "INSERT||MAIN";
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, cOperate))
            {
                buildError("FIOperationLog", "initInfo", "日志处理类初始化失败,日志提交数据库失败" + tPubSubmit.mErrors.getFirstError());
                return false;
            }
            return true;

        }
        catch (Exception ex)
        {
            buildError("FIOperationLog", "initInfo", "日志处理类初始化异常,异常信息为" + ex.getMessage());
            return false;
        }
    }


    public boolean WriteLogTxt(String txt)
    {
        try
        {
            String currentDate = PubFun.getCurrentDate();
            String currentTime = PubFun.getCurrentTime();
            String AllTxt = "[日期：" + currentDate + "][时间：" + currentTime + "]" + "日志内容：" + txt;
            String LogFile = LogFilePath + LogFileName;
            GenTxtFile gTxtFile = new GenTxtFile(LogFile, AllTxt);
            gTxtFile.writeTxt();
        }
        catch (Exception e)
        {
            buildError("FIOperationLog", "WriteLogTxt", "信息写入日志文件异常，异常信息为：" + e.getMessage());
            return  false;
        }
        return true;
    }
    
    public void buildErrorLog(String mess)
    {
		FIRuleDealErrLogSchema tFIRuleDealErrLogSchema = new FIRuleDealErrLogSchema();
		tFIRuleDealErrLogSchema.setErrSerialNo(FinCreateSerialNo.getErrLogSerialNo());
		tFIRuleDealErrLogSchema.setAserialno("0000000000");
		tFIRuleDealErrLogSchema.setIndexCode("00");//业务号码类型
		tFIRuleDealErrLogSchema.setBusinessNo("0000000000");//业务号码
		tFIRuleDealErrLogSchema.setCheckBatchNo(mFIRuleDealLogSchema.getCheckBatchNo());
		tFIRuleDealErrLogSchema.setRuleID("");
		tFIRuleDealErrLogSchema.setErrInfo(mess);//错误信息
		tFIRuleDealErrLogSchema.setRulePlanID("");
		tFIRuleDealErrLogSchema.setDealState("0");
		tFIRuleDealErrLogSchema.setCertificateID("");
		tFIRuleDealErrLogSchema.setCallPointID(mFIRuleDealLogSchema.getCallPointID());
		
    	MMap map = new MMap();
    	map.put(tFIRuleDealErrLogSchema, "INSERT");
    	VData tInputData = new VData();
    	tInputData.add(map);

        String cOperate = "INSERT||MAIN";
        
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tInputData, cOperate))
        {
            buildError("FIOperationLog", "initInfo", "日志处理类初始化失败,日志提交数据库失败" + tPubSubmit.mErrors.getFirstError());
        }
    }
    
    public void buildErrorLog(String tAserialno,String tIndexCode,String tBusinessNo,String mess)
    {
		FIRuleDealErrLogSchema tFIRuleDealErrLogSchema = new FIRuleDealErrLogSchema();
		tFIRuleDealErrLogSchema.setErrSerialNo(FinCreateSerialNo.getErrLogSerialNo());
		tFIRuleDealErrLogSchema.setAserialno(tAserialno);
		tFIRuleDealErrLogSchema.setIndexCode(tIndexCode);//业务号码类型
		tFIRuleDealErrLogSchema.setBusinessNo(tBusinessNo);//业务号码
		tFIRuleDealErrLogSchema.setCheckBatchNo(mFIRuleDealLogSchema.getCheckBatchNo());
		tFIRuleDealErrLogSchema.setRuleID("");
		tFIRuleDealErrLogSchema.setErrInfo(mess);//错误信息
		tFIRuleDealErrLogSchema.setRulePlanID("");
		tFIRuleDealErrLogSchema.setDealState("0");
		tFIRuleDealErrLogSchema.setCertificateID("");
		tFIRuleDealErrLogSchema.setCallPointID(mFIRuleDealLogSchema.getCallPointID());
		
    	MMap map = new MMap();
    	map.put(tFIRuleDealErrLogSchema, "INSERT");
    	VData tInputData = new VData();
    	tInputData.add(map);

        String cOperate = "INSERT||MAIN";
        
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tInputData, cOperate))
        {
            buildError("FIOperationLog", "initInfo", "日志处理类初始化失败,日志提交数据库失败" + tPubSubmit.mErrors.getFirstError());
        }
    }
    public boolean WriteLogTxt(String tLogFilePath,String tLogFileName,String txt)
    {
        try
        {
            String currentDate = PubFun.getCurrentDate();
            String currentTime = PubFun.getCurrentTime();
            String AllTxt = "[日期：" + currentDate + "][时间：" + currentTime + "]" + "内容：" + txt;
            String LogFile = tLogFilePath + tLogFileName;
            GenTxtFile gTxtFile = new GenTxtFile(LogFile, AllTxt);
            gTxtFile.writeTxt();
        }
        catch (Exception e)
        {
            buildError("FIOperationLog", "WriteLogTxt", "信息写入日志文件异常，异常信息为：" + e.getMessage());
            return  false;
        }
        return true;
    }
    
    /**
     * 执行状态
     * @param FinishMark
     * @return
     */
    public boolean Complete(boolean FinishMark)
    {
        try
        {
            String Flag = "0"; //成功结束
            if(FinishMark == false)
            {
               Flag = "1" ;//存在错误
            }
            
            String currentDate = PubFun.getCurrentDate();         
            String currentTime = PubFun.getCurrentTime();
            
            this.mFIOperationLogSchema.setPerformState("0");//执行结束
            this.mFIOperationLogSchema.setOthernoMark(Flag);
            
            this.mFIOperationLogSchema.setModifyDate(currentDate);
            this.mFIOperationLogSchema.setModifyTime(currentTime);
            
            MMap map = new MMap();
            map.put(mFIOperationLogSchema, "UPDATE");

            this.mInputData = new VData();
            this.mInputData.add(map);

            String cOperate = "UPDATE||MAIN";
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, cOperate))
            {
                buildError("FIOperationLog", "Complete", "日志结束处理出错，执行状态提交数据库失败" + tPubSubmit.mErrors.getFirstError());
                return false;
            }
            return true;
        }
        catch (Exception ex)
        {
            buildError("FIOperationLog", "Complete", "日志结束处理异常，异常信息为：" + ex.getMessage());
            return false;
        }

    }

    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    
    public String getEventNo()
    {

    	return mFIOperationLogSchema.getEventNo();
    }
    
    public static void main(String[] args) {

        try
        {
        	FIOperationLog t = new FIOperationLog("001","00");
            t.WriteLogTxt("测试一下");

            t.WriteLogTxt("D:\\","1.txt","测试一下");
            t.Complete(true);
            t.Complete(false);

        } catch (Exception ex)
        {
            System.out.print("11" + ex.getMessage());
        }

    }
	public FIOperationLogSchema getMFIOperationLogSchema() {
		return mFIOperationLogSchema;
	}
	public FIRuleDealLogSchema getMFIRuleDealLogSchema() {
		return mFIRuleDealLogSchema;
	}

}
