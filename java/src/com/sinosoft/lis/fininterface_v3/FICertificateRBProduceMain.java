package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.db.FIAbStandardDataDB;
import com.sinosoft.lis.db.FIDataFeeBackAppDB;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.schema.FIDataFeeBackAppSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.lis.vschema.FIDataFeeBackAppSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2008</p>
 *
 * <p>Company: </p>
 *
 * @author jw
 * @version 1.0
 */
public class FICertificateRBProduceMain 
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = new GlobalInput();

    private MMap map = new MMap();

    public String Content = "";
    private String AppNo = "";  
    private String BusinessNo = "";
    private String DetailIndexID = "";
    private String tReasonType="";
    private FIDataFeeBackAppSchema mFIDataFeeBackAppSchema ;
    private final String enter = "\r\n"; // 换行符
    public FIOperationLog tLogInfoDeal ;

    public FICertificateRBProduceMain()
    {
    }

    public boolean dealProcess(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }
        
        if (!DealWithData()) 
        {
            return false;
        }
        
        if(!PubSubmit())
        {
        	return false;
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        AppNo = (String) cInputData.get(1);
        BusinessNo = (String) cInputData.get(3);
        DetailIndexID = (String) cInputData.get(4);
        tReasonType = (String) cInputData.get(5);
        if (mGlobalInput == null)
        {
            buildError("FICertificateRBProduceMain", "getInputData", "传入登陆信息参数为空");
            return false;
        }
        if (AppNo == null || AppNo.equals(""))
        {
            buildError("FICertificateRBProduceMain", "getInputData", "红冲申请号码为空");
            return false;
        }
        if (BusinessNo == null || BusinessNo.equals(""))
        {
            buildError("FICertificateRBProduceMain", "getInputData", "红冲业务号码为空");
            return false;
        }
        if (DetailIndexID == null || DetailIndexID.equals(""))
        {
            buildError("FICertificateRBProduceMain", "getInputData", "红冲业务索引号码为空");
            return false;
        }
        FIDataFeeBackAppSet tFIDataFeeBackAppSet = new FIDataFeeBackAppDB().executeQuery("select * from FIDataFeeBackApp where AppNo = '"+AppNo+"'");
        if(tFIDataFeeBackAppSet==null||tFIDataFeeBackAppSet.size()<1)
        {
        	buildError("FICertificateRBProduceMain", "getInputData", "获取红冲申请信息失败！");
        	return false;
        }
        
        mFIDataFeeBackAppSchema = tFIDataFeeBackAppSet.get(1);
        return true;
    }

    private boolean DealWithData()
    {
    	FIAbStandardDataSet oFIAbStandardDataSet = new FIAbStandardDataSet();
    	FIAbStandardDataDB tFIAbStandardDataDB = new FIAbStandardDataDB();
    	FIAbStandardDataSet tFIAbStandardDataSet = tFIAbStandardDataDB.executeQuery("select * from FIAbStandardData where indexcode='"+DetailIndexID+"' and indexno='"+BusinessNo+"' and stringinfo01 is null");
    	if(tFIAbStandardDataSet==null || tFIAbStandardDataSet.size()<1)
    	{
    		buildError("FICertificateRBProduceMain", "dealData", "tFIAbStandardDataSet为空!");
            return false;
    	}
    	try {
    		
			String ttSerialno[] = FinCreateSerialNo.getSerialNoByFINDATA(tFIAbStandardDataSet.size());
			for(int i=1; i<=tFIAbStandardDataSet.size(); i++)
	    	{
				FIAbStandardDataSchema tFIAbStandardDataSchema = tFIAbStandardDataSet.get(i);
				//生成FIAbStandardData表红冲数据
            	FIAbStandardDataSchema ttFIAbStandardDataSchema = (FIAbStandardDataSchema)tFIAbStandardDataSchema.clone();
            	ttFIAbStandardDataSchema.setSerialNo(ttSerialno[i-1]);
            	ttFIAbStandardDataSchema.setState("00");
            	oFIAbStandardDataSet.add(ttFIAbStandardDataSchema);
	    	}
	    	
			map.put(oFIAbStandardDataSet, "INSERT");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	try
        {
        	
        	map.put( "update FIVoucheManage a set a.State = '30' where exists(select 1 from FIVoucherDataDetail b,FIAbStandardData c where a.batchno=b.batchno and  c.SerialNo = b.SerialNo and c.StringInfo01 = '"+AppNo+"')","UPDATE");
        	map.put( "update FIAbStandardData set CheckFlag = '00' where StringInfo01 = '"+AppNo+"' ","UPDATE");
        	map.put( "update FIVoucherDataDetail a set CheckFlag = '00' where exists(select 1 from FIAbStandardData b where a.SerialNo = b.SerialNo and b.StringInfo01 = '"+AppNo+"' )","UPDATE");
            
        	map.put( "update FIDataFeeBackApp set AppState = '03',ReasonType='"+tReasonType+"' where AppNo = '" + AppNo + "'", "UPDATE");
            map.put( "update FIErrHandingItem a set a.AppState = '99' where  a.ErrAppNo  = '" + mFIDataFeeBackAppSchema.getErrAppNo() + "' ", "UPDATE");
            map.put( "update FIRuleDealErrLog a set a.DealState = '1' where  a.ErrSerialNo ='" + mFIDataFeeBackAppSchema.getErrAppNo()  + "' ", "UPDATE");
            
            return true;
        }
        catch (Exception ex)
        {
            buildError("FICertificateRBProduceMain","DealWithData", "错误信息为：" + ex.getMessage());
            ex.printStackTrace();
            return false;
        }
    }

    private boolean PubSubmit()
    {
        VData tInputData = new VData();
        tInputData.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tInputData, ""))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buildError("FICertificateRBProduceMain","PubSubmit", "错误信息为：" + tPubSubmit.mErrors.getFirstError());
            return false;
        }
    	return true;
    }
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }

    public static void main(String[] args)
    {
        VData vData = new VData();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";
        String Batchno = "00000000000000000104";
        vData.addElement(tG);
        vData.addElement(Batchno);
        FIDistillRollBack tFIDistillRollBack = new FIDistillRollBack();
        tFIDistillRollBack.dealProcess(vData);
        tFIDistillRollBack.mErrors.getFirstError();
    }

}
