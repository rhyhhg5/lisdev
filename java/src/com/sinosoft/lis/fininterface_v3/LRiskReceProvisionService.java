package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.FIAbStandardDataDB;
import com.sinosoft.lis.db.InterfaceTableDB;
import com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl.SecondaryRule_J01;
import com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl.SecondaryRule_J02;
import com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl.StandSecondaryRule;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.schema.FIDataBaseLinkSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.lis.vschema.InterfaceTableSet;
import com.sinosoft.lis.vschema.LIAboriginalDataSet;

public class LRiskReceProvisionService extends StandSecondaryRule
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    SSRS tSSRS = new SSRS();
	//处理批次
	private String tBatchno = "";
    private String mCurDate = PubFun.getCurrentDate();
    	
	private String[][] mToExcel = null;
	      

    public LRiskReceProvisionService()
    {
    }

    public boolean submitData(VData cInputData, String cOperate) 
    {

    	System.out.println("Start LRiskReceProvisionService Submit ...");
    	System.out.println("开始执行财务接口批处理:"+PubFun.getCurrentTime());
    	
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }            
        //生成凭证
  		VoucherDataTrans();
  		
  	   //凭证汇总
  		VoucherDataGather();
       // 凭证导出
       MoveDataToInterfacetable();
  		//旧接口数据同步       
       MoveDataToOldTable();
  		
  		System.out.println("财务接口批处理执行完毕:"+PubFun.getCurrentTime());
  		
        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
   
	
	

	public String[][] getMToExcel() {
		return mToExcel;
	}
	 
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     * @throws java.text.ParseException 
     */
    public boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
        System.out.println(mGI.ManageCom);
        return true;
    }
    
    private void buiError(String functionName,String errorMessage){
    	CError tError = new CError();
        tError.moduleName = "LRiskReceProvisionBL";
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        mErrors.addOneError(tError);
    }
    
	public void VoucherDataTrans()
	{		
		VData tVData  = new VData();
		String DataType = "1";
		
		tVData.add(mGI);
		tVData.add(DataType);
		
		FIDistillCertificateUI tFIDistillCertificateUI = new FIDistillCertificateUI();
		try
		{
			if(!tFIDistillCertificateUI.submitData(tVData,""))
			{
				 System.out.println("凭证转换出错！");
			}
			tBatchno = (String)tFIDistillCertificateUI.getResult().get(0);
		}
		catch(Exception ex)
		{
			System.out.println("执行异常，原因是:" + ex.toString());
		}
		
		
	}
//凭证汇总

	public void VoucherDataGather(){
		VData tVData = new VData();
		
	    TransferData tTransferData= new TransferData();
	    tTransferData.setNameAndValue("BatchNo",tBatchno);
		
		tVData.add(mGI);
		tVData.add(tTransferData);
		FIVoucherDataGatherUI tFIVoucherDataGatherUI = new FIVoucherDataGatherUI();
		
		try{
			
			if(!tFIVoucherDataGatherUI.submitData(tVData, "")){
				System.out.println("凭证汇总出错！");
			}
			
		}catch (Exception ex) {
			System.out.println("执行异常，原因是:" + ex.toString());
		}
		System.out.println("凭证汇总成功");
	}
	
	/**
	 * 凭证导出，由FIVoucherDataGather表导出至Interfacetable表
	 *
	 */
	public void MoveDataToInterfacetable(){
		VData tVData = new VData();
		
		tVData.add(mGI);
		tVData.add(tBatchno);
		
		FIMoveToInterfacetableUI tFIMoveToInterfacetableUI = new FIMoveToInterfacetableUI();
		
		try{
			
			if(!tFIMoveToInterfacetableUI.submitData(tVData, "")){
				System.out.println("凭证导出出错！");
			}
			
		}catch (Exception ex) {
			System.out.println("执行异常，原因是:" + ex.toString());
		}
		System.out.println("凭证导出成功");
	}
	
	/**
	 * 向旧接口中同步数据，由FIVoucherDataDetail至LIDataTransResult，由FIAbstandardData至LIAboriginalData
	 *
	 */
	public void MoveDataToOldTable(){
		VData tVData = new VData();
		
		tVData.add(mGI);
		tVData.add(tBatchno);
		
		FIMoveToOldTableUI tFIMoveToOldTable = new FIMoveToOldTableUI();
		
		try{
			
			if(!tFIMoveToOldTable.submitData(tVData, "")){
				System.out.println("旧接口数据同步失败");
			}
			
		}catch (Exception ex) {
			System.out.println("执行异常，原因是："+ex.toString());
		}
		System.out.println("旧接口数据同步成功");
	}
    
    public static void main(String[] args)
    {   	
    	
}
    }

