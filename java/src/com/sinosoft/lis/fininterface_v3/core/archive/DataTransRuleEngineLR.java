package com.sinosoft.lis.fininterface_v3.core.archive;
/**
 * <p>Title: 数据归档规则引擎</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class DataTransRuleEngineLR{

	public CErrors mErrors = new CErrors();	
	
	GlobalInput mGlobalInput = new GlobalInput();
	
	private TransferData mTransferData ;
    
	private FIDataExtractRulesSet mFIDataExtractRulesSet ;
	/** 日志处理*/
	private FIOperationLog tLogDeal = null;
	
	/** 数据量*/
	private int count = 0;
	
	private String mRuleType = "" ;
	private String mSubRuleType = "" ;
	/** 事件流水号*/
    private String EventNo = "";
    /** 数据抽取类型*/
    private String mFIExtType;    

    private String mIndexidNo ;   
    
    private final String enter = "\r\n"; // 换行符

    
    public DataTransRuleEngineLR(FIOperationLog logDeal)
    {
    	tLogDeal = logDeal;
    	EventNo = tLogDeal.getEventNo();
    }
    
    /**
     * 普通归档规则执行
     * 
     */
	public boolean ruleService(FIDataExtractRulesSet tFIDataExtractRulesSet,VData param) 
	{
    	mFIDataExtractRulesSet = tFIDataExtractRulesSet;
    	
    	if (!getInputData(param))
        {
			buildError("DataTransRuleEngine","ruleService", "获取参数出错！！");
            tLogDeal.WriteLogTxt("获取参数出错！！" + enter);
    		return false;
        }

    	if (!getCheckData())
        {
			buildError("DataTransRuleEngine","ruleService", "数据校检出错！！");
            tLogDeal.WriteLogTxt("数据校检出错！！" + enter);
    		return false;
        }
    	
    	//执行规则
        dealRuels();
		
		return true;
	}
	
	/**
	 * 归档业务交易及费用
	 * @return
	 */
	public boolean dealFilingBusiness()
	{
		
		//获取要转换的业务交易
		FIBnTypeDefSet tFIBnTypeDefSet = getFIBnType();
		
		if(tFIBnTypeDefSet!=null&&tFIBnTypeDefSet.size()>0)
		{
			for(int i=1;i<=tFIBnTypeDefSet.size();i++)
			{				
				FIBnTypeDefSchema tFIBnTypeDefSchema = tFIBnTypeDefSet.get(i);
				
//				System.out.println("****************getBusinessID*******"+tFIBnTypeDefSchema.getBusinessID());
				
				//处理业务类型判断条件
				String condition1 = getBnCondition(tFIBnTypeDefSchema);
				if(condition1==null)
				{
					break;
				}				
				
				
				//处理费用类型判断条件
				FIBnFeeTypeDefSet tFIBnFeeTypeDefSet = new FIBnFeeTypeDefDB().executeQuery("select * from FIBnFeeTypeDef where BusinessID = '"+tFIBnTypeDefSchema.getBusinessID()+"' order by costdetail with ur ");
				for(int m=1;m<=tFIBnFeeTypeDefSet.size();m++)
				{
					String condition2 ="" ;
					FIBnFeeTypeDefSchema tFIBnFeeTypeDefSchema = tFIBnFeeTypeDefSet.get(m);
					
//					System.out.println("-----------getCostID----"+tFIBnFeeTypeDefSchema.getCostID());
					
					condition2 = getFeeCondition(tFIBnFeeTypeDefSchema);
					
					//准业务数据准备执行
					String dillSQL = "";
										
					if("1".equals(tFIBnFeeTypeDefSchema.getCostType()))
					{
						//主表
						dillSQL = getMainFilingBusinessSql(tFIBnTypeDefSchema,tFIBnFeeTypeDefSchema);
					}
					else if("2".equals(tFIBnFeeTypeDefSchema.getCostType()))
					{
						//明细表
						dillSQL = getFilingBusinessSql(tFIBnTypeDefSchema,tFIBnFeeTypeDefSchema);
					}
					else
					{
						continue;
					}
					dillSQL = dillSQL+condition1+condition2+" with ur ";			
					System.out.println("Data表归档SQL----dillSQL:"+dillSQL);
					
					FIAbStandardDataSet tFIAbStandardDataSet = new FIAbStandardDataSet();
					RSWrapper rsWrapper = new RSWrapper();
					
					if (!rsWrapper.prepareData(tFIAbStandardDataSet, dillSQL))
			        {
			            mErrors.copyAllErrors(rsWrapper.mErrors);
			            System.out.println(rsWrapper.mErrors.getFirstError());
		                buildError("DataTransRuleEngine","dealFilingBusiness", "业务交易及费用归档出错，业务交易编码"+tFIBnTypeDefSchema.getBusinessID()+",费用编码："+tFIBnFeeTypeDefSchema.getCostID()+",提示信息为：" +rsWrapper.mErrors.getFirstError());
		                tLogDeal.WriteLogTxt("业务交易及费用归档出错，业务交易编码"+tFIBnTypeDefSchema.getBusinessID()+",费用编码："+tFIBnFeeTypeDefSchema.getCostID()+",提示信息为：" +rsWrapper.mErrors.getFirstError() + enter);
			            break;
			        }
		            do
		            {
			           	rsWrapper.getData();
		                if (tFIAbStandardDataSet != null && tFIAbStandardDataSet.size() > 0)
		                {
		                	count = count+tFIAbStandardDataSet.size();//累计数据量
		                	
		                	saveStandardData(tFIAbStandardDataSet);
		                }      
		                
		            }while(tFIAbStandardDataSet != null && tFIAbStandardDataSet.size() > 0); 	
					//关闭连接
					rsWrapper.close();		
				}
			}	

		}
		else
		{
			tLogDeal.WriteLogTxt("没有要准备凭证转换的数据！！" + enter);
		}
		return true;
	}
	
	/**
     * 获取参数
     * @param cInputData
     * @return
     */
    private boolean getInputData(VData cInputData)
    {
    	mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
    	mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
    	if(mTransferData==null)
    	{
			buildError("DataTransRuleEngine","getInputData", "mTransferData获取参数出错！！");
            tLogDeal.WriteLogTxt("获取参数出错！！" + enter);
    		return false;
    	}
    	if(mGlobalInput==null)
    	{
    		buildError("DataTransRuleEngine","getInputData ","mGlobalInput获取参数出错！！"); 
            tLogDeal.WriteLogTxt("获取参数出错！！" + enter);
    		return false;
    	}
        mIndexidNo = (String)mTransferData.getValueByName("IndexidNo");
        
        mFIExtType = (String)mTransferData.getValueByName("FIExtType");     

    	
    	return true;
    }	

    /**
     * 数据校检
     * @return
     */
    private boolean getCheckData()
    {
    	if("".equals(EventNo)||EventNo==null)
    	{
			buildError("DataTransRuleEngine","getCheckData", "事件编码为空！！");
            tLogDeal.WriteLogTxt("事件编码为空！！" + enter);
            return false;
    	}
    	
    	return true;
    }

    /**
     * 批量执行规则
     */
    private void dealRuels()
    {
		for(int i=1;i <=mFIDataExtractRulesSet.size();i++)
		{
			FIDataExtractRulesSchema tFIDataExtractRulesSchema = mFIDataExtractRulesSet.get(i);
			
			tLogDeal.WriteLogTxt("开始执行归档规则，规则编码："+tFIDataExtractRulesSchema.getArithmeticID() + enter);		
			
			mRuleType = tFIDataExtractRulesSchema.getArithmeticType();
			mSubRuleType = tFIDataExtractRulesSchema.getSubType();
			
			if(!"01".equals(mRuleType))
			{
				buildError("DataTransRuleEngine","dealRuels", "数据归档规则类型错误，规则编码："+tFIDataExtractRulesSchema.getArithmeticID());
                tLogDeal.WriteLogTxt("数据归档规则类型错误，规则编码："+tFIDataExtractRulesSchema.getArithmeticID() + enter);		
				break;				
			}
			if(!ruleEXE(tFIDataExtractRulesSchema))
			{
				buildError("DataTransRuleEngine","dealRuels", "数据归档规则类型错误，规则编码："+tFIDataExtractRulesSchema.getArithmeticID());
	            tLogDeal.WriteLogTxt("数据归档规则类型错误，规则编码："+tFIDataExtractRulesSchema.getArithmeticID()+ enter);					
			}
		}
    }
 
    /**
     * 规则执行
     * @param tFIDataExtractRulesSchema
     * @return
     */
    private boolean ruleEXE(FIDataExtractRulesSchema tFIDataExtractRulesSchema)
    {
		MMap tMap = new MMap();
		VData tInputData = new VData();
		PubSubmit tPubSubmit = new PubSubmit();
		String update = tFIDataExtractRulesSchema.getMainSQL();

		System.out.println(tFIDataExtractRulesSchema.getArithmeticID());
		tMap.put(update, "UPDATE");	

		tInputData.add(tMap);
        if (!tPubSubmit.submitData(tInputData, ""))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buildError("DataTransRuleEngine","ruleEXE", "数据归档规则执行出错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("数据归档规则执行出错，提示信息为：" +tPubSubmit.mErrors.getFirstError() + enter);
            return false;
        }      
        
    	return true;
    }
    
    /**
     * 获取需要归档的业务
     * @return
     */
    private FIBnTypeDefSet getFIBnType()
    {
    	
		String tSQL = "select * from FIBnTypeDef a where a.state = '1' and not exists(select 1 from Ficodetrans d where codetype = 'Secondary' and d.code = a.BusinessID) " 
			+ " and exists(select 1 from FIAboriginalGenAtt b where b.BusinessCode = a.BusType and b.SubBusinessCode = a.DetailType and b.TrandFlag = '1' and b.SubPhysicalTable in ('LRAccount_BF','LRAccount_PK','LRAccount_SX') and b.indexcode in ('11','10') and b.feetype in ('BF','PK','SX') and b.busdetail = 'LR'" 
			+ " and not exists (select 1 from FIAbStandardData c where c.ASerialNo = b.ASerialNo and c.NoType = '2' and c.indexcode in ('11','10') ))"; 		
		
		tSQL = tSQL+" union " ;
		
		tSQL = tSQL+ "select * from FIBnTypeDef a where a.state = '1' and not exists(select 1 from Ficodetrans d where codetype = 'Secondary' and d.code = a.BusinessID) " 
			+ " and exists(select 1 from FIAboriginalMainAtt b where b.BusinessCode = a.BusType and b.SubBusinessCode = a.DetailType and b.TrandFlag = '1' and b.PhysicalTable in ('LRAccount_M_BF','LRAccount_M_PK','LRAccount_M_SX') and b.indexcode in ('11','10') and b.busdetail = 'LR' and b.feetype in ('BF','PK','SX') " 
			+ " and not exists (select 1 from FIAbStandardData c where c.ASerialNo = b.MSerialNo and c.NoType = '1' and c.indexcode in ('11','10') ))"; 		
	
//		System.out.println("获取归档业务类型---------:"+tSQL);
		
		FIBnTypeDefSet tFIBnTypeDefSet = new FIBnTypeDefDB().executeQuery(tSQL+" with ur");
		
		return tFIBnTypeDefSet;
    }
    
    /**
     * 获取业务交易判断条件
     * @param tFIBnTypeDefSchema
     * @return
     */
    private String getBnCondition(FIBnTypeDefSchema tFIBnTypeDefSchema)
    {
    	String condition1 = " and b.BusinessCode = '"+tFIBnTypeDefSchema.getBusType()+"' and b.SubBusinessCode='"+tFIBnTypeDefSchema.getDetailType()+"' " ;
    	
    	FIBnTypeInfoSet tFIBnTypeInfoSet = new FIBnTypeInfoDB().executeQuery("select * from FIBnTypeInfo where BusinessID = '"+tFIBnTypeDefSchema.getBusinessID()+"' and Purpose='02' with ur");
		
		if(tFIBnTypeInfoSet!=null&&tFIBnTypeInfoSet.size()>0)
		{
			for(int j=1;j<=tFIBnTypeInfoSet.size();j++)
			{
				condition1 = condition1+" and b."+tFIBnTypeInfoSet.get(j).getPropertyCode()+" in(";
				String value = "";
				String values[] = tFIBnTypeInfoSet.get(j).getBusValues().split(";");
				for(int k=0;k<values.length;k++)
				{						
					if(k==values.length-1)
					{
						value = value+"'"+values[k]+"'";
					}
					else
					{
						value = value+"'"+values[k]+"',";
					}
				}
				
				condition1=condition1+value+") " ;
			}
		}

    	System.out.println("condition1:"+condition1);
    	return condition1;
    }
    
    /**
     * 获取费用判断条件
     * @param tFIBnTypeDefSchema
     * @return
     */
    private String getFeeCondition(FIBnFeeTypeDefSchema tFIBnFeeTypeDefSchema)
    {
    	String condition2 ="" ;
    	
    	FIBnFeeInfoSet tFIBnFeeInfo = new FIBnFeeInfoDB().executeQuery("select * from FIBnFeeInfo where CostID = '"+tFIBnFeeTypeDefSchema.getCostID()+"' and Purpose='02' with ur ");
		
		if(tFIBnFeeInfo!=null&&tFIBnFeeInfo.size()>0)
		{
			for(int j=1;j<=tFIBnFeeInfo.size();j++)
			{
				condition2 = condition2+" and b."+tFIBnFeeInfo.get(j).getPropertyCode()+" in(";
				String value = "";
				String values[] = tFIBnFeeInfo.get(j).getFeeValues().split(";");
				for(int k=0;k<values.length;k++)
				{						
					if(k==values.length-1)
					{
						value = value+"'"+values[k]+"'";
					}
					else
					{
						value = value+"'"+values[k]+"',";
					}
				}
				
				condition2=condition2+value+") " ;
			}
		}

		System.out.println("condition2:"+condition2);
		
    	return condition2;
    }
    
    /**
     * 主表标准业务数据提取规则
     * @param BusTypeID
     * @param CostID
     * @return
     */
    private String getMainFilingBusinessSql(FIBnTypeDefSchema tFIBnTypeDefSchema,FIBnFeeTypeDefSchema tFIBnFeeTypeDefSchema)
	{
		boolean YDflag = isYD(tFIBnFeeTypeDefSchema);
    	
    	StringBuffer tSQL = new StringBuffer();
	    tSQL.append(" select '' as SerialNo,");
		tSQL.append(" '1' as NoType,");
		tSQL.append(" b.MSerialNo as ASerialNo,");
		tSQL.append(" '0' as SecondaryFlag,");
		tSQL.append(" '0' as SecondaryType,");
		tSQL.append(" '"+tFIBnTypeDefSchema.getBusinessID()+"' as BusTypeID,");
		tSQL.append(" '"+tFIBnFeeTypeDefSchema.getCostID()+"' as CostID,");
		tSQL.append(" b.BusinessCode as BusinessType,");
		tSQL.append(" b.SubBusinessCode as BusinessDetail,");
		tSQL.append(" b.FeeType as FeeType,");
		tSQL.append(" b.FeeDetail as FeeDetail,");		
		tSQL.append(" b.IndexCode as IndexCode,");
		tSQL.append(" b.IndexNo as IndexNo,");			
		tSQL.append(" a.ListFlag as ListFlag,");
		tSQL.append(" a.GrpContNo as GrpContNo,");
		tSQL.append(" '' as GrpPolNo, ");
		tSQL.append(" a.ContNo as ContNo,");
		tSQL.append(" '' as PolNo,");
		tSQL.append(" '' as EndorsementNo,");
		tSQL.append(" a.BillNo as NotesNo,");
		tSQL.append(" b.FirstYearFlag as FirstYearFlag,");
		tSQL.append(" b.FirstTermFlag as FirstTermFlag,");
		tSQL.append(" '' as BonusType,");
		tSQL.append(" b.DifComFlag as DifComFlag,");
		tSQL.append(" (select c.riskcode from lmriskapp d,FIAboriginalGenDetail c where c.MSerialNo = a.MSerialNo and c.riskcode = d.riskcode and d.subriskflag = 'M' and db2inst1.LF_RISKTYPE(c.riskcode)=b.risktype and c.indexcode in ('11','10') fetch first 1 rows only) as RiskCode,");
		tSQL.append(" b.RiskPeriod as RiskPeriod,");
		tSQL.append(" b.RiskType as RiskType,");
		tSQL.append(" '' as RiskType1,");
		tSQL.append(" '' as RiskType2,");
		tSQL.append(" a.CostCenter as CostCenter,");//成本中心
		tSQL.append(" a.ACType as ACType,"); //中介机构类别
		tSQL.append(" a.AgentCom as AgentCom,");
		tSQL.append(" a.AgentGroup as AgentGroup,");
		tSQL.append(" a.AgentCode as AgentCode,");
		tSQL.append(" b.BClient as CustomerID,");
		tSQL.append(" a.SupplierNo as SupplierNo,");//供应商号码
		tSQL.append(" a.SaleChnl as SaleChnl,");
		tSQL.append(" a.SaleChnlDetail as SaleChnlDetail,");
		
		if(YDflag)
		{   //异地处理
			tSQL.append(" a.ExecuteCom as ManageCom,");
			tSQL.append(" a.ManageCom as ExecuteCom,");
		}
		else
		{
			tSQL.append(" a.ManageCom as ManageCom,");
			tSQL.append(" a.ExecuteCom as ExecuteCom,");
		}

		tSQL.append(" a.CValiDate as CValiDate,");
		tSQL.append(" a.PayIntv as PayIntv,");
		tSQL.append(" cast(null as date) as LastPayToDate,");
		tSQL.append(" cast(null as date) as CurPayToDate,");
		tSQL.append(" b.PolYear as PolYear,");
		tSQL.append(" b.Years as Years,");
		tSQL.append(" b.PremiumType as PremiumType,");
		tSQL.append(" cast(null as int) as PayCount,");
		tSQL.append(" b.PayMode as PayMode,");
		tSQL.append(" a.BankCode as BankCode,");
		tSQL.append(" a.AccName as AccName,");
		tSQL.append(" a.BankAccNo as BankAccNo,");
		tSQL.append(" b.SumActuMoney as SumActuMoney,");
		tSQL.append(" cast(null as date) as MothDate,");
		tSQL.append(" cast(null as date) as BusinessDate,");
		tSQL.append(" b.FIDate as AccountDate,");
		tSQL.append(" b.CashFlow as CashFlowNo,");
		tSQL.append(" b.FirstYear as FirstYear,");//
		tSQL.append(" b.MarketType as MarketType,");
		tSQL.append(" b.OperationType as OperationType,");
		tSQL.append(" '' as Budget,");
		tSQL.append(" b.Currency as Currency ,");
		tSQL.append(" '' as StringInfo01,"); //红冲申请号
		tSQL.append(" b.BusDetail as StringInfo02,");//业务明细
		tSQL.append(" a.PayTypeFlag as StringInfo03,"); //续保标记
		tSQL.append(" cast(null as date) as DateInfo01,");
		tSQL.append(" cast(null as date) as DateInfo02,");
		tSQL.append(" '' as RelatedNo,");
		tSQL.append(" '"+EventNo+"' as EventNo,");
		tSQL.append(" '00' as State,"); //待凭证转换
		tSQL.append(" '0' as ReadState,"); //未同步
		tSQL.append(" '00' as CheckFlag,");
		tSQL.append(" '"+mGlobalInput.Operator+"' as Operator,");
		tSQL.append(" cast(null as date) as MakeDate,");
		tSQL.append(" '' as MakeTime ");
		tSQL.append(" from FIAboriginalMain a,FIAboriginalMainAtt b where a.MSerialNo = b.MSerialNo and a.state ='10' and b.TrandFlag='1' and a.checkflag='00' and b.SumActuMoney<>0 ");
		tSQL.append(" and a.PhysicalTable = b.PhysicalTable and a.PhysicalTable in ('LRAccount_M_BF','LRAccount_M_PK','LRAccount_M_SX')");
		tSQL.append(" and a.indexcode = b.indexcode and a.indexcode in ('11','10') and b.busdetail = 'LR' and a.othernotype in ('BF','PK','SX') and b.feetype = a.othernotype ");
		tSQL.append(" and not exists(select 1 from FIAbStandardData c where c.ASerialNo = a.MSerialNo and NoType='1')");
		return tSQL.toString();
	}
    
    /**
     * 子表标准业务数据提取规则
     * @param BusTypeID
     * @param CostID
     * @return
     */
    private String getFilingBusinessSql(FIBnTypeDefSchema tFIBnTypeDefSchema,FIBnFeeTypeDefSchema tFIBnFeeTypeDefSchema)
	{
		boolean YDflag = isYD(tFIBnFeeTypeDefSchema);
    	
    	StringBuffer tSQL = new StringBuffer();
	    tSQL.append(" select '' as SerialNo,");
		tSQL.append(" '2' as NoType,");
		tSQL.append(" b.ASerialNo as ASerialNo,");
		tSQL.append(" '0' as SecondaryFlag,");
		tSQL.append(" '0' as SecondaryType,");
		tSQL.append(" '"+tFIBnTypeDefSchema.getBusinessID()+"' as BusTypeID,");
		tSQL.append(" '"+tFIBnFeeTypeDefSchema.getCostID()+"' as CostID,");
		tSQL.append(" b.BusinessCode as BusinessType,");
		tSQL.append(" b.SubBusinessCode as BusinessDetail,");
		tSQL.append(" b.FeeType as FeeType,");
		tSQL.append(" b.FeeDetail as FeeDetail,");
		tSQL.append(" b.IndexCode as IndexCode,");
		tSQL.append(" b.IndexNo as IndexNo,");
		tSQL.append(" b.ListFlag as ListFlag,");
		tSQL.append(" a.GrpContNo as GrpContNo,");
		tSQL.append(" a.GrpPolNo as GrpPolNo, ");
		tSQL.append(" a.ContNo as ContNo,");
		tSQL.append(" a.PolNo as PolNo,");
		tSQL.append(" a.EndorsementNo as EndorsementNo,");
		tSQL.append(" (select c.BillNo from FIAboriginalMain c where c.MSerialNo = a.MSerialNo and c.indexcode in ('11','10') )as NotesNo,");
		tSQL.append(" b.FirstYearFlag as FirstYearFlag,");
		tSQL.append(" b.FirstTermFlag as FirstTermFlag,");
		tSQL.append(" '' as BonusType,");
		tSQL.append(" b.DifComFlag as DifComFlag,");
		tSQL.append(" a.RiskCode as RiskCode,");
		tSQL.append(" b.RiskPeriod as RiskPeriod,");
		tSQL.append(" b.RiskType as RiskType,");
		tSQL.append(" '' as RiskType1,");
		tSQL.append(" '' as RiskType2,");
		tSQL.append(" a.CostCenter as CostCenter,");//成本中心
		tSQL.append(" a.ACType as ACType,"); //中介机构类别
		tSQL.append(" a.AgentCom as AgentCom,");
		tSQL.append(" a.AgentGroup as AgentGroup,");
		tSQL.append(" a.AgentCode as AgentCode,");
		tSQL.append(" b.CustomerID as CustomerID,");
		tSQL.append(" a.SupplierNo as SupplierNo,");//供应商号码
		tSQL.append(" a.SaleChnl as SaleChnl,");
		tSQL.append(" a.SaleChnlDetail as SaleChnlDetail,");
		
		if(YDflag)
		{   //异地处理
			tSQL.append(" a.ExecuteCom as ManageCom,");
			tSQL.append(" a.ManageCom as ExecuteCom,");
		}
		else
		{
			tSQL.append(" a.ManageCom as ManageCom,");
			tSQL.append(" a.ExecuteCom as ExecuteCom,");
		}

		tSQL.append(" a.CValiDate as CValiDate,");
		tSQL.append(" a.PayIntv as PayIntv,");
		tSQL.append(" a.LastPayToDate as LastPayToDate,");
		tSQL.append(" a.CurPayToDate as CurPayToDate,");
		tSQL.append(" b.PolYear as PolYear,");
		tSQL.append(" b.Years as Years,");
		tSQL.append(" b.PremiumType as PremiumType,");
		tSQL.append(" a.PayCount as PayCount,");
		tSQL.append(" b.PayMode as PayMode,");
		tSQL.append(" a.BankCode as BankCode,");
		tSQL.append(" a.AccName as AccName,");
		tSQL.append(" a.BankAccNo as BankAccNo,");
		tSQL.append(" b.SumActuMoney as SumActuMoney,");
		tSQL.append(" cast(null as date) as MothDate,");
		tSQL.append(" cast(null as date) as BusinessDate,");
		tSQL.append(" b.FIDate as AccountDate,");
		tSQL.append(" b.CashFlow as CashFlowNo,");
		tSQL.append(" b.FirstYear as FirstYear,");//计费首年
		tSQL.append(" b.MarketType as MarketType,");
		tSQL.append(" b.OperationType as OperationType,");
		tSQL.append(" '' as Budget,");
		tSQL.append(" b.Currency as Currency ,");
		tSQL.append(" '' as StringInfo01,"); //红冲申请号
		tSQL.append(" b.BusDetail as StringInfo02,");//业务明细
		tSQL.append(" a.PayTypeFlag as StringInfo03,");
		tSQL.append(" cast(null as date) as DateInfo01,");
		tSQL.append(" cast(null as date) as DateInfo02,");
		tSQL.append(" '' as RelatedNo,");
		tSQL.append(" '"+EventNo+"' as EventNo,");
		tSQL.append(" '00' as State,");
		tSQL.append(" '0' as ReadState,"); //未同步
		tSQL.append(" '00' as CheckFlag,");
		tSQL.append(" '"+mGlobalInput.Operator+"' as Operator,");
		tSQL.append(" cast(null as date) as MakeDate,");
		tSQL.append(" '' as MakeTime ");
		tSQL.append(" from FIAboriginalGenDetail a,FIAboriginalGenAtt b where a.ASerialNo = b.ASerialNo and a.state ='10' and b.TrandFlag='1' and a.checkflag = '00' and b.SumActuMoney<>0 ");
		tSQL.append(" and b.SubPhysicalTable in ('LRAccount_BF','LRAccount_PK','LRAccount_SX') and b.SubPhysicalTable = a.SubPhysicalTable");
		tSQL.append(" and a.indexcode in ('11','10') and a.indexcode = b.indexcode and b.feetype in ('BF','PK','SX') and b.busdetail = 'LR'  and a.feetype = b.feetype ");
		tSQL.append(" and not exists(select 1 from FIAbStandardData c where c.ASerialNo = a.ASerialNo and NoType='2')");
		return tSQL.toString();
	}

    /**
     * 判断是否是异地
     * @param tFIBnTypeDefSchema
     * @param tFIBnFeeTypeDefSchema
     * @return
     */
    private boolean isYD(FIBnFeeTypeDefSchema tFIBnFeeTypeDefSchema)
    {
    	boolean flag  =false;
    	
    	String tSQL = "select count(*) from FIVoucherBnFee where BusinessID = '"+tFIBnFeeTypeDefSchema.getBusinessID()+"' and FeeID = '"+tFIBnFeeTypeDefSchema.getCostID()+"' and exists(select 1 from FMFinItemDef where FinItemID = D_FinItemID and FinItemType='3' )";
    	
    	ExeSQL tExeSQL = new ExeSQL();
    	int i = Integer.parseInt(tExeSQL.getOneValue(tSQL));
    	if(i>0)
    	{
    		flag = true;
    	}
    	return flag ;   	
    }
	/**
	 * 数据存储
	 * @param tFIAbStandardDataSet
	 * @return
	 */
	private boolean saveStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		MMap tMap= new MMap();
		VData tInputData = new VData();
		PubSubmit tPubSubmit = new PubSubmit();
		
		FIAbStandardDataSet outFIAbStandardDataSet = prepareStandardData(tFIAbStandardDataSet);
		
		tMap.put(outFIAbStandardDataSet, "INSERT");
		tInputData.add(tMap);
		
        if (!tPubSubmit.submitData(tInputData, ""))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buildError("DataTransRuleEngine","saveStandardData", "准业务数据存储时出错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("准业务数据存储时出错，提示信息为：，提示信息为：" +tPubSubmit.mErrors.getFirstError() + enter);
            return false;
        }
        
        tMap=null;
        tInputData=null;
        outFIAbStandardDataSet=null;
        
        return true;
	}
	
	/**
	 * 封装其它信息
	 * @param tFIAbStandardDataSet
	 * @return
	 */
	private FIAbStandardDataSet prepareStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet ssFIAbStandardDataSet = tFIAbStandardDataSet;

		try 
		{
			String[] tSerialNo = FinCreateSerialNo.getSerialNoByFINDATA(ssFIAbStandardDataSet.size());
			for(int i=1;i<=ssFIAbStandardDataSet.size();i++)
			{
				FIAbStandardDataSchema tFIAbStandardDataSchema = ssFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]);
				tFIAbStandardDataSchema.setMakeDate(PubFun.getCurrentDate());
				tFIAbStandardDataSchema.setMakeTime(PubFun.getCurrentTime());
			}			
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		return ssFIAbStandardDataSet;
	}
	
	
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    
    /**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
