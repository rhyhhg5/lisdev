package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;
/**
 * <p>Title: 二次处理规则-续期完全从账户抵扣</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2018</p>
 *
 * <p>Company: </p>
 *
 * @author rosia
 * @version 1.0 
 */

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;

import com.sinosoft.lis.pubfun.PubFun;

import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;

public class SecondaryRule_XQZHDK extends StandSecondaryRule{
	
	public String prepareRule(){
		StringBuffer tSQL = new StringBuffer();
		tSQL.append("select distinct '' as SerialNo, a.NoType as NoType, a.ASerialNo as ASerialNo, '1' as SecondaryFlag, 'DK' as SecondaryType, a.BusTypeID as BusTypeID, a.CostID as CostID, a.BusinessType as BusinessType, a.BusinessDetail as BusinessDetail, a.FeeType as FeeType, a.FeeDetail as FeeDetail, a.IndexCode as IndexCode, a.IndexNo as IndexNo, " +
				" a.ListFlag as ListFlag, a.GrpContNo as GrpContNo, a.GrpPolNo as GrpPolNo, a.ContNo as ContNo, a.PolNo as PolNo, a.EndorsementNo as EndorsementNo, a.NotesNo as NotesNo, a.FirstYearFlag as FirstYearFlag, a.FirstTermFlag as FirstTermFlag, a.BonusType as BonusType, a.DifComFlag as DifComFlag, a.RiskCode as RiskCode, a.RiskPeriod as RiskPeriod,"+
				 " a.risktype as RiskType, a.RiskType1 as RiskType1, a.RiskType2 as RiskType2, a.CostCenter as CostCenter, a.ACType as ACType, a.AgentCom as AgentCom, a.AgentGroup as AgentGroup, a.AgentCode as AgentCode, a.CustomerID as CustomerID, a.SupplierNo as SupplierNo, a.SaleChnl as SaleChnl, a.SaleChnlDetail as SaleChnlDetail, "+
				 " a.ManageCom as ManageCom, a.ExecuteCom as ExecuteCom, a.CValiDate as CValiDate, a.PayIntv as PayIntv, a.LastPayToDate as LastPayToDate, a.CurPayToDate as CurPayToDate, a.PolYear as PolYear, a.Years as Years, a.PremiumType as PremiumType, a.PayCount as PayCount, a.PayMode as PayMode, a.BankCode as BankCode, a.AccName as AccName, "+
				 " a.BankAccNo as BankAccNo, a.SumActuMoney as SumActuMoney, a.MothDate as MothDate, a.BusinessDate as BusinessDate, a.AccountDate as AccountDate, a.CashFlowNo as CashFlowNo, a.FirstYear as FirstYear, a.MarketType as MarketType, a.OperationType as OperationType, a.Budget as Budget, a.Currency as Currency, a.StringInfo01 as StringInfo01,"+
				 " a.StringInfo02 as StringInfo02, a.StringInfo03 as StringInfo03, a.DateInfo01 as DateInfo01, a.DateInfo02 as DateInfo02, a.SerialNo as RelatedNo, a.EventNo as EventNo, a.State as State, a.ReadState as ReadState, a.CheckFlag as CheckFlag, a.Operator as Operator, '"+PubFun.getCurrentDate()+"' as MakeDate, '"+PubFun.getCurrentTime()+"' as MakeTime "+ 
				 " from FIAbStandardData a where a.costid in('F0000000021','F0000000022','F0000000SE1') and feedetail in('YEL','ZE','ZC') and a.bustypeid = 'O-XQ-000001' and a.secondaryflag = '0' and a.indexcode = '02'  and  state = '00' "+
				 " and exists (select 1 from fiabstandarddata f where f.indexno = a.indexno and f.indexcode = a.indexcode and f.bustypeid = 'O-XQ-000001' and f.costid = 'F0000000021' and f.feedetail = 'YEL' and  ( f.sumactumoney =(select c.sumactumoney from fiaboriginalmain c where f.indexno = c.indexno) or f.sumactumoney =(select c.sumactumoney from fiaboriginalmainp c where f.indexno = c.indexno))) and CheckFlag = '00' "+
				 " and not exists (select 1 from FIAbStandardData d where a.SerialNo = d.RelatedNo and d.CostID in('F000000XQ21','F000000XQ22','F00000XQSE1') and d.SecondaryType = 'DK' and a.indexno = d.indexno and a.indexcode = d.indexcode)");
			return tSQL.toString() ;
	}
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet){
		
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				if("F0000000021".equals(tFIAbStandardDataSchema.getCostID()))
					tFIAbStandardDataSchema.setCostID("F000000XQ21");
				else if ("F0000000022".equals(tFIAbStandardDataSchema.getCostID()))
					tFIAbStandardDataSchema.setCostID("F000000XQ22");
				else if("F0000000SE1".equals(tFIAbStandardDataSchema.getCostID()))
					tFIAbStandardDataSchema.setCostID("F00000XQSE1");
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		
		return pFIAbStandardDataSet;
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
