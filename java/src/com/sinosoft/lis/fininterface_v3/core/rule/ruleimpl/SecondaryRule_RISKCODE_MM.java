package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.db.FIAbStandardDataDB;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.utility.VData;
/**
 * <p>Title: 二次处理规则--约定缴费，实收金额为0</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author liusc
 * @version 3.0 
 */
public class SecondaryRule_RISKCODE_MM extends StandSecondaryRule{

	
	public String prepareRule()
	{              
		StringBuffer tSQL = new StringBuffer();
//		tSQL.append(" select distinct '' as SerialNo,");
//		tSQL.append(" a.NoType as NoType ,");
//		tSQL.append(" a.ASerialNo as ASerialNo ,");
//		tSQL.append(" '1' as SecondaryFlag ,");
//		tSQL.append(" 'MM' as SecondaryType ,");
//		tSQL.append(" a.BusTypeID as BusTypeID ,");
//		tSQL.append(" a.CostID as CostID ,");
//		tSQL.append(" a.BusinessType as BusinessType ,");
//		tSQL.append(" a.BusinessDetail as BusinessDetail ,");
//		tSQL.append(" a.FeeType as FeeType ,");
//		tSQL.append(" a.FeeDetail as FeeDetail ,");
//		tSQL.append(" a.IndexCode as IndexCode ,");
//		tSQL.append(" a.IndexNo as IndexNo ,");
//		tSQL.append(" a.ListFlag as ListFlag ,");
//		tSQL.append(" a.GrpContNo as GrpContNo ,");
//		tSQL.append(" a.GrpPolNo as GrpPolNo ,");
//		tSQL.append(" a.ContNo as ContNo ,");
//		tSQL.append(" a.PolNo as PolNo ,");
//		tSQL.append(" a.EndorsementNo as EndorsementNo ,");
//		tSQL.append(" a.NotesNo as NotesNo ,");
//		tSQL.append(" a.FirstYearFlag as FirstYearFlag ,");
//		tSQL.append(" a.FirstTermFlag as FirstTermFlag ,");
//		tSQL.append(" a.BonusType as BonusType ,");
//		tSQL.append(" a.DifComFlag as DifComFlag ,");
//		tSQL.append(" (select b.RiskCode from ljapayperson b where a.indexno = b.payno and a.riskcode <> b.riskcode and b.sumactupaymoney <> 0 and LF_risktype(b.riskcode) <> a.risktype fetch first 1 row only ) as RiskCode ,");
//		tSQL.append(" a.RiskPeriod as RiskPeriod ,");
//		tSQL.append(" LF_risktype((select b.RiskCode from ljapayperson b where a.indexno = b.payno and a.riskcode <> b.riskcode and b.sumactupaymoney <> 0 and LF_risktype(b.riskcode) <> a.risktype fetch first 1 row only )) as RiskType ,");
//		tSQL.append(" a.RiskType1 as RiskType1 ,");
//		tSQL.append(" a.RiskType2 as RiskType2 ,");
//		tSQL.append(" a.CostCenter as CostCenter ,");
//		tSQL.append(" a.ACType as ACType,");
//		tSQL.append(" a.AgentCom as AgentCom,");
//		tSQL.append(" a.AgentGroup as AgentGroup ,");
//		tSQL.append(" a.AgentCode as AgentCode ,");
//		tSQL.append(" a.CustomerID as CustomerID ,");
//		tSQL.append(" a.SupplierNo as SupplierNo ,");
//		tSQL.append(" a.SaleChnl as SaleChnl ,");
//		tSQL.append(" (select b.RiskCode from ljapayperson b where a.indexno = b.payno and a.riskcode <> b.riskcode and b.sumactupaymoney <> 0 and LF_risktype(b.riskcode) <> a.risktype fetch first 1 row only) as SaleChnlDetail ,");
//		tSQL.append(" a.ManageCom as ManageCom ,");
//		tSQL.append(" a.ExecuteCom as ExecuteCom ,");
//		tSQL.append(" a.CValiDate as CValiDate ,");
//		tSQL.append(" a.PayIntv as PayIntv ,");
//		tSQL.append(" a.LastPayToDate as LastPayToDate ,");
//		tSQL.append(" a.CurPayToDate as CurPayToDate ,");
//		tSQL.append(" a.PolYear as PolYear ,");
//		tSQL.append(" a.Years as Years ,");
//		tSQL.append(" a.PremiumType as PremiumType ,");
//		tSQL.append(" a.PayCount as PayCount ,");
//		tSQL.append(" a.PayMode as PayMode ,");
//		tSQL.append(" a.BankCode as BankCode ,");
//		tSQL.append(" a.AccName as AccName,");
//		tSQL.append(" a.BankAccNo as BankAccNo,");
//		tSQL.append(" (select nvl(sum(moneynotax),0) from ljapayperson where payno=a.indexno and sumactupaymoney<>0 and riskcode <> a.riskcode and LF_risktype(riskcode) <> a.risktype) as SumActuMoney,");
//		tSQL.append(" a.MothDate as MothDate ,");
//		tSQL.append(" a.BusinessDate as BusinessDate ,");
//		tSQL.append(" a.AccountDate as AccountDate ,");
//		tSQL.append(" a.CashFlowNo  as CashFlowNo ,");
//		tSQL.append(" a.FirstYear as FirstYear ,");
//		tSQL.append(" a.MarketType as MarketType ,");
//		tSQL.append(" a.OperationType as OperationType ,");
//		tSQL.append(" a.Budget as Budget ,");
//		tSQL.append(" a.Currency as Currency ,");
//		tSQL.append(" a.StringInfo01 as StringInfo01  ,");
//		tSQL.append(" a.StringInfo02 as StringInfo02,");
//		tSQL.append(" a.StringInfo03  as StringInfo03 ,");
//		tSQL.append(" a.DateInfo01 as DateInfo01 ,");
//		tSQL.append(" a.DateInfo02 as DateInfo02 ,");
//		tSQL.append(" a.SerialNo as RelatedNo,");
//		tSQL.append(" a.EventNo as EventNo ,");
//		tSQL.append(" a.State as State,");
//		tSQL.append(" a.ReadState as ReadState ,");
//		tSQL.append(" a.CheckFlag as CheckFlag ,");
//		tSQL.append(" a.Operator as Operator ,");
//		tSQL.append(" '"+PubFun.getCurrentDate()+"' as MakeDate ,");
//		tSQL.append(" '"+PubFun.getCurrentTime()+"' as MakeTime ");
//		tSQL.append(" from FIAbStandardData a");
//		tSQL.append(" where costid='F0000000022' and listflag='1' and (bustypeid like 'O-NC%' or bustypeid like 'O-XQ%') ");
//		tSQL.append(" and exists (select 1 from ljapayperson b where a.indexno = b.payno and a.riskcode <> b.riskcode and b.sumactupaymoney <> 0 and LF_risktype(b.riskcode) <> a.risktype ");
//		tSQL.append(" and exists(select 1 from lmriskapp c where b.riskcode=c.riskcode and c.Subriskflag = 'M') )");
//		tSQL.append(" and state = '00' and CheckFlag='00' and not exists(select 1 from FIAbStandardData d where a.SerialNo = d.RelatedNo and d.CostID ='F0000000C22' and d.SecondaryType='MM' ) ");
//		tSQL.append(" union");
		tSQL.append(" select distinct '' as SerialNo,");
		tSQL.append(" a.NoType as NoType ,");
		tSQL.append(" a.ASerialNo as ASerialNo ,");
		tSQL.append(" '1' as SecondaryFlag ,");
		tSQL.append(" 'MM' as SecondaryType ,");
		tSQL.append(" a.BusTypeID as BusTypeID ,");
		tSQL.append(" a.CostID as CostID ,");
		tSQL.append(" a.BusinessType as BusinessType ,");
		tSQL.append(" a.BusinessDetail as BusinessDetail ,");
		tSQL.append(" a.FeeType as FeeType ,");
		tSQL.append(" a.FeeDetail as FeeDetail ,");
		tSQL.append(" a.IndexCode as IndexCode ,");
		tSQL.append(" a.IndexNo as IndexNo ,");
		tSQL.append(" a.ListFlag as ListFlag ,");
		tSQL.append(" a.GrpContNo as GrpContNo ,");
		tSQL.append(" a.GrpPolNo as GrpPolNo ,");
		tSQL.append(" a.ContNo as ContNo ,");
		tSQL.append(" a.PolNo as PolNo ,");
		tSQL.append(" a.EndorsementNo as EndorsementNo ,");
		tSQL.append(" a.NotesNo as NotesNo ,");
		tSQL.append(" a.FirstYearFlag as FirstYearFlag ,");
		tSQL.append(" a.FirstTermFlag as FirstTermFlag ,");
		tSQL.append(" a.BonusType as BonusType,");
		tSQL.append(" a.DifComFlag as DifComFlag ,");
		tSQL.append(" (select b.RiskCode from ljapayperson b where a.indexno = b.payno and a.riskcode <> b.riskcode and b.sumactupaymoney <> 0 and LF_risktype(b.riskcode) <> a.risktype fetch first 1 row only ) as RiskCode ,");
		tSQL.append(" a.RiskPeriod as RiskPeriod ,");
		tSQL.append(" LF_risktype((select b.RiskCode from ljapayperson b where a.indexno = b.payno and a.riskcode <> b.riskcode and b.sumactupaymoney <> 0 and LF_risktype(b.riskcode) <> a.risktype fetch first 1 row only )) as RiskType ,");
		tSQL.append(" a.RiskType1 as RiskType1 ,");
		tSQL.append(" a.RiskType2 as RiskType2 ,");
		tSQL.append(" a.CostCenter as CostCenter ,");
		tSQL.append(" a.ACType as ACType ,");
		tSQL.append(" a.AgentCom as AgentCom ,");
		tSQL.append(" a.AgentGroup as AgentGroup ,");
		tSQL.append(" a.AgentCode as AgentCode ,");
		tSQL.append(" a.CustomerID as CustomerID ,");
		tSQL.append(" a.SupplierNo as SupplierNo ,");
		tSQL.append(" a.SaleChnl as SaleChnl ,");
		tSQL.append(" (select b.RiskCode from ljapayperson b where a.indexno = b.payno and a.riskcode <> b.riskcode and b.sumactupaymoney <> 0 and LF_risktype(b.riskcode) <> a.risktype fetch first 1 row only ) as SaleChnlDetail ,");
		tSQL.append(" a.ManageCom  as ManageCom ,");
		tSQL.append(" a.ExecuteCom as ExecuteCom ,");
		tSQL.append(" a.CValiDate as CValiDate ,");
		tSQL.append(" a.PayIntv as PayIntv ,");
		tSQL.append(" a.LastPayToDate as LastPayToDate ,");
		tSQL.append(" a.CurPayToDate as CurPayToDate ,");
		tSQL.append(" a.PolYear as PolYear ,");
		tSQL.append(" a.Years as Years ,");
		tSQL.append(" a.PremiumType as PremiumType ,");
		tSQL.append(" a.PayCount as PayCount ,");
		tSQL.append(" a.PayMode as PayMode ,");
		tSQL.append(" a.BankCode as BankCode ,");
		tSQL.append(" a.AccName as AccName ,");
		tSQL.append(" a.BankAccNo as BankAccNo ,");
		tSQL.append(" (select nvl(sum(moneynotax),0) from ljapayperson where payno=a.indexno and sumactupaymoney<>0 and riskcode <> a.riskcode and LF_risktype(riskcode)  <> a.risktype) as SumActuMoney ,");
		tSQL.append(" a.MothDate as MothDate ,");
		tSQL.append(" a.BusinessDate  as BusinessDate ,");
		tSQL.append(" a.AccountDate as AccountDate ,");
		tSQL.append(" a.CashFlowNo as CashFlowNo ,");
		tSQL.append(" a.FirstYear as FirstYear ,");
		tSQL.append(" a.MarketType as MarketType ,");
		tSQL.append(" a.OperationType as OperationType ,");
		tSQL.append(" a.Budget as Budget ,");
		tSQL.append(" a.Currency as Currency ,");
		tSQL.append(" a.StringInfo01 as StringInfo01 ,");
		tSQL.append(" a.StringInfo02 as StringInfo02 ,");
		tSQL.append(" a.StringInfo03 as StringInfo03 ,");
		tSQL.append(" a.DateInfo01 as DateInfo01 ,");
		tSQL.append(" a.DateInfo02 as DateInfo02 ,");
		tSQL.append(" a.SerialNo as RelatedNo,");
		tSQL.append(" a.EventNo as EventNo ,");
		tSQL.append(" a.State as State ,");
		tSQL.append(" a.ReadState as ReadState ,");
		tSQL.append(" a.CheckFlag as CheckFlag ,");
		tSQL.append(" a.Operator as Operator ,");
		tSQL.append(" '"+PubFun.getCurrentDate()+"' as MakeDate ,");
		tSQL.append(" '"+PubFun.getCurrentTime()+"' as MakeTime ");
		tSQL.append(" from FIAbStandardData a");
		tSQL.append(" where costid='F0000000022' and listflag='1' and (bustypeid like 'O-NC%' or bustypeid like 'O-XQ%') ");
		tSQL.append(" and exists (select 1 from ljapayperson b where a.indexno = b.payno and a.riskcode <> b.riskcode and b.sumactupaymoney <> 0 and LF_risktype(b.riskcode) <> a.risktype ");
		tSQL.append(" and exists(select 1 from lmriskapp c where b.riskcode=c.riskcode and c.Subriskflag = 'M') )");
		tSQL.append(" and state = '00' and CheckFlag='00' and not exists(select 1 from FIAbStandardData d where a.SerialNo = d.RelatedNo and d.CostID ='F0000000C22' and d.SecondaryType='MM' ) ");
		
		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				//约定缴费-实收金额为0
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;
				tFIAbStandardDataSchema.setCostID("F0000000C22");
				
				if (tFIAbStandardDataSchema.getBusTypeID().trim()=="O-NC-000001"||"O-NC-000001".equals(tFIAbStandardDataSchema.getBusTypeID().trim())) {
					tFIAbStandardDataSchema.setBusTypeID("O-NC-000002"); 					
				}else if (tFIAbStandardDataSchema.getBusTypeID().trim()=="O-NC-000002"||"O-NC-000002".equals(tFIAbStandardDataSchema.getBusTypeID().trim())) {
					tFIAbStandardDataSchema.setBusTypeID("O-NC-000001");					
				}else if (tFIAbStandardDataSchema.getBusTypeID().trim()=="O-XQ-000001"||"O-XQ-000001".equals(tFIAbStandardDataSchema.getBusTypeID().trim())) {
					tFIAbStandardDataSchema.setBusTypeID("O-XQ-000002");					
				}else{
					tFIAbStandardDataSchema.setBusTypeID("O-XQ-000001"); 
				}
				
				PubSubmit mPubSubmit = new PubSubmit();
				MMap tMap= new MMap();
				VData tInputData = new VData();
				String tsql="update FIAbStandardData a set sumactumoney= ((select sum(sumactupaymoney) from ljapayperson where payno=a.indexno and sumactupaymoney<>0 and LF_risktype(riskcode)=a.risktype and a.risktype not in ('0','5'))" +
						" + nvl((select sumactumoney from fiabstandarddata fi where fi.indexno = a.indexno and fi.costid = 'F00000000OP'),0)) where serialno='"+tFIAbStandardDataSchema.getRelatedNo()+"'";
				
				tMap.put(tsql, "UPDATE");
				tInputData.add(tMap);
		        if (!mPubSubmit.submitData(tInputData, ""))
		        {
		        	throw new  Exception("SecondaryRule_RISKCODE_MM:更新错误！");
		        	
		        }
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
//		SecondaryRule_RISKCODE_MM tSecondaryRule_RISKCODE_MM = new SecondaryRule_RISKCODE_MM();
//		System.out.println("sql:"+tSecondaryRule_RISKCODE_MM.prepareRule());
//		String tsql=tSecondaryRule_RISKCODE_MM.prepareRule();
//		FIAbStandardDataSet tFIAbStandardDataSet=new FIAbStandardDataSet();
//		FIAbStandardDataDB tFIAbStandardDataDB=new FIAbStandardDataDB();
//		tFIAbStandardDataSet=tFIAbStandardDataDB.executeQuery(tsql);
//		
//		MMap tMap= new MMap();
//		VData tInputData = new VData();
//		PubSubmit tPubSubmit = new PubSubmit();
//		
//		//获取处理后的数据
//		FIAbStandardDataSet outFIAbStandardDataSet = tSecondaryRule_RISKCODE_MM.prepareFIAbStandardData(tFIAbStandardDataSet);
//		
//		tMap.put(outFIAbStandardDataSet, "INSERT");
//		
//		tInputData.add(tMap);
//		
//		if (!tPubSubmit.submitData(tInputData, ""))
//		{
//			System.out.println("执行失败！");
//			
//		}
//		
		
		//tSecondaryRule_YD_SS.ruleExe(cInputData, cOperate)
		
	}

}
