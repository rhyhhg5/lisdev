package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--理赔预付申请异地</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class SecondaryRule_LPYFYD extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

		tSQL.append(" select * from FIAbStandardData a where bustypeid in('F-LP-000006','F-LP-000008','F-LP-000003') and a.state = '00' and a.CheckFlag='00' " );
		tSQL.append(" and Feetype='YF' and Feedetail='YF' " );
		tSQL.append(" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.bustypeid in('Y-LP-000003','Y-LP-000001','Y-LP-000012'))");
		
		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			int size = pFIAbStandardDataSet.size();
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(size);
			
			for(int i=1;i<=size;i++)			
			{
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;
				String executeCom = tFIAbStandardDataSchema.getExecuteCom();
				
				tFIAbStandardDataSchema.setBusinessType("05");//异地代收
				tFIAbStandardDataSchema.setBusinessDetail("052");
				tFIAbStandardDataSchema.setFeeType("YF");
				tFIAbStandardDataSchema.setFeeDetail("YF");
				tFIAbStandardDataSchema.setIndexCode("03");
				tFIAbStandardDataSchema.setManageCom(executeCom);
				
				String riskType=tFIAbStandardDataSchema.getRiskType();
				if("0".equals(riskType) || "4".equals(riskType) || "5".equals(riskType))
				{
					//预付赔款-申请-异地(传统险)
					tFIAbStandardDataSchema.setBusTypeID("Y-LP-000001");
				}else
				{
					//预付赔款-申请-异地(账户险)
					tFIAbStandardDataSchema.setBusTypeID("Y-LP-000003");
				}
				tFIAbStandardDataSchema.setCostID("F0000000YFS");
				tFIAbStandardDataSchema.setSecondaryFlag("1");		
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		SecondaryRule_LPYFYD tSecondaryRule_LPYFYD = new SecondaryRule_LPYFYD();

		System.out.println(tSecondaryRule_LPYFYD.prepareRule());
		
	}

}
