package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;

import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/**
 * <p>Title: 二次处理规则--首期约定交费计提-税额</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class SecondaryRule_YDJFSE extends StandSecondaryRule{

       	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();
//		先生效
		tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType," +
				"a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo," +
				"a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType," +
				"a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl," +
				"a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years," +
				"a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,b.prem2 SumActuMoney,a.MothDate,a.BusinessDate," +
				"a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02," +
				"a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate," +
				"a.MakeTime from lcgrppayplandetail_FCSQ b,FIAbStandardData a where a.grpcontno = b.grpcontno and a.riskcode = b.riskcode and b.prem2 <> 0 " +
				" and a.NoType = '2' and trim(a.state) = '00' and trim(a.BusinessType) = '03' and a.Businessdetail='031' and a.payintv =-1 and a.CheckFlag='00' and a.feetype = 'ZE' and a.accountdate >= '2016-05-01'" +
				" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.CostID='F0000000SE2' ) ");
//		先开票
		tSQL.append(" union all ");
		tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType," +
				"a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo," +
				"a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType," +
				"a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl," +
				"a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years," +
				"a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,b.prem2 SumActuMoney,a.MothDate,a.BusinessDate," +
				"a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02," +
				"a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate," +
				"a.MakeTime from lcgrppayplandetail_FCSQ b,FIAbStandardData a where a.grpcontno = b.grpcontno and a.riskcode = b.riskcode and b.prem2 <> 0 " +
				" and a.NoType = '2' and trim(a.state) = '00' and trim(a.BusinessType) = '03' and a.Businessdetail='031' and a.payintv =-1 and a.CheckFlag='00' and a.feetype = 'ZC' and a.accountdate >= '2016-05-01'" +
				" and exists (select 1 from FIAbStandardData f where f.indexno =a.indexno || substr(year(f.accountdate), 3, 2) ||month(f.accountdate) || day(f.accountdate) ||'01' and f.feetype = 'FP' and f.indexcode = '12' and f.BusinessType = '13' and f.Businessdetail='011')"+
				" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.CostID='F0000000SE2' ) ");
		
		
		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
	
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				tFIAbStandardDataSchema.setBusinessType("10");//应收计提
				tFIAbStandardDataSchema.setBusinessDetail("106");//首期约定缴费计提
				tFIAbStandardDataSchema.setBusTypeID("O-NC-000005");
				tFIAbStandardDataSchema.setCostID("F0000000SE2");
				tFIAbStandardDataSchema.setFeeType("ZE");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//	
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_YDJFSE tSecondaryRule_YDJFSE = new SecondaryRule_YDJFSE();
		System.out.println(tSecondaryRule_YDJFSE.prepareRule());
		
	}

}
