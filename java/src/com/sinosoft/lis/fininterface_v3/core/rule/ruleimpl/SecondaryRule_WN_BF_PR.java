package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LAComSet;
import com.sinosoft.lis.vschema.LCContSet;
/**
 * <p>Title: 二次处理规则--万能保单迁移本金</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class SecondaryRule_WN_BF_PR extends StandSecondaryRule{

	
	public String prepareRule()
	{
        String tStartDate = (String)mTransferData.getValueByName("StartDate");
        String tEndDate = (String)mTransferData.getValueByName("EndDate");
		
		StringBuffer tSQL = new StringBuffer();
		tSQL.append(" select distinct '' as SerialNo,");
		tSQL.append(" '0' as NoType ,");
		tSQL.append(" '' as ASerialNo ,");
		tSQL.append(" '1' as SecondaryFlag ,");
		tSQL.append(" 'PR' as SecondaryType ,");
		tSQL.append(" 'Y-BQ-PR-000001' as BusTypeID ,");
		tSQL.append(" 'F00000000AP2' as CostID ,");
		tSQL.append(" '04' as BusinessType ,");
		tSQL.append(" '042' as BusinessDetail ,");
		tSQL.append(" 'PR' as FeeType ,");
		tSQL.append(" 'BF' as FeeDetail ,");
		tSQL.append(" '03' as IndexCode ,");
		tSQL.append(" e.Edoracceptno as IndexNo ,");
		tSQL.append(" '1' ListFlag ,");
		tSQL.append(" a.GrpContNo ,");
		tSQL.append(" a.GrpPolNo ,");
		tSQL.append(" a.ContNo ,");
		tSQL.append(" a.PolNo ,");
		tSQL.append(" e.Edoracceptno as EndorsementNo ,");
		tSQL.append(" '' as NotesNo ,");
		tSQL.append(" 'S' as FirstYearFlag ,");
		tSQL.append(" 'S' as FirstTermFlag ,");
		tSQL.append(" '' as BonusType ,");
		tSQL.append(" '0' as DifComFlag ,");
		tSQL.append(" a.RiskCode ,");
		tSQL.append(" '' as RiskPeriod ,");
		tSQL.append(" '1' as RiskType ,");
		tSQL.append(" '' as RiskType1 ,");
		tSQL.append(" '' as RiskType2 ,");
		tSQL.append(" (select costcenter from labranchgroup where agentgroup = d.agentgroup fetch first 1 rows only) as CostCenter ,");
		tSQL.append(" (select actype from lacom where agentcom=d.agentcom fetch first 1 rows only) as ACType ,");
		tSQL.append(" d.AgentCom ,");
		tSQL.append(" d.AgentGroup ,");
		tSQL.append(" d.AgentCode ,");
		tSQL.append(" '99999999' as CustomerID ,");
		tSQL.append(" '' as SupplierNo ,");//供应商号码
		tSQL.append(" d.SaleChnl ,");
		tSQL.append(" d.SaleChnlDetail ,");
		tSQL.append(" d.ManageCom ,");
		tSQL.append(" a.ManageCom as ExecuteCom ,");
		tSQL.append(" a.CValiDate ,");
		tSQL.append(" a.PayIntv ,");
		tSQL.append(" cast(null as date) as LastPayToDate ,");
		tSQL.append(" cast(null as date) as CurPayToDate ,");
		tSQL.append(" LF_PolYear(a.PayIntv, a.PayYears) as PolYear ,");
		tSQL.append(" LF_Years(a.InsuYearFlag, a.InsuYear, a.Years) as Years ,");
		tSQL.append(" LF_PremiumType(a.RiskCode, 'ZC') as PremiumType ,");
		tSQL.append(" 0 as PayCount ,");
		tSQL.append(" '' as PayMode ,");
		tSQL.append(" '' as BankCode ,");
		tSQL.append(" '' as AccName ,");
		tSQL.append(" '' as BankAccNo ,");
		tSQL.append(" (select sum(money) from lcinsureacctrace cc where cc.contno = a.contno and cc.polno = a.polno ) as SumActuMoney ,");
		tSQL.append(" cast(null as date) as MothDate ,");
		tSQL.append(" b.ConfDate as BusinessDate ,");
		tSQL.append(" b.ConfDate as AccountDate ,");
		tSQL.append(" '' as CashFlowNo ,");
		tSQL.append(" substr(char(b.ConfDate), 1, 4) as FirstYear ,");
		tSQL.append(" '1' as MarketType ,");
		tSQL.append(" '' as OperationType ,");
		tSQL.append(" '' as Budget ,");
		tSQL.append(" 'CNY' as Currency ,");
		tSQL.append(" a.AppntNo as StringInfo01 ,");
		tSQL.append(" '' as StringInfo02 ,");
		tSQL.append(" '' as StringInfo03 ,");
		tSQL.append(" cast(null as date) as DateInfo01 ,");
		tSQL.append(" cast(null as date) as DateInfo02 ,");
		tSQL.append(" '' as RelatedNo,");
		tSQL.append(" '' as EventNo,");
		tSQL.append(" '00' as State,"); //待凭证转换
		tSQL.append(" '0' as ReadState,"); //未同步
		tSQL.append(" '00' as CheckFlag,");
		tSQL.append(" '"+mGlobalInput.Operator+"' as Operator,");
		tSQL.append(" '"+PubFun.getCurrentDate()+"' as MakeDate ,");
		tSQL.append(" '"+PubFun.getCurrentTime()+"' as MakeTime ");
		tSQL.append(" from LCPol a,lpedorapp b,lccont c, lpcont d, Lpedoritem e ");
		tSQL.append(" where e.Edortype = 'PR' and d.Edorno = e.Edoracceptno and c.contno = d.contno and b.Edoracceptno = d.Edorno and a.contno = c.contno ");
		tSQL.append(" and b.ConfDate between '"+tStartDate+"' and '"+tEndDate+"' ");
		tSQL.append(" and not exists(select 1 from FIAbStandardData f where f.indexcode='03' and f.indexno=e.Edoracceptno and f.contno=d.contno and f.PolNo=a.PolNo and f.CostID ='F00000000AP2' and f.FeeDetail='BF' ) ");

		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			int siz = pFIAbStandardDataSet.size();
			int size = pFIAbStandardDataSet.size()*2;
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(size);
			
			for(int i=1;i<=siz;i++)			
			{
				//(1)保单迁移本金(迁出机构)
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getIndexNo());
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//
				
				//(2)保单迁移本金(迁入机构)
				FIAbStandardDataSchema tcFIAbStandardDataSchema = (FIAbStandardDataSchema)tFIAbStandardDataSchema.clone();
				String manageCom = tcFIAbStandardDataSchema.getManageCom();
				String executeCom = tcFIAbStandardDataSchema.getExecuteCom();
				
				tcFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tcFIAbStandardDataSchema.setSerialNo(tSerialNo[size-i]) ;//
				tcFIAbStandardDataSchema.setManageCom(executeCom);
				tcFIAbStandardDataSchema.setExecuteCom(manageCom);
				tcFIAbStandardDataSchema.setCostID("F00000000AP3");
				LCContSet tLCContSet = new LCContDB().executeQuery("select * from lccont where contno='"+tcFIAbStandardDataSchema.getContNo()+"' with ur ");
				if(tLCContSet!=null &&tLCContSet.size()>0)
				{
					LCContSchema tLCContSchema = tLCContSet.get(1);
					tcFIAbStandardDataSchema.setAgentCom(tLCContSchema.getAgentCom());
					tcFIAbStandardDataSchema.setAgentCode(tLCContSchema.getAgentCode());
					tcFIAbStandardDataSchema.setAgentGroup(tLCContSchema.getAgentGroup());
					tcFIAbStandardDataSchema.setSaleChnl(tLCContSchema.getSaleChnl());
					tcFIAbStandardDataSchema.setSaleChnlDetail(tLCContSchema.getSaleChnlDetail());
					LABranchGroupSet tLABranchGroupSet =  new LABranchGroupDB().executeQuery("select * from labranchgroup where agentgroup='"+tLCContSchema.getAgentGroup()+"' with ur ");
					if(tLABranchGroupSet!=null &&tLABranchGroupSet.size()>0)
					{
						tcFIAbStandardDataSchema.setCostCenter(tLABranchGroupSet.get(1).getCostCenter());
					}
					LAComSet tLAComSet = new LAComDB().executeQuery("select * from lacom where agentcom='"+tLCContSchema.getAgentCom()+"' with ur ");
					if(tLAComSet!=null &&tLAComSet.size()>0)
					{
						tcFIAbStandardDataSchema.setACType(tLAComSet.get(1).getACType());
					}
					
					pFIAbStandardDataSet.add(tcFIAbStandardDataSchema);	
				}
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		SecondaryRule_WN_BF_PR tSecondaryRule_WN_BF_PR = new SecondaryRule_WN_BF_PR();
		System.out.println(tSecondaryRule_WN_BF_PR.prepareRule());
		
	}

}
