package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.db.FIDataBaseLinkDB;
import com.sinosoft.lis.fininterface_v3.core.rule.Rule;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.fininterface_v3.tools.dblink.DBConn;
import com.sinosoft.lis.fininterface_v3.tools.dblink.DBConnPool;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIDataBaseLinkSchema;
import com.sinosoft.lis.schema.FIDataExtractRulesSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.lis.vschema.FIDataBaseLinkSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 二次处理规则</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class StandSecondaryRule implements Rule{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    public GlobalInput mGlobalInput = new GlobalInput();
    
    public TransferData mTransferData ;	
	/** 往后面传输数据的容器 */
	private VData mInputData= new VData();
	
	private FIDataExtractRulesSchema mFIDataExtractRulesSchema;
	
    private FIOperationLog tLogDeal = null;
    
    private String EventNo = "";
    
	/** 数据操作字符串 */
	private String mOperate;
	
	
    private final String enter = "\r\n"; // 换行符
    
	public boolean ruleExe(VData cInputData,FIDataExtractRulesSchema cFIDataExtractRulesSchema) 
	{
		// TODO Auto-generated method stub
		this.mFIDataExtractRulesSchema = cFIDataExtractRulesSchema ;
		
		//获取前台数据
		if(!getInputData(cInputData))
		{
			return false;
		}
		//数据校检
		if(!checkData())
		{
			return false;
		}
		
		//进行业务处理
		if (!dealData())
		{
			return false;
		}

		return true;
	}
	/**
	 * 获取前台数据
	 * @param cInputData
	 * @return
	 */
	private boolean getInputData(VData cInputData) 
	{
		// TODO Auto-generated method stub
		mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0);
        tLogDeal =(FIOperationLog)cInputData.getObjectByObjectName("FIOperationLog",0);
        mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData",0);
		return true;
	}
	
	/**
	 * 数据校检
	 * @return
	 */
	private boolean checkData() 
	{
		// TODO Auto-generated method stub
        if(mGlobalInput==null)
        {
        	buildError("StandSecondaryRule","checkData", "前台传输全局公共数据失败!");
        	return false;
        }
        if(tLogDeal==null)
        {
        	buildError("StandSecondaryRule","checkData", "获取事件记录对象失败!");
        	return false;
        }
        EventNo = tLogDeal.getEventNo();
		return true;
	}

	/**
	 * 数据逻辑处理
	 * @return
	 */
	private boolean dealData()
	{
		String ruleSQL = prepareRule();
		if(ruleSQL==null||"".equals(ruleSQL))
		{
			buildError("StandSecondaryRule","dealData", "获取规则失败失败!");
			return false;
		}

		FIDataBaseLinkSchema tFIDataBaseLinkSchema= getDBSource(this.mFIDataExtractRulesSchema);
		
		RSWrapper  rsWrapper = getRSWrapper(tFIDataBaseLinkSchema);
		if(rsWrapper==null)
		{
			System.out.println("StandSecondaryRule-->获取数据源失败!");
			buildError("StandSecondaryRule","dealData", "批量数据抽取对象RSWrapper初始化时出错，规则编码："+this.mFIDataExtractRulesSchema.getArithmeticID()+"");
            tLogDeal.WriteLogTxt("批量数据抽取对象RSWrapper初始化时出错，规则编码："+this.mFIDataExtractRulesSchema.getArithmeticID()+"" + enter);
			return false;
		}
		
		FIAbStandardDataSet tFIAbStandardDataSet = new FIAbStandardDataSet();
		
        if (!rsWrapper.prepareData(tFIAbStandardDataSet, ruleSQL+" with ur "))
        {
            mErrors.copyAllErrors(rsWrapper.mErrors);
            System.out.println(rsWrapper.mErrors.getFirstError());
            buildError("StandSecondaryRule","dealData", "二次处理，规则执行出错,提示信息为：" +rsWrapper.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("二次处理，规则执行出错,提示信息为：" +rsWrapper.mErrors.getFirstError() + enter);
            return false;
        }
        do
        {
           	rsWrapper.getData();
            if (tFIAbStandardDataSet != null && tFIAbStandardDataSet.size() > 0)
            {
            	
            	if(!dealFIAbStandardData(tFIAbStandardDataSet))
        		{
            		tLogDeal.WriteLogTxt("二次处理，数据处理出现错误！" + enter);
        		}
            }    
        }while(tFIAbStandardDataSet != null && tFIAbStandardDataSet.size() > 0);

        rsWrapper.close();	
		
		return true;
	}
	
	/**
	 * 规则准备
	 * @return
	 */
	public String prepareRule()
	{
		String tSQL = "";
			
		return tSQL ;
	}
	
	/**
	 * 数据准备
	 * @param tFIAbStandardDataSet
	 * @return
	 */
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		
		return pFIAbStandardDataSet;
	}
	

	/**
	 * 数据处理
	 * @param tFIAbStandardDataSet
	 * @return
	 */
	private boolean dealFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		MMap tMap= new MMap();
		VData tInputData = new VData();
		PubSubmit tPubSubmit = new PubSubmit();
		
		//获取处理后的数据
		FIAbStandardDataSet outFIAbStandardDataSet = prepareFIAbStandardData(tFIAbStandardDataSet);
		
		tMap.put(outFIAbStandardDataSet, "INSERT");
		
		tInputData.add(tMap);
		
		if (!tPubSubmit.submitData(tInputData, ""))
		{
			buildError("StandSecondaryRule","PubSubmit", "数据存储失败!");
			tLogDeal.WriteLogTxt("二次处理，数据处理出现错误！PubSubmit数据存储失败!" + enter);
			return false;
		}
		
		return true;
	}
	
	/**
	 * 获取大数据量提取对象RSWrapper
	 * @param tFIDataBaseLinkSchema
	 * @return
	 */
	private RSWrapper getRSWrapper(FIDataBaseLinkSchema tFIDataBaseLinkSchema)
	{
		RSWrapper rsWrapper = null;
		DBConn con = null;
		if(tFIDataBaseLinkSchema==null)
		{
			rsWrapper = new RSWrapper();
		}	
		else
		{	//异地数据源
			try 
			{
				con = DBConnPool.getConnection(tFIDataBaseLinkSchema);
			} 
			catch (Exception e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				buildError("DataExtRuleEngine","getRSWrapper", "批量数据抽取对象RSWrapper初始化时出错，数据源："+tFIDataBaseLinkSchema.getInterfaceCode());
				tLogDeal.WriteLogTxt("批量数据抽取对象RSWrapper初始化时出错，数据源："+tFIDataBaseLinkSchema.getInterfaceCode() + enter);
				return null;
			}
			
			rsWrapper = new RSWrapper(con);
		}
		return rsWrapper;
	}
	/**
	 * 获取规则执行数据源
	 * @return
	 */
	private FIDataBaseLinkSchema getDBSource(FIDataExtractRulesSchema tFIDataExtractRulesSchema)
	{
        FIDataBaseLinkDB tFIDataBaseLinkDB = new FIDataBaseLinkDB();
        String strKeyDefSQL = "select * from FIDataBaseLink where InterfaceCode ='"+tFIDataExtractRulesSchema.getDataSource()+"' with ur ";
        FIDataBaseLinkSet tFIDataBaseLinkSet = tFIDataBaseLinkDB.executeQuery(strKeyDefSQL);
        if(tFIDataBaseLinkSet == null || tFIDataBaseLinkSet.size()==0)
        {
            return null;
        }		
		return tFIDataBaseLinkSet.get(1);
	}
	
	public VData getResult()
	{
		return null;
	}

	/**
	 * 错误信息
	 * @param szModuleName
	 * @param szFunc
	 * @param szErrMsg
	 */
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }

}
