package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--首期年度化（税优产品）</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author liusc
 * @version 3.0
 */
public class SecondaryRule_NDHSY extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

		tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType,");
		tSQL.append(" a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,");
		tSQL.append(" a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType,");
		tSQL.append(" a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl,");
		tSQL.append(" a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years,");
		tSQL.append(" a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,");
		tSQL.append(" a.SumActuMoney*LF_LastPayCount(a.CurPayToDate,(select b.payenddate from lcpol b where b.contno=a.contno and b.riskcode=a.riskcode),a.PayIntv) SumActuMoney,");
		tSQL.append(" a.MothDate,a.BusinessDate,a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02,");
		tSQL.append(" a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate,");
		tSQL.append(" a.MakeTime from FIAbStandardData a where a.NoType = '2' and a.state = '00' and ListFlag='1' and a.BusinessType = '03' and a.Businessdetail='031' ");
		tSQL.append(" and a.riskperiod in ('M','S') and a.payintv not in (0,12,-1) and a.CheckFlag='00'  and a.feedetail = 'BF'");
		tSQL.append(" and exists (select 1 from ficodetrans f  where f.code = a.riskcode and f.codetype = 'ShuiYou')");
		tSQL.append(" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.CostID='F0000000SY1')");
		
		tSQL.append(" union all ");
		
		tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType,");
		tSQL.append(" a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,");
		tSQL.append(" a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType,");
		tSQL.append(" a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl,");
		tSQL.append(" a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years,");
		tSQL.append(" a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,");
		tSQL.append(" (select -b.Sumactupaymoney from ljapayperson b where b.contno = a.contno and b.polno = a.polno and Sumactupaymoney>0 and b.PayType='ZC' and exists(select 1 from ljapay c where c.payno = b.payno and c.DueFeeType in('0','2')) order by paycount fetch first 1 rows only) SumActuMoney,");
		tSQL.append(" a.MothDate,a.BusinessDate,a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02,");
		tSQL.append(" a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate,");
		tSQL.append(" a.MakeTime from FIAbStandardData a where a.NoType = '2' and a.state = '00' and ListFlag='1' and a.BusinessType = '03' and a.Businessdetail='032' ");
		tSQL.append(" and a.riskperiod in ('M','S') and a.payintv not in (0,12,-1) and a.CheckFlag='00' and a.feedetail = 'BF'");
		tSQL.append(" and exists (select 1 from ficodetrans f  where f.code = a.riskcode and f.codetype = 'ShuiYou')");
		tSQL.append(" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.CostID='F0000000SY2')");

		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				//短期险非趸缴非年交 年度化保费
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				
				if("O-NC-000002".equals(tFIAbStandardDataSchema.getBusTypeID()))
				{   //首期应收
					tFIAbStandardDataSchema.setSecondaryFlag("1");
					tFIAbStandardDataSchema.setCostID("F0000000SY1");
					tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//	
					tFIAbStandardDataSchema.setBusinessType("12");
					tFIAbStandardDataSchema.setBusinessDetail("101");
					tFIAbStandardDataSchema.setBusTypeID("O-SY-000001");
				}
				else if("O-XQ-000002".equals(tFIAbStandardDataSchema.getBusTypeID()))
				{   
					//税优续期应收反冲
					tFIAbStandardDataSchema.setBusinessType("12");//
					tFIAbStandardDataSchema.setSecondaryFlag("1");
					tFIAbStandardDataSchema.setBusinessDetail("102");//
					tFIAbStandardDataSchema.setCostID("F0000000SY2");
					tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;										
					tFIAbStandardDataSchema.setBusTypeID("O-SY-000002");
					}
					
				}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_NDHSY tSecondaryRule_NDHSY = new SecondaryRule_NDHSY();
		
		
		//tSecondaryRule_YD.ruleExe(cInputData, cOperate)
		
	}

}
