package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--万能税额旧准则</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2013</p>
 *
 * <p>Company: </p>
 *
 * @author 
 * @version 3.0
 */
public class SecondaryRule_WN_SE extends StandSecondaryRule{

//	32048224770
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();
//收费保储
		tSQL.append(" select * from FIAbStandardData a where a.NoType = '2' and a.state = '00' and a.BusinessType in ('03','04') and a.Businessdetail in ('031','032','041','033') " +
				"and a.risktype in ('1','3') and a.CheckFlag='00' and a.accountdate >= '2016-5-1' and a.costid in ('F000000SEGL','F000000EEGL','F0000SETXMF','F000000ZHSE') " +
				"and not exists (select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.SecondaryFlag = '1' and c.costid in ('F0000WNSEGL','F0000WNEEGL','F00TXSETXMF','F0000WNZHSE'))");
		tSQL.append(" union all ");
//		付费保储		
		tSQL.append(" select * from FIAbStandardData a where a.NoType = '2' and a.state = '00' and a.BusinessType in ('02','04','06') and a.Businessdetail in ('022','042','062') " +
				"and a.risktype in ('1','3') and a.CheckFlag='00' and a.accountdate >= '2016-5-1' and a.costid in ('F0000SECTMF_X','F000000SEMF2','F000000SEGL','F000000EEGL','F00000SETM0') " +
				"and not exists (select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.SecondaryFlag = '1' and c.costid in ('F0000WNSEGL','F0000WNEEGL','F00TXSECTMF_X','F0000TXSEMF2','F000XTSETM0'))");
	
		
		
		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				if("F000000SEGL".equals(tFIAbStandardDataSchema.getCostID()))
				{
					tFIAbStandardDataSchema.setCostID("F0000WNSEGL");					
				}
				else if ("F000000EEGL".equals(tFIAbStandardDataSchema.getCostID())){
					tFIAbStandardDataSchema.setCostID("F0000WNEEGL");
				}
				else if ("F0000SETXMF".equals(tFIAbStandardDataSchema.getCostID())){
					tFIAbStandardDataSchema.setCostID("F00TXSETXMF");
				}
				else if ("F000000ZHSE".equals(tFIAbStandardDataSchema.getCostID())){
					tFIAbStandardDataSchema.setCostID("F0000WNZHSE");
				}
				else if ("F0000SECTMF_X".equals(tFIAbStandardDataSchema.getCostID())){
					tFIAbStandardDataSchema.setCostID("F00TXSECTMF_X");
				}
				else if ("F000000SEMF2".equals(tFIAbStandardDataSchema.getCostID())){
					tFIAbStandardDataSchema.setCostID("F0000TXSEMF2");
				}	
				else if ("F00000SETM0".equals(tFIAbStandardDataSchema.getCostID())){
					tFIAbStandardDataSchema.setCostID("F000XTSETM0");
				}			
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;
				
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_WN_SE tSecondaryRule_WN_SE = new SecondaryRule_WN_SE();
		System.out.println(tSecondaryRule_WN_SE.prepareRule());
		
	}

}
