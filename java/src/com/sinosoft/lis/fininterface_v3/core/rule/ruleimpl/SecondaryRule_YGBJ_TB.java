
package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/**
 * <p>Title: 二次处理规则--反冲退保保单的预估结余返还</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author liusc
 * @version 3.0
 */
public class SecondaryRule_YGBJ_TB extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();
//		String sqlTB = "select distinct a.grpcontno from FIAbStandardData a where a.feetype in ('WT','CT','XT') and a.ListFlag='2' and a.BusinessType = '04' and a.Businessdetail='042' and a.indexcode='03'";
		String sqlTB = "select distinct a.grpcontno from FIAbStandardData a where a.feetype in ('WT','CT','XT') and a.state = '00' and a.ListFlag='2' and a.BusinessType = '04' "+
		               " and a.Businessdetail='042' and a.indexcode='03' " +
	                   " and exists (select 1 from FIAbStandardData b where b.feetype='YGBJ' and b.ListFlag='2' and trim(b.BusinessType) = '04' and b.Businessdetail='042' and b.indexcode='03' and a.grpcontno = b.grpcontno)"+
		               " and not exists (select 1 from FIAbStandardData m where m.feetype in ('BJ','YGBJ_F') "+		         
		               " and m.ListFlag='2' and trim(m.BusinessType) = '04' and m.Businessdetail='042' and m.indexcode='03' and a.grpcontno = m.grpcontno) " ;
		
		String a = null; String b = null; String c = null;int j;
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(sqlTB);
//		System.out.println("11111111111111111111111");
		StringBuffer buffer = new StringBuffer();
		int maxRow = tSSRS.getMaxRow();
		if (maxRow != 0){			 
		if (maxRow == 1){			
			c = "'"+tSSRS.GetText(1, 1)+"'";
		}else{
			for (j=1;j<tSSRS.getMaxRow();j++){				

				a = buffer.append("'"+tSSRS.GetText(j, 1)+"',").toString();
			}
	        if (j == tSSRS.getMaxRow()){  
	        	
	          	 b = "'"+tSSRS.GetText(tSSRS.getMaxRow(), 1)+"'"; 
	           }
			 c = a + b;
//			 System.out.println(c);
		}
	       			
			tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType,");
			tSQL.append(" a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,");
			tSQL.append(" a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType,");
			tSQL.append(" a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl,");
			tSQL.append(" a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years,");
			tSQL.append(" a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,");
			tSQL.append(" -SumActuMoney as SumActuMoney,");
			tSQL.append(" a.MothDate,a.BusinessDate,a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02,");
			tSQL.append(" a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,'"+PubFun.getCurrentDate()+"' as MakeDate,'"+PubFun.getCurrentTime()+"' as maketime");
			tSQL.append(" from FIAbStandardData a where a.feetype='YGBJ' and a.BusinessType = '04' and a.Businessdetail='042' ");
			tSQL.append(" and a.indexcode='03' and a.SecondaryFlag = '0' and a.CheckFlag='00'");
			tSQL.append(" and a.grpcontno in ("+c+")");
			tSQL.append(" and not exists(select 1 from FIAbStandardData f where a.SerialNo = f.RelatedNo and a.grpcontno = f.grpcontno and f.SecondaryFlag = '1' and f.feetype = 'YGBJ_F')");
		}else{
			tSQL.append("select 1 from FIAbStandardData where 1=2 ");
		}
		
        return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			String sqldate = "select distinct a.accountdate,a.eventno from FIAbStandardData a where a.feetype in ('WT','CT','XT') and state = '00' and ListFlag='2' and a.BusinessType = '04' "+
		               " and a.Businessdetail='042' and a.indexcode='03'" +
		               " and exists (select 1 from FIAbStandardData b where b.feetype='YGBJ' and b.ListFlag='2' and b.BusinessType = '04' and b.Businessdetail='042' and b.indexcode='03' and a.grpcontno = b.grpcontno)"+
		               " and not exists (select 1 from FIAbStandardData m where m.feetype in ('BJ','YGBJ_F') "+		         
		               " and m.ListFlag='2' and m.BusinessType = '04' and m.Businessdetail='042' and m.indexcode='03' and a.grpcontno = m.grpcontno) fetch first 1 rows only" ;
					 
		
			
			SSRS mSSRS = new SSRS();
			ExeSQL tExeSQL = new ExeSQL();
			mSSRS = tExeSQL.execSQL(sqldate);
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				System.out.println("getSerialNo()--"+tFIAbStandardDataSchema.getSerialNo());
					tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
					tFIAbStandardDataSchema.setSecondaryFlag("1");
					tFIAbStandardDataSchema.setFeeType("YGBJ_F");
					tFIAbStandardDataSchema.setState("00");
					tFIAbStandardDataSchema.setReadState("0");
					tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;					
					String adate = mSSRS.GetText(1,1);
					String aeventno = mSSRS.GetText(1,2);
					tFIAbStandardDataSchema.setEventNo(aeventno);
					tFIAbStandardDataSchema.setAccountDate(adate);
			}
        }
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_YGBJ_TB tSecondaryRule_NDH_GB = new SecondaryRule_YGBJ_TB();
		//tSecondaryRule_YD.ruleExe(cInputData, cOperate)
		
	}
	

}
