package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--约定缴费退保反冲年度化-共保-2013后</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class SecondaryRule_YDJF_FCTB_GB extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

		
		
		tSQL.append(" select distinct '' as SerialNo,a.NoType,'' as ASerialNo,a.SecondaryFlag,'TY'as SecondaryType,a.BusTypeID,a.CostID,a.BusinessType," +
				"a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,'' as ContNo,'' as PolNo," +
				"a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType," +
				"a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl," +
				"a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years," +
				"a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo," +
				"cast(round(-LF_YD_MONEY('',a.riskcode,a.grpcontno,'','TB')*(select sum(rate) from LCCoInsuranceParam b where b.grpcontno = a.grpcontno),2) as decimal(30,2)) SumActuMoney," +
				"a.MothDate,a.BusinessDate," +
				"a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02," +
				"a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate," +
				"a.MakeTime " +
				" from FIAbStandardData a ,LBGrpCont b " +
				" where a.grpcontno = b.grpcontno and b.CoInsuranceFlag = '1'  " +
				" and a.feetype in ('WT','XT','CT') " +
				" and a.BusTypeID = 'Y-BQ-FC-000002' and SecondaryType='YT' and costid = 'F0000000025' " +
				" and a.NoType = '2' and a.payintv =-1 and a.state = '00' and a.CheckFlag='00' and a.BusinessType = '10' and a.Businessdetail='107' " +
				" and exists(select 1 from FIAbStandardData d where a.grpcontno = d.grpcontno " +
				" and d.BusinessType = '03' and d.Businessdetail = '031'  and d.paycount = '1' " +
				" and d.accountdate >= '2014-1-1' fetch first 1 row only)" + 
				" and not exists(select 1 from FIAbStandardData c where c.indexno=a.indexno and c.BusTypeID='GB-BQ-FC-000002' )");
		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{

				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setBusinessType("20");//应收计提
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				tFIAbStandardDataSchema.setBusinessDetail("303");//
				tFIAbStandardDataSchema.setBusTypeID("GB-BQ-FC-000002");
				tFIAbStandardDataSchema.setCostID("GB000000025");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_BQFCYDJF tSecondaryRule_BQFCYDJF = new SecondaryRule_BQFCYDJF();
		System.out.println(tSecondaryRule_BQFCYDJF.prepareRule());
		
	}

}
