package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--J-02</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author liusc
 * @version 3.0
 */
public class SecondaryRule_J02 extends StandSecondaryRule{
	String ipdate="";
	
	
	public String prepareRule()
	{
		String tipdate = PubFun.getCurrentDate();
	    System.out.println(tipdate);
	    String[] ttipdate =  tipdate.toString().split("-");
	    System.out.println(ttipdate[2]);
	    System.out.println("HHHHHHHHHHHHHHHAAAAAAA");
	      
        String[] StartDate = mTransferData.getValueByName("StartDate").toString().split("-");
        String[] EndDate = mTransferData.getValueByName("EndDate").toString().split("-");
        
        if(StartDate[1].equals(EndDate[1]))
        {
        	if(Integer.parseInt(StartDate[2])==1)
        	{
        		ipdate=mTransferData.getValueByName("StartDate").toString();

        	}
        }else if(!StartDate[1].equals(EndDate[1]))
        {
        	
        		ipdate=EndDate[0]+"-"+EndDate[1]+"-01";

        	
        }

		StringBuffer tSQL = new StringBuffer();
	    tSQL.append(" select * from FIAbStandardData a where a.BusTypeID in ('O-XQ-000006') and a.accountdate=(date('"+ipdate+"') - 1 days) and month(date('"+ipdate+"') - 1 days) <> month(date('"+ipdate+"')) ");
	    tSQL.append(" and not exists (select 1 from FIAbStandardData b where (b.grpcontno=a.grpcontno or b.contno=a.contno) and (b.grppolno=a.grppolno or b.polno=a.polno) and b.SecondaryType='J4' and b.accountdate='"+ipdate+"')");
	    if("".equals(ipdate))
			return "select * from FIAbStandardData where 1>2 ";
	    
	    if (!ttipdate[2].equals("01")) return "select * from FIAbStandardData where 1>3 ";
	    
		else
			return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				//月初一号反冲应收计提
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setSumActuMoney(-tFIAbStandardDataSchema.getSumActuMoney());
				tFIAbStandardDataSchema.setAccountDate(ipdate);
				tFIAbStandardDataSchema.setBusinessDetail("105");
				tFIAbStandardDataSchema.setCostID("F0000000025");
				tFIAbStandardDataSchema.setSecondaryType("J4");
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//		
				tFIAbStandardDataSchema.setBusTypeID("O-XQ-000007");

			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_J02 tSecondaryRule_J02 = new SecondaryRule_J02();
		
		
		//tSecondaryRule_YD.ruleExe(cInputData, cOperate)
		
	}

}
