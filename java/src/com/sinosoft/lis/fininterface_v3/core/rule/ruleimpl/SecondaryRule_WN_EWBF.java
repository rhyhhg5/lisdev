package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--万能额外保费</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class SecondaryRule_WN_EWBF extends StandSecondaryRule{

	
	public String prepareRule()
	{
		
		StringBuffer tSQL = new StringBuffer();
		tSQL.append(" select distinct '' as SerialNo,");
		tSQL.append(" a.NoType ,");
		tSQL.append(" '' as ASerialNo ,");
		tSQL.append(" '1' as SecondaryFlag ,");
		tSQL.append(" 'EW' as SecondaryType ,");
		tSQL.append(" a.BusTypeID ,");
		tSQL.append(" a.CostID ,");
		tSQL.append(" a.BusinessType ,");
		tSQL.append(" a.BusinessDetail ,");
		tSQL.append(" a.FeeType ,");
		tSQL.append(" a.FeeDetail ,");
		tSQL.append(" a.IndexCode ,");
		tSQL.append(" a.IndexNo ,");
		tSQL.append(" a.ListFlag ,");
		tSQL.append(" a.GrpContNo ,");
		tSQL.append(" a.GrpPolNo ,");
		tSQL.append(" a.ContNo ,");
		tSQL.append(" a.PolNo ,");
		tSQL.append(" a.EndorsementNo ,");
		tSQL.append(" a.NotesNo ,");
		tSQL.append(" a.FirstYearFlag ,");
		tSQL.append(" a.FirstTermFlag ,");
		tSQL.append(" a.BonusType ,");
		tSQL.append(" a.DifComFlag ,");
		tSQL.append(" d.RiskCode ,");
		tSQL.append(" a.RiskPeriod ,");
		tSQL.append(" a.RiskType ,");
		tSQL.append(" a.RiskType1 ,");
		tSQL.append(" a.RiskType2 ,");
		tSQL.append(" a.CostCenter ,");
		tSQL.append(" a.ACType ,");
		tSQL.append(" a.AgentCom ,");
		tSQL.append(" a.AgentGroup ,");
		tSQL.append(" a.AgentCode ,");
		tSQL.append(" a.CustomerID ,");
		tSQL.append(" a.SupplierNo ,");
		tSQL.append(" a.SaleChnl ,");
		tSQL.append(" a.SaleChnlDetail ,");
		tSQL.append(" a.ManageCom ,");
		tSQL.append(" a.ExecuteCom ,");
		tSQL.append(" a.CValiDate ,");
		tSQL.append(" a.PayIntv ,");
		tSQL.append(" a.LastPayToDate ,");
		tSQL.append(" a.CurPayToDate ,");
		tSQL.append(" a.PolYear ,");
		tSQL.append(" a.Years ,");
		tSQL.append(" LF_PremiumType(d.RiskCode, 'EW') as PremiumType ,");
		tSQL.append(" a.PayCount ,");
		tSQL.append(" a.PayMode ,");
		tSQL.append(" a.BankCode ,");
		tSQL.append(" a.AccName ,");
		tSQL.append(" a.BankAccNo ,");
		tSQL.append(" (d.SumActuPayMoney-LF_OmnipotentMoney(d.contno,d.riskcode,d.SumActuPayMoney)) as SumActuMoney ,");
		tSQL.append(" a.MothDate ,");
		tSQL.append(" a.BusinessDate ,");
		tSQL.append(" a.AccountDate ,");
		tSQL.append(" a.CashFlowNo ,");
		tSQL.append(" a.FirstYear ,");
		tSQL.append(" a.MarketType ,");
		tSQL.append(" a.OperationType ,");
		tSQL.append(" a.Budget ,");
		tSQL.append(" a.Currency ,");
		tSQL.append(" a.StringInfo01 ,");
		tSQL.append(" a.StringInfo02 ,");
		tSQL.append(" a.StringInfo03 ,");
		tSQL.append(" a.DateInfo01 ,");
		tSQL.append(" a.DateInfo02 ,");
		tSQL.append(" a.RelatedNo ,");
		tSQL.append(" a.EventNo ,");
		tSQL.append(" a.State ,");
		tSQL.append(" a.ReadState ,");
		tSQL.append(" a.CheckFlag ,");
		tSQL.append(" a.Operator ,");
		tSQL.append(" '"+PubFun.getCurrentDate()+"' as MakeDate ,");
		tSQL.append(" '"+PubFun.getCurrentTime()+"' as MakeTime ");
		tSQL.append(" from LJAPayperson d,FIAbStandardData a where a.indexno = d.payno and trim(d.PayType) = 'ZC'  and a.feedetail='BF' and a.NoType = '2' and a.state = '00' and a.Bustypeid in('O-NC-000002','O-XQ-000002') and a.costid= 'F00000000AP' and a.RiskCode in ('330801', '331801', '332701', '331301', '331701','333901','334701') and a.risktype ='1' and a.CheckFlag='00' ");
		tSQL.append(" and a.riskcode=d.riskcode and a.secondaryflag='0' and (d.SumActuPayMoney-LF_OmnipotentMoney(d.contno,d.riskcode,d.SumActuPayMoney))<>0 ");
		tSQL.append(" and not exists(select 1 from FIAbStandardData b where b.indexcode=a.indexcode and b.indexno=a.indexno and b.SecondaryType='EW') ");

		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			int tSize = pFIAbStandardDataSet.size();
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(tSize);
			
			for(int i=1;i<=tSize;i++)			
			{
				//(1)额外保费
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getIndexNo());
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		SecondaryRule_WN_EWBF tSecondaryRule_WN_EWBF = new SecondaryRule_WN_EWBF();
		
		System.out.println(tSecondaryRule_WN_EWBF.prepareRule());
		//tSecondaryRule_YD.ruleExe(cInputData, cOperate)
		
	}

}
