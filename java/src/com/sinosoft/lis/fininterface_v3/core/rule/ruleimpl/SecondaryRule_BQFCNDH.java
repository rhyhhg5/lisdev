package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/**
 * <p>Title: 二次处理规则--保全退保年度化反冲（51前和共保数据处理）</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author liusc
 * @version 3.0
 */
public class SecondaryRule_BQFCNDH extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

//a.SerialNo和a.ASerialNo改为空取值，a.ContNo,a.PolNo该取值000000,添加了distinct, 原来都是直接取得原数据的
//	考虑共保
		tSQL.append(" select distinct '' as SerialNo ,a.NoType,'' as ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType," +
				"a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,'000000' as ContNo,'000000' as PolNo," +
				"a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType," +
				"a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl," +
				"a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years," +
				"a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,-LF_PayMoney(a.GrpPolNo, (select b.PayToDate from lbgrppol b where a.grpcontno=b.grpcontno and a.riskcode=b.riskcode), (select b.PayEndDate from lbgrppol b where a.grpcontno=b.grpcontno and a.riskcode=b.riskcode), a.PayIntv) SumActuMoney,a.MothDate,a.BusinessDate," +
				"a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02," +
				"a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate," +
				"a.MakeTime from FIAbStandardData a where a.NoType = '2' and a.state = '00' and a.CheckFlag='00' and ListFlag='2' and a.BusinessType = '04' and a.Businessdetail='042' " +
				" and a.riskperiod in ('M','S') and a.payintv not in (0,12,-1) and a.FeeType in ('WT','XT','CT') " +
				" and exists (select 1 from LCCoInsuranceParam lcc where lcc.grpcontno = a.grpcontno fetch first 1 rows only )"+
				" and not exists(select 1 from FIAbStandardData c where c.costid = 'F0000000025' and c.BusinessDetail='103' and a.indexcode = c.indexcode and a.indexno = c.indexno ) " +
				" and exists (select 1 from ljapay d where d.incomeno = a.grpcontno and d.duefeetype = '0' " +
				" and exists (select 1 from ljapaygrp lj where d.payno = lj.payno and (lj.lastpaytodate >= '2016-05-01' or lj.confdate >='2016-05-01')))" );
		
		tSQL.append(" union all ");
		
		tSQL.append(" select distinct '' as SerialNo ,a.NoType,'' as ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType," +
				"a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,'000000' as ContNo,'000000' as PolNo," +
				"a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType," +
				"a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl," +
				"a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years," +
				"a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,-LF_PayMoney(a.GrpPolNo, (select b.PayToDate from lbgrppol b where a.grpcontno=b.grpcontno and a.riskcode=b.riskcode), (select b.PayEndDate from lbgrppol b where a.grpcontno=b.grpcontno and a.riskcode=b.riskcode), a.PayIntv ) SumActuMoney,a.MothDate,a.BusinessDate," +
				"a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02," +
				"a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate," +
				"a.MakeTime from FIAbStandardData a where a.NoType = '2' and a.state = '00' and a.CheckFlag='00' and ListFlag='2' and a.BusinessType = '04' and a.Businessdetail='042' " +
				" and a.riskperiod in ('M','S') and a.payintv not in (0,12,-1) and a.FeeType in ('WT','XT','CT') " +
				" and not exists(select 1 from FIAbStandardData c where c.costid = 'F0000000025' and c.BusinessDetail='103' and a.indexcode = c.indexcode and a.indexno = c.indexno ) " +
				" and exists (select 1 from ljapay d where d.incomeno = a.grpcontno and d.duefeetype = '0'" +
				" and exists (select 1 from ljapaygrp lj where d.payno = lj.payno and lj.lastpaytodate < '2016-05-01' and lj.confdate <'2016-05-01'))" );
				
		
		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				//短期险非趸缴非年交 年度化保费
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
//				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setBusinessType("10");//
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				tFIAbStandardDataSchema.setBusinessDetail("103");//
				
				tFIAbStandardDataSchema.setCostID("F0000000025");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//
				if("1".equals(tFIAbStandardDataSchema.getRiskType())
						||"3".equals(tFIAbStandardDataSchema.getRiskType()))
				{
					//万能 新特需
					tFIAbStandardDataSchema.setBusTypeID("Y-BQ-FC-000004");
				}
				else if("2".equals(tFIAbStandardDataSchema.getRiskType()))
				{
					//老特需
					if("1605".equals(tFIAbStandardDataSchema.getRiskCode())
							||"170106".equals(tFIAbStandardDataSchema.getRiskCode()))
					{
						tFIAbStandardDataSchema.setBusTypeID("Y-BQ-FC-000003");
					}
					else
					{
						tFIAbStandardDataSchema.setBusTypeID("Y-BQ-FC-000004");
					}
				}
				else
				{
					//传统险
					tFIAbStandardDataSchema.setBusTypeID("Y-BQ-FC-000003");
				}
				
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_BQFCNDH tSecondaryRule_BQFCNDH = new SecondaryRule_BQFCNDH();
		
		
		//tSecondaryRule_YD.ruleExe(cInputData, cOperate)
		
	}

}
