package com.sinosoft.lis.fininterface_v3.core.rule;

import com.sinosoft.lis.schema.FIDataExtractRulesSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title:规则接口</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public interface Rule {
	   /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();	
	/**
	 * 执行规则
	 * @return
	 */
	public boolean ruleExe( VData cInputData,FIDataExtractRulesSchema cFIDataExtractRulesSchema);
	
	/**
	 * 规则返回信息
	 * @return
	 */
	public VData getResult();
}
