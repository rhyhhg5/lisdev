
package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--续期约定交费应收反冲_共保</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class SecondaryRule_YDJFXQFC_GB extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

	tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType," +
				"a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo," +
				"a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType," +
				"a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl," +
				"a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years," +
				"a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo," +
				"cast(round(-a.sumactumoney*(select sum(rate) from LCCoInsuranceParam c where c.grpcontno = a.grpcontno),2) as decimal(30,2)) as SumActuMoney," +
				"a.MothDate,a.BusinessDate," +
				"a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02," +
				"a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate," +
				"a.MakeTime from FIAbStandardData a,LCGrpCont b where a.grpcontno = b.grpcontno and b.CoInsuranceFlag = '1' " +
				" and a.NoType = '2' " +
				"and a.state = '00' " +
				"and a.BusinessType = '10' and a.Businessdetail='102' and a.payintv =-1 " +
				"and (exists(select 1 from FIAbStandardData d where  d.indexno in (select payno  from LJApay where incomeno = a.grpcontno and duefeetype = '0' ) and a.grpcontno = d.grpcontno " +
				"and d.BusinessType = '03' and d.Businessdetail = '031'  and d.paycount = '1' " +
				"and d.accountdate >= '2014-1-1' fetch first 1 row only) or exists(select 1 from FIAbStandardData d where a.grpcontno = d.grpcontno " +
				"and d.BusinessType = '03' and d.Businessdetail = '031'  and d.paycount = '1' " +
				"and d.accountdate <= '2014-1-1' and d.grpcontno = '00183639000001' fetch first 1 row only))" +
				"and a.CheckFlag='00' " +
				" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusTypeID='O-GB-XQ-000001' ) " +
				"union all" +
				" select a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType," +
				"a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo," +
				"a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType," +
				"a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl," +
				"a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years," +
				"a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo," +
				"cast(round(-a.sumactumoney*(select sum(rate) from LCCoInsuranceParam c where c.grpcontno = a.grpcontno),2) as decimal(30,2)) as SumActuMoney," +
				"a.MothDate,a.BusinessDate," +
				"a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02," +
				"a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate," +
				"a.MakeTime from FIAbStandardData a,LCGrpCont b where a.grpcontno = b.grpcontno and b.CoInsuranceFlag = '1' " +
				" and a.NoType = '0' " +
				"and a.state = '00' " +
				"and a.BusinessType = '01' and a.Businessdetail='016' and a.payintv =-1 " +
				"and (exists(select 1 from FIAbStandardData d where d.indexno in (select payno  from LJApay where incomeno = a.grpcontno and duefeetype = '0' ) and a.grpcontno = d.grpcontno " +
				"and d.BusinessType = '03' and d.Businessdetail = '031'  and d.paycount = '1' " +
				"and d.accountdate >= '2014-1-1' fetch first 1 row only) or exists(select 1 from FIAbStandardData d where a.grpcontno = d.grpcontno " +
				"and d.BusinessType = '03' and d.Businessdetail = '031'  and d.paycount = '1' " +
				"and d.accountdate <= '2014-1-1' and d.grpcontno = '00183639000001' fetch first 1 row only))" +
				"and a.CheckFlag='00' " +
				" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusTypeID='O-GB-XQ-000001' )");
		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				tFIAbStandardDataSchema.setBusinessType("20");//应收反冲_共保
				tFIAbStandardDataSchema.setBusinessDetail("302");//续期约定缴费反冲_共保
				tFIAbStandardDataSchema.setBusTypeID("O-GB-XQ-000001");
				tFIAbStandardDataSchema.setCostID("GB000000025");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//	
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_YDJFXQFC_GB tSecondaryRule_YDJFXQYS_GB = new SecondaryRule_YDJFXQFC_GB();
		System.out.println(tSecondaryRule_YDJFXQYS_GB.prepareRule());
		
	}

}



