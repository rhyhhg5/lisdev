
package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--反冲年度化_共保</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author liusc
 * @version 3.0
 */
public class SecondaryRule_FCNDH_GB extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

		tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType," +
				"a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo," +
				"a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType," +
				"a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl," +
				"a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years," +
				"a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo," +
				"cast(round(-SumActuMoney*(select sum(rate) from LCCoInsuranceParam b where b.grpcontno = a.grpcontno),2) as decimal(30,2)) as  SumActuMoney," +
				"a.MothDate,a.BusinessDate," +
				"a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02," +
				"a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate," +
				"a.MakeTime from FIAbStandardData a,LCGrpCont b where  a.grpcontno = b.grpcontno and b.CoInsuranceFlag = '1' and a.NoType = '2' and a.state = '00' and ListFlag='2' and a.BusinessType = '10' and a.Businessdetail='102' " +
				" and a.riskperiod in ('M','S') and a.payintv not in (0,12,-1) " +
				"and exists(select 1 from FIAbStandardData d where a.grpcontno = d.grpcontno " +
				"and d.BusinessType = '03' and d.Businessdetail = '031'  and d.paycount = '1' " +
				"and d.accountdate >= '2014-1-1' fetch first 1 row only) " +
				"and a.CheckFlag='00' " +
				" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusTypeID = 'O-GB-XQ-000001' ) ");
		
		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				tFIAbStandardDataSchema.setBusinessType("20");//共保应收
				tFIAbStandardDataSchema.setBusinessDetail("302");//续期年度化反冲_共保
				tFIAbStandardDataSchema.setBusTypeID("O-GB-XQ-000001");
				tFIAbStandardDataSchema.setCostID("GB000000025");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//	
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
//		try 
//		{
//			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
//			
//			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
//			{
//				//短期险非趸缴非年交 续期反冲年度化
//				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
//				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());	//二次处理关联号	
//				tFIAbStandardDataSchema.setBusinessType("10");	//业务类型
//				tFIAbStandardDataSchema.setSecondaryFlag("1");	//二次处理标记
//				tFIAbStandardDataSchema.setBusinessDetail("102");
//				tFIAbStandardDataSchema.setCostID("F0000000025");	//费用编码
//				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;
//				
//				if("1".equals(tFIAbStandardDataSchema.getRiskType())
//						||"3".equals(tFIAbStandardDataSchema.getRiskType()))
//				{
//					//万能 新特需
//					tFIAbStandardDataSchema.setBusTypeID("O-XQ-000010");
//				}
//				else if("2".equals(tFIAbStandardDataSchema.getRiskType()))
//				{
//					//老特需
//					if("1605".equals(tFIAbStandardDataSchema.getRiskCode())
//							||"170106".equals(tFIAbStandardDataSchema.getRiskCode()))
//					{
//						tFIAbStandardDataSchema.setBusTypeID("O-XQ-000008");
//					}
//					else
//					{
//						tFIAbStandardDataSchema.setBusTypeID("O-XQ-000010");
//					}
//				}
//				else
//				{
//					//传统险
//					tFIAbStandardDataSchema.setBusTypeID("O-XQ-000008");//
//				}
//			}	
//		} 
//		catch (Exception e1) 
//		{
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_FCNDH_GB tSecondaryRule_FCNDH_GB = new SecondaryRule_FCNDH_GB();
		System.out.println(tSecondaryRule_FCNDH_GB.prepareRule());
		
	}

}
