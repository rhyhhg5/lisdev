package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.utility.VData;
/**
 * <p>Title: 二次处理规则--续期约定交费反冲净保费</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author hdl
 * @version 3.0
 */
public class SecondaryRule_YDJF_FCXQBF extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

		tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType," +
				"a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo," +
				"a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType," +
				"a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl," +
				"a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years," +
				"a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo," +
				"(case when sumactumoney>=0 then LF_YD_MONEYBF(a.indexno,a.riskcode,a.grpcontno,b.getnoticeno,'XQ') else -LF_YD_MONEYBF(a.indexno,a.riskcode,a.grpcontno,b.getnoticeno,'XQ') end) SumActuMoney,a.MothDate,a.BusinessDate," +
				"a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02," +
				"a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate," +
				"a.MakeTime from FIAbStandardData a,ljapay b where a.indexno=b.payno and BusTypeID='O-XQ-000001' and costid='F00000000BF' " +
				" and a.NoType = '2' and a.state = '00' and a.BusinessType = '03' and a.Businessdetail='032' and a.payintv =-1 and a.CheckFlag='00' and a.feetype not in ('ZE') " +
				" and not exists (select 1 from LCCoInsuranceParam lcc where lcc.grpcontno = a.grpcontno fetch first 1 rows only )"+
				" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusTypeID = 'O-XQ-000008' and c.CostID = 'F0000000025') " +
				" and exists (select 1 from FIAbStandardData d where d.indexno in (select distinct e.payno from ljapay e where e.incomeno=a.grpcontno and e.duefeetype='0' and e.incometype ='1') and d.BusinessType = '03' and d.Businessdetail = '031'  and d.paycount = '1'" +
				" and d.accountdate >= '2016-5-1' fetch first 1 row only)");
		
		tSQL.append(" union all ");
		
		tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType," +
				"a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo," +
				"a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType," +
				"a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl," +
				"a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years," +
				"a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo," +
				"(case when sumactumoney>=0 then LF_YD_MONEY(a.indexno,a.riskcode,a.grpcontno,b.getnoticeno,'XQ') else -LF_YD_MONEY(a.indexno,a.riskcode,a.grpcontno,b.getnoticeno,'XQ') end) SumActuMoney,a.MothDate,a.BusinessDate," +
				"a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02," +
				"a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate," +
				"a.MakeTime from FIAbStandardData a,ljapay b where a.indexno=b.payno and BusTypeID='O-XQ-000001' and costid='F00000000BF' " +
				" and a.NoType = '2' and a.state = '00' and a.BusinessType = '03' and a.Businessdetail='032' and a.payintv =-1 and a.CheckFlag='00' and a.feetype not in ('ZE') " +
				" and exists (select 1 from LCCoInsuranceParam lcc where lcc.grpcontno = a.grpcontno fetch first 1 rows only )"+
				" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusTypeID = 'O-XQ-000008' and c.CostID = 'F0000000025')");

		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				tFIAbStandardDataSchema.setBusinessType("10");//应收计提
				tFIAbStandardDataSchema.setBusinessDetail("102");//续期约定缴费应收反冲
				tFIAbStandardDataSchema.setBusTypeID("O-XQ-000008");
				tFIAbStandardDataSchema.setCostID("F0000000025");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_YDJF_FCXQBF tSecondaryRule_YDJF_FCXQBF = new SecondaryRule_YDJF_FCXQBF();
		System.out.println(tSecondaryRule_YDJF_FCXQBF.prepareRule());
		
	}

}

