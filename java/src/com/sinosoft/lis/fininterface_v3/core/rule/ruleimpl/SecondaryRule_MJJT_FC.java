
package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import java.util.Date;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/**
 * <p>Title: 二次处理规则--满期金计提数据反冲</p>
 *
 * <p>Description: 当满期数据抽档后反冲掉原来满期计提的数据</p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author liusc
 * @version 3.0
 */
public class SecondaryRule_MJJT_FC extends StandSecondaryRule{

	
	public String prepareRule()
	{
		System.out.println("满期计提开始---、、、");
		String tDate = null;	
		tDate = PubFun.getCurrentDate();
		StringBuffer tSQL = new StringBuffer();

		String sqlBJ = "select distinct contno from fiabstandarddata where businesstype = '04' and businessdetail = '042' and makedate = '"+tDate+"' " +
				       " and bustypeid = 'Y-BQ-OO-000001' and listflag = '1' and indexcode = '03' and state = '00' and costid in ('F00000000MJ','F000000HAMJ','F000000EBMJ')";
		
		String a = null; String b = null; String c = null;int j;
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(sqlBJ);
		StringBuffer buffer = new StringBuffer();
		int maxRow = tSSRS.getMaxRow();
		if (maxRow != 0){			 
		if (maxRow == 1){			
			c = "'"+tSSRS.GetText(1, 1)+"'";
		}else{
			for (j=1;j<tSSRS.getMaxRow();j++){				

				a = buffer.append("'"+tSSRS.GetText(j, 1)+"',").toString();
			}
	        if (j == tSSRS.getMaxRow()){  
	        	
	          	 b = "'"+tSSRS.GetText(tSSRS.getMaxRow(), 1)+"'"; 
	           }
			 c = a + b;
			 System.out.println(c);
		}
	       			
			tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType,");
			tSQL.append(" a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,");
			tSQL.append(" a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType,");
			tSQL.append(" a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl,");
			tSQL.append(" a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years,");
			tSQL.append(" a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,");
			tSQL.append(" -SumActuMoney as SumActuMoney,");
			tSQL.append(" a.MothDate,a.BusinessDate,a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02,");
			tSQL.append(" a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,'"+PubFun.getCurrentDate()+"' as MakeDate,'"+PubFun.getCurrentTime()+"' as maketime");
			tSQL.append(" from FIAbStandardData a where a.bustypeid = 'Y-BQ-OO-00000J' and a.BusinessType = '04' and a.Businessdetail='045' ");
			tSQL.append(" and a.indexcode='03' and a.SecondaryFlag = '0' and a.CheckFlag='00' and a.costid in ('F0000000044','F00000000MJ','F000000EBMJ')");
			tSQL.append(" and a.contno in ("+c+")");
			tSQL.append(" and not exists(select 1 from FIAbStandardData f where a.SerialNo = f.RelatedNo and a.contno = f.contno and f.SecondaryFlag = '1' and f.bustypeid = 'Y-BQ-OO-0000JJ')");
		}else{
			tSQL.append("select 1 from FIAbStandardData where 1=2 ");
		}
		
        return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			//提数日期
			String tDate = null;	
			tDate = PubFun.getCurrentDate();
			System.out.print("hahahahha"+tDate);
			
			String sqldate = "select distinct a.accountdate,a.eventno from FIAbStandardData a where a.businesstype = '04' and a.businessdetail = '042'"+
		               " and a.indexcode='03' and a.bustypeid = 'Y-BQ-OO-000001' and a.listflag = '1' and a.state = '00' and a.costid in ('F0000000044','F00000000MJ','F000000HAMJ','F000000EBMJ') " +
		               " and exists (select 1 from FIAbStandardData b where b.bustypeid = 'Y-BQ-OO-00000J' and b.BusinessType = '04' and b.Businessdetail='045' and b.indexcode='03' and a.contno = b.contno)" +
		               " and a.makedate = '"+tDate+"' fetch first 1 rows only" ;  
					 
					
			SSRS mSSRS = new SSRS();
			ExeSQL tExeSQL = new ExeSQL();
			mSSRS = tExeSQL.execSQL(sqldate);
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				System.out.println("getSerialNo()--"+tFIAbStandardDataSchema.getSerialNo());
					tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
					tFIAbStandardDataSchema.setSecondaryFlag("1");
					tFIAbStandardDataSchema.setBusTypeID("Y-BQ-OO-0000JJ");
					tFIAbStandardDataSchema.setState("00");
					tFIAbStandardDataSchema.setReadState("0");
					tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;					
					String adate = mSSRS.GetText(1,1);
					String aeventno = mSSRS.GetText(1,2);
					tFIAbStandardDataSchema.setEventNo(aeventno);
					tFIAbStandardDataSchema.setAccountDate(adate);
			}
        }
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_MJJT_FC tSecondaryRule_MJJT_FC = new SecondaryRule_MJJT_FC();
		tSecondaryRule_MJJT_FC.prepareRule();
		
	}
	

}
