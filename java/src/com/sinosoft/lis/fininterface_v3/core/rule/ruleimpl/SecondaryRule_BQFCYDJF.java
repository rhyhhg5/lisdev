package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/**
 * <p>Title: 二次处理规则--保全约定交费反冲</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class SecondaryRule_BQFCYDJF extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();
		String sqlTB = "select distinct a.grpcontno from FIAbStandardData a where a.NoType = '2' and a.payintv =-1 and a.state='00' and a.CheckFlag='00' "+
        " and a.BusinessType = '04' and a.Businessdetail='042' and a.feetype in ('WT','XT','CT') " +
        " and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusTypeID='Y-BQ-FC-000002' )";

		String a = null; String b = null; String c = null;int j;
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(sqlTB);
		System.out.println("tSSRS"+tSSRS);
		StringBuffer buffer = new StringBuffer();
		int maxRow = tSSRS.getMaxRow();
		if (maxRow != 0){			 
			if (maxRow == 1){			
				c = "'"+tSSRS.GetText(1, 1)+"'";
			}else{
				for (j=1;j<tSSRS.getMaxRow();j++){				

					a = buffer.append("'"+tSSRS.GetText(j, 1)+"',").toString();
				}
				if (j == tSSRS.getMaxRow()){  
	
					b = "'"+tSSRS.GetText(tSSRS.getMaxRow(), 1)+"'"; 
				}
				c = a + b;
				//System.out.println(c);
			}

		tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType," +
				"a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo," +
				"a.EndorsementNo,a.NotesNo,'S' FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType," +
				"a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl," +
				"a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years," +
				"a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,-b.prem SumActuMoney,a.MothDate,a.BusinessDate," +
				"a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02," +
				"a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate," +
				"a.MakeTime from lcgrppayplandetail_FC b,FIAbStandardData a where a.grpcontno = b.grpcontno and a.riskcode = b.riskcode and b.state = '2' and b.prem <> 0 " +
				" and a.NoType = '2' and a.payintv =-1  and a.state='00' and a.CheckFlag='00' and a.BusinessType = '04' and a.Businessdetail='042' and a.grpcontno in ("+c+")" +
				" and not exists(select 1 from LCGrpPayDue where prtno=b.prtno) and a.feetype in ('WT','XT','CT')"+
//				" and exists (select 1 from FIAboriginalGenAtt d where a.ASerialNo=d.ASerialNo and d.feetype in ('WT','XT','CT')) " +
				" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusTypeID='Y-BQ-FC-000002' ) " +
				" and exists (select 1 from FIAbStandardData f where a.grpcontno = f.grpcontno and f.BusinessType = '03' and f.Businessdetail = '031'  and f.paycount = '1'" +
				" and f.accountdate < '2016-5-1' fetch first 1 row only)");
		}else{
			tSQL.append("select 1 from FIAbStandardData where 1=2 ");
		}		
		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{

				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setBusinessType("10");//应收计提
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				tFIAbStandardDataSchema.setBusinessDetail("107");//
				tFIAbStandardDataSchema.setBusTypeID("Y-BQ-FC-000002");
				tFIAbStandardDataSchema.setCostID("F0000000025");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_BQFCYDJF tSecondaryRule_BQFCYDJF = new SecondaryRule_BQFCYDJF();
		System.out.println(tSecondaryRule_BQFCYDJF.prepareRule());
		
	}

}
