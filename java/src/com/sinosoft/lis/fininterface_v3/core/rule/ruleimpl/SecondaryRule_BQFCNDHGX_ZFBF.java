package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--保全保单终止缴费个险短险普通险年度化反冲(保费)</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company:sinosoft </p>
 * @author gcx
 * @version 3.0
 */
public class SecondaryRule_BQFCNDHGX_ZFBF extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

        String tStartDate = (String)mTransferData.getValueByName("StartDate");
        String tEndDate = (String)mTransferData.getValueByName("EndDate");
        System.out.println("保全保单终止缴费个险年度化反冲(保费)");
        tSQL.append("select '' as SerialNo,'0' as NoType ,'' as ASerialNo ,'1' as SecondaryFlag ,'ZF' as SecondaryType ,'Y-BQ-FC-000003' as BusTypeID ,'F0000000025' as CostID ,'10' as BusinessType ,'103' as BusinessDetail ,'ZF' as FeeType ,'ZF' as FeeDetail ,'03' as IndexCode ,b.Edoracceptno as IndexNo ,'1' ListFlag ,'' as GrpContNo ,'' as GrpPolNo ,b.contno as ContNo ,c.polno as PolNo ,b.Edoracceptno as EndorsementNo ," +
        						"'' as NotesNo ,'X' as FirstYearFlag ,LF_FIRSTTERMFLAG2(a.confdate, b.ContNo) as FirstTermFlag ,'' as BonusType ,'0' as DifComFlag ,c.RiskCode ,'' as RiskPeriod ,'0' as RiskType ,'' as RiskType1 ,'' as RiskType2 ,(select costcenter from labranchgroup where agentgroup = c.agentgroup fetch first 1 rows only) as CostCenter ,(select actype from lacom where agentcom=c.agentcom fetch first 1 rows only) as ACType ," +
        						"c.AgentCom ,c.AgentGroup ,c.AgentCode ,LF_BClient('2', c.ContNo) as CustomerID ,'' as SupplierNo ,c.SaleChnl ,c.SaleChnlDetail ,b.ManageCom ,b.ManageCom as ExecuteCom ,c.CValiDate ,c.PayIntv ,cast(null as date) as LastPayToDate ,cast(null as date) as CurPayToDate ,LF_PolYear(c.PayIntv, c.PayYears) as PolYear ,LF_Years(c.InsuYearFlag, c.InsuYear, c.Years) as Years ,LF_PremiumType(c.RiskCode, 'ZC') as PremiumType ," +
        						"0 as PayCount ,'' as PayMode ,'' as BankCode ,'' as AccName ,'' as BankAccNo ," +
        						"-LF_PAYMONEY6(c.PolNo,c.PayToDate,c.PayEndDate,c.PayIntv,'BF') as SumActuMoney ," +
        						"cast(null as date) as MothDate ,a.ConfDate as BusinessDate ,a.ConfDate as AccountDate ,'' as CashFlowNo ,LF_FIRSTYEAR(b.contno) as FirstYear ,LF_MarketType(c.ContNo) as MarketType ,'' as OperationType ,'' as Budget ,'CNY' as Currency ,'' as StringInfo01 ," +
        						"'' as StringInfo02 ,'' as StringInfo03 ,cast(null as date) as DateInfo01 ,cast(null as date) as DateInfo02 ,'' as RelatedNo,'' as EventNo,'00' as State, '0' as ReadState, '00' as CheckFlag,'"+mGlobalInput.Operator+"' as Operator,'"+PubFun.getCurrentDate()+"' as MakeDate ,'"+PubFun.getCurrentTime()+"' as MakeTime " +
        						"from lpedorapp a, lpedoritem b, lcpol c " +
        						"where a.edoracceptno = b.edorno and b.contno = c.contno and c.conttype = '1' and a.edorstate = '0' and b.edortype = 'ZF' " +
        						"and LF_PAYMONEY6(c.PolNo,c.PayToDate,c.PayEndDate,c.PayIntv,'BF')<>0 and a.confdate between '"+tStartDate+"' and '"+tEndDate+"' " +
        						"and exists (select 1 from lcpol where contno = b.contno and polstate = '03050002' and payintv not in (0, 12, -1) and conttype = '1') " +
        						"and exists (select 1 from lmriskapp where riskcode = c.riskcode and riskcode not in(select distinct riskcode from lmrisk where rnewflagb ='Y' ))" +
        						"and exists (select 1 from ljapay d where d.incomeno = c.contno and d.duefeetype = '0')" +
 										"and not exists(select 1 from FIAbStandardData f where f.indexcode='03' and f.indexno=b.Edoracceptno and f.contno=b.contno and f.polno=c.polno and f.BusTypeID = 'Y-BQ-FC-000003' and f.CostID ='F0000000025' and f.FeeType='ZF' ) ");
		
        tSQL.append(" union all ");
		
        tSQL.append(" select '' as SerialNo,'0' as NoType ,'' as ASerialNo ,'1' as SecondaryFlag ,'ZF' as SecondaryType ,'Y-BQ-FC-000003' as BusTypeID ,'F0000000025' as CostID ,'10' as BusinessType ,'103' as BusinessDetail ,'ZF' as FeeType ,'ZF' as FeeDetail ,'03' as IndexCode ,b.Edoracceptno as IndexNo ,'1' ListFlag ,'' as GrpContNo ,'' as GrpPolNo ,b.contno as ContNo ,c.polno as PolNo ,b.Edoracceptno as EndorsementNo ," +
        						"'' as NotesNo ,'X' as FirstYearFlag ,LF_FIRSTTERMFLAG2(a.confdate, b.ContNo) as FirstTermFlag ,'' as BonusType ,'0' as DifComFlag ,c.RiskCode ,'' as RiskPeriod ,'0' as RiskType ,'' as RiskType1 ,'' as RiskType2 ,(select costcenter from labranchgroup where agentgroup = c.agentgroup fetch first 1 rows only) as CostCenter ,(select actype from lacom where agentcom=c.agentcom fetch first 1 rows only) as ACType ," +
        						"c.AgentCom ,c.AgentGroup ,c.AgentCode ,LF_BClient('2', c.ContNo) as CustomerID ,'' as SupplierNo ,c.SaleChnl ,c.SaleChnlDetail ,b.ManageCom ,b.ManageCom as ExecuteCom ,c.CValiDate ,c.PayIntv ,cast(null as date) as LastPayToDate ,cast(null as date) as CurPayToDate ,LF_PolYear(c.PayIntv, c.PayYears) as PolYear ,LF_Years(c.InsuYearFlag, c.InsuYear, c.Years) as Years ,LF_PremiumType(c.RiskCode, 'ZC') as PremiumType ," +
        						"0 as PayCount ,'' as PayMode ,'' as BankCode ,'' as AccName ,'' as BankAccNo ," +
        						"-LF_PAYMONEY6(c.PolNo,c.PayToDate,c.PayEndDate,c.PayIntv,'BF') as SumActuMoney ," +
        						"cast(null as date) as MothDate ,a.ConfDate as BusinessDate ,a.ConfDate as AccountDate ,'' as CashFlowNo ,LF_FIRSTYEAR(b.contno) as FirstYear ,LF_MarketType(c.ContNo) as MarketType ,'' as OperationType ,'' as Budget ,'CNY' as Currency ,'' as StringInfo01 ,'' as StringInfo02 ,'' as StringInfo03 ," +
        						"cast(null as date) as DateInfo01 ,cast(null as date) as DateInfo02 ,'' as RelatedNo,'' as EventNo,'00' as State,'0' as ReadState,'00' as CheckFlag,'"+mGlobalInput.Operator+"' as Operator,'"+PubFun.getCurrentDate()+"' as MakeDate ,'"+PubFun.getCurrentTime()+"' as MakeTime " +
        						"from lpedorapp a, lpedoritem b, lbpol c " +
        						"where a.edoracceptno = b.edorno and b.contno = c.contno and c.conttype ='1' and a.edorstate = '0' and b.edortype = 'ZF' and LF_PAYMONEY6(c.PolNo,c.PayToDate,c.PayEndDate,c.PayIntv,'BF')<>0 " +
        						"and a.confdate between '"+tStartDate+"' and '"+tEndDate+"' " +
        						"and exists (select 1 from lbpol where contno = b.contno and polstate = '03050002' and conttype = '1' and payintv not in (0, 12, -1)) " +
        						"and exists (select 1 from lmriskapp where riskcode = c.riskcode and riskcode not in(select distinct riskcode from lmrisk where rnewflagb ='Y' ))" +
        						"and exists (select 1 from ljapay d where d.incomeno = c.contno and d.duefeetype = '0')and not exists(select 1 from FIAbStandardData f where f.indexcode='03' and f.indexno=b.Edoracceptno and f.contno=b.contno and f.polno=c.polno and f.BusTypeID = 'Y-BQ-FC-000003' and f.CostID ='F0000000025' and f.FeeType='ZF' ) ");
		
        return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				//个险短险普通险非保证续保期缴终止缴费反冲年度化保费
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tSerialNo[i-1]);
				tFIAbStandardDataSchema.setBusinessType("10");
				tFIAbStandardDataSchema.setBusinessDetail("103");				
				tFIAbStandardDataSchema.setCostID("F0000000025");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_BQFCNDHGX_ZFBF tSecondaryRule_BQFCNDHGX_ZFBF = new SecondaryRule_BQFCNDHGX_ZFBF();
		
	}

}
