package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--保全保单作废年度化反冲(首期在51前)</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author liusc
 * @version 3.0
 */
public class SecondaryRule_BQFCNDH_ZF extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

        String tStartDate = (String)mTransferData.getValueByName("StartDate");
        String tEndDate = (String)mTransferData.getValueByName("EndDate");
        System.out.print("保全保单作废年度化反冲(首期在51前)!!!!!!");
        
        tSQL.append(" select '' as SerialNo,");
		tSQL.append(" '0' as NoType ,");
		tSQL.append(" '' as ASerialNo ,");
		tSQL.append(" '1' as SecondaryFlag ,");
		tSQL.append(" 'ZF' as SecondaryType ,");
		tSQL.append(" 'Y-BQ-FC-000003' as BusTypeID ,");
		tSQL.append(" 'F0000000025' as CostID ,");
		tSQL.append(" '10' as BusinessType ,");
		tSQL.append(" '103' as BusinessDetail ,");
		tSQL.append(" 'ZF' as FeeType ,");
		tSQL.append(" 'ZF' as FeeDetail ,");
		tSQL.append(" '03' as IndexCode ,");
		tSQL.append(" b.Edoracceptno as IndexNo ,");
		tSQL.append(" '2' ListFlag ,");
		tSQL.append(" b.GrpContNo ,");
		tSQL.append(" c.GrpPolNo ,");
		tSQL.append(" '' as ContNo ,");
		tSQL.append(" '' as PolNo ,");
		tSQL.append(" b.Edoracceptno as EndorsementNo ,");
		tSQL.append(" '' as NotesNo ,");
		tSQL.append(" 'X' as FirstYearFlag ,");
		tSQL.append(" LF_FIRSTTERMFLAG2(a.confdate, b.GrpContNo) as FirstTermFlag ,");
		tSQL.append(" '' as BonusType ,");
		tSQL.append(" '0' as DifComFlag ,");
		tSQL.append(" c.RiskCode ,");
		tSQL.append(" '' as RiskPeriod ,");
		tSQL.append(" '0' as RiskType ,");
		tSQL.append(" '' as RiskType1 ,");
		tSQL.append(" '' as RiskType2 ,");
		tSQL.append(" (select costcenter from labranchgroup where agentgroup = c.agentgroup fetch first 1 rows only) as CostCenter ,");
		tSQL.append(" (select actype from lacom where agentcom=c.agentcom fetch first 1 rows only) as ACType ,");
		tSQL.append(" c.AgentCom ,");
		tSQL.append(" c.AgentGroup ,");
		tSQL.append(" c.AgentCode ,");
		tSQL.append(" LF_BClient('2', c.GrpContNo) as CustomerID ,");
		tSQL.append(" '' as SupplierNo ,");//供应商号码
		tSQL.append(" c.SaleChnl ,");
		tSQL.append(" c.SaleChnlDetail ,");
		tSQL.append(" b.ManageCom ,");
		tSQL.append(" b.ManageCom as ExecuteCom ,");
		tSQL.append(" c.CValiDate ,");
		tSQL.append(" c.PayIntv ,");
		tSQL.append(" cast(null as date) as LastPayToDate ,");
		tSQL.append(" cast(null as date) as CurPayToDate ,");
		tSQL.append(" LF_GrpPolYear(c.GrpPolNo) as PolYear ,");
		tSQL.append(" LF_GrpYears(c.GrpPolNo) as Years ,");
		tSQL.append(" LF_PremiumType(c.RiskCode, 'ZC') as PremiumType ,");
		tSQL.append(" 0 as PayCount ,");
		tSQL.append(" '' as PayMode ,");
		tSQL.append(" '' as BankCode ,");
		tSQL.append(" '' as AccName ,");
		tSQL.append(" '' as BankAccNo ,");
		tSQL.append(" -LF_PAYMONEY(c.grppolno, c.paytodate, c.payenddate, c.payintv) as SumActuMoney ,");
		tSQL.append(" cast(null as date) as MothDate ,");
		tSQL.append(" a.ConfDate as BusinessDate ,");
		tSQL.append(" a.ConfDate as AccountDate ,");
		tSQL.append(" '' as CashFlowNo ,");
		tSQL.append(" LF_FIRSTYEAR(b.grpcontno) as FirstYear ,");
		tSQL.append(" LF_MarketType(c.GrpContNo) as MarketType ,");
		tSQL.append(" '' as OperationType ,");
		tSQL.append(" '' as Budget ,");
		tSQL.append(" 'CNY' as Currency ,");
		tSQL.append(" '' as StringInfo01 ,");
		tSQL.append(" '' as StringInfo02 ,");
		tSQL.append(" '' as StringInfo03 ,");
		tSQL.append(" cast(null as date) as DateInfo01 ,");
		tSQL.append(" cast(null as date) as DateInfo02 ,");
		tSQL.append(" '' as RelatedNo,");
		tSQL.append(" '' as EventNo,");
		tSQL.append(" '00' as State,"); //待凭证转换
		tSQL.append(" '0' as ReadState,"); //未同步
		tSQL.append(" '00' as CheckFlag,");
		tSQL.append(" '"+mGlobalInput.Operator+"' as Operator,");
		tSQL.append(" '"+PubFun.getCurrentDate()+"' as MakeDate ,");
		tSQL.append(" '"+PubFun.getCurrentTime()+"' as MakeTime ");
		
		tSQL.append(" from lpedorapp a, lpgrpedoritem b, lcgrppol c ");
		tSQL.append(" where a.edoracceptno = b.edorno and b.grpcontno = c.grpcontno and a.edorstate = '0' and b.edortype = 'ZF' and LF_PAYMONEY(c.grppolno, c.paytodate, c.payenddate, c.payintv) <> 0 ");
		tSQL.append(" and a.confdate between '"+tStartDate+"' and '"+tEndDate+"' ");
		tSQL.append(" and exists (select 1 from lcgrpcont where grpcontno = b.grpcontno and state = '03050002' and payintv not in (0, 12, -1)) " );
		tSQL.append(" and exists (select 1 from lmriskapp where riskcode = c.riskcode and riskperiod in ('S', 'M'))" );
		tSQL.append(" and exists (select 1 from ljapay d where d.incomeno = c.grpcontno and d.duefeetype = '0' " +
		" and exists (select 1 from ljapaygrp lj where d.payno = lj.payno and lj.lastpaytodate < '2016-05-01' and lj.confdate <'2016-05-01'))" );
		tSQL.append(" and not exists(select 1 from FIAbStandardData f where f.indexcode='03' and f.indexno=b.Edoracceptno and f.grpcontno=b.grpcontno and f.grppolno=c.grppolno and f.BusTypeID = 'Y-BQ-FC-000003' and f.CostID ='F0000000025' and f.FeeType='ZF' ) ");
		
		tSQL.append(" union all ");
		
		tSQL.append(" select '' as SerialNo,");
		tSQL.append(" '0' as NoType ,");
		tSQL.append(" '' as ASerialNo ,");
		tSQL.append(" '1' as SecondaryFlag ,");
		tSQL.append(" 'ZF' as SecondaryType ,");
		tSQL.append(" 'Y-BQ-FC-000003' as BusTypeID ,");
		tSQL.append(" 'F0000000025' as CostID ,");
		tSQL.append(" '10' as BusinessType ,");
		tSQL.append(" '103' as BusinessDetail ,");
		tSQL.append(" 'ZF' as FeeType ,");
		tSQL.append(" 'ZF' as FeeDetail ,");
		tSQL.append(" '03' as IndexCode ,");
		tSQL.append(" b.Edoracceptno as IndexNo ,");
		tSQL.append(" '2' ListFlag ,");
		tSQL.append(" b.GrpContNo ,");
		tSQL.append(" c.GrpPolNo ,");
		tSQL.append(" '' as ContNo ,");
		tSQL.append(" '' as PolNo ,");
		tSQL.append(" b.Edoracceptno as EndorsementNo ,");
		tSQL.append(" '' as NotesNo ,");
		tSQL.append(" 'X' as FirstYearFlag ,");
		tSQL.append(" LF_FIRSTTERMFLAG2(a.confdate, b.GrpContNo) as FirstTermFlag ,");
		tSQL.append(" '' as BonusType ,");
		tSQL.append(" '0' as DifComFlag ,");
		tSQL.append(" c.RiskCode ,");
		tSQL.append(" '' as RiskPeriod ,");
		tSQL.append(" '0' as RiskType ,");
		tSQL.append(" '' as RiskType1 ,");
		tSQL.append(" '' as RiskType2 ,");
		tSQL.append(" (select costcenter from labranchgroup where agentgroup = c.agentgroup fetch first 1 rows only) as CostCenter ,");
		tSQL.append(" (select actype from lacom where agentcom=c.agentcom fetch first 1 rows only) as ACType ,");
		tSQL.append(" c.AgentCom ,");
		tSQL.append(" c.AgentGroup ,");
		tSQL.append(" c.AgentCode ,");
		tSQL.append(" LF_BClient('2', c.GrpContNo) as CustomerID ,");
		tSQL.append(" '' as SupplierNo ,");//供应商号码
		tSQL.append(" c.SaleChnl ,");
		tSQL.append(" c.SaleChnlDetail ,");
		tSQL.append(" b.ManageCom ,");
		tSQL.append(" b.ManageCom as ExecuteCom ,");
		tSQL.append(" c.CValiDate ,");
		tSQL.append(" c.PayIntv ,");
		tSQL.append(" cast(null as date) as LastPayToDate ,");
		tSQL.append(" cast(null as date) as CurPayToDate ,");
		tSQL.append(" LF_GrpPolYear(c.GrpPolNo) as PolYear ,");
		tSQL.append(" LF_GrpYears(c.GrpPolNo) as Years ,");
		tSQL.append(" LF_PremiumType(c.RiskCode, 'ZC') as PremiumType ,");
		tSQL.append(" 0 as PayCount ,");
		tSQL.append(" '' as PayMode ,");
		tSQL.append(" '' as BankCode ,");
		tSQL.append(" '' as AccName ,");
		tSQL.append(" '' as BankAccNo ,");
		tSQL.append(" -LF_PAYMONEY(c.grppolno, c.paytodate, c.payenddate, c.payintv) as SumActuMoney ,");
		tSQL.append(" cast(null as date) as MothDate ,");
		tSQL.append(" a.ConfDate as BusinessDate ,");
		tSQL.append(" a.ConfDate as AccountDate ,");
		tSQL.append(" '' as CashFlowNo ,");
		tSQL.append(" LF_FIRSTYEAR(b.grpcontno) as FirstYear ,");
		tSQL.append(" LF_MarketType(c.GrpContNo) as MarketType ,");
		tSQL.append(" '' as OperationType ,");
		tSQL.append(" '' as Budget ,");
		tSQL.append(" 'CNY' as Currency ,");
		tSQL.append(" '' as StringInfo01 ,");
		tSQL.append(" '' as StringInfo02 ,");
		tSQL.append(" '' as StringInfo03 ,");
		tSQL.append(" cast(null as date) as DateInfo01 ,");
		tSQL.append(" cast(null as date) as DateInfo02 ,");
		tSQL.append(" '' as RelatedNo,");
		tSQL.append(" '' as EventNo,");
		tSQL.append(" '00' as State,"); //待凭证转换
		tSQL.append(" '0' as ReadState,"); //未同步
		tSQL.append(" '00' as CheckFlag,");
		tSQL.append(" '"+mGlobalInput.Operator+"' as Operator,");
		tSQL.append(" '"+PubFun.getCurrentDate()+"' as MakeDate ,");
		tSQL.append(" '"+PubFun.getCurrentTime()+"' as MakeTime ");
		
		tSQL.append(" from lpedorapp a, lpgrpedoritem b, lbgrppol c ");
		tSQL.append(" where a.edoracceptno = b.edorno and b.grpcontno = c.grpcontno and a.edorstate = '0' and b.edortype = 'ZF' and LF_PAYMONEY(c.grppolno, c.paytodate, c.payenddate, c.payintv) <> 0 ");
		tSQL.append(" and a.confdate between '"+tStartDate+"' and '"+tEndDate+"' ");
		tSQL.append(" and exists (select 1 from lbgrpcont where grpcontno = b.grpcontno and state = '03050002' and payintv not in (0, 12, -1)) " );
		tSQL.append(" and exists (select 1 from lmriskapp where riskcode = c.riskcode and riskperiod in ('S', 'M'))" );
		tSQL.append(" and exists (select 1 from ljapay d where d.incomeno = c.grpcontno and d.duefeetype = '0' " +
		" and exists (select 1 from ljapaygrp lj where d.payno = lj.payno and lj.lastpaytodate < '2016-05-01' and lj.confdate <'2016-05-01'))" );
		tSQL.append(" and not exists(select 1 from FIAbStandardData f where f.indexcode='03' and f.indexno=b.Edoracceptno and f.grpcontno=b.grpcontno and f.grppolno=c.grppolno and f.BusTypeID = 'Y-BQ-FC-000003' and f.CostID ='F0000000025' and f.FeeType='ZF' ) ");

		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				//短期险非趸缴非年交 年度化保费
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tSerialNo[i-1]);
				tFIAbStandardDataSchema.setBusinessType("10");//
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				tFIAbStandardDataSchema.setBusinessDetail("103");//
				
				tFIAbStandardDataSchema.setCostID("F0000000025");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_BQFCNDH_ZF tSecondaryRule_BQFCNDH_ZF = new SecondaryRule_BQFCNDH_ZF();
		System.out.println(tSecondaryRule_BQFCNDH_ZF.prepareRule()); 
		
	}

}
