package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/**
 * <p>Title: 二次处理规则--约定缴费，实收金额为0</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author liusc
 * @version 3.0
 */
public class SecondaryRule_YD_SS extends StandSecondaryRule{

	
	public String prepareRule()
	{
        String StartDate = mTransferData.getValueByName("StartDate").toString();
        String EndDate = mTransferData.getValueByName("EndDate").toString();
//        System.out.println("RRRRRRRRRRR");      
		StringBuffer tSQL = new StringBuffer();
	    tSQL.append(" select '' as SerialNo,");
		tSQL.append(" '0' as NoType,");
		tSQL.append(" '' as ASerialNo,");
		tSQL.append(" '1' as SecondaryFlag,");
		tSQL.append(" 'YD' as SecondaryType,");
		tSQL.append(" 'O-XQ-000009' as BusTypeID,");
		tSQL.append(" 'F0000000025' as CostID,");
		tSQL.append(" '01' as BusinessType,");
		tSQL.append(" '016' as BusinessDetail,");
		tSQL.append(" '' as FeeType,");
		tSQL.append(" '' as FeeDetail,");		
		tSQL.append(" '02' as IndexCode,");
		tSQL.append(" c.payno as IndexNo,");			
		tSQL.append(" '2' as ListFlag,");
		tSQL.append(" a.GrpContNo as GrpContNo,");
		tSQL.append(" e.grppolno as GrpPolNo, ");
		tSQL.append(" '' as ContNo,");
		tSQL.append(" '' as PolNo,");
		tSQL.append(" '' as EndorsementNo,");
		tSQL.append(" '' as NotesNo,");
		tSQL.append(" 'X' as FirstYearFlag,");
		tSQL.append(" 'X' as FirstTermFlag,");
		tSQL.append(" '' as BonusType,");
		tSQL.append(" '0' as DifComFlag,");
		tSQL.append(" a.riskcode as RiskCode,");
		tSQL.append(" (select RiskPeriod from lmriskapp m where a.riskcode = m.riskcode fetch first 1 rows only) as RiskPeriod,");
		tSQL.append(" '0' as RiskType,");
		tSQL.append(" '' as RiskType1,");
		tSQL.append(" '' as RiskType2,");
		tSQL.append(" (select costcenter from labranchgroup where agentgroup = d.agentgroup fetch first 1 rows only) as CostCenter,");//成本中心
		tSQL.append(" '' as ACType,"); //中介机构类别
		tSQL.append(" d.AgentCom as AgentCom,");
		tSQL.append(" d.AgentGroup as AgentGroup,");
		tSQL.append(" d.AgentCode as AgentCode,");
		tSQL.append(" LF_BClient('2', a.GrpContNo) as CustomerID,");
		tSQL.append(" '' as SupplierNo,");//供应商号码
		tSQL.append(" d.SaleChnl as SaleChnl,");
		tSQL.append(" '' as SaleChnlDetail,");
		tSQL.append(" c.ManageCom as ManageCom,");
		tSQL.append(" '' as ExecuteCom,");
		tSQL.append(" cast(null as date) as CValiDate,");
		tSQL.append(" d.PayIntv as PayIntv,");
		tSQL.append(" cast(null as date) as LastPayToDate,");
		tSQL.append(" cast(null as date) as CurPayToDate,");
		tSQL.append(" LF_GrpPolYear(e.GrpPolNo) as PolYear,");
		tSQL.append(" LF_GrpYears(e.GrpPolNo) as Years,");
		tSQL.append(" LF_PREMIUMTYPE(a.riskcode, 'ZC') as PremiumType,");
		tSQL.append(" 0 as PayCount,");
		tSQL.append(" '' as PayMode,");
		tSQL.append(" '' as BankCode,");
		tSQL.append(" '' as AccName,");
		tSQL.append(" '' as BankAccNo,");
		tSQL.append(" LF_YD_MONEY(c.payno,a.riskcode,a.grpcontno,a.getnoticeno,'XQ') as SumActuMoney,");
		tSQL.append(" cast(null as date) as MothDate,");
		tSQL.append(" cast(null as date) as BusinessDate,");
		tSQL.append(" c.confdate as AccountDate,");
		tSQL.append(" '' as CashFlowNo,");
		tSQL.append(" LF_FIRSTYEAR(c.incomeno) as FirstYear,");//
		tSQL.append(" LF_MarketType(a.GrpContNo) as MarketType,");
		tSQL.append(" '' as OperationType,");
		tSQL.append(" '' as Budget,");
		tSQL.append(" 'CNY' as Currency ,");
		tSQL.append(" '0' as StringInfo01,"); //业务明细
		tSQL.append(" '' as StringInfo02,");
		tSQL.append(" '' as StringInfo03,");
		tSQL.append(" cast(null as date) as DateInfo01,");
		tSQL.append(" cast(null as date) as DateInfo02,");
		tSQL.append(" '' as RelatedNo,");
		tSQL.append(" '' as EventNo,");
		tSQL.append(" '00' as State,"); //待凭证转换
		tSQL.append(" '0' as ReadState,"); //未同步
		tSQL.append(" '00' as CheckFlag,");
		tSQL.append(" '"+mGlobalInput.Operator+"' as Operator,");
		tSQL.append(" '"+PubFun.getCurrentDate()+"' as MakeDate,");
		tSQL.append(" '"+PubFun.getCurrentTime()+"' as MakeTime ");
		tSQL.append(" from LCGrpPayActuDetailV a,lcgrppayplandetailV b,ljapay c,lcgrpcont d,ljapaygrp e " +
				" where a.prtno = b.prtno and a.getnoticeno = c.getnoticeno  and c.payno = e.payno and a.riskcode = e.riskcode and a.riskcode = b.riskcode "+
				" and d.grpcontno = a.grpcontno and c.incometype = '1' and a.plancode = b.plancode and a.prem = 0 and c.sumactupaymoney = 0 and a.state <> '3' and d.payintv = -1 "+
				" and ((d.cvalidate <'2016-05-01' and d.signdate <'2016-05-01') or d.CoInsuranceFlag = '1') ");
		tSQL.append(" and ((exists(select 1 from LCGrpPayDue f where f.prtno=b.prtno and f.plancode = b.plancode and f.ConfState = '01')) " +				
				"or " +
				"(not exists (select 1 from LCGrpPayDue f where f.prtno=b.prtno and f.plancode = b.plancode and f.ConfState = '01'" +
				"and f.plancode in (select max(int(plancode)) from  lcgrppayplandetailV g where b.prtno = g.prtno )" +
				"and exists (select 1 from lcgrppayplandetailV m , LCGrpPayActuDetailV n where m.prtno = n.prtno  and m.plancode = n.plancode " +
				"and n.plancode < b.plancode and m.prtno = b.prtno having  (nvl( sum(n.prem),0) - nvl( sum(m.prem),0)) <0)) " +
				")) ");
		tSQL.append(" and c.Managecom like '86%' and c.confdate >= '"+StartDate+"' and c.confdate <= '"+EndDate+"' " +
				"and not exists(select 1 from FIAbStandardData f where f.grpcontno = c.incomeno and f.SecondaryType='YD' and c.payno=f.indexno and f.CostID ='F0000000025')");
		
		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				//约定缴费-实收金额为0
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tSerialNo[i-1]);
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//		
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_YD_SS tSecondaryRule_YD_SS = new SecondaryRule_YD_SS();
			
		//tSecondaryRule_YD_SS.ruleExe(cInputData, cOperate)
		
	}

}
