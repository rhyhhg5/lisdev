package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import java.text.DecimalFormat;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/**
 * <p>Title: 二次处理规则--契约预计保费回补反冲</p>
 *
 * <p>Description: 保单首次做保费回补操作时，对契约预计保费回补的数据做一次反冲，以保全保费回补操作为准；保单除首次外的保费回补操作不受影响。</p>
 *
 * <p>Copyright: Copyright (c) 2018</p>
 *
 * <p>Company: ZhongKeRuan</p>
 *
 * @author GongCaiXia
 * @version 1.0
 */


public class SecondaryRule_BFHB extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();
		//查询批处理当日是否提取出来了保全保费回补的数据
		String sqlBJ = "select a.accountdate from FIAbStandardData a where a.feetype='BB' and a.NoType = '2' and a.state = '00' and ListFlag='2' and a.BusinessType = '04' and a.Businessdetail='041' " +
				" and a.indexcode='02' and a.endorsementno = (select d.edoracceptno From lpgrpedoritem c,lpedorapp d  " +
										"where c.edortype='BB' and a.grpcontno=c.grpcontno " +
										"and c.edoracceptno=d.edoracceptno " +
										"order by d.confdate,d.makedate,d.maketime fetch first 1 rows only)";
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(sqlBJ);
		int maxRow = tSSRS.getMaxRow();
		if(maxRow != 0){
			tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType,");
			tSQL.append(" a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,");
			tSQL.append(" a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType,");
			tSQL.append(" a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl,");
			tSQL.append(" a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years,");
			tSQL.append(" a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,");
			tSQL.append(" -SumActuMoney as SumActuMoney,");
			tSQL.append(" a.MothDate,a.BusinessDate,a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02,");
			tSQL.append(" a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,'"+PubFun.getCurrentDate()+"' as MakeDate,'"+PubFun.getCurrentTime()+"' as maketime");
			tSQL.append(" from FIAbStandardData a where a.feetype='YGBJ' and a.ListFlag='2' and a.BusinessType = '04' and a.Businessdetail='042' ");
			tSQL.append(" and a.indexcode='03' and a.SecondaryFlag = '0' and a.CheckFlag='00' and feedetail in('DBBFHB','FDBBH') ");
			tSQL.append(" and a.grpcontno in (select distinct grpcontno from FIAbStandardData b where b.feetype='BB' and b.NoType = '2' and b.state = '00' and b.ListFlag='2' and b.BusinessType = '04' and b.Businessdetail='041' ");
			tSQL.append(" and b.indexcode='02' and b.endorsementno = (select d.edoracceptno From lpgrpedoritem c,lpedorapp d ");
			tSQL.append(" where c.edortype='BB' and b.grpcontno=c.grpcontno ");
			tSQL.append(" and c.edoracceptno=d.edoracceptno order by d.confdate,d.makedate,d.maketime fetch first 1 rows only))");
			tSQL.append(" and not exists(select 1 from FIAbStandardData f where a.SerialNo = f.RelatedNo and a.grpcontno = f.grpcontno and f.SecondaryFlag = '1' and f.feetype = 'SSBB_F')");
		}else{
			tSQL.append("select 1 from FIAbStandardData where 1=2 ");
		}

		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			String sqldate = "select distinct a.accountdate,a.grpcontno,a.eventno from FIAbStandardData a where a.feetype='BB' and a.NoType = '2' and a.state = '00' and ListFlag='2' and a.BusinessType = '04' and a.Businessdetail='041' " +
			" and a.indexcode='02' and a.endorsementno = (select d.edoracceptno From lpgrpedoritem c,lpedorapp d  " +
									"where c.edortype='BB' and a.grpcontno=c.grpcontno " +
									"and c.edoracceptno=d.edoracceptno " +
									"order by d.confdate,d.makedate,d.maketime fetch first 1 rows only)  ";
			
			SSRS mSSRS = new SSRS();
			ExeSQL tExeSQL = new ExeSQL();
			mSSRS = tExeSQL.execSQL(sqldate);
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				System.out.println("getSerialNo()--"+tFIAbStandardDataSchema.getSerialNo());
					tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
					tFIAbStandardDataSchema.setSecondaryFlag("1");
					tFIAbStandardDataSchema.setFeeType("SSBB_F");
					tFIAbStandardDataSchema.setState("00");
					tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//	
					for (int row = 1; row <= mSSRS.getMaxRow(); row++)
		            {
//		                for (int col = 1; col <=3; col++)
//		                { 
		                	
		            		String agrpcontno = mSSRS.GetText(row, 2);
							String adate = mSSRS.GetText(row,1);
							String aeventno = mSSRS.GetText(row,3);
							System.out.println("agrpcontno--"+agrpcontno+";adate--"+adate+";aeventno--"+aeventno);
							
							if(tFIAbStandardDataSchema.getGrpContNo().endsWith(agrpcontno)){
								tFIAbStandardDataSchema.setEventNo(aeventno);
								tFIAbStandardDataSchema.setAccountDate(adate);
							}
								
//						}	
		            }
			}
        }
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_BFHB SecondaryRule_BFHB = new SecondaryRule_BFHB();
		
		
		//tSecondaryRule_YD.ruleExe(cInputData, cOperate)
		
	}

}
