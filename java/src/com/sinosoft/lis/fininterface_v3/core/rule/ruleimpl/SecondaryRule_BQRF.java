package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--保单还款特殊处理</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class SecondaryRule_BQRF extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

		tSQL.append(" select * from FIAbStandardData a where a.NoType = '2' and a.state = '00' and a.BusinessType = '01' and  a.BusinessDetail = '013' and CostID in('F0000000RF1','F0000000RF2','F00000SERF2','F00000HLRF2','F00000HLRF1','F000HLSERF2') and a.CheckFlag='00' ");
		tSQL.append(" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.CostID in('F0000000001','F0000000002'))");
		tSQL.append(" and exists(select 1 from FIAboriginalGenAtt b where a.ASerialNo=b.ASerialNo and b.DifComFlag = '0' )");

		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			int tSize = pFIAbStandardDataSet.size();
			int size = pFIAbStandardDataSet.size();
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(size);
			
			for(int i=1;i<=tSize;i++)			
			{
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;
				
				if("1".equals(tFIAbStandardDataSchema.getPayMode()))
				{
					tFIAbStandardDataSchema.setCostID("F0000000001");
				}
				else
				{
					tFIAbStandardDataSchema.setCostID("F0000000002");
				}
				tFIAbStandardDataSchema.setSecondaryFlag("1");
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		SecondaryRule_YDDS tSecondaryRule_YDDS = new SecondaryRule_YDDS();
		
		
		//tSecondaryRule_YD.ruleExe(cInputData, cOperate)
		
	}

}
