package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--保全约定交费反冲（保全保费）</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class SecondaryRule_YDJF_FCTBBF extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

		
		
		tSQL.append(" select distinct '' as SerialNo,a.NoType,'' as ASerialNo,a.SecondaryFlag,'YT'as SecondaryType,a.BusTypeID,a.CostID,a.BusinessType," +
				"a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,'' as ContNo,'' as PolNo," +
				"a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType," +
				"a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl," +
				"a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years," +
				"a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,LF_YD_MONEYBF('',a.riskcode,a.grpcontno,'','TB') SumActuMoney,a.MothDate,a.BusinessDate," +
				"a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02," +
				"a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate," +
				"a.MakeTime from FIAbStandardData a where exists(select 1 from lbgrpcont where grpcontno=a.grpcontno and cvalidate>='2013-01-01') " +
				" and a.NoType = '2' and a.payintv =-1 and a.state = '00' and a.CheckFlag='00' and a.BusinessType = '04' and a.Businessdetail='042' " +
				" and bustypeid='Y-BQ-OO-000001' and costid in('F00000000TB','F00000000TF') and a.feetype not in ('ZE','EE')"+
				" and exists (select 1 from FIAboriginalGenAtt d where a.ASerialNo=d.ASerialNo and d.feetype in ('WT','XT','CT')) " +
				" and not exists (select 1 from LCCoInsuranceParam lcc where lcc.grpcontno = a.grpcontno fetch first 1 rows only )"+
				" and not exists(select 1 from FIAbStandardData c where  c.BusTypeID='Y-BQ-FC-000002' and c.indexno=a.indexno and SecondaryType='YT' and c.costid = 'F0000000025') " +
				" and exists (select 1 from FIAbStandardData d where a.grpcontno = d.grpcontno and d.BusinessType = '03' and d.Businessdetail = '031'  and d.paycount = '1'" +
				" and d.accountdate >= '2016-5-1' fetch first 1 row only)");
				
		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{

				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setBusinessType("10");//应收计提
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				tFIAbStandardDataSchema.setBusinessDetail("107");//
				tFIAbStandardDataSchema.setBusTypeID("Y-BQ-FC-000002");
				tFIAbStandardDataSchema.setCostID("F0000000025");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_YDJF_FCTBBF tSecondaryRule_YDJF_FCTBBF = new SecondaryRule_YDJF_FCTBBF();
		System.out.println(tSecondaryRule_YDJF_FCTBBF.prepareRule());
		
	}

}
