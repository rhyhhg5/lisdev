package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--邮储代收</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class SecondaryRule_YCDS extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

//		tSQL.append(" select * from FIAbStandardData a where a.NoType = '1' and a.state = '00' and a.BusinessType = '01' and a.CheckFlag='00' ");
//		tSQL.append(" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusinessType='07' )");
//		tSQL.append(" and exists(select 1 from FIAboriginalMainAtt b where a.ASerialNo=b.MSerialNo and DifComFlag = '1' and YCFlag='1')");
//
//		tSQL.append(" union all ");

		tSQL.append(" select * from FIAbStandardData a where a.NoType = '2' and a.state = '00' and a.BusinessType = '01' and a.CheckFlag='00' ");
		tSQL.append(" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusinessType='07' )");
		tSQL.append(" and exists(select 1 from FIAboriginalGenAtt b where a.ASerialNo=b.ASerialNo and DifComFlag = '1' and YCFlag='1')");

		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		int tSize = pFIAbStandardDataSet.size();
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=tSize;i++)			
			{
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setBusinessType("07");//异地代收

				String manageCom = tFIAbStandardDataSchema.getManageCom(); //收费机构
				String executeCom = tFIAbStandardDataSchema.getExecuteCom(); //保单机构
				
				tFIAbStandardDataSchema.setManageCom(executeCom);
				tFIAbStandardDataSchema.setExecuteCom(manageCom);
				
				if("011".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
					if ("ZC".equals(tFIAbStandardDataSchema.getFeeDetail())){
					tFIAbStandardDataSchema.setBusinessDetail("071");//新单异地收费
					tFIAbStandardDataSchema.setBusTypeID("DS-NC-000002");//新单邮储\建行代收
					tFIAbStandardDataSchema.setCostID("F000000000P");
					}
					else if ("SE".equals(tFIAbStandardDataSchema.getFeeDetail())){
							tFIAbStandardDataSchema.setBusinessDetail("071");//新单异地收费
							tFIAbStandardDataSchema.setBusTypeID("DS-NC-000002");//新单邮储\建行代收
							tFIAbStandardDataSchema.setCostID("F0000000SEP");						
					}
				}
				else if("012".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
					if ("ZC".equals(tFIAbStandardDataSchema.getFeeDetail())){
					tFIAbStandardDataSchema.setBusinessDetail("072");//续期异地收费
					tFIAbStandardDataSchema.setBusTypeID("DS-XQ-000002");
					tFIAbStandardDataSchema.setCostID("F000000000P");
					}else if ("SE".equals(tFIAbStandardDataSchema.getFeeDetail())){
						
						tFIAbStandardDataSchema.setBusinessDetail("072");//续期异地收费
						tFIAbStandardDataSchema.setBusTypeID("DS-XQ-000002");
						tFIAbStandardDataSchema.setCostID("F0000000SEP");	
					}
				}
				else if("013".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
					tFIAbStandardDataSchema.setBusinessDetail("073");//保全异地收费
					
					if("S-BQ-000002".equals(tFIAbStandardDataSchema.getBusTypeID())&&"RF".equals(tFIAbStandardDataSchema.getFeeType()))
					{	
						tFIAbStandardDataSchema.setBusTypeID("DS-BQ-000003"); //保单质押贷款-还款（异地）
						if("5".equals(tFIAbStandardDataSchema.getRiskType())){
							if("RFBF".equals(tFIAbStandardDataSchema.getFeeDetail()))
							{
								tFIAbStandardDataSchema.setCostID("F00000HLRF1");//保户质押贷款
							}
							else if ("RFLXSE".equals(tFIAbStandardDataSchema.getFeeDetail())){
								
								tFIAbStandardDataSchema.setCostID("F000HLSERF2"); //利息收入的税
							}
							else
							{
								tFIAbStandardDataSchema.setCostID("F00000HLRF2"); //利息收入
							}
						}
						else {
						if("RFBF".equals(tFIAbStandardDataSchema.getFeeDetail()))
						{
							tFIAbStandardDataSchema.setCostID("F0000000RF1");//保户质押贷款
						}
						else if ("RFLXSE".equals(tFIAbStandardDataSchema.getFeeDetail())){
							
							tFIAbStandardDataSchema.setCostID("F00000SERF2"); //利息收入的税
						}
						else
						{
							tFIAbStandardDataSchema.setCostID("F0000000RF2"); //利息收入
						}}
						
					}
					else 
					{
						if ("ZC".equals(tFIAbStandardDataSchema.getFeeDetail())){
						tFIAbStandardDataSchema.setBusTypeID("DS-BQ-000002"); //保全邮储\建行代收
						tFIAbStandardDataSchema.setCostID("F000000000P");
						}else if ("SE".equals(tFIAbStandardDataSchema.getFeeDetail())){
							
								tFIAbStandardDataSchema.setBusTypeID("DS-BQ-000002"); //保全邮储\建行代收
								tFIAbStandardDataSchema.setCostID("F0000000SEP");	
						}
						else if("LRSE".equals(tFIAbStandardDataSchema.getFeeDetail())){
						tFIAbStandardDataSchema.setBusTypeID("DS-BQ-000002"); //保全邮储\建行代收保单遗失补发
						tFIAbStandardDataSchema.setCostID("F00000LRSEP");
						}
                         else if ("FX".equals(tFIAbStandardDataSchema.getFeeDetail())){
						   tFIAbStandardDataSchema.setBusTypeID("DS-BQ-000002"); //保全邮储\建行代收
						   tFIAbStandardDataSchema.setCostID("F0000000FXP");
						 }else if ("FXLX".equals(tFIAbStandardDataSchema.getFeeDetail())){
										
							tFIAbStandardDataSchema.setBusTypeID("DS-BQ-000002"); //保全邮储\建行代收
						   tFIAbStandardDataSchema.setCostID("F0000000LXP");																
						}else if("DBBB".equals(tFIAbStandardDataSchema.getFeeDetail())){
								tFIAbStandardDataSchema.setBusTypeID("DS-BQ-000002");
								tFIAbStandardDataSchema.setCostID("F000000DBBP");					
								}else if("FDBBB".equals(tFIAbStandardDataSchema.getFeeDetail())){
								tFIAbStandardDataSchema.setBusTypeID("DS-BQ-000002");
								tFIAbStandardDataSchema.setCostID("F00000FDBBP");		
								}
					}	
				}
				else if("015".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
					tFIAbStandardDataSchema.setBusinessDetail("075");//理赔异地收费
					tFIAbStandardDataSchema.setBusTypeID("DS-LP-000002");
					tFIAbStandardDataSchema.setCostID("F000000000P");
				}
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;
				
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		SecondaryRule_YCDS tSecondaryRule_YCDS = new SecondaryRule_YCDS();
		
		
		//tSecondaryRule_YD.ruleExe(cInputData, cOperate)
		
	}

}
