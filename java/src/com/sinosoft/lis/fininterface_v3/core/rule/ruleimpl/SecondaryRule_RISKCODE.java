package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.db.FIAbStandardDataDB;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.utility.VData;
/**
 * <p>Title: 二次处理规则--置主表险种</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author liusc
 * @version 3.0 
 */
public class SecondaryRule_RISKCODE extends StandSecondaryRule{

	
	public String prepareRule()
	{              
		System.out.println("主表置险种 ！！！");
		try 
		{
		PubSubmit mPubSubmit = new PubSubmit();
		MMap tMap= new MMap();
		VData tInputData = new VData();
		String tsql=" update FIAbStandardData a set a.riskcode = (select b.riskcode from FIAbStandardData b  where b.indexno = a.indexno and b.notype = '2' and exists (select 1 from ficodetrans fi where fi.codetype = 'WNRiskCode2' and fi.code = b.riskcode)) where a.state = '00' and a.notype = '1' and exists (select 1 from FIAbStandardData fia  where fia.indexno = a.indexno and fia.notype = '2' and exists (select 1 from ficodetrans fi where fi.codetype = 'WNRiskCode2' and fi.code = fia.riskcode)) and a.BusinessType = '03' and a.Businessdetail = '031' and (a.riskcode = '' or a.riskcode is null) ";
		String tsqll=" update FIAbStandardData a set a.riskcode = (select lj.riskcode from ljagetendorse lj where a.indexno = lj.actugetno and lj.feeoperationtype in ('ZB','ZA') and lj.riskcode <> '' and lj.riskcode is not null fetch first 1 rows only) where a.indexcode = '02' and a.state = '00' and a.notype = '1' and a.BusinessType = '04' and a.Businessdetail = '041' and exists (select 1 from ljagetendorse lg where a.indexno = lg.actugetno and lg.feeoperationtype in ('ZB','ZA')) and (a.riskcode = '' or a.riskcode is null)";			
		String tsql2="update FIAbStandardData a set a.riskcode = (select lj.riskcode from ljagetendorse lj where a.indexno = lj.actugetno and lj.feeoperationtype in ('LQ','WT') and lj.riskcode <> '' and lj.riskcode is not null fetch first 1 rows only) where a.indexcode = '03' and a.state = '00' and a.notype = '1' and a.BusinessType = '04' and a.Businessdetail = '042' and exists (select 1 from ljagetendorse lg where a.indexno = lg.actugetno and lg.feeoperationtype in ('LQ','WT')) and (a.riskcode = '' or a.riskcode is null)";
		String tsql3="update FIAbStandardData a set a.riskcode = (select lj.riskcode from ljagetendorse lj where a.indexno = lj.actugetno and a.feetype = lj.feeoperationtype and lj.feeoperationtype in ('CT','XT','WT') and lj.riskcode <> '' and lj.riskcode is not null fetch first 1 rows only) where a.indexcode = '03' and a.state = '00' and a.notype = '1' and a.BusinessType = '04' and a.Businessdetail = '047' and exists (select 1 from ljagetendorse lg where a.indexno = lg.actugetno and lg.feeoperationtype in ('CT','XT','WT')) and (a.riskcode = '' or a.riskcode is null)";
		
		tMap.put(tsql, "UPDATE");
		tMap.put(tsqll, "UPDATE");
		tMap.put(tsql2, "UPDATE");
		tMap.put(tsql3, "UPDATE");
		tInputData.add(tMap);
        if (!mPubSubmit.submitData(tInputData, ""))
        {
        	throw new  Exception("SecondaryRule_RISKCODE:更新错误！");
        	
        }
		}
		catch (Exception e1) 
		{
			e1.printStackTrace();
		}
		StringBuffer tSQL = new StringBuffer();
		tSQL.append("select 1 from fiabstandarddata where 1>2");
		return tSQL.toString();
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		SecondaryRule_RISKCODE tSecondaryRule_RISKCODE = new SecondaryRule_RISKCODE();
		tSecondaryRule_RISKCODE.prepareRule();
	}

}
