
package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--首期年度化_共保</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author liusc
 * @version 3.0
 */
public class SecondaryRule_NDH_GBSQYF extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();



		tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType,");
		tSQL.append(" a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,");
		tSQL.append(" a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType,");
		tSQL.append(" a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl,");
		tSQL.append(" a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years,");
		tSQL.append(" a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,");
		tSQL.append(" cast(round(a.SumActuMoney * (select sum(rate) from LCCoInsuranceParam b where b.grpcontno = a.grpcontno),2) as decimal(30,2)) as SumActuMoney,");
		tSQL.append(" a.MothDate,a.BusinessDate,a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02,");
		tSQL.append(" a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate,");
		tSQL.append(" a.MakeTime from FIAbStandardData a,LCGrpCont b where a.grpcontno = b.grpcontno and b.CoInsuranceFlag = '1' and a.NoType = '2' and a.state = '00' and ListFlag='2' and a.BusinessType = '03' and a.Businessdetail='031' ");
		tSQL.append(" and a.riskperiod in ('M','S') and a.payintv not in (0,12,-1) ");
		tSQL.append(" and exists(select 1 from FIAbStandardData d where a.grpcontno = d.grpcontno " +
				"and d.BusinessType = '03' and d.Businessdetail = '031'  and d.paycount = '1' " +
				"and d.accountdate >= '2014-1-1' fetch first 1 row only) " );
		tSQL.append(" and a.CheckFlag='00'");
		tSQL.append(" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusTypeID = 'GB-N-000002')");
		




		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				tFIAbStandardDataSchema.setBusinessType("20");//共保应付款
				tFIAbStandardDataSchema.setBusinessDetail("206");//首期应付款_共保
				tFIAbStandardDataSchema.setBusTypeID("GB-N-000002");
				tFIAbStandardDataSchema.setCostID("GB000000001");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//	
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
//		try 
//		{
//			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
//			
//			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
//			{
//				//短期险非趸缴非年交 年度化保费
//				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
//				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
//				
//				if("O-XQ-000003".equals(tFIAbStandardDataSchema.getBusTypeID()))
//				{   //续期回退
//					tFIAbStandardDataSchema.setSecondaryFlag("1");
//					tFIAbStandardDataSchema.setCostID("F0000000024");
//					tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//	
//					tFIAbStandardDataSchema.setSumActuMoney(-tFIAbStandardDataSchema.getSumActuMoney());
//				}
//				else
//				{
//					tFIAbStandardDataSchema.setBusinessType("10");//
//					tFIAbStandardDataSchema.setSecondaryFlag("1");
//					tFIAbStandardDataSchema.setBusinessDetail("101");//
//					tFIAbStandardDataSchema.setCostID("F0000000024");
//					tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;
//					
//					if("1".equals(tFIAbStandardDataSchema.getRiskType())
//							||"3".equals(tFIAbStandardDataSchema.getRiskType()))
//					{
//						//万能 新特需
//						tFIAbStandardDataSchema.setBusTypeID("O-NC-000006");
//					}
//					else if("2".equals(tFIAbStandardDataSchema.getRiskType()))
//					{
//						//老特需
//						if("1605".equals(tFIAbStandardDataSchema.getRiskCode())
//								||"170106".equals(tFIAbStandardDataSchema.getRiskCode()))
//						{
//							tFIAbStandardDataSchema.setBusTypeID("O-NC-000003");//
//						}
//						else
//						{
//							tFIAbStandardDataSchema.setBusTypeID("O-NC-000006");
//						}
//					}
//					else
//					{
//						//传统险
//						tFIAbStandardDataSchema.setBusTypeID("O-NC-000003");//
//					}
//				}
//			}	
//		} 
//		catch (Exception e1) 
//		{
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_NDH_GBSQYF tSecondaryRule_NDH_GBSQYF = new SecondaryRule_NDH_GBSQYF();
		
		
		//tSecondaryRule_YD.ruleExe(cInputData, cOperate)
		
	}

}
