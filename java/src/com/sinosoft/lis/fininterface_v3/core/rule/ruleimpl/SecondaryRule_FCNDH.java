package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--反冲年度化</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author liusc
 * @version 3.0
 */
public class SecondaryRule_FCNDH extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

		tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType," +
				"a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo," +
				"a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType," +
				"a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl," +
				"a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years," +
				"a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,(select -b.Sumactupaymoney from ljapaygrp b where b.grpcontno = a.grpcontno and b.grppolno = a.grppolno and Sumactupaymoney>0 and b.PayType='ZC' and exists(select 1 from ljapay c where c.payno = b.payno and c.DueFeeType in('0','2')) order by paycount fetch first 1 rows only) SumActuMoney,a.MothDate,a.BusinessDate," +
				"a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02," +
				"a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate," +
				"a.MakeTime from FIAbStandardData a where a.NoType = '2' and a.state = '00' and ListFlag='2' and a.BusinessType = '03' and a.Businessdetail='032' " +
				" and a.riskperiod in ('M','S') and a.payintv not in (0,12,-1) and a.CheckFlag='00' " +
				" and a.costid <> 'F0000000021' and a.feetype = 'ZC' " +
				" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusinessDetail='102' ) " +
				" and exists (select 1 from FIAbStandardData d where d.indexno in (select e.payno from LJapay e where a.grpcontno=e.incomeno and e.duefeetype='0') and d.BusinessType = '03' and d.Businessdetail = '031'  and d.paycount = '1'" +
				" and d.accountdate < '2016-5-1' fetch first 1 row only)" );
		
		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				//短期险非趸缴非年交 续期反冲年度化
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setBusinessType("10");
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				tFIAbStandardDataSchema.setBusinessDetail("102");
				tFIAbStandardDataSchema.setCostID("F0000000025");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;
				
				if("1".equals(tFIAbStandardDataSchema.getRiskType())
						||"3".equals(tFIAbStandardDataSchema.getRiskType()))
				{
					//万能 新特需
					tFIAbStandardDataSchema.setBusTypeID("O-XQ-000010");
				}
				else if("2".equals(tFIAbStandardDataSchema.getRiskType()))
				{
					//老特需
					if("1605".equals(tFIAbStandardDataSchema.getRiskCode())
							||"170106".equals(tFIAbStandardDataSchema.getRiskCode()))
					{
						tFIAbStandardDataSchema.setBusTypeID("O-XQ-000008");
					}
					else
					{
						tFIAbStandardDataSchema.setBusTypeID("O-XQ-000010");
					}
				}
				else
				{
					//传统险
					tFIAbStandardDataSchema.setBusTypeID("O-XQ-000008");//
				}
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_FCNDH tSecondaryRule_FCNDH = new SecondaryRule_FCNDH();
		System.out.println(tSecondaryRule_FCNDH.prepareRule());
		
	}

}
