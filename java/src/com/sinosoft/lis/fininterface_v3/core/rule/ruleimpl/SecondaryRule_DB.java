package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;
/**
 * <p>Title: 二次处理规则-大病专项改造</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author rosia
 * @version 3.0 
 */

import com.sinosoft.lis.db.FIAbStandardDataDB;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.utility.VData;

public class SecondaryRule_DB extends StandSecondaryRule {
	


		public String prepareRule()
		{        
			StringBuffer tSQL = new StringBuffer();
			tSQL.append("select distinct '' as SerialNo,a.NoType as NoType,a.ASerialNo as ASerialNo,'1' as SecondaryFlag,'DB' as SecondaryType,a.BusTypeID as BusTypeID,'F0000DB2244' as CostID,a.BusinessType as BusinessType,a.BusinessDetail as BusinessDetail,a.FeeType as FeeType,a.FeeDetail as FeeDetail,a.IndexCode as IndexCode,a.IndexNo as IndexNo,a.ListFlag as ListFlag,a.GrpContNo as GrpContNo,a.GrpPolNo as GrpPolNo,a.ContNo as ContNo,a.PolNo as PolNo,a.EndorsementNo as EndorsementNo,a.NotesNo as NotesNo,a.FirstYearFlag as FirstYearFlag,a.FirstTermFlag as FirstTermFlag,a.BonusType as BonusType,a.DifComFlag as DifComFlag,'162201' as RiskCode,a.RiskPeriod as RiskPeriod,a.risktype as RiskType,a.RiskType1 as RiskType1,a.RiskType2 as RiskType2,a.CostCenter as CostCenter,a.ACType as ACType,a.AgentCom as AgentCom,a.AgentGroup as AgentGroup,a.AgentCode as AgentCode,a.CustomerID as CustomerID,a.SupplierNo as SupplierNo,a.SaleChnl as SaleChnl,a.SaleChnlDetail as SaleChnlDetail,a.ManageCom as ManageCom,a.ExecuteCom as ExecuteCom,a.CValiDate as CValiDate,a.PayIntv as PayIntv,a.LastPayToDate as LastPayToDate,a.CurPayToDate as CurPayToDate,a.PolYear as PolYear,a.Years as Years,a.PremiumType as PremiumType,a.PayCount as PayCount,a.PayMode as PayMode,a.BankCode as BankCode,a.AccName as AccName,a.BankAccNo as BankAccNo," +
					"a.SumActuMoney as SumActuMoney," +
					"a.MothDate as MothDate,a.BusinessDate as BusinessDate,a.AccountDate as AccountDate,a.CashFlowNo as CashFlowNo,a.FirstYear as FirstYear,a.MarketType as MarketType,a.OperationType as OperationType,a.Budget as Budget,a.Currency as Currency,a.StringInfo01 as StringInfo01,a.StringInfo02 as StringInfo02,a.StringInfo03 as StringInfo03,a.DateInfo01 as DateInfo01,a.DateInfo02 as DateInfo02,a.SerialNo as RelatedNo,a.EventNo as EventNo,a.State as State,a.ReadState as ReadState,a.CheckFlag as CheckFlag,a.Operator as Operator,'"+PubFun.getCurrentDate()+"' as MakeDate,'"+PubFun.getCurrentTime()+"' as MakeTime " +
					"from FIAbStandardData a where a.secondaryflag = '0' and notype = '2' and a.riskcode = '162201' and substr(costcenter ,5,4) = '9396' and a.indexcode = '03' " +
					"and exists (select 1 from fiabstandarddata f where f.indexno = a.indexno and f.indexcode = a.indexcode and f.costid = 'F0000000044')" +
					" and state = '00' and CheckFlag = '00' " +
					"and not exists (select 1 from FIAbStandardData d where a.SerialNo = d.RelatedNo and d.CostID = 'F0000DB2244' and d.SecondaryType = 'DB' and a.indexno = d.indexno and a.indexcode = d.indexcode)  " +
					"union all " +
					"select distinct '' as SerialNo,a.NoType as NoType,a.ASerialNo as ASerialNo,'1' as SecondaryFlag,'DB' as SecondaryType,a.BusTypeID as BusTypeID,'F0000DB2344' as CostID,a.BusinessType as BusinessType,a.BusinessDetail as BusinessDetail,a.FeeType as FeeType,a.FeeDetail as FeeDetail,a.IndexCode as IndexCode,a.IndexNo as IndexNo,a.ListFlag as ListFlag,a.GrpContNo as GrpContNo,a.GrpPolNo as GrpPolNo,a.ContNo as ContNo,a.PolNo as PolNo,a.EndorsementNo as EndorsementNo,a.NotesNo as NotesNo,a.FirstYearFlag as FirstYearFlag,a.FirstTermFlag as FirstTermFlag,a.BonusType as BonusType,a.DifComFlag as DifComFlag,'162301' as RiskCode,a.RiskPeriod as RiskPeriod,a.risktype as RiskType,a.RiskType1 as RiskType1,a.RiskType2 as RiskType2,a.CostCenter as CostCenter,a.ACType as ACType,a.AgentCom as AgentCom,a.AgentGroup as AgentGroup,a.AgentCode as AgentCode,a.CustomerID as CustomerID,a.SupplierNo as SupplierNo,a.SaleChnl as SaleChnl,a.SaleChnlDetail as SaleChnlDetail,a.ManageCom as ManageCom,a.ExecuteCom as ExecuteCom,a.CValiDate as CValiDate,a.PayIntv as PayIntv,a.LastPayToDate as LastPayToDate,a.CurPayToDate as CurPayToDate,a.PolYear as PolYear,a.Years as Years,a.PremiumType as PremiumType,a.PayCount as PayCount,a.PayMode as PayMode,a.BankCode as BankCode,a.AccName as AccName,a.BankAccNo as BankAccNo," +
					"a.SumActuMoney as SumActuMoney," +
					"a.MothDate as MothDate,a.BusinessDate as BusinessDate,a.AccountDate as AccountDate,a.CashFlowNo as CashFlowNo,a.FirstYear as FirstYear,a.MarketType as MarketType,a.OperationType as OperationType,a.Budget as Budget,a.Currency as Currency,a.StringInfo01 as StringInfo01,a.StringInfo02 as StringInfo02,a.StringInfo03 as StringInfo03,a.DateInfo01 as DateInfo01,a.DateInfo02 as DateInfo02,a.SerialNo as RelatedNo,a.EventNo as EventNo,a.State as State,a.ReadState as ReadState,a.CheckFlag as CheckFlag,a.Operator as Operator,'"+PubFun.getCurrentDate()+"' as MakeDate,'"+PubFun.getCurrentTime()+"' as MakeTime " +
					"from FIAbStandardData a " +
					"where a.secondaryflag = '0' and notype = '2' and a.indexcode = '03' and a.riskcode = '162301' and substr(costcenter ,5,4) = '9396' " +
					"and exists (select 1 from fiabstandarddata f where f.indexno = a.indexno and f.indexcode = a.indexcode and f.costid = 'F0000000044') " +
					"and state = '00' and CheckFlag = '00' " +
					"and not exists (select 1 from FIAbStandardData d where a.SerialNo = d.RelatedNo and d.CostID = 'F0000DB2344' and d.SecondaryType = 'DB' and a.indexno = d.indexno and a.indexcode = d.indexcode)");
			
			return tSQL.toString();
		}
		
		public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
		{
			FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
			
			try 
			{
				String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
				
				for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
				{
					FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
					tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;		
					PubSubmit mPubSubmit = new PubSubmit();
					MMap tMap= new MMap();
					VData tInputData = new VData();
					String tsql="update FIAbStandardData a set a.sumactumoney = nvl((select sum(b.sumactumoney) from fiabstandarddata b where a.indexno = b.indexno and a.indexcode = b.indexcode and  b.secondaryflag = '0' and b.riskcode not in('162201','162301') and b.notype = '2' and  b.feetype not  in('SX')),0),riskcode  = '' where a.notype = '1' and a.indexno = '"+tFIAbStandardDataSchema.getIndexNo()+"'  and a.costid = 'F0000000044' and a.secondaryflag = '0' and a.state = '00'";					
					tMap.put(tsql, "UPDATE");
					tInputData.add(tMap);
			        if (!mPubSubmit.submitData(tInputData, ""))
			        {
			        	throw new  Exception("SecondaryRule_DB:更新错误！");
			        	
			        }
				}	
			} 
			catch (Exception e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return pFIAbStandardDataSet;
		}
		
		/**
		 * @param args
		 */
		public static void main(String[] args) 
		{
			
		}





}
