package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--保全复效个险反冲年度化（保费和税额）</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: </p>
 * @author gcx
 * @version 3.0
 */
public class SecondaryRule_BQFXFCNDHGX extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

		tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,'1' as SecondaryFlag,'GX' as SecondaryType,a.BusTypeID,a.CostID,a.BusinessType," +
				"a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo," +
				"a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType," +
				"a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl," +
				"a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years," +
				"a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,(a.sumactumoney/(select b.MoneyNoTax from ljapayperson b where b.contno = a.contno and b.polno = a.polno and Sumactupaymoney>0 and b.PayType='ZC' and exists (select 1 from ljapay c where c.payno = b.payno and c.DueFeeType in ('0', '2') order by  confdate desc)fetch first rows only))*(select -b.MoneyNoTax from ljapayperson b where b.contno = a.contno and b.polno = a.polno and Sumactupaymoney>0 and b.PayType='ZC' and exists (select 1 from ljapay c where c.payno = b.payno and c.DueFeeType in ('0', '2') order by  confdate desc)fetch first rows only)  SumActuMoney,a.MothDate,a.BusinessDate," +
				"a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02," +
				"a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate," +
				"a.MakeTime from FIAbStandardData a where a.NoType = '2' and a.state = '00' and ListFlag='1' and a.BusinessType = '04' and a.Businessdetail='041' " +
				" and a.riskperiod in ('M','S')  and  a.riskcode not in(select  distinct   riskcode from  lmrisk  where  rnewflagb ='Y'  )and a.payintv not in (0,12,-1) and a.CheckFlag='00' " +
				" and a.costid <> 'F0000000021' and a.feetype = 'FX'  and   a.feedetail = 'BF'  " +
				" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusinessDetail='102' and c.costid = 'F0000000025') " +
				" and exists (select 1 from FIAbStandardData d where d.contno = a.contno  and d.BusinessType = '03' and d.Businessdetail = '031'  and d.paycount = '1'" +
				" fetch first 1 row only)" );
		
		tSQL.append("union all");
		
		tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,'1' as SecondaryFlag,'GS' as SecondaryType,a.BusTypeID,a.CostID,a.BusinessType," +
				"a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo," +
				"a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType," +
				"a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl," +
				"a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years," +
				"a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,(a.sumactumoney/(select b.MoneyTax from ljapayperson b where b.contno = a.contno and b.polno = a.polno and Sumactupaymoney>0 and b.PayType='ZC' and  exists (select 1 from ljapay c where c.payno = b.payno and c.DueFeeType in ('0', '2') order by  confdate desc)fetch first rows only))*(select -b.MoneyTax from ljapayperson b where b.contno = a.contno and b.polno = a.polno and Sumactupaymoney>0 and b.PayType='ZC' and  exists (select 1 from ljapay c where c.payno = b.payno and c.DueFeeType in ('0', '2') order by  confdate desc)fetch first rows only)  SumActuMoney,a.MothDate,a.BusinessDate," +
				"a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02," +
				"a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate," +
				"a.MakeTime from FIAbStandardData a where a.NoType = '2' and ListFlag='1' and a.BusinessType = '04' and a.Businessdetail='041' " +
				" and a.riskperiod in ('M','S') and  a.riskcode not in(select  distinct   riskcode from lmrisk where  rnewflagb ='Y') and a.payintv not in (0,12,-1) and a.CheckFlag='00' and a.state = '00' " +
				" and a.costid <> 'F0000000021' and a.feetype = 'ZE'" +
				" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusinessDetail='102' and c.Costid='F0000000SE2') " +
				" and exists (select 1 from FIAbStandardData d where d.contno = a.contno and d.BusinessType = '03' and d.Businessdetail = '031' and d.paycount = '1'" +
		 		" fetch first 1 row only)" );
		
	
		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				//个险短险普通险非保证续保期缴复效反冲年度化
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setBusinessType("10");
				tFIAbStandardDataSchema.setBusinessDetail("102");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;							
				tFIAbStandardDataSchema.setBusTypeID("O-XQ-000008");
				if("ZE".equals(tFIAbStandardDataSchema.getFeeType()))
				tFIAbStandardDataSchema.setCostID("F0000000SE2");
				else if("FX".equals(tFIAbStandardDataSchema.getFeeType()) && "BF".equals(tFIAbStandardDataSchema.getFeeDetail()))				
				tFIAbStandardDataSchema.setCostID("F0000000025");				
		} 
		}
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_BQFXFCNDHGX tSecondaryRule_BQFXFCNDHGX = new SecondaryRule_BQFXFCNDHGX();
		
	}

}
