package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--再保代收、代付数据</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class SecondaryRule_LRYD extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

		tSQL.append(" select * from FIAbStandardData a where bustypeid in('R-FB-000001','R-TP-000001','R-TS-000001') and a.state = '00' and a.CheckFlag='00' " );
		tSQL.append(" and BusinessType = '30' and BusinessDetail in('301','302','303') and indexcode in('10','11') " );
		tSQL.append(" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.bustypeid in('DF-R-FB-000001','DS-R-TP-000001','DS-R-TS-000001'))");
		
		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			int size = pFIAbStandardDataSet.size();
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(size);
			
			for(int i=1;i<=size;i++)			
			{
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;
				String executeCom = tFIAbStandardDataSchema.getExecuteCom();
				String manageCom = tFIAbStandardDataSchema.getManageCom();  
				
				tFIAbStandardDataSchema.setManageCom(executeCom);
				tFIAbStandardDataSchema.setExecuteCom(manageCom);
				System.out.println("BusTypeID----"+tFIAbStandardDataSchema.getBusTypeID());
				if("R-FB-000001".equals(tFIAbStandardDataSchema.getBusTypeID()))
				{
					//再保分出保费-代付
					tFIAbStandardDataSchema.setBusTypeID("DF-R-FB-000001");
					tFIAbStandardDataSchema.setCostID("F000000FCBF-D");
				}
				if("R-TP-000001".equals(tFIAbStandardDataSchema.getBusTypeID()))
				{
					//再保摊回赔款-代收
					tFIAbStandardDataSchema.setBusTypeID("DS-R-TP-000001");
					tFIAbStandardDataSchema.setCostID("F000000THPK-D");
				}
				else if("R-TS-000001".equals(tFIAbStandardDataSchema.getBusTypeID()))
				{
					//再保摊回手续费-代收
					tFIAbStandardDataSchema.setBusTypeID("DS-R-TS-000001");
					tFIAbStandardDataSchema.setCostID("F000000THSX-D");
				}
				tFIAbStandardDataSchema.setSecondaryFlag("1");		
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		SecondaryRule_LRYD tSecondaryRule_LPYFYD = new SecondaryRule_LRYD();

		System.out.println(tSecondaryRule_LPYFYD.prepareRule());
		
	}

}
