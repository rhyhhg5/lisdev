package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--邮储代付</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class SecondaryRule_YCDF extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

		tSQL.append(" select * from FIAbStandardData a where a.NoType = '1' and a.state = '00' and a.BusinessType = '02' and businessdetail<>'031' and a.CheckFlag='00' ");
		tSQL.append(" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusinessType='06' )");
		tSQL.append(" and exists(select 1 from FIAboriginalMainAtt b where a.ASerialNo=b.MSerialNo and DifComFlag = '1' and YCFlag='1')");

		tSQL.append(" union all ");

		tSQL.append(" select * from FIAbStandardData a where a.NoType = '2' and a.state = '00' and a.BusinessType = '02' and a.CheckFlag='00' ");
		tSQL.append(" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusinessType='06' )");
		tSQL.append(" and exists(select 1 from FIAboriginalGenAtt b where a.ASerialNo=b.ASerialNo and DifComFlag = '1' and YCFlag='1')");

		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				//(1)实际收费机构费用
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setBusinessType("06");//异地代收
				
				String manageCom = tFIAbStandardDataSchema.getManageCom();  //付费机构
				String executeCom = tFIAbStandardDataSchema.getExecuteCom();//保单机构
				
				tFIAbStandardDataSchema.setManageCom(executeCom);
				tFIAbStandardDataSchema.setExecuteCom(manageCom);
				
				if("021".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
					if ("ZC".equals(tFIAbStandardDataSchema.getFeeType()))
					{
					tFIAbStandardDataSchema.setBusinessDetail("061");//暂收退费异地
					tFIAbStandardDataSchema.setBusTypeID("DF-NC-000002");//暂收退费-邮储\建行总部代付
					tFIAbStandardDataSchema.setCostID("F000000000P");
					}
					else if ("SEZT".equals(tFIAbStandardDataSchema.getFeeType()))
					{						
						tFIAbStandardDataSchema.setBusinessDetail("061");//暂收退费异地
						tFIAbStandardDataSchema.setBusTypeID("DF-NC-000002");//暂收退费-邮储\建行总部代付
						tFIAbStandardDataSchema.setCostID("F000000FSEP");
					}
				}
				
				if("020".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{

					tFIAbStandardDataSchema.setBusinessDetail("062");//管B退保
					tFIAbStandardDataSchema.setBusTypeID("DF-BQ-000009");//管B退保
					tFIAbStandardDataSchema.setCostID("F000000000P");
				}
				if("030".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{

					tFIAbStandardDataSchema.setBusinessDetail("062");//守望B退保
					tFIAbStandardDataSchema.setBusTypeID("DF-BQ-000002");//守望B退保
					tFIAbStandardDataSchema.setCostID("F0000000SWP");
				}
				else if("022".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
					tFIAbStandardDataSchema.setBusinessDetail("062");//保全异地
					if("F-BQ-000005".equals(tFIAbStandardDataSchema.getBusTypeID()))
					{
						tFIAbStandardDataSchema.setBusTypeID("DF-BQ-000005");//保单质押贷款-贷款-异地
						tFIAbStandardDataSchema.setCostID("F000000000P");
					}
					else if("HL".equals(tFIAbStandardDataSchema.getStringInfo02()))
					{
						//保全异地付费-邮储\建行总部代付(分红险)
						tFIAbStandardDataSchema.setBusTypeID("DF-BQ-000006");					
						if("HLLX".equals(tFIAbStandardDataSchema.getFeeDetail())
								||"LX".equals(tFIAbStandardDataSchema.getFeeDetail()))
						{
							//保单红利-利息
							tFIAbStandardDataSchema.setCostID("F000000HLLX");
						}
						else if("HLBF".equals(tFIAbStandardDataSchema.getFeeDetail()))
						{
							//现金领取
							tFIAbStandardDataSchema.setCostID("F000000HLLQ");
						}
						else if("HL".equals(tFIAbStandardDataSchema.getFeeDetail()))
						{
							//保单红利-累积生息
							tFIAbStandardDataSchema.setCostID("F000000HLLJ");
						}
						else
						{
							//退保金
							tFIAbStandardDataSchema.setCostID("F000000HLTB");
						}
					}
					else if("F-BQ-000002".equals(tFIAbStandardDataSchema.getBusTypeID()) && "DD".equals(tFIAbStandardDataSchema.getFeeType()))
					{
						tFIAbStandardDataSchema.setBusTypeID("DF-BQ-000006");//分红险变更领取方式
						tFIAbStandardDataSchema.setCostID("F000000HLLQ");
					}
										else if ("F-BQ-000012".equals(tFIAbStandardDataSchema.getBusTypeID())&& "SFDB".equals(tFIAbStandardDataSchema.getFeeDetail())){
						tFIAbStandardDataSchema.setBusTypeID("DF-BQ-000002");//保全异地付费-邮储\建行总部代付
						tFIAbStandardDataSchema.setCostID("P000000SFDB");
					}
					else if ("F-BQ-000012".equals(tFIAbStandardDataSchema.getBusTypeID())&& "SFFDB".equals(tFIAbStandardDataSchema.getFeeDetail())){
						tFIAbStandardDataSchema.setBusTypeID("DF-BQ-000002");//保全异地付费-邮储\建行总部代付
						tFIAbStandardDataSchema.setCostID("P00000SFFDB");
					}
					else
					{
						tFIAbStandardDataSchema.setBusTypeID("DF-BQ-000002");//保全异地付费-邮储\建行总部代付
						tFIAbStandardDataSchema.setCostID("F000000000P");
					}
				}
				
				else if("023".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
//					理赔付费--异地代付					
					tFIAbStandardDataSchema.setBusinessDetail("063");//理赔异地
					if("F-LP-000003".equals(tFIAbStandardDataSchema.getBusTypeID()))
					{
						//理赔付费-邮储\建行总部代付
						if("1".equals(tFIAbStandardDataSchema.getRiskType())||"2".equals(tFIAbStandardDataSchema.getRiskType())
								||"3".equals(tFIAbStandardDataSchema.getRiskType()))
						{
							tFIAbStandardDataSchema.setBusTypeID("DF-LP-000004");//理赔异地付费-邮储\建行总部代付-万能特需
							tFIAbStandardDataSchema.setCostID("F000000000P");
						}else if ("0".equals(tFIAbStandardDataSchema.getRiskType())&& "8201".equals(tFIAbStandardDataSchema.getRiskCode().substring(0, 4))){
							tFIAbStandardDataSchema.setBusTypeID("DF-LP-000003");//理赔异地付费-邮储\建行总部代付-守望B
							tFIAbStandardDataSchema.setCostID("F00000SWYCP");
							
						}
						else
						{
							tFIAbStandardDataSchema.setBusTypeID("DF-LP-000003");//理赔 传统邮储
							tFIAbStandardDataSchema.setCostID("F000000000P");
						}
					}else if("F-LP-000007".equals(tFIAbStandardDataSchema.getBusTypeID()))
					{
						//理赔付费-天津意外险邮储\建行总部代付
						tFIAbStandardDataSchema.setBusTypeID("DF-LP-000005");
						tFIAbStandardDataSchema.setCostID("F000000000P");
					}else if("F-LP-000006".equals(tFIAbStandardDataSchema.getBusTypeID()))
					{
						//理赔付费-预付赔款-异地(传统险)
						tFIAbStandardDataSchema.setBusTypeID("DF-LP-000003");
						tFIAbStandardDataSchema.setCostID("F000000000P");
					}else if("F-LP-000008".equals(tFIAbStandardDataSchema.getBusTypeID()))
					{
						//理赔付费-预付赔款-异地(账户险)
						tFIAbStandardDataSchema.setBusTypeID("DF-LP-000004");
						tFIAbStandardDataSchema.setCostID("F000000000P");
					}
				}
				else if("024".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
					tFIAbStandardDataSchema.setBusinessDetail("064");//余额领取异地
					tFIAbStandardDataSchema.setBusTypeID("DF-LQ-000002");
					tFIAbStandardDataSchema.setCostID("F000000000P");
				}
				else if("028".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
//					理赔分红险含红利利息及红利保费数据实付流转规则 -- 20160224
					tFIAbStandardDataSchema.setBusinessDetail("065");//理赔异地
					if ("F-LP-000HL2".equals(tFIAbStandardDataSchema.getBusTypeID())){
						
						tFIAbStandardDataSchema.setBusTypeID("DF-LP-000HL2");
						
						if("HLLX".equals(tFIAbStandardDataSchema.getFeeDetail()))
						{
						//理赔付费-分红险							
							tFIAbStandardDataSchema.setCostID("M000000HLLX");
						}
						else if ("HLBF".equals(tFIAbStandardDataSchema.getFeeDetail()))
						{
							tFIAbStandardDataSchema.setCostID("M000000HLLQ");							
						}
					}
				}
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;		
				
				}	
			
		}
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
//		SecondaryRule_YCDF tSecondaryRule_YCDF = new SecondaryRule_YCDF();
		
		
		//tSecondaryRule_YD.ruleExe(cInputData, cOperate)
		
	}

}
