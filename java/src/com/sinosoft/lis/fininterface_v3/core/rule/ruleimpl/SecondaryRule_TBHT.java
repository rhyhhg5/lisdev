package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import java.util.Date;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.FIVoucherDataDetailSet;
import com.sinosoft.lis.db.FIVoucherDataDetailDB;
import com.sinosoft.lis.schema.FIVoucherDataDetailSchema;
/**
 * <p>Title: 二次处理规则--退保回退贷方数据提取</p>
 *
 * <p>Description: 退保回退需要完全冲销退保数据的借方</p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author liusc
 * @version 3.0
 */
public class SecondaryRule_TBHT extends StandSecondaryRule{

	
	public String prepareRule()
	{
		System.out.println("退保回退提数开始---、、、");
		String tDate = null;	
		tDate = PubFun.getCurrentDate();
		StringBuffer tSQL = new StringBuffer();

		String sqlBJ = "select distinct actugetno from LJagetendorse where getmoney<>0 and endorsementno in" +
				       " (select distinct c.innersource from lgwork c where c.workno in (select b.endorsementno from LJagetendorse b where b.getmoney <> 0 and b.feeoperationtype = 'RB'  and b.makedate = (date('"+tDate+"') - 1 days)))";
		
		String a = null; String b = null; String c = null;int j;
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(sqlBJ);
		StringBuffer buffer = new StringBuffer();
		int maxRow = tSSRS.getMaxRow();
		if (maxRow != 0){			 
		if (maxRow == 1){			
			c = "'"+tSSRS.GetText(1, 1)+"'";
		}else{
			for (j=1;j<tSSRS.getMaxRow();j++){				

				a = buffer.append("'"+tSSRS.GetText(j, 1)+"',").toString();
			}
	        if (j == tSSRS.getMaxRow()){  
	        	
	          	 b = "'"+tSSRS.GetText(tSSRS.getMaxRow(), 1)+"'"; 
	           }
			 c = a + b;
			 System.out.println(c);
		}
	       			
			tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType,");
			tSQL.append(" a.BusinessDetail,'RB' as feetype,a.FeeDetail,'02' as indexcode,(select distinct actugetno from LJAgetendorse where endorsementno in (select distinct workno from lgwork where innersource in (select distinct endorsementno from LJagetendorse where actugetno=a.indexno) fetch first rows only)) as indexno,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,");
			tSQL.append(" a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType,");
			tSQL.append(" a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl,");
			tSQL.append(" a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years,");
			tSQL.append(" a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,");
			tSQL.append(" SumActuMoney as SumActuMoney,");
			tSQL.append(" a.MothDate,a.BusinessDate,(select distinct makedate from LJAgetendorse where endorsementno in (select distinct workno from lgwork where innersource in (select distinct endorsementno from LJagetendorse where actugetno=a.indexno) fetch first rows only)) as accountdate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02,");
			tSQL.append(" a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,'"+PubFun.getCurrentDate()+"' as MakeDate,'"+PubFun.getCurrentTime()+"' as maketime");
			tSQL.append(" from FIAbStandardData a where a.indexno in ("+c+") and a.indexcode='03' and a.feetype<>'SX'");
//			tSQL.append(" and exists (select 1 from FIVoucherBnFee  b where a.BusTypeID=b.businessid and a.costid=b.feeid and b.D_finitemid is not null)");
			tSQL.append(" and not exists(select 1 from FIAbStandardData f where a.SerialNo = f.RelatedNo and f.SecondaryFlag = '1' and f.bustypeid = 'S-BQ-HT-000001' and a.costid = f.costid  and f.feetype='RB')");
		}else{
			tSQL.append("select 1 from FIAbStandardData where 1=2 ");
		}
		System.out.println(tSQL);
        return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			//提数日期
			String tDate = null;	
			tDate = PubFun.getCurrentDate();
			System.out.print("hahahahha"+tDate);
			
			String sqldate = "select EventType,EventNo from FIOperationLog a where  a.makedate = '"+tDate+"'  and EventType='10' order by maketime desc fetch first 1 rows only" ;  
			String fdate;
			PubSubmit tPubSubmit = new PubSubmit(); 
			MMap tmap=new MMap();
			SSRS mSSRS = new SSRS();
			ExeSQL tExeSQL = new ExeSQL();
			mSSRS = tExeSQL.execSQL(sqldate);
			String[] mFserialno = null;
			FIVoucherDataDetailSet mFIVoucherDataDetailSet=new FIVoucherDataDetailSet();
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
//				凭证表数据处理
				fdate="select * from FiVoucherdatadetail where serialno='"+tFIAbStandardDataSchema.getSerialNo()+"'";
				FIVoucherDataDetailSet  tfivoucherdatadetailset=new FIVoucherDataDetailDB().executeQuery(fdate);
				mFserialno=FinCreateSerialNo.getSerialNoByFIFSerialNo(tfivoucherdatadetailset.size());
				if(tfivoucherdatadetailset.size()==0){
					System.out.println("流水号"+tFIAbStandardDataSchema.getSerialNo()+"查询失败了！！！");
				}else{
					for(int s=1;s<=tfivoucherdatadetailset.size();s++){
						FIVoucherDataDetailSchema tFIVoucherDataDetailSchema=tfivoucherdatadetailset.get(s);
						tFIVoucherDataDetailSchema.setBatchNo("SSSS0000000000008251");
						tFIVoucherDataDetailSchema.setSerialNo(tSerialNo[i-1]);
						tFIVoucherDataDetailSchema.setFSerialNo(mFserialno[s-1]);
//						tFIVoucherDataDetailSchema.setManageCom(tFIAbStandardDataSchema.getManageCom());
						tFIVoucherDataDetailSchema.setAccountDate(tFIAbStandardDataSchema.getAccountDate());
						tFIVoucherDataDetailSchema.setReadState("0");
						mFIVoucherDataDetailSet.add(tFIVoucherDataDetailSchema);
					}		
				}
				
//				准业务转换表数据
				System.out.println("getSerialNo()--"+tFIAbStandardDataSchema.getSerialNo());
					tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
					tFIAbStandardDataSchema.setSecondaryFlag("1");
					tFIAbStandardDataSchema.setState("00");
					tFIAbStandardDataSchema.setReadState("0");
					tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;					
					String adate = mSSRS.GetText(1,1);
					String aeventno = mSSRS.GetText(1,2);
					tFIAbStandardDataSchema.setEventNo(aeventno);
					tFIAbStandardDataSchema.setBusTypeID("S-BQ-HT-000001");
			}
			tmap.put(mFIVoucherDataDetailSet, "INSERT");
//			tmap.put("update fivoucherdatadetail a set finitemtype=(case finitemtype when 'D' then 'C' else 'D' end),accountdate=(select b.accountdate from fiabstandarddata b where a.serialno=b.serialno) where batchno = 'SSSS0000000000008251'", "UPDATE");
//			tmap.put("update fivoucherdatadetail a set accountcode='2205020000' where batchno = 'SSSS0000000000008251' and finitemtype='D' and accountcode in ('2203040000','2221170700','6051990000','2205010000')", "UPDATE");
			VData tdata=new VData();
			tdata.add(tmap);
			
			if (!tPubSubmit.submitData(tdata, ""))
	        {
				System.out.println("凭证表数据更新失败了！！！");
	        }
			
        }
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_TBHT tSecondaryRule_TBHT = new SecondaryRule_TBHT();
		tSecondaryRule_TBHT.prepareRule();
		
	}
	

}
