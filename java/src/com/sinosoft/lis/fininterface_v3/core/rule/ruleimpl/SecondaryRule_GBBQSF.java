
package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--共保保全收费</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author liusc
 * @version 3.0
 */
public class SecondaryRule_GBBQSF extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();



		tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType,");
		tSQL.append(" a.BusinessDetail,'BQ' AS FeeType,a.FeeDetail,'08' AS IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,");
		tSQL.append(" a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType,");
		tSQL.append(" a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl,");
		tSQL.append(" a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years,");
		tSQL.append(" a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,");
		tSQL.append(" cast(round(a.SumActuMoney * (select sum(rate) from LCCoInsuranceParam b where b.grpcontno = a.grpcontno),2) as decimal(30,2)) as SumActuMoney,");
		tSQL.append(" a.MothDate,a.BusinessDate,a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02,");
		tSQL.append(" a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate,");
		tSQL.append(" a.MakeTime from FIAbStandardData a where a.NoType = '2' and a.state = '00' and ListFlag='2' and a.BusinessType = '04' and a.Businessdetail in ('041','042') ");
		tSQL.append(" and a.feetype in ('ZT','WJ','BJ','XT','WT')  and a.risktype <> '4' ");
		tSQL.append(" and exists (select 1 from LCCoInsuranceParam b where a.grpcontno=b.grpcontno) ");
		tSQL.append(" and exists(select 1 from FIAbStandardData d where d.indexno in (select payno from LJApay where incomeno=a.grpcontno and duefeetype='0') " +
								"and d.indexcode='02' and d.accountdate >= '2014-1-1' fetch first 1 row only) " );
		tSQL.append(" and a.CheckFlag='00'");
		tSQL.append(" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusinessDetail = '113')");

		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				tFIAbStandardDataSchema.setBusinessType("20");
				tFIAbStandardDataSchema.setBusinessDetail("113");
				if ("DBBJTF".equals(tFIAbStandardDataSchema.getFeeDetail()))
				{
					tFIAbStandardDataSchema.setCostID("GB000000010");
				}
				else if ("FDBJTF".equals(tFIAbStandardDataSchema.getFeeDetail()))
				{						
					tFIAbStandardDataSchema.setCostID("GB000000012");
				}else
				{
					tFIAbStandardDataSchema.setCostID("GB000000008");
				}
				tFIAbStandardDataSchema.setFeeDetail("BF");
				tFIAbStandardDataSchema.setBusTypeID("GB-BQ-000002");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//	
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;

	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_GBBQSF tSecondaryRule_GBBQSF = new SecondaryRule_GBBQSF();
		
		
		//tSecondaryRule_YD.ruleExe(cInputData, cOperate)
		
	}

}
