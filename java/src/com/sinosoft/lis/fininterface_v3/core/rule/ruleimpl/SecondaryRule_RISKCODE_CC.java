package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.db.FIAbStandardDataDB;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.utility.VData;
/**
 * <p>Title: 二次处理规则--约定缴费，实收金额为0</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author liusc
 * @version 3.0 
 */
public class SecondaryRule_RISKCODE_CC extends StandSecondaryRule{

	
	public String prepareRule()
	{        
		StringBuffer tSQL = new StringBuffer();
		tSQL.append(" select distinct '' as SerialNo,");
		tSQL.append(" a.NoType as NoType ,");
		tSQL.append(" a.ASerialNo as ASerialNo ,");
		tSQL.append(" '1' as SecondaryFlag ,");
		tSQL.append(" 'CC' as SecondaryType ,");
		tSQL.append(" a.BusTypeID as BusTypeID ,");
		tSQL.append(" a.CostID as CostID ,");
		tSQL.append(" a.BusinessType as BusinessType ,");
		tSQL.append(" a.BusinessDetail as BusinessDetail ,");
		tSQL.append(" a.FeeType as FeeType ,");
		tSQL.append(" a.FeeDetail as FeeDetail ,");
		tSQL.append(" a.IndexCode as IndexCode ,");
		tSQL.append(" a.IndexNo as IndexNo ,");
		tSQL.append(" a.ListFlag as ListFlag ,");
		tSQL.append(" a.GrpContNo as GrpContNo ,");
		tSQL.append(" a.GrpPolNo as GrpPolNo ,");
		tSQL.append(" a.ContNo as ContNo ,");
		tSQL.append(" a.PolNo as PolNo ,");
		tSQL.append(" a.EndorsementNo as EndorsementNo ,");
		tSQL.append(" a.NotesNo as NotesNo ,");
		tSQL.append(" a.FirstYearFlag as FirstYearFlag ,");
		tSQL.append(" a.FirstTermFlag as FirstTermFlag ,");
		tSQL.append(" a.BonusType as BonusType ,");
		tSQL.append(" a.DifComFlag as DifComFlag ,");
		tSQL.append(" (select b.RiskCode from lcpol b where a.contno = b.contno and b.prem <> 0 and LF_risktype(b.riskcode) <> a.risktype  fetch first 1 row only ) as RiskCode ,");
		tSQL.append(" a.RiskPeriod as RiskPeriod ,");
		tSQL.append(" LF_risktype((select b.RiskCode from lcpol b where a.contno = b.contno and b.prem <> 0 and LF_risktype(b.riskcode) <> a.risktype fetch first 1 row only )) as RiskType ,");
		tSQL.append(" a.RiskType1 as RiskType1 ,");
		tSQL.append(" a.RiskType2 as RiskType2 ,");
		tSQL.append(" a.CostCenter as CostCenter ,");
		tSQL.append(" a.ACType as ACType,");
		tSQL.append(" a.AgentCom as AgentCom,");
		tSQL.append(" a.AgentGroup as AgentGroup ,");
		tSQL.append(" a.AgentCode as AgentCode ,");
		tSQL.append(" a.CustomerID as CustomerID ,");
		tSQL.append(" a.SupplierNo as SupplierNo ,");
		tSQL.append(" a.SaleChnl as SaleChnl ,");
		tSQL.append(" (select b.RiskCode from lcpol b where a.contno = b.contno and b.prem <> 0 and LF_risktype(b.riskcode) <> a.risktype  fetch first 1 row only ) as SaleChnlDetail ,");
		tSQL.append(" a.ManageCom as ManageCom ,");
		tSQL.append(" a.ExecuteCom as ExecuteCom ,");
		tSQL.append(" a.CValiDate as CValiDate ,");
		tSQL.append(" a.PayIntv as PayIntv ,");
		tSQL.append(" a.LastPayToDate as LastPayToDate ,");
		tSQL.append(" a.CurPayToDate as CurPayToDate ,");
		tSQL.append(" a.PolYear as PolYear ,");
		tSQL.append(" a.Years as Years ,");
		tSQL.append(" a.PremiumType as PremiumType ,");
		tSQL.append(" a.PayCount as PayCount ,");
		tSQL.append(" a.PayMode as PayMode ,");
		tSQL.append(" a.BankCode as BankCode ,");
		tSQL.append(" a.AccName as AccName,");
		tSQL.append(" a.BankAccNo as BankAccNo,");
		tSQL.append("(select sum(-getmoney)  from ljagetendorse where actugetno=a.indexno and getmoney<>0 and db2inst1.LF_risktype(riskcode) <>a.risktype and not exists (select 1 from ljagetendorse where actugetno=a.indexno and getmoney<>0 and db2inst1.LF_risktype(riskcode)='5' and feefinatype='TB') having sum(-getmoney) is not null union select sum(-getmoney)  from ljagetendorse where actugetno=a.indexno and getmoney<>0 and riskcode <> a.riskcode and db2inst1.LF_risktype(riskcode) not in(a.risktype ,'5') and exists (select 1 from ljagetendorse where actugetno=a.indexno and getmoney<>0 and db2inst1.LF_risktype(riskcode)='5' and feefinatype='TB') having sum(-getmoney) is not null) as SumActuMoney ,");
		tSQL.append(" a.MothDate as MothDate ,");
		tSQL.append(" a.BusinessDate as BusinessDate ,");
		tSQL.append(" a.AccountDate as AccountDate ,");
		tSQL.append(" a.CashFlowNo  as CashFlowNo ,");
		tSQL.append(" a.FirstYear as FirstYear ,");
		tSQL.append(" a.MarketType as MarketType ,");
		tSQL.append(" a.OperationType as OperationType ,");
		tSQL.append(" a.Budget as Budget ,");
		tSQL.append(" a.Currency as Currency ,");
		tSQL.append(" a.StringInfo01 as StringInfo01  ,");
		tSQL.append(" a.StringInfo02 as StringInfo02,");
		tSQL.append(" a.StringInfo03  as StringInfo03 ,");
		tSQL.append(" a.DateInfo01 as DateInfo01 ,");
		tSQL.append(" a.DateInfo02 as DateInfo02 ,");
		tSQL.append(" a.SerialNo as RelatedNo,");
		tSQL.append(" a.EventNo as EventNo ,");
		tSQL.append(" a.State as State,");
		tSQL.append(" a.ReadState as ReadState ,");
		tSQL.append(" a.CheckFlag as CheckFlag ,");
		tSQL.append(" a.Operator as Operator ,");
		tSQL.append(" '"+PubFun.getCurrentDate()+"' as MakeDate ,");
		tSQL.append(" '"+PubFun.getCurrentTime()+"' as MakeTime ");
		tSQL.append(" from FIAbStandardData a");
		tSQL.append(" where a.costid='F0000000044' and a.listflag='1' and a.feetype in ('WT','CT','XT') and a.secondaryflag = '0' ");
		tSQL.append(" and exists (select 1 from lcpol b where a.contno = b.contno and b.prem <> 0 and LF_risktype(b.riskcode) <> a.risktype ");
		tSQL.append(" and exists(select 1 from lmriskapp c where b.riskcode=c.riskcode and c.Subriskflag = 'M'))");
		tSQL.append(" and state = '00' and CheckFlag='00' and not exists(select 1 from FIAbStandardData d where a.SerialNo = d.RelatedNo and d.CostID in('F0000000044','F000000HL44') and d.SecondaryType='CC' and a.indexno =d.indexno) ");
		tSQL.append(" union");
		tSQL.append(" select distinct '' as SerialNo,");
		tSQL.append(" a.NoType as NoType ,");
		tSQL.append(" a.ASerialNo as ASerialNo ,");
		tSQL.append(" '1' as SecondaryFlag ,");
		tSQL.append(" 'CC' as SecondaryType ,");
		tSQL.append(" a.BusTypeID as BusTypeID ,");
		tSQL.append(" a.CostID as CostID ,");
		tSQL.append(" a.BusinessType as BusinessType ,");
		tSQL.append(" a.BusinessDetail as BusinessDetail ,");
		tSQL.append(" a.FeeType as FeeType ,");
		tSQL.append(" a.FeeDetail as FeeDetail ,");
		tSQL.append(" a.IndexCode as IndexCode ,");
		tSQL.append(" a.IndexNo as IndexNo ,");
		tSQL.append(" a.ListFlag as ListFlag ,");
		tSQL.append(" a.GrpContNo as GrpContNo ,");
		tSQL.append(" a.GrpPolNo as GrpPolNo ,");
		tSQL.append(" a.ContNo as ContNo ,");
		tSQL.append(" a.PolNo as PolNo ,");
		tSQL.append(" a.EndorsementNo as EndorsementNo ,");
		tSQL.append(" a.NotesNo as NotesNo ,");
		tSQL.append(" a.FirstYearFlag as FirstYearFlag ,");
		tSQL.append(" a.FirstTermFlag as FirstTermFlag ,");
		tSQL.append(" a.BonusType as BonusType,");
		tSQL.append(" a.DifComFlag as DifComFlag ,");
		tSQL.append(" (select b.riskcode from lbpol b where a.contno = b.contno and b.prem <> 0 and LF_risktype(b.riskcode) <> a.risktype fetch first 1 row only ) as RiskCode ,");
		tSQL.append(" a.RiskPeriod as RiskPeriod ,");
		tSQL.append(" LF_risktype((select b.riskcode from lbpol b where a.contno = b.contno and b.prem <> 0 and LF_risktype(b.riskcode) <> a.risktype fetch first 1 row only )) as RiskType ,");
		tSQL.append(" a.RiskType1 as RiskType1 ,");
		tSQL.append(" a.RiskType2 as RiskType2 ,");
		tSQL.append(" a.CostCenter as CostCenter ,");
		tSQL.append(" a.ACType as ACType ,");
		tSQL.append(" a.AgentCom as AgentCom ,");
		tSQL.append(" a.AgentGroup as AgentGroup ,");
		tSQL.append(" a.AgentCode as AgentCode ,");
		tSQL.append(" a.CustomerID as CustomerID ,");
		tSQL.append(" a.SupplierNo as SupplierNo ,");
		tSQL.append(" a.SaleChnl as SaleChnl ,");
		tSQL.append(" (select b.riskcode from lbpol b where a.contno = b.contno and b.prem <> 0 and LF_risktype(b.riskcode) <> a.risktype fetch first 1 row only ) as SaleChnlDetail ,");
		tSQL.append(" a.ManageCom  as ManageCom ,");
		tSQL.append(" a.ExecuteCom as ExecuteCom ,");
		tSQL.append(" a.CValiDate as CValiDate ,");
		tSQL.append(" a.PayIntv as PayIntv ,");
		tSQL.append(" a.LastPayToDate as LastPayToDate ,");
		tSQL.append(" a.CurPayToDate as CurPayToDate ,");
		tSQL.append(" a.PolYear as PolYear ,");
		tSQL.append(" a.Years as Years ,");
		tSQL.append(" a.PremiumType as PremiumType ,");
		tSQL.append(" a.PayCount as PayCount ,");
		tSQL.append(" a.PayMode as PayMode ,");
		tSQL.append(" a.BankCode as BankCode ,");
		tSQL.append(" a.AccName as AccName ,");
		tSQL.append(" a.BankAccNo as BankAccNo ,");
		tSQL.append("(select sum(-getmoney)  from ljagetendorse where actugetno=a.indexno and getmoney<>0 and db2inst1.LF_risktype(riskcode) <>a.risktype and not exists (select 1 from ljagetendorse where actugetno=a.indexno and getmoney<>0 and db2inst1.LF_risktype(riskcode)='5' and feefinatype='TB') having sum(-getmoney) is not null union select sum(-getmoney)  from ljagetendorse where actugetno=a.indexno and getmoney<>0 and db2inst1.LF_risktype(riskcode) not in(a.risktype ,'5') and exists (select 1 from ljagetendorse where actugetno=a.indexno and getmoney<>0 and db2inst1.LF_risktype(riskcode)='5' and feefinatype='TB') having sum(-getmoney) is not null) as SumActuMoney ,");
		tSQL.append(" a.MothDate as MothDate ,");
		tSQL.append(" a.BusinessDate  as BusinessDate ,");
		tSQL.append(" a.AccountDate as AccountDate ,");
		tSQL.append(" a.CashFlowNo as CashFlowNo ,");
		tSQL.append(" a.FirstYear as FirstYear ,");
		tSQL.append(" a.MarketType as MarketType ,");
		tSQL.append(" a.OperationType as OperationType ,");
		tSQL.append(" a.Budget as Budget ,");
		tSQL.append(" a.Currency as Currency ,");
		tSQL.append(" a.StringInfo01 as StringInfo01 ,");
		tSQL.append(" a.StringInfo02 as StringInfo02 ,");
		tSQL.append(" a.StringInfo03 as StringInfo03 ,");
		tSQL.append(" a.DateInfo01 as DateInfo01 ,");
		tSQL.append(" a.DateInfo02 as DateInfo02 ,");
		tSQL.append(" a.SerialNo as RelatedNo,");
		tSQL.append(" a.EventNo as EventNo ,");
		tSQL.append(" a.State as State ,");
		tSQL.append(" a.ReadState as ReadState ,");
		tSQL.append(" a.CheckFlag as CheckFlag ,");
		tSQL.append(" a.Operator as Operator ,");
		tSQL.append(" '"+PubFun.getCurrentDate()+"' as MakeDate ,");
		tSQL.append(" '"+PubFun.getCurrentTime()+"' as MakeTime ");
		tSQL.append(" from FIAbStandardData a");
		tSQL.append(" where a.costid='F0000000044' and a.listflag='1' and a.feetype in ('WT','CT','XT') and a.secondaryflag = '0' ");
		tSQL.append(" and exists (select 1 from lbpol b where a.contno = b.contno and b.prem <> 0 and LF_risktype(b.riskcode) <> a.risktype " );
		tSQL.append(" and exists(select 1 from lmriskapp c where b.riskcode=c.riskcode and c.Subriskflag = 'M'))");
		tSQL.append(" and state = '00' and CheckFlag='00' and not exists(select 1 from FIAbStandardData d where a.SerialNo = d.RelatedNo and d.CostID in('F0000000044','F000000HL44') and d.SecondaryType='CC' and a.indexno =d.indexno)");
		tSQL.append(" union");
		tSQL.append(" select distinct '' as SerialNo,");
		tSQL.append(" a.NoType as NoType ,");
		tSQL.append(" a.ASerialNo as ASerialNo ,");
		tSQL.append(" '1' as SecondaryFlag ,");
		tSQL.append(" 'CC' as SecondaryType ,");
		tSQL.append(" a.BusTypeID as BusTypeID ,");
		tSQL.append(" a.CostID as CostID ,");
		tSQL.append(" a.BusinessType as BusinessType ,");
		tSQL.append(" a.BusinessDetail as BusinessDetail ,");
		tSQL.append(" a.FeeType as FeeType ,");
		tSQL.append(" a.FeeDetail as FeeDetail ,");
		tSQL.append(" a.IndexCode as IndexCode ,");
		tSQL.append(" a.IndexNo as IndexNo ,");
		tSQL.append(" a.ListFlag as ListFlag ,");
		tSQL.append(" a.GrpContNo as GrpContNo ,");
		tSQL.append(" a.GrpPolNo as GrpPolNo ,");
		tSQL.append(" a.ContNo as ContNo ,");
		tSQL.append(" a.PolNo as PolNo ,");
		tSQL.append(" a.EndorsementNo as EndorsementNo ,");
		tSQL.append(" a.NotesNo as NotesNo ,");
		tSQL.append(" a.FirstYearFlag as FirstYearFlag ,");
		tSQL.append(" a.FirstTermFlag as FirstTermFlag ,");
		tSQL.append(" a.BonusType as BonusType,");
		tSQL.append(" a.DifComFlag as DifComFlag ,");
		tSQL.append(" (select b.riskcode from LJagetEndorse b where a.indexno = b.actugetno and b.getmoney<>0 and db2inst1.LF_risktype(b.riskcode) <> a.risktype fetch first 1 row only ) as RiskCode ,");
		tSQL.append(" a.RiskPeriod as RiskPeriod ,");
		tSQL.append(" LF_risktype((select b.riskcode from LJagetEndorse b where a.indexno = b.actugetno and b.getmoney<>0 and db2inst1.LF_risktype(b.riskcode) <> a.risktype fetch first 1 row only )) as RiskType ,");
		tSQL.append(" a.RiskType1 as RiskType1 ,");
		tSQL.append(" a.RiskType2 as RiskType2 ,");
		tSQL.append(" a.CostCenter as CostCenter ,");
		tSQL.append(" a.ACType as ACType ,");
		tSQL.append(" a.AgentCom as AgentCom ,");
		tSQL.append(" a.AgentGroup as AgentGroup ,");
		tSQL.append(" a.AgentCode as AgentCode ,");
		tSQL.append(" a.CustomerID as CustomerID ,");
		tSQL.append(" a.SupplierNo as SupplierNo ,");
		tSQL.append(" a.SaleChnl as SaleChnl ,");
		tSQL.append(" (select b.riskcode from lbpol b where a.contno = b.contno and b.prem <> 0 and LF_risktype(b.riskcode) <> a.risktype fetch first 1 row only ) as SaleChnlDetail ,");
		tSQL.append(" a.ManageCom  as ManageCom ,");
		tSQL.append(" a.ExecuteCom as ExecuteCom ,");
		tSQL.append(" a.CValiDate as CValiDate ,");
		tSQL.append(" a.PayIntv as PayIntv ,");
		tSQL.append(" a.LastPayToDate as LastPayToDate ,");
		tSQL.append(" a.CurPayToDate as CurPayToDate ,");
		tSQL.append(" a.PolYear as PolYear ,");
		tSQL.append(" a.Years as Years ,");
		tSQL.append(" a.PremiumType as PremiumType ,");
		tSQL.append(" a.PayCount as PayCount ,");
		tSQL.append(" a.PayMode as PayMode ,");
		tSQL.append(" a.BankCode as BankCode ,");
		tSQL.append(" a.AccName as AccName ,");
		tSQL.append(" a.BankAccNo as BankAccNo ,");
		tSQL.append("(select sum(-getmoney)  from ljagetendorse where actugetno=a.indexno and getmoney<>0 and db2inst1.LF_risktype(riskcode) <>a.risktype and not exists (select 1 from ljagetendorse where actugetno=a.indexno and getmoney<>0 and db2inst1.LF_risktype(riskcode)='5' and feefinatype='TB') having sum(-getmoney) is not null union select sum(-getmoney)  from ljagetendorse where actugetno=a.indexno and getmoney<>0 and db2inst1.LF_risktype(riskcode) not in(a.risktype ,'5') and exists (select 1 from ljagetendorse where actugetno=a.indexno and getmoney<>0 and db2inst1.LF_risktype(riskcode)='5' and feefinatype='TB') having sum(-getmoney) is not null) as SumActuMoney ,");
		tSQL.append(" a.MothDate as MothDate ,");
		tSQL.append(" a.BusinessDate  as BusinessDate ,");
		tSQL.append(" a.AccountDate as AccountDate ,");
		tSQL.append(" a.CashFlowNo as CashFlowNo ,");
		tSQL.append(" a.FirstYear as FirstYear ,");
		tSQL.append(" a.MarketType as MarketType ,");
		tSQL.append(" a.OperationType as OperationType ,");
		tSQL.append(" a.Budget as Budget ,");
		tSQL.append(" a.Currency as Currency ,");
		tSQL.append(" a.StringInfo01 as StringInfo01 ,");
		tSQL.append(" a.StringInfo02 as StringInfo02 ,");
		tSQL.append(" a.StringInfo03 as StringInfo03 ,");
		tSQL.append(" a.DateInfo01 as DateInfo01 ,");
		tSQL.append(" a.DateInfo02 as DateInfo02 ,");
		tSQL.append(" a.SerialNo as RelatedNo,");
		tSQL.append(" a.EventNo as EventNo ,");
		tSQL.append(" a.State as State ,");
		tSQL.append(" a.ReadState as ReadState ,");
		tSQL.append(" a.CheckFlag as CheckFlag ,");
		tSQL.append(" a.Operator as Operator ,");
		tSQL.append(" '"+PubFun.getCurrentDate()+"' as MakeDate ,");
		tSQL.append(" '"+PubFun.getCurrentTime()+"' as MakeTime ");
		tSQL.append(" from FIAbStandardData a");
		tSQL.append(" where a.costid='F0000000044' and a.listflag='1' and a.feetype in ('WT','CT','XT') and a.secondaryflag = '0' ");
		tSQL.append(" and exists (select 1 from LJagetEndorse b where a.indexno = b.actugetno and b.getmoney<>0 and db2inst1.LF_risktype(b.riskcode) <> a.risktype and exists(select 1 from lmriskapp c where b.riskcode=c.riskcode and c.Subriskflag <> 'M')) " );
		tSQL.append(" and not exists (select 1 from LJagetEndorse b where a.indexno = b.actugetno and b.getmoney<>0 and db2inst1.LF_risktype(b.riskcode) <> a.risktype and exists(select 1 from lmriskapp c where b.riskcode=c.riskcode and c.Subriskflag='M'))");
		tSQL.append(" and state = '00' and CheckFlag='00' and not exists(select 1 from FIAbStandardData d where a.SerialNo = d.RelatedNo and d.CostID in('F0000000044','F000000HL44') and d.SecondaryType='CC' and a.indexno =d.indexno)");
		
		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				//约定缴费-实收金额为0
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;
				System.out.println(tFIAbStandardDataSchema.getBusTypeID());
				if (tFIAbStandardDataSchema.getBusTypeID().trim()=="Y-BQ-WT-000001"||"Y-BQ-WT-000001".equals(tFIAbStandardDataSchema.getBusTypeID().trim())||tFIAbStandardDataSchema.getBusTypeID().trim()=="Y-BQ-XT-000001"||"Y-BQ-XT-000001".equals(tFIAbStandardDataSchema.getBusTypeID().trim())||tFIAbStandardDataSchema.getBusTypeID().trim()=="Y-BQ-CT-000001"||"Y-BQ-CT-000001".equals(tFIAbStandardDataSchema.getBusTypeID().trim())){
				tFIAbStandardDataSchema.setBusTypeID("Y-BQ-OO-000001");
				System.out.println("我是"+tFIAbStandardDataSchema.getBusTypeID()+"我变成了Y-BQ-OO-000001");
				}else {
					if(tFIAbStandardDataSchema.getFeeType().trim()=="WT"||"WT".equals(tFIAbStandardDataSchema.getFeeType())){
						tFIAbStandardDataSchema.setBusTypeID("Y-BQ-WT-000001");
					}else if(tFIAbStandardDataSchema.getFeeType().trim()=="CT"||"CT".equals(tFIAbStandardDataSchema.getFeeType())){
						tFIAbStandardDataSchema.setBusTypeID("Y-BQ-CT-000001");
					}else if(tFIAbStandardDataSchema.getFeeType().trim()=="XT"||"XT".equals(tFIAbStandardDataSchema.getFeeType())){
						tFIAbStandardDataSchema.setBusTypeID("Y-BQ-XT-000001");
					}
				}
				
				if(tFIAbStandardDataSchema.getFeeType().trim()=="WT"||"WT".equals(tFIAbStandardDataSchema.getFeeType())){
					if(tFIAbStandardDataSchema.getRiskType().trim()=="0"||"0".equals(tFIAbStandardDataSchema.getRiskType())){
						tFIAbStandardDataSchema.setBusTypeID("Y-BQ-OO-000001");
					}else if(tFIAbStandardDataSchema.getRiskType().trim()=="5"||"5".equals(tFIAbStandardDataSchema.getRiskType())){
						tFIAbStandardDataSchema.setBusTypeID("Y-BQ-OO-000001");
						tFIAbStandardDataSchema.setCostID("F000000HL44");
					}
				}else if(tFIAbStandardDataSchema.getFeeType().trim()=="CT"||"CT".equals(tFIAbStandardDataSchema.getFeeType())){
					if(tFIAbStandardDataSchema.getRiskType().trim()=="0"||"0".equals(tFIAbStandardDataSchema.getRiskType())){
						tFIAbStandardDataSchema.setBusTypeID("Y-BQ-OO-000001");
					}else if(tFIAbStandardDataSchema.getRiskType().trim()=="5"||"5".equals(tFIAbStandardDataSchema.getRiskType())){
						tFIAbStandardDataSchema.setBusTypeID("Y-BQ-OO-000001");
						tFIAbStandardDataSchema.setCostID("F000000HL44");
					}
				}else if(tFIAbStandardDataSchema.getFeeType().trim()=="XT"||"XT".equals(tFIAbStandardDataSchema.getFeeType())){
					if(tFIAbStandardDataSchema.getRiskType().trim()=="0"||"0".equals(tFIAbStandardDataSchema.getRiskType())){
						tFIAbStandardDataSchema.setBusTypeID("Y-BQ-OO-000001");
					}else if(tFIAbStandardDataSchema.getRiskType().trim()=="5"||"5".equals(tFIAbStandardDataSchema.getRiskType())){
						tFIAbStandardDataSchema.setBusTypeID("Y-BQ-OO-000001");
						tFIAbStandardDataSchema.setCostID("F000000HL44");
					}
				}
				
				PubSubmit mPubSubmit = new PubSubmit();
				MMap tMap= new MMap();
				VData tInputData = new VData();
				if("".equals(tFIAbStandardDataSchema.getSumActuMoney())){tFIAbStandardDataSchema.setSumActuMoney(0);}
				String tsql=" update FIAbStandardData a set sumactumoney = (select nvl(sum(-getmoney),0) from ljagetendorse where actugetno = a.indexno and getmoney<>0 and LF_risktype(riskcode)=a.risktype ) where serialno='"+tFIAbStandardDataSchema.getRelatedNo()+"' and a.state = '00'";
				String tsqll=" update FIAbStandardData a set a.bustypeid = 'Y-BQ-OO-000001',risktype = '0' where a.indexno='"+tFIAbStandardDataSchema.getIndexNo()+"' and a.indexcode = '03' and costid in ('F00000000GB','F000000SEGB') and a.state = '00'";			
				tMap.put(tsql, "UPDATE");
				tMap.put(tsqll, "UPDATE");
				tInputData.add(tMap);
		        if (!mPubSubmit.submitData(tInputData, ""))
		        {
		        	throw new  Exception("SecondaryRule_RISKCODE_CC:更新错误！");
		        	
		        }
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		
	}

}
