package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;

/**
 * <p>Title: 二次处理规则--首期年度化个险（税额）</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company:sinosoft </p>
 * @author GCX
 * @version 3.0
 */
public class SecondaryRule_NDHGXSE extends StandSecondaryRule {
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();
		 
		//先生效		
		tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,'1' as SecondaryFlag,'GS' as SecondaryType,a.BusTypeID,a.CostID,a.BusinessType,");
		tSQL.append(" a.BusinessDetail, a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,");
		tSQL.append(" a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType,");
		tSQL.append(" a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl,");
		tSQL.append(" a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years,");
		tSQL.append(" a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,");
		tSQL.append(" a.SumActuMoney*LF_LastPayCount(a.CurPayToDate,(select b.payenddate from lcpol b where b.contno=a.contno and b.riskcode=a.riskcode union all select b.payenddate from lbpol b where b.contno=a.contno and b.riskcode=a.riskcode fetch  first  rows only),a.PayIntv) SumActuMoney,");
		tSQL.append(" a.MothDate,a.BusinessDate,a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02,");
		tSQL.append(" a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate,");
		tSQL.append(" a.MakeTime from FIAbStandardData a where a.NoType = '2' and a.ListFlag='1' and a.BusinessType = '03' and a.Businessdetail='031' ");
		tSQL.append(" and a.riskperiod in ('M','S') and a.riskcode not in(select distinct  riskcode  from lmrisk where rnewflagb ='Y') and a.payintv not in (0,12,-1) and a.CheckFlag='00' and a.feetype in ('ZE','SZ') and a.state = '00' ");
		tSQL.append(" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.CostID='F0000000SE2')");
		

		tSQL.append(" union all ");
		
		//先开票		
		tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,'1' as SecondaryFlag,'GS' as SecondaryType,a.BusTypeID,a.CostID,a.BusinessType,");
		tSQL.append(" a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo,");
		tSQL.append(" a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType,");
		tSQL.append(" a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl,");
		tSQL.append(" a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years,");
		tSQL.append(" a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,");
		tSQL.append(" (select sum(MoneyTax) from LYPremSeparateDetail d where d.moneytype = '01' and d.moneyno = a.indexno and otherstate = '01' and d.riskcode = a.riskcode) *LF_LastPayCount(a.CurPayToDate,(select b.payenddate from lcpol b where b.contno=a.contno and b.riskcode=a.riskcode),a.PayIntv) SumActuMoney,");
		tSQL.append(" a.MothDate,a.BusinessDate,a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02,");
		tSQL.append(" a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate,");
		tSQL.append(" a.MakeTime from FIAbStandardData a where a.NoType = '2' and a.ListFlag='1' and a.BusinessType = '03' and a.Businessdetail='031' ");
		tSQL.append(" and a.riskperiod in ('M','S') and a.riskcode not in(select distinct riskcode from lmrisk where rnewflagb ='Y') and a.payintv not in (0,12,-1) and a.CheckFlag='00' and a.feetype ='ZC' and a.state = '00' ");
		tSQL.append(" and exists (select 1 from FIAbStandardData f where f.contno = a.contno and f.feetype = 'FP' and f.indexcode = '12' and f.BusinessType = '13' and f.Businessdetail='011')");
		tSQL.append(" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.CostID='F0000000SE2')");
		
		return tSQL.toString();
	}
	
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());

			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				//个险短险普通险非保证续保年度化
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());

					tFIAbStandardDataSchema.setBusinessType("10");
					tFIAbStandardDataSchema.setBusinessDetail("101");
					tFIAbStandardDataSchema.setCostID("F0000000SE2");
					if("ZE".equals(tFIAbStandardDataSchema.getFeeType())||"FP".equals(tFIAbStandardDataSchema.getFeeType()))
					tFIAbStandardDataSchema.setFeeType("ZE");
					else if("SZ".equals(tFIAbStandardDataSchema.getFeeType()))
					tFIAbStandardDataSchema.setFeeType("SZ");
					tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;
					tFIAbStandardDataSchema.setBusTypeID("O-NC-000003");
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
