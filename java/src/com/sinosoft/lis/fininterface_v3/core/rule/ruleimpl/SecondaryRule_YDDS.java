package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--异地代收（非邮储）</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class SecondaryRule_YDDS extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

//		tSQL.append(" select * from FIAbStandardData a where a.NoType = '1' and a.state = '00' and a.BusinessType = '01' and a.CheckFlag='00' ");
//		tSQL.append(" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusinessType='07')");
//		tSQL.append(" and exists(select 1 from FIAboriginalMainAtt b where a.ASerialNo=b.MSerialNo and b.DifComFlag = '1' and b.YCFlag='0')");
//
//		tSQL.append(" union all");

		tSQL.append(" select * from FIAbStandardData a where a.NoType = '2' and a.state = '00' and a.BusinessType = '01' and a.CheckFlag='00' and a.businessdetail <> '036' ");
		tSQL.append(" and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusinessType='07')");
		tSQL.append(" and exists(select 1 from FIAboriginalGenAtt b where a.ASerialNo=b.ASerialNo and b.DifComFlag = '1' and b.YCFlag='0')");

		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			int tSize = pFIAbStandardDataSet.size();
			int size = pFIAbStandardDataSet.size()*2;
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(size);
			
			for(int i=1;i<=tSize;i++)			
			{
				//(1)实际收费机构费用
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setBusinessType("07");//异地代收
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;
				
				String manageCom = tFIAbStandardDataSchema.getManageCom();  //收费机构
				String executeCom = tFIAbStandardDataSchema.getExecuteCom();//保单机构
				//(2)对方机构费用
				FIAbStandardDataSchema tcFIAbStandardDataSchema = (FIAbStandardDataSchema)tFIAbStandardDataSchema.clone();
				

				tcFIAbStandardDataSchema.setManageCom(executeCom);
				tcFIAbStandardDataSchema.setExecuteCom(manageCom);
				tcFIAbStandardDataSchema.setSerialNo(tSerialNo[size-i]) ;//
				
				if("011".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
//					//健管异地
//					if("S-NC-000003".equals(tFIAbStandardDataSchema.getBusTypeID()))
//					{
//						tFIAbStandardDataSchema.setBusinessDetail("071");//新单收费-健管异地
//						tFIAbStandardDataSchema.setBusTypeID("DS-NC-000003");
//						tFIAbStandardDataSchema.setCostID("F000000000P");
//						tFIAbStandardDataSchema.setManageCom(executeCom);
//						tFIAbStandardDataSchema.setExecuteCom(manageCom);
//					}
//					else {
						
					if ("ZC".equals(tFIAbStandardDataSchema.getFeeDetail()))
					{
						tFIAbStandardDataSchema.setBusinessDetail("071");//新单异地收费
						tFIAbStandardDataSchema.setBusTypeID("DS-NC-000001");
						tFIAbStandardDataSchema.setCostID("F000000000M");
						
						tcFIAbStandardDataSchema.setBusinessDetail("071");//新单异地收费
						tcFIAbStandardDataSchema.setBusTypeID("DS-NC-000001");
						tcFIAbStandardDataSchema.setCostID("F000000000P");}
					
					else if ("SE".equals(tFIAbStandardDataSchema.getFeeDetail()))
					{
						tFIAbStandardDataSchema.setBusinessDetail("071");//新单异地收费
						tFIAbStandardDataSchema.setBusTypeID("DS-NC-000001");
						tFIAbStandardDataSchema.setCostID("F0000000SEM");
						
						tcFIAbStandardDataSchema.setBusinessDetail("071");//新单异地收费
						tcFIAbStandardDataSchema.setBusTypeID("DS-NC-000001");
						tcFIAbStandardDataSchema.setCostID("F0000000SEP");
					}
				}
//					}
				else if("012".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
					if ("ZC".equals(tFIAbStandardDataSchema.getFeeDetail())){
					tFIAbStandardDataSchema.setBusinessDetail("072");//续期异地收费
					tFIAbStandardDataSchema.setBusTypeID("DS-XQ-000001");
					tFIAbStandardDataSchema.setCostID("F000000000M");
					
					tcFIAbStandardDataSchema.setBusinessDetail("072");//续期异地收费
					tcFIAbStandardDataSchema.setBusTypeID("DS-XQ-000001");
					tcFIAbStandardDataSchema.setCostID("F000000000P");
					}
					else if ("SE".equals(tFIAbStandardDataSchema.getFeeDetail())){
						tFIAbStandardDataSchema.setBusinessDetail("072");//续期异地收费
						tFIAbStandardDataSchema.setBusTypeID("DS-XQ-000001");
						tFIAbStandardDataSchema.setCostID("F0000000SEM");
						
						tcFIAbStandardDataSchema.setBusinessDetail("072");//续期异地收费
						tcFIAbStandardDataSchema.setBusTypeID("DS-XQ-000001");
						tcFIAbStandardDataSchema.setCostID("F0000000SEP");
					
				}
				}
			
				else if("013".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
					tFIAbStandardDataSchema.setBusinessDetail("073");//保全异地收费
					tcFIAbStandardDataSchema.setBusinessDetail("073");
					if("S-BQ-000005".equals(tFIAbStandardDataSchema.getBusTypeID()))
					{	
						tFIAbStandardDataSchema.setBusTypeID("DS-BQ-000003"); //保单质押贷款-还款（异地）
						tcFIAbStandardDataSchema.setBusTypeID("DS-BQ-000003");
						if("5".equals(tFIAbStandardDataSchema.getRiskType())){
							if("RFBF".equals(tFIAbStandardDataSchema.getFeeDetail()))
							{
								tFIAbStandardDataSchema.setCostID("F0000HLRF1P");//保户质押贷款
								tcFIAbStandardDataSchema.setCostID("F00000HLRF1");
							}
							else if ("RFLXSE".equals(tFIAbStandardDataSchema.getFeeDetail())){
								
								tFIAbStandardDataSchema.setCostID("F00HLSERF2P");
								tcFIAbStandardDataSchema.setCostID("F000HLSERF2");//利息收入的税
							}
							else
							{
								tFIAbStandardDataSchema.setCostID("F0000HLRF2P");
								tcFIAbStandardDataSchema.setCostID("F00000HLRF2");//利息收入
							}
						}
						else {
						if("RFBF".equals(tFIAbStandardDataSchema.getFeeDetail()))
						{
							tFIAbStandardDataSchema.setCostID("F000000RF1P");
							tcFIAbStandardDataSchema.setCostID("F0000000RF1");//保户质押贷款
						}
						else if ("RFLXSE".equals(tFIAbStandardDataSchema.getFeeDetail())){
							
							tFIAbStandardDataSchema.setCostID("F0000SERF2P");
							tcFIAbStandardDataSchema.setCostID("F00000SERF2");//利息收入的税
						}
						else
						{
							tFIAbStandardDataSchema.setCostID("F000000RF2P");
							tcFIAbStandardDataSchema.setCostID("F0000000RF2");//利息收入
						}}
						
					}					
					else
					{
						if ("ZC".equals(tFIAbStandardDataSchema.getFeeDetail())){
						tFIAbStandardDataSchema.setBusTypeID("DS-BQ-000001");
						tFIAbStandardDataSchema.setCostID("F000000000M");
						
						tcFIAbStandardDataSchema.setBusinessDetail("073");//保全异地收费
						tcFIAbStandardDataSchema.setBusTypeID("DS-BQ-000001");
						tcFIAbStandardDataSchema.setCostID("F000000000P");
						}
						else if ("SE".equals(tFIAbStandardDataSchema.getFeeDetail())){
							
						
							tFIAbStandardDataSchema.setBusTypeID("DS-BQ-000001");
							tFIAbStandardDataSchema.setCostID("F0000000SEM");
							
							tcFIAbStandardDataSchema.setBusinessDetail("073");//保全异地收费
							tcFIAbStandardDataSchema.setBusTypeID("DS-BQ-000001");
							tcFIAbStandardDataSchema.setCostID("F0000000SEP");}
						
						else if("LRSE".equals(tFIAbStandardDataSchema.getFeeDetail())){
							tFIAbStandardDataSchema.setBusTypeID("DS-BQ-000001");
							tFIAbStandardDataSchema.setCostID("F00000LRSEM");
							
							tcFIAbStandardDataSchema.setBusinessDetail("073");//保全异地收费保单遗失补发
							tcFIAbStandardDataSchema.setBusTypeID("DS-BQ-000001");
							tcFIAbStandardDataSchema.setCostID("F00000LRSEP");
							
						}
						else if ("FX".equals(tFIAbStandardDataSchema.getFeeDetail())){
								
								tFIAbStandardDataSchema.setBusTypeID("DS-BQ-000001");
								tFIAbStandardDataSchema.setCostID("F0000000FXM");
								
								tcFIAbStandardDataSchema.setBusinessDetail("073");//保全异地收费
								tcFIAbStandardDataSchema.setBusTypeID("DS-BQ-000001");
								tcFIAbStandardDataSchema.setCostID("F0000000FXP");
								}
						else if ("FXLX".equals(tFIAbStandardDataSchema.getFeeDetail())){	
									tFIAbStandardDataSchema.setBusTypeID("DS-BQ-000001");
									tFIAbStandardDataSchema.setCostID("F0000000LXM");
									
									tcFIAbStandardDataSchema.setBusinessDetail("073");//保全异地收费
									tcFIAbStandardDataSchema.setBusTypeID("DS-BQ-000001");
									tcFIAbStandardDataSchema.setCostID("F0000000LXP");						
						}	
//					else if("DBBB".equals(tFIAbStandardDataSchema.getFeeDetail())){
//						tFIAbStandardDataSchema.setBusTypeID("DS-BQ-000001");
//						tFIAbStandardDataSchema.setCostID("F000000DBBM");
//						tcFIAbStandardDataSchema.setBusinessDetail("073");//保全异地收费
//						tcFIAbStandardDataSchema.setBusTypeID("DS-BQ-000001");
//						tcFIAbStandardDataSchema.setCostID("F000000DBBP");					
//						}
//					else if("FDBBB".equals(tFIAbStandardDataSchema.getFeeDetail())){
//						tFIAbStandardDataSchema.setBusTypeID("DS-BQ-000001");
//						tFIAbStandardDataSchema.setCostID("F00000FDBBM");
//						tcFIAbStandardDataSchema.setBusinessDetail("073");//保全异地收费
//						tcFIAbStandardDataSchema.setBusTypeID("DS-BQ-000001");
//						tcFIAbStandardDataSchema.setCostID("F00000FDBBP");					
//						}
					}
				}
				else if("015".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
					tFIAbStandardDataSchema.setBusinessDetail("075");//预付赔款-回收-异地
					tFIAbStandardDataSchema.setBusTypeID("DS-LP-000001");
					tFIAbStandardDataSchema.setCostID("F000000000P");
					tFIAbStandardDataSchema.setManageCom(executeCom);
					tFIAbStandardDataSchema.setExecuteCom(manageCom);
				}
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				
				if(!"DS-NC-000003".equals(tFIAbStandardDataSchema.getBusTypeID())&&
						!"DS-LP-000001".equals(tFIAbStandardDataSchema.getBusTypeID()))
				{
					pFIAbStandardDataSet.add(tcFIAbStandardDataSchema);	
				}			
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		SecondaryRule_YDDS tSecondaryRule_YDDS = new SecondaryRule_YDDS();
		
		
		//tSecondaryRule_YD.ruleExe(cInputData, cOperate)
		
	}

}
