package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/**
 * <p>Title: 二次处理规则--保全退保年度化反冲（净保费和税额）</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2018</p>
 *
 * <p>Company: </p>
 *
 * @author gcx
 * @version 3.0
 */
public class SecondaryRule_BQFCNDHGX extends StandSecondaryRule{

	
	public String prepareRule()
	{		
		StringBuffer tSQL = new StringBuffer();
		

		tSQL.append(" select distinct '' as SerialNo ,a.NoType,a.ASerialNo as ASerialNo,'1' as SecondaryFlag,'GX' as SecondaryType,a.BusTypeID,a.CostID,a.BusinessType," +
				"a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo as ContNo,a.PolNo as PolNo," +
				"a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType," +
				"a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl," +
				"a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years," +
				"a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,-LF_PayMoney6(a.PolNo, (select b.PayToDate from lbpol b where a.contno=b.contno and a.riskcode=b.riskcode), (select b.PayEndDate from lbpol b where a.contno=b.contno and a.riskcode=b.riskcode), a.PayIntv,'BF') SumActuMoney,a.MothDate,a.BusinessDate," +
				"a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02," +
				"a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate," +
				"a.MakeTime from FIAbStandardData a where a.NoType = '2' and a.state = '00' and a.CheckFlag='00' and ListFlag='1' and a.BusinessType = '04' and a.Businessdetail='042' " +
				" and a.riskperiod in ('M','S') and  a.riskcode not in(select  distinct   riskcode    from  lmrisk  where   rnewflagb ='Y'  ) and a.payintv not in (0,12,-1) and a.FeeType in ('WT','XT','CT')" +
				" and not exists(select 1 from FIAbStandardData c where c.costid = 'F0000000025' and c.BusinessDetail='103' and a.indexcode = c.indexcode and a.indexno = c.indexno) " +
				" and exists (select 1 from ljapay d where d.incomeno = a.contno and d.duefeetype = '0')" );

		
		tSQL.append(" union all ");
		
		tSQL.append(" select distinct '' as SerialNo ,a.NoType,a.ASerialNo as ASerialNo,'1' as SecondaryFlag,'GS' as SecondaryType,a.BusTypeID,a.CostID,a.BusinessType," +
				"a.BusinessDetail,a.FeeType,a.FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo as ContNo,a.PolNo as PolNo," +
				"a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType," +
				"a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl," +
				"a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years," +
				"a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,-LF_PayMoney6(a.PolNo, (select b.PayToDate from lbpol b where a.contno=b.contno and a.riskcode=b.riskcode), (select b.PayEndDate from lbpol b where a.contno=b.contno and a.riskcode=b.riskcode), a.PayIntv,'SE') SumActuMoney,a.MothDate,a.BusinessDate," +
				"a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02," +
				"a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate," +
				"a.MakeTime from FIAbStandardData a where a.NoType = '2' and a.state = '00' and a.CheckFlag='00' and ListFlag='1' and a.BusinessType = '04' and a.Businessdetail='042' " +
				" and a.riskperiod in ('M','S') and  a.riskcode not in(select  distinct   riskcode    from  lmrisk  where  rnewflagb ='Y') and a.payintv not in (0,12,-1) and a.feetype in ('ZE','EE') and exists (select 1 from FIAboriginalGenAtt d where a.indexno = d.indexno and a.indexcode = d.indexcode and d.FeeType in ('WT','XT','CT')) " +
				" and not exists(select 1 from FIAbStandardData c where c.costid = 'F0000000SE2' and c.BusinessDetail='103' and a.indexcode = c.indexcode and a.indexno = c.indexno) " +
				" and exists (select 1 from ljapay d where d.incomeno = a.contno and d.duefeetype = '0')" );
					
		
		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				//个险短险普通险非保证续保期缴反冲 年度化保费和税额  
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setBusinessType("10");
				tFIAbStandardDataSchema.setBusinessDetail("103");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;
				if("EE".equals(tFIAbStandardDataSchema.getFeeType()) || "ZE".equals(tFIAbStandardDataSchema.getFeeType()))
					tFIAbStandardDataSchema.setCostID("F0000000SE2");
				else 
					tFIAbStandardDataSchema.setCostID("F0000000025");
					tFIAbStandardDataSchema.setBusTypeID("Y-BQ-FC-000003");				
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_BQFCNDHGX tSecondaryRule_BQFCNDHGX = new SecondaryRule_BQFCNDHGX();
	
		
	}

}
