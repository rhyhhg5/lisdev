
package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--分红险退保进账户保单红利计算</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class SecondaryRule_BQFHTBJZH_HL extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

		tSQL.append(" select a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType," +
				"a.BusinessDetail,a.FeeType,'HL' as FeeDetail,a.IndexCode,a.IndexNo,a.ListFlag,a.GrpContNo,a.GrpPolNo,a.ContNo,a.PolNo," +
				"a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,a.RiskPeriod,a.RiskType," +
				"a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,a.SupplierNo,a.SaleChnl," +
				"a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,a.PolYear,a.Years," +
				"a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo,LF_GETMONEY('HL', 'HLLX', a.indexno) as SumActuMoney,a.MothDate,a.BusinessDate," +
				"a.AccountDate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,a.Currency,a.StringInfo01,a.StringInfo02," +
				"a.StringInfo03,a.DateInfo01,a.DateInfo02,a.RelatedNo,a.EventNo,a.State,a.ReadState,a.CheckFlag,a.Operator,a.MakeDate," +
				"a.MakeTime from FIAbStandardData a where a.NoType = '2' " +
				"and a.state = '00' and a.ListFlag = '1' and a.BusinessType = '04' and a.Businessdetail='044' and a.risktype = '5'" +
				"and a.SecondaryFlag = '1'  and a.feedetail = 'HLLX' and a.feetype = 'CT'" +
				"and exists(select 1 from FIAbStandardData d where a.grpcontno = d.grpcontno " +
				"and d.BusinessType = '04' and d.Businessdetail = '042' and a.indexno = d.indexno  and a.indexcode = d.indexcode and d.feetype = 'YEI' and d.feedetail = 'YEI' fetch first 1 row only)" +
				"and a.CheckFlag='00' " +
				"and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.FeeDetail = 'HL' ) ");

		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				tFIAbStandardDataSchema.setBusinessType("04");
				tFIAbStandardDataSchema.setBusinessDetail("044");
				tFIAbStandardDataSchema.setBusTypeID("Y-BQ-OO-000002");
				tFIAbStandardDataSchema.setCostID("F000000HLLJ");
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//	
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_BQFHTBJZH_HL tSecondaryRule_BQFHTBJZH_HL = new SecondaryRule_BQFHTBJZH_HL();
		System.out.println(tSecondaryRule_BQFHTBJZH_HL.prepareRule());
		
	}

}



