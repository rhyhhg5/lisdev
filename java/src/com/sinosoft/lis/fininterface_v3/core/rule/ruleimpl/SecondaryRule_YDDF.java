package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--异地代付（非邮储）</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class SecondaryRule_YDDF extends StandSecondaryRule{

	
	public String prepareRule()
	{
		StringBuffer tSQL = new StringBuffer();

		tSQL.append(" select * from FIAbStandardData a where a.NoType = '1' and a.state = '00' and a.BusinessType = '02' and a.CheckFlag='00' ");
		tSQL.append(" and a.secondaryflag='0' and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusinessType='06')");
		tSQL.append(" and exists(select 1 from FIAboriginalMainAtt b where a.ASerialNo=b.MSerialNo and DifComFlag = '1' and YCFlag='0' )");

		tSQL.append(" union all ");

		tSQL.append(" select * from FIAbStandardData a where a.NoType = '2' and a.state = '00' and a.BusinessType = '02' and a.CheckFlag='00' ");
		tSQL.append(" and a.secondaryflag='0' and not exists(select 1 from FIAbStandardData c where a.SerialNo = c.RelatedNo and c.BusinessType='06')");
		tSQL.append(" and exists(select 1 from FIAboriginalGenAtt b where a.ASerialNo=b.ASerialNo and DifComFlag = '1' and YCFlag='0' and b.subphysicaltable not in('SocialSecurityFee_F'))");

		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			int siz = pFIAbStandardDataSet.size();
			int size = pFIAbStandardDataSet.size()*2;
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(size);
			
			for(int i=1;i<=siz;i++)			
			{
				//(1)实际收费机构费用
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setBusinessType("06");//异地代收
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//
				
				//(2)对方机构费用
				FIAbStandardDataSchema tcFIAbStandardDataSchema = (FIAbStandardDataSchema)tFIAbStandardDataSchema.clone();
				
				String manageCom = tcFIAbStandardDataSchema.getManageCom();
				String executeCom = tcFIAbStandardDataSchema.getExecuteCom();
				tcFIAbStandardDataSchema.setManageCom(executeCom);
				tcFIAbStandardDataSchema.setExecuteCom(manageCom);
				tcFIAbStandardDataSchema.setSerialNo(tSerialNo[size-i]) ;//
				
				if("021".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
					if ("ZC".equals(tFIAbStandardDataSchema.getFeeType()))
					{
					tFIAbStandardDataSchema.setBusinessDetail("061");//暂收退费异地
					tFIAbStandardDataSchema.setBusTypeID("DF-NC-000001");//暂收退费-邮储\建行总部代付
					tFIAbStandardDataSchema.setCostID("F000000000M");
					
					tcFIAbStandardDataSchema.setBusinessDetail("061");//保全代付
					tcFIAbStandardDataSchema.setBusTypeID("DF-NC-000001");
					tcFIAbStandardDataSchema.setCostID("F000000000P");
					}
					else if ("SEZT".equals(tFIAbStandardDataSchema.getFeeType()))
					{
						tFIAbStandardDataSchema.setBusinessDetail("061");//暂收退费异地
						tFIAbStandardDataSchema.setBusTypeID("DF-NC-000001");//暂收退费-邮储\建行总部代付
						tFIAbStandardDataSchema.setCostID("F000000FSEM");
						
						tcFIAbStandardDataSchema.setBusinessDetail("061");//保全代付
						tcFIAbStandardDataSchema.setBusTypeID("DF-NC-000001");
						tcFIAbStandardDataSchema.setCostID("F000000FSEP");	
					}
				}
				else if("030".equals(tFIAbStandardDataSchema.getBusinessDetail())){
					tFIAbStandardDataSchema.setBusinessDetail("062");//保全代付
					tcFIAbStandardDataSchema.setBusinessDetail("062");//保全代付
					
					tFIAbStandardDataSchema.setBusTypeID("DF-BQ-000001");
					tFIAbStandardDataSchema.setCostID("F0000000SBP");
					tcFIAbStandardDataSchema.setBusTypeID("DF-BQ-000001");
					tcFIAbStandardDataSchema.setCostID("F0000000SBM");
				}
				else if("020".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
					tFIAbStandardDataSchema.setBusinessDetail("062");//保全代付
					tFIAbStandardDataSchema.setBusTypeID("DF-BQ-000010");//暂收退费-邮储\建行总部代付
					tFIAbStandardDataSchema.setCostID("F000000000P");
					tcFIAbStandardDataSchema.setBusinessDetail("062");//保全代付
					tcFIAbStandardDataSchema.setBusTypeID("DF-BQ-000010");//暂收退费-邮储\建行总部代付
					tcFIAbStandardDataSchema.setCostID("F000000000M");
				}
				else if("022".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
					tFIAbStandardDataSchema.setBusinessDetail("062");//保全代付
					tcFIAbStandardDataSchema.setBusinessDetail("062");//保全代付
					
					if("F-BQ-000005".equals(tFIAbStandardDataSchema.getBusTypeID()))
					{
						tFIAbStandardDataSchema.setBusTypeID("DF-BQ-000005");//保单质押贷款-贷款-异地
						tFIAbStandardDataSchema.setCostID("F000000000P");
						tFIAbStandardDataSchema.setManageCom(executeCom);
						tFIAbStandardDataSchema.setExecuteCom(manageCom);
						
					}
					else if("F-BQ-000011".equals(tFIAbStandardDataSchema.getBusTypeID())){
						//保全结余返还-大病非大病
						
						if("SFDB".equals(tFIAbStandardDataSchema.getFeeDetail()))
						{
							//保单红利-利息
							tFIAbStandardDataSchema.setBusTypeID("DF-BQ-000001");
							tcFIAbStandardDataSchema.setBusTypeID("DF-BQ-000001");
							tFIAbStandardDataSchema.setCostID("M000000SFDB");
							tcFIAbStandardDataSchema.setCostID("P000000SFDB");	
							
						}
						else if("SFFDB".equals(tFIAbStandardDataSchema.getFeeDetail()))
						{
							//保单红利-利息
							tFIAbStandardDataSchema.setBusTypeID("DF-BQ-000001");
							tcFIAbStandardDataSchema.setBusTypeID("DF-BQ-000001");
							tFIAbStandardDataSchema.setCostID("M00000SFFDB");
							tcFIAbStandardDataSchema.setCostID("P00000SFFDB");	
							
						}
						else
						{
							tFIAbStandardDataSchema.setBusTypeID("DF-BQ-000001");
							tFIAbStandardDataSchema.setCostID("F000000000M");
							tcFIAbStandardDataSchema.setBusTypeID("DF-BQ-000001");
							tcFIAbStandardDataSchema.setCostID("F000000000P");
						}
						
						
					}
					
					else if("F-BQ-000006".equals(tFIAbStandardDataSchema.getBusTypeID()))
					{
						//分红险付费(退保\现金领取)
						tFIAbStandardDataSchema.setBusTypeID("DF-BQ-000007");
						tcFIAbStandardDataSchema.setBusTypeID("DF-BQ-000007");
						if("HLLX".equals(tFIAbStandardDataSchema.getFeeDetail())
								||"LX".equals(tFIAbStandardDataSchema.getFeeDetail()))
						{
							//保单红利-利息
							tFIAbStandardDataSchema.setCostID("M000000HLLX");
							tcFIAbStandardDataSchema.setCostID("P000000HLLX");	
						}
						else if("HLBF".equals(tFIAbStandardDataSchema.getFeeDetail()))
						{
							//现金领取
							tFIAbStandardDataSchema.setCostID("M000000HLLQ");
							tcFIAbStandardDataSchema.setCostID("P000000HLLQ");	
						}
						else if("HL".equals(tFIAbStandardDataSchema.getFeeDetail()))
						{
							//保单红利-累积生息
							tFIAbStandardDataSchema.setCostID("M000000HLLJ");
							tcFIAbStandardDataSchema.setCostID("P000000HLLJ");	
						}
						else
						{
							//退保金
							tFIAbStandardDataSchema.setCostID("M000000HLTB");
							tcFIAbStandardDataSchema.setCostID("P000000HLTB");	
						}
					}
					else if("F-BQ-000007".equals(tFIAbStandardDataSchema.getBusTypeID()))
					{
						tFIAbStandardDataSchema.setBusTypeID("DF-BQ-000008");
						tFIAbStandardDataSchema.setCostID("F000000000M");
						tcFIAbStandardDataSchema.setBusTypeID("DF-BQ-000008");
						tcFIAbStandardDataSchema.setCostID("F000000000P");
					}
					else
					{
						tFIAbStandardDataSchema.setBusTypeID("DF-BQ-000001");
						tFIAbStandardDataSchema.setCostID("F000000000M");
						tcFIAbStandardDataSchema.setBusTypeID("DF-BQ-000001");
						tcFIAbStandardDataSchema.setCostID("F000000000P");
					}
				}
				
				else if("023".equals(tFIAbStandardDataSchema.getBusinessDetail()))
					{
//					理赔付费--异地代付
					tFIAbStandardDataSchema.setBusinessDetail("063");//理赔代付
					tcFIAbStandardDataSchema.setBusinessDetail("063");//理赔代付
					if("F-LP-000001".equals(tFIAbStandardDataSchema.getBusTypeID()))
					{
						//理赔付费-传统险
						tFIAbStandardDataSchema.setBusTypeID("DF-LP-000001");
						tFIAbStandardDataSchema.setCostID("F000000000M");
						tcFIAbStandardDataSchema.setBusTypeID("DF-LP-000001");
						tcFIAbStandardDataSchema.setCostID("F000000000P");
					}else if("F-LP-000006".equals(tFIAbStandardDataSchema.getBusTypeID())){
						//理赔付费-预付赔款-异地(传统险)
						tFIAbStandardDataSchema.setBusTypeID("DF-LP-000001");
						tFIAbStandardDataSchema.setCostID("F000000000M");
						tcFIAbStandardDataSchema.setBusTypeID("DF-LP-000001");
						tcFIAbStandardDataSchema.setCostID("F000000000P");
					}else if("F-LP-000002".equals(tFIAbStandardDataSchema.getBusTypeID())){
						//理赔付费-万能\特需
						tFIAbStandardDataSchema.setBusTypeID("DF-LP-000002");
						tFIAbStandardDataSchema.setCostID("F000000000M");
						tcFIAbStandardDataSchema.setBusTypeID("DF-LP-000002");
						tcFIAbStandardDataSchema.setCostID("F000000000P");
					}else if("F-LP-000008".equals(tFIAbStandardDataSchema.getBusTypeID())){
						//理赔付费-预付赔款-异地(账户险)
						tFIAbStandardDataSchema.setBusTypeID("DF-LP-000002");
						tFIAbStandardDataSchema.setCostID("F000000000M");
						tcFIAbStandardDataSchema.setBusTypeID("DF-LP-000002");
						tcFIAbStandardDataSchema.setCostID("F000000000P");
					}else if("F-LP-000010".equals(tFIAbStandardDataSchema.getBusTypeID())){
						//理赔付费-守望B-异地
						tFIAbStandardDataSchema.setBusTypeID("DF-LP-000002");
						tFIAbStandardDataSchema.setCostID("F00000LPSWM");
						tcFIAbStandardDataSchema.setBusTypeID("DF-LP-000002");
						tcFIAbStandardDataSchema.setCostID("F00000LPSWP");
					}
					
					}
				else if("024".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
					tFIAbStandardDataSchema.setBusinessDetail("064");//余额领取代付
					tFIAbStandardDataSchema.setBusTypeID("DF-LQ-000001");
					tFIAbStandardDataSchema.setCostID("F000000000M");
					
					tcFIAbStandardDataSchema.setBusinessDetail("064");//余额领取代付
					tcFIAbStandardDataSchema.setBusTypeID("DF-LQ-000001");
					tcFIAbStandardDataSchema.setCostID("F000000000P");
				}
				else if("025".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{ 
					tFIAbStandardDataSchema.setBusinessDetail("065");//手续费代付
					tFIAbStandardDataSchema.setBusTypeID("DF-CG-000001");
					tFIAbStandardDataSchema.setCostID("F000000000M");
					
					tcFIAbStandardDataSchema.setBusinessDetail("065");//手续费代付
					tcFIAbStandardDataSchema.setBusTypeID("DF-CG-000001");
					tcFIAbStandardDataSchema.setCostID("F000000000P");
				}
				else if("029".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
					tFIAbStandardDataSchema.setBusinessDetail("065");//互动管理手续费代付
					tFIAbStandardDataSchema.setBusTypeID("DF-CG-000002");
					tFIAbStandardDataSchema.setCostID("F000000000M");
					
					tcFIAbStandardDataSchema.setBusinessDetail("065");//互动管理手续费代付
					tcFIAbStandardDataSchema.setBusTypeID("DF-CG-000002");
					tcFIAbStandardDataSchema.setCostID("F000000000P");
				}
				else if("028".equals(tFIAbStandardDataSchema.getBusinessDetail()))
				{
//					理赔分红险含红利利息及红利保费数据实付流转规则 -- 20160224
					tFIAbStandardDataSchema.setBusinessDetail("065");//理赔代付
					tcFIAbStandardDataSchema.setBusinessDetail("065");//理赔代付	
					
					if ("F-LP-000HL1".equals(tFIAbStandardDataSchema.getBusTypeID()))
					{

						if("HLLX".equals(tFIAbStandardDataSchema.getFeeDetail()))
						{
						//理赔付费-分红险
						tFIAbStandardDataSchema.setBusTypeID("DF-LP-000HL1");
						tFIAbStandardDataSchema.setCostID("M000000HLLX");
						tcFIAbStandardDataSchema.setBusTypeID("DF-LP-000HL1");
						tcFIAbStandardDataSchema.setCostID("P000000HLLX");
						}
						else if ("HLBF".equals(tFIAbStandardDataSchema.getFeeDetail()))
						{
						tFIAbStandardDataSchema.setBusTypeID("DF-LP-000HL1");
						tFIAbStandardDataSchema.setCostID("M000000HLLQ");
						tcFIAbStandardDataSchema.setBusTypeID("DF-LP-000HL1");
						tcFIAbStandardDataSchema.setCostID("P000000HLLQ");							
						}
					  }
				}
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				
				if(!"DF-BQ-000005".equals(tFIAbStandardDataSchema.getBusTypeID()))
				{
					pFIAbStandardDataSet.add(tcFIAbStandardDataSchema);		
				}			
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		SecondaryRule_YDDF tSecondaryRule_YDDF = new SecondaryRule_YDDF();
		
		
		//tSecondaryRule_YD.ruleExe(cInputData, cOperate)
		
	}

}
