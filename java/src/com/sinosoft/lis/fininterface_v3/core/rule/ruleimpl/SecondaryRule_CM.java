package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

/**
 * <p>Title: 二次处理规则-实收和应付客户资料变更（个、团）</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2018</p>
 *
 * <p>Company: </p>
 *
 * @author rosia
 * @version 3.0 
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;

public class SecondaryRule_CM extends StandSecondaryRule {

	public String prepareRule() {
		// 根据预备SQL准备二次处理类中的SQL
		StringBuffer tsql = new StringBuffer();

		tsql.append("select distinct  a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType,"
						+ "a.BusinessDetail,a.feetype as feetype,a.FeeDetail,a.indexcode as indexcode,a.indexno as indexno,a.ListFlag,a.GrpContNo,"
						+ "a.GrpPolNo,a.ContNo,a.PolNo,a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,"
						+ "a.RiskPeriod,a.RiskType,a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,"
						+ "a.SupplierNo,a.SaleChnl,a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,"
						+ "a.PolYear, a.Years,a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo, a.SumActuMoney,"
						+ "a.MothDate,a.BusinessDate,a.accountdate as accountdate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,"
						+ "a.Currency,a.StringInfo01,a.StringInfo02,a.StringInfo03,a.DateInfo01,a.DateInfo02, a.RelatedNo,a.EventNo,a.State,"
						+ "a.ReadState,a.CheckFlag,a.Operator,'"
						+ PubFun.getCurrentDate()
						+ "' as MakeDate,'"
						+ PubFun.getCurrentTime()
						+ "' as maketime "
						+ "from FIAbStandardData a where a.notype = '1' and a.listflag = '1' and a.state = '00' and a.checkflag = '00' "
						+ "and a.readstate = '0' and a.indexcode in('02','03') and a.sumactumoney != 0  and a.secondaryflag = '0'"
						+ "and exists (select 1 from fiabstandarddata b where a.indexno = b.indexno and b.notype = '2' and b.listflag = '1' "
						+ "and b.state = '00' and b.checkflag = '00' and b.readstate = '0' and b.indexcode in('02','03') and b.feetype = 'CM' "
						+ "and b.sumactumoney != 0 and a.contno!=b.contno) "
						+ "and not exists (select 1 from FIAbStandardData f where a.SerialNo = f.RelatedNo and f.SecondaryFlag = '1' "
						+ "and a.indexno = f.indexno and f.secondarytype = 'CM')");
		tsql.append(" union all ");
		tsql.append("select distinct  a.SerialNo,a.NoType,a.ASerialNo,a.SecondaryFlag,a.SecondaryType,a.BusTypeID,a.CostID,a.BusinessType,"
						+ "a.BusinessDetail,a.feetype as feetype,a.FeeDetail,a.indexcode as indexcode,a.indexno as indexno,a.ListFlag,a.GrpContNo,"
						+ "a.GrpPolNo,a.ContNo,a.PolNo,a.EndorsementNo,a.NotesNo,a.FirstYearFlag,a.FirstTermFlag,a.BonusType,a.DifComFlag,a.RiskCode,"
						+ "a.RiskPeriod,a.RiskType,a.RiskType1,a.RiskType2,a.CostCenter,a.ACType,a.AgentCom,a.AgentGroup,a.AgentCode,a.CustomerID,"
						+ "a.SupplierNo,a.SaleChnl,a.SaleChnlDetail,a.ManageCom,a.ExecuteCom,a.CValiDate,a.PayIntv,a.LastPayToDate,a.CurPayToDate,"
						+ "a.PolYear, a.Years,a.PremiumType,a.PayCount,a.PayMode,a.BankCode,a.AccName,a.BankAccNo, a.SumActuMoney,"
						+ "a.MothDate,a.BusinessDate,a.accountdate as accountdate,a.CashFlowNo,a.FirstYear,a.MarketType,a.OperationType,a.Budget,"
						+ "a.Currency,a.StringInfo01,a.StringInfo02,a.StringInfo03,a.DateInfo01,a.DateInfo02, a.RelatedNo,a.EventNo,a.State,"
						+ "a.ReadState,a.CheckFlag,a.Operator,'"
						+ PubFun.getCurrentDate()
						+ "' as MakeDate,'"
						+ PubFun.getCurrentTime()
						+ "' as maketime "
						+ "from FIAbStandardData a where a.notype = '1' and a.listflag = '2' and a.state = '00' and a.checkflag = '00' "
						+ "and a.readstate = '0' and a.indexcode in('02','03') and a.sumactumoney != 0 and  a.secondaryflag = '0' "
						+ "and exists (select 1 from fiabstandarddata b where a.indexno = b.indexno and b.notype = '2' and b.listflag = '2' "
						+ "and b.state = '00' and b.checkflag = '00' and b.readstate = '0' and b.indexcode in('02','03') and b.feetype = 'CM' "
						+ "and b.sumactumoney != 0 and a.grpcontno != b.grpcontno) "
						+ "and not exists (select 1 from FIAbStandardData f where a.SerialNo = f.RelatedNo and f.SecondaryFlag = '1' "
						+ "and a.indexno = f.indexno and f.secondarytype = 'CM')");
		return tsql.toString();
	}

	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet) {
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		FIAbStandardDataSet mFIAbStandardDataSet = new FIAbStandardDataSet();
		try {
			// 处理数据
			for (int i = 1; i <= pFIAbStandardDataSet.size(); i++) {
				
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				SSRS tSSRS = new SSRS();
				String tContnosql = null;
				ExeSQL tExeSQL = new ExeSQL();
				if("1".equals(tFIAbStandardDataSchema.getListFlag())){
				tContnosql = "select distinct  contno ,sum(sumactumoney)  from  fiabstandarddata a  where notype = '2'  "
						+ "and  feetype = 'CM' and  sumactumoney!= 0 and indexno = '"
						+ tFIAbStandardDataSchema.getIndexNo()
						+ "' and  indexcode in('02','03')  and  a.contno != '"+tFIAbStandardDataSchema.getContNo() +"' group  by contno ";
				}else if("2".equals(tFIAbStandardDataSchema.getListFlag())) {	
					tContnosql=" select  distinct grpcontno ,sum(sumactumoney) from fiabstandarddata a where notype = '2' and feetype = 'CM'"
						+ "and  sumactumoney!= 0 and indexno = '"
						+ tFIAbStandardDataSchema.getIndexNo()
						+ "'  and  indexcode in('02','03') and a.grpcontno !='"+tFIAbStandardDataSchema.getGrpContNo()+"'group by grpcontno ";
				}
				tSSRS = tExeSQL.execSQL(tContnosql);
				String[] tSerialNo = FinCreateSerialNo.getSerialNoByFINDATA(tSSRS.MaxRow);
				tFIAbStandardDataSchema.setRelatedNo(tFIAbStandardDataSchema.getSerialNo());
				tFIAbStandardDataSchema.setSecondaryType("CM");
				tFIAbStandardDataSchema.setSecondaryFlag("1");
				for (int j = 1; j <= tSSRS.MaxRow; j++) {
					PubSubmit tPubSubmit = new PubSubmit();
					MMap tmap = new MMap();
					VData tVData = new VData();
					if("1".equals(tFIAbStandardDataSchema.getListFlag())){
					tFIAbStandardDataSchema.setContNo(tSSRS.GetText(j, 1));
					}else if("2".equals(tFIAbStandardDataSchema.getListFlag())){
						tFIAbStandardDataSchema.setGrpContNo(tSSRS.GetText(j, 1));
					}
					tFIAbStandardDataSchema.setSumActuMoney(tSSRS.GetText(j, 2));
					tFIAbStandardDataSchema.setSerialNo(tSerialNo[j - 1]);
					mFIAbStandardDataSet.add(tFIAbStandardDataSchema);
//					tmap.put(tFIAbStandardDataSchema, "INSERT");
//					tVData.add(tmap);
//					if (!tPubSubmit.submitData(tVData, "")) {
//						System.out.println("客户资料变更拆分保单二次处理类插入数据失败！");
//					}
				}
				
				PubSubmit mPubSubmit = new PubSubmit();
				MMap tMap = new MMap();
				VData tInputData = new VData();
				String tsql = null;
				if("1".equals(tFIAbStandardDataSchema.getListFlag())){
				 tsql = "update FIAbStandardData a set  a.sumactumoney = (select  nvl(sum(sumactumoney),0) from  fiabstandarddata b where a.indexno = b.indexno  and  "
						+ "b.notype = '2' and  b.sumactumoney!=0 and  b.state = '00' and  b.listflag = '1' and  a.contno = b.contno) where a.notype = '1' and indexno='"
						+ tFIAbStandardDataSchema.getIndexNo()
						+ "' and a.secondaryflag = '0' and a.state = '00' and  a.indexcode in('02','03')";
				 }
				else if("2".equals(tFIAbStandardDataSchema.getListFlag())) {
					tsql = "update FIAbStandardData a set  a.sumactumoney = (select  nvl(sum(sumactumoney),0) from  fiabstandarddata b where a.indexno = b.indexno  and  "
						+ "b.notype = '2' and  b.sumactumoney!=0 and  b.state = '00' and  b.listflag = '2' and  a.grpcontno = b.grpcontno) where a.notype = '1' and indexno='"
						+ tFIAbStandardDataSchema.getIndexNo()
						+ "' and a.secondaryflag = '0' and a.state = '00' and  a.indexcode in('02','03')";
				}
				tMap.put(tsql, "UPDATE");
				tInputData.add(tMap);
				if (!mPubSubmit.submitData(tInputData, "")) {
					throw new Exception("个单更新:错误！");
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return mFIAbStandardDataSet;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
