package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;
/**
 * <p>Title: 二次处理规则-保全增减人8201主表数据处理</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author rosia
 * @version 3.0 
 */

import com.sinosoft.lis.db.FIAbStandardDataDB;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.utility.VData;

public class SecondaryRule_BQZJR8201_JR extends StandSecondaryRule {
	
	public String prepareRule()
	{        
		StringBuffer tSQL = new StringBuffer();
		tSQL.append("select distinct '' as SerialNo,a.NoType as NoType ,a.ASerialNo as ASerialNo ,'1' as SecondaryFlag ,'JR' as SecondaryType ,a.BusTypeID as BusTypeID ,'F000000SB44' as CostID ," +
				"a.BusinessType as BusinessType ,a.BusinessDetail as BusinessDetail ,a.FeeType as FeeType ,a.FeeDetail as FeeDetail ,a.IndexCode as IndexCode ,a.IndexNo as IndexNo ,a.ListFlag as ListFlag ,a.GrpContNo as GrpContNo ,a.GrpPolNo as GrpPolNo ,a.ContNo as ContNo ,a.PolNo as PolNo ,a.EndorsementNo as EndorsementNo ,a.NotesNo as NotesNo ,a.FirstYearFlag as FirstYearFlag ,a.FirstTermFlag as FirstTermFlag ,a.BonusType as BonusType ,a.DifComFlag as DifComFlag ," +
				"'8201' as RiskCode ,a.RiskPeriod as RiskPeriod ,a.risktype as RiskType ,a.RiskType1 as RiskType1 ,a.RiskType2 as RiskType2 ,a.CostCenter as CostCenter ,a.ACType as ACType,a.AgentCom as AgentCom,a.AgentGroup as AgentGroup ,a.AgentCode as AgentCode ,a.CustomerID as CustomerID ,a.SupplierNo as SupplierNo ,a.SaleChnl as SaleChnl ,a.SaleChnlDetail as SaleChnlDetail ,a.ManageCom as ManageCom ,a.ExecuteCom as ExecuteCom ,a.CValiDate as CValiDate ,a.PayIntv as PayIntv ,a.LastPayToDate as LastPayToDate ,a.CurPayToDate as CurPayToDate ,a.PolYear as PolYear ,a.Years as Years ,a.PremiumType as PremiumType ,a.PayCount as PayCount ,a.PayMode as PayMode ,a.BankCode as BankCode ,a.AccName as AccName,a.BankAccNo as BankAccNo," +
				"(select -sum(getmoney) from  ljagetendorse e where  a.indexno =e.actugetno and  e.riskcode like '8201%') as SumActuMoney,a.MothDate as MothDate ,a.BusinessDate as BusinessDate ,a.AccountDate as AccountDate ,a.CashFlowNo  as CashFlowNo ,a.FirstYear as FirstYear ,a.MarketType as MarketType ,a.OperationType as OperationType ,a.Budget as Budget ,a.Currency as Currency ,a.StringInfo01 as StringInfo01  ,a.StringInfo02 as StringInfo02,a.StringInfo03  as StringInfo03 ,a.DateInfo01 as DateInfo01 ,a.DateInfo02 as DateInfo02 ,a.SerialNo as RelatedNo,a.EventNo as EventNo ,a.State as State,a.ReadState as ReadState ,a.CheckFlag as CheckFlag ,a.Operator as Operator ,'"+PubFun.getCurrentDate()+"' as MakeDate ,'"+PubFun.getCurrentTime()+"' as MakeTime  " +
				"from FIAbStandardData a where a.costid='F0000000044' and  a.secondaryflag = '0' and  notype ='1' and a.indexcode = '03' " +
				"and  exists (select 1 from  fiabstandarddata f where f.indexno = a.indexno and f.indexcode = a.indexcode and f.riskcode like '8201%' ) " +
				"and state = '00' and CheckFlag='00' and not exists(select 1 from FIAbStandardData d where a.SerialNo = d.RelatedNo and d.CostID ='F0000000044' and d.SecondaryType='JR' and a.indexno =d.indexno and a.indexcode = d.indexcode) ");
		
		return tSQL.toString();
	}
	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;		
				PubSubmit mPubSubmit = new PubSubmit();
				MMap tMap= new MMap();
				VData tInputData = new VData();
				String tsql="update FIAbStandardData a set sumactumoney = (select -sum(getmoney) from  ljagetendorse e where  a.indexno =e.actugetno and  e.riskcode  not like '8201%')  where  a.notype = '1' and serialno='"+tFIAbStandardDataSchema.getRelatedNo()+"' and a.secondaryflag = '0' and a.state = '00'";
				tMap.put(tsql, "UPDATE");
				tInputData.add(tMap);
		        if (!mPubSubmit.submitData(tInputData, ""))
		        {
		        	throw new  Exception("SecondaryRule_BQZJR8201_JR:更新错误！");
		        	
		        }
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		
	}

}

