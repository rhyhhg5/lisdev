package com.sinosoft.lis.fininterface_v3.core.rule.ruleimpl;

import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
/**
 * <p>Title: 二次处理规则--J-01</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author liusc
 * @version 3.0
 */
public class SecondaryRule_J01 extends StandSecondaryRule{

	
	public String prepareRule()
	{

		  String tipdate = PubFun.getCurrentDate();
	      System.out.println(tipdate);
	      String[] ttipdate =  tipdate.toString().split("-");
	      System.out.println(ttipdate[2]);
	      System.out.println("HHHHHHHHHHHHHHH");
	      
	        
		String ipdate="";
        String[] StartDate = mTransferData.getValueByName("StartDate").toString().split("-");
        String[] EndDate = mTransferData.getValueByName("EndDate").toString().split("-");
        
        if(StartDate[1].equals(EndDate[1]))
        {
        	if(Integer.parseInt(StartDate[2])==1)
        	{
        		ipdate=mTransferData.getValueByName("StartDate").toString();
        	}
        }else if(!StartDate[1].equals(EndDate[1]))
        {
        	
        		ipdate=EndDate[0]+"-"+EndDate[1]+"-01";
        	
        }
        
        
		StringBuffer tSQL = new StringBuffer();
	    tSQL.append(" select '' as SerialNo,");
		tSQL.append(" '0' as NoType,");
		tSQL.append(" '' as ASerialNo,");
		tSQL.append(" '1' as SecondaryFlag,");
		tSQL.append(" 'J1' as SecondaryType,");
		tSQL.append(" 'O-XQ-000006' as BusTypeID,");
		tSQL.append(" 'F0000000024' as CostID,");
		tSQL.append(" '10' as BusinessType,");
		tSQL.append(" '104' as BusinessDetail,");
		tSQL.append(" '' as FeeType,");
		tSQL.append(" '' as FeeDetail,");		
		tSQL.append(" '05' as IndexCode,");
		tSQL.append(" a.contno as IndexNo,");			
		tSQL.append(" '1' as ListFlag,");
		tSQL.append(" '' as GrpContNo,");
		tSQL.append(" '' as GrpPolNo, ");
		tSQL.append(" a.ContNo as ContNo,");
		tSQL.append(" a.polno as PolNo,");
		tSQL.append(" '' as EndorsementNo,");
		tSQL.append(" '' as NotesNo,");
		tSQL.append(" 'X' as FirstYearFlag,");
		tSQL.append(" 'X' as FirstTermFlag,");
		tSQL.append(" '' as BonusType,");
		tSQL.append(" '0' as DifComFlag,");
		tSQL.append(" a.riskcode as RiskCode,");
		tSQL.append(" 'L' as RiskPeriod,");
		tSQL.append(" '0' as RiskType,");
		tSQL.append(" '' as RiskType1,");
		tSQL.append(" '' as RiskType2,");
		tSQL.append(" (select costcenter from labranchgroup where agentgroup = a.agentgroup fetch first 1 rows only) as CostCenter,");//成本中心
		tSQL.append(" (select actype from lacom where agentcom=a.agentcom fetch first 1 rows only) as ACType,"); //中介机构类别
		tSQL.append(" a.AgentCom as AgentCom,");
		tSQL.append(" a.AgentGroup as AgentGroup,");
		tSQL.append(" a.AgentCode as AgentCode,");
		tSQL.append(" '99999999' as CustomerID,");
		tSQL.append(" '' as SupplierNo,");//供应商号码
		tSQL.append(" a.SaleChnl as SaleChnl,");
		tSQL.append(" '' as SaleChnlDetail,");
		tSQL.append(" a.ManageCom as ManageCom,");
		tSQL.append(" '' as ExecuteCom,");
		tSQL.append(" a.CValiDate as CValiDate,");
		tSQL.append(" a.PayIntv as PayIntv,");
		tSQL.append(" cast(null as date) as LastPayToDate,");
		tSQL.append(" cast(null as date) as CurPayToDate,");
		tSQL.append(" LF_PolYear(a.PayIntv, a.PayYears) as PolYear,");
		tSQL.append(" LF_Years(a.InsuYearFlag, a.InsuYear, a.Years) as Years,");
		tSQL.append(" '99' as PremiumType,");
		tSQL.append(" 0 as PayCount,");
		tSQL.append(" '' as PayMode,");
		tSQL.append(" '' as BankCode,");
		tSQL.append(" '' as AccName,");
		tSQL.append(" '' as BankAccNo,");
		tSQL.append(" a.prem as SumActuMoney,");
		tSQL.append(" cast(null as date) as MothDate,");
		tSQL.append(" cast(null as date) as BusinessDate,");
		tSQL.append(" (date('"+ipdate+"') - 1 days) as AccountDate,");
		tSQL.append(" '' as CashFlowNo,");
		tSQL.append(" (select substr(char(confdate), 1, 4) from ljapay where incomeno = a.contno and duefeetype = '0' fetch first 1 rows only) as FirstYear,");//
		tSQL.append(" '1' as MarketType,");
		tSQL.append(" '' as OperationType,");
		tSQL.append(" '' as Budget,");
		tSQL.append(" 'CNY' as Currency ,");
		tSQL.append(" a.AppntNo as StringInfo01,"); //业务明细
		tSQL.append(" '' as StringInfo02,");
		tSQL.append(" '' as StringInfo03,");
		tSQL.append(" cast(null as date) as DateInfo01,");
		tSQL.append(" cast(null as date) as DateInfo02,");
		tSQL.append(" '' as RelatedNo,");
		tSQL.append(" '' as EventNo,");
		tSQL.append(" '00' as State,"); //待凭证转换
		tSQL.append(" '0' as ReadState,"); //未同步
		tSQL.append(" '00' as CheckFlag,");
		tSQL.append(" '"+mGlobalInput.Operator+"' as Operator,");
		tSQL.append(" '"+PubFun.getCurrentDate()+"' as MakeDate,");
		tSQL.append(" '"+PubFun.getCurrentTime()+"' as MakeTime ");
		tSQL.append(" from lcpol a where month(date('"+ipdate+"') - 1 days) <> month(date('"+ipdate+"'))");
		tSQL.append(" and a.PayToDate < (date('"+ipdate+"') - 1 days)  and a.payenddate >(date('"+ipdate+"') - 1 days) " +
				" and a.ContType = '1' and a.Payintv <> 0 and a.StateFlag = '1' and a.prem <> 0 " +
				"and exists (select 1 from lmriskapp where riskcode = a.riskcode and riskperiod='L' and kindcode <> 'U') " +
				"and not exists(select 1 from FIAbStandardData c where c.contno = a.contno and c.SecondaryType='J1' and c.accountdate=(date('"+ipdate+"') - 1 days))");
		tSQL.append(" union all ");
		tSQL.append(" select '' as SerialNo,");
		tSQL.append(" '0' as NoType,");
		tSQL.append(" '' as ASerialNo,");
		tSQL.append(" '1' as SecondaryFlag,");
		tSQL.append(" 'J2' as SecondaryType,");
		tSQL.append(" 'O-XQ-000006' as BusTypeID,");
		tSQL.append(" 'F0000000024' as CostID,");
		tSQL.append(" '10' as BusinessType,");
		tSQL.append(" '104' as BusinessDetail,");
		tSQL.append(" '' as FeeType,");
		tSQL.append(" '' as FeeDetail,");		
		tSQL.append(" '05' as IndexCode,");
		tSQL.append(" a.contno as IndexNo,");			
		tSQL.append(" '1' as ListFlag,");
		tSQL.append(" '' as GrpContNo,");
		tSQL.append(" '' as GrpPolNo, ");
		tSQL.append(" a.ContNo as ContNo,");
		tSQL.append(" a.polno as PolNo,");
		tSQL.append(" '' as EndorsementNo,");
		tSQL.append(" '' as NotesNo,");
		tSQL.append(" 'X' as FirstYearFlag,");
		tSQL.append(" 'X' as FirstTermFlag,");
		tSQL.append(" '' as BonusType,");
		tSQL.append(" '0' as DifComFlag,");
		tSQL.append(" a.riskcode as RiskCode,");
		tSQL.append(" (select riskperiod from lmriskapp where riskcode = a.riskcode fetch first 1 rows only) as RiskPeriod,");
		tSQL.append(" '0' as RiskType,");
		tSQL.append(" '' as RiskType1,");
		tSQL.append(" '' as RiskType2,");
		tSQL.append(" (select costcenter from labranchgroup where agentgroup = a.agentgroup fetch first 1 rows only) as CostCenter,");//成本中心
		tSQL.append(" (select actype from lacom where agentcom=a.agentcom fetch first 1 rows only) as ACType,"); //中介机构类别
		tSQL.append(" a.AgentCom as AgentCom,");
		tSQL.append(" a.AgentGroup as AgentGroup,");
		tSQL.append(" a.AgentCode as AgentCode,");
		tSQL.append(" '99999999' as CustomerID,");
		tSQL.append(" '' as SupplierNo,");//供应商号码
		tSQL.append(" a.SaleChnl as SaleChnl,");
		tSQL.append(" '' as SaleChnlDetail,");
		tSQL.append(" a.ManageCom as ManageCom,");
		tSQL.append(" '' as ExecuteCom,");
		tSQL.append(" a.CValiDate as CValiDate,");
		tSQL.append(" a.PayIntv as PayIntv,");
		tSQL.append(" cast(null as date) as LastPayToDate,");
		tSQL.append(" cast(null as date) as CurPayToDate,");
		tSQL.append(" LF_PolYear(a.PayIntv, a.PayYears) as PolYear,");
		tSQL.append(" LF_Years(a.InsuYearFlag, a.InsuYear, a.Years) as Years,");
		tSQL.append(" '99' as PremiumType,");
		tSQL.append(" 0 as PayCount,");
		tSQL.append(" '' as PayMode,");
		tSQL.append(" '' as BankCode,");
		tSQL.append(" '' as AccName,");
		tSQL.append(" '' as BankAccNo,");
		tSQL.append(" a.prem as SumActuMoney,");
		tSQL.append(" cast(null as date) as MothDate,");
		tSQL.append(" cast(null as date) as BusinessDate,");
		tSQL.append(" (date('"+ipdate+"')- 1 days) as AccountDate,");
		tSQL.append(" '' as CashFlowNo,");
		tSQL.append(" (select substr(char(confdate), 1, 4) from ljapay where incomeno = a.contno and duefeetype = '0' fetch first 1 rows only) as FirstYear,");//
		tSQL.append(" '1' as MarketType,");
		tSQL.append(" '' as OperationType,");
		tSQL.append(" '' as Budget,");
		tSQL.append(" 'CNY' as Currency ,");
		tSQL.append(" a.AppntNo as StringInfo01,"); //业务明细
		tSQL.append(" '' as StringInfo02,");
		tSQL.append(" '' as StringInfo03,");
		tSQL.append(" cast(null as date) as DateInfo01,");
		tSQL.append(" cast(null as date) as DateInfo02,");
		tSQL.append(" '' as RelatedNo,");
		tSQL.append(" '' as EventNo,");
		tSQL.append(" '00' as State,"); //待凭证转换
		tSQL.append(" '0' as ReadState,"); //未同步
		tSQL.append(" '00' as CheckFlag,");
		tSQL.append(" '"+mGlobalInput.Operator+"' as Operator,");
		tSQL.append(" '"+PubFun.getCurrentDate()+"' as MakeDate,");
		tSQL.append(" '"+PubFun.getCurrentTime()+"' as MakeTime ");
		tSQL.append(" from lcpol a where month(date('"+ipdate+"') - 1 days) <> month(date('"+ipdate+"'))");
		tSQL.append("  and a.PayToDate < (date('"+ipdate+"') - 1 days) and a.payenddate >(date('"+ipdate+"') - 1 days) " +
				" and a.ContType = '1' and a.Payintv <> 0 and a.StateFlag = '1' and a.prem <> 0 " +
				"and exists (select 1 from lmriskapp where riskcode = a.riskcode and riskperiod in ('S','M')) " +
				"and exists (select 1 from LJSPayPersonB  where PolNo = a.PolNo and dealstate is not null) " +
				"and not exists(select 1 from FIAbStandardData c where c.contno = a.contno and c.SecondaryType='J2' and c.accountdate=(date('"+ipdate+"') - 1 days))");
		tSQL.append(" union all ");
		tSQL.append(" select '' as SerialNo,");
		tSQL.append(" '0' as NoType,");
		tSQL.append(" '' as ASerialNo,");
		tSQL.append(" '1' as SecondaryFlag,");
		tSQL.append(" 'J3' as SecondaryType,");
		tSQL.append(" 'O-XQ-000006' as BusTypeID,");
		tSQL.append(" 'F0000000024' as CostID,");
		tSQL.append(" '10' as BusinessType,");
		tSQL.append(" '104' as BusinessDetail,");
		tSQL.append(" '' as FeeType,");
		tSQL.append(" '' as FeeDetail,");		
		tSQL.append(" '05' as IndexCode,");
		tSQL.append(" a.grpcontno as IndexNo,");			
		tSQL.append(" '2' as ListFlag,");
		tSQL.append(" a.grpcontno as GrpContNo,");
		tSQL.append(" a.grppolno as GrpPolNo, ");
		tSQL.append(" '' as ContNo,");
		tSQL.append(" '' as PolNo,");
		tSQL.append(" '' as EndorsementNo,");
		tSQL.append(" '' as NotesNo,");
		tSQL.append(" 'X' as FirstYearFlag,");
		tSQL.append(" 'X' as FirstTermFlag,");
		tSQL.append(" '' as BonusType,");
		tSQL.append(" '0' as DifComFlag,");
		tSQL.append(" a.riskcode as RiskCode,");
		tSQL.append(" (select riskperiod from lmriskapp where riskcode = a.riskcode fetch first 1 rows only) as RiskPeriod,");
		tSQL.append(" '0' as RiskType,");
		tSQL.append(" '' as RiskType1,");
		tSQL.append(" '' as RiskType2,");
		tSQL.append(" (select costcenter from labranchgroup where agentgroup = a.agentgroup fetch first 1 rows only) as CostCenter,");//成本中心
		tSQL.append(" (select actype from lacom where agentcom=a.agentcom fetch first 1 rows only) as ACType,"); //中介机构类别
		tSQL.append(" a.AgentCom as AgentCom,");
		tSQL.append(" a.AgentGroup as AgentGroup,");
		tSQL.append(" a.AgentCode as AgentCode,");
		tSQL.append(" LF_BClient('2', a.GrpContNo) as CustomerID,");
		tSQL.append(" '' as SupplierNo,");//供应商号码
		tSQL.append(" a.SaleChnl as SaleChnl,");
		tSQL.append(" '' as SaleChnlDetail,");
		tSQL.append(" a.ManageCom as ManageCom,");
		tSQL.append(" '' as ExecuteCom,");
		tSQL.append(" a.CValiDate as CValiDate,");
		tSQL.append(" a.PayIntv as PayIntv,");
		tSQL.append(" cast(null as date) as LastPayToDate,");
		tSQL.append(" cast(null as date) as CurPayToDate,");
		tSQL.append(" LF_GrpPolYear(a.GrpPolNo) as PolYear,");
		tSQL.append(" LF_GrpYears(a.GrpPolNo) as Years,");
		tSQL.append(" '99' as PremiumType,");
		tSQL.append(" 0 as PayCount,");
		tSQL.append(" '' as PayMode,");
		tSQL.append(" '' as BankCode,");
		tSQL.append(" '' as AccName,");
		tSQL.append(" '' as BankAccNo,");
		tSQL.append(" a.prem as SumActuMoney,");
		tSQL.append(" cast(null as date) as MothDate,");
		tSQL.append(" cast(null as date) as BusinessDate,");
		tSQL.append(" (date('"+ipdate+"')- 1 days) as AccountDate,");
		tSQL.append(" '' as CashFlowNo,");
		tSQL.append(" (select substr(char(confdate), 1, 4) from ljapay where incomeno = a.grpcontno and duefeetype = '0' fetch first 1 rows only) as FirstYear,");//
		tSQL.append(" LF_MarketType(a.GrpContNo) as MarketType,");
		tSQL.append(" '' as OperationType,");
		tSQL.append(" '' as Budget,");
		tSQL.append(" 'CNY' as Currency ,");
		tSQL.append(" a.CustomerNo as StringInfo01,"); //业务明细
		tSQL.append(" '' as StringInfo02,");
		tSQL.append(" '' as StringInfo03,");
		tSQL.append(" cast(null as date) as DateInfo01,");
		tSQL.append(" cast(null as date) as DateInfo02,");
		tSQL.append(" '' as RelatedNo,");
		tSQL.append(" '' as EventNo,");
		tSQL.append(" '00' as State,"); //待凭证转换
		tSQL.append(" '0' as ReadState,"); //未同步
		tSQL.append(" '00' as CheckFlag,");
		tSQL.append(" '"+mGlobalInput.Operator+"' as Operator,");
		tSQL.append(" '"+PubFun.getCurrentDate()+"' as MakeDate,");
		tSQL.append(" '"+PubFun.getCurrentTime()+"' as MakeTime ");
		tSQL.append(" from lcgrppol a where month(date('"+ipdate+"') - 1 days) <> month(date('"+ipdate+"'))");
		tSQL.append(" and a.PayToDate < (date('"+ipdate+"') - 1 days)  and a.payenddate >(date('"+ipdate+"') - 1 days) " +
				" and a.Payintv <> 0 and a.StateFlag = '1' and a.prem <> 0 " +
				"and exists (select 1 from lmriskapp where riskcode = a.riskcode and riskperiod ='L' and kindcode<>'U') " +
				"and not exists(select 1 from FIAbStandardData c where c.grpcontno = a.grpcontno and c.SecondaryType='J3' and c.accountdate=(date('"+ipdate+"') - 1 days))");
		
		if("".equals(ipdate))
			return "select * from FIAbStandardData where 1>2 ";
		
		if (!ttipdate[2].equals("01")) return "select * from FIAbStandardData where 1>3 ";

		else
			return tSQL.toString();
	}

	
	public FIAbStandardDataSet prepareFIAbStandardData(FIAbStandardDataSet tFIAbStandardDataSet)
	{
		FIAbStandardDataSet pFIAbStandardDataSet = tFIAbStandardDataSet;
		
		try 
		{
			String[] tSerialNo= FinCreateSerialNo.getSerialNoByFINDATA(pFIAbStandardDataSet.size());
			
			for(int i=1;i<=pFIAbStandardDataSet.size();i++)			
			{
				//应收计提
				FIAbStandardDataSchema tFIAbStandardDataSchema = pFIAbStandardDataSet.get(i);
				tFIAbStandardDataSchema.setRelatedNo(tSerialNo[i-1]);
				tFIAbStandardDataSchema.setSerialNo(tSerialNo[i-1]) ;//		
			}	
		} 
		catch (Exception e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return pFIAbStandardDataSet;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated meecthod stub
		SecondaryRule_J01 tSecondaryRule_J01 = new SecondaryRule_J01();
		
		//System.out.println(tSecondaryRule_J01.prepareRule());
		
	}

}
