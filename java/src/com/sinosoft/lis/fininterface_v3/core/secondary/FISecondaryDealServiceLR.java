package com.sinosoft.lis.fininterface_v3.core.secondary;

import com.sinosoft.lis.db.FIDataExtractDefDB;
import com.sinosoft.lis.db.FIDataExtractRulesDB;
import com.sinosoft.lis.fininterface_v3.core.rule.Rule;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIDataExtractRulesSchema;
import com.sinosoft.lis.vschema.FIDataExtractDefSet;
import com.sinosoft.lis.vschema.FIDataExtractRulesSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
/**
 * <p>Title: 二次处理服务类</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class FISecondaryDealServiceLR {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    public GlobalInput mGlobalInput = new GlobalInput();
    
    private String EventNo = "";
    private final String enter = "\r\n"; // 换行符
    private FIOperationLog tLogDeal = null;
    
    public FISecondaryDealServiceLR(FIOperationLog logDeal)
    {
        tLogDeal = logDeal;
        EventNo = tLogDeal.getEventNo();
    }	
	
	public boolean dealService(VData param)
	{
		param.add(tLogDeal);
		
		String tSQL = "select * from FIDataExtractDef where RuleState = '1' and RuleType = '02' and ruledefid = '0000000207' with ur ";
		
		FIDataExtractDefSet tFIDataExtractDefSet = new FIDataExtractDefDB().executeQuery(tSQL);
		
		if(tFIDataExtractDefSet!=null &&tFIDataExtractDefSet.size()>0)
		{
			String tRuleDefID;
			for(int i=1;i<=tFIDataExtractDefSet.size();i++)
			{
				
				tRuleDefID = tFIDataExtractDefSet.get(i).getRuleDefID();				
				FIDataExtractRulesSet tFIDataExtractRulesSet = getRules(tRuleDefID);
				
				for(int j=1;j<=tFIDataExtractRulesSet.size();j++)
				{
					FIDataExtractRulesSchema tFIDataExtractRulesSchema = tFIDataExtractRulesSet.get(j);
					
					if("1".equals(tFIDataExtractRulesSchema.getDealMode()))
					{				
						//类处理
						try 
						{
							Class tClass = Class.forName(tFIDataExtractRulesSchema.getDealClass());
			            	Rule rule = (Rule) tClass.newInstance();
			            	tLogDeal.WriteLogTxt("执行二次处理，算法编码为："+tFIDataExtractRulesSchema.getArithmeticID()+": "+tFIDataExtractRulesSchema.getDealClass().substring(52)+ enter);
			                if(!rule.ruleExe(param,tFIDataExtractRulesSchema))
			                {
			                    buildError("FISecondaryDealService","dealService", "二次处理出现错误，明细算法编码为" + tFIDataExtractRulesSchema.getArithmeticID() +"，信息为：" + rule.mErrors.getFirstError());
			                    tLogDeal.WriteLogTxt("二次处理出现错误，明细算法编码为" + tFIDataExtractRulesSchema.getArithmeticID() +"，信息为：" + rule.mErrors.getFirstError() + enter);
			                    continue;
			                }
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
		                    buildError("FISecondaryDealService","dealService", "二次处理出现异常，明细算法编码为" + tFIDataExtractRulesSchema.getArithmeticID() +"，信息为：" + e.getMessage());
		                    tLogDeal.WriteLogTxt("二次处理出现异常，明细算法编码为" + tFIDataExtractRulesSchema.getArithmeticID() +"，信息为：" + e.getMessage() + enter);

						}
					}
					else if("2".equals(tFIDataExtractRulesSchema.getDealMode()))
					{
						//SQL处理
						MMap tMap = new MMap();
						VData tInputData = new VData();
						PubSubmit tPubSubmit = new PubSubmit();
						String update = tFIDataExtractRulesSchema.getMainSQL();
						
						tMap.put(update, "UPDATE");	

						tInputData.add(tMap);
						
				        if (!tPubSubmit.submitData(tInputData, ""))
				        {
				            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				            buildError("DataTransRuleEngine","ruleEXE", "数据归档规则执行出错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
				            tLogDeal.WriteLogTxt("数据归档规则执行出错，提示信息为：" +tPubSubmit.mErrors.getFirstError() + enter);
				            return false;
				        }  
					}		
				}
			}			
		}		
		return true;
	}
	
	private FIDataExtractRulesSet getRules(String RuleDefID)
	{
		
		String tSQL = "select * from FIDataExtractRules where state = '1' and ArithmeticType = '02' and RuleDefID = '"+RuleDefID+"' order by dealorder,arithmeticid with ur ";
		
		FIDataExtractRulesDB tFIDataExtractRulesDB = new FIDataExtractRulesDB();
		FIDataExtractRulesSet tFIDataExtractRulesSet  = tFIDataExtractRulesDB.executeQuery(tSQL);
		
		return tFIDataExtractRulesSet;
	}
	
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
