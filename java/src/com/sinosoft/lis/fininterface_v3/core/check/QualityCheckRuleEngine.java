package com.sinosoft.lis.fininterface_v3.core.check;

/**
 * <p>Title: 数据质量校检</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
import com.sinosoft.lis.db.FIVerifyRuleDefDB;
import com.sinosoft.lis.db.FIVerifyRulesDB;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIRuleDealErrLogSchema;
import com.sinosoft.lis.schema.FIRuleDealLogSchema;
import com.sinosoft.lis.schema.FIVerifyRuleDefSchema;
import com.sinosoft.lis.schema.FIVerifyRulesSchema;
import com.sinosoft.lis.vschema.FIRuleDealErrLogSet;
import com.sinosoft.lis.vschema.FIVerifyRuleDefSet;
import com.sinosoft.lis.vschema.FIVerifyRulesSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class QualityCheckRuleEngine {
	
	public CErrors mErrors = new CErrors();	
	
	GlobalInput mGlobalInput = new GlobalInput();
	
	private FIVerifyRuleDefSet mFIVerifyRuleDefSet ;
	/** 日志处理*/
	private FIOperationLog tLogDeal = null;
	
	private String mCallPointID;
	
	private TransferData mTransferData ;
	
	private FIRuleDealLogSchema mFIRuleDealLogSchema;
	
	private ExeSQL mExeSQL = new ExeSQL();
	
	private String EventNo = "";
	
	private MMap mMap= new MMap();
	
    private final String enter = "\r\n"; // 换行符
    
    public QualityCheckRuleEngine(FIOperationLog logDeal)
    {
    	tLogDeal = logDeal;
    	EventNo = tLogDeal.getEventNo();
    }

    /**
     * 校检规则执行
     * @param tFIVerifyRulesSet
     * @return
     */
	public boolean ruleService(String tCallPointID,VData param) 
	{
		// TODO Auto-generated method stub
		
		mCallPointID = tCallPointID;

    	if (!getInputData(param))
        {
			buildError("QualityCheckRuleEngine","ruleService", "获取参数出错！！");
            tLogDeal.WriteLogTxt("获取参数出错！！" + enter);
    		return false;
        }

    	if (!getCheckData())
        {
			buildError("QualityCheckRuleEngine","ruleService", "数据校检出错！！");
            tLogDeal.WriteLogTxt("数据校检出错！！" + enter);
    		return false;
        }
    	
    	//获取质检计划
    	mFIVerifyRuleDefSet = getQualityRuleDef(mCallPointID);
    	
    	if(mFIVerifyRuleDefSet!=null&&mFIVerifyRuleDefSet.size()>0)
    	{
        	for(int i=1;i<=mFIVerifyRuleDefSet.size();i++)
        	{
        		//执行规则
                dealRuels(mFIVerifyRuleDefSet.get(i));
        	}
    	}

		return true;
	}

	/**
     * 获取参数
     * @param cInputData
     * @return
     */
    private boolean getInputData(VData cInputData)
    {
    	mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
    	mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
    	
    	if(mGlobalInput==null)
    	{
			buildError("QualityCheckRuleEngine","getInputData", "mGlobalInput获取参数出错！！");
            tLogDeal.WriteLogTxt("获取参数出错！！" + enter);
    		return false;
    	}
    	System.out.println("校检执行节点："+mCallPointID);
    	return true;
    }
    
    /**
     * 数据校检
     * @return
     */
    private boolean getCheckData()
    {
    	
    	return true;
    }
    
    /**
     * 批量执行规则
     */
    private void dealRuels(FIVerifyRuleDefSchema tFIVerifyRuleDefSchema)
    {
    	FIVerifyRulesSet tFIVerifyRulesSet = getQualityRules(tFIVerifyRuleDefSchema.getRuleDefID());
    	
    	if(tFIVerifyRulesSet!=null&&tFIVerifyRulesSet.size()>0)
    	{
    		mMap = new MMap();
    		
    		prepareDealLog(tFIVerifyRuleDefSchema);
    		
    		for(int i=1;i <=tFIVerifyRulesSet.size();i++)
    		{
    						
    			FIVerifyRulesSchema tFIVerifyRulesSchema = tFIVerifyRulesSet.get(i);
    			
    			if(!runRuel(tFIVerifyRulesSchema))
    			{
    				buildError("QualityCheckRuleEngine","dealRuels", "规则类型错误，规则编码："+tFIVerifyRulesSchema.getArithmeticID());
                    tLogDeal.WriteLogTxt("规则类型错误，规则编码："+tFIVerifyRulesSchema.getArithmeticID() + enter);	
                    mFIRuleDealLogSchema.setRuleResult("Fail");
    			}
    		}
			
			//更新校检后数据校检状态
			changeCheckFlag();
			
			// 准备往后台的数据
			if (!prepareOutputData())
			{
				tLogDeal.WriteLogTxt("准备往后台的数据失败！"+ enter);	
			}
    	}
    	else
    	{
    		System.out.println("该节点没有校检计划!!!");
    	}
		
    }
    /**
     * 批次日志初始化
     * @param tFIVerifyRulesSchema
     */
    private void prepareDealLog(FIVerifyRuleDefSchema tFIVerifyRuleDefSchema)
    {
    	mFIRuleDealLogSchema = new FIRuleDealLogSchema();
    	mFIRuleDealLogSchema.setVersionNo(tFIVerifyRuleDefSchema.getVersionNo());
    	mFIRuleDealLogSchema.setCheckBatchNo(FinCreateSerialNo.getLogSerialNo());
    	mFIRuleDealLogSchema.setEventNo(EventNo);
    	mFIRuleDealLogSchema.setCheckType("00");
    	mFIRuleDealLogSchema.setRuleID(tFIVerifyRuleDefSchema.getRuleDefID());
    	mFIRuleDealLogSchema.setCallPointID(mCallPointID);
    	mFIRuleDealLogSchema.setOperator(mGlobalInput.Operator);
    	mFIRuleDealLogSchema.setRulePlanID(tFIVerifyRuleDefSchema.getRuleDefID());
    	mFIRuleDealLogSchema.setMakeDate(PubFun.getCurrentDate());
    	mFIRuleDealLogSchema.setMakeTime(PubFun.getCurrentTime());
    	mFIRuleDealLogSchema.setLogFilePath("");
    	mFIRuleDealLogSchema.setLogFileName("");
    	mFIRuleDealLogSchema.setDataSource("1");
    	
    	mFIRuleDealLogSchema.setRuleResult("Succ");
    	
    	mMap.put(mFIRuleDealLogSchema, "INSERT");
    }
    
    /**
     * 执行校检规则
     * @param tFIVerifyRulesSchema
     * @return
     */
    private boolean runRuel(FIVerifyRulesSchema tFIVerifyRulesSchema)
    {
    	//SQL规则
    	if("2".equals(tFIVerifyRulesSchema.getDealMode()))
    	{
    		SSRS tSSRS = mExeSQL.execSQL(tFIVerifyRulesSchema.getFinDealSQL());
    		
    		FIRuleDealErrLogSet tFIRuleDealErrLogSet = new FIRuleDealErrLogSet();
    		
    		for (int i=1 ;i<=tSSRS.MaxRow ;i++)
    		{
    			FIRuleDealErrLogSchema tFIRuleDealErrLogSchema = new FIRuleDealErrLogSchema();
    			tFIRuleDealErrLogSchema.setErrSerialNo(FinCreateSerialNo.getErrLogSerialNo());
    			tFIRuleDealErrLogSchema.setAserialno(tSSRS.GetText(i, 1));
    			tFIRuleDealErrLogSchema.setIndexCode(tSSRS.GetText(i, 2));//业务号码类型
    			tFIRuleDealErrLogSchema.setBusinessNo(tSSRS.GetText(i, 3));//业务号码
    			tFIRuleDealErrLogSchema.setCheckBatchNo(mFIRuleDealLogSchema.getCheckBatchNo());
    			tFIRuleDealErrLogSchema.setRuleID(tFIVerifyRulesSchema.getArithmeticID());
    			tFIRuleDealErrLogSchema.setErrInfo(tFIVerifyRulesSchema.getReturnRemark());//错误信息
    			tFIRuleDealErrLogSchema.setRulePlanID(tFIVerifyRulesSchema.getRuleDefID());
    			tFIRuleDealErrLogSchema.setDealState("0");//待处理
    			tFIRuleDealErrLogSchema.setCertificateID("");
    			tFIRuleDealErrLogSchema.setCallPointID(mCallPointID);

    			tFIRuleDealErrLogSet.add(tFIRuleDealErrLogSchema);
    		}
    		mMap.put(tFIRuleDealErrLogSet, "INSERT");
    	}	
    	return true;
    }
    
    private boolean changeCheckFlag()
    {
		
    	if("00".equals(mCallPointID))
		{
    		//数据提取校检后更新数据校检状态
    		String update2 = "update FIAboriginalMain a set a.checkflag = '01' where a.checkflag='00' and a.state ='02' " +
			"and exists(select 1 from FIRuleDealErrLog b where a.IndexCode = b.IndexCode and a.IndexNo=b.BusinessNo and b.DealState = '0' and b.CheckBatchNo='"+mFIRuleDealLogSchema.getCheckBatchNo()+"') ";
    		
    		String update1 = "update FIAboriginalGenDetail a set a.checkflag = '01' where a.checkflag='00' and a.state ='00' " +
    		"and exists(select 1 from FIRuleDealErrLog b where a.IndexCode = b.IndexCode and a.IndexNo=b.BusinessNo and b.DealState = '0' and b.CheckBatchNo='"+mFIRuleDealLogSchema.getCheckBatchNo()+"') ";


    		mMap.put(update1, "UPDATE");
    		mMap.put(update2, "UPDATE");
		}
    	if("10".equals(mCallPointID))
		{
    		//数据提取校检后更新数据校检状态
    		String update2 = "update FIAboriginalMain a set a.checkflag = '01' where a.checkflag='00' and a.state ='10' " +
			"and exists(select 1 from FIRuleDealErrLog b where a.IndexCode = b.IndexCode and a.IndexNo=b.BusinessNo and b.DealState = '0' and b.CheckBatchNo='"+mFIRuleDealLogSchema.getCheckBatchNo()+"') ";
    		
    		String update1 = "update FIAboriginalGenDetail a set a.checkflag = '01' where a.checkflag='00' and a.state ='10' " +
    		"and exists(select 1 from FIRuleDealErrLog b where a.IndexCode = b.IndexCode and a.IndexNo=b.BusinessNo and b.DealState = '0' and b.CheckBatchNo='"+mFIRuleDealLogSchema.getCheckBatchNo()+"') ";

    		mMap.put(update1, "UPDATE");
    		mMap.put(update2, "UPDATE");
		}
    	else if("20".equals(mCallPointID))
    	{
    		//二次处理后更新数据校检状态
    	}
    	else if("30".equals(mCallPointID))
    	{
    		//凭证转换后更新数据校检状态
    		String update2 = "update FIVoucherDataDetail a set a.checkflag = '01' where a.checkflag='00' and a.readstate ='0' " +
			"and exists(select 1 from FIRuleDealErrLog b where a.classtype = b.IndexCode and a.managecom=b.BusinessNo and a.batchno = b.aserialno and b.DealState = '0' and b.CheckBatchNo='"+mFIRuleDealLogSchema.getCheckBatchNo()+"') ";
    		
    		mMap.put(update2, "UPDATE");
    	}
    	
    	return true;
    }
    /**
     * 提交保存校检日志
     * @return
     */
    private boolean prepareOutputData()
    {

		VData tInputData = new VData();
		PubSubmit tPubSubmit = new PubSubmit();
		tInputData.add(mMap);
		
        if (!tPubSubmit.submitData(tInputData, ""))
        {
    		mMap = null;
    		tInputData = null;
    		
        	this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buildError("QualityCheckRuleEngine","saveData", "校检数据保存时出错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("校检数据保存时出错，提示信息为：，提示信息为：" +tPubSubmit.mErrors.getFirstError() + enter);           
    		
            return false;
        }
		
		mMap = null;
		tInputData = null;
    	return true;
    }
    
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    
    /**
     * 质检计划
     * @param tCallPointID,执行节点
     * @return
     */
    private FIVerifyRuleDefSet getQualityRuleDef(String tCallPointID)
    {
		String tSQL = "select * from FIVerifyRuleDef where RuleState='1' and RuleType = '01' and CallPointID = '"+tCallPointID+"' with ur " ;
		FIVerifyRuleDefDB tFIVerifyRuleDefDB = new FIVerifyRuleDefDB();
		FIVerifyRuleDefSet tFIVerifyRuleDefSet  = tFIVerifyRuleDefDB.executeQuery(tSQL);
		
		return tFIVerifyRuleDefSet;
    }

    /**
     * 质检计划规则
     * @param tCallPointID,执行节点
     * @return
     */
    private FIVerifyRulesSet getQualityRules(String tRuleDefID)
    {
		String tSQL = "select * from FIVerifyRules a where state = '1' and a.RuleDefID = '"+tRuleDefID+"' with ur " ;
		FIVerifyRulesDB tFIVerifyRulesDB = new FIVerifyRulesDB();
		FIVerifyRulesSet tFIVerifyRulesSet  = tFIVerifyRulesDB.executeQuery(tSQL);
		
		return tFIVerifyRulesSet;
    }
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
}
