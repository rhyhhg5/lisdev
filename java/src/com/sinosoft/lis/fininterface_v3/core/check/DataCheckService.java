package com.sinosoft.lis.fininterface_v3.core.check;

import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 校检规则服务</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class DataCheckService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    GlobalInput mGlobalInput = new GlobalInput();
	
    private FIOperationLog tLogDeal = null;
    
    private QualityCheckRuleEngine mQualityCheckRuleEngine ;
    
    private DifCheckRuleEngine mDifCheckRuleEngine ;
    
    public DataCheckService(FIOperationLog logDeal)
    {
        tLogDeal = logDeal;
    }
    
    /**
     * 数据质量校检
     * @param tCallPointID，执行节点
     * @return
     */
    public boolean qualityDataCheck(String tCallPointID,VData param)
    {
        mQualityCheckRuleEngine = new QualityCheckRuleEngine(tLogDeal);
        
    	mQualityCheckRuleEngine.ruleService(tCallPointID,param);
    	
    	return true;
    }
    
    
    /**
     * 业财差异校检
     * @param cOperate  00 自动校检；01 手动校检
     * @param param
     * @return
     */
    public boolean difDataCheck(String cOperate,VData param)
    {
    	mDifCheckRuleEngine = new DifCheckRuleEngine(tLogDeal);
    	
    	if(!mDifCheckRuleEngine.ruleExe(cOperate, param))
		{
    		buildError("DataCheckService","difDataCheck", "差异校检规则执行出错！");
    		return false;
		}
    	
    	return true;
    }
    
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
