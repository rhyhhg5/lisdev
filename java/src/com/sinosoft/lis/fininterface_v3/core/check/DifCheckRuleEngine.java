package com.sinosoft.lis.fininterface_v3.core.check;

/**
 * <p>Title: 数据差异校检</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
import com.sinosoft.lis.db.FIDataBaseLinkDB;
import com.sinosoft.lis.db.FIRuleDealErrLogDB;
import com.sinosoft.lis.db.FIVerifyRuleDefDB;
import com.sinosoft.lis.db.FIVerifyRulesDB;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.fininterface_v3.tools.dblink.DBConn;
import com.sinosoft.lis.fininterface_v3.tools.dblink.DBConnPool;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIRuleDealErrLogSchema;
import com.sinosoft.lis.schema.FIRuleDealLogSchema;
import com.sinosoft.lis.schema.FIVerifyRuleDefSchema;
import com.sinosoft.lis.schema.FIVerifyRulesSchema;
import com.sinosoft.lis.vschema.FIDataBaseLinkSet;
import com.sinosoft.lis.vschema.FIRuleDealErrLogSet;
import com.sinosoft.lis.vschema.FIVerifyRuleDefSet;
import com.sinosoft.lis.vschema.FIVerifyRulesSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class DifCheckRuleEngine {
	
	public CErrors mErrors = new CErrors();	
	
	public GlobalInput mGlobalInput = new GlobalInput();
	
	private FIVerifyRuleDefSet mFIVerifyRuleDefSet ;
	/** 日志处理*/
	private FIOperationLog tLogDeal = null;
	private PubCalculator mPubCalculator = new PubCalculator();
	private String mRuleDefID;
	private String mCallPointID;
	private String mStartDate ;
	private String mEndDate ;
	private TransferData mTransferData ;

	private String[] mParams ;
	private String[] mValues ;
	
	private FIRuleDealLogSchema mFIRuleDealLogSchema;
	private FIRuleDealErrLogSet mFIRuleDealErrLogSet;
	
	private ExeSQL mExeSQL = new ExeSQL();
	
	private String EventNo = "";
	private String mOperate;
	private boolean mReturnFlag ; //true :一致，不继续比较 ;false:不一致，继续比较
	private MMap mMap= new MMap();
	
    private final String enter = "\r\n"; // 换行符
    
    public DifCheckRuleEngine(FIOperationLog logDeal)
    {
    	tLogDeal = logDeal;
    	EventNo = tLogDeal.getEventNo();
    }

    /**
     * 校检规则执行
     * @param tFIVerifyRulesSet
     * @return
     */
	public boolean ruleExe(String cOperate,VData tVData) 
	{
		mOperate = cOperate;
		if (!getInputData(tVData))
        {
			buildError("DifCheckRuleEngine","ruleExe", "获取参数出错！！");
            tLogDeal.WriteLogTxt("获取参数出错！！" + enter);
    		return false;
        }

    	if (!checkData())
    	{
			buildError("QualityCheckRuleEngine","ruleExe", "数据校检出错！！");
			return false;
    	}
    	
    	//获取质检计划
    	mFIVerifyRuleDefSet = getQualityRuleDef();
    	
    	if(mFIVerifyRuleDefSet!=null&&mFIVerifyRuleDefSet.size()>0)
    	{
    		
    		for(int i=1;i<=mFIVerifyRuleDefSet.size();i++)
        	{
    			tLogDeal.WriteLogTxt("开始执行差异校检规则，规则编码："+mFIVerifyRuleDefSet.get(i).getRuleDefID() + enter);
    			
    			//初始化
    			init();
    			
    			//执行规则
                if(!dealRuels(mFIVerifyRuleDefSet.get(i)))
                {
                	 tLogDeal.WriteLogTxt("差异校检规则执行出现错误，规则编码："+mFIVerifyRuleDefSet.get(i).getRuleDefID() + enter);
                }
                
                if(!PubSubmit())
            	{
                	 tLogDeal.WriteLogTxt("存储错误日志失败，规则编码："+mFIVerifyRuleDefSet.get(i).getRuleDefID() + enter);
            	}
                
                tLogDeal.WriteLogTxt("差异校检规则执行结束，规则编码："+mFIVerifyRuleDefSet.get(i).getRuleDefID() + enter);
        	}
    	}

		return true;
	}

	/**
     * 获取参数
     * @param cInputData
     * @return
     */
    private boolean getInputData(VData cInputData)
    {
    	mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
    	
    	mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
    	if(mTransferData==null)
    	{
			buildError("DifCheckRuleEngine","getInputData", "mTransferData获取参数出错！！");
            tLogDeal.WriteLogTxt("获取参数出错！！" + enter);
    		return false;
    	}
    	
		mCallPointID = (String)mTransferData.getValueByName("CallPointID");
    	mRuleDefID = (String)mTransferData.getValueByName("RuleDefID");
    	mStartDate = (String)mTransferData.getValueByName("StartDate");
    	mEndDate = (String)mTransferData.getValueByName("EndDate");
    	
    	System.out.println("校检执行节点："+mCallPointID);
    	System.out.println("校检规则："+mRuleDefID);
    	return true;
    }
    /**
     * 数据校检
     * @return
     */
    private boolean checkData()
    {
		tLogDeal.WriteLogTxt("数据校检出错！！" + enter);
		if(tLogDeal==null)
		{
			buildError("DifCheckRuleEngine","checkData", "获取日志处理对象LogDeal参数出错！！");
			return false;
		}
		if(!"00".equals(mOperate)&&!"01".equals(mOperate))
		{
			buildError("DifCheckRuleEngine","checkData", "获取规则执行类型错误!");
			return false;
		}
		if("00".equals(mOperate))
		{
			//自动执行
			if(mCallPointID==null||"".equals(mCallPointID))
			{
				buildError("DifCheckRuleEngine","checkData", "自动执行差异，没有获取到校检节点！");
				return false;
			}
		}
		if("01".equals(mOperate))
		{
			//手动执行
			if(mRuleDefID==null||"".equals(mRuleDefID))
			{
				buildError("DifCheckRuleEngine","checkData", "手动执行差异校检，没有获取到校检规则！");
				return false;
			}
		}
    	return true;
    }
    
    private boolean init()
    {
		
		mFIRuleDealErrLogSet = new FIRuleDealErrLogSet();
		mMap = new MMap();
		
    	return true;
    }
    /**
     * 差异校检计划
     * @return
     */
    private FIVerifyRuleDefSet getQualityRuleDef()
    {
		String tSQL = "";
		if("00".equals(mOperate))
		{
			tSQL = "select * from FIVerifyRuleDef where RuleState='1' and RuleType = '02' and CallPointID = '"+mCallPointID+"' with ur " ;
		}
		else if("01".equals(mOperate))
		{
			tSQL = "select * from FIVerifyRuleDef where RuleState='1' and RuleType = '02' and RuleDefID = '"+mRuleDefID+"' with ur " ;
		}
		FIVerifyRuleDefDB tFIVerifyRuleDefDB = new FIVerifyRuleDefDB();
		FIVerifyRuleDefSet tFIVerifyRuleDefSet  = tFIVerifyRuleDefDB.executeQuery(tSQL);
		
		return tFIVerifyRuleDefSet;
    }
    
    /**
     * 差异校检算法-获取开始算法
     * @param tRuleDefID
     * @return
     */
    private FIVerifyRulesSchema getStartRule(String tRuleDefID)
    {
		String tSQL = "select * from FIVerifyRules a where state = '1' and a.RuleDefID = '"+tRuleDefID+"' and Node='00' with ur " ;
		FIVerifyRulesDB tFIVerifyRulesDB = new FIVerifyRulesDB();
		FIVerifyRulesSet tFIVerifyRulesSet  = tFIVerifyRulesDB.executeQuery(tSQL);
		if(tFIVerifyRulesSet==null||tFIVerifyRulesSet.size()<1)
		{
			return null;
		}
		return tFIVerifyRulesSet.get(1);
    }
    
    /**
     * 差异校检算法
     * @param tRuleDefID
     * @return
     */
    private FIVerifyRulesSchema getNextRule(String tRuleDefID,String tNode)
    {
		String tSQL = "select * from FIVerifyRules a where state = '1' and a.ParentNode = '"+tNode+"' and RuleDefID='"+tRuleDefID+"' and ParentNode<>node with ur " ;
		FIVerifyRulesDB tFIVerifyRulesDB = new FIVerifyRulesDB();
		FIVerifyRulesSet tFIVerifyRulesSet  = tFIVerifyRulesDB.executeQuery(tSQL);
		System.out.println(tSQL);
		if(tFIVerifyRulesSet==null||tFIVerifyRulesSet.size()<1)
		{
			return null;
		}
		return tFIVerifyRulesSet.get(1);
    }
    
    /**
     * 批量执行校检规则算法
     * @param tFIVerifyRuleDefSchema
     * @return
     */
    private boolean dealRuels(FIVerifyRuleDefSchema tFIVerifyRuleDefSchema)
    {

    	//获取开始执行规则
    	FIVerifyRulesSchema tFIVerifyRulesSchema = getStartRule(tFIVerifyRuleDefSchema.getRuleDefID());

    	if(tFIVerifyRulesSchema==null)
    	{
    		buildError("DifCheckRuleEngine","dealRuels", "差异校检规则:"+tFIVerifyRuleDefSchema.getRuleDefID()+"，没有查询到起始执行算法！");
    		tLogDeal.WriteLogTxt("差异校检规则:"+tFIVerifyRuleDefSchema.getRuleDefID()+"，没有查询到起始执行算法！"+ enter);
    		return false;
    	}
    	
		//异地规则执行处理
		if(!"".equals(tFIVerifyRulesSchema.getFileName())&&tFIVerifyRulesSchema.getFileName()!=null)
		{
			if(!nonlocal(tFIVerifyRulesSchema))
			{
				return false;
			}
		}
		
    	prepareDealLog(tFIVerifyRuleDefSchema);
    	
		if(!runRuel(tFIVerifyRulesSchema,null,null))
		{
			buildError("DifCheckRuleEngine","dealRuels", "规则类型错误，规则编码："+tFIVerifyRulesSchema.getArithmeticID());
            tLogDeal.WriteLogTxt("规则类型错误，规则编码："+tFIVerifyRulesSchema.getArithmeticID() + enter);	
            mFIRuleDealLogSchema.setRuleResult("Fail");
            return false;
		}
    	
    	return true;
    }
    /**
     * 批次日志初始化
     * @param tFIVerifyRulesSchema
     */
    private void prepareDealLog(FIVerifyRuleDefSchema tFIVerifyRuleDefSchema)
    {
    	mFIRuleDealLogSchema = new FIRuleDealLogSchema();
    	mFIRuleDealLogSchema.setVersionNo(tFIVerifyRuleDefSchema.getVersionNo());
    	mFIRuleDealLogSchema.setCheckBatchNo(FinCreateSerialNo.getLogSerialNo());
    	mFIRuleDealLogSchema.setEventNo(EventNo);
    	mFIRuleDealLogSchema.setCheckType("01");
    	mFIRuleDealLogSchema.setRuleID(tFIVerifyRuleDefSchema.getRuleDefID());
    	mFIRuleDealLogSchema.setCallPointID(mCallPointID);
    	mFIRuleDealLogSchema.setOperator(mGlobalInput.Operator);
    	mFIRuleDealLogSchema.setRulePlanID(tFIVerifyRuleDefSchema.getRuleDefID());
    	mFIRuleDealLogSchema.setMakeDate(PubFun.getCurrentDate());
    	mFIRuleDealLogSchema.setMakeTime(PubFun.getCurrentTime());
    	mFIRuleDealLogSchema.setLogFilePath("");
    	mFIRuleDealLogSchema.setLogFileName("");
    	mFIRuleDealLogSchema.setDataSource("1");
    	
    	mFIRuleDealLogSchema.setRuleResult("Succ");
    	
    	mMap.put(mFIRuleDealLogSchema, "INSERT");
    }
    
    /**
     * 执行校检规则
     * @param tFIVerifyRulesSchema
     * @return
     */
    private boolean runRuel(FIVerifyRulesSchema tFIVerifyRulesSchema,String[]tParams,String[] tValues)
    {
    	//SQL规则
    	if("2".equals(tFIVerifyRulesSchema.getDealMode()))
    	{
    		//结束节点
			SSRS tFinSSRS = new SSRS();
			SSRS tBusSSRS = new SSRS();
			
			String tFinSQL = prepareRule(tFIVerifyRulesSchema.getFinDealSQL(),tParams,tValues);
			String tBusSQL = prepareRule(tFIVerifyRulesSchema.getBusDealSQL(),tParams,tValues);
			System.out.println("tFinSQL:"+tFinSQL);
			System.out.println("tBusSQL:"+tBusSQL);
			
			if(tFinSQL!=null)
				tFinSSRS = mExeSQL.execSQL(tFinSQL);
			if(tBusSQL!=null)
				tBusSSRS = mExeSQL.execSQL(tBusSQL);
			
    		//末节点,产生记录
    		if("99".equals(tFIVerifyRulesSchema.getNode())||"99".equals(tFIVerifyRulesSchema.getParentNode()))
        	{
    			if(tFinSSRS.MaxRow>0)
    			{
        			for(int i=1;i<=tFinSSRS.MaxRow;i++)
        			{
    	    			String tSQL = " select * from FIRuleDealErrLog where Aserialno='"+tFinSSRS.GetText(i, 1)+"' and IndexCode='"+tFinSSRS.GetText(i, 2)+"' and BusinessNo='"+tFinSSRS.GetText(i, 3)+"' and DealState<>'1' with ur ";
    	    			FIRuleDealErrLogSet tFIRuleDealErrLogSet = new FIRuleDealErrLogDB().executeQuery(tSQL);
    	    			if(tFIRuleDealErrLogSet!=null&&tFIRuleDealErrLogSet.size()>0)
    	    			{
    	    				continue;
    	    			}
        				FIRuleDealErrLogSchema  tFIRuleDealErrLogSchema = new FIRuleDealErrLogSchema();
    	    			tFIRuleDealErrLogSchema.setErrSerialNo(FinCreateSerialNo.getErrLogSerialNo());
    	    			tFIRuleDealErrLogSchema.setAserialno(tFinSSRS.GetText(i, 1)); //流水号
    	    			tFIRuleDealErrLogSchema.setIndexCode(tFinSSRS.GetText(i, 2)); //业务号码类型
    	    			tFIRuleDealErrLogSchema.setBusinessNo(tFinSSRS.GetText(i, 3));//业务号码
    	    			tFIRuleDealErrLogSchema.setCheckBatchNo(mFIRuleDealLogSchema.getCheckBatchNo());
    	    			tFIRuleDealErrLogSchema.setRuleID(tFIVerifyRulesSchema.getArithmeticID());
    	    			tFIRuleDealErrLogSchema.setErrInfo(tFinSSRS.GetText(i,4));//错误信息
    	    			tFIRuleDealErrLogSchema.setRulePlanID(tFIVerifyRulesSchema.getRuleDefID());
    	    			tFIRuleDealErrLogSchema.setDealState("0");
    	    			tFIRuleDealErrLogSchema.setCertificateID("");
    	    			tFIRuleDealErrLogSchema.setCallPointID(mCallPointID);

    	    			mFIRuleDealErrLogSet.add(tFIRuleDealErrLogSchema);
        			}
        			mMap.put(mFIRuleDealErrLogSet, "INSERT");
    			}
    			if(tBusSSRS.MaxRow>0)
    			{
        			for(int i=1;i<=tBusSSRS.MaxRow;i++)
        			{
        				String tSQL = " select * from FIRuleDealErrLog where Aserialno='"+tBusSSRS.GetText(i, 1)+"' and IndexCode='"+tBusSSRS.GetText(i, 2)+"' and BusinessNo='"+tBusSSRS.GetText(i, 3)+"' and DealState<>'1' with ur ";
    	    			FIRuleDealErrLogSet tFIRuleDealErrLogSet = new FIRuleDealErrLogDB().executeQuery(tSQL);
    	    			if(tFIRuleDealErrLogSet!=null&&tFIRuleDealErrLogSet.size()>0)
    	    			{
    	    				continue;
    	    			}
        				FIRuleDealErrLogSchema  tFIRuleDealErrLogSchema = new FIRuleDealErrLogSchema();
    	    			tFIRuleDealErrLogSchema.setErrSerialNo(FinCreateSerialNo.getErrLogSerialNo());
    	    			tFIRuleDealErrLogSchema.setAserialno(tBusSSRS.GetText(i, 1)); //流水号
    	    			tFIRuleDealErrLogSchema.setIndexCode(tBusSSRS.GetText(i, 2)); //业务号码类型
    	    			tFIRuleDealErrLogSchema.setBusinessNo(tBusSSRS.GetText(i, 3));//业务号码
    	    			tFIRuleDealErrLogSchema.setCheckBatchNo(mFIRuleDealLogSchema.getCheckBatchNo());
    	    			tFIRuleDealErrLogSchema.setRuleID(tFIVerifyRulesSchema.getArithmeticID());
    	    			tFIRuleDealErrLogSchema.setErrInfo(tFIVerifyRulesSchema.getReturnRemark()+"管理机构为："+tBusSSRS.GetText(i,4));//错误信息
    	    			tFIRuleDealErrLogSchema.setRulePlanID(tFIVerifyRulesSchema.getRuleDefID());
    	    			tFIRuleDealErrLogSchema.setDealState("0");
    	    			tFIRuleDealErrLogSchema.setCertificateID("");
    	    			tFIRuleDealErrLogSchema.setCallPointID(mCallPointID);

    	    			mFIRuleDealErrLogSet.add(tFIRuleDealErrLogSchema);
        			}
        			mMap.put(mFIRuleDealErrLogSet, "INSERT");
    			}
        	}
    		else
    		{
        		//非末级节点
        		for(int i=1;i<=tFinSSRS.MaxRow;i++)
    			{
        			//默认最后一位是比较值
        			String tFinValue = tFinSSRS.GetText(i,tFinSSRS.MaxCol);
        			String tBusValue = tBusSSRS.GetText(i,tFinSSRS.MaxCol);
        			System.out.println("tFinValue:"+tFinValue+",tBusValue:"+tBusValue);
        			if(!tFinValue.equals(tBusValue))
        			{
        				//存在不一致
        				mReturnFlag = false;
        				tLogDeal.WriteLogTxt("存在不一致数据，"+tFIVerifyRulesSchema.getArithmeticID()+",差异数据为："+tBusValue+"与"+tFinValue+ enter);
        				if("1".equals(tFIVerifyRulesSchema.getReturnFlag()))
        				{
        	    			//默认最后一位是比较值
        	    			String tReturn= tFIVerifyRulesSchema.getReturnStore();
        	    			mParams = tReturn.split(";");
        	    			mValues = new String[mParams.length];
        					for(int j=0;j<mParams.length;j++)
        					{
        						mValues[j] = tBusSSRS.GetText(i,j+1);
        					}
        				}
        				else
        				{
        					mParams = null;
        					mValues = null;
        				}
        				
                		//获取下一个规则算法
        				FIVerifyRulesSchema ttFIVerifyRulesSchema = getNextRule(tFIVerifyRulesSchema.getRuleDefID(),tFIVerifyRulesSchema.getNode());
                    	if(ttFIVerifyRulesSchema==null)
                    	{
                    		buildError("DifCheckRuleEngine","dealRuels", "差异校检规则算法编码:"+tFIVerifyRulesSchema.getArithmeticID()+"，没有取到下个执行规则！");
                    		tLogDeal.WriteLogTxt("差异校检规则:"+tFIVerifyRulesSchema.getArithmeticID()+"，，没有取到下个执行规则！"+ enter);
                    		continue;
                    	}
                    	//递归执行
                    	runRuel(ttFIVerifyRulesSchema,mParams,mValues);
        			}
    			}
    		}
    	}	
    	return true;
    }
    
    private boolean nonlocal(FIVerifyRulesSchema tFIVerifyRulesSchema)
    {
        FIDataBaseLinkDB tFIDataBaseLinkDB = new FIDataBaseLinkDB();
        String strKeyDefSQL = "select * from FIDataBaseLink where InterfaceCode ='"+tFIVerifyRulesSchema.getFileName()+"' with ur ";
        FIDataBaseLinkSet tFIDataBaseLinkSet = tFIDataBaseLinkDB.executeQuery(strKeyDefSQL);
		
        if(tFIDataBaseLinkSet!=null&&tFIDataBaseLinkSet.size()>0)
        {
	        try 
			{
				DBConn con = DBConnPool.getConnection(tFIDataBaseLinkSet.get(1));
				mExeSQL = new ExeSQL(con);
			} 
			catch (Exception e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				buildError("DifCheckRuleEngine","runRuel", "异地数据源："+tFIVerifyRulesSchema.getFileName()+"初始化出错！");
				tLogDeal.WriteLogTxt("异地数据源初始化时出错，数据源："+tFIVerifyRulesSchema.getFileName() + enter);
				return false;
			}
        }
        return true;
    }
	/**
	 * 规则参数初始化
	 * @param tFIDataExtractRulesSchema
	 * @return
	 */
	private String prepareRule(String tSQL ,String[]tParams ,String[] tValues)
	{
    	String tSql = tSQL;
    	if(tSql==null||"".equals(tSql))
    	{
    		return null;
    	}
    	mPubCalculator.addBasicFactor("StartDate", mStartDate);
		mPubCalculator.addBasicFactor("EndDate", mEndDate);
		if(tValues!=null && tParams!=null)
		{
			for(int i=0;i<tParams.length;i++)
			{
				mPubCalculator.addBasicFactor(tParams[i], tValues[i]);
			}
		}
        mPubCalculator.setCalSql(tSql);
        tSql = mPubCalculator.calculateEx();
        
        return tSql;
	}
	
    /**
     * 提交保存校检日志
     * @return
     */
    private boolean PubSubmit()
    {

		VData tInputData = new VData();
		PubSubmit tPubSubmit = new PubSubmit();
		
		//数据提取校检后更新数据校检状态
		String update2 = "update FIAboriginalMain a set a.checkflag = '01' where a.checkflag='00' and a.state ='04' " +
		"and exists(select 1 from FIRuleDealErrLog b where a.IndexCode = b.IndexCode and a.IndexNo=b.BusinessNo and b.DealState = '0' and b.CheckBatchNo='"+mFIRuleDealLogSchema.getCheckBatchNo()+"') ";
		
		String update1 = "update FIAboriginalGenDetail a set a.checkflag = '01' where a.checkflag='00' and a.state ='00' " +
		"and exists(select 1 from FIRuleDealErrLog b where a.IndexCode = b.IndexCode and a.IndexNo=b.BusinessNo and b.DealState = '0' and b.CheckBatchNo='"+mFIRuleDealLogSchema.getCheckBatchNo()+"') ";


		mMap.put(update1, "UPDATE");
		mMap.put(update2, "UPDATE");
		
		tInputData.add(mMap);
		
        if (!tPubSubmit.submitData(tInputData, ""))
        {
    		mMap = null;
    		tInputData = null;
    		
        	this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buildError("DifCheckRuleEngine","PubSubmit", "校检数据保存时出错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("校检数据保存时出错，提示信息为：，提示信息为：" +tPubSubmit.mErrors.getFirstError() + enter);           
    		
            return false;
        }
		
		mMap = null;
		tInputData = null;
    	return true;
    }
    

    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub

		try 
		{
			FIOperationLog tLogDeal = new FIOperationLog("001","00");
			DifCheckRuleEngine tDifCheckRuleEngine = new DifCheckRuleEngine(tLogDeal);
			
			TransferData mTransferData  = new TransferData();
			mTransferData.setNameAndValue("RuleDefID","D000000001");
			mTransferData.setNameAndValue("StartDate","2011-11-21");
			mTransferData.setNameAndValue("EndDate","2011-11-21");
		  
			VData tVData = new VData();
			GlobalInput tG = new GlobalInput();
			tVData.add(mTransferData);
			tVData.add(tG);
			
	    	if(!tDifCheckRuleEngine.ruleExe("01", tVData))
			{
	    		System.out.println("差异校检规则执行出错！");
			}
	    	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
}
