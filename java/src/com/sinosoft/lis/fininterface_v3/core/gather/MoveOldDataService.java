package com.sinosoft.lis.fininterface_v3.core.gather;


import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LIAboriginalDataSet;
import com.sinosoft.lis.vschema.LIDataTransResultSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.VData;
/**
 * <p>Title: 数据同步</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class MoveOldDataService {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors=new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData= new VData();
	/** 全局数据 */
	private GlobalInput mGlobalInput =new GlobalInput() ;
	
	private PubSubmit mPubSubmit = new PubSubmit();
	/** 数据操作字符串 */
	private String mOperate;
    private final String enter = "\r\n"; // 换行符
    private String mBatchNo;
    
	/** 日志处理*/
	private FIOperationLog tLogDeal = null;
	
	public MoveOldDataService(FIOperationLog logDeal){
		tLogDeal = logDeal;
	}
	
	/**
	 * 凭证导出处理
	 * @param cInputData
	 * @param cOperate
	 * @return
	 */
	public boolean submitData(VData cInputData,String cOperate)
	{
		//将操作数据拷贝到本类中
		this.mInputData = cInputData;
		this.mOperate =cOperate;
		
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData())
		{
			return false;
		}
		
		if (!checkData())
		{
			return false;
		}
		
		//向旧平台表中同步数据  ,暂时注掉，同步稳定后放开同步
		if(!synchronousData())
		{
			return false;
		}
		
		tLogDeal.Complete(true);
		
		return true;
	}
	
	/**
	 * 获取前台数据
	 * @return
	 */
	private boolean getInputData()
	{
		mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
		mBatchNo = (String) mInputData.get(1);
		
		return true;
	}
	
	private boolean checkData()
	{
    	if(mBatchNo==null||"".equals(mBatchNo))
    	{
    		buildError("MoveOldDataService","checkData","批次号获取失败！");
    		return false;
    	}
    	if(mGlobalInput==null)
    	{
    		buildError("MoveOldDataService","checkData","全局对象获取失败！");
    		return false;
    	}
    	
//		try 
//		{
//			tLogDeal = new FIOperationLog(mGlobalInput.Operator,"70");
//			
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return false;
//		}
		
		return true;
	}
	
	
    /**
     * 向旧平台表（LIAboriginalData\LIDataTransResult）中同步数据
     * @return
     */
    private boolean synchronousData()
    {
		//(1)准业务转换表同步	
		RSWrapper rsWrapper1 = new RSWrapper();
		LIAboriginalDataSet tLIAboriginalDataSet = new LIAboriginalDataSet();
		String tSQL = getAboriginalDataSQL(mBatchNo);
//		String updateFiabSQL = "update FIAbStandardData a set a.readstate = '1' where  a.readstate = '0' and a.state = '01' and  a.checkflag = '00' and exists( select 1 from fivoucherdatadetail b where a.serialno= b.SerialNo and b.checkflag='00'  and  b.batchno='"+mBatchNo+" ') and  exists(select 1 from LIAboriginalData c where  c.batchno = '"+mBatchNo+"' and c.SerialNo = 'N' || substr(a.serialno,2,24)) ";
		
        if (!rsWrapper1.prepareData(tLIAboriginalDataSet, tSQL))
        {        	        	
            mErrors.copyAllErrors(rsWrapper1.mErrors);
            System.out.println( "准业务转换表往rapper里放数据出错,提示信息为：" +rsWrapper1.mErrors.getFirstError());
            buildError("MoveOldDataService","synchronousData", "准业务转换表往rapper里放数据出错,提示信息为：" +rsWrapper1.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("准业务转换表往rapper里放数据出错,提示信息为：" +rsWrapper1.mErrors.getFirstError() + enter);
            tLogDeal.buildErrorLog("准业务转换表往rapper里放数据出错，执行出现错误,需要重新同步数据！");
            return false;
        }	
        do
        {
        	rsWrapper1.getData();
            if (tLIAboriginalDataSet != null && tLIAboriginalDataSet.size() > 0)
            { 	
            	dealABData(tLIAboriginalDataSet);
/*                MMap tMap= new MMap();
        		VData tInputData = new VData();            	
        		tMap.put(updateFiabSQL, "UPDATE");
        		tInputData.add(tMap);
        		PubSubmit mPubSubmitU = new PubSubmit();
                if (!mPubSubmitU.submitData(tInputData, ""))
                {
                    this.mErrors.copyAllErrors(mPubSubmit.mErrors);
                    System.out.println("更改准业务转换表状态readstate出错，批次号："+mBatchNo+"");
                    buildError("MoveOldDataService","synchronousData", "更改准业务转换表状态readstate出错，批次号："+mBatchNo+"，提示信息为：" +mPubSubmit.mErrors.getFirstError());
                    tLogDeal.WriteLogTxt("更改准业务转换表状态readstate出错，批次号："+mBatchNo+"，提示信息为：" +mPubSubmit.mErrors.getFirstError() + enter);
                    tLogDeal.buildErrorLog("更改准业务转换表状态readstate出错，批次号："+mBatchNo );
                    return false;
                }
                tInputData.clear();  
                tMap = new MMap();*/
                
            }                 
        }while(tLIAboriginalDataSet != null && tLIAboriginalDataSet.size() > 0);
        tLIAboriginalDataSet = null;
        tLogDeal.WriteLogTxt("准业务转换表同步完成！" + enter);
        System.out.println("准业务转换表同步完成！");
        
		//(1)凭证表同步	
		RSWrapper rsWrapper2 = new RSWrapper();
		LIDataTransResultSet tLIDataTransResultSet = new LIDataTransResultSet();
		String tSQL2 = getLIDataTransResultSQL(mBatchNo);
//		String updateFivouSQL = "update FIVoucherDataDetail a set a.readstate = '1' where a.checkflag='00' and   a.readstate = '0'  and  a.batchno = '"+mBatchNo+"' and  exists (select 1 from lidatatransresult b where  b.batchno = '"+mBatchNo+"' and 'N' || substr(a.fserialno,2,24) = b.SerialNo)";
		
        if (!rsWrapper2.prepareData(tLIDataTransResultSet, tSQL2))
        {        	        	
            mErrors.copyAllErrors(rsWrapper2.mErrors);
            System.out.println("凭证表往rapper里放数据出错,提示信息为：" +rsWrapper2.mErrors.getFirstError());
            buildError("MoveOldDataService","synchronousData", "凭证表往rapper里放数据出错,提示信息为：" +rsWrapper2.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("凭证表往rapper里放数据出错,提示信息为：" +rsWrapper2.mErrors.getFirstError() + enter);
            tLogDeal.buildErrorLog("凭证表往rapper里放数据出错，执行出现错误,需要重新同步数据！");
            return false;
        }	
        do
        {
        	rsWrapper2.getData();
            if (tLIDataTransResultSet != null && tLIDataTransResultSet.size() > 0)
            { 	
            	dealResultData(tLIDataTransResultSet);
/*                MMap tMap= new MMap();
        		VData tInputData = new VData();            	
        		tMap.put(updateFivouSQL, "UPDATE");
        		tInputData.add(tMap);
        		PubSubmit mPubSubmitU = new PubSubmit();
                if (!mPubSubmitU.submitData(tInputData, ""))
                {
                    this.mErrors.copyAllErrors(mPubSubmit.mErrors);
                    System.out.println("更改凭证表状态readstate出错，批次号："+mBatchNo+"");                    
                    buildError("MoveOldDataService","synchronousData", "更改凭证表状态readstate出错，批次号："+mBatchNo+"，提示信息为：" +mPubSubmit.mErrors.getFirstError());
                    tLogDeal.WriteLogTxt("更改凭证表状态readstate出错，批次号："+mBatchNo+"，提示信息为：" +mPubSubmit.mErrors.getFirstError() + enter);
                    tLogDeal.buildErrorLog("更改凭证表状态readstate出错，批次号："+mBatchNo );
                    return false;
                }
                tInputData.clear();  
                tMap = new MMap();*/
            }                 
        }while(tLIDataTransResultSet != null && tLIDataTransResultSet.size() > 0);
        tLIDataTransResultSet = null;      
        tLogDeal.WriteLogTxt("凭证表同步完成！" + enter);
        System.out.println("凭证表同步完成！");        
        
		//(1)批次信息更改	
        String updateFimaSQL="update FIVoucheManage set operator='cwjk_ytb' where batchno='"+mBatchNo+"'";
		String insertlidissSQL="insert into lidistilllog values('"+mBatchNo+"',0,(select accountdate from FIVoucherDataDetail where batchno='"+mBatchNo+"' group by accountdate order by count(1) desc fetch first 1 row only),(select accountdate from FIVoucherDataDetail where batchno='"+mBatchNo+"' group by accountdate order by count(1) desc fetch first 1 row only),null,null,'SUCC','server','86','"+PubFun.getCurrentDate()+"','"+PubFun.getCurrentTime()+"')";
        MMap tMap= new MMap();
		VData tInputData = new VData();    
		tMap.put(updateFimaSQL, "UPDATE");		
		tMap.put(insertlidissSQL, "INSERT");		
		tInputData.add(tMap);
		PubSubmit mPubSubmitUI = new PubSubmit();
        if (!mPubSubmitUI.submitData(tInputData, ""))
        {
            this.mErrors.copyAllErrors(mPubSubmit.mErrors);
            System.out.println( "批次信息更改出错，批次号："+mBatchNo+"");
            buildError("MoveOldDataService","synchronousData", "批次信息更改出错，批次号："+mBatchNo+"，提示信息为：" +mPubSubmit.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("批次信息更改出错，批次号："+mBatchNo+"，提示信息为：" +mPubSubmit.mErrors.getFirstError() + enter);
            tLogDeal.buildErrorLog("批次信息更改据出错，批次号："+mBatchNo );
            return false;
        }
        tLogDeal.WriteLogTxt("批次信息更改完成" + enter);
        System.out.println("批次信息更改完成！");   
        tInputData.clear();  
        tMap = new MMap();		
        rsWrapper1.close();
        rsWrapper2.close();	
		
		return true;
		
		}	
      
    private boolean dealResultData(LIDataTransResultSet tLIDataTransResultSet) {

		// TODO Auto-generated method stub
		if(tLIDataTransResultSet!=null && tLIDataTransResultSet.size()>0)
		{
			MMap tMap= new MMap();
			VData tInputData = new VData();			
			tMap.put(tLIDataTransResultSet, "INSERT");
			tInputData.add(tMap);
		    PubSubmit tPubSubmit = new PubSubmit();
	        if (!tPubSubmit.submitData(tInputData, ""))
	        {
	            tMap = null;
	            tInputData = null;
	        	this.mErrors.copyAllErrors(tPubSubmit.mErrors);
	        	System.out.println("往老凭证表插入数据时报错!");
	            buildError("MoveOldDataService","dealResultData", "往老凭证表插入数据时报错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
	            tLogDeal.WriteLogTxt("往老凭证表插入数据时报错，提示信息为：" +tPubSubmit.mErrors.getFirstError() + enter);
	            return false;
	        }
	        tMap = null;
	        tInputData = null;
	        tLIDataTransResultSet=null;
			        
		}
			return true;		
		
	}

	private boolean dealABData(LIAboriginalDataSet tLIAboriginalDataSet) {

		// TODO Auto-generated method stub
		if(tLIAboriginalDataSet!=null && tLIAboriginalDataSet.size()>0)
		{
			MMap tMap= new MMap();
			VData tInputData = new VData();			
			tMap.put(tLIAboriginalDataSet, "INSERT");
			tInputData.add(tMap);
		    PubSubmit tPubSubmit = new PubSubmit();
	        if (!tPubSubmit.submitData(tInputData, ""))
	        {
	            tMap = null;
	            tInputData = null;
	        	this.mErrors.copyAllErrors(tPubSubmit.mErrors);
	        	System.out.println("往老准业务转换表插入数据时报错!");
	            buildError("MoveOldDataService","dealABData", "往老准业务转换表插入数据时报错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
	            tLogDeal.WriteLogTxt("往老准业务转换表插入数据时报错，提示信息为：" +tPubSubmit.mErrors.getFirstError() + enter);
	            return false;
	        }
	        tMap = null;
	        tInputData = null;
	        tLIAboriginalDataSet=null;
			        
		}
			return true;		
		
	}

	/**
     * 向LIAboriginalData中同步数据SQL
     * @param tBatchNo
     * @return
     */
    private String getAboriginalDataSQL(String tBatchNo)
    {
    	StringBuffer tSQL  = new StringBuffer();
    	tSQL.append("select ");
    	tSQL.append("'N' || substr(a.SerialNo,2,24) as SERIALNO, ") ; //key
    	tSQL.append("'"+tBatchNo+"' as BATCHNO, ") ;  //NOT NULL
    	tSQL.append("a.BusTypeID as CLASSID, ") ;  //NOT NULL
    	tSQL.append("indexno as TEMPFEENO, ") ;
    	tSQL.append("indexno as ACTUGETNO, ") ;
    	tSQL.append("indexno as PAYNO, ") ;
    	tSQL.append("'' as EDORACCEPTNO, ") ;
    	tSQL.append("a.EndorsementNo as ENDORSEMENTNO, ") ;
    	tSQL.append("'' as CASENO, ") ;
    	tSQL.append("a.FeeType as FEEOPERATIONTYPE, ") ;
    	tSQL.append("a.FeeDetail as FEEFINATYPE, ") ;
    	tSQL.append("a.ListFlag as LISTFLAG, ") ;//?
    	tSQL.append("a.GrpContNo as GRPCONTNO, ") ;
    	tSQL.append("a.GrpPolNo as GRPPOLNO, ") ;
    	tSQL.append("a.ContNo as CONTNO, ") ;
    	tSQL.append("a.PolNo as POLNO, ") ;
    	tSQL.append("'' as PAYINTVFLAG, ") ;
    	tSQL.append("a.PayIntv as PAYINTV, ") ;
    	tSQL.append("a.AccountDate as PAYDATE, ") ;
    	tSQL.append("'' as PAYTYPE, ") ;  //?
    	tSQL.append("a.AccountDate as ENTERACCDATE, ") ;
    	tSQL.append("a.AccountDate as CONFDATE, ") ;
    	tSQL.append("a.LastPayToDate as LASTPAYTODATE, ") ;
    	tSQL.append("a.CurPayToDate as CURPAYTODATE, ") ;
    	tSQL.append("a.CValiDate as CVALIDATE, ") ;
    	tSQL.append("a.PolYear as POLYEAR, ") ;
    	tSQL.append("a.FirstYearFlag as FIRSTYEARFLAG, ") ;
    	tSQL.append("a.PayCount as PAYCOUNT, ") ;
    	tSQL.append("a.FirstTermFlag as FIRSTTERMFLAG, ") ;
    	tSQL.append("a.Years as YEARS, ") ;
    	tSQL.append("a.RiskCode as RISKCODE, ") ;
    	tSQL.append("a.RiskPeriod as RISKPERIOD, ") ;
    	tSQL.append("a.RiskType as RISKTYPE, ") ;
    	tSQL.append("a.RiskType1 as RISKTYPE1, ") ;
    	tSQL.append("a.SaleChnl as SALECHNL, ") ;
    	tSQL.append("a.ManageCom as MANAGECOM, ") ;
    	tSQL.append("a.ExecuteCom as EXECUTECOM, ") ;
    	tSQL.append("a.AgentCom as AGENTCOM, ") ;
    	tSQL.append("a.AgentCode as AGENTCODE, ") ;
    	tSQL.append("a.AgentGroup as AGENTGROUP, ") ;
    	tSQL.append("a.BankCode as BANKCODE, ") ;
    	tSQL.append("a.AccName as ACCNAME, ") ;  //?
    	tSQL.append("a.BankAccNo as BANKACCNO, ") ;
    	tSQL.append("a.SumActuMoney as SUMDUEMONEY, ") ;
    	tSQL.append("a.SumActuMoney as SUMACTUMONEY, ") ;
    	tSQL.append("'R' as CURRENCY, ") ;
    	tSQL.append("a.PayMode as PAYMODE, ") ;
    	tSQL.append("a.OperationType as OPERATIONTYPE, ") ;
    	tSQL.append("a.CashFlowNo as BUDGET, ") ;
    	tSQL.append("a.SaleChnlDetail as SALECHNLDETAIL, ") ;
    	tSQL.append("'' as STANDBYSTRING1, ") ;
    	tSQL.append("'' as STANDBYSTRING2, ") ;
    	tSQL.append("'' as STANDBYSTRING3, ") ;
    	tSQL.append("cast(null as int) as STANDBYNUM1, ") ;
    	tSQL.append("cast(null as int) as STANDBYNUM2, ") ;
    	tSQL.append("cast(null as date) as STANDBYDATE1, ") ;
    	tSQL.append("cast(null as date) as STANDBYDATE2, ") ;
    	tSQL.append("a.CustomerID as BCLIENT, ") ;
    	tSQL.append("a.MarketType as MARKETTYPE, ") ;
    	tSQL.append("a.PremiumType as PREMIUMTYPE, ") ;
    	tSQL.append("a.FirstYear as FIRSTYEAR ") ;
    	tSQL.append(" from FIAbStandardData a where a.CheckFlag = '00' and a.ReadState='0' ");
    	tSQL.append(" and exists(select 1 from FIVoucherDataDetail b where a.SerialNo = b.SerialNo and b.CheckFlag = '00' and b.batchno = '"+tBatchNo+"')");
    	tSQL.append(" and not exists(select 1 from LIAboriginalData c where  c.batchno = '"+tBatchNo+"' and c.SerialNo = 'N' || substr(a.serialno,2,24)) with ur ");
    	
    	return tSQL.toString() ;
    }
    
    /**
     * 向LIDataTransResult中同步数据SQL
     * @param tBatchNo
     * @return
     */
    private String getLIDataTransResultSQL(String tBatchNo)
    {
    	StringBuffer tSQL  = new StringBuffer();
    	tSQL.append("select ");
    	tSQL.append("'N' || substr(a.FSerialNo,2,24) as SERIALNO,") ;      //key
    	tSQL.append("a.BatchNo as BATCHNO ,") ;      //NOT NULL
    	tSQL.append("(select codealias from ficodetrans where codetype='ClassTypeNew' and code=b.bustypeid and othersign=a.listflag and versionno=a.certificateid fetch first 1 rows only) as CLASSTYPE , ") ; //再保提数用
    	tSQL.append("(select BusTypeID from FIAbStandardData e where e.serialno=a.serialno fetch first 1 rows only) as CLASSID , ") ;      //NOT NULL    	
    	tSQL.append("trim(a.contno) ||','||trim(b.indexno)||','||trim(a.costid)||','||trim(a.vouchertype) as KEYUNIONVALUE , ") ;//NOT NULL ?    	    	    	
    	tSQL.append("a.AccountCode as ACCOUNTCODE , ") ;  //NOT NULL
    	tSQL.append("a.FinItemType as FINITEMTYPE , ") ;  //NOT NULL
    	tSQL.append("a.SumMoney as SUMMONEY, ") ;      //NOT NULL
    	tSQL.append("a.AccountDate as ACCOUNTDATE , ") ;  //NOT NULL
    	tSQL.append("a.SaleChnl as SALECHNL, ") ;
    	tSQL.append("a.ManageCom as MANAGECOM , ") ;    //NOT NULL
    	tSQL.append("a.BankAccNo as BANKACCNO , ") ;
    	tSQL.append("a.BankCode as BANKCODE, ") ;
    	tSQL.append("a.RiskCode as RISKCODE, ") ;
    	tSQL.append("a.AgentCom as AGENTCOM, ") ;
    	tSQL.append("a.AgentCode as AGENTCODE , ") ;
    	tSQL.append("a.AgentGroup as AGENTGROUP, ") ;
    	tSQL.append("a.ListFlag as LISTFLAG, ") ;      //NOT NULL
    	tSQL.append("a.ContNo as CONTNO, ") ;        //NOT NULL
    	tSQL.append("a.CostCenter as COSTCENTER, ") ;
    	tSQL.append("'' as FEETYPE , ") ;  //?
    	tSQL.append("'R' as CURRENCY, ") ;
    	tSQL.append("a.CashFlowNo as BUDGET, ") ;
    	tSQL.append("a.ExecuteCom as EXECUTECOM, ") ;
    	tSQL.append("a.SupplierNo as STANDBYSTRING1, ") ;
    	tSQL.append("'' as STANDBYSTRING2, ") ;
    	tSQL.append("'N' || substr(a.SerialNo,2,24) as STANDBYSTRING3, ") ;
    	tSQL.append("cast(null as int) as STANDBYNUM1 , ") ;
    	tSQL.append("cast(null as int) as STANDBYNUM2 , ") ;
    	tSQL.append("cast(null as date) as STANDBYDATE1, ") ;
    	tSQL.append("cast(null as date) as STANDBYDATE2, ") ;
    	tSQL.append("a.BClient as BCLIENT , ") ;
    	tSQL.append("a.MarketType as MARKETTYPE, ") ;
    	tSQL.append("a.Pcont as PCONT , ") ;
    	tSQL.append("a.PolYear as POLYEAR , ") ;
    	tSQL.append("a.Years as YEARS , ") ;
    	tSQL.append("a.PremiumType as PREMIUMTYPE , ") ;
    	tSQL.append("a.FirstYear as FIRSTYEAR  ") ;
    	tSQL.append(" from FIVoucherDataDetail a ,FIAbStandardData b where a.CheckFlag = '00' and a.ReadState='0' and a.batchno = '"+tBatchNo+"' and a.SumMoney <> 0 and a.serialno=b.serialno");   	
    	tSQL.append(" and not exists (select 1 from lidatatransresult c where  c.batchno = '"+tBatchNo+"' and 'N' || substr(a.fserialno,2,24) = c.SerialNo)  with ur"); 
    	 
    	System.out.println("daoSQL:"+ tSQL);
    	
    	return tSQL.toString() ;
    }   
    
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
