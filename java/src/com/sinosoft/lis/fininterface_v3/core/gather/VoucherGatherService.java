package com.sinosoft.lis.fininterface_v3.core.gather;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIVoucherDataGatherSchema;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
/**
 * <p>Title: 凭证数据汇总</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class VoucherGatherService {
	
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    public GlobalInput mGlobalInput = new GlobalInput();
        
	/** 日志处理*/
	private FIOperationLog tLogDeal = null;
	private FIRulesVersionSet mFIRulesVersionSet;
	private TransferData mTransferData ;
    private String mBatchNo;
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    
    private final String enter = "\r\n"; // 换行符
    
    public VoucherGatherService(FIOperationLog logDeal)
    {
        tLogDeal = logDeal;
    }
    
    public boolean dealService(VData cInputData)
    {  	
    	if(!getInputData(cInputData))
    	{
    		return false;
    	}
    	//数据校检
    	if(!checkData())
    	{
    		return false;
    	}
    	//凭证汇总
    	if(!gatherData())
    	{
    		return false;
    	}
    	//历史数据备份
//    	if(!backData())
//    	{
//    		return false;
//    	}
    	tLogDeal.Complete(true);
    	return true;
    }
    
    /**
     * 获取数据
     * @param cInputData
     * @return
     */
    private boolean getInputData(VData cInputData)
    {
    	mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
    	
    	mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
    	
    	if(mTransferData==null||mTransferData==null)
    	{
			buildError("VoucherGatherService","getInputData", "mTransferData获取参数出错！！");
            tLogDeal.WriteLogTxt("获取参数出错！！" + enter);
    		return false;
    	}
    	
    	mBatchNo = (String)mTransferData.getValueByName("BatchNo");
    	
    	return true;
    }
    
    /**
     * 校检数据
     * @return
     */
    private boolean checkData()
    {  	
    	 try
         {
             String rSQL = "select * from FIVoucheManage a where BatchNo = '" + mBatchNo + "' and state='30' with ur ";
             
             System.out.println(rSQL);
             FIVoucheManageSet tFIVoucheManageSet = new FIVoucheManageDB().executeQuery(rSQL);
             
             if(tFIVoucheManageSet==null||tFIVoucheManageSet.size()<1)
             {
                 buildError("VoucherGatherService", "checkData","该批次数据已经汇总");
                 return false;
             }

             ExeSQL tExeSQL = new ExeSQL();
             SSRS tSSRS = new SSRS();
             tSSRS = tExeSQL.execSQL( "select min(AccountDate),max(AccountDate) from FIVoucherDataDetail where batchno='" +mBatchNo + "' with ur ");
             if(tExeSQL.mErrors.needDealError())
             {
                 buildError("VoucherGatherService","checkData","查询该批次数据对应时间区间出错");
                 return false;
             }

             String StartDate = tSSRS.GetText(1, 1);
             String EndDate = tSSRS.GetText(1, 2);

             if(StartDate == null || EndDate == null || StartDate.equals("") || EndDate.equals(""))
             {
                 buildError("VoucherGatherService","checkData","未查询到该批次数据对应时间区间，该批次数据可能为空");
                 return false;
             }

             //校验该批数据账务日期是否对应开启的会计区间
             if(!getPeriodManagement(StartDate, EndDate))
             {
            	 tLogDeal.WriteLogTxt("获取参数出错！！" + enter);
            	 return false;
             }

             //校验该批数据是否对应多个账务规则版本，并获得规则版本号码
             if (!getRuleVersion(StartDate, EndDate)) 
             {
                 return false;
             }

         }
         catch (Exception ex)
         {
             buildError("VoucherGatherService", "infoCheck","执行条件检测出现异常，信息为" + ex.getMessage());
             return false;
         }
        return true;
    }   

    /**
     * 校检会计期间
     * @param sDate
     * @param eDate
     * @return
     */
    private boolean getPeriodManagement(String sDate, String eDate)
    {
        String sql = "  select * from FIPeriodManagement where StartDate<='" + eDate + "' " +
                     " and EndDate>='" + sDate + "' and state='1' order by StartDate asc with ur ";
        System.out.println("校检会计期间"+sql);
        FIPeriodManagementSet tFIPeriodManagementSet = new FIPeriodManagementSet();
        FIPeriodManagementDB tFIPeriodManagementDB = new FIPeriodManagementDB();
        tFIPeriodManagementSet = tFIPeriodManagementDB.executeQuery(sql);
        if (tFIPeriodManagementSet.size() ==0)
        {
            buildError("FICertificateGatherBL","getPeriodManagement", "您输入的时间区间所对应的会计区间尚未开启，请重新输入起始日期！");
            return false;
        }
        else if(tFIPeriodManagementSet.size() !=1)
        {
            String tInfo = "您输入的时间区间内查询到"+tFIPeriodManagementSet.size()+"个有效会计区间！分别是";
            for(int i =0;i<tFIPeriodManagementSet.size();i++)
            {
                if(i==0)
                {
                    tInfo += "[" + tFIPeriodManagementSet.get(i + 1).getStartDate() + "][" +  tFIPeriodManagementSet.get(i + 1).getEndDate() + "]";
                }
                else
                {
                    tInfo += "," + "[" + tFIPeriodManagementSet.get(i + 1).getStartDate() + "][" +  tFIPeriodManagementSet.get(i + 1).getEndDate() + "]";
                }
            }
            buildError("VoucherGatherService","getPeriodManagement", tInfo);
            return false;
        }
        return true;
    }
    
    /**
     * 校检账务规则版本
     * @param sDate
     * @param eDate
     * @return
     */
    private boolean getRuleVersion(String sDate, String eDate)
    {

        try
        {
            String sql = " select * from FIRulesVersion where  versionstate ='01' and StartDate<='" + eDate + "' "
                         + " and (EndDate>'" + sDate + "' or EndDate is null)" + " order by startdate asc with ur ";
            
            System.out.println("账务规则版本"+sql);
            FIRulesVersionDB tFIRulesVersionDB = new FIRulesVersionDB();
            mFIRulesVersionSet = tFIRulesVersionDB.executeQuery(sql);
            if (mFIRulesVersionSet.size() == 0)
            {
                buildError("VoucherGatherService","getRuleVersion", "您输入的时间区间内提数规则无版本，请重新输入起始日期！");
                return false;
            }
            else if (mFIRulesVersionSet.size() != 1)
            {
                String tInfo = "您输入的时间区间内查询到" + mFIRulesVersionSet.size() + "个提数有效版本！版本分别是：";
                for (int i = 0; i < mFIRulesVersionSet.size(); i++) {
                    if (i == 0) {
                        tInfo += "[" + mFIRulesVersionSet.get(i + 1).getStartDate() + "][" +  mFIRulesVersionSet.get(i + 1).getEndDate()+ "]";
                    } else {
                        tInfo += "," +  "[" + mFIRulesVersionSet.get(i + 1).getStartDate() + "][" +  mFIRulesVersionSet.get(i + 1).getEndDate()+ "]";
                    }
                }
            }
            
            return true;
        }
        catch (Exception e)
        {
            buildError("VoucherGatherService","getRuleVersion", "提数规则版本核对出现异常，异常信息:" + e.getMessage());
            return false;
        }
    }
    
    /**
     * 凭证汇总
     * @return
     */
    private boolean gatherData()
    {
    	try
        {  
    		String oSQL = getGatherSQL();
            System.out.println("凭证汇总SQL："+oSQL);

//            FIVoucherDataGatherSet tFIVoucherDataGatherSet = new FIVoucherDataGatherDB().executeQuery(oSQL);
//            if(tFIVoucherDataGatherSet!=null&&tFIVoucherDataGatherSet.size()>0)
//            {           	
//                for (int i = 1; i <= tFIVoucherDataGatherSet.size(); i++)
//                {
//                	FIVoucherDataGatherSchema tFIVoucherDataGatherSchema = tFIVoucherDataGatherSet.get(i);
//                	tFIVoucherDataGatherSchema.setVSerialNo(FinCreateSerialNo.getFGSerialNo()rialNo());
//                    tFIVoucherDataGatherSchema.setOperator(mGlobalInput.Operator);
//                	tFIVoucherDataGatherSchema.setMakeDate(PubFun.getCurrentDate());
//                	tFIVoucherDataGatherSchema.setMakeTime(PubFun.getCurrentTime());
//                	tFIVoucherDataGatherSchema.setModifyDate(PubFun.getCurrentDate());
//                	tFIVoucherDataGatherSchema.setModifyTime(PubFun.getCurrentTime());
//                }
                String insertGatherData = "insert into FIVoucherDataGather select bb.BatchNo,bb.VersionNo,to_char((nextval for SEQ_LF_VSerialID)) as VSerialNo,bb.ImportType,bb.DCFlag,bb.VoucherType,bb.Currency,bb.DepCode,bb.FinMangerCount,bb.AccountCode,bb.BClient,bb.CostCenter,bb.RiskCode,bb.MarketType,bb.Chinal,bb.PCont,bb.CashFlowNo,bb.ChargeDate,bb.ChargeTime,sum(bb.SumMoney),bb.VoucherYear,bb.PolYear,bb.Years,bb.AgentNo,bb.PremiumType,bb.FirstYear,bb.StandByString1,bb.StandByString2,bb.StandByString3,bb.StandByNum1,bb.StandByNum2,bb.StandByDate1,bb.StandByDate2,bb.ReadState,bb.MakeDate,bb.MakeTime,bb.ModifyDate,bb.ModifyTime,bb.Operator,bb.ContNo from ("+ oSQL +") bb " +
                		" group by bb.BatchNo,bb.VersionNo,bb.ImportType,bb.DCFlag,bb.VoucherType,bb.Currency,bb.DepCode,bb.FinMangerCount,bb.AccountCode,bb.BClient,bb.CostCenter,bb.RiskCode,bb.MarketType,bb.Chinal,bb.PCont,bb.CashFlowNo,bb.ChargeDate,bb.ChargeTime, bb.VoucherYear,bb.PolYear,bb.Years,bb.AgentNo,bb.PremiumType,bb.FirstYear,bb.StandByString1,bb.StandByString2,bb.StandByString3,bb.StandByNum1,bb.StandByNum2,bb.StandByDate1,bb.StandByDate2,bb.ReadState,bb.MakeDate,bb.MakeTime,bb.ModifyDate,bb.ModifyTime,bb.Operator,bb.ContNo ";
            	//提交后台处理
                MMap tmap = new MMap();
                VData tInputData = new VData();
                tmap.put(insertGatherData, "INSERT");
                tmap.put("update FIVoucheManage set State = '40' where BatchNo = '"+mBatchNo+"'", "UPDATE");
                
                String update = "update FIVoucherDataDetail a set a.VSerialNo = (" +
                "select b.VSerialNo from FIVoucherDataGather b where b.batchno = a.batchno and a.VoucherType = b.VoucherType and b.AccountCode =a.AccountCode and  b.DCFlag=a.FinItemType and  b.ChargeDate=a.AccountDate and  b.importtype=a.classtype and  b.DepCode=a.ManageCom and b.Polyear = a.Polyear and b.Years  = a.Years" +
                " and nvl(b.Finmangercount,'1')=nvl(a.Executecom,'1') and nvl(b.Pcont,'1') = nvl(a.Pcont,'1') and nvl(b.PremiumType,'1') = nvl(a.Premiumtype,'1') and nvl(b.Firstyear,'1') = nvl(a.Firstyear,'1') and nvl(b.Riskcode,'1') = nvl(a.Riskcode,'1') and nvl(b.Costcenter,'1') = nvl(a.Costcenter,'1')" +
                " and nvl(b.Cashflowno,'1') = nvl(a.Cashflowno,'1') and nvl(b.Chinal,'1') = nvl(a.Salechnl,'1') and nvl(b.Agentno,'1') = nvl(a.Supplierno,'1') and nvl(b.Markettype,'1') = nvl(a.Markettype,'1') and nvl(b.Bclient,'1') = nvl(a.Bclient,'1') fetch first 1 rows only ) where a.BatchNo = '"+mBatchNo+"' ";
                update = update+" " ;
                
                // tmap.put(update, "UPDATE");
                tInputData.add(tmap);
                PubSubmit tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(tInputData, ""))
                {
                    this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                    buildError("VoucherGatherService", "gatherData", "批次号码为" + mBatchNo + "的汇总凭证数据保存在接口表时出错，提示信息为：" + tPubSubmit.mErrors.getFirstError());
                    tLogDeal.WriteLogTxt("批次号码为" + mBatchNo + "的汇总凭证数据保存在接口表时出错，提示信息为：" + tPubSubmit.mErrors.getFirstError() + enter);
                    return false;
                }
//            }

        }
        catch (Exception ex)
        {
            buildError("VoucherGatherService", "gatherData",  "批次号码为" + mBatchNo + "的汇总凭证数据处理过程出现异常,信息为：" + ex.getMessage());
            tLogDeal.WriteLogTxt("批次号码为" + mBatchNo + "的汇总凭证数据处理过程出现异常,信息为：" + ex.getMessage());
            return false;
        }
    	return true;
    }
    
    //提数表数据备份到P表
    private boolean backData()
    {
    	System.out.println("开始进行数据备份,广东卡单不备份！");
    	try
    	{
    		String insertmain="insert into FIAboriginalMainP select * from FIAboriginalMain c where exists(select 1 from FIAbStandardData a,FIVoucherDataDetail b where a.serialno =b.serialno and c.indexcode =a.indexcode and c.indexno =a.indexno and a.BusTypeID<>'O-NC-000004' and b.batchno = '"+mBatchNo+"')";
    		String deletemain="delete from FIAboriginalMain c where exists(select indexno from FIAbStandardData a,FIVoucherDataDetail b where a.serialno =b.serialno and c.indexcode =a.indexcode and c.indexno = a.indexno and a.BusTypeID<>'O-NC-000004' and b.batchno = '"+mBatchNo+"')";
    		String insertdetail="insert into FIAboriginalGenDetailP select * from FIAboriginalGenDetail c where exists(select indexno from FIAbStandardData a,FIVoucherDataDetail b where a.serialno=b.serialno and c.indexcode =a.indexcode and c.indexno=a.indexno and a.BusTypeID<>'O-NC-000004' and b.batchno = '"+mBatchNo+"')";
    		String deletedetail="delete from FIAboriginalGenDetail c where exists(select 1 from FIAbStandardData a,FIVoucherDataDetail b where a.serialno=b.serialno and c.indexcode =a.indexcode and c.indexno = a.indexno and a.BusTypeID<>'O-NC-000004' and b.batchno = '"+mBatchNo+"')";
      	   		
    		MMap mmap = new MMap();
    		mmap.put(insertmain, "INSERT");
    		mmap.put(deletemain, "DELETE");
    		mmap.put(insertdetail, "INSERT");
    		mmap.put(deletedetail, "DELETE");
    		
    		
    		VData tInputData = new VData();
    		tInputData.add(mmap);
    		
    		PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tInputData, ""))
			{
			    this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			    buildError("VoucherGatherService", "backData", "批次号码为" + mBatchNo + "的汇总凭证数据备份MAIN保存在接口表时出错，提示信息为：" + tPubSubmit.mErrors.getFirstError());
			    tLogDeal.WriteLogTxt("批次号码为" + mBatchNo + "的汇总凭证数据备份MAIN保存在接口表时出错，提示信息为：" + tPubSubmit.mErrors.getFirstError() + enter);
			    return false;
			}
    	}
    	catch(Exception ex)
    	{
    		buildError("VoucherGatherService", "backData",  "批次号码为" + mBatchNo + "的汇总凭证数据在数据备份处理过程出现异常,信息为：" + ex.getMessage());
            tLogDeal.WriteLogTxt("批次号码为" + mBatchNo + "的汇总凭证数据在数据备份处理过程出现异常,信息为：" + ex.getMessage());
            return false;
    	}
    	return true;
    }
    private String getGatherSQL()
    {
    	StringBuffer tSQL = new StringBuffer();
    	tSQL.append(" select ");
    	tSQL.append(" a.BatchNo as BatchNo ,");
    	tSQL.append(" a.VersionNo as VersionNo ,");
    	tSQL.append(" '' as VSerialNo ,");
    	tSQL.append(" a.ClassType as ImportType ,");
    	tSQL.append(" a.FinItemType as DCFlag ,");
    	tSQL.append(" a.VoucherType as VoucherType ,");
    	tSQL.append(" a.Currency as Currency ,");
    	tSQL.append(" a.ManageCom as DepCode ,");
    	tSQL.append(" a.ExecuteCom as FinMangerCount ,");
    	tSQL.append(" a.AccountCode as AccountCode ,");
    	tSQL.append(" a.BClient as BClient ,");
//    	tSQL.append(" (case when a.accountcode = '1122010000' then '' else a.CostCenter end ) as CostCenter ,");
    	tSQL.append(" (case when a.accountcode in('1122010000','1123010100') and substr(costcenter,5,2) <> '93' then '' else a.CostCenter end ) as CostCenter ,");
    	tSQL.append(" a.riskcode as RiskCode ,");
    	tSQL.append(" a.MarketType as MarketType ,");
    	tSQL.append(" a.SaleChnl as Chinal ,");
    	tSQL.append(" a.PCont as PCont ,");
    	tSQL.append(" a.CashFlowNo as CashFlowNo ,");
    	tSQL.append(" a.AccountDate as ChargeDate ,");
    	tSQL.append(" '' as ChargeTime ,");
    	tSQL.append(" sum(SumMoney) as SumMoney ,");
    	tSQL.append(" '' as VoucherYear ,");
    	tSQL.append(" a.PolYear as PolYear ,");
    	tSQL.append(" a.Years as Years ,");
    	tSQL.append(" '' as AgentNo ,");
    	tSQL.append(" a.Premiumtype as PremiumType ,");
    	tSQL.append(" a.Firstyear as FirstYear ,");
    	tSQL.append(" '' as StandByString1 ,");
    	tSQL.append(" '' as StandByString2 ,");
    	tSQL.append(" '' as StandByString3 ,");
    	tSQL.append(" 0 as StandByNum1 ,");
    	tSQL.append(" 0 as StandByNum2 ,");
    	tSQL.append(" cast(null as date) as StandByDate1 ,");
    	tSQL.append(" cast(null as date) as StandByDate2 ,");
    	tSQL.append(" '0' as ReadState ,");
    	tSQL.append(" cast('"+mCurrentDate+"' as date) as MakeDate ,");
    	tSQL.append(" '"+mCurrentTime+"' as MakeTime ,");
    	tSQL.append(" cast('"+mCurrentDate+"' as date) as ModifyDate ,");
    	tSQL.append(" '"+mCurrentTime+"' as ModifyTime ,");
    	tSQL.append(" 'cwjk' as Operator, ");
    	tSQL.append(" (case when substr(costcenter,5,2) = '93' then ContNo else '' end) as ContNo ");
    	tSQL.append(" from  FIVoucherDataDetail a where a.Batchno='"+mBatchNo+"' and accountcode not in('2202010000','2202010100','3001010000') and checkflag='00' ");
    	tSQL.append(" group by BatchNo,VersionNo, ClassType,FinItemType,VoucherType,Currency,ManageCom,ExecuteCom,AccountCode,BClient,CostCenter,riskcode,MarketType,SaleChnl,PCont,CashFlowNo,AccountDate,PolYear,Years,Premiumtype,Firstyear,ContNo ");
    	tSQL.append(" having sum(a.SumMoney) <>0 ");
    	
    	tSQL.append("union all ");
 
    	tSQL.append("select ");
    	tSQL.append(" a.BatchNo as BatchNo ,");
    	tSQL.append(" a.VersionNo as VersionNo ,");
    	tSQL.append(" '' as VSerialNo ,");
    	tSQL.append(" a.ClassType as ImportType ,");
    	tSQL.append(" a.FinItemType as DCFlag ,");
    	tSQL.append(" a.VoucherType as VoucherType ,");
    	tSQL.append(" a.Currency as Currency ,");
    	tSQL.append(" a.ManageCom as DepCode ,");
    	tSQL.append(" a.ExecuteCom as FinMangerCount ,");
    	tSQL.append(" '' as AccountCode ,");
    	tSQL.append(" a.BClient as BClient ,");
    	tSQL.append(" a.CostCenter as CostCenter ,");
    	tSQL.append(" a.riskcode as RiskCode ,");
    	tSQL.append(" a.MarketType as MarketType ,");
    	tSQL.append(" a.SaleChnl as Chinal ,");
    	tSQL.append(" a.PCont as PCont ,");
    	tSQL.append(" a.CashFlowNo as CashFlowNo ,");
    	tSQL.append(" a.AccountDate as ChargeDate ,");
    	tSQL.append(" '' as ChargeTime ,");
    	tSQL.append(" sum(SumMoney) as SumMoney ,");
    	tSQL.append(" '' as VoucherYear ,");
    	tSQL.append(" a.PolYear as PolYear ,");
    	tSQL.append(" a.Years as Years ,");
    	tSQL.append(" a.SupplierNo as AgentNo ,");
    	tSQL.append(" a.Premiumtype as PremiumType ,");
    	tSQL.append(" a.Firstyear as FirstYear ,");
    	tSQL.append(" '' as StandByString1 ,");
    	tSQL.append(" '' as StandByString2 ,");
    	tSQL.append(" '' as StandByString3 ,");
    	tSQL.append(" 0 as StandByNum1 ,");
    	tSQL.append(" 0 as StandByNum2 ,");
    	tSQL.append(" cast(null as date) as StandByDate1 ,");
    	tSQL.append(" cast(null as date) as StandByDate2 ,");
    	tSQL.append(" '0' as ReadState ,");
    	tSQL.append(" cast('"+mCurrentDate+"' as date) as MakeDate ,");
    	tSQL.append(" '"+mCurrentTime+"' as MakeTime ,");
    	tSQL.append(" cast('"+mCurrentDate+"' as date) as ModifyDate ,");
    	tSQL.append(" '"+mCurrentTime+"' as ModifyTime ,");
    	tSQL.append(" 'cwjk' as Operator ,");
    	tSQL.append(" (case when substr(costcenter,5,2) = '93' then ContNo else '' end)  as ContNo ");
    	tSQL.append(" from  FIVoucherDataDetail a where a.Batchno='"+mBatchNo+"' and accountcode in('2202010000','2202010100','3001010000') and checkflag='00' ");
    	tSQL.append(" group by BatchNo,VersionNo, ClassType,FinItemType,VoucherType,Currency,ManageCom,ExecuteCom,AccountCode,BClient,CostCenter,riskcode,MarketType,SaleChnl,PCont,CashFlowNo,AccountDate,PolYear,Years,SupplierNo,Premiumtype,Firstyear,ContNo ");
    	tSQL.append(" having sum(a.SumMoney) <>0 ");
    	return tSQL.toString();
    }
    
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
