package com.sinosoft.lis.fininterface_v3.core.gather;

import com.sinosoft.lis.db.InterfaceTableDB;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.InterfaceTableSet;
import com.sinosoft.lis.vschema.LIAboriginalDataSet;
import com.sinosoft.lis.vschema.LIDataTransResultSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.VData;
/**
 * <p>Title: 凭证导出</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class MoveDataService {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors=new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData= new VData();
	/** 全局数据 */
	private GlobalInput mGlobalInput =new GlobalInput() ;
	
	private PubSubmit mPubSubmit = new PubSubmit();
	/** 数据操作字符串 */
	private String mOperate;
    private final String enter = "\r\n"; // 换行符
    private String mBatchNo;
    
	/** 日志处理*/
	private FIOperationLog tLogDeal = null;
	
	public MoveDataService(FIOperationLog logDeal){
		tLogDeal = logDeal;
	}
	
	/**
	 * 凭证导出处理
	 * @param cInputData
	 * @param cOperate
	 * @return
	 */
	public boolean submitData(VData cInputData,String cOperate)
	{
		//将操作数据拷贝到本类中
		this.mInputData = cInputData;
		this.mOperate =cOperate;
		
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData())
		{
			return false;
		}
		if (!checkData())
		{
			return false;
		}
		//向旧平台表中同步数据  ,暂时注掉，同步稳定后放开同步
//		if(!synchronousData())
//		{
//			return false;
//		}
		//进行业务处理
		if (!moveDataToInterfaceTable())
		{
			return false;
		}
		
		
		tLogDeal.Complete(true);
		
		return true;
	}
	
	/**
	 * 获取前台数据
	 * @return
	 */
	private boolean getInputData()
	{
		mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
		mBatchNo = (String) mInputData.get(1);
		
		return true;
	}
	/**
	 * 数据校检
	 * @return
	 */
	private boolean checkData()
	{

		String tSQL = "select code from Ficodetrans a where codetype = 'TransFlag' with ur ";
    	String transflag = new ExeSQL().getOneValue(tSQL);
    	if(!"1".equals(transflag))
    	{
    		System.out.println("不进行同步,transflag:"+transflag);
    		buildError("MoveDataService","checkData","同步标记未开启！");
    		return false;
    	}
    	
    	if(mBatchNo==null||"".equals(mBatchNo))
    	{
    		buildError("MoveDataService","checkData","批次号获取失败！");
    		return false;
    	}
    	if(mGlobalInput==null)
    	{
    		buildError("MoveDataService","checkData","全局对象获取失败！");
    		return false;
    	}
    	
//		try 
//		{
//			tLogDeal = new FIOperationLog(mGlobalInput.Operator,"60");
//			
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return false;
//		}
		
		return true;
	}
	
	/**
	 * 凭证导出
	 * @return
	 */
	private boolean moveDataToInterfaceTable()
	{
		String tSQL = getDillInterfaceData();
		InterfaceTableSet tInterfaceTableSet = new InterfaceTableDB().executeQuery(tSQL);
		
        if(tInterfaceTableSet == null || tInterfaceTableSet.size()< 0)
        {
        	return false;
        }
        String insertInterfaceTable="insert into interfacetable "+tSQL;
		MMap tMap= new MMap();
		VData tInputData = new VData();	

		tMap.put(insertInterfaceTable, "INSERT");
		tMap.put("update FIVoucheManage set state='50' where batchno='"+mBatchNo+"'", "UPDATE");
		tMap.put("update InterfaceTable set readstate = '3' where batchno = '"+mBatchNo+"' and chargedate = current date ", "UPDATE");
		tMap.put("update InterfaceTable set readstate = NULL where chargedate = current date -1 days and readstate = '3' and readdate is null  ", "UPDATE");
		
		tInputData.add(tMap);
        if (!mPubSubmit.submitData(tInputData, ""))
        {
            this.mErrors.copyAllErrors(mPubSubmit.mErrors);
            buildError("MoveDataService","moveDataToInterfaceTable", "凭证导出，保存数据时出错，批次号："+mBatchNo+"，提示信息为：" +mPubSubmit.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("凭证导出，保存数据时出错，批次号："+mBatchNo+"，提示信息为：" +mPubSubmit.mErrors.getFirstError() + enter);
            tLogDeal.buildErrorLog("凭证导出，保存数据时出错，批次号："+mBatchNo );
            tMap=null;
            tInputData=null;
            return false;
        }
        tMap=null;
        tInputData=null;
        
        
		return true;
	}
	
	/**
	 * 接口表导入数据SQL
	 * @return
	 */
	private String getDillInterfaceData()
	{
		String tSQL = "select a.Vserialno serialno,a.batchno,a.importtype,a.dcflag,a.vouchertype,a.currency,a.depcode," +
				"a.finmangercount,a.accountcode,a.bclient,a.costcenter,a.riskcode,a.markettype," +
				"a.chinal,a.pcont,a.cashflowno,a.chargedate,a.chargetime,a.summoney," +
				"cast(null as char) voucherid,cast(null as char) readstate,cast(null as char) voucheryear," +
				"cast(null as date) backvdate,cast(null as char) backvtime,cast(null as date) Readdate,cast(null as char) Readtime,"+
				"'"+PubFun.getCurrentDate().toString()+"' makedate," +
				"'"+PubFun.getCurrentTime().toString()+"' maketime," +
				"'"+PubFun.getCurrentDate().toString()+"' modifydate,'"+PubFun.getCurrentTime().toString()+"' modifytime," +
				"'"+mGlobalInput.Operator+"' operator,polyear,years,agentno,premiumtype,firstyear,ContNo from FIVoucherDataGather a " +
				" where a.batchno='"+mBatchNo+"' and a.summoney <> 0" +
				" and exists (select 1 from FIVoucheManage b where a.batchno=b.batchno and b.state='40') with ur ";
		
		return tSQL;
	}
	
    /**
     * 向旧平台表（LIAboriginalData\LIDataTransResult）中同步数据
     * @return
     */
    private boolean synchronousData()
    {
    	//(1) 向旧平台表（LIAboriginalData）中同步数据
		String tSQL = getAboriginalDataSQL(mBatchNo);
		LIAboriginalDataSet tLIAboriginalDataSet = new LIAboriginalDataSet();
		RSWrapper  rsWrapper  = new RSWrapper();
        if (!rsWrapper.prepareData(tLIAboriginalDataSet, tSQL))
        {
        	System.out.println(rsWrapper.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("向旧平台表（LIAboriginalData）中同步数据时出错，批次号："+mBatchNo+"，错误信息:" +rsWrapper.mErrors.getFirstError() + enter);
            tLogDeal.buildErrorLog("向旧平台表（LIAboriginalData）中同步数据时出错，批次号："+mBatchNo);
            return false;
        }
        do
        {
           	rsWrapper.getData();
            if (tLIAboriginalDataSet != null && tLIAboriginalDataSet.size() > 0)
            {           	
            	//数据处理
            	dealAboriginalData(tLIAboriginalDataSet);
            }      
            
        }while(tLIAboriginalDataSet != null && tLIAboriginalDataSet.size() > 0);

        rsWrapper.close();  	
    	
    	
    	//(2)向旧平台表（LIDataTransResult）中同步数据
        
		String tSQL2 = getLIDataTransResultSQL(mBatchNo);
		LIDataTransResultSet tLIDataTransResultSet = new LIDataTransResultSet();
		RSWrapper  rsWrapper2  = new RSWrapper();
        if (!rsWrapper2.prepareData(tLIDataTransResultSet, tSQL2))
        {
        	System.out.println(rsWrapper2.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("向旧平台表（LIDataTransResult）中同步数据时出错，批次号："+mBatchNo+"，错误信息:" +rsWrapper2.mErrors.getFirstError() + enter);
            tLogDeal.buildErrorLog("向旧平台表（LIDataTransResult）中同步数据时出错，批次号："+mBatchNo );
            return false;
        }
        do
        {
        	rsWrapper2.getData();
            if (tLIDataTransResultSet != null && tLIDataTransResultSet.size() > 0)
            {           	
            	//数据处理
            	dealLIDataTransResult(tLIDataTransResultSet);
            }      
            
        }while(tLIDataTransResultSet != null && tLIDataTransResultSet.size() > 0);

        rsWrapper2.close();          
        
    	return true;
    }	
	
    /**
     * 向LIAboriginalData中同步数据SQL
     * @param tBatchNo
     * @return
     */
    private String getAboriginalDataSQL(String tBatchNo)
    {
    	StringBuffer tSQL  = new StringBuffer();
    	tSQL.append("select ");
    	tSQL.append("a.SerialNo as SERIALNO, ") ; //key
    	tSQL.append("'"+tBatchNo+"' as BATCHNO, ") ;  //NOT NULL
    	tSQL.append("a.BusTypeID as CLASSID, ") ;  //NOT NULL
    	tSQL.append("(case when IndexCode='01' then IndexNo else '' end) as TEMPFEENO, ") ;
    	tSQL.append("(case when IndexCode in('04','05') then IndexNo else '' end) as ACTUGETNO, ") ;
    	tSQL.append("(case when IndexCode='03' then IndexNo else '' end) as PAYNO, ") ;
    	tSQL.append("'' as EDORACCEPTNO, ") ;
    	tSQL.append("a.EndorsementNo as ENDORSEMENTNO, ") ;
    	tSQL.append("'' as CASENO, ") ;
    	tSQL.append("a.FeeType as FEEOPERATIONTYPE, ") ;
    	tSQL.append("a.FeeDetail as FEEFINATYPE, ") ;
    	tSQL.append("a.ListFlag as LISTFLAG, ") ;//?
    	tSQL.append("a.GrpContNo as GRPCONTNO, ") ;
    	tSQL.append("a.GrpPolNo as GRPPOLNO, ") ;
    	tSQL.append("a.ContNo as CONTNO, ") ;
    	tSQL.append("a.PolNo as POLNO, ") ;
    	tSQL.append("'' as PAYINTVFLAG, ") ;
    	tSQL.append("a.PayIntv as PAYINTV, ") ;
    	tSQL.append("a.AccountDate as PAYDATE, ") ;
    	tSQL.append("'' as PAYTYPE, ") ;  //?
    	tSQL.append("a.AccountDate as ENTERACCDATE, ") ;
    	tSQL.append("a.AccountDate as CONFDATE, ") ;
    	tSQL.append("a.LastPayToDate as LASTPAYTODATE, ") ;
    	tSQL.append("a.CurPayToDate as CURPAYTODATE, ") ;
    	tSQL.append("a.CValiDate as CVALIDATE, ") ;
    	tSQL.append("a.PolYear as POLYEAR, ") ;
    	tSQL.append("a.FirstYearFlag as FIRSTYEARFLAG, ") ;
    	tSQL.append("a.PayCount as PAYCOUNT, ") ;
    	tSQL.append("a.FirstTermFlag as FIRSTTERMFLAG, ") ;
    	tSQL.append("a.Years as YEARS, ") ;
    	tSQL.append("a.RiskCode as RISKCODE, ") ;
    	tSQL.append("a.RiskPeriod as RISKPERIOD, ") ;
    	tSQL.append("a.RiskType as RISKTYPE, ") ;
    	tSQL.append("a.RiskType1 as RISKTYPE1, ") ;
    	tSQL.append("a.SaleChnl as SALECHNL, ") ;
    	tSQL.append("a.ManageCom as MANAGECOM, ") ;
    	tSQL.append("a.ExecuteCom as EXECUTECOM, ") ;
    	tSQL.append("a.AgentCom as AGENTCOM, ") ;
    	tSQL.append("a.AgentCode as AGENTCODE, ") ;
    	tSQL.append("a.AgentGroup as AGENTGROUP, ") ;
    	tSQL.append("a.BankCode as BANKCODE, ") ;
    	tSQL.append("a.AccName as ACCNAME, ") ;  //?
    	tSQL.append("a.BankAccNo as BANKACCNO, ") ;
    	tSQL.append("a.SumActuMoney as SUMDUEMONEY, ") ;
    	tSQL.append("a.SumActuMoney as SUMACTUMONEY, ") ;
    	tSQL.append("'R' as CURRENCY, ") ;
    	tSQL.append("a.PayMode as PAYMODE, ") ;
    	tSQL.append("a.OperationType as OPERATIONTYPE, ") ;
    	tSQL.append("a.CashFlowNo as BUDGET, ") ;
    	tSQL.append("a.SaleChnlDetail as SALECHNLDETAIL, ") ;
    	tSQL.append("'' as STANDBYSTRING1, ") ;
    	tSQL.append("'' as STANDBYSTRING2, ") ;
    	tSQL.append("'' as STANDBYSTRING3, ") ;
    	tSQL.append("cast(null as int) as STANDBYNUM1, ") ;
    	tSQL.append("cast(null as int) as STANDBYNUM2, ") ;
    	tSQL.append("cast(null as date) as STANDBYDATE1, ") ;
    	tSQL.append("cast(null as date) as STANDBYDATE2, ") ;
    	tSQL.append("a.CustomerID as BCLIENT, ") ;
    	tSQL.append("a.MarketType as MARKETTYPE, ") ;
    	tSQL.append("a.PremiumType as PREMIUMTYPE, ") ;
    	tSQL.append("a.FirstYear as FIRSTYEAR ") ;
    	tSQL.append(" from FIAbStandardData a where a.CheckFlag = '00' and a.ReadState='0' ");
    	tSQL.append(" and exists(select 1 from FIVoucherDataDetail b where a.SerialNo = b.SerialNo and b.CheckFlag = '00' and b.batchno = '"+tBatchNo+"')");
    	tSQL.append(" and not exists(select 1 from LIAboriginalData c where  c.batchno = '"+tBatchNo+"') with ur ");
    	
    	return tSQL.toString() ;
    }
    
    /**
     * 向LIAboriginalData中同步数据
     * @param tLIAboriginalDataSet
     * @return
     */
    private boolean dealAboriginalData(LIAboriginalDataSet tLIAboriginalDataSet)
    {
		MMap tMap= new MMap();
		VData tInputData = new VData();
		String updateSQL = "update FIAbStandardData a set a.readstate = '1' where "+
			" a.readstate = '0' and exists( select 1 from LIAboriginalData b where a.SerialNo = b.SerialNo  and  b.batchno='"+mBatchNo+"')";
		tMap.put(tLIAboriginalDataSet, "INSERT");
		tMap.put(updateSQL, "UPDATE");
		tInputData.add(tMap);
        if (!mPubSubmit.submitData(tInputData, ""))
        {
            this.mErrors.copyAllErrors(mPubSubmit.mErrors);
            buildError("MoveDataService","dealAboriginalData", "向LIAboriginalData中同步数据，保存数据时出错，批次号："+mBatchNo+"，提示信息为：" +mPubSubmit.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("向LIAboriginalData中同步数据，保存数据时出错，批次号："+mBatchNo+"，提示信息为：" +mPubSubmit.mErrors.getFirstError() + enter);
            tLogDeal.buildErrorLog("向LIAboriginalData中同步数据，保存数据时出错，批次号："+mBatchNo );
            tMap=null;
            tInputData=null;
            return false;
        }
        tMap=null;
        tInputData=null;
    	
    	return true;
    }
      
    /**
     * 向LIDataTransResult中同步数据SQL
     * @param tBatchNo
     * @return
     */
    private String getLIDataTransResultSQL(String tBatchNo)
    {
    	StringBuffer tSQL  = new StringBuffer();
    	tSQL.append("select ");
    	tSQL.append("a.FSerialNo as SERIALNO,") ;      //key
    	tSQL.append("a.BatchNo as BATCHNO ,") ;      //NOT NULL
    	tSQL.append("(case when a.ClassType='N-05' and a.ListFlag='1' then 'N-03' else a.ClassType end) as CLASSTYPE , ") ; //再保提数用
    	tSQL.append("(select BusTypeID from FIAbStandardData e where e.serialno=a.serialno fetch first 1 rows only) as CLASSID , ") ;      //NOT NULL
    	tSQL.append("(case when exists (select 1 from FIAboriginalGenDetail d "+
    				"where exists (select 1 from FIAbStandardData b where b.aserialno=d.aserialno and b.notype='2' "+
    				"and a.serialno=b.serialno "+
    				") fetch first 1 rows only) then (select d.keyunionvalue from FIAboriginalGenDetail d "+
    				"where exists (select 1 from FIAbStandardData b where b.aserialno=d.aserialno and b.notype='2' "+
    				"and a.serialno=b.serialno "+
    				") fetch first 1 rows only) else (select d.keyunionvalue from FIAboriginalMain d "+
    				"where exists (select 1 from FIAbStandardData b where b.aserialno=d.mserialno and b.notype='1' "+
    				"and a.serialno=b.serialno "+
    				") fetch first 1 rows only) end) as KEYUNIONVALUE , ") ;//NOT NULL ?
    	tSQL.append("a.AccountCode as ACCOUNTCODE , ") ;  //NOT NULL
    	tSQL.append("a.FinItemType as FINITEMTYPE , ") ;  //NOT NULL
    	tSQL.append("a.SumMoney as SUMMONEY, ") ;      //NOT NULL
    	tSQL.append("a.AccountDate as ACCOUNTDATE , ") ;  //NOT NULL
    	tSQL.append("a.SaleChnl as SALECHNL, ") ;
    	tSQL.append("a.ManageCom as MANAGECOM , ") ;    //NOT NULL
    	tSQL.append("a.BankAccNo as BANKACCNO , ") ;
    	tSQL.append("a.BankCode as BANKCODE, ") ;
    	tSQL.append("a.RiskCode as RISKCODE, ") ;
    	tSQL.append("a.AgentCom as AGENTCOM, ") ;
    	tSQL.append("a.AgentCode as AGENTCODE , ") ;
    	tSQL.append("a.AgentGroup as AGENTGROUP, ") ;
    	tSQL.append("a.ListFlag as LISTFLAG, ") ;      //NOT NULL
    	tSQL.append("a.ContNo as CONTNO, ") ;        //NOT NULL
    	tSQL.append("a.CostCenter as COSTCENTER, ") ;
    	tSQL.append("'' as FEETYPE , ") ;  //?
    	tSQL.append("'R' as CURRENCY, ") ;
    	tSQL.append("a.CashFlowNo as BUDGET, ") ;
    	tSQL.append("a.ExecuteCom as EXECUTECOM, ") ;
    	tSQL.append("a.SupplierNo as STANDBYSTRING1, ") ;
    	tSQL.append("'' as STANDBYSTRING2, ") ;
    	tSQL.append("a.SerialNo as STANDBYSTRING3, ") ;
    	tSQL.append("cast(null as int) as STANDBYNUM1 , ") ;
    	tSQL.append("cast(null as int) as STANDBYNUM2 , ") ;
    	tSQL.append("cast(null as date) as STANDBYDATE1, ") ;
    	tSQL.append("cast(null as date) as STANDBYDATE2, ") ;
    	tSQL.append("a.BClient as BCLIENT , ") ;
    	tSQL.append("a.MarketType as MARKETTYPE, ") ;
    	tSQL.append("a.Pcont as PCONT , ") ;
    	tSQL.append("a.PolYear as POLYEAR , ") ;
    	tSQL.append("a.Years as YEARS , ") ;
    	tSQL.append("a.PremiumType as PREMIUMTYPE , ") ;
    	tSQL.append("a.FirstYear as FIRSTYEAR  ") ;
    	tSQL.append(" from FIVoucherDataDetail a where a.CheckFlag = '00' and a.ReadState='0' and a.batchno = '"+tBatchNo+"' and a.SumMoney <> 0");   	
    	tSQL.append(" and not exists (select 1 from lidatatransresult b where  b.batchno = '"+tBatchNo+"') with ur ");   	
    	
//    	System.out.println("daoSQL:"+ tSQL);
    	
    	return tSQL.toString() ;
    }   
    
    /**
     * 向LIDataTransResult中同步数据
     * @param tLIDataTransResultSet
     * @return
     */
    private boolean dealLIDataTransResult(LIDataTransResultSet tLIDataTransResultSet)
    {
		MMap tMap= new MMap();
		VData tInputData = new VData();
		String updateSQL = "update FIVoucherDataDetail a set a.readstate = '1' where a.batchno = '"+mBatchNo+
		"' and a.readstate = '0' and exists(select 1 from LIDataTransResult b where a.FSerialNo = b.SerialNo and a.batchno='"+mBatchNo+"')";
		
		tMap.put(tLIDataTransResultSet, "INSERT");
		tMap.put(updateSQL, "UPDATE");
		tInputData.add(tMap);
        if (!mPubSubmit.submitData(tInputData, ""))
        {
            this.mErrors.copyAllErrors(mPubSubmit.mErrors);
            buildError("MoveDataService","dealLIDataTransResult", "向LIDataTransResult中同步数据，保存数据时出错，批次号："+mBatchNo+"，提示信息为：" +mPubSubmit.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("向LIDataTransResult中同步数据，保存数据时出错，批次号："+mBatchNo+"，提示信息为：" +mPubSubmit.mErrors.getFirstError() + enter);
            tLogDeal.buildErrorLog("向LIDataTransResult中同步数据，保存数据时出错，批次号："+mBatchNo );
            tMap=null;
            tInputData=null;
            return false;
        }
        tMap=null;
        tInputData=null;
        
    	return true;
    }
    
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
