package com.sinosoft.lis.fininterface_v3.core.trans.special;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
/**
 * <p>Title:转换专项处理类接口 </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public interface FIAssociatedItem {

	   /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public boolean DealInfo(VData cInputData);
    public boolean CheckInfo(VData cInputData);
    public String getClumnValue();
}
