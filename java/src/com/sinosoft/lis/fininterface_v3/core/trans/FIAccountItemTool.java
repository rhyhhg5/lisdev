package com.sinosoft.lis.fininterface_v3.core.trans;

import com.sinosoft.lis.db.FIAssociatedDirectItemDefDB;
import com.sinosoft.lis.db.FIAssociatedItemDefDB;
import com.sinosoft.lis.db.FIDetailFinItemCodeDB;
import com.sinosoft.lis.db.FIDetailFinItemDefDB;
import com.sinosoft.lis.db.FIFinItemDefDB;
import com.sinosoft.lis.fininterface_v3.core.trans.special.FIFinItemClass;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.schema.FIAssociatedDirectItemDefSchema;
import com.sinosoft.lis.schema.FIAssociatedItemDefSchema;
import com.sinosoft.lis.schema.FIDetailFinItemCodeSchema;
import com.sinosoft.lis.schema.FIDetailFinItemDefSchema;
import com.sinosoft.lis.schema.FIFinItemDefSchema;
import com.sinosoft.lis.schema.FIVoucherBnFeeSchema;
import com.sinosoft.lis.schema.FIVoucherDataDetailSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.lis.vschema.FIAssociatedDirectItemDefSet;
import com.sinosoft.lis.vschema.FIAssociatedItemDefSet;
import com.sinosoft.lis.vschema.FIDetailFinItemCodeSet;
import com.sinosoft.lis.vschema.FIDetailFinItemDefSet;
import com.sinosoft.lis.vschema.FIFinItemDefSet;
import com.sinosoft.lis.vschema.FIVoucherDataDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
/**
 * <p>Title:科目信息转换工具类 </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class FIAccountItemTool {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 科目类型定义 */
    private FIFinItemDefSchema mFIFinItemDefSchema;

    private FIDetailFinItemDefSet mFIDetailFinItemDefSet;
    /** 明细科目类型判断条件 */
    private String ItemCondition[][];
    /** 科目关联核算项*/
    private FIAssociatedItemDefSet mFIAssociatedItemDefSet;


    /** 明细科目类型判断条件数值组合对照表 */
    private FIDetailFinItemCodeSet mFIDetailFinItemCodeSet;
    /** 明细科目类型判断条件数值组合对照表索引*/
    private FIDetailFinItemCodeSet[] mFIDetailFinItemCodeSetUint;

    /** 科目固定专项定义表*/
    private FIAssociatedDirectItemDefSet mFIAssociatedDirectItemDefSet;
    /** 科目特殊处理类*/
    private FIFinItemClass mFIFinItemClass;

    private FIVoucherBnFeeSchema mFIVoucherBnFeeSchema;
    private String[] mFserialno = null;
    private int Fno=0;
    private String VersionNo = "";
    private String BatchNo ;
    private String mFinItemType = "";
    private String mFinItemID ;
    private String ErrInfo = "";
    private String ErrType = "";
    private final String enter = "\r\n"; // 换行符

    public FIOperationLog tLogDeal = null;

    public FIAccountItemTool(FIOperationLog logDeal)
    {
    	tLogDeal = logDeal;
    }

    /**
     * 凭证数据转换处理
     * @param tFIAbStandardDataSet
     * @param tFIVoucherBnFeeSchema
     * @param tOperType  借贷：D,C
     * @return
     */
    public boolean dealService(FIAbStandardDataSet tFIAbStandardDataSet,FIVoucherBnFeeSchema tFIVoucherBnFeeSchema ,String tBatchNo, String tFinItemType)
    {  	
    	try
        {
    		
    		BatchNo = tBatchNo;
    		mFIVoucherBnFeeSchema = tFIVoucherBnFeeSchema; 
	    	mFinItemType = tFinItemType;
	    	
	    	VersionNo = tFIVoucherBnFeeSchema.getVersionNo();

	    	if("D".equals(mFinItemType))
	    	{
	    		mFinItemID = tFIVoucherBnFeeSchema.getD_FinItemID();
	    	}
	    	else if("C".equals(mFinItemType))
	    	{
	    		mFinItemID = tFIVoucherBnFeeSchema.getC_FinItemID();    
	    	}
	    	else
	    	{
	    		return false;
	    	}
	    	
	    	//数据处理
	    	FIVoucherDataDetailSet tFIVoucherDataDetailSet = new FIVoucherDataDetailSet();
	    	if(mFinItemID!=null&&!"".equals(mFinItemID))
	    	{
	    		if(!initItemInfo(mFinItemID))
	    		{
		            buildError("FIAccountItemTool","ruleEXE", "初始化科目错误,业务交易编码:" + tFIVoucherBnFeeSchema.getBusinessID() +"费用编码:" + tFIVoucherBnFeeSchema.getFeeID()+"科目编码:" + mFinItemID );
		            tLogDeal.WriteLogTxt("初始化科目错误,业务交易编码:" + tFIVoucherBnFeeSchema.getBusinessID() +"费用编码:" + tFIVoucherBnFeeSchema.getFeeID()+"科目编码:" + mFinItemID + enter);
		            return false;	    			
	    		}
	    		else
	    		{
	    			mFserialno=FinCreateSerialNo.getSerialNoByFIFSerialNo(tFIAbStandardDataSet.size());
	    			
	    			for (int j = 1; j <=tFIAbStandardDataSet.size(); j++)
	    	        {           
	    				Fno = j-1;
	    				FIAbStandardDataSchema tFIAbStandardDataSchema = tFIAbStandardDataSet.get(j);
	    	            
	    	            if(mFIFinItemDefSchema!=null)
	    	            {
	    	            	FIVoucherDataDetailSchema tmpFIVoucherDataDetailSchema = transData(mFinItemType,tFIAbStandardDataSchema);
	    		            if (tmpFIVoucherDataDetailSchema == null)
	    		            {
	    		                buildError("FIAccountItemTool","ruleEXE", "业务交易编码:" + tFIVoucherBnFeeSchema.getBusinessID() +"费用编码:" + tFIVoucherBnFeeSchema.getFeeID()+", 业务号码为" + tFIAbStandardDataSchema.getIndexNo() + "数据生成会计分录数据错误");
	    		                tLogDeal.WriteLogTxt("业务交易编码:" + tFIVoucherBnFeeSchema.getBusinessID()+"费用编码:" + tFIVoucherBnFeeSchema.getFeeID() +  ", 业务号码为" + tFIAbStandardDataSchema.getIndexNo() + "数据生成会计分录数据错误" + enter);
	    		                
	    		                //DealError(mFIAccountItemType.ErrInfo,mFIAccountItemType.ErrType,tFIAbStandardDataSchema.getASerialNo(), tFIAbStandardDataSchema.getAccountDate());	    		                
	    		                continue;
	    		            }

	    		            tFIVoucherDataDetailSet.add(tmpFIVoucherDataDetailSchema);
	    	            }   
	    	        }
	    		}
	    	}
	    	
	        MMap map = new MMap();
	        map.put(tFIVoucherDataDetailSet, "INSERT");
	        
	        VData tInputInfo = new VData();
	        tInputInfo.add(map);
	        PubSubmit tPubSubmit = new PubSubmit();
	        if (!tPubSubmit.submitData(tInputInfo, ""))
	        {
	            buildError("FIAccountItemTool","ruleEXE", "业务交易编码:" + tFIVoucherBnFeeSchema.getBusinessID() +"费用编码:" + tFIVoucherBnFeeSchema.getFeeID()+"提交数据库凭证数据失败,错误是：" + tPubSubmit.mErrors.getFirstError() );
	            tLogDeal.WriteLogTxt("业务交易编码:" + tFIVoucherBnFeeSchema.getBusinessID() +"费用编码:" + tFIVoucherBnFeeSchema.getFeeID()+"提交数据库凭证数据失败,错误是：" + tPubSubmit.mErrors.getFirstError() + enter);
	            return false;
	        }
	        map = null;
	        tFIVoucherDataDetailSet = null;
	        tInputInfo = null;
        } 
    	catch (Exception ex)
        {
            buildError("FIAccountItemTool","ruleEXE","业务交易编码:" +tFIVoucherBnFeeSchema.getBusinessID() +"费用编码:" + tFIVoucherBnFeeSchema.getFeeID()+"生成凭证数据出现异常,错误是：" + ex.getMessage());
            tLogDeal.WriteLogTxt("业务交易编码:" + tFIVoucherBnFeeSchema.getBusinessID() +"费用编码:" +tFIVoucherBnFeeSchema.getFeeID()+"生成凭证数据出现异常,错误是：" +  ex.getMessage() + enter);
            return false;
        }
    	return true;
    }
    
    private boolean initItemInfo(String tFinItemID)
    {

        FIFinItemDefDB tFIFinItemDefDB = new FIFinItemDefDB();
    	String tSQL = "select * from FIFinItemDef where VersionNo = '"+VersionNo+"' and FinItemID = '"+tFinItemID+"' with ur ";
    	System.out.println("初始化科目FIFinItemDef:"+tSQL);
    	FIFinItemDefSet tDFIFinItemDefSet  = tFIFinItemDefDB.executeQuery(tSQL);
    	if(tDFIFinItemDefSet.size()>0)
    	{
    		mFIFinItemDefSchema = tDFIFinItemDefSet.get(1);

            if(mFIFinItemDefSchema.getDealMode().equals("1"))
            {
                FIDetailFinItemDefDB tFIDetailFinItemDefDB = new FIDetailFinItemDefDB();
                String tSql = "select * from FIDetailFinItemDef where FinItemID ='" + tFinItemID + "' and versionno = '" + VersionNo + "' order by JudgementNo with ur ";
                mFIDetailFinItemDefSet = tFIDetailFinItemDefDB.executeQuery(tSql);
                if (tFIDetailFinItemDefDB.mErrors.needDealError()) {
                    buildError("FIAccountItemTool", "initItemInfo","编号为" + tFinItemID + "版本编号为" + VersionNo + "的科目明细判断条件定义查询出错");
                    tLogDeal.WriteLogTxt("编号为" + tFinItemID + "版本编号为" + VersionNo +"的科目判断条件定义查询出错" + enter);
                    return false;
                }

                tSql = "select * from FIDetailFinItemCode where FinItemID ='" + tFinItemID + "' and versionno = '" + VersionNo + "' order by JudgementNo with ur ";
                FIDetailFinItemCodeDB tFIDetailFinItemCodeDB = new FIDetailFinItemCodeDB();
                mFIDetailFinItemCodeSet = tFIDetailFinItemCodeDB.executeQuery(tSql);
                if (tFIDetailFinItemCodeDB.mErrors.needDealError()) 
                {
                    buildError("FIAccountItemTool", "initItemInfo", "编号为" + tFinItemID + "版本编号为" + VersionNo + "的科目明细映射定义查询出错");
                    tLogDeal.WriteLogTxt("编号为" + tFinItemID + "版本编号为" + VersionNo + "的科目明细映射定义查询出错" + enter);
                    return false;
                }

                tSql = "select * from FIAssociatedItemDef a where a.VersionNo = '" +
                       VersionNo + "' and a.AssociatedID in (select AssociatedID from FIInfoFinItemAssociated b where b.VersionNo = '" +
                       VersionNo + "' and b.FinItemID = '" + tFinItemID + "') with ur ";
                FIAssociatedItemDefDB tFIAssociatedItemDefDB = new FIAssociatedItemDefDB();
                mFIAssociatedItemDefSet = tFIAssociatedItemDefDB.executeQuery(tSql);
                if (tFIAssociatedItemDefDB.mErrors.needDealError()) 
                {
                    buildError("FIAccountItemTool", "initItemInfo", "编号为" + tFinItemID + "版本编号为" + VersionNo + "的科目关联专项定义查询出错");
                    tLogDeal.WriteLogTxt("编号为" + tFinItemID + "版本编号为" + VersionNo + "的科目关联专项定义查询出错" + enter);
                    return false;
                }

                FIAssociatedDirectItemDefDB tFIAssociatedDirectItemDefDB = new
                        FIAssociatedDirectItemDefDB();
                tFIAssociatedDirectItemDefDB.setVersionNo(VersionNo);
                mFIAssociatedDirectItemDefSet = tFIAssociatedDirectItemDefDB.query();
                if (tFIAssociatedDirectItemDefDB.mErrors.needDealError()) 
                {
                    buildError("FIAccountItemTool", "initItemInfo", "编号为" + tFinItemID + "版本编号为" + VersionNo + "的科目固定数据项定义查询出错");
                    tLogDeal.WriteLogTxt("编号为" + tFinItemID + "版本编号为" + VersionNo +"的科目固定数据项定义查询出错" + enter);
                    return false;
                }

                //初始化科目条件信息
                if (!InitItemCondition()) {
                    return false;
                }
                //初始化专项关联信息
                if (!InitInfoAssociated()) {
                    return false;
                }
                //初始化固定关联信息
                if (!InitInfoDirect()) {
                    return false;
                }
            }
            else
            {
            	try
            	{
	            	Class tClass = Class.forName(mFIFinItemDefSchema.getDealSpecialClass());
	            	mFIFinItemClass = (FIFinItemClass) tClass.newInstance();
	                if(!mFIFinItemClass.InitInfo(mFIVoucherBnFeeSchema,mFinItemType,tLogDeal))
	                {
	                    this.mErrors.copyAllErrors(this.mFIFinItemClass.mErrors);
	                    return false;
	                }
            	}
                catch (Exception ex)
                {
                    buildError("FIAccountItemTool","initItemInfo", "编号为" + mFinItemID +  "版本编号为" + mFIVoucherBnFeeSchema.getVersionNo() + "的科目处理类初始化异常，信息为：" + ex.getMessage());
                    tLogDeal.WriteLogTxt("编号为" + mFinItemID +  "版本编号为" + mFIVoucherBnFeeSchema.getVersionNo() + "的科目处理类初始化异常，信息为：" + ex.getMessage() + enter);
                    return false;
                }
            }     		
    	}

    	return true;
    }  

    /**
     * 初始化科目条件信息
     * @return
     */
    private boolean InitItemCondition()
    {
    	FIAbStandardDataSchema tFIAbStandardDataSchema = new  FIAbStandardDataSchema();
        if(mFIDetailFinItemDefSet.size()>0)
        {
            ItemCondition = new  String[mFIDetailFinItemDefSet.size()][];
            for (int i = 0; i < mFIDetailFinItemDefSet.size(); i++)
            {
                FIDetailFinItemDefSchema tFIDetailFinItemDefSchema = mFIDetailFinItemDefSet.get(i+1);
                String tLevelCondition[] = tFIDetailFinItemDefSchema.getLevelCondition().trim().split(",");
                ItemCondition[i] = new  String[tLevelCondition.length];
                for (int j = 0; j < tLevelCondition.length; j++)
                {
                    String temp = tLevelCondition[j];
                    if (tFIAbStandardDataSchema.getFieldIndex(temp) < 0)
                    {
                        buildError("FIAccountItemTool","InitItemCondition","科目定义编号为"+ mFIFinItemDefSchema.getFinItemID()+ "的明细科目判断条件定义涉及的数据字段" + temp + "在FIAboriginalData表中不存在");
                        tLogDeal.WriteLogTxt("科目定义编号为"+ mFIFinItemDefSchema.getFinItemID()+ "的明细科目判断条件定义涉及的数据字段" + temp + "在FIAboriginalData表中不存在" + enter);
                        return false;
                    }
                    else
                    {
                       ItemCondition[i][j] = temp;
                    }
                }
            }

            if(mFIDetailFinItemCodeSet.size()==0)
            {
                buildError("FIAccountItemTool","InitItemCondition","科目定义编号为"+ mFIFinItemDefSchema.getFinItemID()+ "的明细科目判断条件定义表存在但明细科目映射数据定义为空");
                tLogDeal.WriteLogTxt("科目定义编号为"+ mFIFinItemDefSchema.getFinItemID()+ "的明细科目判断条件定义表存在但明细科目映射数据定义为空" + enter);
                return false;
            }

            mFIDetailFinItemCodeSetUint = new FIDetailFinItemCodeSet[mFIDetailFinItemDefSet.size()];
            int tempIndex=0;
            String tempJudgementNo = mFIDetailFinItemDefSet.get(1).getJudgementNo();
            if(!tempJudgementNo.equals(mFIDetailFinItemCodeSet.get(1).getJudgementNo()))
            {
                buildError("FIAccountItemTool","InitItemCondition","科目定义编号为"+ mFIFinItemDefSchema.getFinItemID()+ "的明细科目判断条件定义与明细科目映射数据定义不匹配，明细科目映射数据可能缺少或多余定义");
                tLogDeal.WriteLogTxt("科目定义编号为"+ mFIFinItemDefSchema.getFinItemID()+ "的明细科目判断条件定义与明细科目映射数据定义不匹配，明细科目映射数据可能缺少或多余定义" + enter);
                return false;
            }

            for(int i = 1;i<=mFIDetailFinItemCodeSet.size();i++)
            {
                 if(i==1)
                 {
                    mFIDetailFinItemCodeSetUint[tempIndex] = new FIDetailFinItemCodeSet();
                 }

                 if(mFIDetailFinItemCodeSet.get(i).getJudgementNo().equals(tempJudgementNo))
                 {
                     mFIDetailFinItemCodeSetUint[tempIndex].add(mFIDetailFinItemCodeSet.get(i));
                 }
                 else
                 {
                     tempIndex++;
                     if(tempIndex>=mFIDetailFinItemDefSet.size())
                     {
                         buildError("FIAccountItemTool","InitItemCondition","科目定义编号为"+ mFIFinItemDefSchema.getFinItemID()+ "的明细科目判断条件定义与明细科目映射数据定义不匹配，明细科目映射数据可能缺少或多余定义");
                         tLogDeal.WriteLogTxt("科目定义编号为"+ mFIFinItemDefSchema.getFinItemID()+ "的明细科目判断条件定义与明细科目映射数据定义不匹配，明细科目映射数据可能缺少或多余定义" + enter);
                         return false;
                     }
                     tempJudgementNo = mFIDetailFinItemCodeSet.get(i).getJudgementNo();
                     if(!tempJudgementNo.equals(mFIDetailFinItemDefSet.get(tempIndex+1).getJudgementNo()))
                     {
                         buildError("FIAccountItemTool","InitItemCondition","科目定义编号为"+ mFIFinItemDefSchema.getFinItemID()+ "的明细科目判断条件定义与明细科目映射数据定义不匹配，明细科目映射数据可能缺少或多余定义");
                         tLogDeal.WriteLogTxt("科目定义编号为"+ mFIFinItemDefSchema.getFinItemID()+ "的明细科目判断条件定义与明细科目映射数据定义不匹配，明细科目映射数据可能缺少或多余定义" + enter);
                         return false;
                     }
                     mFIDetailFinItemCodeSetUint[tempIndex] = new FIDetailFinItemCodeSet();
                     mFIDetailFinItemCodeSetUint[tempIndex].add(mFIDetailFinItemCodeSet.get(i));
                 }

            }

        }
        return true;
    }
    /**
     * 初始化专项关联信息
     * @return
     */
    private boolean InitInfoAssociated()
    {
    	FIAbStandardDataSchema tFIAbStandardDataSchema = new  FIAbStandardDataSchema();
    	FIVoucherDataDetailSchema tFIVoucherDataDetailSchema = new  FIVoucherDataDetailSchema();
        if(mFIAssociatedItemDefSet.size()>0)
        {
            for (int i = 0; i < mFIAssociatedItemDefSet.size(); i++)
            {
                FIAssociatedItemDefSchema tFIAssociatedItemDefSchema = mFIAssociatedItemDefSet.get(i+1);
                String tempFountainID = tFIAssociatedItemDefSchema.getSourceColumnID();
                String tempTargetID = tFIAssociatedItemDefSchema.getColumnID();
                if (tFIAbStandardDataSchema.getFieldIndex(tempFountainID) < 0)
                {
                    buildError("FIAccountItemTool","InitInfoAssociated","科目定义编号为"+ mFIFinItemDefSchema.getFinItemID()+ "的科目专项定义涉及的源数据字段" + tempFountainID + "在FIAboriginalData表中不存在");
                    tLogDeal.WriteLogTxt("科目定义编号为"+ mFIFinItemDefSchema.getFinItemID()+ "的科目专项定义涉及的源数据字段" + tempFountainID + "在FIAboriginalData表中不存在" + enter);
                    return false;
                }
                if(tFIVoucherDataDetailSchema.getFieldIndex(tempTargetID)<0)
                {
                    buildError("FIAccountItemTool","InitInfoAssociated","科目定义编号为"+ mFIFinItemDefSchema.getFinItemID()+ "的科目专项定义涉及的目标数据字段" + tempTargetID + "在FIDataTransResult表中不存在");
                    tLogDeal.WriteLogTxt("科目定义编号为"+ mFIFinItemDefSchema.getFinItemID()+ "的科目专项定义涉及的目标数据字段" + tempTargetID + "在FIDataTransResult表中不存在" + enter);
                    return false;
                }
            }
        }        
        return true;
    }

    /**
     * 初始化固定关联信息
     * @return
     */
    private boolean InitInfoDirect()
    {
    	FIAbStandardDataSchema tFIAbStandardDataSchema = new  FIAbStandardDataSchema();
    	FIVoucherDataDetailSchema tFIVoucherDataDetailSchema = new  FIVoucherDataDetailSchema();
        if(mFIAssociatedDirectItemDefSet.size()>0)
        {
            for (int i = 0; i < mFIAssociatedDirectItemDefSet.size(); i++)
            {
                FIAssociatedDirectItemDefSchema tFIAssociatedDirectItemDefSchema = mFIAssociatedDirectItemDefSet.get(i+1);
                String tempFountainID = tFIAssociatedDirectItemDefSchema.getSourceColumnID();
                String tempTargetID = tFIAssociatedDirectItemDefSchema.getColumnID();
                if (tFIAbStandardDataSchema.getFieldIndex(tempFountainID) < 0)
                {
                    buildError("FIAccountItemTool","InitInfoAssociated","科目定义编号为"+ mFIFinItemDefSchema.getFinItemID()+ "的科目固定数据定义涉及的源数据字段" + tempFountainID + "在FIAboriginalData表中不存在");
                    tLogDeal.WriteLogTxt("科目定义编号为"+ mFIFinItemDefSchema.getFinItemID()+ "的科目固定数据定义涉及的源数据字段" + tempFountainID + "在FIAboriginalData表中不存在" + enter);
                    return false;
                }
                if(tFIVoucherDataDetailSchema.getFieldIndex(tempTargetID)<0)
                {
                    buildError("FIAccountItemTool","InitInfoAssociated","科目定义编号为"+ mFIFinItemDefSchema.getFinItemID()+ "的科目固定数据定义涉及的目标数据字段" + tempTargetID + "在FIDataTransResult表中不存在");
                    tLogDeal.WriteLogTxt("科目定义编号为"+ mFIFinItemDefSchema.getFinItemID()+ "的科目固定数据定义涉及的目标数据字段" + tempTargetID + "在FIDataTransResult表中不存在" + enter);
                    return false;
                }
            }
        }        
        return true;
    }

    /**
     * 凭证转换
     * @param tFIAbStandardDataSet
     * @return
     */
    private FIVoucherDataDetailSchema transData(String OperType,FIAbStandardDataSchema tFIAbStandardDataSchema)
    {
    	FIVoucherDataDetailSchema tFIVoucherDataDetailSchema = new FIVoucherDataDetailSchema();
    	
        try
        {
            if (mFIFinItemDefSchema.getDealMode().equals("1"))
            {
                //封装科目编码信息
                if (!BuildItemCode(tFIAbStandardDataSchema,tFIVoucherDataDetailSchema))
                {
                    return null;
                }

                //封装一些固定转换信息
                if (!SetDirectInfo(tFIAbStandardDataSchema,tFIVoucherDataDetailSchema))
                {
                    return null;
                }

                //封装一些专项信息
                if (!SetAssociatedInfo(tFIAbStandardDataSchema,tFIVoucherDataDetailSchema))
                {
                    return null;
                }

                //封装一些重要信息
                if (!SetImportanceInfo(tFIAbStandardDataSchema,tFIVoucherDataDetailSchema))
                {
                    return null;
                }

            }
            else if (mFIFinItemDefSchema.getDealMode().equals("2"))
            {
            	tFIVoucherDataDetailSchema = mFIFinItemClass.DealInfo(tFIAbStandardDataSchema);
            }
        }
        catch (Exception ex)
        {
            return null;

        }
    	return tFIVoucherDataDetailSchema;
    }
    
    /**
     * 封装科目编码信息
     * @param tFIAbStandardDataSchema
     * @param tFIVoucherDataDetailSchema
     * @return
     */
    private boolean BuildItemCode(FIAbStandardDataSchema tFIAbStandardDataSchema,FIVoucherDataDetailSchema tFIVoucherDataDetailSchema)
    {	
    	ErrType = "N-01";
	    try
	    {
	        String maincode = mFIFinItemDefSchema.getItemMainCode();
	        int ii = 0;
	        if (mFIDetailFinItemDefSet.size() > 0)
	        {
	            String nextJudgementNo = mFIDetailFinItemDefSet.get(1).getJudgementNo();
	            while(!nextJudgementNo.equals("end"))
	            {
	                ii ++;
	                if(ii>20)
	                {
	                    Exception ex = new Exception("明细科目生成定义可能存在死锁");
	                    throw ex;
	                }
	                int index = this.getDetailFinItemIndex(nextJudgementNo)-1;
	                String SegCode = "";
	                for (int j = 0; j < ItemCondition[index].length; j++) {
	                    String temp = tFIAbStandardDataSchema.getV(ItemCondition[index][j]).trim();
	                    if (temp.equals("null"))
	                    {
	                        ErrInfo = "类型为" + tFIAbStandardDataSchema.getCostID() + "的业务数据数据在生成科目代码为" + maincode + "的财务数据信息时出错，因为必要信息字段" + ItemCondition[index][j].trim() + "值为空" + ",业务索引号码为：" + tFIAbStandardDataSchema.getIndexNo();
	                        buildError("FIFIAccountItemTool","BuildItemCode",ErrInfo);
	                        tLogDeal.WriteLogTxt(ErrInfo + enter);
	                        tLogDeal.buildErrorLog(tFIAbStandardDataSchema.getASerialNo(),tFIAbStandardDataSchema.getIndexCode(),tFIAbStandardDataSchema.getIndexNo(),ErrInfo);
	                        return false;
	                    }
	                    if (j != ItemCondition[index].length - 1) {
	                        temp += ",";
	                    }
	                    SegCode += temp;
	                }
	                FIDetailFinItemCodeSchema tFIDetailFinItemCodeSchema = TRansItemCode(mFIFinItemDefSchema.getFinItemID(),nextJudgementNo,SegCode);
	                String partcode = tFIDetailFinItemCodeSchema.getLevelCode();
	                if ((partcode == null || partcode.equals("") || partcode.equals("null"))&&(tFIDetailFinItemCodeSchema.getJudgementNo()==null||tFIDetailFinItemCodeSchema.getJudgementNo().equals("") || tFIDetailFinItemCodeSchema.getJudgementNo().equals("null")))
	                {
	                    ErrInfo = "类型为" + tFIAbStandardDataSchema.getCostID() +
	                               "的业务数据数据在生成科目代码为" + maincode +
	                               "的财务数据信息时出错，" +
	                               "的明细科目判断编号为" + nextJudgementNo + "的信息" + SegCode +
	                               "无对应转换值"+ ",业务索引号码为：" + tFIAbStandardDataSchema.getIndexNo();
	                    buildError("FIFIAccountItemTool","BuildItemCode",ErrInfo);
	                    tLogDeal.WriteLogTxt(ErrInfo + enter);
	                    tLogDeal.buildErrorLog(tFIAbStandardDataSchema.getASerialNo(),tFIAbStandardDataSchema.getIndexCode(),tFIAbStandardDataSchema.getIndexNo(),ErrInfo);
	                    return false;
	                }
	                else
	                {
	                    if(!(partcode == null || partcode.equals("") || partcode.equals("null")))
	                    {
	                        maincode += partcode;
	                    }
	                }
	                if(tFIDetailFinItemCodeSchema.getNextJudgementNo()==null||tFIDetailFinItemCodeSchema.getNextJudgementNo().equals("") || tFIDetailFinItemCodeSchema.getNextJudgementNo().equals("null") || tFIDetailFinItemCodeSchema.getNextJudgementNo().equals("END"))
	                {
	                    nextJudgementNo="end";
	                }
	                else
	                {
	                    nextJudgementNo=tFIDetailFinItemCodeSchema.getNextJudgementNo();
	                }
	            }
	        }
	        tFIVoucherDataDetailSchema.setAccountCode(maincode);
	        tFIVoucherDataDetailSchema.setStandByString1(mFIFinItemDefSchema.getFinItemID());//科目定义ID
	        return true;
	    }
	    catch (Exception ex)
	    {
	        ErrInfo = "类型为" + tFIAbStandardDataSchema.getCostID() +"的业务数据数据在生成科目代码为" + mFIFinItemDefSchema.getItemMainCode() 
	                   + "的财务数据明细科目代码信息时出错，原因是：" + ex.getMessage()+ ",业务索引号码为：" + tFIAbStandardDataSchema.getIndexNo();
	        buildError("FIFIAccountItemTool","BuildItemCode",ErrInfo);
	        tLogDeal.WriteLogTxt(ErrInfo + enter);
	        tLogDeal.buildErrorLog(tFIAbStandardDataSchema.getASerialNo(),tFIAbStandardDataSchema.getIndexCode(),tFIAbStandardDataSchema.getIndexNo(),ErrInfo);
	        return false;
	    }
    }
    /**
     * 封装一些固定转换信息
     * @param tFIAbStandardDataSchema
     * @param tFIVoucherDataDetailSchema
     * @return
     */
    private boolean SetDirectInfo(FIAbStandardDataSchema tFIAbStandardDataSchema,FIVoucherDataDetailSchema tFIVoucherDataDetailSchema)
    {
        ErrType = "N-02";
        try
        {
            for(int i=0;i<mFIAssociatedDirectItemDefSet.size();i++)
            {
                FIAssociatedDirectItemDefSchema tFIAssociatedDirectItemDefSchema = mFIAssociatedDirectItemDefSet.get(i+1);
                String FoundID = tFIAssociatedDirectItemDefSchema.getSourceColumnID();
                String TargetID = tFIAssociatedDirectItemDefSchema.getColumnID();
                String info = tFIAbStandardDataSchema.getV(FoundID);
                if(info == null || info.equals("") || info.equals("null"))
                {
                    ErrInfo = "类型为" + tFIAbStandardDataSchema.getCostID() +
                               "的业务数据数据在生成科目代码为" + mFIFinItemDefSchema.getItemMainCode() + "的财务数据固定数据信息时出错，原因是会计分录固定数据：" + tFIAssociatedDirectItemDefSchema.getColumnID()+ "设置信息为空,业务索引号码为：" + tFIAbStandardDataSchema.getIndexNo();
                    buildError("FIFIAccountItemTool","SetDirectInfo",ErrInfo);
                    tLogDeal.WriteLogTxt(ErrInfo + enter);
                    tLogDeal.buildErrorLog(tFIAbStandardDataSchema.getASerialNo(),tFIAbStandardDataSchema.getIndexCode(),tFIAbStandardDataSchema.getIndexNo(),ErrInfo);
                    return false;
                }
                tFIVoucherDataDetailSchema.setV(TargetID,tFIAbStandardDataSchema.getV(FoundID));
            }
        }
        catch (Exception ex)
        {
            ErrInfo = "类型为" + tFIAbStandardDataSchema.getCostID() +
                       "的业务数据数据在生成科目代码为" + mFIFinItemDefSchema.getItemMainCode() + "的财务数据固定数据信息时出错，原因是：" + ex.getMessage()+ ",业务索引号码为：" + tFIAbStandardDataSchema.getIndexNo();
            buildError("FIFIAccountItemTool","SetDirectInfo",ErrInfo);
            
            tLogDeal.WriteLogTxt(ErrInfo + enter);
            tLogDeal.buildErrorLog(tFIAbStandardDataSchema.getASerialNo(),tFIAbStandardDataSchema.getIndexCode(),tFIAbStandardDataSchema.getIndexNo(),ErrInfo);
            return false;
        }
        return true;
    }
    /**
     * 封装一些专项信息
     * @param tFIAbStandardDataSchema
     * @param tFIVoucherDataDetailSchema
     * @return
     */
    private boolean SetAssociatedInfo(FIAbStandardDataSchema tFIAbStandardDataSchema,FIVoucherDataDetailSchema tFIVoucherDataDetailSchema)
    {
    	ErrType = "N-03";
        try
        {
            for(int i=0;i<mFIAssociatedItemDefSet.size();i++)
            {
                FIAssociatedItemDefSchema tFIAssociatedItemDefSchema = mFIAssociatedItemDefSet.get(i+1);
                String FoundID = tFIAssociatedItemDefSchema.getSourceColumnID();
                String TargetID = tFIAssociatedItemDefSchema.getColumnID();
                String info = tFIAbStandardDataSchema.getV(FoundID);
                if(info == null || info.equals("") || info.equals("null"))
                {
                    ErrInfo = "类型为" + tFIAbStandardDataSchema.getCostID() +"的业务数据数据在生成科目代码为" + mFIFinItemDefSchema.getItemMainCode() 
                    	+ "的财务数据专项数据信息时出错，原因是专项：" + tFIAssociatedItemDefSchema.getAssociatedID() + "设置信息为空,业务索引号码为：" + tFIAbStandardDataSchema.getIndexNo();
                    buildError("FIFIAccountItemTool","SetDirectInfo",ErrInfo);
                    tLogDeal.WriteLogTxt(ErrInfo + enter);
                    tLogDeal.buildErrorLog(tFIAbStandardDataSchema.getASerialNo(),tFIAbStandardDataSchema.getIndexCode(),tFIAbStandardDataSchema.getIndexNo(),ErrInfo);
                    return false;
                }
                tFIVoucherDataDetailSchema.setV(TargetID,tFIAbStandardDataSchema.getV(FoundID));
            }
        }
        catch (Exception ex)
        {
            ErrInfo = "类型为" + tFIAbStandardDataSchema.getCostID() +"的业务数据数据在生成科目代码为" + mFIFinItemDefSchema.getItemMainCode() 
                       + "的财务数据专项数据信息时出错，原因是：" + ex.getMessage()+ ",业务索引号码为：" + tFIAbStandardDataSchema.getIndexNo();
            buildError("FIFIAccountItemTool","SetDirectInfo",ErrInfo);
            tLogDeal.WriteLogTxt(ErrInfo + enter);
            tLogDeal.buildErrorLog(tFIAbStandardDataSchema.getASerialNo(),tFIAbStandardDataSchema.getIndexCode(),tFIAbStandardDataSchema.getIndexNo(),ErrInfo);
            return false;
        }
        return true;
    }
    /**
     * 封装一些重要信息
     * @param tFIAbStandardDataSchema
     * @param tFIVoucherDataDetailSchema
     * @return
     */
    private boolean SetImportanceInfo(FIAbStandardDataSchema tFIAbStandardDataSchema,FIVoucherDataDetailSchema tFIVoucherDataDetailSchema)
    {
        ErrType = "N-04";
        //tFIVoucherDataDetailSchema.setFSerialNo(FinCreateSerialNo.getFSerialNo());
        tFIVoucherDataDetailSchema.setFSerialNo(mFserialno[Fno]);
        tFIVoucherDataDetailSchema.setSerialNo(tFIAbStandardDataSchema.getSerialNo());
        tFIVoucherDataDetailSchema.setCertificateID(mFIVoucherBnFeeSchema.getVoucherID());
        tFIVoucherDataDetailSchema.setCostID(tFIAbStandardDataSchema.getCostID());
        tFIVoucherDataDetailSchema.setFinItemType(mFinItemType);
        tFIVoucherDataDetailSchema.setSumMoney(tFIAbStandardDataSchema.getSumActuMoney());
        tFIVoucherDataDetailSchema.setAccountDate(tFIAbStandardDataSchema.getAccountDate());
        tFIVoucherDataDetailSchema.setCurrency(tFIAbStandardDataSchema.getCurrency());
        tFIVoucherDataDetailSchema.setBatchNo(BatchNo);
        tFIVoucherDataDetailSchema.setListFlag(tFIAbStandardDataSchema.getListFlag());
        tFIVoucherDataDetailSchema.setReadState("0");
        tFIVoucherDataDetailSchema.setCheckFlag("00");
        
        if("1".equals(tFIVoucherDataDetailSchema.getListFlag()))
        	tFIVoucherDataDetailSchema.setContNo(tFIAbStandardDataSchema.getContNo());
        else
        	tFIVoucherDataDetailSchema.setContNo(tFIAbStandardDataSchema.getGrpContNo());
        
        tFIVoucherDataDetailSchema.setVersionNo(VersionNo);
        
        return true;
    }
    
    private int getDetailFinItemIndex(String JudgementNo) throws Exception
    {
        for(int i=1;i<=mFIDetailFinItemDefSet.size();i++)
        {
            if(mFIDetailFinItemDefSet.get(i).getJudgementNo().equals(JudgementNo))
            {
                return i;
            }
        }
        Exception e = new Exception("科目定义编号为" + this.mFIFinItemDefSchema.getFinItemID()+ "处理实例中不存在判断编号为"+ JudgementNo + "数据准备信息" );
        throw e;
    }

    private FIDetailFinItemCodeSchema TRansItemCode(String FinItemID, String JudgementNo, String LevelCondition)
    {

        FIDetailFinItemCodeSchema tFIDetailFinItemCodeSchema = new FIDetailFinItemCodeSchema();
        int index = 0;
        for (int i = 1; i <= mFIDetailFinItemDefSet.size(); i++)
        {
            if (mFIDetailFinItemDefSet.get(i).getJudgementNo().equals(JudgementNo))
            {
                index = i - 1;
                break;
            }
            if (i == mFIDetailFinItemDefSet.size())
            {
                return tFIDetailFinItemCodeSchema;
            }
        }

        //兼容科目由顺序段值拼接模式
        if(mFIDetailFinItemCodeSetUint[index].size()==1 && mFIDetailFinItemCodeSetUint[index].get(1).getLevelConditionValue().equals("original"))
        {
            tFIDetailFinItemCodeSchema.setSchema(mFIDetailFinItemCodeSetUint[index].get(1));
            tFIDetailFinItemCodeSchema.setLevelCode(LevelCondition + tFIDetailFinItemCodeSchema.getLevelCode());
            return tFIDetailFinItemCodeSchema;
        }

        for (int i = 1; i <= mFIDetailFinItemCodeSetUint[index].size(); i++)
        {
            FIDetailFinItemCodeSchema tt = mFIDetailFinItemCodeSetUint[index].get(i);
            if (tt.getFinItemID().trim().equals(FinItemID.trim()) &&
                tt.getJudgementNo().trim().equals(JudgementNo) &&
                tt.getLevelConditionValue().trim().equals
                (LevelCondition.trim()))
            {
                tFIDetailFinItemCodeSchema.setSchema(tt);
                break;
            }
        }
        return tFIDetailFinItemCodeSchema;
    }
    
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
