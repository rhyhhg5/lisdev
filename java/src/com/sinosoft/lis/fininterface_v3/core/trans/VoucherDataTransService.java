package com.sinosoft.lis.fininterface_v3.core.trans;

import java.util.Date;

import com.sinosoft.lis.db.FIAssociatedItemDefDB;
import com.sinosoft.lis.db.FIPeriodManagementDB;
import com.sinosoft.lis.db.FIRulesVersionDB;
import com.sinosoft.lis.db.FIVoucherBnDB;
import com.sinosoft.lis.fininterface_v3.core.trans.special.FIAssociatedItem;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIAssociatedItemDefSchema;
import com.sinosoft.lis.schema.FIVoucheManageSchema;
import com.sinosoft.lis.vschema.FIAssociatedItemDefSet;
import com.sinosoft.lis.vschema.FIPeriodManagementSet;
import com.sinosoft.lis.vschema.FIRulesVersionSet;
import com.sinosoft.lis.vschema.FIVoucherBnSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 凭证数据转换</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class VoucherDataTransService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    public GlobalInput mGlobalInput = new GlobalInput();
    
    private FIOperationLog tLogDeal = null;
    /** 0 小于本会计期间，1 本会计期间 */
    private String DataType = "";
    
    private String StartDate ;
    
    private String EndDate ;
    
    private Date tDate;
    
    private String mBatchNo ;
    
    private String EventNo = "";
    
    private MMap map = new MMap();
    
    private FIRulesVersionSet mFIRulesVersionSet ;
    
    private FIVoucherBnSet mFIVoucherBnSet ;
    
    private VoucherTransEngine mVoucherTransEngine ;
    
    private final String enter = "\r\n"; // 换行符
	
    public VoucherDataTransService(FIOperationLog logDeal)
    {
        tLogDeal = logDeal;
        EventNo = tLogDeal.getEventNo();
        mVoucherTransEngine = new VoucherTransEngine(tLogDeal);
    }
    
    
    public boolean dealService(VData cInputData)
    {
    	if(!prepareData(cInputData))
    	{
    		return false;
    	}
    	
    	if(!createFIVoucheManage())
    	{
    		return false;
    	}
    	
    	if(!dealDataTrans())
    	{
    		return false;
    	}
    	
    	//更新准业务转换数据状态,及特殊数据处理
    	updateData();
    	
    	
    	return true;
    }
    
    /**
     * 获取数据
     * @param cInputData
     * @return
     */
    private boolean prepareData(VData cInputData)
    {
        
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        DataType = (String) cInputData.get(1);
        //同步银行二级科目配置信息
        synchronousBankNoData();

    	//获取当前开启的会计期间 
        if(!getPeriodManagement())
        {
            return false;
        }
        
        //账务规则版本号码
        if (!getRuleVersion(StartDate, EndDate)) 
        {
            return false;
        }
        return true;
    }
    
    private boolean synchronousBankNoData()
    {
    	MMap tMap = new MMap();
    	String tSQL = "insert into FIDetailFinItemCode select '00000000000000000002' as VersionNo,'0000003' as FinItemID,'02' as JudgementNo,a.LevelCondition as LevelConditionValue,a.LevelCode as LevelCode,'END' as NextJudgementNo,'银行账号' ReMark ";
    	tSQL = tSQL+" from LIDetailFinItemCode a where a.FinItemID='0000003' and not exists(select 1 from FIDetailFinItemCode b where b.Finitemid=a.Finitemid and a.LevelCondition = b.LevelConditionValue and a.LevelCode=b.LevelCode) and a.LevelCondition not in('N01','Y01') ";
    	
    	String dSQL="delete from FIDetailFinItemCode a where a.FinItemID='0000003' and a.versionno='00000000000000000002' and LevelConditionValue not in('N','Y') and not exists(select 1 from LIDetailFinItemCode b where b.Finitemid=a.Finitemid and b.LevelCondition = a.LevelConditionValue and a.LevelCode=b.LevelCode) ";
    	
    	tMap.put(dSQL, "DELETE");
      tMap.put(tSQL, "INSERT");
 
        VData tInputInfo = new VData();
        tInputInfo.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tInputInfo, ""))
        {
            buildError("VoucherDataTransService","synchronousBankNoData", "同步银行二级科目信息失败,错误是：" + tPubSubmit.mErrors.getFirstError() );
            tLogDeal.WriteLogTxt("同步银行二级科目信息失败,错误是：" + tPubSubmit.mErrors.getFirstError() + enter);
            return false;
        }
    	
    	return true;
    }
    
    /**
     * 创建批次管理信息
     * @return
     */
    private boolean createFIVoucheManage()
    {
    	FIVoucheManageSchema tFIVoucheManageSchema  = new FIVoucheManageSchema();
    	mBatchNo = FinCreateSerialNo.getBatchNo();
 	
    	tFIVoucheManageSchema.setBatchNo(mBatchNo);
    	tFIVoucheManageSchema.setStartDate(StartDate);
    	tFIVoucheManageSchema.setEndDate(EndDate);
    	tFIVoucheManageSchema.setEventNo(EventNo);
    	tFIVoucheManageSchema.setMakeDate(PubFun.getCurrentDate());
    	tFIVoucheManageSchema.setMakeTime(PubFun.getCurrentTime());
    	tFIVoucheManageSchema.setState("30");
    	tFIVoucheManageSchema.setModifyDate(PubFun.getCurrentDate());
    	tFIVoucheManageSchema.setModifyTime(PubFun.getCurrentTime());
    	tFIVoucheManageSchema.setOperator(mGlobalInput.Operator);
    	
    	map.put(tFIVoucheManageSchema, "INSERT");
    	 
    	return true;
    }
    
    /**
     * 凭证转换处理
     * @return
     */
    private boolean dealDataTrans()
    {
    	VData param = new VData();   	
    	TransferData tTransferData = new TransferData();
    	tTransferData.setNameAndValue("StartDate", StartDate);
    	tTransferData.setNameAndValue("EndDate", EndDate);
    	tTransferData.setNameAndValue("DataType", DataType);
    	tTransferData.setNameAndValue("BatchNo", mBatchNo);
    	
    	param.add(mGlobalInput);
    	param.add(tTransferData);
    	
    	//凭证转换处理
    	for(int i=1;i<=mFIRulesVersionSet.size();i++)
    	{
        	String tVersionNo = mFIRulesVersionSet.get(i).getVersionNo();
    		if(!initInfo(tVersionNo))
        	{
        		return false;
        	}
        	if(mFIVoucherBnSet.size()<1)
        	{
        		System.out.println("没有待转换的业务数据！！");
        		continue;
        	}
        	
        	//凭证转换处理
        	mVoucherTransEngine.ruleService(mFIVoucherBnSet, param);
        	
        	//专项数据转换
        	dealAssociatedData(tVersionNo,mBatchNo);
    	}
    	
    	return true;
    }
    
    /**
     * 专项数据转换
     * @return
     */
    private boolean dealAssociatedData(String tVersionNo,String tBatchNo)
    {
    	FIAssociatedItemDefDB tFIAssociatedItemDefDB = new FIAssociatedItemDefDB();
    	tFIAssociatedItemDefDB.setVersionNo(tVersionNo);
    	
    	FIAssociatedItemDefSet tFIAssociatedItemDefSet = tFIAssociatedItemDefDB.query();
        if (tFIAssociatedItemDefDB.mErrors.needDealError())
        {
            buildError("VoucherDataTransService","dealAssociatedData", "专项定义查询失败");
            tLogDeal.WriteLogTxt("专项定义查询失败" + enter);
            return false;
        }
        if (tFIAssociatedItemDefSet.size() >0)
        {
            for(int j=1;j<=tFIAssociatedItemDefSet.size();j++)
            {
            	FIAssociatedItemDefSchema tFIAssociatedItemDefSchema = tFIAssociatedItemDefSet.get(j);
            	
            	String transmode = tFIAssociatedItemDefSchema.getTransFlag();
            	
                if (transmode.equals("N"))
                {
                    continue;
                }
                else if (transmode.equals("S"))
                {
                    
                	String sql = tFIAssociatedItemDefSchema.getTransSQL();
                	
                    String mUpdateSQL = "update FIVoucherDataDetail set " +
                    tFIAssociatedItemDefSchema.getColumnID().trim()
                    + " = (" + sql + ")" +
                    " where " +
                    " exists(select 'X' from FIInfoFinItemAssociated " +
                    " where FIInfoFinItemAssociated.VersionNo = FIVoucherDataDetail.VersionNo and FIInfoFinItemAssociated.finitemid = FIVoucherDataDetail.StandByString1  " +
                    " and AssociatedID = '" + tFIAssociatedItemDefSchema.getAssociatedID() + 
                    "') and BatchNo = '" + tBatchNo + "' and VersionNo = '"+tVersionNo+"'";

                    ExeSQL tExeSQL = new ExeSQL();
                    System.out.println("Associated:"+mUpdateSQL);
                    if (!tExeSQL.execUpdateSQL(mUpdateSQL))
                    {
                        buildError("VoucherDataTransService", "dealAssociatedData", "专项" + tFIAssociatedItemDefSchema.getAssociatedID() + "SQL方式更新专项字段失败，信息为：" + tExeSQL.mErrors.getFirstError());
                        tLogDeal.WriteLogTxt("专项" + tFIAssociatedItemDefSchema.getAssociatedID() + "SQL方式更新专项字段失败，信息为：" + tExeSQL.mErrors.getFirstError() + enter);
                        continue;
                    }
                }
                else if (transmode.equals("C"))
                {
                    VData sVData = new VData();
                    sVData.add(mBatchNo);
                    
					try 
					{
						Class tClass = Class.forName(tFIAssociatedItemDefSchema.getTransClass());
						
	                    FIAssociatedItem mTransTypeClass = (FIAssociatedItem)tClass.newInstance();
	                    
	                    if(!mTransTypeClass.DealInfo(sVData))
	                    {
	                        buildError("FICertificateGatherBL", "DealAssociatedData", "专项" + tFIAssociatedItemDefSchema.getAssociatedID() + "类方式更新专项字段失败，信息为：" + mTransTypeClass.mErrors.getFirstError());
	                        tLogDeal.WriteLogTxt("专项" + tFIAssociatedItemDefSchema.getAssociatedID() + "类方式更新专项字段失败，信息为：" + mTransTypeClass.mErrors.getFirstError() + enter);
	                        return false;
	                    }	                    
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                }
            }
        }
        else
        {
            tLogDeal.WriteLogTxt("未查询到任何专项定义" + enter);
        }
    	
    	return true;
    }
    
    
    /**
     * 初始化规则信息,获取凭证模板
     * @return
     */
    private boolean initInfo(String VersionNo)
    {  	
    	String tSQL = "select * from FIVoucherBn a where a.VersionNo = '"+VersionNo+"' " 
    		+" and exists(select 1 from FIVoucherDef b where a.VoucherID = b.VoucherID and b.VersionNo='"+VersionNo+"')"
    		+" and exists(select 1 from FIAbStandardData c where a.BusinessID = c.BusTypeID and c.state='00' ";
    	if("0".equals(DataType))
    	{
    		tSQL = tSQL+" and c.AccountDate <'"+StartDate+"' )";
    	}
    	if("1".equals(DataType))
    	{
    		tSQL = tSQL+" and c.AccountDate between '"+StartDate+"' and '"+EndDate+"')";
    	}
    	System.out.println("初始化FIVoucherBn:"+tSQL);
    	
    	mFIVoucherBnSet = new FIVoucherBnDB().executeQuery(tSQL+" with ur ");
        
        return true;
    }
    
    /**
     * 获取当前开启的会计期间 
     * @return
     */
    private boolean getPeriodManagement()
    {
        String sql = "  select * from FIPeriodManagement where  state='1' order by StartDate asc with ur ";
        
        FIPeriodManagementSet tFIPeriodManagementSet = new FIPeriodManagementDB().executeQuery(sql);
        
        if (tFIPeriodManagementSet.size() ==0)
        {
            buildError("FIDistillMain","getPeriodManagement", "没有开启的会计期间！");
            return false;
        }
        else if(tFIPeriodManagementSet.size() !=1)
        {
            String tInfo = "您输入的时间区间内查询到"+tFIPeriodManagementSet.size()+"个有效会计区间！分别是";
            for(int i =0;i<tFIPeriodManagementSet.size();i++)
            {
                if(i==0)
                {
                    tInfo += "[" + tFIPeriodManagementSet.get(i + 1).getStartDate() + "][" +  tFIPeriodManagementSet.get(i + 1).getEndDate() + "]";
                }
                else
                {
                    tInfo += "," + "[" + tFIPeriodManagementSet.get(i + 1).getStartDate() + "][" +  tFIPeriodManagementSet.get(i + 1).getEndDate() + "]";
                }
            }
            buildError("VoucherDataTransService","getPeriodManagement", tInfo);
            return false;
        }

        StartDate = tFIPeriodManagementSet.get(1).getStartDate();
        EndDate = tFIPeriodManagementSet.get(1).getEndDate();
        
        //账务日期维护
        String tenddate = PubFun.getCurrentDate();
        if(java.sql.Date.valueOf(EndDate).after(java.sql.Date.valueOf(tenddate)))
        {
//        	FDate changeDate = new FDate();
//        	tDate = changeDate.getDate(tenddate);
//        	tDate = PubFun.calDate(tDate, -1, "D", null);
//        	tenddate = changeDate.getString(tDate);
        	EndDate = tenddate;
        }
        return true;
    }
    
    /**
     * 获取账务规则版本
     * @param sDate
     * @param eDate
     * @return
     */
    private boolean getRuleVersion(String sDate, String eDate)
    {

        String sql = " select * from FIRulesVersion where  versionstate ='01' and StartDate<='" + eDate + "' and (EndDate>'" + sDate + "' or EndDate is null)" + " order by startdate asc with ur ";

        FIRulesVersionDB tFIRulesVersionDB = new FIRulesVersionDB();
        mFIRulesVersionSet = tFIRulesVersionDB.executeQuery(sql);
        if (mFIRulesVersionSet.size() == 0)
        {
            buildError("VoucherDataTransService","getRuleVersion", "您输入的时间区间内提数规则无版本，请重新输入起始日期！");
            return false;
        }
        return true;

    }
    
    private boolean updateData()
    {
    	String tActumoney ="update FIAbStandardData a set a.state = '01'  where a.state = '00' and a.checkflag = '00' and sumactumoney = 0 ";
    	String tSQL  ="update FIAbStandardData a set a.state = '01'  where a.state = '00' and a.checkflag = '00' and exists(select 1 from FIVoucherDataDetail b where a.SerialNo = b.SerialNo )";
        //PICC与原版本凭证类型同步转换
        String tClassTypeSQL  =" update FIVoucherDataDetail a set ClassType=( select codealias from ficodetrans b,fiabstandarddata c where b.codetype = 'ClassTypeNew' and a.serialno=c.serialno and b.code = c.bustypeid and b.othersign = a.listflag and b.versionno = a.certificateid fetch first 1 rows only ) where a.batchno= '"+mBatchNo+"'";
        String tVoucherTypeSql =  "update FIVoucherDataDetail a set a.vouchertype = (select b.vouchertype from FIVoucherDef b where b.VoucherID = a.CertificateID) where a.batchno = '" + mBatchNo + "'";
        //新疆共保处理
        String tXJGBSql = "update FIVoucherDataDetail a set a.classtype=a.classtype||'-Y' ,a.vouchertype = 'Y'||substr(vouchertype,2,length(vouchertype)-1) where a.batchno='"+mBatchNo+"' and Classtype not in('R-01','R-02','R-03') and exists(select 1 from FIAbStandardData b where a.SerialNo = b.SerialNo and b.managecom='86650105')";
		//科目调整
        String tAccountCode1 = "update FIVoucherDataDetail a set accountcode='2611020101' where batchno='"+mBatchNo+"' and accountcode in('2611020301','2611020401','2611020108','2611020702','2611020701','2611020201') and exists(select 1 from FIAbStandardData b where a.SerialNo = b.SerialNo and  b.riskcode in ('170106','1605','170101')) ";
        String tAccountCode2 = "update FIVoucherDataDetail a set accountcode='6051010000' where batchno='"+mBatchNo+"' and accountcode in('6051010500','6051010100') and exists(select 1 from FIAbStandardData b where a.SerialNo = b.SerialNo and  b.riskcode in ('170106','1605','170101')) ";
        //应付业务支出粒度改造
        String tAccountCode3 ="update fivoucherdatadetail a set (a.riskcode, a.costcenter) =(select b.riskcode, b.costcenter from fiabstandarddata b where a.serialno = b.serialno ) where a.accountcode like '2203%' and exists (select 1 from fiabstandarddata c   where a.serialno = c.serialno and c.riskcode in ('162201', '162301') and substr(c.costcenter, 5, 4) = '9396' and c.indexcode in ('03', '04')) and (a.riskcode is null or a.riskcode = '') and  (a.costcenter = '' or a.costcenter is null) and  a.batchno='"+mBatchNo+"'";

        //更新状态
		String checkflag = "update FIVoucherDataDetail a set a.checkflag = '01' where a.checkflag='00' and a.readstate ='0' and a.batchno='"+mBatchNo+"' "+
		"and exists(select 1 from FIRuleDealErrLog b, FIAbStandardData c where c.IndexCode = b.IndexCode and c.indexno = b.BusinessNo and a.serialno = c.serialno and b.DealState = '0' and b.CheckBatchNo='"+tLogDeal.getMFIRuleDealLogSchema().getCheckBatchNo()+"') ";
		
		//将再保系统往来科目的客户号字段置值
		String tBclient = "update FIVoucherDataDetail a set a.Bclient = 'IC'||a.ExecuteCom where a.batchno ='"+mBatchNo+"' and a.accountcode = '3001010000' ";
		
		//保单借贷不平数据
		String checkContData = "update FIVoucherDataDetail a set a.checkflag='01' where a.contno in (" +
				"select contno from FIVoucherDataDetail where batchno='"+mBatchNo+"' and checkflag='00' group by contno having sum(case finitemtype when 'C' then -summoney else summoney end )<>0" +
				") and a.batchno='"+mBatchNo+"' and a.checkflag='00'";
		
		if("0".equals(DataType))
		{
			System.out.println("批次"+mBatchNo+"过账日期小于会计期间，将其过账日期置为会计期间首日！");
			String dateSql = "update FIVoucherDataDetail a set a.AccountDate = '"+StartDate+"' where a.batchno='"+mBatchNo+"' and a.AccountDate < '"+StartDate+"' ";
			map.put(dateSql, "UPDATE");
		}
		
		map.put(tActumoney, "UPDATE");
        map.put(tSQL, "UPDATE");
        map.put(tClassTypeSQL, "UPDATE");
        map.put(tVoucherTypeSql, "UPDATE");
        map.put(tXJGBSql, "UPDATE");
        map.put(tAccountCode1, "UPDATE");
        map.put(tAccountCode2, "UPDATE");
       	map.put(tAccountCode3, "UPDATE");
        map.put(checkflag, "UPDATE");
        map.put(tBclient, "UPDATE");
        map.put(checkContData, "UPDATE");
        //退保回退数据处理
        map.put("update fivoucherdatadetail a set accountcode='2205020000',standbystring1='0000016',costcenter=null,bclient=db2inst1.LF_BCLIENT(a.listflag,a.contno) where batchno = 'SSSS0000000000008251' and finitemtype='C' and vouchertype in ('X4','Y4') and accountcode in ('2203040000','2221170700','6051990000','2205010000')", "UPDATE");
		map.put("update fivoucherdatadetail a set finitemtype=(case finitemtype when 'D' then 'C' else 'D' end),batchno='"+mBatchNo+"' where batchno = 'SSSS0000000000008251' and accountdate between '"+StartDate+"' and '"+EndDate+"'", "UPDATE");		
		
        VData tInputInfo = new VData();
        tInputInfo.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tInputInfo, ""))
        {
            buildError("VoucherDataTransService","updateState", "凭证转换后，更新标准转换数据状态失败,错误是：" + tPubSubmit.mErrors.getFirstError() );
            tLogDeal.WriteLogTxt("凭证转换后，更新标准转换数据状态失败,错误是：" + tPubSubmit.mErrors.getFirstError() + enter);
            return false;
        }
        map = null;
        tInputInfo = null;
    	
    	return true;
    }
    
    
    
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    
    public String getBatchno(){
    	return this.mBatchNo;
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
