package com.sinosoft.lis.fininterface_v3.core.trans.special;

import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.schema.FIAbStandardDataSchema;
import com.sinosoft.lis.schema.FIVoucherBnFeeSchema;
import com.sinosoft.lis.schema.FIVoucherDataDetailSchema;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title:转换科目处理类接口 </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public interface FIFinItemClass {

	   /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public boolean InitInfo(FIVoucherBnFeeSchema tFIVoucherBnFeeSchema,String tFinItemType,FIOperationLog tLogDeal);
    public FIVoucherDataDetailSchema DealInfo(FIAbStandardDataSchema tFIAbStandardDataSchema);
}
