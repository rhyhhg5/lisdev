package com.sinosoft.lis.fininterface_v3.core.trans;

import com.sinosoft.lis.db.FIVoucherBnFeeDB;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.FIVoucherBnFeeSchema;
import com.sinosoft.lis.schema.FIVoucherBnSchema;
import com.sinosoft.lis.vschema.FIAbStandardDataSet;
import com.sinosoft.lis.vschema.FIVoucherBnFeeSet;
import com.sinosoft.lis.vschema.FIVoucherBnSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 凭证数据转换</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class VoucherTransEngine {

	public CErrors mErrors = new CErrors();	
	
	private GlobalInput mGlobalInput = new GlobalInput();
	
	private TransferData mTransferData ;

    /** 0 小于本会计期间，1 本会计期间 */
    private String mDataType = "";
    
    private String mStartDate ;
    
    private String mEndDate ;
    
    private String EventNo = "";
    
    private String BatchNo ;
    
	private FIVoucherBnSet mFIVoucherBnSet ;
	/** 日志处理*/
	private FIOperationLog tLogDeal = null;
	
	private FIAccountItemTool mCFIAccountItemTool ;
	private FIAccountItemTool mDFIAccountItemTool ;
	
    private final String enter = "\r\n"; // 换行符
    
    public VoucherTransEngine(FIOperationLog logDeal)
    {
    	tLogDeal = logDeal;
    	EventNo = tLogDeal.getEventNo();
    	mCFIAccountItemTool = new FIAccountItemTool(logDeal);
    	mDFIAccountItemTool = new FIAccountItemTool(logDeal);
    }

	public boolean ruleService(FIVoucherBnSet tFIVoucherBnSet,
			VData param) {
		// TODO Auto-generated method stub

		mFIVoucherBnSet = tFIVoucherBnSet;
    	
    	if (!getInputData(param))
        {
			buildError("VoucherTransEngine","ruleService", "获取参数出错！！");
            tLogDeal.WriteLogTxt("获取参数出错！！" + enter);
    		return false;
        }

    	if (!getCheckData())
        {
			buildError("VoucherTransEngine","ruleService", "数据校检出错！！");
            tLogDeal.WriteLogTxt("数据校检出错！！" + enter);
    		return false;
        }
    	
    	//执行规则
        dealRuels();
		
		return true;
	}

	/**
     * 获取参数
     * @param cInputData
     * @return
     */
    private boolean getInputData(VData cInputData)
    {
    	mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
    	mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
    	if(mTransferData==null)
    	{
			buildError("VoucherTransEngine","getInputData", "mTransferData获取参数出错！！");
            tLogDeal.WriteLogTxt("获取参数出错！！" + enter);
    		return false;
    	}
    	if(mGlobalInput==null)
    	{
    		buildError("VoucherTransEngine","getInputData ","mGlobalInput获取参数出错！！"); 
            tLogDeal.WriteLogTxt("获取参数出错！！" + enter);
    		return false;
    	}
    	
        /** 0 小于本会计期间，1 本会计期间 */
        mDataType = (String)mTransferData.getValueByName("DataType");
        mStartDate = (String)mTransferData.getValueByName("StartDate");
        mEndDate = (String)mTransferData.getValueByName("EndDate");
        BatchNo = (String)mTransferData.getValueByName("BatchNo");
        
    	return true;
    }	
    
    /**
     * 数据校检
     * @return
     */
    private boolean getCheckData()
    {
    	
    	return true;
    }
    
    /**
     * 批量执行规则
     */
    private void dealRuels()
    {
		for(int i=1;i <=mFIVoucherBnSet.size();i++)
		{
			FIVoucherBnSchema tFIVoucherBnSchema = mFIVoucherBnSet.get(i);
			
			FIVoucherBnFeeSet tFIVoucherBnFeeSet = getFIVoucherBnFee(tFIVoucherBnSchema);
			
			if(tFIVoucherBnFeeSet.size()<1)
			{
				buildError("DataTransRuleEngine","dealRuels", "凭证没有配置账务流转规则，"+"账务规则版本："+tFIVoucherBnSchema.getVersionNo()+"，凭证编码："+tFIVoucherBnSchema.getVoucherID());
	            tLogDeal.WriteLogTxt("凭证没有配置账务流转规则，"+"账务规则版本："+tFIVoucherBnSchema.getVersionNo()+"，凭证编码："+tFIVoucherBnSchema.getVoucherID()+ enter);					
	            continue;
			}
			if(!ruleEXE(tFIVoucherBnFeeSet))
			{
				buildError("DataTransRuleEngine","dealRuels", "凭证转换错误，"+"账务规则版本："+tFIVoucherBnSchema.getVersionNo()+"，凭证编码："+tFIVoucherBnSchema.getVoucherID());
	            tLogDeal.WriteLogTxt("凭证转换错误，"+"账务规则版本："+tFIVoucherBnSchema.getVersionNo()+"，凭证编码："+tFIVoucherBnSchema.getVoucherID()+ enter);					
			}
		}
    }

    /**
     * 规则执行
     * @param tFIDataExtractRulesSchema
     * @return
     */
    private boolean ruleEXE(FIVoucherBnFeeSet tFIVoucherBnFeeSet)
    {
        for(int i=1 ;i<=tFIVoucherBnFeeSet.size();i++)
        {
        	FIVoucherBnFeeSchema tFIVoucherBnFeeSchema = tFIVoucherBnFeeSet.get(i);
        	String tDFinItemID = tFIVoucherBnFeeSchema.getD_FinItemID();
        	String tCFinItemID = tFIVoucherBnFeeSchema.getC_FinItemID();
	    	//获取准业务转换数据
        	SchemaSet tSchemaSet = new FIAbStandardDataSet();
	    	String tSQL = getAbStandardDataSQL(tFIVoucherBnFeeSchema);
	    	boolean flag = false;

        	//通过游标处理
        	RSWrapper  rsWrapper = new RSWrapper();
        	if (!rsWrapper.prepareData(tSchemaSet, tSQL))
            {
                mErrors.copyAllErrors(rsWrapper.mErrors);
                System.out.println(rsWrapper.mErrors.getFirstError());
                buildError("DataExtRuleEngine","getDataExt", "批量数据抽取时出错,提示信息为：" +rsWrapper.mErrors.getFirstError());
                tLogDeal.WriteLogTxt("批量数据抽取时出错，提示信息为：" +rsWrapper.mErrors.getFirstError() + enter);
                tLogDeal.buildErrorLog("批量数据抽取时出错，执行出现错误,需要 重新补提数据！");
            }
        	do
            {
        		rsWrapper.getData();
		    	if(tSchemaSet!=null&&tSchemaSet.size()>0)
		    	{
		        	//借方凭证数据处理
		    		if(tDFinItemID != null && !"".equals(tDFinItemID))
		        	{
		        		flag = true;
		        		if(!mDFIAccountItemTool.dealService((FIAbStandardDataSet)tSchemaSet,tFIVoucherBnFeeSchema,BatchNo,"D"))
		            	{
		    				buildError("DataTransRuleEngine","dealRuels", "凭证转换错误，"+"账务规则版本："+tFIVoucherBnFeeSchema.getVersionNo()+"，凭证编码："+tFIVoucherBnFeeSchema.getVoucherID()+"，费用编码："+tFIVoucherBnFeeSchema.getFeeID());
		    	            tLogDeal.WriteLogTxt("凭证转换错误，"+"账务规则版本："+tFIVoucherBnFeeSchema.getVersionNo()+"，凭证编码："+tFIVoucherBnFeeSchema.getVoucherID()+"，费用编码："+tFIVoucherBnFeeSchema.getFeeID()+ enter);					
		            	}
		        	}
		    		//贷方凭证数据处理
		        	if(tCFinItemID != null && !"".equals(tCFinItemID))
		        	{
		        		flag = true;
		        		if(!mCFIAccountItemTool.dealService((FIAbStandardDataSet)tSchemaSet,tFIVoucherBnFeeSchema,BatchNo,"C"))
		            	{
		    				buildError("DataTransRuleEngine","dealRuels", "凭证转换错误，"+"账务规则版本："+tFIVoucherBnFeeSchema.getVersionNo()+"，凭证编码："+tFIVoucherBnFeeSchema.getVoucherID()+"，费用编码："+tFIVoucherBnFeeSchema.getFeeID());
		    	            tLogDeal.WriteLogTxt("凭证转换错误，"+"账务规则版本："+tFIVoucherBnFeeSchema.getVersionNo()+"，凭证编码："+tFIVoucherBnFeeSchema.getVoucherID()+"，费用编码："+tFIVoucherBnFeeSchema.getFeeID()+ enter);					
		            	}
		        	}
		        	if(!flag)
		        	{
	    	            tLogDeal.WriteLogTxt("凭证费用没有配置流转规则,"+"账务规则版本："+tFIVoucherBnFeeSchema.getVersionNo()+"，凭证编码："+tFIVoucherBnFeeSchema.getVoucherID()+"，费用编码："+tFIVoucherBnFeeSchema.getFeeID()+ enter);
		        	}
		    	}
		    	else
		    	{
		    		tLogDeal.WriteLogTxt("没有要流转的业务数据,"+"账务规则版本："+tFIVoucherBnFeeSchema.getVersionNo()+"，凭证编码："+tFIVoucherBnFeeSchema.getVoucherID()+"，费用编码："+tFIVoucherBnFeeSchema.getFeeID()+ enter);
		    	}
            }while(tSchemaSet != null && tSchemaSet.size() > 0);
        	
        	rsWrapper.close();
        }
    	return true;
    }
    
    /**
     * 获取凭证费用流转规则
     * @param tFIVoucherBnSchema
     * @return
     */
    private FIVoucherBnFeeSet getFIVoucherBnFee(FIVoucherBnSchema tFIVoucherBnSchema)
    {
    	String tSQL = "select * from FIVoucherBnFee where VersionNo = '"+tFIVoucherBnSchema.getVersionNo()+"' and VoucherID = '"+tFIVoucherBnSchema.getVoucherID()+"' and state = '01' with ur ";
    	
    	System.out.println("FIVoucherBnFee:"+tSQL);
    	FIVoucherBnFeeSet tFIVoucherBnFeeSet = new FIVoucherBnFeeDB().executeQuery(tSQL);
    	
    	return tFIVoucherBnFeeSet;
    }
 
    /**
     * 获取待转换标准业务数据
     * @param tFIVoucherBnFeeSchema
     * @return
     */
    private String getAbStandardDataSQL(FIVoucherBnFeeSchema tFIVoucherBnFeeSchema)
    {
    	String tSQL = "select * from FIAbStandardData a where a.BusTypeID = '"+tFIVoucherBnFeeSchema.getBusinessID()+"' and a.CostID = '"+tFIVoucherBnFeeSchema.getFeeID()+"' and state = '00' and checkflag = '00'  and sumactumoney<>0 ";
    	tSQL = tSQL+" and not exists(select 1 from FIVoucherDataDetail b where a.SerialNo = b.SerialNo and b.VersionNo  = '"+tFIVoucherBnFeeSchema.getVersionNo()+"' )";
    	//万能规则特殊处理
    	if("O-WN-000001".equals(tFIVoucherBnFeeSchema.getVoucherID()))
    	{
    		tSQL = tSQL +" and not (a.risktype ='1' and a.SecondaryFlag ='1' and a.costid in('F00000000AP','F00000000GL','F00000000MF','F000000SEGL','F000000EEGL') and riskcode in('330801', '331801', '332701', '331301', '331701','333901','334701'))";
    	}
    	if("O-WN-000002".equals(tFIVoucherBnFeeSchema.getVoucherID()))
    	{
    		tSQL = tSQL +" and not (a.risktype ='1' and a.SecondaryFlag <>'1' and a.costid in('F00000000AP','F00000000GL','F00000000MF','F000000SEGL','F000000EEGL') and riskcode in('330801', '331801', '332701', '331301', '331701','333901','334701'))";
    	}
    	//万能犹退基础额外处理
    	if("Y-BQ-WT-000001".equals(tFIVoucherBnFeeSchema.getBusinessID()))
    	{
        	if("00000000000000000001".equals(tFIVoucherBnFeeSchema.getVersionNo()))
        	{
        		//旧准则
        		tSQL = tSQL + " and not (a.costid in('F00000000AP','F00000000GL','F00000000MF','F000000SEGL','F000000EEGL') and a.SecondaryType not in('ZB','JC','EW') and a.riskcode in ('330801', '331801', '332701', '331301', '331701','333901','334701'))";
        	}
        	else
        	{
        		//新准则
        		tSQL = tSQL + " and not (a.SecondaryFlag ='1' and a.costid='F00000000AP' and a.SecondaryType in('ZB','JC','EW'))";
        	}
    	}
    	
    	//委托管理险部分领取，不生成旧准则凭证
    	if ("Y-BQ-LQ-000001".equals(tFIVoucherBnFeeSchema.getBusinessID())){
    		
    		if("00000000000000000001".equals(tFIVoucherBnFeeSchema.getVersionNo()))
        	{
        		//旧准则
        		tSQL = tSQL + " and a.riskcode not in ('690201', '690101')  ";
        	}
    	}
    	
    	//特需旧准则特殊处理
    	if("00000000000000000001".equals(tFIVoucherBnFeeSchema.getVersionNo()))
    	{
    		tSQL = tSQL + " and a.riskcode not in ('1605', '170106' , '170101') ";
    		//理赔应付，回退-死伤医疗给付 Y00000064
    		if("Y-LP-000005".equals(tFIVoucherBnFeeSchema.getBusinessID())&&"F00000000CK".equals(tFIVoucherBnFeeSchema.getFeeID()))
    		{
    			tSQL = tSQL + " and exists(select 1 from FIAboriginalGenDetail b where b.ASerialNo = a.ASerialNo and b.othernotype in('5','C')) ";
    		}
    	}
    	
    	if("0".equals(mDataType))
    	{
    		tSQL = tSQL+" and a.AccountDate <'"+mStartDate+"' ";
    	}
    	if("1".equals(mDataType))
    	{
    		tSQL = tSQL+" and a.AccountDate between '"+mStartDate+"' and '"+mEndDate+"' ";
    	}
    	System.out.println("FIAbStandardData:"+tSQL+" with ur ");
    	
    	return tSQL;    	
    }
    
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
