package com.sinosoft.lis.fininterface_v3.core;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 财务接口流程管理</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class FIProcessManage {

	public CErrors mErrors = new CErrors();
	
	private GlobalInput mGlobalInput = new GlobalInput();
	
	private String mAutoFlag;
	
    public FIProcessManage() 
    {
    	
    }
    
    private boolean init()
    {
    	return true;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        try
        {
            this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
//            this.mRIWFLogSet = (RIWFLogSet)cInputData.getObjectByObjectName("RIWFLogSet",0) ;
//            this.mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData",0);
//            this.mPath = (String)mTransferData.getValueByName("PATH");
//            this.mAutoFlag = (String)mTransferData.getValueByName("AUTOFLAG");
        }
        catch(Exception ex)
        {
            buildError("recordLog", " 流程管理程序得到业务数据时出错。 "+ex.getMessage());
            return false;
        }
        return true;
    }
    
    private boolean verify()
    {
    	
    	return true;
    }
    
    public boolean submitData(VData cInputData, String cOperate) 
    {
        if (!getInputData(cInputData, cOperate)) 
        {
            return false;
        }
        try
        {
            if (!dealData()) 
            {
                return false;
            }
        } 
        catch (Exception ex) 
        {
            buildError("initInfo","数据处理失败"+ex.getMessage());
            return false;
        }
        return true;
    }
    

    private boolean dealData() 
    {
    	
    	
    	return true;
    }
    
    private boolean autoExeWFlow() 
    {
    	
    	return true;
    }
    
    private boolean singlExeWFlow()
    {
    	
    	return true;
    }
    
    private boolean destroy()
    {
    	
    	return true;
    }
    
    private boolean prepareOutputData() 
    {
    	
    	return true;
    }
    
    private void buildError(String szFunc, String szErrMsg) 
    {
        CError cError = new CError();
        cError.moduleName = "RIWFManage";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}

}
