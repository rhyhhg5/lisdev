package com.sinosoft.lis.fininterface_v3.core.ext;

/**
 * <p>Title: 数据抽取规则引擎</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
import com.sinosoft.lis.db.FIDataBaseLinkDB;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.fininterface_v3.tools.dblink.DBConn;
import com.sinosoft.lis.fininterface_v3.tools.dblink.DBConnPool;
import com.sinosoft.lis.fininterface_v3.tools.dblink.PubSubmitForInterface;
import com.sinosoft.lis.fininterface_v3.tools.sequence.FinCreateSerialNo;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class DataExtRuleEngine{

	public CErrors mErrors = new CErrors();	
	
	GlobalInput mGlobalInput = new GlobalInput();
	
	private TransferData mTransferData ;
	
	private PubCalculator mPubCalculator = new PubCalculator();
    
	private FIDataExtractRulesSet mFIDataExtractRulesSet ;
	/** 日志处理*/
	private FIOperationLog tLogDeal = null;
	/** 数据量*/
	private int count = 0;
	
	private String mRuleType = "" ;
	private String mSubRuleType = "" ;
	/** 事件流水号*/
    private String EventNo = "";
    /** 数据提取起期*/
	private String StartDate = "";
	/** 数据提取止期*/
    private String EndDate = "";
    /** 数据机构*/
    private String ManageCom = "";
    /** 数据抽取类型*/
    private String mFIExtType;    

    private String mIndexidNo ;
    
    private final String enter = "\r\n"; // 换行符
    
    public DataExtRuleEngine(FIOperationLog logDeal)
    {
    	tLogDeal = logDeal;
    }
    
    public boolean ruleService(FIDataExtractRulesSet tFIDataExtractRulesSet,VData param)
    {
    	mFIDataExtractRulesSet = tFIDataExtractRulesSet;
    	
    	if (!getInputData(param))
        {
			buildError("DataExtRuleEngine","ruleService", "获取参数出错！！");
            tLogDeal.WriteLogTxt("获取参数出错！！" + enter);
    		return false;
        }

    	if (!getCheckData())
        {
			buildError("DataExtRuleEngine","ruleService", "数据校检出错！！");
            tLogDeal.WriteLogTxt("数据校检出错！！" + enter);
    		return false;
        }
    	
    	//执行规则
        dealRuels();

        return true;
    }
    /**
     * 获取参数
     * @param cInputData
     * @return
     */
    private boolean getInputData(VData cInputData)
    {
    	mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
    	mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
    	if(tLogDeal==null)
    	{
    		buildError("DataExtRuleEngine","getInputData", "获取LogDeal日志处理对象出错！！");
    		return false;
    	}
    	EventNo = tLogDeal.getEventNo();
    	
    	if(mTransferData==null)
    	{
			buildError("DataExtRuleEngine","getInputData", "mTransferData获取参数出错！！");
            tLogDeal.WriteLogTxt("获取参数出错！！" + enter);
    		return false;
    	}
    	if(mGlobalInput==null)
    	{
    		buildError("DataExtRuleEngine","getInputData ","mGlobalInput获取参数出错！！"); 
            tLogDeal.WriteLogTxt("获取参数出错！！" + enter);
    		return false;
    	}
    	
    	StartDate = (String)mTransferData.getValueByName("StartDate");
    	EndDate = (String)mTransferData.getValueByName("EndDate");   	
    	ManageCom = (String)mTransferData.getValueByName("ManageCom");
        mIndexidNo = (String)mTransferData.getValueByName("IndexidNo");
        
        mFIExtType = (String)mTransferData.getValueByName("FIExtType");      
    	
    	return true;
    }
    /**
     * 数据校检
     * @return
     */
    private boolean getCheckData()
    {
    	if("".equals(EventNo)||EventNo==null)
    	{
			buildError("DataExtRuleEngine","getCheckData", "事件编码为空！！");
            tLogDeal.WriteLogTxt("事件编码为空！！" + enter);
            return false;
    	}
    	if("00".equals(mFIExtType))
    	{
        	if("".equals(EndDate)||EndDate==null)
        	{
    			buildError("DataExtRuleEngine","getCheckData", "数据提取止期为空！！");
                tLogDeal.WriteLogTxt("数据提取止期为空！！" + enter);
                return false;
        	} 
    	}

    	
    	return true;
    }
    /**
     * 批量执行规则
     */
    private void dealRuels()
    {
		for(int i=1;i <=mFIDataExtractRulesSet.size();i++)
		{
			FIDataExtractRulesSchema tFIDataExtractRulesSchema = mFIDataExtractRulesSet.get(i);
			FIDataBaseLinkSchema tFIDataBaseLinkSchema= getDBSource(tFIDataExtractRulesSchema);
			
			System.out.println("数据抽取规则====="+tFIDataExtractRulesSchema.getArithmeticID());
			
			mRuleType = tFIDataExtractRulesSchema.getArithmeticType();
			mSubRuleType = tFIDataExtractRulesSchema.getSubType();
			
			if(!"00".equals(mRuleType))
			{
				buildError("DataExtRuleEngine","dealRuels", "规则类型错误，规则编码："+tFIDataExtractRulesSchema.getArithmeticID());
                tLogDeal.WriteLogTxt("规则类型错误，规则编码："+tFIDataExtractRulesSchema.getArithmeticID() + enter);		
				continue;				
			}

			if(!"00".equals(mSubRuleType)&&!"01".equals(mSubRuleType))
			{
				buildError("DataExtRuleEngine","dealRuels", "规则明细类型错误，规则编码："+tFIDataExtractRulesSchema.getArithmeticID());
                tLogDeal.WriteLogTxt("规则明细类型错误，规则编码："+tFIDataExtractRulesSchema.getArithmeticID() + enter);		
                continue;				
			}
			
			//执行数据抽取
			if(!getDataExt(tFIDataExtractRulesSchema,tFIDataBaseLinkSchema))
			{
				buildError("DataExtRuleEngine","dealRuels", "批量数据抽取出错，规则编码："+tFIDataExtractRulesSchema.getArithmeticID());
                tLogDeal.WriteLogTxt("批量数据抽取出错，规则编码："+tFIDataExtractRulesSchema.getArithmeticID() + enter);		
                continue;
			}
			tLogDeal.WriteLogTxt("批量数据抽取，规则编码："+tFIDataExtractRulesSchema.getArithmeticID() +"(数据量："+count+")。"+ enter);	
		}
    }
    /**
     * 数据抽取规则执行
     * @param tFIDataExtractRulesSchema
     * @param tFIDataBaseLinkSchema
     * @return
     */
	private boolean getDataExt(FIDataExtractRulesSchema tFIDataExtractRulesSchema,FIDataBaseLinkSchema tFIDataBaseLinkSchema)
	{		
		SchemaSet tSchemaSet = null;
		//初始化数据量
		count = 0 ;
		
		if("00".equals(mSubRuleType))
			tSchemaSet = new FIAboriginalMainSet();  //主数据提取
		else
			tSchemaSet = new FIAboriginalGenDetailSet();//子数据提取
			
		if("1".equals(tFIDataExtractRulesSchema.getDealMode()))
		{
			//类处理			
		}
		else if("2".equals(tFIDataExtractRulesSchema.getDealMode()))
		{			
			RSWrapper  rsWrapper = getRSWrapper(tFIDataBaseLinkSchema);
			if(rsWrapper==null)
			{
				System.out.println("DataExtRuleEngine-->获取数据源失败!");
				buildError("DataExtRuleEngine","getDataExt", "批量数据抽取对象RSWrapper初始化时出错，规则编码："+tFIDataExtractRulesSchema.getArithmeticID()+"");
                tLogDeal.WriteLogTxt("批量数据抽取对象RSWrapper初始化时出错，规则编码："+tFIDataExtractRulesSchema.getArithmeticID()+"" + enter);
				return false;
			}
			
	        //获取SQL规则
	        String tSql = prepareRule(tFIDataExtractRulesSchema);
	        
	        if (!rsWrapper.prepareData(tSchemaSet, tSql))
	        {
	            mErrors.copyAllErrors(rsWrapper.mErrors);
	            System.out.println(rsWrapper.mErrors.getFirstError());
                buildError("DataExtRuleEngine","getDataExt", "批量数据抽取时出错，规则编码："+tFIDataExtractRulesSchema.getArithmeticID()+",提示信息为：" +rsWrapper.mErrors.getFirstError());
                tLogDeal.WriteLogTxt("批量数据抽取时出错，规则编码："+tFIDataExtractRulesSchema.getArithmeticID()+",提示信息为：" +rsWrapper.mErrors.getFirstError() + enter);
                tLogDeal.buildErrorLog("批量数据抽取时出错，规则编码："+tFIDataExtractRulesSchema.getArithmeticID()+"，执行出现错误,需要 重新补提数据！");
                return false;
	        }
            do
            {
	           	rsWrapper.getData();
                if (tSchemaSet != null && tSchemaSet.size() > 0)
                {
                	count = count+tSchemaSet.size();//累计数据量
                	
                	if(!saveAboriginalData(tSchemaSet,tFIDataBaseLinkSchema))
            		{
                		tLogDeal.WriteLogTxt("批量数据抽取时出错，规则编码："+tFIDataExtractRulesSchema.getArithmeticID()+"，执行出现错误,需要 重新补提数据！" + enter);
                		//生成错误处理日志
                		tLogDeal.buildErrorLog("批量数据抽取时出错，规则编码："+tFIDataExtractRulesSchema.getArithmeticID()+"，执行出现错误,需要 重新补提数据！");
            		}
                }      
                
            }while(tSchemaSet != null && tSchemaSet.size() > 0);

            rsWrapper.close();			
		}
		else if("3".equals(tFIDataExtractRulesSchema.getDealMode()))
		{
			//其他			
		}
		
		return true;
	}
	
	/**
	 * 数据存储
	 * @param tSchemaSet
	 * @return
	 */
	private boolean saveAboriginalData(SchemaSet tSchemaSet,FIDataBaseLinkSchema tFIDataBaseLinkSchema)
	{
		MMap tMap= new MMap();
		VData tInputData = new VData();
		PubSubmit tPubSubmit = new PubSubmit();
		
		if("00".equals(mSubRuleType))
		{	//主数据存储
			FIAboriginalMainSet tFIAboriginalMainSet = (FIAboriginalMainSet)tSchemaSet;
			FIAboriginaInfoSet tFIAboriginaInfoSet = getAboriginaInfoData(tFIAboriginalMainSet);
			
			tMap.put(tFIAboriginalMainSet, "INSERT");
			tMap.put(tFIAboriginaInfoSet, "INSERT");
			tInputData.add(tMap);
			
            if (!tPubSubmit.submitData(tInputData, ""))
            {
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                buildError("DataExtRuleEngine","saveAboriginalData", "主数据抽取,保存在本地数据库时出错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
                tLogDeal.WriteLogTxt("主数据抽取,保存在本地数据库时出错，提示信息为：，提示信息为：" +tPubSubmit.mErrors.getFirstError() + enter);
                return false;
            }
			if(tFIDataBaseLinkSchema!=null)
			{
		        PubSubmitForInterface tPubSubmitForInterface = new PubSubmitForInterface();
		        if (!tPubSubmitForInterface.submitData(tInputData, tFIDataBaseLinkSchema))
		        {
		        	this.mErrors.copyAllErrors(tPubSubmitForInterface.mErrors);
		            buildError("DataExtRuleEngine","saveAboriginalData", "主数据抽取，保存异地数据库时出错，数据源编码:"+tFIDataBaseLinkSchema.getInterfaceCode()+"，提示信息为：" +tPubSubmitForInterface.mErrors.getFirstError());
		            tLogDeal.WriteLogTxt("主数据抽取，保存异地数据库时出错，提示信息为：" +tPubSubmitForInterface.mErrors.getFirstError() + enter);

		        	//数据回滚
		        	MMap sMap= new MMap();
		    		VData sInputData = new VData();
		    		
					sMap.put(tFIAboriginalMainSet, "DELETE");
					sMap.put(tFIAboriginaInfoSet, "DELETE");
					sInputData.add(tMap);					
		        	if(tPubSubmit.submitData(sInputData, ""))
		        	{
			        	this.mErrors.copyAllErrors(tPubSubmitForInterface.mErrors);
			            buildError("DataExtRuleEngine","saveAboriginalData", "主数据抽取，保存异地数据库出错，本地数据回滚回滚失败，提示信息为：" +tPubSubmit.mErrors.getFirstError());
			            tLogDeal.WriteLogTxt("主数据抽取，保存异地数据库出错，本地数据回滚回滚失败!提示信息为：" +tPubSubmit.mErrors.getFirstError() + enter);		        		
			            return false;
		        	}
		            return false;
		        }
			}
		}
		else
		{
			//子数据存储
			FIAboriginalGenDetailSet tFIAboriginalGenDetailSet = (FIAboriginalGenDetailSet)tSchemaSet;
			tFIAboriginalGenDetailSet = prepareAboriginaData(tFIAboriginalGenDetailSet);

			tMap.put(tFIAboriginalGenDetailSet, "INSERT");

			tInputData.add(tMap);
            if (!tPubSubmit.submitData(tInputData, ""))
            {
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                buildError("DataExtRuleEngine","saveAboriginalData", "明细数据抽取,保存在本地数据库时出错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
                tLogDeal.WriteLogTxt("明细数据抽取,保存在本地数据库时出错，提示信息为：" +tPubSubmit.mErrors.getFirstError() + enter);
                return false;
            }
			if(tFIDataBaseLinkSchema!=null)
			{
		        PubSubmitForInterface tPubSubmitForInterface = new PubSubmitForInterface();
		        if (!tPubSubmitForInterface.submitData(tInputData, tFIDataBaseLinkSchema))
		        {
		        	this.mErrors.copyAllErrors(tPubSubmitForInterface.mErrors);
		            buildError("DataExtRuleEngine","saveAboriginalData", "明细数据抽取，保存异地数据库时出错，数据源编码:"+tFIDataBaseLinkSchema.getInterfaceCode()+"，提示信息为：" +tPubSubmitForInterface.mErrors.getFirstError());
		            tLogDeal.WriteLogTxt("明细数据抽取，保存异地数据库时出错，提示信息为：" +tPubSubmitForInterface.mErrors.getFirstError() + enter);

		        	//数据回滚
		        	MMap sMap= new MMap();
		    		VData sInputData = new VData();

		    		sMap.put(tFIAboriginalGenDetailSet, "DELETE");
					
					sInputData.add(tMap);					
		        	if(tPubSubmit.submitData(sInputData, ""))
		        	{
			            buildError("DataExtRuleEngine","saveAboriginalData", "明细数据抽取，保存异地数据库出错，本地数据回滚回滚失败，提示信息为：" +tPubSubmit.mErrors.getFirstError());
			            tLogDeal.WriteLogTxt("明细数据抽取，保存异地数据库出错，本地数据回滚回滚失败!提示信息为：" +tPubSubmit.mErrors.getFirstError() + enter);		        		
			            
			            return false ;
		        	}		        	
		            return false;
		        }
			}
		}

		tMap=null;
		tInputData = null;
		
		return true;
	}
	
	/**
	 * 规则参数初始化
	 * @param tFIDataExtractRulesSchema
	 * @return
	 */
	private String prepareRule(FIDataExtractRulesSchema tFIDataExtractRulesSchema)
	{
		String tSql = "";

        //主数据抽取
        if("00".equals(tFIDataExtractRulesSchema.getSubType()))
        {	
        	
        	if("00".equals(mFIExtType))
        	{
            	tSql = tFIDataExtractRulesSchema.getMainSQL();
            	mPubCalculator.addBasicFactor("StartDate", StartDate);
        		mPubCalculator.addBasicFactor("EndDate", EndDate);
        		mPubCalculator.addBasicFactor("ManageCom", ManageCom);
        		
                mPubCalculator.setCalSql(tSql);
                tSql = mPubCalculator.calculateEx();
        	}
        	else
        	{
        		//单笔数据抽取
        		tSql = tFIDataExtractRulesSchema.getAssistSQL();
//        		mPubCalculator.addBasicFactor("StartDate", StartDate);
//        		mPubCalculator.addBasicFactor("EndDate", EndDate);
        		mPubCalculator.addBasicFactor("BusinessNo", mIndexidNo);   		
                mPubCalculator.setCalSql(tSql);
                tSql = mPubCalculator.calculateEx();        		
        	}
        }
        else
        {
        	if(
        			"E-ZS-000002".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000007".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000017".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000021".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000022".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000023".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000024".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000025".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000026".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000027".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000028".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000029".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000030".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000031".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000032".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000033".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000034".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000035".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000036".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000037".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-ZS-000038".equals(tFIDataExtractRulesSchema.getArithmeticID()) ||
        			"E-YF-000004".equals(tFIDataExtractRulesSchema.getArithmeticID()) || "E-SS-000035".equals(tFIDataExtractRulesSchema.getArithmeticID()) || "E-SS-000036".equals(tFIDataExtractRulesSchema.getArithmeticID()) || "E-SS-000037".equals(tFIDataExtractRulesSchema.getArithmeticID())
        			|| "E-SS-000038".equals(tFIDataExtractRulesSchema.getArithmeticID()) || "E-SS-000039".equals(tFIDataExtractRulesSchema.getArithmeticID()) || "E-SS-000040".equals(tFIDataExtractRulesSchema.getArithmeticID())	|| "E-YF-000021".equals(tFIDataExtractRulesSchema.getArithmeticID())	
            )
        	{
        		tSql = tFIDataExtractRulesSchema.getMainSQL();
            	mPubCalculator.addBasicFactor("StartDate", StartDate);
        		mPubCalculator.addBasicFactor("EndDate", EndDate);
        		mPubCalculator.addBasicFactor("ManageCom", ManageCom);
        		
                mPubCalculator.setCalSql(tSql);
                tSql = mPubCalculator.calculateEx();
        	}       	
        	else{
        		tSql = tFIDataExtractRulesSchema.getMainSQL();
        	}
        }
        tSql = tSql+" with ur ";
		return tSql;
	}
	
	/**
	 * 初始化生成
	 * @param tFIAboriginalMainSet
	 * @return
	 */
	private FIAboriginaInfoSet getAboriginaInfoData(FIAboriginalMainSet tFIAboriginalMainSet)
	{
		FIAboriginaInfoSet tFIAboriginaInfoSet = new FIAboriginaInfoSet();
		int size = tFIAboriginalMainSet.size();
		String[] mString = null;
		
		try {
			mString = FinCreateSerialNo.getSerialNoByFIMSerialNo(size);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 1; i <= tFIAboriginalMainSet.size(); i++)
		{
			FIAboriginalMainSchema tFIAboriginalMainSchema = tFIAboriginalMainSet.get(i);
			//tFIAboriginalMainSchema.setMSerialNo(FinCreateSerialNo.getMSerialNo());
			tFIAboriginalMainSchema.setMSerialNo(mString[i-1]);
			tFIAboriginalMainSchema.setEventNo(EventNo);
			tFIAboriginalMainSchema.setMakeDate(PubFun.getCurrentDate());
			tFIAboriginalMainSchema.setMakeTime(PubFun.getCurrentTime());
			tFIAboriginalMainSchema.setState("01");
			tFIAboriginalMainSchema.setCheckFlag("00");
			tFIAboriginalMainSchema.setOperator(mGlobalInput.Operator);
			
			//================初始化业务数据主键记录=============================
			
			FIAboriginaInfoSchema tFIAboriginaInfoSchema = new FIAboriginaInfoSchema();
			
			tFIAboriginaInfoSchema.setPhysicalTable(tFIAboriginalMainSchema.getPhysicalTable());
			tFIAboriginaInfoSchema.setKeyUnionValue(tFIAboriginalMainSchema.getKeyUnionValue());
			tFIAboriginaInfoSchema.setMSerialNo(tFIAboriginalMainSchema.getMSerialNo());
			tFIAboriginaInfoSchema.setIndexCode(tFIAboriginalMainSchema.getIndexCode());
			tFIAboriginaInfoSchema.setIndexNo(tFIAboriginalMainSchema.getIndexNo());
			tFIAboriginaInfoSchema.setEventNo(EventNo);
			tFIAboriginaInfoSchema.setMakeDate(PubFun.getCurrentDate());
			tFIAboriginaInfoSchema.setMakeTime(PubFun.getCurrentTime());
			
			tFIAboriginaInfoSet.add(tFIAboriginaInfoSchema);
		}
		
		return tFIAboriginaInfoSet;
	}

	/**
	 * 子表数据提取数据补充
	 * @param tFIAboriginalGenDetailSet
	 * @return
	 */
	private FIAboriginalGenDetailSet prepareAboriginaData(FIAboriginalGenDetailSet tFIAboriginalGenDetailSet)
	{
		int size = tFIAboriginalGenDetailSet.size();
		String[] mString = null;
		
		try {
			mString = FinCreateSerialNo.getSerialNoByFIASerialNo(size);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 1; i <= tFIAboriginalGenDetailSet.size(); i++)
		{
			FIAboriginalGenDetailSchema tFIAboriginalGenDetailSchema = tFIAboriginalGenDetailSet.get(i);
			//tFIAboriginalGenDetailSchema.setASerialNo(FinCreateSerialNo.getASerialNo());
			tFIAboriginalGenDetailSchema.setASerialNo(mString[i-1]);
			tFIAboriginalGenDetailSchema.setEventNo(EventNo);
			
			tFIAboriginalGenDetailSchema.setState("00");//数据抽取
			tFIAboriginalGenDetailSchema.setCheckFlag("00");
			tFIAboriginalGenDetailSchema.setOperator(mGlobalInput.Operator);
			tFIAboriginalGenDetailSchema.setMakeDate(PubFun.getCurrentDate());
			tFIAboriginalGenDetailSchema.setMakeTime(PubFun.getCurrentTime());	
		}
		return tFIAboriginalGenDetailSet;
	}
	
	/**
	 * 获取大数据量提取对象RSWrapper
	 * @param tFIDataBaseLinkSchema
	 * @return
	 */
	private RSWrapper getRSWrapper(FIDataBaseLinkSchema tFIDataBaseLinkSchema)
	{
		RSWrapper rsWrapper = null;
		DBConn con = null;
		if(tFIDataBaseLinkSchema==null)
		{
			rsWrapper = new RSWrapper();
		}	
		else
		{	//异地数据源
			try 
			{
				con = DBConnPool.getConnection(tFIDataBaseLinkSchema);
			} 
			catch (Exception e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				buildError("DataExtRuleEngine","getRSWrapper", "批量数据抽取对象RSWrapper初始化时出错，数据源："+tFIDataBaseLinkSchema.getInterfaceCode());
				tLogDeal.WriteLogTxt("批量数据抽取对象RSWrapper初始化时出错，数据源："+tFIDataBaseLinkSchema.getInterfaceCode() + enter);
				return null;
			}
			
			rsWrapper = new RSWrapper(con);
		}
		return rsWrapper;
	}
	/**
	 * 获取规则执行数据源
	 * @return
	 */
	private FIDataBaseLinkSchema getDBSource(FIDataExtractRulesSchema tFIDataExtractRulesSchema)
	{
        FIDataBaseLinkDB tFIDataBaseLinkDB = new FIDataBaseLinkDB();
        String strKeyDefSQL = "select * from FIDataBaseLink where InterfaceCode ='"+tFIDataExtractRulesSchema.getDataSource()+"' with ur ";
        FIDataBaseLinkSet tFIDataBaseLinkSet = tFIDataBaseLinkDB.executeQuery(strKeyDefSQL);
        if(tFIDataBaseLinkSet == null || tFIDataBaseLinkSet.size()==0)
        {
            return null;
        }		
		return tFIDataBaseLinkSet.get(1);
	}
	
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}

}
