package com.sinosoft.lis.fininterface_v3.core.ext;

import com.sinosoft.lis.db.FIDataBaseLinkDB;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.fininterface_v3.tools.dblink.DBConn;
import com.sinosoft.lis.fininterface_v3.tools.dblink.DBConnPool;
import com.sinosoft.lis.fininterface_v3.tools.dblink.PubSubmitForInterface;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIDataBaseLinkSchema;
import com.sinosoft.lis.schema.FIDataExtractRulesSchema;
import com.sinosoft.lis.vschema.FIAboriginalGenDetailSet;
import com.sinosoft.lis.vschema.FIAboriginalMainSet;
import com.sinosoft.lis.vschema.FIDataBaseLinkSet;
import com.sinosoft.lis.vschema.FIDataExtractRulesSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.VData;
/**
 * <p>Title: 数据补充规则引擎</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class DataExtAddRuleEngine {

	public CErrors mErrors = new CErrors();	
	
	GlobalInput mGlobalInput = new GlobalInput();
	
//	private TransferData mTransferData ;
	
	private PubCalculator mPubCalculator = new PubCalculator();
    
	private FIDataExtractRulesSet mFIDataExtractRulesSet ;
	
	/** 日志处理*/
	private FIOperationLog tLogDeal = null;
	
	private String mRuleType = "" ;
	private String mSubRuleType = "" ;

	/** 事件流水号*/
    private String EventNo = "";
//    /** 提取数据目标表*/
//    private String PhysicalTable = "";
//    private String IndexCode ;
//    private String IndexNo ;
//    /** 附加执行条件*/
//    private String Condition; 
//    private String OperatType = "";
    
    private final String enter = "\r\n"; // 换行符
    
    public DataExtAddRuleEngine(FIOperationLog logDeal)
    {
    	tLogDeal = logDeal;
    	EventNo = tLogDeal.getEventNo();
    }

	public boolean ruleService(FIDataExtractRulesSet tFIDataExtractRulesSet,
			VData param) {
		// TODO Auto-generated method stub
    	mFIDataExtractRulesSet = tFIDataExtractRulesSet;
    	
    	if (!getInputData(param))
        {
			buildError("DataExtAddRuleEngine","ruleService", "获取参数出错！！");
            tLogDeal.WriteLogTxt("获取参数出错！！" + enter);
    		return false;
        }

    	if (!getCheckData())
        {
			buildError("DataExtAddRuleEngine","ruleService", "数据校检出错！！");
            tLogDeal.WriteLogTxt("数据校检出错！！" + enter);
    		return false;
        }
    	
    	//执行规则
        dealRuels();

        return true;
	}
    
    /**
     * 获取参数
     * @param cInputData
     * @return
     */
    private boolean getInputData(VData cInputData)
    {
    	
    	return true;
    }
 
    /**
     * 数据校检
     * @return
     */
    private boolean getCheckData()
    {
    	if("".equals(EventNo)||EventNo==null)
    	{
			buildError("DataExtAddRuleEngine","getCheckData", "事件编码为空！！");
            tLogDeal.WriteLogTxt("事件编码为空！！" + enter);
            return false;
    	}
    	
    	return true;
    }


    /**
     * 批量执行规则
     */
    private void dealRuels()
    {
		for(int i=1;i <=mFIDataExtractRulesSet.size();i++)
		{
			FIDataExtractRulesSchema tFIDataExtractRulesSchema = mFIDataExtractRulesSet.get(i);
			FIDataBaseLinkSchema tFIDataBaseLinkSchema= getDBSource(tFIDataExtractRulesSchema);
			
			mRuleType = tFIDataExtractRulesSchema.getArithmeticType();
			mSubRuleType = tFIDataExtractRulesSchema.getSubType();
			
			if(!"00".equals(mRuleType))
			{
				buildError("DataExtAddRuleEngine","dealRuels", "规则类型错误，规则编码："+tFIDataExtractRulesSchema.getArithmeticID());
                tLogDeal.WriteLogTxt("规则类型错误，规则编码："+tFIDataExtractRulesSchema.getArithmeticID() + enter);		
				break;				
			}

			if(!"02".equals(mSubRuleType)&&!"03".equals(mSubRuleType))
			{
				buildError("DataExtAddRuleEngine","dealRuels", "规则明细类型错误，规则编码："+tFIDataExtractRulesSchema.getArithmeticID());
                tLogDeal.WriteLogTxt("规则明细类型错误，规则编码："+tFIDataExtractRulesSchema.getArithmeticID() + enter);		
				break;				
			}
			
			if(tFIDataBaseLinkSchema==null)
			{
				if(!localRuleEXE(tFIDataExtractRulesSchema))
				{
					buildError("DataExtAddRuleEngine","dealRuels", "数据补充规则执行出错，规则编码："+tFIDataExtractRulesSchema.getArithmeticID());
		            tLogDeal.WriteLogTxt("数据补充规则执行出错，规则编码："+tFIDataExtractRulesSchema.getArithmeticID()+ enter);
				}
			}
			else
			{
				if(!nonlocalRuleEXE(tFIDataExtractRulesSchema,tFIDataBaseLinkSchema))
				{
					buildError("DataExtAddRuleEngine","dealRuels", "数据补充规则执行出错，规则编码："+tFIDataExtractRulesSchema.getArithmeticID());
		            tLogDeal.WriteLogTxt("数据补充规则执行出错，规则编码："+tFIDataExtractRulesSchema.getArithmeticID()+ enter);					
				}
			}
		}
    }
    
    /**
     * 本地数据源规则执行
     * @return
     */
    private boolean localRuleEXE(FIDataExtractRulesSchema tFIDataExtractRulesSchema)
    {
		MMap tMap = new MMap();
		VData tInputData = new VData();
		PubSubmit tPubSubmit = new PubSubmit();
		String update = tFIDataExtractRulesSchema.getMainSQL();

		tMap.put(update, "UPDATE");	

		tInputData.add(tMap);
        if (!tPubSubmit.submitData(tInputData, ""))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buildError("DataExtAddRuleEngine","localRuleEXE", "数据信息补充出错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("数据信息补充出错，提示信息为：" +tPubSubmit.mErrors.getFirstError() + enter);
            return false;
        }      
        
    	return true;
    }
    
    /**
     * 异地数据源规则执行
     * @return
     */
    private boolean nonlocalRuleEXE(FIDataExtractRulesSchema tFIDataExtractRulesSchema,FIDataBaseLinkSchema tFIDataBaseLinkSchema)
    {
		MMap tMap = new MMap();
		VData tInputData = new VData();

		String update = tFIDataExtractRulesSchema.getMainSQL();

		tMap.put(update, "UPDATE");
		tInputData.add(tMap);
        PubSubmitForInterface tPubSubmitForInterface = new PubSubmitForInterface();
        if (!tPubSubmitForInterface.submitData(tInputData, tFIDataBaseLinkSchema))
        {
        	this.mErrors.copyAllErrors(tPubSubmitForInterface.mErrors);
            buildError("DataExtAddRuleEngine","updateData", "异地执行数据补充规则时出错，数据源编码:"+tFIDataBaseLinkSchema.getInterfaceCode()+"，提示信息为：" +tPubSubmitForInterface.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("异地执行数据补充规则时出错，提示信息为：" +tPubSubmitForInterface.mErrors.getFirstError() + enter);

            return false;
        }
		//更新数据
        updateData( tFIDataExtractRulesSchema, tFIDataBaseLinkSchema);
        
    	return true;
    }

	/**
	 * 执行数据补充规则
	 * @param tFIAboriginalMainSet
	 * @param tFIDataBaseLinkSchema
	 * @return
	 */
	private boolean updateData(FIDataExtractRulesSchema tFIDataExtractRulesSchema,FIDataBaseLinkSchema tFIDataBaseLinkSchema)
	{
		if(tFIDataBaseLinkSchema==null)
		{
			return false;
		}
        RSWrapper  rsWrapper = getRSWrapper(tFIDataBaseLinkSchema);
        PubSubmit tPubSubmit = new PubSubmit();

        //获取SQL规则
        String tSelectSql = "select * "+tFIDataExtractRulesSchema.getAssistSQL();
        
        String tSubRuleType = tFIDataExtractRulesSchema.getSubType();
        SchemaSet tSchemaSet = null;
        
		if("02".equals(tSubRuleType))
		{
			tSchemaSet = new FIAboriginalMainSet();  //主数据补充
		}
		else if("03".equals(tSubRuleType))
		{
			tSchemaSet = new FIAboriginalGenDetailSet();//子数据补充
		}
		else
		{
			return false;
		}
        
		if (!rsWrapper.prepareData(tSchemaSet, tSelectSql+" with ur "))
		{
			mErrors.copyAllErrors(rsWrapper.mErrors);
			System.out.println(rsWrapper.mErrors.getFirstError());
			buildError("DataExtRuleEngine","getDataExt", "批量数据抽取时出错，规则编码："+tFIDataExtractRulesSchema.getArithmeticID()+",提示信息为：" +rsWrapper.mErrors.getFirstError());
			tLogDeal.WriteLogTxt("批量数据抽取时出错，规则编码："+tFIDataExtractRulesSchema.getArithmeticID()+",提示信息为：" +rsWrapper.mErrors.getFirstError() + enter);
			tLogDeal.buildErrorLog("批量数据抽取时出错，规则编码："+tFIDataExtractRulesSchema.getArithmeticID()+"，执行出现错误,需要 重新补提数据！");
			return false;
		}
		do
		{
			rsWrapper.getData();
			if (tSchemaSet != null && tSchemaSet.size() > 0)
			{
         		VData tInputData = new VData();
        		MMap tMap= new MMap();
        		String tDeleteSql = "delete "+tFIDataExtractRulesSchema.getAssistSQL();
        		tMap.put(tDeleteSql, "DELETE");
        		tMap.put(tSchemaSet, "INSERT");
        		tInputData.add(tMap);
        		
                if (!tPubSubmit.submitData(tInputData, ""))
                {
                    this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                    buildError("DataExtAddRuleEngine","updateData", "本地执行数据补充规则时出错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
                    tLogDeal.WriteLogTxt("本地执行数据补充规则执出错，提示信息为：" +tPubSubmit.mErrors.getFirstError() + enter);
                    return false;
                }
                tInputData = null;
                tMap = null;
             }        
         }while(tSchemaSet != null && tSchemaSet.size() > 0);

		rsWrapper.close();	
		return true;
	}

	/**
	 * 获取大数据量提取对象RSWrapper
	 * @param tFIDataBaseLinkSchema
	 * @return
	 */
	private RSWrapper getRSWrapper(FIDataBaseLinkSchema tFIDataBaseLinkSchema)
	{
		RSWrapper rsWrapper = null;
		DBConn con = null;
		if(tFIDataBaseLinkSchema==null)
		{
			rsWrapper = new RSWrapper();
		}	
		else
		{	//异地数据源
			try 
			{
				con = DBConnPool.getConnection(tFIDataBaseLinkSchema);
			} 
			catch (Exception e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				buildError("DataExtRuleEngine","getRSWrapper", "批量数据抽取对象RSWrapper初始化时出错，数据源："+tFIDataBaseLinkSchema.getInterfaceCode());
				tLogDeal.WriteLogTxt("批量数据抽取对象RSWrapper初始化时出错，数据源："+tFIDataBaseLinkSchema.getInterfaceCode() + enter);
				return null;
			}
			
			rsWrapper = new RSWrapper(con);
		}
		return rsWrapper;
	}
	/**
	 * 获取规则执行数据源
	 * @return
	 */
	private FIDataBaseLinkSchema getDBSource(FIDataExtractRulesSchema tFIDataExtractRulesSchema)
	{
        FIDataBaseLinkDB tFIDataBaseLinkDB = new FIDataBaseLinkDB();
        String strKeyDefSQL = "select * from FIDataBaseLink where InterfaceCode ='"+tFIDataExtractRulesSchema.getDataSource()+"' with ur ";
        FIDataBaseLinkSet tFIDataBaseLinkSet = tFIDataBaseLinkDB.executeQuery(strKeyDefSQL);
        if(tFIDataBaseLinkSet == null || tFIDataBaseLinkSet.size()==0)
        {
            return null;
        }		
		return tFIDataBaseLinkSet.get(1);
	}
	
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MMap tMap = new MMap();
		VData tInputData = new VData();
		PubSubmit tPubSubmit = new PubSubmit();
		
		tMap.put("update FIAboriginalGenDetail b set (payintv, cvalidate, salechnl, CustomerNo) = (select a.payintv, a.cvalidate, a.salechnl, a.customerno from LCGrpPol a where b.subphysicaltable = 'LJAPayGrp' and a.grppolno = b.polno) where exists(select 1 from LCGrpPol a where a.grppolno = b.polno and b.subphysicaltable = 'LJAPayGrp')", "UPDATE");
		
		tInputData.add(tMap);
        if (!tPubSubmit.submitData(tInputData, ""))
        {
        	System.out.println("=================");
        }   
	}
}
