package com.sinosoft.lis.fininterface_v3.core.ext;

import com.sinosoft.lis.db.FIDataBaseLinkDB;
import com.sinosoft.lis.db.FIDataExtractDefDB;
import com.sinosoft.lis.db.FIDataExtractRulesDB;
import com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog;
import com.sinosoft.lis.fininterface_v3.tools.dblink.PubSubmitForInterface;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FIDataBaseLinkSchema;
import com.sinosoft.lis.schema.FIDataExtractRulesSchema;
import com.sinosoft.lis.vschema.FIDataBaseLinkSet;
import com.sinosoft.lis.vschema.FIDataExtractDefSet;
import com.sinosoft.lis.vschema.FIDataExtractRulesSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * <p>Title: 数据提取服务类</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: </p>
 *
 * @author caosg
 * @version 3.0
 */
public class FIDataExtractService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    GlobalInput mGlobalInput = new GlobalInput();
    
    private TransferData mTransferData ;
    
    private String mIndexType = "";
    private final String enter = "\r\n"; // 换行符
    private FIOperationLog tLogDeal = null;
    private FIDataExtractDefSet mFIDataExtractDefSet ;
    //数据提取
    private DataExtRuleEngine mDataExtRuleEngine ;
    //数据补充
    private DataExtAddRuleEngine mDataExtAddRuleEngine;
	
    public FIDataExtractService(FIOperationLog logDeal)
    {
        tLogDeal = logDeal;
        mDataExtRuleEngine = new DataExtRuleEngine(tLogDeal);
        mDataExtAddRuleEngine = new DataExtAddRuleEngine(tLogDeal);
    }
    /**
	 * 提取所有定义的业务交易数据
	 * @param param
	 * @return
	 */		
	public boolean extractAll(VData param)
	{
		String tSQL = "select * from FIDataExtractDef where RuleState = '1' and RuleType = '00' with ur ";
		
		FIDataExtractDefSet tFIDataExtractDefSet = new FIDataExtractDefDB().executeQuery(tSQL);

		String tRuleDefID = "";
		
		if(tFIDataExtractDefSet!=null &&tFIDataExtractDefSet.size()>0)
		{
			for (int i=1 ;i<=tFIDataExtractDefSet.size();i++)
			{					
				tRuleDefID = tFIDataExtractDefSet.get(i).getRuleDefID();
				//主数据提取
				dealAboriginalData( tRuleDefID,param);		
				//明细数据提取				
				dealAboriginalDetail(tRuleDefID,param);	
				//更新数据状态
				changeState(tRuleDefID);
				
				//数据补充
				dealAboriginalDataAdd(tRuleDefID,param);
			}
		}

		return true;
	}
	/**
	 * 通过算法定义提取数据
	 * @param param
	 * @return
	 */	
	public boolean extractByRuleDef(VData param)
	{
		System.out.println("Start Time:"+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
		if(!getInputData(param))
		{
			return false;
		}
		if(mFIDataExtractDefSet==null||mFIDataExtractDefSet.size()<1)
		{
			// @@错误处理
			buildError("FIDataExtractService","extractByRuleDef", "前台传输数据失败,未获得数据提取规则!");
			tLogDeal.WriteLogTxt("前台传输数据失败,未获得数据提取规则!" + enter);
			return false;
		}
		
		FIDataExtractDefSet tFIDataExtractDefSet = mFIDataExtractDefSet;
		String tRuleDefID = "";
		
		if(tFIDataExtractDefSet!=null &&tFIDataExtractDefSet.size()>0)
		{
			for (int i=1 ;i<=tFIDataExtractDefSet.size();i++)
			{					
				tRuleDefID = tFIDataExtractDefSet.get(i).getRuleDefID();
				//主数据提取
				dealAboriginalData( tRuleDefID,param);		
				//明细数据提取				
				dealAboriginalDetail(tRuleDefID,param);	
				//更新数据状态
				changeState(tRuleDefID);
				
				//数据补充
				dealAboriginalDataAdd(tRuleDefID,param);	
			}
		}
		System.out.println("End Time:"+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
		return true;
	}
	
	/**
	 * 通过主业务交易号码，单笔提取业务数据
	 * @param param
	 * @return
	 */
	public boolean extractByIndexNo(VData param)
	{
		if(!getInputData(param))
		{
			return false;
		}
		
		String tSQL = "select * from FIDataExtractDef where RuleState = '1' and RuleType = '00' and IndexCode= '"+mIndexType+"' with ur ";
		
		FIDataExtractDefSet tFIDataExtractDefSet = new FIDataExtractDefDB().executeQuery(tSQL);
		
		String tRuleDefID = "";
		
		if(tFIDataExtractDefSet!=null &&tFIDataExtractDefSet.size()>0)
		{
			for (int i=1 ;i<=tFIDataExtractDefSet.size();i++)
			{									
				tRuleDefID = tFIDataExtractDefSet.get(i).getRuleDefID();
				//主数据提取
				dealAboriginalData( tRuleDefID,param);		
				//明细数据提取				
				dealAboriginalDetail(tRuleDefID,param);		
				
				//更新数据状态
				changeState(tRuleDefID);
				
				//数据补充
				dealAboriginalDataAdd(tRuleDefID,param);
				
			}
		}
		
		return true;
	}
	
	 /**
	  * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 *
	 */
	private boolean getInputData(VData cInputData)
	{
		mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0);
		mFIDataExtractDefSet = (FIDataExtractDefSet)cInputData.getObjectByObjectName("FIDataExtractDefSet",0);
		mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData",0);
			
		if (mGlobalInput == null) 
		{
           // @@错误处理
           buildError("FIDataExtractService","getInputData", "前台传输全局公共数据失败!");
           tLogDeal.WriteLogTxt("数据提取获取参数出错！！" + enter);
           return false;
		}
		if (mTransferData == null) 
		{
           buildError("FIDataExtractService","getInputData", "前台传输数据失败!");
           tLogDeal.WriteLogTxt("数据提取获取参数出错！！" + enter);
           return false;
		}
		
		mIndexType = (String) mTransferData.getValueByName("IndexType");
		
		return true;
	}
	/**
	 * 主数据提取
	 * @param tRuleDefID
	 * @param param
	 * @return
	 */
	private boolean dealAboriginalData(String tRuleDefID,VData param)
	{
		FIDataExtractRulesSet tFIDataExtractRulesSet = null;
		if(tRuleDefID==null)
			tFIDataExtractRulesSet = getMainRules();
		else
			tFIDataExtractRulesSet = getMainRules(tRuleDefID);

		if(tFIDataExtractRulesSet==null||tFIDataExtractRulesSet.size()<1)
		{
			buildError("FIDataExtractService","dealAboriginalData", "主数据提取,未获得数据提取规则!");
			tLogDeal.WriteLogTxt("主数据提取,未获得数据提取规则编码为："+tRuleDefID +"的规则定义"+ enter);
			return false;
		}
		//数据提取
		mDataExtRuleEngine.ruleService(tFIDataExtractRulesSet, param);
		
		return true;
	}
	
	/**
	 * 明细数据提取
	 * @param tRuleDefID
	 * @param param
	 * @return
	 */
	private boolean dealAboriginalDetail(String tRuleDefID,VData param)
	{
		FIDataExtractRulesSet tFIDataExtractRulesSet = null;
		
		if(tRuleDefID==null)
			tFIDataExtractRulesSet = getSubRules();
		else
			tFIDataExtractRulesSet = getSubRules(tRuleDefID);
		
		if(tFIDataExtractRulesSet==null||tFIDataExtractRulesSet.size()<1)
		{
			buildError("FIDataExtractService","dealAboriginalDetail", "明细数据提取,未获得数据提取规则!");
			tLogDeal.WriteLogTxt("明细数据提取,未获得数据提取规则编码为："+tRuleDefID +"的规则定义"+ enter);
			return false;
		}
		//数据提取
		mDataExtRuleEngine.ruleService(tFIDataExtractRulesSet, param);
		
		return true;
	}
	

	/**
	 * 数据补充
	 * @param tRuleDefID
	 * @param param
	 * @return
	 */
	private boolean dealAboriginalDataAdd(String tRuleDefID,VData param)
	{
		FIDataExtractRulesSet tFIDataExtractRulesSet = null;
		if(tRuleDefID==null)
			tFIDataExtractRulesSet = getMainAddRules();
		else
			tFIDataExtractRulesSet = getMainAddRules(tRuleDefID);
		
		if(tFIDataExtractRulesSet==null||tFIDataExtractRulesSet.size()<1)
		{
			buildError("FIDataExtractService","dealAboriginalDataAdd", "数据补充,未获得算法规则!");
			tLogDeal.WriteLogTxt("数据补充,未获得规则编码为："+tRuleDefID +"的规则定义"+ enter);
			return false;
		}
		//数据补充
		mDataExtAddRuleEngine.ruleService(tFIDataExtractRulesSet, param);
			
		return true;
	}	
	
	/**
	 * 主数据提取规则
	 * @return
	 */
	private FIDataExtractRulesSet getMainRules()
	{
		String tSQL = "select * from FIDataExtractRules where state = '1' and ArithmeticType = '00' and SubType = '00' order by RuleDefID,BuType,Dealorder with ur ";
		
		FIDataExtractRulesDB tFIDataExtractRulesDB = new FIDataExtractRulesDB();
		FIDataExtractRulesSet tFIDataExtractRulesSet  = tFIDataExtractRulesDB.executeQuery(tSQL);
		
		return tFIDataExtractRulesSet;
	}

	/**
	 * 主数据提取规则
	 * @param RuleDefID
	 * @return
	 */
	private FIDataExtractRulesSet getMainRules(String RuleDefID)
	{
		String tSQL = "select * from FIDataExtractRules where state = '1' and ArithmeticType = '00' and SubType = '00' and RuleDefID = '"+RuleDefID+"' order by BuType,Dealorder with ur ";
		
		FIDataExtractRulesDB tFIDataExtractRulesDB = new FIDataExtractRulesDB();
		FIDataExtractRulesSet tFIDataExtractRulesSet  = tFIDataExtractRulesDB.executeQuery(tSQL);
		
		return tFIDataExtractRulesSet;
	}
	
	/**
	 * 子数据提取规则
	 * @return
	 */	
	private FIDataExtractRulesSet getSubRules()
	{
		String tSQL = "select * from FIDataExtractRules where state = '1' and ArithmeticType = '00' and SubType = '01' order by RuleDefID,BuType,DealOrder with ur ";

		FIDataExtractRulesDB tFIDataExtractRulesDB = new FIDataExtractRulesDB();
		FIDataExtractRulesSet tFIDataExtractRulesSet  = tFIDataExtractRulesDB.executeQuery(tSQL);
		
		return tFIDataExtractRulesSet;
	}

	/**
	 * 子数据提取规则
	 * @param RuleDefID
	 * @return
	 */	
	private FIDataExtractRulesSet getSubRules(String RuleDefID)
	{
		String tSQL = "select * from FIDataExtractRules where state = '1' and ArithmeticType = '00' and SubType = '01' and RuleDefID = '"+RuleDefID+"' order by BuType,DealOrder with ur ";

		FIDataExtractRulesDB tFIDataExtractRulesDB = new FIDataExtractRulesDB();
		FIDataExtractRulesSet tFIDataExtractRulesSet  = tFIDataExtractRulesDB.executeQuery(tSQL);
		
		return tFIDataExtractRulesSet;
	}
	
	/**
	 * 数据信息补充提取规则
	 * @return
	 */
	private FIDataExtractRulesSet getMainAddRules()
	{
		String tSQL = "select * from FIDataExtractRules where state = '1' and ArithmeticType = '00' and SubType in('02','03') order by RuleDefID,DealOrder with ur ";

		FIDataExtractRulesDB tFIDataExtractRulesDB = new FIDataExtractRulesDB();
		FIDataExtractRulesSet tFIDataExtractRulesSet  = tFIDataExtractRulesDB.executeQuery(tSQL);
		
		return tFIDataExtractRulesSet;
	}

	/**
	 * 数据信息补充提取规则
	 * @param RuleDefID  9000000000(公共补充规则)
	 * @return
	 */
	private FIDataExtractRulesSet getMainAddRules(String RuleDefID)
	{
		String tSQL = "select * from FIDataExtractRules where state = '1' and ArithmeticType = '00' and SubType in('02','03') and RuleDefID in('"+RuleDefID+"','9000000000' )order by RuleDefID,DealOrder with ur ";

		FIDataExtractRulesDB tFIDataExtractRulesDB = new FIDataExtractRulesDB();
		FIDataExtractRulesSet tFIDataExtractRulesSet  = tFIDataExtractRulesDB.executeQuery(tSQL);
		
		return tFIDataExtractRulesSet;
	}
	
	private boolean changeState(String tRuleDefID)
	{
		MMap tMap= new MMap();
		VData tInputData = new VData();
		PubSubmit tPubSubmit = new PubSubmit();
		
		FIDataBaseLinkSchema tFIDataBaseLinkSchema = getDBSource(tRuleDefID);
		
		String updateSql = "update FIAboriginalMain a set State='02' where State='01' and exists(select 1 from FIAboriginalGenDetail b where a.MSerialNo = b.MSerialNo)";
		//一笔业务多缴费方式
		String updateSql2 = "update FIAboriginalMain a set State='02' where State='01' and PhysicalTable = 'LJTempFeeClass' and exists(select 1 from FIAboriginalGenDetail b where a.IndexCode = b.IndexCode and a.IndexNo = b.IndexNo)";
		tMap.put(updateSql, "UPDATE");
		tMap.put(updateSql2, "UPDATE");
		
		tInputData.add(tMap);
        if (!tPubSubmit.submitData(tInputData, ""))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            buildError("DataExtRuleEngine","saveAboriginalData", "数据提取后更新数据状态时出错，提示信息为：" +tPubSubmit.mErrors.getFirstError());
            tLogDeal.WriteLogTxt("数据提取后更新数据状态时出错，提示信息为：" +tPubSubmit.mErrors.getFirstError() + enter);
            return false;
        }
		if(tFIDataBaseLinkSchema!=null)
		{
	        PubSubmitForInterface tPubSubmitForInterface = new PubSubmitForInterface();
	        if (!tPubSubmitForInterface.submitData(tInputData, tFIDataBaseLinkSchema))
	        {		    		
	        	MMap sMap= new MMap();
	    		VData sInputData = new VData();
	        	String backSql1 = "update FIAboriginalMain a set State='01' where State='02' and not exists(select 1 from FIAboriginalGenDetail b where a.MSerialNo = b.MSerialNo)";

				sMap.put(backSql1, "UPDATE");
			
	        	if(tPubSubmit.submitData(sInputData, ""))
	        	{
		            buildError("DataExtRuleEngine","saveAboriginalData", "数据提取后更新数据状态时出错2，提示信息为：" +tPubSubmit.mErrors.getFirstError());
		            tLogDeal.WriteLogTxt("数据提取后更新数据状态时出错2!提示信息为：" +tPubSubmit.mErrors.getFirstError() + enter);		        		
		            
		            return false ;
	        	}	
	        }
		}
		tMap=null;
		tInputData = null;
		return true;
	}
	
	/**
	 * 获取规则执行数据源
	 * @return
	 */
	private FIDataBaseLinkSchema getDBSource(String tRuleDefID)
	{
		String sql = "select * from FIDataExtractRules where ArithmeticType='00' and SubType='00' and RuleDefID='"+tRuleDefID+"' and state='1' with ur ";
		FIDataExtractRulesSet tFIDataExtractRulesSet= new FIDataExtractRulesDB().executeQuery(sql);
		
		if(tFIDataExtractRulesSet==null||tFIDataExtractRulesSet.size()<1)
		{
			return null;
		}
		FIDataExtractRulesSchema tFIDataExtractRulesSchema = tFIDataExtractRulesSet.get(1);
		
		FIDataBaseLinkDB tFIDataBaseLinkDB = new FIDataBaseLinkDB();
        String strKeyDefSQL = "select * from FIDataBaseLink where InterfaceCode ='"+tFIDataExtractRulesSchema.getDataSource()+"' with ur ";
        FIDataBaseLinkSet tFIDataBaseLinkSet = tFIDataBaseLinkDB.executeQuery(strKeyDefSQL);
        if(tFIDataBaseLinkSet == null || tFIDataBaseLinkSet.size()==0)
        {
            return null;
        }		
		return tFIDataBaseLinkSet.get(1);
	}
	
	/**
	 * 错误信息
	 * @param szModuleName
	 * @param szFunc
	 * @param szErrMsg
	 */
    private void buildError(String szModuleName, String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = szModuleName;
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
    
	public static void main(String[] args)
	{

	}
}
