package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;



public class FIVoucherBnBL 
{

	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	private String mPageFlag;
	private String voucherno;
	
	private MMap mMMap = new MMap();
	
	private TransferData mTransferData = new TransferData();
	
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	
	private FIVoucherBnSchema mFIVoucherTypeBusSchema = new FIVoucherBnSchema();
	private FMVoucherBnSchema tFMVoucherTypeBusSchema = new FMVoucherBnSchema();
	private FIVoucherBnSchema oFIVoucherTypeBusSchema = new FIVoucherBnSchema();

	/**
	 * 传输数据的公共方法
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */

	public boolean submitData(VData cInputData, String cOperate)
	{
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData))
		{
			return false;
		}
		if (!checkData())
		{
			return false;
		}
		// 进行业务处理
		if (!dealData())
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIVoucherFeeDefBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败FIVoucherFeeDefBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		// 准备往后台的数据
		if (!prepareOutputData())
		{
			return false;
		}

		if (!pubSubmit())
		{
			return false;
		}

		return true;
	}
	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData)
	{
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		voucherno = (String)cInputData.get(1);
		mFIVoucherTypeBusSchema =  (FIVoucherBnSchema) cInputData.getObjectByObjectName("FIVoucherBnSchema", 0);
		
		return true;
	}
	
	
	private boolean checkData()
	{

		if(mFIVoucherTypeBusSchema==null)
		{
			
			return false;
		}
		if("".equals(mOperate)||mOperate==null)
		{
			return false;
		}
		return true;
	}
	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 * @return
	 */
	private boolean dealData()
	{		
		if ("INSERT".equals(mOperate))
		{
			System.out.println("进入新增模块！");
			mMMap.put(mFIVoucherTypeBusSchema, "INSERT");
		}

		else if ("UPDATE".equals(mOperate))
		{

			System.out.println("进入修改模块！");
			CheckFMTable();
			mMMap.put(mFIVoucherTypeBusSchema, "UPDATE");

		}

		else if ("DELETE".equals(mOperate))
		{

			System.out.println("进入删除模块！");
			SSRS tSSRS = new SSRS();
			ExeSQL es = new ExeSQL();
			FIVoucherBnFeeSet tFIFeeTypeDefSet = new FIVoucherBnFeeSet();
			 
			tSSRS = es.execSQL("select costid from FIBnFeeTypeDef where BusinessID='"+mFIVoucherTypeBusSchema.getBusinessID()+"'");
			for(int i=1;i<=tSSRS.MaxRow;i++){
				
					FIVoucherBnFeeSchema tFIFeeTypeDefSchema = new FIVoucherBnFeeSchema();
	            	tFIFeeTypeDefSchema.setVersionNo(mFIVoucherTypeBusSchema.getVersionNo());
	    			tFIFeeTypeDefSchema.setVoucherID(mFIVoucherTypeBusSchema.getVoucherID());
	    			tFIFeeTypeDefSchema.setBusinessID(mFIVoucherTypeBusSchema.getBusinessID());
	    			tFIFeeTypeDefSchema.setFeeID(tSSRS.GetText(i, 1));
	    			tFIFeeTypeDefSet.add(tFIFeeTypeDefSchema);
	    			
	            }
			
	    	mMMap.put(tFIFeeTypeDefSet, "DELETE");
			CheckFMTable();
			mMMap.put(mFIVoucherTypeBusSchema, "DELETE");

		}

		return true;
	}	
	
	/**
	 * 判断是否需要往FM表中插入数据 
	 * */
	private void CheckFMTable()
	{
	
		
		FIVoucherBnDB mFIVoucherTypeBusDB = new FIVoucherBnDB();
		oFIVoucherTypeBusSchema = mFIVoucherTypeBusDB.executeQuery("select * from FIVoucherBn where voucherid='"+mFIVoucherTypeBusSchema.getVoucherID()+"'").get(1);
		//查询FM表中是否有数据
		String tMaintNo = PubFun1.CreateMaxNo("MaintNo_TypeBus",20);
	  	System.out.println("流水号作业：修改记录编号" + tMaintNo);
	  	tFMVoucherTypeBusSchema.setMaintNo(tMaintNo);
		tFMVoucherTypeBusSchema.setVoucherID(oFIVoucherTypeBusSchema.getVoucherID());
		tFMVoucherTypeBusSchema.setVersionNo(oFIVoucherTypeBusSchema.getVersionNo());
		tFMVoucherTypeBusSchema.setVoucherName(oFIVoucherTypeBusSchema.getVoucherName());
		tFMVoucherTypeBusSchema.setBusinessID(oFIVoucherTypeBusSchema.getBusinessID());
		tFMVoucherTypeBusSchema.setBusinessName(oFIVoucherTypeBusSchema.getBusinessName());
			
			
			mMMap.put(tFMVoucherTypeBusSchema, "INSERT");
		
		
	}
	/**
	 * 准备需要保存的数据
	 * @return boolean
	 */
	private boolean prepareOutputData()
	{
		try
		{
			mInputData.clear();
			mInputData.add(mMMap);
		}
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIVoucherFeeDefBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	/**
	 * 提交数据
	 * @return
	 */
	private boolean pubSubmit()
	{
		// 进行数据提交
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, ""))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "FIVoucherFeeDefBL";
			tError.functionName = "PubSubmit.submitData";
			tError.errorMessage = "数据提交失败FIVoucherFeeDefBL-->pubSubmit!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}

	public VData getResult()
	{
		return this.mResult;
	}

	public static void main(String[] args)
	{

	}	
}
