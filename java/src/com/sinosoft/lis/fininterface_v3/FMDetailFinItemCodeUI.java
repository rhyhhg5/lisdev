package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.db.FIDetailFinItemCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * <p>
 * ClassName: FIDetailFinItemCodeUI
 * </p>
 * <p>
 * Description: 财务接口-财务规则参数管理-明细科目分支影射定义UI
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: sinosoft
 * </p>
 * @Database: 财务接口
 * @author：董健
 * @version：1.0
 * @CreateDate：2008-08-11
 */

public class FMDetailFinItemCodeUI 
{

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	/** 业务处理相关变量 */
	private FMDetailFinItemCodeSchema mFMDetailFinItemCodeSchema = new FMDetailFinItemCodeSchema();

	//private FIDetailFinItemCodeSet mFIDetailFinItemCodeSet = new FIDetailFinItemCodeSet();

	private FMDetailFinItemCodeBL mFMDetailFinItemCodeBL = new FMDetailFinItemCodeBL();

	private String mVersionNo = "";
	private String mFinItemID = "";
	private String mJudgementNo = "";
	private String mLevelConditionValue = "";	
	

	public FMDetailFinItemCodeUI()
	{}
	
	public String getVersionNo()
	{
		return mVersionNo;
	}	

	public String getFinItemID()
	{
		return mFinItemID;
	}
	
	public String getJudgementNo()
	{
		return mJudgementNo;
	}
	
	public String getLevelConditionValue()
	{
		return mLevelConditionValue;
	}
	

	/**
	 * 传输数据的公共方法
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate)
	{
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		System.out.println("getInputData");
		if (!getInputData(cInputData))
		{
			return false;
		}

		System.out.println("dealData");
		if (!dealData())
		{
			return false;
		}

		// 准备往后台的数据
		if (!prepareOutputData())
		{
			return false;
		}
		System.out.println("Start FMDetailFinItemCodeUI Submit...");
		System.out.println("VersionNo:" + mFMDetailFinItemCodeSchema.getVersionNo());
		System.out.println("FinItemID:" + mFMDetailFinItemCodeSchema.getFinItemID());
		System.out.println("AssociatedID:" + mFMDetailFinItemCodeSchema.getJudgementNo());

		mFMDetailFinItemCodeBL.submitData(mInputData, mOperate);

		System.out.println("End FIDetailFinItemCodeUI Submit...");
		// 如果有需要处理的错误，则返回
		if (mFMDetailFinItemCodeBL.mErrors.needDealError())
		{
			// @@错误处理
			this.mErrors.copyAllErrors(mFMDetailFinItemCodeBL.mErrors);
			CError tError = new CError();
			tError.moduleName = "FMDetailFinItemCodeUI";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		if (mOperate.equals("QUERY||MAIN"))
		{
			this.mResult.clear();
			this.mResult = mFMDetailFinItemCodeBL.getResult();
		}
		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData)
	{
		// 全局变量
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0)); //(GlobalInput)强制类型转换
		this.mFMDetailFinItemCodeSchema.setSchema((FMDetailFinItemCodeSchema) cInputData.getObjectByObjectName("FMDetailFinItemCodeSchema", 0));
		if (mGlobalInput == null)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FMDetailFinItemCodeUI";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的信息！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 根据前面的输入数据，进行UI逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 * @return
	 */
	private boolean dealData()
	{
		boolean tReturn = true;

		if (this.mOperate.equals("INSERT||MAIN"))
		{
			//明细科目分支影射定义页面点击“添加”按钮重复录入的校验
			if (!FIDetailFinItemCodeInsertFail())
			{
				CError tError = new CError();
				tError.moduleName = "FMDetailFinItemCodeUI";
				tError.functionName = "dealData";
				tError.errorMessage = "您输入的记录在数据库中已经存在，请重新录入";
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		return tReturn;
	}
	
	private boolean FIDetailFinItemCodeInsertFail()
	{
		boolean tReturn = true;
		int Count;
		ExeSQL tExeSQL = new ExeSQL();
		String sql = "select COUNT(*) from FMDetailFinItemCode where VersionNo='" + this.mFMDetailFinItemCodeSchema.getVersionNo() 
				+ "'  and FinItemID='" + this.mFMDetailFinItemCodeSchema.getFinItemID() + "'  and JudgementNo='" + this.mFMDetailFinItemCodeSchema.getJudgementNo() 
				+ "'  and LevelConditionValue='" + this.mFMDetailFinItemCodeSchema.getLevelConditionValue() + "' and maintno = '"+this.mFMDetailFinItemCodeSchema.getMaintNo()+"'";
		Count = Integer.parseInt(tExeSQL.getOneValue(sql));
		if (Count >= 1)
		{
			tReturn = false;
		}
		return tReturn;
	}

	/**
	 * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
	 * @return
	 */
	private boolean prepareOutputData()
	{
		try
		{
			mInputData.clear();
			mInputData.add(this.mGlobalInput);
			mInputData.add(this.mFMDetailFinItemCodeSchema);
		}
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FMDetailFinItemCodeUI";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public VData getResult()
	{
		return this.mResult;

	}

	public static void main(String[] args)
	{
		
	}



}
