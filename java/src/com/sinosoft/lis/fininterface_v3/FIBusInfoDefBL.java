/**
 * <p>ClassName: FIBusTypeDefBL.java </p>
 * <p>Description: 业务交易定义 </p>
 * <p>Copyright: Copyright (c) 2009 </p>
 * <p>Company: </p>
 * @author
 * @version 1.0
 * @CreateDate：2011/8/25
 */
 
//包名
package com.sinosoft.lis.fininterface_v3;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class FIBusInfoDefBL  {
 /** 错误处理类，每个需要错误处理的类中都放置该类 */
 public  CErrors mErrors=new CErrors();
 private VData mResult = new VData();
 /** 往后面传输数据的容器 */
 private VData mInputData= new VData();
 /** 全局数据 */
 private GlobalInput mGlobalInput =new GlobalInput() ;
	/** 数据操作字符串 */
	private String mOperate;
 private String mOperater;
 private String mManageCom;
 
 /** 业务处理相关变量 */
 //业务类型编号
 private String BusTypeID="";
 //处理类型
 private String tMaintNo="";
 //页面类型
 private String PageFlag="";
 //版本编号
 private String VersionNo="";
 //
 //private FIBusTypeDefSchema oFIBusTypeDefSchema = new FIBusTypeInfoSchema();
 private FIBnTypeInfoSchema iFIBnTypeInfoSchema = new FIBnTypeInfoSchema();
 
 private MMap map=new MMap();
 
 public FIBusInfoDefBL(){

 }

 /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
 public boolean submitData(VData cInputData,String cOperate){
  //将操作数据拷贝到本类中
  this.mInputData = cInputData;
  this.mOperate =cOperate;
  
  //得到外部传入的数据,将数据备份到本类中
  if (!getInputData(cInputData, cOperate)){
      return false;
  }

  if (!checkData()){
      return false;
  }

  //进行业务处理
  if (!dealData()){
      return false;
  }

  if (!prepareOutputData()){
      return false;
  }

  PubSubmit tPubSubmit = new PubSubmit();
  
  System.out.println("Start FIBusTypeDefBL Submit...");
  
  if (!tPubSubmit.submitData(mInputData, null)){
    this.mErrors.copyAllErrors(tPubSubmit.mErrors);
    return false;
  }

  System.out.println("End FIBusTypeDefBL Submit...");
  mInputData=null;
  return true;
 }


 /**
  * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 *
 */
 private boolean getInputData(VData cInputData, String cOperate)
 {
	 mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
	 tMaintNo = (String)mInputData.get(1);
	 iFIBnTypeInfoSchema = (FIBnTypeInfoSchema)mInputData.getObjectByObjectName("FIBnTypeInfoSchema",2);
	 
	 if (mGlobalInput == null) {
         // @@错误处理
         //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
         CError tError = new CError();
         tError.moduleName = "FIBusTypeDefBL";
         tError.functionName = "getInputData";
         tError.errorMessage = "前台传输全局公共数据失败!";
         this.mErrors.addOneError(tError);

         return false;
     }

     //获得操作员编码
		mOperater = mGlobalInput.Operator;
     if ((mOperater == null) || mOperater.trim().equals("")) {
         // @@错误处理
         //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
         CError tError = new CError();
         tError.moduleName = "FIBusTypeDefBL";
         tError.functionName = "getInputData";
         tError.errorMessage = "前台传输全局公共数据Operate失败!";
         this.mErrors.addOneError(tError);

         return false;
     }

     //获得登陆机构编码
     mManageCom = mGlobalInput.ManageCom;
     if ((mManageCom == null) || mManageCom.trim().equals("")) {
         // @@错误处理
         //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
         CError tError = new CError();
         tError.moduleName = "FIBusTypeDefBL";
         tError.functionName = "getInputData";
         tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
         this.mErrors.addOneError(tError);

         return false;
     }

     mOperate = cOperate;
     if ((mOperate == null) || mOperate.trim().equals("")) {
         // @@错误处理
         //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
         CError tError = new CError();
         tError.moduleName = "FIBusTypeDefBL";
         tError.functionName = "getInputData";
         tError.errorMessage = "前台传输全局公共数据Operate任务节点编码失败!";
         this.mErrors.addOneError(tError);

         return false;
     }
	 return true;
 }

 private boolean checkData()
 {
	 
  return true;
 }

 /**
  * 根据前面的输入数据，进行BL逻辑处理
  * 如果在处理过程中出错，则返回false,否则返回true
  */
 private boolean dealData()
 {
	 try
	 {
		 FMBnTypeInfoSchema tFMBnFeeInfoSchema = new FMBnTypeInfoSchema();
		 FIBnTypeInfoSchema oFIBnFeeInfoSchema = new FIBnTypeInfoSchema();
		 FIBnTypeInfoDB mFIBusFeeInfoDB = new  FIBnTypeInfoDB();
		if("insert".equals(mOperate))
		{
			System.out.println("FIBusTypeDefBL----新增业务类型!");
			map.put(iFIBnTypeInfoSchema, "INSERT");
		}
		else if("delete".equals(mOperate))
		{
			oFIBnFeeInfoSchema = mFIBusFeeInfoDB.executeQuery("select * from FIBnTypeInfo "
					+" where BusinessID='"+iFIBnTypeInfoSchema.getBusinessID()+"' and PropertyCode = '"+iFIBnTypeInfoSchema.getPropertyCode()+"'").get(1);
			tMaintNo = PubFun1.CreateMaxNo("MaintNo_info",20);
		  	System.out.println("流水号作业：修改记录编号" + tMaintNo);
		  	tFMBnFeeInfoSchema.setMaintNo(tMaintNo);
		  	tFMBnFeeInfoSchema.setVersionNo("00000000000000000001");
		  	tFMBnFeeInfoSchema.setBusinessID(oFIBnFeeInfoSchema.getBusinessID());
		  	tFMBnFeeInfoSchema.setPropertyCode(oFIBnFeeInfoSchema.getPropertyCode());
		  	tFMBnFeeInfoSchema.setPropertyName(oFIBnFeeInfoSchema.getPropertyName());
		  	tFMBnFeeInfoSchema.setBusClass(oFIBnFeeInfoSchema.getBusClass());
		  	tFMBnFeeInfoSchema.setBusClassName(oFIBnFeeInfoSchema.getBusClassName());
		  	tFMBnFeeInfoSchema.setBusValues(PubFun.getCurrentDate()+" "+ PubFun.getCurrentTime());
		  	tFMBnFeeInfoSchema.setPurpose(oFIBnFeeInfoSchema.getPurpose());
		  	tFMBnFeeInfoSchema.setRemark(oFIBnFeeInfoSchema.getRemark());

			map.put(tFMBnFeeInfoSchema, "INSERT");
				
			System.out.println("FIBusTypeDefBL----删除业务类型!");
			map.put(iFIBnTypeInfoSchema, "DELETE");
		}
	 }
	 catch(Exception ex)
	 {
		 System.out.println("FIBusTypeDefBL--->dealData处理出错。");
		 return false;
	 }
  return true;
 }

 private boolean prepareOutputData()
 {
	 try
	 {
		 mInputData.clear();
		 mInputData.add(map);
	 }
	 catch(Exception ex)
	 {
		// @@错误处理
		CError tError = new CError();
		tError.moduleName = "FIBusTypeDefBL";
		tError.functionName = "prepareData";
		tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
		this.mErrors.addOneError(tError);
		return false;
	 }
  return true;
 }
 
	/**
	 * 提交数据
	 * @return
	 */
	private boolean pubSubmit()
	{
		// 进行数据提交
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, ""))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "FIBusTypeDefBL";
			tError.functionName = "PubSubmit.submitData";
			tError.errorMessage = "数据提交失败FIBusTypeDefBL-->pubSubmit!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}
 
 public VData getResult()
 {
	 return null;
 }

 public static void main(String[] args) {
 }
}

