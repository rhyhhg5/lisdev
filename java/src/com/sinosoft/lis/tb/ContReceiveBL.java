/**
 * 2007-11-17
 */
package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContReceiveDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCContReceiveSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCContReceiveSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class ContReceiveBL
{
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private MMap mMap = new MMap();

    private String mOperate = "";

    private String[] mOperateType = { "CreateReceive", "ReceiveCont" };

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    private LCContSchema mLCContSchema = null;

    private LAAgentSchema mLAAgentSchema = null;

    private double mReceiveCount = 0;

    public ContReceiveBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    private boolean getInputData(VData data, String cOperate)
    {
        mOperate = cOperate;
        mTransferData = (TransferData) data.getObjectByObjectName(
                "TransferData", 0);
        mGlobalInput = (GlobalInput) data.getObjectByObjectName("GlobalInput",
                0);

        return true;
    }

    private boolean checkData()
    {
        if (mGlobalInput == null)
        {
            buildError("checkData", "处理超时，请重新登录。");
            return false;
        }

        if (mTransferData == null)
        {
            buildError("checkData", "所需参数不完整。");
            return false;
        }

        if (mOperate == null || !checkOperate())
        {
            buildError("checkData", "操作类型错误。");
            return false;
        }

        return true;
    }

    private boolean dealData()
    {
        if (!loadContInfo())
        {
            return false;
        }

        if (!loadAgentInfo())
        {
            return false;
        }

        if ("CreateReceive".equals(mOperate))
        {
            if (!createReceiveTrace())
                return false;
        }

        if ("ReceiveCont".equals(mOperate))
        {
            if (!receiveCont())
                return false;
        }

        return true;
    }

    private boolean createReceiveTrace()
    {
        System.out.println("dealReceive");

        if (!cleanReceiveHistory())
        {
            return false;
        }

        LCContReceiveSchema tLCContReceiveSchema = new LCContReceiveSchema();
        tLCContReceiveSchema.setReceiveID(mReceiveCount + 1);
        tLCContReceiveSchema.setContNo(mLCContSchema.getContNo());
        tLCContReceiveSchema.setPrtNo(mLCContSchema.getPrtNo());
        tLCContReceiveSchema.setContType(mLCContSchema.getContType());
        tLCContReceiveSchema.setCValiDate(mLCContSchema.getCValiDate());
        tLCContReceiveSchema.setAppntNo(mLCContSchema.getAppntNo());
        tLCContReceiveSchema.setAppntName(mLCContSchema.getAppntName());
        tLCContReceiveSchema.setPrem(mLCContSchema.getPrem());
        tLCContReceiveSchema.setAgentCode(mLCContSchema.getAgentCode());
        tLCContReceiveSchema.setAgentName(mLAAgentSchema.getName());
        tLCContReceiveSchema.setSignDate(mLCContSchema.getSignDate());
        tLCContReceiveSchema.setManageCom(mLCContSchema.getManageCom());
        tLCContReceiveSchema.setPrintManageCom(mGlobalInput.ManageCom);
        tLCContReceiveSchema.setPrintOperator(mGlobalInput.Operator);
        tLCContReceiveSchema.setPrintDate(mCurrentDate);
        tLCContReceiveSchema.setPrintTime(mCurrentTime);
        tLCContReceiveSchema.setPrintCount(1);//mPrintCount
        tLCContReceiveSchema.setReceiveState("0");
        tLCContReceiveSchema.setDealState("0");
        tLCContReceiveSchema.setMakeDate(mCurrentDate);
        tLCContReceiveSchema.setMakeTime(mCurrentTime);
        tLCContReceiveSchema.setModifyDate(mCurrentDate);
        tLCContReceiveSchema.setModifyTime(mCurrentTime);

        mMap.put(tLCContReceiveSchema, SysConst.INSERT);

        if (!cleanGetPolDateOfCont())
        {
            return false;
        }

        return true;
    }

    /**
     * 加载合同信息。
     * @return
     */
    private boolean loadContInfo()
    {
        String tContNo = (String) mTransferData.getValueByName("ContNo");
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tContNo);
        if (!tLCContDB.getInfo())
        {
            buildError("loadContInfo", "未找到相关合同信息。");
            return false;
        }
        mLCContSchema = tLCContDB.getSchema();
        return true;
    }

    /**
     * 加载业务员信息。
     * @return
     */
    private boolean loadAgentInfo()
    {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mLCContSchema.getAgentCode());
        if (!tLAAgentDB.getInfo())
        {
            buildError("loadAgentInfo", "该保单业务员不存在。");
            return false;
        }
        mLAAgentSchema = tLAAgentDB.getSchema();
        return true;
    }

    private boolean cleanReceiveHistory()
    {
        LCContReceiveSet tLCContReceiveSet = null;

        // 获取保单现有的合同接收记录条数。
        String tSql = "select count(1) from LCContReceive where ContNo = '"
                + mLCContSchema.getContNo() + "'";
        String tReceiveCount = new ExeSQL().getOneValue(tSql);

        if ("".equals(tReceiveCount))
        {
            System.out.println("查询合同接收数据失败。");
            buildError("cleanReceiveHistory", "查询合同接收数据失败。");
            return false;
        }

        try
        {
            mReceiveCount = Double.parseDouble(tReceiveCount);
        }
        catch (Exception e)
        {
            System.out.println("获取 ReceiveCount 失败。");
            buildError("cleanReceiveHistory", "获取 ReceiveCount 失败。");
            return false;
        }

        if (mReceiveCount > 0)
        {
            LCContReceiveDB tLCContReceiveDB = new LCContReceiveDB();
            tLCContReceiveDB.setContNo(mLCContSchema.getContNo());
            tLCContReceiveDB.setDealState("0");
            tLCContReceiveSet = tLCContReceiveDB.query();

            if (tLCContReceiveSet.size() > 0)
            {
                for (int i = 1; i <= tLCContReceiveSet.size(); i++)
                {
                    LCContReceiveSchema tTmpLCContReceiveSchema = null;
                    tTmpLCContReceiveSchema = tLCContReceiveSet.get(i);
                    tTmpLCContReceiveSchema.setDealState("1");
                }
                mMap.put(tLCContReceiveSet, SysConst.UPDATE);
            }
        }
        else
        {
            System.out.println("第1次生成合同接收记录成");
        }

        return true;
    }

    /**
     * 生成待接收记录时，清除保单层中的接收日期与回销日期信息。
     * 由于简易件会在录入时，自动置入接收日期与回销日期，因此，需要清除。
     * @return
     */
    private boolean cleanGetPolDateOfCont()
    {
        String tSql = "update LCCont set GetPolDate = null, CustomGetPolDate = null "
                + " where ContNo = '" + mLCContSchema.getContNo() + "' ";
        mMap.put(tSql, SysConst.UPDATE);
        return true;
    }

    private boolean checkOperate()
    {
        // 无指定操作符类型时，不进行校验。
        if (mOperateType == null)
            return true;

        for (int i = 0; i < mOperateType.length; i++)
        {
            if (mOperate.equals(mOperateType[i]))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * 进行保单合同接收
     * 在该函数中会直接进行数据库的同步动作。
     * @return
     */
    private boolean receiveCont()
    {
        LCContReceiveSchema tLCContReceiveSchema = getLastReceiveTrace();
        if (tLCContReceiveSchema == null)
        {
            return false;
        }

        VData tTmpVData = new VData();
        tTmpVData.add(tLCContReceiveSchema);
        tTmpVData.add(mGlobalInput);
        String tTmpOperation = "RECEIVE";

        LCContReceiveUI tLCContReceiveUI = new LCContReceiveUI();
        if (!tLCContReceiveUI.submitData(tTmpVData, tTmpOperation))
        {
            System.out.println("合同接收操作失败。");
            if (tLCContReceiveUI.mErrors.needDealError())
            {
                System.out.println(tLCContReceiveUI.mErrors.getFirstError());
                buildError("receiveCont", tLCContReceiveUI.mErrors
                        .getFirstError());
            }
            else
            {
                System.out.println("保存失败，但是没有详细的原因");
                buildError("receiveCont", "保存失败，但是没有详细的原因");
            }
        }

        return true;
    }

    /**
     * 获取最近一次的有效合同接收轨迹。
     * @return
     */
    private LCContReceiveSchema getLastReceiveTrace()
    {
        String tSql = "select * from LCContReceive where DealState = '0' "
                + " and ContNo = '" + mLCContSchema.getContNo() + "' ";
        LCContReceiveSet tLCContReceiveSet = new LCContReceiveDB()
                .executeQuery(tSql);
        if (tLCContReceiveSet.size() != 1)
        {
            System.out.println("获取 ReceiveCount 失败。");
            buildError("cleanReceiveHistory", "获取 ReceiveCount 失败。");
            return null;
        }
        return tLCContReceiveSet.get(1);
    }

    /**
     * 提交数据库
     * @return
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败。");
            buildError("submit", "提交数据失败。");

            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FFIvoiceExportUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
