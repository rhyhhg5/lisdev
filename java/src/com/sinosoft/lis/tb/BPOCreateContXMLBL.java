package com.sinosoft.lis.tb;

import java.io.*;
import java.util.*;

import org.jdom.*;
import org.jdom.output.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 根据外包保单数据生成待导入的xml文件
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class BPOCreateContXMLBL
{
    public CErrors mErrors = new CErrors();

    private String mBPOBatchNo = null;
    private String mContID = null;
    private String mPrtNo = null;
    private HashMap mContMap = new HashMap();  //存储需要处理的批次号<->MissionID
    private BPOSentScanBL mBPOSentScanBL = new BPOSentScanBL();
    private BPOMissionStateSchema mCurBPOMissionStateSchema = null;

    public BPOCreateContXMLBL()
    {
    }

    /**
     * 生成xml
     * @param cContIDs String[]：合同ID
     * @return String：生成的xml文件路径及名字
     */
    public ArrayList createXML(ArrayList cContList)
    {
        ArrayList tXMLPathList = new ArrayList();

        Iterator tIterator = cContList.iterator();

        while(tIterator.hasNext())
        {
            //所有保单信息的父节点
            Element tContTableE = new Element("CONTTABLE");

            String[] data = (String[]) tIterator.next();
            if(!createOneCont(tContTableE, data))
            {
                return null;
            }

            //获得扫描类型，用于生成 XML 名称
            String subType = new ExeSQL().getOneValue("select SubType from es_doc_main where DocCode = '" + mPrtNo + "'");
        	mBPOSentScanBL.setSubType(subType);
            String tBatchNo = mBPOSentScanBL.gettBPOBatchNo("") + "Modify"; //非外包xml的批次号应该与外包批次号不同

            //根节点
            Element root = new Element("DATASET");
            Document doc = new Document(root);

            Element tBatchIDE = new Element("BATCHID");
            tBatchIDE.setText(tBatchNo);
            root.addContent(tBatchIDE);

            //原外包批次号节点
            Element tBPOBATCHNOE = new Element("BPOBATCHNO");
            tBPOBATCHNOE.setText(StrTool.cTrim(this.mBPOBatchNo));
            root.addContent(tBPOBATCHNOE);

            //保单信息父节点跟在原外包批次节点后
            root.addContent(tContTableE);

            //输出到磁盘
            XMLOutputter xo = new XMLOutputter("  ", true, "GBK");
            try
            {
                String tUIRoot = CommonBL.getUIRoot();
                BPOServerInfoDB tBPOServerInfoDB = new BPOServerInfoDB();
                tBPOServerInfoDB.setBPOID(mCurBPOMissionStateSchema.getBPOID());
                if(!tBPOServerInfoDB.getInfo())
                {
                    CError tError = new CError();
                    tError.moduleName = "BPOCreateContXMLBL";
                    tError.functionName = "createXML";
                    tError.errorMessage = "没有查询外包商"
                                          + tBPOServerInfoDB.getBPOID() + "信息";
                    mErrors.addOneError(tError);
                    System.out.println(tError.errorMessage);
                    continue;
                }
                String path = tUIRoot + tBPOServerInfoDB.getBackBackupBasePath()
                              + tBatchNo + ".xml";
                xo.output(doc, new FileOutputStream(path));
                tXMLPathList.add(path);
            }
            catch(IOException ex)
            {
                ex.printStackTrace();
                CError tError = new CError();
                tError.moduleName = "BPOCreateContXMLBL";
                tError.functionName = "createXML";
                tError.errorMessage = "无法输出xml文件";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return null;
            }
        }

        return tXMLPathList;
    }

    /**
     * 生成一个保单的xml信息
     * @param tContTableE Element：根节点CONTTABLE
     * @param tBPOLCPolSet BPOLCPolSet：险种信息
     * @return boolean
     */
    private boolean createOneCont(Element tContTableE, String[] data)
    {
        mBPOBatchNo = data[0];
        String tMissionID = data[1];

        //查询状态
        mCurBPOMissionStateSchema = getBPOMissionStateSchema(tMissionID);
        if(mCurBPOMissionStateSchema == null)
        {
            return false;
        }

        mPrtNo = mCurBPOMissionStateSchema.getBussNo();

        BPOLCPolSet tBPOLCPolSet = getBPOLCPolSet();
        if(tBPOLCPolSet == null)
        {
            return false;
        }
        mContID = tBPOLCPolSet.get(1).getContID();

        BPOLCAppntSet tBPOLCAppntSet = getBPOLCAppntInfo();
        BPOLCInsuredSet tBPOLCInsuredSet = getBPOLCInsuredInfo();
        BPOLCBnfSet tBPOLCBnfSet = getBPOLCBnfInfo();
        BPOLCImpartSet tBPOLCImpartSet = getBPOLCImpartInfo();
        BPOIssueSet tBPOIssueSet = getBPOIssueInfo();
        BPOLCNationSet tBPOLCNationSet = getBPOLCNationInfo();
        BPOLCRiskDutyWrapSet tBPOLCRiskDutyWrapSet = getBPOLCRiskDutyWrapInfo();

        if(tBPOLCAppntSet == null || tBPOLCInsuredSet == null
           || tBPOLCBnfSet == null || tBPOLCImpartSet == null
           || tBPOIssueSet == null || tBPOLCNationSet == null
           || tBPOLCRiskDutyWrapSet == null)
        {
            return false;
        }

        //为每个保单定义一个ROW节点
        Element tRowE = new Element("ROW");

        //添加CONTID节点
        Element tContIDE = new Element("CONTID");
        tContIDE.setText(mContID);
        tRowE.addContent(tContIDE);

        //添加PrtNo节点
        Element tPrtNoIDE = new Element("PrtNo");
        tPrtNoIDE.setText(mPrtNo);
        tRowE.addContent(tPrtNoIDE);

        //是否存在错误标志
        Element tISSUEEXISTE = new Element("ISSUEEXIST");
        tISSUEEXISTE.setText("0");
        tRowE.addContent(tISSUEEXISTE);

        //外包错误信息
        if(tBPOIssueSet.size() > 0)
        {
            tISSUEEXISTE.setText("1");

            Element tISSUETABLEE = new Element("ISSUETABLE");
            tRowE.addContent(tISSUETABLEE);
            if(!addRow(tISSUETABLEE, tBPOIssueSet))
            {
                return false;
            }
        }

        //添加投保人节点
        Element tAPPNTTABLEE = new Element("APPNTTABLE");
        tRowE.addContent(tAPPNTTABLEE);
        if(!addRow(tAPPNTTABLEE, tBPOLCAppntSet))
        {
            return false;
        }

        //添加被保人节点
        Element tINSUREDTABLEE = new Element("INSUREDTABLE");
        tRowE.addContent(tINSUREDTABLEE);
        if(!addRow(tINSUREDTABLEE, tBPOLCInsuredSet))
        {
            return false;
        }

        //添加险种节点
        if(tBPOLCPolSet.size() > 0)
        {
            Element tLCPOLTABLEE = new Element("LCPOLTABLE");
            tRowE.addContent(tLCPOLTABLEE);
            if(!addRow(tLCPOLTABLEE, tBPOLCPolSet))
            {
                return false;
            }
        }

        //添加受益人节点
        if(tBPOLCBnfSet.size() > 0)
        {
            Element tBNFTABLEE = new Element("BNFTABLE");
            tRowE.addContent(tBNFTABLEE);
            if(!addRow(tBNFTABLEE, tBPOLCBnfSet))
            {
                return false;
            }
        }

        if(tBPOLCImpartSet.size() > 0)
        {
            //添加客户告知节点
            Element tIMPARTTABLEE = new Element("IMPARTTABLE");
            tRowE.addContent(tIMPARTTABLEE);
            if(!addRow(tIMPARTTABLEE, tBPOLCImpartSet))
            {
                return false;
            }
        }

        //添加抵达国家节点
        if(tBPOLCNationSet.size() > 0)
        {
            Element tNationTABLE = new Element("NATIONTABLE");
            tRowE.addContent(tNationTABLE);
            if(!addRow(tNationTABLE, tBPOLCNationSet))
            {
                return false;
            }
        }

        //添加套餐节点
        if(tBPOLCRiskDutyWrapSet.size() > 0)
        {
            Element tRiskDutyWrap = new Element("RISKDUTYWRAP");
            tRowE.addContent(tRiskDutyWrap);
            if(!addRow(tRiskDutyWrap, tBPOLCRiskDutyWrapSet))
            {
                return false;
            }
        }

        tContTableE.addContent(tRowE);

        return true;
    }


    /**
     * addAppnt
     *
     * @return boolean
     */
    private boolean addRow(Element tElement, SchemaSet cSet)
    {
        for(int i = 1; i <= cSet.size(); i++)
        {
            Element tRowE = new Element("ROW");
            tElement.addContent(tRowE);

            Schema schema = (Schema) cSet.getObj(i);

            for(int j = 1; j < schema.getFieldCount(); j++)
            {
                String tFieldName = schema.getFieldName(j);
                String tFileValue = schema.getV(tFieldName);
                tFileValue = (tFileValue == null || tFileValue.equals("null")
                              ? "" : tFileValue);

                Element tFieldNodeE = new Element(tFieldName);
                tFieldNodeE.setText(StrTool.cTrim(tFileValue));
                tRowE.addContent(tFieldNodeE);
            }
        }

        return true;
    }

    /**
     * getBPOLCImpartInfo
     * 查询外包错误信息
     * @return BPOIssueSet
     */
    private BPOIssueSet getBPOIssueInfo()
    {
        String sql = "select * from BPOIssue "
                     + "where BPOBatchNo = '" + mBPOBatchNo + "' "
                     + "   and ContID = '" + mContID + "' ";
        System.out.println(sql);
        BPOIssueDB db = new BPOIssueDB();
        BPOIssueSet set = db.executeQuery(sql);
        if(db.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "BPOCreateContXMLBL";
            tError.functionName = "getBPOIssueInfo";
            tError.errorMessage = "查询外包错误信息出错，批次号" + mBPOBatchNo
                                  + "，印刷号" + mPrtNo;
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return set;
    }

    /**
     * getBPOLCImpartInfo
     * 查询外包客户告知信息
     * @return BPOLCImpartSet
     */
    private BPOLCImpartSet getBPOLCImpartInfo()
    {
        String sql = "select * from BPOLCImpart "
                     + "where BPOBatchNo = '" + mBPOBatchNo + "' "
                     + "   and ContID = '" + mContID + "' ";
        System.out.println(sql);
        BPOLCImpartDB db = new BPOLCImpartDB();
        BPOLCImpartSet set = db.executeQuery(sql);
        if(db.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "BPOCreateContXMLBL";
            tError.functionName = "getBPOLCImpartInfo";
            tError.errorMessage = "查询外包受益人信息出错，批次号" + mBPOBatchNo
                                  + "，印刷号" + mPrtNo;
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return set;
    }

    /**
     * getBPOLCBnfInfo
     * 查询外包受益人信息
     * @return BPOLCBnfSet
     */
    private BPOLCBnfSet getBPOLCBnfInfo()
    {
        String sql = "select * from BPOLCBnf "
                     + "where BPOBatchNo = '" + mBPOBatchNo + "' "
                     + "   and ContID = '" + mContID + "' ";
        System.out.println(sql);
        BPOLCBnfDB db = new BPOLCBnfDB();
        BPOLCBnfSet set = db.executeQuery(sql);
        if(db.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "BPOCreateContXMLBL";
            tError.functionName = "getBPOLCBnfInfo";
            tError.errorMessage = "查询外包受益人信息出错，批次号" + mBPOBatchNo
                                  + "，印刷号" + mPrtNo;
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return set;
    }

    /**
     * getBPOLCInsuredInfo
     * 查询外包被保人信息
     * @return BPOLCInsuredSet
     */
    private BPOLCInsuredSet getBPOLCInsuredInfo()
    {
        String sql = "select * from BPOLCInsured "
                     + "where BPOBatchNo = '" + mBPOBatchNo + "' "
                     + "   and ContID = '" + mContID + "' ";
        System.out.println(sql);
        BPOLCInsuredDB db = new BPOLCInsuredDB();
        BPOLCInsuredSet set = db.executeQuery(sql);
        if(db.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "BPOCreateContXMLBL";
            tError.functionName = "getBPOLCInsuredInfo";
            tError.errorMessage = "查询外包被保人信息出错，批次号" + mBPOBatchNo
                                  + "，印刷号" + mPrtNo;
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        if(set.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "BPOCreateContXMLBL";
            tError.functionName = "getBPOLCPolSet";
            tError.errorMessage = "没有查询到外包被保人信息信息" + mPrtNo;
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return set;
    }

    /**
     * getBPOMissionStateSchema
     *
     * @param cBPOBatchNo String
     * @param cMissionID String
     * @return BPOMissionStateSchema
     */
    private BPOMissionStateSchema getBPOMissionStateSchema(String cMissionID)
    {
        BPOMissionStateDB tBPOMissionStateDB = new BPOMissionStateDB();
        tBPOMissionStateDB.setBPOBatchNo(mBPOBatchNo);
        tBPOMissionStateDB.setMissionID(cMissionID);
        tBPOMissionStateDB.setBussNoType("TB");
        tBPOMissionStateDB.setState(BPO.MISSION_STATE_DATA_ERR);
        long a = System.currentTimeMillis();
        BPOMissionStateSet set = tBPOMissionStateDB.query();
        System.out.println(System.currentTimeMillis() - a);

        if(tBPOMissionStateDB.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "BPOCreateContXMLBL";
            tError.functionName = "getBPOMissionStateSchema";
            tError.errorMessage = "查询外包任务状态时出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        if(set.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "BPOCreateContXMLBL";
            tError.functionName = "getBPOMissionStateSchema";
            tError.errorMessage = "没有查查询到外包任务信息" + cMissionID;
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return set.get(1);
    }

    /**
     * getBPOLCPolSet
     *
     * @param cBPOBatchNo String
     * @param cMissionID String
     * @return BPOLCPolSet
     */
    private BPOLCPolSet getBPOLCPolSet()
    {
        String sql = "select * from BPOLCPol a "
                     + "where BPOBatchNo = '" + mBPOBatchNo + "' "
                     + "   and PrtNo = '" + mPrtNo + "' "
                     //+ "order by RelationToMainInsured, int(polid) ";
                     // 调整为已InsuredId为排序字段，原则上主被保人会第一个进行录入，因此流水号应该最小。
                     + "order by int(InsuredId), int(polid) ";
                     // -----------------------------------
        System.out.println(sql);
        BPOLCPolDB db = new BPOLCPolDB();
        BPOLCPolSet set = db.executeQuery(sql);
        if(db.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "BPOCreateContXMLBL";
            tError.functionName = "getBPOLCPolSet";
            tError.errorMessage = "查询险种信息出错，批次号" + mBPOBatchNo
                                  + "，应刷号" + mPrtNo;
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        if(set.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "BPOCreateContXMLBL";
            tError.functionName = "getBPOLCPolSet";
            tError.errorMessage = "没有查询到保单险种信息" + mPrtNo;
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return set;
    }

    /**
     * getBPOLCAppntSet
     *
     * @param cMissionID String
     * @return BPOLCAppntSet
     */
    private BPOLCAppntSet getBPOLCAppntInfo()
    {
        String sql = "select * from BPOLCAppnt "
                     + "where BPOBatchNo = '" + mBPOBatchNo + "' "
                     + "   and ContID = '" + mContID + "' ";
        System.out.println(sql);
        BPOLCAppntDB db = new BPOLCAppntDB();
        BPOLCAppntSet set = db.executeQuery(sql);
        if(db.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "BPOCreateContXMLBL";
            tError.functionName = "getBPOLCPolSet";
            tError.errorMessage = "查询险种信息出错，批次号" + mBPOBatchNo
                                  + "，印刷号" + mPrtNo;
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        if(set.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "BPOCreateContXMLBL";
            tError.functionName = "getBPOLCPolSet";
            tError.errorMessage = "没有查询到保单险种信息" + mPrtNo;
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return set;
    }

    /**
     * getBPOLCNationInfo
     * 获取抵达国家信息
     * @return BPOLCNationSet
     */
    private BPOLCNationSet getBPOLCNationInfo()
    {
        String sql = "select * from BPOLCNation "
                     + "where BPOBatchNo = '" + mBPOBatchNo + "' "
                     + "   and ContID = '" + mContID + "' ";
        System.out.println(sql);
        BPOLCNationDB db = new BPOLCNationDB();
        BPOLCNationSet set = db.executeQuery(sql);
        if(db.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "BPOCreateContXMLBL";
            tError.functionName = "getBPOLCNationInfo";
            tError.errorMessage = "查询险种信息出错，批次号" + mBPOBatchNo
                                  + "，印刷号" + mPrtNo;
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return set;
    }

    /**
     * getBPOLCRiskDutyWrapInfo
     * 获得套餐信息
     * @return BPOLCRiskDutyWrapSet
     */
    private BPOLCRiskDutyWrapSet getBPOLCRiskDutyWrapInfo()
    {
        String sql = "select * from BPOLCRiskDutyWrap "
                     + "where BPOBatchNo = '" + mBPOBatchNo + "' "
                     + "   and ContID = '" + mContID + "' ";
        System.out.println(sql);
        BPOLCRiskDutyWrapDB db = new BPOLCRiskDutyWrapDB();
        BPOLCRiskDutyWrapSet set = db.executeQuery(sql);
        if(db.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "BPOCreateContXMLBL";
            tError.functionName = "getBPOLCRiskDutyWrapInfo";
            tError.errorMessage = "查询险种信息出错，批次号" + mBPOBatchNo
                                  + "，印刷号" + mPrtNo;
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return set;
    }

    public static void main(String[] args) throws IOException
    {
        ArrayList lis = new ArrayList();

        String[] data = {"ScanListSendOut_20071008_150741_1", "00000000000000023035"};
        String[] data2 = {"ScanListSendOut_20071008_150741_1", "00000000000000023034"};
        lis.add(data);
        lis.add(data2);
        BPOCreateContXMLBL bl = new BPOCreateContXMLBL();
        if(bl.createXML(lis) == null)
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
