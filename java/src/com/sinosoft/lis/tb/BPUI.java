package com.sinosoft.lis.tb;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class BPUI {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	
	/** 往前面传输数据的容器 */
	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;
	
	public BPUI(){}
	
	public boolean submitData(VData cInputData, String cOperate) {
		mOperate = cOperate;
		this.mInputData = (VData) cInputData.clone();
		BPBL tBPBL = new BPBL();
		try {
			if (!tBPBL.submitData(mInputData, mOperate)) {
				this.mErrors.copyAllErrors(tBPBL.mErrors);
				CError tError = new CError();
				tError.moduleName = "BPBL";
				tError.functionName = "submitDat";
				tError.errorMessage = "BL类处理失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		mResult = tBPBL.getResult();
		return true;
	}

	public VData getResult() {
		return mResult;
	}
	
}
