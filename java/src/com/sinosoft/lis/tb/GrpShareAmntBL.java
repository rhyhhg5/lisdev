/**
 * 2014-7-30
 */
package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpRiskShareAmntSchema;
import com.sinosoft.lis.schema.LCGrpShareAmntSchema;
import com.sinosoft.lis.vschema.LCGrpRiskShareAmntSet;
import com.sinosoft.lis.vschema.LCGrpShareAmntSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 共用保额后台处理类
 * @author zjd
 * @version 1.0
 */
public class GrpShareAmntBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 往前台传输数据的容器 */
    private VData tResult = new VData();

    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();

    //private VData mIputData = new VData();
    private TransferData mTransferData = new TransferData();
    /**
     * 共用保额主表
     */
    private LCGrpShareAmntSet mLCGrpShareAmntSet=new LCGrpShareAmntSet();
    
    /**
     * 共用计划主表提交
     */
    
    private LCGrpShareAmntSet tLCGrpShareAmntSet=new LCGrpShareAmntSet();
    /**
     * 共用保额分表
     */
    private LCGrpRiskShareAmntSet mLCGrpRiskShareAmntSet=new LCGrpRiskShareAmntSet();
    /**
     * 共用计划分表提交
     */
    private LCGrpRiskShareAmntSet tLCGrpRiskShareAmntSet=new LCGrpRiskShareAmntSet();
    ExeSQL tExeSQL = new ExeSQL();
    
    private MMap map;
    /** 数据操作字符串 */
    private String mOperater;
    private String mManageCom;
    private String mOperate;
    
    private String mprtNo;
    private String mContPlanCode;
    private String mSharePlanCode;
    /**
     * 传输数据的公共方法
     *
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate)) {
            return false;
        }
        System.out.println("---GrpShareAmntBL getInputData---");
        // 数据操作业务处理
        if (!dealData()) {
            return false;
        }
        System.out.println("---GrpShareAmntBL dealData---");

        //准备给后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        System.out.println("---GrpShareAmntBL prepareOutputData---");
        
        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(this.mResult, "INSERT"))
        {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        
        return true;
    }
    
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    private boolean getInputData(VData cInputData, String cOperate) {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput = ((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput",
                0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData",
                0);
        mInputData = cInputData;
        if (mGlobalInput == null) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpShareAmntBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得操作员编码
        mOperater = mGlobalInput.Operator;
        if ((mOperater == null) || mOperater.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpShareAmntBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
        if ((mManageCom == null) || mManageCom.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpShareAmntBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        mOperate = cOperate;
        if ((mOperate == null) || mOperate.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpShareAmntBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate任务节点编码失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
          mLCGrpShareAmntSet=((LCGrpShareAmntSet) cInputData.getObjectByObjectName(
                  "LCGrpShareAmntSet",
                  0));
          
        /**
         * 共用保额分表
         */
          mLCGrpRiskShareAmntSet=((LCGrpRiskShareAmntSet) cInputData.getObjectByObjectName(
                  "LCGrpRiskShareAmntSet",
                  0));
        return true;
    }
    
    
    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
        System.out.println("Into GrpShareAmntBL.dealData()..." + mOperate);
        /** 执行新单录入过程 */
        if (this.mOperate.equals("INSERT"))
        {
            if (!insertSharePlan())
            {
                return false;
            }
        }
        else if (this.mOperate.equals("INSERTDetail"))
        {
            if (!insertShareDetail())
            {
                return false;
            }
        }
        else if (this.mOperate.equals("DELETE"))
        {
            if (!deleteData())
            {
                return false;
            }
        }
        else
        {
            buildError("dealData", "目前只支持保存修改,删除功能不支持其它操作！");
            return false;
        }
        return true;
    }
    
    /**
     * 处理共用保额主表信息
     * @return
     */
    private boolean insertSharePlan(){
        if(mLCGrpShareAmntSet!=null && mLCGrpShareAmntSet.size()>0){
            for(int i=0;i<mLCGrpShareAmntSet.size();i++){
                LCGrpShareAmntSchema tLCGrpShareAmntSchema=new LCGrpShareAmntSchema();
                tLCGrpShareAmntSchema.setSchema(mLCGrpShareAmntSet.get(i+1));
                String mSerialNo=PubFun1.CreateMaxNo("ShareAmnt", 20);//共用主表流水号
                tLCGrpShareAmntSchema.setSerialNo(mSerialNo);
                tLCGrpShareAmntSchema.setShareType("01");//01 险种公共用 02 责任共用
                //共用计划编码
                String tsql=" Select Case When max(SharePlanCode) Is Null Then '0' Else max(SharePlanCode) End from LCGrpShareAmnt where PrtNo='"+tLCGrpShareAmntSchema.getPrtNo()+"' and ContPlanCode='"+tLCGrpShareAmntSchema.getContPlanCode()+"' ";
                String tSharePlanCode=tExeSQL.getOneValue(tsql);
                tLCGrpShareAmntSchema.setSharePlanCode(String.valueOf(Integer.parseInt(tSharePlanCode)+1));//生成共用计划编码
                tLCGrpShareAmntSet.add(tLCGrpShareAmntSchema);   
                   
            }
        }
        return true;
    }
    
    /**
     * 处理共用保额分表信息
     * @return
     */
    private boolean  insertShareDetail(){
        if(mLCGrpRiskShareAmntSet!=null && mLCGrpRiskShareAmntSet.size()>0){
            //插入前先删除
            String tdelSql2=" delete from LCGrpRiskShareAmnt where PrtNo='"+mLCGrpRiskShareAmntSet.get(1).getPrtNo()+"' and ContPlanCode='"+mLCGrpRiskShareAmntSet.get(1).getContPlanCode()+"' and SharePlanCode='"+mLCGrpRiskShareAmntSet.get(1).getSharePlanCode()+"' ";
            if (map == null)
            {
                map = new MMap();
            }
            map.put(tdelSql2, "DELETE");
            
            for(int i=0; i<mLCGrpRiskShareAmntSet.size();i++){
                LCGrpRiskShareAmntSchema mLCGrpRiskShareAmntSchema=new LCGrpRiskShareAmntSchema();
                mLCGrpRiskShareAmntSchema=mLCGrpRiskShareAmntSet.get(i+1);
                String tsql="select distinct dutycode from lccontplandutyparam where grpcontno = '"+mLCGrpRiskShareAmntSchema.getGrpContNo()+"' and contplancode='"+mLCGrpRiskShareAmntSchema.getContPlanCode()+"' and riskcode='"+mLCGrpRiskShareAmntSchema.getRiskcode()+"' with ur";
                SSRS tSSRS=tExeSQL.execSQL(tsql);
                if(tSSRS!=null && tSSRS.MaxNumber >0){
                    for(int j=1;j<=tSSRS.MaxNumber;j++){
                        LCGrpRiskShareAmntSchema tLCGrpRiskShareAmntSchema=new LCGrpRiskShareAmntSchema();
                        String mSerialNo=PubFun1.CreateMaxNo("RiskShareAmnt", 20);//共用分表流水号
                        tLCGrpRiskShareAmntSchema.setSchema(mLCGrpRiskShareAmntSchema);
                        tLCGrpRiskShareAmntSchema.setSerialNo(mSerialNo);
                        tLCGrpRiskShareAmntSchema.setDutycode(tSSRS.GetText(j,1));
                        tLCGrpRiskShareAmntSet.add(tLCGrpRiskShareAmntSchema);
                    }
                    
                }
            }
        }
        return true;   
    }
    /**
     * 删除共用计划
     * @return
     */
    private boolean deleteData(){
        if(mTransferData!=null){
              mprtNo=(String)mTransferData.getValueByName("prtNo");
              mContPlanCode=(String)mTransferData.getValueByName("ContPlanCode");
              mSharePlanCode=(String)mTransferData.getValueByName("SharePlanCode");
              
              if("".equals(mprtNo) || "".equals(mContPlanCode) || "".equals(mSharePlanCode)){
                  buildError("mTransferData", "前台传输全局公共数据mTransferData失败！");
                  return false;
              }else{
                  String tdelSql1=" delete from LCGrpShareAmnt where PrtNo='"+mprtNo+"' and ContPlanCode='"+mContPlanCode+"' and SharePlanCode='"+mSharePlanCode+"' ";
                  
                  String tdelSql2=" delete from LCGrpRiskShareAmnt where PrtNo='"+mprtNo+"' and ContPlanCode='"+mContPlanCode+"' and SharePlanCode='"+mSharePlanCode+"' ";
                  
                  if (map == null)
                  {
                      map = new MMap();
                  }
                  map.put(tdelSql1, "DELETE");
                  map.put(tdelSql2, "DELETE");
              }
              
              
        }
        return true;
    }
    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        

        if (this.mOperate.equals("INSERT"))
        {
            if (map == null)
            {
                map = new MMap();
            }
            map.put(this.tLCGrpShareAmntSet, "DELETE&INSERT");
            mResult.add(map);
        }else
        if (this.mOperate.equals("INSERTDetail"))
        {
             if (map == null)
             {
                 map = new MMap();
             }
             map.put(this.tLCGrpRiskShareAmntSet, "DELETE&INSERT");
             mResult.add(map);
        }
        else if (this.mOperate.equals("DELETE"))
        {
            if (this.map.size() > 0)
            {
                this.mResult.add(map);
            }
            else
            {
                buildError("prepareOutputData", "进行删除操作,但没有删除数据！");
                return false;
            }
        }
        return true;
    }
    
    public VData getResult() {
        return tResult;
    }

    /**
     * 出错处理
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GrpShareAmntBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.err.println("程序报错：" + cError.errorMessage);
    }
    
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // TODO 自动生成方法存根

    }

}
