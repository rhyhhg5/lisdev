/*
 * @(#)ProposalSignBL.java	2004-12-13
 *
 * Copyright 2004 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.tb;

import java.text.*;
import java.util.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.ulitb.CachedRiskInfo;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保签单业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft</p>
 * @author WUJS
 * @version 6.0
 */
public class ProposalSignBL
{
    /**存放结果*/
    public VData mVResult = new VData();

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 数据操作字符串 */
    private String mOperate;

    private FDate fDate = new FDate();

    /*转换精确位数的对象   */
    private String FORMATMODOL = "0.00"; //保费保额计算出来后的精确位数

    private DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL); //数字转换对象

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 个人投保单表 */
    private LCPolSet mLCPolSet = new LCPolSet();

    /** 实收总表 */
    private LJAPaySchema mLJAPaySchema = new LJAPaySchema();

    /**实收总表--存储用*/
    private LJAPaySet mSaveLJAPaySet = new LJAPaySet();

    /**系统变量表 */
    LDSysVarSchema mLDSysVarSchema = null;

    /**变量拷贝*/
    Reflections rf = new Reflections();

    /**险种承保描述表*/
    LMRiskAppSchema mLMRiskAppSchema = null;

    private String mRiskFlag = "";

    private String mNeedReadBankFlag = "";

    private String mSameBatch = "0"; // 主附险是否属于同一批 "0"--非同一批 "1"--同一批

    private String mMainPolNo = null; // 同一批处理的附险对应的主险的新保单号

    private String mSerialNo = "";

    private String mPolType = "1"; // 1-个人单 2-集体下的个人单 3-合同下的个人单

    private String mBack = ""; // 签单追溯标记 "0"--不追溯 "1"--追溯到签单次日 "2"--追溯到交费到帐最大次日 "3"--追溯到交费到帐最小次日

    private Date mValiDate = null;

    private double mLeft = 0;

    private Date mFirstPayDate = null;

    private double sumMoney = 0; // 总的交费额

    private double sumPrem = 0;

    private Date maxPayDate = null;

    private Date maxEnterAccDate = null;

    private boolean mInsuredAppAgeChangeFalg = false;

    private boolean alreadyChangeCValiDate = false; //是否已经更改过生效日期

    private boolean mBQFlag = false; //保全标记

    private boolean mNotCreatInsuaccFlag = false; //不生成帐户的标记
    
    private CachedRiskInfo mCRI = CachedRiskInfo.getInstance();

    private String DELETE = "DELETE";

    private String INSERT = "INSERT";

    private String UPDATE = "UPDATE";

    private String mNewContNo = null; //新的合同号

    private TransferData mBQData;

    MMap rMap = new MMap();

    private VData mBufferedPolData = new VData();

    private Date maxEndDate;

    public VData getResult()
    {
        return this.mInputData;
    }

    public VData getBufferedPolData()
    {
        return this.mBufferedPolData;
    }

    public static void main(String[] args)
    {

        /**插入方法测试
         ProposalSignBL proposalSignBL = new ProposalSignBL();
         MMap dest = new MMap();
         VData srcData = new VData();

         for ( int i=0;i<4;i++){
         LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
         LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();

         tLCUWMasterSchema.setPolNo("tNewPolNo<"+i+">");
         tLCUWMasterSchema.setGrpPolNo("tLCPolSchema.getGrpPolNo("+i+")");
         tLCUWMasterSchema.setContNo("tLCPolSchema.getContNo("+i+")");
         tLCUWMasterSet.add(tLCUWMasterSchema);
         srcData.add(i ,tLCUWMasterSet);
         }


         int changeIndex = 3;
         proposalSignBL.addIntoMapByAction(dest, srcData, changeIndex);
         System.out.println("here");
         */
        /*** 拷贝schemaset测试
         ProposalSignBL ps = new ProposalSignBL();
         LJAPaySchema tLJAPaySchema = new LJAPaySchema();
         tLJAPaySchema.setPayNo("payno");
         tLJAPaySchema.setIncomeNo("polno");
         tLJAPaySchema.setIncomeType("2");
         tLJAPaySchema.setAppntNo("appntno");
         LJAPaySet tSet = new LJAPaySet();
         tSet.add( tLJAPaySchema );
         LJAPaySet tmpSet = (LJAPaySet) ps.copySchemaSet((SchemaSet)tSet );
         */

        GlobalInput tG = new GlobalInput();
        tG.ComCode = "86";
        tG.Operator = "001";

        //生成一个序列号
        String tLimit = PubFun.getNoLimit("86");
        String mSerialNo = PubFun1.CreateMaxNo("SerialNo", tLimit);
        if (mSerialNo.equals(""))
        {
            //     mErrors.addOneError("生成序列号失败!");
            return;
        }

        //获取实收总表数据
        LJAPayDB tLJAPayDB = new LJAPayDB();
        tLJAPayDB.setIncomeNo("86330020040220000029");
        tLJAPayDB.setIncomeType("1");

        LJAPaySet tLJAPaySet = tLJAPayDB.query();
        LJAPaySchema tLJAPaySchema = tLJAPaySet.get(1);

        //准备调用签单需要的数据
        VData vPolPub = new VData();
        vPolPub.add(tG);
        vPolPub.add(mSerialNo);
        vPolPub.add(tLJAPaySchema);

        TransferData tBQData = new TransferData();
        tBQData.setNameAndValue("EdorType", "GANAME");

        ProposalSignBL tProposalSignBL = new ProposalSignBL();
        //        MMap tVData = tProposalSignBL.dealOnePol("00000000000000000000",
        //                "00000000000000000000", "86110020040210000240", vPolPub, tBQData);
        tProposalSignBL.mGlobalInput.Operator = "001";
        tProposalSignBL.mGlobalInput.ManageCom = "86";
        //   VData tVData = tProposalSignBL.dealOnePol("",
        //                                        "", "86110020040210000243", null, null);
        VData tVData = null;
        if (tVData == null)
        {

            return;
        }
        MMap tmpMap = (MMap) tVData.getObjectByObjectName("MMap", 0);
        LCPolSchema tLCPolSchema = (LCPolSchema) tVData.getObjectByObjectName(
                "LCPolSchema", 0);
        tmpMap.put(tLCPolSchema, "INSERT");
        VData tInputData = new VData();
        tInputData.add(tmpMap);

        PubSubmit pubSubmit = new PubSubmit();
        pubSubmit.submitData(tInputData, null);

    }

    /**
     * 传输数据的公共方法
     *
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @return:
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (this.getInputData() == false)
        {
            return false;
        }

        // 数据操作业务处理
        if (this.dealData() == false)
        {
            return false;
        }

        return true;

    }

    /**
     * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData()
    {

        String tPolNo = "";
        String tInsuredNo = "";
        String tGrpPolNo = "";

        TransferData tTransferData = new TransferData();

        mInputData = new VData();
        rMap = new MMap();
        LCPolSet tLCPolSet = new LCPolSet();
        //* ↓ *** liuhao *** 2005-05-13 *** add **********
        //险种序号
        int iRiskSeqNo = 0;
        //* ↑ *** liuhao *** 2005-05-13 *** add **********
        // 处理签单数据--假设传入的数据都是主险
        int polCount = mLCPolSet.size();
        maxEndDate = fDate.getDate("1900-1-1");
        for (int i = 1; i <= mLCPolSet.size(); i++)
        {
            if (maxEndDate.before(fDate.getDate(mLCPolSet.get(i).getEndDate())))
            {
                maxEndDate = fDate.getDate(mLCPolSet.get(i).getEndDate());
                System.out.println("保单终止日期为 : " + fDate.getString(maxEndDate));
            }
        }

        LCDutySet tLCDutySet = new LCDutySet();

        for (int i = 1; i <= polCount; i++)
        {

            LCPolSchema tLCPolSchema = mLCPolSet.get(i);
            if (tLCPolSchema == null)
            {
                continue;
            }

            if (!tLCPolSchema.getContType().equals("2")){
            //校验险种是否停售
            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("RiskStopSale");
            tLDCodeDB.setCode(tLCPolSchema.getRiskCode());
            LDCodeSet tLDCodeSet = tLDCodeDB.query();
            if (tLDCodeSet != null && tLDCodeSet.size() == 1)
            {
                System.out
                        .println("险种 " + tLCPolSchema.getRiskCode() + " 已停售！");
                CError.buildErr(this, "险种 " + tLCPolSchema.getRiskCode()
                        + " 已停售！");
                return false;
            }

            //校验分公司是否停售万能险种
            String universalSaleSQL = "select * from LDCode where CodeType = 'UniversalSale' "
                    + "and Code = '"
                    + tLCPolSchema.getManageCom().substring(0, 4)
                    + "' "
                    + "and exists (select 1 from LMRiskApp where RiskCode = '"
                    + tLCPolSchema.getRiskCode() + "' and RiskType4 = '4')";
            tLDCodeSet = tLDCodeDB.executeQuery(universalSaleSQL);
            if (tLDCodeSet != null && tLDCodeSet.size() == 1)
            {
                System.out.println("分公司 " + tLCPolSchema.getManageCom()
                        + " 万能险已停售！");
                CError.buildErr(this, "分公司 " + tLCPolSchema.getManageCom()
                        + " 万能险已停售！");
                return false;
            }
            }

            tPolNo = tLCPolSchema.getPolNo();
            tGrpPolNo = tLCPolSchema.getGrpPolNo();
            tInsuredNo = tLCPolSchema.getInsuredNo();

            //* ↓ *** liuhao *** 2005-06-09 *** modify **********
            //险种编码处理
            //if(i > 1 && tInsuredNo.equals(mLCPolSet.get(i-1).getInsuredNo()))
            //{
            iRiskSeqNo = iRiskSeqNo + 1;
            //}else
            //{
            //    iRiskSeqNo = 1;
            //}
            if (mLCPolSet.get(i).getRiskSeqNo() == null
                    || mLCPolSet.get(i).getRiskSeqNo().equals(""))
            {
                mLCPolSet.get(i).setRiskSeqNo(
                        ("" + (1000 + iRiskSeqNo)).substring(2, 4));
            }
            //* ↑ *** liuhao *** 2005-06-09 *** modify **********

            //下面dealOnePol函数被集体签单处调用
            VData mapPol = this.dealOnePolData(tLCPolSchema);

            if (mapPol != null)
            {
                MMap tmpMap = (MMap) mapPol.getObjectByObjectName("MMap", 0);
                tLCPolSchema.setSchema((LCPolSchema) mapPol
                        .getObjectByObjectName("LCPolSchema", 0));

                //                //查找并替换对应附加险的主险保单号
                for (int j = 1; j <= mLCPolSet.size(); j++)
                {
                    if (mLCPolSet.get(j).getMainPolNo().equals(tPolNo))
                    {
                        mLCPolSet.get(j).setMainPolNo(tLCPolSchema.getPolNo());
                    }
                }

                //                mLCPolSet.set(i, tLCPolSchema);
                tmpMap.put(tLCPolSchema, "INSERT");
                if (rMap == null)
                {
                    rMap = tmpMap;
                }
                else
                {
                    rMap.add(tmpMap);
                }
                tLCPolSet.add(tLCPolSchema);

                tTransferData = (TransferData) mapPol.getObjectByObjectName(
                        "TransferData", 0);

                LCDutySet tTmpLCDutySet = (LCDutySet) mapPol
                        .getObjectByObjectName("LCDutySet", 0);
                tLCDutySet.add(tTmpLCDutySet);
            }
            else
            {
                return false;
            }
        }
        this.mInputData.add(tLCPolSet);
        mInputData.add(rMap);
        
        mInputData.add(tTransferData);
        mInputData.add(tLCDutySet);

        return true;
    }

    /**
     * 处理一张投保单的数据- 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param tLCPolSchema 合同号
     * @return com.sinosoft.utility.VData
     */
    private VData dealOnePolData(LCPolSchema tLCPolSchema)
    {
        VData tReturn = new VData();

        TransferData tTransferData = new TransferData();

        // 初始化类属性
        if (!this.initV())
        {
            return null;
        }

        MMap tmpMap = new MMap();

        LMRiskSchema tLMRiskSchema = null;

        // 准备投保单的数据
        if (tLCPolSchema.getContType().equals("2"))
        {
            mPolType = "2";
        }
        //保全签单
        if (tLCPolSchema.getAppFlag().equals("2")
                || tLCPolSchema.getAppFlag().equals("4"))
        {
            mBQFlag = true;
        }

        LCPolSchema delPolSchema = new LCPolSchema();
        delPolSchema.setSchema(tLCPolSchema);
        tmpMap.put(delPolSchema, "DELETE");

        if(!mPolType.equals("2")){
        // 签单校验
        if (this.checkOnePol(tLCPolSchema) == false)
        {
            return null;
        }
        }

        // 准备公用信息-生成险种保单号　
        String tNewPolNo = this.preparePubInfo(tLCPolSchema);

        if (tNewPolNo == null)
        {
            return null;
        }

        //准备一个险种保单的签单数据
        VData tPolDetail = this.getOnePol(tLCPolSchema, tNewPolNo);

        if (tPolDetail == null)
        {
            return null;
        }

        //(保全财务部分，承保部分财务不在这里处理)  ---该部分会更改tPolDetail
        if (mBQFlag)
        {
            if (verifyFinance(tPolDetail, tLCPolSchema, tNewPolNo, mBQData) == false)
            {
                return null;
            }
        }

        //处理帐户
        //在getOnePol函数中，如果因为签单日期追溯部分的重新计算，此时tLCPolSchema的保单号就会是投保单号
        if (mNotCreatInsuaccFlag == false)
        {
            tLCPolSchema.setPolNo(tNewPolNo);
            tLCPolSchema.setContNo(mNewContNo);
            //判断是否生成账户信息
            LCPremSet PubLCPremSet = (LCPremSet) tPolDetail
                    .getObjectByObjectName("LCPremSet", 0);
            boolean needDealAccount = false;
            for (int i = 1; i <= PubLCPremSet.size(); i++)
            {
                if (StrTool.cTrim(PubLCPremSet.get(i).getNeedAcc()).equals("1"))
                {
                    needDealAccount = true;
                }
            }
            if (needDealAccount
                    && DealAccount(tPolDetail, tLCPolSchema, tLMRiskSchema) == false)
            {
                return null;
            }

        }

        //把准备好的数据放到结果集中
        tmpMap.add((MMap) tPolDetail.getObjectByObjectName("MMap", 0));
        //  tmpMap.put("select 1 from dual","oo");
        tmpMap.put(
                (LCPremSet) tPolDetail.getObjectByObjectName("LCPremSet", 0),
                this.INSERT);
        tmpMap.put(
                (LCDutySet) tPolDetail.getObjectByObjectName("LCDutySet", 0),
                this.INSERT);
        tmpMap.put((LCGetSet) tPolDetail.getObjectByObjectName("LCGetSet", 0),
                this.INSERT);
        //         tmpMap.put((LJAPaySet)tAPay.getObjectByObjectName("LJAPaySet",0 ) , tAPay.getObjectByObjectName("String",0));
        tLCPolSchema = (LCPolSchema) tPolDetail.getObjectByObjectName(
                "LCPolSchema", 0);

        tTransferData = (TransferData) tPolDetail.getObjectByObjectName(
                "TransferData", 0);

        tReturn.add(tmpMap);

        tReturn.add(tLCPolSchema);
        tReturn.add(tPolDetail.getObjectByObjectName("LCPremSet", 0));
        /** @author:Yangming 保全增人需要指定生效日期 */
        tReturn.add(tPolDetail.getObjectByObjectName("LCGetSet", 0));
        tReturn.add(tPolDetail.getObjectByObjectName("LCDutySet", 0));

        tReturn.add(tTransferData);

        //         //同时把数据缓冲起来，待外头调用
        VData bufferData = new VData();
        bufferData.add((LCPolSchema) tPolDetail.getObjectByObjectName(
                "LCPolSchema", 0));
        bufferData.add(tPolDetail.getObjectByObjectName("LCPremSet", 0));
        this.mBufferedPolData.add(bufferData);

        return tReturn;

    }

    /**
     * 处理一张投保单以及其关联的附险投保单的数据
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param tLCPolSchema LCPolSchema
     * @param tNewPolNo String
     * @return VData
     */
    private VData getOnePol(LCPolSchema tLCPolSchema, String tNewPolNo)
    {
        VData tReturn = new VData();
        TransferData tTransferData = new TransferData();

        MMap tmpMap = new MMap();
        tmpMap.add(this.getElseUpdateTableSql(tLCPolSchema.getPolNo(),
                mNewContNo, tNewPolNo));

        // 取出原来的保费项
        LCPremSet tLCPremSet = this.getOldPrem(tLCPolSchema, tNewPolNo);

        if (tLCPremSet == null)
        {
            return null;
        }

        tmpMap.put((LCPremSet) PubFun.copySchemaSet(tLCPremSet), "DELETE");

        // 取出原来的领取项
        LCGetSet tLCGetSet = this.getOldGet(tLCPolSchema, tNewPolNo);

        if (tLCGetSet == null)
        {
            return null;
        }
        tmpMap.put((LCGetSet) PubFun.copySchemaSet(tLCGetSet), "DELETE");

        // 取出原来的责任
        LCDutySet tLCDutySet = this.getOldDuty(tLCPolSchema, tNewPolNo);

        if (tLCDutySet == null)
        {
            return null;
        }
        tmpMap.put((LCDutySet) PubFun.copySchemaSet(tLCDutySet), "DELETE");

        //如果是需要生效日期追溯
        if (this.needReCal(tLCPolSchema) == true)
        {
            // 改变保单生效日期
            tLCPolSchema.setCValiDate(fDate.getString(mValiDate));
            //  System.out.println(mValiDate);

            VData tReCal = null;
            LMRiskAppSchema tLMRiskAppSchema = mCRI.findRiskAppByRiskCodeClone(tLCPolSchema.getRiskCode());
            if(!"370301".equals(tLCPolSchema.getRiskCode())&&"G".equals(tLMRiskAppSchema.getRiskProp()) && "4".equals(tLMRiskAppSchema.getRiskType4())){
            	tReCal = this.reCalULI(tLCPolSchema, tLCGetSet, tLCDutySet,
                        tLCPremSet);
            }else{
            	// 签单日期追溯部分的重新计算（涉及领取项、保费项、责任和保单部分）
            	tReCal = this.reCal(tLCPolSchema, tLCGetSet, tLCDutySet,
                    tLCPremSet);
            }
            
            
            if (tReCal == null)
            {
                return null;
            }

            tLCPremSet.set((LCPremSet) tReCal.get(0));
            tLCGetSet.set((LCGetSet) tReCal.get(1));
            tLCDutySet.set((LCDutySet) tReCal.get(2));
            tLCPolSchema = (LCPolSchema) tReCal.get(3);

            tTransferData.setNameAndValue("CValidateChangeFlag", "Y");
        }
        //        else //如果不需要追溯
        //        {
        //            //如果是附加险，并且系统变量指定其生效日期为主险，那么需要重新计算日期（涉及领取项、保费项、责任和保单部分）
        //            if (alreadyChangeCValiDate == true)
        //            {
        //                VData tReCal = this.reCal(tLCPolSchema, tLCGetSet, tLCDutySet,
        //                                          tLCPremSet);
        //                tLCPremSet.set((LCPremSet) tReCal.get(0));
        //                tLCGetSet.set((LCGetSet) tReCal.get(1));
        //                tLCDutySet.set((LCDutySet) tReCal.get(2));
        //                tLCPolSchema = (LCPolSchema) tReCal.get(3);
        //            }
        //        }

        if (mInsuredAppAgeChangeFalg == true)
        {
            //return null;
        }

        tTransferData.setNameAndValue("InsuredAppAgeChangeFlag", ""
                + mInsuredAppAgeChangeFalg);

        // 保费项
        if (tLCPolSchema.getAppFlag().equals("9"))
        {
            LCPremDB tLCPremDB = new LCPremDB();
            tLCPremDB.setPolNo(tNewPolNo);
            tLCPremSet = tLCPremDB.query();
        }
        tLCPremSet = this.preparePrem(tLCPolSchema, tLCPremSet, tNewPolNo);

        if (tLCPremSet == null)
        {
            return null;
        }
        //  tmpMap .put(tLCPremSet , "INSERT");
        tReturn.add(tLCPremSet);

        // 领取项
        if (tLCPolSchema.getAppFlag().equals("9"))
        {
            LCGetDB tLCGetDB = new LCGetDB();
            tLCGetDB.setPolNo(tNewPolNo);
            tLCGetSet = tLCGetDB.query();
        }
        tLCGetSet = this.prepareGet(tLCPolSchema, tLCGetSet, tNewPolNo);

        if (tLCGetSet == null)
        {
            return null;
        }

        tReturn.add(tLCGetSet);

        // 责任
        if (tLCPolSchema.getAppFlag().equals("9"))
        {
            LCDutyDB tLCDutyDB = new LCDutyDB();
            tLCDutyDB.setPolNo(tNewPolNo);
            tLCDutySet = tLCDutyDB.query();
        }
        tLCDutySet = this.prepareDuty(tLCPolSchema, tLCDutySet, tNewPolNo,
                tLCPremSet);

        if (tLCDutySet == null)
        {
            return null;
        }
        tReturn.add(tLCDutySet);

        // 险种保单（需要处理的投保单）
        LCPolSchema tLCPolMain = this.preparePol(tLCPolSchema, tNewPolNo,
                tLCDutySet);

        if (tLCPolMain == null)
        {
            return null;
        }

        tReturn.add(tLCPolMain);

        tReturn.add(tmpMap);
        tReturn.add(tTransferData);

        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean getInputData()
    {
        //全局变量
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);

        //@@可以处理险种保单集和单个险种保单的情况
        LCPolSet tLCPolSet = (LCPolSet) mInputData.getObjectByObjectName(
                "LCPolSet", 1);
        mNewContNo = (String) mInputData.getObjectByObjectName("String", 2);
        //        System.out.println("newContNo :" + mNewContNo);
        mBQData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);

        if (tLCPolSet != null && tLCPolSet.size() > 0)
        {
            mLCPolSet.set(tLCPolSet);
        }
        else
        {
            LCPolSchema tSchema = (LCPolSchema) mInputData
                    .getObjectByObjectName("LCPolSchema", 0);
            if (tSchema != null)
            {
                mLCPolSet.add(tSchema);
            }
        }

        if (mLCPolSet.size() <= 0)
        {
            return false;
        }
        return true;

    }

    /**
     * 校验数据是否合法 输出：如果发生错误则返回false,否则返回true
     *
     * @param tLCPolSchema LCPolSchema
     * @return boolean
     */
    private boolean checkOnePol(LCPolSchema tLCPolSchema)
    {

        //检验代理人是否已经离职
        ExeSQL tExeSQL = new ExeSQL();
        String state = tExeSQL
                .getOneValue("SELECT agentstate FROM laagent where agentcode='"
                        + tLCPolSchema.getAgentCode() + "'");

        if ((state == null) || state.equals(""))
        {
            CError.buildErr(this, "无法查到该代理人的状态!");
            return false;
        }
        if (!mBQFlag && Integer.parseInt(state) >= 6)
        {
            CError.buildErr(this, "该代理人已经离职，不能签单!");
            return false;
        }

        return true;
    }

    /**
     * 准备公用信息 输出：如果发生错误则返回false,否则返回true
     *
     * @param tLCPolSchema LCPolSchema
     * @return String
     */
    private String preparePubInfo(LCPolSchema tLCPolSchema)
    {
        /** 预打保单不换号 */
        if (tLCPolSchema.getAppFlag().equals("9"))
        {
            return tLCPolSchema.getPolNo();
        }
        // 生成险种保单号
        String tLimit = PubFun.getNoLimit(tLCPolSchema.getManageCom());
        String tPolNo = PubFun1.CreateMaxNo("POLNO", tLimit);

        if (StrTool.cTrim(tPolNo).equals(""))
        {
            // @@错误处理
            CError.buildErr(this, "险种保单号生成失败!");
            return null;
        }

        return tPolNo;
    }

    /**
     * 准备保费项信息 输出：如果发生错误则返回false,否则返回true
     *
     * @param tLCPolSchema LCPolSchema
     * @param tNewPolNo String
     * @return LCPremSet
     */
    private LCPremSet getOldPrem(LCPolSchema tLCPolSchema, String tNewPolNo)
    {
        String tOldPolNo = tLCPolSchema.getProposalNo();
        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setPolNo(tOldPolNo);

        LCPremSet tLCPremSet = tLCPremDB.query();
        if (tLCPremSet.size() <= 0)
        {
            tLCPremDB.setPolNo(tLCPolSchema.getPolNo());
            tLCPremSet = tLCPremDB.query();
        }
        if (tLCPremSet == null || tLCPremSet.size() <= 0)
        {
            // @@错误处理
            CError.buildErr(this, "LCPrem表取数失败!");
            return null;
        }
        if (tLCPolSchema.getAppFlag().equals("9")
                && tLCPolSchema.getContType().equals("1"))
        {
            tLCPremDB.setPolNo(tLCPolSchema.getPolNo());
            LCPremSet tLCPremSet2 = tLCPremDB.query();
            if (tLCPremSet2 != null && tLCPremSet2.size() > 0)
            {
                tLCPremSet.add(tLCPremSet2);
            }
        }
        return tLCPremSet;
    }

    /**
     * 准备保费项信息 输出：如果发生错误则返回false,否则返回true
     *
     * @param tLCPolSchema LCPolSchema
     * @param tLCPremSet LCPremSet
     * @param tNewPolNo String
     * @return LCPremSet
     */
    private LCPremSet preparePrem(LCPolSchema tLCPolSchema,
            LCPremSet tLCPremSet, String tNewPolNo)
    {
        int n = tLCPremSet.size();

        for (int i = 1; i <= n; i++)
        {
            LCPremSchema tLCPremSchema = tLCPremSet.get(i);

            tLCPremSchema.setPolNo(tNewPolNo);
            tLCPremSchema.setSumPrem(tLCPremSchema.getPrem());
            sumPrem += tLCPremSchema.getPrem();

            // 计算交至日期
            Date baseDate = fDate.getDate(tLCPremSchema.getPayStartDate());
            Date paytoDate = null;
            System.out.println("tLCPremSchema.getPayPlanCode()"
                    + tLCPremSchema.getPayPlanCode());
            String tPayPlanCode = tLCPremSchema.getPayPlanCode()
                    .substring(0, 6);
            System.out.println("tPayPlanCode" + tPayPlanCode);
            if (tLCPremSchema.getPayIntv() == 0
                    && (!StrTool.cTrim(tPayPlanCode).equals("000000")))
            { // 趸交按照交费终止日期计算
                paytoDate = fDate.getDate(tLCPremSchema.getPayEndDate());
            }
            else if (tLCPremSchema.getPayIntv() == 0
                    && StrTool.cTrim(tPayPlanCode).equals("000000"))
            {
                if (!StrTool.cTrim(tLCPremSchema.getPayEndDate()).equals(""))
                {
                    paytoDate = fDate.getDate(tLCPremSchema.getPayEndDate());
                }
                else
                {
                    paytoDate = fDate.getDate("3000-01-01");
                }
            }
            else
            {
                int interval = tLCPremSchema.getPayIntv();

                if (interval == -1)
                {
                    //interval = 0; // 不定期缴费按照交费日期计算
                	paytoDate = fDate.getDate(tLCPremSchema.getPayEndDate());
                }
                else
                {
                String unit = "M";
                paytoDate = PubFun.calDate(baseDate, interval, unit, null);
                System.out.println("PayToDate : " + paytoDate);
                /** 处理失效日期和交至日期的问题 */
                paytoDate = paytoDate.before(fDate.getDate(tLCPolSchema
                        .getEndDate())) ? paytoDate : fDate
                        .getDate(tLCPolSchema.getEndDate());
                System.out.println("终交日期 : " + fDate.getString(paytoDate));
                }
            }

            tLCPremSchema.setPayTimes(1);
            tLCPremSchema.setContNo(mNewContNo);
            tLCPremSchema.setPaytoDate(fDate.getString(paytoDate));
            tLCPremSchema.setModifyDate(PubFun.getCurrentDate());
            tLCPremSchema.setModifyTime(PubFun.getCurrentTime());
            tLCPremSchema.setOperator(mGlobalInput.Operator);
            //tLCPremSchema.setMakeDate( );
            // 集体投保单

            tLCPremSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
            tLCPremSchema.setMakeDate(tLCPolSchema.getMakeDate());
            tLCPremSchema.setMakeTime(tLCPolSchema.getMakeTime());
            tLCPremSet.set(i, tLCPremSchema);
        }
        if (tLCPremSet == null || tLCPremSet.size() <= 0)
        {
            return null;
        }
        return tLCPremSet;
    }

    /**
     * 准备领取项信息 输出：如果发生错误则返回false,否则返回true
     *
     * @param tLCPolSchema LCPolSchema
     * @param tNewPolNo String
     * @return LCGetSet
     */
    private LCGetSet getOldGet(LCPolSchema tLCPolSchema, String tNewPolNo)
    {
        String tOldPolNo = tLCPolSchema.getProposalNo();
        LCGetDB tLCGetDB = new LCGetDB();
        tLCGetDB.setPolNo(tOldPolNo);

        LCGetSet tLCGetSet = tLCGetDB.query();
        //如ProposalNo查不出来，用PolNo查询。与LCPrem和LCDuty统一
        if (tLCGetSet.size() <= 0)
        {
            tLCGetDB.setPolNo(tLCPolSchema.getPolNo());
            tLCGetSet = tLCGetDB.query();
        }

        if (tLCGetDB.mErrors.needDealError())
        {
            // @@错误处理
            CError.buildErr(this, "LCGet表取数失败!");
            return null;
        }
        if (tLCPolSchema.getAppFlag().equals("9"))
        {
            tLCGetDB.setPolNo(tLCPolSchema.getPolNo());
            LCGetSet tLCGetSet2 = tLCGetDB.query();
            if (tLCGetSet2 != null && tLCGetSet2.size() > 0)
            {
                tLCGetSet.add(tLCGetSet2);
            }
        }
        return tLCGetSet;
    }

    /**
     * 准备领取项信息 输出：如果发生错误则返回false,否则返回true
     *
     * @param tLCPolSchema LCPolSchema
     * @param tLCGetSet LCGetSet
     * @param tNewPolNo String
     * @return LCGetSet
     */
    private LCGetSet prepareGet(LCPolSchema tLCPolSchema, LCGetSet tLCGetSet,
            String tNewPolNo)
    {
        int n = tLCGetSet.size();

        for (int i = 1; i <= n; i++)
        {
            LCGetSchema tLCGetSchema = tLCGetSet.get(i);

            tLCGetSchema.setPolNo(tNewPolNo);

            tLCGetSchema.setContNo(mNewContNo);
            tLCGetSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
            tLCGetSchema.setMakeDate(tLCPolSchema.getMakeDate());
            tLCGetSchema.setMakeTime(tLCPolSchema.getMakeTime());
            tLCGetSchema.setOperator(mGlobalInput.Operator);
            tLCGetSchema.setModifyDate(PubFun.getCurrentDate());
            tLCGetSchema.setModifyTime(PubFun.getCurrentTime());

            tLCGetSet.set(i, tLCGetSchema);
        }

        return tLCGetSet;
    }

    /**
     * 准备责任信息 输出：如果发生错误则返回false,否则返回true
     *
     * @param tLCPolSchema LCPolSchema
     * @param tNewPolNo String
     * @return LCDutySet
     */
    private LCDutySet getOldDuty(LCPolSchema tLCPolSchema, String tNewPolNo)
    {
        String tOldPolNo = tLCPolSchema.getProposalNo();

        LCDutyDB tLCDutyDB = new LCDutyDB();
        tLCDutyDB.setPolNo(tOldPolNo);

        LCDutySet tLCDutySet = tLCDutyDB.query();
        if (tLCDutySet.size() <= 0)
        {
            tLCDutyDB.setPolNo(tLCPolSchema.getPolNo());
            tLCDutySet = tLCDutyDB.query();
        }
        //        if (tLCDutyDB.mErrors.needDealError()) {
        //            CError.buildErr(this, "LCDuty表取数失败!");
        //            return null;
        //        }
        if (tLCDutySet == null || tLCDutySet.size() <= 0)
        {
            return null;
        }
        if (tLCPolSchema.getAppFlag().equals("9"))
        {
            tLCDutyDB.setPolNo(tLCPolSchema.getPolNo());
            LCDutySet tLCDutySet2 = tLCDutyDB.query();
            if (tLCDutySet2 != null && tLCDutySet2.size() > 0)
            {
                tLCDutySet.add(tLCDutySet2);
            }
        }
        return tLCDutySet;
    }

    /**
     * 准备责任信息 输出：如果发生错误则返回false,否则返回true
     *
     * @param tLCPolSchema LCPolSchema
     * @param tLCDutySet LCDutySet
     * @param tNewPolNo String
     * @param tLCPremSet LCPremSet
     * @return LCDutySet
     */
    private LCDutySet prepareDuty(LCPolSchema tLCPolSchema,
            LCDutySet tLCDutySet, String tNewPolNo, LCPremSet tLCPremSet)
    {
        int n = tLCDutySet.size();

        for (int i = 1; i <= n; i++)
        {
            LCDutySchema tLCDutySchema = tLCDutySet.get(i);

            tLCDutySchema.setPolNo(tNewPolNo);
            tLCDutySchema.setSumPrem(tLCDutySchema.getPrem());

            // 交至日期
            Date maxPaytoDate = fDate.getDate("1900-01-01");
            int m = tLCPremSet.size();

            for (int j = 1; j <= m; j++)
            {
                LCPremSchema tLCPremSchema = tLCPremSet.get(j);

                if (tLCDutySchema.getDutyCode().trim().equals(
                        tLCPremSchema.getDutyCode().trim())
                        && fDate.getDate(tLCPremSchema.getPaytoDate()).after(
                                maxPaytoDate))
                {
                    maxPaytoDate = fDate.getDate(tLCPremSchema.getPaytoDate());
                    System.out.println("maxPaytoDate1:" + maxPaytoDate);
                }
            }
            /** 处理失效日期和交至日期的问题 */
            maxPaytoDate = maxPaytoDate.before(fDate.getDate(tLCPolSchema
                    .getEndDate())) ? maxPaytoDate : fDate.getDate(tLCPolSchema
                    .getEndDate());
            // System.out.println("maxPaytoDate2:" + maxPaytoDate);
            tLCDutySchema.setPaytoDate(maxPaytoDate);
            tLCDutySchema.setContNo(mNewContNo);
            //?            tLCDutySchema.setFirstPayDate(tLCPolSchema.getCValiDate());
            tLCDutySchema.setMakeDate(tLCPolSchema.getMakeDate());
            tLCDutySchema.setMakeTime(tLCPolSchema.getMakeTime());
            tLCDutySchema.setOperator(mGlobalInput.Operator);
            tLCDutySchema.setModifyDate(PubFun.getCurrentDate());
            tLCDutySchema.setModifyTime(PubFun.getCurrentTime());

            tLCDutySet.set(i, tLCDutySchema);
        }
        if (tLCDutySet == null || tLCDutySet.size() <= 0)
        {
            return null;
        }
        return tLCDutySet;
    }

    /**
     * 准备险种保单信息 输出：如果发生错误则返回false,否则返回true
     *
     * @param tLCPolSchema LCPolSchema
     * @param tNewPolNo String
     * @param tLCDutySet LCDutySet
     * @return LCPolSchema
     */
    private LCPolSchema preparePol(LCPolSchema tLCPolSchema, String tNewPolNo,
            LCDutySet tLCDutySet)
    {

        if (tLCPolSchema.getMainPolNo().equals(tLCPolSchema.getPolNo()))
        {
            tLCPolSchema.setMainPolNo(tNewPolNo);
        }
        tLCPolSchema.setPolNo(tNewPolNo);
        tLCPolSchema.setContNo(mNewContNo);
        tLCPolSchema.setSignCom(mGlobalInput.ManageCom);
        tLCPolSchema.setSumPrem(tLCPolSchema.getPrem());
        tLCPolSchema.setLeavingMoney(mLeft);
        //在合同统一处理
        tLCPolSchema.setSignDate("");
        tLCPolSchema.setSignTime("");
        tLCPolSchema.setLastRevDate(tLCPolSchema.getCValiDate()); // 把最近复效日期置为起保日期
        //   if (!mNewGrpContNo.equals("")) tLCPolSchema.setGrpContNo(mNewGrpContNo) ;
        if (StrTool.cTrim(this.mOperate).equals("PREVIEW"))
        {
            tLCPolSchema.setAppFlag("9");
        }
        else
        {
            tLCPolSchema.setAppFlag("1");
            tLCPolSchema.setStateFlag("1");
        }
        tLCPolSchema.setPolState("00019999"); //sxy-2003-09-08(polstatus编码规则:前两位描述险种保单状态,后两位描述使险种保单处于该状态的原因.末尾四位描述了前一险种保单状态和处于该状态的原因)

        // 交至日期
        Date maxPaytoDate = null;
        Date PayEndDate = null;
        for (int j = 1; j <= tLCDutySet.size(); j++)
        {
            LCDutySchema tLCDutySchema = tLCDutySet.get(j);

            if (maxPaytoDate == null
                    || fDate.getDate(tLCDutySchema.getPaytoDate()).after(
                            maxPaytoDate))
            {
                maxPaytoDate = fDate.getDate(tLCDutySchema.getPaytoDate());
            }
            if (PayEndDate == null
                    || PayEndDate.before(fDate.getDate(tLCDutySchema
                            .getPayEndDate())))
            {
                PayEndDate = fDate.getDate(tLCDutySchema.getPayEndDate());
            }
        }
        /** 交至日期 */
        maxPaytoDate = maxPaytoDate.before(fDate.getDate(tLCPolSchema
                .getEndDate())) ? maxPaytoDate : fDate.getDate(tLCPolSchema
                .getEndDate());

        //交至日期
        tLCPolSchema.setPaytoDate(fDate.getString(maxPaytoDate));
        tLCPolSchema.setPayEndDate(fDate.getString(PayEndDate));
        //从暂交费信息中取firstpaydate日期信息
        getPaydates(tLCPolSchema);
        tLCPolSchema.setFirstPayDate(this.mFirstPayDate);

        tLCPolSchema.setModifyDate(PubFun.getCurrentDate());
        tLCPolSchema.setModifyTime(PubFun.getCurrentTime());

        //查询LMRiskApp,看是否从暂缴费表中读取银行的账号和户名--并且是个人
        //mNeedReadBankFlag 在前面已经付过值
        //        if ((mNeedReadBankFlag != null) && mNeedReadBankFlag.equals("1") &&
        //            mPolType.equals("1"))
        //        {
        //           // System.out.println("个人险种保单标记:" + mPolType);
        //
        //            GetPayType tGetPayType = new GetPayType();
        //
        //            if (tGetPayType.getPayTypeForLCPol(tLCPolSchema.getPrtNo(), 4) == false)
        //            {
        //            }
        //            else
        //            {
        //                /*Lis5.3 upgrade set
        //                 tLCPolSchema.setBankAccNo(tGetPayType.getBankAccNo());
        //                 tLCPolSchema.setAccName(tGetPayType.getAccName());
        //                 */
        //            }
        //        }

        return tLCPolSchema;
    }

    /**
     * 初始化类属性 输出：无
     *
     * @return boolean
     */
    private boolean initV()
    {
        mLJAPaySchema = new LJAPaySchema();
        mRiskFlag = "";
        mNeedReadBankFlag = "";
        mSameBatch = "0"; // 主附险是否属于同一批 "0"--非同一批 "1"--同一批
        mPolType = "1"; // 1-个人单 2-集体下的个人单 3-合同下的个人单
        mLeft = 0;
        mFirstPayDate = null;
        mMainPolNo = null;
        sumMoney = 0; // 总的交费额
        maxPayDate = null;
        maxEnterAccDate = null;
        mInsuredAppAgeChangeFalg = false;
        alreadyChangeCValiDate = false; //是否已经更改过生效日期
        mBQFlag = false; //保全标记初始化置为假
        mLMRiskAppSchema = null;
        mNotCreatInsuaccFlag = false;

        // 生成流水号
        if (mSerialNo.trim().equals(""))
        {
            String tLimit = "";
            System.out
                    .println("ProposalSignBL.initV()  \n--Line:993  --Author:YangMing");
            System.out.println("测试管理机构");
            System.out.println("ManageCom : " + this.mGlobalInput);
            tLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
            mSerialNo = PubFun1.CreateMaxNo("SerialNo", tLimit);
            if (mSerialNo.trim().equals(""))
            {
                CError.buildErr(this, "流水号生成失败!");
                return false;
            }
        }
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("ChangeSubValiDate");
        if (tLDSysVarDB.getInfo() == false)
        {
            CError.buildErr(this, "系统变量表没有ChangeSubValiDate变量！");
            return false;
        }
        mLDSysVarSchema = tLDSysVarDB.getSchema();
        return true;
    }

    /**
     * 判断是否需要追溯险种保单生效日期 输出：boolean
     *
     * @param tLCPolSchema LCPolSchema
     * @return boolean
     */
    private boolean needReCal(LCPolSchema tLCPolSchema)
    {
        mInsuredAppAgeChangeFalg = false;

        //如果附险的生效日期随主险，那么直接跳过，不用重新计算
        if (alreadyChangeCValiDate == true)
        {
            return false;
        }
        LMRiskAppSchema tLMRiskAppSchema = mCRI.findRiskAppByRiskCodeClone(tLCPolSchema.getRiskCode());
        if("G".equals(tLMRiskAppSchema.getRiskProp()) && ("4".equals(tLMRiskAppSchema.getRiskType4()) 
        		|| tLCPolSchema.getRiskCode().equals("170501")
        		|| tLCPolSchema.getRiskCode().equals("162501")
        		|| tLCPolSchema.getRiskCode().equals("162401")
        		|| tLCPolSchema.getRiskCode().equals("162601")
        		|| tLCPolSchema.getRiskCode().equals("162801")
        		)){
        	//团体万能追溯到签单次日 不需要管生日是否变化
        	mValiDate = this.calValiDate(tLCPolSchema, "1");
        	return true;
        }
        
        if (mPolType.equals("1"))
        { // 个人单才需要进行追溯
            LDSysVarDB tLDSysVarDB = new LDSysVarDB();
            tLDSysVarDB.setSysVar("SignValiDateBackFlag");
            tLDSysVarDB.getInfo();
            mBack = tLDSysVarDB.getSysVarValue().trim();

            if (mBack.equals("1") || mBack.equals("2") || mBack.equals("3"))
            {
                if (tLCPolSchema.getSpecifyValiDate() == null)
                { //如果为空，置为N
                    tLCPolSchema.setSpecifyValiDate("N");
                }

                if ((tLCPolSchema.getSpecifyValiDate() != null)
                        && tLCPolSchema.getSpecifyValiDate().equals("N"))
                {
                    mValiDate = this.calValiDate(tLCPolSchema, mBack);

                    int newAge = PubFun.calInterval(fDate.getDate(tLCPolSchema
                            .getInsuredBirthday()), mValiDate, "Y");

                    if (newAge == tLCPolSchema.getInsuredAppAge())
                    {
                        System.out.println("投保年龄未发生变化。");
                        return true;
                    }
                    else
                    {
                        mInsuredAppAgeChangeFalg = true;

                        CError.buildErr(this, tLCPolSchema.getPolNo()
                                + "号投保单以签单生效日期变更后，则被保险人年龄有变化，不能签单！");

                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * 改变险种保单生效日期 输出：LCPolSchema
     *
     * @param tLCPolSchema LCPolSchema
     * @param flag String
     * @return Date
     */
    private Date calValiDate(LCPolSchema tLCPolSchema, String flag)
    {
        Date tValiDate = null;

        // 选择签单次日
        if (flag.equals("1"))
        {
            tValiDate = fDate.getDate(PubFun.getCurrentDate());
            tValiDate = PubFun.calDate(tValiDate, 1, "D", null);
        }

        // 选择交费的到帐日期的最大值
        if (flag.equals("2") || flag.equals("3"))
        {
            Date maxDate = fDate.getDate("1900-01-01");
            Date minDate = fDate.getDate("3000-01-01");

            LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
            tLJTempFeeDB.setOtherNo(tLCPolSchema.getProposalNo());
            tLJTempFeeDB.setOtherNoType("0");
            tLJTempFeeDB.setTempFeeType("1");
            tLJTempFeeDB.setConfFlag("0");

            LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.query();

            if (tLJTempFeeDB.mErrors.needDealError())
            {
                CError.buildErr(this, "财务信息取数失败!");

                return null;
            }

            int n = tLJTempFeeSet.size();

            for (int i = 1; i <= n; i++)
            {
                LJTempFeeSchema tLJTempFeeSchema = tLJTempFeeSet.get(i);

                Date enterAccDate = fDate.getDate(tLJTempFeeSchema
                        .getEnterAccDate());

                if (enterAccDate.before(minDate))
                {
                    minDate = enterAccDate;
                }

                if (enterAccDate.after(maxDate))
                {
                    maxDate = enterAccDate;
                }
            }

            if (flag.equals("2"))
            {
                tValiDate = PubFun.calDate(maxDate, 1, "D", null);
            }

            if (flag.equals("3"))
            {
                tValiDate = PubFun.calDate(minDate, 1, "D", null);
            }
        }

        return tValiDate;
    }

    /**
     * 重新计算投保单 输出：VData
     *
     * @param tLCPolSchema LCPolSchema
     * @param tLCGetSet LCGetSet
     * @param tLCDutySet LCDutySet
     * @param tLCPremSet LCPremSet
     * @return VData
     */
    private VData reCal(LCPolSchema tLCPolSchema, LCGetSet tLCGetSet,
            LCDutySet tLCDutySet, LCPremSet tLCPremSet)
    {
        VData tReturn = new VData();
        LCPolBL tLCPolBL = new LCPolBL();
        LCDutyBLSet tLCDutyBLSet = new LCDutyBLSet();
        LCGetBLSet tLCGetBLSet = new LCGetBLSet();

        // 把保费项中加费的部分提出来
        String newStart = tLCPolSchema.getCValiDate();
        int premCount = tLCPremSet.size();

        for (int i = 1; i <= premCount; i++)
        {
            LCPremSchema tLCPremSchema = tLCPremSet.get(i);

            if (!tLCPremSchema.getPayPlanCode().substring(0, 6)
                    .equals("000000"))
            {
                tLCPremSet.remove(tLCPremSchema);
                premCount--;
                i--;
            }
            else
            {
                int chg = PubFun.calInterval(tLCPremSchema.getPayStartDate(),
                        newStart, "D");
                Date payStartDate = PubFun.calDate(fDate.getDate(tLCPremSchema
                        .getPayStartDate()), chg, "D", null);
                tLCPremSchema.setPayStartDate(fDate.getString(payStartDate));
                
                //修改加费终止日期，无法签单的Bug
                if(tLCPremSchema.getPayEndDate() != null && !"".equals(tLCPremSchema.getPayEndDate()))
                {
	                Date payEndDate = PubFun.calDate(fDate.getDate(tLCPremSchema
	                        .getPayEndDate()), chg, "D", null);
	                tLCPremSchema.setPayEndDate(fDate.getString(payEndDate));
                }
            }
        }
        
        int dutyCount = tLCDutySet.size();
 
        if (dutyCount == 1)
        {
            LCDutySchema tLCDutySchema = tLCDutySet.get(1);
            if(!"701005".equals(tLCDutySchema.getDutyCode())
            		&&!"738004".equals(tLCDutySchema.getDutyCode())
            		&&!"740005".equals(tLCDutySchema.getDutyCode())
            		&&!"460001".equals(tLCDutySchema.getDutyCode())
            		&&!"460002".equals(tLCDutySchema.getDutyCode())
            		){
            	tLCDutySchema.setDutyCode(null);
            }

            tLCDutySet.set(1, tLCDutySchema);
        }

        int getCount = tLCGetSet.size();

        for (int i = 1; i <= getCount; i++)
        {
            LCGetSchema tLCGetSchema = tLCGetSet.get(i);

            if (tLCGetSchema.getGetDutyKind() == null)
            {
                tLCGetSet.remove(tLCGetSchema);
                getCount--;
                i--;
            }
        }
        
        if("170501".equals(tLCPolSchema.getRiskCode())
        		||"370301".equals(tLCPolSchema.getRiskCode())
        		||"162501".equals(tLCPolSchema.getRiskCode())
        		||"162401".equals(tLCPolSchema.getRiskCode())
        		||"162601".equals(tLCPolSchema.getRiskCode())
        		||"162801".equals(tLCPolSchema.getRiskCode())
        		||"520901".equals(tLCPolSchema.getRiskCode())
        		){
        	for(int i=1;i<=tLCDutySet.size();i++){
        		tLCDutySet.get(i).setEndDate("");
        		tLCDutySet.get(i).setPaytoDate("");
        		tLCDutySet.get(i).setFirstPayDate("");
        	}
        }

        tLCPolBL.setSchema(tLCPolSchema);
        tLCDutyBLSet.set(tLCDutySet);
        tLCGetBLSet.set(tLCGetSet);

        CalBL tCalBL;

        if (getCount == 0)
        {
            tCalBL = new CalBL(tLCPolBL, tLCDutyBLSet, "");
        }
        else
        {
            tCalBL = new CalBL(tLCPolBL, tLCDutyBLSet, tLCGetBLSet, "");
        }

        if (tCalBL.calPol() == false)
        {
            CError.buildErr(this, "险种保单重新计算时失败:"
                    + tCalBL.mErrors.getFirstError());

            return null;
        }

        //取出计算的结果
        tLCPolSchema = tCalBL.getLCPol().getSchema();

        LCPremSet tLCPremSet1 = (LCPremSet) tCalBL.getLCPrem();
        tLCGetSet = (LCGetSet) tCalBL.getLCGet();
        tLCDutySet = (LCDutySet) tCalBL.getLCDuty();

        // 加入加费的部分
        double addPrem = 0;
        int dutyCount1 = tLCDutySet.size();

        for (int i = 1; i <= premCount; i++)
        {
            LCPremSchema tLCPremSchema = tLCPremSet.get(i);
            addPrem += tLCPremSchema.getPrem();

            for (int j = 1; j <= dutyCount1; j++)
            {
                LCDutySchema tLCDutySchema = tLCDutySet.get(j);

                if (tLCDutySchema.getDutyCode().equals(
                        tLCPremSchema.getDutyCode()))
                {
                    tLCDutySchema.setPrem(tLCPremSchema.getPrem()
                            + tLCDutySchema.getPrem());
                }
            }
        }

        tLCPolSchema.setPrem(tLCPolSchema.getPrem() + addPrem);
        tLCPremSet1.add(tLCPremSet);

        tReturn.add(tLCPremSet1);
        tReturn.add(tLCGetSet);
        tReturn.add(tLCDutySet);
        tReturn.add(tLCPolSchema);

        return tReturn;
    }

    /**
     * 重新计算投保单 输出：VData
     *
     * @param tLCPolSchema LCPolSchema
     * @param tLCGetSet LCGetSet
     * @param tLCDutySet LCDutySet
     * @param tLCPremSet LCPremSet
     * @return VData
     */
    private VData reCalULI(LCPolSchema tLCPolSchema, LCGetSet tLCGetSet,
            LCDutySet tLCDutySet, LCPremSet tLCPremSet)
    {
        VData tReturn = new VData();
        LCPolBL tLCPolBL = new LCPolBL();
        LCDutyBLSet tLCDutyBLSet = new LCDutyBLSet();
        LCGetBLSet tLCGetBLSet = new LCGetBLSet();

        // 重算保费相关日期
        String newStart = tLCPolSchema.getCValiDate();
        int premCount = tLCPremSet.size();

        for (int i = 1; i <= premCount; i++)
        {
        	LCPremSchema tLCPremSchema = tLCPremSet.get(i);

            int chg = PubFun.calInterval(tLCPremSchema.getPayStartDate(),newStart, "D");
            Date payStartDate = PubFun.calDate(fDate.getDate(tLCPremSchema.getPayStartDate()), chg, "D", null);
            Date payEndDate = PubFun.calDate(fDate.getDate(tLCPremSchema.getPayEndDate()), chg, "D", null);
            tLCPremSchema.setPayStartDate(fDate.getString(payStartDate));
            tLCPremSchema.setPayEndDate(fDate.getString(payEndDate));
        }
        
        int dutyCount = tLCDutySet.size();
        int getCount = tLCGetSet.size();

        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(tLCPolSchema.getContNo());
        tLCInsuredDB.setInsuredNo(tLCPolSchema.getInsuredNo());
        tLCInsuredDB.getInfo();
        if (!tLCInsuredDB.getInfo())
        {
            CError.buildErr(this, "查询险种描述表失败！");
            return null;
        }
        
        String tGetStartDate = "";
    	for(int i = 1; i <= getCount; i++){
    		if(tLCGetSet.get(i).getGetDutyKind() != null && !tLCGetSet.get(i).getGetDutyKind().equals("")){
    			tGetStartDate = tLCGetSet.get(i).getGetStartDate();
    			break;
    		}
    	}
        
        for (int i = 1; i <= dutyCount; i++){
        	LCDutySchema tLCDutySchema = tLCDutySet.get(i);
        	int getYear = 0;
        	if(!tGetStartDate.equals("")){
        		getYear = PubFun.calInterval(tLCInsuredDB.getBirthday(), tGetStartDate, "Y");
        		tLCDutySchema.setGetYear(getYear);
        		tLCDutySchema.setGetYearFlag("A");
        	}        	
        }
        
        tLCPolSchema.setPrem(0);
        tLCPolSchema.setStandPrem(0);
        tLCPolSchema.setSumPrem(0);
        tLCPolBL.setSchema(tLCPolSchema);
        tLCDutyBLSet.set(tLCDutySet);
        tLCGetBLSet.set(tLCGetSet);

        com.sinosoft.lis.ulitb.CalBL tCalBL;

        if (getCount == 0)
        {
            tCalBL = new com.sinosoft.lis.ulitb.CalBL(tLCPolBL, tLCDutyBLSet, "");
        }
        else
        {
            tCalBL = new com.sinosoft.lis.ulitb.CalBL(tLCPolBL, tLCDutyBLSet, tLCGetBLSet, "");
        }

        if (tCalBL.calPol() == false)
        {
            CError.buildErr(this, "险种保单重新计算时失败:"
                    + tCalBL.mErrors.getFirstError());

            return null;
        }

        //取出计算的结果
        Reflections  tReflections = new Reflections();
        tReflections.transFields(tLCPolSchema, tCalBL.getLCPol().getSchema());
        

        LCPremSet tLCPremSet1 = (LCPremSet) tCalBL.getLCPrem();
        tLCGetSet = (LCGetSet) tCalBL.getLCGet();
        tLCDutySet = (LCDutySet) tCalBL.getLCDuty();

        tReturn.add(tLCPremSet1);
        tReturn.add(tLCGetSet);
        tReturn.add(tLCDutySet);
        tReturn.add(tLCPolSchema);

        return tReturn;
    }    
    
    
    /**
     * 处理账户类险种的账户信息
     * @param PolDetail VData
     * @param inLCPolSchema LCPolSchema
     * @param inLMRiskSchema LMRiskSchema
     * @return boolean
     */
    private boolean DealAccount(VData PolDetail, LCPolSchema inLCPolSchema,
            LMRiskSchema inLMRiskSchema)
    {

        if (inLMRiskSchema == null)
        {
            //查询险种描述表：是否和帐户相关
            LMRiskDB tLMRiskDB = new LMRiskDB();
            tLMRiskDB.setRiskCode(inLCPolSchema.getRiskCode());

            if (tLMRiskDB.getInfo() == false)
            {
                CError.buildErr(this, "查询险种描述表失败！");

                return false;
            }

            inLMRiskSchema = tLMRiskDB.getSchema();
        }

        if ((inLMRiskSchema.getInsuAccFlag() == null)
                || inLMRiskSchema.getInsuAccFlag().equals("N"))
        {
            return true;
        }

        String Rate = null; //分配比率置空，待调整

        LCPremSet tLCPremSet = (LCPremSet) PolDetail.getObjectByObjectName(
                "LCPremSet", 0);
        DealAccount tDealAccount = new DealAccount();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("AccCreatePos", "1"); //生成位置：承保时
        tTransferData.setNameAndValue("OtherNoType", "1"); //其它号码类型：个人险种保单号
        tTransferData.setNameAndValue("Rate", Rate);

        //注意：此时数据尚未存入数据库,因此，暂时用投保单号代替保单号。
        tTransferData.setNameAndValue("PolNo", inLCPolSchema.getProposalNo());
        tTransferData.setNameAndValue("OtherNo", inLCPolSchema.getProposalNo());

        //生成帐户结构
        VData tVData = tDealAccount.createInsureAccForBat(tTransferData,
                inLCPolSchema, inLMRiskSchema);

        if (tDealAccount.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tDealAccount.mErrors);
            return false;
        }
        //计算并填充账户结构
        if (tVData == null)
        {
            CError.buildErr(this, "创建账户结构失败");
            return false;
        }

        //创建管理费结构表
        LCInsureAccSet tmpLCInsureAccSet = (LCInsureAccSet) (tVData
                .getObjectByObjectName("LCInsureAccSet", 0));

        LCInsureAccSet tLCInsureAccSet = (LCInsureAccSet) (tVData
                .getObjectByObjectName("LCInsureAccSet", 0));
        LCPremToAccSet tLCPremToAccSet = (LCPremToAccSet) (tVData
                .getObjectByObjectName("LCPremToAccSet", 0));
        LCGetToAccSet tLCGetToAccSet = (LCGetToAccSet) (tVData
                .getObjectByObjectName("LCGetToAccSet", 0));
        LCInsureAccTraceSet tLCInsureAccTraceSet = (LCInsureAccTraceSet) (tVData
                .getObjectByObjectName("LCInsureAccTraceSet", 0));
        LCInsureAccClassSet tLCInsureAccClassSet = (LCInsureAccClassSet) (tVData
                .getObjectByObjectName("LCInsureAccClassSet", 0));
        if (tLCInsureAccTraceSet == null)
        {
            tLCInsureAccTraceSet = new LCInsureAccTraceSet();
            tVData.add(tLCInsureAccTraceSet);
        }
        LCInsureAccFeeSet tLCInsureAccFeeSet = new LCInsureAccFeeSet();
        LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = new LCInsureAccClassFeeSet();
        ;
        LCInsureAccFeeTraceSet tLCInsureAccFeeTraceSet = new LCInsureAccFeeTraceSet();
        ;
        VData feeData = null;
        //创建管理费结构
        feeData = tDealAccount.getManageFeeStru(inLCPolSchema, tLCPremToAccSet,
                tmpLCInsureAccSet);
        if (feeData != null)
        {
            tLCInsureAccFeeSet = (LCInsureAccFeeSet) feeData
                    .getObjectByObjectName("LCInsureAccFeeSet", 0);
            tLCInsureAccClassFeeSet = (LCInsureAccClassFeeSet) feeData
                    .getObjectByObjectName("LCInsureAccClassFeeSet", 0);

        }
        tVData.add(tLCInsureAccFeeSet);
        tVData.add(tLCInsureAccClassFeeSet);
        tVData.add(tLCInsureAccFeeTraceSet);

        //计算管理费和账户注入资金
        tVData.add(tLCPremSet);
        tVData.add(inLCPolSchema);

        // 传入帐户制定的相关要素
        String sql = "select distinct a.grpcontno,a.riskcode,b.contplancode,a.poltypeflag from "
                + "lcpol a,lcinsured b where proposalno='"
                + inLCPolSchema.getProposalNo()
                + "' and a.insuredno=b.insuredno and a.grpcontno=b.grpcontno";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS == null)
        {
            CError.buildErr(this, "查询团体合同信息,被保险人信息,保险计划信息失败!");
            return false;
        }
        String[][] Data = tSSRS.getAllData();
        if (Data.length > 1)
        {
            CError.buildErr(this, "被保险人同一险种下保险计划信息出现冗余!");
            return false;
        }
        LCContPlanDutyParamDB tLCContPlanDutyParamDB = new LCContPlanDutyParamDB();
        tLCContPlanDutyParamDB.setGrpContNo(Data[0][0]);
        tLCContPlanDutyParamDB.setRiskCode(Data[0][1]);
        tLCContPlanDutyParamDB.setContPlanCode(Data[0][2]);
        LCContPlanDutyParamSet tLCContPlanDutyParamSet = tLCContPlanDutyParamDB
                .query();
        if (tLCContPlanDutyParamSet == null)
        {
            CError.buildErr(this, "保障计划要素查询失败。");
            return false;
        }
        tVData.add(tLCContPlanDutyParamSet);
        // -------------------------

        CManageFee cmanageFee = new CManageFee();
        boolean cal = cmanageFee.calPremManaFee(tVData, "1", inLCPolSchema
                .getPolNo(), "1", "BF");

        if (!cal || cmanageFee.mErrors.needDealError())
        {
            if (cmanageFee.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(cmanageFee.mErrors);
            }
            else
            {
                CError.buildErr(this, "计算账户金额或管理费时出错");
            }
            return false;
        }

        //add by zjd  险种 170501 账户表中保额替换保费
        if("170501".equals(inLCPolSchema.getRiskCode())
        		||"162501".equals(inLCPolSchema.getRiskCode())
        		||"162401".equals(inLCPolSchema.getRiskCode())){
        	for(int m=1;m<=tLCInsureAccSet.size();m++){
        		tLCInsureAccSet.get(m).setInsuAccBala(inLCPolSchema.getAmnt());
        		tLCInsureAccSet.get(m).setSumPay(inLCPolSchema.getAmnt());
        		tLCInsureAccSet.get(m).setLastAccBala(inLCPolSchema.getAmnt());
        		System.out.println("执行了险种"+ inLCPolSchema.getRiskCode() +"账户表保费变保额操作！");
        	}
        	for(int m=1;m<=tLCInsureAccTraceSet.size();m++){
        		tLCInsureAccTraceSet.get(m).setMoney((inLCPolSchema.getAmnt()));
        		System.out.println("执行了险种"+ inLCPolSchema.getRiskCode() +"账户轨迹表保费变保额操作！");
        	}
        	for(int m=1;m<=tLCInsureAccClassSet.size();m++){
        		tLCInsureAccClassSet.get(m).setInsuAccBala(inLCPolSchema.getAmnt());
        		tLCInsureAccClassSet.get(m).setSumPay(inLCPolSchema.getAmnt());
        		tLCInsureAccClassSet.get(m).setLastAccBala(inLCPolSchema.getAmnt());
        		System.out.println("执行了险种"+ inLCPolSchema.getRiskCode() +"账户分类表保费变保额操作！");
        	}
        	for(int m=1;m<=tLCInsureAccFeeSet.size();m++){
        		tLCInsureAccFeeSet.get(m).setFee(inLCPolSchema.getPrem()-inLCPolSchema.getAmnt());
        		System.out.println("执行了险种"+ inLCPolSchema.getRiskCode() +"保额保费差值存储到LCInsureAccFee表中");
        	}
        	for(int m=1;m<=tLCInsureAccClassFeeSet.size();m++){
        		tLCInsureAccClassFeeSet.get(m).setFee(inLCPolSchema.getPrem()-inLCPolSchema.getAmnt());
        		System.out.println("执行了险种"+ inLCPolSchema.getRiskCode() +"保额保费差值存储到LCInsureAccClassFee表中");
        	}
        }
        
        MMap tmpMap = (MMap) PolDetail.getObjectByObjectName("MMap", 0);
        tmpMap.put(tLCInsureAccSet, this.INSERT);
        tmpMap.put(tLCPremToAccSet, this.INSERT);
        tmpMap.put(tLCGetToAccSet, this.INSERT);
        tmpMap.put(tLCInsureAccClassSet, this.INSERT);
        tmpMap.put(tLCInsureAccTraceSet, this.INSERT);
        tmpMap.put(tLCInsureAccFeeSet, this.INSERT);
        tmpMap.put(tLCInsureAccClassFeeSet, this.INSERT);
        tmpMap.put(tLCInsureAccFeeTraceSet, this.INSERT);
        tmpMap.put(createAccBalanceInfo(tLCInsureAccSet), SysConst.INSERT);

        return true;
    }

    /**
     * createAccBalanceInfo
     *
     * @param cLCInsureAccSet LCInsureAccSet
     * @return Object
     */
    private LCInsureAccBalanceSet createAccBalanceInfo(
            LCInsureAccSet cLCInsureAccSet)
    {
        if (cLCInsureAccSet == null)
        {
            return null;
        }

        LCInsureAccBalanceSet set = new LCInsureAccBalanceSet();
        Reflections ref = new Reflections();
        for (int i = 1; i <= cLCInsureAccSet.size(); i++)
        {

            LCInsureAccBalanceSchema schema = new LCInsureAccBalanceSchema();
            ref.transFields(schema, cLCInsureAccSet.get(i));
            schema.setSequenceNo(cLCInsureAccSet.get(i).getPolNo()
                    + cLCInsureAccSet.get(i).getInsuAccNo());
            schema.setBalaCount(0);
            schema.setDueBalaDate(cLCInsureAccSet.get(i).getBalaDate());
            schema.setDueBalaTime("00:00:00");
            schema.setRunDate(schema.getMakeDate());
            schema.setRunTime(schema.getMakeTime());
            schema.setPolYear(1);
            schema.setPolMonth(1);
            schema
                    .setInsuAccBalaBefore(cLCInsureAccSet.get(i)
                            .getInsuAccBala());
            schema.setInsuAccBalaAfter(cLCInsureAccSet.get(i).getInsuAccBala());
            schema.setInsuAccBalaGurat(cLCInsureAccSet.get(i).getInsuAccBala());
            schema.setPrintCount(0);
            set.add(schema);
        }

        return set;
    }

    /**
     * 生成紧紧需要更新合同号的表的sql
     * @param oldPolNo String
     * @param newContNo String
     * @param newPolNo String
     * @return MMap
     */
    private MMap getElseUpdateTableSql(String oldPolNo, String newContNo,
            String newPolNo)
    {
        Vector sqlVector = new Vector();
        String wherepart = " PolNo='" + oldPolNo + "'";
        String[] tables1 = { "LCSpecNote" };
        String condition = " polno='" + newPolNo + "'";
        sqlVector.addAll(PubFun.formUpdateSql(tables1, condition, wherepart));
        //只更新PolNo的表
        String[] polOnlyTables = { "LCSpec", "LCInsuredRelated", "LCPrem_1" };
        condition = condition + " , ModifyDate='" + PubFun.getCurrentDate()
                + "', ModifyTime='" + PubFun.getCurrentTime() + "' ";

        sqlVector.addAll(PubFun.formUpdateSql(polOnlyTables, condition,
                wherepart));
        //还需更新ContNo的表
        String[] tables = { "LCUWMaster", "LCUWError", "LCUWSub", "LCBnf" };
        condition = " contno='" + newContNo + "'," + condition;

        sqlVector.addAll(PubFun.formUpdateSql(tables, condition, wherepart));

        if (sqlVector != null)
        {
            MMap tmpMap = new MMap();
            for (int i = 0; i < sqlVector.size(); i++)
            {
                tmpMap.put((String) sqlVector.get(i), this.UPDATE);
            }
            return tmpMap;
        }
        return null;
    }

    /**
     * 从投保单财务信息中查询交费日期信息等 输出：如果发生错误则返回false,否则返回true
     *
     * @param tLCPolSchema LCPolSchema
     * @return boolean
     */
    private boolean getPaydates(LCPolSchema tLCPolSchema)
    {
        double sumPrem = 0;
        sumMoney = 0;
        //  mFirstPayDate = null;
        // maxPayDate = null;
        //  maxEnterAccDate = null;
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        LJTempFeeSet tLJTempFeeSet;
        if (!mBQFlag)
        {
            //            tLJTempFeeDB.setOtherNo(tLCPolSchema.getPrtNo());
            //            // tLJTempFeeDB.setRiskCode(tLCPolSchema.getRiskCode());
            //            tLJTempFeeDB.setOtherNoType("4");
            //            tLJTempFeeDB.setTempFeeType("1");
            //            tLJTempFeeDB.setConfFlag("0");
            tLJTempFeeSet = tLJTempFeeDB
                    .executeQuery("select * from ljtempfee where otherno='"
                            + tLCPolSchema.getPrtNo()
                            + "' and othernotype = '4' and confflag='0' and enteraccdate is not null");
        }
        else
        {
            String tEdorNo = (String) mBQData.getValueByName("EdorNo");
            tLJTempFeeDB.setOtherNo(tEdorNo);
            tLJTempFeeDB.setOtherNoType("3");
            tLJTempFeeDB.setTempFeeType("4");
            tLJTempFeeDB.setConfFlag("1");
            tLJTempFeeSet = tLJTempFeeDB.query();
        }

        if (tLJTempFeeDB.mErrors.needDealError())
        {
            CError.buildErr(this, "财务信息取数失败!");
            return false;
        }
        //
        int m = tLJTempFeeSet.size();

        for (int j = 1; j <= m; j++)
        {
            LJTempFeeSchema tLJTempFeeSchema = tLJTempFeeSet.get(j);

            if (tLJTempFeeSchema.getPayDate() == null
                    || tLJTempFeeSchema.getEnterAccDate() == null)
            {
                CError.buildErr(this, "有交费号为["
                        + tLJTempFeeSchema.getTempFeeNo() + "]未到帐");
                return false;
            }

            // 取首次交费日期
            Date payDate = fDate.getDate(tLJTempFeeSchema.getPayDate());

            if (mFirstPayDate == null || payDate.before(mFirstPayDate))
            {
                mFirstPayDate = payDate;
            }

            if (maxPayDate == null || maxPayDate.before(payDate))
            {
                maxPayDate = payDate;
            }

            Date enterAccDate = fDate.getDate(tLJTempFeeSchema
                    .getEnterAccDate());

            if (maxEnterAccDate == null || maxEnterAccDate.before(enterAccDate))
            {
                maxEnterAccDate = enterAccDate;
            }

        }

        return true;
    }

    /**
     * 核销财务（根据不同情况处理）
     *
     * @param tPolData VData
     * @param tLCPolSchema LCPolSchema
     * @param PolNo String
     * @param tBQTransferData TransferData
     * @return boolean
     */
    private boolean verifyFinance(VData tPolData, LCPolSchema tLCPolSchema,
            String PolNo, TransferData tBQTransferData)
    {
        LCPremSet tLCPremSet = (LCPremSet) tPolData.getObjectByObjectName(
                "LCPremSet", 0);

        String inEdorType = (String) tBQTransferData.getValueByName("EdorType");

        //如果保全类型是集体下新单加人
        if ("NI".equals(inEdorType))
        {
            //根据保全下个人签单流程走-将前面保存的个人险种保单签单标记为1(得到该对象的引用，直接更改)
            LCPolSchema saveLCPolSchema = (LCPolSchema) tPolData
                    .getObjectByObjectName("LCPolSchema", 0);

            VData tVData = new VData();
            tBQTransferData.setNameAndValue("NewPolNo", PolNo);
            tVData.add(mGlobalInput);
            tVData.add(tBQTransferData);
            tVData.add(tLCPremSet);
            tVData.add(tLCPolSchema);

            EdorSignBL tEdorSignBL = new EdorSignBL();

            if (tEdorSignBL.submitData(tVData, "QUERY") == false)
            {
                return false;
            }

            tVData = tEdorSignBL.getResult();

            LJAGetEndorseSet outLJAGetEndorseSet = (LJAGetEndorseSet) tVData
                    .getObjectByObjectName("LJAGetEndorseSet", 0);
            LJAPayPersonSet outLJAPayPersonSet = (LJAPayPersonSet) tVData
                    .getObjectByObjectName("LJAPayPersonSet", 0);
            rMap.put(outLJAGetEndorseSet, "DELETE");
            rMap.put(outLJAPayPersonSet, "INSERT");
            return true;
        }

        //如果保全类型是新增附加险
        //        if ("NS".equals(inEdorType)) {
        //            //根据保全下个人签单流程走-将前面保存的个人险种保单签单标记为3(得到该对象的引用，直接更改)
        //            LCPolSchema saveLCPolSchema = (LCPolSchema) tPolData.
        //                                          getObjectByObjectName(
        //                                                  "LCPolSchema",
        //                                                  0);
        //            saveLCPolSchema.setAppFlag("1");
        //            /*Lis5.3 upgrade set
        //                  saveLCPolSchema.setCustomGetPolDate(PubFun.getCurrentDate());
        //                  saveLCPolSchema.setGetPolDate(PubFun.getCurrentDate());
        //             */
        //            LCPolDB tLCPolDB = new LCPolDB();
        //            tLCPolDB.setPrtNo(saveLCPolSchema.getPrtNo());
        //            tLCPolDB.setRiskCode(saveLCPolSchema.getRiskCode());
        //            tLCPolDB.setAppFlag("1");
        //
        //            LCPolSet tLCPolSet = tLCPolDB.query();
        //
        //            if (tLCPolSet.size() > 0) {
        //                //需要更新已经存在的附加险的续保标记
        //                tBQTransferData.setNameAndValue("UPLCPol", tLCPolSet);
        //            }
        //
        //            //VData LJAPayData=(VData)tPolData.getObjectByObjectName("VData",0);
        //            //LJAPaySet tLJAPaySet=(LJAPaySet)tPolData.getObjectByObjectName("LJAPaySet",0);
        //            VData tVData = new VData();
        //
        //            //tBQTransferData.setNameAndValue("NewPolNo",PolNo);
        //            tVData.add(mGlobalInput);
        //            tVData.add(tBQTransferData);
        //            tVData.add(tLCPremSet);
        //            tVData.add(tLCPolSchema);
        //
        //            //tVData.add(tLJAPaySet);
        //            EdorSignBL tEdorSignBL = new EdorSignBL();
        //
        //            if (tEdorSignBL.submitData(tVData, "QUERY") == false) {
        //                return false;
        //            }
        //
        //            tVData = tEdorSignBL.getResult();
        //
        //            //删除
        //            LJAGetEndorseSet outLJAGetEndorseSet = (LJAGetEndorseSet) tVData.
        //                    getObjectByObjectName(
        //                            "LJAGetEndorseSet",
        //                            0);
        //
        //            //插入
        //            LJAPayPersonSet outLJAPayPersonSet = (LJAPayPersonSet) tVData.
        //                                                 getObjectByObjectName(
        //                    "LJAPayPersonSet",
        //                    0);
        //
        //            tPolData.add(outLJAGetEndorseSet);
        //            tPolData.add(outLJAPayPersonSet);
        //
        //            return true;
        //        }

        //集体下无名单加人。其实不是保全项目，但是流程类似，借用
        if ("GANAME".equals(inEdorType))
        {
            //将前面保存的个人险种保单签单标记为4的改为5(得到该对象的引用，直接更改)
            LCPolSchema saveLCPolSchema = (LCPolSchema) tPolData
                    .getObjectByObjectName("LCPolSchema", 0);
            saveLCPolSchema.setAppFlag("1");
            saveLCPolSchema.setStateFlag("1");

            LCDutySet txLCDutySet = (LCDutySet) tPolData.getObjectByObjectName(
                    "LCDutySet", 0);
            LCPremSet txLCPremSet = (LCPremSet) tPolData.getObjectByObjectName(
                    "LCPremSet", 0);

            VData tVData = new VData();
            GrpAddNameSignBL tGrpAddNameSignBL = new GrpAddNameSignBL();
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("NewPolNo", saveLCPolSchema
                    .getPolNo());
            tTransferData.setNameAndValue("NewPrem", saveLCPolSchema
                    .getStandPrem());
            tTransferData.setNameAndValue("SerialNo", mSerialNo);
            tTransferData.setNameAndValue("OldPolNo", saveLCPolSchema
                    .getMasterPolNo());
            tTransferData.setNameAndValue("ProposalNo", saveLCPolSchema
                    .getProposalNo());
            tVData.add(mGlobalInput);
            tVData.add(tTransferData);
            tVData.add(saveLCPolSchema);
            tVData.add(txLCDutySet);
            tVData.add(txLCPremSet);

            if (tGrpAddNameSignBL.submitData(tVData, "QUERY") == false)
            {
                this.mErrors.copyAllErrors(tGrpAddNameSignBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "ProposalSignBL";
                tError.functionName = "preparePol";
                tError.errorMessage = "无名单加人数据处理失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tVData = tGrpAddNameSignBL.getResult();
            tBQTransferData.setNameAndValue("GANAME", tVData);
            mNotCreatInsuaccFlag = true;
        }
        return true;
    }
}
