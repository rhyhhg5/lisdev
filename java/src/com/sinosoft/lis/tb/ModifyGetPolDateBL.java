package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCContGetPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ModifyGetPolDateBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private GlobalInput tGI = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    private MMap map = new MMap();

    /** 数据操作字符串 */
    private String mOperate;

    /** 单证号码 */
    private String ContNo;
    
    /** 修改于投保人的关系*/
    private String GetPolDate;
    private String SaleState;
    private String WageNo;

    private Reflections mReflections = new Reflections();

    /**
     * submitData
     * 
     * @param cInputData
     *            VData
     * @param cOperate
     *            String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        mInputData = (VData) cInputData.clone();

        if (!getInputData())
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit ps = new PubSubmit();
		if (!ps.submitData(mInputData, null)) {
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}

        return true;
    }

    /**
     * getInputData
     * 
     * @return boolean
     */
    private boolean getInputData()
    {
        try
        {
            tGI = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput",
                    0);

            TransferData mTransferData = (TransferData) mInputData
                    .getObjectByObjectName("TransferData", 0);
            ContNo = (String) mTransferData.getValueByName("ContNo");
            GetPolDate = (String) mTransferData.getValueByName("GetPolDate");
            SaleState=(String) mTransferData.getValueByName("SaleState");
            WageNo=(String) mTransferData.getValueByName("WageNo");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ScanDeleBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * checkData
     * 
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 
     * @return boolean
     */
    private boolean dealData()
    {
        if (!dealGetPolDate())
        {
            return false;
        }

        return true;
    }

    /**
     * 更改回执回销日期
     * @return
     */
    private boolean dealGetPolDate()
    {
    	LCContGetPolDB tLCContGetPolDB = new LCContGetPolDB();
    	tLCContGetPolDB.setContNo(ContNo);
        if (tLCContGetPolDB.getInfo() == false)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "dealGetPolDate";
            tError.functionName = "dealData";
            tError.errorMessage = "该保单没有进行合同接受!";
            this.mErrors.addOneError(tError);
            return false;
        }
    	if(tLCContGetPolDB.getContType().equals("1"))
    	{
    		map.put("update LCCont set customgetpoldate = '"+GetPolDate+"', Operator = '" 
    				+ tGI.Operator + "', ModifyDate = Current Date, ModifyTime = Current Time where contno = '"+ContNo+"'", "UPDATE");
    	}
    	if(tLCContGetPolDB.getContType().equals("2"))
    	{
    		map.put("update LCGrpCont set customgetpoldate = '"+GetPolDate+"', Operator = '" 
    				+ tGI.Operator + "', ModifyDate = Current Date, ModifyTime = Current Time where grpcontno = '"+ContNo+"'", "UPDATE");
    		map.put("update LCCont set customgetpoldate = '"+GetPolDate+"', Operator = '" 
    				+ tGI.Operator + "', ModifyDate = Current Date, ModifyTime = Current Time where grpcontno = '"+ContNo+"'", "UPDATE");
    		
    	}
        
        map.put("update LCContGetPol set getpoldate = '"+GetPolDate+"', ModifyDate = Current Date, ModifyTime = Current Time where contno = '"+ContNo+"'", "UPDATE");
        if(SaleState.equals("1")){
        	map.put("update lacommision set wageno='"+WageNo+"',customgetpoldate = '"+GetPolDate+"', Operator = '" 
    				+ tGI.Operator + "', ModifyDate = Current Date, ModifyTime = Current Time where contno = '"+ContNo+"'", "UPDATE");
        }
        
        return true;
    }
    
	/**
	 * prepareOutputData
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {

		try {
			mInputData.add(map);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ModifyRelationToAppntBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "准备传输数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}
}
