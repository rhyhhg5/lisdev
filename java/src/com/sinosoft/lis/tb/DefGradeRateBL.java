/**
 * Copyright ? 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.tb;

import java.util.*;

import com.sinosoft.lis.db.LCContPlanDB;
import com.sinosoft.lis.db.LCContPlanDutyParamDB;
import com.sinosoft.lis.db.LCContPlanRiskDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LMRiskAccPayDB;
import com.sinosoft.lis.db.LMRiskDutyFactorDB;
import com.sinosoft.lis.db.LMRiskFeeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.schema.LCContPlanSchema;
import com.sinosoft.lis.schema.LMRiskAccPaySchema;
import com.sinosoft.lis.schema.LMRiskFeeSchema;
import com.sinosoft.lis.vschema.LCContPlanDutyDefGradeSet;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.lis.vschema.LCContPlanSet;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LCGrpFeeSet;
import com.sinosoft.lis.vschema.LMRiskDutyFactorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.StrTool;

/**
 * 数据准备类
 * <p>Title: </p>
 * <p>Description: 根据操作类型不同，对数据进行校验、准备处理 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author 朱向峰
 * @version 1.0
 */
public class DefGradeRateBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LCContPlanDutyDefGradeSet mLCContPlanDutyDefGradeSet = new
    		LCContPlanDutyDefGradeSet();
    private String mGrpContNo = "";
    private String mContPlanCode = "";
    private String mProposalGrpContNo = "";
    private String mDefDutycode = "";
    private MMap tmap=new MMap();
    public DefGradeRateBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        
        //数据校验
        if (!checkData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContPlanBL";
            tError.functionName = "checkData";
            tError.errorMessage = "数据处理失败LCContPlanBL-->checkData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        //　数据提交、保存
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start GroupRiskBL Submit...");

        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "ContBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("---commitData---");
        return true;
    }

    /**
     * 数据校验
     * @return boolean
     */
    private boolean checkData()
    {
      return true;	
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealData()
    {
    	
    	 return true;	
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLCContPlanDutyDefGradeSet.set((LCContPlanDutyDefGradeSet) cInputData
                .getObjectByObjectName("LCContPlanDutyDefGradeSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData
                .getObjectByObjectName("GlobalInput", 0));
        this.mProposalGrpContNo = (String) cInputData.get(2);
        this.mGrpContNo = (String) cInputData.get(3);
        this.mContPlanCode = (String) cInputData.get(4);
        this.mDefDutycode = (String) cInputData.get(5);
        return true;
    }

    

    private boolean prepareOutputData()
    {
    	if(tmap==null){
    		tmap=new MMap();
    	}
    	mInputData=new VData();
    	tmap.put(mLCContPlanDutyDefGradeSet, mOperate);
    	mInputData.add(tmap);
    	 return true;	
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
