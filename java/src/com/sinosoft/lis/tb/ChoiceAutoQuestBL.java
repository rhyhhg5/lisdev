package com.sinosoft.lis.tb;

/*
 * <p>ClassName: ChoiceAutoQuestBL </p>
 * <p>Description: ChoiceAutoQuestBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-05-04 12:41:46
 */
import com.sinosoft.lis.db.LCGrpIssuePolDB;
import com.sinosoft.lis.schema.LCGrpIssuePolSchema;
import com.sinosoft.lis.vschema.LCGrpIssuePolSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class ChoiceAutoQuestBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LCGrpIssuePolSet mLCGrpIssuePolSet = new LCGrpIssuePolSet();
    private String LoadFlag;
    private ExeSQL tExeSql = new ExeSQL();
    private SSRS tSSRS = new SSRS();
    public ChoiceAutoQuestBL() {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        this.mInputData = cInputData;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start ChoiceAutoQuestBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "ChoiceAutoQuestBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("END ChoiceAutoQuestBL Submit...");
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        /*        if (this.mOperate.equals("INSERT||MAIN")) {
                    if (mLCGrpIssuePolSet.size() > 0) {
                        for (int n = 1; n <= mLCGrpIssuePolSet.size(); n++) {
         LCGrpIssuePolDB tLCGrpIssuePolDB = new LCGrpIssuePolDB();
         tLCGrpIssuePolDB.setProposalGrpContNo(mLCGrpIssuePolSet.get(
                                    n).getProposalGrpContNo());
         tLCGrpIssuePolDB.setSerialNo(mLCGrpIssuePolSet.get(n).
                                                         getSerialNo());
                            if (!tLCGrpIssuePolDB.getInfo()) {
         this.mErrors.copyAllErrors(tLCGrpIssuePolDB.mErrors);
                                CError tError = new CError();
                                tError.moduleName = "ChoiceAutoQuestBL";
                                tError.functionName = "submitData";
                                tError.errorMessage = "在查询问题件时未找到问题件信息,请确认是"
                                                      + "否选择问题件!";
                                this.mErrors.addOneError(tError);
                                return false;
                            }
         mLCGrpIssuePolSet.get(n).setSchema(tLCGrpIssuePolDB.
                                    getSchema());
                        }
                    }
                    map.put(mLCGrpIssuePolSet, "UPDATE");
                }
         */
        /**
         * 团体人工核保下发问题件复用此类
         */
        if (this.mOperate.equals("INSERT||QUEST")) {
            int IssueCont = mLCGrpIssuePolSet.size();
            System.out.println("mLCGrpIssuePolSet.size() is"+mLCGrpIssuePolSet.size());
            String strSql = "";
            if (IssueCont > 0) {
                strSql = "select prtno from lcgrpcont where grpcontno='" +
                         mLCGrpIssuePolSet.get(1).getProposalGrpContNo() + "'";
            }
            System.out.println("strSql"+strSql);
            ExeSQL tExeSQL = new ExeSQL();
            String prtNo = "";
            try {
                prtNo = tExeSQL.getOneValue(strSql);
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(
                        "ChoiceAutoQuestBL.dealData()  \n--Line:129  --Author:YangMing");
                System.out.println("PrtNo查询失败");
                CError tError = new CError();
                tError.moduleName = "LDClassInfoBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询合同印刷号失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            System.out.println("印刷号 ： " + prtNo);
            LCGrpIssuePolDB tLCGrpIssuePolDB = new LCGrpIssuePolDB();
            LCGrpIssuePolSet tLCGrpIssuePolSet = new LCGrpIssuePolSet();
            tLCGrpIssuePolSet = tLCGrpIssuePolDB.executeQuery(
                    "select * from LCGrpIssuepol where grpcontno='" +
                    mLCGrpIssuePolSet.get(1).getGrpContNo() +
                    "' and state<>'5' "
                                );
            boolean flag = false;
            if (tLCGrpIssuePolSet.size() == IssueCont) {
                for (int i = 0; i < mLCGrpIssuePolSet.size(); i++) {
                    if (mLCGrpIssuePolSet.get(i + 1).getSerialNo().equals("")) {
                        flag = true;
                        break;
                    } else {
                        flag = false;
                        break;
                    }
                }
            } else {
                flag = true;
            }
            System.out.println("qiang" + flag);
            if (flag) {
                for (int i = 1; i <= IssueCont; i++) {
                    String SerialNo = PubFun1.CreateMaxNo("GIPAYNOTICENO", 14);
                    System.out.println("流水号 ： " + SerialNo);
                    String user = findDownuwUser(this.mGlobalInput.Operator, i);
                    if (user == null || user.equals("")) {
                        return false;
                    }
                    if(StrTool.cTrim(mLCGrpIssuePolSet.get(i).getOperator()).equals(""))
                    {
                        System.out.println("jjjj");
                        mLCGrpIssuePolSet.get(i).setOperator(user);
                        System.out.println("jjjj1");
                    }else if(StrTool.cTrim(mLCGrpIssuePolSet.get(i).getPrtSeq()).equals(tLCGrpIssuePolSet.get(i).getPrtSeq())&&!StrTool.cTrim(mLCGrpIssuePolSet.get(i).getBackObjType()).equals(tLCGrpIssuePolSet.get(i).getBackObjType())){
                        System.out.println("jjjj2");
                        mLCGrpIssuePolSet.get(i).setOperator(user);
                        System.out.println("jjjj3");
                    }
                    mLCGrpIssuePolSet.get(i).setManageCom(this.mGlobalInput.
                            ManageCom);
                    if ((mLCGrpIssuePolSet.get(i).getSerialNo() == null) ||
                        mLCGrpIssuePolSet.get(i).getSerialNo().equals("")) {
                        mLCGrpIssuePolSet.get(i).setSerialNo(SerialNo);
                    }
                    mLCGrpIssuePolSet.get(i).setMakeDate(PubFun.getCurrentDate());
                    mLCGrpIssuePolSet.get(i).setModifyDate(PubFun.
                            getCurrentDate());
                    mLCGrpIssuePolSet.get(i).setMakeTime(PubFun.getCurrentTime());
                    mLCGrpIssuePolSet.get(i).setModifyTime(PubFun.
                            getCurrentTime());
                    if (mLCGrpIssuePolSet.get(i).getState().equals("")) {
                        mLCGrpIssuePolSet.get(i).setState("1");
                    }
                    map.put("delete from LCGrpIssuePol where GrpContNo='" +
                            mLCGrpIssuePolSet.get(1).getGrpContNo() +
                            "' and state<>'5'",
                            "DELETE");
                    map.put(mLCGrpIssuePolSet, "INSERT");
                }
            } else {
                CError tError = new CError();
                tError.moduleName = "LDClassInfoBL";
                tError.functionName = "dealData";
                tError.errorMessage = "问题件条数没有发生增减变化,不允许重复保存!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        if (this.mOperate.equals("UPDATE||MAIN")) {
            int IssueCont = mLCGrpIssuePolSet.size();
            String strSql = "";
            if (IssueCont > 0) {
                strSql = "select prtno from lcgrpcont where grpcontno='" +
                         mLCGrpIssuePolSet.get(1).getProposalGrpContNo() + "'";
            }
            ExeSQL tExeSQL = new ExeSQL();
            String prtNo = "";
            try {
                prtNo = tExeSQL.getOneValue(strSql);
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(
                        "ChoiceAutoQuestBL.dealData()  \n--Line:129  --Author:YangMing");
                System.out.println("PrtNo查询失败");
                CError tError = new CError();
                tError.moduleName = "LDClassInfoBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询合同印刷号失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            System.out.println("印刷号 ： " + prtNo);
            LCGrpIssuePolDB tLCGrpIssuePolDB = new LCGrpIssuePolDB();
            LCGrpIssuePolSet tLCGrpIssuePolSet = new LCGrpIssuePolSet();
            tLCGrpIssuePolDB.setGrpContNo(mLCGrpIssuePolSet.get(1).getGrpContNo());
            tLCGrpIssuePolDB.setSerialNo(mLCGrpIssuePolSet.get(1).getSerialNo());
            tLCGrpIssuePolSet = tLCGrpIssuePolDB.query();
            String user = findDownuwUser(this.mGlobalInput.Operator, 1);
            if (user == null || user.equals("")) {
                CError tError = new CError();
                 tError.moduleName = "LDClassInfoBL";
                 tError.functionName = "dealData";
                 tError.errorMessage = "取登录用户失败，不能保存！请关闭所有窗口,重新登录系统！";
                 this.mErrors.addOneError(tError);
                 return false;
            }
            if(mLCGrpIssuePolSet.get(1).getBackObjType().equals("0")){
                mLCGrpIssuePolSet.get(1).setOperator(user);
            }
            mLCGrpIssuePolSet.get(1).setOperator(user);
            mLCGrpIssuePolSet.get(1).setManageCom(this.mGlobalInput.ManageCom);
            mLCGrpIssuePolSet.get(1).setMakeDate(tLCGrpIssuePolSet.get(1).
                                                 getMakeDate());
            mLCGrpIssuePolSet.get(1).setModifyDate(PubFun.getCurrentDate());
            mLCGrpIssuePolSet.get(1).setMakeTime(tLCGrpIssuePolSet.get(1).
                                                 getMakeTime());
            mLCGrpIssuePolSet.get(1).setModifyTime(PubFun.getCurrentTime());
            mLCGrpIssuePolSet.get(1).setState(tLCGrpIssuePolSet.get(1).getState());
            map.put("delete from LCGrpIssuePol where GrpContNo='" +
                    mLCGrpIssuePolSet.get(1).getGrpContNo() +
                    "' and SerialNo='" + mLCGrpIssuePolSet.get(1).getSerialNo() +
                    "' ", "DELETE");
            map.put(mLCGrpIssuePolSet, "INSERT");
        }
        if (this.mOperate.equals("REPLY||MAIN")) {
            int IssueCont = mLCGrpIssuePolSet.size();
            String strSql = "";
            if (IssueCont > 0) {
                strSql = "select prtno from lcgrpcont where grpcontno='" +
                         mLCGrpIssuePolSet.get(1).getProposalGrpContNo() + "'";
            }
            ExeSQL tExeSQL = new ExeSQL();
            String prtNo = "";
            try {
                prtNo = tExeSQL.getOneValue(strSql);
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(
                        "ChoiceAutoQuestBL.dealData()  \n--Line:129  --Author:YangMing");
                System.out.println("PrtNo查询失败");
                CError tError = new CError();
                tError.moduleName = "LDClassInfoBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询合同印刷号失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            System.out.println("印刷号 ： " + prtNo);
            map.put("update LCGrpIssuePol set ReplyMan='" +
                    this.mGlobalInput.Operator + "',"
                    + "ModifyDate='" + PubFun.getCurrentDate() +
                    "',ModifyTime='" + PubFun.getCurrentTime() +
                    "',State='5' where GrpContNo='" +
                    mLCGrpIssuePolSet.get(1).getGrpContNo() +
                    "' and SerialNo='" + mLCGrpIssuePolSet.get(1).getSerialNo() +
                    "' "
                    , "UPDATE");
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLCGrpIssuePolSet.set((LCGrpIssuePolSet) cInputData.
                                   getObjectByObjectName(
                                           "LCGrpIssuePolSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        TransferData tTransferData =
                (TransferData) mInputData.getObjectByObjectName("TransferData",
                0);
        this.LoadFlag = (String) tTransferData.getValueByName("LoadFlag");

//        tTransferData.getValueByName("LoadFlag");
        System.out.println("LoadFlag" + LoadFlag);
        System.out.println("this.mGlobalInput"+this.mGlobalInput.Operator);
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LCGrpIssuePolDB tLCGrpIssuePolDB = new LCGrpIssuePolDB();
//        tLCGrpIssuePolDB.setSchema(this.mLDClassInfoSchema);
        //如果有需要处理的错误，则返回
        if (tLCGrpIssuePolDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGrpIssuePolDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDClassInfoBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLCGrpIssuePolSet);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLCGrpIssuePolSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDClassInfoBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    //取操作员的下级相关信息
    private String findDownuwUser(String UserCode, int i) {
        String LastUserCode = "";
        boolean Userflag = false;
        ExeSQL tExeSQL = new ExeSQL();
        StringBuffer tSBql = new StringBuffer(128);
        if (LoadFlag.equals("1")) {

            if (mLCGrpIssuePolSet.get(i).getBackObjType().equals("1")) {
                tSBql.append(
                        "select distinct upusercode,downuwcode,uwno from lcuwsendtrace where otherno='" +
                        mLCGrpIssuePolSet.get(1).getGrpContNo() +
                        "' and downuwcode<>upusercode and sendtype in ('1','2') and upusercode='"+UserCode+"' order by uwno desc");
                SSRS tSSRS = tExeSQL.execSQL(tSBql.toString());
                if (tSSRS.getMaxRow() == 0) {
                    LastUserCode = tExeSql.getOneValue(
                            "select approvecode from lcgrpcont where grpcontno='" +
                            mLCGrpIssuePolSet.get(1).getGrpContNo() + "'");
                    if (StrTool.cTrim(LastUserCode).equals("")) {
                        CError tError = new CError();
                        tError.moduleName = "LDClassInfoBL";
                        tError.functionName = "prepareData";
                        tError.errorMessage = "复核人员为空，不能保存！";
                        this.mErrors.addOneError(tError);
                        return null;
                    }
                } else {
                    System.out.println("UserCode" + UserCode);
                    for (int j = 1; j <= tSSRS.getMaxRow(); j++) {
                        if (UserCode.equals(tSSRS.GetText(j, 1))) {
                            //System.out.println("User"+i+"Code"+tSSRS.GetText(i, 1));
                            LastUserCode = tSSRS.GetText(j, 2);
                            Userflag = true;
                            System.out.println("Userflag11" + Userflag);
                            //               System.out.println("LastUserCode"+LastUserCode);
                            break;
                        }
                    }
                    System.out.println("User11" + LastUserCode);
                }
            } else {
                LastUserCode = tExeSQL.getOneValue("select UserCode from lduser where UserName in (select firsttrialoperator from lcgrpcont where grpcontno='" +
                        mLCGrpIssuePolSet.get(1).getGrpContNo() +
                        "') and OtherPopedom='1'");
                if (StrTool.cTrim(LastUserCode).equals("")) {
                    CError tError = new CError();
                    tError.moduleName = "LDClassInfoBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "初审人员为空,不允许进行保存!";
                    this.mErrors.addOneError(tError);
                    return null;
                }
            }
        } else {
            if (mLCGrpIssuePolSet.get(i).getBackObjType().equals("1")) {
                System.out.println("aaaa"+mLCGrpIssuePolSet.get(i).getBackObjType());
                LastUserCode = tExeSQL.getOneValue(
                        "select InputOperator from lcgrpcont where grpcontno='" +
                        mLCGrpIssuePolSet.get(1).getGrpContNo() + "'");
                if (StrTool.cTrim(LastUserCode).equals("")) {
                    CError tError = new CError();
                    tError.moduleName = "LDClassInfoBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "录入员为空,不允许进行保存!";
                    this.mErrors.addOneError(tError);
                    return null;
                }
                if(LoadFlag.equals("3")){
                    map.put(" update lwmission set missionprop20='N1' where missionprop1 = '"+mLCGrpIssuePolSet.get(1).getGrpContNo()+"'  and processid = '0000000004' and activityid = '0000002001' ", "UPDATE");
                }
            } else {
                LastUserCode = tExeSQL.getOneValue("select UserCode from lduser where UserName in (select firsttrialoperator from lcgrpcont where grpcontno='" +
                        mLCGrpIssuePolSet.get(1).getGrpContNo() +
                        "') and OtherPopedom='1'");
                if (StrTool.cTrim(LastUserCode).equals("")) {
                    CError tError = new CError();
                    tError.moduleName = "LDClassInfoBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "初审人员为空,不允许进行保存!";
                    this.mErrors.addOneError(tError);
                    return null;
                }
            }
            if(LoadFlag.equals("3")){
                map.put(" update lwmission set missionprop20='N1' where missionprop1 = '"+mLCGrpIssuePolSet.get(1).getGrpContNo()+"'  and processid = '0000000004' and activityid = '0000002001' ", "UPDATE");
            }
        }
        return LastUserCode;
    }
}
