package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class GrpSignAfterPrintUI {
    public GrpSignAfterPrintUI() {
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        this.mInputData = cInputData;
        GrpSignAfterPrintBL tGrpSignAfertPrintBL = new GrpSignAfterPrintBL();
        tGrpSignAfertPrintBL.submitData(mInputData, mOperate);
        //如果有需要处理的错误，则返回
        if (tGrpSignAfertPrintBL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tGrpSignAfertPrintBL.mErrors);
            System.out.println("ljdfljslf");
            return false;
        }
        this.mResult.clear();
        this.mResult = tGrpSignAfertPrintBL.getResult();
        mInputData = null;
        return true;
    }

    public static void main(String[] args) {
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

}
