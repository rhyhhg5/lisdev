/**
 * Copyright ? 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LCContPlanDB;
import com.sinosoft.lis.db.LCContPlanDutyParamDB;
import com.sinosoft.lis.db.LCContPlanRiskDB;
import com.sinosoft.lis.db.LMRiskDutyFactorDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.schema.LCContPlanSchema;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.lis.vschema.LCContPlanSet;
import com.sinosoft.lis.vschema.LMRiskDutyFactorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.MMap;


/**
 * 数据准备类
 * <p>Title: </p>
 * <p>Description: 根据操作类型不同，对数据进行校验、准备处理 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author 朱向峰
 * @version 1.0
 */
public class LCContPlanNewBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();


    /** 往后面传输数据的容器 */
    private VData mInputData;
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();


    /** 数据操作字符串 */
    private String mOperate;


    /** 业务处理相关变量 */
    private LCContPlanSchema mLCContPlanSchema = new LCContPlanSchema();
    private LCContPlanSet mLCContPlanSet = new LCContPlanSet();

//    private String mGrpContNo = ""; //团体合同号
//    private String mContPlanCode = ""; //保险计划或默认值编码
//    private String mProposalGrpContNo = "";
//    private String mPlanType = "";
//    private String mPlanSql = "";
//    private String mPeoples3 = "";
    private String mSql = "";

    public LCContPlanNewBL()
    {
    }

    public static void main(String[] args)
    {
    }


    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        LCContPlanSchema tt = (LCContPlanSchema)cInputData.getObjectByObjectName("LCContPlanSchema",0);
        System.out.println("tt.getContPlanCode() = "+tt.getContPlanCode());

        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        System.out.println("mOperate = "+mOperate);
        if (!getInputData(cInputData))
        {
            return false;
        }

        //数据校验
        if (!checkData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContPlanBL";
            tError.functionName = "checkData";
            tError.errorMessage = "数据处理失败LCContPlanBL-->checkData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContPlanBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LCContPlanBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start LCContPlanNewBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("mLCContPlanSchema.getRemark() = "+mLCContPlanSchema.getRemark());
            System.out.println("mLCContPlanSchema.getRemark2() = "+mLCContPlanSchema.getRemark2());
            MMap map = null;
            map = (MMap) mInputData.getObjectByObjectName("MMap", 0);
            if(map == null){
                System.out.println("======map null========");
            }else{
               System.out.println("======mapmap========");
                for (int i = 0; i < map.keySet().size(); i++) {
                    System.out.println("======"+i+"========");
                    Object key = map.getKeyByOrder(String.valueOf(i + 1));
                    System.out.println(map.get(key).toString());
                }

            }
            if (!tPubSubmit.submitData(mInputData, cOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LDPersonBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("End LCContPlanNewBL Submit...");
            //如果有需要处理的错误，则返回
        }
        mInputData = null;
        return true;
    }


    /**
     * 数据校验
     * @return boolean
     */
    private boolean checkData()
    {
        //add remark
        if (this.mOperate.trim().equals("UPDATE") )
        {
           if(mLCContPlanSchema.getContPlanCode()==null||
              mLCContPlanSchema.getContPlanCode().trim().equals("")){
               CError tError = new CError();
                        tError.moduleName = "mLCContPlanSchema.getContPlanCode";
                        tError.functionName = "checkData";
                        tError.errorMessage = "shuchang" ;
                        this.mErrors.addOneError(tError);
                        return false;
           }
           if(mLCContPlanSchema.getGrpContNo()==null||
               mLCContPlanSchema.getGrpContNo().trim().equals("")){
                CError tError = new CError();
                tError.moduleName = "mLCContPlanSchema.getGrpContNo";
                tError.functionName = "checkData";
                tError.errorMessage = "shuchang";
                this.mErrors.addOneError(tError);
                return false;
            }
            if(mLCContPlanSchema.getPlanType()==null||
               mLCContPlanSchema.getPlanType().trim().equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "mLCContPlanSchema.getPlanType";
                tError.functionName = "checkData";
                tError.errorMessage = "shuchang";
                this.mErrors.addOneError(tError);
                return false;
            }

            return true;
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealData()
    {
        mLCContPlanSchema.setMakeDate(PubFun.getCurrentDate());
        mLCContPlanSchema.setMakeTime(PubFun.getCurrentTime());
        mLCContPlanSchema.setModifyDate(PubFun.getCurrentDate());
        mLCContPlanSchema.setModifyTime(PubFun.getCurrentTime());

        boolean tReturn = true;
        //新增处理，修改处理
        if (this.mOperate.trim().equals("UPDATE") )
        {
            map.put(mLCContPlanSchema,"UPDATE");
            for (int i = 0; i < map.keySet().size(); i++) {
              Object key = map.getKeyByOrder(String.valueOf(i + 1));
              System.out.println(map.get(key).toString());
            }
        }
        //由于删除操作可能没有multine数据，因此需要一些特殊参数，并且根据参数查询要删除的数据
        if (this.mOperate.compareTo("DELETE||MAIN") == 0)
        {
//            //准备保单保险计划的删除数据
//            LCContPlanDB tLCContPlanDB = new LCContPlanDB();
//            tLCContPlanDB.setGrpContNo(this.mLCContPlanSchema.getGrpContNo());
//            tLCContPlanDB.setContPlanCode(this.mLCContPlanSchema.
//                                          getContPlanCode());
//            tLCContPlanDB.setPlanType(this.mLCContPlanSchema.getPlanType());
//            tLCContPlanDB.setProposalGrpContNo(this.mLCContPlanSchema.
//                                               getProposalGrpContNo());
//            mLCContPlanSet = tLCContPlanDB.query();
//
//            //准备保单险种保险计划的删除数据
//            LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
//            tLCContPlanRiskDB.setGrpContNo(this.mLCContPlanSchema.getGrpContNo());
//            tLCContPlanRiskDB.setContPlanCode(this.mLCContPlanSchema.
//                                              getContPlanCode());
//            tLCContPlanRiskDB.setPlanType(this.mLCContPlanSchema.getPlanType());
//            tLCContPlanRiskDB.setProposalGrpContNo(this.mLCContPlanSchema.
//                    getProposalGrpContNo());
//            mLCContPlanRiskSet = tLCContPlanRiskDB.query();
//
//            //准备保险计划责任要素值的删除数据
//            LCContPlanDutyParamDB tLCContPlanDutyParamDB = new
//                    LCContPlanDutyParamDB();
//            tLCContPlanDutyParamDB.setGrpContNo(this.mLCContPlanSchema.
//                                                getGrpContNo());
//            tLCContPlanDutyParamDB.setContPlanCode(this.mLCContPlanSchema.
//                    getContPlanCode());
//            tLCContPlanDutyParamDB.setPlanType(this.mLCContPlanSchema.
//                                               getPlanType());
//            tLCContPlanDutyParamDB.setProposalGrpContNo(this.mLCContPlanSchema.
//                    getProposalGrpContNo());
//            mLCContPlanDutyParamSet = tLCContPlanDutyParamDB.query();
        }

        tReturn = true;
        return tReturn;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        this.mLCContPlanSchema.setSchema((LCContPlanSchema) cInputData.
                                         getObjectByObjectName(
                                                 "LCContPlanSchema",
                                                 0));
        mInputData = cInputData;
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     * @return boolean
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start LCContPlanBLQuery Submit...");
        LCContPlanDB tLCContPlanDB = new LCContPlanDB();
        tLCContPlanDB.setSchema(this.mLCContPlanSchema);
        this.mLCContPlanSet = tLCContPlanDB.query();
        this.mResult.add(this.mLCContPlanSet);
        System.out.println("End LCContPlanBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLCContPlanDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCContPlanDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCContPlanBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.addElement(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContPlanBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
