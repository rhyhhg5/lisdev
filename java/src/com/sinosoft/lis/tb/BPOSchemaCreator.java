package com.sinosoft.lis.tb;

import java.lang.reflect.*;
import javax.xml.transform.*;

import org.apache.xpath.*;
import org.apache.xpath.objects.*;
import org.w3c.dom.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class BPOSchemaCreator
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private String mPackagPath = "com.sinosoft.lis.";  //包路径

    private String mFieldName = null;
    private String mSchemaClassName = null;

    public BPOSchemaCreator()
    {
    }

    /**
     * 生成外包投保人信息BPOLCAppntSchema
     * @return Schema
     */
    public SchemaSet createSchema(Node cNode, String tSchemaClassName,
                                  String cBPOBatchNo, String cPrtNo,
                                  String cOperator)
    {
        mSchemaClassName = tSchemaClassName;

        NodeList tRowNodeList = null;

        Class schemaSetClass = null;

        //定义Set对象
        Class schemaClass = getSchemaClass(tSchemaClassName);
        schemaSetClass = getSchemaSetClass(tSchemaClassName);
        SchemaSet tSchemaSet = getSchemaSet(schemaSetClass, tSchemaClassName);
        if(schemaClass == null || tSchemaSet == null)
        {
            return null;
        }

        try
        {
            tRowNodeList = XPathAPI.selectNodeList(cNode, "ROW");
        }
        catch(TransformerException ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("XML格式有误");
            return null;
        }

        //循环每一ROW行数据
        for(int i = 0; i < tRowNodeList.getLength(); i++)
        {
            Node tRowNode = tRowNodeList.item(i);
            NodeList tFieldNodeList = tRowNode.getChildNodes();

            //循环ROW下每一个节点值，ROW下节点对应库表中一个字段
            Schema schema = getSchemaObj(schemaClass);
            for(int j = 0; j < tFieldNodeList.getLength(); j++)
            {
                Node tFieldNode = tFieldNodeList.item(j);
                mFieldName = tFieldNode.getNodeName();

                if(mFieldName.equals("#text")
                   || mFieldName.equals("#comment")
                   || mFieldName.equals("SUBDUTYTABLE"))
                {
                    continue;
                }

                XObject tXObject = null;
                try
                {
                    tXObject = XPathAPI.eval(tRowNode, mFieldName);
                }
                catch(TransformerException ex)
                {
                }

                String tFieldValue = (tXObject == null ? "" : tXObject.toString());
                if(tFieldValue.startsWith("\n ") || tFieldValue.startsWith("\n\r "))
                {
                    tFieldValue = "";  //外包给的空值xml节点以回车+空格开始的字符串，特做此调整
                }

                setField(schemaClass, schema, "set" + mFieldName, tFieldValue, true);
            }

            //处理BPOBatchNo字段
            setField(schemaClass, schema, "setBPOBatchNo", cBPOBatchNo, true);
            //处理BPOBatchNo字段
            setField(schemaClass, schema, "setPrtNo", cPrtNo, true);
            //处理BPOBatchNo字段
            setField(schemaClass, schema, "setSequenceNo", "" + i, false);
            //处理Operator字段
            setField(schemaClass, schema, "setOperator", StrTool.cTrim(cOperator), true);
            PubFun.fillDefaultField(schema);

            //将Schema放入Set
            setField(schemaSetClass, tSchemaSet, "add", schema, true);
        }

        return tSchemaSet;
    }

    /**
     * getSchemaSetClass
     *
     * @param cSchemaClassName String
     * @return Class
     */
    private Class getSchemaSetClass(String cSchemaClassName)
    {
        String tSchemaSetClassName = mPackagPath + "vschema."
                                     + cSchemaClassName + "Set";
        try
        {
            return Class.forName(tSchemaSetClassName);
        }
        catch(ClassNotFoundException ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("没有找到SchemaSet类" + cSchemaClassName);
            return null;
        }
    }

    private boolean setField(Class schemaClass, Object schema, String methodName,
                             Object tFieldValue, boolean cDealErr)
    {
        try
        {
            Class[] paramType = new Class[1];
            paramType[0] = Class.forName(tFieldValue.getClass().getName());
            Method method = schemaClass.getMethod(methodName, paramType);
            Object[] args = new Object[1];
            args[0] = tFieldValue;
            method.invoke(schema, args);
        }
        catch(InvocationTargetException ex)
        {
        }
        catch(IllegalArgumentException ex)
        {
        }
        catch(IllegalAccessException ex)
        {
        }
        catch(SecurityException ex)
        {
        }
        catch(NoSuchMethodException ex)
        {
            if(cDealErr)
            {
                ex.printStackTrace();
                mErrors.addOneError("XML节点错误：" + mFieldName);
                return false;
            }
        }
        catch(ClassNotFoundException ex)
        {
            if(cDealErr)
            {
                ex.printStackTrace();
                mErrors.addOneError("没有找到Schema：" + mSchemaClassName);
                return false;
            }
        }

        return true;
    }

    /**
     * 得到Schema对象
     * @param schemaClass Class
     * @return Schema
     */
    private Schema getSchemaObj(Class schemaClass)
    {
        Schema schema = null;
        try
        {
            schema = (Schema) schemaClass.newInstance();
        }
        catch(IllegalAccessException ex)
        {
        }
        catch(InstantiationException ex)
        {
        }
        return schema;
    }

    /**
     * getSchemaClass
     *
     * @param tSchemaClassName String
     * @return Class
     */
    private Class getSchemaClass(String tSchemaClassName)
    {
        try
        {
            return Class.forName(mPackagPath + "schema."
                          + tSchemaClassName + "Schema"); //定义Schema对象
        }
        catch(ClassNotFoundException ex)
        {
            ex.printStackTrace();
            mErrors.addOneError("没有找到Schema：" + mSchemaClassName);
        }
        return null;
    }

    /**
     * 生成SchemaSet对象
     * @param schemaSetClass Class
     * @param tSchemaSetClassName String
     * @return SchemaSet
     */
    private SchemaSet getSchemaSet(Class cSchemaSetClass, String tSchemaClassName)
    {
        SchemaSet tSchemaSet = null;
        try
        {
            tSchemaSet = (SchemaSet) cSchemaSetClass.newInstance();
        }
        catch(IllegalAccessException ex1)
        {
        }
        catch(InstantiationException ex1)
        {
        }

        return tSchemaSet;
    }

    public static void main(String[] args)
    {
        BPOSchemaCreator bpocreateschema = new BPOSchemaCreator();
    }
}
