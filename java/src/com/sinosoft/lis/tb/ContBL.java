package com.sinosoft.lis.tb;

import com.sinosoft.lis.bl.LCAppntBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.util.Vector;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.vbl.LCDutyBLSet;
import com.sinosoft.lis.vbl.LCPremBLSet;
import com.sinosoft.lis.vbl.LCGetBLSet;
import com.sinosoft.lis.cbcheck.RecalculationPremBL;

/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author HST
 * @version 1.0
 * @date 2002-09-25
 */
public class ContBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private Reflections ref = new Reflections();

    private GlobalInput mGlobalInput = new GlobalInput();

    private MMap map = new MMap();

    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();

    private String theCurrentTime = PubFun.getCurrentTime();

    private boolean changecvalidateflag = false; //判断是否修改生效日期标记

    private boolean TakeBackCertifyFalg = false; //是否回收单证标记

    /** 业务处理相关变量 */
    private LCContSchema mLCContSchema = new LCContSchema();

    private LCAppntBL mLCAppntBL = new LCAppntBL();

    private LDPersonSchema mLDPersonSchema = new LDPersonSchema();

    //private LDPersonSchema tLDPersonSchema = new LDPersonSchema();
    private LDPersonSet mLDPersonSet = new LDPersonSet();

    private LCAddressSchema mLCAddressSchema = new LCAddressSchema();

    private LCAccountSchema mLCAccountSchema = new LCAccountSchema();

    private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();

    private LCContSchema mOldLCCont = new LCContSchema();

    private String mSavePolType = "";

    private String GrpNo = "";

    private String GrpName = "";

    private LDGrpSchema tLDGrpSchema;

    private LCCustomerImpartSet mLCCustomerImpartSet;

    private LCCustomerImpartParamsSet mLCCustomerImpartParamsSet;

    private boolean updateldperson = false;

    private LCPolBL tLCPolBL = new LCPolBL();

    private LCDutyBLSet tLCDutyBLSet = new LCDutyBLSet();

    private LCContSchema mmLCContSchema = new LCContSchema();

    private LCPolSchema mmLCPolSchema = new LCPolSchema();

    private LCDutySet mmLCDutySet = new LCDutySet();

    private LCGetSet mmLCGetSet = new LCGetSet();

    private LCPremSet mmLCPremSet = new LCPremSet();

    private LCPolBL mLCPolBL = new LCPolBL();

    private LCPremBLSet mLCPremBLSet = new LCPremBLSet();

    private LCGetBLSet mLCGetBLSet = new LCGetBLSet();

    private LCDutyBLSet mLCDutyBLSet = new LCDutyBLSet();

    private LCPolSchema mLCPolSchema = new LCPolSchema();

    double SumPrem = 0; //合计保费

    /** 处理投保人换号被保人同时换号问题 */
    private MMap insured_map;

    private MMap mMap = new MMap();

    /** 解决 LCPol 表被二次覆盖的问题，备份要更新信息 */
    private String mStrLcpolup = null;

    private String mStrLcpolPaymodeup = null;

    private String CustomerType;
	
		//modify by zxs 
	private String Authorization = "";
	
	private String YBTFlag="";
	
	//modify by zxs 2019-01-16
	private String AppntPassIDNo = "";
    
    private LCExtendSchema mLCExtendSchema = new LCExtendSchema();
    
    //#1855 客户类型
    private LCContSubSchema mLCContSubSchema = new LCContSubSchema();

    // @Constructor
    public ContBL()
    {
    }

    /**
     * 数据提交的公共方法
     *
     * @param: cInputData 传入的数据 cOperate 数据操作字符串
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将传入的数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        System.out.println("now in ContBL submit");
        // 将外部传入的数据分解到本类的属性中，准备处理
        if (this.getInputData() == false)
        {
            return false;
        }
        System.out.println("---getInputData---");

        // 校验传入的数据
        //        if (!mOperate.equals("DELETE||CONT"))
        //        {
        if (this.checkData() == false)
        {
            return false;
        }
        System.out.println("---checkData---");
        //        }

        // 根据业务逻辑对数据进行处理

        if (this.dealData() == false)
        {
            return false;
        }
        

        if(!dealContSub()){
            return false;
        }


        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");

        //　数据提交、保存

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start tPRnewManualDunBLS Submit...");

        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "ContBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("---commitData---");

        //    if (TakeBackCertifyFalg) { //如果回收单证标记为真
        //      //单证回收
        //      PubCertifyTakeBack tPubCertifyTakeBack = new PubCertifyTakeBack();
        //      if (tPubCertifyTakeBack.CheckNewType(tPubCertifyTakeBack.CERTIFY_CheckNo2)) { //如果需要单证回收
        //        String operator = mGlobalInput.Operator;
        //        if (!tPubCertifyTakeBack.CertifyTakeBack_A(mLCGrpPolSchema.getPrtNo(),
        //            mLCGrpPolSchema.getPrtNo(), tPubCertifyTakeBack.CERTIFY_GrpProposal,
        //            operator)) {
        //          System.out.println("单证回收错误（集体投保单）:" + mLCGrpPolSchema.getPrtNo());
        //          System.out.println("错误原因：" +
        //                             tPubCertifyTakeBack.mErrors.getFirstError().
        //                             toString());
        //          //保存错误信息
        //        }
        //      }
        //    }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     *
     * @return boolean
     */
    private boolean getInputData()
    {
        try
        {
            //全局变量
            mGlobalInput.setSchema((GlobalInput) mInputData
                    .getObjectByObjectName("GlobalInput", 0));
            //合同表
            mLCContSchema.setSchema((LCContSchema) mInputData
                    .getObjectByObjectName("LCContSchema", 0));
            mLCAppntBL.setSchema((LCAppntSchema) mInputData
                    .getObjectByObjectName("LCAppntSchema", 0));
            System.out.println("BL3乱码没得"+mLCAppntBL.getAppntName()+"BL3乱码没得");
            //      mLDPersonSchema.setSchema( (LDPersonSchema) mInputData.
            //                                getObjectByObjectName("LDPersonSchema", 0));
            mLCAddressSchema.setSchema((LCAddressSchema) mInputData
                    .getObjectByObjectName("LCAddressSchema", 0));
            mLCAccountSchema.setSchema((LCAccountSchema) mInputData
                    .getObjectByObjectName("LCAccountSchema", 0));
            mLCExtendSchema = (LCExtendSchema) mInputData.getObjectByObjectName("LCExtendSchema", 0);
            //   mLCInsuredSchema.setSchema( (LCInsuredSchema) mInputData.
            //                               getObjectByObjectName("LCInsuredSchema", 0));
            //告知信息
            this.mLCCustomerImpartSet = (LCCustomerImpartSet) mInputData
                    .getObjectByObjectName("LCCustomerImpartSet", 0);
            
            mLCContSubSchema = (LCContSubSchema) mInputData.getObjectByObjectName("LCContSubSchema", 0);
            
            TransferData tTransferData = (TransferData) mInputData
                    .getObjectByObjectName("TransferData", 0);
            GrpNo = (String) tTransferData.getValueByName("GrpNo");
            GrpName = (String) tTransferData.getValueByName("GrpName");
			//modify by zxs 
			Authorization = (String) tTransferData.getValueByName("Authorization");
			YBTFlag = (String) tTransferData.getValueByName("channl");
			//modify by zxs 2019-01-16
			AppntPassIDNo = (String) tTransferData.getValueByName("AppntPassIDNo");
            CustomerType = (String) tTransferData
                    .getValueByName("CustomerType");
            if (CustomerType == null || CustomerType.equals("")
                    || CustomerType.equals("null"))
            {
                CustomerType = "1"; //个人
            }
            if (mOperate.equals("UPDATE||CONT"))
            {
                LCContDB tLCContDB = new LCContDB();
                tLCContDB.setContNo(mLCContSchema.getContNo());
                if (tLCContDB.getInfo() == false)
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLCContDB.mErrors);
                    return false;
                }
                mOldLCCont.setSchema(tLCContDB);
                if (mOldLCCont.getStateFlag() == null
                        || mOldLCCont.getStateFlag().equals(""))
                {
                    mOldLCCont.setStateFlag("0");
                }
            }
            
            return true;
        }
        catch (Exception ex)
        {
            CError tError = new CError();
            tError.moduleName = "ProposalBL";
            tError.functionName = "checkData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            return false;

        }

    }

    /**
     * 校验传入的数据
     *
     * @return boolean
     */
    private boolean checkData()
    {
        if (!mOperate.equals("DELETE||CONT"))
        {
            if (mLCContSchema.getContType() == null)
            { //2-集体总单,1-个人总投保单)
                mLCContSchema.setContType("1"); //缺省设为个人单
            }

            if (mLCContSchema.getPolType() == null)
            { //保单类型标记0--个人单，1--无名单，2 ---公共帐户)
                mLCContSchema.setPolType("0"); //缺省设为个人单
            }
            if (mLCContSchema.getAppFlag() == null)
            { //签单标记为空时，补为0
                mLCContSchema.setAppFlag("0");
                mLCContSchema.setStateFlag("0");
            }

            if (mLCContSchema.getCardFlag() == null)
            { //卡单标记为空时，补为0
                mLCContSchema.setCardFlag("0");
            }

            if (this.checkLCAppnt() == false)
            {
                return false;
            }
            if (this.checkLCAddress() == false)
            {
                return false;
            }
            // 校验非空值
            if (mLCContSchema.getPrtNo() == null
                    || mLCContSchema.getPrtNo().trim().equals(""))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ContBL";
                tError.functionName = "checkData";
                tError.errorMessage = "请录入印刷号码!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //校验代理人编码并置上代理人组别
            if (mLCContSchema.getAgentCode() == null
                    || mLCContSchema.getAgentCode().equals(""))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpProposalBL";
                tError.functionName = "dealData";
                tError.errorMessage = "请您确认有：代理人编码!";
                this.mErrors.addOneError(tError);
                return false;
            }
            else
            {
                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(mLCContSchema.getAgentCode());
                if (tLAAgentDB.getInfo() == false)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "GrpProposalBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "请您确认：代理人编码没有输入错误!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (tLAAgentDB.getManageCom() == null)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ContBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "代理人编码对应数据库中的管理机构为空！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (!tLAAgentDB.getManageCom().equals(
                        mLCContSchema.getManageCom()))
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "GrpProposalBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "您录入的管理机构和数据库中代理人编码对应的管理机构不符合！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                mLCContSchema.setAgentGroup(tLAAgentDB.getAgentGroup());

            }
        }

        if (mOperate.equals("INSERT||CONT"))
        {

            String strSQL = "select count(*) from lccont where prtno='"
                    + mLCContSchema.getPrtNo() + "'";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(strSQL);
            String strCount = tSSRS.GetText(1, 1);
            int SumCount = Integer.parseInt(strCount);
            if (SumCount > 0)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "checkData";
                tError.errorMessage = "同一印刷号只能录入一个合同!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        if (mOperate.equals("DELETE||CONT"))
        {

            String strSQL = "select count(*) from LCInsured where contno='"
                    + mLCContSchema.getContNo() + "'";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(strSQL);
            String strCount = tSSRS.GetText(1, 1);
            int SumCount = Integer.parseInt(strCount);
            if (SumCount > 0)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ContBL";
                tError.functionName = "checkData";
                tError.errorMessage = "该合同下还有被保险人未删除，不能删除合同信息!";
                this.mErrors.addOneError(tError);
                return false;
            }

        }
        /*生成号码
         if( mLCGrpPolSchema.getGrpNo() == null || mLCGrpPolSchema.getGrpNo().trim().equals( "" ))
         {
         String tLimit = "SN";
         String tNo = PubFun1.CreateMaxNo( "GrpNo", tLimit );
         mLCGrpPolSchema.setGrpNo( tNo );
         }
         */
        if (mLCContSchema.getManageCom() == null
                || mLCContSchema.getManageCom().trim().equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ContBL";
            tError.functionName = "checkData";
            tError.errorMessage = "请录入管理机构!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     *
     * @return boolean
     */
    private boolean dealData()
    {
        //产生集体投保单号码
        //设置LCCont相关值
        // TakeBackCertifyFalg = true;
        String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
        String tNo = "";
        System.out.println("aaaa" + mLCContSchema.getContNo());
        if (mLCContSchema.getContNo() == null
                || ("").equals(mLCContSchema.getContNo()))
        {
            tNo = PubFun1.CreateMaxNo("ProposalContNo", tLimit);
            mLCContSchema.setProposalContNo(tNo);
            //tNo = PubFun1.CreateMaxNo("ContNo", tLimit);
            //System.out.println("ContNo" + tNo);
            mLCContSchema.setContNo(tNo);
            mLCContSchema.setGrpContNo(SysConst.ZERONO); //集体合同号
        }
        mLCContSchema.setSignCom(mLCContSchema.getManageCom());
        mLCContSchema.setExecuteCom(mLCContSchema.getManageCom());
        //设置合同信息
        mLCContSchema.setAppFlag("0");
        mLCContSchema.setStateFlag("0");
        mLCContSchema.setUWFlag("0");
        mLCContSchema.setApproveFlag("0");
        mLCContSchema.setMult(0);
        mLCContSchema.setPrem(0);
        mLCContSchema.setAmnt(0);
        if (mLCContSchema.getIntlFlag() == null
                || mLCContSchema.getIntlFlag().equals(""))
        {
            mLCContSchema.setIntlFlag("0"); //非国际业务
        }
        if (mLCContSchema.getPayerType() == null
                || mLCContSchema.getPayerType().equals(""))
        {
            mLCContSchema.setPayerType("1"); //非国际业务
        }

        mLCContSchema.setModifyDate(theCurrentDate);
        mLCContSchema.setModifyTime(theCurrentTime);
        mLCContSchema.setOperator(mGlobalInput.Operator);

        String sqlScan = "update  ES_DOC_MAIN " + "set State = '02' "
                + "where DocCode = '" + mLCContSchema.getPrtNo() + "' "
                + "   and SubType = 'TB01' " + "   and State = '01' ";
        map.put(sqlScan, SysConst.UPDATE);
        
        //将客户身份证号码中的x转换成大写（投保人） 2009-02-05 liuyp
        if (mLCAppntBL.getIDType() != null && mLCAppntBL.getIDNo() != null) {
        	if (mLCAppntBL.getIDType().equals("0")) {
        		String tLCAppntIdNo = mLCAppntBL.getIDNo().toUpperCase();
                mLCAppntBL.setIDNo(tLCAppntIdNo);
            }
            
            //校验投保人的证件类型                     by zhangyang     2011-02-23
            String appntName = mLCAppntBL.getAppntName();
            String appntIDType = mLCAppntBL.getIDType();
            if(!checkIDType(appntIDType,"appnt",appntName))
            {
                return false;
            }
            //--------------------------------------------------------------
        }

        //产生客户号
        if (mLCAppntBL.getAppntNo() == null
                || ("").equals(mLCAppntBL.getAppntNo()))
        {
            
            tNo = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
            
            //ref.transFields(mLDPersonSchema, mLCAppntBL);
            mLCAppntBL.setAppntNo(tNo);
            mLDPersonSchema.setCustomerNo(tNo);
            mLDPersonSchema.setName(mLCAppntBL.getAppntName());
            mLDPersonSchema.setSex(mLCAppntBL.getAppntSex());
            mLDPersonSchema.setBirthday(mLCAppntBL.getAppntBirthday());
            mLDPersonSchema.setIDType(mLCAppntBL.getIDType());
            mLDPersonSchema.setIDNo(mLCAppntBL.getIDNo());
            mLDPersonSchema.setNativePlace(mLCAppntBL.getNativePlace());
            mLDPersonSchema.setNationality(mLCAppntBL.getNationality());
            mLDPersonSchema.setRgtAddress(mLCAppntBL.getRgtAddress());
            mLDPersonSchema.setMarriage(mLCAppntBL.getMarriage());
            mLDPersonSchema.setDegree(mLCAppntBL.getDegree());
            mLDPersonSchema.setOccupationType(mLCAppntBL.getOccupationType());
            mLDPersonSchema.setOccupationCode(mLCAppntBL.getOccupationCode());
            mLDPersonSchema.setWorkType(mLCAppntBL.getWorkType());
            mLDPersonSchema.setSalary(mLCAppntBL.getSalary());
            mLDPersonSchema.setPosition(mLCAppntBL.getPosition());
            mLDPersonSchema.setPluralityType(mLCAppntBL.getPluralityType());
            mLDPersonSchema.setSmokeFlag(mLCAppntBL.getSmokeFlag());
            mLDPersonSchema.setMakeDate(theCurrentDate);
            mLDPersonSchema.setMakeTime(theCurrentTime);
            mLDPersonSchema.setModifyDate(theCurrentDate);
            mLDPersonSchema.setModifyTime(theCurrentTime);
            mLDPersonSchema.setOperator(mGlobalInput.Operator);
            mLDPersonSchema.setGrpNo(GrpNo);
            mLDPersonSchema.setGrpName(GrpName);
            mLDPersonSchema.setCustomerType(CustomerType);
            mLDPersonSchema.setNativeCity(mLCAppntBL.getNativeCity());
            
            System.out.println("BL乱码没得"+mLDPersonSchema.getName()+"BL乱码没得");
			//modify by zxs 
		if(Authorization!=null&&!Authorization.equals("")){
			mLDPersonSchema.setAuthorization(Authorization);
			}
		if(AppntPassIDNo!=null&&!AppntPassIDNo.equals("")){
			mLDPersonSchema.setOriginalIDNo(AppntPassIDNo);
		}
            mLDPersonSet.add(mLDPersonSchema); //tLDPersonSet包含需要新建的客户
        }
        else
        {
            LDPersonDB tLDPersonDB = new LDPersonDB();
            tLDPersonDB.setCustomerNo(mLCAppntBL.getAppntNo());
            if (tLDPersonDB.getInfo() == false)
            {
                CError tError = new CError();
                tError.moduleName = "ContBL";
                tError.functionName = "updateldperson";
                tError.errorMessage = "没有查到客户信息!";
                this.mErrors.addOneError(tError);
            }
            tLDPersonDB.setGrpName(GrpName);
            tLDPersonDB.setCustomerType(CustomerType);
            tLDPersonDB.setNativePlace(mLCAppntBL.getNativePlace());
            tLDPersonDB.setNativeCity(mLCAppntBL.getNativeCity());
           //modify by zxs 
			LDPersonSchema ldPersonSchema = tLDPersonDB.getSchema();
			
			if(Authorization!=null&&!Authorization.equals("")){
				if(Authorization.equals("0")&&(ldPersonSchema.getAuthorization()==null||ldPersonSchema.getAuthorization().equals(""))){
					ldPersonSchema.setAuthorization(Authorization);
				}
				if(Authorization.equals("1")){
					ldPersonSchema.setAuthorization(Authorization);
				}
			}
			if(AppntPassIDNo!=null&&!AppntPassIDNo.equals("")){
				ldPersonSchema.setOriginalIDNo(AppntPassIDNo);
			}
			if(!mLCAppntBL.getIDType().equals("7")){
			ldPersonSchema.setIDType(mLCAppntBL.getIDType());
			ldPersonSchema.setIDNo(mLCAppntBL.getIDNo());
			}
			System.out.println("BL2乱码没得"+mLDPersonSchema.getName()+"BL2乱码没得");
			map.put(ldPersonSchema, SysConst.UPDATE); // 由于界面显示了单位名称，而LCCont没有该信息

            //            mLDPersonSchema = tLDPersonDB.getSchema();
            //            mLDPersonSchema.setBirthday(mLCAppntBL.getAppntBirthday());
            //            mLDPersonSchema.setName(mLCAppntBL.getAppntName());
            //            mLDPersonSchema.setSex(mLCAppntBL.getAppntSex());
            //            mLDPersonSchema.setOccupationCode(mLCAppntBL.getOccupationCode());
            //            mLDPersonSchema.setIDType(mLCAppntBL.getIDType());
            //            mLDPersonSchema.setIDNo(mLCAppntBL.getIDNo());
            //            mLDPersonSchema.setNativePlace(mLCAppntBL.getNativePlace());
            //            mLDPersonSchema.setNationality(mLCAppntBL.getNationality());
            //            mLDPersonSchema.setRgtAddress(mLCAppntBL.getRgtAddress());
            //            mLDPersonSchema.setMarriage(mLCAppntBL.getMarriage());
            //            mLDPersonSchema.setDegree(mLCAppntBL.getDegree());
            //            mLDPersonSchema.setOccupationType(mLCAppntBL.getOccupationType());
            //            mLDPersonSchema.setOccupationCode(mLCAppntBL.getOccupationCode());
            //            mLDPersonSchema.setWorkType(mLCAppntBL.getWorkType());
            //            mLDPersonSchema.setPluralityType(mLCAppntBL.getPluralityType());
            //            mLDPersonSchema.setSmokeFlag(mLCAppntBL.getSmokeFlag());
            //            mLDPersonSchema.setGrpNo(GrpNo);
            //            mLDPersonSchema.setGrpName(GrpName);
            //            mLDPersonSet.add(mLDPersonSchema);
            //            this.map.put(mLDPersonSet, "UPDATE");
            //            updateldperson = true;
        }
        //zxs #4351 20190424
        
        if(!"ybt".equals(YBTFlag)){
        if("1".equals(mLCContSchema.getContType())&&!mLCContSchema.getPrtNo().startsWith("PD")){
        String appntno = new ExeSQL().getOneValue("select appntno from lcappnt where prtno = '"+mLCContSchema.getPrtNo()+"'");
        MMap mMap = new MMap();
        if(appntno!=null&&!appntno.equals("")){
        	mMap.put("delete from LCPersonTrace where customerno = '"+appntno+"' and ContractNo='"+mLCContSchema.getPrtNo()+"' ", "DELETE");
        }
        VData vData = new VData();
        vData.add(mMap);
        PubSubmit pubSubmit = new PubSubmit();
        if (!pubSubmit.submitData(vData, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(pubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "ContBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        LCPersonTraceSchema lcPersonTraceSchema = new LCPersonTraceSchema();
        if(tNo!=null&&!tNo.equals("")&&tNo.length()==9){
        	lcPersonTraceSchema.setCustomerNo(tNo);
        }else{
        	lcPersonTraceSchema.setCustomerNo(mLCAppntBL.getAppntNo());
        }
        SSRS tSSRS2 = new SSRS();
        String sql1 = "Select Case When varchar(max(int(TraceNo))) Is Null Then '0' Else varchar(max(int(TraceNo))) End from LCPersonTrace where CustomerNo='"
                + lcPersonTraceSchema.getCustomerNo() + "'";
        ExeSQL tExeSQL1 = new ExeSQL();
        tSSRS2 = tExeSQL1.execSQL(sql1);
        Integer firstinteger1 = Integer.valueOf(tSSRS2.GetText(1, 1));
        int tTraceNo = firstinteger1.intValue() + 1;
        Integer sTraceNo = new Integer(tTraceNo);
        String mTraceNo = sTraceNo.toString();
        lcPersonTraceSchema.setTraceNo(mTraceNo);
        if(Authorization==null || Authorization.equals("")||"0".equals(Authorization)){
        	 lcPersonTraceSchema.setSharedMark("0");
             lcPersonTraceSchema.setAuthorization("0");
             lcPersonTraceSchema.setCustomerContact("6");
        }else{
        	 lcPersonTraceSchema.setSharedMark(Authorization);
             lcPersonTraceSchema.setAuthorization(Authorization);
             lcPersonTraceSchema.setCustomerContact("0");
        }
        lcPersonTraceSchema.setSpecialLimitMark("0");
        lcPersonTraceSchema.setCompanySource("2");//人保健康
        lcPersonTraceSchema.setInstitutionSource(mLCContSchema.getManageCom());
        lcPersonTraceSchema.setBusinessLink("1");
        System.out.println("ssssssss="+mLCContSchema.getPrtNo());
        lcPersonTraceSchema.setAuthType("1");//授权条款
        lcPersonTraceSchema.setAuthVersion("1.0");
        lcPersonTraceSchema.setContractNo(mLCContSchema.getPrtNo());
        lcPersonTraceSchema.setSendDate(theCurrentDate);
        lcPersonTraceSchema.setSendTime(theCurrentTime);
        lcPersonTraceSchema.setModifyDate(theCurrentDate);
        lcPersonTraceSchema.setModifyTime(theCurrentTime);
         map.put(lcPersonTraceSchema, "INSERT");
        }
        }
        

        //产生客户地址,需要增加统一处理客户地址方法
        if (mLCAddressSchema != null)
        {
            if (mLCAddressSchema.getAddressNo() == null
                    || ("").equals(mLCAddressSchema.getAddressNo()))
            {
                try
                {
                    SSRS tSSRS = new SSRS();
                    String sql = "Select Case When varchar(max(int(AddressNo))) Is Null Then '0' Else varchar(max(int(AddressNo))) End from LCAddress where CustomerNo='"
                            + mLCAppntBL.getAppntNo() + "'";
                    ExeSQL tExeSQL = new ExeSQL();
                    tSSRS = tExeSQL.execSQL(sql);
                    Integer firstinteger = Integer.valueOf(tSSRS.GetText(1, 1));
                    int ttNo = firstinteger.intValue() + 1;
                    Integer integer = new Integer(ttNo);
                    tNo = integer.toString();
                    System.out.println("得到的地址码是：" + tNo);
                    mLCAddressSchema.setAddressNo(tNo);
                }
                catch (Exception e)
                {
                    CError tError = new CError();
                    tError.moduleName = "ContBL";
                    tError.functionName = "createAddressNo";
                    tError.errorMessage = "地址码超长,生成号码失败,请先删除原来的超长地址码!";
                    this.mErrors.addOneError(tError);
                    mLCAddressSchema.setAddressNo("");
                }

            }
            mLCAddressSchema.setCustomerNo(mLCAppntBL.getAppntNo());
            mLCAddressSchema.setModifyDate(theCurrentDate);
            mLCAddressSchema.setModifyTime(theCurrentTime);
            mLCAddressSchema.setOperator(mGlobalInput.Operator);
            mLCAddressSchema.setMakeDate(theCurrentDate);
            mLCAddressSchema.setMakeTime(theCurrentTime);
            map.put(mLCAddressSchema, "DELETE&INSERT");
        }

        //产生客户账户,需要增加统一处理客户账户方法
        if (mLCAccountSchema != null)
        {
            if (mLCAccountSchema.getBankCode() == null
                    || mLCAccountSchema.getBankAccNo() == null
                    || mLCAccountSchema.getBankCode().equals("")
                    || mLCAccountSchema.getBankAccNo().equals(""))
            {
            }
            else
            {
                mLCAccountSchema.setCustomerNo(mLCAppntBL.getAppntNo());
                if (mLCAccountSchema.getAccName() == null
                        && ("").equals(mLCAccountSchema.getAccName()))
                {
                    mLCAccountSchema.setAccName(mLCAppntBL.getAppntName());
                }
                mLCAccountSchema.setMakeDate(theCurrentDate);
                mLCAccountSchema.setMakeTime(theCurrentTime);
                mLCAccountSchema.setModifyDate(theCurrentDate);
                mLCAccountSchema.setModifyTime(theCurrentTime);
                mLCAccountSchema.setOperator(mGlobalInput.Operator);

                map.put(mLCAccountSchema, "DELETE&INSERT");
            }
        }

        //个人投保人部分信息
        if (mLCAddressSchema != null)
        {
            mLCAppntBL.setAddressNo(mLCAddressSchema.getAddressNo());
        }
        mLCAppntBL.setContNo(mLCContSchema.getContNo());
        mLCAppntBL.setGrpContNo(mLCContSchema.getGrpContNo());
        mLCAppntBL.setPrtNo(mLCContSchema.getPrtNo());
        mLCAppntBL.setManageCom(mLCContSchema.getManageCom());
        mLCAppntBL.setModifyDate(theCurrentDate);
        mLCAppntBL.setModifyTime(theCurrentTime);
        mLCAppntBL.setOperator(mGlobalInput.Operator);
        if (this.mOperate.equals("UPDATE||CONT"))
        {
            if (!dealInsured())
            {
                return false;
            }

        }
        //设置合同上投保人信息
        mLCContSchema.setAppntSex(mLCAppntBL.getAppntSex());
        mLCContSchema.setAppntBirthday(mLCAppntBL.getAppntBirthday());
        mLCContSchema.setAppntIDNo(mLCAppntBL.getIDNo());
        mLCContSchema.setAppntIDType(mLCAppntBL.getIDType());
        mLCContSchema.setAppntName(mLCAppntBL.getAppntName());
        mLCContSchema.setAppntNo(mLCAppntBL.getAppntNo());
        mLCContSchema.setInsuredNo("0"); //暂填
        mLCContSchema.setAppFlag("0");
        mLCContSchema.setStateFlag("0");
        mLCContSchema.setApproveFlag("0");
        mLCContSchema.setUWFlag("0");

        //处理告知信息
        if (mLCCustomerImpartSet != null && mLCCustomerImpartSet.size() > 0)
        {
            //设置所有告知信息得客户号码
            for (int i = 1; i <= mLCCustomerImpartSet.size(); i++)
            {
                mLCCustomerImpartSet.get(i)
                        .setContNo(mLCContSchema.getContNo());
                mLCCustomerImpartSet.get(i).setGrpContNo(
                        mLCContSchema.getGrpContNo());
                mLCCustomerImpartSet.get(i).setPrtNo(mLCContSchema.getPrtNo());
                mLCCustomerImpartSet.get(i).setProposalContNo(
                        mLCContSchema.getProposalContNo());
                mLCCustomerImpartSet.get(i).setCustomerNo(
                        mLCAppntBL.getAppntNo());
            }
            CustomerImpartBL mCustomerImpartBL = new CustomerImpartBL();
            VData tempVData = new VData();
            tempVData.add(mLCCustomerImpartSet);
            tempVData.add(mGlobalInput);
            mCustomerImpartBL.submitData(tempVData, "IMPART||DEAL");
            if (mCustomerImpartBL.mErrors.needDealError())
            {
                CError tError = new CError();
                tError.moduleName = "ContInsuredBL";
                tError.functionName = "dealData";
                tError.errorMessage = mCustomerImpartBL.mErrors.getFirstError()
                        .toString();
                this.mErrors.addOneError(tError);
                return false;
            }
            tempVData.clear();
            tempVData = mCustomerImpartBL.getResult();
            if (null != (LCCustomerImpartSet) tempVData.getObjectByObjectName(
                    "LCCustomerImpartSet", 0))
            {
                mLCCustomerImpartSet = (LCCustomerImpartSet) tempVData
                        .getObjectByObjectName("LCCustomerImpartSet", 0);
                System.out.println("告知条数" + mLCCustomerImpartSet.size());
            }
            else
            {
                System.out.println("告知条数为空");
            }

            if (null != (LCCustomerImpartParamsSet) tempVData
                    .getObjectByObjectName("LCCustomerImpartParamsSet", 0))
            {
                mLCCustomerImpartParamsSet = (LCCustomerImpartParamsSet) tempVData
                        .getObjectByObjectName("LCCustomerImpartParamsSet", 0);
            }
        }

        if (mOperate.equals("UPDATE||CONT"))
        {
            mLCAppntBL.setContNo(mLCContSchema.getContNo());
            mLCAppntBL.setPrtNo(mLCContSchema.getPrtNo());
            mLCContSchema.setProposalContNo(mOldLCCont.getProposalContNo());
            mLCContSchema.setContNo(mOldLCCont.getContNo());
            mLCContSchema.setAppFlag(mOldLCCont.getAppFlag());
            mLCContSchema.setStateFlag(mOldLCCont.getStateFlag());
            mLCContSchema.setUWFlag(mOldLCCont.getUWFlag());
            mLCContSchema.setApproveFlag(mOldLCCont.getApproveFlag());
            mLCContSchema.setApproveDate(mOldLCCont.getApproveDate());
            mLCContSchema.setApproveCode(mOldLCCont.getApproveCode());
            mLCContSchema.setMult(mOldLCCont.getMult());
            mLCContSchema.setPrem(mOldLCCont.getPrem());
            mLCContSchema.setAmnt(mOldLCCont.getAmnt());
            mLCContSchema.setInsuredBirthday(mOldLCCont.getInsuredBirthday());
            
            //将客户身份证号码中的x转换成大写（被保险人） 2009-02-13 liuyp
            if (mOldLCCont.getInsuredIDType() != null && mOldLCCont.getInsuredIDNo() != null ) {
            	if (mOldLCCont.getInsuredIDType().equals("0")) {
                    String tOldLCContIdNo = mOldLCCont.getInsuredIDNo().toUpperCase();
                    mOldLCCont.setInsuredIDNo(tOldLCContIdNo);
                }
            }
             
            mLCContSchema.setInsuredIDNo(mOldLCCont.getInsuredIDNo());
            mLCContSchema.setInsuredIDType(mOldLCCont.getInsuredIDType());
            mLCContSchema.setInsuredName(mOldLCCont.getInsuredName());
            mLCContSchema.setInsuredNo(mOldLCCont.getInsuredNo());
            mLCContSchema.setInsuredNo(mOldLCCont.getInsuredNo());
            mLCContSchema.setInsuredSex(mOldLCCont.getInsuredSex());
            //            mLCContSchema.setFirstTrialDate(mOldLCCont.getFirstTrialDate());
            //            mLCContSchema.setFirstTrialOperator(mOldLCCont.
            //                                                getFirstTrialOperator());
            //            mLCContSchema.setFirstTrialTime(mOldLCCont.getFirstTrialTime());
            //            mLCContSchema.setReceiveDate(mOldLCCont.getReceiveDate());
            mLCContSchema.setInputOperator(mOldLCCont.getInputOperator());
            if (StrTool.cTrim(mLCContSchema.getInputDate()).equals(""))
            {
                mLCContSchema.setInputDate(mOldLCCont.getInputDate());
            }
            //mLCContSchema.setInputDate(mOldLCCont.getInputDate());
            mLCContSchema.setInputTime(mOldLCCont.getInputTime());
            mLCContSchema.setInputTime(mOldLCCont.getInputTime());
            mLCContSchema.setUWOperator(mOldLCCont.getUWOperator());
            mLCContSchema.setOperator(mOldLCCont.getOperator());
            mLCContSchema.setMakeDate(mOldLCCont.getMakeDate());
            mLCContSchema.setMakeTime(mOldLCCont.getMakeTime());
            mLCContSchema.setModifyDate(theCurrentDate);
            mLCContSchema.setCardFlag(mOldLCCont.getCardFlag());
        }
        if (mOperate.equals("INSERT||CONT"))
        {
            if (StrTool.cTrim(mLCContSchema.getPolApplyDate()).equals(""))
            {
                mLCContSchema.setPolApplyDate(theCurrentDate);
            }
            mLCContSchema.setMakeDate(theCurrentDate);
            mLCContSchema.setMakeTime(theCurrentTime);
            mLCAppntBL.setMakeDate(theCurrentDate);
            mLCAppntBL.setMakeTime(theCurrentTime);
            if (mLDPersonSet.size() > 0 && !updateldperson)
            {
                if (!StrTool.cTrim(mLDPersonSet.get(1).getCustomerNo()).equals(
                        ""))
                {
                    ExeSQL tExeSQL = new ExeSQL();
                    SSRS tSSRS = new SSRS();
                    tSSRS = tExeSQL
                            .execSQL("select Appntnum from ldperson where customerno='"
                                    + mLDPersonSet.get(1).getCustomerNo()
                                    + "' with ur");
                    if (tSSRS.getMaxRow() > 0)
                    {
                        mLDPersonSet.get(1).setAppntNum(tSSRS.GetText(1, 1));
                    }
                }
                map.put(mLDPersonSet, "DELETE&INSERT");
            }
            map.put(mLCContSchema, "INSERT");
            map.put(mLCAppntBL.getSchema(), "INSERT");
            if (mLCCustomerImpartParamsSet != null)
            {
                map.put(mLCCustomerImpartParamsSet, "INSERT");
            }
            if (mLCCustomerImpartSet != null)
            {
                map.put(mLCCustomerImpartSet, "INSERT");
            }

        }
        if (mOperate.equals("UPDATE||CONT"))
        {
            if (mLDPersonSet.size() > 0)
            {
                if (!StrTool.cTrim(mLDPersonSet.get(1).getCustomerNo()).equals(
                        ""))
                {
                    ExeSQL tExeSQL = new ExeSQL();
                    SSRS tSSRS = new SSRS();
                    tSSRS = tExeSQL
                            .execSQL("select Appntnum from ldperson where customerno='"
                                    + mLDPersonSet.get(1).getCustomerNo()
                                    + "' with ur");
                    if (tSSRS.getMaxRow() > 0)
                    {
                        mLDPersonSet.get(1).setAppntNum(tSSRS.GetText(1, 1));
                    }
                }

                map.put(mLDPersonSet, "DELETE&INSERT");
            }
            //            if (mLDPersonSet.size() > 0 && updateldperson) {
            //                map.put(mLDPersonSet, "UPDATE");
            //            }
            String uw="select uwdate,uwtime from LCCont where prtno ='"+mLCContSchema.getPrtNo()+"' ";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS uwdateSSRS = tExeSQL.execSQL(uw);
            if(uwdateSSRS.getMaxRow()>0){
            	String date = uwdateSSRS.GetText(1, 1);
            	String time = uwdateSSRS.GetText(1, 2);
            	mLCContSchema.setUWDate(date);
            	mLCContSchema.setUWTime(time);
            }
            map.put(mLCContSchema, "UPDATE");
            if (changecvalidateflag == true)
            {
                map
                        .put(
                                "update lccont set prem=(select sum(prem) from lcpol where contno='"
                                        + mLCContSchema.getContNo()
                                        + "'), amnt=(select sum(amnt) from lcpol where contno='"
                                        + mLCContSchema.getContNo()
                                        + "'), mult=(select sum(mult) from lcpol where contno='"
                                        + mLCContSchema.getContNo()
                                        + "') where contno='"
                                        + mLCContSchema.getContNo() + "'",
                                "UPDATE");
            }
            map.put(mLCAppntBL.getSchema(), "UPDATE");
            
            
            if (mLCInsuredSchema != null
                    && mLCInsuredSchema.getContNo() != null
                    && mLCInsuredSchema.getInsuredNo() != null)
            {
                if (TakeBackCertifyFalg == false)
                {
                	mLCInsuredSchema.setUWDate(mLCInsuredSchema.getUWDate());
                	mLCInsuredSchema.setUWTime(mLCInsuredSchema.getUWTime());
                    map.put(this.mLCInsuredSchema, "UPDATE");
                }
            }
            map.put(mLCCustomerImpartSet, "DELETE&INSERT");
        }
        if (mOperate.equals("UPDATE||CONT"))
        {

            String strSQL = "select count(*) from LCInsured where contno='"
                    + mLCContSchema.getContNo() + "'";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(strSQL);
            String strCount = tSSRS.GetText(1, 1);
            String strSQL1 = "select polno from LCpol where contno='"
                    + mLCContSchema.getContNo() + "'";
            SSRS tSSRS1 = tExeSQL.execSQL(strSQL);
            int SumCount = Integer.parseInt(strCount);
            if (SumCount > 0)
            {
                map.put("update LCInsured set AppntNo='"
                        + mLCAppntBL.getAppntNo() + "'," + "ManageCom='"
                        + mLCContSchema.getManageCom() + "'"
                        //+"ExecuteCom='"+mLCContSchema.getExecuteCom()+"' "
                        + "where contno='" + mLCContSchema.getContNo() + "'",
                        "UPDATE");
                mStrLcpolup = "update LCPol set AppntNo='"
                        + mLCAppntBL.getAppntNo() + "'," + "AppntName='"
                        + mLCAppntBL.getAppntName() + "'," + "ManageCom='"
                        + mLCContSchema.getManageCom() + "'," + "AgentCom='"
                        + mLCContSchema.getAgentCom() + "'," + "SaleChnl='"
                        + mLCContSchema.getSaleChnl() + "'," + "polapplydate='"
                        + mLCContSchema.getPolApplyDate() + "',"
                        + "AgentCode='" + mLCContSchema.getAgentCode() + "',"
                        + "AgentGroup='" + mLCContSchema.getAgentGroup() + "' "
                        + "where contno='" + mLCContSchema.getContNo() + "'";
                map.put(mStrLcpolup, "UPDATE");
                /*                map.put("update LCPol set InsuredName='" + mLCAppntBL.getAppntName() + "',"
                 + "InsuredNo='" + mLCAppntBL.getAppntNo() + "'"
                 + "where contno='" + mLCContSchema.getContNo() +"' and InsuredNo='" + mLCAppntBL.getAppntNo() + "'"
                 , "UPDATE");
                 for(int m=1;m<=tSSRS1.getMaxRow();m++)
                 {
                 map.put("update LCBnf set "
                 + "InsuredNo='" + mLCAppntBL.getAppntNo() + "'"
                 + "where contno='" + mLCContSchema.getContNo() +"' and InsuredNo='" + mLCAppntBL.getAppntNo() + "' and polno='"+tSSRS1.GetText(1,m)+"'"
                 , "UPDATE");
                 }
                 */
                map.put("update LCPrem set AppntNo='" + mLCAppntBL.getAppntNo()
                        + "' " + "where contno='" + mLCContSchema.getContNo()
                        + "'", "UPDATE");
                
                //同步更新LCPrem和LCGet的ManageCom字段        by zhangyang 2011-09-16
                map.put("update LCPrem set ManageCom = '" + mLCContSchema.getManageCom() 
                		+ "' where ContNo = '" + mLCContSchema.getContNo()
                		+ "' ", "UPDATE");
                map.put("update LCGet set ManageCom = '" + mLCContSchema.getManageCom() 
                		+ "' where ContNo = '" + mLCContSchema.getContNo()
                		+ "' ", "UPDATE");
                //----------------------------------

                // 不同时更新LCPol中PayIntv字段。
                //更新LCPOL的数据paymode
                //                mStrLcpolPaymodeup = "update LCPol set Paymode='"
                //                        + mLCContSchema.getPayMode() + "',payintv="
                //                        + mLCContSchema.getPayIntv() + " " + "where contno='"
                //                        + mLCContSchema.getContNo() + "'";
                mStrLcpolPaymodeup = "update LCPol set Paymode='"
                        + mLCContSchema.getPayMode() + "' " + " where contno='"
                        + mLCContSchema.getContNo() + "' ";
                map.put(mStrLcpolPaymodeup, "UPDATE");
                // --------------------------------
            }

        }

        if (mOperate.equals("DELETE||CONT"))
        {
            map.put(mLCContSchema, "DELETE");
            map.put("delete from LCAppnt where ContNo='"
                    + mLCContSchema.getContNo() + "'", "DELETE");
            map.put("delete from LCCustomerImpart where ContNo='"
                    + mLCContSchema.getContNo() + "'", "DELETE");
        }
        
        if(mOperate.equals("DELETE||CONT")){
        	
        	MMap tTmpMap = null;
            tTmpMap = deleteExtendInfo(mLCExtendSchema);
            
            if (tTmpMap != null && tTmpMap.size() != 0)
            {
            	map.add(tTmpMap);
            } 
        } else if (mLCExtendSchema != null && !"".equals(mLCExtendSchema.getAssistSalechnl())) {
            MMap tTmpMap = null;
            tTmpMap = delExtendInfo(mLCExtendSchema);
            if (tTmpMap == null)
            {
                return false;
            }
            map.add(tTmpMap);
        } else {
        	String tSql = "delete from LCExtend where prtno = '" + mLCContSchema.getPrtNo() + "'";
        	map.put(tSql, SysConst.DELETE);
        }
        
        System.out.println("Map : " + map.size());
        return true;
    }

    /**
     * dealInsured
     *
     * @return boolean
     */
    private boolean dealInsured()
    {
        System.out.println("查询生效日期又没有修改，如果修改过则重新计算保费");
        LCContDB doldLCContDB = new LCContDB();
        doldLCContDB.setContNo(mLCContSchema.getContNo());
        LCContSet doldLCContSet = doldLCContDB.query(); //查询保单信息，对比生效日期
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(mLCContSchema.getContNo());
        tLCInsuredDB.setRelationToAppnt("00");
        //        tLCInsuredDB.setInsuredNo(mLCAppntBL.getAppntNo());
        LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
        
        //将客户身份证号码中的x转换成大写（被保险人） 2009-02-15 liuyp
        if (doldLCContSet != null && doldLCContSet.size() != 0)
        {
            for (int j = 1; j <= doldLCContSet.size(); j++) {
            	if (doldLCContSet.get(j).getInsuredIDType() != null && doldLCContSet.get(j).getInsuredIDNo() != null) {
            		if (doldLCContSet.get(j).getInsuredIDType().equals("0")) {
                        String toldLCContIdNo = doldLCContSet.get(j).getInsuredIDNo().toUpperCase();
                        doldLCContSet.get(j).setInsuredIDNo(toldLCContIdNo);
                    }
                    
                    //被保险人的证件类型校验                          by zhangyang    2011-02-23
                    String insuredName = doldLCContSet.get(j).getInsuredName();
                    String insuredIDType = doldLCContSet.get(j).getInsuredIDType();
                    if(insuredIDType != null && !insuredIDType.equals(""))
                    {
                        if(!checkIDType(insuredIDType,"insured",insuredName))
                        {
                            return false;
                        }
                    }
                    //-------------------------------------------------------------------
            	}
            }
        }
        
        if (tLCInsuredSet != null && tLCInsuredSet.size() != 0)
        {
            for (int j = 1; j <= tLCInsuredSet.size(); j++) {
            	if(tLCInsuredSet.get(j).getIDType() != null && tLCInsuredSet.get(j).getIDNo() != null) {
	                if (tLCInsuredSet.get(j).getIDType().equals("0")) {
	                    String tLCInsuredIdNo = tLCInsuredSet.get(j).getIDNo().toUpperCase();
	                    tLCInsuredSet.get(j).setIDNo(tLCInsuredIdNo);
	                }
                }
            }
            
            //解决万能险平台，修改投保人信息后，如果被保险人是本人的话，险种保额会翻倍的问题。  by zhangyang 2011-02-09
            LCInsuredSchema tLCInsuredSchema = tLCInsuredSet.get(1);
            String oldInsuredno = tLCInsuredSchema.getInsuredNo();
            String newInsuredno = mLCAppntBL.getAppntNo();
            
            if(!oldInsuredno.equals(newInsuredno)) //只有换了投保人了才进行
            {
                String strSQL = "select 1 from lcriskdutywrap where contno = '"
                    + mLCContSchema.getContNo() + "' and insuredno = '"
                    + oldInsuredno + "'";
                ExeSQL tExeSQL = new ExeSQL();
                String strCount = tExeSQL.getOneValue(strSQL);
                
                if(strCount != null && !strCount.equals(""))
                {
                    map.put("update lcriskdutywrap set insuredno = '" + newInsuredno + "' "
                            + "where contno = '" + mLCContSchema.getContNo() + "' "
                            + "and insuredno = '" + oldInsuredno + "' ",
                            "UPDATE");
                }
            }
            //-------------------------------------------------------------------
        }
        
        if (doldLCContSet.size() < 1)
        {
            System.out.println("查询合同信息失败！");
            CError tError = new CError();
            tError.moduleName = "ContBL.java";
            tError.functionName = "dealInsured";
            tError.errorMessage = "查询合同信息失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        String doldCValiDate = doldLCContSet.get(1).getCValiDate();
        String dnewCValiDate = mLCContSchema.getCValiDate();
        String doldInsuredno = "";
        if (!doldCValiDate.equals(dnewCValiDate))
        { //生效日期修改过
            System.out.println("修改过生效日期，进行整单重算,先看投保人信息有没有修改过！");
            if (tLCInsuredSet.size() <= 0)
            {
                System.out
                        .println("ContBL.dealData()  \n--Line:476  --Author:YangMing");
                System.out.println("被保险人与投保人不是同一个人，可以重算保费");
            }
            else if (tLCInsuredSet.size() > 1)
            {
                System.out.println("系统中存在多个与投保人是本人的被保人！");
                CError tError = new CError();
                tError.moduleName = "ContBL.java";
                tError.functionName = "dealInsured";
                tError.errorMessage = "系统中存在多个与投保人是本人的被保人！";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tLCInsuredSet != null && tLCInsuredSet.size() != 0)
            {
                LCInsuredSchema doldLCInsuredSchema = tLCInsuredSet.get(1);
                doldInsuredno = doldLCInsuredSchema.getInsuredNo();
            }
            
            String dnewInsuredno = mLCAppntBL.getAppntNo();
            if (doldInsuredno.equals(dnewInsuredno)
                    && tLCInsuredSet.size() == 1)
            {
                System.out.println("投保人和被保人是同一人，且信息没有变，准备重算保费");
                LCPremDB dLCPremDB = new LCPremDB();
                dLCPremDB.setContNo(mLCContSchema.getContNo());
                dLCPremDB.setPayPlanCode("000000");
                LCPremSet dLCPremSet = dLCPremDB.query();
                if (dLCPremSet.size() > 0)
                {
                    System.out.println("做过加费,请先删除加费后再修改生效日期");
                    CError tError = new CError();
                    tError.moduleName = "ContBL.java";
                    tError.functionName = "dealInsured";
                    tError.errorMessage = "做过加费,请先删除加费后再修改生效日期！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                LCPolDB dLCPolDB = new LCPolDB();
                dLCPolDB.setContNo(mLCContSchema.getContNo());
                LCPolSet dLCPolSet = dLCPolDB.query();
                if (dLCPolSet.size() < 1)
                {
                    System.out.println("没有险种信息，不需要保费重算！");
                }
                else
                {
                    System.out.println("开始循环计算保费");
                    mLCInsuredSchema = null;

                    for (int i = 1; i <= dLCPolSet.size(); i++)
                    {
                        System.out.println("第" + i + "次循环");
                        //                            LCPolSchema dLCPolSchema = dLCPolSet.get(i);
                        //                            dLCPolSchema.setCValiDate(mLCContSchema.
                        //                                    getCValiDate());
                        //                            LCDutyDB dLCDutyDB = new LCDutyDB();
                        //                            dLCDutyDB.setPolNo(dLCPolSchema.getPolNo());
                        //                            LCDutySet dLCDutySet = dLCDutyDB.query();
                        //                            for (int j = 1; j <=dLCDutySet.size(); j++) {
                        //                                dLCDutySet.get(j).setPayEndDate("");
                        //                                dLCDutySet.get(j).setFirstPayDate("");
                        //                                dLCDutySet.get(j).setEndDate("");
                        //                            }
                        //                            tLCPolBL = new LCPolBL();
                        //                            tLCPolBL.setSchema(dLCPolSchema);
                        //                            tLCDutyBLSet = new LCDutyBLSet();
                        //                            tLCDutyBLSet.set(dLCDutySet);
                        //                            tCalBL = new CalBL(tLCPolBL, tLCDutyBLSet, "");
                        //                            if (tCalBL.calPol() == false) {
                        //                                CError tError = new CError();
                        //                                tError.moduleName = "ContBL";
                        //                                tError.functionName = "dealDataPerson-cal1";
                        //                                tError.errorMessage = tCalBL.mErrors.
                        //                                        getFirstError();
                        //                                this.mErrors.addOneError(tError);
                        //                                return false;
                        //                            }
                        //                            mLCPolBL.setSchema(tCalBL.getLCPol().getSchema());
                        //                            mLCPremBLSet = tCalBL.getLCPrem(); //得到的保费项集合不包括加费的保费项，所以在后面处理
                        //                            mLCGetBLSet = tCalBL.getLCGet();
                        //                            mLCDutyBLSet = tCalBL.getLCDuty();
                        //                            mLCPremBLSet.setPolNo(mLCPolBL.getPolNo());
                        //                            mLCGetBLSet.setPolNo(mLCPolBL.getPolNo());
                        //
                        //                            for (int j = 1; j <= mLCPremBLSet.size(); j++) {
                        //                                mLCPremBLSet.get(j).setOperator(mGlobalInput.
                        //                                        Operator);
                        //                                mLCPremBLSet.get(j).setMakeDate(theCurrentDate);
                        //                                mLCPremBLSet.get(j).setMakeTime(theCurrentTime);
                        //                                mLCPremBLSet.get(j).setModifyDate(
                        //                                        theCurrentDate);
                        //                                mLCPremBLSet.get(j).setModifyTime(
                        //                                        theCurrentTime);
                        //                            }
                        //                            for (int j = 1; j <= mLCGetBLSet.size(); j++) {
                        //                                mLCGetBLSet.get(j).setOperator(mGlobalInput.
                        //                                        Operator);
                        //                                mLCGetBLSet.get(j).setMakeDate(theCurrentDate);
                        //                                mLCGetBLSet.get(j).setMakeTime(theCurrentTime);
                        //                                mLCGetBLSet.get(j).setModifyDate(theCurrentDate);
                        //                                mLCGetBLSet.get(j).setModifyTime(theCurrentTime);
                        //                            }
                        //                            for (int j = 1; j <= mLCDutyBLSet.size(); j++) {
                        //                                mLCDutyBLSet.get(j).setOperator(mGlobalInput.
                        //                                        Operator);
                        //                                mLCDutyBLSet.get(j).setMakeDate(theCurrentDate);
                        //                                mLCDutyBLSet.get(j).setMakeTime(theCurrentTime);
                        //                                mLCDutyBLSet.get(j).setModifyDate(theCurrentDate);
                        //                                mLCDutyBLSet.get(j).setModifyTime(theCurrentTime);
                        //                            }
                        //                            mmLCDutySet= new LCDutySet();
                        //                            mmLCDutySet.set(mLCDutyBLSet);
                        //                            mmLCGetSet = new LCGetSet();
                        //                            mmLCGetSet.set(mLCGetBLSet);
                        //                            mmLCPremSet = new LCPremSet();
                        //                            mmLCPremSet.set(mLCPremBLSet);
                        //
                        //                            dLCPolSchema.setPrem(mLCPolBL.getPrem());
                        //                            dLCPolSchema.setStandPrem(mLCPolBL.getStandPrem());
                        //                            dLCPolSchema.setAmnt(mLCPolBL.getAmnt());
                        //                            dLCPolSchema.setMult(mLCPolBL.getMult());
                        //                            dLCPolSchema.setPayEndDate(mLCPolBL.getPayEndDate());
                        //                            dLCPolSchema.setGetStartDate(mLCPolBL.getGetStartDate());
                        //                            dLCPolSchema.setEndDate(mLCPolBL.getEndDate());
                        //                            map.put(mmLCDutySet,"UPDATE");
                        //                            map.put(mmLCGetSet,"UPDATE");
                        //                            System.out.println("mmLCGetSet.size is "+mmLCGetSet.size());
                        //                            map.put(mmLCPremSet,"UPDATE");
                        //                            map.put(dLCPolSchema,"UPDATE");

                        LCPolSchema tLCPolSchema = dLCPolSet.get(i).getSchema();
                        tLCPolSchema.setCValiDate(mLCContSchema.getCValiDate());

                        //循环调用RecalculationPremBL类，重新计算保费

                        RecalculationPremBL aRecalculationPremBL = new RecalculationPremBL();
                        VData aVData = new VData();
                        aVData.add(mGlobalInput);
                        aVData.add(tLCPolSchema);
                        System.out.println(tLCPolSchema.getPolNo());
                        boolean tResult = aRecalculationPremBL.submitData(
                                aVData, "");
                        System.out.println("after!tResult===" + tResult);
                        MMap tMap = null;
                        if (tResult)
                        {
                            tMap = (MMap) aRecalculationPremBL.getResult()
                                    .getObjectByObjectName("MMap", 0);
                        }
                        if (tMap != null)
                        {
                            this.mMap.add(tMap);
                        }
                    }
                    changecvalidateflag = true;
                    System.out.println("结束计算保费");
                }

            }
            else if (!doldInsuredno.equals(dnewInsuredno)
                    && tLCInsuredSet.size() == 0)
            {
                System.out.println("投保人和被保人不是同一人，且信息没有变，准备重算保费");
                LCPremDB dLCPremDB = new LCPremDB();
                dLCPremDB.setContNo(mLCContSchema.getContNo());
                dLCPremDB.setPayPlanCode("000000");
                LCPremSet dLCPremSet = dLCPremDB.query();
                if (dLCPremSet.size() > 0)
                {
                    System.out.println("做过加费,请先删除加费后再修改生效日期");
                    CError tError = new CError();
                    tError.moduleName = "ContBL.java";
                    tError.functionName = "dealInsured";
                    tError.errorMessage = "做过加费,请先删除加费后再修改生效日期！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                LCPolDB dLCPolDB = new LCPolDB();
                dLCPolDB.setContNo(mLCContSchema.getContNo());
                LCPolSet dLCPolSet = dLCPolDB.query();
                if (dLCPolSet.size() < 1)
                {
                    System.out.println("没有险种信息，不需要保费重算！");
                }
                else
                {
                    System.out.println("开始循环计算保费");
                    mLCInsuredSchema = null;

                    for (int i = 1; i <= dLCPolSet.size(); i++)
                    {
                        System.out.println("第" + i + "次循环");
                        //                            LCPolSchema dLCPolSchema = dLCPolSet.get(i);
                        //                            dLCPolSchema.setCValiDate(mLCContSchema.
                        //                                    getCValiDate());
                        //                            LCDutyDB dLCDutyDB = new LCDutyDB();
                        //                            dLCDutyDB.setPolNo(dLCPolSchema.getPolNo());
                        //                            LCDutySet dLCDutySet = dLCDutyDB.query();
                        //                            for (int j = 1; j <=dLCDutySet.size(); j++) {
                        //                                dLCDutySet.get(j).setPayEndDate("");
                        //                                dLCDutySet.get(j).setFirstPayDate("");
                        //                                dLCDutySet.get(j).setEndDate("");
                        //                            }
                        //                            tLCPolBL = new LCPolBL();
                        //                            tLCPolBL.setSchema(dLCPolSchema);
                        //                            tLCDutyBLSet = new LCDutyBLSet();
                        //                            tLCDutyBLSet.set(dLCDutySet);
                        //                            tCalBL = new CalBL(tLCPolBL, tLCDutyBLSet, "");
                        //                            if (tCalBL.calPol() == false) {
                        //                                CError tError = new CError();
                        //                                tError.moduleName = "ContBL";
                        //                                tError.functionName = "dealDataPerson-cal1";
                        //                                tError.errorMessage = tCalBL.mErrors.
                        //                                        getFirstError();
                        //                                this.mErrors.addOneError(tError);
                        //                                return false;
                        //                            }
                        //                            mLCPolBL.setSchema(tCalBL.getLCPol().getSchema());
                        //                            mLCPremBLSet = tCalBL.getLCPrem(); //得到的保费项集合不包括加费的保费项，所以在后面处理
                        //                            mLCGetBLSet = tCalBL.getLCGet();
                        //                            mLCDutyBLSet = tCalBL.getLCDuty();
                        //                            mLCPremBLSet.setPolNo(mLCPolBL.getPolNo());
                        //                            mLCGetBLSet.setPolNo(mLCPolBL.getPolNo());
                        //
                        //                            for (int j = 1; j <= mLCPremBLSet.size(); j++) {
                        //                                mLCPremBLSet.get(j).setOperator(mGlobalInput.
                        //                                        Operator);
                        //                                mLCPremBLSet.get(j).setMakeDate(theCurrentDate);
                        //                                mLCPremBLSet.get(j).setMakeTime(theCurrentTime);
                        //                                mLCPremBLSet.get(j).setModifyDate(
                        //                                        theCurrentDate);
                        //                                mLCPremBLSet.get(j).setModifyTime(
                        //                                        theCurrentTime);
                        //                            }
                        //                            for (int j = 1; j <= mLCGetBLSet.size(); j++) {
                        //                                mLCGetBLSet.get(j).setOperator(mGlobalInput.
                        //                                        Operator);
                        //                                mLCGetBLSet.get(j).setMakeDate(theCurrentDate);
                        //                                mLCGetBLSet.get(j).setMakeTime(theCurrentTime);
                        //                                mLCGetBLSet.get(j).setModifyDate(theCurrentDate);
                        //                                mLCGetBLSet.get(j).setModifyTime(theCurrentTime);
                        //                            }
                        //                            for (int j = 1; j <= mLCDutyBLSet.size(); j++) {
                        //                                mLCDutyBLSet.get(j).setOperator(mGlobalInput.
                        //                                        Operator);
                        //                                mLCDutyBLSet.get(j).setMakeDate(theCurrentDate);
                        //                                mLCDutyBLSet.get(j).setMakeTime(theCurrentTime);
                        //                                mLCDutyBLSet.get(j).setModifyDate(theCurrentDate);
                        //                                mLCDutyBLSet.get(j).setModifyTime(theCurrentTime);
                        //                            }
                        //                            mmLCDutySet= new LCDutySet();
                        //                            mmLCDutySet.set(mLCDutyBLSet);
                        //                            mmLCGetSet = new LCGetSet();
                        //                            mmLCGetSet.set(mLCGetBLSet);
                        //                            mmLCPremSet = new LCPremSet();
                        //                            mmLCPremSet.set(mLCPremBLSet);
                        //
                        //                            dLCPolSchema.setPrem(mLCPolBL.getPrem());
                        //                            dLCPolSchema.setStandPrem(mLCPolBL.getStandPrem());
                        //                            dLCPolSchema.setAmnt(mLCPolBL.getAmnt());
                        //                            dLCPolSchema.setMult(mLCPolBL.getMult());
                        //                            dLCPolSchema.setPayEndDate(mLCPolBL.getPayEndDate());
                        //                            dLCPolSchema.setGetStartDate(mLCPolBL.getGetStartDate());
                        //                            dLCPolSchema.setEndDate(mLCPolBL.getEndDate());
                        //                            map.put(mmLCDutySet,"UPDATE");
                        //                            map.put(mmLCGetSet,"UPDATE");
                        //                            System.out.println("mmLCGetSet.size is "+mmLCGetSet.size());
                        //                            map.put(mmLCPremSet,"UPDATE");
                        //                            map.put(dLCPolSchema,"UPDATE");

                        LCPolSchema tLCPolSchema = dLCPolSet.get(i).getSchema();
                        tLCPolSchema.setCValiDate(mLCContSchema.getCValiDate());
                        

                        //循环调用RecalculationPremBL类，重新计算保费

                        RecalculationPremBL aRecalculationPremBL = new RecalculationPremBL();
                        VData aVData = new VData();
                        aVData.add(mGlobalInput);
                        aVData.add(tLCPolSchema);
                        System.out.println(tLCPolSchema.getPolNo());
                        boolean tResult = aRecalculationPremBL.submitData(
                                aVData, "");
                        System.out.println("after!tResult===" + tResult);
                        MMap tMap = null;
                        if (tResult)
                        {
                            tMap = (MMap) aRecalculationPremBL.getResult()
                                    .getObjectByObjectName("MMap", 0);
                        }
                        if (mMap != null)
                        {
                            this.mMap.add(tMap);
                        }

                    }
                    changecvalidateflag = true;
                    System.out.println("结束计算保费");
                }
            }
            else
            {
                System.out.println("生效日期和有险种信息的投保人不能同时修改！");
                CError tError = new CError();
                tError.moduleName = "ContBL.java";
                tError.functionName = "dealInsured";
                tError.errorMessage = "生效日期和有险种信息的投保人信息不能同时修改！";
                this.mErrors.addOneError(tError);
                return false;
            }

        }
        else
        { //生效日期没有修改过
            System.out.println("查询被保险人，如果被保险人和投保人是同一人更新投保人时也更新被保人");
            if (tLCInsuredSet.size() <= 0)
            {
                System.out
                        .println("ContBL.dealData()  \n--Line:476  --Author:YangMing");
                System.out.println("被保险人与投保人不是同一个人");
                mLCInsuredSchema = null;
                return true;
            }
            else
            {
                if (tLCInsuredSet.size() > 1)
                {
                    System.out
                            .println("程序第731行出错，请检查ContBL.java中的dealInsured方法！");
                    CError tError = new CError();
                    tError.moduleName = "ContBL.java";
                    tError.functionName = "dealInsured";
                    tError.errorMessage = "系统中存在多个与投保人是本人的被保人！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                mLCInsuredSchema = tLCInsuredSet.get(1);
                /** 更新客户号 */
                String old_Insuredno = mLCInsuredSchema.getInsuredNo();
                String new_Insuredno = mLCAppntBL.getAppntNo();
                //            mLCInsuredSchema.setInsuredNo(mLCAppntBL.getAppntNo());
                String AccName = mLCInsuredSchema.getAccName();
                String BankCode = mLCInsuredSchema.getBankCode();
                String BankAccCode = mLCInsuredSchema.getBankAccNo();

                Reflections ref = new Reflections();
                ref.transFields(mLCInsuredSchema, mLCAppntBL.getSchema());
                mLCInsuredSchema.setBirthday(mLCAppntBL.getAppntBirthday());
                mLCInsuredSchema.setSex(mLCAppntBL.getAppntSex());
                mLCInsuredSchema.setName(mLCAppntBL.getAppntName());
                /**
                 * 缴费帐户不放入理赔账户
                 */
                mLCInsuredSchema.setAccName(AccName);
                mLCInsuredSchema.setBankAccNo(BankAccCode);
                mLCInsuredSchema.setBankCode(BankCode);
                //                System.out.println("测试　"+mLCInsuredSchema.encode());
                /**
                 * 准备保费重算
                 */
                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setContNo(mLCContSchema.getContNo());
                tLCPolDB.setInsuredNo(mLCInsuredSchema.getInsuredNo());
                LCPolSet tLCPolSet = tLCPolDB.query();
                if (tLCPolSet != null)
                {
                    if (tLCPolSet.size() > 0)
                    {
                        ReCalInsuredBL tReCalInsuredBl = new ReCalInsuredBL(
                                mLCInsuredSchema, tLCPolSet, mLCContSchema,
                                mGlobalInput, new_Insuredno);
                        if (!tReCalInsuredBl.reCalInsured())
                        {
                            this.mErrors.copyAllErrors(tReCalInsuredBl.mErrors);
                            return false;
                        }
                        VData tInputData = tReCalInsuredBl.getResult();
                        MMap tMap = (MMap) tInputData.getObjectByObjectName(
                                "MMap", 0);
                        if (tMap != null)
                        {
                            this.mMap.add(tMap);
                        }
                    }
                }
                /** 上述操作完成了需要将被保险人号码换掉 */
                if (!old_Insuredno.equals(new_Insuredno))
                {
                    if (!changInsuredNo(old_Insuredno, new_Insuredno))
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * changInsuredNo
     *
     * @return boolean
     * @param old_Insuredno String
     * @param new_Insuredno String
     */
    private boolean changInsuredNo(String old_Insuredno, String new_Insuredno)
    {
        String[] tableName = { "LCCUWError", "LCCUWMaster", "LCCUWSub",
                "LCGet", "LCIndUWError", "LCIndUWMaster", "LCIndUWSub",
                "LCInsured", "LCUWError", "LCUWMaster", "LCUWSub", "LCBnf" };
        String condition = "Insuredno='" + new_Insuredno + "'";
        String wherepart = " Contno='" + this.mLCContSchema.getContNo()
                + "' and Insuredno='" + old_Insuredno + "'";
        Vector VecContPol = PubFun.formUpdateSql(tableName, condition,
                wherepart);
        String[] lcPol = { "LCPol" };
        String lcpol_condition = "Insuredno='" + new_Insuredno
                + "',InsuredName='" + mLCInsuredSchema.getName()
                + "',InsuredBirthday='" + mLCInsuredSchema.getBirthday()
                + "',InsuredSex='" + mLCInsuredSchema.getSex() + "'";
        Vector lcPol_sql = PubFun.formUpdateSql(lcPol, lcpol_condition,
                wherepart);
        insured_map = new MMap();
        for (int i = 0; i < VecContPol.size(); i++)
        {
            this.insured_map.put((String) VecContPol.get(i), "UPDATE");
        }
        for (int i = 0; i < lcPol_sql.size(); i++)
        {
            this.insured_map.put((String) lcPol_sql.get(i), "UPDATE");
        }
        
        //将客户身份证号码中的x转换成大写（被保险人） 2009-02-13 liuyp
        if (mLCInsuredSchema.getIDType() != null && mLCInsuredSchema.getIDNo() != null) {
        	if (mLCInsuredSchema.getIDType().equals("0")) {
                String tLCInsuredIdNo = mLCInsuredSchema.getIDNo().toUpperCase();
                mLCInsuredSchema.setIDNo(tLCInsuredIdNo);
            }
        }
        
        String sql = "update lccont set " + "Insuredno='" + new_Insuredno
                + "',InsuredName='" + mLCInsuredSchema.getName()
                + "',InsuredBirthday='" + mLCInsuredSchema.getBirthday()
                + "',InsuredSex='" + mLCInsuredSchema.getSex()
                + "',InsuredIDType='" + mLCInsuredSchema.getIDType()
                + "',InsuredIDNo='" + mLCInsuredSchema.getIDNo()
                + "' where contno ='" + mLCInsuredSchema.getContNo() + "'";
        this.insured_map.put(sql, "UPDATE");
        String tsql = "update lccustomerimpart set customerno='"
                + new_Insuredno
                + "' where grpcontno='00000000000000000000' and proposalcontno='"
                + mLCContSchema.getProposalContNo() + "' and customerno='"
                + old_Insuredno + "'";
        String tsql2 = "update lccustomerimpartdetail set customerno='"
                + new_Insuredno
                + "' where grpcontno='00000000000000000000' and proposalcontno='"
                + mLCContSchema.getProposalContNo() + "' and customerno='"
                + old_Insuredno + "'";
        String tsql3 = "update lccustomerimpartparams set customerno='"
                + new_Insuredno
                + "' where grpcontno='00000000000000000000' and proposalcontno='"
                + mLCContSchema.getProposalContNo() + "' and customerno='"
                + old_Insuredno + "'";
        //更新体检，契调表中的客户信息
        insured_map.put("update LCPENoticeResult set customerno='"
                + new_Insuredno + "',name='" + mLCInsuredSchema.getName()
                + "'  where contno='" + mLCContSchema.getContNo()
                + "' and customerno='" + old_Insuredno + "'", "UPDATE");
        insured_map.put("update LCRReport set customerno='" + new_Insuredno
                + "',name='" + mLCInsuredSchema.getName() + "'  where contno='"
                + mLCContSchema.getContNo() + "' and customerno='"
                + old_Insuredno + "'", "UPDATE");
        insured_map.put("update LCPENotice set customerno='" + new_Insuredno
                + "',name='" + mLCInsuredSchema.getName() + "'  where contno='"
                + mLCContSchema.getContNo() + "' and customerno='"
                + old_Insuredno + "'", "UPDATE");

        this.insured_map.put(tsql, "UPDATE");
        this.insured_map.put(tsql2, "UPDATE");
        this.insured_map.put(tsql3, "UPDATE");
        return true;
    }

    /**
     * 根据业务逻辑对数据进行处理
     *
     */
    private void prepareOutputData()
    {

        map.add(this.mMap);
        if (insured_map != null)
        {
            if (insured_map.size() > 0)
            {
                map.add(insured_map);
            }
        }

        if (mStrLcpolup != null && !"".equals(mStrLcpolup))
            map.put(mStrLcpolup + " and 1 = 1", "UPDATE");
        if (mStrLcpolPaymodeup != null && !"".equals(mStrLcpolPaymodeup))
            map.put(mStrLcpolPaymodeup + " and 1 = 1", "UPDATE");
        
        mInputData.clear();
        mInputData.add(map);
        mResult.clear();
        mResult.add(mLCInsuredSchema);
        mResult.add(mLCAddressSchema);
        mResult.add(mLCContSchema);
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 校验个人投保单的客户
     *
     * @return boolean
     */
    private boolean checkLDPerson()
    {
        //如果是无名单或者公共帐户的个人，不校验返回
        if (mLCContSchema.getPolType().equals("1")
                || mLCContSchema.getPolType().equals("2"))
        {
            return true;
        }

        // 投保人-个人客户--如果是集体下个人，该投保人为空,所以个人时校验个人投保人
        if (mLCContSchema.getContType().equals("1"))
        {
            if (mLDPersonSchema != null)
            {
                if (mLDPersonSchema.getName() != null)
                { //去空格
                    mLDPersonSchema.setName(mLDPersonSchema.getName().trim());
                    if (mLDPersonSchema.getSex() != null)
                    {
                        mLDPersonSchema.setSex(mLDPersonSchema.getSex().trim());
                    }
                    if (mLDPersonSchema.getIDNo() != null)
                    {
                        mLDPersonSchema.setIDNo(mLDPersonSchema.getIDNo()
                                .trim());
                    }
                    if (mLDPersonSchema.getIDType() != null)
                    {
                        mLDPersonSchema.setIDType(mLDPersonSchema.getIDType()
                                .trim());
                    }
                    if (mLDPersonSchema.getBirthday() != null)
                    {
                        mLDPersonSchema.setBirthday(mLDPersonSchema
                                .getBirthday().trim());
                    }

                }
                if (mLDPersonSchema.getCustomerNo() != null)
                {
                    //如果有客户号
                    if (!mLDPersonSchema.getCustomerNo().equals(""))
                    {
                        LDPersonDB tLDPersonDB = new LDPersonDB();

                        tLDPersonDB.setCustomerNo(mLDPersonSchema
                                .getCustomerNo());
                        if (tLDPersonDB.getInfo() == false)
                        {
                            CError tError = new CError();
                            tError.moduleName = "ContBL";
                            tError.functionName = "checkPerson";
                            tError.errorMessage = "数据库查询失败!";
                            this.mErrors.addOneError(tError);

                            return false;
                        }
                        if (mLDPersonSchema.getName() != null)
                        {
                            String Name = StrTool.GBKToUnicode(tLDPersonDB
                                    .getName().trim());
                            String NewName = StrTool
                                    .GBKToUnicode(mLDPersonSchema.getName()
                                            .trim());

                            if (!Name.equals(NewName))
                            {
                                CError tError = new CError();
                                tError.moduleName = "ProposalBL";
                                tError.functionName = "checkPerson";
                                tError.errorMessage = "您输入的投保人客户号对应在数据库中的客户姓名("
                                        + Name + ")与您录入的客户姓名(" + NewName
                                        + ")不匹配！";
                                this.mErrors.addOneError(tError);

                                return false;
                            }
                        }
                        if (mLDPersonSchema.getSex() != null)
                        {
                            if (!tLDPersonDB.getSex().equals(
                                    mLDPersonSchema.getSex()))
                            {
                                CError tError = new CError();
                                tError.moduleName = "ContBL";
                                tError.functionName = "checkPerson";
                                tError.errorMessage = "您输入的投保人客户号对应在数据库中的客户性别("
                                        + tLDPersonDB.getSex() + ")与您录入的客户性别("
                                        + mLDPersonSchema.getSex() + ")不匹配！";
                                this.mErrors.addOneError(tError);

                                return false;
                            }
                        }
                    }
                    else
                    { //如果没有客户号,查找客户信息表是否有相同名字，性别，出生年月，身份证号的纪录，若有，取客户号
                        if ((mLDPersonSchema.getName() != null)
                                && (mLDPersonSchema.getSex() != null)
                                && (mLDPersonSchema.getIDNo() != null))
                        {
                            LDPersonDB tLDPersonDB = new LDPersonDB();
                        	LDPersonSet tLDPersonSet=new LDPersonSet();
                        	tLDPersonDB.setName(mLDPersonSchema.getName());
                        	tLDPersonDB.setIDNo(mLDPersonSchema.getIDNo());
                        	tLDPersonDB.setIDType(mLDPersonSchema.getIDType());
                        	if(tLDPersonDB.getIDType().equals("0")){
                        		tLDPersonSet = tLDPersonDB.query();
                        	}else if(tLDPersonDB.getIDType().equals("a")||tLDPersonDB.getIDType().equals("b")){
                           	 tLDPersonDB.setIDNo(mLDPersonSchema.getOriginalIDNo());
                             tLDPersonDB.setIDType("7");
                             tLDPersonDB.setName(mLDPersonSchema.getName());
                             tLDPersonDB.setSex(mLDPersonSchema.getSex());
                             tLDPersonDB.setBirthday(mLDPersonSchema.getBirthday());
                             tLDPersonSet = tLDPersonDB.query();
                             if(tLDPersonSet.size()<=0){
                            	 LDPersonDB mLDPersonDB = new LDPersonDB();
                            	 mLDPersonDB.setName(mLDPersonSchema.getName());
                            	 mLDPersonDB.setSex(mLDPersonSchema.getSex());
                            	 mLDPersonDB.setBirthday(mLDPersonSchema.getBirthday());
                            	 mLDPersonDB.setIDType(mLDPersonSchema.getIDType());
                            	 mLDPersonDB.setIDNo(mLDPersonSchema.getIDNo());
                                 tLDPersonSet = mLDPersonDB.query();
                             }
                        }else if(tLDPersonDB.getIDType().equals("7")){
                       	 LDPersonDB cLDPersonDB = new LDPersonDB();   
                       	 cLDPersonDB.setIDNo(mLDPersonSchema.getIDNo());
                       	 cLDPersonDB.setIDType("7");
                       	 cLDPersonDB.setName(mLDPersonSchema.getName());
                       	 cLDPersonDB.setSex(mLDPersonSchema.getSex());
                       	 cLDPersonDB.setBirthday(mLDPersonSchema.getBirthday());
                         tLDPersonSet = cLDPersonDB.query();
                         if(tLDPersonSet.size()<=0){
                        	 LDPersonDB mLDPersonDB = new LDPersonDB();
                        	 mLDPersonDB.setName(mLDPersonSchema.getName());
                        	 mLDPersonDB.setSex(mLDPersonSchema.getSex());
                        	 mLDPersonDB.setBirthday(mLDPersonSchema
                                     .getBirthday());
                        	 mLDPersonDB.setOriginalIDNo(mLDPersonSchema.getIDNo());
                             tLDPersonSet = mLDPersonDB.query();
                         }
                    }else{
                        		tLDPersonDB.setSex(mLDPersonSchema.getSex());
                            	tLDPersonDB.setBirthday(mLDPersonSchema.getBirthday());
                            	tLDPersonSet = tLDPersonDB.query();
                        	}
                        	if (tLDPersonSet != null)
                            {
                                if (tLDPersonSet.size() > 0)
                                {
                                    mLDPersonSchema.setCustomerNo(tLDPersonSet
                                            .get(1).getCustomerNo());
                                }
                            }
                        }
                    }
                }

            }
        }
        return true;
    }

    /**
     * 检查投保人数据是否正确 如果在处理过程中出错或者数据有错误，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean checkLCAppnt()
    {
        //如果是无名单或者公共帐户的个人，不校验返回
        if (mLCContSchema.getPolType().equals("1")
                || mLCContSchema.getPolType().equals("2"))
        {
            return true;
        }

        // 投保人-个人客户--如果是集体下个人，该投保人为空,所以个人时校验个人投保人
        if (mLCContSchema.getContType().equals("1"))
        {
            if (mLCAppntBL != null)
            {
                if (mLCAppntBL.getAppntName() != null)
                { //去空格
                    mLCAppntBL.setAppntName(mLCAppntBL.getAppntName().trim());
                }
                if (mLCAppntBL.getAppntSex() != null)
                {
                    mLCAppntBL.setAppntSex(mLCAppntBL.getAppntSex().trim());
                }
                if (mLCAppntBL.getIDNo() != null)
                {
                    mLCAppntBL.setIDNo(mLCAppntBL.getIDNo().trim());
                }
                if (mLCAppntBL.getIDType() != null)
                {
                    mLCAppntBL.setIDType(mLCAppntBL.getIDType().trim());
                }
                if (mLCAppntBL.getAppntBirthday() != null)
                {
                    mLCAppntBL.setAppntBirthday(mLCAppntBL.getAppntBirthday()
                            .trim());
                }

                if (mLCAppntBL.getAppntNo() != null)
                {
                    //如果有客户号
                    if (!mLCAppntBL.getAppntNo().equals(""))
                    {
                        LDPersonDB tLDPersonDB = new LDPersonDB();

                        tLDPersonDB.setCustomerNo(mLCAppntBL.getAppntNo());
                        if (tLDPersonDB.getInfo() == false)
                        {
                            CError tError = new CError();
                            tError.moduleName = "ProposalBL";
                            tError.functionName = "checkPerson";
                            tError.errorMessage = "数据库查询失败!";
                            this.mErrors.addOneError(tError);

                            return false;
                        }
                        if (mLCAppntBL.getAppntName() != null)
                        {
                            String Name = StrTool.GBKToUnicode(tLDPersonDB
                                    .getName().trim());
                            String NewName = StrTool.GBKToUnicode(mLCAppntBL
                                    .getAppntName().trim());

                            if (!Name.equals(NewName))
                            {
                                CError tError = new CError();
                                tError.moduleName = "ProposalBL";
                                tError.functionName = "checkPerson";
                                tError.errorMessage = "您输入的投保人客户号对应在数据库中的客户姓名("
                                        + Name + ")与您录入的客户姓名(" + NewName
                                        + ")不匹配！";
                                this.mErrors.addOneError(tError);

                                return false;
                            }
                        }
                        if (mLCAppntBL.getAppntSex() != null)
                        {
                            if (!tLDPersonDB.getSex().equals(
                                    mLCAppntBL.getAppntSex()))
                            {
                                CError tError = new CError();
                                tError.moduleName = "ProposalBL";
                                tError.functionName = "checkPerson";
                                tError.errorMessage = "您输入的投保人客户号对应在数据库中的客户性别("
                                        + tLDPersonDB.getSex() + ")与您录入的客户性别("
                                        + mLCAppntBL.getAppntSex() + ")不匹配！";
                                this.mErrors.addOneError(tError);

                                return false;
                            }
                        }
                        if (mLCAppntBL.getAppntBirthday() != null)
                        {
                            if (!tLDPersonDB.getBirthday().equals(
                                    mLCAppntBL.getAppntBirthday()))
                            {
                                CError tError = new CError();
                                tError.moduleName = "ProposalBL";
                                tError.functionName = "checkPerson";
                                tError.errorMessage = "您输入的投保人客户号对应在数据库中的客户生日("
                                        + tLDPersonDB.getBirthday()
                                        + ")与您录入的客户生日("
                                        + mLCAppntBL.getAppntBirthday()
                                        + ")不匹配！";
                                this.mErrors.addOneError(tError);

                                return false;
                            }
                        }
                        if (mLCAppntBL.getIDNo() != null
                        //&&!StrTool.cTrim(mLCAppntBL.getIDType()).equals("4")
                        )
                        {
                            if (!StrTool.cTrim(tLDPersonDB.getIDNo()).equals(
                                    StrTool.cTrim(mLCAppntBL.getIDNo())))
                            {
                            	if(!StrTool.cTrim(tLDPersonDB.getOriginalIDNo()).equals(
                                        StrTool.cTrim(mLCAppntBL.getIDNo()))){
                                CError tError = new CError();
                                tError.moduleName = "ProposalBL";
                                tError.functionName = "checkPerson";
                                tError.errorMessage = "您输入的投保人客户号对应在数据库中的客户证件号码("
                                        + tLDPersonDB.getIDNo()
                                        + ")与您录入的客户证件号码("
                                        + mLCAppntBL.getIDNo() + ")不匹配！";
                                this.mErrors.addOneError(tError);

                                return false;
                            	}
                            }
                        }
                        //校验证件类型
                        if (mLCAppntBL.getIDType() != null)
                        {
                            if (!StrTool.cTrim(tLDPersonDB.getIDType()).equals(
                                    StrTool.cTrim(mLCAppntBL.getIDType())))
                            {
                            	if ((tLDPersonDB.getIDType().equals("a")||tLDPersonDB.getIDType().equals("b"))&&mLCAppntBL.getIDType().equals("7")){
                            	}else{
                                CError tError = new CError();
                                tError.moduleName = "ProposalBL";
                                tError.functionName = "checkPerson";
                                tError.errorMessage = "您输入的投保人客户号对应在数据库中的客户证件类型("
                                        + tLDPersonDB.getIDType()
                                        + ")与您录入的客户证件类型("
                                        + mLCAppntBL.getIDType() + ")不匹配！";
                                this.mErrors.addOneError(tError);

                                return false;
                            	}
                            }
                        }

                    }
                    else
                    { //如果没有客户号,查找客户信息表是否有相同名字，性别，出生年月，身份证号的纪录，若有，取客户号
                    	if ("".equals(mLCAppntBL.getAppntName())
								|| "null".equals(mLCAppntBL.getAppntName())) {
							CError tError = new CError();
							tError.moduleName = "ContBL";
							tError.functionName = "checkLCAppnt";
							tError.errorMessage = "投保人姓名不能为空";
							this.mErrors.addOneError(tError);
							return false;
						}
						if ("".equals(mLCAppntBL.getAppntSex())
								|| "null".equals(mLCAppntBL.getAppntSex())) {
							CError tError = new CError();
							tError.moduleName = "ContBL";
							tError.functionName = "checkLCAppnt";
							tError.errorMessage = "投保人性别不能为空";
							this.mErrors.addOneError(tError);
							return false;
						}
						if ("".equals(mLCAppntBL.getIDNo())
								|| "null".equals(mLCAppntBL.getIDNo())) {
							CError tError = new CError();
							tError.moduleName = "ContBL";
							tError.functionName = "checkLCAppnt";
							tError.errorMessage = "投保人证件号码不能为空";
							this.mErrors.addOneError(tError);
							return false;
						}
						if ("".equals(mLCAppntBL.getIDType())
								|| "null".equals(mLCAppntBL.getIDType())) {
							CError tError = new CError();
							tError.moduleName = "ContBL";
							tError.functionName = "checkLCAppnt";
							tError.errorMessage = "投保人证件类型不能为空";
							this.mErrors.addOneError(tError);
							return false;
						}
						
						if( !("0".equals(mLCAppntBL.getIDType()))) {
							if("".equals(mLCAppntBL.getAppntBirthday())
									|| "null".equals(mLCAppntBL.getAppntBirthday())){
							CError tError = new CError();
							tError.moduleName = "ContBL";
							tError.functionName = "checkLCAppnt";
							tError.errorMessage = "投保人出生日期不能为空";
							this.mErrors.addOneError(tError);
							return false;
						}}
                    	if ((mLCAppntBL.getAppntName() != null)
                                && (mLCAppntBL.getAppntSex() != null)
                                && (mLCAppntBL.getIDNo() != null))
                        {
                            LDPersonDB tLDPersonDB = new LDPersonDB();
                            LDPersonSet tLDPersonSet = new LDPersonSet();
                            if(mLCAppntBL.getIDType().equals("0")){
                            	tLDPersonDB.setName(mLCAppntBL.getAppntName());
                                tLDPersonDB.setIDType(mLCAppntBL.getIDType());
                                tLDPersonDB.setIDNo(mLCAppntBL.getIDNo());
                                tLDPersonSet = tLDPersonDB.query();
                                //zxs
                            }else if(mLCAppntBL.getIDType().equals("a")||mLCAppntBL.getIDType().equals("b")){
                            	 tLDPersonDB.setIDNo(mLCAppntBL.getOriginalIDNo());
                                 tLDPersonDB.setIDType("7");
                                 tLDPersonDB.setName(mLCAppntBL.getAppntName());
                                 tLDPersonDB.setSex(mLCAppntBL.getAppntSex());
                                 tLDPersonDB.setBirthday(mLCAppntBL.getAppntBirthday());
                                 tLDPersonSet = tLDPersonDB.query();
                                 if(tLDPersonSet.size()<=0){
                                	 LDPersonDB mLDPersonDB = new LDPersonDB();
                                	 mLDPersonDB.setName(mLCAppntBL.getAppntName());
                                	 mLDPersonDB.setSex(mLCAppntBL.getAppntSex());
                                	 mLDPersonDB.setBirthday(mLCAppntBL
                                             .getAppntBirthday());
                                	 mLDPersonDB.setIDType(mLCAppntBL.getIDType());
                                	 mLDPersonDB.setIDNo(mLCAppntBL.getIDNo());
                                     tLDPersonSet = mLDPersonDB.query();
                                 }
                            }else if(mLCAppntBL.getIDType().equals("7")){
                            	 LDPersonDB cLDPersonDB = new LDPersonDB();   
                            	 cLDPersonDB.setIDNo(mLCAppntBL.getIDNo());
                            	 cLDPersonDB.setIDType("7");
                            	 cLDPersonDB.setName(mLCAppntBL.getAppntName());
                            	 cLDPersonDB.setSex(mLCAppntBL.getAppntSex());
                            	 cLDPersonDB.setBirthday(mLCAppntBL.getAppntBirthday());
                                 tLDPersonSet = cLDPersonDB.query();
                                 if(tLDPersonSet.size()<=0){
                                	 LDPersonDB mLDPersonDB = new LDPersonDB();
                                	 mLDPersonDB.setName(mLCAppntBL.getAppntName());
                                	 mLDPersonDB.setSex(mLCAppntBL.getAppntSex());
                                	 mLDPersonDB.setBirthday(mLCAppntBL
                                             .getAppntBirthday());
                                	 mLDPersonDB.setOriginalIDNo(mLCAppntBL.getIDNo());
                                     tLDPersonSet = mLDPersonDB.query();
                                 }
                            }else{
                            	 tLDPersonDB.setName(mLCAppntBL.getAppntName());
                                 tLDPersonDB.setSex(mLCAppntBL.getAppntSex());
                                 tLDPersonDB.setBirthday(mLCAppntBL
                                         .getAppntBirthday());
                                 tLDPersonDB.setIDType(mLCAppntBL.getIDType());
                                 tLDPersonDB.setIDNo(mLCAppntBL.getIDNo());
                                 tLDPersonSet = tLDPersonDB.query();
                            }
                            
                            if (tLDPersonSet != null)
                            {
                                if (tLDPersonSet.size() > 0)
                                {
                                    mLCAppntBL.setAppntNo(tLDPersonSet.get(1)
                                            .getCustomerNo());
                                }
                            }
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * 检查地址数据是否正确 如果在处理过程中出错或者数据有错误，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean checkLCAddress()
    {

        if (mLCAddressSchema != null)
        {
            if (mLCAddressSchema.getPostalAddress() != null)
            { //去空格
                mLCAddressSchema.setPostalAddress(mLCAddressSchema
                        .getPostalAddress().trim());
                if (mLCAddressSchema.getZipCode() != null)
                {
                    mLCAddressSchema.setZipCode(mLCAddressSchema.getZipCode()
                            .trim());
                }
                if (mLCAddressSchema.getPhone() != null)
                {
                    mLCAddressSchema.setPhone(mLCAddressSchema.getPhone()
                            .trim());
                }

            }
            if (mLCAddressSchema.getAddressNo() != null)
            {
                //如果有地址号
                if (!mLCAddressSchema.getAddressNo().equals(""))
                {
                    LCAddressDB tLCAddressDB = new LCAddressDB();

                    tLCAddressDB.setAddressNo(mLCAddressSchema.getAddressNo());
                    tLCAddressDB
                            .setCustomerNo(mLCAddressSchema.getCustomerNo());
                    if (tLCAddressDB.getInfo() == false)
                    {
                        CError tError = new CError();
                        tError.moduleName = "ContBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "数据库查询失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    if (!StrTool.compareString(
                            mLCAddressSchema.getCustomerNo(), tLCAddressDB
                                    .getCustomerNo())
                            || !StrTool.compareString(mLCAddressSchema
                                    .getAddressNo(), tLCAddressDB
                                    .getAddressNo())
                            || !StrTool.compareString(mLCAddressSchema
                                    .getPostalAddress(), tLCAddressDB
                                    .getPostalAddress())
                            || !StrTool.compareString(mLCAddressSchema
                                    .getZipCode(), tLCAddressDB.getZipCode())
                            || !StrTool.compareString(mLCAddressSchema
                                    .getPhone(), tLCAddressDB.getPhone())
                            || !StrTool.compareString(
                                    mLCAddressSchema.getFax(), tLCAddressDB
                                            .getFax())
                            || !StrTool.compareString(mLCAddressSchema
                                    .getHomeAddress(), tLCAddressDB
                                    .getHomeAddress())
                            || !StrTool.compareString(mLCAddressSchema
                                    .getHomeZipCode(), tLCAddressDB
                                    .getHomeZipCode())
                            || !StrTool.compareString(mLCAddressSchema
                                    .getHomePhone(), tLCAddressDB
                                    .getHomePhone())
                            || !StrTool.compareString(mLCAddressSchema
                                    .getHomeFax(), tLCAddressDB.getHomeFax())
                            || !StrTool.compareString(mLCAddressSchema
                                    .getCompanyAddress(), tLCAddressDB
                                    .getCompanyAddress())
                            || !StrTool.compareString(mLCAddressSchema
                                    .getCompanyZipCode(), tLCAddressDB
                                    .getCompanyZipCode())
                            || !StrTool.compareString(mLCAddressSchema
                                    .getCompanyPhone(), tLCAddressDB
                                    .getCompanyPhone())
                            || !StrTool.compareString(mLCAddressSchema
                                    .getCompanyFax(), tLCAddressDB
                                    .getCompanyFax())
                            || !StrTool.compareString(mLCAddressSchema
                                    .getMobile(), tLCAddressDB.getMobile())
                            || !StrTool.compareString(mLCAddressSchema
                                    .getMobileChs(), tLCAddressDB
                                    .getMobileChs())
                            || !StrTool.compareString(mLCAddressSchema
                                    .getEMail(), tLCAddressDB.getEMail())
                            || !StrTool.compareString(mLCAddressSchema.getBP(),
                                    tLCAddressDB.getBP())
                            || !StrTool.compareString(mLCAddressSchema
                                    .getMobile2(), tLCAddressDB.getMobile2())
                            || !StrTool.compareString(mLCAddressSchema
                                    .getMobileChs2(), tLCAddressDB
                                    .getMobileChs2())
                            || !StrTool.compareString(mLCAddressSchema
                                    .getEMail2(), tLCAddressDB.getEMail2())
                            || !StrTool.compareString(
                                    mLCAddressSchema.getBP2(), tLCAddressDB
                                            .getBP2()))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ContInsuredBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "您输入的地址信息与数据库里对应的信息不符，请去掉地址号，重新生成!";
                        this.mErrors.addOneError(tError);

                        return false;

                    }

                }
            }

        }

        return true;
    }
    
    /**
     * 校验录入的证件类型是否正确，如果错误，则返回false,否则返回true
     * 
     * 根据传入的type字段判断是投保人还是被保险人
     *
     * by zhangyang   2011-02-23
     * @return boolean
     */
    private boolean checkIDType(String IDType, String type, String name)
    {
        String strSQL = "select CodeName from ldcode where codetype = 'idtype' and code = '" 
                        + IDType + "'";
        ExeSQL tExeSQL = new ExeSQL();
        String strCount = tExeSQL.getOneValue(strSQL);
        
        if(strCount != null && !strCount.equals(""))
        {
            return true;
        }
        else
        {
            if(type.equals("appnt"))
            {
                CError tError = new CError();
                tError.moduleName = "ContBL";
                tError.functionName = "checkIDType";
                tError.errorMessage = "您输入的投保人[" + StrTool.unicodeToGBK(name) + "]的证件类型[" 
                                    + IDType + "]有误，不符合系统的描述规范，请进行修改！";
                this.mErrors.addOneError(tError);
            }
            
            if(type.equals("insured"))
            {
                CError tError = new CError();
                tError.moduleName = "ContBL";
                tError.functionName = "checkIDType";
                tError.errorMessage = "您输入的被保险人[" + name + "]的证件类型[" 
                                    + IDType + "]有误，不符合系统的描述规范，请进行修改！";
                this.mErrors.addOneError(tError);
            }
            
            return false;
        }
    }

    /**
     * 处理综合开拓数据。
     * @param cGrpContNo
     * @return
     */
    private MMap delExtendInfo(LCExtendSchema aLCExtendSchema)
    {
    	if(aLCExtendSchema == null){
    		aLCExtendSchema = new LCExtendSchema();
    		aLCExtendSchema.setPrtNo(mLCContSchema.getPrtNo());
    	}
        MMap tMMap = null;

        LCAssistSalechnlBL tAssistSalechnlBL = new LCAssistSalechnlBL();

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(aLCExtendSchema);

        tMMap = tAssistSalechnlBL.submitData(tVData, "");

        if (tMMap == null)
        {
            CError tError = new CError();
            tError.moduleName = "ContBL";
            tError.functionName = "delExtendInfo";
            tError.errorMessage = "综合开拓数据处理失败！";
            this.mErrors.addOneError(tError);
            return null;
        }

        return tMMap;
    }
    
    /**
     * 处理综合开拓数据。
     * @param cGrpContNo
     * @return
     */
    private MMap deleteExtendInfo(LCExtendSchema aLCExtendSchema)
    {
    	if(aLCExtendSchema == null){
    		aLCExtendSchema = new LCExtendSchema();
    		aLCExtendSchema.setPrtNo(mLCContSchema.getPrtNo());
    	}
        MMap tMMap = null;

        LCAssistSalechnlBL tAssistSalechnlBL = new LCAssistSalechnlBL();

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(aLCExtendSchema);

        tMMap = tAssistSalechnlBL.submitData(tVData, "DELETE");

        return tMMap;
    }
    
    private boolean dealContSub()
    {
    	if(!"b".equals(mLCContSchema.getCardFlag())&&!"0".equals(mLCContSchema.getCardFlag())&&!"8".equals(mLCContSchema.getCardFlag())&&mLCContSchema.getCardFlag()!=null){
    		return true;
    	}
    	if("b".equals(mLCContSchema.getCardFlag())&&mLCContSubSchema==null){
    		return true;
    	}
    	LSInsuredListSchema tLSInsuredListSchema = new LSInsuredListSchema();
    	LSInsuredListDB tLSInsuredListDB = new LSInsuredListDB();
    	LSInsuredListSet tLSInsuredListSet = new LSInsuredListSet();
    	LCContSubSchema tOldLCContSubSchema = new LCContSubSchema();
    	System.out.println("managecom:"+mLCContSchema.getManageCom());
    	mLCContSubSchema.setManageCom(mLCContSchema.getManageCom());

    	LCContSubDB tLCContSubDB = new LCContSubDB();
    	tLCContSubDB.setPrtNo(mLCContSubSchema.getPrtNo());
    	boolean exists = tLCContSubDB.getInfo();
    	if(exists){
    		tOldLCContSubSchema = tLCContSubDB.getSchema();
    	}
    	
    	if (mOperate.equals("DELETE||CONT"))
        {

            map.put("delete from lccontsub where prtno='"
                    + mLCContSubSchema.getPrtNo() + "'", "DELETE");
            if(exists){
            	String taxflag = tOldLCContSubSchema.getTaxFlag();
            	if("b".equals(mLCContSchema.getCardFlag())){
            		
            	}else{
            		if("2".equals(taxflag)){
                		tLSInsuredListDB.setBatchNo(tOldLCContSubSchema.getBatchNo());
                		tLSInsuredListDB.setInsuredId(tOldLCContSubSchema.getInsuredId());
                		if(tLSInsuredListDB.getInfo()){
                			tLSInsuredListDB.setPrtNo("");
                			tLSInsuredListDB.setModifyDate(theCurrentDate);
                			tLSInsuredListDB.setModifyTime(theCurrentTime);
                			tLSInsuredListDB.setOperator(mGlobalInput.Operator);
                			map.put(tLSInsuredListDB.getSchema(), "UPDATE");
               		    }
                	}  
            	}
            	         	
            }
            return true;
        }
    	
    	if("2".equals(mLCContSubSchema.getTaxFlag())){    	
    		if("b".equals(mLCContSchema.getCardFlag()) || "SH_WX".equals(mLCContSchema.getOperator()) && "0".equals(mLCContSchema.getCardFlag())){
    			
    		}else{
    			tLSInsuredListDB.setBatchNo(mLCContSubSchema.getBatchNo());
        		tLSInsuredListDB.setInsuredId(mLCContSubSchema.getInsuredId());
        		if(!tLSInsuredListDB.getInfo()){
        			 CError tError = new CError();
        	         tError.moduleName = "ContBL";
        	         tError.functionName = "delExtendInfo";
        	         tError.errorMessage = "查询团体税优被保人失败！";
        	         this.mErrors.addOneError(tError);
        	         return false;
        		}
        		    		
        		tLSInsuredListSchema = tLSInsuredListDB.getSchema();
        		
        		String Name = StrTool.GBKToUnicode(tLSInsuredListSchema.getName());
                String NewName = StrTool.GBKToUnicode(mLCContSchema.getAppntName().trim());
                
        		if(!Name.equals(NewName)
        				||!tLSInsuredListSchema.getSex().equals(mLCContSchema.getAppntSex())
        				||!tLSInsuredListSchema.getBirthday().equals(mLCContSchema.getAppntBirthday())
        				||!tLSInsuredListSchema.getIdNo().equals(mLCContSchema.getAppntIDNo())
        				||!tLSInsuredListSchema.getIdType().equals(mLCContSchema.getAppntIDType())){
        			
        			 CError tError = new CError();
        	         tError.moduleName = "ContBL";
        	         tError.functionName = "delExtendInfo";
        	         tError.errorMessage = "该单投保人与导入的团体税优被保人不是同一人！";
        	         this.mErrors.addOneError(tError);
        	         return false;
        			
        		}
        		tLSInsuredListSchema.setPrtNo(mLCContSchema.getPrtNo());
        		tLSInsuredListSchema.setModifyDate(theCurrentDate);
        		tLSInsuredListSchema.setModifyTime(theCurrentTime);
        		map.put(tLSInsuredListSchema, "UPDATE");
    		}
    		
    		    		
    	}
    	
    	if(mOperate.equals("INSERT||CONT")){    		
    		mLCContSubSchema.setMakeDate(theCurrentDate);
    		mLCContSubSchema.setMakeTime(theCurrentTime);
    		mLCContSubSchema.setModifyDate(theCurrentDate);
    		mLCContSubSchema.setModifyTime(theCurrentTime);
    		mLCContSubSchema.setOperator(mGlobalInput.Operator);
    		map.put(mLCContSubSchema, "INSERT");
    	}
    	
    	if(mOperate.equals("UPDATE||CONT")){
    		if(exists){
    			if(!mOldLCCont.getAppntNo().equals(mLCContSchema.getAppntNo())){
    				mLCContSubSchema.setCheckFlag("99");
        			mLCContSubSchema.setCheckInfo("投保人变更，请重新验证！"); 				
        		}else{
        			mLCContSubSchema.setCheckFlag(tOldLCContSubSchema.getCheckFlag());
					mLCContSubSchema.setCheckInfo(tOldLCContSubSchema.getCheckInfo());
        		}
    			
        		mLCContSubSchema.setMakeDate(tOldLCContSubSchema.getMakeDate());
        		mLCContSubSchema.setMakeTime(tOldLCContSubSchema.getMakeTime());
        		mLCContSubSchema.setAgreedPayFlag(tOldLCContSubSchema.getAgreedPayFlag());
        		mLCContSubSchema.setAgreedPayDate(tOldLCContSubSchema.getAgreedPayDate());
        		mLCContSubSchema.setModifyDate(theCurrentDate);
        		mLCContSubSchema.setModifyTime(theCurrentTime);
        		mLCContSubSchema.setOperator(mGlobalInput.Operator);
        		map.put(mLCContSubSchema, "UPDATE");
        		if("b".equals(mLCContSchema.getCardFlag())){
        			
        		}else{
        			if("2".equals(tOldLCContSubSchema.getTaxFlag())){
            			if(!tOldLCContSubSchema.getBatchNo().equals(mLCContSubSchema.getBatchNo())&&!tOldLCContSubSchema.getInsuredId().equals(mLCContSubSchema.getInsuredId())){
            				tLSInsuredListDB.setBatchNo(tOldLCContSubSchema.getBatchNo());
                    		tLSInsuredListDB.setInsuredId(tOldLCContSubSchema.getInsuredId());
                    		if(tLSInsuredListDB.getInfo()){
                    			tLSInsuredListDB.setPrtNo("");
                    			tLSInsuredListDB.setModifyDate(theCurrentDate);
                    			tLSInsuredListDB.setModifyTime(theCurrentTime);
                    			tLSInsuredListDB.setOperator(mGlobalInput.Operator);
                    			map.put(tLSInsuredListDB.getSchema(), "UPDATE");
                   		    }
            			}
            		}
        		}
        		
        		
        	}else{ 
        		mLCContSubSchema.setMakeDate(theCurrentDate);
        		mLCContSubSchema.setMakeTime(theCurrentTime);
        		mLCContSubSchema.setModifyDate(theCurrentDate);
        		mLCContSubSchema.setModifyTime(theCurrentTime);
        		mLCContSubSchema.setOperator(mGlobalInput.Operator);
        		map.put(mLCContSubSchema, "INSERT");
        	}
        	
    	}
    			
        
        return true;
    }
}
