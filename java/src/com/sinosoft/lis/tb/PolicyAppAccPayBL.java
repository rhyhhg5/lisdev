/**
 * 2011-2-21
 */
package com.sinosoft.lis.tb;

import com.sinosoft.lis.bq.AppAcc;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAppAccTraceSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class PolicyAppAccPayBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    /** 保单合同号 */
    private String mContNo = null;

    /** 转出保费 */
    private double mAppAccMoney;

    /** 投保人帐户号 */
    private String mAppAccNo = null;

    public PolicyAppAccPayBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mContNo = (String) mTransferData.getValueByName("ContNo");
        if (mContNo == null)
        {
            buildError("getInputData", "取保单合同号失败。");
            return false;
        }

        mAppAccNo = (String) mTransferData.getValueByName("AppAccNo");
        if (mAppAccNo == null)
        {
            buildError("getInputData", "取转出金额失败。");
            return false;
        }

        String tAppAccMoney = (String) mTransferData.getValueByName("AppAccMoney");
        if (tAppAccMoney == null)
        {
            buildError("getInputData", "取转出金额失败。");
            return false;
        }
        try
        {
            mAppAccMoney = Double.parseDouble(tAppAccMoney);
        }
        catch (Exception e)
        {
            buildError("getInputData", "转出金额为非法数值。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("AppAccPay".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = pay4AppAcc();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        return true;
    }

    private MMap pay4AppAcc()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        // 获取保单信息
        LCContSchema tContInfo = loadContInfo(mContNo);
        if (tContInfo == null)
        {
            return null;
        }
        // --------------------

        // 从投保人帐户划款。
        tTmpMap = null;
        tTmpMap = dealAppAccPayMoney(tContInfo, mAppAccMoney);
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        // 处理暂收
        tTmpMap = null;
        tTmpMap = dealTempFeeMoney(tContInfo, mAppAccMoney);
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        return tMMap;
    }

    private LCContSchema loadContInfo(String cContNo)
    {
        LCContDB tContInfo = new LCContDB();
        tContInfo.setContNo(cContNo);
        if (!tContInfo.getInfo())
        {
            buildError("loadContInfo", "未找到该[" + cContNo + "]合同号下保单信息。");
            return null;
        }

        return tContInfo.getSchema();
    }

    private MMap dealAppAccPayMoney(LCContSchema cContInfo, double cMoney)
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        String tAppAccNo = cContInfo.getAppntNo();
        if (tAppAccNo != null && !tAppAccNo.equals(mAppAccNo))
        {
            buildError("dealAppAccPayMoney", "投保人帐户与保单不符，请核实。");
            return null;
        }

        // 生成投保人账户
        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
        tLCAppAccTraceSchema.setCustomerNo(tAppAccNo);

        tLCAppAccTraceSchema.setOtherNo(cContInfo.getPrtNo());
        tLCAppAccTraceSchema.setOtherType("4"); // 代表个单印刷号
        tLCAppAccTraceSchema.setBakNo(cContInfo.getContNo());

        double tPayMoney = -1 * cMoney;
        if (tPayMoney > 0)
        {
            buildError("dealAppAccPayMoney", "付费金额异常。");
            return null;
        }
        tLCAppAccTraceSchema.setMoney(tPayMoney);

        tLCAppAccTraceSchema.setOperator(mGlobalInput.Operator);

        AppAcc tAppAcc = new AppAcc();

        tTmpMap = null;
        tTmpMap = tAppAcc.accTakeOutXQY(tLCAppAccTraceSchema);
        if (tTmpMap == null)
        {
            String tErrInfo = "投保人帐户异常。";
            CErrors tObjErr = tAppAcc.mErrors;
            if (tObjErr != null)
            {
                tErrInfo = tObjErr.getFirstError();
            }
            buildError("dealAppAcc", tErrInfo);
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;

        return tMMap;
    }

    private MMap dealTempFeeMoney(LCContSchema cContInfo, double cMoney)
    {
        MMap tMMap = new MMap();

        String tCurDate = PubFun.getCurrentDate();
        String tCurTime = PubFun.getCurrentTime();

        String prtSeq = PubFun1.CreateMaxNo("PAYNOTICENO", cContInfo.getPrtNo());
        String serNo = PubFun1.CreateMaxNo("SERIALNO", cContInfo.getManageCom());

        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        tLJTempFeeSchema.setTempFeeNo(prtSeq);
        tLJTempFeeSchema.setTempFeeType("1");
        tLJTempFeeSchema.setRiskCode("000000");

        tLJTempFeeSchema.setAgentGroup(cContInfo.getAgentGroup());
        tLJTempFeeSchema.setAPPntName(cContInfo.getAppntName());
        tLJTempFeeSchema.setAgentCode(cContInfo.getAgentCode());
        tLJTempFeeSchema.setPayDate(tCurDate);

        tLJTempFeeSchema.setPayMoney(cMoney);
        tLJTempFeeSchema.setEnterAccDate(tCurDate);
        tLJTempFeeSchema.setConfMakeDate(tCurDate);
        tLJTempFeeSchema.setConfMakeTime(tCurTime);

        tLJTempFeeSchema.setManageCom(cContInfo.getManageCom());
        tLJTempFeeSchema.setOtherNo(cContInfo.getPrtNo());
        tLJTempFeeSchema.setOtherNoType("4");
        tLJTempFeeSchema.setPolicyCom(cContInfo.getManageCom());
        tLJTempFeeSchema.setSerialNo(serNo);
        tLJTempFeeSchema.setConfFlag("0");
        tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeSchema.setMakeDate(tCurDate);
        tLJTempFeeSchema.setMakeTime(tCurTime);
        tLJTempFeeSchema.setModifyDate(tCurDate);
        tLJTempFeeSchema.setModifyTime(tCurTime);

        tMMap.put(tLJTempFeeSchema, SysConst.INSERT);

        LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
        tLJTempFeeClassSchema.setTempFeeNo(prtSeq);

        tLJTempFeeClassSchema.setPayMode("5");

        tLJTempFeeClassSchema.setPayDate(tCurDate);
        tLJTempFeeClassSchema.setPayMoney(cMoney);
        tLJTempFeeClassSchema.setEnterAccDate(tCurDate);
        tLJTempFeeClassSchema.setConfMakeDate(tCurDate);
        tLJTempFeeClassSchema.setConfMakeTime(tCurTime);

        tLJTempFeeClassSchema.setManageCom(cContInfo.getManageCom());
        tLJTempFeeClassSchema.setPolicyCom(cContInfo.getManageCom());

        tLJTempFeeClassSchema.setSerialNo(serNo);
        tLJTempFeeClassSchema.setConfFlag("0");
        tLJTempFeeClassSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeClassSchema.setMakeDate(tCurDate);
        tLJTempFeeClassSchema.setMakeTime(tCurTime);
        tLJTempFeeClassSchema.setModifyDate(tCurDate);
        tLJTempFeeClassSchema.setModifyTime(tCurTime);

        tMMap.put(tLJTempFeeClassSchema, SysConst.INSERT);

        return tMMap;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ":" + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
