package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class ProposalDownloadNewUI {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private VData mInputData;
    private String mOperate;

    public boolean submitData(VData cInputData, String cOperate) throws
            Exception {
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        ProposalDownloadNewBL tProposalDownloadNewBL = new ProposalDownloadNewBL();
        tProposalDownloadNewBL.submitData(mInputData, mOperate);
        return true;
    }
}
