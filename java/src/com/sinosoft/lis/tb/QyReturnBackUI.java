package com.sinosoft.lis.tb;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class QyReturnBackUI {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private String mOperate = "";

	public QyReturnBackUI() {
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LCContReceiveUI";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public boolean submitData(VData cInputData, String cOperate) {
		try {
			if (!cOperate.equals("UPDATE")) { //修改
				buildError("submitData", "不支持的操作字符串");
				return false;
			}
			this.mOperate = cOperate;
			QyReturnBackBL tQyReturnBackBL = new QyReturnBackBL();

			System.out.println("QyReturnBackBL BEGIN");
			if (tQyReturnBackBL.submitData(cInputData, mOperate) == false) {
				// @@错误处理
				this.mErrors.copyAllErrors(tQyReturnBackBL.mErrors);
				return false;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			buildError("submit", "发生异常");
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
	}

}
