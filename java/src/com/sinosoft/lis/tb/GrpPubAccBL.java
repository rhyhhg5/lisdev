package com.sinosoft.lis.tb;

import java.lang.reflect.Method;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContPlanDB;
import com.sinosoft.lis.db.LCContPlanDutyParamDB;
import com.sinosoft.lis.db.LCContPlanRiskDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpFeeDB;
import com.sinosoft.lis.db.LCGrpInterestDB;
import com.sinosoft.lis.db.LCInsuredListDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LMRiskDutyFactorDB;
import com.sinosoft.lis.db.LMRiskFeeDB;
import com.sinosoft.lis.db.LMRiskInsuAccDB;
import com.sinosoft.lis.db.LMRiskInterestDB;
import com.sinosoft.lis.db.LMRiskToAccDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.schema.LCContPlanSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpFeeSchema;
import com.sinosoft.lis.schema.LCGrpInterestSchema;
import com.sinosoft.lis.schema.LCInsuredListSchema;
import com.sinosoft.lis.schema.LDPromiseRateSchema;
import com.sinosoft.lis.schema.LMRiskInsuAccSchema;
import com.sinosoft.lis.schema.LMRiskInterestSchema;
import com.sinosoft.lis.schema.LMRiskSchema;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.lis.vschema.LCContPlanSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCGrpFeeSet;
import com.sinosoft.lis.vschema.LCGrpInterestSet;
import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.lis.vschema.LDPromiseRateSet;
import com.sinosoft.lis.vschema.LMRiskFeeSet;
import com.sinosoft.lis.vschema.LMRiskInterestSet;
import com.sinosoft.lis.vschema.LMRiskToAccSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 *
 * <p>Title: 处理公共账户</p>
 *
 * <p>Description: 将公共账户的信息传入,处理相关各表的保费项</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author YangMing
 * @version 1.0
 */
public class GrpPubAccBL
{
    /**
     * 公共传输对象
     */
    private VData mInputData = new VData();

    private String mOperate = "";

    public CErrors mErrors = new CErrors();

    private LCInsuredListSet mLCInsuredListSet = new LCInsuredListSet();

    private TransferData mTransferData = new TransferData();

    private LCContPlanDutyParamSet mLCContPlanDutyParamSet = new LCContPlanDutyParamSet();

    private LCContPlanRiskSchema mLCContPlanRiskSchema = new LCContPlanRiskSchema();

    private LCContPlanSchema mLCContPlanSchema = new LCContPlanSchema();

    private LMRiskFeeSet mLMRiskFeeSet = new LMRiskFeeSet();

    private LCGrpContSchema mLCGrpContSchema = null;

    private LCGrpFeeSet mLCGrpFeeSet = null;

    private LCGrpFeeSet cLCGrpFeeSet = null;

    private GlobalInput mGlobalInput = new GlobalInput();

    private LCGrpInterestSet mLCGrpInterestSet = new LCGrpInterestSet();
    
    private LDPromiseRateSet mLDPromiseRateSet = new LDPromiseRateSet();

    private MMap map = new MMap();

    /**日期时间对象*/
    private String CurrentDate = PubFun.getCurrentDate();

    private String CurrentTime = PubFun.getCurrentTime();

    /**用于向保费计算时传送一个批次号*/
    private String BatchNo = null;

    /**团体合同、险种信息*/
    private String GrpContNo = null;

    private String GrpPolNo = null;

    private String RiskCode = null;

    private String InsuAccNo = null;
    
    private String ClaimNum = null;
    
    private String Flag = null;

    public GrpPubAccBL()
    {
    }

    public static void main(String[] args)
    {
        GrpPubAccBL grppubaccbl = new GrpPubAccBL();
        grppubaccbl.testDealInterest("1400000784");
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mInputData = cInputData;
        this.mOperate = cOperate;
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        if (!preparesubmitData())
        {
            CError tError = new CError();
            tError.moduleName = "GrpPubAccBL";
            tError.functionName = "preparesubmitData";
            tError.errorMessage = "准备递交时失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(this.mInputData, "INSERT"))
        {
            System.out
                    .println("GrpPubAccBL.submitData(cInputData, cOperate)  \n--Line:64  --Author:YangMing");
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }

        if ("DELETE||MAIN".equals(mOperate))
        {
            // 删除帐户信息时，同时删除关于帐户的那张公共账户保单。
            String tStrSql = " select * from lccont "
                    + " where grpcontno = '"
                    + this.GrpContNo
                    + "' "
                    + " and PolType = '2' "
                    + " and contno in (select contno from lcpol where grpcontno = '"
                    + this.GrpContNo + "' and riskcode = '" + this.RiskCode
                    + "') ";
            LCContDB tLCContDB = new LCContDB();
            LCContSet tLCContSet = tLCContDB.executeQuery(tStrSql);
            if (tLCContSet != null && tLCContSet.size() > 0)
            {
                // 删除公共账户保单的所有相关信息。
                LCContDelPUI tLCContDelPUI = new LCContDelPUI();
                VData tInputData = new VData();
                tInputData.add(mGlobalInput);

                tInputData.add(tLCContSet);

                if (!tLCContDelPUI.submitData(tInputData, "DELETE"))
                {
                    if (tLCContDelPUI.mErrors.needDealError())
                    {
                        mErrors.copyAllErrors(tLCContDelPUI.mErrors);
                        return false;
                    }
                    else
                    {
                        buildError("submitData",
                                "LCContDelPUI发生错误，但是没有提供详细的出错信息");
                        return false;
                    }
                }
            }
        }
        //------------------------------------------------

        /**
         * 要在递交后处理保费合计问题
         * 出要处理LCDuty,LCPol,LCCont,LCGrpCont,LCGrpPol
         * 原理从保费项中查询处总保费然后填充各表
         */
        if (!dealSumPrem())
        {
            return false;
        }
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData()
    {
        /** 校验利率保存相关 */
        if (this.mOperate != null && this.mOperate.equals("INTEREST||MAIN"))
        {
            if (this.mLCGrpInterestSet == null
                    || this.mLCGrpInterestSet.size() <= 0)
            {
                buildError("checkData", "没有录入利率信息！");
                return false;
            }
        }
        return true;
    }

    /**
     * dealSumPrem
     *
     * @return boolean
     */
    private boolean dealSumPrem()
    {
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            if (!insertData())
            {
                return false;
            }
        }
        if (this.mOperate.equals("HOUSTO||MAIN"))
        {
        	if (!insertHouston())
            {
                return false;
            }
        }
        //处理管理费
        if (this.mOperate.equals("MANAGEFEE||MAIN"))
        {
        	if(!dealPubacc()){
            	return false;
            }
        	if (!insertManage())
            {
                return false;
            }
        }
        //处理162501赔付顺序
        if (this.mOperate.equals("CLAIMNUM||MAIN"))
        {
        	if (!insertClaimNum())
            {
                return false;
            }
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            if (!updateData())
            {
                return false;
            }
        }
        //删除操作
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            if (!deleteData())
            {
                return false;
            }
        }
        if (this.mOperate.equals("INTEREST||MAIN"))
        {
            if (!interestData())
            {
                return false;
            }
        }
        if (this.mOperate.equals("GuaranteeRate||MAIN"))
        {
        	if(!dealGuaRate())
        	{
        		return false;
        	}
        }
        return true;
    }

    /**
     * InterestData
     *
     * @return boolean
     */
    private boolean interestData()
    {
        for (int i = 1; i <= this.mLCGrpInterestSet.size(); i++)
        {
            LCGrpInterestSchema tLCGrpInterestSchema = mLCGrpInterestSet.get(i);
            LCGrpInterestDB tLCGrpInterestDB = tLCGrpInterestSchema.getDB();
            if (!tLCGrpInterestDB.getInfo())
            {
                buildError("interestData", tLCGrpInterestSchema.mErrors
                        .getFirstError());
                return false;
            }
            tLCGrpInterestSchema.setMakeDate(tLCGrpInterestDB.getMakeDate());
            tLCGrpInterestSchema.setMakeTime(tLCGrpInterestDB.getMakeTime());
            tLCGrpInterestSchema.setModifyDate(PubFun.getCurrentDate());
            tLCGrpInterestSchema.setModifyTime(PubFun.getCurrentTime());
            tLCGrpInterestSchema.setOperator(this.mGlobalInput.Operator);
        }
        map.put(mLCGrpInterestSet, "DELETE&INSERT");
        return true;
    }

    /**
     * updateData
     *
     * @return boolean
     */
    private boolean updateData()
    {
        return true;
    }

    /**
     * insertData
     *
     * @return boolean
     */
    private boolean insertData()
    {
        int AccCount = mLCInsuredListSet.size();
        System.out.println("公共账户数目 : " + AccCount);
        //这里的批次号只是临时的批次号在导入被保险人清单后就会删除
        BatchNo = PubFun1.CreateMaxNo("BATCHNO", 10);
        String ContID = "";
        if (AccCount > 0)
        {
            String MaxContID = "select max(integer(InsuredID)) from lcinsuredlist where grpcontno='"
                    + mLCInsuredListSet.get(1).getGrpContNo()
                    + "' and InsuredID not in ('C','G')";
            System.out.println(" ###查询sql :" + MaxContID);
            ExeSQL tExeSQL = new ExeSQL();
            try
            {
                ContID = String.valueOf(Integer.parseInt(tExeSQL
                        .getOneValue(MaxContID)) + 1);
            }
            catch (Exception ex)
            {
                ContID = "1";
            }
        }
        System.out.println("ContID : " + ContID);
        if (Integer.parseInt(ContID) <= 0)
        {
            ContID = "1";
        }
        //需要根据页面传入的责任信息,
        for (int i = 1; i <= AccCount; i++)
        {
            /**
             * 账户型险种真对险种,不对保险计划
             * 但是系统支持不好需要在保费计算时进行处理
             */
            mLCInsuredListSet.get(i).setState("0");
            mLCInsuredListSet.get(i).setContNo("0");
            ContID = String.valueOf(Integer.parseInt(ContID) + 1);
            mLCInsuredListSet.get(i).setInsuredID(
                    mLCInsuredListSet.get(i).getPublicAccType());
            mLCInsuredListSet.get(i).setOperator(mGlobalInput.Operator);
            mLCInsuredListSet.get(i).setMakeDate(this.CurrentDate);
            mLCInsuredListSet.get(i).setModifyDate(this.CurrentDate);
            mLCInsuredListSet.get(i).setMakeTime(this.CurrentTime);
            mLCInsuredListSet.get(i).setModifyTime(this.CurrentTime);
            String Sql = "delete from LCInsuredList where grpcontno='"
                    + mLCInsuredListSet.get(1).getGrpContNo() + "' and "
                    + " InsuredID='" + mLCInsuredListSet.get(i).getInsuredID()
                    + "'";
            map.put(Sql, "DELETE");
        }
        if (this.mLCInsuredListSet.size() > 0)
        {
            map.put(this.mLCInsuredListSet, "INSERT");
        }
        /**
         * 将账户要素信息保存到保险计划中的要素表中
         */
        if (mLCContPlanDutyParamSet == null)
        {
            return builderError("insertData", "要素信息为空!");
        }
        if (mLCContPlanDutyParamSet.size() <= 0)
        {
            return builderError("insertData", "要素信息为空!");
        }
        LMRiskDB tLMRiskDB = new LMRiskDB();
        tLMRiskDB.setRiskCode(mLCContPlanDutyParamSet.get(1).getRiskCode());
        if (!tLMRiskDB.getInfo())
        {
            return builderError("insertData", "查询险种代码失败！");
        }
        LMRiskSchema tLMRiskSchema = tLMRiskDB.getSchema();
        for (int i = 1; i <= this.mLCContPlanDutyParamSet.size(); i++)
        {
            mLCContPlanDutyParamSet.get(i).setRiskVersion(
                    tLMRiskSchema.getRiskVer());
            mLCContPlanDutyParamSet.get(i).setMainRiskVersion(
                    tLMRiskSchema.getRiskVer());
        }
        if (!dealContRiskDutyFactor(tLMRiskSchema))
        {
            return builderError("insertData", "处理保险计划要素信息失败！");
        }
        if (!dealManageFee())
        {
            return builderError("insertData", "计算管理费失败！");
        }
        if (!dealInterest())
        {
            return builderError("insertData", "利息计算失败！");
        }
        
        if(!dealRateType()){
        	return builderError("insertData", "账户保费计算失败！");
        }

        return true;
    }

    private boolean dealRateType() {
    	
    	LDCodeDB tLDCodeDB = new LDCodeDB();
    	tLDCodeDB.setCodeType("chargefeeratetype");
    	tLDCodeDB.setCode(RiskCode);
    	if(!tLDCodeDB.getInfo()){
    		return true;
    	}
    	String feetype="";
    	for(int i=1;i<=mLCContPlanDutyParamSet.size();i++){
    		if("ChargeFeeRateType".equals(mLCContPlanDutyParamSet.get(i).getCalFactor())){
    			feetype=mLCContPlanDutyParamSet.get(i).getCalFactorValue();
    			break;
    		}
    	}
    	if("04".equals(feetype)){
    		double fee=1.0;
        	double tpublicacc = 0.0;
        	for(int i=1;i<=mLCGrpFeeSet.size();i++){
        		
            	if(InsuAccNo.equals(mLCGrpFeeSet.get(i).getInsuAccNo())){
            		fee=mLCGrpFeeSet.get(i).getFeeValue();
            		break;
            	}
            }
        	if (this.mLCInsuredListSet.size() > 0)
            {    		
                for(int i=1;i<=mLCInsuredListSet.size();i++){
                	tpublicacc=mLCInsuredListSet.get(i).getPublicAcc();
                	mLCInsuredListSet.get(i).setPublicAcc(tpublicacc+tpublicacc*fee);
                }
        		map.put(this.mLCInsuredListSet, "DELETE&INSERT");
            }
    	}
    	
		return true;
	}
    
    private boolean dealPubacc() {   	
	    LCContPlanDutyParamDB tLCContPlanDutyParamDB = new LCContPlanDutyParamDB();
	    LMRiskInsuAccDB tLMRiskInsuAccDB = new LMRiskInsuAccDB();
	    LMRiskInsuAccSchema tLMRiskInsuAccSchema = new LMRiskInsuAccSchema();
	    LCInsuredListSet tLCInsuredListSet = new LCInsuredListSet();
	    LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
	    String acctype = "";

        String tStrSql = "select * from LCContPlanDutyParam lc where  GrpContNo = '"+ this.GrpContNo + "' and riskcode='"+this.RiskCode+"' "
                       + " and exists (select 1 from ldcode where codetype='chargefeeratetype' and code=lc.riskcode) "
                       + " and lc.ContPlanCode='11' and calfactor='ChargeFeeRateType' and lc.calfactorvalue='04' ";

        LCContPlanDutyParamSet tLCContPlanDutyParamSet = tLCContPlanDutyParamDB.executeQuery(tStrSql);
        if(tLCContPlanDutyParamSet.size()>0){
    	    LCGrpFeeSchema tLCGrpFeeSchema= new LCGrpFeeSchema();
    	    tStrSql = "select * from  LCContPlanDutyParam lc where  GrpContNo = '"+ this.GrpContNo + "' and riskcode='"+this.RiskCode+"' and lc.ContPlanCode='11' and calfactor='PublicAcc'";
    	    tLCContPlanDutyParamSet = tLCContPlanDutyParamDB.executeQuery(tStrSql);
    	    double publicacc = Double.parseDouble(tLCContPlanDutyParamSet.get(1).getCalFactorValue());
    	    for (int i = 1; i <= this.cLCGrpFeeSet.size(); i++)
            {
    		    tLCGrpFeeSchema=cLCGrpFeeSet.get(i);
    		    if("000000".equals(tLCGrpFeeSchema.getInsuAccNo())){
    		    	continue;
    		    }
    		    acctype="select * from LMRiskInsuAcc where insuaccno ='"+tLCGrpFeeSchema.getInsuAccNo()+"'";
    		    tLMRiskInsuAccSchema=tLMRiskInsuAccDB.executeQuery(acctype).get(1);
    		    if("001".equals(tLMRiskInsuAccSchema.getAccType())){ //C
    			    tStrSql="select * from lcinsuredlist where grpcontno='"+this.GrpContNo+"' and  InsuredID='C'";
    			    if(tLCInsuredListDB.executeQuery(tStrSql).size()>0){
    			        LCInsuredListSchema tLCInsuredListSchema=tLCInsuredListDB.executeQuery(tStrSql).get(1);
    			        tLCInsuredListSchema.setPublicAcc(publicacc+publicacc*tLCGrpFeeSchema.getFeeValue());
    			        tLCInsuredListSet.add(tLCInsuredListSchema);
    			    }
    		    }else if ("004".equals(tLMRiskInsuAccSchema.getAccType())){ //G
    			    tStrSql="select * from lcinsuredlist where grpcontno='"+this.GrpContNo+"' and  InsuredID='G'";
    			    if(tLCInsuredListDB.executeQuery(tStrSql).size()>0){
    			        LCInsuredListSchema tLCInsuredListSchema=tLCInsuredListDB.executeQuery(tStrSql).get(1);
    			        tLCInsuredListSchema.setPublicAcc(publicacc+publicacc*tLCGrpFeeSchema.getFeeValue());
    			        tLCInsuredListSet.add(tLCInsuredListSchema);
    			    }
    		     }
            }
        }
        map.put(tLCInsuredListSet, "DELETE&INSERT");
	
		return true;
	}

	/**
     * dealInterest
     *
     * @return boolean
     */
    private boolean dealInterest()
    {
        mLCGrpInterestSet = new LCGrpInterestSet();
        LMRiskToAccDB tLMRisktoAccDB = new LMRiskToAccDB();
        tLMRisktoAccDB.setRiskCode(this.RiskCode);
        LMRiskToAccSet tLMRiskToAccSet = tLMRisktoAccDB.query();
        if (tLMRiskToAccSet.size() <= 0)
        {
            return builderError("initManageFee", "未查询到账户描述描述");
        }
        System.out.println("开始计算利息");
        for (int i = 1; i <= tLMRiskToAccSet.size(); i++)
        {
            if (tLMRiskToAccSet.get(i).getInsuAccNo().equals(InsuAccNo))
            {
                LMRiskInterestDB tLMRiskInterestDB = new LMRiskInterestDB();
                tLMRiskInterestDB.setRiskCode(this.RiskCode);
                tLMRiskInterestDB.setInsuAccNo(InsuAccNo);
                LMRiskInterestSet tLMRiskInterestSet = tLMRiskInterestDB
                        .query();
                if (tLMRiskInterestSet != null && tLMRiskInterestSet.size() > 0)
                {
                    for (int m = 1; m <= tLMRiskInterestSet.size(); m++)
                    {
                        /** 有两种操作，１、取固定值。２、取算法 */
                        double tInterest = getInterest(tLMRiskInterestSet
                                .get(m));
                        LCGrpInterestSchema tLCGrpInterestSchema = new LCGrpInterestSchema();
                        tLCGrpInterestSchema.setGrpContNo(this.GrpContNo);
                        tLCGrpInterestSchema.setGrpPolNo(this.GrpPolNo);
                        tLCGrpInterestSchema.setInsuAccNo(tLMRiskToAccSet
                                .get(i).getInsuAccNo());
                        tLCGrpInterestSchema.setInterestCode(tLMRiskInterestSet
                                .get(m).getInterestCode());
                        tLCGrpInterestSchema.setRiskCode(this.RiskCode);
                        tLCGrpInterestSchema
                                .setInterestTakePlace(tLMRiskInterestSet.get(m)
                                        .getInterestTakePlace());
                        tLCGrpInterestSchema.setInterestType(tLMRiskInterestSet
                                .get(m).getInterestType());
                        tLCGrpInterestSchema
                                .setInterestCalType(tLMRiskInterestSet.get(m)
                                        .getInterestCalType());
                        tLCGrpInterestSchema
                                .setDefaultCalType(tLMRiskInterestSet.get(m)
                                        .getDefaultCalType());
                        tLCGrpInterestSchema.setDefaultRate(tInterest);
                        tLCGrpInterestSchema.setFloatCalType(tLMRiskInterestSet
                                .get(m).getFloatCalType());
                        tLCGrpInterestSchema.setFloatCalCode(tLMRiskInterestSet
                                .get(m).getFloatCalCode());
                        tLCGrpInterestSchema
                                .setInterestPeriod(tLMRiskInterestSet.get(m)
                                        .getInterestPeriod());
                        /*                            tLCGrpInterestSchema.setMakeDate(
                         PubFun.getCurrentDate());
                         tLCGrpInterestSchema.setMakeTime(
                         PubFun.getCurrentTime());
                         tLCGrpInterestSchema.setModifyDate(
                         PubFun.getCurrentDate());
                         tLCGrpInterestSchema.setModifyTime(
                         PubFun.getCurrentTime());
                         */
                        PubFun.fillDefaultField(tLCGrpInterestSchema);
                        tLCGrpInterestSchema.setOperator(mGlobalInput.Operator);
                        mLCGrpInterestSet.add(tLCGrpInterestSchema);
                    }
                }
            }
        }
        if (mLCGrpInterestSet.size() > 0)
        {
            this.map.put(mLCGrpInterestSet, "DELETE&INSERT");
        }
        return true;
    }

    /**
     * 取利息率，
     *
     * @param tLMRiskInterestSchema LMRiskInterestSchema
     * @return double
     */
    private double getInterest(LMRiskInterestSchema tLMRiskInterestSchema)
    {
        /** 取固定值 */
        System.out.println("tLMRiskInterestSchema " + tLMRiskInterestSchema);
        if (tLMRiskInterestSchema.getFloatCalType().equals("1"))
        {
            System.out.println("\n去利息固定值");
            return tLMRiskInterestSchema.getDefaultRate();
        }
        /** 根据算法计算 */
        if (tLMRiskInterestSchema.getFloatCalType().equals("2"))
        {
            System.out.println("\n取算法计算利息率");
            return getCalInterest(tLMRiskInterestSchema);
        }
        return 0.0;
    }

    /**
     * 根据算法计算利息
     *
     * @param tLMRiskInterestSchema LMRiskInterestSchema
     * @return double
     */
    private double getCalInterest(LMRiskInterestSchema tLMRiskInterestSchema)
    {
        int tInsuYear = PubFun.calInterval2(this.mLCGrpContSchema
                .getCValiDate(), this.mLCGrpContSchema.getCInValiDate(), "D");
        Calculator tCalculator = new Calculator();
        tCalculator.setCalCode(tLMRiskInterestSchema.getFloatCalCode());
        tCalculator.addBasicFactor("InsuYear", String.valueOf(tInsuYear));
        String tInterest = tCalculator.calculate();
        if (tInterest == null || tInterest.equals(""))
        {
            tInterest = "0.0";
        }
        return Double.parseDouble(tInterest);
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mLCInsuredListSet = (LCInsuredListSet) cInputData
                .getObjectByObjectName("LCInsuredListSet", 0);
        mLCContPlanDutyParamSet = (LCContPlanDutyParamSet) cInputData
                .getObjectByObjectName("LCContPlanDutyParamSet", 0);
        cLCGrpFeeSet = (LCGrpFeeSet) cInputData.getObjectByObjectName(
                "LCGrpFeeSet", 0);
        mLCGrpInterestSet = (LCGrpInterestSet) cInputData
                .getObjectByObjectName("LCGrpInterestSet", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mLDPromiseRateSet = (LDPromiseRateSet) cInputData.getObjectByObjectName(
        		"LDPromiseRateSet", 0);
        this.GrpContNo = (String) mTransferData.getValueByName("GrpContNo");
        this.GrpPolNo = (String) mTransferData.getValueByName("GrpPolNo");
        this.RiskCode = (String) mTransferData.getValueByName("RiskCode");
        this.InsuAccNo = (String) mTransferData.getValueByName("InsuAccNo");
        this.ClaimNum = (String) mTransferData.getValueByName("ClaimNum");
        this.Flag = (String) mTransferData.getValueByName("Flag");
        return true;
    }

    /**
     * 准备向后台递交
     *
     * @return boolean
     */
    private boolean preparesubmitData()
    {
        this.mInputData.clear();
        this.mInputData.add(map);
        return true;
    }

    private boolean builderError(String tFunctionName, String tErrorMessage)
    {
        CError tError = new CError();
        tError.moduleName = "GrpPubAccBL";
        tError.functionName = tFunctionName;
        tError.errorMessage = tErrorMessage;
        this.mErrors.addOneError(tError);
        return false;
    }

    /**
     * 处理保险计划要素信息
     * @param tLMRiskSchema LMRiskSchema
     * @return boolean
     */
    private boolean dealContRiskDutyFactor(LMRiskSchema tLMRiskSchema)
    {
        LMRiskDutyFactorDB tLMRiskDutyFactorDB = new LMRiskDutyFactorDB();
        tLMRiskDutyFactorDB.setRiskCode(tLMRiskSchema.getRiskCode());
        /**处理保险计划险种信息*/
        try
        {
            mLCContPlanRiskSchema.setGrpContNo(mLCContPlanDutyParamSet.get(1)
                    .getGrpContNo());
            mLCContPlanRiskSchema.setProposalGrpContNo(mLCContPlanDutyParamSet
                    .get(1).getGrpContNo());
            mLCContPlanRiskSchema.setMainRiskCode(mLCContPlanDutyParamSet
                    .get(1).getRiskCode());
            mLCContPlanRiskSchema.setMainRiskVersion(mLCContPlanDutyParamSet
                    .get(1).getMainRiskVersion());
            mLCContPlanRiskSchema.setRiskCode(mLCContPlanDutyParamSet.get(1)
                    .getRiskCode());
            mLCContPlanRiskSchema.setRiskVersion(mLCContPlanDutyParamSet.get(1)
                    .getRiskVersion());
            mLCContPlanRiskSchema.setContPlanCode(mLCContPlanDutyParamSet
                    .get(1).getContPlanCode());
            mLCContPlanRiskSchema.setContPlanName(mLCContPlanDutyParamSet
                    .get(1).getContPlanName());
            mLCContPlanRiskSchema.setPlanType(mLCContPlanDutyParamSet.get(1)
                    .getPlanType());
            mLCContPlanRiskSchema.setOperator(mGlobalInput.Operator);
            mLCContPlanRiskSchema.setMakeDate(this.CurrentDate);
            mLCContPlanRiskSchema.setMakeTime(this.CurrentTime);
            mLCContPlanRiskSchema.setModifyDate(this.CurrentDate);
            mLCContPlanRiskSchema.setModifyTime(this.CurrentTime);
            /**在处理账户时只会有一个计划*/
            mLCContPlanSchema
                    .setGrpContNo(mLCContPlanRiskSchema.getGrpContNo());
            mLCContPlanSchema.setProposalGrpContNo(mLCContPlanRiskSchema
                    .getGrpContNo());
            mLCContPlanSchema.setContPlanCode(mLCContPlanRiskSchema
                    .getContPlanCode());
            mLCContPlanSchema.setContPlanName(mLCContPlanRiskSchema
                    .getContPlanName());
            mLCContPlanSchema.setPlanType(mLCContPlanRiskSchema.getPlanType());
            mLCContPlanSchema.setOperator(mGlobalInput.Operator);
            mLCContPlanSchema.setMakeDate(this.CurrentDate);
            mLCContPlanSchema.setMakeTime(this.CurrentTime);
            mLCContPlanSchema.setModifyDate(this.CurrentDate);
            mLCContPlanSchema.setModifyTime(this.CurrentTime);
            map.put(mLCContPlanDutyParamSet, "DELETE&INSERT");
            map.put(mLCContPlanRiskSchema, "DELETE&INSERT");
            map.put(mLCContPlanSchema, "DELETE&INSERT");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 处理管理费
     * @return boolean
     */
    private boolean dealManageFee()
    {
        if (!initManageFee())
            return false;
        if (!calFee())
            return false;
        return true;
    }

    /**
     * 初始化管理费
     * @return boolean
     */
    private boolean initManageFee()
    {
        LMRiskToAccDB tLMRisktoAccDB = new LMRiskToAccDB();
        tLMRisktoAccDB.setRiskCode(this.RiskCode);
        LMRiskToAccSet tLMRiskToAccSet = tLMRisktoAccDB.query();
        if (tLMRiskToAccSet.size() <= 0)
        {
            return builderError("initManageFee", "未查询到账户描述描述");
        }
        for (int i = 1; i <= tLMRiskToAccSet.size(); i++)
        {
            String InsuAccNo = tLMRiskToAccSet.get(i).getInsuAccNo();
            LMRiskInsuAccDB tLMRiskInsuAccDB = new LMRiskInsuAccDB();
            tLMRiskInsuAccDB.setInsuAccNo(InsuAccNo);
            if (!tLMRiskInsuAccDB.getInfo())
            {
                buildError("initManageFee", "查询账户定义描述失败！");
                return false;
            }
            if (InsuAccNo.equals(this.InsuAccNo)
                    || tLMRiskInsuAccDB.getAccType().equals("002"))
            {
                LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
                StringBuffer manage_sql = new StringBuffer(255);
                manage_sql
                        .append("select * from lmriskfee where InsuAccNo = '");
                manage_sql.append(InsuAccNo);
                manage_sql.append("' and FeeItemType<>'00'");
                LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB
                        .executeQuery(manage_sql.toString());
                System.out.println("管理费 ： " + manage_sql.toString());
                this.mLMRiskFeeSet.add(tLMRiskFeeSet);
            }
        }
        StringBuffer sql = new StringBuffer();
        sql
                .append("select * from lmriskfee where InsuAccNo ='000000' and payplancode in (select payplancode from LMDutyPayRela where dutycode in (select dutycode from lmriskduty where riskcode='");
        sql.append(this.RiskCode);
        sql.append("'))and FeeItemType<>'00'");
        LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
        LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.executeQuery(sql.toString());
        this.mLMRiskFeeSet.add(tLMRiskFeeSet);

        if("370301".equals(this.RiskCode)){
        	LMRiskFeeDB ttLMRiskFeeDB = new LMRiskFeeDB();
        	String sql1 = "select * from lmriskfee where InsuAccNo ='000000' and riskcode='"
        			    + this.RiskCode 
        			    + "'and FeeItemType<>'00'";		
            LMRiskFeeSet ttLMRiskFeeSet = ttLMRiskFeeDB
                    .executeQuery(sql1);
        	this.mLMRiskFeeSet.add(ttLMRiskFeeSet);
        }

        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(this.GrpContNo);
        if (!tLCGrpContDB.getInfo())
        {
            return builderError("initManageFee", "未查询到团体合同信息！");
        }
        mLCGrpContSchema = new LCGrpContSchema();
        this.mLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());
        return true;
    }

    /**
     * 计算管理费
     * @return boolean
     */
    private boolean calFee()
    {
        LCGrpFeeSet tLCGrpFeeSet = new LCGrpFeeSet();
        LCGrpFeeSchema tLCGrpFeeSchema = null;
        System.out.println("调试 : " + mLMRiskFeeSet.size() + " 其中 "
                + mLMRiskFeeSet.encode());
        for (int i = 1; i <= this.mLMRiskFeeSet.size(); i++)
        {
            //如果FeeCalModeType==1 表示根据sql算法计算管理费
            if (mLMRiskFeeSet.get(i).getFeeCalModeType().equals("1"))
            {
                if (StrTool.cTrim(mLMRiskFeeSet.get(i).getFeeCalCode()).equals(
                        ""))
                    return builderError("calFee", "为描述管理费计算算法！");
                String CalCode = mLMRiskFeeSet.get(i).getFeeCalCode();
                Calculator mCalculator = initCalFactor();
                String calValue = cal(mCalculator, CalCode);
                if (StrTool.cTrim(calValue).equals(""))
                {
                    //                    return builderError("calFee", "计算管理费失败！");
                    System.out.println("当管理费计素失败时认为管理费为0");
                    calValue = "0";
                }
                double FeeRate = Double.parseDouble(calValue);
                tLCGrpFeeSchema = new LCGrpFeeSchema();
                //将计算出来的管理费比率存入
                if (FeeRate > 0)
                {
                    tLCGrpFeeSchema.setFeeValue(FeeRate);
                    tLCGrpFeeSchema.setFeeCode(mLMRiskFeeSet.get(i)
                            .getFeeCode());
                    tLCGrpFeeSchema.setInsuAccNo(mLMRiskFeeSet.get(i)
                            .getInsuAccNo());
                    tLCGrpFeeSchema.setPayPlanCode(mLMRiskFeeSet.get(i)
                            .getPayPlanCode());
                    tLCGrpFeeSchema.setPayInsuAccName(mLMRiskFeeSet.get(i)
                            .getPayInsuAccName());
                    tLCGrpFeeSchema.setPayInsuAccName(mLMRiskFeeSet.get(i)
                            .getPayInsuAccName());
                    tLCGrpFeeSchema.setFeeCalMode(mLMRiskFeeSet.get(i)
                            .getFeeCalMode());
                    tLCGrpFeeSchema.setFeeCalModeType(mLMRiskFeeSet.get(i)
                            .getFeeCalModeType());
                    tLCGrpFeeSchema.setFeePeriod(mLMRiskFeeSet.get(i)
                            .getFeePeriod());
                    tLCGrpFeeSchema.setMaxTime(mLMRiskFeeSet.get(i)
                            .getMaxTime());
                    tLCGrpFeeSchema.setDefaultFlag(mLMRiskFeeSet.get(i)
                            .getDefaultFlag());
                    dealOtherFee(tLCGrpFeeSchema);
                    tLCGrpFeeSet.add(tLCGrpFeeSchema);
                }
            }
            //直接取值
            else if (mLMRiskFeeSet.get(i).getFeeCalModeType().equals("0"))
            {
                tLCGrpFeeSchema = new LCGrpFeeSchema();
                tLCGrpFeeSchema.setFeeValue(mLMRiskFeeSet.get(i).getFeeValue());
                tLCGrpFeeSchema.setFeeCode(mLMRiskFeeSet.get(i).getFeeCode());
                tLCGrpFeeSchema.setInsuAccNo(mLMRiskFeeSet.get(i)
                        .getInsuAccNo());
                tLCGrpFeeSchema.setPayPlanCode(mLMRiskFeeSet.get(i)
                        .getPayPlanCode());
                tLCGrpFeeSchema.setPayInsuAccName(mLMRiskFeeSet.get(i)
                        .getPayInsuAccName());
                tLCGrpFeeSchema.setPayInsuAccName(mLMRiskFeeSet.get(i)
                        .getPayInsuAccName());
                tLCGrpFeeSchema.setFeeCalMode(mLMRiskFeeSet.get(i)
                        .getFeeCalMode());
                tLCGrpFeeSchema.setFeeCalModeType(mLMRiskFeeSet.get(i)
                        .getFeeCalModeType());
                tLCGrpFeeSchema.setFeePeriod(mLMRiskFeeSet.get(i)
                        .getFeePeriod());
                tLCGrpFeeSchema.setMaxTime(mLMRiskFeeSet.get(i).getMaxTime());
                tLCGrpFeeSchema.setDefaultFlag(mLMRiskFeeSet.get(i)
                        .getDefaultFlag());
                dealOtherFee(tLCGrpFeeSchema);
                tLCGrpFeeSet.add(tLCGrpFeeSchema);
            }
        }
        if (tLCGrpFeeSet.size() > 0)
        {
            mLCGrpFeeSet = new LCGrpFeeSet();
            mLCGrpFeeSet.add(tLCGrpFeeSet);
            map.put(mLCGrpFeeSet, "DELETE&INSERT");
        }
        return true;
    }

    /**
     * 封装计算类
     * @return Calculator
     */
    private Calculator initCalFactor()
    {
        Calculator tCalculator = new Calculator();
        //基金规模
        tCalculator.addBasicFactor("PremScope", String.valueOf(mLCGrpContSchema
                .getPremScope()));
        for (int i = 1; i <= this.mLCContPlanDutyParamSet.size(); i++)
        {
            tCalculator.addBasicFactor(mLCContPlanDutyParamSet.get(i)
                    .getCalFactor(), mLCContPlanDutyParamSet.get(i)
                    .getCalFactorValue());
        }
        return tCalculator;
    }

    /**
     * 进行计算
     * @param tCalculator Calculator
     * @param tCalCode String
     * @return String
     */
    private String cal(Calculator tCalculator, String tCalCode)
    {
        tCalculator.setCalCode(tCalCode);
        return tCalculator.calculate();
    }

    /**
     * 处理管理费
     * @param tLCGrpFeeSchema LCGrpFeeSchema
     * @return boolean
     */
    private boolean dealOtherFee(LCGrpFeeSchema tLCGrpFeeSchema)
    {
        tLCGrpFeeSchema.setGrpContNo(this.GrpContNo);
        tLCGrpFeeSchema.setGrpPolNo(this.GrpPolNo);
        tLCGrpFeeSchema.setRiskCode(this.RiskCode);
        tLCGrpFeeSchema.setOperator(this.mGlobalInput.Operator);
        tLCGrpFeeSchema.setModifyDate(this.CurrentDate);
        tLCGrpFeeSchema.setMakeDate(this.CurrentDate);
        tLCGrpFeeSchema.setMakeTime(this.CurrentTime);
        tLCGrpFeeSchema.setModifyTime(this.CurrentTime);
        return true;
    }

    /**
     * 在处理页面委托基金信息定义时操作
     * @return boolean
     */
    private boolean insertHouston()
    {
        for (int i = 1; i <= this.cLCGrpFeeSet.size(); i++)
        {
            this.cLCGrpFeeSet.get(i).setDefaultFlag("0");
        }
        this.map.put(this.cLCGrpFeeSet, "DELETE&INSERT");
        if (!this.preparesubmitData())
            return false;
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(this.mInputData, "INSERT"))
        {
            System.out
                    .println("GrpPubAccBL.submitData(cInputData, cOperate)  \n--Line:64  --Author:YangMing");
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;

    }

    /**
     * 在处理页面录入管理费时操作
     * @return boolean
     */
    private boolean insertManage()
    {
        if (this.cLCGrpFeeSet.size() <= 0)
            return builderError("insertManage", "传入的管理费为空请确认！");
        for (int i = 1; i <= this.cLCGrpFeeSet.size(); i++)
        {
            this.cLCGrpFeeSet.get(i).setDefaultFlag("0");
        }
        this.map.put(this.cLCGrpFeeSet, "DELETE&INSERT");
        if (!this.preparesubmitData())
            return false;
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(this.mInputData, "INSERT"))
        {
            System.out
                    .println("GrpPubAccBL.submitData(cInputData, cOperate)  \n--Line:64  --Author:YangMing");
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 在处理页面录入赔付顺序
     * @return boolean
     */
    private boolean insertClaimNum()
    {
    	String sql="select * from lcgrpfee where grpcontno='"+this.GrpContNo+"'";
    	LCGrpFeeDB tLCGrpFeeDB=new LCGrpFeeDB();
    	this.cLCGrpFeeSet=tLCGrpFeeDB.executeQuery(sql);  
    	LCGrpFeeSchema tLCGrpFeeSchema=new LCGrpFeeSchema();
    	if(this.cLCGrpFeeSet.size() <= 0){
    		return builderError("insertManage", "请先添加账户信息！");
    	}
    	System.out.println("赔付顺序："+this.ClaimNum);
        for (int i = 1; i <= this.cLCGrpFeeSet.size(); i++)
        {
            this.cLCGrpFeeSet.get(i).setDefaultFlag("0");
            tLCGrpFeeSchema=this.cLCGrpFeeSet.get(i);
            tLCGrpFeeSchema.setClaimNum(this.ClaimNum);
        }
        this.map.put(this.cLCGrpFeeSet, "DELETE&INSERT");
        if (!this.preparesubmitData())
            return false;
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(this.mInputData, "INSERT"))
        {
            System.out
                    .println("GrpPubAccBL.submitData(cInputData, cOperate)  \n--Line:64  --Author:YangMing");
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }
    
    /**
     * 处理保证利率
     */
    private boolean dealGuaRate()
    {
    	LDPromiseRateSchema tLDPromiseRateSchema = mLDPromiseRateSet.get(1);
    	if(Flag.equals("0")){
    		tLDPromiseRateSchema.setMakeDate(CurrentDate);
    		tLDPromiseRateSchema.setMakeTime(CurrentTime);
    		tLDPromiseRateSchema.setModifyDate(CurrentDate);
    		tLDPromiseRateSchema.setModifyTime(CurrentTime);
    		tLDPromiseRateSchema.setPromulgateDate(CurrentDate);
    		this.mOperate="INSERT";
    	}else if(Flag.equals("1")){
    		tLDPromiseRateSchema.setModifyDate(CurrentDate);
    		tLDPromiseRateSchema.setModifyTime(CurrentTime);
    		String tSql = "select makedate,maketime,promulgatedate from ldpromiserate where prtno='"+tLDPromiseRateSchema.getPrtNo()+"' and riskcode='370301'";
    		SSRS tSSRS = new ExeSQL().execSQL(tSql);
    		tLDPromiseRateSchema.setMakeDate(tSSRS.GetText(1, 1));
    		tLDPromiseRateSchema.setMakeTime(tSSRS.GetText(1, 2));
    		tLDPromiseRateSchema.setPromulgateDate(tSSRS.GetText(1, 3));
    		this.mOperate="UPDATE";
    	}
    	this.map.put(tLDPromiseRateSchema, mOperate);
        return true;
    }

    /**
     * 删除操作
     * @return boolean
     */
    private boolean deleteData()
    {
        //如果录入了保险计划 则不许删除
        boolean CanDelete = true;
        LCContPlanDutyParamDB tLCContPlanDutyParamDB = new LCContPlanDutyParamDB();

        // 不删除添加险种时，加入的默认计划要素。
        //tLCContPlanDutyParamDB.setGrpContNo(this.GrpContNo);
        //tLCContPlanDutyParamDB.setRiskCode(this.RiskCode);
        String tStrSql = "select * from LCContPlanDutyParam "
                + " where RiskCode = '" + this.RiskCode + "' and GrpContNo = '"
                + this.GrpContNo + "' and Riskversion != '" + this.RiskCode
                + "' ";

        LCContPlanDutyParamSet tLCContPlanDutyParamSet = tLCContPlanDutyParamDB
                .executeQuery(tStrSql);
        for (int i = 1; i <= tLCContPlanDutyParamSet.size(); i++)
        {
            if (!tLCContPlanDutyParamSet.get(i).getContPlanCode().equals("11"))
            {
                CanDelete = false;
                break;
            }
        }
        if (CanDelete)
        {
            LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
            tLCContPlanRiskDB.setGrpContNo(this.GrpContNo);
            tLCContPlanRiskDB.setRiskCode(this.RiskCode);
            tLCContPlanRiskDB.setContPlanCode("11");
            LCContPlanRiskSet tLCContPlanRiskSet = tLCContPlanRiskDB.query();
            if (tLCContPlanRiskSet.size() > 0)
            {
                this.map.put(tLCContPlanRiskSet, "DELETE");
            }
            LCContPlanDB tLCContPlanDB = new LCContPlanDB();
            tLCContPlanDB.setGrpContNo(this.GrpContNo);
            tLCContPlanDB.setContPlanCode("11");
            LCContPlanSet tLCContPlanSet = tLCContPlanDB.query();
            /** 本应删除默认计划,但由于计算方向也保存在默认计划下,因此,判断是否有默认要素如果有的话,不删除计划 */
            StringBuffer sql = new StringBuffer();
            sql
                    .append("select count(1) from lccontplanrisk where contplancode='11' and riskcode<>'");
            sql.append(this.RiskCode);
            sql.append("' and grpcontno='");
            sql.append(this.GrpContNo);
            sql.append("'");
            String chk = (new ExeSQL()).getOneValue(sql.toString());
            if (Integer.parseInt(chk) > 0 && tLCContPlanSet.size() > 0)
            {
                this.map.put(tLCContPlanSet, "DELETE");
            }
            if (tLCContPlanDutyParamSet.size() > 0)
            {
                this.map.put(tLCContPlanDutyParamSet, "DELETE");
            }
            LCGrpFeeDB tLCGrpFeeDB = new LCGrpFeeDB();
            tLCGrpFeeDB.setGrpContNo(this.GrpContNo);
            //只删除该险种的帐户
            tLCGrpFeeDB.setRiskCode(this.RiskCode);
            //-----------------------
            LCGrpFeeSet tLCGrpFeeSet = tLCGrpFeeDB.query();
            if (tLCGrpFeeSet.size() > 0)
            {
                this.map.put(tLCGrpFeeSet, "DELETE");
            }
            if (this.mLCInsuredListSet.size() > 0)
            {
                for (int i = 1; i <= this.mLCInsuredListSet.size(); i++)
                {
                    mLCInsuredListSet.get(i).setInsuredID(
                            mLCInsuredListSet.get(i).getPublicAccType());
                }
                this.map.put(mLCInsuredListSet, "DELETE");
            }
            LCGrpInterestDB tLCGrpInterestDB = new LCGrpInterestDB();
            tLCGrpInterestDB.setGrpContNo(this.GrpContNo);
            //只删除该险种的帐户
            tLCGrpInterestDB.setRiskCode(this.RiskCode);
            //-----------------------
            LCGrpInterestSet tLCGrpInterestSet = tLCGrpInterestDB.query();
            if (tLCGrpInterestSet.size() > 0)
            {
                this.map.put(tLCGrpInterestSet, "DELETE");
            }
        }
        else
        {
            System.out.println("程序第558行出错，请检查GrpPubAccBL.java中的deleteData方法！");
            CError tError = new CError();
            tError.moduleName = "GrpPubAccBL.java";
            tError.functionName = "deleteData";
            tError.errorMessage = "已存在保障计划信息，需要先删除保障计划信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GrpPubAccBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    /**
     * 用于测试
     *
     * @return boolean
     */
    public boolean testDealInterest(String tGrpContNo)
    {
        this.RiskCode = "1605";
        this.InsuAccNo = "100001";
        mLCGrpContSchema = new LCGrpContSchema();
        mLCGrpContSchema.setGrpContNo(tGrpContNo);
        mLCGrpContSchema.getDB().getInfo();
        if (!dealInterest())
        {
            return false;
        }
        return true;
    }
}
