package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDRelatedPartySchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class RelatedTransactionBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private GlobalInput tGI = new GlobalInput();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	private MMap map = new MMap();

	/** 统一更新日期 */
	private String mCurrentDate = PubFun.getCurrentDate();

	/** 统一更新时间 */
	private String mCurrentTime = PubFun.getCurrentTime();

	/** 数据操作字符串 */
	private String mOperate;
	
	private LDRelatedPartySchema mLDRelatedPartySchema = new LDRelatedPartySchema();
	
	public boolean submitData(VData cInputData, String cOperate){
		mOperate = cOperate;
        mInputData = (VData) cInputData.clone();
        
		if(!getInputData()){
			return false;
		}
		if(!checkData()){
			return false;
		}
		if(!dealData()){
			CError tError = new CError();
			tError.moduleName = "RelatedTransactionBL";
			tError.functionName = "dealData";
			tError.errorMessage = "数据处理失败RelatedTransactionBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}
		if(!prepareOutputData()){
			return false;
		}
		PubSubmit ps = new PubSubmit();
		if(!ps.submitData(mInputData, null)){
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}
		return true;
	}
	
	private boolean getInputData(){
		try{
			tGI.setSchema((GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0));
			mLDRelatedPartySchema = (LDRelatedPartySchema)mInputData.getObjectByObjectName("LDRelatedPartySchema", 0);
			return true;
		}catch(Exception ex){
			CError tError = new CError();
            tError.moduleName = "RelatedTransactionBL";
            tError.functionName = "getInputData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            return false;
		}
	}
	
	private boolean checkData(){
		if(mLDRelatedPartySchema.getRelateNo()==null || "".equals(mLDRelatedPartySchema.getRelateNo())){
			CError tError = new CError();
            tError.moduleName = "RelatedTransactionBL";
            tError.functionName = "checkData";
            tError.errorMessage = "未获得关联方编号!";
            this.mErrors.addOneError(tError);
            return false;
		}
		if(mLDRelatedPartySchema.getName()==null || "".equals(mLDRelatedPartySchema.getName())){
			CError tError = new CError();
            tError.moduleName = "RelatedTransactionBL";
            tError.functionName = "checkData";
            tError.errorMessage = "未获得关联方名称!";
            this.mErrors.addOneError(tError);
            return false;
		}
		if((mLDRelatedPartySchema.getRelatedType()==null || "".equals(mLDRelatedPartySchema.getRelatedType()))){
			CError tError = new CError();
            tError.moduleName = "RelatedTransactionBL";
            tError.functionName = "checkData";
            tError.errorMessage = "未获得关联方类别!";
            this.mErrors.addOneError(tError);
            return false;
		}
		return true;
	}
	
	private boolean dealData(){
		if("INSERT".equals(mOperate)){
			map.put(mLDRelatedPartySchema, SysConst.INSERT);
		}
		if("UPDATE".equals(mOperate)){
			
			map.put(mLDRelatedPartySchema, SysConst.UPDATE);
		}
		return true;
	}
	
	private boolean prepareOutputData(){
		try {
			mInputData.add(map);
		}catch (Exception ex){
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TaxIncentivesGrpBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "准备传输数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	public VData getResult(){
        return mResult;
    }
}
