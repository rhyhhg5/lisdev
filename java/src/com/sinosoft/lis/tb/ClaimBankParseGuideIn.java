/**
 * 2012-3-7
 */
package com.sinosoft.lis.tb;

import java.io.File;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.certifybusiness.CertifyDiskImportLog;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.tb.LCPolImpInfo;
import com.sinosoft.lis.tb.XmlFileUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLPathTool;

/**
 * @author LY
 *
 */
public class ClaimBankParseGuideIn
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public CErrors logErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /**内存文件暂存*/
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private String FileName;

    private String XmlFileName;

    private String mPreFilePath;

    private String FilePath = "";

    private String ParseRootPath = "/DATASET/BATCHNO";

    private String ParsePath = "CONTTABLE/ROW";

    //配置文件Xml节点描述
    private String ImportFileName;

    private String ConfigFileName;

    private String mBatchNo = "";

    private String mPrtNo = "";

    private String mContID = null;

    private org.w3c.dom.Document m_doc = null;

    private LCPolImpInfo m_LCPolImpInfo = new LCPolImpInfo();

    private String[] m_strDataFiles = null;

    private boolean ShowSchedule = false;

    //    boolean mIsBPO = false;

    public ClaimBankParseGuideIn()
    {
        bulidDocument();
    }

    public ClaimBankParseGuideIn(String fileName)
    {
        bulidDocument();
        FileName = fileName;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        getInputData();
        if (!checkData())
        {
            return false;
        }
        System.out.println("开始时间:" + PubFun.getCurrentTime());
        try
        {
            if (!parseVts())
            {
                CError tError = new CError();
                tError.moduleName = "TSParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = this.mErrors.getFirstError();
                logErrors.addOneError(tError);
                return false;
            }

            for (int nIndex = 0; nIndex < m_strDataFiles.length; nIndex++)
            {
                XmlFileName = m_strDataFiles[nIndex];
                if (!ParseXml())
                {
                    CError tError = new CError();
                    tError.moduleName = "TSParseGuideIn";
                    tError.functionName = "checkData";
                    tError.errorMessage = this.mErrors.getFirstError();
                    logErrors.addOneError(tError);
                    return false;
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "TSParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "导入文件格式有误!";
            logErrors.addOneError(tError);
        }

        mErrors = logErrors;
        System.out.println("结束时间:" + PubFun.getCurrentTime());
        if (mErrors.getErrorCount() > 0)
        {
            return false;
        }

        return true;
    }

    /**
     * 得到传入数据
     */
    private void getInputData()
    {
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
        mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
        mPreFilePath = (String) mTransferData.getValueByName("FilePath");
        System.out.println(mPreFilePath);
    }

    /**
     * 校验传输数据
     *
     * @return boolean
     */
    private boolean checkData()
    {
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "无操作员信息，请重新登录!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mTransferData == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "无导入文件信息，请重新导入!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            FileName = (String) mTransferData.getValueByName("FileName");
            System.out.println("FileName: " + FileName);
        }
        return true;
    }

    /**
     * 得到生成文件路径
     *
     * @return boolean
     */
    private boolean getFilePath()
    {
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("WiiXmlImportDir");
        if (!tLDSysVarDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "缺少文件导入路径!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            FilePath = tLDSysVarDB.getSysVarValue();
        }

        return true;
    }

    /**
     * 检验文件是否存在
     *
     * @return boolean
     */
    private boolean checkXmlFileName()
    {
        File tFile = new File(XmlFileName);
        if (!tFile.exists())
        {
            LDSysVarDB tLDSysVarDB = new LDSysVarDB();
            tLDSysVarDB.setSysVar("WiiXmlImportDir");
            if (!tLDSysVarDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = "缺少文件导入路径!";
                this.mErrors.addOneError(tError);
                return false;
            }
            else
            {
                FilePath = tLDSysVarDB.getSysVarValue();
            }

            File tFile1 = new File(FilePath);
            if (!tFile1.exists())
            {
                tFile1.mkdirs();
            }
            XmlFileName = FilePath + XmlFileName;
            File tFile2 = new File(XmlFileName);
            if (!tFile2.exists())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = "请上传相应的数据文件到指定路径" + FilePath + "!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        //得到批次号
        XMLPathTool tXPT = new XMLPathTool(XmlFileName);
        if (!getBatchNo(tXPT))
        {
            return false;
        }

        return true;
    }

    /**
     * 检查导入配置文件是否存在
     *
     * @return boolean
     */
    private boolean checkImportConfig()
    {
        this.getFilePath();

        String filePath = mPreFilePath + FilePath;
        //String filePath = "E:/v.1.1/ui/temp/";
        File tFile1 = new File(filePath);
        if (!tFile1.exists())
        {
            //初始化创建目录
            tFile1.mkdirs();
        }

        ConfigFileName = filePath + "ClaimBankImport.xml";
        File tFile2 = new File(ConfigFileName);
        if (!tFile2.exists())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "请上传配置文件到指定路径" + FilePath + "!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 初始化上传文件
     *
     * @return boolean
     */
    private boolean initImportFile()
    {
        this.getFilePath();
        ImportFileName = mPreFilePath + FilePath + FileName;
        //ImportFileName = "E:/v.1.1/ui/temp/" + FileName;

        File tFile = new File(ImportFileName);
        if (!tFile.exists())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "TSParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "未上传文件到指定路径" + FilePath + "!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("-----导入文件");
        return true;
    }

    /**
     * 解析excel并转换成xml文件
     *
     * @return boolean
     * @throws Exception
     */
    private boolean parseVts() throws Exception
    {
        //初始化导入文件
        if (!this.initImportFile())
        {
            return false;
        }
        //检查导入配置文件是否存在
        if (!this.checkImportConfig())
        {
            return false;
        }

        ClaimBankPolVTSParser lcpvp = new ClaimBankPolVTSParser();

        if (!lcpvp.setFileName(ImportFileName))
        {
            mErrors.copyAllErrors(lcpvp.mErrors);
            return false;
        }
        if (!lcpvp.setConfigFileName(ConfigFileName))
        {
            mErrors.copyAllErrors(lcpvp.mErrors);
            return false;
        }
        //转换excel到xml
        if (!lcpvp.transform())
        {
            mErrors.copyAllErrors(lcpvp.mErrors);
            return false;
        }

        // 得到生成的XML文件名列表
        m_strDataFiles = lcpvp.getDataFiles();
        return true;
    }

    /**
     * 解析xml
     *
     * @return boolean
     */
    private boolean ParseXml()
    {
        if (!checkXmlFileName())
        {
            return false;
        }
        this.mErrors.clearErrors();

        //得到保单的传入信息
        //        XMLPathTool tXPT = new XMLPathTool(XmlFileName);
        //        NodeList nodeList = tXPT.parseN(ParsePath);

        // 转换报文格式
        boolean tFlag = DealData(XmlFileName);
        if (!tFlag)
        {
            return false;
        }

        //解析完删除XML文件
        File tFile = new File(XmlFileName);
        if (tFile.exists())
        {
            //            tXPT = null;
            if (!tFile.delete())
            {
                System.out.println("删除Ｘｍｌ文件失败！");
            }
            else
            {
                System.out.println("删除Ｘｍｌ文件成功！");
            }
        }

        return true;
    }

    /**
     * getBatchNo
     * 得到批次信息BatchNo、BPOBatchNo
     * @return boolean
     */
    private boolean getBatchNo(XMLPathTool tXPT)
    {
        try
        {
            //批次号
            mBatchNo = tXPT.parseX(ParseRootPath).getFirstChild().getNodeValue();

            if (mBatchNo == null || mBatchNo.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "TSParseGuideIn";
                tError.functionName = "ParseXml";
                tError.errorMessage = "没有获得批次号";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "TSParseGuideIn";
            tError.functionName = "getBatchNo";
            tError.errorMessage = "获取批次号信息出现异常。";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    private boolean DealData(String cXmlFileName)
    {
    	// 读取文件，转换成报文数据Docment
        Document tInXmlDoc = loadFile4Xml(cXmlFileName);
        if (tInXmlDoc == null)
        {
        	String tStrErr = "获取导入数据失败！";
            mErrors.addOneError(tStrErr);
        	return false;
        }
        // --------------------

        Element tEleRoot = tInXmlDoc.getRootElement();

        List tDataList = tEleRoot.getChild("CONTTABLE").getChildren("ROW");
        if (tDataList == null || tDataList.size() == 0)
        {
        	String tStrErr = "获取导入清单数据失败！";
            mErrors.addOneError(tStrErr);
            return false;
        }
        
        try {
        	CertifyDiskImportLog cImportLog = new CertifyDiskImportLog(mGlobalInput, mBatchNo);
        	boolean tAllFlag = true;
        	ExeSQL tExeSQL = new ExeSQL();
			for (int i = 0; i < tDataList.size(); i++)
	        {
	            // 转换保单数据
	            Element tEleData = (Element) tDataList.get(i);
	            Element tElePolicy = tEleData.getChild("POLICYTABLE");

	            String tGrpContNo = tElePolicy.getChildText("GrpContNo");
	            if(!isNULL(tGrpContNo)){
	            	String tStrErr = "保单号码不能为空！";
	                if (!cImportLog.errLog(tGrpContNo+" ", tStrErr))
	                {
	                    mErrors.copyAllErrors(cImportLog.mErrors);
	                    tAllFlag = false;
	                }
	                continue;
	            }
	            if (!checkExists(tGrpContNo))
	            {
	                String tStrErr = "保单号为【" + tGrpContNo + "】的保单未签单，或不存在该保单数据！";
	                if (!cImportLog.errLog(tGrpContNo, tStrErr))
	                {
	                    mErrors.copyAllErrors(cImportLog.mErrors);
	                    tAllFlag = false;
	                }
	                continue;
	            }

	            String tGrpName = tElePolicy.getChildText("GrpName");
	            if(!isNULL(tGrpName)){
	            	String tStrErr = "保单号为【" + tGrpContNo + "】的客户名称不能为空！";
	                if (!cImportLog.errLog(tGrpContNo, tStrErr))
	                {
	                    mErrors.copyAllErrors(cImportLog.mErrors);
	                    tAllFlag = false;
	                }
	                continue;
	            }
	            
	            if(!checkGrpContNoAndGrpName(tGrpContNo,tGrpName)){
	            	String tStrErr = "保单号为【" + tGrpContNo + "】的保单，与客户名称【"+tGrpName+"】不匹配！";
	                if (!cImportLog.errLog(tGrpContNo, tStrErr))
	                {
	                    mErrors.copyAllErrors(cImportLog.mErrors);
	                    tAllFlag = false;
	                }
	                continue;
	            }

	            String tClaimBankCode = tElePolicy.getChildText("ClaimBankCode");
	            if(!isNULL(tClaimBankCode)){
	            	String tStrErr = "保单号为【" + tGrpContNo + "】的理赔金银行编码不能为空！";
	                if (!cImportLog.errLog(tGrpContNo, tStrErr))
	                {
	                    mErrors.copyAllErrors(cImportLog.mErrors);
	                    tAllFlag = false;
	                }
	                continue;
	            }
	            
	            if(!checkExistsBankCode(tGrpContNo,tClaimBankCode)){
	            	String tStrErr = "保单号为【" + tGrpContNo + "】的理赔金银行编码【"+tClaimBankCode+"】，不是系统中已配置的银行编码！";
	                if (!cImportLog.errLog(tGrpContNo, tStrErr))
	                {
	                    mErrors.copyAllErrors(cImportLog.mErrors);
	                    tAllFlag = false;
	                }
	                continue;
	            }

	            String tClaimAccName = tElePolicy.getChildText("ClaimAccName");
	            if(!isNULL(tClaimAccName)){
	            	String tStrErr = "保单号为【" + tGrpContNo + "】的理赔金银行账户名不能为空！";
	                if (!cImportLog.errLog(tGrpContNo, tStrErr))
	                {
	                    mErrors.copyAllErrors(cImportLog.mErrors);
	                    tAllFlag = false;
	                }
	                continue;
	            }

	            String tClaimBankAccNo = tElePolicy.getChildText("ClaimBankAccNo");
	            if(!isNULL(tClaimBankAccNo)){
	            	String tStrErr = "保单号为【" + tGrpContNo + "】的理赔金银行账号不能为空！";
	                if (!cImportLog.errLog(tGrpContNo, tStrErr))
	                {
	                    mErrors.copyAllErrors(cImportLog.mErrors);
	                    tAllFlag = false;
	                }
	                continue;
	            }
	            if (!PubFun.isNumeric(tClaimBankAccNo))
	            {
	            	String tStrErr = "保单号为【" + tGrpContNo + "】的保单，理赔金银行账号【"+tClaimBankAccNo+"】不是数字！";
	                if (!cImportLog.errLog(tGrpContNo, tStrErr))
	                {
	                    mErrors.copyAllErrors(cImportLog.mErrors);
	                    tAllFlag = false;
	                }
	                continue;
	            }
	            
	            if(checkExistsClaimBankCode(tGrpContNo)){
	            	String tStrErr = "保单号为【" + tGrpContNo + "】的保单已存在理赔金账户信息！";
	                if (!cImportLog.errLog(tGrpContNo, tStrErr))
	                {
	                    mErrors.copyAllErrors(cImportLog.mErrors);
	                    tAllFlag = false;
	                }
	                continue;
	            }
	            
	            String tUpdateSQL = "update LCGrpAppnt set ClaimBankCode = '"+tClaimBankCode+"',ClaimAccName = '"+tClaimAccName+"',ClaimBankAccNo = '"+tClaimBankAccNo+"',modifydate = current date,modifytime = current time where GrpContNo = '"+tGrpContNo+"' ";
	            if(!tExeSQL.execUpdateSQL(tUpdateSQL)){
	            	String tStrErr = "保单号为【" + tGrpContNo + "】的保单导入理赔金账户信息失败！";
	                if (!cImportLog.errLog(tGrpContNo, tStrErr))
	                {
	                    mErrors.copyAllErrors(cImportLog.mErrors);
	                    tAllFlag = false;
	                }
	                continue;
	            }else{
	            	String tInfo = "保单号为【" + tGrpContNo + "】的保单导入理赔金账户信息成功！";
	            	if (!cImportLog.infoLog(tGrpContNo, tInfo))
	                {
	                    mErrors.copyAllErrors(cImportLog.mErrors);
	                    tAllFlag = false;
	                }
	            	continue;
	            }

	        }
			String tWMSQL = "update lwmission set missionprop5 = '1' where activityid = '0000012102' and missionprop1 = '"+mBatchNo+"' ";
			if(!tExeSQL.execUpdateSQL(tWMSQL)){
				String tStrErr = "导入完成后，处理批次状态失败！";
	            mErrors.addOneError(tStrErr);
	        	return false;
			}
			cImportLog.logEnd();
		} catch (Exception e) {
			// TODO 自动生成 catch 块
			e.printStackTrace();
		}

        return true;
    }

    private Document loadFile4Xml(String cXmlFileName)
    {
        String tFileName = cXmlFileName;

        Document tXmlDoc = null;
        tXmlDoc = XmlFileUtil.xmlFile2Doc(tFileName);
        if (tXmlDoc == null)
        {
            buildError("loadFile4Xml", "解析Xml文件失败。");
            return null;
        }

        return tXmlDoc;
    }

    public String getExtendFileName(String aFileName)
    {
        File tFile = new File(aFileName);
        String aExtendFileName = "";
        String name = tFile.getName();
        for (int i = name.length() - 1; i >= 0; i--)
        {
            if (i < 1)
            {
                i = 1;
            }
            if (name.substring(i - 1, i).equals("."))
            {
                aExtendFileName = name.substring(i, name.length());
                System.out.println("ExtendFileName;" + aExtendFileName);
                return aExtendFileName;
            }
        }
        return aExtendFileName;
    }

    /**
     * 字符串替换
     *
     * @param s1 String
     * @param OriginStr String
     * @param DestStr String
     * @return java.lang.String
     */
    public static String replace(String s1, String OriginStr, String DestStr)
    {
        s1 = s1.trim();
        int mLenOriginStr = OriginStr.length();
        for (int j = 0; j < s1.length(); j++)
        {
            int befLen = s1.length();
            if (s1.substring(j, j + 1) == null || s1.substring(j, j + 1).trim().equals(""))
            {
                continue;
            }
            else
            {
                if (OriginStr != null && DestStr != null)
                {
                    if (j + mLenOriginStr <= s1.length())
                    {

                        if (s1.substring(j, j + mLenOriginStr).equals(OriginStr))
                        {

                            OriginStr = s1.substring(j, j + mLenOriginStr);

                            String startStr = s1.substring(0, j);
                            String endStr = s1.substring(j + mLenOriginStr, s1.length());

                            s1 = startStr + DestStr + endStr;

                            j = j + s1.length() - befLen;
                            if (j < 0)
                            {
                                j = 0;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        return s1;
    }

    /**
     * 得到日志显示结果
     *
     * @return com.sinosoft.utility.VData
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
    }

    /**
     * Build a instance document object for function transfromNode()
     */
    private void bulidDocument()
    {
        DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
        dfactory.setNamespaceAware(true);

        try
        {
            m_doc = dfactory.newDocumentBuilder().newDocument();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * 创建错误日志
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ParseGuideIn";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        CErrors tCErrors = new CErrors();
        tCErrors.addOneError(cError);
    }
    
//  是否已有签单数据
    private boolean checkExists(String aGrpContNo)
    {
        String tStrSql = "select 1 from lcgrpcont where GrpContNo = '" + aGrpContNo + "' and appflag = '1' " ;
        String tResult = new ExeSQL().getOneValue(tStrSql);
        if ("1".equals(tResult))
        {
            return true;
        }
        return false;
    }
    
//	校验节点值是否为空
    private boolean isNULL(String aFlagValue)
    {
        if (aFlagValue == null || "".equals(aFlagValue.trim()))
        {
            return false;
        }
        return true;
    }
    
//  校验保单号码和机构名称是否匹配
    private boolean checkGrpContNoAndGrpName(String aGrpContNo,String aGrpName)
    {
        String tStrSql = "select 1 from lcgrpcont where GrpContNo = '" + aGrpContNo + "' and GrpName = '"+aGrpName+"' " ;
        String tResult = new ExeSQL().getOneValue(tStrSql);
        if ("1".equals(tResult))
        {
            return true;
        }
        return false;
    }
    
//  是否已存在该银行编码
    private boolean checkExistsBankCode(String aGrpContNo,String aClaimBankCode)
    {
        String tStrSql = "select managecom from lcgrpcont where GrpContNo = '" + aGrpContNo + "' " ;
        String tManageCom = new ExeSQL().getOneValue(tStrSql);
        if (tManageCom.length()>4)
        {
        	tManageCom = tManageCom.substring(0,4);
        }
        String tCheckBankCodeSQL = "select 1 from LDBank where (BankUniteFlag is null or BankUniteFlag<>'1') and Comcode like '" + tManageCom + "%' and bankcode = '"+aClaimBankCode+"' ";
        String tResult = new ExeSQL().getOneValue(tCheckBankCodeSQL);
        if ("1".equals(tResult))
        {
            return true;
        }
        return false;
    }
    
//  校验是否已存在理赔金账户
    private boolean checkExistsClaimBankCode(String aGrpContNo)
    {
        String tStrSql = "select 1 from lcgrpappnt where GrpContNo = '" + aGrpContNo + "' and ((ClaimBankCode is not null and ClaimBankCode != '') or (ClaimAccName is not null and ClaimAccName != '') or (ClaimBankAccNo is not null and ClaimBankAccNo != '')) " ;
        String tResult = new ExeSQL().getOneValue(tStrSql);
        if ("1".equals(tResult))
        {
            return true;
        }
        return false;
    }
}
