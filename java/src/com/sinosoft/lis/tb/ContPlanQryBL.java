package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.util.*;


/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: BL层业务逻辑处理类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @version 1.0
 * @date 2004-12-01
 */
public class ContPlanQryBL {
    /** 传入数据的容器 */
    private VData mInputData = new VData();


    /** 往前面传输数据的容器 */
    private VData mResult = new VData();
    private TransferData mTransferData = new TransferData();

    /** 数据操作字符串 */
    private String mOperate;


    /** 错误处理类 */
    public CErrors mErrors = new CErrors();


    /** 全局数据 */

    /** 业务处理相关变量 */
    private LCContPlanRiskSchema mLCContPlanRiskSchema = new
            LCContPlanRiskSchema();
    private LCContPlanDutyParamSet mLCContPlanDutyParamSet = new
            LCContPlanDutyParamSet();
    private LCContPlanDutyParamSet mDefualtLCContPlanDutyParamSet = new
            LCContPlanDutyParamSet();

    private LCDutySchema oneDuty = new LCDutySchema();
    private LCDutySet mLCDutySet = new LCDutySet();
    private LCGetSet mLCGetSet = new LCGetSet();
    private LCPremSet mLCPremSet = new LCPremSet();
    private String mGetDutyKind = "";


    // @Constructor
    public ContPlanQryBL() {}


    /**
     * 数据提交的公共方法
     * @param: cInputData 传入的数据
     *		  cOperate 数据操作字符串
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 将传入的数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        System.out.println("now in ContPlanQryBL submit");
        // 将外部传入的数据分解到本类的属性中，准备处理
        if (!getInputData()) {
            return false;
        }
        System.out.println("---getInputData---");

        // 校验传入的数据
        if (!checkData()) {
            return false;
        }

        // 根据业务逻辑对数据进行处理

        if (this.queryData() == false) {
            return false;
        }

        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();

        return true;
    }


    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData() {
        try {
            //全局变量
//      mGlobalInput.setSchema( (GlobalInput) mInputData.getObjectByObjectName(
//          "GlobalInput", 0));
//
//      mTransferData = (TransferData) mInputData.getObjectByObjectName(
//          "mTransferData", 0);
            //保单险种保险计划
            mLCContPlanRiskSchema.setSchema((LCContPlanRiskSchema) mInputData.
                                            getObjectByObjectName(
                    "LCContPlanRiskSchema", 0));
            return true;
        } catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "ProposalBL";
            tError.functionName = "checkData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            return false;

        }

    }


    /**
     * 校验传入的数据
     * @param: 无
     * @return: boolean
     */
    private boolean checkData() {
        if (mLCContPlanRiskSchema.getProposalGrpContNo() == null ||
            mLCContPlanRiskSchema.getProposalGrpContNo().equals("")
            || mLCContPlanRiskSchema.getRiskCode() == null ||
            mLCContPlanRiskSchema.getRiskCode().equals("")
            || mLCContPlanRiskSchema.getContPlanCode() == null ||
            mLCContPlanRiskSchema.getContPlanCode().equals("")
            || mLCContPlanRiskSchema.getMainRiskCode() == null ||
            mLCContPlanRiskSchema.getMainRiskCode().equals("")) {
            CError tError = new CError();
            tError.moduleName = "ContPlanQryBL";
            tError.functionName = "checkData";
            tError.errorMessage = "未获得查询保险计划必要的信息!";
            this.mErrors.addOneError(tError);
        }
        return true;
    }


    /**
     * 根据传入的团单号码，险种计划编码，险种编码，查询险种计划和默认值中该险种下的所有信息，
     * 准备到责任集合中.
     * @param: 无
     * @return: boolean
     */
    private boolean queryData() {
        LCContPlanDutyParamDB tLCContPlanDutyParamDB = new
                LCContPlanDutyParamDB();
        tLCContPlanDutyParamDB.setContPlanCode(mLCContPlanRiskSchema.
                                               getContPlanCode());
        tLCContPlanDutyParamDB.setProposalGrpContNo(mLCContPlanRiskSchema.
                getProposalGrpContNo());
        tLCContPlanDutyParamDB.setRiskCode(mLCContPlanRiskSchema.getRiskCode());
        tLCContPlanDutyParamDB.setMainRiskCode(mLCContPlanRiskSchema.
                                               getMainRiskCode());
        String sql =
                "select * from LCContPlanDutyParam where ProposalGrpContNo='"
                + mLCContPlanRiskSchema.getProposalGrpContNo() +
                "' and ContPlanCode='"
                + mLCContPlanRiskSchema.getContPlanCode()
                + "' and RiskCode='"
                + mLCContPlanRiskSchema.getRiskCode() +
                "' and MainRiskCode='"
                + mLCContPlanRiskSchema.getMainRiskCode() + "'"
                + " and PlanType in ('0','3')";
        mLCContPlanDutyParamSet = tLCContPlanDutyParamDB.executeQuery(sql);
        if (tLCContPlanDutyParamDB.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "ContPlanQryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "查询保险计划要素时失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mLCContPlanRiskSchema.getContPlanCode().equals("00")) {
            //如果不是默认值，还需要查出默认值要素
            String calFactorSql = "select CalFactor from LCContPlanDutyParam "
                                  + "where ContPlanCode='"
                                  + mLCContPlanRiskSchema.getContPlanCode()
                                  + "' and ProposalGrpContNo='"
                                  + mLCContPlanRiskSchema.getProposalGrpContNo()
                                  + "' and Riskcode='"
                                  + mLCContPlanRiskSchema.getRiskCode()
                                  + "' and MainRiskCode='"
                                  + mLCContPlanRiskSchema.getMainRiskCode() +
                                  "'"
                                  + " and PlanType in ('0','3')";
            String dutycodeSql =
                    "select distinct dutycode from LCContPlanDutyParam "
                    + "where ContPlanCode='"
                    + mLCContPlanRiskSchema.getContPlanCode()
                    + "' and ProposalGrpContNo='"
                    + mLCContPlanRiskSchema.getProposalGrpContNo()
                    + "' and Riskcode='"
                    + mLCContPlanRiskSchema.getRiskCode()
                    + "' and MainRiskCode='"
                    + mLCContPlanRiskSchema.getMainRiskCode() + "'"
                    + " and PlanType in ('0','3')";

            String dutySQL = "Select * from LCContPlanDutyParam where "
                             + "ProposalGrpContNo='"
                             + mLCContPlanRiskSchema.getProposalGrpContNo()
                             + "' and Riskcode='"
                             + mLCContPlanRiskSchema.getRiskCode()
                             + "' and ContPlanCode ='00' and CalFactor not in "
                             + "(" + calFactorSql + ") and dutycode in "
                             + "(" + dutycodeSql + ")"
                             + " and PlanType in ('0','3')";
            System.out.println("dutySQL" + dutySQL);

            mDefualtLCContPlanDutyParamSet = tLCContPlanDutyParamDB.
                                             executeQuery(dutySQL);
            mLCContPlanDutyParamSet.add(mDefualtLCContPlanDutyParamSet);

        }
        boolean needGetDuty = false;
        boolean needPremDuty = false;
        for (int i = 1; i <= mLCContPlanDutyParamSet.size(); i++) {
            if (!mLCContPlanDutyParamSet.get(i).getGetDutyCode().equals(
                    "000000")) {
                needGetDuty = true;
            }
            if (!mLCContPlanDutyParamSet.get(i).getPayPlanCode().equals(
                    "000000")) {
                needPremDuty = true;
            }
        }
        //根据险种的责任准备责任信息
        LMRiskDutyDB mLMRiskDutyDB = new LMRiskDutyDB();
        mLMRiskDutyDB.setRiskCode(mLCContPlanRiskSchema.getRiskCode());
        LMRiskDutySet mLMRiskDutySet = new LMRiskDutySet();
        mLMRiskDutySet = mLMRiskDutyDB.query();
        if (mLMRiskDutyDB.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "ContPlanQryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "查询险种责任失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mLMRiskDutySet.size() == 1) {
            oneDuty.setDutyCode(mLMRiskDutySet.get(1).getDutyCode());
//      LMDutyDB mLMDutyDB=new LMDutyDB();
//      mLMDutyDB.setDutyCode(mLMRiskDutySet.get(1).getDutyCode());
//      mLMDutyDB.getInfo();
//      oneDuty.setOperator(mLMDutyDB.getDutyName());//将险种代码放到
            if (!transPlanToDuty(oneDuty)) {
                CError tError = new CError();
                tError.moduleName = "ContPlanQryBL";
                tError.functionName = "queryData";
                tError.errorMessage = "从保险计划向责任赋值失败!";
                this.mErrors.addOneError(tError);
                return false;

            }
        } else {
            for (int i = 1; i <= mLMRiskDutySet.size(); i++) {
                LCDutySchema tempLCDutySchema = new LCDutySchema();
                tempLCDutySchema.setDutyCode(mLMRiskDutySet.get(i).getDutyCode());
                if (!transPlanToDuty(tempLCDutySchema)) {
                    CError tError = new CError();
                    tError.moduleName = "ContPlanQryBL";
                    tError.functionName = "queryData";
                    tError.errorMessage = "从保险计划向责任赋值失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (!tempLCDutySchema.getDutyCode().equals("")) {
                    mLCDutySet.add(tempLCDutySchema);
                }
                //将给付责任查询出并填充
                if (needGetDuty) {
                    LMDutyGetRelaDB tLMDutyGetRelaDB = new LMDutyGetRelaDB();
                    tLMDutyGetRelaDB.setDutyCode(mLMRiskDutySet.get(i).
                                                 getDutyCode());
                    LMDutyGetRelaSet tLMDutyGetRelaSet = tLMDutyGetRelaDB.query();
                    for (int m = 1; m <= tLMDutyGetRelaSet.size(); m++) {
                        LCGetSchema tempLCGetSchema = new LCGetSchema();
                        tempLCGetSchema.setDutyCode(tLMDutyGetRelaSet.get(m).
                                getDutyCode());
                        tempLCGetSchema.setGetDutyCode(tLMDutyGetRelaSet.get(m).
                                getGetDutyCode());
                        if (!transPlanToGet(tempLCGetSchema)) {
                            CError tError = new CError();
                            tError.moduleName = "ContPlanQryBL";
                            tError.functionName = "queryData";
                            tError.errorMessage = "从保险计划向责任赋值失败!";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                        if (!tempLCGetSchema.getDutyCode().equals("")) {
                            mLCGetSet.add(tempLCGetSchema);
                        }
                    }
                }
                //保费项填充
                if (needPremDuty) {
                    LMDutyPayRelaDB tLMDutyPayRelaDB = new LMDutyPayRelaDB();
                    tLMDutyPayRelaDB.setDutyCode(mLMRiskDutySet.get(i).
                                                 getDutyCode());
                    LMDutyPayRelaSet tLMDutyPayRelaSet = tLMDutyPayRelaDB.query();
                    for (int m = 1; m <= tLMDutyPayRelaSet.size(); m++) {
                        LCPremSchema tempLCPremSchema = new LCPremSchema();
                        tempLCPremSchema.setDutyCode(tLMDutyPayRelaSet.get(m).
                                getDutyCode());
                        tempLCPremSchema.setPayPlanCode(tLMDutyPayRelaSet.get(m).
                                getPayPlanCode());
                        if (!transPlanToPrem(tempLCPremSchema)) {
                            CError tError = new CError();
                            tError.moduleName = "ContPlanQryBL";
                            tError.functionName = "queryData";
                            tError.errorMessage = "从保险计划向责任赋值失败!";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                        if (!tempLCPremSchema.getDutyCode().equals("")) {
                            this.mLCPremSet.add(tempLCPremSchema);
                        }
                    }
                }
            }
        }
        return true;
    }

    private boolean transPlanToPrem(LCPremSchema tempLCPremSchema) {
        String PayPlanCode = tempLCPremSchema.getPayPlanCode();
        boolean dutyFlag = false;
        for (int i = 1; i <= mLCContPlanDutyParamSet.size(); i++) {
            if (mLCContPlanDutyParamSet.get(i).getPayPlanCode().equals("000000")) {
                continue;
            }
            String CalFactor = mLCContPlanDutyParamSet.get(i).getCalFactor();
            String CalFactorValue = mLCContPlanDutyParamSet.get(i).
                                    getCalFactorValue();
            String CalPayPlanCode = mLCContPlanDutyParamSet.get(i).
                                    getPayPlanCode();
            if (PayPlanCode.trim().equals(CalPayPlanCode.trim())) {
                if (tempLCPremSchema.getFieldIndex(CalFactor) == -1) {
                    mTransferData.setNameAndValue(CalFactor, CalFactorValue);
                } else {
                    tempLCPremSchema.setV(CalFactor, CalFactorValue);
                    dutyFlag = true;
                }
            }
        }
        if (!dutyFlag) {
            tempLCPremSchema.setDutyCode("");
        }
        return true;
    }

    /**
     * 填充lcGet
     * @param tempLCGetSchema LCGetSchema
     * @return boolean
     */
    private boolean transPlanToGet(LCGetSchema tempLCGetSchema) {
        String getDutyCode = tempLCGetSchema.getGetDutyCode();
        boolean dutyFlag = false;
        for (int i = 1; i <= mLCContPlanDutyParamSet.size(); i++) {
            if (mLCContPlanDutyParamSet.get(i).getGetDutyCode().equals("000000")) {
                continue;
            }
            String CalFactor = mLCContPlanDutyParamSet.get(i).getCalFactor();
            String CalFactorValue = mLCContPlanDutyParamSet.get(i).
                                    getCalFactorValue();
            String CalGetDutyCode = mLCContPlanDutyParamSet.get(i).
                                    getGetDutyCode();
            if (getDutyCode.trim().equals(CalGetDutyCode.trim())) {
                if (tempLCGetSchema.getFieldIndex(CalFactor) == -1) {
                    mTransferData.setNameAndValue(CalFactor, CalFactorValue);
                } else {
                    tempLCGetSchema.setV(CalFactor, CalFactorValue);
                    dutyFlag = true;
                }
            }
        }
        if (!dutyFlag) {
            tempLCGetSchema.setDutyCode("");
        }
        return true;
    }

    /**
     * 根据查询出来的保险计划和默认值中的要素赋值到责任中，如果该责任在计划和默认值中
     * 都没有描述，则不准备该责任
     * @param: tLCDutySchema
     * @return: boolean
     */
    private boolean transPlanToDuty(LCDutySchema tLCDutySchema) {
        // Class dutyClaass = tLCDutySchema.getClass();
        String dutycode = tLCDutySchema.getDutyCode();
        // Field[] dutyField = dutyClaass.getDeclaredFields();
        // AccessibleObject.setAccessible(dutyField, true);
        //判断该责任是否有值标记
        boolean dutyFlag = false;
        for (int i = 1; i <= mLCContPlanDutyParamSet.size(); i++) {
            String CalFactor = mLCContPlanDutyParamSet.get(i).getCalFactor();
            String CalFactorValue = mLCContPlanDutyParamSet.get(i).
                                    getCalFactorValue();
            String CalDutyCode = mLCContPlanDutyParamSet.get(i).getDutyCode();
            //System.out.println("dutyCode:"+dutycode+"  caldutycode:"+CalDutyCode+"  calfactor:"+CalFactor+"  value:"+CalFactorValue);
            if (dutycode.trim().equals(CalDutyCode.trim())) {
                if (tLCDutySchema.getFieldIndex(CalFactor) == -1) {
                    mTransferData.setNameAndValue(CalFactor, CalFactorValue);
                    /** 如果录入的保额 */
                    if (StrTool.cTrim(CalFactor).equals("DutyAmnt") &&
                        CalFactorValue != null && !CalFactorValue.equals("") &&
                        Double.parseDouble(CalFactorValue) > 0) {
                        tLCDutySchema.setAmnt(CalFactorValue);
                    }
                } else {
                    tLCDutySchema.setV(CalFactor, CalFactorValue);
                    dutyFlag = true;
//        for (int j = 0; j < dutyField.length; j++) {
//          Field f = dutyField[j];
//          Class type = f.getType();
//          String name = f.getName();
//          String typeName = type.getName();
//          if (name.trim().equals(CalFactor.trim())) {
//
//            try {
//              switch (transType(typeName)) {
//                case 3:
//                  f.setDouble(tLCDutySchema, Double.parseDouble(CalFactorValue));
//
//                  break;
//                case 5:
//                  f.setInt(tLCDutySchema, Integer.parseInt(CalFactorValue));
//
//                  break;
//                case 93:
//                  f.set(tLCDutySchema, CalFactorValue);
//                  break;
//                default:
//                  f.set(tLCDutySchema, CalFactorValue);
//              }
//              break;
//            }
//            catch (Exception e) {
//              CError tError = new CError();
//              tError.moduleName = "ContPlanQryBL";
//              tError.functionName = "queryData";
//              tError.errorMessage = "从保险计划给责任赋值时失败!";
//              this.mErrors.addOneError(tError);
//              return false;
//            }
//          }
//        } //end dutyField.length
                    //System.out.println("dutycode:"+dutycode+"  dutyFlag:"+dutyFlag+CalDutyCode+"  calfactor:"+CalFactor+"  value:"+CalFactorValue);
                }
            }
        }
        if (!dutyFlag) {
            tLCDutySchema.setDutyCode("");
        }
        //当计算规则为统一费率时，不将费率带到界面，在计算中重新到保险计划要素中取
        if (StrTool.compareString(tLCDutySchema.getCalRule(), "1")) {
            tLCDutySchema.setFloatRate(0.0);
        }
        return true;
    }


    /**
     * 根据业务逻辑对数据进行处理
     * @param: 无
     * @return: void
     */
    private void prepareOutputData() {

        mInputData.clear();

        mResult.clear();
        if (mLCDutySet.size() > 0) {
            mResult.add(mLCDutySet);
        } else {
            //System.out.println(oneDuty.encode());
            mResult.add(oneDuty);
            //mResult.add(mGetDutyKind);
        }
        if (this.mLCGetSet.size() > 0) {
            this.mResult.add(this.mLCGetSet);
        }
        if (this.mLCPremSet.size() > 0) {
            this.mResult.add(this.mLCPremSet);
        }
    }


    /**
     * 取得操作结果
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }

    public TransferData getTransferData() {
        return mTransferData;
    }

    public static void main(String args[]) {
//    String dutySQL="Select distinct dutycode from LCContPlanDutyParam where ContPlanCode='"
//        +"C"+"' and GrpContNo='"
//        +"86110020040990000041"+"' and Riskcode='"+"211672"+"'";
//     ExeSQL tExeSQL=new ExeSQL();
//     SSRS tSSRS=tExeSQL.execSQL(dutySQL);
//     String dutycode[]=tSSRS.getRowData(1);
//     System.out.println(tSSRS.getAllData());
        LCContPlanRiskSchema mmLCContPlanRiskSchema = new LCContPlanRiskSchema();
        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86";
        mGlobalInput.Operator = "001";
        mmLCContPlanRiskSchema.setContPlanCode("A");
        mmLCContPlanRiskSchema.setProposalGrpContNo("140110000000143");
        mmLCContPlanRiskSchema.setRiskCode("211672");
        mmLCContPlanRiskSchema.setMainRiskCode("211672");
        ContPlanQryBL tContPlanQryBL = new ContPlanQryBL();
        VData cInputData = new VData();
        cInputData.add(mmLCContPlanRiskSchema);
        cInputData.add(mGlobalInput);
        tContPlanQryBL.submitData(cInputData, "");
        VData mResult = new VData();
        mResult = tContPlanQryBL.getResult();
    }

}
