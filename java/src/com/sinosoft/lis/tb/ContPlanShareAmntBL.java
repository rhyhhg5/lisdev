/**
 * <p>ClassName: ContPlanShareAmntBL.java </p>
 * <p>Description: 共用保额要素维护 </p>
 * <p>Copyright: Copyright (c) 2009 </p>
 * <p>Company: </p>
 * @author
 * @version 1.0
 * @CreateDate：2010-04-06
 */

//包名
package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;


public class ContPlanShareAmntBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LCContPlanDutyParamSet mLCContPlanDutyParamSet = new
            LCContPlanDutyParamSet();

    private MMap mMap = new MMap();

    public ContPlanShareAmntBL() {

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mInputData = cInputData;
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            return false;
        }

        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();

        System.out.println("Start ContPlanShareAmntBL Submit...");

        if (!tPubSubmit.submitData(mInputData, null)) {
            buildError("PubSubmit", "数据提交失败");
            return false;
        }

        mInputData = null;
        System.out.println("End ContPlanShareAmntBL Submit...");
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     */
    private boolean getInputData() {
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLCContPlanDutyParamSet = (LCContPlanDutyParamSet) mInputData.
                                  getObjectByObjectName(
                                          "LCContPlanDutyParamSet", 0);
        if (mGlobalInput == null) {
            buildError("getInputData", "获取登录信息失败");
            return false;
        }

        if (mLCContPlanDutyParamSet == null) {
            buildError("getInputData", "获取要素信息失败");
            return false;
        }

        return true;
    }

    private boolean checkData() {
        if (mLCContPlanDutyParamSet.size() <= 0) {
            buildError("getInputData", "获取要素信息失败");
            return false;
        }
        String tCalValue = "";

        for (int i = 1; i <= mLCContPlanDutyParamSet.size(); i++) {
            LCContPlanDutyParamSchema tLCContPlanDutyParamSchema =
                    mLCContPlanDutyParamSet.get(i);
            if (tLCContPlanDutyParamSchema.getCalFactorValue() == null ||
                "".equals(tLCContPlanDutyParamSchema.getCalFactorValue())) {
                continue;
            } else {
                double tCalFactorValue = 0.0;
                try {
                    tCalFactorValue = Double.parseDouble(
                            tLCContPlanDutyParamSchema.getCalFactorValue());
                } catch (Exception ex) {
                    buildError("getInputData", "共用保额要素值不是数字");
                    return false;
                }

                if (tCalFactorValue < 0) {
                    buildError("getInputData", "共用保额要素值不能小于0");
                    return false;
                }

                if (i == 1) {
                    tCalValue = tLCContPlanDutyParamSchema.getCalFactorValue();
                } else {
                    if (!tCalValue.equals(tLCContPlanDutyParamSchema.
                                          getCalFactorValue())) {
                        buildError("getInputData", "共用保额要素值不一致");
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        String tEdorNo = PubFun1.CreateMaxNo("SAMODIFY", mGlobalInput.Operator);

        //正常保单的维护
        LCContPlanDutyParamSet tLCContPlanDutyParamSet = new
                LCContPlanDutyParamSet();
        //退保保单的维护
        LBContPlanDutyParamSet tLBContPlanDutyParamSet = new
                LBContPlanDutyParamSet();
        //备份数据
        LPContPlanDutyParamSet tLPContPlanDutyParamSet = new
                LPContPlanDutyParamSet();

        for (int i = 1; i <= mLCContPlanDutyParamSet.size(); i++) {
            Reflections tReflections = new Reflections();

            LCContPlanDutyParamSchema tLBContPlanDutyParamSchema = new
                    LCContPlanDutyParamSchema();
            LBContPlanDutyParamSchema tLBBContPlanDutyParamSchema = new
                    LBContPlanDutyParamSchema();

            LPContPlanDutyParamSchema tLPContPlanDutyParamSchema = new
                    LPContPlanDutyParamSchema();

            LCContPlanDutyParamSchema tLCContPlanDutyParamSchema =
                    mLCContPlanDutyParamSet.get(i);

            //查询该保障计划下是否有该责任
            LCContPlanDutyParamDB tLCContPlanDutyParamDB = new
                    LCContPlanDutyParamDB();
            tLCContPlanDutyParamDB.setGrpContNo(tLCContPlanDutyParamSchema.
                                                getGrpContNo());
            tLCContPlanDutyParamDB.setRiskCode(tLCContPlanDutyParamSchema.
                                               getRiskCode());
            tLCContPlanDutyParamDB.setDutyCode(tLCContPlanDutyParamSchema.
                                               getDutyCode());
            tLCContPlanDutyParamDB.setMainRiskCode(tLCContPlanDutyParamSchema.
                    getMainRiskCode());
            tLCContPlanDutyParamDB.setContPlanCode(tLCContPlanDutyParamSchema.
                    getContPlanCode());

            LCContPlanDutyParamSet aLCContPlanDutyParamSet =
                    tLCContPlanDutyParamDB.query();
            LBContPlanDutyParamSet aLBContPlanDutyParamSet = new
                    LBContPlanDutyParamSet();
            LBContPlanDutyParamDB tLBContPlanDutyParamDB = new
                    LBContPlanDutyParamDB();

            if (aLCContPlanDutyParamSet.size() <= 0) {
                //查询退保保单下该保障计划下是否有该责任
                tLBContPlanDutyParamDB.setGrpContNo(tLCContPlanDutyParamSchema.
                        getGrpContNo());
                tLBContPlanDutyParamDB.setRiskCode(tLCContPlanDutyParamSchema.
                        getRiskCode());
                tLBContPlanDutyParamDB.setDutyCode(tLCContPlanDutyParamSchema.
                        getDutyCode());
                tLBContPlanDutyParamDB.setMainRiskCode(
                        tLCContPlanDutyParamSchema.
                        getMainRiskCode());
                tLBContPlanDutyParamDB.setContPlanCode(
                        tLCContPlanDutyParamSchema.
                        getContPlanCode());
                aLBContPlanDutyParamSet = tLBContPlanDutyParamDB.query();

                if (aLBContPlanDutyParamSet.size() <= 0) {
                    buildError("dealData", "该保障计划下没有该责任");
                    return false;
                }
            }
            if (aLCContPlanDutyParamSet.size() > 0) {
                //修改正常保单的
                tLBContPlanDutyParamSchema = aLCContPlanDutyParamSet.get(1);
                tLCContPlanDutyParamDB.setSchema(tLCContPlanDutyParamSchema);
                if (tLCContPlanDutyParamDB.getInfo()) {
                    //已经有了
                    tLBContPlanDutyParamSchema = tLCContPlanDutyParamDB.
                                                 getSchema();
                    tReflections.transFields(tLPContPlanDutyParamSchema,
                                             tLBContPlanDutyParamSchema);
                    tLPContPlanDutyParamSchema.setEdorNo(tEdorNo);
                    tLPContPlanDutyParamSchema.setEdorType("SA");
                    tLPContPlanDutyParamSet.add(tLPContPlanDutyParamSchema);

                    tLBContPlanDutyParamSchema.setCalFactorValue(
                            tLCContPlanDutyParamSchema.getCalFactorValue());
                    tLBContPlanDutyParamSchema.setRemark(
                            tLCContPlanDutyParamSchema.getRemark());
                } else {
                    //新增的
                    tLBContPlanDutyParamSchema.setCalFactor("ShareAmnt");
                    tLBContPlanDutyParamSchema.setCalFactorType(
                            tLCContPlanDutyParamSchema.getCalFactorType());
                    tLBContPlanDutyParamSchema.setCalFactorValue(
                            tLCContPlanDutyParamSchema.getCalFactorValue());
                    tLBContPlanDutyParamSchema.setRemark(
                            tLCContPlanDutyParamSchema.
                            getRemark());
                    tLBContPlanDutyParamSchema.setPlanType(
                            tLCContPlanDutyParamSchema.getPlanType());
                    tLBContPlanDutyParamSchema.setPayPlanCode(
                            tLCContPlanDutyParamSchema.getPayPlanCode());
                    tLBContPlanDutyParamSchema.setGetDutyCode(
                            tLCContPlanDutyParamSchema.getGetDutyCode());
                    tLBContPlanDutyParamSchema.setInsuAccNo(
                            tLCContPlanDutyParamSchema.getInsuAccNo());
                    tLBContPlanDutyParamSchema.setOrder(0);
                }
                tLCContPlanDutyParamSet.add(tLBContPlanDutyParamSchema);
            } else {
                //处理退保保单的
                tLBBContPlanDutyParamSchema = aLBContPlanDutyParamSet.get(1);
                tLBContPlanDutyParamDB.setSchema(tLBBContPlanDutyParamSchema);
                if (tLBContPlanDutyParamDB.getInfo()) {
                    //已经有了
                    tLBBContPlanDutyParamSchema = tLBContPlanDutyParamDB.
                                                  getSchema();
                    tReflections.transFields(tLPContPlanDutyParamSchema,
                                             tLBBContPlanDutyParamSchema);
                    tLPContPlanDutyParamSchema.setEdorNo(tEdorNo);
                    tLPContPlanDutyParamSchema.setEdorType("SA");
                    tLPContPlanDutyParamSet.add(tLPContPlanDutyParamSchema);

                    tLBBContPlanDutyParamSchema.setCalFactorValue(
                            tLCContPlanDutyParamSchema.getCalFactorValue());
                    tLBBContPlanDutyParamSchema.setRemark(
                            tLCContPlanDutyParamSchema.getRemark());
                } else {
                    //新增
                    tLBBContPlanDutyParamSchema.setCalFactor("ShareAmnt");
                    tLBBContPlanDutyParamSchema.setCalFactorType(
                            tLCContPlanDutyParamSchema.getCalFactorType());
                    tLBBContPlanDutyParamSchema.setCalFactorValue(
                            tLCContPlanDutyParamSchema.getCalFactorValue());
                    tLBBContPlanDutyParamSchema.setRemark(
                            tLCContPlanDutyParamSchema.
                            getRemark());
                    tLBBContPlanDutyParamSchema.setPlanType(
                            tLCContPlanDutyParamSchema.getPlanType());
                    tLBBContPlanDutyParamSchema.setPayPlanCode(
                            tLCContPlanDutyParamSchema.getPayPlanCode());
                    tLBBContPlanDutyParamSchema.setGetDutyCode(
                            tLCContPlanDutyParamSchema.getGetDutyCode());
                    tLBBContPlanDutyParamSchema.setInsuAccNo(
                            tLCContPlanDutyParamSchema.getInsuAccNo());
                    tLBBContPlanDutyParamSchema.setOrder(0);
                }
                tLBContPlanDutyParamSet.add(tLBBContPlanDutyParamSchema);
            }
        }

        if (tLPContPlanDutyParamSet.size() > 0) {
            mMap.put(tLPContPlanDutyParamSet, "INSERT");
        }
        if (tLCContPlanDutyParamSet.size() > 0) {
            mMap.put(tLCContPlanDutyParamSet, "DELETE&INSERT");
        }
        if (tLBContPlanDutyParamSet.size() > 0) {
            mMap.put(tLBContPlanDutyParamSet, "DELETE&INSERT");
        }

        return true;
    }

    private boolean prepareOutputData() {
        System.out.println("prepareOutputData()...");
        try {
            mInputData.clear();
            mInputData.add(mMap);
            mResult.add(mMap);
        } catch (Exception ex) {
            buildError("prepareOutputData", "在准备往后层处理所需要的数据时出错。");
            return false;
        }

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }


    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "ContPlanShareAmntBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }


    public static void main(String[] args) {

    }
}
