package com.sinosoft.lis.tb;

import java.lang.*;
import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class CreatBonusNotice
{
    private GlobalInput mGlobalInput=new GlobalInput();
    private String mFiscalYear="";
    public int COUNT=100;
    public CErrors mErrors=new CErrors();
    public CreatBonusNotice()
    {
    }
    public static void main(String[] args)
    {
        CreatBonusNotice creatBonusNotice1 = new CreatBonusNotice();
        GlobalInput tGI=new GlobalInput();
        String year="2003";
        VData tVData=new VData();
        tVData.add(tGI);
        tVData.add(year);
        creatBonusNotice1.submitData(tVData);
    }

    public boolean submitData(VData cInputData)
    {
        if (!getInputData(cInputData))
                return false;

        if(dealData()==false)
        {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = ((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        mFiscalYear=((String)cInputData.getObjectByObjectName("String",0));
        if(mGlobalInput==null||mFiscalYear==null)
        {
            CError.buildErr(this, "没有传入数据!");
            return false;
        }
        if(mFiscalYear.equals(""))
        {
            CError.buildErr(this, "会计年度必须录入!");
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        String strSQL="select count(*) from LOBonusPol where BonusFlag='0' and FiscalYear='"+mFiscalYear+"' ";
        //String strSQL="select count(*) from LOBonusPol where polno not in (select otherno from LOPRTManager where code='30' ) and FiscalYear='"+mFiscalYear+"' ";
        SSRS tSSRS=tExeSQL.execSQL(strSQL);
        String strCount=tSSRS.GetText(1,1);
        int SumCount = Integer.parseInt(strCount);
        if(SumCount==0)
        {
            return true;
        }
        int Num=1;
        strSQL="select PolNo from LOBonusPol where BonusFlag='0' and FiscalYear='"+mFiscalYear+"' ";
        // strSQL="select PolNo from LOBonusPol where polno not in (select otherno from LOPRTManager where code='30' ) and FiscalYear='"+mFiscalYear+"' ";
        String tPolNo="";
        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSchema tLCPolSchema = new LCPolSchema();
        LOBonusPolDB tLOBonusPolDB=new LOBonusPolDB();
        while(Num<=SumCount)
        {
            tExeSQL = new ExeSQL();
            tSSRS=tExeSQL.execSQL(strSQL,Num,COUNT);
            for( int i = 1; i <=tSSRS.getMaxRow(); i++ )
            {
                if(dealPolPrintData(tSSRS.GetText(i,1))==false)
                    return false;
            }
            Num=Num+COUNT;
        }
        return true;
    }

    /**
     *
     * @param tPolNo
     * @return
     */
    public boolean dealPolPrintData(String tPolNo)
    {
        String strSQL="select AgentCode,ManageCom,Operator from LCPol where PolNo='"+tPolNo+"' ";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS=tExeSQL.execSQL(strSQL);
        if(tSSRS==null||tSSRS.getMaxRow()==0)
        {
//            CError.buildErr(this, "没有找到保单号："+tPolNo+"的数据");
//            return false;
            return true;
        }

        //如果有未打印的红利通知书在，返回
        LOPRTManagerDB qLOPRTManagerDB=new LOPRTManagerDB();
        qLOPRTManagerDB.setCode(PrintManagerBL.CODE_BONUSPAY);
        qLOPRTManagerDB.setOtherNo(tPolNo);
        qLOPRTManagerDB.setOtherNoType("00");
        qLOPRTManagerDB.setStateFlag("0");
        LOPRTManagerSet qLOPRTManagerSet=qLOPRTManagerDB.query();
        if(qLOPRTManagerSet.size()>0)
            return true;
        try
        {
            String AgentCode=tSSRS.GetText(1,1);
            String ManageCom=tSSRS.GetText(1,2);
            String Operator=tSSRS.GetText(1,3);
            LOPRTManagerSchema tLOPRTManagerSchema=new LOPRTManagerSchema();

            String tLimit=PubFun.getNoLimit(ManageCom);
            String prtSeqNo=PubFun1.CreateMaxNo("PRTSEQNO",tLimit);
            tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            tLOPRTManagerSchema.setOtherNo(tPolNo);
            tLOPRTManagerSchema.setOtherNoType("00");
            tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_BONUSPAY);
            tLOPRTManagerSchema.setManageCom(ManageCom);
            tLOPRTManagerSchema.setAgentCode(AgentCode);
            tLOPRTManagerSchema.setReqCom(ManageCom);
            tLOPRTManagerSchema.setReqOperator(Operator);
            tLOPRTManagerSchema.setPrtType("0");
            tLOPRTManagerSchema.setStateFlag("0");
            tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            LOPRTManagerDB tLOPRTManagerDB=new LOPRTManagerDB();
            tLOPRTManagerDB.setSchema(tLOPRTManagerSchema);
            if(tLOPRTManagerDB.insert()==false)
            {
                CError.buildErr(this, "打印管理表插入失败:"+tLOPRTManagerDB.mErrors.getFirstError());
                return false;
            }
        }
        catch(Exception ex)
        {
            CError.buildErr(this, "发生错误:"+ex);
            return false;
        }

        return true;
    }
}