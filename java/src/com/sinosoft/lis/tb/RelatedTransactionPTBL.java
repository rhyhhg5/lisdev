package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCGrpAppntDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCGrpAppntSchema;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.vschema.LCGrpAppntSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class RelatedTransactionPTBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private GlobalInput tGI = new GlobalInput();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	private MMap map = new MMap();

	/** 统一更新日期 */
	private String mCurrentDate = PubFun.getCurrentDate();

	/** 统一更新时间 */
	private String mCurrentTime = PubFun.getCurrentTime();

	/** 数据操作字符串 */
	private String mOperate;
	
	private LCGrpAppntSet mLCGrpAppntSet = new LCGrpAppntSet();
	private LCAppntSet mLCAppntSet = new LCAppntSet();
	private String sdate;
	private String edate;
	
	
	public boolean submitData(VData cInputData){
        mInputData = (VData) cInputData.clone();
        
		if(!getInputData()){
			return false;
		}
		if(!checkData()){
			return false;
		}
		if(!dealData()){
			CError tError = new CError();
			tError.moduleName = "RelatedTransactionPTBL";
			tError.functionName = "dealData";
			tError.errorMessage = "数据处理失败RelatedTransactionPTBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}
		if(!prepareOutputData()){
			return false;
		}
		PubSubmit ps = new PubSubmit();
		if(!ps.submitData(mInputData, "UPDATE")){
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}
		return true;
	}
	
	private boolean getInputData(){
		try{
			sdate = (String)mInputData.getObject(1);
			edate = (String)mInputData.getObject(2);
			if(sdate==null || "".equals(sdate) || edate==null || "".equals(edate)){
				CError tError = new CError();
				tError.moduleName = "RelatedTransactionPTBL";
				tError.functionName = "getInputData";
				tError.errorMessage = "获取数据失败RelatedTransactionPTBL-->getInputData!";
				this.mErrors.addOneError(tError);
				return false;
			}
			System.out.println(sdate);
			System.out.println(edate);
			return true;
		}catch(Exception ex){
			CError tError = new CError();
            tError.moduleName = "RelatedTransactionPTBL";
            tError.functionName = "getInputData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            return false;
		}
	}
	
	private boolean checkData(){
		return true;
	}
	
	private boolean dealData(){
		
		int n = PubFun.calInterval3(sdate, edate, "D");
		String d = sdate;
		for(int i = 1;i<=n;i++){
			deal(d);
			d = PubFun.getLastDate(d, 1, "D");
		}
		return true;
	}
	
	private boolean prepareOutputData(){
		try {
			mInputData.add(map);
		}catch (Exception ex){
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "RelatedTransactionPTBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "准备传输数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	public VData getResult(){
        return mResult;
    }
	
	public boolean deal(String date){
		String sql = "select a.grpcontno as r,e.relatedtype as t "
				+ "from lcgrpcont a,lcgrpaddress c,lcgrpappnt d,ldrelatedparty e "
				+ "where a.grpcontno=d.grpcontno "
				+ "and a.appntno=c.customerno "
				+ "and a.addressno=c.addressno "
				+ "and e.relatedtype='1' "
				+ "and c.linkman1 = e.name "
				+ "and e.idno=d.idno "
				+ "and a.appflag='1' "
				+ "and a.stateflag='1' "
				+ "and d.Relatedtags is null "
				+ "and a.signdate='"+date+"' "
				+ "union "
				+ "select a.grpcontno as r,e.relatedtype as t "
				+ "from lcgrpcont a,lcgrpaddress c,lcgrpappnt d,ldrelatedparty e "
				+ "where a.grpcontno=d.grpcontno "
				+ "and a.appntno=c.customerno "
				+ "and a.addressno=c.addressno "
				+ "and e.relatedtype='2' "
				+ "and d.name like ('%'||e.name||'%') "
				+ "and a.appflag='1' "
				+ "and a.stateflag='1' "
				+ "and d.Relatedtags is null "
				+ "and a.signdate='"+date+"' "
				+ "order by r,t "
				+ "with ur"
				;



		SSRS ssrs = new ExeSQL().execSQL(sql);
		if(ssrs.getMaxRow()>0){
			for(int i=1;i<=ssrs.getMaxRow();i++){
				LCGrpAppntSet tLCGrpAppntSet = new LCGrpAppntSet();
				LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
				LCGrpAppntSchema tLCGrpAppntSchema = new LCGrpAppntSchema();
				tLCGrpAppntDB.setGrpContNo(ssrs.GetText(i, 1));
				tLCGrpAppntSet = tLCGrpAppntDB.query();
				if(tLCGrpAppntSet.size()<1){
					CError tError = new CError();
					tError.moduleName = "RelatedTransactionPTBL";
					tError.functionName = "dealData";
					tError.errorMessage = "数据查询失败RelatedTransactionPTBL-->deal!";
					this.mErrors.addOneError(tError);
					return false;
				}else{
					tLCGrpAppntSchema = tLCGrpAppntSet.get(1);
					tLCGrpAppntSchema.setRelatedTags(ssrs.GetText(i, 2));
					tLCGrpAppntSchema.setModifyDate(mCurrentDate);
					tLCGrpAppntSchema.setModifyTime(mCurrentTime);
					mLCGrpAppntSet.add(tLCGrpAppntSchema);
				}
				
			}
			map.put(this.mLCGrpAppntSet,"UPDATE");
		}
		//关联交易-个单
		String sql1="select a.contno,c.relatedtype "
				+ " from lccont a,lcappnt b,ldrelatedparty c "
				+ " where a.contno=b.contno "
				+ " and b.idno=c.idno "
				+ " and b.appntname=c.name "
				+ " and a.signdate='"+date+"' "
				+ " and a.appflag='1' "
				+ " and a.stateflag='1' "
				+ " and a.conttype='1' "
				+ " and c.relatedtype='1' "
				+ " and b.Relatedtags is null "
				+ " with ur ";
		SSRS tSSRS = new ExeSQL().execSQL(sql1);
		if(tSSRS.getMaxRow()>0){
			for(int i=1;i<=tSSRS.getMaxRow();i++){
				LCAppntSet tLCAppntSet = new LCAppntSet();
				LCAppntDB tLCAppntDB = new LCAppntDB();
				tLCAppntDB.setContNo(tSSRS.GetText(i, 1));
				LCAppntSchema tLCAppntSchema = new LCAppntSchema();
				tLCAppntSet=tLCAppntDB.query();
				if(tLCAppntSet.size()<1){
					CError tError = new CError();
					tError.moduleName = "RelatedTransactionPTBL";
					tError.functionName = "dealData";
					tError.errorMessage = "数据查询失败RelatedTransactionPTBL-->dealData!";
					this.mErrors.addOneError(tError);
					return false;
				}else{
					tLCAppntSchema=tLCAppntSet.get(1);
					tLCAppntSchema.setRelatedTags(tSSRS.GetText(i, 2));
					tLCAppntSchema.setModifyDate(mCurrentDate);
					tLCAppntSchema.setModifyTime(mCurrentTime);
					mLCAppntSet.add(tLCAppntSchema);
				}
			}
			map.put(this.mLCAppntSet, "UPDATE");
		}
		
		return true;
	}
}
