/**
 * Copyright ? 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.tb;

import java.sql.Connection;

import com.sinosoft.lis.vdb.LCPayRuleFactoryDBSet;
import com.sinosoft.lis.vdb.LCPayRuleParamsDBSet;
import com.sinosoft.lis.vschema.LCPayRuleFactorySet;
import com.sinosoft.lis.vschema.LCPayRuleParamsSet;
import com.sinosoft.utility.*;


/**
 * 保障计划要素后台提交
 * <p>Title: </p>
 * <p>Description: 根据操作类型，进行新增、删除、修改操作 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SINOSOFT</p>
 * @author ZHUXF
 * @version 1.0
 */
public class LCPayRuleFactoryBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    /** 数据操作字符串 */
    private String mOperate;
    public LCPayRuleFactoryBLS()
    {
    }

    public static void main(String[] args)
    {
    }


    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start LCPayRuleFactoryBLS Submit...");
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            tReturn = saveLCPayRule(cInputData);
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            tReturn = deleteLCPayRule(cInputData);
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            tReturn = updateLCPayRule(cInputData);
        }
        if (tReturn)
            System.out.println(" sucessful");
        else
            System.out.println("Save failed");
        System.out.println("End LCPayRuleFactoryBLS Submit...");
        return tReturn;
    }


    /**
     * 新增处理
     * @param mInputData VData
     * @return boolean
     */
    private boolean saveLCPayRule(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPayRuleFactoryBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LCPayRuleFactoryDBSet tNewLCPayRuleFactoryDBSet = new
                    LCPayRuleFactoryDBSet(conn);
            LCPayRuleFactoryDBSet tOldLCPayRuleFactoryDBSet = new
                    LCPayRuleFactoryDBSet(conn);
            //获得删除数据set和新增数据set
            tNewLCPayRuleFactoryDBSet.set((LCPayRuleFactorySet) mInputData.
                                           get(1));
            tOldLCPayRuleFactoryDBSet.set((LCPayRuleFactorySet) mInputData.
                                           get(3));
            //删除旧有数据
            if (!tOldLCPayRuleFactoryDBSet.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tOldLCPayRuleFactoryDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LCPayRuleFactoryBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            //插入新增数据
            if (!tNewLCPayRuleFactoryDBSet.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tNewLCPayRuleFactoryDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LCPayRuleFactoryBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            LCPayRuleParamsDBSet tNewLCPayRuleParamsDBSet = new
                    LCPayRuleParamsDBSet(conn);
            LCPayRuleParamsDBSet tOldLCPayRuleParamDBSet = new
                    LCPayRuleParamsDBSet(conn);
            //获得删除数据set和新增数据set
            tNewLCPayRuleParamsDBSet.set((LCPayRuleParamsSet) mInputData.get(2));
            tOldLCPayRuleParamDBSet.set((LCPayRuleParamsSet) mInputData.get(4));
            //删除旧有数据
            if (!tOldLCPayRuleParamDBSet.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tOldLCPayRuleParamDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LCPayRuleFactoryBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            //插入新增数据
            if (!tNewLCPayRuleParamsDBSet.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tNewLCPayRuleParamsDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LCPayRuleFactoryBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPayRuleFactoryBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }


    /**
     * 删除处理
     * @param mInputData VData
     * @return boolean
     */
    private boolean deleteLCPayRule(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Del...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPayRuleFactoryBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Del 删除...");

            LCPayRuleFactoryDBSet tOldLCPayRuleFactoryDBSet = new
                    LCPayRuleFactoryDBSet(conn);
            //获得删除数据set
            tOldLCPayRuleFactoryDBSet.set((LCPayRuleFactorySet) mInputData.
                                           get(3));
            //删除旧有数据
            if (!tOldLCPayRuleFactoryDBSet.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tOldLCPayRuleFactoryDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LCPayRuleFactoryBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            LCPayRuleParamsDBSet tOldLCPayRuleParamsDBSet = new
                    LCPayRuleParamsDBSet(conn);
            //获得删除数据set
            tOldLCPayRuleParamsDBSet.set((LCPayRuleParamsSet) mInputData.get(4));
            //删除旧有数据
            if (!tOldLCPayRuleParamsDBSet.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tOldLCPayRuleParamsDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LCPayRuleFactoryBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPayRuleFactoryBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }


    /**
     * 保存函数
     */
    private boolean updateLCPayRule(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "LCPayRuleFactoryBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
//            LCPayRuleDB tLCPayRuleDB = new LCPayRuleDB(conn);
//            tLCPayRuleDB.setSchema((LCPayRuleSchema) mInputData.
//                                    getObjectByObjectName(
//                    "LCPayRuleSchema", 0));
//            if (!tLCPayRuleDB.update())
//            {
//                // @@错误处理
//                this.mErrors.copyAllErrors(tLCPayRuleDB.mErrors);
//                CError tError = new CError();
//                tError.moduleName = "LCPayRuleFactoryBLS";
//                tError.functionName = "saveData";
//                tError.errorMessage = "数据保存失败!";
//                this.mErrors.addOneError(tError);
//                conn.rollback();
//                conn.close();
//                return false;
//            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPayRuleFactoryBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }
}
