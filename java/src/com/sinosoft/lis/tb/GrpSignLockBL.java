package com.sinosoft.lis.tb;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SysConst;

public class GrpSignLockBL {

	public CErrors mErrors = new CErrors();
    private boolean mflag = false;
    private Connection con;
    private String mPrtNo = "";
    MMap map = new MMap();
    private GlobalInput mGlobalInput = new GlobalInput();
    
    public GrpSignLockBL() {}
    
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCContReceiveUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public boolean submitData(VData cInputData, String cOperate) {
    	if (!cOperate.equals("DELETE")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("End getInputData");
        
        DeleteSQL(mPrtNo);
        VData vData = new VData();
        vData.add(map);
        
        PubSubmit pubsubmit = new PubSubmit();
        if (!pubsubmit.submitData(vData, "")) {
            mErrors.copyAllErrors(pubsubmit.mErrors);
            return false;
        }
        System.out.println("---End pubsubmit---");
        System.out.println("删除成功！");
    	return true;
    }
    
    private boolean getInputData(VData cInputData) {
        //获取数据的时候不需要区分打印模式
        mGlobalInput.setSchema((GlobalInput) cInputData.
                               getObjectByObjectName(
                                       "GlobalInput", 0));
        mPrtNo = (String)cInputData.getObjectByObjectName("String", 0);
        
        return true;
    }
    
    public boolean DeleteSQL(String PrtNo)
    {
        String sql = "delete from lcurgeverifylog where SerialNo = '" + PrtNo + "'";
        map.put(sql, SysConst.DELETE);
        return true;
    }
}
