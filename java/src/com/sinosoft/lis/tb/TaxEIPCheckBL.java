package com.sinosoft.lis.tb;

import org.apache.log4j.Logger;

import com.sinosoft.httpclient.dto.pty001.response.PTY001Response;
import com.sinosoft.httpclient.inf.GetPlatformCustomerNo;
import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.db.LSInsuredListDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.LSInsuredListSchema;
import com.sinosoft.lis.vschema.LDPersonSet;
import com.sinosoft.lis.vschema.LSInsuredListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class TaxEIPCheckBL {
	private Logger cLogger = Logger.getLogger(getClass());
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();
	/**存储交互错误信息*/
	private String mErrorInfo ;
	
	/** 往前面传输数据的容器 */
	private VData mResult = new VData();
	
	private GlobalInput mGlobalInput = null;
	
	private TransferData mTransferData = null;
	
	/** 传输到后台处理的容器 */
	private MMap mMap = new MMap();
	
	private VData mInputData = new VData();
	
	private TransferData mOutXML = new TransferData();
	private ExeSQL mExeSQL=new ExeSQL();
	private SSRS tSSRS = new SSRS();
	
	/** 数据操作字符串 */
	private String mOperate = "";
	
	private String mBatchNo = null;
	
	private String[] tCustomerNoList = null;
	
	public TaxEIPCheckBL(){}
	
	public boolean submitData(VData cInputData,String cOperate){
		mOperate = cOperate;
		if(!getInputData(cInputData,mOperate)){
			return false;
		}
		
		if(!checkData()){
			return false;
		}
		
		//因dealData要用客户号来生成报文，故先在此将数据提交
		mInputData.add(mMap);
		PubSubmit ps = new PubSubmit();
		if(!ps.submitData(mInputData, "")){
			buildError("submitData", "提交数据失败");
            return false;
		}
		
		//在dealData中主要用来生成报文，调用前置机接口，及处理返回报文
		if(!dealData()){
			return false;
		}
		return true;
	}
	
	private boolean getInputData(VData cInputData,String cOperate){
		mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput", 0);
		if(mGlobalInput==null){
			buildError("getInputData", "处理超时，请重新登录。");
            return false;
		}
		
		mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
		if(mTransferData==null){
			buildError("getInputData", "所需参数不完整。");
			return false;
		}
		
		mBatchNo = (String)mTransferData.getValueByName("BatchNo");
		if(mBatchNo==null || "".equals(mBatchNo)){
			buildError("getInputData", "获取批次信息失败。");
            return false;
		}
		
		return true;
	}
	
	private boolean checkData(){
		//获取批次下的被保人记录
		LSInsuredListDB tLSInsuredListDB = new LSInsuredListDB();
		tLSInsuredListDB.setBatchNo(mBatchNo);
		LSInsuredListSet tLSInsuredListSet = new LSInsuredListSet();
		tLSInsuredListSet = tLSInsuredListDB.query();
		if(tLSInsuredListSet==null || tLSInsuredListSet.size()<=0){
			buildError("checkData","批次下没有被保人信息");
			return false;
		}
		//遍历被保人记录
		//1、依据姓名，证件类型，证件号码从LDPerson中查找，看是否已存在该客户
		//a、若存在，将customerno的值传给LSInsuredList的customerno字段
		//b、不不存在，新生成一条ldperson记录，并将customerno的值传给LSInsuredList的customerno字段
		LSInsuredListSchema tLSInsuredListSchema = new LSInsuredListSchema();
		LDPersonSchema tLDPersonSchema = null;
		LDPersonSet tLDPersonSet = new LDPersonSet();
		String tCustomerNo = "",havePersonSQL = "";
		for(int i=1; i<=tLSInsuredListSet.size(); i++){
			tLSInsuredListSchema = tLSInsuredListSet.get(i);
			tCustomerNo = getCustomerNoFromLDPerson(tLSInsuredListSchema);
			havePersonSQL = "select 1 from ldperson "
								 + "where name='"+tLSInsuredListSchema.getName()+"' "
								 + "and sex='"+tLSInsuredListSchema.getSex()+"' "
								 + "and birthday='"+tLSInsuredListSchema.getBirthday()+"' "
								 + "and idtype='"+tLSInsuredListSchema.getIdType()+"' "
								 + "and idno='"+tLSInsuredListSchema.getIdNo()+"'";
			String havePerson = new ExeSQL().getOneValue(havePersonSQL);
			if(havePerson==null || "".equals(havePerson)){
				tLDPersonSchema = new LDPersonSchema();
				tLDPersonSchema.setCustomerNo(tCustomerNo);
				tLDPersonSchema.setName(tLSInsuredListSchema.getName());
				tLDPersonSchema.setBirthday(tLSInsuredListSchema.getBirthday());
				tLDPersonSchema.setSex(tLSInsuredListSchema.getSex());
				tLDPersonSchema.setIDType(tLSInsuredListSchema.getIdType());
				tLDPersonSchema.setIDNo(tLSInsuredListSchema.getIdNo());
				tLDPersonSchema.setOperator(mGlobalInput.Operator);
				tLDPersonSchema.setMakeDate(PubFun.getCurrentDate());
				tLDPersonSchema.setMakeTime(PubFun.getCurrentTime());
				tLDPersonSchema.setModifyDate(PubFun.getCurrentDate());
				tLDPersonSchema.setModifyTime(PubFun.getCurrentTime());
				tLDPersonSet.add(tLDPersonSchema);
			}
			tLSInsuredListSchema.setCustomerNo(tCustomerNo);
			tLSInsuredListSchema.setModifyDate(PubFun.getCurrentDate());
			tLSInsuredListSchema.setModifyTime(PubFun.getCurrentTime());
			mMap.put(tLSInsuredListSchema, SysConst.UPDATE);
			havePersonSQL = null;
		}
		mMap.put(tLDPersonSet, SysConst.INSERT);
		
		return true;
	}
	
	private boolean dealData(){
		//准备传送数据
		String tInsuredSQL = "select customerno "
						   + "from lsinsuredlist "
						   + "where batchno='"+mBatchNo+"'";
		tSSRS = mExeSQL.execSQL(tInsuredSQL);
		if(tSSRS!=null && tSSRS.MaxRow>0){
			tCustomerNoList = new String[tSSRS.MaxRow];
			for(int i=1; i<=tSSRS.MaxRow; i++){
				tCustomerNoList[i-1] = tSSRS.GetText(i, 1);
				String theCustomerNo = tCustomerNoList[i-1];
				//1、对每一个被保人，根据客户号(CustomerNo)从LDPerson表中查找平台返回客户号(PlatformCustomerNo)是否已有值
				String tHavePCNoSQL = "select platformcustomerno from ldperson "
									+ "where customerno='"+theCustomerNo+"'";
				String tPlatformCustomerNo = new ExeSQL().getOneValue(tHavePCNoSQL);
				//2、如果有:不用调接口，直接更新LSInsuredList表的CheckFlag字段的值为'00'
				if(tPlatformCustomerNo!=null && !"".equals(tPlatformCustomerNo)){
					String updateSQL = "update lsinsuredlist set checkflag='00',errormessage='内检成功',modifydate=current date,modifytime=current time where batchno='"+mBatchNo+"' and customerno='"+theCustomerNo+"'";
					ExeSQL tExeSQL = new ExeSQL();
					if(!tExeSQL.execUpdateSQL(updateSQL)){
						System.out.println("更新被保人内检状态失败！");
					}
				}else{//3、否则调用前置机接口
					if(!getCheckResult(theCustomerNo)){
						System.out.println("获取平台客户号失败！");
						return false;
					}
				}
				
			}
		}else{
			buildError("dealData","批次上传被保人信息获取失败！");
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 * @param cLSInsuredListSchema 
	 * @return
	 */
	private String getCustomerNoFromLDPerson(LSInsuredListSchema cLSInsuredListSchema){
		String tCustomerNo = null;
		LSInsuredListSchema tLSInsuredListSchema = cLSInsuredListSchema;
		LDPersonDB tLDPersonDB = new LDPersonDB();
		LDPersonSet tLDPersonSet = new LDPersonSet();
		//从ldperson表中获取客户号
		//查看被保人的证件类型，如是0身份证，从三要素获取客户号，否则从五要素获取
		//五要素：姓名，性别，生日，证件类型，证件号码
		//三要素：姓名，证件类型，证件号码
		if(tLSInsuredListSchema.getIdType().equals("0")){
			tLDPersonDB.setName(tLSInsuredListSchema.getName());
			tLDPersonDB.setIDType(tLSInsuredListSchema.getIdType());
			tLDPersonDB.setIDNo(tLSInsuredListSchema.getIdNo());
			tLDPersonSet = tLDPersonDB.query();
		}else{
			tLDPersonDB.setName(tLSInsuredListSchema.getName());
			tLDPersonDB.setIDType(tLSInsuredListSchema.getIdType());
			tLDPersonDB.setIDNo(tLSInsuredListSchema.getIdNo());
			tLDPersonDB.setSex(tLSInsuredListSchema.getSex());
			tLDPersonDB.setBirthday(tLSInsuredListSchema.getBirthday());
			tLDPersonSet = tLDPersonDB.query();
		}
		//查看是否获得了客户号，如有，直接获取；如无，生成新的客户号。
		if(tLDPersonSet!=null && tLDPersonSet.size()>=1){
			tCustomerNo = tLDPersonSet.get(1).getCustomerNo();
		}else{
			tCustomerNo = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
		}
		
		return tCustomerNo;
	}
	
	private void buildError(String szFunc, String szErrMsg){
        CError cError = new CError();
        cError.moduleName = "TaxEIPCheckBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
	
	public VData getResult(){
        return mResult;
    }
	
	private boolean getCheckResult(String CustomerNo){
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("CustomerNo",CustomerNo);
		tTransferData.setNameAndValue("BatchNo",mBatchNo);
		tVData.add(tTransferData);
		
		GetPlatformCustomerNo tGetPlatformCustomerNo = new GetPlatformCustomerNo();
		tGetPlatformCustomerNo.submitData(tVData, "ZBXPT");
       	return true;
	}
	
	/**
	 * 将核心与平台交互返回的信息进行存储
	 * @param args
	 */
	public void saveErrorList(PTY001Response mPTY001Response,String mErrorInfo,String mCustomerNo){
		String TransType = mPTY001Response.getHead().getTransType();
		String resultStatus = mPTY001Response.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
		String updateSQL = "update lsinsuredlist set checkflag='"+resultStatus+"',errormessage='"+mErrorInfo+"',modifydate=current date,modifytime=current time where batchno='"+mBatchNo+"' and customerno='"+mCustomerNo+"'";
		ExeSQL tExeSQL = new ExeSQL();
		if(!tExeSQL.execUpdateSQL(updateSQL)){
			System.out.println("更新信息失败--"+TransType+"--"+resultStatus);
		}
		System.out.println("更新信息成功--"+TransType+"--"+resultStatus);
	}
	
	public static void main(String[] args) {
		TaxEIPCheckBL test = new TaxEIPCheckBL();
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("BatchNo","TAX2015092111484801");
		tVData.add(tTransferData);
		test.submitData(tVData, "HQSYSBM");
	}
}
