package com.sinosoft.lis.tb;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LOMixAgentDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LOMixAgentSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCGrpContSet;


import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class CrsnanceBL {
	private static String host = "mail.sinosoft.com.cn";//发送方邮箱所在的smtp主机
	private static String sender = "******@sinosoft.com.cn";//发送方邮箱 
	private static String password = "*****";//密码
	public CErrors mErrors = new CErrors();
	private String mOperate;

	private GlobalInput mGlobalInput = new GlobalInput();
	private TransferData mTransferData = new TransferData();
	private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
	private LOMixAgentSchema mLOMixAgentSchema = new LOMixAgentSchema();
	private LCContSchema mLCContSchema = new LCContSchema();

	private LDCodeSchema mLDCodeSchema = new LDCodeSchema();

	private String mContType;
	private String mPrtNo;
	private String mContNo;
	private String mSaleChnl;
	private String mGrpAgentCom;
	private String mGrpAgentCode;
	private String mGrpAgentName;
	private String mGrpAgentIdNo;
	private String mCrsSaleChnl;
	private String mCrsBussType;
	private String mAppFlag;

	public CrsnanceBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		this.mOperate = cOperate;
		System.out.println("动作为：" + mOperate);

		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		return true;
	}

	/**
	 * 校验录入的数据是否合法
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		// 校验保单类型是否正确
		if (!mContType.equals("1") && !mContType.equals("2")) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CrsnanceBL";
			tError.functionName = "checkData";
			tError.errorMessage = "保单类型数据有误，ContType = '" + mContType
					+ "'，不符合规范!";
			this.mErrors.addOneError(tError);
			return false;
		}

		if (mOperate.equals("modify")) {
			if (mSaleChnl != null && !"".equals(mSaleChnl)) {
				LDCodeDB tLDCodeDB = new LDCodeDB();
				tLDCodeDB.setCodeType("salechnl");
				tLDCodeDB.setCode(mSaleChnl);
				if (tLDCodeDB.getInfo() == false) {
					CError tError = new CError();
					tError.moduleName = "CrsnanceBL";
					tError.functionName = "checkData";
					tError.errorMessage = "输入的销售渠道有误，Salechnl = '" + mSaleChnl
							+ "'，请修改后在进行修改!";
					this.mErrors.addOneError(tError);
					return false;
				}
				mLDCodeSchema = tLDCodeDB.getSchema();
				System.out.println("销售渠道为：" + mLDCodeSchema.getCodeName());
			}
			if (mCrsSaleChnl != null && !"".equals(mCrsSaleChnl)) {
				LDCodeDB tLDCodeDB1 = new LDCodeDB();
				tLDCodeDB1.setCodeType("crs_salechnl");
				tLDCodeDB1.setCode(mCrsSaleChnl);
				if (tLDCodeDB1.getInfo() == false) {
					CError tError = new CError();
					tError.moduleName = "CrsnanceBL";
					tError.functionName = "checkData";
					tError.errorMessage = "输入的交叉销售渠道有误，crs_salechnl = '"
							+ mCrsSaleChnl + "'，请修改后在进行修改!";
					this.mErrors.addOneError(tError);
					return false;
				}
				mLDCodeSchema = tLDCodeDB1.getSchema();
				System.out.println("交叉销售渠道为：" + mLDCodeSchema.getCodeName());
			}
			if (mCrsBussType != null && !"".equals(mCrsBussType)) {
				LDCodeDB tLDCodeDB2 = new LDCodeDB();
				tLDCodeDB2.setCodeType("crs_busstype");
				tLDCodeDB2.setCode(mCrsBussType);
				if (tLDCodeDB2.getInfo() == false) {
					CError tError = new CError();
					tError.moduleName = "CrsnanceBL";
					tError.functionName = "checkData";
					tError.errorMessage = "输入的交叉销售业务类型有误，crs_busstype = '"
							+ mCrsBussType + "'，请修改后在进行修改!";
					this.mErrors.addOneError(tError);
					return false;
				}
				mLDCodeSchema = tLDCodeDB2.getSchema();
				System.out.println("交叉销售业务类型为：" + mLDCodeSchema.getCodeName());
			}

		}

		return true;
	}

	/**
	 * dealData 处理业务数据
	 *
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean dealData() {
		// 维护销售渠道
		if (mOperate.equals("modify")) {
			if (!doSaleChnl()) {
				return false;
			}
		}
		if (mOperate.equals("insert")) {
			if (!doinsert()) {
				return false;
			}

		}
		return true;
	}

	/**
	 * getInputData 将外部传入的数据分解到本类的属性中
	 * 
	 * @param cInputData
	 *            VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean getInputData(VData cInputData) {
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));

		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		if (mTransferData == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CrsnanceBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "在接受数据时没有得到TransferData的数据!";
			this.mErrors.addOneError(tError);

			return false;
		}

		if (mTransferData.getValueByName("tContType") == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CrsnanceBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保单类型失败!";
			this.mErrors.addOneError(tError);

			return false;
		}

		mContType = (String) mTransferData.getValueByName("tContType");
		System.out.println("保单类型为：" + mContType);

		if (mTransferData.getValueByName("tPrtNo") == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CrsnanceBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保单印刷号失败!";
			this.mErrors.addOneError(tError);

			return false;
		}
		mPrtNo = (String) mTransferData.getValueByName("tPrtNo");
		System.out.println("保单印刷号为：" + mPrtNo);

		if (mTransferData.getValueByName("tContNo") == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CrsnanceBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保单号失败!";
			this.mErrors.addOneError(tError);

			return false;
		}

		mContNo = (String) mTransferData.getValueByName("tContNo");
		System.out.println("保单号为：" + mContNo);
		mSaleChnl = (String) mTransferData.getValueByName("tSaleChnl");
		mCrsSaleChnl = (String) mTransferData
				.getValueByName("tCrsSaleChnl");
		if (mOperate.equals("modify")) {
			mGrpAgentCom = (String) mTransferData
					.getValueByName("tGrpAgentCom");
			mGrpAgentCode = (String) mTransferData
					.getValueByName("tGrpAgentCode");
			mGrpAgentName = (String) mTransferData
					.getValueByName("tGrpAgentName");
			mGrpAgentIdNo = (String) mTransferData
					.getValueByName("tGrpAgentIdNo");
			mCrsSaleChnl = (String) mTransferData
					.getValueByName("tCrsSaleChnl");
			mCrsBussType = (String) mTransferData
					.getValueByName("tCrsBussType");

		}
		if (!getContMessage()) {
			return false;
		}

		return true;
	}

	// 获取保单号
	private boolean getContMessage() {
		if (mContType.equals("1")) {
			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setPrtNo(mPrtNo);
			tLCContDB.setContNo(mContNo);
			LCContSet tLCContSet = new LCContSet();
			if (tLCContDB.getInfo() == false) {
				CError tError = new CError();
				tError.moduleName = "CrsnanceBL";
				tError.functionName = "getContMessage";
				tError.errorMessage = "没有查到个单信息!";
				this.mErrors.addOneError(tError);
				return false;
			}

			tLCContSet = tLCContDB.query();
			if (tLCContSet.size() > 1) {
				CError tError = new CError();
				tError.moduleName = "CrsnanceBL";
				tError.functionName = "getContMessage";
				tError.errorMessage = "保单号不唯一!";
				this.mErrors.addOneError(tError);
				return false;
			}
			mLCContSchema = tLCContSet.get(1);
			mAppFlag = mLCContSchema.getAppFlag();

		} else if (mContType.equals("2")) {
			LCGrpContDB tLCGrpContDB = new LCGrpContDB();
			LCGrpContSet tLCGrpContSet = new LCGrpContSet();
			tLCGrpContDB.setPrtNo(mPrtNo);
			tLCGrpContDB.setGrpContNo(mContNo);
			if (tLCGrpContDB.getInfo() == false) {
				CError tError = new CError();
				tError.moduleName = "CrsnanceBL";
				tError.functionName = "getContMessage";
				tError.errorMessage = "没有查到团单信息!";
				this.mErrors.addOneError(tError);
				return false;
			}
			tLCGrpContSet = tLCGrpContDB.query();
			if (tLCGrpContSet != null && tLCGrpContSet.size() > 1) {
				CError tError = new CError();
				tError.moduleName = "CrsnanceBL";
				tError.functionName = "getContMessage";
				tError.errorMessage = "保单号不唯一!";
				this.mErrors.addOneError(tError);
				return false;
			}
			mLCGrpContSchema = tLCGrpContSet.get(1);
			mAppFlag = mLCGrpContSchema.getAppFlag();
		}
		if (mOperate.equals("insert")) {
			if (!mAppFlag.equals("1")) {
				CError tError = new CError();
				tError.moduleName = "CrsnanceBL";
				tError.functionName = "getContMessage";
				tError.errorMessage = "不是已經签发保单!";
				this.mErrors.addOneError(tError);
				return false;
			}
		}

		return true;

	}

	private boolean update() {
		System.out.println("-------------- 开始进行维护----------------------");
		if (mContType.equals("1")) {

			if (mSaleChnl != null && !"".equals(mSaleChnl))
				mLCContSchema.setSaleChnl(mSaleChnl);

			mLCContSchema.setModifyDate(PubFun.getCurrentDate());
			mLCContSchema.setModifyTime(PubFun.getCurrentTime());

			if (mGrpAgentCode != null && !"".equals(mGrpAgentCode))
				mLCContSchema.setGrpAgentCode(mGrpAgentCode);

			if (mGrpAgentCom != null && !"".equals(mGrpAgentCom))
				mLCContSchema.setGrpAgentCom(mGrpAgentCom);

			if (mGrpAgentIdNo != null && !"".equals(mGrpAgentIdNo))
				mLCContSchema.setGrpAgentIDNo(mGrpAgentIdNo);

			if (mGrpAgentName != null && !"".equals(mGrpAgentName))
				mLCContSchema.setGrpAgentName(mGrpAgentName);

			if (mCrsSaleChnl != null && !"".equals(mCrsSaleChnl))
				mLCContSchema.setCrs_SaleChnl(mCrsSaleChnl);

			if (mCrsBussType != null && !"".equals(mCrsBussType))
				mLCContSchema.setCrs_BussType(mCrsBussType);

		} else if (mContType.equals("2")) {
			if (mSaleChnl != null && !"".equals(mSaleChnl))
				mLCGrpContSchema.setSaleChnl(mSaleChnl);

			if (mGrpAgentCom != null && !"".equals(mGrpAgentCom))
				mLCGrpContSchema.setGrpAgentCom(mGrpAgentCom);

			if (mGrpAgentIdNo != null && !"".equals(mGrpAgentIdNo))
				mLCGrpContSchema.setGrpAgentIDNo(mGrpAgentIdNo);

			if (mGrpAgentName != null && !"".equals(mGrpAgentName))
				mLCGrpContSchema.setGrpAgentName(mGrpAgentName);

			if (mGrpAgentCode != null && !"".equals(mGrpAgentCode))
				mLCGrpContSchema.setGrpAgentCode(mGrpAgentCode);

			if (mCrsSaleChnl != null && !"".equals(mCrsSaleChnl))
				mLCGrpContSchema.setCrs_SaleChnl(mCrsSaleChnl);

			if (mCrsBussType != null && !"".equals(mCrsBussType))
				mLCGrpContSchema.setCrs_BussType(mCrsBussType);

			mLCGrpContSchema.setModifyDate(PubFun.getCurrentDate());
			mLCGrpContSchema.setModifyTime(PubFun.getCurrentTime());

		}
		return true;
	}

	// 维护销售渠道传值
	private boolean doSaleChnl() {
		if (!update()) {
			CError tError = new CError();
			tError.moduleName = "CrsnanceBL";
			tError.functionName = "doSaleChnl";
			tError.errorMessage = "销售渠道信息传递失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		MMap tMMap = new MMap();
		if (mContType.equals("1")) {
			tMMap.put(mLCContSchema, "UPDATE");

		} else if (mContType.equals("2")) {
			tMMap.put(mLCGrpContSchema, "UPDATE");

		}

		VData tVData = new VData();
		tVData.add(tMMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(tVData, "UPDATE")) {
			CError tError = new CError();
			tError.moduleName = "CrsnanceBL";
			tError.functionName = "PubSubmit";
			tError.errorMessage = "维护销售渠道信息失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean doinsert() {

		LOMixAgentDB tLOMixAgentDB = new LOMixAgentDB();
		
		if (mContType.equals("1")) {
			tLOMixAgentDB.setContNo(mContNo);
			tLOMixAgentDB.setGrpContNo("0000");
			tLOMixAgentDB.setContType("1");
		}
		if (mContType.equals("2")) {
			tLOMixAgentDB.setContNo("0000");
			tLOMixAgentDB.setGrpContNo(mContNo);
			tLOMixAgentDB.setContType("2");
		}
		if (tLOMixAgentDB.getInfo()) {
			tLOMixAgentDB.setSchema(tLOMixAgentDB.query().get(1));
			tLOMixAgentDB.delete();

		}
		
		if (mContType.equals("1")) {
			String mcrssalechnl=new ExeSQL().getOneValue("select Crs_salechnl from lccont where contno='"+mContNo+"'");
			mLOMixAgentSchema.setContNo(mContNo);
			mLOMixAgentSchema.setGrpContNo("0000");
			mLOMixAgentSchema.setContType(mContType);
			mLOMixAgentSchema.setSaleChnl(mcrssalechnl);
			mLOMixAgentSchema.setRemark("集团交叉提数");
			mLOMixAgentSchema.setOperator("郭忠华");
			mLOMixAgentSchema.setMakeDate(PubFun.getCurrentDate());
			mLOMixAgentSchema.setMakeTime(PubFun.getCurrentTime());
			mLOMixAgentSchema.setModifyDate(PubFun.getCurrentDate());
			mLOMixAgentSchema.setModifyTime(PubFun.getCurrentTime());

		}
		if (mContType.equals("2")) {
			String mcrssalechnl=new ExeSQL().getOneValue("select Crs_salechnl from lcgrpcont where grpcontno='"+mContNo+"'");
			mLOMixAgentSchema.setContNo("0000");
			mLOMixAgentSchema.setGrpContNo(mContNo);
			mLOMixAgentSchema.setContType(mContType);
			mLOMixAgentSchema.setSaleChnl(mcrssalechnl);
			mLOMixAgentSchema.setRemark("集团交叉提数");
			mLOMixAgentSchema.setOperator("郭忠华");
			mLOMixAgentSchema.setMakeDate(PubFun.getCurrentDate());
			mLOMixAgentSchema.setMakeTime(PubFun.getCurrentTime());
			mLOMixAgentSchema.setModifyDate(PubFun.getCurrentDate());
			mLOMixAgentSchema.setModifyTime(PubFun.getCurrentTime());

		}

		MMap tMMap = new MMap();
		tMMap.put(mLOMixAgentSchema, "INSERT");
		VData tVData = new VData();
		tVData.add(tMMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(tVData, "INSERT")) {
			CError tError = new CError();
			tError.moduleName = "CrsnanceBL";
			tError.functionName = "PubSubmit";
			tError.errorMessage = "上报失败";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	/**
     * 发送维护邮件
     * (暂时未使用) **处自己修改，如需使用可以将发送人及收件人加入到配置中 
     * by lw
     */ 
    public  void sendTextEmail()  { 
    	
		try{
			StringBuffer sb=new StringBuffer();
				if("1".equals(mContType)){
					LCContDB tLCContDB = new LCContDB();
		            tLCContDB.setPrtNo(mPrtNo);
		            tLCContDB.setContNo(mContNo);
		            LCContSet tLCContSet = new LCContSet();	            
		            tLCContSet = tLCContDB.query();
		            
		            LCContSchema tLCContSchema = tLCContSet.get(1);
					String agentcode=tLCContSchema.getAgentCode();
					String Agentcom=tLCContSchema.getAgentCom();
					String agentgroup=tLCContSchema.getAgentGroup();
					String salechnl=tLCContSchema.getSaleChnl();
					
					sb.append("大家好：<br>");
					sb.append("&nbsp&nbsp现有一保单，合同号："+mContNo+"的保单，需要维护业务员。<br>");
					sb.append("&nbsp&nbsp维护前：Contno："+mContNo+"；");
					
					sb.append("<br>");
					sb.append("&nbsp&nbsp请问契约是否可以直接维护?<br>");
					sb.append("<br>");
					sb.append("<HR width='10%' SIZE=1 style='float:left;'>");
					sb.append("<br>");
					sb.append("&nbsp&nbsp&nbsp**<br>");
					sb.append("&nbsp&nbsp&nbsp"+PubFun.getCurrentDate()+"<br>");
					sb.append("&nbsp&nbsp&nbsp中科软科技股份有限公司");
					
					
				}
				
				Properties props = new Properties();
				//Setup mail server
				props.put("mail.smtp.host", host);//设置smtp主机
				props.put("mail.transport.protocol", "smtp");
				props.put("mail.smtp.auth", "true");//使用smtp身份验证
				//Get session
				Session session = Session.getDefaultInstance(props, null);
				//Define message
				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress(sender, "**"));
				 // 3. To: 收件人
		        message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress("***@sinosoft.com.cn", "USER_CC", "UTF-8"));
		        //    To: 增加收件人（可选）
//		        message.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress("dd@receive.com", "USER_DD", "UTF-8"));
		        //    Cc: 抄送（可选）
		       // message.setRecipient(MimeMessage.RecipientType.CC, new InternetAddress("ee@receive.com", "USER_EE", "UTF-8"));
		        //    Bcc: 密送（可选）
//		        message.setRecipient(MimeMessage.RecipientType.BCC, new InternetAddress("ff@receive.com", "USER_FF", "UTF-8"));
				//message.addRecipient(Message.RecipientType.TO, new InternetAddress(receiver));	
				message.setSubject("测试","UTF-8");		//主题名	
				message.setContent(sb.toString(), "text/html;charset=UTF-8"); //文本样式与HTML相同，如需添加样式参考html
				//添加附件
//				BodyPart messageBodyPart = new MimeBodyPart();     
//	            messageBodyPart.setText("bodypart");     
//	              
//	            Multipart multipart = new MimeMultipart();     
//	            multipart.addBodyPart(messageBodyPart);     
//	              
//	            messageBodyPart = new MimeBodyPart();     
//	              
//	            //	          设置上传的资源  
//	            DataSource source = new FileDataSource("E:\\111.jpg");    
//	            //	          添加到  
//	            messageBodyPart.setDataHandler(new DataHandler(source));     
//	            //	          设置文件名称,记得后缀名  
//	            messageBodyPart.setFileName("test.doc");     
//	            multipart.addBodyPart(messageBodyPart);    
//	              
//	            message.setContent(multipart);     
				
				
				message.saveChanges();
				//Send message
				Transport transport = session.getTransport();	
				System.out.println("******正在连接" + host);
				transport.connect(host, sender, password);
				System.out.println("******正在发送给" + "");
				transport.sendMessage(message, message.getAllRecipients());
				System.out.println("******邮件发送成功");
	        
			
		}catch(Exception e){
			System.out.println("发送普通邮件异常"+e);
			
		}	
		
    } 
	public static void main(String[] args) {
		try {
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
}
