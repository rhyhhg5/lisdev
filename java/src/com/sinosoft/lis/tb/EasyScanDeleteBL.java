package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.BPOMissionStateSchema;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class EasyScanDeleteBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private GlobalInput tGI = new GlobalInput();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	private MMap map = new MMap();

	/** 统一更新日期 */
	private String mCurrentDate = PubFun.getCurrentDate();

	/** 统一更新时间 */
	private String mCurrentTime = PubFun.getCurrentTime();

	/** 数据操作字符串 */
	private String mOperate;

	/** 保单印刷号 */
	private String prtNo;

	/** 外包批次号 */
	private String batchNo;

	/**
	 * submitData
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		mOperate = cOperate;
        mInputData = (VData) cInputData.clone();
        
		if (!getInputData()) {
			return false;
		}
		if (!checkData()) {
			return false;
		}
		if (!dealData()) {
			return false;
		}
		if (!prepareOutputData()) {
			return false;
		}
		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(mInputData, null)) {
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}
		return true;
	}

	/**
	 * getInputData
	 * 
	 * @return boolean
	 */
	private boolean getInputData() {
		try {
			tGI = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
			
			TransferData mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
			prtNo = (String) mTransferData.getValueByName("prtNo");
			batchNo = (String) mTransferData.getValueByName("batchNo");
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ScanDeleBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "接收数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * checkData
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		return true;
	}

	/**
	 * dealData
	 * 
	 * @return boolean
	 */
	private boolean dealData() {

		if (mOperate.equals(SysConst.UPDATE)) {
			System.out.println("印刷号：" + prtNo + "   批次号：" + batchNo);
			map.put("update ES_DOC_MAIN set State = '05', Operator = '" 
					+ tGI.Operator + "', ModifyDate = Current Date, ModifyTime = Current Time where DocCode = '" 
					+ prtNo + "' and SubType in ('TB01', 'TB05')", SysConst.UPDATE);
			
			map.put("update BPOMissionState set State = '02', Operator = '" 
					+ tGI.Operator + "', ModifyDate = Current Date, ModifyTime = Current Time where BussNo = '" 
					+ prtNo + "' and BatchNo = '" + batchNo + "'", SysConst.UPDATE);
		}
		else
		{
			CError tError = new CError();
			tError.moduleName = "ScanDeleBL";
			tError.functionName = "dealData";
			tError.errorMessage = "不支持的操作类型！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	/**
	 * prepareOutputData
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {

		try {
			mInputData.add(map);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ScanDeleBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "准备传输数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}

}
