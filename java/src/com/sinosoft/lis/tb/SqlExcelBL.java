/**
 * 交单录入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  xiongxin
 * @version 1.0
 */

package com.sinosoft.lis.tb;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.MappedByteBuffer;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;












import com.sinosoft.lis.db.LCProjectInfoDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LCGrpContSubSchema;
import com.sinosoft.lis.schema.LCProjectInfoSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

import examples.newsgroups;



public class SqlExcelBL {
 private static final int BUFFER = 2048;
 public String Error;
 private  List<String> filelist=new ArrayList<String>();
 private int i=1;
	public SqlExcelBL() {
	}

	public boolean submitData(String  sql, String zipfilepath,String filepath) {
		try {
		String countsql="";
		SSRS countSSRS=new SSRS();
		try {
			countsql= "select count(1) from  (" +sql+") where 1=1";
			countSSRS = new ExeSQL().execSQL(countsql);
		} catch (Exception e) {
			
			Error="执行sql语句时出错,请核实sql语句";
			System.out.println(Error);
			return false;
		}
		
		System.out.println(countSSRS.GetText(1, 1));
		if(countSSRS.getMaxRow()==0){
			System.out.println("SQL有错误");
			Error="SQL查询不出数据";
			return false;
		}else{
		
		//filepath=filepath+"\\tishu\\";
		System.out.println(filepath);
		//createDir(filepath);
//		File sourceFile = new File(filepath);  
//		File[] sourceFiles = sourceFile.listFiles(); 
//		 if(null == sourceFiles || sourceFiles.length<1){  
//	         
//	     }else{
//	    	 for(int i=0;i<sourceFiles.length;i++){  
//                                           
//	    		 filelist.add(filepath+"\\"+sourceFiles[i].getName());
//             }  
//	    	 deleteFile();
//	     }
			int max = Integer.parseInt(countSSRS.GetText(1, 1));
			String[] sheetName ={"1"}; 
			if(max>=10000){
				int min = 0;
								
				while (min != max) {
					CreateExcelList createexcellist = new CreateExcelList("");//指定文件名	 
					createexcellist.createExcelFile();	
					System.out.println("开始分批导出：从第" + min + "行开始导出10000行");
					String maxsql = "SELECT * FROM "
							+ "( SELECT B.*, ROWNUMBER() OVER() AS RN FROM ("
							+ sql + ") AS B" + ")AS A WHERE A.RN >=" + min
							+ " fetch first 10000 rows only";
					
					createexcellist.addSheet(sheetName);
					try {
						createexcellist.setData(0, maxsql);	
					} catch (Exception e) {
						Error="执行sql语句时数据过多，内存溢出";
						return false;
					}
								
					
					if (min +10000 >= max) {
						min = max;

					} else {
						min += 10000;
					}
					
					try{
						   createexcellist.write(filepath+"\\"+i+".xls");
						   int tryCount = 0;  
						    while(tryCount++ <10)  
						    {  	    
						    System.gc();  
						    
						    } 
						}catch(Exception e)
						{			
							System.out.println(e);
							Error="写入EXCEL出错";
							System.out.println(Error);
							return false;
						}
					i++;
				}
				
			}else{
				CreateExcelList createexcellist = new CreateExcelList("");//指定文件名	 
				createexcellist.createExcelFile();	
				createexcellist.addSheet(sheetName);
				createexcellist.setData(0, sql);
				
				try{
					   createexcellist.write(filepath+"\\1.xls");
					}catch(Exception e)
					{			
						System.out.println(e);
						Error="写入EXCEL出错";
						System.out.println(Error);
						return false;
					}
			  
			}   
			if(i!=1){
				i--;
			}
		    if(!fileToZip(filepath, zipfilepath)){
		    	Error="压缩文件出错";
				return false;
		    	
		    };
		     
		
		
		
		
		
	
	}
		} catch (Exception e) {
			Error="执行sql语句时出错";
			System.out.println(Error);
			return false;
			
		}
		return true;
}
	   
    /** 
     * 将存放在sourceFilePath目录下的源文件，打包成fileName名称的zip文件，并存放到zipFilePath路径下 
     * @param sourceFilePath :待压缩的文件路径 
     * @param zipFilePath :压缩后存放路径     
     * @return 
     */  
    public  boolean fileToZip(String sourceFilePath,String zipFilePath){  
        boolean flag = false;  
       //File sourceFile = new File(sourceFilePath);  
        FileInputStream fis = null;  
        BufferedInputStream bis = null;  
        FileOutputStream fos = null;  
        ZipOutputStream zos = null; 
        
          
//        if(sourceFile.exists() == false){  
//            System.out.println("待压缩的文件目录："+sourceFilePath+"不存在.");  
//        }else{         	
            try {  
                File zipFile = new File(zipFilePath);  
                if(zipFile.exists()){  
                    System.out.println(zipFilePath);  
                }else{  
//                    File[] sourceFiles = sourceFile.listFiles();  
//                    if(null == sourceFiles || sourceFiles.length<1){  
//                        System.out.println("待压缩的文件目录：" + sourceFilePath + "里面不存在文件，无需压缩.");  
//                    }else{  
                        fos = new FileOutputStream(zipFile);  
                        zos = new ZipOutputStream(new BufferedOutputStream(fos));  
                        byte[] bufs = new byte[1024*10];  
                        //for(int i=0;i<sourceFiles.length;i++){  
                            //创建ZIP实体，并添加进压缩包  
                        for(int j=1;j<=i;j++){
                        	 File sourceFile = new File(sourceFilePath+"\\"+j+".xls");
                        	 ZipEntry zipEntry = new ZipEntry(sourceFile.getName());  
                             zos.putNextEntry(zipEntry);  
                             //读取待压缩的文件并写进压缩包里  
                             fis = new FileInputStream(sourceFile);  
                             bis = new BufferedInputStream(fis, 1024*10);  
                             int read = 0;  
                             while((read=bis.read(bufs, 0, 1024*10)) != -1){  
                                 zos.write(bufs,0,read);  
                             }                            
                             filelist.add(sourceFilePath+"\\"+sourceFile.getName());
                        }
                           
                       // }  
                        flag = true;  
                    }  
 //               }  
            } catch (FileNotFoundException e) {  
                e.printStackTrace();  
                throw new RuntimeException(e);  
            } catch (IOException e) {  
                e.printStackTrace();  
                throw new RuntimeException(e);  
            } finally{  
                //关闭流  
                try {  
                	if(null != fis) fis.close();                	
                    if(null != bis) bis.close();                     
                    if(null != zos) zos.close();  
                    deleteFile();
                } catch (IOException e) {  
                	deleteFile();
                    e.printStackTrace();  
                    throw new RuntimeException(e);  
                }  
            }  
       // }  
        return flag;  
    }  
      
    /**
     * 删除生成的ZIP文件和XML文件
     * @param cFilePath String：完整文件名
     */
    public  boolean deleteFile()
    {
    	for(String cFilePath:filelist){
    		 File f = new File(cFilePath);
    	     System.out.println(cFilePath);      
    	    if(f.exists()){
    	    	if(!f.delete()){
    	    		forceDelete(f);
    	    	};
    	    }
    	     
    	     
    	}
       

        return true;
    }
    /*
	 * 创建目录 如果存在则不创建
	 */
	public static boolean createDir(String destDirName) {
		File dir = new File(destDirName);
		if (dir.exists()) {// 判断目录是否存在

			return true;
		}

		if (dir.mkdirs()) {// 创建目标目录
			System.out.println("创建目录成功！" + destDirName);
			return true;
		} else {
			System.out.println("创建目录失败！");
			return false;
		}
	}
	public static boolean forceDelete(File f)  
	{  
	    boolean result = false;  
	    int tryCount = 0;  
	    while(!result && tryCount++ <10)  
	    {  	    
	    System.gc();  
	    result = f.delete();  
	    }  
	    return result;  
	}  


}