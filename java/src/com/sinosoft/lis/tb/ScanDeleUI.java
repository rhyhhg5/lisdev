package com.sinosoft.lis.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class ScanDeleUI
{
    public CErrors mErrors = new CErrors();

    public ScanDeleUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            ScanDeleBL tScanDeleBL = new ScanDeleBL();
            if (!tScanDeleBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tScanDeleBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
