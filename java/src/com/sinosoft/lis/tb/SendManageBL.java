package com.sinosoft.lis.tb;

import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 短信发送获取数据处理类
 * </p>
 * 通过xml可以直接配置短信内容及查询
 * </p>
 * 短息配置举例还以参考 SMS_brithday.xml
 * </p>
 * 
 * @author 张成轩
 */
public class SendManageBL {

	/** 错误处理类 */
	private CErrors mErrors = new CErrors();

	/** 数据查询 */
	private String querySql;

	/** 短信内容配置 */
	private String content;

	/** 管理机构索引位置 */
	private int manageComIndex;

	/** 手机号索引位置 */
	private int mobileIndex;

	/** 短信内容参数配置 */
	private String queryInfIndex[][];

	/**
	 * 入口函数
	 * 
	 * @param sendType
	 *            短信类型
	 * @return
	 */
	public boolean submitData(String sendType) {

		try {
			if (!dealData(sendType)) {
				return false;
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 主要处理函数
	 * 
	 * @param sendType
	 *            短信类型
	 * @return
	 */
	private boolean dealData(String sendType) {

		System.out.println("---获取短信---");

		String path = getXMLPath();
		if (path == null) {
			mErrors.addOneError("获取短信配置路径失败");
			return false;
		}
		System.out.println("---getXMLPath---end");

		if (!readXML(path, sendType)) {
			return false;
		}
		System.out.println("---readXML---end");

		if (!sendData()) {
			return false;
		}
		System.out.println("---sendData---end");

		return true;
	}

	/**
	 * 获取短信内容配置路径
	 * 
	 * @return
	 */
	private String getXMLPath() {

		System.out.println("---获取短信配置路径---");

		LDSysVarDB tLDSysVarDB = new LDSysVarDB();
		tLDSysVarDB.setSysVar("ServerRoot");
		if (!tLDSysVarDB.getInfo()) {
			return null;
		}
		String path = tLDSysVarDB.getSysVarValue() + "f1print/picctemplate/";
		//path = "E:\\loong\\work\\ui\\f1print\\picctemplate\\";
		System.out.println(path);

		return path;
	}

	/**
	 * 读取、解析短信配置文件
	 * 
	 * @param path
	 *            配置文件路径
	 * @param sendType
	 *            配置文件类型
	 * @return
	 */
	private boolean readXML(String path, String sendType) {

		// 短信配置文件名 必须为 SMS_短信类型_.xml
		// 短信类型为数据库配置的code
		String xmlPath = path + "SMS_" + sendType + ".xml";
		System.out.println("--开始读取格式文件--" + xmlPath);

		SAXBuilder builder = new SAXBuilder(false);

		try {

			Document doc = builder.build(xmlPath);
			Element root = doc.getRootElement();

			// 读取短信数据的查询sql
			Element query = root.getChild("Query");

			querySql = query.getChild("QuerySql").getText().trim();

			// 读取机构、手机号的数据索引（索引为数据查询sql对应的列），这两个必须配置
			// 机构的索引列
			manageComIndex = Integer.parseInt(query.getChild("ManageCom")
					.getAttributeValue("index"));
			// 手机的索引列
			mobileIndex = Integer.parseInt(query.getChild("Mobile")
					.getAttributeValue("index"));

			Element content = root.getChild("Content");
			Element dataSource = content.getChild("DataSource");

			// 读取短信内容信息配置
			this.content = dataSource.getText().trim();

			List dateCol = content.getChildren("Field");

			// 设置参数索引，用于替换及处理短信内容配置中的?（动态参数），可以没有
			queryInfIndex = new String[dateCol.size()][2];
			for (int col = 0; col < dateCol.size(); col++) {
				Element field = (Element) dateCol.get(col);
				// 参数对应的?位置索引
				queryInfIndex[col][0] = field.getAttributeValue("index");
				// 如果有src 会通过src中的sql进行动态赋值
				queryInfIndex[col][1] = field.getAttributeValue("src");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			mErrors.addOneError("短信格式配置读取处理失败");
			return false;
		}
		return true;
	}

	/**
	 * 发送短信
	 * 
	 * @return
	 */
	private boolean sendData() {

		SendManager tSendManager = new SendManager();

		SSRS tSSRS = getData();
		System.out.println("短信数量---" + tSSRS.getMaxRow());
		
		for (int row = 1; row <= tSSRS.getMaxRow(); row++) {

			// 处理短信内容，主要是处理短信中动态的参数
			String sendContent = dealContent(content, tSSRS.getRowData(row));

			// 发送短信
			tSendManager.sendMsg(tSSRS.GetText(row, mobileIndex), sendContent,
					tSSRS.GetText(row, manageComIndex));
		}
		return true;
	}

	/**
	 * 短信内容处理类
	 * 
	 * @param content
	 *            短信内容
	 * @param rowData
	 *            发送数据数组
	 * @return
	 */
	private String dealContent(String content, String[] rowData) {

		String sendContent = content;

		// 逐一替换参数
		for (int index = 0; index < queryInfIndex.length; index++) {

			String contentPart;
			String srcQuerySql = queryInfIndex[index][1];

			// 判断是否是由sql动态赋值
			if (srcQuerySql != null && !"".equals(srcQuerySql.trim())) {
				srcQuerySql = srcQuerySql.replace("?",
						"'"
								+ rowData[Integer
										.parseInt(queryInfIndex[index][0]) - 1]
								+ "'");
				// 通过查询确认参数值
				contentPart = new ExeSQL().getOneValue(srcQuerySql);
			} else {
				// 没有src的sql，直接替换
				contentPart = rowData[Integer.parseInt(queryInfIndex[index][0]) - 1];
			}

			// 替换?（动态参数）
			sendContent = sendContent.substring(0, sendContent.indexOf("?"))
					+ contentPart
					+ sendContent.substring(sendContent.indexOf("?") + 1);
		}

		return sendContent;
	}

	/**
	 * 获取短信发送数据
	 * 
	 * @return
	 */
	private SSRS getData() {
		ExeSQL tExeSQL = new ExeSQL();
		return tExeSQL.execSQL(querySql);
	}

	/**
	 * 获取错误对象
	 * 
	 * @return
	 */
	public CErrors getCErrors() {
		return mErrors;
	}

	public static void main(String[] arr) {
		/*SendManageBL tSendManageBL = new SendManageBL();
		tSendManageBL.submitData("brithday");*/
	}
}
