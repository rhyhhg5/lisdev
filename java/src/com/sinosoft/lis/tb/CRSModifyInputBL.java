package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.*;



public class CRSModifyInputBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 数据操作字符串 */
	private String mOperate = "";

	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	//录入
	private String mContNo = "";
	private String mContType = "";
	private String mCrs_SaleChnl = "";
	private String mCrs_BussType = "";
	private String mGrpagentcom = "";
	private String mGrpagentname = "";
	private String mGrpagentcode = "";
	private String mGrpagentidno = "";
	private String mGrpagentcomName = "";
	private MMap map = new MMap();
    private VData mResult = new VData();
    
    public CRSModifyInputBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {

    	if (!getInputData(cInputData)) {
			return false;
		}
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "QYModifyAmntBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectYearInfoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		
        mOperate = cOperate;
        mResult.add(map);
        
        PubSubmit pubsubmit = new PubSubmit();
        if (!pubsubmit.submitData(mResult,cOperate)) {
            mErrors.copyAllErrors(pubsubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
            return false;
       }
       System.out.println("---End pubsubmit---");
      
        return true;
    }
   
    private boolean getInputData(VData cInputData) {
    	this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0); 
		mContType = (String)tTransferData.getValueByName("ContType");
		mContNo = (String)tTransferData.getValueByName("ContNo");
    	mCrs_SaleChnl = (String)tTransferData.getValueByName("Crs_SaleChnl");
    	mCrs_BussType = (String)tTransferData.getValueByName("Crs_BussType");
    	mGrpagentcom = (String)tTransferData.getValueByName("Grpagentcom");
    	mGrpagentname = (String)tTransferData.getValueByName("Grpagentname");
    	mGrpagentcode = (String)tTransferData.getValueByName("Grpagentcode");
    	mGrpagentidno = (String)tTransferData.getValueByName("Grpagentidno");
    	mOperate = (String)tTransferData.getValueByName("strOperation");
    	mGrpagentcomName = (String)tTransferData.getValueByName("GrpagentcomName");
    	return true;
    }
    
    private boolean dealData(){
    	
		
    	if("1".equals(mContType)&&"DELETE".equals(mOperate)){
    		System.out.println("jinrusql");
    		String tSQL = "delete from LOMixAgent where contno='"+mContNo+"' and Grpcontno='0000' and contType='"+mContType+"'";
    		
    		String tSQL1 = "update lccont set Crs_Salechnl = null, " +
    				"Crs_Busstype = null,Grpagentcom= null,Grpagentcode= null,Grpagentname= null," +
    				"Grpagentidno= null where contno = '"+mContNo+"' ";
    		map.put(tSQL, "DELETE");
    		map.put(tSQL1, "DELETE");
    		
    	System.out.print(tSQL);
    	System.out.print(";");
    	System.out.print(tSQL1);
    	System.out.print(";");
    	}
    	if("2".equals(mContType)&&"DELETE".equals(mOperate)){
    		String tSQL = "delete from LOMixAgent where contno="+mContNo+" and Grpcontno='0000' and contType="+mContType+"";
    		
    		String tSQL1 ="update lcgrpcont set Crs_Salechnl = null, Crs_Busstype = null," +
    				"Grpagentcom=null,Grpagentcode=null,Grpagentname=null," +
    				"Grpagentidno=null where grpcontno = '"+mContNo+"' ";
    		map.put(tSQL, "DELETE");
    		map.put(tSQL1, "DELETE");
    	System.out.println(tSQL);
    	System.out.print(tSQL1);
    	}
    	if("1".equals(mContType)&&"INSERT".equals(mOperate)){
    		
    		String tSQL = "update lccont set Crs_Salechnl = '"+mCrs_SaleChnl+"'," +
    				" Crs_Busstype = '"+mCrs_BussType+"',Grpagentcom='"+mGrpagentcom+"'," +
    				"Grpagentcode='"+mGrpagentcode+"',Grpagentname='"+mGrpagentname+"',Grpagentidno='"+mGrpagentidno+"' " +
    				"where contno = '"+mContNo+"' with ur";
    		String tSQL1 = "delete from LOMixAgent where contno='"+mContNo+"' and Grpcontno='0000' and contType='"+mContType+"'";
    		
    		String tSQL2 = "insert into LOMixAgent values('"+mContNo+"','0000','1','"+mCrs_BussType+"'," +
    				"'集团交叉提数',null,null,'郭忠华',current date,current time,current date,current time)";
    		
    		map.put(tSQL, "INSERT");
    		map.put(tSQL1, "INSERT");
    		map.put(tSQL2, "INSERT");
    		System.out.println(tSQL);
    		System.out.println(tSQL1);
    		System.out.println(tSQL2);
    	}
    	if("2".equals(mContType)&&"INSERT".equals(mOperate)){
    		
    		String tSQL = "update lcgrpcont set Crs_Salechnl = '"+mCrs_SaleChnl+"', Crs_Busstype = '"+mCrs_BussType+"'," +
    				"Grpagentcom='"+mGrpagentcom+"'," +
    				"Grpagentcode='"+mGrpagentcode+"',Grpagentname='"+mGrpagentname+"',Grpagentidno='"+mGrpagentidno+"' " +
    				"where grpcontno = '"+mContNo+"' with ur";
    		String tSQL1 = "delete from LOMixAgent where contno="+mContNo+" and Grpcontno='0000' and contType="+mContType+"";
    		
    		String tSQL2 ="insert into LOMixAgent values('0000','"+mContNo+"','2','"+mCrs_BussType+"','集团交叉提数',null,null," +
    				"'郭忠华',current date,current time,current date,current time)";

    		map.put(tSQL, "INSERT");
    		map.put(tSQL1, "INSERT");
    		map.put(tSQL2, "INSERT");
    	System.out.println(tSQL);
    	System.out.println(tSQL1);
    	System.out.println(tSQL2);
    	}
    	return true;
    }


}

