/**
 * Copyright ? 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContPlanSchema;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class AskLCContPlanBL {

    public CErrors mErrors = new CErrors();
    private VData  mResult = new VData();
    private MMap map = new MMap();

    /** 往后面传输数据的容器 */
    private VData  mInputData = null;

    /** 全局数据 */
    private GlobalInput mGlobalInput = null;

    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();
    private String theCurrentTime = PubFun.getCurrentTime();

    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LCContPlanSchema mLCContPlanSchema = null;
    private LCContPlanRiskSchema mLCContPlanRiskSchema =new LCContPlanRiskSchema();
    private LCContPlanRiskSet mLCContPlanRiskSet = null;

    public AskLCContPlanBL() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //数据校验
        if (!checkData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AskLCContPlanBL";
            tError.functionName = "checkData";
            tError.errorMessage = "数据处理失败AskLCContPlanBL-->checkData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AskLCContPlanBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败AskLCContPlanBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        // 装配处理好的数据，准备给后台进行保存
        this.prepareOutputData();
        System.out.println("---prepareOutputData---");

        //　数据提交、保存

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start AskLCContPlanBL Submit...");

        if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "AskLCContPlanBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }

            System.out.println("---commitData---");

            mInputData = null;
            return true;
        }
    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData() {
        if(!mOperate.equals("DELETE||MAIN")){
        }
        return true;
    }
    /**
     * 处理业务逻辑
     * @return boolean
     */
    private boolean dealData() {
        if (mOperate.equals("INSERT||MAIN")){
            //设置非空项
            for (int i = 1; i <= this.mLCContPlanRiskSet.size(); i++) {
                mLCContPlanRiskSet.get(i).setOperator(mGlobalInput.Operator);
                mLCContPlanRiskSet.get(i).setMakeDate(theCurrentDate);
                mLCContPlanRiskSet.get(i).setModifyDate(theCurrentDate);
                mLCContPlanRiskSet.get(i).setMakeTime(theCurrentTime);
                mLCContPlanRiskSet.get(i).setModifyTime(theCurrentTime);
            }
            this.mLCContPlanSchema.setOperator(mGlobalInput.Operator);
            this.mLCContPlanSchema.setMakeDate(theCurrentDate);
            this.mLCContPlanSchema.setMakeTime(theCurrentTime);
            this.mLCContPlanSchema.setModifyDate(theCurrentDate);
            this.mLCContPlanSchema.setModifyTime(theCurrentTime);
            map.put(this.mLCContPlanSchema,"INSERT");
            map.put(this.mLCContPlanRiskSet,"INSERT");
            }
      if (mOperate.equals("UPDATE||MAIN")) {
          for (int i = 1; i <= this.mLCContPlanRiskSet.size(); i++) {
               mLCContPlanRiskSet.get(i).setOperator(mGlobalInput.Operator);
               mLCContPlanRiskSet.get(i).setMakeDate(theCurrentDate);
               mLCContPlanRiskSet.get(i).setModifyDate(theCurrentDate);
               mLCContPlanRiskSet.get(i).setMakeTime(theCurrentTime);
               mLCContPlanRiskSet.get(i).setModifyTime(theCurrentTime);
           }
           this.mLCContPlanSchema.setOperator(mGlobalInput.Operator);
           this.mLCContPlanSchema.setMakeDate(theCurrentDate);
           this.mLCContPlanSchema.setMakeTime(theCurrentTime);
           this.mLCContPlanSchema.setModifyDate(theCurrentDate);
           this.mLCContPlanSchema.setModifyTime(theCurrentTime);
           map.put(this.mLCContPlanSchema,"UPDATE");
           map.put(this.mLCContPlanRiskSet,"UPDATE");

      }
      if (mOperate.equals("DELETE||MAIN")){
          map.put(this.mLCContPlanSchema,"DELETE");
          map.put(this.mLCContPlanRiskSet,"DELETE");
      }
      return true;
    }
    /**
     * 准备向后台传输的数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        this.mInputData.clear();
        this.mInputData.add(map);
        this.mResult.clear();
        this.mInputData.add(this.mLCContPlanSchema);
        this.mInputData.add(this.mLCContPlanRiskSet);
        return true;
    }
    /**
     * 获取从UI传入的数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        this.mInputData = cInputData;
        if (this.mInputData == null) {
                // @@错误处理
              CError tError = new CError();
              tError.moduleName = "AskLCContPlanBL";
              tError.functionName = "getInputData";
              tError.errorMessage = "数据处理失败AskLCContPlanBL-->getInputData!";
              this.mErrors.addOneError(tError);
              return false;
        }
        this.mLCContPlanRiskSet = (LCContPlanRiskSet) mInputData.getObjectByObjectName(
                "LCContPlanRiskSet", 0);
        this.mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        this.mLCContPlanSchema =(LCContPlanSchema) mInputData.getObjectByObjectName(
                "LCContPlanSchema",0);
        return true;
    }
    /**
     * 获取结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

}
