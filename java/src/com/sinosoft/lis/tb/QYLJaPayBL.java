package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LAComToAgentDB;
import com.sinosoft.lis.db.LCCUWMasterDB;
import com.sinosoft.lis.db.LCCUWSubDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContGetPolDB;
import com.sinosoft.lis.db.LCContReceiveDB;
import com.sinosoft.lis.db.LCGCUWMasterDB;
import com.sinosoft.lis.db.LCGCUWSubDB;
import com.sinosoft.lis.db.LCGUWMasterDB;
import com.sinosoft.lis.db.LCGUWSubDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCUWMasterDB;
import com.sinosoft.lis.db.LCUWSubDB;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LCCUWMasterSet;
import com.sinosoft.lis.vschema.LCCUWSubSet;
import com.sinosoft.lis.vschema.LCContGetPolSet;
import com.sinosoft.lis.vschema.LCContReceiveSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCGCUWMasterSet;
import com.sinosoft.lis.vschema.LCGCUWSubSet;
import com.sinosoft.lis.vschema.LCGUWMasterSet;
import com.sinosoft.lis.vschema.LCGUWSubSet;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCUWMasterSet;
import com.sinosoft.lis.vschema.LCUWSubSet;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class QYLJaPayBL {
	public CErrors mErrors = new CErrors();
	private String mOperate;
	private String mFinancialAction;
	private GlobalInput mGlobalInput = new GlobalInput();
	private TransferData mTransferData = new TransferData();
	private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
	private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
	private LCContSet mLCContSet = new LCContSet();
	private LCContSchema mLCContSchema = new LCContSchema();
	private LCPolSet mLCPolSet = new LCPolSet();
	private LCGCUWSubSet mLCGCUWSubSet = new LCGCUWSubSet();
	private LCGUWSubSet mLCGUWSubSet = new LCGUWSubSet();
	private LCGCUWMasterSet mLCGCUWMasterSet = new LCGCUWMasterSet();
	private LCGUWMasterSet mLCGUWMasterSet = new LCGUWMasterSet();
	private LCCUWSubSet mLCCUWSubSet = new LCCUWSubSet();
	private LCUWSubSet mLCUWSubSet = new LCUWSubSet();
	private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();
	private LCCUWMasterSet mLCCUWMasterSet = new LCCUWMasterSet();
	private LCContReceiveSet mLCContReceiveSet = new LCContReceiveSet();
	private LCContGetPolSet mLCContGetPolSet = new LCContGetPolSet();
	private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
	private LJAPaySet mLJAPaySet = new LJAPaySet();
	private LJAPayGrpSet mLJAPayGrpSet = new LJAPayGrpSet();
	private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();
	private LAAgentSchema mLAAgentSchema = new LAAgentSchema();

	private String mPrtNo;
	private String mContNo;

	public QYLJaPayBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		this.mOperate = cOperate;
		System.out.println("动作为：" + mOperate);

		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		return true;
	}

	/**
	 * 校验录入的数据是否合法
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		return true;
	}

	/**
	 * dealData 处理业务数据
	 *
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean dealData() {

		// 财务充负
		if (mFinancialAction.equals("doNegative")) {
			if (!negativeRecoil()) {
				return false;
			}
		}

		// 财务充正
		if (mFinancialAction.equals("doPositive")) {
			if (!positiveRecoil()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * getInputData 将外部传入的数据分解到本类的属性中
	 * 
	 * @param cInputData
	 *            VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean getInputData(VData cInputData) {
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));

		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		if (mTransferData == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "QYLJaPayBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "在接受数据时没有得到TransferData的数据!";
			this.mErrors.addOneError(tError);

			return false;
		}

		if (mTransferData.getValueByName("tPrtNo") == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "QYLJaPayBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保单印刷号失败!";
			this.mErrors.addOneError(tError);

			return false;
		}

		mPrtNo = (String) mTransferData.getValueByName("tPrtNo");
		System.out.println("保单印刷号为：" + mPrtNo);

		if (mTransferData.getValueByName("tContNo") == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "QYLJaPayBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取保单号失败!";
			this.mErrors.addOneError(tError);

			return false;
		}

		mContNo = (String) mTransferData.getValueByName("tContNo");
		System.out.println("保单号为：" + mContNo);

		mFinancialAction = (String) mTransferData
				.getValueByName("tFinancialAction");
		System.out.println("财务动作为：" + mFinancialAction);

		if (!getContMessage()) {
			return false;
		}
		return true;
	}

	// 获取保单号
	private boolean getContMessage() {

		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		LCGrpContSet tLCGrpContSet = new LCGrpContSet();
		tLCGrpContDB.setPrtNo(mPrtNo);
		tLCGrpContDB.setGrpContNo(mContNo);
		if (tLCGrpContDB.getInfo() == false) {
			CError tError = new CError();
			tError.moduleName = "QYLJaPayBL";
			tError.functionName = "getContMessage";
			tError.errorMessage = "没有查到团单信息!";
			this.mErrors.addOneError(tError);
			return false;
		}
		tLCGrpContSet = tLCGrpContDB.query();
		if (tLCGrpContSet != null && tLCGrpContSet.size() > 1) {
			CError tError = new CError();
			tError.moduleName = "QYLJaPayBL";
			tError.functionName = "getContMessage";
			tError.errorMessage = "保单号不唯一!";
			this.mErrors.addOneError(tError);
			return false;
		}
		this.mLCGrpContSchema = tLCGrpContSet.get(1);
		String tGrpContNo = mLCGrpContSchema.getGrpContNo();
		System.out.println("tGrpContNo = " + tGrpContNo);

		LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
		tLCGrpPolDB.setPrtNo(mPrtNo);
		tLCGrpPolDB.setGrpContNo(tGrpContNo);
		mLCGrpPolSet = tLCGrpPolDB.query();
		if (mLCGrpPolSet == null) {
			CError tError = new CError();
			tError.moduleName = "QYLJaPayBL";
			tError.functionName = "getContMessage";
			tError.errorMessage = "获取团单LCGRPPol数据失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setGrpContNo(tGrpContNo);
		mLCContSet = tLCContDB.query();
		if (mLCContSet == null) {
			CError tError = new CError();
			tError.moduleName = "QYLJaPayBL";
			tError.functionName = "getContMessage";
			tError.errorMessage = "获取团单分单LCCont数据失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setGrpContNo(tGrpContNo);
		mLCPolSet = tLCPolDB.query();
		if (mLCPolSet == null) {
			CError tError = new CError();
			tError.moduleName = "QYLJaPayBL";
			tError.functionName = "getContMessage";
			tError.errorMessage = "获取团单分单LCPol数据失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 获取保单核保信息
		if (!getUWMessage()) {
			CError tError = new CError();
			tError.moduleName = "QYLJaPayBL";
			tError.functionName = "getContMessage";
			tError.errorMessage = "获取核保信息失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		LCContReceiveDB tLCContReceiveDB = new LCContReceiveDB();
		tLCContReceiveDB.setContNo(tGrpContNo);
		mLCContReceiveSet = tLCContReceiveDB.query();

		LCContGetPolDB tLCContGetPolDB = new LCContGetPolDB();
		tLCContGetPolDB.setContNo(tGrpContNo);
		mLCContGetPolSet = tLCContGetPolDB.query();

		LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
		tLJTempFeeDB.setOtherNo(tGrpContNo);
		mLJTempFeeSet = tLJTempFeeDB.query();
		if (mLJTempFeeSet == null) {
			CError tError = new CError();
			tError.moduleName = "QYLJaPayBL";
			tError.functionName = "getContMessage";
			tError.errorMessage = "获取财务暂收数据失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 获取财务实收数据
		if (!getFinancialMessage()) {
			CError tError = new CError();
			tError.moduleName = "QYLJaPayBL";
			tError.functionName = "getContMessage";
			tError.errorMessage = "获取财务实收数据失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	// 获取保单核保数据
	private boolean getUWMessage() {

		String tGrpContNo = mLCGrpContSchema.getGrpContNo();

		LCGCUWSubDB tLCGCUWSubDB = new LCGCUWSubDB();
		tLCGCUWSubDB.setGrpContNo(tGrpContNo);
		mLCGCUWSubSet = tLCGCUWSubDB.query();

		LCGUWSubDB tLCGUWSubDB = new LCGUWSubDB();
		tLCGUWSubDB.setGrpContNo(tGrpContNo);
		mLCGUWSubSet = tLCGUWSubDB.query();

		LCGCUWMasterDB tLCGCUWMasterDB = new LCGCUWMasterDB();
		tLCGCUWMasterDB.setGrpContNo(tGrpContNo);
		mLCGCUWMasterSet = tLCGCUWMasterDB.query();

		LCGUWMasterDB tLCGUWMasterDB = new LCGUWMasterDB();
		tLCGUWMasterDB.setGrpContNo(tGrpContNo);
		mLCGUWMasterSet = tLCGUWMasterDB.query();

		LCCUWSubDB tLCCUWSubDB = new LCCUWSubDB();
		tLCCUWSubDB.setGrpContNo(tGrpContNo);
		mLCCUWSubSet = tLCCUWSubDB.query();

		LCUWSubDB tLCUWSubDB = new LCUWSubDB();
		tLCUWSubDB.setGrpContNo(tGrpContNo);
		mLCUWSubSet = tLCUWSubDB.query();

		LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
		tLCUWMasterDB.setGrpContNo(tGrpContNo);
		mLCUWMasterSet = tLCUWMasterDB.query();

		LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
		tLCCUWMasterDB.setGrpContNo(tGrpContNo);
		mLCCUWMasterSet = tLCCUWMasterDB.query();

		return true;
	}

	// 获取实收数据
	private boolean getFinancialMessage() {

		String tGrpContNo = mLCGrpContSchema.getGrpContNo();

		LJAPayDB tLJAPayDB = new LJAPayDB();
		tLJAPayDB.setIncomeNo(tGrpContNo);
		tLJAPayDB.setDueFeeType("0");
		mLJAPaySet = tLJAPayDB.query();
		if (mLJAPaySet == null) {
			return false;
		}

		if (mLJAPaySet != null && mLJAPaySet.size() > 0) {
			LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
			System.out.println("PayNo = " + mLJAPaySet.get(1).getPayNo());

			tLJAPayGrpDB.setPayNo(mLJAPaySet.get(1).getPayNo());
			tLJAPayGrpDB.setGrpContNo(tGrpContNo);
			mLJAPayGrpSet = tLJAPayGrpDB.query();
			if (mLJAPayGrpSet == null) {
				return false;
			}

			LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
			tLJAPayPersonDB.setPayNo(mLJAPaySet.get(1).getPayNo());
			tLJAPayPersonDB.setGrpContNo(tGrpContNo);
			mLJAPayPersonSet = tLJAPayPersonDB.query();
			if (mLJAPayPersonSet == null) {
				return false;
			}
		}

		return true;
	}

	// 财务充负
	String mdate = "2016-05-01";
	double fmoneytax;
	String mMoneytax;

	private boolean negativeRecoil() {
		String tConfDate = mLJAPaySet.get(1).getConfDate();
		System.out.println("实收确认日期：" + tConfDate);
		LJAPaySet tLJAPaySet = mLJAPaySet;
		LJAPayGrpSet tLJAPayGrpSet = null;
		LJAPayPersonSet tLJAPayPersonSet = null;
		String tNewPayNo = PubFun1.CreateMaxNo("PAYNO", null);
		System.out.println("充负的PayNo = " + tNewPayNo);

		MMap tMMap = new MMap();
		VData tVData = new VData();
		tLJAPayGrpSet = mLJAPayGrpSet;
		tLJAPayPersonSet = mLJAPayPersonSet;

		if (tLJAPaySet != null && tLJAPaySet.size() > 0) {
			tLJAPaySet.get(1).setPayNo(tNewPayNo);
			tLJAPaySet.get(1).setSumActuPayMoney(
					-1 * mLJAPaySet.get(1).getSumActuPayMoney());
			tLJAPaySet.get(1).setDueFeeType("2");
			tLJAPaySet.get(1).setConfDate(tConfDate);
			tLJAPaySet.get(1).setMakeDate(PubFun.getCurrentDate());
			tLJAPaySet.get(1).setMakeTime(PubFun.getCurrentTime());
			tLJAPaySet.get(1).setModifyDate(PubFun.getCurrentDate());
			tLJAPaySet.get(1).setModifyTime(PubFun.getCurrentTime());
			tMMap.put(tLJAPaySet, "INSERT");
		}

		if (tLJAPayGrpSet != null && tLJAPayGrpSet.size() > 0) {
			if (mLJAPayGrpSet.get(1).getConfDate().compareTo(mdate) < 0) {
				for (int i = 1; i <= tLJAPayGrpSet.size(); i++) {

					tLJAPayGrpSet.get(i).setPayNo(tNewPayNo);
					tLJAPayGrpSet.get(i).setSumActuPayMoney(
							-1 * mLJAPayGrpSet.get(i).getSumActuPayMoney());
					tLJAPayGrpSet.get(i).setSumDuePayMoney(
							-1 * mLJAPayGrpSet.get(i).getSumDuePayMoney());
					tLJAPayGrpSet.get(i).setConfDate(tConfDate);
					tLJAPayGrpSet.get(i).setMakeDate(PubFun.getCurrentDate());
					tLJAPayGrpSet.get(i).setMakeTime(PubFun.getCurrentTime());
					tLJAPayGrpSet.get(i).setModifyDate(PubFun.getCurrentDate());
					tLJAPayGrpSet.get(i).setModifyTime(PubFun.getCurrentTime());
					tLJAPayGrpSet.get(i).setMoneyNoTax(
							(mLJAPayGrpSet.get(i).getSumActuPayMoney()) + "");
					tLJAPayGrpSet.get(i).setMoneyTax("0");
					tLJAPayGrpSet.get(i).setBusiType("01");
					tLJAPayGrpSet.get(i).setTaxRate("0.00");
				}

			} else if (mLJAPayGrpSet.get(1).getMoneyNoTax() != null) {
				for (int i = 1; i <= tLJAPayGrpSet.size(); i++) {
					fmoneytax = Double.parseDouble(mLJAPayGrpSet.get(i)
							.getMoneyTax());
					fmoneytax = 0.0 - fmoneytax;
					mMoneytax = Double.toString(fmoneytax);
					tLJAPayGrpSet.get(i).setPayNo(tNewPayNo);
					tLJAPayGrpSet.get(i).setSumActuPayMoney(
							-1 * mLJAPayGrpSet.get(i).getSumActuPayMoney());
					tLJAPayGrpSet.get(i).setSumDuePayMoney(
							-1 * mLJAPayGrpSet.get(i).getSumDuePayMoney());
					tLJAPayGrpSet.get(i).setConfDate(tConfDate);
					tLJAPayGrpSet.get(i).setMakeDate(PubFun.getCurrentDate());
					tLJAPayGrpSet.get(i).setMakeTime(PubFun.getCurrentTime());
					tLJAPayGrpSet.get(i).setModifyDate(PubFun.getCurrentDate());
					tLJAPayGrpSet.get(i).setModifyTime(PubFun.getCurrentTime());
					tLJAPayGrpSet.get(i).setMoneyNoTax(
							"-" + mLJAPayGrpSet.get(i).getMoneyNoTax());
					tLJAPayGrpSet.get(i).setMoneyTax(mMoneytax);
				}
			} else {
				for (int i = 1; i <= tLJAPayGrpSet.size(); i++) {
					tLJAPayGrpSet.get(i).setPayNo(tNewPayNo);
					tLJAPayGrpSet.get(i).setSumActuPayMoney(
							-1 * mLJAPayGrpSet.get(i).getSumActuPayMoney());
					tLJAPayGrpSet.get(i).setSumDuePayMoney(
							-1 * mLJAPayGrpSet.get(i).getSumDuePayMoney());
					tLJAPayGrpSet.get(i).setConfDate(tConfDate);
					tLJAPayGrpSet.get(i).setMakeDate(PubFun.getCurrentDate());
					tLJAPayGrpSet.get(i).setMakeTime(PubFun.getCurrentTime());
					tLJAPayGrpSet.get(i).setModifyDate(PubFun.getCurrentDate());
					tLJAPayGrpSet.get(i).setModifyTime(PubFun.getCurrentTime());
				}
			}
			tMMap.put(tLJAPayGrpSet, "INSERT");
		}

		if (tLJAPayPersonSet != null && tLJAPayPersonSet.size() > 0) {

			for (int i = 1; i <= tLJAPayPersonSet.size(); i++) {

				tLJAPayPersonSet.get(i).setPayNo(tNewPayNo);
				tLJAPayPersonSet.get(i).setSumActuPayMoney(
						-1 * mLJAPayPersonSet.get(i).getSumActuPayMoney());
				tLJAPayPersonSet.get(i).setSumDuePayMoney(
						-1 * mLJAPayPersonSet.get(i).getSumDuePayMoney());
				tLJAPayPersonSet.get(i).setConfDate(tConfDate);
				tLJAPayPersonSet.get(i).setMakeDate(PubFun.getCurrentDate());
				tLJAPayPersonSet.get(i).setMakeTime(PubFun.getCurrentTime());
				tLJAPayPersonSet.get(i).setModifyDate(PubFun.getCurrentDate());
				tLJAPayPersonSet.get(i).setModifyTime(PubFun.getCurrentTime());
			}

			tMMap.put(tLJAPayPersonSet, "INSERT");
		}

		tVData.add(tMMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(tVData, "INSERT")) {
			CError tError = new CError();
			tError.moduleName = "QYLJaPayBL";
			tError.functionName = "PubSubmit";
			tError.errorMessage = "财务数据充负失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	// 财务充正
	private boolean positiveRecoil() {
		String tCvaliDate = this.mLCGrpContSchema.getCValiDate();
		System.out.println("保单生效日：" + tCvaliDate);
		String tConfDate = mLJAPaySet.get(1).getConfDate();
		System.out.println("实收确认日期：" + tConfDate);
		LJAPaySet tLJAPaySet = mLJAPaySet;
		LJAPayGrpSet tLJAPayGrpSet = null;
		LJAPayPersonSet tLJAPayPersonSet = null;
		String tNewPayNo = PubFun1.CreateMaxNo("PAYNO", null);
		System.out.println("充正的PayNo = " + tNewPayNo);

		MMap tMMap = new MMap();
		VData tVData = new VData();

		tLJAPayGrpSet = mLJAPayGrpSet;
		tLJAPayPersonSet = mLJAPayPersonSet;

		tLJAPaySet.get(1).setPayNo(tNewPayNo);
		tLJAPaySet.get(1).setDueFeeType("2");
		tLJAPaySet.get(1).setConfDate(tConfDate);
		tLJAPaySet.get(1).setMakeDate(PubFun.getCurrentDate());
		tLJAPaySet.get(1).setMakeTime(PubFun.getCurrentTime());
		tLJAPaySet.get(1).setModifyDate(PubFun.getCurrentDate());
		tLJAPaySet.get(1).setModifyTime(PubFun.getCurrentTime());

		if (tLJAPayGrpSet != null && tLJAPayGrpSet.size() > 0) {
			if (mLJAPayGrpSet.get(1).getConfDate().compareTo(mdate) < 0) {
				for (int i = 1; i <= tLJAPayGrpSet.size(); i++) {
					tLJAPayGrpSet.get(i).setPayNo(tNewPayNo);
					tLJAPayGrpSet.get(i).setConfDate(tConfDate);
					tLJAPayGrpSet.get(i).setLastPayToDate(tCvaliDate);
					tLJAPayGrpSet.get(i).setMakeDate(PubFun.getCurrentDate());
					tLJAPayGrpSet.get(i).setMakeTime(PubFun.getCurrentTime());
					tLJAPayGrpSet.get(i).setModifyDate(PubFun.getCurrentDate());
					tLJAPayGrpSet.get(i).setModifyTime(PubFun.getCurrentTime());
					tLJAPayGrpSet.get(i).setMoneyNoTax(
							mLJAPayGrpSet.get(i).getSumActuPayMoney() + "");
					tLJAPayGrpSet.get(i).setMoneyTax("0");
					tLJAPayGrpSet.get(i).setBusiType("01");
					tLJAPayGrpSet.get(i).setTaxRate("0.00");
				}
			} else {
				for (int i = 1; i <= tLJAPayGrpSet.size(); i++) {
					tLJAPayGrpSet.get(i).setPayNo(tNewPayNo);
					tLJAPayGrpSet.get(i).setConfDate(tConfDate);
					tLJAPayGrpSet.get(i).setLastPayToDate(tCvaliDate);
					tLJAPayGrpSet.get(i).setMakeDate(PubFun.getCurrentDate());
					tLJAPayGrpSet.get(i).setMakeTime(PubFun.getCurrentTime());
					tLJAPayGrpSet.get(i).setModifyDate(PubFun.getCurrentDate());
					tLJAPayGrpSet.get(i).setModifyTime(PubFun.getCurrentTime());
				}
			}
		}

		if (tLJAPayPersonSet != null && tLJAPayPersonSet.size() > 0) {

			for (int i = 1; i <= tLJAPayPersonSet.size(); i++) {
				tLJAPayPersonSet.get(i).setPayNo(tNewPayNo);
				tLJAPayPersonSet.get(i).setConfDate(tConfDate);
				tLJAPayPersonSet.get(i).setLastPayToDate(tCvaliDate);
				tLJAPayPersonSet.get(i).setMakeDate(PubFun.getCurrentDate());
				tLJAPayPersonSet.get(i).setMakeTime(PubFun.getCurrentTime());
				tLJAPayPersonSet.get(i).setModifyDate(PubFun.getCurrentDate());
				tLJAPayPersonSet.get(i).setModifyTime(PubFun.getCurrentTime());
			}

		}

		tMMap.put(tLJAPaySet, "INSERT");
		tMMap.put(tLJAPayGrpSet, "INSERT");
		tMMap.put(tLJAPayPersonSet, "INSERT");
		tVData.add(tMMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(tVData, "INSERT")) {
			CError tError = new CError();
			tError.moduleName = "QYLJaPayBL";
			tError.functionName = "PubSubmit";
			tError.errorMessage = "财务数据充正失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

}
