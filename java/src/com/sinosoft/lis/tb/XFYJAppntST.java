package com.sinosoft.lis.tb;


import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.tb.OnlyDiskImport;









import examples.newsgroups;

import java.util.*;
import java.util.regex.Pattern;

public class XFYJAppntST {
	private GlobalInput mGlobalInput;
	private VData mResult = new VData();
	private String mEdorNo = null;
	public CErrors mErrors = new CErrors();
	private final String[] schemaName = { "XFYJAppntSchema",
			"XFYJInsuredSchema" };
	private final String[] setName = { "XFYJAppntSet", "XFYJInsuredSet" };
	private String[] sheetName = { "AppntInfo", "InsuredInfo" };
	private String configName = "XFYJ.xml";
	private String[] mTableNames = { "XFYJAppnt", "XFYJInsured" };
	Boolean flag=true;
	
	public boolean doAdd(String path, String fileName,
			GlobalInput mGlobalInput, String Edorno) throws Exception {
		this.mGlobalInput = mGlobalInput;
		this.mEdorNo = Edorno;
		OnlyDiskImport importFile = new OnlyDiskImport(path + fileName, path
				+ configName, sheetName, mTableNames, schemaName, setName);
		System.out.println(path + configName);
		try {
			if (!importFile.doImport()) {
				return false;
			}
		} catch (Exception e) {
			CError tError = new CError();
			tError.moduleName = "GrpBriefTbWorkFlowBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "存在序号为空的投保人或被保人，请核实再上传!";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		List<SchemaSet> list = new ArrayList();
		list = importFile.getSchemaList();
		XFYJAppntSet mXFYJAppntSet = (XFYJAppntSet) list.get(0);
		MMap map = new MMap();
		for (int i = 1; i <= mXFYJAppntSet.size(); i++) {
			if(!addoneXFYJAppntSchema(map, mXFYJAppntSet.get(i))){
				return false;
			}
		}
		if(!flag){
			CError tError = new CError();
			tError.moduleName = "GrpBriefTbWorkFlowBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "请查看日志!";
			this.mErrors.addOneError(tError);
			return false;
		}
		if (!submitData(map)) {
			return false;
		}
		
		System.out.println("2222222222");
		XFYJInsuredSet mXFYJInsuredSet = (XFYJInsuredSet) list.get(1);
		System.out.println(mXFYJInsuredSet.size());
		MMap map2 = new MMap();
		for (int i = 1; i <= mXFYJInsuredSet.size(); i++) {	
			
			if(!addoneXFYJInsuredSchema(map2, mXFYJInsuredSet.get(i))){				
				
				return false;
				
			}
			
			
			
		}
		if(!flag){
			CError tError = new CError();
			tError.moduleName = "GrpBriefTbWorkFlowBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "请查看日志!";
			this.mErrors.addOneError(tError);
			return false;
		}
		if (!submitData(map2)) {
			return false;
		}
		// XFYJAppntSchema tXFYJAppntSchema =new XFYJAppntSchema();
		// XFYJAppntDB tXfyjAppntDB=new XFYJAppntDB();
		// tXfyjAppntDB.setBatchNo(Edorno);
		// tXfyjAppntDB.setPrtNo("000000");
		// tXfyjAppntDB.delete();
		
		
		
		
		return true;
	}

	private boolean addoneXFYJAppntSchema(MMap map,
			XFYJAppntSchema mXFYJAppntSchema) {
		XFYJAppntSchema tXFYJAppntSchema = mXFYJAppntSchema;
		tXFYJAppntSchema.setFlag("0");
		tXFYJAppntSchema.setBatchNo(mEdorNo);
		tXFYJAppntSchema.setMakeDate(PubFun.getCurrentDate());
		tXFYJAppntSchema.setModifyDate(PubFun.getCurrentDate());
		tXFYJAppntSchema.setMakeTime(PubFun.getCurrentTime());
		tXFYJAppntSchema.setModifyTime(PubFun.getCurrentTime());
		tXFYJAppntSchema.setOperator(mGlobalInput.Operator);
		tXFYJAppntSchema.setMngCom(mGlobalInput.ManageCom);
		String appntName = mXFYJAppntSchema.getAppntName();
		String IDType = mXFYJAppntSchema.getAppntIDtype();
		String IDNo = mXFYJAppntSchema.getAppntIDno();
		String Birthday=mXFYJAppntSchema.getAppntBirthday();
		String Sex=mXFYJAppntSchema.getAppntSex();
		String IDEndDate=mXFYJAppntSchema.getIDEndDate();
		String idstartdate=mXFYJAppntSchema.getIDStartDate();
		String PolapplyDate=mXFYJAppntSchema.getPolapplyDate();
		String receive=mXFYJAppntSchema.getReceiveDate();
		String prtno=mXFYJAppntSchema.getPrtNo();
    	String mobile=tXFYJAppntSchema.getMobile();
    	String homephone=tXFYJAppntSchema.getHomePhone();
    	String phone = mXFYJAppntSchema.getPhone();
    	String email = tXFYJAppntSchema.getEmail();
		if("".equals(prtno)||prtno==null){
			CError tError = new CError();
			tError.moduleName = "GrpBriefTbWorkFlowBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "存在印刷号为空的投保人，请核实再上传!";
			this.mErrors.addOneError(tError);
			return false;
		}
		if(!PubFun.checkDateForm(Birthday)){
//			CError tError = new CError();
//			tError.moduleName = "GrpBriefTbWorkFlowBL";
//			tError.functionName = "getInputData";
//			tError.errorMessage = "印刷号为："+tXFYJAppntSchema.getPrtNo()+"的生日格式不正确!";
//			this.mErrors.addOneError(tError);
//			return false;
			String error="投保人生日格式不正确!";
			writeerror(error, prtno);	
			flag=false;
		}
		if(!PubFun.checkDateForm(IDEndDate)){
//			CError tError = new CError();
//			tError.moduleName = "GrpBriefTbWorkFlowBL";
//			tError.functionName = "getInputData";
//			tError.errorMessage = "印刷号为："+tXFYJAppntSchema.getPrtNo()+"的身份证失效日期格式不正确!";
//			this.mErrors.addOneError(tError);
//			return false;
			String error="投保人身份证失效日期格式不正确!";
			writeerror(error, prtno);
			flag=false;
		}
		if(!PubFun.checkDateForm(idstartdate)){
//			CError tError = new CError();
//			tError.moduleName = "GrpBriefTbWorkFlowBL";
//			tError.functionName = "getInputData";
//			tError.errorMessage = "印刷号为："+tXFYJAppntSchema.getPrtNo()+"的身份证生效日期格式不正确!";
//			this.mErrors.addOneError(tError);
//			return false;
			String error="投保人身份证生效日期格式不正确!";
			writeerror(error, prtno);	
			flag=false;
		}
		if(!PubFun.checkDateForm(PolapplyDate)){
//			CError tError = new CError();
//			tError.moduleName = "GrpBriefTbWorkFlowBL";
//			tError.functionName = "getInputData";
//			tError.errorMessage = "印刷号为："+tXFYJAppntSchema.getPrtNo()+"的投保单申请日期格式不正确!";
//			this.mErrors.addOneError(tError);
//			return false;
			String error="投保单申请日期格式不正确!";
			writeerror(error, prtno);	
			flag=false;
		}
		if(!PubFun.checkDateForm(receive)){
//			CError tError = new CError();
//			tError.moduleName = "GrpBriefTbWorkFlowBL";
//			tError.functionName = "getInputData";
//			tError.errorMessage = "印刷号为："+tXFYJAppntSchema.getPrtNo()+"的收单日期格式不正确!";
//			this.mErrors.addOneError(tError);
//			return false;
			String error="收单日期格式不正确!";
			writeerror(error, prtno);	
			flag=false;
		}
		//zxs 20190522
		System.out.println("PolapplyDate="+PolapplyDate+",receive="+receive);
		String day = new ExeSQL().getOneValue("select code from ldcode where  codetype = 'checkpolapplydate'");
		String polApplyFlag = new ExeSQL().getOneValue("select case when '"+PolapplyDate+"' between current date -"+day+" day and  current date  then 1 else 0 end from dual ");
		if("0".equals(polApplyFlag)){
			String error="投保申请日期录入有误，不得晚于当前日期，且不得早于当前日期-"+day+"天。";
			writeerror(error, prtno);	
			flag=false;
		}
		String receiveFlag = new ExeSQL().getOneValue("select case when '"+receive+"' between current date -"+day+" day and  current date  then 1 else 0 end from dual ");
		if("0".equals(receiveFlag)){
			String error="投保收单日期录入有误，不得晚于当前日期，且不得早于当前日期-"+day+"天。";
			writeerror(error, prtno);	
			flag=false;
		}
		
		//增加对身份证号的校验 20170818
		String strReturn =PubFun.CheckIDNo(IDType,IDNo,Birthday,Sex);
		if(!strReturn.equals("")){
			String error="客户"+appntName+"的"+strReturn;
			writeerror(error, prtno);	
			flag=false;
		}
		//增加对客户姓名等的规范性校验 zxs 20190415
		String nameCheck = PubFun.checkValidateName(IDType, appntName);
		if(nameCheck!=""){
			String error = "投保人"+nameCheck;
			writeerror(error, prtno);	
			flag=false;	
		}
    	if(!checkDate(1,prtno,mobile,homephone,email)) {
    		flag=false;
		}
		//2018-06-04校验投保人手机号和业务员手机号是否相同 CLH
    	if(!checkphone(prtno,mobile,homephone,phone,IDNo)){
    		flag = false;
    	}
		map.put(tXFYJAppntSchema, "DELETE&INSERT");
		return true;
	}

	private boolean checkphone(String prtno, String mobile, String homephone,String phone, String id) {
		ExeSQL tExeSQL = new ExeSQL();
		if(!isNull(phone)){
			String mobilSql = "select groupagentcode from laagent where phone='" + phone + "' and idno <> '"+id+"' with ur";
			String result = tExeSQL.getOneValue(mobilSql);
			if(!isNull(result)){
				String error = "该投保人联系电话与本方业务员" + result + "电话号码相同，请核查！";
				writeerror(error,prtno);
				return false;
			}
			mobilSql = "select groupagentcode from laagent where mobile='" + phone + "' and idno <> '"+id+"' with ur";
			result = tExeSQL.getOneValue(mobilSql);
			if(!isNull(result)){
				String error = "该投保人联系电话与本方业务员" + result + "手机号码相同，请核查！";
				writeerror(error,prtno);
				return false;
			}
		}
		
		if(!isNull(mobile)){
			String phoneSql = "select groupagentcode from laagent where mobile='" + mobile + "' and idno <> '"+id+"' with ur";
			String result = tExeSQL.getOneValue(phoneSql);
			if(!isNull(result)){
				String error = "该投保人移动电话与本方业务员" + result + "手机号码相同，请核查！";
				writeerror(error,prtno);
				return false;
			}
			phoneSql = "select groupagentcode from laagent where phone='" + mobile + "' and idno <> '"+id+"' with ur";
			result = tExeSQL.getOneValue(phoneSql);
			if(!isNull(result)){
				String error = "该投保人移动电话与本方业务员" + result + "电话号码相同，请核查！";
				writeerror(error,prtno);
				return false;
			}
		}
		if(!isNull(homephone)){
			String phoneSql = "select groupagentcode from laagent where mobile='" + homephone + "' and idno <> '"+id+"' with ur";
			String result = tExeSQL.getOneValue(phoneSql);
			if(!isNull(result)){
				String error = "该投保人固定电话与本方业务员" + result + "手机号码相同，请核查！";
				writeerror(error,prtno);
				return false;
			}
			phoneSql = "select groupagentcode from laagent where phone='" + homephone + "' and idno <> '"+id+"' with ur";
			result = tExeSQL.getOneValue(phoneSql);
			if(!isNull(result)){
				String error = "该投保人固定电话与本方业务员" + result + "电话号码相同，请核查！";
				writeerror(error,prtno);
				return false;
			}
		}
		return true;
	}

	private boolean easyExecSql(String mobilSql) {
		// TODO Auto-generated method stub
		return false;
	}

	private boolean isNull(String phone) {
		if("".equals(phone) || phone == null){
			return true;
		}
		return false;
	}

	private boolean addoneXFYJInsuredSchema(MMap map,
			XFYJInsuredSchema mXFYJInsuredSchema) {
		XFYJInsuredSchema tXFYJInsuredSchema = mXFYJInsuredSchema;
		tXFYJInsuredSchema.setBusiNo(PubFun1.CreateMaxNo("XFYJBUSINO", "86",
				null));		
		tXFYJInsuredSchema.setMakeDate(PubFun.getCurrentDate());		
		tXFYJInsuredSchema.setModifyDate(PubFun.getCurrentDate());
		tXFYJInsuredSchema.setMakeTime(PubFun.getCurrentTime());
		tXFYJInsuredSchema.setModifyTime(PubFun.getCurrentTime());
		tXFYJInsuredSchema.setOther(mEdorNo);
		String birthday=tXFYJInsuredSchema.getBirthday();
		String InsuredName=tXFYJInsuredSchema.getInsuredName();
		String InsuredSex=tXFYJInsuredSchema.getInsuredSex();
		String IDType=tXFYJInsuredSchema.getIDType();
		String IDNo=tXFYJInsuredSchema.getIDNo();
		String IDEndDate=tXFYJInsuredSchema.getIDEndDate();
		String IDStartDate=tXFYJInsuredSchema.getIDStartDate();
		String prtno=tXFYJInsuredSchema.getPrtNo();
    	String mobile=tXFYJInsuredSchema.getMobile();
    	String email = tXFYJInsuredSchema.getEmail();
		System.out.println("prtno:"+prtno);
		System.out.println(birthday);
		if("".equals(prtno)||prtno==null){
			CError tError = new CError();
			tError.moduleName = "GrpBriefTbWorkFlowBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "存在印刷号为空的被保人，请核实再上传!";
			this.mErrors.addOneError(tError);
			return false;
		}
		if(!"".equals(birthday)&&!PubFun.checkDateForm(birthday)&&birthday!=null){
//			CError tError = new CError();
//			tError.moduleName = "GrpBriefTbWorkFlowBL";
//			tError.functionName = "getInputData";
//			tError.errorMessage = "印刷号为："+tXFYJInsuredSchema.getPrtNo()+"的收单日期格式不正确!";
//			this.mErrors.addOneError(tError);
//			return false;
			String error="被保人生日格式不正确!";
			writeerror(error, prtno);	
			flag=false;
			
		}
		if(!"".equals(IDEndDate)&&!PubFun.checkDateForm(IDEndDate)&&IDEndDate!=null){
//			CError tError = new CError();
//			tError.moduleName = "GrpBriefTbWorkFlowBL";
//			tError.functionName = "getInputData";
//			tError.errorMessage = "印刷号为："+tXFYJInsuredSchema.getPrtNo()+"身份证失效格式不正确!";
//			this.mErrors.addOneError(tError);
//			return false;
			String error="被保人身份证失效日期格式不正确!";
			writeerror(error, prtno);	
			flag=false;
		}
		if(!"".equals(IDStartDate)&&!PubFun.checkDateForm(IDStartDate)&&IDStartDate!=null){
//			CError tError = new CError();
//			tError.moduleName = "GrpBriefTbWorkFlowBL";
//			tError.functionName = "getInputData";
//			tError.errorMessage = "印刷号为："+tXFYJInsuredSchema.getPrtNo()+"的身份证生效格式不正确!";
//			this.mErrors.addOneError(tError);
//			return false;	
			String error="被保人身份证生效日期格式不正确!";
			writeerror(error, prtno);	
			flag=false;
		}
		
		//增加对身份证号的校验 20170818
		String strReturn =PubFun.CheckIDNo(IDType,IDNo,birthday,InsuredSex);
		if(!strReturn.equals("")){
			String error="客户"+InsuredName+"的"+strReturn;
			writeerror(error, prtno);	
			flag=false;
		}
		//zxs 20190415 客户姓名相关校验
		String nameCheck = PubFun.checkValidateName(IDType, InsuredName);
		if(nameCheck!=""){
			String error = "被保人"+nameCheck;
			writeerror(error, prtno);	
			flag=false;	
		}
		String num = "";
		if("1".equals(tXFYJInsuredSchema.getNo())){
			num = "第一被保人";
		}
		if("2".equals(tXFYJInsuredSchema.getNo())){
			num = "第二被保人";
		}
		if("3".equals(tXFYJInsuredSchema.getNo())){
			num = "第三被保人";
		}
		if("30".equals(tXFYJInsuredSchema.getRelationToAppnt())){
			String error = "请明确"+num+"与投保人关系！";
			writeerror(error, prtno);	
			flag=false;	
		}
		if("30".equals(tXFYJInsuredSchema.getRelationToMainInsured())){
			String error = "请明确"+num+"与第一被保险人关系！";
			writeerror(error, prtno);	
			flag=false;	
		}
		
		if(!checkDate(2,prtno,mobile,"",email)) {
			flag=false;
		}
		//2018-06-04校验投保人手机号和业务员手机号是否相同
    	if(!checkmobile(prtno,mobile,IDNo,InsuredName)){
    		flag = false;
    	}
		
		map.put(tXFYJInsuredSchema, "DELETE&INSERT");
		return true;
	}
	

	private boolean checkmobile(String prtno, String mobile, String id,String InsuredName) {
		ExeSQL tExeSQL = new ExeSQL();
		if(!isNull(mobile)){
			String mobilSql = "select groupagentcode from laagent where phone='" + mobile + "' and idno <> '"+id+"' with ur";
			String result = tExeSQL.getOneValue(mobilSql);
			if(!isNull(result)){
				String error = "被保人" + InsuredName + "手机号码与本方业务员" + result + "电话号码相同，请核查！";
				writeerror(error,prtno);
				return false;
			}
			mobilSql = "select groupagentcode from laagent where mobile='" + mobile + "' and idno <> '"+id+"' with ur";
			result = tExeSQL.getOneValue(mobilSql);
			if(!isNull(result)){
				String error = "被保人" + InsuredName + "手机号码与本方业务员" + result + "手机号码相同，请核查！";
				writeerror(error,prtno);
				return false;
			}
		}
		return true;
	}

	private boolean submitData(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		try {
			if (!tPubSubmit.submitData(data, "")) {
				
				return false;
			}
		} catch (Exception e) {			
			mErrors=tPubSubmit.mErrors;
			return false;	
			
		}
		
		return true;
	}
	private boolean checkDate(int type,String prtno,String mobile,String homephone,String email ) {
		String str="";
		if(type==1) {
			str="投保人";
		}else {
			str="被保人";
		}
    	if(prtno != null && !prtno.equals("")&& prtno.matches(".*[^a-zA-Z0-9]+.*")) { 
    		String error = str+"的印刷号不能包含除数字和英文字母之外的字符，请检查!";
    		writeerror(error, prtno);	
            return false;
    	}
    	if(mobile !=null && !mobile.equals("")) {
    		if(mobile.length()!=11||!mobile.matches("[0-9]+")) {
    			String error = str+"的电话号码只能是11位的数字，请检查!";
    			writeerror(error, prtno);	
    			return false;
    		}
    		if(!mobile.substring(0,2).equals("13")&&!mobile.substring(0,2).equals("16")&&!mobile.substring(0,2).equals("14")&&!mobile.substring(0,2).equals("15")&&!mobile.substring(0,2).equals("18")&&!mobile.substring(0,2).equals("17")&&!mobile.substring(0,2).equals("19")) {
    			String error = str+"的电话号码输入有误，请检查!";
    			writeerror(error, prtno);	
    			return false;
    		}
    	}
    	if(homephone!=null && !homephone.equals("")) {
    		String a="0123456789()-";
    		  for(int i=0;i<homephone.length();i++)
    		  {
    		    if(a.indexOf(homephone.charAt(i))<0) {
    		    	String error = str+"的固定电话号码输入有误，请检查!";
    		    	writeerror(error, prtno);	
    		    	return false;
    		    }
    		  }
    	}
    	if(email != null && !email.equals("")) {
    		String REGEX_EMAIL = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
    		if(!Pattern.matches(REGEX_EMAIL, email)) {
    			String error = str+"的邮箱输入有误，请检查!";
    			writeerror(error, prtno);	
    			return false;
    		}
    	}
    	return true;
	}
	private void writeerror(String error,String prtno){
		XFYJLogSchema mXfyjLogSchema = new XFYJLogSchema();
		mXfyjLogSchema.setBusiNo(PubFun1.CreateMaxNo("XFYJBUSINO", "86",
				null));
		mXfyjLogSchema.setPrtNo(prtno);
		mXfyjLogSchema.setLogInfo(error);
		mXfyjLogSchema.setBatchNo(mEdorNo);
		mXfyjLogSchema.setOther("导入失败！");
		mXfyjLogSchema.setMakeDate(PubFun.getCurrentDate());
		mXfyjLogSchema.setModifyDate(PubFun.getCurrentDate());
		mXfyjLogSchema.setMakeTime(PubFun.getCurrentTime());
		mXfyjLogSchema.setModifyTime(PubFun.getCurrentTime());
		MMap tMMap = new MMap();
		tMMap.put(mXfyjLogSchema, "DELETE&INSERT");

		VData tvData = new VData();
		tvData.add(tMMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(tvData, "DELETE&INSERT")) {
			CError tError = new CError();
			tError.moduleName = "FamilyTaxBL";
			tError.functionName = "PubSubmit";
			tError.errorMessage = "生成日志失败!";
			this.mErrors.addOneError(tError);			
		}
		ExeSQL meExeSQL = new ExeSQL();
		meExeSQL.execUpdateSQL("update xfyjappnt set state='3' where batchno='"
				+ mEdorNo
				+ "' and prtno='"
				+ "000000"
				+ "'");
		meExeSQL.execUpdateSQL("update xfyjappnt set flag='1' where batchno='"
				+ mEdorNo
				+ "' and prtno='"
				+ prtno
				+ "'");
	}
}
