package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LCContGetPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LDSysTraceSchema;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LDSysTraceSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class EstmakedateBL {

	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private GlobalInput tGI = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    private MMap map = new MMap();

    /** 数据操作字符串 */
    private String mOperate;

   private LDSysTraceSchema tLDSysTraceSchema=new LDSysTraceSchema();
   private LDCodeSchema tLDCodeSchema=new LDCodeSchema();


    /**
     * submitData
     * 
     * @param cInputData
     *            VData
     * @param cOperate
     *            String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        mInputData = (VData) cInputData.clone();

        if (!getInputData())
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit ps = new PubSubmit();
		if (!ps.submitData(mInputData, null)) {
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}

        return true;
    }

    /**
     * getInputData
     * 
     * @return boolean
     */
    private boolean getInputData()
    {
        try
        {
            tGI = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput",
                    0);

            TransferData mTransferData = (TransferData) mInputData
                    .getObjectByObjectName("TransferData", 0);
            tLDSysTraceSchema =(LDSysTraceSchema)mInputData.getObjectByObjectName("LDSysTraceSchema", 0);
            tLDCodeSchema=(LDCodeSchema)mInputData.getObjectByObjectName("LDCodeSchema", 0);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ScanDeleBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * checkData
     * 
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 
     * @return boolean
     */
    private boolean dealData()
    {
        if (!dealGetPolDate())
        {
            return false;
        }

        return true;
    }

    /**
     * @return
     */
    private boolean dealGetPolDate()
    {  
    	LDSysTraceSet tLDSysTraceSet=new LDSysTraceSet();
    	tLDSysTraceSet.add(tLDSysTraceSchema);
    	map.put(tLDSysTraceSet, "INSERT");
    	
    	LDCodeSet tLDCodeSet=new LDCodeSet();
    	tLDCodeSet.add(tLDCodeSchema);
    	map.put("update  ldcode  set  code='"+tLDCodeSchema.getCode()+"' where codetype='"+tLDCodeSchema.getCodeType()+"' ", "UPDATE");
        return true;
    }
    
	/**
	 * prepareOutputData
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {

		try {
			mInputData.add(map);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "EstmakedateBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "准备传输数据失败!!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}
}
