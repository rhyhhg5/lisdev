package com.sinosoft.lis.tb;

import com.sinosoft.lis.db.LSTransInfoDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LSTransInfoSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class TransContInfoBL {

	/** 传入数据的容器 */
	private VData mInputData = new VData();

	private GlobalInput mGlobalInput = new GlobalInput();

	private MMap map = new MMap();
	
	/** 往前面传输数据的容器 */
    private String mOperate = "";

	private LSTransInfoSchema mLSTransInfoSchema = new LSTransInfoSchema();
	
    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();

    private String theCurrentTime = PubFun.getCurrentTime();
	

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();


	public boolean submitData(VData cInputData, String cOperate) {

		// 数据过滤器
		if (!getInputData(cInputData, cOperate)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		
		this.prepareOutputData();

		PubSubmit tPubSubmit = new PubSubmit();

		if (!tPubSubmit.submitData(mInputData, cOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);

			CError tError = new CError();
			tError.moduleName = "TransContInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";

			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	private boolean dealData() {

		String type = mLSTransInfoSchema.getContType();
		if("1".equals(type)){
			mLSTransInfoSchema.setTransContNo(mLSTransInfoSchema.getTransGrpContNo());
		}
		
		if("save".equals(mOperate)){
			mLSTransInfoSchema.setMakeDate(theCurrentDate);
			mLSTransInfoSchema.setMakeTime(theCurrentTime);
			mLSTransInfoSchema.setModifyDate(theCurrentDate);
			mLSTransInfoSchema.setModifyTime(theCurrentTime);
			mLSTransInfoSchema.setInputOperator(mGlobalInput.Operator);
			mLSTransInfoSchema.setOperator(mGlobalInput.Operator);
			map.put(mLSTransInfoSchema, "INSERT");
		}else if("update".equals(mOperate)){
			LSTransInfoDB tLSTransInfoDB = new LSTransInfoDB();
			tLSTransInfoDB.setPrtNo(mLSTransInfoSchema.getPrtNo());
			if (!tLSTransInfoDB.getInfo()) {
				CError tError = new CError();
				tError.moduleName = "TransContInfoBL";
				tError.functionName = "dealData";
				tError.errorMessage = "转入保单信息查询失败！";
				this.mErrors.addOneError(tError);
				return false;
			}
			mLSTransInfoSchema.setMakeDate(tLSTransInfoDB.getMakeDate());
			mLSTransInfoSchema.setMakeTime(tLSTransInfoDB.getMakeTime());
			mLSTransInfoSchema.setModifyDate(theCurrentDate);
			mLSTransInfoSchema.setModifyTime(theCurrentTime);
			mLSTransInfoSchema.setOperator(mGlobalInput.Operator);
			map.put(mLSTransInfoSchema, "UPDATE");
		}else if("delete".equals(mOperate)){
			map.put("delete from lstransinfo where prtno='"
                    + mLSTransInfoSchema.getPrtNo() + "'", "DELETE");
		}
		

		return true;
	}

	private void prepareOutputData() {
		mInputData.clear();
		mInputData.add(map);

	}

	private boolean getInputData(VData cInputData, String cOperate) {
		
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));

		mLSTransInfoSchema.setSchema((LSTransInfoSchema) cInputData.getObjectByObjectName(
				"LSTransInfoSchema", 0));
		
		mOperate = cOperate;
		
		if(!"save".equals(mOperate)&&!"update".equals(mOperate)&&!"delete".equals(mOperate)){
			CError tError = new CError();
			tError.moduleName = "TransContInfoBL";
			tError.functionName = "dealData";
			tError.errorMessage = "操作类型错误！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		return true;
	}
	
}


