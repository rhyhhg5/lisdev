/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpPayDueSchema;
import com.sinosoft.lis.schema.LCGrpPayPlanSchema;
import com.sinosoft.lis.vschema.LCGrpPayDueSet;
import com.sinosoft.lis.vschema.LCGrpPayPlanSet;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证管理回收操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author kevin
 * @version 1.0
 */

public class GrpPayPlanBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();


    /* 私有成员 */
    private String mszOperate = "";


    /* 业务相关的数据 */
    private VData mResult = new VData();
    private GlobalInput globalInput = new GlobalInput();
    private String mProposalGrpContNo = "";
    private String mPrtNo = "";
    private TransferData mTransferData = null;
    private LCGrpPayPlanSet mLCGrpPayPlanSet = new LCGrpPayPlanSet();
    private LCGrpPayDueSet mLCGrpPayDueSet = new LCGrpPayDueSet();
    private Reflections tReflections = new Reflections();


    // 记录下当前操作到哪一条记录，如果操作没有成功完成，给用户返回所有未能成功处理的数据。

    public GrpPayPlanBL()
    {
    }

    public static void main(String[] args)
    {
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        mszOperate = cOperate;
        if (mszOperate.equals(""))
        {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }

        if (!getInputData(cInputData))
        {
        	return false;
        }
        if (!dealData())
        {
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData(VData vData)
    {
    	
    	globalInput.setSchema((GlobalInput) vData.getObjectByObjectName(
                "GlobalInput", 0));
    	if(!"DELETE".equals(mszOperate)){
    		mLCGrpPayPlanSet.set((LCGrpPayPlanSet) vData.getObjectByObjectName("LCGrpPayPlanSet",0));
            if(mLCGrpPayPlanSet == null || mLCGrpPayPlanSet.size()<=0){
            	buildError("getInputData","获取约定缴费计划信息失败！");
            	return false;
            }
    	}
        mTransferData = (TransferData) vData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mProposalGrpContNo = (String) mTransferData.getValueByName("ProposalGrpContNo");
        if (mProposalGrpContNo == null || "".equals(mProposalGrpContNo))
        {
            buildError("getInputData", "获取投保单号码失败。");
            return false;
        }
        
        String tPrtNoSql = "select prtno from lcgrpcont where ProposalGrpContNo = '"+mProposalGrpContNo+"' "; 
  	  	mPrtNo = new ExeSQL().getOneValue(tPrtNoSql);
  	    if(mPrtNo == null || "".equals(mPrtNo)){
	  		buildError("getInputData", "获取保单印刷号码失败。");
	        return false;
  	    }
  	    
        return true;
    }
    
//  根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData()
    {
    	MMap tMMap = new MMap();
    	
    	if("INSERT".equals(mszOperate)){
    		String tGetContPlanCodeSql = "select contplancode from lccontplan where proposalgrpcontno = '"+mProposalGrpContNo+"' and contplancode !='11' order by contplancode ";
        	SSRS tSSRS = new ExeSQL().execSQL(tGetContPlanCodeSql);
        	if(tSSRS ==null || tSSRS.MaxRow<=0){
        		buildError("dealData", "获取保障计划失败。");
    	        return false;
        	}
        	LCGrpPayPlanSet tLCGrpPayPlanSet = new LCGrpPayPlanSet();
    		for(int i=1;i<=mLCGrpPayPlanSet.size();i++){
    			for(int m=1;m<=tSSRS.MaxRow;m++){
    				LCGrpPayPlanSchema tLCGrpPayPlanSchema = new LCGrpPayPlanSchema();
    				tLCGrpPayPlanSchema.setPrtNo(mPrtNo);
    				tLCGrpPayPlanSchema.setProposalGrpContNo(mProposalGrpContNo);
    				tLCGrpPayPlanSchema.setPlanCode(String.valueOf(i));
    				tLCGrpPayPlanSchema.setPaytoDate(mLCGrpPayPlanSet.get(i).getPaytoDate());
    				tLCGrpPayPlanSchema.setContPlanCode(tSSRS.GetText(m, 1));
    				tLCGrpPayPlanSchema.setOperator(globalInput.Operator);
    				tLCGrpPayPlanSchema.setMakeDate(PubFun.getCurrentDate());
    				tLCGrpPayPlanSchema.setMakeTime(PubFun.getCurrentTime());
    				tLCGrpPayPlanSchema.setModifyDate(PubFun.getCurrentDate());
    				tLCGrpPayPlanSchema.setModifyTime(PubFun.getCurrentTime());
    				tLCGrpPayPlanSet.add(tLCGrpPayPlanSchema);
    				
    				if(i>1){
    					LCGrpPayDueSchema tLCGrpPayDueSchema = new LCGrpPayDueSchema();
        				tReflections.transFields(tLCGrpPayDueSchema,tLCGrpPayPlanSchema);
        				tLCGrpPayDueSchema.setGrpContNo(mProposalGrpContNo);
        				tLCGrpPayDueSchema.setGetNoticeNo("000000");
        				tLCGrpPayDueSchema.setPayNo("000000");
        				tLCGrpPayDueSchema.setConfState("02");
        				mLCGrpPayDueSet.add(tLCGrpPayDueSchema);
    				}
    			}
        	}
    		tMMap.put(tLCGrpPayPlanSet, SysConst.DELETE_AND_INSERT);
    		if(mLCGrpPayDueSet.size()>0){
    			tMMap.put(mLCGrpPayDueSet, SysConst.DELETE_AND_INSERT);
    		}
    	}else if("INSERTDetail".equals(mszOperate)){
    		LCGrpPayPlanSet tLCGrpPayPlanSet = new LCGrpPayPlanSet();
    		for(int i=1;i<=mLCGrpPayPlanSet.size();i++){
    			LCGrpPayPlanSchema tLCGrpPayPlanSchema = new LCGrpPayPlanSchema();
    			tLCGrpPayPlanSchema = mLCGrpPayPlanSet.get(i);
				tLCGrpPayPlanSchema.setPrtNo(mPrtNo);
				tLCGrpPayPlanSchema.setProposalGrpContNo(mProposalGrpContNo);
				tLCGrpPayPlanSchema.setOperator(globalInput.Operator);
				tLCGrpPayPlanSchema.setMakeDate(PubFun.getCurrentDate());
				tLCGrpPayPlanSchema.setMakeTime(PubFun.getCurrentTime());
				tLCGrpPayPlanSchema.setModifyDate(PubFun.getCurrentDate());
				tLCGrpPayPlanSchema.setModifyTime(PubFun.getCurrentTime());
				tLCGrpPayPlanSet.add(tLCGrpPayPlanSchema);
				
				if(!"1".equals(tLCGrpPayPlanSchema.getPlanCode())){
					LCGrpPayDueSchema tLCGrpPayDueSchema = new LCGrpPayDueSchema();
    				tReflections.transFields(tLCGrpPayDueSchema,tLCGrpPayPlanSchema);
    				tLCGrpPayDueSchema.setGrpContNo(mProposalGrpContNo);
    				tLCGrpPayDueSchema.setGetNoticeNo("000000");
    				tLCGrpPayDueSchema.setPayNo("000000");
    				tLCGrpPayDueSchema.setConfState("02");
    				mLCGrpPayDueSet.add(tLCGrpPayDueSchema);
				}
    		}
    		tMMap.put(tLCGrpPayPlanSet, SysConst.DELETE_AND_INSERT);
    		if(mLCGrpPayDueSet.size()>0){
    			tMMap.put(mLCGrpPayDueSet, SysConst.DELETE_AND_INSERT);
    		}
    	}else if("DELETE".equals(mszOperate)){
    		String tDelSql = "delete from LCGrpPayPlan where proposalgrpcontno = '"+mProposalGrpContNo+"' ";
    		String tDelSql1 = "delete from LCGrpPayDue where proposalgrpcontno = '"+mProposalGrpContNo+"' ";
    		tMMap.put(tDelSql,SysConst.DELETE);
    		tMMap.put(tDelSql1,SysConst.DELETE);
    	}
    	if(!submit(tMMap)){
	        return false;
    	}
    	return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CertifyTakeBackUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit(MMap aMMap)
    {
        VData data = new VData();
        data.add(aMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }
}
