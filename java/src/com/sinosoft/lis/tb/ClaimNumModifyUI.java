/**
 * <p>ClassName: LAContReceiveUI </p>
 * <p>Description: LAContReceive类文件 </p>
 * <p>Copyright: Copyright (c) 2007.7</p>
 * @Database: 代理人管理
 * <p>Company: sinosoft</p>
 * @author xiongxin
 */

package com.sinosoft.lis.tb;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ClaimNumModifyUI {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;
	

	//业务处理相关变量

	public ClaimNumModifyUI() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		mOperate = cOperate;
		this.mInputData = (VData) cInputData.clone();
		ClaimNumModifyBL tClaimNumModifyBL = new 	ClaimNumModifyBL();
		try {
			if (!tClaimNumModifyBL.submitData(mInputData, mOperate)) {
				this.mErrors.copyAllErrors(tClaimNumModifyBL.mErrors);
				CError tError = new CError();
				tError.moduleName = "ClaimNumModifyUI";
				tError.functionName = "submitDat";
				tError.errorMessage = "BL类处理失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
}
