package com.sinosoft.lis.tb;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LCAskUWNoteSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.vschema.LCAskUWNoteSet;
import com.sinosoft.lis.db.LCAskUWNoteDB;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LCAskUWNoteBL {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCAskUWNoteSchema mLCAskUWNoteSchema = new
            LCAskUWNoteSchema();
    private LCAskUWNoteSet mLCAskUWNoteSet = new LCAskUWNoteSet();
    private String mPrtNo;
    private String mNoteID;
    public LCAskUWNoteBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!checkData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAskUWNoteBL";
            tError.functionName = "checkData";
            tError.errorMessage = "数据处理失败LCAskUWNoteBL-->checkData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAskUWNoteBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LCAskUWNoteBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("Start LCAskUWNoteBL Submit...");
        LCAskUWNoteBLS tLCAskUWNoteBLS = new LCAskUWNoteBLS();
        tLCAskUWNoteBLS.submitData(mInputData, cOperate);
        System.out.println("End LCAskUWNoteBL Submit...");
        //如果有需要处理的错误，则返回
        if (tLCAskUWNoteBLS.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCAskUWNoteBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCAskUWNoteBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLCAskUWNoteSchema.setSchema((LCAskUWNoteSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LCAskUWNoteSchema", 0));
        mPrtNo = mLCAskUWNoteSchema.getPrtNo();
        mNoteID = mLCAskUWNoteSchema.getNoteID();
        mInputData = cInputData;
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAskUWNoteBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean checkData() {
        if (this.mOperate.compareTo("INSERT||MAIN") == 0) {

            return true;
        }
        if (this.mOperate.compareTo("UPDATE||MAIN") == 0) {
            String tSql = "";
            ExeSQL texeSQL = new ExeSQL();
            tSql = "select stateflag from LCAskUWNote where prtno='" + mPrtNo +
                   "' and noteid='" + mNoteID + "'";
            SSRS ssrs = texeSQL.execSQL(tSql);
            if (ssrs.GetText(1, 1).compareTo("3") == 0) { //状态为3时是最终确认的
                CError tError = new CError();
                tError.moduleName = "LCAskUWNoteBL";
                tError.functionName = "checkData";
                tError.errorMessage = "已经回复过该信息！";
                this.mErrors.addOneError(tError);
                return false;
            } else {
                return true;
            }
        }
        if (this.mOperate.compareTo("UPDATE||UWPLAN") == 0) {
            String tSql = "";
            ExeSQL texeSQL = new ExeSQL();
            tSql = "select stateflag from LCAskUWNote where prtno='" + mPrtNo +
                   "' and noteid='" + mNoteID + "'";
            SSRS ssrs = texeSQL.execSQL(tSql);
            if (ssrs.GetText(1, 1).compareTo("0") == 0) { //状态为0时可以修改
                return true;
            } else {
                CError tError = new CError();
                tError.moduleName = "LCAskUWNoteBL";
                tError.functionName = "checkData";
                tError.errorMessage = "只有“未发送”状态的记录可以修改";
                this.mErrors.addOneError(tError);

                return false;
            }
        }

        else {
            return false;
        }

    }

    private boolean dealData() {
        boolean treturn = true;
        if (this.mOperate.compareTo("INSERT||MAIN") == 0) {
            String tSql = "";
            ExeSQL texeSQL = new ExeSQL();
            tSql = "select count(*) from LCAskUWNote where prtno='" + mPrtNo +
                   "'";
            SSRS ssrs = texeSQL.execSQL(tSql);
            if (ssrs.GetText(1, 1).compareTo("0") > 0) {
                System.out.println("非第一次插入记录");
                String cSql = "";
                ExeSQL cexeSQL = new ExeSQL();
                cSql = "select noteid from LCAskUWNote where prtno='" +
                       mPrtNo +
                       "' order by int(noteid) desc";
                SSRS csrs = cexeSQL.execSQL(cSql);
                int tNoteID = Integer.parseInt(csrs.GetText(1, 1)) + 1;
                mLCAskUWNoteSchema.setNoteID(tNoteID + "");
                mLCAskUWNoteSchema.setUWOperator(mGlobalInput.Operator);
                mLCAskUWNoteSchema.setMngCom(mGlobalInput.ManageCom);
                mLCAskUWNoteSchema.setMakeDate(PubFun.getCurrentDate());
                mLCAskUWNoteSchema.setMakeTime(PubFun.getCurrentTime());
                mLCAskUWNoteSchema.setModifyDate(PubFun.getCurrentDate());
                mLCAskUWNoteSchema.setModifyTime(PubFun.getCurrentTime());
                mLCAskUWNoteSchema.setStateFlag("0");
                treturn = true;
            } else {
                System.out.println("第一次插入记录");
                mLCAskUWNoteSchema.setNoteID("1");
                mLCAskUWNoteSchema.setUWOperator(mGlobalInput.Operator);
                mLCAskUWNoteSchema.setMngCom(mGlobalInput.ManageCom);
                mLCAskUWNoteSchema.setMakeDate(PubFun.getCurrentDate());
                mLCAskUWNoteSchema.setMakeTime(PubFun.getCurrentTime());
                mLCAskUWNoteSchema.setModifyDate(PubFun.getCurrentDate());
                mLCAskUWNoteSchema.setModifyTime(PubFun.getCurrentTime());
                mLCAskUWNoteSchema.setStateFlag("0");
                treturn = true;
            }
        }
        if (this.mOperate.compareTo("UPDATE||MAIN") == 0) {
            LCAskUWNoteDB tLCAskUWNoteDB = new LCAskUWNoteDB();
            tLCAskUWNoteDB.setNoteID(mLCAskUWNoteSchema.getNoteID());
            tLCAskUWNoteDB.setGrpContNo(mLCAskUWNoteSchema.getGrpContNo());
            LCAskUWNoteSet tLCAskUWNoteSet = new LCAskUWNoteSet();
            tLCAskUWNoteSet = tLCAskUWNoteDB.query();
            if (tLCAskUWNoteSet.size() > 0) {
                mLCAskUWNoteSchema.setUWPlan(tLCAskUWNoteSet.get(1).getUWPlan());
                mLCAskUWNoteSchema.setUWOperator(tLCAskUWNoteSet.get(1).
                                                 getUWOperator());
                mLCAskUWNoteSchema.setMngCom(tLCAskUWNoteSet.get(1).getMngCom());
                mLCAskUWNoteSchema.setMakeDate(tLCAskUWNoteSet.get(1).
                                               getMakeDate());
                mLCAskUWNoteSchema.setMakeTime(tLCAskUWNoteSet.get(1).
                                               getMakeTime());
                mLCAskUWNoteSchema.setProOperator(mGlobalInput.Operator);
                mLCAskUWNoteSchema.setModifyDate(PubFun.getCurrentDate());
                mLCAskUWNoteSchema.setModifyTime(PubFun.getCurrentTime());
                mLCAskUWNoteSchema.setStateFlag("2");
                treturn = true;
            } else {
                treturn = false;
            }
        }
        if (this.mOperate.compareTo("UPDATE||UWPLAN") == 0) {
            LCAskUWNoteDB tLCAskUWNoteDB = new LCAskUWNoteDB();
            tLCAskUWNoteDB.setNoteID(mLCAskUWNoteSchema.getNoteID());
            tLCAskUWNoteDB.setGrpContNo(mLCAskUWNoteSchema.getGrpContNo());
            LCAskUWNoteSet tLCAskUWNoteSet = new LCAskUWNoteSet();
            tLCAskUWNoteSet = tLCAskUWNoteDB.query();
            if (tLCAskUWNoteSet.size() > 0) {
                mLCAskUWNoteSchema.setUWOperator(mGlobalInput.Operator);
                mLCAskUWNoteSchema.setMngCom(mGlobalInput.ManageCom);
                mLCAskUWNoteSchema.setMakeDate(PubFun.getCurrentDate());
                mLCAskUWNoteSchema.setMakeTime(PubFun.getCurrentTime());
                mLCAskUWNoteSchema.setModifyDate(PubFun.getCurrentDate());
                mLCAskUWNoteSchema.setModifyTime(PubFun.getCurrentTime());
                mLCAskUWNoteSchema.setStateFlag("0");
                treturn = true;
            } else {
                treturn = false;
            }
        }

        return treturn;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData = new VData();
            mLCAskUWNoteSet.add(mLCAskUWNoteSchema);
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLCAskUWNoteSet);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAskUWNoteBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

}
