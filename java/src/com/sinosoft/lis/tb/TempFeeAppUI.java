/**
 * 2008-1-21
 */
package com.sinosoft.lis.tb;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class TempFeeAppUI
{
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    public TempFeeAppUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            System.out.println("Start TempFeeAppUI Submit ...");
            TempFeeAppBL tTempFeeAppBL = new TempFeeAppBL();

            if (!tTempFeeAppBL.submitData(cInputData, cOperate))
            {
                if (tTempFeeAppBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tTempFeeAppBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData", "TempFeeAppBL 发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tTempFeeAppBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("submitData", "意外错误");
            return false;
        }
    }

    public VData getResult()
    {
        return mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "TempFeeAppUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
    }

}
